﻿--select * from [dbo].[FnTableRangoFechas]('20160523','20160525')
CREATE FUNCTION [dbo].[FnTableRangoFechas](@FechaIni smalldatetime, @FechaFin smalldatetime)
RETURNS @TblFecha TABLE(FechaR datetime, FechaI datetime, FechaF datetime, FechaRF datetime)
AS
BEGIN
	DECLARE @Fecha datetime, @FechaFR datetime
	SET @Fecha = @FechaIni
	WHILE(@Fecha <= @FechaFin)
	BEGIN
		SET @FechaFR = DATEADD(SECOND, 86399, @Fecha)
		INSERT INTO @TblFecha(FechaR, FechaI, FechaF, FechaRF)
		VALUES(@Fecha, @FechaIni, @FechaFin, @FechaFR)
		SET @Fecha = DATEADD(DAY, 1, @Fecha)
	END
	RETURN;
END
