﻿Create Function dbo.FnDevuelvoGroupCode(@CardType varchar(2),@IdSNFirstLetter varchar(5))
returns int
As
Begin
	Declare @GroupCode int = 0
	if @CardType = 'C'
	Begin
		if @IdSNFirstLetter = 'C'
			Set @GroupCode = 100
		if @IdSNFirstLetter = 'CEX'
			Set @GroupCode = 104
	End

	if @CardType = 'S' Or @CardType = 'P'
	Begin
		if @IdSNFirstLetter = 'P'
			Set @GroupCode = 101
		if @IdSNFirstLetter = 'PEX'
			Set @GroupCode = 108
		if @IdSNFirstLetter = 'E'
			Set @GroupCode = 112
	End

	return @GroupCode
End
