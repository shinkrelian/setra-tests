﻿Create function dbo.FnAcomodosEspecialesxIDDetxCapacidadxMat(@IDDet int,@QtCapacidad tinyint,@EsMatrimonial bit)
returns varchar(max)
as
Begin
	Declare @TotalAcomodo varchar(max)='',@tiCont tinyint = 0, @TextoAcomodo varchar(max)='',@QtCap tinyint,@EsMat bit
	Declare curAcomodo cursor for
	Select * from(
	Select ch.QtCapacidad,Case When CHARINDEX('(MAT)',ch.NoCapacidad)> 0 then 1 Else 0 End As EsMatrimonial,
	cd.Servicio + ' '+ ch.NoCapacidad as Acomodo
	from COTIDET_ACOMODOESPECIAL ca Left Join CAPACIDAD_HABITAC ch On ca.CoCapacidad = ch.CoCapacidad
	Left Join COTIDET cd On ca.IDDet = cd.IDDet 
	where ca.IDDet = @IDDet) as X
	where X.QtCapacidad = @QtCapacidad And X.EsMatrimonial =@EsMatrimonial
	Open curAcomodo
	Fetch Next From curAcomodo into @QtCap,@EsMat,@TextoAcomodo
	While @@Fetch_status = 0
	Begin
		  If @tiCont=0          
		    Set @TotalAcomodo=@TextoAcomodo         
		  Else          
			Set @TotalAcomodo=@TotalAcomodo+ @TextoAcomodo
		            
		  Set @tiCont = @tiCont + 1          
		Fetch Next From curAcomodo into @QtCap,@EsMat,@TextoAcomodo	
	End
	Close curAcomodo
	DealLocate curAcomodo
	
	if @TotalAcomodo <> ''
		Set @TotalAcomodo = ltrim(rtrim(@TotalAcomodo))
	
	Set @TotalAcomodo = Isnull(@TotalAcomodo,'')
	return @TotalAcomodo
End
