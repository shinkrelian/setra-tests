﻿CREATE Function [dbo].[FnEsTotalxPax](@IDCab int)
	returns bit
as
	Begin
	Declare @bReturn bit = 0, @TotalCotiDet numeric(8,2)=0, @Pax smallint=0, @TotalCotiPax numeric(8,2)=0

	SELECT @TotalCotiDet = (SELECT SUM(TOTAL) FROM COTIDET WHERE IDCAB=CD.IDCAB),
	  @Pax=NroPax+isnull(NroLiberados,0)
	FROM COTICAB CD WHERE CD.IDCab=@IDCab
	
	SELECT @TotalCotiPax=Sum(Total) FROM COTIPAX WHERE IDCab=@IDCab

	If (@TotalCotiDet*@Pax)<>@TotalCotiPax
		Set @bReturn = 1

	Return @bReturn
	End
