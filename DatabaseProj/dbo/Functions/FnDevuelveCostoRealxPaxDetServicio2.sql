﻿CREATE function dbo.FnDevuelveCostoRealxPaxDetServicio2(@IDServicio_Det int,
													   @NroPax int)
returns numeric(12,4)
As
Begin
	Declare @CostoRealxPax numeric(12,4)=0,@AplicaPolitLib bit=0,@TipoLib char(1)='',@NroLiberado int=0,@Tcambio numeric(8,2)=0,
			@ApxRango bit=0,@MaxLiberado tinyint=0
	Select @CostoRealxPax = IsNull(IsNull(Monto_sgl,Monto_sgls),0),@AplicaPolitLib=PoliticaLiberado,
		   @TipoLib=IsNuLL(TipoLib,''),@NroLiberado=Liberado,@Tcambio=IsNull(TC,0),@MaxLiberado=MaximoLiberado
	from MASERVICIOS_DET
	Where IDServicio_Det = @IDServicio_Det
	
	if (@AplicaPolitLib = 1 and @TipoLib='P') and @CostoRealxPax = 0
	Begin
		If (@MaxLiberado > 0 and @NroPax >= @NroLiberado)
			Select @NroPax = @NroPax - @MaxLiberado
		else
			Select @NroPax = @NroPax - (@NroPax / @NroLiberado)
	End
	
	if @CostoRealxPax = 0
	Begin
		Set @CostoRealxPax = dbo.FnDevuelveCostoPorRango2(@IDServicio_Det,@NroPax)
		Set @ApxRango = 1
	End
	
	if @ApxRango = 1
	Begin
		if @NroPax > 0 and @CostoRealxPax > 0
			Set @CostoRealxPax = @CostoRealxPax / @NroPax
		
		if @Tcambio > 0 and @CostoRealxPax > 0
			Set @CostoRealxPax = @CostoRealxPax / @Tcambio
	End
	
	return @CostoRealxPax
End
