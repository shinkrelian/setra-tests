﻿
--------------------------------------------------------------------------------------------
--
CREATE Function [dbo].[FnDocumentoSunat_Obtener_RegistroVentas] (@NuDocum char(10))
	Returns char(2) As
BEGIN
declare @CoSunat char(2)=null

declare @NuDocum2 int=CAST(@NuDocum AS int)
set @CoSunat = null

--while (@CoSunat is null)
begin
		set @CoSunat=(SELECT  top 1  MATIPODOC.CoSunat
						FROM         DOCUMENTO INNER JOIN
								  MATIPODOC ON DOCUMENTO.IDTipoDoc = MATIPODOC.IDtipodoc
   						WHERE     (cast(DOCUMENTO.NuDocum AS int)+1 = @NuDocum2)
   					)
   		set @NuDocum2=@NuDocum2+1	
end


RETURN @CoSunat
END

