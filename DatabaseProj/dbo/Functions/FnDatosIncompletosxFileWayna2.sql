﻿Create Function dbo.FnDatosIncompletosxFileWayna2(@IDCab int)
	returns bit
As
	Begin
	Declare @bIncompletos bit=0
	
	If Not Exists(
		Select IDPax From COTIPAX Where IDCab=@IDCab And (
			FlEntWP=1
		))

		Set @bIncompletos=1

	Return @bIncompletos
	End
