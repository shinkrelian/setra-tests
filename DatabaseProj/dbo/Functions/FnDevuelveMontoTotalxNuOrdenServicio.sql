﻿Create Function dbo.FnDevuelveMontoTotalxNuOrdenServicio(@NuOrdenServicio int)
returns numeric(9,2)
as
Begin
	Declare @NuIgv numeric(9,2)=(select NuIGV from parametro)
	Declare @MontoTotal numeric(9,2)=(Select SUM(isnull(Case when isnull(TotalCotizadoCal,0) = 0 then                    
															Cast(Case When Afecto=1 Then NetoGen+(NetoGen*(@NuIgv/100)) Else NetoGen End as numeric(8,2))
																else TotalProgram End,0)) as TotalGen
					 from (                    
					 Select Case When isnull(odds.NetoCotizado,0) = 0 then                                                 
							   Case When dCot.IDServicio_Det IS NULL Then           
								  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(odds.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)),                    
								   dCot.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)
								 Else          
								   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)),                  
								   dCot.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)
								 End          
							  Else                 
							 odds.TotalProgram                 
							 End As NetoGen,odds.TotalProgram,isnull(odds.TipoCambio,0) as TipoCambio,odds.IDMoneda as IDMonedaOper,
							 odds.TotalCotizado As TotalCotizadoCal,isnull(dCot.Afecto,d.Afecto) as Afecto
					 From ORDEN_SERVICIO os
						Left Join ORDEN_SERVICIO_DET osd On os.NuOrden_Servicio=osd.NuOrden_Servicio
						Left Join OPERACIONES_DET_DETSERVICIOS odds On osd.IDServicio_Det=odds.IDServicio_Det and osd.IDOperacion_Det=odds.IDOperacion_Det and odds.Activo=1
						Left Join MASERVICIOS_DET d On d.IDServicio_Det = odds.IDServicio_Det
						Left Join OPERACIONES_DET Rd On odds.IDOperacion_Det=Rd.IDOperacion_Det
						Left Join OPERACIONES Rv On Rd.IDOperacion=Rv.IDOperacion
						Left Join MASERVICIOS_DET dCot On Isnull(odds.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det
						Left Join RESERVAS_DET rdd On Rd.IDReserva_Det=rdd.IDReserva_Det
						where os.NuOrden_Servicio = @NuOrdenServicio)
					as X)
	Set @MontoTotal = Isnull(@MontoTotal,0)
	return @MontoTotal
End
