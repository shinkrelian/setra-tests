﻿--PPMG-20151002--Linea 10 y 72 Si no existe IsNull(cd.IDDetRel,0)=0 envia la descripción del servicio
CREATE Function [dbo].[FnDevuelveAcomodoBookingConfirmation](@IDDet int,@IDCab int)
returns varchar(Max)
As
Begin
	Declare @Acomodo varchar(Max)=''
	Declare @IDTipoProv char(3)='',@Servicio varchar(100)='',@NombreProveedor varchar(100)='',@ConAlojamiento bit=0,@PlanAlimenticio bit=0,@AcomodoEspecial bit=0
	Declare @CantidadTemp tinyint=0,@ServCapac varchar(50)=''
	--Acomdos Normales
	Declare @CantSimple tinyint, @CantTwin tinyint, @CantMat tinyint, @CantTriple tinyint
	SELECT @CantSimple = IsNull(Simple,0)+IsNull(SimpleResidente,0),
		   @CantTwin = IsNull(Twin,0)+IsNull(TwinResidente,0),
		   @CantMat = IsNull(Matrimonial,0)+IsNull(MatrimonialResidente,0),
		   @CantTriple = IsNull(Triple,0)+IsNull(TripleResidente,0)
	FROM COTICAB where IDCAB=@IDCab
	--Si existe detalle relacionados
	IF EXISTS(select p.IDTipoProv,sd.ConAlojamiento,sd.PlanAlimenticio,AcomodoEspecial,cd.Servicio,p.NombreCorto
			  from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
			  Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
			  where IDDET=@IDDet And IsNull(cd.IDDetRel,0)=0)
	BEGIN
		--Tipo de Servicio (Solo alojamiento u Hotel sin planAlimenticio) Y sub-dividir en acomodos normales o especiales
		select @IDTipoProv=p.IDTipoProv,@ConAlojamiento=sd.ConAlojamiento,@PlanAlimenticio=sd.PlanAlimenticio,
			   @AcomodoEspecial=AcomodoEspecial,@Servicio=cd.Servicio,@NombreProveedor = p.NombreCorto
		from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
		Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
		where IDDET=@IDDet And IsNull(cd.IDDetRel,0)=0

		--Acomdos Normales
		/*Declare @CantSimple tinyint = (select IsNull(Simple,0)+IsNull(SimpleResidente,0) from COTICAB where IDCAB=@IDCab)
		Declare @CantTwin tinyint = (select IsNull(Twin,0)+IsNull(TwinResidente,0) from COTICAB where IDCAB=@IDCab)
		Declare @CantMat tinyint = (select IsNull(Matrimonial,0)+IsNull(MatrimonialResidente,0) from COTICAB where IDCAB=@IDCab)
		Declare @CantTriple tinyint = (select IsNull(Triple,0)+IsNull(TripleResidente,0) from COTICAB where IDCAB=@IDCab)*/

		--Set @AcomodoEspecial = 0
		if (@IDTipoProv='001' And @PlanAlimenticio=0) Or (@IDTipoProv<>'001' And @ConAlojamiento=1)
		Begin
			If @AcomodoEspecial=1
			Begin	
				Declare CurAcoEsp cursor for
				Select ca.QtPax,ch.NoCapacidad from COTIDET_ACOMODOESPECIAL ca Left Join CAPACIDAD_HABITAC ch On ca.CoCapacidad=ch.CoCapacidad
				where IDDet=@IDDet
				Order By ch.QtCapacidad,ch.FlMatrimonial asc
				Open CurAcoEsp
				Fetch Next From CurAcoEsp into @CantidadTemp,@ServCapac
				While @@FETCH_STATUS =0 
				Begin	
					Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantidadTemp as varchar(2))+' '+@Servicio+' '+@ServCapac + CHAR(13)
					Fetch Next From CurAcoEsp into @CantidadTemp,@ServCapac
				End
				Close CurAcoEsp
				DealLocate CurAcoEsp
			End
			Else
			Begin
				if @CantSimple > 0
					Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantSimple as varchar(2))+' '+@Servicio+' SINGLE' + CHAR(13)
				if @CantTwin > 0
					Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantTwin as varchar(2))+' '+@Servicio+' TWIN' + CHAR(13)
				if @CantMat > 0
					Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantMat as varchar(2))+' '+@Servicio+' DOUBLE(MAT)' + CHAR(13)
				if @CantTriple > 0
					Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantTriple as varchar(2))+' '+@Servicio+' TRIPLE' + CHAR(13)
			End
		End

		if Ltrim(Rtrim(@Acomodo))<>''
			Set @Acomodo = SUBSTRING(@Acomodo,1,Len(@Acomodo)-1)	
	END
	ELSE
	BEGIN
		SELECT @Servicio = '', @NombreProveedor = ''
		select @Servicio=cd.Servicio,@NombreProveedor = p.NombreCorto
		from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
		Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
		where IDDET=@IDDet

		if @CantSimple > 0
			Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantSimple as varchar(2))+' '+@Servicio+' SINGLE '
		if @CantTwin > 0
			Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantTwin as varchar(2))+' '+@Servicio+' TWIN '
		if @CantMat > 0
			Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantMat as varchar(2))+' '+@Servicio+' DOUBLE(MAT) '
		if @CantTriple > 0
			Set @Acomodo = @Acomodo + @NombreProveedor+' - '+Cast(@CantTriple as varchar(2))+' '+@Servicio+' TRIPLE '
	END

	Return @Acomodo
End
