﻿Create Function dbo.FnDevuelveTituloTablaHoteles(@IDCab int)
returns varchar(50)
As
Begin
Declare @Titulo varchar(50) =
(select Top(1) Case X.OrdenCat when 1 then 'Tourist' when 2 then 'First' when 3 then 'Deluxe' End as Titulo
from (
select c.Descripcion, Case When s.IDCat In ('001','002','009','008','007') then 1 else
	Case When s.IDCat In ('003','004') then 2 else 3 End End as OrdenCat
from COTIDET cd 
Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
Left Join MACATEGORIA c On s.IDCat=c.IDCat
Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
where IDCAB=@IDCab And p.IDTipoProv='001'
) as X
Order By x.OrdenCat desc)
return @titulo
End
