﻿create FUNCTION F_Unidades(@Numero as bigint, @Estilo as bit=0) 
RETURNS varchar(500) AS 
BEGIN 
DECLARE @Texto varchar(500)
SELECT @Texto=''
SELECT @Texto=
CASE 
WHEN @Numero=0 THEN 'CERO '
WHEN @Numero=1 THEN 'UNO '
WHEN @Numero=2 AND @Estilo=0 THEN 'DOS '
WHEN @Numero=2 AND @Estilo=1 THEN 'DOS '
WHEN @Numero=3 AND @Estilo=0 THEN 'TRES '
WHEN @Numero=3 AND @Estilo=1 THEN 'TRES '
WHEN @Numero=4 THEN 'CUATRO '
WHEN @Numero=5 THEN 'CINCO '
WHEN @Numero=6 AND @Estilo=0 THEN 'SEIS '
WHEN @Numero=6 AND @Estilo=1 THEN 'SEIS '
WHEN @Numero=7 THEN 'SIETE '
WHEN @Numero=8 THEN 'OCHO '
WHEN @Numero=9 AND @Estilo=0 THEN 'NUEVE '
WHEN @Numero=9 AND @Estilo=1 THEN 'NUEVE '
END
RETURN @Texto
END 
