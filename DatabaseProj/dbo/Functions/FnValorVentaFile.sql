﻿CREATE Function dbo.FnValorVentaFile(@IDCab int)    
returns numeric(13,2)    
As    
 Begin    
 --declare @IDCab int=7144    
 Declare @ValorVentaReturn numeric(13,2)=0    
 Declare @EstadoFacturac char(1)='',@Simple tinyint,@Twin tinyint,@Matrim tinyint,@Triple tinyint, @NroPax smallint, @NroLiberados smallint    
     
 Select    
 @EstadoFacturac=EstadoFacturac,@Simple=ISNULL(Simple,0),@Twin=isnull(Twin,0),@Matrim=isnull(Matrimonial,0),@Triple=isnull(Triple,0),    
  @NroPax=NroPax, @NroLiberados=ISNULL(NroLiberados,0)    
 From COTICAB Where IDCAB=@IDCab    
    
    
 If @EstadoFacturac='F'     
  Begin    
  --select '1'    
  Set @ValorVentaReturn=isnull((Select SUM(Total) From DEBIT_MEMO Where IDCab=@IDCab and IDEstado<>'AN') ,0)    
  End    
 Else    
  Begin    
   --select '2'    
   Declare @bExisteHotel bit=(Select Case When Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                            
   Where cd.IDCAB = @IDCab and p.IDTipoProv = '001') Then 1 Else 0 End)    
   If @bExisteHotel=1    
    Begin    
    --select '3'    
    Declare @bEsTotalxPax bit=dbo.FnEsTotalxPax(@IDCab)     
    If @bEsTotalxPax=1    
     Begin    
     --select '3.1'    
     Set @ValorVentaReturn=isnull((Select sum(total) From COTIPAX Where idcab=@IDCab),0)
     Set @ValorVentaReturn = @ValorVentaReturn + dbo.FnDevuelveMontoTotalxPaxVuelos(@IDCab,0)       
     End    
    Else    
     Begin        
     --select '3.2'    
     Declare @TotalDoble numeric(10,2)=(select SUM(Total) from COTIDET cd Where cd.IDCAB = @IDCAB) *(@Twin+@Matrim) *2     
     Declare @TotalSimple numeric(10,2)=(select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = @IDCAB) * @Simple    
     Declare @TotalTriple numeric(10,2)=(select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = @IDCAB) * (@Triple*3)    
       
     Set @ValorVentaReturn=@TotalSimple+@TotalDoble+@TotalTriple    
     Set @ValorVentaReturn = @ValorVentaReturn + dbo.FnDevuelveMontoTotalxPaxVuelos(@IDCab,1)
     End    
    
    End    
   Else    
    Begin    
    --select '4'    
    Declare @TotalCotiz numeric(10,2)=(select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = @IDCAB)     
    Set @ValorVentaReturn=@TotalCotiz*(@NroPax + @NroLiberados)    
    Set @ValorVentaReturn = @ValorVentaReturn + dbo.FnDevuelveMontoTotalxPaxVuelos(@IDCab,1)
    End    
    
  End    
    
 return @ValorVentaReturn    
 --print @ValorVentaReturn    
 End    
