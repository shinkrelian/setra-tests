﻿Create Function dbo.FnCorrelativoDMValido(@IDCab int)  
returns char(2)  
As  
Begin  
 Declare @CorrelativoMin char(2) = (select Top(1) substring(IDDebitMemo,9,10) as Correlativo 
									from DEBIT_MEMO where IDCab = @IDCab and IDEstado <> 'AN' 
									Order by cast(substring(IDDebitMemo,9,10) as tinyint))
 return @CorrelativoMin  
End  
