﻿CREATE Function [dbo].[FnNuevaTarifa](@IDServicio char(8), @Variante varchar(7), @Anio char(4), 
	@FechaDiaServ smalldatetime, @IDServicio_Det int,@Codigo varchar(20) )
	returns int
As
	Begin	
	
	Declare @IDServicio_DetRet int=0		

	SELECT --top 1
		@IDServicio_DetRet = IDServicio_Det FROM MASERVICIOS_DET where IDServicio=@IDServicio and tipo=@Variante
		--and cast(Anio as smallint)>cast(@Anio as smallint)
		and Codigo=@Codigo 
		and Anio = year(@FechaDiaServ) and IDServicio_Det>@IDServicio_Det
		AND Estado='A' AND year(@FechaDiaServ)>cast(@Anio as smallint)
		
		--Order by IDServicio_Det desc

	--if @IDServicio_DetRet=0
	--	Set @IDServicio_DetRet = @IDServicio_Det
    
	Return @IDServicio_DetRet
	End

