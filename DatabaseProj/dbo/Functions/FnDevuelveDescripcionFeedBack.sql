﻿Create Function dbo.FnDevuelveDescripcionFeedBack(@Excellent bit,@VeryGood bit,@Good bit,@Average bit,@Poor bit)
returns varchar(Max)
As
Begin
	Declare @texto varchar(Max)=''
	if @Excellent = 1
		Set @texto = 'excellent'
	if @VeryGood = 1
		Set @texto = 'verygood'
	if @Good = 1
		Set @texto = 'good'
	if @Average = 1
		Set @texto = 'average'
	if @Poor = 1
		Set @texto = 'poor'

	return @texto
End
