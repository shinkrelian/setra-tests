﻿create FUNCTION F_Centenas(@Numero as bigint, @Estilo as bit=0) 
RETURNS varchar(500) AS 
BEGIN 
DECLARE @Texto varchar(500)
SELECT @Texto=''
SELECT @Texto=
CASE 
WHEN @Numero=000 THEN ' '
WHEN @Numero=100 THEN 'CIEN'
WHEN @Numero>100 and @Numero<200 THEN 'CIENTO ' + 
dbo.F_Decenas(RIGHT(CONVERT(varchar, @Numero), 2), 0)
WHEN (@Numero>=200 and @Numero<500) or 
(@Numero>500 and @Numero<1000) THEN 
dbo.F_Decenas(LEFT(CONVERT(varchar, @Numero), 1), 1) + 
'CIENTOS ' + 
dbo.F_Decenas(RIGHT(CONVERT(varchar, @Numero), 2), 1)
WHEN @Numero=500 THEN 'QUINIENTOS'
WHEN @Numero<10 THEN dbo.F_Unidades(@Numero, 0)
WHEN @Numero<100 THEN dbo.F_Decenas(@Numero, @Estilo)
END
RETURN @Texto
END 
