﻿Create Function dbo.FnRedondearValor(@Numero numeric(8,2))
returns numeric(8,2)
As
Begin
	Declare @NumeroRetorno numeric(8,2)=0
	Declare @NumStr varchar(Max) = Cast(@Numero as varchar(Max))
	Declare @UltNum char(1)=SUBSTRING(@NumStr,Len(@NumStr),Len(@NumStr))
	Declare @NumEnt varchar(Max)=SUBSTRING(@NumStr,charindex(@NumStr,'.')-2,Len(@NumStr))
	Declare @DecimalPart varchar(Max)=Replace(@NumStr,@NumEnt+'.','')
	
	if @UltNum='1'
		Set @NumeroRetorno =@Numero -0.01
	if @UltNum='9'
		Set @NumeroRetorno =@Numero +0.01
	if (@UltNum<>'1' and @UltNum<>'9') and @NumeroRetorno=0 --Suma entre la parte Numerica y Decimal
		Set @NumeroRetorno = cast(@NumEnt as numeric(8,2))+(cast(@DecimalPart as numeric(8,2)) / power(10,Len(@DecimalPart)))
	
	return @NumeroRetorno
End
