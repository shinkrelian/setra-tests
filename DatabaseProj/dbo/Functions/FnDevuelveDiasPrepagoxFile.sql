﻿Create Function dbo.FnDevuelveDiasPrepagoxFile(@IDCab int,@IDProveedor char(6))
returns smallint
As
Begin
Declare @DiasPrePago int = 0
Declare @NroPax int = 0

Select @NroPax = NroPax+IsNull(NroLiberados,0) from COTICAB where IDCAB=@IDCab

select top(2) @DiasPrePago = DiasSinPenalidad
from MAPOLITICAPROVEEDORES 
where IDProveedor=@IDProveedor And DefinTipo='PAX' And @NroPax between DefinDia and DefinHasta
Order By CAST(Anio as int) desc--,DiasSinPenalidad desc

--select @DiasPrePago
Return @DiasPrePago
End
