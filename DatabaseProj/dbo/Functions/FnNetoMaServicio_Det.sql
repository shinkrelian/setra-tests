﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--select dbo.[FnNetoMaServicio_Det](35412,18)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--JHD-20150223- Se amplio el tipo de dato de @ValorReturn: Declare @ValorReturn numeric(12,2)=0
CREATE Function [dbo].[FnNetoMaServicio_Det] 
	(@IDServicio_Det int, @NroPaxLib smallint)

Returns numeric(12,2)
As 

Begin	
	--Declare @ValorReturn numeric(8,2)=0
	Declare @ValorReturn numeric(12,2)=0
	Declare @Monto_sgl numeric(8,2)
	Declare @Monto_sgls numeric(8,2)
	Declare @TC numeric(8,2)
	
	Select @Monto_sgl=Monto_sgl, @Monto_sgls=Monto_sgls, @TC=TC 
		From MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det

	If @Monto_sgl Is Null		
		Select @ValorReturn=Monto From MASERVICIOS_DET_COSTOS   
		Where IDServicio_Det=@IDServicio_Det And  
		@NroPaxLib Between PaxDesde And PaxHasta  
	Else
		Begin
		If @TC Is Null 
			Set @ValorReturn=@Monto_sgl * @NroPaxLib	
		Else
			Set @ValorReturn=@Monto_sgls * @NroPaxLib	
		End
	Return Isnull(@ValorReturn,0)
End


