﻿
Create Function dbo.FnNumeroEncontradoenCadena(@vcCadena varchar(200))
	returns bit
As
	Begin
	Declare @cCar char(1), @tiCont tinyint=1

	While (@tiCont<=Len(@vcCadena))
		Begin
		Set @cCar=SUBSTRING(@vcCadena,@tiCont,1)
		If ISNUMERIC(@cCar)=1
			return 1

		Set @tiCont+=1
		End

	return 0
	End
