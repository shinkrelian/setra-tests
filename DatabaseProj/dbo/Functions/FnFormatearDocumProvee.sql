﻿
---------------------------------------------------------------------------

--JHD-20141205-Se cambio el tipo de dato de @NuDocum a varchar(30)
--JHD-20141205-Se cambio el tipo de dato de @@ValorReturn a varchar(31)
CREATE Function [dbo].[FnFormatearDocumProvee]     
 --(@NuDocum varchar(15))    
 
 (@NuDocum varchar(30))    
    
--Returns varchar(16)   
Returns varchar(31)   
As     
    
Begin     
	--Declare @ValorReturn varchar(16)='' 
	Declare @ValorReturn varchar(31)='' 
	
	
	If ltrim(rtrim(@NuDocum))<>''
		Begin
		Declare @vcTamanioDoc as tinyint=Len(ltrim(rtrim(@NuDocum)))
		Declare @tiCantCar tinyint=3	
		Inicio:
		Declare @vcSerie varchar(10)=substring(@NuDocum,1,@tiCantCar)
		
		If cast(@vcSerie as smallint)=0
			Begin
			Set @tiCantCar+=1
			goto Inicio
			End
		Else
			Set @ValorReturn=@vcSerie+'-'+substring(@NuDocum,len(@vcSerie)+1,@vcTamanioDoc-len(@vcSerie))
		
		End

	Return @ValorReturn    
End    



