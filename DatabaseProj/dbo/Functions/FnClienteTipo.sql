﻿
CREATE FUNCTION [dbo].[FnClienteTipo](@Tipo char(2))
RETURNS varchar(30)
AS
BEGIN
	DECLARE @ReturnTipo varchar(30)=''
	SELECT @ReturnTipo = CASE WHEN @Tipo = 'CO' THEN 'COUNTER'
							  WHEN @Tipo = 'WB' THEN 'WEB'
							  WHEN @Tipo = 'CL' THEN 'CLIENTE'
							  WHEN @Tipo = 'IT' THEN 'INTERESADO'
							  WHEN @Tipo = 'ND' THEN 'NO DESEADO'
						 END
	RETURN @ReturnTipo
END
