﻿

CREATE Function dbo.Fn_MASERVICIOS_Sel_VerificaHora
	(@IDServicio char(8),        
	@FechaDia varchar(20))         
	Returns Bit       

	Begin
	Declare @Hora Time(4),@Rows tinyint,@NumDiaFechaDia tinyint, @bRet bit=0
     
	set @Rows = (Select  Count(*) from MASERVICIOS_ATENCION Where IDservicio = @IDServicio)    
	Set @FechaDia=CONVERT(SMALLDATETIME,@FechaDia,103)    
	set @Hora = Cast(@FechaDia As Time(0))    
	Set @NumDiaFechaDia = dbo.FnDiaSemana(Case When SUBSTRING(DATENAME(dw,@FechaDia),0,2) = 'M' Then       
			Case when SUBSTRING(DATENAME(dw,@FechaDia),0,3) = 'MI'      
			Then 'X'      
			Else SUBSTRING(DATENAME(dw,@FechaDia),0,2) End      
		Else SUBSTRING(DATENAME(dw,@FechaDia),0,2) End)      
	--Declare @bitDia bit    
	--Execute MASERVICIOS_Sel_VerificaDiaOutPut @IDServicio,@FechaDia,@bitDia Output    
	--if @bitDia    
        
	If @Rows <> 0  and CONVERT(char(5),@Hora,108)<>'00:00'
		If Exists(select IDAtencion from MASERVICIOS_ATENCION Where IDservicio =@IDServicio 
				And (@NumDiaFechaDia <= dbo.FnDiaSemana(Dia2) and @NumDiaFechaDia >= dbo.FnDiaSemana(Dia1))     
				And cast(@Hora as smalldatetime) between Desde1 And Hasta1)
			set @bRet  =1
		else
			If Exists(select IDAtencion from MASERVICIOS_ATENCION Where IDservicio =@IDServicio 
				And (@NumDiaFechaDia <= dbo.FnDiaSemana(Dia2) and @NumDiaFechaDia >= dbo.FnDiaSemana(Dia1))     
				And cast(@Hora as smalldatetime) between Desde2 And Hasta2)
				set @bRet  =1
			else 
				set @bRet  =0
		Else    
			set @bRet = 1    

		Return @bRet
	End

