﻿

-----------------------------------------------------------------------------------------------------

create FUNCTION FnConvertir_Segundos_a_Tiempo
      (@nsecs int)
       
RETURNS nvarchar(8)
WITH EXECUTE AS CALLER
AS
-- place the body of the function here
BEGIN
      declare @rc nvarchar(12)
--    declare @nSecs int = 1234
       
      select @rc = right(replicate('0', 2) + ltrim(datepart(minute, convert(time, dateadd(second, @nsecs, '0:00')))),2) +
       ':' + right(replicate('0', 2) + ltrim(datepart(second, convert(time, dateadd(second, @nsecs, '0:00')))),2)
 
      if @nsecs > 60 * 60 -- more than 1 hour...then prepend # hrs
     
      begin
            declare @nHrs nvarchar(5)
             declare @nHrs2 nvarchar(5)
            set @nHrs = convert(nvarchar,datepart(hour, convert(time, dateadd(second, @nSecs, '0:00'))))
            set @nHrs2 = cast(@nsecs/3600 as nvarchar(5))
            set @nHrs2= REPLICATE('0',2-LEN(@nHrs2))+@nHrs2
            set @rc = @nHrs2 + ':' + @rc
            set @rc =  (select case when len(@rc)=5 then '00:'+@rc else @rc end)
      end
 
--    select dbo.SecsToTime(12345)
      return (select case when len(@rc)=5 then '00:'+@rc else @rc end) --@rc
END
