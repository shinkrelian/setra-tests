﻿--JRF-20131216-Ampliar la Cantidad para acomodos de mayor rango
CREATE Function [dbo].[FnMailAcomodosReservas]   
 (@Cantidad tinyint, @EsMatrimonial bit)  
Returns varchar(10)  
As   
Begin  
 Declare @bResultador varchar(10)  
 if(@Cantidad=1)  
  set @bResultador='SGL'  
 if(@Cantidad=2)  
  If(@EsMatrimonial=1)  
   set @bResultador='MAT'  
  else  
   set @bResultador='TWIN'  
 if(@Cantidad=3)  
  set @bResultador='TPL'  
 if(@Cantidad=4)  
  set @bResultador='QUAD'  
  if(@Cantidad>4)  
  set @bResultador= Cast(@Cantidad as varchar(5))+'PAX'
 Return @bResultador   
End  
