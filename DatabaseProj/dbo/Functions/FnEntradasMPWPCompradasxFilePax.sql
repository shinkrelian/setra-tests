﻿
CREATE Function [dbo].[FnEntradasMPWPCompradasxFilePax](@IDCab int, @IDPax int,@vcMPWP varchar(10))
	returns bit
As
	Begin
	Declare @bComprados bit=0	
	Declare @IDServicioMPWP char(8)

	If @vcMPWP = 'MAPI'		
		Set @IDServicioMPWP='CUZ00132'		
	If @vcMPWP = 'WAYNA'		
		Set @IDServicioMPWP='CUZ00134'
		
	Declare @Pax smallint=0, @EntCompradasMP smallint=0,@EntCompradasWP smallint=0

	Select @Pax = isnull(Sum(isnull(Pax,0)),0), @EntCompradasMP = isnull(Sum(isnull(EntCompradasMP,0)),0),
		@EntCompradasWP = isnull(Sum(isnull(EntCompradasWP,0)),0)
	From
		(Select 
		(Select COUNT(*) From COTIDET_PAX Where IDDET=cd.IDDet And IDPax=@IDPax And FlDesactNoShow=0) as Pax, 
		(Select COUNT(*) From COTIDET_PAX Where IDDET=cd.IDDet And IDPax=@IDPax And FlEntMPGenerada=1 And FlDesactNoShow=0) as EntCompradasMP,
		(Select COUNT(*) From COTIDET_PAX Where IDDET=cd.IDDet And IDPax=@IDPax And FlEntWPGenerada=1 And FlDesactNoShow=0) as EntCompradasWP		
		From COTIDET cd Inner Join MASERVICIOS_DET sd1 On cd.IDServicio_Det=sd1.IDServicio_Det
		--Inner Join MASERVICIOS_DET sd2 On sd1.IDServicio_Det_V=sd2.IDServicio_Det
		--	And sd2.IDServicio IN(@IDServicioMPWP,'CUZ00133') 					
		Where cd.IDCAB=@IDCab 
			--And dbo.FnServicioMPWP(sd1.IDServicio_Det) in(@IDServicioMPWP,'CUZ00133')
			AND
		(SELECT Top 1 IDServicio FROM MASERVICIOS_DET where IDServicio_Det in(SELECT IDServicio_Det_V 
		FROM MASERVICIOS_DET where IDServicio=SD1.IDServicio and Anio=SD1.Anio and tipo=SD1.Tipo)
			And IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00912','CUZ00946'))
			in(@IDServicioMPWP,'CUZ00133')

		) as X

	If @vcMPWP = 'MAPI'
		Begin
		If @Pax=@EntCompradasMP And @Pax>0
			Set @bComprados = 1
		End
	If @vcMPWP = 'WAYNA'
		Begin
		If @Pax=@EntCompradasWP And @Pax>0
			Set @bComprados = 1
		End
	Return @bComprados
	End


