﻿--HLF-20140819-Comentando If @IDMonedaOutput='SOL' por Else    
--HLF-20140929-If @TCambio<>0...  
--HLF-20141120-@TCambio numeric(8,2) a numeric(8,3)
--Returns numeric(12,2) a numeric(12,4) 
CREATE Function [dbo].[FnCambioMoneda]       
 (@MontoInput numeric(12,2),       
 @IDMonedaInput char(3),       
 @IDMonedaOutput char(3),       
 @TCambio numeric(8,3))      
      
Returns numeric(12,4)      
As       
      
Begin       
 Declare @ValorReturn numeric(12,4)=0      
  
 If @IDMonedaInput <> @IDMonedaOutput        
  Begin      
  If @IDMonedaOutput='USD'      
   Begin     
   If @TCambio<>0  
    Set @ValorReturn = @MontoInput/@TCambio      
   Else  
    Set @ValorReturn = 0  
   End  
  Else    
   Set @ValorReturn = @MontoInput*@TCambio         
  End      
 Else      
  Set @ValorReturn = @MontoInput      
  
 Return @ValorReturn      
End 
