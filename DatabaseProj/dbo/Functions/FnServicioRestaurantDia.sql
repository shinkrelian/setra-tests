﻿
CREATE Function dbo.FnServicioRestaurantDia
	(@IDCab int, 
	 @IDProveedor char(6), 
	 @IDReserva_Det int)

	Returns Varchar(250)
As 

Begin	
	Declare @ValorReturn Varchar(250)=''


	Select 
	@ValorReturn=
	IsNull('R: '+(Select Top 1 p1.NombreCorto + ' - ' + sd1.Descripcion From RESERVAS_DET rd1 
	Inner Join MASERVICIOS_DET sd1 On sd1.IDServicio_Det=rd1.IDServicio_Det 
	Inner Join RESERVAS r1 On rd1.IDReserva=r1.IDReserva And r1.IDCab=@IDCab
	Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor 
	And (p1.IDTipoProv='002' Or sd1.planalimenticio=1)
	Where CONVERT(smalldatetime ,CONVERT(varchar, rd1.Dia,103))=CONVERT(smalldatetime ,CONVERT(varchar, rd.Dia,103))		

	Order by rd1.FechaOut Desc
	),'')
	From RESERVAS o 
	Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor 
	And o.IDCab=@IDCab And o.IDProveedor=@IDProveedor
	--Inner Join OPERACIONES_DET od On o.IDOperacion=od.IDOperacion
	Inner Join RESERVAS_DET rd On o.IDReserva=rd.IDReserva  
	Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=rd.IDServicio_Det 	
	And Not  (p.IDTipoProv='002' Or sd.planalimenticio=1) 
	And rd.IDReserva_Det = @IDReserva_Det
	Order by rd.Dia
	
	Return IsNull(@ValorReturn,'')
End


