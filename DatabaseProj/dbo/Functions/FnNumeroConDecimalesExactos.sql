﻿Create Function dbo.FnNumeroConDecimalesExactos(@NumeroCompleto varchar(Max),@CantDec tinyint)
returns varchar(Max)
As
Begin

	Declare @IndPoint int = (select CHARINDEX('.',@NumeroCompleto))
	Declare @NumEnt varchar(Max) = (select SUBSTRING(@NumeroCompleto,1,@IndPoint))
	Declare @NumDec varchar(Max) = (select SUBSTRING(@NumeroCompleto,@IndPoint+1,@CantDec))
	
	if ltrim(rtrim(@NumDec))='' 
	Begin
		set @NumEnt = replace(@NumEnt,'.','')
	End
	else
	Begin
		set @NumEnt = @NumEnt + @NumDec
	End
	return @NumEnt
End
