﻿Create Function dbo.FnGastosTransferencia(
	@IDCab int,
	@CoProveedor char(6),
	@CoFormaPago char(3)
	)
Returns numeric(10,2)      
As      
	Begin 
	Declare @Total numeric(10,2)=0, @CantFiles numeric(5,2)=0
	If @CoFormaPago<>'002'--Prepago
		Begin	
		SELECT @Total=isnull(SUM(CD.Total),0) FROM MAPROVEEDORES P 
			INNER JOIN COTIDET CD ON CD.IDProveedor=P.IDProveedor and cd.IDProveedor=@CoProveedor AND 
			CD.IDCAB in(Select cc1.IDCab From COTICAB cc1 Inner Join COTIDET cd1 On cc1.IDCAB=cd1.IDCAB
				Where cd1.IDProveedor=@CoProveedor And
				CONVERT(char(6),cc1.FechaOut,112)=(Select CONVERT(char(6),FechaOut,112) 
				From COTICAB Where IDCAB=@IDCab ))
			inner join COTICAB cc on cc.IDCAB=cd.IDCAB and cc.Estado='A'
			Where Not Exists(Select IDProveedor From MAADMINISTPROVEEDORES Where IDProveedor=p.IDProveedor and TipoCuenta='NP')
		Select @CantFiles=COUNT(*) From 
		(select distinct cc2.IDCAB from COTICAB cc2 Inner Join COTIDET cd2 On cc2.IDCAB=cd2.IDCAB
				Inner Join MAPROVEEDORES p2 On cd2.IDProveedor=p2.IDProveedor 
				Where cd2.IDProveedor=@CoProveedor And CONVERT(char(6),cc2.FechaOut,112)=(Select CONVERT(char(6),FechaOut,112) 
					From COTICAB Where IDCAB=@IDCab )
				and cc2.Estado='A' And Not Exists(Select IDProveedor From MAADMINISTPROVEEDORES Where IDProveedor=p2.IDProveedor and TipoCuenta='NP')
				) as X
		End
	Else --como Liquid. Mensual	
		SELECT @Total=isnull(SUM(CD.Total),0) FROM MAPROVEEDORES P 
			INNER JOIN COTIDET CD ON CD.IDProveedor=P.IDProveedor and cd.IDProveedor=@CoProveedor AND 
			CD.IDCAB = @IDCab	

	Declare @TotReturn numeric(10,2)
	If @CantFiles=0 Set @CantFiles=1
	If @Total<=10000
		Set @TotReturn = 60/@CantFiles
	else
		Set @TotReturn = 70/@CantFiles	

	Return @TotReturn
	End

	

