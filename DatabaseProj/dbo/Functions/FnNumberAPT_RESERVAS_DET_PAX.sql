﻿CREATE FUNCTION [dbo].[FnNumberAPT_RESERVAS_DET_PAX]
						(
						 @IDCab int,
						 @IDReserva_Det int
						)
RETURNS varchar(max)
AS
BEGIN
	DECLARE @sNumberAPT varchar(max)
	SET @sNumberAPT = ''

	--SELECT @sNumberAPT = COALESCE(@sNumberAPT, '') + CASE WHEN ISNULL(CP1.NumberAPT,'') = '' THEN '' ELSE CP1.NumberAPT + ' / ' END
	--FROM RESERVAS_DET_PAX RDP1 INNER JOIN COTIPAX CP1 ON RDP1.IDPax = CP1.IDPax AND CP1.IDCab = @IDCab
	--WHERE IDReserva_Det = @IDReserva_Det AND CP1.FlTC = 0
	
	SELECT @sNumberAPT = COALESCE(@sNumberAPT, '') + CASE WHEN ISNULL(NumberAPT,'') = '' THEN '' ELSE NumberAPT + ' / ' END
	FROM (
			SELECT CP1.NumberAPT
			FROM RESERVAS_DET_PAX RDP1 INNER JOIN COTIPAX CP1 ON RDP1.IDPax = CP1.IDPax AND CP1.IDCab = @IDCab
			WHERE IDReserva_Det = @IDReserva_Det -- AND CP1.FlTC = 0
			GROUP BY CP1.NumberAPT
		) A

	IF LEN(@sNumberAPT) > 3
	BEGIN
		SELECT @sNumberAPT = CASE WHEN SUBSTRING(@sNumberAPT,1,3) = ' / ' THEN '' + SUBSTRING(@sNumberAPT,4,LEN(@sNumberAPT)) ELSE @sNumberAPT END
		SELECT @sNumberAPT = CASE WHEN SUBSTRING(@sNumberAPT,LEN(@sNumberAPT)-1,3) = ' / ' THEN SUBSTRING(@sNumberAPT,1,LEN(@sNumberAPT)-2) ELSE @sNumberAPT END
	END
	RETURN @sNumberAPT 
END
