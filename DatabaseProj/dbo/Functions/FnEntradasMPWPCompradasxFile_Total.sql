﻿--JRF-20150715-Se cambio la función [FnServicioMPWP]
CREATE Function dbo.FnEntradasMPWPCompradasxFile_Total(@IDCab int,@vcMPWP varchar(10))
	returns bit
As
	Begin
	Declare @bComprados bit=0	
	Declare @IDServicioMPWP char(8)

	If @vcMPWP = 'MAPI'		
		Set @IDServicioMPWP='CUZ00132'		
	If @vcMPWP = 'WAYNA'		
		Set @IDServicioMPWP='CUZ00134'
	If @vcMPWP = 'RAQCHI'		
		Set @IDServicioMPWP='CUZ00145'
		
	Declare @Pax smallint=0, @EntCompradasMP smallint=0,@EntCompradasWP smallint=0,@EntCompradasRQ smallint=0

	Select @Pax = isnull(Sum(isnull(Pax,0)),0), @EntCompradasMP = isnull(Sum(isnull(EntCompradasMP,0)),0),
		@EntCompradasWP = isnull(Sum(isnull(EntCompradasWP,0)),0)
		,@EntCompradasRQ = isnull(Sum(isnull(EntCompradasRQ,0)),0)
	From
		(Select 
		(Select COUNT(*) From COTIDET_PAX Where IDDET=cd.IDDet  And FlDesactNoShow=0) as Pax, 
		(Select COUNT(*) From COTIDET_PAX Where IDDET=cd.IDDet  And FlEntMPGenerada=1 And FlDesactNoShow=0) as EntCompradasMP,
		(Select COUNT(*) From COTIDET_PAX Where IDDET=cd.IDDet  And FlEntWPGenerada=1 And FlDesactNoShow=0) as EntCompradasWP,		
		(Select COUNT(*) From COTIDET_PAX Where IDDET=cd.IDDet  And FlEntRQGenerada=1 And FlDesactNoShow=0) as EntCompradasRQ		
		From COTIDET cd Inner Join MASERVICIOS_DET sd1 On cd.IDServicio_Det=sd1.IDServicio_Det
		--Inner Join MASERVICIOS_DET sd2 On sd1.IDServicio_Det_V=sd2.IDServicio_Det
		--	And sd2.IDServicio IN(@IDServicioMPWP,'CUZ00133') 					
		Where cd.IDCAB=@IDCab 
			--And dbo.FnServicioMPWP(sd1.IDServicio_Det) in(@IDServicioMPWP,'CUZ00133')
			And (select Top (1) sd2.IDServicio from MASERVICIOS_DET sd2 
			 Where sd2.IDServicio_Det In (select IDServicio_Det_V from MASERVICIOS_DET sd3 where sd3.IDServicio=sd1.IDServicio And sd3.Anio=sd1.Anio And sd3.Tipo=sd1.Tipo
			 ) And sd2.IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00912','CUZ00946')) in(@IDServicioMPWP,'CUZ00133')
		) as X

	If @vcMPWP = 'MAPI'
		Begin
		If @Pax=@EntCompradasMP And @Pax>0
			Set @bComprados = 1
		End
	If @vcMPWP = 'WAYNA'
		Begin
		If @Pax=@EntCompradasWP And @Pax>0
			Set @bComprados = 1
		End
	If @vcMPWP = 'RAQCHI'
		Begin
		If @Pax=@EntCompradasRQ And @Pax>0
			Set @bComprados = 1
		End
	Return @bComprados
	End
