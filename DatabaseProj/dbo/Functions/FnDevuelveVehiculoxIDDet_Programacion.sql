﻿--create
create Function dbo.FnDevuelveVehiculoxIDDet_Programacion(@IDDet int,@IDServicio_Det int)
returns varchar(max)
As
Begin
	Declare @NombreVehiculo varchar(max)=''
	Declare @IDTipoOper char(1)=''

	select @IDTipoOper=p.IDTipoOper
	from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	where IDDET=@IDDet And IsNull(cd.IDDetRel,0)=0

	Declare @IDReserva_Det int = 0
	If @IDTipoOper='I'
	Begin
		Set @IDReserva_Det = (select distinct IDReserva_Det from RESERVAS_DET where IDDet=@IDDet And Anulado=0)
	End
	Else
	Begin 
		Set @IDReserva_Det = (select distinct IDReserva_Det from RESERVAS_DET where IDDet=@IDDet And IDServicio_Det=@IDReserva_Det And Anulado=0)
	End
	/*
	Select @NombreVehiculo = IsNull(vt.NoVehiculoCitado + ' ('+ Cast(vt.QtCapacidad as varchar(2))+')','')
	from RESERVAS_DET rd Left Join MAVEHICULOS_TRANSPORTE vt On rd.NuVehiculo=vt.NuVehiculo
	where IDReserva_Det=@IDReserva_Det
	*/
	declare @tablaveh as table
	(idvehiculo smallint)

	insert into @tablaveh
	select distinct isnull(od.IDVehiculo_Prg,0)
		from OPERACIONES_DET_DETSERVICIOS ods
		inner join operaciones_Det od on od.IDOperacion_Det=ods.IDOperacion_Det
		where od.IDReserva_Det=@IDReserva_Det
		and od.IDServicio_Det=@IDServicio_Det

Declare @Nombres varchar(Max)='',@Name varchar(Max)=''
	Declare curPax cursor for
	select
	IsNull(vt.NoVehiculoCitado + ' ('+ Cast(vt.QtCapacidad as varchar(2))+')','')
	from  MAVEHICULOS_TRANSPORTE vt
	inner join @tablaveh t on vt.NuVehiculo=t.idvehiculo
	

	Open curPax
	Fetch Next From curPax into @Name
	Begin
		While @@FETCH_STATUS=0
		Begin
			Set @NombreVehiculo = @NombreVehiculo + @Name + char(13)
			Fetch Next From curPax into @Name
		End
	End
	Close curPax
	DealLocate curPax
	--return @Nombres
	
	return isnull(@NombreVehiculo,'')
End

