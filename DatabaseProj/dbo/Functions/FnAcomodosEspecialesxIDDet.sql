﻿CREATE function dbo.FnAcomodosEspecialesxIDDet(@IDDet int)
returns varchar(max)
as
Begin
	Declare @TotalAcomodo varchar(max)='',@tiCont tinyint = 0, @TextoAcomodo varchar(max)=''
	Declare curAcomodo cursor for
	Select cast(ca.QtPax as varchar(3))+' ' + Case When p.IDTipoProv <> '001' And sd.ConAlojamiento=1 Then sd.Descripcion Else cd.Servicio End + ' '+ ch.NoCapacidad + ' + ' as Acomodo
	from COTIDET_ACOMODOESPECIAL ca Left Join CAPACIDAD_HABITAC ch On ca.CoCapacidad = ch.CoCapacidad
	Left Join COTIDET cd On ca.IDDet = cd.IDDet 
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
	where ca.IDDet = @IDDet
	Order By ca.CoCapacidad
	Open curAcomodo
	Fetch Next From curAcomodo into @TextoAcomodo
	While @@Fetch_status = 0
	Begin
		  If @tiCont=0          
		    Set @TotalAcomodo=@TextoAcomodo         
		  Else          
			Set @TotalAcomodo=@TotalAcomodo+ @TextoAcomodo
		            
		  Set @tiCont = @tiCont + 1          
		Fetch Next From curAcomodo into @TextoAcomodo	
	End
	Close curAcomodo
	DealLocate curAcomodo
	
	if @TotalAcomodo <> ''
		Set @TotalAcomodo = substring(@TotalAcomodo,1,len(@TotalAcomodo)-2)
	
	Set @TotalAcomodo = Isnull(@TotalAcomodo,'')
	return @TotalAcomodo
End
