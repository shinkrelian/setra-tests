﻿
CREATE FUNCTION [dbo].[FnExisteUsuarioProvClie](
												@IDContacto char(3),
												@IDTablaParent char(6),
												@TipoTablaParent char(1),
												@CodUsu varchar(20))
RETURNS bit
AS
BEGIN
	DECLARE @ReturnExists bit = 0
	IF LTRIM(RTRIM(ISNULL(@CodUsu,''))) = ''
		RETURN @ReturnExists
	IF @TipoTablaParent = 'P'
	BEGIN
		IF EXISTS(SELECT IDContacto, IDProveedor FROM MACONTACTOSPROVEEDOR WHERE [Usuario] = @CodUsu AND IDContacto+'|'+IDProveedor <> @IDContacto+'|'+@IDTablaParent)
			SET @ReturnExists = 1
		IF EXISTS(SELECT IDProveedor FROM MAPROVEEDORES WHERE [Usuario] = @CodUsu)
			SET @ReturnExists = 1
		IF EXISTS(SELECT IDContacto, IDCliente FROM MACONTACTOSCLIENTE WHERE [UsuarioLogeo] = @CodUsu)
			SET @ReturnExists = 1
	END
	ELSE IF @TipoTablaParent = 'C'
	BEGIN
		IF EXISTS(SELECT IDContacto, IDProveedor FROM MACONTACTOSPROVEEDOR WHERE [Usuario] = @CodUsu)
			SET @ReturnExists = 1
		IF EXISTS(SELECT IDProveedor FROM MAPROVEEDORES WHERE [Usuario] = @CodUsu)
			SET @ReturnExists = 1
		IF EXISTS(SELECT IDContacto, IDCliente FROM MACONTACTOSCLIENTE WHERE [UsuarioLogeo] = @CodUsu AND IDContacto+'|'+IDCliente <> @IDContacto+'|'+@IDTablaParent)
			SET @ReturnExists = 1
	END
	ELSE IF @TipoTablaParent = 'V'
	BEGIN
		IF EXISTS(SELECT IDContacto, IDProveedor FROM MACONTACTOSPROVEEDOR WHERE [Usuario] = @CodUsu)
			SET @ReturnExists = 1
		IF EXISTS(SELECT IDProveedor FROM MAPROVEEDORES WHERE [Usuario] = @CodUsu AND IDProveedor <> @IDTablaParent)
			SET @ReturnExists = 1
		IF EXISTS(SELECT IDContacto, IDCliente FROM MACONTACTOSCLIENTE WHERE [UsuarioLogeo] = @CodUsu)
			SET @ReturnExists = 1
	END
	RETURN @ReturnExists
END
