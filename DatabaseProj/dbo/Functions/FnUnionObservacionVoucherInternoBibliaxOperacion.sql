﻿Create Function dbo.FnUnionObservacionVoucherInternoBibliaxOperacion(@IDOperacion int,@IDVoucher int)
returns varchar(Max)
As
Begin
	Declare @ObservacionGeneral varchar(Max)='',@ObservBiblia varchar(Max)='',@ObservInterno varchar(Max)='',
			@ObservVoucher varchar(Max)=''
			
	Declare curtItem cursor for
	Select distinct Isnull(ObservBiblia,'') as Biblia,Isnull(ObservInterno,'') as Interno,Isnull(vo.ObservVoucher,'')
	from OPERACIONES_DET od Left Join VOUCHER_OPERACIONES vo On od.IDVoucher=vo.IDVoucher
	Where od.IDOperacion = @IDOperacion and (@IDVoucher= 0 Or od.IDVoucher = @IDVoucher)
	Open curtItem
	Fetch Next From curtItem into @ObservBiblia,@ObservInterno,@ObservVoucher
	While @@Fetch_status=0
		Begin
			Declare @UnionHorizontal varchar(Max) = ''--@ObservBiblia+char(13)+@ObservInterno+char(13)+@ObservVoucher+char(13)
			--print @ObservBiblia + ' >>c'
			if @ObservBiblia='' and @ObservInterno='' and @ObservVoucher=''
			  set @UnionHorizontal = ''
			else 
			Begin
			  set @UnionHorizontal = ltrim(rtrim(@ObservBiblia+' '+@ObservInterno+' '+@ObservVoucher))
			End  
			
			if @UnionHorizontal <> ''
			Begin
				Set @UnionHorizontal = @UnionHorizontal + char(13)
			End
			
			if @ObservacionGeneral='' and @UnionHorizontal<>''
				set @ObservacionGeneral = @UnionHorizontal
			else
			Begin 
				if @ObservacionGeneral <> ''
					set @ObservacionGeneral = @ObservacionGeneral + @UnionHorizontal
			End
			Fetch Next From curtItem into @ObservBiblia,@ObservInterno,@ObservVoucher
		End
	Close curtItem
	DealLocate curtItem
	Return @ObservacionGeneral
End
