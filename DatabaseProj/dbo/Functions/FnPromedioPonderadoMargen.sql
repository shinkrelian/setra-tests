﻿--HLF-20140422-If @Total > 0
CREATE Function [dbo].[FnPromedioPonderadoMargen]  
	(@IDCab int,   
	@IDProveedor char(6))  

	Returns numeric(10,2)  
As   
  
	Begin   
	Declare @ValorReturn numeric(10,4), @TotalxMargenAplicado numeric(10,6), @Total numeric(10,2)  

	Select @TotalxMargenAplicado=sum(Total*(MargenAplicado/100)),@Total=sum(Total)   
	From COTIDET Where IDCab=@IDCab And IDProveedor=@IDProveedor  

	If @Total > 0
	Set @ValorReturn=@TotalxMargenAplicado/@Total  
	Else
	Set @ValorReturn=0

	Return IsNull(@ValorReturn*100,0)  
	End  
	  