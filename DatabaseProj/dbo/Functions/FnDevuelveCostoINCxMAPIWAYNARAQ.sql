﻿--JRF-20150526-Considerar los comprados o pendientes
--JRF-20150527-Verificar si se considera el costos de los niños.
CREATE Function dbo.FnDevuelveCostoINCxMAPIWAYNARAQ(@IDDet int,@IDServicio char(8),@Anio char(4),@Comprado bit,@ConsiderarNinios bit)
returns numeric(12,4)
As
Begin
	Declare @CostoTotal numeric(12,4) = 0
	Declare @NroPaxAdulto tinyint = 0
	Declare @NroPaxAdultoCAN tinyint = 0
	Declare @NroPaxNinio tinyint = 0
	Declare @NroPaxNinioCAN tinyint = 0

	if @IDServicio = 'CUZ00133'
	Begin
		Set @NroPaxAdulto= (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0 
							And FlEntWPGenerada=@Comprado And Not cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxAdultoCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0 And
								FlEntWPGenerada=@Comprado And cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxNinio = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
							FlEntWPGenerada=@Comprado And Not cp.IDNacionalidad In ('000323','000018','000013','000007'))
		Set @NroPaxNinioCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
							   FlEntWPGenerada=@Comprado And cp.IDNacionalidad In ('000323','000018','000013','000007'))
	End
	Else
	Begin
		Set @NroPaxAdulto= (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0 
							And FlEntMPGenerada=@Comprado And Not cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxAdultoCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0 And
								FlEntMPGenerada=@Comprado And cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxNinio = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
							FlEntMPGenerada=@Comprado And Not cp.IDNacionalidad In ('000323','000018','000013','000007'))
		Set @NroPaxNinioCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
							   FlEntMPGenerada=@Comprado And cp.IDNacionalidad In ('000323','000018','000013','000007'))
	End

	if @ConsiderarNinios = 0
	Begin
		Set @CostoTotal = dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'AD' End) * @NroPaxAdulto
		Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CAN' End) * @NroPaxAdultoCAN)
	End
	else
	Begin
		Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CHD' End) * @NroPaxNinio)
		Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CHD-CAN' End) * @NroPaxNinioCAN)		
	End
	
	--Set @CostoTotal = dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'AD' End) * @NroPaxAdulto
	--Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CAN' End) * @NroPaxAdultoCAN)
	--Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CHD' End) * @NroPaxNinio)
	--Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CHD-CAN' End) * @NroPaxNinioCAN)
	return @CostoTotal
End
