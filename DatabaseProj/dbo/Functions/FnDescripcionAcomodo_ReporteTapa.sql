﻿--JRF-20150225-No Considerar los NoShow.
CREATE Function dbo.FnDescripcionAcomodo_ReporteTapa(@IDCab int,@Dia smalldatetime)    
returns varchar(Max)    
as    
Begin    
 Declare @DescAcomodo varchar(Max)=''    
 Declare @NombreCorto varchar(200),@Acomodo varchar(250),@CodReserva varchar(40),@Estado Char(2)    
 Declare @CodReservaTmp varchar(200) = ''    
 Declare @Count tinyint = (Select Count(*)    
 from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva    
 Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor    
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det    
 where r.Anulado = 0 and rd.Anulado = 0 and rd.FlServicioNoShow=0 and r.IDCab = @IDCab     
  and @Dia Between cast(convert(char(10),rd.Dia,103) as smalldatetime) and dateadd(d,-1,cast(convert(char(10),rd.FechaOut,103) as smalldatetime))--CONVERT(Char(10),rd.Dia,103) = Convert(Char(10),@Dia,103)     
  and ((p.IDTipoProv = '001' and sd.PlanAlimenticio = 0) Or (p.IDTipoProv = '003' and sd.ConAlojamiento = 1))    
  and r.Estado <> 'XL')    
 Declare @tiCont tinyint = 0    
     
 Declare curAcomodos cursor for    
 Select p.NombreCorto,ltrim(rtrim(dbo.FnAcomodosReservasxIDReserva(rd.IDReserva,rd.Dia))) As Acomodo,Isnull(r.CodReservaProv,'') as CodReservaProv,r.Estado    
 from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva    
 Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor    
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det    
 where r.Anulado = 0 and rd.Anulado = 0 and rd.FlServicioNoShow=0 and r.IDCab = @IDCab     
    and @Dia Between cast(convert(char(10),rd.Dia,103) as smalldatetime) and dateadd(d,-1,cast(convert(char(10),rd.FechaOut,103) as smalldatetime))--CONVERT(Char(10),rd.Dia,103) = Convert(Char(10),@Dia,103)     
    and ((p.IDTipoProv = '001' and sd.PlanAlimenticio = 0) Or (p.IDTipoProv = '003' and sd.ConAlojamiento = 1))    
    and r.Estado <> 'XL'    
 Order By Dia    
 Open curAcomodos     
 Fetch Next From curAcomodos into @NombreCorto,@Acomodo,@CodReserva,@Estado    
 While @@Fetch_Status = 0    
 Begin    
      
     If CharIndex(@NombreCorto,@DescAcomodo) = 0    
     Begin    
   Set @DescAcomodo = @DescAcomodo + @NombreCorto + Char(13)    
  End    
      
  if substring(@Acomodo,LEN(@Acomodo)-1,LEN(@Acomodo))=char(13)    
  begin    
   set @Acomodo = substring(@Acomodo,0,LEN(@Acomodo)-1)    
  End              
  if CharIndex(@Acomodo,@DescAcomodo) = 0    
  Begin    
   Set @DescAcomodo = @DescAcomodo + @Acomodo --+ char(13)     
  End    
      
  if ltrim(rtrim(@CodReserva))<> ''    
  Begin    
   If CharIndex(@CodReserva,@DescAcomodo) = 0    
   Begin    
    Set @DescAcomodo = @DescAcomodo + @CodReserva + char(13)     
   End    
  End    
      
  Set @tiCont = @tiCont + 1              
  if @tiCont = @Count     
  Begin    
   Set @DescAcomodo = @DescAcomodo + char(13) + @Estado    
  End    
  Fetch Next From curAcomodos into @NombreCorto,@Acomodo,@CodReserva,@Estado    
 End    
 Close curAcomodos    
 DealLocate curAcomodos    
     
 Return @DescAcomodo    
End    
