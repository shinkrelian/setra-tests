﻿CREATE Function [dbo].[FnSaldoFavorExportador]   
 (@IDCab int)  
  
	Returns numeric(12,2)  
	As   

	Begin   
	Declare @Igv numeric(5,2)=(Select NuIgv From PARAMETRO)/100
	Declare @ValorReturn numeric(12,2)=0  

	SELECT @ValorReturn=SUM(  
	CASE WHEN idTipoOC='001' THEN CD.CostoReal * @Igv
	ELSE  
		CASE WHEN idTipoOC='006' THEN (CD.CostoReal*@Igv)*0.5 
			ELSE 0 
	END    
	END * CD.NroPax) 
	FROM COTIDET CD INNER JOIN MASERVICIOS_DET SD ON CD.IDServicio_Det=SD.IDServicio_Det  
	INNER JOIN MASERVICIOS S ON S.IDServicio=SD.IDServicio
	INNER JOIN MAPROVEEDORES p ON S.IDProveedor=p.IDProveedor 
	
	WHERE CD.IDCab=@IDCab 

	Set @ValorReturn = ISNULL(@ValorReturn,0)  

    Declare @ValorSumSubServ as numeric(12,2)=0
    
	SELECT @ValorSumSubServ = SUM(  
	CASE WHEN X.idTipoOC='001' THEN X.CostoReal * @Igv
	ELSE  
		CASE WHEN X.idTipoOC='006' THEN (X.CostoReal*@Igv)*0.5 
			ELSE 0 
		END    
	END  * X.NroPax  )
	
	FROM
		(SELECT DISTINCT CDS.CostoReal, SD.idTipoOC, CD.NroPax 
		FROM COTIDET_DETSERVICIOS CDS 
		INNER JOIN COTIDET CD ON CD.IDDET=CDS.IDDET And cd.IDCAB=@IDCab
		INNER JOIN MASERVICIOS_DET SD ON CDS.IDServicio_Det=SD.IDServicio_Det 
		) AS X
	--Where CDS.IDDET In (Select IDDet From COTIDET Where IDCAB=@IDCab)
	
    Set @ValorSumSubServ = ISNULL(@ValorSumSubServ,0)  
    
	Return ISNULL(@ValorReturn+@ValorSumSubServ,0)  
	--Return ISNULL(@ValorSumSubServ,0)  
End  
  