﻿--JRF-20130403-Validar una ruta x servicios cuando no halla hoteles. y si hay entonces
--			   Tomar en cuenta la ciudad del Hotel.
--HLF-20150511-Agregando q se considere operadores con alojamiento (p.IDTipoProv='003' and sd.ConAlojamiento=1)
CREATE Function [dbo].[FnDestinosDia] (@IDCab int,@Dia smalldatetime)
Returns varchar(Max)
As 
Begin
	Declare @Destinos varchar(Max)='', @DestinoTmp varchar(60)='', @Destino varchar(60)
	 ,@bitEncontroHotel bit = 0
 
	 If Exists(select cd.IDDET
			   from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
			   Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det 
			   where IDCAB = @IDCab and 
			   ((p.IDTipoProv = '001' and sd.PlanAlimenticio = 0) or
			   (p.IDTipoProv='003' and sd.ConAlojamiento=1))
			   )
	 Begin
		set @bitEncontroHotel = 1
	 End
	if(@bitEncontroHotel=0)
	Begin
	
	Declare curDestinos Cursor For
	SELECT OrigenDestino FROM 
	(
	Select c.ITEM,			
	Case When c.IDubigeoOri<>'000001' Then		
		--uOri.Descripcion+' - '		
		isnull(uOri.Codigo+'-','')		
	Else
		--u.Descripcion+' - '		
		isnull(u.Codigo+'-','')
	End	as OrigenDestino,
		
	1 As Ord
	From COTIDET c Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo
	Left Join MAUBIGEO uOri On c.IDubigeoOri=uOri.IDubigeo	
	Inner Join MAPROVEEDORES p On p.IDProveedor=c.IDProveedor And p.IDTipoProv<>'001'
	Where IDCAB=@IDCab And (c.Dia=@Dia Or @Dia='01/01/1900')
	
	Union
	
	Select c.ITEM,		
	Case When c.IDubigeoOri<>'000001' Then		
		isnull(uDes.Codigo+'-','')
	Else		
		isnull(u.Codigo+'-','')
	End	as OrigenDestino,

	2 As Ord
	From COTIDET c Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo
	Left Join MAUBIGEO uDes On c.IDubigeoDes=uDes.IDubigeo	
	Inner Join MAPROVEEDORES p On p.IDProveedor=c.IDProveedor And p.IDTipoProv<>'001'
	Where IDCAB=@IDCab And (c.Dia=@Dia Or @Dia='01/01/1900')
	
	) AS X
	Order by ITEM,Ord



	Open curDestinos
	Fetch Next From curDestinos Into @Destino
	While @@Fetch_Status=0
		Begin
		If @DestinoTmp<>@Destino 
			Begin
			If charindex(@Destino,@Destinos)=0 
				Set @Destinos=@Destinos+@Destino
			If ltrim(rtrim(@Destino))<>'' Set @DestinoTmp=@Destino
			End
		
		Fetch Next From curDestinos Into @Destino
		End

	Close curDestinos
	Deallocate curDestinos
	
	End
	Else
	Begin
		Declare curDestinos Cursor For  
			 SELECT OrigenDestino FROM   
			 (  
			   Select c.ITEM,     
				 Case When c.IDubigeoOri<>'000001' Then    
				  isnull(uOri.Codigo+'-','')  
				    
				 Else  
				  isnull(u.Codigo+'-','')  
				    
				 End as OrigenDestino,  
				    
				 1 As Ord  
				 From COTIDET c Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo And u.IDubigeo<>'000001'  
				 Left Join MAUBIGEO uOri On c.IDubigeoOri=uOri.IDubigeo And uOri.IDubigeo<>'000001'   
				 Inner Join MAPROVEEDORES p On p.IDProveedor=c.IDProveedor --And p.IDTipoProv='001'  
				 Left Join MASERVICIOS_DET sd On c.IDServicio_Det = sd.IDServicio_Det 
				 Where IDCAB=@IDCab And (c.Dia=@Dia Or @Dia='01/01/1900')  
				  and ((p.IDTipoProv = '001' and sd.PlanAlimenticio = 0) or
					(p.IDTipoProv='003' and sd.ConAlojamiento=1))
			   
			 ) AS X  
			 Order by ITEM,Ord  

			 Open curDestinos  
			 Fetch Next From curDestinos Into @Destino  
			 While @@Fetch_Status=0  
			  Begin  
			  If @DestinoTmp<>@Destino   
			   Begin  
			   If charindex(@Destino,@Destinos)=0   
				Set @Destinos=@Destinos+@Destino  
			   If ltrim(rtrim(@Destino))<>'' Set @DestinoTmp=@Destino  
			   End  
			    
			  Fetch Next From curDestinos Into @Destino  
			  End  
			  
			 Close curDestinos  
			 Deallocate curDestinos 
	End
	
	If ltrim(rtrim(@Destinos))<>'' 
		Begin
		If Right(ltrim(rtrim(@Destinos)),1)='-' 
		Set  @Destinos=Left(@Destinos,Len(@Destinos)-1)
		End
	Return @Destinos 
End

