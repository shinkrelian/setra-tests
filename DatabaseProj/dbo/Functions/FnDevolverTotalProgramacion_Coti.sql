﻿
CREATE Function [dbo].[FnDevolverTotalProgramacion_Coti]   
(
@idcab int,
@IDMoneda char(3)
)

Returns numeric(20,6)  
As       
      
Begin 
declare                                    
 @IDOperacion_Det int,                              
 @SumaTotal numeric(20,6)
                                      
 --Declare @TipoCambio numeric(8,2)                                  
 Declare @TipoCambio numeric(8,2)                                  
    
 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)                                  
  
--------------------------------------------------------------
declare @tabla as table(
idoperacion_det int null,
TipoCambio numeric(20,6) null,
SimboloMoneda varchar(5) null,
TotalGen numeric(20,6) null,
TotalUSD numeric(20,6) null
)


 Declare curVentasReceptivoMensual Cursor For 
 
 select distinct sd.IDOperacion_Det from OPERACIONES_DET_DETSERVICIOS sd
inner join OPERACIONES_DET d on d.IDOperacion_Det=sd.IDOperacion_Det
and
IDOperacion in (select IDOperacion from OPERACIONES where IDCab=@idcab)
 
 Open curVentasReceptivoMensual      
 Fetch Next From curVentasReceptivoMensual Into @IDOperacion_Det    
 While @@FETCH_STATUS=0      
 Begin      

 Select @TipoCambio=SsTipCam From COTICAB_TIPOSCAMBIO Where                                   
 IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                    
 AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=@IDOperacion_Det)        
 
insert into @tabla
select IDOperacion_Det,
 
		TipoCambio,   
 SimboloMoneda,                                  
                   
TotalGen,
--TotalUSD=cast(dbo.FnCambioMoneda(TotalGen,idmoneda,@IDMoneda,TipoCambio) as numeric(20,6))
TotalUSD=dbo.FnCambioMoneda(case when isnull(TotalProgram,0)=0 then TotalGen else totalprogram end,idmoneda,@IDMoneda,TipoCambio)
 from(

SELECT 
 
 IDOperacion_Det,
 
		TipoCambio,   
 SimboloMoneda,                                  
 idmoneda,              
 isnull(Case when isnull(TotalCotizadoCal,0) = 0 then                          
  Cast(Case When Afecto=1 Then                       
  NetoGen+(NetoGen*(@NuIgv/100))                       
  Else                       
  NetoGen                                                
  End as numeric(20,6))                           
 else                          
 TotalCotizadoCal                          
 End,0) as TotalGen--,                                  
,TotalProgram                               
 FROM                                  
 (                           
 Select od.IDOperacion_Det,   od.IgvCotizado,   od.TotalCotizado as TotalCotizadoCal,                          
 isnull(pCot.NombreCorto,p1.NombreCorto) as DescProveedor_Cot,                                              
 isnull(sCot.Descripcion,d1.Descripcion) as DescServicio_Cot,                                              
                                             
 od.IDEstadoSolicitud,                                              
 od.Activo, s.IDServicio as IDServicio_Prg,                                              
 d.IDServicio,                                          
 d.IDServicio_Det,                                               
                  
 isnull(dCot.tipo,d1.Tipo) as Variante_Prg,              
               
 isnull(pPrg.NombreCorto,pPrgTrsl.Nombre) as DescProveedor_Prg,                                         
 d2.Anio as Anio_Prg,                                             
      
 isnull(pCot.IDTipoProv,'')         
       
 as IDTipoProv,        
         
 isnull(pPrg.IDProveedor,'') as IDProveedor_Prg,   
 Case When pPrg.IDProveedor = '000467' Or pPrg.IDProveedor='000527' Then ISNULL(pPrg.Email3,'')+Isnull('; '+ pPrg.Email4+';','') else       
 Case When ISNULL(pPrg.Email3,'')='' Then pPrg.Email4 Else pPrg.Email3 End  End      
       
 as CorreoReservasProveedor_Prg,                                            
 isnull(od.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,                                              
 isnull(od.IDServicio_Det_V_Prg,0) as IDServicio_Det_V_Prg,                                              
 od.Total As TotalCotizado,                                              
                                             
 od.IDMoneda As IDMoneda,                                            
 isnull(od.tipoCambio ,0) As TipoCambio,                                                                                      
                                             
 od.Total_PrgEditado ,                                            
 IsNull(od.Total_Prg,0.00) as TotalProgramado,                                              
 IsNull(od.ObservEdicion,'') as ObservEdicion,                                              
 0 as CostoRealEditado,                                             
 od.CostoReal,od.CostoLiberado,od.Margen,                                            
 0 as MargenAplicadoEditado,                                      od.MargenAplicado,od.MargenLiberado,                                                                                          
 od.CostoRealImpto                                              
 ,od.CostoLiberadoImpto,od.MargenImpto,od.MargenLiberadoImpto,                                                                                           
 od.TotImpto,'' as Upd,                                 
                                                              
 op.NroPax+isnull(op.NroLiberados,0) as PaxmasLiberados,                            
                                                        
 Case When isnull(NetoCotizado,0) = 0 then                                                          
    Case When dbo.FnNetoMaServicio_Det(od.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)) = 0 Or op.FlServicioTC=1 Then
    od.CostoReal
    Else
    Case When dCot.IDServicio_Det IS NULL Then                 
	 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(od.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),  
	  dCot.CoMoneda,@IDMoneda,@TipoCambio)                                                            
     --dCot.CoMoneda,od.IDMoneda,@TipoCambio) --@IDMoneda
    Else                
     dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                                                               
     dCot.CoMoneda,@IDMoneda,@TipoCambio)    --@IDMoneda                       
	 --dCot.CoMoneda,od.IDMoneda,@TipoCambio)    --@IDMoneda                       
    End 
    End
                   
  Else                       
 NetoCotizado                       
End 
 As NetoGen,                                    
                        
 ISNULL(dCot.CoMoneda,'') as IDMonedaGen,                          
 isnull(mon.Simbolo,'') as SimboloMoneda,                                              
 isnull(dCot.Afecto,d.Afecto) as Afecto,                                   
                                  
 Isnull(od.NetoProgram,0) as NetoProgram,                                    
 Isnull(od.IgvProgram,0) as IgvProgram,                                    
 Isnull(od.TotalProgram,0) as TotalProgram                              
                                        
 ,od.IDVehiculo_Prg                      
 ,od.IngManual          ,            
 od.FlMontosSincerados  ,          
 Case When od.CoEstadoSincerado is null then                    
 Case When isnull(pCot.IDTipoProv,'') NOT IN ('004','005','008') then 'SP' Else od.CoEstadoSincerado End End as CoEstadoSincerado,  
 od.FlServicioNoShow  
 From OPERACIONES_DET_DETSERVICIOS od Left Join MASERVICIOS_DET d                                                 
 On d.IDServicio_Det = od.IDServicio_Det                                                                                           
 Left Join MASERVICIOS_DET d2 On ISNULL(od.IDServicio_Det_V_Prg,0)=d2.IDServicio_Det                                                
 Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio                                                                                         
 Left Join MAPROVEEDORES pPrg On od.IDProveedor_Prg=pPrg.IDProveedor                                        
 Left Join MAUSUARIOS pPrgTrsl On od.IDProveedor_Prg=pPrgTrsl.IDUsuario                                                                     
 Left Join MASERVICIOS_DET dCot On ISNULL(od.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det                                                
 Left Join MASERVICIOS sCot  On sCot.IDServicio=dCot.IDServicio                                                
 Left Join MAPROVEEDORES pCot On sCot.IDProveedor=pCot.IDProveedor                                                
 Left Join MASERVICIOS_DET d1 On d1.IDServicio_Det=od.IDServicio_Det                                                
 Left Join MASERVICIOS s1  On d1.IDServicio=s1.IDServicio                                                
 Left Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor                                            
 Left Join OPERACIONES_DET op On od.IDOperacion_Det=op.IDOperacion_Det                                            
 Left Join RESERVAS_DET rd On op.IDReserva_Det=rd.IDReserva_Det                                        
 Left Join MAMONEDAS mon on od.IDMoneda=mon.IDMoneda         
 
                                  
 Where  od.IDOperacion_Det = @IDOperacion_Det   and od.Activo=1                                              
) AS X                         
) as Y

 Fetch Next From curVentasReceptivoMensual Into @IDOperacion_Det    
 End      
      
 Close curVentasReceptivoMensual      
 Deallocate curVentasReceptivoMensual  

--------------------------------------------------------------	

 select @SumaTotal= sum(TotalUSD) from @tabla --sum(totalusd) from @tabla
 return isnull(@SumaTotal,0)
end

