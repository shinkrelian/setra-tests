﻿
--HLF-20151022-CONVERT(SMALLDATETIME,CONVERT(VARCHAR,@Fecha,103)) between...
CREATE Function [dbo].[FnServicioHotelEsAlimenticioPuro] 
	(@IDReserva int, @Fecha smalldatetime)
Returns Bit
As 
Begin
	Declare @bEs bit=1

	
	If Exists(
		Select IDReserva, IDReserva_Det, Dia,FechaOut, Servicio From RESERVAS_DET rd 
		Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det And sd.PlanAlimenticio=0
		Where rd.IDReserva=@IDReserva AND 
			--@Fecha between Dia and FechaOut)
			--CONVERT(SMALLDATETIME,CONVERT(VARCHAR,@Fecha,103)) between CONVERT(SMALLDATETIME,CONVERT(VARCHAR,Dia,103)) and FechaOut)
			CONVERT(DATE,@Fecha) between CONVERT(DATE,Dia) and FechaOut)
		Set @bEs = 0
		
	Return @bEs 
End


