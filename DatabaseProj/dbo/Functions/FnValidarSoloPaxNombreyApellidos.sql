﻿Create Function dbo.FnValidarSoloPaxNombreyApellidos(@IDCab int)
returns bit
As
Begin
	Declare @PaxCompletos smallint =0
	Set @PaxCompletos = (
			  Select Count(*) from (
				Select * from (
					select dbo.FnExisteNumeroEn(Nombres+' '+Apellidos,(Select Max(Orden) from cotipax cp2 where cp2.IDCab=cp1.IDCab)) as DatosNombresInCompletos
					From COTIPAX cp1 where cp1.IDCab=@IDCab
					) As Y
				 Where Y.DatosNombresInCompletos = 0
				) As X
			  )
	return @PaxCompletos
End
