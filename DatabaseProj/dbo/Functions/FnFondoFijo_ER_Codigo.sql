﻿Create Function dbo.FnFondoFijo_ER_Codigo(@CoProveedor char(6),@NuFondoFijo int)
Returns varchar(30)
As
	Begin
	Declare @NuCodigo_ER varchar(30)=''
	--Declare @NuFondoFijo int=0

	If @CoProveedor='001554' --Set. Lima
		Begin
		--Set @NuFondoFijo=isnull((select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoTipoFondoFijo='ER' 
		--and NuCodigo_ER='ER40897783' ORDER BY CAST(COSAP AS INT) DESC),0)

		If @NuFondoFijo<>0
			select @NuCodigo_ER=isnull(NuCodigo_ER,'')+'-'+isnull(CoSap,'') from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo

		End
	If @CoProveedor='000544' --Set. Cusco
		Begin
		--Set @NuFondoFijo=isnull((select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoTipoFondoFijo='ER' 
		--and NuCodigo_ER='ER44587677' ORDER BY CAST(COSAP AS INT) DESC),0)

		If @NuFondoFijo<>0
		--select @pNuCodigo_ER=isnull(NuCodigo_ER,'')+'-'+isnull(CoSap,'') from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo
			select @NuCodigo_ER=isnull(NuCodigo_ER,'')+'-01-'+isnull(CoSap,'') from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo

		End
		Return @NuCodigo_ER
	End
