﻿--JRF-20150522-Considerar la funcion para los subservicios
--JRF-20150526-...Or (p.IDTipoProv='001' And sd.PlanAlimenticio=1)
CREATE Function dbo.FnDevuelvePendienteUpdateMReservas(@IDCab int)
returns bit
As
Begin
	Declare @bHotelPendienteM bit =0,@bServPendienteM bit=0,@bUpdateM bit=0
	Select @bHotelPendienteM=1 from (
	Select dbo.FnCambiosenVentasPostFileReservasSoloAlojamiento(rd.IDReserva) as LogRes
	from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
	Where ((p.IDTipoProv='001' And sd.PlanAlimenticio=0) Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1))
	And rd.Anulado=0 And r.Anulado=0 And r.Estado<>'XL' and r.IDCab=@IDCab And r.IDReservaProtecc is Null) as X
	Where X.LogRes=1
	
	Set @bUpdateM = @bHotelPendienteM
	if @bUpdateM = 0
	Begin
		Select @bServPendienteM=1 from (
		Select 
			Case When p.IDTipoOper='I' Or (p.IDTipoProv='001' And sd.PlanAlimenticio=1)
				Then dbo.FnCambiosenVentasPostFileReservasSoloServicios(rd.IDReserva) 
				Else dbo.FnCambiosEnVentasPostFileSoloSubServicios(rd.IDReserva) End
		AS LogRes
		from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
		Left Join RESERVAS r On rd.IDReserva=r.IDReserva
		Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		Where Not ((p.IDTipoProv='001' And sd.PlanAlimenticio=0) Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1))
		And rd.Anulado=0 And r.Anulado=0 And r.Estado<>'XL' and r.IDCab=@IDCab And p.IDTipoOper='E' 
		And r.IDReservaProtecc is Null) as X
		Where X.LogRes=1
		Set @bUpdateM = @bServPendienteM
	End
	
	Return @bUpdateM
End
