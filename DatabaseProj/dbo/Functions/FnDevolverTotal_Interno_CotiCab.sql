﻿--JHD-20160307- select @SumaTotal= IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0)
CREATE function [dbo].[FnDevolverTotal_Interno_CotiCab]
  (
@idcab int,
@CoUbigeo_Oficina char(6)
)
Returns numeric(20,2)  
as
begin
 declare @SumaTotal numeric(20,6)=0
 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)
 Declare @MargenInterno as numeric(6,2)=10

--SELECT @SumaTotal=IsNull(Sum(Total),0)
select @SumaTotal= IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0)
 FROM                    
  (                    
  SELECT                     
   ZZ.IDProveedor, ZZ.DescProveedor, ZZ.DescProveedorInternac,                     
   DescPaisProveedor,  
                     
   (SELECT Top 1--DISTINCT                      
      LTRIM(RTRIM(SUBSTRING(NoArchAdjunto,CHARINDEX('.',NoArchAdjunto,0)-13,13)))                      
       From .BDCORREOSETRA..ADJUNTOSCORREOFILE WHERE NuCorreo IN                      
      (                      
      SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE NuCorreo IN                      
       (                      
       SELECT NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE WHERE NuCorreo IN                  
        (                      
SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'                        
        AND (SELECT Top 1 ISNULL(p1.Email3,'')+','+ISNULL(p1.Email4,'')+','+                      
        ISNULL(p1.EmailRecepcion1,'')+','+ISNULL(p1.EmailRecepcion2,'')                      
        FROM MAPROVEEDORES p1                       
         WHERE p1.IDProveedor=ZZ.IDProveedor) LIKE '%'+NoCuentaPara+'%'                      
                               
        )                      
                              
       Union                      
       SELECT NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE WHERE NuCorreo IN                      
        (                           
        SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'  and                       
         NoCuentaPara In (select correo from MACORREOREVPROVEEDOR where IDProveedor=ZZ.IDProveedor)                      
        )                      
       )                      
      )                       
      ) AS NroOrdenServicio,                        
                     
   ZZ.CompraNeto, ZZ.IGVCosto,                     
   ZZ.IGV_SFE, ZZ.Moneda,                     
   ZZ.CompraNeto+ZZ.IGVCosto+ZZ.IGV_SFE AS Total,                    
   ZZ.idTipoOC,                 
   ZZ.DescOperacionContab,                 
   --'GASTOS ADMINISTRATIVOS' as DescOperacionContab,                    
   ZZ.Margen,                    
   ZZ.CostoBaseUSD,                     
   ROUND(ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD                    
   --,YY.FacturaExportacion,YY.BoletaNoExportacion                    
  FROM                    
   (                    
   SELECT                     
    YY.IDProveedor, YY.DescProveedor, YY.DescProveedorInternac,                     
    DescPaisProveedor,  
    YY.CompraNeto, YY.IGVCosto,                     
    YY.IGV_SFE, YY.CompraNeto+YY.IGVCosto+YY.IGV_SFE AS Total,                    
    YY.Moneda, YY.idTipoOC, YY.DescOperacionContab, YY.Margen,                    
    YY.CompraNeto+IGVCosto+IGV_SFE as CostoBaseUSD                     
    --YY.VentaUSD,                    
    --YY.FacturaExportacion,YY.BoletaNoExportacion                    
   FROM                    
    (                    
  SELECT XX.IDProveedor, DescProveedor, DescProveedorInternac,                 
    DescPaisProveedor,     
     CompraNeto, IGVCosto,                     
     Case When idTipoOC='001' Then                     
       CompraNeto*(@IGV/100)                     
      Else                      
       Case When idTipoOC='006' Then (CompraNeto*(@IGV/100))*0.5 Else 0 End                    
      End as IGV_SFE,                     
     Moneda, idTipoOC, DescOperacionContab, Margen       
     --CostoBaseUSD, VentaUSD,                   
     --Case When XX.idTipoOC='001' Then VentaUSD Else 0 End As FacturaExportacion,                    
     --Case When XX.idTipoOC='001' Then 0 Else VentaUSD End As BoletaNoExportacion                    
    FROM                    
     (                      
     SELECT IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,  
                         
      CompraNeto, IGVCosto,Moneda,                     
      --Total,                     
      idTipoOC, DescOperacionContab, Margen                   
      --CostoBaseUSD,                    
      --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD                    
     FROM                    
      (                    
      SELECT IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,  
      SUM(CompraNeto) AS CompraNeto,                    
      SUM(IGVCosto) AS IGVCosto, Moneda,                     
      --SUM(Total) AS Total,                    
      idTipoOC, DescOperacionContab,                    
   AVG(Margen) AS Margen                    
      --SUM(Total) as CostoBaseUSD                    
      FROM                    
       (                    
                       
       SELECT X.*,  
                  
        --dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) AS Margen                
        Case When ltrim(rtrim(@CoUbigeo_Oficina))='' Then
			dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) 
        Else
			@MargenInterno
        End as Margen
        FROM                    
        (                    
      SELECT           
      isnull(P.IDProveedor,usrTrsl.IDUsuario) as IDProveedor,           
      pint.NombreCorto as DescProveedorInternac,                    
        isnull(P.NombreCorto,usrTrsl.Nombre) as DescProveedor,        
        paPI.Descripcion as DescPaisProveedor,                 
        ODS.IDOperacion_Det,                    
                
        
 Case When ODS.IngManual=0 Then               
    Case When isnull(NetoCotizado,0) = 0 then                              
           
  --  dbo.FnCambioMoneda(
		--dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,            
  --  OD.NroPax+isnull(od.NroLiberados,0)),                               
  --  SD.CoMoneda,ods.IDMoneda,isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
  --And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))

			dbo.FnCambioMoneda(		  
				--dbo.FnNetoMaServicio_Det(
				--	SD.IDServicio_Det,
				--	OD.NroPax+isnull(od.NroLiberados,0)), 
						IsNull((Select 
								Case When sd6.Monto_sgl is null then 
									(select Monto from MASERVICIOS_DET_COSTOS
									 where IDServicio_Det=sd6.IDServicio_Det And
									 (OD.NroPax+isnull(od.NroLiberados,0) between PaxDesde and PaxHasta)) 
								else
								IsNull(Monto_sgls,Monto_sgl) * (OD.NroPax+isnull(od.NroLiberados,0)) End
						 from MASERVICIOS_DET sd6 
						 where sd6.IDServicio_Det=sd.IDServicio_Det),0),
			SD.CoMoneda,
			'USD',
			isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where     
					IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))
    else                   
   ods.NetoCotizado        
    End               
 Else        
  ODS.NetoProgram        
 End        
        
 As CompraNeto,                                
        
                            
        --ODS.TotImpto as IGVCosto,                    
        Case When ODS.TotImpto>0 Then                             
    isnull(ODS.NetoProgram,ODS.CostoReal)*(@IGV/100)                    
  Else                
   0                
  End-- * (Case When sd.idTipoOC='005' Then 1 Else OD.NroPax End)                
  As IGVCosto,                
        ODS.IDMoneda as Moneda,                
                        
        --sd.idTipoOC,                    
        '005' As idTipoOC,          
     --tox.Descripcion as DescOperacionContab,                    
        'GASTOS ADMINISTRATIVOS' as DescOperacionContab,                    
        --SCot.IDProveedor as IDProveedorCot,                    
        --PCot.NombreCorto as DescProveedorCot,                    
        --SDCot.IDServicio_Det                    
        ODS.IDServicio_Det,                    
        O.IDProveedor as IDProveedorOpe                    
        ,SD.IDServicio                    
        FROM OPERACIONES_DET_DETSERVICIOS ODS                        
        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det                     
        AND OD.IDServicio=ODS.IDServicio                    
        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=@IDCab               
        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det                    
                            
        LEFT JOIN COTICAB CC ON CC.IDCAB=O.IDCab      
        LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor                                        
                                 
         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Prg=SD.IDServicio_Det                    
         LEFT join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc                    
                           
        LEFT JOIN MASERVICIOS SCot ON SD.IDServicio=SCot.IDServicio                    
        LEFT JOIN MAPROVEEDORES PCot ON SCot.IDProveedor=PCot.IDProveedor                     
           
 Left Join MAUBIGEO ubPI On P.IDCiudad=ubPI.IDubigeo  
    Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo           
           
                    
        Left Join MAPROVEEDORES pint On pint.IDProveedor=pCOT.IDProveedorInternacional                    
        Left Join MAUSUARIOS usrTrsl On ODS.IDProveedor_Prg=usrTrsl.IDUsuario             
        Where --not OD.IDTrasladista_Prg is null Or not OD.IDVehiculo_Prg is null                     
         (isnull(p.IDUsuarioTrasladista,'0000')<>'0000' --AND tox.IdToc='005'                  
         or isnull(usrTrsl.IDUsuario,'0000')<>'0000' --AND tox.IdToc='005'                  
         OR  (ODS.IDProveedor_Prg = '001935') --SETOURS CUSCO                      
         And ODS.Activo=1)      
         And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
		 and cc.estado<>'X'
        ) AS X                    
        where CompraNeto>0      
       ) AS Y                    
      GROUP BY IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,Moneda,idTipoOC,DescOperacionContab                    
     ) as Z                    
     ) AS XX                    
    ) AS YY                    
   ) AS ZZ                      
  ) AS A

  return @SumaTotal
End
