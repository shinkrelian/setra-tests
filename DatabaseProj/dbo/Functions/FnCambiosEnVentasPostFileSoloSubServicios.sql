﻿--sp_Helptext [FnCambiosEnVentasPostFileSoloSubServicios]
--Declare @IDReserva int = 45265
--JRF-20150520-Considerar solo cambios desde Ventas
--JRF-20150526-Considerar Cotidet x IDServicio_Det para Editados. 
--(IsNull((Select cd3.CostoRealEditado from COTIDET cd3 where cd3.IDDET=cds.IDDET and cd3.IDServicio_Det=...)
--JRF-20150528-No Considerar los Items Generados por Acomodo de Transporte (Implementar Diferente evaluación)
--JRF-20150619-el sub-servicio debe existir en COTIDET >> and @RowCountCotiDet > 0
--JRF-20150623-..cdsCot.ConAlojamiento .. Count(*) para COTIDET
--JRF-20150908-Verificar existencia de todos los servicios de ventas relacionados.
--JRF-20150909-Solo comparar los servicios con estado 'A'
--JRF-20151006-Validar la eliminación completa
--JRF-20160122-Left Join RESERVAS_DET rd On cds.IDDet= rd.IDDet and rd.IDReserva=@IDReserva --and cds.IDServicio_Det=rd.IDServicio_Det
--JRF-20160413-..Select @bCambiosVentas = 1 from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDET And rd.IDServicio_Det=cd.IDServicio_Det..
CREATE Function [dbo].[FnCambiosEnVentasPostFileSoloSubServicios](@IDReserva int)
returns bit
Begin
 Declare @bCambiosVentas bit = 0
 Declare @IDProveedor char(6)='',@IDCab int=0,@RowCountCotiDet int = 0,@IDTipoProv char(3)='',
 @ExstConAlojamiento bit=0,@ExstSinAlojamiento bit=0

 Select @IDProveedor=r.IDProveedor,@IDCab=r.IDCab,@IDTipoProv=p.IDTipoProv
 from RESERVAS r   Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor       
 Where r.IDReserva =@IDReserva and r.Anulado = 0 and r.Estado <> 'XL'  
           
 Declare @IDDet int=0,@Dia smalldatetime='01/01/2000',@Servicio varchar(Max)='',@NroPax int=0,@CantPagar int=0,@IDServicio_Det int=0,@IDIdioma varchar(12)='',            
 @FecMod datetime='01/01/1900',@IDReserva_Det int=0,@IDReservaTmp int=0,@RowCountCotiDetTemp int = 0,@bTransfer bit = 0,@CostoReal numeric(12,4)=0,
 @CantResDel tinyint=0

  If Exists(Select rd.IDReserva_Det from RESERVAS_DET rd 
			Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det --And sd.ConAlojamiento=1
			Where rd.Anulado=0 And sd.ConAlojamiento=1 And @IDTipoProv='003' and sd.Estado='A' and rd.IDReserva=@IDReserva)
	Set @ExstConAlojamiento = 1

  If Exists(Select rd.IDReserva_Det from RESERVAS_DET rd 
			Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det --And sd.ConAlojamiento=1
			Where rd.Anulado=0 And sd.ConAlojamiento=0 And @IDTipoProv='003' and sd.Estado='A' and rd.IDReserva=@IDReserva)
	Set @ExstSinAlojamiento = 1

  Declare @RowCountReservaDet int = (select count(*) from (            
            select Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
            then 1 else 0 End As ServicioHotespedaje from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det            
            Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
            Left Join cotidet cd On rd.IDDet = cd.IDDet          
            Where rd.IDReserva=@IDReserva and sd.Estado='A' and rd.Anulado=0 and rd.FlServicioIngManual=0 and r.Estado<> 'XL' and IsNull(cd.IDDetRel,0)=0 and IsNull (rd.IDReserva_DetOrigAcomVehi,0)=0) as X            
            Where X.ServicioHotespedaje=0) 

  --Sumar los servicios que serán sub-dividos
  Set @RowCountReservaDet = @RowCountReservaDet + (Select Count(distinct x.IDReserva_DetOrigAcomVehi) from (
            select Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
            then 1 else 0 End As ServicioHotespedaje,rd.IDReserva_DetOrigAcomVehi from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
            Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
            Left Join cotidet cd On rd.IDDet = cd.IDDet          
            Where rd.IDReserva=@IDReserva and rd.Anulado=0 and rd.FlServicioIngManual=0 and r.Estado<> 'XL' and IsNull(cd.IDDetRel,0)=0  and sd.Estado='A'
			and IsNull(rd.IDReserva_DetOrigAcomVehi,0)<>0) As X
			Where X.ServicioHotespedaje=0)

 Declare @bExisteProveedor bit = 0            
 If Exists(select IDDET from COTIDET where IDProveedor=@IDProveedor and IDCAB=@IDCab)           
  Set @bExisteProveedor = 1  

 --select @IDProveedor,@IDCab,@bExisteProveedor
  if @IDProveedor <> '' and @IDCab <> 0 and @bExisteProveedor=1
 Begin

       Set @RowCountCotiDet = (Select count(*) from (            
		Select cd.IDDet,cd.Dia,cd.Servicio,cd.NroPax,cd.IDServicio_Det,cd.IDIdioma,cd.FecMod,--rd.IDReserva_Det,            
            r.IDReserva,Case When (p.IDTipoProv = '001' and cdsCot.PlanAlimenticio=0) Or (p.IDTipoProv='003' and cdsCot.ConAlojamiento=1)             
            then 1 else 0 End As ServicioHotespedaje             
        from COTIDET_DETSERVICIOS cds Left Join cotidet cd On cds.IDDET=cd.IDDET
		Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
        Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det                
        Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'         
        --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva            
		Left Join MASERVICIOS_DET cdsCot On cds.IDServicio_Det=cdsCot.IDServicio_Det                
		Left Join MASERVICIOS_DET sdCot On cdsCot.IDServicio_Det_V=sdCot.IDServicio_Det
		Left Join MASERVICIOS sCot On sdCot.IDServicio=scot.IDServicio
        Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and IsNull(r.Anulado,0) = 0  and IsNull(cd.IDDetRel,0)=0 
		And IsNull(SCOT.IDProveedor,'') <> '001836' and cdsCot.Estado='A'
        ) as X            
        where X.ServicioHotespedaje = 0)

  Select @bCambiosVentas = 1
  from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDET And rd.IDServicio_Det=cd.IDServicio_Det
  Inner Join RESERVAS r On rd.IDReserva=r.IDReserva
  Where rd.IDReserva=@IDReserva And r.Anulado=0 And rd.Anulado=0 And rd.Dia <> cd.Dia And rd.FeUpdateRow is not null

  if @bCambiosVentas=0
  Begin

  Declare curItemsExist cursor for
  Select distinct        
   X.IDDET,X.Dia,X.Servicio,X.NroPax,x.CantidadAPagar,X.IDServicio_Det,X.IDidioma,X.FecMod,X.IDReserva_Det,X.IDReserva,            
      0 As CantidadCotiDet,X.Transfer, X.CostoReal      
  from (            
  Select cds.IDDet,cd.Dia,cd.Servicio,Case When cd.IncGuia = 1 then 1 Else cd.NroPax+IsNull(cd.NroLiberados,0) End as NroPax,rd.CantidadAPagar,cds.IDServicio_Det,      
      Case When p.IDProveedor In('000545','001836') and Exists(select ID from cotivuelos cv Where cv.idcab=cd.IDCab and cv.IDDet=cd.IDDet)      
      Then 'NO APLICA' Else cd.IDIdioma End as IDIdioma,sd.FecMod,      
   rd.IDReserva_Det,r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
      then 1 else 0 End As ServicioHotespedaje,cd.Transfer,     
   Case When IsNull((Select cd3.CostoRealEditado from COTIDET cd3 where cd3.IDDET=cds.IDDET and cd3.IDServicio_Det=cds.IDServicio_Det),0)=1 Then cds.CostoReal * IsNull(sd.TC,1) Else      
     IsNull(sd.Monto_sgls,
	 dbo.FnDevuelveCostoRealxPaxDetServicio2(cds.IDServicio_Det,(cd.NroPax+IsNull(cd.NroLiberados,0)))* IsNull(sd.TC,1)) End --* IsNull(sd.TC,1)
   as CostoReal   
  from COTIDET_DETSERVICIOS cds Left Join COTIDET cd On cds.IDDET=cd.IDDET
  Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
  Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det                
  Left Join RESERVAS_DET rd On cds.IDDet= rd.IDDet and rd.IDReserva=@IDReserva --and cds.IDServicio_Det=rd.IDServicio_Det
  Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'      
  Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and rd.Anulado =0  and r.Anulado=0 
  and IsNull(rd.FlServicioIngManual,0)=0  And IsNull(rd.IDReserva_DetOrigAcomVehi,0)=0
  ) as X            
  where X.ServicioHotespedaje = 0
 
  Open curItemsExist            
  Fetch Next From curItemsExist into @IDDet,@Dia,@Servicio,@NroPax,@CantPagar,@IDServicio_Det,@IDIdioma,@FecMod,@IDReserva_Det,@IDReservaTmp,@RowCountCotiDetTemp,@bTransfer,@CostoReal            
  While @@Fetch_Status=0            
  Begin            
   If @bCambiosVentas= 0            
   Begin            
    if Isnull(@IDReserva_Det,0)=0            
    Begin            
     Set @bCambiosVentas = 1             
    End            
    else            
    Begin            
    Declare @intDateDiff integer = (Select Case When FeUpdateRow is null then 0 else datediff(s,@FecMod,FeUpdateRow) End from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det)
	--Select @intDateDiff,'Test'
     if (@intDateDiff <> 0) 
     Begin      
	  Declare @IncGuia bit = IsNull((select IncGuia from COTIDET where IDDET=@IDDet And IDServicio_Det=@IDServicio_Det),0)

      If Not Exists(Select IDReserva_Det from RESERVAS_DET Where IDReserva=@IDReserva and anulado=0 and IDDet= @IDDet and IDServicio_Det=@IDServicio_Det)      
       Set @bCambiosVentas = 1            
      if @NroPax <> (Select (NroPax+ISNULL(NroLiberados,0)) from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0 and IDServicio_Det=@IDServicio_Det) 
       Set @bCambiosVentas = 1               
      if ltrim(rtrim(@IDIdioma))<>(Select ltrim(rtrim(IDIdioma)) from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0 and IDServicio_Det=@IDServicio_Det)
	   Set @bCambiosVentas = 1            
      if @IDServicio_Det <> (Select IDServicio_Det from RESERVAS_DET where IDReserva_Det=@IDReserva_Det)
      Set @bCambiosVentas = 1            
      if((Cast(@CostoReal as Numeric(12,2))<>(Select  Cast(NetoHab as  Numeric(12,2)) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0)))
		Set @bCambiosVentas = 1       
      if (Convert(char(10),@Dia,103)<>(Select  Convert(char(10),dia,103) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0))    
       Set @bCambiosVentas = 1         
      if @IDProveedor <> '001836'
	  Begin
		if (@CantPagar <> dbo.FnDevuelveCantidadPorPagar(@IDServicio_Det,@IDDet)) And @IncGuia = 0
		   Set @bCambiosVentas = 1  
	  End
	  if @IncGuia=1
	  Begin
		if @CantPagar <> 1 
			Set @bCambiosVentas = 1  
	  End       
     End

	 --if @bCambiosVentas = 1
		--Select Cast(@CostoReal as Numeric(12,2)),(Select  Cast(NetoHab as  Numeric(12,2)) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0)
	    --Select Cast(@CostoReal as Numeric(12,2)),(Select  Cast(NetoHab as  Numeric(12,2)) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0)
		--Select @Servicio,Cast(@CostoReal as Numeric(12,2)),(Select  Cast(NetoHab as  Numeric(12,2)) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0)
	End              
   End            
   Fetch Next From curItemsExist into @IDDet,@Dia,@Servicio,@NroPax,@CantPagar,@IDServicio_Det,@IDIdioma,@FecMod,@IDReserva_Det,@IDReservaTmp,@RowCountCotiDetTemp,@bTransfer,@CostoReal            
  End            
  Close curItemsExist            
  DealLocate curItemsExist 
 End

 if exists(select cd.IDDET from RESERVAS_DET rd Left Join COTIDET cd On rd.IDDet=cd.IDDET
		   where IDReserva=@IDReserva and Anulado=0 and cd.IDDET is null and rd.FlServicioIngManual=0)
	Set @bCambiosVentas = 1

 --select * from RESERVAS_DET where IDReserva_Det=178520 

  --Select @bCambiosVentas
 Declare @ExisteAcomodoVehi bit = 0
 Declare @IDDet2 int=0,@CantidadGenerada tinyint=0

 select @ExisteAcomodoVehi = 1
 from ACOMODO_VEHICULO_EXT ave Inner Join COTIDET c On ave.IDDet=c.IDDET
 where c.IDCAB=@IDCab And c.IDProveedor=@IDProveedor

	--Select @ExisteAcomodoVehi as Vehiculo
	 if @ExisteAcomodoVehi = 1
	 Begin
		--Select IDReserva_Det from RESERVAS_DET rd
		--			   Where rd.IDReserva=@IDReserva And rd.Anulado=0 And IDReserva_DetOrigAcomVehi is not null
		--Evaluación de Transportes
		 If Not Exists(Select IDReserva_Det from RESERVAS_DET rd
					   Where rd.IDReserva=@IDReserva And rd.Anulado=0 And IDReserva_DetOrigAcomVehi is not null)
		 Begin
			Set @bCambiosVentas = 1
		 End

		  --Evaluación por el total de los acomodos (Primer Cursor)
		 Declare CurTransp cursor for
		 Select IDDet,Sum(Cantidad) as CantidadGenerada
		 from RESERVAS_DET rd 
		 where rd.IDReserva=@IDReserva and rd.Anulado=0 And rd.IDReserva_DetOrigAcomVehi is not null --and FeUpdateRow is not null
		 group By IDDet
		 Open CurTransp
		 Fetch Next From CurTransp into @IDDet2,@CantidadGenerada
		 Begin
			While @@FETCH_STATUS=0
			Begin
				if @bCambiosVentas = 0
				Begin
					--Select QtPax as Texte from ACOMODO_VEHICULO_EXT where IDDet=@IDDet2	
					if (@CantidadGenerada <> (Select QtPax from ACOMODO_VEHICULO_EXT where IDDet=@IDDet2) And 
						((Select QtPax from ACOMODO_VEHICULO_EXT where IDDet=@IDDet2) -@CantidadGenerada) > 1)
					Begin
						Set @bCambiosVentas = 1
					End
					Else
					Begin
						Declare @CantPart int =(Select QtGrupo from ACOMODO_VEHICULO_EXT where IDDet=@IDDet2)
						Declare @CantResPart int = (Select count(*) from RESERVAS_DET where IDDet=@IDDet2 And Anulado=0)

						if @CantPart <> @CantResPart
							Set @bCambiosVentas = 1
					End
				End
				Fetch Next From CurTransp into @IDDet2,@CantidadGenerada
			End
		 End
		 Close CurTransp
		 DealLocate CurTransp


	 End
	 Else
	 Begin
	 
	 --Select IDReserva_Det from RESERVAS_DET rd
	 --Where rd.IDReserva=@IDReserva And rd.Anulado=0 And IDReserva_DetOrigAcomVehi is not null

	 	If Exists(Select IDReserva_Det from RESERVAS_DET rd
					   Where rd.IDReserva=@IDReserva And rd.Anulado=0 And IDReserva_DetOrigAcomVehi is not null)
			Set @bCambiosVentas = 1
	 End
 End
 else
 Begin
	if @IDProveedor <> '' and @IDCab <> 0 and @bExisteProveedor=0
	Begin
		Set @bCambiosVentas=1
	End
 End


 --SELECT @ExstSinAlojamiento ,@ExstConAlojamiento
 --Select @RowCountCotiDet,@RowCountReservaDet,@bCambiosVentas
 if (@RowCountCotiDet <> @RowCountReservaDet) and @bCambiosVentas= 0 and @RowCountCotiDet > 0 
	And Not(@IDTipoProv='003' And (@ExstSinAlojamiento=1 And @ExstConAlojamiento=1))
 Begin            
  Set @bCambiosVentas=1            
 End  

 --Select @bCambiosVentas
 return @bCambiosVentas
End
