﻿Create Function [dbo].[FnTipoDeCambioxMonedaFile](@CoMoneda char(3),
	@IDCab int)  
 returns numeric(8,3)  
as  
 Begin  
 Declare @TipoCambio numeric(8,3)=0  
                    
 Select @TipoCambio=SsTipCam From COTICAB_TIPOSCAMBIO   
 Where IDCab=@IDCab And CoMoneda=@CoMoneda
   
 return @TipoCambio  
 End
