﻿Create Function dbo.FnTipoDeCambioxIDOperacionDet(@IDOperacion_Det int)
returns numeric(8,2)
as
Begin
 Declare @TipoCambio numeric(8,2)                    
 Select @TipoCambio=TipoCambio From COTICAB Where                     
 IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                    
 set @TipoCambio = isnull(@TipoCambio,0)
 return @TipoCambio
End
