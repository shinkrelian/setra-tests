﻿--create
Create Function dbo.FnDevuelveGuias_ReservasxIDDet(@IDCab int,@IDDet int,@IDServicio_Det int)
returns varchar(Max)
as
begin
	Declare @Nombres varchar(Max)='',@Name varchar(Max)=''
	Declare curPax cursor for
	
	select distinct p.NombreCorto from RESERVAS_DET rd
			inner join reservas r on rd.IDReserva=r.IDReserva
			left join MAPROVEEDORES p on p.IDProveedor=rd.IDGuiaProveedor
			where r.idcab=@IDCab
			and rd.IDServicio_Det=@IDServicio_Det
			and rd.iddet=@IDDet and
			p.IDTipoProv='005'

	Open curPax
	Fetch Next From curPax into @Name
	Begin
		While @@FETCH_STATUS=0
		Begin
			Set @Nombres = @Nombres + @Name + char(13)
			Fetch Next From curPax into @Name
		End
	End
	Close curPax
	DealLocate curPax
	return @Nombres

end
