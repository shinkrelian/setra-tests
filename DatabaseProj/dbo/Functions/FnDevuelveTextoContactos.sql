﻿CREATE Function dbo.FnDevuelveTextoContactos(@UsuarioLogeo_Hash varchar(Max),@Destinatario varchar(Max),@TipoMensaje char(1))
returns varchar(Max)
As
Begin
Declare @Mensaje varchar(Max)=''

set @Mensaje = '<!DOCTYPE html>'
set @Mensaje = @Mensaje +'<html>'
set @Mensaje = @Mensaje + '<head>'
set @Mensaje = @Mensaje + '<meta charset="utf-8" />'
set @Mensaje = @Mensaje + '<title>Notificacion SETRA</title>'
set @Mensaje = @Mensaje + '<style>'
set @Mensaje = @Mensaje + 'p, a {'
set @Mensaje = @Mensaje + 'font-family: "Arial Narrow", sans-serif;'
set @Mensaje = @Mensaje + 'font-size:13px;'
set @Mensaje = @Mensaje + '}'
set @Mensaje = @Mensaje + '</style>'
set @Mensaje = @Mensaje + '</head>'
set @Mensaje = @Mensaje + '<body>'
set @Mensaje = @Mensaje + '<p>Estimado Proveedor:</p>'

if @TipoMensaje = 'G'
Begin
set @Mensaje = @Mensaje + '<p>Se comunica que a partir de la fecha, usted podrá consultar el “estado de pago” de sus comprobantes; ingresando a nuestra EXTRANET.</p>'
set @Mensaje = @Mensaje + '<p><strong>Usuario:</strong> '+@Destinatario+'</p>'
set @Mensaje = @Mensaje + '<p>Para el acceso, deberá cambiar su contraseña haciendo click en el siguiente enlace: </p>​'
set @Mensaje = @Mensaje + '<p><a href="http://extranet.setours.com/reset-password/'+@UsuarioLogeo_Hash+'">http://extranet.setours.com/reset-password/'+@UsuarioLogeo_Hash+'</a></p>'
set @Mensaje = @Mensaje + '<p>(Si usted no puede dar click en el enlace, c&oacute;pielo y péguelo en el navegador)​</p>'
set @Mensaje = @Mensaje + '<p>Al iniciar la sesi&oacute;n, se mostrará una guía de ayuda para efectuar la consulta.</p>'
End

set @Mensaje = @Mensaje + '<br>'
set @Mensaje = @Mensaje + '<p>Atentamente,</p>'
set @Mensaje = @Mensaje + '<p><strong>SETOURS S.A.</strong><br></p>'​
set @Mensaje = @Mensaje + '<p>'
set @Mensaje = @Mensaje + 'Cualquier consulta adicional que tengan, pueden escribir a: <br/>' 
set @Mensaje = @Mensaje + '<p><strong>Perú:</strong> maria.mamani@setours.com <br/> <strong>Internacional:</strong> keyla.medina@setours.com<br/><p>'
set @Mensaje = @Mensaje + 'Por favor no responder a este mensaje, esta cuenta sirve solo para envíos de informaci&oacute;n, no recibirá respuesta.'
set @Mensaje = @Mensaje + '</p>'
set @Mensaje = @Mensaje + '</body>'
set @Mensaje = @Mensaje + '</html>'

return @Mensaje
End
