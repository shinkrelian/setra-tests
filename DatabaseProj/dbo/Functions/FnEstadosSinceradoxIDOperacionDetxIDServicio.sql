﻿Create function dbo.FnEstadosSinceradoxIDOperacionDetxIDServicio(@IDOperacion_Det int,@IDServicio char(8))
returns varchar(50)
as
Begin
	Declare @strEstado varchar(50)='' 
	Declare @CantSA tinyint = (select count(*)
			 from OPERACIONES_DET_DETSERVICIOS odd     
			 Left Join MASERVICIOS_DET sd On odd.IDServicio_Det_V_Cot =sd.IDServicio_Det    
			 Left Join MASERVICIOS s On s.IDServicio = sd.IDServicio    
			 Left Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor    
	where IDOperacion_Det = @IDOperacion_Det and odd.IDServicio = @IDServicio
			 and odd.Activo = 1 and p.IDTipoProv = '006' and odd.CoEstadoSincerado='SA') 
	Declare @CantTotal tinyint = (select count(*)
			 from OPERACIONES_DET_DETSERVICIOS odd     
			 Left Join MASERVICIOS_DET sd On odd.IDServicio_Det_V_Cot =sd.IDServicio_Det    
			 Left Join MASERVICIOS s On s.IDServicio = sd.IDServicio    
			 Left Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor    
	where IDOperacion_Det = @IDOperacion_Det and odd.IDServicio = @IDServicio
			 and odd.Activo = 1 and p.IDTipoProv = '006')
	
	if @CantSA <> @CantTotal
		set @strEstado = 'PS'
	else
		set @strEstado = 'SA'
	
	return @strEstado
End
