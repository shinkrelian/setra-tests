﻿Create Function FnDiaSemana (@Dia char(1))
	Returns tinyint As
	Begin
	    Declare @NumDiaSem tinyint
		if(Upper(@Dia)='L')
		Begin 
			set @NumDiaSem = 1
		End
		if(Upper(@Dia)='M')
		Begin 
			set @NumDiaSem = 2
		End
		if(Upper(@Dia)='X')
		Begin 
			set @NumDiaSem = 3
		End
		if(Upper(@Dia)='j')
		Begin 
			set @NumDiaSem = 4
		End
		if(Upper(@Dia)='V')
		Begin 
			set @NumDiaSem = 5
		End
		if(Upper(@Dia)='S')
		Begin 
			set @NumDiaSem = 6
		End
		if(Upper(@Dia)='D')
		Begin 
			set @NumDiaSem = 7
		End
		Return @NumDiaSem 
	End
