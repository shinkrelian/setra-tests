﻿create function [dbo].[FnDevolverCoTipoCostoTC_IDServicio_Det](@IDServicio_Det int)    
returns char(3)    
as     
Begin    

declare @CoTipoCostoTC char(3)=null,
@FlEsViaticosNac bit,
@FlEsViaticosInt bit,
@FlEsTarjetaTelefonica bit,
@FlEsTaxi bit,
@FlEsHotelNocheAntesDesp bit,
@FlEsVuelo bit,
@FlEsHonorario bit,
@FlEsTicketAereos bit,
@FlEsIGVHoteles bit,
@FlEsOtros bit,
@FlEsGEBADS_PrimerDia bit,
@FlEsGEBADS_UltimoDia bit


select 
@FlEsViaticosNac =FlEsViaticosNac,
@FlEsViaticosInt =FlEsViaticosInt,
@FlEsTarjetaTelefonica=FlEsTarjetaTelefonica ,
@FlEsTaxi =FlEsTaxi,
@FlEsHotelNocheAntesDesp=FlEsHotelNocheAntesDesp ,
@FlEsVuelo=FlEsVuelo ,
@FlEsHonorario=FlEsHonorario ,
@FlEsTicketAereos=FlEsTicketAereros ,
@FlEsIGVHoteles =FlEsIGVHoteles,
@FlEsOtros =FlEsOtros,
@FlEsGEBADS_PrimerDia=FlEsGEBADS_PrimerDia ,
@FlEsGEBADS_UltimoDia =FlEsGEBADS_UltimoDia
from MASERVICIOS_DET
where IDServicio_Det=@IDServicio_Det

if @FlEsViaticosNac =1
begin
set @CoTipoCostoTC='VIS'
end

if @FlEsViaticosInt =1
begin
set @CoTipoCostoTC='VID'
end

if @FlEsTarjetaTelefonica =1
begin
set @CoTipoCostoTC='TEL'
end

if @FlEsTaxi =1
begin
set @CoTipoCostoTC='TAX'
end

if @FlEsHotelNocheAntesDesp =1
begin
set @CoTipoCostoTC='NAD'
end

if @FlEsVuelo =1
begin
set @CoTipoCostoTC='VUE'
end

if @FlEsHonorario =1
begin
set @CoTipoCostoTC='HON'
end

if @FlEsTicketAereos =1
begin
set @CoTipoCostoTC='TAE'
end

if @FlEsIGVHoteles =1
begin
set @CoTipoCostoTC='IGV'
end

if @FlEsOtros =1
begin
set @CoTipoCostoTC='OTR'
end

if @FlEsGEBADS_PrimerDia =1
begin
set @CoTipoCostoTC='GPD'
end

if @FlEsGEBADS_UltimoDia =1
begin
set @CoTipoCostoTC='GUD'
end

 return @CoTipoCostoTC    
End  ;
