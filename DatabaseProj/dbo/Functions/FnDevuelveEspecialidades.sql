﻿Create Function dbo.FnDevuelveEspecialidades(@IDProveedor char(6))
returns varchar(Max)
As
Begin
	Declare @texto varchar(Max) = '',@TextoTemp varchar(Max)=''
	Declare curEspecialidades cursor for
	select es.Descripcion from MAESPECIALIDADPROVEEDOR esp Left Join MAESPECIALIDAD es On esp.IDEspecialidad=es.IDEspecialidad
	Where IDProveedor=@IDProveedor
	Open curEspecialidades
	Begin
	Fetch Next From curEspecialidades into @TextoTemp
	While @@FETCH_STATUS =0
	Begin
		Set @texto = @texto + @TextoTemp + char(13)
		Fetch Next From curEspecialidades into @TextoTemp
	End
	End
	Close curEspecialidades
	DealLocate curEspecialidades
	return @texto
End
