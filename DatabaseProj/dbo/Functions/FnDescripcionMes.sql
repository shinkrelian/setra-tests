﻿


---------------------------------------------------------------------------------------------------------

--select dbo.FnDescripcionMes(11)
--CREATE
CREATE Function dbo.FnDescripcionMes (@Mes tinyint)
	Returns varchar(20) As
	Begin
	    Declare @NomMes varchar(20) 
	    
	 select @NomMes=  case when @Mes =1 then 'Enero' 
			when @Mes =2 then 'Febrero' 
			when @Mes =3 then 'Marzo' 
			when @Mes =4 then 'Abril' 
			when @Mes =5 then 'Mayo' 
			when @Mes =6 then 'Junio' 
			when @Mes =7 then 'Julio' 
			when @Mes =8 then 'Agosto' 
			when @Mes =9 then 'Setiembre' 
			when @Mes =10 then 'Octubre' 
			when @Mes =11 then 'Noviembre' 
			when @Mes =12 then 'Diciembre' 
		end
		Return @NomMes 
	End;
