﻿--JRF-20150604-ConsiderarCasos Capacidad 0
--JRF-20150619-Considerar Polit. de Liberado
--JRF-20150619-Considerar TCambio Tarif.
--JRF-20150707- If Declare @NroPaxLibReserva...
--			   ...ango2(@IDServicio_Det,@NroPaxCoti+@NroPa...
--JRF-20150831-if @NroPaxLibReservas > 0....
--JRF-20150909-Si es Costo editado en ventas.. debe serlo en reservas
CREATE Function dbo.FnDevuelveCostoNetoOperadoConAlojamiento(@IDDet int,@IDServicio_Det int,@Capacidad tinyint)
returns numeric(12,2)
As
Begin
	Declare @NroPaxCoti smallint = 0,@SuppSimple numeric(12,2)=0,@SuppTriple numeric(12,2)=0
	Declare @NroPaxResv smallint = (select Sum(CantidadAPagar) from RESERVAS_DET where Anulado=0 And IDDet=@IDDet)
	Declare @CostoNeto numeric(12,2) = Cast((Select IsNull(Monto_sgls,Monto_sgl) from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det) as Numeric(12,2))
	Declare @TipoLib char(1)='',@MaxLib tinyint=0,@NumLiber smallint=0,@IDTipoProv char(3)='',@TCambio numeric(12,2)=0
	Declare @CostoRealEditado bit = 0

	Select @NroPaxCoti=NroPax+IsNull(NroLiberados,0),
		   @SuppSimple = IsNull(Case When cd.FlSSCREditado = 1 Then cd.SSCR Else sd.DiferSS End,0),
		   @SuppTriple = IsNull(Case When cd.FlSTCREditado = 1 Then cd.STCR Else sd.DiferST End,0),
		   @CostoNeto = Case When cd.CostoRealEditado=1 Then cd.CostoReal Else IsNull(IsNull(sd.Monto_sgls,sd.Monto_sgl),0) End,
		   @IDTipoProv=p.IDTipoProv,@TipoLib=IsNull(sd.TipoLib,''),@NumLiber=IsNull(sd.Liberado,0),@MaxLib=IsNull(sd.MaximoLiberado,0),
		   @TCambio = IsNull(sd.TC,1),
		   @CostoRealEditado = cd.CostoRealEditado
	from COTIDET cd Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
	where IDDET=@IDDet
	Set @CostoNeto = IsNull(@CostoNeto,0)

	--Select @NroPaxCoti,@SuppSimple,@SuppTriple,@CostoNeto,@IDTipoProv
	if @CostoNeto = 0
	Begin 
		if @IDTipoProv = '003'
		Begin
			If @TipoLib='P' And (@NroPaxCoti > @NumLiber And @MaxLib > 0)
				Set @NroPaxCoti = @NroPaxCoti - @MaxLib
		End
		
		Declare @NroPaxLibReservas smallint = 0 
		If @Capacidad=2
		Begin
			Set @NroPaxLibReservas = (select sum(NroPax+IsNull(NroLiberados,0)) from COTIDET where IDDET=@IDDet) -
									 (select Sum(CantidadAPagar) from RESERVAS_DET Where IDDet=@IDDet And Anulado=0)
		End

		if @NroPaxLibReservas > 0
		Begin
			Set @CostoNeto = dbo.FnDevuelveCostoPorRango2(@IDServicio_Det,@NroPaxCoti+@NroPaxLibReservas)
			Set @CostoNeto = @CostoNeto / @NroPaxCoti
		End


		if @Capacidad = 1
		Begin
			Set @CostoNeto = @CostoNeto + @SuppSimple
		End
		if @Capacidad = 3
		Begin
			Set @CostoNeto = @CostoNeto + @SuppTriple
		End
	End
	else
	Begin
		if @Capacidad = 1
		Begin
			Set @CostoNeto = @CostoNeto + @SuppSimple
		End
		if @Capacidad = 3
		Begin
			Set @CostoNeto = @CostoNeto + @SuppTriple
		End
		if @Capacidad = 0
		Begin
			Set @Capacidad = 1
		End
		--select @TCambio
		set @CostoNeto = @CostoNeto * @TCambio
		--Set @CostoNeto = @CostoNeto * @Capacidad
	End

	--select @CostoNeto,@NroPaxResv
	return @CostoNeto
End
