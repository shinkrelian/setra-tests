﻿create Function [dbo].[FnDevuelveIdFilePorVoucher](@IDVoucher int)  
returns char(8)
as  
Begin  
   Declare @IDFile char(8)=''

 set @IDFile=( select TOP 1 c.IDFile
from DOCUMENTO_PROVEEDOR d left join OPERACIONES_DET od on od.IDVoucher=d.nuvoucher
left join operaciones o on o.IDOperacion=od.IDOperacion
left join coticab c on c.IDCAB=o.IDCab
where NuVoucher = @IDVoucher and d.IDCab is null and CardCodeSAP is null and FlActivo=1)

   return isnull(@IDFile,'')
End;
