﻿--HLF-20140626-numeric(8,3)  
CREATE Function [dbo].[FnTipoDeCambioVtaUltimoxFecha](@Fecha smalldatetime)  
 returns numeric(8,3)  
as  
 Begin  
 Declare @TipoCambio numeric(8,3)=0  
                    
 Select Top 1 @TipoCambio=ValVenta From MATIPOCAMBIO   
 Where convert(smalldatetime,CONVERT(varchar,Fecha,103))=convert(smalldatetime,CONVERT(varchar,@Fecha,103))  
 Order by Fecha desc  
  
 set @TipoCambio = isnull(@TipoCambio,0)  
  
 return @TipoCambio  
 End
 
