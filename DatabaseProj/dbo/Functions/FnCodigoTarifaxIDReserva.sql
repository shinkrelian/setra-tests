﻿CREATE function dbo.FnCodigoTarifaxIDReserva(@IDReserva int)    
returns varchar(max)    
as     
Begin    
 Declare @DescVariante varchar(max) = '',@TmpVariante varchar(max) = ''    
 Declare @CodTarifa varchar(max) = ''    
 Declare @IDServicio_Det int = (select top(1) rd.IDServicio_Det     
  from RESERVAS_DET rd left join cotidet cd on rd.IDDet=cd.IDDet left join maservicios_Det sd on rd.IDServicio_Det=sd.IDServicio_Det    
  where rd.anulado=0 and rd.IDReserva = @IDReserva) --and sd.PlanAlimenticio = 1 and cd.IncGuia = 0)    
      
 Declare @VerDetaTipo bit = (select Verdetatipo from maservicios_det where idservicio_Det = @IDServicio_Det)    
 --Declare     
     
 if @VerDetaTipo = 1    
 begin    
  set @CodTarifa = (select isnull(CodTarifa,'') from maservicios_det where idservicio_Det = @IDServicio_Det)    
  set @DescVariante = (select Detatipo from maservicios_det where idservicio_Det = @IDServicio_Det)    
      
  if @CodTarifa <> ''    
   set @TmpVariante = @CodTarifa    
  else    
   set @TmpVariante = @DescVariante    
 End    
     
     
 return @TmpVariante    
End    
