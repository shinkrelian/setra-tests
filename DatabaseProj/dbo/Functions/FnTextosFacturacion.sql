﻿
--JRF-20140825-Considerar NuFileLibre     
--JRF-20141103-Alargar la Longitud de Letras permitidas en la primera Linea.    
--HLF-20141118-      set @TextoLine = @TextTmp + REPLICATE(' ',case when @MaxLetras>=LEN(@TextTmp) then   
--  @MaxLetras-LEN(@TextTmp) else ' ' end)  + case when @IDFile<>'' then ' FILE: ' + @IDFile else '' end+ char(13)                  
--HLF-20150417-ddt.NoTexto AS NoTexto,
--HLF-20151019-Set @TextoLine = REPLICATE(' ',120) + ' FILE: '+@IDFile+char(13)
--HLF-20151105-Set @TextoLine = REPLICATE(' ',110) + ' FILE: '+@IDFile+char(13)
--HLF-20151203-Set @TextoLine = REPLICATE(' ',90) + ' FILE: '+@IDFile+char(13)
--PPMG-20160121-Implemente Declare @chrDiv char(1) = '|' en ves del char(13)
CREATE Function [dbo].[FnTextosFacturacion](@NuDocum char(10),@IDTipoDoc char(3))                  
returns varchar(Max)                  
as                  
Begin                  
 Declare @MaxLetras smallint = 90                  
 if @IDTipoDoc = 'FAC'                  
  set @MaxLetras = 78                  
 if @IDTipoDoc = 'BOL'                 
  if substring(@NuDocum,1,3)='005'                 
    set @MaxLetras = 120                
  else       
  Begin      
   if substring(@NuDocum,1,3)='004'      
    set @MaxLetras = 90      
   else      
    set @MaxLetras = 68                  
  End               
 if @IDTipoDoc = 'CBR'     
 Begin    
  if substring(@NuDocum,1,3)='004'      
 set @MaxLetras = 80  
  else    
 set @MaxLetras = 58              
 End    
 if @IDTipoDoc = 'NCR'            
  set @MaxLetras = 78              
                  
                  
 Declare @TextoLine varchar(Max) = ''                  
 Declare @Text varchar(Max) = ''                  
 Declare @TextTmp varchar(Max)='',@IDFile varchar(8)=''   
 Declare @chrDiv char(1) = '|'

 if substring(@NuDocum,1,3)='002' And @IDTipoDoc='BLE'
 Begin
	 Declare curTextos Cursor for                  
	 select --LTRIM(RTRIM(ddt.NoTexto)) AS NoTexto,
	 ddt.NoTexto AS NoTexto,
	 Isnull(c.IDFile,Isnull(d.NuFileLibre,'')) as IDFile                  
	 from DOCUMENTO_DET_TEXTO ddt Left Join DOCUMENTO d On ddt.NuDocum= d.NuDocum and ddt.IDTipoDoc= d.IDTipoDoc                  
	 Left Join COTICAB c On d.IDCab = c.IDCAB                  
	 where ddt.NuDocum = @NuDocum and ddt.IDTipoDoc = @IDTipoDoc                  
	 Order By ddt.NuDetalle Desc
	 Open curTextos                  
	  Fetch Next From curTextos into @Text,@IDFile                  
	  While @@Fetch_Status= 0                  
	  Begin                                    
          if Ltrim(Rtrim(@TextoLine))=''
			--Set @TextoLine = REPLICATE(' ',110) + @Text+char(13)
			--Set @TextoLine = REPLICATE(' ',60) + @Text+' FILE: '+@IDFile+char(13)
			--Set @TextoLine = REPLICATE(' ',110) + ' FILE: '+@IDFile+char(13)
			Set @TextoLine = REPLICATE(' ',90) + ' FILE: '+@IDFile+char(13)
		  Else
		  Begin
			Set @TextoLine =  @TextoLine + @Text+char(13)
		  End     
	   Fetch Next From curTextos into @Text,@IDFile                  
	  End                  
	 Close curTextos                  
	 DealLocate curTextos 
 End
 Else
 Begin
	 Declare curTextos Cursor for                  
	 select --LTRIM(RTRIM(ddt.NoTexto)) AS NoTexto,
	 ddt.NoTexto AS NoTexto,
	 Isnull(c.IDFile,Isnull(d.NuFileLibre,'')) as IDFile                  
	 from DOCUMENTO_DET_TEXTO ddt Left Join DOCUMENTO d On ddt.NuDocum= d.NuDocum and ddt.IDTipoDoc= d.IDTipoDoc                  
	 Left Join COTICAB c On d.IDCab = c.IDCAB                  
	 where ddt.NuDocum = @NuDocum and ddt.IDTipoDoc = @IDTipoDoc                  
	 Open curTextos                  
	  Fetch Next From curTextos into @Text,@IDFile                  
	  While @@Fetch_Status= 0                  
	  Begin                  
	   set @TextTmp = @Text                  
	   if ltrim(rtrim(@TextTmp))<>''                  
		if @TextoLine = ''                   
		 if charindex(@chrDiv,@TextTmp) > 0                  
		  set @TextoLine = SUBSTRING(@TextTmp,1,charindex(@chrDiv,@TextTmp)-1) +                   
			   REPLICATE(' ',@MaxLetras-LEN(SUBSTRING(@TextTmp,1,charindex(@chrDiv,@TextTmp)-1)))  +                  
			  'FILE: ' + @IDFile+ char(13)+ SUBSTRING(@TextTmp,charindex(@chrDiv,@TextTmp)+1,LEN(@TextTmp))                  
		 else                  
		  --set @TextoLine = @TextTmp + REPLICATE(' ',@MaxLetras-LEN(@TextTmp))  +'FILE: ' + @IDFile+ char(13)                  
		  set @TextoLine = @TextTmp + REPLICATE(' ',case when @MaxLetras>=LEN(@TextTmp) then   
						   @MaxLetras-LEN(@TextTmp) else ' ' end)  + case when @IDFile<>'' then ' FILE: ' + @IDFile else '' end+ @chrDiv
		else                  
		 set @TextoLine = @TextoLine + @TextTmp + @chrDiv
                    
	   Fetch Next From curTextos into @Text,@IDFile                  
	  End                  
	 Close curTextos                  
	 DealLocate curTextos
	 SET @TextoLine = REPLACE(@TextoLine,@chrDiv,char(13))
 End               
                   
 Return @TextoLine                  
End                  
