﻿Create function [dbo].[fnDescBusesxIDReserva](@IDReserva int)
Returns varchar(max)
as
Begin
	Declare @DescBuss varchar(max)='',@tiCont tinyint = 0
	Declare @TmpDescBuss varchar(max)
	
	Declare curBuss Cursor For
	select vh.NoVehiculoCitado
	from RESERVAS_DET rd Left Join MAVEHICULOS vh on rd.NuVehiculo = vh.NuVehiculo
	where IDReserva = @IDReserva
	and vh.NoVehiculoCitado is not null
	order by vh.QtDe
	Open curBuss
	Fetch Next From curBuss Into @TmpDescBuss
	while @@Fetch_Status=0
		Begin
			If @tiCont=0
				Set @DescBuss=@TmpDescBuss+char(13)
			Else
				if charindex(@TmpDescBuss,@DescBuss)=0
					Set @DescBuss=@DescBuss+@TmpDescBuss+char(13)
			
			Set @tiCont	= @tiCont + 1	
			Fetch Next From curBuss Into @TmpDescBuss
		End
	Close curBuss
	Deallocate curBuss
	
	Return @DescBuss
End
