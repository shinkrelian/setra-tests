﻿--JRF-20151012-Desde otra fuente de datos [Tabla]
CREATE Function [dbo].[FnIdiomasProveedor] (@IDProveedor char(6))
Returns varchar(Max)
As 
Begin
	Declare @Idiomas varchar(Max)='', @Idioma varchar(Max), @tiCont tinyint=0

	Declare curIdiomas Cursor For
	Select IDIdioma+'-('+
		Case rtrim(IsNull(Estado,''))
			When 'P' then 'PREFERENTE'
			When 'O' then 'OK'
			When 'E' then 'EVITAR'
		Else '' End
	+')' From MAENTREVISTAPROVEEDOR Where IDProveedor=@IDProveedor And Estado is not null

	Open curIdiomas
	Fetch Next From curIdiomas Into @Idioma
	While @@Fetch_Status=0
		Begin		
		If @tiCont=0
			Set @Idiomas=@Idioma+char(13)
		Else
			Set @Idiomas=@Idiomas+@Idioma+char(13)
		
		Set @tiCont	= @tiCont + 1
		Fetch Next From curIdiomas Into @Idioma
		End

	Close curIdiomas
	Deallocate curIdiomas
	
	Return @Idiomas
End
