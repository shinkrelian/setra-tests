﻿--HLF-20150916-Cambios cuentas
--if @IDBanco = '010'--bcp counter		
CREATE Function dbo.FnDevuelveTransferAccount(@IDBanco char(3),@IDMoneda char(3))
returns varchar(10)
as
Begin
	Declare @CuentaCont varchar(10)=''
	if @IDBanco = '001'--bcp
	Begin
		If @IDMoneda = 'SOL'
			set @CuentaCont='10411001'
		Else
			set @CuentaCont='10412001'
	End

	if @IDBanco = '002'--bbva
	Begin
		If @IDMoneda = 'SOL'
			set @CuentaCont='10411002'
		Else
			set @CuentaCont='10412002'
	End

	if @IDBanco = '010'--bcp counter		
		set @CuentaCont='10412003'
	
	Return @CuentaCont
End

