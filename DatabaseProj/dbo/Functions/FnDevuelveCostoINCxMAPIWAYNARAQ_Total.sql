﻿--JHD-20160211 - Comentar And FlEntWPGenerada=1 
--JHD-20160211 - Comentar And FlEntMPGenerada=1 
--JHD-20160415 - SELECT @NroPaxAdulto = SUM(CASE WHEN FlDesactNoShow = 0 AND 
CREATE Function dbo.FnDevuelveCostoINCxMAPIWAYNARAQ_Total(@IDDet int,@IDServicio char(8),@Anio char(4))
returns numeric(12,4)
As
Begin
	Declare @CostoTotal numeric(12,4) = 0
	Declare @NroPaxAdulto tinyint = 0
	Declare @NroPaxAdultoCAN tinyint = 0
	Declare @NroPaxNinio tinyint = 0
	Declare @NroPaxNinioCAN tinyint = 0
	
	SELECT @NroPaxAdulto = SUM(CASE WHEN FlDesactNoShow = 0 AND cp.IDNacionalidad NOT IN('000323','000018','000013','000007')
								AND NOT(Lower(Titulo)='chd.' AND FlAdultoINC=0) THEN 1 ELSE 0 END),
		@NroPaxAdultoCAN = SUM(CASE WHEN FlDesactNoShow = 0 AND cp.IDNacionalidad IN('000323','000018','000013','000007')
								AND NOT(Lower(Titulo)='chd.' AND FlAdultoINC=0) THEN 1 ELSE 0 END),
		@NroPaxNinio = SUM(CASE WHEN FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
								AND cp.IDNacionalidad NOT IN ('000323','000018','000013','000007') THEN 1 ELSE 0 END),
		@NroPaxNinioCAN = SUM(CASE WHEN FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
								AND cp.IDNacionalidad IN ('000323','000018','000013','000007') THEN 1 ELSE 0 END)
	FROM COTIDET_PAX CDP INNER JOIN COTIPAX CP ON CDP.IDPax = CP.IDPax
	WHERE IDDet = @IDDet

	/*
	if @IDServicio = 'CUZ00133'
	Begin
		Set @NroPaxAdulto= (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0 
							/*And FlEntWPGenerada=1*/ And Not cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxAdultoCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0
							/* And FlEntWPGenerada=1*/ And cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxNinio = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
							/*And	FlEntWPGenerada=1*/ And Not cp.IDNacionalidad In ('000323','000018','000013','000007'))
		Set @NroPaxNinioCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
							/*  And FlEntWPGenerada=1 */ And cp.IDNacionalidad In ('000323','000018','000013','000007'))
	
	End
	Else
	Begin
		Set @NroPaxAdulto= (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0 
							/*And FlEntMPGenerada=1*/ And Not cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxAdultoCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And FlDesactNoShow=0 
							/*And FlEntMPGenerada=1*/ And cp.IDNacionalidad In('000323','000018','000013','000007') and Not(Lower(Titulo)='chd.' and FlAdultoINC=0))
		Set @NroPaxNinio = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
							/*And FlEntMPGenerada=1*/ And Not cp.IDNacionalidad In ('000323','000018','000013','000007'))
		Set @NroPaxNinioCAN = (Select count(*) from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax Where IDDet=@IDDet And FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
							/*And FlEntMPGenerada=1*/ And cp.IDNacionalidad In ('000323','000018','000013','000007'))
	End
	--*/
	
	Set @CostoTotal = dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'AD' End) * @NroPaxAdulto
	Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CAN' End) * @NroPaxAdultoCAN)
	Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CHD' End) * @NroPaxNinio)
	Set @CostoTotal = @CostoTotal + (dbo.FnDevuelveTarifaINC(@IDServicio,@Anio,Case When @IDServicio='CUZ00145' Then '' Else 'CHD-CAN' End) * @NroPaxNinioCAN)
	
	return @CostoTotal;
End;
