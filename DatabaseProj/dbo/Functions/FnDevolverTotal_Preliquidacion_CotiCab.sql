﻿

--JHD-20150623- Filtrar por estado anulado
CREATE Function [dbo].[FnDevolverTotal_Preliquidacion_CotiCab]   
(
@idcab int,
@CoUbigeo_Oficina char(6)
)
Returns numeric(20,2)  
--As       
      
Begin 
declare @SumaTotal numeric(20,6)

 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)              
 Declare @MargenInterno as numeric(6,2)=10
                                                       
declare @tabla as table(
TotalUSD numeric(20,6) null
)

insert into @tabla
--1. Vouchers
SELECT                
  
  Total

 FROM              

  (              

  SELECT               

    IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,IDVoucher,CompraNeto,              

    IGVCosto,IGV_SFE,Moneda,        

    CompraNetoUSD+IGVCostoUSD+IGV_SFEUSD AS Total,        

    idTipoOC,              

    DescOperacionContab, Margen,               
               

 CompraNetoUSD+IGVCostoUSD AS CostoBaseUSD,               

    

    ROUND( (CompraNetoUSD+IGVCostoUSD) /(1 - (Margen/100)) ,0) AS VentaUSD        

     FROM               

   (              

   SELECT YY.IDProveedor,YY.DescProveedor, YY.DescProveedorInternac,DescPaisProveedor,  

   YY.IDVoucher,              

    YY.CompraNeto,--YY.CompraNetoxPax,        

    CompraNetoUSD,        

    YY.IGVCosto, --yy.IGVCostoxPax,      

    IGVCostoUSD,        

    Case When YY.idTipoOC='001' Then               

     YY.CompraNeto*(@IGV/100)              

    Else                

     Case When YY.idTipoOC='006' Then (YY.CompraNeto*(@IGV/100))*0.5 Else 0 End              

    End as IGV_SFE,              

            

    Case When YY.idTipoOC='001' Then               

     YY.CompraNetoUSD*(@IGV/100)              

    Else                

     Case When YY.idTipoOC='006' Then (YY.CompraNetoUSD*(@IGV/100))*0.5 Else 0 End              

    End as IGV_SFEUSD,              

    

    yy.Moneda, --YY.Total,               

    idTipoOC,YY.DescOperacionContab, YY.Margen              

   
   FROM              

    (              

    SELECT XX.*              


    FROM              

     (              

     SELECT Z.*,               

 

     Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 

		dbo.FnPromedioPonderadoMargen(@IDCab, Z.IDProveedor) 

	 Else

		@MargenInterno

	 End AS Margen

    

     FROM              

      (              

      SELECT IDProveedor, DescProveedorInternac,              

      DescProveedor,DescPaisProveedor,IDVoucher,              

      

      CompraNeto as CompraNeto,          

      dbo.FnCambioMoneda(CompraNeto,IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Y.IDMoneda),TC)) as CompraNetoUSD,          

     

      IGVCosto as IGVCosto,          

      dbo.FnCambioMoneda(IGVCosto,IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Y.IDMoneda),TC)) as IGVCostoUSD,          

 
      IDMoneda As Moneda ,               

      idTipoOC,DescOperacionContab              

      FROM               

       (              

              

       SELECT IDProveedor,DescProveedorInternac, DescProveedor,DescPaisProveedor,  

       IDVoucher,              

       (SUM(CostoReal)) as CompraNeto,               
             

       SUM(TotImpto) as IGVCosto,            
         

       idTipoOC,DescOperacionContab,IDMoneda,TC          

       FROM              

        (              

        SELECT O.IDProveedor,               

        pint.NombreCorto as  DescProveedorInternac,              

        P.NombreCorto AS DescProveedor,   

        paPI.Descripcion as DescPaisProveedor,  

        OD.IDVoucher, OD.NroPax,                                   

        Case When SD.ConAlojamiento=1 Or p.IDProveedor='001836' Then                

	   od.NetoGen        

	  Else        

	   Case When sd.idTipoOC In ('006','001') Then--Proporcion o Exportacion        

		od.NetoGen        

	   else        
	    Case When @CoUbigeo_Oficina<>'000113' Then 
			od.TotalGen			
		Else--
			
			Case when o.IDProveedor='002315' Then --Setours Buenos Aires
				(SELECT isnull(sum(dbo.FnCambioMoneda(ODS2.Total,ods2.IDMoneda,'USD',isnull(tca.SsTipCam,sd2.TC))),0)
				FROM OPERACIONES_DET_DETSERVICIOS ODS2 
				INNER JOIN OPERACIONES_DET OD2 ON ODS2.IDOperacion_Det=OD2.IDOperacion_Det AND OD2.IDServicio=ODS2.IDServicio
				INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab AND O2.IDProveedor=o.IDProveedor--Setours Buenos Aires
				INNER JOIN MASERVICIOS_DET SD2 ON ODS2.IDServicio_Det_V_Cot=SD2.IDServicio_Det
				INNER JOIN MASERVICIOS S2 ON SD2.IDServicio=S2.IDServicio 
				INNER JOIN MAPROVEEDORES P2 ON S2.IDProveedor=P2.IDProveedor AND P2.IDProveedor<>'002329'--Transporte Buenos Aires 
				LEft Join COTICAB_TIPOSCAMBIO tca On tca.IDCab=o2.IDCab And tca.CoMoneda=ods2.IDMoneda
				WHERE ODS2.IDOperacion_Det IN(od.IDOperacion_Det))
			Else
				od.TotalGen
			End


		End
	   End        

	  End as CostoReal,          


        Case When sd.idTipoOC in('002','006') Then               

   Case When SD.ConAlojamiento=1 Or p.IDProveedor='001836' Then         
      

    OD.NetoGen*(@IGV/100)        

   Else        

   Case When sd.idTipoOC = '002' Then        

    OD.TotalGen*(@IGV/100)         

   Else--Proporcion 006        

    (OD.NetoGen*(@IGV/100) )/2        

   End        

  End         

  Else              

         0              

        End               

        As TotImpto,              

        sd.idTipoOC,              

        tox.Descripcion as DescOperacionContab, 

		Case When @CoUbigeo_Oficina='000113' and o.IDProveedor='002315' Then
			'USD'
		Else
			RD.IDMoneda
		End as IDMoneda, 


		SD.TC              

                      

        FROM OPERACIONES O INNER JOIN MAPROVEEDORES P ON O.IDProveedor=P.IDProveedor              

        --And P.IDTipoOper='E'

        And (P.IDTipoOper='E' Or LTRIM(rtrim(@CoUbigeo_Oficina))<>'') 

			And (P.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='' )

        INNER JOIN OPERACIONES_DET OD ON O.IDOperacion=OD.IDOperacion                

        AND (OD.IDVoucher<>0 OR O.IDProveedor='001836' )--PERURAIL              

        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              

        INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det              

        Inner join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc              

        Left Join MAPROVEEDORES pint On pint.IDProveedor=p.IDProveedorInternacional              

          

        Left Join MAUBIGEO ubPI On p.IDCiudad=ubPI.IDubigeo  
        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo 
        Left Join RESERVAS r On O.IDReserva=r.IDReserva    

		LEFT join coticab cc on cc.idcab=o.idcab



        WHERE O.IDCab=@IDCab and r.Anulado=0 and RD.Anulado=0 --AND (OD.IDVoucher<>0 OR O.IDProveedor='001836' )              
		and cc.estado<>'X'
		
        ) AS X              

       GROUP BY IDProveedor,DescProveedorInternac,DescProveedor,DescPaisProveedor,idTipoOC,DescOperacionContab,IDVoucher,IDMoneda,TC          

       ) AS Y              

      ) AS Z          

     ) AS XX              

    ) AS YY              

   ) AS ZZ         

  ) AS ZZ1              

 WHERE CostoBaseUSD<>0

 --Órdenes de Servicio
 insert into @tabla
 --------------------------------------------------------------------------------------------------------------------
 SELECT Total

 FROM                  

  (                  

  SELECT                   

   ZZ.IDProveedor, ZZ.DescProveedor, ZZ.DescProveedorInternac,   

   DescPaisProveedor,ZZ.NroOrdenServicio,                  

   ZZ.CompraNeto, ZZ.IGVCosto,           

   ZZ.IGV_SFE, ZZ.Moneda,                   

   ZZ.CompraNetoUSD+ZZ.IGVCosto+ZZ.IGV_SFE AS Total,                  

   ZZ.idTipoOC, ZZ.DescOperacionContab, ZZ.Margen,                  

   ZZ.CostoBaseUSD,                   

   ROUND( ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD                  

   --,YY.FacturaExportacion,YY.BoletaNoExportacion                  

  FROM                  

   (                  

   SELECT                   

    YY.IDProveedor, YY.DescProveedor, YY.DescProveedorInternac,   

    DescPaisProveedor,YY.NroOrdenServicio,                  

    YY.CompraNeto,YY.CompraNetoUSD, YY.IGVCosto,                   

    YY.IGV_SFE, --YY.CompraNetoUSD+YY.IGVCosto+YY.IGV_SFE AS Total,                  

    YY.Moneda, YY.idTipoOC, YY.DescOperacionContab, YY.Margen,                  

    --YY.CompraNeto+IGVCosto+IGV_SFE as CostoBaseUSD                   

    YY.CompraNetoUSD+IGVCosto as CostoBaseUSD                   

    --YY.VentaUSD,                  

    --YY.FacturaExportacion,YY.BoletaNoExportacion                  

   FROM                  

    (                  

    SELECT XX.IDProveedor, DescProveedor, DescProveedorInternac,   

     DescPaisProveedor,NroOrdenServicio,                  

     CompraNeto,CompraNetoUSD, IGVCosto,                   

     Case When idTipoOC='001' Then                   

       CompraNeto*(@IGV/100)                   

      Else                    

       Case When idTipoOC='006' Then (CompraNeto*(@IGV/100))*0.5 Else 0 End                  

      End as IGV_SFE,                   

     Moneda, idTipoOC, DescOperacionContab, Margen                   

    FROM                  

     (       

     SELECT IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,  

                       

      --(SELECT Top 1--DISTINCT                  

      --LTRIM(RTRIM(SUBSTRING(NoArchAdjunto,CHARINDEX('.',NoArchAdjunto,0)-13,13)))                  

      -- From .BDCORREOSETRA..ADJUNTOSCORREOFILE WHERE NuCorreo IN                  

      --(                  

      --SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE NuCorreo IN                  

      -- (                  

      -- SELECT NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE WHERE NuCorreo IN                  

      --  (                  

      --  SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'                    

      --  AND (SELECT Top 1 ISNULL(p1.Email3,'')+','+ISNULL(p1.Email4,'')+','+                  

      --  ISNULL(p1.EmailRecepcion1,'')+','+ISNULL(p1.EmailRecepcion2,'')                  

      --  FROM MAPROVEEDORES p1                   

      --   WHERE p1.IDProveedor=Z.IDProveedor) LIKE '%'+NoCuentaPara+'%'                  

                           

      --  )                  

                          

      -- Union                  

      -- SELECT NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE WHERE NuCorreo IN                  

      --  (                       

      --  SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'  and                   

      --   NoCuentaPara In (select correo from MACORREOREVPROVEEDOR where IDProveedor=Z.IDProveedor)                  

      --  )                  

      -- )                  

      --)                   

      --) AS NroOrdenServicio,                    

      (Select substring(CoOrden_Servicio,1,8)+'-'+substring(CoOrden_Servicio,9,6) From ORDEN_SERVICIO Where IDCab=@IDCab And IDProveedor=Z.IDProveedor) AS NroOrdenServicio,                    

                       

  CompraNeto, CompraNetoUSD, IGVCosto,Moneda,                   

      --Total,                   

      idTipoOC, DescOperacionContab, Margen                  

      --CostoBaseUSD,                  

      --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD     

     FROM                  

      (                  

      SELECT IDProveedor,DescProveedor,DescProveedorInternac,  

      DescPaisProveedor,  

      SUM(CompraNeto) AS CompraNeto,   sum(CompraNetoUSD) as CompraNetoUSD,              

      SUM(IGVCosto) AS IGVCosto, Moneda,                   

      --SUM(Total) AS Total,                   

      idTipoOC, DescOperacionContab,                  

      AVG(Margen) AS Margen                  

      --SUM(Total) as CostoBaseUSD                  

      FROM                  

       (                  

       --Declare @IDCab int=2309                  

       SELECT X.*,                  

       --(SELECT AVG(MargenAplicado) FROM COTIDET cd1                   

       -- WHERE cd1.IDCAB=@IDCab AND cd1.IDProveedor=X.IDProveedorOpe  And isnull(cd1.IDDetRel,0)=0            

       -- ) AS Margen,            

       

       --dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) AS Margen,            

        

		 Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 

			dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) 

		 Else

			@MargenInterno

		 End AS Margen,                      

        

        Case When idTipoOC='002' Then             

   CompraNeto*(@IGV/100)             

    Else            

   0            

  End as IGVCosto            

                          

  FROM                  

        (                  

        SELECT ODS.IDProveedor_Prg as IDProveedor, pint.NombreCorto as DescProveedorInternac,                  

        P.NombreCorto as DescProveedor,                     

        paPI.Descripcion As DescPaisProveedor,  

        ODS.IDOperacion_Det,                  

 Case When ODS.IngManual=0 Then             

    Case When isnull(NetoCotizado,0) = 0 then                            

         

 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,          

    OD.NroPax+isnull(od.NroLiberados,0)),                             

    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,ods.IDMoneda,isnull(@TipoCambio,SD.TC))                     

    SD.CoMoneda,ods.IDMoneda,isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     

  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                     

    else                 

  --isnull(ODS.NetoProgram,ODS.CostoReal)            

   ods.NetoCotizado      

    End             

 Else      

  ODS.NetoProgram      

 End      

      

  --End            

  As CompraNeto,                              

            

  --Case When ODS.IDMoneda='USD' Then            

  -- isnull(ODS.NetoProgram,ODS.CostoReal)            

  --Else             

 Case When ODS.IngManual=0 Then             

  Case When isnull(NetoCotizado,0) = 0 then                            

  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(od.NroLiberados,0)),                             

  --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,'USD',isnull(@TipoCambio,SD.TC))                     

  SD.CoMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where     

  IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))                     

  else                 

   --dbo.FnCambioMoneda(isnull(ODS.NetoProgram,ODS.CostoReal),ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))                   

   dbo.FnCambioMoneda(isnull(ODS.NetoProgram,ODS.CostoReal),ODS.IDMoneda,'USD',    

 isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     

  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                   

  End            

  --End      

 Else      

  --dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))      

  dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',    

 isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     

  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))      

 End      

  As CompraNetoUSD,                              

              

              

              

        ODS.TotImpto,                  

              

        ODS.IDMoneda as Moneda,              

        --ODS.CostoReal+ODS.TotImpto as Total ,               

        sd.idTipoOC,                  

        tox.Descripcion as DescOperacionContab,                  

        --SCot.IDProveedor as IDProveedorCot,                  

        --PCot.NombreCorto as DescProveedorCot,                  

        --SDCot.IDServicio_Det                  

        ODS.IDServicio_Det,                  

        O.IDProveedor as IDProveedorOpe                  

        ,SD.IDServicio                  

        FROM OPERACIONES_DET_DETSERVICIOS ODS                      

        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det                   

   AND OD.IDServicio=ODS.IDServicio                  

        --And not ODS.TotalCotizado is null              

        --AND OD.IDServicio=ODS.IDServicio                  

        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=@IDCab                  

        --INNER JOIN MASERVICIOS S ON OD.IDServicio=S.IDServicio AND S.IDProveedor=O.IDProveedor                  

        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              

                          

        LEFT JOIN COTICAB CC ON CC.IDCAB=O.IDCab                  

        LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor                  

         --INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det                      

                               

         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Cot=SD.IDServicio_Det                  
         LEFT join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc                  

                         --LEFT JOIN MASERVICIOS_DET SDCot ON ODS.IDServicio_Det=SDCot.IDServicio_Det                  

        --LEFT JOIN MASERVICIOS SCot ON SDCot.IDServicio=SCot.IDServicio                  

        --LEFT JOIN MAPROVEEDORES PCot ON SCot.IDProveedor=PCot.IDProveedor                  

                  

        Left Join MAPROVEEDORES pint On pint.IDProveedor=p.IDProveedorInternacional                  

    

        Left Join MAUBIGEO ubPI On p.IDCiudad=ubPI.IDubigeo  

        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo  

                        

        Where not ODS.IDProveedor_Prg is null and not P.NombreCorto is null                  

         And isnull(p.IDUsuarioTrasladista,'0000')='0000'              

         --And p.IDUsuarioTrasladista is null              

         And ODS.IDProveedor_Prg <> '001935'--SETOURS CUSCO                  

         And ODS.Activo=1-- and ODS.TotalProgram>0          

         And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
		 and cc.estado<>'X'
        ) AS X                  

       ) AS Y                  

      GROUP BY IDProveedor,DescProveedor,DescProveedorInternac,DescPaisProveedor,Moneda,idTipoOC,DescOperacionContab                  

      ) as Z                  

     ) AS XX                  

    ) AS YY                  

   ) AS ZZ                    

  ) AS A                    

      WHERE CompraNeto>0   and not NroOrdenServicio is null

--3. Entradas/Presupuestos
insert into @tabla
SELECT Total
 FROM              
  (              
  SELECT               
   ZZ.IDProveedor, ZZ.DescProveedor, ZZ.DescProveedorInternac,DescPaisProveedor,  
   ZZ.CompraNeto, ZZ.IGVCosto,               
   ZZ.IGV_SFE, ZZ.Moneda,               
   ZZ.Total,              
   ZZ.idTipoOC, ZZ.DescOperacionContab, ZZ.Margen,              
   ZZ.CostoBaseUSD,               
   ROUND( ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD              
   --,YY.FacturaExportacion,YY.BoletaNoExportacion              
  FROM              
   (              
   SELECT               
    YY.IDProveedor, YY.DescProveedor, YY.DescProveedorInternac,DescPaisProveedor,  
    YY.CompraNeto, YY.IGVCosto,               
    YY.IGV_SFE,             
    --YY.CompraNeto+YY.IGVCosto+YY.IGV_SFE AS Total,              
    YY.Total,            
    YY.Moneda, YY.idTipoOC, YY.DescOperacionContab, YY.Margen,              
    CompraNetoUSD+TotImptoUSD+IGV_SFE as CostoBaseUSD               
    --YY.VentaUSD,              
    --YY.FacturaExportacion,YY.BoletaNoExportacion              
   FROM              
    (              
    SELECT XX.IDProveedor, DescProveedor, DescProveedorInternac,DescPaisProveedor,  
     CompraNeto, IGVCosto,               
     --Case When idTipoOC='001' Then               
     --  CompraNeto*(@IGV/100)               
     -- Else                
     --  Case When idTipoOC='006' Then (CompraNeto*(@IGV/100))*0.5 Else 0 End              
     -- End as IGV_SFE,               
     0 AS IGV_SFE,            
                 
     Moneda, idTipoOC, DescOperacionContab, Margen, Total,            
     CompraNetoUSD, TotImptoUSD               
     --CostoBaseUSD, VentaUSD,              
     --Case When XX.idTipoOC='001' Then VentaUSD Else 0 End As FacturaExportacion,              
     --Case When XX.idTipoOC='001' Then 0 Else VentaUSD End As BoletaNoExportacion              
    FROM              
     (                
     SELECT IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,  
                   
      CompraNeto, IGVCosto,Moneda,               
      --CompraNetoUSD+TotImptoUSD as Total,               
      CompraNetoUSD as Total,               
      idTipoOC, DescOperacionContab, Margen, CompraNetoUSD, TotImptoUSD              
      --CostoBaseUSD,              
      --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD              
     FROM              
      (              
      SELECT IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,  
      SUM(CompraNeto) AS CompraNeto,              
      SUM(IGVCosto) AS IGVCosto, Moneda,               
      --SUM(CompraNeto+IGVCosto) AS Total,               
      idTipoOC, DescOperacionContab,              
      AVG(Margen) AS Margen, sum(CompraNetoUSD) as CompraNetoUSD, sum(TotImptoUSD) as TotImptoUSD            
      --SUM(Total) as CostoBaseUSD              
      FROM              
       (              
       SELECT X.*,              
       --(SELECT AVG(MargenAplicado) FROM COTIDET cd1                           
       -- WHERE cd1.IDCAB=@IDCab AND cd1.IDProveedor=X.IDProveedorOpe  And isnull(IDDetRel,0)=0            
       -- ) AS Margen              

        --dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) AS Margen          
		 Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 
			dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe)
		 Else
			@MargenInterno
		 End AS Margen

        FROM              
   (              
        SELECT PCot.IDProveedor as IDProveedor, pint.NombreCorto as DescProveedorInternac,              
        PCot.NombreCorto as DescProveedor,       
        paPI.Descripcion as DescPaisProveedor,            
        ODS.IDOperacion_Det,              
        --isnull(ODS.NetoProgram ,ODS.CostoReal) as CompraNeto,              
                      
        --Case When ODS.IDMoneda = 'USD' Then isnull(Ods.CostoReal ,0)               
   --Else dbo.FnCambioMoneda(isnull(Ods.CostoReal ,0),'USD','SOL',SD.TC)              
        --End --* OD.NroPax                       
        --as CompraNeto,              
        --ODS.CostoReal as CompraNeto,              
                
                    
    --    Case When isnull(NetoCotizado,0) = 0 then             
    --dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(OD.NroLiberados,0)),                           
    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,odS.IDMoneda,SD.TC)                   
    --else               
    --NetoCotizado               
    --End As CompraNeto,              
                    
                    
    --    isnull(Ods.CostoReal ,0) as CompraNetoUSD,            
                
        
 Case When ODS.IngManual=0 Then               
    Case When isnull(NetoCotizado,0) = 0 then                              
           
    dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,            
    OD.NroPax+isnull(od.NroLiberados,0)),                               
    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,ods.IDMoneda,isnull(@TipoCambio,SD.TC))                       
    SD.CoMoneda,ods.IDMoneda,isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     
  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                       
    else                   
   ods.NetoCotizado        
    End               
 Else        
  ODS.NetoProgram        
 End        
        
 As CompraNeto,                                
              
 Case When ODS.IngManual=0 Then               
  Case When isnull(NetoCotizado,0) = 0 then                              
  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(od.NroLiberados,0)),                               
  --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,'USD',isnull(@TipoCambio,SD.TC))                       
  SD.CoMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))    
  else                   
   --dbo.FnCambioMoneda(NetoCotizado,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))                     
   dbo.FnCambioMoneda(NetoCotizado,ODS.IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
 And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                     
  End              
        
 Else        
  --dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))        
  dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
 And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))        
 End        
  As CompraNetoUSD,                                
                
        
                
        --ODS.TotImpto as IGVCosto,                      
        Case When ODS.IDMoneda = 'USD' Then ODS.TotImpto               
        Else dbo.FnCambioMoneda(ODS.TotImpto,'USD','SOL',SD.TC)              
        End as IGVCosto,            
        ODS.TotImpto  AS TotImptoUSD,            
                       
                    
        ODS.IDMoneda as Moneda,            
                    
        --ODS.CostoReal+ODS.TotImpto as Total ,              
                    
        sd2.idTipoOC,              
        tox.Descripcion as DescOperacionContab,              
        --SCot.IDProveedor as IDProveedorCot,              
        --PCot.NombreCorto as DescProveedorCot,              
        --SDCot.IDServicio_Det              
        ODS.IDServicio_Det,              
        O.IDProveedor as IDProveedorOpe       
        ,SD.IDServicio              
        FROM OPERACIONES_DET_DETSERVICIOS ODS                  
        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det               
        AND OD.IDServicio=ODS.IDServicio              
        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=@IDCab  
        --INNER JOIN MASERVICIOS S ON OD.IDServicio=S.IDServicio AND S.IDProveedor=O.IDProveedor              
        --INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              
                      
        LEFT JOIN COTICAB CC ON CC.IDCAB=O.IDCab              
        --LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor              
         --INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det                  
                           
         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Cot=SD.IDServicio_Det              
         Left JOIN MASERVICIOS_DET SD2 ON ODS.IDServicio_Det=SD2.IDServicio_Det              
                   
         LEFT join MATIPOOPERACION tox on sd2.idTipoOC=tox.IdToc              
                       
              
        --INNER JOIN MASERVICIOS_DET SDCot ON ODS.IDServicio_Det_V_Cot=SDCot.IDServicio_Det              
        INNER JOIN MASERVICIOS SCot ON SD.IDServicio=SCot.IDServicio              
        INNER JOIN MAPROVEEDORES PCot ON SCot.IDProveedor=PCot.IDProveedor             
        And (PCot.IDTipoProv in ('006') OR PCot.IDProveedor='001593' ) --MUSEO  O CONSETTUR TRANSPORTISTA          
			And (PCot.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')

        Left Join MAPROVEEDORES pint On pint.IDProveedor=pCOT.IDProveedorInternacional              
        --Where --not ODS.IDProveedor_Prg is null and not P.NombreCorto is null              
         --ODS.IDProveedor_Prg <> '001935'--SETOURS CUSCO     
  
  Left Join MAUBIGEO ubPI On PCot.IDCiudad=ubPI.IDubigeo  
        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo           
                 
         Where ODS.Activo=1  
		 and cc.estado<>'X'        
        ) AS X              
       ) AS Y              
      GROUP BY IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,  
      Moneda,idTipoOC,DescOperacionContab              
      ) as Z              
     ) AS XX              
    ) AS YY              
   ) AS ZZ                
  ) AS A                
 
 --4. Buses y Vuelos
 insert into @tabla
  SELECT  Total
 FROM            
  (            
  Select Z.*,      
  Case When Tarifa=0 Then       
 ROUND( Z.Total /(1 - (Margen/100)) ,0)       
  Else        
 Z.Tarifa*Z.NroPax       
  End AS VentaUSD             
  From            
   (            
   Select Y.*,             
   (Y.CostoRealUSD+Y.TotImptoUSD+Y.IGV_SFE) as TotalUSD,      
   (Y.CompraNeto+Y.TotImpto+Y.IGV_SFE) as Total                
         
   From            
    (            
    Select X.IDProveedor, X.DescProveedor, X.DescProveedorInternac, RutayCodReserva,            
    DescPaisProveedor,  
    X.CostoReal  as CompraNeto,      
          
     Case When IDMoneda = 'USD' Then       
  CostoReal      
  Else       
  --dbo.FnCambioMoneda(X.CostoReal,'SOL','USD',ISNULL(@TipoCambio,tc))        
  dbo.FnCambioMoneda(X.CostoReal, X.IDMoneda,'USD',    
 ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=X.IDMoneda),tc))        
  End as CostoRealUSD,       
          
    X.CostoReal *(@IGV/100) as TotImpto,               
    Case When IDMoneda = 'USD' Then       
  X.CostoReal *(@IGV/100)      
  Else       
  --dbo.FnCambioMoneda(X.CostoReal *(@IGV/100),'SOL','USD',ISNULL(@TipoCambio,tc))        
  dbo.FnCambioMoneda(X.CostoReal *(@IGV/100),X.IDMoneda,'USD',    
 ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=X.IDMoneda),tc))        
  End as TotImptoUSD,               
                
                
    --Case When idTipoOC='001' Then             
    -- X.CostoReal*(@IGV/100)        
    --Else              
    -- Case When idTipoOC='006' Then (X.CostoReal*(@IGV/100))*0.5 Else 0 End            
    --End as IGV_SFE,             
    0 as IGV_SFE,      
    X.IDMoneda,            
         
    X.DescOperacionContab,             
    X.Margen, X.idTipoOC, X.Tarifa, X.NroPax, X.Servicio      
    From            
     (            
     Select p.IDProveedor,p.NombreCorto as DescProveedor,             
     pint.NombreCorto as DescProveedorInternac,             
     paPI.Descripcion as DescPaisProveedor,  
     --isnull(cv.ruta,'')+ ' - '+ ISNULL(cv.CodReserva,'') as RutayCodReserva,      
     isnull(cv.ruta,'') as RutayCodReserva,      
     --cd.CostoReal * cd.NroPax as CostoReal,             
           
     Case When isnull(cv.CostoOperador,0)=0 Then      
  isnull(od.TotalGen,(cv.Tarifa*(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID))/(1+(@IGV/100)) )       
     Else      
  cv.CostoOperador*(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID)/(1+(@IGV/100))      
     End as CostoReal,      
     --Isnull(cd.TotImpto*cd.NroPax,(cv.Tarifa*(@IGV/100))* (Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID)) as TotImpto,           
     isnull(rd.IDMoneda,'USD') as IDMoneda,            
     ISNULL(tox.Descripcion,'GRAVADO') as DescOperacionContab,             
     
     --ISNULL(cd.MargenAplicado,0) as Margen,      
     Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 
		ISNULL(cd.MargenAplicado,0) 
	 Else
		@MargenInterno
	 End as Margen,
     
     --Isnull(cd.NroPax,(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID)) as NroPax,       
     isnull(sd.idTipoOC,'002') AS idTipoOC,sd.TC,isnull(cv.Tarifa,0) as Tarifa,      
     (Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID) as NroPax,    
     isnull(cv.Servicio,'') as Servicio    
        
     From COTIVUELOS cv           
     Inner Join MAPROVEEDORES p On cv.IdLineaA=p.IDProveedor            
  And p.IDProveedor <> '001836'--PERURAIL            
  And cv.IDCAB=@IDCab             
  And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
     Left Join COTIDET cd On cv.IdDet=cd.IDDet and cv.TipoTransporte in('V','B')            
     Left Join MAPROVEEDORES pint On pint.IDProveedor=p.IDProveedorInternacional            
     Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det            
     Left JOIN RESERVAS_DET rd ON cd.IDDET=rd.IDDet            
     Left join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc --AND sd.idTipoOC='003'--NO GRAVADO        
     Left Join OPERACIONES_DET od On rd.IDReserva_Det=od.IDReserva_Det          
       
 Left Join MAUBIGEO ubPI On P.IDCiudad=ubPI.IDubigeo  
    Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo           
     left join coticab cc on cc.IDCAB=cv.IDCAB
       
     Where --(sd.idTipoOC='003'  )--NO GRAVADO        
     --and       
     ((isnull(cv.IdDet,0)<>0 And sd.idTipoOC='003') Or (isnull(cv.IdDet,0)=0 And cv.EmitidoSetours=1))      
     and cv.TipoTransporte in('V','B')          
     and (isnull(cv.IdDet,0)<>0 Or (isnull(cv.IdDet,0)=0 And cv.EmitidoSetours=1))    
	 and cc.estado<>'X'  
     ) as X            
    ) as Y            
   ) as Z            
  ) as XX            

--5. Interno
insert into @tabla
SELECT Total  
 FROM                    
  (                    
  SELECT                     
   ZZ.IDProveedor, ZZ.DescProveedor, ZZ.DescProveedorInternac,                     
   DescPaisProveedor,  
                     
   (SELECT Top 1--DISTINCT                      
      LTRIM(RTRIM(SUBSTRING(NoArchAdjunto,CHARINDEX('.',NoArchAdjunto,0)-13,13)))                      
       From .BDCORREOSETRA..ADJUNTOSCORREOFILE WHERE NuCorreo IN                      
      (                      
      SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE NuCorreo IN                      
       (                      
       SELECT NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE WHERE NuCorreo IN                  
        (                      
        SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'                        
        AND (SELECT Top 1 ISNULL(p1.Email3,'')+','+ISNULL(p1.Email4,'')+','+                      
        ISNULL(p1.EmailRecepcion1,'')+','+ISNULL(p1.EmailRecepcion2,'')                      
        FROM MAPROVEEDORES p1                       
         WHERE p1.IDProveedor=ZZ.IDProveedor) LIKE '%'+NoCuentaPara+'%'                      
                               
        )                      
                              
       Union                      
       SELECT NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE WHERE NuCorreo IN                      
        (                           
        SELECT NuCorreo From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'  and                       
         NoCuentaPara In (select correo from MACORREOREVPROVEEDOR where IDProveedor=ZZ.IDProveedor)                      
        )                      
       )                      
      )                       
      ) AS NroOrdenServicio,                        
                     
   ZZ.CompraNeto, ZZ.IGVCosto,                     
   ZZ.IGV_SFE, ZZ.Moneda,                     
   ZZ.CompraNeto+ZZ.IGVCosto+ZZ.IGV_SFE AS Total,                    
   ZZ.idTipoOC,                 
   ZZ.DescOperacionContab,                 
   --'GASTOS ADMINISTRATIVOS' as DescOperacionContab,                    
   ZZ.Margen,                    
   ZZ.CostoBaseUSD,                     
   ROUND( ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD                    
   --,YY.FacturaExportacion,YY.BoletaNoExportacion                    
  FROM                    
   (                    
   SELECT                     
    YY.IDProveedor, YY.DescProveedor, YY.DescProveedorInternac,                     
    DescPaisProveedor,  
    YY.CompraNeto, YY.IGVCosto,                     
    YY.IGV_SFE, YY.CompraNeto+YY.IGVCosto+YY.IGV_SFE AS Total,                    
    YY.Moneda, YY.idTipoOC, YY.DescOperacionContab, YY.Margen,                    
    YY.CompraNeto+IGVCosto+IGV_SFE as CostoBaseUSD                     
    --YY.VentaUSD,                    
    --YY.FacturaExportacion,YY.BoletaNoExportacion                    
   FROM                    
    (                    
  SELECT XX.IDProveedor, DescProveedor, DescProveedorInternac,                 
    DescPaisProveedor,     
     CompraNeto, IGVCosto,                     
     Case When idTipoOC='001' Then                     
       CompraNeto*(@IGV/100)                     
      Else                      
       Case When idTipoOC='006' Then (CompraNeto*(@IGV/100))*0.5 Else 0 End                    
      End as IGV_SFE,                     
     Moneda, idTipoOC, DescOperacionContab, Margen       
     --CostoBaseUSD, VentaUSD,                    
     --Case When XX.idTipoOC='001' Then VentaUSD Else 0 End As FacturaExportacion,                    
     --Case When XX.idTipoOC='001' Then 0 Else VentaUSD End As BoletaNoExportacion                    
    FROM                    
     (                      
     SELECT IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,  
                         
      CompraNeto, IGVCosto,Moneda,                     
      --Total,                     
      idTipoOC, DescOperacionContab, Margen                   
      --CostoBaseUSD,                    
      --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD                    
     FROM                    
      (                    
      SELECT IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,  
      SUM(CompraNeto) AS CompraNeto,                    
      SUM(IGVCosto) AS IGVCosto, Moneda,                     
      --SUM(Total) AS Total,                    
      idTipoOC, DescOperacionContab,                    
   AVG(Margen) AS Margen                    
      --SUM(Total) as CostoBaseUSD                    
      FROM                    
       (                    
                       
       SELECT X.*,  
                  
        --dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) AS Margen                
        Case When ltrim(rtrim(@CoUbigeo_Oficina))='' Then
			dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) 
        Else
			@MargenInterno
        End as Margen
        FROM                    
        (                    
      SELECT           
      isnull(P.IDProveedor,usrTrsl.IDUsuario) as IDProveedor,           
      pint.NombreCorto as DescProveedorInternac,                    
        isnull(P.NombreCorto,usrTrsl.Nombre) as DescProveedor,        
        paPI.Descripcion as DescPaisProveedor,                 
        ODS.IDOperacion_Det,                    
                
        
 Case When ODS.IngManual=0 Then               
    Case When isnull(NetoCotizado,0) = 0 then                              
           
    dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,            
    OD.NroPax+isnull(od.NroLiberados,0)),                               
    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,ods.IDMoneda,isnull(@TipoCambio,SD.TC))                       
    SD.CoMoneda,ods.IDMoneda,isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
  And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                       
    else                   
   ods.NetoCotizado        
    End               
 Else        
  ODS.NetoProgram        
 End        
        
 As CompraNeto,                                
        
                            
        --ODS.TotImpto as IGVCosto,                    
        Case When ODS.TotImpto>0 Then                             
    isnull(ODS.NetoProgram,ODS.CostoReal)*(@IGV/100)                    
  Else                
   0                
  End-- * (Case When sd.idTipoOC='005' Then 1 Else OD.NroPax End)                
  As IGVCosto,                
        ODS.IDMoneda as Moneda,                
                        
        --sd.idTipoOC,                    
        '005' As idTipoOC,          
        --tox.Descripcion as DescOperacionContab,                    
        'GASTOS ADMINISTRATIVOS' as DescOperacionContab,                    
        --SCot.IDProveedor as IDProveedorCot,                    
        --PCot.NombreCorto as DescProveedorCot,                    
        --SDCot.IDServicio_Det                    
        ODS.IDServicio_Det,                    
        O.IDProveedor as IDProveedorOpe                    
        ,SD.IDServicio                    
        FROM OPERACIONES_DET_DETSERVICIOS ODS                        
        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det                     
        AND OD.IDServicio=ODS.IDServicio                    
        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=@IDCab               
        --INNER JOIN MASERVICIOS S ON OD.IDServicio=S.IDServicio AND S.IDProveedor=O.IDProveedor                    
        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det                    
                            
        LEFT JOIN COTICAB CC ON CC.IDCAB=O.IDCab      
        LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor                    
         --INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det                        
                                 
         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Prg=SD.IDServicio_Det                    
         LEFT join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc                    
                                     
                    
        --INNER JOIN MASERVICIOS_DET SDCot ON ODS.IDServicio_Det_V_Cot=SDCot.IDServicio_Det                    
        --INNER JOIN MASERVICIOS SPrg ON SD.IDServicio=SPrg.IDServicio                    
        --INNER JOIN MAPROVEEDORES PPrg ON PPrg.IDProveedor=ODS.IDProveedor_Prg And PPrg.IDTipoProv in ('017')--TRASLADISTAS CUSCO                    
                    
                           
        LEFT JOIN MASERVICIOS SCot ON SD.IDServicio=SCot.IDServicio                    
        LEFT JOIN MAPROVEEDORES PCot ON SCot.IDProveedor=PCot.IDProveedor                     
           
 Left Join MAUBIGEO ubPI On P.IDCiudad=ubPI.IDubigeo  
    Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo           
           
                    
        Left Join MAPROVEEDORES pint On pint.IDProveedor=pCOT.IDProveedorInternacional                    
        Left Join MAUSUARIOS usrTrsl On ODS.IDProveedor_Prg=usrTrsl.IDUsuario             
        Where --not OD.IDTrasladista_Prg is null Or not OD.IDVehiculo_Prg is null                     
         (isnull(p.IDUsuarioTrasladista,'0000')<>'0000' --AND tox.IdToc='005'                  
         or isnull(usrTrsl.IDUsuario,'0000')<>'0000' --AND tox.IdToc='005'                  
         OR  (ODS.IDProveedor_Prg = '001935') --SETOURS CUSCO                      
         And ODS.Activo=1)      
         And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
		 and cc.estado<>'X'
        ) AS X                    
        where CompraNeto>0      
       ) AS Y                    
      GROUP BY IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,Moneda,idTipoOC,DescOperacionContab                    
     ) as Z                    
     ) AS XX                    
    ) AS YY                    
   ) AS ZZ                      
  ) AS A   
      


 select @SumaTotal= sum(TotalUSD) from @tabla --sum(totalusd) from @tabla
return isnull(cast(round(@SumaTotal,2) as numeric(20,2)),0)

end


