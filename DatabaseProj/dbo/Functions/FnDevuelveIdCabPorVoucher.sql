﻿create Function [dbo].[FnDevuelveIdCabPorVoucher](@IDVoucher int)  
returns int
as  
Begin  
   Declare @IdCab int=0

 set @IdCab=( select TOP 1 c.IDCAB
from DOCUMENTO_PROVEEDOR d left join OPERACIONES_DET od on od.IDVoucher=d.nuvoucher
left join operaciones o on o.IDOperacion=od.IDOperacion
left join coticab c on c.IDCAB=o.IDCab
where NuVoucher = @IDVoucher and d.IDCab is null and CardCodeSAP is null and FlActivo=1)

   return @IdCab
End;
