﻿CREATE Function [dbo].[FnDescripcionDiaSemana] (@Dia tinyint)
	Returns varchar(20) As
	Begin
	    Declare @NomDiaSem varchar(20) 
	    
	 select @NomDiaSem=  case when @Dia =1 then 'Lunes' 
			when @Dia =2 then 'Martes' 
			when @Dia =3 then 'Miércoles' 
			when @Dia =4 then 'Jueves' 
			when @Dia =5 then 'Viernes' 
			when @Dia =6 then 'Sábado' 
			when @Dia =7 then 'Domingo' 

		end
	    
	
		Return @NomDiaSem 
	End;
