﻿Create Function dbo.FnExisteNumeroEn(@CadenaEval varchar(Max),@Max int)
returns bit 
As
Begin
	Declare @ExisteNumeroCadena bit = 0
	While @Max>0 
	Begin	
		if charindex(Cast(@Max as varchar(2)),@CadenaEval) > 0
		Begin 
			Set @ExisteNumeroCadena = 1
			Set @Max = 1
		End
		Set @Max = @Max -1
	End

	return @ExisteNumeroCadena
End
