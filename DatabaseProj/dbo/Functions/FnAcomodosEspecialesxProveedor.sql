﻿--JRF-20150620-No repetir ... distinct
CREATE Function dbo.FnAcomodosEspecialesxProveedor(@IDCab int,@IDProveedor char(6))
returns @AcomodosEspeciales table (Cantidad  tinyint,Acomodo varchar(40),ItemLoc bit)
as
Begin
	Insert into @AcomodosEspeciales
	Select distinct cda.QtPax,ch.NoCapacidad,0
	from COTIDET cd Left Join COTIDET_ACOMODOESPECIAL cda On cda.IDDet=cd.IDDET
	Left Join CAPACIDAD_HABITAC ch On cda.CoCapacidad=ch.CoCapacidad
	Where IDCAB= @IDCab and IDProveedor=@IDProveedor and AcomodoEspecial=1
	return;
End
