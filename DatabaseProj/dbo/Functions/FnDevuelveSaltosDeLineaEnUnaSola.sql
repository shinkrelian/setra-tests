﻿CREATE Function dbo.FnDevuelveSaltosDeLineaEnUnaSola(@Texto varchar(Max),@IDVoucher int)
returns varchar(Max)
As
Begin
	Declare @OneLine varchar(Max)=''
	While CHARINDEX(char(13),@Texto) <> 0
	Begin
		Declare @FirsLine varchar(Max)= SUBSTRING(@Texto,1,CHARINDEX(char(13),@Texto))
		set @OneLine = @OneLine + Replace(@FirsLine,char(13),' ')
		Set @Texto = REPLACE(@Texto,@FirsLine,'')
		--set @Texto = REPLACE(@Texto,char(13),'')
	
	End

	return @OneLine+' '+ REPLACE(@Texto,@OneLine,'')
End
