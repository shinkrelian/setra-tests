﻿Create Function dbo.FnDevuelvePendienteUpdateMOperaciones(@IDCab int)
returns bit
As
Begin
	Declare @bUpdateM bit=0
	Select @bUpdateM=1 from (
	select dbo.FnCambiosenReservasPostFileOperaciones(o.IDOperacion) as LogOpe
	from OPERACIONES o Inner Join RESERVAS r On o.IDReserva=r.IDReserva
	Where o.IDCab=@IDCab And r.Anulado=0 ) As X
	Where X.LogOpe=1
	return @bUpdateM
End
