﻿Create Function dbo.FnDevuelveListaPaxxIDDet(@IDDet int)
returns varchar(Max)
As
Begin
	Declare @Nombres varchar(Max)='',@Name varchar(Max)=''
	Declare curPax cursor for
	Select cp.Apellidos + ' '+cp.Nombres as NombreCompleto
	from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax
	Where cdp.IDDet=@IDDet and cp.FlNoShow=0
	Open curPax
	Fetch Next From curPax into @Name
	Begin
		While @@FETCH_STATUS=0
		Begin
			Set @Nombres = @Nombres + @Name + char(13)
			Fetch Next From curPax into @Name
		End
	End
	Close curPax
	DealLocate curPax
	return @Nombres
End
