﻿Create Function dbo.FnDevuelveCantidadPorPagar(@IDServicio_Det int,@IDDet int)
returns smallint
As
Begin
	Declare @NroPaxPagar smallint =(select NroPax+IsNull(NroLiberados,0) from COTIDET where IDDET=@IDDet)
	Declare @TipoLib char(1) = IsNull((select TipoLib from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det),'')
	Declare @ApPolitica bit = IsNull((select PoliticaLiberado from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det),0)
	Declare @MaxLib tinyint = IsNull((select MaximoLiberado from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det),0)
	Declare @Lib tinyint = IsNull((select Liberado from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det),0)

	if @ApPolitica = 1
	Begin
	 if @MaxLib > 0 and @NroPaxPagar >= @Lib
	 Begin
	  set @NroPaxPagar = @NroPaxPagar-@MaxLib
	 End
	End
	return @NroPaxPagar
End
