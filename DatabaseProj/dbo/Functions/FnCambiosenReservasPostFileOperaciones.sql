﻿--JRF-20141014-Considerar como cambio el Costo.  
--JRF-20150224-Aumentar la longitud de TotalGen
--JRF-20150408-Agregar (..and od.IDOperacion=@IDOperacion) al SubQuery del Cursor
--HLF-20150703-	If @CambioenVentas=0
--		Set @CambioenVentas=dbo.FnExisteNuevaTarifa(@IDOperacion_Det) 
--HLF-20150710-If Exists(SELECT ods.IDServicio_Det,ods.IDServicio_Det_V_Cot,...
--HLF-20150917-((isnull(ods.IDServicio_Det_V_Cot,0)<>ISNULL(sd.IDServicio_Det_V,0)) OR SD.IDServicio_Det_V IS NULL))
--JRF-20151001-if @TipoOper='I'...if Exists(select rd.IDReserva_Det ... [Verifica que no exista ningun registro anulado en reservas_Det]
--JRF-20151006-..And @CambioenVentas=0
----JRF-20151020-... IsNull(od.FlServInManualOper,0)=0 And IsNull(od.FlServicioNoCobrado,0)=0
--JHD-20151116-... Separar funcionabilidad de comparacion de costos:  if (Cast(@TotalGen_OperacDet as Numeric(12,2)) <> Cast(@TotalGen as numeric(12,2))) And @CambioenVentas=0
--JRF-20151116-..If @TipoProv In ('002') Or Ex.. [Restaurantes no generan sub-servicios]
--PPMG-20160304-Se comento el segundo if en la diferencia de totales.
--JRF-20160305-if Not (select FeUpdateRowReservas from OPERACIONES_DET where IDOperacion_Det = @IDOperacion_Det) Is null ..
--JRF-20160405-..if @IgvGen_OperacDet <>  @IgvGen.
--JRF-20160412-..if Exists(select IDOperacion_Det...
CREATE Function [dbo].[FnCambiosenReservasPostFileOperaciones](@IDOperacion int)
returns bit
As
Begin          
 Declare @IDReserva int= (Select IDReserva from OPERACIONES where IDOperacion = @IDOperacion)      
 Declare @CantOperacionesDet tinyint = (select count(*) from OPERACIONES_DET where IDOperacion= @IDOperacion and IDReserva_Det is Not Null)          
 Declare @CantReservasdet tinyint = (select count(*) from RESERVAS_DET where IDReserva=@IDReserva and Anulado=0)            
 Declare @CambioenVentas bit = 0,@IDReserva_Det int,@Dia smalldatetime,@Servicio varchar(Max),          
         @NroPax tinyint,@CantPagar int,@IDServicio_Det int,@IDIdioma varchar(12),@TotalGen numeric(12,4)=0,@IgvGen numeric(12,4)    
 Declare @TipoOper char(1)='',@TipoProv char(3)=''
 Select @TipoOper=p.IDTipoOper,@TipoProv=p.IDTipoProv
 from OPERACIONES o Left Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
 Where IDOperacion=@IDOperacion

 if @TipoOper='I'
 Begin	
	if Exists(select rd.IDReserva_Det
	from OPERACIONES_DET od Left Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det
	where IDOperacion=@IDOperacion And rd.Anulado=1 And (od.FlServInManualOper=0 Or od.FlServicioNoCobrado=0))
		Set @CambioenVentas = 1
 End

 if((@CantReservasdet <> @CantOperacionesDet) and @CantReservasdet > 0)
 Begin      
  set @CambioenVentas = 1          
  goto FinCursor      
 End      
 else             

  Declare curItemsReservaDet cursor for          
  select Rd.IDReserva_Det,Rd.Dia,          
  Case When (p.IDTipoProv = '001' Or (p.IDTipoProv='003' and sd.ConAlojamiento=1))          
    then sd.Descripcion else Rd.Servicio End as Servicio,          
      Rd.CantidadAPagar as NroPax,Rd.IDServicio_Det,Rd.IDIdioma,Rd.TotalGen,Rd.IgvGen 
  from RESERVAS_DET Rd         
  Left Join MASERVICIOS_DET sd On Rd.IDServicio_Det=sd.IDServicio_Det          
  Left Join RESERVAS r On Rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor          
  Left Join OPERACIONES_DET od On Rd.IDReserva_Det=od.IDReserva_Det  
  where Rd.IDReserva = @IDReserva and Rd.Anulado = 0 
  And IsNull(od.FlServInManualOper,0)=0 And IsNull(od.FlServicioNoCobrado,0)=0  --and od.FeUpdateRowReservas is not null
  Order by Dia          
  Open curItemsReservaDet          
  Begin          
   Fetch Next From curItemsReservaDet into @IDReserva_Det,@Dia,@Servicio,@NroPax,@IDServicio_Det,@IDIdioma,@TotalGen,@IgvGen
   While @@FETCH_STATUS=0          
   Begin        
         
    Declare @IDReservaDet_OperacDet int=0,@Dia_OperacDet smalldatetime='01/01/1900',@Servicio_OperacDet varchar(Max)='',          
    @NroPax_OperacDet tinyint=0,@IDServicio_Det_OperacDet int=0,@IDIdioma_OperacDet varchar(12)='',@TotalGen_OperacDet numeric(12,4)=0,@IDOperacion_Det int=0,
	@IgvGen_OperacDet numeric(12,4)=0
             
  select @IDReservaDet_OperacDet=od.IDReserva_Det,@Dia_OperacDet=od.Dia,      
  @Servicio_OperacDet=Case When (p.IDTipoProv = '001' Or (p.IDTipoProv='003' and sd.ConAlojamiento=1))then sd.Descripcion else od.Servicio End,          
  @NroPax_OperacDet=od.CantidadAPagar,@IDServicio_Det_OperacDet=od.IDServicio_Det,@IDIdioma_OperacDet=od.IDIdioma,  
  @TotalGen_OperacDet=od.TotalGen,  
  @IDOperacion_Det=od.IDOperacion_Det,
  @IgvGen_OperacDet = od.IgvGen
  from OPERACIONES_DET od      
  Left Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det          
  Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion Left Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor          
  where od.IDReserva_Det = @IDReserva_Det and od.IDOperacion=@IDOperacion --and od.FeUpdateRowReservas is not null  
       
  --set @CambioenVentas = '('+@Servicio+')'+'['+@Servicio_OperacDet+']'+@CambioenVentas      
  if @CambioenVentas =0      
  Begin      
    if @IDReservaDet_OperacDet = 0    
      set @CambioenVentas=1       
    if (@Servicio_OperacDet <> @Servicio)      
     set @CambioenVentas=1      
    if (@IDIdioma_OperacDet <> @IDIdioma)      
     set @CambioenVentas=1      
    if (@IDServicio_Det_OperacDet <> @IDServicio_Det) And @CambioenVentas=0 
		Begin
     	If @TipoProv In ('002') Or Exists(SELECT ods.IDServicio_Det FROM OPERACIONES_DET_DETSERVICIOS ods 
			inner join MASERVICIOS_DET sd on ods.IDServicio_Det=sd.IDServicio_Det
			WHERE ods.IDOperacion_Det=@IDOperacion_Det And  
			--isnull(ods.IDServicio_Det_V_Cot,0)<>sd.IDServicio_Det_V)
			((isnull(ods.IDServicio_Det_V_Cot,0)<>ISNULL(sd.IDServicio_Det_V,0)) OR SD.IDServicio_Det_V IS NULL))
			set @CambioenVentas=0  
		Else
			set @CambioenVentas=1
		End
    if (@NroPax_OperacDet <> @NroPax)      
     set @CambioenVentas=1       
    if (convert(char(10),@Dia_OperacDet,103)<>convert(char(10),@Dia,103))      
     set @CambioenVentas=1       
  if (@TipoProv='001'  )
  Begin
	 if @IgvGen_OperacDet <>  @IgvGen
		Set @CambioenVentas = 1
  End
  if (Cast(@TotalGen_OperacDet as Numeric(12,2)) <> Cast(@TotalGen as numeric(12,2))) And @CambioenVentas=0
		Begin
		-->PPMG20160304
		--If @TipoProv In ('002') Or Exists(SELECT ods.IDServicio_Det FROM OPERACIONES_DET_DETSERVICIOS ods 
		--	inner join MASERVICIOS_DET sd on ods.IDServicio_Det=sd.IDServicio_Det
		--	WHERE ods.IDOperacion_Det=@IDOperacion_Det And  
		--	((isnull(ods.IDServicio_Det_V_Cot,0)<>ISNULL(sd.IDServicio_Det_V,0)) OR SD.IDServicio_Det_V IS NULL))
		--	set @CambioenVentas=0  
		--Else
		    if Not (select FeUpdateRowReservas from OPERACIONES_DET where IDOperacion_Det = @IDOperacion_Det) Is null 
				set @CambioenVentas=1
		End
	If @CambioenVentas=0
		Set @CambioenVentas=dbo.FnExisteNuevaTarifa(@IDOperacion_Det) 
  End    
          
    Fetch Next From curItemsReservaDet into @IDReserva_Det,@Dia,@Servicio,@NroPax,@IDServicio_Det,@IDIdioma,@TotalGen,@IgvGen
   End           
  Close curItemsReservaDet          
  DealLocate curItemsReservaDet        

  if Exists(select IDOperacion_Det
			from OPERACIONES_DET od Inner Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det
			Where od.IDOperacion=@IDOperacion And rd.Anulado=0 And od.Dia <> rd.Dia)
  Begin
	Set @CambioenVentas = 1
  End

        
FinCursor:         
 End
  
  --Select @CambioenVentas
  return @CambioenVentas          
End   
