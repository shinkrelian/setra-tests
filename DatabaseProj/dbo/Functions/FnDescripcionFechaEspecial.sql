﻿--JRF-20140626-Ajustar Convert(char(10),Fecha,103) -Se debe comparar cadenas
CREATE Function dbo.FnDescripcionFechaEspecial(@Fecha smalldatetime,@IDUbigeo char(6))  
returns varchar(Max)  
As  
Begin  
 Declare @Descripcion varchar(Max)=''  
 Declare @FechaTmp1 char(10) = Convert(char(5),@Fecha,103)+'/1900'  
 Declare @FechaTmp2 char(10) = Convert(char(10),@Fecha,103)   
 If Exists(select Fecha from MAFECHASNAC where Convert(char(10),Fecha,103) = @FechaTmp1 and IDUbigeo = @IDUbigeo)  
 Begin  
  Set @Descripcion = (select Descripcion from MAFECHASNAC where Convert(char(10),Fecha,103) = @FechaTmp1 and IDUbigeo = @IDUbigeo)  
  Goto Retornar  
 End  
   
 If Exists(select Fecha from MAFECHASNAC where Convert(char(10),Fecha,103) = @FechaTmp2 and IDUbigeo = @IDUbigeo)  
 Begin  
  Set @Descripcion = (select Descripcion from MAFECHASNAC where Convert(char(10),Fecha,103) = @FechaTmp2 and IDUbigeo = @IDUbigeo)  
  Goto Retornar  
 End  
  
    Retornar:  
 Return @Descripcion  
End  
