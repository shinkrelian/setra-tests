﻿--JHD-20140901-Se cambio tipo de dato @MontoSegunTC a numeric(12,4)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Function dbo.FnDevuelveCostoConSubServicioNetoHabGenTemp(@IDDet int,@NroPax tinyint,@IDMoneda varchar(3),@HotelPlanAlimenticio bit)
returns numeric(12,4)
As
Begin
	Declare @CostoReturn numeric(12,4)=0
	Declare @IDServicio_Det int = 0,@CoMoneda varchar(3)='',@MontoReal numeric(12,4)=0,@MontoSegunTC numeric(12,4)=0,
			@MontoCostoReal numeric(12,4)=0,@TCambio numeric(12,4)=0,@Afecto bit=0,@PoliticaLib bit=0,@CantMinLiberada tinyint=0,
			@TipoLib varchar(10)='',@CantidadLiberada tinyint=0,@MontoLiberado numeric(12,4)=0,@DiferSS numeric(12,4)=0,
			@DiferST numeric(12,4)=0--,@CostoxPax numeric(12,4)=0
	
	if @HotelPlanAlimenticio=1
	Begin
	Declare curCostosDet1 cursor for
	select  cds.IDServicio_Det,sd.CoMoneda,IsNull(sd.Monto_sgl,0) as MontoReal,IsNull(sd.Monto_sgls,0) as MontoSegunTC,
			IsNull((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det 
			 and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) as MontoCostoReal,
			 Isnull(IsNull(sd.TC,(select tc.SsTipCam from MATIPOSCAMBIO_USD tc where CoMoneda=@IDMoneda)),0) as TC,
			 sd.Afecto,sd.PoliticaLiberado,IsNull(sd.Liberado,0) CantMinLiberada,IsNuLL(sd.TipoLib,'') As TipoLib,
			 IsNull(sd.MaximoLiberado,0) as CantidadLiberada,IsNull(sd.MontoL,0) as MontoLiberado,
			 IsNull(sd.DiferSS,0) as DiferSS,IsNull(sd.DiferST,0) as DiferST
	from COTIDET cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det
	where IDDET=@IDDet and CostoReal >0
	Open curCostosDet1
		Fetch Next From curCostosDet1 into @IDServicio_Det,@CoMoneda,@MontoReal,@MontoSegunTC,@MontoCostoReal,@TCambio,@Afecto,@PoliticaLib,@CantMinLiberada,@TipoLib,@CantidadLiberada,@MontoLiberado,@DiferSS,@DiferST
		While @@FETCH_STATUS=0
		Begin
		Declare @MontoSuma numeric(12,4)=0
		if @MontoCostoReal > 0
			set @MontoSuma = @MontoCostoReal /  @NroPax
		else
		Begin
			if @MontoSegunTC > 0
				set @MontoSuma = @MontoSegunTC
			else
				Set @MontoSuma = @MontoReal
		End
		
		if ((@CoMoneda='ARS' Or @CoMoneda='CLP' Or @CoMoneda='SOL') And @IDMoneda='USD') --and @TCambio > 0
			Set @MontoSuma = @MontoSuma / @TCambio
		if @IDMoneda <> 'USD' And @CoMoneda ='USD'
			Set @MontoSuma = @MontoSuma * @TCambio

		
		Set @CostoReturn = @CostoReturn+@MontoSuma
		Fetch Next From curCostosDet1 into @IDServicio_Det,@CoMoneda,@MontoReal,@MontoSegunTC,@MontoCostoReal,@TCambio,@Afecto,@PoliticaLib,@CantMinLiberada,@TipoLib,@CantidadLiberada,@MontoLiberado,@DiferSS,@DiferST
	End
	Close curCostosDet1
	DealLocate curCostosDet1
	End
	else
	Begin
	Declare curCostosDet cursor for
	select  cds.IDServicio_Det,sd.CoMoneda,IsNull(sd.Monto_sgl,0) as MontoReal,IsNull(sd.Monto_sgls,0) as MontoSegunTC,
		IsNull((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det 
		 and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) as MontoCostoReal,
		 Isnull(IsNull(sd.TC,(select tc.SsTipCam from MATIPOSCAMBIO_USD tc where CoMoneda=@IDMoneda)),0) as TC,
		 sd.Afecto,sd.PoliticaLiberado,IsNull(sd.Liberado,0) CantMinLiberada,IsNuLL(sd.TipoLib,'') As TipoLib,
		 IsNull(sd.MaximoLiberado,0) as CantidadLiberada,IsNull(sd.MontoL,0) as MontoLiberado,
		 IsNull(sd.DiferSS,0) as DiferSS,IsNull(sd.DiferST,0) as DiferST
	from COTIDET_DETSERVICIOS cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det
	where IDDET=@IDDet and CostoReal > 0
	Open curCostosDet
		Fetch Next From curCostosDet into @IDServicio_Det,@CoMoneda,@MontoReal,@MontoSegunTC,@MontoCostoReal,@TCambio,@Afecto,@PoliticaLib,@CantMinLiberada,@TipoLib,@CantidadLiberada,@MontoLiberado,@DiferSS,@DiferST
		While @@FETCH_STATUS=0
		Begin
		Declare @MontoSuma2 numeric(12,4)=0
		if @MontoCostoReal > 0
			set @MontoSuma2 = @MontoCostoReal /  @NroPax
		else
		Begin
			if @MontoSegunTC > 0
				set @MontoSuma2 = @MontoSegunTC
			else
				Set @MontoSuma2 = @MontoReal
		End
		
		if (@CoMoneda='ARS' Or @CoMoneda='CLP' Or @CoMoneda='SOL') And @IDMoneda='USD'
			Set @MontoSuma2 = @MontoSuma2 / @TCambio
		if @IDMoneda <> 'USD' And @CoMoneda ='USD'
			Set @MontoSuma2 = @MontoSuma2 * @TCambio

		
		Set @CostoReturn = @CostoReturn+@MontoSuma2
		Fetch Next From curCostosDet into @IDServicio_Det,@CoMoneda,@MontoReal,@MontoSegunTC,@MontoCostoReal,@TCambio,@Afecto,@PoliticaLib,@CantMinLiberada,@TipoLib,@CantidadLiberada,@MontoLiberado,@DiferSS,@DiferST
	End
	Close curCostosDet
	DealLocate curCostosDet
	End
	
	return @CostoReturn
End

