﻿Create function [dbo].[FnTotalFilePreLiquidacion](@IDCab int, @CoUbigeo_Oficina char(6))
Returns numeric(13,2)
as

Begin
--ROUND( ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD      
	Declare @TotalRet numeric(13,2)=0
	Declare @MargenInterno as numeric(6,2)=10
	Declare @CoProvSetours char(6)='', @CoProvTransporte char(6)=''
	If @CoUbigeo_Oficina='000113' 
		Begin
		Set @CoProvSetours='002315'--Setours Buenos Aires
		Set @CoProvTransporte='002329'--Transporte Buenos Aires 
		End

	SELECT  @TotalRet+=Round((isnull(SUM(
	dbo.FnCambioMoneda(OD2.TotalGen,od2.IDMoneda,'USD',	
	isnull(tca.SsTipCam,sd.TC)
	)),0))/(1-(@MargenInterno/100)),0)
	
				FROM OPERACIONES_DET OD2 INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab
				INNER JOIN MAPROVEEDORES P2 ON O2.IDProveedor=P2.IDProveedor AND P2.CoUbigeo_Oficina=@CoUbigeo_Oficina
				Left Join MASERVICIOS_DET sd on sd.IDServicio_Det=od2.IDServicio_Det
				LEft Join COTICAB_TIPOSCAMBIO tca On tca.IDCab=o2.IDCab And tca.CoMoneda=od2.IDMoneda
				WHERE OD2.IDVoucher<>0 AND P2.IDTipoOper='E'

	SELECT @TotalRet+=Round((ISNULL(SUM(dbo.FnCambioMoneda(ODS2.Total,ods2.IDMoneda,'USD',isnull(tca.SsTipCam,sd2.TC))),0))/(1-(@MargenInterno/100)),0)
				FROM OPERACIONES_DET_DETSERVICIOS ODS2 
				INNER JOIN OPERACIONES_DET OD2 ON ODS2.IDOperacion_Det=OD2.IDOperacion_Det AND OD2.IDServicio=ODS2.IDServicio
				INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab AND 
					(O2.IDProveedor=@CoProvSetours Or ltrim(rtrim(@CoProvSetours))='')
				INNER JOIN MASERVICIOS_DET SD2 ON ODS2.IDServicio_Det_V_Cot=SD2.IDServicio_Det
				INNER JOIN MASERVICIOS S2 ON SD2.IDServicio=S2.IDServicio 
				INNER JOIN MAPROVEEDORES P2 ON S2.IDProveedor=P2.IDProveedor AND P2.IDProveedor<>@CoProvTransporte 
				LEft Join COTICAB_TIPOSCAMBIO tca On tca.IDCab=o2.IDCab And tca.CoMoneda=ods2.IDMoneda
				WHERE ODS2.IDOperacion_Det IN(SELECT IDOperacion_Det FROM OPERACIONES_DET WHERE IDOperacion=O2.IDOperacion and IDVoucher<>0)
				
	SELECT @TotalRet+=Round((ISNULL(SUM(ODS2.Total),0))/(1-(@MargenInterno/100)),0)
				FROM OPERACIONES_DET_DETSERVICIOS ODS2 
				INNER JOIN OPERACIONES_DET OD2 ON ODS2.IDOperacion_Det=OD2.IDOperacion_Det AND OD2.IDServicio=ODS2.IDServicio
				AND not ODS2.IDProveedor_Prg is null 
				INNER JOIN MAPROVEEDORES PPRG ON ODS2.IDProveedor_Prg=PPRG.IDProveedor  and not PPRG.NombreCorto is null                  
					And isnull(PPRG.IDUsuarioTrasladista,'0000')='0000'              
					AND pPRG.CoUbigeo_Oficina=@CoUbigeo_Oficina
         And ODS2.IDProveedor_Prg <> '001935'--SETOURS CUSCO           
         And ODS2.Activo=1-- and ODS.TotalProgram>0          
				INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab 
				INNER JOIN ORDEN_SERVICIO OS ON OS.IDCab=O2.IDCab AND O2.IDProveedor=OS.IDProveedor
				

	--OPERACIONES_SelEntradasPresupuestosPreLiquidacion
	SELECT @TotalRet+=Round((ISNULL(SUM(
	
	Case When ODS2.IngManual=0 Then               

		Case When isnull(NetoCotizado,0) = 0 then                              

			dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD2.IDServicio_Det,OD2.NroPax+isnull(od2.NroLiberados,0)),  
			SD2.CoMoneda,'USD',isnull(tca.SsTipCam,SD2.TC))    
		else   
			dbo.FnCambioMoneda(NetoCotizado,ODS2.IDMoneda,'USD',isnull(tca.SsTipCam,SD2.TC))
		End
	Else
		dbo.FnCambioMoneda(NetoProgram,ODS2.IDMoneda,'USD',isnull(tca.SsTipCam,SD2.TC))                     
	End        	
	),0))/(1-(@MargenInterno/100)),0)
	FROM OPERACIONES_DET_DETSERVICIOS ODS2 
	INNER JOIN OPERACIONES_DET OD2 ON ODS2.IDOperacion_Det=OD2.IDOperacion_Det AND OD2.IDServicio=ODS2.IDServicio
	INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab 					
	INNER JOIN MASERVICIOS_DET SD2 ON ODS2.IDServicio_Det_V_Cot=SD2.IDServicio_Det
	INNER JOIN MASERVICIOS S2 ON SD2.IDServicio=S2.IDServicio 
	INNER JOIN MAPROVEEDORES P2 ON S2.IDProveedor=P2.IDProveedor 
	And (P2.IDTipoProv in ('006') OR P2.IDProveedor='001593' ) --MUSEO  O CONSETTUR TRANSPORTISTA          
	And P2.CoUbigeo_Oficina=@CoUbigeo_Oficina 
	LEft Join COTICAB_TIPOSCAMBIO tca On tca.IDCab=o2.IDCab And tca.CoMoneda=ods2.IDMoneda
	WHERE ODS2.IDOperacion_Det IN(SELECT IDOperacion_Det FROM OPERACIONES_DET WHERE IDOperacion=O2.IDOperacion and IDVoucher<>0)


	--COTIVUELOS_SelBusesVuelosPreLiquidacion

	

    SELECT @TotalRet+=Round((ISNULL(SUM(
	 Case When isnull(CostoOperador,0)=0 Then      
		isnull(TotalGen,(Tarifa*CantPax))        
     Else
		CostoOperador*CantPax
     End
	 ),0))/(1-(@MargenInterno/100)),0)
	FROM
	(
	SELECT CV.CostoOperador, CV.Tarifa, od.TotalGen,
	(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID) AS CantPax
	From COTIVUELOS cv           
	Inner Join MAPROVEEDORES p On cv.IdLineaA=p.IDProveedor            
	And cv.IDCAB=@IDCab             
	And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
	Left Join COTIDET cd On cv.IdDet=cd.IDDet and cv.TipoTransporte in('V','B')
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	Left JOIN RESERVAS_DET rd ON cd.IDDET=rd.IDDet   
	Left Join OPERACIONES_DET od On rd.IDReserva_Det=od.IDReserva_Det        
	Where 
     ((isnull(cv.IdDet,0)<>0 And sd.idTipoOC='003') Or (isnull(cv.IdDet,0)=0 And cv.EmitidoSetours=1))      
     and cv.TipoTransporte in('V','B')          
     and (isnull(cv.IdDet,0)<>0 Or (isnull(cv.IdDet,0)=0 And cv.EmitidoSetours=1))  
	) AS X


	--OPERACIONES_SelTraladistasyMovilidadPreLiquidacion

	SELECT @TotalRet+=Round((ISNULL(SUM(ODS2.Total),0))/(1-(@MargenInterno/100)),0)
				FROM OPERACIONES_DET_DETSERVICIOS ODS2 
				INNER JOIN OPERACIONES_DET OD2 ON ODS2.IDOperacion_Det=OD2.IDOperacion_Det AND OD2.IDServicio=ODS2.IDServicio
				AND not ODS2.IDProveedor_Prg is null 
				INNER JOIN MAPROVEEDORES PPRG ON ODS2.IDProveedor_Prg=PPRG.IDProveedor  and not PPRG.NombreCorto is null                  
					And isnull(PPRG.IDUsuarioTrasladista,'0000')='0000'              
					AND pPRG.CoUbigeo_Oficina=@CoUbigeo_Oficina         
         And ODS2.Activo=1-- and ODS.TotalProgram>0          
				INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab 
				Left Join MAUSUARIOS usrTrsl On ODS2.IDProveedor_Prg=usrTrsl.IDUsuario            
		Where 
         (isnull(PPRG.IDUsuarioTrasladista,'0000')<>'0000' 
         or isnull(usrTrsl.IDUsuario,'0000')<>'0000'          
         And ODS2.Activo=1)      
         And (PPRG.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')				


	Return @TotalRet
End

