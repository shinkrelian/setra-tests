﻿--JRF-20141105-(Inner Join cotidet cd On rd.IDDet = cd.IDDet) en @RowCountReservaDet            
--HLF-20141125-if @CostoReal <> (Select CostoReal from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0)      
--JRF-20141204-Cambio de datos FecMod [smalldatetime >> datetime]      
--JRF-20141205-Ajustar el cambio de NroPax      
--JRF-JHD-20150205-Adicionando el valor del día    
--JRF-Adicionar la condición si se ingreso manualmente.  
--JRF-20150414-..and rd.IDReserva=@IDReserva
--JRF-20150529-Adaptar los cambios de vehiculos
--JRF-20150907-Verificar los liberados.
--JRF-20150908-Considera los registros eliminados
--JRF-20150908-Verificación del Igv para PlanAlimenticio
--JRF-20150910-@COUNT5... And FlServicioIngManual=0
--JRF-20151006-Considerar la eliminación completa...
--JRF-20151120-Considerar Viaticos
--JRF-20160309-..if @IDProveedor = '001554' And Exists(select .... @bCambiosVentas = 0
--JRF-20160403-..Set @bCambiosVentas = dbo.FnDevuelveCambiosVentasPostReservasDetaPax(@IDReserva)
--JRF-20160413-..select @bCambiosVentas = 1   from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDET...
--PPMG-20160414- Mejoras al Script (Optimización)
--JRF-20160429-((@RowCountCotiDet <> @RowCountReservaDet) And @RowCountReservaDet > 0) And @bCambiosVentas = 0
CREATE Function [dbo].[FnCambiosenVentasPostFileReservasSoloServicios](@IDReserva int)
returns bit
Begin              
 Declare @bCambiosVentas bit = 0            
 Declare @IDProveedor char(6)='',@IDCab int=0,@RowCountCotiDet int = 0   
 Declare @TipoProv varchar(3)=''         
 Select @IDProveedor=r.IDProveedor,@IDCab=r.IDCab,@TipoProv=p.IDTipoProv
 from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
 Where r.IDReserva =@IDReserva and r.Anulado = 0 and r.Estado <> 'XL'            
 Declare @IDDet int=0,@Dia smalldatetime='01/01/2000',@Servicio varchar(Max)='',@NroPax int=0,@IDServicio_Det int=0,@IDIdioma varchar(12)='',            
 @FecMod datetime='01/01/1900',@IDReserva_Det int=0,@IDReservaTmp int=0,@RowCountCotiDetTemp int = 0,@bTransfer bit = 0,@CostoReal numeric(12,4)=0,
 @CantResDel tinyint=0,@CantidadaPagar int=0
                  
 Declare @RowCountReservaDet int = (select count(*) from (            
            select Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
            then 1 else 0 End As ServicioHotespedaje from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det            
            Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
            Left Join cotidet cd On rd.IDDet = cd.IDDet          
            Where rd.IDReserva=@IDReserva and rd.Anulado=0 and rd.FlServicioIngManual=0 and r.Estado<> 'XL' And rd.IDReserva_DetOrigAcomVehi is null) as X            
            Where X.ServicioHotespedaje=0)            
                        
 Declare @bExisteProveedor bit = 0            
 If Exists(select IDDET from COTIDET where IDProveedor=@IDProveedor and IDCAB=@IDCab)            
  Set @bExisteProveedor = 1            
                    

--Select @IDProveedor,@IDCab,@bExisteProveedor
 if (@IDProveedor <> '' and @TipoProv <> '') and @IDCab <> 0 and @bExisteProveedor=1
 Begin            
  Set @RowCountCotiDet = (Select count(*) from (            
        Select cd.IDDet,cd.Dia,cd.Servicio,cd.NroPax,cd.IDServicio_Det,cd.IDIdioma,cd.FecMod,--rd.IDReserva_Det,            
            r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
            then 1 else 0 End As ServicioHotespedaje             
        from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
        Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det                
        Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'         
        --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva            
        Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and r.Anulado= 0          
        ) as X          
        where X.ServicioHotespedaje = 0)            
  
  Set @CantResDel = (select Count(*)from RESERVAS_DET rd Left Join COTIDET cd On rd.IDDet=cd.IDDet
					 Where IDReserva=@IDReserva And FlServicioIngManual=0 and Anulado=0 And cd.IDDet is Null And rd.IDReserva_Det_Rel is null)
  --select @CantResDel,@RowCountReservaDet

  if (IsNull(@CantResDel,0)>0) --And (IsNull(@CantResDel,0)=@RowCountReservaDet Or @RowCountReservaDet=0))
  Begin
	Set @bCambiosVentas = 1
  End

  --select @bCambiosVentas as Before
  --If Exists(select IDDET from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
		--	where IDCAB=@IDCab and cd.IDProveedor=@IDProveedor And IDDetRel is null And p.IDTipoProv <> '001'
		--	And Not Exists(select IDReserva_Det from RESERVAS_DET rd where rd.IDReserva=@IDReserva and rd.IDDet=cd.IDDET))
--PPMG20160414
  If Exists(select cd.IDDET from COTIDET cd inner join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
			left outer join (SELECT IDDet FROM RESERVAS_DET WHERE IDReserva=@IDReserva GROUP BY IDDet) rd on cd.IDDET = rd.IDDet
			where cd.IDCAB=@IDCab and cd.IDProveedor=@IDProveedor And cd.IDDetRel is null And p.IDTipoProv <> '001'
			And rd.IDDet is null)
  Begin
	Set @bCambiosVentas = 1
  End

  --Validar cambios de Horario
  select @bCambiosVentas = 1 
  from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDET
  Inner Join RESERVAS r On rd.IDReserva=r.IDReserva
  Where rd.IDReserva=@IDReserva And r.Anulado= 0 And rd.FeUpdateRow is not null And rd.Dia <> cd.Dia

  if @bCambiosVentas= 0
  Begin
		--PPMG20160414
		DECLARE @NroPaxTmp as int, @ServicioTmp as varchar(max), @IDIdiomaTmp as varchar(12), @CostoRealTmp as numeric(12,2), @DiaTmp as char(10), @CantidadaPagarTmp as int
	  --Evaluación si Existe La reserva.            
	  Declare curItemsExist cursor for            
	  Select distinct        
	   X.IDDET,X.Dia,X.Servicio,X.NroPax,X.IDServicio_Det,X.IDidioma,X.FecMod,X.IDReserva_Det,X.IDReserva,            
		  0 As CantidadCotiDet,X.Transfer, X.CostoReal ,X.CantidadPagar2  
	  from (            
	  Select cd.IDDet,cd.Dia,cd.Servicio,Case When cd.IncGuia = 1 then 1 Else cd.NroPax+IsNull(cd.NroLiberados,0) End as NroPax,cd.IDServicio_Det,      
		  Case When p.IDProveedor In('000545','001836') and Exists(select ID from cotivuelos cv Where cv.idcab=cd.IDCab and cv.IDDet=cd.IDDet)      
		  Then 'NO APLICA' Else cd.IDIdioma End as IDIdioma,cd.FecMod,      
	   rd.IDReserva_Det,r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
		  then 1 else 0 End As ServicioHotespedaje,cd.Transfer,        
	   Case When cd.CostoRealEditado=0 Then      
	  dbo.FnDevuelveCostoConSubServicioNetoHabGenTemp(cd.IDDet,cd.NroPax+IsNull(cd.NroLiberados,0),rd.IDMoneda,      
	  Case When p.IDTipoProv='001' and sd.PlanAlimenticio=1 Then 1 Else 0 End) Else      
		 cd.CostoReal * IsNull(sd.TC,1) End      
	   as CostoReal  , Case When cd.IncGuia=1 then rd.CantidadAPagar else dbo.FnDevuelveCantidadPorPagar(rd.IDServicio_Det,rd.IDDET) End as CantidadPagar2 
	  from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
	  Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det                
	  Left Join RESERVAS_DET rd On rd.IDDet= cd.IDDet and rd.IDReserva=@IDReserva           
	  Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'      
	  --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva            
	  Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and rd.Anulado =0  and r.Anulado=0 and IsNull(rd.FlServicioIngManual,0)=0      
		 and rd.FeUpdateRow is not null And rd.IDReserva_DetOrigAcomVehi is null
	  ) as X            
	  where X.ServicioHotespedaje = 0      
	  --Group By X.IDDET,X.Dia,X.Servicio,X.NroPax,X.IDServicio_Det,X.IDidioma,X.FecMod,X.IDReserva_Det,X.IDReserva,X.Transfer            
              
	  Open curItemsExist            
	  Fetch Next From curItemsExist into @IDDet,@Dia,@Servicio,@NroPax,@IDServicio_Det,@IDIdioma,@FecMod,@IDReserva_Det,@IDReservaTmp,@RowCountCotiDetTemp,@bTransfer,@CostoReal,@CantidadaPagar
	  While @@Fetch_Status=0            
	  Begin            
	   --Select @FecMod,FeUpdateRow from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det 
	   --Select datediff(s,cast('12/02/2014' as smalldatetime),cast('28/02/2014' as smalldatetime))     
	   --Select Case When FeUpdateRow is null then -1 else datediff(S,@FecMod,FeUpdateRow) End from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det      
   
	   --Select @IDReserva_Det
	   --Select @CostoReal,CostoReal,Servicio,@bCambiosVentas from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0      
	   If @bCambiosVentas= 0            
	   Begin            
			if Isnull(@IDReserva_Det,0)=0            
			Begin            
			 Set @bCambiosVentas = 1             
			End            
			else            
			Begin            
			--Select FeUpdateRow,* from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det
			Declare @intDateDiff integer = (Select Case When FeUpdateRow is not null then DATEDIFF(ss,FeUpdateRow,@FecMod) Else 0 End 
											from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det)
			--select @bCambiosVentas,@NroPax,(Select NroPax+ISNULL(NroLiberados,0) from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0)
			--select @intDateDiff
			 --Select @intDateDiff,NroPax+ISNULL(NroLiberados,0) from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0
			 if (@intDateDiff <> 0)      
			 Begin      
		   --Select @IDIdioma,IDIdioma from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0
				--PPMG20160414
				SELECT @NroPaxTmp = NULL, @ServicioTmp = NULL, @IDIdiomaTmp = NULL, @CostoRealTmp = NULL, @DiaTmp = NULL, @CantidadaPagarTmp = NULL
				SELECT @NroPaxTmp = NroPax+ISNULL(NroLiberados,0),
					   @ServicioTmp = Servicio,
					   @IDIdiomaTmp = ltrim(rtrim(IDIdioma)),
					   @CostoRealTmp = Cast(NetoHab as  Numeric(12,2)),
					   @DiaTmp = Convert(char(10),dia,103),
					   @CantidadaPagarTmp = Cast(CantidadAPagar as  int)
				FROM RESERVAS_DET WHERE IDReserva_Det = @IDReserva_Det AND anulado = 0

			  If Not Exists(Select IDReserva_Det from RESERVAS_DET Where IDReserva=@IDReserva and anulado=0 and IDDet= @IDDet)      
			   Set @bCambiosVentas = 1            
			  --if @NroPax <> (Select NroPax+ISNULL(NroLiberados,0) from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0)            
			  if @NroPax <> @NroPaxTmp
			   Set @bCambiosVentas = 1            
			  --if (@Servicio <> (Select Servicio from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0)) and @bTransfer=0            
			  if (@Servicio <> @ServicioTmp) and @bTransfer=0
			   Set @bCambiosVentas = 1            
			  --if ltrim(rtrim(@IDIdioma))<>(Select ltrim(rtrim(IDIdioma)) from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0)      
			  if ltrim(rtrim(@IDIdioma))<>@IDIdiomaTmp
			   Set @bCambiosVentas = 1            
			  if @IDServicio_Det <> (Select IDServicio_Det from RESERVAS_DET where IDReserva_Det=@IDReserva_Det)            
			   Set @bCambiosVentas = 1            
			  --if(Cast(@CostoReal as Numeric(12,2))<>(Select  Cast(NetoHab as  Numeric(12,2)) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0))      
			  if(Cast(@CostoReal as Numeric(12,2))<>@CostoRealTmp)
			   Set @bCambiosVentas = 1       
			  --if (Convert(char(10),@Dia,103)<>(Select  Convert(char(10),dia,103) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0))    
			  if (Convert(char(10),@Dia,103)<>@DiaTmp)
			   Set @bCambiosVentas = 1
			  --if (@CantidadaPagar <> (Select  Cast(CantidadAPagar as  int) from RESERVAS_DET where IDReserva_Det=@IDReserva_Det and anulado=0))
			  if (@CantidadaPagar <> @CantidadaPagarTmp)
			   Set @bCambiosVentas = 1     

			 End

			 
			 --select @bCambiosVentas
			 --Select @NroPax,NroPax+ISNULL(NroLiberados,0) from RESERVAS_DET Where IDReserva_Det=@IDReserva_Det and anulado=0
			End
		--if @bCambiosVentas=1
		--  Select @Servicio,Cast(@CostoReal as Numeric(12,2)),@CostoRealTmp
	   End   
	  
	   --Set @RowCountCotiDet = (@RowCountCotiDetTemp+1)+@RowCountCotiDet            
               
	   Fetch Next From curItemsExist into @IDDet,@Dia,@Servicio,@NroPax,@IDServicio_Det,@IDIdioma,@FecMod,@IDReserva_Det,@IDReservaTmp,@RowCountCotiDetTemp,@bTransfer,@CostoReal,@CantidadaPagar
	  End            
	  Close curItemsExist            
	  DealLocate curItemsExist     

  End       
  
  if @TipoProv = '001' and @bCambiosVentas=0
  Begin

	Declare @CountReservasDet4 int = (select count(*) from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
									 where IDReserva=@IDReserva and Anulado=0 and sd.PlanAlimenticio=1 And FlServicioIngManual=0)
	Declare @CountReservasDet5 int = (select count(*) from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
									  Inner Join COTIDET cd On rd.IDDet=cd.IDDET
									  where IDReserva=@IDReserva and Anulado=0 and sd.PlanAlimenticio=1)
	--select rd.Servicio,rd.*
	--from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	--where IDReserva=40834 and Anulado=0 and sd.PlanAlimenticio=1
	if @CountReservasDet4 <> @CountReservasDet5
	Begin
		Set @bCambiosVentas=1
	End
	else
	Begin
		if Exists(select FeUpdateRow from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
			where IDReserva=@IDReserva and Anulado=0 and sd.PlanAlimenticio=1 And rd.FeUpdateRow is not null And (rd.IgvHab=0 and sd.Afecto=1))
			Set @bCambiosVentas = 1
	End
  End


  --select @bCambiosVentas
  if @bCambiosVentas= 0
  Begin
	Declare @ExisteAcomodoVNT bit = 0
	Declare @IDDetVeh int = 0,@NuVehiculo smallint = 0,@TotalPaxVeh tinyint = 0
	--Select av. *
	--from COTIDET cd Inner Join ACOMODO_VEHICULO av On cd.IDDET=av.IDDet
	--Where cd.IDCAB=@IDCab And cd.IDProveedor=@IDProveedor

	Select @ExisteAcomodoVNT = 1
	from COTIDET cd Inner Join ACOMODO_VEHICULO av On cd.IDDET=av.IDDet
	Where cd.IDCAB=@IDCab And cd.IDProveedor=@IDProveedor

	--Select @ExisteAcomodoVNT as AcomodoV
	If @ExisteAcomodoVNT = 1
	Begin
		--select rd.IDReserva_Det,@bCambiosVentas from RESERVAS_DET rd 
		--Where rd.IDReserva=@IDReserva And rd.Anulado=0 And rd.IDReserva_DetOrigAcomVehi is not null
		If Not Exists(select rd.IDReserva_Det from RESERVAS_DET rd 
					  Where rd.IDReserva=@IDReserva And rd.Anulado=0 And rd.IDReserva_DetOrigAcomVehi is not null) --and FeUpdateRow is not null)
		Begin
			Set @bCambiosVentas=1
		End
		Else
		Begin
			Declare curTransporte cursor for
			Select av.IDDet,av.NuVehiculo,Sum(av.QtPax*av.QtVehiculos) as TotalPax
			from COTIDET cd Inner Join ACOMODO_VEHICULO av On cd.IDDET=av.IDDet
			Where cd.IDCAB=@IDCab And cd.IDProveedor=@IDProveedor
			Group By av.IDDet,av.NuVehiculo
			Open curTransporte
			Fetch Next From curTransporte into @IDDetVeh,@NuVehiculo,@TotalPaxVeh
			While @@FETCH_STATUS=0
			Begin
				if @bCambiosVentas = 0
				Begin
					if @TotalPaxVeh <> (Select sum(Cantidad) from RESERVAS_DET where NuVehiculo=@NuVehiculo And IDDet=@IDDetVeh And Anulado=0)
					Begin
						Set @bCambiosVentas=1
					End
				End

				Fetch Next From curTransporte into @IDDetVeh,@NuVehiculo,@TotalPaxVeh
			End
			Close curTransporte
			DealLocate curTransporte

			--select rd.* from RESERVAS_DET rd 
			--Where rd.IDReserva=@IDReserva And rd.Anulado=0 And rd.IDReserva_DetOrigAcomVehi is not null	
		End
		
	End
	Else
	Begin
		If Exists(select rd.IDReserva_Det from RESERVAS_DET rd 
				  Where rd.IDReserva=@IDReserva And rd.Anulado=0 And rd.IDReserva_DetOrigAcomVehi is not null) --and FeUpdateRow is not null)
		Set @bCambiosVentas = 1
	End
  End
 End
 
 if @IDProveedor = '001554' And Exists(select IDCab from COTICAB_TOURCONDUCTOR where IDCab=@IDCab) And @bCambiosVentas = 0
 Begin
	Set @bCambiosVentas = dbo.FnCambioenViaticosVentasAReservas(@IDReserva)
 End

 if @bCambiosVentas = 0
 Begin
	Set @bCambiosVentas = dbo.FnDevuelveCambiosVentasPostReservasDetaPax(@IDReserva)
 End
              
 --select @RowCountCotiDet,@RowCountReservaDet,@bCambiosVentas
 if ((@RowCountCotiDet <> @RowCountReservaDet) And @RowCountReservaDet > 0) And @bCambiosVentas = 0
 Begin            
  Set @bCambiosVentas=1            
 End            
             
 FinFuncion:
 
 --select @bCambiosVentas as CompareResult
 return @bCambiosVentas              
End      
