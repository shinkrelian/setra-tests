﻿CREATE FUNCTION [dbo].[FnDocumento_GetTexto_Concatenado](@IDTipoDoc as char(3), @NuDocum as char(10)) 
RETURNS varchar(500) AS 
BEGIN 
--declare
--	@IDTipoDoc char(3)='TIP',
--	@NuDocum char(10)='15110198'
--select * from DOCUMENTO_DET_TEXTO where NuDocum=@NuDocum and IDTipoDoc= @IDTipoDoc

declare @notexto as varchar(max)=''
declare @NoTexto_Concat as varchar(max)=''
Declare curDocs cursor for 
select notexto from DOCUMENTO_DET_TEXTO where NuDocum=@NuDocum and IDTipoDoc= @IDTipoDoc
Open curDocs
Begin
	Fetch Next From curDocs into @NoTexto
	While @@FETCH_STATUS=0
	Begin
		set @NoTexto_Concat=rtrim(ltrim(@NoTexto_Concat))+ ' '+rtrim(ltrim(@NoTexto))
	Fetch Next From curDocs into @NoTexto
	End
End
CursorEnd:
Close curDocs
DealLocate curDocs;

RETURN substring(@notexto,0,50)
END
