﻿--JRF-20150526-Agregar el filtro de estado.. (... and Estado <> 'X')
CREATE Function dbo.FnExisteTarifasDif(@IDDet int)
returns bit
As
Begin
	Declare @ExisteTrfDist bit =0,@Variante varchar(7)='',@Anio char(4),@IDServicio char(8)=''
	Declare @IDServicio_Det int = 0,@IDTipoProv char(3)='',@IDDetRel int=0
	select @IDServicio_Det=IDServicio_Det ,@IDTipoProv = p.IDTipoProv,@IDDetRel=IsNull(cd.IDDetRel,0)
	from COTIDET cd Left Join MAPROVEEDORES p on cd.IDProveedor=p.IDProveedor
	where IDDET=@IDDet


	Select @IDServicio = IDServicio,@Anio=Anio,@Variante=Ltrim(Rtrim(Tipo))
	from MASERVICIOS_DET
	where IDServicio_Det=@IDServicio_Det and Estado <> 'X'

	--Select @IDServicio
	If Ltrim(Rtrim(@IDServicio)) <> ''
	Begin
		If Exists(select IDServicio_Det from COTIDET_DETSERVICIOS where IDDET=@IDDet)
		Begin
		  If Exists(Select cds.IDServicio_Det from COTIDET_DETSERVICIOS cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det = sd.IDServicio_Det
					Where cds.IDDET=@IDDet And (sd.IDServicio <> @IDServicio Or sd.Anio <> @Anio Or Ltrim(Rtrim(sd.Tipo)) <> @Variante))
		  Begin
			 Set @ExisteTrfDist = 1
		  End
		End
		Else
		Begin
			If @IDTipoProv <> '001' And @IDDetRel = 0
			  Begin
					Set @ExisteTrfDist = 1
			  End
			Else
			Begin
				if @IDTipoProv='001'
				Begin
					if Exists(select IDServicio_Det from COTIDET_DETSERVICIOS where IDDET=@IDDet)
					Begin
						Set @ExisteTrfDist = 1	
					End
				End
			End
		End
	End

	--select @ExisteTrfDist
	return @ExisteTrfDist
End
