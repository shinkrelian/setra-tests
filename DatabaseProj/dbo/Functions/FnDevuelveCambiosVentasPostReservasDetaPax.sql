﻿--JRF-20160412-...Inner Join COTIPAX cp On cdp.IDPax=cp.IDPax ... FlNoShow = 0
--JRF-20160413-...And rd.FeUpdateRow is not null.. omitir registros ya actualizados
CREATE Function dbo.FnDevuelveCambiosVentasPostReservasDetaPax(@IDReserva int)
returns bit
As
Begin
Declare @IDReserva_Det int=0,@IDDet int=0
Declare @ExisteCambio bit = 0
Declare @TmpPaxFaltantes table (IDPax int,Loc bit)

Declare curDet cursor for
select IDReserva_Det,rd.IDDet from COTIDET cd Inner Join RESERVAS_DET rd On rd.IDDet=cd.IDDET And rd.IDServicio_Det=cd.IDServicio_Det
Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
Inner Join RESERVAS r On rd.IDReserva=r.IDReserva
Inner Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
where rd.IDReserva=@IDReserva  And rd.Anulado=0 And r.Anulado = 0
And FlServicioIngManual=0 And FlServicioParaGuia=0 And IsNull(cd.IncGuia,0)=0 --And cd.IDdet=856746
And cd.IncGuia=0 And Not ((p.IDTipoProv='001' And sd.PlanAlimenticio=0) Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1))
And rd.FlServicioTC=0 And IsNull(rd.NuViaticoTC,0) = 0 And rd.FeUpdateRow is not null
Open curDet
Fetch Next from curDet into @IDReserva_Det,@IDDet
While @@FETCH_STATUS = 0
Begin
	Delete from @TmpPaxFaltantes

	Insert into @TmpPaxFaltantes 
	Select cdp.IDPax,1 from COTIDET_PAX cdp Inner Join COTIPAX cp On cdp.IDPax=cp.IDPax where IDDet=@IDDet And cp.FlNoShow=0

	--Select IDPax from COTIDET_PAX where IDDet=@IDDet
	--Select rd.IDPax from RESERVAS_DET_PAX rd Where IDReserva_Det=@IDReserva_Det

	Update @TmpPaxFaltantes Set Loc=0
	Where IDPax In (Select rd.IDPax from RESERVAS_DET_PAX rd Inner Join COTIPAX cp On rd.IDPax=cp.IDPax Where IDReserva_Det=@IDReserva_Det And cp.FlNoShow=0)

	--select * from @TmpPaxFaltantes
	If Exists(select IDPax from @TmpPaxFaltantes Where Loc=1) And @ExisteCambio=0
	Begin
		--Select @IDDet
		Set @ExisteCambio = 1
	End
	Fetch Next from curDet into @IDReserva_Det,@IDDet
End

Close curDet
DealLocate curDet
--select @ExisteCambio

Return @ExisteCambio
End
