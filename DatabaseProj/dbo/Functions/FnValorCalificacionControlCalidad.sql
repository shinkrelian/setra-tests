﻿Create Function dbo.FnValorCalificacionControlCalidad(@NuControlCalidad int,
													  @NuCuest tinyint,
													  @IDProveedor char(6))
Returns tinyint
Begin
	Declare @ValReturn tinyint=0
	Select 
	@ValReturn = Case When FlExcelent = 1 Then 5 Else 
					Case When FlVeryGood = 1 Then 4 Else
						Case When FlGood = 1 then 3 Else
							Case When FlAverage = 1 Then 2 Else
								1
							End
						End
					End
				 End
	from CONTROL_CALIDAD_PRO_MACUESTIONARIO
	Where NuControlCalidad=@NuControlCalidad And NuCuest=@NuCuest And IDProveedor=@IDProveedor
	
	Return @ValReturn
End
