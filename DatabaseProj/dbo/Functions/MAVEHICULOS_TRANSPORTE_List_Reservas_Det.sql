﻿CREATE function [dbo].[MAVEHICULOS_TRANSPORTE_List_Reservas_Det]  
(@idreserva_Det as int)
returns int
--declare @idreserva_Det int=171085,
as
begin
--@FlOtraCiudad bit  
declare
@CoUbigeo char(6),
@IDTipoServ char(3),
@CoTipo smallint,
@CoProveedor char(6)='',
@Tipo varchar(7)='',--'',
@IDUbigeoOri char(6)='',
@IDUbigeoDes char(6)='',
@IDCliente char(6)='',

@contador as int=0

select @CoUbigeo=rd.IDubigeo,@IDTipoServ=(Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End),@CoTipo=case when rd.Transfer=1 then 2 else 3 end,
@coproveedor=s.IDProveedor,@Tipo=sd.Tipo,@IDUbigeoOri=rd.IDUbigeoOri,@IDUbigeoDes=rd.IDUbigeoDes,@IDCliente=(select idcliente from coticab where idcab=r.IDCab)
from RESERVAS_DET rd
inner join reservas r on r.IDReserva=rd.IDReserva
inner join MASERVICIOS_DET sd on rd.IDServicio_Det=sd.IDServicio_Det
inner join maservicios s on s.IDServicio=sd.IDServicio
 where IDReserva_Det=@idreserva_Det

--select @CoUbigeo,@IDTipoServ,@CoTipo,@coproveedor,@Tipo,@IDUbigeoOri,@IDUbigeoDes,@IDCliente

--select distinct tipo from MASERVICIOS_DET

--As  
 DECLARE @CoPais char(6)
select @CoPais=IDPais from MAUBIGEO where IDubigeo=@CoUbigeo

BEGIN
if (@CoPais='000323')  -- peru
	if @CoProveedor in ('000544','001554') --SETOURS CUSCO Y LIMA --and (@IDTipoServ='PVT' OR @CoTipo=3)
	begin
		
		--if ((@IDTipoServ='PVT' AND @COTIPO=2) OR (@CoTipo=3 and @CoUbigeo in ('000066','000068')))
		--begin
			select @CoUbigeo = case when @CoProveedor='000544' then '000066' when  @CoProveedor='001554' then '000065' else @CoUbigeo end
			select @CoTipo = case when (@IDUbigeoOri in ('000066','000068') and @IDUbigeoDes in ('000066','000068') /*and @CoTipo=2*/) then 3 else ( case when @CoTipo=3 then 2 else @CoTipo end) end
			
			--select * from (
			select @contador = count(*) from (
			select 0 as NuVehiculo,'' AS Descripcion,0 as QtDe
			Union
			Select 
			NuVehiculo,
			NoVehiculoCitado+' ('+cast(isnull(QtDe,0) as varchar(2)) +' - '+cast(isnull(QtHasta,0) as varchar(2))  +') Pax - Cap. '+ case when Cast(isnull(QtCapacidad,'') as Varchar(2)) IN ('','0') then 'TBA' ELSE Cast(isnull(QtCapacidad,'') as Varchar(2)) END  As

 Descripcion ,isnull(QtDe,0) as QtDe
			
			from MAVEHICULOS_TRANSPORTE  
			--where CoUbigeo  in ('000066') /*and CoUbigeo=@CoUbigeo*/   and CoTipo=@CoTipo
			where CoUbigeo = @CoUbigeo /*and CoUbigeo=@CoUbigeo*/   and CoTipo=@CoTipo
			--and ((@tipo='GEB' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB') and CoMercado is null))
			--and ((@tipo in ('GEB','GER') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			--and NuVehiculo_Anterior is null
			and LTRIM(RTRIM(@IDTipoServ)) in ('PVT','OT')
			and flActivo=1
			and FlReservas=1
			)as X
			--Order By X.QtDe 
		--end	
	end
	else
	begin
		    --select @CoUbigeo = case when @CoProveedor='000544' then '000066' when  @CoProveedor='001554' then '000065' else @CoUbigeo end
			select @contador = count(*) from (
			select 0 as NuVehiculo,'' AS Descripcion,0 as QtDe
			Union
			Select 
			NuVehiculo,
			NoVehiculoCitado+' ('+cast(isnull(QtDe,0) as varchar(2)) +' - '+cast(isnull(QtHasta,0) as varchar(2))  +') Pax - Cap. '+ case when Cast(isnull(QtCapacidad,'') as Varchar(2)) IN ('','0') then 'TBA' ELSE Cast(isnull(QtCapacidad,'') as Varchar(2)) END  As

 Descripcion ,isnull(QtDe,0) as QtDe
			from MAVEHICULOS_TRANSPORTE  
			--where CoUbigeo  in ('000066') /*and CoUbigeo=@CoUbigeo*/   and CoTipo=@CoTipo
			where CoUbigeo is null --= @CoUbigeo and CoTipo=@CoTipo
			--and ((@tipo='GEB' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB') and CoMercado is null))
			--and ((@tipo in ('GEB','GER') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			and LTRIM(RTRIM(@IDTipoServ)) in ('PVT','OT')
		--	and NuVehiculo_Anterior is null
		and flActivo=1
		and FlReservas=1

			)as X
			--Order By X.QtDe 
		--end	
	end
else
	begin

		if @CoTipo=3
				set @cotipo=1

		--select @CoUbigeo = case when @CoProveedor='000544' then '000066' when  @CoProveedor='001554' then '000065' else @CoUbigeo end
		select @contador = count(*) from (
		select 0 as NuVehiculo,'' AS Descripcion,0 as QtDe
		Union
		Select 
		NuVehiculo,
		NoVehiculoCitado+' ('+cast(isnull(QtDe,0) as varchar(2)) +' - '+cast(isnull(QtHasta,0) as varchar(2))  +') Pax - Cap. '+ case when Cast(isnull(QtCapacidad,'') as Varchar(2)) IN ('','0') then 'TBA' ELSE Cast(isnull(QtCapacidad,'') as Varchar(2)) END  As Descripcion ,
		isnull(QtDe,0) as QtDe
		from MAVEHICULOS_TRANSPORTE  
		--where CoUbigeo  in ('000066') /*and CoUbigeo=@CoUbigeo*/   and CoTipo=@CoTipo
		where CoUbigeo = @CoUbigeo /*and CoUbigeo=@CoUbigeo*/   and CoTipo=@CoTipo
		and flActivo=1
		and FlReservas=1
		--and ((@tipo='GEB' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB') and CoMercado is null))
		)as X
		--Order By X.QtDe 
	end

return @contador
end
END;
