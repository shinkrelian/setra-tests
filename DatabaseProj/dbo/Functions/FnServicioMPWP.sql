﻿--JRF-20150430-Adicionar (CUZ00145) >> Raqchi
--JRF-20150506-Adicionar (CUZ00905) >> Mountain
--JRF-20150508-Adicionar (CUZ00912) >> Museum , (CUZ00946) >> Mountain Supplement
CREATE Function dbo.FnServicioMPWP(@IDServicio_Det int)
	returns char(8)
As
	Begin
	
	Declare @IDServicioMPWP char(8)='', @IDServicio char(8)='', @Variante varchar(7)='', @Anio char(4)=''
	
	SELECT @IDServicio=IDServicio,@Variante=Tipo,@Anio=Anio FROM MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det	

	SELECT Top 1 @IDServicioMPWP=IDServicio FROM MASERVICIOS_DET where IDServicio_Det in(SELECT IDServicio_Det_V 
		FROM MASERVICIOS_DET where IDServicio=@IDServicio and Anio=@Anio and tipo=@Variante)
			And IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00912','CUZ00946')

	Return @IDServicioMPWP
	End
