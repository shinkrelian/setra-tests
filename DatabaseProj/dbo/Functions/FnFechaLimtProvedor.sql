﻿
--JRF-20150309-Considerar la fecha de Inicio de los servicio.  
--JRF-20150313-En caso de No encontrar tarifa para el año indicado, tomar la del año anterior  
--JRF-20150317-Considerar el tipo de Politica (Hab,pax)
--JRF-20151223-Considerar el Mayor Prepago
--HLF-20160128-Declare @AnioServMin smallint=isnull(	....   If @AnioServ>=@AnioServMin
CREATE Function dbo.FnFechaLimtProvedor(@IDProveedor char(6),@Pax smallint,@RoomBit bit,@AnioServ char(4),@IDCab int)
returns smallint      
As
Begin      
    declare @Menor smallint = 256,@ExistePol bit=0,@TipoPol char(3)=''
    declare @MayorRooming smallint = 0,@MayorPrepago smallint = 0      
    declare @RoomPreeliminar smallint=0,@RoomActualizado smallint=0,@RoomFinal smallint=0,      
    @PrePag1_Dias smallint=0,@PrePag2_Dias smallint=0,@Saldo_Dias smallint=0  
  
  Declare @HabitxIDCab tinyint=0

  Declare @AnioServMin smallint=isnull((select min(anio) from MAPOLITICAPROVEEDORES where IDProveedor = @IDProveedor ),0)
  
  If @AnioServMin>0
	Begin
	Encontro:
	  select Top(1) @TipoPol = DefinTipo from MAPOLITICAPROVEEDORES where IDProveedor = @IDProveedor and Anio=@AnioServ

	  if Ltrim(Rtrim(@TipoPol))='' And Exists(select DefinTipo from MAPOLITICAPROVEEDORES where IDProveedor = @IDProveedor)
	  Begin
		set @AnioServ= Cast(Cast(@AnioServ as smallint)-1 as char(4))
		If @AnioServ>=@AnioServMin
			goto Encontro
	  End
	End
  if Ltrim(Rtrim(@TipoPol))='HAB'
  Begin
	Set @HabitxIDCab =(select IsNull(Simple,0)+IsNull(Twin,0)+IsNull(Matrimonial,0)+IsNull(Triple,0)+
					          IsNull(SimpleResidente,0)+IsNull(TwinResidente,0)+IsNull(MatrimonialResidente,0)+IsNull(TripleResidente,0)
			   from COTICAB Where IDCab=@IDCab)
	Set @Pax = @HabitxIDCab
  End

  select @ExistePol=1,@RoomPreeliminar=RoomPreeliminar,@RoomActualizado=RoomActualizado,@RoomFinal=RoomFinal,      
   @PrePag1_Dias=PrePag1_Dias,@PrePag2_Dias=PrePag2_Dias,@Saldo_Dias=Saldo_Dias,@MayorRooming=DiasSinPenalidad
  from MAPOLITICAPROVEEDORES       
  where IDProveedor = @IDProveedor and Anio=@AnioServ and @Pax between DefinDia and DefinHasta     
    
  --select @RoomPreeliminar=RoomPreeliminar,@RoomActualizado=RoomActualizado,@RoomFinal=RoomFinal,      
  --  @PrePag1_Dias=PrePag1_Dias,@PrePag2_Dias=PrePag2_Dias,@Saldo_Dias=Saldo_Dias,@MayorRooming=DiasSinPenalidad,
  --  @TipoPol = DefinTipo
  -- from MAPOLITICAPROVEEDORES       
  -- where IDProveedor = @IDProveedor and Anio=Cast(Cast(@AnioServ as int)-1 as char(4))   
  -- and @Pax between DefinDia and DefinHasta

  --if IsNull(@ExistePol,0)=0  
  --Begin  
  --    select Top(1) @TipoPol = DefinTipo
	 -- from MAPOLITICAPROVEEDORES where IDProveedor = @IDProveedor and Anio=Cast(Cast(@AnioServ as int)-1 as char(4))
	 -- if Ltrim(Rtrim(@TipoPol))='HAB'
	 -- Begin
		--Set @HabitxIDCab =(select IsNull(Simple,0)+IsNull(Twin,0)+IsNull(Matrimonial,0)+IsNull(Triple,0)+
		--						  IsNull(SimpleResidente,0)+IsNull(TwinResidente,0)+IsNull(MatrimonialResidente,0)+IsNull(TripleResidente,0)
		--		   from COTICAB Where IDCab=@IDCab)
		--Set @Pax = @HabitxIDCab
	 -- End
   
  -- select @RoomPreeliminar=RoomPreeliminar,@RoomActualizado=RoomActualizado,@RoomFinal=RoomFinal,      
  --  @PrePag1_Dias=PrePag1_Dias,@PrePag2_Dias=PrePag2_Dias,@Saldo_Dias=Saldo_Dias,@MayorRooming=DiasSinPenalidad,
  --  @TipoPol = DefinTipo
  -- from MAPOLITICAPROVEEDORES       
  -- where IDProveedor = @IDProveedor and Anio=Cast(Cast(@AnioServ as int)-1 as char(4))   
  -- and @Pax between DefinDia and DefinHasta
  --End  
    
     
  ----Obteniendo Los Mayores Rooming y de Prepagos.      
  --if(@RoomPreeliminar > @MayorRooming) set @MayorRooming  = @RoomPreeliminar      
  --if(@RoomActualizado > @MayorRooming) set @MayorRooming  = @RoomActualizado      
  --if(@RoomFinal > @MayorRooming) set @MayorRooming  = @RoomFinal      
        
  if(@PrePag1_Dias > @MayorPrepago) set @MayorPrepago = @PrePag1_Dias      
  if(@PrePag2_Dias > @MayorPrepago) set @MayorPrepago = @PrePag2_Dias      
  if(@Saldo_Dias > @MayorPrepago) set @MayorPrepago = @Saldo_Dias   
  if(@RoomBit=1) 
   Set @MayorPrepago = @RoomFinal
         
  --Obteniendo el menor de ambos.      
  if(@RoomBit=1) set @Menor = @MayorRooming else set @Menor = @MayorPrepago    
      
  --select (@Menor * -1)
  return (@Menor * -1)
 End      

