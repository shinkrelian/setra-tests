﻿--JHD-20151204 - Se cambió el cálculo para obtener la categoría predominante.
CREATE Function [dbo].[FnDevolverCategoriaPredominante_File]       
(@IDFile char(8))      
-- declare @IDFile char(8)='16010104'
Returns char(10)
AS
 BEGIN

declare @descripcion as char(10)
declare @promedio as integer

declare @tabla_pesos as table
(
idprovedor char(6),
qtpeso decimal(8,2)
)

declare @dias as table
(
idprovedor char(6),
dias int
)

declare @tabla_pesos_dias as table
(
idprovedor char(6),
dias int,
qtpeso decimal(8,2)
)


declare @tabla_pesos2 as table
(
qtpeso decimal(8,2),
diferencia int
)

insert into @tabla_pesos
select DISTINCT p.IDProveedor,C.QTPESO from cotidet cd
inner join MASERVICIOS s on s.IDServicio=cd.IDServicio
inner join MAPROVEEDORES p on p.IDProveedor=s.IDProveedor
inner join MACATEGORIA c on c.IDCat=s.idcat
where cd.idcab=(select top 1 idcab from coticab where IDFile=@idfile) and p.IDTipoProv='001'

insert into @dias
select DISTINCT p.IDProveedor,count(cd.dia) as dias from cotidet cd
inner join MAPROVEEDORES p on p.IDProveedor=cd.IDProveedor
where cd.idcab=(select top 1 idcab from coticab where IDFile=@idfile) and p.IDTipoProv='001'
group by p.IDProveedor

--select * from @dias

insert into @tabla_pesos_dias
select tp.idprovedor,td.dias,tp.qtpeso
from @tabla_pesos tp
inner join @dias td on tp.idprovedor=td.idprovedor

--select * from @tabla_pesos_dias

set @promedio= (select case when isnull(sum(dias),0)>0 then sum(dias*qtpeso)/sum(dias) else 0 end from @tabla_pesos_dias)

/*
set @promedio= (select DISTINCT CASE WHEN (sum((cd.NroPax+isnull(cd.NroLiberados,0))))=0 THEN 0 ELSE (sum(C.QTPESO*(cd.NroPax+isnull(cd.NroLiberados,0)))/sum((cd.NroPax+isnull(cd.NroLiberados,0)))) END as promedio from cotidet cd
inner join MASERVICIOS s on s.IDServicio=cd.IDServicio
inner join MAPROVEEDORES p on p.IDProveedor=s.IDProveedor
inner join MACATEGORIA c on c.IDCat=s.idcat
where cd.idcab=(select idcab from coticab where IDFile=@idfile) and p.IDTipoProv='001')
*/

--select * from @tabla_pesos
--select @promedio

insert into @tabla_pesos2
select qtpeso,diferencia=min(abs(qtpeso-@promedio))
from @tabla_pesos
group by qtpeso

--select * from @tabla_pesos2

select top 1 @descripcion=c.Descripcion/*, diferencia,p.qtpeso*/ from
@tabla_pesos2 p
inner join MACATEGORIA c on c.qtpeso=p.qtpeso
order by diferencia 

RETURN isnull(@descripcion,'')
--select isnull(@descripcion,'')
END;
