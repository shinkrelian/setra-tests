﻿
CREATE Function [dbo].[FnDestinatariosCorreo](@NuCorreo int, @FlCuentaCopia	bit)
	Returns varchar(Max)	
As
	Begin
	Declare @NoCuentaPara varchar(150)
	Declare @Destinatarios varchar(Max)=''
	
	Declare curCorreos cursor For
	Select NoCuentaPara 
	From .BDCORREOSETRA..DESTINATARIOSCORREOFILE
	Where NuCorreo=@NuCorreo And FlCuentaCopia=@FlCuentaCopia
	
	Open curCorreos
	Fetch Next From curCorreos Into @NoCuentaPara
	While @@Fetch_Status=0
		Begin
		Set @Destinatarios+=@NoCuentaPara+'; '
		Fetch Next From curCorreos Into @NoCuentaPara
		End
	Close curCorreos
	Deallocate curCorreos
	
	If ltrim(rtrim(@Destinatarios))<>'' 
		Begin
		If Right(ltrim(rtrim(@Destinatarios)),1)=';' 
		Set  @Destinatarios=Left(@Destinatarios,Len(@Destinatarios)-1)
		End
	Return @Destinatarios 
	End