﻿Create Function dbo.FnDevuelveCadenaHotelServiciosAdicionales(@IDDet int,@IDIdioma varchar(12))
returns varchar(Max)
as
Begin
	Declare @DescripComida varchar(Max)='',@Desayuno bit=0,@BoxLunch bit=0,@Almuerzo bit=0,@Cena bit=0
	Select @Desayuno=Desayuno,@BoxLunch=Lonche,@Almuerzo=Almuerzo,@Cena=Cena
	from COTIDET
	Where IDDet=@IDDet
	
	if Not(@Desayuno=0 and @BoxLunch=0 and @Almuerzo=0 and @Cena=0)
	Begin
		Set @DescripComida= 'Incluye: '
		if @Desayuno=1
			Set @DescripComida = @DescripComida + 'Desayuno,'
		if @BoxLunch=1
			Set @DescripComida = @DescripComida + 'Box Lunch,'
		if @Almuerzo=1
			Set @DescripComida = @DescripComida + 'Almuerzo,'
		if @Cena=1
			Set @DescripComida = @DescripComida + 'Cena,'
	
		if Ltrim(Rtrim(@IDIdioma))='fr-FR'
		Begin
			Set @DescripComida= 'Comprend: '
			if @Desayuno=1
				Set @DescripComida = @DescripComida + 'Petit-déjeuner,'
			if @BoxLunch=1
				Set @DescripComida = @DescripComida + 'Box Lunch,'
			if @Almuerzo=1
				Set @DescripComida = @DescripComida + 'Déjeuner,'
			if @Cena=1
				Set @DescripComida = @DescripComida + 'Dîner,'
		End
		
		if Ltrim(Rtrim(@IDIdioma))='en-US'
		Begin
			Set @DescripComida= 'Include: '
			if @Desayuno=1
				Set @DescripComida = @DescripComida + 'Breakfast,'
			if @BoxLunch=1
				Set @DescripComida = @DescripComida + 'Box Lunch,'
			if @Almuerzo=1
				Set @DescripComida = @DescripComida + 'Lunch,'
			if @Cena=1
				Set @DescripComida = @DescripComida + 'Dinner,'
		End
		
		if Ltrim(Rtrim(@IDIdioma))='de-DE'
		Begin
			Set @DescripComida= 'Inklusive: '
			if @Desayuno=1
				Set @DescripComida = @DescripComida + 'Frühstück,'
			if @BoxLunch=1
				Set @DescripComida = @DescripComida + 'Box Lunch,'
			if @Almuerzo=1
				Set @DescripComida = @DescripComida + 'Mittagessen,'
			if @Cena=1
				Set @DescripComida = @DescripComida + 'Abendessen,'
		End
		
		if Ltrim(Rtrim(@IDIdioma))='pt-BR'
		Begin
			Set @DescripComida= 'Inclui: '
			if @Desayuno=1
				Set @DescripComida = @DescripComida + 'Café da manhã,'
			if @BoxLunch=1
				Set @DescripComida = @DescripComida + 'Merendeira,'
			if @Almuerzo=1
				Set @DescripComida = @DescripComida + 'Almoço,'
			if @Cena=1
				Set @DescripComida = @DescripComida + 'Jantar,'
		End
		
		if @DescripComida <> ''
			Set @DescripComida = substring(@DescripComida,1,Len(@DescripComida)-1)
	End
	
	Return @DescripComida
End;
