﻿Create Function dbo.FnDevuelveVehiculoxIDDet(@IDDet int,@IDServicio_Det int)
returns varchar(200)
As
Begin
	Declare @NombreVehiculo varchar(200)=''
	Declare @IDTipoOper char(1)=''

	select @IDTipoOper=p.IDTipoOper
	from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	where IDDET=@IDDet And IsNull(cd.IDDetRel,0)=0

	Declare @IDReserva_Det int = 0
	If @IDTipoOper='I'
	Begin
		Set @IDReserva_Det = (select distinct IDReserva_Det from RESERVAS_DET where IDDet=@IDDet And Anulado=0)
	End
	Else
	Begin 
		Set @IDReserva_Det = (select distinct IDReserva_Det from RESERVAS_DET where IDDet=@IDDet And IDServicio_Det=@IDReserva_Det And Anulado=0)
	End

	Select @NombreVehiculo = IsNull(vt.NoVehiculoCitado + ' ('+ Cast(vt.QtCapacidad as varchar(2))+')','')
	from RESERVAS_DET rd Left Join MAVEHICULOS_TRANSPORTE vt On rd.NuVehiculo=vt.NuVehiculo
	where IDReserva_Det=@IDReserva_Det

	return @NombreVehiculo
End
