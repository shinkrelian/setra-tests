﻿--JHD-20160307- select @SumaTotal= IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0) FROM
CREATE function dbo.FnDevolverTotal_Voucher_CotiCab
  (
@idcab int,
@CoUbigeo_Oficina char(6)
)
Returns numeric(20,6)  
as
begin
 declare @SumaTotal numeric(20,6)=0
 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)
 Declare @MargenInterno as numeric(6,2)=10

--SELECT @SumaTotal=IsNull(Sum(Total),0) from
select @SumaTotal= IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0) FROM
  (
  SELECT 
    IDProveedor,IDVoucher,CompraNeto,
    IGVCosto,IGV_SFE,Moneda, CompraNetoUSD+IGVCostoUSD+IGV_SFEUSD AS Total, idTipoOC, Margen, CompraNetoUSD+IGVCostoUSD AS CostoBaseUSD, 
	ROUND((CompraNetoUSD+IGVCostoUSD) /(1 - (Margen/100)) ,0) AS VentaUSD FROM 
   (
      SELECT YY.IDProveedor,  
	     YY.IDVoucher,YY.CompraNeto, CompraNetoUSD, YY.IGVCosto, IGVCostoUSD, 
		 Case When YY.idTipoOC='001' Then  YY.CompraNeto*(@IGV/100) Else Case When YY.idTipoOC='006' Then (YY.CompraNeto*(@IGV/100))*0.5 Else 0 End End as IGV_SFE,
		 Case When YY.idTipoOC='001' Then  YY.CompraNetoUSD*(@IGV/100)  Else  Case When YY.idTipoOC='006' Then (YY.CompraNetoUSD*(@IGV/100))*0.5 Else 0 End
		 End as IGV_SFEUSD,  yy.Moneda, idTipoOC, YY.Margen

   
   FROM

    (

    SELECT XX.*


    FROM

     (

     SELECT Z.*, 

 

     Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 

		dbo.FnPromedioPonderadoMargen(@IDCab, Z.IDProveedor) 

	 Else

		@MargenInterno

	 End AS Margen

    

     FROM

      (

      SELECT IDProveedor,IDVoucher,

      

      CompraNeto as CompraNeto,   

      dbo.FnCambioMoneda(CompraNeto,IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Y.IDMoneda),TC)) as CompraNetoUSD,   

     

      IGVCosto as IGVCosto,   

      dbo.FnCambioMoneda(IGVCosto,IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Y.IDMoneda),TC)) as IGVCostoUSD,   

 
      IDMoneda As Moneda , 

      idTipoOC

      FROM 

(



SELECT IDProveedor ,IDVoucher,

(SUM(CostoReal)) as CompraNeto, 
      

SUM(TotImpto) as IGVCosto,     
  

idTipoOC,IDMoneda,TC   

FROM

 (

 SELECT  o.IDProveedor,

 OD.IDVoucher,

 Case When SD.ConAlojamiento=1 Or p.IDProveedor='001836' Then

	   od.NetoGen 

	  Else 

	   Case When sd.idTipoOC In ('006','001') Then--Proporcion o Exportacion 

		od.NetoGen 

	   else 
	    Case When @CoUbigeo_Oficina<>'000113' Then 
			od.TotalGen			
		Else--
			
			Case when o.IDProveedor='002315' Then --Setours Buenos Aires
				(SELECT isnull(sum(dbo.FnCambioMoneda(ODS2.Total,ods2.IDMoneda,'USD',isnull(tca.SsTipCam,sd2.TC))),0)
				FROM OPERACIONES_DET_DETSERVICIOS ODS2 
				INNER JOIN OPERACIONES_DET OD2 ON ODS2.IDOperacion_Det=OD2.IDOperacion_Det AND OD2.IDServicio=ODS2.IDServicio
				INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab AND O2.IDProveedor=o.IDProveedor--Setours Buenos Aires
				INNER JOIN MASERVICIOS_DET SD2 ON ODS2.IDServicio_Det_V_Cot=SD2.IDServicio_Det
				INNER JOIN MASERVICIOS S2 ON SD2.IDServicio=S2.IDServicio 
				INNER JOIN MAPROVEEDORES P2 ON S2.IDProveedor=P2.IDProveedor AND P2.IDProveedor<>'002329'--Transporte Buenos Aires 
				LEft Join COTICAB_TIPOSCAMBIO tca On tca.IDCab=o2.IDCab And tca.CoMoneda=ods2.IDMoneda
				WHERE ODS2.IDOperacion_Det IN(od.IDOperacion_Det))
			Else
				od.TotalGen
			End


		End
	   End 

	  End as CostoReal,   


 Case When sd.idTipoOC in('002','006') Then  
   Case When SD.ConAlojamiento=1 Or p.IDProveedor='001836' Then OD.NetoGen*(@IGV/100) Else 
     Case When sd.idTipoOC = '002' Then OD.TotalGen*(@IGV/100)  
	   Else (OD.NetoGen*(@IGV/100) )/2 End 

  End  

  Else

  0

 End 

 As TotImpto,

 sd.idTipoOC, 

		Case When @CoUbigeo_Oficina='000113' and o.IDProveedor='002315' Then 'USD' Else RD.IDMoneda 
		End as IDMoneda, SD.TC

 

 FROM OPERACIONES O INNER JOIN MAPROVEEDORES P ON O.IDProveedor=P.IDProveedor

 And P.IDTipoOper='E'

 And (P.IDTipoOper='E' Or LTRIM(rtrim(@CoUbigeo_Oficina))<>'') 

			And (P.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='' )

 INNER JOIN OPERACIONES_DET OD ON O.IDOperacion=OD.IDOperacion  

 AND (OD.IDVoucher<>0 OR O.IDProveedor='001836' )--PERURAIL

 INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det

 INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det

 Inner join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc

 Left Join MAPROVEEDORES pint On pint.IDProveedor=p.IDProveedorInternacional

   

 Left Join MAUBIGEO ubPI On p.IDCiudad=ubPI.IDubigeo  
 Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo 
 Left Join RESERVAS r On O.IDReserva=r.IDReserva    

		LEFT join coticab cc on cc.idcab=o.idcab



 WHERE O.IDCab=@IDCab and r.Anulado=0 and RD.Anulado=0 --AND (OD.IDVoucher<>0 OR O.IDProveedor='001836' )
		and cc.estado<>'X'
		
 ) AS X

GROUP BY IDProveedor,idTipoOC,IDVoucher,IDMoneda,TC   

) AS Y

      ) AS Z   

     ) AS XX

    ) AS YY

   ) AS ZZ  

  ) AS ZZ1

 WHERE CostoBaseUSD<>0

 return @SumaTotal
 end

