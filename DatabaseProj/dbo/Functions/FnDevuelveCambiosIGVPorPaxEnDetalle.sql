﻿--JRF-20160412-..Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det...
--JRF-20160412-..sd.AFECTO=0
CREATE Function dbo.FnDevuelveCambiosIGVPorPaxEnDetalle(@IDReserva int,@IDCab int)
returns bit
As
Begin
Declare @AplicaIGVHotel bit = Case When Exists(select IDPax from COTIPAX Where (IDNacionalidad = '000323' Or Residente=1) And IDCab=@IDCab) then 1 Else 0 End
Declare @bCambiosVentas bit = 0

if @AplicaIGVHotel=1
Begin
	select @bCambiosVentas = 1
	from reservas_det rd Inner Join cotidet cd  On rd.IDDet=cd.IDDet
	Left Join reservas_det_pax rdp On rd.IDReserva_Det=rdp.IDReserva_Det
	Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	where rd.idreserva = @idreserva And rd.Anulado=0 And r.Anulado=0 And sd.PlanAlimenticio=0
	And rdp.IDPax In (select IDPax from COTIPAX Where (IDNacionalidad = '000323' Or Residente=1) And IDCab=@IDCab) And IsNull(rd.IgvHab,0) = 0
End
Else
Begin
	--select IDPax from COTIPAX Where (IDNacionalidad = '000323' Or Residente=0) And IDCab=@IDCab
	select @bCambiosVentas = 1
	from reservas_det rd Inner Join cotidet cd  On rd.IDDet=cd.IDDet
	Left Join reservas_det_pax rdp On rd.IDReserva_Det=rdp.IDReserva_Det
	Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	where rd.idreserva = @idreserva And rd.Anulado=0 And r.Anulado=0 And sd.PlanAlimenticio=0
	And rdp.IDPax In (select IDPax from COTIPAX Where (IDNacionalidad = '000323' Or Residente=0) And IDCab=@IDCab And FlServicioNoShow=0) And IsNull(rd.IgvHab,0) > 0
	And sd.Afecto=0
End

--select @bCambiosVentas
return @bCambiosVentas
End
