﻿--JHD-20150526-Agregar validacion de pax sin nacionalidad
CREATE Function [dbo].[FnDatosIncompletosPaxxFile](@IDCab int)
	returns bit
As
	Begin
	Declare @bIncompletos bit=0
	--Exec COTIPAX_Sel_ExisteIncompletosOutput @IDCab, @bIncompletos output

	If Exists(
		Select IDPax From COTIPAX Where IDCab=@IDCab And (
		(CHARINDEX('Pax',Nombres)>0 And dbo.FnNumeroEncontradoenCadena(Nombres)=1) Or
		(CHARINDEX('Pax',Apellidos)>0 And dbo.FnNumeroEncontradoenCadena(Apellidos)=1) Or
		len(isnull(NumIdentidad,''))<6	Or
		FecNacimiento is null or (IDNacionalidad='000001')
		)
			)
		
		Set @bIncompletos=1

	Return @bIncompletos
	End
