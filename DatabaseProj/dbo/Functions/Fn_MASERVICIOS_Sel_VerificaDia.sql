﻿
CREATE Function dbo.Fn_MASERVICIOS_Sel_VerificaDia
(@IDServicio char(8),    
@FechaDia varchar(20))

Returns Bit 
As  
 
	Begin
	Declare @NumDiaFechaDia Tinyint, @Rows Tinyint, @bRet bit=0
	Set @FechaDia=CONVERT(SMALLDATETIME,@FechaDia,103)
	   
	Set @NumDiaFechaDia = dbo.FnDiaSemana(Case When SUBSTRING(DATENAME(dw,@FechaDia),0,2) = 'M' Then   
              Case when SUBSTRING(DATENAME(dw,@FechaDia),0,3) = 'MI'  
               Then 'X'  
              Else SUBSTRING(DATENAME(dw,@FechaDia),0,2) End  
            Else SUBSTRING(DATENAME(dw,@FechaDia),0,2) End)  
    set @Rows = (select COUNT(*) from MASERVICIOS_ATENCION Where IDservicio = @IDServicio)
    if @Rows <> 0
		If Exists(select IDAtencion 
					  from MASERVICIOS_ATENCION Where IDservicio =@IDServicio  
					  And (@NumDiaFechaDia >= dbo.FnDiaSemana(Dia1) And @NumDiaFechaDia <= dbo.FnDiaSemana(Dia2)))
			Set @bRet = 1
		Else
			Set @bRet = 0		
	else
		Set @bRet = 1				  

	Return @bRet
	End

	
