﻿Create Function dbo.FnDevuelveCadenaPaxxReservaDet(@IDReserva_Det int)
returns varchar(Max)
As
Begin
	Declare @CadPax varchar(Max)='',@NombrePax varchar(100)=''
	Declare curPax cursor for
	Select isnull(cp.Nombres,'') + ' ' + isnull(cp.Apellidos,'') As NombreCompleto
	from RESERVAS_DET_PAX rdp Inner Join RESERVAS_DET rd On rdp.IDReserva_Det=rd.IDReserva_Det
	Left Join COTIPAX cp On rdp.IDPax=cp.IDPax
	Where rd.IDReserva_Det=@IDReserva_Det
	Open curPax
	Fetch Next From curPax into @NombrePax
	While @@Fetch_Status=0
	Begin
		Set @CadPax = @CadPax + @NombrePax +', '
		Fetch Next From curPax into @NombrePax
	End
	Close curPax
	DealLocate curPax
	
	if @CadPax <> ''
		Set @CadPax = substring(@CadPax,1,Len(@CadPax)-1)
	
	return @CadPax
End
