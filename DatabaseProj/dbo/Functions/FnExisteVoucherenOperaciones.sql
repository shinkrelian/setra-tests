﻿Create function dbo.FnExisteVoucherenOperaciones(@IDOperacion int)
returns bit
As
Begin
	Declare @ExisteOperacionVal bit = 0
	If Exists(select IDVoucher from OPERACIONES_DET where IDOperacion=@IDOperacion and IDVoucher <> 0)
	Begin
		--select * from OPERACIONES_DET Where IDOperacion=@IDOperacion
		If Exists(select IDOperacion_Det from OPERACIONES_DET_TMP Where IDOperacion=@IDOperacion And IsNull(IDOperacion_DetReal,0)=0)
		   And Exists(select IDOperacion_Det from OPERACIONES_DET where IDOperacion=@IDOperacion)
			Set @ExisteOperacionVal = 1
	End
	else
	Begin
		set @ExisteOperacionVal = 0
	End
	return @ExisteOperacionVal
End
