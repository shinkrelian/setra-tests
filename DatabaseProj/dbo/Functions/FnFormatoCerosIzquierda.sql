﻿
Create Function [dbo].[FnFormatoCerosIzquierda](@ValorNumerico int, @Tamanio tinyint)
	returns varchar(20)
as
	Begin
	Declare @ValorReturn varchar(20)=''
	Declare @vcValorNumerico varchar(10)=cast(@ValorNumerico as varchar(10))
	
	Set @ValorReturn = Replicate('0',@Tamanio-len(@vcValorNumerico)) + @vcValorNumerico 

	Return @ValorReturn
	End
