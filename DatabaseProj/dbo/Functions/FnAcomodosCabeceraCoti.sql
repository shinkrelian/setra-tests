﻿--JRF-20141018-Considerar la cantidad de residentes por Acomodo
CREATE Function dbo.FnAcomodosCabeceraCoti(@IDCab int)  
returns @AcomodoCabecera table (Cantidad  tinyint,Acomodo varchar(40),ItemLoc bit)  
as  
Begin  
 Declare @Simple tinyint =0,@Twin tinyint=0,@Matrimonial tinyint=0,@Triple tinyint=0,
		 @SimpleResidente tinyint =0,@TwinResidente tinyint=0,@MatrimonialResidente tinyint=0,@TripleResidente tinyint=0,
		 @SimpleTotal tinyint =0,@TwinTotal tinyint=0,@MatrimonialTotal tinyint=0,@TripleTotal tinyint=0
		 
 Select @Simple = Isnull(Simple,0),  
		@SimpleResidente=Isnull(SimpleResidente,0),
		@Twin = Isnull(Twin,0),
		@TwinResidente = Isnull(TwinResidente,0),
		@Matrimonial = Isnull(Matrimonial,0),
		@MatrimonialResidente = Isnull(MatrimonialResidente,0),
		@Triple = Isnull(Triple,0),
		@TripleResidente = Isnull(TripleResidente,0)
 from COTICAB  
 Where IDCab = @IDCab  
 
 Set @SimpleTotal = @Simple + @SimpleResidente
 Set @TwinTotal = @Twin + @TwinResidente
 Set @MatrimonialTotal = @Matrimonial + @MatrimonialResidente
 Set @TripleTotal = @Triple + @TripleResidente
 
   
 if (@SimpleTotal + @TwinTotal + @MatrimonialTotal + @TripleTotal) > 0  
 Begin  
	--Evaluando el Simple
	if @SimpleTotal > 0
	Begin
		if @SimpleResidente > 0
		Begin
			Insert into @AcomodoCabecera values(@SimpleResidente,'SINGLE',0)
			if @Simple > 0
				Insert into @AcomodoCabecera values(@Simple,'SINGLE',0)
		End
		else
		Begin
			Insert into @AcomodoCabecera values(@SimpleTotal,'SINGLE',0)
		End
	End
	
	--Evaluando el Twin
	if @TwinTotal > 0
	Begin
		if @TwinResidente > 0
		Begin
			Insert into @AcomodoCabecera values(@TwinResidente,'TWIN',0)
			if @Twin > 0
				Insert into @AcomodoCabecera values(@Twin,'TWIN',0)
		End
		else
		Begin
			Insert into @AcomodoCabecera values(@TwinTotal,'TWIN',0)
		End
	End
	
	--Evaluando el Matrimonial
	if @MatrimonialTotal > 0
	Begin
		if @MatrimonialResidente > 0
		Begin
			Insert into @AcomodoCabecera values(@MatrimonialResidente,'MAT',0)
			If @Matrimonial > 0
				Insert into @AcomodoCabecera values(@Matrimonial,'MAT',0)
		End
		else
		Begin
			Insert into @AcomodoCabecera values(@MatrimonialTotal,'MAT',0)
		End
	End
	
	--Evaluando el Triple
	if @TripleTotal > 0
	Begin
		if @TripleResidente > 0
		Begin
			Insert into @AcomodoCabecera values(@TripleResidente,'TRIPLE',0)
			if @Triple > 0
				Insert into @AcomodoCabecera values(@Triple,'TRIPLE',0)
		End
		else
		Begin
			Insert into @AcomodoCabecera values(@TripleTotal,'TRIPLE',0)
		End
	End
 End  
 
 return;  
End  
