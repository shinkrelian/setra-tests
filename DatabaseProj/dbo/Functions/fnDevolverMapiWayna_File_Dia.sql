﻿create function fnDevolverMapiWayna_File_Dia (@idcab int,@dia as smalldatetime)
returns varchar(1000)
as
begin

declare @tipos as table(
Tipo varchar(20) null
)

insert into @tipos
select distinct 
case MAPIWAYNRAQCCODE when 'CUZ00145' then 'RAQCHI' when 'CUZ00134' then 'WAYNA' when 'CUZ00133' then 'WAYNA' else 'MAPI' END AS Tipo 

 --,valores= COALESCE(@valores + ', ', '') + (case MAPIWAYNRAQCCODE when 'CUZ00145' then 'RAQCHI' when 'CUZ00134' then 'WAYNA' when 'CUZ00133' then 'WAYNA' else 'MAPI' END) 

from (
select distinct c.IDCAB, c.IDFile,
	cd.iddet,cd.idservicio,sd.anio,
	dbo.FnServicioMPWP(cd.IDServicio_Det) as MAPIWAYNRAQCCODE,
	(Select Min(Dia) From COTIDET Where IDCAB=cd.IDCAB And IDubigeo='000066') as FechaINCusco
	,cd.dia
	--,cd.*
from cotidet cd
inner join coticab c on c.IDCAB=cd.IDCAB
inner join MASERVICIOS_DET sd on sd.IDServicio_Det=cd.IDServicio_Det
where exists(select IDDet from COTIDET_PAX cdp where cdp.IDDet=cd.IDDET and (cdp.FlEntMPGenerada=1 or cdp.FlEntWPGenerada=1 or cdp.FlEntRQGenerada=1))
and c.IDCAB=@idcab --and cd.Dia=@dia 
and ( CONVERT(VARCHAR(20), cd.Dia, 103) = CONVERT(VARCHAR(20), @dia, 103) )
) AS X


DECLARE @valores VARCHAR(1000)
SELECT @valores= COALESCE(@valores + '/ ', '') + tipo FROM @tipos
return isnull(@valores,'')
end;
