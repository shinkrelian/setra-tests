﻿CREATE Function [dbo].[FnNombres_Pax] (@Iddet int)
		Returns varchar(5000) AS
		Begin
		declare @retvalue varchar(5000)
		declare @retvalueVf varchar(5000)
		set @retvalue=''
		 
		select @retvalue = @retvalue +ltrim(rtrim(isnull(Nombre,'')))+'/'
		from (select distinct isnull(Ctpx.Nombres,'') as Nombre
				from COTIDET Cdt Left Join COTIDET_PAX Cdtpx On Cdt.IDDET= Cdtpx.IDDet
					 Left Join COTIPAX Ctpx On Ctpx.IDPax= Cdtpx.IDPax
				Where Cdt.IDDET=@Iddet And Ctpx.Nombres is not null ) as tmp_tbl
		if(@retvalue='')
			Begin
			  set @retvalueVf=''
			End
		Else
			Begin
			  set @retvalueVf= substring(@retvalue,1,len(@retvalue)-1)
			End
		return @retvalueVf
		End
