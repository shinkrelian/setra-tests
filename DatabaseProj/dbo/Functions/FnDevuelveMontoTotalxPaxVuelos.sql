﻿Create Function dbo.FnDevuelveMontoTotalxPaxVuelos(@IDCab int,@bCostoPax bit)
returns numeric(12,4)
As
Begin
 Declare @TotalVenta numeric(12,4)=0,@TotalVentaSum numeric(12,4)=0
 Declare curItemSum cursor for
 Select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) * 
		Case When @bCostoPax=1 then (select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) else 1 End
 from COTIVUELOS cv
 Where cv.TipoTransporte='V' And cv.EmitidoSetours=1 And cv.IDCab=@IDCab and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0
 Open curItemSum
 Fetch Next From curItemSum into @TotalVenta
 While @@Fetch_Status = 0
 Begin
	Set @TotalVentaSum = @TotalVentaSum + @TotalVenta
	Fetch Next From curItemSum into @TotalVenta
 End
 Close curItemSum
 DealLocate curItemSum
 return @TotalVentaSum
End
