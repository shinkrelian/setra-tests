﻿
CREATE Function [dbo].[FnAdjuntosCorreo](@NuCorreo int)
	Returns varchar(Max)	
As
	Begin
	Declare @NoArchAdjunto	varchar(150)
	Declare @Adjuntos varchar(Max)=''
	
	Declare curAdjuntos cursor For
	Select NoArchAdjunto 
	From .BDCORREOSETRA..ADJUNTOSCORREOFILE
	Where NuCorreo=@NuCorreo
	
	Open curAdjuntos
	Fetch Next From curAdjuntos Into @NoArchAdjunto
	While @@Fetch_Status=0
		Begin
		Set @Adjuntos+=@NoArchAdjunto+'; '
		Fetch Next From curAdjuntos Into @NoArchAdjunto
		End
	Close curAdjuntos
	Deallocate curAdjuntos
	
	If ltrim(rtrim(@Adjuntos))<>'' 
		Begin
		If Right(ltrim(rtrim(@Adjuntos)),1)=';' 
		Set  @Adjuntos=Left(@Adjuntos,Len(@Adjuntos)-1)
		End
	Return @Adjuntos 
	End

