﻿Create Function dbo.FnDevuelveIDProveedorxIDUserTrasladista(@IDUsuarioTras varchar(6))
returns char(6)
as
Begin
Declare @IDProveedor varchar(6)=@IDUsuarioTras
if Len(Ltrim(Rtrim(@IDProveedor)))=4
Begin
	Set @IDProveedor = (select IDProveedor from MAPROVEEDORES where IDUsuarioTrasladista=@IDUsuarioTras)
End
return @IDProveedor
End
