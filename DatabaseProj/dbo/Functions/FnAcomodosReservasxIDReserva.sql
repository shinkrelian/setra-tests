﻿
--JRF-20140228-Reemplazo del CantidadAPagar x Cantidad de Acomodos    
--JRF-20150225-No considerar los noShow
--PPMG-20160427-cambie rd.Dia por convert(date,rd.Dia), los minutos hacian duplicar.
CREATE FUNCTION [dbo].[FnAcomodosReservasxIDReserva](@IDReserva int,@DiaSelect smalldatetime)                
Returns varchar(max)                
As                 
Begin                
 Declare @DesHotels varchar(max) ='',@DesHotel varchar(max) = '',@tiCont tinyint = 0                
 declare curDetReserva cursor for                
  select distinct DesHotel from(          
 select             
 Case when (sd.PlanAlimenticio = 0  and p.IDTipoProv = '001')  then             
 cast(cast(rd.Cantidad as tinyint) as varchar(2)) +' '+rd.Servicio             
 else            
 sd.Descripcion + char(13)+ cast(cast(rd.Cantidad as tinyint) as varchar(3))  + replace(rd.Servicio,sd.Descripcion,'')            
 End as DesHotel,Isnull((select IncGuia from cotidet cd Where cd.IDDet = rd.Iddet),0) as IncGuia                 
 from reservas_det rd --left join cotidet cd on  rd.iddet = cd.iddet                 
 left join maservicios_det sd on rd.IDServicio_Det= sd.IDServicio_Det                
 Left Join reservas r ON rd.IDReserva= r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor= p.IDProveedor              
 where r.Anulado=0 and rd.Anulado=0 and rd.FlServicioNoShow=0 and rd.idreserva = @IDReserva  and @DiaSelect between convert(date,rd.Dia) and dateadd(d,-1,rd.FechaOut)        
 and ((sd.PlanAlimenticio = 0  and p.IDTipoProv = '001') Or (sd.ConAlojamiento =1  and p.IDTipoProv = '003')) )          
 as X          
Where X.IncGuia = 0          
 open curDetReserva                
 Fetch Next From curDetReserva Into @DesHotel                
 While @@Fetch_Status=0                
  Begin                  
  If @tiCont=0                
   Set @DesHotels=@DesHotel+ char(13)                
  Else                
      if charindex(@DesHotel,@DesHotels)=0                
    Set @DesHotels=@DesHotels+@DesHotel+ char(13)                
                  
  Set @tiCont = @tiCont + 1                
  Fetch Next From curDetReserva Into @DesHotel                
  End                
 close curDetReserva                
 deallocate curDetReserva                
 if @DesHotels <> ''                
  set @DesHotel = substring(@DesHotel,0,LEN(@DesHotel)-1)                
                 
 Return @DesHotels                
End 
