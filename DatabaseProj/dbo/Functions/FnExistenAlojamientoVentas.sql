﻿Create Function dbo.FnExistenAlojamientoVentas(@IDProveedor char(6),@IDCab int)
returns bit
As
Begin
	Declare @ReturnExists bit =0
	Declare @IDTipoProv char(3)=''
	Select @IDTipoProv=IDTipoProv from MAPROVEEDORES where IDProveedor=@IDProveedor

	If @IDTipoProv='001'
		Set @ReturnExists = 1

	If Exists(select cd.IDDET from COTIDET cd Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det 
	where sd.ConAlojamiento=1 and cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab) And @ReturnExists= 0
		Set @ReturnExists = 1

	return @ReturnExists
End
