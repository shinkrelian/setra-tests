﻿--JHD-20160307- select @SumaTotal= IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0)
CREATE function dbo.FnDevolverTotal_VuelosyBuses_CotiCab
  (
@idcab int,
@CoUbigeo_Oficina char(6)
)
Returns numeric(20,6)  
as
begin
 declare @SumaTotal numeric(20,6)=0
 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)
 Declare @MargenInterno as numeric(6,2)=10

  SELECT @SumaTotal = IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0)
 FROM            
  (            
  Select Z.*,      
  Case When Tarifa=0 Then       
 ROUND(Z.Total /(1 - (Margen/100)) ,0)       
  Else        
 Z.Tarifa*Z.NroPax       
  End AS VentaUSD             
  From            
   (            
   Select Y.*,             
   (IsNull(Y.CostoRealUSD,0)+IsNull(Y.TotImptoUSD,0)+IsNull(Y.IGV_SFE,0)) as TotalUSD,      
   (Y.CompraNeto+Y.TotImpto+Y.IGV_SFE) as Total                
         
   From            
    (            
    Select X.IDProveedor, X.DescProveedor, X.DescProveedorInternac, RutayCodReserva,            
    DescPaisProveedor,  
    X.CostoReal  as CompraNeto,      
          
     Case When IDMoneda = 'USD' Then       
  CostoReal      
  Else       
  --dbo.FnCambioMoneda(X.CostoReal,'SOL','USD',ISNULL(@TipoCambio,tc))        
  dbo.FnCambioMoneda(X.CostoReal, X.IDMoneda,'USD',    
 ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=X.IDMoneda),tc))        
  End as CostoRealUSD,       
          
    X.CostoReal *(@IGV/100) as TotImpto,               
    Case When IDMoneda = 'USD' Then       
  X.CostoReal *(@IGV/100)      
  Else       
  --dbo.FnCambioMoneda(X.CostoReal *(@IGV/100),'SOL','USD',ISNULL(@TipoCambio,tc))        
  dbo.FnCambioMoneda(X.CostoReal *(@IGV/100),X.IDMoneda,'USD',    
 ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=X.IDMoneda),tc))        
  End as TotImptoUSD,               
                
                
    --Case When idTipoOC='001' Then             
    -- X.CostoReal*(@IGV/100)        
    --Else              
    -- Case When idTipoOC='006' Then (X.CostoReal*(@IGV/100))*0.5 Else 0 End     
    --End as IGV_SFE,             
    0 as IGV_SFE,      
    X.IDMoneda,            
         
    X.DescOperacionContab,             
    X.Margen, X.idTipoOC, X.Tarifa, X.NroPax, X.Servicio      
    From            
     (            
     Select p.IDProveedor,p.NombreCorto as DescProveedor,             
     pint.NombreCorto as DescProveedorInternac,             
     paPI.Descripcion as DescPaisProveedor,  
     --isnull(cv.ruta,'')+ ' - '+ ISNULL(cv.CodReserva,'') as RutayCodReserva,      
     isnull(cv.ruta,'') as RutayCodReserva,      
     --cd.CostoReal * cd.NroPax as CostoReal,             
           
     Case When isnull(cv.CostoOperador,0)=0 Then      
  isnull(od.TotalGen,(cv.Tarifa*(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID))/(1+(@IGV/100)) )       
     Else      
  cv.CostoOperador*(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID)/(1+(@IGV/100))      
     End as CostoReal,      
     --Isnull(cd.TotImpto*cd.NroPax,(cv.Tarifa*(@IGV/100))* (Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID)) as TotImpto,           
     isnull(rd.IDMoneda,'USD') as IDMoneda,            
     ISNULL(tox.Descripcion,'GRAVADO') as DescOperacionContab,             
     
     --ISNULL(cd.MargenAplicado,0) as Margen,      
     Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 
		ISNULL(cd.MargenAplicado,0) 
	 Else
		@MargenInterno
	 End as Margen,
     
     --Isnull(cd.NroPax,(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID)) as NroPax,       
     isnull(sd.idTipoOC,'002') AS idTipoOC,sd.TC,isnull(cv.Tarifa,0) as Tarifa,      
     (Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID) as NroPax,    
     isnull(cv.Servicio,'') as Servicio    
        
     From COTIVUELOS cv           
     Inner Join MAPROVEEDORES p On cv.IdLineaA=p.IDProveedor            
  And p.IDProveedor <> '001836'--PERURAIL            
 And cv.IDCAB=@IDCab             
  And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
     Left Join COTIDET cd On cv.IdDet=cd.IDDet and cv.TipoTransporte in('V','B')            
     Left Join MAPROVEEDORES pint On pint.IDProveedor=p.IDProveedorInternacional            
     Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det            
     Left JOIN RESERVAS_DET rd ON cd.IDDET=rd.IDDet            
     Left join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc --AND sd.idTipoOC='003'--NO GRAVADO        
     Left Join OPERACIONES_DET od On rd.IDReserva_Det=od.IDReserva_Det          
       
 Left Join MAUBIGEO ubPI On P.IDCiudad=ubPI.IDubigeo  
    Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo           
     left join coticab cc on cc.IDCAB=cv.IDCAB
       
     Where --(sd.idTipoOC='003'  )--NO GRAVADO        
     --and       
     ((isnull(cv.IdDet,0)<>0 And sd.idTipoOC='003') Or (isnull(cv.IdDet,0)=0 And cv.EmitidoSetours=1))      
     and cv.TipoTransporte in('V','B')          
     and (isnull(cv.IdDet,0)<>0 Or (isnull(cv.IdDet,0)=0 And cv.EmitidoSetours=1))    
	 and cc.estado<>'X'  
     ) as X            
    ) as Y            
   ) as Z            
  ) as XX        

RETURN @SumaTotal
End

