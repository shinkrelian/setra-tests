﻿--HLF-20150921-Set @NombreCompleto=Ltrim(rtrim(@NombreCompleto))
CREATE Function dbo.FnDevuelveFirstOrLastName(@NombreCompleto varchar(200),@PrimerNombre bit)
returns varchar(200)
As
Begin
	
	Set @NombreCompleto=Ltrim(rtrim(@NombreCompleto))

	Declare @Resultado varchar(200)=''
	Declare @PrimerNom varchar(20)= Ltrim(Rtrim((Select SUBSTRING(@NombreCompleto,1,CHARINDEX(' ',@NombreCompleto)))))
	Declare @CharIndex int = CHARINDEX(' ',@NombreCompleto)

	if @CharIndex = 0
	Begin
		Set @PrimerNom = @NombreCompleto
	End

	Select @Resultado = Case When @PrimerNombre = 1 Then @PrimerNom else 
		   Case When Ltrim(Rtrim(REPLACE(@NombreCompleto,@PrimerNom,'')))='' then ' ' Else Ltrim(Rtrim(REPLACE(@NombreCompleto,@PrimerNom,''))) End End
	--Set @Resultado = Case When Ltrim(rtrim(@Resultado))='' Then ' ' Else Ltrim(rtrim(@Resultado)) End
	return @Resultado
End
