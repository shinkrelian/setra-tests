﻿Create Function [dbo].[FnCalculoValorReglade3] 
	(@MontoInput numeric(12,2), @TotalInput numeric(12,2), @TotalNivelXInput numeric(12,2))
Returns numeric(12,2)
As 
Begin	
		
	Return (@MontoInput*@TotalNivelXInput)/@TotalInput
End

