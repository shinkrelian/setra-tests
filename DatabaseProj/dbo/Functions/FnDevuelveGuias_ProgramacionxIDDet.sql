﻿
--create
create Function dbo.FnDevuelveGuias_ProgramacionxIDDet(@IDCab int,@IDServicio_Det int)
returns varchar(Max)
as
begin
	Declare @Nombres varchar(Max)='',@Name varchar(Max)=''
	Declare curPax cursor for
	
	select distinct p.NombreCorto from 
		OPERACIONES_DET_DETSERVICIOS ods
		left join operaciones_det od on od.IDOperacion_Det=ods.IDOperacion_Det
		left join operaciones o on o.IDOperacion=od.IDOperacion
		left join MAPROVEEDORES p on p.IDProveedor=ods.IDProveedor_Prg
		where
		o.IDCab=@IDCab and od.IDServicio_Det=@IDServicio_Det and
		p.IDTipoProv='005'

	Open curPax
	Fetch Next From curPax into @Name
	Begin
		While @@FETCH_STATUS=0
		Begin
			Set @Nombres = @Nombres + @Name + char(13)
			Fetch Next From curPax into @Name
		End
	End
	Close curPax
	DealLocate curPax
	return @Nombres

end
