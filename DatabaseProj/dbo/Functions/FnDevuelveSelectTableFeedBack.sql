﻿Create Function dbo.FnDevuelveSelectTableFeedBack(@intMin smallint,@intMax smallint,
 @IDUbigeo char(6),@IDIdioma varchar(12),@Guia_Estado char(1),@Nombre varchar(100),
 @CoPais char(6), @IDTipoProv char(3))
returns @feedBack 
table (IDProveedor char(6),DescTipoProv varchar(Max),Nombre varchar(100),
ApPaterno varchar(Max),FecNacimiento char(10),IdiomasNivel varchar(Max),
Guia_Estado varchar(Max),Direccion varchar(Max),DescPais varchar(70),
DescCiudad varchar(80),Email1 varchar(Max),DescTipoDoc varchar(80),
NumIdentidad varchar(30),RUC varchar(30),Movil1 varchar(30),Movil2 varchar(30),
Adicionales varchar(Max),DocCurriculo varchar(200),DocFoto binary,Ruta varchar(200),
Imagen binary,Poor int,Average int,Good int,VeryGood int,
Excelente int,Especialidades varchar(Max))
as
Begin

	Declare @tmpControlCalidad table (Excelente smallint,VeryGood smallint,Good smallint,Average smallint,Poor smallint,IDProveedor char(6))
	Declare @tmpfeedBack table (IDProveedor char(6),DescTipoProv varchar(Max),Nombre varchar(100),
				ApPaterno varchar(Max),FecNacimiento char(10),IdiomasNivel varchar(Max),
				Guias_Estado varchar(Max),Direccion varchar(Max),DescPais varchar(70),
				DescCiudad varchar(80),Email varchar(Max),DescTipoDoc varchar(80),
				NumIdentidad varchar(30),RUC varchar(30),Movil1 varchar(30),Movil varchar(30),
				Adicionales varchar(Max),DocCurriculo varchar(200),DocFoto binary,Ruta varchar(200),
				Imagen binary,Poor int,Average int,Good int,VeryGood int,
				Excelente int,Especialidades varchar(Max))
	
	Insert into @tmpControlCalidad
	Select SumaExcelente,SumaVeryGood,SumaGood,SumaAverage,SumaPoor,IDProveedor from (
	Select Sum(Cast(FlExcelent as tinyint)) as SumaExcelente,
		   Sum(Cast(FlVeryGood as tinyint)) as SumaVeryGood,
		   Sum(Cast(FlGood as tinyint)) as SumaGood,
		   Sum(Cast(FlAverage as tinyint)) as SumaAverage,
		   Sum(Cast(FlPoor as tinyint)) as SumaPoor,
		   Count(distinct ccp.IDCab) as CantFiles,
		   ccpc.IDProveedor
	from (Select * from CONTROL_CALIDAD_PRO_MACUESTIONARIO where
		  (FlExcelent=1 Or FlVeryGood=1 Or FlGood=1 Or FlAverage=1 Or FlPoor=1)) ccpc
	Left Join MAPROVEEDORES p On ccpc.IDProveedor=p.IDProveedor
	Left Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad
	Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo  
	Left Join MAUBIGEO up On u.IDPais=up.IDubigeo  
	where ccpc.IDProveedor<>'000000'
	Group By ccpc.IDProveedor
	) as X


		Insert into @tmpfeedBack
		 Select X.* from (
			 Select IsNull(tmpP.IDProveedor,p.IDProveedor) as IDProveedor,tp.Descripcion
			  + Case When Ltrim(Rtrim(@IDUbigeo))='' then ' '+ u.Descripcion Else '' End  as DescTipoProv,
			  Case When p.Nombre is null then REPLACE(REPLACE(p.NombreCorto,',','') ,'-','')
			   else Upper(isnull(Nombre,'')) End As Nombre, Upper(ISNULL(ApPaterno,'')) ApPaterno,
				Convert(char(10),FecNacimiento,103) As FecNacimiento
			 , dbo.FnIdiomasProveedor(P.IDProveedor) As IdiomasNivel,   
			 Case isnull(Guia_Estado,'')   
			  When 'P' Then 'PREFERENCIA'  
			  When 'O' Then 'OK'  
			  When 'E' Then 'EVITAR'  
			 End Guia_Estado,  
			 p.Direccion,  
			 isnull(up.Descripcion,'') as DescPais,  
			 isnull(u.Descripcion,'') as DescCiudad,  
			 isnull(p.Email1,'') As Email1,ISNULL(tdi.Descripcion,'') as DescTipoDoc, p.NumIdentidad,   
			 isnull(p.RUC,'') as RUC,isnull(p.Movil1,'') as Movil1,
			 isnull(Movil2,'') As Movil2, isnull(p.Adicionales,'') Adicionales,
			 isnull(DocCurriculo,'') As DocCurriculo , cast(ISNULL(DocFoto,'') As binary) DocFoto,
			 isnull(DocFoto,'') As Ruta,CAST('' as binary) As Imagen,
			 IsNull(tmpP.Poor,0) as Poor,IsNull(tmpP.Average,0) as Average,IsNull(tmpp.Good,0) as Good,
			 IsNull(tmpP.VeryGood,0) as VeryGood,IsNull(tmpP.Excelente,0) as Excelente,
			 dbo.FnDevuelveEspecialidades(IsNull(tmpP.IDProveedor,p.IDProveedor)) as Especialidades
			 From MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo  
			 Left Join MAUBIGEO up On u.IDPais=up.IDubigeo  
			 Left Join MATIPOIDENT tdi On p.IDIdentidad=tdi.IDIdentidad  
			 Left Join MATIPOPROVEEDOR tp On p.IDTipoProv=tp.IDTipoProv
			 Inner Join @tmpControlCalidad tmpP On p.IDProveedor=tmpP.IDProveedor
			 Where (p.IDTipoProv In('005','017','020') And (p.IDTipoProv=@IDTipoProv Or Rtrim(Ltrim(@IDTipoProv))=''))
			 And (@IDIdioma In (Select ep.IDIdioma From MAENTREVISTAPROVEEDOR ep Where ep.IDProveedor=p.IDProveedor And 
								ep.FlConocimiento=1 And (Ltrim(Rtrim(@Guia_Estado))='' Or ep.Estado=@Guia_Estado))Or ltrim(rtrim(@IDIdioma))='')  
			 And (@Guia_Estado In (Select ep.Estado From MAENTREVISTAPROVEEDOR ep Where ep.IDProveedor=p.IDProveedor And 
									ep.FlConocimiento=1 And (Ltrim(Rtrim(@Guia_Estado))='' Or ep.Estado=@Guia_Estado))Or ltrim(rtrim(@Guia_Estado))='')  
			 And (p.IDCiudad=@IDUbigeo Or ltrim(rtrim(@IDUbigeo))='')  
			 And p.Activo = 'A'
			 AND (up.IDubigeo =@CoPais or rtrim(ltrim(@CoPais))='') 
			 ) as X
			 Where (X.Nombre Like '%'+@Nombre+'%' Or ltrim(rtrim(@Nombre))='')
			 Order by Nombre 

	if (@intMin=0 And @intMax=1) Or (@intMin=1 And @intMax=1)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where Poor > 0  And (Average + Good + VeryGood + Excelente)=0
		Order by Nombre  
	End
	
	if (@intMin=0 And @intMax=2) Or (@intMin=1 And @intMax=2)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Poor+Average) > 0  And (Good + VeryGood + Excelente)=0
		Order by Nombre
	End

	if (@intMin=0 And @intMax=3) Or (@intMin=1 And @intMax=3)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Poor+Average+Good) > 0  And (VeryGood + Excelente)=0
		Order by Nombre
	End

	if (@intMin=0 And @intMax=4) Or (@intMin=1 And @intMax=4)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Poor+Average+Good+VeryGood) > 0  And (Excelente)=0
		Order by Nombre
	End

	if (@intMin=0 And @intMax=5) Or (@intMin=1 And @intMax=5) Or (@intMin=0 And @intMax=0) Or (@intMin > @intMax)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Poor+Average+Good+VeryGood+Excelente) > 0
		Order by Nombre
		goto ret
	End

	if(@intMin=2 And @intMax=2)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where Average > 0  And (Good + VeryGood + Excelente)=0 And Poor=0
		Order by Nombre
	End

	if(@intMin=2 And @intMax=3)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Average + Good) > 0  And (VeryGood + Excelente)=0 And Poor=0
		Order by Nombre
	End

	if(@intMin=2 And @intMax=4)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Average + Good + VeryGood) > 0  And Excelente=0 And Poor=0
		Order by Nombre
	End

	if(@intMin=2 And @intMax=5)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Average + Good + VeryGood + Excelente) > 0  And Poor=0
		Order by Nombre
	End

	if(@intMin=3 And @intMax=3)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where Good > 0  And (VeryGood + Excelente)=0 And Poor=0 And Average=0
		Order by Nombre
	End

	if(@intMin=3 And @intMax=4)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Good + VeryGood) > 0  And Excelente=0 And Poor=0 And Average=0
		Order by Nombre
	End

	if(@intMin=3 And @intMax=5)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (Good + VeryGood + Excelente) > 0  And Poor=0 And Average=0
		Order by Nombre
	End

	if(@intMin=4 And @intMax=4)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where VeryGood > 0  And Excelente=0 And Poor=0 And Average=0 And Good=0
		Order by Nombre
	End

	if(@intMin=4 And @intMax=5)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where (VeryGood + Excelente) > 0 And Poor=0 And Average=0 And Good=0
		Order by Nombre
	End

	if(@intMin=4 And @intMax=5)
	Begin
		Insert @feedBack
		select * from @tmpfeedBack
		where Excelente > 0 And Poor=0 And Average=0 And Good=0 And Excelente = 0
		Order by Nombre
	End
	ret:
	return
End
