﻿--JRF-20160223-...And rd.FeUpdateRow is not null
CREATE function [dbo].[FnCambioenViaticosVentasAReservas](@IDReserva int)
returns bit
As
Begin
Declare @bCambiosVentas bit = 0
Declare @IDCab int =0, @IDProveedor char(6)=''
Select @IDCab = r.IDCab,@IDProveedor = r.IDProveedor
from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
Left Join COTICAB c On r.IDCab=c.IDCAB
where r.IDReserva=@IDReserva And r.Anulado=0

if (@IDCab <> 0 And @IDProveedor <> '')
Begin
	Declare @CoTipoHon char(3)=(select CoTipoHon from COTICAB_TOURCONDUCTOR where IDCab=@IDCab)
	if Exists(select * from COTICAB_TOURCONDUCTOR where IDCab=@IDCab)
	Begin
		Declare @CantidadViaticosTC int = (select count(*) from VIATICOS_TOURCONDUCTOR where IDCab=@IDCab and FlActivo = 1)
		Declare @ReservaDetViaticosTC int = (select count(*) from RESERVAS_DET where IDReserva=@IDReserva And Anulado=0 And NuViaticoTC > 0)
		--select * from RESERVAS_DET where IDReserva=@IDReserva Order By Dia asc
		if @CantidadViaticosTC <> @ReservaDetViaticosTC
		Begin
			Set @bCambiosVentas = 1
		End
		else
		Begin
			--select * from RESERVAS_DET where IDReserva=@IDReserva and NuViaticoTC>0 And Anulado=0
			--Declare @IDMonedaViatico char(3)='',@CostoViatico numeric(8,2)=0,@IDMonedaResDet char(3)='',@CostoResDet numeric(8,2)=0

			if exists(select x.IDMonedaRes from (
			select Case When vt.CostoUSD > 0 then 'USD' else 'SOL' End as IDMonedaVi,rd.IDMoneda as IDMonedaRes,(vt.CostoSOL+vt.CostoUSD) as CostoTotal,rd.TotalGen
			from VIATICOS_TOURCONDUCTOR vt Left Join (select * from RESERVAS_DET where IDReserva=@IDReserva and NuViaticoTC>0 And Anulado=0) rd
			On vt.IDServicio_Det = rd.IDServicio_Det And vt.NuViaticoTC = rd.NuViaticoTC
			where IDCab=@IDCab and vt.FlActivo = 1) as X
			where x.IDMonedaVi <> x.IDMonedaRes Or x.CostoTotal <> x.CostoTotal)
			Begin
				Set @bCambiosVentas = 1
			End
			--set @bCambiosVentas = 0
			--select Servicio,* 
			--from (select * from RESERVAS_DET where IDReserva=@IDReserva) rd 
			--Left Join (select * from VIATICOS_TOURCONDUCTOR where IDCab=@IDCab) vc On rd.IDServicio_Det=vc.IDServicio_Det
			--And CONVERT(char(10)
			--where rd.IDReserva=@IDReserva and Anulado=0

			if @bCambiosVentas = 0
			Begin
				Declare @ViaticosSoles numeric(8,2)=0,@ViaticosDolares numeric(8,2)=0,@SsMontoTotalHotel numeric(8,2)=0,@SsVuelo numeric(8,2)=0,@SsHonorario numeric(8,2)=0,
						@SsTicketAereos numeric(8,2)=0,@SsIGVHoteles numeric(8,2)=0,@SsTarjetaTelef numeric(8,2)=0,@SsTaxi numeric(8,2)=0,@SsOtros numeric(8,2)=0

				select @SsMontoTotalHotel=SsMontoTotalHotel,@SsVuelo=SsVuelo,@SsHonorario=SsHonorario,@ViaticosSoles=SsViaticosSOL,@ViaticosDolares=SsViaticosDOL,
					   @SsTicketAereos=SsTicketAereos,@SsIGVHoteles=SsIGVHoteles,@SsTarjetaTelef=SsTarjetaTelef,@SsTaxi=SsTaxi,@SsOtros=SsOtros
				from COTICAB_TOURCONDUCTOR
				where IDCab=@IDCab

				If cast((select SUM(TotalGen) from RESERVAS_DET Where IDReserva=@IDReserva And IsNull(NuViaticoTC,0)>0 And IDMoneda='SOL') as numeric(8,0)) <> @ViaticosSoles
					set @bCambiosVentas = 1
				If cast((select SUM(TotalGen) from RESERVAS_DET Where IDReserva=@IDReserva And IsNull(NuViaticoTC,0)>0 And IDMoneda='USD') as numeric(8,0))<> @ViaticosDolares
					Set @bCambiosVentas = 1

				
				if @bCambiosVentas = 0
				Begin
					Declare @TipoCambioCot numeric(8,2) = (select SsTipCam from COTICAB_TIPOSCAMBIO where IDCab=@IDCab and CoMoneda='SOL')
					Declare @IDReserva_Det int = (select IDReserva_Det from RESERVAS_DET 
												  where IDReserva=@IDReserva and Anulado=0 and IDServicio='LIM00918' And IDDet is not Null)

					--select sd.Descripcion,ROUND((rds.Total*rd.NroPax)/ @TipoCambioCot,1) as Total1,rds.* 
					--from RESERVAS_DETSERVICIOS rds Left Join RESERVAS_DET rd On rds.IDReserva_Det=rd.IDReserva_Det
					--Left Join COTIDET_DETSERVICIOS cds On rds.IDServicio_Det=cds.IDServicio_Det and rd.IDDet=cds.IDDET
					--Left Join MASERVICIOS_DET sd On rds.IDServicio_Det=sd.IDServicio_Det
					--where rds.IDReserva_Det=@IDReserva_Det
					
				 --   select * from (
					--select CoTipoCostoTC,x.Descripcion,X.Total / X.TipoCambio as Total1,MontoTotal from (
					--select sd.IDServicio_Det,sd.Descripcion,Case When sd.CoMoneda='USD' Or @CoTipoHon<>'002' Then 1 else @TipoCambioCot End As TipoCambio,
					--	   Round(rds.Total * rd.NroPax,0) as Total,
					--	   Round(Case sd.CoTipoCostoTC 
					--		When 'TAX' then @SsTaxi
					--		When 'TEL' then @SsTarjetaTelef
					--		When 'OTR' then @SsOtros
					--		When 'VUE' then @SsVuelo
					--		When 'NAD' then @SsMontoTotalHotel
					--		When 'IGV' then @SsIGVHoteles
					--		When 'HON' then @SsHonorario
					--		When 'TAE' then @SsTicketAereos
					--	   End,0) as MontoTotal,sd.CoTipoCostoTC
					--from RESERVAS_DETSERVICIOS rds Left Join RESERVAS_DET rd On rds.IDReserva_Det=rd.IDReserva_Det
					--Left Join MASERVICIOS_DET sd On rds.IDServicio_Det=sd.IDServicio_Det
					--where rds.IDReserva_Det=@IDReserva_Det And rd.FeUpdateRow is not null
					--) as X
					--Where x.MontoTotal is not null
					--) as Y
					--where y.Total1 <> y.MontoTotal

					If Exists(select * from (
					select CoTipoCostoTC,x.Descripcion,X.Total / X.TipoCambio as Total1,MontoTotal from (
					select sd.IDServicio_Det,sd.Descripcion,Case When sd.CoMoneda='USD' Or @CoTipoHon<>'002' Then 1 else @TipoCambioCot End As TipoCambio,
						   Round(rds.Total * rd.NroPax,0) as Total,
						   Round(Case sd.CoTipoCostoTC 
							When 'TAX' then @SsTaxi
							When 'TEL' then @SsTarjetaTelef
							When 'OTR' then @SsOtros
							When 'VUE' then @SsVuelo
							When 'NAD' then @SsMontoTotalHotel
							When 'IGV' then @SsIGVHoteles
							When 'HON' then @SsHonorario
							When 'TAE' then @SsTicketAereos
						   End,0) as MontoTotal,sd.CoTipoCostoTC
					from RESERVAS_DETSERVICIOS rds Left Join RESERVAS_DET rd On rds.IDReserva_Det=rd.IDReserva_Det
					Left Join MASERVICIOS_DET sd On rds.IDServicio_Det=sd.IDServicio_Det
					where rds.IDReserva_Det=@IDReserva_Det And rd.FeUpdateRow is not null
					) as X
					Where x.MontoTotal is not null
					) as Y
					where y.Total1 <> y.MontoTotal)
				Begin	
					Set @bCambiosVentas = 1
				End

				End
			End
		End
	End	
End
	--select @bCambiosVentas
	Return @bCambiosVentas
End 
