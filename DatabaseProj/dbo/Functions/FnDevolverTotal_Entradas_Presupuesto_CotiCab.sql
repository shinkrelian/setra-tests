﻿--JHD-20160307- select @SumaTotal= IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0)
CREATE FUNCTION dbo.FnDevolverTotal_Entradas_Presupuesto_CotiCab
  (
@idcab int,
@CoUbigeo_Oficina char(6)
)
Returns numeric(20,2)  
as
begin
declare @SumaTotal numeric(20,6)=0
 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)
 Declare @MargenInterno as numeric(6,2)=10

 select @SumaTotal= IsNull(Sum(Total),0)-IsNull(Sum(IGV_SFE),0)
 FROM              
  (              
  SELECT               
   ZZ.IDProveedor, 
   ZZ.CompraNeto, ZZ.IGVCosto,               
   ZZ.IGV_SFE, ZZ.Moneda,               
   ZZ.Total,              
   ZZ.idTipoOC,  ZZ.Margen,              
   ZZ.CostoBaseUSD,               
   ROUND( ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD              
   --,YY.FacturaExportacion,YY.BoletaNoExportacion              
  FROM              
   (              
   SELECT               
    YY.IDProveedor, 
    YY.CompraNeto, YY.IGVCosto,               
    YY.IGV_SFE,             
    --YY.CompraNeto+YY.IGVCosto+YY.IGV_SFE AS Total,              
    YY.Total,            
    YY.Moneda, YY.idTipoOC,  YY.Margen,              
    CompraNetoUSD+TotImptoUSD+IGV_SFE as CostoBaseUSD               
    --YY.VentaUSD,              
    --YY.FacturaExportacion,YY.BoletaNoExportacion              
   FROM              
    (              
    SELECT XX.IDProveedor, 
     CompraNeto, IGVCosto,               
     --Case When idTipoOC='001' Then               
     --  CompraNeto*(@IGV/100)               
     -- Else                
     --  Case When idTipoOC='006' Then (CompraNeto*(@IGV/100))*0.5 Else 0 End              
     -- End as IGV_SFE,               
     0 AS IGV_SFE,            
     Moneda, idTipoOC, Margen, Total,            
     CompraNetoUSD, TotImptoUSD               
     --CostoBaseUSD, VentaUSD,              
     --Case When XX.idTipoOC='001' Then VentaUSD Else 0 End As FacturaExportacion,              
     --Case When XX.idTipoOC='001' Then 0 Else VentaUSD End As BoletaNoExportacion              
    FROM              
     (                
     SELECT IDProveedor,  
      CompraNeto, IGVCosto,Moneda,               
      --CompraNetoUSD+TotImptoUSD as Total,               
      CompraNetoUSD as Total,               
      idTipoOC, Margen, CompraNetoUSD, TotImptoUSD              
      --CostoBaseUSD,              
      --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD              
     FROM              
      (              
      SELECT IDProveedor,
      SUM(CompraNeto) AS CompraNeto,              
      SUM(IGVCosto) AS IGVCosto, Moneda,               
      --SUM(CompraNeto+IGVCosto) AS Total,               
      idTipoOC,         
      AVG(Margen) AS Margen, sum(CompraNetoUSD) as CompraNetoUSD, sum(TotImptoUSD) as TotImptoUSD            
      --SUM(Total) as CostoBaseUSD              
      FROM              
       (              
       SELECT X.*,              
       --(SELECT AVG(MargenAplicado) FROM COTIDET cd1                           
       -- WHERE cd1.IDCAB=@IDCab AND cd1.IDProveedor=X.IDProveedorOpe  And isnull(IDDetRel,0)=0            
       -- ) AS Margen              
        --dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) AS Margen          
		 Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 
			dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe)
		 Else
			@MargenInterno
		 End AS Margen
        FROM              
   (              
        SELECT PCot.IDProveedor as IDProveedor,           
        ODS.IDOperacion_Det,              
        --isnull(ODS.NetoProgram ,ODS.CostoReal) as CompraNeto,              
        --Case When ODS.IDMoneda = 'USD' Then isnull(Ods.CostoReal ,0)               
   --Else dbo.FnCambioMoneda(isnull(Ods.CostoReal ,0),'USD','SOL',SD.TC)              
        --End --* OD.NroPax  
        --as CompraNeto,              
        --ODS.CostoReal as CompraNeto,                                  
    --    Case When isnull(NetoCotizado,0) = 0 then             
    --dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(OD.NroLiberados,0)),                           
    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,odS.IDMoneda,SD.TC)                   
    --else               
    --NetoCotizado               
    --End As CompraNeto,              
    --    isnull(Ods.CostoReal ,0) as CompraNetoUSD,            
 Case When ODS.IngManual=0 Then               
    Case When isnull(NetoCotizado,0) = 0 then                              
		--dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,            
		--OD.NroPax+isnull(od.NroLiberados,0)),                               
		----Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,ods.IDMoneda,isnull(@TipoCambio,SD.TC))                       
		--SD.CoMoneda,ods.IDMoneda,isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     
	 -- CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))           
	 --#Cambio#1
				dbo.FnCambioMoneda(
					--dbo.FnNetoMaServicio_Det(
					--	SD.IDServicio_Det,
					--	OD.NroPax+isnull(od.NroLiberados,0))
						IsNull((Select 
								Case When sd6.Monto_sgl is null then 
									(select Monto from MASERVICIOS_DET_COSTOS
									 where IDServicio_Det=sd6.IDServicio_Det And
									 (OD.NroPax+isnull(od.NroLiberados,0) between PaxDesde and PaxHasta)) 
								else
								IsNull(Monto_sgls,Monto_sgl) * (OD.NroPax+isnull(od.NroLiberados,0)) End
						 from MASERVICIOS_DET sd6 
						 where sd6.IDServicio_Det=sd.IDServicio_Det),0)
						,
				SD.CoMoneda,
				ods.IDMoneda,
				isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     
						CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))         
    else                   
   ods.NetoCotizado        
    End               
 Else        
  ODS.NetoProgram        
 End        
 As CompraNeto,       
                          
 Case When ODS.IngManual=0 Then               
  Case When isnull(NetoCotizado,0) = 0 then                              
		 -- dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(od.NroLiberados,0)),                               
		 -- --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,'USD',isnull(@TipoCambio,SD.TC))                       
		 -- SD.CoMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))    
		 -- else                   
		 --  --dbo.FnCambioMoneda(NetoCotizado,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))                     
		 --  dbo.FnCambioMoneda(NetoCotizado,ODS.IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
		 --And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                     
		  --#Cambio#1
			dbo.FnCambioMoneda(		  
				--dbo.FnNetoMaServicio_Det(
				--	SD.IDServicio_Det,
				--	OD.NroPax+isnull(od.NroLiberados,0)), 
						IsNull((Select 
								Case When sd6.Monto_sgl is null then 
									(select Monto from MASERVICIOS_DET_COSTOS
									 where IDServicio_Det=sd6.IDServicio_Det And
									 (OD.NroPax+isnull(od.NroLiberados,0) between PaxDesde and PaxHasta)) 
								else
								IsNull(Monto_sgls,Monto_sgl) * (OD.NroPax+isnull(od.NroLiberados,0)) End
						 from MASERVICIOS_DET sd6 
						 where sd6.IDServicio_Det=sd.IDServicio_Det),0),
			SD.CoMoneda,
			'USD',
			isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where     
					IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))
  End                     
 Else        
  --dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))        
  dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
 And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))        
 End        
  As CompraNetoUSD,    
                              
        --ODS.TotImpto as IGVCosto,                      
        Case When ODS.IDMoneda = 'USD' Then ODS.TotImpto               
        Else dbo.FnCambioMoneda(ODS.TotImpto,'USD','SOL',SD.TC)              
        End as IGVCosto,            
        ODS.TotImpto  AS TotImptoUSD,            
        ODS.IDMoneda as Moneda,            
        --ODS.CostoReal+ODS.TotImpto as Total ,              
        sd2.idTipoOC,              
        --SCot.IDProveedor as IDProveedorCot,              
        --PCot.NombreCorto as DescProveedorCot,              
        --SDCot.IDServicio_Det              
        ODS.IDServicio_Det,              
        O.IDProveedor as IDProveedorOpe       
        ,SD.IDServicio              
        FROM OPERACIONES_DET_DETSERVICIOS ODS                  
        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det               
        AND ODS.IDServicio=OD.IDServicio              
        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=@IDCab  
        --INNER JOIN MASERVICIOS S ON OD.IDServicio=S.IDServicio AND S.IDProveedor=O.IDProveedor              
        --INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              
        LEFT JOIN COTICAB CC ON CC.IDCAB=O.IDCab              
        --LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor              
         --INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det                  
         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Cot=SD.IDServicio_Det              
         Left JOIN MASERVICIOS_DET SD2 ON ODS.IDServicio_Det=SD2.IDServicio_Det              
         LEFT join MATIPOOPERACION tox on sd2.idTipoOC=tox.IdToc                     
        --INNER JOIN MASERVICIOS_DET SDCot ON ODS.IDServicio_Det_V_Cot=SDCot.IDServicio_Det              
        INNER JOIN MASERVICIOS SCot ON SD.IDServicio=SCot.IDServicio              
        INNER JOIN MAPROVEEDORES PCot ON SCot.IDProveedor=PCot.IDProveedor             
        And (PCot.IDTipoProv in ('006') OR PCot.IDProveedor='001593' ) --MUSEO  O CONSETTUR TRANSPORTISTA          
			And (PCot.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
        Left Join MAPROVEEDORES pint On pint.IDProveedor=pCOT.IDProveedorInternacional              
        --Where --not ODS.IDProveedor_Prg is null and not P.NombreCorto is null              
         --ODS.IDProveedor_Prg <> '001935'--SETOURS CUSCO     
  Left Join MAUBIGEO ubPI On PCot.IDCiudad=ubPI.IDubigeo  
        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo                  
         Where ODS.Activo=1  
		 and cc.estado<>'X'        
        ) AS X              
       ) AS Y              
      GROUP BY IDProveedor,
      Moneda,idTipoOC       
      ) as Z              
     ) AS XX              
    ) AS YY              
   ) AS ZZ                
  ) AS A                
	return @SumaTotal
end;

