﻿CREATE Function dbo.FnDescUbigeoHoteluOperAnterior(@IDCab int,@Dia smalldatetime,@Retro1Day bit)
returns varchar(max)
As	
Begin
	Declare @DiaAyer smalldatetime 
	if @Retro1Day=1
		Begin
		set @DiaAyer = dateadd(d,-1,Cast(Convert(Char(10),@Dia,103) as smalldatetime))
		End
	else
		Begin
		set @DiaAyer = Cast(Convert(Char(10),@Dia,103) as smalldatetime)
		End
	
	Declare @DescUbigeo varchar(max) = ''
	Declare @CantHotel tinyint = (select count(distinct u.Descripcion) from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
	Left Join MAUBIGEO u On cd.IDubigeo = u.IDubigeo Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
	where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) = @DiaAyer
	and (p.IDTipoProv = '001' and sd.PlanAlimenticio = 0))
	
	Declare @CantOper tinyint = (select count(distinct u.Descripcion) from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
	Left Join MAUBIGEO u On cd.IDubigeo = u.IDubigeo Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
	where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) = @DiaAyer
	and (p.IDTipoProv = '003' and sd.ConAlojamiento = 1))
	
	if @CantHotel = 1 
	Begin
		set @DescUbigeo = (select distinct u.Descripcion from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
				Left Join MAUBIGEO u On cd.IDubigeo = u.IDubigeo Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
				where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) = @DiaAyer
				and (p.IDTipoProv = '001' and sd.PlanAlimenticio = 0))			
		Goto FinDescUbigeo
	End		
	if @CantOper = 1
	Begin
		Declare @HotelAntg varchar(max) = '',@DescUbigOrig varchar(max) = '',@DescUbigDest varchar(max)
		if @Retro1Day = 0
		Begin
			if (select count(distinct u.Descripcion) from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
			Left Join MAUBIGEO u On cd.IDubigeo = u.IDubigeo Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
			where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) =dateadd(d,-1,@DiaAyer)
			and (p.IDTipoProv = '001' and sd.PlanAlimenticio = 0)) > 0
				Begin
					Set @HotelAntg = (select distinct u.Descripcion from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
					Left Join MAUBIGEO u On cd.IDubigeo = u.IDubigeo Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
					where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) =dateadd(d,-1,@DiaAyer)
					and (p.IDTipoProv = '001' and sd.PlanAlimenticio = 0))
				End
			Else
				Begin
					Set @HotelAntg = (select Top(1) u.Descripcion from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
					Left Join MAUBIGEO u On cd.IDUbigeoDes = u.IDubigeo Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
					where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) =dateadd(d,-1,@DiaAyer)
					and (p.IDTipoProv = '003' and sd.ConAlojamiento = 1))
				End
		End
		else
		Begin
			Set @HotelAntg = (select distinct u.Descripcion from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
			Left Join MAUBIGEO u On cd.IDubigeo = u.IDubigeo Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
			where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) = @DiaAyer
			and (p.IDTipoProv = '001' and sd.PlanAlimenticio = 0))
		End
		
		
		--
		Set @DescUbigOrig = (select Top(1) u2.Descripcion from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
			Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det Left Join MAUBIGEO u2 On cd.IDUbigeoOri = u2.IDubigeo
			where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) = @DiaAyer
			and (p.IDTipoProv = '003' and sd.ConAlojamiento = 1))
			
	   --
		if upper(ltrim(rtrim(@HotelAntg))) <> upper(ltrim(rtrim(@DescUbigOrig)))
		Begin
			set @DescUbigeo = (select Top(1) u3.Descripcion + ' - '+ u2.Descripcion from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
				Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det Left Join MAUBIGEO u2 On cd.IDUbigeoDes = u2.IDubigeo
				Left Join MAUBIGEO u3 On cd.IDUbigeoOri = u3.IDubigeo
				where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) = @DiaAyer
				and (p.IDTipoProv = '003' and sd.ConAlojamiento = 1))
		End
		else
		Begin
			set @DescUbigeo = (select Top(1) u2.Descripcion from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor
				Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det Left Join MAUBIGEO u2 On cd.IDUbigeoDes = u2.IDubigeo
				where IDCAB = @IDCab and cd.IncGuia = 0 and cast(CONVERT(Char(10),Dia,103) as smalldatetime) = @DiaAyer
				and (p.IDTipoProv = '003' and sd.ConAlojamiento = 1))		
		End
		Goto FinDescUbigeo
	End
	
FinDescUbigeo:
	if ltrim(rtrim(@DescUbigeo))='--------'
	   set @DescUbigeo = ''
	   
	return @DescUbigeo
End
