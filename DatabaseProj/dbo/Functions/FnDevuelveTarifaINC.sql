﻿Create Function dbo.FnDevuelveTarifaINC(@IDServicio char(8),
										@Anio char(4),
										@Variante varchar(15))
returns numeric(12,4)
As
Begin
	Declare @TarifaINC numeric(12,4)=0
	Select @TarifaINC=IsNull(Monto_sgls,Monto_sgl)
	from MASERVICIOS_DET 
	Where IDServicio=@IDServicio And Anio=@Anio And Tipo=@Variante
	Return @TarifaINC
End
