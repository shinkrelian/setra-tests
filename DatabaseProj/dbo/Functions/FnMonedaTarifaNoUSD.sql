﻿Create Function [dbo].[FnMonedaTarifaNoUSD](@IDServicio_Det int, @IDCab int)
	returns decimal(6,3)
As
	Begin	
	
	Declare @IDServicio char(8),@Variante varchar(7),@Anio char(4), @CoMoneda char(3)='', @SsTipCam decimal(6,3)=0

	Select @CoMoneda=CoMoneda,@IDServicio=IDServicio,@Variante=tipo,
			@Anio = Anio
	From MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det

	If @CoMoneda='USD'
		Begin
		SELECT Top 1 @CoMoneda = CoMoneda FROM MASERVICIOS_DET where IDServicio=@IDServicio and tipo=@Variante
			and Anio=@Anio and CoMoneda<>'USD'
		If @CoMoneda<>'USD' and @CoMoneda<>''
			Select @SsTipCam=SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab and CoMoneda=@CoMoneda
		End
	Else
		Begin
		If @CoMoneda<>''
			Select @SsTipCam=SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab and CoMoneda=@CoMoneda
		End

	Return @SsTipCam

	End