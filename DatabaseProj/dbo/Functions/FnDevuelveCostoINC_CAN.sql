﻿CREATE Function dbo.FnDevuelveCostoINC_CAN(@IDDet int,@IDServicio char(8))
returns numeric(8,2)
As
Begin
	Declare @CostoAdulto numeric(8,2)=0,@CostoNinio numeric(8,2)=0,@CostoTotal numeric(8,2)=0
	Declare @NroPax tinyint =(Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
							  Where IDDET=@IDDet And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
							  And Not(cp.Titulo='chd.' And cp.FlAdultoINC=0))
	Declare @NroPaxNinios tinyint =(Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
							  Where IDDET=@IDDet And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
							  And (cp.Titulo='chd.' And cp.FlAdultoINC=0))
	
	Declare @Variante varchar(Max)='CAN', @Anio char(4)=''
	Select @Anio=Cast(Year(dia) as char(4)) from cotidet where IDDET=@IDDet

	Select @CostoAdulto=Monto_sgls
	from MASERVICIOS_DET
	where IDServicio=@IDServicio and Anio=@Anio and Tipo=@Variante

	Set @Variante='CHD-CAN'
	Select @CostoNinio=Monto_sgls
	from MASERVICIOS_DET
	where IDServicio=@IDServicio and Anio=@Anio and Tipo=@Variante

	Set @CostoTotal= (IsNull(@CostoAdulto,0)*@NroPax + IsNull(@CostoNinio,0)*@NroPaxNinios)
	return @CostoTotal
End
