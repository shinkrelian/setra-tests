﻿Create function dbo.FnExisteSoloAcomodoEspecial(@IDReserva int)
returns bit
As
Begin
Declare @bExistSoloAcomoEsp bit = 0
	If Not Exists(select rd.IDReserva_Det
	from RESERVAS_DET rd 
	Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
	Left Join COTIDET cd On rd.IDDet=cd.IDDET
	where rd.IDReserva=@IDReserva and FlServicioNoShow=0 and FlServicioIngManual=0
	And ((p.IDTipoProv='001' And sd.PlanAlimenticio=0) Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1))
	And IsNull(cd.IncGuia,0)=0 And cd.AcomodoEspecial=0)
		Set @bExistSoloAcomoEsp = 1

Return @bExistSoloAcomoEsp
End
