﻿Create Function dbo.FnDatosIncompletosxFileMAPI(@IDCab int)
	returns bit
As
	Begin
	Declare @bIncompletos bit=0
	
	If Not Exists(
		Select IDPax From COTIPAX Where IDCab=@IDCab And (
			(FlEntMP=1 And FlAutorizarMPWP=1) 
		)
			)		
		Set @bIncompletos=1

	Return @bIncompletos
	End

