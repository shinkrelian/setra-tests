﻿
CREATE FUNCTION [dbo].[FnLiberadoAPT](@PoliticaLiberado bit,
									  @Liberado tinyint,
									  @MaximoLiberado tinyint,
									  @CapacidadHab tinyint,
									  @EsMatrimonial bit,
									  @NroPax smallint)
	RETURNS int
AS
BEGIN
	DECLARE @NroLiberado int
	SET @NroLiberado = 0
	IF @CapacidadHab = 1 AND @EsMatrimonial = 0
	BEGIN
		IF @PoliticaLiberado = 1 AND @Liberado > 0
		BEGIN
			IF @NroPax > @Liberado
			BEGIN
				IF @MaximoLiberado = 0
				BEGIN
					SELECT @NroLiberado = @NroPax/@Liberado
				END
				ELSE
				BEGIN
					SELECT @NroLiberado = @MaximoLiberado
				END
			END
		END
	END
	Return @NroLiberado
END
