﻿Create Function [dbo].[FnExisteNuevaTarifa](@IDOperacion_Det int)
	returns bit
As
	Begin	
	
	Declare @ExisteTarifasNuevas bit=0		
	
	If Exists(	
		Select NewIDServicio_Det_V_Cot From 
			(
			Select dbo.FnNuevaTarifa(sd.IDServicio,sd.Tipo,sd.Anio,od.Dia,ods.IDServicio_Det_V_Cot,sd.Codigo) as NewIDServicio_Det_V_Cot
			From OPERACIONES_DET_DETSERVICIOS ods Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=ods.IDServicio_Det_V_Cot
			Inner Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
			Where ods.IDOperacion_Det = @IDOperacion_Det And ods.Activo=1
			) as X 
		Where X.NewIDServicio_Det_V_Cot<>0
		)
		Set @ExisteTarifasNuevas = 1		

	Return @ExisteTarifasNuevas 

	End

