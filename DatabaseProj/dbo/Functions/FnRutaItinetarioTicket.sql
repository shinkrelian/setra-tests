﻿CREATE Function [dbo].[FnRutaItinetarioTicket] (@CoTicket Char(14))
	Returns varchar(150)
As 
	Begin
		Declare @CoCiudadPar char(3), @CoCiudadLle char(3)--, @siCont smallint=1
		Declare @ItinerTicket varchar(150)=''
		
		Declare curItiner Cursor For
		Select CoCiudadPar,CoCiudadLle FROM TICKET_AMADEUS_ITINERARIO 
		Where CoTicket=@CoTicket
		Order by NuSegmento
	
		Open curItiner
		Fetch Next From curItiner Into @CoCiudadPar, @CoCiudadLle
		While @@Fetch_Status=0
			Begin
			--If (@siCont % 2) = 0
				Set @ItinerTicket = @ItinerTicket+@CoCiudadPar+'-'
			--Else
				
			
			--Set @siCont+=1
			Fetch Next From curItiner Into @CoCiudadPar, @CoCiudadLle
			End

		Close curItiner
		Deallocate curItiner
		
		Set @ItinerTicket = @ItinerTicket+@CoCiudadLle
		
		Return @ItinerTicket
	End

