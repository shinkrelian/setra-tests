﻿Create Function dbo.FnTodosContieneTPLxIDCab(@IDCab int)
returns bit
as
Begin
Declare @ContienteTPL bit = 1


If Exists(select X.ContieneTPL from 
(select Case When (sd.IDServicio_DetxTriple is null And sd.Monto_tri is null And            
					sd.Monto_tris is null And sd.IdHabitTriple = '000') Then 0 Else 1 End as ContieneTPL
			from COTIDET cd Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
			Left Join MAPROVEEDORES p ON cd.IDProveedor = p.IDProveedor
			where IDCab = @IDCab and (p.IDTipoProv='001' Or (p.IDTipoProv='003' and sd.ConAlojamiento=1))) as X
			Where X.ContieneTPL =0)
	Set @ContienteTPL = 0

return @ContienteTPL
End
