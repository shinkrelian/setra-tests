﻿--JRF-20141018-Ajuste para los casos con Paxs Residentes en la función [dbo.FnAcomodosCabeceraCoti]          
--JRF-20141128-Comparar el CostoNeto [Ventas >> Reservas]        
--JRF-20141203-Asignar La fecha FeUpdateRow Al evaluar el Costo      
--JRF-20141204-Ajustar la funcion para comparar diferentes precios.      
--JRF-20141223-Ajustar la función para las Reservas de Protección      
--JRF-20150117-Considerar el Tipo de cambio para la comparación de costos    
--JRF-20150204-Considerar Valor doble para Alojamiento  
--JRF-20150211-Ajustes adicionales para casos de varios alojamientos en un solo día.  
--JRF-20150218-No Considerar las reservas ingresadas manualmente.
--JRF-20150223-No considerar dentro del algoritmo las reservas de proteccion [Sub-Query]
--JRF-20150521-Considerar MontoSgl con Guía .. Case When cd.IncGuia = 1 Then IsNull(sd.Monto_sgl,sd.Monto_sgls) Else cd.CostoReal End as CostoReal
--JRF-20150521-Considerar una tabla temporal adicional para los casos de acomodo especial
--JRF-HLF-20150715-..@FeUpdatRowTmp2 
--JRF-20150907-Evaluar cant. de Sub-Servicios adicionales[Operadores con Alojamiento]
--JRF-20150909-Evaluar cant. de Sub-Servicios adicionales[Operadores con Alojamiento] - Desde COTIDET_DETSERVICIOS
--JRF-20150910-Evaluar cant. de Sub-Servicios adicionales[Operadores con Alojamiento] - Contra RESERVAS_DETSERVICIOS
--JRF-20150910-Evaluar cant. de Sub-Servicios adicionales[Operadores con Alojamiento] - sumar servicios no generados (CostoReal = 0)
--JRF-20150930-Considerar Generación de Cantidades A Pagar x Liberado en Agrupado (dia,servicio) [Pax - Cant. de Hab.] (Nueva Tabla tmp = @TmpGeneracionCantidadPagarHotel)
--HLF-20151001-@TmpGeneracionCantidadPagarHotel servicio(de 100 a max)
--variables tinyint a smallint
--JRF-20151006-else..((@IDProveedor<>'' and @TipoProv<>'') and @IDCab <> 0) and @bExisteProveedor = 0..
--JRF-20151012-if @sumCantidadPax >= (@Liberado+1)
--JRF-20151028-...And x.IDReserva=@IDReserva Primer Query]
--JRF-20151102-@FecMod datetime[Antes smalldatetime] ^ @FeUpdateRow RESERVAS_DE datetime
--JRF-20151102-..If Exists(Select IDReserva_Det from RESERVAS_DET rd Inner Join COT.. [verifar tarifa para triple]
--HLF-20151110-If @CantiReservaDet>0
--JRF-20160201-..(CantidadPagarResv<>CantidadHab and CantidadHab>0)
Create Function dbo.FnCambiosenVentasPostFileReservasSoloAlojamiento(@IDReserva int)
returns bit
As
Begin

 Declare @bCambiosVentas bit = 0            
 Declare @IDProveedor char(6)='',@IDCab int=0,@RowCountCotiDet int = 0,@bCambioCosto bit=0,@SoloGuia bit=0    
 Declare @TipoProv varchar(3)='',@TipoProvIgv char(3)=''
             
 Declare @tmpGeneracionReservasHTL table (IDDet int,IDServicio_Det int,FechaIn smalldatetime,FechaOut smalldatetime)
 Declare @tmpGeneracionReservasEspecialesHTL table (IDDet int,IDServicio_Det int,FechaIn smalldatetime,FechaOut smalldatetime)
 Declare @tmpAcomodoCab table(Cantidad  smallint,Acomodo varchar(40),ItemLoc bit)      
 Declare @tmpAcomodosEspeciales table(Cantidad smallint,Acomodo varchar(40),ItemLoc bit)      
 Declare @tmpCostos table(IDDet int,Cantidad smallint,TipoAcomodo varchar(40),Costo numeric(8,4),ItemLoc bit)      
 Declare @TmpIngresosOperAlojamiento 
 table(IDDet int,Dia smalldatetime,Servicio varchar(Max),NroPax smallint,IDServicio_Det int,IDIdioma varchar(12),FecMod smalldatetime,IDReserva int,ServicioHospedaje bit)
 Declare @TmpGeneracionCantidadPagarHotel table(IDReservaDet int,Dia smalldatetime,
	Servicio varchar(max),--CapacidadHab smallint,
	CapacidadHab smallint,
	EsMatrimonial bit,IDServicio_Det int,IDDet int,CantidadPagarResv smallint,CantidadHab smallint,CantidadPax int)
 
 --select FeUpdateRow,* from RESERVAS_DET where IDReserva=@IDReserva and Anulado=0
              
 Select @IDProveedor=r.IDProveedor,@IDCab=r.IDCab,@TipoProv=Ltrim(Rtrim(p.IDTipoProv)),@TipoProvIgv = Ltrim(Rtrim(p.IDTipoProv))
 from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
 Where r.IDReserva =@IDReserva and r.Anulado = 0 and r.Estado <> 'XL' and r.IDReservaProtecc is null          
 Declare @IDDet int=0,@Dia smalldatetime='01/01/1900',@Servicio varchar(Max)='',@NroPax int=0,@IDServicio_Det int=0,@IDIdioma varchar(12)='',            
      @FecMod datetime='01/01/1900',@CostoReal numeric(12,4)=0,@SSCR numeric(12,4)=0,@STCR numeric(12,4)=0,@IDReserva_Det int=0,@IDReservaTmp int=0,      
      @RowCountCotiDetTemp int = 0,@IDDetTemp int=0,@DiaReservaDet smalldatetime='01/01/1900',@FechaOutReservaDet smalldatetime='01/01/1900',      
      @DiaTemp smalldatetime='01/01/1900',@DiaTemp2 smalldatetime='01/01/1900',@IDServicio_DetTmp int=0,@IDTipoProv char(3)='',      
      @ServicioReservaDet varchar(Max),@CantiReservaDet smallint=0,@CantiPagarReservaDet smallint=0,@ExisteAcoNormales bit = 0,@CostoRealEditCot bit=0,
	  @FlSSCREditado bit=0,@FlSTCREditado bit=0,    
      @TCambio numeric(8,2)=0 
                  
 Declare @RowCountReservaDet int = (select count(*) from (            
            select Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
   then 1 else 0 End As ServicioHotespedaje from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det            
            Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
            Where rd.IDReserva=@IDReserva and rd.Anulado=0 and rd.FlServicioIngManual=0) as X            
            Where X.ServicioHotespedaje=1)            
                 
 --Execute dbo.tmpGeneracionReservasHTL_Del @IDCab            
 Declare @bExisteProveedor bit = 0      
 If Exists(select IDDET from COTIDET where IDProveedor=@IDProveedor and IDCAB=@IDCab)            
  Set @bExisteProveedor = 1
             
 if ((@IDProveedor<>'' and @TipoProv<>'') and @IDCab <> 0) and @bExisteProveedor = 1 and @bCambiosVentas=0            
 Begin            
  --select * from dbo.FnAcomodosCabeceraCoti(@IDCab)          
  --Inserto Los acomodos Comunes        
  Insert into @tmpAcomodoCab            
  select * from dbo.FnAcomodosCabeceraCoti(@IDCab)            
  
  --select * from dbo.FnAcomodosCabeceraCoti(@IDCab)            
 -- 	Select cd.IDDET,cda.QtPax,ch.NoCapacidad,0
	--from COTIDET cd Left Join COTIDET_ACOMODOESPECIAL cda On cda.IDDet=cd.IDDET
	--Left Join CAPACIDAD_HABITAC ch On cda.CoCapacidad=ch.CoCapacidad
	--Where IDCAB= @IDCab and IDProveedor=@IDProveedor and AcomodoEspecial=1
  --select FeUpdateRow,* from RESERVAS_DET where IDReserva=@IDReserva and Anulado=0

  --Inserto los acomodos Especiales            
  Insert into @tmpAcomodosEspeciales            
  select * from dbo.FnAcomodosEspecialesxProveedor(@IDCab,@IDProveedor) 

  Set @RowCountCotiDet = (Select count(*) from (            
        Select cd.IDDet,cd.Dia,cd.Servicio,cd.NroPax,cd.IDServicio_Det,cd.IDIdioma,cd.FecMod,--rd.IDReserva_Det,            
            r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)    
            then 1 else 0 End As ServicioHotespedaje         
        from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
        Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det                
        Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'            
        --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva            
        Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and r.Anulado= 0 and r.IDReservaProtecc is null --and rd.Anulado =0            
        ) as X            
        where X.ServicioHotespedaje = 1)
                 
   Insert into @TmpIngresosOperAlojamiento
   Select X.* from (            
        Select cd.IDDet,cd.Dia,cd.Servicio,cd.NroPax,cd.IDServicio_Det,cd.IDIdioma,cd.FecMod,--rd.IDReserva_Det,            
            r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)    
            then 1 else 0 End As ServicioHotespedaje         
        from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
        Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det     
   Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'            
        --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva            
        Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and r.Anulado= 0 and r.IDReservaProtecc is null --and rd.Anulado =0            
        ) as X            
        where X.ServicioHotespedaje = 1

  --print @RowCountCotiDet            
  Declare curItemsExist cursor for            
  Select X.IDDET,X.Dia,X.Servicio,X.IDServicio_Det,X.IDidioma,X.CostoReal,X.SSCR,X.STCR,X.FecMod,X.IDReserva,X.IDTipoProv            
  from (            
   Select cd.IDDet,cd.Dia,cd.Servicio,cd.NroPax,cd.IDServicio_Det,cd.IDIdioma,cd.FecMod,            
       r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
       then 1 else 0 End As ServicioHotespedaje,p.IDTipoProv ,Case When cd.IncGuia = 1 Then IsNull(sd.Monto_sgl,sd.Monto_sgls) Else cd.CostoReal End as CostoReal
	   ,cd.SSCR,cd.STCR          
   from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
   Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det                
   --Left Join RESERVAS_DET rd On rd.IDDet= cd.IDDet            
   Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'    
   --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva        
   Where cd.IDProveedor=@IDProveedor And cd.Transfer=0 and cd.IDCAB=@IDCab 
   and IsNull(r.Anulado,0) = 0 and cd.AcomodoEspecial=0 and r.IDReservaProtecc is null --and rd.Anulado =0            
   ) as X            
  Where X.ServicioHotespedaje=1 And x.IDReserva=@IDReserva
  Order by X.IDServicio_Det,X.Dia  
              
  Open curItemsExist            
  Fetch Next From curItemsExist into @IDDet,@Dia,@Servicio,@IDServicio_Det,@IDIdioma,@CostoReal,@SSCR,@STCR,@FecMod,@IDReservaTmp,@IDTipoProv      
  While @@Fetch_Status=0            
  Begin            
  Set @ExisteAcoNormales = 1            
   Set @SoloGuia = 0    
   Declare @DayDiff int=0            
   --select @bCambiosVentas            
   --select @Dia,CAST(convert(char(10),Dia,103) as smalldatetime),FechaOut from RESERVAS_DET            
   --      where IDReserva = @IDReservaTmp And Anulado=0 and IDServicio_Det=@IDServicio_Det            
   Declare @IncGuia bit = (select IncGuia from COTIDET where IDDet=@IDDet)            
   If @IncGuia=1 and Not Exists(select * from @tmpAcomodoCab where Cantidad=1 and Acomodo='SINGLE')            
   Begin            
    Insert into @tmpAcomodoCab values(1,'SINGLE',0)    
    Set @SoloGuia = 1    
   End            
               
   --select @Dia,@IDReservaTmp,IDReserva_Det  AS RESULT,Case When @IDTipoProv='001' then dateadd(d,-1,FechaOut) else FechaOut End from RESERVAS_DET            
   --      where IDReserva = @IDReservaTmp And Anulado=0 and IDServicio_Det=@IDServicio_Det             
   --      and @Dia between CAST(convert(char(10),Dia,103) as smalldatetime)             
   --      and Case When @IDTipoProv='001' then dateadd(d,-1,FechaOut) else FechaOut End

   --select @IncGuia
   if @bCambiosVentas=0            
   Begin           
    If not Exists(select IDReserva_Det  from RESERVAS_DET            
         where IDReserva = @IDReservaTmp And Anulado=0 and IDServicio_Det=@IDServicio_Det             
         and @Dia between CAST(convert(char(10),Dia,103) as smalldatetime)             
         and Case When @IDTipoProv='001' then dateadd(d,-1,FechaOut) else FechaOut End) And @IDTipoProv <> '003'      
       set @bCambiosVentas=1             
   End            

 --  if @bCambiosVentas=1
 --  Begin
 --   select @Dia,@IDReservaTmp
	----select IDReserva_Det,@IDTipoProv  from RESERVAS_DET where IDReserva = @IDReservaTmp And Anulado=0 and IDServicio_Det=@IDServicio_Det             
 ----        and @Dia between CAST(convert(char(10),Dia,103) as smalldatetime)             
 ----        and Case When @IDTipoProv='001' then dateadd(d,-1,FechaOut) else FechaOut End
 --  End
               
   --select @IDTipoProv
   --Generación de los Segmentos de Entradas y Salidas(Inicio)            
   if @IDTipoProv='001'            
   Begin           
   --SELECT @IDServicio_DetTmp  
    if convert(char(10),@DiaTemp,103)='01/01/1900' and @IDServicio_DetTmp=0            
    Begin 
     Set @DiaTemp = @Dia 
     Set @DiaTemp2 = @Dia --Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
     Set @IDServicio_DetTmp = @IDServicio_Det            
     Insert into @tmpGeneracionReservasHTL             
     values(@IDDet,@IDServicio_DetTmp,@Dia,dateadd(d,1,@Dia))     
     --SELECT @IDDet,@IDServicio_DetTmp,@Dia  
    End            
    Else            
    Begin            
     --select @DiaTemp,@Dia,@DiaTemp2            
     set @DayDiff = (select DATEDIFF(d,@DiaTemp,@Dia))            
     --print ':'+ cast(@DayDiff as varchar)            
     if @DayDiff = 1          
     Begin            
      if Not Exists(select IDDet from @tmpGeneracionReservasHTL where IDServicio_Det=@IDServicio_Det)            
      Begin            
       Set @DiaTemp = dateadd(d,1,@Dia)  
       Set @IDServicio_DetTmp = @IDServicio_Det  
       Insert into @tmpGeneracionReservasHTL             
       values(@IDDet,@IDServicio_DetTmp,@Dia,dateadd(d,1,@Dia))     
      End            
      else    
        Begin          
    Begin            
     Update @tmpGeneracionReservasHTL set FechaOut = Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End             
     where FechaIn=@DiaTemp2 and IDServicio_Det=@IDServicio_DetTmp            
    End                 
    Set @DiaTemp = @Dia            
    Set @IDServicio_DetTmp = @IDServicio_Det            
  End  
     End 
       
     else            
       
     Begin  
      if Not Exists(select IDDet from @tmpGeneracionReservasHTL where IDServicio_Det=@IDServicio_Det) Or  @DayDiff > 1  
      Begin  
  Insert into @tmpGeneracionReservasHTL             
    values(@IDDet,@IDServicio_Det,@Dia,Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End)            
    Set @DiaTemp = @Dia--Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
    set @DiaTemp2 = @Dia--Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
    Set @IDServicio_DetTmp = @IDServicio_Det    
      End  
      else  
      Begin  
   --select @IDServicio_Det,@Dia  
    Update @tmpGeneracionReservasHTL set FechaOut = dateadd(d,1,@Dia) where IDServicio_Det =@IDServicio_Det   
      End  
     End      
             
    End          
   End  
       
   else        
   --Operadores con Alojamiento      
   Begin            
    if convert(char(10),@DiaTemp,103)='01/01/1900' and @IDServicio_DetTmp=0            
    Begin             
     Set @DiaTemp = @Dia            
     Set @DiaTemp2 = @Dia            
     Set @IDServicio_DetTmp = @IDServicio_Det            
     Insert into @tmpGeneracionReservasHTL             
     values(@IDDet,@IDServicio_DetTmp,@Dia,Case When @RowCountCotiDet=1 then dateadd(d,1,@Dia)else @Dia End)            
    End            
    Else            
    Begin           
	 --Select @DiaTemp,@Dia 
     set @DayDiff = (select DATEDIFF(d,@DiaTemp,@Dia))         
     --print ':'+ cast(@DayDiff as varchar)            
     --select @DayDiff,@Servicio
     if @DayDiff = 1             
     Begin            
      if Not Exists(select IDDet from @tmpGeneracionReservasHTL where IDServicio_Det=@IDServicio_Det)            
   Begin            
       Insert into @tmpGeneracionReservasHTL             
       values(@IDDet,@IDServicio_DetTmp,@Dia,Case When @RowCountCotiDet=1 then dateadd(d,1,@Dia)else @Dia End)            
      End            
      else            
      Begin            
       Update @tmpGeneracionReservasHTL set FechaOut = Case When @RowCountCotiDet=1 then dateadd(d,1,@Dia)else @Dia End             
       where FechaIn=@DiaTemp2 and IDServicio_Det=@IDServicio_DetTmp            
      End                 
      Set @DiaTemp = @Dia--Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End             
      Set @IDServicio_DetTmp = @IDServicio_Det            
     End     
   else            
     Begin  
	  --Select @RowCountCotiDet,@Dia,@IDServicio_Det
	            
      Insert into @tmpGeneracionReservasHTL             
      values(@IDDet,@IDServicio_Det,@Dia,Case When (Select COUNT(*) from @TmpIngresosOperAlojamiento where IDServicio_Det=@IDServicio_Det)=1 then dateadd(d,1,@Dia)else @Dia End)  
      Set @DiaTemp = @Dia  
      set @DiaTemp2 = @Dia  
      Set @IDServicio_DetTmp = @IDServicio_Det     
    End
	 
	            
    End            
   End            
   --Generación de los Segmentos de Entradas y Salidas(Fin)        
   --Select Case When FeUpdateRow is null then -1 else datediff(s,@FecMod,FeUpdateRow)End       
         
  --Select Isnull(FeUpdateRow,@FecMod) as FeUpdateRow,netohab,Cantidad,CapacidadHab,CostoRealEditado,FlSSCREditado,FlSTCREditado,IsNull(sd.TC,0) as TCambio    ,IDReserva_Det
  --from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDet      
  --Left Join MASERVICIOS_DET sd On rd.IDServicio_Det =sd.IDServicio_Det    
  --Left Join RESERVAS r On rd.IDReserva =r.IDReserva
  --where r.IDReservaProtecc is null and rd.IDDet=@IDDet and rd.Anulado=0 and r.anulado=0 and (cd.CostoReal > 0) And r.IDReserva=@IDReserva
  --select * from RESERVAS_DET where IDReserva_Det=158157
   --Set @bCambioCosto=0      
   
   --Cambio del CostoReal (Inicio)        
   If Exists(select IDReserva_Det from RESERVAS_DET where IDDet=@IDDet and Anulado=0)
   Begin        
   Declare @FeUpdateRowTemp datetime='01/01/1900',@CostoRealResev numeric(8,2),@Cantidad smallint=1,@Capacidad smallint=''      

  --Select Isnull(FeUpdateRow,@FecMod) as FeUpdateRow,netohab,Cantidad,CapacidadHab,CostoRealEditado,FlSSCREditado,FlSTCREditado,IsNull(sd.TC,0) as TCambio    
  --from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDet      
  --Left Join MASERVICIOS_DET sd On rd.IDServicio_Det =sd.IDServicio_Det    
  --Left Join RESERVAS r On rd.IDReserva =r.IDReserva
  --where r.IDReservaProtecc is null and rd.Transfer=0 and rd.IDDet=@IDDet and rd.Anulado=0 and r.anulado=0 and (cd.CostoReal > 0)

  	 If Exists(Select IDReserva_Det from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDet    
			 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det    
			 Left Join RESERVAS r On rd.IDReserva=r.IDReserva
			 where r.IDReservaProtecc is null and rd.IDDet=@IDDet 
			 and rd.anulado=0 and rd.Transfer=0 and r.Anulado=0 and (cd.CostoReal > 0) And (NetoHab=0 and IsNull(sd.Monto_tri,sd.Monto_tris)>0 And CapacidadHab=3))
	 Begin
		Set @bCambioCosto=1
	 End
         
  --Detalles por ingreso (Inicio)      
  Declare curItemsResDet cursor for      
  Select Isnull(FeUpdateRow,@FecMod) as FeUpdateRow,netohab,Cantidad,CapacidadHab,CostoRealEditado,FlSSCREditado,FlSTCREditado,IsNull(sd.TC,0) as TCambio    
  from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDet      
  Left Join MASERVICIOS_DET sd On rd.IDServicio_Det =sd.IDServicio_Det    
  Left Join RESERVAS r On rd.IDReserva =r.IDReserva
  where r.IDReservaProtecc is null and rd.Transfer=0 and rd.IDDet=@IDDet and rd.Anulado=0 and r.anulado=0 and (cd.CostoReal > 0)
  Open curItemsResDet      
  Fetch Next From curItemsResDet into @FeUpdateRowTemp,@CostoRealResev,@Cantidad,@Capacidad,@CostoRealEditCot,@FlSSCREditado,@FlSTCREditado,@TCambio    
  While @@FETCH_STATUS=0      
  Begin       

   --select @Capacidad,@CostoRealResev As NetoHab,@CostoReal as CostoReal,@SSCR As CostoSimple,@STCR as CostoTriple,@FeUpdateRowTemp,@FecMod      
   Declare @FecModUpdate integer = (Select datediff(s,@FecMod,@FeUpdateRowTemp))      
   --select @FecModUpdate,@FecMod,@FeUpdateRowTemp,@TCambio,@bCambioCosto
   If @FecModUpdate<>0 and @bCambioCosto=0      
	Begin      
	  if @IDTipoProv='003'	
	  Begin
		Declare @CostoRealLocal numeric(12,2) = dbo.FnDevuelveCostoNetoOperadoConAlojamiento(@IDDet,@IDServicio_Det,@Capacidad)
		if Not Exists(select IDServicio_Det from MASERVICIOS_DET_COSTOS where IDServicio_Det =@IDServicio_Det )
		Begin
			set @CostoRealLocal= @CostoRealLocal * @Capacidad
		End
		--Select * from MASERVICIOS_DET_COSTOS where IDServicio_Det=@IDServicio_Det
	    --Select @CostoRealResev,dbo.FnDevuelveCostoNetoOperadoConAlojamiento(@IDDet,@IDServicio_Det,@Capacidad)
		if @CostoRealResev <> @CostoRealLocal--dbo.FnDevuelveCostoNetoOperadoConAlojamiento(@IDDet,@IDServicio_Det,@Capacidad)
		Begin
			--Select @CostoRealResev,@CostoRealLocal,@Servicio,@Capacidad,@IDDet as IDDet,@IDServicio_Det as IDServicio_Det
			--Select @CostoRealResev,@IDDet,@IDServicio_Det,@Capacidad,dbo.FnDevuelveCostoNetoOperadoConAlojamiento(@IDDet,@IDServicio_Det,14)
			if @CostoRealLocal > @CostoRealResev
				if @CostoRealLocal - @CostoRealLocal > 0.01
					Set @bCambioCosto= 1
			if @CostoRealResev > @CostoRealLocal
				if @CostoRealResev - @CostoRealLocal > 0.01
					Set @bCambioCosto= 1
		End
	  End
	  Else
	  Begin
		if @TCambio=0    
			Begin    
				if @Capacidad = 1      
					If (@CostoRealResev <> Cast((@SSCR+@CostoReal) as Numeric(8,2)))-- and @FlSSCREditado=1)      
					Set @bCambioCosto=1 
				if @Capacidad = 2  
					If (@CostoRealResev <> Cast((@CostoReal*2) as Numeric(8,2))) --and @CostoRealEditCot=1)      
					Set @bCambioCosto=1    
					End
		Else  
		Begin    
			if @Capacidad = 1      
			If (@CostoRealResev <> Cast((@SSCR+@CostoReal)*@TCambio as Numeric(8,2)) and @FlSSCREditado=1)      
			Set @bCambioCosto=1      
			if @Capacidad = 2  
			Begin
				--If (@CostoRealResev <> Cast((@CostoReal)*@TCambio as Numeric(8,2))  and @CostoRealEditCot=1)
				--	Set @bCambioCosto=1      

				if @CostoRealEditCot=0 and @bCambioCosto=0
				Begin
					Declare @CostoServicio numeric(8,2) = Cast((select IsNull(isNull(Monto_dbls,Monto_dbl),0) from MASERVICIOS_DET 
																where IDServicio_Det=@IDServicio_Det) as Numeric(8,2))
					if @CostoRealResev <> @CostoServicio
						Set @bCambioCosto=1      
				End
				else
				If (@CostoRealResev <> Cast((@CostoReal)*@TCambio as Numeric(8,2))  and @CostoRealEditCot=1)
					Set @bCambioCosto=1      
			End    
			End
	  End
	End  
     
  --  if @bCambioCosto=1    
  --  Begin
		--select @IDDet,@IDTipoProv,@IDServicio_Det,@Capacidad
  --  End
   Fetch Next From curItemsResDet into @FeUpdateRowTemp,@CostoRealResev,@Cantidad,@Capacidad,@CostoRealEditCot,@FlSSCREditado,@FlSTCREditado,@TCambio    
  End      
        
  Close curItemsResDet      
  Deallocate curItemsResDet      
  --Detalles por ingreso (Fin)      
  End   
           
   Fetch Next From curItemsExist into @IDDet,@Dia,@Servicio,@IDServicio_Det,@IDIdioma,@CostoReal,@SSCR,@STCR,@FecMod,@IDReservaTmp,@IDTipoProv            
  End            
  Close curItemsExist            
  DealLocate curItemsExist            
           
  --select * from @tmpGeneracionReservasHTL
      
  --select @bCambioCosto,@bCambiosVentas
  --select @bCambiosVentas            
  --select * from @tmpGeneracionReservasHTL            
  if @bCambioCosto = 1        
    set @bCambiosVentas = 1        
           
  if @bCambiosVentas = 0 And @TipoProvIgv='001'
	Set @bCambiosVentas = dbo.FnDevuelveCambiosIGVPorPaxEnDetalle(@IDReserva,@IDCab)
  
  --select * from @tmpAcomodosEspeciales
  --select * from @tmpGeneracionReservasHTL   
  if ((select count(*) from @tmpAcomodosEspeciales)>0)  And @bCambiosVentas=0
  Begin         
   --Set @bCambioCosto = 0 
  --select * from @tmpAcomodosEspeciales

  --Select X.IDDET,X.Dia,X.Servicio,X.IDServicio_Det,X.IDidioma,X.CostoReal,X.SSCR,X.STCR,X.FecMod,X.IDReserva,X.IDTipoProv            
  -- from (            
  --  Select cd.IDDet,cd.Dia,cd.Servicio,cd.NroPax,cd.IDServicio_Det,cd.IDIdioma,cd.FecMod,            
  --      r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
  --      then 1 else 0 End As ServicioHotespedaje,p.IDTipoProv,Case When cd.IncGuia = 1 Then IsNull(sd.Monto_sgl,sd.Monto_sgls) Else cd.CostoReal End as CostoReal
		--,cd.SSCR,cd.STCR      
  --  from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
  --  Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det                
  --  --Left Join RESERVAS_DET rd On rd.IDDet= cd.IDDet            
  --  Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'            
  --  --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva         
  --  Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and r.Anulado = 0 and cd.AcomodoEspecial=1 and r.IDReservaProtecc is null--and rd.Anulado =0            
  --  ) as X            
  -- Where X.ServicioHotespedaje=1            
  -- Order by X.Dia,X.IDServicio_Det  

   Declare curItemsExistEsp cursor for   
  Select X.IDDET,X.Dia,X.Servicio,X.IDServicio_Det,X.IDidioma,X.CostoReal,X.SSCR,X.STCR,X.FecMod,X.IDReserva,X.IDTipoProv            
   from (            
    Select cd.IDDet,cd.Dia,cd.Servicio,cd.NroPax,cd.IDServicio_Det,cd.IDIdioma,cd.FecMod,            
        r.IDReserva,Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
        then 1 else 0 End As ServicioHotespedaje,p.IDTipoProv,Case When cd.IncGuia = 1 Then IsNull(sd.Monto_sgl,sd.Monto_sgls) Else cd.CostoReal End as CostoReal
		,cd.SSCR,cd.STCR      
    from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor                
    Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det                
    --Left Join RESERVAS_DET rd On rd.IDDet= cd.IDDet            
    Left Join RESERVAS r On cd.IDProveedor =r.IDProveedor and r.IDCab=cd.IDCAB and r.Estado <> 'XL'            
    --Left Join RESERVAS_DET rd On r.IDReserva= rd.IDReserva            
    Where cd.IDProveedor=@IDProveedor and cd.IDCAB=@IDCab and r.Anulado = 0 and cd.AcomodoEspecial=1 and r.IDReservaProtecc is null--and rd.Anulado =0            
    ) as X            
   Where X.ServicioHotespedaje=1            
   Order by X.Dia,X.IDServicio_Det  
               
   Open curItemsExistEsp            
   Fetch Next From curItemsExistEsp into @IDDet,@Dia,@Servicio,@IDServicio_Det,@IDIdioma,@CostoReal,@SSCR,@STCR,@FecMod,@IDReservaTmp,@IDTipoProv            
   While @@Fetch_Status=0            
   Begin            
    Declare @DayDiff1 int=0       
    --select @bCambiosVentas            
    --select @Dia,CAST(convert(char(10),Dia,103) as smalldatetime),FechaOut from RESERVAS_DET            
    -- where IDReserva = @IDReservaTmp And Anulado=0 and IDServicio_Det=@IDServicio_Det            
    Declare @IncGuia1 bit = (select IncGuia from COTIDET where IDDet=@IDDet)            
    If @IncGuia=1 and Not Exists(select * from @tmpAcomodoCab where Cantidad=1 and Acomodo='SINGLE')            
    Begin            
     Insert into @tmpAcomodoCab values(1,'SINGLE',0)            
    End            
        --select * from @tmpAcomodosEspeciales
                
    if @bCambiosVentas=0            
    Begin            
     If not Exists(select IDReserva_Det  from RESERVAS_DET            
          where IDReserva = @IDReservaTmp And Anulado=0 and IDServicio_Det=@IDServicio_Det             
          and @Dia between CAST(convert(char(10),Dia,103) as smalldatetime)             
          and Case When @IDTipoProv='001' then dateadd(d,-1,FechaOut) else FechaOut End) And @IDTipoProv <> '003'           
        set @bCambiosVentas=1             
    End            
                
    --select @bCambiosVentas            
    --Generación de los Segmentos de Entradas y Salidas(Inicio)            
    --print convert(char(10),@DiaTemp,103) +'|' + cast(@IDServicio_DetTmp as varchar)            
    if @IDTipoProv='001'            
    Begin            
     if convert(char(10),@DiaTemp,103)='01/01/1900' and @IDServicio_DetTmp=0            
     Begin             
      Set @DiaTemp = @Dia            
      Set @DiaTemp2 = @Dia --Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
      Set @IDServicio_DetTmp = @IDServicio_Det            
      Insert into @tmpGeneracionReservasEspecialesHTL 
      values(@IDDet,@IDServicio_DetTmp,@Dia,dateadd(d,1,@Dia))            
     End            
     Else            
     Begin            
      set @DayDiff = (select DATEDIFF(d,@DiaTemp,@Dia))            
	  --Select @DayDiff
      --print ':'+ cast(@DayDiff as varchar)            
	  --select @DiaTemp2,* from @tmpGeneracionReservasEspecialesHTL where IDServicio_Det=@IDServicio_Det
      if @DayDiff = 1             
      Begin            
       if Not Exists(select IDDet from @tmpGeneracionReservasEspecialesHTL where IDServicio_Det=@IDServicio_Det)     
       Begin            
        Insert into @tmpGeneracionReservasEspecialesHTL             
        values(@IDDet,@IDServicio_DetTmp,@Dia,dateadd(d,1,@Dia))            
       End            
       else   
        Begin                  
			 Begin            
				   Update @tmpGeneracionReservasEspecialesHTL set FechaOut = Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End             
				   where FechaIn=@DiaTemp2 and IDServicio_Det=@IDServicio_DetTmp            
			 End                 
			 Set @DiaTemp = @Dia            
			 Set @IDServicio_DetTmp = @IDServicio_Det   
        End           
      End    
                
      else     
               
      Begin            
       Insert into @tmpGeneracionReservasEspecialesHTL             
       values(@IDDet,@IDServicio_Det,@Dia,Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End)            
       Set @DiaTemp = Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
       set @DiaTemp2 = Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
       Set @IDServicio_DetTmp = @IDServicio_Det            
         
      End            
     End            
    End 
               
    else            
    Begin            
     if convert(char(10),@DiaTemp,103)='01/01/1900' and @IDServicio_DetTmp=0            
     Begin             
      Set @DiaTemp = @Dia            
      Set @DiaTemp2 = @Dia            
      Set @IDServicio_DetTmp = @IDServicio_Det            
      Insert into @tmpGeneracionReservasEspecialesHTL             
      values(@IDDet,@IDServicio_DetTmp,@Dia,Case When @RowCountCotiDet=1 then dateadd(d,1,@Dia)else @Dia End)      
     End            
     Else            
     Begin            
      set @DayDiff = (select DATEDIFF(d,@DiaTemp,@Dia))        
      --print ':'+ cast(@DayDiff as varchar)            
      --select * from @tmpGeneracionReservasHTL 
      if @DayDiff = 1             
      Begin            
       if Not Exists(select IDDet from @tmpGeneracionReservasEspecialesHTL where IDServicio_Det=@IDServicio_Det)            
       Begin            
        Insert into @tmpGeneracionReservasEspecialesHTL             
        values(@IDDet,@IDServicio_DetTmp,@Dia,Case When @RowCountCotiDet=1 then dateadd(d,1,@Dia)else @Dia End)            
  End            
       else            
       Begin            
        Update @tmpGeneracionReservasEspecialesHTL set FechaOut = Case When @RowCountCotiDet=1 then dateadd(d,1,@Dia)else @Dia End             
        where FechaIn=@DiaTemp2 and IDServicio_Det=@IDServicio_DetTmp            
       End                 
       Set @DiaTemp = @Dia--Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End             
       Set @IDServicio_DetTmp = @IDServicio_Det            
      End            
      else            
      Begin            
       Insert into @tmpGeneracionReservasEspecialesHTL             
       values(@IDDet,@IDServicio_Det,@Dia,Case When @RowCountCotiDet=1 then dateadd(d,1,@Dia)else @Dia End)            
       Set @DiaTemp = @Dia--Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
       set @DiaTemp2 = @Dia--Case When @IDTipoProv='001' then dateadd(d,1,@Dia)else @Dia End            
       Set @IDServicio_DetTmp = @IDServicio_Det 
      End       
     End            
    End           
    --Generación de los Segmentos de Entradas y Salidas(Fin)      
    --Select FeUpdateRow,netohab,Cantidad,CapacidadHab from RESERVAS_DET where IDDet=@IDDet and Anulado=0      

--select * from @tmpGeneracionReservasHTL
            
          
    If Exists(select IDReserva_Det from RESERVAS_DET where IDDet=@IDDet and Anulado=0)        
   Begin        
   Declare @FeUpdateRowTemp2 datetime='01/01/1900',@CostoRealResev2 numeric(8,2),@Cantidad2 smallint=1,@Capacidad2 smallint=''   
    
	 If Exists(Select IDReserva_Det from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDet    
			 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det    
			 Left Join RESERVAS r On rd.IDReserva=r.IDReserva
			 where r.IDReservaProtecc is null and rd.IDDet=@IDDet 
			 and rd.anulado=0 and rd.Transfer=0 and r.Anulado=0 and (cd.CostoReal > 0) And (NetoHab=0 and IsNull(sd.Monto_tri,sd.Monto_tris)>0 And CapacidadHab=3))
	 Begin
		Set @bCambioCosto=1
	 End

	 --Detalles por ingreso (Inicio)      
	 Declare curItemsResDet cursor for      
	 Select Isnull(FeUpdateRow,@FecMod) as FeUpdateRow,netohab,Cantidad,CapacidadHab,CostoRealEditado,FlSSCREditado,FlSTCREditado,IsNull(sd.TC,0) as TCambio    
	 from RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDet    
	 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det    
	 Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	 where r.IDReservaProtecc is null and rd.IDDet=@IDDet 
	 and rd.anulado=0 and rd.Transfer=0 and r.Anulado=0 and (cd.CostoReal > 0)     
	 Open curItemsResDet      
	 Fetch Next From curItemsResDet into @FeUpdateRowTemp2,@CostoRealResev2,@Cantidad2,@Capacidad2,@CostoRealEditCot,@FlSSCREditado,@FlSTCREditado,@TCambio     
	 While @@FETCH_STATUS=0      
	 Begin      
	  --select @Capacidad2,@CostoRealResev2 As NetoHab,@CostoReal as CostoReal,@SSCR As CostoSimple,@STCR as CostoTriple,@FeUpdateRowTemp,@FecMod      
	  Declare @FecModUpdate2 integer = (Select datediff(s,@FecMod,@FeUpdateRowTemp2))      
	  --select @FecModUpdate,@FecMod,@FeUpdateRowTemp      
	  If @FecModUpdate2<>0 and @bCambioCosto=0      
	  Begin      
	  if @TCambio = 0    
	  Begin    
	   if @Capacidad2 = 1      
		If (@CostoRealResev2 <> Cast((@SSCR+@CostoReal) as Numeric(8,2)))--and @FlSSCREditado=1)      
		 Set @bCambioCosto=1      
	   if @Capacidad2 = 2      
		If (@CostoRealResev2 <> Cast((@CostoReal*2) as Numeric(8,2)))--and @CostoRealEditCot=1)      
		 Set @bCambioCosto=1      
	  End    
	  Else    
	  Begin    
	  if @Capacidad2 = 1      
		If (@CostoRealResev2 <> Cast((@SSCR+@CostoReal)*@TCambio as Numeric(8,2)) and @FlSSCREditado=1)      
		 Set @bCambioCosto=1      
	  if @Capacidad2 = 2      
		If (@CostoRealResev2 <> Cast(@CostoReal*@TCambio as Numeric(8,2)) and @CostoRealEditCot=1)      
		 Set @bCambioCosto=1      
	  End    
	  End
	  
	 -- if @bCambioCosto =1
	 -- Begin
		--Select @CostoRealResev,Cast((@CostoReal*2) as Numeric(8,2)) Num2
	 -- End      
	  Fetch Next From curItemsResDet into @FeUpdateRowTemp2,@CostoRealResev2,@Cantidad2,@Capacidad2,@CostoRealEditCot,@FlSSCREditado,@FlSTCREditado,@TCambio    
	 End      
       
 Close curItemsResDet      
 Deallocate curItemsResDet      
 --Detalles por ingreso (Fin)      
  End
                
    Fetch Next From curItemsExistEsp into @IDDet,@Dia,@Servicio,@IDServicio_Det,@IDIdioma,@CostoReal,@SSCR,@STCR,@FecMod,@IDReservaTmp,@IDTipoProv            
   End            
   Close curItemsExistEsp            
   DealLocate curItemsExistEsp   
         
  --Select *,'Test1' from @tmpGeneracionReservasHTL
  --Select *,'Test' from @tmpGeneracionReservasEspecialesHTL   
  --select * from @tmpAcomodosEspeciales
  
  delete from @tmpAcomodosEspeciales where Cantidad is null
  --select @bCambioCosto,@bCambiosVentas  as CambioVentas
    if @bCambioCosto = 1        
	 set @bCambiosVentas = 1        
  End   

 -- select X.IDReserva_Det,X.IDDet,X.dia,X.FechaOut,X.Servicio,X.Cantidad from 
	--(select rd.IDReserva_Det,rd.IDDet,rd.Dia,rd.Servicio,rd.Cantidad,rd.FechaOut,            
	--	Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
	--	then 1 else 0 End As ServicioHotespedaje,            
	--	Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0) as AcomodoEspecial            
	--	from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det            
	--	 Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
	--	 --Left Join COTIDET cd On rd.IDDet=cd.IDDET            
	--	 Where r.Anulado=0 And rd.IDReserva=@IDReserva and rd.Anulado=0 and rd.FlServicioIngManual=0 and r.IDReservaProtecc is null and            
	--	 Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0)= 0) as X            
 -- Where X.ServicioHotespedaje=1 --and X.IDDet=@IDDet            
 -- Order by X.Dia 

 -- select * from @tmpGeneracionReservasHTL

  Declare @IDReservaDet___Temp2 int =0,@IDServicioDet___Temp2 int =0,@bEvaluoCantidadPag bit = 0,@IDDet__Temp2 int = 0,@_CapacidadHab smallint=0,@_EsMatrimonial bit=0

 -- select X.IDReserva_Det,X.IDDet,X.dia,X.FechaOut,X.Servicio,X.Cantidad from 
	--(select rd.IDReserva_Det,rd.IDDet,rd.Dia,rd.Servicio,rd.CantidadAPagar as Cantidad,rd.FechaOut,            
	--	Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)          
	--	then 1 else 0 End As ServicioHotespedaje,            
	--	Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0) as AcomodoEspecial            
	--	from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det            
	--	 Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
	--	 --Left Join COTIDET cd On rd.IDDet=cd.IDDET            
	--	 Where r.Anulado=0 And rd.IDReserva=@IDReserva and rd.Anulado=0 and rd.FlServicioIngManual=0 and r.IDReservaProtecc is null and            
	--	 Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0)= 0) as X            
 -- Where X.ServicioHotespedaje=1 --and X.IDDet=@IDDet            
 -- Order by X.Dia          

  --Evaluación de Fechas y acomodos (Normales) - Inicio            
  Declare curReservaDet cursor for            
  select X.IDReserva_Det,X.IDServicio_Det,X.IDDet,X.dia,X.CapacidadHab,X.EsMatrimonial,X.FechaOut,X.Servicio,X.Cantidad,X.CantidadAPagar from 
	(select rd.IDReserva_Det,rd.IDServicio_Det,rd.IDDet,rd.Dia,rd.CapacidadHab,rd.EsMatrimonial,rd.Servicio,rd.Cantidad as Cantidad,rd.CantidadAPagar,rd.FechaOut,            
		Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
		then 1 else 0 End As ServicioHotespedaje,            
		Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0) as AcomodoEspecial            
		from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det            
		 Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
		 --Left Join COTIDET cd On rd.IDDet=cd.IDDET            
		 Where r.Anulado=0 And rd.IDReserva=@IDReserva and rd.Anulado=0 and rd.transfer=0 and rd.FlServicioIngManual=0 and r.IDReservaProtecc is null and            
		 Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0)= 0) as X            
  Where X.ServicioHotespedaje=1 --and X.IDDet=@IDDet            
  Order by X.Dia            
  Open curReservaDet            
  Fetch Next From curReservaDet into @IDReservaDet___Temp2,@IDServicioDet___Temp2,@IDDetTemp,@DiaReservaDet,@_CapacidadHab,@_EsMatrimonial,
  @FechaOutReservaDet,@ServicioReservaDet,@CantiReservaDet,@CantiPagarReservaDet
  While @@FETCH_STATUS=0            
  Begin   
   Set @ExisteAcoNormales = 1        

   
   --Declare @Change varchar(15)=''      
   --select @CantiReservaDet,@ServicioReservaDet   
   --select @CantiReservaDet,@IDReservaTemp2         
   if @bCambiosVentas=0            
   Begin      
    if Not Exists(select IDDet from COTIDET where IDDet=@IDDetTemp)
     Set @bCambiosVentas=1            
    if convert(char(10),@DiaReservaDet,103) <> Convert(char(10),(select FechaIn from @tmpGeneracionReservasHTL where IDDet=@IDDetTemp),103)
	 Set @bCambiosVentas=1            
    if Convert(char(10),@FechaOutReservaDet,103) <> Convert(char(10),(select FechaOut from @tmpGeneracionReservasHTL where IDDet=@IDDetTemp And IDServicio_Det=@IDServicio_DetTmp),103)
	  Set @bCambiosVentas=1
	if @IDTipoProv='003'
	 Begin
		If Not Exists(Select Cantidad from @tmpAcomodoCab where Cantidad=@CantiReservaDet 
					  and CHARINDEX(Acomodo,(select Servicio from RESERVAS_DET Where IDReserva_Det=@IDReservaDet___Temp2))>0)            
		Begin 
			Set @bCambiosVentas=1        
		End            
		else            
		Begin 
		 Update @tmpAcomodoCab Set ItemLoc=1 where Cantidad=@CantiReservaDet 
		 and CHARINDEX(Acomodo,(select Servicio from RESERVAS_DET Where IDReserva_Det=@IDReservaDet___Temp2))>0
		End
	 End
	 Else
	 Begin
		If @CantiReservaDet>0
			Begin
			If Not Exists(Select Cantidad from @tmpAcomodoCab where Cantidad=@CantiReservaDet and CHARINDEX(Acomodo,@ServicioReservaDet)>0)            
			Begin            
				Set @bCambiosVentas=1        
			End            
			else            
			Begin 
			 Update @tmpAcomodoCab Set ItemLoc=1 where Cantidad=@CantiReservaDet and CHARINDEX(Acomodo,@ServicioReservaDet)>0            
			End 
			End
	 End

	 --if @bCambiosVentas=1
		--select FechaOut from @tmpGeneracionReservasHTL where IDDet=@IDDetTemp And IDServicio_Det=@IDServicio_DetTmp
   End

   			Insert into @TmpGeneracionCantidadPagarHotel
			values(@IDReservaDet___Temp2,@DiaReservaDet,@ServicioReservaDet,@_CapacidadHab,@_EsMatrimonial,
			@IDServicioDet___Temp2,@IDDetTemp,@CantiPagarReservaDet,@CantiReservaDet,
			(select count(*) from RESERVAS_DET_PAX where IDReserva_Det=@IDReservaDet___Temp2))
   
     Fetch Next From curReservaDet into @IDReservaDet___Temp2,@IDServicioDet___Temp2,@IDDetTemp,@DiaReservaDet,@_CapacidadHab,@_EsMatrimonial,
	 @FechaOutReservaDet,@ServicioReservaDet,@CantiReservaDet,@CantiPagarReservaDet
  End            
  Close curReservaDet            
  DealLocate curReservaDet      
  
  --Update RESERVAS_DET SET Cantidad=0 WHERE IDReserva_Det=246757
  --select @bCambiosVentas as TEST
  --select * from @TmpGeneracionCantidadPagarHotel
  --Evaluacion Cant. A Pagar Solo Hoteles
  if @IDTipoProv='001' And @bCambiosVentas=0
  Begin
  --select * from @TmpGeneracionCantidadPagarHotel
  --Order By Dia,CapacidadHab,EsMatrimonial desc,CantidadHab desc
  Declare @TipoLibEval char(1)='',@PoliticaLibEval bit=0,@MaxLiberado smallint = 0

	Declare @IDReservaDet_CantPorPagar int=0,@DiaServicio_CantPorPagar smalldatetime='01/01/1900',@Servicio_CantPorPagar varchar(100)='',@CapacidadHab_CantPorPagar smallint=0,@EsMatrimonial_CantPorPagar bit=0,
	@IDServicio_Det_CantPorPagar int=0,@IDDet_CantPorPagar int=0,@CantidadAPagar_CantPorPagar smallint=0,@cantidad_CantPorPagar smallint=0,@cantidadPax_CantPorPagar int=0,@iddettmp_3 int=0,@Liberado smallint=0

	--select @bCambiosVentas,* from @TmpGeneracionCantidadPagarHotel

	Declare curEvalCantPag cursor for
	select * from @TmpGeneracionCantidadPagarHotel
	Order By Dia,CapacidadHab,EsMatrimonial desc,CantidadHab
	Open curEvalCantPag
	Fetch Next From curEvalCantPag into @IDReservaDet_CantPorPagar,@DiaServicio_CantPorPagar,@Servicio_CantPorPagar,@CapacidadHab_CantPorPagar,@EsMatrimonial_CantPorPagar,
										@IDServicio_Det_CantPorPagar,@IDDet_CantPorPagar,@CantidadAPagar_CantPorPagar,@cantidad_CantPorPagar,@cantidadPax_CantPorPagar
	While @@FETCH_STATUS=0
	Begin
		select @TipoLibEval=TipoLib,@PoliticaLibEval=PoliticaLiberado,@Liberado=IsNull(Liberado,0),@MaxLiberado=IsNull(MaximoLiberado,0)
		from MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det_CantPorPagar

		if @PoliticaLibEval =1
		Begin
			if @TipoLibEval = 'H'
			Begin
				Declare @sumCantidadHab int = (select sum(CantidadHab) from @TmpGeneracionCantidadPagarHotel 
											   where convert(char(10),dia,103)=convert(char(10),@DiaServicio_CantPorPagar,103) and IDServicio_Det=@IDServicio_Det_CantPorPagar And IDDet=@IDDet_CantPorPagar)

				if @iddettmp_3 <> @IDDet_CantPorPagar
				Begin
					--select @IDDet_CantPorPagar,* from @TmpGeneracionCantidadPagarHotel where IDReservaDet=@IDReservaDet_CantPorPagar 
					--Update RESERVAS_DET set Cantidad=0 where IDReserva_Det=246757
					if @sumCantidadHab > @Liberado
					Begin
					  Update @TmpGeneracionCantidadPagarHotel set CantidadHab= Case When @MaxLiberado > 0 then CantidadHab-@MaxLiberado else CantidadHab End
					  where IDReservaDet=@IDReservaDet_CantPorPagar --And CantidadHab > 0
					End
				End
			End
			if @TipoLibEval = 'P'
			Begin
				Declare @sumCantidadPax int = (select sum(CantidadPax) from @TmpGeneracionCantidadPagarHotel 
											   where convert(char(10),dia,103)=convert(char(10),@DiaServicio_CantPorPagar,103) 
											   and IDServicio_Det=@IDServicio_Det_CantPorPagar And IDDet=@IDDet_CantPorPagar)
				if @iddettmp_3 <> @IDDet_CantPorPagar
				Begin
					if @sumCantidadPax >= (@Liberado+1)
					Begin
				      Update @TmpGeneracionCantidadPagarHotel set CantidadHab= Case When @MaxLiberado > 0 then CantidadHab-@MaxLiberado else CantidadHab End
					  where IDReservaDet=@IDReservaDet_CantPorPagar
					End
				End
			End
		End

		set @iddettmp_3 = @IDDet_CantPorPagar
	Fetch Next From curEvalCantPag into @IDReservaDet_CantPorPagar,@DiaServicio_CantPorPagar,@Servicio_CantPorPagar,@CapacidadHab_CantPorPagar,@EsMatrimonial_CantPorPagar,
										@IDServicio_Det_CantPorPagar,@IDDet_CantPorPagar,@CantidadAPagar_CantPorPagar,@cantidad_CantPorPagar,@cantidadPax_CantPorPagar
	End
	Close curEvalCantPag
	DealLocate curEvalCantPag

	--select * from @TmpGeneracionCantidadPagarHotel
	--select @bCambiosVentas

	--select * from @TmpGeneracionCantidadPagarHotel where CantidadPagarResv<>CantidadHab
	
	--select * from @TmpGeneracionCantidadPagarHotel
	--Order By Dia,CapacidadHab,EsMatrimonial desc
	--select * from @TmpGeneracionCantidadPagarHotel where CantidadPagarResv<>CantidadHab
	if exists(select * from @TmpGeneracionCantidadPagarHotel where (CantidadPagarResv<>CantidadHab and CantidadHab>0)) And @TipoLibEval='H'
	   And Exists(select IDReserva_Det from RESERVAS_DET where IDReserva=@IDReserva aND Anulado=0 And FeUpdateRow is not null)
		Set @bCambiosVentas=1
  End
        
  --select @bCambiosVentas as TEST2
  if @SoloGuia = 0 And @bCambiosVentas =0
  Begin
	if Not Exists(select rd.IDReserva_Det from RESERVAS_DET rd Left Join COTIDET cd On  rd.IDDet=cd.IDDET
				  where IDReserva=@IDReserva And Anulado=0 And FlServicioIngManual=0 And IsNull(cd.IncGuia,0)=0)
	   Set @SoloGuia=1
  End

  --select @bCambiosVentas    
  If @SoloGuia=1    
	Update @tmpAcomodoCab set ItemLoc=1 where ItemLoc=0            
      
  
  if dbo.FnExisteSoloAcomodoEspecial(@IDReserva)=1
  Begin
	delete from @tmpAcomodoCab where ItemLoc=0
  End
  --select @bCambiosVentas

  --Evaluacion solo Operadores con alojamiento
  If @TipoProv='003'
  Begin
   
 --  select rd2.Servicio from RESERVAS_DETSERVICIOS rd 
	--							  Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	--							  Left Join RESERVAS_DET rd2 On rd.IDReserva_Det=rd2.IDReserva_Det
	--							  Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det=sd2.IDServicio_Det
	--							  where rd2.IDReserva=@IDReserva And rd2.Anulado=0 and rd2.FlServicioIngManual = 0 
	--							  And (sd.ConAlojamiento=0 and sd2.ConAlojamiento=0) AND sd.Estado='A'
   
 --  select rd.Servicio from RESERVAS_DET rd 
	--							  Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	--							  where rd.IDReserva=@IDReserva And rd.Anulado=0 and rd.FlServicioIngManual = 0 
	--							  And (sd.ConAlojamiento=0 and sd.ConAlojamiento=0) AND sd.Estado='A' 
	--							  And Not Exists(select IDServicio_Det from RESERVAS_DETSERVICIOS where RESERVAS_DETSERVICIOS.IDReserva_Det=rd.IDReserva_Det)

	--select cd.Servicio from COTIDET_DETSERVICIOS cds Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=cds.IDServicio_Det 
	--							Inner Join COTIDET cd On cds.IDDET=cd.IDDET
	--							where IDProveedor=@IDProveedor and IDCAB=@IDCab And sd.ConAlojamiento=0 And sd.Estado='A' --and cds.CostoReal > 0

	--select * from @tmpGeneracionReservasHTL
	Declare @IDServicio_DetTMPOper int = 0
	Declare @CantServicios int = (select count(*) from RESERVAS_DETSERVICIOS rd 
								  Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
								  Left Join RESERVAS_DET rd2 On rd.IDReserva_Det=rd2.IDReserva_Det
								  Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det=sd2.IDServicio_Det
								  where rd2.IDReserva=@IDReserva And rd2.Anulado=0 and rd2.FlServicioIngManual = 0 
								  And (sd.ConAlojamiento=0 and sd2.ConAlojamiento=0) AND sd.Estado='A')
	Set @CantServicios = @CantServicios + (select count(*) from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
								  where rd.IDReserva=@IDReserva And rd.Anulado=0 and rd.FlServicioIngManual = 0 
								  And sd.ConAlojamiento=0 AND sd.Estado='A' And Not Exists(select * from RESERVAS_DETSERVICIOS where 
								  RESERVAS_DETSERVICIOS.IDReserva_Det=rd.IDReserva_Det))
	Declare @CantServicios2 int = 0
	--select * from @tmpGeneracionReservasHTL

	--Declare curServGenerados cursor for
	--select distinct IDServicio_Det from @tmpGeneracionReservasHTL
	--Open curServGenerados
	--Begin
	--	Fetch Next From curServGenerados into @IDServicio_DetTMPOper
	--	While @@FETCH_STATUS = 0
	--	Begin
	--		set @CantServicios2 = @CantServicios2 + (Select Count(*) from MASERVICIOS_DET sd 
	--		Where sd.IDServicio = (select IDServicio from MASERVICIOS_DET sd2 where sd2.IDServicio_Det=@IDServicio_DetTMPOper)
	--		And sd.Tipo = (select Tipo from MASERVICIOS_DET sd2 where sd2.IDServicio_Det=@IDServicio_DetTMPOper)
	--		And sd.Anio = (select Anio from MASERVICIOS_DET sd2 where sd2.IDServicio_Det=@IDServicio_DetTMPOper) And sd.ConAlojamiento=0)
			

	--		Fetch Next From curServGenerados into @IDServicio_DetTMPOper
	--	End
	--End
	--Close curServGenerados
	--DealLocate curServGenerados

	Declare @CantServCot int = (select count(*) from COTIDET_DETSERVICIOS cds Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=cds.IDServicio_Det 
								Inner Join COTIDET cd On cds.IDDET=cd.IDDET
								where IDProveedor=@IDProveedor and IDCAB=@IDCab And sd.ConAlojamiento=0 And sd.Estado='A')
	set @CantServCot = @CantServCot + (select count(*) from COTIDET cds Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=cds.IDServicio_Det 
									   where IDProveedor=@IDProveedor and IDCAB=@IDCab And sd.ConAlojamiento=0 And sd.Estado='A' And
									   Not Exists(select cds.IDDET from COTIDET_DETSERVICIOS where COTIDET_DETSERVICIOS.IDDET=cds.IDDET))
	set @CantServicios2 = @CantServicios2 + @CantServCot

	if @CantServicios <> @CantServicios2
		Set @bCambiosVentas = 1
  End

  --select @bCambiosVentas
  
  If (Exists(select Cantidad from @tmpAcomodoCab where ItemLoc=0) and 
      Exists(select IDReservaDet from @TmpGeneracionCantidadPagarHotel where CantidadPagarResv <> CantidadHab)) and @ExisteAcoNormales =1            
  Begin            
   set @bCambiosVentas=1            
  End            
  --Evaluación de Fechas y acomodos (Normales) - Inicio            
  --select @bCambiosVentas
  --select @bCambiosVentas            
 --Evaluación de Fechas y acomodos (Especiales) - Inicio            
  Declare curReservaDetEspecial cursor for            
  select X.IDDet,X.dia,X.FechaOut,X.Servicio,X.Cantidad from (select rd.*,            
    Case When (p.IDTipoProv = '001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)             
    then 1 else 0 End As ServicioHotespedaje,            
    Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0) as AcomodoEspecial            
    from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det            
     Left Join RESERVAS r On rd.IDReserva=r.IDReserva Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor            
     --Left Join COTIDET cd On rd.IDDet=cd.IDDET            
     Where rd.IDReserva=@IDReserva and rd.Anulado=0 and rd.FlServicioIngManual=0 and r.IDReservaProtecc is null and            
     Isnull((select AcomodoEspecial from COTIDET cd where cd.IDDET=rd.IDDET),0)= 1) as X 
  Where X.ServicioHotespedaje=1 --and X.IDDet=@IDDet            
  Order by X.Dia        
  Open curReservaDetEspecial            
  Fetch Next From curReservaDetEspecial into @IDDetTemp,@DiaReservaDet,@FechaOutReservaDet,@ServicioReservaDet,@CantiReservaDet            
  While @@FETCH_STATUS=0            
  Begin            
   if @bCambiosVentas=0 and @CantiReservaDet>0           
   Begin            
    If Not Exists(Select Cantidad from @tmpAcomodosEspeciales   
     where Cantidad=@CantiReservaDet and CHARINDEX(Acomodo,@ServicioReservaDet)>0)            
    Begin            
     Set @bCambiosVentas=1            
    End            
    else            
    Begin            
     Update @tmpAcomodosEspeciales Set ItemLoc=1 where Cantidad=@CantiReservaDet and CHARINDEX(Acomodo,@ServicioReservaDet)>0            
    End            
   End            
   --Select * from dbo.FnAcomodosCabeceraCoti(@IDCab)            
   --Select Acomodo,@ServicioReservaDet,Cantidad from dbo.FnAcomodosCabeceraCoti(@IDCab) where Cantidad=@CantiReservaDet-- and CHARINDEX(Acomodo,@ServicioReservaDet)>0            
   Fetch Next From curReservaDetEspecial into @IDDetTemp,@DiaReservaDet,@FechaOutReservaDet,@ServicioReservaDet,@CantiReservaDet         
  End            
  Close curReservaDetEspecial     
  DealLocate curReservaDetEspecial  
  
  --select @bCambiosVentas   
     
  --select * from @tmpAcomodosEspeciales
  If Exists(select Cantidad from @tmpAcomodosEspeciales where ItemLoc=0)            
  Begin            
   set @bCambiosVentas=1            
  End     
  
  --select @bCambiosVentas       
  --Evaluación de Fechas y acomodos (Especiales) - Fin            
 End
 else
 Begin
	if ((@IDProveedor<>'' and @TipoProv<>'') and @IDCab <> 0) and @bExisteProveedor = 0
	Begin
		Set @bCambiosVentas = 1
	End
 End

 --select * from @tmpGeneracionReservasHTL
 --select * from @tmpAcomodosEspeciales
  
 --select @bCambiosVentas
 FinFuncion:    
       
 Return @bCambiosVentas
End                   
