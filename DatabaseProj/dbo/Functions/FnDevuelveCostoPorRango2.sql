﻿Create Function dbo.FnDevuelveCostoPorRango2(@IDServicio_Det int,@NroPax tinyint)  
returns numeric(12,4)  
as  
Begin  
   Declare @CostoReal numeric(12,4)=0
   select @CostoReal=Monto 
   from MASERVICIOS_DET_COSTOS Where IDServicio_Det=@IDServicio_Det and @NroPax between PaxDesde and PaxHasta
   
 --  If @CostoReal = 0
 --Set @CostoReal = IsNull((select Top(1) Monto from MASERVICIOS_DET_COSTOS Where IDServicio_Det=@IDServicio_Det Order By PaxHasta Desc),0)--select CostoReal*@NroPax from COTIDET_DETSERVICIOS where IDDET=@IDDet and IDServicio_Det=@IDServicio_Det)
   
   return @CostoReal
End  
