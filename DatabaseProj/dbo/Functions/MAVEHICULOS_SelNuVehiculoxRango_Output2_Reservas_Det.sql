﻿--create
create function [dbo].[MAVEHICULOS_SelNuVehiculoxRango_Output2_Reservas_Det]    
(@idreserva_Det as int)
returns tinyint
As    

BEGIN
 declare
--@flOtraCiudad bit,    
@CoUbigeo char(6),
@Cantidad tinyint,   
@CoTipo smallint,
@IDTipoServ char(3),
@CoProveedor char(6)='',
@Tipo varchar(7)='',
@IDUbigeoOri char(6)='',
@IDUbigeoDes char(6)='',
@IDCliente char(6)=''

select @CoUbigeo=rd.IDubigeo,@Cantidad=(rd.NroPax+isnull(rd.NroLiberados,0)),@IDTipoServ=(Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End),@CoTipo=case when rd.Transfer=1 then 2 else 3 end,
@coproveedor=s.IDProveedor,@Tipo=sd.Tipo,@IDUbigeoOri=rd.IDUbigeoOri,@IDUbigeoDes=rd.IDUbigeoDes,@IDCliente=(select idcliente from coticab where idcab=r.IDCab)
from RESERVAS_DET rd
inner join reservas r on r.IDReserva=rd.IDReserva
inner join MASERVICIOS_DET sd on rd.IDServicio_Det=sd.IDServicio_Det
inner join maservicios s on s.IDServicio=sd.IDServicio
 where IDReserva_Det=@idreserva_Det

 declare @pNuVehiculo tinyint
DECLARE @CoPais char(6)
select @CoPais=IDPais from MAUBIGEO where IDubigeo=@CoUbigeo
if (@CoPais='000323')  -- peru
	if @CoProveedor in ('000544','001554') --and (@IDTipoServ='PVT' OR @CoTipo=3)
	begin
			select @CoUbigeo = case when @CoProveedor='000544' then '000066' when  @CoProveedor='001554' then '000065' else @CoUbigeo end
			select @CoTipo = case when (@IDUbigeoOri in ('000066','000068') and @IDUbigeoDes in ('000066','000068')) then 3 else ( case when @CoTipo=3 then 2 else @CoTipo end) end
			set @pNuVehiculo = (select Top(1) NuVehiculo    
			from MAVEHICULOS_TRANSPORTE  
			where CoUbigeo = @CoUbigeo /*and CoUbigeo=@CoUbigeo*/   and CoTipo=@CoTipo
			--and ((@tipo='GEB' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB') and CoMercado is null))
			and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			--and NuVehiculo_Anterior is null
			and LTRIM(RTRIM(@IDTipoServ)) in ('PVT','OT')
			and FlActivo=1
			and FlReservas=1
			and @Cantidad between QtDe and QtHasta
			)
	end
	else
	begin
			set @pNuVehiculo = (select Top(1) NuVehiculo    
			from MAVEHICULOS_TRANSPORTE  
			where CoUbigeo is null --= @CoUbigeo and CoTipo=@CoTipo
			--and ((@tipo='GEB' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB') and CoMercado is null))
			and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			--and NuVehiculo_Anterior is null
			and FlActivo=1
			and LTRIM(RTRIM(@IDTipoServ)) in ('PVT','OT')
			and FlReservas=1
			and @Cantidad between QtDe and QtHasta
			)
	end
else
	begin

		if @CoTipo=3
				set @cotipo=1

		set @pNuVehiculo = (select Top(1) NuVehiculo    
		from MAVEHICULOS_TRANSPORTE  
		where CoUbigeo = @CoUbigeo /*and CoUbigeo=@CoUbigeo*/ 
		and FlReservas=1
		  and CoTipo=@CoTipo
		and @Cantidad between QtDe and QtHasta	and FlActivo=1)
		
		
	end

 set @pNuVehiculo = ISNULL(@pNuVehiculo,0)  
 return @pNuVehiculo
END
