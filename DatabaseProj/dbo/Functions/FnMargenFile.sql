﻿--JHD-20160407-Declare @TotalImpto numeric(12,4)=isnull((select SUM(TotImpto) from COTIDET cd where cd.IDCAB = @IDCab),0)
CREATE Function dbo.FnMargenFile(@IDCab int)
	returns numeric(5,2)  
As
	Begin
     
	Declare @TotalDobleNeto numeric(10,2)=isnull((select SUM(Total) from COTIDET cd Where cd.IDCAB = @IDCab),0)+
		IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
	    from COTIVUELOS cv 
		Where cv.IDCAB=@IDCab And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 
		and IsNull(Tarifa,0)>0),0)

    --Declare @TotalImpto numeric(7,4)=isnull((select SUM(TotImpto) from COTIDET cd where cd.IDCAB = @IDCab),0)
	Declare @TotalImpto numeric(12,4)=isnull((select SUM(TotImpto) from COTIDET cd where cd.IDCAB = @IDCab),0)
    Declare @TotalCostoReal numeric(14,4)=isnull((select SUM(CostoReal) from COTIDET cd where cd.IDCAB = @IDCab),0)
    Declare @TotalLib numeric(14,4)=isnull((select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = @IDCab),0)

	Declare @ValReturn numeric(5,2)  = 0
	If @TotalDobleNeto <> 0
		--Set @ValReturn = ((@TotalDobleNeto - @TotalImpto - @TotalCostoReal - @TotalLib) / @TotalDobleNeto) * 100
		Set @ValReturn = CAST(ROUND(((@TotalDobleNeto - @TotalImpto - @TotalCostoReal - @TotalLib) / @TotalDobleNeto) * 100,2) AS numeric(5,2))
   
	Return @ValReturn
	End

