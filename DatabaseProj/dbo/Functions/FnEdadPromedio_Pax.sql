﻿
--create
--select dbo.FnEdadPromedio_Pax(10914)
Create FUNCTION dbo.FnEdadPromedio_Pax(@IDCab as int) 
RETURNS int AS 
BEGIN 

declare @edad_prom as decimal(10,2)

declare @edades as table
(edad int null)

insert into @edades
select edad=DATEDIFF(day,fecnacimiento,getdate())/365--,*
 from COTIPAX where IDCab=@IDCab and fecnacimiento is not null

declare @cont_Edades int=(select COUNT(edad) from @edades)

set @edad_prom= case when @cont_Edades = 0 then 0 else round((select cast(SUM(edad) as decimal(10,2))/COUNT(edad) from @edades),2) end

RETURN CAST(round(@edad_prom,0)AS INT)
END 
