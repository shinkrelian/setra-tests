﻿--JRF-20131015-Agrupación de los codigos de Reserva(Cotivuelos) que comparten los pasajeros.  
--JRF-20131023-Correción de Eliminación de la última letra.
CREATE function dbo.FnPNRxIDCabxDia(@IDCab int,@Dia smalldatetime)    
returns varchar(max)    
as    
Begin    
 Declare @PNRReturn varchar(max)='',@tiCont tinyint =0    
 Declare @FechaVuelo smalldatetime  
 Declare @CodReservaTransp varchar(max)  
        
 Declare curPNR cursor for    
    select * from(    
   select Distinct cast(convert(char(10),v.Fec_Salida,103) as smalldatetime) as Fec  
   ,dbo.FnApellidosPaxxCodTransporte(Isnull(CodReservaTransp,CodReserva),v.Fec_Salida,v.Ruta,cp.IDCab) GrupoPaxPorCodigo  
   from COTITRANSP_PAX ctp Left Join COTIPAX cp On ctp.IDPax =cp.IDPax        
   Left Join COTIVUELOS v On ctp.IDTransporte = v.ID  
   where v.IDCAB = @IDCab   
   --Order by CAST(convert(char(10),v.Fec_Salida,103) as smalldatetime)  
  ) as X    
  Where Convert(Varchar(10),X.Fec,103)=Convert(varchar(10),@Dia,103)  
    
  Open curPNR     
  Fetch Next From curPNR into @FechaVuelo,@CodReservaTransp   
  while @@fetch_status = 0    
  Begin  
     
    if @tiCont = 0     
  if charindex(',',@CodReservaTransp)>0  
   set @PNRReturn = @CodReservaTransp +char(13)  
  else  
   set @PNRReturn = @CodReservaTransp   
    else     
  if charindex(',',@CodReservaTransp)>0  
   set @PNRReturn = @PNRReturn + char(13)+ @CodReservaTransp + char(13)  
  else  
   set @PNRReturn = @PNRReturn + '*'+ @CodReservaTransp +'*'  
    
  Set @tiCont = @tiCont + 1     
  Fetch Next From curPNR into @FechaVuelo,@CodReservaTransp  
  end    
 Close curPNR    
 DealLocate curPNR    
     
      
 if @PNRReturn <> ''   
  set @PNRReturn = ltrim(rtrim(@PNRReturn)) 
  --set @PNRReturn = SUBSTRING(@PNRReturn,0,LEN(@PNRReturn))  
  if  substring(@PNRReturn,len(@PNRReturn),len(@PNRReturn))=char(13)
  begin
	set @PNRReturn = SUBSTRING(@PNRReturn,0,LEN(@PNRReturn))  
  End
  set @PNRReturn = REPLACE(@PNRReturn,'**','*')  
  set @PNRReturn = REPLACE(@PNRReturn,'*',', ')  
        
 return @PNRReturn    
End  

