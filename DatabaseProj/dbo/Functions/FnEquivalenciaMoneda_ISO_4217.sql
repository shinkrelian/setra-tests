﻿
CREATE FUNCTION [dbo].[FnEquivalenciaMoneda_ISO_4217](
												@IDMoneda char(3)
												)
RETURNS char(3)
AS
BEGIN
	DECLARE @ReturnMoneda char(3) = ''
		IF @IDMoneda = 'USD'	--DÓLARES
			SET @ReturnMoneda = 'USD'
		IF @IDMoneda = 'SOL'	--NUEVOS SOLES
			SET @ReturnMoneda = 'PEN'
		IF @IDMoneda = 'EUR'	--EUROS
			SET @ReturnMoneda = 'EUR'
		IF @IDMoneda = 'ARS'	--PESO ARGENTINO
			SET @ReturnMoneda = 'ARS'
		IF @IDMoneda = 'CLP'	--PESO CHILENO
			SET @ReturnMoneda = 'CLP'
		IF @IDMoneda = 'BOB'	--BOLIVIANO
			SET @ReturnMoneda = 'BOB'
		IF @IDMoneda = 'BRL'	--REAL BRASILEÑO
			SET @ReturnMoneda = 'BRL'
		IF @IDMoneda = 'NAP'	--NO APLICA
			SET @ReturnMoneda = ''
	RETURN @ReturnMoneda
END
