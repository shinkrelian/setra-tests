﻿Create function dbo.FnRegistroOperacionesDetServiciosValido(@IDOperacion_Det int,@IDServicio_Det int)
returns bit
As
Begin
	Declare @bDatoValido bit = 1
	Declare @Anio char(4)='',@Variante varchar(7)='',@IDServicio char(8)=''
	Select @Anio=sd.Anio,@Variante=IsNull(sd.Tipo,''),@IDServicio=sd.IDServicio
	from OPERACIONES_DET od Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
	Where IDOperacion_Det=@IDOperacion_Det

	Declare @AnioServ char(4)='',@VarianteServ varchar(7)='',@IDServicioServ char(8)=''
	Select @AnioServ=Anio,@VarianteServ=IsNull(Tipo,''),@IDServicioServ=IDServicio
	from MASERVICIOS_DET 
	Where IDServicio_Det=@IDServicio_Det

	if @Anio <> @AnioServ Or ltrim(rtrim(@Variante)) <> ltrim(rtrim(@VarianteServ)) Or @IDServicio <> @IDServicioServ
	Begin
		Set @bDatoValido = 0
	End

	return @bDatoValido
End
