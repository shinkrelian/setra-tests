﻿
CREATE FUNCTION [dbo].[FnExistenAlojamiento](@IDReserva int)
returns bit
As
Begin
	Declare @ReturnExists bit =0
	Declare @IDTipoProv char(3)=''
	Select @IDTipoProv=p.IDTipoProv from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
	where IDReserva=@IDReserva

	If @IDTipoProv='001'
	Begin
		Set @ReturnExists = 1
		return @ReturnExists--se grego esto
	End

	If Exists(select IDReserva_Det from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det 
	where sd.ConAlojamiento=1 and rd.IDReserva=@IDReserva and rd.Anulado=0) And @ReturnExists= 0
	Begin
		Set @ReturnExists = 1
	End
	return @ReturnExists
End
