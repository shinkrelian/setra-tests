﻿--JRF-20150227-Ajuste en el Nombre del Excellent
CREATE Function dbo.FnDevuelveValorCadenaCalificacionControlCalidad(@NumEval tinyint)  
returns varchar(15)  
Begin  
 Declare @CadenaRet varchar(15)=''  
 if @NumEval = 5  
  Set @CadenaRet='EXCELLENT'  
 if @NumEval = 4  
  Set @CadenaRet='VERY GOOD'  
 if @NumEval = 3  
  Set @CadenaRet='GOOD'  
 if @NumEval = 2  
  Set @CadenaRet='AVERAGE'  
 if @NumEval = 1  
  Set @CadenaRet='POOR'  
 if @NumEval = 0  
  Set @CadenaRet=''  
   
 return @CadenaRet  
End  
