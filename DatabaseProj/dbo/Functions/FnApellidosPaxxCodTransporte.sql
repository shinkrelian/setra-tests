﻿
CREATE Function dbo.FnApellidosPaxxCodTransporte(@CodReservaTransporte varchar(20),
												@DiaTransporte smalldatetime,@Ruta varchar(30),
												@IDCab int)
Returns varchar(max)
As
Begin
	Declare @ApellidosPax varchar(max) = '',@Apellido varchar(20),@Cantidad smallint,@tiCont smallint = 0
	Declare @NroTotalPax smallint = (select NroPax+Isnull(NroLiberados,0) from coticab where IDCab = @IDCab)
	Declare @TotalPaxSinCodR smallint = (select count(IDPax) from COTITRANSP_PAX cpx Left Join COTIVUELOS v On cpx.IDTransporte = v.ID
										Where v.IDCAB = @IDCab and v.Ruta = @Ruta And Convert(Varchar(10),Fec_Salida,103)=Convert(varchar(10),@DiaTransporte,103)
										And Isnull(cpx.CodReservaTransp,'') = '')
	if @TotalPaxSinCodR <> @NroTotalPax
	Begin
	
	Declare CurTrans cursor for 
	SELECT px.Apellidos, count(px.Apellidos) CantidadApellidos 
	FROM COTITRANSP_PAX cpx Left Join cotipax px On cpx.IDPax = px.IDPax
	where CodReservaTransp= @CodReservaTransporte and 
	IDTransporte In(select ID from cotivuelos where idcab = @IDCab --and ltrim(rtrim(ruta)) = ltrim(rtrim(@Ruta))
					and convert(varchar(10),Fec_Salida,103)= convert(varchar(10),@DiaTransporte,103))
	Group By px.Apellidos
	Open CurTrans
	Fetch Next From CurTrans into @Apellido,@Cantidad
	While @@Fetch_Status = 0
	Begin
		if @tiCont = 0
			Set @ApellidosPax = @CodReservaTransporte +' ('+@Apellido+' x'+cast(@Cantidad as varchar(2))
		else
			Set @ApellidosPax = @ApellidosPax + ','+@Apellido+' x'+cast(@Cantidad as varchar(2))
		
		Set @tiCont = @tiCont + 1
		Fetch Next From CurTrans into @Apellido,@Cantidad
	End
	Close CurTrans
	DealLocate CurTrans
	
	
	if ltrim(rtrim(@ApellidosPax)) <> ''
		Set @ApellidosPax = @ApellidosPax+')'
    End
    Else
    Begin
		set @ApellidosPax = @CodReservaTransporte
    End
	
	return @ApellidosPax
End

