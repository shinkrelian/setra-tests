﻿Create Function dbo.FnLogCompararDesdeReservasVsVentasHotelesuOperAloj(@IDReserva int)
returns bit
As
Begin	
	Declare @CambiosVentasNoAplicados bit = 0,@IDDet int = 0
	Declare curReserv cursor for
	Select IDDet
	from RESERVAS_DET rd
	Where rd.IDReserva =@IDReserva and rd.Anulado=0
	Open curReserv
		Fetch Next From  curReserv into @IDDet
		While @@Fetch_Status=0
		Begin
			if @CambiosVentasNoAplicados=0
			Begin
				If Not Exists(Select IDDet from COTIDET where IDDet = @IDDet)
					Set @CambiosVentasNoAplicados=1
			End
			Fetch Next From  curReserv into @IDDet
		End
	Close curReserv
	DealLocate curReserv
	return @CambiosVentasNoAplicados
End
