﻿Create Function dbo.FnExisteServiciosIncluidosCotizados(@IDCab int,
														@IDIncluido int)
returns bit
As
Begin
	Declare @bEncontro bit = 0,@IDServicio char(8)=''
	Declare curServ cursor for
	select IDServicio from MAINCLUIDO_SERVICIOS Where IDIncluido=@IDIncluido
	Open curServ
	Fetch Next from curServ into @IDServicio
	While @@Fetch_Status=0
	Begin
		if @bEncontro = 0
			Select @bEncontro= 1 from COTIDET Where IDCab=@IDCab and IDServicio=@IDServicio
	Fetch Next from curServ into @IDServicio
	End
	Close curServ
	DealLocate curServ
	
	return Cast(@bEncontro as bit)
End
