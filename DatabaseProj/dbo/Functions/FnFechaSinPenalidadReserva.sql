﻿--JRF-20150309-Considerar el Año de la fecha de inicio de los servicios
--JRF-20150313-Considerar la tarifa del año anterior en caso que no exista
CREATE Function dbo.FnFechaSinPenalidadReserva(@IDCab int,@IDProveedor varchar(6))  
returns varchar(20)  
As  
Begin  
   
 Declare @Tipo Varchar(5),@ValReturn varchar(20),@Cantidad smallint,@IDReserva int,  
 @FechaIn smalldatetime  
   
 set @Tipo = (SELECT top(1) DefinTipo FROM MAPOLITICAPROVEEDORES WHERE IDProveedor = @IDProveedor)   
 set @IDReserva = (select top(1)IDReserva from RESERVAS Where IDCab = @IDCab And IDProveedor = @IDProveedor and anulado=0)  
 set @FechaIn = (Select top(1)Dia from RESERVAS_DET rd Left Join RESERVAS r On r.IDReserva=rd.IDReserva 
				 where r.Anulado=0 and rd.IDReserva = @IDReserva and rd.anulado=0 order by Dia asc)
   
 set @Cantidad = (select   
       Case When ltrim(rtrim(@Tipo))='HAB' then  
         ((IsNull(Simple,0)+IsNull(SimpleResidente,0) )) +  
         ((IsNull(Twin,0)+ISNULL(TwinResidente,0))) +  
         ((IsNull(Matrimonial,0)+ISNULL(MatrimonialResidente,0))) +  
         ((IsNull(Triple,0)+ISNULL(TripleResidente,0)))   
         when ltrim(rtrim(@Tipo))='PAX' then  
         Isnull(NroPax,0)+IsNull(NroLiberados,0)
       End  
          from coticab where IdCab = @IDCab)   
  
 Declare @DiasSinPenalidad smallint=0 
 set @DiasSinPenalidad = (select top(1)isnull(DiasSinPenalidad,0) from MAPOLITICAPROVEEDORES   
        where IDProveedor=@IDProveedor and Anio=Cast(Year(@FechaIn) as int) and (@Cantidad between DefinDia and DefinHasta))  
 
 if IsNuLL(@DiasSinPenalidad,0)=0
 Begin
       set @DiasSinPenalidad = (select top(1)isnull(DiasSinPenalidad,0) from MAPOLITICAPROVEEDORES   
       where IDProveedor=@IDProveedor and Anio=Cast(Year(@FechaIn)-1 as int) and (@Cantidad between DefinDia and DefinHasta))   
 End
   
 set @ValReturn = Case When @DiasSinPenalidad=0 then '' else Convert(varchar(10),dateadd(DD,@DiasSinPenalidad*-1,@FechaIn),103) End  
 return Isnull(@ValReturn,'')  
End  
