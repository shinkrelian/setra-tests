﻿Create Function dbo.FnDevuelveCapacidadxCadena(@CadenaDescrip varchar(200))
returns tinyint 
as
Begin
	Declare @NumReturn tinyint = 0
	if charindex('SINGLE',@CadenaDescrip)>0
		Set @NumReturn = 1
	if charindex('TRIPLE',@CadenaDescrip)>0
		Set @NumReturn = 3
	if charindex('CUADRUPLE',@CadenaDescrip)>0
		Set @NumReturn = 4
	if charindex('QUINTUPLE',@CadenaDescrip)>0
		Set @NumReturn = 5
	if charindex('SEXTUPLE',@CadenaDescrip)>0
		Set @NumReturn = 6
	if charindex('SEPTUPLE',@CadenaDescrip)>0
		Set @NumReturn = 7
	if charindex('OCTUPLE',@CadenaDescrip)>0
		Set @NumReturn = 8
	if charindex('DOUBLE (MAT)',@CadenaDescrip)>0 Or charindex('TWIN',@CadenaDescrip)>0
	Begin
		Set @NumReturn = 2
		if charindex('TWIN + (EXCHILD)',@CadenaDescrip) > 0
			Set @NumReturn = 3
	End
	return @NumReturn
End
