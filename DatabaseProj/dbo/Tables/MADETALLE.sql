﻿CREATE TABLE [dbo].[MADETALLE] (
    [IDDetalle]   CHAR (3)  NOT NULL,
    [Descripcion] TEXT      NOT NULL,
    [IDIdioma]    CHAR (12) NOT NULL,
    [IDUsuario]   CHAR (4)  NOT NULL,
    [Descri]      CHAR (10) NOT NULL,
    [UserMod]     CHAR (4)  NULL,
    [FecMod]      DATETIME  NULL,
    CONSTRAINT [PK_MADETALLE] PRIMARY KEY CLUSTERED ([IDDetalle] ASC)
);


GO

--36
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MADETALLE_LOG_Del]
  On [dbo].[MADETALLE] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDetalle from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MADETALLE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--34
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MADETALLE_LOG_Upd]
  On [dbo].[MADETALLE] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDetalle from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MADETALLE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--32
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- TABLA MADETALLE

CREATE Trigger [dbo].[MADETALLE_LOG_Ins]
  On [dbo].[MADETALLE] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDetalle from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MADETALLE',@UserMod,@RegistroPK1,'','','',@Accion
		End  
