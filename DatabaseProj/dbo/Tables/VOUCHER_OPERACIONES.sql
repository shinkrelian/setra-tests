﻿CREATE TABLE [dbo].[VOUCHER_OPERACIONES] (
    [IDVoucher]        INT             NOT NULL,
    [ServExportac]     BIT             NOT NULL,
    [ExtrNoIncluid]    BIT             NOT NULL,
    [UserMod]          CHAR (4)        NOT NULL,
    [FecMod]           DATETIME        DEFAULT (getdate()) NOT NULL,
    [ObservVoucher]    VARCHAR (MAX)   NULL,
    [CoEstado]         CHAR (2)        DEFAULT ('PD') NOT NULL,
    [SsSaldo]          NUMERIC (12, 2) NULL,
    [SsDifAceptada]    NUMERIC (12, 2) NULL,
    [TxObsDocumento]   VARCHAR (MAX)   NULL,
    [NuVoucherBup]     INT             NULL,
    [SsMontoTotal]     NUMERIC (12, 2) DEFAULT (NULL) NULL,
    [UserNuevo]        CHAR (4)        NULL,
    [CoUbigeo_Oficina] CHAR (6)        NULL,
    CONSTRAINT [PK_VOUCHER_OPERACIONES] PRIMARY KEY NONCLUSTERED ([IDVoucher] ASC),
    CONSTRAINT [FK_VOUCHER_OPERACIONES_ESTADO_OBLIGACIONESPAGO] FOREIGN KEY ([CoEstado]) REFERENCES [dbo].[ESTADO_OBLIGACIONESPAGO] ([CoEstado])
);


GO

--17
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[VOUCHER_OPERACIONES_LOG_Ins]
 On [Dbo].[VOUCHER_OPERACIONES] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDVoucher from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'VOUCHER_OPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--18
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[VOUCHER_OPERACIONES_LOG_Upd]
 On [Dbo].[VOUCHER_OPERACIONES] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDVoucher from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'VOUCHER_OPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--19
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[VOUCHER_OPERACIONES_LOG_Del]
 On [Dbo].[VOUCHER_OPERACIONES] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDVoucher from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'VOUCHER_OPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
