﻿CREATE TABLE [dbo].[MACUESTIONARIO] (
    [NuCuest]       TINYINT       NOT NULL,
    [IDTipoProv]    CHAR (3)      NULL,
    [NoDescripcion] VARCHAR (MAX) NULL,
    [Activo]        BIT           DEFAULT ((0)) NULL,
    [UserMod]       CHAR (4)      NOT NULL,
    [FecMod]        SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MACUESTIONARIO] PRIMARY KEY CLUSTERED ([NuCuest] ASC),
    CONSTRAINT [FK_MACUESTIONARIO_MATIPOPROVEEDOR] FOREIGN KEY ([IDTipoProv]) REFERENCES [dbo].[MATIPOPROVEEDOR] ([IDTipoProv])
);

