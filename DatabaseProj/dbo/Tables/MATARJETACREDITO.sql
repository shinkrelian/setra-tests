﻿CREATE TABLE [dbo].[MATARJETACREDITO] (
    [CodTar]      CHAR (3)     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [UserMod]     CHAR (4)     NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF_MATARJETACREDITO_FecMod] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_MATARJETACREDITO_CodTar] PRIMARY KEY CLUSTERED ([CodTar] ASC)
);


GO

--220
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MATARJETACREDITO_LOG_Ins]
  On [dbo].[MATARJETACREDITO] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select CodTar from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATARJETACREDITO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--221
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATARJETACREDITO_LOG_Upd]
  On [dbo].[MATARJETACREDITO] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select CodTar from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATARJETACREDITO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--222
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MATARJETACREDITO_LOG_Del]
  On [dbo].[MATARJETACREDITO] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select CodTar from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATARJETACREDITO',@UserMod,@RegistroPK1,'','','',@Accion
		End  
