﻿CREATE TABLE [dbo].[MAHABITTRIPLE] (
    [IdHabitTriple] CHAR (3)     NOT NULL,
    [Abreviacion]   CHAR (4)     NOT NULL,
    [Descripcion]   VARCHAR (50) NOT NULL,
    [UserMod]       CHAR (4)     NOT NULL,
    [FecMod]        DATETIME     CONSTRAINT [DF__MAHABITTR__FecMo__25C68D63] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAHABITTRIPLE] PRIMARY KEY CLUSTERED ([IdHabitTriple] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MAHABITTRIPLE_Abreviacion]
    ON [dbo].[MAHABITTRIPLE]([Abreviacion] ASC);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MAHABITTRIPLE_Descripcion]
    ON [dbo].[MAHABITTRIPLE]([Descripcion] ASC);


GO

--53
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- TABLA MAHABITTRIPLE

CREATE Trigger [dbo].[MAHABITTRIPLE_LOG_Ins]
  On [dbo].[MAHABITTRIPLE] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdHabitTriple from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAHABITTRIPLE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--60
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAHABITTRIPLE_LOG_Del]
  On [dbo].[MAHABITTRIPLE] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IdHabitTriple from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAHABITTRIPLE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--56
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAHABITTRIPLE_LOG_Upd]
  On [dbo].[MAHABITTRIPLE] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdHabitTriple from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAHABITTRIPLE',@UserMod,@RegistroPK1,'','','',@Accion
		End  
