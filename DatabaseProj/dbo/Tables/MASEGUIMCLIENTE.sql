﻿CREATE TABLE [dbo].[MASEGUIMCLIENTE] (
    [IDSeguim]   INT      NOT NULL,
    [IDCliente]  CHAR (6) NOT NULL,
    [Fecha]      DATETIME CONSTRAINT [DF__MASEGUIMC__Fecha__7720AD13] DEFAULT (getdate()) NOT NULL,
    [IDContacto] CHAR (3) NOT NULL,
    [Notas]      TEXT     NOT NULL,
    [UserMod]    CHAR (4) NOT NULL,
    [FecMod]     DATETIME CONSTRAINT [DF__MASEGUIMC__FecMo__7814D14C] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MASEGUIMCLIENTE] PRIMARY KEY CLUSTERED ([IDSeguim] ASC),
    CONSTRAINT [FK_MASEGUIMCLIENTE_MACONTACTOSCLIENTE1] FOREIGN KEY ([IDContacto], [IDCliente]) REFERENCES [dbo].[MACONTACTOSCLIENTE] ([IDContacto], [IDCliente])
);


GO

--117
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- TABLA MASEGUIMCLIENTE

CREATE Trigger [dbo].[MASEGUIMCLIENTE_LOG_Ins]
  On [dbo].[MASEGUIMCLIENTE] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDSeguim from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASEGUIMCLIENTE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO


--123
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASEGUIMCLIENTE_LOG_Del]
  On [dbo].[MASEGUIMCLIENTE] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDSeguim from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASEGUIMCLIENTE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--120
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASEGUIMCLIENTE_LOG_Upd]
  On [dbo].[MASEGUIMCLIENTE] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDSeguim from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASEGUIMCLIENTE',@UserMod,@RegistroPK1,'','','',@Accion
		End  
