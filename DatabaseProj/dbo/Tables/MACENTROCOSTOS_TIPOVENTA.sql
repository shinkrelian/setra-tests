﻿CREATE TABLE [dbo].[MACENTROCOSTOS_TIPOVENTA] (
    [CoCeCos]     CHAR (6) NOT NULL,
    [CoTipoVenta] CHAR (2) NOT NULL,
    [UserMod]     CHAR (4) NOT NULL,
    [FecMod]      DATETIME CONSTRAINT [DF_MACENTROCOSTOS_TIPOVENTA_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MACENTROCOSTOS_TIPOVENTA] PRIMARY KEY CLUSTERED ([CoCeCos] ASC, [CoTipoVenta] ASC)
);

