﻿CREATE TABLE [dbo].[MAINCLUIDOOPERACIONES] (
    [NuIncluido]   INT           NOT NULL,
    [NoObserAbrev] VARCHAR (100) NOT NULL,
    [FlPorDefecto] BIT           NOT NULL,
    [UserMod]      CHAR (4)      NOT NULL,
    [FecMod]       DATETIME      DEFAULT (getdate()) NOT NULL,
    [FlInterno]    BIT           DEFAULT ((0)) NOT NULL,
    [NuOrden]      TINYINT       DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MAINCLUIDOOPERACIONES] PRIMARY KEY CLUSTERED ([NuIncluido] ASC)
);


GO
Create Trigger [dbo].[MAINCLUIDOOPERACIONES_LOG_Upd]  
  On [dbo].[MAINCLUIDOOPERACIONES] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuIncluido from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDOOPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAINCLUIDOOPERACIONES_LOG_Ins]  
  On [dbo].[MAINCLUIDOOPERACIONES] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuIncluido from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDOOPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAINCLUIDOOPERACIONES_LOG_Del]  
  On [dbo].[MAINCLUIDOOPERACIONES] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuIncluido from Deleted)    
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDOOPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
