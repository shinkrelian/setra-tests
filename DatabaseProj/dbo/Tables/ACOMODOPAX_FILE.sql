﻿CREATE TABLE [dbo].[ACOMODOPAX_FILE] (
    [IDPax]         INT      NOT NULL,
    [CapacidadHab]  TINYINT  NOT NULL,
    [IDCAB]         INT      NOT NULL,
    [IDHabit]       TINYINT  NOT NULL,
    [EsMatrimonial] BIT      CONSTRAINT [DF__ACOMODOPA__EsMat__32816A03] DEFAULT ((0)) NULL,
    [UserMod]       CHAR (4) NOT NULL,
    [FecMod]        DATETIME CONSTRAINT [DF__ACOMODOPA__FecMo__18C19800] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ACOMODOPAX_FILE] PRIMARY KEY CLUSTERED ([IDPax] ASC),
    CONSTRAINT [FK_ACOMODOPAX_FILE_COTICAB] FOREIGN KEY ([IDCAB]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_ACOMODOPAX_FILE_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax])
);


GO
CREATE NONCLUSTERED INDEX [ACOMODOPAX_FILE_IDCab]
    ON [dbo].[ACOMODOPAX_FILE]([IDCAB] ASC);


GO

--223
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ACOMODOPAX_FILE_LOG_Ins]
 On [dbo].[ACOMODOPAX_FILE] for Insert
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDPax from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ACOMODOPAX_FILE',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO


--224
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ACOMODOPAX_FILE_LOG_Upd]
 On [dbo].[ACOMODOPAX_FILE] for Update
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDPax from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ACOMODOPAX_FILE',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO

--225
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ACOMODOPAX_FILE_LOG_Del]
 On [dbo].[ACOMODOPAX_FILE] for Delete
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from deleted) = 1
		Begin
			set @RegistroPK1 = (Select IDPax from Deleted)
			set @UserMod = (Select UserMod from Deleted)
			set @Accion = 'D'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ACOMODOPAX_FILE',@UserMod,@RegistroPK1,'','','',@Accion
		End
