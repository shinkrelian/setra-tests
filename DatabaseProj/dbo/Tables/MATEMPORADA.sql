﻿CREATE TABLE [dbo].[MATEMPORADA] (
    [IDTemporada]     INT          NOT NULL,
    [NombreTemporada] VARCHAR (80) NOT NULL,
    [UserMod]         CHAR (4)     NOT NULL,
    [FecMod]          DATETIME     NOT NULL,
    [TipoTemporada]   CHAR (1)     DEFAULT ('N') NOT NULL,
    CONSTRAINT [PK_MATEMPORADA] PRIMARY KEY CLUSTERED ([IDTemporada] ASC)
);

