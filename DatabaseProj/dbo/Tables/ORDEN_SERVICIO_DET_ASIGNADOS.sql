﻿CREATE TABLE [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS] (
    [NuOrden_Servicio_Det] INT           NOT NULL,
    [IDProveedor]          CHAR (6)      NOT NULL,
    [NuVehiculoProg]       SMALLINT      NULL,
    [UserMod]              CHAR (4)      NOT NULL,
    [FecMod]               SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [FK_ORDEN_SERVICIO_DET_ASIGNADOS] PRIMARY KEY CLUSTERED ([NuOrden_Servicio_Det] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_ORDEN_SERVICIO_DET_ASIGNADOS_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_ORDEN_SERVICIO_DET_ASIGNADOS_ORDEN_SERVICIO_DET] FOREIGN KEY ([NuOrden_Servicio_Det]) REFERENCES [dbo].[ORDEN_SERVICIO_DET] ([NuOrden_Servicio_Det])
);


GO
Create Trigger [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS_LOG_Del]  
  On [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio_Det from Deleted)   
   set @RegistroPK2 = (select IDProveedor from Deleted)
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO_DET_ASIGNADOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS_LOG_Ins]  
  On [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio_Det from Inserted)        
   set @RegistroPK2 = (select IDProveedor from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO_DET_ASIGNADOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS_LOG_Upd]  
  On [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio_Det from Inserted)        
   set @RegistroPK2 = (select IDProveedor from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO_DET_ASIGNADOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
