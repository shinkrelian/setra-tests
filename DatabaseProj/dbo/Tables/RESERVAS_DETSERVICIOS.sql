﻿CREATE TABLE [dbo].[RESERVAS_DETSERVICIOS] (
    [IDReserva_Det]           INT             NOT NULL,
    [IDServicio_Det]          INT             NOT NULL,
    [CostoReal]               NUMERIC (12, 4) NOT NULL,
    [CostoLiberado]           NUMERIC (12, 4) NOT NULL,
    [Margen]                  NUMERIC (12, 4) NOT NULL,
    [MargenAplicado]          NUMERIC (6, 2)  NOT NULL,
    [MargenLiberado]          NUMERIC (12, 4) NOT NULL,
    [Total]                   NUMERIC (12, 2) NOT NULL,
    [SSCR]                    NUMERIC (12, 4) NOT NULL,
    [SSCL]                    NUMERIC (12, 4) NOT NULL,
    [SSMargen]                NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberado]        NUMERIC (12, 4) NOT NULL,
    [SSTotal]                 NUMERIC (12, 4) NOT NULL,
    [STCR]                    NUMERIC (12, 4) NOT NULL,
    [STMargen]                NUMERIC (12, 4) NOT NULL,
    [STTotal]                 NUMERIC (12, 4) NOT NULL,
    [CostoRealImpto]          NUMERIC (12, 4) NOT NULL,
    [CostoLiberadoImpto]      NUMERIC (12, 4) NOT NULL,
    [MargenImpto]             NUMERIC (12, 4) NOT NULL,
    [MargenLiberadoImpto]     NUMERIC (6, 2)  NOT NULL,
    [SSCRImpto]               NUMERIC (12, 4) NOT NULL,
    [SSCLImpto]               NUMERIC (12, 4) NOT NULL,
    [SSMargenImpto]           NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberadoImpto]   NUMERIC (12, 4) NOT NULL,
    [STCRImpto]               NUMERIC (12, 4) NOT NULL,
    [STMargenImpto]           NUMERIC (12, 4) NOT NULL,
    [TotImpto]                NUMERIC (12, 4) NOT NULL,
    [UserMod]                 CHAR (4)        NOT NULL,
    [FecMod]                  DATETIME        DEFAULT (getdate()) NOT NULL,
    [FlServicioInCorrectoxTI] BIT             DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_RESERVAS_DETSERVICIOS] PRIMARY KEY NONCLUSTERED ([IDReserva_Det] ASC, [IDServicio_Det] ASC),
    CONSTRAINT [FK_RESERVAS_DETSERVICIOS_MASERVICIOS_DET] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det]),
    CONSTRAINT [FK_RESERVAS_DETSERVICIOS_RESERVAS] FOREIGN KEY ([IDReserva_Det]) REFERENCES [dbo].[RESERVAS_DET] ([IDReserva_Det])
);


GO

--41
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_DETSERVICIOS_LOG_Upd]
 On [Dbo].[RESERVAS_DETSERVICIOS] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva_Det from Inserted)
			set @RegistroPK2 = (select IDServicio_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--43
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_DETSERVICIOS_LOG_Del]
 On [Dbo].[RESERVAS_DETSERVICIOS] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDReserva_Det from Deleted)
			set @RegistroPK2 = (select IDServicio_Det from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--39
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_DETSERVICIOS_LOG_Ins]
 On [Dbo].[RESERVAS_DETSERVICIOS] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva_Det from Inserted)
			set @RegistroPK2 = (select IDServicio_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
