﻿CREATE TABLE [dbo].[COTIDET_PAX] (
    [IDDet]           INT      NOT NULL,
    [IDPax]           INT      NOT NULL,
    [UserMod]         CHAR (4) NOT NULL,
    [FecMod]          DATETIME DEFAULT (getdate()) NOT NULL,
    [FlDesactNoShow]  BIT      DEFAULT ((0)) NOT NULL,
    [FlEntMPGenerada] BIT      DEFAULT ((0)) NOT NULL,
    [FlEntWPGenerada] BIT      DEFAULT ((0)) NOT NULL,
    [FlEntRQGenerada] BIT      DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_COTIDET_PAX] PRIMARY KEY NONCLUSTERED ([IDPax] ASC, [IDDet] ASC),
    CONSTRAINT [FK_COTIDET_PAX_COTIDET] FOREIGN KEY ([IDDet]) REFERENCES [dbo].[COTIDET] ([IDDET]),
    CONSTRAINT [FK_COTIDET_PAX_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax])
);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_PAX_IDDet]
    ON [dbo].[COTIDET_PAX]([IDDet] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_PAX_FlDesactNoShow_FlEntMPGenerada_IDDet]
    ON [dbo].[COTIDET_PAX]([FlDesactNoShow] ASC, [FlEntMPGenerada] ASC, [IDDet] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_PAX_FlDesactNoShow_IDDet]
    ON [dbo].[COTIDET_PAX]([FlDesactNoShow] ASC, [IDDet] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_PAX_FlDesactNoShow_FlEntWPGenerada_IDDet]
    ON [dbo].[COTIDET_PAX]([FlDesactNoShow] ASC, [FlEntWPGenerada] ASC, [IDDet] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_PAX]
    ON [dbo].[COTIDET_PAX]([FlDesactNoShow] ASC)
    INCLUDE([IDDet], [IDPax]);


GO

--152
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIDET_PAX_LOG_Ins]
  On [dbo].[COTIDET_PAX] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDet from Inserted)
			set @RegistroPK2 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--156
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_PAX_LOG_Del]
  On [dbo].[COTIDET_PAX] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDet from Deleted)
			set @RegistroPK2 = (select IDPax from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--154
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_PAX_LOG_Upd]
  On [dbo].[COTIDET_PAX] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDet from Inserted)
			set @RegistroPK2 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
