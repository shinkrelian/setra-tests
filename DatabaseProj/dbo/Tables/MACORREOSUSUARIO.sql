﻿CREATE TABLE [dbo].[MACORREOSUSUARIO] (
    [IDUsuario]   CHAR (4)      NOT NULL,
    [Correo]      VARCHAR (150) NOT NULL,
    [Descripcion] VARCHAR (MAX) NULL,
    [Estado]      CHAR (1)      DEFAULT ('A') NULL,
    [UserMod]     CHAR (4)      NULL,
    [FecMod]      DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_MACORREOSUSUARIO] PRIMARY KEY NONCLUSTERED ([IDUsuario] ASC, [Correo] ASC),
    CONSTRAINT [FK_MACORREOSUSUARIO_MAUSUARIOS] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario])
);

