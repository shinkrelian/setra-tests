﻿CREATE TABLE [dbo].[ORDEN_SERVICIO_DET] (
    [NuOrden_Servicio_Det] INT           NOT NULL,
    [NuOrden_Servicio]     INT           NOT NULL,
    [IDOperacion_Det]      INT           NOT NULL,
    [IDServicio_Det]       INT           NOT NULL,
    [UserMod]              CHAR (4)      NOT NULL,
    [FecMod]               SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [FlServicioNoShow]     BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ORDEN_SERVICIO_DET] PRIMARY KEY CLUSTERED ([NuOrden_Servicio_Det] ASC),
    CONSTRAINT [FK_ORDEN_SERVICIO_DET_OPERACIONES_DET_DETSERVICIOS] FOREIGN KEY ([IDOperacion_Det], [IDServicio_Det], [FlServicioNoShow]) REFERENCES [dbo].[OPERACIONES_DET_DETSERVICIOS] ([IDOperacion_Det], [IDServicio_Det], [FlServicioNoShow]),
    CONSTRAINT [FK_ORDEN_SERVICIO_DET_ORDEN_SERVICIO] FOREIGN KEY ([NuOrden_Servicio]) REFERENCES [dbo].[ORDEN_SERVICIO] ([NuOrden_Servicio])
);


GO
CREATE NONCLUSTERED INDEX [IX_NuOrdenServicio]
    ON [dbo].[ORDEN_SERVICIO_DET]([NuOrden_Servicio] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_ORDEN_SERVICIO_DET]
    ON [dbo].[ORDEN_SERVICIO_DET]([IDOperacion_Det] ASC, [IDServicio_Det] ASC, [FlServicioNoShow] ASC);


GO
Create Trigger [dbo].[ORDEN_SERVICIO_DET_LOG_Upd]  
  On [dbo].[ORDEN_SERVICIO_DET] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio_Det from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO_DET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[ORDEN_SERVICIO_DET_LOG_Del]  
  On [dbo].[ORDEN_SERVICIO_DET] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio_Det from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO_DET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[ORDEN_SERVICIO_DET_LOG_Ins]  
  On [dbo].[ORDEN_SERVICIO_DET] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio_Det from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO_DET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
