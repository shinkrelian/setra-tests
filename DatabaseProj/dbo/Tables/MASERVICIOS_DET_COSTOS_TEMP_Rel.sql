﻿CREATE TABLE [dbo].[MASERVICIOS_DET_COSTOS_TEMP_Rel] (
    [Correlativo]    TINYINT        NOT NULL,
    [IDServicio_Det] INT            NOT NULL,
    [PaxDesde]       SMALLINT       NOT NULL,
    [PaxHasta]       SMALLINT       NOT NULL,
    [Monto]          NUMERIC (8, 2) NOT NULL,
    [Mig]            BIT            NOT NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       NOT NULL
);

