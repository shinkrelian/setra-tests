﻿CREATE TABLE [dbo].[COTI_CATEGORIAHOTELES] (
    [IDCab]          INT          NOT NULL,
    [IDUbigeo]       CHAR (6)     NOT NULL,
    [Categoria]      VARCHAR (20) NOT NULL,
    [IDProveedor]    CHAR (6)     NOT NULL,
    [NroOrd]         TINYINT      NOT NULL,
    [IDServicio_Det] INT          NOT NULL,
    [UserMod]        CHAR (4)     NOT NULL,
    [FecMod]         DATETIME     CONSTRAINT [DF__COTI_CATE__FecMo__5402595F] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTI_CATEGORIAHOTELES] PRIMARY KEY NONCLUSTERED ([IDCab] ASC, [IDUbigeo] ASC, [Categoria] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_COTI_CATEGORIAHOTELES_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_COTI_CATEGORIAHOTELES_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_COTI_CATEGORIAHOTELES_MAUBIGEO] FOREIGN KEY ([IDUbigeo]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO

--91
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTI_CATEGORIAHOTELES_LOG_Upd]
 On [Dbo].[COTI_CATEGORIAHOTELES] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @RegistroPK4 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDCab from Inserted)
			set @RegistroPK2 = (select IDUbigeo from Inserted)
			set @RegistroPK3 = (select Categoria from Inserted)
			set @RegistroPK4 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_CATEGORIAHOTELES',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,@RegistroPK4,@Accion
		End  

GO

--89
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTI_CATEGORIAHOTELES_LOG_Ins]
 On [Dbo].[COTI_CATEGORIAHOTELES] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @RegistroPK4 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDCab from Inserted)
			set @RegistroPK2 = (select IDUbigeo from Inserted)
			set @RegistroPK3 = (select Categoria from Inserted)
			set @RegistroPK4 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_CATEGORIAHOTELES',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,@RegistroPK4,@Accion
		End  

GO

--93
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTI_CATEGORIAHOTELES_LOG_Del]
 On [Dbo].[COTI_CATEGORIAHOTELES] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @RegistroPK4 varchar(150)	
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDCab from Deleted)
			set @RegistroPK2 = (select IDUbigeo from Deleted)
			set @RegistroPK3 = (select Categoria from Inserted)
			set @RegistroPK4 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_CATEGORIAHOTELES',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,@RegistroPK4,@Accion
		End  
