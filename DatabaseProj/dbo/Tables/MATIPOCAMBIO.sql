﻿CREATE TABLE [dbo].[MATIPOCAMBIO] (
    [Fecha]     SMALLDATETIME  NOT NULL,
    [ValCompra] DECIMAL (6, 3) NOT NULL,
    [ValVenta]  DECIMAL (6, 3) NOT NULL,
    [UserMod]   CHAR (4)       NOT NULL,
    [FecMod]    DATETIME       CONSTRAINT [DF_MATIPOCAMBIO_FecMod] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_MATIPOCAMBIO] PRIMARY KEY CLUSTERED ([Fecha] ASC),
    CONSTRAINT [CK_MATIPOCAMBIO_ValCompra_ValVenta] CHECK ([ValCompra]<=[ValVenta])
);


GO
--209
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MATIPOCAMBIO

CREATE Trigger [dbo].[MATIPOCAMBIO_LOG_Ins]
  On dbo.MATIPOCAMBIO for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select Fecha from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOCAMBIO',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--212
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOCAMBIO_LOG_Upd]
  On dbo.MATIPOCAMBIO for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select Fecha from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOCAMBIO',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--215
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOCAMBIO_LOG_Del]
  On dbo.MATIPOCAMBIO for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select Fecha from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOCAMBIO',@UserMod,@RegistroPK1,'','','',@Accion
		End
