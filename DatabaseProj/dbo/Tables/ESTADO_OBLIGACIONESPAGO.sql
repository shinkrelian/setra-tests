﻿CREATE TABLE [dbo].[ESTADO_OBLIGACIONESPAGO] (
    [CoEstado]      CHAR (2)      NOT NULL,
    [NoEstado]      VARCHAR (50)  NOT NULL,
    [TxDescripcion] VARCHAR (250) NULL,
    [UserMod]       CHAR (4)      NOT NULL,
    [FecMod]        DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ESTADO_OBLIGACIONESPAGO] PRIMARY KEY NONCLUSTERED ([CoEstado] ASC)
);

