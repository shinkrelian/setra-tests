﻿CREATE TABLE [dbo].[RPT_ENTRADASINC] (
    [IDFile]                   CHAR (8)        NOT NULL,
    [IDCab]                    INT             NOT NULL,
    [DescCliente]              VARCHAR (MAX)   NOT NULL,
    [Titulo]                   VARCHAR (MAX)   NOT NULL,
    [FechaINCusco]             SMALLDATETIME   NOT NULL,
    [FechaMPWP]                SMALLDATETIME   NOT NULL,
    [Pax]                      SMALLINT        NOT NULL,
    [EntCompradasMAPI]         SMALLINT        NOT NULL,
    [EntPendientesMAPI]        SMALLINT        NOT NULL,
    [EntCompradasWAYNA]        SMALLINT        NOT NULL,
    [EntPendientesWAYNA]       SMALLINT        NOT NULL,
    [EntCompradas]             SMALLINT        NOT NULL,
    [EntPendientes]            SMALLINT        NOT NULL,
    [Tarifa]                   NUMERIC (12, 4) NOT NULL,
    [DescServicio]             VARCHAR (MAX)   NOT NULL,
    [EjecReservas]             VARCHAR (100)   NOT NULL,
    [DatosPaxIncompletos]      BIT             NOT NULL,
    [ExisteNinio]              BIT             NOT NULL,
    [CantNinios]               SMALLINT        NOT NULL,
    [FileAutorizado]           SMALLINT        NOT NULL,
    [CodMapioWaynaoRaqchioMyW] CHAR (8)        NOT NULL,
    [IDDet]                    INT             NOT NULL,
    [Estado]                   VARCHAR (MAX)   NOT NULL,
    [FlPostGenerado]           BIT             NOT NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_RPT_EntradasINC]
    ON [dbo].[RPT_ENTRADASINC]([IDCab] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RPT_ENTRADASINC_CodMapioWaynaoRaqchioMyW]
    ON [dbo].[RPT_ENTRADASINC]([CodMapioWaynaoRaqchioMyW] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RPT_ENTRADASINC_IDCab_CodMapio_FlPostGenerado]
    ON [dbo].[RPT_ENTRADASINC]([IDCab] ASC, [CodMapioWaynaoRaqchioMyW] ASC, [FlPostGenerado] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RPT_ENTRADASINC_IDCab_CodMapio]
    ON [dbo].[RPT_ENTRADASINC]([IDCab] ASC, [CodMapioWaynaoRaqchioMyW] ASC);

