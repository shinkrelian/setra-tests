﻿CREATE TABLE [dbo].[RH_CARGO] (
    [CoCargo]   TINYINT       NOT NULL,
    [TxCargo]   VARCHAR (250) NOT NULL,
    [FlActivo]  BIT           CONSTRAINT [DF_RH_CARGO_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]   CHAR (4)      NOT NULL,
    [FecMod]    DATETIME      CONSTRAINT [DF_RH_CARGO_FecMod] DEFAULT (getdate()) NOT NULL,
    [CoEmpresa] TINYINT       NULL,
    CONSTRAINT [PK_RH_CARGO] PRIMARY KEY CLUSTERED ([CoCargo] ASC)
);

