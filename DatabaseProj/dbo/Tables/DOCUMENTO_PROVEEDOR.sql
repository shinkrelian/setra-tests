﻿CREATE TABLE [dbo].[DOCUMENTO_PROVEEDOR] (
    [NuDocumProv]          INT             NOT NULL,
    [NuVoucher]            INT             NULL,
    [NuDocum]              VARCHAR (30)    NOT NULL,
    [CoTipoDoc]            CHAR (3)        NOT NULL,
    [FeEmision]            SMALLDATETIME   NOT NULL,
    [FeRecepcion]          SMALLDATETIME   NOT NULL,
    [CoTipoDetraccion]     CHAR (5)        NULL,
    [CoMoneda]             CHAR (3)        NOT NULL,
    [SsIGV]                NUMERIC (8, 2)  NOT NULL,
    [SsTotalOriginal]      NUMERIC (9, 2)  NOT NULL,
    [CoEstado]             CHAR (2)        CONSTRAINT [DF_DOCUMENTO_PROVEEDOR_CoEstadi] DEFAULT ('RE') NOT NULL,
    [UserMod]              CHAR (4)        NOT NULL,
    [FecMod]               DATETIME        CONSTRAINT [DF_DOCUMENTO_PROVEEDOR_FecMod] DEFAULT (getdate()) NOT NULL,
    [NuDocInterno]         CHAR (8)        NULL,
    [FlActivo]             BIT             DEFAULT ((1)) NOT NULL,
    [SsNeto]               NUMERIC (9, 2)  NOT NULL,
    [SsOtrCargos]          NUMERIC (9, 2)  NULL,
    [SsDetraccion]         NUMERIC (9, 2)  NULL,
    [SSTipoCambio]         DECIMAL (6, 3)  NULL,
    [SSTotal]              NUMERIC (9, 2)  NULL,
    [CoTipoOC]             CHAR (3)        NULL,
    [CoCtaContab]          VARCHAR (14)    NULL,
    [CoMoneda_Pago]        CHAR (3)        NULL,
    [NuOrden_Servicio]     INT             NULL,
    [CoObligacionPago]     CHAR (3)        DEFAULT ('VOU') NULL,
    [CoMoneda_FEgreso]     CHAR (3)        NULL,
    [SsTipoCambio_FEgreso] NUMERIC (6, 3)  NULL,
    [SsTotal_FEgreso]      NUMERIC (9, 2)  NULL,
    [UserNuevo]            CHAR (4)        NULL,
    [CoOrdPag]             INT             NULL,
    [IDOperacion]          INT             NULL,
    [NuOrdComInt]          INT             NULL,
    [NuVouOfiExtInt]       INT             NULL,
    [NuDocumVta]           CHAR (10)       NULL,
    [CoTipoDocVta]         CHAR (3)        NULL,
    [NuPreSob]             INT             NULL,
    [NuVouPTrenInt]        INT             NULL,
    [NuSerie]              VARCHAR (10)    NULL,
    [CoCeCos]              CHAR (6)        NULL,
    [NuFondoFijo]          INT             NULL,
    [CoTipoFondoFijo]      CHAR (3)        NULL,
    [SSPercepcion]         NUMERIC (12, 2) NULL,
    [TxConcepto]           VARCHAR (MAX)   NULL,
    [CoProveedor]          CHAR (6)        NULL,
    [IDCab]                INT             NULL,
    [FeVencimiento]        DATETIME        NULL,
    [CoTipoDocSAP]         TINYINT         NULL,
    [CoFormaPago]          CHAR (3)        NULL,
    [CoTipoDoc_Ref]        CHAR (3)        NULL,
    [NuSerie_Ref]          VARCHAR (10)    NULL,
    [NuDocum_Ref]          VARCHAR (30)    NULL,
    [FeEmision_Ref]        DATETIME        NULL,
    [CoCeCon]              CHAR (6)        NULL,
    [CoCtaContab_SAP]      VARCHAR (14)    NULL,
    [CardCodeSAP]          VARCHAR (15)    NULL,
    [CoGasto]              VARCHAR (12)    NULL,
    [CoEstadoPago]         VARCHAR (2)     DEFAULT (NULL) NULL,
    [FlCorrelativo]        BIT             DEFAULT ((0)) NOT NULL,
    [NuDocum_Multiple]     INT             NULL,
    [CoTipoCompra]         CHAR (2)        NULL,
    [FlMultiple]           BIT             DEFAULT ((0)) NOT NULL,
    [CoTipOpeDet]          CHAR (2)        NULL,
    [CoTipDocSusEnt]       CHAR (3)        NULL,
    [NuDocumSusEnt]        CHAR (15)       NULL,
    [NuOperBanSusEnt]      VARCHAR (20)    NULL,
    [IDDocFormaEgreso]     INT             NULL,
    CONSTRAINT [PK_DOCUMENTO_PROVEEDOR] PRIMARY KEY NONCLUSTERED ([NuDocumProv] ASC),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_MAMONEDA] FOREIGN KEY ([CoMoneda_FEgreso]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_MAMONEDAS] FOREIGN KEY ([CoMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_MAMONEDAS_PAGO] FOREIGN KEY ([CoMoneda_Pago]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_MATIPODETRACCION] FOREIGN KEY ([CoTipoDetraccion]) REFERENCES [dbo].[MATIPODETRACCION] ([CoTipoDetraccion]),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_MATIPODOC] FOREIGN KEY ([CoTipoDoc]) REFERENCES [dbo].[MATIPODOC] ([IDtipodoc]),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_ORDEN_SERVICIO] FOREIGN KEY ([NuOrden_Servicio]) REFERENCES [dbo].[ORDEN_SERVICIO] ([NuOrden_Servicio]),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_ORDENPAGO] FOREIGN KEY ([CoOrdPag]) REFERENCES [dbo].[ORDENPAGO] ([IDOrdPag]),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_VOUCHER_OPERACIONES] FOREIGN KEY ([NuVoucher]) REFERENCES [dbo].[VOUCHER_OPERACIONES] ([IDVoucher])
);


GO
CREATE NONCLUSTERED INDEX [IX_DOCUMENTO_PROVEEDOR_CoMoneda_Pago]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([CoMoneda_Pago] ASC)
    INCLUDE([NuDocum], [SSTotal]);


GO
CREATE NONCLUSTERED INDEX [IX_DOCUMENTO_PROVEEDOR]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([FlActivo] ASC, [CoOrdPag] ASC)
    INCLUDE([NuDocum], [FeRecepcion]);


GO
CREATE NONCLUSTERED INDEX [DOCUMENTO_PROVEEDOR_NuDocum]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([NuDocum] ASC, [CoMoneda_Pago] ASC)
    INCLUDE([SSTotal]);


GO
CREATE NONCLUSTERED INDEX [IX_DOCUMENTO_PROVEEDOR_NuOrden_Servicio]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([NuOrden_Servicio] ASC)
    INCLUDE([NuDocum]);


GO
CREATE NONCLUSTERED INDEX [IX_DOCUMENTO_PROVEEDOR_FlActivo]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([FlActivo] ASC)
    INCLUDE([NuVoucher], [FeRecepcion], [CoMoneda], [SSTotal]);


GO
CREATE NONCLUSTERED INDEX [IX_DOCUMENTO_PROVEEDOR_CoOrdPag]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([CoOrdPag] ASC)
    INCLUDE([NuDocumProv]);


GO
CREATE NONCLUSTERED INDEX [IX_DOCUMENTO_PROVEEDOR_FlActivo2]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([FlActivo] ASC)
    INCLUDE([NuVoucher], [SSTotal], [SsTotal_FEgreso]);


GO
CREATE NONCLUSTERED INDEX [IX_DOCUMENTO_PROVEEDOR_FlActivo3]
    ON [dbo].[DOCUMENTO_PROVEEDOR]([FlActivo] ASC)
    INCLUDE([SSTotal], [SsTotal_FEgreso], [NuVouPTrenInt]);

