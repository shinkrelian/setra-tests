﻿CREATE TABLE [dbo].[COTICAB] (
    [IDCAB]                         INT            IDENTITY (1, 1) NOT NULL,
    [Cotizacion]                    CHAR (8)       NOT NULL,
    [Fecha]                         SMALLDATETIME  CONSTRAINT [DF_COTICAB_Fecha] DEFAULT (getdate()) NOT NULL,
    [Estado]                        CHAR (1)       CONSTRAINT [DF_COTICAB_Estado] DEFAULT ('P') NOT NULL,
    [IDCliente]                     CHAR (6)       NOT NULL,
    [Titulo]                        VARCHAR (100)  NOT NULL,
    [TipoPax]                       CHAR (3)       NOT NULL,
    [CotiRelacion]                  CHAR (8)       NULL,
    [IDUsuario]                     CHAR (4)       NOT NULL,
    [NroPax]                        SMALLINT       NOT NULL,
    [NroLiberados]                  SMALLINT       NULL,
    [Tipo_Lib]                      CHAR (1)       NULL,
    [Margen]                        NUMERIC (8, 2) NULL,
    [IDBanco]                       CHAR (3)       NOT NULL,
    [IDFile]                        CHAR (8)       NULL,
    [IDTipoDoc]                     CHAR (3)       NOT NULL,
    [Observaciones]                 TEXT           NULL,
    [FecInicio]                     SMALLDATETIME  NOT NULL,
    [FechaOut]                      SMALLDATETIME  NOT NULL,
    [Cantidad_dias]                 TINYINT        NOT NULL,
    [FechaSeg]                      SMALLDATETIME  NULL,
    [DatoSeg]                       VARCHAR (MAX)  NULL,
    [TipoCambio]                    NUMERIC (8, 2) NULL,
    [DocVta]                        INT            NULL,
    [TipoServicio]                  CHAR (2)       NOT NULL,
    [IDIdioma]                      VARCHAR (12)   NOT NULL,
    [Simple]                        TINYINT        NULL,
    [Twin]                          TINYINT        NULL,
    [Matrimonial]                   TINYINT        NULL,
    [Triple]                        TINYINT        NULL,
    [IDubigeo]                      CHAR (6)       NOT NULL,
    [Notas]                         TEXT           NULL,
    [Programa_Desc]                 VARCHAR (100)  NULL,
    [PaxAdulE]                      TINYINT        NULL,
    [PaxAdulN]                      TINYINT        NULL,
    [PaxNinoE]                      TINYINT        NULL,
    [PaxNinoN]                      TINYINT        NULL,
    [Aporte]                        BIT            CONSTRAINT [DF_COTICAB_Aporte] DEFAULT ((1)) NOT NULL,
    [AporteMonto]                   NUMERIC (8, 2) NULL,
    [AporteTotal]                   NUMERIC (8, 2) NULL,
    [Redondeo]                      NUMERIC (3, 1) NOT NULL,
    [IDCabCopia]                    INT            NULL,
    [DocWordIDIdioma]               VARCHAR (12)   CONSTRAINT [DF_COTICAB_DocWordIDIdioma] DEFAULT ('NO APLICA') NOT NULL,
    [DocWordRutaImagen]             VARCHAR (200)  NULL,
    [DocWordSubTitulo]              TEXT           NULL,
    [DocWordTexto]                  TEXT           NULL,
    [IdCoti_Serie]                  SMALLINT       NULL,
    [Categoria]                     VARCHAR (20)   NULL,
    [MargenCero]                    BIT            CONSTRAINT [DF__COTICAB__MargenC__1BF30A66] DEFAULT ((0)) NOT NULL,
    [IDUsuarioRes]                  CHAR (4)       CONSTRAINT [DF_COTICAB_IDUsuarioRes] DEFAULT ('0000') NOT NULL,
    [IDUsuarioOpe]                  CHAR (4)       CONSTRAINT [DF_COTICAB_IDUsuarioOpe] DEFAULT ('0000') NOT NULL,
    [FechaReserva]                  SMALLDATETIME  NULL,
    [FechaOperaciones]              SMALLDATETIME  NULL,
    [UserMod]                       CHAR (4)       NOT NULL,
    [FecMod]                        DATETIME       CONSTRAINT [DF_COTICAB_FecMod] DEFAULT (getdate()) NOT NULL,
    [IDDebitMemo]                   CHAR (8)       NULL,
    [SimpleResidente]               TINYINT        NULL,
    [TwinResidente]                 TINYINT        NULL,
    [MatrimonialResidente]          TINYINT        NULL,
    [TripleResidente]               TINYINT        NULL,
    [DiasReconfirmacion]            TINYINT        NOT NULL,
    [FechaAsigReserva]              SMALLDATETIME  CONSTRAINT [DF__COTICAB__FechaAs__6B5AC003] DEFAULT (NULL) NULL,
    [EstadoFacturac]                CHAR (1)       CONSTRAINT [DF__COTICAB__EstadoF__5D96B091] DEFAULT ('P') NOT NULL,
    [ObservacionGeneral]            VARCHAR (MAX)  NULL,
    [ObservacionVentas]             VARCHAR (MAX)  NULL,
    [CoTipoVenta]                   CHAR (2)       CONSTRAINT [DF__COTICAB__CoTipoV__32774862] DEFAULT ('RC') NOT NULL,
    [DocWordVersion]                TINYINT        CONSTRAINT [DF__COTICAB__DocWord__3647D946] DEFAULT ((1)) NOT NULL,
    [DocWordFecha]                  SMALLDATETIME  NULL,
    [FeFacturac]                    SMALLDATETIME  NULL,
    [NuSerieCliente]                INT            CONSTRAINT [DF__COTICAB__NuSerie__558B7A75] DEFAULT (NULL) NULL,
    [FechaAsigOperaciones]          SMALLDATETIME  CONSTRAINT [DF__COTICAB__FechaAs__6D630406] DEFAULT (NULL) NULL,
    [FechaRetornoReservas]          SMALLDATETIME  CONSTRAINT [DF__COTICAB__FechaRe__6E57283F] DEFAULT (NULL) NULL,
    [FechaEntregaKitPax]            SMALLDATETIME  CONSTRAINT [DF__COTICAB__FechaEn__6F4B4C78] DEFAULT (NULL) NULL,
    [FeAnulacion]                   SMALLDATETIME  NULL,
    [IDUsuarioAn]                   CHAR (4)       NULL,
    [PoConcretVta]                  NUMERIC (5, 2) NULL,
    [FlPoConcretAutoUpd]            BIT            NULL,
    [FlPoConcretManualUpd]          BIT            CONSTRAINT [DF__COTICAB__FlPoCon__1121418F] DEFAULT ((0)) NOT NULL,
    [FechaReservaBup]               SMALLDATETIME  NULL,
    [IDUsuarioOpe_Cusco]            CHAR (4)       NULL,
    [CoSerieWeb]                    VARCHAR (6)    CONSTRAINT [DF__COTICAB__CoSerie__2CB44D6B] DEFAULT (NULL) NULL,
    [FlModificadoxTI]               BIT            CONSTRAINT [DF__COTICAB__FlModif__7BA701BC] DEFAULT ((0)) NULL,
    [FlFileMigA_SAP]                BIT            CONSTRAINT [DF__COTICAB__FlFileM__49A591D4] DEFAULT ((0)) NOT NULL,
    [FlCorreoAnuEnviado]            BIT            CONSTRAINT [DF__COTICAB__FlCorre__7AD2D914] DEFAULT ((0)) NOT NULL,
    [FlHistorico]                   BIT            CONSTRAINT [DF__COTICAB__FlHisto__27707561] DEFAULT ((0)) NOT NULL,
    [FlFileAnulaEn_SAP]             BIT            CONSTRAINT [DF__COTICAB__FlFileA__06CE9BA5] DEFAULT ((0)) NOT NULL,
    [FlCorreoEnviadoxCotAceptada]   BIT            CONSTRAINT [DF__COTICAB__FlCorre__08B6E417] DEFAULT ((0)) NOT NULL,
    [FlCorreoEliEnviado]            BIT            CONSTRAINT [DF__COTICAB__FlCorre__09AB0850] DEFAULT ((0)) NOT NULL,
    [FlUpdateSAPMassive]            BIT            CONSTRAINT [DF__COTICAB__FlUpdat__0E3AB343] DEFAULT ((0)) NOT NULL,
    [NuPedInt]                      INT            NULL,
    [CoEstadoAddIn]                 CHAR (2)       CONSTRAINT [DF__COTICAB__CoEstad__7D9A3726] DEFAULT ('PD') NOT NULL,
    [FeOKClienteAddIn]              SMALLDATETIME  CONSTRAINT [DF__COTICAB__FeOKCli__7E8E5B5F] DEFAULT (NULL) NULL,
    [NoRuta]                        VARCHAR (200)  CONSTRAINT [DF__COTICAB__NoRuta__5833A84D] DEFAULT (NULL) NULL,
    [FechaRetornoReservas_2doEnvio] SMALLDATETIME  NULL,
    [IDContactoCliente]             CHAR (3)       NULL,
    [Test]                          VARCHAR (20)   NOT NULL,
    CONSTRAINT [PK_COTICAB] PRIMARY KEY NONCLUSTERED ([IDCAB] ASC),
    CONSTRAINT [CK_COTICAB_CoTipoVenta] CHECK ([CoTipoVenta]='VC' OR [CoTipoVenta]='VS' OR [CoTipoVenta]='CU' OR [CoTipoVenta]='RC' OR [CoTipoVenta]='OP' OR [CoTipoVenta]='TE' OR [CoTipoVenta]='DR'),
    CONSTRAINT [Fk_COTICAB_COTI_SERIE] FOREIGN KEY ([IdCoti_Serie]) REFERENCES [dbo].[COTI_SERIE] ([IdCoti_Serie]),
    CONSTRAINT [FK_COTICAB_MABANCOS] FOREIGN KEY ([IDBanco]) REFERENCES [dbo].[MABANCOS] ([IDBanco]),
    CONSTRAINT [FK_COTICAB_MACLIENTES] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[MACLIENTES] ([IDCliente]),
    CONSTRAINT [FK_COTICAB_MAIDIOMAS] FOREIGN KEY ([IDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_COTICAB_MAIDIOMAS1] FOREIGN KEY ([DocWordIDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_COTICAB_MAMODOSERVICIO] FOREIGN KEY ([TipoServicio]) REFERENCES [dbo].[MAMODOSERVICIO] ([CodModoServ]),
    CONSTRAINT [FK_COTICAB_MATIPODOC] FOREIGN KEY ([IDTipoDoc]) REFERENCES [dbo].[MATIPODOC] ([IDtipodoc]),
    CONSTRAINT [FK_COTICAB_MAUBIGEO] FOREIGN KEY ([IDubigeo]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_COTICAB_MAUSUARIOS] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario]),
    CONSTRAINT [FK_COTICAB_MAUSUARIOS_Ope] FOREIGN KEY ([IDUsuarioOpe]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario]),
    CONSTRAINT [FK_COTICAB_MAUSUARIOS_Res] FOREIGN KEY ([IDUsuarioRes]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario]),
    CONSTRAINT [FK_COTICAB_PEDIDO] FOREIGN KEY ([NuPedInt]) REFERENCES [dbo].[PEDIDO] ([NuPedInt])
);


GO
CREATE NONCLUSTERED INDEX [IX_Estado]
    ON [dbo].[COTICAB]([Estado] ASC)
    INCLUDE([IDCAB]);


GO
CREATE NONCLUSTERED INDEX [IX_COTICAB_IDCab_IDUsuarioRes]
    ON [dbo].[COTICAB]([IDFile] ASC)
    INCLUDE([IDCAB], [IDUsuarioRes]);


GO
CREATE NONCLUSTERED INDEX [IX_COTICAB_IDCliente]
    ON [dbo].[COTICAB]([IDCliente] ASC)
    INCLUDE([IDCAB], [Estado], [CoTipoVenta]);


GO
CREATE NONCLUSTERED INDEX [IX_COTICAB_Estado_EstadoFacturado]
    ON [dbo].[COTICAB]([Estado] ASC, [EstadoFacturac] ASC)
    INCLUDE([IDCAB], [IDCliente], [Titulo], [IDFile], [FechaOut], [IDubigeo], [CoTipoVenta]);


GO
CREATE NONCLUSTERED INDEX [IX_COTICAB_EstadoFacturac]
    ON [dbo].[COTICAB]([EstadoFacturac] ASC)
    INCLUDE([IDCAB], [IDCliente], [Titulo], [IDFile], [FechaOut], [IDubigeo], [CoTipoVenta]);


GO
CREATE NONCLUSTERED INDEX [COTICAB_Estado_FlHistorico]
    ON [dbo].[COTICAB]([Estado] ASC, [FlHistorico] ASC)
    INCLUDE([IDCAB], [Fecha], [IDUsuario], [IDFile], [FecInicio], [FechaOut], [IDUsuarioRes], [IDUsuarioOpe], [FechaReserva], [FechaAsigReserva]);


GO
CREATE NONCLUSTERED INDEX [IX_COTICAB_Estado_FlHistorico]
    ON [dbo].[COTICAB]([Estado] ASC, [FlHistorico] ASC)
    INCLUDE([IDCAB], [Fecha], [IDUsuario], [IDFile], [FecInicio], [FechaOut], [IDUsuarioRes], [IDUsuarioOpe], [FechaReserva], [FechaAsigReserva]);


GO
CREATE NONCLUSTERED INDEX [IX_NuPedInt]
    ON [dbo].[COTICAB]([NuPedInt] ASC);


GO
--11
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTICAB_LOG_Ins]
  On dbo.COTICAB for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTICAB',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--14
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTICAB_LOG_Upd]
  On dbo.COTICAB for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTICAB',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--8
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTICAB_LOG_Del]
  On dbo.COTICAB for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTICAB',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--HLF-20150309-@Total=Total
--HLF-20150316-@Total=dbo.FnValorVentaFile(@IDCab),
--Se cambio @NuUpd tinyint por @NuUpd smallint
CREATE Trigger [dbo].[COTICAB_HISTORIAL_PVW_Trg_Ins]
On dbo.COTICAB for Insert, Update
As
	Set NoCount On
	If (select count(*) from Inserted)=1
		Begin
		Declare @IDFile char(8),@FechaOut smalldatetime,@Estado char(1),
		@PoConcretVta numeric(5,2), @Margen numeric(8,2),@NroPax smallint,
		@NroLiberados smallint, @Total numeric(11,2), @IDCab int,
		@UserUpd char(4),@SsValorProyectado numeric(11,2)
		
		--Select @IDCab=IDCab from Inserted
		
		Select @IDCab=IDCab,
			@IDFile=IDFile, @FechaOut=FechaOut, @Estado=Estado,
			@PoConcretVta=PoConcretVta, @Margen=Margen, @NroPax=NroPax,
			@NroLiberados=NroLiberados, 
			--@Total=isnull((Select sum(total*nropax) From Cotidet Where idcab=i.idcab),0),
			@Total=dbo.FnValorVentaFile(@IDCab),
			@UserUpd=UserMod
		From Inserted i
		
		Set @SsValorProyectado=@Total*@PoConcretVta
		
		Declare @NuUpd smallint=1
		Select @NuUpd=NuUpd+1 From COTICAB_HISTORIAL_PVW Where IDCab=@IDCab
		
		If @NuUpd = 1
			Insert Into COTICAB_HISTORIAL_PVW(
				IDCab,NuUpd,CoAccion,IDFile,FechaOut,Estado,PoConcretVta,
				Margen,NroPax,NroLiberados,Total,SsValorProyectado,UserMod)
			Values(@IDCab,@NuUpd,'I',@IDFile,@FechaOut,@Estado,@PoConcretVta,
				@Margen,@NroPax,@NroLiberados,@Total,isnull(@SsValorProyectado,0),@UserUpd)
		Else
			Begin
			
			--If (Select count(*) From COTICAB_HISTORIAL_PVW Where IDCab=@IDCab)=2
			--	Delete From COTICAB_HISTORIAL_PVW Where IDCab=@IDCab 
			--		And NuUpd=(Select Top 1 NuUpd From COTICAB_HISTORIAL_PVW 
			--			Where IDCab=@IDCab Order by NuUpd)
			
			Insert Into COTICAB_HISTORIAL_PVW(
				IDCab,NuUpd,CoAccion,IDFile,FechaOut,Estado,PoConcretVta,
				Margen,NroPax,NroLiberados,Total,SsValorProyectado,UserMod)
			Values(@IDCab,@NuUpd,'U',@IDFile,@FechaOut,@Estado,@PoConcretVta,
				@Margen,@NroPax,@NroLiberados,@Total,isnull(@SsValorProyectado,0),@UserUpd)
				
				
			--Declare @tiNuUpd1 tinyint=(Select min(NuUpd) From COTICAB_HISTORIAL_PVW Where IDCab=@IDCab)
			Declare @tiNuUpd2 smallint=(Select max(NuUpd) From COTICAB_HISTORIAL_PVW Where IDCab=@IDCab)
			Declare @tiNuUpd1 smallint=@tiNuUpd2-1
			

			Select 
			@IDFile=IDFile, @FechaOut=FechaOut, @Estado=Estado,
			@PoConcretVta=PoConcretVta, @Margen=Margen, @NroPax=NroPax,
			@NroLiberados=NroLiberados, 
			--@Total=isnull((Select sum(total*nropax) From Cotidet Where idcab=i.idcab),0)
			@Total=Total
			From COTICAB_HISTORIAL_PVW i Where IDCab=@IDCab 
					And NuUpd=@tiNuUpd1

			Declare 
			--@IDFile2 char(8),
			@FechaOut2 smalldatetime,
			@Estado2 char(1),
			@PoConcretVta2 numeric(5,2), 
			--@Margen2 numeric(8,2),
			@NroPax2 smallint,
			--@NroLiberados2 smallint, 
			@Total2 numeric(9,2)


			Select 
			--@IDFile2=IDFile, 
			@FechaOut2=FechaOut, 
			@Estado2=Estado,
			@PoConcretVta2=PoConcretVta, 
			--@Margen2=Margen, 
			@NroPax2=NroPax,
			--@NroLiberados2=NroLiberados, 
			--@Total2=isnull((Select sum(total*nropax) From Cotidet Where idcab=i.idcab),0)
			@Total2=Total
			From COTICAB_HISTORIAL_PVW i Where IDCab=@IDCab 
					And NuUpd=@tiNuUpd2
					
			--If @IDFile<>@IDFile2
			--	Insert Into COTICAB_HISTORIAL_UPD(IDCab,NoCampo,TxValorAntes,
			--		TxValorDespues,UserMod)
			--	Values(@IDCab,'IDFile',@IDFile,@IDFile2,@UserUpd)				

			If @FechaOut<>@FechaOut2
				Insert Into COTICAB_HISTORIAL_UPD(IDCab,NuUpd,NoCampo,TxValorAntes,
					TxValorDespues,UserMod)
				Values(@IDCab,@NuUpd,'FechaOut',convert(varchar,@FechaOut,103),convert(varchar,@FechaOut2,103),@UserUpd)				
				
			If @Estado<>@Estado2
				Insert Into COTICAB_HISTORIAL_UPD(IDCab,NuUpd,NoCampo,TxValorAntes,
					TxValorDespues,UserMod)
				Values(@IDCab,@NuUpd,'Estado',@Estado,@Estado2,@UserUpd)					
			
			If @PoConcretVta<>@PoConcretVta2
				Insert Into COTICAB_HISTORIAL_UPD(IDCab,NuUpd,NoCampo,TxValorAntes,
					TxValorDespues,UserMod)
				Values(@IDCab,@NuUpd,'PoConcretVta',@PoConcretVta,@PoConcretVta2,@UserUpd)					

			--If @Margen<>@Margen2
			--	Insert Into COTICAB_HISTORIAL_UPD(IDCab,NoCampo,TxValorAntes,
			--		TxValorDespues,UserMod)
			--	Values(@IDCab,'Margen',@Margen,@Margen2,@UserUpd)					

			If @NroPax<>@NroPax2
				Insert Into COTICAB_HISTORIAL_UPD(IDCab,NuUpd,NoCampo,TxValorAntes,
					TxValorDespues,UserMod)
				Values(@IDCab,@NuUpd,'NroPax',@NroPax,@NroPax2,@UserUpd)
			
			--If @NroLiberados<>@NroLiberados2
			--	Insert Into COTICAB_HISTORIAL_UPD(IDCab,NoCampo,TxValorAntes,
			--		TxValorDespues,UserMod)
			--	Values(@IDCab,'NroLiberados',@NroLiberados,@NroLiberados2,@UserUpd)		
				
			If @Total<>@Total2
				Insert Into COTICAB_HISTORIAL_UPD(IDCab,NuUpd,NoCampo,TxValorAntes,
					TxValorDespues,UserMod)
				Values(@IDCab,@NuUpd,'Total',@Total,@Total2,@UserUpd)						
				
				
			If Not Exists(Select NuUpd From COTICAB_HISTORIAL_UPD Where IDCab=@IDCab and NuUpd=@NuUpd)
				Delete From COTICAB_HISTORIAL_PVW Where IDCab=@IDCab and NuUpd=@NuUpd
				
			End		
		End
