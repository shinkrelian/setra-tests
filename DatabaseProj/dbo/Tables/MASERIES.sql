﻿CREATE TABLE [dbo].[MASERIES] (
    [IDSerie]     CHAR (3)     NOT NULL,
    [Descripcion] VARCHAR (60) NULL,
    [UserMod]     CHAR (4)     NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF_MASERIES_FecMod] DEFAULT (getdate()) NULL,
    [CoTipoVenta] CHAR (2)     NULL,
    CONSTRAINT [PK_MASERIES] PRIMARY KEY CLUSTERED ([IDSerie] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_MASERIES_CoTipoVenta]
    ON [dbo].[MASERIES]([CoTipoVenta] ASC);


GO

--4
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERIES_LOG_Del]
  On [dbo].[MASERIES] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDSerie from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERIES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--126
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MASERIES

CREATE Trigger [dbo].[MASERIES_LOG_Ins]
  On [dbo].[MASERIES] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDSerie from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERIES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--128
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERIES_LOG_Upd]
  On [dbo].[MASERIES] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDSerie from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERIES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
