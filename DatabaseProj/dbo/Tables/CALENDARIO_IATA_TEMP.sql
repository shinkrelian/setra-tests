﻿CREATE TABLE [dbo].[CALENDARIO_IATA_TEMP] (
    [CoPeriodo] CHAR (9)       NULL,
    [FeIniPer]  NVARCHAR (255) NULL,
    [FeFinPer]  NVARCHAR (255) NULL,
    [FeEntFac]  NVARCHAR (255) NULL,
    [FeDesRep]  NVARCHAR (255) NULL,
    [FePagAge]  NVARCHAR (255) NULL,
    [CoEjeCou]  NVARCHAR (255) NULL,
    [UserMod]   NVARCHAR (255) NULL,
    [FecMod]    NVARCHAR (255) NULL
);

