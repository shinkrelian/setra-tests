﻿CREATE TABLE [dbo].[VOUCHER_OPERACIONES_DET] (
    [IDVoucher]      INT             NOT NULL,
    [CoMoneda]       CHAR (3)        NOT NULL,
    [CoEstado]       CHAR (2)        DEFAULT ('PD') NULL,
    [SsMontoTotal]   NUMERIC (14, 4) DEFAULT (NULL) NULL,
    [SsSaldo]        NUMERIC (14, 4) NULL,
    [SsDifAceptada]  NUMERIC (14, 4) NULL,
    [UserMod]        CHAR (4)        NOT NULL,
    [FecMod]         DATETIME        DEFAULT (getdate()) NOT NULL,
    [TxObsDocumento] VARCHAR (MAX)   NULL,
    [FeCreacion]     DATETIME        NULL,
    CONSTRAINT [PK_VOUCHER_OPERACIONES_DET] PRIMARY KEY NONCLUSTERED ([IDVoucher] ASC, [CoMoneda] ASC),
    CONSTRAINT [FK_VOUCHER_OPERACIONES_DET_ESTADO_OBLIGACIONESPAGO] FOREIGN KEY ([CoEstado]) REFERENCES [dbo].[ESTADO_OBLIGACIONESPAGO] ([CoEstado])
);

