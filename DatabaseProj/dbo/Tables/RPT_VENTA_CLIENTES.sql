﻿CREATE TABLE [dbo].[RPT_VENTA_CLIENTES] (
    [NuAnio]      CHAR (4)        NOT NULL,
    [CoCliente]   CHAR (6)        NOT NULL,
    [SsImporte]   NUMERIC (12, 4) NULL,
    [QtNumFiles]  INT             NULL,
    [SsPromedio]  NUMERIC (12, 4) NULL,
    [SsUtilidad]  NUMERIC (12, 4) NULL,
    [CoTipo]      CHAR (1)        NULL,
    [UserMod]     CHAR (4)        NULL,
    [FecMod]      DATETIME        CONSTRAINT [DF_RPT_VENTA_CLIENTES_FecMod] DEFAULT (getdate()) NULL,
    [QtDias]      INT             NULL,
    [QtEdad_Prom] INT             NULL,
    CONSTRAINT [PK_RPT_VENTA_CLIENTES_1] PRIMARY KEY CLUSTERED ([NuAnio] ASC, [CoCliente] ASC)
);

