﻿CREATE TABLE [dbo].[MAFECHASNAC] (
    [Fecha]           SMALLDATETIME NOT NULL,
    [IDUbigeo]        CHAR (6)      NOT NULL,
    [Descripcion]     VARCHAR (MAX) NOT NULL,
    [UserMod]         CHAR (4)      NOT NULL,
    [FecMod]          DATETIME      CONSTRAINT [DF_MAFECHASNAC_FecMod] DEFAULT (getdate()) NOT NULL,
    [NoConsideraAnio] BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MAFECHASNAC] PRIMARY KEY NONCLUSTERED ([Fecha] ASC, [IDUbigeo] ASC),
    CONSTRAINT [FK_MAFECHASNAC_MAUBIGEO] FOREIGN KEY ([IDUbigeo]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO

--40
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAFECHASNAC_LOG_Upd]
  On [dbo].[MAFECHASNAC] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select Fecha from Inserted)
			set @RegistroPK2 = (select IDUbigeo from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFECHASNAC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--42
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAFECHASNAC_LOG_Del]
  On [dbo].[MAFECHASNAC] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select Fecha from Deleted)
			set @RegistroPK2 = (select IDUbigeo from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFECHASNAC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--38
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAFECHASNAC

CREATE Trigger [dbo].[MAFECHASNAC_LOG_Ins]
  On [dbo].[MAFECHASNAC] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select Fecha from Inserted)
			set @RegistroPK2 = (select IDUbigeo from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFECHASNAC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
