﻿CREATE TABLE [dbo].[SINCERADO_DET_PAX_BUP] (
    [NuSincerado_Det_Pax] INT           NOT NULL,
    [NuSincerado_Det]     INT           NOT NULL,
    [IDPax]               INT           NOT NULL,
    [UserMod]             CHAR (4)      NOT NULL,
    [FecMod]              SMALLDATETIME NOT NULL
);

