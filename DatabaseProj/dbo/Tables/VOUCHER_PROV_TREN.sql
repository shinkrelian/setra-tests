﻿CREATE TABLE [dbo].[VOUCHER_PROV_TREN] (
    [NuVouPTrenInt]  INT            NOT NULL,
    [IDCab]          INT            NOT NULL,
    [CoProveedor]    CHAR (6)       NOT NULL,
    [FeVoucher]      SMALLDATETIME  NOT NULL,
    [CoMoneda]       CHAR (3)       NOT NULL,
    [SsImpuestos]    NUMERIC (8, 2) NOT NULL,
    [SsTotal]        NUMERIC (8, 2) NOT NULL,
    [SsSaldo]        NUMERIC (8, 2) NOT NULL,
    [SsDifAceptada]  NUMERIC (8, 2) NOT NULL,
    [TxObsDocumento] VARCHAR (MAX)  NULL,
    [CoEstado]       CHAR (2)       DEFAULT ('PD') NOT NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       DEFAULT (getdate()) NOT NULL,
    [UserNuevo]      CHAR (4)       NULL,
    CONSTRAINT [PK_VOUCHER_PROV_TREN] PRIMARY KEY NONCLUSTERED ([NuVouPTrenInt] ASC),
    CONSTRAINT [FK_VOUCHER_PROV_TREN_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_VOUCHER_PROV_TREN_MAMONEDAS] FOREIGN KEY ([CoMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_VOUCHER_PROV_TREN_MAPROVEEDORES] FOREIGN KEY ([CoProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);

