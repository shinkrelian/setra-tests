﻿CREATE TABLE [dbo].[VentasporClienteMes] (
    [CoCliente]   CHAR (6)        NULL,
    [NoCliente]   VARCHAR (80)    NULL,
    [CoPais]      CHAR (6)        NULL,
    [NoPais]      VARCHAR (60)    NULL,
    [EneVentaMes] NUMERIC (10, 2) NULL,
    [FebVentaMes] NUMERIC (10, 2) NULL,
    [MarVentaMes] NUMERIC (10, 2) NULL,
    [AbrVentaMes] NUMERIC (10, 2) NULL,
    [MayVentaMes] NUMERIC (10, 2) NULL,
    [JunVentaMes] NUMERIC (10, 2) NULL,
    [JulVentaMes] NUMERIC (10, 2) NULL,
    [AgoVentaMes] NUMERIC (10, 2) NULL,
    [SetVentaMes] NUMERIC (10, 2) NULL,
    [OctVentaMes] NUMERIC (10, 2) NULL,
    [NovVentaMes] NUMERIC (10, 2) NULL,
    [DicVentaMes] NUMERIC (10, 2) NULL,
    [Total]       NUMERIC (13, 2) NULL,
    [ParamAnio]   CHAR (4)        NULL
);

