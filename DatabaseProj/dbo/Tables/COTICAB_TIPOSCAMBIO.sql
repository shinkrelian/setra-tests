﻿CREATE TABLE [dbo].[COTICAB_TIPOSCAMBIO] (
    [IDCab]    INT            NOT NULL,
    [CoMoneda] CHAR (3)       NOT NULL,
    [SsTipCam] DECIMAL (6, 3) NOT NULL,
    [UserMod]  CHAR (4)       NOT NULL,
    [FecMod]   DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTICAB_TIPOSCAMBIO] PRIMARY KEY NONCLUSTERED ([IDCab] ASC, [CoMoneda] ASC)
);

