﻿CREATE TABLE [dbo].[TICKET] (
    [CoTicket]       CHAR (10)      NOT NULL,
    [IDCab]          INT            NULL,
    [IDCliente]      CHAR (6)       NOT NULL,
    [FlArea]         CHAR (1)       NOT NULL,
    [SsTCambioCont]  NUMERIC (4, 2) NULL,
    [SsTCambioVenta] NUMERIC (4, 2) NULL,
    [FeEmision]      SMALLDATETIME  NOT NULL,
    [FeViaje]        SMALLDATETIME  NOT NULL,
    [FeRetorno]      SMALLDATETIME  NOT NULL,
    [IDPax]          INT            NULL,
    [TxPax]          VARCHAR (100)  NULL,
    [IDProveedor]    CHAR (6)       NOT NULL,
    [IDUbigeoOri]    CHAR (6)       NOT NULL,
    [IDUbigeoDes]    CHAR (6)       NOT NULL,
    [TxRuta]         VARCHAR (50)   NOT NULL,
    [CoLugarVenta]   CHAR (2)       NOT NULL,
    [CoCounter]      CHAR (4)       NOT NULL,
    [SsTarifa]       NUMERIC (8, 2) NULL,
    [SsPTA]          NUMERIC (8, 2) NULL,
    [SsPenalidad]    NUMERIC (8, 2) NULL,
    [SsOtros]        NUMERIC (8, 2) NULL,
    [SsIGV]          NUMERIC (6, 2) NULL,
    [SsImpExt]       NUMERIC (8, 2) NULL,
    [PoDscto]        NUMERIC (5, 2) NULL,
    [SsDscto]        NUMERIC (8, 2) NULL,
    [SsTotal]        NUMERIC (8, 2) NOT NULL,
    [CoTarjCred]     CHAR (3)       NULL,
    [CoNroTarjCred]  VARCHAR (25)   NULL,
    [CoNroAprobac]   VARCHAR (25)   NULL,
    [SsTotTarjeta]   NUMERIC (8, 2) NULL,
    [SsTotEfectivo]  NUMERIC (8, 2) NULL,
    [PoComision]     NUMERIC (5, 2) NULL,
    [SsComision]     NUMERIC (8, 2) NULL,
    [SsIGVComision]  NUMERIC (6, 2) NULL,
    [PoTarifa]       NUMERIC (5, 2) NULL,
    [PoOver]         NUMERIC (5, 2) NULL,
    [SsOver]         NUMERIC (8, 2) NULL,
    [SsIGVOver]      NUMERIC (6, 2) NULL,
    [FlMigrado]      BIT            NOT NULL,
    [CoProvOrigen]   CHAR (6)       NOT NULL,
    [TxReferencia]   VARCHAR (200)  NULL,
    [CoTipo]         CHAR (2)       NOT NULL,
    [CoTipoCambio]   CHAR (2)       NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TICKET] PRIMARY KEY NONCLUSTERED ([CoTicket] ASC),
    CONSTRAINT [FK_TICKET_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_TICKET_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax]),
    CONSTRAINT [FK_TICKET_MACLIENTES] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[MACLIENTES] ([IDCliente]),
    CONSTRAINT [FK_TICKET_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_TICKET_MATARJETACREDITO] FOREIGN KEY ([CoTarjCred]) REFERENCES [dbo].[MATARJETACREDITO] ([CodTar]),
    CONSTRAINT [FK_TICKET_MAUBIGEO1] FOREIGN KEY ([IDUbigeoOri]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_TICKET_MAUBIGEO2] FOREIGN KEY ([IDUbigeoDes]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_TICKET_MAUSUARIOS] FOREIGN KEY ([CoCounter]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario])
);


GO
Create Trigger [dbo].[TICKET_LOG_Ins]  
  On [dbo].[TICKET] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_LOG_Upd]  
  On [dbo].[TICKET] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_LOG_Del]  
  On [dbo].[TICKET] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
