﻿CREATE TABLE [dbo].[LOGAUDITORIA] (
    [Equipo]      VARCHAR (90)  NULL,
    [Tabla]       VARCHAR (30)  NULL,
    [UserMod]     CHAR (4)      NULL,
    [RegistroPk1] VARCHAR (150) NULL,
    [RegistroPk2] VARCHAR (150) NULL,
    [RegistroPk3] VARCHAR (150) NULL,
    [RegistroPk4] VARCHAR (150) NULL,
    [FecMod]      DATETIME      DEFAULT (getdate()) NOT NULL,
    [Accion]      CHAR (1)      NOT NULL
);

