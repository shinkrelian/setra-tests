﻿CREATE TABLE [dbo].[OPERACIONES_DET_PAX] (
    [IDOperacion_Det] INT      NOT NULL,
    [IDPax]           INT      NOT NULL,
    [UserMod]         CHAR (4) NOT NULL,
    [FecMod]          DATETIME CONSTRAINT [DF_OPERACIONES_DET_PAX_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OPERACIONES_DET_PAX] PRIMARY KEY NONCLUSTERED ([IDOperacion_Det] ASC, [IDPax] ASC),
    CONSTRAINT [FK_OPERACIONES_DET_PAX_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax]),
    CONSTRAINT [FK_OPERACIONES_DET_PAX_OPERACIONES_DET] FOREIGN KEY ([IDOperacion_Det]) REFERENCES [dbo].[OPERACIONES_DET] ([IDOperacion_Det])
);


GO

--69
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[OPERACIONES_DET_PAX_LOG_Del]
 On [Dbo].[OPERACIONES_DET_PAX] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDOperacion_Det from Deleted)
			set @RegistroPK2 = (select IDPax from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--65
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_DET_PAX_LOG_Ins]
 On [Dbo].[OPERACIONES_DET_PAX] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDOperacion_Det from Inserted)
			set @RegistroPK2 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--67
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[OPERACIONES_DET_PAX_LOG_Upd]
 On [Dbo].[OPERACIONES_DET_PAX] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDOperacion_Det from Inserted)
			set @RegistroPK2 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
