﻿CREATE TABLE [dbo].[MATIPODOC] (
    [IDtipodoc]           CHAR (3)     NOT NULL,
    [Descripcion]         VARCHAR (60) NOT NULL,
    [Sunat]               CHAR (3)     NULL,
    [UserMod]             CHAR (4)     NOT NULL,
    [FecMod]              DATETIME     CONSTRAINT [DF_MATIPODOC_FecMod] DEFAULT (getdate()) NOT NULL,
    [FlOrdenPreLiq]       TINYINT      NULL,
    [NoFiscal]            VARCHAR (60) NOT NULL,
    [CoSunat]             CHAR (2)     NULL,
    [CoStarSoft]          CHAR (2)     NULL,
    [NuOrdenCruceDebMemo] TINYINT      NULL,
    [FlDocProvee]         BIT          DEFAULT ((0)) NOT NULL,
    [CoStarSoft_Compras]  CHAR (2)     NULL,
    [CoUbigeo_Oficina]    CHAR (6)     NULL,
    [CoUbigeo_Oficina2]   CHAR (6)     NULL,
    [FlFondoFijo]         BIT          DEFAULT ((0)) NOT NULL,
    [CoSAP]               CHAR (2)     NULL,
    CONSTRAINT [PK_MATIPODOC] PRIMARY KEY CLUSTERED ([IDtipodoc] ASC)
);


GO

--203
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPODOC_LOG_Upd]
  On [dbo].[MATIPODOC] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDtipodoc from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPODOC',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--206
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPODOC_LOG_Del]
  On [dbo].[MATIPODOC] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDtipodoc from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPODOC',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--200
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MATIPODOC

CREATE Trigger [dbo].[MATIPODOC_LOG_Ins]
  On [dbo].[MATIPODOC] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDtipodoc from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPODOC',@UserMod,@RegistroPK1,'','','',@Accion
		End  
