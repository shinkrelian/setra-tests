﻿CREATE TABLE [dbo].[COTICAB_MAILJOB] (
    [IDCab]              INT           NOT NULL,
    [CorreoDestinatario] VARCHAR (MAX) NOT NULL,
    [Usuario]            VARCHAR (MAX) NOT NULL,
    [DatosIncompletos]   BIT           NOT NULL,
    [FecInicio]          SMALLDATETIME NOT NULL,
    [IDUsuarioRes]       CHAR (4)      NOT NULL,
    [FechaINCusco]       SMALLDATETIME NOT NULL,
    [IDFile]             CHAR (8)      NOT NULL,
    [EnviarMail]         BIT           NOT NULL
);

