﻿CREATE TABLE [dbo].[TICKET_AMADEUS_TARIFA] (
    [CoTicket]    CHAR (13)      NOT NULL,
    [NuTarifa]    TINYINT        NOT NULL,
    [CoCiudadPar] CHAR (3)       NOT NULL,
    [CoCiudadLle] CHAR (3)       NOT NULL,
    [SsTarifa]    NUMERIC (6, 2) NOT NULL,
    [SsQ]         NUMERIC (6, 2) NOT NULL,
    [UserMod]     CHAR (4)       NOT NULL,
    [FecMod]      DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TICKET_AMADEUS_TARIFA] PRIMARY KEY NONCLUSTERED ([CoTicket] ASC, [NuTarifa] ASC),
    CONSTRAINT [FK_TICKET_AMADEUS_TARIFA_TICKET_AMADEUS] FOREIGN KEY ([CoTicket]) REFERENCES [dbo].[TICKET_AMADEUS] ([CoTicket])
);

