﻿CREATE TABLE [dbo].[MANACIONALIDADPROVEEDOR] (
    [IDubigeo]    CHAR (6) NOT NULL,
    [IDProveedor] CHAR (6) NOT NULL,
    [UserMod]     CHAR (4) NOT NULL,
    [FecMod]      DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MANACIONALIDADPROVEEDOR] PRIMARY KEY CLUSTERED ([IDubigeo] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_MANACIONALIDADPROVEEDOR] FOREIGN KEY ([IDubigeo]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_MANACIONALIDADPROVEEDOR2] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO
Create Trigger [dbo].[MANACIONALIDADPROVEEDOR_LOG_Ins]  
  On [dbo].[MANACIONALIDADPROVEEDOR] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDubigeo from Inserted)     
   set @RegistroPK2 = (select IDProveedor from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MANACIONALIDADPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MANACIONALIDADPROVEEDOR_LOG_Upd]  
  On [dbo].[MANACIONALIDADPROVEEDOR] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDubigeo from Inserted)    
   set @RegistroPK2 = (select IDProveedor from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MANACIONALIDADPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MANACIONALIDADPROVEEDOR_LOG_Del]  
  On [dbo].[MANACIONALIDADPROVEEDOR] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDubigeo from Deleted)    
   set @RegistroPK2 = (select IDProveedor from Deleted)    
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MANACIONALIDADPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
