﻿CREATE TABLE [dbo].[COTI_SERIE] (
    [IdCoti_Serie] SMALLINT      NOT NULL,
    [Descripcion]  VARCHAR (120) NOT NULL,
    [UserMod]      CHAR (4)      NOT NULL,
    [FecMod]       DATETIME      CONSTRAINT [DF__MASERIE__FecMod__2FAFEA50] DEFAULT (getdate()) NULL,
    CONSTRAINT [Pk_MASERIE_IdSerie] PRIMARY KEY CLUSTERED ([IdCoti_Serie] ASC)
);


GO

--103
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTI_SERIE_LOG_Upd]
  On [dbo].[COTI_SERIE] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdCoti_Serie from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_SERIE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--105
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTI_SERIE_LOG_Del]
  On [dbo].[COTI_SERIE] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IdCoti_Serie from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_SERIE',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--101
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTI_SERIE_LOG_Ins]
  On [dbo].[COTI_SERIE] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdCoti_Serie from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_SERIE',@UserMod,@RegistroPK1,'','','',@Accion
		End  
