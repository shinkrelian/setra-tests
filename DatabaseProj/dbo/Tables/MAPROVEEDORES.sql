﻿CREATE TABLE [dbo].[MAPROVEEDORES] (
    [IDProveedor]              CHAR (6)       NOT NULL,
    [IDTipoProv]               CHAR (3)       NOT NULL,
    [IDTipoOper]               CHAR (1)       NULL,
    [IDProveedor_Cadena]       CHAR (6)       NULL,
    [Persona]                  CHAR (1)       NOT NULL,
    [RazonSocial]              VARCHAR (100)  NOT NULL,
    [Nombre]                   VARCHAR (60)   NULL,
    [ApPaterno]                VARCHAR (60)   NULL,
    [ApMaterno]                VARCHAR (60)   NULL,
    [Domiciliado]              BIT            NOT NULL,
    [EsCadena]                 BIT            NOT NULL,
    [NombreCorto]              VARCHAR (100)  NOT NULL,
    [Sigla]                    CHAR (3)       NULL,
    [Direccion]                VARCHAR (150)  NOT NULL,
    [IDCiudad]                 CHAR (6)       NOT NULL,
    [IDIdentidad]              CHAR (3)       NOT NULL,
    [NumIdentidad]             VARCHAR (15)   NOT NULL,
    [Telefono1]                VARCHAR (50)   NULL,
    [Telefono2]                VARCHAR (50)   NULL,
    [TelefonoReservas1]        VARCHAR (50)   NULL,
    [TelefonoReservas2]        VARCHAR (50)   NULL,
    [Celular_Claro]            VARCHAR (20)   NULL,
    [Celular_Movistar]         VARCHAR (20)   NULL,
    [Celular_Nextel]           VARCHAR (20)   NULL,
    [NomContacto]              VARCHAR (80)   NULL,
    [Celular]                  VARCHAR (20)   NULL,
    [Fax]                      VARCHAR (50)   NULL,
    [FaxReservas1]             VARCHAR (50)   NULL,
    [FaxReservas2]             VARCHAR (50)   NULL,
    [Email1]                   VARCHAR (100)  NULL,
    [Email2]                   VARCHAR (100)  NULL,
    [Email3]                   VARCHAR (100)  NULL,
    [Email4]                   VARCHAR (100)  NULL,
    [TelefonoRecepcion1]       VARCHAR (50)   NULL,
    [TelefonoRecepcion2]       VARCHAR (50)   NULL,
    [FaxRecepcion]             VARCHAR (60)   NULL,
    [EmailRecepcion1]          VARCHAR (100)  NULL,
    [EmailRecepcion2]          VARCHAR (100)  NULL,
    [Web]                      VARCHAR (150)  NULL,
    [Adicionales]              TEXT           NULL,
    [PrePago]                  NUMERIC (8, 2) NULL,
    [PlazoDias]                TINYINT        NULL,
    [IDFormaPago]              CHAR (3)       NOT NULL,
    [TipoMuseo]                CHAR (3)       NULL,
    [Guia_Nacionalidad]        CHAR (6)       NOT NULL,
    [Guia_Especialidad]        CHAR (3)       NULL,
    [Guia_Estado]              CHAR (1)       NULL,
    [Guia_Ubica_CV]            VARCHAR (10)   NULL,
    [Guia_PoliticaMovilidad]   BIT            NOT NULL,
    [Guia_Asociacion]          CHAR (3)       NULL,
    [Guia_CarnetApotur]        BIT            NOT NULL,
    [Guia_CarnetGuiaOficial]   BIT            NOT NULL,
    [Guia_CertificadoPBIP]     BIT            NOT NULL,
    [Guia_CursoPBIP]           BIT            NOT NULL,
    [Guia_ExperienciaTC]       BIT            NOT NULL,
    [Guia_LugaresTC]           VARCHAR (150)  NULL,
    [Activo]                   CHAR (1)       CONSTRAINT [DF_MAPROVEEDORES_Activo] DEFAULT ('A') NOT NULL,
    [EsReservaEsp]             BIT            CONSTRAINT [DF_MAPROVEEDORES_ProveedorReservaEsp] DEFAULT ((0)) NOT NULL,
    [DiasCredito]              TINYINT        NULL,
    [EmailContactoAdmin]       VARCHAR (150)  NULL,
    [TelefonoContactoAdmin]    VARCHAR (50)   NULL,
    [DocPdfRutaPdf]            VARCHAR (200)  NULL,
    [Margen]                   NUMERIC (6, 2) NULL,
    [IDUsuarioProg]            CHAR (4)       NULL,
    [Disponible]               TEXT           NULL,
    [DocFoto]                  VARCHAR (MAX)  NULL,
    [DocCurriculo]             VARCHAR (MAX)  NULL,
    [RUC]                      VARCHAR (12)   NULL,
    [FecNacimiento]            SMALLDATETIME  NULL,
    [Movil1]                   VARCHAR (30)   NULL,
    [Movil2]                   VARCHAR (30)   NULL,
    [OtrosEspecialidad]        VARCHAR (110)  NULL,
    [OtrosNacionalidad]        VARCHAR (110)  NULL,
    [PreferFITS]               BIT            CONSTRAINT [DF__MAPROVEED__Prefe__7948ECA7] DEFAULT ((0)) NOT NULL,
    [PreferGroups]             BIT            CONSTRAINT [DF__MAPROVEED__Prefe__7A3D10E0] DEFAULT ((0)) NOT NULL,
    [PreferFamilias]           BIT            CONSTRAINT [DF__MAPROVEED__Prefe__7B313519] DEFAULT ((0)) NOT NULL,
    [Prefer3raEdad]            BIT            CONSTRAINT [DF__MAPROVEED__Prefe__7C255952] DEFAULT ((0)) NOT NULL,
    [PreferOtros]              TEXT           NULL,
    [EntrBriefing]             CHAR (2)       NULL,
    [EntrCultGeneral]          CHAR (2)       NULL,
    [EntrInfHistorica]         CHAR (2)       NULL,
    [EntrActitud]              CHAR (2)       NULL,
    [EntrOtros]                VARCHAR (150)  NULL,
    [MeetingPoint]             VARCHAR (MAX)  NULL,
    [UserMod]                  CHAR (4)       NOT NULL,
    [FecMod]                   DATETIME       CONSTRAINT [DF_MAPROVEEDORES_FecMod] DEFAULT (getdate()) NOT NULL,
    [ObservacionPolitica]      TEXT           NULL,
    [IDUsuarioProgTransp]      CHAR (4)       NULL,
    [IDProveedorInternacional] CHAR (6)       NULL,
    [IDUsuarioTrasladista]     CHAR (4)       CONSTRAINT [CK_MAPROVEEDORES_IDUsuarioTrasladista] DEFAULT (NULL) NULL,
    [SsMontoCredito]           NUMERIC (8, 2) DEFAULT (NULL) NULL,
    [CoMonedaCredito]          CHAR (3)       NULL,
    [SsComision]               DECIMAL (6, 2) NULL,
    [CoIATA]                   CHAR (2)       NULL,
    [TxLatitud]                VARCHAR (12)   NULL,
    [TxLongitud]               VARCHAR (12)   NULL,
    [SSTipoCambio]             DECIMAL (6, 3) NULL,
    [CoUbigeo_Oficina]         CHAR (6)       NULL,
    [TxTexto_OS]               TEXT           NULL,
    [FlDocumIdentidadUpdSAP]   BIT            DEFAULT ((0)) NOT NULL,
    [NumIdentidadAntSAP]       VARCHAR (15)   DEFAULT (NULL) NULL,
    [CardCodeSAP]              VARCHAR (15)   NULL,
    [FlExcluirSAP]             BIT            DEFAULT ((0)) NOT NULL,
    [FlUpdMasivoSAPInterfaz]   BIT            DEFAULT ((0)) NOT NULL,
    [CoTarjCredAcept]          CHAR (3)       NULL,
    [Usuario]                  VARCHAR (80)   NULL,
    [Password]                 VARCHAR (255)  NULL,
    [FlAccesoWeb]              BIT            DEFAULT ((0)) NOT NULL,
    [UsuarioLogeo_Hash]        VARCHAR (MAX)  NULL,
    [FechaHash]                DATETIME       NULL,
    CONSTRAINT [PK_MAPROVEEDORES] PRIMARY KEY CLUSTERED ([IDProveedor] ASC),
    FOREIGN KEY ([CoMonedaCredito]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_MAPROVEEDORES_MAFORMASPAGO] FOREIGN KEY ([IDFormaPago]) REFERENCES [dbo].[MAFORMASPAGO] ([IdFor]),
    CONSTRAINT [FK_MAPROVEEDORES_MAFORMASPAGO.CoTarjCredAcept] FOREIGN KEY ([CoTarjCredAcept]) REFERENCES [dbo].[MAFORMASPAGO] ([IdFor]),
    CONSTRAINT [FK_MAPROVEEDORES_MATIPOIDENT] FOREIGN KEY ([IDIdentidad]) REFERENCES [dbo].[MATIPOIDENT] ([IDIdentidad]),
    CONSTRAINT [FK_MAPROVEEDORES_MATIPOPROVEEDOR] FOREIGN KEY ([IDTipoProv]) REFERENCES [dbo].[MATIPOPROVEEDOR] ([IDTipoProv]),
    CONSTRAINT [FK_MAPROVEEDORES_MAUBIGEO] FOREIGN KEY ([IDCiudad]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_MAPROVEEDORES_MAUBIGEO_Oficina] FOREIGN KEY ([CoUbigeo_Oficina]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_MAPROVEEDORES_MAUBIGEO1] FOREIGN KEY ([Guia_Nacionalidad]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO
CREATE NONCLUSTERED INDEX [IX_MAPROVEEDORES_IDProveedorInternacional]
    ON [dbo].[MAPROVEEDORES]([IDProveedorInternacional] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MAPROVEEDORES_IDUsuarioTrasladista]
    ON [dbo].[MAPROVEEDORES]([IDUsuarioTrasladista] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MAPROVEEDORES]
    ON [dbo].[MAPROVEEDORES]([Domiciliado] ASC, [Activo] ASC)
    INCLUDE([IDProveedor], [IDTipoProv], [IDTipoOper], [Persona], [NombreCorto], [Direccion], [IDCiudad], [NumIdentidad], [Telefono1], [Telefono2], [Email1], [Web]);


GO
CREATE NONCLUSTERED INDEX [IX_MAPROVEEDORES_CoUbigeo_IDProveedor]
    ON [dbo].[MAPROVEEDORES]([CoUbigeo_Oficina] ASC, [IDProveedor] ASC)
    INCLUDE([NombreCorto], [IDFormaPago]);


GO
CREATE NONCLUSTERED INDEX [IX_MAPROVEEDORES_TipoProv]
    ON [dbo].[MAPROVEEDORES]([IDTipoProv] ASC)
    INCLUDE([IDProveedor]);


GO

--52
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MAPROVEEDORES

CREATE Trigger [dbo].[MAPROVEEDORES_LOG_Ins]
  On dbo.MAPROVEEDORES for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAPROVEEDORES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--57
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAPROVEEDORES_LOG_Del]
  On dbo.MAPROVEEDORES for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDProveedor from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAPROVEEDORES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--54
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAPROVEEDORES_LOG_Upd]
  On dbo.MAPROVEEDORES for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAPROVEEDORES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
