﻿CREATE TABLE [dbo].[MAATENCION_MAPROVEEDOR] (
    [IDAtencion]  TINYINT       NOT NULL,
    [IDProveedor] CHAR (6)      NOT NULL,
    [Dia1]        CHAR (1)      NOT NULL,
    [Dia2]        CHAR (1)      NOT NULL,
    [Desde1]      SMALLDATETIME NOT NULL,
    [Hasta1]      SMALLDATETIME NOT NULL,
    [Desde2]      SMALLDATETIME NOT NULL,
    [Hasta2]      SMALLDATETIME NOT NULL,
    [UserMod]     CHAR (4)      NOT NULL,
    [FecMod]      SMALLDATETIME CONSTRAINT [CK_MAATENCION_MAPROVEEDOR_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAATENCION_MAPROVEEDOR] PRIMARY KEY CLUSTERED ([IDAtencion] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_MAATENCION_MAPROVEEDOR_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO
Create Trigger [dbo].[MAATENCION_MAPROVEEDOR_LOG_Del]  
  On [dbo].[MAATENCION_MAPROVEEDOR] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDAtencion from Deleted)   
   set @RegistroPK2 = (select IDProveedor from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAATENCION_MAPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MAATENCION_MAPROVEEDOR_LOG_Upd]  
  On [dbo].[MAATENCION_MAPROVEEDOR] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDAtencion from Inserted)     
   set @RegistroPK2 = (select IDProveedor from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAATENCION_MAPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MAATENCION_MAPROVEEDOR_LOG_Ins]  
  On [dbo].[MAATENCION_MAPROVEEDOR] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDAtencion from Inserted)     
   set @RegistroPK2 = (select IDProveedor from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAATENCION_MAPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
