﻿CREATE TABLE [dbo].[MATIPOGASTO] (
    [IDgasto]     INT             IDENTITY (1, 1) NOT NULL,
    [Descripcion] VARCHAR (100)   NOT NULL,
    [CtaCon]      VARCHAR (12)    NULL,
    [CostoS]      NUMERIC (10, 2) NULL,
    [CostoD]      NUMERIC (10, 2) NULL,
    [grupo]       CHAR (8)        NULL,
    [stock]       NUMERIC (10)    NULL,
    [saldo]       NUMERIC (10)    NULL,
    [UserMod]     CHAR (4)        NULL,
    [FecMod]      DATETIME        NULL,
    CONSTRAINT [PK_MATIPOGASTO] PRIMARY KEY CLUSTERED ([IDgasto] ASC)
);


GO

--197
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOGASTO_LOG_Del]
  On [dbo].[MATIPOGASTO] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDgasto from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOGASTO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--193
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MATIPOGASTO

CREATE Trigger [dbo].[MATIPOGASTO_LOG_Ins]
  On [dbo].[MATIPOGASTO] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDgasto from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOGASTO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--195
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOGASTO_LOG_Upd]
  On [dbo].[MATIPOGASTO] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDgasto from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOGASTO',@UserMod,@RegistroPK1,'','','',@Accion
		End  
		
