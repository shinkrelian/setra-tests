﻿CREATE TABLE [dbo].[ORDENPAGO_DET] (
    [IDOrdPag_Det]      INT             NOT NULL,
    [IDOrdPag]          INT             NOT NULL,
    [IDReserva_Det]     INT             NULL,
    [IDServicio_Det]    INT             NULL,
    [NroPax]            SMALLINT        NOT NULL,
    [NroLiberados]      SMALLINT        NOT NULL,
    [Cantidad]          TINYINT         NOT NULL,
    [Noches]            TINYINT         NULL,
    [Total]             NUMERIC (12, 2) NULL,
    [UserMod]           CHAR (4)        NOT NULL,
    [FecMod]            DATETIME        DEFAULT (getdate()) NOT NULL,
    [IDTransporte]      INT             NULL,
    [TxDescripcion]     VARCHAR (MAX)   NULL,
    [SSDetraccion]      NUMERIC (12, 2) NULL,
    [Total_Final]       NUMERIC (12, 2) NULL,
    [SSDetraccion_USD]  NUMERIC (12, 2) NULL,
    [SSOtrosDescuentos] NUMERIC (12, 2) NULL,
    CONSTRAINT [PK_ORDENPAGO_DET] PRIMARY KEY NONCLUSTERED ([IDOrdPag_Det] ASC),
    CONSTRAINT [FK_ORDENPAGO_DET_COTIVUELOS] FOREIGN KEY ([IDTransporte]) REFERENCES [dbo].[COTIVUELOS] ([ID]),
    CONSTRAINT [FK_ORDENPAGO_DET_ORDENPAGO] FOREIGN KEY ([IDOrdPag]) REFERENCES [dbo].[ORDENPAGO] ([IDOrdPag]),
    CONSTRAINT [FK_ORDENPAGO_DET_RESERVAS_DET] FOREIGN KEY ([IDReserva_Det]) REFERENCES [dbo].[RESERVAS_DET] ([IDReserva_Det])
);


GO

--242
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ORDENPAGO_DET_LOG_Ins]
 On [dbo].[ORDENPAGO_DET] for Insert
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDOrdPag_Det from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ORDENPAGO_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO

--244
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ORDENPAGO_DET_LOG_Del]
 On [dbo].[ORDENPAGO_DET] for Delete
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from deleted) = 1
		Begin
			set @RegistroPK1 = (Select IDOrdPag_Det from Deleted)
			set @UserMod = (Select UserMod from Deleted)
			set @Accion = 'D'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ORDENPAGO_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO


--243
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ORDENPAGO_DET_LOG_Upd]
 On [dbo].[ORDENPAGO_DET] for Update
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDOrdPag_Det from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ORDENPAGO_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End
