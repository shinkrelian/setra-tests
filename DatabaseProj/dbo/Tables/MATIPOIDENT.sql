﻿CREATE TABLE [dbo].[MATIPOIDENT] (
    [IDIdentidad]    CHAR (3)     NOT NULL,
    [Descripcion]    VARCHAR (60) NOT NULL,
    [UserMod]        CHAR (4)     NULL,
    [FecMod]         DATETIME     NULL,
    [CodPeruRail]    CHAR (3)     NULL,
    [CoAmadeus]      CHAR (2)     NULL,
    [CoSAP]          SMALLINT     DEFAULT (NULL) NULL,
    [IDIdentidadSAP] CHAR (3)     NULL,
    CONSTRAINT [PK_MATIPOIDENT] PRIMARY KEY CLUSTERED ([IDIdentidad] ASC)
);


GO

--187
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MATIPOIDENT

CREATE Trigger [dbo].[MATIPOIDENT_LOG_Ins]
  On [dbo].[MATIPOIDENT] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIdentidad from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOIDENT',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--191
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOIDENT_LOG_Del]
  On [dbo].[MATIPOIDENT] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIdentidad from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOIDENT',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO
	

--189
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOIDENT_LOG_Upd]
  On [dbo].[MATIPOIDENT] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIdentidad from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOIDENT',@UserMod,@RegistroPK1,'','','',@Accion
		End  
