﻿CREATE TABLE [dbo].[RPT_VENTA_PAIS] (
    [NuAnio]      CHAR (4)        NOT NULL,
    [CoPais]      CHAR (6)        NOT NULL,
    [EneVentaMes] NUMERIC (13, 2) NULL,
    [FebVentaMes] NUMERIC (13, 2) NULL,
    [MarVentaMes] NUMERIC (13, 2) NULL,
    [AbrVentaMes] NUMERIC (13, 2) NULL,
    [MayVentaMes] NUMERIC (13, 2) NULL,
    [JunVentaMes] NUMERIC (13, 2) NULL,
    [JulVentaMes] NUMERIC (13, 2) NULL,
    [AgoVentaMes] NUMERIC (13, 2) NULL,
    [SetVentaMes] NUMERIC (13, 2) NULL,
    [OctVentaMes] NUMERIC (13, 2) NULL,
    [NovVentaMes] NUMERIC (13, 2) NULL,
    [DicVentaMes] NUMERIC (13, 2) NULL,
    [Total]       NUMERIC (13, 2) NULL,
    [UserMod]     CHAR (4)        NULL,
    [FecMod]      DATETIME        CONSTRAINT [DF_RPT_VENTA_PAIS_FecMod] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_RPT_VENTA_PAIS_1] PRIMARY KEY CLUSTERED ([NuAnio] ASC, [CoPais] ASC)
);

