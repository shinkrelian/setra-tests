﻿CREATE TABLE [dbo].[PEDIDO] (
    [NuPedInt]         INT           NOT NULL,
    [NuPedido]         CHAR (9)      NOT NULL,
    [CoCliente]        CHAR (6)      NOT NULL,
    [FePedido]         SMALLDATETIME NOT NULL,
    [CoEjeVta]         CHAR (4)      NOT NULL,
    [TxTitulo]         VARCHAR (100) NOT NULL,
    [CoEstado]         CHAR (2)      NOT NULL,
    [UserMod]          CHAR (4)      NOT NULL,
    [FecMod]           DATETIME      DEFAULT (getdate()) NOT NULL,
    [FePedidoEnviado]  SMALLDATETIME DEFAULT (NULL) NULL,
    [FePedidoAceptado] SMALLDATETIME DEFAULT (NULL) NULL,
    [FlCreadoDsdAddIn] BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_PEDIDO] PRIMARY KEY NONCLUSTERED ([NuPedInt] ASC)
);

