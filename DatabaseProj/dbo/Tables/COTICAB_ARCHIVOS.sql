﻿CREATE TABLE [dbo].[COTICAB_ARCHIVOS] (
    [IDArchivo]         INT           NOT NULL,
    [IDCAB]             INT           NOT NULL,
    [Nombre]            VARCHAR (200) NULL,
    [RutaOrigen]        VARCHAR (200) NULL,
    [RutaDestino]       VARCHAR (200) NULL,
    [CoTipo]            CHAR (3)      NULL,
    [FlCopiado]         BIT           NULL,
    [CoUsuarioCreacion] CHAR (4)      NULL,
    [UserMod]           CHAR (4)      NOT NULL,
    [FecMod]            DATETIME      CONSTRAINT [DF_COTICAB_ARCHIVOS_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTICAB_ARCHIVOS_1] PRIMARY KEY NONCLUSTERED ([IDCAB] ASC, [IDArchivo] ASC),
    CONSTRAINT [FK_COTICAB_ARCHIVOS_COTICAB] FOREIGN KEY ([IDCAB]) REFERENCES [dbo].[COTICAB] ([IDCAB])
);

