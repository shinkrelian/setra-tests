﻿CREATE TABLE [dbo].[MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP] (
    [CtaContable] VARCHAR (14) NOT NULL,
    [CoTipodoc]   CHAR (3)     NOT NULL,
    [CoTipoOC]    CHAR (3)     NOT NULL,
    [CoMoneda]    CHAR (3)     NOT NULL,
    [CoProveedor] CHAR (6)     NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF_MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP] PRIMARY KEY CLUSTERED ([CtaContable] ASC, [CoTipodoc] ASC, [CoTipoOC] ASC, [CoMoneda] ASC, [CoProveedor] ASC)
);

