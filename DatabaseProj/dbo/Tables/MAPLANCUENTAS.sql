﻿CREATE TABLE [dbo].[MAPLANCUENTAS] (
    [CtaContable]          VARCHAR (14)  NOT NULL,
    [Descripcion]          VARCHAR (150) NOT NULL,
    [UserMod]              CHAR (4)      NOT NULL,
    [FecMod]               DATETIME      DEFAULT (getdate()) NOT NULL,
    [CoCtroCosto]          CHAR (6)      NULL,
    [FlFacturacion]        BIT           DEFAULT ((0)) NOT NULL,
    [FlClientes]           BIT           DEFAULT ((0)) NOT NULL,
    [FlMigrado]            BIT           DEFAULT ((0)) NOT NULL,
    [TxDefinicion]         VARCHAR (255) NULL,
    [CtaContable_Anterior] VARCHAR (14)  NULL,
    [FlFondoFijo]          BIT           DEFAULT ((0)) NOT NULL,
    [FlExcluirCC]          BIT           DEFAULT ((0)) NOT NULL,
    [FlCompra]             BIT           NOT NULL,
    [FlActivo]             BIT           DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MAPLANCUENTAS] PRIMARY KEY NONCLUSTERED ([CtaContable] ASC)
);


GO

--254
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MAPLANCUENTAS
  
CREATE Trigger [dbo].[MAPLANCUENTAS_LOG_Ins]  
  On [dbo].[MAPLANCUENTAS] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CtaContable from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAPLANCUENTAS',@UserMod,@RegistroPK1,'','','',@Accion  
  End  

GO


  
--255
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
  
CREATE Trigger [dbo].[MAPLANCUENTAS_LOG_Upd]  
  On [dbo].[MAPLANCUENTAS] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CtaContable from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAPLANCUENTAS',@UserMod,@RegistroPK1,'','','',@Accion  
  End  

GO


--256
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  
CREATE Trigger [dbo].[MAPLANCUENTAS_LOG_Del]  
  On [dbo].[MAPLANCUENTAS] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Deleted) = 1   
  Begin   
   set @RegistroPK1 = (select CtaContable from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAPLANCUENTAS',@UserMod,@RegistroPK1,'','','',@Accion  
  End  

