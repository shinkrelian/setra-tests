﻿CREATE TABLE [dbo].[MAUBIGEO] (
    [IDubigeo]       CHAR (6)     NOT NULL,
    [Descripcion]    VARCHAR (60) NOT NULL,
    [Gentilicio]     VARCHAR (60) NULL,
    [TipoUbig]       VARCHAR (20) NOT NULL,
    [Codigo]         CHAR (3)     NULL,
    [Prove]          CHAR (1)     CONSTRAINT [DF_MAUBIGEO_Prove] DEFAULT ('N') NOT NULL,
    [NI]             CHAR (1)     CONSTRAINT [DF_MAUBIGEO_NI] DEFAULT ('N') NOT NULL,
    [DC]             CHAR (3)     NULL,
    [Llamar]         VARCHAR (15) NULL,
    [IDPais]         CHAR (6)     NULL,
    [EsOficina]      BIT          CONSTRAINT [DF__MAUBIGEO__EsOfic__61915EA7] DEFAULT ((0)) NOT NULL,
    [UserMod]        CHAR (4)     NULL,
    [FecMod]         DATETIME     NULL,
    [CodigoINC]      VARCHAR (3)  NULL,
    [CodigoPeruRail] VARCHAR (5)  NULL,
    [CoInternacPais] CHAR (2)     NULL,
    [CoMoneda]       CHAR (3)     NULL,
    [CoCounter]      CHAR (3)     NULL,
    [CoRegion]       CHAR (2)     NULL,
    [FlTourAdicAPT]  BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MAUBIGEO] PRIMARY KEY CLUSTERED ([IDubigeo] ASC),
    CONSTRAINT [FK_MAUBIGEO_MAMONEDAS] FOREIGN KEY ([CoMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda])
);


GO

--198
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MAUBIGEO

CREATE Trigger [dbo].[MAUBIGEO_LOG_Ins]
  On dbo.MAUBIGEO for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDubigeo from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUBIGEO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--204
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAUBIGEO_LOG_Del]
  On dbo.MAUBIGEO for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDubigeo from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUBIGEO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--201
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAUBIGEO_LOG_Upd]
  On dbo.MAUBIGEO for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDubigeo from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUBIGEO',@UserMod,@RegistroPK1,'','','',@Accion
		End  
