﻿CREATE TABLE [dbo].[MAOPCIONES] (
    [IDOpc]       CHAR (6)     NOT NULL,
    [Nombre]      VARCHAR (50) NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [EsMenu]      BIT          NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF_MAOPCIONES_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAOPCIONES] PRIMARY KEY CLUSTERED ([IDOpc] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MAOPCIONES_Nombre]
    ON [dbo].[MAOPCIONES]([Nombre] ASC);


GO


--106
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAOPCIONES_LOG_Del]
  On [dbo].[MAOPCIONES] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAOPCIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--102
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAOPCIONES

CREATE Trigger [dbo].[MAOPCIONES_LOG_Ins]
  On [dbo].[MAOPCIONES] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAOPCIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--104
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAOPCIONES_LOG_Upd]
  On [dbo].[MAOPCIONES] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAOPCIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
