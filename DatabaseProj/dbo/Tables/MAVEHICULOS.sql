﻿CREATE TABLE [dbo].[MAVEHICULOS] (
    [NuVehiculo]       SMALLINT     NOT NULL,
    [QtDe]             TINYINT      NOT NULL,
    [QtHasta]          TINYINT      NOT NULL,
    [NoVehiculoCitado] VARCHAR (50) NOT NULL,
    [QtCapacidad]      TINYINT      NOT NULL,
    [FlOtraCiudad]     BIT          NOT NULL,
    [Usermod]          CHAR (4)     NOT NULL,
    [Fecmod]           DATETIME     DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_MAVEHICULOS] PRIMARY KEY CLUSTERED ([NuVehiculo] ASC)
);


GO
Create Trigger [dbo].[MAVEHICULOS_LOG_Ins]  
  On [dbo].[MAVEHICULOS] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuVehiculo from Inserted)         
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAVEHICULOS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAVEHICULOS_LOG_Upd]  
  On [dbo].[MAVEHICULOS] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuVehiculo from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAVEHICULOS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAVEHICULOS_LOG_Del]  
  On [dbo].[MAVEHICULOS] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuVehiculo from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAVEHICULOS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
