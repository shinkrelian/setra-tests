﻿CREATE TABLE [dbo].[MAENTREVISTAPROVEEDOR] (
    [IDProveedor]    CHAR (6)      NOT NULL,
    [IDIdioma]       VARCHAR (12)  NOT NULL,
    [FlConocimiento] BIT           DEFAULT ((0)) NOT NULL,
    [FeEntrevista]   SMALLDATETIME NULL,
    [IDUserEntre]    CHAR (4)      NULL,
    [TxComentario]   VARCHAR (MAX) NULL,
    [Estado]         CHAR (1)      NULL,
    [UserMod]        CHAR (4)      NOT NULL,
    [FecMod]         SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAENTREVISTAPROVEEDOR] PRIMARY KEY CLUSTERED ([IDProveedor] ASC, [IDIdioma] ASC),
    CONSTRAINT [FK_MAENTREVISTAPROVEEDOR_idioma] FOREIGN KEY ([IDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_MAENTREVISTAPROVEEDOR_idproveedor] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_MAENTREVISTAPROVEEDOR_IDUser] FOREIGN KEY ([IDUserEntre]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario])
);

