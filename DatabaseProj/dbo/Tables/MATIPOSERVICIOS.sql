﻿CREATE TABLE [dbo].[MATIPOSERVICIOS] (
    [IDTipoServ]  CHAR (3)     NOT NULL,
    [Descripcion] VARCHAR (20) NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MATIPOSERVICIOS] PRIMARY KEY NONCLUSTERED ([IDTipoServ] ASC)
);


GO

--165
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOSERVICIOS_LOG_Upd]
  On [dbo].[MATIPOSERVICIOS] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDTipoServ from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOSERVICIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--163
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MATIPOSERVICIOS

CREATE Trigger [dbo].[MATIPOSERVICIOS_LOG_Ins]
  On [dbo].[MATIPOSERVICIOS] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDTipoServ from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOSERVICIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--167
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOSERVICIOS_LOG_Del]
  On [dbo].[MATIPOSERVICIOS] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDTipoServ from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOSERVICIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  
