﻿CREATE TABLE [dbo].[MANIVELUSUARIO] (
    [IDNivel]     CHAR (3)     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Activo]      CHAR (1)     CONSTRAINT [DF_MANIVELUSUARIO_Activo] DEFAULT ('A') NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF_MANIVELUSUARIO_FecMod] DEFAULT (getdate()) NOT NULL,
    [Orden]       TINYINT      NULL,
    CONSTRAINT [PK_MANIVELUSUARIO] PRIMARY KEY CLUSTERED ([IDNivel] ASC)
);


GO

--98
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MANIVELUSUARIO_LOG_Upd]
  On [dbo].[MANIVELUSUARIO] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDNivel from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MANIVELUSUARIO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--96
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MANIVELUSUARIO

CREATE Trigger [dbo].[MANIVELUSUARIO_LOG_Ins]
  On [dbo].[MANIVELUSUARIO] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDNivel from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MANIVELUSUARIO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--100
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MANIVELUSUARIO_LOG_Del]
  On [dbo].[MANIVELUSUARIO] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDNivel from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MANIVELUSUARIO',@UserMod,@RegistroPK1,'','','',@Accion
		End  
