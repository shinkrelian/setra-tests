﻿CREATE TABLE [dbo].[ACOMODOPAX_PROVEEDOR] (
    [IDReserva]     INT      NOT NULL,
    [IDPax]         INT      NOT NULL,
    [IDHabit]       TINYINT  NOT NULL,
    [CapacidadHab]  TINYINT  NOT NULL,
    [EsMatrimonial] BIT      CONSTRAINT [DF__ACOMODOPA__EsMat__33758E3C] DEFAULT ((0)) NOT NULL,
    [UserMod]       CHAR (4) NOT NULL,
    [FecMod]        DATETIME CONSTRAINT [DF__ACOMODOPA__FecMo__55FFB06A] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ACOMODOPAX_PROVEEDOR] PRIMARY KEY CLUSTERED ([IDReserva] ASC, [IDPax] ASC),
    CONSTRAINT [FK_ACOMODOPAX_PROVEEDOR_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax]),
    CONSTRAINT [FK_ACOMODOPAX_PROVEEDOR_RESERVAS] FOREIGN KEY ([IDReserva]) REFERENCES [dbo].[RESERVAS] ([IDReserva])
);


GO

--227
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ACOMODOPAX_PROVEEDOR_LOG_Ins]
 On [dbo].[ACOMODOPAX_PROVEEDOR] for Insert
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDReserva from Inserted)
			set @RegistroPK2 = (Select IDPax from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ACOMODOPAX_PROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO


--228
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ACOMODOPAX_PROVEEDOR_LOG_Upd]
 On [dbo].[ACOMODOPAX_PROVEEDOR] for Update
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDReserva from Inserted)
			set @RegistroPK2 = (Select IDPax from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ACOMODOPAX_PROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO

--229
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ACOMODOPAX_PROVEEDOR_LOG_Del]
 On [dbo].[ACOMODOPAX_PROVEEDOR] for Delete
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from deleted) = 1
		Begin
			set @RegistroPK1 = (Select IDReserva from Deleted)
			set @RegistroPK2 = (Select IDPax from Deleted)
			set @UserMod = (Select UserMod from Deleted)
			set @Accion = 'D'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ACOMODOPAX_PROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End
