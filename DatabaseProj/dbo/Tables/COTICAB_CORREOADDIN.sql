﻿CREATE TABLE [dbo].[COTICAB_CORREOADDIN] (
    [CoConvertionIndexOutlook] VARCHAR (MAX) NULL,
    [NuPedInt]                 INT           NULL,
    [IDCab]                    INT           NULL,
    [UserMod]                  CHAR (4)      NULL,
    [FecMod]                   SMALLDATETIME DEFAULT (getdate()) NOT NULL
);

