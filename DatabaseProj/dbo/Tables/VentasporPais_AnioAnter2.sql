﻿CREATE TABLE [dbo].[VentasporPais_AnioAnter2] (
    [Posicion]      BIGINT           NULL,
    [IDCiudad]      CHAR (6)         NULL,
    [Descripcion]   VARCHAR (60)     NULL,
    [CoRegion]      CHAR (2)         NULL,
    [SsImporte]     NUMERIC (38, 4)  NULL,
    [QtDias]        INT              NULL,
    [QtEdad_Prom]   INT              NOT NULL,
    [Cant_Clientes] INT              NULL,
    [ParamAnio]     CHAR (4)         NULL,
    [TicketFile]    NUMERIC (23, 15) NULL
);

