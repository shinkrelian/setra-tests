﻿CREATE TABLE [dbo].[CAMPOPROCESO] (
    [NuCampo]         INT           NOT NULL,
    [NoCampo]         VARCHAR (30)  NOT NULL,
    [NoTabla]         VARCHAR (30)  NOT NULL,
    [FlMultiLineas]   BIT           NOT NULL,
    [CoProceso]       CHAR (3)      NOT NULL,
    [UserMod]         CHAR (4)      NOT NULL,
    [FecMod]          SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [FlMultiColumnas] BIT           NOT NULL,
    CONSTRAINT [PK_CAMPOPROCESO] PRIMARY KEY NONCLUSTERED ([NuCampo] ASC),
    CONSTRAINT [FK_CAMPOPROCESO_PROCESO] FOREIGN KEY ([CoProceso]) REFERENCES [dbo].[PROCESO] ([CoProceso])
);


GO
Create Trigger [dbo].[CAMPOPROCESO_LOG_Ins]  
  On [dbo].[CAMPOPROCESO] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuCampo from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'CAMPOPROCESO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[CAMPOPROCESO_LOG_Upd]  
  On [dbo].[CAMPOPROCESO] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuCampo from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'CAMPOPROCESO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[CAMPOPROCESO_LOG_Del]  
  On [dbo].[CAMPOPROCESO] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuCampo from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'CAMPOPROCESO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
