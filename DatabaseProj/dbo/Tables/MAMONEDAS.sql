﻿CREATE TABLE [dbo].[MAMONEDAS] (
    [IDMoneda]    CHAR (3)     NOT NULL,
    [Descripcion] VARCHAR (20) NOT NULL,
    [Simbolo]     VARCHAR (5)  NOT NULL,
    [Activo]      CHAR (1)     CONSTRAINT [DF_MAMONEDAS_Activo] DEFAULT ('A') NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF_MAMONEDAS_FecMod] DEFAULT (getdate()) NOT NULL,
    [CoSAP]       CHAR (3)     NULL,
    CONSTRAINT [PK_MAMONEDAS] PRIMARY KEY NONCLUSTERED ([IDMoneda] ASC)
);


GO

--86
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- TABLA MAMONEDAS

CREATE Trigger [dbo].[MAMONEDAS_LOG_Ins]
  On [dbo].[MAMONEDAS] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDMoneda from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAMONEDAS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--88
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAMONEDAS_LOG_Del]
  On [dbo].[MAMONEDAS] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDMoneda from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAMONEDAS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--87
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAMONEDAS_LOG_Upd]
  On [dbo].[MAMONEDAS] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDMoneda from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAMONEDAS',@UserMod,@RegistroPK1,'','','',@Accion
		End  
