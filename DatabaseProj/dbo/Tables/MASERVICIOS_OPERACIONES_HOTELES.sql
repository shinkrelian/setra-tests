﻿CREATE TABLE [dbo].[MASERVICIOS_OPERACIONES_HOTELES] (
    [CoServicio]                 CHAR (8)      NOT NULL,
    [FlRecepcion]                BIT           NULL,
    [FlEstacionamiento]          BIT           NULL,
    [FlBoxDesayuno]              BIT           NULL,
    [FlRestaurantes]             BIT           NULL,
    [FlRestaurantesSoloDesayuno] BIT           NULL,
    [QtRestCantidad]             TINYINT       NULL,
    [QtRestCapacidad]            TINYINT       NULL,
    [QtTriples]                  TINYINT       NULL,
    [QtNumHabitaciones]          TINYINT       NULL,
    [CoTipoCama]                 CHAR (3)      NULL,
    [FlActivo]                   BIT           CONSTRAINT [DF_MASSERVICIOS_OPERACIONES_HOTELES_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]                    CHAR (4)      NOT NULL,
    [FecMod]                     DATETIME      CONSTRAINT [DF_MASSERVICIOS_OPERACIONES_HOTELES_FecMod] DEFAULT (getdate()) NOT NULL,
    [TxObservacion]              VARCHAR (250) NULL,
    CONSTRAINT [PK_MASSERVICIOS_OPERACIONES_HOTELES] PRIMARY KEY CLUSTERED ([CoServicio] ASC),
    CONSTRAINT [FK_MASSERVICIOS_OPERACIONES_HOTELES_MASERVICIOS] FOREIGN KEY ([CoServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio])
);

