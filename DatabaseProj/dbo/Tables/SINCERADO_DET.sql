﻿CREATE TABLE [dbo].[SINCERADO_DET] (
    [NuSincerado_Det] INT             NOT NULL,
    [NuSincerado]     INT             NOT NULL,
    [IDServicio_Det]  INT             NOT NULL,
    [QtPax]           TINYINT         NOT NULL,
    [QtPaxLiberados]  TINYINT         NULL,
    [SsCostoNeto]     DECIMAL (12, 4) NULL,
    [SsCostoIgv]      DECIMAL (12, 4) NULL,
    [SsCostoTotal]    DECIMAL (12, 4) NULL,
    [UserMod]         CHAR (4)        NOT NULL,
    [FecMod]          SMALLDATETIME   DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SINCERADO_DET] PRIMARY KEY CLUSTERED ([NuSincerado_Det] ASC),
    CONSTRAINT [FK_SINCERADO_DET_MASERVICIOS_DET] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det]),
    CONSTRAINT [FK_SINCERADO_DET_SINCERADO] FOREIGN KEY ([NuSincerado]) REFERENCES [dbo].[SINCERADO] ([NuSincerado])
);

