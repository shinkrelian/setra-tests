﻿CREATE TABLE [dbo].[SINCERADO_DET_PAX] (
    [NuSincerado_Det_Pax] INT           NOT NULL,
    [NuSincerado_Det]     INT           NOT NULL,
    [IDPax]               INT           NOT NULL,
    [UserMod]             CHAR (4)      NOT NULL,
    [FecMod]              SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SINCERADO_DET_PAX] PRIMARY KEY CLUSTERED ([NuSincerado_Det_Pax] ASC),
    CONSTRAINT [FK_SINCERADO_DET_PAX_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax]),
    CONSTRAINT [FK_SINCERADO_DET_PAX_SINCERADO_DET] FOREIGN KEY ([NuSincerado_Det]) REFERENCES [dbo].[SINCERADO_DET] ([NuSincerado_Det])
);

