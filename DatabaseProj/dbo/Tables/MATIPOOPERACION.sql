﻿CREATE TABLE [dbo].[MATIPOOPERACION] (
    [IdToc]        CHAR (3)        NOT NULL,
    [Descripcion]  VARCHAR (100)   NOT NULL,
    [CtaContIGV]   VARCHAR (14)    NOT NULL,
    [UserMod]      CHAR (4)        NULL,
    [FecMod]       DATETIME        NULL,
    [Orden]        TINYINT         NULL,
    [CoDestOper]   CHAR (3)        NULL,
    [TxDefinicion] VARCHAR (400)   NULL,
    [SsTasa]       NUMERIC (12, 4) DEFAULT ((0.00)) NOT NULL,
    CONSTRAINT [PK_MATIPOOPERACION] PRIMARY KEY CLUSTERED ([IdToc] ASC)
);


GO

--73
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MATIPOOPERACION

CREATE Trigger [dbo].[MATIPOOPERACION_LOG_Ins]
  On dbo.MATIPOOPERACION for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdToc from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOOPERACION',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--77
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MATIPOOPERACION_LOG_Del]
  On dbo.MATIPOOPERACION for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IdToc from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOOPERACION',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--75
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MATIPOOPERACION_LOG_Upd]
  On dbo.MATIPOOPERACION for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdToc from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOOPERACION',@UserMod,@RegistroPK1,'','','',@Accion
		End  
