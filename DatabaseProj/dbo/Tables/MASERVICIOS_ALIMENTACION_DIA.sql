﻿CREATE TABLE [dbo].[MASERVICIOS_ALIMENTACION_DIA] (
    [IDServicio]  CHAR (8) NOT NULL,
    [Dia]         TINYINT  NOT NULL,
    [Desayuno]    BIT      NOT NULL,
    [Lonche]      BIT      NOT NULL,
    [Almuerzo]    BIT      NOT NULL,
    [Cena]        BIT      NOT NULL,
    [IDUbigeoOri] CHAR (6) NOT NULL,
    [IDUbigeoDes] CHAR (6) NOT NULL,
    [Mig]         BIT      CONSTRAINT [DF__MASERVICIOS__Mig__2A7633E7] DEFAULT ((0)) NOT NULL,
    [UserMod]     CHAR (4) NOT NULL,
    [FecMod]      DATETIME CONSTRAINT [DF_MASERVICIOS_ALIMENTACION_DIA_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MASERVICIOS_ALIMENTACION_DIA] PRIMARY KEY CLUSTERED ([IDServicio] ASC, [Dia] ASC),
    CONSTRAINT [FK_MASERVICIOS_ALIMENTACION_DIA_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio])
);


GO

--133
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MASERVICIOS_ALIMENTACION_DIA

CREATE Trigger [dbo].[MASERVICIOS_ALIMENTACION_DIA_LOG_Ins]
  On [dbo].[MASERVICIOS_ALIMENTACION_DIA] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Inserted)
			set @RegistroPK2 = (select Dia from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_ALIMENTACION_DIA',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--134
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_ALIMENTACION_DIA_LOG_Upd]
  On [dbo].[MASERVICIOS_ALIMENTACION_DIA] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Inserted)
			set @RegistroPK2 = (select Dia from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_ALIMENTACION_DIA',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--135
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_ALIMENTACION_DIA_LOG_Del]
  On [dbo].[MASERVICIOS_ALIMENTACION_DIA] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Deleted)
			set @RegistroPK2 = (select Dia from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_ALIMENTACION_DIA',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End 
