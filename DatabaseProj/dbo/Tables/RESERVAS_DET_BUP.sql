﻿CREATE TABLE [dbo].[RESERVAS_DET_BUP] (
    [IDReserva]      INT           NOT NULL,
    [IDReserva_Det]  INT           NOT NULL,
    [Dia]            SMALLDATETIME NOT NULL,
    [FechaOut]       SMALLDATETIME NULL,
    [Noches]         TINYINT       NULL,
    [Servicio]       VARCHAR (MAX) NULL,
    [Cantidad]       TINYINT       NOT NULL,
    [NroPax]         SMALLINT      NOT NULL,
    [CodReservaProv] VARCHAR (50)  NULL,
    [UserMod]        CHAR (4)      NOT NULL,
    [FecMod]         DATETIME      CONSTRAINT [DF__RESERVAS___FecMo__38852773] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RESERVAS_DET_BUP] PRIMARY KEY NONCLUSTERED ([IDReserva] ASC, [IDReserva_Det] ASC)
);


GO

--58
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_DET_BUP_LOG_Del]
 On [Dbo].[RESERVAS_DET_BUP] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Deleted)
			set @RegistroPK2 = (select IDReserva_Det from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_BUP',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--51
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_DET_BUP_LOG_Ins]
 On [Dbo].[RESERVAS_DET_BUP] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Inserted)
			set @RegistroPK2 = (select IDReserva_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_BUP',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--50
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_DET_BUP_LOG_Upd]
 On [Dbo].[RESERVAS_DET_BUP] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Inserted)
			set @RegistroPK2 = (select IDReserva_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_BUP',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
