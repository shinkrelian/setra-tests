﻿CREATE TABLE [dbo].[OPERACIONES_DET_DETSERVICIOS_LOGPRESUPUESTOS] (
    [IDOperacion_Det]      INT             NOT NULL,
    [IDServicio_Det]       INT             NOT NULL,
    [IDServicio_Det_V_Cot] INT             NOT NULL,
    [NuLog]                TINYINT         NOT NULL,
    [FeReg]                DATETIME        DEFAULT (getdate()) NOT NULL,
    [QtPax]                SMALLINT        NOT NULL,
    [QtLiberados]          SMALLINT        NOT NULL,
    [SsTotal]              NUMERIC (12, 4) NOT NULL,
    [CoAccion]             CHAR (1)        NOT NULL,
    [FlAnulado]            BIT             DEFAULT ((0)) NOT NULL,
    [UserMod]              CHAR (4)        NOT NULL,
    [FecMod]               DATETIME        DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_OPERACIONES_DET_DETSERVICIOS_LOGPRESUPUESTOS] PRIMARY KEY NONCLUSTERED ([IDOperacion_Det] ASC, [IDServicio_Det] ASC, [IDServicio_Det_V_Cot] ASC, [NuLog] ASC)
);

