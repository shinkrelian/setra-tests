﻿CREATE TABLE [dbo].[MAFORMASPAGO] (
    [IdFor]         CHAR (3)     NOT NULL,
    [Descripcion]   VARCHAR (50) NOT NULL,
    [Tipo]          CHAR (3)     NOT NULL,
    [UserMod]       CHAR (4)     NULL,
    [FecMod]        DATETIME     NULL,
    [CoSUNAT_SAP]   CHAR (3)     DEFAULT (NULL) NULL,
    [CoSap]         SMALLINT     DEFAULT (NULL) NULL,
    [NuDiasCredito] SMALLINT     NULL,
    [FlTarjCred]    BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MAFORMASPAGO] PRIMARY KEY CLUSTERED ([IdFor] ASC)
);


GO

--20
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MAFORMASPAGO

CREATE Trigger [dbo].[MAFORMASPAGO_LOG_Ins]
  On dbo.MAFORMASPAGO for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdFor from Inserted)
			set @UserMod = (select UserMod from Inserted)			
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFORMASPAGO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--21
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAFORMASPAGO_LOG_Upd]
  On dbo.MAFORMASPAGO for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IdFor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFORMASPAGO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--22
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAFORMASPAGO_LOG_Del]
  On dbo.MAFORMASPAGO for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IdFor from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFORMASPAGO',@UserMod,@RegistroPK1,'','','',@Accion
		End  
