﻿CREATE TABLE [dbo].[MACENTROCOSTOS] (
    [CoCeCos]      CHAR (6)      NOT NULL,
    [Descripcion]  VARCHAR (150) NOT NULL,
    [CoCtaGasto]   CHAR (2)      NULL,
    [CoCtaIngreso] CHAR (2)      NULL,
    [FlActivo]     BIT           CONSTRAINT [DF_MACENTROCOSTOS_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]      CHAR (4)      NOT NULL,
    [FecMod]       DATETIME      CONSTRAINT [DF_MACENTROCOSTOS_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MACENTROCOSTOS] PRIMARY KEY CLUSTERED ([CoCeCos] ASC)
);

