﻿CREATE TABLE [dbo].[COTIDET_ACOMODOESPECIAL_DISTRIBUC] (
    [IDDet]       INT      NOT NULL,
    [CoCapacidad] CHAR (2) NOT NULL,
    [IDPax]       INT      NOT NULL,
    [IDHabit]     TINYINT  NOT NULL,
    [UserMod]     CHAR (4) NOT NULL,
    [FecMod]      DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTIDET_ACOMODOESPECIAL_DISTRIBUC] PRIMARY KEY NONCLUSTERED ([IDDet] ASC, [CoCapacidad] ASC, [IDPax] ASC),
    CONSTRAINT [FK_COTIDET_ACOMODOESPECIAL_DISTRIBUC_COTIDET_ACOMODOESPECIAL] FOREIGN KEY ([IDDet], [CoCapacidad]) REFERENCES [dbo].[COTIDET_ACOMODOESPECIAL] ([IDDet], [CoCapacidad])
);


GO
Create Trigger [dbo].[COTIDET_ACOMODOESPECIAL_DISTRIBUC_LOG_Ins]  
  On [dbo].[COTIDET_ACOMODOESPECIAL_DISTRIBUC] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)   
 Declare @RegistroPK3 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Inserted)  
   set @RegistroPK2 = (select CoCapacidad from Inserted)  
   set @RegistroPK3 = (select IDPax from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_ACOMODOESPECIAL_DISTRIBUC',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion  
  End 

GO
Create Trigger [dbo].[COTIDET_ACOMODOESPECIAL_DISTRIBUC_LOG_Upd]  
  On [dbo].[COTIDET_ACOMODOESPECIAL_DISTRIBUC] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)   
 Declare @RegistroPK3 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Inserted)  
   set @RegistroPK2 = (select CoCapacidad from Inserted)  
   set @RegistroPK3 = (select IDPax from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_ACOMODOESPECIAL_DISTRIBUC',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion  
  End 

GO
Create Trigger [dbo].[COTIDET_ACOMODOESPECIAL_DISTRIBUC_LOG_Del]  
  On [dbo].[COTIDET_ACOMODOESPECIAL_DISTRIBUC] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)
 Declare @RegistroPK3 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Deleted)     
   set @RegistroPK2 = (select CoCapacidad from Deleted)     
   set @RegistroPK3 = (select IDPax from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_ACOMODOESPECIAL_DISTRIBUC',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion  
  End 
