﻿CREATE TABLE [dbo].[SINCERADO_DET_BUP] (
    [NuSincerado_Det] INT             NOT NULL,
    [NuSincerado]     INT             NOT NULL,
    [IDServicio_Det]  INT             NOT NULL,
    [QtPax]           TINYINT         NOT NULL,
    [QtPaxLiberados]  TINYINT         NULL,
    [SsCostoNeto]     DECIMAL (12, 4) NULL,
    [SsCostoIgv]      DECIMAL (12, 4) NULL,
    [SsCostoTotal]    DECIMAL (12, 4) NULL,
    [UserMod]         CHAR (4)        NOT NULL,
    [FecMod]          SMALLDATETIME   NOT NULL
);

