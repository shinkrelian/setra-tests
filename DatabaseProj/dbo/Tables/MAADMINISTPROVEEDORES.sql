﻿CREATE TABLE [dbo].[MAADMINISTPROVEEDORES] (
    [IDAdminist]           CHAR (2)     NOT NULL,
    [IDProveedor]          CHAR (6)     NOT NULL,
    [Banco]                CHAR (3)     NOT NULL,
    [CtaCte]               VARCHAR (30) NULL,
    [IDMoneda]             CHAR (3)     NOT NULL,
    [SWIFT]                VARCHAR (30) NULL,
    [IVAN]                 VARCHAR (45) NULL,
    [NombresTitular]       VARCHAR (80) NULL,
    [ApellidosTitular]     VARCHAR (80) NULL,
    [ReferenciaAdminist]   TEXT         NULL,
    [CtaInter]             VARCHAR (30) NULL,
    [BancoInterm]          CHAR (3)     NOT NULL,
    [TipoCuenta]           CHAR (2)     NULL,
    [TitularBenef]         VARCHAR (90) NULL,
    [TipoCuentBenef]       CHAR (1)     NULL,
    [NroCuentaBenef]       VARCHAR (45) NULL,
    [DireccionBenef]       VARCHAR (70) NULL,
    [NombreBancoPag]       VARCHAR (80) NULL,
    [PECBancoPag]          VARCHAR (70) NULL,
    [DireccionBancoPag]    VARCHAR (80) NULL,
    [SWIFTBancoPag]        VARCHAR (10) NULL,
    [EsOpcBancoInte]       BIT          NULL,
    [NombreBancoInte]      VARCHAR (80) NULL,
    [PECBancoInte]         VARCHAR (90) NULL,
    [SWIFTBancoInte]       VARCHAR (10) NULL,
    [CuentBncPagBancoInte] VARCHAR (15) NULL,
    [BancoExtranjero]      VARCHAR (80) NULL,
    [BancoInterExtranjero] VARCHAR (80) NULL,
    [UserMod]              CHAR (4)     NOT NULL,
    [FecMod]               DATETIME     CONSTRAINT [DF__MAADMINIS__FecMo__5EDF0F2E] DEFAULT (getdate()) NOT NULL,
    [CoCBU]                VARCHAR (30) DEFAULT (NULL) NULL,
    CONSTRAINT [PK_MAADMINISTPROVEEDORES] PRIMARY KEY NONCLUSTERED ([IDAdminist] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_MAADMINISTPROVEEDORES_MABANCOS] FOREIGN KEY ([Banco]) REFERENCES [dbo].[MABANCOS] ([IDBanco]),
    CONSTRAINT [FK_MAADMINISTPROVEEDORES_MABANCOS1] FOREIGN KEY ([BancoInterm]) REFERENCES [dbo].[MABANCOS] ([IDBanco]),
    CONSTRAINT [FK_MAADMINISTPROVEEDORES_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_MAADMINISTPROVEEDORES_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO

--166
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAADMINISTPROVEEDORES_LOG_Upd]
  On [dbo].[MAADMINISTPROVEEDORES] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDAdminist from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAADMINISTPROVEEDORES',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--164
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAADMINISTPROVEEDORES

CREATE Trigger [dbo].[MAADMINISTPROVEEDORES_LOG_Ins]
  On [dbo].[MAADMINISTPROVEEDORES] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDAdminist from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAADMINISTPROVEEDORES',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--168
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAADMINISTPROVEEDORES_LOG_Del]
  On [dbo].[MAADMINISTPROVEEDORES] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDAdminist from Deleted)
			set @RegistroPK2 = (select IDProveedor from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAADMINISTPROVEEDORES',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
