﻿CREATE TABLE [dbo].[MACONTACTOSPROVEEDOR] (
    [IDContacto]        CHAR (3)      NOT NULL,
    [IDProveedor]       CHAR (6)      NOT NULL,
    [IDIdentidad]       CHAR (3)      NOT NULL,
    [NumIdentidad]      VARCHAR (15)  NOT NULL,
    [Titulo]            VARCHAR (5)   NOT NULL,
    [Nombres]           VARCHAR (80)  NOT NULL,
    [Apellidos]         VARCHAR (80)  NOT NULL,
    [Cargo]             VARCHAR (100) NOT NULL,
    [Utilidad]          CHAR (3)      NOT NULL,
    [Telefono]          VARCHAR (80)  NULL,
    [Fax]               VARCHAR (50)  NULL,
    [Celular]           VARCHAR (60)  NULL,
    [Email]             VARCHAR (100) NULL,
    [Anexo]             VARCHAR (10)  NULL,
    [FechaNacimiento]   SMALLDATETIME NULL,
    [UserMod]           CHAR (4)      NOT NULL,
    [FecMod]            DATETIME      CONSTRAINT [DF_MACONTACTOSPROVEEDOR_FecMod] DEFAULT (getdate()) NOT NULL,
    [Usuario]           VARCHAR (80)  NULL,
    [Password]          VARCHAR (255) NULL,
    [FlAccesoWeb]       BIT           DEFAULT ((0)) NOT NULL,
    [CoToken]           VARCHAR (MAX) DEFAULT (NULL) NULL,
    [UsuarioLogeo_Hash] VARCHAR (MAX) NULL,
    [FechaHash]         DATETIME      NULL,
    CONSTRAINT [PK_MACONTACTOSPROVEEDOR] PRIMARY KEY NONCLUSTERED ([IDContacto] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_MACONTACTOSPROVEEDOR_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO

--207
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACONTACTOSPROVEEDOR_LOG_Del]
  On [dbo].[MACONTACTOSPROVEEDOR] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDContacto from Deleted)
			set @RegistroPK2 = (select IDProveedor from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACONTACTOSPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--205
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACONTACTOSPROVEEDOR_LOG_Upd]
  On [dbo].[MACONTACTOSPROVEEDOR] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDContacto from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACONTACTOSPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--202
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MACONTACTOSPROVEEDOR

CREATE Trigger [dbo].[MACONTACTOSPROVEEDOR_LOG_Ins]
  On [dbo].[MACONTACTOSPROVEEDOR] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDContacto from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACONTACTOSPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
