﻿CREATE TABLE [dbo].[VentasReceptivoMensual] (
    [FecOutPeru]          VARCHAR (6)     NULL,
    [ImporteUSD]          NUMERIC (38, 2) NULL,
    [ImporteUSDAnioAnter] NUMERIC (12, 4) NOT NULL,
    [VarImporteUSD]       NUMERIC (38, 6) NULL,
    [CantPax]             INT             NULL,
    [CantPaxAnioAnter]    INT             NOT NULL,
    [VarCantPax]          INT             NULL,
    [CantFiles]           INT             NULL,
    [CantFilesAnioAnter]  INT             NOT NULL,
    [VarCantFiles]        INT             NULL,
    [SumaProy]            NUMERIC (38, 6) NULL,
    [Dias_Dif]            INT             NULL,
    [Edad_Prom]           INT             NOT NULL,
    [ParamAnio]           CHAR (4)        NULL
);

