﻿CREATE TABLE [dbo].[COTITRANSP_PAX] (
    [IDTransporte]     INT            NOT NULL,
    [IDPaxTransp]      SMALLINT       NOT NULL,
    [IDPax]            INT            NULL,
    [CodReservaTransp] VARCHAR (12)   NULL,
    [IDProveedorGuia]  CHAR (6)       NULL,
    [UserMod]          CHAR (4)       NOT NULL,
    [FecMod]           DATETIME       DEFAULT (getdate()) NOT NULL,
    [SsReembolso]      NUMERIC (8, 2) NULL,
    CONSTRAINT [PK_COTITRANSP_PAX] PRIMARY KEY NONCLUSTERED ([IDTransporte] ASC, [IDPaxTransp] ASC),
    CONSTRAINT [FK_COTITRANSP_PAX_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax]),
    CONSTRAINT [FK_COTITRANSP_PAX_COTIVUELOS] FOREIGN KEY ([IDTransporte]) REFERENCES [dbo].[COTIVUELOS] ([ID]),
    CONSTRAINT [FK_COTITRANSP_PAX_MAPROVEEDORES] FOREIGN KEY ([IDProveedorGuia]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO

Create Trigger dbo.COTITRANSP_PAX_Log_Del
On dbo.COTITRANSP_PAX for Delete
As
	Set NoCount On
	Declare @RegistroPK1 varchar(10)
	Declare @RegistroPK2 varchar(10)
	Declare @RegistroPK3 varchar(10)
	Declare @RegistroPK4 varchar(10)
	Declare @UserMod varchar(4)
	Declare @Accion char(1)
	
	if (select count(*) from Deleted) = 1
	Begin
		set @RegistroPK1 = (select IDTransporte from Deleted)
		set @RegistroPK2 = (select IDPaxTransp from Deleted)
		set @RegistroPK3 = (select IDPax from Deleted)
		set @RegistroPK4 = (select IDProveedorGuia from Deleted)
		set @UserMod = (select UserMod from Deleted)
		set @Accion = 'D'
		
		Execute [dbo].[LOGAUDITORIA_Ins] 'COTITRANSP_PAX',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,@RegistroPK4,@Accion
	End

GO
CREATE Trigger [dbo].[COTITRANSP_PAX_Log_Upd]
On [dbo].[COTITRANSP_PAX] for Update
As
	Set NoCount On
	Declare @RegistroPK1 varchar(10)
	Declare @RegistroPK2 varchar(10)
	Declare @RegistroPK3 varchar(10)
	Declare @RegistroPK4 varchar(10)
	Declare @UserMod varchar(4)
	Declare @Accion char(1)
	
	if (select count(*) from Inserted) = 1
	Begin
		set @RegistroPK1 = (select IDTransporte from Inserted)
		set @RegistroPK2 = (select IDPaxTransp from Inserted)
		set @RegistroPK3 = (select IDPax from Inserted)
		set @RegistroPK4 = (select IDProveedorGuia from Inserted)
		set @UserMod = (select UserMod from Inserted)
		set @Accion = 'U'
		
		Execute [dbo].[LOGAUDITORIA_Ins] 'COTITRANSP_PAX',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,@RegistroPK4,@Accion
	End

GO
Create Trigger dbo.COTITRANSP_PAX_Log_Ins
On dbo.COTITRANSP_PAX for Insert
As
	Set NoCount On
	Declare @RegistroPK1 varchar(10)
	Declare @RegistroPK2 varchar(10)
	Declare @RegistroPK3 varchar(10)
	Declare @RegistroPK4 varchar(10)
	Declare @UserMod varchar(4)
	Declare @Accion char(1)
	
	if (select count(*) from Inserted) = 1
	Begin
		set @RegistroPK1 = (select IDTransporte from Inserted)
		set @RegistroPK2 = (select IDPaxTransp from Inserted)
		set @RegistroPK3 = (select IDPax from Inserted)
		set @RegistroPK4 = (select IDProveedorGuia from Inserted)
		set @UserMod = (select UserMod from Inserted)
		set @Accion = 'I'
		
		Execute [dbo].[LOGAUDITORIA_Ins] 'COTITRANSP_PAX',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,@RegistroPK4,@Accion
	End
