﻿CREATE TABLE [dbo].[MACLIENTES] (
    [IDCliente]                 CHAR (6)       NOT NULL,
    [Persona]                   CHAR (1)       NOT NULL,
    [RazonSocial]               VARCHAR (80)   NULL,
    [RazonComercial]            VARCHAR (80)   NOT NULL,
    [Nombre]                    VARCHAR (60)   NULL,
    [ApPaterno]                 VARCHAR (60)   NULL,
    [ApMaterno]                 VARCHAR (60)   NULL,
    [Domiciliado]               BIT            NOT NULL,
    [Ciudad]                    VARCHAR (60)   NULL,
    [IDIdentidad]               CHAR (3)       NOT NULL,
    [NumIdentidad]              VARCHAR (15)   NULL,
    [Tipo]                      CHAR (2)       NOT NULL,
    [Direccion]                 VARCHAR (200)  NOT NULL,
    [Telefono1]                 VARCHAR (30)   NULL,
    [Telefono2]                 VARCHAR (30)   NULL,
    [Celular]                   VARCHAR (25)   NULL,
    [Fax]                       VARCHAR (25)   NULL,
    [Comision]                  DECIMAL (6, 2) NULL,
    [Notas]                     TEXT           NULL,
    [IDCiudad]                  CHAR (6)       NOT NULL,
    [IDvendedor]                CHAR (4)       NOT NULL,
    [Web]                       VARCHAR (100)  NULL,
    [CodPostal]                 VARCHAR (50)   NULL,
    [Credito]                   BIT            NOT NULL,
    [Activo]                    CHAR (1)       CONSTRAINT [DF_MACLIENTES_Activo] DEFAULT ('A') NOT NULL,
    [UserMod]                   CHAR (4)       NOT NULL,
    [FecMod]                    DATETIME       CONSTRAINT [DF_MACLIENTES_FecMod] DEFAULT (getdate()) NOT NULL,
    [MostrarEnReserva]          BIT            DEFAULT ((0)) NOT NULL,
    [Complejidad]               CHAR (1)       DEFAULT ('N') NOT NULL,
    [Frecuencia]                CHAR (1)       DEFAULT ('N') NOT NULL,
    [Solvencia]                 TINYINT        DEFAULT ((0)) NOT NULL,
    [CoStarSoft]                CHAR (11)      NULL,
    [NuCuentaComision]          VARCHAR (14)   NULL,
    [CoTipoVenta]               CHAR (2)       NOT NULL,
    [NuCuentaAnt]               CHAR (6)       NULL,
    [FlCtaCambiada]             BIT            DEFAULT ((0)) NOT NULL,
    [FlTop]                     BIT            NULL,
    [FePerfilUpdate]            DATETIME       NULL,
    [CoUserPerfilUpdate]        CHAR (4)       NULL,
    [txPerfilInformacion]       VARCHAR (MAX)  NULL,
    [txPerfilOperatividad]      VARCHAR (MAX)  NULL,
    [FlProveedor]               BIT            NULL,
    [NuPosicion]                TINYINT        NULL,
    [NuCuentaComision_Anterior] CHAR (6)       NULL,
    [CardCodeSAP]               VARCHAR (15)   NULL,
    [FlExcluirSAP]              BIT            DEFAULT ((0)) NOT NULL,
    [CoTipDocIdentPax]          CHAR (3)       NULL,
    [NuDocIdentPax]             VARCHAR (15)   NULL,
    CONSTRAINT [PK_MACLIENTES] PRIMARY KEY CLUSTERED ([IDCliente] ASC),
    CONSTRAINT [CK_MACLIENTES_Tipo] CHECK ([Tipo]='CL' OR [Tipo]='IT' OR [Tipo]='WB' OR [Tipo]='CO' OR [Tipo]='ND'),
    CONSTRAINT [FK_MACLIENTES_MATIPOIDENT] FOREIGN KEY ([IDIdentidad]) REFERENCES [dbo].[MATIPOIDENT] ([IDIdentidad]),
    CONSTRAINT [FK_MACLIENTES_MATIPOIDENT_Pax] FOREIGN KEY ([CoTipDocIdentPax]) REFERENCES [dbo].[MATIPOIDENT] ([IDIdentidad]),
    CONSTRAINT [FK_MACLIENTES_MAUSUARIOS] FOREIGN KEY ([IDvendedor]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario])
);


GO
CREATE NONCLUSTERED INDEX [IX_RAZONCOMERCIAL]
    ON [dbo].[MACLIENTES]([RazonComercial] ASC)
    INCLUDE([IDCliente]);


GO
CREATE NONCLUSTERED INDEX [IX_IDCLIENTE_RAZONCOMERCIAL_FLTOP]
    ON [dbo].[MACLIENTES]([Activo] ASC)
    INCLUDE([IDCliente], [RazonComercial], [FlTop]);


GO
CREATE NONCLUSTERED INDEX [IX_DocIdentPax]
    ON [dbo].[MACLIENTES]([CoTipDocIdentPax] ASC, [NuDocIdentPax] ASC)
    INCLUDE([IDCliente]);


GO

--181
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MACLIENTES

CREATE Trigger [dbo].[MACLIENTES_LOG_Ins]
  On [dbo].[MACLIENTES] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCliente from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACLIENTES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--185
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACLIENTES_LOG_Del]
  On [dbo].[MACLIENTES] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCliente from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACLIENTES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--183
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACLIENTES_LOG_Upd]
  On [dbo].[MACLIENTES] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCliente from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACLIENTES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
