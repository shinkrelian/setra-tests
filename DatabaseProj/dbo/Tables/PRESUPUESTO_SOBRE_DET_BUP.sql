﻿CREATE TABLE [dbo].[PRESUPUESTO_SOBRE_DET_BUP] (
    [NuDetPSo]             TINYINT         NOT NULL,
    [NuPreSob]             INT             NOT NULL,
    [CoTipPSo]             CHAR (2)        NOT NULL,
    [FeDetPSo]             SMALLDATETIME   NOT NULL,
    [IDServicio_Det]       INT             NULL,
    [NuSincerado_Det]      INT             NULL,
    [TxServicio]           VARCHAR (250)   NOT NULL,
    [QtPax]                SMALLINT        NOT NULL,
    [SsPreUni]             NUMERIC (12, 4) NOT NULL,
    [SsTotal]              NUMERIC (12, 2) NOT NULL,
    [SsTotSus]             NUMERIC (12, 2) NOT NULL,
    [SsTotDev]             NUMERIC (12, 2) NOT NULL,
    [UserMod]              CHAR (4)        NOT NULL,
    [FecMod]               DATETIME        NOT NULL,
    [IDOperacion_Det]      INT             NULL,
    [CoPago]               CHAR (3)        NOT NULL,
    [CoPrvPrg]             CHAR (6)        NULL,
    [IDMoneda]             CHAR (3)        NULL,
    [IDPax]                INT             NULL,
    [CoSap]                VARCHAR (15)    NULL,
    [IDServicio_Det_V_Cot] INT             NULL
);

