﻿CREATE TABLE [dbo].[MABANCOS] (
    [IDBanco]     CHAR (3)      NOT NULL,
    [Descripcion] VARCHAR (60)  NOT NULL,
    [Moneda]      CHAR (1)      NOT NULL,
    [NroCuenta]   VARCHAR (20)  NOT NULL,
    [Sigla]       VARCHAR (8)   NOT NULL,
    [CtaInter]    VARCHAR (30)  NULL,
    [CtaCon]      VARCHAR (20)  NULL,
    [Direccion]   VARCHAR (100) NULL,
    [SWIFT]       VARCHAR (20)  NULL,
    [UserMod]     CHAR (4)      NULL,
    [FecMod]      DATETIME      NULL,
    CONSTRAINT [PK_MABANCOS] PRIMARY KEY CLUSTERED ([IDBanco] ASC)
);


GO

--170
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MABANCOS

CREATE Trigger [dbo].[MABANCOS_LOG_Ins]
  On [dbo].[MABANCOS] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDBanco from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
				
			Execute [dbo].[LOGAUDITORIA_Ins] 'MABANCOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--172
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MABANCOS_LOG_Upd]
  On [dbo].[MABANCOS] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDBanco from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MABANCOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--174
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MABANCOS_LOG_Del]
  On [dbo].[MABANCOS] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDBanco from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MABANCOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  
