﻿CREATE TABLE [dbo].[MANIVELOPC] (
    [IDOpc]      CHAR (6) NOT NULL,
    [IDNivel]    CHAR (3) NOT NULL,
    [Consultar]  BIT      NOT NULL,
    [Grabar]     BIT      NOT NULL,
    [Actualizar] BIT      NOT NULL,
    [Eliminar]   BIT      NOT NULL,
    [UserMod]    CHAR (4) NOT NULL,
    [FecMod]     DATETIME CONSTRAINT [DF_MANIVELOPC_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MANIVELOPC] PRIMARY KEY CLUSTERED ([IDOpc] ASC, [IDNivel] ASC),
    CONSTRAINT [FK_MANIVELOPC_MANIVELUSUARIO] FOREIGN KEY ([IDNivel]) REFERENCES [dbo].[MANIVELUSUARIO] ([IDNivel]),
    CONSTRAINT [FK_MANIVELOPC_MAOPCIONES] FOREIGN KEY ([IDOpc]) REFERENCES [dbo].[MAOPCIONES] ([IDOpc])
);


GO

--92
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MANIVELOPC_LOG_Upd]
  On [dbo].[MANIVELOPC] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Inserted)
			set @RegistroPK2 = (select IDNivel from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MANIVELOPC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--94
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MANIVELOPC_LOG_Del]
  On [dbo].[MANIVELOPC] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Deleted)
			set @RegistroPK2 = (select IDNivel from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MANIVELOPC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--90
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MANIVELOPC

CREATE Trigger [dbo].[MANIVELOPC_LOG_Ins]
  On [dbo].[MANIVELOPC] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Inserted)
			set @RegistroPK2 = (select IDNivel from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MANIVELOPC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
