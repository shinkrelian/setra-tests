﻿CREATE TABLE [dbo].[RESERVAS_MENSAJES] (
    [ConversationID] VARCHAR (255) NOT NULL,
    [IDReserva]      INT           NOT NULL,
    [Asunto]         VARCHAR (MAX) NULL,
    [UserMod]        CHAR (4)      NOT NULL,
    [FecMod]         DATETIME      CONSTRAINT [DF__RESERVAS___FecMo__4E89772B] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RESERVAS_MENSAJES] PRIMARY KEY CLUSTERED ([ConversationID] ASC, [IDReserva] ASC)
);


GO

--37
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[RESERVAS_MENSAJES_LOG_Del]
 On [Dbo].[RESERVAS_MENSAJES] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select ConversationID from Deleted)
			set @RegistroPK2 = (select IDReserva from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_MENSAJESTELEF',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--35
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[RESERVAS_MENSAJES_LOG_Upd]
 On [Dbo].[RESERVAS_MENSAJES] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select ConversationID from Inserted)
			set @RegistroPK2 = (select IDReserva from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_MENSAJES',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--33
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_MENSAJES_LOG_Ins]
 On [Dbo].[RESERVAS_MENSAJES] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select ConversationID from Inserted)
			set @RegistroPK2 = (select IDReserva from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_MENSAJES',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
