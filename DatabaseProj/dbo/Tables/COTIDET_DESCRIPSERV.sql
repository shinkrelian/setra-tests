﻿CREATE TABLE [dbo].[COTIDET_DESCRIPSERV] (
    [IDDet]             INT           NOT NULL,
    [IDIdioma]          VARCHAR (12)  NOT NULL,
    [DescCorta]         VARCHAR (100) NULL,
    [DescLarga]         TEXT          NULL,
    [UserMod]           CHAR (4)      NOT NULL,
    [FechaMod]          DATETIME      DEFAULT (getdate()) NOT NULL,
    [NoHotel]           VARCHAR (100) NULL,
    [TxWebHotel]        VARCHAR (150) NULL,
    [TxDireccHotel]     VARCHAR (150) NULL,
    [TxTelfHotel1]      VARCHAR (50)  NULL,
    [TxTelfHotel2]      VARCHAR (50)  NULL,
    [FeHoraChkInHotel]  SMALLDATETIME NULL,
    [FeHoraChkOutHotel] SMALLDATETIME NULL,
    [CoTipoDesaHotel]   CHAR (2)      NULL,
    [CoCiudadHotel]     CHAR (6)      NULL,
    CONSTRAINT [PK_COTIDET_DESCRIPSERV] PRIMARY KEY NONCLUSTERED ([IDDet] ASC, [IDIdioma] ASC),
    CONSTRAINT [FK_COTIDET_DESCRIPSERV_COTIDET] FOREIGN KEY ([IDDet]) REFERENCES [dbo].[COTIDET] ([IDDET])
);


GO

--127
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_DESCRIPSERV_LOG_Upd]
  On [dbo].[COTIDET_DESCRIPSERV] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDet from Inserted)
			set @RegistroPK2 = (select IDIdioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_DESCRIPSERV',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--125
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIDET_DESCRIPSERV_LOG_Ins]
  On [dbo].[COTIDET_DESCRIPSERV] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDet from Inserted)
			set @RegistroPK2 = (select IDIdioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_DESCRIPSERV',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--129
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_DESCRIPSERV_LOG_Del]
  On [dbo].[COTIDET_DESCRIPSERV] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDet from Deleted)
			set @RegistroPK2 = (select IDIdioma from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_DESCRIPSERV',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
