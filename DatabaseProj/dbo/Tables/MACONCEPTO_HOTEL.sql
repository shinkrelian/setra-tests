﻿CREATE TABLE [dbo].[MACONCEPTO_HOTEL] (
    [CoConcepto]    TINYINT       NOT NULL,
    [TxDescripcion] VARCHAR (150) NULL,
    [CoTipo]        CHAR (3)      NULL,
    [FlActivo]      BIT           CONSTRAINT [DF_MACONCEPTO_HOTEL_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]       CHAR (4)      NOT NULL,
    [FecMod]        DATETIME      CONSTRAINT [DF_MACONCEPTO_HOTEL_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MACONCEPTO_HOTEL] PRIMARY KEY CLUSTERED ([CoConcepto] ASC)
);

