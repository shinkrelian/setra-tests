﻿CREATE TABLE [dbo].[MATIPOHONORARIOS] (
    [CoTipoHon]     CHAR (3)      NOT NULL,
    [NoDescripcion] VARCHAR (25)  NOT NULL,
    [UserMod]       CHAR (4)      NOT NULL,
    [FecMod]        SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MATIPOHONORARIOS] PRIMARY KEY CLUSTERED ([CoTipoHon] ASC)
);

