﻿CREATE TABLE [dbo].[PROVEEDORCOUNTER_USUARIO] (
    [CoProveedor] CHAR (6)     NOT NULL,
    [CoUsuario]   VARCHAR (20) NOT NULL,
    [NoValor]     VARCHAR (50) NOT NULL,
    [flActivo]    BIT          NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     NOT NULL,
    CONSTRAINT [PK_PROVEEDORCOUNTERUSUARIO] PRIMARY KEY NONCLUSTERED ([CoProveedor] ASC, [CoUsuario] ASC)
);

