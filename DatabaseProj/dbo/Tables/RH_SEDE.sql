﻿CREATE TABLE [dbo].[RH_SEDE] (
    [CoSede]      TINYINT       NOT NULL,
    [TxSede]      VARCHAR (150) NOT NULL,
    [CoUbigeo]    CHAR (6)      NOT NULL,
    [TxDireccion] VARCHAR (250) NOT NULL,
    [FlActivo]    BIT           CONSTRAINT [DF_RH_SEDE_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]     CHAR (4)      NOT NULL,
    [FecMod]      DATETIME      CONSTRAINT [DF_RH_SEDE_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RH_SEDE] PRIMARY KEY CLUSTERED ([CoSede] ASC)
);

