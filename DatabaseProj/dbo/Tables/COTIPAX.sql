﻿CREATE TABLE [dbo].[COTIPAX] (
    [IDPax]                 INT            IDENTITY (1, 1) NOT NULL,
    [IDCab]                 INT            NOT NULL,
    [IDIdentidad]           CHAR (3)       NOT NULL,
    [NumIdentidad]          VARCHAR (25)   NULL,
    [Nombres]               VARCHAR (50)   NOT NULL,
    [Apellidos]             VARCHAR (50)   NOT NULL,
    [Titulo]                VARCHAR (5)    NOT NULL,
    [FecNacimiento]         SMALLDATETIME  NULL,
    [IDNacionalidad]        CHAR (6)       NOT NULL,
    [Peso]                  NUMERIC (5, 2) NULL,
    [ObservEspecial]        VARCHAR (200)  NULL,
    [Total]                 NUMERIC (8, 2) NOT NULL,
    [SSTotal]               NUMERIC (8, 2) NOT NULL,
    [STTotal]               NUMERIC (8, 2) NOT NULL,
    [Actualizar]            BIT            CONSTRAINT [DF_COTIPAX_Actualizar] DEFAULT ((0)) NOT NULL,
    [UserMod]               CHAR (4)       NOT NULL,
    [FecMod]                DATETIME       CONSTRAINT [DF__COTIPAX__FecMod__6CE315C2] DEFAULT (getdate()) NOT NULL,
    [Orden]                 SMALLINT       NOT NULL,
    [Residente]             BIT            DEFAULT ((0)) NOT NULL,
    [IDPaisResidencia]      CHAR (6)       NOT NULL,
    [FecIngresoPais]        SMALLDATETIME  NULL,
    [PassValidado]          BIT            DEFAULT ((0)) NOT NULL,
    [TxTituloAcademico]     VARCHAR (5)    NOT NULL,
    [FlTC]                  BIT            NOT NULL,
    [FlTripleNA]            BIT            DEFAULT ((0)) NOT NULL,
    [FlUpdExcel]            BIT            DEFAULT ((0)) NOT NULL,
    [NoRutaArchivoExcelPax] VARCHAR (250)  NULL,
    [CoUserExcelPax]        CHAR (4)       NULL,
    [NoPCExcelPax]          VARCHAR (80)   NULL,
    [FlNoShow]              BIT            DEFAULT ((0)) NOT NULL,
    [FlEntMP]               BIT            DEFAULT ((0)) NOT NULL,
    [FlEntWP]               BIT            DEFAULT ((0)) NOT NULL,
    [FlAutorizarMPWP]       BIT            DEFAULT ((0)) NOT NULL,
    [FlAdultoINC]           BIT            DEFAULT ((0)) NOT NULL,
    [NumberAPT]             VARCHAR (20)   NULL,
    [FlActualizadoxExtrNet] BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_COTIPAX] PRIMARY KEY NONCLUSTERED ([IDPax] ASC),
    CONSTRAINT [FK_COTIPAX_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_COTIPAX_MATIPOIDENT] FOREIGN KEY ([IDIdentidad]) REFERENCES [dbo].[MATIPOIDENT] ([IDIdentidad]),
    CONSTRAINT [FK_COTIPAX_MAUBIGEO] FOREIGN KEY ([IDNacionalidad]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDCab]
    ON [dbo].[COTIPAX]([IDCab] ASC);


GO

--16
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIPAX_LOG_Del]
  On dbo.COTIPAX for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDPax from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIPAX',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--10
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIPAX_LOG_Ins]
  On dbo.COTIPAX for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIPAX',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO
CREATE Trigger [dbo].[COTIPAX_LOG_Upd]
  On [dbo].[COTIPAX] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIPAX',@UserMod,@RegistroPK1,'','','',@Accion
		End  

