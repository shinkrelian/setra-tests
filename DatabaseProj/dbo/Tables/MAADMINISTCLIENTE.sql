﻿CREATE TABLE [dbo].[MAADMINISTCLIENTE] (
    [IDAdminist]         CHAR (2)     NOT NULL,
    [IDCliente]          CHAR (6)     NOT NULL,
    [Banco]              VARCHAR (30) NULL,
    [CtaCte]             VARCHAR (30) NULL,
    [IDMoneda]           CHAR (3)     NOT NULL,
    [SWIFT]              VARCHAR (30) NULL,
    [IVAN]               VARCHAR (30) NULL,
    [NombresTitular]     VARCHAR (80) NULL,
    [ApellidosTitular]   VARCHAR (80) NULL,
    [ReferenciaAdminist] TEXT         NULL,
    [CtaInter]           VARCHAR (30) NULL,
    [BancoInterm]        VARCHAR (30) NULL,
    [UserMod]            CHAR (4)     NOT NULL,
    [FecMod]             DATETIME     CONSTRAINT [DF__MAADMINIS__FecMo__74444068] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAADMINISTCLIENTE_1] PRIMARY KEY NONCLUSTERED ([IDAdminist] ASC, [IDCliente] ASC),
    CONSTRAINT [FK_MAADMINISTCLIENTE_MACLIENTES] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[MACLIENTES] ([IDCliente]),
    CONSTRAINT [FK_MAADMINISTCLIENTE_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda])
);


GO

--157
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAADMINISTCLIENTE

CREATE Trigger [dbo].[MAADMINISTCLIENTE_LOG_Ins]
  On [dbo].[MAADMINISTCLIENTE] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDAdminist from Inserted)
			set @RegistroPK2 = (select IDCliente from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAADMINISTCLIENTE',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--162
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAADMINISTCLIENTE_LOG_Del]
  On [dbo].[MAADMINISTCLIENTE] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDAdminist from Deleted)
			set @RegistroPK2 = (select IDCliente from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAADMINISTCLIENTE',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--159
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAADMINISTCLIENTE_LOG_Upd]
  On [dbo].[MAADMINISTCLIENTE] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDAdminist from Inserted)
			set @RegistroPK2 = (select IDCliente from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAADMINISTCLIENTE',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
