﻿CREATE TABLE [dbo].[OPERACIONES_DET] (
    [IDOperacion_Det]              INT             NOT NULL,
    [IDOperacion]                  INT             NOT NULL,
    [IDReserva_Det]                INT             NULL,
    [IDVoucher]                    INT             CONSTRAINT [DF__OPERACION__IDVou__12B48446] DEFAULT ((0)) NOT NULL,
    [Item]                         NUMERIC (6, 3)  NOT NULL,
    [Dia]                          SMALLDATETIME   NOT NULL,
    [DiaNombre]                    VARCHAR (10)    NOT NULL,
    [FechaOut]                     SMALLDATETIME   NULL,
    [Noches]                       TINYINT         NULL,
    [IDubigeo]                     CHAR (6)        NOT NULL,
    [IDServicio]                   CHAR (8)        NOT NULL,
    [IDServicio_Det]               INT             NOT NULL,
    [Servicio]                     VARCHAR (MAX)   NOT NULL,
    [IDIdioma]                     VARCHAR (12)    NOT NULL,
    [Especial]                     BIT             NOT NULL,
    [MotivoEspecial]               VARCHAR (200)   NULL,
    [RutaDocSustento]              VARCHAR (200)   NULL,
    [Desayuno]                     BIT             NOT NULL,
    [Lonche]                       BIT             NOT NULL,
    [Almuerzo]                     BIT             NOT NULL,
    [Cena]                         BIT             NOT NULL,
    [Transfer]                     BIT             NOT NULL,
    [IDDetTransferOri]             INT             NULL,
    [IDDetTransferDes]             INT             NULL,
    [TipoTransporte]               CHAR (1)        NULL,
    [IDUbigeoOri]                  CHAR (6)        NOT NULL,
    [IDUbigeoDes]                  CHAR (6)        NOT NULL,
    [NroPax]                       SMALLINT        NOT NULL,
    [NroLiberados]                 SMALLINT        NULL,
    [Tipo_Lib]                     CHAR (1)        NULL,
    [Cantidad]                     TINYINT         NOT NULL,
    [CantidadAPagar]               TINYINT         NOT NULL,
    [CostoReal]                    NUMERIC (12, 4) NOT NULL,
    [CostoRealAnt]                 NUMERIC (12, 4) NOT NULL,
    [CostoLiberado]                NUMERIC (12, 4) NOT NULL,
    [Margen]                       NUMERIC (12, 4) NOT NULL,
    [MargenAplicado]               NUMERIC (6, 2)  NOT NULL,
    [MargenAplicadoAnt]            NUMERIC (6, 2)  NULL,
    [MargenLiberado]               NUMERIC (12, 4) NOT NULL,
    [Total]                        NUMERIC (8, 2)  NOT NULL,
    [TotalOrig]                    NUMERIC (12, 4) NOT NULL,
    [CostoRealImpto]               NUMERIC (12, 4) NOT NULL,
    [CostoLiberadoImpto]           NUMERIC (12, 4) NOT NULL,
    [MargenImpto]                  NUMERIC (12, 4) NOT NULL,
    [MargenLiberadoImpto]          NUMERIC (12, 4) NOT NULL,
    [TotImpto]                     NUMERIC (12, 4) NOT NULL,
    [NetoHab]                      NUMERIC (12, 4) NOT NULL,
    [IgvHab]                       NUMERIC (12, 4) NOT NULL,
    [TotalHab]                     NUMERIC (12, 4) NOT NULL,
    [NetoGen]                      NUMERIC (12, 4) NOT NULL,
    [IgvGen]                       NUMERIC (12, 4) NOT NULL,
    [TotalGen]                     NUMERIC (12, 4) NOT NULL,
    [FechaRecordatorio]            SMALLDATETIME   NULL,
    [Recordatorio]                 VARCHAR (MAX)   NULL,
    [ObservVoucher]                VARCHAR (MAX)   NULL,
    [ObservBiblia]                 VARCHAR (MAX)   NULL,
    [ObservInterno]                VARCHAR (MAX)   NULL,
    [VerObserVoucher]              BIT             CONSTRAINT [DF__OPERACION__VerOb__292D09F3] DEFAULT ((0)) NOT NULL,
    [VerObserBiblia]               BIT             CONSTRAINT [DF__OPERACION__VerOb__2838E5BA] DEFAULT ((1)) NOT NULL,
    [UserMod]                      CHAR (4)        NOT NULL,
    [FecMod]                       DATETIME        CONSTRAINT [DF_OPERACIONES_DET_FecMod] DEFAULT (getdate()) NOT NULL,
    [IDTipoOC]                     CHAR (3)        NOT NULL,
    [IDTrasladista_Prg]            CHAR (4)        NULL,
    [IDVehiculo_Prg]               SMALLINT        NULL,
    [IDGuiaProveedor]              CHAR (6)        NULL,
    [RegeneradoxAcomodo]           BIT             DEFAULT ((0)) NOT NULL,
    [IDOperacion_DetAcomodoTMP]    INT             NULL,
    [NuVoucherBup]                 INT             NULL,
    [FeUpdateRowReservas]          DATETIME        NULL,
    [FlServicioNoShow]             BIT             DEFAULT ((0)) NOT NULL,
    [FlServInManualOper]           BIT             DEFAULT ((0)) NOT NULL,
    [IDMoneda]                     CHAR (3)        NOT NULL,
    [FlServicioTC]                 BIT             DEFAULT ((0)) NOT NULL,
    [Estado_Reserva]               CHAR (2)        NULL,
    [Fec_NV]                       DATETIME        NULL,
    [Fec_RQ]                       DATETIME        NULL,
    [Fec_OK]                       DATETIME        NULL,
    [Fec_WL]                       DATETIME        NULL,
    [Fec_XL]                       DATETIME        NULL,
    [Fec_rr]                       DATETIME        NULL,
    [FlServicioNoCobrado]          BIT             DEFAULT ((0)) NOT NULL,
    [SSTotalGenAntNoCobrado]       NUMERIC (12, 4) NULL,
    [QtCantidadAPagarAntNoCobrado] TINYINT         NULL,
    [CantidadAPagarEditado]        BIT             CONSTRAINT [DF_OPERACIONES_DET_CantidadAPagarEditado] DEFAULT ((0)) NULL,
    [NetoHabEditado]               BIT             CONSTRAINT [DF_OPERACIONES_DET_NetoHabEditado] DEFAULT ((0)) NULL,
    [IgvHabEditado]                BIT             CONSTRAINT [DF_OPERACIONES_DET_IgvHabEditado] DEFAULT ((0)) NULL,
    CONSTRAINT [PK_OPERACIONES_DET] PRIMARY KEY NONCLUSTERED ([IDOperacion_Det] ASC),
    CONSTRAINT [FK_OPERACIONES_DET_MAIDIOMAS] FOREIGN KEY ([IDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_OPERACIONES_DET_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_OPERACIONES_DET_MAPROVEEDORES] FOREIGN KEY ([IDGuiaProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_OPERACIONES_DET_MATIPOOPERACION] FOREIGN KEY ([IDTipoOC]) REFERENCES [dbo].[MATIPOOPERACION] ([IdToc]),
    CONSTRAINT [FK_OPERACIONES_DET_MAUBIGEO] FOREIGN KEY ([IDubigeo]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_OPERACIONES_DET_MAUBIGEO1] FOREIGN KEY ([IDUbigeoOri]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_OPERACIONES_DET_MAUBIGEO2] FOREIGN KEY ([IDUbigeoDes]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_OPERACIONES_DET_MAUSUARIOS] FOREIGN KEY ([IDTrasladista_Prg]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario]),
    CONSTRAINT [FK_OPERACIONES_DET_OPERACIONES] FOREIGN KEY ([IDOperacion]) REFERENCES [dbo].[OPERACIONES] ([IDOperacion]),
    CONSTRAINT [FK_OPERACIONES_DET_RESERVAS_DET] FOREIGN KEY ([IDReserva_Det]) REFERENCES [dbo].[RESERVAS_DET] ([IDReserva_Det]),
    CONSTRAINT [FK_OPERACIONES_DET_VOUCHER_OPERACIONES1] FOREIGN KEY ([IDVoucher]) REFERENCES [dbo].[VOUCHER_OPERACIONES] ([IDVoucher])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDVoucher]
    ON [dbo].[OPERACIONES_DET]([IDVoucher] ASC)
    INCLUDE([IDOperacion]);


GO
CREATE NONCLUSTERED INDEX [IX_IDVoucher_2]
    ON [dbo].[OPERACIONES_DET]([IDVoucher] ASC)
    INCLUDE([IDOperacion], [IDServicio_Det], [TotalGen], [IDMoneda]);


GO
CREATE NONCLUSTERED INDEX [IX_IDOperacion]
    ON [dbo].[OPERACIONES_DET]([IDOperacion] ASC)
    INCLUDE([IDVoucher], [IDServicio_Det], [TotalGen], [IDMoneda]);


GO
CREATE NONCLUSTERED INDEX [IX_IDReserva_Det]
    ON [dbo].[OPERACIONES_DET]([IDReserva_Det] ASC)
    INCLUDE([TotalGen]);


GO
CREATE NONCLUSTERED INDEX [IX_IDVoucher_3]
    ON [dbo].[OPERACIONES_DET]([IDVoucher] ASC, [IDReserva_Det] ASC, [IDOperacion_Det] ASC, [IDOperacion] ASC, [NuVoucherBup] ASC, [FlServInManualOper] ASC);


GO
--15
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_DET_LOG_Del]
  On dbo.OPERACIONES_DET for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOperacion_Det from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--9
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_DET_LOG_Ins]
  On dbo.OPERACIONES_DET for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOperacion_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--12
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_DET_LOG_Upd]
  On dbo.OPERACIONES_DET for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOperacion_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End
