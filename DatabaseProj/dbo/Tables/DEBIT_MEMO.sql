﻿CREATE TABLE [dbo].[DEBIT_MEMO] (
    [IDDebitMemo]             CHAR (10)       NOT NULL,
    [Fecha]                   SMALLDATETIME   DEFAULT (getdate()) NOT NULL,
    [FecVencim]               SMALLDATETIME   NOT NULL,
    [IDCab]                   INT             NULL,
    [IDMoneda]                CHAR (3)        NOT NULL,
    [SubTotal]                NUMERIC (8, 2)  NOT NULL,
    [TotalIGV]                NUMERIC (8, 2)  NOT NULL,
    [Total]                   NUMERIC (8, 2)  NOT NULL,
    [IDBanco]                 CHAR (3)        NOT NULL,
    [IDEstado]                CHAR (2)        NOT NULL,
    [IDTipo]                  CHAR (1)        NOT NULL,
    [UserMod]                 CHAR (4)        NOT NULL,
    [FecMod]                  DATETIME        DEFAULT (getdate()) NOT NULL,
    [FechaIn]                 SMALLDATETIME   NOT NULL,
    [FechaOut]                SMALLDATETIME   NOT NULL,
    [Cliente]                 VARCHAR (80)    NOT NULL,
    [Pax]                     SMALLINT        NOT NULL,
    [Titulo]                  VARCHAR (100)   NOT NULL,
    [Responsable]             VARCHAR (80)    NOT NULL,
    [FlImprimioDebit]         BIT             NOT NULL,
    [IDDebitMemoResumen]      CHAR (10)       NULL,
    [FlManual]                BIT             NOT NULL,
    [IDCliente]               CHAR (6)        NULL,
    [Saldo]                   NUMERIC (10, 2) NOT NULL,
    [FlFacturado]             BIT             DEFAULT ((0)) NOT NULL,
    [FlPorAjustePrecio]       BIT             DEFAULT ((0)) NOT NULL,
    [FlAjustePrecioenDocNota] BIT             DEFAULT ((0)) NOT NULL,
    [FlEnvioInvoice]          BIT             DEFAULT ((0)) NOT NULL,
    [IDMotivo]                INT             NULL,
    [IDResponsable]           CHAR (4)        NULL,
    [IDSupervisor]            CHAR (4)        NULL,
    [TxObservacion]           VARCHAR (MAX)   NULL,
    [FlTransCurso]            BIT             DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DEBIT_MEMO] PRIMARY KEY NONCLUSTERED ([IDDebitMemo] ASC),
    CONSTRAINT [FK_DEBIT_MEMO_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_DEBIT_MEMO_IDDebitMemoResumen] FOREIGN KEY ([IDDebitMemoResumen]) REFERENCES [dbo].[DEBIT_MEMO] ([IDDebitMemo]),
    CONSTRAINT [FK_DEBIT_MEMO_MABANCOS] FOREIGN KEY ([IDBanco]) REFERENCES [dbo].[MABANCOS] ([IDBanco]),
    CONSTRAINT [FK_DEBIT_MEMO_MACLIENTES] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[MACLIENTES] ([IDCliente]),
    CONSTRAINT [FK_DEBIT_MEMO_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDEstado]
    ON [dbo].[DEBIT_MEMO]([IDCab] ASC, [IDEstado] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_DEBIT_MEMO_IDTipo]
    ON [dbo].[DEBIT_MEMO]([IDTipo] ASC)
    INCLUDE([IDDebitMemo], [FecVencim], [IDCab], [Total], [IDEstado], [FechaIn], [FechaOut], [Cliente], [Titulo], [Responsable], [FlImprimioDebit], [IDDebitMemoResumen], [IDCliente], [Saldo]);


GO

Create Trigger [dbo].[DEBIT_MEMO_LOG_Ins]  
  On [dbo].[DEBIT_MEMO] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDebitMemo from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DEBIT_MEMO',@UserMod,@RegistroPK1,'','','',@Accion  
  End    

GO

Create Trigger [dbo].[DEBIT_MEMO_LOG_Del]  
  On [dbo].[DEBIT_MEMO] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Deleted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDebitMemo from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DEBIT_MEMO',@UserMod,@RegistroPK1,'','','',@Accion  
  End    

GO

Create Trigger [dbo].[DEBIT_MEMO_LOG_Upd]  
  On [dbo].[DEBIT_MEMO] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDebitMemo from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DEBIT_MEMO',@UserMod,@RegistroPK1,'','','',@Accion  
  End    
