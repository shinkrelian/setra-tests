﻿CREATE TABLE [dbo].[CONTROL_CALIDAD_PRO_MACUESTIONARIO] (
    [NuControlCalidad] INT           NOT NULL,
    [NuCuest]          TINYINT       NOT NULL,
    [IDProveedor]      CHAR (6)      NOT NULL,
    [FlExcelent]       BIT           DEFAULT ((0)) NOT NULL,
    [FlVeryGood]       BIT           DEFAULT ((0)) NOT NULL,
    [FlGood]           BIT           DEFAULT ((0)) NOT NULL,
    [FlAverage]        BIT           DEFAULT ((0)) NOT NULL,
    [FlPoor]           BIT           DEFAULT ((0)) NOT NULL,
    [TxComentario]     TEXT          NULL,
    [UserMod]          CHAR (4)      NOT NULL,
    [FecMod]           SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_CONTROL_CALIDAD_PRO_MACUESTIONARIO] PRIMARY KEY CLUSTERED ([NuControlCalidad] ASC, [NuCuest] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_CONTROL_CALIDAD_PRO_DET_MACUESTIONARIO] FOREIGN KEY ([NuCuest]) REFERENCES [dbo].[MACUESTIONARIO] ([NuCuest]),
    CONSTRAINT [FK_CONTROL_CALIDAD_PRO_MACUESTIONARIO_CONTROL_CALIDAD_PRO] FOREIGN KEY ([NuControlCalidad]) REFERENCES [dbo].[CONTROL_CALIDAD_PRO] ([NuControlCalidad]),
    CONSTRAINT [FK_CONTROL_CALIDAD_PRO_MACUESTIONARIO_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDProveedor]
    ON [dbo].[CONTROL_CALIDAD_PRO_MACUESTIONARIO]([IDProveedor] ASC);

