﻿CREATE TABLE [dbo].[MAEMPRESA] (
    [CoEmpresa] TINYINT       NOT NULL,
    [TxEmpresa] VARCHAR (250) NOT NULL,
    [FlActivo]  BIT           CONSTRAINT [DF_MAEMPRESA_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]   CHAR (4)      NOT NULL,
    [FecMod]    DATETIME      CONSTRAINT [DF_MAEMPRESA_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAEMPRESA] PRIMARY KEY CLUSTERED ([CoEmpresa] ASC)
);

