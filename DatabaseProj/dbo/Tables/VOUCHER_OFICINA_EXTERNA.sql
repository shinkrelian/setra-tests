﻿CREATE TABLE [dbo].[VOUCHER_OFICINA_EXTERNA] (
    [NuVouOfiExtInt] INT            NOT NULL,
    [NuVouOfiExt]    CHAR (13)      NOT NULL,
    [IDCab]          INT            NOT NULL,
    [CoProveedor]    CHAR (6)       NOT NULL,
    [FeVoucher]      SMALLDATETIME  NOT NULL,
    [CoMoneda]       CHAR (3)       NOT NULL,
    [SsImpuestos]    NUMERIC (8, 2) NOT NULL,
    [SsTotal]        NUMERIC (8, 2) NOT NULL,
    [SsSaldo]        NUMERIC (8, 2) NOT NULL,
    [SsDifAceptada]  NUMERIC (8, 2) NOT NULL,
    [TxObsDocumento] VARCHAR (MAX)  NULL,
    [CoEstado]       CHAR (2)       DEFAULT ('PD') NOT NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       DEFAULT (getdate()) NOT NULL,
    [UserNuevo]      CHAR (4)       NULL,
    CONSTRAINT [PK_VOUCHER_OFICINA_EXTERNA] PRIMARY KEY NONCLUSTERED ([NuVouOfiExtInt] ASC),
    CONSTRAINT [FK_VOUCHER_OFICINA_EXTERNA_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_VOUCHER_OFICINA_EXTERNA_MAMONEDAS] FOREIGN KEY ([CoMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_VOUCHER_OFICINA_EXTERNA_MAPROVEEDORES] FOREIGN KEY ([CoProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);

