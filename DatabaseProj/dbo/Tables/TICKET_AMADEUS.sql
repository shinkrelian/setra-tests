﻿CREATE TABLE [dbo].[TICKET_AMADEUS] (
    [CoTicket]             CHAR (13)       NOT NULL,
    [NoArchivo]            VARCHAR (20)    NULL,
    [CoAir]                CHAR (14)       NOT NULL,
    [CoUsrCrea]            CHAR (9)        NOT NULL,
    [CoUsrDuenio]          CHAR (9)        NOT NULL,
    [CoUsrEmite]           CHAR (9)        NOT NULL,
    [IDCab]                INT             NULL,
    [IDCliente]            CHAR (6)        NULL,
    [CoIATA]               CHAR (8)        NOT NULL,
    [CoLineaAereaRecord]   CHAR (9)        NOT NULL,
    [NoLineaAerea]         VARCHAR (50)    NOT NULL,
    [CoLineaAereaEmisora]  CHAR (7)        NOT NULL,
    [CoLineaAereaSigla]    CHAR (3)        NOT NULL,
    [CoTransacEmision]     CHAR (8)        NOT NULL,
    [CoLineaAereaServicio] CHAR (4)        NOT NULL,
    [TxFechaCreacion]      SMALLDATETIME   NOT NULL,
    [TxFechaModificacion]  SMALLDATETIME   NOT NULL,
    [TxFechaEmision]       SMALLDATETIME   NOT NULL,
    [TxIndicaVenta]        CHAR (1)        NOT NULL,
    [CoPtoVentayEmision]   CHAR (3)        NOT NULL,
    [TxTarifaAerea]        NUMERIC (10, 2) NOT NULL,
    [TxTotal]              NUMERIC (10, 2) NOT NULL,
    [NoPax]                VARCHAR (50)    NOT NULL,
    [TxRestricciones]      VARCHAR (250)   NULL,
    [CoFormaPago]          CHAR (11)       NOT NULL,
    [CoDocumIdent]         CHAR (2)        NOT NULL,
    [NuDocumIdent]         VARCHAR (15)    NOT NULL,
    [TxComision]           NUMERIC (5, 2)  NOT NULL,
    [TxTasaDY_AE]          NUMERIC (6, 2)  NULL,
    [TxTasaHW_DE]          NUMERIC (6, 2)  NULL,
    [TxTasaXC_DP]          NUMERIC (6, 2)  NULL,
    [TxTasaXB_TI]          NUMERIC (6, 2)  NULL,
    [TxTasaQQ_TO]          NUMERIC (6, 2)  NULL,
    [TxTasaAH_SE]          NUMERIC (6, 2)  NULL,
    [UserMod]              CHAR (4)        NOT NULL,
    [FecMod]               SMALLDATETIME   DEFAULT (getdate()) NOT NULL,
    [CoProvOrigen]         CHAR (6)        NOT NULL,
    [CoPNR_Amadeus]        CHAR (6)        NOT NULL,
    [CoTicketOrig]         CHAR (14)       NULL,
    [CoEstado]             CHAR (4)        DEFAULT ('TKTT') NOT NULL,
    [CoEjeCou]             CHAR (4)        NOT NULL,
    [CoEjeRec]             CHAR (4)        NULL,
    [TxObsCou]             VARCHAR (MAX)   NULL,
    [CoEjeCouAmd]          CHAR (8)        NULL,
    [TxTituloPax]          VARCHAR (150)   NULL,
    [CoNacionalidad]       CHAR (6)        NULL,
    [SsComision]           NUMERIC (6, 2)  NOT NULL,
    [SsCosto]              NUMERIC (10, 2) NOT NULL,
    [CoPNR_Aerolinea]      VARCHAR (20)    NOT NULL,
    [NuNtaVta]             CHAR (6)        NULL,
    CONSTRAINT [PK_TICKET_AMADEUS] PRIMARY KEY NONCLUSTERED ([CoTicket] ASC),
    CONSTRAINT [FK_TICKET_AMADEUS_MAUBIGEO] FOREIGN KEY ([CoNacionalidad]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO
Create Trigger [dbo].[TICKET_AMADEUS_LOG_Upd]  
  On [dbo].[TICKET_AMADEUS] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_AMADEUS_LOG_Del]  
  On [dbo].[TICKET_AMADEUS] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_AMADEUS_LOG_Ins]  
  On [dbo].[TICKET_AMADEUS] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
