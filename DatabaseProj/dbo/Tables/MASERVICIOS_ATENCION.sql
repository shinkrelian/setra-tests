﻿CREATE TABLE [dbo].[MASERVICIOS_ATENCION] (
    [IDAtencion] TINYINT       NOT NULL,
    [IDservicio] CHAR (8)      NOT NULL,
    [Dia1]       CHAR (1)      NOT NULL,
    [Dia2]       CHAR (1)      NOT NULL,
    [Desde1]     SMALLDATETIME NOT NULL,
    [Hasta1]     SMALLDATETIME NOT NULL,
    [Desde2]     SMALLDATETIME NOT NULL,
    [Hasta2]     SMALLDATETIME NOT NULL,
    [Mig]        BIT           CONSTRAINT [DF__MASERVICIOS__Mig__2B6A5820] DEFAULT ((0)) NOT NULL,
    [UserMod]    CHAR (4)      NOT NULL,
    [FecMod]     DATETIME      CONSTRAINT [DF__MASERVICI__FecMo__1F398B65] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MASERVICIOS_ATENCION] PRIMARY KEY CLUSTERED ([IDAtencion] ASC, [IDservicio] ASC),
    CONSTRAINT [CK_MASERVICIOS_ATENCION] CHECK ([Dia1]='L' OR [Dia1]='M' OR [Dia1]='X' OR [Dia1]='J' OR [Dia1]='V' OR [Dia1]='S' OR [Dia1]='D'),
    CONSTRAINT [CK_MASERVICIOS_ATENCION2] CHECK ([Dia2]='L' OR [Dia2]='M' OR [Dia2]='X' OR [Dia2]='J' OR [Dia2]='V' OR [Dia2]='S' OR [Dia2]='D'),
    CONSTRAINT [FK_MASERVICIOS_MASERVICIOS_ATENCION] FOREIGN KEY ([IDservicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio])
);


GO


--248
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_ATENCION_LOG_Ins]
 On [dbo].[MASERVICIOS_ATENCION] for Insert
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDAtencion from Inserted)
			set @RegistroPK2 = (Select IDServicio from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_ATENCION',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO


--249
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_ATENCION_LOG_Upd]
 On [dbo].[MASERVICIOS_ATENCION] for Update
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDAtencion from Inserted)
			set @RegistroPK2 = (Select IDServicio from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_ATENCION',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO

--250
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_ATENCION_LOG_Del]
 On [dbo].[MASERVICIOS_ATENCION] for Delete
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from deleted) = 1
		Begin
			set @RegistroPK1 = (Select IDAtencion from Deleted)
			set @RegistroPK2 = (Select IDServicio from Deleted)
			set @UserMod = (Select UserMod from Deleted)
			set @Accion = 'D'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_ATENCION',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End
