﻿CREATE TABLE [dbo].[COTIDET_LOG] (
    [ID]                    INT            IDENTITY (1, 1) NOT NULL,
    [FechaLog]              DATETIME       CONSTRAINT [DF_COTIDET_LOG_FechaLog] DEFAULT (getdate()) NOT NULL,
    [Accion]                CHAR (1)       NOT NULL,
    [UserLog]               CHAR (4)       NOT NULL,
    [Atendido]              BIT            CONSTRAINT [DF_COTIDET_LOG_Atendido] DEFAULT ((0)) NOT NULL,
    [IDDET]                 INT            NOT NULL,
    [IDCAB]                 INT            NOT NULL,
    [Cotizacion]            CHAR (8)       NOT NULL,
    [Item]                  NUMERIC (6, 3) NOT NULL,
    [Dia]                   SMALLDATETIME  NOT NULL,
    [DiaNombre]             VARCHAR (10)   NOT NULL,
    [IDubigeo]              CHAR (6)       NOT NULL,
    [IDProveedor]           CHAR (6)       NOT NULL,
    [IDServicio]            CHAR (8)       NOT NULL,
    [IDServicio_Det]        INT            NOT NULL,
    [IDidioma]              VARCHAR (12)   NOT NULL,
    [Especial]              BIT            NOT NULL,
    [MotivoEspecial]        VARCHAR (200)  NULL,
    [RutaDocSustento]       VARCHAR (200)  NULL,
    [Desayuno]              BIT            NOT NULL,
    [Lonche]                BIT            NOT NULL,
    [Almuerzo]              BIT            NOT NULL,
    [Cena]                  BIT            NOT NULL,
    [Transfer]              BIT            NOT NULL,
    [IDDetTransferOri]      INT            NULL,
    [IDDetTransferDes]      INT            NULL,
    [CamaMat]               TINYINT        NULL,
    [TipoTransporte]        CHAR (1)       NULL,
    [IDUbigeoOri]           CHAR (6)       NOT NULL,
    [IDUbigeoDes]           CHAR (6)       NOT NULL,
    [NroPax]                SMALLINT       NOT NULL,
    [NroLiberados]          SMALLINT       NULL,
    [Tipo_Lib]              CHAR (1)       NULL,
    [CostoReal]             NUMERIC (8, 2) NOT NULL,
    [CostoRealAnt]          NUMERIC (8, 2) NULL,
    [CostoLiberado]         NUMERIC (8, 2) NOT NULL,
    [Margen]                NUMERIC (8, 2) NOT NULL,
    [MargenAplicado]        NUMERIC (6, 2) NOT NULL,
    [MargenAplicadoAnt]     NUMERIC (6, 2) NULL,
    [MargenLiberado]        NUMERIC (8, 2) NOT NULL,
    [Total]                 NUMERIC (8, 2) NOT NULL,
    [TotalOrig]             NUMERIC (8, 2) NOT NULL,
    [SSCR]                  NUMERIC (8, 2) NOT NULL,
    [SSCL]                  NUMERIC (8, 2) NOT NULL,
    [SSMargen]              NUMERIC (8, 2) NOT NULL,
    [SSMargenLiberado]      NUMERIC (8, 2) NOT NULL,
    [SSTotal]               NUMERIC (8, 2) NOT NULL,
    [SSTotalOrig]           NUMERIC (8, 2) NOT NULL,
    [STCR]                  NUMERIC (8, 2) NOT NULL,
    [STMargen]              NUMERIC (8, 2) NOT NULL,
    [STTotal]               NUMERIC (8, 2) NOT NULL,
    [STTotalOrig]           NUMERIC (8, 2) NOT NULL,
    [CostoRealImpto]        NUMERIC (8, 2) NOT NULL,
    [CostoLiberadoImpto]    NUMERIC (8, 2) NOT NULL,
    [MargenImpto]           NUMERIC (8, 2) NOT NULL,
    [MargenLiberadoImpto]   NUMERIC (6, 2) NOT NULL,
    [SSCRImpto]             NUMERIC (8, 2) NOT NULL,
    [SSCLImpto]             NUMERIC (8, 2) NOT NULL,
    [SSMargenImpto]         NUMERIC (8, 2) NOT NULL,
    [SSMargenLiberadoImpto] NUMERIC (8, 2) NOT NULL,
    [STCRImpto]             NUMERIC (8, 2) NOT NULL,
    [STMargenImpto]         NUMERIC (8, 2) NOT NULL,
    [TotImpto]              NUMERIC (8, 2) NOT NULL,
    [IDDetRel]              INT            NULL,
    [Servicio]              VARCHAR (MAX)  NULL,
    [IDDetCopia]            INT            NULL,
    [IncGuia]               BIT            NOT NULL,
    [UserMod]               CHAR (4)       NOT NULL,
    [FecMod]                DATETIME       NOT NULL,
    [UserModAtendidoLog]    CHAR (4)       NULL,
    [FecModAtendidoLog]     DATETIME       NULL,
    CONSTRAINT [PK_COTIDET_LOG] PRIMARY KEY NONCLUSTERED ([ID] ASC),
    CONSTRAINT [CK_COTIDET_LOG] CHECK ([Accion]='N' OR [Accion]='M' OR [Accion]='B' OR [Accion]=' ')
);


GO

--5
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIDET_LOG_LOG_Ins]
  On dbo.COTIDET_LOG for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select ID from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_LOG',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--6
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIDET_LOG_LOG_Upd]
  On dbo.COTIDET_LOG for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select ID from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_LOG',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--7
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIDET_LOG_LOG_Del]
  On dbo.COTIDET_LOG for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select ID from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_LOG',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'([Accion]=''N'' OR [Accion]=''M'' OR [Accion]=''B'' OR [Accion]='' '')', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'COTIDET_LOG', @level2type = N'CONSTRAINT', @level2name = N'CK_COTIDET_LOG';

