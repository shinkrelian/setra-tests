﻿CREATE TABLE [dbo].[COTIDET_DETSERVICIOS] (
    [IDDET]                   INT             NOT NULL,
    [IDServicio_Det]          INT             NOT NULL,
    [CostoReal]               NUMERIC (12, 4) NOT NULL,
    [CostoLiberado]           NUMERIC (12, 4) NOT NULL,
    [Margen]                  NUMERIC (12, 4) NOT NULL,
    [MargenAplicado]          NUMERIC (6, 2)  NOT NULL,
    [MargenLiberado]          NUMERIC (12, 4) NOT NULL,
    [Total]                   NUMERIC (8, 2)  NOT NULL,
    [SSCR]                    NUMERIC (12, 4) NOT NULL,
    [SSCL]                    NUMERIC (12, 4) NOT NULL,
    [SSMargen]                NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberado]        NUMERIC (12, 4) NOT NULL,
    [SSTotal]                 NUMERIC (12, 4) NOT NULL,
    [STCR]                    NUMERIC (12, 4) NOT NULL,
    [STMargen]                NUMERIC (12, 4) NOT NULL,
    [STTotal]                 NUMERIC (12, 4) NOT NULL,
    [CostoRealImpto]          NUMERIC (12, 4) NOT NULL,
    [CostoLiberadoImpto]      NUMERIC (12, 4) NOT NULL,
    [MargenImpto]             NUMERIC (12, 4) NOT NULL,
    [MargenLiberadoImpto]     NUMERIC (6, 2)  NOT NULL,
    [SSCRImpto]               NUMERIC (12, 4) NOT NULL,
    [SSCLImpto]               NUMERIC (12, 4) NOT NULL,
    [SSMargenImpto]           NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberadoImpto]   NUMERIC (12, 4) NOT NULL,
    [STCRImpto]               NUMERIC (12, 4) NOT NULL,
    [STMargenImpto]           NUMERIC (12, 4) NOT NULL,
    [TotImpto]                NUMERIC (12, 4) NOT NULL,
    [UserMod]                 CHAR (4)        NOT NULL,
    [FecMod]                  DATETIME        CONSTRAINT [DF__COTIDET_D__FecMo__32B6742D] DEFAULT (getdate()) NOT NULL,
    [RedondeoTotal]           NUMERIC (12, 4) NOT NULL,
    [FlServicioInCorrectoxTI] BIT             DEFAULT ((0)) NOT NULL,
    [NuNro]                   TINYINT         DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_COTIDET_DETSERVICIOS] PRIMARY KEY NONCLUSTERED ([IDDET] ASC, [IDServicio_Det] ASC, [NuNro] ASC),
    CONSTRAINT [FK_COTIDET_DETSERVICIOS_COTIDET] FOREIGN KEY ([IDDET]) REFERENCES [dbo].[COTIDET] ([IDDET]),
    CONSTRAINT [FK_COTIDET_DETSERVICIOS_MASERVICIOS_DET] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det])
);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_DETSERVICIOS_IDServicio_Det]
    ON [dbo].[COTIDET_DETSERVICIOS]([IDServicio_Det] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_DETSERVICIOS_IDDET]
    ON [dbo].[COTIDET_DETSERVICIOS]([IDDET] ASC);


GO

--132
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_DETSERVICIOS_LOG_Del]
  On [dbo].[COTIDET_DETSERVICIOS] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET from Deleted)
			set @RegistroPK2 = (select IDServicio_Det from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--130
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_DETSERVICIOS_LOG_Ins]
  On [dbo].[COTIDET_DETSERVICIOS] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET from Inserted)
			set @RegistroPK2 = (select IDServicio_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--131
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_DETSERVICIOS_LOG_Upd]
  On [dbo].[COTIDET_DETSERVICIOS] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET from Inserted)
			set @RegistroPK2 = (select IDServicio_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
