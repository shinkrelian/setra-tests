﻿CREATE TABLE [dbo].[Correlativo] (
    [Tabla]             VARCHAR (30)  NOT NULL,
    [Correlativo]       INT           NOT NULL,
    [UserMod]           CHAR (4)      NOT NULL,
    [FecMod]            DATETIME      DEFAULT (getdate()) NOT NULL,
    [IDTipoDoc]         CHAR (3)      NULL,
    [CoSerie]           CHAR (3)      NULL,
    [NoSerie]           VARCHAR (50)  NULL,
    [IDTipoDoc2]        CHAR (3)      NULL,
    [TxTextoImpresion]  VARCHAR (80)  NULL,
    [TxTextoArchivoTxt] VARCHAR (100) NULL
);

