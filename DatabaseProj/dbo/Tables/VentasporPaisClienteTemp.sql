﻿CREATE TABLE [dbo].[VentasporPaisClienteTemp] (
    [CoPais]          CHAR (6)        NULL,
    [DescPaisCliente] VARCHAR (60)    NULL,
    [CoCliente]       CHAR (6)        NOT NULL,
    [DescCliente]     VARCHAR (80)    NOT NULL,
    [FecOutPeru]      VARCHAR (6)     NULL,
    [ValorVenta]      NUMERIC (38, 6) NULL,
    [ParamAnio]       CHAR (4)        NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_VentasporPaisClienteTemp]
    ON [dbo].[VentasporPaisClienteTemp]([CoPais] ASC, [CoCliente] ASC, [FecOutPeru] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_VentaPorPaisClienteTemp]
    ON [dbo].[VentasporPaisClienteTemp]([ParamAnio] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_VentasporPaisClienteTemp_2]
    ON [dbo].[VentasporPaisClienteTemp]([ParamAnio] ASC)
    INCLUDE([CoPais], [DescPaisCliente], [CoCliente], [DescCliente], [FecOutPeru], [ValorVenta]);

