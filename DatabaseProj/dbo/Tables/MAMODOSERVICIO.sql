﻿CREATE TABLE [dbo].[MAMODOSERVICIO] (
    [CodModoServ] CHAR (2)     NOT NULL,
    [Descripcion] VARCHAR (30) NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MODOSERVICIO] PRIMARY KEY CLUSTERED ([CodModoServ] ASC)
);


GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_MAMODOSERVICIO_Descripcion]
    ON [dbo].[MAMODOSERVICIO]([Descripcion] ASC);


GO

--81
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAMODOSERVICIO

CREATE Trigger [dbo].[MAMODOSERVICIO_LOG_Ins]
  On [dbo].[MAMODOSERVICIO] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select CodModoServ from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAMODOSERVICIO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO


--85
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAMODOSERVICIO_LOG_Del]
  On [dbo].[MAMODOSERVICIO] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select CodModoServ from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAMODOSERVICIO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--83
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAMODOSERVICIO_LOG_Upd]
  On [dbo].[MAMODOSERVICIO] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select CodModoServ from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAMODOSERVICIO',@UserMod,@RegistroPK1,'','','',@Accion
		End  
