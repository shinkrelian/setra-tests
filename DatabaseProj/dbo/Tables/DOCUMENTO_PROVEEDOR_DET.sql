﻿CREATE TABLE [dbo].[DOCUMENTO_PROVEEDOR_DET] (
    [NuDocumProv]    INT            NOT NULL,
    [NuDocumProvDet] TINYINT        NOT NULL,
    [IDServicio_Det] INT            NULL,
    [TxServicio]     VARCHAR (MAX)  NULL,
    [SsMonto]        NUMERIC (9, 2) NOT NULL,
    [FlActivo]       BIT            CONSTRAINT [DF_DOCUMENTO_PROVEEDOR_DET_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       CONSTRAINT [DF_DOCUMENTO_PROVEEDOR_DET_FecMod] DEFAULT (getdate()) NOT NULL,
    [QtCantidad]     INT            NULL,
    [CoProducto]     CHAR (6)       NULL,
    [CoAlmacen]      CHAR (6)       NULL,
    [SsIGV]          NUMERIC (8, 2) NULL,
    [CoTipoOC]       CHAR (3)       NULL,
    [CoCtaContab]    VARCHAR (14)   NULL,
    [CoCeCos]        CHAR (6)       NULL,
    [CoCeCon]        CHAR (6)       NULL,
    [SSPrecUni]      NUMERIC (9, 2) NULL,
    [IDCab]          INT            NULL,
    [SSTotal]        NUMERIC (9, 2) NULL,
    CONSTRAINT [PK_DOCUMENTO_PROVEEDOR_DET] PRIMARY KEY NONCLUSTERED ([NuDocumProv] ASC, [NuDocumProvDet] ASC),
    CONSTRAINT [FK_DOCUMENTO_PROVEEDOR_DET_DOCUMENTO_PROVEEDOR] FOREIGN KEY ([NuDocumProv]) REFERENCES [dbo].[DOCUMENTO_PROVEEDOR] ([NuDocumProv])
);

