﻿CREATE TABLE [dbo].[MACORREOSBIBLIAPROVEEDOR] (
    [IDProveedor] CHAR (6)      NOT NULL,
    [Correo]      VARCHAR (150) NOT NULL,
    [Descripcion] VARCHAR (MAX) NULL,
    [UserMod]     CHAR (4)      NOT NULL,
    [FecMod]      SMALLDATETIME NOT NULL,
    CONSTRAINT [PK_MACORREOSBIBLIAPROVEEDOR] PRIMARY KEY CLUSTERED ([IDProveedor] ASC, [Correo] ASC)
);


GO
Create Trigger [dbo].[MACORREOSBIBLIAPROVEEDOR_LOG_Ins]  
  On [dbo].[MACORREOSBIBLIAPROVEEDOR] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDProveedor from Inserted)     
   set @RegistroPK2 = (select Correo from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MACORREOSBIBLIAPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MACORREOSBIBLIAPROVEEDOR_LOG_Upd]  
  On [dbo].[MACORREOSBIBLIAPROVEEDOR] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDProveedor from Inserted)     
   set @RegistroPK2 = (select Correo from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MACORREOSBIBLIAPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MACORREOSBIBLIAPROVEEDOR_LOG_Del]  
  On [dbo].[MACORREOSBIBLIAPROVEEDOR] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDProveedor from Deleted)   
   set @RegistroPK2 = (select Correo from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MACORREOSBIBLIAPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
