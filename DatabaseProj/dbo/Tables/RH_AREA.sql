﻿CREATE TABLE [dbo].[RH_AREA] (
    [CoArea]   CHAR (2)      NOT NULL,
    [NoArea]   VARCHAR (150) NOT NULL,
    [FlActivo] BIT           CONSTRAINT [DF_RH_AREA_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]  CHAR (4)      NOT NULL,
    [FecMod]   DATETIME      CONSTRAINT [DF_RH_AREA_FecMod] DEFAULT (getdate()) NOT NULL,
    [NuOrden]  TINYINT       DEFAULT (NULL) NULL,
    [CoCeCos]  CHAR (6)      DEFAULT (NULL) NULL,
    CONSTRAINT [PK_RH_AREA] PRIMARY KEY CLUSTERED ([CoArea] ASC),
    CONSTRAINT [FK_RH_AREA_CoCeCos] FOREIGN KEY ([CoCeCos]) REFERENCES [dbo].[MACENTROCOSTOS] ([CoCeCos])
);

