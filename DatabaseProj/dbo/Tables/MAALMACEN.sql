﻿CREATE TABLE [dbo].[MAALMACEN] (
    [CoAlmacen] CHAR (6)       NOT NULL,
    [NoAlmacen] NVARCHAR (100) NULL,
    [CoSAP]     NVARCHAR (20)  NULL,
    [FlActivo]  BIT            CONSTRAINT [DF_MAALMACEN_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]   CHAR (4)       NOT NULL,
    [FecMod]    DATETIME       CONSTRAINT [DF_ALMACEN_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAALMACEN] PRIMARY KEY CLUSTERED ([CoAlmacen] ASC)
);

