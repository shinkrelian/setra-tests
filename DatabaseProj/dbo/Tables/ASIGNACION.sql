﻿CREATE TABLE [dbo].[ASIGNACION] (
    [NuAsignacion]    INT             NOT NULL,
    [FeAsignacion]    SMALLDATETIME   NOT NULL,
    [TxObservacion]   VARCHAR (MAX)   NULL,
    [SsImportePagado] NUMERIC (10, 2) NOT NULL,
    [UserMod]         CHAR (4)        NOT NULL,
    [FecMod]          SMALLDATETIME   CONSTRAINT [CK_ASIGNACION] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ASIGNACION] PRIMARY KEY CLUSTERED ([NuAsignacion] ASC)
);


GO
Create Trigger [dbo].[ASIGNACION_LOG_Ins]  
  On [dbo].[ASIGNACION] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuAsignacion from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ASIGNACION',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[ASIGNACION_LOG_Upd]  
  On [dbo].[ASIGNACION] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuAsignacion from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ASIGNACION',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[ASIGNACION_LOG_Del]  
  On [dbo].[ASIGNACION] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuAsignacion from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ASIGNACION',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
