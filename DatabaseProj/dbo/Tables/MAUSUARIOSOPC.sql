﻿CREATE TABLE [dbo].[MAUSUARIOSOPC] (
    [IDOpc]      CHAR (6) NOT NULL,
    [IDUsuario]  CHAR (4) NOT NULL,
    [Consultar]  BIT      NOT NULL,
    [Grabar]     BIT      NOT NULL,
    [Actualizar] BIT      NOT NULL,
    [Eliminar]   BIT      NOT NULL,
    [UserMod]    CHAR (4) NOT NULL,
    [FecMod]     DATETIME CONSTRAINT [DF_MAUSUARIOSOPC_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAUSUARIOSOPC] PRIMARY KEY CLUSTERED ([IDOpc] ASC, [IDUsuario] ASC),
    CONSTRAINT [FK_MAUSUARIOSOPC_MAOPCIONES] FOREIGN KEY ([IDOpc]) REFERENCES [dbo].[MAOPCIONES] ([IDOpc]),
    CONSTRAINT [FK_MAUSUARIOSOPC_MAUSUARIOS] FOREIGN KEY ([IDUsuario]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario])
);


GO

--138
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAUSUARIOSOPC

CREATE Trigger [dbo].[MAUSUARIOSOPC_LOG_Ins]
  On [dbo].[MAUSUARIOSOPC] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Inserted)
			set @RegistroPK2 = (select IDUsuario from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUSUARIOSOPC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--142
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAUSUARIOSOPC_LOG_Upd]
  On [dbo].[MAUSUARIOSOPC] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Inserted)
			set @RegistroPK2 = (select IDUsuario from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUSUARIOSOPC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--146
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAUSUARIOSOPC_LOG_Del]
  On [dbo].[MAUSUARIOSOPC] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOpc from Deleted)
			set @RegistroPK2 = (select IDUsuario from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUSUARIOSOPC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
