﻿CREATE TABLE [dbo].[STOCK_TICKET_ENTRADAS] (
    [IDServicio]     CHAR (8) NOT NULL,
    [QtStkEnt]       SMALLINT NOT NULL,
    [UserMod]        CHAR (4) NOT NULL,
    [FecMod]         DATETIME DEFAULT (getdate()) NOT NULL,
    [QtEntCompradas] SMALLINT DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_STOCK_TICKET_ENTRADAS] PRIMARY KEY NONCLUSTERED ([IDServicio] ASC),
    CONSTRAINT [FK_STOCK_TICKET_ENTRADAS_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio])
);

