﻿CREATE TABLE [dbo].[COTIDET_QUIEBRES] (
    [IDDET_Q]               INT             IDENTITY (1, 1) NOT NULL,
    [IDCAB_Q]               INT             NOT NULL,
    [IDDet]                 INT             NOT NULL,
    [CostoReal]             NUMERIC (12, 4) NOT NULL,
    [CostoLiberado]         NUMERIC (12, 4) NOT NULL,
    [Margen]                NUMERIC (12, 4) NOT NULL,
    [MargenAplicado]        NUMERIC (6, 2)  NOT NULL,
    [MargenLiberado]        NUMERIC (6, 2)  NOT NULL,
    [Total]                 NUMERIC (8, 2)  NOT NULL,
    [RedondeoTotal]         NUMERIC (12, 4) NOT NULL,
    [SSCR]                  NUMERIC (12, 4) NOT NULL,
    [SSCL]                  NUMERIC (12, 4) NOT NULL,
    [SSMargen]              NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberado]      NUMERIC (12, 4) NOT NULL,
    [SSTotal]               NUMERIC (12, 4) NOT NULL,
    [STCR]                  NUMERIC (12, 4) NOT NULL,
    [STMargen]              NUMERIC (12, 4) NOT NULL,
    [STTotal]               NUMERIC (12, 4) NOT NULL,
    [CostoRealImpto]        NUMERIC (12, 4) NOT NULL,
    [CostoLiberadoImpto]    NUMERIC (12, 4) NOT NULL,
    [MargenImpto]           NUMERIC (12, 4) NOT NULL,
    [MargenLiberadoImpto]   NUMERIC (6, 2)  NOT NULL,
    [SSCRImpto]             NUMERIC (12, 4) NOT NULL,
    [SSCLImpto]             NUMERIC (12, 4) NOT NULL,
    [SSMargenImpto]         NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberadoImpto] NUMERIC (12, 4) NOT NULL,
    [STCRImpto]             NUMERIC (12, 4) NOT NULL,
    [STMargenImpto]         NUMERIC (12, 4) NOT NULL,
    [TotImpto]              NUMERIC (12, 4) NOT NULL,
    [UserMod]               CHAR (4)        NOT NULL,
    [FecMod]                DATETIME        CONSTRAINT [DF__COTIDET_Q__FecMo__2685A772] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTIDET_QUIEBRES_1] PRIMARY KEY CLUSTERED ([IDDET_Q] ASC),
    CONSTRAINT [FK_COTIDET_QUIEBRES_COTICAB_QUIEBRES] FOREIGN KEY ([IDCAB_Q]) REFERENCES [dbo].[COTICAB_QUIEBRES] ([IDCAB_Q]),
    CONSTRAINT [FK_COTIDET_QUIEBRES_COTIDET] FOREIGN KEY ([IDDet]) REFERENCES [dbo].[COTIDET] ([IDDET])
);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_QUIEBRES_IDDet]
    ON [dbo].[COTIDET_QUIEBRES]([IDDet] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_QUIEBRES_IDCab_Q]
    ON [dbo].[COTIDET_QUIEBRES]([IDCAB_Q] ASC);


GO

--149
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_QUIEBRES_LOG_Del]
  On [dbo].[COTIDET_QUIEBRES] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET_Q from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_QUIEBRES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--145
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_QUIEBRES_LOG_Upd]
  On [dbo].[COTIDET_QUIEBRES] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET_Q from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_QUIEBRES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--141
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIDET_QUIEBRES_LOG_Ins]
  On [dbo].[COTIDET_QUIEBRES] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET_Q from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_QUIEBRES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
