﻿CREATE TABLE [dbo].[VentasporEjecutivoAnio] (
    [IDUsuario]              CHAR (4)        NOT NULL,
    [Nombre]                 VARCHAR (60)    NOT NULL,
    [VentaAnualUSD]          NUMERIC (38, 6) NULL,
    [VentaAnualUSDAnioAnter] NUMERIC (12, 4) NOT NULL,
    [VarVentaAnualUSD]       NUMERIC (38, 6) NULL,
    [CantFilesAnioAnter]     TINYINT         NOT NULL,
    [ParamAnio]              CHAR (4)        NULL
);

