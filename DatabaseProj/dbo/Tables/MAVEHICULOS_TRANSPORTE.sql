﻿CREATE TABLE [dbo].[MAVEHICULOS_TRANSPORTE] (
    [NuVehiculo]          SMALLINT      NOT NULL,
    [NuVehiculo_Anterior] SMALLINT      NULL,
    [CoTipo]              SMALLINT      NULL,
    [QtDe]                TINYINT       NULL,
    [QtHasta]             TINYINT       NULL,
    [NoVehiculoCitado]    VARCHAR (200) NULL,
    [QtCapacidad]         TINYINT       NULL,
    [FlOtraCiudad]        BIT           NULL,
    [CoUbigeo]            CHAR (6)      NULL,
    [CoMercado]           SMALLINT      NULL,
    [FlActivo]            BIT           DEFAULT ((1)) NOT NULL,
    [Usermod]             CHAR (4)      NOT NULL,
    [Fecmod]              DATETIME      DEFAULT (getdate()) NULL,
    [FlReservas]          BIT           DEFAULT ((1)) NOT NULL,
    [FlProgramacion]      BIT           DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MAVEHICULOS_TRANSPORTE] PRIMARY KEY CLUSTERED ([NuVehiculo] ASC)
);

