﻿CREATE TABLE [dbo].[MAINCLUIDOOPERACIONES_IDIOMAS] (
    [NuIncluido]    INT           NOT NULL,
    [IDIdioma]      VARCHAR (12)  NOT NULL,
    [NoObservacion] VARCHAR (MAX) NOT NULL,
    [UserMod]       CHAR (4)      NOT NULL,
    [FecMod]        DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAINCLUIDOOPERACIONES_IDIOMAS] PRIMARY KEY CLUSTERED ([NuIncluido] ASC, [IDIdioma] ASC),
    CONSTRAINT [FK_MAINCLUIDOOPERACIONES_IDIOMAS_IDIOMAS] FOREIGN KEY ([IDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_MAINCLUIDOOPERACIONES_IDIOMAS_MAINCLUIDOOPERACIONES] FOREIGN KEY ([NuIncluido]) REFERENCES [dbo].[MAINCLUIDOOPERACIONES] ([NuIncluido])
);


GO
Create Trigger [dbo].[MAINCLUIDOOPERACIONES_IDIOMAS_LOG_Del]  
  On [dbo].[MAINCLUIDOOPERACIONES_IDIOMAS] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuIncluido from Deleted)    
   set @RegistroPK2 = (select IDIdioma from Deleted)    
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDOOPERACIONES_IDIOMAS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MAINCLUIDOOPERACIONES_IDIOMAS_LOG_Ins]  
  On [dbo].[MAINCLUIDOOPERACIONES_IDIOMAS] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuIncluido from Inserted)     
   set @RegistroPK2 = (select IDIdioma from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDOOPERACIONES_IDIOMAS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MAINCLUIDOOPERACIONES_IDIOMAS_LOG_Upd]  
  On [dbo].[MAINCLUIDOOPERACIONES_IDIOMAS] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuIncluido from Inserted)    
   set @RegistroPK2 = (select IDIdioma from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDOOPERACIONES_IDIOMAS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
