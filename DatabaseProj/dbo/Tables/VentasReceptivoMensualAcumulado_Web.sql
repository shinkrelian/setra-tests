﻿CREATE TABLE [dbo].[VentasReceptivoMensualAcumulado_Web] (
    [FecOutPeru]             CHAR (6)        NULL,
    [ImporteMesUSD]          NUMERIC (12, 2) NULL,
    [ImporteMesUSDAnioAnter] NUMERIC (12, 2) NULL,
    [ParamAnio]              CHAR (4)        NULL
);

