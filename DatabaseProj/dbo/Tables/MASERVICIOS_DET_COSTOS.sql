﻿CREATE TABLE [dbo].[MASERVICIOS_DET_COSTOS] (
    [Correlativo]    TINYINT         NOT NULL,
    [IDServicio_Det] INT             NOT NULL,
    [PaxDesde]       SMALLINT        NOT NULL,
    [PaxHasta]       SMALLINT        NOT NULL,
    [Monto]          NUMERIC (15, 4) NOT NULL,
    [Mig]            BIT             CONSTRAINT [DF__MASERVICIOS__Mig__2E46C4CB] DEFAULT ((0)) NOT NULL,
    [UserMod]        CHAR (4)        NOT NULL,
    [FecMod]         DATETIME        CONSTRAINT [DF_MASERVICIOS_DET_COSTOS_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MASERVICIOS_DET_COSTOS] PRIMARY KEY CLUSTERED ([Correlativo] ASC, [IDServicio_Det] ASC),
    CONSTRAINT [FK_MASERVICIOS_DET_COSTOS_MASERVICIOS_DET] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDServicio_Det]
    ON [dbo].[MASERVICIOS_DET_COSTOS]([IDServicio_Det] ASC, [PaxDesde] ASC, [PaxHasta] ASC);


GO

--247
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_DET_COSTOS_LOG_Del]
 On [dbo].[MASERVICIOS_DET_COSTOS] for Delete
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from deleted) = 1
		Begin
			set @RegistroPK1 = (Select Correlativo from Deleted)
			set @RegistroPK2 = (Select IDServicio_Det from Deleted)
			set @UserMod = (Select UserMod from Deleted)
			set @Accion = 'D'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DET_COSTOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO


--245
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_DET_COSTOS_LOG_Ins]
 On [dbo].[MASERVICIOS_DET_COSTOS] for Insert
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select Correlativo from Inserted)
			set @RegistroPK2 = (Select IDServicio_Det from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DET_COSTOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO


--246
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_DET_COSTOS_LOG_Upd]
 On [dbo].[MASERVICIOS_DET_COSTOS] for Update
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select Correlativo from Inserted)
			set @RegistroPK2 = (Select IDServicio_Det from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DET_COSTOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End
