﻿CREATE TABLE [dbo].[MATEXTOSERVICIOS] (
    [IDServicio] CHAR (8)      NOT NULL,
    [IDIdioma]   VARCHAR (12)  NOT NULL,
    [Titulo]     VARCHAR (100) NULL,
    [Texto]      TEXT          NOT NULL,
    [UserMod]    CHAR (4)      NOT NULL,
    [FechaMod]   DATETIME      CONSTRAINT [DF__MATEXTOSE__Fecha__09946309] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MATEXTOSERVICIOS] PRIMARY KEY NONCLUSTERED ([IDServicio] ASC, [IDIdioma] ASC)
);


GO

--218
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MATEXTOSERVICIOS_LOG_Upd]  
  On [dbo].[MATEXTOSERVICIOS] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDServicio from Inserted)  
   set @RegistroPK2 = (select IDIdioma from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MATEXTOSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End  

GO

--219
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MATEXTOSERVICIOS_LOG_Del]  
  On [dbo].[MATEXTOSERVICIOS] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Deleted) = 1   
  Begin   
   set @RegistroPK1 = (select IDServicio from Deleted)  
   set @RegistroPK2 = (select IDIdioma from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MATEXTOSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End  

GO

--217
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MATEXTOSERVICIOS_LOG_Ins]  
  On [dbo].[MATEXTOSERVICIOS] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDServicio from Inserted)  
   set @RegistroPK2 = (select IDIdioma from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MATEXTOSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End  
