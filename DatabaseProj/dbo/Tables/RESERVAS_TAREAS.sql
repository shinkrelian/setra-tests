﻿CREATE TABLE [dbo].[RESERVAS_TAREAS] (
    [IDReserva]    INT           NOT NULL,
    [IDTarea]      SMALLINT      NOT NULL,
    [IDEstado]     CHAR (2)      NOT NULL,
    [FecDeadLine]  SMALLDATETIME NOT NULL,
    [IDCab]        INT           NOT NULL,
    [Descripcion]  VARCHAR (200) NULL,
    [UltimoMinuto] BIT           NOT NULL,
    [UserMod]      CHAR (4)      NOT NULL,
    [FecMod]       DATETIME      CONSTRAINT [DF__RESERVAS___FecMo__0055DCE9] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RESERVAS_TAREAS] PRIMARY KEY CLUSTERED ([IDReserva] ASC, [IDTarea] ASC, [IDCab] ASC),
    CONSTRAINT [FK_RESERVAS_TAREAS_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_RESERVAS_TAREAS_RESERVAS] FOREIGN KEY ([IDReserva]) REFERENCES [dbo].[RESERVAS] ([IDReserva])
);


GO

--23
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_TAREAS_LOG_Del]
 On [Dbo].[RESERVAS_TAREAS] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Deleted)
			set @RegistroPK2 = (select IDTarea from Deleted)
			set @RegistroPK3 = (select IDCab from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_TAREAS',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion
		End  

GO

--28
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_TAREAS_LOG_Ins]
 On [Dbo].[RESERVAS_TAREAS] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Inserted)
			set @RegistroPK2 = (select IDTarea from Inserted)
			set @RegistroPK3 = (select IDCab from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_TAREAS',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion
		End  

GO

--25
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[RESERVAS_TAREAS_LOG_Upd]
 On [Dbo].[RESERVAS_TAREAS] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Inserted)
			set @RegistroPK2 = (select IDTarea from Inserted)
			set @RegistroPK3 = (select IDCab from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_TAREAS',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion
		End  
