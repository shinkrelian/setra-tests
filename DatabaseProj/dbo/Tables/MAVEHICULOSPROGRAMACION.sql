﻿CREATE TABLE [dbo].[MAVEHICULOSPROGRAMACION] (
    [NuVehiculoProg] SMALLINT     NOT NULL,
    [NoUnidadProg]   VARCHAR (50) NOT NULL,
    [QtCapacidadPax] TINYINT      NOT NULL,
    [UserMod]        CHAR (4)     NOT NULL,
    [FecMod]         DATETIME     DEFAULT (getdate()) NOT NULL,
    [FlActivo]       BIT          DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_MAVEHICULOSPROGRAMACION] PRIMARY KEY CLUSTERED ([NuVehiculoProg] ASC)
);


GO
Create Trigger [dbo].[MAVEHICULOSPROGRAMACION_LOG_Del]  
  On [dbo].[MAVEHICULOSPROGRAMACION] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuVehiculoProg from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAVEHICULOSPROGRAMACION',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAVEHICULOSPROGRAMACION_LOG_Upd]  
  On [dbo].[MAVEHICULOSPROGRAMACION] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuVehiculoProg from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAVEHICULOSPROGRAMACION',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAVEHICULOSPROGRAMACION_LOG_Ins]  
  On [dbo].[MAVEHICULOSPROGRAMACION] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuVehiculoProg from Inserted)         
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAVEHICULOSPROGRAMACION',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
