﻿CREATE TABLE [dbo].[PROCESO] (
    [CoProceso]     CHAR (3)      NOT NULL,
    [NoProceso]     VARCHAR (70)  NOT NULL,
    [TxRutaArchivo] VARCHAR (200) NULL,
    [UserMod]       CHAR (4)      NOT NULL,
    [FecMod]        SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_PROCESO] PRIMARY KEY NONCLUSTERED ([CoProceso] ASC)
);


GO
Create Trigger [dbo].[PROCESO_LOG_Ins]  
  On [dbo].[PROCESO] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoProceso from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'PROCESO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[PROCESO_LOG_Upd]  
  On [dbo].[PROCESO] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoProceso from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'PROCESO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[PROCESO_LOG_Del]  
  On [dbo].[PROCESO] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoProceso from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'PROCESO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
