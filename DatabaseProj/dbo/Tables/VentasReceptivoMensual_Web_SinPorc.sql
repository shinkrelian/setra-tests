﻿CREATE TABLE [dbo].[VentasReceptivoMensual_Web_SinPorc] (
    [FecOutPeru]          VARCHAR (6)     NULL,
    [ImporteUSD]          NUMERIC (38, 2) NULL,
    [ImporteUSDAnioAnter] NUMERIC (12, 4) NOT NULL,
    [VarImporteUSD]       NUMERIC (38, 6) NULL,
    [SumaProy]            NUMERIC (38, 2) NULL,
    [ParamAnio]           CHAR (4)        NULL
);

