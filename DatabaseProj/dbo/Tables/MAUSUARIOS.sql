﻿CREATE TABLE [dbo].[MAUSUARIOS] (
    [IDUsuario]           CHAR (4)      NOT NULL,
    [Usuario]             VARCHAR (20)  NOT NULL,
    [Nombre]              VARCHAR (60)  NOT NULL,
    [Direccion]           VARCHAR (MAX) NULL,
    [Telefono]            VARCHAR (12)  NULL,
    [Celular]             VARCHAR (12)  NULL,
    [Nivel]               CHAR (3)      NULL,
    [Password]            VARCHAR (255) NULL,
    [Siglas]              CHAR (3)      NULL,
    [FechaNac]            SMALLDATETIME NULL,
    [Activo]              CHAR (1)      CONSTRAINT [DF_MAUSUARIOS_Activo] DEFAULT ('A') NULL,
    [Directorio]          VARCHAR (50)  NULL,
    [Correo]              VARCHAR (50)  CONSTRAINT [DF_MAUSUARIOS_Correo] DEFAULT ('@') NOT NULL,
    [IdArea]              CHAR (2)      NULL,
    [IdiomaPc]            VARCHAR (12)  CONSTRAINT [DF__MAUSUARIO__Idiom__17ED6F58] DEFAULT ('ENGLISH') NOT NULL,
    [EsVendedor]          BIT           NOT NULL,
    [CambioPassword]      BIT           CONSTRAINT [DF__MAUSUARIO__Cambi__32375140] DEFAULT ((1)) NOT NULL,
    [UserMod]             CHAR (4)      NOT NULL,
    [FecMod]              DATETIME      CONSTRAINT [DF_MAUSUARIOS_fecmod] DEFAULT (getdate()) NOT NULL,
    [Profile_id]          INT           CONSTRAINT [DF_MAUSUARIOS_Profile_id] DEFAULT ((0)) NOT NULL,
    [Account_id]          INT           CONSTRAINT [DF_MAUSUARIOS_Account_id] DEFAULT ((0)) NOT NULL,
    [FlSegPersonalizada]  BIT           DEFAULT ((0)) NOT NULL,
    [IDUbigeo]            CHAR (6)      NOT NULL,
    [CoUsrAmd]            CHAR (8)      NULL,
    [CoUsrAmd2]           CHAR (8)      NULL,
    [TxApellidos]         VARCHAR (60)  NULL,
    [FecIngreso]          SMALLDATETIME NULL,
    [CoSexo]              CHAR (1)      NULL,
    [CoTipoDoc]           CHAR (3)      NULL,
    [NumIdentidad]        VARCHAR (15)  NULL,
    [CoEmpresa]           TINYINT       NULL,
    [CoCargo]             TINYINT       NULL,
    [CoUserJefe]          CHAR (4)      NULL,
    [NuAnexo]             VARCHAR (5)   NULL,
    [TxLema]              VARCHAR (MAX) NULL,
    [NoArchivoFoto]       VARCHAR (200) NULL,
    [NuOrdenArea]         TINYINT       DEFAULT ((0)) NULL,
    [FlTrabajador_Activo] BIT           DEFAULT ((1)) NOT NULL,
    [CardCodeSAP]         VARCHAR (15)  DEFAULT (NULL) NULL,
    [FlEsSupervisor]      BIT           DEFAULT ((0)) NOT NULL,
    [FlExcluirSAP]        BIT           DEFAULT ((0)) NOT NULL,
    [CoMarcador]          VARCHAR (10)  DEFAULT (NULL) NULL,
    [FecCese]             SMALLDATETIME NULL,
    [CoToken]             VARCHAR (MAX) DEFAULT (NULL) NULL,
    [FlEsStaff]           BIT           DEFAULT ((0)) NOT NULL,
    [CoUbicacionOficina]  VARCHAR (15)  DEFAULT (NULL) NULL,
    [Celular_Trabajo]     VARCHAR (12)  NULL,
    CONSTRAINT [PK_MAUSUARIOS] PRIMARY KEY CLUSTERED ([IDUsuario] ASC),
    CONSTRAINT [FK_MAUSUARIOS_MAEMPRESA] FOREIGN KEY ([CoEmpresa]) REFERENCES [dbo].[MAEMPRESA] ([CoEmpresa]),
    CONSTRAINT [FK_MAUSUARIOS_MAIDIOMA] FOREIGN KEY ([IdiomaPc]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_MAUSUARIOS_MANIVELUSUARIOS] FOREIGN KEY ([Nivel]) REFERENCES [dbo].[MANIVELUSUARIO] ([IDNivel]),
    CONSTRAINT [FK_MAUSUARIOS_MAUBIGEO] FOREIGN KEY ([IDUbigeo]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_MAUSUARIOS_MAUSUARIOS] FOREIGN KEY ([CoUserJefe]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario]),
    CONSTRAINT [FK_MAUSUARIOS_RH_AREA] FOREIGN KEY ([IdArea]) REFERENCES [dbo].[RH_AREA] ([CoArea]),
    CONSTRAINT [FK_MAUSUARIOS_RH_CARGO] FOREIGN KEY ([CoCargo]) REFERENCES [dbo].[RH_CARGO] ([CoCargo]),
    CONSTRAINT [IX_MAUSUARIOS] UNIQUE NONCLUSTERED ([Usuario] ASC)
);


GO
--150
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAUSUARIOS

CREATE Trigger [dbo].[MAUSUARIOS_LOG_Ins]
  On dbo.MAUSUARIOS for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDUsuario from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUSUARIOS',@UserMod,@RegistroPK1,'','','',@Accion			
		End

GO
--153
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAUSUARIOS_LOG_Upd]
  On dbo.MAUSUARIOS for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDUsuario from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUSUARIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--155
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAUSUARIOS_LOG_Del]
  On dbo.MAUSUARIOS for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDUsuario from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAUSUARIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End
