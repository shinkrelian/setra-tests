﻿CREATE TABLE [dbo].[RESERVAS_MENSAJESTELEF] (
    [IDReserva] INT           NOT NULL,
    [IDMensaje] TINYINT       NOT NULL,
    [Contacto]  VARCHAR (100) NOT NULL,
    [Fecha]     SMALLDATETIME NOT NULL,
    [IDUsuario] CHAR (4)      NOT NULL,
    [Asunto]    VARCHAR (200) NOT NULL,
    [Mensaje]   VARCHAR (MAX) NOT NULL,
    [UserMod]   CHAR (4)      NOT NULL,
    [FecMod]    DATETIME      CONSTRAINT [DF__RESERVAS___FecMo__34E9A0B9] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RESERVAS_MENSAJESTELEF_1] PRIMARY KEY CLUSTERED ([IDReserva] ASC, [IDMensaje] ASC)
);


GO

--31
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[RESERVAS_MENSAJESTELEF_LOG_Del]
 On [Dbo].[RESERVAS_MENSAJESTELEF] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Deleted)
			set @RegistroPK2 = (select IDMensaje from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_MENSAJESTELEF',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--30
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[RESERVAS_MENSAJESTELEF_LOG_Upd]
 On [Dbo].[RESERVAS_MENSAJESTELEF] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Inserted)
			set @RegistroPK2 = (select IDMensaje from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_MENSAJESTELEF',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--29
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_MENSAJESTELEF_LOG_Ins]
 On [Dbo].[RESERVAS_MENSAJESTELEF] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva from Inserted)
			set @RegistroPK2 = (select IDMensaje from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_MENSAJESTELEF',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
