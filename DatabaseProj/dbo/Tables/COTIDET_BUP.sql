﻿CREATE TABLE [dbo].[COTIDET_BUP] (
    [IDDET]                 INT            NOT NULL,
    [IDCAB]                 INT            NOT NULL,
    [Cotizacion]            CHAR (8)       NOT NULL,
    [Item]                  TINYINT        NOT NULL,
    [Dia]                   SMALLDATETIME  NOT NULL,
    [DiaNombre]             VARCHAR (10)   NOT NULL,
    [IDubigeo]              CHAR (6)       NOT NULL,
    [IDProveedor]           CHAR (6)       NOT NULL,
    [IDServicio]            CHAR (8)       NOT NULL,
    [IDServicio_Det]        INT            NOT NULL,
    [IDidioma]              VARCHAR (12)   NOT NULL,
    [Especial]              BIT            NOT NULL,
    [MotivoEspecial]        VARCHAR (200)  NULL,
    [RutaDocSustento]       VARCHAR (200)  NULL,
    [Desayuno]              BIT            NOT NULL,
    [Lonche]                BIT            NOT NULL,
    [Almuerzo]              BIT            NOT NULL,
    [Cena]                  BIT            NOT NULL,
    [Transfer]              BIT            NOT NULL,
    [IDDetTransferOri]      INT            NULL,
    [IDDetTransferDes]      INT            NULL,
    [CamaMat]               TINYINT        NULL,
    [TipoTransporte]        CHAR (1)       NULL,
    [IDUbigeoOri]           CHAR (6)       NOT NULL,
    [IDUbigeoDes]           CHAR (6)       NOT NULL,
    [NroPax]                SMALLINT       NOT NULL,
    [NroLiberados]          SMALLINT       NULL,
    [Tipo_Lib]              CHAR (1)       NULL,
    [CostoReal]             NUMERIC (8, 2) NOT NULL,
    [CostoRealAnt]          NUMERIC (8, 2) NULL,
    [CostoLiberado]         NUMERIC (8, 2) NOT NULL,
    [Margen]                NUMERIC (8, 2) NOT NULL,
    [MargenAplicado]        NUMERIC (6, 2) NOT NULL,
    [MargenAplicadoAnt]     NUMERIC (6, 2) NULL,
    [MargenLiberado]        NUMERIC (6, 2) NOT NULL,
    [Total]                 NUMERIC (8, 2) NOT NULL,
    [TotalOrig]             NUMERIC (8, 2) NOT NULL,
    [SSCR]                  NUMERIC (8, 2) NOT NULL,
    [SSCL]                  NUMERIC (8, 2) NOT NULL,
    [SSMargen]              NUMERIC (8, 2) NOT NULL,
    [SSMargenLiberado]      NUMERIC (8, 2) NOT NULL,
    [SSTotal]               NUMERIC (8, 2) NOT NULL,
    [SSTotalOrig]           NUMERIC (8, 2) NOT NULL,
    [STCR]                  NUMERIC (8, 2) NOT NULL,
    [STMargen]              NUMERIC (8, 2) NOT NULL,
    [STTotal]               NUMERIC (8, 2) NOT NULL,
    [STTotalOrig]           NUMERIC (8, 2) NOT NULL,
    [CostoRealImpto]        NUMERIC (8, 2) NOT NULL,
    [CostoLiberadoImpto]    NUMERIC (8, 2) NOT NULL,
    [MargenImpto]           NUMERIC (8, 2) NOT NULL,
    [MargenLiberadoImpto]   NUMERIC (6, 2) NOT NULL,
    [SSCRImpto]             NUMERIC (8, 2) NOT NULL,
    [SSCLImpto]             NUMERIC (8, 2) NOT NULL,
    [SSMargenImpto]         NUMERIC (8, 2) NOT NULL,
    [SSMargenLiberadoImpto] NUMERIC (8, 2) NOT NULL,
    [STCRImpto]             NUMERIC (8, 2) NOT NULL,
    [STMargenImpto]         NUMERIC (8, 2) NOT NULL,
    [TotImpto]              NUMERIC (8, 2) NOT NULL,
    [IDDetRel]              INT            NULL,
    [Servicio]              VARCHAR (MAX)  NULL,
    [IDDetCopia]            INT            NULL,
    [Recalcular]            BIT            CONSTRAINT [DF_COTIDET_BUP_Recalcular] DEFAULT ((0)) NOT NULL,
    [PorReserva]            BIT            CONSTRAINT [DF_COTIDET_BUP_PorReserva] DEFAULT ((0)) NOT NULL,
    [ExecTrigger]           BIT            CONSTRAINT [DF_COTIDET_BUP_ExecTrigger] DEFAULT ((1)) NOT NULL,
    [UserMod]               CHAR (4)       NOT NULL,
    [FecMod]                DATETIME       CONSTRAINT [DF__COTIDET_B__FecMo__7330EBF5] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTIDET_BUP] PRIMARY KEY NONCLUSTERED ([IDDET] ASC)
);


GO

--116
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_BUP_LOG_Ins]
  On [dbo].[COTIDET_BUP] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_BUP',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--119
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_BUP_LOG_Upd]
  On [dbo].[COTIDET_BUP] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_BUP',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--122
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTIDET_BUP_LOG_Del]
  On [dbo].[COTIDET_BUP] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_BUP',@UserMod,@RegistroPK1,'','','',@Accion
		End 
