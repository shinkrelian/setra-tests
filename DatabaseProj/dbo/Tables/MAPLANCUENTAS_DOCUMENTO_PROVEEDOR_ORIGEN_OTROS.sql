﻿CREATE TABLE [dbo].[MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_ORIGEN_OTROS] (
    [CtaContable] VARCHAR (14) NOT NULL,
    [FlFile]      BIT          NOT NULL,
    [FlCoCecos]   BIT          NOT NULL,
    [FlCoCecon]   BIT          NOT NULL,
    [IdFile]      CHAR (8)     NOT NULL,
    [CoCecos]     CHAR (6)     NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF_MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_ORIGEN_OTROS_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_ORIGEN_OTROS] PRIMARY KEY CLUSTERED ([CtaContable] ASC, [FlFile] ASC, [FlCoCecos] ASC, [FlCoCecon] ASC, [IdFile] ASC, [CoCecos] ASC)
);

