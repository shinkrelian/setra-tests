﻿CREATE TABLE [dbo].[MATIPOUBIG] (
    [Descripcion] VARCHAR (20) NOT NULL,
    [UserMod]     CHAR (4)     NULL,
    [FecMod]      DATETIME     NULL,
    CONSTRAINT [PK_MATIPOUBIG] PRIMARY KEY CLUSTERED ([Descripcion] ASC)
);


GO

--158
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MATIPOUBIG

CREATE Trigger [dbo].[MATIPOUBIG_LOG_Ins]
  On [dbo].[MATIPOUBIG] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select Descripcion from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOUBIG',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--161
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOUBIG_LOG_Del]
  On [dbo].[MATIPOUBIG] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select Descripcion from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOUBIG',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--160
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOUBIG_LOG_Upd]
  On [dbo].[MATIPOUBIG] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select Descripcion from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOUBIG',@UserMod,@RegistroPK1,'','','',@Accion
		End  
