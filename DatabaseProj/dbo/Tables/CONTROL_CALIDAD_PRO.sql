﻿CREATE TABLE [dbo].[CONTROL_CALIDAD_PRO] (
    [NuControlCalidad] INT           NOT NULL,
    [IDCab]            INT           NULL,
    [UserMod]          CHAR (4)      NOT NULL,
    [FecMod]           SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [TxComentario]     TEXT          NULL,
    [IDPax]            INT           NULL,
    [CoProveedor]      CHAR (6)      NULL,
    [CoCliente]        CHAR (6)      NULL,
    CONSTRAINT [PK_CONTROL_CALIDAD_PRO] PRIMARY KEY CLUSTERED ([NuControlCalidad] ASC),
    CONSTRAINT [FK_CONTROL_CALIDAD_PRO_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_CONTROL_CALIDAD_PRO_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax])
);

