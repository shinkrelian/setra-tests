﻿CREATE TABLE [dbo].[TICKET_AMADEUS_ITINERARIO] (
    [CoTicket]        CHAR (13)     NOT NULL,
    [NuSegmento]      TINYINT       NOT NULL,
    [CoCiudadPar]     CHAR (3)      NOT NULL,
    [CoLineaAerea]    CHAR (2)      NOT NULL,
    [NuVuelo]         CHAR (4)      NOT NULL,
    [CoSegmento]      CHAR (1)      NOT NULL,
    [CoBooking]       CHAR (1)      NOT NULL,
    [TxFechaPar]      SMALLDATETIME NOT NULL,
    [TxBaseTarifa]    CHAR (6)      NOT NULL,
    [TxBag]           CHAR (3)      NOT NULL,
    [CoST]            CHAR (2)      NOT NULL,
    [CoCiudadLle]     CHAR (3)      NOT NULL,
    [TxHoraLle]       SMALLDATETIME NOT NULL,
    [UserMod]         CHAR (4)      NOT NULL,
    [FecMod]          SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [CoPNR_Amadeus]   CHAR (6)      NOT NULL,
    [CoPNR_Aerolinea] VARCHAR (20)  NOT NULL,
    CONSTRAINT [PK_TICKET_AMADEUS_ITINERARIO] PRIMARY KEY NONCLUSTERED ([CoTicket] ASC, [NuSegmento] ASC),
    CONSTRAINT [FK_TICKET_AMADEUS_ITINERARIO_TICKET_AMADEUS] FOREIGN KEY ([CoTicket]) REFERENCES [dbo].[TICKET_AMADEUS] ([CoTicket])
);


GO
Create Trigger [dbo].[TICKET_AMADEUS_ITINERARIO_LOG_Del]  
  On [dbo].[TICKET_AMADEUS_ITINERARIO] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Deleted)  
   set @RegistroPK2 = (select NuSegmento from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS_ITINERARIO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_AMADEUS_ITINERARIO_LOG_Upd]  
  On [dbo].[TICKET_AMADEUS_ITINERARIO] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)      
   set @RegistroPK2 = (select NuSegmento from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS_ITINERARIO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_AMADEUS_ITINERARIO_LOG_Ins]  
  On [dbo].[TICKET_AMADEUS_ITINERARIO] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)        
   set @RegistroPK2 = (select NuSegmento from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS_ITINERARIO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
