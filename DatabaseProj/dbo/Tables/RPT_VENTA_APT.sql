﻿CREATE TABLE [dbo].[RPT_VENTA_APT] (
    [NuAnio]                     CHAR (4)        NOT NULL,
    [CoMes]                      CHAR (2)        NOT NULL,
    [SsImporte]                  NUMERIC (12, 4) NULL,
    [SsImporte_Sin_APT]          NUMERIC (12, 4) NULL,
    [QtPax]                      INT             NULL,
    [QtNumFiles]                 INT             NULL,
    [UserMod]                    CHAR (4)        NULL,
    [FecMod]                     DATETIME        CONSTRAINT [DF_RPT_VENTA_APT_FecMod] DEFAULT (getdate()) NULL,
    [QtDias]                     INT             NULL,
    [QtEdad_Prom]                INT             NULL,
    [SsImporte_Sin_Porc]         NUMERIC (12, 4) NULL,
    [SsImporte_Sin_Porc_Sin_APT] NUMERIC (12, 4) NULL,
    [SsImporte_Web]              NUMERIC (12, 4) NULL,
    [SsImporte_Sin_Porc_Web]     NUMERIC (12, 4) NULL,
    CONSTRAINT [PK_RPT_VENTA_APT] PRIMARY KEY NONCLUSTERED ([NuAnio] ASC, [CoMes] ASC)
);

