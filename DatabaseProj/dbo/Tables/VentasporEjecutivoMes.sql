﻿CREATE TABLE [dbo].[VentasporEjecutivoMes] (
    [IDUsuario]     CHAR (4)        NOT NULL,
    [Nombre]        VARCHAR (60)    NOT NULL,
    [FecOutPeru]    VARCHAR (6)     NULL,
    [CantFiles]     INT             NULL,
    [VentaAnualUSD] NUMERIC (38, 6) NULL,
    [Prom_Margen]   NUMERIC (38, 6) NOT NULL,
    [ParamAnio]     CHAR (4)        NULL
);

