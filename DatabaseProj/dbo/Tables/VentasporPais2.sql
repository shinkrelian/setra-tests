﻿CREATE TABLE [dbo].[VentasporPais2] (
    [Posicion]      BIGINT           NULL,
    [CoPais]        CHAR (6)         NULL,
    [NoPais]        VARCHAR (60)     NULL,
    [CoRegion]      CHAR (2)         NULL,
    [Total]         NUMERIC (38, 6)  NULL,
    [Dias_Dif]      INT              NULL,
    [Edad_Prom]     INT              NOT NULL,
    [Cant_Clientes] INT              NULL,
    [ParamAnio]     CHAR (4)         NULL,
    [TicketFile]    NUMERIC (23, 15) NULL
);

