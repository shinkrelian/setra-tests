﻿CREATE TABLE [dbo].[MATIPODETRACCION] (
    [CoTipoDetraccion] CHAR (5)       NOT NULL,
    [NoTipoBien]       VARCHAR (100)  NOT NULL,
    [TxDescripcion]    VARCHAR (MAX)  NULL,
    [SsTasa]           NUMERIC (5, 4) NOT NULL,
    [SsMayor]          NUMERIC (8, 2) NOT NULL,
    [flActivo]         BIT            CONSTRAINT [DF_MATIPODETRACCION_flActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]          CHAR (4)       NOT NULL,
    [FecMod]           DATETIME       CONSTRAINT [DF_MATIPODETRACCION_FecMod] DEFAULT (getdate()) NOT NULL,
    [CoSAP]            NVARCHAR (4)   NULL,
    CONSTRAINT [PK_TIPODETRACCION] PRIMARY KEY NONCLUSTERED ([CoTipoDetraccion] ASC)
);

