﻿CREATE TABLE [dbo].[TICKET_AMADEUS_TAX] (
    [CoTicket] CHAR (13)      NOT NULL,
    [NuTax]    TINYINT        NOT NULL,
    [CoTax]    CHAR (5)       NOT NULL,
    [SsMonto]  NUMERIC (6, 2) NOT NULL,
    [UserMod]  CHAR (4)       NOT NULL,
    [FecMod]   SMALLDATETIME  DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_TICKET_AMADEUS_TAX] PRIMARY KEY NONCLUSTERED ([CoTicket] ASC, [NuTax] ASC),
    CONSTRAINT [FK_TICKET_AMADEUS_TAX_TICKET_AMADEUS] FOREIGN KEY ([CoTicket]) REFERENCES [dbo].[TICKET_AMADEUS] ([CoTicket])
);


GO
Create Trigger [dbo].[TICKET_AMADEUS_TAX_LOG_Ins]  
  On [dbo].[TICKET_AMADEUS_TAX] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)        
   set @RegistroPK2 = (select NuTax from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS_TAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_AMADEUS_TAX_LOG_Del]  
  On [dbo].[TICKET_AMADEUS_TAX] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Deleted)  
   set @RegistroPK2 = (select NuTax from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS_TAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[TICKET_AMADEUS_TAX_LOG_Upd]  
  On [dbo].[TICKET_AMADEUS_TAX] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoTicket from Inserted)      
   set @RegistroPK2 = (select NuTax from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'TICKET_AMADEUS_TAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
