﻿CREATE TABLE [dbo].[ORDENPAGO] (
    [IDOrdPag]               INT             NOT NULL,
    [IDReserva]              INT             NOT NULL,
    [IDCab]                  INT             NOT NULL,
    [IDFormaPago]            CHAR (3)        NOT NULL,
    [IDBanco]                CHAR (3)        NOT NULL,
    [FechaPago]              SMALLDATETIME   NOT NULL,
    [FechaEnvio]             SMALLDATETIME   NULL,
    [Porcentaje]             NUMERIC (5, 2)  NOT NULL,
    [TCambio]                NUMERIC (5, 2)  NULL,
    [Observaciones]          VARCHAR (MAX)   NULL,
    [CtaCte]                 VARCHAR (30)    NULL,
    [IDMoneda]               CHAR (3)        NOT NULL,
    [IDEstado]               CHAR (2)        NOT NULL,
    [UserMod]                CHAR (4)        NOT NULL,
    [FecMod]                 DATETIME        CONSTRAINT [DF__ORDENPAGO__FecMo__47477CBF] DEFAULT (getdate()) NOT NULL,
    [FechaEmision]           SMALLDATETIME   DEFAULT (getdate()) NOT NULL,
    [Numeroformapago]        VARCHAR (20)    DEFAULT (NULL) NULL,
    [CoEstado]               CHAR (2)        DEFAULT ('PD') NOT NULL,
    [SsSaldo]                NUMERIC (12, 2) NULL,
    [SsDifAceptada]          NUMERIC (8, 2)  NULL,
    [TxObsDocumento]         VARCHAR (MAX)   NULL,
    [SsMontoTotal]           NUMERIC (12, 2) NULL,
    [UserNuevo]              CHAR (4)        NULL,
    [SSDetraccion]           NUMERIC (12, 2) NULL,
    [CoSap]                  VARCHAR (15)    NULL,
    [FlEnviado]              BIT             DEFAULT ((0)) NOT NULL,
    [SsMontoTotal_SinDetrac] NUMERIC (12, 2) NULL,
    [SsMontoPagadoEnSAP]     NUMERIC (12, 4) NULL,
    [CoResponsable]          CHAR (4)        NULL,
    [FlEnviadoSupResv]       BIT             DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_ORDENPAGO] PRIMARY KEY NONCLUSTERED ([IDOrdPag] ASC),
    CONSTRAINT [FK_ORDENPAGO_MABANCOS] FOREIGN KEY ([IDBanco]) REFERENCES [dbo].[MABANCOS] ([IDBanco]),
    CONSTRAINT [FK_ORDENPAGO_MAFORMASPAGO] FOREIGN KEY ([IDFormaPago]) REFERENCES [dbo].[MAFORMASPAGO] ([IdFor]),
    CONSTRAINT [FK_ORDENPAGO_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_ORDENPAGO_RESERVAS] FOREIGN KEY ([IDReserva]) REFERENCES [dbo].[RESERVAS] ([IDReserva])
);


GO

--241
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ORDENPAGO_LOG_Del]
 On [dbo].[ORDENPAGO] for Delete
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from deleted) = 1
		Begin
			set @RegistroPK1 = (Select IDOrdPag from Deleted)
			set @UserMod = (Select UserMod from Deleted)
			set @Accion = 'D'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ORDENPAGO',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO


--240
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ORDENPAGO_LOG_Upd]
 On [dbo].[ORDENPAGO] for Update
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDOrdPag from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ORDENPAGO',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO


--239
--Text 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[ORDENPAGO_LOG_Ins]
 On [dbo].[ORDENPAGO] for Insert
As
	Set NoCount On
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4)
	Declare @Accion char(1)
	If (select count(*) from Inserted) = 1
		Begin
			set @RegistroPK1 = (Select IDOrdPag from Inserted)
			set @UserMod = (Select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'ORDENPAGO',@UserMod,@RegistroPK1,'','','',@Accion
		End
