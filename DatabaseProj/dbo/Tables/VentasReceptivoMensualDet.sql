﻿CREATE TABLE [dbo].[VentasReceptivoMensualDet] (
    [FecInicio]        SMALLDATETIME   NOT NULL,
    [FecOut]           SMALLDATETIME   NOT NULL,
    [FecOutPeru]       VARCHAR (6)     NULL,
    [IDCAB]            INT             NOT NULL,
    [IDFile]           CHAR (8)        NULL,
    [Titulo]           VARCHAR (100)   NOT NULL,
    [ValorVenta]       NUMERIC (38, 2) NOT NULL,
    [PoConcretVta]     NUMERIC (5, 2)  NULL,
    [NroPax]           SMALLINT        NULL,
    [ESTADO]           CHAR (1)        NOT NULL,
    [IDUsuario]        CHAR (4)        NOT NULL,
    [Responsable]      VARCHAR (20)    NULL,
    [IDCliente]        CHAR (6)        NOT NULL,
    [CoPais]           CHAR (6)        NULL,
    [DescPaisCliente]  VARCHAR (60)    NULL,
    [DescCliente]      VARCHAR (80)    NOT NULL,
    [MargenGan]        NUMERIC (38, 6) NULL,
    [Utilidad]         NUMERIC (38, 2) NULL,
    [Total_Proy]       NUMERIC (38, 2) NULL,
    [Total_Concret]    NUMERIC (38, 2) NOT NULL,
    [SumaProy]         NUMERIC (38, 6) NULL,
    [SumaProy2]        NUMERIC (38, 2) NOT NULL,
    [Dias]             INT             NULL,
    [Edad_Prom]        INT             NOT NULL,
    [Total_Programado] NUMERIC (38, 4) NULL,
    [ParamAnio]        CHAR (4)        NULL,
    [TxCategoria]      CHAR (10)       NULL
);


GO
CREATE NONCLUSTERED INDEX [IX_VentasReceptivoMensualDet]
    ON [dbo].[VentasReceptivoMensualDet]([ParamAnio] ASC)
    INCLUDE([FecOutPeru], [IDUsuario], [MargenGan], [SumaProy]);


GO
CREATE NONCLUSTERED INDEX [VentasReceptivoMensualDet]
    ON [dbo].[VentasReceptivoMensualDet]([ParamAnio] ASC)
    INCLUDE([FecOutPeru], [IDCliente], [CoPais], [DescPaisCliente], [DescCliente], [SumaProy]);


GO
CREATE NONCLUSTERED INDEX [IX_VentasReceptivoMensualDet_2]
    ON [dbo].[VentasReceptivoMensualDet]([ParamAnio] ASC)
    INCLUDE([IDCliente], [CoPais]);


GO
CREATE NONCLUSTERED INDEX [IX_VentasReceptivoMensualDet_3]
    ON [dbo].[VentasReceptivoMensualDet]([ParamAnio] ASC)
    INCLUDE([CoPais], [DescPaisCliente], [SumaProy], [Dias], [Edad_Prom]);

