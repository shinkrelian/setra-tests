﻿CREATE TABLE [dbo].[VentasDebitMemoPendientes] (
    [IDFile]      CHAR (8)       NULL,
    [DescCliente] VARCHAR (80)   NULL,
    [Ejecutivo]   VARCHAR (80)   NULL,
    [Total]       NUMERIC (8, 2) NULL,
    [Descripcion] VARCHAR (MAX)  NULL,
    [Supervisor]  VARCHAR (120)  NULL
);

