﻿CREATE TABLE [dbo].[MAESPECIALIDAD] (
    [IDEspecialidad] CHAR (2)      NOT NULL,
    [Descripcion]    VARCHAR (150) NOT NULL,
    [UserMod]        CHAR (4)      NOT NULL,
    [FecMod]         DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAESPECIALIDAD] PRIMARY KEY CLUSTERED ([IDEspecialidad] ASC)
);


GO
Create Trigger [dbo].[MAESPECIALIDAD_LOG_Upd]  
  On [dbo].[MAESPECIALIDAD] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDEspecialidad from Inserted)   
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAESPECIALIDAD',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAESPECIALIDAD_LOG_Del]  
  On [dbo].[MAESPECIALIDAD] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDEspecialidad from Deleted)    
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAESPECIALIDAD',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MAESPECIALIDAD_LOG_Ins]  
  On [dbo].[MAESPECIALIDAD] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDEspecialidad from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAESPECIALIDAD',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
