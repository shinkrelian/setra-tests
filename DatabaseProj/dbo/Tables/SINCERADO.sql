﻿CREATE TABLE [dbo].[SINCERADO] (
    [NuSincerado]     INT           NOT NULL,
    [IDOperacion_Det] INT           NOT NULL,
    [IDServicio_Det]  INT           NOT NULL,
    [UserMod]         CHAR (4)      NOT NULL,
    [FecMod]          SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_SINCERADO] PRIMARY KEY CLUSTERED ([NuSincerado] ASC)
);

