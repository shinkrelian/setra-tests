﻿CREATE TABLE [dbo].[MASERVICIOS_DET_COSTOS_TEMP_DUP2] (
    [Correlativo]    TINYINT        NOT NULL,
    [IDServicio_Det] INT            NOT NULL,
    [PaxDesde]       SMALLINT       NOT NULL,
    [PaxHasta]       SMALLINT       NOT NULL,
    [Monto]          NUMERIC (8, 2) NOT NULL,
    [Mig]            BIT            NOT NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       NOT NULL,
    [Borrado]        BIT            DEFAULT ((0)) NULL,
    CONSTRAINT [PK_MASERVICIOS_DET_COSTOS_TEMP_DUP2] PRIMARY KEY NONCLUSTERED ([Correlativo] ASC, [IDServicio_Det] ASC)
);

