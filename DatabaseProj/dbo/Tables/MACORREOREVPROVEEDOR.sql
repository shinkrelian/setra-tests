﻿CREATE TABLE [dbo].[MACORREOREVPROVEEDOR] (
    [IDProveedor] CHAR (6)      NOT NULL,
    [Correo]      VARCHAR (150) NOT NULL,
    [Descripcion] VARCHAR (MAX) NULL,
    [Estado]      CHAR (1)      DEFAULT ('A') NULL,
    [UserMod]     CHAR (4)      NULL,
    [FecMod]      DATETIME      DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_MACORREOREVPROVEEDOR] PRIMARY KEY CLUSTERED ([IDProveedor] ASC, [Correo] ASC),
    CONSTRAINT [FK_MACORREOREVPROVEEDOR_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO


--210
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MACORREOREVPROVEEDOR

CREATE Trigger [dbo].[MACORREOREVPROVEEDOR_LOG_Ins]
  On [dbo].[MACORREOREVPROVEEDOR] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDProveedor from Inserted)
			set @RegistroPK2 = (select Correo from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACORREOREVPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--213
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACORREOREVPROVEEDOR_LOG_Upd]
  On [dbo].[MACORREOREVPROVEEDOR] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDProveedor from Inserted)
			set @RegistroPK2 = (select Correo from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACORREOREVPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO
	

--216
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACORREOREVPROVEEDOR_LOG_Del]
  On [dbo].[MACORREOREVPROVEEDOR] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDProveedor from Deleted)
			set @RegistroPK2 = (select Correo from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACORREOREVPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
