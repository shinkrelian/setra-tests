﻿CREATE TABLE [dbo].[MAESPECIALIDADPROVEEDOR] (
    [IDEspecialidad] CHAR (2) NOT NULL,
    [IDProveedor]    CHAR (6) NOT NULL,
    [UserMod]        CHAR (4) NOT NULL,
    [FecMod]         DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAESPECIALIDADPROVEEDOR] PRIMARY KEY CLUSTERED ([IDEspecialidad] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_MAESPECIALIDADPROVEEDOR] FOREIGN KEY ([IDEspecialidad]) REFERENCES [dbo].[MAESPECIALIDAD] ([IDEspecialidad]),
    CONSTRAINT [FK_MAESPECIALIDADPROVEEDOR2] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO
Create Trigger [dbo].[MAESPECIALIDADPROVEEDOR_LOG_Ins]  
  On [dbo].[MAESPECIALIDADPROVEEDOR] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDEspecialidad from Inserted)     
   set @RegistroPK2 = (select IDProveedor from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAESPECIALIDADPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MAESPECIALIDADPROVEEDOR_LOG_Del]  
  On [dbo].[MAESPECIALIDADPROVEEDOR] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDEspecialidad from Deleted)   
   set @RegistroPK2 = (select IDProveedor from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAESPECIALIDADPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[MAESPECIALIDADPROVEEDOR_LOG_Upd]  
  On [dbo].[MAESPECIALIDADPROVEEDOR] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDEspecialidad from Inserted)     
   set @RegistroPK2 = (select IDProveedor from Inserted)     
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MAESPECIALIDADPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
