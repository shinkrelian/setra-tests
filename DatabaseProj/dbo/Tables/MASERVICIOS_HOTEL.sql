﻿CREATE TABLE [dbo].[MASERVICIOS_HOTEL] (
    [CoServicio]     CHAR (8)     NOT NULL,
    [CoConcepto]     TINYINT      NOT NULL,
    [FlServicio]     BIT          NULL,
    [FlCosto]        BIT          NULL,
    [CoTipoMenu]     CHAR (3)     NULL,
    [FlFijoPortable] BIT          NULL,
    [CoTipoBanio]    CHAR (3)     NULL,
    [CoEstadoHab]    CHAR (3)     NULL,
    [FlActivo]       BIT          CONSTRAINT [DF_MASERVICIOS_HOTEL_FlActivo] DEFAULT ((1)) NULL,
    [UserMod]        CHAR (4)     NULL,
    [FecMod]         DATETIME     CONSTRAINT [DF_MASERVICIOS_HOTEL_FecMod] DEFAULT (getdate()) NULL,
    [FlPortable]     BIT          NULL,
    [TxPisos]        VARCHAR (20) NULL,
    CONSTRAINT [PK_MASERVICIOS_HOTEL] PRIMARY KEY CLUSTERED ([CoServicio] ASC, [CoConcepto] ASC),
    CONSTRAINT [FK_MASERVICIOS_HOTEL_MACONCEPTO_HOTEL] FOREIGN KEY ([CoConcepto]) REFERENCES [dbo].[MACONCEPTO_HOTEL] ([CoConcepto]),
    CONSTRAINT [FK_MASERVICIOS_HOTEL_MASERVICIOS] FOREIGN KEY ([CoServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio])
);

