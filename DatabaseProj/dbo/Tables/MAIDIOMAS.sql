﻿CREATE TABLE [dbo].[MAIDIOMAS] (
    [IDidioma]        VARCHAR (12) NOT NULL,
    [Cultura]         VARCHAR (50) NULL,
    [PorDefault]      BIT          CONSTRAINT [DF_MAIDIOMAS_PorDefault] DEFAULT ((0)) NOT NULL,
    [Siglas]          CHAR (3)     NOT NULL,
    [Activo]          CHAR (1)     CONSTRAINT [DF_MAIDIOMAS_Activo] DEFAULT ('A') NOT NULL,
    [UserMod]         CHAR (4)     NULL,
    [FecMod]          DATETIME     CONSTRAINT [DF_MAIDIOMAS_FecMod] DEFAULT (getdate()) NULL,
    [FlReqEntrevista] BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MAIDIOMAS] PRIMARY KEY CLUSTERED ([IDidioma] ASC),
    CONSTRAINT [IX_MAIDIOMAS_Siglas] UNIQUE NONCLUSTERED ([Siglas] ASC)
);


GO

--144
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAIDIOMAS_LOG_Upd]
  On dbo.MAIDIOMAS for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDidioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAIDIOMAS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--148
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAIDIOMAS_LOG_Del]
  On dbo.MAIDIOMAS for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDidioma from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAIDIOMAS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--140
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MAIDIOMAS

CREATE Trigger [dbo].[MAIDIOMAS_LOG_Ins]
  On dbo.MAIDIOMAS for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDidioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAIDIOMAS',@UserMod,@RegistroPK1,'','','',@Accion
		End  
