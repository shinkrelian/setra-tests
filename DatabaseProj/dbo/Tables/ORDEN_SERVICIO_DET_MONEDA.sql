﻿CREATE TABLE [dbo].[ORDEN_SERVICIO_DET_MONEDA] (
    [NuOrden_Servicio] INT             NOT NULL,
    [CoMoneda]         CHAR (3)        NOT NULL,
    [CoEstado]         CHAR (2)        DEFAULT ('PD') NULL,
    [SsMontoTotal]     NUMERIC (14, 4) DEFAULT (NULL) NULL,
    [SsSaldo]          NUMERIC (14, 4) NULL,
    [SsDifAceptada]    NUMERIC (14, 4) NULL,
    [UserMod]          CHAR (4)        NOT NULL,
    [FecMod]           DATETIME        DEFAULT (getdate()) NOT NULL,
    [TxObsDocumento]   VARCHAR (MAX)   NULL,
    CONSTRAINT [PK_ORDEN_SERVICIO_DET_MONEDA] PRIMARY KEY NONCLUSTERED ([NuOrden_Servicio] ASC, [CoMoneda] ASC),
    CONSTRAINT [FK_ORDEN_SERVICIO_DET_MONEDA_ESTADO_OBLIGACIONESPAGO] FOREIGN KEY ([CoEstado]) REFERENCES [dbo].[ESTADO_OBLIGACIONESPAGO] ([CoEstado])
);

