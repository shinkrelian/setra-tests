﻿CREATE TABLE [dbo].[MACONTACTOSCLIENTE] (
    [IDContacto]        CHAR (3)      NOT NULL,
    [IDCliente]         CHAR (6)      NOT NULL,
    [Titulo]            VARCHAR (5)   NOT NULL,
    [Nombres]           VARCHAR (80)  NOT NULL,
    [Apellidos]         VARCHAR (80)  NOT NULL,
    [Cargo]             VARCHAR (100) NULL,
    [Telefono]          VARCHAR (30)  NULL,
    [Fax]               VARCHAR (30)  NULL,
    [Celular]           VARCHAR (30)  NULL,
    [Email]             VARCHAR (200) NULL,
    [Email2]            VARCHAR (200) NULL,
    [Email3]            VARCHAR (200) NULL,
    [Anexo]             VARCHAR (15)  NULL,
    [UsuarioLogeo]      VARCHAR (80)  NULL,
    [PasswordLogeo]     VARCHAR (MAX) NULL,
    [FechaPassword]     DATETIME      NULL,
    [EsAdminist]        BIT           NOT NULL,
    [Notas]             VARCHAR (MAX) NULL,
    [UserMod]           CHAR (4)      NOT NULL,
    [FecMod]            DATETIME      CONSTRAINT [DF__MACONTACT__FecMo__6BAEFA67] DEFAULT (getdate()) NOT NULL,
    [FlAccesoWeb]       BIT           DEFAULT ((0)) NOT NULL,
    [CoToken]           VARCHAR (MAX) DEFAULT (NULL) NULL,
    [UsuarioLogeo_Hash] VARCHAR (MAX) NULL,
    [FechaHash]         DATETIME      NULL,
    CONSTRAINT [PK_MACONTACTOSCLIENTE] PRIMARY KEY NONCLUSTERED ([IDContacto] ASC, [IDCliente] ASC),
    CONSTRAINT [FK_MACONTACTOSCLIENTE_MACLIENTES] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[MACLIENTES] ([IDCliente])
);


GO
--194
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MACONTACTOSCLIENTE

CREATE Trigger [dbo].[MACONTACTOSCLIENTE_LOG_Ins]
  On dbo.MACONTACTOSCLIENTE for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDContacto from Inserted)
			set @RegistroPK2 = (select IDCliente from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACONTACTOSCLIENTE',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO
--196
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACONTACTOSCLIENTE_LOG_Upd]
  On dbo.MACONTACTOSCLIENTE for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDContacto from Inserted)
			set @RegistroPK2 = (select IDCliente from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACONTACTOSCLIENTE',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End

GO
--199
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACONTACTOSCLIENTE_LOG_Del]
  On dbo.MACONTACTOSCLIENTE for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDContacto from Deleted)
			set @RegistroPK2 = (select IDCliente from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACONTACTOSCLIENTE',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End
