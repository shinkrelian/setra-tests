﻿CREATE TABLE [dbo].[RESERVAS_DET_PAX] (
    [IDReserva_Det] INT      NOT NULL,
    [IDPax]         INT      NOT NULL,
    [UserMod]       CHAR (4) NOT NULL,
    [FecMod]        DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_RESERVAS_DET_PAX] PRIMARY KEY NONCLUSTERED ([IDPax] ASC, [IDReserva_Det] ASC),
    CONSTRAINT [FK_RESERVAS_DET_PAX_COTIPAX] FOREIGN KEY ([IDPax]) REFERENCES [dbo].[COTIPAX] ([IDPax]),
    CONSTRAINT [FK_RESERVAS_DET_PAX_RESERVAS_DET] FOREIGN KEY ([IDReserva_Det]) REFERENCES [dbo].[RESERVAS_DET] ([IDReserva_Det])
);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS_DET_PAX_IDReserva_Det]
    ON [dbo].[RESERVAS_DET_PAX]([IDReserva_Det] ASC);


GO

--47
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[RESERVAS_DET_PAX_LOG_Upd]
 On [Dbo].[RESERVAS_DET_PAX] for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva_Det from Inserted)
			set @RegistroPK2 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--45
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[RESERVAS_DET_PAX_LOG_Ins]
 On [Dbo].[RESERVAS_DET_PAX] for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDReserva_Det from Inserted)
			set @RegistroPK2 = (select IDPax from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--49
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[RESERVAS_DET_PAX_LOG_Del]
 On [Dbo].[RESERVAS_DET_PAX] for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDReserva_Det from Deleted)
			set @RegistroPK2 = (select IDPax from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_PAX',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
