﻿CREATE TABLE [dbo].[COTICAB_HISTORIAL_PVW] (
    [IDCab]             INT             NOT NULL,
    [NuUpd]             SMALLINT        NOT NULL,
    [CoAccion]          CHAR (1)        NOT NULL,
    [IDFile]            CHAR (8)        NULL,
    [FechaOut]          SMALLDATETIME   NOT NULL,
    [Estado]            CHAR (1)        NOT NULL,
    [PoConcretVta]      NUMERIC (5, 2)  NULL,
    [Margen]            NUMERIC (8, 2)  NULL,
    [NroPax]            SMALLINT        NOT NULL,
    [NroLiberados]      SMALLINT        NULL,
    [Total]             NUMERIC (11, 2) NOT NULL,
    [SsValorProyectado] NUMERIC (11, 2) NOT NULL,
    [FlInicial]         BIT             DEFAULT ((0)) NOT NULL,
    [UserMod]           CHAR (4)        NOT NULL,
    [NoEquipo]          VARCHAR (20)    DEFAULT (host_name()) NOT NULL,
    [FecMod]            DATETIME        DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTICAB_HISTORIAL_PVW] PRIMARY KEY CLUSTERED ([IDCab] ASC, [NuUpd] ASC)
);

