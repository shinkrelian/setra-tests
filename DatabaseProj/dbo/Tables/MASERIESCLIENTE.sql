﻿CREATE TABLE [dbo].[MASERIESCLIENTE] (
    [NuSerie]          INT            NOT NULL,
    [IDCliente]        CHAR (6)       NOT NULL,
    [NoSerie]          VARCHAR (MAX)  NOT NULL,
    [PoConcretizacion] NUMERIC (8, 2) NOT NULL,
    [UserMod]          CHAR (4)       NOT NULL,
    [FecMod]           SMALLDATETIME  DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MASERIESCLIENTE] PRIMARY KEY CLUSTERED ([NuSerie] ASC),
    CONSTRAINT [FK_MASERIESCLIENTE_MACLIENTES] FOREIGN KEY ([IDCliente]) REFERENCES [dbo].[MACLIENTES] ([IDCliente])
);


GO
Create Trigger [dbo].[MASERIESCLIENTE_LOG_Del]  
  On [dbo].[MASERIESCLIENTE] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuSerie from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MASERIESCLIENTE',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MASERIESCLIENTE_LOG_Ins]  
  On [dbo].[MASERIESCLIENTE] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuSerie from Inserted)         
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MASERIESCLIENTE',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[MASERIESCLIENTE_LOG_Upd]  
  On [dbo].[MASERIESCLIENTE] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuSerie from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MASERIESCLIENTE',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
