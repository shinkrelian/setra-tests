﻿CREATE TABLE [dbo].[CAPACIDAD_HABITAC] (
    [CoCapacidad]   CHAR (2)     NOT NULL,
    [NoCapacidad]   VARCHAR (50) NOT NULL,
    [QtCapacidad]   TINYINT      NOT NULL,
    [UserMod]       CHAR (4)     NOT NULL,
    [FecMod]        DATETIME     DEFAULT (getdate()) NOT NULL,
    [FlMatrimonial] BIT          DEFAULT ((0)) NOT NULL,
    [FlGuide]       BIT          DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_CAPACIDAD_HABITAC] PRIMARY KEY NONCLUSTERED ([CoCapacidad] ASC)
);


GO
Create Trigger [dbo].[CAPACIDAD_HABITAC_LOG_Del]  
  On [dbo].[CAPACIDAD_HABITAC] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoCapacidad from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'CAPACIDAD_HABITAC',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[CAPACIDAD_HABITAC_LOG_Upd]  
  On [dbo].[CAPACIDAD_HABITAC] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoCapacidad from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'CAPACIDAD_HABITAC',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[CAPACIDAD_HABITAC_LOG_Ins]  
  On [dbo].[CAPACIDAD_HABITAC] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select CoCapacidad from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'CAPACIDAD_HABITAC',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
