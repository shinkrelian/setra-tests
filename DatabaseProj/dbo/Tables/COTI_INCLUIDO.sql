﻿CREATE TABLE [dbo].[COTI_INCLUIDO] (
    [IDCAB]      INT      NOT NULL,
    [IDIncluido] INT      NOT NULL,
    [UserMod]    CHAR (4) NOT NULL,
    [FecMod]     DATETIME CONSTRAINT [DF_COTI_INCLUIDO_FecMod] DEFAULT (getdate()) NOT NULL,
    [Orden]      TINYINT  NULL,
    CONSTRAINT [PK_COTI_INCLUIDO] PRIMARY KEY CLUSTERED ([IDCAB] ASC, [IDIncluido] ASC),
    CONSTRAINT [FK_COTI_INCLUIDO_IDCAB] FOREIGN KEY ([IDCAB]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_COTI_INCLUIDO_IDIncluido] FOREIGN KEY ([IDIncluido]) REFERENCES [dbo].[MAINCLUIDO] ([IDIncluido])
);


GO

--95
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTI_INCLUIDO_LOG_Ins]
  On [dbo].[COTI_INCLUIDO] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB from Inserted)
			set @RegistroPK2 = (select IDIncluido from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_INCLUIDO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--99
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTI_INCLUIDO_LOG_Del]
  On [dbo].[COTI_INCLUIDO] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB from Deleted)
			set @RegistroPK2 = (select IDIncluido from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_INCLUIDO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--97
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTI_INCLUIDO_LOG_Upd]
  On [dbo].[COTI_INCLUIDO] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB from Inserted)
			set @RegistroPK2 = (select IDIncluido from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTI_INCLUIDO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
