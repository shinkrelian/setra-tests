﻿CREATE TABLE [dbo].[NOTA_VENTA] (
    [IDNotaVenta] CHAR (8)       NOT NULL,
    [IDSerie]     CHAR (3)       NOT NULL,
    [Fecha]       SMALLDATETIME  DEFAULT (getdate()) NOT NULL,
    [IDCab]       INT            NOT NULL,
    [IDMoneda]    CHAR (3)       NOT NULL,
    [SubTotal]    NUMERIC (8, 2) NOT NULL,
    [TotalIGV]    NUMERIC (8, 2) NOT NULL,
    [Total]       NUMERIC (8, 2) NOT NULL,
    [IDEstado]    CHAR (2)       NOT NULL,
    [UserMod]     CHAR (4)       NOT NULL,
    [FecMod]      DATETIME       DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_NOTA_VENTA] PRIMARY KEY NONCLUSTERED ([IDNotaVenta] ASC),
    CONSTRAINT [FK_NOTA_VENTA_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_NOTA_VENTA_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda])
);


GO
Create Trigger [dbo].[NOTA_VENTA_LOG_Ins]  
  On [dbo].[NOTA_VENTA] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDNotaVenta from Inserted)         
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'NOTA_VENTA',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[NOTA_VENTA_LOG_Upd]  
  On [dbo].[NOTA_VENTA] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)    
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDNotaVenta from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'NOTA_VENTA',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[NOTA_VENTA_LOG_Del]  
  On [dbo].[NOTA_VENTA] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDNotaVenta from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'NOTA_VENTA',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
