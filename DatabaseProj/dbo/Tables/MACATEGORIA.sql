﻿CREATE TABLE [dbo].[MACATEGORIA] (
    [IDCat]       CHAR (3)       NOT NULL,
    [Descripcion] CHAR (10)      NOT NULL,
    [Comision]    NUMERIC (6, 2) NOT NULL,
    [UserMod]     CHAR (4)       NULL,
    [FecMod]      DATETIME       NULL,
    [QtPeso]      DECIMAL (8, 2) NULL,
    CONSTRAINT [PK_MACATEGORIA] PRIMARY KEY CLUSTERED ([IDCat] ASC)
);


GO

--176
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MACATEGORIA

CREATE Trigger [dbo].[MACATEGORIA_LOG_Ins]
  On [dbo].[MACATEGORIA] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCat from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACATEGORIA',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--180
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACATEGORIA_LOG_Del]
  On [dbo].[MACATEGORIA] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCat from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACATEGORIA',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--178
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACATEGORIA_LOG_Upd]
  On [dbo].[MACATEGORIA] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCat from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACATEGORIA',@UserMod,@RegistroPK1,'','','',@Accion
		End  
