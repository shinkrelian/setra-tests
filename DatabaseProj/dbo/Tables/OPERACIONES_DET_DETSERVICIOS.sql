﻿CREATE TABLE [dbo].[OPERACIONES_DET_DETSERVICIOS] (
    [IDOperacion_Det]            INT             NOT NULL,
    [IDServicio_Det]             INT             NOT NULL,
    [IDServicio_Det_V_Cot]       INT             NULL,
    [IDServicio_Det_V_Prg]       INT             NULL,
    [IDProveedor_Prg]            CHAR (6)        NULL,
    [CostoReal]                  NUMERIC (14, 4) NOT NULL,
    [CostoLiberado]              NUMERIC (14, 4) NOT NULL,
    [Margen]                     NUMERIC (14, 4) NOT NULL,
    [MargenAplicado]             NUMERIC (6, 2)  NOT NULL,
    [MargenLiberado]             NUMERIC (14, 4) NOT NULL,
    [Total]                      NUMERIC (14, 4) NOT NULL,
    [Total_Prg]                  NUMERIC (14, 4) NULL,
    [SSCR]                       NUMERIC (14, 4) NULL,
    [SSCL]                       NUMERIC (14, 4) NULL,
    [SSMargen]                   NUMERIC (14, 4) NULL,
    [SSMargenLiberado]           NUMERIC (14, 4) NULL,
    [SSTotal]                    NUMERIC (14, 4) NULL,
    [STCR]                       NUMERIC (14, 4) NULL,
    [STMargen]                   NUMERIC (14, 4) NULL,
    [STTotal]                    NUMERIC (14, 4) NULL,
    [CostoRealImpto]             NUMERIC (14, 4) NOT NULL,
    [CostoLiberadoImpto]         NUMERIC (14, 4) NOT NULL,
    [MargenImpto]                NUMERIC (14, 4) NOT NULL,
    [MargenLiberadoImpto]        NUMERIC (6, 2)  NOT NULL,
    [SSCRImpto]                  NUMERIC (14, 4) NULL,
    [SSCLImpto]                  NUMERIC (14, 4) NULL,
    [SSMargenImpto]              NUMERIC (14, 4) NULL,
    [SSMargenLiberadoImpto]      NUMERIC (14, 4) NULL,
    [STCRImpto]                  NUMERIC (14, 4) NULL,
    [STMargenImpto]              NUMERIC (14, 4) NULL,
    [TotImpto]                   NUMERIC (14, 4) NOT NULL,
    [IDEstadoSolicitud]          CHAR (2)        CONSTRAINT [DF_OPERACIONES_DET_DETSERVICIOS_IDEstadoSolicitud] DEFAULT ('NV') NOT NULL,
    [IDCorreoEstadoAC]           VARCHAR (MAX)   NULL,
    [ObservEdicion]              VARCHAR (250)   NULL,
    [UserMod]                    CHAR (4)        CONSTRAINT [DF_OPERACIONES_DET_DETSERVICIOS_UserMod] DEFAULT ('NV') NOT NULL,
    [FecMod]                     DATETIME        CONSTRAINT [CK_OPERACIONES_DET_DETSERVICIOS_FecMod] DEFAULT (getdate()) NOT NULL,
    [TipoCambio]                 NUMERIC (8, 2)  NULL,
    [IDMoneda]                   CHAR (3)        NOT NULL,
    [IDServicio]                 CHAR (8)        NOT NULL,
    [Total_PrgEditado]           BIT             DEFAULT ((0)) NOT NULL,
    [NetoProgram]                NUMERIC (14, 4) NULL,
    [IgvProgram]                 NUMERIC (14, 4) NULL,
    [TotalProgram]               NUMERIC (14, 4) NULL,
    [Activo]                     BIT             DEFAULT ((1)) NOT NULL,
    [IDVehiculo_Prg]             SMALLINT        NULL,
    [NetoCotizado]               NUMERIC (14, 4) DEFAULT (NULL) NULL,
    [IgvCotizado]                NUMERIC (14, 4) DEFAULT (NULL) NULL,
    [TotalCotizado]              NUMERIC (14, 4) DEFAULT (NULL) NULL,
    [IngManual]                  BIT             DEFAULT ((0)) NOT NULL,
    [FlServicioInCorrectoxTI]    BIT             DEFAULT ((0)) NOT NULL,
    [FlMontosSincerados]         BIT             DEFAULT ((0)) NOT NULL,
    [CoEstadoSincerado]          CHAR (2)        NULL,
    [FlServicioNoShow]           BIT             DEFAULT ((0)) NOT NULL,
    [FlServicioNoCobrado]        BIT             DEFAULT ((0)) NOT NULL,
    [SSTotalProgramAntNoCobrado] NUMERIC (14, 4) NULL,
    [QtPax]                      SMALLINT        NOT NULL,
    CONSTRAINT [PK_OPERACIONES_DET_DETSERVICIOS] PRIMARY KEY CLUSTERED ([IDOperacion_Det] ASC, [IDServicio_Det] ASC, [FlServicioNoShow] ASC),
    CONSTRAINT [FK_OPERACIONES_DET_DETSERVICIOS_IDOperacion_Det] FOREIGN KEY ([IDOperacion_Det]) REFERENCES [dbo].[OPERACIONES_DET] ([IDOperacion_Det]),
    CONSTRAINT [FK_OPERACIONES_DET_DETSERVICIOS_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_OPERACIONES_DET_DETSERVICIOS_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio])
);


GO
CREATE NONCLUSTERED INDEX [IX_OPERACIONES_DET_DETSERVICIOS_Activo_IDProveedor_Prg]
    ON [dbo].[OPERACIONES_DET_DETSERVICIOS]([Activo] ASC, [IDProveedor_Prg] ASC)
    INCLUDE([IDOperacion_Det], [IDServicio_Det_V_Cot], [CostoReal], [IDMoneda], [IDServicio], [NetoProgram], [NetoCotizado], [IngManual]);


GO
CREATE NONCLUSTERED INDEX [IX_OPERACIONES_DET_DETSERVICIOS_IDServicio_Det_V_Activo]
    ON [dbo].[OPERACIONES_DET_DETSERVICIOS]([IDServicio_Det_V_Cot] ASC, [Activo] ASC)
    INCLUDE([IDOperacion_Det], [IDProveedor_Prg], [CostoReal], [IDMoneda], [IDServicio], [NetoProgram], [NetoCotizado], [IngManual]);


GO
CREATE NONCLUSTERED INDEX [IX_OPERACIONES_DETSERVICIOS_All]
    ON [dbo].[OPERACIONES_DET_DETSERVICIOS]([Activo] ASC, [IDProveedor_Prg] ASC, [IDEstadoSolicitud] ASC, [IDOperacion_Det] ASC, [IDServicio_Det] ASC, [IDServicio_Det_V_Cot] ASC, [IDServicio_Det_V_Prg] ASC, [IDMoneda] ASC, [TotalProgram] ASC, [IDVehiculo_Prg] ASC, [NetoCotizado] ASC);


GO
	

--186
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_DET_DETSERVICIOS_LOG_Del]
 On dbo.OPERACIONES_DET_DETSERVICIOS for Delete
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Deleted)=1
		Begin
			set @RegistroPK1 = (select IDOperacion_Det from Deleted)
			set @RegistroPK2 = (select IDServicio_Det from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--182
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_DET_DETSERVICIOS_LOG_Ins]
 On dbo.OPERACIONES_DET_DETSERVICIOS for Insert
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	  If (select count(*) from Inserted) = 1
	  Begin
			set @RegistroPK1 = (select IDOperacion_Det from Inserted)
			set @RegistroPK2 = (select IDServicio_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
	  End  

GO
	
	

--184
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_DET_DETSERVICIOS_LOG_Upd]
 On dbo.OPERACIONES_DET_DETSERVICIOS for Update
As
	Set NoCount On 
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	If (select Count(*) from Inserted)=1
		Begin
			set @RegistroPK1 = (select IDOperacion_Det from Inserted)
			set @RegistroPK2 = (select IDServicio_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES_DET_DETSERVICIOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
