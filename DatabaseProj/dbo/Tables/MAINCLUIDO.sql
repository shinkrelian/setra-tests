﻿CREATE TABLE [dbo].[MAINCLUIDO] (
    [IDIncluido]   INT           IDENTITY (1, 1) NOT NULL,
    [Tipo]         CHAR (1)      NOT NULL,
    [DescAbrev]    VARCHAR (100) NOT NULL,
    [Descripcion]  VARCHAR (250) NULL,
    [PorDefecto]   BIT           NOT NULL,
    [IDIdioma]     VARCHAR (12)  NULL,
    [UserMod]      CHAR (4)      NOT NULL,
    [FecMod]       DATETIME      CONSTRAINT [DF_MAINCLUIDO_FecMod] DEFAULT (getdate()) NOT NULL,
    [Orden]        TINYINT       DEFAULT ((0)) NOT NULL,
    [TxObsStatus]  TEXT          NULL,
    [FlAutomatico] BIT           DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MAINCLUIDO] PRIMARY KEY CLUSTERED ([IDIncluido] ASC)
);


GO

--72
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAINCLUIDO_LOG_Upd]
  On [dbo].[MAINCLUIDO] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIncluido from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--71
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAINCLUIDO

CREATE Trigger [dbo].[MAINCLUIDO_LOG_Ins]
  On [dbo].[MAINCLUIDO] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIncluido from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDO',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--74
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAINCLUIDO_LOG_Del]
  On [dbo].[MAINCLUIDO] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIncluido from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDO',@UserMod,@RegistroPK1,'','','',@Accion
		End  
