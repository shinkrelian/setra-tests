﻿CREATE TABLE [dbo].[ORDEN_SERVICIO] (
    [NuOrden_Servicio]     INT            NOT NULL,
    [IDCab]                INT            NOT NULL,
    [IDProveedor]          CHAR (6)       NOT NULL,
    [IDUsuarioResponsable] CHAR (4)       NOT NULL,
    [NuVehiculoProg]       SMALLINT       NULL,
    [CoOrden_Servicio]     CHAR (14)      NOT NULL,
    [FlMigrado]            BIT            DEFAULT ((0)) NOT NULL,
    [UserMod]              CHAR (4)       NOT NULL,
    [FecMod]               SMALLDATETIME  NOT NULL,
    [CoEstado]             CHAR (2)       DEFAULT ('PD') NOT NULL,
    [SsSaldo]              NUMERIC (8, 2) DEFAULT (NULL) NULL,
    [SsDifAceptada]        NUMERIC (8, 2) DEFAULT (NULL) NULL,
    [TxObsDocumento]       VARCHAR (MAX)  DEFAULT (NULL) NULL,
    [SsMontoTotal]         NUMERIC (8, 2) DEFAULT (NULL) NULL,
    [UserNuevo]            CHAR (4)       NULL,
    [IDProveedorSetours]   CHAR (6)       DEFAULT (NULL) NULL,
    [FeCreacion]           DATETIME       NULL,
    CONSTRAINT [PK_ORDEN_SERVICIO] PRIMARY KEY CLUSTERED ([NuOrden_Servicio] ASC),
    CONSTRAINT [FK_ORDEN_SERVICIO_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_ORDEN_SERVICIO_ESTADO_OBLIGACIONESPAGO] FOREIGN KEY ([CoEstado]) REFERENCES [dbo].[ESTADO_OBLIGACIONESPAGO] ([CoEstado]),
    CONSTRAINT [FK_ORDEN_SERVICIO_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDcab]
    ON [dbo].[ORDEN_SERVICIO]([IDCab] ASC);


GO
Create Trigger [dbo].[ORDEN_SERVICIO_LOG_Ins]  
  On [dbo].[ORDEN_SERVICIO] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[ORDEN_SERVICIO_LOG_Upd]  
  On [dbo].[ORDEN_SERVICIO] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio from Inserted)    
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[ORDEN_SERVICIO_LOG_Del]  
  On [dbo].[ORDEN_SERVICIO] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuOrden_Servicio from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'ORDEN_SERVICIO',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
