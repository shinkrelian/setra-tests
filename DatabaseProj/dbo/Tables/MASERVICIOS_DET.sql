﻿CREATE TABLE [dbo].[MASERVICIOS_DET] (
    [IDServicio_Det]          INT             NOT NULL,
    [IDServicio]              CHAR (8)        NOT NULL,
    [IDServicio_Det_V]        INT             NULL,
    [Anio]                    CHAR (4)        NOT NULL,
    [Tipo]                    VARCHAR (7)     NULL,
    [Correlativo]             TINYINT         NOT NULL,
    [DetaTipo]                TEXT            NULL,
    [VerDetaTipo]             BIT             CONSTRAINT [DF_MASERVICIOS_DET_VerDetaTipo_1] DEFAULT ((0)) NOT NULL,
    [Codigo]                  VARCHAR (20)    NOT NULL,
    [Descripcion]             VARCHAR (200)   NOT NULL,
    [Afecto]                  BIT             NOT NULL,
    [Monto_sgl]               NUMERIC (10, 4) NULL,
    [Monto_dbl]               NUMERIC (10, 4) NULL,
    [Monto_tri]               NUMERIC (10, 4) NULL,
    [cod_x_triple]            VARCHAR (20)    NULL,
    [IDServicio_DetxTriple]   INT             NULL,
    [IdHabitTriple]           CHAR (3)        NOT NULL,
    [Monto_sgls]              NUMERIC (12, 4) NULL,
    [Monto_dbls]              NUMERIC (12, 4) NULL,
    [Monto_tris]              NUMERIC (12, 4) NULL,
    [TC]                      NUMERIC (10, 4) NULL,
    [ConAlojamiento]          BIT             NOT NULL,
    [PlanAlimenticio]         BIT             NOT NULL,
    [DiferSS]                 NUMERIC (10, 4) NULL,
    [DiferST]                 NUMERIC (10, 4) NULL,
    [TipoGasto]               CHAR (1)        NULL,
    [TipoDesayuno]            CHAR (2)        NOT NULL,
    [Desayuno]                BIT             NOT NULL,
    [Lonche]                  BIT             NOT NULL,
    [Almuerzo]                BIT             NOT NULL,
    [Cena]                    BIT             NOT NULL,
    [PoliticaLiberado]        BIT             NOT NULL,
    [Liberado]                TINYINT         NULL,
    [TipoLib]                 CHAR (1)        NULL,
    [MaximoLiberado]          TINYINT         NULL,
    [LiberadoM]               TINYINT         NULL,
    [MontoL]                  NUMERIC (10, 4) NULL,
    [Tarifario]               BIT             NULL,
    [TituloGrupo]             VARCHAR (30)    NULL,
    [idTipoOC]                CHAR (3)        NOT NULL,
    [CtaContable]             VARCHAR (14)    NOT NULL,
    [CtaContableC]            VARCHAR (20)    NULL,
    [IDServicio_DetCopia]     INT             NULL,
    [Mig]                     BIT             CONSTRAINT [DF__MASERVICIOS__Mig__2D52A092] DEFAULT ((0)) NOT NULL,
    [UserMod]                 CHAR (4)        NOT NULL,
    [FecMod]                  DATETIME        CONSTRAINT [DF_MASERVICIOS_DET_FecMod] DEFAULT (getdate()) NOT NULL,
    [Estado]                  CHAR (1)        DEFAULT ('A') NOT NULL,
    [CodTarifa]               VARCHAR (MAX)   NULL,
    [PoliticaLibNivelDetalle] BIT             DEFAULT ((0)) NOT NULL,
    [CoMoneda]                CHAR (3)        NOT NULL,
    [NuCuentaAnt]             CHAR (6)        NULL,
    [FlCtaCambiada]           BIT             DEFAULT ((0)) NOT NULL,
    [FlEsViaticosNac]         BIT             DEFAULT ((0)) NOT NULL,
    [FlEsViaticosInt]         BIT             DEFAULT ((0)) NOT NULL,
    [FlEsTarjetaTelefonica]   BIT             DEFAULT ((0)) NOT NULL,
    [FlEsTaxi]                BIT             DEFAULT ((0)) NOT NULL,
    [FlEsHotelNocheAntesDesp] BIT             DEFAULT ((0)) NOT NULL,
    [FlEsVuelo]               BIT             DEFAULT ((0)) NOT NULL,
    [FlEsHonorario]           BIT             DEFAULT ((0)) NOT NULL,
    [FlEsTicketAereros]       BIT             DEFAULT ((0)) NOT NULL,
    [FlEsIGVHoteles]          BIT             DEFAULT ((0)) NOT NULL,
    [FlEsOtros]               BIT             DEFAULT ((0)) NOT NULL,
    [CtaContable_Anterior]    VARCHAR (14)    NULL,
    [CoCeCos]                 CHAR (6)        NULL,
    [FlDetraccion]            BIT             NULL,
    [FlEsGEBADS_PrimerDia]    BIT             DEFAULT ((0)) NOT NULL,
    [FlEsGEBADS_UltimoDia]    BIT             DEFAULT ((0)) NOT NULL,
    [CoTipoCostoTC]           CHAR (3)        NULL,
    [TipoUsoSistema]          CHAR (4)        DEFAULT ('') NULL,
    CONSTRAINT [PK_MASERVICIOS_DET_1] PRIMARY KEY CLUSTERED ([IDServicio_Det] ASC),
    CONSTRAINT [FK_MASERVICIOS_DET_MAHABITTRIPLE] FOREIGN KEY ([IdHabitTriple]) REFERENCES [dbo].[MAHABITTRIPLE] ([IdHabitTriple]),
    CONSTRAINT [FK_MASERVICIOS_DET_MAMONEDAS] FOREIGN KEY ([CoMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_MASERVICIOS_DET_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio]),
    CONSTRAINT [FK_MASERVICIOS_DET_MATIPOOPERACION] FOREIGN KEY ([idTipoOC]) REFERENCES [dbo].[MATIPOOPERACION] ([IdToc])
);


GO
CREATE NONCLUSTERED INDEX [IX_MASERVICIOS_DET_IDServicio_Anio]
    ON [dbo].[MASERVICIOS_DET]([IDServicio] ASC, [Anio] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MASERVICIOS_DET_IDServicio_DetxTriple]
    ON [dbo].[MASERVICIOS_DET]([IDServicio_DetxTriple] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_MASERVICIOS_DET_Estado]
    ON [dbo].[MASERVICIOS_DET]([IDServicio_Det] ASC, [Estado] ASC);


GO
CREATE NONCLUSTERED INDEX [MASERVICIOS_DET_IDServicio_Anio_Tipo_Estado]
    ON [dbo].[MASERVICIOS_DET]([IDServicio] ASC, [Anio] ASC, [Tipo] ASC, [Estado] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDtipoOC]
    ON [dbo].[MASERVICIOS_DET]([idTipoOC] ASC)
    INCLUDE([IDServicio_Det]);


GO

--24
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MASERVICIOS_DET_LOG_Ins]
  On dbo.MASERVICIOS_DET for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--27
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MASERVICIOS_DET_LOG_Upd]
  On dbo.MASERVICIOS_DET for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio_Det from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--26
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MASERVICIOS_DET_LOG_Del]
  On dbo.MASERVICIOS_DET for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio_Det from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DET',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'MASERVICIOS_DET', @level2type = N'COLUMN', @level2name = N'TipoDesayuno';

