﻿CREATE TABLE [dbo].[MAPROVEEDORES_RANGO_APT] (
    [Correlativo] TINYINT         NOT NULL,
    [IDProveedor] CHAR (6)        NOT NULL,
    [NuAnio]      CHAR (4)        NOT NULL,
    [PaxDesde]    SMALLINT        NOT NULL,
    [PaxHasta]    SMALLINT        NOT NULL,
    [Monto]       NUMERIC (10, 4) NOT NULL,
    [UserMod]     CHAR (4)        NOT NULL,
    [FecMod]      DATETIME        CONSTRAINT [DF_MAPROVEEDORES_RANGO_APT_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAPROVEEDORES_RANGO_APT] PRIMARY KEY CLUSTERED ([Correlativo] ASC, [IDProveedor] ASC, [NuAnio] ASC),
    CONSTRAINT [FK_MAPROVEEDORES_RANGO_APT_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);

