﻿CREATE TABLE [dbo].[MAINCLUIDO_IDIOMAS] (
    [IDIncluido]  INT           NOT NULL,
    [IDIdioma]    VARCHAR (12)  NOT NULL,
    [Descripcion] VARCHAR (MAX) NOT NULL,
    [UserMod]     CHAR (4)      NOT NULL,
    [FecMod]      DATETIME      CONSTRAINT [DF__MAINCLUID__FecMo__1446FBA6] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAINCLUIDO_IDIOMAS] PRIMARY KEY NONCLUSTERED ([IDIncluido] ASC, [IDIdioma] ASC),
    CONSTRAINT [FK_MAINCLUIDO_IDIOMAS_MAIDIOMAS] FOREIGN KEY ([IDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_MAINCLUIDO_IDIOMAS_MAINCLUIDO] FOREIGN KEY ([IDIncluido]) REFERENCES [dbo].[MAINCLUIDO] ([IDIncluido])
);


GO

--76
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAINCLUIDO_IDIOMAS

CREATE Trigger [dbo].[MAINCLUIDO_IDIOMAS_LOG_Ins]
  On [dbo].[MAINCLUIDO_IDIOMAS] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIncluido from Inserted)
			set @RegistroPK2 = (select IDIdioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDO_IDIOMAS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--79
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAINCLUIDO_IDIOMAS_LOG_Del]
  On [dbo].[MAINCLUIDO_IDIOMAS] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIncluido from Deleted)
			set @RegistroPK2 = (select IDIdioma from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDO_IDIOMAS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--78
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAINCLUIDO_IDIOMAS_LOG_Upd]
  On [dbo].[MAINCLUIDO_IDIOMAS] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIncluido from Inserted)
			set @RegistroPK2 = (select IDIdioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAINCLUIDO_IDIOMAS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
