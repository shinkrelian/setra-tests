﻿CREATE TABLE [dbo].[VentasxCliente_Anual] (
    [Anio]                VARCHAR (4)      NULL,
    [IDCliente]           CHAR (6)         NOT NULL,
    [DescCliente]         VARCHAR (80)     NULL,
    [ImporteUSD]          NUMERIC (38, 6)  NULL,
    [ImporteUSDAnioAnter] NUMERIC (12, 4)  NOT NULL,
    [Crecimiento]         NUMERIC (38, 6)  NULL,
    [CantFiles]           INT              NULL,
    [CantFilesAnioAnter]  INT              NOT NULL,
    [TicketFileAnioAnter] NUMERIC (23, 15) NULL,
    [PromMargen]          NUMERIC (38, 6)  NOT NULL,
    [PromMargenAnioAnter] NUMERIC (12, 4)  NOT NULL,
    [Utilidad]            NUMERIC (38, 6)  NULL,
    [UtilidadAnioAnter]   NUMERIC (29, 12) NULL,
    [Dias]                INT              NULL,
    [Edad_Prom]           INT              NOT NULL,
    [ParamAnio]           CHAR (4)         NULL,
    [TicketFile]          NUMERIC (23, 15) NULL
);

