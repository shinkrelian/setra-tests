﻿CREATE TABLE [dbo].[MASERVICIOS_DIA] (
    [IDServicio]        CHAR (8)      NOT NULL,
    [Dia]               TINYINT       NOT NULL,
    [IDIdioma]          VARCHAR (12)  NOT NULL,
    [DescripCorta]      VARCHAR (100) NULL,
    [Descripcion]       TEXT          NULL,
    [Mig]               BIT           CONSTRAINT [DF__MASERVICIOS__Mig__2C5E7C59] DEFAULT ((0)) NOT NULL,
    [UserMod]           CHAR (4)      NOT NULL,
    [FechaMod]          DATETIME      CONSTRAINT [DF__MASERVICI__Fecha__32816A03] DEFAULT (getdate()) NOT NULL,
    [NoHotel]           VARCHAR (100) NULL,
    [TxWebHotel]        VARCHAR (150) NULL,
    [TxDireccHotel]     VARCHAR (150) NULL,
    [TxTelfHotel1]      VARCHAR (50)  NULL,
    [TxTelfHotel2]      VARCHAR (50)  NULL,
    [FeHoraChkInHotel]  SMALLDATETIME NULL,
    [FeHoraChkOutHotel] SMALLDATETIME NULL,
    [CoTipoDesaHotel]   CHAR (2)      NULL,
    [CoCiudadHotel]     CHAR (6)      NULL,
    CONSTRAINT [PK_MASERVICIOS_DIA] PRIMARY KEY NONCLUSTERED ([IDServicio] ASC, [Dia] ASC, [IDIdioma] ASC),
    CONSTRAINT [FK_MASERVICIOS_DIA_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio])
);


GO

--2
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MASERVICIOS_DIA_LOG_Upd]
  On [dbo].[MASERVICIOS_DIA] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Inserted)
			set @RegistroPK2 = (select Dia from Inserted)
			set @RegistroPK3 = (select IDIdioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DIA',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion
		End  

GO


--3
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MASERVICIOS_DIA_LOG_Del]  
  On [dbo].[MASERVICIOS_DIA] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)  
 Declare @RegistroPK3 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Deleted) = 1   
  Begin   
   set @RegistroPK1 = (select IDServicio from Deleted)  
   set @RegistroPK2 = (select Dia from Deleted)  
   set @RegistroPK3 = (select IDIdioma from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DIA',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion  
  End  

GO
--1
----Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


CREATE Trigger [dbo].[MASERVICIOS_DIA_LOG_Ins]
  On [dbo].[MASERVICIOS_DIA] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @RegistroPK3 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Inserted)
			set @RegistroPK2 = (select Dia from Inserted)
			set @RegistroPK3 = (select IDIdioma from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS_DIA',@UserMod,@RegistroPK1,@RegistroPK2,@RegistroPK3,'',@Accion
		End  
