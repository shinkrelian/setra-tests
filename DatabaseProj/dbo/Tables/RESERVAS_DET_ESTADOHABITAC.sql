﻿CREATE TABLE [dbo].[RESERVAS_DET_ESTADOHABITAC] (
    [IDDet]       INT      NOT NULL,
    [Simple]      TINYINT  NULL,
    [Twin]        TINYINT  NULL,
    [Matrimonial] TINYINT  NULL,
    [Triple]      TINYINT  NULL,
    [UserMod]     CHAR (4) NOT NULL,
    [FecMod]      DATETIME DEFAULT (getdate()) NOT NULL,
    [NuIngreso]   TINYINT  NOT NULL,
    CONSTRAINT [PK_RESERVAS_DET_ESTADOHABITAC] PRIMARY KEY NONCLUSTERED ([IDDet] ASC, [NuIngreso] ASC),
    CONSTRAINT [FK_RESERVAS_DET_ESTADOHABITAC_COTIDET] FOREIGN KEY ([IDDet]) REFERENCES [dbo].[COTIDET] ([IDDET])
);


GO
Create Trigger [dbo].[RESERVAS_DET_ESTADOHABITAC_LOG_Ins]  
  On [dbo].[RESERVAS_DET_ESTADOHABITAC] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Inserted)        
   set @RegistroPK2 = (select NuIngreso from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_ESTADOHABITAC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_DET_ESTADOHABITAC_LOG_Upd]  
  On [dbo].[RESERVAS_DET_ESTADOHABITAC] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Inserted)      
   set @RegistroPK2 = (select NuIngreso from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_ESTADOHABITAC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_DET_ESTADOHABITAC_LOG_Del]  
  On [dbo].[RESERVAS_DET_ESTADOHABITAC] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Deleted)   
   set @RegistroPK2 = (select NuIngreso from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_ESTADOHABITAC',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
