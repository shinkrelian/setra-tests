﻿CREATE TABLE [dbo].[MASERVICIOS_DET_DESC_APT] (
    [IDServicio_Det]     INT             NOT NULL,
    [IDTemporada]        INT             NOT NULL,
    [DescripcionAlterna] VARCHAR (200)   NULL,
    [UserMod]            CHAR (4)        NOT NULL,
    [FecMod]             DATETIME        CONSTRAINT [DF_MASERVICIOS_DET_DESC_APT_FecMod] DEFAULT (getdate()) NOT NULL,
    [SingleSupplement]   NUMERIC (10, 4) NULL,
    [PoliticaLiberado]   BIT             DEFAULT ((0)) NOT NULL,
    [Liberado]           TINYINT         NULL,
    [TipoLib]            CHAR (1)        NULL,
    [MaximoLiberado]     TINYINT         NULL,
    [MontoL]             NUMERIC (10, 4) NULL,
    CONSTRAINT [PK_MASERVICIOS_DET_DESC_APT] PRIMARY KEY CLUSTERED ([IDServicio_Det] ASC, [IDTemporada] ASC),
    CONSTRAINT [FK_MASERVICIOS_DET_DESC_APT_MASERVICIOS_DET] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det]),
    CONSTRAINT [FK_MASERVICIOS_DET_DESC_APT_MATEMPORADA] FOREIGN KEY ([IDTemporada]) REFERENCES [dbo].[MATEMPORADA] ([IDTemporada])
);

