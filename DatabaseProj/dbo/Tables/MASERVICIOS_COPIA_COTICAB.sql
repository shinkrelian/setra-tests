﻿CREATE TABLE [dbo].[MASERVICIOS_COPIA_COTICAB] (
    [IDServicioAntiguo]   CHAR (8) NOT NULL,
    [IDServicioNuevo]     CHAR (8) NOT NULL,
    [IDServicio_Det]      INT      NOT NULL,
    [IDServicio_DetNuevo] INT      NULL,
    [IDCab]               INT      NOT NULL,
    [UserMod]             CHAR (4) NOT NULL,
    [FecMod]              DATETIME DEFAULT (getdate()) NOT NULL
);

