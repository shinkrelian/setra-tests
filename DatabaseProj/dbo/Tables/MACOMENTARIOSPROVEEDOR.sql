﻿CREATE TABLE [dbo].[MACOMENTARIOSPROVEEDOR] (
    [IDComentario] CHAR (3)      NOT NULL,
    [IDProveedor]  CHAR (6)      NOT NULL,
    [Fecha]        SMALLDATETIME CONSTRAINT [DF_MACOMENTARIOSPROVEEDOR_Fecha] DEFAULT (getdate()) NOT NULL,
    [IDFile]       CHAR (10)     NULL,
    [Comentario]   TEXT          NOT NULL,
    [Estado]       CHAR (1)      NOT NULL,
    [UserMod]      CHAR (4)      NOT NULL,
    [FecMod]       DATETIME      CONSTRAINT [DF_MACOMENTARIOSPROVEEDOR_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MACOMENTARIOSPROVEEDOR] PRIMARY KEY NONCLUSTERED ([IDComentario] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_MACOMENTARIOSPROVEEDOR_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO

--192
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACOMENTARIOSPROVEEDOR_LOG_Del]
  On [dbo].[MACOMENTARIOSPROVEEDOR] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDComentario from Deleted)
			set @RegistroPK2 = (select IDProveedor from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACOMENTARIOSPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--188
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MACOMENTARIOSPROVEEDOR

CREATE Trigger [dbo].[MACOMENTARIOSPROVEEDOR_LOG_Ins]
  On [dbo].[MACOMENTARIOSPROVEEDOR] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDComentario from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACOMENTARIOSPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--190
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MACOMENTARIOSPROVEEDOR_LOG_Upd]
  On [dbo].[MACOMENTARIOSPROVEEDOR] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDComentario from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MACOMENTARIOSPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
