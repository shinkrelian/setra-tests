﻿CREATE TABLE [dbo].[COTIDET] (
    [IDDET]                            INT             IDENTITY (1, 1) NOT NULL,
    [IDCAB]                            INT             NOT NULL,
    [Cotizacion]                       CHAR (8)        NOT NULL,
    [Item]                             NUMERIC (6, 3)  NOT NULL,
    [Dia]                              SMALLDATETIME   NOT NULL,
    [DiaNombre]                        VARCHAR (10)    NOT NULL,
    [IDubigeo]                         CHAR (6)        NOT NULL,
    [IDProveedor]                      CHAR (6)        NOT NULL,
    [IDServicio]                       CHAR (8)        NOT NULL,
    [IDServicio_Det]                   INT             NOT NULL,
    [IDidioma]                         VARCHAR (12)    NOT NULL,
    [Especial]                         BIT             NOT NULL,
    [MotivoEspecial]                   VARCHAR (200)   NULL,
    [RutaDocSustento]                  VARCHAR (200)   NULL,
    [Desayuno]                         BIT             NOT NULL,
    [Lonche]                           BIT             NOT NULL,
    [Almuerzo]                         BIT             NOT NULL,
    [Cena]                             BIT             NOT NULL,
    [Transfer]                         BIT             NOT NULL,
    [IDDetTransferOri]                 INT             NULL,
    [IDDetTransferDes]                 INT             NULL,
    [CamaMat]                          TINYINT         NULL,
    [TipoTransporte]                   CHAR (1)        NULL,
    [IDUbigeoOri]                      CHAR (6)        NOT NULL,
    [IDUbigeoDes]                      CHAR (6)        NOT NULL,
    [NroPax]                           SMALLINT        NOT NULL,
    [NroLiberados]                     SMALLINT        NULL,
    [Tipo_Lib]                         CHAR (1)        NULL,
    [CostoReal]                        NUMERIC (12, 4) NOT NULL,
    [CostoRealEditado]                 BIT             CONSTRAINT [DF_COTIDET_CostoRealEditado] DEFAULT ((0)) NOT NULL,
    [CostoRealAnt]                     NUMERIC (12, 4) NULL,
    [CostoLiberado]                    NUMERIC (12, 4) NOT NULL,
    [Margen]                           NUMERIC (12, 4) NOT NULL,
    [MargenAplicado]                   NUMERIC (6, 2)  NOT NULL,
    [MargenAplicadoEditado]            BIT             CONSTRAINT [DF_COTIDET_MargenAplicadoEditado] DEFAULT ((0)) NOT NULL,
    [MargenAplicadoAnt]                NUMERIC (6, 2)  NULL,
    [MargenLiberado]                   NUMERIC (12, 4) NOT NULL,
    [Total]                            NUMERIC (8, 2)  NOT NULL,
    [TotalOrig]                        NUMERIC (8, 2)  NOT NULL,
    [SSCR]                             NUMERIC (12, 4) NOT NULL,
    [SSCL]                             NUMERIC (12, 4) NOT NULL,
    [SSMargen]                         NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberado]                 NUMERIC (12, 4) NOT NULL,
    [SSTotal]                          NUMERIC (12, 4) NOT NULL,
    [SSTotalOrig]                      NUMERIC (12, 4) NOT NULL,
    [STCR]                             NUMERIC (12, 4) NOT NULL,
    [STMargen]                         NUMERIC (12, 4) NOT NULL,
    [STTotal]                          NUMERIC (12, 4) NOT NULL,
    [STTotalOrig]                      NUMERIC (12, 4) NOT NULL,
    [CostoRealImpto]                   NUMERIC (12, 4) NOT NULL,
    [CostoRealImptoEditado]            BIT             CONSTRAINT [DF_COTIDET_CostoRealImptoEditado] DEFAULT ((0)) NOT NULL,
    [CostoLiberadoImpto]               NUMERIC (12, 4) NOT NULL,
    [MargenImpto]                      NUMERIC (12, 4) NOT NULL,
    [MargenLiberadoImpto]              NUMERIC (12, 4) NOT NULL,
    [SSCRImpto]                        NUMERIC (12, 4) NOT NULL,
    [SSCLImpto]                        NUMERIC (12, 4) NOT NULL,
    [SSMargenImpto]                    NUMERIC (12, 4) NOT NULL,
    [SSMargenLiberadoImpto]            NUMERIC (12, 4) NOT NULL,
    [STCRImpto]                        NUMERIC (12, 4) NOT NULL,
    [STMargenImpto]                    NUMERIC (12, 4) NOT NULL,
    [TotImpto]                         NUMERIC (12, 4) NOT NULL,
    [IDDetRel]                         INT             NULL,
    [Servicio]                         VARCHAR (MAX)   NULL,
    [ServicioEditado]                  BIT             CONSTRAINT [DF_COTIDET_ServicioEditado] DEFAULT ((0)) NOT NULL,
    [IDDetCopia]                       INT             NULL,
    [Recalcular]                       BIT             CONSTRAINT [DF_COTIDET_Recalcular] DEFAULT ((0)) NOT NULL,
    [PorReserva]                       BIT             CONSTRAINT [DF_COTIDET_PorReserva] DEFAULT ((0)) NOT NULL,
    [ExecTrigger]                      BIT             CONSTRAINT [DF_COTIDET_ExecTrigger] DEFAULT ((1)) NOT NULL,
    [IncGuia]                          BIT             CONSTRAINT [DF_COTIDET_IncGuia] DEFAULT ((0)) NOT NULL,
    [FueradeHorario]                   BIT             CONSTRAINT [DF_COTIDET_FueradeHorario] DEFAULT ((0)) NOT NULL,
    [UserMod]                          CHAR (4)        NOT NULL,
    [FecMod]                           DATETIME        CONSTRAINT [DF_COTIDET_FecMod] DEFAULT (getdate()) NOT NULL,
    [RedondeoTotal]                    NUMERIC (12, 4) NOT NULL,
    [DebitMemoPendiente]               BIT             DEFAULT ((1)) NOT NULL,
    [InsertadoporTI]                   BIT             DEFAULT ((0)) NULL,
    [IDDebitMemo]                      CHAR (10)       NULL,
    [CalculoTarifaPendiente]           BIT             DEFAULT ((0)) NOT NULL,
    [AcomodoEspecial]                  BIT             DEFAULT ((0)) NOT NULL,
    [FlServicioCorregidoTI_UpdCalculo] BIT             DEFAULT ((0)) NOT NULL,
    [FlServicioCorregidoTI_DelSubServ] BIT             DEFAULT ((0)) NOT NULL,
    [FlSSCREditado]                    BIT             DEFAULT ((0)) NOT NULL,
    [FlSTCREditado]                    BIT             DEFAULT ((0)) NOT NULL,
    [FlServicioTC]                     BIT             DEFAULT ((0)) NOT NULL,
    [IDDetAntiguo]                     INT             DEFAULT (NULL) NULL,
    CONSTRAINT [PK_COTIDET] PRIMARY KEY CLUSTERED ([IDDET] ASC),
    CONSTRAINT [FK_COTIDET_COTICAB] FOREIGN KEY ([IDCAB]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_COTIDET_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio]),
    CONSTRAINT [FK_COTIDET_MASERVICIOS_DET] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det]),
    CONSTRAINT [FK_MAPROVEEDORES_IDProv] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDCAB]
    ON [dbo].[COTIDET]([IDCAB] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_IDDetTransferOri]
    ON [dbo].[COTIDET]([IDDetTransferOri] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_IDDetTransferDest]
    ON [dbo].[COTIDET]([IDDetTransferDes] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_IDProveedor]
    ON [dbo].[COTIDET]([IDProveedor] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDCAB_DIA]
    ON [dbo].[COTIDET]([IDubigeo] ASC)
    INCLUDE([IDCAB], [Dia]);


GO
CREATE NONCLUSTERED INDEX [IX_DebitMemoPendiente]
    ON [dbo].[COTIDET]([DebitMemoPendiente] ASC)
    INCLUDE([IDCAB]);


GO
CREATE NONCLUSTERED INDEX [IX_IDUbigeo]
    ON [dbo].[COTIDET]([IDubigeo] ASC)
    INCLUDE([IDCAB], [Dia], [IDServicio_Det]);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_IDServicio]
    ON [dbo].[COTIDET]([IDServicio] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDProveedor]
    ON [dbo].[COTIDET]([IDProveedor] ASC)
    INCLUDE([IDCAB], [Dia], [IDServicio_Det]);


GO
CREATE NONCLUSTERED INDEX [IX_COTIDET_Dia_IDDetRel]
    ON [dbo].[COTIDET]([Dia] ASC, [IDDetRel] ASC);


GO
CREATE Trigger [dbo].[COTIDET_LOG2_Upd]  
  On [dbo].[COTIDET] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
 Declare @ExecTrigger bit --=(Select ExecTrigger From Inserted)  
   
 if (Select count(*) from Inserted) = 1 --And @ExecTrigger = 1  
  Begin   
   set @ExecTrigger = (Select ExecTrigger From Inserted)  
   
   If @ExecTrigger = 1
   begin
	   set @RegistroPK1 = (select IDDET from Inserted)  
	   set @UserMod = (select UserMod from Inserted)  
	   set @Accion = 'U'  
	   Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET',@UserMod,@RegistroPK1,'','','',@Accion     
   end
  End    

GO

--147
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIDET_LOG2_Ins]
  On dbo.COTIDET for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDET from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET',@UserMod,@RegistroPK1,'','','',@Accion
		End  
