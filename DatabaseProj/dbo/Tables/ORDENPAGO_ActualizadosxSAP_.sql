﻿CREATE TABLE [dbo].[ORDENPAGO_ActualizadosxSAP$] (
    [FechaEmisión] DATETIME       NULL,
    [FechaPago]    DATETIME       NULL,
    [IDOrdPago]    FLOAT (53)     NULL,
    [TipoVenta]    NVARCHAR (255) NULL,
    [File]         FLOAT (53)     NULL,
    [Proveedor]    NVARCHAR (255) NULL,
    [País]         NVARCHAR (255) NULL,
    [Banco]        NVARCHAR (255) NULL,
    [Moneda]       NVARCHAR (255) NULL,
    [Total]        FLOAT (53)     NULL,
    [F11]          NVARCHAR (255) NULL,
    [F12]          NVARCHAR (255) NULL,
    [F13]          NVARCHAR (255) NULL,
    [F14]          NVARCHAR (255) NULL,
    [F15]          NVARCHAR (255) NULL,
    [F16]          NVARCHAR (255) NULL
);

