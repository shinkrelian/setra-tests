﻿CREATE TABLE [dbo].[MADESAYUNOS] (
    [IDDesayuno]  CHAR (2)     NOT NULL,
    [Descripcion] VARCHAR (50) NOT NULL,
    [Sigla]       VARCHAR (3)  NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FecMod]      DATETIME     CONSTRAINT [DF__MADESAYUN__FecMo__0CA5D9DE] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MADESAYUNOS] PRIMARY KEY NONCLUSTERED ([IDDesayuno] ASC)
);


GO
	
	

--214
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MADESAYUNOS_LOG_Del]
  On dbo.MADESAYUNOS for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDesayuno from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MADESAYUNOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--211
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MADESAYUNOS_LOG_Upd]
  On dbo.MADESAYUNOS for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDesayuno from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MADESAYUNOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--208
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- select * from sys.triggers
-- select COUNT(*) from sys.tables where name like 'MA%'
-- select * from LOGAUDITORIA 
-- Tablas con Triggers de Auditoria

--> MADESAYUNOS,MAADMINISTCLIENTE,MAADMINISTPROVEEDORES,MABANCOS,MACATEGORIA,MACLIENTES,
--  MACOMENTARIOSPROVEEDOR,MACONTACTOSCLIENTE,MACONTACTOSPROVEEDOR,MACORREOREVPROVEEDOR,MADETALLE,
--  MAFECHASNAC,MAFERIADOS,MAFORMASPAGO,MAHABITTRIPLE,MAIDIOMAS,MAIDIOMASPROVEEDOR,MAINCLUIDO,
--  MAINCLUIDO_IDIOMAS,MAMODOSERVICIO,MAMONEDAS,MANIVELOPC,MANIVELUSUARIO,MAOPCIONES,MAPOLITICAPROVEEDORES,
--  MAPROVEEDORES,MARECORDATORIOSRESERVAS,MASEGUIMCLIENTE,MASERIES,MASERVICIOS,MASERVICIOS_ALIMENTACION_DIA
--  MAUSUARIOSOPC,MAUSUARIOS,MAUBIGEO,MATIPOUBIG,MATIPOSERVICIOS,MATIPOSERV,MATIPOPROVEEDOR,MATIPOOPERACION
--  MATIPOIDENT,MATIPOGASTO,MATIPODOC,MATIPOCAMBIO,MA--TextOSERVICIOS,MATARJETACREDITO,MASERVICIOS_DIA

--*--

--> VOUCHER_OPERACIONES,RESERVAS_TAREAS,RESERVAS_MENSAJESTELEF,RESERVAS_MENSAJES,
--  RESERVAS_DETSERVICIOS,RESERVAS_DET_PAX,RESERVAS_DET_BUP,PRESUPUESTOS_DET,PRESUPUESTOS,OPERACIONES_DET_PAX
--  OPERACIONES_DET_DETSERVICIOS,MASERVICIOS_DET

-- TABLA MADESAYUNO

CREATE Trigger [dbo].[MADESAYUNOS_LOG_Ins]
  On dbo.MADESAYUNOS for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDDesayuno from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MADESAYUNOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  
