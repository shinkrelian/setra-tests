﻿CREATE TABLE [dbo].[MASERVICIOS_DET_RANGO_APT] (
    [Correlativo]    TINYINT         NOT NULL,
    [IDServicio_Det] INT             NOT NULL,
    [PaxDesde]       SMALLINT        NOT NULL,
    [PaxHasta]       SMALLINT        NOT NULL,
    [Monto]          NUMERIC (10, 4) NOT NULL,
    [UserMod]        CHAR (4)        NOT NULL,
    [FecMod]         DATETIME        CONSTRAINT [DF_MASERVICIOS_DET_RANGO_APT_FecMod] DEFAULT (getdate()) NOT NULL,
    [IDTemporada]    INT             NOT NULL,
    CONSTRAINT [PK_MASERVICIOS_DET_RANGO_APT] PRIMARY KEY CLUSTERED ([Correlativo] ASC, [IDServicio_Det] ASC, [IDTemporada] ASC),
    CONSTRAINT [FK_MASERVICIOS_DET_RANGO_APT_MASERVICIOS_DET_DESC_APT] FOREIGN KEY ([IDServicio_Det], [IDTemporada]) REFERENCES [dbo].[MASERVICIOS_DET_DESC_APT] ([IDServicio_Det], [IDTemporada])
);

