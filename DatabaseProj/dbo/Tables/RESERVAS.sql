﻿CREATE TABLE [dbo].[RESERVAS] (
    [IDReserva]               INT           NOT NULL,
    [Fecha]                   SMALLDATETIME CONSTRAINT [DF_RESERVAS_Fecha] DEFAULT (getdate()) NOT NULL,
    [IDProveedor]             CHAR (6)      NOT NULL,
    [IDCab]                   INT           NOT NULL,
    [IDFile]                  CHAR (8)      NOT NULL,
    [Estado]                  CHAR (2)      NOT NULL,
    [UserModEstado]           CHAR (4)      NULL,
    [IDEmailModEstado]        VARCHAR (255) NULL,
    [IDUsuarioVen]            CHAR (4)      NOT NULL,
    [IDUsuarioRes]            CHAR (4)      NOT NULL,
    [Observaciones]           VARCHAR (MAX) NULL,
    [CodReservaProv]          VARCHAR (50)  NULL,
    [UserMod]                 CHAR (4)      NOT NULL,
    [FecMod]                  DATETIME      CONSTRAINT [DF__RESERVAS__FecMod__4C171ED4] DEFAULT (getdate()) NOT NULL,
    [CambiosVtasAceptados]    BIT           DEFAULT ((0)) NOT NULL,
    [EstadoSolicitud]         CHAR (4)      NOT NULL,
    [Anulado]                 BIT           DEFAULT ((0)) NOT NULL,
    [RegeneradoxAcomodo]      BIT           DEFAULT ((0)) NOT NULL,
    [CambiosenVentasPostFile] BIT           DEFAULT ((0)) NOT NULL,
    [IDReservaProtecc]        INT           NULL,
    CONSTRAINT [PK_RESERVAS] PRIMARY KEY NONCLUSTERED ([IDReserva] ASC),
    CONSTRAINT [FK_RESERVAS_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_RESERVAS_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_RESERVAS_MAUSUARIOS_Res] FOREIGN KEY ([IDUsuarioRes]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario]),
    CONSTRAINT [FK_RESERVAS_MAUSUARIOS_Ven] FOREIGN KEY ([IDUsuarioVen]) REFERENCES [dbo].[MAUSUARIOS] ([IDUsuario])
);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS_IDReservaProtecc]
    ON [dbo].[RESERVAS]([IDReservaProtecc] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_Proveedor]
    ON [dbo].[RESERVAS]([IDProveedor] ASC, [IDCab] ASC, [Anulado] ASC, [IDReservaProtecc] ASC, [Estado] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDCab]
    ON [dbo].[RESERVAS]([IDCab] ASC, [Anulado] ASC, [Estado] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS]
    ON [dbo].[RESERVAS]([Anulado] ASC, [Estado] ASC)
    INCLUDE([IDReserva], [IDProveedor], [IDCab]);


GO
Create Trigger [dbo].[RESERVAS_LOG_Ins]  
  On [dbo].[RESERVAS] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDReserva from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_LOG_Del]  
  On [dbo].[RESERVAS] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDReserva from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_LOG_Upd]  
  On [dbo].[RESERVAS] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDReserva from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
