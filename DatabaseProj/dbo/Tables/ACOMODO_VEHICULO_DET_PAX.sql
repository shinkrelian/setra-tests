﻿CREATE TABLE [dbo].[ACOMODO_VEHICULO_DET_PAX] (
    [NuVehiculo]    SMALLINT NOT NULL,
    [NuNroVehiculo] TINYINT  NOT NULL,
    [IDDet]         INT      NOT NULL,
    [NuPax]         INT      NOT NULL,
    [FlGuia]        BIT      NOT NULL,
    [IDReserva_Det] INT      NULL,
    [UserMod]       CHAR (4) NOT NULL,
    [FecMod]        DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_ACOMODO_VEHICULO_DET_PAX] PRIMARY KEY NONCLUSTERED ([NuVehiculo] ASC, [NuNroVehiculo] ASC, [IDDet] ASC, [NuPax] ASC),
    CONSTRAINT [FK_ACOMODO_VEHICULO_DET_PAX_ACOMODO_VEHICULO] FOREIGN KEY ([NuVehiculo], [IDDet]) REFERENCES [dbo].[ACOMODO_VEHICULO] ([NuVehiculo], [IDDet]),
    CONSTRAINT [FK_ACOMODO_VEHICULO_DET_PAX_RESERVAS_DET] FOREIGN KEY ([IDReserva_Det]) REFERENCES [dbo].[RESERVAS_DET] ([IDReserva_Det])
);

