﻿CREATE TABLE [dbo].[MAIDIOMASPROVEEDOR] (
    [IDIdioma]    VARCHAR (12) NOT NULL,
    [IDProveedor] CHAR (6)     NOT NULL,
    [Nivel]       CHAR (2)     NOT NULL,
    [UserMod]     CHAR (4)     NOT NULL,
    [FechaMod]    DATETIME     DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAIDIOMASPROVEEDOR] PRIMARY KEY CLUSTERED ([IDIdioma] ASC, [IDProveedor] ASC),
    CONSTRAINT [FK_MAIDIOMASPROVEEDOR_MAIDIOMAS] FOREIGN KEY ([IDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_MAIDIOMASPROVEEDOR_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO

--70
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAIDIOMASPROVEEDOR_LOG_Del]
  On [dbo].[MAIDIOMASPROVEEDOR] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIdioma from Deleted)
			set @RegistroPK2 = (select IDProveedor from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAIDIOMASPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--68
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAIDIOMASPROVEEDOR_LOG_Upd]
  On [dbo].[MAIDIOMASPROVEEDOR] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIdioma from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAIDIOMASPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--66
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAIDIOMASPROVEEDOR

CREATE Trigger [dbo].[MAIDIOMASPROVEEDOR_LOG_Ins]
  On [dbo].[MAIDIOMASPROVEEDOR] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDIdioma from Inserted)
			set @RegistroPK2 = (select IDProveedor from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAIDIOMASPROVEEDOR',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
