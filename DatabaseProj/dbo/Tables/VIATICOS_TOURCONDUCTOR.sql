﻿CREATE TABLE [dbo].[VIATICOS_TOURCONDUCTOR] (
    [NuViaticoTC]    INT            NOT NULL,
    [Dia]            SMALLDATETIME  NOT NULL,
    [IDCab]          INT            NOT NULL,
    [IDServicio_Det] INT            NOT NULL,
    [Item]           SMALLINT       NOT NULL,
    [CostoUSD]       NUMERIC (8, 2) NOT NULL,
    [CostoSOL]       NUMERIC (8, 2) NOT NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       DEFAULT (getdate()) NOT NULL,
    [FlActivo]       BIT            DEFAULT ((1)) NOT NULL,
    CONSTRAINT [PK_VIATICOS_TOURCONDUCTOR_NuViaticoTC] PRIMARY KEY CLUSTERED ([NuViaticoTC] ASC),
    CONSTRAINT [FK_VIATICOS_TOURCONDUCTOR_IDServicio_Det] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det])
);


GO
CREATE NONCLUSTERED INDEX [IX_VIATICOS_TOURCONDUCTOR_IDCab_FlActivo]
    ON [dbo].[VIATICOS_TOURCONDUCTOR]([IDCab] ASC, [FlActivo] ASC);

