﻿CREATE TABLE [dbo].[OPERACIONES] (
    [IDOperacion]               INT           NOT NULL,
    [IDReserva]                 INT           NULL,
    [Fecha]                     SMALLDATETIME DEFAULT (getdate()) NOT NULL,
    [IDCab]                     INT           NOT NULL,
    [IDFile]                    CHAR (8)      NOT NULL,
    [IDProveedor]               CHAR (6)      NOT NULL,
    [Observaciones]             VARCHAR (250) NULL,
    [UserMod]                   CHAR (4)      NOT NULL,
    [FecMod]                    DATETIME      DEFAULT (getdate()) NOT NULL,
    [CambiosenReservasPostFile] BIT           DEFAULT ((0)) NOT NULL,
    [NuVouPTrenInt]             INT           NULL,
    CONSTRAINT [PK_OPERACIONES] PRIMARY KEY NONCLUSTERED ([IDOperacion] ASC),
    CONSTRAINT [FK_OPERACIONES_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_OPERACIONES_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_OPERACIONES_RESERVAS] FOREIGN KEY ([IDReserva]) REFERENCES [dbo].[RESERVAS] ([IDReserva])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDCAB]
    ON [dbo].[OPERACIONES]([IDCab] ASC);


GO
CREATE NONCLUSTERED INDEX [OPERACIONES_IDReserva]
    ON [dbo].[OPERACIONES]([IDReserva] ASC);


GO

--80
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[OPERACIONES_LOG_Ins]
  On [dbo].[OPERACIONES] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOperacion from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--82
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[OPERACIONES_LOG_Upd]
  On [dbo].[OPERACIONES] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOperacion from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--84
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[OPERACIONES_LOG_Del]
  On [dbo].[OPERACIONES] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDOperacion from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'OPERACIONES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
