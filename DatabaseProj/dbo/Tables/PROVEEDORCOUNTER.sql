﻿CREATE TABLE [dbo].[PROVEEDORCOUNTER] (
    [CoProveedor]   CHAR (6)      NOT NULL,
    [NoRazonSocial] VARCHAR (100) NOT NULL,
    [CoGDS_ID]      VARCHAR (20)  NOT NULL,
    [flActivo]      BIT           NOT NULL,
    [UserMod]       CHAR (4)      NOT NULL,
    [FecMod]        DATETIME      NOT NULL,
    CONSTRAINT [PK_PROVEEDORCOUNTER] PRIMARY KEY NONCLUSTERED ([CoProveedor] ASC)
);

