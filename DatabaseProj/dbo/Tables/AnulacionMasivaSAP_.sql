﻿CREATE TABLE [dbo].[AnulacionMasivaSAP$] (
    [U_SYP_MDTD]    FLOAT (53)     NULL,
    [U_SYP_MDSD]    FLOAT (53)     NULL,
    [U_SYP_MDCD]    NVARCHAR (255) NULL,
    [CardCode]      NVARCHAR (255) NULL,
    [U_SYP_STATUS]  NVARCHAR (255) NULL,
    [U_SYP_TCOMPRA] NVARCHAR (255) NULL
);

