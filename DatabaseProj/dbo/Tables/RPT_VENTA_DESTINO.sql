﻿CREATE TABLE [dbo].[RPT_VENTA_DESTINO] (
    [NuAnio]      CHAR (4) NOT NULL,
    [CoUbigeo]    CHAR (6) NOT NULL,
    [QtServicios] INT      NULL,
    [UserMod]     CHAR (4) NULL,
    [FecMod]      DATETIME CONSTRAINT [DF_RPT_VENTA_DESTINO_FecMod] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_RPT_VENTA_DESTINO_1] PRIMARY KEY CLUSTERED ([NuAnio] ASC, [CoUbigeo] ASC)
);

