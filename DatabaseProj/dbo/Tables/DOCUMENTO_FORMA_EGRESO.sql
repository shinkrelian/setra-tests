﻿CREATE TABLE [dbo].[DOCUMENTO_FORMA_EGRESO] (
    [IDDocFormaEgreso] INT             NOT NULL,
    [TipoFormaEgreso]  CHAR (3)        NOT NULL,
    [CoMoneda]         CHAR (3)        NOT NULL,
    [CoEstado]         CHAR (2)        DEFAULT ('PD') NULL,
    [SsMontoTotal]     NUMERIC (14, 4) DEFAULT (NULL) NULL,
    [SsSaldo]          NUMERIC (14, 4) NULL,
    [SsDifAceptada]    NUMERIC (14, 4) NULL,
    [UserMod]          CHAR (4)        NOT NULL,
    [FecMod]           DATETIME        DEFAULT (getdate()) NOT NULL,
    [TxObsDocumento]   VARCHAR (MAX)   NULL,
    [FeCreacion]       DATETIME        NULL,
    [CoUbigeo_Oficina] CHAR (6)        NULL,
    CONSTRAINT [PK_DOCUMENTO_FORMA_EGRESO] PRIMARY KEY NONCLUSTERED ([IDDocFormaEgreso] ASC, [TipoFormaEgreso] ASC, [CoMoneda] ASC),
    CONSTRAINT [FK_DOCUMENTO_FORMA_EGRESO_ESTADO_OBLIGACIONESPAGO] FOREIGN KEY ([CoEstado]) REFERENCES [dbo].[ESTADO_OBLIGACIONESPAGO] ([CoEstado])
);

