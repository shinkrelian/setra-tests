﻿CREATE TABLE [dbo].[RPT_VENTA_EJECUTIVO] (
    [NuAnio]          CHAR (4)        NOT NULL,
    [CoUserEjecutivo] CHAR (4)        NOT NULL,
    [SsImporte]       NUMERIC (12, 4) NULL,
    [QtNumFiles]      TINYINT         NULL,
    [UserMod]         CHAR (4)        NULL,
    [FecMod]          DATETIME        CONSTRAINT [DF_RPT_VENTA_EJECUTIVO_FecMod] DEFAULT (getdate()) NULL,
    CONSTRAINT [PK_RPT_VENTA_EJECUTIVO_1] PRIMARY KEY CLUSTERED ([NuAnio] ASC, [CoUserEjecutivo] ASC)
);

