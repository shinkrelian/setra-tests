﻿CREATE TABLE [dbo].[MAPRODUCTOS] (
    [CoProducto]    CHAR (6)       NOT NULL,
    [NoProducto]    NVARCHAR (100) NULL,
    [CoRubro]       TINYINT        NULL,
    [CoSAP]         NVARCHAR (20)  NULL,
    [FlActivo]      BIT            CONSTRAINT [DF_MAPRODUCTOS_FlActivo] DEFAULT ((1)) NOT NULL,
    [UserMod]       CHAR (4)       NOT NULL,
    [FecMod]        DATETIME       CONSTRAINT [DF_MAPRODUCTOS_FecMod] DEFAULT (getdate()) NOT NULL,
    [CoAlmacen]     CHAR (6)       NULL,
    [CoCtaContable] VARCHAR (14)   NULL,
    CONSTRAINT [PK_MAPRODUCTOS] PRIMARY KEY CLUSTERED ([CoProducto] ASC)
);

