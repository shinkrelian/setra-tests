﻿CREATE TABLE [dbo].[COTIVUELOS] (
    [ID]                     INT             IDENTITY (1, 1) NOT NULL,
    [IdDet]                  INT             NULL,
    [IDCAB]                  INT             NULL,
    [Cotizacion]             CHAR (8)        NULL,
    [Servicio]               VARCHAR (250)   NULL,
    [NombrePax]              VARCHAR (100)   NULL,
    [Ruta]                   VARCHAR (100)   NULL,
    [Num_Boleto]             CHAR (12)       NULL,
    [RutaD]                  CHAR (6)        NULL,
    [RutaO]                  CHAR (6)        NULL,
    [Vuelo]                  VARCHAR (20)    NULL,
    [Fec_Emision]            SMALLDATETIME   NULL,
    [Fec_Documento]          SMALLDATETIME   NULL,
    [Fec_Salida]             SMALLDATETIME   NULL,
    [Fec_Retorno]            SMALLDATETIME   NULL,
    [Salida]                 VARCHAR (20)    NULL,
    [HrRecojo]               VARCHAR (20)    NULL,
    [PaxC]                   SMALLINT        NULL,
    [PCotizado]              NUMERIC (10, 2) NULL,
    [TCotizado]              NUMERIC (10, 2) NULL,
    [PaxV]                   SMALLINT        NULL,
    [PVolado]                NUMERIC (10, 2) NULL,
    [TVolado]                NUMERIC (10, 2) NULL,
    [Status]                 CHAR (2)        NULL,
    [Comentario]             TEXT            NULL,
    [Llegada]                VARCHAR (20)    NULL,
    [Emitido]                CHAR (1)        NULL,
    [TipoTransporte]         CHAR (1)        NOT NULL,
    [CodReserva]             VARCHAR (20)    NULL,
    [Tarifa]                 NUMERIC (10, 2) NULL,
    [PTA]                    NUMERIC (10, 2) NULL,
    [ImpuestosExt]           NUMERIC (10, 2) NULL,
    [Penalidad]              NUMERIC (10, 2) NULL,
    [IGV]                    NUMERIC (10, 2) NULL,
    [Otros]                  NUMERIC (10, 2) NULL,
    [PorcComm]               NUMERIC (8, 2)  NULL,
    [MontoContado]           NUMERIC (10, 2) NULL,
    [MontoTarjeta]           NUMERIC (10, 2) NULL,
    [IdTarjCred]             CHAR (3)        NULL,
    [NumeroTarjeta]          VARCHAR (20)    NULL,
    [Aprobacion]             VARCHAR (20)    NULL,
    [PorcOver]               NUMERIC (8, 2)  NULL,
    [PorcOverInCom]          NUMERIC (8, 2)  NULL,
    [MontComm]               NUMERIC (10, 2) NULL,
    [MontCommOver]           NUMERIC (10, 2) NULL,
    [IGVComm]                NUMERIC (10, 2) NULL,
    [IGVCommOver]            NUMERIC (10, 2) NULL,
    [PorcTarifa]             NUMERIC (10, 2) NULL,
    [NetoPagar]              NUMERIC (10, 2) NULL,
    [Descuento]              NUMERIC (10, 2) NULL,
    [IdLineaA]               CHAR (6)        NOT NULL,
    [MontoFEE]               NUMERIC (10, 2) NULL,
    [IGVFEE]                 NUMERIC (10, 2) NULL,
    [TotalFEE]               NUMERIC (10, 2) NULL,
    [EmitidoSetours]         BIT             CONSTRAINT [DF__COTIVUELO__Emiti__1451E89E] DEFAULT ((0)) NOT NULL,
    [EmitidoOperador]        BIT             CONSTRAINT [DF__COTIVUELO__Emiti__228AF95C] DEFAULT ((0)) NOT NULL,
    [IDProveedorOperador]    CHAR (6)        NULL,
    [CostoOperador]          NUMERIC (8, 2)  NULL,
    [UserMod]                CHAR (4)        NULL,
    [FecMod]                 DATETIME        CONSTRAINT [DF_COTIVUELOS_FecMod] DEFAULT (getdate()) NULL,
    [IdaVuelta]              CHAR (1)        NULL,
    [IDServicio_DetPeruRail] INT             NULL,
    [FEC_EMISION_BUP]        SMALLDATETIME   NULL,
    [CoTicket]               CHAR (13)       NULL,
    [NuSegmento]             TINYINT         NULL,
    [FlUpdExcelPRail]        BIT             DEFAULT ((0)) NOT NULL,
    [FlMotivo]               BIT             DEFAULT ((0)) NOT NULL,
    [DescMotivo]             VARCHAR (200)   NULL,
    [CostoOperadorOriginal]  NUMERIC (8, 2)  DEFAULT ((0)) NULL,
    CONSTRAINT [PK_COTIVUELOS_ID] PRIMARY KEY CLUSTERED ([ID] ASC),
    CONSTRAINT [COTIVUELOS_FK_TICKET_AMADEUS_ITINERARIO] FOREIGN KEY ([CoTicket], [NuSegmento]) REFERENCES [dbo].[TICKET_AMADEUS_ITINERARIO] ([CoTicket], [NuSegmento]),
    CONSTRAINT [FK_COTIVUELOS_IdLineaA] FOREIGN KEY ([IdLineaA]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_COTIVUELOS_MAPROVEEDORES_OPERADOR] FOREIGN KEY ([IDProveedorOperador]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_COTIVUELOS_RUTAD] FOREIGN KEY ([RutaD]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo]),
    CONSTRAINT [FK_COTIVUELOS_RUTAO] FOREIGN KEY ([RutaO]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO
CREATE NONCLUSTERED INDEX [IX_COTIVUELOS_IDCAB]
    ON [dbo].[COTIVUELOS]([IDCAB] ASC);


GO
--59
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIVUELOS_LOG_Upd]
  On dbo.COTIVUELOS for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select ID from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIVUELOS',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--61
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIVUELOS_LOG_Del]
  On dbo.COTIVUELOS for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select ID from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIVUELOS',@UserMod,@RegistroPK1,'','','',@Accion
		End

GO
--55
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTIVUELOS_LOG_Ins]
  On dbo.COTIVUELOS for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select ID from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTIVUELOS',@UserMod,@RegistroPK1,'','','',@Accion
		End
