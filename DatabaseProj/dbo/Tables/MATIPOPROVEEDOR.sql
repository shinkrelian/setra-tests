﻿CREATE TABLE [dbo].[MATIPOPROVEEDOR] (
    [IDTipoProv]            CHAR (3)       NOT NULL,
    [Descripcion]           VARCHAR (30)   NOT NULL,
    [PideIdioma]            BIT            NOT NULL,
    [Comision]              NUMERIC (6, 2) NOT NULL,
    [UserMod]               CHAR (4)       NOT NULL,
    [FecMod]                DATETIME       CONSTRAINT [DF__MATIPOPRO__FecMo__3A179ED3] DEFAULT (getdate()) NOT NULL,
    [FlTieneControlCalidad] BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MATIPOPROVEEDOR] PRIMARY KEY CLUSTERED ([IDTipoProv] ASC)
);


GO

--175
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


-- TABLA MATIPOPROVEEDOR

CREATE Trigger [dbo].[MATIPOPROVEEDOR_LOG_Ins]
  On [dbo].[MATIPOPROVEEDOR] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDTipoProv from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOPROVEEDOR',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--179
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOPROVEEDOR_LOG_Del]
  On [dbo].[MATIPOPROVEEDOR] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDTipoProv from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOPROVEEDOR',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--177
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOPROVEEDOR_LOG_Upd]
  On [dbo].[MATIPOPROVEEDOR] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDTipoProv from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOPROVEEDOR',@UserMod,@RegistroPK1,'','','',@Accion
		End  
