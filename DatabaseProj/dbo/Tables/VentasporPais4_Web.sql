﻿CREATE TABLE [dbo].[VentasporPais4_Web] (
    [Posicion]            BIGINT           NULL,
    [Posicion_AnioAnt]    BIGINT           NULL,
    [CoPais]              CHAR (6)         NULL,
    [NoPais]              VARCHAR (60)     NULL,
    [CoRegion]            CHAR (2)         NULL,
    [Region]              VARCHAR (9)      NOT NULL,
    [Total]               NUMERIC (38, 6)  NULL,
    [Total_AnioAnt]       NUMERIC (38, 4)  NOT NULL,
    [Crecimiento]         INT              NULL,
    [Dias_Actual]         INT              NOT NULL,
    [Edad_Prom_Actual]    INT              NOT NULL,
    [Dias]                INT              NOT NULL,
    [Edad_Prom]           INT              NOT NULL,
    [Cant_Clientes]       INT              NOT NULL,
    [Cant_Clientes_Ant]   INT              NOT NULL,
    [ParamAnio]           CHAR (4)         NULL,
    [TicketFile]          NUMERIC (23, 15) NULL,
    [TicketFileAnioAnter] NUMERIC (23, 15) NULL
);

