﻿CREATE TABLE [dbo].[MAPOLITICAPROVEEDORES] (
    [IDPolitica]           INT            IDENTITY (1, 1) NOT NULL,
    [IDProveedor]          CHAR (6)       NOT NULL,
    [Nombre]               VARCHAR (120)  NOT NULL,
    [DefinDia]             SMALLINT       NOT NULL,
    [DefinHasta]           SMALLINT       NOT NULL,
    [DefinTipo]            CHAR (6)       NOT NULL,
    [RoomPreeliminar]      SMALLINT       NOT NULL,
    [RoomActualizado]      SMALLINT       NOT NULL,
    [RoomFinal]            SMALLINT       NOT NULL,
    [PrePag1_Dias]         SMALLINT       NOT NULL,
    [PrePag1_Porc]         NUMERIC (5, 2) NOT NULL,
    [PrePag1_Mont]         NUMERIC (8, 2) NOT NULL,
    [PrePag2_Dias]         SMALLINT       NOT NULL,
    [PrePag2_Porc]         NUMERIC (5, 2) NOT NULL,
    [PrePag2_Mont]         NUMERIC (8, 2) NOT NULL,
    [Saldo_Dias]           SMALLINT       NOT NULL,
    [Saldo_Porc]           NUMERIC (5, 2) NOT NULL,
    [Saldo_Mont]           NUMERIC (8, 2) NOT NULL,
    [UserMod]              CHAR (4)       NULL,
    [FecMod]               DATETIME       CONSTRAINT [DF_MAPOLITICAPROVEEDORES_FecMod] DEFAULT (getdate()) NULL,
    [DiasSinPenalidad]     SMALLINT       NOT NULL,
    [PoliticaFechaReserva] BIT            DEFAULT ((0)) NOT NULL,
    [Anio]                 CHAR (4)       NULL,
    CONSTRAINT [Pk_MAPOLITICAPROVEEDORES_IDPolitica] PRIMARY KEY CLUSTERED ([IDPolitica] ASC),
    CONSTRAINT [CK_MAPOLITICAPROVEEDORES] CHECK ([Anio] IS NOT NULL),
    CONSTRAINT [Fk_MAPOLITICAPROVEEDORES_IDProveedor] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor])
);


GO

--109
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAPOLITICAPROVEEDORES_LOG_Upd]
  On dbo.MAPOLITICAPROVEEDORES for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDPolitica from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAPOLITICAPROVEEDORES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--111
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MAPOLITICAPROVEEDORES_LOG_Del]
  On dbo.MAPOLITICAPROVEEDORES for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDPolitica from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAPOLITICAPROVEEDORES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--107
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MAPOLITICAPROVEEDORES

CREATE Trigger [dbo].[MAPOLITICAPROVEEDORES_LOG_Ins]
  On dbo.MAPOLITICAPROVEEDORES for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDPolitica from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAPOLITICAPROVEEDORES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
