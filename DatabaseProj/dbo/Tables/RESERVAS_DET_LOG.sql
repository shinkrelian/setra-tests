﻿CREATE TABLE [dbo].[RESERVAS_DET_LOG] (
    [ID]                  INT             IDENTITY (1, 1) NOT NULL,
    [FechaLog]            DATETIME        CONSTRAINT [DF_RESERVAS_DET_LOG_FechaLog] DEFAULT (getdate()) NOT NULL,
    [Accion]              CHAR (1)        NOT NULL,
    [UserLog]             CHAR (4)        NOT NULL,
    [Atendido]            BIT             CONSTRAINT [DF_RESERVAS_DET_LOG_Atendido] DEFAULT ((0)) NOT NULL,
    [IDReserva_Det]       INT             NOT NULL,
    [IDReserva]           INT             NOT NULL,
    [IDFile]              CHAR (8)        NOT NULL,
    [IDDet]               INT             NULL,
    [Item]                NUMERIC (6, 3)  NOT NULL,
    [Dia]                 SMALLDATETIME   NOT NULL,
    [DiaNombre]           VARCHAR (10)    NOT NULL,
    [FechaOut]            SMALLDATETIME   NULL,
    [Noches]              TINYINT         NULL,
    [IDubigeo]            CHAR (6)        NOT NULL,
    [IDServicio]          CHAR (8)        NOT NULL,
    [IDServicio_Det]      INT             NOT NULL,
    [IDIdioma]            VARCHAR (12)    NOT NULL,
    [Especial]            BIT             NOT NULL,
    [MotivoEspecial]      VARCHAR (200)   NULL,
    [RutaDocSustento]     VARCHAR (200)   NULL,
    [Desayuno]            BIT             NOT NULL,
    [Lonche]              BIT             NOT NULL,
    [Almuerzo]            BIT             NOT NULL,
    [Cena]                BIT             NOT NULL,
    [Transfer]            BIT             NOT NULL,
    [IDDetTransferOri]    INT             NULL,
    [IDDetTransferDes]    INT             NULL,
    [CamaMat]             TINYINT         NULL,
    [TipoTransporte]      CHAR (1)        NULL,
    [IDUbigeoOri]         CHAR (6)        NOT NULL,
    [IDUbigeoDes]         CHAR (6)        NOT NULL,
    [NroPax]              SMALLINT        NOT NULL,
    [NroLiberados]        SMALLINT        NULL,
    [Tipo_Lib]            CHAR (1)        NULL,
    [Cantidad]            TINYINT         NOT NULL,
    [CantidadAPagar]      NUMERIC (5, 2)  NOT NULL,
    [CostoReal]           NUMERIC (8, 2)  NOT NULL,
    [CostoRealAnt]        NUMERIC (8, 2)  NULL,
    [CostoLiberado]       NUMERIC (8, 2)  NOT NULL,
    [Margen]              NUMERIC (8, 2)  NOT NULL,
    [MargenAplicado]      NUMERIC (6, 2)  NOT NULL,
    [MargenAplicadoAnt]   NUMERIC (6, 2)  NULL,
    [MargenLiberado]      NUMERIC (8, 2)  NOT NULL,
    [Total]               NUMERIC (8, 2)  NOT NULL,
    [TotalOrig]           NUMERIC (8, 2)  NOT NULL,
    [CostoRealImpto]      NUMERIC (8, 2)  NOT NULL,
    [CostoLiberadoImpto]  NUMERIC (8, 2)  NOT NULL,
    [MargenImpto]         NUMERIC (8, 2)  NOT NULL,
    [MargenLiberadoImpto] NUMERIC (8, 2)  NOT NULL,
    [TotImpto]            NUMERIC (8, 2)  NOT NULL,
    [NetoHab]             NUMERIC (8, 2)  NOT NULL,
    [IgvHab]              NUMERIC (5, 2)  NOT NULL,
    [TotalHab]            NUMERIC (8, 2)  NOT NULL,
    [NetoGen]             NUMERIC (12, 2) NOT NULL,
    [IgvGen]              NUMERIC (5, 2)  NOT NULL,
    [TotalGen]            NUMERIC (12, 2) NOT NULL,
    [IDDetRel]            INT             NULL,
    [Servicio]            VARCHAR (MAX)   NULL,
    [IDReserva_DetCopia]  INT             NULL,
    [IDReserva_Det_Rel]   INT             NULL,
    [IDEmailRef]          VARCHAR (255)   NULL,
    [IDEmailEdit]         VARCHAR (255)   NULL,
    [IDEmailNew]          VARCHAR (255)   NULL,
    [CapacidadHab]        TINYINT         NULL,
    [EsMatrimonial]       BIT             NOT NULL,
    [UserMod]             CHAR (4)        NOT NULL,
    [FecMod]              DATETIME        NOT NULL,
    [IDMoneda]            CHAR (3)        NULL,
    [IDGuiaProveedor]     CHAR (6)        NULL,
    [NuVehiculo]          SMALLINT        NULL,
    [UserModAtendidoLog]  CHAR (4)        NULL,
    [FecModAtendidoLog]   DATETIME        NULL,
    CONSTRAINT [PK_RESERVAS_DET_LOG] PRIMARY KEY NONCLUSTERED ([ID] ASC),
    CONSTRAINT [CK_RESERVAS_DET_LOG] CHECK ([Accion]='N' OR [Accion]='M' OR [Accion]='B' OR [Accion]=' ')
);


GO
CREATE NONCLUSTERED INDEX [IX_Accion_Atendido_IDServicio_Det]
    ON [dbo].[RESERVAS_DET_LOG]([Accion] ASC, [Atendido] ASC, [IDServicio_Det] ASC);


GO
Create Trigger [dbo].[RESERVAS_DET_LOG_LOG_Ins]  
  On [dbo].[RESERVAS_DET_LOG] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select ID from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_LOG',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_DET_LOG_LOG_Upd]  
  On [dbo].[RESERVAS_DET_LOG] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select ID from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_LOG',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_DET_LOG_LOG_Del]  
  On [dbo].[RESERVAS_DET_LOG] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select ID from Deleted)  
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET_LOG',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
EXECUTE sp_addextendedproperty @name = N'MS_Description', @value = N'([Accion]=''N'' OR [Accion]=''M'' OR [Accion]=''B'' OR [Accion]='' '')', @level0type = N'SCHEMA', @level0name = N'dbo', @level1type = N'TABLE', @level1name = N'RESERVAS_DET_LOG', @level2type = N'CONSTRAINT', @level2name = N'CK_RESERVAS_DET_LOG';

