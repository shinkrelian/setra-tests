﻿CREATE TABLE [dbo].[COTICAB_QUIEBRES] (
    [IDCAB_Q]   INT      IDENTITY (1, 1) NOT NULL,
    [IDCAB]     INT      NOT NULL,
    [Pax]       SMALLINT NOT NULL,
    [Liberados] SMALLINT NULL,
    [Tipo_Lib]  CHAR (1) NULL,
    [UserMod]   CHAR (4) NOT NULL,
    [FecMod]    DATETIME CONSTRAINT [DF_COTICAB_QUIEBRES_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTICAB_QUIEBRES_1] PRIMARY KEY NONCLUSTERED ([IDCAB_Q] ASC),
    CONSTRAINT [FK_COTICAB_QUIEBRES_COTICAB] FOREIGN KEY ([IDCAB]) REFERENCES [dbo].[COTICAB] ([IDCAB])
);


GO
CREATE NONCLUSTERED INDEX [IX_COTICAB_QUIEBRES_IDCab_Pax]
    ON [dbo].[COTICAB_QUIEBRES]([IDCAB] ASC, [Pax] ASC);


GO

--110
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTICAB_QUIEBRES_LOG_Upd]
  On [dbo].[COTICAB_QUIEBRES] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB_Q from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTICAB_QUIEBRES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--112
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[COTICAB_QUIEBRES_LOG_Del]
  On [dbo].[COTICAB_QUIEBRES] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB_Q from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTICAB_QUIEBRES',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--108
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[COTICAB_QUIEBRES_LOG_Ins]
  On [dbo].[COTICAB_QUIEBRES] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDCAB_Q from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'COTICAB_QUIEBRES',@UserMod,@RegistroPK1,'','','',@Accion
		End  
