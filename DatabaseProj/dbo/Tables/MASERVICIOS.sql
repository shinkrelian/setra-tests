﻿CREATE TABLE [dbo].[MASERVICIOS] (
    [IDServicio]            CHAR (8)        NOT NULL,
    [IDProveedor]           CHAR (6)        NOT NULL,
    [IDTipoProv]            CHAR (3)        NOT NULL,
    [IDTipoServ]            CHAR (3)        NOT NULL,
    [Transfer]              BIT             NOT NULL,
    [TipoTransporte]        CHAR (1)        NULL,
    [Activo]                BIT             CONSTRAINT [DF_MASERVICIOS_Activo] DEFAULT ((1)) NOT NULL,
    [MotivoDesactivacion]   VARCHAR (200)   NULL,
    [Descripcion]           VARCHAR (100)   NOT NULL,
    [Tarifario]             BIT             CONSTRAINT [DF_MASERVICIOS_Tarifario] DEFAULT ((0)) NOT NULL,
    [Descri_tarifario]      VARCHAR (100)   NULL,
    [IDubigeo]              CHAR (6)        NOT NULL,
    [Telefono]              VARCHAR (50)    NULL,
    [Celular]               VARCHAR (50)    NULL,
    [Comision]              NUMERIC (7, 4)  NULL,
    [UpdComision]           BIT             CONSTRAINT [DF_MASERVICIOS_UpdComision_1] DEFAULT ((0)) NOT NULL,
    [IDCat]                 CHAR (3)        NOT NULL,
    [Categoria]             TINYINT         NULL,
    [Capacidad]             SMALLINT        NULL,
    [Observaciones]         TEXT            NULL,
    [Lectura]               TEXT            NULL,
    [FecCaducLectura]       SMALLDATETIME   NULL,
    [AtencionLunes]         BIT             CONSTRAINT [DF_MASERVICIOS_AtencionLunes] DEFAULT ((1)) NOT NULL,
    [AtencionMartes]        BIT             CONSTRAINT [DF_MASERVICIOS_AtencionMartes] DEFAULT ((1)) NOT NULL,
    [AtencionMiercoles]     BIT             CONSTRAINT [DF_MASERVICIOS_AtencionMiercoles] DEFAULT ((1)) NOT NULL,
    [AtencionJueves]        BIT             CONSTRAINT [DF_MASERVICIOS_AtencionJueves] DEFAULT ((1)) NOT NULL,
    [AtencionViernes]       BIT             CONSTRAINT [DF_MASERVICIOS_AtencionViernes] DEFAULT ((1)) NOT NULL,
    [AtencionSabado]        BIT             CONSTRAINT [DF_MASERVICIOS_AtencionSabado] DEFAULT ((1)) NOT NULL,
    [AtencionDomingo]       BIT             CONSTRAINT [DF_MASERVICIOS_AtencionDomingo] DEFAULT ((1)) NOT NULL,
    [HoraDesde]             SMALLDATETIME   CONSTRAINT [DF_MASERVICIOS_HoraDesde] DEFAULT (getdate()) NOT NULL,
    [HoraHasta]             SMALLDATETIME   CONSTRAINT [DF_MASERVICIOS_HoraHasta] DEFAULT (getdate()) NOT NULL,
    [DesayunoHoraDesde]     SMALLDATETIME   NULL,
    [DesayunoHoraHasta]     SMALLDATETIME   NULL,
    [PreDesayunoHoraDesde]  SMALLDATETIME   NULL,
    [PreDesayunoHoraHasta]  SMALLDATETIME   NULL,
    [IDPreDesayuno]         CHAR (2)        NOT NULL,
    [ObservacionesDesayuno] VARCHAR (250)   NULL,
    [HoraCheckIn]           SMALLDATETIME   NULL,
    [HoraCheckOut]          SMALLDATETIME   NULL,
    [cActivo]               CHAR (1)        CONSTRAINT [DF_MASERVICIOS_cActivo] DEFAULT ('A') NOT NULL,
    [Dias]                  TINYINT         CONSTRAINT [DF_MASERVICIOS_Dias] DEFAULT ((1)) NOT NULL,
    [IDServicioCopia]       CHAR (8)        NULL,
    [PoliticaLiberado]      BIT             NOT NULL,
    [Liberado]              TINYINT         NULL,
    [TipoLib]               CHAR (1)        NULL,
    [MaximoLiberado]        TINYINT         NULL,
    [MontoL]                NUMERIC (10, 4) NULL,
    [Mig]                   BIT             CONSTRAINT [DF__MASERVICIOS__Mig__0ECE1972] DEFAULT ((0)) NOT NULL,
    [SinDescripcion]        BIT             CONSTRAINT [DF_MASERVICIOS_SinDescripcion] DEFAULT ((0)) NOT NULL,
    [UserMod]               CHAR (4)        NOT NULL,
    [FecMod]                DATETIME        CONSTRAINT [DF_MASERVICIOS_FecMod] DEFAULT (getdate()) NOT NULL,
    [ServicioVarios]        BIT             DEFAULT ((0)) NOT NULL,
    [IDCabVarios]           INT             NULL,
    [CoPago]                CHAR (3)        NULL,
    [IDDesayuno]            CHAR (2)        NULL,
    [FlServicioSoloTC]      BIT             DEFAULT ((0)) NULL,
    [APTServicoOpcional]    CHAR (1)        CONSTRAINT [DF_MASERVICIOS_APTServicoOpcional] DEFAULT ('N') NOT NULL,
    [HoraPredeterminada]    SMALLDATETIME   NULL,
    [FlNoPresupuestos]      BIT             DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_MASERVICIOS_1] PRIMARY KEY CLUSTERED ([IDServicio] ASC),
    CONSTRAINT [FK_MASERVICIOS_COTICAB] FOREIGN KEY ([IDCabVarios]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_MASERVICIOS_MACATEGORIA] FOREIGN KEY ([IDCat]) REFERENCES [dbo].[MACATEGORIA] ([IDCat]),
    CONSTRAINT [FK_MASERVICIOS_MAPROVEEDORES] FOREIGN KEY ([IDProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_MASERVICIOS_MATIPOPROVEEDOR] FOREIGN KEY ([IDTipoProv]) REFERENCES [dbo].[MATIPOPROVEEDOR] ([IDTipoProv]),
    CONSTRAINT [FK_MASERVICIOS_MATIPOSERVICIOS] FOREIGN KEY ([IDTipoServ]) REFERENCES [dbo].[MATIPOSERVICIOS] ([IDTipoServ]),
    CONSTRAINT [FK_MASERVICIOS_MAUBIGEO] FOREIGN KEY ([IDubigeo]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO
CREATE NONCLUSTERED INDEX [IX_MASERVICIOS_IDCabVarios]
    ON [dbo].[MASERVICIOS]([IDCabVarios] ASC);


GO

--121
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MASERVICIOS_LOG_Upd]
  On dbo.MASERVICIOS for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--124
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE Trigger [dbo].[MASERVICIOS_LOG_Del]
  On dbo.MASERVICIOS for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--118
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- TABLA MASERVICIOS

CREATE Trigger [dbo].[MASERVICIOS_LOG_Ins]
  On dbo.MASERVICIOS for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDServicio from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MASERVICIOS',@UserMod,@RegistroPK1,'','','',@Accion
		End  
