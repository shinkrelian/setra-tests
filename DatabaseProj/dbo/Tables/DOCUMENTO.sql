﻿CREATE TABLE [dbo].[DOCUMENTO] (
    [NuDocum]                 CHAR (10)      NOT NULL,
    [IDTipoDoc]               CHAR (3)       NOT NULL,
    [IDCab]                   INT            NULL,
    [FeDocum]                 SMALLDATETIME  DEFAULT (getdate()) NOT NULL,
    [IDMoneda]                CHAR (3)       NOT NULL,
    [SsTotalDocumUSD]         NUMERIC (9, 2) NOT NULL,
    [CoEstado]                CHAR (2)       CONSTRAINT [DF__DOCUMENTO__CoEst__43A1D464] DEFAULT ('GD') NOT NULL,
    [UserMod]                 CHAR (4)       NOT NULL,
    [FecMod]                  DATETIME       DEFAULT (getdate()) NOT NULL,
    [IDCliente]               CHAR (6)       NULL,
    [FlDocManual]             BIT            DEFAULT ((0)) NOT NULL,
    [TxTitulo]                VARCHAR (100)  NOT NULL,
    [SsIGV]                   NUMERIC (8, 2) NOT NULL,
    [NuDocumVinc]             CHAR (10)      NULL,
    [FlPorAjustePrecio]       BIT            DEFAULT ((0)) NOT NULL,
    [IDTipoDocVinc]           CHAR (3)       NULL,
    [CoTipoVenta]             CHAR (2)       NOT NULL,
    [CoCtaContable]           VARCHAR (14)   NULL,
    [FeEmisionVinc]           SMALLDATETIME  NULL,
    [CoCtroCosto]             CHAR (6)       NULL,
    [NuFileLibre]             CHAR (8)       NULL,
    [NuNtaVta]                CHAR (6)       NULL,
    [NuCuentaAnt]             CHAR (6)       NULL,
    [FlCtaCambiada]           BIT            DEFAULT ((0)) NOT NULL,
    [FlAjustePrecioUpdMontos] BIT            DEFAULT ((0)) NOT NULL,
    [NuItem_DM]               INT            NULL,
    [CtaContable_Anterior]    VARCHAR (14)   NULL,
    [SsSaldoMatchAsignac]     NUMERIC (9, 2) NULL,
    [CoDebitMemoAnticipo]     CHAR (10)      NULL,
    [CoEstadoPago]            CHAR (2)       DEFAULT ('PD') NOT NULL,
    [CoSAP]                   VARCHAR (15)   NULL,
    [FlVtaAdicional]          BIT            DEFAULT ((0)) NOT NULL,
    CONSTRAINT [PK_DOCUMENTO] PRIMARY KEY NONCLUSTERED ([NuDocum] ASC, [IDTipoDoc] ASC),
    CONSTRAINT [FK_DOCUMENTO_COTICAB] FOREIGN KEY ([IDCab]) REFERENCES [dbo].[COTICAB] ([IDCAB]),
    CONSTRAINT [FK_DOCUMENTO_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_DOCUMENTO_MATIPODOC] FOREIGN KEY ([IDTipoDoc]) REFERENCES [dbo].[MATIPODOC] ([IDtipodoc])
);


GO
CREATE NONCLUSTERED INDEX [IX_IDTipoDoc]
    ON [dbo].[DOCUMENTO]([IDTipoDoc] ASC)
    INCLUDE([NuDocum], [IDCab], [FeDocum], [IDMoneda], [SsTotalDocumUSD], [CoEstado], [IDCliente], [SsIGV], [NuDocumVinc], [IDTipoDocVinc], [CoTipoVenta], [CoCtaContable], [FeEmisionVinc], [CoCtroCosto]);


GO
Create Trigger [dbo].[DOCUMENTO_LOG_Ins]  
  On [dbo].[DOCUMENTO] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuDocum from Inserted)  
   set @RegistroPK2 = (select IDTipoDoc from Inserted)   
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DOCUMENTO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[DOCUMENTO_LOG_Upd]  
  On [dbo].[DOCUMENTO] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuDocum from Inserted)  
   set @RegistroPK2 = (select IDTipoDoc from Inserted)   
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DOCUMENTO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[DOCUMENTO_LOG_Del]  
  On [dbo].[DOCUMENTO] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150) 
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuDocum from Deleted)     
   set @RegistroPK2 = (select IDTipoDoc from Deleted) 
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DOCUMENTO',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
