﻿CREATE TABLE [dbo].[MASERVICIOS_RANGO_APT] (
    [Correlativo] TINYINT         NOT NULL,
    [IDServicio]  CHAR (8)        NOT NULL,
    [PaxDesde]    SMALLINT        NOT NULL,
    [PaxHasta]    SMALLINT        NOT NULL,
    [Monto]       NUMERIC (10, 4) NOT NULL,
    [UserMod]     CHAR (4)        NOT NULL,
    [FecMod]      DATETIME        CONSTRAINT [DF_MASERVICIOS_RANGO_APT_FecMod] DEFAULT (getdate()) NOT NULL,
    [IDTemporada] INT             NOT NULL,
    CONSTRAINT [PK_MASERVICIOS_RANGO_APT] PRIMARY KEY CLUSTERED ([Correlativo] ASC, [IDServicio] ASC, [IDTemporada] ASC),
    CONSTRAINT [FK_MASERVICIOS_RANGO_APT_MASERVICIOS_DESC_APT] FOREIGN KEY ([IDServicio], [IDTemporada]) REFERENCES [dbo].[MASERVICIOS_DESC_APT] ([IDServicio], [IDTemporada])
);

