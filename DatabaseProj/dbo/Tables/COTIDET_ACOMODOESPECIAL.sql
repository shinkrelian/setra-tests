﻿CREATE TABLE [dbo].[COTIDET_ACOMODOESPECIAL] (
    [IDDet]       INT      NOT NULL,
    [CoCapacidad] CHAR (2) NOT NULL,
    [QtPax]       TINYINT  NOT NULL,
    [UserMod]     CHAR (4) NOT NULL,
    [FecMod]      DATETIME DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTIDET_ACOMODOESPECIAL] PRIMARY KEY NONCLUSTERED ([IDDet] ASC, [CoCapacidad] ASC),
    CONSTRAINT [FK_COTIDET_ACOMODOESPECIAL_CAPACIDAD_HABITAC] FOREIGN KEY ([CoCapacidad]) REFERENCES [dbo].[CAPACIDAD_HABITAC] ([CoCapacidad]),
    CONSTRAINT [FK_COTIDET_ACOMODOESPECIAL_COTIDET] FOREIGN KEY ([IDDet]) REFERENCES [dbo].[COTIDET] ([IDDET])
);


GO
Create Trigger [dbo].[COTIDET_ACOMODOESPECIAL_LOG_Del]  
  On [dbo].[COTIDET_ACOMODOESPECIAL] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Deleted)     
   set @RegistroPK2 = (select CoCapacidad from Deleted)     
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_ACOMODOESPECIAL',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[COTIDET_ACOMODOESPECIAL_LOG_Upd]  
  On [dbo].[COTIDET_ACOMODOESPECIAL] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150) 
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Inserted)  
   set @RegistroPK2 = (select CoCapacidad from Inserted) 
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_ACOMODOESPECIAL',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 

GO
Create Trigger [dbo].[COTIDET_ACOMODOESPECIAL_LOG_Ins]  
  On [dbo].[COTIDET_ACOMODOESPECIAL] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)   
 Declare @RegistroPK2 varchar(150)   
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDet from Inserted)  
   set @RegistroPK2 = (select CoCapacidad from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'COTIDET_ACOMODOESPECIAL',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End 
