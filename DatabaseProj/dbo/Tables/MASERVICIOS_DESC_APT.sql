﻿CREATE TABLE [dbo].[MASERVICIOS_DESC_APT] (
    [IDServicio]         CHAR (8)        NOT NULL,
    [IDTemporada]        INT             NOT NULL,
    [DescripcionAlterna] VARCHAR (200)   NULL,
    [UserMod]            CHAR (4)        NOT NULL,
    [FecMod]             DATETIME        CONSTRAINT [DF_MASERVICIOS_DESC_APT_FecMod] DEFAULT (getdate()) NOT NULL,
    [AgruparServicio]    BIT             DEFAULT ((0)) NOT NULL,
    [PoliticaLiberado]   BIT             DEFAULT ((0)) NOT NULL,
    [Liberado]           TINYINT         NULL,
    [TipoLib]            CHAR (1)        NULL,
    [MaximoLiberado]     TINYINT         NULL,
    [MontoL]             NUMERIC (10, 4) NULL,
    CONSTRAINT [PK_MASERVICIOS_DESC_APT] PRIMARY KEY CLUSTERED ([IDServicio] ASC, [IDTemporada] ASC),
    CONSTRAINT [FK_MASERVICIOS_DESC_APT_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio]),
    CONSTRAINT [FK_MASERVICIOS_DESC_APT_MATEMPORADA] FOREIGN KEY ([IDTemporada]) REFERENCES [dbo].[MATEMPORADA] ([IDTemporada])
);

