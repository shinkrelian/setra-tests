﻿CREATE TABLE [dbo].[COTICAB_HISTORIAL_UPD] (
    [NuLog]          INT           IDENTITY (1, 1) NOT NULL,
    [IDCab]          INT           NOT NULL,
    [NuUpd]          SMALLINT      NOT NULL,
    [NoCampo]        VARCHAR (15)  NOT NULL,
    [TxValorAntes]   VARCHAR (100) NOT NULL,
    [TxValorDespues] VARCHAR (100) NOT NULL,
    [UserMod]        CHAR (4)      NOT NULL,
    [NoEquipo]       VARCHAR (20)  DEFAULT (host_name()) NOT NULL,
    [FecMod]         DATETIME      DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_COTICAB_HISTORIAL_UPD] PRIMARY KEY NONCLUSTERED ([NuLog] ASC),
    CONSTRAINT [FK_COTICAB_HISTORIAL_UPD_COTICAB_HISTORIAL_PVW] FOREIGN KEY ([IDCab], [NuUpd]) REFERENCES [dbo].[COTICAB_HISTORIAL_PVW] ([IDCab], [NuUpd])
);

