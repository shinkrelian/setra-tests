﻿CREATE TABLE [dbo].[MATIPOSERV] (
    [IDservicio]  CHAR (3)     NOT NULL,
    [Descripcion] VARCHAR (60) NOT NULL,
    [CtaCon]      VARCHAR (12) NULL,
    [Ingles]      VARCHAR (60) NULL,
    [SubDiario]   VARCHAR (4)  NULL,
    [CtaVta]      VARCHAR (12) NULL,
    [SdVta]       VARCHAR (4)  NULL,
    [IdFactu]     CHAR (5)     NULL,
    [UserMod]     CHAR (4)     NULL,
    [FecMod]      DATETIME     NULL,
    CONSTRAINT [PK_MATIPOSERV] PRIMARY KEY CLUSTERED ([IDservicio] ASC)
);


GO

--171
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOSERV_LOG_Upd]
  On [dbo].[MATIPOSERV] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDservicio from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOSERV',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO

--169
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MATIPOSERV

CREATE Trigger [dbo].[MATIPOSERV_LOG_Ins]
  On [dbo].[MATIPOSERV] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select IDservicio from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOSERV',@UserMod,@RegistroPK1,'','','',@Accion
		End  

GO
	

--173
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MATIPOSERV_LOG_Del]
  On [dbo].[MATIPOSERV] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select IDservicio from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MATIPOSERV',@UserMod,@RegistroPK1,'','','',@Accion
		End  
