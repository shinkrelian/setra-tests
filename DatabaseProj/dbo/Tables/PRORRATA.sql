﻿CREATE TABLE [dbo].[PRORRATA] (
    [NuCodigoPro] SMALLINT       NOT NULL,
    [FeDesde]     SMALLDATETIME  NOT NULL,
    [FeHasta]     SMALLDATETIME  NOT NULL,
    [QtProrrata]  NUMERIC (8, 2) NOT NULL,
    [UserMod]     CHAR (4)       NOT NULL,
    [FecMod]      SMALLDATETIME  NOT NULL,
    CONSTRAINT [PK_PRORRATA] PRIMARY KEY CLUSTERED ([NuCodigoPro] ASC)
);


GO
Create Trigger [dbo].[PRORRATA_LOG_Upd]  
  On [dbo].[PRORRATA] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuCodigoPro from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'PRORRATA',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[PRORRATA_LOG_Ins]  
  On [dbo].[PRORRATA] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuCodigoPro from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'PRORRATA',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[PRORRATA_LOG_Del]  
  On [dbo].[PRORRATA] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select NuCodigoPro from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'PRORRATA',@UserMod,@RegistroPK1,'','','',@Accion  
  End 
