﻿CREATE TABLE [dbo].[RESERVAS_DET] (
    [IDReserva_Det]             INT             NOT NULL,
    [IDReserva]                 INT             NOT NULL,
    [IDFile]                    CHAR (8)        NOT NULL,
    [IDDet]                     INT             NULL,
    [Item]                      NUMERIC (6, 3)  NOT NULL,
    [Dia]                       SMALLDATETIME   NOT NULL,
    [DiaNombre]                 VARCHAR (10)    NOT NULL,
    [FechaOut]                  SMALLDATETIME   NULL,
    [Noches]                    TINYINT         NULL,
    [IDubigeo]                  CHAR (6)        NOT NULL,
    [IDServicio]                CHAR (8)        NOT NULL,
    [IDServicio_Det]            INT             NOT NULL,
    [IDIdioma]                  VARCHAR (12)    NOT NULL,
    [Especial]                  BIT             NOT NULL,
    [MotivoEspecial]            VARCHAR (200)   NULL,
    [RutaDocSustento]           VARCHAR (200)   NULL,
    [Desayuno]                  BIT             NOT NULL,
    [Lonche]                    BIT             NOT NULL,
    [Almuerzo]                  BIT             NOT NULL,
    [Cena]                      BIT             NOT NULL,
    [Transfer]                  BIT             NOT NULL,
    [IDDetTransferOri]          INT             NULL,
    [IDDetTransferDes]          INT             NULL,
    [CamaMat]                   TINYINT         NULL,
    [TipoTransporte]            CHAR (1)        NULL,
    [IDUbigeoOri]               CHAR (6)        NOT NULL,
    [IDUbigeoDes]               CHAR (6)        NOT NULL,
    [NroPax]                    SMALLINT        NOT NULL,
    [NroLiberados]              SMALLINT        NULL,
    [Tipo_Lib]                  CHAR (1)        NULL,
    [Cantidad]                  TINYINT         NOT NULL,
    [CantidadAPagar]            NUMERIC (5, 2)  NOT NULL,
    [CostoReal]                 NUMERIC (12, 4) NOT NULL,
    [CostoRealAnt]              NUMERIC (12, 4) NOT NULL,
    [CostoLiberado]             NUMERIC (12, 4) NOT NULL,
    [Margen]                    NUMERIC (12, 4) NOT NULL,
    [MargenAplicado]            NUMERIC (6, 2)  NOT NULL,
    [MargenAplicadoAnt]         NUMERIC (12, 4) NULL,
    [MargenLiberado]            NUMERIC (12, 4) NOT NULL,
    [Total]                     NUMERIC (12, 4) NOT NULL,
    [TotalOrig]                 NUMERIC (12, 4) NOT NULL,
    [CostoRealImpto]            NUMERIC (12, 4) NOT NULL,
    [CostoLiberadoImpto]        NUMERIC (12, 4) NOT NULL,
    [MargenImpto]               NUMERIC (12, 4) NOT NULL,
    [MargenLiberadoImpto]       NUMERIC (12, 4) NOT NULL,
    [TotImpto]                  NUMERIC (12, 4) NOT NULL,
    [NetoHab]                   NUMERIC (12, 4) NOT NULL,
    [IgvHab]                    NUMERIC (12, 4) NOT NULL,
    [TotalHab]                  NUMERIC (12, 4) NOT NULL,
    [NetoGen]                   NUMERIC (12, 4) NOT NULL,
    [IgvGen]                    NUMERIC (12, 4) NOT NULL,
    [TotalGen]                  NUMERIC (12, 4) NOT NULL,
    [IDDetRel]                  INT             NULL,
    [Servicio]                  VARCHAR (MAX)   NULL,
    [IDReserva_DetCopia]        INT             NULL,
    [IDReserva_Det_Rel]         INT             NULL,
    [IDEmailRef]                VARCHAR (255)   NULL,
    [IDEmailEdit]               VARCHAR (255)   NULL,
    [IDEmailNew]                VARCHAR (255)   NULL,
    [CapacidadHab]              TINYINT         NULL,
    [EsMatrimonial]             BIT             CONSTRAINT [DF__RESERVAS___EsMat__42B7D1CC] DEFAULT ((0)) NOT NULL,
    [UserMod]                   CHAR (4)        NOT NULL,
    [FecMod]                    DATETIME        CONSTRAINT [DF__RESERVAS___FecMo__52C41C63] DEFAULT (getdate()) NOT NULL,
    [IDMoneda]                  CHAR (3)        NULL,
    [IDGuiaProveedor]           CHAR (6)        NULL,
    [NuVehiculo]                SMALLINT        NULL,
    [ExecTrigger]               BIT             DEFAULT ((1)) NOT NULL,
    [Anulado]                   BIT             DEFAULT ((0)) NOT NULL,
    [RegeneradoxAcomodo]        BIT             DEFAULT ((0)) NOT NULL,
    [IDReserva_DetAcomodoTMP]   INT             NULL,
    [FlServicioParaGuia]        BIT             DEFAULT ((0)) NOT NULL,
    [FeUpdateRow]               DATETIME        NULL,
    [FlServicioNoShow]          BIT             DEFAULT ((0)) NOT NULL,
    [FlServicioIngManual]       BIT             DEFAULT ((0)) NOT NULL,
    [FlServicioTC]              BIT             DEFAULT ((0)) NOT NULL,
    [IDReserva_DetOrigAcomVehi] INT             NULL,
    [CantidadAPagarEditado]     BIT             CONSTRAINT [DF_RESERVAS_DET_CantidadAPagarEditado] DEFAULT ((0)) NULL,
    [NetoHabEditado]            BIT             CONSTRAINT [DF_RESERVAS_DET_NetoHabEditado] DEFAULT ((0)) NULL,
    [IgvHabEditado]             BIT             CONSTRAINT [DF_RESERVAS_DET_IgvHabEditado] DEFAULT ((0)) NULL,
    [NuViaticoTC]               INT             NULL,
    [FlAcomodoResidente]        BIT             DEFAULT ((0)) NOT NULL,
    [IDReserva_DetAntiguo]      INT             DEFAULT (NULL) NULL,
    CONSTRAINT [PK_RESERVAS_DET] PRIMARY KEY NONCLUSTERED ([IDReserva_Det] ASC),
    CONSTRAINT [FK_RESERVAS_DET_MAIDIOMAS] FOREIGN KEY ([IDIdioma]) REFERENCES [dbo].[MAIDIOMAS] ([IDidioma]),
    CONSTRAINT [FK_RESERVAS_DET_MAMONEDAS] FOREIGN KEY ([IDMoneda]) REFERENCES [dbo].[MAMONEDAS] ([IDMoneda]),
    CONSTRAINT [FK_RESERVAS_DET_MAPROVEEDORES] FOREIGN KEY ([IDGuiaProveedor]) REFERENCES [dbo].[MAPROVEEDORES] ([IDProveedor]),
    CONSTRAINT [FK_RESERVAS_DET_MASERVICIOS] FOREIGN KEY ([IDServicio]) REFERENCES [dbo].[MASERVICIOS] ([IDServicio]),
    CONSTRAINT [FK_RESERVAS_DET_MASERVICIOS_DET] FOREIGN KEY ([IDServicio_Det]) REFERENCES [dbo].[MASERVICIOS_DET] ([IDServicio_Det]),
    CONSTRAINT [FK_RESERVAS_DET_RESERVAS] FOREIGN KEY ([IDReserva]) REFERENCES [dbo].[RESERVAS] ([IDReserva])
);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS_DET_IDReserva]
    ON [dbo].[RESERVAS_DET]([IDReserva] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_IDDET]
    ON [dbo].[RESERVAS_DET]([IDDet] ASC)
    INCLUDE([IDReserva_Det], [CapacidadHab], [EsMatrimonial], [Anulado]);


GO
CREATE NONCLUSTERED INDEX [IX_Anulado]
    ON [dbo].[RESERVAS_DET]([Anulado] ASC)
    INCLUDE([IDReserva], [IDServicio_Det]);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS_DET]
    ON [dbo].[RESERVAS_DET]([Anulado] ASC)
    INCLUDE([IDReserva], [Dia], [IDServicio_Det], [TotalGen], [IDMoneda]);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS_DET_Anulado]
    ON [dbo].[RESERVAS_DET]([Anulado] ASC)
    INCLUDE([IDReserva], [Dia], [IDubigeo], [IDServicio], [IDServicio_Det], [NroPax], [NroLiberados], [TotalGen]);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS_DET_IDServicio_Det_NuViaticosTC]
    ON [dbo].[RESERVAS_DET]([IDServicio_Det] ASC, [Anulado] ASC, [NuViaticoTC] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESERVAS_DET_Anulado_NuViaticosTC]
    ON [dbo].[RESERVAS_DET]([Anulado] ASC)
    INCLUDE([IDServicio_Det], [NuViaticoTC]);


GO
Create Trigger [dbo].[RESERVAS_DET_LOG_Ins]  
  On [dbo].[RESERVAS_DET] for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDReserva_Det from Inserted)        
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_DET_LOG_Upd]  
  On [dbo].[RESERVAS_DET] for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDReserva_Det from Inserted)      
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
Create Trigger [dbo].[RESERVAS_DET_LOG_Del]  
  On [dbo].[RESERVAS_DET] for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDReserva_Det from Deleted)   
   set @UserMod = (select UserMod from Deleted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'RESERVAS_DET',@UserMod,@RegistroPK1,'','','',@Accion  
  End 

GO
CREATE Trigger [dbo].[OPERACIONES_UpdCambiosenReservasPostFileActivo_UpdInsDel]
	On [dbo].[RESERVAS_DET] for Update, Delete, Insert
As
	Set NoCount On	
	
	--If (Select Count(*) From Inserted)=1
	--	Begin
		
		--Declare @IDReserva_DetUpd int=(Select IDReserva_Det From Inserted)
		Declare @IDReserva int=(Select Top 1 IDReserva From Inserted)
		--Declare @IDDetUpd int=(Select IDDet From Inserted)
		--Declare @Estado char(1)=(Select Estado From CotiCab Where IDCab=(Select IDCab From Inserted))
		Declare @IDCab int
		Select @IDCab=IDCab From RESERVAS Where IDReserva=@IDReserva
		
		--Declare @Anulado bit=(Select Anulado from Inserted) 	
		--Declare @ExecTrigger bit=(Select ExecTrigger from Inserted)
		
		If 	Exists(Select IDOperacion From OPERACIONES  
			Where IDReserva = @IDReserva And idcab=@IDCab)
			--(Select IDReserva From RESERVAS_DET Where IDReserva_Det=@IDReserva_DetUpd)) 
			--and @ExecTrigger =1 --And @Anulado=0
			
			Begin
			Declare @UserMod char(4)=(Select Top 1 UserMod From Inserted)
			
			Declare @IDOperacion int=(Select r.IDOperacion 
				From OPERACIONES r 
				Where	r.idcab=@IDCab				
				And r.IDReserva=@IDReserva)			
		
			Exec OPERACIONES_UpdCambiosenReservasPostFile @IDOperacion,1,@UserMod
								
			End 

--	End  
