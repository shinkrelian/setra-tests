﻿CREATE TABLE [dbo].[MAFERIADOS] (
    [FechaFeriado] DATE          NOT NULL,
    [IDPais]       CHAR (6)      NOT NULL,
    [Descripcion]  VARCHAR (100) NOT NULL,
    [UserMod]      CHAR (4)      NOT NULL,
    [FecMod]       SMALLDATETIME CONSTRAINT [DF_MAFERIADOS_FecMod] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_MAFERIADOS] PRIMARY KEY NONCLUSTERED ([FechaFeriado] ASC, [IDPais] ASC),
    CONSTRAINT [FK_MAFERIADOS_MAUBIGEO] FOREIGN KEY ([IDPais]) REFERENCES [dbo].[MAUBIGEO] ([IDubigeo])
);


GO

--46
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAFERIADOS_LOG_Upd]
  On [dbo].[MAFERIADOS] for Update
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select FechaFeriado from Inserted)
			set @RegistroPK2 = (select IDPais from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'U'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFERIADOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--48
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE Trigger [dbo].[MAFERIADOS_LOG_Del]
  On [dbo].[MAFERIADOS] for Delete
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Deleted) = 1 
		Begin 
			set @RegistroPK1 = (select FechaFeriado from Deleted)
			set @RegistroPK2 = (select IDPais from Deleted)
			set @UserMod = (select UserMod from Deleted)
			set @Accion = 'D'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFERIADOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  

GO

--44
--Text
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

-- TABLA MAFERIADOS

CREATE Trigger [dbo].[MAFERIADOS_LOG_Ins]
  On [dbo].[MAFERIADOS] for Insert
As
	Set NoCount On
	
	Declare @RegistroPK1 varchar(150)
	Declare @RegistroPK2 varchar(150)
	Declare @UserMod char(4) 
	Declare @Accion char(1)
	
	if (Select count(*) from Inserted) = 1 
		Begin 
			set @RegistroPK1 = (select FechaFeriado from Inserted)
			set @RegistroPK2 = (select IDPais from Inserted)
			set @UserMod = (select UserMod from Inserted)
			set @Accion = 'I'
			Execute [dbo].[LOGAUDITORIA_Ins] 'MAFERIADOS',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion
		End  
