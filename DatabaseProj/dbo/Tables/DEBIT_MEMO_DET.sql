﻿CREATE TABLE [dbo].[DEBIT_MEMO_DET] (
    [IDDebitMemo]    CHAR (10)      NOT NULL,
    [IDDebitMemoDet] TINYINT        NOT NULL,
    [Descripcion]    VARCHAR (MAX)  NOT NULL,
    [CapacidadHab]   CHAR (3)       NULL,
    [NroPax]         SMALLINT       NOT NULL,
    [CostoPersona]   NUMERIC (8, 2) NOT NULL,
    [SubTotal]       NUMERIC (8, 2) NOT NULL,
    [TotalIGV]       NUMERIC (8, 2) NOT NULL,
    [Total]          NUMERIC (8, 2) NOT NULL,
    [UserMod]        CHAR (4)       NOT NULL,
    [FecMod]         DATETIME       CONSTRAINT [DF__DEBIT_MEM__FecMo__2F25DA6B] DEFAULT (getdate()) NOT NULL,
    CONSTRAINT [PK_DEBIT_MEMO_DET] PRIMARY KEY NONCLUSTERED ([IDDebitMemo] ASC, [IDDebitMemoDet] ASC),
    CONSTRAINT [FK_DEBIT_MEMO_DET_DEBIT_MEMO] FOREIGN KEY ([IDDebitMemo]) REFERENCES [dbo].[DEBIT_MEMO] ([IDDebitMemo])
);


GO
Create Trigger [dbo].[DEBIT_MEMO_DET_LOG_Ins]  
  On dbo.DEBIT_MEMO_DET for Insert  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDebitMemo from Inserted)  
   set @RegistroPK2 = (select IDDebitMemoDet from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'I'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DEBIT_MEMO_DET',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End

GO
Create Trigger [dbo].[DEBIT_MEMO_DET_LOG_Upd]  
  On dbo.DEBIT_MEMO_DET for Update  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Inserted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDebitMemo from Inserted)  
   set @RegistroPK2 = (select IDDebitMemoDet from Inserted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'U'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DEBIT_MEMO_DET',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End

GO
Create Trigger [dbo].[DEBIT_MEMO_DET_LOG_Del]  
  On dbo.DEBIT_MEMO_DET for Delete  
As  
 Set NoCount On  
   
 Declare @RegistroPK1 varchar(150)  
 Declare @RegistroPK2 varchar(150)  
 Declare @UserMod char(4)   
 Declare @Accion char(1)  
   
 if (Select count(*) from Deleted) = 1   
  Begin   
   set @RegistroPK1 = (select IDDebitMemo from Deleted)  
   set @RegistroPK2 = (select IDDebitMemoDet from Deleted)  
   set @UserMod = (select UserMod from Inserted)  
   set @Accion = 'D'  
   Execute [dbo].[LOGAUDITORIA_Ins] 'DEBIT_MEMO_DET',@UserMod,@RegistroPK1,@RegistroPK2,'','',@Accion  
  End
