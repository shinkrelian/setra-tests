﻿Create Procedure dbo.VIATICOS_TOURCONDUCTOR_SelIDMonedaxNuViaticoTC
@NuViaticoTC int,
@pIDMoneda char(3) output
As
Set NoCount On
Select @pIDMoneda = Case When CostoUSD > 0 then 'USD' else 'SOL' End
from VIATICOS_TOURCONDUCTOR
Where NuViaticoTC = @NuViaticoTC
