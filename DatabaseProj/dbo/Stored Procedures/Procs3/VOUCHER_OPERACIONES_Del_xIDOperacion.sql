﻿Create Procedure [Dbo].[VOUCHER_OPERACIONES_Del_xIDOperacion]
@IDOperacion int
As
	Set NoCount On
	
	
    Delete from VOUCHER_OPERACIONES 
    where IDVoucher in (select IDVoucher from OPERACIONES_DET where IDOperacion = @IDOperacion)
    And IDVoucher <> 0
