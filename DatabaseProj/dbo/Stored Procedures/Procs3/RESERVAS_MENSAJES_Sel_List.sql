﻿CREATE procedure [dbo].[RESERVAS_MENSAJES_Sel_List]
	@IDReserva int
As

	Set NoCount On
	Select IDReserva, ConversationID, Asunto from RESERVAS_MENSAJES where IDReserva=@IDReserva
