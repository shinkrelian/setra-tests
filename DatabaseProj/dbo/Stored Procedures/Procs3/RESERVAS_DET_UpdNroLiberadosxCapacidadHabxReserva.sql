﻿


CREATE Procedure dbo.RESERVAS_DET_UpdNroLiberadosxCapacidadHabxReserva
	@IDReserva	int,
	@CapacidadHab tinyint,
	@NroLiberados	smallint,
	@UserMod char(4)
As
	Set NoCount On
	
	UPDATE RESERVAS_DET 
	Set NroPax=rd.NroPax-@NroLiberados,
	NroLiberados=@NroLiberados,	
	UserMod=@UserMod, FecMod=GETDATE()
	From RESERVAS_DET rd Left Join COTIDET cd On rd.IDDet=cd.IDDET
	WHERE IDReserva IN (@IDReserva)
	And CapacidadHab=@CapacidadHab
	And rd.NroPax>=@NroLiberados
	And cd.IncGuia=0



