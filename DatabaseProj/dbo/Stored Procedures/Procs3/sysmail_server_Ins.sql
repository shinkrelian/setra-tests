﻿Create Procedure dbo.sysmail_server_Ins
	@account_id	int,
	@servername	nvarchar(128)
As
	Set NoCount On  
	
	Insert Into msdb.dbo.sysmail_server
	(account_id, servertype, servername, port, last_mod_datetime )
	Values(@account_id, 'SMTP', @servername, 25, GETDATE() )

