﻿
--MLL-20140609              
--HLF-20140620-Agregando ltrim(rtrim(@IDFile))='' a (cc.IDCAB In (select px.IDCab ...            
--(cc.IDUsuarioRes=@IDUsuario Or cc.IDUsuario=@IDUsuario Or cc.IDUsuarioOpe=@IDUsuario))            
--JRF-20140624-Agregar la columna [CoEstado] y ajuste en el Orden de la columnas        
--JRF-20140625-Quitar el filtro de Titulo y agregar el de Rango FechaEmision      
--HLF-20140627-(ltrim(rtrim(@IDUsuario))='' Or t.CoEjeRec=@IDUsuario)            
--HLF-20140630-(T.NoPax  Like '%'+@NombrePax+'%' Or ltrim(rtrim(@NombrePax))='')     
--HLF-20140704-TxTotal, SsComision,  SsCosto,     
--JHD-20140924-Se corrigio el filtro de cliente  
--JRF-20141007-DescEstadoTicket  
--HLF-20150107-Desc al Order by
--JHD-20150110-Se agrego parametro @NuNtaVta varchar(6)=''
--JHD-20150110-Se agrego columna NuNtaVta
--HLF-20151207-And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1) 
CREATE PROCEDURE [dbo].[TICKET_Sel_List]                 
 @IDFile CHAR(8),             
 @CoTicket CHAR(13),                                    
 @DescCliente VARCHAR(60),                                    
 @TxFechaParIni SMALLDATETIME,                                  
 @TxFechaParFin SMALLDATETIME,                  
 @TxFechaEmisionIni SMALLDATETIME,                  
 @TxFechaEmisionFin SMALLDATETIME,      
 @IDUsuario char(4),              
 @Estado char(1) ,                    
 @NombrePax varchar(100),              
 @Cotizacion char(8) ,              
 @CoProvOrigen CHAR(4),      
 @CoLineaAerea char(3),      
 @DCUbigeoOrigen char(3),      
 @DCUbigeoDestino char(3),      
 @CoEjeCou char(4),      
 @NuDocumBSP char(11),
 @NuNtaVta varchar(6)='',
 @FlHistorico bit=1  
AS                  
                 
 SET NOCOUNT ON                 
              
               
 SELECT TxFechaEmision, NuSerieTicket,CoEstado , NuTicket,               
 CoTicket, IDCab,              
 CoLineaAereaSigla, Ruta, NoPax,  IDFile,        
 --TxTarifaAerea,    
 TxTotal,     
     
 --TotalCom,     
 SsComision,     
 SsCosto,     
 Seg,        
 --TxTotal-TotalCom As TotalaPagar,             
     
 TxRestricciones,DescEstadoTicket,Observaciones    ,
 NuNtaVta  
 FROM              
 (              
  Select t.TxFechaEmision, substring(t.CoTicket,1,3) as NuSerieTicket, t.CoEstado,              
  right(t.CoTicket,10) as NuTicket,               
  t.CoTicket, isnull(t.IDCab,0) as IDCab,              
  t.CoLineaAereaSigla, dbo.FnRutaItinetarioTicket(t.CoTicket) as Ruta,              
  (Select Top 1 TxFechaPar From TICKET_AMADEUS_ITINERARIO Where CoTicket=t.CoTicket Order by NuSegmento) as FechaViaje,      
  (Select Top 1 CoCiudadPar From TICKET_AMADEUS_ITINERARIO Where CoTicket=t.CoTicket Order by NuSegmento) as UbigOrigen,      
  (Select Top 1 CoCiudadLle From TICKET_AMADEUS_ITINERARIO Where CoTicket=t.CoTicket Order by NuSegmento) as UbigDestin,      
  (Select COUNT(*) From TICKET_AMADEUS_ITINERARIO Where CoTicket=t.CoTicket) as Seg,              
  t.TxTarifaAerea,----              
  t.TxTotal,               
  --(Select SUM(SsMonto) From TICKET_AMADEUS_TAX Where CoTicket=t.CoTicket) as TotalCom,              
  t.NoPax,              
  isnull(cc.IDFile,'') as IDFile,    
    t.NuNtaVta          ,
  t.TxRestricciones,  
  UPPER(Case ltrim(rtrim(t.CoEstado))  
 When 'TKTT' Then 'Emitido'  
 When 'CANN' Then 'Anulado por Amadeus'  
 When 'EXCH' Then 'Exchange'  
 When 'CANX' Then 'Anulado por Usuario'  
 When 'CNJ' Then 'Conjunción'  
  End) as DescEstadoTicket,  
  Isnull(t.TxObsCou,'') as Observaciones,    
  t.SsComision, t.SsCosto    

  From TICKET_AMADEUS t                 
  Left Join COTICAB cc On isnull(t.IDCab,0)=cc.IDCAB                 
  Left Join MACLIENTES c On isnull(t.IDCliente,cc.IDCliente)=c.IDCliente                  
  Where (t.CoTicket Like '%'+ltrim(rtrim(@CoTicket))+'%' Or ltrim(rtrim(@CoTicket))='') And                  
  ((t.TxFechaEmision Between @TxFechaEmisionIni And @TxFechaEmisionFin) Or @TxFechaEmisionIni='01/01/1900') And                  
  (isnull(c.RazonComercial,'') Like '%'+@DescCliente+'%' Or  ltrim(rtrim(@DescCliente))='') And                   
  (cc.IDFile Like '%'+ltrim(rtrim(@IDFile))+'%' Or ltrim(rtrim(@IDFile))='')   and                
  (ltrim(rtrim(@IDUsuario))='' Or t.CoEjeRec=@IDUsuario)            
         
   And              
  (LTRIM(rtrim(@CoLineaAerea))='' Or t.CoLineaAereaSigla = @CoLineaAerea) and      
  (cc.Estado=@Estado Or ltrim(rtrim(@Estado))='') and  --09062014 MLLAPAPASCA              
  (cc.Cotizacion Like '%'+ltrim(rtrim(@Cotizacion))+'%' Or ltrim(rtrim(@Cotizacion))='') and --09062014 MLLAPAPASCA              
  (ltrim(rtrim(@CoEjeCou))='' Or t.CoEjeCou = @CoEjeCou) and      
  --(ltrim(rtrim(@NuDocumBSP))='' Or Isnull(t.NuDocumBSP,'') like @NuDocumBSP +'%') and      
    
  --(cc.IDCAB In (select px.IDCab from COTIPAX px Where px.IDCab=cc.IDCAB                                     
  -- And (px.Nombres+' '+px.Apellidos) Like '%'+@NombrePax+'%' Or ltrim(rtrim(@NombrePax))='')             
  -- Or ltrim(rtrim(@IDFile))='')            
  (T.NoPax  Like '%'+@NombrePax+'%' Or ltrim(rtrim(@NombrePax))='')     
   AND   --09062014 MLLAPAPASCA              
  (T.CoProvOrigen=@CoProvOrigen Or ltrim(rtrim(@CoProvOrigen))='')   --09062014 MLLAPAPASCA          
   And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1)    
 ) as X                
 WHERE              
 (ltrim(rtrim(@DCUbigeoOrigen))='' Or x.UbigOrigen =@DCUbigeoOrigen) and      
 (ltrim(rtrim(@DCUbigeoDestino))='' Or x.UbigDestin =@DCUbigeoDestino) and      
 
 ((cast(convert(char(10),x.FechaViaje,103) as smalldatetime) between @TxFechaParIni and @TxFechaParFin) Or                 
 convert(char(10),@TxFechaParIni,103)='01/01/1900')                
 
 AND (ltrim(rtrim(@NuNtaVta))='' Or NuNtaVta like '%' +@NuNtaVta + '%') 

 Order by X.TxFechaEmision Desc



