﻿------------------------------------------------------------------------------------------

CREATE PROCEDURE RH_HORARIO_ACTIVO_UPD

		@CoHorario tinyint,
		   @FlActivo BIT,
           @UserMod char(4)
AS
BEGIN
UPDATE RH_HORARIO
   SET 
      FlActivo =@FlActivo
    
      ,UserMod = @UserMod
      ,FecMod =GETDATE()
      
      
 WHERE CoHorario = @CoHorario
 END;

