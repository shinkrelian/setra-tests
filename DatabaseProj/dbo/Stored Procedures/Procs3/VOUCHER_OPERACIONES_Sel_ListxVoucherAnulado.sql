﻿Create Procedure dbo.VOUCHER_OPERACIONES_Sel_ListxVoucherAnulado
@IDVoucher int
As
Set NoCount On
select Distinct od.IDVoucher
from VOUCHER_OPERACIONES vo Left Join OPERACIONES_DET od On vo.IDVoucher= od.IDVoucher
Left Join VOUCHER_HISTORICO vh On od.IDOperacion_Det=vh.IDOperacion_Det
Left Join VOUCHER_OPERACIONES vo2 On vh.IDVoucher= vo2.IDVoucher
where (od.IDVoucher<>0 and vh.IDVoucher=@IDVoucher)
