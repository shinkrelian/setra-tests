﻿CREATE Procedure [dbo].[RESERVAS_DETSERVICIOS_InsxIDCab]  
 @IDCab int,   
 @UserMod char(4)  
As  
 Set Nocount On  
   
  
 Insert Into RESERVAS_DETSERVICIOS  
 (IDReserva_Det,IDServicio_Det,  
 CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,  
 SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,  
 MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,  
 STMargenImpto,TotImpto,UserMod)    
 Select Distinct od.IDReserva_Det,rs.IDServicio_Det,  
 rs.CostoReal,rs.CostoLiberado,rs.Margen,rs.MargenAplicado,rs.MargenLiberado,rs.Total,  
 rs.SSCR,rs.SSCL,rs.SSMargen,rs.SSMargenLiberado,rs.SSTotal,rs.STCR,  
 rs.STMargen,rs.STTotal,rs.CostoRealImpto,rs.CostoLiberadoImpto,rs.MargenImpto,rs.MargenLiberadoImpto,  
 rs.SSCRImpto,rs.SSCLImpto,rs.SSMargenImpto,rs.SSMargenLiberadoImpto,rs.STCRImpto,rs.STMargenImpto,  
 rs.TotImpto,@UserMod  
 From COTIDET_DETSERVICIOS rs Inner Join COTIDET rd On rs.IDDET=rd.IDDET  
 Inner Join COTICAB r On r.IDCAB=rd.IDCAB And r.IDCab=@IDCab  
 Inner Join RESERVAS_DET od On rd.IDDET=od.IDDet  
 Inner Join MASERVICIOS_DET sd On rs.IDServicio_Det=sd.IDServicio_Det  
 Where rs.Total<>0  
