﻿--JRF-20151103-..(Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sdd.IDServicio..)
--HLF-20151103-pd.IDServicio_Det_V_Cot AS IDServicio_Det_V_Cot
--IDServicio=(select IDServicio from MASERVICIOS_DET where IDServicio_Det = pd.IDServicio_Det_V_Cot)
--HLF-20151125-Union --Tickets (sin guia)
--HLF-20151222-Agregando columna IDServicio, Case When isnull(sds.IDServicio,isnull(ods.IDServicio,''))='LIM00392'
--HLF-20151226-TotalPreCompra
--HLF-20160114-Left Join MASERVICIOS_DET sd On sd.IDServicio_Det=pd.IDServicio_Det_V_Cot
--HLF-20160201- and IDProveedor=@IDProveedor en (NuDetPSo is null 	and	not isnull((select top 1 coestado from ...
--HLF-20160308-case when sdd.Afecto=1 then 1+(@NuIgv/100) else 1 end
--HLF-20160308-Cambios en Modificaciones 1 y 2
--HLF-20160309-And Not exists(Select IDOperacion_Det From OPERACIONES_DET_DETSERVICIOS_LOGPRESUPUESTOS ...			
--HLF-20160316-And isnull(CoPrvPrg,'')=ltrim(rtrim(@IDProveedor_PrgGuia)) reemplaza a And CoPrvPrg=IDProveedor_PrgGuia
--And ((Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET ...
--And Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET  en Tickets (sin guia)
--comentando psd.NuDetPSo is null and ...
--HLF-20160323-Comentando --And lower(s1.Descripcion) Like 'tips%'
--HLF-20160326-
--HLF-20160331-
--HLF-20160404-And isnull(s1.CoPago,'EFE') in('EFE'))	And isnull(spr.CoPago,'EFE') in('EFE','TCK') 
--HLF-20160405-And IDServicio_Det_V_Cot=ods.IDServicio_Det_V_Cot)
--HLF-20160407-And FlNoPresupuestos=0
--Like 'Airport Tax%'
CREATE Procedure [dbo].[SINCERADO_DET_SelEntradasPresupuestoSobre]                        
 @IDCab int,                        
 @IDProveedor char(6),                        
 @IDProveedor_PrgGuia char(6),
 @NuPreSob int,
 @SoloModific bit  
As                      
 Set Nocount On                         
----Declare @DescGuia varchar(100)=isnull((Select NombreCorto From MAPROVEEDORES Where IDProveedor=@IDProveedor_PrgGuia),'')                        
 Declare @NuIgv numeric(8,2)=(select NuIGV from PARAMETRO)  


Select Y.IDServicio_Det,Y.IDServicio_Det_V_Cot,Y.IDOperacion_Det,
		Case When Y.NuDetPSo=0 Then 'M'+Cast(ROW_NUMBER() Over(Order by Y.CoPago,Y.Dia) as varchar)Else Cast(Y.NuDetPSo as varchar) End 
		as NuDetPSo,Y.NuSincerado_Det,Y.Dia,
	   Y.Hora,Y.DescProveeGuia,Y.IDProveedorGuia,Y.IDPax,Y.DescServicio,Y.Pax,Y.IDMoneda,Y.SimMoneda,Y.PreUnit,Y.Total,Y.Total as TotalPreCompra,
	   Case When Y.NuDetPSo=0 Then Case When Y.CoPago='TCK' Then Y.Total Else Y.Sustentado End Else Y.Sustentado End as Sustentado,
	   Y.Devuelto,Y.RegistradoPresup,Y.RegistradoSincer,Y.CoTipPSo,Y.DescCoTipPSo,Y.CoPago,Y.DescPago,Y.RegVal, Y.NuPreSob, 
	   RTRIM(LTRIM(Y.CoEstado)) AS CoEstado, IDServicio--, NroUnion
--INTO #PREVIEWPRESUPUESTOSLIMA
FROM 
(
 Select IDServicio_Det,IDServicio_Det_V_Cot,IDOperacion_Det,Isnull(CAST(NuDetPSo as varchar(5)),'') as NuDetPSo,                 
  NuSincerado_Det,Dia,        
  convert(varchar(5),Dia,108) As Hora,                
 isnull(DescProveeGuia,'') as DescProveeGuia,isnull(IDProveedorGuia,'') as IDProveedorGuia,        
 0 as IDPax,
 DescServicio,                    
 isnull(PaxPres,Pax) as Pax,IDMoneda,SimMoneda,
 IsNuLL(PreUniPres,
 case when isnull(PaxPres,Pax)>0 then
	CAST(ISNULL(TotalPres,ISNULL(Total,TotalServ))/isnull(PaxPres,Pax) as Numeric(8,2))
 else
	0 
 end
	) as PreUnit,  
 Case When CoPago = 'STK' Then 0 Else ISNULL(TotalPres ,ISNULL(Total,TotalServ)) End AS Total,                      
 Sustentado, Devuelto, RegistradoPresup, RegistradoSincer,                
 CoTipPSo, (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=x.CoTipPSo) as DescCoTipPSo,                                 
 CoPago, DescPago ,'R' RegVal  --,DescServicio2            
 ,NuPreSob, isnull(CoEstado,'') as CoEstado, IDServicio, 1 as NroUnion
 FROM                        
 (------                        
 Select 
 --isnull(sd.IDServicio_Det,isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)) As IDServicio_Det,                
 od.IDServicio_Det As IDServicio_Det,               
 isnull(ods.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,
 
 isnull(sds.IDServicio,isnull(ods.IDServicio,''))as IDServicio,      
 od.IDOperacion_Det,                
 sd.NuSincerado_Det,                 
 od.Dia,                         
 Case When ltrim(rtrim(pr.IDTipoProv)) in('005,008') Then           
  replace(pr.ApPaterno+' '+pr.ApMaterno+', '+pr.Nombre,'-','')             
 Else          
  isnull(pr.NombreCorto,'')             
 End as DescProveeGuia,          
          
 isnull(pr.IDProveedor,'') as IDProveedorGuia,          
 --'----'+
 isnull(psd.TxServicio,                     
  Case When sd.NuSincerado_Det Is Null Then                        
   sdd.Descripcion +                         
   Case When IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'')='' Then ''                         
   Else ' - '+IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'') End                         
  Else             
   sds.Descripcion +                         
   Case When IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'')='' Then ''                         
   Else ' - '+IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'') End                         
     
  End                
   )                        
                           
  as DescServicio,       
  sds.Descripcion as DescServicio2,      
 isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0)) as Pax,                         
 case when isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0))>0 then
	isnull(sd.SsCostoTotal,isnull(ods.TotalProgram,isnull(ods.TotalCotizado,od.TotalGen)))/isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0)) 
 else
	0
 end as PreUnit,
 sd.SsCostoTotal as Total, 
 
 Case When sd.SsCostoTotal is null Then            

isnull(isnull(TotalProgram,
	Case When isnull(TotalCotizado,0) = 0 then                                  
		Case When dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,NroPax+NroLiberados) = 0 Or FlServicioTC=1 Then
		od.CostoReal
		Else
		Case When sdd.IDServicio_Det IS NULL Then      
		 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,NroPax+NroLiberados),                          
		 sds.CoMoneda,ods.IDMoneda,	ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where                                   
			IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=ods.IDOperacion_Det))                    
			AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=ods.IDOperacion_Det))  
			,(Select SsTipCam From MATIPOSCAMBIO_USD Where CoMoneda=sds.CoMoneda))) 
		Else                
		 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(sdd.IDServicio_Det,NroPax+NroLiberados),                                    
		 sds.CoMoneda,ods.IDMoneda,ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where                                   
			IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=ods.IDOperacion_Det))                    
			AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=ods.IDOperacion_Det))  
			,(Select SsTipCam From MATIPOSCAMBIO_USD Where CoMoneda=sds.CoMoneda))) 
		End 
		End
                   
	  Else                       
	 TotalCotizado                       
	End * --case when sds.Afecto=1 then 1+(@NuIgv/100) else 1 end
		case when sdd.Afecto=1 then 1+(@NuIgv/100) else 1 end
	)
	 ,0) 


 Else                        
  sd.SsCostoTotal        
 End  
 As TotalServ,        

 isnull(psd.SsTotSus,0) as Sustentado,                        
 isnull(psd.SsTotDev,0) as Devuelto,                        
Case When psd.NuDetPSo IS NULL Then 0 Else 1 End as RegistradoPresup,             
 Case When isnull(ods.CoEstadoSincerado,'') = 'SA' Then 1 Else 0 End as RegistradoSincer,        
 psd.NuDetPSo,                
 psd.QtPax as PaxPres,
 
 Case When isnull(psd.CoPago,isnull(s.CoPago,'EFE')) = 'STK' Then IsNull(mo.IDMoneda,'SOL')  Else IsNull(mo.IDMoneda,'SOL') End As IDMoneda, --mo.IDMoneda End as IDMoneda,
 Case When isnull(psd.CoPago,isnull(s.CoPago,'EFE')) = 'STK' Then IsNull(mo.Simbolo,'S/.')  Else IsNull(mo.Simbolo,'S/.') End as SimMoneda, --mo.Simbolo End as SimMoneda,

 psd.SsPreUni as PreUniPres, psd.SsTotal as TotalPres                    
 ,isnull(psd.CoPago,isnull(s.CoPago,'EFE')) as CoPago,                
 Case isnull(psd.CoPago,isnull(s.CoPago,'EFE'))            
 When 'EFE' Then 'EFECTIVO'                
 When 'TCK' Then 'TICKET'                
 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sdd.IDServicio),0) As varchar(4))+')'
 When 'PCM' Then 'PRECOMPRA'
 
 End As DescPago,                
ISnull(psd.CoTipPSo,                
 case when p.IDTipoProv='006' Then 'EN'                 
 else                 
  case when s.copago in('EFE','TCK') and s.Descripcion like 'Tips%' then 'PR' else 'VA' end                
 end) as CoTipPSo      ,p.IDTipoProv,ps.NuPreSob, isnull(ps.CoEstado,'') as CoEstado
 From                         
 OPERACIONES_DET od                       
 Inner Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det 
	And od.IDServicio=ods.IDServicio And ods.Activo=1  
 --and (@NuPreSobre=0 Or ods.IDServicio_Det_V_Cot In(select psd1.IDServicio_Det from PRESUPUESTO_SOBRE_DET psd1 where NuPreSob=@NuPreSobre))      
 Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab      
  And o.IDProveedor=@IDProveedor   
  --**
  Inner Join MASERVICIOS_DET sdd On (isnull(ods.IDServicio_Det_V_Cot,0)=sdd.IDServicio_Det or 
	isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)=sdd.IDServicio_Det  and od.Servicio like 'Airport Tax%'  )
 Inner Join MASERVICIOS s On sdd.IDServicio=s.IDServicio And s.FlNoPresupuestos=0
 Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor                 
 --And p.IDTipoProv='006'                        
   And p.IDTipoProv Not In ('004','005','008')      
            
 Left Join MASERVICIOS spr On ods.IDServicio=spr.IDServicio                      
 Left Join SINCERADO sn On --sd.NuSincerado=sn.NuSincerado And                        
  sn.IDOperacion_Det=od.IDOperacion_Det                          
 Left Join SINCERADO_DET sd On --sd.IDServicio_Det=ods.IDServicio_Det_V_Cot                          
  sn.NuSincerado=sd.NuSincerado                        
 Left Join MASERVICIOS_DET sds On sds.IDServicio_Det=sd.IDServicio_Det                        
 Left Join PRESUPUESTO_SOBRE_DET psd On  ods.IDOperacion_Det=psd.IDOperacion_Det and IsNull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det) = psd.IDServicio_Det      
 --Left Join PRESUPUESTO_SOBRE_DET psd2 On  od.IDOperacion_Det=psd2.IDOperacion_Det and od.IDServicio_Det = psd2.IDServicio_Det      
 Left Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=psd.NuPreSob                 
-- And ps.IDCab=@IDCab And ps.IDProveedor=@IDProveedor and ps.CoPrvGui=@IDProveedor_PrgGuia                
 Left Join MAPROVEEDORES pr On isnull(psd.CoPrvPrg,ods.IDProveedor_Prg)=pr.IDProveedor          
  Left Join MAMONEDAS mo On isnull(psd.IDMoneda,ods.idmoneda)=mo.IDMoneda
 Where --od.IDGuiaProveedor=@IDProveedor_PrgGuia                        
 (                
 (
	
	od.IDOperacion_Det in
	 (Select ISNULL(psd1.IDOperacion_Det,ods1.IDOperacion_Det)          
	  From OPERACIONES_DET_DETSERVICIOS ods1 Left Join PRESUPUESTO_SOBRE_DET psd1           
	  On ods1.IDOperacion_Det=psd1.IDOperacion_Det          
	            inner join MASERVICIOS s1 on ods1.IDServicio=s1.IDServicio  And s1.FlNoPresupuestos=0
				Inner Join OPERACIONES_DET od1 On ods1.IDOperacion_Det=od1.IDOperacion_Det
	  Where ods1.IDOperacion_Det=od.IDOperacion_Det                        
	  And ods1.IDServicio=od.IDServicio And                 
	  (
	  isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg)=@IDProveedor_PrgGuia Or  
	   (isnull(isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg),'')='' 
	   --And s1.CoPago in('EFE'))
	   And isnull(s1.CoPago,'EFE') in ('EFE'))
	
	--HLF-20160331-I
	or CONVERT(VARCHAR,od1.Dia,103) in	
	(SELECT CONVERT(VARCHAR,od2.Dia,103)
	FROM OPERACIONES_DET_DETSERVICIOS ods2 
	Inner Join OPERACIONES_DET od2 On ods2.IDOperacion_Det=od2.IDOperacion_Det
	Inner Join OPERACIONES o2 On od2.IDOperacion=o2.IDOperacion and o2.IDCab=@IDCab
		and o2.IDProveedor=@IDProveedor
		And ods2.IDProveedor_Prg=@IDProveedor_PrgGuia
	
	)
	--HLF-20160331-F

	 )      
	  )
                 
	

 And (	
	@NuPreSob=0
	)
		
		)
or
 (isnull((select top 1 coestado from PRESUPUESTO_SOBRE where 
	IDCab=@IDCab order by NuPreSob desc),'') 
				In ('NV','PG')
				
				And @NuPreSob<>0  and ps.NuPreSob is null
				And 
				((isnull(ods.IDProveedor_Prg,'')='' 
				--And spr.CoPago in('EFE','TCK') 
				And isnull(spr.CoPago,'EFE') in('EFE','TCK') 
				And lower(spr.Descripcion) Like 'tips%') 
				--Or (@SoloModific=0 And @NuPreSob<>0)
				)
	)
 )		
	And Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET           
            Where IDOperacion_Det=od.IDOperacion_Det And isnull(CoPrvPrg,'')=ltrim(rtrim(@IDProveedor_PrgGuia))
				--And isnull(IDServicio_Det_V_Cot,0)=isnull(ods.IDServicio_Det_V_Cot,0))
				And IDServicio_Det_V_Cot=ods.IDServicio_Det_V_Cot)
			
 ) AS X       
 Where (X.IDProveedorGuia=@IDProveedor_PrgGuia Or Ltrim(Rtrim(X.IDProveedorGuia))='')
	And @SoloModific=0


 Union--Tips Efectivo

Select IDServicio_Det,IDServicio_Det_V_Cot,IDOperacion_Det,Isnull(CAST(NuDetPSo as varchar(5)),'') as NuDetPSo,        
  NuSincerado_Det,Dia,        
  convert(varchar(5),Dia,108) As Hora,                
 isnull(DescProveeGuia,'') as DescProveeGuia,isnull(IDProveedorGuia,'') as IDProveedorGuia,        
 0 as IDPax,
 DescServicio,                    
 isnull(PaxPres,Pax) as Pax,IDMoneda,SimMoneda,
 IsNuLL(PreUniPres,
	Case When isnull(PaxPres,Pax)>0 then
		CAST(ISNULL(TotalPres,ISNULL(Total,TotalServ))/isnull(PaxPres,Pax) as Numeric(8,2))
	else
		0
	end
	) as PreUnit,  
 Case When CoPago = 'STK' Then 0 Else ISNULL(TotalPres ,ISNULL(Total,TotalServ)) End AS Total,                      
Sustentado, Devuelto, RegistradoPresup, RegistradoSincer,                
 CoTipPSo, (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=x.CoTipPSo) as DescCoTipPSo,                                 
 CoPago, DescPago ,'R' RegVal  --,DescServicio2            
 ,NuPreSob, isnull(CoEstado,'') as CoEstado, IDServicio, 2 as NroUnion
 FROM                        

(SELECT 
od.IDServicio_Det As IDServicio_Det,                
 isnull(ods.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,
 
 isnull(sds.IDServicio,isnull(ods.IDServicio,''))as IDServicio,      
 od.IDOperacion_Det,                
 sd.NuSincerado_Det,                 
 od.Dia,                         
 Case When ltrim(rtrim(pr.IDTipoProv)) in('005,008') Then           
  replace(pr.ApPaterno+' '+pr.ApMaterno+', '+pr.Nombre,'-','')             
 Else          
  isnull(pr.NombreCorto,'')             
 End as DescProveeGuia,          
          
 isnull(pr.IDProveedor,'') as IDProveedorGuia,          
 --'----'+
 isnull(psd.TxServicio,                     
  Case When sd.NuSincerado_Det Is Null Then                        
   spr.Descripcion
  Else             
   sds.Descripcion +                         
   Case When IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'')='' Then ''                         
   Else ' - '+IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'') End                   
                          
  End                        
   )                        
                           
  as DescServicio,       
  sds.Descripcion as DescServicio2,      
 isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0)) as Pax,                         

 isnull(sd.SsCostoTotal,
	case when isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0))>0 then
		isnull(ods.TotalProgram,isnull(ods.TotalCotizado,od.TotalGen))/isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0)) 
     else
		0
	 end) as PreUnit,
 sd.SsCostoTotal as Total, 
 
 
 Case When sd.SsCostoTotal is null Then                         
      
 --isnull(sd.SsCostoTotal,isnull(ods.TotalProgram,isnull(ods.TotalCotizado,od.TotalGen)))
 

isnull(isnull(TotalProgram,
	Case When isnull(TotalCotizado,0) = 0 then                                  
		Case When dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,NroPax+NroLiberados) = 0 Or FlServicioTC=1 Then
		od.CostoReal
		Else
		
		 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,NroPax+NroLiberados),                                    
		 sds.CoMoneda,ods.IDMoneda,	ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where                                   
			IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=ods.IDOperacion_Det))                    
			AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=ods.IDOperacion_Det))  
			,(Select SsTipCam From MATIPOSCAMBIO_USD Where CoMoneda=sds.CoMoneda))) 
		End
                   
	  Else                       
	 TotalCotizado                       
	End * case when sds.Afecto=1 then 1+(@NuIgv/100) else 1 end
	)
	 ,0) 


 Else                   
  sd.SsCostoTotal        
 End  
 As TotalServ,        

 isnull(psd.SsTotSus,0) as Sustentado,  
 isnull(psd.SsTotDev,0) as Devuelto,      
Case When psd.NuDetPSo IS NULL Then 0 Else 1 End as RegistradoPresup,                     
-- Case When Exists (Select NuPreSob From PRESUPUESTO_SOBRE_DET Where NuPreSob=ps.NuPreSob And IDOperacion_Det=od.IDOperacion_Det) then 1 else 0 end as RegistradoPresup,
 --Case When sd.NuSincerado_Det IS NULL Then 0 Else 1 End as RegistradoSincer,                        
 Case When isnull(ods.CoEstadoSincerado,'') = 'SA' Then 1 Else 0 End as RegistradoSincer,                      
 --Case When psd.FlTicket IS NULL Then 0 Else psd.FlTicket End as Ticket,                
 psd.NuDetPSo,                
 psd.QtPax as PaxPres,
 
 Case When isnull(psd.CoPago,isnull(spr.CoPago,'EFE')) = 'STK' Then '' Else IsNull(mo.IDMoneda,'SOL') End As IDMoneda, --mo.IDMoneda End as IDMoneda,
 Case When isnull(psd.CoPago,isnull(spr.CoPago,'EFE')) = 'STK' Then '' Else IsNull(mo.Simbolo,'S/.') End as SimMoneda, --mo.Simbolo End as SimMoneda,
 --mo.IDMoneda as IDMoneda,
 --mo.Simbolo as SimMoneda,

 psd.SsPreUni as PreUniPres, psd.SsTotal as TotalPres                    
 ,isnull(psd.CoPago,isnull(spr.CoPago,'EFE')) as CoPago,                
 Case isnull(psd.CoPago,isnull(spr.CoPago,'EFE'))            
 When 'EFE' Then 'EFECTIVO'                
 When 'TCK' Then 'TICKET'                 
 End As DescPago,                
ISnull(psd.CoTipPSo,'PR' ) as CoTipPSo    ,pr.IDTipoProv,ps.NuPreSob, isnull(ps.CoEstado,'') as CoEstado

FROM OPERACIONES_DET_DETSERVICIOS ods 
Inner Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
Inner Join MASERVICIOS spr On ods.IDServicio=spr.IDServicio   And spr.FlNoPresupuestos=0
Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion and o.IDCab=@IDCab
	and o.IDProveedor=@IDProveedor	And isnull(IDProveedor_Prg,'')=''
	--And spr.CoPago in('EFE'
	And isnull(spr.CoPago,'EFE') in('EFE'
	)--,'TCK') 
	And lower(spr.Descripcion) Like 'tips%' 

 Left Join SINCERADO sn On --sd.NuSincerado=sn.NuSincerado And            
  sn.IDOperacion_Det=od.IDOperacion_Det                          
 Left Join SINCERADO_DET sd On --sd.IDServicio_Det=ods.IDServicio_Det_V_Cot                          
  sn.NuSincerado=sd.NuSincerado                        
 Left Join MASERVICIOS_DET sds On sds.IDServicio_Det=sd.IDServicio_Det                        
 Left Join PRESUPUESTO_SOBRE_DET psd On  ods.IDOperacion_Det=psd.IDOperacion_Det and IsNull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det) = psd.IDServicio_Det      
 --Left Join PRESUPUESTO_SOBRE_DET psd2 On  od.IDOperacion_Det=psd2.IDOperacion_Det and od.IDServicio_Det = psd2.IDServicio_Det      
 Left Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=psd.NuPreSob                 
-- And ps.IDCab=@IDCab And ps.IDProveedor=@IDProveedor and ps.CoPrvGui=@IDProveedor_PrgGuia                
 Left Join MAPROVEEDORES pr On isnull(psd.CoPrvPrg,ods.IDProveedor_Prg)=pr.IDProveedor          
  Left Join MAMONEDAS mo On isnull(psd.IDMoneda,ods.idmoneda)=mo.IDMoneda


Where 
	CONVERT(VARCHAR,od.Dia,103) in
	
	(SELECT CONVERT(VARCHAR,od.Dia,103)
	FROM OPERACIONES_DET_DETSERVICIOS ods 
	Inner Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
	Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion and o.IDCab=@IDCab
		and o.IDProveedor=@IDProveedor
		And IDProveedor_Prg=@IDProveedor_PrgGuia
	)
	
	--And ((Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET           
 --           Where IDOperacion_Det=od.IDOperacion_Det And CoPrvPrg=@IDProveedor_PrgGuia)
	--		And @NuPreSob<>0 AND ps.COESTADO In ('NV','PG') )or @NuPreSob=0 )
	--HLF-20160326-I
	--And ((Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET           
 --           Where IDOperacion_Det=od.IDOperacion_Det And isnull(CoPrvPrg,'')=ltrim(rtrim(@IDProveedor_PrgGuia))
	--			And isnull(IDServicio_Det_V_Cot,0)=isnull(ods.IDServicio_Det_V_Cot,0))
	--		And ((@NuPreSob<>0 AND ps.COESTADO In ('NV','PG')) or @NuPreSob=0 ))
	--		)
		And Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET           
            Where IDOperacion_Det=od.IDOperacion_Det And isnull(CoPrvPrg,'')=ltrim(rtrim(@IDProveedor_PrgGuia))
				--And isnull(IDServicio_Det_V_Cot,0)=isnull(ods.IDServicio_Det_V_Cot,0))
				And IDServicio_Det_V_Cot=ods.IDServicio_Det_V_Cot)
		And @NuPreSob=0
		--HLF-20160326-F
	) as X
Where (X.IDProveedorGuia=@IDProveedor_PrgGuia Or Ltrim(Rtrim(X.IDProveedorGuia))='')
	And @SoloModific=0


Union --Tickets (sin guia)


Select IDServicio_Det,IDServicio_Det_V_Cot,IDOperacion_Det,Isnull(CAST(NuDetPSo as varchar(5)),'') as NuDetPSo,                 
  NuSincerado_Det,Dia,        
  convert(varchar(5),Dia,108) As Hora,                
 isnull(DescProveeGuia,'') as DescProveeGuia,isnull(IDProveedorGuia,'') as IDProveedorGuia,        
 0 as IDPax,
 DescServicio,                    
 isnull(PaxPres,Pax) as Pax,IDMoneda,SimMoneda,
 IsNuLL(PreUniPres,
 case when isnull(PaxPres,Pax)>0 then
 CAST(ISNULL(TotalPres ,ISNULL(Total,TotalServ))/isnull(PaxPres,Pax) as Numeric(8,2))
 else 
	0
 end) as PreUnit,  
 Case When CoPago = 'STK' Then 0 Else ISNULL(TotalPres ,ISNULL(Total,TotalServ)) End AS Total,                      
 Sustentado, Devuelto, RegistradoPresup, RegistradoSincer,                
 CoTipPSo, (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=x.CoTipPSo) as DescCoTipPSo,                                 
 CoPago, DescPago ,'R' RegVal  --,DescServicio2            
 ,NuPreSob, isnull(CoEstado,'') as CoEstado, IDServicio, 3 as NroUnion
 FROM                        
(------                        
 Select 
 --isnull(sd.IDServicio_Det,isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)) As IDServicio_Det,                
 od.IDServicio_Det As IDServicio_Det,                
 isnull(ods.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,
 
 isnull(sds.IDServicio,isnull(ods.IDServicio,''))as IDServicio,      
 od.IDOperacion_Det,                
 sd.NuSincerado_Det,                 
 od.Dia,                         
 Case When ltrim(rtrim(pr.IDTipoProv)) in('005,008') Then           
  replace(pr.ApPaterno+' '+pr.ApMaterno+', '+pr.Nombre,'-','')             
 Else          
  isnull(pr.NombreCorto,'')             
 End as DescProveeGuia,          
          
 isnull(pr.IDProveedor,'') as IDProveedorGuia,          
 --'----'+
 isnull(psd.TxServicio,     
  Case When sd.NuSincerado_Det Is Null Then                        
   sdd.Descripcion +                         
   Case When IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'')='' Then ''                         
   Else ' - '+IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'') End                     
  Else             
   sds.Descripcion +                         
   Case When IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'')='' Then ''                         
   Else ' - '+IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'') End                         
                          
  End                        
   )                        
                           
  as DescServicio,       
  sds.Descripcion as DescServicio2,      
 isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0)) as Pax,                         

 --isnull(sd.SsCostoTotal,isnull(ods.TotalProgram,isnull(ods.TotalCotizado,od.TotalGen)))/isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0)) as PreUnit,
 Case When isnull(sds.IDServicio,isnull(ods.IDServicio,''))='LIM00392' Then --Tips Airport
	1.5
 Else
	1
 End as PreUnit,
 --sd.SsCostoTotal as Total, 
 Case When isnull(sds.IDServicio,isnull(ods.IDServicio,''))='LIM00392' Then --Tips Airport     
	isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0))*1.5
 Else
	isnull(sd.QtPax,od.NroPax+ISNULL(od.NroLiberados,0))*1
 End as Total, 
 
 
 Case When sd.SsCostoTotal is null Then                         

isnull(isnull(TotalProgram,
	Case When isnull(TotalCotizado,0) = 0 then                                  
		Case When dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,NroPax+NroLiberados) = 0 Or FlServicioTC=1 Then
		od.CostoReal
		Else
		Case When sdd.IDServicio_Det IS NULL Then      
		 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,NroPax+NroLiberados),                          
		 sds.CoMoneda,ods.IDMoneda,	ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where                                   
			IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=ods.IDOperacion_Det))                    
			AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=ods.IDOperacion_Det))  
			,(Select SsTipCam From MATIPOSCAMBIO_USD Where CoMoneda=sds.CoMoneda))) 
		Else                
		 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(sdd.IDServicio_Det,NroPax+NroLiberados),                                    
		 sds.CoMoneda,ods.IDMoneda,ISNULL((Select SsTipCam From COTICAB_TIPOSCAMBIO Where                                 
			IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=ods.IDOperacion_Det))                    
			AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=ods.IDOperacion_Det))  
			,(Select SsTipCam From MATIPOSCAMBIO_USD Where CoMoneda=sds.CoMoneda))) 
		End 
		End
              
	  Else                       
	 TotalCotizado                       
	End * --case when sds.Afecto=1 then 1+(@NuIgv/100) else 1 end
	case when sdd.Afecto=1 then 1+(@NuIgv/100) else 1 end
	)
	 ,0) 


 Else                        
  sd.SsCostoTotal        
 End  
 As TotalServ,        

 isnull(psd.SsTotSus,0) as Sustentado,                        
 isnull(psd.SsTotDev,0) as Devuelto,    
Case When psd.NuDetPSo IS NULL Then 0 Else 1 End as RegistradoPresup,                        
 Case When isnull(ods.CoEstadoSincerado,'') = 'SA' Then 1 Else 0 End as RegistradoSincer,                      
 psd.NuDetPSo,                
 psd.QtPax as PaxPres,
 
 --Case When isnull(psd.CoPago,isnull(s.CoPago,'EFE')) = 'STK' Then IsNull(mo.IDMoneda,'SOL')  Else IsNull(mo.IDMoneda,'SOL') End As IDMoneda, --mo.IDMoneda End as IDMoneda,
 --Case When isnull(psd.CoPago,isnull(s.CoPago,'EFE')) = 'STK' Then IsNull(mo.Simbolo,'S/.')  Else IsNull(mo.Simbolo,'S/.') End as SimMoneda, --mo.Simbolo End as SimMoneda,
 'SOL'  As IDMoneda, 'S/.'  as SimMoneda,
 psd.SsPreUni as PreUniPres, psd.SsTotal as TotalPres                    
 ,isnull(psd.CoPago,isnull(s.CoPago,'EFE')) as CoPago,                
 Case isnull(psd.CoPago,isnull(s.CoPago,'EFE'))            
 When 'EFE' Then 'EFECTIVO'                
 When 'TCK' Then 'TICKET'                
 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sdd.IDServicio),0) As varchar(4))+')'
 When 'PCM' Then 'PRECOMPRA'
 
 End As DescPago,                
ISnull(psd.CoTipPSo,                
 case when p.IDTipoProv='006' Then 'EN'                 
 else                 
  case when s.copago in('EFE','TCK') and s.Descripcion like 'Tips%' then 'PR' else 'VA' end                
 end) as CoTipPSo      ,p.IDTipoProv,ps.NuPreSob, isnull(ps.CoEstado,'') as CoEstado
 From                         
 OPERACIONES_DET od                          
 Inner Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det 
	And od.IDServicio=ods.IDServicio And ods.Activo=1  
 --and (@NuPreSobre=0 Or ods.IDServicio_Det_V_Cot In(select psd1.IDServicio_Det from PRESUPUESTO_SOBRE_DET psd1 where NuPreSob=@NuPreSobre))      
 Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab      
  And o.IDProveedor=@IDProveedor                        
  --Inner Join MASERVICIOS_DET sdd On isnull(ods.IDServicio_Det_V_Cot,0)=sdd.IDServicio_Det       
  Inner Join MASERVICIOS_DET sdd On isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)=sdd.IDServicio_Det       
 Inner Join MASERVICIOS s On sdd.IDServicio=s.IDServicio   And s.FlNoPresupuestos=0
 Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor                 
 --And p.IDTipoProv='006'                        
   And p.IDTipoProv Not In ('004','005','008')      
            
 Left Join MASERVICIOS spr On ods.IDServicio=spr.IDServicio                      
 Left Join SINCERADO sn On --sd.NuSincerado=sn.NuSincerado And                        
  sn.IDOperacion_Det=od.IDOperacion_Det                          
 Left Join SINCERADO_DET sd On --sd.IDServicio_Det=ods.IDServicio_Det_V_Cot                          
  sn.NuSincerado=sd.NuSincerado                        
 Left Join MASERVICIOS_DET sds On sds.IDServicio_Det=sd.IDServicio_Det           
 Left Join PRESUPUESTO_SOBRE_DET psd On  ods.IDOperacion_Det=psd.IDOperacion_Det and IsNull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det) = psd.IDServicio_Det      
 --Left Join PRESUPUESTO_SOBRE_DET psd2 On od.IDOperacion_Det=psd2.IDOperacion_Det and od.IDServicio_Det = psd2.IDServicio_Det      
 Left Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=psd.NuPreSob                 
-- And ps.IDCab=@IDCab And ps.IDProveedor=@IDProveedor and ps.CoPrvGui=@IDProveedor_PrgGuia                
 Left Join MAPROVEEDORES pr On isnull(psd.CoPrvPrg,ods.IDProveedor_Prg)=pr.IDProveedor          
  Left Join MAMONEDAS mo On isnull(psd.IDMoneda,ods.idmoneda)=mo.IDMoneda
 Where --od.IDGuiaProveedor=@IDProveedor_PrgGuia                        
                 
 od.IDOperacion_Det in
	 (Select ods1.IDOperacion_Det          
	  From OPERACIONES_DET_DETSERVICIOS ods1  inner join MASERVICIOS s1 on ods1.IDServicio=s1.IDServicio      
	               And s1.FlNoPresupuestos=0
	  Where ods1.IDOperacion_Det=od.IDOperacion_Det                      
	  And ods1.IDServicio=od.IDServicio 
	  And                 
	  (
	  --isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg)=@IDProveedor_PrgGuia Or  
	  -- (isnull(isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg),'')='' And 
	   s1.CoPago in('TCK') 
			And lower(s1.Descripcion) Like 'tips%'
			--)
	  )      
	  )-- Or ltrim(rtrim(@IDProveedor_PrgGuia))=''))                        
                 

	And Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET           
            Where IDOperacion_Det=od.IDOperacion_Det And isnull(CoPrvPrg,'')=ltrim(rtrim(@IDProveedor_PrgGuia))
				--And isnull(IDServicio_Det_V_Cot,0)=isnull(ods.IDServicio_Det_V_Cot,0)
				And IDServicio_Det_V_Cot=ods.IDServicio_Det_V_Cot)
				
			
 ) AS X       
 Where @SoloModific=0 And ltrim(rtrim(@IDProveedor_PrgGuia))='' And @NuPreSob=0


 --HLF-20160326-I
 --Union--Modificaciones

 --Select 
 -- IDServicio_Det,
 -- IDServicio_Det_V_Cot,
 -- IDOperacion_Det,Isnull(CAST(NuDetPSo as varchar(5)),'') as NuDetPSo,                 
 -- NuSincerado_Det,Dia,        
 -- convert(varchar(5),Dia,108) As Hora,                
 -- isnull(DescProveeGuia,'') as DescProveeGuia,isnull(IDProveedorGuia,'') as IDProveedorGuia,        
 --0 as IDPax,
 --DescServicio,                    
 --isnull(PaxPres,Pax) as Pax,IDMoneda,SimMoneda,
 --IsNuLL(PreUniPres,CAST(ISNULL(TotalPres ,Total)/isnull(PaxPres,Pax) as Numeric(8,2))) as PreUnit,  
 --Case When CoPago = 'STK' Then 0 Else ISNULL(TotalPres ,Total) End AS Total,                      
 --Sustentado, Devuelto, RegistradoPresup, RegistradoSincer,                
 --CoTipPSo, (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=x.CoTipPSo) as DescCoTipPSo,                                 
 --CoPago, DescPago ,'R' RegVal  --,DescServicio2            
 --,NuPreSob , isnull(CoEstado,'') as CoEstado, IDServicio
 --From

 --(
 --Select 
 ----isnull(sd.IDServicio_Det,isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)) As IDServicio_Det, 
 --l.IDServicio_Det,   
 --l.IDServicio_Det_V_Cot,   
 --isnull(od.IDServicio,'') as IDServicio,      
 --od.IDOperacion_Det,                
 --null as NuSincerado_Det,                 
 --od.Dia,                         
 --Case When ltrim(rtrim(pr.IDTipoProv)) in('005,008') Then           
 -- replace(pr.ApPaterno+' '+pr.ApMaterno+', '+pr.Nombre,'-','')             
 --Else          
 -- isnull(pr.NombreCorto,'')             
 --End as DescProveeGuia,          
          
 --isnull(pr.IDProveedor,'') as IDProveedorGuia,          
 --  --'++++'+
 --  sdd.Descripcion +                         
 --  Case When IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'')='' Then ''                         
 --Else ' - '+IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'') End                         
 -- as DescServicio,       
 -- null as DescServicio2,      
 --case when l.QtPax+ISNULL(l.QtLiberados,0)=0 then
	--od.NroPax+ISNULL(od.NroLiberados,0) 
 --else
	--l.QtPax+ISNULL(l.QtLiberados,0)
 --end as Pax,              
 --case when l.QtPax+ISNULL(l.QtLiberados,0)=0 then
	--l.SsTotal/od.NroPax+ISNULL(od.NroLiberados,0) 
 --else      
	--l.SsTotal/l.QtPax+ISNULL(l.QtLiberados,0) 
 --end as PreUnit, --sd.SsCostoTotal as Total, 
 --l.SsTotal as Total, 
 --0 as Sustentado,                        
 --0 as Devuelto,  
 ----Case When psd.NuDetPSo IS NULL Then 0 Else 1 End as RegistradoPresup,                        
 --0 as RegistradoPresup,
 --0 as RegistradoSincer,     
 --null as NuDetPSo, null as PaxPres,
 --Case When isnull(s.CoPago,'EFE') = 'STK' Then '' Else IsNull(mo.IDMoneda,'SOL') End As IDMoneda, --mo.IDMoneda End as IDMoneda,
 --Case When isnull(s.CoPago,'EFE') = 'STK' Then '' Else IsNull(mo.Simbolo,'S/.') End as SimMoneda, --mo.Simbolo End as SimMoneda, 
 -- null as PreUniPres, null as TotalPres                    
 --,isnull(s.CoPago,'EFE') as CoPago,                
 --Case isnull(s.CoPago,'EFE')
 --When 'EFE' Then 'EFECTIVO'                
 --When 'TCK' Then 'TICKET'                
 --When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sdd.IDServicio),0) As varchar(4))+')'
 --When 'PCM' Then 'PRECOMPRA' 
 --End As DescPago,                

 --case when p.IDTipoProv='006' Then 'EN'                 
 --else                 
 -- case when s.copago in('EFE','TCK') and s.Descripcion like 'Tips%' then 'PR' else 'VA' end                
 --end as CoTipPSo      ,
 --p.IDTipoProv, null as NuPreSob, null as CoEstado
 --From                         
 --OPERACIONES_DET_LOGPRESUPUESTOS l 
 -- Inner Join OPERACIONES_DET od On l.IDOperacion_Det=od.IDOperacion_Det And l.IDServicio_Det=od.IDServicio_Det
 -- And l.FlAnulado=0 And (l.CoAccion='T' or l.CoAccion='N')
 -- Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab      
 -- And o.IDProveedor=@IDProveedor                        

 -- --Inner Join MASERVICIOS_DET sdd On isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)=sdd.IDServicio_Det       
 -- Inner Join MASERVICIOS_DET sdd On od.IDServicio_Det=sdd.IDServicio_Det       
 --Inner Join MASERVICIOS s On sdd.IDServicio=s.IDServicio       
 --Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor                 
 --   And p.IDTipoProv Not In ('004','005','008')                 
 --Left Join MASERVICIOS spr On od.IDServicio=spr.IDServicio                      
 ----Left Join PRESUPUESTO_SOBRE_DET psd On  ods.IDOperacion_Det=psd.IDOperacion_Det and IsNull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det) = psd.IDServicio_Det      
 ----Left Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=psd.NuPreSob                 
 --Left Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det And ods.IDServicio_Det=od.IDServicio_Det
	--And ods.Activo=1
 --Left Join MAPROVEEDORES pr On ods.IDProveedor_Prg=pr.IDProveedor          
 -- Left Join MAMONEDAS mo On od.IDMoneda=mo.IDMoneda
 --Where                                    
 --od.IDOperacion_Det in(
 --Select ISNULL(psd1.IDOperacion_Det,ods1.IDOperacion_Det)          
 -- From OPERACIONES_DET_DETSERVICIOS ods1 Left Join PRESUPUESTO_SOBRE_DET psd1           
 -- On ods1.IDOperacion_Det=psd1.IDOperacion_Det          
 --         inner join MASERVICIOS s1 on ods1.IDServicio=s1.IDServicio      
 -- Where ods1.IDOperacion_Det=od.IDOperacion_Det                        
 -- And ods1.IDServicio=od.IDServicio And      
 -- isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg)=@IDProveedor_PrgGuia      
 -- --HLF-20160308-I     
 -- --(
	-- -- (
	--	--isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg)=@IDProveedor_PrgGuia AND
	-- --   (isnull(isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg),'')='' And s1.CoPago in('EFE')   
	--	--And lower(s1.Descripcion) Like 'tips%')
	--	--And (@SoloModific=1 And @NuPreSob=0) And ltrim(rtrim(@IDProveedor_PrgGuia))<>''
	-- -- )
 -- --or
	-- -- (s1.CoPago in('TCK')   
	--	--And lower(s1.Descripcion) Like 'tips%')
	--	--And (@SoloModific=1 And @NuPreSob=0) And ltrim(rtrim(@IDProveedor_PrgGuia))=''
	-- -- )
 ----HLF-20160308-F

 -- )
	--And (

   
 --  (
 --  (
 --isnull((select top 1 coestado from PRESUPUESTO_SOBRE where 
	--IDCab=@IDCab order by NuPreSob desc),'') 
	--			In ('NV','PG')
	--			And @NuPreSob<>0)	
	--			--HLF-20160308-I			
	--			--Or (@SoloModific=1 And @NuPreSob=0)
	--			--HLF-20160308-F
	--			Or @NuPreSob=0
	--			)
	
 --  )				
				
							
 --) AS X       
 --Where (X.IDProveedorGuia=@IDProveedor_PrgGuia Or Ltrim(Rtrim(X.IDProveedorGuia))='' Or (@SoloModific=1 And @NuPreSob=0))
	
 ----Fin Modificaciones

 -- Union--Modificaciones 2

 --Select 
 -- IDServicio_Det,IDServicio_Det_V_Cot,
 -- IDOperacion_Det,Isnull(CAST(NuDetPSo as varchar(5)),'') as NuDetPSo,                 
 -- NuSincerado_Det,Dia,        
 -- convert(varchar(5),Dia,108) As Hora,  
 --  isnull(DescProveeGuia,'') as DescProveeGuia,isnull(IDProveedorGuia,'') as IDProveedorGuia,        
 --0 as IDPax,
 --DescServicio,                    
 --isnull(PaxPres,Pax) as Pax,IDMoneda,SimMoneda,
 --IsNuLL(PreUniPres,CAST(ISNULL(TotalPres ,Total)/isnull(PaxPres,Pax) as Numeric(8,2))) as PreUnit,  
 --Case When CoPago = 'STK' Then 0 Else ISNULL(TotalPres ,Total) End AS Total,                      
 --Sustentado, Devuelto, RegistradoPresup, RegistradoSincer,                
 --CoTipPSo, (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=x.CoTipPSo) as DescCoTipPSo,                                 
 --CoPago, DescPago ,'R' RegVal  --,DescServicio2            
 --,NuPreSob , isnull(CoEstado,'') as CoEstado, IDServicio
 --From

 --(
 --Select 
 ----isnull(sd.IDServicio_Det,isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)) As IDServicio_Det,             
 --l.IDServicio_Det,   
 --l.IDServicio_Det_V_Cot,   
 --isnull(sds.IDServicio,isnull(ods.IDServicio,''))as IDServicio,      
 --od.IDOperacion_Det,                
 --sd.NuSincerado_Det,                 
 --od.Dia,                         
 --Case When ltrim(rtrim(pr.IDTipoProv)) in('005,008') Then           
 -- replace(pr.ApPaterno+' '+pr.ApMaterno+', '+pr.Nombre,'-','')             
 --Else          
 -- isnull(pr.NombreCorto,'')     
 --End as DescProveeGuia,          
          
 --isnull(pr.IDProveedor,'') as IDProveedorGuia,          
 ----'****'+
 -- Case When sd.NuSincerado_Det Is Null Then                        
 --  sdd.Descripcion +                         
 --  Case When IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'')='' Then ''        
 --  Else ' - '+IsNull(ltrim(rtrim(cast(sdd.DetaTipo as varchar(250)))),'') End                         
 -- Else             
 --  sds.Descripcion +                         
 --  Case When IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'')='' Then ''                         
 --  Else ' - '+IsNull(ltrim(rtrim(cast(sds.DetaTipo as varchar(250)))),'') End                         
                          
 -- End as DescServicio,       
 -- sds.Descripcion as DescServicio2,      
 --case when l.QtPax+ISNULL(l.QtLiberados,0)=0 then
	--od.NroPax+ISNULL(od.NroLiberados,0) 
 --else
	--l.QtPax+ISNULL(l.QtLiberados,0)
 --end as Pax,              
 --case when l.QtPax+ISNULL(l.QtLiberados,0)=0 then
	--l.SsTotal/od.NroPax+ISNULL(od.NroLiberados,0) 
 --else           
	--l.SsTotal/l.QtPax+ISNULL(l.QtLiberados,0) 
 --end as PreUnit,
 ----sd.SsCostoTotal as Total, 
 --l.SsTotal as Total, 
 --0 as Sustentado,                        
 --0 as Devuelto,                        
 ----Case When psd.NuDetPSo IS NULL Then 0 Else 1 End as RegistradoPresup,                        
 --0 as RegistradoPresup,
 --Case When isnull(ods.CoEstadoSincerado,'') = 'SA' Then 1 Else 0 End as RegistradoSincer,     
 --null as NuDetPSo, null as PaxPres,
 --Case When isnull(s.CoPago,'EFE') = 'STK' Then '' Else IsNull(mo.IDMoneda,'SOL') End As IDMoneda, --mo.IDMoneda End as IDMoneda,
 --Case When isnull(s.CoPago,'EFE') = 'STK' Then '' Else IsNull(mo.Simbolo,'S/.') End as SimMoneda, --mo.Simbolo End as SimMoneda, 
 --null as PreUniPres, null as TotalPres                    
 --,isnull(s.CoPago,'EFE') as CoPago,                
 --Case isnull(s.CoPago,'EFE')
 --When 'EFE' Then 'EFECTIVO'                
 --When 'TCK' Then 'TICKET'                
 --When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sdd.IDServicio),0) As varchar(4))+')'
 --When 'PCM' Then 'PRECOMPRA' 
 --End As DescPago,                

 --case when p.IDTipoProv='006' Then 'EN'                 
 --else                 
 -- case when s.copago in('EFE','TCK') and s.Descripcion like 'Tips%' then 'PR' else 'VA' end                
 --end as CoTipPSo      ,
 --p.IDTipoProv, null as NuPreSob, null as CoEstado
 --From                         
 --OPERACIONES_DET_DETSERVICIOS_LOGPRESUPUESTOS l 
 --Inner Join OPERACIONES_DET_DETSERVICIOS ods On l.IDOperacion_Det=ods.IDOperacion_Det 
	--And l.IDServicio_Det_V_Cot=isnull(ods.IDServicio_Det_V_Cot,0)
	--And l.IDServicio_det=ods.IDServicio_Det
	--And ods.Activo=1      
	--And l.FlAnulado=0 And (l.CoAccion='T' or l.CoAccion='N')
	--Inner Join OPERACIONES_DET od On od.IDOperacion_Det=ods.IDOperacion_Det 
	--	And od.IDServicio=ods.IDServicio
 -- Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab      
-- And o.IDProveedor=@IDProveedor                        
                    
 -- Inner Join MASERVICIOS_DET sdd On isnull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det)=sdd.IDServicio_Det       
 --Inner Join MASERVICIOS s On sdd.IDServicio=s.IDServicio       
 --Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor                 
 --   And p.IDTipoProv Not In ('004','005','008')                 
 --Left Join MASERVICIOS spr On ods.IDServicio=spr.IDServicio       
 --Left Join SINCERADO sn On --sd.NuSincerado=sn.NuSincerado And                        
 -- sn.IDOperacion_Det=od.IDOperacion_Det          
 --Left Join SINCERADO_DET sd On --sd.IDServicio_Det=ods.IDServicio_Det_V_Cot                          
 -- sn.NuSincerado=sd.NuSincerado                        
 --Left Join MASERVICIOS_DET sds On sds.IDServicio_Det=sd.IDServicio_Det                        
 ----Left Join PRESUPUESTO_SOBRE_DET psd On  ods.IDOperacion_Det=psd.IDOperacion_Det and IsNull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det) = psd.IDServicio_Det      
 ----Left Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=psd.NuPreSob                 

 --Left Join MAPROVEEDORES pr On ods.IDProveedor_Prg=pr.IDProveedor          
 -- Left Join MAMONEDAS mo On ods.IDMoneda=mo.IDMoneda
 --Where --od.IDGuiaProveedor=@IDProveedor_PrgGuia                                         
 --od.IDOperacion_Det in(
 --Select ISNULL(psd1.IDOperacion_Det,ods1.IDOperacion_Det)          
 -- From OPERACIONES_DET_DETSERVICIOS ods1 Left Join PRESUPUESTO_SOBRE_DET psd1           
 -- On ods1.IDOperacion_Det=psd1.IDOperacion_Det          
 -- --and (@NuPreSobre=0 Or ods.IDServicio_Det_V_Cot In(select psd1.IDServicio_Det from PRESUPUESTO_SOBRE_DET psd1 where NuPreSob=@NuPreSobre))      
 -- --And psd1.CoPrvPrg=ods1.IDProveedor_Prg        
 --         inner join MASERVICIOS s1 on ods1.IDServicio=s1.IDServicio      
 -- Where ods1.IDOperacion_Det=od.IDOperacion_Det                        
 -- And ods1.IDServicio=od.IDServicio And                 
 -- isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg)=@IDProveedor_PrgGuia
 -- --HLF-20160308-I
 -- --(
 -- --(
	-- -- (isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg)=@IDProveedor_PrgGuia AND  
	-- --  (isnull(isnull(psd1.CoPrvPrg,ods1.IDProveedor_Prg),'')='' And s1.CoPago in('EFE')   
	--	--And lower(s1.Descripcion) Like 'tips%')
	--	--And (@SoloModific=1 And @NuPreSob=0) And ltrim(rtrim(@IDProveedor_PrgGuia))<>''
	-- -- )
 -- --or
	-- -- (s1.CoPago in('TCK')   
	--	--And lower(s1.Descripcion) Like 'tips%')
	--	--And (@SoloModific=1 And @NuPreSob=0) And ltrim(rtrim(@IDProveedor_PrgGuia))=''
	-- -- )
 -- --HLF-20160308-F
 -- )

	--And (
		
	
 --(
	--(isnull((select top 1 coestado from PRESUPUESTO_SOBRE where 
	--IDCab=@IDCab order by NuPreSob desc),'') 
	--			In ('NV','PG')
	--			And @NuPreSob<>0)
	--			--HLF-20160308-I
	--			--Or (@SoloModific=1 And @NuPreSob=0)
	--			--HLF-20160308-F
	--			Or @NuPreSob=0
	--			) 
				
 --  )				
							
 --) AS X       
 --Where (X.IDProveedorGuia=@IDProveedor_PrgGuia Or Ltrim(Rtrim(X.IDProveedorGuia))='' Or (@SoloModific=1 And @NuPreSob=0))
	

 ----Fin Modificaciones 2
 --HLF-20160326-F

 Union
 Select IsNull(pd.IDServicio_Det,0) as IDServicio_Det,
 --0 AS IDServicio_Det_V_Cot,
 pd.IDServicio_Det_V_Cot AS IDServicio_Det_V_Cot,
 IsNull(PD.IDOperacion_Det,0) as IDOperacion_Det,
 pd.NuDetPSo,0 as NuSincerado_Det,pd.FeDetPSo as Dia,Convert(char(5),pd.FeDetPSo,108) as Hora,
 IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescProveeGuia,
 IsNull(pd.CoPrvPrg,'') as CoPrvPrg,IsNull(pd.IDPax,0) as IDPax, pd.TxServicio,
 pd.QtPax,IsNull(pd.IDMoneda,'') as IDMoneda,IsNull(mo.Simbolo,'') as SimMoneda, pd.SsPreUni, pd.SsTotal as Total, 
 pd.SsTotSus as Sustentado, pd.SsTotDev as Devuelto, 1 as RegistradoPresup,0 as RegistradoSincer,pd.CoTipPSo,es.NoTipPSo as DescCotipPSo,

 pd.CoPago,
 Case pd.CoPago           
 When 'EFE' Then 'EFECTIVO'        
 When 'TCK' Then 'TICKET'                
 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where 
	IDServicio=(select IDServicio from MASERVICIOS_DET where IDServicio_Det = pd.IDServicio_Det_V_Cot)),0) As varchar(4))+')'
 When 'PCM' Then 'PRECOMPRA'
 --When 'PCM' Then 'PRECOMPRA ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio_Det=pd.IDServicio_Det),0) As varchar(4))+')'
 --When '' Then 'EFECTIVO'                
 End As DescPago,'R',pd.NuPreSob, isnull(ps.CoEstado,'') as CoEstado,sd.IDServicio, 4 as NroUnion
 from PRESUPUESTO_SOBRE_DET pd Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor
 Left Join PRESUPUESTO_SOBRE ps On pd.NuPreSob=ps.NuPreSob
 Left Join COTIPAX cp On pd.IDPax=cp.IDPax
 Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
 Left Join MAMONEDAS mo On pd.IDMoneda=mo.IDMoneda
 --Left Join MASERVICIOS_DET sd On sd.IDServicio_Det=pd.IDServicio_Det
 Left Join MASERVICIOS_DET sd On sd.IDServicio_Det=pd.IDServicio_Det_V_Cot
 Where pd.NuPreSob=@NuPreSob --and pd.IDOperacion_Det is Null
	And @NuPreSob<>0 
	And @SoloModific=0
Union
 Select IsNull(pd.IDServicio_Det,0) as IDServicio_Det,
 --0 AS IDServicio_Det_V_Cot,
 pd.IDServicio_Det_V_Cot AS IDServicio_Det_V_Cot,
 IsNull(PD.IDOperacion_Det,0) as IDOperacion_Det,
 pd.NuDetPSo,0 as NuSincerado_Det,pd.FeDetPSo as Dia,Convert(char(5),pd.FeDetPSo,108) as Hora,
 IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescProveeGuia,
 IsNull(pd.CoPrvPrg,'') as CoPrvPrg,IsNull(pd.IDPax,0) as IDPax, pd.TxServicio,
 pd.QtPax,IsNull(pd.IDMoneda,'') as IDMoneda,IsNull(mo.Simbolo,'') as SimMoneda, pd.SsPreUni, pd.SsTotal as Total, 
 pd.SsTotSus as Sustentado, pd.SsTotDev as Devuelto, 1 as RegistradoPresup,0 as RegistradoSincer,pd.CoTipPSo,es.NoTipPSo as DescCotipPSo,
 pd.CoPago,
 Case pd.CoPago           
 When 'EFE' Then 'EFECTIVO'                
 When 'TCK' Then 'TICKET'                
 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where 
	IDServicio=(select IDServicio from MASERVICIOS_DET where IDServicio_Det = pd.IDServicio_Det_V_Cot)),0) As varchar(4))+')'
 When 'PCM' Then 'PRECOMPRA'
 --When 'PCM' Then 'PRECOMPRA ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio_Det=pd.IDServicio_Det),0) As varchar(4))+')'
 --When '' Then 'EFECTIVO'                
 End As DescPago,'R',pd.NuPreSob, isnull(ps.CoEstado,'') as CoEstado,sd.IDServicio, 5 as NroUnion
 from PRESUPUESTO_SOBRE_DET pd Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor
 Left Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=pd.NuPreSob
 Left Join COTIPAX cp On pd.IDPax=cp.IDPax
 Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
 Left Join MAMONEDAS mo On pd.IDMoneda=mo.IDMoneda
 --Left Join MASERVICIOS_DET sd On sd.IDServicio_Det=pd.IDServicio_Det
 Left Join MASERVICIOS_DET sd On sd.IDServicio_Det=pd.IDServicio_Det_V_Cot
 Where pd.NuPreSob=@NuPreSob and pd.IDOperacion_Det is Null
	And @NuPreSob<>0
	And @SoloModific=0
 ) as Y

Order by Y.CoPago,Y.Dia 



