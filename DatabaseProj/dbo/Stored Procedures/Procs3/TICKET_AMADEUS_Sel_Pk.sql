﻿
--JRF-20140627-Agregar el RUC  
CREATE Procedure dbo.TICKET_AMADEUS_Sel_Pk  
@CoTicket char(13)    
As    
Set NoCount On    
Select t.* ,Isnull(c.IDFile,'') as IDFile,ISNULL(c.Cotizacion,'')  as Cotizacion  
,cl.IDCliente as IDClienteDef,Isnull(cl.RazonComercial,'') as DescCliente,  
Case When pSgl.IDIdentidad = '001' then pSgl.NumIdentidad else '' End as NumRUC  
from TICKET_AMADEUS t Left Join COTICAB c On t.IDCab = c.IDCAB    
Left Join MACLIENTES cl On Isnull(t.IDCliente,c.IDCliente) = cl.IDCliente    
Left Join MAPROVEEDORES pSgl On t.CoLineaAereaSigla = pSgl.Sigla  
where CoTicket = @CoTicket    
