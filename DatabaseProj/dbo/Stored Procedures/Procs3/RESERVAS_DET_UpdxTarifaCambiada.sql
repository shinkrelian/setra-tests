﻿Create Procedure dbo.RESERVAS_DET_UpdxTarifaCambiada
@IDServicio_Det int,
@UserMod char(4)
As
	Set NoCount On
	Update RESERVAS_DET 
		Set FeUpdateRow=GETDATE(),
			UserMod = @UserMod,
			FecMod = GETDATE()
	From RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
	Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det
	Where Not ((p.IDTipoProv='001' And sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' and ConAlojamiento=1))
	And p.IDTipoOper='E' And sd.IDServicio_Det=@IDServicio_Det
