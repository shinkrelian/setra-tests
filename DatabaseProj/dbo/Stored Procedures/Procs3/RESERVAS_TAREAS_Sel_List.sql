﻿CREATE Procedure  [dbo].[RESERVAS_TAREAS_Sel_List]
	@IDUsuarioRes	char(4),
	@FechaDeadLine	smalldatetime,
	@Estado	char(2)
As
	Set NoCount On
			
	Select us.Usuario, rs.FecDeadLine, ct.FechaReserva, 
	rs.IDReserva,
	Case When rs.IDReserva=0 Then
		''
	Else
		r.IDProveedor 
	End	as IDProveedor,
	pr.IDTipoProv,
	Case When rs.IDReserva=0 Then
		'PROVEEDORES FILE'
	Else
		pr.NombreCorto 
	End	as DescProveedor,	
	pr.Email3 as CorreoProv,
	Case When rs.IDReserva=0 Then
		rec.Descripcion
	Else
		rs.Descripcion
	End	As TipoTarea,
	ct.Titulo,
    rs.IDEstado as IDEstado,
    Case rs.IDEstado 
		When 'PD' Then 'PENDIENTE'
		When 'RZ' Then 'REALIZADA'
    End As Estado, 
    ct.IDFile, 
    rs.IDCab, r.CodReservaProv, cl.RazonComercial as DescCliente,
    rs.IDTarea,
    Case When rs.IDReserva=0 Then 'POR FILE'
	Else
		Case When rs.IDTarea<=6 Then 
			'POR POLITICA PROV.'
		Else 'MANUAL'
		End
	End +
	Case When rs.UltimoMinuto=1 Then '(Último Minuto)' Else '' End
	as Tipo

	From RESERVAS_TAREAS rs 	
	Left Join RESERVAS r On rs.IDReserva=r.IDReserva 
	Left Join MAPROVEEDORES pr On r.IDProveedor=pr.IDProveedor
	--Left Join RESERVAS_DET dr On rs.IDReserva=dr.IDReserva
	--Left Join MASERVICIOS_DET ds On dr.IDServicio_Det=ds.IDServicio_Det
	--Left Join MASERVICIOS s On dr.IDServicio=s.IDServicio
	Left Join COTICAB ct On rs.IDCab=ct.IDCAB
	--Left Join COTIDET cd On dr.IDDet=cd.IDDET
	Left Join MACLIENTES cl On ct.IDCliente=cl.IDCliente
	Left Join MAUSUARIOS us On ct.IDUsuarioRes=us.IDUsuario	
	Left Join MARECORDATORIOSRESERVAS rec On rec.IDRecordatorio=rs.IDTarea
	Where (ct.IDUsuarioRes=@IDUsuarioRes OR ltrim(rtrim(@IDUsuarioRes))='')
	And (r.Estado=@Estado Or ltrim(rtrim(@Estado))='')
	And CONVERT(varchar,rs.FecDeadLine,112)=CONVERT(varchar,@FechaDeadLine,112)
	Order by rs.FecDeadLine
