﻿
----------------------------------------------------------------------------------------------------

CREATE PROCEDURE [RH_TRABAJADOR_Del]
	@CoTrabajador char(11)
AS
BEGIN
	Delete [dbo].[RH_TRABAJADOR]
	Where 
		[CoTrabajador] = @CoTrabajador
END
