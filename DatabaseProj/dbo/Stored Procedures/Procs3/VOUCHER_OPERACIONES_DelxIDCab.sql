﻿Create Procedure dbo.VOUCHER_OPERACIONES_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM VOUCHER_OPERACIONES WHERE IDVoucher IN (select IDVoucher FROM OPERACIONES_DET 
	WHERE idoperacion in (Select IDOperacion FROM OPERACIONES WHERE IDCab=@IDCab))
	AND IDVoucher<>0

