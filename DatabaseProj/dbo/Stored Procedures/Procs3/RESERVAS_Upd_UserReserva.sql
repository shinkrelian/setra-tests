﻿ 
--JRF-20131017-Actualizar la Fecha de Asignación de Ejecutivo.  
--HLF-20141015-	
	--Case when COTICAB.FechaAsigReserva is null then 
		--GETDATE() 
	--else 
	--	COTICAB.FechaAsigReserva 
	--End
CREATE Procedure RESERVAS_Upd_UserReserva    
	@IDCab int,    
	@IDUsuarioReser char(4),  
	@UserMod char(4)    
As     

 Set NoCount On    

 Update COTICAB     
  Set IDUsuarioRes = @IDUsuarioReser  
  ,FechaAsigReserva = 
  case when LTRIM(rtrim(@IDUsuarioReser))='0000' then 
	null 
  else 
	--GETDATE() 

	Case when COTICAB.FechaAsigReserva is null then 
		GETDATE() 
	else 
		COTICAB.FechaAsigReserva 
	End
	
  End  
  ,UserMod = @UserMod,FecMod = GetDate()    
 Where IDCAB = @IDCab     
       
 Update RESERVAS    
  Set IDUsuarioRes=@IDUsuarioReser,UserMod = @UserMod,FecMod = Getdate()    
 Where IDCab = @IDCab    
