﻿
  
CREATE Procedure dbo.RESERVAS_DET_UpdTipoLib  
 @IDCab int,  
 @UserMod char(4)   
As  
 Set Nocount On  
   
 Update RESERVAS_DET Set RESERVAS_DET.Tipo_Lib=Null, 
 RESERVAS_DET.UserMod=@UserMod, RESERVAS_DET.FecMod=GETDATE()  
 Where ISNULL(RESERVAS_DET.NroLiberados,0)=0  
 And RESERVAS_DET.IDReserva In (Select IDReserva From RESERVAS Where IDCab=@IDCab)  

