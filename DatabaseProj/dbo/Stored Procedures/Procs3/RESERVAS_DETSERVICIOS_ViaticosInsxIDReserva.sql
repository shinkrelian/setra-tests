﻿--JRF-20151216-...Not Exists (Select CAST(IDReserva_Det as varchar(20)) + ' '+ CAST(rds.IDServicio_Det as 
CREATE Procedure [dbo].[RESERVAS_DETSERVICIOS_ViaticosInsxIDReserva]
	@IDCab int,
	@IDReserva int,  
	@UserMod char(4)
As  
	Set NoCount On  
    Declare @ExistenViaticos bit = Case when exists(select * from RESERVAS_DET 
	where IDReserva = @IDReserva and Anulado=0 And IsNull(NuViaticoTC,0) > 0) then 1 else 0 End
     
	Insert Into RESERVAS_DETSERVICIOS  
	(IDReserva_Det,IDServicio_Det,  
	CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,  
	SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,  
	MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,  
	STMargenImpto,TotImpto,UserMod)    

	Select IDReserva_Det,IDServicio_Det,      
	CostoReal*TCambio,X.CostoLiberado*TCambio,X.Margen*TCambio,X.MargenAplicado,
	X.MargenLiberado*TCambio,X.Total*TCambio,      
	X.SSCR*TCambio,X.SSCL*TCambio,X.SSMargen*TCambio,X.SSMargenLiberado*TCambio,
	X.SSTotal*TCambio,X.STCR*TCambio,X.STMargen*TCambio,X.STTotal*TCambio,
	X.CostoRealImpto*TCambio,X.CostoLiberadoImpto*TCambio,X.MargenImpto*TCambio,
	X.MargenLiberadoImpto*TCambio,X.SSCRImpto*TCambio,X.SSCLImpto*TCambio,
	X.SSMargenImpto*TCambio,X.SSMargenLiberadoImpto*TCambio,X.STCRImpto*TCambio,
	X.STMargenImpto*TCambio,X.TotImpto*TCambio,@UserMod
	From

		(
		Select Distinct rd.IDReserva_Det,rd.IDServicio_Det,  
		rd.CostoReal,rd.CostoLiberado,rd.Margen,rd.MargenAplicado,rd.MargenLiberado,rd.Total,  
		0 as SSCR,0 as SSCL,0 as SSMargen,0 as SSMargenLiberado,0 as SSTotal,0 as STCR,  
		0 as STMargen,0 as STTotal,rd.CostoRealImpto,rd.CostoLiberadoImpto,rd.MargenImpto,rd.MargenLiberadoImpto,  
		0 as SSCRImpto,0 as SSCLImpto,0 as SSMargenImpto,0 as SSMargenLiberadoImpto,0 as STCRImpto,0 as STMargenImpto,  
		0 as TotImpto,
		Case When isnull(rd.IDMoneda,'USD')='USD' Then 
		1 
		Else
			Case When tc.SsTipCam Is Null Then sd.TC Else tc.SsTipCam End      
		End as TCambio		
		From RESERVAS_DET rd 
		Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det  
		Inner Join RESERVAS res On res.IDReserva=@IDReserva And res.IDProveedor='001554'
		Left Join COTICAB_TIPOSCAMBIO tc On tc.IDCab=@IDCab And tc.CoMoneda=rd.IDMoneda
		Where rd.Total>0 And IsNull(rd.NuViaticoTC,0) > 0  And
		--CAST(rd.IDReserva_Det as varchar(20)) + ' '+ CAST(rd.IDServicio_Det as varchar(20))   
		Not Exists (Select CAST(IDReserva_Det as varchar(20)) + ' '+ CAST(rds.IDServicio_Det as varchar(20))  
		From RESERVAS_DETSERVICIOS rds Left Join MASERVICIOS_DET sd On rds.IDServicio_Det=sd.IDServicio_Det
		Where (rds.IDReserva_Det=rd.IDReserva_Det And rds.IDServicio_Det=sd.IDServicio_Det) And Not sd.CoTipoCostoTC is null)
		) as X 
		--Where @IDCab = 0
		--where @ExistenViaticos = 0 And Not @IDCab In(13643,13646)
