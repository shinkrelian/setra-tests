﻿CREATE Procedure dbo.TICKET_AMADEUS_TAX_Upd
@CoTicket char(13),
@NuTax tinyint,
@CoTax char(5),
@SsMonto numeric(6,2),
@UserMod char(4)
As
Set NoCount On
UPDATE [dbo].[TICKET_AMADEUS_TAX]
   SET [CoTax] = @CoTax
      ,[SsMonto] = @SsMonto
      ,[UserMod] = @UserMod
      ,[FecMod] = GetDate()
 WHERE [CoTicket] = @CoTicket
       and [NuTax] =@NuTax

