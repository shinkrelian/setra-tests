﻿Create Procedure dbo.sysmail_account_Upd
	@account_id int,
	@email_address nvarchar(128),
	@display_name nvarchar(128)	
As
	Set NoCount On  
	
	Update [msdb].[dbo].[sysmail_account]
    Set [name]=@email_address,
	[email_address]=@email_address,
    [display_name]=@display_name,
    [replyto_address]=@email_address,
    [last_mod_datetime]=getdate()
    Where account_id=@account_id
  
