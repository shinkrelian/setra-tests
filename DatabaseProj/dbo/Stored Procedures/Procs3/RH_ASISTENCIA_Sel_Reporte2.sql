﻿
-----------------------------------------------------------------

--[RH_ASISTENCIA_Sel_Reporte2] '01/01/2014','31/12/2014'
create PROCEDURE [RH_ASISTENCIA_Sel_Reporte2]

@FeFecha1 smalldatetime=null,
@FeFecha2 smalldatetime=null

AS
BEGIN
SELECT     RH_AREA.NoArea, RH_SEDE.TxSede, 
CoTrabajador=ltrim(rtrim(RH_TRABAJADOR.CoTrabajador)), 
Trabajador=RH_TRABAJADOR.NoApPaterno+' '+ RH_TRABAJADOR.NoApMaterno+', '+ RH_TRABAJADOR.NoNombres,
 RH_ASISTENCIA.FeFecha,
 NUSEMANA,
 Semana='SEMANA '+cast(NUSEMANA as CHAR(1)),
 
 RH_ASISTENCIA.NuMinutosTarde,
 QtMinutosTarde=case when RH_ASISTENCIA.NuMinutosTarde>0 then 1 else 0 end,
 Max_Horas_Trabajadas=(
							SELECT  top 1   RH_HORARIO.NuMaxHorasSemana
							FROM         RH_HORARIO INNER JOIN
												  RH_TRABAJADOR_HORARIO ON RH_HORARIO.CoHorario = RH_TRABAJADOR_HORARIO.CoHorario
							WHERE     (RH_TRABAJADOR_HORARIO.CoTrabajador = RH_TRABAJADOR.CoTrabajador) AND ((RH_ASISTENCIA.FeFecha > RH_TRABAJADOR_HORARIO.FeFechaInicio) and RH_ASISTENCIA.FeFecha<(RH_TRABAJADOR_HORARIO.FeFechaFin+1) )
 
						),
						
 Horas_Refrigerio=(
							SELECT  top 1   RH_HORARIO.FeHorasRefrigerio
							FROM         RH_HORARIO INNER JOIN
												  RH_TRABAJADOR_HORARIO ON RH_HORARIO.CoHorario = RH_TRABAJADOR_HORARIO.CoHorario
							WHERE     (RH_TRABAJADOR_HORARIO.CoTrabajador = RH_TRABAJADOR.CoTrabajador) AND ((RH_ASISTENCIA.FeFecha > RH_TRABAJADOR_HORARIO.FeFechaInicio) and RH_ASISTENCIA.FeFecha<(RH_TRABAJADOR_HORARIO.FeFechaFin+1) )
 
						),
 RH_ASISTENCIA.NuHorasTrabajadas
 
 into #reporte1
FROM         RH_AREA_SEDE INNER JOIN
                      RH_AREA ON RH_AREA_SEDE.CoArea = RH_AREA.CoArea INNER JOIN
                      RH_SEDE ON RH_AREA_SEDE.CoSede = RH_SEDE.CoSede INNER JOIN
                      RH_TRABAJADOR_CARGO INNER JOIN
                      RH_TRABAJADOR ON RH_TRABAJADOR_CARGO.CoTrabajador = RH_TRABAJADOR.CoTrabajador ON 
                      RH_AREA_SEDE.CoArea = RH_TRABAJADOR_CARGO.CoArea AND RH_AREA_SEDE.CoSede = RH_TRABAJADOR_CARGO.CoSede INNER JOIN
                      RH_ASISTENCIA ON RH_TRABAJADOR_CARGO.NuTrabCargo = RH_ASISTENCIA.CoTrabCargo
where ((FeFecha > @FeFecha1) and FeFecha<(@FeFecha2+1) )
order by NuSemana,noarea,Trabajador

--select * from #reporte1 where cotrabajador='72181761'


SELECT     NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 --FeFecha,
 NUSEMANA,
 Semana,
 
 NuMinutosTarde=SUM(NuMinutosTarde),
 QtMinutosTarde=SUM(QtMinutosTarde),
 Max_Horas_Trabajadas,
						
 --Horas_Refrigerio,
 --NuHorasTrabajadas=SUM(NuHorasTrabajadas)
 NuHorasTrabajadas=  sum( DATEPART(SECOND, NuHorasTrabajadas) + 60 * DATEPART(MINUTE, NuHorasTrabajadas) + 3600 * DATEPART(HOUR, NuHorasTrabajadas ) ) ,
 NuHorasTrabajadas2= dbo.FnConvertir_Segundos_a_Tiempo(sum( DATEPART(SECOND, NuHorasTrabajadas) + 60 * DATEPART(MINUTE, NuHorasTrabajadas) + 3600 * DATEPART(HOUR, NuHorasTrabajadas ) ) )  --(SELECT CONVERT(VARCHAR, DATEADD(second,sum( DATEPART(SECOND, NuHorasTrabajadas) + 60 * DATEPART(MINUTE, NuHorasTrabajadas) + 3600 * DATEPART(HOUR, NuHorasTrabajadas ) ) ,0),108))
into #reporte2
from #reporte1
group by  NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 --FeFecha,
 NUSEMANA,
 semana,
 Max_Horas_Trabajadas
order by NuSemana,noarea,Trabajador 
 
 
 SELECT     NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 NUSEMANA,
 Semana,
 
 NuMinutosTarde,
 QtMinutosTarde,
 Max_Horas_Trabajadas,
						
 NuHorasTrabajadas,
 NuHorasTrabajadas2,
 NuHorasExtra=case when (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))<0 then 0 else  (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600)) end,
 NuHorasExtra2=case when (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))<0 then '00:00:00' else  dbo.FnConvertir_Segundos_a_Tiempo((NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))) end
into #reporte3
from #reporte2 


SELECT  distinct   NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 --NUSEMANA,
 --Semana,
 
 NuMinutosTarde_S1=(select sum(NuMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),
 NuMinutosTarde_S2=(select sum(NuMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),
 NuMinutosTarde_S3=(select sum(NuMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),
 NuMinutosTarde_S4=(select sum(NuMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4),
 
 --QtMinutosTarde,
 
 QtMinutosTarde_S1=(select sum(QtMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),
 QtMinutosTarde_S2=(select sum(QtMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),
 QtMinutosTarde_S3=(select sum(QtMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),
 QtMinutosTarde_S4=(select sum(QtMinutosTarde) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4),
 
 

 --Max_Horas_Trabajadas,
						
 --NuHorasTrabajadas,
 NuHorasTrabajadas_S1=(select sum(NuHorasTrabajadas) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),
 NuHorasTrabajadas_S2=(select sum(NuHorasTrabajadas) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),
 NuHorasTrabajadas_S3=(select sum(NuHorasTrabajadas) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),
 NuHorasTrabajadas_S4=(select sum(NuHorasTrabajadas) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4),
 
 --NuHorasTrabajadas2,
-- NuHorasExtra=case when (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))<0 then 0 else  (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600)) end,
 
 NuHorasExtra_S1=(select sum(NuHorasExtra) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),
 NuHorasExtra_S2=(select sum(NuHorasExtra) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),
 NuHorasExtra_S3=(select sum(NuHorasExtra) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),
 NuHorasExtra_S4=(select sum(NuHorasExtra) from #reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4)
 --NuHorasExtra2
 from #reporte3 r3

drop table #reporte1
drop table #reporte2
drop table #reporte3

END;
