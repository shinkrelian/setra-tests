﻿--HLF-20151103-@IDServicio char(8)
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_Sel_Pk
@IDServicio char(8)
As
Set NoCount On
select ste.IDServicio,s.Descripcion as Servicio,
		--sd.Tipo as Variante,
		ste.QtStkEnt ,--sd.Anio,
	   ste.QtEntCompradas
from STOCK_TICKET_ENTRADAS ste 
	--Left Join MASERVICIOS_DET sd On ste.IDServicio_Det=sd.IDServicio_Det
	Left Join MASERVICIOS s On ste.IDServicio=s.IDServicio
Where ste.IDServicio=@IDServicio
