﻿--JRF-20151118-Agregar campo activo
--JRF-20160413-..Order By vt.Dia asc,vt.Item asc
CREATE Procedure dbo.VIATICOS_TOURCONDUCTOR_SelxIDCab
@IDCab int
As
Set NoCount On
Select vt.NuViaticoTC as NuViaticos,convert(char(10),vt.Dia,103) as Dia,vt.IDServicio_Det as IDServicio_Det,vt.Item as Conteo,
ttc.NoDescripcion as Item,vt.CostoUSD,vt.CostoSOL,vt.FlActivo
from VIATICOS_TOURCONDUCTOR vt 
Left Join MASERVICIOS_DET sd On vt.IDServicio_Det=sd.IDServicio_Det
Left Join MATIPOCOSTOSTC ttc On sd.CoTipoCostoTC=ttc.CoTipoCostoTC
Where IDCab=@IDCab
Order By vt.Dia asc,vt.Item asc
