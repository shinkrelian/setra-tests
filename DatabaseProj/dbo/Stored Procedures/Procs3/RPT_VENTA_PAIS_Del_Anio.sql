﻿CREATE PROCEDURE [RPT_VENTA_PAIS_Del_Anio]
	@NuAnio char(4)
AS
BEGIN
	Delete [dbo].[RPT_VENTA_PAIS]
	Where 
		[NuAnio] = @NuAnio
END
