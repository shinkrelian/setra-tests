﻿

--HLF-20160108-And NuTckEntIng=(Select Min(NuTckEntIng) ...
--HLF-20160114-comentando ((QtStSaldo<QttkComprado or QtStSaldo=QttkComprado) 
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_INGRESOS_SelNroDocOperSustento
	@IDServicio char(8)
As
	Set Nocount on
		
	Select isnull(NroDocSus,'') as NroDocSus, isnull(IDTipoDocSus,'') as IDTipoDocSus, isnull(NuOperBan ,'') as NuOperBan
	From STOCK_TICKET_ENTRADAS_INGRESOS Where IDServicio=@IDServicio 
		--And NuTckEntIng=(Select Max(NuTckEntIng) from STOCK_TICKET_ENTRADAS_INGRESOS where IDServicio=@IDServicio)		
		And NuTckEntIng=(Select Min(NuTckEntIng) from STOCK_TICKET_ENTRADAS_INGRESOS where IDServicio=@IDServicio
			--And  ((QtStSaldo<QttkComprado or QtStSaldo=QttkComprado) 
			And QtStSaldo>0
			--) 
		)

