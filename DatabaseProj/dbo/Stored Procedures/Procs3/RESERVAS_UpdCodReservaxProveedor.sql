﻿Create Procedure dbo.RESERVAS_UpdCodReservaxProveedor
	@IDCab	int,
	@IDProveedor char(6),
	@CodReservaProv varchar(50),
	@Observaciones varchar(max),
	@UserMod char(4)
As 
	Set Nocount On  

	Update RESERVAS 
	Set CodReservaProv=
	Case When ltrim(rtrim(@CodReservaProv))='' Then Null Else @CodReservaProv End,
	Observaciones = 
	Case When ltrim(rtrim(@Observaciones))='' Then Null Else @Observaciones End,
	FecMod=GETDATE(), UserMod=@UserMod	
	Where IDCab=@IDCab And IDProveedor=@IDProveedor 
	And Anulado=0
	
