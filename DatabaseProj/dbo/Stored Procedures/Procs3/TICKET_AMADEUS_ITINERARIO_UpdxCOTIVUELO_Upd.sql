﻿Create Procedure dbo.TICKET_AMADEUS_ITINERARIO_UpdxCOTIVUELO_Upd  
@CoTicket char(13),
@NuSegmento tinyint,
@CoLineaAerea char(2),  
@NuVuelo char(4),
@CodReserva varchar(20),
@TxFechaPar smalldatetime,  
@TxHoraLle smalldatetime,  
@UserMod char(4)  
As  
Set NoCount On  
UPDATE [TICKET_AMADEUS_ITINERARIO]  
   SET [CoLineaAerea] = @CoLineaAerea,  
       [NuVuelo] = @NuVuelo,   
       [TxFechaPar] = @TxFechaPar,   
       [TxHoraLle] = CAST(CONVERT(char(10),[TICKET_AMADEUS_ITINERARIO].TxHoraLle,103) +' '
				     +CONVERT(char(5),@TxHoraLle,108) As Smalldatetime),
	   [CoPNR_Aerolinea]=@CodReserva,
       [UserMod] = @UserMod,   
       [FecMod] = GetDate()  
 WHERE [CoTicket] = @CoTicket and [NuSegmento] = @NuSegmento  
