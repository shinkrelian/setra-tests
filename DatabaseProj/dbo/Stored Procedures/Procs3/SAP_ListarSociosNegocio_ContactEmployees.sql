﻿--JRF-20150810-Name = Campo Primario en SAP
--JHD-20150813-Name = select distinct Name,FirstName,LastName,Address,E_mail,Active,CardCode,
--(select top 1 IDSocioNegocio from @tmpDatosContactos) as IDSocioNegocio from @tmpDatosContactos 
--JRF-20150827-Considerar Name
--JRF-20151020-Agregar el @TipoSocio
CREATE Procedure dbo.SAP_ListarSociosNegocio_ContactEmployees
@IDSocioNegocio varchar(15),
@TipoSocio varchar(1)=''
As
Begin
Set NoCount On
Declare @tmpDatosContactos table (Name varchar(Max),FirstName varchar(Max),LastName varchar(Max),Address varchar(max),
								  E_mail varchar(Max),Active smallint,CardCode varchar(15),IDSocioNegocio varchar(15),TipoSocio char(1))

Insert into @tmpDatosContactos
Select distinct Y.Name+' '+Y.LastName,Y.FirstName,Y.LastName,Y.Address,Y.E_mail,Y.Active,Y.CardCode,y.IDSocioNeg,Y.TipoSocioNegocio
from (
	Select Ltrim(Rtrim(X.Name)) as Name,Ltrim(Rtrim(X.FirstName)) as FirstName,
		   Ltrim(Rtrim(X.LastName)) as LastName,X.Address,X.E_mail,X.Active,
		   Case When X.IDSocioNegocioSAP is not null then  X.IDSocioNegocioSAP Else 
		   X.IdSNFirstLetter + REPLICATE('0',12-(Len(X.IdSNFirstLetter)+Len(X.IdSNCode)))+X.IdSNCode End
		   As CardCode,X.IDSocioNeg,X.TipoSocioNegocio
	from (
		SELECT Nombres as Name,dbo.FnDevuelveFirstOrLastName(Nombres,1) as FirstName,Apellidos as LastName,
			   IsNull(Email,'') as E_mail,'' as Address,1 as Active,
			   p.CardCodeSAP As IDSocioNegocioSAP,
			   case when u1.IDPais != '000323' Then 'PEX' Else 'P' End IdSNFirstLetter,
			   Ltrim(rtrim(
				Case When IsNull(u1.IDPais,'') != '000323' then p.IDProveedor Else
						Cast(Case when p.IDIdentidad IN(001,002,003) And Ltrim(Rtrim(p.NumIdentidad))<>'' Then p.NumIdentidad else 
						Case When p.IDIdentidad IN(000,004,005) Then p.IDProveedor else '' End End as varchar(15))
				End))
				as IdSNCode,
				Case When Ltrim(Rtrim(IsNull(p.NumIdentidad,''))) = ''  Or Ltrim(Rtrim(IsNull(p.NumIdentidad,'')))='-' Then 0 Else
				ROW_NUMBER()Over(Partition by Ltrim(Rtrim(p.NumIdentidad)) Order By p.IDProveedor) 
				End as CorrelativoDoc,cp.IDProveedor As IDSocioNeg,'P' as TipoSocioNegocio
		FROM MACONTACTOSPROVEEDOR cp Inner Join MAPROVEEDORES p On cp.IDProveedor=p.IDProveedor
		left join MAUBIGEO u1 on p.IDCiudad = u1.IDubigeo
		where p.Activo='A'
		Union
		SELECT Nombres as Name,dbo.FnDevuelveFirstOrLastName(Nombres,1) as FirstName,Apellidos as LastName,
			   IsNull(Email,'') as E_mail,'' as Address,1 as Active,
			   c.CardCodeSAP as IDSocioNegocioSAP,
			   case when c.IDCiudad != '000323' Then 'CEX' Else 'C' End IdSNFirstLetter,
			   Ltrim(rtrim(
				Case When IsNull(c.IDCiudad,'') != '000323' then c.IDCliente Else
				Cast(Case when c.IDIdentidad IN(001,002,003) Then IsNull(c.NumIdentidad,c.IDCliente) else 
						Case When c.IDIdentidad IN(000,004,005) Then c.IDCliente else '' End End as varchar(15))
				End))
				as IdSNCode,
				Case When Ltrim(Rtrim(IsNull(c.NumIdentidad,''))) = ''  Or Ltrim(Rtrim(IsNull(c.NumIdentidad,'')))='-' Then 0 Else
				ROW_NUMBER()Over(Partition by c.NumIdentidad Order By c.IDCliente) End as CorrelativoDo,cc.IDCliente As IDSocioNeg,
				'C' as TipoSocioNegocio
		FROM MACONTACTOSCLIENTE cc Inner Join MACLIENTES c On cc.IDCliente=c.IDCliente
			 left join MAUBIGEO u1 on c.IDCiudad = u1.IDubigeo
		where c.Activo='A'
	) as X
	Where Len(CorrelativoDoc)<=2 
	--And ((Ltrim(Rtrim(@TipoSocio))='' And Ltrim(Rtrim(TipoSocioNegocio))='') Or TipoSocioNegocio=@TipoSocio)
) as Y
Where Y.CardCode = '' --(@TipoSocio = 'P' And Y.TipoSocioNegocio <> 'P')

--select distinct * from @tmpDatosContactos 
select distinct Name,FirstName,LastName,Address,E_mail,Active,CardCode,IDSocioNegocio 
from @tmpDatosContactos 
--where CHARINDEX(CardCode,@IDSocioNegocio)>0 
where IDSocioNegocio = @IDSocioNegocio
And (Ltrim(rtrim(@TipoSocio))='' Or TipoSocio=@TipoSocio)
--Or (CHARINDEX(@IDSocioNegocio,'CEX') > 0 And 
--And IDSocioNegocio=SUBSTRING(@IDSocioNegocio,7,6)
--)
Order By 1
End
