﻿Create Procedure dbo.TICKET_AMADEUS_UpdxCOTIVUELO_Upd
@CoTicket char(13),
@NoLineaAerea varchar(50),
@CoLineaAereaSigla char(3),
@CoPNR_AerorLn varchar(20),
@TxTotal numeric(10,2),
@UserMod char(4)
as
Set NoCount On
Update TICKET_AMADEUS
	Set CoLineaAereaSigla=@CoLineaAereaSigla,
		NoLineaAerea=@NoLineaAerea,
		TxTotal=@TxTotal,
		CoPNR_Aerolinea=@CoPNR_AerorLn,
		UserMod=@UserMod,
		FecMod=GETDATE()
where CoTicket=@CoTicket
