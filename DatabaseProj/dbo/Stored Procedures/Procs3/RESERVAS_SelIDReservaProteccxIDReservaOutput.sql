﻿Create Procedure dbo.RESERVAS_SelIDReservaProteccxIDReservaOutput
	@IDCab int,
	@IDReserva int,
	@pEsProtegido bit output
As
	Set Nocount On
	--(si soy padre de alguna(s) reserva)
	Set @pEsProtegido = 0
	
	If Exists(Select IDReserva From RESERVAS 
		Where IDCab=@IDCab And IDReservaProtecc=@IDReserva)
		Set @pEsProtegido = 1
		
