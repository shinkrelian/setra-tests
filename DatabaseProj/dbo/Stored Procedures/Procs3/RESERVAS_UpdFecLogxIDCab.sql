﻿--JRF-20160416-...DATEADD(SS,120,GETDATE())
CREATE Procedure dbo.RESERVAS_UpdFecLogxIDCab
@IDCab int,
@UserMod char(4)
as
Set NoCount On
Declare @TimeOut datetime = GETDATE()
Update RESERVAS_DET
set FeUpdateRow=DATEADD(SS,120,@TimeOut),
	UserMod=@UserMod,
	FecMod=@TimeOut
From RESERVAS_DET rd Inner Join RESERVAS r on rd.IDReserva=r.IDReserva
where rd.Anulado=0 and r.Anulado=0 and r.IDCab=@IDCab
