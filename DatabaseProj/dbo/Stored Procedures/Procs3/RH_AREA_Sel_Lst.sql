﻿
---------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[RH_AREA_Sel_Lst]
	@CoArea tinyint=0,
	@NoArea varchar(150)='',
	@FlActivo bit=0
AS
BEGIN
	Select
		[CoArea],
 		[NoArea],
 		[FlActivo]--,
 		--[UserMod],
 		--[FecMod]
 	From [dbo].[RH_AREA]
	Where 
		(@CoArea=0 or [CoArea] = @CoArea)
		and
		 (FlActivo = @FlActivo or @FlActivo =0) AND
		(ltrim(rtrim(@NoArea))='' Or NoArea like '%' +@NoArea + '%')   
END;
