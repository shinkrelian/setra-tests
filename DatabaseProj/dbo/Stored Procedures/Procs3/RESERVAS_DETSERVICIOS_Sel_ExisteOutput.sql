﻿Create Procedure [dbo].[RESERVAS_DETSERVICIOS_Sel_ExisteOutput]
	@IDReserva_Det int,
	@IDServicio_Det	int, 
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDReserva_Det From RESERVAS_DETSERVICIOS
		WHERE IDReserva_Det=@IDReserva_Det	And IDServicio_Det=@IDServicio_Det)
		Set @pExiste=1
	Else
		Set @pExiste=0
