﻿--select GETDATE()
--go
--TICKET_AMADEUS_Rpt_IATA '01/01/2015','18/02/2015',''
--TICKET_AMADEUS_Rpt_IATA '01/01/1900','01/01/1900','20140602W'
--ALTER
--JHD-20150218-Se agrego una 2da tabla que devuelve los documentos BSP
CREATE PROCEDURE TICKET_AMADEUS_Rpt_IATA
@fecha1 as smalldatetime='01/01/1900',--'01/06/2014',
@fecha2 as smalldatetime='01/01/1900',--'01/07/2014',
@CoPeriodo char(9)=''
AS
BEGIN
select 
TxFechaEmision,
substring(t.CoTicket,1,3) as NuSerieTicket,
right(t.CoTicket,10) as NuTicket,
t.CoLineaAereaSigla,
isnull(dbo.FnRutaItinetarioTicket(t.CoTicket),'') as Ruta,
TxTarifaAerea,
IGV=isnull((select SUM(ssmonto) from TICKET_AMADEUS_TAX where CoTax IN ('PE AD','PE') and  CoTicket=t.CoTicket),0),
Otros=isnull((select SUM(ssmonto) from TICKET_AMADEUS_TAX where CoTax NOT IN ('PE AD','PE') and  CoTicket=t.CoTicket),0),
TxTotal,
--aa=(1+(select top 1 NuIGV from PARAMETRO)/100),
Comision_Sub=cast(round((ssComision/(1+(select top 1 NuIGV from PARAMETRO)/100)),2) as numeric(6,2)),
IGV_Comision=ssComision-cast(round((ssComision/(1+(select top 1 NuIGV from PARAMETRO)/100)),2) as numeric(6,2)),
SsComision,
SsCosto,
t.NoPax,
File_NV=ISNULL( case when t.IDCab IS null then
		 (select top 1 NuNtaVta from NOTAVENTA_COUNTER_DETALLE where NuDocumento=t.CoTicket)
		 else (select idfile from COTICAB where IDCab=t.IDCab)
		 end,''),
Grupo=ISNULL(case when t.IDCab IS not null then
		 (select titulo from COTICAB where IDCab=t.IDCab)
		 else ''
		 end,''),
Cuenta=ISNULL(case when t.IDCab IS not null then
		 (
			select top 1 Isnull(cl.RazonComercial,'')
			from TICKET_AMADEUS ti inner Join COTICAB c On ti.IDCab = c.IDCAB    
			inner Join MACLIENTES cl On Isnull(ti.IDCliente,c.IDCliente) = cl.IDCliente    
			and c.IDCAB=t.IDCab
		 )
		 else ''
		 end,''),
t.CoFormaPago,
--Observaciones=isnull(TxObsCou,isnull((select Observaciones from coticab where idcab=t.idcab),''))
Observaciones=isnull(TxObsCou,'')--isnull((select Observaciones from coticab where idcab=t.idcab),''))
from TICKET_AMADEUS t
where CoProvOrigen='IATA'
and (@fecha1 ='01/01/1900' or (t.TxFechaEmision>@fecha1 and t.TxFechaEmision<@fecha2+1))
and (ltrim(rtrim(@CoPeriodo))='' or ((t.TxFechaEmision>(select FeIniPer from CALENDARIO_IATA where CoPeriodo=@CoPeriodo)) and (t.TxFechaEmision<(select FeFinPer+1 from CALENDARIO_IATA where CoPeriodo=@CoPeriodo)) ))
ORDER BY T.TxFechaEmision

----------------------- DOCUMENTOS BSP
SELECT    TICKET_AMADEUS.CoLineaAereaSigla,
		  --DOCUMENTOBSP.NuDocumBSP,
		  substring(DOCUMENTOBSP.NuDocumBSP,1,3)+' - '+right(DOCUMENTOBSP.NuDocumBSP,8) as NuDocumBSP,
		  SSTotalDocum=case when CoTipDocBSP IN ('ACM','RA') THEN DOCUMENTOBSP.SSTotalDocum*-1 when CoTipDocBSP IN ('ADM','EMD') then DOCUMENTOBSP.SSTotalDocum else DOCUMENTOBSP.SSTotalDocum end
FROM         TICKET_AMADEUS INNER JOIN
                      DOCUMENTOBSP ON TICKET_AMADEUS.CoTicket = DOCUMENTOBSP.CoTicket
WHERE     ((@fecha1 ='01/01/1900' or (FeEmision>@fecha1 and FeEmision<@fecha2+1))) 
and (ltrim(rtrim(@CoPeriodo))='' or CoPeriodo=@CoPeriodo )
AND TICKET_AMADEUS.CoProvOrigen='IATA'
END

