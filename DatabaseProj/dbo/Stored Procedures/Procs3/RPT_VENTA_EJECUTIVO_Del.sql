﻿
----------------------------------------------------------------------------------------------

--CREATE
CREATE PROCEDURE RPT_VENTA_EJECUTIVO_Del
	@NuAnio char(4)='',
	@CoUserEjecutivo char(4)=''
AS
BEGIN
	Delete dbo.RPT_VENTA_EJECUTIVO
	Where 
		NuAnio = @NuAnio And 
		CoUserEjecutivo = @CoUserEjecutivo
END;
