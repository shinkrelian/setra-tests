﻿Create Procedure dbo.TICKET_AMADEUS_TARIFA_Del
 @CoTicket char(13),
 @NuTarifa tinyint
As
	Set NoCount On
	Delete from TICKET_AMADEUS_TARIFA
	where CoTicket = @CoTicket and NuTarifa = @NuTarifa
