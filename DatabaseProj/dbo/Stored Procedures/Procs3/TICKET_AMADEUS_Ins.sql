﻿


--HLF-20140623-Agregando parametro @CoPNR_Amadeus
--Agregando parametro @CoTicketOrig
--HLF-20140625-Agregando parametro @CoEjeCouAmd, @CoEjeCou, @CoEjeRec, @TxObsCou,@TxTituloPax, @CoNacionalidad
--HLF-20140627-@CoDocumIdent char(2)
--HLF-20140630-@CoFormaPago char(11)
--HLF-20140704-@CoTicket char(13), @TxComision numeric(5,2),
--SsComision   (@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
--SsCosto @TxTotal-(@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
--HLF-20140707-Agregando parametro @CoPNR_Aerolinea 
--JHD-20140911-Agregado campo NuNtaVta
--JHD-20141031-Se corrigio el parametro @CoPNR_Aerolinea para que admita 9 caracteres (char(9))
CREATE Procedure dbo.TICKET_AMADEUS_Ins  
	@CoTicket char(13),  
	@NoArchivo varchar(20),  
	@CoAir char(14),  
	@CoProvOrigen char(6),  
	@CoUsrCrea char(9),  
	@CoUsrDuenio char(9),  
	@CoUsrEmite char(9),  
	@CoIATA char(8),  
	@CoLineaAereaRecord char(9),  
	@NoLineaAerea varchar(50),  
	@CoLineaAereaEmisora char(7),  
	@CoLineaAereaSigla char(3),  
	@CoTransacEmision char(8),  
	@CoLineaAereaServicio char(4),  
	@TxFechaCreacion smalldatetime,  
	@TxFechaModificacion smalldatetime,  
	@TxFechaEmision smalldatetime,  
	@TxIndicaVenta char(1),  
	@CoPtoVentayEmision char(3),  
	@TxTarifaAerea numeric(10,2),  
	@TxTotal numeric(10,2),  
	@NoPax varchar(50),  
	@TxRestricciones varchar(250),  
	@CoFormaPago char(11),  
	@CoDocumIdent char(2),  
	@NuDocumIdent varchar(15),  
	--@TxComision char(4),  
	@TxComision numeric(5,2),  
	@TxTasaDY_AE numeric(6,2),  
	@TxTasaHW_DE numeric(6,2),  
	@TxTasaXC_DP numeric(6,2),  
	@TxTasaXB_TI numeric(6,2),  
	@TxTasaQQ_TO numeric(6,2),  
	@TxTasaAH_SE numeric(6,2),  

	@IDCab int,  
	@IDCliente char(6),  
	@CoPNR_Amadeus   char(6),  
	@CoTicketOrig char(14),
	@CoEjeCouAmd char(8),
	@CoEjeCou char(4),
	@CoEjeRec char(4),
	@TxObsCou varchar(max),
	@TxTituloPax varchar(150),
	@CoNacionalidad char(6),
	@CoPNR_Aerolinea char(9),
	@NuNtaVta CHAR(6),
	@UserMod char(4)  
As  
	Set NoCount On  
	Declare @Igv numeric(5,2)=(Select NuIGV From PARAMETRO)
	
	INSERT INTO [TICKET_AMADEUS]  
	([CoTicket]  
	,NoArchivo  
	,[CoAir]  
	,CoProvOrigen  
	,[CoUsrCrea]  
	,[CoUsrDuenio]  
	,[CoUsrEmite]  
	,[CoIATA]  
	,[CoLineaAereaRecord]  
	,CoLineaAereaSigla  
	,[NoLineaAerea]  
	,[CoLineaAereaEmisora]  
	,[CoTransacEmision]  
	,[CoLineaAereaServicio]  
	,[TxFechaCreacion]  
	,[TxFechaModificacion]  
	,[TxFechaEmision]  
	,[TxIndicaVenta]  
	,[CoPtoVentayEmision]  
	,[TxTarifaAerea]  
	,[TxTotal]  
	,[NoPax]  
	,[TxRestricciones]  
	,[CoFormaPago]  
	,CoDocumIdent  
	,[NuDocumIdent]  
	,[TxComision]  
	,[TxTasaDY_AE]  
	,[TxTasaHW_DE]  
	,[TxTasaXC_DP]  
	,[TxTasaXB_TI]  
	,[TxTasaQQ_TO]  
	,[TxTasaAH_SE]  
	,IDCab  
	,IDCliente   
	,CoPNR_Amadeus
	,CoPNR_Aerolinea
	,CoTicketOrig
	,CoEjeCouAmd
	,CoEjeCou
	,CoEjeRec
	,TxObsCou
	,TxTituloPax
	,CoNacionalidad
	,SsComision
	,SsCosto
	,[UserMod]  
	,[FecMod]  
	,NuNtaVta) 
	VALUES  
	(@CoTicket,   
	Case When LTRIM(rtrim(@NoArchivo))='' then Null else @NoArchivo End,   
	@CoAir,   
	@CoProvOrigen,  
	@CoUsrCrea,   
	@CoUsrDuenio,  
	@CoUsrEmite,   
	@CoIATA,   
	@CoLineaAereaRecord,   
	@CoLineaAereaSigla,  
	@NoLineaAerea,   
	@CoLineaAereaEmisora,  
	@CoTransacEmision,  
	@CoLineaAereaServicio,  
	@TxFechaCreacion,   
	@TxFechaModificacion,  
	@TxFechaEmision,   
	@TxIndicaVenta,  
	@CoPtoVentayEmision,  
	@TxTarifaAerea,   
	@TxTotal,   
	@NoPax,   
	Case When LTRIM(rtrim(@TxRestricciones))='' then Null else @TxRestricciones End,   
	@CoFormaPago,   
	@CoDocumIdent,  
	@NuDocumIdent,  
	@TxComision,  
	Case when @TxTasaDY_AE=0 Then Null else @TxTasaDY_AE End,   
	Case When @TxTasaHW_DE=0 Then Null else @TxTasaHW_DE End,   
	Case When @TxTasaXC_DP=0 Then Null else @TxTasaXC_DP End,   
	Case When @TxTasaXB_TI=0 Then Null else @TxTasaXB_TI End,   
	Case When @TxTasaQQ_TO=0 Then Null else @TxTasaQQ_TO End,   
	Case When @TxTasaAH_SE=0 Then Null else @TxTasaAH_SE End,   
	Case When @IDCab=0 Then Null Else @IDCab End,  
	Case When ltrim(rtrim(@IDCliente))='' Then Null Else @IDCliente End,  
	@CoPNR_Amadeus,
	@CoPNR_Aerolinea,
	Case When ltrim(rtrim(@CoTicketOrig))='' Then Null Else @CoTicketOrig End,  
	Case When ltrim(rtrim(@CoEjeCouAmd))='' Then Null Else @CoEjeCouAmd End,  
	@CoEjeCou,
	Case When ltrim(rtrim(@CoEjeRec))='' Then Null Else @CoEjeRec End,  
	Case When ltrim(rtrim(@TxObsCou))='' Then Null Else @TxObsCou End,  
	Case When ltrim(rtrim(@TxTituloPax))='' Then Null Else @TxTituloPax End,  
	Case When ltrim(rtrim(@CoNacionalidad))='' Then Null Else @CoNacionalidad End,  
	(@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
	@TxTotal-(@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
	@UserMod,     
	GetDate()
	,case when ltrim(rtrim(@NuNtaVta))='' then null else @NuNtaVta end    )  



