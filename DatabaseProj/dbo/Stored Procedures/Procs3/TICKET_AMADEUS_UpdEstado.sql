﻿CREATE Procedure dbo.TICKET_AMADEUS_UpdEstado
	@CoTicket char(13),
	@CoEstado char(4),
	@UserMod char(4)
 As
	SET NOCOUNT ON    
	
	Update TICKET_AMADEUS Set CoEstado=@CoEstado, FecMod=GETDATE(), UserMod=@UserMod
	Where CoTicket=@CoTicket

