﻿--JHD-20150812- SubjectToWithholdingTax= case when (select count(*) from MAPROVEEDORES_MATIPODETRACCION pd where pd.IDProveedor=p.IDProveedor) >0 then 1 else 0 end,
--JRF-20150814-Considerar al personal extranjero como no domicilado por el Passaporte
--JRF-20150817-Considerar espacios en blanco
--JRF-20150909-And p.EsCadena = 0..
--JRF-20151002-Case When p.IDTipoProv='005' Or p.IDTipoProv='017' Or p.IDTipoProv='020'... PARA PROVEEDORES
--JRF-20151006-Case When p.IDTipoProv='005' Or p.IDTipoProv='017' Or p.IDTipoProv='020'... PARA PROVEEDORES [Deshacer]
--JRF-20151007-los campos de Ident y u_syp_bptd para guia deben ser RUC [Guías, Trasladistas Lima y Cusco]
--JRF-20151107-Cambiar [p.NombreCorto] x [p.RazonSocial]
--JHD-20160222-Where  p.FlExcluirSAP = 0 
--JHD-20160222-Where u.FlTrabajador_Activo=1 and u.FlExcluirSAP = 0
CREATE Procedure [dbo].[SAP_ListarSociosNegocio]
@IDSocioNegocio varchar(10),
@TipoSocioNegocio char(1)
As 

--select * from MAUBIGEO where Descripcion='PERU'
--select * from MACLIENTES where FlExcluirSAP=0 And Activo='A' And IDCiudad='000323'

--Delete from SocioNegocioExcel1$

--Insert into SocioNegocioExcel1$
--SELECT Z.CardCode,Z.CardName,Z.CardType,Z.GroupCode,Z.Phone1,Z.Phone2,'' as Fax,'' as ContactPerson,'' as Notes,Z.PayTermsGrpCode as PayTermsGrpCode,Z.FederalTaxID,
--	   '' As [FreeText],'' As SalesPersonCode,Z.Currency,'' as Celular,z.EmailAddress,'' As DefaultAccount,'' As DefaultBankCode,Z.DebitoAccount As DebitoAccount,z.Valid,
--	   '' as ValidFrom,'' ValidTo,'' as ValidRemarks,'' As Frozen,'' As FrozenFrom,'' As FrozenTo,'' as ForzenRemarks,'' As BankCountry,Z.SubjectToWithholdingTax,
--	   '' As DownPaymentsClearAct,Z.U_SYP_BPAP,z.U_SYP_BPAM,U_SYP_BPNO,U_SYP_BPN2,'' as U_SYP_BPAT,z.U_SYP_BPTP,z.U_SYP_BPTD,Z.IDSocioNegocio,z.data
SELECT *
From (
select Y.* From (
Select Case When X.IDSocioNegocioSAP is not null then  X.IDSocioNegocioSAP Else 
	   X.IdSNFirstLetter +
	   --REPLICATE('0',12-(Len(X.IdSNFirstLetter)+Len(X.IdSNCode)))+
		X.IdSNCode +
		Case When x.IdSNFirstLetter='E' then '' Else
		REPLICATE('0',2-Len(X.CorrelativoDoc)) + Cast(X.CorrelativoDoc as varchar(2)) 
		End
		End
	   As CardCode, 
	   X.CardName,
	   Case X.CardType
		when 'C' Then 0 
		when 'P' Then 1
		when 'S' then 1
	   End as CardType,dbo.FnDevuelvoGroupCode(X.CardType,X.IdSNFirstLetter) as GroupCode,X.CoSAPFormaPago,
	   Case When Charindex('EX',x.IdSNFirstLetter) > 0 
	   Then  X.IdSNFirstLetter + REPLICATE('0',11-(Len(X.IdSNFirstLetter)+Len(X.IdSNCode)))+X.IdSNCode Else 
				--IsNull(X.NumIdentidad,'')
			Case When x.data=2 then 
			 Case When (select IDTipoProv from MAPROVEEDORES where IDProveedor= X.IDSocioNegocio) In('005','017','020') then
					IsNull((select RUC from MAPROVEEDORES where IDProveedor= X.IDSocioNegocio) ,'') else IsNull(X.NumIdentidad,'') End
			 else IsNull(X.NumIdentidad,'') End
	   End as FederalTaxID,
	   X.Currency, Case When X.Domiciliado = 0  Then 'SND' else
	    --Case When X.data=2 Then
		--  Case When (select IDTipoProv from MAPROVEEDORES where IDProveedor= X.IDSocioNegocio) In ('005','017','020') then 'TPJ' Else X.TipoPersona End
		 --else X.TipoPersona End End As U_SYP_BPTP,
		 X.TipoPersona End as U_SYP_BPTP,X.U_SYP_BPTD,X.U_SYP_BPAP,X.U_SYP_BPAM,
	   dbo.FnDevuelveFirstOrLastName(X.NombreCompleto,0) As U_SYP_BPN2,dbo.FnDevuelveFirstOrLastName(X.NombreCompleto,1) As U_SYP_BPNO,
	   X.EmailAddress,X.Phone1,X.Phone2,X.SubjectToWithholdingTax,X.Valid,X.AddressName,X.Street,X.ZipCode,X.Block,X.AddressType,
	   X.CampoTipoProvRestOperTrans,X.ClienteValido,x.DebitoAccount,X.CoSAPFormaPago as PayTermsGrpCode,X.IDSocioNegocio,x.data
from (
SELECT
	case when c.IDCiudad != '000323' Then 
	 Case When IsNull(c.FlProveedor,0) = 1 then 'PEX' else 'CEX' End Else 
	 Case When IsNull(c.FlProveedor,0) = 1 then 'P' else 'C' End End IdSNFirstLetter,
	Ltrim(rtrim(
	Case When IsNull(c.IDCiudad,'') != '000323' then c.IDCliente Else
	Cast(Case when c.IDIdentidad IN(001,002,003) Then IsNull(c.NumIdentidad,c.IDCliente) else 
			Case When c.IDIdentidad IN(000,004,005) Then c.IDCliente else '' End End as varchar(15))
	End))
	as IdSNCode,
	c.RazonComercial as CardName, 'C' as CardType, 
	IsNull(c.NumIdentidad,'') as NumIdentidad,	'##' as Currency,
	c.Domiciliado as Domiciliado,
	'TP'+c.Persona as TipoPersona,
    --IsNull(idSap.CoSAP,IsNull(id.CoSAP,0)) as U_SYP_BPTD,
	IsNull(id.CoSAP,IsNull((select CoSAP from MATIPOIDENT tSap where tSap.IDIdentidad=id.IDIdentidadSAP),0)) as U_SYP_BPTD,
    Replace(IsNull(Ltrim(Rtrim(c.ApPaterno)),' '),'-',' ') as U_SYP_BPAP, Replace(IsNull(Ltrim(Rtrim(c.ApMaterno)),' '),'-',' ') as U_SYP_BPAM,
	IsNull(c.Nombre,'') as NombreCompleto,'' as EmailAddress,IsNull(c.Telefono1,'') as Phone1,IsNull(c.Telefono2,'') as Phone2,
	--SubjectToWithholdingTax= case when (select count(*) from MAPROVEEDORES_MATIPODETRACCION pd where pd.IDProveedor=p.IDProveedor) >0 then 1 else 0 end,
	SubjectToWithholdingTax= 0,
    Case When c.Activo='A' then 1 else 0 End as Valid,
	Cast(c.IDCliente as varchar(10)) as IDSocioNegocio,c.CardCodeSAP as IDSocioNegocioSAP,
	'Fac' As AddressName,IsNull(c.Direccion,'') as Street,'' as ZipCode,'' as Block,1 as AddressType,
	Case When (Ltrim(Rtrim(IsNull(c.NumIdentidad,''))) = '' Or Ltrim(Rtrim(IsNull(c.NumIdentidad,'')))='-') Or 
			  c.IDCiudad != '000323' 
	Then 0 Else ROW_NUMBER()Over(Partition by c.NumIdentidad Order By c.IDCliente) End as CorrelativoDoc,1 as data,c.IDCliente as CodigoReal,0 as CoSAPFormaPago,'tNo' as CampoTipoProvRestOperTrans,
	Case When c.Tipo In ('CL','CO','WB') then 1 Else 0 End as ClienteValido,
	Case When c.IDCliente In ('000054','001981') then '13121002' else
		Case When c.IDCliente In ('001589','001864') then '13125001' else '12122001' End End as DebitoAccount
FROM MACLIENTES c 
	left join MAUBIGEO u1 on c.IDCiudad = u1.IDubigeo 
	left join MATIPOIDENT id on c.IDIdentidad = id.IDIdentidad
where c.Activo='A' And c.FlExcluirSAP = 0 And c.IDCliente <> '000895'
--And c.NumIdentidad = c.CoStarSoft
And @TipoSocioNegocio = 'C'
Union
SELECT
	case when u1.IDPais != '000323' Then 'PEX' Else 'P' End IdSNFirstLetter,
	Ltrim(rtrim(
	Case When IsNull(u1.IDPais,'') != '000323' then p.IDProveedor Else
			Case When p.IDTipoProv In('005','017','020') then IsNull(p.RUC,'') Else
			Cast(Case when p.IDIdentidad IN(001,002,003) And Ltrim(Rtrim(p.NumIdentidad))<>'' Then p.NumIdentidad else 
			Case When p.IDIdentidad IN(000,004,005) Then p.IDProveedor else '' End End as varchar(15))
	End End))
	as IdSNCode,
	Case When p.Persona='N' And Ltrim(Rtrim(IsNull(p.RazonSocial,''))) = '' then p.Nombre+' '+p.ApPaterno+ ' '+ Replace(Replace(p.ApMaterno,'-',''),'?','')
		Else Case When p.Persona='J' And Replace(IsNull(p.RazonSocial,''),'-','') ='' then p.NombreCorto else p.RazonSocial End End as CardName, 'P' as CardType, 
	IsNull(p.NumIdentidad,'') as NumIdentidad
	--Case When p.IDTipoProv='005' Or p.IDTipoProv='017' Or p.IDTipoProv='020' Then 
	--IsNull(p.RUC,'') else IsNull(p.NumIdentidad,'') End as NumIdentidad
	,'##' as Currency,
	p.Domiciliado as Domiciliado,
	'TP'+p.Persona as TipoPersona,
    --IsNull(idSap.CoSAP,IsNull(id.CoSAP,0)) as U_SYP_BPTD,
	Case When p.IDTipoProv='005' Or p.IDTipoProv='017' Or p.IDTipoProv='020' Then 6 Else 
	IsNull(id.CoSAP,IsNull((select CoSAP from MATIPOIDENT tSap where tSap.IDIdentidad=id.IDIdentidadSAP),0)) End  as U_SYP_BPTD,
    Replace(IsNull(Ltrim(Rtrim(p.ApPaterno)),' '),'-',' ') as U_SYP_BPAP, Replace(IsNull(Ltrim(Rtrim(p.ApMaterno)),'' ),'-',' ') as U_SYP_BPAM,
	IsNull(p.Nombre,'') as NombreCompleto,IsNull(Email1,'') as EmailAddress,IsNull(p.Telefono1,'') as Phone1,IsNull(p.Telefono2,'') as Phone2, 
	--0 as SubjectToWithholdingTax,
	SubjectToWithholdingTax= case when (select count(*) from MAPROVEEDORES_MATIPODETRACCION pd where pd.IDProveedor=p.IDProveedor) >0 then 1 else 0 end,
    Case When p.Activo='A' then 1 else 0 End as Valid,
	Cast(p.IDProveedor as varchar(10)) as IDSocioNegocio,p.CardCodeSAP as IDSocioNegocioSAP,
	'Fac' As AddressName,IsNull(p.Direccion,'') as Street,'' as ZipCode,'' as Block,1 as AddressType,
	
	Case When Ltrim(Rtrim(p.NumIdentidad)) = ''  Or Ltrim(Rtrim(p.NumIdentidad))='-'  Or Ltrim(Rtrim(p.NumIdentidad))='0'
			  Or Not Exists(select IDProveedor from MAPROVEEDORES pInt where pInt.NumIdentidad=p.NumIdentidad)
	Then 0 Else
	ROW_NUMBER()Over(Partition by Ltrim(Rtrim(p.NumIdentidad)) Order By p.IDProveedor) End as CorrelativoDoc,2 as data,p.IDProveedor as CodigoReal,
	IsNull(fp.CoSap,0) as CoSAPFormaPago,
	Case When p.IDTipoProv In ('002','003','004') then 'tYes' else 'tNo' End as CampoTipoProvRestOperTrans,1 as ClienteValido,
	Case When p.IDProveedor In ('000467','000527') then '43125002' Else 
	 Case When p.IDProveedor In ('002315','002317') Then '43122002' else 
	 Case When p.IDTipoProv = '005' Then '42411001' Else '42122001' End End End as DebitoAccount 
	--,tp.Descripcion as TipoProv,u.Descripcion as Ciudad--,IsNull(P.NumIdentidad,'') as Ident
	--,id.Descripcion
FROM MAPROVEEDORES p
	left join MAUBIGEO u1 on p.IDCiudad = u1.IDubigeo
	left join MATIPOIDENT id on p.IDIdentidad = id.IDIdentidad
	left Join MATIPOPROVEEDOR tp On p.IDTipoProv=tp.IDTipoProv
	left Join MAUBIGEO u On p.IDCiudad = u.IDubigeo
	left Join MAFORMASPAGO fp On p.IDFormaPago=fp.IdFor
--Where p.Activo='A' And p.FlExcluirSAP = 0 
Where  p.FlExcluirSAP = 0 
--And Not p.IDProveedor In (select distinct IDProveedor_Cadena from MAPROVEEDORES Where IDProveedor_Cadena is not null)
And p.EsCadena = 0
And Not p.IDProveedor In ('000470','001829','002087')
--And Not p.IDIdentidad In ('000','005')
And @TipoSocioNegocio = 'P'
Union
SELECT
	'E' IdSNFirstLetter,
	Case When Not u.IDUbigeo In ('000065','000066') then u.IDUsuario else
	Ltrim(rtrim(IsNull(u.NumIdentidad,''))) End as IdSNCode,
	IsNull(u.TxApellidos,'')+' '+u.Nombre as CardName, 'S' as CardType, 
	IsNull(u.NumIdentidad,'') as NumIdentidad,	'##' as Currency,
	Case When Not u.IDUbigeo In ('000065','000066') Then 0 Else 1 End as Domiciliado,
    Case When U.CoTipoDoc = '003' Or U.CoTipoDoc='007' Then 'SND' Else 'TPN' End as TipoPersona,
    --IsNull(idSap.CoSAP,IsNull(id.CoSAP,0)) as U_SYP_BPTD,
	IsNull(id.CoSAP,IsNull((select CoSAP from MATIPOIDENT tSap where tSap.IDIdentidad=id.IDIdentidadSAP),0)) as U_SYP_BPTD,
    dbo.FnDevuelveFirstOrLastName(Replace(IsNull(u.TxApellidos,''),'-',''),1) as U_SYP_BPAP, dbo.FnDevuelveFirstOrLastName(Replace(IsNull(u.TxApellidos,''),'-',''),0) as U_SYP_BPAM,
	IsNull(u.Nombre,'') as NombreCompleto,IsNull(u.Correo,'') as EmailAddress,IsNull(u.Telefono,'') as Phone1,IsNull(u.Celular,'') as Phone2, 0 as SubjectToWithholdingTax,
    Case When u.Activo='A' then 1 else 0 End as Valid,
	Cast(u.IDUsuario as varchar(10)) as IDSocioNegocio,u.CardCodeSAP as IDSocioNegocioSAP,
	'Fac' As AddressName,IsNull(u.Direccion,'') as Street,'' as ZipCode,'' as Block,1 as AddressType,
	0 as CorrelativoDoc,3 as data,u.IDUsuario as CodigoReal,0 as CoSAPFormaPago,'tNo'as CampoTipoProvRestOperTrans,1 as ClienteValido,'14191009' as DebitoAccount
FROM MAUSUARIOS u
	left join MAUBIGEO u1 on u.IDUbigeo = u1.IDubigeo
	left join MATIPOIDENT id on u.CoTipoDoc = id.IDIdentidad
--Where u.Activo='A' and u.FlTrabajador_Activo=1 and u.FlExcluirSAP = 0
Where u.FlTrabajador_Activo=1 and u.FlExcluirSAP = 0
And @TipoSocioNegocio = 'U'
) as X)  as Y 
--Where Y.data=1 And charindex('ex',Y.CardCode)=0
--And charindex(Y.FederalTaxID,Y.CardCode)=0
) --cMig Inner Join MACLIENTES cl On SUBSTRING(cMig.CardCode,2,6) = cl.IDCliente
as Z 
--Where Z.data = 1 
--And charindex('ex',Z.CardCode)=0
--And charindex(Z.FederalTaxID,Z.CardCode)=0
--where LEN(Z.CardCode)=5
--where Z.DebitoAccount = '42411001'
--where z.CardCode = 'CEX000000054'
--WHERE CHARINDEX('HEDDY',Z.CardName)> 0
Where Z.IDSocioNegocio=@IDSocioNegocio
Order By z.CardCode
--where CHARINDEX('.',Z.FederalTaxID)>0
--select * from MASOCIOSNEGOCIO_TEST Where Len(CardCode) < 8
