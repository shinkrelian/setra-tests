﻿--HLF-20160218-@CoServicio a tipo de dato nvarchar(8) 
CREATE Procedure [dbo].[VENTA_ADICIONAL_DET_UpdDebitMemo]
	@NuVtaAdi int ,
	@CoServicio nvarchar(8),
	@NuPax int,
	@IDDebitMemo char(10),
	@UserMod char(4)
As
	Set Nocount On

	Update VENTA_ADICIONAL_DET Set IDDebitMemo=@IDDebitMemo,UserMod=@UserMod, FecMod=getdate() 
	Where NuVtaAdi=@NuVtaAdi And
		CoServicio=@CoServicio And	NuPax=@NuPax 

