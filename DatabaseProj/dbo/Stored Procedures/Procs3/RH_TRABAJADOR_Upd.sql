﻿

------------------------------------------------------------------------

CREATE PROCEDURE [RH_TRABAJADOR_Upd]
	@CoTrabajador char(11),
	@NoApPaterno varchar(50)='',
	@NoApMaterno varchar(50)='',
	@NoNombres varchar(100)='',
	@CoTipoDoc char(3)='',
	@NuNumeroDoc char(11)='',
	@UserMod char(4)=''
AS
BEGIN
	Update [dbo].[RH_TRABAJADOR]
	Set
		[NoApPaterno] = Case When ltrim(rtrim(@NoApPaterno))='' Then Null Else @NoApPaterno End,
 		[NoApMaterno] = Case When ltrim(rtrim(@NoApMaterno))='' Then Null Else @NoApMaterno End,
 		[NoNombres] = Case When ltrim(rtrim(@NoNombres))='' Then Null Else @NoNombres End,
 		[CoTipoDoc] = Case When ltrim(rtrim(@CoTipoDoc))='' Then Null Else @CoTipoDoc End,
 		[NuNumeroDoc] = Case When ltrim(rtrim(@NuNumeroDoc))='' Then Null Else @NuNumeroDoc End,
 		[UserMod] = Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End
 	Where 
		[CoTrabajador] = @CoTrabajador
END
