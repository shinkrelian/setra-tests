﻿Create Procedure dbo.RESERVAS_ExisteDifDetallePax
@IDReserva int,
@pExisteCambio bit output
As
Set NoCount On
Set @pExisteCambio=0
Set @pExisteCambio = dbo.FnDevuelveCambiosVentasPostReservasDetaPax(@IDReserva)
