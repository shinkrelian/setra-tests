﻿--HLF-20151103-@IDServicio_Det_V_Cot int
--IDServicio=(select IDServicio from MASERVICIOS_DET ...
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_SelOutput  
--@IDServicio_Det int,  
@IDServicio_Det_V_Cot int,
@pQtStkEnt smallint output  
As  
Set Nocount on  
  
Set @pQtStkEnt=0  
   
Select @pQtStkEnt=QtStkEnt From STOCK_TICKET_ENTRADAS  
Where IDServicio=(select IDServicio from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det_V_Cot)

