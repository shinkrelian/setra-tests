﻿

-----------------------------------------------------------------------


CREATE PROCEDURE [RH_AREA_Ins]
	
	@NoArea varchar(150)='',
	@UserMod char(4)=''
AS
BEGIN
	declare @CoArea tinyint
	set @CoArea=isnull((select MAX(coarea) from RH_AREA),0)+1
	
	Insert Into [dbo].[RH_AREA]
		(
			[CoArea],
 			[NoArea],
 			[UserMod]
 		)
	Values
		(
			@CoArea,
 			Case When ltrim(rtrim(@NoArea))='' Then Null Else @NoArea End,
 			Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End
 		)
END;
