﻿CREATE Procedure dbo.SOCIONEGOCIOSAP_SelxCardCodeSAP
@CardCodeSAP varchar(15),
@IDSocioNegocio varchar(6) output,
@TipoSocio char(1) output
As
Begin
Set NoCount On
Set @IDSocioNegocio = ''
Set @TipoSocio = ''
Select @IDSocioNegocio = IDProveedor,@TipoSocio='P'
from MAPROVEEDORES where Ltrim(Rtrim(CardCodeSAP))=Ltrim(Rtrim(@CardCodeSAP))

Select @IDSocioNegocio = IDCliente,@TipoSocio='C'
from MACLIENTES where Ltrim(Rtrim(CardCodeSAP))=Ltrim(Rtrim(@CardCodeSAP))
And @IDSocioNegocio = ''

Select @IDSocioNegocio = IDUsuario,@TipoSocio = 'U'
from MAUSUARIOS where Ltrim(Rtrim(CardCodeSAP))=Ltrim(Rtrim(@CardCodeSAP))
And @IDSocioNegocio = ''
End
