﻿--JRF-20160401-Validar Existencia
CREATE Procedure dbo.VOUCHER_HISTORICO_Ins
@IDVoucher int,
@IDOperacion_Det int,
@UserMod char(4)
As
Set NoCount On
If Not Exists (select IDVoucher from VOUCHER_HISTORICO Where IDVoucher=@IDVoucher And IDOperacion_Det=@IDOperacion_Det)
Begin
Insert into VOUCHER_HISTORICO
		(IDVoucher,
		 IDOperacion_Det,
		 UserMod,
		 FecMod)
	   values (@IDVoucher,
			   @IDOperacion_Det,
			   @UserMod,
			   Getdate())
End
