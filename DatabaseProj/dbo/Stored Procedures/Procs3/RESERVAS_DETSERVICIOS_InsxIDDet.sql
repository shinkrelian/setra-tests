﻿--HLF-20130829-Agregando CAST(od.IDReserva_Det as varchar(20)) + ' '+ CAST(rs.IDServicio_Det as varchar(20))   
 --Not In ....  
--HLF-20141013-Case When od.IDMoneda='USD' Then ...
--Left Join COTICAB_TIPOSCAMBIO tc On tc.IDCab=@IDCab And tc.CoMoneda=od.IDMoneda
--HLF-20141013-Case When isnull(od.IDMoneda,'USD')='USD' Then 
--Left Join COTICAB_TIPOSCAMBIO tc On tc.IDCab=@IDCab And tc.CoMoneda=od.IDMoneda 
CREATE Procedure [dbo].[RESERVAS_DETSERVICIOS_InsxIDDet]  
	@IDCab int,  
	@IDDet int,  
	@IDReserva int,  
	@UserMod char(4)  
As  
	Set NoCount On  
   
	Insert Into RESERVAS_DETSERVICIOS  
	(IDReserva_Det,IDServicio_Det,  
	CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,  
	SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,  
	MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,  
	STMargenImpto,TotImpto,UserMod)    

	Select IDReserva_Det,IDServicio_Det,      
	CostoReal*TCambio,X.CostoLiberado*TCambio,X.Margen*TCambio,X.MargenAplicado,
	X.MargenLiberado*TCambio,X.Total*TCambio,      
	X.SSCR*TCambio,X.SSCL*TCambio,X.SSMargen*TCambio,X.SSMargenLiberado*TCambio,
	X.SSTotal*TCambio,X.STCR*TCambio,X.STMargen*TCambio,X.STTotal*TCambio,
	X.CostoRealImpto*TCambio,X.CostoLiberadoImpto*TCambio,X.MargenImpto*TCambio,
	X.MargenLiberadoImpto*TCambio,X.SSCRImpto*TCambio,X.SSCLImpto*TCambio,
	X.SSMargenImpto*TCambio,X.SSMargenLiberadoImpto*TCambio,X.STCRImpto*TCambio,
	X.STMargenImpto*TCambio,X.TotImpto*TCambio,@UserMod
	From

		(
		Select Distinct od.IDReserva_Det,rs.IDServicio_Det,  
		rs.CostoReal,rs.CostoLiberado,rs.Margen,rs.MargenAplicado,rs.MargenLiberado,rs.Total,  
		rs.SSCR,rs.SSCL,rs.SSMargen,rs.SSMargenLiberado,rs.SSTotal,rs.STCR,  
		rs.STMargen,rs.STTotal,rs.CostoRealImpto,rs.CostoLiberadoImpto,rs.MargenImpto,rs.MargenLiberadoImpto,  
		rs.SSCRImpto,rs.SSCLImpto,rs.SSMargenImpto,rs.SSMargenLiberadoImpto,rs.STCRImpto,rs.STMargenImpto,  
		rs.TotImpto,
		Case When isnull(od.IDMoneda,'USD')='USD' Then 
		1 
		Else
			Case When tc.SsTipCam Is Null Then sd.TC Else tc.SsTipCam End      
		End as TCambio		
		From COTIDET_DETSERVICIOS rs Inner Join COTIDET rd On rs.IDDET=rd.IDDET And rd.IDDET=@IDDet  
		Inner Join COTICAB r On r.IDCab=@IDCab  
		Inner Join RESERVAS res On r.IDCAB=rd.IDCAB And res.IDReserva=@IDReserva  
		Inner Join RESERVAS_DET od On rd.IDDET=od.IDDet And od.IDReserva=res.IDReserva  
		Inner Join MASERVICIOS_DET sd On rs.IDServicio_Det=sd.IDServicio_Det  
		Left Join COTICAB_TIPOSCAMBIO tc On tc.IDCab=@IDCab And tc.CoMoneda=od.IDMoneda
		Where rs.Total>0 And IsNull(od.NuViaticoTC,0) = 0 And
		CAST(od.IDReserva_Det as varchar(20)) + ' '+ CAST(rs.IDServicio_Det as varchar(20))   
		Not In (Select CAST(IDReserva_Det as varchar(20)) + ' '+ CAST(IDServicio_Det as varchar(20))  
		From RESERVAS_DETSERVICIOS)  
		) as X 
