﻿Create Procedure dbo.SINCERADO_DET_Upd
@NuSincerado_Det int,
@QtPax tinyint,
@QtPaxLiberados tinyint,
@SsCostoNeto numeric(12,4),
@SsCostoIgv numeric(12,4),
@SsCostoTotal numeric(12,4),
@UserMod char(4)
As
Set NoCount on
Update SINCERADO_DET
	   set QtPax = @QtPax,
		   QtPaxLiberados =@QtPaxLiberados,
		   SsCostoNeto = @SsCostoNeto,
		   SsCostoIgv = @SsCostoIgv,
		   SsCostoTotal = @SsCostoTotal,
		   UserMod = @UserMod
Where NuSincerado_Det = @NuSincerado_Det
