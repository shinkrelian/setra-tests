﻿
--JRF-20150306-Agregando Campos QtEntCompradas,FlServStockUso
--HLF-20151103-@IDServicio char(8)
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_Ins  
--@IDServicio_Det int,  
@IDServicio char(8),  
@QtSkEnt smallint,  
@QtEntCompradas smallint,
@UserMod char(4)  
As  
Set NoCount On  
Insert into STOCK_TICKET_ENTRADAS   
  (IDServicio,  
   QtEntCompradas,
   QtStkEnt,  
   UserMod,  
   FecMod)  
    values  
     (@IDServicio,  
      @QtEntCompradas,
      @QtSkEnt,  
      @UserMod,  
      GETDATE())  


