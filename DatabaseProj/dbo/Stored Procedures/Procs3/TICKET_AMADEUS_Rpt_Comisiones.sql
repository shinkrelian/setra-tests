﻿
--UPDATE MATIPOOPERACION SET TxDefinicion='';
--GO

--TICKET_AMADEUS_Rpt_Comisiones '01/03/2015','18/03/2015'
--CREATE
CREATE PROCEDURE TICKET_AMADEUS_Rpt_Comisiones
--declare
@fecha1 as smalldatetime='01/01/1900',--'01/01/2014',--'01/06/2014',
@fecha2 as smalldatetime='01/01/1900',--'18/02/2015',--'01/07/2014',
@CoPeriodo char(9)=''
AS
BEGIN
SELECT CoLineaAereaSigla,
	   CoLineaAereaSigla2,
	   Num_Ticket=COUNT(NuTicket),
	   Neto=SUM(TxTarifaAerea),
	   IGV=SUM(IGV),
	   Otros=SUM(Otros),
	   TxTotal=SUM(TxTotal),
	   TxComision=AVG(TxComision),
	   Comision_Sub=SUM(Comision_Sub),
	   IGV_Comision=SUM(IGV_Comision),
	   SsComision=SUM(SsComision),
	   SsCosto=SUM(SsCosto),
	   orden=case when CoLineaAereaSigla2='LP' THEN 1 WHEN CoLineaAereaSigla2='LP2' THEN 2 WHEN CoLineaAereaSigla2='' THEN 99 ELSE 3 END
	   
 FROM(
select 

TxFechaEmision,
NuSerieTicket,
NuTicket,
CoLineaAereaSigla,
CoLineaAereaSigla2= case when ((Es_IQT_PEM+Es_INT)>0 and CoLineaAereaSigla='LP') THEN 'LP2' ELSE CoLineaAereaSigla END,
Ruta,
TxTarifaAerea,
IGV=case when ((Es_IQT_PEM+Es_INT)>0 and CoLineaAereaSigla='LP') THEN 0 ELSE IGV END,
Otros,
TxTotal,
TxComision,
Comision_Sub,
IGV_Comision,
SsComision,
SsCosto,
NoPax,
File_NV,
Grupo,
Cuenta,
CoFormaPago
,Es_IQT_PEM
,Es_INT

from
		(
		select 
		TxFechaEmision,
		substring(t.CoTicket,1,3) as NuSerieTicket,
		right(t.CoTicket,10) as NuTicket,
		t.CoLineaAereaSigla,
		CoLineaAereaSigla2=CoLineaAereaSigla
		,isnull(dbo.FnRutaItinetarioTicket(t.CoTicket),'') as Ruta,
		TxTarifaAerea,
		IGV=isnull((select SUM(ssmonto) from TICKET_AMADEUS_TAX where CoTax IN ('PE AD','PE') and  CoTicket=t.CoTicket),0),
		Otros=isnull((select SUM(ssmonto) from TICKET_AMADEUS_TAX where CoTax NOT IN ('PE AD','PE') and  CoTicket=t.CoTicket),0),
		TxTotal,
		--aa=(1+(select top 1 NuIGV from PARAMETRO)/100),
		TxComision,
		Comision_Sub=cast(round((ssComision/(1+(select top 1 NuIGV from PARAMETRO)/100)),2) as numeric(6,2)),
		IGV_Comision=ssComision-cast(round((ssComision/(1+(select top 1 NuIGV from PARAMETRO)/100)),2) as numeric(6,2)),
		SsComision,
		SsCosto,
		t.NoPax,
		File_NV=ISNULL( case when t.IDCab IS null then
				 (select top 1 NuNtaVta from NOTAVENTA_COUNTER_DETALLE where NuDocumento=t.CoTicket)
				 else (select idfile from COTICAB where IDCab=t.IDCab)
				 end,''),
		Grupo=ISNULL(case when t.IDCab IS not null then
				 (select titulo from COTICAB where IDCab=t.IDCab)
				 else ''
				 end,''),
		Cuenta=ISNULL(case when t.IDCab IS not null then
				 (
					select top 1 Isnull(cl.RazonComercial,'')
					from TICKET_AMADEUS ti inner Join COTICAB c On ti.IDCab = c.IDCAB    
					inner Join MACLIENTES cl On Isnull(ti.IDCliente,c.IDCliente) = cl.IDCliente    
					and c.IDCAB=t.IDCab
				 )
				 else ''
				 end,''),
		t.CoFormaPago,
		--Observaciones=isnull(TxObsCou,isnull((select Observaciones from coticab where idcab=t.idcab),''))
		Observaciones=isnull(TxObsCou,'')--isnull((select Observaciones from coticab where idcab=t.idcab),''))
		,Es_IQT_PEM=case when (select COUNT(*) from TICKET_AMADEUS_ITINERARIO where CoTicket=t.CoTicket and (CoCiudadPar in ('IQT','PEM') or CoCiudadLle in ('IQT','PEM')))>0 then 1 else 0 end
		,Es_INT=case when (select COUNT(*) from TICKET_AMADEUS_ITINERARIO where CoTicket=t.CoTicket and (CoCiudadPar in (select distinct codigo from MAUBIGEO where IDPais<>'000323' ) Or CoCiudadLle IN (select distinct codigo from MAUBIGEO where IDPais<>'000323' )) )>0 then 1 else 0 end
		from TICKET_AMADEUS t
		where 
		--CoProvOrigen='IATA' and
		 (@fecha1 ='01/01/1900' or (t.TxFechaEmision>@fecha1 and t.TxFechaEmision<@fecha2+1))
		and (ltrim(rtrim(@CoPeriodo))='' or ((t.TxFechaEmision>(select FeIniPer from CALENDARIO_IATA where CoPeriodo=@CoPeriodo)) and (t.TxFechaEmision<(select FeFinPer+1 from CALENDARIO_IATA where CoPeriodo=@CoPeriodo)) ))

		) as x
--where Es_INT=1
) AS Y
group by CoLineaAereaSigla,
	     CoLineaAereaSigla2
ORDER BY orden,CoLineaAereaSigla2--TxFechaEmision
END
