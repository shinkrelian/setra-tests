﻿--JRF-JHD-20160328-..@FlEntradasMuseos bit ..Where (@FlEntradasMuseos=1 And p.IDTipoProv='006') Or 
--                  (@FlEntradasMuseos=0 And (p.IDTipoProv<>'006' And ods.IDProveedor_Prg Is Not Null))
--JRF-20160413-Case When Case When @FlEntradasMuseos=1 T...
CREATE Procedure dbo.RESERVAS_Sel_Rpt_PresupuestosGastos
@IDProveedor char(6),
@IDUsuarioResp char(4),
@IDUsuarioResv char(4),
@IDUsuarioOper char(4),
@IDFile char(8),
@IDFormaPago varchar(3),
@FePagoDesde smalldatetime,
@FePagoHasta smalldatetime,
@FeAsigResDesde smalldatetime,
@FeAsigResHasta smalldatetime,
@FeAceptDesde smalldatetime,
@FeAceptHasta smalldatetime,
@FeInServDesde smalldatetime,
@FeInServHasta smalldatetime,
@FeOutDesde smalldatetime,
@FeOutHasta smalldatetime,
@FeCotizDesde smalldatetime,
@FeCotizHasta smalldatetime,
@CoOficinaFact char(6),
@FlEntradasMuseos bit
As

--Declare @IDProveedor char(6)='', --OK
--		@IDUsuarioResp char(4)='', --OK
--		@IDUsuarioResv char(4)='', --OK
--		@IDUsuarioOper char(4)='', --OK
--		@IDFile char(8)='', --OK - 16030105
--		@IDFormaPago varchar(30)='', --Ok
--		@FePagoDesde smalldatetime = '13/04/2016',
--		@FePagoHasta smalldatetime = '15/04/2016',
--		@FeAsigResDesde smalldatetime = '01/01/1900', --Ok
--		@FeAsigResHasta smalldatetime = '01/01/1900', --Ok
--		@FeAceptDesde smalldatetime = '01/01/1900',
--		@FeAceptHasta smalldatetime = '01/01/1900',
--		@FeInServDesde smalldatetime = '01/01/1900', --Ok
--		@FeInServHasta smalldatetime = '01/01/1900', --Ok
--		@FeOutDesde smalldatetime = '01/01/1900', --Ok
--		@FeOutHasta smalldatetime = '01/01/1900', --Ok
--		@FeCotizDesde smalldatetime = '01/01/1900', --Ok
--		@FeCotizHasta smalldatetime = '01/01/1900', --Ok
--		@CoOficinaFact char(6) = '000065',
--		@FlEntradasMuseos bit = 0

Set NoCount On
Declare @IDMonedaOfi char(3)='SOL'
if @CoOficinaFact = '000113'
	Set @IDMonedaOfi = 'ARS'
if @CoOficinaFact = '000110'
	Set @IDMonedaOfi = 'CLP'

Declare @IDProvInterno char(8) = '001554'
if @CoOficinaFact = '000066'
	Set @IDProvInterno = '000544'
if @CoOficinaFact = '000113'
	Set @IDProvInterno = '002315'
if @CoOficinaFact = '000110'
	Set @IDProvInterno = '002317'

Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)
DECLARE @Files as table (idcab int,IDFile char(8))
​
insert into @Files
select x.idcab,X.IDFile from(
select  c.idcab,c.IDFile from coticab c 
where c.flhistorico=0 and c.Estado='A' 
And (Convert(char(10),@FeInServDesde,103)='01/01/1900' Or c.FecInicio between @FeInServDesde and @FeInServHasta)
And (Convert(char(10),@FeOutDesde,103)='01/01/1900' Or c.FechaOut between @FeOutDesde and @FeOutHasta)
And (Convert(char(10),@FeCotizDesde,103)='01/01/1900' Or c.Fecha between @FeCotizDesde and @FeCotizHasta)
And (Convert(char(10),@FeAsigResDesde,103)='01/01/1900' Or c.FechaAsigReserva between @FeAsigResDesde and @FeAsigResHasta)
And (Convert(char(10),@FeAceptDesde,103)='01/01/1900' Or c.FechaReserva between @FeAceptDesde and @FeAceptHasta)
And (Ltrim(Rtrim(@IDFile))='' Or c.IDFile = @IDFile)
And (ltrim(rtrim(@IDUsuarioResp))='' Or c.IDUsuario = @IDUsuarioResp)
And (ltrim(rtrim(@IDUsuarioResv))='' Or c.IDUsuarioRes = @IDUsuarioResv)
And (ltrim(rtrim(@IDUsuarioOper))='' Or c.IDUsuarioOpe = @IDUsuarioOper)
) as x

Declare @ReporteGastos table(FeVctoPago smalldatetime,FeRecepcion smalldatetime,FechaInicio smalldatetime,FechaOut smalldatetime,FormaPago varchar(50),Proveedor varchar(100),
							 IDFile char(8),TotalMonedaDif numeric(12,2),TotalMonedaUSD numeric(12,2))
Declare @ReporteGastosTmp table (IDReserva int,FechaOrdenPago smalldatetime,TotalOrdenPago numeric(12,2),FeRecepcion smalldatetime,TotalDocumento numeric(12,2),
								 Proveedor varchar(200),IDFormaPago char(3),IDFile char(8),IDMoneda char(3),NuDiasCredito smallint,
								 TotalModulo numeric(12,2),FechaIn smalldatetime,FechaOut smalldatetime,IDCab int,IDProveedo char(6))

								 

Insert into @ReporteGastosTmp
Select Y.* 
from (
Select X.IDReserva,null as FechaOrdenPago,null as TotalOrdenPago,
	   X.FeRecepcion,X.TotalDocumento,X.NombreCorto,X.IDFormaPago,X.IDFile,X.IDMoneda,x.NuDiasCredito,
	   Sum(X.NetoGen) As Total,Min(X.Dia) as FechaIn, Max(X.Dia) As FechaOut,x.IDCab,x.IDProveedor_Prg
from (
select 0 as IDReserva,dp.FeRecepcion,dp.SSTotal as TotalDocumento,p.NombreCorto,p.IDFormaPago,c.IDFile,ods.IDMoneda,
IsNull(ods.TotalProgram, 
	   Case When isnull(NetoCotizado,0) = 0 then
			Case When dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)) = 0 Or od.FlServicioTC=1 Then
				od.CostoReal
			Else
				Case When dCot.IDServicio_Det IS NULL Then
					dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),dCot.CoMoneda,ods.IDMoneda,
					Case When ods.IDMoneda<>'USD' Then (select ct.SsTipCam from COTICAB_TIPOSCAMBIO ct where ct.IDCab=o.IDCab And ct.CoMoneda=ods.IDMoneda) else 1 End)
    Else                
     dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),
		dCot.CoMoneda,ods.IDMoneda,Case When ods.IDMoneda<>'USD' Then (select ct.SsTipCam from COTICAB_TIPOSCAMBIO ct where ct.IDCab=o.IDCab And ct.CoMoneda=ods.IDMoneda) else		1 End) End End    
  Else NetoCotizado End) as NetoGen,Cast(Convert(char(10),od.Dia,103) as smalldatetime) as Dia,IsNull(fp.NuDiasCredito,0) as NuDiasCredito,
  p.IDProveedor as IDProveedor_Prg,o.IDCab,
  Case When @FlEntradasMuseos=1 Then Case When p.IDTipoProv='006' then 1 else 0 End Else 0 End As FiltroEntradasMuseo
from OPERACIONES_DET_DETSERVICIOS ods 
Inner Join OPERACIONES_DET od On ods.IDServicio=od.IDServicio And ods.IDOperacion_Det=od.IDOperacion_Det And ods.Activo= 1 --And ods.IDProveedor_Prg is not Null
Inner Join OPERACIONES o On od.IDOperacion = o.IDOperacion
Inner Join @Files c On o.IDCab=c.idcab
Left Join MASERVICIOS_DET dCot On ISNULL(ods.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det
Left Join MASERVICIOS sCot On dCot.IDServicio=sCot.IDServicio
--Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg = p.IDProveedor And p.CoUbigeo_Oficina=@CoOficinaFact And p.IDFormaPago <> '003'
Inner Join MAPROVEEDORES p On IsNull(ods.IDProveedor_Prg,sCot.IDProveedor) = p.IDProveedor And p.CoUbigeo_Oficina=@CoOficinaFact And p.IDFormaPago <> '003'
Left Join MAFORMASPAGO fp On p.IDFormaPago=fp.IdFor
Left Join ORDEN_SERVICIO_DET osd On ods.IDOperacion_Det=osd.IDOperacion_Det And ods.IDServicio_Det=osd.IDServicio_Det And ods.FlServicioNoShow=osd.FlServicioNoShow
Left Join DOCUMENTO_PROVEEDOR dp On osd.NuOrden_Servicio=dp.NuOrden_Servicio And dp.FlActivo=1
--Where (@FlEntradasMuseos=1 And p.IDTipoProv='006') Or (@FlEntradasMuseos=0 And (p.IDTipoProv<>'006' And ods.IDProveedor_Prg Is Not Null))
) as X
Where (x.IDFormaPago = @IDFormaPago Or Ltrim(Rtrim(@IDFormaPago))='') And
	  (x.IDProveedor_Prg = @IDProveedor Or Ltrim(Rtrim(@IDProveedor))='') And x.FiltroEntradasMuseo=@FlEntradasMuseos
Group By X.IDReserva,X.FeRecepcion,X.TotalDocumento,X.NombreCorto,X.IDFormaPago,X.IDFile,X.IDMoneda,x.NuDiasCredito,X.IDProveedor_Prg,X.IDCab
Union
Select x.IDReserva,x.FechaOrdenPago,x.TotalOrdenPago,x.FeRecepcion,x.TotalDocumento,x.NombreCorto,x.IDFormaPago,x.IDFile,x.IDMoneda,x.NuDiasCredito,
	   x.TotalReservas,x.FechaIn,x.FechaOut ,x.IDCab,x.IDProveedor
from (
select r.IDReserva,op.FechaPago as FechaOrdenPago,dp.FeRecepcion,dp.SSTotal as TotalDocumento,
	   Case When p.IDTipoOper='I' And Exists(select o.IDOperacion from OPERACIONES o Where o.IDReserva=r.IDReserva) 
	   then 
		Case When Exists(select ods.IDOperacion_Det from OPERACIONES o 
				Inner Join @Files f On o.IDCab=f.idcab And f.idcab=r.IDCab
				Inner Join OPERACIONES_DET od On o.IDOperacion=od.IDOperacion
				Inner Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det And od.IDServicio=ods.IDServicio 
				And ods.Activo=1 where o.IDReserva=r.IDReserva And ods.IDProveedor_Prg is not null) then 1 else 0 End
		else
		 Case When Exists(select o.IDOperacion from OPERACIONES o 
					      Inner Join @Files f On o.IDCab=f.idcab And f.idcab=r.IDCab And o.IDProveedor=r.IDProveedor
						  Inner Join OPERACIONES_DET od On o.IDOperacion=od.IDOperacion 
						  Inner Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det And IsNull(rd.Anulado,0)=0
						  Where IsNull(od.IDVoucher,0) <> 0) then 1 else 0 End
		End 
	   As ExisteOperacionesPrg,
	   p.NombreCorto,p.IDFormaPago,c.IDFile,rd.IDMoneda,IsNull(fp.NuDiasCredito,0) as NuDiasCredito,
	   Round(Sum(rd.TotalGen),2) as TotalReservas,
	   Round(op.SsMontoTotal,2) as TotalOrdenPago,
	   Cast(convert(char(10),Min(rd.Dia),103) as smalldatetime) as FechaIn,
	   Cast(Convert(char(10),IsNull(Max(rd.FechaOut),Max(rd.Dia)),103) as smalldatetime) as FechaOut,r.IDCab,r.IDProveedor
from RESERVAS r Inner Join @Files f On r.IDCab=f.idcab And r.Anulado=0
Inner Join COTICAB c On r.idcab=c.IDCAB
Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor And p.CoUbigeo_Oficina = @CoOficinaFact And p.IDFormaPago <> '003'
Left Join MAFORMASPAGO fp On p.IDFormaPago=fp.IdFor
Inner Join RESERVAS_DET rd On r.IDReserva=rd.IDReserva And rd.Anulado=0 And rd.FlServicioNoShow=0
Left Join ORDENPAGO op On r.IDReserva=op.IDReserva
Left Join DOCUMENTO_PROVEEDOR dp On op.IDOrdPag = dp.CoOrdPag And dp.FlActivo=1
Where @FlEntradasMuseos=0
Group By r.IDReserva,op.FechaPago,dp.FeRecepcion,dp.SSTotal,op.SsMontoTotal,r.IDCab,r.IDProveedor,p.IDTipoOper,p.NombreCorto,p.IDFormaPago,c.IDFile,rd.IDMoneda,fp.NuDiasCredito
) as X
Where X.ExisteOperacionesPrg = 0 --And x.IDFile='16100100'
And (x.IDFormaPago = @IDFormaPago Or Ltrim(Rtrim(@IDFormaPago))='') 
And (x.IDProveedor = @IDProveedor Or Ltrim(Rtrim(@IDProveedor))='')
Union
Select x.IDReserva,x.FechaOrdenPago,x.TotalOperaciones,x.FeRecepcion,x.TotalDocumento,x.NombreCorto,x.IDFormaPago,x.IDFile,x.IDMoneda,x.NuDiasCredito,
	   IsNull(x.TotalDocumento,x.TotalOperaciones) as TotalOperaciones,x.FechaIn,x.FechaOut ,x.IDCab,x.IDProveedor
from (
select o.IDReserva,null as FechaOrdenPago,dp.FeRecepcion,dp.SSTotal as TotalDocumento,
	   Case When p.IDTipoOper='I'
	   then 
		Case When Exists(select ods.IDOperacion_Det from OPERACIONES o2
				Inner Join @Files f On o2.IDCab=f.idcab And f.idcab=o2.IDCab
				Inner Join OPERACIONES_DET od On o2.IDOperacion=od.IDOperacion
				Inner Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det And od.IDServicio=ods.IDServicio 
				And ods.Activo=1 where o2.IDOperacion = o.IDOperacion  And ods.IDProveedor_Prg is not null) then 1 else 0 End
		else
		 0
		End 
	   As ExisteOperacionesPrg,
	   p.NombreCorto,p.IDFormaPago,c.IDFile,IsNull(dp.CoMoneda,od.IDMoneda) as IDMoneda,
	   IsNull(fp.NuDiasCredito,0) as NuDiasCredito,
	   Round(Sum(od.TotalGen),2) as TotalOperaciones, vod.SsMontoTotal as TotalVoucher,
	   Cast(convert(char(10),Min(od.Dia),103) as smalldatetime) as FechaIn,
	   Cast(Convert(char(10),IsNull(Max(od.FechaOut),Max(od.Dia)),103) as smalldatetime) as FechaOut,o.IDCab,o.IDProveedor
from OPERACIONES o Inner Join @Files f On o.IDCab=f.idcab
--Inner Join RESERVAS r On o.IDOperacion=r.IDReserva And r.Anulado=0
Inner Join COTICAB c On o.idcab=c.IDCAB
Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor And p.CoUbigeo_Oficina = @CoOficinaFact And p.IDFormaPago <> '003'
Left Join MAFORMASPAGO fp On p.IDFormaPago=fp.IdFor
Inner Join OPERACIONES_DET od On o.IDOperacion=od.IDOperacion And od.FlServicioNoShow=0
Left Join VOUCHER_OPERACIONES_DET vod On od.IDVoucher = vod.IDVoucher And od.IDMoneda=vod.CoMoneda
Inner Join VOUCHER_OPERACIONES vo On vod.IDVoucher = vo.IDVoucher
Left Join DOCUMENTO_PROVEEDOR dp On vo.IDVoucher = dp.NuVoucher And dp.FlActivo=1
Where @FlEntradasMuseos=0
Group By o.IDReserva,o.IDOperacion,dp.FeRecepcion,dp.SSTotal,vod.SsMontoTotal,o.IDCab,o.IDProveedor,
p.IDTipoOper,p.NombreCorto,p.IDFormaPago,c.IDFile,dp.CoMoneda,od.IDMoneda,fp.NuDiasCredito
) as X
Where X.ExisteOperacionesPrg = 0
And (x.IDFormaPago = @IDFormaPago Or Ltrim(Rtrim(@IDFormaPago))='')
And (x.IDProveedor = @IDProveedor Or Ltrim(Rtrim(@IDProveedor))='')
) As Y


Insert into @ReporteGastos
Select * from (
select Case When rpg.FechaOrdenPago is Not Null then rpg.FechaOrdenPago Else
	    Case When rpg.NuDiasCredito > 0 then DATEADD(d,rpg.NuDiasCredito,IsNull(rpg.FeRecepcion,rpg.FechaOut)) else 
			Case When rpg.IDFormaPago='002' Then DATEADD(d,dbo.FnDevuelveDiasPrepagoxFile(rpg.IDCab,rpg.IDProveedo)*-1,FechaIn) Else 
				   Case When rpg.IDFormaPago='010' Then DATEADD(d,15,IsNull(rpg.FeRecepcion,rpg.FechaOut)) Else Null End End End End As FeVctoPago,
	FeRecepcion,FechaIn,FechaOut,fp.Descripcion as FormaPago,Proveedor,IDFile,
	0 As TotalMonedaDif,TotalModulo As TotalMonedaUSD
from @ReporteGastosTmp rpg Left Join MAFORMASPAGO fp On rpg.IDFormaPago=fp.IdFor
Where IDMoneda='USD'
) as X
Where (x.FeVctoPago Between @FePagoDesde And @FePagoHasta Or Convert(char(10),@FePagoDesde,103)='01/01/1900')

Insert into @ReporteGastos
Select * from (
select Case When rpg.FechaOrdenPago is Not Null then rpg.FechaOrdenPago Else
	    Case When rpg.NuDiasCredito > 0 then DATEADD(d,rpg.NuDiasCredito,IsNull(FeRecepcion,FechaOut)) else 
			Case When rpg.IDFormaPago='002' Then DATEADD(d,dbo.FnDevuelveDiasPrepagoxFile(rpg.IDCab,rpg.IDProveedo)*-1,FechaIn) Else 
			   Case When rpg.IDFormaPago='010' Then DATEADD(d,15,FechaOut) Else 
			     Case When p.IDTipoProv='006' Then FechaIn Else Null End End End End End As FeVctoPago,
	FeRecepcion,FechaIn,FechaOut,fp.Descripcion as FormaPago,Proveedor,IDFile,TotalModulo As TotalMonedaDif,0 As TotalMonedaUSD
from @ReporteGastosTmp rpg Left Join MAFORMASPAGO fp On rpg.IDFormaPago=fp.IdFor
Left Join MAPROVEEDORES p On rpg.IDProveedo=p.IDProveedor
Where IDMoneda=@IDMonedaOfi And Not Exists(select rp1.Proveedor from @ReporteGastos rp1 Where rp1.IDFile=rpg.IDFile And rp1.Proveedor=rpg.Proveedor)
) as X
Where (x.FeVctoPago Between @FePagoDesde And @FePagoHasta Or Convert(char(10),@FePagoDesde,103)='01/01/1900')

select FeVctoPago,FeRecepcion,FechaInicio,FechaOut,FormaPago,Proveedor,IDFile,
	   IsNull((select sum(rpgt.TotalModulo) from @ReporteGastosTmp rpgt where rpgt.IDMoneda=@IDMonedaOfi And rpgt.IDFile=rpg.IDFile And rpgt.Proveedor=rpg.Proveedor),0) 
	   as TotalMonedaDif,TotalMonedaUSD
from @ReporteGastos rpg
Order By FeVctoPago 
