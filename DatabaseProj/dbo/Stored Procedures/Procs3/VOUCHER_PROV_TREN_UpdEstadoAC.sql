﻿
--HLF-20150317- @IDCab int,  
--vt.IDCAB=@IDCab
CREATE Procedure dbo.VOUCHER_PROV_TREN_UpdEstadoAC     
	--@NuFile char(8),       
	@IDCab int,  
	@CoProveedor char(6),
	@UserMod char(4)     

As      

	Set NoCount On      	

	Update VOUCHER_PROV_TREN set CoEstado='AC',UserMod=@UserMod,FecMod=GETDATE()
	where NuVouPTrenInt In (Select vt.NuVouPTrenInt From VOUCHER_PROV_TREN vt 
	--Inner Join COTICAB cc On vt.IDCab=cc.IDCAB 
	--And cc.IDFile=@NuFile 
	Where vt.IDCab=@IDCab
	And vt.CoProveedor=@CoProveedor)

