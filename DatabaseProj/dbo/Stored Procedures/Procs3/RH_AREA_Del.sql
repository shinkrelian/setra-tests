﻿
----------------------------------------------------------------------------------------

CREATE PROCEDURE [RH_AREA_Del]
	@CoArea tinyint
AS
BEGIN
	Delete [dbo].[RH_AREA]
	Where 
		[CoArea] = @CoArea
END;
