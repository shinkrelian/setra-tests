﻿--HLF-20140623-Agregando parametro @CoPNR_Amadeus  
--HLF-20140707-Agregando parametro @CoPNR_Aerolinea   
--HLF-20141023-@CoPNR_Aerolinea varchar(20)
CREATE Procedure dbo.TICKET_AMADEUS_ITINERARIO_Ins    
@CoTicket char(13),    
@NuSegmento tinyint,    
@CoCiudadPar char(3),    
@CoLineaAerea char(2),    
@NuVuelo char(4),    
@CoSegmento char(1),    
@CoBooking char(1),    
@TxFechaPar smalldatetime,    
--@TxHoraPar char(4),    
@TxBaseTarifa char(6),    
@TxBag char(3),    
@CoST char(2),    
@CoCiudadLle char(3),    
@TxHoraLle smalldatetime,    
@CoPNR_Amadeus   char(6),    
@CoPNR_Aerolinea varchar(20),  
@UserMod char(4)    
As    
Set NoCount On    
Insert into [TICKET_AMADEUS_ITINERARIO]    
           ([CoTicket]    
           ,[NuSegmento]    
           ,[CoCiudadPar]    
           ,[CoLineaAerea]    
           ,[NuVuelo]    
           ,[CoSegmento]    
           ,[CoBooking]    
           ,[TxFechaPar]    
           --,[TxHoraPar]    
           ,[TxBaseTarifa]    
           ,[TxBag]    
           ,[CoST]    
           ,[CoCiudadLle]    
           ,[TxHoraLle]   
           ,CoPNR_Amadeus  
           ,CoPNR_Aerolinea  
           ,[UserMod]    
           ,[FecMod])    
     VALUES    
           (@CoTicket    
           ,@NuSegmento    
           ,@CoCiudadPar     
           ,@CoLineaAerea     
           ,@NuVuelo     
           ,@CoSegmento    
           ,@CoBooking    
           ,@TxFechaPar    
           --,@TxHoraPar    
           ,@TxBaseTarifa    
           ,@TxBag    
           ,@CoST    
           ,@CoCiudadLle    
           ,@TxHoraLle    
           ,@CoPNR_Amadeus  
           ,@CoPNR_Aerolinea  
           ,@UserMod    
           ,GetDate())    
             
