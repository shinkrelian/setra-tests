﻿
---------------------------------------------------------------------------------------------------------

--CREATE
CREATE PROCEDURE RPT_VENTA_CLIENTES_Del
	@NuAnio char(4)='',
	--@CoMes tinyint=0,
	@CoCliente char(6)=''
AS
BEGIN
	Delete dbo.RPT_VENTA_CLIENTES
	Where 
		NuAnio = @NuAnio And 
		--CoMes = @CoMes And 
		CoCliente = @CoCliente
END;
