﻿--JRF-20140623-Agregando el campo [CoPNR_Amadeus]  
--HLF-20140704-@CoTicket char(13)  
--JRF-20140707-Agregar el campo [CoPNR_Aerolinea]  
--JRF-20141021-Agregar el IDTransporte de COTIVUELOS
CREATE Procedure dbo.TICKET_AMADEUS_ITINERARIO_Sel_ListxCoTicket        
@CoTicket char(13)        
As        
Set NoCount On        
select tai.NuSegmento,CoLineaAerea ,NuVuelo,CoCiudadPar+'/'+CoCiudadLle as Itinerario  
 ,CoLineaAerea +' '+NuVuelo as LAVLO,CoCiudadPar,CoCiudadLle ,  
 CONVERT(char(10),  TxFechaPar,103) as Fecha,  
 CONVERT(CHAR(10),TxFechaPar,103) as TxFechaPar,  CONVERT(CHAR(10),TxHoraLle,103) as TxFechaLle,  
 CONVERT(CHAR(5),TxFechaPar,108) as TxHoraPar ,CONVERT(CHAR(5),TxHoraLle,108) as TxHoraLle    
 , CoSegmento, CoBooking ,TxBaseTarifa ,ISNULL(CoPNR_Amadeus,'') as CoPNR_Amadeus,  
 CoPNR_Aerolinea ,TxBag ,CoST  ,ISNULL(cv.ID,0) as IDTrans
from TICKET_AMADEUS_ITINERARIO tai --tai Left Join MAUBIGEO ub On tai.CoCiudadPar=ub.Codigo        
Left Join COTIVUELOS cv On tai.CoTicket=cv.CoTicket and tai.NuSegmento=cv.NuSegmento
where tai.CoTicket = @CoTicket
