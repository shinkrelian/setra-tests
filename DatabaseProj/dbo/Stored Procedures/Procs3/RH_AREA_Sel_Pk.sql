﻿
-----------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[RH_AREA_Sel_Pk]
	@CoArea tinyint=0
AS
BEGIN
	Select
		[CoArea],
 		[NoArea],
 		[FlActivo],
 		[UserMod],
 		[FecMod]
 	From [dbo].[RH_AREA]
	Where 
		(@CoArea=0 or [CoArea] = @CoArea)
		
END;
