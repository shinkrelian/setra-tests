﻿
Create Procedure [dbo].RESERVAS_UpdEstado2
	@IDReserva	int,
	@IDEstado	char(2),
	@UserMod	char(4)
As
	Set NoCount On

	Update RESERVAS Set Estado=@IDEstado, FecMod=GETDATE(), UserMod=@UserMod,
	UserModEstado=@UserMod
	Where IDReserva=@IDReserva
