﻿
Create Procedure dbo.VOUCHER_OFICINA_EXTERNA_Ins
	@IDCab int,
	@CoProveedor char(6),
	@FeVoucher smalldatetime,

	@CoMoneda char(3) ,
	@SsImpuestos numeric(8,2) ,
	@SsTotal numeric(8,2) ,
	
	@CoUbigeo_Oficina char(6),
	@UserMod char(4),
	@pNuVouOfiExtInt int output
As
	Set Nocount on

	Declare @AnioMes char(7)=cast(year(GETDATE()) as CHAR(4))+upper(CAST(GETDATE() AS nvarchar(3)))
	Declare @vcNuVouOfi varchar(3)
	Select @vcNuVouOfi=isnull(MAX(CAST(RIGHT(NuVouOfiExt,3) as smallint)),0)+1 From VOUCHER_OFICINA_EXTERNA Where Left(NuVouOfiExt,7)=@AnioMes	
	Set @vcNuVouOfi=REPLICATE('0',3-len(@vcNuVouOfi)) +@vcNuVouOfi
	
	Declare @CoUbigeoDC char(3)=isnull((Select isnull(DC,'') From MAUBIGEO Where IDubigeo=@CoUbigeo_Oficina),'')
	
	Declare @NuVouOfiExtInt int,@NuVouOfiExt char(13)=@AnioMes+@CoUbigeoDC+@vcNuVouOfi
	
	Exec Correlativo_SelOutput 'VOUCHER_OFICINA_EXTERNA',1,10,@NuVouOfiExtInt output           

	Insert Into VOUCHER_OFICINA_EXTERNA
	(NuVouOfiExtInt,
	NuVouOfiExt,
	IDCab,
	CoProveedor,
	FeVoucher,
	CoMoneda,
	SsImpuestos,
	SsTotal,
	SsSaldo,
	SsDifAceptada,	
	UserMod)
	Values
	(@NuVouOfiExtInt,
	@NuVouOfiExt,
	@IDCab,
	@CoProveedor,
	Case When @FeVoucher='01/01/1900' Then getdate() Else @FeVoucher End,
	@CoMoneda,
	@SsImpuestos,
	@SsTotal,
	@SsTotal,
	0,	
	@UserMod)
	
	Set @pNuVouOfiExtInt=@NuVouOfiExtInt
	
