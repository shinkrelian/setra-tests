﻿Create Procedure dbo.RESERVAS_UpdCambiosVtasAceptados
	@IDReserva	int,
	@CambiosVtasAceptados	bit,
	@UserMod	char(4)
As
	Set Nocount On
	
	Update RESERVAS Set CambiosVtasAceptados=@CambiosVtasAceptados,
	UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva=@IDReserva
	
