﻿
Create Procedure dbo.VENTA_ADICIONAL_DET_UpdDocumento
	@NuVtaAdi int ,
	@CoServicio smallint ,
	@NuPax int,
	@NuDocum char(10),
	@IDTipoDoc char(3),
	@UserMod char(4)
As
	Set Nocount On

	Update VENTA_ADICIONAL_DET Set NuDocum=@NuDocum,IDTipoDoc=@IDTipoDoc,
	UserMod=@UserMod, FecMod=getdate() 
	Where NuVtaAdi=@NuVtaAdi And
		CoServicio=@CoServicio And	NuPax=@NuPax 

