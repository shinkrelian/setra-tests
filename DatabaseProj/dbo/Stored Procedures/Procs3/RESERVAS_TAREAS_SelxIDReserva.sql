﻿CREATE Procedure [dbo].[RESERVAS_TAREAS_SelxIDReserva]
	
	@IDReserva int
As
	Set NoCount On

	Select IDTarea,	FecDeadLine, Descripcion, IDEstado, 
	Case  IDEstado
		When 'PD' Then 'PENDIENTE'
		When 'RZ' Then 'REALIZADA'
	End as DescEstado,
	Case When IDReserva=0 Then 'POR FILE'
	Else
		Case When IDTarea<=6 Then 
			'POR POLITICA PROV.'
		Else 'MANUAL'
		End
	End +
	Case When UltimoMinuto=1 Then '(Último Minuto)' Else '' End
	as Tipo
	
	From RESERVAS_TAREAS
	Where IDReserva=@IDReserva
	Order by FecDeadLine
