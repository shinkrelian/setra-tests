﻿Create Procedure dbo.SINCERADO_DET_Ins
@NuSincerado int,
@IDServicio_Det int,
@QtPax tinyint,
@QtPaxLiberados tinyint,
@SsCostoNeto numeric(12,4),
@SsCostoIgv numeric(12,4),
@SsCostoTotal numeric(12,4),
@UserMod char(4),
@pNuSincerado_Det int output
As
Set NoCount On
Declare @NuSincerado_Det int =(select Isnull(MAX(NuSincerado_Det),0)+1 from SINCERADO_DET)  
Insert into SINCERADO_DET
		(NuSincerado_Det,
		 NuSincerado,
		 IDServicio_Det,
		 QtPax,
		 QtPaxLiberados,
		 SsCostoNeto,
		 SsCostoIgv,
		 SsCostoTotal,
		 UserMod,
		 FecMod)
		values(
		 @NuSincerado_Det,
		 @NuSincerado,
		 @IDServicio_Det,
		 @QtPax,
		 @QtPaxLiberados,
		 @SsCostoNeto,
		 @SsCostoIgv,
		 @SsCostoTotal,
		 @UserMod,
		 GETDATE())
set @pNuSincerado_Det = @NuSincerado_Det
