﻿CREATE Procedure dbo.RESERVAS_SelIDReservaOutput
	@IDCab int,        
	@IDProveedor char(6),
	@pIDReserva int Output        
As        
	Set Nocount On        

	Select @pIDReserva=IDReserva From  RESERVAS Where IDCab=@IDCab And IDProveedor=@IDProveedor
	And anulado=1
	Set @pIDReserva=Isnull(@pIDReserva,0)
	
