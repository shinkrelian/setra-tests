﻿--HLF-20130614-Cambiando tamanio de @Observaciones de varchar(250) a varchar(MAX)       
--JRF-20130703-Agregando el campo EstadoSolicitud.(Para tema de mensajería)    
--HLF-20131010-Agregando parametro @RegeneradoxAcomodo  
CREATE Procedure [dbo].[RESERVAS_Upd]        
 @IDReserva int,         
 @Estado char(2),        
 @IDUsuarioRes char(4),        
 @Observaciones varchar(MAX),        
 @CodReservaProv varchar(50),        
 @EstadoSolicitud char(4),    
 @RegeneradoxAcomodo bit,  
 @UserMod char(4)        
As        
 Set NoCount On 
 Declare @IDCab int = (select distinct IDCab from RESERVAS where IDReserva =@IDReserva)
 update COTICAB set IDUsuarioRes =@IDUsuarioRes,FechaAsigReserva = GETDATE() where FechaAsigReserva is null and IDCAB= @IDCab
         
 Update RESERVAS         
   Set Estado=@Estado,            
    IDUsuarioRes=@IDUsuarioRes,        
    Observaciones= Case when LTRIM(rtrim(@Observaciones))='' Then Null else @Observaciones End,        
    CodReservaProv=Case when ltrim(rtrim(@CodReservaProv))='' then null else @CodReservaProv End,        
    EstadoSolicitud = @EstadoSolicitud ,    
    UserMod=@UserMod,      
    RegeneradoxAcomodo=@RegeneradoxAcomodo,  
    FecMod=GetDate()         
 Where IDReserva=@IDReserva     
