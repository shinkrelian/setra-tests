﻿
---------------------------------------------------------------------------------------------------------

--CREATE
CREATE PROCEDURE RPT_VENTA_CLIENTES_Sel_List
	@NuAnio char(4)='',
	--@CoMes tinyint=0,
	@CoCliente char(6)=''
AS
BEGIN
	Select
		NuAnio,
 		--CoMes,
 		--Mes=dbo.FnDescripcionMes(CoMes),
 		CoCliente,
 		Clientes=isnull((select isnull(razonsocial,RazonComercial) from MACLIENTES where IDCliente=CoCliente),''),
 		SsImporte,
 		QtNumFiles,
 		SsPromedio,
 		SsUtilidad,
 		CoTipo,
 		UserMod,
 		FecMod
 	From dbo.RPT_VENTA_CLIENTES
	Where 
		(NuAnio = @NuAnio or LTRIM(rtrim(@NuAnio))='') And 
		--(CoMes = @CoMes or @CoMes=0) And 
		(CoCliente = @CoCliente or LTRIM(rtrim(@CoCliente))='')
END;
