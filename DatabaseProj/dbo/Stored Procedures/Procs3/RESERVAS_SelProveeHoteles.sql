﻿CREATE Procedure dbo.RESERVAS_SelProveeHoteles 
	@IDCab int
As    
	Set Nocount On    

	SELECT IDProveedor, IDReserva, CodReservaProv, Observaciones FROM
	(
	Select r.IDProveedor, r.IDReserva, 
	isnull(r.CodReservaProv,'') as CodReservaProv, 
	isnull(r.Observaciones,'') as Observaciones, 
	p.IDTipoProv ,
    (Select COUNT(*) From RESERVAS_DET rd1                           
	Inner Join RESERVAS r1 On rd1.IDReserva=r1.IDReserva And r1.IDReserva=r.IDReserva
	Inner Join MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor And p1.IDTipoProv='003'                          
	Inner Join MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det 
		And sd1.ConAlojamiento=1                                                  
	) as OperadorConAlojam 
	From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor		
	Where r.IDCab=@IDCab And r.Anulado=0 And r.Estado<>'XL'
	) AS X
	WHERE X.IDTipoProv='001' OR (X.IDTipoProv='003' And X.OperadorConAlojam>=1)                                    

