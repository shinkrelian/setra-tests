﻿--JRF-20150519-Actualizar solo los Proveedores Internos
--JRF-20160414-Actualizar por From (Agregar @IDServicio_Det)
CREATE Procedure dbo.RESERVAS_DET_UpdFeRowxIDDet
@IDDet int,
@IDServicio_Det int
As  
Set NoCount On  
Update RESERVAS_DET 
	set FeUpdateRow = DATEADD(ss,120,GETDATE())
From RESERVAS_DET rd Inner Join COTIDET cd On rd.IDDet=cd.IDDET
Where rd.IDDet=@IDDet And rd.IDServicio_Det=@IDServicio_Det
