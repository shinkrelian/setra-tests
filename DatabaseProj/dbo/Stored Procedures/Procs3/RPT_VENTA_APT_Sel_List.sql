﻿

-----------------------------------------------------------------------------------


--CREATE
--JHD-20150116- Se cambió el campo @CoMes a char(2)
--JHD-20150116- Mes=dbo.FnDescripcionMes(cast(CoMes as tinyint))
CREATE PROCEDURE [dbo].[RPT_VENTA_APT_Sel_List]
	@NuAnio char(4)='',
	@CoMes char(2)=''
AS
BEGIN
	Select
		NuAnio,
 		CoMes,
 		Mes=dbo.FnDescripcionMes(cast(CoMes as tinyint)),
 		SsImporte,
 		SsImporte_Sin_APT,
 		QtPax,
 		QtNumFiles,
 		UserMod,
 		FecMod
 	From dbo.RPT_VENTA_APT
	Where 
		(NuAnio = @NuAnio or LTRIM(rtrim(@NuAnio))='' ) And 
		(CoMes = @CoMes or @CoMes=0)
END;
