﻿CREATE PROCEDURE VOUCHER_OPERACIONES_List_Proveedor_Doc
@coproveedor char(6)
AS
BEGIN
select distinct v.IDVoucher,
c.Titulo,
c.IDFile,
Monto=isnull((select sum(SSTotal) from DOCUMENTO_PROVEEDOR where NuVoucher=v.IDVoucher),0)
from VOUCHER_OPERACIONES v
left join OPERACIONES_DET od 
on v.IDVoucher=od.IDVoucher
left join operaciones o on o.IDOperacion=od.IDOperacion
left join coticab c on c.IDCAB=o.IDCab
where o.IDProveedor=@coproveedor
and v.IDVoucher not in (select distinct NuVoucher from DOCUMENTO_PROVEEDOR where NuVoucher is not null)
and v.IDVoucher<>0
order by c.idfile, v.IDVoucher
END;
