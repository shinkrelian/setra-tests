﻿Create Procedure dbo.SINCERADO_DET_PAX_Ins
@NuSincerado_Det int,
@IDPax int,
@UserMod char(4)
As
Set NoCount On
Declare @NuSincerado_Det_Pax int = (select Isnull(Max(NuSincerado_Det_Pax),0)+1 
									from SINCERADO_DET_PAX)
Insert into SINCERADO_DET_PAX
		(NuSincerado_Det_Pax,
		 NuSincerado_Det,
		 IDPax,
		 UserMod,
		 FecMod)
		values
		(@NuSincerado_Det_Pax,
		 @NuSincerado_Det,
		 @IDPax,
		 @UserMod,
		 GETDATE())
