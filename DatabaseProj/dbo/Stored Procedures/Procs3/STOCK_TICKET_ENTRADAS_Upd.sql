﻿
--HLF-20151103-@IDServicio char(8)
--HLF-20160107-FecMod=GETDATE()
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_Upd
--@IDServicio_Det int,  
@IDServicio char(8),
@QtStkEnt smallint,
@QtEntCompradas smallint,
@UserMod char(4)
As
Set NoCount On
Update STOCK_TICKET_ENTRADAS
Set QtEntCompradas=@QtEntCompradas,
    QtStkEnt=@QtStkEnt,UserMod=@UserMod,
	FecMod=GETDATE()
Where IDServicio=@IDServicio

