﻿--JHD-20150929- Se cambio la consulta para listar por detalle de voucher
--JHD-20151002- Codigo=v.IDVoucher,
--JHD-20151027- cambiar la busqueda incluyendo los proveedores que tienen como prov. internacional el @idproveedor
--JHD-20151104- IGV=(SELECT SUM(IgvGen) FROM OPERACIONES_DET WHERE IDVoucher=vd.IDVoucher AND IDMoneda=VD.CoMoneda),
CREATE PROCEDURE [dbo].[VOUCHER_OPERACIONES_Sel_List_Proveedor]
@idproveedor char(6),
@CoMoneda char(3)=''
as
BEGIN
 
 declare @tablaproveedores as table(
 idprov char(6)
 )

 insert into @tablaproveedores values (@idproveedor)

 insert into @tablaproveedores 
 select distinct idproveedor from MAPROVEEDORES where IDProveedorInternacional=@idproveedor

 --declare @idproveedor_int as char(6)=''
 --set @idproveedor_int=isnull((select IDProveedorInternacional from MAPROVEEDORES where IDProveedor=@idproveedor),'')

 --if @idproveedor_int<>''
	--set @idproveedor=@idproveedor_int

select distinct
Codigo=v.IDVoucher,
v.IDVoucher,
c.Titulo,
c.IDCAB,
c.IDFile,
--od.IDMoneda,
vd.CoMoneda as IDMoneda,
--m.Simbolo,
Simbolo=(select simbolo from MAMONEDAS where IDMoneda=vd.comoneda),
--Total1=Sum(od.Total),
IGV=(SELECT SUM(IgvGen) FROM OPERACIONES_DET WHERE IDVoucher=vd.IDVoucher AND IDMoneda=VD.CoMoneda),
vd.SsMontoTotal as Total
from VOUCHER_OPERACIONES v
inner join OPERACIONES_DET od on od.IDVoucher=v.IDVoucher
inner join operaciones o on o.IDOperacion=od.IDOperacion
inner join coticab c on c.IDCAB=o.IDCab
--inner join mamonedas m on m.IDMoneda=od.IDMoneda
left join VOUCHER_OPERACIONES_DET vd on vd.IDVoucher=v.IDVoucher
--where o.IDProveedor=@idproveedor
where o.IDProveedor in (select idprov from @tablaproveedores)
----v.IDVoucher=416476
AND (VD.CoMoneda=@CoMoneda or @CoMoneda='')
and v.idvoucher<>'0'
and v.IDVoucher not in (select distinct nuvoucher from DOCUMENTO_PROVEEDOR where nuvoucher is not null and FlActivo=1)

/*
group by v.IDVoucher,
c.Titulo,
c.IDFile,
vd.comoneda,
m.Simbolo
*/
order by v.IDVoucher
END;
