﻿Create Procedure dbo.RESERVAS_SelExistsIngresoManual
@IDReserva int,  
@pExiste bit OutPut  
As  
 Set NoCount On  
 Set @pExiste = 0  
 If Exists(select IDDet from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva   
     where rd.Anulado=0 and r.Anulado=0 and rd.IDReserva=@IDReserva and rd.FlServicioIngManual=1)  
  Set @pExiste=1  
