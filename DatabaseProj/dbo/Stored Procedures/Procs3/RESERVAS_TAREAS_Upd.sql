﻿Create Procedure [dbo].[RESERVAS_TAREAS_Upd]
	@IDReserva int,	
	@IDTarea	smallint,
	@IDEstado	char(2),
	@FecDeadLine smalldatetime,	
	@IDCab	int,
	@Descripcion	varchar(200),
	@UserMod char(4)
As
	Set NoCount On
	
	Update RESERVAS_TAREAS Set IDEstado=@IDEstado, FecDeadLine=@FecDeadLine, 
	Descripcion=@Descripcion, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva=@IDReserva And IDTarea=@IDTarea And IDCab=@IDCab
