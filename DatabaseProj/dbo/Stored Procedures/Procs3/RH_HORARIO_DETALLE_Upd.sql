﻿
------------------------------------------------------------------------------------------

CREATE PROCEDURE RH_HORARIO_DETALLE_Upd
	@CoHorario tinyint,
	@CoDetalle tinyint,
	@NuDia tinyint,
	@FeHoraInicio time,
	@FeHoraFin time,
	@UserMod char(4)
AS
BEGIN
	Update dbo.RH_HORARIO_DETALLE
	Set
		NuDia = @NuDia,
 		FeHoraInicio = @FeHoraInicio,
 		FeHoraFin = @FeHoraFin,
 		UserMod = Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End

 	Where 
		CoHorario = @CoHorario And 
		CoDetalle = @CoDetalle
END;
