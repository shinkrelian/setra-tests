﻿CREATE PROCEDURE [RH_ASISTENCIA_Ins_Migrar_Excel]
	--@NuAsistencia int OUT ,
	@ID nvarchar(255),
	@DIA nvarchar(255),
	@HENT nvarchar(255),
	@HSAL nvarchar(255),
	@retraso nvarchar(255),
	@ida nvarchar(255),
	
	--@CoHorario tinyint,

	@UserMod char(4)
	
AS
BEGIN
	declare @NuAsistencia int = null
	set @NuAsistencia=isnull((select MAX(nuasistencia) from RH_ASISTENCIA),0)+1
	
	INSERT INTO [RH_ASISTENCIA]
           (NuAsistencia
           ,[CoTrabCargo]
           ,[CoTipoAsistencia]
           ,[FeFecha]
           ,[CoHorario]
           ,[FeMarcacionEntrada]
           ,[FeMarcacionSalida]
           ,[NuMinutosTarde]
           ,[NuHorasTrabajadas]
           ,[NuSemana]
          
           ,[UserMod]
          )
          
 SELECT 
	   @NuAsistencia
	  ,NuTrabCargo=(select NuTrabCargo from dbo.RH_TRABAJADOR_CARGO where cotrabajador=@ID AND FLACTIVO=1)
      ,1
      
      ,Fecha=CONVERT(smalldatetime,@DIA,101)
      --,@CoHorario
      ,CoHorario=(select top 1 cohorario from dbo.RH_TRABAJADOR_HORARIO where FeFechaInicio<CONVERT(smalldatetime,@DIA,101) and CONVERT(smalldatetime,@DIA,101)<FeFechaFin+1)
      
      ,entrada=CAST(CONVERT(smalldatetime,@HENT,120) AS TIME(0))
      
      ,salida=CAST(CONVERT(smalldatetime,@HSAL,120) AS TIME(0))
      
      --,Retraso_minutos=ISNULL(CAST(retraso as time(0)),'00:00:00')
      ,Retraso_minutos2=ISNULL((select (datepart(HOUR,CAST(@retraso as time(0)))*60)+datepart(MINUTE,CAST(@retraso as time(0)))),0)
      ,TotalHorasdia=CAST(CONVERT(smalldatetime,@HSAL,120)-CONVERT(smalldatetime,@HENT,120)-('1900-01-01 01:00:00') as time(0))
      ,semana=substring(@ida,len(@ida),1)
      ,@UserMod
	

        Select @NuAsistencia
END;
