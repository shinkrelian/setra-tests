﻿
CREATE PROCEDURE dbo.RESERVAS_DET_UpdAnuladoxIDReserva
	@IDReserva	int,
	@UserMod char(4),
	@Anulado bit
As
	Set NoCount On
	
	Update RESERVAS_DET Set Anulado=@Anulado, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva = @IDReserva
