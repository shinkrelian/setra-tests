﻿CREATE Procedure dbo.TICKET_AMADEUS_TAX_Ins
@CoTicket char(13),
@NuTax tinyint,
@CoTax char(5),
@SsMonto numeric(6,2),
@UserMod char(4)
As
Set NoCount On
INSERT INTO [dbo].[TICKET_AMADEUS_TAX]
           ([CoTicket]
           ,[NuTax]
           ,[CoTax]
           ,[SsMonto]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@CoTicket,
            @NuTax,
            @CoTax,
            @SsMonto,
            @UserMod,
            GetDate())

