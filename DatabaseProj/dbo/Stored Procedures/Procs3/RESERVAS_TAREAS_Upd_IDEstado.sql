﻿Create Procedure [dbo].[RESERVAS_TAREAS_Upd_IDEstado]
	@IDReserva	int,
	@IDTarea	smallint,
	@IDCab	int,
	@IDEstado	char(2),
	@UserMod	char(4)
As
	Set NoCount On
	
	Update RESERVAS_TAREAS Set IDEstado=@IDEstado, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva=@IDReserva And IDTarea=@IDTarea And IDCab=@IDCab
