﻿CREATE PROCEDURE [RPT_VENTA_DESTINO_Del]
	@NuAnio char(4),
	@CoUbigeo char(6)
AS
BEGIN
	Delete [dbo].[RPT_VENTA_DESTINO]
	Where 
		[NuAnio] = @NuAnio And 
		[CoUbigeo] = @CoUbigeo
END
