﻿CREATE Procedure [dbo].[RESERVAS_DET_Upd_NuevoIDServicio]
	@IDServicio char(8),		
	@IDServicioNue char(8),
	@UserMod char(4)
As
	Set NoCount On
	
	Update RESERVAS_DET
		SET IDServicio=@IDServicioNue, FecMod=GETDATE(), UserMod=@UserMod
	Where IDServicio=@IDServicio
