﻿CREATE Procedure [dbo].[RESERVAS_DET_Upd_IDDet]
	@IDReserva	int,
	@IDDet	int,
	@UserMod	char(4)
As
	Set NoCount On
	
	Update RESERVAS_DET Set IDDet=@IDDet, UserMod=@UserMod, FecMod=GETDATE()	
	Where IDReserva=@IDReserva
