﻿Create Procedure dbo.SINCERADO_DET_PAX_Sel_List
@NuSincerado_Det int
As
Set NoCount On
select sdp.*,sd.NuSincerado,sd.IDServicio_Det from SINCERADO_DET_PAX sdp 
Left Join SINCERADO_DET sd On sdp.NuSincerado_Det = sd.NuSincerado_Det
where sdp.NuSincerado_Det = @NuSincerado_Det
