﻿--JRF-20130613-Utilizar la fecha del detalle y la ubigeo de reserva.  
CREATE Procedure RESERVAS_Sel_RptObservaciones    
@IDCab int    
As     
 Set NoCount On     
 select     
   
 Isnull((select top(1) u.Descripcion from reservas_det dt left join MAUBIGEO u On dt.IDubigeo = u.IDubigeo  
 where dt.IDReserva = r.IDReserva Order by Dia),'') as Ciudad,  
 Isnull(Convert(char(10),  
 (select top(1) Dia from reservas_det dt where dt.IDReserva = r.IDReserva order by Dia)  
 ,103),'') As [Date],Observaciones   
 from RESERVAS r --r Left Join MAUBIGEO     
 where IDCab = @IDCab and Observaciones is not null    
