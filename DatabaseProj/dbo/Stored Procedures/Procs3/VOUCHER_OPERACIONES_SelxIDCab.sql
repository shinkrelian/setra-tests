﻿--HLF-20130813-Agregar columna ObservVoucher  
--JRF-20141028-Considerar solo no anulados
CREATE Procedure dbo.VOUCHER_OPERACIONES_SelxIDCab    
 @IDCab int    
As    
 Set Nocount On    
     
 Select IDVoucher, ServExportac, ExtrNoIncluid, isnull(ObservVoucher,'') as ObservVoucher  
 From VOUCHER_OPERACIONES    
 Where CoEstado <> 'AN' and IDVoucher In (Select IDVoucher From OPERACIONES_DET     
 Where IDOperacion In (Select IDOperacion From OPERACIONES Where IDCab=@IDCab))    
 And IDVoucher<>0    
