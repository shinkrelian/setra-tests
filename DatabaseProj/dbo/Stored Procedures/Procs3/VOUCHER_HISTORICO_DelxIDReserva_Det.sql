﻿Create Procedure dbo.VOUCHER_HISTORICO_DelxIDReserva_Det
@IDReserva_Det int
As
Set NoCount On
Delete from VOUCHER_HISTORICO
Where IDOperacion_Det In (select IDOperacion_Det from OPERACIONES_DET where IDReserva_Det=@IDReserva_Det)
