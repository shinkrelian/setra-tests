﻿--JRF-20150409-..distinct
CREATE Procedure dbo.RESERVAS_SelOperxRangoFechaDet
@IDCab int,
@Fecha1 smalldatetime,
@Fecha2 smalldatetime
As
select distinct r.IDProveedor,p.NombreCorto --,p.Telefono1 ,Telefono2
from RESERVAS r --Left Join RESERVAS_DET rd On rd.IDReserva = r.IDReserva
Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor
where r.Anulado=0 and r.IDCab = @IDCab 
and IDReserva In(select IDReserva from RESERVAS_DET 
				 where Anulado=0 and Dia between @Fecha1 and @Fecha2)
and p.IDTipoProv='003'
