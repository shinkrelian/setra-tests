﻿

--HLF-20151216-And r.IDProveedor IN (SELECT CoUsrPrvDe From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab)
--And r.IDCliente IN (SELECT CoUsrCliDe From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab) para todos los querys
CREATE Procedure [dbo].[RESERVAS_SelProveedoresCorreo]
	@IDCab int

As
	
	SELECT * FROM
		(
		Select 'BEVS' as CoArea ,R.IDCliente as CoClienteProvee, p.RazonSocial as DescClienteProvee
		From COTICAB r Inner Join MACLIENTES p On r.IDCliente=p.IDCliente
		Where r.IDCab=@IDCab 
			And r.IDCliente IN (SELECT CoUsrCliDe From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab)
		Union
		Select 'EEVS' as CoArea ,R.IDCliente, p.RazonSocial as DescCliente
		From COTICAB r Inner Join MACLIENTES p On r.IDCliente=p.IDCliente
		Where r.IDCab=@IDCab 
					And r.IDCliente IN (SELECT dc1.CoUsrCliPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 inner Join .BDCORREOSETRA..CORREOFILE cf1
				On dc1.NuCorreo=cf1.NuCorreo And cf1.IDCab=@IDCab)			
		) AS XCL
	UNION
	SELECT * FROM
		(
		Select 'BERS' as CoArea ,p.IDProveedor, p.NombreCorto as DescProveedor
		From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		Where r.IDCab=@IDCab And r.Anulado=0
			And r.IDProveedor IN (SELECT CoUsrPrvDe From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab)
		Union
		Select 'EERS' as CoArea ,p.IDProveedor, p.NombreCorto as DescProveedor
		From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		Where r.IDCab=@IDCab And r.Anulado=0
			And r.IDProveedor IN (SELECT dc1.CoUsrPrvPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 inner Join .BDCORREOSETRA..CORREOFILE cf1
				On dc1.NuCorreo=cf1.NuCorreo And cf1.IDCab=@IDCab)
		Union
		Select 'BEOP' as CoArea ,p.IDProveedor, p.NombreCorto as DescProveedor
		From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		Where r.IDCab=@IDCab And r.Anulado=0
			And r.IDProveedor IN (SELECT CoUsrPrvDe From .BDCORREOSETRA..CORREOFILE WHERE IDCab=@IDCab)
		Union
		Select 'EEOP' as CoArea ,p.IDProveedor, p.NombreCorto as DescProveedor
		From OPERACIONES r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		Where r.IDCab=@IDCab 
			And r.IDProveedor IN (SELECT dc1.CoUsrPrvPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 inner Join .BDCORREOSETRA..CORREOFILE cf1
				On dc1.NuCorreo=cf1.NuCorreo And cf1.IDCab=@IDCab)

		) AS XPR
	
	Order by 1,3
