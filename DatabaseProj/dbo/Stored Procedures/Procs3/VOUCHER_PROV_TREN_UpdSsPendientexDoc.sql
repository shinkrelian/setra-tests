﻿--JHD-20150821-No actualizar estado a Aceptado por BD
CREATE Procedure dbo.VOUCHER_PROV_TREN_UpdSsPendientexDoc      
 @NuVouPTrenInt int,      
 @SsTotalOrig numeric(9,2),      
 @UserMod char(4)      
As      
 Set NoCount On      
 Update VOUCHER_PROV_TREN      
  Set SsSaldo=SsSaldo-@SsTotalOrig,      
   UserMod=@UserMod,      
   FecMod=GETDATE()      
 where NuVouPTrenInt=@NuVouPTrenInt  
       
 Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from VOUCHER_PROV_TREN where NuVouPTrenInt=@NuVouPTrenInt)
  
 --if @SsSaldoPend = 0      
 -- Update VOUCHER_PROV_TREN set CoEstado='AC' where NuVouPTrenInt=@NuVouPTrenInt
