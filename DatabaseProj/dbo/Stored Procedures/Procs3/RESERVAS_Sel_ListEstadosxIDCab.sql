﻿--HLF-20140206-And r.Anulado=0
--PPMG-20151007-, r.IDReserva,-CotiDetLog
--JRF-20160412- ... And Exists(select rd2.IDReserva_Det from reservas_det rd2 Where rd2.IDReserva=r.IDReserva)
CREATE Procedure [dbo].[RESERVAS_Sel_ListEstadosxIDCab]      
				@IDCab Int
AS
BEGIN
	Set NoCount On
	SELECT IDProveedor, DescProveedor, IDTipoProv, IDEstado, DescEstado,
		   Case When CantCambiosReservas=0 Then '' Else 'Cambios en Reservas Pendientes' End as CambiosReservas,
		   Case When dbo.FnCambiosenVentasPostFileReservasSoloAlojamiento(IDReserva) = 1 Then 'M'
																						 Else 'O' End AS CotiDetLog,
		   0 As Inac, 0 AS NoHoteles, IDReserva, IDReservaProtecc, IDProveedorProtecc
		   , 0 as IDOperacion, 0 AS ExistAlojamiento, EmailReservas1, EmailReservas2, CodReservaProv
		   , ExisteAcomodoEspecial, EstadoSolicitud, IDFormaPago, CoUbigeoOficina, CodigoTarifa,
		   (CASE WHEN OperadorConAlojam = 0 THEN 0 ELSE 1 END) AS ConAlojamiento
		   ,FechaIn, FechaOut, Observaciones, IDTipoOper, SumCantidad
	FROM
	(
	Select r.IDProveedor, p.NombreCorto as DescProveedor,p.IDTipoProv, r.Estado as IDEstado, '' As DescEstado
	, r.IDReserva, ISNULL(RD.NReg,0) AS CantCambiosReservas, ISNULL(RD2.NReg,0) AS OperadorConAlojam,
	ISNULL(r.IDReservaProtecc,0) AS IDReservaProtecc, ISNULL(rp.IDProveedor,'') as IDProveedorProtecc,
	IsNull(pinternac.Email3,IsNull(p.Email3,'')) as EmailReservas1, IsNull(pinternac.Email4,IsNull(p.Email4,'')) as EmailReservas2
	, r.CodReservaProv, CASE WHEN ISNULL(CT1.NregProvCotiDet,0) >0 THEN 1 ELSE 0 END AS ExisteAcomodoEspecial
	, r.EstadoSolicitud, p.IDFormaPago, IsNull(p.CoUbigeo_Oficina,'') as CoUbigeoOficina
	, (CASE WHEN ISNULL(RDT1.NRegT,0) > 1 THEN '' ELSE dbo.FnCodigoTarifaxIDReserva(r.IDReserva) END) AS CodigoTarifa
	, (Select Top(1) Convert(char(10),Dia,103) from RESERVAS_DET rdf where rdf.IDReserva = r.IDReserva order by rdf.Dia) As FechaIn
	, (Select Top(1) Convert(char(10),FechaOut,103) from RESERVAS_DET rdf2
	   where rdf2.IDReserva = r.IDReserva and not rdf2.FechaOut is null order by rdf2.FechaOut desc)As FechaOut
	, r.Observaciones, p.IDTipoOper, ISNULL(RSC.SumCantidad,0) AS SumCantidad
	FROM RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
	LEFT JOIN (SELECT IDReserva, COUNT(*) AS NReg FROM RESERVAS_DET rd0 Where IDDet Is Null GROUP BY IDReserva) AS RD ON r.IDReserva = RD.IDReserva
	LEFT JOIN (SELECT rd1.IDReserva, COUNT(*) AS NReg FROM RESERVAS_DET rd1
					INNER JOIN RESERVAS r1 On rd1.IDReserva=r1.IDReserva
					INNER JOIN MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor
					INNER JOIN MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det
					LEFT OUTER JOIN (select IDDet, COUNT(*) AS NRegC from COTIVUELOS where IDCab=@IDCab GROUP BY IDDet) CT ON CT.IDDet=rd1.IDDet
						--And Not Exists(select ID from COTIVUELOS where IDCab=@IDCab And IDDet=rd1.IDDet)
					WHERE p1.IDTipoProv='003' AND sd1.ConAlojamiento=1 AND ISNULL(CT.NRegC,0) > 0
					GROUP BY rd1.IDReserva
					) RD2 ON RD2.IDReserva = r.IDReserva
	LEFT JOIN RESERVAS rp On r.IDReservaProtecc = rp.IDReserva
	LEFT JOIN MAPROVEEDORES pinternac On p.IDProveedorInternacional=pinternac.IDProveedor
	LEFT JOIN (SELECT IDProveedor, COUNT(*) AS NregProvCotiDet FROM COTIDET CD1
			   WHERE AcomodoEspecial = 1 and CD1.IncGuia = 0 and IDCab = @IDCab GROUP BY IDProveedor) CT1 ON r.IDProveedor = CT1.IDProveedor
	LEFT JOIN (SELECT IDReserva, COUNT(distinct IDServicio_Det) AS NRegT FROM RESERVAS_DET RDt GROUP BY IDReserva) RDT1 ON RDT1.IDReserva = r.IDReserva
	LEFT JOIN (SELECT IDReserva, SUM(Cantidad) AS SumCantidad FROM RESERVAS_DET RSDS LEFT JOIN MASERVICIOS_DET MSD ON RSDS.IDServicio_Det = MSD.IDServicio_Det
			   WHERE PlanAlimenticio = 0 GROUP BY IDReserva) RSC ON RSC.IDReserva = r.IDReserva
	WHERE r.IDCab=@IDCab AND (p.IDTipoProv='001' Or p.IDTipoProv='003') And r.Anulado=0
	And Exists(select rd2.IDReserva_Det from reservas_det rd2 Where rd2.IDReserva=r.IDReserva)
	) as X
	WHERE X.IDTipoProv='001' OR (X.IDTipoProv='003' And X.OperadorConAlojam>=1)
	UNION ALL
	SELECT IDProveedor, DescProveedor, IDTipoProv, IDEstado, DescEstado,
		   Case When CantCambiosReservas=0 Then '' Else 'Cambios en Reservas Pendientes' End as CambiosReservas,
		   Case When (IDTipoOper='I' Or dbo.FnExistenAlojamiento(IDReserva)=1) Or TieneReServ = 0 Then
					Case When dbo.FnCambiosenVentasPostFileReservasSoloServicios(IDReserva) = 1 Then 'M' Else 'O' End
				else Case When dbo.FnCambiosEnVentasPostFileSoloSubServicios(IDReserva) = 1 Then 'M' else 'O' End End As CotiDetLog
		   , 0 As Inac, 1 AS NoHoteles, IDReserva, IDReservaProtecc, IDProveedorProtecc
		   , 0 as IDOperacion, dbo.FnExistenAlojamiento(IDReserva) as ExistAlojamiento, EmailReservas1, EmailReservas2
		   , CodReservaProv, 0 AS ExisteAcomodoEspecial, EstadoSolicitud, IDFormaPago, CoUbigeoOficina, CodigoTarifa
		   , (CASE WHEN OperadorConAlojam = 0 THEN 0 ELSE 1 END) AS ConAlojamiento
		   , FechaIn, FechaOut, Observaciones, IDTipoOper, SumCantidad
	FROM
	(      
	Select r.IDProveedor, p.NombreCorto as DescProveedor,p.IDTipoProv, r.Estado as IDEstado, '' As DescEstado
	, r.IDReserva, ISNULL(RD.NReg,0) AS CantCambiosReservas, ISNULL(RD2.NReg,0) AS OperadorConAlojam
	, (Case When ISNULL(RD3.NRegP,0) > 0 Then 1 Else 0 End) AS PlanAlimenticio, p.IDTipoOper
	, (CASE WHEN ISNULL(RSDET.NReg_DS,0) = 0 THEN 0 ELSE 1 END) AS TieneReServ
	, ISNULL(r.IDReservaProtecc,0) AS IDReservaProtecc, '' as IDProveedorProtecc
	, IsNull(pinternac.Email3,IsNull(p.Email3,'')) as EmailReservas1, IsNull(pinternac.Email4,IsNull(p.Email4,'')) as EmailReservas2
	, r.CodReservaProv, r.EstadoSolicitud, p.IDFormaPago, IsNull(p.CoUbigeo_Oficina,'') as CoUbigeoOficina
	, CASE WHEN p.IDTipoProv = '002' then
		Case when ISNULL(RDT1.NRegT,0) > 1 then 'n' else dbo.FnCodigoTarifaxIDReserva(r.IDReserva) End
		else '' End as CodigoTarifa
	, (Select Top(1) Convert(char(10),Dia,103) from RESERVAS_DET rdf where rdf.IDReserva = r.IDReserva order by rdf.Dia) As FechaIn
	, (Select Top(1) Convert(char(10),FechaOut,103) from RESERVAS_DET rdf2
	   where rdf2.IDReserva = r.IDReserva and not rdf2.FechaOut is null order by rdf2.FechaOut desc)As FechaOut
	, r.Observaciones, ISNULL(RSC.SumCantidad,0) AS SumCantidad
    FROM RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor      
	LEFT JOIN (SELECT IDReserva, COUNT(*) AS NReg FROM RESERVAS_DET rd0 Where IDDet Is Null GROUP BY IDReserva) AS RD ON r.IDReserva = RD.IDReserva
	LEFT JOIN (SELECT rd1.IDReserva, COUNT(*) AS NReg FROM RESERVAS_DET rd1
					INNER JOIN RESERVAS r1 On rd1.IDReserva=r1.IDReserva
					INNER JOIN MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor
					INNER JOIN MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det
					LEFT OUTER JOIN (select IDDet, COUNT(*) AS NRegC from COTIVUELOS where IDCab=@IDCab GROUP BY IDDet) CT ON CT.IDDet=rd1.IDDet
						--And Not Exists(select ID from COTIVUELOS where IDCab=@IDCab And IDDet=rd1.IDDet)
					WHERE p1.IDTipoProv='003' AND sd1.ConAlojamiento=1 AND ISNULL(CT.NRegC,0) > 0
					GROUP BY rd1.IDReserva
					) RD2 ON RD2.IDReserva = r.IDReserva
	LEFT JOIN (Select rd4.IDReserva, COUNT(*) AS NRegP FROM RESERVAS_DET rd4
					INNER JOIN MASERVICIOS_DET sd1 On rd4.IDServicio_Det=sd1.IDServicio_Det
					WHERE sd1.PlanAlimenticio=1 GROUP BY rd4.IDReserva
					) RD3 ON RD3.IDReserva = r.IDReserva
	LEFT JOIN (SELECT RDD1.IDReserva, COUNT(RSDD1.IDServicio_Det) AS NReg_DS from RESERVAS_DETSERVICIOS RSDD1 INNER JOIN RESERVAS_DET RDD1
			   ON RSDD1.IDReserva_Det = RDD1.IDReserva_Det GROUP BY RDD1.IDReserva) AS RSDET ON RSDET.IDReserva = r.IDReserva
	LEFT JOIN MAPROVEEDORES pinternac On p.IDProveedorInternacional=pinternac.IDProveedor
	LEFT JOIN (SELECT IDReserva, COUNT(distinct IDServicio_Det) AS NRegT FROM RESERVAS_DET RDt WHERE Anulado = 0 GROUP BY IDReserva) RDT1 ON RDT1.IDReserva = r.IDReserva
	LEFT JOIN (SELECT IDReserva, ISNULL(SUM(Cantidad),0) AS SumCantidad FROM RESERVAS_DET RSDS LEFT JOIN MASERVICIOS_DET MSD ON RSDS.IDServicio_Det = MSD.IDServicio_Det
			   WHERE PlanAlimenticio = 0 GROUP BY IDReserva) RSC ON RSC.IDReserva = r.IDReserva
	WHERE IDCab = @IDCab And r.Anulado = 0
	And Exists(select rd2.IDReserva_Det from reservas_det rd2 Where rd2.IDReserva=r.IDReserva)
    ) as X1
	WHERE X1.OperadorConAlojam = 0 And (X1.IDTipoProv <> '001' OR (X1.IDTipoProv = '001' And PlanAlimenticio = 1))
	Order by DescProveedor
END
