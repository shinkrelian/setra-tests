﻿
--------------------------------------------------------------------


CREATE PROCEDURE [dbo].[RH_HORARIO_DETALLE_Ins]
	@CoHorario tinyint,
	--@CoDetalle tinyint,
	@NuDia tinyint,
	@FeHoraInicio time,
	@FeHoraFin time,
	@UserMod char(4)=''
AS
BEGIN
	declare @CoDetalle tinyint
	set @CoDetalle=isnull((select MAX(CoDetalle) from RH_HORARIO_DETALLE where CoHorario=@CoHorario),0)+1
	Insert Into dbo.RH_HORARIO_DETALLE
		(
			CoHorario,
 			CoDetalle,
 			NuDia,
 			FeHoraInicio,
 			FeHoraFin,
 			UserMod
 		)
	Values
		(
			@CoHorario,
 			@CoDetalle,
 			@NuDia,
 			@FeHoraInicio,
 			@FeHoraFin,
 			Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End
 		)
END;
