﻿Create Procedure dbo.RESERVAS_DET_UpdDiaxIDReserva_Det
@IDReserva_Det int,
@Dia smalldatetime,
@UserMod char(4)
As
	Set NoCount On
	UPDATE RESERVAS_DET
		Set Dia = @Dia,
			ExecTrigger = 0,
			UserMod = @UserMod,
			FecMod = GetDate()
	where IDReserva_Det = @IDReserva_Det
