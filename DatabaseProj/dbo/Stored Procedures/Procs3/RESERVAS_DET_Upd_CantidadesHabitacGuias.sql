﻿Create Procedure dbo.RESERVAS_DET_Upd_CantidadesHabitacGuias
	@IDCab int,
	@UserMod char(4)
As
	Set Nocount On
	
	Update RESERVAS_DET Set Cantidad=1, UserMod=@UserMod, FecMod=GETDATE()
	Where IDDet In (Select IDDet From COTIDET Where IDCAB=@IDCab And IncGuia=1)
	