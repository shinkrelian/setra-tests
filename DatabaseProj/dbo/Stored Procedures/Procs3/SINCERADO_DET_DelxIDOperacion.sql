﻿Create Procedure dbo.SINCERADO_DET_DelxIDOperacion  
@IDOperacion int  
as  
Set NoCount On  
delete from SINCERADO_DET 
where NuSincerado In   
(Select odds.NuSincerado   
from SINCERADO odds Left Join OPERACIONES_DET_DETSERVICIOS odd On odds.IDOperacion_Det = odd.IDOperacion_Det and odds.IDServicio_Det = odd.IDServicio_Det  
Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det  
Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion  
where o.IDOperacion = @IDOperacion)
