﻿--JHD-20150821-No actualizar estado a Aceptado por BD
CREATE Procedure dbo.VOUCHER_OFICINA_EXTERNA_UpdSsPendientexDoc      
 @NuVouOfiExtInt int,      
 @SsTotalOrig numeric(9,2),      
 @UserMod char(4)      
As      
 Set NoCount On      
 Update VOUCHER_OFICINA_EXTERNA      
  Set SsSaldo=SsSaldo-@SsTotalOrig,      
   UserMod=@UserMod,      
   FecMod=GETDATE()      
 where NuVouOfiExtInt=@NuVouOfiExtInt  
       
 Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from VOUCHER_OFICINA_EXTERNA where NuVouOfiExtInt=@NuVouOfiExtInt)      
  
 --if @SsSaldoPend = 0      
 -- Update VOUCHER_OFICINA_EXTERNA set CoEstado='AC' where NuVouOfiExtInt=@NuVouOfiExtInt  
  
