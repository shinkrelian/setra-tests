﻿CREATE PROCEDURE dbo.RESERVAS_DET_UpdIDsVentasPostM  
 @IDCab int,  
 @IDProveedor char(6),  
 @UserMod char(4)   
As  
  
 Set Nocount On  
   
 UPDATE RESERVAS_DET SET IDDet=Y.IDDetNueva, UserMod=@UserMod,FecMod=Getdate(),FeUpdateRow=Null
 FROM  
 (  
  
 SELECT * FROM   
 (  
 SELECT IDReserva_Det,  
  (SELECT   
  TOP 1 IDDet   
  FROM COTIDET WHERE   
  IDProveedor=R.IDProveedor AND IDServicio_Det=RD.IDServicio_Det AND  
  CONVERT(VARCHAR,Dia,103)=CONVERT(VARCHAR,RD.Dia,103)   
  and IDCab=R.IDCab ) AS IDDetNueva  
  FROM RESERVAS_DET RD   
  INNER JOIN RESERVAS R ON RD.IDReserva=R.IDReserva And R.IDProveedor=@IDProveedor And R.Anulado=0  
   AND RD.Anulado=0   
  INNER JOIN COTICAB CC ON CC.IDCAB=R.IDCab AND CC.IDCAB=@IDCab    
  WHERE RD.IDDet NOT IN (SELECT IDDet FROM COTIDET where IDCab=@IDCab)  
    
 ) AS X   
 WHERE NOT X.IDDetNueva IS NULL  
 ) AS Y   
 INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=Y.IDReserva_Det  
