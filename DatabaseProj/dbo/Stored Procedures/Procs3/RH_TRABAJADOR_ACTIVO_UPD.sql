﻿
CREATE PROCEDURE RH_TRABAJADOR_ACTIVO_UPD

		 @CoTrabajador char(11),
		   @FlActivo BIT,
           @UserMod char(4)
AS
BEGIN
UPDATE [RH_TRABAJADOR]
   SET 
      FlActivo =@FlActivo
    
      ,[UserMod] = @UserMod
      ,[FecMod] =GETDATE()
      
      
 WHERE CoTrabajador = @CoTrabajador
 END;

