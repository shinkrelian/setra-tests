﻿--CREATE
CREATE PROCEDURE [dbo].[RH_Cargo_Sel_Cbo_Lst]
@Todos bit,
@CoEmpresa tinyint=0
AS
BEGIN
Select '' as CoCargo,'<TODOS>' as Descripcion
Where @Todos = 1
Union
	Select
		[CoCargo],
 		[TxCargo] as Descripcion
 	
 	From [dbo].[RH_Cargo]
	Where 
		 (FlActivo =1 and (coempresa=@CoEmpresa or @CoEmpresa=0)) 
END;
