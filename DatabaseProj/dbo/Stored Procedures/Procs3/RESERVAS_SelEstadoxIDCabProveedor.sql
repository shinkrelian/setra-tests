﻿--JRF-20160413-..And Exists (select rd.IDReserva_Det from RESERVAS_DET rd Where rd.IDReserva=r.IDReserva And rd.Anulado=0)
Create Procedure dbo.RESERVAS_SelEstadoxIDCabProveedor
	@IDCab int,
	@CoProveedor char(6),
	@pCoEstado char(2) output
As
	Set Nocount On

	Set @pCoEstado=''
	Select Distinct @pCoEstado=Estado From RESERVAS r
	Where IDCab=@IDCab And IDProveedor=@CoProveedor And r.Anulado=0
	And Exists (select rd.IDReserva_Det from RESERVAS_DET rd Where rd.IDReserva=r.IDReserva And rd.Anulado=0)
