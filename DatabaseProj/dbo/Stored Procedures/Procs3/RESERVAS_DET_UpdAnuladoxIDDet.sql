﻿Create Procedure dbo.RESERVAS_DET_UpdAnuladoxIDDet
	@IDDet	int,
	@UserMod char(4),
	@Anulado bit
As
	Set NoCount On
	
	Update RESERVAS_DET Set Anulado=@Anulado,UserMod=@UserMod, FecMod=GETDATE()
	Where IDDet = @IDDet
