﻿
--JRF-20121207-Ordenando por la menor FechaIN, FechaOUT.                                      
--HLF-20120816-Subquery OperadorConAlojamiento                                        
--HLF-20120905-Nuevos campos Email3, Email4                                         
--HLF-20130130-Cambiando logica del As CotiDetLog                                    
--HLF-20130220-Agregando campo CambiosVtasAceptados                                  
--JRF-20130228-Cambiando posicion Imagen del estado y ordena x fecha.                                
--JRF-20130320-Ingresando la Máx. Fecha de Anulación(FechaIn - DiasSinPenalidad[MAPROVEEDORES])                              
--HLF-20130510-Agregando and not rd.FechaOut is null y order by fechaout Desc a FechaOut                            
--HLF-20130618-Agregando columna NuIngreso                          
--JRF-20130619-Agregando los campos de lectura (ObservacionPolitica,DocPdfRutaPdf)                        
--HLF-20130625-Reemplazando Atendido=0 And Accion='D') Then por Atendido=0 And Accion='B') Then  En columna CotiDetLog                      
--JRF-20130703-Agregando el campo EstadoSolicitud.(Para tema de mensajería)                    
--JRF-20130808-Agregando col CodTarifa                 
--HLF-20130901-Agregando Anulado = 0                
--HLF-20130916-Agregando pinternac.NombreCorto as DescProvInternac,              
--HLF-20131010-Agregando columna RegeneradoxAcomodo                 
--HLF-20131018-Agregando Case When Rv.CambiosenVentasPostFile          
--HLF-20131029-Agregando IDReservaProtecc, IDProveedorProtecc          
--JRF-20131213-Agrego campo que identifica si existe en Acomodos Especiales.        
--JRF-20140901-Agregar Funcion que compare lo actual en COTIDET >> RESERVAS_DET    
--JRF-20150115-Agregar el campo que identifica la Oficina de Fact. [CoUbigeo_Oficina]  
--JRF-20150413-Agregar el campo (...Pr.IDTipoOper)
--JRF-20151007-..IsNull(pinternac.Email3,I...
--JRF-20151030- --IsNull(pinternac.Email3,IsNull(pr.Email3,'')) as EmailReservas1, IsNull(pinternac.Email4,IsNull(pr.Email4,'')) as EmailReservas2,
-- Case When pr.IDProveedorInternacional is not null then IsNull(pinternac.Email3,'') else ISNull(pr.Email3,'') End as EmailReservas1,
-- Case When pr.IDProveedorInternacional is not null then IsNull(pinternac.Email4,'') else ISNull(pr.Email4,'') End as EmailReservas2
--JRF-20151209-Considerar Texto adicional para las areas administrativas.
--JRF-20160317-...y enviarla al siguiente correo:'+char(13)
CREATE PROCEDURE [dbo].[RESERVAS_Sel_ListxIDCabHotelyEspc]    
 @IDCab int                                            
AS
BEGIN
 Set NoCount On                                            
 Declare @TextAdmin varchar(Max)=''
 Set @TextAdmin = char(13)+'Al *status* esta reserva, le solicitamos adjuntar la copia de la FACTURA / INVOICE  que corresponde a los servicios contratados y enviarla al siguiente correo:'+char(13)
 + 'pagos1@setours.com' + char(13)
 + 'El documento (FACTURA / INVOICE) debe consignar la siguiente información:'+char(13)
 + 'Nombre comercial: SETOURS S.A.'+char(13)
 + 'RUC: 20100939887'+char(13)
 + 'Dirección: Av. Comandante Espinar 229 - Miraflores, Lima - Perú'+char(13)
 + 'Igualmente debe consignar: Servicio contratado / número de file / importa a pagar en la moneda pactada con SETOURS.'
                                          
 SELECT * FROM
 (                                        
	Select Rv.IDProveedor, Pr.NombreCorto as DescProveedor, isnull(pinternac.NombreCorto,'') as DescProvInternac,
	Pr.IDTipoProv, Pr.IDTipoOper,                                      
	--Case When ISNull(pr.Email3,'') = '' then ISNULL(pr.Email4,'') Else Email3 End as Email,                                            
	--IsNull(pinternac.Email3,IsNull(pr.Email3,'')) as EmailReservas1, IsNull(pinternac.Email4,IsNull(pr.Email4,'')) as EmailReservas2,
	Case When pr.IDProveedorInternacional is not null then IsNull(pinternac.Email3,'') else ISNull(pr.Email3,'') End as EmailReservas1,
	Case When pr.IDProveedorInternacional is not null then IsNull(pinternac.Email4,'') else ISNull(pr.Email4,'') End as EmailReservas2,
	u.IDPais, pr.IDCiudad, u.Descripcion as DescCiudad, Rv.Estado as IDEstado, Rv.IDReserva, 0 as IDOperacion, Rv.Observaciones, Rv.CodReservaProv,                                            
	Pr.IDFormaPago,                                        
	 --(Select COUNT(*) From RESERVAS_DET rd1                                         
	 --Inner Join RESERVAS r1 On rd1.IDReserva=r1.IDReserva And r1.IDReserva=Rv.IDReserva                                        
	 --Inner Join MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor And p1.IDTipoProv='003'                                        
	 --Inner Join MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det And sd1.ConAlojamiento=1 And Not Exists(select ID from COTIVUELOS where IDCab=@IDCab And IDDet=rd1.IDDet)                                   
	 -- ) as OperadorConAlojam ,                                      
	ISNULL(RCA.NReg,0) AS OperadorConAlojam,
	--(Select Top(1) Convert(char(10),rd.Dia,103) from RESERVAS_DET rd where rd.IDReserva = rv.IDReserva order by rd.Dia) As FechaIn, 
	--(Select Top(1) Convert(char(10),rd.FechaOut,103) from RESERVAS_DET rd    
	-- where rd.IDReserva = rv.IDReserva and not rd.FechaOut is null order by rd.FechaOut desc)As FechaOut,                                      
	Convert(char(10),FecDet.FechaIn,103) AS FechaIn,
	Convert(char(10),FecDet.FechaOut,103) AS FechaOut,
	dbo.FnFechaSinPenalidadReserva(Rv.IDCab,Rv.IDProveedor) As FechaPenalidad,
	Case When dbo.FnCambiosenVentasPostFileReservasSoloAlojamiento(Rv.IDReserva) = 1 Then    
			'M'          
		Else
			'O'
	End As CotiDetLog,                           
	Rv.CambiosVtasAceptados, 0 As Inac, 1 as NuIngreso, IsNull(Pr.ObservacionPolitica,'') as ObservacionPolitica,
	IsNull(Pr.DocPdfRutaPdf,'') as DocPdfRutaPdf, Rv.EstadoSolicitud,
	--Case when IsNull((select COUNT(distinct IDServicio_Det) from RESERVAS_DET rd where rd.IDReserva = Rv.IDReserva),0) > 1 then '' else
	--	dbo.FnCodigoTarifaxIDReserva(Rv.IDReserva) End as CodigoTarifa,                  
	Case when IsNull(RCT.NRegCT,0) > 1 then '' else
		dbo.FnCodigoTarifaxIDReserva(Rv.IDReserva) End as CodigoTarifa,
	rv.RegeneradoxAcomodo, ISNULL(Rv.IDReservaProtecc,0)  as IDReservaProtecc, isnull(rp.IDProveedor,'') as IDProveedorProtecc,        
	--case when (select COUNT(iddet) from cotidet cd
	--		   where AcomodoEspecial = 1 and cd.IncGuia = 0 and IDCab = @IDCab and Rv.IDProveedor = cd.IDProveedor)>0         
	--	then 1 Else 0 End as ExisteAcomodoEspecial,
	case when ISNULL(RAE.NRegAE,0) > 0         
		then 1 Else 0 End as ExisteAcomodoEspecial,
	IsNull(Pr.CoUbigeo_Oficina,'') as CoUbigeoOficina,
	Case When Pr.CoUbigeo_Oficina='000065' And Pr.IDFormaPago = '002' Then @TextAdmin else '' End As ObservacionAdmHotel
	From RESERVAS Rv Left Join MAPROVEEDORES Pr On Rv.IDProveedor= Pr.IDProveedor                                            
	Left Join MAUBIGEO u On pr.IDCiudad=u.IDubigeo                               
	Left Join MAPROVEEDORES pinternac On pr.IDProveedorInternacional=pinternac.IDProveedor              
	Left Join RESERVAS rp On rv.IDReservaProtecc=rp.IDReserva          
	--Left Join MAPROVEEDORES pOfiFact On Pr.CoUbigeo_Oficina=pOfiFact.CoUbigeo_Oficina 
	LEFT OUTER JOIN (SELECT r1.IDReserva, COUNT(*) AS NReg FROM RESERVAS_DET rd1
					 INNER JOIN RESERVAS r1 On rd1.IDReserva=r1.IDReserva
					 INNER JOIN MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor
					 INNER JOIN MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det
					 LEFT OUTER JOIN (SELECT IDDet FROM COTIVUELOS WHERE IDCab=@IDCab) CV on CV.IDDet = rd1.IDDet
					 WHERE p1.IDTipoProv='003' AND sd1.ConAlojamiento=1 AND CV.IDDet is null AND r1.IDCab = @IDCab
					 GROUP BY r1.IDReserva) RCA ON Rv.IDReserva = RCA.IDReserva
	LEFT OUTER JOIN (SELECT Rdf.IDReserva, MIN(Dia) AS FechaIn, MAX(FechaOut) AS FechaOut FROM RESERVAS_DET Rdf
					 INNER JOIN RESERVAS Rf ON Rdf.IDReserva = Rf.IDReserva
					 WHERE Rf.IDCab = @IDCab
					 GROUP BY Rdf.IDReserva) FecDet ON Rv.IDReserva = FecDet.IDReserva
	LEFT OUTER JOIN (SELECT IDReserva, COUNT(distinct IDServicio_Det) AS NRegCT FROM RESERVAS_DET GROUP BY IDReserva) RCT ON Rv.IDReserva = RCT.IDReserva
	LEFT OUTER JOIN (SELECT IDProveedor, COUNT(IDDET) AS NRegAE FROM COTIDET
					 WHERE AcomodoEspecial = 1 AND IncGuia = 0 AND IDCab = @IDCab
					 GROUP BY IDProveedor) RAE ON Rv.IDProveedor = RAE.IDProveedor
	 Where rv.IDCab=@IDCab And (Pr.IDTipoProv='001' Or Pr.IDTipoProv='003')                 
	 And rv.Anulado = 0                                        
 ) As X                                        
 WHERE X.IDTipoProv='001' OR (X.IDTipoProv='003' And X.OperadorConAlojam>=1)                            
 Order by cast(X.FechaIn as smalldatetime)                 
END
