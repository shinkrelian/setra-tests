﻿--HLF-20131206-Comentando --and Orig.CapacidadHab = Dest.CapacidadHab      
--HLF-20150528-And orig.Anulado=0
--,IDReserva_DetOrigAcomVehi=orig.IDReserva_DetOrigAcomVehi
--Case When Orig.Transfer=0 Then ....
--JRF-20160413-Habilitar Día
CREATE Procedure dbo.RESERVAS_DET_UpdxAcomodoTMP      
 @IDReserva int,      
 @IDReserva_Det int,      
 @UserMod char(4)      
 As      
 Set Nocount On      
       
 UPDATE RESERVAS_DET Set       
       [Dia] = Case When Convert(char(5),Orig.Dia,108) = '00:00' Then Dest.[Dia] else Orig.Dia End
      ,[DiaNombre] = Orig.DiaNombre      
      ,[FechaOut] = Orig.FechaOut      
      ,[Noches] = Orig.Noches      
      ,[IDubigeo] = Orig.IDubigeo      
      ,[IDServicio] = Orig.IDServicio      
      ,[IDServicio_Det] = Orig.IDServicio_Det      
      ,[IDIdioma] = Orig.IDIdioma      
      ,[Especial] = Orig.Especial      
      ,[MotivoEspecial] = Orig.MotivoEspecial      
      ,[RutaDocSustento] = Orig.RutaDocSustento      
      ,[Desayuno] = Orig.Desayuno      
      ,[Lonche] = Orig.Lonche      
      ,[Almuerzo] = Orig.Almuerzo      
      ,[Cena] = Orig.Cena      
      ,[Transfer] = Orig.Transfer      
      ,[IDDetTransferOri] = Orig.IDDetTransferOri      
      ,[IDDetTransferDes] = Orig.IDDetTransferDes      
      ,[CamaMat] = Orig.CamaMat      
      ,[TipoTransporte] = Orig.TipoTransporte      
      ,[IDUbigeoOri] = Orig.IDUbigeoOri      
      ,[IDUbigeoDes] = Orig.IDUbigeoDes      
      ,[NroPax] = Orig.NroPax      
      ,[NroLiberados] = Orig.NroLiberados      
      ,[Tipo_Lib] = Orig.Tipo_Lib      
      ,[Cantidad] = Orig.Cantidad      
      ,[CantidadAPagar] = Orig.CantidadAPagar      
      ,[CostoReal] = Orig.CostoReal      
      ,[CostoRealAnt] = Orig.CostoRealAnt      
      ,[CostoLiberado] = Orig.CostoLiberado      
      ,[Margen] = Orig.Margen      
      ,[MargenAplicado] = Orig.MargenAplicado      
      ,[MargenAplicadoAnt] = Orig.MargenAplicadoAnt      
      ,[MargenLiberado] = Orig.MargenLiberado      
      ,[Total] = Orig.Total      
      ,[TotalOrig] = Orig.TotalOrig      
      ,[CostoRealImpto] = Orig.CostoRealImpto      
      ,[CostoLiberadoImpto] = Orig.CostoLiberadoImpto      
      ,[MargenImpto] = Orig.MargenImpto      
      ,[MargenLiberadoImpto] = Orig.MargenLiberadoImpto      
      ,[TotImpto] = Orig.TotImpto      
      ,[NetoHab] = Orig.NetoHab      
      ,[IgvHab] = Orig.IgvHab      
      ,[TotalHab] = Orig.TotalHab      
      ,[NetoGen] = Orig.NetoGen      
      ,[IgvGen] = Orig.IgvGen      
      ,[TotalGen] = Orig.TotalGen      
      ,[IDDetRel] = Orig.IDDetRel      
	  --,[Servicio] = Case When Orig.Transfer=0 Then Orig.Servicio Else Dest.Servicio End            
      ,[Servicio] = 
	  Case When Orig.Transfer=0 Then 
		Orig.Servicio 
	  Else 
		Case When Orig.IDReserva_DetOrigAcomVehi is null then
			Dest.Servicio 
		Else
			Orig.Servicio 	
		End            
	  End
      ,[IDReserva_DetCopia] = Orig.IDReserva_DetCopia      
      ,[IDReserva_Det_Rel] = Orig.IDReserva_Det_Rel      
      ,[IDEmailRef] = Orig.IDEmailRef      
      ,[IDEmailEdit] = Orig.IDEmailEdit      
      ,[IDEmailNew] = Orig.IDEmailNew      
      ,[CapacidadHab] = Orig.CapacidadHab      
      ,[EsMatrimonial] = Orig.EsMatrimonial      
      ,[UserMod] = @UserMod      
      ,[FecMod] = getdate()      
      ,[IDMoneda] = Orig.IDMoneda      
      ,[IDGuiaProveedor] = Orig.IDGuiaProveedor      
      ,[NuVehiculo] = Orig.NuVehiculo      
      ,[ExecTrigger] = 0      
      ,[Anulado] = Orig.Anulado      
      ,[RegeneradoxAcomodo] = 1      
      ,[IDReserva_DetAcomodoTMP] = null      
      ,FeUpdateRow=Null
      ,IDReserva_DetOrigAcomVehi=orig.IDReserva_DetOrigAcomVehi
 FROM RESERVAS_DET Dest Inner Join RESERVAS_DET_TMP Orig On       
 Orig.IDReserva_DetReal=Dest.IDReserva_Det And Orig.IDReserva_DetReal=@IDReserva_Det       
 --and Orig.CapacidadHab = Dest.CapacidadHab      
 WHERE Dest.IDReserva=@IDReserva And orig.Anulado=0
