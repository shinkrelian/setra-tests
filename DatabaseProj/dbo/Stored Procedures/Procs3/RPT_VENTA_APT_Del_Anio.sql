﻿
-------------------------------------------------------------------------------------------------------

--CREATE
CREATE PROCEDURE RPT_VENTA_APT_Del_Anio
	@NuAnio char(4)=''
AS
BEGIN
	Delete dbo.RPT_VENTA_APT
	Where 
		NuAnio = @NuAnio
END;
