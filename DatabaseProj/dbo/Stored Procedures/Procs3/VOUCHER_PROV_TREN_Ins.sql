﻿Create Procedure dbo.VOUCHER_PROV_TREN_Ins
	@IDCab int,
	@CoProveedor char(6),
	@FeVoucher smalldatetime,

	@CoMoneda char(3) ,
	@SsImpuestos numeric(8,2) ,
	@SsTotal numeric(8,2) ,
	
	@CoUbigeo_Oficina char(6),
	@UserMod char(4),
	@pNuVouPTrenInt int output
As
	Set Nocount on

	
	Declare @NuVouPTrenInt int	
	Exec Correlativo_SelOutput 'VOUCHER_PROV_TREN',1,10,@NuVouPTrenInt output           

	Insert Into VOUCHER_PROV_TREN
	(NuVouPTrenInt,	
	IDCab,
	CoProveedor,
	FeVoucher,
	CoMoneda,
	SsImpuestos,
	SsTotal,
	SsSaldo,
	SsDifAceptada,	
	UserMod)
	Values
	(@NuVouPTrenInt,	
	@IDCab,
	@CoProveedor,
	Case When @FeVoucher='01/01/1900' Then getdate() Else @FeVoucher End,
	@CoMoneda,
	@SsImpuestos,
	@SsTotal,
	@SsTotal,
	0,	
	@UserMod)
	
	Set @pNuVouPTrenInt=@NuVouPTrenInt
	
