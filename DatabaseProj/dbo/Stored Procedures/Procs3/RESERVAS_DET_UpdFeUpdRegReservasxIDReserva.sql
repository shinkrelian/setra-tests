﻿Create Procedure dbo.RESERVAS_DET_UpdFeUpdRegReservasxIDReserva
@IDReserva int,
@UserMod char(4)
As
Set NoCount On
Update RESERVAS_DET
	Set FeUpdateRow = Null,
		UserMod=@UserMod
Where IDReserva=@IDReserva
