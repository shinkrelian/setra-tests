﻿--JHD-20160929 - u.Usuario as Nombre
--JHD-20160929 - ,cl.Notas as Comentario
--JHD-20160929 - ,cl.RazonComercial as Cuenta
--JHD-20160929 - ,Tipo=case when Tipo='CL' THEN 'C' when Tipo='CO' then 'CO' when Tipo='WB' then 'B' when tipo='IT' then 'I'  when tipo='Z' then 'Z' ELSE '' end
--JHD-20160929 - and cl.Tipo<>'CO'
--JHD-20160929 - ,Tipo= case when c.CoTipoVenta='DR' THEN 'Z' else cl.tipo end
--JHD-20160929 - Se agrego filtro de usuarios
--PPMG-20160321-, C.DatoSeg as Comentario
--JRF-20160331- ...DATEDIFF(SECOND,c.fecha,c.FeOKClienteAddIn)
--JHD-20160416- ,Revenue= isnull(isnull( (Select top 1  ISNULL( Sum(dq.Total),0)*(cq.Pax+isnull(cq.Liberados,0)) as Revenue ....
CREATE procedure [dbo].[Rpt_Concretizacion]
@Fecha1 smalldatetime,
@Fecha2 smalldatetime,
@strCadena VARCHAR(max)
as

--declare
--@Fecha1 smalldatetime='01/04/2016',
--@Fecha2 smalldatetime='15/04/2016',
--@strCadena VARCHAR(max)=''

begin
Set NoCount On
declare @strcadena1 as varchar(max)=@strCadena
if @strCadena<>''
begin
	
	declare @usuarios as table
(idusuario char(4))

	DECLARE
 -- @strCadena VARCHAR(255),
  @strValor VARCHAR(6),
  @intBandera BIT,
  @intTamano SMALLINT
 
--SET @strCadena = @lstUsuarios
SET @intBandera = 0
 
 
WHILE @intBandera = 0
BEGIN
  BEGIN TRY
    SET @strValor = RIGHT(LEFT(@strCadena,CHARINDEX(',', @strCadena,1)-1),CHARINDEX(',', @strCadena,1)-1)
    PRINT @strValor /* En esta variable se guarda un número */
    SET @intTamano = LEN(@strValor) 
 
    SET @strCadena = SUBSTRING(@strCadena,@intTamano + 2, LEN(@strCadena)) 

	insert into @usuarios values (@strValor)

	--select @strCadena,@strValor
  END TRY
  BEGIN CATCH 
  insert into @usuarios values (@strCadena)
    PRINT @strCadena
    SET @intBandera = 1
  END CATCH 
END
end

select 
Cotizacion,idfile as 'File',
FechaIn,
nombre as Ejecutivo,
--Fecha_file as 'Fecha - File',

Fecha as Fecha_Coti,

isnull(
convert(varchar(15), cast(round((segundos/86400),0,1) as int)   ) + ':' + 
convert(varchar(15), cast(round(((segundos%86400)/3600),0,1) as int)  ) + ':'+
convert(varchar(15),  cast(round((((segundos%86400)%3600)/60),0,1) as int)   ) + ':'+
convert(varchar(15), cast(round((((segundos%86400)%3600)%60),0,1) as int)  )
,'') as 'Tiempo Rpta'
--,'') as 'DD:HH:MM:SS'

,Margen
--,Tipo=case when Tipo='CL' THEN 'Cliente' when Tipo='CO' then 'Counter' when Tipo='WB' then 'Web' when tipo='IT' then 'Interesado' ELSE '' end
,Tipo=case when Tipo='CL' THEN 'C' when Tipo='CO' then 'CO' when Tipo='WB' then 'B' when tipo='IT' then 'I'  when tipo='Z' then 'Z' ELSE '' end
,Cuenta
,Pais

,Estado as Status

--,diferencia=dbo.fnCalcularTiempo(fecha,fecha_file,'1')

,Comentario
,Orden_Status= case when estado='P' THEN 0 WHEN Estado='A' THEN 1 ELSE 2 END
,segundos
,Revenue

 from
(
select 

--u.Nombre+ ' ' +isnull(u.TxApellidos,'') as Nombre
u.Usuario as Nombre

,idcab,cotizacion,idfile,

fecha,

(select  Min(cd.Dia) from cotidet cd where cd.IDCAB=c.IDCAB) as FechaIn,

Fecha_Pedido=(select FePedidoEnviado from pedido p where p.NuPedInt=c.NuPedInt),

segundos= Case When c.FeOKClienteAddIn is not null then IsNull(DATEDIFF(SECOND,c.FeOKClienteAddIn,c.fecha),0000.0000) else 0000.0000 End
--(case when (idfile is not null or ltrim(rtrim(idfile))='') then
			--DATEDIFF(SECOND,c.fecha,((select FePedidoEnviado from pedido p where p.NuPedInt=c.NuPedInt)))
			--DATEDIFF(SECOND,c.fecha,c.FeOKClienteAddIn)
			--else 0000.0000 end) 

,c.Estado


--,cl.Tipo
,Tipo= case when c.CoTipoVenta='DR' THEN 'Z' else cl.tipo end

,Pais=(select descripcion from maubigeo where IDubigeo= cl.IDCiudad)
--,c.ObservacionVentas as Comentario
--,cl.Notas as Comentario
, C.DatoSeg as Comentario
,isnull(c.Margen,0) as Margen
,cl.RazonComercial as Cuenta


--,Revenue= isnull(case when (select count(*) from COTICAB_QUIEBRES cq where cq.IDCAB=c.IDCAB)>0 
--		  then (Select top 1  ISNULL( Sum(dq.Total),0)*(cq.Pax+isnull(cq.Liberados,0)) as Revenue
--				From COTICAB_QUIEBRES cq Left Join COTIDET_QUIEBRES dq On dq.IDCAB_Q=cq.IDCAB_Q      
--				Where cq.IDCAB=c.IDCAB Group by cq.IDCAB_Q, cq.Pax, cq.Liberados, cq.Tipo_Lib  order by cq.Pax)
--		  else (select sum(total)*(c.NroPax+isnull(c.NroLiberados,0)) from cotidet cd where IDCAB=c.IDCab)
--		 end,0)


,Revenue= isnull(isnull( (Select top 1  ISNULL( Sum(dq.Total),0)*(cq.Pax+isnull(cq.Liberados,0)) as Revenue
				From COTICAB_QUIEBRES cq Left Join COTIDET_QUIEBRES dq On dq.IDCAB_Q=cq.IDCAB_Q      
				Where cq.IDCAB=c.IDCAB Group by cq.IDCAB_Q, cq.Pax, cq.Liberados, cq.Tipo_Lib  order by cq.Pax)	,
			(select sum(cd.total)*(c.NroPax+isnull(c.NroLiberados,0)) from cotidet cd where IDCAB=c.IDCab)),0)

from coticab c
left join MAUSUARIOS u on c.IDUsuario=u.IDUsuario
left join maclientes cl on cl.IDCliente=c.IDCliente
--where year(c.Fecha)=2015
--where (c.fecha Between @Fecha1 And @Fecha2 Or Convert(Char(10),@Fecha1,103)='01/01/1900')
where ((c.fecha >= @Fecha1 And c.fecha<@Fecha2+1) Or Convert(Char(10),@Fecha1,103)='01/01/1900')
--and (c.idfile is not null or rtrim(ltrim(c.IDFile))='')
and cl.Tipo<>'CO'

and ((@strcadena1='' and u.Activo='A') or u.IDUsuario in (select IDUsuario from @usuarios))
)
as x
--
--where Fecha_Pedido is not null
--
order by fechain--ejecutivo,cotizacion,fecha
end;
