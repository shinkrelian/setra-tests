﻿ 
 CREATE Procedure dbo.RESERVAS_Sel_AnuladoOutput
	@IDReserva int,        
	@pAnulado bit Output        
As        
	Set Nocount On        

	Select @pAnulado=Anulado From  RESERVAS Where IDReserva=@IDReserva
	
	Set @pAnulado=Isnull(@pAnulado,0)

