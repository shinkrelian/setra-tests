﻿
Create Procedure dbo.VOUCHER_PROV_TREN_SelIDxIDCabProveeOutput
	@IDCab int,
	@CoProveedor char(6),
	@pNuVouPTrenInt int output	
As
	Set Nocount on

	Set @pNuVouPTrenInt=0
	
	SELECT @pNuVouPTrenInt=NuVouPTrenInt FROM VOUCHER_PROV_TREN Where IDCab=@IDCab And CoProveedor=@CoProveedor

