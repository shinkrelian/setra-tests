﻿CREATE procedure VOUCHER_OPERACIONES_Upd_Estado_OP
--declare @idvoucher int=435201
@idvoucher int
as
BEGIN
declare @total_voucher numeric(8,2)
declare @total_docs_op numeric(9,2)
set @total_voucher=(select SsMontoTotal from VOUCHER_OPERACIONES where IDVoucher=@idvoucher)
set @total_docs_op= (select sum (ssmontototal) from ordenpago where IDOrdPag in
(select distinct		op2.IDOrdPag
from 
									ordenpago op2
									inner join reservas r2 on op2.IDReserva=r2.IDReserva
									inner join OPERACIONES o2 on o2.IDReserva=r2.IDReserva
									inner join OPERACIONES_DET od2 on od2.IDOperacion=o2.IDOperacion --and od2.IDVoucher=435201
									inner join RESERVAS_DET rd on rd.IDReserva=r2.IDReserva
									and od2.IDReserva_Det=rd.IDReserva_Det
									inner join DOCUMENTO_PROVEEDOR dc on dc.CoOrdPag=op2.IDOrdPag
									where od2.IDVoucher=435201 /*and op2.IDOrdPag in (select IDOrdPag from DOCUMENTO_PROVEEDOR where FlActivo=1)*/ )
)

--select @total_voucher,@total_docs_op
if @total_voucher=@total_docs_op
begin
	update VOUCHER_OPERACIONES set SsSaldo=@total_voucher-@total_docs_op, CoEstado='AC' where IDVoucher=@idvoucher
end
else
begin
	update VOUCHER_OPERACIONES set SsSaldo=@total_voucher-@total_docs_op where IDVoucher=@idvoucher
end
END;
