﻿--JRF-20150126-...And Anulado=0
CREATE Procedure dbo.RESERVAS_Sel_ExistenxIDCabOutput
 @IDCab int,  
 @pbOk bit Output  
As  
 Set Nocount On  
 Set @pbOk = 0  
 If Exists(Select IDReserva From RESERVAS Where IDCab=@IDCab And Anulado=0)
  Set @pbOk = 1  
