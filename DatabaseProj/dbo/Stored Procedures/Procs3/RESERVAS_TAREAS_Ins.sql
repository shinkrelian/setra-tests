﻿CREATE Procedure [dbo].[RESERVAS_TAREAS_Ins]
	@IDReserva int,	
	@IDTarea	smallint,
	@IDEstado	char(2),
	@FecDeadLine smalldatetime,	
	@IDCab	int,
	@Descripcion	varchar(200),
	@UltimoMinuto	bit,
	@UserMod char(4)
As
	Set NoCount On
	--Declare @IDTarea tinyint=Isnull((Select Max(IDTarea) From RESERVAS_TAREAS Where IDReserva=@IDReserva),0)+1
	
	Insert Into RESERVAS_TAREAS (IDReserva, IDTarea, IDEstado, FecDeadLine, IDCab, 
		Descripcion, UltimoMinuto, UserMod)
	Values(@IDReserva, @IDTarea, @IDEstado, @FecDeadLine, @IDCab, 
		Case When ltrim(rtrim(@Descripcion))='' Then Null Else @Descripcion End, 
		@UltimoMinuto, @UserMod)
