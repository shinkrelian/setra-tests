﻿--HLF-20160303-CantidadAPagar=(Select COUNT(*) From RESERVAS_DET_PAX Where IDReserva_Det=@IDReserva_Det),
CREATE Procedure [dbo].[RESERVAS_DET_Upd_NroPax2]    
 @IDReserva_Det int,    
 @UserMod char(4)    
As    
 Set NoCount On    
     
 Update RESERVAS_DET Set   
    NroPax=(Select COUNT(*) From RESERVAS_DET_PAX Where IDReserva_Det=@IDReserva_Det)
     	  -ISNULL(NroLiberados,0),
	CantidadAPagar=(Select COUNT(*) From RESERVAS_DET_PAX Where IDReserva_Det=@IDReserva_Det),     	  
    UserMod=@UserMod, FecMod=GETDATE()     
 Where IDReserva_Det=@IDReserva_Det 