﻿Create Procedure [dbo].[RESERVAS_TAREAS_Upd_UltimoMinuto]
	@IDReserva	int,
	@IDTarea	smallint,
	@IDCab	int,
	@UserMod	char(4)
As
	Set NoCount On
	
	Update RESERVAS_TAREAS Set UltimoMinuto=1, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva=@IDReserva And IDTarea=@IDTarea And IDCab=@IDCab
