﻿CREATE Procedure dbo.TICKET_AMADEUS_TAX_Del
@CoTicket char(13),
@NuTax tinyint
As
Set NoCount On
Delete from TICKET_AMADEUS_TAX
 WHERE [CoTicket] = @CoTicket
       and [NuTax] = @NuTax

