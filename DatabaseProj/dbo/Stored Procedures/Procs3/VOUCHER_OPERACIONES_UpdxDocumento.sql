﻿--HLF-20150108-Case When @FlDesdeOtraForEgreso=0 Then...
CREATE Procedure [dbo].[VOUCHER_OPERACIONES_UpdxDocumento]  
@IDVoucher int,  
@CoEstado char(2),  
@SsSaldo numeric(8,2),  
@SsDifAceptada numeric(8,2),  
@TxObsDocumento varchar(Max),  
@FlDesdeOtraForEgreso bit,
@UserMod char(4) ,
@CoMoneda_FEgreso char(3)='' 
As  
BEGIN  
Set NoCount On  
Update VOUCHER_OPERACIONES  
 Set CoEstado=@CoEstado,  
  SsSaldo= Case When @SsSaldo=0 then Null Else @SsSaldo End,  
  SsDifAceptada= Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,  
  --TxObsDocumento=Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
  TxObsDocumento= 
	Case When @FlDesdeOtraForEgreso=0 Then
		Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
	Else
		TxObsDocumento
	End,
  UserMod=@UserMod,  
  FecMod=GETDATE(),  
  UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end  
Where IDVoucher= @IDVoucher  

declare @IDOrdPag int =0

--set @IDOrdPag =(select  op.IDOrdPag
--					from VOUCHER_OPERACIONES vo
--				inner join OPERACIONES_DET od on vo.IDVoucher=od.IDVoucher
--				inner join operaciones o on o.IDOperacion=od.IDOperacion
--				inner join MAPROVEEDORES p on o.IDProveedor=p.IDProveedor
--				where p.IDProveedor=@CoProveedor and vo.IDVoucher<>0
--				and o.IDReserva is not null
--							) as v
--				inner join ordenpago op on v.IDReserva=op.IDReserva
--				inner join DOCUMENTO_PROVEEDOR d on d.CoOrdPag=op.IDOrdPag
--				inner join MATIPODOC td on d.CoTipoDoc=td.IDtipodoc
--				inner join MAPROVEEDORES p on p.IDProveedor=d.CoProveedor
--				inner join mamonedas m on m.IDMoneda=d.CoMoneda
--					where --d.CoObligacionPago in ('VOU')
--					--and 
--					d.FlActivo=1
--				)


if rtrim(ltrim(@CoMoneda_FEgreso))<>''
	BEGIN
			Update VOUCHER_OPERACIONES_DET  
			 Set CoEstado=@CoEstado,  
			  SsSaldo= Case When @SsSaldo=0 then Null Else @SsSaldo End,  
			  SsDifAceptada= Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,  
			  --TxObsDocumento=Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
			  TxObsDocumento= 
				Case When @FlDesdeOtraForEgreso=0 Then
					Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
				Else
					TxObsDocumento
				End,
			  UserMod=@UserMod,  
			  FecMod=GETDATE()--,  
			 -- UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end  
			Where IDVoucher= @IDVoucher  AND COMONEDA=@CoMoneda_FEgreso
	END

END;  
