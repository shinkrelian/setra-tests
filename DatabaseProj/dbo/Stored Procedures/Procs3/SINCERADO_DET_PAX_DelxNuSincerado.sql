﻿Create Procedure dbo.SINCERADO_DET_PAX_DelxNuSincerado
@NuSincerado int
As
Set NoCount On
Delete from SINCERADO_DET_PAX
where NuSincerado_Det In (Select NuSincerado_Det from SINCERADO_DET 
						  where NuSincerado = @NuSincerado)
