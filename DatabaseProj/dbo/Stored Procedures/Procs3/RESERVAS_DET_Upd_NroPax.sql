﻿CREATE Procedure [dbo].[RESERVAS_DET_Upd_NroPax]
	@IDCab	int,
	@IDProveedor	char(6),
	@Servicio varchar(max),	
	
	
	@NroPax	smallint,
	@IDDet	int,
	@UserMod	char(4)
As
	Set NoCount On
	
	Update RESERVAS_DET Set 	
	NroPax=@NroPax, UserMod=@UserMod, FecMod=GETDATE()
	From RESERVAS_DET d Inner Join RESERVAS c On d.IDReserva=c.IDReserva And c.IDcab=@IDCab
	Where c.IDProveedor=@IDProveedor And d.Servicio=@Servicio And c.IDcab=@IDCab
	And d.IDDet=@IDDet
