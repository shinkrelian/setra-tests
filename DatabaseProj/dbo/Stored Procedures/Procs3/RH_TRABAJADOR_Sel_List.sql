﻿

CREATE PROCEDURE [dbo].[RH_TRABAJADOR_Sel_List]
	@CoTrabajador char(11)='',
	@NoTrabajador varchar(150)='',
	@FlActivo bit=0
as
SELECT		  RH_TRABAJADOR.CoTrabajador,
			  RH_TRABAJADOR.NoApPaterno,
			  RH_TRABAJADOR.NoApMaterno,
			  RH_TRABAJADOR.NoNombres,
			  NoTrabajador=NoApPaterno+' '+NoApMaterno+', '+NoNombres,
			  RH_TRABAJADOR.CoTipoDoc, 
              MATIPOIDENT.Descripcion,
              RH_TRABAJADOR.NuNumeroDoc,
              RH_TRABAJADOR.FlActivo--,
              --RH_TRABAJADOR.UserMod,
              --RH_TRABAJADOR.FecMod
FROM         RH_TRABAJADOR INNER JOIN
                      MATIPOIDENT ON RH_TRABAJADOR.CoTipoDoc = MATIPOIDENT.IDIdentidad
WHERE     
		--(ltrim(rtrim(@CoTrabajador))='' Or CoTrabajador like '%' +@CoTrabajador + '%') and  
		(ltrim(rtrim(@CoTrabajador))='' Or CoTrabajador like '%' +@CoTrabajador + '%') and
		 (FlActivo = @FlActivo or @FlActivo =0) AND
		(ltrim(rtrim(@NoTrabajador))='' Or (NoApPaterno+' '+NoApMaterno+', '+NoNombres) like '%' +@NoTrabajador + '%')
