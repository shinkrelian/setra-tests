﻿
------------------------------------------------------------------------------------------

create PROCEDURE RH_HORARIO_Upd
	@CoHorario tinyint,
	@NoHorario varchar(150)='',
	@UserMod char(4)='',
	@FeHorasRefrigerio time,
	@NuMaxHorasSemana tinyint
AS
BEGIN
	Update dbo.RH_HORARIO
	Set
		NoHorario = Case When ltrim(rtrim(@NoHorario))='' Then Null Else @NoHorario End,
 		UserMod = 	Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End,
 		FeHorasRefrigerio = @FeHorasRefrigerio,
 		NuMaxHorasSemana = @NuMaxHorasSemana
 	Where 
		CoHorario = @CoHorario
END
