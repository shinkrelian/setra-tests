﻿--HLF-20140114-And d.Anulado=0
--JRF-20150625-Solo servicios de Prov. Interno
CREATE Procedure [dbo].[RESERVAS_DET_UpdMontosxNroPax]  
 @IDCab int,  
 @IDDet int,  
 @UserMod char(4)  
As  
 Set NoCount On  
   
 Update RESERVAS_DET Set    
 CostoReal=d.CostoReal*Cantidad,  
 CostoRealAnt=d.CostoRealAnt*Cantidad,  
 CostoLiberado=d.CostoLiberado*Cantidad,  
 Margen=d.Margen*Cantidad,  
 MargenLiberado=d.MargenLiberado*Cantidad,  
 Total=d.Total*Cantidad,  
 TotalOrig=d.TotalOrig*Cantidad,  
 CostoRealImpto=d.CostoRealImpto*Cantidad,  
 CostoLiberadoImpto=d.CostoLiberadoImpto*Cantidad,  
 MargenImpto=d.MargenImpto*Cantidad,
 MargenLiberadoImpto=d.MargenLiberadoImpto*Cantidad,  
 TotImpto=d.TotImpto*Cantidad,  
 UserMod=@UserMod, FecMod=GETDATE()  
 From RESERVAS_DET d Left Join RESERVAS r On r.IDReserva=d.IDReserva
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
 Where d.IDReserva In (Select IDReserva From RESERVAS r2 
					   Left Join MAPROVEEDORES p4 On r.IDProveedor=p4.IDProveedor Where r2.IDCab=@IDCab And p4.IDTipoOper='I')
 And IsNull(d.Cantidad,0) > 1 And d.IDDet=@IDDet  
 And d.Anulado=0 And p.IDTipoOper='I'
