﻿
---------------------------------------------------------------------------------------------------------

--CREATE
CREATE PROCEDURE RPT_VENTA_CLIENTES_Del_Anio
	@NuAnio char(4)=''
AS
BEGIN
	Delete dbo.RPT_VENTA_CLIENTES
	Where 
		NuAnio = @NuAnio
END;
