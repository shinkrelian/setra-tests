﻿
--HLF-20150901-isnull(d.CoCtroCosto,'940101') as CostingCode,
--'000000' as CostingCode2,
CREATE Procedure dbo.SAP_DOCUMENTO_DET_TEXTO_Sel_Fk
	@IDTipoDoc char(3),
	@NuDocum char(10)
As
	Set nocount on
	Declare @NuIGV numeric(5,2)=(Select NuIGV From PARAMETRO)

	Select dt.NoTexto as ItemDescription, dt.SsTotal as LineTotal, 
		Case When d.ssIGV=0 Then 0 Else dt.SsTotal*(@NuIGV/100) End as TaxTotal,
		Case When d.ssIGV=0 Then 'EXE_IGV' Else 'IGV' End as TaxCode,		
		--Case When d.ssIGV=0 Then '' Else 'IGV' End as TaxCode,		
		isnull(d.CoCtaContable,'00000000') as AccountCode,
		--isnull(d.CoCtroCosto,'000000') as CostingCode,
		isnull(d.CoCtroCosto,'940101') as CostingCode,
		--'999999' as CostingCode2,
		'000000' as CostingCode2,
		isnull(cc.IDFile,'00000000') as CostingCode3
	From
	DOCUMENTO_DET_TEXTO dt Inner Join DOCUMENTO d On d.IDTipoDoc=dt.IDTipoDoc And d.NuDocum=dt.NuDocum
	Left Join COTICAB cc On d.IDCab=cc.IDCAB
	Where dt.IDTipoDoc=@IDTipoDoc And dt.NuDocum=@NuDocum

