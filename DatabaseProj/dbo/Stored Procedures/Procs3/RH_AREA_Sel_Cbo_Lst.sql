﻿CREATE PROCEDURE [dbo].[RH_AREA_Sel_Cbo_Lst]
@Todos bit
AS
BEGIN
Select '' as CoArea,'<TODOS>' as Descripcion
Where @Todos = 1
Union
	Select
		[CoArea],
 		[NoArea] as Descripcion
 	
 	From [dbo].[RH_AREA]
	Where 
		 (FlActivo =1) 
END;
