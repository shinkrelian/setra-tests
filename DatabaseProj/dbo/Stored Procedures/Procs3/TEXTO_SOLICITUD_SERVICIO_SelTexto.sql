﻿CREATE PROCEDURE DBO.TEXTO_SOLICITUD_SERVICIO_SelTexto
	@IDProveedor char(6) ,
	@CoTipoProv char(3) 

As
	Set Nocount on
	
	Select TxTexto From TEXTO_SOLICITUD_SERVICIO 
	Where CoPais=(SELECT PA.IDubigeo FROM MAPROVEEDORES P 
		INNER JOIN MAUBIGEO C ON C.IDubigeo=P.IDCiudad
		INNER JOIN MAUBIGEO PA ON C.IDPais=PA.IDubigeo
		WHERE P.IDProveedor=@IDProveedor)
	And CoTipoProv=@CoTipoProv
	
