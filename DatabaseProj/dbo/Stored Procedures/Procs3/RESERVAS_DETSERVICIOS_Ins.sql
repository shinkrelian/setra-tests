﻿--JRF-20140228-Aumento de Longitud para las columnas con numeric(12,4) 
--JRF-20150221-Considerar cambios de Columna al agregar un NoShow
CREATE Procedure [dbo].[RESERVAS_DETSERVICIOS_Ins]  
@IDReserva_Det int,  
@IDServicio_Det int,  
@CostoReal numeric(12,4),  
@CostoLiberado numeric(12,4),  
@Margen numeric(12,4), 
@SSCR numeric(12,4),
@SSCL numeric(12,4),
@SSMargen numeric(12,4),
@SSMargenLiberado numeric(12,4),
@SSTotal numeric(12,4),
@STCR numeric(12,4),
@STMargen numeric(12,4),
@STTotal numeric(12,4),
@SSCRImpto numeric(12,4),
@SSCLImpto numeric(12,4),
@SSMargenImpto numeric(12,4),
@SSMargenLiberadoImpto numeric(12,4),
@STCRImpto numeric(12,4),
@STMargenImpto numeric(12,4),
@MargenAplicado numeric(6,2),  
@MargenLiberado numeric(6,2),  
@Total numeric(8,2),  
@CostoRealImpto numeric(12,4),  
@CostoLiberadoImpto numeric(12,4),  
@MargenImpto numeric(12,4),  
@MargenLiberadoImpto numeric(12,4),  
@TotImpto numeric(12,4),  
@UserMod char(4)  
As  
 Set NoCount On  
INSERT INTO [dbo].[RESERVAS_DETSERVICIOS]
           ([IDReserva_Det]
           ,[IDServicio_Det]
           ,[CostoReal]
           ,[CostoLiberado]
           ,[Margen]
           ,[MargenAplicado]
           ,[MargenLiberado]
           ,[Total]
           ,[SSCR]
           ,[SSCL]
           ,[SSMargen]
           ,[SSMargenLiberado]
           ,[SSTotal]
           ,[STCR]
           ,[STMargen]
           ,[STTotal]
           ,[CostoRealImpto]
           ,[CostoLiberadoImpto]
           ,[MargenImpto]
           ,[MargenLiberadoImpto]
           ,[SSCRImpto]
           ,[SSCLImpto]
           ,[SSMargenImpto]
           ,[SSMargenLiberadoImpto]
           ,[STCRImpto]
           ,[STMargenImpto]
           ,[TotImpto]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@IDReserva_Det
           ,@IDServicio_Det
           ,@CostoReal
           ,@CostoLiberado
           ,@Margen
           ,@MargenAplicado
           ,@MargenLiberado
           ,@Total
           ,@SSCR
           ,@SSCL
           ,@SSMargen
           ,@SSMargenLiberado
           ,@SSTotal
           ,@STCR
           ,@STMargen
           ,@STTotal
           ,@CostoRealImpto
           ,@CostoLiberadoImpto
           ,@MargenImpto
           ,@MargenLiberadoImpto
           ,@SSCRImpto
           ,@SSCLImpto
           ,@SSMargenImpto
           ,@SSMargenLiberadoImpto
           ,@STCRImpto
           ,@STMargenImpto
           ,@TotImpto
           ,@UserMod
           ,GETDATE())  
