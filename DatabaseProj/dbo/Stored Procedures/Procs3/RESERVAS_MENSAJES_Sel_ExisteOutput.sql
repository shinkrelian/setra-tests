﻿CREATE Procedure [dbo].[RESERVAS_MENSAJES_Sel_ExisteOutput]
	@ConversationID	varchar(255),
	@IDReserva	int,
	@pbExists	bit	output
	
As
	Set NoCount On	
	
	If Exists(Select ConversationID From RESERVAS_MENSAJES Where ConversationID=@ConversationID
		And IDReserva=@IDReserva) 
		
		Set @pbExists = 1
	Else
		Set @pbExists = 0
