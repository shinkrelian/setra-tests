﻿Create Procedure [dbo].[RESERVAS_DETSERVICIOS_DelxIDDet]
	@IDDet	int
As
	Set NoCount On
	
	Delete From RESERVAS_DETSERVICIOS
		Where IDReserva_Det In (Select IDReserva_Det From RESERVAS_DET Where IDDet = @IDDet)
