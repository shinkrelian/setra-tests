﻿--JRF-and Not(FlServicioNoShow=1 and FlServicioIngManual=1)
CREATE Procedure [dbo].[RESERVAS_DETSERVICIOS_DelxIDReserva]  
 @IDReserva int  
As  
  
 Delete From RESERVAS_DETSERVICIOS   
  Where IDReserva_Det In (Select IDReserva_Det From RESERVAS_DET Where IDReserva = @IDReserva and Not(FlServicioNoShow=1 and FlServicioIngManual=1))
