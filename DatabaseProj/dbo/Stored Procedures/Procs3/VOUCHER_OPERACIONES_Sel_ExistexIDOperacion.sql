﻿Create Procedure dbo.VOUCHER_OPERACIONES_Sel_ExistexIDOperacion
@IDOperacion int,
@pExiste bit Output
As
Set NoCount On
Set @pExiste = 0
Set @pExiste = dbo.FnExisteVoucherenOperaciones(@IDOperacion)
