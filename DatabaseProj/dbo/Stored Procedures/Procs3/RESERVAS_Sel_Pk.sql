﻿--Sp_helptext RESERVAS_Sel_Pk 
--Go 

CREATE Procedure [dbo].[RESERVAS_Sel_Pk] 
@IDReservas int    
as    
 Set NoCount On    
 Select  Rv.*,C.RazonComercial as Cliente,u.Nombre UsuarioReservas,uv.Nombre UsuarioVentas,  
   Cc.Cotizacion,Cc.Fecha FecCotizacion,Cc.Estado,Cc.IDUsuario UsuarioCotizador,  
   Cc.FecInicio FechaInicio,Cc.FechaOut FechaLimite,pr.RazonSocial  
     
   from RESERVAS Rv Left Join COTICAB Cc On Rv.IDCab=Cc.IDCAB  
        Left Join MACLIENTES c On Cc.IDCliente=c.IDCliente  
        Left Join MAUSUARIOS u On Rv.IDUsuarioRes=u.IDUsuario  
        Left Join MAUSUARIOS uV On Cc.IDUsuario=uV.IDUsuario  
        Left Join MAPROVEEDORES pr On Rv.IDProveedor=pr.IDProveedor  
  Where IDReserva=@IDReservas
