﻿
Create Procedure dbo.RESERVAS_UpdAnulado
	@IDReserva	int,
	@UserMod char(4),
	@Anulado bit
As
	Set NoCount On
	
	Update RESERVAS Set Anulado=@Anulado,UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva=@IDReserva
