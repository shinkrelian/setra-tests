﻿
CREATE Procedure [dbo].[RESERVAS_SelProveedoresCorreoxPedido]
	@NuPedInt int

As
	
	SELECT * FROM
		(
		--Select 'BE'+cast(r.idcab as nvarchar(10))+'|BEVS' as CoArea ,
		Select 'BEVS' as CoArea ,
		R.IDCliente as CoClienteProvee, p.RazonSocial as DescClienteProvee
		From COTICAB r Inner Join MACLIENTES p On r.IDCliente=p.IDCliente
		Where r.NuPedInt=@NuPedInt
			And r.IDCliente IN (SELECT CoUsrCliDe From .BDCORREOSETRA..CORREOFILE cf inner join COTICAB cc on cf.IDCab=cc.IDCAB				
				 and cc.NuPedInt=@NuPedInt)
		Union
		--Select 'EE'+cast(r.idcab as nvarchar(10))+'|EEVS' as CoArea ,
		Select 'EEVS' as CoArea ,
		R.IDCliente, p.RazonSocial as DescCliente
		From COTICAB r Inner Join MACLIENTES p On r.IDCliente=p.IDCliente
		Where r.NuPedInt=@NuPedInt
					And r.IDCliente IN (SELECT dc1.CoUsrCliPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 inner Join .BDCORREOSETRA..CORREOFILE cf1
				On dc1.NuCorreo=cf1.NuCorreo inner join COTICAB cc on cf1.IDCab=cc.IDCAB				
				 and cc.NuPedInt=@NuPedInt)			
		) AS XCL
	UNION
	SELECT * FROM
		(
		--Select 'BE'+cast(r.idcab as nvarchar(10))+'|BERS' as CoArea ,
		Select 'BERS' as CoArea ,
		p.IDProveedor, p.NombreCorto as DescProveedor
		From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		inner join COTICAB cc on r.IDCab=cc.IDCAB
		Where  cc.NuPedInt=@NuPedInt And r.Anulado=0
			And r.IDProveedor IN (SELECT CoUsrPrvDe From .BDCORREOSETRA..CORREOFILE cf1 inner join COTICAB cc1 on cf1.IDCab=cc1.IDCAB				
				 and cc1.NuPedInt=@NuPedInt)
		Union
		--Select 'EE'+cast(r.idcab as nvarchar(10))+'|EERS' as CoArea ,
		Select 'EERS' as CoArea ,
		p.IDProveedor, p.NombreCorto as DescProveedor
		From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		inner join COTICAB cc on r.IDCab=cc.IDCAB
		Where  cc.NuPedInt=@NuPedInt And r.Anulado=0
			And r.IDProveedor IN (SELECT dc1.CoUsrPrvPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 inner Join .BDCORREOSETRA..CORREOFILE cf1
				On dc1.NuCorreo=cf1.NuCorreo inner join COTICAB cc1 on cf1.IDCab=cc1.IDCAB and cc1.NuPedInt=@NuPedInt)
		Union
		--Select 'BE'+cast(r.idcab as nvarchar(10))+'|BEOP' as CoArea ,
		Select 'BEOP' as CoArea ,
		p.IDProveedor, p.NombreCorto as DescProveedor
		From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		inner join COTICAB cc on r.IDCab=cc.IDCAB
		Where  cc.NuPedInt=@NuPedInt And r.Anulado=0
			And r.IDProveedor IN (SELECT CoUsrPrvDe From .BDCORREOSETRA..CORREOFILE cf1 inner join COTICAB cc1 on cf1.IDCab=cc1.IDCAB and 
			cc1.NuPedInt=@NuPedInt)
		Union
		--Select 'EE'+cast(r.idcab as nvarchar(10))+'|EEOP' as CoArea ,
		Select 'EEOP' as CoArea ,
		p.IDProveedor, p.NombreCorto as DescProveedor
		From OPERACIONES r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
		inner join COTICAB cc on r.IDCab=cc.IDCAB
		Where cc.NuPedInt=@NuPedInt
			And r.IDProveedor IN (SELECT dc1.CoUsrPrvPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 inner Join .BDCORREOSETRA..CORREOFILE cf1
				On dc1.NuCorreo=cf1.NuCorreo inner join COTICAB cc1 on cf1.IDCab=cc1.IDCAB And cc1.NuPedInt=@NuPedInt)

		) AS XPR
	
	Order by 1,3

