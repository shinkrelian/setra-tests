﻿Create Procedure dbo.sysmail_profileaccount_Ins
	@profile_id	int,
	@account_id	int	
	--@UserMod	char(4)
As
	Set NoCount On  
	
	Declare @sequence_number int
	Select 	@sequence_number=isnull(MAX(sequence_number),0)+1 
	From msdb.dbo.sysmail_profileaccount
	Where profile_id=@profile_id And account_id=@account_id
	
	Insert Into msdb.dbo.sysmail_profileaccount
	(profile_id, account_id, sequence_number, last_mod_datetime)
	Values(@profile_id, @account_id,@sequence_number, GETDATE())
