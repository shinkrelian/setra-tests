﻿--HLF-20160114-@QtStkSus=isnull(SUM(QtPax),0)	update STOCK_TICKET_ENTRADAS_INGRESOS  set ...
--HLF-20160119-If @QtStSaldo=0	Select Top 1 @NuTckEntIng=
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_UpdEntradas  
@IDServicio_Det_V_Cot int,  
@IDCab int,
@UserMod char(4)  
As  
Set NoCount On  

Declare @IDServicio char(8)=(select IDServicio from MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det_V_Cot)
Declare @QtUtilizada int=0, @QtComprada int=0
Select @QtUtilizada=isnull(SUM(QtUtilizada),0) from COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS where IDServicio=@IDServicio
Select @QtComprada=isnull(SUM(QttkComprado),0) from STOCK_TICKET_ENTRADAS_INGRESOS where IDServicio=@IDServicio
--Declare @QtStkSus smallint=0--, @QtStkDev smallint=0
--Select --@QtStkDev=isnull(SUM(SsTotDev),0),
--		--@QtStkSus=isnull(SUM(SsTotSus),0) 
--		@QtStkSus=isnull(SUM(QtPax),0) 
--	From PRESUPUESTO_SOBRE_DET pd inner join PRESUPUESTO_SOBRE p
--	On pd.NuPreSob=p.NuPreSob And p.IDCab=@IDCab And pd.IDServicio_Det_V_Cot=@IDServicio_Det_V_Cot


Update STOCK_TICKET_ENTRADAS
set --QtStkEnt=STOCK_TICKET_ENTRADAS.QtEntCompradas-
	QtStkEnt=@QtComprada-
	--ISNULL(
 --   (select SUM(QtPax) from PRESUPUESTO_SOBRE_DET psd Left Join PRESUPUESTO_SOBRE ps On psd.NuPreSob=ps.NuPreSob
 --    Where psd.IDServicio_Det=STOCK_TICKET_ENTRADAS.IDServicio_Det and psd.CoPago='PCM' and ps.CoEstado='EN'),0),
	@QtUtilizada,--+@QtStkDev	,
    UserMod = @UserMod,
	FecMod=GETDATE()	
Where IDServicio = @IDServicio  

--Actualizar STOCK_TICKET_ENTRADAS_INGRESOS.QtStSaldo
Declare @NuTckEntIng smallint=0, @QtStSaldo smallint=0--, @QtStNeto smallint=0
--InicioBusq:

Select Top 1 @NuTckEntIng=NuTckEntIng,@QtStSaldo=QtStSaldo 
From STOCK_TICKET_ENTRADAS_INGRESOS 
	Where IDServicio=@IDServicio And QtStSaldo>0 Order by NuTckEntIng

If @QtStSaldo=0
	Select Top 1 @NuTckEntIng=NuTckEntIng
	From STOCK_TICKET_ENTRADAS_INGRESOS 
	Where IDServicio=@IDServicio Order by NuTckEntIng desc
--If @QtStNeto=0
--	Set @QtStNeto = @QtStSaldo-@QtStkSus---@QtStkDev
--Else
--	Set @QtStNeto = @QtStSaldo-@QtStNeto
--If @QtStNeto >= 0
--	Update STOCK_TICKET_ENTRADAS_INGRESOS Set QtStSaldo=@QtStNeto, FecMod=GETDATE(), UserMod=@UserMod
--	Where IDServicio=@IDServicio And NuTckEntIng=@NuTckEntIng
--Else
--	Begin
--	Update STOCK_TICKET_ENTRADAS_INGRESOS Set QtStSaldo=0, FecMod=GETDATE(), UserMod=@UserMod
--	Where IDServicio=@IDServicio And NuTckEntIng=@NuTckEntIng
--	Set @QtStNeto = Abs(@QtStNeto)
--	goto InicioBusq
--	End

update STOCK_TICKET_ENTRADAS_INGRESOS  set STOCK_TICKET_ENTRADAS_INGRESOS.QtStSaldo = x.QttkComprado-x.QtUtilizada,
	UserMod = @UserMod,	FecMod=GETDATE()	
From
(Select idservicio,
	ISNULL((SELECT sum(QttkComprado) FROM STOCK_TICKET_ENTRADAS_INGRESOS st where st.IDServicio=STOCK_TICKET_ENTRADAS_INGRESOS.IDServicio and NuTckEntIng=@NuTckEntIng),0) as QttkComprado,
	ISNULL((SELECT SUM(QtUtilizada) FROM COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS WHERE IDServicio=STOCK_TICKET_ENTRADAS_INGRESOS.IDServicio and NuTckEntIng=@NuTckEntIng),0) as QtUtilizada
	From STOCK_TICKET_ENTRADAS_INGRESOS Where IDServicio=@IDServicio and NuTckEntIng=@NuTckEntIng
	group by IDServicio) as x 
	inner join STOCK_TICKET_ENTRADAS_INGRESOS on STOCK_TICKET_ENTRADAS_INGRESOS.IDServicio=x.IDServicio And STOCK_TICKET_ENTRADAS_INGRESOS.NuTckEntIng=@NuTckEntIng

