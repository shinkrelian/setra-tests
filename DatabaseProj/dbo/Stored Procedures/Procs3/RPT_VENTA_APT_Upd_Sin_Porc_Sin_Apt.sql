﻿--create
create PROCEDURE [dbo].[RPT_VENTA_APT_Upd_Sin_Porc_Sin_Apt]
	@NuAnio char(4),
	@CoMes char(2),
	@SsImporte_Sin_Porc_Sin_APT numeric(12,4),
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[RPT_VENTA_APT]
	Set
		SsImporte_Sin_Porc_Sin_APT=@SsImporte_Sin_Porc_Sin_APT,
 		[UserMod] = @UserMod,
 		[FecMod] = getdate()
 	Where 
		[NuAnio] = @NuAnio And 
		[CoMes] = @CoMes
END
