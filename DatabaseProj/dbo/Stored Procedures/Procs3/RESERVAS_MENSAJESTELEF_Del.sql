﻿Create Procedure [dbo].[RESERVAS_MENSAJESTELEF_Del]
	@IDReserva int,
	@IDMensaje tinyint
	
As	
	Set NoCount On
	
	Delete From RESERVAS_MENSAJESTELEF
	Where
	IDReserva=@IDReserva And
	IDMensaje=@IDMensaje
