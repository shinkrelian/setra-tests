﻿CREATE Procedure dbo.VOUCHER_OPERACIONES_DET_Upd_Estado_Multiple
@NuVoucher int,
@CoEstado char(2),
@CoMoneda_FEgreso char(3),
@UserMod char(4)
As  
Set NoCount On  
BEGIN

	Update VOUCHER_OPERACIONES
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  
	where IDVoucher=@NuVoucher

	Update VOUCHER_OPERACIONES_DET 
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  
where IDVoucher=@NuVoucher  and CoMoneda=@CoMoneda_FEgreso
END;

