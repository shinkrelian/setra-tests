﻿--HLF-20131029-Agregando IDReservaProtecc, IDProveedorProtecc
--JRF-20150413-Agregando ..., ExisteAlojamiento
CREATE Procedure dbo.RESERVAS_SelProvee  
 @IDCab int  
As      
 Set Nocount On      
  
 SELECT IDProveedor, IDReserva, CodReservaProv, Observaciones, 
 IDReservaProtecc, IDProveedorProtecc ,ExisteAlojamiento
 FROM  
 (  
 Select r.IDProveedor, r.IDReserva,   
 isnull(r.CodReservaProv,'') as CodReservaProv,   
 isnull(r.Observaciones,'') as Observaciones,   
 dbo.FnExistenAlojamiento(r.IDReserva) as ExisteAlojamiento,
 
 isnull(r.IDReservaProtecc,0) as IDReservaProtecc,
 isnull(rp.IDProveedor,'') as IDProveedorProtecc
    
 From RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor    
 Left Join RESERVAS rp On r.IDReservaProtecc=rp.IDReserva
 Where r.IDCab=@IDCab And IsNull(r.Anulado,0)=0 And r.Estado<>'XL'  
 ) AS X  
