﻿CREATE PROCEDURE [dbo].[Rpt_Tarifario_Hoteles]
@anio char(4)='',
@idPais char(6)=''

--@anio char(4)='2016',
--@idPais char(6)='000003'

AS
BEGIN

select distinct

p.IDProveedor,

 up.Descripcion as Pais, u.Descripcion as Ciudad, p.NombreCorto as Proveedor,
s.IDServicio,s.Descripcion as Servicio,
sd.IDServicio_Det,

--case when isnull(cast(sd.DetaTipo as varchar(max)),'')='' then sd.Descripcion
--else  sd.Descripcion + ' - ' + isnull(sd.tipo,'')+'' + ' - '+isnull(cast(sd.DetaTipo as varchar(max)),'') end
--as Detalle_Servicio,

sd.Descripcion as Room_Type,
--isnull(sd.tipo,'')+'' + ' - '+isnull(cast(sd.DetaTipo as varchar(max)),'') as Valid_From_to ,
isnull(cast(sd.DetaTipo as varchar(max)),'') as Valid_From_to ,
 --charindex(')', isnull(cast(sd.DetaTipo as varchar(max)),'')) as Valid_From_to2 ,
 SUBSTRING(isnull(cast(sd.DetaTipo as varchar(max)),''),0,charindex(')', isnull(cast(sd.DetaTipo as varchar(max)),''))+1)  as Valid_From_to1 ,
 
--isnull(sd.Monto_sgl,0) as SWB,
--isnull(sd.Monto_dbl,0) as  DWB,
--isnull(sd.Monto_tri,0) as TPL,

cast(round(isnull(sd.Monto_sgl,0)+(isnull(s.Comision,0)/100)*isnull(sd.Monto_sgl,0),0) as numeric(10,0)) as SWB,
cast(round(isnull(sd.Monto_dbl,0)+(isnull(s.Comision,0)/100)*isnull(sd.Monto_dbl,0),0) as numeric(10,0)) as  DWB,
cast(round(isnull(sd.Monto_tri,0)+(isnull(s.Comision,0)/100)*isnull(sd.Monto_tri,0),0) as numeric(10,0)) as TPL,

E_Bed= case when sd.IdHabitTriple in ('001','006','004') then '*' else '' end,

Breakfast=isnull((select descripcion from MADESAYUNOS where IDDesayuno=sd.tipodesayuno),''),
/*
CB= case when sd.TipoDesayuno='02' then 'INC' ELSE '' END,
AB= case when sd.TipoDesayuno='01' then 'INC' ELSE '' END,
BB= case when sd.TipoDesayuno='03' then 'INC' ELSE '' END,
*/
Categoria=isnull(s.Categoria,''),
--Cat_Setours=isnull((select Descripcion from MACATEGORIA where IDCat=s.IDCat),''),
cast(isnull(s.Comision,0)/100 as decimal(10,2)) as Margen,

Direccion=p.Direccion
,Telefono=+isnull(p.Telefono1,'')+'/ Fax:'+isnull(p.Fax,'')
,Web=isnull(p.Web,''),

Notes=isnull((select top 1 Nombre+' (' + cast(defindia as char(3))+' to '+(case when DefinHasta=999 then 'more' else cast(definhasta as char(4)) end ) +  ( case when DefinTipo='HAB' THEN ' rooms)' when DefinTipo='PAX' THEN ' pax)' ELSE '' end)
from MAPOLITICAPROVEEDORES
where IDProveedor=p.IDProveedor
and nombre in ('FITS','GROUPS')),''),

--Lunch=isnull((select min(sd1.Monto_sgl) from MASERVICIOS_DET sd1 where sd1.IDServicio=s.idservicio and sd1.Descripcion like '%Lunch%' and sd1.tipo not in ('APT','CHD') AND sd1.Anio=@anio),0),
--Dinner=isnull((select min(sd1.Monto_sgl) from MASERVICIOS_DET sd1 where sd1.IDServicio=s.idservicio and sd1.Descripcion like '%Dinner%' and sd1.tipo not in ('APT','CHD') AND sd1.Anio=@anio),0),

Lunch=ROUND(isnull((select min(sd1.Monto_sgl) from MASERVICIOS_DET sd1 where sd1.IDServicio=s.idservicio and sd1.Descripcion like '%Lunch%' and sd1.tipo not in ('APT','CHD') AND sd1.Anio=@anio),0),0),
Dinner=ROUND(isnull((select min(sd1.Monto_sgl) from MASERVICIOS_DET sd1 where sd1.IDServicio=s.idservicio and sd1.Descripcion like '%Dinner%' and sd1.tipo not in ('APT','CHD') AND sd1.Anio=@anio),0),0),

orden=1
from MAPROVEEDORES p
inner join MASERVICIOS s on s.IDProveedor=p.IDProveedor
inner join MASERVICIOS_DET sd on s.IDServicio=sd.IDServicio
inner join MAUBIGEO u on p.IDCiudad=u.IDubigeo
inner join MAUBIGEO up on up.IDubigeo=u.IDPais
where p.IDTipoProv='001'
and up.IDubigeo  = @idPais --in ('000011','000003','000008')
and p.Activo='A' and s.Activo=1 and sd.Anio=@anio

order by  Pais,  Ciudad, Proveedor,orden,Servicio,IDServicio_Det

END;


