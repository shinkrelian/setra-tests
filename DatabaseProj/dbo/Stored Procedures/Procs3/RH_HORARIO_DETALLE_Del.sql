﻿
------------------------------------------------------------------------------------------

CREATE PROCEDURE RH_HORARIO_DETALLE_Del
	@CoHorario tinyint,
	@CoDetalle tinyint
AS
BEGIN
	Delete dbo.RH_HORARIO_DETALLE
	Where 
		CoHorario = @CoHorario And 
		CoDetalle = @CoDetalle
END;
