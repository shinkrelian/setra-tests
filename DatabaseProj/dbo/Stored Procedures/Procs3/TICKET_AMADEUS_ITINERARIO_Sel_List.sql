﻿
--JRF-20140623-Agregar el campo [CoPNR_Amadeus]
--JRF-20140707-Agregar el campo [CoPNR_Aerolinea]
CREATE Procedure dbo.TICKET_AMADEUS_ITINERARIO_Sel_List      
 @CoTicket char(13)      
As      
 Set NoCount On      
   
 --Select CoTicket,NuSegmento,CoCiudadPar,CoCiudadLle,NuVuelo,TxFechaPar,      
 -- TxHoraLle--,ub.Descripcion      
 --from TICKET_AMADEUS_ITINERARIO --tai Left Join MAUBIGEO ub On tai.CoCiudadPar=ub.Codigo      
 --where CoTicket = @CoTicket      
  
 Select it.NuSegmento, it.CoCiudadPar+'/'+it.CoCiudadLle as Itinerario,  
 it.CoLineaAerea+' '+it.NuVuelo as LA_Vlo, it.CoSegmento as Clase,  it.CoPNR_Amadeus,
 it.CoPNR_Aerolinea,
 it.TxFechaPar as Fecha,   
 CONVERT(smalldatetime, substring(CONVERT(varchar,it.TxFechaPar,100),13,5)) as HoraPar,  
 CONVERT(smalldatetime, substring(CONVERT(varchar,it.TxHoraLle,100),13,5)) as HoraLle ,
 it.CoST 
 from TICKET_AMADEUS_ITINERARIO it  
 where CoTicket = @CoTicket    

