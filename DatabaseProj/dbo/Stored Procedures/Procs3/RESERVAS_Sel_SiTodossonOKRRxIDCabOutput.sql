﻿Create Procedure dbo.RESERVAS_Sel_SiTodossonOKRRxIDCabOutput
	@IDCab	int,
	@pbOk	bit Output
As
	Set Nocount On

	If Exists(Select Estado From RESERVAS Where Not Estado In ('OK','RR','XL') And IDCab=@IDCab)
		Set @pbOk = 0
	Else
		Set @pbOk = 1
		
