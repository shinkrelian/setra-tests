﻿


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


--JRF-20140623-Agregando el campo [CoPNR_Amadeus]
--JRF-20140624-Agregando el campo [CoEstado]
--JRF-20140627-Agregando los campos[TxObsCou,CoNacionalidad,CoEjeRec,
--									CoEjeCou,CoEjeCouAmd,TxTituloPax]
--JRF-20140627-Cambiando la longituda al CoDocumIdent[@CoDocumIdent]
--HLF-20140704-@CoTicket char(13), @TxComision numeric(5,2)
--SSComision=(@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
--SsCosto @TxTotal-(@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
--HLF-20140707-Agregando parametro @CoPNR_Aerolinea 
--JHD-20140911-Agregado campo NuNtaVta
--JHD-20141031-Se corrigio el parametro @CoPNR_Aerolinea para que admita 9 caracteres (char(9))
CREATE Procedure dbo.TICKET_AMADEUS_Upd  
@CoTicket char(13),  
@CoAir char(14),  
@CoProvOrigen char(6),  
@CoUsrCrea char(9),  
@CoUsrDuenio char(9),  
@CoUsrEmite char(9),  
@CoIATA char(8),  
@CoLineaAereaRecord char(9),  
@CoLineaAereaSigla char(3),  
@NoLineaAerea varchar(50),  
@CoLineaAereaEmisora char(7),  
@CoTransacEmision char(8),  
@CoLineaAereaServicio char(4),  
@TxFechaCreacion smalldatetime,  
@TxFechaModificacion smalldatetime,  
@TxFechaEmision smalldatetime,  
@TxIndicaVenta char(1),  
@CoPtoVentayEmision char(3),  
@TxTarifaAerea numeric(10,2),  
@TxTotal numeric(10,2),  
@NoPax varchar(50),  
@TxRestricciones varchar(250),  
@CoFormaPago char(11),  
@CoDocumIdent char(2),  
@NuDocumIdent varchar(15),  
@TxComision numeric(5,2),
@TxTasaDY_AE numeric(6,2),  
@TxTasaHW_DE numeric(6,2),  
@TxTasaXC_DP numeric(6,2),  
@TxTasaXB_TI numeric(6,2),  
@TxTasaQQ_TO numeric(6,2),  
@TxTasaAH_SE numeric(6,2),  
  
@IDCab int,  
@IDCliente char(6),  
@CoPNR_Amadeus char(6),
@CoEstado char(4),
@TxObsCou varchar(max),
@CoNacionalidad char(6),
@CoEjeRec char(4),
@CoEjeCou char(4),
@CoEjeCouAmd char(8),
@TxTituloPax varchar(150),
@CoPNR_Aerolinea char(9),
@UserMod char(4),
@NuNtaVta CHAR(6)
As  
Set NoCount On  

Declare @Igv numeric(5,2)=(Select NuIGV From PARAMETRO)

UPDATE [dbo].[TICKET_AMADEUS]  
   SET [CoAir] = @CoAir,   
  CoProvOrigen = @CoProvOrigen,  
       [CoUsrCrea] = @CoUsrCrea,   
       [CoUsrDuenio] = @CoUsrDuenio,  
       [CoUsrEmite] = @CoUsrEmite,  
       [CoIATA] = @CoIATA,  
       [CoLineaAereaRecord] = @CoLineaAereaRecord,  
       CoLineaAereaSigla = @CoLineaAereaSigla,  
       [NoLineaAerea] = @NoLineaAerea,  
       [CoLineaAereaEmisora] = @CoLineaAereaEmisora,   
       [CoTransacEmision] = @CoTransacEmision,   
       [CoLineaAereaServicio] = @CoLineaAereaServicio,   
       [TxFechaCreacion] = @TxFechaCreacion,   
       [TxFechaModificacion] = @TxFechaModificacion,  
       [TxFechaEmision] = @TxFechaEmision,  
       [TxIndicaVenta] = @TxIndicaVenta,  
       [CoPtoVentayEmision] = @CoPtoVentayEmision,   
       [TxTarifaAerea] = @TxTarifaAerea,   
       [TxTotal] = @TxTotal,   
       [NoPax] = @NoPax,   
       [TxRestricciones] = Case When LTRIM(rtrim(@TxRestricciones))='' then Null else @TxRestricciones End,   
       [CoFormaPago] = @CoFormaPago,   
       CoDocumIdent=@CoDocumIdent,  
       [NuDocumIdent] = @NuDocumIdent,   
       [TxComision] = @TxComision,   
       [TxTasaDY_AE] = Case when @TxTasaDY_AE=0 Then Null else @TxTasaDY_AE End,  
       [TxTasaHW_DE] = Case When @TxTasaHW_DE=0 Then Null else @TxTasaHW_DE End,  
       [TxTasaXC_DP] = Case When @TxTasaXC_DP=0 Then Null else @TxTasaXC_DP End,  
       [TxTasaXB_TI] = Case When @TxTasaXB_TI=0 Then Null else @TxTasaXB_TI End,  
       [TxTasaQQ_TO] = Case When @TxTasaQQ_TO=0 Then Null else @TxTasaQQ_TO End,  
       [TxTasaAH_SE] = Case When @TxTasaAH_SE=0 Then Null else @TxTasaAH_SE End,  
       IDCab=Case When @IDCab=0 Then Null Else @IDCab End,  
       IDCliente=Case When ltrim(rtrim(@IDCliente))='' Then Null Else @IDCliente End,  
       [CoPNR_Amadeus] = @CoPNR_Amadeus,--Case When ltrim(rtrim(@CoPNR_Amadeus)) = '' then Null Else @CoPNR_Amadeus End,
       [CoEstado] = @CoEstado,
       [TxObsCou] = Case When LTRIM(rtrim(@TxObsCou))='' Then Null Else @TxObsCou End,
       [CoNacionalidad] = Case When ltrim(rtrim(@CoNacionalidad))='000000' then Null Else @CoNacionalidad End,
       [CoEjeRec] = Case When LTRIM(rtrim(@CoEjeRec))='' then Null Else @CoEjeRec End,
       [CoEjeCou]=  Case When ltrim(rtrim(@CoEjeCou))='' then Null Else @CoEjeCou End,
	   [CoEjeCouAmd] = Case When ltrim(rtrim(@CoEjeCouAmd))='' then Null Else @CoEjeCouAmd End,
	   [TxTituloPax] = Case When LTRIM(rtrim(@TxTituloPax))='' then Null Else @TxTituloPax End,
	   
       SsComision=(@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
       SsCosto = @TxTotal-(@TxTarifaAerea*(@TxComision/100))*(1+(@Igv/100)),
	   CoPNR_Aerolinea=@CoPNR_Aerolinea,
       [UserMod] = @UserMod,   
       [FecMod] = GetDate()  ,
	   NuNtaVta=case when ltrim(rtrim(@NuNtaVta))='' then null else @NuNtaVta end
 WHERE [CoTicket] = @CoTicket  

