﻿--JRF-20141021- Isnull(sdRel.Tipo,'') >>> Isnull(sd.Tipo,'') 
--JRF-20150220- Agregar IDReserva_Det 
CREATE Procedure dbo.RESERVAS_DETSERVICIOS_Sel_ListxIDReservaDet      
@IDReserva_Det int      
As      
Set NoCount On      
Select Cast(rds.IDReserva_Det as varchar) as IDReserva_DetSb,sd.IDServicio_Det,p.NombreCorto as ProveedorCotizado,sd.Descripcion as Servicio,      
Isnull(sd.Tipo,'') as Variante,      
rds.CostoReal as Neto,rds.TotImpto,mo.Simbolo as Moneda,rds.Total      
--,rds.*      
from RESERVAS_DETSERVICIOS rds Left Join RESERVAS_DET rd On rds.IDReserva_Det=rd.IDReserva_Det      
Left Join MASERVICIOS_DET sd On rds.IDServicio_Det = sd.IDServicio_Det      
Left Join MASERVICIOS_DET sdRel On Isnull(sd.IDServicio_Det_V,sd.IDServicio_Det) = sdRel.IDServicio_Det      
Left Join MASERVICIOS s On sdRel.IDServicio = s.IDServicio Left Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor      
Left Join MAMONEDAS mo On rd.IDMoneda= mo.IDMoneda      
Where rds.IDReserva_Det= @IDReserva_Det  
