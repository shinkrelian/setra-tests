﻿--HLF-20151103-@IDServicio_Det_V_Cot int
--IDServicio=(select IDServicio from MASERVICIOS_DET ...
--HLF-20160115-Declare @NuTckEntIng smallint = ... Declare @QtStSaldo smallint= 0 ...
--HLF-20160119-Else Set @NuTckEntIng = isnull((Select Max(NuTckEntIng) ...
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_INGRESOS_SeNuTckEntlOutput
	@IDServicio_Det_V_Cot int,
--@pNuTckEntIng int output	
	@pNuTckEntIng_Saldo nvarchar(11) output
As
	Set NoCount On
	
	--Set @pNuTckEntIng = IsNULL((Select Max(NuTckEntIng) from STOCK_TICKET_ENTRADAS_INGRESOS 
	--where --IDServicio_Det=@IDServicio_Det
	--IDServicio=(select IDServicio from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det_V_Cot)
	--),0)
	Declare @IDServicio char(8)=(select IDServicio from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det_V_Cot)
	Declare @NuTckEntIng smallint = isnull((Select Min(NuTckEntIng) from STOCK_TICKET_ENTRADAS_INGRESOS where IDServicio=@IDServicio			
			And QtStSaldo>0),0)

	Declare @QtStSaldo smallint=0
	If @NuTckEntIng<>0
		Begin
		Select @QtStSaldo=QtStSaldo From STOCK_TICKET_ENTRADAS_INGRESOS where 
			IDServicio=@IDServicio	And NuTckEntIng=@NuTckEntIng		
		--Set @pNuTckEntIng_Saldo=cast(@NuTckEntIng as nvarchar(5))+'-'+cast(@QtStSaldo as nvarchar(5))
		End
	Else
		Begin
		Set @NuTckEntIng = isnull((Select Max(NuTckEntIng) from STOCK_TICKET_ENTRADAS_INGRESOS where IDServicio=@IDServicio),0)
		--Set @pNuTckEntIng_Saldo=''
		End
	Set @pNuTckEntIng_Saldo=cast(@NuTckEntIng as nvarchar(5))+'-'+cast(@QtStSaldo as nvarchar(5))

