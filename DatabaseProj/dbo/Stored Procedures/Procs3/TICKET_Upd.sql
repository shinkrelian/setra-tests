﻿	
	
Create Procedure dbo.TICKET_Upd

	@CoTicket char(10),
	@IDCab int ,
	@IDCliente char(6),
	@FlArea char(1),

	@SsTCambioCont numeric(4,2),
	@SsTCambioVenta numeric(4,2),

	@FeEmision smalldatetime,
	@FeViaje smalldatetime,
	@FeRetorno smalldatetime,
	@IDPax int,
	@TxPax	varchar(100),
	@IDProveedor char(6),
	@IDUbigeoOri char(6),
	@IDUbigeoDes char(6),
	@TxRuta varchar(50),

	@CoLugarVenta char(2),
	@CoCounter char(4),

	@SsTarifa numeric(8,2),
	@SsPTA numeric(8,2),
	@SsPenalidad numeric(8,2),
	@SsOtros numeric(8,2),
	@SsIGV numeric(6,2),
	@SsImpExt numeric(8,2),

	@PoDscto  numeric(5,2),
	@SsDscto  numeric(8,2),

	@SsTotal numeric(8,2),

	@CoTarjCred char(3),
	@CoNroTarjCred varchar(25),
	@CoNroAprobac varchar(25),
	@SsTotTarjeta numeric(8,2),
	@SsTotEfectivo numeric(8,2),

	@PoComision numeric(5,2),
	@SsComision numeric(8,2),
	@SsIGVComision numeric(6,2),


	@PoTarifa numeric(5,2),
	@PoOver numeric(5,2),
	@SsOver numeric(8,2),
	@SsIGVOver numeric(6,2),

	@FlMigrado bit,
	@CoProvOrigen char(6),

	@TxReferencia varchar(200),
	@CoTipo char(2),
	@CoTipoCambio char(2),

	@UserMod char(4)


As
	Set Nocount On
	
	Update TICKET Set
	IDCab=case when @IDCab=0 Then Null Else @IDCab End,
	IDCliente=@IDCliente,
	FlArea=@FlArea,
	SsTCambioCont=case when @SsTCambioCont=0 Then Null Else @SsTCambioCont End,
	SsTCambioVenta=case when @SsTCambioVenta=0 Then Null Else @SsTCambioVenta End,
	FeEmision=@FeEmision,
	FeViaje=@FeViaje,
	FeRetorno=@FeRetorno,
	IDPax=case when @IDPax=0 Then Null Else @IDPax End,
	TxPax=case when LTRIM(RTRIM(@TxPax))='' Then Null Else @TxPax End,
	IDProveedor=@IDProveedor,	
	IDUbigeoOri=@IDUbigeoOri,
	IDUbigeoDes=@IDUbigeoDes,
	TxRuta=@TxRuta,
	CoLugarVenta=@CoLugarVenta,
	CoCounter=@CoCounter,
	SsTarifa=case when @SsTarifa=0 Then Null Else @SsTarifa End,
	SsPTA=case when @SsPTA=0 Then Null Else @SsPTA End,
	SsPenalidad=case when @SsPenalidad=0 Then Null Else @SsPenalidad End,
	SsOtros=case when @SsOtros=0 Then Null Else @SsOtros End,
	SsIGV=case when @SsIGV=0 Then Null Else @SsIGV End,
	SsImpExt=case when @SsImpExt=0 Then Null Else @SsImpExt End,
	PoDscto=case when @PoDscto=0 Then Null Else @PoDscto End,
	SsDscto=case when @SsDscto=0 Then Null Else @SsDscto End,
	SsTotal=@SsTotal,
	CoTarjCred=case when LTRIM(RTRIM(@CoTarjCred))='' Then Null Else @CoTarjCred End,
	CoNroTarjCred=case when LTRIM(RTRIM(@CoNroTarjCred))='' Then Null Else @CoNroTarjCred End,
	CoNroAprobac=case when LTRIM(RTRIM(@CoNroAprobac))='' Then Null Else @CoNroAprobac End,
	SsTotTarjeta=case when @SsTotTarjeta=0 Then Null Else @SsTotTarjeta End,
	SsTotEfectivo=case when @SsTotEfectivo=0 Then Null Else @SsTotEfectivo End,
	PoComision=case when @PoComision=0 Then Null Else @PoComision End,
	SsComision=case when @SsComision=0 Then Null Else @SsComision End,
	SsIGVComision=case when @SsIGVComision=0 Then Null Else @SsIGVComision End,
	PoTarifa=case when @PoTarifa=0 Then Null Else @PoTarifa End,
	PoOver=case when @PoOver=0 Then Null Else @PoOver End,
	SsOver=case when @SsOver=0 Then Null Else @SsOver End,
	SsIGVOver=case when @SsIGVOver=0 Then Null Else @SsIGVOver End,
	FlMigrado=@FlMigrado,
	CoProvOrigen=@CoProvOrigen,
	TxReferencia=case when LTRIM(RTRIM(@TxReferencia))='' Then Null Else @TxReferencia End,
	CoTipo=@CoTipo,
	CoTipoCambio=case when LTRIM(RTRIM(@CoTipoCambio))='' Then Null Else @CoTipoCambio End,
	FecMod=GETDATE(),
	UserMod=@UserMod
	Where	CoTicket = @CoTicket


