﻿Create Procedure dbo.VOUCHER_OPERACIONES_Anu
@IDVoucher int,
@CoEstado char(2),
@UserMod char(4)
As
Set NoCount On
Update VOUCHER_OPERACIONES
	Set CoEstado = @CoEstado,
		UserMod = @UserMod,
		FecMod = GetDate()
Where IDVoucher=@IDVoucher
