﻿--JRF-20160311-...0 as IDDetAntiguo
CREATE Procedure dbo.VIATICOS_TOURCONDUCTOR_SelxIDCabInReservasDet
@IDCab int
As
Begin
Set NoCount On
Declare @NumEmpty numeric(8,2)=0,
		@MaxItemCostoTC numeric(10,3) = (select Max(item) from COTIDET where IDCAB=@IDCab And FlServicioTC=1)
Select Y.*,Y.CostoReal+Y.RedondeoTotal As Total,Y.CostoReal+Y.RedondeoTotal As TotalOrig,@NumEmpty as TotImpto,cast(0 as int) as IDDetRel,
	   cast(0 as int) as IDDetCopia,0 as PorReserva,0 as ExecTrigger, 
	   IsNull((select Top(1) IDReserva from RESERVAS where Anulado=0 and IDProveedor=Y.IDProveedor And IDCab=Y.IDCab),0) as IDReserva,0 As IncGuia,
	   0 as FueradeHorario,0 as ConAlojamiento,0 as CostoRealEditado,0 as CostoRealImptoEditado,0 as MargenAplicadoEditado,0 as ServicioEditado,0 as PlanAlimenticio,
	   NuViaticoTC,0 as IDDetAntiguo
from (
	select IDReserva_Det,IDDet,NroPax,NroLiberados,Tipo_Lib,CostoLiberado,CostoLiberadoImpto,
		   CostoReal = Total / NroPax ,CostoRealAnt = Total / NroPax,@NumEmpty as CostoRealImpto,Cotizacion,dia,0 as Especial,IDCab,
		   IDIdioma,IDProveedor,IDTipoProv,Dias,IDServicio,IDServicio_Det,IDUbigeo,@MaxItemCostoTC + NroItem as Item,@NumEmpty as Margen,
		   @NumEmpty as MargenAplicado,@NumEmpty as MargenAplicadoAnt,@NumEmpty as MargenImpto,@NumEmpty as MargenLiberado,
		   @NumEmpty as MargenLiberadoImpto,'' as MotivoEspecial,'' as RutaDocSustento,0 as Desayuno,0 as Lonche,0 as Almuerzo,0 as Cena,
		   0 as Transfer,cast(0 as int) as IDDetTransferOri,cast(0 as int) as IDDetTransferDes,'' as TipoTransporte,'000001' as IDUbigeoOri,'000001' as IDUbigeoDes,
		   Servicio,@NumEmpty as SSCL,@NumEmpty as SSCLImpto,@NumEmpty as SSCR,@NumEmpty as SSCRImpto,@NumEmpty as SSMargen,@NumEmpty as STMargenImpto,
		   @NumEmpty as SSMargenLiberado,@NumEmpty as SSMargenLiberadoImpto,@NumEmpty as SSTotal,@NumEmpty as SSTotalOrig,@NumEmpty as STCR,@NumEmpty as STCRImpto,
		   @NumEmpty as STMargen,@NumEmpty as STTotal,@NumEmpty as STTotalOrig,
		   Case when (Total / NroPax) = 0 then 0 else 1-((Total / NroPax)-FLOOR(Total / NroPax)) End As RedondeoTotal,NuViaticoTC,IDFile,Null as FechaOut,
		   Cast(0 as tinyint) as Noches,
		   Cast(NroPax as tinyint) as Cantidad,NroPax as CantidadAPagar,0 as IDReserva_DetCopia,0 as IDReserva_Det_Rel,
		   Cast(0 as tinyint) as CapacidadHab,cast(0 as bit) as EsMatrimonial,IDMoneda,'' as IDGuiaProveedor,0 as NuVehiculo,
		   Cast(0 as bit) as ExecTrigger,Cast(0 as bit) as FlServicioParaGuia,
		   Cast(0 as bit) as FlServicioNoShow,Cast(1 as bit) as FlServicioIngManual
	from (
		select --IsNull((
		--  select x.IDReserva_Det from (
		--	select rd.IDReserva_Det,rd.NuViaticoTC
		--	from VIATICOS_TOURCONDUCTOR vt Left Join (select * from RESERVAS_DET where IDReserva=rd.IDReserva and NuViaticoTC>0 And Anulado=0) rd
		--	On vt.IDServicio_Det = rd.IDServicio_Det And vt.NuViaticoTC = rd.NuViaticoTC
		--	where IDCab=@IDCab ) as X Where X.NuViaticoTC = vc.NuViaticoTC
		--)
		--,0) 
		0 As IDReserva_Det,0 As IDDet,c.NroPax,0 as NroLiberados,'' as Tipo_Lib,@NumEmpty as CostoLiberado,@NumEmpty as CostoLiberadoImpto,
		c.Cotizacion, Case When vc.CostoSOL > 0 then 'SOL' Else 'USD' End as IDMoneda,Total = vc.CostoSOL+vc.CostoUSD,vc.Dia,'NO APLICA' IDIdioma,
		vc.IDCab,'001554' as IDProveedor,'003' as IDTipoProv,1 as Dias,sd.IDServicio,vc.IDServicio_Det,vc.NuViaticoTC,'000065' As IDUbigeo,
		ROW_NUMBER()Over(Order By vc.NuViaticoTC) as NroItem,sd.Descripcion as Servicio, IsNull(c.IDFile,'') as IDFile
		from VIATICOS_TOURCONDUCTOR vc Left Join COTICAB c On vc.IDCab=c.IDCAB
		Left Join MASERVICIOS_DET sd On vc.IDServicio_Det=sd.IDServicio_Det
		Left Join RESERVAS_DET rd On vc.IDServicio_Det=rd.IDServicio_Det And rd.Anulado=0 And rd.NuViaticoTC=vc.NuViaticoTC
		where vc.IDCab=@IDCab and vc.FlActivo = 1
	) As X 
) As Y
End
