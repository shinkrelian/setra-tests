﻿--HLF-20140915-Comentando Left Join MAUBIGEO uP y Left Join MAUBIGEO uL
CREATE Procedure dbo.TICKET_AMADEUS_TARIFA_SelxCoTicket  
 @CoTicket char(13)  
As  
 Set NoCount On  
 Select CoTicket,NuTarifa,Cp.CoCiudadPar,Cp.CoCiudadLle,  
 Cp.CoCiudadPar+'/'+Cp.CoCiudadLle as Ruta,SsTarifa,SsQ,SsTarifa+SsQ as Total  
 from TICKET_AMADEUS_TARIFA Cp 
 --Left Join MAUBIGEO uP On Cp.CoCiudadPar= uP.DC  
 --Left Join MAUBIGEO uL On Cp.CoCiudadLle= uL.DC  
 where coTicket = @CoTicket  
