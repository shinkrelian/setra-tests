﻿Create Procedure [dbo].[RESERVAS_MENSAJESTELEF_Sel_Pk]
	@IDReserva int,
	@IDMensaje tinyint
	
As	
	Set NoCount On
	
	Select * From RESERVAS_MENSAJESTELEF
	Where
	IDReserva=@IDReserva And
	IDMensaje=@IDMensaje
