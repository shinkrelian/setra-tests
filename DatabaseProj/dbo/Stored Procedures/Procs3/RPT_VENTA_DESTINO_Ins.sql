﻿CREATE PROCEDURE [RPT_VENTA_DESTINO_Ins]
	@NuAnio char(4)='',
	@CoUbigeo char(6)='',
	@QtServicios int=0,
	@UserMod char(4)=''
AS
BEGIN
	Insert Into [dbo].[RPT_VENTA_DESTINO]
		(
			[NuAnio],
 			[CoUbigeo],
 			[QtServicios],
 			[UserMod],
 			[FecMod]
 		)
	Values
		(
			case when LTRIM(RTRIM(@NuAnio))='' then Null Else @NuAnio End,
 			case when LTRIM(RTRIM(@CoUbigeo))='' then Null Else @CoUbigeo End,
 			case when @QtServicios=0 then Null Else @QtServicios End,
 			case when LTRIM(RTRIM(@UserMod))='' then Null Else @UserMod End,
 			GETDATE()
 		)
END
