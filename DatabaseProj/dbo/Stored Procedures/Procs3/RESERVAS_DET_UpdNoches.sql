﻿--HL-20121203-Cambiando Where IDReserva_Det=@IDReserva_Det por Where IDDet=@IDDet        
--HL-20121203-Agregando 2do Update        
--HL-20130516-Agregando 3er y 4to Update        
--HL-20130522-Agregando If    
--HLF-JRF-20140317-Condicionar el Anulado  
--HLF-20140327-Set NetoGen = CantidadAPagar * TotalHab * Noches, 
CREATE Procedure dbo.RESERVAS_DET_UpdNoches         
 --@IDReserva_Det int,        
 @IDDet int,        
 --@Noches tinyint,        
 @FechaOut smalldatetime,        
 @UserMod char(4)        
As        
 Set NoCount On        
        
 Update RESERVAS_DET Set --Noches=@Noches,        
 FechaOut=@FechaOut, UserMod=@UserMod, FecMod=GETDATE()        
 Where IDDet=@IDDet and Anulado = 0  
        
 Update RESERVAS_DET Set Noches=DATEDIFF(D,Dia,FechaOut),        
 UserMod=@UserMod, FecMod=GETDATE()        
 Where IDDet=@IDDet and Anulado = 0      
         
 If Not Exists(Select cd.IDDET From COTIDET cd     
 Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=cd.IDServicio_Det     
 And sd.ConAlojamiento=1    
 Inner Join MASERVICIOS s On s.IDServicio=sd.IDServicio And s.Dias>1    
 Where IDDET=@IDDet)        
     
	 Begin    
	    
	 Update RESERVAS_DET 
	 --Set NetoGen = NetoGen * Noches,      
	 Set NetoGen = CantidadAPagar * TotalHab * Noches, 
	 UserMod=@UserMod, FecMod=GETDATE()        
	 Where IDDet=@IDDet and Anulado = 0          
	    
	 Update RESERVAS_DET Set TotalGen = NetoGen + IgvGen,      
	 UserMod=@UserMod, FecMod=GETDATE()        
	 Where IDDet=@IDDet  and Anulado = 0   
	 End    