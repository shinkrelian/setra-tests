﻿  
  
    
--JRF-20140623-Agregando el campo [CoPNR_Amadeus]  
--HLF-20140704-@CoTicket char(13)  
--HLF-20140707-Agregando parametro @CoPNR_Aerolinea   
--HLF-20141023-@CoPNR_Aerolinea varchar(20)
CREATE Procedure dbo.TICKET_AMADEUS_ITINERARIO_Upd  
@CoTicket char(13),  
@NuSegmento tinyint,  
@CoCiudadPar char(3),  
@CoLineaAerea char(2),  
@NuVuelo char(4),  
@CoSegmento char(1),  
@CoBooking char(1),  
@TxFechaPar smalldatetime,  
--@TxHoraPar char(4),  
@TxBaseTarifa char(6),  
@TxBag char(3),  
@CoST char(2),  
@CoCiudadLle char(3),  
@TxHoraLle smalldatetime,  
@CoPNR_Amadeus char(6),  
@CoPNR_Aerolinea char(20),  
@UserMod char(4)  
As  
Set NoCount On  
UPDATE [TICKET_AMADEUS_ITINERARIO]  
   SET [CoCiudadPar] = @CoCiudadPar,  
       [CoLineaAerea] = @CoLineaAerea,  
       [NuVuelo] = @NuVuelo,   
       [CoSegmento] = @CoSegmento,   
       [CoBooking] = @CoBooking,   
       [TxFechaPar] = @TxFechaPar,   
       --[TxHoraPar] = @TxHoraPar,   
       [TxBaseTarifa] = @TxBaseTarifa,   
       [TxBag] = @TxBag,   
       [CoST] = @CoST,   
       [CoCiudadLle] = @CoCiudadLle,   
       [TxHoraLle] = @TxHoraLle,   
       [CoPNR_Amadeus] = @CoPNR_Amadeus,--Case When ltrim(rtrim(@CoPNR_Amadeus))='' then Null Else @CoPNR_Amadeus End,  
       CoPNR_Aerolinea=@CoPNR_Aerolinea,  
       [UserMod] = @UserMod,   
       [FecMod] = GetDate()  
 WHERE [CoTicket] = @CoTicket and [NuSegmento] = @NuSegmento  
  
  
  