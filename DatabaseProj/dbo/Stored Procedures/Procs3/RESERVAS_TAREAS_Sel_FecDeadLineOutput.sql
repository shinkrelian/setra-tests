﻿CREATE Procedure [dbo].[RESERVAS_TAREAS_Sel_FecDeadLineOutput]
	@IDCab	int,
	@pFecDeadLine smalldatetime Output
As

	Set NoCount On	
	
	Select Top 1 @pFecDeadLine=IsNull(FecDeadLine,'01/01/1900') From RESERVAS_TAREAS 
	Where IDCab=@IDCab Order by FecDeadLine
	
	Set @pFecDeadLine=ISnull(@pFecDeadLine,'01/01/1900')
