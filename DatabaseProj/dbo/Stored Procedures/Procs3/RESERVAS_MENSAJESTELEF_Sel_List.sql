﻿CREATE Procedure [dbo].[RESERVAS_MENSAJESTELEF_Sel_List]
	@IDReserva	int
As
	Set NoCount On
	
	Select m.IDMensaje, m.Fecha, m.Contacto as DescContactoPara,
	--cp.Nombres+ ' ' + cp.Apellidos as DescContactoPara,
	m.Asunto, u.Nombre as DescContactoDe, m.Mensaje	
	From RESERVAS_MENSAJESTELEF m
	Left Join RESERVAS r On m.IDReserva=r.IDReserva
	--Left Join MACONTACTOSPROVEEDOR cp On cp.IDProveedor=r.IDProveedor And 
	--cp.IDContacto=cp.IDContacto
	Left Join MAUSUARIOS u On m.IDUsuario=u.IDUsuario
	Where m.IDReserva=@IDReserva
	Order by IDMensaje
