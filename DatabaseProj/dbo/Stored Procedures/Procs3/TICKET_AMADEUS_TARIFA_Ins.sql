﻿create Procedure dbo.TICKET_AMADEUS_TARIFA_Ins
 @CoTicket char(13),
 @CoCiudadPar char(3),
 @CoCiudadLle char(3),
 @SsTarifa numeric(6,2),
 @SsQ numeric(6,2),
 @UserMod char(4)
As
	Set NoCount On
	Declare @NuTarifa tinyint =(select Isnull(Max(NuTarifa),0)+1 
								from TICKET_AMADEUS_TARIFA where CoTicket =@CoTicket)
	INSERT INTO [dbo].[TICKET_AMADEUS_TARIFA]
			   ([CoTicket]
			   ,[NuTarifa]
			   ,[CoCiudadPar]
			   ,[CoCiudadLle]
			   ,[SsTarifa]
			   ,[SsQ]
			   ,[UserMod]
			   ,[FecMod])
		 VALUES
			   (@CoTicket,
				@NuTarifa,
				@CoCiudadPar,
				@CoCiudadLle,
				@SsTarifa, 
				@SsQ, 
				@UserMod, 
				Getdate())
