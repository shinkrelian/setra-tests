﻿CREATE PROCEDURE [RPT_VENTA_PAIS_Ins]
	@NuAnio char(4)='',
	@CoPais char(6)='',
	@EneVentaMes numeric(13,2)=0,
	@FebVentaMes numeric(13,2)=0,
	@MarVentaMes numeric(13,2)=0,
	@AbrVentaMes numeric(13,2)=0,
	@MayVentaMes numeric(13,2)=0,
	@JunVentaMes numeric(13,2)=0,
	@JulVentaMes numeric(13,2)=0,
	@AgoVentaMes numeric(13,2)=0,
	@SetVentaMes numeric(13,2)=0,
	@OctVentaMes numeric(13,2)=0,
	@NovVentaMes numeric(13,2)=0,
	@DicVentaMes numeric(13,2)=0,
	@Total numeric(13,2)=0,
	@UserMod char(4)=''
	
AS
BEGIN
	Insert Into [dbo].[RPT_VENTA_PAIS]
		(
			[NuAnio],
 			[CoPais],
 			[EneVentaMes],
 			[FebVentaMes],
 			[MarVentaMes],
 			[AbrVentaMes],
 			[MayVentaMes],
 			[JunVentaMes],
 			[JulVentaMes],
 			[AgoVentaMes],
 			[SetVentaMes],
 			[OctVentaMes],
 			[NovVentaMes],
 			[DicVentaMes],
 			[Total],
 			[UserMod],
 			[FecMod]
 		)
	Values
		(
		    case when LTRIM(RTRIM(@NuAnio))='' then Null Else @NuAnio End,
			case when LTRIM(RTRIM(@CoPais))='' then Null Else @CoPais End,
			case when @EneVentaMes=0 then Null Else @EneVentaMes End,
			case when @FebVentaMes=0 then Null Else @FebVentaMes End,
			case when @MarVentaMes=0 then Null Else @MarVentaMes End,
			case when @AbrVentaMes=0 then Null Else @AbrVentaMes End,
			case when @MayVentaMes=0 then Null Else @MayVentaMes End,
			case when @JunVentaMes=0 then Null Else @JunVentaMes End,
			case when @JulVentaMes=0 then Null Else @JulVentaMes End,
			case when @AgoVentaMes=0 then Null Else @AgoVentaMes End,
			case when @SetVentaMes=0 then Null Else @SetVentaMes End,
			case when @OctVentaMes=0 then Null Else @OctVentaMes End,
			case when @NovVentaMes=0 then Null Else @NovVentaMes End,
			case when @DicVentaMes=0 then Null Else @DicVentaMes End,
			case when @Total=0 then Null Else @Total End,
 		    case when LTRIM(RTRIM(@UserMod))='' then Null Else @UserMod End,
 			GETDATE()
			)
END;
