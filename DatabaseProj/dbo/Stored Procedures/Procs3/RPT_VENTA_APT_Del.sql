﻿
-----------------------------------------------------------------------------------------------------------
--ALTER
--JHD-20150116- Se cambió el campo @CoMes a char(2)
CREATE PROCEDURE [dbo].[RPT_VENTA_APT_Del]
	@NuAnio char(4)='',
	@CoMes char(2)=''
AS
BEGIN
	Delete dbo.RPT_VENTA_APT
	Where 
		NuAnio = @NuAnio And 
		CoMes = @CoMes
END;
