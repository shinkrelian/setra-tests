﻿

  
--HLF-20140627-@CoNacionalidad,@IDCliente  
--HLF-20140630-REPLACE(Vuelo  
--HLF-20141022-@pIDCab int output 
--HLF-20141023-Comentar CotiVuelos y buscar en Cotipax
CREATE PROCEDURE dbo.TICKET_AMADEUS_UpdIDCabxDatosVuelo    
 @CoTicket char(13),    
 @UserMod char(4),  
 @NuDocumIdent varchar(15),
 @pIDCab int output 
As    
 Set NoCount On         
     
 Declare @IDCab int, @CoNacionalidad char(6), @IDCliente char(6)  
     
 --Select Distinct @IDCab=cv.IDCAB, @IDCliente=cc.IDCliente   
 --From COTIVUELOS cv Left Join COTICAB cc  
 --On cc.IDCAB=cv.IDCAB    
 --Where ltrim(rtrim(REPLACE(Vuelo,' ','')))+' '+convert(varchar,Fec_Salida,103) In     
 --(Select ltrim(rtrim(CoLineaAerea))+ltrim(rtrim(NuVuelo))+' '+    
 -- convert(varchar,TxFechaPar,103)     
 -- From TICKET_AMADEUS_ITINERARIO Where CoTicket=@CoTicket)    
   
 --Select @CoNacionalidad=IDNacionalidad From COTIPAX Where NumIdentidad=@NuDocumIdent    
   
 Select Top 1 @IDCab=IDCab, @CoNacionalidad=IDNacionalidad 
 From COTIPAX Where NumIdentidad=@NuDocumIdent Order by FecMod desc  
 
 If Not @IDCab Is Null
	Select @IDCliente=IDCliente From COTICAB Where IDCAB=@IDCab  
   
 UPDATE TICKET_AMADEUS SET IDCab=@IDCab,
  CoNacionalidad=@CoNacionalidad,  
  IDCliente=@IDCliente,  
  UserMod=@UserMod, FecMod=GETDATE()    
  Where CoTicket=@CoTicket  
  
 Set @pIDCab=isnull(@IDCab,0)
 
