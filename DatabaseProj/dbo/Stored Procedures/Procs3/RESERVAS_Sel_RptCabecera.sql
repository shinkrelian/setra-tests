﻿--JRF-20140324-Se debe verificar que exista un vuelo con Aerodianda
--JRF-20140324-Se debe verificar que existan Sericios que contengan Inca Trail%..
--JRF-20140324-Se debe verificar que existan entradas de Hoteles en el ubigeo de Aguas Calientes.
CREATE Procedure RESERVAS_Sel_RptCabecera            
@IDCab int          
As          
 Set NoCount On          
 select (select CONVERT(Char(10),Min(Dia),103) from COTIDET where IDCAB = @IDCab) As FechaInServ,Titulo   
 ,ms.Descripcion As TipoServ,NroPax,i.IDidioma,Cl.RazonComercial As Cliente  
 ,Ub.Descripcion As Ubigeo,  
 Case When  
  --NroPax > 15   
  Exists(select IDDET from COTIDET cd Left Join MAUBIGEO Ub On cd.IDubigeo =Ub.IDubigeo  
   where IDCAB = @IDCab and ub.IDPais <> '000007' and ub.IDPais <> '000323')  
 then   
  'M'   
 Else   
   Case When c.NroPax > 15 Then 'G' Else 'F' End  
 End As EsMulti_Fits_Group,
  Case When Exists(select ID from COTIVUELOS cv where cv.IDCAB = c.IDCAB and cv.IdLineaA = '000587') 
	Then 1 Else 0 End as ExisteLineaAeroDiana,
  Case When Exists(select cd.Servicio from COTIDET cd where cd.IDCAB = c.IDCAB and lower(cd.Servicio) Like '%inca trail%')
    Then 1 Else 0 End as ExisteIncaTrail,
  Case When exists(select cd.IDubigeo from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor 
					where cd.IDCAB = c.IDCAB and p.IDTipoProv = '001' and cd.IDubigeo = '000374')
	then 1 Else 0 End as ExistePernocteAguasCalientes
 from COTICAB c Left Join MAMODOSERVICIO ms On c.TipoServicio = ms.CodModoServ          
 Left Join MAIDIOMAS i On c.IDIdioma= i.IDidioma          
 Left Join MACLIENTES Cl On c.IDCliente = Cl.IDCliente          
 Left Join MAUBIGEO Ub On c.IDubigeo= Ub.IDubigeo          
  where IDCAB = @IDCab          
  And Exists(select IDReserva from RESERVAS where IDCab = @IDCab)           
