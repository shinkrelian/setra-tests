﻿
-----------------------------------------------------------------------

CREATE PROCEDURE [RH_TRABAJADOR_Sel_PK]
	@CoTrabajador char(11)
as
SELECT		  RH_TRABAJADOR.CoTrabajador,
			  RH_TRABAJADOR.NoApPaterno,
			  RH_TRABAJADOR.NoApMaterno,
			  RH_TRABAJADOR.NoNombres,
			  NoTrabajador=NoApPaterno+' '+NoApMaterno+', '+NoNombres,
			  RH_TRABAJADOR.CoTipoDoc, 
              MATIPOIDENT.Descripcion,
              RH_TRABAJADOR.NuNumeroDoc,
              RH_TRABAJADOR.FlActivo,
              RH_TRABAJADOR.UserMod,
              RH_TRABAJADOR.FecMod
FROM         RH_TRABAJADOR INNER JOIN
                      MATIPOIDENT ON RH_TRABAJADOR.CoTipoDoc = MATIPOIDENT.IDIdentidad
WHERE     
		CoTrabajador =@CoTrabajador
		
