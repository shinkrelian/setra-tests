﻿CREATE Procedure dbo.TICKET_AMADEUS_ITINERARIO_Del
@CoTicket char(13),
@NuSegmento tinyint
As
Set NoCount On
Delete from TICKET_AMADEUS_ITINERARIO
where [CoTicket] = @CoTicket and [NuSegmento] = @NuSegmento
