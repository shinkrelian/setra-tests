﻿Create Procedure dbo.RESERVAS_UpdEstadoxProveedor
	@IDCab	int,
	@IDProveedor char(6),
	@Estado char(2),
	@UserMod char(4)
As 
	Set Nocount On  

	Update RESERVAS 
	Set Estado=@Estado,	
	FecMod=GETDATE(), UserMod=@UserMod	
	Where IDCab=@IDCab And IDProveedor=@IDProveedor 
	And Anulado=0
	
