﻿Create Procedure dbo.RESERVAS_DETSERVICIOS_Del_CostosGuiasVehiculos
	@IDCab int,
	@CoProveedor char(6),
	@UserMod char(4)
As
--	Set Nocount on

Inicio:	
	
	SELECT * Into #RESERVAS_DETSERVICIOSTEMP FROM
		(
		Select	DISTINCT
		rd.IDReserva_Det,--cd.IDServicio_Det,

		(SELECT top 1 rds1.IDServicio_Det FROM RESERVAS_DETSERVICIOS RDS1 INNER JOIN MASERVICIOS_DET SD1 ON RDS1.IDServicio_Det=sd1.IDServicio_Det
		INNER JOIN MASERVICIOS_DET SDV1 ON SD1.IDServicio_Det_V=sdV1.IDServicio_Det
		INNER JOIN MASERVICIOS S1 ON S1.IDServicio=SDV1.IDServicio
		INNER JOIN MAPROVEEDORES P1 ON S1.IDProveedor=P1.IDProveedor and p1.IDTipoProv in ('005','008')
		WHERE RDS1.IDReserva_Det=rd.IDReserva_Det) as IDServicio_Det,

		Servicio,
		NroPaX+ISNULL(NROLIBERADOS,0) AS Pax,
			(select COUNT(*) from ACOMODO_VEHICULO_DET_PAX where IDReserva_Det=rd.IDReserva_Det and FlGuia=1
			) as CantGuiasVehi,

		(SELECT count(*) FROM RESERVAS_DETSERVICIOS RDS INNER JOIN MASERVICIOS_DET SD ON RDS.IDServicio_Det=sd.IDServicio_Det
		INNER JOIN MASERVICIOS_DET SDV ON SD.IDServicio_Det_V=sdV.IDServicio_Det
		INNER JOIN MASERVICIOS S ON S.IDServicio=SDV.IDServicio
		INNER JOIN MAPROVEEDORES P ON S.IDProveedor=P.IDProveedor and p.IDTipoProv in ('005','008')
		WHERE RDS.IDReserva_Det=rd.IDReserva_Det) as CantGuiasSubServ	
		From RESERVAS_DET rd inner join RESERVAS r On rd.IDReserva=r.IDReserva And r.IDCab=@IDCab And r.IDProveedor=@CoProveedor	
			And rd.Anulado=0 And r.Anulado=0
			inner join ACOMODO_VEHICULO av On rd.IDDET=av.IDDet
		) AS X WHERE X.CantGuiasVehi<X.CantGuiasSubServ
	ORDER BY IDReserva_Det,IDServicio_Det
	
	Declare @tiRows tinyint=@@rowcount

	If @tiRows>0
		Begin
		Declare curRESERVAS_DETSERVICIOSTEMP Cursor For 
		SELECT IDReserva_Det,IDServicio_Det,CantDel
		From
			(
			SELECT X.*, (X.CantGuiasSubServ-X.CantGuiasVehi) as CantDel
			FROM 
				(
				SELECT * FROM #RESERVAS_DETSERVICIOSTEMP
				) as X
			--WHERE X.CantGuiasVehi<X.CantGuiasSubServ
			) as Y
					
		Declare @IDReserva_Det int,@IDServicio_Det int,@CantDel tinyint
		
		Open curRESERVAS_DETSERVICIOSTEMP
		Fetch Next From curRESERVAS_DETSERVICIOSTEMP Into @IDReserva_Det,@IDServicio_Det,@CantDel
		Declare @tiCont tinyint=@CantDel
		While @@FETCH_STATUS=0
			Begin
			While @tiCont >= @CantDel
				Begin
				--print @IDReserva_Det
				--print @IDServicio_Det 
				Delete From RESERVAS_DETSERVICIOS Where IDReserva_Det=@IDReserva_Det And IDServicio_Det=@IDServicio_Det 
				Set @tiCont-=1
				End				

			Fetch Next From curRESERVAS_DETSERVICIOSTEMP Into @IDReserva_Det,@IDServicio_Det,@CantDel
			End
		Close curRESERVAS_DETSERVICIOSTEMP
		Deallocate curRESERVAS_DETSERVICIOSTEMP

		DROP TABLE #RESERVAS_DETSERVICIOSTEMP

		goto Inicio
		End
