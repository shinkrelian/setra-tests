﻿
CREATE PROCEDURE [TICKET_Sel_ExisteOutput]
	@CoTicket char(10),
	@pExiste bit Output
AS
BEGIN
	SET NOCOUNT ON
	
	IF EXISTS(SELECT CoTicket FROM TICKET WHERE CoTicket = @CoTicket)
		SET @pExiste = 1
	ELSE
		SET @pExiste = 0
END
