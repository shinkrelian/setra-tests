﻿Create Procedure dbo.VOUCHER_PROV_TREN_UpdSsTotal
	@NuVouPTrenInt int,
	@SsTotal numeric(8,2),
	@SsImpuestos  numeric(8,2),
	@UserMod char(4)
As
	Set Nocount on

	Update VOUCHER_PROV_TREN Set SsTotal=SsTotal+@SsTotal, 
	SsSaldo=SsSaldo+@SsTotal,
	SsImpuestos=SsImpuestos+@SsImpuestos,
	UserMod=@UserMod, FecMod=GETDATE()
	Where NuVouPTrenInt=@NuVouPTrenInt

