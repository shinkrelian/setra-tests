﻿--JRF-20140711-Actualizar el MontoTotal
--JRF-20151019- ..And IDVoucher <> 0
CREATE Procedure [dbo].[VOUCHER_OPERACIONES_UpdSaldo]  
 @IDVoucher int,  
 @UserMod char(4)  
As      
 Set Nocount On     
 Declare @MonedaFEgreso char(3)=(select top(1) rd.IDMoneda
							from DOCUMENTO_PROVEEDOR dp Left Join OPERACIONES_DET od On dp.NuVoucher=od.IDVoucher
							Inner Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det and rd.Anulado=0
							Where dp.NuVoucher=@IDVoucher)
 
 Declare @TotalDoc numeric(9,2)=Isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) 
								 from DOCUMENTO_PROVEEDOR where NuVoucher=@IDVoucher and FlActivo=1),0)
								 
 Update VOUCHER_OPERACIONES 
 Set SsMontoTotal=(Select Sum(TotalGen) From Operaciones_Det Where IDVoucher=@IDVoucher), 
	 SsSaldo=(Select Sum(TotalGen) From Operaciones_Det Where IDVoucher=@IDVoucher)-@TotalDoc-IsNull(VOUCHER_OPERACIONES.SsDifAceptada,0),
	 usermod=@UserMod,  
	 fecmod=getdate()  
 Where IDVoucher=@IDVoucher And IDVoucher <> 0
