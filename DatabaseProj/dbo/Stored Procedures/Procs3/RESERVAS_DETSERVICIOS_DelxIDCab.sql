﻿
Create Procedure dbo.RESERVAS_DETSERVICIOS_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM RESERVAS_DETSERVICIOS 
	WHERE IDReserva_Det IN 
		(SELECT IDReserva_Det FROM RESERVAS_DET WHERE IDReserva IN (SELECT IDReserva FROM RESERVAS WHERE IDCab = @IDCab))
	
