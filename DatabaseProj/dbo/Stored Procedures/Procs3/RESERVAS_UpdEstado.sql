﻿CREATE Procedure [dbo].[RESERVAS_UpdEstado]
	@IDReserva	int,
	@IDEstado	char(2),
	@IDEmailModEstado	varchar(255),
	@CodReservaProv	varchar(50),
	@UserMod	char(4)
As
	Set NoCount On

	Update RESERVAS Set Estado=@IDEstado, FecMod=GETDATE(), UserMod=@UserMod,
	UserModEstado=@UserMod,
	IDEmailModEstado=Case When ltrim(rtrim(@IDEmailModEstado))='' Then Null Else @IDEmailModEstado End,
	CodReservaProv=Case When ltrim(rtrim(@CodReservaProv))='' Then Null Else @CodReservaProv End
	Where IDReserva=@IDReserva
