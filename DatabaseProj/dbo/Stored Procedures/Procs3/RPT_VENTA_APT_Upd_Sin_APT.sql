﻿CREATE PROCEDURE [RPT_VENTA_APT_Upd_Sin_APT]
	@NuAnio char(4),
	@CoMes char(2),
	@SsImporte_Sin_APT numeric,
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[RPT_VENTA_APT]
	Set
		[SsImporte_Sin_APT] = @SsImporte_Sin_APT,
 		[UserMod] = @UserMod,
 		[FecMod] = getdate()
 	Where 
		[NuAnio] = @NuAnio And 
		[CoMes] = @CoMes
END
