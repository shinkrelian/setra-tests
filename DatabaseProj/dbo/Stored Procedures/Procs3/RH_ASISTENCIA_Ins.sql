﻿

-----------------------------------------------------------------------------------------------------


CREATE PROCEDURE [RH_ASISTENCIA_Ins]
	--@NuAsistencia int OUT ,
	@CoTrabCargo tinyint,
	@CoTipoAsistencia tinyint,
	@FeFecha smalldatetime,
	@CoHorario tinyint,
	@FeMarcacionEntrada time,
	@FeMarcacionSalida time,
	@NuMinutosTarde tinyint,
	@NuHorasTrabajadas time,
	@NuSemana tinyint,
	@UserMod char(4)
AS
BEGIN
	declare @NuAsistencia int = null
	set @NuAsistencia=isnull((select MAX(nuasistencia) from RH_ASISTENCIA),0)+1
	Insert Into [dbo].[RH_ASISTENCIA]
		(
			NuAsistencia,
			[CoTrabCargo],
 			[CoTipoAsistencia],
 			[FeFecha],
 			[CoHorario],
 			[FeMarcacionEntrada],
 			[FeMarcacionSalida],
 			[NuMinutosTarde],
 			[NuHorasTrabajadas],
 			[NuSemana],
 			[UserMod]
 		)
	Values
		(
			@NuAsistencia,
			@CoTrabCargo,
 			@CoTipoAsistencia,
 			@FeFecha,
 			@CoHorario,
 			@FeMarcacionEntrada,
 			@FeMarcacionSalida,
 			@NuMinutosTarde,
 			@NuHorasTrabajadas,
 			@NuSemana,
 			@UserMod
 		)
        Select @NuAsistencia
END;
