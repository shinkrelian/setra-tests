﻿

-----------------------------------------------------


create PROCEDURE RH_HORARIO_Ins
	--@CoHorario tinyint,
	@NoHorario varchar(150)='',
	@UserMod char(4)='',
	@FeHorasRefrigerio time,
	@NuMaxHorasSemana tinyint
AS
BEGIN
	declare @CoHorario tinyint
	set @CoHorario=isnull((select MAX(CoHorario) from RH_HORARIO),0)+1
	Insert Into dbo.RH_HORARIO
		(
			CoHorario,
 			NoHorario,
 			UserMod,
 			FeHorasRefrigerio,
 			NuMaxHorasSemana
 		)
	Values
		(
			@CoHorario,
			Case When ltrim(rtrim(@NoHorario))='' Then Null Else @NoHorario End,
 			Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End,
 			@FeHorasRefrigerio,
 			@NuMaxHorasSemana
 		)
END;
