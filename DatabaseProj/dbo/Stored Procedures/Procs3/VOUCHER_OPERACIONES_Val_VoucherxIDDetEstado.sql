﻿
--JRF-20151102-Ajuste [...And vd.IDVoucher <> 0]
--PPMG-20151130-Se cambio el parametro de salida, a hora devuelve el ejecutivo de operaciones.
--PPMG-20151130-Se aumento la validación del estado en blanco.
CREATE Procedure [dbo].[VOUCHER_OPERACIONES_Val_VoucherxIDDetEstado]
	@IDDet  int,
	@CoEstado char(2),
	@TipoTable char(1)='C',
	@pValido varchar(150) output
	--@pValido bit output
As
Begin
	Set NoCount On
	SET @pValido = ''
	SELECT @CoEstado = CASE WHEN LTRIM(RTRIM(ISNULL(@CoEstado,''))) = '' THEN NULL ELSE @CoEstado END
	IF @TipoTable = 'C'
	BEGIN
		IF EXISTS (SELECT VD.IDVoucher, VD.CoEstado, CD.IDDet FROM VOUCHER_OPERACIONES_DET AS VD INNER JOIN
					OPERACIONES_DET AS OD ON VD.IDVoucher = OD.IDVoucher INNER JOIN RESERVAS_DET AS RD ON OD.IDReserva_Det = RD.IDReserva_Det
					INNER JOIN COTIDET CD ON RD.IDDet = CD.IDDET
					WHERE CD.IDDet = @IDDet And vd.IDVoucher <> 0 AND vd.CoEstado = COALESCE(@CoEstado,vd.CoEstado)
				   GROUP BY VD.IDVoucher, VD.CoEstado, CD.IDDet)
		BEGIN
			SELECT @pValido = Usu
			FROM (
			SELECT U.Usuario + ' - ' + U.Nombre + ' ' + ISNULL(U.TxApellidos,'') AS Usu
			FROM COTIDET AS CD INNER JOIN COTICAB AS C ON CD.IDCab = C.IDCAB
			INNER JOIN MAUSUARIOS AS U ON C.IDUsuarioOpe = U.IDUsuario
			WHERE CD.IDDet = @IDDet GROUP BY U.Usuario, U.Nombre, U.TxApellidos
			) T
		END
	END
	ELSE IF @TipoTable = 'R'
	BEGIN
		IF EXISTS (SELECT VD.IDVoucher, VD.CoEstado, RD.IDReserva_Det FROM VOUCHER_OPERACIONES_DET AS VD INNER JOIN
					OPERACIONES_DET AS OD ON VD.IDVoucher = OD.IDVoucher INNER JOIN RESERVAS_DET AS RD ON OD.IDReserva_Det = RD.IDReserva_Det
					WHERE RD.IDReserva_Det = @IDDet And vd.IDVoucher <> 0 AND vd.CoEstado = COALESCE(@CoEstado,vd.CoEstado)
				   GROUP BY VD.IDVoucher, VD.CoEstado, RD.IDReserva_Det)
		BEGIN
			SELECT @pValido = Usu
			FROM (
			SELECT U.Usuario + ' - ' + U.Nombre + ' ' + ISNULL(U.TxApellidos,'') AS Usu
			FROM RESERVAS_DET AS D INNER JOIN RESERVAS AS R ON R.IDReserva = D.IDReserva INNER JOIN
			COTICAB AS C ON R.IDCab = C.IDCAB INNER JOIN MAUSUARIOS AS U ON C.IDUsuarioOpe = U.IDUsuario
			WHERE D.IDReserva_Det = @IDDet GROUP BY U.Usuario, U.Nombre, U.TxApellidos
			) T
		END
	END
	ELSE IF @TipoTable = 'O'
	BEGIN
		IF EXISTS (SELECT VD.IDVoucher, VD.CoEstado, OD.IDOperacion_Det FROM VOUCHER_OPERACIONES_DET AS VD INNER JOIN
					OPERACIONES_DET AS OD ON VD.IDVoucher = OD.IDVoucher
					WHERE OD.IDOperacion_Det = @IDDet And vd.IDVoucher <> 0 AND vd.CoEstado = COALESCE(@CoEstado,vd.CoEstado))
		BEGIN
			SELECT @pValido = Usu
			FROM (
			SELECT U.Usuario + ' - ' + U.Nombre + ' ' + ISNULL(U.TxApellidos,'') AS Usu
			FROM OPERACIONES_DET AS OD INNER JOIN OPERACIONES AS OP ON OP.IDOperacion = OD.IDOperacion
			INNER JOIN COTICAB AS C ON OP.IDCab = C.IDCAB INNER JOIN MAUSUARIOS AS U ON C.IDUsuarioOpe = U.IDUsuario
			WHERE OD.IDOperacion_Det = @IDDet GROUP BY U.Usuario, U.Nombre, U.TxApellidos
			) T
		END
	END
End

