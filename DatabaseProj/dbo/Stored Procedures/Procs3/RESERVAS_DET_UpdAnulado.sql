﻿--JRF-20151123-Blanquear [NuViaticoTC]
CREATE Procedure dbo.RESERVAS_DET_UpdAnulado  
 @IDReserva_Det int,  
 @UserMod char(4),  
 @Anulado bit  
As  
 Set NoCount On  
   
 Update RESERVAS_DET Set NuViaticoTC=0 ,Anulado=@Anulado, UserMod=@UserMod, FecMod=GETDATE() ,FeUpdateRow=Null
 Where IDReserva_Det=@IDReserva_Det  
