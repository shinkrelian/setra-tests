﻿
Create Procedure dbo.RESERVAS_DET_UpdNochesFinal
	@IDDet int,
	@UserMod char(4)
As
	Set NoCount On      
	
	Update RESERVAS_DET Set NetoGen=NetoHab*CantidadAPagar*Noches,
	UserMod=@UserMod, FecMod=GETDATE()      
	Where IDDet=@IDDet	
	
	Update RESERVAS_DET Set TotalGen = NetoGen + IgvGen,    
	UserMod=@UserMod, FecMod=GETDATE()      
	Where IDDet=@IDDet  
