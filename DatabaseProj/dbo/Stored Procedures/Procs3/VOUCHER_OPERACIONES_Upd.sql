﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--HLF-20130813-Agregando parametro @ObservVoucher
--JHD-20141211-Agregando parametro @CoUbigeo_Oficina
CREATE Procedure dbo.VOUCHER_OPERACIONES_Upd  
 @IDVoucher int,  
 @ServExportac bit,  
 @ExtrNoIncluid bit,  
 @ObservVoucher varchar(max),
 @CoUbigeo_Oficina char(6)='',
 @UserMod char(4)  
As  
 Set Nocount On   
   
 Update VOUCHER_OPERACIONES  
 Set ServExportac=@ServExportac,ExtrNoIncluid=@ExtrNoIncluid,
 ObservVoucher=case when ltrim(rtrim(@ObservVoucher))='' then null else @ObservVoucher end,
 CoUbigeo_Oficina=Case When ltrim(rtrim(@CoUbigeo_Oficina))='' Then CoUbigeo_Oficina /*Null*/ Else @CoUbigeo_Oficina End,
 UserMod=@UserMod, FecMod=getdate()  
 Where IDVoucher=@IDVoucher  
 
