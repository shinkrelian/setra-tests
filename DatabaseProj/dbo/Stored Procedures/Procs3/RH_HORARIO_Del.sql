﻿
------------------------------------------------------------------------------------------

CREATE PROCEDURE RH_HORARIO_Del
	@CoHorario tinyint
AS
BEGIN
	Delete dbo.RH_HORARIO
	Where 
		CoHorario = @CoHorario
END
