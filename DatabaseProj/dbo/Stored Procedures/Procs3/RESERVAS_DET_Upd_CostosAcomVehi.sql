﻿CREATE Procedure dbo.RESERVAS_DET_Upd_CostosAcomVehi
	@IDCab int,
	@CoProveedor char(6),
	@UserMod char(4)
As
	--Set Nocount On

	--Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)        
	
	Update RESERVAS_DET Set 		
		NetoHab=y.CostoReal, 
		IgvHab=y.TotImpto,
		TotalHab=y.CostoReal+y.TotImpto,
		
		NetoGen=y.CostoReal * (NroPax+ISNULL(NroLiberados,0)),
		IgvGen=y.TotImpto * (NroPax+ISNULL(NroLiberados,0)),
		TotalGen=(y.CostoReal+y.TotImpto) * (NroPax+ISNULL(NroLiberados,0)),
		FecMod=GETDATE(),
		UserMod=@UserMod
	From
		(
		Select  x.IDReserva_Det,
		x.TotImpto ,
		x.CostoReal
		From 
		(Select ods.IDReserva_Det, 
		
		sum(ods.TotImpto) as TotImpto,
		Sum(ods.CostoReal) as CostoReal
		From RESERVAS_DETSERVICIOS ODS 
		--Left Join MASERVICIOS_DET dCot On ods.IDServicio_Det=dCot.IDServicio_Det                                                
		Inner Join RESERVAS_DET od On ods.IDReserva_Det=od.IDReserva_Det
		Inner Join MASERVICIOS_DET sd On ods.IDServicio_Det=sd.IDServicio_Det        
		Inner Join RESERVAS o On od.IDReserva=o.IDReserva And o.IDCab=@IDCab And o.IDProveedor=@CoProveedor
			And od.Anulado=0 And o.Anulado=0
		--Inner Join ACOMODO_VEHICULO_DET_PAX avp On avp.IDReserva_Det=od.IDReserva_Det
		Group by ods.IDReserva_Det
		) as X
		--Inner Join RESERVAS_DET od On od.IDReserva_Det  In (Select IDReserva_Det From RESERVAS_DET od1 
		--Inner Join RESERVAS o1 On od1.IDReserva=o1.IDReserva And o1.IDCab=@IDCab And o1.IDProveedor=@IDProveedor)
		  
		) as Y INNER JOIN RESERVAS_DET ON RESERVAS_DET.IDReserva_Det = y.IDReserva_Det
		WHERE RESERVAS_DET.IDReserva_Det In 
			(Select IDReserva_Det From ACOMODO_VEHICULO_DET_PAX av Inner Join COTIDET cd On av.IDDet=cd.iddet	
				And cd.IDCAB=@IDCab)



