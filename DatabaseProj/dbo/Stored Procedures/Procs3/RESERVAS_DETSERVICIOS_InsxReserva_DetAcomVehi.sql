﻿
CREATE Procedure dbo.RESERVAS_DETSERVICIOS_InsxReserva_DetAcomVehi
	@IDReserva_Det	int,
	@IDReserva_DetNew	int,
	@UserMod	char(4)
As
	Set Nocount On	

	INSERT INTO RESERVAS_DETSERVICIOS
           ([IDReserva_Det]
           ,[IDServicio_Det]
           ,[CostoReal]
           ,[CostoLiberado]
           ,[Margen]
           ,[MargenAplicado]
           ,[MargenLiberado]
           ,[Total]
           ,[SSCR]
           ,[SSCL]
           ,[SSMargen]
           ,[SSMargenLiberado]
           ,[SSTotal]
           ,[STCR]
           ,[STMargen]
           ,[STTotal]
           ,[CostoRealImpto]
           ,[CostoLiberadoImpto]
           ,[MargenImpto]
           ,[MargenLiberadoImpto]
           ,[SSCRImpto]
           ,[SSCLImpto]
           ,[SSMargenImpto]
           ,[SSMargenLiberadoImpto]
           ,[STCRImpto]
           ,[STMargenImpto]
           ,[TotImpto]
           ,[UserMod])           
	Select @IDReserva_DetNew
           ,[IDServicio_Det]
           ,[CostoReal]
           ,[CostoLiberado]
           ,[Margen]
           ,[MargenAplicado]
           ,[MargenLiberado]
           ,[Total]
           ,[SSCR]
           ,[SSCL]
           ,[SSMargen]
           ,[SSMargenLiberado]
           ,[SSTotal]
           ,[STCR]
           ,[STMargen]
           ,[STTotal]
           ,[CostoRealImpto]
           ,[CostoLiberadoImpto]
           ,[MargenImpto]
           ,[MargenLiberadoImpto]
           ,[SSCRImpto]
           ,[SSCLImpto]
           ,[SSMargenImpto]
           ,[SSMargenLiberadoImpto]
           ,[STCRImpto]
           ,[STMargenImpto]
           ,[TotImpto]
           ,@UserMod                      
		From RESERVAS_DETSERVICIOS 
		Where IDReserva_Det=@IDReserva_Det

