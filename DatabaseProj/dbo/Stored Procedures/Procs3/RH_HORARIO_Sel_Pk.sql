﻿
------------------------------------------------------------------------------------------

CREATE PROCEDURE RH_HORARIO_Sel_Pk
	@CoHorario tinyint=0
AS
BEGIN
	Select
		CoHorario,
 		NoHorario,
 		FeHorasRefrigerio,
 		NuMaxHorasSemana,
 		FlActivo
 	From dbo.RH_HORARIO
	Where 
		(CoHorario = @CoHorario)
END;

