﻿
--HLF-20151103-@IDServicio char(8)
--HLF-20151229-NuOperBan
--HLF-20160108-QtStSaldo
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_INGRESOS_Ins
	@IDServicio char(8),
	@QttkComprado smallint,
	@FeComprada smalldatetime,
	@FlSustentado bit,
	@IDTipoDocSus char(3),
	@NroDocSus varchar(15),
	@NuOperBan varchar(20),
	@UserMod char(4)
As
	Set NoCount On
	Declare @NuTckEntIng smallint= IsNull((Select Count(*) from STOCK_TICKET_ENTRADAS_INGRESOS Where IDServicio=@IDServicio),0)+1
	Insert into STOCK_TICKET_ENTRADAS_INGRESOS(NuTckEntIng,IDServicio,QttkComprado,FeComprada,FlSustentado,IDTipoDocSus,
		NroDocSus,NuOperBan,QtStSaldo,UserMod)
	Values(@NuTckEntIng,@IDServicio,@QttkComprado,@FeComprada,@FlSustentado,
		   Case When Ltrim(Rtrim(@IDTipoDocSus))='' Then Null Else @IDTipoDocSus End,
		   Case When Ltrim(Rtrim(@NroDocSus))='' Then Null Else @NroDocSus End,
		   Case When Ltrim(Rtrim(@NuOperBan))='' Then Null Else @NuOperBan End,
		   @QttkComprado,
		   @UserMod)

