﻿create PROCEDURE [dbo].[RH_HORARIO_Sel_Correlativo]
	
AS
BEGIN
	declare @CoHorario tinyint
	set @CoHorario=isnull((select MAX(CoHorario) from RH_HORARIO),0)+1
	select @CoHorario
END;
