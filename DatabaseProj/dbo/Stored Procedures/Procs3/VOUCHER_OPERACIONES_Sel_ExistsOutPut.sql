﻿Create Procedure dbo.VOUCHER_OPERACIONES_Sel_ExistsOutPut
@IDVoucher int,
@pExiste bit output
As
	Set @pExiste = 0
	If Exists(select IDVoucher from VOUCHER_OPERACIONES where IDVoucher=@IDVoucher)
		Set @pExiste=1
