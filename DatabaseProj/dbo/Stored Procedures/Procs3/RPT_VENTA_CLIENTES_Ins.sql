﻿--JHD-20150115-Se cambio tipo de dato @QtNumFiles a int
--JHD-20150320- Se agregaron los campos @QtDias int=0, @QtEdad_Prom int=0
CREATE PROCEDURE RPT_VENTA_CLIENTES_Ins
	@NuAnio char(4)='',
	--@CoMes tinyint=0,
	@CoCliente char(6)='',
	@SsImporte numeric(12,4)=0,
	@QtNumFiles int=0,
	@SsPromedio numeric(12,4)=0,
	@SsUtilidad numeric(12,4)=0,
	@CoTipo char(1)='',
	@UserMod char(4)='',
	@QtDias int=0,
	@QtEdad_Prom int=0
AS
BEGIN
	Insert Into dbo.RPT_VENTA_CLIENTES
		(
			NuAnio,
 			--CoMes,
 			CoCliente,
 			SsImporte,
 			QtNumFiles,
 			SsPromedio,
 			SsUtilidad,
 			CoTipo,
 			UserMod,
 			FecMod,
 			QtDias,
			QtEdad_Prom
 		)
	Values
		(
			case when LTRIM(RTRIM(@NuAnio))='' then Null Else @NuAnio End,
 			--case when @CoMes=0 then Null Else @CoMes End,
 			case when LTRIM(RTRIM(@CoCliente))='' then Null Else @CoCliente End,
 			case when @SsImporte=0 then Null Else @SsImporte End,
 			case when @QtNumFiles=0 then Null Else @QtNumFiles End,
 			case when @SsPromedio=0 then Null Else @SsPromedio End,
 			case when @SsUtilidad=0 then Null Else @SsUtilidad End,
 			case when LTRIM(RTRIM(@CoTipo))='' then Null Else @CoTipo End,
 			case when LTRIM(RTRIM(@UserMod))='' then Null Else @UserMod End,
 			getdate(),
 			case when @QtDias=0 then Null Else @QtDias End, --@QtDias int=0,
			case when @QtEdad_Prom=0 then Null Else @QtEdad_Prom End
 		)
END;

