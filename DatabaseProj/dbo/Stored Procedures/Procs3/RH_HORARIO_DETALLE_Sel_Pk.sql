﻿
------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.RH_HORARIO_DETALLE_Sel_Pk
	@CoHorario tinyint=0,
	@CoDetalle tinyint=0
AS
SELECT     --RH_HORARIO_DETALLE.CoHorario,
RH_HORARIO_DETALLE.CoDetalle,
--RH_HORARIO.NoHorario,
RH_HORARIO_DETALLE.NuDia, 
NoDia=dbo.FnDescripcionDiaSemana(RH_HORARIO_DETALLE.NuDia),
                      RH_HORARIO_DETALLE.FeHoraInicio,
                      RH_HORARIO_DETALLE.FeHoraFin--,
                      --RH_HORARIO_DETALLE.FlActivo
FROM         RH_HORARIO INNER JOIN
                      RH_HORARIO_DETALLE ON RH_HORARIO.CoHorario = RH_HORARIO_DETALLE.CoHorario
Where 
		RH_HORARIO_DETALLE.CoHorario = @CoHorario;
