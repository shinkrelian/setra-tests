﻿
--------------------------------------------


CREATE PROCEDURE [RH_AREA_Upd]
	@CoArea tinyint,
	@NoArea varchar(150)='',
	@UserMod char(4)=''
AS
BEGIN
	Update [dbo].[RH_AREA]
	Set
		[NoArea] = Case When ltrim(rtrim(@NoArea))='' Then Null Else @NoArea End,
 		[UserMod] = Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End,
 		[FecMod] = GETDATE()
 	Where 
		[CoArea] = @CoArea
END;
