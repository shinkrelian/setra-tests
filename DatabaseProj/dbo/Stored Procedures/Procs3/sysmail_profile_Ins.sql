﻿
Create Procedure dbo.sysmail_profile_Ins
	@name	nvarchar(128),
	--@UserMod	char(4),
	@pprofile_id int output
As
	Set NoCount On  
	Insert Into msdb.dbo.sysmail_profile
	(name,last_mod_datetime)
	Values(@name,GETDATE())

	Set @pprofile_id =(Select MAX(profile_id) From msdb.dbo.sysmail_profile)
