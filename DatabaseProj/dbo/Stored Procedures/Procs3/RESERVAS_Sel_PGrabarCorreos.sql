﻿Create Procedure dbo.RESERVAS_Sel_PGrabarCorreos
As
Set NoCount On
select distinct r.IDProveedor,r.IDCab,r.IDFile,c.IDUsuarioRes,u.Usuario as PerfilCorreo,
u.Correo as CorreoUsuario
from RESERVAS r Left Join COTICAB c On r.IDCab = c.IDCAB 
Left Join MAUSUARIOS u On c.IDUsuarioRes = u.IDUsuario
where (c.Estado <> 'X' AND c.Estado <> 'E') and
r.Estado <> 'XL' and r.Anulado = 0 and c.IDUsuarioRes <> '0000' and ISNULL(r.IDFile,'') <> ''
