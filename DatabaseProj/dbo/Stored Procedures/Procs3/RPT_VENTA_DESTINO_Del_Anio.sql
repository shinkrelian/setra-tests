﻿
--CREATE
CREATE PROCEDURE [RPT_VENTA_DESTINO_Del_Anio]
	@NuAnio char(4)
AS
BEGIN
	Delete [dbo].[RPT_VENTA_DESTINO]
	Where 
		[NuAnio] = @NuAnio
END
