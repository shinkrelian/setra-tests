﻿Create Procedure dbo.RptInvoiceRangoAPT_InsxTMP
@IDCab int,
@IDTemporada int
As
Begin
Set NoCount On
Declare @NuRptInvoiceAPT int = 0
Select Top(1) @NuRptInvoiceAPT = NuRptInvoiceAPT from RptInvoiceRangoAPT_Tmp
Where IDCAB=@IDCab And IDTemporada=@IDTemporada
Order By VersionRpt Desc

If Exists(select NuRptInvoiceAPT from RptInvoiceRangoAPT Where NuRptInvoiceAPT=@NuRptInvoiceAPT)
Begin
	Declare @NuRptInvoiceRangoAPT2 int = 0
	Select @NuRptInvoiceRangoAPT2 = IsNull(Max(NuRptInvoiceAPT),0)+1  from RptInvoiceRangoAPT

	if @NuRptInvoiceRangoAPT2 > 0
	Begin
		--Update RptInvoiceRangoAPT Set Valid=0 Where NuRptInvoiceAPT=@NuRptInvoiceAPT

		Insert into RptInvoiceRangoAPT  
		Select @NuRptInvoiceRangoAPT2,IDCAB,@IDTemporada, IDFile, Titulo, NroPax, NroLiberados, FlIncluirTC, Simple, Twin, Matrimonial, Triple, NombreTC, 
		TD_DAYS, C_SWB, C_TWIN, C_MAT, C_TRIP, IDDebitMemo,VersionRpt+1,Changed,NuRptInvoiceAPTAntiguo
		from RptInvoiceRangoAPT_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT And Changed=1

		Insert into RptInvoiceRangoAPT_Det
		Select (Select IsNull(Max(NuRptInvoiceDetPT),0) from RptInvoiceRangoAPT_Det) + ROW_NUMBER()Over(ORDER BY TipG, DiaRes, Dia, Item) As NuDet,
			   @NuRptInvoiceAPT,IDCAB,IDDet,NroOrdenIDDet,Item,sFecha,Dia,DiaRes,Ubigeo,Servicio,IDTipoServ,IDTipoProv,sNumerAPT,NroPax_RD,NroLiberados_RD,
			   Total_RD,Monto,Total,DescripcionAlterna,TipG,VersionRpt+1,ChangedDet,NuRptInvoiceDetPTAntiguo
		from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT And ChangedDet=1
	End
End
Else
Begin
	Insert into RptInvoiceRangoAPT
	Select * from RptInvoiceRangoAPT_Tmp where NuRptInvoiceAPT=@NuRptInvoiceAPT

	Insert into RptInvoiceRangoAPT_Det
	Select * from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT
End
End
