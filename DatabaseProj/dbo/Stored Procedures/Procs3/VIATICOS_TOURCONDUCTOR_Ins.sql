﻿--JRF-20151118-Agregar campo Activo
CREATE Procedure dbo.VIATICOS_TOURCONDUCTOR_Ins
@Dia smalldatetime,
@IDCab int,
@IDServicio_Det int,
@Item smallint,
@CostoUSD numeric(8,2),
@CostoSOL numeric(8,2),
@FlActivo bit,
@UserMod char(4)
As
Set NoCount On
Declare @NuMaxViaticoTC int = IsNull((select Max(NuViaticoTC) from VIATICOS_TOURCONDUCTOR),0) + 1

Insert into VIATICOS_TOURCONDUCTOR
values(@NuMaxViaticoTC,@Dia,@IDCab,@IDServicio_Det,@Item,@CostoUSD,@CostoSOL,@UserMod,getdate(),@FlActivo)
