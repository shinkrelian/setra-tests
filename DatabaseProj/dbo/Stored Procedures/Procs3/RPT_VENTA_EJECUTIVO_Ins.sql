﻿
--------------------------------------------------------------


--ALTER
--JHD-20150115-Se cambio tipo de dato @QtNumFiles a int
CREATE PROCEDURE RPT_VENTA_EJECUTIVO_Ins
	@NuAnio char(4)='',
	@CoUserEjecutivo char(4)='',
	@SsImporte numeric(12,4)=0,
	@QtNumFiles int=0,
	@UserMod char(4)=''
AS
BEGIN
	Insert Into dbo.RPT_VENTA_EJECUTIVO
		(
			NuAnio,
 			CoUserEjecutivo,
 			SsImporte,
 			QtNumFiles,
 			UserMod,
 			FecMod
 		)
	Values
		(
			case when LTRIM(RTRIM(@NuAnio))='' then Null Else @NuAnio End,   
 			case when LTRIM(RTRIM(@CoUserEjecutivo))='' then Null Else @CoUserEjecutivo End,
 			case when @SsImporte=0 then Null Else @SsImporte End,
 			case when @QtNumFiles=0 then Null Else @QtNumFiles End,
 			case when LTRIM(RTRIM(@UserMod))='' then Null Else @UserMod End,
 			GETDATE()
 		)
END;
