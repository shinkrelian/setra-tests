﻿Create Procedure dbo.TICKET_AMADEUS_Sel_List
	@CoTicket char(14),
	@TxFechaEmisionIni smalldatetime,
	@TxFechaEmisionFin smalldatetime,
	@IDFile varchar(8),
	@DescCliente varchar(80),
	@TxFechaParIni smalldatetime,
	@TxFechaParFin smalldatetime
As
	Set Nocount On
	
	Select t.CoTicket,cc.IDFile, 
	isnull(c.RazonComercial,cf.RazonComercial) as DescCliente,	
	t.TxFechaEmision,
	(Select Top 1 TxFechaPar From TICKET_AMADEUS_ITINERARIO Where CoTicket=t.CoTicket Order by NuSegmento) as FechaViaje,
	(Select Top 1 TxHoraLle From TICKET_AMADEUS_ITINERARIO Where CoTicket=t.CoTicket Order by NuSegmento Desc) as FechaRetorno,
	t.NoLineaAerea,t.CoLineaAereaEmisora,t.CoLineaAereaRecord,
	t.TxTotal,
	t.CoDocumIdent, t.NuDocumIdent, t.NoPax
	From TICKET_AMADEUS t 
	Left Join COTICAB cc On isnull(t.IDCab,0)=cc.IDCAB
	Left Join MACLIENTES c On isnull(t.IDCliente,'')=c.IDCliente
	Left Join MACLIENTES cf On isnull(cc.IDCliente,'')=c.IDCliente
	Inner Join TICKET_AMADEUS_ITINERARIO td On t.CoTicket=td.CoTicket
	Where (t.CoTicket Like '%'+ltrim(rtrim(@CoTicket))+'%' Or ltrim(rtrim(@CoTicket))='') And
	((t.TxFechaEmision Between @TxFechaEmisionIni And @TxFechaEmisionFin) 
		Or @TxFechaEmisionIni='01/01/1900') And
	(isnull(c.RazonComercial,cf.RazonComercial) Like ''+@DescCliente+'' Or 
		ltrim(rtrim(@DescCliente))='') And 
	((td.TxFechaPar Between @TxFechaParIni And DATEADD(S,-1,DATEADD(D,1,CONVERT(VARCHAR,@TxFechaParFin,103)))) 
		Or @TxFechaParIni='01/01/1900')	And 
	(cc.IDFile Like '%'+ltrim(rtrim(@IDFile))+'%' Or ltrim(rtrim(@IDFile))='') 
	Order by t.TxFechaEmision
	
