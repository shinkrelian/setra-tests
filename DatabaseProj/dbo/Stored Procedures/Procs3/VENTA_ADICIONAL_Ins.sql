﻿--HLF-20160217-@CoTipo char(2),
CREATE Procedure [dbo].[VENTA_ADICIONAL_Ins]
	@IDCab int,
	@QtPax smallint,	
	@CoUbigeo char(6),
	@CoTipo char(2),
	@UserMod char(4),
	@pNuVtaAdi int output
As
	Set Nocount On

	Declare @NuVtaAdi int
	Execute dbo.Correlativo_SelOutput 'VENTA_ADICIONAL',1,10,@NuVtaAdi output    

	Insert Into VENTA_ADICIONAL(
	NuVtaAdi,
	IDCab,	
	QtPax,	
	CoUbigeo,	
	CoTipo,
	UserMod)
	Values
	(@NuVtaAdi,
	@IDCab,	
	@QtPax,	
	Case When ltrim(rtrim(@CoUbigeo))='' Then Null Else @CoUbigeo End,	
	@CoTipo,
	@UserMod)

	Set @pNuVtaAdi=@NuVtaAdi


