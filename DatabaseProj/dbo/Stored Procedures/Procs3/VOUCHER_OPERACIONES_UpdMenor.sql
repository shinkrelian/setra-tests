﻿--JRF-20141110-Se debe actualizar el Correlativo de la tabla CORRELATIVOS.
CREATE Procedure dbo.VOUCHER_OPERACIONES_UpdMenor  
 @IDVoucherNuevo int,    
 @IDVoucherAntiguo int,    
 @ServExportac bit,    
 @ExtrNoIncluid bit,    
 @UserMod char(4)    
As    
Begin
 Set Nocount On     
     
 Update VOUCHER_OPERACIONES    
 Set IDVoucher=@IDVoucherNuevo, ServExportac=@ServExportac,ExtrNoIncluid=@ExtrNoIncluid,UserMod=@UserMod, FecMod=getdate()    
 Where IDVoucher=@IDVoucherAntiguo    
 
 Update Correlativo set Correlativo=(select MAX(IDVoucher) from VOUCHER_OPERACIONES) where Tabla='VOUCHER_OPERACIONES'
End;
