﻿Create Procedure dbo.SINCERADO_SelExistOutPut  
@IDOperacion_Det int,  
@IDServicio_Det int,  
@pExiste bit output  
As  
Set NoCount On  
set @pExiste = 0  
if Exists(select NuSincerado from SINCERADO   
    where IDOperacion_Det =@IDOperacion_Det and IDServicio_Det = @IDServicio_Det)  
 set @pExiste = 1  
