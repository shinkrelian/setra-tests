﻿Create Procedure dbo.SAP_DOCUMENTO_Sel_DocsVinculados
	@IDTipoDoc char(3),
	@NuDocum char(10)
As
	Set Nocount on

	Select isnull(td.CoSAP,'') as U_SYP_MDTD, 	Left(d.NuDocum,3) as U_SYP_MDSD,
	Right(d.NuDocum,7) as U_SYP_MDCD,
	Case When d.IDMoneda='USD' then
		0
	Else
		d.SsTotalDocumUSD-d.SsIGV
	End	as GrossAmountToDraw,--Soles
	Case When d.IDMoneda='USD' then
		d.SsTotalDocumUSD-d.SsIGV
	else
		0
	end as GrossAmountToDrawFC, --Dolares
	Case When d.IDMoneda='USD' then
		0
	Else
		d.SsTotalDocumUSD-d.SsIGV
	End	as AmountToDraw,--Soles
	Case When d.IDMoneda='USD' then
		d.SsTotalDocumUSD-d.SsIGV
	else
		0
	end as AmountToDrawFC --Dolares
	From DOCUMENTO d Inner Join MATIPODOC td On td.IDtipodoc=d.IDTipoDoc
	Where d.IDTipoDoc+' '+d.NuDocum In 
		(Select IDTipoDocVinc+' '+NuDocumVinc From DOCUMENTO Where IDTipoDoc=@IDTipoDoc And NuDocum=@NuDocum)
