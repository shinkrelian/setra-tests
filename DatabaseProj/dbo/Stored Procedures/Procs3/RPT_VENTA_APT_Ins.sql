﻿--JHD-20150115-Se cambio tipo de dato @QtNumFiles a int
--JHD-20150115-Se cambio tipo de dato @QtPax a int
--JHD-20150116- Se cambió el campo @CoMes a char(2)
--JHD-20150320- Se agregaron los campos @QtDias int=0, @QtEdad_Prom int=0
CREATE PROCEDURE [dbo].[RPT_VENTA_APT_Ins]
	@NuAnio char(4)='',
	@CoMes char(2)='',
	@SsImporte numeric(12,4)=0,
	@SsImporte_Sin_APT numeric(12,4)=0,
	@QtPax int=0,
	@QtNumFiles int=0,
	@UserMod char(4)='',
	@QtDias int=0,
	@QtEdad_Prom int=0
AS
BEGIN
	Insert Into dbo.RPT_VENTA_APT
		(
			NuAnio,
 			CoMes,
 			SsImporte,
 			SsImporte_Sin_APT,
 			QtPax,
 			QtNumFiles,
 			UserMod,
 			FecMod,
 			QtDias,
			QtEdad_Prom
 		)
	Values
		(
			case when LTRIM(RTRIM(@NuAnio))='' then Null Else @NuAnio End,
			case when LTRIM(RTRIM(@CoMes))='' then Null Else @CoMes End,
 			case when @SsImporte=0 then Null Else @SsImporte End,
 			case when @SsImporte_Sin_APT=0 then Null Else @SsImporte_Sin_APT End,
 			case when @QtPax=0 then Null Else @QtPax End,
 			case when @QtNumFiles=0 then Null Else @QtNumFiles End,
 			case when LTRIM(RTRIM(@UserMod))='' then Null Else @UserMod End,
 			GETDATE(),
 			case when @QtDias=0 then Null Else @QtDias End, --@QtDias int=0,
			case when @QtEdad_Prom=0 then Null Else @QtEdad_Prom End
 		)
END;

