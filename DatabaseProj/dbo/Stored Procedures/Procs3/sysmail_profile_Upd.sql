﻿Create Procedure dbo.sysmail_profile_Upd
	@profile_id int,
	@name	nvarchar(128)	
As
	Set NoCount On  
	Update msdb.dbo.sysmail_profile
	Set name=@name,last_mod_datetime=getdate()
	Where profile_id = @profile_id 
	
