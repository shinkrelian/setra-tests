﻿
Create Procedure dbo.RESERVAS_SelEstadoxIDDet
	@IDDet int,
	@pCoEstado char(2) output
As
	Set Nocount On

	Set @pCoEstado=''
	Select Distinct @pCoEstado=r.Estado From RESERVAS r Inner Join RESERVAS_DET rd On r.IDReserva=rd.IDReserva
	And rd.IDDet=@IDDet and rd.Anulado=0

