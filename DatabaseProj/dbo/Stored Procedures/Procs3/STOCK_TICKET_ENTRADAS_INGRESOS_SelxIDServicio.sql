﻿--HLF-20151229-NuOperBan
--HLF-20160120-QtStSaldo
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_INGRESOS_SelxIDServicio
	@IDServicio char(8)
As
	Set NoCount On
	Select NuTckEntIng,FeComprada,QttkComprado,FlSustentado,
			Case When FlSustentado = 1 Then 'Sustentado' Else 'No Sustentado' End as EstadoSus,
			QtStSaldo,
			IsNull(IDTipoDocSus,'') As IDTipoDocSus,IsNull(NroDocSus,'') As NroDocSus,
			isnull(NuOperBan,'') as NuOperBan
	from STOCK_TICKET_ENTRADAS_INGRESOS
	Where IDServicio=@IDServicio

