﻿
----------------------------------------------------------------------------------------------

--CREATE
CREATE PROCEDURE RPT_VENTA_EJECUTIVO_Del_Anio
	@NuAnio char(4)=''
AS
BEGIN
	Delete dbo.RPT_VENTA_EJECUTIVO
	Where 
		NuAnio = @NuAnio 
END;
