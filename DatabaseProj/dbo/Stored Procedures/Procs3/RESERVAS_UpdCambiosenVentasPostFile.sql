﻿Create Procedure dbo.RESERVAS_UpdCambiosenVentasPostFile
	@IDReserva int,
	@CambiosenVentasPostFile bit,
	@UserMod char(4)
As
	Set Nocount On
	
	Update RESERVAS Set CambiosenVentasPostFile=@CambiosenVentasPostFile, 
	UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva=@IDReserva
	
