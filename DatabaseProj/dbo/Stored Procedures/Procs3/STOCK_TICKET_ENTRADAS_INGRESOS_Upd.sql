﻿
--HLF-20151103-@IDServicio char(8)
--HLF-20151229-NuOperBan
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_INGRESOS_Upd
@NuTckEntIng smallint,
@IDServicio char(8),
@FlSustentado bit,
@IDTipoDocSus char(3),
@NroDocSus varchar(15),
@NuOperBan varchar(20),
@UserMod char(4)
As
Set NoCount On
Update STOCK_TICKET_ENTRADAS_INGRESOS
	   Set FlSustentado = @FlSustentado,
		   IDTipoDocSus = Case When Ltrim(Rtrim(@IDTipoDocSus))='' Then Null Else Ltrim(Rtrim(@IDTipoDocSus)) End,
		   NroDocSus = Case When Ltrim(Rtrim(@NroDocSus))='' Then Null Else Ltrim(Rtrim(@NroDocSus)) End,
		   NuOperBan = Case When Ltrim(Rtrim(@NuOperBan))='' Then Null Else @NuOperBan End,
		   UserMod = @UserMod,
		   FecMod = GETDATE()
Where IDServicio=@IDServicio And NuTckEntIng=@NuTckEntIng

