﻿--HLF-20150902-Case When Isnull(d.IDCliente,c.IDCliente)='000054' Then--APT ...
--Case When Isnull(d.IDCliente,c.IDCliente) in('001589','001864') Then--Translivik ...
--D.FeEmisionVinc as U_SYP_FECHAREF,
--HLF-20150909-Case when LEFT(@NuDocum,3)='005' Then..
--HLF-20150914-	Select x.*,	Case When U_SYP_TCTMP=0 then dbo.FnTipoDeCambioVtaUltimoxFecha(getdate()) else U_SYP_TCTMP end as U_SYP_TC
--HLF-20150929-Case when d.IDTipoDoc in('NCR','NCA') then...
--dbo.FnCambioMoneda(SsTotalDocumUSD,IDMoneda,'SOL',U_SYP_TC) ... as DocTotal, DocTotalFc
--HLF-20151001-Case when LEFT(@NuDocum,3)='005' Then--anticipos   as  U_SYP_STATUS
--Case when LEFT(@NuDocum,3)='005' Then--anticipos   as  CardCode
--HLF-20151006-isnull((select isnull(c1.CardCodeSAP,'') from DEBIT_MEMO dm1 ...
--HLF-20151015-... as U_SYP_DOCEXPORT
--Case when d.IDTipoDoc <> 'CBR' then --<> Doc. Cobranza Else ...
--JHD-20151112-case when d.IDTipoDoc='TIP' THEN '000' ELSE Left(d.NuDocum,3) END as U_SYP_MDSD,
--JHD-20151112-case when d.IDTipoDoc='TIP' THEN d.NuDocum ELSE Right(d.NuDocum,7) END as U_SYP_MDCD,
--JHD-20151113-JournalMemo=([dbo].[FnDocumento_GetTexto_Concatenado](d.idtipodoc, d.nudocum) )
--JHD-20151113-Comments=([dbo].[FnDocumento_GetTexto_Concatenado](d.idtipodoc, d.nudocum) )
--HLF-20160112-@cTipoAnul char(1)=''   Case When ltrim(rtrim(@cTipoAnul)) = '' Then
CREATE Procedure [dbo].[SAP_DOCUMENTO_Sel_Pk]
	@IDTipoDoc char(3),
	@NuDocum char(10),
	@cTipoAnul char(1)=''
As
	Set nocount on

	--Declare @SsTipoCambio numeric(8,3)=dbo.FnTipoDeCambioVtaUltimoxFecha(getdate())	

	SELECT Y.*,
	Case When IDMoneda='USD' then
		dbo.FnCambioMoneda(SsTotalDocumUSD,IDMoneda,'SOL',U_SYP_TC)
	Else
		SsTotalDocumUSD
	End	as DocTotal,--Soles
	Case When IDMoneda='USD' then
		SsTotalDocumUSD
	else
		dbo.FnCambioMoneda(SsTotalDocumUSD,IDMoneda,'USD',U_SYP_TC)
	end as DocTotalFc --Dolares 
	FROM
		(
		Select x.*,

	
			Case When U_SYP_TCTMP=0 then dbo.FnTipoDeCambioVtaUltimoxFecha(getdate()) else U_SYP_TCTMP end as U_SYP_TC
		From
		(
		Select 
		d.FeDocum as TaxDate, d.FeDocum as DocDate , 
		--isnull((select Top 1 FecVencim from DEBIT_MEMO Where IDCab=d.idcab),d.FeDocum) as DocDueDate,
		d.FeDocum as DocDueDate,
		--isnull(cl.CardCodeSAP,'') as CardCode, 
		Case when LEFT(@NuDocum,3)='005' Then--anticipos
			--select isnull(c1.CardCodeSAP,'') from DEBIT_MEMO dm1 Inner Join MACLIENTES c1 On dm1.IDCliente=c1.IDCliente 
				-- Where IDDebitMemo=d.CoDebitMemoAnticipo
			isnull((select isnull(c1.CardCodeSAP,'') from DEBIT_MEMO dm1 Inner Join MACLIENTES c1 On dm1.IDCliente=c1.IDCliente 
				 Where IDDebitMemo=d.CoDebitMemoAnticipo),isnull(cl.CardCodeSAP,''))
		else
			isnull(cl.CardCodeSAP,'') 
		End as CardCode, 
		--'C'+cl.NumIdentidad as CardCode, 
		Case When cl.IDCiudad='000323' Then
			isnull(cl.NumIdentidad,'') 
		Else
			'CEX0000'+Isnull(d.IDCliente,c.IDCliente)	
		End as FederalTaxID, 
		--isnull(cl.NumIdentidad,'')  as FederalTaxID, 
		mon.CoSAP as DocCurrency,
		--isnull(d.CoCtaContable,'') as ControlAccount, 
		--Case When d.IDMoneda='SOL' then		'12121001' 	Else		'12122001' 	End as ControlAccount, 

		Case when d.IDTipoDoc <> 'CBR' then --<> Doc. Cobranza
			Case when LEFT(@NuDocum,3)='005' Then--anticipos
				Case When Isnull(d.IDCliente,c.IDCliente)='000054' Then--APT 
					'13121002'
				Else
					Case When d.IDMoneda='SOL' then
						'12121001' 
					Else
						'12122001' 
					End 
				End
			Else
				Case When Isnull(d.IDCliente,c.IDCliente)='000054' Then--APT 
					'13121002' 
				Else
					Case When Isnull(d.IDCliente,c.IDCliente) in('001589','001864') Then--Translivik
						Case When d.IDMoneda='SOL' then
							'13125001' 
						Else
							'13125002' 
						End 

					Else
						Case When d.IDMoneda='SOL' then
							'12121001' 
						Else
							'12122001' 
						End 
					End
				End
			End
		Else
			Case When d.IDMoneda='SOL' then
				'12121002' 
			Else
				'12122002' 
			End 
		End
		as ControlAccount, 

		--Case When d.IDMoneda='USD' then
		--	dbo.FnCambioMoneda(d.SsTotalDocumUSD,d.IDMoneda,'SOL',@SsTipoCambio)
		--Else
		--	d.SsTotalDocumUSD
		--End	as DocTotal,--Soles
		--Case When d.IDMoneda='USD' then
		--	d.SsTotalDocumUSD
		--else
		--	0
		--end as DocTotalFc, --Dolares
		D.IDMoneda,	D.SsTotalDocumUSD,
		0 As DiscountPercent,

		--Case When d.IDMoneda='USD' then
		--	0
		--Else
		--	d.SsTotalDocumUSD
		--End	as DocTotal,--Soles
		--Case When d.IDMoneda='USD' then
		--	d.SsTotalDocumUSD
		--else
		--	0
		--end as DocTotalFc, --Dolares
	
		--Left(d.NuDocum,3) as U_SYP_MDSD,
		case when d.IDTipoDoc='TIP' THEN '000' ELSE Left(d.NuDocum,3) END as U_SYP_MDSD,
		case when d.IDTipoDoc='TIP' THEN d.NuDocum ELSE Right(d.NuDocum,7) END as U_SYP_MDCD,
		ISNULL(td.CoSAP,'') as U_SYP_MDTD,

		Left(ISNULL(D.NuDocumVinc,''),3) as U_SYP_MDSO,
		Right(ISNULL(D.NuDocumVinc,''),7) as U_SYP_MDCO,
		ISNULL(tdv.CoSAP,'') as U_SYP_MDTO,	
		--getdate() as U_SYP_FECHAREF,
		D.FeEmisionVinc as U_SYP_FECHAREF,

		Case When d.CoEstado='AN' then 
			Case When ltrim(rtrim(@cTipoAnul)) = '' Then
				Case when LEFT(@NuDocum,3)='005' Then--anticipos
					'A' 
				else
					'I'
				end
			Else
				@cTipoAnul
			End
		else 
			'V' 
		end as U_SYP_STATUS,
		--@SsTipoCambio as U_SYP_TC,

		--dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeDocum) as U_SYP_TCTMP,
		Case when d.IDTipoDoc in('NCR','NCA') then
			dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeEmisionVinc) 
		else
			dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeDocum) 
		End as U_SYP_TCTMP,
		ISNULL(C.IDFile,'') AS U_SYP_NFILE,

		Case when LEFT(@NuDocum,3)='001' and d.IDTipoDoc='FAC' Then
			'Y' Else 'N' End as U_SYP_DOCEXPORT,
		--
		JournalMemo=([dbo].[FnDocumento_GetTexto_Concatenado](d.idtipodoc, d.nudocum) )
	    --,Comments=([dbo].[FnDocumento_GetTexto_Concatenado](d.idtipodoc, d.nudocum) )
		From DOCUMENTO d 
		Left Join COTICAB c On d.IDCab = c.IDCAB        
		Left Join MACLIENTES cl On Isnull(d.IDCliente,c.IDCliente) = cl.IDCliente         
		Left Join MATIPODOC td On d.IDTipoDoc=td.IDtipodoc	
		Left Join MATIPODOC tdv On d.IDTipoDocVinc=tdv.IDtipodoc	
		Left Join MAMONEDAS mon On d.IDMoneda=mon.IDMoneda
		Where d.IDTipoDoc=@IDTipoDoc And d.NuDocum=@NuDocum
		) as X
	) AS Y


