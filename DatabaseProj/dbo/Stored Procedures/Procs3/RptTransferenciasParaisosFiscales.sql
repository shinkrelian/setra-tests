﻿--HLF-20150316-inner join (Select IDTipoDoc,NuDocum,sum(SsMonto) as SsMonto From INGRESO_DOCUMENTO group by IDTipoDoc,NuDocum) dmd2 ON D.IDTipoDoc=DMD2.IDTipoDoc AND D.NuDocum=DMD2.NuDocum
--JHD-20160112- Comentar: -- And pa1.NuAnio=@Anio
--JHD-20160112- Comentar: -- And pa2.NuAnio=@Anio
--JHD-20160112- Comentar: -- And pa3.NuAnio=@Anio
--JHD-20160112- And (Not pa1.CoPais is null Or Not pa2.CoPais is null Or Not pa3.CoPais is null)
CREATE Procedure dbo.RptTransferenciasParaisosFiscales
	@Anio char(4)
As
	Set Nocount On
	Declare @RazonSocialSetours as varchar(200)=isnull((Select NoNombreEmpresa From PARAMETRO),'')

	Select DISTINCT cl.RazonComercial as DescCliente,iff.TxObservacion, up.Descripcion as DescPaisCli, iff.FeAcreditada,
	iff.NoBancoOrdendante, ubo.Descripcion as DescPaisOrdenante,
	iff.NoBancoCorresponsal1, ubc.Descripcion as DescPaisCorresp,
	iff.NoBancoCorresponsal2, ubc2.Descripcion as DescPaisCorresp2,
	ba.Sigla as DescBancoBenef, up.Descripcion as DescPaisBenef, iff.SsRecibido, 
	iff.SsGastosTransferencia,
	iff.SsBanco as  Comision,
	iff.SsNeto, 
	Case When iff.SsNeto>0 Then
		'INGRESO' 
	Else
		'PAGO PROVEEDOR' 
	End	as DescTipoOperacion,
	@RazonSocialSetours as DescBeneficiario,
	iff.CoOperacionBan,
	d.FeDocum as FecEmisionDoc,
	d.IDTipoDoc, Left(d.NuDocum,3) as NuSerieDoc, SUBSTRING(D.NuDocum,4,10) as NuDocum, c.IDFile, 
	c.Titulo as DescFile, --d.SsTotalDocumUSD
	--DMD.SsMonto
	DMD2.SsMonto
	FROM INGRESO_DEBIT_MEMO IDM 
	INNER JOIN DEBIT_MEMO DM ON IDM.IDDebitMemo=DM.IDDebitMemo
	INNER JOIN INGRESO_FINANZAS IFF ON IDM.NuIngreso=IFF.NuIngreso
	inner join INGRESO_DOCUMENTO DMD ON IDM.NuIngreso=DMD.NuIngreso
	inner join DOCUMENTO D ON DMD.IDTipoDoc=D.IDTipoDoc AND DMD.NuDocum=D.NuDocum
	inner join (Select IDTipoDoc,NuDocum,sum(SsMonto) as SsMonto From INGRESO_DOCUMENTO group by IDTipoDoc,NuDocum) dmd2 ON D.IDTipoDoc=DMD2.IDTipoDoc AND D.NuDocum=DMD2.NuDocum
	Inner Join COTICAB c On D.IDCab = c.IDCAB Left Join MACLIENTES CL On IFF.IDCliente=CL.IDCliente
	Left Join MAUBIGEO up On cl.IDCiudad=up.IDubigeo
	Left Join MAUBIGEO ubo On iff.IDUbigeoBancoOrde=ubo.IDubigeo
	Left Join MAUBIGEO ubc On iff.IDUbigeoBancoCor1=ubc.IDubigeo
	Left Join MAUBIGEO ubc2 On iff.IDUbigeoBancoCor2=ubc2.IDubigeo
	Left Join MABANCOS ba On iff.IDBanco=ba.IDBanco
	--Left Join MAUBIGEO ubb On iff.IDUbigeoBancoCor2=ubb.IDubigeo
	Left Join MAPARAISOSFISCALES_ANIO pa1 On cl.IDCiudad=pa1.CoPais-- And pa1.NuAnio=@Anio
	Left Join MAPARAISOSFISCALES_ANIO pa2 On iff.IDUbigeoBancoOrde=pa2.CoPais-- And pa2.NuAnio=@Anio
	Left Join MAPARAISOSFISCALES_ANIO pa3 On iff.IDUbigeoBancoCor1=pa3.CoPais-- And pa3.NuAnio=@Anio
	Where year(iff.FeAcreditada)=@Anio 
	--And (Not pa1.CoPais is null Or Not pa2.CoPais is null Or Not pa3.CoPais is null)
	And iff.SsNeto<>0
	--And (cl.IDCiudad In (Select CoPais From MAPARAISOSFISCALES_ANIO Where NuAnio=@Anio)
	--Or iff.IDUbigeoBancoOrde In (Select CoPais From MAPARAISOSFISCALES_ANIO Where NuAnio=@Anio)
	--Or iff.IDUbigeoBancoCor1 In (Select CoPais From MAPARAISOSFISCALES_ANIO Where NuAnio=@Anio)
	--)
	Order by iff.FeAcreditada,C.IDFile,d.FeDocum,D.IDTipoDoc ,Left(d.NuDocum,3),SUBSTRING(D.NuDocum,4,10) 


