﻿ --HLF-20130621-Reemplazando contenido de columna NomGuia                
--JRF-20130701-Reemplazando contenido de columna descBus.              
--JRF-20130731-Nueva funcion para obtener los diferentes buses.            
--JRF-20130805-Solo debe mostrar los servicios mas no operadores con alojamiento.          
--JRF-20130815-Solo los que no esten en estado CANCELADO        
--JRF-20130828-No debe Aparecer Los Servicios de PERURail(Transportista).      
--JRF-20130930-Deben aparecer los que tienen Detalles.    
--HLF-20141126-and rd.Anulado=0)     
 --and r.Anulado=0   
--JRF-20150223-And Not(rd.FlServicioIngManual=1 and rd.FlServicioNoShow=1)
CREATE Procedure dbo.RESERVAS_SelxIDCab_RptServicios                        
 @IDCab int                          
As                          
 Set NoCount On                          
 select * from(                           
 Select                       
  u.Descripcion  As DescCiudad                        
 -- u.Descripcion as DescCiuda                        
 , p.NombreCorto as DescProveedor,                           
 p.IDTipoProv,                          
 Case When p.IDTipoOper='I'                     
 Then                     
  Isnull(                    
  (SELECT top(1) p1.NombreCorto                     
  FROM OPERACIONES_DET_DETSERVICIOS od1 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                     
  WHERE IDOperacion_Det In                     
  (SELECT IDOperacion_Det FROM  OPERACIONES_DET WHERE IDOPERACION IN (SELECT IDOperacion FROM OPERACIONES WHERE IDReserva = r.IDReserva))                    
  And IDServicio_Det_V_Cot is not Null And IDProveedor_Prg Is not Null                     
  ORDER BY IDOperacion_Det) ,'')                    
                      
 Else                   
 (select top(1) ISNULL(p2.NombreCorto,'')  from RESERVAS_DET rd2                 
 LEFT jOIN MAPROVEEDORES p2 On p2.IDProveedor=rd2.IDGuiaProveedor                
 where r.IDReserva = rd2.IDReserva  Order by Item)                        
 End As NomGuia,                           
 dbo.fnDescBusesxIDReserva(r.IDReserva)                   
 As DescBus               
 ,                    
 r.Estado,ISNULL(r.CodReservaProv  ,'') As CodigoReservaProv                     
 ,(select top(1) Dia  from RESERVAS_DET rd2 where r.IDReserva = rd2.IDReserva Order by Dia Asc)  As Dia                     
 , case when p.IDTipoProv = '003' and           
    exists(select IDReserva from RESERVAS_DET rd2 LEFT join MASERVICIOS_DET sd on rd2.IDServicio_Det = sd.IDServicio_Det          
    where rd2.IDReserva =r.IDReserva and sd.ConAlojamiento=1) then 1 else 0 end as OperAlojamiento          
 From RESERVAS r           
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor                          
 Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo                          
 Left Join COTICAB C On r.IDCab = C.IDCAB                        
 Where r.IDCab=@IDCab And p.IDTipoProv <> '001'And  p.IDTipoProv <> '002' and r.Estado <> 'XL'      
 and p.IDProveedor <> '001836' and exists(select IDReserva from RESERVAS_DET rd 
										  Where rd.IDReserva = r.IDReserva and rd.Anulado=0
										  And IsNull(rd.FlServicioNoShow,0)=0)
 and r.Anulado=0   
 ) As X                  
 where X.OperAlojamiento = 0          
 Order by X.Dia 
