﻿--JRF-20140228-Aumento de Longitud para las columnas con numeric(12,4) 
--JRF-20150221-Considerar cambios de Columna al agregar un NoShow
CREATE Procedure [dbo].[RESERVAS_DETSERVICIOS_Upd]  
@IDReserva_Det int,  
@IDServicio_Det int,  
@CostoReal numeric(12,4),  
@CostoLiberado numeric(12,4),  
@Margen numeric(12,4),  
@MargenAplicado numeric(6,2),  
@MargenLiberado numeric(12,4),  
@Total numeric(8,2), 
@SSCR numeric(12,4),
@SSCL numeric(12,4),
@SSMargen numeric(12,4),
@SSMargenLiberado numeric(12,4),
@SSTotal numeric(12,4),
@STCR numeric(12,4),
@STMargen numeric(12,4),
@STTotal numeric(12,4),
@SSCRImpto numeric(12,4),
@SSCLImpto numeric(12,4),
@SSMargenImpto numeric(12,4),
@SSMargenLiberadoImpto numeric(12,4),
@STCRImpto numeric(12,4),
@STMargenImpto numeric(12,4), 
@CostoRealImpto numeric(12,4),  
@CostoLiberadoImpto numeric(12,4),  
@MargenImpto numeric(12,4),  
@MargenLiberadoImpto numeric(12,4),  
@TotImpto numeric(12,4),  
@UserMod char(4)  
As  
 Set NoCount On  
UPDATE [dbo].[RESERVAS_DETSERVICIOS]
   SET [CostoReal] = @CostoReal
      ,[CostoLiberado] = @CostoLiberado
      ,[Margen] =@Margen
      ,[MargenAplicado] = @MargenAplicado
      ,[MargenLiberado] = @MargenLiberado
      ,[Total] = @Total
      ,[SSCR] = @SSCR
      ,[SSCL] = @SSCL
      ,[SSMargen] = @SSMargen
      ,[SSMargenLiberado] = @SSMargenLiberado
      ,[SSTotal] = @SSTotal
      ,[STCR] = @STCR
      ,[STMargen] = @STMargen
      ,[STTotal] = @STTotal
      ,[CostoRealImpto] = @CostoRealImpto
      ,[CostoLiberadoImpto] = @CostoLiberadoImpto
      ,[MargenImpto] = @MargenImpto
      ,[MargenLiberadoImpto] = @MargenLiberadoImpto
      ,[SSCRImpto] = @SSCRImpto
      ,[SSCLImpto] = @SSCLImpto
      ,[SSMargenImpto] = @SSMargenImpto
      ,[SSMargenLiberadoImpto] = @SSMargenLiberadoImpto
      ,[STCRImpto] = @STCRImpto
      ,[STMargenImpto] = @STMargenImpto
      ,[TotImpto] = @TotImpto
      ,[UserMod] = @UserMod
      ,[FecMod] = Getdate()
  Where IDReserva_Det= @IDReserva_Det And IDServicio_Det= @IDServicio_Det
