﻿
------------------------------------------------------------------------------------------

CREATE PROCEDURE RH_HORARIO_Sel_Lst
	@CoHorario tinyint=0,
	@NoHorario varchar(150)='',
	@FlActivo bit=0
AS
BEGIN
	Select
		CoHorario,
 		NoHorario,
 		FeHorasRefrigerio,
 		NuMaxHorasSemana,
 		FlActivo
 	From dbo.RH_HORARIO
	Where 
		(@CoHorario=0 or CoHorario = @CoHorario)
		and
		 (FlActivo = @FlActivo or @FlActivo =0) AND
		(ltrim(rtrim(@NoHorario))='' Or NoHorario like '%' +@NoHorario + '%')   
END;

