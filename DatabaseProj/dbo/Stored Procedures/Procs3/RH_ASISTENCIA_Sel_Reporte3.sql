﻿

-----------------======================================================================================0


--[RH_ASISTENCIA_Sel_Reporte3] '01/01/2014','31/12/2014'
create PROCEDURE [RH_ASISTENCIA_Sel_Reporte3]

@FeFecha1 smalldatetime,
@FeFecha2 smalldatetime

--declare @FeFecha1 smalldatetime='01/01/2014'
--declare @FeFecha2 smalldatetime='31/12/2014'

AS
BEGIN

declare @reporte1 table(

NoArea varchar(150),
TxSede varchar(150), 
CoTrabajador char(11), 
Trabajador varchar(250),
FeFecha smalldatetime,
 NUSEMANA tinyint,
 Semana char(8),
 
 NuMinutosTarde tinyint,
 QtMinutosTarde tinyint,
 Max_Horas_Trabajadas tinyint,
						
 Horas_Refrigerio time(0),
 NuHorasTrabajadas time(0)
)

insert into @reporte1
SELECT     RH_AREA.NoArea, RH_SEDE.TxSede, 
CoTrabajador=ltrim(rtrim(RH_TRABAJADOR.CoTrabajador)), 
Trabajador=RH_TRABAJADOR.NoApPaterno+' '+ RH_TRABAJADOR.NoApMaterno+', '+ RH_TRABAJADOR.NoNombres,
 RH_ASISTENCIA.FeFecha,
 NUSEMANA,
 Semana='SEMANA '+cast(NUSEMANA as CHAR(1)),
 
 NuMinutosTarde=isnull(RH_ASISTENCIA.NuMinutosTarde,0),
 QtMinutosTarde=case when RH_ASISTENCIA.NuMinutosTarde>0 then 1 else 0 end,
 Max_Horas_Trabajadas=(
							SELECT  top 1   RH_HORARIO.NuMaxHorasSemana
							FROM         RH_HORARIO INNER JOIN
												  RH_TRABAJADOR_HORARIO ON RH_HORARIO.CoHorario = RH_TRABAJADOR_HORARIO.CoHorario
							WHERE     (RH_TRABAJADOR_HORARIO.CoTrabajador = RH_TRABAJADOR.CoTrabajador) AND ((RH_ASISTENCIA.FeFecha > RH_TRABAJADOR_HORARIO.FeFechaInicio) and RH_ASISTENCIA.FeFecha<(RH_TRABAJADOR_HORARIO.FeFechaFin+1) )
 
						),
						
 Horas_Refrigerio=(
							SELECT  top 1   RH_HORARIO.FeHorasRefrigerio
							FROM         RH_HORARIO INNER JOIN
												  RH_TRABAJADOR_HORARIO ON RH_HORARIO.CoHorario = RH_TRABAJADOR_HORARIO.CoHorario
							WHERE     (RH_TRABAJADOR_HORARIO.CoTrabajador = RH_TRABAJADOR.CoTrabajador) AND ((RH_ASISTENCIA.FeFecha > RH_TRABAJADOR_HORARIO.FeFechaInicio) and RH_ASISTENCIA.FeFecha<(RH_TRABAJADOR_HORARIO.FeFechaFin+1) )
 
						),
 RH_ASISTENCIA.NuHorasTrabajadas
 
 --into #reporte1
FROM         RH_AREA_SEDE INNER JOIN
                      RH_AREA ON RH_AREA_SEDE.CoArea = RH_AREA.CoArea INNER JOIN
                      RH_SEDE ON RH_AREA_SEDE.CoSede = RH_SEDE.CoSede INNER JOIN
                      RH_TRABAJADOR_CARGO INNER JOIN
                      RH_TRABAJADOR ON RH_TRABAJADOR_CARGO.CoTrabajador = RH_TRABAJADOR.CoTrabajador ON 
                      RH_AREA_SEDE.CoArea = RH_TRABAJADOR_CARGO.CoArea AND RH_AREA_SEDE.CoSede = RH_TRABAJADOR_CARGO.CoSede INNER JOIN
                      RH_ASISTENCIA ON RH_TRABAJADOR_CARGO.NuTrabCargo = RH_ASISTENCIA.CoTrabCargo
where ((FeFecha > @FeFecha1) and FeFecha<(@FeFecha2+1) )


--Select * from @Reporte1 order by NuSemana,noarea,Trabajador;

--select * from #reporte1 where cotrabajador='72181761'



declare @reporte2 table(

NoArea varchar(150),
TxSede varchar(150), 
CoTrabajador char(11), 
Trabajador varchar(250),
 NUSEMANA tinyint,
 Semana char(8),
 
 NuMinutosTarde tinyint,
 QtMinutosTarde tinyint,
 Max_Horas_Trabajadas tinyint,
						
 NuHorasTrabajadas int,
 NuHorasTrabajadas2 nvarchar(8)
)

insert into @reporte2
SELECT     NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 --FeFecha,
 NUSEMANA,
 Semana,
 
 NuMinutosTarde=SUM(NuMinutosTarde),
 QtMinutosTarde=SUM(QtMinutosTarde),
 Max_Horas_Trabajadas,
						
 --Horas_Refrigerio,
 --NuHorasTrabajadas=SUM(NuHorasTrabajadas)
 NuHorasTrabajadas=  sum( DATEPART(SECOND, NuHorasTrabajadas) + 60 * DATEPART(MINUTE, NuHorasTrabajadas) + 3600 * DATEPART(HOUR, NuHorasTrabajadas ) ) ,
 NuHorasTrabajadas2= dbo.FnConvertir_Segundos_a_Tiempo(sum( DATEPART(SECOND, NuHorasTrabajadas) + 60 * DATEPART(MINUTE, NuHorasTrabajadas) + 3600 * DATEPART(HOUR, NuHorasTrabajadas ) ) )  --(SELECT CONVERT(VARCHAR, DATEADD(second,sum( DATEPART(SECOND, NuHorasTrabajadas) + 60 * DATEPART(MINUTE, NuHorasTrabajadas) + 3600 * DATEPART(HOUR, NuHorasTrabajadas ) ) ,0),108))
--into #reporte2
from @Reporte1 --#reporte1
group by  NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 --FeFecha,
 NUSEMANA,
 semana,
 Max_Horas_Trabajadas

--select * from @reporte2
 
-- Select * from Reporte2 order by NuSemana,noarea,Trabajador 
 
declare @reporte3 table(

NoArea varchar(150),
TxSede varchar(150), 
CoTrabajador char(11), 
Trabajador varchar(250),
 NUSEMANA tinyint,
 Semana char(8),
 
 NuMinutosTarde int,
 QtMinutosTarde tinyint,
 Max_Horas_Trabajadas tinyint,
						
 NuHorasTrabajadas int,
 NuHorasTrabajadas2 nvarchar(8),
 NuHorasExtra int,
 NuHorasExtra2 nvarchar(8)
)
 
 insert into @reporte3
 SELECT     NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 NUSEMANA,
 Semana,
 
 NuMinutosTarde=isnull(NuMinutosTarde*60,0),
 QtMinutosTarde=isnull(QtMinutosTarde,0),
 Max_Horas_Trabajadas=ISNULL(Max_Horas_Trabajadas,0),
						
 NuHorasTrabajadas=ISNULL(NuHorasTrabajadas,0),
 NuHorasTrabajadas2,
 NuHorasExtra=case when (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))<0 then 0 else  (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600)) end,
 NuHorasExtra2=case when (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))<0 then '00:00:00' else  dbo.FnConvertir_Segundos_a_Tiempo((NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))) end

from @reporte2 


SELECT  distinct   NoArea,
TxSede, 
CoTrabajador, 
Trabajador,
 --NUSEMANA,
 --Semana,
 
 NuMinutosTarde_S1=isnull((select sum(NuMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),0),
 NuMinutosTarde_S2=isnull((select sum(NuMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),0),
 NuMinutosTarde_S3=isnull((select sum(NuMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),0),
 NuMinutosTarde_S4=isnull((select sum(NuMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4),0),
 
 --QtMinutosTarde,
 
 QtMinutosTarde_S1=isnull((select sum(QtMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),0),
 QtMinutosTarde_S2=isnull((select sum(QtMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),0),
 QtMinutosTarde_S3=isnull((select sum(QtMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),0),
 QtMinutosTarde_S4=isnull((select sum(QtMinutosTarde) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4),0),
 
 

 --Max_Horas_Trabajadas,
						
 --NuHorasTrabajadas,
 NuHorasTrabajadas_S1=isnull((select sum(NuHorasTrabajadas) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),0),
 NuHorasTrabajadas_S2=isnull((select sum(NuHorasTrabajadas) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),0),
 NuHorasTrabajadas_S3=isnull((select sum(NuHorasTrabajadas) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),0),
 NuHorasTrabajadas_S4=isnull((select sum(NuHorasTrabajadas) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4),0),
 
 --NuHorasTrabajadas2,
-- NuHorasExtra=case when (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600))<0 then 0 else  (NuHorasTrabajadas-(Max_Horas_Trabajadas*3600)) end,
 
 NuHorasExtra_S1=isnull((select sum(NuHorasExtra) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=1),0),
 NuHorasExtra_S2=isnull((select sum(NuHorasExtra) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=2),0),
 NuHorasExtra_S3=isnull((select sum(NuHorasExtra) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=3),0),
 NuHorasExtra_S4=isnull((select sum(NuHorasExtra) from @reporte3 r where r.NoArea=r3.noarea and r.cotrabajador=r3.cotrabajador and nusemana=4),0)
 --NuHorasExtra2
 from @reporte3 r3

--drop table #reporte1
--drop table #reporte2
--drop table #reporte3
--*/

END;
