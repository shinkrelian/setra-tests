﻿Create Procedure dbo.TICKET_AMADEUS_TARIFA_Upd
 @CoTicket char(13),
 @NuTarifa tinyint,
 @CoCiudadPar char(3),
 @CoCiudadLle char(3),
 @SsTarifa numeric(6,2),
 @SsQ numeric(6,2),
 @UserMod char(4)
As
	Set NoCount On
	UPDATE [dbo].[TICKET_AMADEUS_TARIFA]
	   SET [CoCiudadPar] = @CoCiudadPar,
		   [CoCiudadLle] = @CoCiudadLle,
		   [SsTarifa] = @SsTarifa,
		   [SsQ] = @SsQ,
		   [UserMod] = @UserMod,
		   [FecMod] = Getdate()
	 WHERE CoTicket = @CoTicket and NuTarifa = @NuTarifa
