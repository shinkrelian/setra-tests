﻿
--JRF-20121207-Ordenando por la menor FechaIN, FechaOUT.                                  
--HLF-20120816-Subquery OperadorConAlojamiento                                    
--HLF-20120905-Nuevos campos Email3, Email4                                     
--HLF-20130130-Cambiardo logica del As CotiDetLog                                
--HLF-20130220-Agregando campo CambiosVtasAceptados                              
--JRF-20130228-Cambiando posicion Imagen del estado y ordena x fecha.                            
--HLF-20130320-Agregando (...) as PlanAlimenticio y And (X.IDTipoProv<>'001' OR (X.IDTipoProv='001' And PlanAlimenticio=1))                          
--JRF-20130619-Agregando los campos de lectura (ObservacionPolitica,DocPdfRutaPdf)                        
--HLF-20130625-Reemplazando Atendido=0 And Accion='D') Then por Atendido=0 And Accion='B') Then  En columna CotiDetLog                      
--JRF-20130703-Agregando el campo EstadoSolicitud.(Para tema de mensajería)                    
--JRF-20130808-Agregando col CodTarifa                  
--HLF-20130901-Agregando Anulado = 0                
--HLF-20130916-Agregando pinternac.NombreCorto as DescProvInternac,              
--HLF-20131010-Agregando columna RegeneradoxAcomodo                 
--HLF-20131018-Agregando Case When Rv.CambiosenVentasPostFile          
--JRF-20140901-Agregar Funcion que compare lo actual en COTIDET >> RESERVAS_DET    
--JRF-20150115-Agregar el campo que identifica la Oficina de Fact. [CoUbigeo_Oficina] 
--JRF-20150413-Identificar los NoHoteles con Alojamiento
--JRF-20151007-..IsNull(pinternac.Email3,I...
--JRF-20151123-...And IsNull(rd.NuViaticoTC,0)=0
--JRF-20151125-.. and FlServicioTC=0 [SubQuery de fecha In]
--JRF-20151216-.. and (FlServicioTC=0 Or Not sd.CoTipoCostoTC In ('ALM','CEN'))
--JRF-20160317-...y enviarla al siguiente correo:'+char(13)
CREATE Procedure [dbo].[RESERVAS_Sel_ListxIDCabNoHoteles]
 @IDCab int                                        
AS
BEGIN
 Set NoCount On 
 Declare @TextAdmin varchar(Max)=''
 Set @TextAdmin = char(13)+'Al *status* esta reserva, le solicitamos adjuntar la copia de la FACTURA / INVOICE  que corresponde a los servicios contratados y enviarla al siguiente correo:'+char(13)
 + 'pagos1@setours.com' + char(13)
 + 'El documento (FACTURA / INVOICE) debe consignar la siguiente información:'+char(13)
 + 'Nombre comercial: SETOURS S.A.'+char(13)
 + 'RUC: 20100939887'+char(13)
 + 'Dirección: Av. Comandante Espinar 229 - Miraflores, Lima - Perú'+char(13)
 + 'Igualmente debe consignar: Servicio contratado / número de file / importa a pagar en la moneda pactada con SETOURS.'
                               
 SELECT * FROM                                     
 (                                     
	 Select Rv.IDProveedor,Pr.NombreCorto as DescProveedor, isnull(pinternac.NombreCorto,'') as DescProvInternac, Pr.IDTipoProv,                                        
	 --Case When ISNull(pr.Email3,'') = '' then ISNULL(pr.Email4,'') Else Email3 End as Email,                                        
	 IsNull(pinternac.Email3,IsNull(pr.Email3,'')) as EmailReservas1, IsNull(pinternac.Email4,IsNull(pr.Email4,'')) as EmailReservas2,                                    
	 u.IDPais, pr.IDCiudad, u.Descripcion as DescCiudad, Rv.Estado  as IDEstado, Rv.IDReserva, 0 as IDOperacion, Rv.Observaciones, Rv.CodReservaProv,
	 Pr.IDTipoOper, Pr.IDFormaPago,
	 --(Select COUNT(*) From RESERVAS_DET rd1                                     
	 --Inner Join RESERVAS r1 On rd1.IDReserva=r1.IDReserva And r1.IDReserva=Rv.IDReserva                                    
	 --Inner Join MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor And p1.IDTipoProv='003'                                    
	 --Inner Join MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det And sd1.ConAlojamiento=1 And Not Exists(select ID from COTIVUELOS where IDCab=@IDCab And IDDet=rd1.IDDet)                                 
	 --) as OperadorConAlojam,
 	ISNULL(RCA.NReg,0) AS OperadorConAlojam,
	 --(Select Top(1) Convert(char(10),rd.Dia,103) from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
		--where rd.IDReserva = rv.IDReserva And IsNull(rd.NuViaticoTC,0)=0 --and FlServicioTC=0
		--and (FlServicioTC=0 Or Not sd.CoTipoCostoTC In ('ALM','CEN'))
	 -- order by rd.Dia) As FechaIn,
	 Convert(char(10),FecDet.FechaIn,103) As FechaIn,
	  Case When (pr.IDTipoOper='I' Or dbo.FnExistenAlojamiento(Rv.IDReserva)=1) Or 
			--Not Exists(select IDServicio_Det from RESERVAS_DETSERVICIOS rds 
			--			where rds.IDReserva_Det In(select rd2.IDReserva_Det from RESERVAS_DET rd2 where rd2.IDReserva=rv.IDReserva)) Then
			ISNULL(RSR.NRegS,0) = 0 Then
		Case When dbo.FnCambiosenVentasPostFileReservasSoloServicios(Rv.IDReserva) = 1 Then 'M' Else 'O' End         
	   else
		Case When dbo.FnCambiosEnVentasPostFileSoloSubServicios(Rv.IDReserva) = 1 Then 'M' else	'O'	End
	   End As CotiDetLog,                              
	 Rv.CambiosVtasAceptados,
	 --Case When Exists(Select rd1.IDReserva From	RESERVAS_DET rd1 Inner Join MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det                          
		--				And sd1.PlanAlimenticio=1 Where rd1.IDReserva=Rv.IDReserva) Then 1 Else 0 End as PlanAlimenticio                          
	 Case When ISNULL(RDD.NRegD,0)>0 Then 1 Else 0 End as PlanAlimenticio                          
						,IsNull(Pr.ObservacionPolitica,'') as ObservacionPolitica,                        
	 IsNull(Pr.DocPdfRutaPdf,'') as DocPdfRutaPdf, Rv.EstadoSolicitud,
	 case when pr.IDTipoProv = '002' then
	  --Case when (select COUNT(distinct IDServicio_Det) from RESERVAS_DET rd where rd.Anulado=0 and rd.IDReserva = Rv.IDReserva) > 1                   
	  Case when ISNULL(RCT.NRegCT,0) > 1                   
		then 'n' else dbo.FnCodigoTarifaxIDReserva(Rv.IDReserva) End else '' End as CodigoTarifa,
	 rv.RegeneradoxAcomodo, IsNull(Pr.CoUbigeo_Oficina,'') as CoUbigeo_Oficina, dbo.FnExistenAlojamiento(RV.IDReserva) as ExistAlojamiento,
	 Case When Pr.CoUbigeo_Oficina='000065' And Pr.IDFormaPago = '002' Then @TextAdmin else '' End As ObservacionAdmHotel
 From RESERVAS Rv Left Join MAPROVEEDORES Pr On Rv.IDProveedor= Pr.IDProveedor                                        
 Left Join MAUBIGEO u On pr.IDCiudad=u.IDubigeo                                        
 Left Join MAPROVEEDORES pinternac On pr.IDProveedorInternacional=pinternac.IDProveedor              
	LEFT OUTER JOIN (SELECT r1.IDReserva, COUNT(*) AS NReg FROM RESERVAS_DET rd1
					 INNER JOIN RESERVAS r1 On rd1.IDReserva=r1.IDReserva
					 INNER JOIN MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor
					 INNER JOIN MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det
					 LEFT OUTER JOIN (SELECT IDDet FROM COTIVUELOS WHERE IDCab=@IDCab) CV on CV.IDDet = rd1.IDDet
					 WHERE p1.IDTipoProv='003' AND sd1.ConAlojamiento=1 AND CV.IDDet is null AND r1.IDCab = @IDCab
					 GROUP BY r1.IDReserva) RCA ON Rv.IDReserva = RCA.IDReserva
	LEFT OUTER JOIN (SELECT Rdf.IDReserva, MIN(Dia) AS FechaIn FROM RESERVAS_DET Rdf INNER JOIN RESERVAS Rf ON Rdf.IDReserva = Rf.IDReserva
					 LEFT JOIN MASERVICIOS_DET sd On Rdf.IDServicio_Det = sd.IDServicio_Det
					 WHERE Rf.IDCab = @IDCab And IsNull(Rdf.NuViaticoTC,0) = 0 AND (FlServicioTC = 0 OR NOT sd.CoTipoCostoTC IN ('ALM','CEN'))
					 GROUP BY Rdf.IDReserva) FecDet ON Rv.IDReserva = FecDet.IDReserva
	LEFT OUTER JOIN (SELECT Rd.IDReserva, COUNT(Rds.IDServicio_Det) AS NRegS FROM RESERVAS_DET Rd INNER JOIN
					 RESERVAS_DETSERVICIOS Rds ON Rd.IDReserva_Det = Rds.IDReserva_Det
					 INNER JOIN RESERVAS Rf ON Rd.IDReserva = Rf.IDReserva WHERE Rf.IDCab = @IDCab
					 GROUP BY Rd.IDReserva) RSR ON Rv.IDReserva = RSR.IDReserva
	LEFT OUTER JOIN (SELECT Rdn.IDReserva, COUNT(distinct IDServicio_Det) AS NRegCT FROM RESERVAS_DET Rdn
					 INNER JOIN RESERVAS Rf ON Rdn.IDReserva = Rf.IDReserva
					 WHERE Rf.IDCab = @IDCab AND Rdn.Anulado = 0 GROUP BY Rdn.IDReserva) RCT ON Rv.IDReserva = RCT.IDReserva
	LEFT OUTER JOIN (SELECT Rdd.IDReserva, COUNT(IDReserva_Det) AS NRegD From RESERVAS_DET Rdd INNER JOIN MASERVICIOS_DET Sdd ON Rdd.IDServicio_Det = Sdd.IDServicio_Det                          
					 INNER JOIN RESERVAS Rf ON Rdd.IDReserva = Rf.IDReserva
					 WHERE Rf.IDCab = @IDCab AND Sdd.PlanAlimenticio = 1 GROUP BY Rdd.IDReserva) RDD ON Rv.IDReserva = RDD.IDReserva
 Where rv.IDCab=@IDCab --And Pr.IDTipoProv <> '001'                   
 And rv.Anulado = 0                                        
 ) AS X                           
 WHERE X.OperadorConAlojam=0 And (X.IDTipoProv<>'001' OR (X.IDTipoProv='001' And PlanAlimenticio=1))                          
 Order by Cast(X.FechaIn as smalldatetime)
END
