﻿--HLF-20141106-if @SsSaldoPend = 0  
--JHD-20150821-No actualizar estado a Aceptado por BD
--JHD-20150828-@CoMoneda_FEgreso char(3)=''
--JHD-20150828-if rtrim(ltrim(@CoMoneda_FEgreso))<>'' ...
CREATE Procedure dbo.VOUCHER_OPERACIONES_UpdSsPendientexDoc  
@NuVoucher int,  
@SsTotalOrig numeric(9,2),  
@UserMod char(4),
@CoMoneda_FEgreso char(3)=''
As  
Set NoCount On  
BEGIN

Update VOUCHER_OPERACIONES  
 Set SsSaldo= VOUCHER_OPERACIONES.SsSaldo-@SsTotalOrig,  
  UserMod=@UserMod,  
  FecMod=GETDATE()  
where IDVoucher=@NuVoucher  
  
if rtrim(ltrim(@CoMoneda_FEgreso))<>''
begin
	Update VOUCHER_OPERACIONES_DET 
	Set SsSaldo= VOUCHER_OPERACIONES_DET.SsSaldo-@SsTotalOrig,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  
where IDVoucher=@NuVoucher  and CoMoneda=@CoMoneda_FEgreso

Declare @SsSaldoPend_Det numeric(14,2)=(select Isnull(SsSaldo,0) from VOUCHER_OPERACIONES_Det where IDVoucher=@NuVoucher and CoMoneda=@CoMoneda_FEgreso)  
	if @SsSaldoPend_Det <> 0  
		 Update VOUCHER_OPERACIONES_DET set CoEstado='PD' where IDVoucher=@NuVoucher and CoMoneda=@CoMoneda_FEgreso
end

Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from VOUCHER_OPERACIONES where IDVoucher=@NuVoucher)  
----if @SsSaldoPend <= 0  
--if @SsSaldoPend = 0  
-- Update VOUCHER_OPERACIONES set CoEstado='AC' where IDVoucher=@NuVoucher  
END;
