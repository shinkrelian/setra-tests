﻿Create Procedure dbo.RESERVAS_SelExistLiberados
@IDReserva int,
@pExisteLiberado bit output
As
Set NoCount On
Set @pExisteLiberado = 0
If Exists(select IDReserva_Det from RESERVAS_DET 
          Where Anulado=0 and FlServicioNoShow=0 
		  and IDReserva=@IDReserva And CantidadAPagar<Cantidad and FechaOut is not null)
Begin
Set @pExisteLiberado =1
End
