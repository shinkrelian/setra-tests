﻿
Create Procedure dbo.RESERVAS_SelRegeneradoxAcomodoOutput
	@IDCab int,
	@IDProveedor char(6),
	@pRegeneradoxAcomodo bit Output
As
	Set NoCount On	
	
	Select @pRegeneradoxAcomodo=RegeneradoxAcomodo 
	From RESERVAS 
	Where IDProveedor=@IDProveedor
	And IDCab=@IDCab 
	And Anulado=0
	
	Set @pRegeneradoxAcomodo=ISNULL(@pRegeneradoxAcomodo,0)
