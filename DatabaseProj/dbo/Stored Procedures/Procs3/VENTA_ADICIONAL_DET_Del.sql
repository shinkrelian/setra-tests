﻿Create Procedure dbo.VENTA_ADICIONAL_DET_Del
	@NuVtaAdi int,
	@CoServicio int,
	@NuPax int
As
	Set Nocount On

	Delete From VENTA_ADICIONAL_DET
	Where NuVtaAdi=@NuVtaAdi And	
	CoServicio=@CoServicio And
	NuPax=@NuPax
