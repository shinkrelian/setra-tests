﻿--HLF-20130828-Agregar Not In(Select CAST(IDReserva_Det ...              
--HLF-JRF-20140303-Creación de Indices y Cambio del Subquery x el Exists.            
--HLF-20141013-Case When od.IDMoneda='USD' Then ...          
--Left Join COTICAB_TIPOSCAMBIO tc On tc.IDCab=@IDCab And tc.CoMoneda=od.IDMoneda          
--JRF-20150107-Los campos que se consideran como total son la suma (CostoReal+TotImpto)      
--JRF-20150126-Considerar el IGV desde el sd.Afecto      
--JRF-20150228-Considerar Costos por Pax.    
--JRF-20150409-Se deben considerar el TourConductor
--JRF-20151125-No considerar Viaticos[Otro store]
--JRF-20151216-No considerar Viaticos (SubQuery)
CREATE Procedure dbo.RESERVAS_DETSERVICIOS_InsxIDReserva                    
 @IDCab int,              
 @IDReserva int,       
 --@IDServicio_Det int,         
 @UserMod char(4)                
As                
 Set NoCount On                
       
 Declare @NuIgv numeric(8,2)= (select NuIGV from PARAMETRO)      
 Insert Into RESERVAS_DETSERVICIOS                
 (IDReserva_Det,IDServicio_Det,                
 CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,                
 SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,                
 MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,                
 STMargenImpto,TotImpto,UserMod)                
           
 Select IDReserva_Det,IDServicio_Det,                
 X.CostoReal*TCambio,X.CostoLiberado*TCambio,X.Margen*TCambio,X.MargenAplicado,          
 X.MargenLiberado*TCambio,X.Total*TCambio,        
 X.SSCR*TCambio,X.SSCL*TCambio,X.SSMargen*TCambio,X.SSMargenLiberado*TCambio,          
 X.SSTotal*TCambio,X.STCR*TCambio,X.STMargen*TCambio,X.STTotal*TCambio,          
 X.CostoRealImpto*TCambio,X.CostoLiberadoImpto*TCambio,X.MargenImpto*TCambio,          
 X.MargenLiberadoImpto*TCambio,X.SSCRImpto*TCambio,X.SSCLImpto*TCambio,          
 X.SSMargenImpto*TCambio,X.SSMargenLiberadoImpto*TCambio,X.STCRImpto*TCambio,          
 X.STMargenImpto*TCambio,X.TotImpto*TCambio,@UserMod          
 From          
  (          
  Select Distinct od.IDReserva_Det,rs.IDServicio_Det,                
  Case When od.FlServicioTC=1 Then rs.CostoReal else 
  dbo.FnDevuelveCostoRealxPaxDetServicio(rs.IDServicio_Det,(rd.NroPax+IsNull(rd.NroLiberados,0))) End
  as CostoReal,    
  rs.CostoLiberado,rs.Margen,rs.MargenAplicado,rs.MargenLiberado,        
  (Case When od.FlServicioTC=1 Then rs.CostoReal else 
  dbo.FnDevuelveCostoRealxPaxDetServicio(rs.IDServicio_Det,(rd.NroPax+IsNull(rd.NroLiberados,0))) End+    
  Case When sd.Afecto=1 Then
   --dbo.FnDevuelveCostoRealxPaxDetServicio(rs.IDServicio_Det,(rd.NroPax+IsNull(rd.NroLiberados,0)))
   Case When od.FlServicioTC=1 Then rs.CostoReal else 
   dbo.FnDevuelveCostoRealxPaxDetServicio(rs.IDServicio_Det,(rd.NroPax+IsNull(rd.NroLiberados,0))) End
   *(@NuIgv/100)     
  Else 0 End) as Total,--,rs.Total,                
  rs.SSCR,rs.SSCL,rs.SSMargen,rs.SSMargenLiberado,rs.SSTotal,rs.STCR,                
  rs.STMargen,rs.STTotal,rs.CostoRealImpto,rs.CostoLiberadoImpto,rs.MargenImpto,rs.MargenLiberadoImpto,                
  rs.SSCRImpto,rs.SSCLImpto,rs.SSMargenImpto,rs.SSMargenLiberadoImpto,rs.STCRImpto,rs.STMargenImpto,                
  Case When sd.Afecto=1 Then rs.CostoReal*(@NuIgv/100) Else 0 End As TotImpto,  --rs.TotImpto,          
  IsNull(
  Case When od.FlServicioTC=1 then
	Case When isnull(sd.CoMoneda,'USD')<>'USD' Then (select SsTipCam from COTICAB_TIPOSCAMBIO TC2 where tc2.IDCab=@IDCab and tc2.CoMoneda=isnull(sd.CoMoneda,'USD')) else 1 End
  else
	Case When isnull(od.IDMoneda,'USD')='USD' Then 1 Else Case When sd.TC is null then tc.SsTipCam else sd.TC End
  End  End 
  ,1)
  as TCambio          
  From COTIDET_DETSERVICIOS rs Inner Join COTIDET rd On rs.IDDET=rd.IDDET                
  Inner Join COTICAB r On r.IDCab=@IDCab                
  Inner Join RESERVAS res On r.IDCAB=rd.IDCAB And res.IDReserva=@IDReserva          
  Inner Join RESERVAS_DET od On rd.IDDET=od.IDDet And od.IDReserva=res.IDReserva                
  Inner Join MASERVICIOS_DET sd On rs.IDServicio_Det=sd.IDServicio_Det                
  Left Join COTICAB_TIPOSCAMBIO tc On tc.IDCab=@IDCab And tc.CoMoneda=od.IDMoneda          
  Where IsNull(od.NuViaticoTC,0)=0  And             
  Not Exists(Select CAST(IDReserva_Det as varchar(20))+' '+CAST(rds.IDServicio_Det as varchar(20)) From RESERVAS_DETSERVICIOS rds 
			 Left Join MASERVICIOS_DET sd On rds.IDServicio_Det = sd.IDServicio_Det
			 Where (rds.IDReserva_Det = od.IDReserva_Det and rds.IDServicio_Det = rs.IDServicio_Det) And sd.CoTipoCostoTC is null)
  --CAST(od.IDReserva_Det as varchar(20))+' '+CAST(rs.IDServicio_Det as varchar(20))              
  --Not In (Select CAST(IDReserva_Det as varchar(20))+' '+CAST(IDServicio_Det as varchar(20)) From RESERVAS_DETSERVICIOS)              
  ) as X          
  Where X.CostoReal>0 --and (@IDServicio_Det=0 Or x.IDServicio_Det=@IDServicio_Det)
