﻿--JRF-20140527-Considerar el titulo academico como prefijo al apellidos
--JRF-20140528-Considerar descTC para cuando el campo FlTC este activo.
CREATE Procedure dbo.RESERVAS_Sel_PaxxAcomodo  
@IDReserva int  
As  
 Set NoCount On  
 --select distinct p.Orden as NroOrd,p.Apellidos,p.Nombres,p.Titulo,Substring(id.Descripcion,1,3) as DescIdentidad,  
 select distinct p.Orden as NroOrd,p.Apellidos,p.Nombres,
 Case when ltrim(rtrim(p.TxTituloAcademico)) <> '---' then p.TxTituloAcademico else p.Titulo End as Titulo,Substring(id.Descripcion,1,3) as DescIdentidad,  
 Isnull(p.NumIdentidad,'') as NumIdentidad,Isnull(ub.DC,'') as DescNacionalidad,  
 Isnull(Convert(char(10),p.FecNacimiento,103),'') as FecNacimiento,ISNULL(p.Peso,0.00) as Peso,  
 p.Residente,Isnull(p.ObservEspecial,'') as ObservEspecial,p.IDIdentidad,IDNacionalidad,p.IDPax,ap.CapacidadHab  
 ,cast(ap.EsMatrimonial as CHAR(1)) as EsMatrimonial  
 ,Case When ap.CapacidadHab Is Not Null Then    
  dbo.FnMailAcomodosReservas(ap.CapacidadHab,ap.EsMatrimonial) + ' '+     
   CAST(ap.IDHabit As varchar(3))    
  Else '' End As ACC,--,rd.IDReserva_Det  
  Case When FlTC =1 then 'TC' Else '' End as DescTC
 from COTIPAX p --On rdp.IDPax = p.IDPax  
 Left Join ACOMODOPAX_PROVEEDOR ap On p.IDPax = ap.IDPax and ap.IDReserva = @IDReserva    
 Left Join MATIPOIDENT id On p.IDIdentidad = id.IDIdentidad  
 Left Join MAUBIGEO ub On p.IDNacionalidad = ub.IDubigeo  
 --Left Join RESERVAS_DET rd --On rdp.IDReserva_Det = rd.IDReserva_Det  
 where ap.IDReserva = @IDReserva  
 --where rdp.IDReserva_Det = 32580  
 Order by p.Orden  
