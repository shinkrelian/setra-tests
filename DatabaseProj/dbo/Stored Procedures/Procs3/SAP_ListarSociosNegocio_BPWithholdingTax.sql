﻿--JHD-20150916-order by p.FecMod
--JRF-20151007-Estos casos ya se encuentran ingresados por defecto en SAP.
--JRF-20151027-No considerar cuyo RUC inician con '10...', estos ya estan considerados por SAP
--JHD-20151029-Se deshizo el cambio anterior
CREATE Procedure [dbo].[SAP_ListarSociosNegocio_BPWithholdingTax]
@IDSocioNegocio varchar(15)
As
begin
	Set NoCount On

	select WTCode= d.CoSAP from
	MAPROVEEDORES_MATIPODETRACCION p
	inner join MATIPODETRACCION d on p.CoTipoDetraccion=d.CoTipoDetraccion
	inner Join MAPROVEEDORES pr On p.IDProveedor=pr.IDProveedor
	where p.IDProveedor=@IDSocioNegocio 
		  --And Not(d.CoTipoDetraccion='00001' And (pr.Persona='N' And pr.IDIdentidad='001'))
		  --And Not(SUBSTRING(IsNull(pr.RUC,''),1,2)='10')
	order by p.FecMod  --d.CoSAP
end;
