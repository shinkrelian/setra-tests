﻿
Create Procedure dbo.RESERVAS_UpdRegeneradoxAcomodo
	@IDReserva int,
	@RegeneradoxAcomodo bit,
	@UserMod char(4)
As
	Set Nocount On
	
	Update RESERVAS Set RegeneradoxAcomodo=@RegeneradoxAcomodo,
	UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva=@IDReserva
	
