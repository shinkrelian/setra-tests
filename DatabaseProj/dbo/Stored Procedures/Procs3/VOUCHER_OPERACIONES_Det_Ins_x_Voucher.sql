﻿--JHD-20150828-arreglar is null
--JHD-20150828-agregar campo TxObsDocumento
--JRF-20151019-And v.IDVoucher <> 0
--JHD-20160405-FeCreacion=getdate()
CREATE Procedure [dbo].[VOUCHER_OPERACIONES_Det_Ins_x_Voucher]  
 @IDVoucher int,  
 @UserMod char(4) 
 AS
 BEGIN
	delete from VOUCHER_OPERACIONES_DET where IDVoucher=@IDVoucher
	INSERT INTO VOUCHER_OPERACIONES_DET
	Select v.IDVoucher,od.IDMoneda,v.CoEstado,
	Sum(TotalGen),
	SsSaldo=Sum(isnull(TotalGen,0))-isnull((select sum(isnull(sstotal,0)) 
			from DOCUMENTO_PROVEEDOR where NuVoucher=@IDVoucher and FlActivo=1 and CoMoneda=od.IDMoneda),0),
	SSDifAceptada=case when v.CoEstado='AC' 
						THEN (Sum(TotalGen)-isnull((select sum(sstotal) 
						from DOCUMENTO_PROVEEDOR where NuVoucher=@IDVoucher and FlActivo=1 and CoMoneda=od.IDMoneda),0)) ELSE 0 END,
	UserMod=@UserMod,
	FecMod=getdate(),
	TxObsDocumento='',
	FeCreacion=getdate()
	From Operaciones_Det od
	left join OPERACIONES o on o.IDOperacion=od.IDOperacion_Det
	left join VOUCHER_OPERACIONES v on v.IDVoucher=od.IDVoucher
	Where v.IDVoucher=@IDVoucher And v.IDVoucher <> 0
	group by v.IDVoucher,od.IDMoneda,v.CoEstado
END;
