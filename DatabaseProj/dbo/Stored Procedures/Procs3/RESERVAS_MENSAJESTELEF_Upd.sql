﻿CREATE Procedure [dbo].[RESERVAS_MENSAJESTELEF_Upd]
	@IDReserva int,
	@IDMensaje tinyint,
	@Contacto varchar(100),           
	@Fecha	smalldatetime,
	@IDUsuario char(4),
	@Asunto varchar(200),
	@Mensaje varchar(Max),
	@UserMod char(4)
As
	Set NoCount On
	
	UPDATE RESERVAS_MENSAJESTELEF
    Set    Contacto=@Contacto
		   ,Fecha=@Fecha
           ,IDUsuario=@IDUsuario
           ,Asunto=@Asunto
           ,Mensaje=@Mensaje
           ,FecMod=Getdate()
           ,UserMod=@UserMod
	Where
           IDReserva=@IDReserva And
           IDMensaje=@IDMensaje
