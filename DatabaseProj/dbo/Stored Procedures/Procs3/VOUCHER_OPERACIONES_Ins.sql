﻿


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--HLF-20130812-Limitar el correlativo @IDVoucher a 6 caracteres
--HLF-20130813-Agregando parametro @ObservVoucher
--JHD-20141211-Agregando parametro @CoUbigeo_Oficina
CREATE Procedure dbo.VOUCHER_OPERACIONES_Ins  
 @ServExportac bit,  
 @ExtrNoIncluid bit,  
 @ObservVoucher varchar(max),
 @CoUbigeo_Oficina char(6)='',
 @UserMod char(4),  
 @pIDVoucher int output  
As  
 Set Nocount On  
   
 Declare @IDVoucher int  
 Execute dbo.Correlativo_SelOutput 'VOUCHER_OPERACIONES',1,6,@IDVoucher output  
   
 Insert Into VOUCHER_OPERACIONES(IDVoucher,ServExportac,ExtrNoIncluid,ObservVoucher,CoUbigeo_Oficina,UserMod)  
 Values(@IDVoucher,@ServExportac,@ExtrNoIncluid,
 case when ltrim(rtrim(@ObservVoucher))='' then null else @ObservVoucher end,
 Case When ltrim(rtrim(@CoUbigeo_Oficina))='' Then Null Else @CoUbigeo_Oficina End,
 @UserMod)  
  
 Set @pIDVoucher=@IDVoucher  
 

