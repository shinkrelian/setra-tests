﻿

-----------------------------------------------------------------------------------------

CREATE PROCEDURE RH_AREA_ACTIVO_UPD

		 @CoArea tinyint,
		   @FlActivo BIT,
           @UserMod char(4)
AS
BEGIN
UPDATE [RH_AREA]
   SET 
      FlActivo =@FlActivo
    
      ,[UserMod] = @UserMod
      ,[FecMod] =GETDATE()
      
      
 WHERE CoArea = @CoArea
 END;

