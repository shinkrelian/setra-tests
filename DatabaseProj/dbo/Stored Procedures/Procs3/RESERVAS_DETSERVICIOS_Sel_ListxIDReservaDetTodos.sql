﻿Create Procedure dbo.RESERVAS_DETSERVICIOS_Sel_ListxIDReservaDetTodos
@IDReserva_Det int
As
Set NoCount On
select rds.*,p.NombreCorto as ProveeCot,sdRel.Descripcion as ServicioCot, sd.Tipo as VarianteCot
from RESERVAS_DETSERVICIOS rds
Left Join MASERVICIOS_DET sd On rds.IDServicio_Det=sd.IDServicio_Det
Left Join MASERVICIOS_DET sdRel On IsNull(sd.IDServicio_Det_V,sd.IDServicio_Det) = sdRel.IDServicio_Det
Left Join MASERVICIOS s On sdRel.IDServicio=s.IDServicio
Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
where rds.IDReserva_Det =@IDReserva_Det
