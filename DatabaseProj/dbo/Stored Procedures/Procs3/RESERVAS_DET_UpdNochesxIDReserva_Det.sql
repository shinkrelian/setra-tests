﻿      

CREATE Procedure dbo.RESERVAS_DET_UpdNochesxIDReserva_Det
	@IDReserva_Det int, 
	@FechaOut smalldatetime,      
	@UserMod char(4)      
As      
	Set NoCount On      
	  
	Update RESERVAS_DET Set --Noches=@Noches,      
	FechaOut=@FechaOut, UserMod=@UserMod, FecMod=GETDATE()      
	Where IDReserva_Det=@IDReserva_Det
	  
	Update RESERVAS_DET Set Noches=DATEDIFF(D,Dia,FechaOut),      
	UserMod=@UserMod, FecMod=GETDATE()      
	Where IDReserva_Det=@IDReserva_Det
	   
	If Not Exists(Select cd.IDDET From COTIDET cd   
	Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=cd.IDServicio_Det   
	And sd.ConAlojamiento=1  
	Inner Join MASERVICIOS s On s.IDServicio=sd.IDServicio And s.Dias>1  
	Inner Join RESERVAS_DET rd On IDReserva_Det=@IDReserva_Det And sd.IDServicio_Det=rd.IDServicio_Det
	)      

	Begin  

	Update RESERVAS_DET Set NetoGen = NetoGen * Noches,    
	UserMod=@UserMod, FecMod=GETDATE()      
	Where IDReserva_Det=@IDReserva_Det

	Update RESERVAS_DET Set TotalGen = NetoGen + IgvGen,    
	UserMod=@UserMod, FecMod=GETDATE()      
	Where IDReserva_Det=@IDReserva_Det
	End  
  