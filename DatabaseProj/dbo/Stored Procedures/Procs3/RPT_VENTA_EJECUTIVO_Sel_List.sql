﻿
-------------------------------------------------------------------------------------------------------

--CREATE
CREATE PROCEDURE RPT_VENTA_EJECUTIVO_Sel_List
	@NuAnio char(4)='',
	@CoUserEjecutivo char(4)=''
AS
BEGIN
	Select
		NuAnio,
 		CoUserEjecutivo,
 		Ejecutivo=isnull((Select nombre from MAUSUARIOS where IDUsuario=CoUserEjecutivo),''),
 		SsImporte,
 		QtNumFiles
 	From dbo.RPT_VENTA_EJECUTIVO
	Where 
		(NuAnio = @NuAnio or LTRIM(rtrim(@NuAnio))='' ) And 
		(CoUserEjecutivo = @CoUserEjecutivo or LTRIM(rtrim(@CoUserEjecutivo))='')
END;
