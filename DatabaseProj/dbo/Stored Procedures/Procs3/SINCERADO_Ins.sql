﻿Create Procedure dbo.SINCERADO_Ins  
@IDOperacion_Det int,  
@IDServicio_Det int, 
@UserMod char(4),  
@pNuSincerado int output  
As  
Set NoCount On  
Declare @NuSincerado int = (select Isnull(MAX(NuSincerado),0)+1 from SINCERADO)  
Insert into dbo.SINCERADO  
  (NuSincerado,  
   IDOperacion_Det,  
   IDServicio_Det,  
   UserMod,  
   FecMod)  
    values  
     (@NuSincerado,  
      @IDOperacion_Det,  
      @IDServicio_Det,  
      @UserMod,  
      Getdate())  
Set @pNuSincerado = @NuSincerado  
