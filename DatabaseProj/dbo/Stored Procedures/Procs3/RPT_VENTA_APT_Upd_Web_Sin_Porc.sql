﻿create PROCEDURE [dbo].[RPT_VENTA_APT_Upd_Web_Sin_Porc]
	@NuAnio char(4),
	@CoMes char(2),
	@SsImporte_Sin_Porc_Web numeric(12,4),
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[RPT_VENTA_APT]
	Set
		SsImporte_Sin_Porc_Web = @SsImporte_Sin_Porc_Web,
 		[UserMod] = @UserMod,
 		[FecMod] = getdate()
 	Where 
		[NuAnio] = @NuAnio And 
		[CoMes] = @CoMes
END

