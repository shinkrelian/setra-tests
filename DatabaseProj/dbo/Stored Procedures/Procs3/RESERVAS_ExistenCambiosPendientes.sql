﻿Create Procedure dbo.RESERVAS_ExistenCambiosPendientes
@IDCab int,
@pExtCambioPend bit output
As
Begin 
	Set @pExtCambioPend = 0
	set @pExtCambioPend = dbo.FnDevuelvePendienteUpdateMReservas(@IDCab)
End
