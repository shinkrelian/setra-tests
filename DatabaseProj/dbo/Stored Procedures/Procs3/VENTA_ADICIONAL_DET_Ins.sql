﻿--HLF-20160218-@CoServicio a tipo de dato nvarchar(8) 
CREATE Procedure [dbo].[VENTA_ADICIONAL_DET_Ins]
	@NuVtaAdi int,
	@CoServicio nvarchar(8),--smallint,
	@NuPax int,
	@NuGrupo tinyint,
	@FlTitular bit,
	@SsMonto numeric(7,2),
	@UserMod char(4)
As
	Set Nocount On

	Insert Into VENTA_ADICIONAL_DET
	(NuVtaAdi,
	CoServicio,
	NuPax,
	NuGrupo,
	FlTitular,
	SsMonto,
	UserMod)
	Values
	(@NuVtaAdi,
	@CoServicio,
	@NuPax,
	Case When @NuGrupo=0 Then Null Else @NuGrupo End,
	@FlTitular,
	@SsMonto,
	@UserMod)


