﻿Create Procedure [dbo].[RESERVAS_TAREAS_Del]
	@IDReserva int,	
	@IDTarea	smallint,
	@IDCab	int	
As
	Set NoCount On

	Delete From RESERVAS_TAREAS 
	Where IDReserva=@IDReserva And IDTarea=@IDTarea And IDCab=@IDCab
