﻿CREATE Procedure [dbo].[RESERVAS_MENSAJESTELEF_Ins]
	@IDReserva int,	
	@Contacto varchar(100),           
	@IDUsuario char(4),
	@Fecha	smalldatetime,
	@Asunto varchar(200),
	@Mensaje varchar(Max),
	@UserMod char(4)
As
	Set NoCount On
	Declare @IDMensaje tinyint=Isnull((Select Max(IDMensaje) From RESERVAS_MENSAJESTELEF 
		Where IDReserva=@IDReserva),0)+1
	
	
	INSERT INTO RESERVAS_MENSAJESTELEF
           ([IDReserva]
           ,[IDMensaje]
           ,[Contacto]    
           ,Fecha       
           ,[IDUsuario]
           ,[Asunto]
           ,[Mensaje]
           ,[UserMod])
           
     VALUES
           (@IDReserva
           ,@IDMensaje
           ,@Contacto
           ,@Fecha
           ,@IDUsuario
           ,@Asunto 
           ,@Mensaje
           ,@UserMod)
