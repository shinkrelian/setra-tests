﻿
---------------------------------------------------------------



CREATE PROCEDURE [RH_TRABAJADOR_Ins]
	@CoTrabajador char(11),
	@NoApPaterno varchar(50)='',
	@NoApMaterno varchar(50)='',
	@NoNombres varchar(100)='',
	@CoTipoDoc char(3)='',
	@NuNumeroDoc char(11)='',
	@UserMod char(4)=''
AS
BEGIN
	Insert Into [dbo].[RH_TRABAJADOR]
		(
			[CoTrabajador],
 			[NoApPaterno],
 			[NoApMaterno],
 			[NoNombres],
 			[CoTipoDoc],
 			[NuNumeroDoc],
 			[UserMod]
 		)
	Values
		(
			@CoTrabajador,
 			Case When ltrim(rtrim(@NoApPaterno))='' Then Null Else @NoApPaterno End,
 			Case When ltrim(rtrim(@NoApMaterno))='' Then Null Else @NoApMaterno End,
 			Case When ltrim(rtrim(@NoNombres))='' Then Null Else @NoNombres End,
 			Case When ltrim(rtrim(@CoTipoDoc))='' Then Null Else @CoTipoDoc End,
 			Case When ltrim(rtrim(@NuNumeroDoc))='' Then Null Else @NuNumeroDoc End,
 			Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End
 		)
END;
