﻿CREATE Procedure [dbo].[RESERVAS_MENSAJES_Ins]
@ConversationID varchar(255),
@IDReserva int,
@Asunto varchar(max),
@UserMod char(4)
as
	Set NoCount On
	insert into RESERVAS_MENSAJES
			    ([ConversationID]
			    ,[IDReserva]
			    ,[Asunto]
			    ,[UserMod])
			 	values (@ConversationID
			 			,@IDReserva
			 			,@Asunto
			 			,@UserMod)
