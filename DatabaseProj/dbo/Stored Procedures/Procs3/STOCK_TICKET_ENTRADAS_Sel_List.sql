﻿
--HLF-20151103-ste.IDServicio, quitando Left Join MASERVICIOS_DET 
CREATE Procedure dbo.STOCK_TICKET_ENTRADAS_Sel_List
@Descripcion varchar(Max)
As
Set NoCount On
Select --ste.IDServicio_Det,
ste.IDServicio,
p.NombreCorto,--sd.Anio,sd.Tipo, 
--sd.Descripcion,
s.Descripcion,
ste.QtStkEnt 
from STOCK_TICKET_ENTRADAS ste 
--Left Join MASERVICIOS_DET sd On ste.IDServicio_Det=sd.IDServicio_Det
Left Join MASERVICIOS s On s.IDServicio=ste.IDServicio
Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
Where s.cActivo='A' and 
	  (Ltrim(Rtrim(@Descripcion))='' Or
	  s.Descripcion Like '%'+Ltrim(Rtrim(@Descripcion))+'%')
Order By s.Descripcion desc




