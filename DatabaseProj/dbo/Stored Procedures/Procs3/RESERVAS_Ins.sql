﻿  
--HLF-20130614-Cambiando tamanio de @Observaciones de varchar(250) a varchar(MAX)       
--JRF-20130703-Agregando el campo EstadoSolicitud.(Para tema de mensajería)    
--HLF-20131010-Agregando parametro @RegeneradoxAcomodo 
--HLF-20131030-Agregando parametro @IDReservaProtecc 
CREATE Procedure [dbo].[RESERVAS_Ins]        
 @IDProveedor char(6),        
 @IDCab int,        
 @IDFile char(8),        
 @Estado char(2),        
 @IDUsuarioVen char(4),        
 @IDUsuarioRes char(4),        
 @Observaciones varchar(MAX),        
 @CodReservaProv varchar(50),      
 @EstadoSolicitud char(4),    
 @RegeneradoxAcomodo bit,  
 @IDReservaProtecc int,
 @UserMod char(4),         
 @pIDReserva int output        
As        
 Set NoCount On        
         
 Declare @IDReserva int        
         
 Execute dbo.Correlativo_SelOutput 'RESERVAS',1,10,@IDReserva output        
        
 INSERT INTO RESERVAS        
      ([IDReserva]        
      ,[IDProveedor]        
      ,[IDCab]        
      ,[IDFile]        
      ,[Estado]        
      ,[EstadoSolicitud]    
      ,[IDUsuarioVen]        
      ,[IDUsuarioRes]        
      ,[Observaciones]        
      ,CodReservaProv     
      ,RegeneradoxAcomodo
      ,IDReservaProtecc  
      ,[UserMod]        
      ,[FecMod])        
   VALUES        
      (@IDReserva        
      ,@IDProveedor        
      ,@IDCab        
      ,@IDFile --(select IDFile from COTICAB Where IDCAB=@IDCab)        
      ,@Estado        
      ,@EstadoSolicitud    
      ,@IDUsuarioVen        
      ,@IDUsuarioRes        
      ,Case when ltrim(rtrim(@Observaciones))='' then null else @Observaciones End        
      ,Case when ltrim(rtrim(@CodReservaProv))='' then null else @CodReservaProv End        
      ,@RegeneradoxAcomodo  
      ,Case When @IDReservaProtecc=0 Then Null Else @IDReservaProtecc End
      ,@UserMod        
      ,GETDATE()        
      )        
         
 Set @pIDReserva=@IDReserva     
   
  
  
