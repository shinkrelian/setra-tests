﻿Create Procedure dbo.sysmail_account_Ins	
	@email_address nvarchar(128),
	@display_name nvarchar(128),	
	--@UserMod	char(4),
	@paccount_id int output
As
	Set NoCount On  
	
	INSERT INTO [msdb].[dbo].[sysmail_account]
           ([name]           
           ,[email_address]
           ,[display_name]
           ,[replyto_address]
           ,[last_mod_datetime]
           )
     VALUES
           (@email_address,           
           @email_address,
           @display_name,
           @email_address,
           GETDATE())
  
	Set @paccount_id =(Select MAX(account_id) From msdb.dbo.sysmail_account)	         
