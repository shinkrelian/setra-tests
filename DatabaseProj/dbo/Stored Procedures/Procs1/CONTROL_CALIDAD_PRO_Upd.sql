﻿--JRF-20151125-Ampliar TxComentario a TEXT
CREATE Procedure dbo.CONTROL_CALIDAD_PRO_Upd
@TxComentario Text,
@IDPax int,
@UserMod char(4),
@CoProveedor char(6)='',  
@CoCliente char(6)='',    
@NuControlCalidad int
As
Set NoCount On
Update CONTROL_CALIDAD_PRO 
	Set TxComentario=Case When Ltrim(Rtrim(Cast(@TxComentario as varchar(Max)))) = '' Then Null Else @TxComentario End,
	    IDPax = Case When @IDPax = 0 Then Null Else @IDPax End,
		CoProveedor=  Case When Ltrim(Rtrim(@CoProveedor)) = '' Then Null Else Ltrim(Rtrim(@CoProveedor)) End,
		CoCliente= Case When Ltrim(Rtrim(@CoCliente)) = '' Then Null Else Ltrim(Rtrim(@CoCliente)) End,
		UserMod=@UserMod,
		FecMod=GETDATE()
Where NuControlCalidad=@NuControlCalidad
