﻿--JRF-20160104-Agregar columna ,c2.IDCliente
--JRF-20160209-Agregar [NuPedInt]
--JRF-20160323-Agregar Cotizacion
--JRF-20160328-Agregar Estado AddIn
CREATE Procedure [dbo].[CORREOFILE_SelxPk] 
@NuCorreo int
As
Set NoCount On
select IsNull(u.Usuario,'') as Perfil,IsNull(c2.IDCliente,IsNull(p.CoCliente,'')) as IDCliente,
	   IsNull(c2.Cotizacion,'') as Cotizacion,IsNull(c2.IDFile,'') as IDFile,IsNull(c.NuPedInt,0) as NuPedInt,
	   IsNull(p.NuPedido,'') as NuPedido,IsNull(c2.CoEstadoAddIn,'PD') as CoEstadoAddIn,c.*
From .BDCORREOSETRA..CORREOFILE c Left Join MAUSUARIOS u On c.CoUsrIntDe=u.IDUsuario
Left Join COTICAB c2 On c.IDCab=c2.IDCAB
Left Join PEDIDO p On c.NuPedInt = p.NuPedInt
Where NuCorreo=@NuCorreo
