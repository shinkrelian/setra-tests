﻿
Create Procedure dbo.MABANCOS_Sel_ListUnidoTarjetaCredito
@Todos bit
as
Begin
select '' as Codigo,'<TODOS>' As Descripcion 
where @Todos = 1
Union
select IDBanco as Codigo,Descripcion from MABANCOS
where IDBanco <> '000'
union
select CodTar as Codigo,Descripcion from MATARJETACREDITO
order by Descripcion
End
