﻿--JRF-20141226-Agregar el filtro por el NuControlCalidad
--JRF-20150217-Considerar la existencia del Registro
--JHD-20150217-'' AS Ciudad, '' as Pais ,
CREATE Procedure dbo.MACUESTIONARIO_SelxIDTipoProvxIDCab    
@IDTipoProv char(3),    
@IDCab int,  
@NuControlCalidad int  
as    
Select c.NuCuest,'000000' as IDProveedor,NoDescripcion,  
	'' AS Ciudad, '' as Pais ,
    Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,    
    Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Isnull(ccpc.TxComentario,'') as TxComentario,
    Case When Not Exists(select NuControlCalidad from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc2 
						where ccpc2.NuControlCalidad=@NuControlCalidad and ccpc2.NuCuest=ccpc.NuCuest and ccpc2.IDProveedor=ccpc.IDProveedor) 
	Then 'N' else 'M' End as ValRegistro
from MACUESTIONARIO c Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On c.NuCuest=ccpc.NuCuest    
and IsNull(ccpc.NuControlCalidad,0)= @NuControlCalidad    

where (Activo=1 and IsNull(IDTipoProv,'') = @IDTipoProv)    

