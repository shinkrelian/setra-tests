﻿--JRF-20120824-Nueva Columna EmitidoSetours                        
--JRF-20120823-Alteracion de Orden en las columnas.                          
--HLF-20120920-Quitar Cast a campo Llegada                      
--HLF-20120920-Rutas Ubigeo Codigo a DC                      
--HLF-20120920-Columna RutaVuelo que muestre campo DC por Join                      
--HLF-20120921-Columna as Descripcion a ,cv.Servicio as Descripcion                      
--HLF-20120921-Columna as Dia a ,cv.Fec_Emision as Dia                      
--HLF-20130121-Agregando Order by Salida                      
--HLF-20130124-Agregando ,cv.Fec_Emision as Dia  por cv.Fec_Salida                     
--JRF-20130124-Nuevos Campos(Operador)                  
--HLF-20130208-Order by , Cast(CV.Salida as Time)                
--HLF-20130221-Agregando campo IdaVuelta                
--JRF-20130312-Invirtiendo posicion de Servicio y Proveedor              
--HLF-20130327-Agregando Isnull(cdOri.IDProveedor,cdDes.IDProveedor) as IDProveedorTransfer y quitando IdaVuelta            
--HLF-20130408-Agregando IsNull(IDServicio_DetPeruRail,0) as IDServicio_DetPeruRail            
--HLF-20130411-Agregando columna CantPax          
--HLF-20130613-cv.Fec_Salida as Dia  por ,Case When cv.TipoTransporte='V' Then cv.Fec_Salida Else cv.Fec_Emision End as Dia         
--JRF-20131007-Ordenandolo por Dia y Hora de Salida(Salida es otra columna char(5))      
--JRF-20141021-Nuevo campos [CoTicket] - [NuSegmento]  
--HLF-20150218-FlUpdExcelPRail
--PPMG-20151209-Se agrego dos campos FlMotivo y DescMotivo
CREATE Procedure [dbo].[COTIVUELOS_Sel_List]                                    
 @ID int,                                                
 @IDCAB int,                                            
 @Cotizacion char(8),                                             
 @Ruta varchar(100),                                                
 @Origen char(6),                                              
 @Destino char(6),                                                
 @TipoTransporte char(1),                                
 @Emitido char(1),                                              
 @Status char(2)                                              
AS
BEGIN
 Set NoCount On                                                
 Select * from(                            
 Select                                          
 case when cv.IdDet is null then '' else cv.IdDet End as iddet            
 --,(select dia from cotidet where IDDET=cv.iddet) as Dia                                    
 --,cv.Fec_Emision as Dia                      
 --,cv.Fec_Salida as Dia        
         
 ,Case When cv.TipoTransporte='V' Then cv.Fec_Salida Else cv.Fec_Emision End as Dia         
         
 --,(select servicio from cotidet  where IDDET=cv.iddet) as Descripcion                                    
 ,pr.NombreCorto as DescProveedor                              
 ,cv.Servicio as Descripcion                      
           
 ,(SELECT COUNT(*) FROM COTITRANSP_PAX WHERE IDTransporte=cv.ID ) as CantPax          
           
 /* 1*/      ,[ID]                                            
 /* 2*/      ,cv.IDCAB                                           
 /* 3*/      ,cv.Cotizacion                                    
 /* 4*/      ,[NombrePax]                                        
 --/* 5*/      ,[Ruta] as RutaVuelo                                            
 ,IsNull(u.DC,'')+' / '+IsNull(Ub.DC,'') as RutaVuelo                           
 /* 6*/      ,[Vuelo]                                        
 /* 7*/      ,u.Descripcion Origen                                            
 /* 8*/      ,Ub.Descripcion Destino                                            
 /* 9*/      ,[RutaD]                                            
 /*10*/      ,[RutaO]                                            
 ,IsNull(u.DC,'') as RutaOAbr                              
 ,IsNull(Ub.DC,'') as RutaDAbr                              
 /*11*/      ,Isnull(Num_Boleto,'') as Num_Boleto    
 /*12*/      ,isnull([Fec_Emision],cast('01/01/1900' as smalldatetime)) as Fec_Emision    
 /*13*/      ,isnull([Fec_Documento],cast('01/01/1900' as smalldatetime)) as Fec_Documento    
 /*14*/      ,ISNULL([Fec_Salida],CAST('01/01/1900' As smalldatetime)) as Fec_Salida    
 /*16*/      ,Cast([Salida] As CHAR(5)) Salida                          
 /*15*/      ,ISNULL([Fec_Retorno],CAST('01/01/1900' As smalldatetime)) as Fec_Retorno    
 --/*25*/      ,Cast([Llegada] As Char(5)) Llegada                          
 ,Llegada                      
 /*+1*/      ,Isnull(Cast([HrRecojo] As Char(5)),'00:00') as HrRecojo                          
 /*17*/      ,[PaxC]                              
 /*18*/      ,[PCotizado]                                            
 /*19*/      ,[TCotizado]                                            
 /*20*/      ,[PaxV]                                            
 /*21*/      ,[PVolado]                                            
 /*22*/      ,[TVolado]                                            
 /*23*/      ,[Status]                                            
 /*24*/      ,[Comentario]                                                          
 /*26*/      ,[Emitido]                                            
 /*+1*/      ,cv.TipoTransporte            
 /*27*/      ,Isnull([CodReserva],'') as CodReserva    
 /*28*/      ,[Tarifa]                                            
 /*29*/      ,[PTA]                                            
 /*30*/      ,[ImpuestosExt]                                            
 /*31*/      ,[Penalidad]                                            
 /*32*/      ,[IGV]                                            
 /*33*/      ,[Otros]                                            
 /*34*/      ,[PorcComm]                                            
 /*35*/      ,[MontoContado]                                            
 /*36*/      ,[MontoTarjeta]                                
 /*37*/      ,[IdTarjCred]                                
 /*38*/      ,Isnull([NumeroTarjeta],'') as NumeroTarjeta    
 /*39*/      ,[Aprobacion]                                            
 /*40*/      ,[PorcOver]                                            
 /*41*/      ,[PorcOverInCom]                                            
 /*42*/      ,[MontComm]                                            
 /*43*/      ,[MontCommOver]                                            
 /*44*/      ,[IGVComm]                                            
 /*45*/      ,[IGVCommOver]                                            
 /*46*/      ,[NetoPagar]                                          
 /*47*/      ,[PorcTarifa]                                          
 /*48*/      ,[Descuento]                                            
 /*49*/      ,[IdLineaA]                                            
               
 /*50*/      ,[MontoFEE]                                      
 /*51*/      ,[IGVFEE]                                       
 /*52*/      ,[TotalFEE]                            
 /*53*/      ,[EmitidoSetours]                  
 ,EmitidoOperador                  
 ,isnull(IDProveedorOperador,'') As IDProveedorOperador                  
 ,isnull(CostoOperador,'0.00') As CostoOperador,                 
 --IsNull(cv.IdaVuelta,'') as IdaVuelta                            
 Isnull(cdOri.IDProveedor,cdDes.IDProveedor) as IDProveedorTransfer,            
 IsNull(IDServicio_DetPeruRail,0) as IDServicio_DetPeruRail,  
 IsNull(CoTicket,'') as CoTicket,  
 IsNull(NuSegmento,0) as NuSegmento,
 cv.FlUpdExcelPRail,
 ISNULL(cv.FlMotivo,0) AS FlMotivo,
 ISNULL(cv.DescMotivo,'') AS DescMotivo
 From COTIVUELOS cv Left join MAUBIGEO u On cv.RutaO=u.IDubigeo                                                 
 Left Join MAUBIGEO Ub On cv.RutaD=ub.IDubigeo                                    
 Left Join MAPROVEEDORES pr on cv.IdLineaA=pr.IDProveedor                      
 LEFT JOIN COTIDET cdOri ON CV.IdDet=cdOri.IDDetTransferOri            
 LEFT JOIN COTIDET cdDes ON CV.IdDet=cdDes.IDDetTransferDes                             
 Where (LTRIM(RTRIM(@Ruta))='' Or Ruta like '%'+@Ruta+'%')                    
 And (LTRIM(rtrim(@Cotizacion))='' Or cv.Cotizacion=@Cotizacion)                                            
 And (LTRIM(RTRIM(@Origen))='' Or RutaO=@Origen)                                           
 And (LTRIM(RTRIM(@Destino))='' Or RutaD=@Destino)                                                
 And (LTRIM(RTRIM(@Status))='' Or Status=@Status)                                                 
 And (LTRIM(RTRIM(@Emitido))='' Or Emitido=@Emitido)                                            
 and (cv.IDCAB=@IDCAB Or @IDCAB=0)                                              
 And (ID=@ID Or @ID=0) And (LTRIM(rtrim(@TipoTransporte))='' Or cv.TipoTransporte=@TipoTransporte)           
 ) As X      
 Order by cast((CONVERT(CHAR(10),X.Dia,103)+' '+Isnull(X.Salida,'00:00:00')) as smalldatetime) --cv.Fec_Salida, Cast(CV.Salida as Time)                
END
