﻿CREATE Procedure [dbo].[MAIDIOMAS_SelCulturas_Cbo]
	@bTodos	bit

As

	Set NoCount On

	SELECT * FROM
	(
	Select '' as Cultura,'<TODOS>' as DescIdioma, 0 as Ord
	Union	
	
	Select IsNull(Cultura,'') as Cultura,IDidioma as DescIdioma, 1 as Ord From MAIDIOMAS
	Where Activo='A' And PorDefault=1
	) AS X
	Where (@bTodos=1 Or Ord<>0) 
	Order by Ord,2
