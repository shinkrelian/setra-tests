﻿
--HLF-20151124-isnull(Celular,'') as Celular ,
 CREATE Procedure [dbo].[MACONTACTOSPROVEEDOR_Sel_xIDProveedor]
	@IDProveedor	char(6)
As
	Set NoCount On
	
	Select --IDContacto, Nombres+' '+Apellidos as Nombre
	Titulo+' '+Nombres+' '+Apellidos as Nombre,
	Isnull(Telefono,'')+' '+Isnull(Anexo,'') as Telefono,
	isnull(Celular,'') as Celular ,
	ISnull(Email,'') as Email, Cargo, IDContacto	
	From MACONTACTOSPROVEEDOR
	Where IDProveedor=@IDProveedor
	Order by 1

