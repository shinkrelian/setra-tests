﻿
--HLF-20120823-Nuevo campo Siglas
CREATE Procedure [dbo].[MAIDIOMAS_Ins]
	@IDIdioma varchar(12),
	@Siglas	char(3),
    @UserMod char(4)
As
	Set NoCount On
	
	
	INSERT INTO MAIDIOMAS
           ([IDidioma]           
           ,Siglas
           ,[UserMod]
           )
     VALUES
           (@IDIdioma,            
            @Siglas,
            @UserMod)

