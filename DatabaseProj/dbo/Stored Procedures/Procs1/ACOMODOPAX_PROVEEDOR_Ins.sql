﻿CREATE Procedure [dbo].[ACOMODOPAX_PROVEEDOR_Ins]    
@IDReserva int,    
@IDPax int,    
@IDHabit tinyint,    
@Capacidad tinyint,  
@EsMatrimonial Bit,
@UserMod char(4)    
As    
 Set NoCount On    
 Insert Into ACOMODOPAX_PROVEEDOR (IDReserva,IDPax,IDHabit,CapacidadHab,EsMatrimonial,UserMod)    
  Values (@IDReserva,@IDPax,@IDHabit,@Capacidad,@EsMatrimonial,@UserMod)    
