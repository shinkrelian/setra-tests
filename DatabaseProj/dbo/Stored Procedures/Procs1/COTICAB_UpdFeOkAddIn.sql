﻿Create Procedure dbo.COTICAB_UpdFeOkAddIn
@IDCab int,
@FeOKClienteAddIn smalldatetime
As
Set NoCount On
Update COTICAB
	Set CoEstadoAddIn = 'OK',
		FeOKClienteAddIn = Case When FeOKClienteAddIn is null then @FeOKClienteAddIn else FeOKClienteAddIn End
Where IDCAB=@IDCab
