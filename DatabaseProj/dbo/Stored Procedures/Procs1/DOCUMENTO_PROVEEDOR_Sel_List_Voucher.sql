﻿CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_List_Voucher]        
 @NuDocumProv int=0,        
 --@NuVoucher int=0,        
 @NuVoucher varchar(14)='',        
 --@NuDocum varchar(10)='',        
 @NuDocum varchar(30)='',        
 @CoTipoDoc char(3)='',        
 --@CoEstado char(2)='',        
 @FlActivo bit=0,        
 @CoProveedor char(6)='',    
 @IDOperacion int=0,
 @CoMoneda char(3)=''
AS        
BEGIN        

declare @tblOrdPago as table
(
IDOrdPag int null
)

declare @IDOrdPag int=0

If Left(@NuVoucher,2)='PR' Set @NuVoucher='0'  


If Left(@NuVoucher,2)<>'PR' 
begin
insert into @tblOrdPago
	select distinct isnull(op.IDOrdPag,0)
	from 
	ordenpago op
	left join reservas r on op.IDReserva=r.IDReserva
	left join OPERACIONES o on o.IDReserva=r.IDReserva
	left join OPERACIONES_DET od on od.IDOperacion=o.IDOperacion
	where od.IDVoucher=@NuVoucher and o.IDOperacion=@IDOperacion /*AND OP.IDEstado='PG'*/
end
  
--select  @IDOrdPag

SELECT     dp.NuDocumProv,        
   dp.NuVoucher,        
   --dp.NuDocum,                 
   --NuDocum2=dbo.FnFormatearDocumProvee(dp.NuDocum),      
   --NuDocum= case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
   NuDocum= case when FlCorrelativo=1 then '' else
				 case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,
   --NuDocum2=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,  
   NuDocum2=case when FlCorrelativo=1 then '' else
				case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,  
   --dp.NuDocInterno,        
    NuDocInterno= dp.CardCodeSAP,    
   dp.CoTipoDoc,         
          td.Descripcion,        
          dp.FeEmision,        
          dp.FeRecepcion,        
          dp.CoTipoDetraccion,         
          PorcentajeDet=(select isnull(SsTasa,0)*100 from dbo.MATIPODETRACCION where CoTipoDetraccion=dp.CoTipoDetraccion),        
          dp.CoMoneda,        
          mo.Descripcion AS Moneda,   
          SsNeto,        
          SsOtrCargos,        
          SsDetraccion,        
          dp.SsIGV,        
          dp.SSTotalOriginal,         
           dp.SSTipoCambio,        
          dp.SSTotal,        
          --dp.CoEstado,        
          --Estado=case when dp.CoEstado='RE' THEN 'Recepcionado' when dp.CoEstado='PG' then 'Pagado' when dp.CoEstado='AN' THEN 'Anulado' else '' end        
          dp.FlActivo        
FROM         DOCUMENTO_PROVEEDOR dp LEFT JOIN        
                      VOUCHER_OPERACIONES vo ON dp.NuVoucher = vo.IDVoucher     
                      Left Join OPERACIONES o On o.IDOperacion=dp.idoperacion    
                      LEFT JOIN MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc     
                      LEFT JOIN    MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda        
WHERE     (dp.NuDocumProv = @NuDocumProv or @NuDocumProv=0) AND        
--(dp.NuVoucher = @NuVoucher or (@NuVoucher=0 and @CoProveedor<>'001836')) AND        
(dp.NuVoucher = @NuVoucher or 
	--(@NuVoucher='0' and @CoProveedor='001836' and dp.IDOperacion=@IDOperacion)) AND        
	(dp.NuVouPTrenInt=@NuVoucher and @CoProveedor='001836')) AND        
(@NuDocum='' OR dp.NuDocum LIKE '%'+@NuDocum+'%' ) AND         
                      (dp.CoTipoDoc = @CoTipoDoc or @CoTipoDoc='') AND        
                     (dp.FlActivo = @FlActivo or @FlActivo =0)-- AND        
                      --(dp.CoEstado = @CoEstado or @CoEstado='')        
and (dp.CoMoneda_FEgreso=@CoMoneda or @CoMoneda='')
union 
SELECT     dp.NuDocumProv,    
   dp.NuVoucher,    
  -- NuVoucher=ISNULL(@NuVoucher,''),
   --dp.NuDocum,    
   --NuDocum2=dbo.FnFormatearDocumProvee(dp.NuDocum),
  -- NuDocum=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
    NuDocum= case when FlCorrelativo=1 then '' else
				 case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,
   --NuDocum2=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum, 
    NuDocum2=case when FlCorrelativo=1 then '' else
				case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,  
   --dp.NuDocInterno,        
    NuDocInterno= dp.CardCodeSAP,      
   dp.CoTipoDoc,     
          td.Descripcion,    
          dp.FeEmision,    
          dp.FeRecepcion,    
          dp.CoTipoDetraccion,     
          PorcentajeDet=(select isnull(SsTasa,0)*100 from dbo.MATIPODETRACCION where CoTipoDetraccion=dp.CoTipoDetraccion),    
          dp.CoMoneda,    
          mo.Descripcion AS Moneda,    
          SsNeto,    
          SsOtrCargos,    
          SsDetraccion,    
          dp.SsIGV,    
          dp.SSTotalOriginal,     
           dp.SSTipoCambio,    
          dp.SSTotal,    
          dp.FlActivo    
FROM         DOCUMENTO_PROVEEDOR dp   
INNER JOIN  MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc INNER JOIN    
                      MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda    
WHERE     (dp.NuDocumProv = @NuDocumProv or @NuDocumProv=0) AND    
--@IDOrdPag>0 AND
(select count(IDOrdPag) from @tblOrdPago)>0 AND
--(dp.NuOrden_Servicio In (select NuOrden_Servicio from ORDEN_SERVICIO os where ltrim(rtrim(os.CoOrden_Servicio)) Like @NuVoucher+'%' )) AND    
--(dp.CoOrdPag In (select IDOrdPag from ORDENPAGO op where op.IDOrdPag  = @IDOrdPag and op.IDEstado='PG' )) AND    
(dp.CoOrdPag In (select IDOrdPag from ORDENPAGO op where op.IDOrdPag  in (select IDOrdPag from @tblOrdPago) /*and op.IDEstado='PG'*/ )) AND    
(ltrim(rtrim(@NuDocum))='' OR dp.NuDocum LIKE '%'+@NuDocum+'%' ) AND     
                      (dp.CoTipoDoc = @CoTipoDoc or ltrim(rtrim(@CoTipoDoc))='') AND    
                        (dp.FlActivo = @FlActivo or @FlActivo =0)  
	and (dp.CoMoneda_FEgreso=@CoMoneda or @CoMoneda='')
END;     

