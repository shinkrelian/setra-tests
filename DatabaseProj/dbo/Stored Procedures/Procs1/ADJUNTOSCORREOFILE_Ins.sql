﻿

CREATE Procedure [dbo].[ADJUNTOSCORREOFILE_Ins]
@NuCorreo int,
@NoArchAdjunto varchar(150),
@UserMod char(4)
As
	Set NoCount On
	Insert Into .BDCORREOSETRA..ADJUNTOSCORREOFILE
		values(@NuCorreo,@NoArchAdjunto,@UserMod,GetDate())

