﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--ALTER
--JRF-20140811-Agregar el CoTipoDet
--JHD-20141204-Agregar el Pais del proveedor
--JHD-20141204-Agregar el campo BoletaServiciosExterior
--JHD-20141211-Condicion para campos FacturaExportacion,BoletaNoExportacion y BoletaServiciosExterior
CREATE Procedure dbo.DOCUMENTO_DET_Sel_ListPDF  
@IDCab int  
AS  
Set NoCount On  
Select dd.IDTipoDoc,
dd.NuDocum,
dd.IDProveedor,
Isnull(p.NombreCorto,uTras.Nombre) as Proveedor,  
--
Pais=isnull((select Descripcion from MAUBIGEO where IDubigeo=(select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)),''),

--
Isnull(ISNULL(CAST(dd.NuOrdenServicio as varchar(13)),
CAST(dd.IDVoucher as varchar(13))),'') as NroVoucher_OS  
,dd.SsCompraNeto,
dd.SsIGVCosto,
dd.SsIGVSFE,
dd.IDMonedaCosto,
dd.SsTotalCostoUSD,  
toc.Descripcion as TipoOperacionContable,
dd.SsTotalCostoUSD,
dd.SsMargen,
dd.SsTotalDocumUSD,  

 CASE WHEN dd.IDTipoOC = '011' then 0
 else
	CASE WHEN dd.IDTipoOC = '001' then  
	dd.SsTotalDocumUSD Else   
	CASE WHEN dd.IDTipoOC = '006' Then dd.SsTotalDocumUSD / 2 Else 0 End  
	End 
 end as FacturaExportacion,  
 
 CASE WHEN dd.IDTipoOC = '011' then 0
 ELSE
	CASE WHEN dd.IDTipoOC = '001' then  
	0 Else   
	CASE WHEN dd.IDTipoOC = '006' Then dd.SsTotalDocumUSD / 2 Else dd.SsTotalDocumUSD End End
 END as BoletaNoExportacion,  
  
  CASE WHEN dd.IDTipoOC = '011' then dd.SsTotalDocumUSD Else 0 End as BoletaServiciosExterior,  
  
Isnull(dd.IDVoucher,0) as IDVoucher,

Isnull(dd.NuOrdenServicio,'') as NuOrdenServicio,  
Case When uTras.IDUsuario is not null Or dd.IDProveedor = '001935' Or p.IDUsuarioTrasladista is not null   
  then 'I' Else '' End as Interno,
  dd.IDTipoOC,  
  Case When (p.IDTipoProv = '004' or p.IDTipoProv = '015') and (not dd.IDProveedor in('001836','001593')) Then  'VB' Else '' End as VueloBus  ,
  dd.CoTipoDet
from DOCUMENTO_DET dd Left Join DOCUMENTO d On dd.IDTipoDoc = d.IDTipoDoc and dd.NuDocum = d.NuDocum  
Left Join MAPROVEEDORES p On dd.IDProveedor = p.IDProveedor Left Join MATIPOOPERACION toc On dd.IDTipoOC = toc.IdToc   
Left Join MAUSUARIOS uTras On dd.IDProveedor = uTras.IDUsuario  
where d.IDCab = @IDCab   

