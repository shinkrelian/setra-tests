﻿Create Procedure dbo.COTIDET_UpdItem
	@IDDet int,
	@Item numeric(6,3)
As
	Set Nocount On
	
	Update COTIDET Set Item=@Item, ExecTrigger=0 Where IDDET=@IDDet
	
