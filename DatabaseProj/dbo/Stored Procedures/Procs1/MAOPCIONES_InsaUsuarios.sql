﻿Create Procedure [dbo].[MAOPCIONES_InsaUsuarios]
	@IDOpc	char(6),
	@UserMod	char(4)
As
	Set NoCount On
	
	Insert Into MAUSUARIOSOPC(IDOpc, IDUsuario, Consultar, Grabar, Actualizar, Eliminar, UserMod)
	Select @IDOpc,IDUsuario,0,0,0,0,@UserMod From MAUSUARIOS
