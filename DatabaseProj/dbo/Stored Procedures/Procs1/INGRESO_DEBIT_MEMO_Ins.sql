﻿

--HLF-20150817-Insert.. ,SsSaldoMatchDocumento)
--HLF-20150922-@SsComisionBanco
--@sspago+@SsGastosTransferencia
CREATE Procedure dbo.INGRESO_DEBIT_MEMO_Ins
@NuAsignacion int,
@NuIngreso int,
@IDDebitMemo char(10),
@SsPago numeric(10,2),
@SsGastosTransferencia numeric(10,2),
@SsComisionBanco numeric(8,2),
@SsSaldoNuevo numeric(10,2),
@UserMod char(4)
As
Begin
	Set NoCount On
	Insert into INGRESO_DEBIT_MEMO
		  (NuAsignacion,NuIngreso,IDDebitMemo,SsPago,SsGastosTransferencia,
		  SsComisionBanco,
		  SsSaldoNuevo,SsSaldoMatchDocumento,
		  UserMod,FecMod)
	values(@NuAsignacion,@NuIngreso,@IDDebitMemo,@SsPago,@SsGastosTransferencia,
		Case When @SsComisionBanco=0 Then Null Else @SsComisionBanco End,
		@SsSaldoNuevo,@sspago+@SsGastosTransferencia,
		@UserMod,Getdate())
End

