﻿Create Procedure [dbo].[MAFORMASPAGO_Sel_Rpt]
	@Descripcion varchar(50)
As
	Set NoCount On
	
	Select IDFor,Descripcion
	From MAFORMASPAGO
	Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')
	order by 1
