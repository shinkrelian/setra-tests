﻿CREATE Procedure [dbo].[MACONTACTOSCLIENTE_Sel_xContAdminist]	
	@IDCliente char(6)
As
	Set NoCount On
	
	Select IDContacto, Nombres, Apellidos FROM MACONTACTOSCLIENTE 
    WHERE IDCliente=@IDCliente And EsAdminist=1
