﻿Create Procedure dbo.ACOMODOPAX_PROVEEDOR_SelExistexIDReserva
@IDReserva int,
@pExiste bit output
As
	Set NoCount On
	Set @pExiste = 0
	If Exists(select IDReserva from ACOMODOPAX_PROVEEDOR Where IDReserva = @IDReserva)
		Set @pExiste  = 1
