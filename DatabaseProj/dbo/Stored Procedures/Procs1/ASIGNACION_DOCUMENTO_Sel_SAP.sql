﻿--HLF-20150916-And @bCredito=0
--If @bSeCreoAnticipos=0	If Exists(Select dm.IDDebitMemo From DEBIT_MEMO dm Inner Join COTICAB cc 
--HLF-20150925-And d.FeDocum<=(Select if1.FeAcreditada From INGRESO_FINANZAS ...
--abs(
--HLF-20151016-And (CONVERT(SMALLDATETIME,CONVERT(VARCHAR,d.FeDocum,103))<=(Select CONVERT(SMALLDATETIME,CONVERT(VARCHAR,if1.FeAcreditada,103)) From INGRESO_FINANZAS if1 
CREATE Procedure dbo.ASIGNACION_DOCUMENTO_Sel_SAP
@NuAsignacion int,
@TCambioIng numeric(6,3),
@IDDebitMemo char(10),
@bSeCreoAnticipos bit
As

Set NoCount On

Declare --@FecOut char(6)='', @FecAcred char(6)='', 
@bCredito bit=0

--Select @FecAcred = CONVERT(char(6),ifs.FeAcreditada,112), @FecOut = CONVERT(char(6),(Select   
--		 isnull((Select Max(Dia) From COTIDET cd1 Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo     
--		  And u1.IDPais='000323'
--		  Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det  
--		  Where IDCAB=cc.IDCAB And SD1.idTipoOC<>'005'),cc.FechaOut) ),112)
--From INGRESO_FINANZAS ifs Inner Join INGRESO_DEBIT_MEMO idm On ifs.NuIngreso=idm.NuIngreso
--	And idm.NuAsignacion=@NuAsignacion And idm.IDDebitMemo=@IDDebitMemo
--	Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo
--	Inner Join COTICAB cc On dm.IDCab=cc.IDCAB

--If @FecOut<>''
	--If @FecOut=@FecAcred
		--Set @bCredito=1

--Declare @VtaAdic bit=0
--If exists(
--	Select dm.IDDebitMemo From DEBIT_MEMO dm 
--	Inner Join VENTA_ADICIONAL va on dm.IDCab=va.IDCab And dm.IDDebitMemo=@IDDebitMemo 
--	Inner Join VENTA_ADICIONAL_DET vd On va.NuVtaAdi=vd.NuVtaAdi And vd.IDDebitMemo=@IDDebitMemo 
--	)
--	Set @VtaAdic = 1

If @bSeCreoAnticipos=0	
	If Exists(Select dm.IDDebitMemo From DEBIT_MEMO dm Inner Join COTICAB cc 
	On dm.IDDebitMemo=@IDDebitMemo  And cc.IDCAB=dm.IDCab And cc.EstadoFacturac='P')		
		Set @bCredito=1

Select 
	Case When ia.IDTipoDoc='NCR' then 14 else 13 End as InvoiceType, 
	--Case When SUBSTRING(ia.NuDocum,1,3)='005' Then
	--	203
	--Else
	--	Case When ia.IDTipoDoc='NCR' then 14 else 13 End 
	--End as InvoiceType, 

	td.CoSAP as U_SYP_MDTD,
	SUBSTRING(ia.NuDocum,1,3) as U_SYP_MDSD,SUBSTRING(ia.NuDocum,4,10) as U_SYP_MDCD,
	M.CoSAP As DocCur,
	--ABS(dbo.FnCambioMoneda(ia.SsMonto,d.IDMoneda,'SOL',@TCambioIng)) as SumApplied,
	(dbo.FnCambioMoneda(ia.SsMonto,d.IDMoneda,'SOL',@TCambioIng)) as SumApplied,
	--ABS(dbo.FnCambioMoneda(ia.SsMonto,d.IDMoneda,'USD',@TCambioIng)) as AppliedFC
	(dbo.FnCambioMoneda(ia.SsMonto,d.IDMoneda,'USD',@TCambioIng)) as AppliedFC
From ASIGNACION_DOCUMENTO ia Inner Join MATIPODOC td On ia.IDTipoDoc=td.IDtipodoc
	Inner Join DOCUMENTO d On ia.NuDocum=d.NuDocum And ia.IDTipoDoc=d.IDTipoDoc 
	--Inner Join ASIGNACION asi On ia.NuAsignacion=asi.NuAsignacion And asi.SsSaldoMatchDocumento>0
	--Inner Join INGRESO_DEBIT_MEMO idm On idm.NuAsignacion=ia.NuAsignacion And idm.IDDebitMemo=@IDDebitMemo
	LEFT JOIN MAMONEDAS M ON D.IDMoneda=M.IDMoneda
where ia.NuAsignacion=@NuAsignacion And ia.IDDebitMemo=@IDDebitMemo
	And @bCredito=0
	--And (d.FeDocum<=(Select if1.FeAcreditada From INGRESO_FINANZAS if1 
	And (CONVERT(SMALLDATETIME,CONVERT(VARCHAR,d.FeDocum,103))<=(Select CONVERT(SMALLDATETIME,CONVERT(VARCHAR,if1.FeAcreditada,103)) From INGRESO_FINANZAS if1 
						Inner Join INGRESO_DEBIT_MEMO idm1 On if1.NuIngreso=idm1.NuIngreso And 
						idm1.IDDebitMemo=@IDDebitMemo And idm1.NuAsignacion=ia.NuAsignacion)
			Or 	@bSeCreoAnticipos=1	)
