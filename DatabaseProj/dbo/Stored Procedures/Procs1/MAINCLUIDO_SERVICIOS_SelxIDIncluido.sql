﻿Create Procedure dbo.MAINCLUIDO_SERVICIOS_SelxIDIncluido
@IDIncluido int
As
Set NoCount On
Select iss.IDIncluido,iss.IDServicio,s.Descripcion as Servicio,s.IDTipoServ,p.NombreCorto as Proveedor
from MAINCLUIDO_SERVICIOS iss Left Join MASERVICIOS s On iss.IDServicio = s.IDServicio
Left Join MAPROVEEDORES p on s.IDProveedor=p.IDProveedor
Where iss.IDIncluido=@IDIncluido
