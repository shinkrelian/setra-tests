﻿--JRF-20160406- ..If Not Exists(select IDCab from COTICAB_TIPOSCAMBIO ...
CREATE Procedure dbo.COTICAB_TIPOSCAMBIO_Ins
 @IDCab int,
 @CoMoneda char(3),
 @SsTipCam decimal(6,3),
 @UserMod char(4)
As
	Set NoCount On
	If Not Exists(select IDCab from COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=@CoMoneda)
	Begin
		Insert into COTICAB_TIPOSCAMBIO(IDCab,CoMoneda,SsTipCam,UserMod,FecMod)
		Values (@IDCab,@CoMoneda,@SsTipCam,@UserMod,GetDate())
	End
