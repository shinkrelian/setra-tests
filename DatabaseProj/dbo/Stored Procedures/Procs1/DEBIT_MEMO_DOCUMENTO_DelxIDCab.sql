﻿
--HLF-20140807-Updates
CREATE Procedure dbo.DEBIT_MEMO_DOCUMENTO_DelxIDCab  
	@IDCab int  
As  
 Set Nocount ON  
   
	 Delete From DEBIT_MEMO_DOCUMENTO Where IDDebitMemo In   
	  (Select IDDebitMemo From DEBIT_MEMO Where IDCab=@IDCab)  
	    
	Update DEBIT_MEMO Set SsSaldoxFacturar=Total, SsSaldoxFacturarNCA=Total Where IDCab=@IDCab
	Update DOCUMENTO Set SsSaldoCruceDMemo=SsTotalDocumUSD Where IDCab=@IDCab
  

