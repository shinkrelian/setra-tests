﻿
--JRF-20150227-No Considerar el NoShow  
--JRF-20150416-Agregar al NoShow ...and rd.IDServicio_Det=o.IDServicio_Det
--JHD-20150805-(select Top(1) Nombre+TxApellidos from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' 
--and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) as Nombre2,                
-- (select Top(1) '+51 ' + Celular from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad  
--and FlTrabajador_Activo=1 Order by IDUsuario) as Celular2                
--JRF-20151202-Ajustar el cambio de nro De telefonos set. Chile/Buenos Aires
--PPMG-20160125- and FlTrabajador_Activo=1 al select de CUSCO 2
--PPMG-20160126-,'SETOURS LIMA (HEADQUARTERS)' as Proveedor,--UPPER(NombreCorto) as Proveedor,
--PPMG-20160126-, 'emergency@setours.com' as Email,
CREATE Procedure [dbo].[COTIDET_SelOperadoresxIDCab_Rpt]    
@IDCab int                    
As
BEGIN
 Set NoCount On                    
 select Ord, Ciudad, IDProveedor, Proveedor, Telefonos, Email,
 ISNULL(Nombre1,'') AS Nombre1, ISNULL(Celular1,'') AS Celular1, ISNULL(Nombre2,'') AS Nombre2, ISNULL(Celular2,'') AS Celular2
 from(                    
select 1 As Ord,'MAIN OFFICE' as Ciudad,IDProveedor,'SETOURS LIMA (HEADQUARTERS)' as Proveedor,--UPPER(NombreCorto) as Proveedor,                    
 pr.Telefono1  as Telefonos, 'emergency@setours.com' as Email,
 CASE WHEN (select LTRIM(RTRIM(Nombre + IsNull(TxApellidos,''))) from MAUSUARIOS where IDUsuario = (select IDUsuarioOpe from COTICAB where IDCAB = 18734))='' THEN '	'
ELSE (select LTRIM(RTRIM(Nombre + ' ' + IsNull(TxApellidos,''))) from MAUSUARIOS where IDUsuario = (select IDUsuarioOpe from COTICAB where IDCAB = 18734)) END as Nombre1,                
 ISNULL((select '+51 '+ Celular from MAUSUARIOS where IDUsuario = (select IDUsuarioOpe from COTICAB where IDCAB = @IDCab)),'+51 987 52 4620') as Celular1,                
 --(select Top(1) Nombre from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad Order by IDUsuario) as Nombre2,                
 --(select Top(1) '+51 ' + Celular from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad Order by IDUsuario) as Celular2                
 (select Top(1) Nombre+TxApellidos from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) as Nombre2,                
 (select Top(1) '+51 ' + Celular from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad  and FlTrabajador_Activo=1 Order by IDUsuario) as Celular2                
 from MAPROVEEDORES pr                    
 where IDProveedor = '001554'                 
 Union                     
 select distinct 2 as Ord,'CUSCO' as Ciudad,p4.IDProveedor,upper(p4.NombreCorto) as Proveedor, p4.Telefono1 as Telefonos, '' as Email,
 u.Nombre + ' ' +IsNull(u.TxApellidos,'') as Nombre1,'+51 '+u.Celular as Celular1,Null as Nombre2,Null as Celular2          
 from OPERACIONES_DET_DETSERVICIOS odd Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det          
 Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion          
 Left Join MAPROVEEDORES p On odd.IDProveedor_Prg = p.IDProveedor          
 Left Join MAUSUARIOS u On p.IDUsuarioTrasladista = u.IDUsuario          
 Left Join MAPROVEEDORES p4 On o.IDProveedor = p4.IDProveedor          
 where o.IDCab = @IDCab and o.IDProveedor ='000544' and p.IDUsuarioTrasladista is not null           
 and IDProveedor_Prg is not null             
 Union          
 select distinct 2 as Ord,'CUSCO' As Ciudad,'000544' as IDProveedor,'SETOURS CUSCO' as Proveedor,'+51 (84) 225571' as Telefono1, '' as Email,
 Nombre + ' ' +IsNull(TxApellidos,'') as Nombre1,        
 --hlf-20140904-I        
 '+51 '+Celular as Celular1,        
 --hlf-20140904-F        
 Null as Nombre1,Null as Celular1          
 from MAUSUARIOS where Nivel = 'SOP'           
 and IDUbigeo = '000066' and Activo ='A' and FlTrabajador_Activo=1
 Union      
 Select Y.Ord,Y.Ciudad,Y.IDProveedor,Y.Proveedor,Y.Telefonos, Y.Email,Y.Nombre1,Y.Celular1,Y.Nombre2,Y.Celular2
 from (            
 select 3 As Ord,u.Descripcion as Ciudad,o.IDProveedor,UPPER(NombreCorto) as Proveedor,                    
 pr.Telefono1  as Telefonos, '' as Email,
 Case When o.IDProveedor In ('002315','002317') Then (select rtrim(ltrim(Nombre))+' '+rtrim(ltrim(TxApellidos)) from MAUSUARIOS 
	where IDUsuario = Case When o.IDProveedor='002315' then '0130' else '0132' End) 
 else pr.NomContacto End as Nombre1,            
 Case When o.IDProveedor = '002315' Then '+54 11 9 ' else Case When o.IDProveedor='002317' then '+56 9 ' Else '' End End+
 Case When o.IDProveedor In ('002315','002317') Then (select Celular from MAUSUARIOS 
 where IDUsuario = Case When o.IDProveedor='002315' then '0130' else '0132' End) else pr.Celular End as Celular1,               
 Case When o.IDProveedor In ('002315','002317') Then (select Top(1) rtrim(ltrim(Nombre))+' '+rtrim(ltrim(TxApellidos)) from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' 
 and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) Else Null End as Nombre2,            
 Case When o.IDProveedor = '002315' Then '+54 11 9 ' else Case When o.IDProveedor='002317' then '+56 9 ' Else '' End End+
 Case When o.IDProveedor In ('002315','002317') Then 
 (select Top(1) Celular from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) 
 else Null End as Celular2,
  Case When ((pr.IDTipoProv <> '001' and sd.ConAlojamiento=1) Or (pr.IDTipoProv='001' and sd.PlanAlimenticio=0))   
 Then   
   Case When ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0)=  
      (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)) And   
     ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0) > 0 And  
      (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)>0) 
  Then 1 else 0 End  
 Else   
  IsNull((select Top(1) rd.FlServicioNoShow from RESERVAS_DET rd Where rd.IDDET=o.IDDet and rd.IDServicio_Det=o.IDServicio_Det and rd.Anulado=0),0)  
 End as ItemNoShow   
 from COTIDET o Left Join MAPROVEEDORES pr On o.IDProveedor = pr.IDProveedor                
 Left Join MAUBIGEO u On pr.IDCiudad = u.IDubigeo                
 Left Join MASERVICIOS_DET sd On o.IDServicio_Det=sd.IDServicio_Det    
 where o.IDCab = @IDCab and (pr.IDTipoProv = '003' and sd.ConAlojamiento=0)     
 and Not o.IDProveedor in('001554','000544')   
 ) as Y  
  Where Y.ItemNoShow=0  
 ) as X                
 Order by X.Ord       
END
