﻿CREATE Procedure [dbo].[COTIDET_PAX_Sel_MayorPaxOutput]
	@IDDet	int,	
	@pIDPax	int Output	
As
	Set NoCount On
	
	Select @pIDPax=Max(IDPax) From COTIDET_PAX
		WHERE IDDet=@IDDet	

	Set @pIDPax=IsNull(@pIDPax,0)
