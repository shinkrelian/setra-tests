﻿CREATE Procedure [dbo].[MAOPCIONES_Sel_List]
	@Descripcion	varchar(50)
As
	Set NoCount On
	
	Select IDOpc, Nombre, Descripcion From MAOPCIONES          
    Where  (Descripcion Like '%'+@Descripcion+'%' Or rtrim(ltrim(@Descripcion))='')
    Order by IDOpc
