﻿--HLF-20140220-d.SsTotalDocumUSD                 
--Comentando SUBSTRING(d.NuDocum,1,3) as Serie,SUBSTRING(d.NuDocum,4,10) as Numero,                  
--y Agregando d.NuDocum                
--JRF-Se agregaron mas filtro para la consulta. (TipoDocumento,Serie,Estado,FechasEmision)              
--JRF-20140407-Se agrego la columna del NuDocum por Separado            
--JRF-20140410-Nuevo campo FlDocManual.          
--HLF-20140416-Agregando CoEstado, DescEstado        
--'GENERADO' reemplazado por 'PRE-LIQUIDAC.'        
--d.TxTitulo as Referencia,        
--HLF-20140430-Order by NuDocum,       
--Agregando columna NuDocumVinc      
--HLF-20140521-Case When c.IDCliente=cl2.IDCliente Then .... as Cliente    
--HLF-20140605-Agregando % al inicio de los likes y agregando  OR cl2.RazonComercial like '%' +@DescCliente + '%' )     
--JRF-20140918-Agregar las columnas referentes a Total y Sub-Total.  
--JHD-20140919-Agregar la columna Num. de Nota de Venta.  
--HLF-20150206-@CoUbigeo_Oficina char(6)
--HLF-20150416-(Select IDtipodoc From MATIPODOC Where CoUbigeo_Oficina=@CoUbigeo_Oficina Or CoUbigeo_Oficina2=@CoUbigeo_Oficina)
--HLF-20150508-Case When d.IDCliente=@CoClienteVtasAdicAPT Then...
--HLF-20150512- Case When d.IDCliente=@CoClienteVtasAdicAPT Then
--(Select Top 1 IDDebitMemo From VENTA_ADICIONAL_DET Where NuDocum=d.nudocum And IDTipoDoc=d.idtipodoc)
--HLF-20150903-ISNULL(d.CoSAP,'') as CoSAP
--JHD-20151112-case when d.IDTipoDoc='TIP' THEN '' ELSE  SUBSTRING(d.NuDocum,1,3) END as Serie,
--JHD-20151112-case when d.IDTipoDoc='TIP' THEN d.NuDocum ELSE  SUBSTRING(d.NuDocum,4,10) END as Numero,
--JHD-20151112-case when d.IDTipoDoc='TIP' THEN d.NuDocum ELSE  SUBSTRING(d.NuDocum,1,3) +'-'+SUBSTRING(d.NuDocum,4,10) END as NuDocum,   
--HLF-20151207- And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)  
--HLF-20151210-And ((c.FlHistorico=@FlHistorico or c.IDCAB is null) Or @FlHistorico=1)  
--HLF-20151215-or d.FlVtaAdicional=1 
CREATE Procedure [dbo].[DOCUMENTO_Sel_List]                  
 @DescCliente varchar(100),                  
 @Referencia varchar(200),                  
 @IDFile varchar(8),                  
 @TipoDocumento char(3),              
 @Serie varchar(3),              
 @Numero varchar(10),              
 @CoEstado char(2),              
 @FecEmisionInicio smalldatetime,              
 @FecEmisionFinal smalldatetime,
 @CoUbigeo_Oficina char(6),
 @FlHistorico bit=1
As              
 Set NoCount On                  

 Declare @CoClienteVtasAdicAPT char(6)=(Select CoClienteVtasAdicAPT From PARAMETRO)
                
 Select * from (                  
 select d.FeDocum as Fecha,                
 td.Descripcion as TipoDocumento,                  
 Case When d.IDCliente=@CoClienteVtasAdicAPT or d.FlVtaAdicional=1 Then
	'VENTA ADICIONAL'
 Else
	Case when d.FlDocManual = 1 Then 'MANUAL' Else 'PRE-LIQUIDAC.' End  
 End as DocumentoIng,          
 Case d.CoEstado         
 When 'GD' Then 'GENERADO'         
 When 'IM' Then 'IMPRESO'         
 When 'AN' Then 'ANULADO'        
 End as DescEstado,          

 -- SUBSTRING(d.NuDocum,1,3) as Serie,SUBSTRING(d.NuDocum,4,10) as Numero,                  
 case when d.IDTipoDoc='TIP' THEN '' ELSE  SUBSTRING(d.NuDocum,1,3) END as Serie,
 -- SUBSTRING(d.NuDocum,4,10) as Numero,                  
 case when d.IDTipoDoc='TIP' THEN d.NuDocum ELSE  SUBSTRING(d.NuDocum,4,10) END as Numero,

  --SUBSTRING(d.NuDocum,1,3) +'-'+SUBSTRING(d.NuDocum,4,10) as NuDocum,    
 case when d.IDTipoDoc='TIP' THEN d.NuDocum ELSE  SUBSTRING(d.NuDocum,1,3) +'-'+SUBSTRING(d.NuDocum,4,10) END as NuDocum,    
       
  isnull(SUBSTRING(d.NuDocumVinc,1,3) +'-'+SUBSTRING(d.NuDocumVinc,4,10),'') As NuDocumVinc,      
 Case When d.IDCliente=@CoClienteVtasAdicAPT Then
	isnull((Select Top 1 isnull(IDDebitMemo,'') From VENTA_ADICIONAL_DET Where NuDocum=d.nudocum And IDTipoDoc=d.idtipodoc),'')
 Else
	''
 End as DMVentasAdicAPT,
  case when c.IDFile IS null then (d.NuFileLibre) else c.IDFile end as 'IDFile',   
  d.NuNtaVta,
  --clfile.RazonComercial as Cliente,        
  Case When d.IDCliente IS null Then clfile.RazonComercial     
  Else    
   Case When cl2.IDCliente=clfile.IDCliente Then     
  clfile.RazonComercial     
   Else    
  cl2.RazonComercial    
   End    
  End    
  as Cliente,     --cl2.IDCliente as ClienteDoc,clfile.IDCliente as ClienteFile,    
  --c.Titulo as Referencia,        
  d.TxTitulo as Referencia, d.SsTotalDocumUSD-d.SsIGV as SsSubTotal,d.SsIGV,       
  d.SsTotalDocumUSD ,d.NuDocum as NuDocumCompleto,      
  d.IDTipoDoc, d.CoEstado,
  ISNULL(d.CoSAP,'') as CoSAP
 from DOCUMENTO d Left Join COTICAB c On d.IDCab = c.IDCAB                  
 Left Join MACLIENTES clfile On c.IDCliente = clfile.IDCliente                  
 Left Join MACLIENTES cl2 On d.IDCliente = cl2.IDCliente                  
 Left Join MATIPODOC td On d.IDTipoDoc = td.IDtipodoc                   
 Where (ltrim(rtrim(@DescCliente))='' Or clfile.RazonComercial like '%' + @DescCliente + '%' OR cl2.RazonComercial like '%' +@DescCliente + '%' ) and                  
    (ltrim(rtrim(@Referencia))='' Or c.Titulo like '%' +@Referencia + '%') and                  
    (ltrim(rtrim(@IDFile))='' Or c.IDFile like '%' +@IDFile+'%')  and              
    (ltrim(rtrim(@TipoDocumento))='' Or d.IDTipoDoc = @TipoDocumento) and              
    (ltrim(rtrim(@CoEstado))='' Or d.CoEstado = @CoEstado) and              
    ((Convert(Char(10),@FecEmisionInicio,103)='01/01/1900' and Convert(Char(10),@FecEmisionFinal,103)='01/01/1900') Or              
    (CAST(CONVERT(CHAR(10),d.FeDocum,103)AS SMALLDATETIME) between @FecEmisionInicio and @FecEmisionFinal))               
    and (ltrim(rtrim(@Numero))='' Or d.NuDocum like  '%'+ @Numero+'%')      
    and d.IDTipoDoc In 
		--(Select IDtipodoc From MATIPODOC Where CoUbigeo_Oficina=@CoUbigeo_Oficina)
		(Select IDtipodoc From MATIPODOC Where CoUbigeo_Oficina=@CoUbigeo_Oficina Or CoUbigeo_Oficina2=@CoUbigeo_Oficina)
	 And ((c.FlHistorico=@FlHistorico or c.IDCAB is null) Or @FlHistorico=1)  
 ) as X                  
 where (ltrim(rtrim(@Serie)) ='' Or X.Serie like @Serie+'%')       
                  
 Order by NuDocum--Fecha              
  

