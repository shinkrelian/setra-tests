﻿Create Procedure dbo.COTICAB_Sel_FilesxUpdSAPMassive
As
select IDCAB,IDFile as CenterCode,IDFile as CenterName,3 as InWhichDimension,
	   FechaReserva as Effectivefrom,FeAnulacion as EffectiveTo,Case When c.Estado='X' then 0 else 1 End as Active,
	   cl.CardCodeSAP as CardCode,cl.RazonComercial as CardName
from COTICAB c Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente
where FlHistorico=0 And FlFileMigA_SAP=1 And FlUpdateSAPMassive = 0 and IDFile is not null
