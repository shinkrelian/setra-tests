﻿CREATE Procedure [dbo].[MAIDIOMAS_Sel_Cbo]
	@bTodos	bit

As

	Set NoCount On

	SELECT * FROM
	(
	Select '' as IDidioma,'<TODOS>' as DescIdioma, 0 as Ord
	Union	
	
	Select IDidioma,IDidioma as DescIdioma, 1 as Ord From MAIDIOMAS
	Where Activo='A'
	) AS X
	Where (@bTodos=1 Or Ord<>0) 
	Order by Ord,2
