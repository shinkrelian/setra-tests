﻿Create Procedure [dbo].[MAHABITTRIPLE_Sel_Lvw]  
 @IDHabitTriple char(3),  
 @Descripcion varchar(60)  
As  
 Set NoCount On  
   
 Select Descripcion   
 From MAHABITTRIPLE  
 Where (Descripcion Like '%'+@Descripcion+'%')  
 And (IdHabitTriple<>@IDHabitTriple Or ltrim(rtrim(@IDHabitTriple))='')  
 Order by Descripcion  
