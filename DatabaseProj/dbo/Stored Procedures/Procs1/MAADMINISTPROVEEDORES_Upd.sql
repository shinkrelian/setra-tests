﻿--JRF-20130402- CORRECION ERROR  
--JRF-20141112- Nuevo campo =CoCBU  
--JRF-20150120-IVAN varchar(45),DireccionBancoPag varchar(80),NroCuentaBenef varchar(45)
CREATE Procedure [dbo].[MAADMINISTPROVEEDORES_Upd]      
 @IDAdminist char(2),      
 @IDProveedor char(6),      
 @Banco char(3),      
       
 @BancoExtranjero varchar(80) ,      
       
 @CtaCte varchar(30),      
 @IDMoneda char(3),      
 @SWIFT varchar(30),      
 @IVAN varchar(45),      
 @NombresTitular varchar(80),      
 @ApellidosTitular varchar(80),      
 @ReferenciaAdminist text,      
 @CtaInter varchar(30),      
 @BancoInterm char(3),      
       
 @BancoInterExtranjero varchar(80),      
 @TipoCuenta char(2),  
       
 @TitularBenef varchar(90) ,      
 @TipoCuentBenef char(1) ,      
 @NroCuentaBenef varchar(45) ,      
 @DireccionBenef varchar(70) ,      
       
 @NombreBancoPag varchar(80) ,      
 @PECBancoPag varchar(70) ,      
 @DireccionBancoPag varchar(80) ,      
 @SWIFTBancoPag varchar(10) ,      
       
 @EsOpcBancoInte bit,      
 @NombreBancoInte varchar(80) ,      
 @PECBancoInte varchar(90) ,      
 @SWIFTBancoInte varchar(10) ,      
 @CuentBncPagBancoInte varchar(15) ,      
 @CoCBU varchar(30),  
 @UserMod char(4)      
      
As      
 Set NoCount On      
       
 UPDATE MAADMINISTPROVEEDORES Set      
           Banco=Case When RTRIM(ltrim(@Banco))= '' Then Null Else @Banco End,      
                 
           BancoExtranjero = Case When rtrim(ltrim(@BancoExtranjero))='' Then Null Else @BancoExtranjero End,      
                 
           CtaCte=Case When RTRIM(ltrim(@CtaCte ))= '' Then Null Else @CtaCte End,      
           IDMoneda=@IDMoneda,      
           SWIFT=Case When RTRIM(ltrim(@SWIFT))= '' Then Null Else @SWIFT End,      
           IVAN=Case When RTRIM(ltrim(@IVAN))= '' Then Null Else @IVAN End,      
           NombresTitular=Case When RTRIM(ltrim(@NombresTitular))= '' Then Null Else @NombresTitular End,      
           ApellidosTitular=Case When RTRIM(ltrim(@ApellidosTitular))= '' Then Null Else @ApellidosTitular End,      
           ReferenciaAdminist=Case When RTRIM(ltrim(Cast (@ReferenciaAdminist as varchar(max))))= '' Then Null Else @ReferenciaAdminist End,      
           CtaInter=Case When RTRIM(ltrim(@CtaInter))= '' Then Null Else @CtaInter End,      
           BancoInterm=Case When RTRIM(ltrim(@BancoInterm))= '' Then Null Else @BancoInterm End,      
                 
           BancoInterExtranjero =Case When RTRIM(ltrim(@BancoInterExtranjero))= '' Then Null Else @BancoInterExtranjero End,      
           TipoCuenta = @TipoCuenta,      
                 
           TitularBenef =Case when RTRIM(LTRIM(@TitularBenef))='' Then Null Else @TitularBenef End,      
           TipoCuentBenef = Case When RTRIM(ltrim(@TipoCuentBenef))='' Then Null Else @TipoCuentBenef End,      
           NroCuentaBenef = Case When RTRIM(ltrim(@NroCuentaBenef))='' Then Null Else @NroCuentaBenef End,      
           DireccionBenef = Case When RTRIM(ltrim(@DireccionBenef))='' Then Null Else @DireccionBenef End,      
                  
           NombreBancoPag = Case When RTRIM(LTRIM(@NombreBancoPag))='' Then Null Else @NombreBancoPag End,      
           PECBancoPag = Case When RTRIM(ltrim(@PECBancoPag))='' Then Null Else @PECBancoPag End,      
           DireccionBancoPag = Case When RTRIM(ltrim(@DireccionBancoPag))='' Then Null Else @DireccionBancoPag End,      
           SWIFTBancoPag = Case When RTRIM(ltrim(@SWIFTBancoPag))='' Then Null Else @SWIFTBancoPag End,      
                  
           EsOpcBancoInte=@EsOpcBancoInte,      
           NombreBancoInte = Case When RTRIM(ltrim(@NombreBancoInte))='' Then Null Else @NombreBancoInte End,      
           PECBancoInte = Case When RTRIM(ltrim(@PECBancoInte))='' Then Null Else @PECBancoInte End,      
           SWIFTBancoInte = Case When RTRIM(ltrim(@SWIFTBancoInte))='' Then Null Else @SWIFTBancoInte End,      
           CuentBncPagBancoInte = Case When RTRIM(ltrim(@CuentBncPagBancoInte))='' Then Null Else @CuentBncPagBancoInte End,      
     CoCBU= Case When rtrim(ltrim(@CoCBU))='' Then Null Else @CoCBU End,  
       UserMod=@UserMod      
     WHERE      
           IDAdminist=@IDAdminist And IDProveedor=@IDProveedor      
