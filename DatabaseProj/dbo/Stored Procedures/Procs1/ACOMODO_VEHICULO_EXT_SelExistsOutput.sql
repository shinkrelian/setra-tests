﻿
Create Procedure dbo.ACOMODO_VEHICULO_EXT_SelExistsOutput
	@IDDet int,
	@pExists bit Output
As
	Set Nocount on
	Set @pExists = 0

	If Exists(Select IDDet From ACOMODO_VEHICULO_EXT
		Where IDDet=@IDDet)
		Set @pExists = 1
