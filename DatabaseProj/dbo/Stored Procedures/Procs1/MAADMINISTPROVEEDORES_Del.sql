﻿--JRF-2013.04.02- CORRECION ERROR
CREATE Procedure [dbo].[MAADMINISTPROVEEDORES_Del]  
 @IDAdminist char(2),  
 @IDProveedor char(6)  
As  
 Set NoCount On  
   
 DELETE FROM MAADMINISTPROVEEDORES   
    WHERE IDAdminist=@IDAdminist And IDProveedor=@IDProveedor  
