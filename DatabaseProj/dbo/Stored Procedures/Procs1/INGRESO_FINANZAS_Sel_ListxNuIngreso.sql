﻿--JRF-20131127-Cambio de Columna FeDeposito x FeAcreditada  
--JRF-20140115-Considerar el valor de cambio
CREATE Procedure dbo.INGRESO_FINANZAS_Sel_ListxNuIngreso    
@NuIngreso int    
As    
Begin    
 Set NoCount On    
 Select CONVERT(char(10),FeAcreditada,103) as FeAcreditada,ISNULL(ifn.ssTC,0.00) as ssTC,mo.Descripcion as DescMoneda,
 cl.RazonComercial as DescCliente,    
 Isnull(b.Descripcion,'') as Banco,    
 Isnull(ifn.IDBanco,'000') IDBanco,  
 Isnull(ifn.CoOperacionBan,'') as CoOperacionBan,ISNULL(ifn.TxObservacion,'') as Detalle,    
 Isnull(ifn.SsOrdenado,0) as ImpteOrdenado,SsRecibido,    
 IsNull(ifn.SsGastosTransferencia,0) as SsGastosTransferencia,SsBanco,SsNeto,Isnull(ifn.IDCliente,'') as IDCliente,    
 Isnull((select SUM(idm.SsPago+idm.SsGastosTransferencia) from INGRESO_DEBIT_MEMO idm where idm.NuIngreso= ifn.NuIngreso),0) as MontoAsignado,    
 ifn.CoEstado  ,ifn.IDMoneda,ifn.CoEstado  
 from INGRESO_FINANZAS ifn Left Join MABANCOS b On ifn.IDBanco = b.IDBanco    
 Left Join MACLIENTES cl On ifn.IDCliente = cl.IDCliente    
 Left Join MAMONEDAS mo On ifn.IDMoneda = mo.IDMoneda
 Where ifn.NuIngreso = @NuIngreso    
End    
