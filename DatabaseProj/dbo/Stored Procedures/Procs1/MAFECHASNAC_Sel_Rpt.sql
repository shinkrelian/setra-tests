﻿create Procedure [dbo].[MAFECHASNAC_Sel_Rpt]
	@Fecha smalldatetime,
	@IDUbigeo char(6)
As
	Set NoCount On
	
	Select Fecha, Descripcion
	From MAFECHASNAC
    Where (Fecha=@Fecha Or @Fecha='01/01/1900') And 
    (IDUbigeo=@IDUbigeo)
