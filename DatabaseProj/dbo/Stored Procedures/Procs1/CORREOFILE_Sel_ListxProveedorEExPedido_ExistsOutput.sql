﻿

CREATE Procedure [dbo].[CORREOFILE_Sel_ListxProveedorEExPedido_ExistsOutput]
	@NuPedInt	int,
	@CoUsrPrvPara	char(6),
	@CoArea char(2),
	@pExists bit output
As
	Set Nocount on

	Set @pExists = 0

	If Exists(Select 
		c.NuCorreo
		From .BDCORREOSETRA..CORREOFILE c
		Where IDCab in (Select IDCab From COTICAB where nupedint=@nupedint) And   
		(@CoUsrPrvPara In (Select CoUsrPrvPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE Where NuCorreo=c.NuCorreo))
		And	CoTipoBandeja='EE'
		And (c.CoUsrIntDe in (Select IDUsuario From MAUSUARIOS Where IdArea=@CoArea) ))

		Set @pExists = 1

