﻿--HLF-20151103-@IDServicio_Det_V_Cot int
--IDServicio=(select IDServicio from MASERVICIOS_DET ...
--HLF-20160114-And QtUtilizada<0
CREATE Procedure dbo.COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS_Del
@IDCab int,
@IDServicio_Det_V_Cot int
As
Set NoCount On
Delete from COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS
where IDCab=@IDCab And --IDServicio_Det=@IDServicio_Det
IDServicio = (select IDServicio from MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det_V_Cot)
And QtUtilizada<0

