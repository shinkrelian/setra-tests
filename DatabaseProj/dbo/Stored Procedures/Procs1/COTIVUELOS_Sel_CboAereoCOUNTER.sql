﻿CREATE Procedure [dbo].[COTIVUELOS_Sel_CboAereoCOUNTER]             
 @bTodos bit,             
 @CoIATA char(2),  
 @bTodosDesc bit  
As            
            
 Set NoCount On            
       
 SELECT * FROM            
 (            
 Select '' as IDProveedor,CASE WHEN @bTodosDesc = 0 THEN 'NO APLICA                                                                |'   
 ELSE '<TODOS>                                                                |'  END as RazonSocial, 0 as Ord            
 Union            
        
 Select      
 IDProveedor,NombreCorto + '                                                                |'+ Isnull(CoIATA,'') As RazonSocial        
 ,1 from MAPROVEEDORES            
 where 
 --(LTRIM(RTRIM(@CoIATA))='' Or ISNULL(CoIATA,'') 
	--Like '%'+rtrim(ltrim(@CoIATA))+'%')             

 (LTRIM(RTRIM(@CoIATA))='' Or CoIATA
	Like '%'+rtrim(ltrim(@CoIATA))+'%')             

 --And ltrim(rtrim(ISNULL(CoIATA,''))) <>''  and Activo = 'A'      
 And ltrim(rtrim(CoIATA)) <>''  and Activo = 'A'      
 ) as X            
 Where (@bTodos=1 Or Ord<>0)            
 Order by Ord,2        
 
