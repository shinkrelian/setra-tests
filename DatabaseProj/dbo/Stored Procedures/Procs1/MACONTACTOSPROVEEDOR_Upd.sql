﻿
--JHD-20151124-Se agregaron nuevos campos: @Usuario varchar(20)='',@Password varchar(255)=''
--PPMG-20151125-Se valido que no se duplique el usuario.
--JHD-20151126-Se agrego nuevo campo: @FlAccesoWeb bit=0
CREATE Procedure [dbo].[MACONTACTOSPROVEEDOR_Upd]
	@IDContacto char(3),
	@IDProveedor char(6),
	
	@IDIdentidad char(3),
	@NumIdentidad varchar(15),
	@Titulo varchar(5),
	@Nombres varchar(80),
	@Apellidos varchar(80),
	@Cargo varchar(100),
	@Utilidad	char(3),
	@Telefono varchar(80),
	@Fax varchar(50),
	@Celular varchar(60),
	@Email varchar(100),
	@Anexo varchar(10),
	@FechaNacimiento	smalldatetime,
	@UserMod char(4),
	@Usuario varchar(80)='',
	@Password varchar(255)='',
	@FlAccesoWeb bit=0
AS
BEGIN
	SET @Usuario = ltrim(rtrim(@Usuario))
	IF @Usuario <> ''
	BEGIN
		IF [dbo].[FnExisteUsuarioProvClie](@IDContacto, @IDProveedor, 'P', @Usuario)=1
		BEGIN
			RAISERROR ('Ya esiste el nombre del usuario a ingresar.', 16, 1);
			RETURN
		END
	END
	Set NoCount On
	
	UPDATE MACONTACTOSPROVEEDOR Set           
           IDIdentidad=@IDIdentidad
           ,NumIdentidad=@NumIdentidad
           ,Titulo=@Titulo
           ,Nombres=@Nombres
           ,Apellidos=@Apellidos
           ,Cargo=@Cargo
           ,Utilidad=@Utilidad
           ,Telefono=Case When ltrim(rtrim(@Telefono))='' Then Null Else @Telefono End
           ,Fax=Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End
           ,Celular=Case When ltrim(rtrim(@Celular))='' Then Null Else @Celular End
           ,Email=Case When ltrim(rtrim(@Email))='' Then Null Else @Email End
           ,Anexo=Case When ltrim(rtrim(@Anexo))='' Then Null Else @Anexo End
           ,FechaNacimiento=Case When @FechaNacimiento='01/01/1900' Then Null Else @FechaNacimiento End
           ,UserMod=@UserMod           
           ,FecMod=GETDATE()
		   ,Usuario=Case When ltrim(rtrim(@Usuario))='' Then Null Else @Usuario End
		   ,[Password]=Case When ltrim(rtrim(@Password))='' Then Null Else @Password End
		   ,FlAccesoWeb=@FlAccesoWeb
     WHERE
           IDContacto=@IDContacto And IDProveedor=@IDProveedor
END;
