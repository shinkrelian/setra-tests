﻿
CREATE PROCEDURE [dbo].[MACONTACTOSPROVCLIE_Exists_UsuarioExtranet]
						@IDContacto char(3),
						@IDTablaParent char(6),
						@TipoTablaParent char(1),
						@CodUsuario varchar(20),
						@pExists bit Output
AS
BEGIN
	Set NoCount On
	Set @pExists = [dbo].[FnExisteUsuarioProvClie](@IDContacto, @IDTablaParent, @TipoTablaParent, @CodUsuario)
END
