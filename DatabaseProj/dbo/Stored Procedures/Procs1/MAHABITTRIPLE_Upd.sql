﻿
CREATE Procedure MAHABITTRIPLE_Upd
@IdHabitTriple char(3),
@Abreviacion char(4),
@Descripcion varchar(50),
@UserMod char(4)
As

	Set NoCount On
	
	Update MAHABITTRIPLE
		set Abreviacion = @Abreviacion,
			Descripcion = @Descripcion,
			UserMod = @UserMod,
			FecMod = GETDATE()
		Where IdHabitTriple = @IdHabitTriple

