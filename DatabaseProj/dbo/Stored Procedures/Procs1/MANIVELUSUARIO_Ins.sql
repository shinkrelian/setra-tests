﻿Create Procedure [dbo].[MANIVELUSUARIO_Ins]
	@IDNivel char(3),
    @Descripcion varchar(50),
    @UserMod char(4)
As
	Set NoCount On
	
	INSERT INTO MANIVELUSUARIO
           ([IDNivel]
           ,[Descripcion]
           ,[UserMod])
     VALUES
           (@IDNivel,
            @Descripcion,
            @UserMod)
