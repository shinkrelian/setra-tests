﻿Create Procedure dbo.COTIDET_SelServiciosxAcomodoEspecialGroup
@IDCab int
As
Select *,  
dbo.FnAcomodosEspecialesxIDDetxCapacidadxMat(X.IDDET,X.QtCapacidad,X.EsMatrimonial) as AcomodoTituloxCapacidad  
from (  
Select cd.IDDet,ch.QtCapacidad,Case When Charindex('(MAT)',ch.NoCapacidad)> 0 Then 1 Else 0 End As EsMatrimonial,  
Convert(char(10),cd.Dia,103) as FechaIN,ch.NoCapacidad,ca.QtPax,  
dbo.FnAcomodosEspecialesxIDDet(cd.IDDET) as AcomodoTitulo ,cd.IDProveedor 
--case when (select COUNT(*) from COTIDET_ACOMODOESPECIAL_DISTRIBUC cad Where cad.IDDet = cd.IDDET) > 0   
--then 1 Else 0 End as ExisteAcomodoxPax  
from COTIDET cd --Left Join RESERVAS_DET rd On cd.IDDET = rd.IDDet  
Left Join COTIDET_ACOMODOESPECIAL ca On cd.IDDET = ca.IDDet  
Left Join CAPACIDAD_HABITAC ch On ca.CoCapacidad = ch.CoCapacidad  
Where AcomodoEspecial = 1 and IDCAB = @IDCab) as X  
Order by CAST(X.FechaIN as smalldatetime)  
