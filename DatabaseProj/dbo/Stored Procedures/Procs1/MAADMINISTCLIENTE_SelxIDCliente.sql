﻿CREATE Procedure [dbo].[MAADMINISTCLIENTE_SelxIDCliente]
	@IDCliente char(6)
As
	Set NoCount On
	
	Select IDAdminist, Banco, CtaCte, ma.IDMoneda,--*/ mo.Descripcion as IDMoneda,
	SWIFT, IVAN, ApellidosTitular, NombresTitular,
	ReferenciaAdminist, CtaInter, BancoInterm,'' as btnDel
	From MAADMINISTCLIENTE ma Left Join MAMONEDAS mo On ma.IDMoneda=mo.IDMoneda
	Where ma.IDCliente=@IDCliente
