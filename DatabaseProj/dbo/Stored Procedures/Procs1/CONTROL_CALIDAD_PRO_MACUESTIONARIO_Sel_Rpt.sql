﻿--JRF-20150302-Agregar el filtro por File (@IDCab)  
--JRF-20150626-And (Ltrim(Rtrim(@IDCliente))='' Or (c.IDCliente=@IDCliente or ccp.CoCliente=@IDCliente))    
--JRF-20150626-isnull(cl.RazonComercial,(select ISNULL(RazonComercial,'') from maclientes where idcliente=ccp.coCliente)) as DescCliente,
--JHD-20160321-case when c.idfile is null then ( case when Cast(IsNull(ccpc.TxComentario,'') as varchar(Max))='' then Cast(IsNull(ccp.TxComentario,'') as varchar(Max)) else  Cast(IsNull(ccpc.TxComentario,'') as varchar(Max)) end) else Cast(IsNull(ccpc.TxComentario,'') as varchar(Max)) end as Comentario,
CREATE Procedure dbo.CONTROL_CALIDAD_PRO_MACUESTIONARIO_Sel_Rpt    
@IDCliente char(6),    
@IDProveedor char(6),    
@FecIngRango1 smalldatetime,    
@FecIngRango2 smalldatetime,    
@RatingDe tinyint,    
@RatingHasta tinyint,  
@IDCab int    
As    
Set NoCount On    
if @RatingHasta = 0    
 Set @RatingHasta = 5    

Select ROW_NUMBER()Over(Partition by Y.IDFile,Y.ApellidoPax Order By Y.FecInicio) As Item,
Y.* 
from (
Select distinct
X.FecInicio,
X.IDFile,
X.DescProveedor,
X.DescCliente,X.Titulo,X.ApellidoPax,X.EjecutivaVentas,X.EjecutivaOper,X.RoomNights,    
    dbo.FnDevuelveValorCadenaCalificacionControlCalidad(X.ValorCuestionario) As Rating,X.Comentario,    
    X.CantExcelent,X.CantVeryGood,X.CantGood,X.CantAverage,X.CantPoor ,X.CantServiciosResv   
from (    
select c.FecInicio,c.IDFile,    
    p.NombreCorto as DescProveedor,c.Titulo,IsNull(cp.Apellidos,'') as ApellidoPax, uVent.Siglas as EjecutivaVentas,    
    uOper.Siglas as EjecutivaOper,    
   -- Case When p.IDTipoProv='001' Or p.IDTipoProv='003' Then     
   IsNull((select COUNT(distinct Dia) from COTIDET c2 LEFT Join MASERVICIOS_DET sd On c2.IDServicio_Det= sd.IDServicio_Det
    Left Join MAPROVEEDORES p4 On c2.IDProveedor=p4.IDProveedor
    where ((p4.IDTipoProv='001' and sd.PlanAlimenticio=0) Or(p4.IDTipoProv<>'001' and sd.ConAlojamiento=1)) 
		  And c2.IDCAB=ccp.IDCAB and c2.IDProveedor=ccpc.IDProveedor),0)
    as RoomNights,    
    dbo.FnValorCalificacionControlCalidad(ccpc.NuControlCalidad,ccpc.NuCuest,ccpc.IDProveedor) as ValorCuestionario,    
   
   -- Cast(IsNull(ccpc.TxComentario,'') as varchar(Max)) as Comentario,
	 case when c.idfile is null then ( case when Cast(IsNull(ccpc.TxComentario,'') as varchar(Max))='' then Cast(IsNull(ccp.TxComentario,'') as varchar(Max)) else  Cast(IsNull(ccpc.TxComentario,'') as varchar(Max)) end) else
	 Cast(IsNull(ccpc.TxComentario,'') as varchar(Max)) end as Comentario,
	
	isnull(cl.RazonComercial,(select ISNULL(RazonComercial,'') from maclientes where idcliente=ccp.coCliente)) as DescCliente,    
    Case When dbo.FnValorCalificacionControlCalidad(ccpc.NuControlCalidad,ccpc.NuCuest,ccpc.IDProveedor)=5 then 1 else 0 End as CantExcelent,    
    Case When dbo.FnValorCalificacionControlCalidad(ccpc.NuControlCalidad,ccpc.NuCuest,ccpc.IDProveedor)=4 then 1 else 0 End as CantVeryGood,    
    Case When dbo.FnValorCalificacionControlCalidad(ccpc.NuControlCalidad,ccpc.NuCuest,ccpc.IDProveedor)=3 then 1 else 0 End as CantGood,    
    Case When dbo.FnValorCalificacionControlCalidad(ccpc.NuControlCalidad,ccpc.NuCuest,ccpc.IDProveedor)=2 then 1 else 0 End as CantAverage,    
    Case When dbo.FnValorCalificacionControlCalidad(ccpc.NuControlCalidad,ccpc.NuCuest,ccpc.IDProveedor)=1 then 1 else 0 End as CantPoor,
    (select COUNT(*) from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva
     Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
     Left Join MAPROVEEDORES p5 On r.IDProveedor=p5.IDProveedor
     Where r.Anulado=0 and rd.Anulado=0 and r.IDProveedor=ccpc.IDProveedor and r.IDCab=ccp.IDCab and r.Estado<>'XL'
     And ((p5.IDTipoProv='001' And sd.PlanAlimenticio=1) Or(p5.IDTipoProv<>'001' and sd.ConAlojamiento=0))
    ) as CantServiciosResv
from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc     
Left Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad    
Left Join COTICAB c On ccp.IDCab=c.IDCAB    
Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente    
Left Join MAPROVEEDORES p On ccpc.IDProveedor=p.IDProveedor    
Left Join COTIPAX cp On ccp.IDPax=cp.IDPax    
Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario     
Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario    
Where Not ccpc.NuCuest In(1,2,3,4)     
      and(CAST(ccpc.FlExcelent as tinyint)+Cast(ccpc.FlVeryGood as tinyint)+Cast(ccpc.FlGood as tinyint)+    
          Cast(ccpc.FlAverage as tinyint)+Cast(ccpc.FlPoor as tinyint) >0)    
      and(CONVERT(Char(10),@FecIngRango1,103)='01/01/1900' Or     
    c.FecInicio between @FecIngRango1 and @FecIngRango2)    
   And (Ltrim(Rtrim(@IDCliente))='' Or (c.IDCliente=@IDCliente or ccp.CoCliente=@IDCliente))    
   And (LTRIM(rtrim(@IDProveedor))='' Or ccpc.IDProveedor=@IDProveedor)    
   And (@IDCab=0 Or c.IDCAB=@IDCab)  
) as X    
Where ((@RatingDe=0 and @RatingHasta=0) Or X.ValorCuestionario between @RatingDe and @RatingHasta)    
) As Y
Order By Y.FecInicio    
