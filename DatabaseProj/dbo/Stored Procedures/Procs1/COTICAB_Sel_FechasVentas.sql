﻿
CREATE Procedure dbo.COTICAB_Sel_FechasVentas

As

Set NoCount On

select '' as ID,'<TODOS>' as Anio
Union
select distinct cast(Year(FecInicio) as nvarchar(4)) As ID, cast(Year(FecInicio) as nvarchar(4)) as Anio from COTICAB

Where Year(FecInicio)>2013

Order by 1

