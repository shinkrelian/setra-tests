﻿
--HLF-20150223-@FlDesactNoShow bit
CREATE PROCEDURE [dbo].[COTIDET_PAX_Sel_xIDDet_Lvw]

	@IDDet	int,
	@FlDesactNoShow bit=0
As

	Set NoCount On	
	
		Select c.IDPax
	From COTIDET_PAX d Inner Join COTIPAX c On c.IDPax=d.IDPax And d.IDDet=@IDDet
	Where d.FlDesactNoShow = @FlDesactNoShow 
	Order by c.IDPax

