﻿CREATE PROCEDURE [dbo].[COTIPAX_Sel_CONSETTUR_Distintos]
	@IDCab int
AS
BEGIN
	Set Nocount On

	Declare @IDServicioMPWP char(8)	

	Set @IDServicioMPWP='CUZ00132'	--'MAPI'

	select  Apellidos,Nombre, TipoDoc,NroDoc, IDPais,Sexo,Edad, Categoria,     
		Tipo, IDDET, IDPax
	 from (

	select distinct * from (

		Select x.orden, X.Apellidos,X.Nombre, X.TipoDoc, X.NroDoc, X.IDPais, X.Sexo, X.Edad, X.Categoria,     
		case When Edad < 18   THEN '2' Else '1' End as Tipo, IDDET=0, IDPax
		From(        
		select Apellidos,Nombres as Nombre,

		case when ti.IDIdentidad In ('004','003','002') then '1' else ti.Descripcion End as TipoDoc,         
		cp.NumIdentidad NroDoc,ISNULL(u.CodigoINC,'') as IDPais        
		, case upper(cp.Titulo) When 'MR.' then 'M' When 'MS.' Then 'F' End as Sexo,        
		ISNULL(DATEDIFF(YEAR,FecNacimiento,getdate()),0) as Edad,        
		case CP.IDIdentidad
		when '004' Then 1    --PASAPORTE
		When '002' then 2    --DNI
		When '003' then 4    --CARNET EXTRANJERIA
		When '006' then 6      --CARNET UNIVERSITARIO
		End as Categoria ,Orden, cd.IDDet, CP.IDPax
		from COTIPAX cp Left Join MATIPOIDENT ti On cp.IDIdentidad= ti.IDIdentidad           
		Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo           
		Inner Join COTIDET_PAX cdp On cdp.IDPax=cp.IDPax 
		Inner Join COTIDET cd On cdp.IDDet=cd.IDDet 
		And dbo.FnServicioMPWP(cd.IDServicio_Det) IN(@IDServicioMPWP,'CUZ00133','CUZ00905')
		and (cp.Titulo <>'Chd.' or (cp.Titulo ='Chd.' and cp.FlAdultoINC=1))
		where cp.IDCab=@IDCab        

		) as X        


	) as y
	) as z

	Order by z.Orden 
END

