﻿
--HLF-20121006-Agregando cc.TipoCambio as TipoCambioCotiCab        
--HLF-20121210-Agregando cd.Especial, cd.CostoReal, cd.MargenAplicado        
--HLF-20121214-Agregando isnull(cd.CostoRealAnt,0) as CostoRealAnt, isnull(cd.MargenAplicadoAnt,0) as MargenAplicadoAnt        
--HLF-20130121-Agregando isnull(cd.IDDetRel,0) as IDDetRel        
--HLF-20130123-Agregando cd.IncGuia        
--HLF-20130125-Agregando cd.CostoRealEditado, cd.MargenAplicadoEditado      
--HLF-20130916-Agregando cd.Dia    
--HLF-20140718-Cambios para columna TipoCambioCotiCab  
--HLF-20140828-Agregando cd.FlSSCREditado, cd.FlSTCREditado, cd.SSCR, cd.STCR
--HLF-20150601-Case When Not Exists(Select IDDet From ACOMODO_VEHICULO Where IDDet=cd.IDDET)...
CREATE Procedure dbo.COTIDET_Sel_xIDCab_Q        
 @IDCab_Q int        
As        
 Set Nocount on        
         
 Select p.IDTipoProv, cd.IncGuia, IsNull(cc.Margen,0) as MargenCotiCab,        
 cd.IDDET, cdq.IDDET_Q, cd.IDServicio, cd.IDServicio_Det,        
 sd.Tipo, sd.Anio,   
 --IsNull(cc.TipoCambio,0) as TipoCambioCotiCab,         
 isnull(ctc.SsTipCam,0)  as TipoCambioCotiCab,   
 cd.Especial, cd.CostoReal, cd.MargenAplicado,         
 isnull(cd.CostoRealAnt,0) as CostoRealAnt,         
 isnull(cd.MargenAplicadoAnt,0) as MargenAplicadoAnt,        
 isnull(cd.IDDetRel,0) as IDDetRel, cd.IncGuia,      
 cd.CostoRealEditado, cd.MargenAplicadoEditado, cd.Dia,
 cd.FlSSCREditado, cd.FlSTCREditado, cd.SSCR, cd.STCR,
 Case When Not Exists(Select IDDet From ACOMODO_VEHICULO Where IDDet=cd.IDDET)
	And Not Exists(Select IDDet From ACOMODO_VEHICULO_EXT Where IDDet=cd.IDDET) Then 0 Else 1 
	End FlAcomodosVehic
 From COTIDET cd Inner Join COTIDET_QUIEBRES cdq On cd.IDDET=cdq.IDDET         
 And cdq.IDCAB_Q=@IDCab_Q        
 Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det        
 Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor        
 Left Join COTICAB cc On cc.IDCAB=cd.IDCAB        
 Left Join COTICAB_TIPOSCAMBIO ctc On sd.CoMoneda=ctc.CoMoneda and ctc.IDCab=cd.IDCAB                       
   

