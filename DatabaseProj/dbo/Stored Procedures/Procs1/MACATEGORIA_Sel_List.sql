﻿CREATE Procedure [dbo].[MACATEGORIA_Sel_List]
	@IDCat char(3),
	@Descripcion varchar(10)
As	
	Set NoCount On

	Select IdCat,Descripcion 
	From MACATEGORIA Where (Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='') and (IDCat=@IDCat Or LTRIM(RTRIM(@IDCat))='')
