﻿Create Procedure dbo.COTICAB_COSTOSTC_Ins
@IDCab int,
@CoTipoHon char(3),
@QtDesde smallint,
@QtHasta smallint,
@SsPrecio numeric(8,2),
@UserMod char(4)
As
Set NoCount On
Declare @NuCosto int = IsNull((select MAX(NuCosto) from COTICAB_COSTOSTC where IDCab=@IDCab),0)+1
INSERT INTO [dbo].[COTICAB_COSTOSTC]
           ([NuCosto]
           ,[CoTipoHon]
           ,[IDCab]
           ,[QtDesde]
           ,[QtHasta]
           ,[SsPrecio]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@NuCosto
           ,@CoTipoHon
           ,@IDCab
           ,@QtDesde
           ,@QtHasta
           ,@SsPrecio
           ,@UserMod
           ,GetDate())
