﻿Create Procedure dbo.COTIVUELOS_Sel_ListxIDProvOperxIDcab
@IDCab int,
@IDProveedor char(6)
As
Set NoCount On 
Select c.ID as IDTransporte,(select COUNT(*) from COTITRANSP_PAX cp where cp.IDTransporte=c.ID) as CantidadPax,
	   'Ticket Aereo '+ Ruta as Servicio,'USD' as Moneda,'US$' as SimboloMoneda,
	   IsNull(CostoOperador,0)*(select COUNT(*) from COTITRANSP_PAX cp where cp.IDTransporte=c.ID) as CostoOperador
from COTIVUELOS c
where IDCAB=@IDCab and IDProveedorOperador=@IDProveedor and TipoTransporte='V' and EmitidoOperador=1
