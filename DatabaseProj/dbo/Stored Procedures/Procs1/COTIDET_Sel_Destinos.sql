﻿Create Procedure [dbo].[COTIDET_Sel_Destinos]
	@IDCab	int
As
	Set Nocount On
	
	Select u.Descripcion as Destino
	From COTIDET c Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo
	Where IDCAB=@IDCab
	order by c.Dia
