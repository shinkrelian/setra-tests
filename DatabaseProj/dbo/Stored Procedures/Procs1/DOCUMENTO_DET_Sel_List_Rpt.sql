﻿
--JRF-20140422-No se muestran los detalles                               
--JRF-20140424-Se muestran los Textos de la  nueva tabla                            
--HLF-20140430-Agregando u.Descripcion as DescPais                          
--HLF-20140507-Agregando d.NuDocumVinc, d.IDTipoDocVinc                        
--JRF-20140512-Agregando el IGV                      
--JRF-20140516-Agregar la Fecha de Emisión                  
--HLF-20140520-Agregar la Fecha de Emisión de Doc. Vinculado                    
--HLF-20140521-Case When d.IDCliente IS null Then ....                
--HLF-20140602-Cambiar --dvinc.FeDocum por d.FeEmisionVinc as  FeDocumVinc              
--JRF-20140709-Agregar el Nro. de Identidad.           
--HLF-20140714-quitar el isnull a clfile.NumIdentidad          
--JRF-20140821-Agregar 'VIAJE DE INSPECCIÓN' solo cuando el file lo indique.      
--HLF-20140903-,mon.Descripcion as DescMoneda
--JRF-20140911-,mon.Simbolo as SimboloMoneda
--HLF-20150416-Case When ltrim(rtrim(@CoClienteVtasAdicAPT))='' Then...
--JRF-20150612-...Or Ltrim(rtrim(@NoPaxTitular)....
--HLF-20160311-d.CoEstado
CREATE Procedure dbo.DOCUMENTO_DET_Sel_List_Rpt                                    
	@NuDocum char(10),                                      
	@IDTipoDoc char(3)                                      
As                                      
	Set NoCount On                            
	Declare @CoClienteVtasAdicAPT char(6)='', @NoPaxTitular varchar(100)='', @NuIdentidad varchar(15)='', @NoPais varchar(60)=''

	If Left(@NuDocum,3)='002' And @IDTipoDoc='BLE'
		Begin
		Select @CoClienteVtasAdicAPT=isnull(CoClienteVtasAdicAPT,'') From PARAMETRO
		Declare @siCantPax smallint=(Select COUNT(*) From VENTA_ADICIONAL_DET Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc)
		If @siCantPax=1
			Select @NoPaxTitular=Apellidos+' '+Nombres, @NuIdentidad=NumIdentidad,
				@NoPais=isnull((Select Descripcion From MAUBIGEO Where IDubigeo=cp.IDPaisResidencia),'') 
				From COTIPAX cp Where IDPax In(Select NuPax From VENTA_ADICIONAL_DET Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc)
		Else
			Select @NoPaxTitular=Apellidos+' '+Nombres, @NuIdentidad=NumIdentidad,
				@NoPais=isnull((Select Descripcion From MAUBIGEO Where IDubigeo=cp.IDPaisResidencia),'') 
				From COTIPAX cp Where IDPax In(Select NuPax From VENTA_ADICIONAL_DET Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc And FlTitular=1)
        End
	--select @NoPaxTitular as Test

	Select                 
	 --cl.RazonComercial as DescCliente,                                 
	 Case When ltrim(rtrim(@CoClienteVtasAdicAPT))='' Or Ltrim(rtrim(@NoPaxTitular)) = ''Then
		Case When d.IDCliente IS null Then clfile.RazonComercial                 
		Else                
		Case When cl2.IDCliente=clfile.IDCliente Then                 
		clfile.RazonComercial                 
		Else                
		cl2.RazonComercial                
		End                
		End  
	  Else
		@NoPaxTitular
	  End as DescCliente,                
                 
	 --clfile.Direccion as DireccionCLiente,                  
	 Case When ltrim(rtrim(@CoClienteVtasAdicAPT))='' Or Ltrim(rtrim(@NoPais))='' Then
		 Case When d.IDCliente IS null Then clfile.Direccion                
		  Else                
		   Case When cl2.IDCliente=clfile.IDCliente Then                 
		  clfile.Direccion                
		   Else                
		  cl2.Direccion                
		   End                
		  End  
	  Else
			@NoPais
	  End  as DireccionCLiente,                
                
                 
	 Isnull(c.IDFile,'') as IDFile ,                              
	 d.SsTotalDocumUSD,                                
	 SUBSTRING(d.NuDocum,1,3) as Serie,d.TxTitulo ,                              
	 CONVERT(char(10),c.FecInicio,103) as FechaInicio, CONVERT(char(10),c.FechaOut,103) as FechaOut  ,                            
	 --ddt.NoTexto,ddt.SsTotal,                          
	 dbo.FnTextosFacturacion(d.NuDocum,d.IDTipoDoc)       
	 + Case When c.CoTipoVenta = 'VC' Or c.CoTipoVenta = 'VS' Then CHAR(13)+'VIAJE DE INSPECCIÓN' Else '' End as NoTexto,d.SsTotalDocumUSD as SSTotal,        
	   --u.Descripcion as DescPais ,                        
	 Case When d.IDCliente IS null Then u.Descripcion                 
	  Else                
	   Case When cl2.IDCliente=clfile.IDCliente Then                 
	  u.Descripcion                 
	   Else                
	  u2.Descripcion                 
	   End                
	  End  as DescPais,                
                   
                   
	   d.NuDocumVinc, d.IDTipoDocVinc ,d.SsIGV,                        
	   Case when d.SsTotalDocumUSD<0 then d.SsTotalDocumUSD*-1 else d.SsTotalDocumUSD End                        
	   -Case when d.SsIGV<0 then d.SsIGV*-1 else d.SsIGV End as PreTotal,                   
	   d.FeDocum,               
	   --dvinc.FeDocum as  FeDocumVinc                    
	   d.FeEmisionVinc as  FeDocumVinc  ,            
	   --Isnull(clfile.NumIdentidad,'') as NumIdentidad          
	   Case When ltrim(rtrim(@CoClienteVtasAdicAPT))='' Or Ltrim(rtrim(@NuIdentidad)) ='' Then	     
		clfile.NumIdentidad          
	   Else
		@NuIdentidad
	   End as NumIdentidad
	   ,Case When mon.IDMoneda='SOL' then 'SOLES' else mon.Descripcion End as DescMoneda ,mon.Simbolo +' :' as SimboloMoneda,
	   d.CoEstado
	from DOCUMENTO d --Left Join DOCUMENTO_DET_TEXTO ddt On d.NuDocum = ddt.NuDocum and d.IDTipoDoc = ddt.IDTipoDoc                            
	Left Join COTICAB c On c.IDCab = d.IDCab                                
                
	Left Join MACLIENTES clfile On Isnull(d.IDCliente,c.IDCliente) = clfile.IDCliente                                
	Left Join MAUBIGEO u On clfile.IDCiudad=u.IDubigeo                          
                
	Left Join MACLIENTES cl2 On d.IDCliente = cl2.IDCliente                           
	Left Join MAUBIGEO u2 On cl2.IDCiudad=u2.IDubigeo                          
	Left Join MAMONEDAS mon On mon.IDMoneda=d.IDMoneda  
	 --Left Join MAUBIGEO up On u.IDPais=up.IDubigeo                          
	 --Left Join DOCUMENTO dvinc On d.IDTipoDocVinc=dvinc.IDTipoDoc And d.NuDocumVinc=dvinc.NuDocum                    
	where d.NuDocum = @NuDocum and d.IDTipoDoc =@IDTipoDoc                           
	--Order by NuDetalle                            
	--Order By ddt.NoTexto desc

