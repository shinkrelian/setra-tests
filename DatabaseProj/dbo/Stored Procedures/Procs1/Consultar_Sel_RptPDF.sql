﻿--JRF-20130924-Nuevo campo calculado (Total,TotalCompleto,TotalPorcentaje) y Observaciones                                          
--HLF-20130716-Cambiando contenido de columna Total,                                           
--Cambiando contenido de columna TotalPorcentaje,                                           
--Agregando columna Noches                                          
--Agregando columna IDTipoProv                                          
--Cambiando NroPax por Cantidad                                          
--HLF-20130717-Agregando columna CantidadaPagar                                          
--Agregando columna SaldoxPagar                                         
--HLF-20130724-Cambiando cl.RazonSocial as Cliete por cl.RazonComercial as Cliente                                        
--JRF-20130724-Agregando la descripcion del estado                                      
--JRF-20130726-Cambiando valores de estado.                                    
--HLF-20130730-Agregando manejo de cambio de moneda                                  
--JRF-20130809-Agregand FechaIN / FechaOUT                                
--HLF-20130821-Agregando isnull(FechaOut,'')                               
--HLF-20130906--Cambiar isnull(o.TCambio,sd.TC) en vez de o.TCambio                          
--JRF-20130910--Colocar el Nombre del ejecutivo de reserva responsable.                        
--JRF-HLF-20131014- Cambiando El NetoHab por TotalHab           
--JRF-20140712-Agregando los campos de solvencia y Estado DebitMemo.                     
--HLF-20140905- case when o.IDBanco ='000' then         
-- isnull((Select isnull(NombreBancoPag,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte),'')        
-- else mb.Descripcion End As BancoDeposito,      
--JRF-20150113-Cambio del campo NombreBancoPag [BancoExtranjero]      
--JRF-20150303-IsNull(CtaCte,'')... y Agregar columnas TipoCuent  
--JRF-20150304-Agrgar las columnas del DatoBenef.  
--JRF-20150411-... IsNull(od.TxDescripcion,'')
--JRF-20150422-Filtrar por la moneda ingresada.
--JHD-20150611-SSDetraccion=isnull(o.SSDetraccion,0)
--JHD-20150611-,SSDetraccion
--JRF-20150903-Agregar  IsNull((Select isnull(m.Simbolo,'') From MAADMINISTPROVEEDORES ad Left Join MAMONEDAS m On ad.IDMoneda=m.IDMoneda
--		 Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And ad.IDMoneda=o.IDMoneda),'') as MonedaSimboloCuenta,  
--JRF-20150903-TCambio
--HLF-20150915-@TipoCuenta
--JRF-20151021-..isnull((Select isnull(NombresTitular,'') From MAADMINISTPROVEEDORES Where.. As ProveedorRazonSocial
--FLK-JRF-20151028-Validar la Ofic. de Fac. para el Tipo de Cuenta.
--HLF-20151029-isnull(fpc.Descripcion,'') as NoFormaPagoTarjCred
--Left Join MAFORMASPAGO fpc On pr.CoTarjCredAcept=fpc.IdFor
--HLF-20151109-Inner Join ORDENPAGO_DET od  en vez de Left Join ORDENPAGO_DET od 
--HLF-20151111-FechaEmision
--Left Join MAUSUARIOS er On o.CoResponsable = er.IDUsuario    
--ISNULL(rtrim(er.Nombre),'')+' '+isnull(ltrim(er.TxApellidos),'') 
--HLF-20151112-TotalCompleto+SaldoxPagar as TotalCompleto
--,SELECT FROM AS Y
--PPMG - 20160218 - En tipo de cuenta ciudad 000065
--HLF-20160330- - OtrasOrdenes  ... as OtrasOrdenes,
CREATE Procedure [dbo].[Consultar_Sel_RptPDF]                                              
 @IDOrdPago int                                              
As                                              
 Set NoCount On                                              

 Declare @TipoCuenta char(2)=''
 Select @TipoCuenta= 
		--Case when ub.IDPais='000323' Then 'NP' else
		--PPMG-20160218
		--Case when (ub.IDPais='000323' And Exists(select TipoCuenta from MAADMINISTPROVEEDORES ad where ad.IDProveedor=r.IDProveedor and ad.TipoCuenta='NP')) Then 'NP' else
		Case when (ub.IDPais='000323' And 
				Exists(select TipoCuenta from MAADMINISTPROVEEDORES ad where ad.IDProveedor=r.IDProveedor and ad.TipoCuenta='NP'))
				OR (p.CoUbigeo_Oficina='000065' And Exists(select TipoCuenta from MAADMINISTPROVEEDORES ad where ad.IDProveedor=r.IDProveedor and ad.TipoCuenta='NP')
				) Then 'NP' else
		Case When ub.IDPais='000011' and p.CoUbigeo_Oficina='000110' Then 'NC' else 
			Case When ub.IDPais='000003' and p.CoUbigeo_Oficina='000113' Then 'NA' else 'EX' End End End
 From 
 ORDENPAGO op Inner Join RESERVAS r On op.IDReserva=r.IDReserva
 Inner Join MAPROVEEDORES p On p.IDProveedor=r.IDProveedor And op.IDOrdPag=@IDOrdPago
 Left Join MAUBIGEO ub On ub.IDubigeo=p.IDCiudad 

 SELECT 
 
	FechaIn, FechaOut,                              
	 Cantidad,Servicio,SimboloMoneda,Total,
	 --TotalCompleto
	 TotalCompleto+Case When IDReserva_Det=229692 then 0 else SaldoxPagar End as TotalCompleto,
	 TotalPorcentaje,
	 --TotalPorcentaje+SaldoxPagar as TotalPorcentaje,    
	 PorPrepago,                                  
	 IDOrdPag,IDFile,NroPax,NroLiberados,    
	 Noches,FechaPago,Titulo,Cliente,                                              
	 Nombre,ProveedorReferente,ProveedorRazonSocial,                                  
	 BancoDeposito,CtaCte,TipoCuenta, Observacion, IDTipoProv, CantidadAPagar,                                                
	 SaldoxPagar, 
	 TotalSinPrc,   
	 --TotalSinPrc+SaldoxPagar as TotalSinPrc,
	 IDMoneda,DescEstado, ResponsableReservas ,IDReserva_Det,IDServicio_Det ,Solvencia,EstadoDebitMemo,  
	 TitularBenef,DireccionBenef,NroCuentaBenef,  
	 NombreBancoInte,NombreBancoPag,PECBancoInte,PECBancoPag,SWIFTBancoInte,SWIFTBancoPag                
	  ,SSDetraccion, MonedaSimboloCuenta,MonedaDescCuenta,MonedaIDCuenta,TCambio,NoFormaPagoTarjCred,
	  FechaEmision
	 ,DescontarMismaMoneda,DescontarDifMoneda,OtrasOrdenes
 FROM
	 (
	 SELECT FechaIn, isnull(FechaOut,'') as FechaOut,                              
	 Cantidad,Servicio,SimboloMoneda,Total,
	 TotalCompleto,           
	 case when Porcentaje > 0 then                     
	  cast(TotalCompleto  as numeric(10,2))                     
	 else                    
	  TotalSinPrc                    
	 End As TotalPorcentaje,    
	 PorPrepago,                                  
	 IDOrdPag,IDFile,NroPax,NroLiberados,    
	 Noches,FechaPago,Titulo,Cliente,                                              
	 Nombre,ProveedorReferente,ProveedorRazonSocial,                                  
	 BancoDeposito,CtaCte,TipoCuenta, Observacion, IDTipoProv, CantidadAPagar,                                                
	 Case When IDOrdPag In(2142,3400,3449) Then 0 Else
	 Case when Isnull(SaldoxPagar - DescontarMismaMoneda - DescontarDifMoneda,0) < 0 then 0 else            
	 Isnull(SaldoxPagar - DescontarMismaMoneda - DescontarDifMoneda - OtrasOrdenes,0)  End End
	 as SaldoxPagar, TotalSinPrc,                    
	 IDMoneda,DescEstado, ResponsableReservas ,IDReserva_Det,IDServicio_Det ,Solvencia,EstadoDebitMemo,  
	 TitularBenef,DireccionBenef,NroCuentaBenef,  
	 NombreBancoInte,NombreBancoPag,PECBancoInte,PECBancoPag,SWIFTBancoInte,SWIFTBancoPag                
	  ,SSDetraccion, MonedaSimboloCuenta,MonedaDescCuenta,MonedaIDCuenta,TCambio,NoFormaPagoTarjCred,
	  FechaEmision 
	  ,DescontarMismaMoneda,DescontarDifMoneda,OtrasOrdenes
	 FROM 
	 (            
	 Select                                            
	  (Select Top(1) Convert(char(10),rd.Dia,103) from RESERVAS_DET rd where rd.IDReserva = o.IDReserva order by rd.Dia) As FechaIn,                    
	 (Select Top(1) Convert(char(10),rd.FechaOut,103) from RESERVAS_DET rd                               
	 where rd.IDReserva = o.IDReserva and not rd.FechaOut is null order by rd.FechaOut desc) As FechaOut,                                                    
                                 
	 Case When pr.IDTipoProv='001' or sd.ConAlojamiento=1  Then                                          
	 od.Cantidad         
	 Else                                          
	 1                                          
	 End as Cantidad,                                            
	 Case When od.IDTransporte is not null Then 'Ticket Aereo '+ cv.Ruta Else ISNULL(od.TxDescripcion,IsNull(rd.Servicio,'')) End AS Servicio,
	 mo.Simbolo as SimboloMoneda,                               
	 --rd.NetoGen as Total ,                                          
	 --rd.NetoHab as Total ,                                          
                                   
	 Case When od.IDReserva_Det Is Null Then  
		 Case When od.IDTransporte is not null then IsNull(cv.CostoOperador,0) Else
		 od.Total End
	 Else                   
	  Case When o.IDMoneda=rd.IDMoneda Then                                  
	   rd.TotalHab --rd.NetoHab                                   
	  Else                                  
	   od.Total--dbo.FnCambioMoneda(IsNull(od.Total,0) ,rd.IDMoneda,o.IDMoneda,isnull(o.TCambio,sd.TC)) --dbo.FnCambioMoneda(rd.NetoHab ,rd.IDMoneda,o.IDMoneda,isnull(o.TCambio,sd.TC))  -- o.TCambio)                                  
	  End                           
	 End as Total,     
                                           
                           
	 Case When od.IDReserva_Det Is Null Then                          
	  od.Total                          
	 Else               
	  Case When o.IDMoneda=rd.IDMoneda Then                                  
	   --rd.TotalGen                                  
	   od.Total                  
	  Else                                  
	   od.Total--dbo.FnCambioMoneda(rd.TotalGen ,rd.IDMoneda,o.IDMoneda,isnull(o.TCambio,sd.TC))                                  
	  End                                    
	 End as TotalCompleto,                                   
                                  
                                              
	 --cast((o.Porcentaje/100) * rd.TotalGen  as numeric(10,2)) as TotalPorcentaje,                                           
	 Porcentaje, TotalGen,  
	 'Prepago ('+CAST(o.Porcentaje as varchar(8))+'%)' As PorPrepago                                              
	 ,o.IDOrdPag,c.IDFile,c.NroPax,Isnull(c.NroLiberados,0) As NroLiberados,                                           
	 isnull(od.Noches,0) as Noches ,                    
	 Convert(char(10),o.FechaPago,103) As FechaPago,c.Titulo,  
	 cl.RazonComercial as Cliente,  
	 Ev.Nombre,pr.NombreCorto as ProveedorReferente,
	 --pr.RazonSocial as ProveedorRazonSocial,
	 isnull((Select isnull(NombresTitular,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as ProveedorRazonSocial,
	 case when o.IDBanco ='000' then         
	 isnull((Select isnull(BancoExtranjero,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'')  
	 else mb.Descripcion End As BancoDeposito,                                            
	 IsNull((Select isnull(TipoCuenta,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as TipoCuenta,  
	 IsNull((Select isnull(TitularBenef,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as TitularBenef,  
	 IsNull((Select isnull(DireccionBenef,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as DireccionBenef,  
	 IsNull((Select isnull(NroCuentaBenef,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as NroCuentaBenef,  
	 IsNull((Select isnull(NombreBancoPag,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as NombreBancoPag,  
	 IsNull((Select isnull(PECBancoPag,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as PECBancoPag,  
	 IsNull((Select isnull(SWIFTBancoPag,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as SWIFTBancoPag,  
	 IsNull((Select isnull(NombreBancoInte,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as NombreBancoInte,  
	 IsNull((Select isnull(PECBancoInte,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as PECBancoInte,  
	 IsNull((Select isnull(SWIFTBancoInte,'') From MAADMINISTPROVEEDORES Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And IDMoneda=o.IDMoneda And TipoCuenta=@TipoCuenta),'') as SWIFTBancoInte,  
	 IsNull(o.CtaCte,'') as CtaCte,       
	 IsNull((Select Top(1) isnull(m.Simbolo,'') From MAADMINISTPROVEEDORES ad Left Join MAMONEDAS m On ad.IDMoneda=m.IDMoneda
			 Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And TipoCuenta=@TipoCuenta),'') as MonedaSimboloCuenta,  
	 IsNull((Select Top(1) isnull(m.Descripcion,'') From MAADMINISTPROVEEDORES ad Left Join MAMONEDAS m On ad.IDMoneda=m.IDMoneda
			 Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And TipoCuenta=@TipoCuenta),'') as MonedaDescCuenta,  
	 IsNull((Select Top(1) isnull(m.IDMoneda,'') From MAADMINISTPROVEEDORES ad Left Join MAMONEDAS m On ad.IDMoneda=m.IDMoneda
			 Where IDProveedor=r.IDProveedor and CtaCte=o.CtaCte And TipoCuenta=@TipoCuenta),'') as MonedaIDCuenta,
	 Isnull(O.Observaciones,'') as Observacion                                            
	 ,pr.IDTipoProv,                           
	 Case When od.IDReserva_Det Is Null Then                          
	   Case When od.IDTransporte is not null then (select COUNT(*) from COTITRANSP_PAX ctp where ctp.IDTransporte=od.idTransporte)
	   Else 0 End
	 Else             
	 Cast(rd.CantidadAPagar as tinyint)                            
	 End as CantidadAPagar,                   
                         
	 Case When od.IDReserva_Det Is Null Then                          
	  Case When od.IDTransporte is not null then IsNull(cv.CostoOperador,0)*(select COUNT(*) from COTITRANSP_PAX cpx Where cpx.IDTransporte=od.IDTransporte)
	  else 0 End
	 Else                          
	  Case When o.IDMoneda=rd.IDMoneda Then                                  
	   rd.TotalGen                 
	  Else                                  
	   dbo.FnCambioMoneda(rd.TotalGen ,rd.IDMoneda,o.IDMoneda,isnull(o.TCambio,sd.TC))                                  
	  End                                  
	 End  as SaldoxPagar,                    
	 --o.IDMoneda  As Moneda1,                    
	 Case When od.IDTransporte is not Null then  
	  (select SUM(odp3.Total) from ORDENPAGO_DET odp3 Where odp3.IDTransporte=od.IDTransporte)
	 Else                   
	 (select SUM(total) from ORDENPAGO_DET odp2 Left Join ORDENPAGO o2 On odp2.IDOrdPag = o2.IDOrdPag                 
	  where odp2.IDReserva_Det = rd.IDReserva_Det and odp2.IDServicio_Det = rd.IDServicio_Det                     
	  and o2.IDMoneda = o.IDMoneda and o2.IDCab = o.IDCab
	  ) 
	 End 
	  as DescontarMismaMoneda,                    
                     
	 ISNULL(                    
	 (select sum(TotalDifMon) from (                 
	 select                    
	  dbo.FnCambioMoneda(od1.Total,op1.IDMoneda,o.IDMoneda,Isnull(Isnull(sd1.TC,o.TCambio),op1.TCambio)) as TotalDifMon
	 From ORDENPAGO_DET od1                               
	 Inner Join ORDENPAGO op1 On od1.IDOrdPag=op1.IDOrdPag  --and od1.IDOrdPag=o.IDOrdPag
	 Left Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                              
	 Inner Join RESERVAS_DET rd1 On od1.IDReserva_Det=rd1.IDReserva_Det And rd1.Anulado=0                      
	 Where od1.IDReserva_Det=rd.IDReserva_Det And op1.IDMoneda <> o.IDMoneda) as XTotal),0)                     
                     
	 as DescontarDifMoneda,                    

	--isnull((Select  sum(o1.SsMontoTotal) --SUM(dbo.FnCambioMoneda(o1.SsMontoTotal,o1.IDMoneda,o.IDMoneda,isnull(o.TCambio,0))) 
	--From ORDENPAGO o1
	--Where o1.IDCab=o.IDCab and o1.IDReserva=o.IDReserva and o1.IDOrdPag<>o.IDOrdPag and o1.idestado<>'EA'),0)
	--as OtrasOrdenes,
	0 as OtrasOrdenes,
	
	od.Total as TotalSinPrc,                     
	 od.IDReserva_Det,                        
	 OD.IDServicio_Det,                    
	 o.IDMoneda  ,                         
	--o.TCambio,                                 
	 Case when o.IDEstado = 'GR' Then 'GENERADO' Else                                      
	 case when  o.IDEstado = 'PD' Then 'PENDIENTE' else                       
	 case when o.IDEstado = 'DV' Then 'DEVUELTO' ELSE 'PAGADO' End                                      
	 End                                      
	 End as DescEstado              ,                
	 case when o.IDOrdPag = 343 Then 
		ISNULL(Opr.Nombre,'') 
	 Else 
		--ISNULL(er.Nombre,'')
		Case When ltrim(rtrim(ISNULL(rtrim(er.Nombre),'')+' '+isnull(ltrim(er.TxApellidos),'')))<>'' then
			ltrim(rtrim(ISNULL(rtrim(er.Nombre),'')+' '+isnull(ltrim(er.TxApellidos),'')))
		else
			ltrim(rtrim(ISNULL(rtrim(er2.Nombre),'')+' '+isnull(ltrim(er2.TxApellidos),'')))
		end 
	 End As ResponsableReservas,          
	 cl.Solvencia,          
	 Case When Exists(Select IDDebitMemo from DEBIT_MEMO dm where dm.IDCab = c.IDCab and dm.IDEstado = 'PG') Then          
	 'PAGADO' Else 'PENDIENTE' End  as EstadoDebitMemo,
	 SSDetraccion=isnull(o.SSDetraccion,0),IsNull(o.TCambio,0) as TCambio,
	 isnull(fpc.Descripcion,'') as NoFormaPagoTarjCred,
	 o.FechaEmision
	 from ORDENPAGO o Inner Join ORDENPAGO_DET od On o.IDOrdPag=od.IDOrdPag                                              
	 Left Join COTICAB c on o.IDCab = c.IDCAB Left Join MACLIENTES cl on c.IDCliente = cl.IDCliente                                              
	 Left Join MAUSUARIOS Ev On c.IDUsuario = Ev.IDUsuario Left Join RESERVAS r On o.IDReserva = r.IDReserva                                              
	 Left Join MAPROVEEDORES pr on r.IDProveedor = pr.IDProveedor                             
	 Left Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det 
	 Left Join MAMONEDAS mo On o.IDMoneda = mo.IDMoneda                                              
	 Left Join MASERVICIOS_DET sd On sd.IDServicio_Det = rd.IDServicio_Det                                           
	 Left Join MABANCOS mb On o.IDBanco = mb.IDBanco Left Join MAFORMASPAGO fp on o.IDFormaPago = fp.IdFor                                            
	 --Left Join MAUSUARIOS er On c.IDUsuarioRes = er.IDUsuario                        
	 Left Join MAUSUARIOS er On o.CoResponsable = er.IDUsuario   
	 Left Join MAUSUARIOS er2 On c.IDUsuarioRes = er2.IDUsuario                             
	 Left Join MAUSUARIOS Opr On c.IDUsuarioOpe = Opr.IDUsuario                
	 Left Join COTIVUELOS cv On od.IDTransporte= cv.ID 
	 Left Join MAFORMASPAGO fpc On pr.CoTarjCredAcept=fpc.IdFor
	 where o.IDOrdPag = @IDOrdPago -- And od.Total > 0                                     
	 ) as X 
 ) as Y
