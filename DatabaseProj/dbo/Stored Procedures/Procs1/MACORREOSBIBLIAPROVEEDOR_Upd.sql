﻿Create Procedure dbo.MACORREOSBIBLIAPROVEEDOR_Upd
@IDProveedor char(6),
@Correo varchar(150),
@Descripcion varchar(Max),
@UserMod char(4)
As
Set NoCount On
Update MACORREOSBIBLIAPROVEEDOR
	set Descripcion = Case When ltrim(rtrim(@Descripcion))='' then Null Else @Descripcion End,
		UserMod = @UserMod,
		FecMod = GETDATE()
Where IDProveedor = @IDProveedor and Correo = @Correo
