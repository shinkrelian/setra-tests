﻿--JRF-20140628-Agregando el identificador Amadeus
CREATE Procedure dbo.COTIPAX_Sel_Pk  
@IDPax int  
As  
Set NoCount On  
Select cp.*,Isnull(ti.CoAmadeus,'') as CoAmadeusIdent
from COTIPAX cp Left Join MATIPOIDENT ti On cp.IDIdentidad = ti.IDIdentidad
where IDPax = @IDPax  
