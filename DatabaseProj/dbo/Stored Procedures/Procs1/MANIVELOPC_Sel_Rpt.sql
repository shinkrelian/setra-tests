﻿CREATE procedure [dbo].[MANIVELOPC_Sel_Rpt]	 
@IDNivel char(3)
as
	Set NoCount On
	
	select nop.IDOpc,nu.IDNivel,nu.Descripcion as Cargo,
	 Descripcion=
		 case when SUBSTRING(nop.IDOpc,3,4)='0000' then
			op.Descripcion
		 Else
			case when SUBSTRING(nop.IDOpc,5,2)<>'00' then
			 '    '+op.Descripcion
			 else
			  --case when SUBSTRING(upc.IDOpc,3,2)<>'00' then
			   '  ' +op.Descripcion
			  --End
			End
		 End
	,nop.Consultar,nop.Grabar,nop.Actualizar
	,nop.Eliminar
	FROM MANIVELOPC nop Left Join MANIVELUSUARIO nu On nop.IDNivel=nu.IDNivel
	Left join MAOPCIONES op On nop.IDOpc=op.IDOpc
		where nop.IDNivel=@IDNivel
	Order by nop.IDOpc
