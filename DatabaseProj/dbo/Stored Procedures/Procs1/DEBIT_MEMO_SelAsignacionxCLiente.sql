﻿

--JRF-20131121-Subdividir IDFile -Correlativo (DebitMemo)  
--JRF-20131202-Considerar el Tipo de DebitMemo.
--HLF-20150512-	Case When dm.IDCliente=@CoClienteVtasAdicAPT Then
--isnull((Select Top 1 isnull(NuDocum,'') From VENTA_ADICIONAL_DET Where IDDebitMemo=dm.IDDebitMemo),'')
--HLF-20150904-,'' as CoSAP
--HLF-20150922-SSComisionBanco
CREATE Procedure dbo.DEBIT_MEMO_SelAsignacionxCLiente
	@IDCLiente varchar(6)      
As      

	Set NoCount On      

	Declare @CoClienteVtasAdicAPT char(6)=(Select CoClienteVtasAdicAPT From PARAMETRO)

	Select distinct dm.IDDebitMemo as IDDebitMemoCod,
	Case When dm.IDTipo = 'I' Then SUBSTRING(dm.IDDebitMemo,1,8)+'-'+SUBSTRING(dm.IDDebitMemo,9,10)
	Else SUBSTRING(dm.IDDebitMemo,1,3)+'-'+SUBSTRING(dm.IDDebitMemo,4,10) End as IDDebitMemo,
	Case When dm.IDCliente=@CoClienteVtasAdicAPT Then
		isnull((Select Top 1 isnull(NuDocum,'') From VENTA_ADICIONAL_DET Where IDDebitMemo=dm.IDDebitMemo),'')
	Else
		''
	End as DocVentasAdicAPT,
	dm.IDEstado,dm.IDTipo,dm.Titulo,
	Case When dm.IDTipo = 'I' then Convert(Char(10),dm.Fecha,103) Else '' End as FechaCobro,      
	Case When dm.IDTipo = 'I' then CONVERT(Char(10),dm.FecVencim,103) else '' End as FechaVencimiento,      
	Case When dm.IDTipo = 'I' then CONVERT(Char(10),dm.FechaIn,103)else '' End as FechaIN,
	Case When dm.IDTipo = 'I' then CONVERT(Char(10),dm.FechaOut,103)else '' End as FechaOUT,      
	dm.Total,dm.Total - Isnull((Select SUM(SsPago+SsGastosTransferencia) from INGRESO_DEBIT_MEMO idm2 where dm.IDDebitMemo=idm2.IDDebitMemo),0) as Saldo,      
	0 as ExistePago,      
	0.00 as SsPago ,0.00 as GastoTransfer ,       
	Case When cast(substring(dm.IDDebitMemo,9,10) as tinyint) = 0 then      
	(Select SUM(Total*NroPax) from COTIDET cd where cd.IDCAB = c.IDCAB and (cd.IDServicio = 'LIM00234' Or cd.IDServicio = 'LIM00461'))      
	else 0 End      
	as GastoTransferCotiz,  
	0 as SSComisionBanco,    
	dm.Total - Isnull((Select SUM(SsPago+SsGastosTransferencia) from INGRESO_DEBIT_MEMO idm2 where dm.IDDebitMemo=idm2.IDDebitMemo),0) as SaldoNuevo,      
	Case When a.NuAsignacion Is Null Or dm.IDEstado = 'PP' then 'N' else 'M' End As Accion,      
	Isnull(dm.IDDebitMemoResumen,'') as IDDebitMemoResumen, c.IDCAB
	,'' as CoSAP
	from DEBIT_MEMO dm Left Join INGRESO_DEBIT_MEMO idm On dm.IDDebitMemo = idm.IDDebitMemo      
	Left Join ASIGNACION a On idm.NuAsignacion = a.NuAsignacion      
	Left Join COTICAB c On dm.IDCab = c.IDCAB      
	Where dm.IDTipo <> 'R' And dm.IDCliente = @IDCLiente and dm.Saldo <> 0 and dm.IDEstado <> 'AN'      


