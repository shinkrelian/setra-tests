﻿--HLF-20160307-stefan.vedder@setours.com; cynthia.alegre@setours.com
CREATE Procedure [dbo].[EnvioCorreosWeeklyNetSales]
	@Fecha smalldatetime	
As
	Set Nocount on
	
	Declare @vcMensaje1 varchar(max)=''--, @vcMensaje2 varchar(max)=''
	
	Exec ReporteWeeklyNetSalesHTML @Fecha,'01/01/1900','w',@vcMensaje1 output--,@vcMensaje2 output

	--Exec EnviarCorreo 'SETRA','hector.lindley@setours.com','','',
	--	@vcMensaje1,'Listado de Files Nuevos Semanal','HTML',''

	--Exec EnviarCorreo 'SETRA','hector.lindley@setours.com','','',
	--	@vcMensaje2,'Listado de Files Modificados/Anulados Semanal','HTML',''
	
	--Exec EnviarCorreo 'SETRA','hector.lindley@setours.com','','',
	Exec EnviarCorreo 'SETRA','peter.holzweber@setours.com','fanny.vilchez@setours.com; stefan.vedder@setours.com; cynthia.alegre@setours.com',
		'hector.lindley@setours.com',	
		@vcMensaje1,'Envío Semanal de Files Nuevos/Modificados/Anulados','HTML',''

	--Exec EnviarCorreo 'SETRA','floris.klick@setours.com','','',
		--@vcMensaje1,'Test Envío Semanal de Files Nuevos/Modificados/Anulados','HTML',''
