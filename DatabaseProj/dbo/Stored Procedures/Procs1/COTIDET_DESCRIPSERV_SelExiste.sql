﻿
Create Procedure COTIDET_DESCRIPSERV_SelExiste
@IDDet int,
@IDIdioma varchar(12),
@pExiste bit output
As
	Set NoCount On
	If Exists(select IDIdioma from COTIDET_DESCRIPSERV Where IDDet = @IDDet And IDIdioma = @IDIdioma)
		set @pExiste = 1
	else
		set @pExiste = 0
