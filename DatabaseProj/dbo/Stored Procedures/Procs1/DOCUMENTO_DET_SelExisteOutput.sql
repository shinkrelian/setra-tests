﻿
--HLF-20140807-@IDVoucher varchar(15) (antes int)  
--HLF-20140819-@NoServicio
CREATE Procedure dbo.DOCUMENTO_DET_SelExisteOutput    
 @IDCab int,    
 @IDProveedor char(6),    
 @IDTipoOC char(3),    
 @IDMoneda char(3),    
 @CoTipoDet char(2),    
 @IDVoucher varchar(15),  
 @NoServicio varchar(230),
 @pNuDetalle tinyint Output    
As    
 Set Nocount On    
     
      
 Select @pNuDetalle=dd.NuDetalle From DOCUMENTO_DET dd Inner Join DOCUMENTO d     
  On d.NuDocum=dd.NuDocum And d.IDTipoDoc=dd.IDTipoDoc And d.IDCab=@IDCab    
  Where dd.IDProveedor=@IDProveedor And dd.IDTipoOC=@IDTipoOC     
  And dd.IDMonedaCosto=@IDMoneda And dd.CoTipoDet=@CoTipoDet    
  And isnull(dd.IDVoucher,'')=@IDVoucher     
  And (dd.NoServicio=@NoServicio Or LTRIM(RTRIM(@NoServicio))='')
  
 Set @pNuDetalle=ISNULL(@pNuDetalle,0) 
 
 
