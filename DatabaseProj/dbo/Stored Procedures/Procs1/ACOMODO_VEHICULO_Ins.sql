﻿Create Procedure dbo.ACOMODO_VEHICULO_Ins
	@NuVehiculo smallint,
	@IDDet int,
	@QtVehiculos tinyint,
	@QtPax tinyint,
	@FlGuia bit,
	@UserMod char(4)
As
	Set Nocount on

	Insert Into ACOMODO_VEHICULO
	(NuVehiculo,IDDet,QtVehiculos,QtPax,FlGuia,UserMod)
	Values(@NuVehiculo,@IDDet,@QtVehiculos,@QtPax,@FlGuia,@UserMod)
