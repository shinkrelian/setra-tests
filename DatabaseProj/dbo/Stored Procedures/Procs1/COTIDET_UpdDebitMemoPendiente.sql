﻿CREATE Procedure dbo.COTIDET_UpdDebitMemoPendiente  
 @IDDet int,  
 @IDDebitMemo char(10),
 @DebitMemoPendiente bit,
 @UserMod char(4)  
As  
 Set Nocount On  
   
 Update COTIDET Set DebitMemoPendiente=@DebitMemoPendiente, 
 IDDebitMemo = case when ltrim(rtrim(@IDDebitMemo))='' then Null else @IDDebitMemo End,
 UserMod=@UserMod, FecMod=GETDATE()  
 Where IDDET=@IDDet  
