﻿--JHD-20160104-Se agregaron nuevos campos @TipoPax y @Tipo_Lib
--JHD-20160104-Se agrego un bucle de insercion para cotipax
--JHD-20160106-Se agrego el campo redondeo
CREATE Procedure [dbo].[COTICAB_Ins_Outlook]     
 @IDCliente char(6),                
 @Titulo varchar(100),          
 @IDUsuario char(4),                
 @NroPax smallint,                
 @NroLiberados smallint, 
 @TipoPax char(3),  
 @Tipo_Lib char(1)='',
 @NuPedInt int,
 @UserMod char(4),        
 @pIDCab int Output                
As                
 Set NoCount On                
 Declare @FechaMesActual varchar(4)=SubString((convert(varchar,getdate(),112)),3,4)                
 Declare @siCotizacion int=IsNull((Select MAX(Cotizacion) From COTICAB                 
  Where Left(Cotizacion,4)=@FechaMesActual), CAST(@FechaMesActual+'0000' AS INT)  )+1                
 Declare @Cotizacion varchar(8)=Cast(@siCotizacion as varchar(8))                 
 --Set @Cotizacion=@FechaMesActual + REPLICATE('0',(4-LEN(@Cotizacion))) + @Cotizacion                
                 
 INSERT INTO COTICAB                
           (Cotizacion                           
           ,IDCliente                
           ,Titulo                
           ,TipoPax                
           ,IDUsuario                
           ,NroPax                
           ,NroLiberados   
		   ,Tipo_lib                       
           ,IDBanco                               
           ,IDTipoDoc                
           ,FecInicio                
           ,FechaOut                
           ,Cantidad_dias                
           ,TipoServicio                
           ,IDIdioma                
           ,IDubigeo                
           --,MargenCero                     
           ,DiasReconfirmacion          
           ,CoTipoVenta  
           ,DocWordVersion  
           ,UserMod
           ,Redondeo
		   ,FechaSeg
		   ,NuPedInt
		   ,Aporte)
     VALUES                
           (@Cotizacion,                             
            @IDCliente,                
            @Titulo ,                
			-- 'EXT' ,                
			@TipoPax,
		   @IDUsuario ,                
		   @NroPax ,                
		   Case When @NroLiberados=0 Then Null Else @NroLiberados End,     
		   Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End,                            
		   '000',                            
		   'FAC',                
		   GETDATE(),                
		   GETDATE(),                
		   0,                
		   'RG',                
		   'ENGLISH',                
		   (select idciudad from MACLIENTES where IDCliente=@IDCliente),                
		   --@MargenCero,          
		   0,          
		   'RC',    
		   1,  
		   @UserMod
		   ,1
		   ,GETDATE()
		   ,Case When @NuPedInt = 0 Then Null else @NuPedInt End
		   ,0
		)                    
 Set @pIDCab =(Select MAX(IDCAB) From COTICAB)  

--declare @NroPaxpax_Tmp int=@NroPax
 declare @contador int=1

 while @contador<=@NroPax
 BEGIN
	--SELECT @contador
	
	--insertar en cotipax
				INSERT INTO [COTIPAX]        
			 ([IDCab]      
			 ,[Orden]    
			 ,[IDIdentidad]        
			 ,[NumIdentidad] 
			 ,[Nombres]  
			 ,[Apellidos]
			 ,[Titulo]   
			 ,[FecNacimiento]        
			 ,[IDNacionalidad]        
			 ,[Peso]      
			 ,[ObservEspecial]      
			 ,[Total]         
			 ,[SSTotal]    
			 ,[STTotal]    
			 ,[Residente]  
			 ,[IDPaisResidencia]  
			 ,[FecIngresoPais]  
			 ,[UserMod]  
			 ,TxTituloAcademico 
			 ,FlTC
			 ,FlNoShow
			 ,FlEntMP
			 ,FlEntWP
			 ,FlAutorizarMPWP
			 ,NumberAPT
			 )        

			 VALUES        

			 (@pIDCab, 
			 cast(@contador as smallint),  
			 '004'        
			 , Null   
			 ,'Pax '+cast(@contador as varchar(50))
			 ,'Pax '+cast(@contador as varchar(50))        
			 ,'Mr.'        
			 ,Null
			 ,'000001'      
			 ,Null 
			 , Null 
			 ,0        
			 ,0       
			 ,0       
			 ,cast(0 as bit)    
			 ,'000001'        
			 ,Null          
			 ,@UserMod
			 ,'---' 
			 ,cast(0 as bit)      
			 ,cast(0 as bit)     
			 ,cast(0 as bit)    
			 ,cast(0 as bit)    
			 ,cast(0 as bit)    
			 ,''
			 )        

	SET @contador=@contador+1
 END
