﻿--JRF-20150319-Nuevo campo (@SsReembolso)
CREATE Procedure dbo.COTITRANSP_PAX_Upd  
 @IDTransporte int,  
 @IDPaxTransp smallint,   
 @CodReservaTransp varchar(12),  
 @IDProveedorGuia char(6),  
 @SsReembolso numeric(8,2),
 @UserMod char(4)  
As  
 Set Nocount on  
  
 Update COTITRANSP_PAX Set CodReservaTransp=Case When ltrim(rtrim(@CodReservaTransp))='' Then Null Else @CodReservaTransp End,  
 IDProveedorGuia=Case When ltrim(rtrim(@IDProveedorGuia))='' Then Null Else @IDProveedorGuia End,
 SsReembolso=Case When @SsReembolso=0 Then Null else @SsReembolso End,  
 UserMod=@UserMod,  
 FecMod=GETDATE()  
 Where IDTransporte=@IDTransporte And IDPaxTransp=@IDPaxTransp  
