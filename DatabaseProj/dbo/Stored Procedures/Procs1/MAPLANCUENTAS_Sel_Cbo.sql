﻿----JHD-20150812 - 	select'000000' as CtaContable,'' As Descripcion ...
CREATE Procedure dbo.MAPLANCUENTAS_Sel_Cbo
As
	Set NoCount On
	select'000000' as CtaContable,'' As Descripcion 
	union
	select CtaContable,
	Case When CtaContable = '000000' Then '' Else CtaContable + ' - ' +Descripcion End As Descripcion 
	from MAPLANCUENTAS
	Order by 1
