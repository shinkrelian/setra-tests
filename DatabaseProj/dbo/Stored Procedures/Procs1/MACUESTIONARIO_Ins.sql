﻿Create Procedure dbo.MACUESTIONARIO_Ins
@IDTipoProv char(3),
@NoDescripcion varchar(Max),
@UserMod char(4)
As
Set NoCount On
	Declare @NuCuest tinyint = (Select Isnull(MAX(NuCuest),0) From MACUESTIONARIO)+1
	Insert into MACUESTIONARIO(NuCuest,IDTipoProv,NoDescripcion,Activo,UserMod,FecMod)
		Values(@NuCuest,Case When Ltrim(Rtrim(@IDTipoProv))='' Then Null Else @IDTipoProv End,
			   @NoDescripcion,1,@UserMod,Getdate())
