﻿Create Procedure dbo.ACOMODO_VEHICULO_DET_PAX_DelxIDDet
	@IDDet int
As
	Set Nocount on

	Delete From ACOMODO_VEHICULO_DET_PAX
	Where IDDet=@IDDet
