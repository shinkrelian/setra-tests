﻿Create Procedure dbo.COTICAB_TIPOSCAMBIO_SelExistsOutPut
@IDCab int,
@pExiste bit OutPut
As
Set NoCount On
Set @pExiste = 0
If Exists(select CoMoneda from COTICAB_TIPOSCAMBIO where IDCab = @IDCab)
	Set @pExiste = 1
