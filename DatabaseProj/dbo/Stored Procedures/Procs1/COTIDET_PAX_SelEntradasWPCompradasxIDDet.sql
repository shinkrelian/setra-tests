﻿
Create Procedure dbo.COTIDET_PAX_SelEntradasWPCompradasxIDDet
	@IDDet int,
	@pbCompradas bit output
As
	Set NoCount On

	Set @pbCompradas = 0
		
	If Exists(Select IDPax From COTIDET_PAX Where IDDET=@IDDet And 
		FlEntWPGenerada=1 And FlDesactNoShow=0)
		Set @pbCompradas = 1

