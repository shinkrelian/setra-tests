﻿--JHD-20150611-Se agrego filtro de voucher y documento       
--JHD-20150702-Codigo_Excel='FF. '+rtrim(ltrim(f.CoPrefijo))+' N° '+f.NuCodigo 
--JHD-20150702-Titulo_Excel='FONDO FIJO'+CASE WHEN rtrim(ltrim(f.CoPrefijo))='OL'THEN ' - OFICINA LIMA' WHEN rtrim(ltrim(f.CoPrefijo))='OC'THEN ' - OFICINA CUSCO' ELSE '' END
--HLF-20160317-isnull(f.CoPrefijo,'') as CoPrefijo
CREATE PROCEDURE DOCUMENTO_PROVEEDOR_Sel_Cabecera_FondoFijo
@NuFondoFijo int=1
AS
begin
SELECT      f.NuFondoFijo,
			NuCodigo=isnull(f.NuCodigo,''),
			isnull(f.CoCeCos,'') as CoCeCos,
			isnull(f.CoCeCos,'')+'-'+isnull(cc.Descripcion,'') AS Centro_Costos,
			isnull(f.CtaContable,'') as CtaContable, 
            isnull(f.CtaContable,'')+' - '+isnull(pc.Descripcion,'') AS Cuenta_Contable,
			f.Descripcion,
			f.CoUserResponsable,
			u.Usuario, 
            u.Nombre,
			f.CoMoneda,
			mo.Descripcion AS Moneda,
			mo.Simbolo,
			SSMonto=cast(round(f.SSMonto,2) as numeric(12,2)), 
            f.FlActivo,
			f.CoEstado,
			Isnull(f.SsSaldo,0) as SaldoPendiente,
			ISNULL(f.SsDifAceptada,0) as SsDifAceptada,  
			IsNull(f.TxObsDocumento,'') as TxObsDocumento ,
			Codigo_Excel='FF. '+rtrim(ltrim(f.CoPrefijo))+' N° '+f.NuCodigo,
			Titulo_Excel='FONDO FIJO'+CASE WHEN rtrim(ltrim(f.CoPrefijo))='OL'THEN ' - OFICINA LIMA' WHEN rtrim(ltrim(f.CoPrefijo))='OC'THEN ' - OFICINA CUSCO' ELSE '' END,
			isnull(f.CoPrefijo,'') as CoPrefijo
FROM            MAFONDOFIJO f LEFT JOIN
                         MACENTROCOSTOS cc ON f.CoCeCos = cc.CoCeCos LEFT JOIN
                         MAPLANCUENTAS pc ON f.CtaContable = pc.CtaContable LEFT JOIN
                         MAUSUARIOS u ON f.CoUserResponsable = u.IDUsuario LEFT JOIN
                         MAMONEDAS mo ON f.CoMoneda = mo.IDMoneda
WHERE        (f.NuFondoFijo = @NuFondoFijo OR @NuFondoFijo=0) 
end;

