﻿
CREATE PROCEDURE [dbo].[DOCUMENTO_FORMA_EGRESO_Sel_List_Proveedor]
	@IDProveedor	char(6),
	@CoMoneda		char(3)=''
as
BEGIN
 
	DECLARE @IDProveedor_Zicasso char(6) = '003557',
			@IDServicio_Zicasso char(8)	= 'LAX00002',
			@Servicio_Det_Zicasso CHAR(4) = 'ZIC6'
	DECLARE @Tbl_ServDet Table(IDProveedorX char(6),
							   IDServicioX char(8),
							   IDServicio_DetX int);
	Set NoCount On
	INSERT INTO @Tbl_ServDet(IDProveedorX, IDServicioX, IDServicio_DetX)
	SELECT S.IDProveedor, D.IDServicio, D.IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
	WHERE TipoUsoSistema = @Servicio_Det_Zicasso AND S.IDProveedor = @IDProveedor_Zicasso AND D.IDServicio = @IDServicio_Zicasso
	Set NoCount Off

SELECT '' AS Codigo, '' AS IDVoucher, Cab.Titulo, Cab.IDCAB, Cab.IDFIle,
'USD'AS IDMoneda, '$' AS Simbolo, 0.00 AS IGV, Cab.TotalDet AS Total
FROM 
(SELECT C.IDCAB, IDFIle, D.IDProveedor, Titulo, 0 AS IDOperacion, P.NombreCorto AS DescProveedor, SUM(ISNULL(Total,0)) AS TotalDet, P.NumIdentidad
FROM COTICAB C INNER JOIN COTIDET D ON C.IDCAB = D.IDCAB
INNER JOIN MAPROVEEDORES P ON D.IDProveedor = P.IDProveedor
INNER JOIN @Tbl_ServDet T ON D.IDServicio_Det = T.IDServicio_DetX AND D.IDProveedor = T.IDProveedorX AND D.IDServicio = T.IDServicioX
WHERE (C.IDFile IS NOT NULL)
GROUP BY C.IDCAB, IDFIle, D.IDProveedor, Titulo, P.NombreCorto, P.NumIdentidad) Cab LEFT OUTER JOIN
(SELECT DF.IDDocFormaEgreso, DF.TipoFormaEgreso, DF.CoMoneda, DP.IDCab
FROM DOCUMENTO_FORMA_EGRESO AS DF INNER JOIN DOCUMENTO_PROVEEDOR AS DP ON DF.IDDocFormaEgreso = DP.IDDocFormaEgreso
 AND DF.TipoFormaEgreso = DP.CoObligacionPago AND DF.CoMoneda = DP.CoMoneda_FEgreso
 INNER JOIN ESTADO_OBLIGACIONESPAGO EO ON DF.CoEstado = EO.CoEstado
 INNER JOIN MAMONEDAS MO ON DF.CoMoneda = MO.IDMoneda
WHERE (DF.TipoFormaEgreso = 'ZIC')	AND (DP.NuDocum IS NULL)
GROUP BY DF.IDDocFormaEgreso, DF.TipoFormaEgreso, DF.CoMoneda, DP.IDCab) Det
ON Cab.IDCAB = Det.IDCab
WHERE Det.IDDocFormaEgreso IS NULL AND @CoMoneda = 'USD'
ORDER BY Cab.IDFIle

/*
select distinct
Codigo=v.IDVoucher,
v.IDVoucher,
c.Titulo,
c.IDCAB,
c.IDFile,
--od.IDMoneda,
vd.CoMoneda as IDMoneda,
--m.Simbolo,
Simbolo=(select simbolo from MAMONEDAS where IDMoneda=vd.comoneda),
--Total1=Sum(od.Total),
IGV=(SELECT SUM(IgvGen) FROM OPERACIONES_DET WHERE IDVoucher=vd.IDVoucher AND IDMoneda=VD.CoMoneda),
vd.SsMontoTotal as Total
from VOUCHER_OPERACIONES v
inner join OPERACIONES_DET od on od.IDVoucher=v.IDVoucher
inner join operaciones o on o.IDOperacion=od.IDOperacion
inner join coticab c on c.IDCAB=o.IDCab
--inner join mamonedas m on m.IDMoneda=od.IDMoneda
left join VOUCHER_OPERACIONES_DET vd on vd.IDVoucher=v.IDVoucher
--where o.IDProveedor=@idproveedor
where o.IDProveedor in (select idprov from @tablaproveedores)
----v.IDVoucher=416476
AND (VD.CoMoneda=@CoMoneda or @CoMoneda='')
and v.idvoucher<>'0'
and v.IDVoucher not in (select distinct nuvoucher from DOCUMENTO_PROVEEDOR where nuvoucher is not null and FlActivo=1)

/*
group by v.IDVoucher,
c.Titulo,
c.IDFile,
vd.comoneda,
m.Simbolo
*/
order by v.IDVoucher
*/
END;

