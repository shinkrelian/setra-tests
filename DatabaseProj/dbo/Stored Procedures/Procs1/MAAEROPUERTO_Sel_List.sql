﻿--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-  And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=MAAEROPUERTO.CoUbigeo)=@CoPais )
CREATE Procedure [dbo].[MAAEROPUERTO_Sel_List]    
@CoAero char(3)='',
@NoDescripcion varchar(200)='',
@CoUbigeo char(6)='',
@TxCiudad varchar(60)='',
@TxPais varchar(60)='',
@FlActivo bit=0,
@CoPais char(6)=''  
AS
BEGIN

SELECT      MAAEROPUERTO.CoAero,
			MAAEROPUERTO.NoDescripcion,
			MAAEROPUERTO.CoUbigeo,
			MAUBIGEO.Descripcion,
			Pais=(select Descripcion from maubigeo u where u.IDubigeo=MAUBIGEO.IDPais),
			--MAAEROPUERTO.UserMod,
			--MAAEROPUERTO.FecMod,
			MAAEROPUERTO.FlActivo
FROM         MAAEROPUERTO INNER JOIN
                      MAUBIGEO ON MAAEROPUERTO.CoUbigeo = MAUBIGEO.IDubigeo
WHERE    
 (ltrim(rtrim(@CoAero))='' Or CoAero like '%' +@CoAero + '%') and   
 (ltrim(rtrim(@NoDescripcion))='' Or NoDescripcion like '%' +@NoDescripcion + '%') and   
 (ltrim(rtrim(@CoUbigeo))='' Or CoUbigeo like '%' +@CoUbigeo + '%') and   
 (ltrim(rtrim(@TxCiudad))='' Or MAUBIGEO.Descripcion like '%' +@TxCiudad + '%') and     
 (ltrim(rtrim(@TxPais))='' Or (select Descripcion from maubigeo u where u.IDubigeo=MAUBIGEO.IDPais) like '%' +@TxPais + '%') and     
  (MAAEROPUERTO.FlActivo = @FlActivo or @FlActivo =0)
  And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=MAAEROPUERTO.CoUbigeo)=@CoPais )

END;

