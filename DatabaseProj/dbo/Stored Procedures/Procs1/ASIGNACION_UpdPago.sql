﻿Create Procedure dbo.ASIGNACION_UpdPago
	@NuAsignacion int,
	@SsImportePagado numeric(10,2),
	@UserMod char(4)
As

	Update ASIGNACION Set SsImportePagado=SsImportePagado+@SsImportePagado, UserMod=@UserMod, FecMod=GETDATE()
	Where NuAsignacion=@NuAsignacion
