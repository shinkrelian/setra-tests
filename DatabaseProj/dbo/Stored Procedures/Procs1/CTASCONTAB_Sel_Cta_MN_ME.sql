﻿create procedure CTASCONTAB_Sel_Cta_MN_ME
as
 Select pc.CtaContable,pc.CtaContable+ ' - ' + pc.Descripcion as Descripcion,
 TxDefinicion=ISNULL(TxDefinicion,'')
 From 
 MAPLANCUENTAS pc
 where pc.CtaContable in ('49711001','49711002')
