﻿CREATE Procedure [Dbo].[ACOMODOPAX_FILE_Del]  
@IDPax int,  
@CapacidadHab tinyint,  
@IDCab int  
As  
 Set NoCount On  
 Delete from ACOMODOPAX_FILE Where IDPax = @IDPax And   
  CapacidadHab = @CapacidadHab And IDCAB = @IDCab  
