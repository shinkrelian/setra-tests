﻿CREATE Procedure [dbo].[MAOPCIONES_SelAccionesxUsuario_Tvw]
	@IDUsuario	char(4),
	@NivelOpc	tinyint	
As
	Set NoCount On
	
	Set @NivelOpc=@NivelOpc+@NivelOpc-1
	Create Table #OpcionesAccion(IDOpc	char(6), Accion	varchar(10), Activo Bit)
	
	Insert Into #OpcionesAccion 

	Select mo.IDOPc,'Consultar',uo.Consultar
	From MAOPCIONES mo Left Join MAUSUARIOSOPC uo On mo.IDOPc=uo.IDOpc And uo.IDUsuario=@IDUsuario
	Where --SUBSTRING(mo.IDOpc,@NivelOpc,2)<>'00' and  right(mo.IDOpc,4)<>'0000'
	(@NivelOpc=1 And right(mo.IDOpc,4)='0000')	Or
	(SUBSTRING(mo.IDOpc,3,2)<>'00' And @NivelOpc=3 And right(mo.IDOpc,2)='00') Or 	
	(@NivelOpc=5 And right(mo.IDOpc,2)<>'00')	
	Union

	Select mo.IDOPc,'Grabar',uo.Grabar
	From MAOPCIONES mo Left Join MAUSUARIOSOPC uo On mo.IDOPc=uo.IDOpc And uo.IDUsuario=@IDUsuario
	Where --SUBSTRING(mo.IDOpc,@NivelOpc,2)<>'00' and  right(mo.IDOpc,4)<>'0000'
	(@NivelOpc=1 And right(mo.IDOpc,4)='0000')	Or
	(SUBSTRING(mo.IDOpc,3,2)<>'00' And @NivelOpc=3 And right(mo.IDOpc,2)='00') Or 	
	(@NivelOpc=5 And right(mo.IDOpc,2)<>'00')	
	
	Union
	Select mo.IDOPc,'Actualizar',uo.Actualizar
	From MAOPCIONES mo Left Join MAUSUARIOSOPC uo On mo.IDOPc=uo.IDOpc And uo.IDUsuario=@IDUsuario
	Where --SUBSTRING(mo.IDOpc,@NivelOpc,2)<>'00' and  right(mo.IDOpc,4)<>'0000'
	(@NivelOpc=1 And right(mo.IDOpc,4)='0000')	Or
	(SUBSTRING(mo.IDOpc,3,2)<>'00' And @NivelOpc=3 And right(mo.IDOpc,2)='00') Or 	
	(@NivelOpc=5 And right(mo.IDOpc,2)<>'00')	
	
	Union
	Select mo.IDOPc,'Eliminar',uo.Eliminar
	From MAOPCIONES mo Left Join MAUSUARIOSOPC uo On mo.IDOPc=uo.IDOpc And uo.IDUsuario=@IDUsuario
	Where --SUBSTRING(mo.IDOpc,@NivelOpc,2)<>'00' and  right(mo.IDOpc,4)<>'0000'
	(@NivelOpc=1 And right(mo.IDOpc,4)='0000')	Or
	(SUBSTRING(mo.IDOpc,3,2)<>'00' And @NivelOpc=3 And right(mo.IDOpc,2)='00') Or 	
	(@NivelOpc=5 And right(mo.IDOpc,2)<>'00')	
	
	Select * From #OpcionesAccion
	
	Drop Table #OpcionesAccion
