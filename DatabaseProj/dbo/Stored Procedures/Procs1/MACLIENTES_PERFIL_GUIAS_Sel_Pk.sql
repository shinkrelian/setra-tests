﻿
------------------------------------------------------------------------------------------------------------------------------------------

--create
CREATE PROCEDURE [MACLIENTES_PERFIL_GUIAS_Sel_Pk]
	@CoCliente char(6),
	@CoProveedor char(6)
AS
BEGIN
SELECT     MACLIENTES_PERFIL_GUIAS.*,
			MAPROVEEDORES.NombreCorto
			
FROM         MACLIENTES_PERFIL_GUIAS INNER JOIN
                      MAPROVEEDORES ON MACLIENTES_PERFIL_GUIAS.CoProveedor = MAPROVEEDORES.IDProveedor
WHERE     
			(MACLIENTES_PERFIL_GUIAS.CoCliente = @CoCliente ) and
				(MACLIENTES_PERFIL_GUIAS.CoProveedor = @CoProveedor) 
END;
