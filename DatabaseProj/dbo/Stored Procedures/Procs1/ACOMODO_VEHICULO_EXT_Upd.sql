﻿

Create Procedure dbo.ACOMODO_VEHICULO_EXT_Upd
	@IDDet int ,
	@QtPax smallint ,
	@QtGrupo tinyint ,		
	@UserMod char(4) 
As
	Set Nocount On

	Update ACOMODO_VEHICULO_EXT 
		Set QtPax=@QtPax,
		QtGrupo=@QtGrupo,
		UserMod=@UserMod,
		FecMod=GETDATE()
	Where IDDet=@IDDet

