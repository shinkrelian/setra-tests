﻿--JHD-20150608-Se quito la actualizacion de codigo, al ser correlativo
--JHD-20150608-Se agrego campo CoPrefijo
--JHD-20150911-Nuevo campo @CoTipoFondoFijo char(2)='',
--JHD-20150930-NuCodigo_ER=case when @CoTipoFondoFijo ='ER' THEN  'ER'+isnull((SELECT NumIdentidad FROM MAUSUARIOS WHERE IDUsuario=@CoUserResponsable),'')/*+'-'+@NuCodigo2*/ else '' end
CREATE PROCEDURE [dbo].[MAFONDOFIJO_Upd]
	@NuFondoFijo int=0,
	@NuCodigo varchar(14)='',
	@CoCeCos char(6)='',
	@CtaContable varchar(14)='',
	@Descripcion varchar(255)='',
	@CoUserResponsable char(4)='',
	@CoMoneda char(3)='',
	@SSMonto numeric=0,
	--
	@CoPrefijo char(4)='',
	@CoTipoFondoFijo char(2)='',
	@UserMod char(4)=''
AS
BEGIN
	Update MAFONDOFIJO
	Set
		--NuCodigo = Case When Ltrim(Rtrim(@NuCodigo))='' Then Null Else @NuCodigo End,  
 		CoCeCos = Case When Ltrim(Rtrim(@CoCeCos))='' Then Null Else @CoCeCos End,  
 		CtaContable = Case When Ltrim(Rtrim(@CtaContable))='' Then Null Else @CtaContable End,  
 		Descripcion = Case When Ltrim(Rtrim(@Descripcion))='' Then Null Else @Descripcion End,  
 		CoUserResponsable = Case When Ltrim(Rtrim(@CoUserResponsable))='' Then Null Else @CoUserResponsable End,  
 		CoMoneda = Case When Ltrim(Rtrim(@CoMoneda))='' Then Null Else @CoMoneda End,  
 		SSMonto = Case When @SSMonto=0 Then Null Else @SSMonto End,   
		SsSaldo = Case When SsSaldo=SSMonto Then @SSMonto Else SsSaldo End,   
		CoPrefijo=Case When Ltrim(Rtrim(@CoPrefijo))='' Then Null Else @CoPrefijo End,  
		CoTipoFondoFijo=Case When Ltrim(Rtrim(@CoTipoFondoFijo))='' Then Null Else @CoTipoFondoFijo End,
 		UserMod = @UserMod,
 		FecMod = getdate(),
		NuCodigo_ER=case when @CoTipoFondoFijo ='ER' THEN  'ER'+isnull((SELECT NumIdentidad FROM MAUSUARIOS WHERE IDUsuario=@CoUserResponsable),'')/*+'-'+@NuCodigo2*/ else '' end
 	Where 
		NuFondoFijo = @NuFondoFijo
END;
