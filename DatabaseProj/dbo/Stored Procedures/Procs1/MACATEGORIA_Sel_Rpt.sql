﻿CREATE procedure [dbo].[MACATEGORIA_Sel_Rpt]
@Descripcion char(10)
As
	Set NoCount on
	
	SELECT IDCat,Descripcion 
	FROM MACATEGORIA
	where (Descripcion like '%'+LTRIM(RTRIM(@Descripcion))+'%' or 
	LTRIM(RTRIM(@Descripcion))='')
