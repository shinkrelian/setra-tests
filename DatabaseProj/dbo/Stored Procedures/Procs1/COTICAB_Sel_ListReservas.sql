﻿
--JRF-04.02.2013- Mostrar el correo del Eje. de Ventas.                
--JRF-Agregrar el fitro de Estado              
--JRF-20130301-Mostrando el Estado.            
--JRF-20130502-Mostrando la Fec. De Inicio de Los Servicios.          
--JRF-20131017-Agregando Filtro de Fechas Inicio de Cotizacion.      
--JRF-20131018-Agregando Filtro de Fechas por asignación de ejecutivo de Reservas.      
--JRF-20140325- Agregnado la columna DocWordVersion    
--JRF-HLF-20140514- Optimizar el --exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB x El IDCAB in (...)  
--JRF-20140519-Agregar la fecha de Asignación de Reservas
--JHD-20150716-Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,
--JHD-20150716- ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes,    
--JHD-20150716- uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe    
--HLF-20151203-And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)    
--HLF-20160216-isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt, Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt
CREATE Procedure dbo.COTICAB_Sel_ListReservas                        
 @IDUsuario char(4),                        
 @Cotizacion char(8),                        
 @IDFile char(8),                        
 @DescCliente varchar(60),                        
 @Titulo varchar(100),                       
 @FilesAntiguos bit,                        
 @UsuarioNoAsignado bit ,                     
 @NombrePax varchar(100),              
 @Estado char(1),      
 @FecIniciRango1 smalldatetime,                      
 @FecIniciRango2 smalldatetime,      
 @FecAsigReserRango1 smalldatetime,      
 @FecAsigReserRango2 smalldatetime,
 @FlHistorico bit=1
As                        
 Set NoCount On                        
 Select UsuarioVen,UsuarioRes,IDUsuario,CorreoEjecReservas,CorreoEjecVenta,IDUsuarioOper,UsuarioOpe,Fecha,FechaAsigReserva,Cotizacion,      
 IDFile,NuPedido,NuPedInt,DescCliente,Titulo,DocWordVersion,Ruta,Estado,IDCAB,NroPax,FecEntregaVentas       
 from (       
 --Select uv.Nombre As UsuarioVen,                  
 Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,     
 --ur.Nombre as UsuarioRes,        
 ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes, 
  ur.IDUsuario As IDUsuario,                       
  isnull(ur.Correo,'') as CorreoEjecReservas,                        
 isnull(uv.Correo,'') As CorreoEjecVenta,                
  uo.IDUsuario as IDUsuarioOper ,
  --uo.Nombre as UsuarioOpe                        
   uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe 
 , Convert(varchar(10),c.FecInicio,103) as Fecha,  
 --Case When FechaAsigReserva is null then CAST('01/01/1900' as smalldatetime) else Cast(convert(char(10),FechaAsigReserva,103) as smalldatetime) End As FechaAsigReserva,             
 c.FechaAsigReserva,
 c.Cotizacion,c.IDFile,              
  isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt,          
 cl.RazonComercial as DescCliente, c.Titulo,                        
 '' As Ruta,                           
 c.Estado as Estado,                        
 c.IDCAB,                       
 c.NroPax        ,      
 CAST(c.DocWordVersion as varchar(2)) as DocWordVersion    ,c.FechaRetornoReservas as FecEntregaVentas
 From COTICAB c                         
 Left Join MAUSUARIOS uv on c.IDUsuario=uv.IDUsuario                        
 Left Join MAUSUARIOS ur On c.IDUsuarioRes = ur.IDUsuario                        
 Left Join MAUSUARIOS uo on c.IDUsuarioOpe = uo.IDUsuario                        
 Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente   
 Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt                                  
 Where                         
 (c.Cotizacion Like '%'+ltrim(rtrim(@Cotizacion))+'%' Or ltrim(rtrim(@Cotizacion))='') And                        
 (c.IDUsuarioRes=@IDUsuario Or ltrim(rtrim(@IDUsuario))='') And                        
 (ltrim(rtrim(@IDFile))='' Or c.IDFile like '%' + ltrim(rtrim(@IDFile)) + '%') And                              
 --Not c.IDFile Is Null And                        
 (cl.RazonSocial Like '%'+@DescCliente+'%' Or ltrim(rtrim(@DescCliente))='') And                        
 (c.Titulo Like '%'+@Titulo+'%' Or ltrim(rtrim(@Titulo))='') And                         
 (c.Estado=@Estado Or ltrim(rtrim(@Estado))='')              
             
 And (c.FechaReserva < GETDATE() Or @FilesAntiguos=0)                        
 And (@UsuarioNoAsignado=0 OR c.IDUsuarioRes='0000')                        
 --exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB                       
 And c.IDCAB In (select px.IDCab from COTIPAX px Where px.IDCab=c.IDCAB                       
 And (px.Nombres+' '+px.Apellidos) Like '%'+@NombrePax+'%' Or ltrim(rtrim(@NombrePax))='')                     
 And ((LTRIM(rtrim(convert(char(10),@FecIniciRango1,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecIniciRango2,103)))='01/01/1900') Or      
   cast(convert(char(10),c.FecInicio,103) as smalldatetime) between @FecIniciRango1 and @FecIniciRango2)
     And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)
) as X      
Where ((LTRIM(rtrim(convert(char(10),@FecAsigReserRango2,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecAsigReserRango2,103)))='01/01/1900') Or      
  cast(convert(char(10),X.FechaAsigReserva,103) as smalldatetime) between @FecAsigReserRango1 and @FecAsigReserRango2)      
 Order by cast(X.Fecha as smalldatetime),X.Cotizacion      



