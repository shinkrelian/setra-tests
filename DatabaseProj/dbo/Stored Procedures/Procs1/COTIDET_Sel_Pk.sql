﻿

--HLF-20130628-Quitando parametro @IDCab
--Agregando campos p.IDTipoProv, s.Dias,sd.ConAlojamiento, sd.PlanAlimenticio
--HLF-20130829-Agregando  subquery IDReserva_Det
CREATE Procedure COTIDET_Sel_Pk
	--@IDCab int,
	@IDDet int
As
	Set NoCount On
	Select cd.*, p.IDTipoProv, s.Dias,sd.ConAlojamiento, sd.PlanAlimenticio,
	isnull((Select Top 1 isnull(IDReserva_Det,0) From RESERVAS_DET Where IDDet=cd.IDDET),0) as IDReserva_Det
	From COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor 
	Left Join MASERVICIOS s On cd.IDServicio=s.IDServicio
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	Where 
	--IDCAB = @IDCab And 
	cd.IDDET =@IDDet


