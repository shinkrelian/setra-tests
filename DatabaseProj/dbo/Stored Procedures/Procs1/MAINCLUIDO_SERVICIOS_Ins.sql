﻿Create Procedure dbo.MAINCLUIDO_SERVICIOS_Ins
@IDIncluido int,
@IDServicio char(8),
@UserMod char(4)
As
Set NoCount On
Insert into MAINCLUIDO_SERVICIOS(IDIncluido,IDServicio,UserMod,FecMod)
	   values (@IDIncluido,@IDServicio,@UserMod,GetDate())
