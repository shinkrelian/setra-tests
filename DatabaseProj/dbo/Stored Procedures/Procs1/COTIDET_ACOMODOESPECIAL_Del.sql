﻿
CREATE PROCEDURE [COTIDET_ACOMODOESPECIAL_Del]
	@IDDet INT,
	@CoCapacidad CHAR(2)
AS
BEGIN
-- =====================================================================
-- Author	   : Dony Tinoco
-- Create date : 02.12.2013
-- Description : SP que elimina el acomodo especial
-- =====================================================================
	SET NOCOUNT ON
  
	DELETE FROM COTIDET_ACOMODOESPECIAL WHERE IDDet = @IDDet And CoCapacidad = @CoCapacidad
END
