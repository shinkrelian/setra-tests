﻿Create Procedure DOCUMENTO_DET_TEXTO_Ins
@NuDocum char(10),
@IDTipoDoc char(3),
@NoTexto varchar(Max),
@SsTotal numeric(8,2),
@UserMod char(4)
As
Set NoCount On
Declare @NuDetalle tinyint=isnull((Select MAX(NuDetalle) From DOCUMENTO_DET_TEXTO     
  Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc),0)+1      

INSERT INTO [dbo].[DOCUMENTO_DET_TEXTO]
           ([NuDocum]
           ,[IDTipoDoc]
           ,[NuDetalle]
           ,[NoTexto]
           ,[SsTotal]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@NuDocum
           ,@IDTipoDoc
           ,@NuDetalle
           ,@NoTexto
           ,@SsTotal
           ,@UserMod
           ,Getdate())
