﻿Create Procedure dbo.COTICAB_COSTOSTC_Upd
@NuCosto tinyint,
@IDCab int,
@QtDesde smallint,
@QtHasta smallint,
@SsPrecio numeric(8,2),
@UserMod char(4)
As
Set NoCount On
UPDATE [dbo].[COTICAB_COSTOSTC]
   SET [QtDesde] = @QtDesde
      ,[QtHasta] = @QtHasta
      ,[SsPrecio] = @SsPrecio
      ,[UserMod] = @UserMod
      ,[FecMod] = Getdate()
 WHERE IDCab =@IDCab and NuCosto=@NuCosto
