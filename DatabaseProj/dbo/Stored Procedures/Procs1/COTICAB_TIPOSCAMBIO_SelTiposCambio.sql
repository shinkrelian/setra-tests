﻿Create Procedure dbo.COTICAB_TIPOSCAMBIO_SelTiposCambio
as
Set NoCount On
select 0 as IDCab,tcu.CoMoneda,mo.Descripcion as DescMoneda,mo.Simbolo as Simb
,tcu.SsTipCam,ub.IDubigeo as IDPais
from  MATIPOSCAMBIO_USD tcu  
Left Join MAMONEDAS mo On tcu.CoMoneda = mo.IDMoneda
Left Join MAUBIGEO ub On mo.IDMoneda = ub.CoMoneda
