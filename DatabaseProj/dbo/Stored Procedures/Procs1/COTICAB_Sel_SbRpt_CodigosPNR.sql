﻿Create Procedure dbo.COTICAB_Sel_SbRpt_CodigosPNR
@IDCab int
As
Set NoCount On
Begin
if exists(select CodReservaTransp from COTITRANSP_PAX ctp Left Join COTIPAX cp On ctp.IDPax =cp.IDPax  
		  where IDTransporte In (select ID from COTIVUELOS where IDCAB = @IDCab) and CodReservaTransp is not null)
Begin
	select distinct cp.Apellidos+' '+cp.Nombres as Pax,CodReservaTransp as CodReserva
	from COTITRANSP_PAX ctp Left Join COTIPAX cp On ctp.IDPax =cp.IDPax  
	where IDTransporte In (select ID from COTIVUELOS where IDCAB = @IDCab)
	--and CONVERT(char(10),Fec_Salida,103) = CONVERT(char(10),@Dia,103) )  
	and CodReservaTransp is not null  
	--group by cp.Apellidos,CodReservaTransp  

End
else
Begin
	select distinct cp.Apellidos+' '+cp.Nombres As Pax,cv.CodReserva
	from COTIVUELOS cv Inner Join COTITRANSP_PAX ct On cv.ID=ct.IDTransporte
	Inner Join COTIPAX cp On cp.IDPax=ct.IDPax
	Where cv.IDCAB=@IDCab and cv.TipoTransporte = 'V' And IsNull(cv.CodReserva,'') <> ''
End
End
