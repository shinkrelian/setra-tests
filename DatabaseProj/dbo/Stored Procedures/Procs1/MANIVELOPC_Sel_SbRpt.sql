﻿CREATE Procedure [dbo].[MANIVELOPC_Sel_SbRpt] 
@IDNivel char(3)
as
	Set NoCount On
	
	select *
	from (
	SELECT row_number() over (order by Usuario) as NroFila,Y.*
		from(
		select IDNivel,u.Usuario,u.Nombre, u.Correo
		from MAUSUARIOS u Left Join MANIVELUSUARIO nu On u.Nivel=nu.IDNivel
	where Nivel=@IDNivel) as Y) as X
