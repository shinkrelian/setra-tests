﻿
--HLF-20150922-@SsComisionBanco
CREATE Procedure dbo.INGRESO_DEBIT_MEMO_Upd
@NuAsignacion int,
@NuIngreso int,
@IDDebitMemo char(10),
@SsPago numeric(10,2),
@SsGastosTransferencia numeric(10,2),
@SsComisionBanco numeric(8,2),
@SsSaldoNuevo numeric(10,2),
@UserMod char(4)
As
Begin
	Set NoCount On
	Update INGRESO_DEBIT_MEMO
		Set SsPago = @SsPago,
			SsGastosTransferencia = @SsGastosTransferencia,
			NuAsignacion = @NuAsignacion,
			SsSaldoNuevo = @SsSaldoNuevo,
			SsComisionBanco= Case When @SsComisionBanco=0 Then Null Else @SsComisionBanco End,
			UserMod = @UserMod,
			FecMod = GETDATE()
	Where NuIngreso = @NuIngreso and IDDebitMemo = @IDDebitMemo 
End

