﻿CREATE Procedure [dbo].[COTIDET_PAX_Ins_CopiaIDDet]
	@IDDet	int,
	@IDDETCopia int,	
	@UserMod	char(4)
As
	Set NoCount On
	
	Insert into COTIDET_PAX(IDDet,IDPax,UserMod)
	Select @IDDet,IDPax,@UserMod From COTIDET_PAX Where IDDet=@IDDETCopia
