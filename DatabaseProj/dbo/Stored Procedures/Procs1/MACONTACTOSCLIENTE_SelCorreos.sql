﻿
--DEBIT_MEMO_GenPDFIndividual 53,''

--SELECT * FROM MACLIENTES
-------- 02.01.2013 - 
--Go
Create Procedure MACONTACTOSCLIENTE_SelCorreos
@IDCLiente varchar(6)
As
	Set NoCount On
	select DISTINCT Email As Correo
	from MACONTACTOSCLIENTE 
	Where Email IS NOT NULL AND
	IDCliente = @IDCLiente
	
