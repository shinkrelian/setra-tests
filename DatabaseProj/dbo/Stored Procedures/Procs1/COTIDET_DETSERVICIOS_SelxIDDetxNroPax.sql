﻿--JRF-20150302-Definir el Sup. Simple y Triple dependiendo de la edición de cotización (Case When cd.FlSTCREditado=1...)  
--JRF-20150330-Definir el CostoReal De Ventas en lugar no tenga.
--JRF-20150411-Agregar Filtro x @IDServicio_Det
--JRF-20151022-Agregar NroPax Cotizacion
CREATE Procedure dbo.COTIDET_DETSERVICIOS_SelxIDDetxNroPax      
@IDDet int,   
@IDServicio_Det int,   
@NroPax tinyint,      
@IDMoneda char(3)      
As      
Begin
Set NoCount On 
Declare @SevicioTC bit = IsNull((select FlServicioTC from COTIDET where IDDET=@IDDet),0)

if @SevicioTC = 0
Begin
	Select *,0 as MontoTourConductor,Cast(0 as bit) as EsCostoTC from (     
		select  cds.IDServicio_Det,sd.CoMoneda,Case When cd.CostoRealEditado=1 And cd.CostoReal=0 then 0 Else IsNull(sd.Monto_sgl,0) End as MontoReal,
		   IsNull(sd.Monto_sgls,0) as MontoSegunTC,      
		   Case When cd.CostoRealEditado=1 And cd.CostoReal=0 Then 0 Else
		   Case When Not Exists((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta))
			Then 
		   IsNull((select Top(1) sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det Order By sdc.PaxHasta Desc),0)
		   Else
		   IsNull((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) End End
		   as MontoCostoReal, 
		   Case When Not Exists((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta)) 
			Then
		   IsNull((select Top(1) sdc.PaxHasta from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det Order By sdc.PaxHasta Desc),0)
		   Else
		   IsNull((select sdc.PaxHasta from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) End
		   as PaxHastaCostoReal,
		   Isnull(IsNull(sd.TC,(select tc.SsTipCam from MATIPOSCAMBIO_USD tc where CoMoneda=@IDMoneda)),0) as TC,      
		   sd.Afecto,sd.PoliticaLiberado,IsNull(sd.Liberado,0) CantMinLiberada,IsNuLL(sd.TipoLib,'') As TipoLib,      
		   IsNull(sd.MaximoLiberado,0) as CantidadLiberada,IsNull(sd.MontoL,0) as MontoLiberado,      
		   Case When cd.FlSSCREditado=1 Then cd.SSCR Else IsNull(sd.DiferSS,0) End as DiferSS,  
		   Case When cd.FlSTCREditado=1 Then cd.STCR Else IsNull(sd.DiferST,0) End as DiferST,    
		   sd.Descripcion as ServicioCot,p.NombreCorto as ProveeCot,sdRel.Tipo as VarianteCot    ,
		   cd.NroPax,cd.NroLiberados,CDS.IDDET,
		   (select count(*) from COTIPAX cp Where cp.IDCab=c.IDCAB And cp.FlNoShow=0) as NroPaxCoti
		from COTIDET_DETSERVICIOS cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det      
		Left Join MASERVICIOS_DET sdRel On sd.IDServicio_Det_V= sdRel.IDServicio_Det    
		Left Join MASERVICIOS s On sdRel.IDServicio=s.IDServicio    
		Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor    
		Inner Join COTIDET cd On cds.IDDET=cd.IDDET  
		Inner Join COTICAB c On cd.IDCAB=c.IDCAB
		where cds.IDDET=@IDDet and (@IDServicio_Det=0 Or cds.IDServicio_Det=@IDServicio_Det)
		) as X
		Where (X.MontoReal+X.MontoSegunTC+X.MontoCostoReal <> 0 Or
		dbo.FnDevuelveCantidadPorPagar(X.IDServicio_Det,X.IDDET)<> (X.NroPax+IsNuLL(X.NroLiberados,0)))
End
Else
Begin
	Select *,Cast(1 as bit) as EsCostoTC from (
		select  cds.IDServicio_Det,sd.CoMoneda,Round(cds.CostoReal*cd.NroPax,0) as MontoReal,IsNull(sd.Monto_sgls,0) as MontoSegunTC,      
		   Case When Not Exists((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta))
			Then
		   IsNull((select Top(1) sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det Order By sdc.PaxHasta Desc),0)
		   Else
		   IsNull((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) End
		   as MontoCostoReal, 
		   Case When Not Exists((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta))
			Then
		   IsNull((select Top(1) sdc.PaxHasta from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det Order By sdc.PaxHasta Desc),0)
		   Else
		   IsNull((select sdc.PaxHasta from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det       
		   and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) End
		   as PaxHastaCostoReal,
		   Isnull(IsNull(sd.TC,(select tc.SsTipCam from MATIPOSCAMBIO_USD tc where CoMoneda=@IDMoneda)),0) as TC,      
		   sd.Afecto,sd.PoliticaLiberado,IsNull(sd.Liberado,0) CantMinLiberada,IsNuLL(sd.TipoLib,'') As TipoLib,      
		   IsNull(sd.MaximoLiberado,0) as CantidadLiberada,IsNull(sd.MontoL,0) as MontoLiberado,      
		   Case When cd.FlSSCREditado=1 Then cd.SSCR Else IsNull(sd.DiferSS,0) End as DiferSS,  
		   Case When cd.FlSTCREditado=1 Then cd.STCR Else IsNull(sd.DiferST,0) End as DiferST,    
		   sd.Descripcion as ServicioCot,p.NombreCorto as ProveeCot,sdRel.Tipo as VarianteCot    ,
		   cd.NroPax,cd.NroLiberados,CDS.IDDET,
		   (select count(*) from COTIPAX cp Where cp.IDCab=c.IDCAB And cp.FlNoShow=0) as NroPaxCoti,
		   Round(cds.CostoReal,2) as MontoTourConductor
		from COTIDET_DETSERVICIOS cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det      
		Left Join MASERVICIOS_DET sdRel On sd.IDServicio_Det_V= sdRel.IDServicio_Det    
		Left Join MASERVICIOS s On sdRel.IDServicio=s.IDServicio    
		Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor    
		Inner Join COTIDET cd On cds.IDDET=cd.IDDET  
		Inner Join COTICAB c On cd.IDCAB=c.IDCAB
		where cds.IDDET=@IDDet and (@IDServicio_Det=0 Or cds.IDServicio_Det=@IDServicio_Det)
		) as X
		--where x.MontoCostoReal > 0
End
End
