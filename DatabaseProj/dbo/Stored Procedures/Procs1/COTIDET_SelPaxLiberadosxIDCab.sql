﻿--JRF-20140929-Agregar si es CostoRealEditado
CREATE Procedure dbo.COTIDET_SelPaxLiberadosxIDCab
 @IDCab int  
As  
 Set NoCount On  
   
 Select IDDET, NroPax, IsNull(NroLiberados,0) as NroLiberados,CostoRealEditado
 From COTIDET Where IDCAB=@IDCab  
