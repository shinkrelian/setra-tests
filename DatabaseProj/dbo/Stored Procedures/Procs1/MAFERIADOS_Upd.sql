﻿CREATE PROCEDURE [dbo].[MAFERIADOS_Upd]
@FechaFeriado date,
@IDPais char(6),
@Descripcion varchar(100),
@UserMod char(4)
as
begin
	set nocount on
	update dbo.MAFERIADOS
		set Descripcion=@Descripcion,
			UserMod=@UserMod,
			FecMod=GETDATE()
		where FechaFeriado=@FechaFeriado and IDPais=@IDPais 
end
