﻿Create Procedure dbo.COTICAB_UpdFlHistorico 
	@IDCab int,
	@FlHistorico bit,
	@UserMod char(4)
As

	Update COTICAB Set FlHistorico=@FlHistorico, UserMod=@UserMod, FecMod=GETDATE()
	Where IDCAB=@IDCab