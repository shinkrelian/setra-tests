﻿CREATE Procedure [dbo].[MACOMENTARIOSPROVEEDOR_Ins]
	@IDComentario char(3),
	@IDProveedor	char(6),	
	@IDFile	char(8),
	@Comentario	text,
	@Estado	char(1),
	@UserMod	char(4)	
As
	Set NoCount On
	
	--Declare @IDComentario char(3), @tiIDComentario tinyint
	--Select @tiIDComentario = IsNull(MAX(IDComentario),0)+1 From MACOMENTARIOSPROVEEDOR Where IDProveedor=@IDProveedor	

	--Set @IDComentario=@tiIDComentario
	--Set @IDComentario=REPLICATE('0',3-LEN(@IDComentario))+@IDComentario
	
	Insert Into MACOMENTARIOSPROVEEDOR
	(IDComentario, IDProveedor, IDFile, Comentario, Estado, UserMod)
	Values(@IDComentario, @IDProveedor, 
	Case When LTRIM(rtrim(@IDFile))='' Then Null Else @IDFile End, 
	@Comentario, @Estado, @UserMod)
