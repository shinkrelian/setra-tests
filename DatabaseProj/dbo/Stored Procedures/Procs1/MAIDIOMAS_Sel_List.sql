﻿CREATE Procedure [dbo].[MAIDIOMAS_Sel_List]
	@IDIdioma varchar(12)
As	
	Set NoCount On

	Select IDIdioma 
	From MAIDIOMAS Where (IDIdioma Like '%'+@IDIdioma+'%'
	Or ltrim(rtrim(@IDIdioma))='')
	And Activo='A'
