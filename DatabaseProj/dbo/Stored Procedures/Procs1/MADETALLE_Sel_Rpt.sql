﻿create Procedure [dbo].[MADETALLE_Sel_Rpt]
	@Descripcion text
As
	Set NoCount On
	Declare @vcDescripcion	varchar(MAX)=Cast(@Descripcion as varchar(max))
	
	Select u.Usuario ,Descri, IDIdioma, IDDetalle
	From MADETALLE md Left Join MAUSUARIOS u On md.IDUsuario=u.IDUsuario
	Where (Descripcion Like '%'+@vcDescripcion+'%' Or LTRIM(RTRIM(@vcDescripcion))='')
