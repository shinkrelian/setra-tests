﻿--JRF-20140317-Ordenar las columnas xTipo xPorDefecto xOrden  
--JRF-20150304-Agregar columna FlAutomatico
CREATE Procedure [dbo].[MAINCLUIDO_Sel_List]     
 @IDIncluido int,    
 @DescAbrev varchar(100),    
 @Tipo char(1)   
As    
 Set NoCount On    
 Select FlAutomatico,PorDefecto,    
 IDIncluido,    
 case Tipo when 'I' then 'Incluido'     
     when 'N' then 'No Incluido'     
     when 'O' then 'Observación'     
 End as Tipo,    
 DescAbrev    
 from MAINCLUIDO    
 Where (DescAbrev like '%'+@DescAbrev+'%' Or LTRIM(rtrim(@DescAbrev))='')    
 And (IDIncluido=@IDIncluido Or @IDIncluido = 0) And     
 (Tipo=@Tipo Or LTRIM(RTRIM(@Tipo))='')    
 Order by Tipo,PorDefecto desc,Orden  
