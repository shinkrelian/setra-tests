﻿Create Procedure dbo.ASIGNACION_Ins
@TxObservacion varchar(max),
@SsImportePagado numeric(10,2),
@pNuAsignacion int output,
@UserMod char(4)
As
Begin
	Set NoCount On 
	Declare @NuAsignacion int
	Execute dbo.Correlativo_SelOutput 'ASIGNACION',1,10,@NuAsignacion output
	Insert Into ASIGNACION (NuAsignacion,FeAsignacion,TxObservacion,SsImportePagado,UserMod,FecMod)
					values (@NuAsignacion,GETDATE(),@TxObservacion,@SsImportePagado,@UserMod,GETDATE())
	Set @pNuAsignacion = @NuAsignacion
End
