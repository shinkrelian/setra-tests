﻿
--Execute COTIDET_QUIEBRES_Sel_TodosxIDCab 43
--Go

Create Procedure COTIDET_DETSERVICIOS_Sel_TodosxIDCab
@IDCab int
As
	Set NoCount On
	Select * from COTIDET_DETSERVICIOS 
	where IDDET	In (select IDDET from COTIDET where IDCAB = @IDCab)
