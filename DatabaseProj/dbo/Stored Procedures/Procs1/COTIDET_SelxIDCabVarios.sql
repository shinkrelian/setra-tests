﻿--JRF-20160126-Agregar la columna IsNull(sd.IDServicio_Det_V,0) as IDServicio_Det_V
CREATE Procedure dbo.COTIDET_SelxIDCabVarios
@IDCabVarios int
As
Set NoCount On
Select distinct cd.IDDET, sd.IDServicio,sd.Descripcion as Servicio,s.IDProveedor,s.IDubigeo,
	   sd.Anio,sd.Tipo as Variante,sd.IDServicio_Det,IsNull(sd.IDServicio_Det_V,0) as IDServicio_Det_V
from COTIDET cd Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det 
	 Left Join MASERVICIOS s On sd.IDServicio = s.IDServicio
where s.IDCabVarios = @IDCabVarios and IDCAB = @IDCabVarios
