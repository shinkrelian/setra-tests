﻿--HLF-20140424-Campo NuDocum, Group by NuDocum, And CoEstado='GD'      
--HLF-20140507-Agregando IDTipoDoc 
--HLF-20140530-Agregando FeDocum
--HLF-20151124-- -isnull((Select sum(SsTotalDocumUSD) From DOCUMENTO Where IDTipoDoc='NCA' ...		
CREATE Procedure dbo.ASIGNACION_SelAnticipos        
 @IDCab int        
As        
 Set Nocount On        
      
 --SELECT sum(idm.SsPago) AS SsPago        
 --FROM ASIGNACION asg         
 --Inner Join INGRESO_DEBIT_MEMO idm On asg.NuAsignacion=idm.NuAsignacion        
 --Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo And dm.IDCab=@IDCab        
 --Inner Join COTICAB cc On dm.IDCab=cc.IDCAB And asg.FeAsignacion < cc.FechaOut        
      
 Select IDTipoDoc,NuDocum,FeDocum,sum(SsTotalDocumUSD)
	-isnull((Select sum(SsTotalDocumUSD) From DOCUMENTO Where IDTipoDoc='NCA' 
		and IDTipoDocVinc=d.IDTipoDoc 
		and NuDocumVinc=d.NuDocum and CoEstado='GD'
		),0) AS SsPago       
 From DOCUMENTO d      
 Where IDCab=@IDCab And       
 IDTipoDoc='BOL' And LEFT(NuDocum,3)='005'     
 --And CoEstado='PD'      
 And CoEstado='GD'      
 Group by IDTipoDoc,NuDocum,FeDocum    

