﻿Create Procedure dbo.MAINCLUIDOOPERACIONES_Sel_Lvw
 @NuIncluido int,  
 @DescAbrev varchar(100)  
as  
  
 Set NoCount On  
   
 Select NoObserAbrev  
 from MAINCLUIDOOPERACIONES 
 Where (NoObserAbrev like '%'+@DescAbrev+'%' Or LTRIM(rtrim(@DescAbrev))='')  
 And (NuIncluido=@NuIncluido Or @NuIncluido = 0)  
