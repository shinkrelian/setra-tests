﻿
--HLF-20150929-Case When DOCUMENTO.SsSaldoMatchAsignac>0...
CREATE Procedure dbo.DOCUMENTO_UpdSsSaldoMatchAsignac
	@NuAsignacion int,
	@IDDebitMemo char(10),
	@UserMod char(4)
As
	Set Nocount on
	Update DOCUMENTO Set DOCUMENTO.SsSaldoMatchAsignac=isnull(DOCUMENTO.SsSaldoMatchAsignac,0)+
	--ad.SsMonto,
		Case When DOCUMENTO.SsTotalDocumUSD>0 then   --positivo
			Abs(ad.SsMonto)
		Else
			ad.SsMonto
		End,
	FecMod=GETDATE(), DOCUMENTO.UserMod=@UserMod
	From DOCUMENTO d Inner Join ASIGNACION_DOCUMENTO ad On d.IDTipoDoc=ad.IDTipoDoc And d.NuDocum=ad.NuDocum 
	And ad.IDDebitMemo=@IDDebitMemo And ad.NuAsignacion=@NuAsignacion
	Inner Join DOCUMENTO On d.IDTipoDoc=DOCUMENTO.IDTipoDoc And d.NuDocum=DOCUMENTO.NuDocum 

