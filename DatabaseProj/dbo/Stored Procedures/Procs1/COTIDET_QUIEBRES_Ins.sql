﻿--JRF-20120926-- Nuevo Campo - RedondeoTotal  
--JRF-20140227-- Extensión de los decimales
CREATE Procedure [dbo].[COTIDET_QUIEBRES_Ins]    
 @IDCAB_Q int,    
 @IDDET int,    
 @CostoReal numeric(12,4),    
 @CostoLiberado numeric(12,4),    
 @Margen numeric(12,4),
 @MargenAplicado numeric(6,2),    
 @MargenLiberado numeric(6,2),    
 @Total numeric(8,2) ,    
 @RedondeoTotal numeric(12,4)= 0,  
 @SSCR numeric(12,4),    
 @SSCL numeric(12,4),    
 @SSMargen numeric(12,4),    
 @SSMargenLiberado numeric(12,4),    
 @SSTotal numeric(12,4),    
 @STCR numeric(12,4),    
 @STMargen numeric(12,4),    
 @STTotal numeric(12,4),    
 @CostoRealImpto numeric(12,4),    
 @CostoLiberadoImpto numeric(12,4),    
 @MargenImpto numeric(12,4),    
 @MargenLiberadoImpto numeric(12,4),    
 @SSCRImpto numeric(12,4),    
 @SSCLImpto numeric(12,4),    
 @SSMargenImpto numeric(12,4),    
 @SSMargenLiberadoImpto numeric(12,4),    
 @STCRImpto numeric(12,4),    
 @STMargenImpto numeric(12,4),    
 @TotImpto numeric(12,4),     
 @UserMod char(4)    
As    
 Set Nocount On    
     
 INSERT INTO COTIDET_QUIEBRES    
           (IDCAB_Q    
   ,IDDET     
           ,CostoReal    
           ,CostoLiberado    
           ,Margen    
           ,MargenAplicado    
           ,MargenLiberado    
           ,Total   
           ,RedondeoTotal   
           ,SSCR    
           ,SSCL    
           ,SSMargen    
           ,SSMargenLiberado    
           ,SSTotal    
           ,STCR    
           ,STMargen    
           ,STTotal    
           ,CostoRealImpto    
           ,CostoLiberadoImpto    
           ,MargenImpto    
           ,MargenLiberadoImpto    
           ,SSCRImpto    
           ,SSCLImpto    
           ,SSMargenImpto    
           ,SSMargenLiberadoImpto    
           ,STCRImpto    
           ,STMargenImpto    
           ,TotImpto                          
           ,UserMod)    
     VALUES    
           (@IDCAB_Q ,    
   @IDDET ,               
           @CostoReal ,    
           @CostoLiberado ,    
           @Margen ,    
           @MargenAplicado ,    
           @MargenLiberado ,    
           @Total ,  
           @RedondeoTotal ,    
           @SSCR ,    
           @SSCL ,    
           @SSMargen ,    
           @SSMargenLiberado ,    
           @SSTotal ,    
           @STCR ,    
           @STMargen ,    
           @STTotal ,    
           @CostoRealImpto ,    
           @CostoLiberadoImpto ,    
           @MargenImpto ,    
           @MargenLiberadoImpto ,    
           @SSCRImpto ,    
           @SSCLImpto ,    
           @SSMargenImpto ,    
           @SSMargenLiberadoImpto ,    
           @STCRImpto ,    
           @STMargenImpto ,    
           @TotImpto ,    
           @UserMod)    
