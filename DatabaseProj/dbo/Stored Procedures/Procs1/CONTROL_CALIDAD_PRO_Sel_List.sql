﻿
--JHD-20141229- Se agrego el filtro de cliente  
--JRF-20150216- Se agrego la columna y el Campo de Pax
--JHD-20150626- Se agregaron los campos Cliente y Proveedor
--JHD-20150626-isnull(Cli.RazonComercial,isnull((select razoncomercial from maclientes where idcliente=ccp.cocliente),'')) as Descliente,
--JHD-20150626-isnull(cp.Titulo,'')+' '+isnull(cp.Apellidos,'') + ' ' +isnull(cp.Nombres,'') As NombrePax,
--JHD-20150626-isnull(ccp.CoProveedor,'') as CoProveedor,
		     --isnull((select NombreCorto from MAPROVEEDORES where IDProveedor=ccp.CoProveedor),'') as Proveedor
--JRF-20150819-..Cast(ccp.CoProveedor as varchar(6)...
--HLF-20151207-And (c.FlHistorico=@FlHistorico Or @FlHistorico=1) 
CREATE  Procedure dbo.CONTROL_CALIDAD_PRO_Sel_List  
@IDFile varchar(8),  
@Proveedor varchar(100),  
@IDTipoProv char(3),  
@Cliente varchar(100),
@FlHistorico bit=1  
as  
Set NoCOunt On  
Select X.NuControlCalidad,X.IDCab,X.IDFile,X.Titulo,X.NombrePax,X.EjecutivoVentas,
	   X.EjecutivoOperaciones,X.Descliente,X.FechaOut,X.CoProveedor,X.Proveedor,X.CoCliente  
 from (  
Select ccp.NuControlCalidad,isnull(ccp.IDCab,0) as IDCab,c.IDFile,c.Titulo,uVent.Nombre as EjecutivoVentas,--,uRese.Nombre as EjecutivoReservas,  
uOper.Nombre as EjecutivoOperaciones,
isnull(Cli.RazonComercial,isnull((select razoncomercial from maclientes where idcliente=ccp.cocliente),'')) as Descliente,
c.FechaOut,  
Case When Exists(select NuCuest from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc 
				 Left Join MAPROVEEDORES p On ccpc.IDProveedor=p.IDProveedor  
				 Where ccp.NuControlCalidad=ccpc.NuControlCalidad and 
				 (LTRIM(RTRIM(@Proveedor))='' Or p.NombreCorto Like '%'+@Proveedor+'%') and
				 (LTRIM(rtrim(@IDTipoProv))='' Or p.IDTipoProv=@IDTipoProv))
Then 1 Else 0 End as ExisteProv,
isnull(cp.Titulo,'')+' '+isnull(cp.Apellidos,'') + ' ' +isnull(cp.Nombres,'') As NombrePax,
isnull(Cast(ccp.CoProveedor as varchar(6)),'') as CoProveedor,
isnull((select NombreCorto from MAPROVEEDORES where IDProveedor=ccp.CoProveedor),'') as Proveedor,
isnull(ccp.CoCliente,'') as CoCliente
from CONTROL_CALIDAD_PRO ccp Left Join COTICAB c On ccp.IDCab=c.IDCAB  
Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario  
--Left Join MAUSUARIOS uRese On c.IDUsuarioRes=uRese.IDUsuario  
Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario  
Left Join MACLIENTES Cli On c.IDCliente=Cli.IDCliente  
Left Join COTIPAX cp On ccp.IDPax =cp.IDPax
Where (Ltrim(Rtrim(@IDFile))='' Or c.IDFile Like @IDFile+'%')  
and (Ltrim(Rtrim(@Cliente))='' Or Cli.RazonSocial Like @Cliente+'%')  
And (c.FlHistorico=@FlHistorico Or @FlHistorico=1) 
) as X 
where X.ExisteProv=1

