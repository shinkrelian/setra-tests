﻿CREATE Procedure dbo.MABANCOS_Sel_Cbo_OrdPag 
As  
 Set NoCount On  
 select IDBanco, Case When IDBanco = '000' Then 'NO APLICA' Else Descripcion+' - '+NroCuenta  End As BancoCuenta
   from MABANCOS  
 Order By IDBanco Asc
