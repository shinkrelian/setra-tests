﻿CREATE Procedure [dbo].[COTIPAX_Sel_RptPeruRAIL]
@IdCab int
as
	Set NoCount On
	
	select *
	from (
	  select ROW_NUMBER() over (Order by Nombre) as NroFila,Y.*
	  from (
		select Nombres as Nombre,Apellidos,ti.Descripcion TipoDoc, 
		cp.NumIdentidad NroDoc,u.Gentilicio,
		convert(nchar(10),cp.FecNacimiento,103) as FecNacimiento
	from COTIPAX cp Left Join MATIPOIDENT ti On cp.IDIdentidad= ti.IDIdentidad 
	Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo
	where cp.IDCab=@IdCab) as Y) as X
