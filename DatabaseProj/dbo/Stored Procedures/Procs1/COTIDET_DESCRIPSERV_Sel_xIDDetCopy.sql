﻿
--HLF-20151020-Quitar * From
--Isnulls, nuevas columnas NoHotel,TxWebHotel
--HLF-20160128-TxDireccHotel,TxTelfHotel1,TxTelfHotel2,FeHoraChkInHotel,FeHoraChkOutHotel,CoTipoDesaHotel,CoCiudadHotel
CREATE Procedure dbo.COTIDET_DESCRIPSERV_Sel_xIDDetCopy
@IDDet int
As
	select --*
	iddet, IDIdioma, isnull(DescCorta,'') as DescCorta,
	isnull(DescLarga,'') as DescLarga,
	isnull(NoHotel,'') as NoHotel,
	isnull(TxWebHotel,'') as TxWebHotel,

	isnull(TxDireccHotel,'') as TxDireccHotel,
		isnull(TxTelfHotel1,'') as TxTelfHotel1,
		isnull(TxTelfHotel2,'') as TxTelfHotel2,
		CONVERT(char(5),FeHoraChkInHotel,108) as FeHoraChkInHotel,
		CONVERT(char(5),FeHoraChkOutHotel,108) as FeHoraChkOutHotel,
		isnull(CoTipoDesaHotel,'') as CoTipoDesaHotel,
		isnull(CoCiudadHotel,'') as CoCiudadHotel
	from COTIDET_DESCRIPSERV where IDDet =@IDDet
