﻿Create Procedure COTIVUELOS_SelExisteManual
@IDCab int,
@pExiste bit Output
As    
 Set NoCount On   
 Set @pExiste = 0
 If Exists(select ID from COTIVUELOS Where IDCAB = @IDCab and IdDet=0)
 Begin
	Set @pExiste = 1
 End