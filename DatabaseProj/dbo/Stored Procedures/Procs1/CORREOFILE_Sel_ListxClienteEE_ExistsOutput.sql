﻿
CREATE Procedure [dbo].[CORREOFILE_Sel_ListxClienteEE_ExistsOutput]
	@IDCab int,  
	@CoUsrCliPara	char(6),
	@CoArea char(2),
	@pExists bit output
As   
	Set NoCount On  
   
	Set @pExists = 0

	If Exists(
		Select
		c.NuCorreo
		From .BDCORREOSETRA..CORREOFILE c  
		Where IDCab=@IDCab And   
		(@CoUsrCliPara In (Select CoUsrCliPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE Where NuCorreo=c.NuCorreo) )
		And  CoTipoBandeja='EE'  
		And (c.CoUsrIntDe in (Select IDUsuario From MAUSUARIOS Where IdArea=@CoArea) ))

		Set @pExists = 1

