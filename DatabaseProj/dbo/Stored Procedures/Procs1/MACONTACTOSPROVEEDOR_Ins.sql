﻿
--JHD-20151124-Se agregaron nuevos campos: @Usuario varchar(20)='',@Password varchar(255)=''
--PPMG-20151125-Se valido que no se duplique el usuario.
--JHD-20151126-Se agrego nuevo campo: @FlAccesoWeb bit=0
CREATE PROCEDURE [dbo].[MACONTACTOSPROVEEDOR_Ins]
	@IDProveedor char(6),	
	@IDIdentidad char(3),
	@NumIdentidad varchar(15),
	@Titulo varchar(5),
	@Nombres varchar(80),
	@Apellidos varchar(80),
	@Cargo varchar(100),
	@Utilidad char(3),
	@Telefono varchar(80),
	@Fax varchar(50),
	@Celular varchar(60),
	@Email varchar(100),
	@Anexo varchar(10),
	@FechaNacimiento	smalldatetime,
	@UserMod char(4),
	--
	@Usuario varchar(80)='',
	@Password varchar(255)='',
	@FlAccesoWeb bit=0
AS
BEGIN
	SET @Usuario = ltrim(rtrim(@Usuario))
	IF @Usuario <> ''
	BEGIN
		IF [dbo].[FnExisteUsuarioProvClie]('', @IDProveedor, 'P', @Usuario)=1
		BEGIN
			RAISERROR ('Ya esiste el nombre del usuario a ingresar.', 16, 1);
			RETURN
		END
	END
	Set NoCount On
	Declare @IDContacto char(3), @tiIDContacto tinyint

	Select @tiIDContacto = IsNull(MAX(IDContacto),0)+1 From MACONTACTOSPROVEEDOR Where IDProveedor=@IDProveedor	

	Set @IDContacto=@tiIDContacto
	Set @IDContacto=REPLICATE('0',3-LEN(@IDContacto))+@IDContacto

	INSERT INTO MACONTACTOSPROVEEDOR
           (IDContacto
           ,IDProveedor
           
           ,IDIdentidad
           ,NumIdentidad
           ,Titulo
           ,Nombres
           ,Apellidos
           ,Cargo
           ,Utilidad
           ,Telefono
           ,Fax
           ,Celular
           ,Email
           ,Anexo
           ,FechaNacimiento
           ,UserMod
		   ,Usuario
		   ,[Password]
		   ,FlAccesoWeb
           )
     VALUES
           (@IDContacto,
            @IDProveedor,
            @IDIdentidad,
            @NumIdentidad,
            @Titulo,
            @Nombres,
            @Apellidos,
            @Cargo,
            @Utilidad,
            Case When ltrim(rtrim(@Telefono))='' Then Null Else @Telefono End,
            Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End,
            Case When ltrim(rtrim(@Celular))='' Then Null Else @Celular End,
            Case When ltrim(rtrim(@Email))='' Then Null Else @Email End,
            Case When ltrim(rtrim(@Anexo))='' Then Null Else @Anexo End,
			Case When @FechaNacimiento='01/01/1900' Then Null Else @FechaNacimiento End,
            @UserMod,
			Case When ltrim(rtrim(@Usuario))='' Then Null Else @Usuario End,
			Case When ltrim(rtrim(@Password))='' Then Null Else @Password End,
			@FlAccesoWeb
           )
END;
