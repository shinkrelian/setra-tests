﻿--HLF-20151203-if  CONVERT(SMALLDATETIME,CONVERT(VARCHAR, @fechaout,103))<=CONVERT(SMALLDATETIME,CONVERT(VARCHAR, GETDATE(),103))
CREATE procedure COTICAB_CompararFechaOut
@idcab int,--=8441
@pFecha bit output
as
begin
declare @fechaout smalldatetime=(select FechaOut from coticab where IDCAB=@idcab)
--declare @fechaout smalldatetime=(select FechaOut from coticab where IDCAB=@idcab)
if  CONVERT(SMALLDATETIME,CONVERT(VARCHAR, @fechaout,103))<=CONVERT(SMALLDATETIME,CONVERT(VARCHAR, GETDATE(),103))
	set @pFecha=0
else
	set @pFecha=1
--select @blnfecha
end;
