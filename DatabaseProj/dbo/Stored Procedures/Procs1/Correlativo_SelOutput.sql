﻿
  
    
--HLF-20141202-Agregando parametros @IDTipoDoc, @CoSerie e If ltrim(rtrim(@IDTipoDoc))=''     
--HLF-20140328-Agregando uso de campo IDTipoDoc2  
--HLF-20140416-isnull(Correlativo,0)
--if @vcCorrelativo<>'0'
CREATE Procedure [dbo].[Correlativo_SelOutput]        
	@Tabla varchar(30),        
	@Upd Bit,        
	@Tamanio TinyInt,    
	@pCorrelativo varchar(10) output,    
	@IDTipoDoc char(3)='',    
	@CoSerie char(3)=''     
   
As        
	Set NoCount On        
	Declare @iCorrelativo int, @vcCorrelativo varchar(10)        
  
	If ltrim(rtrim(@IDTipoDoc))=''   
		Begin    
		Select @iCorrelativo=Correlativo+1        
		From Correlativo Where Tabla=@Tabla        

		Set @vcCorrelativo=isnull(@iCorrelativo,0)
		if @vcCorrelativo<>'0'
			Set @vcCorrelativo=REPLICATE('0',@Tamanio-LEN(@vcCorrelativo))+@vcCorrelativo        
		else
			Set @vcCorrelativo=''	

		If @Upd=1        
			Update Correlativo Set Correlativo=@iCorrelativo,        
			FecMod=GETDATE()        
			Where Tabla=@Tabla        
		  
		Set @pCorrelativo=@vcCorrelativo      
		End    
	Else     
		Begin    
		Select @iCorrelativo=Correlativo+1        
		From Correlativo Where Tabla=@Tabla And CoSerie=@CoSerie And (IDTipoDoc=@IDTipoDoc Or IDTipoDoc2=@IDTipoDoc)   

		Set @vcCorrelativo=isnull(@iCorrelativo,0)    
		if @vcCorrelativo<>'0'    
			Set @vcCorrelativo=@CoSerie+REPLICATE('0',(@Tamanio-3)-LEN(@vcCorrelativo))+@vcCorrelativo        
		else
			Set @vcCorrelativo=''	

		If @Upd=1        
			Update Correlativo Set Correlativo=@iCorrelativo,        
			FecMod=GETDATE()        
			Where Tabla=@Tabla And CoSerie=@CoSerie And (IDTipoDoc=@IDTipoDoc Or IDTipoDoc2=@IDTipoDoc)   
			  
		Set @pCorrelativo=@vcCorrelativo      
		End    
      
