﻿
-----------------------------------------------------------------------------------------------------------------------------------------------
--CREATE
--MACONCEPTO_HOTEL_Sel_List '','SER',''

create PROCEDURE MACONCEPTO_HOTEL_Sel_List
	@CoConcepto tinyint=0,
	@CoTipo char(3)='',
	@TxDescripcion varchar(150)=''
AS
BEGIN
	Select
		CoConcepto,
 		TxDescripcion,
 		CoTipo
 		--FlActivo,
 		--UserMod,
 		--FecMod
 	From dbo.MACONCEPTO_HOTEL
	Where 
		(CoConcepto = @CoConcepto or @CoConcepto=0) and 
		(CoTipo = @CoTipo or @CoTipo='') and 
		(@TxDescripcion='' or TxDescripcion like '%'+@TxDescripcion+'%')
END
