﻿

  
CREATE Procedure [Dbo].[COTIDET_PAX_Sel_xIDDet]      
 @IDDet Int    
As    
 Set NoCount On    
     
 Select IDPax, Nombre    
 From    
 (    
 Select cdp.IDPax, Nombres + ' '+ Apellidos As Nombre,    
 Case When ISNUMERIC(SUBSTRING(Apellidos,5,2))=1 Then      
  SUBSTRING(Apellidos,5,2)    
 Else    
  Nombres    
 End as NroNombrePax, Orden    
 From COTIDET_PAX cdp Inner Join COTIPAX cp On cdp.IDPax=cp.IDPax
 Where cdp.IDDet=@IDDet
 --Order By Nombre    
 ) as X    
  Order by Orden  
    
   
