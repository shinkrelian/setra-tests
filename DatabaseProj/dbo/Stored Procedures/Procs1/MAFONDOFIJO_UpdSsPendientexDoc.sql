﻿--JHD-20150611-Se quito el cambiar el estado, puesto que se realizar al reembolsar.
--JHD-20150821-No actualizar estado a Aceptado por BD
CREATE Procedure [dbo].[MAFONDOFIJO_UpdSsPendientexDoc]      
@NuFondoFijo int,      
@SsTotalOrig numeric(9,2),      
@UserMod char(4)      
As      
Set NoCount On      
BEGIN
Update MAFONDOFIJO      
 Set SsSaldo=SsSaldo-@SsTotalOrig,      
  UserMod=@UserMod,      
  FecMod=GETDATE()      
where NuFondoFijo=@NuFondoFijo  
      
Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo)      
  
--if @SsSaldoPend = 0      
 --Update MAFONDOFIJO set CoEstado='AC' where NuFondoFijo=@NuFondoFijo      
END;
