﻿
--------------------------------------------------------------------------------------------------------

--alter
--JHD-20141118-Se agrego Campo IDPais
CREATE PROCEDURE [MACLIENTES_PERFIL_GUIAS_Sel_Ciudades_List]
	@CoCliente char(6)
	
AS
BEGIN

SELECT distinct MAUBIGEO.IDubigeo,
		PAIS.Descripcion AS Pais,
		MAUBIGEO.Descripcion AS Ciudad,
		MACLIENTES_PERFIL_GUIAS.CoCliente,
		PAIS.IDubigeo AS IDPais
FROM         MACLIENTES_PERFIL_GUIAS INNER JOIN
                      MAPROVEEDORES ON MACLIENTES_PERFIL_GUIAS.CoProveedor = MAPROVEEDORES.IDProveedor INNER JOIN
                      MAUBIGEO ON MAPROVEEDORES.IDCiudad = MAUBIGEO.IDubigeo INNER JOIN
                      MAUBIGEO AS PAIS ON MAUBIGEO.IDPais = PAIS.IDubigeo
WHERE     (MACLIENTES_PERFIL_GUIAS.CoCliente = @CoCliente)

END;
