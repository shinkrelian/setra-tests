﻿Create Procedure [dbo].[MAFORMASPAGO_Del]
	@IDFor char(3)
As

	Set NoCount On

	Delete From MAFORMASPAGO
	Where IDFor = @IDFor
