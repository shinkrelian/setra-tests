﻿
--JRF-20141017-Unir los guías        
--JRF-20141019-Unir los Guias ingresados desde reservas.      
--JRF-20141224-Ajustar en casos de Hoteles con Servicios de Plan Alimenticio [Restaurantes]    
--JRF-20141226-Ajustar el Union para casos Con reservas    
--JRF-20141231-Ajustar la unón de Guias ingresados desde Operaciones (Ya no desde Reservas)  
--JRF-20150120-Or p.IDTipoProv='002'
--JRF-20150217-Condicionar la existencia del Registro
--JRF-20150525-Agregar columnas Ciudad y Pais
--PPMG-20160419-Se agrego un union para lo que se ingresa desde programación.
CREATE Procedure [dbo].[COTICAB_Sel_ListProvxIDCab]    
@IDCab int,      
@NuControlCalidad int    
as
BEGIN      
Set NoCount On        
Select distinct X.*,
	   Case When Not Exists(select NuControlCalidad from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc2 
						where ccpc2.NuControlCalidad=@NuControlCalidad and ccpc2.NuCuest=X.NuCuest and ccpc2.IDProveedor=X.IDProveedor) 
	   Then 'N' Else 'M' End as ValRegistro
from (    
--#Sección de Union de Hoteles (Hoteles, Operadores con Alojamiento)        
Select Case When IsNull(ccpc.NuCuest,0)=6 Then '002' Else '001' End as IDTipoProv,ISNULL(ccpc.NuCuest,c.NuCuest) as NuCuest,p.IDProveedor,p.NombreCorto as DescProveedor,    
isnull(u.Descripcion,'') as Ciudad,isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
    Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,          
    Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Cast(Isnull(ccpc.TxComentario,'') as varchar(Max)) as TxComentario    
from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor    
Left Join MACUESTIONARIO c On '001'=c.IDTipoProv    
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On ccpc.IDProveedor=p.IDProveedor and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad    
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
where (r.IDCab=@IDCab and r.Anulado=0 and r.Estado<>'XL') and     
   p.IDTipoProv='001' --and   
   --(p.IDTipoProv='003' and Exists(select IDReserva from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det    
         -- Where rd.IDReserva=r.IDReserva and rd.Anulado=0 and sd.ConAlojamiento=1)))    
Union    
--#Sección de Union de Restaurantes u Hoteles(Con Plan Alimenticio)    
Select Case When IsNull(ccpc.NuCuest,0) = 5 Then '001' Else '002' End as IDTipoProv,ISNULL(ccpc.NuCuest,c.NuCuest) as NuCuest,p.IDProveedor,p.NombreCorto,    
  isnull(u.Descripcion,'') as Ciudad,isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
    Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,          
    Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Cast(Isnull(ccpc.TxComentario,'') as varchar(Max)) as TxComentario    
from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor    
Left Join MACUESTIONARIO c On '002'=c.IDTipoProv--p.IDTipoProv=c.IDTipoProv    
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On ccpc.IDProveedor=p.IDProveedor and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad  
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
where (r.IDCab=@IDCab and r.Anulado=0 and r.Estado<>'XL') and     
   (((p.IDTipoProv='001' And Exists(select IDReserva from RESERVAS_DET rd2 Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det= sd2.IDServicio_Det    
            Where rd2.IDReserva=r.IDReserva and rd2.Anulado=0 and sd2.PlanAlimenticio=1) Or p.IDTipoProv='002')))     
Union    
--#Sección de Unión para considerar Guías a Enigma o Peruvian Sacred Valled [Proveedores Especificos]    
Select '005' as IDTipoProv,c.NuCuest,p.IDProveedor,p.NombreCorto as DescProveedor,    
isnull(u.Descripcion,'') as Ciudad,isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
    Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,          
    Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Cast(Isnull(ccpc.TxComentario,'') as varchar(Max)) as TxComentario    
from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor    
Left Join MACUESTIONARIO c On '005'=c.IDTipoProv    
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On ccpc.IDProveedor=p.IDProveedor and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad  
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad  
where (r.IDCab=@IDCab and r.Anulado=0 and r.Estado<>'XL') 
and p.IDTipoProv = '005' --and     
   --(p.IDProveedor In('000552','000636'))    
Union    
--#Sección de Unión para considerar Transportistas y Otros    
Select p.IDTipoProv as IDTipoProv,c.NuCuest,p.IDProveedor,p.NombreCorto as DescProveedor,    
isnull(u.Descripcion,'') as Ciudad,isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
    Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,          
    Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Cast(Isnull(ccpc.TxComentario,'') as varchar(Max)) as TxComentario    
from RESERVAS r Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor    
Left Join MACUESTIONARIO c On p.IDTipoProv=c.IDTipoProv    
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On ccpc.IDProveedor=p.IDProveedor and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad  
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad  
where (r.IDCab=@IDCab and r.Anulado=0 and r.Estado<>'XL') and     
   (Not p.IDTipoProv In ('001','002')) and c.NuCuest is not Null    
--#Sección de Unión de los Transportistas y Guías Programados en Setours Lima, Cusco, Argentina, Chile    
Union    
Select distinct p.IDTipoProv,c.NuCuest,odd.IDProveedor_Prg as IDProveedor,
(case when p.IDUsuarioTrasladista is null then( case when p.IDTipoProv ='005' then '(G) ' else '' end) ELSE '(T) ' END)+p.NombreCorto as DescProveedor, 
isnull(u.Descripcion,'') as Ciudad,isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,          
Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Cast(Isnull(ccpc.TxComentario,'') as varchar(Max)) as TxComentario          
from OPERACIONES_DET_DETSERVICIOS odd Inner Join OPERACIONES_DET od On odd.IDServicio=od.IDServicio and odd.IDOperacion_Det= od.IDOperacion_Det        
Inner Join OPERACIONES o on o.IDOperacion=od.IDOperacion        
Left Join MAPROVEEDORES p On odd.IDProveedor_Prg = p.IDProveedor        
Left Join MACUESTIONARIO c On p.IDTipoProv=c.IDTipoProv        
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On ccpc.IDProveedor=odd.IDProveedor_Prg and c.NuCuest=ccpc.NuCuest        
and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad--ccpc.NuControlCalidad=(select NuControlCalidad from CONTROL_CALIDAD_PRO where IDCab=o.IDCab)        
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
where odd.IDProveedor_Prg is not null and o.IDCab=@IDCab        
And (p.IDTipoProv ='005' Or p.IDTipoProv='004')      
Union        
Select p.IDTipoProv,c.NuCuest,p.IDProveedor as IDProveedor,p.NombreCorto as DescProveedor,     
isnull(u.Descripcion,'') as Ciudad,isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
  Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,          
  Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Cast(Isnull(ccpc.TxComentario,'') as varchar(Max)) as TxComentario     
from OPERACIONES_DET od Left Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det and rd.Anulado=0    
Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion Inner Join MAPROVEEDORES p On od.IDGuiaProveedor=p.IDProveedor    
Left Join MACUESTIONARIO c On p.IDTipoProv=c.IDTipoProv    
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On ccpc.IDProveedor=od.IDGuiaProveedor and c.NuCuest=ccpc.NuCuest        
and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad    
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
where o.IDCab=@IDCab and Not o.IDProveedor In ('001554','000544') And p.IDTipoProv <> '005'
Union
Select p.IDTipoProv,c.NuCuest,p.IDProveedor as IDProveedor,p.NombreCorto as DescProveedor,     
isnull(u.Descripcion,'') as Ciudad,isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
  Isnull(ccpc.FlExcelent,0) as FlExcelent,Isnull(ccpc.FlVeryGood,0) as FlVeryGood,ISNULL(ccpc.FlGood,0) FlGood,          
  Isnull(ccpc.FlAverage,0) as FlAverage,Isnull(ccpc.FlPoor,0) as FlPoor,Cast(Isnull(ccpc.TxComentario,'') as varchar(Max)) as TxComentario     
from OPERACIONES_DET od
Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor    
Left Join MACUESTIONARIO c On p.IDTipoProv=c.IDTipoProv    
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On ccpc.IDProveedor=od.IDGuiaProveedor and c.NuCuest=ccpc.NuCuest        
and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
where o.IDCab=@IDCab and o.IDReserva is null

)    
as X
END
