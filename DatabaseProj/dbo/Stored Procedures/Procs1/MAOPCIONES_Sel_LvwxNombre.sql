﻿CREATE Procedure [dbo].[MAOPCIONES_Sel_LvwxNombre]
	@ID	char(6),
	@Nombre varchar(50)
As
	Set NoCount On
	
	Select Nombre
	From MAOPCIONES
	Where (Nombre Like '%'+@Nombre+'%')
	And (IDOpc <>@ID Or ltrim(rtrim(@ID))='')
	Order by Nombre
