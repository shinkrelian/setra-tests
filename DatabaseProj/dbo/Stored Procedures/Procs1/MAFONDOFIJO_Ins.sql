﻿--JHD-20150605-Se agrego campo FLEsReembolso
--JHD-20150608-Se agrego campo CoPrefijo
--JHD-20150608-Se agrego campo CoAnio
--JHD-20150608-Se agrego calculo Correlativo del codigo
--JHD-20150627-Se agrego campo CoSap
--JHD-20150627-DECLARE @NuCodigo2 varchar(14)=(SELECT RIGHT('00000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,5)+1 as int)),1) as char(5)))),5)  as correlativo FROM MAFONDOFIJO where  rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
--JHD-20150910-Nuevo campo @CoTipoFondoFijo char(2)='',
--JHD-20150910-case when @CoTipoFondoFijo ='ER' THEN  'ER'+isnull((SELECT NumIdentidad FROM MAUSUARIOS WHERE IDUsuario=@CoUserResponsable),'')+'-'+@NuCodigo2 else '' end
--JHD-20150911-case when @CoTipoFondoFijo ='ER' THEN  'ER'+isnull((SELECT NumIdentidad FROM MAUSUARIOS WHERE IDUsuario=@CoUserResponsable),'') else '' end
CREATE PROCEDURE [dbo].[MAFONDOFIJO_Ins]
	--@NuCodigo varchar(14)='',
	@CoCeCos char(6)='',
	@CtaContable varchar(14)='',
	@Descripcion varchar(255)='',
	@CoUserResponsable char(4)='',
	@CoMoneda char(3)='',
	@SSMonto numeric=0,
	@CoPrefijo char(4)='',
	@CoTipoFondoFijo char(2)='',
	@UserMod char(4)=''
AS
BEGIN
Declare @NuFondoFijo int =(select Isnull(Max(NuFondoFijo),0)+1         
        from MAFONDOFIJO )        

DECLARE @NuCodigo varchar(14)=(SELECT RIGHT('000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,3)+1 as int)),1) as char(3)))),3) +'-'+substring(cast(year(getdate()) as char(4)),3,4) as correlativo FROM MAFONDOFIJO where coanio=cast(year(getdate()) as char(4)) and rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
DECLARE @NuCodigo2 varchar(14)=(SELECT RIGHT('00000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,5)+1 as int)),1) as char(5)))),5)  as correlativo FROM MAFONDOFIJO where  rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))

	Insert Into [dbo].[MAFONDOFIJO]
		(
			[NuFondoFijo],
 			[NuCodigo],
 			[CoCeCos],
 			[CtaContable],
 			[Descripcion],
 			[CoUserResponsable],
 			[CoMoneda],
 			[SSMonto],
			[SsSaldo],
			[SsDifAceptada],
			FLEsReembolso,
			CoPrefijo,
			CoAnio,
 			[UserMod],
 			[FecMod],
			CoSap,
			CoTipoFondoFijo,
			NuCodigo_ER
 		)
	Values
		(
			Case When Ltrim(Rtrim(@NuFondoFijo))='' Then Null Else @NuFondoFijo End,  
 			Case When Ltrim(Rtrim(@NuCodigo))='' Then Null Else @NuCodigo End,  
 			Case When Ltrim(Rtrim(@CoCeCos))='' Then Null Else @CoCeCos End,  
 			Case When Ltrim(Rtrim(@CtaContable))='' Then Null Else @CtaContable End,  
 			Case When Ltrim(Rtrim(@Descripcion))='' Then Null Else @Descripcion End,  
 			Case When Ltrim(Rtrim(@CoUserResponsable))='' Then Null Else @CoUserResponsable End,  
 			Case When Ltrim(Rtrim(@CoMoneda))='' Then Null Else @CoMoneda End,  
 			Case When @SSMonto=0 Then Null Else @SSMonto End,   
			Case When @SSMonto=0 Then Null Else @SSMonto End,   
			0,   
			0,
			Case When Ltrim(Rtrim(@CoPrefijo))='' Then Null Else @CoPrefijo End, 
			cast(year(getdate()) as char(4)),
 			@UserMod,
 			getdate(),
			@NuCodigo2,
			Case When Ltrim(Rtrim(@CoTipoFondoFijo))='' Then Null Else @CoTipoFondoFijo End,
			case when @CoTipoFondoFijo ='ER' THEN  'ER'+isnull((SELECT NumIdentidad FROM MAUSUARIOS WHERE IDUsuario=@CoUserResponsable),'')/*+'-'+@NuCodigo2*/ else '' end
 		)
END;
