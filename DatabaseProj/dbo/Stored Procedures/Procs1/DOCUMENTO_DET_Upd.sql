﻿

  
    
--HLF-20140807-@IDVoucher varchar(15) (antes int)    
--HLF-20140819-@NoServicio varchar(230)
--HLF-20141209-@SsIGVSFE numeric(6, 2),  
CREATE Procedure [dbo].[DOCUMENTO_DET_Upd]        
 @NuDocum char(10),        
 @IDTipoDoc char(3),         
 @NuDetalle tinyint,      
       
 @IDVoucher varchar(15),        
 @NuOrdenServicio char(13),         
 @SsCompraNeto numeric(9, 2),        
 @SsIGVCosto numeric(6, 2),        
 --@SsIGVSFE numeric(5, 2),        
 @SsIGVSFE numeric(6, 2),        
 @SsTotalCosto numeric(9, 2),         
 @SsMargen numeric(6, 2),        
 @NoServicio varchar(230),  
 @SsTotalCostoUSD numeric(9, 2),        
 @SsTotalDocumUSD numeric(9, 2),         
 @UserMod char(4)          
As        
 Set NoCount On        
         
 Update DOCUMENTO_DET Set      
 IDVoucher=Case When ltrim(rtrim(@IDVoucher))='' Then Null Else @IDVoucher End,          
 NuOrdenServicio=Case When ltrim(rtrim(@NuOrdenServicio))='' Then Null Else @NuOrdenServicio End,          
 SsCompraNeto=@SsCompraNeto,        
 SsIGVCosto=@SsIGVCosto,        
 SsIGVSFE=@SsIGVSFE,        
 SsTotalCosto=@SsTotalCosto,            
 SsMargen=@SsMargen,        
 NoServicio=Case When ltrim(rtrim(@NoServicio))='' Then Null Else @NoServicio End,  
 SsTotalCostoUSD=@SsTotalCostoUSD,        
 SsTotalDocumUSD=@SsTotalDocumUSD,         
 FecMod=getdate(),      
 UserMod=@UserMod      
 Where      
 NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc And NuDetalle=@NuDetalle   
  