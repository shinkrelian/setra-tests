﻿--CREATE
CREATE Procedure dbo.MACLIENTES_Upd_CardCodeSAP
@CardCodeSAP varchar(15),
@IDCliente char(6)
As      
 UPDATE  MACLIENTES
    SET CardCodeSAP=@CardCodeSAP
 WHERE IDCliente=@IDCliente     
