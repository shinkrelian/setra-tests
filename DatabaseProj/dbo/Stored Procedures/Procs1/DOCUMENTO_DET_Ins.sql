﻿	
  
--HLF-20140807-@pNuDetalle tinyint output      
--@IDVoucher varchar(15) (antes int)      
--HLF-20140819-@NoServicio varchar(230)    
--HLF-20141209-@SsIGVSFE numeric(6, 2),  
--HLF-20150209-@NuOrdenServicio char(13) a char(14)
CREATE Procedure dbo.DOCUMENTO_DET_Ins  
 @NuDocum char(10),            
 @IDTipoDoc char(3),             
 @IDProveedor char(6),            
 @IDVoucher varchar(15),            
 @NuOrdenServicio char(14),            
 @IDMonedaCosto char(3),        
 @SsCompraNeto numeric(9, 2),            
 @SsIGVCosto numeric(6, 2),            
 --@SsIGVSFE numeric(5, 2),            
 @SsIGVSFE numeric(6, 2),  
 @SsTotalCosto numeric(9, 2),            
 @IDTipoOC char(3),            
 @SsMargen numeric(6, 2),            
 @SsTotalCostoUSD numeric(9, 2),            
 @SsTotalDocumUSD numeric(9, 2),           
 @CoTipoDet char(2),        
 @NoServicio varchar(230),      
 @UserMod char(4),          
 @pNuDetalle tinyint output      
As            
 Set NoCount On            
             
 Declare @NuDetalle tinyint=isnull((Select MAX(NuDetalle) From DOCUMENTO_DET             
 Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc),0)+1            
          
 Insert Into DOCUMENTO_DET(            
 NuDocum,            
 IDTipoDoc,             
 NuDetalle,            
 IDProveedor,            
 IDVoucher,            
 NuOrdenServicio,            
 IDMonedaCosto,        
 SsCompraNeto,            
 SsIGVCosto,            
 SsIGVSFE,            
 SsTotalCosto,            
 IDTipoOC,            
 SsMargen,            
 SsTotalCostoUSD,            
 SsTotalDocumUSD,             
 CoTipoDet,        
 NoServicio,    
 UserMod)            
 Values(            
 @NuDocum,            
 @IDTipoDoc,             
 @NuDetalle,            
 @IDProveedor,            
 Case When ltrim(rtrim(@IDVoucher))='' Then Null Else @IDVoucher End,            
 Case When ltrim(rtrim(@NuOrdenServicio))='' Then Null Else @NuOrdenServicio End,            
 @IDMonedaCosto,        
 @SsCompraNeto,            
 @SsIGVCosto,            
 @SsIGVSFE,            
 @SsTotalCosto,            
 @IDTipoOC,            
 @SsMargen,            
 @SsTotalCostoUSD,            
 @SsTotalDocumUSD,             
 @CoTipoDet,        
 Case When ltrim(rtrim(@NoServicio))='' Then Null Else @NoServicio End,    
 @UserMod)      
      
 Set @pNuDetalle=@NuDetalle      
       
