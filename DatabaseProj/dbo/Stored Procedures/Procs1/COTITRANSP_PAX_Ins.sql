﻿--HLF-20150410-@SsReembolso numeric(8,2)  =0  ,
CREATE Procedure dbo.COTITRANSP_PAX_Ins  
 @IDTransporte int,  
 @IDPax int,  
 @CodReservaTransp varchar(12),  
 @IDProveedorGuia char(6),  
 --@SsReembolso numeric(8,2),
 @SsReembolso numeric(8,2)=0,
 @UserMod char(4)  
As  
 Set Nocount on  
 Declare @IDPaxTransp smallint=(Select Isnull(Max(IDPaxTransp),0)+1   
  From COTITRANSP_PAX Where IDTransporte=@IDTransporte)  
  
 Insert Into COTITRANSP_PAX(IDTransporte, IDPaxTransp,   
  IDPax,   
  CodReservaTransp,  
  IDProveedorGuia,  
  SsReembolso, 
  UserMod)  
 Values(@IDTransporte, @IDPaxTransp,   
     Case When @IDPax=0 Then Null Else @IDPax End,  
  Case When ltrim(rtrim(@CodReservaTransp))='' Then Null Else @CodReservaTransp End,  
  Case When ltrim(rtrim(@IDProveedorGuia))='' Then Null Else @IDProveedorGuia End,  
  Case When @SsReembolso=0 Then Null else @SsReembolso End,
  @UserMod)  

