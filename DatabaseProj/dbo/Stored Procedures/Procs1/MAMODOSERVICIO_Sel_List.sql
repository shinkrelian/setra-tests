﻿Create Procedure MAMODOSERVICIO_Sel_List
@Descripcion varchar(30)
As
	Set NoCount On
	Select CodModoServ,Descripcion from MAMODOSERVICIO
	Where (LTRIM(rtrim(@Descripcion))='' Or Descripcion like '%'+@Descripcion+'%')
