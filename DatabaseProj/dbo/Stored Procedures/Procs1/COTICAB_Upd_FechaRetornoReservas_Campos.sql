﻿--JHD-20160405-Se quito el parametro @Campo para declararlo dentro de sp
--JHD-20160405-SELECT @Campo=CASE WHEN (FechaRetornoReservas IS NULL) THEN 1 ...
CREATE Procedure dbo.COTICAB_Upd_FechaRetornoReservas_Campos
@IDCab int,
--@FechaRetornoReservas smalldatetime,
--@Campo smallint,
@UserMod char(4)
as
Set NoCount On
begin

declare @Campo smallint

SELECT @Campo=CASE WHEN (FechaRetornoReservas IS NULL) THEN 1 
			  ELSE (CASE WHEN FechaRetornoReservas_2doEnvio is null then 2 else 0 end)
			  end
FROM COTICAB WHERE IDCAB=@IDCab

Update COTICAB set 
	--FechaRetornoReservas =getdate(), --Case when convert(char(10),@FechaRetornoReservas,103) ='01/01/1900' then Null else @FechaRetornoReservas End, 
	FechaRetornoReservas = case when @campo=1 then getdate() else FechaRetornoReservas end,
	FechaRetornoReservas_2doEnvio  = case when @campo=2 then getdate() else FechaRetornoReservas_2doEnvio end,
	UserMod = @UserMod,
	FecMod = GETDATE()
Where IDCAB = @IDCab
end;
