﻿--JHD-20160127-,DocTotal=  dbo.FnCambioMoneda(d.SsTotal_FEgreso,d.CoMoneda_FEgreso,'SOL',@SSTipoCambio),
--JHD-20160204-,JournalMemo= case when d.CoProveedor='001593' then 'Transp. Turístico Consettur' else '' end--isnull(d.TxConcepto,'')
--JHD-20160210-,JournalMemo= isnull(d.TxConcepto,'')
CREATE Procedure [dbo].[Documento_Proveedor_List_SAP_Inc]
@nudocumprov int
as
begin

declare @IdProveedor as char(6)=(select CoProveedor from DOCUMENTO_PROVEEDOR where NuDocumProv=@nudocumprov)

DECLARE @NuCodigo_ER VARCHAR(30)
SET @NuCodigo_ER = isnull((SELECT TOP 1 NuCodigo_ER FROM ENTRADASINC_SAP WHERE NuDocumProv=@nudocumprov),'')

declare @SSTipoCambio decimal(6,3)=0
select @SSTipoCambio= case when isnull(NuDocum_Ref,'') = '' then isnull(SSTipoCambio,isnull((select ValVenta from matipocambio where fecha=feemision),0))
	   else isnull((select ValVenta from matipocambio where fecha=FeEmision_Ref),0) end
from DOCUMENTO_PROVEEDOR where NuDocumProv=@nudocumprov

select
TaxDate=FeEmision,

--DocDate=FeEmision,
--DocDate=FeRecepcion,
DocDate=
	FeRecepcion,


DocDueDate=FeVencimiento,
CardCode=

	isnull(p.CardCodeSAP,''),
CardName=isnull(p.NombreCorto,''),

DocType=case when d.CoTipoDocSAP=2 then 1 else d.CoTipoDocSAP end,

FederalTaxID= 
	
		isnull(case when (SELECT IDPAIS FROM MAUBIGEO where IDubigeo=p.IDCiudad)<>'000323' THEN P.CardCodeSAP ELSE p.NumIdentidad END,'')

	,

DocCurrency=isnull(rtrim(ltrim(m.CoSAP)),isnull((SELECT CoSAP from mamonedas where IDMoneda=D.CoMoneda_FEgreso),'')),

--ControlAccount=isnull(d.CoCtaContab_SAP,''),
ControlAccount=isnull(d.CoCtaContab,''),


--CashAccount=isnull((select CtaContable from MAFONDOFIJO where NuFondoFijo=d.NuFondoFijo),'')
CashAccount= case when CoObligacionPago='FFI' THEN  isnull((select CtaContable from MAFONDOFIJO where NuFondoFijo=d.NuFondoFijo),'')
					when CoObligacionPago='PST' AND (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null 
					then isnull((select CtaContable from MAFONDOFIJO ff inner join PRESUPUESTO_SOBRE ps on ff.NuFondoFijo=ps.NuFondoFijo where ps.NuPreSob=d.NuPreSob),'')
				else '' end
--,Comments=isnull(d.TxConcepto,'')

--,JournalMemo= case when d.CoProveedor='001593' then 'Transp. Turístico Consettur' else '' end--isnull(d.TxConcepto,'')
,JournalMemo= isnull(d.TxConcepto,'')
,Comments= '' -- case when d.CoProveedor='001593' then 'prueba 2' else '' end
/*
case when isnull(d.NuDocumSusEnt,'')+isnull(d.NuOperBanSusEnt,'')='' then 
	isnull(d.TxConcepto,'')
else	
	case when isnull(d.NuDocumSusEnt,'')<>'' then 
		d.CoTipDocSusEnt+'-'+d.NuDocumSusEnt
	else
		d.NuOperBanSusEnt
	end
end
*/
,PaymentGroupCode=1--d.CoFormaPago
--,PaymentGroupCode=(select cosap from MAFORMASPAGO fp where fp.IdFor=p.IDFormaPago)--d.CoFormaPago
,DiscountPercent=0

--,DocTotal= isnull(round(case when d.CoMoneda in ('USD','SOL') THEN
--						abs(isnull(case when d.CoMoneda_FEgreso='SOL' THEN
--								CASE WHEN d.comoneda='SOL' THEN d.sstotal
--								else d.sstotal_FEgreso end
--						  else
--								CASE WHEN d.comoneda='SOL' THEN d.sstotaloriginal
--								--else dbo.FnCambioMoneda(d.sstotal,d.comoneda,'SOL',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0))) end
--								else dbo.FnCambioMoneda(d.sstotal,d.comoneda,'SOL',@SSTipoCambio) end
--						  end,0))
--					else --monedas diferentes, cambio de dolares a soles
--						 --dbo.FnCambioMoneda(d.SsTotal_FEgreso,d.CoMoneda_FEgreso,'SOL',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0)))
--						 --dbo.FnCambioMoneda(d.SsTotal_FEgreso,d.CoMoneda_FEgreso,'SOL',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0)))
--						 dbo.FnCambioMoneda(d.SsTotal_FEgreso,d.CoMoneda_FEgreso,'SOL',@SSTipoCambio)
				 
--					end
--			-
--			isnull(SsDetraccion,0),2),0),

,DocTotal=  dbo.FnCambioMoneda(d.SsTotal_FEgreso,d.CoMoneda_FEgreso,'SOL',@SSTipoCambio),
				 


DocTotalFc=	isnull(round(case when d.CoMoneda in ('USD','SOL') THEN
				abs(isnull(case when d.CoMoneda<>'SOL' then d.SSTotal else 0 end,0))
			else
				abs(isnull(case when d.CoMoneda_FEgreso<>'SOL' then d.SsTotal_FEgreso else 0 end,0))
			end
			--- isnull(dbo.FnCambioMoneda(d.SsDetraccion,'SOL','USD',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0))),0)
			- isnull(dbo.FnCambioMoneda(d.SsDetraccion,'SOL','USD',@SSTipoCambio),0)
			,2),0)
,U_SYP_MDTD=isnull(td.CoSAP,'')
--,U_SYP_MDSD=isnull(d.NuSerie,'')
,U_SYP_MDSD=isnull(d.NuSerie,'000')
,U_SYP_MDCD=isnull(d.NuDocum,'')
--,U_SYP_MDTO=ISNULL((select cosunat from matipodoc where idtipodoc=d.cotipodoc_Ref),'')
,U_SYP_MDTO=ISNULL((select CoSAP from matipodoc where idtipodoc=d.cotipodoc_Ref),'')
,U_SYP_MDSO=isnull(NuSerie_Ref,'')
,U_SYP_MDCO=isnull(NuDocum_Ref,'')
,U_SYP_STATUS= case when d.FlActivo=1 then 'V' ELSE 'A' END

,U_SYP_FECHAREF=CASE WHEN NuDocum_Ref IS NOT NULL THEN FeEmision_Ref ELSE '01/01/1900' END
,U_SYP_NUMOPER=d.NuDocumProv
,U_SYP_TC=isnull(@SSTipoCambio,0)
--,U_SYP_TCOMPRA= CASE WHEN d.cotipoCompra is not null then LTRIM(RTRIM(d.CoTipoCompra))
--				else 
--					case when CoObligacionPago='FFI' THEN isnull((select CoTipoFondoFijo from MAFONDOFIJO where NuFondoFijo=d.NuFondoFijo),'') -- 'CC'
--					when CoObligacionPago='PST' AND (SELECT TOP 1 NuCodigo_ER FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null then 'ER'
--					when CoObligacionPago='PST' AND (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null THEN 'ER'--'CC'
--					else '*' END
--				end

,U_SYP_TCOMPRA=  'ER'

,U_SYP_NFILE=isnull((select idfile from coticab where idcab=d.idcab),'')


--,U_SYP_CODERCC= Case CoObligacionPago
--					When 'FFI' Then 
--						(select (CASE WHEN CoTipoFondoFijo='CC' THEN ISNULL(CtaContable,'') ELSE ISNULL(NuCodigo_ER,'') END)+'-'+CoSap from MAFONDOFIJO where NuFondoFijo=d.NuFondoFijo)
--					when 'PST' Then 
--						Case When ltrim(rtrim(d.NuDocumSusEnt ))='' and ltrim(rtrim(d.NuOperBanSusEnt ))='' Then
--							Case When ps.IDProveedor<>'000544' or (ps.IDProveedor='000544' and exists(Select NuPreSob from PRESUPUESTO_SOBRE_DET where NuPreSob=d.NuPreSob and CoPago='EFE')) then
--								Case When (SELECT TOP 1 NuCodigo_ER FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null then 
--									+isnull((select /*ctacontable*/ isnull(ps.NuCodigo_ER,'') from PRESUPUESTO_SOBRE ps where ps.NuPreSob=d.NuPreSob),'')
--								Else
--									Case When (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null then /*'CHOLIM-'*/+
--										isnull((select /*ctacontable*/ isnull(ff.NuCodigo_ER,'')+'-'+CoSap from MAFONDOFIJO ff inner join PRESUPUESTO_SOBRE ps on 
--										ff.NuFondoFijo=ps.NuFondoFijo where ps.NuPreSob=d.NuPreSob),'')--00001'
--									else '' 
--									END
--								End
--							else						
--								''							
--							end
--						Else
--							''
--						End
--				 End

,U_SYP_CODERCC= @NuCodigo_ER

,U_SYP_CTAERCC= case when CoObligacionPago='FFI' THEN  isnull((select CtaContable from MAFONDOFIJO where NuFondoFijo=d.NuFondoFijo),'')
				else '' end
,U_SYP_TIPOBOLETO= case when CoObligacionPago='PRL' then '2' else '' end-- peru rail: 2 por defecto
,U_SYP_TPO_OP= case when isnull(SsDetraccion,0)>0 then '01' else '' end--isnull(d.CoTipOpeDet,'') --'01'
from DOCUMENTO_PROVEEDOR d
left join MAPROVEEDORES p on d.CoProveedor=p.IDProveedor
--left join MAPROVEEDORES pPtt on pPtt.IDProveedor='002960'
left join MAMONEDAS m on m.IDMoneda=d.CoMoneda
--left join MAMONEDAS m on m.IDMoneda=d.CoMoneda_Pago
left join matipodoc td on td.IDtipodoc=d.CoTipoDoc
Left Join PRESUPUESTO_SOBRE ps On d.NuPreSob=ps.NuPreSob
where d.nudocumprov=@nudocumprov
end;
