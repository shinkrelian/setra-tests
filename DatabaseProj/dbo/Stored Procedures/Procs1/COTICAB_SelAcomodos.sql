﻿
Create Procedure dbo.COTICAB_SelAcomodos
	@IDCab int
As
	Set Nocount On

	Select isnull(Simple,0) as Simple, isnull(Twin,0) as Twin, isnull(Matrimonial,0) as Matrimonial,isnull(Triple,0) as Triple, 
	isnull(SimpleResidente,0) as SimpleResidente, isnull(TwinResidente,0) as TwinResidente, 
	isnull(MatrimonialResidente,0) as MatrimonialResidente, 
	isnull(TripleResidente,0) as TripleResidente
	From COTICAB
	Where IDCAB=@IDCab

