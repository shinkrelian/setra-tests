﻿
CREATE PROCEDURE [dbo].[COTIDET_Sel_Valor_Zicasso]
				@IDCab			int,
				@pTotalParaZ	numeric(10,2) output
AS
BEGIN
	DECLARE @IDProveedor_Zicasso char(6) = '003557',
			@IDServicio_Zicasso char(8)	= 'LAX00002',
			@Servicio_Det_Zicasso CHAR(4) = 'ZIC6'
	DECLARE @Tbl_ServDet Table(IDProveedorX char(6),
							   IDServicioX char(8),
							   IDServicio_DetX int);
	Set NoCount On
	INSERT INTO @Tbl_ServDet(IDProveedorX, IDServicioX, IDServicio_DetX)
	SELECT S.IDProveedor, D.IDServicio, D.IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
	WHERE D.TipoUsoSistema = @Servicio_Det_Zicasso AND S.IDProveedor = @IDProveedor_Zicasso AND D.IDServicio = @IDServicio_Zicasso
	GROUP BY S.IDProveedor, D.IDServicio, D.IDServicio_Det

	SELECT @pTotalParaZ = SUM(ISNULL(Total,0))
	FROM COTIDET D LEFT OUTER JOIN @Tbl_ServDet T ON D.IDProveedor = T.IDProveedorX AND D.IDServicio = T.IDServicioX AND D.IDServicio_Det = T.IDServicio_DetX
	WHERE IDCAB = @IDCab AND IDServicio_DetX IS NULL
	SET @pTotalParaZ = ISNULL(@pTotalParaZ,0)
	IF @pTotalParaZ <> 0
	BEGIN
		--SET @pTotalParaZ = ROUND(((@pTotalParaZ * 0.0638)),0)
		SET @pTotalParaZ = CEILING((@pTotalParaZ * 0.0638))
	END
END
