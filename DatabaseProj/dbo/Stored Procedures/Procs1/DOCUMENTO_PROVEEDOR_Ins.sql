﻿
--JHD-20141028-Se agregaron campos de SsNeto,SsOtrCargos y SsDetraccion        
--JHD-20141029-Se agregaron campos de SSTipoCambio,SSTotal        
--JHD-20141029-Se cambio el nombre del campo de SSMonto por SSTotalOriginal        
--JHD-20141029-Se cambio el nombre del campo de SSTotal_Dolares por SSTotal        
--JHD-20141029-Se agrego campo de NuOrden_Servicio        
--JHD-20141030-Se agregaron campos de CoTipoOC,CoCtaContab,CoObligacionPago        
--JHD-20141103-Se aumentó la longitud del parametro @NuDocum a varchar(15)        
--JHD-20141103-Se agrego campo de CoMoneda_Pago      
--JRF-20141107-Insertar nuevos campos.[CoMoneda_FEgreso,SsTipoCambio_FEgreso,SsTotal_FEgreso]      
--JHD-20141205-Se cambio el tipo de dato de @NuDocum a varchar(30)      
--JHD-20141205-Se agregó campo de UserNuevo      
--JHD-20141209-Se agrego campo de CoOrdPag      
--HLF-20150127-Se agrego campo de IDOperacion    
--HLF-20150203-Se agrego campo de NuOrdComInt  
--HLF-20150209-Se agrego campos de @NuVouOfiExtInt int=0, @NuDocumVta char(10)='', @CoTipoDocVta char(3)=''  
--HLF-20150210-Case When @FeEmision='01/01/1900' Then getdate() Else @FeEmision End,  
--Case When @FeRecepcion='01/01/1900' Then getdate() Else @FeRecepcion End,  
--HLF-20150212-@NuPreSob int=0  
--HLF-20150217-@NuVouPTrenInt int=0  
--JHD-20150518-Se agrego campo de NuSerie 
--JHD-20150601-Se agrego campo de CoCeCos 
--JHD-20150601-@CoCtaContab varchar(14)='', 
--JHD-20150608-Se agregaron los parametros @NuFondoFijo int=0, @CoTipoFondoFijo char(3)='', @SSPercepcion numeric(12,2)=0, @TxConcepto varchar(max)='', @CoProveedor char(6)=''
--JHD-20150608-Se agrego el parametro @IDCab int=0
--JHD-20150722-Se agregaron campos nuevos: FeVencimiento, CoTipoDocSAP, CoFormaPago, CoTipoDoc_Ref, NuSerie_Ref, NuDocum_Ref, FeEmision_Ref
--JHD-20150723-Se agrego el campo: CoCeCon
--JHD-20150724-,@CoTipoDocSAP
--JHD-20150724- @pNuDocumProv int output
--JHD-20150724-set @pNuDocumProv=@NuDocumProv
--JHD-20150807-Se agrego el campo CoCtaContab_SAP varchar(14)
--JHD-20150807-select @CoProveedor_SAP= case when @CoProveedor not in('000467','000527','002315','002317')  then '000000' else @CoProveedor end
--JHD-20150817-Nuevo Campo CoGasto
--JHD-20150819-set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
--JHD-20150820-set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_Pago and CoProveedor=@CoProveedor_SAP),'')
--JHD-20150826- Declare @NuDocum2 varchar(30)                              
--				Exec Correlativo_SelOutput 'DOCUMENTO_PROVEEDOR',1,10,@NuDocum2 output    
--				 Case When Ltrim(Rtrim(@NuDocum))='' Then @NuDocum2 Else @NuDocum End,  
--JHD-20150826- Se agrego el campo FlCorrelativo
--				,Case When Ltrim(Rtrim(@NuDocum))='' Then CAST(1 AS BIT) Else CAST(0 AS BIT) End
--JHD-20150826-set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
--JHD-20150911-set @CoCtaContab_SAP= case when @CoMoneda in ('USD','SOL') then
									--isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
									--else
									--isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_FEgreso and CoProveedor=@CoProveedor_SAP),'')
									--end
--JHD-20150917-declare @CoTipoCompra char(2)=''
--JHD-20150917-set @CoTipoCompra= case when @CoObligacionPago='FFI' THEN ...
--JHD-20150929-Nuevos campos  @FlMultiple bit=0, @NuDocum_Multiple int=0
--HLF-20151119-Quitando '000467' de select @CoProveedor_SAP=...
--HLF-20160104-@CoTipDocSusEnt char(3), @NuDocumSusEnt char(15), @NuOperBanSusEnt varchar(20)
--HLF-20160107-set @CoTipoCompra= ...
--HLF-20160119-When 'PTT' then '01' (@CoTipoCompra)
--HLF-20160329-Case When @CoTipoDocSAP<>2 Then
--PPMG-20160411-Se agrego esta columna IDDocFormaEgreso
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Ins]        
	--@NuDocumProv int,        
	@NuVoucher int=0,        
	--@NuDocum varchar(15),        
	@NuDocum varchar(30)='',        
	@NuDocInterno char(8),        
	@CoTipoDoc char(3),        
	@FeEmision smalldatetime,        
	@FeRecepcion smalldatetime,        
	@CoTipoDetraccion CHAR(5)='',        
	@CoMoneda char(3),        
	@SsIGV numeric(8,2),        
	@SsNeto numeric(9,2),        
	@SsOtrCargos  numeric(9,2)=0,        
	@SsDetraccion numeric(9,2)=0,        
	@SSTotalOriginal numeric(9,2),        
	--@CoEstado char(2),        
	@SSTipoCambio decimal(6,3)=0,        
	@SSTotal numeric(9,2)=0,        
	@NuOrden_Servicio int=0,        
	@UserMod char(4)='',        
	@CoTipoOC char(3)='',        
	@CoCtaContab varchar(14)='',        
	@CoObligacionPago char(3)='',        
	@CoMoneda_Pago char(3)='',      
	@CoMoneda_FEgreso char(3)='',      
	@SsTipoCambio_FEgreso numeric(6,3)=0,      
	@SsTotal_FEgreso numeric(9,2)=0,      
	@UserNuevo char(4)='',      
	@CoOrdPag int=0,    
	@IDOperacion int=0,  
	@NuOrdComInt int=0,  
	@NuVouOfiExtInt int=0,  
	@NuDocumVta char(10)='',  
	@CoTipoDocVta char(3)='',  
	@NuPreSob int=0,
	@NuVouPTrenInt int=0,
	@NuSerie varchar(10)='',
	@CoCeCos char(6)='',
	@NuFondoFijo int=0,
	@CoTipoFondoFijo char(3)='',
	@SSPercepcion numeric(12,2)=0,
	@TxConcepto varchar(max)='',
	@CoProveedor char(6)='',
	@IDCab int=0,
	--
	@FeVencimiento datetime='01/01/1900',
	@CoTipoDocSAP tinyint=0,
	@CoFormaPago char(3)='',
	@CoTipoDoc_Ref char(3)='',
	@NuSerie_Ref varchar(10)='',
	@NuDocum_Ref varchar(30)='',
	@FeEmision_Ref datetime='01/01/1900',
	@CoCeCon char(6)='',
	@CoGasto varchar(12)='',
	@FlMultiple bit=0,
	@NuDocum_Multiple int=0,

	@CoTipDocSusEnt char(3)='', 
	@NuDocumSusEnt char(15)='',
	@NuOperBanSusEnt varchar(20)='',
	@IDDocFormaEgreso int = 0,
	@pNuDocumProv int output

AS        
   
	Declare @NuDocum2 varchar(30)                              
	Exec Correlativo_SelOutput 'DOCUMENTO_PROVEEDOR',1,10,@NuDocum2 output     
  
	Declare @NuDocumProv int =(select Isnull(Max(NuDocumProv),0)+1         
		from DOCUMENTO_PROVEEDOR )        

	declare @CoProveedor_SAP char(6)
	declare @CoCtaContab_SAP varchar(14)
	declare @CoTipoCompra char(2)=''
	--select @CoProveedor_SAP= case when @CoProveedor='' then '000000' else @CoProveedor end
	--select @CoProveedor_SAP= case when @CoProveedor not in('000467','000527','002315','002317')  then '000000' else @CoProveedor end
	select @CoProveedor_SAP= case when @CoProveedor not in('000527','002315','002317')  then '000000' else @CoProveedor end

	--set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_FEgreso and CoProveedor=@CoProveedor_SAP),'')
	--set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
	set @CoCtaContab_SAP=
				case when @CoMoneda in ('USD','SOL') then
				isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
				else
				isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_FEgreso and CoProveedor=@CoProveedor_SAP),'')
				end
	--set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_Pago and CoProveedor=@CoProveedor_SAP),'')

	--set @CoTipoCompra= case when @CoObligacionPago='FFI' THEN isnull((select CoTipoFondoFijo from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo),'') -- 'CC'
	--				when @CoObligacionPago='PST' AND (SELECT TOP 1 NuCodigo_ER FROM PRESUPUESTO_SOBRE WHERE NuPreSob=@NuPreSob) is not null then 'ER'
	--				when @CoObligacionPago='PST' AND (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=@NuPreSob) is not null THEN 'ER'--'CC'
	--				else (
	--						case when rtrim(ltrim(@CoTipoDoc)) in ('HTD','RH') then '05'
	--								when rtrim(ltrim(@CoTipoDoc)) in ('RSP') then '04' 
	--						else
	--							case when (SELECT top 1 up.IDubigeo
	--										FROM MAPROVEEDORES p
	--										left join MAUBIGEO u on p.IDCiudad=u.IDubigeo
	--										left join MAUBIGEO up on u.IDPais=up.IDubigeo
	--										where p.IDProveedor=@CoProveedor)='000323' --peru
	--							then '01' else '02' end
	--						end
	--					)				
	--				end

	set @CoTipoCompra= 
		Case @CoObligacionPago
			When 'FFI' Then isnull((select CoTipoFondoFijo from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo),'') 
			when 'PST' Then
				--Case When ltrim(rtrim(@NuDocumSusEnt ))='' and ltrim(rtrim(@NuOperBanSusEnt ))='' Then
				Case When @CoTipoDocSAP<>2 Then
					Case When (SELECT TOP 1 NuCodigo_ER FROM PRESUPUESTO_SOBRE WHERE NuPreSob=@NuPreSob) is not null 
						or (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=@NuPreSob) is not null THEN 
						'ER'
					Else (
								case when rtrim(ltrim(@CoTipoDoc)) in ('HTD','RH') then '05'
										when rtrim(ltrim(@CoTipoDoc)) in ('RSP') then '04' 
								else
									case when (SELECT top 1 up.IDubigeo
												FROM MAPROVEEDORES p
												left join MAUBIGEO u on p.IDCiudad=u.IDubigeo
												left join MAUBIGEO up on u.IDPais=up.IDubigeo
												where p.IDProveedor=@CoProveedor)='000323' --peru
									then '01' else '02' end
								end
							)				
					End
				Else
					'01'
				End
			When 'PTT' then '01'
		End
		
        
	Insert Into [dbo].[DOCUMENTO_PROVEEDOR]        
	(        
	[NuDocumProv],        
	[NuVoucher],        
	NuOrden_Servicio, 
	NuSerie,
	[NuDocum],        
	NuDocInterno,        
	[CoTipoDoc],        
	[FeEmision],        
	[FeRecepcion],        
	[CoTipoDetraccion],        
	[CoMoneda],        
	[SsIGV],        
	SsNeto,        
	SsOtrCargos,        
	SsDetraccion,        
	[SSTotalOriginal],        
	SSTipoCambio,        
	SSTotal,        
	--[CoEstado],        
	[UserMod]        
	,CoTipoOC        
	,CoCtaContab         
	,CoObligacionPago        
	,CoMoneda_Pago      
	,CoMoneda_FEgreso      
	,SsTipoCambio_FEgreso      
	,SsTotal_FEgreso      
	,UserNuevo      
	,CoOrdPag      
	,IDOperacion    
	,NuOrdComInt  
	,NuVouOfiExtInt  
	,NuDocumVta   
	,CoTipoDocVta  
	,NuPreSob  
	,NuVouPTrenInt
	,CoCeCos
	,NuFondoFijo
	,CoTipoFondoFijo 
	,SSPercepcion
	,TxConcepto
	,CoProveedor
	,IDCab
	,FeVencimiento
	,CoTipoDocSAP
	,CoFormaPago
	,CoTipoDoc_Ref
	,NuSerie_Ref
	,NuDocum_Ref
	,FeEmision_Ref
	,CoCeCon
	,CoCtaContab_SAP
	,CoGasto
	,FlCorrelativo
	,CoTipoCompra
	,FlMultiple
	,NuDocum_Multiple
	,CoTipDocSusEnt
	,NuDocumSusEnt
	,NuOperBanSusEnt
	,IDDocFormaEgreso
	)        
	Values        
	(        
	@NuDocumProv,        
	Case When @NuVoucher=0 Then Null Else @NuVoucher End,         
	Case When @NuOrden_Servicio=0 Then Null Else @NuOrden_Servicio End,  
	Case When Ltrim(Rtrim(@NuSerie))='' Then Null Else @NuSerie End,        
	Case When Ltrim(Rtrim(@NuDocum))='' Then @NuDocum2 Else @NuDocum End, --@NuDocum,        
	@NuDocInterno,        
	@CoTipoDoc,        
	--@FeEmision,        
	Case When @FeEmision='01/01/1900' Then getdate() Else @FeEmision End,  
	--@FeRecepcion,        
	Case When @FeRecepcion='01/01/1900' Then getdate() Else @FeRecepcion End,  
	--Case When @CoTipoDetraccion='' Then Null Else @CoTipoDetraccion End,                         
	@CoTipoDetraccion,        
	@CoMoneda,        
	@SsIGV,        
	@SsNeto,        
	Case When @SsOtrCargos=0 Then Null Else @SsOtrCargos End,         
	Case When @SsDetraccion=0 Then Null Else @SsDetraccion End,         
	@SSTotalOriginal,        
	Case When @SSTipoCambio=0 Then Null Else @SSTipoCambio End,         
	Case When @SSTotal=0 Then Null Else @SSTotal End,         
	--@CoEstado,        
	@UserMod,        
	Case When Ltrim(Rtrim(@CoTipoOC))='' Then Null Else @CoTipoOC End,        
	Case When Ltrim(Rtrim(@CoCtaContab))='' Then Null Else @CoCtaContab End,        
	Case When Ltrim(Rtrim(@CoObligacionPago))='' Then Null Else @CoObligacionPago End,        
	Case When Ltrim(Rtrim(@CoMoneda_Pago))='' Then Null Else @CoMoneda_Pago End,      
	Case When Ltrim(Rtrim(@CoMoneda_FEgreso))='' Then Null Else @CoMoneda_FEgreso End,      
	Case When @SsTipoCambio_FEgreso =0 then Null Else @SsTipoCambio_FEgreso End,      
	Case When @SsTotal_FEgreso=0 Then Null Else @SsTotal_FEgreso End,      
	Case When Ltrim(Rtrim(@UserNuevo))='' Then Null Else @UserNuevo End,      
	Case When @CoOrdPag=0 Then Null Else @CoOrdPag End,      
	Case When @IDOperacion=0 Then Null Else @IDOperacion End,  
	Case When @NuOrdComInt=0 Then Null Else @NuOrdComInt End,  
      
	Case When @NuVouOfiExtInt=0 Then Null Else @NuVouOfiExtInt End,  
	Case When Ltrim(Rtrim(@NuDocumVta))='' Then Null Else @NuDocumVta End,      
	Case When Ltrim(Rtrim(@CoTipoDocVta))='' Then Null Else @CoTipoDocVta End,  
	Case When @NuPreSob=0 Then Null Else @NuPreSob End,  
	Case When @NuVouPTrenInt=0 Then Null Else @NuVouPTrenInt End,
	Case When Ltrim(Rtrim(@CoCeCos))='' Then Null Else @CoCeCos End,
	Case When @NuFondoFijo=0 Then Null Else @NuFondoFijo End, 
	Case When Ltrim(Rtrim(@CoTipoFondoFijo))='' Then Null Else @CoTipoFondoFijo End,
	Case When @SSPercepcion=0 Then Null Else @SSPercepcion End, 
	Case When Ltrim(Rtrim(@TxConcepto))='' Then Null Else @TxConcepto End, 
	Case When Ltrim(Rtrim(@CoProveedor))='' Then Null Else @CoProveedor End,
	Case When @IDCab=0 Then Null Else @IDCab End,
	--
	Case When @FeVencimiento='01/01/1900' Then getdate() Else @FeVencimiento End
	--,Case When @CoTipoDocSAP=0 Then Null Else @CoTipoDocSAP End
	,@CoTipoDocSAP
	,Case When Ltrim(Rtrim(@CoFormaPago))='' Then Null Else @CoFormaPago End
	,Case When Ltrim(Rtrim(@CoTipoDoc_Ref))='' Then Null Else @CoTipoDoc_Ref End
	,Case When Ltrim(Rtrim(@NuSerie_Ref))='' Then Null Else @NuSerie_Ref End 
	,Case When Ltrim(Rtrim(@NuDocum_Ref))='' Then Null Else @NuDocum_Ref End 
	,Case When @FeEmision_Ref='01/01/1900' Then Null Else @FeEmision_Ref End 
	,Case When Ltrim(Rtrim(@CoCeCon))='' Then Null Else @CoCeCon End 
	,Case When Ltrim(Rtrim(@CoCtaContab_SAP))='' Then Null Else @CoCtaContab_SAP End 
	,Case When Ltrim(Rtrim(@CoGasto))='' Then Null Else @CoGasto End 
	,Case When Ltrim(Rtrim(@NuDocum))='' Then CAST(1 AS BIT) Else CAST(0 AS BIT) End
	,isnull(@CoTipoCompra,'*'),
	@FlMultiple
	,Case When @NuDocum_Multiple=0 Then Null Else @NuDocum_Multiple End
	,Case when ltrim(rtrim(@CoTipDocSusEnt))='' then null else @CoTipDocSusEnt end
	,Case when ltrim(rtrim(@NuDocumSusEnt ))='' then null else @NuDocumSusEnt end
	,Case when ltrim(rtrim(@NuOperBanSusEnt ))='' then null else @NuOperBanSusEnt end
	, @IDDocFormaEgreso
	) 
	
	set @pNuDocumProv=@NuDocumProv;



