﻿CREATE Procedure dbo.ActualizarNotaVentaxAnios
As
Begin
Set NoCount On
Set Dateformat dmy
Declare @AnioTmp char(4)=''
Declare curUpdateNotaVenta cursor for
Select distinct Year(FecInicio) from COTICAB
where Year(FecInicio) > 2013
Order By 1
Open curUpdateNotaVenta
Fetch Next from curUpdateNotaVenta into @AnioTmp
While @@FETCH_STATUS = 0
Begin
	Execute dbo.GrabarNotaVenta @AnioTmp,''

	Fetch Next from curUpdateNotaVenta into @AnioTmp
End
Close curUpdateNotaVenta
DealLocate curUpdateNotaVenta
End
