﻿CREATE Procedure [dbo].[MACLIENTES_Anu]
	@IDCliente char(6),
	@UserMod	char(4)
As

	Set NoCount On
	UPDATE MACLIENTES SET           
		Activo='I'
	    ,UserMod=@UserMod
        ,FecMod=getdate()
     WHERE
           IDCliente = @IDCliente

	--Update MACLIENTES Set	Activo='I',
	--UserMod=@UserMod ,FecMod=getdate() 
	--Where Relacion=@IDCliente
