﻿Create Procedure dbo.COTICAB_SelFechaOutxDebitMemoOutput
	@IDDebitMemo char(10),
	@pFechaOut smalldatetime Output
As
	Set Nocount On
	Set @pFechaOut='01/01/1900'
	
	Select @pFechaOut=FechaOut From COTICAB 
		Where IDCAB=(Select IDCAB From DEBIT_MEMO Where IDDebitMemo=@IDDebitMemo)
		And EstadoFacturac='P'