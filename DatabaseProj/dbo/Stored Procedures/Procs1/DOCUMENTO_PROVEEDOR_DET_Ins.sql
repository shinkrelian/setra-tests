﻿--JHD-20150806-Se agregaron campos nuevos para SAP
--JHD-20150928-@QtCantidad int=0,
--JHD-20160125- case when LTRIM(RTRIM(@CoCeCos)) ='' then null else @CoCeCos end,
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_DET_Ins]
	@NuDocumProv int,
	--@NuDocumProvDet tinyint,
	@IDServicio_Det int=0,
	@TxServicio varchar(max)='',
	@SsMonto numeric(9,2)=0,
	--@QtCantidad tinyint=0,
	@QtCantidad int=0,
	@UserMod char(4)='',
	@IDCab int=0,
	@CoProducto char(6)='',
	@CoAlmacen char(6)='',
	@SsIGV numeric(8,2)=0,
	@CoTipoOC char(3)='',
	@CoCtaContab varchar(14)='',
	@CoCeCos char(6)='',
	@CoCeCon char(6)='',
	@SSPrecUni numeric(9,2)=0,
	@SSTotal numeric(9,2)

AS
BEGIN

Declare @NuDocumProvDet tinyint =(select Isnull(Max(NuDocumProvDet),0)+1 
								from DOCUMENTO_PROVEEDOR_DET where NuDocumProv=@NuDocumProv )


	Insert Into dbo.DOCUMENTO_PROVEEDOR_DET
		(
			NuDocumProv,
 			NuDocumProvDet,
 			IDServicio_Det,
 			TxServicio,
 			SsMonto,
 			QtCantidad,
 			UserMod,

			IDCab,
 			CoProducto,
 			CoAlmacen,
 			SsIGV,
 			CoTipoOC,
 			CoCtaContab,
 			CoCeCos,
 			CoCeCon,
 			SSPrecUni,
			SSTotal
 		)
	Values
		(
			@NuDocumProv,
 			@NuDocumProvDet,
 			Case When @IDServicio_Det=0 Then Null Else @IDServicio_Det End,
 			case when LTRIM(RTRIM(@TxServicio)) ='' then null else @TxServicio end,

 			Case When @SsMonto=0 Then Null Else @SsMonto End,
 			Case When @QtCantidad=0 Then Null Else @QtCantidad End,
 			@UserMod,
			Case When @IDCab=0 Then Null Else @IDCab End,
 			case when LTRIM(RTRIM(@CoProducto)) ='' then null else @CoProducto end,
 			case when LTRIM(RTRIM(@CoAlmacen)) ='' then null else @CoAlmacen end,
 			Case When @SsIGV=0 Then Null Else @SsIGV End,

 			case when LTRIM(RTRIM(@CoTipoOC)) ='' then null else @CoTipoOC end,
 			case when LTRIM(RTRIM(@CoCtaContab)) ='' then null else @CoCtaContab end,
 			case when LTRIM(RTRIM(@CoCeCos)) ='' then null else @CoCeCos end,
 			case when LTRIM(RTRIM(@CoCeCon)) ='' then null else @CoCeCon end,

 			Case When @SSPrecUni=0 Then Null Else @SSPrecUni End,
			Case When @SSTotal=0 Then Null Else @SSTotal End
 		)
END;
