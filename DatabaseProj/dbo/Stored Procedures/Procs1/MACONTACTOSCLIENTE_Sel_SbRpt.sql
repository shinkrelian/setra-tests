﻿CREATE procedure [dbo].[MACONTACTOSCLIENTE_Sel_SbRpt] 
	@Persona char(1),
	@Tipo char(1),
	@IDVendedor char(6),
	@IDPais char(6),
	@IDCiudad char (6),
	@Domiciliado bit 
as
	Set NoCount On
	
	select NroFila,IDCLiente,Nombre_Completo,Cargo,Celular,Email,Numero
	from(
	select ROW_NUMBER() over (order by Nombre_Completo) as NroFila ,y.*
	from (
	select cc.IDCliente,CAST(Titulo+' '+Nombres+' '+Apellidos as varchar) as Nombre_Completo,
	     Cargo,cc.Celular,cc.Email,
	     Numero=CAST(cc.Anexo+'-'+cc.Telefono as varchar)
	from MACONTACTOSCLIENTE cc 
	Where cc.IDCliente In (select c.IDCliente from MACLIENTES c
	Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo
		Left Join MAUBIGEO up On u.IDPais=up.IDubigeo
	Left Join MACONTACTOSCLIENTE cc On c.IDCliente=cc.IDCliente 
	--Left join MAMONEDAS mo On mo.IDMoneda=c.IDMoneda 
	where (c.Persona=@Persona Or LTRIM(RTRIM(@Persona))='') And 
			(c.Tipo=@Tipo Or LTRIM(RTRIM(@Tipo))='') And 
			(c.IDvendedor=@IDVendedor Or LTRIM(RTRIM(@IDVendedor))='') And
			(u.IDPais=@IDPais Or LTRIM(RTRIM(@IDPais))='') And
			(c.IDCiudad=@IDCiudad Or LTRIM(RTRIM(@IDCiudad))='') And
			(c.Domiciliado=@Domiciliado)
	
	)
	) as y
	) as x
