﻿CREATE Procedure [dbo].[MADESAYUNOS_Sel_Lvw]  
 @IDDesayuno char(2),  
 @Descripcion varchar(50)  
As  
 Set NoCount On  
   
 Select Descripcion   
 From MADESAYUNOS  
 Where (Descripcion Like '%'+@Descripcion+'%')  
 And (IDDesayuno<>@IDDesayuno Or ltrim(rtrim(@IDDesayuno))='')  
 Order by Descripcion
