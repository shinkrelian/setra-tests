﻿CREATE Procedure [dbo].[MACATEGORIA_Sel_CboMargen]
	@bTodos	bit
As
	Set NoCount On
	
	SELECT * FROM
	(	
	Select '' as IdCat,'<TODOS>' as Descripcion, 0 as Ord
	Union
	Select IdCat, Descripcion+SPACE(100)+'|'+cast(Comision as varchar(6)),1 
	From MACATEGORIA
	) AS X
	Where (@bTodos=1 Or Ord<>0)
	Order by Ord,2
