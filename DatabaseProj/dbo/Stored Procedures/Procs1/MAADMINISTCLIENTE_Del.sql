﻿Create Procedure [dbo].[MAADMINISTCLIENTE_Del]
	@IDAdminist char(2),
	@IDCliente char(6)
As

	Set NoCount On
	
	Delete From MAADMINISTCLIENTE 
	WHERE
	IDAdminist=@IDAdminist And IDCliente=@IDCliente
