﻿
/*
declare @co numeric(8,2)
execute COTIVUELOS_SelMontoOrginal_Pk 43501, @co output
select @co
*/

CREATE PROCEDURE [dbo].[COTIVUELOS_SelMontoOrginal_Pk]
						@ID						int,
						@pCostoOperadorOriginal	numeric(8,2) output
AS
BEGIN
	SET NOCOUNT ON
	SELECT @pCostoOperadorOriginal = ISNULL(CostoOperadorOriginal,0)
	FROM COTIVUELOS
	WHERE ID=@ID
END
