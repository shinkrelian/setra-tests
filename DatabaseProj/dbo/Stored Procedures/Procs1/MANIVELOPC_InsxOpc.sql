﻿CREATE PROCEDURE [dbo].[MANIVELOPC_InsxOpc]
	@IDOpc	char(6),
	@IDUsuario	char(4)
As
	Set NoCount On
	
	Insert Into MANIVELOPC(IDOpc,IDNivel,Consultar, Grabar, Actualizar, Eliminar, UserMod)
	Select @IDOpc,IDNivel,0,0,0,0,@IDUsuario  From MANIVELUSUARIO
