﻿
CREATE PROCEDURE [dbo].[MACONTACTOSCLIENTE_Sel_Pk_InvoicePaymentwall]	
	@IDCliente char(6),
	@IDContacto char(3)
AS
BEGIN
	Set NoCount On

	Select ISNULL(CC.Email,'') AS email,
		   CC.Nombres AS first_name,
		   CC.Apellidos AS last_name,
		   CL.RazonSocial AS company_name,
		   CC.Titulo AS salutation
	FROM MACONTACTOSCLIENTE CC INNER JOIN MACLIENTES CL ON CC.IDCliente = CL.IDCliente
    WHERE CC.IDContacto=@IDContacto And CC.IDCliente=@IDCliente

END
