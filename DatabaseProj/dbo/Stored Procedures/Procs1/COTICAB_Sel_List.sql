﻿--JRF-20121204- Nueva Columna IDCliente para Anticipos                  
--HLF-20130125- Order by                
--JRF-20130227- Habilitar la opcion de Estado 'X'              
--JRF-20130301- Mostrando el Estado.            
--JRF-20130301- Agregando la columna de IDUSUARIO Ventas          
--JRF-20131017- Agregando Filtro de Fechas Inicio de Cotizacion.        
--JRF-20131018- Agregando Filtro de Fechas por asignación de ejecutivo de Reservas.        
--JRF-20140325- Agregnado la columna DocWordVersion    
--JRF-HLF-20140514- Optimizar el --exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB x El IDCAB in (...)  
--JRF-20140519-Agregar la fecha de Asignación de Reservas
--JHD-20150716-Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,
--JHD-20150716- ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes,    
--JHD-20150716- uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe
--HLF-20151201-And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)    
--HLF-20160216-isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt, Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt
--HLF-20160223-@CoTipoAPT char(1)=''	And (ltrim(rtrim(@CoTipoAPT))='' or exists
--JRF-20160411-Cambiar la función FnDestinosDia X Nueva Columna
CREATE Procedure [dbo].[COTICAB_Sel_List]                          
 @IDUsuario char(4),                          
 @Cotizacion char(8),                          
 @IDFile char(8),                          
 @DescCliente varchar(60),                          
 @Titulo varchar(100),                          
 @Estado char(1),                      
 @NombrePax varchar(100),        
 @FecIniciRango1 smalldatetime,                        
 @FecIniciRango2 smalldatetime,        
 @FecAsigReserRango1 smalldatetime,        
 @FecAsigReserRango2 smalldatetime,
 @FlHistorico bit=1,
 @CoTipoAPT char(2)=''
As                          
 Set NoCount On                          
 Select UsuarioVen,UsuarioRes,IDUsuario,CorreoEjecReservas,UsuarioOpe,Fecha,FechaAsigReserva,Cotizacion,IDFile,NuPedido,NuPedInt,DescCliente,Titulo,        
 DocWordVersion,Ruta,Estado,IDCAB,NroPax,Observaciones,IDCliente        
 from (        
 --Select uv.Nombre as UsuarioVen,          
 --Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,      
 Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,      
     
 --ur.Nombre as UsuarioRes,                          
 ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes,                          
 uv.IDUsuario,          
 IsNull(ur.Correo,'') as CorreoEjecReservas,                          
 --uo.Nombre as UsuarioOpe                          
 uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe                          
 , Convert(varchar,c.Fecha,103) as Fecha,    
 --Case When FechaAsigReserva is null then CAST('01/01/1900' as smalldatetime) else Cast(convert(char(10),FechaAsigReserva,103) as smalldatetime) End As FechaAsigReserva    ,                          
 c.FechaAsigReserva,
 c.Cotizacion,c.IDFile,
 isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt,
 cl.RazonComercial as DescCliente, c.Titulo,                          
 --Cast((DATEDIFF(d,c.FecInicio,c.FechaOut)+1) as varchar(2))+' '+ dbo.FnDestinosDia(c.IDCAB,0) as Ruta,                          
 Cast((DATEDIFF(d,c.FecInicio,c.FechaOut)+1) as varchar(2))+' '+ IsNull(c.NoRuta,'') as Ruta,
 c.Estado ,                          
 c.IDCAB,                          
 c.NroPax ,c.Observaciones ,c.IDCliente ,        
 CAST(c.DocWordVersion as varchar(2)) as DocWordVersion    
 From COTICAB c Left Join MAUSUARIOS uv on c.IDUsuario=uv.IDUsuario                          
 Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente                          
 Left Join MAUSUARIOS ur On c.IDUsuarioRes = ur.IDUsuario                          
 Left Join MAUSUARIOS uo On c.IDUsuarioOpe = uo.IDUsuario           
 Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt
 Where (c.IDUsuario=@IDUsuario Or ltrim(rtrim(@IDUsuario))='') And                          
 (c.Cotizacion Like '%'+ltrim(rtrim(@Cotizacion))+'%' Or ltrim(rtrim(@Cotizacion))='') And                          
 (c.IDUsuario=@IDUsuario Or ltrim(rtrim(@IDUsuario))='') And                          
 (c.IDFile Like '%'+ ltrim(rtrim(@IDFile))+'%' Or ltrim(rtrim(@IDFile))='') And                          
 (cl.RazonComercial Like '%'+@DescCliente+'%' Or ltrim(rtrim(@DescCliente))='') And                          
 (c.Titulo Like '%'+@Titulo+'%' Or ltrim(rtrim(@Titulo))='') And                           
 (c.Estado=@Estado Or ltrim(rtrim(@Estado))='') And                                      
 --exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB                       
 c.IDCAB in (select px.IDCab from COTIPAX px Where px.IDCab=c.IDCAB                       
 And (px.Nombres+' '+px.Apellidos) Like '%'+@NombrePax+'%' Or ltrim(rtrim(@NombrePax))='')        
 --And CHARINDEX(@NombrePax,(px.Nombres+' '+px.Apellidos))>0 Or ltrim(rtrim(@NombrePax))='')  
 And ((LTRIM(rtrim(convert(char(10),@FecIniciRango1,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecIniciRango2,103)))='01/01/1900') Or        
   cast(convert(char(10),c.FecInicio,103) as smalldatetime) between @FecIniciRango1 and @FecIniciRango2)        
  And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)
  And (ltrim(rtrim(@CoTipoAPT))='' or 
	(exists(Select iddet From COTIDET cd1 Inner Join MASERVICIOS s1 On cd1.IDServicio=s1.IDServicio 
		Where IDCAB=c.IDCAB And s1.APTServicoOpcional=Case @CoTipoAPT When 'SE' Then 'E' When 'FC' then 'F' End) 
		and DBO.FnDatosIncompletosPaxxFile(c.IDCAB)=0))
 )as X        
 Where ((LTRIM(rtrim(convert(char(10),@FecAsigReserRango2,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecAsigReserRango2,103)))='01/01/1900') Or        
  cast(convert(char(10),X.FechaAsigReserva,103) as smalldatetime) between @FecAsigReserRango1 and @FecAsigReserRango2)        
 Order by X.Cotizacion Desc                

