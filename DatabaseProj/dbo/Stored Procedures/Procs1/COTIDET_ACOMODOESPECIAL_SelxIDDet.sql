﻿--HLF-20140109-Order by cd.CoCapacidad, cd.QtPax      
--JRF-20140318-Nuevo campo Matrimonial (Bit)    
--JRF-20140716-aGREGAR EL IDCab  
--JRF-20140721-Ordenar por FlMatrimonial.
--JRF-20150903-Nuevo Campo Guide
CREATE Procedure [dbo].[COTIDET_ACOMODOESPECIAL_SelxIDDet]
 @IDDet int      
As      
 Set Nocount On      
 Select cdd.IDCAB,cd.CoCapacidad, cd.QtPax, cap.QtCapacidad, cap.NoCapacidad,cap.FlMatrimonial ,cap.FlGuide
 From COTIDET_ACOMODOESPECIAL cd       
 Inner Join CAPACIDAD_HABITAC cap On cd.CoCapacidad=cap.CoCapacidad      
 Left Join COTIDET cdd On cd.IDDet = cdd.IDDET  
 Where cd.IDDet=@IDDet      
 Order by cap.FlMatrimonial asc,cd.CoCapacidad ,cd.QtPax
