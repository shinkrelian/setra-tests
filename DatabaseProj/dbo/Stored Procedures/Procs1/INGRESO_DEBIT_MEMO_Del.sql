﻿Create Procedure dbo.INGRESO_DEBIT_MEMO_Del
@NuIngreso int,
@IDDebitMemo char(10)
As
Begin
	Set NoCount On
	Delete from INGRESO_DEBIT_MEMO where NuIngreso = @NuIngreso and IDDebitMemo = @IDDebitMemo
End
