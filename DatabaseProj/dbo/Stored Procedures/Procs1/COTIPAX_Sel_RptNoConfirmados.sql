﻿CREATE Procedure dbo.COTIPAX_Sel_RptNoConfirmados  
@IDFile varchar(8),  
@FecIngresoIn smalldatetime,  
@FecIngresoOut smalldatetime  
As  
Set NoCount On  
Select X.* from (  
Select c.IDFile,ti.Descripcion+': '+ Isnull(cp.NumIdentidad,'') as IDentidad,cp.Apellidos,cp.Nombres,  
u.DC as Nac,Isnull(cp.FecIngresoPais,c.FecInicio) as FechaIngreso,c.FecInicio,c.FechaOut  
from COTIPAX cp Left Join COTICAB c On cp.IDCab=c.IDCAB  
Left Join MATIPOIDENT ti On cp.IDIdentidad=ti.IDIdentidad  
Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo  
where cp.PassValidado=0 --and cp.IDCab=(select IDCab from COTICAB where IDFile = @IDFile)  
and (Ltrim(Rtrim(@IDFile))='' Or c.IDFile=@IDFile) and c.Estado='A')  
as X  
Where (Convert(char(10),@FecIngresoIn,103)='01/01/1900' Or X.FechaIngreso between @FecIngresoIn and @FecIngresoOut)  
Order by X.FechaIngreso,X.IDFile  
