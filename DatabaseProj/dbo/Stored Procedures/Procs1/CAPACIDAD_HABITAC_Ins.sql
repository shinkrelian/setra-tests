﻿Create Procedure dbo.CAPACIDAD_HABITAC_Ins
@NoCapacidad varchar(50),
@QtCapacidad tinyint,
@UserMod char(4),
@pCoCapacidad char(2) output
As
	Set NoCount On
	Declare @CoCapacidad char(2)
	Execute dbo.Correlativo_SelOutput 'CAPACIDAD_HABITAC',1,2,@CoCapacidad output
	Insert Into CAPACIDAD_HABITAC (CoCapacidad,NoCapacidad,QtCapacidad,UserMod,FecMod)
		    values (@CoCapacidad,@NoCapacidad,@QtCapacidad,@UserMod,Getdate())
	Set @pCoCapacidad = @CoCapacidad		    
