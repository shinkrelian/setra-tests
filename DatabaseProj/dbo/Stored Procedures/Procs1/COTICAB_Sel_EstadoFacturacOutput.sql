﻿CREATE Procedure [dbo].[COTICAB_Sel_EstadoFacturacOutput]
	@IDCab	int,
	@pEstadoFacturac char(1)	Output
As
	Set NoCount On
	
	Set @pEstadoFacturac=''
	
	Select @pEstadoFacturac=EstadoFacturac From COTICAB 
		Where IDCab=@IDCab

