﻿Create Procedure dbo.COTICAB_UpdFlCorreoAnuEnviadoxIDCab
@IDCab int,
@UserMod char(4)
As
Set NoCount On
Update COTICAB 
	set FlCorreoAnuEnviado=1,
		UserMod=@UserMod,
		FecMod=GETDATE()
where IDCAB=@IDCab
