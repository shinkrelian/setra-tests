﻿CREATE Procedure [dbo].[MAIDIOMASPROVEEDOR_Sel_List]
	@IDProveedor	char(6),
	@IDIdioma	varchar(12)
As
	Set Nocount On
	
	Select IDIdioma, Nivel as IDNivel,	
	Case Nivel 
		When 'B' Then 'BÁSICO'
		When 'II' Then 'INTERMEDIO BÁSICO'
		When 'I' Then 'INTERMEDIO'
		When 'IS' Then 'INTERMEDIO AVANZADO'
		When 'A' Then 'AVANZADO'
	End As Nivel 
	From MAIDIOMASPROVEEDOR 
	Where IDProveedor=@IDProveedor And
		(IDIdioma Like '%'+ @IDIdioma +'%' Or LTRIM(RTRIM(@IDIdioma))='')
	Order by IDIdioma
