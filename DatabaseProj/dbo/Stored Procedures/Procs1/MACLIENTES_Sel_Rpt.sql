﻿--JRF-20140711-Agregar filtro por el nuevo campo CoTipoVenta
CREATE Procedure [dbo].[MACLIENTES_Sel_Rpt]  
 @RazonSocial varchar(60),  
 @IDPais char(6),  
 @Contacto varchar(100),  
 @Top tinyint,
 @CoTipoVenta char(2)
AS  
 Set NoCount On  
   
 SELECT  NumIdentidad,RazonSocial,Email,Direccion,Numeros,  
 Ciudad,Pais,IDubigeoPais,IDIdentidad,Status,IDCliente  
 FROM   
 (  
 SELECT row_number() over (order by RazonSocial, Nivel) as NroFila ,X.*   
 FROM   
 (    
 Select Distinct  
 c.NumIdentidad ,   
 RazonSocial,  
 '' as Email,    
 c.Direccion,  
 u.Descripcion as Ciudad,  
 up.Descripcion as Pais,   
 up.IDubigeo as IDubigeoPais,   
 Numeros=   
 Case When Right(isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,''),1)='/' Then   
  isnull(c.Telefono1,'')  
 Else  
  Case When Left(isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,''),1)='/' Then   
   isnull(c.Telefono2,'')  
   else  
    isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,'')  
  End  
 End  
 ,  
 c.IDIdentidad ,   
 Case c.Tipo When 'I' then 'INTERESADO' When 'C' then 'CLIENTE' End as Status,  
 --Case When Nivel='P' Then 'PRINCIPAL' Else 'SEDE' End as Nivel,  
 'PRINCIPAL' as Nivel,  
 c.IDCliente  
 From MACLIENTES c Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo  
 Left Join MAUBIGEO up On u.IDPais=up.IDubigeo  
 Left Join MACONTACTOSCLIENTE cc On c.IDCliente=cc.IDCliente  
 Where    
 (c.RazonSocial Like '%'+@RazonSocial+'%' Or ltrim(rtrim(@RazonSocial))='')   
 And c.Activo='A'  
 And (ltrim(rtrim(@IDPais))='' Or up.IDubigeo=@IDPais)  
 And ((cc.Nombres+ ' ' + cc.Apellidos Like '%'+@Contacto+'%' Or ltrim(rtrim(@Contacto))=''))  
 And (c.CoTipoVenta = @CoTipoVenta)
 --Or (c.RazonSocial Like '%'+@Contacto+'%' Or ltrim(rtrim(@Contacto))=''))  
 ) AS X   
 ) AS Y   
 WHERE (Y.NROFILA<=@Top Or @Top=0)  
 Order by RazonSocial  
