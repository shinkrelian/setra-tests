﻿--JHD-20160306-Consultar=isnull(uo.Consultar, cast(0 as bit))
CREATE Procedure [dbo].[MAOPCIONES_SelAccionesxUsuario_Menu]
	@IDUsuario	char(4)	
As
	Set NoCount On
	

	Select mo.IDOPc,mo.Nombre,
	Consultar=isnull(uo.Consultar, cast(0 as bit))
	From MAOPCIONES mo Left Join MAUSUARIOSOPC uo On mo.IDOPc=uo.IDOpc And uo.IDUsuario=@IDUsuario
	--Where UO.Consultar=1

