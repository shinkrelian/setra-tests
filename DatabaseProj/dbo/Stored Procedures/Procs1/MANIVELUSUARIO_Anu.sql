﻿Create Procedure [dbo].[MANIVELUSUARIO_Anu]
	@IDNivel char(3),
	@UserMod char(4)
As
	Set NoCount On
	
	Update MANIVELUSUARIO Set Activo='I',
	UserMod=@UserMod,FecMod=getdate()
    Where IDNivel=@IDNivel
