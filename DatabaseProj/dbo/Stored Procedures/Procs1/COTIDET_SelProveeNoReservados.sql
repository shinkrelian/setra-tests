﻿
--HLF-20130610-Usando IDServicio_Det en vez de IDDET y comentando And IDDetRel IS NULL    
--HLF-20130701-Agregando columna IDReserva y Select As X  
--JRF-20150126-Validar la columna de Anulado para RESERVAS_DET y RESERVAS
--JRF-20150413-Adicionar el Con Alojamiento
--PPMG-20160330-AND cd.IDProveedor <> '003557' Proveedor Zicasso
CREATE Procedure [dbo].[COTIDET_SelProveeNoReservados]    
 @IDCab int    
AS
BEGIN
	DECLARE @IDProveedor_Zicasso char(6) = '003557'

 Set Nocount On    
   
 SELECT * FROM  
 (    
 Select distinct cd.IDProveedor,dbo.FnExistenAlojamientoVentas(cd.IDProveedor,cd.IDCAB) As ExisteAlojamiento,
			isnull(
			(Select top 1 IDReserva From RESERVAS r2 Where r2.IDProveedor=cd.IDProveedor and r2.IDCab=@IDCab and r2.Anulado=0)
			,0) as IDReserva  
			 From COTIDET  cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
			 Where cd.IDCAB=@IDCab --And IDDetRel IS NULL    
			 AND cd.IDServicio_Det Not in (Select rd.IDServicio_Det From RESERVAS_DET rd
			 Where rd.Anulado=0 and rd.IDReserva In(Select r.IDReserva From RESERVAS r Where r.IDCab=@IDCab and r.Anulado=0)
			 AND p.IDProveedor <> @IDProveedor_Zicasso)   
) AS X  
WHERE IDReserva=0  
END
