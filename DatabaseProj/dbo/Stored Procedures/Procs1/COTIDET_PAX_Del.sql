﻿CREATE Procedure [dbo].[COTIDET_PAX_Del]
	@IDDet	int,
	@IDPax	int,
	@UserMod	char(4)
As
	Set NoCount On
	
	Delete From COTIDET_PAX Where 
	IDDet=@IDDet And IDPax=@IDPax
	
	Update COTIPAX Set Actualizar=1,UserMod=@UserMod,FecMod=GETDATE() Where IDPax=@IDPax
