﻿Create Procedure [dbo].[MACLIENTES_Del]
	@IDCliente char(6)
AS
	Set NoCount On
	
	Delete From MACLIENTES Where IDCliente = @IDCliente
