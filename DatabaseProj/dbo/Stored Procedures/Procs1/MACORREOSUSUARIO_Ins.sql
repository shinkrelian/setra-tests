﻿

Create Procedure dbo.MACORREOSUSUARIO_Ins
	@IDUsuario char(4),
	@Correo varchar(150),
	@Descripcion varchar(max),	
	@UserMod char(4)
As
	Set Nocount on
	
	Insert Into MACORREOSUSUARIO
	(IDUsuario, Correo, Descripcion, UserMod)
	Values
	(@IDUsuario, @Correo, 
	Case When ltrim(rtrim(@Descripcion))='' Then Null Else @Descripcion End, 
	@UserMod)
