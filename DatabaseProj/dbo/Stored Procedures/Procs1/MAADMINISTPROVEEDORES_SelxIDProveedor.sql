﻿--JRF-20141112-Agregando la columna CoCBU
CREATE Procedure [dbo].[MAADMINISTPROVEEDORES_SelxIDProveedor]     
 @IDProveedor CHAR(6)      
As      
 Set NoCount On      
     
  Select IDAdminist, Banco as IDBanco,    
   Case when Banco <> '000' Then b1.Descripcion Else BancoExtranjero End as Banco ,     
   ma.CtaCte, ma.IDMoneda, mo.Descripcion as Moneda,      
   ma.SWIFT, IVAN, ApellidosTitular, NombresTitular,  
   NombresTitular+' '+isnull(ApellidosTitular,'') as NombreCompleto,      
  ReferenciaAdminist, ma.CtaInter, BancoInterm as IDBancoInterm,Case  When BancoInterm  <> '000' then b2.Descripcion else BancoInterExtranjero End as BancoInterm  ,    
      
  TitularBenef,TipoCuentBenef,NroCuentaBenef,DireccionBenef,     
  NombreBancoPag,PECBancoPag,DireccionBancoPag,SWIFTBancoPag,    
  EsOpcBancoInte,    
  NombreBancoInte,PECBancoInte,SWIFTBancoInte,CuentBncPagBancoInte    
  ,TipoCuenta  ,IsNull(CoCBU,'') as CoCBU
  --,'' as btnDel      
  From MAADMINISTPROVEEDORES ma Left Join MAMONEDAS mo On ma.IDMoneda=mo.IDMoneda      
  Left Join MABANCOS b1 On ma.Banco=b1.IDBanco      
  Left Join MABANCOS b2 On ma.BancoInterm=b2.IDBanco      
  Where ma.IDProveedor = @IDProveedor    
  Order By 1    
