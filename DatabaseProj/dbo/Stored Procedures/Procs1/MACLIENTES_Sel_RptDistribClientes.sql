﻿Create procedure dbo.MACLIENTES_Sel_RptDistribClientes
@IDvendedor char(4)
as
Set Nocount On
Select u.Descripcion as Pais,c.RazonComercial as DescCliente,
	Case c.Complejidad
		when 'A' Then 'ALTA'
		when 'M' Then 'MEDIANA'
		when 'B' Then 'BAJA'
		when 'N' Then 'NIGUNA'
	End as Complejidad,
	Case c.Frecuencia
	when 'A' Then 'ALTA'
		when 'M' Then 'MEDIANA'
		when 'B' Then 'BAJA'
		when 'N' Then 'NIGUNA'
	End as Frecuencia ,
	Case When Solvencia = 0 Then 'NINGUNA' Else REPLICATE('*',Solvencia) End as Solvencia,
	Case When c.Credito = 1 then 'SI' Else 'NO' End as Credito,UPPER(us.Nombre) As UsuarioVendedor,
	Case when c.Complejidad = 'A' Then 1 else 0 End as CountComplejidad_Alta,Case when c.Complejidad = 'M' Then 1 else 0 End as CountComplejidad_Media,
	Case when c.Complejidad = 'B' Then 1 else 0 End as CountComplejidad_Baja,Case when c.Complejidad = 'N' Then 1 else 0 End as CountComplejidad_Ninguna,
	Case when c.Frecuencia = 'A' Then 1 else 0 End as CountFrecuencia_Alta,Case when c.Frecuencia = 'M' Then 1 else 0 End as CountFrecuencia_Media,
	Case when c.Frecuencia = 'B' Then 1 else 0 End as CountFrecuencia_Baja,Case when c.Frecuencia = 'N' Then 1 else 0 End as CountFrecuencia_Ninguna
from MACLIENTES c Left Join MAUBIGEO u On c.IDCiudad = u.IDubigeo
Left Join MAUSUARIOS us On c.IDvendedor = us.IDUsuario
Where c.Activo = 'A' and (LTRIM(rtrim(@IDvendedor))='0000' Or c.IDvendedor = @IDvendedor) and c.IDvendedor <> '0000'
Order by us.Nombre
