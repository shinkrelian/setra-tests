﻿Create Procedure [dbo].[MABANCOS_Del]
	@IDBanco char(3)
As

	Set NoCount On

	Delete From MABANCOS
	Where IDBanco = @IDBanco
