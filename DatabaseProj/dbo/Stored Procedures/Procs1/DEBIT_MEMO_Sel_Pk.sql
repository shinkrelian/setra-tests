﻿--JRF-20130704-Quitando el campo de Serie.  
--JRF-20130902-Agregando el Nuevo Campo IDCliente.
--PPMG-20160111-Se agrego el campo FlEnvioInvoice
--JHD-20160211-Se agrego el campo IDMotivo
--JHD-20160212-Se agregaron los campos IDResponsable y IDSupervisor
--JHD-20160212-,Supervisor=(select IsNull(Nombre,'') + ' ' +IsNuLL(TxApellidos,'') from MAUSUARIOS where IDUsuario=dm.IDSupervisor)
--JHD-20160215-Se agrego el campo TxObservacion
CREATE Procedure [dbo].[DEBIT_MEMO_Sel_Pk]    
@IDDebitMemo char(10)    
AS
BEGIN
 Set NoCount On    
 SELECT dm.[IDDebitMemo]    
    ,dm.[Fecha]    
    ,[FecVencim]    
    ,dm.[IDCab]    
    ,[IDMoneda]    
    ,[SubTotal]    
    ,[TotalIGV]    
    ,[Total]    
    ,dm.[IDBanco]    
    ,[IDEstado]    
    ,[IDTipo]  ,  
    dm.FechaIn,dm.FechaOut,dm.Cliente,dm.Pax,dm.Titulo,dm.Responsable  
    ,Isnull(dm.IDCliente,'') as IDCLiente
	, ISNULL(FlEnvioInvoice,0) AS FlEnvioInvoice
	, ISNULL(IDMotivo,0) AS IDMotivo

	,Isnull(dm.IDResponsable,'') as IDResponsable
	,Isnull(dm.IDSupervisor,'') as IDSupervisor
	,Supervisor=isnull((select IsNull(Nombre,'') + ' ' +IsNuLL(TxApellidos,'') from MAUSUARIOS where IDUsuario=dm.IDSupervisor),'')
	,Isnull(dm.TxObservacion,'') as TxObservacion
  FROM [dbo].[DEBIT_MEMO] dm Left Join COTICAB c On dm.IDCab = c.IDCAB    
  Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente    
  Left Join MAUSUARIOS u on c.IDUsuario = u.IDUsuario    
  WHERE dm.[IDDebitMemo] = @IDDebitMemo    
END
