﻿CREATE Procedure [dbo].[COTI_CATEGORIAHOTELES_Sel_xIDCab]
	@IDCab	int
As
	Set NoCount On

	Select u.IDPais, ch.IDUbigeo,u.Descripcion as DescUbigeo,ch.Categoria,ch.IDProveedor, 
	ch.IDServicio_Det,sd.Descripcion as DescServicioDet,sd.IDServicio,p.NombreCorto as DescProveedor,
	IsNull(p.Web,'') as Web	,''	as 	IDProvCatSel
	
	From COTI_CATEGORIAHOTELES ch Left Join MAUBIGEO u On ch.IDUbigeo=u.IDubigeo
	Left Join MAPROVEEDORES p On ch.IDProveedor=p.IDProveedor
	Left Join MASERVICIOS_DET sd On ch.IDServicio_Det=sd.IDServicio_Det
	--LEft Join COTICAB cc On ch.IDCab=cc.IDCAB And ch.Categoria=cc.Categoria
	Where ch.IDCab=@IDCab
	Order by ch.NroOrd
