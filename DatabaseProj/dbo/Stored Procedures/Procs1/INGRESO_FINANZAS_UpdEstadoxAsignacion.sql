﻿Create Procedure dbo.INGRESO_FINANZAS_UpdEstadoxAsignacion
@NuIngreso int,
@CoEstado char(2),
@UserMod char(4)
As
	Set NoCount On
	Update INGRESO_FINANZAS 
	set CoEstado = @CoEstado,
		UserMod = @UserMod,
		FecMod = GetDate()
	Where NuIngreso = @NuIngreso
