﻿
Create Procedure dbo.DEBIT_MEMO_DET_Del
@IDDebitMemo char(10),
@IDDebitMemoDet tinyint
As
	Set NoCount On
	delete from DEBIT_MEMO_DET 
	Where [IDDebitMemo] = @IDDebitMemo
		   And [IDDebitMemoDet] = @IDDebitMemoDet
