﻿CREATE PROCEDURE [dbo].[MACONTACTOSPROVCLIE_Sel_UsuarioExtranet]
						@CodUsuario varchar(80)
AS
BEGIN
	Set NoCount On
	DECLARE @TblReturn Table(Codigo			char(6),
							 Nombres		varchar(80),
							 Apellidos		varchar(80),
							 Telefono		varchar(80),
							 Celular		varchar(60),
							 Email			varchar(100),
							 TipoUsuario	char(1),
							 RUC			varchar(15),
							 RazonSocial	varchar(100),
							 IdTipo			varchar(3),
							 NombreTipo		varchar(50),
							 CardCodeSAP	varchar(15),
							 [Usuario]		varchar(80),
							 [Password]		varchar(255))
	--Nombres, Apellidos, ApeNom, Telefono, Celular, Email (ContactoProveedor)
	--Nombres, Apellidos, ApeNom, Telefono, Celular, Email (ContactoCliente)
	--TipoUsuario {'C','P'}	--Solo Juridica
	--RUC, RazonSocial
	--Tipo {IdTipo, NombreTipo}
	IF EXISTS(SELECT Usuario FROM MACONTACTOSPROVEEDOR WHERE [Usuario] = @CodUsuario AND FlAccesoWeb = 1)
	BEGIN
		INSERT INTO @TblReturn(Codigo, Nombres, Apellidos, Telefono, Celular, Email, TipoUsuario, RUC, RazonSocial, IdTipo, NombreTipo, CardCodeSAP, [Usuario], [Password])
		SELECT P.IDProveedor, ISNULL(C.Nombres,'') AS Nombres, ISNULL(C.Apellidos,'') AS Apellidos, ISNULL(C.Telefono,'') AS Telefono,
			   ISNULL(C.Celular,'') AS Celular, ISNULL(C.Email,'') AS Email, 'P' AS TipoUsuario,
			   --CASE WHEN P.Persona = 'J' THEN ISNULL(P.NumIdentidad,'') ELSE NULL END AS NumIdentidad, 
			   ISNULL(P.NumIdentidad,'') AS NumIdentidad, 
			   --CASE WHEN P.Persona = 'J' THEN ISNULL(P.RazonSocial,'') ELSE NULL END AS RazonSocial,
			   CASE WHEN P.Persona = 'J' THEN ISNULL(P.RazonSocial,'') ELSE IsNull(p.NombreCorto,'')  END AS RazonSocial,
			   ISNULL(P.IDTipoProv,'') AS IdTipo, ISNULL(T.Descripcion,'') AS NombreTipo, ISNULL(P.CardCodeSAP,'') AS CardCodeSAP, C.[Usuario], C.[Password]
		FROM MACONTACTOSPROVEEDOR AS C INNER JOIN MAPROVEEDORES AS P ON C.IDProveedor = P.IDProveedor INNER JOIN
			 MATIPOPROVEEDOR T ON P.IDTipoProv = T.IDTipoProv
		WHERE C.[Usuario] = @CodUsuario AND C.FlAccesoWeb = 1
	END
	ELSE IF EXISTS(SELECT UsuarioLogeo FROM MACONTACTOSCLIENTE WHERE [UsuarioLogeo] = @CodUsuario AND FlAccesoWeb = 1)
	BEGIN
		INSERT INTO @TblReturn(Codigo, Nombres, Apellidos, Telefono, Celular, Email, TipoUsuario, RUC, RazonSocial, IdTipo, NombreTipo, CardCodeSAP, [Usuario], [Password])
		SELECT C.IDCliente, ISNULL(C.Nombres,'') AS Nombres, ISNULL(C.Apellidos,'') AS Apellidos, ISNULL(C.Telefono,'') AS Telefono,
			   ISNULL(C.Celular,'') AS Celular, ISNULL(C.Email,'') AS Email, 'C' AS TipoUsuario,
			   --CASE WHEN P.Persona = 'J' THEN ISNULL(P.NumIdentidad,'') ELSE NULL END AS NumIdentidad, 
			   ISNULL(P.NumIdentidad,'') AS NumIdentidad, 
			   --CASE WHEN P.Persona = 'J' THEN ISNULL(P.RazonSocial,'') ELSE NULL END AS RazonSocial,
			   CASE WHEN P.Persona = 'J' THEN ISNULL(P.RazonSocial,'') ELSE IsNull(P.RazonComercial,'') END AS RazonSocial,
			   ISNULL(P.Tipo,'') AS IdTipo, dbo.FnClienteTipo(P.Tipo) AS NombreTipo, ISNULL(P.CardCodeSAP,'') AS CardCodeSAP, UsuarioLogeo, PasswordLogeo
		FROM MACONTACTOSCLIENTE AS C INNER JOIN MACLIENTES AS P ON C.IDCliente = P.IDCliente
		WHERE [UsuarioLogeo] = @CodUsuario AND FlAccesoWeb = 1
	END
	ELSE IF EXISTS(SELECT Usuario FROM MAPROVEEDORES WHERE [Usuario] = @CodUsuario AND FlAccesoWeb = 1)
	BEGIN
		INSERT INTO @TblReturn(Codigo, Nombres, Apellidos, Telefono, Celular, Email, TipoUsuario, RUC, RazonSocial, IdTipo, NombreTipo, CardCodeSAP, [Usuario], [Password])
		SELECT P.IDProveedor, ISNULL(P.Nombre,'') AS Nombres, ISNULL(P.ApPaterno,'') + ISNULL(P.ApMaterno,'') AS Apellidos, ISNULL(P.Telefono1,'') AS Telefono,
			   ISNULL(P.Celular,'') AS Celular, ISNULL(P.Email1,'') AS Email, 'P' AS TipoUsuario,
			   --CASE WHEN P.Persona = 'J' THEN ISNULL(P.NumIdentidad,'') ELSE NULL END AS NumIdentidad, 
			   ISNULL(P.NumIdentidad,'') AS NumIdentidad, 
			   --CASE WHEN P.Persona = 'J' THEN ISNULL(P.RazonSocial,'') ELSE NULL END AS RazonSocial,
			   CASE WHEN P.Persona = 'J' THEN ISNULL(P.RazonSocial,'') ELSE IsNull(P.NombreCorto,'') END AS RazonSocial,
			   ISNULL(P.IDTipoProv,'') AS IdTipo, ISNULL(T.Descripcion,'') AS NombreTipo, ISNULL(P.CardCodeSAP,'') AS CardCodeSAP, P.[Usuario], P.[Password]
		FROM MAPROVEEDORES AS P INNER JOIN MATIPOPROVEEDOR T ON P.IDTipoProv = T.IDTipoProv
		WHERE P.[Usuario] = @CodUsuario AND P.FlAccesoWeb = 1
	END

	Set NoCount Off
	SELECT Codigo, Nombres, Apellidos, Nombres + ' ' + Apellidos AS ApeNom, Telefono, Celular, Email, TipoUsuario, RUC, RazonSocial, IdTipo, NombreTipo, CardCodeSAP, [Usuario], [Password]
	FROM @TblReturn
END
