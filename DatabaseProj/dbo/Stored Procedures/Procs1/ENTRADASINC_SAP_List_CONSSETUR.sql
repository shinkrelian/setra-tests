﻿
--JHD-20160129 - Monto=CAST(isnull((select  monto from ENTRADASINC_SAP where IdDet=y.iddet and tipo = 'CONSETTUR' ),Y.Monto) AS INT),
--JHD-20160202 - ,0 as NuDetPSo
--JHD-20160202 - Where (e.FechaPago Between @FePago1 And @FePago2 Or Convert(Char(10),@FePago1,103)='01/01/1900')
--PPMG-20160419-Se optimizo la consulta.
CREATE PROCEDURE [dbo].[ENTRADASINC_SAP_List_CONSSETUR]

@FePago1 smalldatetime='01/01/1900',
@FePago2 smalldatetime='01/01/1900',
@FechaINCusco1 smalldatetime='01/01/1900',
@FechaINCusco2 smalldatetime='01/01/1900'
AS
BEGIN
SET NOCOUNT ON
--/*
SELECT @FePago1 = CASE WHEN @FePago1 = '01/01/1900' THEN NULL ELSE @FePago1 END
	 , @FePago2 = CASE WHEN @FePago2 = '01/01/1900' THEN NULL ELSE @FePago2 END
	 , @FechaINCusco1 = CASE WHEN @FechaINCusco1 = '01/01/1900' THEN NULL ELSE @FechaINCusco1 END
	 , @FechaINCusco2 = CASE WHEN @FechaINCusco2 = '01/01/1900' THEN NULL ELSE @FechaINCusco2 END

select X.IDCAB, X.IDFile, X.Tipo, CAST(isnull(E.Monto,X.Monto) AS INT),
FechaINCusco, FechaPago, isnull(NuCodigo_ER,'') as NuCodigo_ER, X.iddet
 ,0 as NuDetPSo
from
(
select distinct c.IDCAB, c.IDFile, cd.iddet, cd.idservicio, cd.IDServicio_Det, sd.anio,	'CONSETTUR' AS Tipo,
Monto0=cds.CostoReal, Monto=cds.CostoReal*(cd.NroPax+isnull(cd.NroLiberados,0)),
FecMin as FechaINCusco, CONVERT(VARCHAR(20), cd.Dia, 103) as Dia, cd.Dia as Dia2
from cotidet cd inner join coticab c on c.IDCAB=cd.IDCAB inner join
COTIDET_DETSERVICIOS cds on cd.iddet=cds.IDDET
inner join MASERVICIOS_DET sd on sd.IDServicio_Det=cds.IDServicio_Det
inner join MASERVICIOS_DET sd1 on sd.IDServicio_Det_V=sd1.IDServicio_Det and sd1.Tipo <> 'GUIDE'
inner join MASERVICIOS s on sd1.IDServicio=s.IDServicio
inner join MAPROVEEDORES p on s.IDProveedor=p.IDProveedor
INNER JOIN (Select IDCAB, Min(Dia) AS FecMin From COTIDET Where IDubigeo='000066' GROUP BY IDCAB) DetUbi ON cd.IDCAB = DetUbi.IDCAB
where ISNULL(c.IDFile,'') <>'' and p.IDProveedor = '001593' and sd.Estado='A' and c.Estado='A'
AND CONVERT(DATE,FecMin) Between COALESCE(@FechaINCusco1,FecMin) AND COALESCE(@FechaINCusco2,FecMin)
) as X left outer join
(SELECT IDFile, Tipo, IdDet, FechaPago, Monto, NuCodigo_ER FROM ENTRADASINC_SAP
	where Tipo='CONSETTUR' AND CONVERT(DATE,FechaPago) Between COALESCE(@FePago1,FechaPago) AND COALESCE(@FePago2,FechaPago)
) E on X.IDFile = E.IDFile and X.Tipo = E.Tipo and X.IDDET = E.IdDet

--*/
/*
select Y.IDCAB,Y.IDFile,
Y.Tipo,
--Monto=cast(Y.Monto as int),
Monto=CAST(isnull((select  monto from ENTRADASINC_SAP where IdDet=y.iddet and tipo = 'CONSETTUR' ),Y.Monto) AS INT),
 FechaINCusco,
 FechaPago,
 isnull(NuCodigo_ER,'') as NuCodigo_ER,
 Y.iddet
 ,0 as NuDetPSo
from (
select * from
(
select distinct c.IDCAB, c.IDFile,
	cd.iddet,
	cd.idservicio,
	cd.IDServicio_Det,
	sd.anio,
	--dbo.FnServicioMPWP(cd.IDServicio_Det) as MAPIWAYNRAQCCODE,
	'CONSETTUR' AS Tipo,
	Monto0=cds.CostoReal,
	Monto=cds.CostoReal*(cd.NroPax+isnull(cd.NroLiberados,0)),
	(Select Min(Dia) From COTIDET Where IDCAB=cd.IDCAB And IDubigeo='000066') as FechaINCusco,
	 CONVERT(VARCHAR(20), cd.Dia, 103) as Dia,
	  cd.Dia as Dia2
from 
cotidet cd
inner join coticab c on c.IDCAB=cd.IDCAB
inner join
COTIDET_DETSERVICIOS cds on cd.iddet=cds.IDDET
inner join MASERVICIOS_DET sd on sd.IDServicio_Det=cds.IDServicio_Det
inner join MASERVICIOS_DET sd1 on sd.IDServicio_Det_V=sd1.IDServicio_Det and sd1.Tipo <> 'GUIDE'
inner join MASERVICIOS s on sd1.IDServicio=s.IDServicio
inner join MAPROVEEDORES p on s.IDProveedor=p.IDProveedor
where-- cd.iddet=656416
-- exists(select IDDet from COTIDET_PAX cdp where cdp.IDDet=cd.IDDET and (cdp.FlEntMPGenerada=1 or cdp.FlEntWPGenerada=1 or cdp.FlEntRQGenerada=1))
--and  
--((Select Min(Dia) From COTIDET Where IDCAB=cd.IDCAB And IDubigeo='000066') Between @FechaINCusco1 And @FechaINCusco2 Or Convert(Char(10),@FechaINCusco1,103)='01/01/1900')
ISNULL(c.IDFile,'') <>''-- is not null
and p.IDProveedor = '001593'
and sd.Estado='A'
and c.Estado='A'
) as X

where (FechaINCusco Between @FechaINCusco1 And @FechaINCusco2 Or Convert(Char(10),@FechaINCusco1,103)='01/01/1900')
) as Y left outer join ENTRADASINC_SAP e on Y.IDFile=e.IDFile and Y.Tipo=e.Tipo and Y.IDDET=e.IdDet AND E.Tipo='CONSETTUR'
Where (e.FechaPago Between @FePago1 And @FePago2 Or Convert(Char(10),@FePago1,103)='01/01/1900')
--*/
END;

