﻿--HLF-20150819-and not d.IDTipoDoc in ('NCR','NCA')
--HLF-20150828-AND D.CoEstado<>'AN'
--comentando--and left(d.NuDocum,3)<>'005' and not d.IDTipoDoc in ('NCR','NCA') 
--If exists...
--HLF-20150901-manejo de saldos negativos con NCR
--HLF-20150918-Case When d.IDTipoDoc='FAC' then 2 else 1 end as Orden ... Order by X.Orden
--HLF-20150929-and left(d.NuDocum,3)<>'005' 
--HLF-20151016-and not d.CoSAP is null
--HLF-20160408-declare @VtaAdic bit=0	If exists(... And ((Left(d.NuDocum,3) <> '002' and @VtaAdic=0...
CREATE Procedure [dbo].[ASIGNACION_DOCUMENTO_InsMatch]
	@NuIngreso int,
	@NuAsignacion int,
	@IDDebitMemo char(10),
	@UserMod char(4)
As     
	Set NoCount On        

	Declare @NuDocum char(10), @IDTipoDoc  char(3),@SsSaldoDocum numeric(9,2), @CoMonedaDoc char(3), 
		@CoMonedaIng char(3), @SsSaldoAsignac numeric(10,2)=0, @SsTCambio numeric(6,3)--,@IDDebitMemo2 char(10)

	Declare @VtaAdic bit=0
	If exists(
		Select dm.IDDebitMemo From DEBIT_MEMO dm 
		Inner Join VENTA_ADICIONAL va on dm.IDCab=va.IDCab And dm.IDDebitMemo=@IDDebitMemo 
		Inner Join VENTA_ADICIONAL_DET vd On va.NuVtaAdi=vd.NuVtaAdi And vd.IDDebitMemo=@IDDebitMemo 
		)
		Set @VtaAdic = 1

	If Not Exists(Select NuDocum From DOCUMENTO Where CoDebitMemoAnticipo=@IDDebitMemo)
				
		Declare curDocs cursor for
		Select NuDocum, IDTipoDoc, SsSaldoMatchAsignac, CoMonedaDoc, CoMonedaIng,TCambio
		From
			(
			Select d.NuDocum, d.IDTipoDoc, isnull(d.SsSaldoMatchAsignac,0) as SsSaldoMatchAsignac, d.IDMoneda as CoMonedaDoc, ifi.IDMoneda as CoMonedaIng,
			IsNull(ifi.ssTC,(select ctc.SsTipCam from COTICAB_TIPOSCAMBIO ctc where ctc.CoMoneda='SOL' And ctc.IDCab=dm.IDCab)) as TCambio,
			Case When d.IDTipoDoc='FAC' then 2 else 1 end as Orden, d.FeDocum
			--,idm.IDDebitMemo
			FROM INGRESO_DEBIT_MEMO idm Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo And idm.IDDebitMemo=@IDDebitMemo
			Inner Join DOCUMENTO d On dm.IDCab=d.IDCab
			Inner Join INGRESO_FINANZAS ifi On idm.NuIngreso=ifi.NuIngreso
			WHERE idm.NuIngreso=@NuIngreso and idm.NuAsignacion=@NuAsignacion
				--and left(d.NuDocum,3)<>'005' and not d.IDTipoDoc in ('NCR','NCA') 
				and left(d.NuDocum,3)<>'005'-- and not d.IDTipoDoc in ('NCR','NCA') 
				AND D.CoEstado<>'AN'
				and d.SsSaldoMatchAsignac<>0 and idm.SsSaldoMatchDocumento<>0
				and not d.CoSAP is null
				And ((Left(d.NuDocum,3) <> '002' and @VtaAdic=0) or (Left(d.NuDocum,3) = '002' and d.FlVtaAdicional=1 and @VtaAdic=1))
			) as X
		Order by X.Orden, X.FeDocum
		--Order by d.SsTotalDocumUSD Desc	
		
	Else
		Declare curDocs cursor for
		Select d.NuDocum, d.IDTipoDoc, isnull(d.SsSaldoMatchAsignac,0) as SsSaldoMatchAsignac, d.IDMoneda, ifi.IDMoneda,
		IsNull(ifi.ssTC,(select ctc.SsTipCam from COTICAB_TIPOSCAMBIO ctc where ctc.CoMoneda='SOL' And ctc.IDCab=dm.IDCab)) as TCambio
		--,idm.IDDebitMemo
		FROM INGRESO_DEBIT_MEMO idm Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo And idm.IDDebitMemo=@IDDebitMemo
		Inner Join DOCUMENTO d On dm.IDCab=d.IDCab
		Inner Join INGRESO_FINANZAS ifi On idm.NuIngreso=ifi.NuIngreso
		WHERE idm.NuIngreso=@NuIngreso and idm.NuAsignacion=@NuAsignacion
			--and left(d.NuDocum,3)<>'005' and not d.IDTipoDoc in ('NCR','NCA') 
			AND D.CoEstado<>'AN'
			and d.SsSaldoMatchAsignac<>0 and idm.SsSaldoMatchDocumento<>0
			And d.CoDebitMemoAnticipo=@IDDebitMemo
			and not d.CoSAP is null
			And ((Left(d.NuDocum,3) <> '002' and @VtaAdic=0) or (Left(d.NuDocum,3) = '002' and d.FlVtaAdicional=1 and @VtaAdic=1))
		Order by d.FeDocum
		--Order by d.SsTotalDocumUSD Desc	

	Open curDocs
	Fetch Next From curDocs into @NuDocum, @IDTipoDoc, @SsSaldoDocum, @CoMonedaDoc, @CoMonedaIng, @SsTCambio--,@IDDebitMemo
	While @@FETCH_STATUS=0        
		Begin
		InicioBucle:
		Set @SsSaldoAsignac = 0
		Select @SsSaldoAsignac=isnull(SsSaldoMatchDocumento,0) From INGRESO_DEBIT_MEMO
			Where NuAsignacion=@NuAsignacion And IDDebitMemo=@IDDebitMemo
		If @SsSaldoAsignac<0 and not (@IDTipoDoc='NCR' and @SsSaldoAsignac=@SsSaldoDocum*(-1))
			Begin
			goto Sgte
			End			

		If @SsSaldoAsignac<>0
			Begin			
			Declare @SsAsignacNeta numeric(10,2)=0			
			If @CoMonedaDoc=@CoMonedaIng
				Select @SsSaldoDocum=isnull(SsSaldoMatchAsignac,0) From DOCUMENTO Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc
			Else--a moneda de la asignacion
				Select @SsSaldoDocum=dbo.FnCambioMoneda(isnull(SsSaldoMatchAsignac,0),@CoMonedaDoc,@CoMonedaIng,@SsTCambio) 
				From DOCUMENTO Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc

			If @SsSaldoAsignac<=@SsSaldoDocum						
				Set @SsAsignacNeta=@SsSaldoAsignac			
			Else			
				Set @SsAsignacNeta=@SsSaldoDocum
			
			If @SsAsignacNeta<>0
				Begin
				If @CoMonedaDoc=@CoMonedaIng
					Begin
					If @SsAsignacNeta<0 and (@IDTipoDoc='NCR' and @SsSaldoAsignac=@SsSaldoDocum*(-1))
						Update DOCUMENTO Set SsSaldoMatchAsignac=SsSaldoMatchAsignac-(@SsAsignacNeta*(-1)), UserMod=@UserMod, FecMod=GETDATE()
						Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc
					Else
						Update DOCUMENTO Set SsSaldoMatchAsignac=SsSaldoMatchAsignac-@SsAsignacNeta, UserMod=@UserMod, FecMod=GETDATE()
						Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc
					End
				Else
					Begin
					If @SsAsignacNeta<0 and (@IDTipoDoc='NCR' and @SsSaldoAsignac=@SsSaldoDocum*(-1))
						Update DOCUMENTO Set SsSaldoMatchAsignac=SsSaldoMatchAsignac-(dbo.FnCambioMoneda(@SsAsignacNeta,@CoMonedaIng,@CoMonedaDoc,@SsTCambio)*(-1)), 
						UserMod=@UserMod, FecMod=GETDATE()
						Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc
					Else
						Update DOCUMENTO Set SsSaldoMatchAsignac=SsSaldoMatchAsignac-dbo.FnCambioMoneda(@SsAsignacNeta,@CoMonedaIng,@CoMonedaDoc,@SsTCambio), 
						UserMod=@UserMod, FecMod=GETDATE()
						Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc

					End
				Update INGRESO_DEBIT_MEMO Set SsSaldoMatchDocumento=SsSaldoMatchDocumento-@SsAsignacNeta, UserMod=@UserMod, FecMod=GETDATE()
				Where NuAsignacion=@NuAsignacion And IDDebitMemo=@IDDebitMemo
		
				If @CoMonedaDoc=@CoMonedaIng
					Insert Into ASIGNACION_DOCUMENTO(IDTipoDoc,NuDocum,NuAsignacion,IDDebitMemo,SsMonto,UserMod)
					Values(@IDTipoDoc,@NuDocum,@NuAsignacion,@IDDebitMemo,@SsAsignacNeta,@UserMod)
				Else
					Insert Into ASIGNACION_DOCUMENTO(IDTipoDoc,NuDocum,NuAsignacion,IDDebitMemo,SsMonto,UserMod)--en moneda del documento
					Values(@IDTipoDoc,@NuDocum,@NuAsignacion,@IDDebitMemo,dbo.FnCambioMoneda(@SsAsignacNeta,@CoMonedaIng,@CoMonedaDoc,@SsTCambio),@UserMod)

				goto InicioBucle
				End
			End
			Sgte:
		Fetch Next From curDocs into @NuDocum, @IDTipoDoc, @SsSaldoDocum, @CoMonedaDoc, @CoMonedaIng, @SsTCambio--,@IDDebitMemo
		End
	Close curDocs
	Deallocate curDocs

