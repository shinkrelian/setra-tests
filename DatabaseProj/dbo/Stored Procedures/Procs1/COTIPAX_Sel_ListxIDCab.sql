﻿--HLF-20121003-u.Descripcion as DescNacionalidad en vez de u.Gentilicio            
--HLF-20121108-Cambiar a Order by Cast(SUBSTRING(cp.Apellidos,4,3) as tinyint)     
--HLF-20121204-Cambiando Order by            
--JRF-20121210-Cambio Orden de Columnas.     
--HLF-20130124-Cambiando Order by 
--HLF-20130208-Agregando Orden as NroOrd y Order by         
--JRF-20130227-Agregando CAMPO Residente      
--JRF-20130304-Cambiando posición del Nombre x la del apellido.    
--JRF-20130515-Agregando los campos [IDPaisResidencia,FecIngresoPais]  
--JRF-20131003-Agregando el campo [PassValidado]
--MLL-20140527-Agregado el campo TxTituloAcademico ,FlTC 
--JHD-20141009-Agregado el campo NoShow
--HLF-20150318-cp.FlEntMP, cp.FlEntWP, cp.FlAutorizarMPWP
--HLF-20150326-dbo.FnEntradasMPWPCompradasxFilePax...
--JHD-20141009-Agregado el campo FlAdultoINC
--PPMG-20151109-Se agrego la columna NumberAPT
--JRF-20160116-Agregar FlActualizadoxExtrNet
CREATE Procedure [dbo].[COTIPAX_Sel_ListxIDCab]
 @IDCab int=0
AS
BEGIN
 Set NoCount On                 
 
 Select          
 NroOrd, Apellidos, Nombres,Titulo,DescIdentidad,NumIdentidad, DescNacionalidad,          
 FecNacimiento,Peso,Residente ,ObservEspecial,  IDIdentidad,                 
 IDNacionalidad,IDPax,IDPaisResidencia,FecIngresoPais ,PassValidado,
TxTituloAcademico ,
FlTC  ,
FlNoShow, FlEntMP, FlEntWP, FlAutorizarMPWP, EntMPCompradas, EntWPCompradas
,FlAdultoINC, NumberAPT, FlActualizadoxExtrNet
 From
 (   
 Select 
 --ROW_NUMBER() over(order by Nombres   )           
 cp.Orden As NroOrd,          
 cp.IDIdentidad,di.Descripcion as DescIdentidad, ltrim(rtrim(isnull(cp.NumIdentidad,''))) NumIdentidad,                 
 cp.Titulo, cp.Nombres, cp.Apellidos, cp.FecNacimiento,            
 cp.IDNacionalidad,u.Descripcion as DescNacionalidad ,cp.IDPax ,Residente           
 ,isnull(cp.Peso,0) As Peso,Isnull(cp.ObservEspecial,'') As ObservEspecial,         
 Case When ISNUMERIC(SUBSTRING(Apellidos,4,3))=1 Then              
  SUBSTRING(Apellidos,4,3)            
 Else            
  Nombres        
 End as NroNombrePax,cp.IDPaisResidencia as IDPaisResidencia   
 ,cp.FecIngresoPais As FecIngresoPais , cp.PassValidado ,
 ISNULL(cp.TxTituloAcademico , '---') as TxTituloAcademico ,
 ISNULL(cp.FlTC ,0) as  FlTC 
 ,cp.FlNoShow,
 cp.FlEntMP, cp.FlEntWP, cp.FlAutorizarMPWP, 
 dbo.FnEntradasMPWPCompradasxFilePax(@IDCab, cp.IDPax,'MAPI') as EntMPCompradas,
 dbo.FnEntradasMPWPCompradasxFilePax(@IDCab, cp.IDPax,'WAYNA') as EntWPCompradas
 --CAST (0 AS BIT) as EntMPCompradas,
 --CAST (0 AS BIT) as EntWPCompradas,
 ,CP.FlAdultoINC
 ,cp.NumberAPT
 ,cp.FlActualizadoxExtrNet
 From COTIPAX cp Left Join MATIPOIDENT di On cp.IDIdentidad=di.IDIdentidad                
 Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo                
 Where cp.IDCab=@IDCab                

 ) as X            
 --Order by Cast((Case When ISNUMERIC(NroNombrePax)=1 Then NroNombrePax Else 0 End) As smallint)        
  Order by NroOrd    
END
