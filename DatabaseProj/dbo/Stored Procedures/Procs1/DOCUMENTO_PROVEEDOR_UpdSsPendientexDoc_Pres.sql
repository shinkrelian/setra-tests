﻿--JHD-20150703-CoEstado='PD'
--JHD-20150703-CoEstadoFormaEgreso=Case When (Isnull(SsSaldo,0)+@TotalOrig) > 0 and CoEstadoFormaEgreso='AC' then 'PD' Else CoEstadoFormaEgreso End  
--JHD-20150703-CoEstadoFormaEgreso=Case When (Isnull(SsSaldo_USD,0)+@TotalOrig) > 0 and CoEstadoFormaEgreso='AC' then 'PD' Else CoEstadoFormaEgreso End  
CREATE Procedure dbo.DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc_Pres  
@NuDocumProv int   
as  
Set NoCount On  
Declare @NuVoucher int=0,@NuOrdenServicio int=0 ,@Moneda char(3)='',@CoOrdPag int=0,@NuFondoFijo int=0,@NuPreSob int=0
  
Select @NuPreSob=Isnull(NuPreSob,0),

@Moneda=Isnull(CoMoneda,'')
from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv  

--Declare @TotalOrig numeric(9,2)=(select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) 
--								 from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv and FlActivo=0)    

Declare @TotalOrig numeric(9,2)=(select Sum(SsTotalOriginal) 
								 from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv and FlActivo=0)    

--select @NuVoucher  
if @NuPreSob > 0  
Begin  
	if @Moneda='SOL'
		BEGIN
		 Update PRESUPUESTO_SOBRE   
		 set SsSaldo= Isnull(SsSaldo,0)+@TotalOrig,
		 CoEstadoFormaEgreso=Case When (Isnull(SsSaldo,0)+@TotalOrig) > 0 and CoEstadoFormaEgreso='AC' then 'PD' Else CoEstadoFormaEgreso End  
		 where NuPreSob=@NuPreSob  
		END
	ELSE
		BEGIN
		 Update PRESUPUESTO_SOBRE   
		 set SsSaldo_USD= Isnull(SsSaldo_USD,0)+@TotalOrig,
		 CoEstadoFormaEgreso=Case When (Isnull(SsSaldo_USD,0)+@TotalOrig) > 0 and CoEstadoFormaEgreso='AC' then 'PD' Else CoEstadoFormaEgreso End  
		 where NuPreSob=@NuPreSob  
		END

End  

