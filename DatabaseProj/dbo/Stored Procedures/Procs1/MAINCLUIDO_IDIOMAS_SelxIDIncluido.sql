﻿CREATE Procedure [dbo].[MAINCLUIDO_IDIOMAS_SelxIDIncluido] 	
	@IDIncluido	int
As

	Set NoCount On
	
	Select IDIdioma,Descripcion
	From MAINCLUIDO_IDIOMAS Where IDIncluido=@IDIncluido
