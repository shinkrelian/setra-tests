﻿--HLF-20160126-and cs.QtUtilizada>0
CREATE Procedure dbo.COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS_SelQtUtilizada
	@IDCab int,
	@CoServicio char(8)
As
	Set Nocount on

	Select cs.NuTckEntIng,cs.QtUtilizada, isnull(se.IDTipoDocSus,'') as IDTipoDocSus,
		isnull(se.NroDocSus,'') as NroDocSus, isnull(se.NuOperBan,'') as NuOperBan
	
	From COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS cs inner join STOCK_TICKET_ENTRADAS_INGRESOS se on cs.IDServicio=se.IDServicio
		And cs.NuTckEntIng=se.NuTckEntIng
	where cs.IDServicio=@CoServicio and cs.IDCab=@IDCab and cs.QtUtilizada>0
	Order by 1
