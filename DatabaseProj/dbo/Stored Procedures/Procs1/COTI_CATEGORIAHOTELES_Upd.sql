﻿CREATE Procedure [dbo].[COTI_CATEGORIAHOTELES_Upd]
	@IDCab	int,
	@IDUbigeo char(6),
	@Categoria varchar(20),
	@IDProveedor char(6),
	@IDProveedorUpd char(6),
	@IDServicio_Det	int,
	@UserMod char(4)
As
	Set NoCount On
	
	UPDATE COTI_CATEGORIAHOTELES
	Set IDProveedor=@IDProveedor,IDServicio_Det=@IDServicio_Det,
	UserMod=@UserMod,FecMod=getdate()
	WHERE IDCab=@IDCab And
	IDUbigeo=@IDUbigeo And 
	Categoria=@Categoria And
	IDProveedor=@IDProveedorUpd
