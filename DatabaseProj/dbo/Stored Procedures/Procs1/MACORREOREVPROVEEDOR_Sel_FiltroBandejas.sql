﻿--JRF-201303-Agregar los correos de los contactos del proveedor para el filtro.
CREATE Procedure [dbo].[MACORREOREVPROVEEDOR_Sel_FiltroBandejas]      
 @IDProveedor char(6)    
As    
 Set NoCount On    
     
 SELECT IDProveedor,REPLACE(Replace(Correo,'/',';'),',',';') As Correo,Filtro From    
 (     
 Select IDProveedor, Email3 as Correo , 'TO' As Filtro  
 From MAPROVEEDORES    
 Where IDProveedor = @IDProveedor And Activo='A'    
 Union    
 Select IDProveedor, Email4 as Correo, 'TO' As Filtro  
 From MAPROVEEDORES    
 Where IDProveedor = @IDProveedor And Activo='A'     
 Union    
 Select IDProveedor,Correo, 'TO' As Filtro  
 From MACORREOREVPROVEEDOR     
 Where IDProveedor = @IDProveedor    
 union
 select IDProveedor,Email as Correo,'TO' As Filtro 
 from MACONTACTOSPROVEEDOR 
 where IDProveedor = @IDProveedor
 
 Union   
 select IDProveedor,EmailRecepcion1 as Correo, 'CCO' As Filtro  
 from MAPROVEEDORES  
 Where IDProveedor = @IDProveedor And Activo = 'A'    
 Union   
 select IDProveedor,EmailRecepcion2 as Correo, 'CCO' As Filtro  
 from MAPROVEEDORES  
 Where IDProveedor = @IDProveedor And Activo = 'A'    
 ) as X    
 Where Not Correo Is Null   
 Order by 3 desc  
