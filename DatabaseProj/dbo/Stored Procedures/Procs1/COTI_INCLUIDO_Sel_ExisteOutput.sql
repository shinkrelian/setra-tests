﻿Create Procedure [dbo].[COTI_INCLUIDO_Sel_ExisteOutput]
	@IDCAB int,
	@IDIncluido	int, 
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDCAB From COTI_INCLUIDO
		WHERE IDCAB=@IDCAB	And IDIncluido=@IDIncluido)
		Set @pExiste=1
	Else
		Set @pExiste=0
