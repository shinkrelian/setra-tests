﻿--HLF-20130917-Agregando parametro @Estado   
--HLF-20141210-FechaReserva=Case When FechaReserva is null Then GETDATE() Else FechaReserva End
CREATE Procedure [dbo].[COTICAB_Upd_IDFile]    
 @IDCab int,    
 @IDFile char(8),    
 --@FecInicioRes smalldatetime,    
 --@FechaOutRes smalldatetime,    
 @IDUsuarioRes char(4),    
 @Estado char(1),  
 @UserMod char(4)    
As    
 Set NoCount On    
     
 Update COTICAB Set IDFile=Case When ltrim(rtrim(@IDFile))='' Then IDFile Else @IDFile End,    
 --FecInicioRes=@FecInicioRes,    
 --FechaOutRes=@FechaOutRes,    
 IDUsuarioRes=@IDUsuarioRes,    
 Estado=@Estado,  
 UserMod=@UserMod, FecMod=GETDATE(),    
 --FechaReserva=GETDATE(),
 FechaReserva=Case When FechaReserva is null Then GETDATE() Else FechaReserva End
 Where IDCAB=@IDCab    
   