﻿CREATE Procedure [dbo].[COTI_CATEGORIAHOTELES_Ins]
	@IDCab	int,
	@IDUbigeo char(6),
	@Categoria varchar(20),
	@IDProveedor char(6),
	@IDServicio_Det	int,
	@NroOrd	tinyint,
	@UserMod char(4)
As
	Set NoCount On
	If @NroOrd=0 
		Set @NroOrd=(Select IsNull(MAX(NroOrd),0)+1 From COTI_CATEGORIAHOTELES Where IDCab=@IDCab)
	
	INSERT INTO COTI_CATEGORIAHOTELES(IDCab,IDUbigeo,Categoria,
	IDProveedor,IDServicio_Det,NroOrd,UserMod)
	Values(@IDCab,@IDUbigeo,@Categoria,
	@IDProveedor,@IDServicio_Det,@NroOrd,@UserMod)
