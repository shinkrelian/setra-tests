﻿Create Procedure dbo.COTICAB_COSTOSTC_Del
@NuCosto tinyint,
@IDCab int
As
Set NoCount On
 DELETE FROM COTICAB_COSTOSTC
 WHERE IDCab =@IDCab and NuCosto=@NuCosto
