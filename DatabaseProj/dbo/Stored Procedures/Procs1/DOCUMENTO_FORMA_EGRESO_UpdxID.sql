﻿
CREATE Procedure [dbo].[DOCUMENTO_FORMA_EGRESO_UpdxID]  
	@IDDocFormaEgreso	int,
	@TipoFormaEgreso	char(3),
	@CoMoneda			char(3),
	@CoEstado			char(2),
	@SsSaldo			numeric(14,4),
	@SsDifAceptada		numeric(14,4),
	@UserMod			char(4),
	@TxObsDocumento		varchar(max)
As  
BEGIN  
	UPDATE DOCUMENTO_FORMA_EGRESO
	SET CoEstado = @CoEstado, SsSaldo= CASE WHEN @SsSaldo = 0 THEN NULL ELSE @SsSaldo END,
		SsDifAceptada = CASE WHEN @SsDifAceptada = 0 THEN NULL ELSE @SsDifAceptada End,
		TxObsDocumento = LTRIM(RTRIM(@TxObsDocumento)),
		UserMod = @UserMod,
		FecMod = GETDATE()
	WHERE IDDocFormaEgreso = @IDDocFormaEgreso AND TipoFormaEgreso = @TipoFormaEgreso AND CoMoneda = @CoMoneda

END;  
