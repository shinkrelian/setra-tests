﻿
--create
create PROCEDURE COTICAB_ARCHIVOS_Del
	@IDArchivo int,
	@IDCAB int=0
AS
BEGIN
	Delete dbo.COTICAB_ARCHIVOS
	Where 
		IDArchivo = @IDArchivo
		AND IDCAB=@IDCAB
END;
