﻿Create Procedure [dbo].[COTIDET_Upd_TransferDes]
	@IDDet	int,
	@IDDetTransferDes	int
As
	Set Nocount On  
	
	Update COTIDET Set IDDetTransferDes=@IDDetTransferDes
	Where IDDet=@IDDet
