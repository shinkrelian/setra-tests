﻿CREATE Procedure dbo.MAFORMASPAGO_Upd    
 @IDFor char(3),    
 @Descripcion varchar(50),    
 @SiglaEsUtil char(3) ,  
 @UserMod char(4)    
    
As    
    
 Set NoCount On    
    
 Update MAFORMASPAGO Set     
 Descripcion=@Descripcion,    
 UserMod=@UserMod ,    
 Tipo = @SiglaEsUtil,  
 FecMod=GETDATE()     
 Where IDFor = @IDFor     
