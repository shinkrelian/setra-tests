﻿Create Procedure [dbo].[MAIDIOMAS_Del]
	@IDIdioma varchar(12)	
As
	Set NoCount On
	
	Delete From MAIDIOMAS
	Where IDIdioma=@IDIdioma
