﻿Create Procedure dbo.ACOMODO_VEHICULO_EXT_DelxIDDet
	@IDDet int
As
	Set Nocount on

	Delete From ACOMODO_VEHICULO_EXT
	Where IDDet=@IDDet
