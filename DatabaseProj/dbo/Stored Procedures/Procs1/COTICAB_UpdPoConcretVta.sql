﻿--HLF-20141204-And cc.FlPoConcretManualUpd=0    
--HLF-20150217-Declare @bPaxCompletos bit=1
	--If Exists( Select IDPax From COTIPAX Where IDCab=@IDCab And ...
	--Case When @bPaxCompletos=1 Then 100
CREATE Procedure dbo.COTICAB_UpdPoConcretVta    
	@IDCab int,    
	@UserMod char(4)    
As    
	Set Nocount On    

	Declare @bPaxCompletos bit=1

	If Exists( Select IDPax From COTIPAX Where IDCab=@IDCab And 
	(ISNULL(NumIdentidad,'')='' Or ISNULL(FecNacimiento,'01/01/1900')='01/01/1900' Or 
		CHARINDEX('Pax',Nombres)>0 Or CHARINDEX('Pax',Apellidos)>0))
		Set @bPaxCompletos = 0
		
	 Update COTICAB Set PoConcretVta=    
	 Case When NuSerieCliente Is Null Then
		Case When @bPaxCompletos=1 Then 100
		Else
			
		  Case When NroPax+ISNULL(nroliberados,0)<8 Then 
			95  
		  Else 
			60  
		  End    
		End
	 Else    
	  sc.PoConcretizacion    
	 End,       
	 FlPoConcretAutoUpd=0,    
	 FecMod=GETDATE(), UserMod=@UserMod          
	 From COTICAB cc Left Join MASERIESCLIENTE sc On sc.NuSerie=cc.NuSerieCliente    
	 Where cc.IDCAB=@IDCab And isnull(cc.FlPoConcretAutoUpd,0)=0    
	  And Isnull(cc.FlPoConcretManualUpd,0)=0    
	  
