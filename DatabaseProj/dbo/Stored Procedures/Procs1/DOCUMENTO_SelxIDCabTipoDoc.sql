﻿--HLF-20140416-Cambiando descripciones a DescEstado    
--HLF-20140430-Agregando columna FlPorAjustePrecio  
--HLF-20141217-isnull(IDTipoDocVinc,'') as IDTipoDocVinc,isnull(NuDocumVinc,'') as NuDocumVinc, isnull(FeEmisionVinc,'01/01/1900') as FeEmisionVinc,
--JHD-20150522-NuItem_DM=isnull(NuItem_DM,0)
--HLF-20160310-And FlVtaAdicional=0		 And left(nudocum,3) in ('001','005')
--HLF-20160323-agregando '006' al left(nudocum,3) in (...
CREATE Procedure [dbo].[DOCUMENTO_SelxIDCabTipoDoc]    
 @IDCab int,    
 @IDTipoDoc char(3)    
As     
 Set NoCount On    
     
 Select NuDocum,SsTotalDocumUSD,    
 Case CoEstado     
  When 'GD' Then 'GENERADO'     
  When 'IM' Then 'IMPRESO'       
 End As DescEstado,  
 FlPorAjustePrecio,
 isnull(IDTipoDocVinc,'') as IDTipoDocVinc,isnull(NuDocumVinc,'') as NuDocumVinc, isnull(FeEmisionVinc,'01/01/1900') as FeEmisionVinc,
 NuItem_DM=isnull(NuItem_DM,0)
 From DOCUMENTO     
 Where IDCab=@IDCab And IDTipoDoc=@IDTipoDoc    
 And CoEstado<>'AN'    
 And FlVtaAdicional=0
 And left(nudocum,3) in ('001','005','006')