﻿CREATE Procedure ANTICIPOS_Ins  
@Fecha smalldatetime,  
@IdTipoDoc char(3),  
@IdSerie char(3),  
@Numero char(8),  
@Correlativo char(2),  
@IGV numeric(6,2),  
@IDMoneda char(3),  
@TC numeric(8,2),  
@Anulado bit,  
@IDCab int,  
@IDFile char(8),  
@IDCliente char(6),  
@NroPax tinyint,  
@Referencia varchar(100),  
@IDUsuarioResponsable char(4),  
@IDbanco char(3),  
@FechaIN smalldatetime,  
@FechaOut smalldatetime,  
@Exportacion numeric(8,2),  
@Gravado numeric(8,2),  
@Nogravado numeric(8,2),  
@UserMod char(4)  
As  
 Set NoCount On  
 INSERT INTO [dbo].[ANTICIPOS]  
      ([Fecha]  
      ,[IdTipoDoc]  
      ,[IdSerie]  
      ,[Numero]  
      ,[Correlativo]  
      ,[IGV]  
      ,[IDMoneda]  
      ,[TC]  
      ,[Anulado]  
      ,[IDCab]  
      ,[IDFile]  
      ,[IDCliente]  
      ,[NroPax]  
      ,[Referencia]  
      ,[IDUsuarioResponsable]  
      ,[IDbanco]  
      ,[FechaIN]  
      ,[FechaOut]  
      ,[Exportacion]  
      ,[Gravado]  
      ,[Nogravado]  
      ,[UserMod])  
   VALUES  
      (@Fecha,  
      @IdTipoDoc,  
      Case When ltrim(rtrim(@IdSerie))='' Then Null Else @IdSerie End,  
      Case When ltrim(rtrim(@Numero))='' Then Null Else @Numero End,  
      Case When ltrim(rtrim(@Correlativo))='' Then Null Else @Correlativo End,  
      Case When @IGV=0 Then Null Else @IGV End,  
      @IDMoneda,  
      Case When @TC=0 Then Null Else @TC End,  
      @Anulado,  
      @IDCab,  
      @IDFile,  
      @IDCliente,  
      @NroPax,  
      Case When ltrim(rtrim(@Referencia))='' Then Null Else @Referencia End,  
      @IDUsuarioResponsable,  
      @IDbanco,   
      @FechaIN,   
      @FechaOut,  
      Case When @Exportacion = 0 Then Null Else @Exportacion End,  
      Case When @Gravado = 0 Then Null Else @Gravado End,  
      Case When @Nogravado = 0 Then Null Else @Nogravado End,  
      @UserMod)  
