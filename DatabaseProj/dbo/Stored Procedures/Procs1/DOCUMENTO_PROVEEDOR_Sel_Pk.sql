﻿--JHD-20141028-Se agregaron campos de flactivo,SsNeto,SsOtrCargos y SsDetraccion        
--JHD-20141029-Se agregaron campos de SSTipoCambio,SSTotal        
--JHD-20141029-Se cambio el nombre del campo de SSMonto por SSTotalOriginal        
--JHD-20141029-Se cambio el nombre del campo de SSTotal_Dolares por SSTotal        
--JHD-20141029-Se agrego campo de NuOrden_Servicio        
--JHD-20141030-Se agregaron campos de CoTipoOC,CoCtaContab        
--HLF-20141030-se comenta INNER JOIN  VOUCHER_OPERACIONES ON ...      
--JHD-20141103-Se agrego campo de CoMoneda_Pago      
--HLF-20141106-dbo.FnFormatearDocumProvee(NuDocum) as NuDocumFormat    
--JRF-20141106-Se agrego el campo de CoMoneda_FEgreso.  
--HLF-20150204-          isnull(oc.CoProveedor,'') as CoProveedorOC,
--          isnull(poc.NombreCorto,'') as NoProveedorOC
--JHD-20150518-,NuSerie=isnull(dp.NuSerie,'')
--JHD-20150520- SSDetraccion=isnull(dp.SSDetraccion,0),   
--JHD-20150601-Se agrego campo de CoCeCos 
--JHD-20150722-Se agregaron campos nuevos: FeVencimiento, CoTipoDocSAP, CoFormaPago, CoTipoDoc_Ref, NuSerie_Ref, NuDocum_Ref, FeEmision_Ref, SSPercepcion, TxConcepto
--JHD-20150723-Se agrego el campo: CoCeCon
--JHD-20150810-isnull(dp.CardCodeSAP,'') as NuDocInterno, 
--JHD-20150817-Nuevo Campo CoGasto
--JHD-20150824-Nuevos Campos IdCab,IDFile
--JHD-20150824-isnull(poc.IdProveedor,'') as CoProveedorOC,
--JHD-20150824-Left Join MAPROVEEDORES poc On poc.IDProveedor=dp.CoProveedor
--JHD-20150826-NuDocum= case when FlCorrelativo=1 then '' else dp.NuDocum end,
--JHD-20150826-NuDocumFormat= case when FlCorrelativo=1 then '' else dbo.FnFormatearDocumProvee(dp.NuDocum) end,
--JHD-20150911-NuDocumFormat= case when FlCorrelativo=1 then '' else dp.NuDocum /*dbo.FnFormatearDocumProvee(dp.NuDocum)*/ end,
--JHD-20150930-,FlMultiple
--JHD-20150930-,NuDocum_Multiple=isnull(dp.NuDocum_Multiple,0)
CREATE Procedure [dbo].[DOCUMENTO_PROVEEDOR_Sel_Pk]        
 @NuDocumProv int        
AS        
      
SEt Nocount On      
      
SELECT dp.NuDocumProv,        
    dp.NuVoucher,        
    --dp.NuDocum,        
	 NuDocum= case when FlCorrelativo=1 then '' else dp.NuDocum end,
   --dbo.FnFormatearDocumProvee(dp.NuDocum) as NuDocumFormat,    
   -- NuDocumFormat= case when FlCorrelativo=1 then '' else dbo.FnFormatearDocumProvee(dp.NuDocum) end,
   NuDocumFormat= case when FlCorrelativo=1 then '' else dp.NuDocum /*dbo.FnFormatearDocumProvee(dp.NuDocum)*/ end,
    --dp.NuDocInterno,        
	isnull(dp.CardCodeSAP,'') as NuDocInterno,        
    dp.CoTipoDoc,         
          td.Descripcion,        
          dp.FeEmision,        
          dp.FeRecepcion,        
          dp.CoTipoDetraccion,         
          dp.CoMoneda,        
          mo.Descripcion AS Moneda,        
          dp.SsIGV,        
          SsNeto,        
          SsOtrCargos,        
          --oc.SSDetraccion, 
		  SSDetraccion=isnull(dp.SSDetraccion,0),               
          dp.SSTotalOriginal,         
          dp.FlActivo,        
          dp.SSTipoCambio,        
          dp.SSTotal,        
          dp.NuOrden_Servicio,        
          CoTipoOC,        
          CoCtaContab      
          --os.CoOrden_Servicio      
          ,dp.CoMoneda_Pago,  
          dp.CoMoneda_FEgreso,  
          dp.SsTipoCambio_FEgreso,  
          dp.SsTotal_FEgreso,
          --isnull(oc.CoProveedor,'') as CoProveedorOC,
		  isnull(poc.IdProveedor,'') as CoProveedorOC,
          isnull(poc.NombreCorto,'') as NoProveedorOC

		  ,NuSerie=isnull(dp.NuSerie,'')
		   ,cocecos=isnull(dp.cocecos,'')
		   --
		    ,FeVencimiento
		   ,CoTipoDocSAP=isnull(CoTipoDocSAP,0)
		   ,CoFormaPago=isnull(CoFormaPago,'')
		   ,CoTipoDoc_Ref=isnull(CoTipoDoc_Ref,'')
		   ,NuSerie_Ref=isnull(NuSerie_Ref,'')
		   ,NuDocum_Ref=isnull(NuDocum_Ref,'')
		   ,FeEmision_Ref
		   ,SSPercepcion=isnull(dp.SSPercepcion,0),
		   TxConcepto=isnull(dp.TxConcepto,'')
		   ,CoCeCon=isnull(dp.CoCeCon,'')
		   ,CoGasto=isnull(dp.CoGasto,'')
		   ,idcab=isnull(dp.IDCab,0)
		   ,IDFile=isnull((select idfile from coticab where idcab=dp.IDCab),'')

		     ,FlMultiple
			 ,NuDocum_Multiple=isnull(dp.NuDocum_Multiple,0)
FROM DOCUMENTO_PROVEEDOR dp      
--INNER JOIN  VOUCHER_OPERACIONES ON dp.NuVoucher = VOUCHER_OPERACIONES.IDVoucher       
INNER JOIN        
 MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc INNER JOIN        
                      MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda        
          --Left Join ORDEN_SERVICIO os On isnull(dp.NuOrden_Servicio,0)=os.NuOrden_Servicio      
          Left Join ORDENCOMPRA oc On dp.NuOrdComInt=oc.NuOrdComInt
          --Left Join MAPROVEEDORES poc On poc.IDProveedor=oc.CoProveedor
		  Left Join MAPROVEEDORES poc On poc.IDProveedor=dp.CoProveedor
WHERE     (dp.NuDocumProv = @NuDocumProv);
