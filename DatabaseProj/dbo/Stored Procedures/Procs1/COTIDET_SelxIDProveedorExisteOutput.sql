﻿

Create Procedure dbo.COTIDET_SelxIDProveedorExisteOutput
	@IDCab	int,
	@IDProveedor char(6),
	@pExiste bit Output
As
	Set Nocount On
	
	Set @pExiste = 0
	
	If Exists(Select IDDet From COTIDET Where IDCAB=@IDCab 
		And IDProveedor=@IDProveedor)
		Set @pExiste = 1
		
