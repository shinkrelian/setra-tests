﻿CREATE Procedure [dbo].[MADETALLE_Ins]	
	@Descripcion text,
	@IDIdioma char(12),
	@IDUsuario char(4),	
	@UserMod char(4),
	@pIDDetalle char(3) output
As

	Set NoCount On
	Declare @IDDetalle char(3)	
	Exec Correlativo_SelOutput 'MADETALLE',1,3,@IDDetalle output
	
	INSERT INTO MADETALLE
           ([IDdetalle]
           ,[Descripcion]
           ,[IDIdioma]
           ,[IDUsuario]
           ,[Descri]
           ,[UserMod])
     VALUES
           (@IDdetalle,
            @Descripcion,
            @IDIdioma, 
            @IDUsuario,
            Cast(@Descripcion as Varchar(10)), 
            @UserMod)
            
	Set @pIDDetalle=@IDDetalle
