﻿
Create Procedure dbo.MACORREOSUSUARIO_SelExisteOutput    
	@IDUsuario char(4), 
	@Correo varchar(150),  
	@pbExists bit Output    
As     
	 Set NoCount On    
	 Set @pbExists = 0    
  
	 If Exists(Select Correo From MACORREOSUSUARIO 
		Where IDUsuario=@IDUsuario And Correo=@Correo)  
		
		Set @pbExists = 1    
      
