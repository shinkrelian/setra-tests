﻿--JRF-20150828-Campos nuevos [SsGEBPrimerDia,SsGEBUltimoDia]
CREATE Procedure dbo.COTICAB_TOURCONDUCTOR_Ins
@IDCab int ,
@FeDesde smalldatetime ,
@FeHasta smalldatetime ,
@FlIncluiNochesAntDesp bit,
@SsMontoRealHotel numeric(8,2),
@SsMontoTotalHotel numeric(8,2) ,
@FlIncluirVuelo bit,
@SsVuelo numeric(8,2) ,
@CoTipoHon char(3) ,
@SsHonorario numeric(8,2) ,
@PoMargen numeric(5,2) ,
@SsViaticosSOL numeric(8,2) ,
@SsViaticosDOL numeric(8,2) ,
@SsTicketAereos numeric(8,2) ,
@SsIGVHoteles numeric(8,2) ,
@SsTarjetaTelef numeric(8,2) ,
@SsTaxi numeric(8,2) ,
@SsOtros numeric(8,2),
@SsGEBPrimerDia numeric(8,2)=0,
@SsGEBUltimoDia numeric(8,2)=0,
@Total numeric(8,2) ,
@UserMod char(4)
As
Set NoCount On
INSERT INTO[dbo].[COTICAB_TOURCONDUCTOR]
           ([IDCab]
           ,[FeDesde]
           ,[FeHasta]
           ,[FlIncluiNochesAntDesp]
           ,[SsMontoRealHotel]
           ,[SsMontoTotalHotel]
           ,[FlIncluirVuelo]
           ,[SsVuelo]
           ,[CoTipoHon]
           ,[SsHonorario]
           ,[PoMargen]
           ,[SsViaticosSOL]
           ,[SsViaticosDOL]
           ,[SsTicketAereos]
           ,[SsIGVHoteles]
           ,[SsTarjetaTelef]
           ,[SsTaxi]
           ,[SsOtros]
		   ,[SsGEBPrimerDia]
		   ,[SsGEBUltimoDia]
           ,[Total]
           ,[UserMod])
     VALUES
           (@IDCab
           ,@FeDesde
           ,@FeHasta
           ,@FlIncluiNochesAntDesp
           ,@SsMontoRealHotel
           ,@SsMontoTotalHotel
           ,@FlIncluirVuelo
           ,@SsVuelo
           ,@CoTipoHon
           ,@SsHonorario
           ,@PoMargen
           ,@SsViaticosSOL
           ,@SsViaticosDOL
           ,@SsTicketAereos
           ,@SsIGVHoteles
           ,@SsTarjetaTelef
           ,@SsTaxi
           ,@SsOtros
		   ,@SsGEBPrimerDia
		   ,@SsGEBUltimoDia
           ,@Total
           ,@UserMod)
