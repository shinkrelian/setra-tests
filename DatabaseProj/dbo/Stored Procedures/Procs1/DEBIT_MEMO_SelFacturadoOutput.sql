﻿
Create Procedure dbo.DEBIT_MEMO_SelFacturadoOutput
	@NuDebitMemo char(10),
	@pFacturado bit output
As
	Set Nocount On

	Set @pFacturado=0
	Select @pFacturado=FlFacturado From DEBIT_MEMO Where IDDebitMemo=@NuDebitMemo

