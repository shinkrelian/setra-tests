﻿CREATE Procedure [dbo].[COTI_CATEGORIAHOTELES_Ins_xIDCab]
	@IDCab	int,
	@UserMod char(4)	
As
	Set NoCount On
	
	INSERT INTO COTI_CATEGORIAHOTELES(IDCab,IDUbigeo,Categoria,
	IDProveedor,UserMod)
	Select @IDCab,cd.IDubigeo,'CATEGORIA INICIAL',cd.IDProveedor,@UserMod From COTIDET cd Inner Join MAPROVEEDORES p On 
	cd.IDProveedor=p.IDProveedor And p.IDTipoProv='001'
	Where cd.IDCAB=@IDCab
	And Not cd.IDUbigeo+' '+'CATEGORIA INICIAL'+' '+cd.IDProveedor In (Select IDUbigeo+' '+Categoria+' '+
	IDProveedor From COTI_CATEGORIAHOTELES Where IDCab=@IDCab)
	Group by cd.IDubigeo, cd.IDProveedor
