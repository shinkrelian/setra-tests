﻿
    
--HLF-20140623-Agreagando columna QtVecesBusqueda  
--HLF-20140630-Agregando columna TxSeparador  
--HLF-20140709-Agregando columna FlMultiColumnas
CREATE PROCEDURE INICIOFINCAMPOPROCESO_Sel_List      
 @CoProceso CHAR(3)      
AS      
      
 SET NOCOUNT ON      
       
 SELECT cap.NoCampo, ifp.QtInicio, ifp.QtFin,     
 ifp.TxClave, ifp.QtSegmentClave, ifp.QtPosInicio, ifp.QtTamanio,     
 cap.NoTabla, cap.FlMultiLineas, ifp.QtVecesBusqueda, ifp.TxSeparador,
 cap.FlMultiColumnas
 FROM INICIOFINCAMPOPROCESO ifp      
   LEFT JOIN CAMPOPROCESO cap ON cap.CoProceso = @CoProceso AND  ifp.NuCampo = cap.NuCampo      
 ORDER BY ifp.NuCampo      
   
