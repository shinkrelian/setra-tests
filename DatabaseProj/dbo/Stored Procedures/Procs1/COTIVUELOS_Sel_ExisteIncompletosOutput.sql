﻿Create Procedure [dbo].[COTIVUELOS_Sel_ExisteIncompletosOutput]
	@IDCab	int,
	@pExiste	bit Output
As
	Set Nocount On
	
	Set @pExiste=0
	
	If Exists(Select ID From COTIVUELOS Where IDCab=@IDCab And
		CodReserva Is Null )
		
		Set @pExiste=1
