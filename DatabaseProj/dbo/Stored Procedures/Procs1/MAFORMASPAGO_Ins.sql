﻿CREATE Procedure dbo.MAFORMASPAGO_Ins     
 @Descripcion varchar(50),    
 @SiglaEsUtil char(3) ,  
 @UserMod char(4),    
 @pIDFor char(3) Output    
As    
    
 Set NoCount On    
     
 Declare @IDFor char(3)     
 Exec Correlativo_SelOutput 'MAFORMASPAGO',1,3,@IDFor output     
    
 INSERT INTO MAFORMASPAGO    
           ([IDFor]    
           ,[Descripcion]    
           ,Tipo  
           ,[UserMod]  
           ,[FecMod]   
           )    
     VALUES    
           (@IDFor,    
            @Descripcion ,    
            @SiglaEsUtil,  
            @UserMod,  
            GETDATE() )    
                
 Set @pIDFor = @IDFor     
