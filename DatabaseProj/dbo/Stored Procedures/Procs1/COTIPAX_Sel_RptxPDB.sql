﻿

-----------------------------------------------------------------------------
--COTIPAX_Sel_RptxPDB '01/01/2014','10/10/2014'
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JRF-Considerar los campos de Asignación  
--JRF-Agregar el campo de distincion de estado de los Debit Memos.
--JHD-Filtrar los Pax que no sean NoShow
CREATE Procedure dbo.COTIPAX_Sel_RptxPDB      
@RangoFechaInicio smalldatetime,      
@RangoFechaFin smalldatetime      
As      
 Set NoCount On      
 Select Distinct x.NroCorrelativo,x.IDFile,x.Orden, x.Identidad,x.Apellidos,x.Nombres,x.CodigoPais,x.Banco,x.FechaIngPais,      
     Convert(char(10),x.FecInicio,103) as FechaInicio,Convert(Char(10),x.FechaOut,103) as FechaOut,x.Total,      
     x.IDDebitMemo,x.FechaDebitMemo,x.TotalDebitMemo,x.FormaPago,x.NoOperacion ,x.EstadoDebit   
 from(      
 select cp.IDPax,substring(ti.Descripcion,1,3)+' : '+ Isnull(cp.NumIdentidad,'') as Identidad,cp.Apellidos,cp.Nombres      
 ,Case When Isnull(u.Codigo,'') = 'NAP' Then '' Else Isnull(u.Codigo,u.Descripcion) End as CodigoPais      
 ,Isnull(cp.FecIngresoPais,cb.FecInicio) As FechaIngPais,cb.FecInicio,cb.FechaOut,cp.Total ,Isnull(ban.Sigla,'') as Banco     
 --,case when d.IDDebitMemo is null then '' else SUBSTRING(d.IDDebitMemo,9,10) End as IDDebitMemo_Corr      
 ,Isnull(d.IDDebitMemo,'') as IDDebitMemo,Case when d.Fecha is null then '' Else Convert(char(10),d.Fecha,103) End as FechaDebitMemo      
 ,Isnull(d.Total,0) as TotalDebitMemo      
 , Isnull(f.Descripcion,'')  --(select distinct NuIngreso from INGRESO_DEBIT_MEMO idm where idm.IDDebitMemo = d.IDDebitMemo)    
  As FormaPago,Isnull(ing.CoOperacionBan,'') as NoOperacion,Isnull(cb.IDFile,'') as IDFile,cp.Orden ,
  case d.IDEstado     
	when 'PD' Then 'PENDIENTE'
	when 'PG' Then 'PAGADO'
	when 'AN' Then 'ANULADO'
 Else '' End as EstadoDebit,
 CAST(substring(d.IDDebitMemo,9,10) as tinyint) as NroCorrelativo
 from INGRESO_DEBIT_MEMO idm Right Join DEBIT_MEMO d On idm.IDDebitMemo = d.IDDebitMemo    
 Left Join COTICAB cb On d.IDCab = cb.IDCAB Left Join COTIPAX cp On cb.IDCAB = cp.IDCab    
 Left Join MAUBIGEO u On cp.IDNacionalidad = u.IDubigeo Left Join MATIPOIDENT ti On cp.IDIdentidad = ti.IDIdentidad      
 Left Join INGRESO_FINANZAS ing On idm.NuIngreso = ing.NuIngreso    
 Left Join MAFORMASPAGO f On ing.IDFormaPago = f.IdFor    
 Left Join MABANCOS ban On ing.IDBanco = ban.IDBanco    
 where cb.Estado = 'A'      
 --jorge
 and cp.flnoshow=0
 ) as x      
 where ((CONVERT(char(10),@RangoFechaInicio,103)='01/01/1900' and CONVERT(char(10),@RangoFechaFin,103)='01/01/1900')      
     Or x.FechaIngPais between @RangoFechaInicio and @RangoFechaFin)      
 Order By cast(FechaIngPais as smalldatetime),X.IDFile,x.Orden      

