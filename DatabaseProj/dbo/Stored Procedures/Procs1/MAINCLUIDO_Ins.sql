﻿--JRF-20140317-Nuevo campo "Orden"    
--JRF-20140428-Nuevo campo TxObsStatus  
--JRF-20150304-Nuevo campo "FlAutomatico"
CREATE Procedure [dbo].[MAINCLUIDO_Ins]      
 @Tipo char(1),      
 @DescAbrev varchar(100),      
 @PorDefecto bit,    
 @FlAutomatico bit,  
 @Orden tinyint,    
 @TxObsStatus text,  
 @UserMod char(4),      
 @pIDIncluido int output      
As      
 Set NoCount On      
       
 Insert into MAINCLUIDO       
  ([Tipo],      
   [DescAbrev],    
   [PorDefecto],   
   [FlAutomatico], 
   [Orden],    
   [TxObsStatus],  
   [UserMod],      
   [FecMod])      
   values       
   (case when LTRIM(RTRIM(@Tipo))='' then null else @Tipo End,      
   @DescAbrev,      
   @PorDefecto,    
   @FlAutomatico,  
   @Orden,    
   Case When ltrim(rtrim(CAST(@TxObsStatus as varchar(max))))='' then Null Else @TxObsStatus End,  
   @UserMod,GETDATE())      
 Set @pIDIncluido=(Select Top 1 IDIncluido From MAINCLUIDO ORder by IDIncluido Desc)      
