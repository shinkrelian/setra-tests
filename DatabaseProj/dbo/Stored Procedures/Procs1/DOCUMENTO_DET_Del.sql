﻿
Create Procedure dbo.DOCUMENTO_DET_Del
	@NuDocum Char(10),   
	@IDTipoDoc char(3),
	@NuDetalle tinyint
As
	Set NoCount On  

	Delete 
	From DOCUMENTO_DET
	Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc
		And NuDetalle=@NuDetalle
		
