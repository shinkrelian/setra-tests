﻿Create Procedure [dbo].[MABANCOS_Sel_Rpt]
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Sigla , Descripcion, 
	Case Moneda When 'N' Then 'Nacional' When 'E' Then 'Extrangera' End as Moneda,
	IDBanco
	From MABANCOS
	Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')
