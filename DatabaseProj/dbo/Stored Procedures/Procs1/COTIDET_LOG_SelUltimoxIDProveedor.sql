﻿Create Procedure dbo.COTIDET_LOG_SelUltimoxIDProveedor
	@IDCab	int,
	@IDProveedor char(6)
As
	Set Nocount on
	
	Select Top 1 Servicio From COTIDET_LOG 
	Where IDProveedor=@IDProveedor And IDCAB=@IDCab And Accion<>' '
	Order by FechaLog Desc
