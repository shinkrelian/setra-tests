﻿ 
  
--HLF-20130703-Agregando campos p.IDTipoProv, sd.PlanAlimenticio, sd.ConAlojamiento  
--HLF-20130902-Agregando subquery IDReserva_Det
CREATE Procedure dbo.COTIDET_SelxRangoFechasProvee    
 @IDCab int,    
 @IDProveedor char(6),    
 @IDServicio_Det int,    
 @FechaIni smalldatetime,    
 @FechaFin smalldatetime    
    
As    
 Set Nocount On    
     
 Select cd.*,p.IDTipoProv, sd.PlanAlimenticio, sd.ConAlojamiento,
 isnull((Select Top 1 isnull(IDReserva_Det,0) From RESERVAS_DET Where IDDet=cd.IDDET),0) as IDReserva_Det
 From COTIDET cd   
 Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det  
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor  
 Where cd.IDCAB=@IDCab And cd.IDProveedor=@IDProveedor    
 And cd.Dia between @FechaIni And @FechaFin     
 And cd.IDServicio_Det=@IDServicio_Det    