﻿
--PPMG-20160428-Se aumento el lenght @NumIdentidad varchar(25)
CREATE PROCEDURE [dbo].[COTIPAX_UPD_EXCEL_APT]  
				@IDPax int,
				@NumberAPT varchar(20),
				@Apellidos varchar(50),
				@Nombres varchar(50),
				@Titulo	varchar(5),
				@ObservEspecial varchar(200),
				@FecNacimiento smalldatetime='01/01/1900',
				@IDNacionalidad char(6),
				@NumIdentidad varchar(25),
				@UserMod char(4),
				@NoRutaArchivoExcelPax varchar(250)='',
				@CoUserExcelPax char(4)='',
				@NoPCExcelPax varchar(80)=''
AS  
BEGIN  
	UPDATE COTIPAX  
	SET
		NumberAPT = UPPER(@NumberAPT),
		Apellidos = UPPER(@Apellidos),
		Nombres = UPPER(@Nombres),
		Titulo = CASE WHEN RTRIM(ISNULL(@Titulo,'')) = '' THEN 'Mr.' ELSE @Titulo END,
		ObservEspecial = @ObservEspecial,
		FecNacimiento = Case When @FecNacimiento='01/01/1900' Then Null Else @FecNacimiento End,  
		IDNacionalidad= CASE WHEN RTRIM(ISNULL(@IDNacionalidad,'')) = '' THEN '000004' ELSE @IDNacionalidad END,
		IDIdentidad = '004',
		NumIdentidad = @NumIdentidad,
		FlUpdExcel = 1,
		NoRutaArchivoExcelPax = @NoRutaArchivoExcelPax,
		CoUserExcelPax = @CoUserExcelPax,
		NoPCExcelPax = @NoPCExcelPax,
		UserMod = @UserMod,
		FecMod = GETDATE()
	WHERE IDPax = @IDPax
END;

