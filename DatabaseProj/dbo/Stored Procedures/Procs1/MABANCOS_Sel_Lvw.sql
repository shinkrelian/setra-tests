﻿CREATE Procedure [dbo].[MABANCOS_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion	
	From MABANCOS
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDBanco<>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
