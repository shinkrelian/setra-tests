﻿
Create Procedure dbo.COTIVUELOS_RptNotaVenta
@IDNotaVenta char(10)
As
	Set NoCount On
	Declare @IDCab int
	set @IDCab = (select IDCab from NOTA_VENTA where IDNotaVenta = @IDNotaVenta)
	
	Select Convert(char(10),Fec_Salida,103) As FecSalida,cd.Servicio As Tren, 
	cv.CodReserva ,cv.Ruta
	from COTIVUELOS cv Left Join COTICAB c On c.IDCAB = cv.IDCAB
	Left Join COTIDET cd On cd.IDDET = cv.IdDet
	Where cv.IDCAB = @IDCab And cv.TipoTransporte = 'T'
