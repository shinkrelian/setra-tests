﻿CREATE Procedure dbo.ASIGNACION_SelAnticiposxNuAsignacionReproceso      
	@FecAcredIni smalldatetime,
	@FecAcredFin smalldatetime
As
 SELECT CC.IDCAB, 
 cc.IDFile,
 idm.SsPago + idm.SsGastosTransferencia as SsPago,  
 ifz.FeAcreditada    
 FROM ASIGNACION asg         
 Inner Join INGRESO_DEBIT_MEMO idm On asg.NuAsignacion=idm.NuAsignacion       
 Inner Join INGRESO_FINANZAS ifz On idm.NuIngreso=ifz.NuIngreso  
 --And asg.NuAsignacion=@NuAsignacion       
 And ifz.FeAcreditada Between @FecAcredIni and @FecAcredFin
 Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo       
 Inner Join COTICAB cc On dm.IDCab=cc.IDCAB     
 --And asg.FeAsignacion < cc.FechaOut        
 And datediff(m,asg.FeAsignacion,cc.FechaOut)>=1    
  And cc.EstadoFacturac='P'      
 Where idm.SsPago>0    
