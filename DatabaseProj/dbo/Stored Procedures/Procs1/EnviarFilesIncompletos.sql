﻿CREATE Procedure dbo.EnviarFilesIncompletos
As
Begin
Set NoCount On
Set Dateformat dmy
Declare @IDCabTemp int =0,
@CorreoDestinatarioTemp varchar(Max) ='',
@UsuarioTemp varchar(Max) ='',
@DatosIncompletosTemp bit =0,
@FecInicioTemp smalldatetime ='01/01/1900',
@IDUsuarioResTemp char(4)='0000',
@FechaINCuscoTemp smalldatetime ='01/01/1900',
@IDFileTemp char(8)='',
@EnviarMailTemp bit=0
Delete from COTICAB_MAILJOB

Insert into COTICAB_MAILJOB
Select * 
from (
	Select X.*, 
		   Case When DATEDIFF(D,Getdate(),X.FechaINCusco) > 0 Then 1 Else 0 End as EnviarMail
	from (
		Select IDCAB,uRes.Correo,uRes.Usuario,dbo.FnDatosIncompletosPaxxFile(IDCab) As DatosIncompletos,c.FecInicio,c.IDUsuarioRes,
			   DATEADD(D,-45,
			   Cast(Convert(char(10),(select Min(cd.Dia) From COTIDET cd Where cd.IDCAB=c.IDCAB And cd.IDubigeo='000066'),103) 
			   as smalldatetime)) as FechaINCusco,c.IDFile
		from COTICAB c Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
		Where Estado='A' and uRes.Activo='A')
	As X
	Where X.DatosIncompletos=1 --And x.FechaINCusco=45
) As Y
Where y.EnviarMail=1
Order By Y.IDUsuarioRes,Y.FechaINCusco asc


Declare CurEnviarMail cursor for
Select IDCab,CorreoDestinatario,Usuario,DatosIncompletos,IDUsuarioRes,FechaINCusco,IDFile,EnviarMail 
from COTICAB_MAILJOB
Open CurEnviarMail
Fetch Next From CurEnviarMail into @IDCabTemp,@CorreoDestinatarioTemp,@UsuarioTemp,@DatosIncompletosTemp,@IDUsuarioResTemp,@FechaINCuscoTemp,@IDFileTemp,@EnviarMailTemp
Begin
	While @@FETCH_STATUS = 0
	Begin
		If CONVERT(char(10),@FechaINCuscoTemp,103)=CONVERT(char(10),Getdate(),103)
		Begin
			Declare @msgCorreo nvarchar(Max)=''
			Execute dbo.CorreoHTMLFilesPendientesDatosFalantes @IDUsuarioResTemp,@FechaINCuscoTemp,@msgCorreo output
			--SELECT @msgCorreo,@CorreoDestinatarioTemp
			Execute dbo.EnviarCorreo 'SETRA',@CorreoDestinatarioTemp,'','',@msgCorreo,'Files con datos incompletos','HTML',''
			Delete from COTICAB_MAILJOB Where IDUsuarioRes=@IDUsuarioResTemp And Convert(Char(10),FechaINCusco,103)=Convert(char(10),@FechaINCuscoTemp,103)
		End		

		Fetch Next From CurEnviarMail into @IDCabTemp,@CorreoDestinatarioTemp,@UsuarioTemp,@DatosIncompletosTemp,@IDUsuarioResTemp,@FechaINCuscoTemp,@IDFileTemp,@EnviarMailTemp
	End
End
Close CurEnviarMail
DealLocate CurEnviarMail
End
