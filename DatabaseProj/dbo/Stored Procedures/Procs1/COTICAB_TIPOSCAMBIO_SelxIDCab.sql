﻿CREATE Procedure dbo.COTICAB_TIPOSCAMBIO_SelxIDCab  
@IDCab int  
As  
Set NoCount On   
select Isnull(ctc.IDCab,0) as IDCab,tcu.CoMoneda,mo.Descripcion as DescMoneda,mo.Simbolo as Simb  
,ISNULL(ctc.SsTipCam,tcu.SsTipCam) as SsTipCam,ub.IDubigeo as IDPais,  
Case When ctc.IDCab is not null then 1 else 0 End as AplicaTC  ,
Case When ctc.IDCab is not null then 1 else 0 End as AplicaTCam  

from (select * from COTICAB_TIPOSCAMBIO where IDCab = @IDCab) ctc  
Right Join MATIPOSCAMBIO_USD tcu On ctc.CoMoneda = tcu.CoMoneda  
Left Join MAMONEDAS mo On tcu.CoMoneda = mo.IDMoneda  
Left Join MAUBIGEO ub On mo.IDMoneda = ub.CoMoneda  
--where ctc.IDCab = @IDCab Or ctc.IDCab is null  
