﻿

---ALTER

--JHD-20141204-Agregar el campo FlPoConcretManualUpd para actualizar a 1 de forma manual
 CREATE Procedure dbo.COTICAB_Actualizar_PoConcretVta
	@IDCab int,
	@UserMod char(4),
	@PoConcretVta numeric(5,2)=0
	--@FlPoConcretManualUpd bit=0
 As
 BEGIN
	Set Nocount On
	
	Update COTICAB 
	Set PoConcretVta=@PoConcretVta,
	FlPoConcretManualUpd=1,
	FecMod=GETDATE(),
	UserMod=@UserMod	
	
	--From COTICAB cc Left Join MASERIESCLIENTE sc On sc.NuSerie=cc.NuSerieCliente
	Where IDCAB=@IDCab
END;
