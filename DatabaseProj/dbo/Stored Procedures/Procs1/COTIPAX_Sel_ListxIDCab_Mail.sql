﻿--JRF-20130516-Mostrar las 3 primeras Letras (DC,Descripcion Identidad)        
--JRF-20130516-Nuevo Filtro para Obtener los acomodos.    
--JRF-20140527-Considerar el titulo academico como prefijo al apellidos    
--JRF-20140528-Agregar TC si el campo FlTC esta Activo  
--JRF-20150106-Debe existir la relación entre Acomodos y Pax INNER JOIN  
--JHD-20150603-left Join ACOMODOPAX_PROVEEDOR ap On cp.IDPax = ap.IDPax and ap.IDReserva = @IDReserva 
--JRF-20150727-Left Join (SELECT distinct rd.IDRes...
--JRF-20160412-...And cp.FlNoShow = 0
CREATE PROCEDURE [dbo].[COTIPAX_Sel_ListxIDCab_Mail]                      
 @IDCab int,        
 @IDReserva int                        
As                        
                        
 Set NoCount On                         
                         
 Select distinct
 NroOrd, Apellidos, Nombres,Titulo,DescIdentidad,NumIdentidad, DescNacionalidad,                  
 FecNacimiento,Peso,Residente ,ObservEspecial,  IDIdentidad,                         
 IDNacionalidad,IDPax,ACC,CapacidadHab,EsMatrimonial,DescTC                 
 From                    
 (                         
 Select                   
 cp.Orden As NroOrd,                  
 cp.IDIdentidad,substring(di.Descripcion,1,3) as DescIdentidad, ltrim(rtrim(isnull(cp.NumIdentidad,''))) NumIdentidad,                         
 --cp.Titulo, cp.Nombres, cp.Apellidos, isnull(CONVERT(char(10),cp.FecNacimiento,103),'') As  FecNacimiento,          
 Case when ltrim(rtrim(cp.TxTituloAcademico)) <> '---' then cp.TxTituloAcademico else cp.Titulo End as Titulo, cp.Nombres, cp.Apellidos, isnull(CONVERT(char(10),cp.FecNacimiento,103),'') As  FecNacimiento,          
 cp.IDNacionalidad,isnull(u.DC,SUBSTRING(u.Descripcion,1,3)) as DescNacionalidad ,cp.IDPax ,Residente                   
 ,isnull(cp.Peso,0) As Peso,Isnull(cp.ObservEspecial,'') As ObservEspecial,                    
 Case When ISNUMERIC(SUBSTRING(Apellidos,4,3))=1 Then                      
  SUBSTRING(Apellidos,4,3)                    
 Else                    
  Nombres                    
 End as NroNombrePax,        
 Case When ap.CapacidadHab Is Not Null Then        
 dbo.FnMailAcomodosReservas(ap.CapacidadHab,ap.EsMatrimonial) + ' '+         
  CAST(ap.IDHabit As varchar(3))        
 Else '' End As ACC  ,      
 cast(IsNull(ap.CapacidadHab,rd.CapacidadHab) as varchar(5)) as  CapacidadHab,
 cast(IsNull(ap.EsMatrimonial,rd.EsMatrimonial) as CHAR(1)) as EsMatrimonial ,    
 Case When cp.FlTC = 1 then 'TC' Else '' End As DescTC                    
 From COTIPAX cp Left Join MATIPOIDENT di On cp.IDIdentidad=di.IDIdentidad                        
 Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo                        
 --Inner Join ACOMODOPAX_PROVEEDOR ap On cp.IDPax = ap.IDPax and ap.IDReserva = @IDReserva        
 left Join ACOMODOPAX_PROVEEDOR ap On cp.IDPax = ap.IDPax and ap.IDReserva = @IDReserva        
 Left Join (SELECT distinct rd.IDReserva,rdp.IDPax,rd.CapacidadHab,rd.EsMatrimonial
			FROM RESERVAS_DET_PAX rdp Inner Join RESERVAS_DET rd On rdp.IDReserva_Det=rd.IDReserva_Det
			Inner Join COTIDET cd On rd.IDDet=cd.IDDET
			WHERE IDReserva=@IDReserva And Anulado=0 And rd.CapacidadHab <> 0 
			And IsNull(cd.AcomodoEspecial,0)=0) rd On IsNull(ap.IDReserva,@IDReserva)=rd.IDReserva And rd.IDPax=cp.IDPax
 Where cp.IDCab=@IDCab And ISnULL(cp.FlNoShow,0) = 0) as X                    
 Order by NroOrd            
