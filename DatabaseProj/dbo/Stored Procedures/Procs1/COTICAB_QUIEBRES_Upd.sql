﻿CREATE Procedure [dbo].[COTICAB_QUIEBRES_Upd]
	@IDCAB_Q	int,    
    @Pax smallint,
    @Liberados smallint,
    @Tipo_Lib char(1),
    @UserMod	char(4)
As

	Set NoCount On
		
	Update COTICAB_QUIEBRES	Set
           Pax=@Pax
           ,Liberados=Case When @Liberados=0 Then Null Else @Liberados End
           ,Tipo_Lib=Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End
           ,UserMod=@UserMod
           ,FecMod=GETDATE()
    Where
           IDCAB_Q=@IDCAB_Q
