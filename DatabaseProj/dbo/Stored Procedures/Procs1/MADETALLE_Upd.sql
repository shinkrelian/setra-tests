﻿CREATE Procedure [dbo].[MADETALLE_Upd]
	@IDDetalle char(3),
	@Descripcion text,
	@IDIdioma char(12),
	@IDUsuario char(4),	
	@UserMod char(4)

As

	Set NoCount On

	Update MADETALLE
	Set Descripcion=@Descripcion,
	IDIdioma=@IDIdioma, 
	IDUsuario=@IDUsuario,
	Descri=Cast(@Descripcion as Varchar(10)), 
	UserMod=@UserMod,
	FecMod=GETDATE() 
	Where IDDetalle=@IDDetalle
