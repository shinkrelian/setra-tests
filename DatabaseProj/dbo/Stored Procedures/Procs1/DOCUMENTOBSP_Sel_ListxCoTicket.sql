﻿--HLF-20140704-@CoTicket char(13)  
--JRF-20140708-Addicionar las nuevas Columnas [FeEmision,CoPeriodo]
CREATE Procedure dbo.DOCUMENTOBSP_Sel_ListxCoTicket  
@CoTicket char(13)  
As  
Set NoCount On  
Select NuDocumBSP,SUBSTRING(NuDocumBSP,1,3)+'-'+SUBSTRING(NuDocumBSP,4,11) as NuDocumBSP_  
    ,CoTipDocBSP,  
    case CoTipDocBSP  
  when 'ACM' then 'ACM - CRÉDITO BSP'  
  when 'ADM' then 'ADM - DÉBITO BSP'  
  when 'EMD' then 'EMD - PREPAGO/POSTPAGO'  
  when 'RA ' then 'RA  - REFUND APPLICATION'  
    End as DesTipoDocumentoBSP,  FeEmision,CoPeriodo,
    SSTotalDocum  
from DOCUMENTOBSP  
Where CoTicket = @CoTicket  
