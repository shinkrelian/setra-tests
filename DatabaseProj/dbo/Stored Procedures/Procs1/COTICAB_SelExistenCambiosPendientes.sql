﻿Create Procedure dbo.COTICAB_SelExistenCambiosPendientes
@IDCab int,
@pAreaPendiente char(1) output
As
Set NoCount On
Set @pAreaPendiente = ''
Select @pAreaPendiente = Case When dbo.FnDevuelvePendienteUpdateMReservas(@IDCab)=1 Then 'R' 
						 Else Case When dbo.FnDevuelvePendienteUpdateMOperaciones(@IDCab)=1 Then 'O' Else '' End End
