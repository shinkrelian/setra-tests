﻿--JRF-20140212-Considerar ServicioVarios.  
--JRF-20140925-Ordenar por Item
CREATE Procedure [dbo].[COTIDET_Sel_TodosxIDCab]        
 @IDCab int        
As        
 Set NoCount On        
         
 Select Case when p.IDTipoProv='003'and sd.ConAlojamiento= 1 then sd.Descripcion else cd.Servicio End as Servicio2,    
 cd.*,p.IDTipoProv,sd.PlanAlimenticio,sd.ConAlojamiento,s.ServicioVarios,
 Case When Isnull(cd.IDDetRel,0)<>0 Then Isnull((select IDServicio_Det from COTIDET cd2 where cd2.IDDET=cd.IDDetRel),cd.IDServicio_Det)
	Else cd.IDServicio_Det End as IDServicioReal
 From COTIDET cd Left Join MAPROVEEDORES p on cd.IDProveedor = p.IDProveedor      
 Left Join MASERVICIOS_DET sd On cd.IDServicio_Det= sd.IDServicio_Det      
 Left Join MASERVICIOS s On sd.IDServicio = s.IDServicio  
 Where IDCAB=@IDCab
 Order By Item       
