﻿
--ALTER
--JHD-20141209-Agregar condicion para actualizar en Orden de Pago
--JHD-20150608-Agregar condicion para actualizar en Fondo Fijo
--JHD-20150703-Agregar condicion para actualizar en Orden de Compra
--JHD-20150928-@CoEstado CHAR(2)=''
--JHD-20150928-CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  ( ...
--JHD-20150928-IF rtrim(ltrim(@MonedaFEgreso))<>'' ...
--JHD-20151118-Actualizar agregando la tabla ORDEN_SERVICIO_DET_MONEDA
--PPMG-20160412-@IDDocFormaEgreso int=0
--PPMG-20160412-IF @IDDocFormaEgreso>0 ...
CREATE Procedure [dbo].[DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc]  
@NuDocumProv int ,
@CoEstado CHAR(2)=''
as  
Set NoCount On  
Declare @NuVoucher int=0,@NuOrdenServicio int=0 ,@MonedaFEgreso char(3)='',@CoOrdPag int=0,@NuFondoFijo int=0,
@NuOrdComInt int=0
	,@IDDocFormaEgreso int=0
  
Select @NuVoucher=Isnull(NuVoucher,0),
@NuOrdenServicio=Isnull(NuOrden_Servicio,0),
@MonedaFEgreso=Isnull(CoMoneda_FEgreso,''),
@CoOrdPag=Isnull(CoOrdPag,0),
@NuFondoFijo=Isnull(NuFondoFijo,0),
@NuOrdComInt=Isnull(NuOrdComInt,0),
@IDDocFormaEgreso=Isnull(iDDocFormaEgreso,0)
from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv  

Declare @TotalOrig numeric(9,2)=(select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) 
								 from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv and FlActivo=0)    
--select @NuVoucher  
if @NuVoucher > 0  
Begin  
 Update VOUCHER_OPERACIONES   
 set SsSaldo= Isnull(VOUCHER_OPERACIONES.SsSaldo,0)+@TotalOrig,
 --CoEstado=Case When (Isnull(VOUCHER_OPERACIONES.SsSaldo,0)+@TotalOrig) > 0 and VOUCHER_OPERACIONES.CoEstado='AC' then 'PD' Else VOUCHER_OPERACIONES.CoEstado End  
 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(VOUCHER_OPERACIONES.SsSaldo,0)+@TotalOrig) > 0 and VOUCHER_OPERACIONES.CoEstado='AC' then 'PD' Else VOUCHER_OPERACIONES.CoEstado End) END
 where IDVoucher=@NuVoucher  

	IF rtrim(ltrim(@MonedaFEgreso))<>''
		Update VOUCHER_OPERACIONES_DET
		 set SsSaldo= Isnull(VOUCHER_OPERACIONES_DET.SsSaldo,0)+@TotalOrig,
		 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(VOUCHER_OPERACIONES_DET.SsSaldo,0)+@TotalOrig) > 0 and VOUCHER_OPERACIONES_DET.CoEstado='AC' then 'PD' Else VOUCHER_OPERACIONES_DET.CoEstado End) END
		 where IDVoucher=@NuVoucher AND CoMoneda=@MonedaFEgreso

End  
if @NuOrdenServicio > 0  
Begin  
 Update ORDEN_SERVICIO set SsSaldo= Isnull(ORDEN_SERVICIO.SsSaldo,0)+@TotalOrig,
 --CoEstado=Case When (Isnull(ORDEN_SERVICIO.SsSaldo,0)+@TotalOrig) > 0 and ORDEN_SERVICIO.CoEstado='AC' then 'PD' Else ORDEN_SERVICIO.CoEstado End  
 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(ORDEN_SERVICIO.SsSaldo,0)+@TotalOrig) > 0 and ORDEN_SERVICIO.CoEstado='AC' then 'PD' Else ORDEN_SERVICIO.CoEstado End ) END
 where NuOrden_Servicio=@NuOrdenServicio  

 IF rtrim(ltrim(@MonedaFEgreso))<>''
		Update ORDEN_SERVICIO_DET_MONEDA
		 set SsSaldo= Isnull(ORDEN_SERVICIO_DET_MONEDA.SsSaldo,0)+@TotalOrig,
		 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(ORDEN_SERVICIO_DET_MONEDA.SsSaldo,0)+@TotalOrig) > 0 and ORDEN_SERVICIO_DET_MONEDA.CoEstado='AC' then 'PD' Else ORDEN_SERVICIO_DET_MONEDA.CoEstado End) END
		 where NuOrden_Servicio=@NuOrdenServicio  AND CoMoneda=@MonedaFEgreso

End  
if @CoOrdPag > 0  
Begin  
 Update ORDENPAGO set SsSaldo= Isnull(ORDENPAGO.SsSaldo,0)+@TotalOrig,
 --CoEstado=Case When (Isnull(ORDENPAGO.SsSaldo,0)+@TotalOrig) > 0 and ORDENPAGO.CoEstado='AC' then 'PD' Else ORDENPAGO.CoEstado End  
 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(ORDENPAGO.SsSaldo,0)+@TotalOrig) > 0 and ORDENPAGO.CoEstado='AC' then 'PD' Else ORDENPAGO.CoEstado End) END
 where IDOrdPag=@CoOrdPag  
End  
if @NuFondoFijo > 0  
Begin  
 Update MAFONDOFIJO set SsSaldo= Isnull(MAFONDOFIJO.SsSaldo,0)+@TotalOrig,
 --CoEstado=Case When (Isnull(MAFONDOFIJO.SsSaldo,0)+@TotalOrig) > 0 and MAFONDOFIJO.CoEstado='AC' then 'PD' Else MAFONDOFIJO.CoEstado End  
 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(MAFONDOFIJO.SsSaldo,0)+@TotalOrig) > 0 and MAFONDOFIJO.CoEstado='AC' then 'PD' Else MAFONDOFIJO.CoEstado End) END
 where NuFondoFijo=@NuFondoFijo  
End 
if @NuOrdComInt > 0  
Begin  
 Update ORDENCOMPRA set SsSaldo= Isnull(ORDENCOMPRA.SsSaldo,0)+@TotalOrig,
 --CoEstado=Case When (Isnull(ORDENCOMPRA.SsSaldo,0)+@TotalOrig) > 0 and ORDENCOMPRA.CoEstado='AC' then 'PD' Else ORDENCOMPRA.CoEstado End  
 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(ORDENCOMPRA.SsSaldo,0)+@TotalOrig) > 0 and ORDENCOMPRA.CoEstado='AC' then 'PD' Else ORDENCOMPRA.CoEstado End) END
 where NuOrdComInt=@NuOrdComInt  
End

if @IDDocFormaEgreso > 0  
Begin  
 Update DOCUMENTO_FORMA_EGRESO set SsSaldo= Isnull(DOCUMENTO_FORMA_EGRESO.SsSaldo,0)+@TotalOrig,
 --CoEstado=Case When (Isnull(ORDENPAGO.SsSaldo,0)+@TotalOrig) > 0 and ORDENPAGO.CoEstado='AC' then 'PD' Else ORDENPAGO.CoEstado End  
 CoEstado= CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (Isnull(DOCUMENTO_FORMA_EGRESO.SsSaldo,0)+@TotalOrig) > 0 and DOCUMENTO_FORMA_EGRESO.CoEstado='AC' then 'PD' Else DOCUMENTO_FORMA_EGRESO.CoEstado End) END
 where IDDocFormaEgreso=@IDDocFormaEgreso  
End  
;



