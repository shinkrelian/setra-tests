﻿--JHD-20150805-Se agrego consulta para detracciones
--JHD-20150805-WTCode= isnull((select CoSAP from MATIPODETRACCION where CoTipoDetraccion=d.CoTipoDetraccion),''),
--JHD-20150805-WTAmount=abs(isnull(d.SsDetraccion,0)),
			--WTAmountFC=0),
--JHD-20150915-WTAmountFC=dbo.FnCambioMoneda(abs(isnull(d.SsDetraccion,0)),'SOL','USD',ISNULL(SSTipoCambio,isnull((select ValVenta from matipocambio where fecha=FeEmision),0)))--0
--JHD-20150915-Redondear montos a 2 decimales
--JHD-20150915-Se agregaron los demas codigos de retenciones por honorarios
CREATE PROCEDURE Documento_Proveedor_List_SAP_WithholdingTaxData
@nudocumprov int
as
select

--WTCode= case when (d.CoTipoDoc='RH' AND CoTipoOC='007') then 'R4TA' else '' end,
WTCode= case when (d.CoTipoDoc in ('RH','HTD'))
		then 
			case when d.CoTipoOC='007' then 'R4TA'
				 when d.CoTipoOC='008' then 'ND24'
				 when d.CoTipoOC='009' then 'ND10'
				 else '' end
		else '' end,
WTAmount=round(abs(isnull(case when d.CoMoneda_FEgreso='SOL' THEN
							CASE WHEN d.comoneda='SOL' THEN d.ssigv
							else dbo.FnCambioMoneda(d.ssigv,d.comoneda,'SOL',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0))) end
				 else
					CASE WHEN d.comoneda='SOL' THEN d.ssigv--d.sstotaloriginal
					else dbo.FnCambioMoneda(d.ssigv,d.comoneda,'SOL',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0))) end
			  end,0)),2),
WTAmountFC=round(abs(isnull(case when d.CoMoneda<>'SOL' then d.ssigv else 0 end,0)),2)

from DOCUMENTO_PROVEEDOR d
where d.nudocumprov=@nudocumprov
--and (d.CoTipoDoc='RH' AND CoTipoOC='007')
and (d.CoTipoDoc in ('RH','HTD') AND CoTipoOC in ('007','008','009'))


UNION

select

WTCode= isnull((select CoSAP from MATIPODETRACCION where CoTipoDetraccion=d.CoTipoDetraccion),''),


WTAmount=round(abs(isnull(d.SsDetraccion,0)),2),
--WTAmountFC=0
WTAmountFC=round(dbo.FnCambioMoneda(abs(isnull(d.SsDetraccion,0)),'SOL','USD',ISNULL(SSTipoCambio,isnull((select ValVenta from matipocambio where fecha=FeEmision),0))),2)

--WTAmount=abs(isnull(case when d.CoMoneda_FEgreso='SOL' THEN
--							CASE WHEN d.comoneda='SOL' THEN d.SsDetraccion
--							else dbo.FnCambioMoneda(d.SsDetraccion,d.comoneda,'SOL',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0))) end
--				 else
--					CASE WHEN d.comoneda='SOL' THEN d.SsDetraccion--d.sstotaloriginal
--					else dbo.FnCambioMoneda(d.ssigv,d.comoneda,'SOL',ISNULL(d.sstipocambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0))) end
--			  end,0)),
--WTAmountFC=abs(isnull(case when d.CoMoneda<>'SOL' then d.SsDetraccion else 0 end,0))

from DOCUMENTO_PROVEEDOR d
where d.nudocumprov=@nudocumprov
and (d.SsDetraccion>0 )
