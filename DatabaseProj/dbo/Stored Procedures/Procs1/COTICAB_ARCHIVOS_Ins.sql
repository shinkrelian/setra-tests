﻿create PROCEDURE COTICAB_ARCHIVOS_Ins
	@IDCAB int=0,
	@Nombre varchar(200)='',
	@RutaOrigen varchar(200)='',
	@RutaDestino varchar(200)='',
	@CoTipo char(3)='',
	@FlCopiado bit=0,
	@CoUsuarioCreacion char(4)='',
	@UserMod char(4)
AS
BEGIN

Declare @IDArchivo tinyint =(select Isnull(Max(IDArchivo),0)+1 
								from COTICAB_ARCHIVOS where IDCAB=@IDCAB )

	Insert Into dbo.COTICAB_ARCHIVOS
		(
			IDArchivo,
			IDCAB,
 			Nombre,
 			RutaOrigen,
 			RutaDestino,
 			CoTipo,
 			FlCopiado,
 			CoUsuarioCreacion,
 			UserMod,
 			FecMod
 		)
	Values
		(
			@IDArchivo,
			@IDCAB,
 			@Nombre,
 			@RutaOrigen,
 			@RutaDestino,
 			@CoTipo,
 			@FlCopiado,
 			@CoUsuarioCreacion,
 			@UserMod,
 			getdate()
 		)   
END;
