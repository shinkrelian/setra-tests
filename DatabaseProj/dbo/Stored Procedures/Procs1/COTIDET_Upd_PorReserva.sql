﻿CREATE Procedure [dbo].[COTIDET_Upd_PorReserva]	
	@IDDet	int,
	@PorReserva	bit,
	@UserMod	char(4)
As
	Set NoCount On

	Update COTIDET Set PorReserva=@PorReserva,UserMod=@UserMod, ExecTrigger=0,
	FecMod=GETDATE()
	Where IDDET=@IDDet
