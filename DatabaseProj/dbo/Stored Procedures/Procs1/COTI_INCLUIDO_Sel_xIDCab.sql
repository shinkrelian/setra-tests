﻿CREATE Procedure [dbo].[COTI_INCLUIDO_Sel_xIDCab]  
 @IDCab int,  
 @IDIdioma varchar(12)  
As  
  
 Set NoCount On  
  
 Select   
  IDIncluido, Orden, Tipo,   
  Case When LTRIM(RTRIM(Descripcion))='' Then   
   (Select Descripcion From MAINCLUIDO_IDIOMAS Where IDIdioma='SPANISH' and IDIncluido=X.IDIncluido)  
  Else Descripcion   
  End as Descripcion,  
    
  Case When LTRIM(RTRIM(Descripcion))='' Then 1   
  Else  0   
  End as NoHallado  
 From  
 (   
 Select ci.IDIncluido, ci.Orden ,mi.Tipo, mid.Descripcion From COTI_INCLUIDO ci   
 Inner Join MAINCLUIDO mi On ci.IDIncluido=mi.IDIncluido And ci.IDCAB=@IDCab   
 Inner Join MAINCLUIDO_IDIOMAS mid On mi.IDIncluido=mid.IDIncluido And mid.IDIdioma=@IDIdioma  
 ) as X  
 Order by Orden  
