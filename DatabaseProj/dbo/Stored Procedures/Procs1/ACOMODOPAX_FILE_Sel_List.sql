﻿
----------------------------------------------------------------------------------------

CREATE Procedure ACOMODOPAX_FILE_Sel_List     
@IDCAB integer    
As    
 Set NoCount On    
 Select ac.IDCAB,ac.IDPax,IDHabit,CapacidadHab,EsMatrimonial,ac.UserMod
 from ACOMODOPAX_FILE ac Left Join COTIPAX cp On ac.IDPax = cp.IDPax 
 Where ac.IDCAB = @IDCAB    
 Order By CapacidadHab,IDHabit,cp.Orden
