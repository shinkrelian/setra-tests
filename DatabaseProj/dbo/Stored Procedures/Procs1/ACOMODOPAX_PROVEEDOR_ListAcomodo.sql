﻿--HLF-20130528-Agregando cp.Orden,cp.Peso,        
--JRF-20140528-Considerar descTC para cuando el campo FlTC este activo.    
--JRF-20150219-Considerar Cambios de Ingresos Manuales
CREATE Procedure [dbo].[ACOMODOPAX_PROVEEDOR_ListAcomodo]                
@IDReserva int                
As                
 Set NoCount On       
 select * from  (               
 select distinct ap.IDPax,r.IDFile, p.NombreCorto,p.Direccion, cb.Titulo,cb.NroPax,                
   u.Nombre ,                
   ISNULL(r.CodReservaProv,'') As CodReservaProv                
   ,ub.Descripcion as Nacionalidad,i.IDidioma,          
   ltrim(rtrim(convert(varchar,rd.Dia,103))) as FechaIn,                 
   ltrim(rtrim(convert(varchar,rd.FechaOut,103))) as FechaOut                
    ,rd.Servicio,                
     ti.Descripcion As TipoDoc,isnull(cp.NumIdentidad,'') As NumIdentidad,        
     cp.Orden,cp.Peso,        
     cp.Titulo + ' '+ cp.Nombres + ' ' + cp.Apellidos As NombrePax,                
   Isnull(convert(varchar(10),cp.FecNacimiento,103),'') As FechNacimiento , Isnull(ubp.DC,'') As NacionalidadPax              
    ,ap.IDHabit,ap.CapacidadHab ,ap.EsMatrimonial , isnull(cp.ObservEspecial ,'') As ObservEspecial  ,        
    Isnull(cb.Simple,0)+Isnull(cb.SimpleResidente,0) as TotalCantSimple,        
    Isnull(cb.Twin,0)+Isnull(cb.TwinResidente,0) as TotalCanTwin,        
    Isnull(cb.Matrimonial,0)+Isnull(cb.MatrimonialResidente,0) as TotalCantMatrimonial,        
    Isnull(cb.Triple,0)+Isnull(cb.TripleResidente,0) as TotalCantTriple,        
    Isnull(r.Observaciones,'') As Observaciones ,      
    case when (select COUNT(iddet) from cotidet cd      
   where AcomodoEspecial = 1 and cd.IncGuia = 0 and IDCab = r.IDCab and r.IDProveedor = cd.IDProveedor      
   and cd.Dia between rd.Dia and rd.FechaOut)>0       
   then 1 Else 0 End as ExisteAcomodoEspecial ,rd.IDDet ,    
   Case When cp.FlTC = 1 then 'TC' else '' End as DescTC     
           
    from ACOMODOPAX_PROVEEDOR ap                 
    Inner Join COTIPAX cp On ap.IDPax = cp.IDPax                
    Left Join MATIPOIDENT ti On ti.IDIdentidad = cp.IDIdentidad              
    Left Join COTICAB cb On cp.IDCab = cb.IDCAB                
    Left Join MAUSUARIOS u On cb.IDUsuarioRes = u.IDUsuario                
    Left Join MAUBIGEO ub On cb.IDubigeo = ub.IDubigeo                
    Left Join MAUBIGEO ubp On cp.IDNacionalidad = ubp.IDubigeo              
    Left Join MAIDIOMAS i On cb.IDIdioma = i.IDidioma                
    Left Join RESERVAS r On ap.IDReserva = r.IDReserva                
    Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor                
    Left Join RESERVAS_DET rd On ap.IDReserva = rd.IDReserva And rd.CapacidadHab = ap.CapacidadHab And rd.EsMatrimonial = ap.EsMatrimonial  and rd.Anulado = 0  And rd.FlServicioIngManual=0
    Left Join COTIDET cd On rd.IDDet = cd.IDDET      
  Where ap.IDReserva = @IDReserva) as X                
  --Group By ap.IDPax          
  where X.ExisteAcomodoEspecial = 0      
  Order By X.CapacidadHab Asc,IDHabit Asc ,Servicio         
