﻿--HLF-20121105-Agregando Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det And sd.PlanAlimenticio=0          
--JRF-20130403-Adicional a Hoteles deberan Listarse los Operadores con alojamiento.        
--JRF-20131210-Considerar no guias para estos reportes.    
--HLF-20151014-Isnull(cdd.NoHotel,p.NombreCorto) as DescProveedor,
--IsNull(cdd.TxWebHotel,IsNull(p.Web,'')) as Web
--Left Join MASERVICIOS_DIA sdia ...--Left Join COTIDET_DESCRIPSERV cdd ...  Left Join MASERVICIOS_ALIMENTACION_DIA sad
--HLF-20160113-(Select ROW_NUMBER() OVER (PARTITION BY IDServicio ORDER BY IDServicio) AS DiaServ,* From COTIDET cd1 ...) cd  
--comentando And IsNull(cd.IDDetRel,0)=0	
--=cd.DiaServ	And isnull(sdia.Dia,0) <>0
--PPMG-20160122-order by convert(datetime,Dia)
--HLF-20160203-IF @NroDia=0 and @IDIdioma=''	If not exists(select Dia from #Hoteles where NroDia>0 and IDServicio=@IDServicio)
CREATE Procedure [dbo].[COTIDET_Sel_Hoteles]
 @IDCab int              
As              
Begin
 Set NoCount On              
 Declare @Dia datetime,@IDProveedor char(6),@Desayuno bit,@Lonche bit,@Almuerzo bit,@Cena bit,@Categoria varchar(90),@IDUbigeo varchar(6)          
 ,@DescUbigeo varchar(60),@DescrProvee varchar(100),@Servicio varchar(max),@Web varchar(max),@NroDia tinyint,@IDIdioma varchar(12), @IDServicio char(8)    
          
 Create table #Hoteles (Dia datetime,IDProveedor Char(6),Desayuno bit,Lonche bit,Almuerzo bit,Cena bit,Categoria varchar(90),IDUbigeo varchar(6),          
 DescUbigeo varchar(60),DescProveedor varchar(100),Servicio varchar(max),Web varchar(max), NroDia tinyint, IDIdioma varchar(12), IDServicio char(8))            
             
 Declare CurHoteles cursor for              
 Select X.Dia,X.IDProveedor,X.Desayuno,X.Lonche,X.Almuerzo,X.Cena,
        X.Categoria,X.IDUbigeo,X.DescUbigeo,X.DescProveedor,X.Servicio,
		Case When charindex('http://',X.Web)=0 then (select IsNull(Web,'') from MAPROVEEDORES p Where p.IDProveedor=X.IDProveedor) else X.Web End as Web,
		NroDia, IDIdioma, IDServicio
 from (        
 Select cd.Dia,cd.IDProveedor,cd.Desayuno,cd.Lonche,cd.Almuerzo,cd.Cena, p.NombreCorto as Categoria, p.IDCiudad as IDUbigeo, u.Descripcion as DescUbigeo,              
 p.NombreCorto as DescProveedor,cd.Servicio, IsNull(p.Web,'') as Web, 0  as NroDia   ,'' as IDIdioma, sd.IDServicio         
 From COTIDET cd               
 Inner Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor And p.IDTipoProv='001'              
 Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det And sd.PlanAlimenticio=0          
 Left Join MAUBIGEO u On cd.IDubigeo=u.IDubigeo              
 Where cd.IDCAB=@IDCab and cd.IncGuia =0      
 Union        
 Select cd.Dia,cd.IDProveedor,cd.Desayuno,cd.Lonche,cd.Almuerzo,cd.Cena, p.NombreCorto as Categoria, 
 
 --p.IDCiudad as IDUbigeo, 
 --isnull(sad.IDUbigeoDes,p.IDCiudad) as IDUbigeo, 
 sad.IDUbigeoDes as IDUbigeo, 

 --u1.Descripcion  as DescUbigeo,
 --case when sad.IDUbigeoDes is null then u1.Descripcion else u2.Descripcion  end as DescUbigeo,
 u2.Descripcion  as DescUbigeo,

 --p.NombreCorto as DescProveedor,
 Isnull(cdd.NoHotel,p.NombreCorto) as DescProveedor,
 Isnull(Cast(sd.DetaTipo as varchar(max)),sd.Descripcion) As Servicio, 
 --IsNull(p.Web,'') as Web              
 IsNull(cdd.TxWebHotel,IsNull(p.Web,'')) as Web, isnull(sdia.Dia,0) as NroDia, isnull(sdia.IDIdioma,'') as IDIdioma,
 sd.IDServicio
 From 
 --COTIDET cd               
 (Select ROW_NUMBER() OVER (PARTITION BY cd1.IDServicio ORDER BY cd1.IDServicio) AS DiaServ,cd1.* From COTIDET cd1
  Inner Join MAPROVEEDORES p On cd1.IDProveedor=p.IDProveedor And p.IDTipoProv='003'              
 Inner Join MASERVICIOS_DET sd On cd1.IDServicio_Det=sd.IDServicio_Det And sd.ConAlojamiento = 1    
 Where IDCAB=@idcab and cd1.IncGuia = 0) cd      
    
Inner Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor And p.IDTipoProv='003' 
   Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det And sd.ConAlojamiento = 1  
 --Inner Join MASERVICIOS_DIA sdia On sd.IDServicio=sdia.IDServicio 
 Left Join COTIDET_DESCRIPSERV cdd On cd.IDDET=cdd.IDDet And Left(ltrim(cdd.IDIdioma),9)='HOTEL/WEB' 
 And Right(rtrim(cdd.IDIdioma),1)=cd.DiaServ--=cast(sdia.dia as char(1))

 --Left Join MASERVICIOS_ALIMENTACION_DIA sad On sad.IDServicio=sdia.IDServicio And sad.dia=sdia.dia
 Left Join MASERVICIOS_ALIMENTACION_DIA sad On sad.IDServicio=cd.IDServicio And sad.dia=cd.DiaServ
 Left Join MAUBIGEO u2 On sad.IDUbigeoDes=u2.IDubigeo        
 Left Join MASERVICIOS_DIA sdia On sdia.IDServicio=cd.IDServicio  And sdia.dia=cd.DiaServ And sdia.IDIdioma='HOTEL/WEB' 
 Where cd.IDCAB=@IDCab and cd.IncGuia = 0-- And IsNull(cd.IDDetRel,0)=0
	--And isnull(sdia.Dia,0) <>0
 ) As X        
 --Where not X.IDUbigeo = '000001'
 --Order By  X.dia Asc
 Order By NroDia desc
            
 --Delete from #Hoteles            
             
 Open CurHoteles            
 fetch next from CurHoteles into @Dia,@IDProveedor,@Desayuno,@Lonche,@Almuerzo,@Cena,@Categoria,@IDUbigeo,@DescUbigeo,
	@DescrProvee,@Servicio,@Web,@NroDia, @IDIdioma, @IDServicio
 while (@@FETCH_STATUS=0)            
 begin            
            
 If Not Exists(select Dia from #Hoteles where DescProveedor = @DescrProvee And Servicio=@Servicio )            
 --where IDUbigeo=@IDUbigeo)--
	Begin            
	If not exists(select Dia from #Hoteles where IDUbigeo = @IDUbigeo And Servicio=@Servicio and NroDia>0)
		Begin
		IF @NroDia=0 and @IDIdioma=''
			Begin
			If not exists(select Dia from #Hoteles where NroDia>0 and IDServicio=@IDServicio)
				Insert Into #Hoteles            
				Values(@Dia,@IDProveedor,@Desayuno,@Lonche,@Almuerzo,@Cena,@Categoria,isnull(@IDUbigeo,''),isnull(@DescUbigeo,''),
					@DescrProvee,@Servicio,@Web,@NroDia,@IDIdioma, @IDServicio)       
			End
		Else
			Insert Into #Hoteles            
				Values(@Dia,@IDProveedor,@Desayuno,@Lonche,@Almuerzo,@Cena,@Categoria,isnull(@IDUbigeo,''),isnull(@DescUbigeo,''),
					@DescrProvee,@Servicio,@Web,@NroDia,@IDIdioma, @IDServicio) 
		End
  End            
 fetch next from CurHoteles into @Dia,@IDProveedor,@Desayuno,@Lonche,@Almuerzo,@Cena,@Categoria,@IDUbigeo,@DescUbigeo,
	@DescrProvee,@Servicio,@Web,@NroDia,@IDIdioma, @IDServicio
 end            
             
 close CurHoteles            
 deallocate CurHoteles            
             
 Select convert(varchar(10),Dia,103) As Dia,IDProveedor,Categoria, IDUbigeo, DescUbigeo, DescProveedor,Servicio, Web,NroDia,IDIdioma,IDServicio            
 From #Hoteles order by convert(datetime,Dia)

 --Drop Table #Hoteles
End
