﻿--JRF-20150318-Considerar el FlNoSHOW
--JRF-20150319-Considerar el segundo campo (SsReembolso)
CREATE Procedure dbo.COTITRANSP_PAX_SelxIDTransporte  
 @IDTransporte int  
As  
 Set Nocount on  
   
 SELECT Sel,IDPaxTransp, IDPax, NombrePax, CodReservaTransp, IDProveedorGuia, NombreGuia, FlNoShow,SsReembolso--, Orden1, Orden2  
 FROM   
 (  
 Select Case When ct.IDPax Is Null Then 0 Else 1 End as Sel,   
 ct.IDPaxTransp, cp.IDPax as IDPax,  
 cp.Titulo+' '+cp.Apellidos+' '+cp.Nombres as NombrePax,  
 ISNULL(ct.CodReservaTransp,'') as CodReservaTransp,  
 '' as IDProveedorGuia,  
 '' as NombreGuia,  
 1 as Orden1, cp.Orden as Orden2  ,cp.FlNoShow,ct.SsReembolso
 From (Select * From COTIPAX Where IDCab In (select IDCab from COTIVUELOS where ID=@IDTransporte)) cp   
  Left Join COTITRANSP_PAX ct On ct.IDPax=cp.IDPax And ct.IDTransporte=@IDTransporte   
 Where    
 ct.IDProveedorGuia is null   
 Union  
 Select 1 as Sel,   
 ct.IDPaxTransp, 0 as IDPax,  
 '' as NombrePax,  
 ISNULL(ct.CodReservaTransp,'') as CodReservaTransp,  
 ISNULL(ct.IDProveedorGuia,'') as IDProveedorGuia,  
 ltrim(rtrim(ISNULL(p.ApPaterno,'')+' '+ISNULL(p.Nombre,''))) as NombreGuia,  
 2 as Orden1,1 as Orden2  ,0 as FlNoShow,0 as SsReembolso
 From COTITRANSP_PAX ct   
 Left Join MAPROVEEDORES p On isnull(ct.IDProveedorGuia,'')=p.IDProveedor  
 Where ct.IDTransporte=@IDTransporte And not ct.IDProveedorGuia is null  
 ) AS X  
 ORDER BY Orden1, Orden2  
