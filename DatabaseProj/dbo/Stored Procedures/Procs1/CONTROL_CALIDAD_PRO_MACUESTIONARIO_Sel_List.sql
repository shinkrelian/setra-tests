﻿--JHD-20141229- Se agrego el campo TxComentario
--JRF-20150216- Adicionar Filtros de TipoProv y NombreProveedor
CREATE Procedure dbo.CONTROL_CALIDAD_PRO_MACUESTIONARIO_Sel_List  
@NuControlCalidad int,
@IDTipoPrv char(3),
@NombreProv varchar(80)
as  
Set NoCount On  
Select tp.Descripcion as TipoProv,p.NombreCorto as DescProveedor,  
    ccpc.FlExcelent,ccpc.FlVeryGood,ccpc.FlGood,ccpc.FlAverage,ccpc.FlPoor  
    ,TxComentario=isnull(ccpc.TxComentario,'')  
from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc 
Left Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad  
Left Join MACUESTIONARIO c On ccpc.NuCuest=c.NuCuest  
Left Join MAPROVEEDORES p On ccpc.IDProveedor=p.IDProveedor  
Left Join MATIPOPROVEEDOR tp On c.IDTipoProv=tp.IDTipoProv  
Where ccpc.NuControlCalidad=@NuControlCalidad and c.IDTipoProv is not Null  
And (LTRIM(RTRIM(@IDTipoPrv))='' Or p.IDTipoProv=@IDTipoPrv)
And (LTRIM(rtrim(@NombreProv))='' Or p.NombreCorto Like '%'+LTRIM(rtrim(@NombreProv))+'%')
