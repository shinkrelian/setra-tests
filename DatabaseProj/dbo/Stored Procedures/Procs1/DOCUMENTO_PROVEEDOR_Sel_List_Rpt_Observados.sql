﻿--JHD-20160321-Se limitaron los union para vouchers, ordenes de servicio y prepagos.
--JHD-20160321-and p.CoUbigeo_Oficina='000065'  
--JHD-20160321-and ((ub.IDPais='000323' and @Pais=1) or (ub.IDPais<>'000323' and @Pais=2) or (@Pais=0))
--JHD-20160322-CoEstado,
--JHD-20160322-Estado= isnull((select e.NOESTADO from ESTADO_OBLIGACIONESPAGO e where e.coestado= x.CoEstado),''),
--JHD-20160322-Cont_Pendientes= case when (coestado='PD') THEN 1 ELSE 0 end,
--JHD-20160322-Cont_Aceptados=case when (coestado='AC') THEN 1 ELSE 0 end
--JHD-20160322-((vd.IDVoucher In (select distinct NuVoucher from DOCUMENTO_PROVEEDOR where ((FeRecepcion >= @Fecha1 and FeRecepcion< @Fecha2 +1)) And FlActivo=1 aND NuVoucher Is Not Null)) or @Fecha1='01/01/1900')
--JHD-20160322-(os.NuOrden_Servicio In (select distinct NuOrden_Servicio from DOCUMENTO_PROVEEDOR where ((FeRecepcion >= @Fecha1 and FeRecepcion< @Fecha2 +1)) And FlActivo=1 aND NuOrden_Servicio Is Not Null) or @Fecha1='01/01/1900')
--JHD-20160322-(op.IDOrdPag In (select distinct CoOrdPag from DOCUMENTO_PROVEEDOR where ((FeRecepcion >= @Fecha1 and FeRecepcion< @Fecha2 +1) or @Fecha1='01/01/1900') And FlActivo=1 aND CoOrdPag Is Not Null) or @Fecha1='01/01/1900')
--JHD-20160322-@IDProveedor char(6) = '',
--JHD-20160322-and (@IDProveedor='' or p.IDProveedor like '%'+@IDProveedor+'%')   
--JHD-20160406-Se agregaron mas UNION para formas de egreso sin documentos
--JHD-20160414-@FlSinDocumentos bit=0
--JHD-20160414-and @FlSinDocumentos=0
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_List_Rpt_Observados]
@Fecha1 smalldatetime='01/01/1900',
@Fecha2 smalldatetime='01/01/1900',
@IDFile varchar(8) = '',                 
--@Proveedor varchar(50) = '',--'Setours',                                           
@IDProveedor char(6) = '',
@CoEstado char(2)='',              
@TipoVouOrd varchar(3)='',
@Pais tinyint=0,
@FlSinDocumentos bit=0
AS

BEGIN

SELECT 

IDFile,
EjecutivoVentas,
EjecutivoReservas,
EjecutivoOperaciones,
IDVoucher,
RazonComercial,
Titulo,
Proveedor ,
Observacion,
Pais,
TipoVouOrd,             
--CoEstado=case when Observacion<>'' then 'OB' ELSE coestado END,
--Estado=case when Observacion<>'' then 'OBSERVADO' ELSE isnull((select e.NOESTADO from ESTADO_OBLIGACIONESPAGO e where e.coestado= x.CoEstado),'') END,
CoEstado,
Estado= isnull((select e.NOESTADO from ESTADO_OBLIGACIONESPAGO e where e.coestado= x.CoEstado),''),
--Cont_Observados= case when Observacion<>'' THEN 1 ELSE 0 end,
Cont_Observados= case when (Observacion<>'' OR COESTADO='OB') THEN 1 ELSE 0 end,
--Cont_Pendientes= case when (Observacion='' and coestado='PD') THEN 1 ELSE 0 end,
--Cont_Aceptados=case when (Observacion='' and coestado='AC') THEN 1 ELSE 0 end
Cont_Pendientes= case when (coestado='PD') THEN 1 ELSE 0 end,
Cont_Aceptados=case when (coestado='AC') THEN 1 ELSE 0 end
FROM
(
--VOUCHERS
select distinct 
c.IDFile,uVen.Nombre+' '+IsNull(uven.TxApellidos,'') as EjecutivoVentas,
uRes.Nombre+' '+IsNull(uRes.TxApellidos,'') as EjecutivoReservas,
uope.Nombre+' '+IsNull(uope.TxApellidos,'') as EjecutivoOperaciones,
cast(vd.IDVoucher as varchar(50)) as IDVoucher,
cl.RazonComercial,
c.Titulo,
p.NombreCorto as Proveedor,

REplace(dbo.FnDevuelveSaltosDeLineaEnUnaSola(Cast( ( case when  IsNull(vd.TxObsDocumento,'')='' then IsNull(v.TxObsDocumento,'') else IsNull(vd.TxObsDocumento,'') end ) as varchar(Max)),vd.IDVoucher),char(13),' ') as Observacion,
--IsNull(vd.TxObsDocumento,'')  as Observacion,
(select Descripcion from MAUBIGEO u2 Where u2.IDubigeo=ub.IDPais) as Pais,
--into #tmpReporteVoucher

Case When p.IDProveedor='001836' Then 'PRL' Else 'VOU' End as TipoVouOrd,

 Case When p.IDProveedor<>'001836' Then        
	(SELECT isnull(CoEstado,'') FROM VOUCHER_OPERACIONES_DET WHERE IDVoucher=Vd.IDVoucher and comoneda=(case when od.FlServInManualOper=1 then (select top 1 IDMoneda from OPERACIONES_DET where IDOperacion_Det=od.IDOperacion_Det)   
	else  (select top 1  IDMoneda from RESERVAS_DET where IDReserva_Det=od.IDReserva_Det)  end))
 Else
	vd.CoEstado
 End as CoEstado



from VOUCHER_OPERACIONES_DET vd Inner Join OPERACIONES_DET od On vd.IDVoucher=od.IDVoucher
Inner Join OPERACIONES o On o.IDOperacion=od.IDOperacion
Inner Join COTICAB c On o.IDCab=c.IDCAB
Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
Left Join MAUSUARIOS uVen On c.IDUsuario=uVen.IDUsuario
Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
Left Join MAUSUARIOS uOpe On c.IDUsuarioOpe=uOpe.IDUsuario
Inner Join MACLIENTES cl On c.IDCliente=cl.IDCliente
Left Join MAUBIGEO ub On p.IDCiudad=ub.IDubigeo
left join VOUCHER_OPERACIONES v on v.IDVoucher=vd.IDVoucher
Where 
(
	(vd.IDVoucher In (select distinct NuVoucher
					from DOCUMENTO_PROVEEDOR where ((FeRecepcion >= @Fecha1 and FeRecepcion< @Fecha2 +1)) And FlActivo=1
					aND NuVoucher Is Not Null))
	or
	@Fecha1='01/01/1900'
)
and (c.IDFile=@IDFile or @IDFile='')
 --and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')   
 and (@IDProveedor='' or p.IDProveedor like '%'+@IDProveedor+'%')   
--And vd.CoEstado In ('OB','AC')
and p.CoUbigeo_Oficina='000065'
and ((ub.IDPais='000323' and @Pais=1) or (ub.IDPais<>'000323' and @Pais=2) or (@Pais=0))
and @FlSinDocumentos=0

UNION
--VOUCHERS SIN DOCUMENTOS
select distinct 
c.IDFile,uVen.Nombre+' '+IsNull(uven.TxApellidos,'') as EjecutivoVentas,
uRes.Nombre+' '+IsNull(uRes.TxApellidos,'') as EjecutivoReservas,
uope.Nombre+' '+IsNull(uope.TxApellidos,'') as EjecutivoOperaciones,
cast(vd.IDVoucher as varchar(50)) as IDVoucher,
cl.RazonComercial,
c.Titulo,
p.NombreCorto as Proveedor,

REplace(dbo.FnDevuelveSaltosDeLineaEnUnaSola(Cast( ( case when  IsNull(vd.TxObsDocumento,'')='' then IsNull(v.TxObsDocumento,'') else IsNull(vd.TxObsDocumento,'') end ) as varchar(Max)),vd.IDVoucher),char(13),' ') as Observacion,
--IsNull(vd.TxObsDocumento,'')  as Observacion,
(select Descripcion from MAUBIGEO u2 Where u2.IDubigeo=ub.IDPais) as Pais,
--into #tmpReporteVoucher

Case When p.IDProveedor='001836' Then 'PRL' Else 'VOU' End as TipoVouOrd,

 Case When p.IDProveedor<>'001836' Then        
	(SELECT isnull(CoEstado,'') FROM VOUCHER_OPERACIONES_DET WHERE IDVoucher=Vd.IDVoucher and comoneda=(case when od.FlServInManualOper=1 then (select top 1 IDMoneda from OPERACIONES_DET where IDOperacion_Det=od.IDOperacion_Det)   
	else  (select top 1  IDMoneda from RESERVAS_DET where IDReserva_Det=od.IDReserva_Det)  end))
 Else
	vd.CoEstado
 End as CoEstado
 

from VOUCHER_OPERACIONES_DET vd Inner Join OPERACIONES_DET od On vd.IDVoucher=od.IDVoucher
Inner Join OPERACIONES o On o.IDOperacion=od.IDOperacion
Inner Join COTICAB c On o.IDCab=c.IDCAB
Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
Left Join MAUSUARIOS uVen On c.IDUsuario=uVen.IDUsuario
Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
Left Join MAUSUARIOS uOpe On c.IDUsuarioOpe=uOpe.IDUsuario
Inner Join MACLIENTES cl On c.IDCliente=cl.IDCliente
Left Join MAUBIGEO ub On p.IDCiudad=ub.IDubigeo
left join VOUCHER_OPERACIONES v on v.IDVoucher=vd.IDVoucher
Where 
(
	(vd.IDVoucher NOT In (select distinct NuVoucher
					from DOCUMENTO_PROVEEDOR where NuVoucher Is Not Null))
	
	AND

	((VD.FeCreacion >= @Fecha1 and VD.FeCreacion< @Fecha2 +1) OR @Fecha1='01/01/1900')
)
and (c.IDFile=@IDFile or @IDFile='')
 --and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')   
 and (@IDProveedor='' or p.IDProveedor like '%'+@IDProveedor+'%')   
--And vd.CoEstado In ('OB','AC')
and p.CoUbigeo_Oficina='000065'
and ((ub.IDPais='000323' and @Pais=1) or (ub.IDPais<>'000323' and @Pais=2) or (@Pais=0))




UNION
--ordenes de servicio
select distinct            
 c.IDFile,   
 uVen.Nombre+' '+IsNull(uven.TxApellidos,'') as EjecutivoVentas,
uRes.Nombre+' '+IsNull(uRes.TxApellidos,'') as EjecutivoReservas,
uope.Nombre+' '+IsNull(uope.TxApellidos,'') as EjecutivoOperaciones,
                  
 SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,              
   cl.RazonComercial, 
   c.Titulo,        
    p.NombreCorto as Proveedor , 

	REplace(dbo.FnDevuelveSaltosDeLineaEnUnaSola(Cast( ( case when  IsNull(osm.TxObsDocumento,'')='' then IsNull(osm.TxObsDocumento,'') else IsNull(osm.TxObsDocumento,'') end ) as varchar(Max)),0),char(13),' ') as Observacion,

	(select Descripcion from MAUBIGEO u2 Where u2.IDubigeo=ub.IDPais) as Pais,
--into #tmpReporteVoucher

'OSV' as TipoVouOrd,
          
 osm.CoEstado

 from ORDEN_SERVICIO os
 left join ORDEN_SERVICIO_DET_MONEDA osm on os.NuOrden_Servicio=osm.NuOrden_Servicio
  Left Join MAPROVEEDORES p On os.IDProveedor=p.IDProveedor                           
  Left Join MAUBIGEO ub On p.IDCiudad=ub.IDubigeo
 Left Join COTICAB c On os.IDCab=c.IDCAB  
 Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente                    
 Left Join ESTADO_OBLIGACIONESPAGO eop On osm.CoEstado=eop.CoEstado         
 
 Left Join MAUSUARIOS uVen On c.IDUsuario=uVen.IDUsuario
Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
Left Join MAUSUARIOS uOpe On c.IDUsuarioOpe=uOpe.IDUsuario
              
 where             
     
  (@IDFile='' or c.IDFile like '%'+@IDFile+'%')                       
  --and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                         
   and (@IDProveedor='' or p.IDProveedor like '%'+@IDProveedor+'%')   
  and           

(             
os.NuOrden_Servicio In (select distinct NuOrden_Servicio
					from DOCUMENTO_PROVEEDOR where ((FeRecepcion >= @Fecha1 and FeRecepcion< @Fecha2 +1)) And FlActivo=1
					aND NuOrden_Servicio Is Not Null)
or @Fecha1='01/01/1900'
)
and p.CoUbigeo_Oficina='000065'
and ((ub.IDPais='000323' and @Pais=1) or (ub.IDPais<>'000323' and @Pais=2) or (@Pais=0))
and @FlSinDocumentos=0

--ORDENES DE SERVICIO SIN DOCUMENTOS
UNION

select distinct            
 c.IDFile,   
 uVen.Nombre+' '+IsNull(uven.TxApellidos,'') as EjecutivoVentas,
uRes.Nombre+' '+IsNull(uRes.TxApellidos,'') as EjecutivoReservas,
uope.Nombre+' '+IsNull(uope.TxApellidos,'') as EjecutivoOperaciones,
                  
 SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,              
   cl.RazonComercial, 
   c.Titulo,        
    p.NombreCorto as Proveedor , 

	REplace(dbo.FnDevuelveSaltosDeLineaEnUnaSola(Cast( ( case when  IsNull(osm.TxObsDocumento,'')='' then IsNull(osm.TxObsDocumento,'') else IsNull(osm.TxObsDocumento,'') end ) as varchar(Max)),0),char(13),' ') as Observacion,

	(select Descripcion from MAUBIGEO u2 Where u2.IDubigeo=ub.IDPais) as Pais,
--into #tmpReporteVoucher

'OSV' as TipoVouOrd,
          
 osm.CoEstado

 from ORDEN_SERVICIO os
 left join ORDEN_SERVICIO_DET_MONEDA osm on os.NuOrden_Servicio=osm.NuOrden_Servicio
  Left Join MAPROVEEDORES p On os.IDProveedor=p.IDProveedor                           
  Left Join MAUBIGEO ub On p.IDCiudad=ub.IDubigeo
 Left Join COTICAB c On os.IDCab=c.IDCAB  
 Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente                    
 Left Join ESTADO_OBLIGACIONESPAGO eop On osm.CoEstado=eop.CoEstado         
 
 Left Join MAUSUARIOS uVen On c.IDUsuario=uVen.IDUsuario
Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
Left Join MAUSUARIOS uOpe On c.IDUsuarioOpe=uOpe.IDUsuario
              
 where             
     
  (@IDFile='' or c.IDFile like '%'+@IDFile+'%')                       
  --and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                         
   and (@IDProveedor='' or p.IDProveedor like '%'+@IDProveedor+'%')   
  and           


(
	(os.NuOrden_Servicio NOT In (select distinct NuOrden_Servicio
					from DOCUMENTO_PROVEEDOR where NuOrden_Servicio Is Not Null))
	
	AND

	((os.FeCreacion >= @Fecha1 and os.FeCreacion< @Fecha2 +1) OR @Fecha1='01/01/1900')
)

and p.CoUbigeo_Oficina='000065'
and ((ub.IDPais='000323' and @Pais=1) or (ub.IDPais<>'000323' and @Pais=2) or (@Pais=0))


--ORDENES DE PAGO    
UNION              
              
select distinct  
 c.IDFile,                

 uVen.Nombre+' '+IsNull(uven.TxApellidos,'') as EjecutivoVentas,
uRes.Nombre+' '+IsNull(uRes.TxApellidos,'') as EjecutivoReservas,
uope.Nombre+' '+IsNull(uope.TxApellidos,'') as EjecutivoOperaciones,

 IDVoucher='OP-'+(CAST(op.IDOrdPag as varchar(5))),              
 cl.RazonComercial, 
 c.Titulo,          
 p.NombreCorto as Proveedor ,

 REplace(dbo.FnDevuelveSaltosDeLineaEnUnaSola(Cast( ( case when  IsNull(op.TxObsDocumento,'')='' then IsNull(op.TxObsDocumento,'') else IsNull(op.TxObsDocumento,'') end ) as varchar(Max)),0),char(13),' ') as Observacion,
 	(select Descripcion from MAUBIGEO u2 Where u2.IDubigeo=ub.IDPais) as Pais,
 'OPA' as TipoVouOrd,                   
 op.CoEstado

 from ORDENPAGO op              
 LEFT JOIN RESERVAS re on re.IDReserva=op.IDReserva              
  Left Join MAPROVEEDORES p On re.IDProveedor=p.IDProveedor                           
 Left Join COTICAB c On op.IDCab=c.IDCAB      
  Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente                   
 Left Join ESTADO_OBLIGACIONESPAGO eop On op.CoEstado=eop.CoEstado                   
   Left Join MAUSUARIOS uVen On c.IDUsuario=uVen.IDUsuario
Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
Left Join MAUSUARIOS uOpe On c.IDUsuarioOpe=uOpe.IDUsuario
  Left Join MAUBIGEO ub On p.IDCiudad=ub.IDubigeo                              
 where --(Ltrim(Rtrim(@CoEstado))='' Or op.CoEstado=@CoEstado)                       
-- op.CoEstado<>'OB'            
  --and       
  (@IDFile='' or c.IDFile like '%'+@IDFile+'%')          
  --and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                         
   and (@IDProveedor='' or p.IDProveedor like '%'+@IDProveedor+'%')   
             
  and                      
(
op.IDOrdPag In (select distinct CoOrdPag
					from DOCUMENTO_PROVEEDOR where ((FeRecepcion >= @Fecha1 and FeRecepcion< @Fecha2 +1) or @Fecha1='01/01/1900') And FlActivo=1
					aND CoOrdPag Is Not Null)
 or @Fecha1='01/01/1900'
)
and p.CoUbigeo_Oficina='000065'  
and ((ub.IDPais='000323' and @Pais=1) or (ub.IDPais<>'000323' and @Pais=2) or (@Pais=0))
and @FlSinDocumentos=0

--ORDENES DE PAGO SIN DOCUMENTOS
UNION

select distinct  
 c.IDFile,                

 uVen.Nombre+' '+IsNull(uven.TxApellidos,'') as EjecutivoVentas,
uRes.Nombre+' '+IsNull(uRes.TxApellidos,'') as EjecutivoReservas,
uope.Nombre+' '+IsNull(uope.TxApellidos,'') as EjecutivoOperaciones,

 IDVoucher='OP-'+(CAST(op.IDOrdPag as varchar(5))),              
 cl.RazonComercial, 
 c.Titulo,          
 p.NombreCorto as Proveedor ,

 REplace(dbo.FnDevuelveSaltosDeLineaEnUnaSola(Cast( ( case when  IsNull(op.TxObsDocumento,'')='' then IsNull(op.TxObsDocumento,'') else IsNull(op.TxObsDocumento,'') end ) as varchar(Max)),0),char(13),' ') as Observacion,
 	(select Descripcion from MAUBIGEO u2 Where u2.IDubigeo=ub.IDPais) as Pais,
 'OPA' as TipoVouOrd,                   
 op.CoEstado

 from ORDENPAGO op              
 LEFT JOIN RESERVAS re on re.IDReserva=op.IDReserva              
  Left Join MAPROVEEDORES p On re.IDProveedor=p.IDProveedor                           
 Left Join COTICAB c On op.IDCab=c.IDCAB      
  Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente                   
 Left Join ESTADO_OBLIGACIONESPAGO eop On op.CoEstado=eop.CoEstado                   
   Left Join MAUSUARIOS uVen On c.IDUsuario=uVen.IDUsuario
Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
Left Join MAUSUARIOS uOpe On c.IDUsuarioOpe=uOpe.IDUsuario
  Left Join MAUBIGEO ub On p.IDCiudad=ub.IDubigeo                              
 where --(Ltrim(Rtrim(@CoEstado))='' Or op.CoEstado=@CoEstado)                       
-- op.CoEstado<>'OB'            
  --and       
  (@IDFile='' or c.IDFile like '%'+@IDFile+'%')          
  --and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                         
   and (@IDProveedor='' or p.IDProveedor like '%'+@IDProveedor+'%')   
             

AND


(
	(op.IDOrdPag NOT In (select distinct IDOrdPag
					from DOCUMENTO_PROVEEDOR where IDOrdPag Is Not Null))
	
	AND

	((op.FechaEmision >= @Fecha1 and op.FechaEmision< @Fecha2 +1) OR @Fecha1='01/01/1900')
)

and p.CoUbigeo_Oficina='000065'  
and ((ub.IDPais='000323' and @Pais=1) or (ub.IDPais<>'000323' and @Pais=2) or (@Pais=0))

) AS X

where (@CoEstado ='' or CoEstado=@CoEstado)
and CoEstado is not null
and (@TipoVouOrd='' or TipoVouOrd=@TipoVouOrd)
order by TipoVouOrd,idfile,IDVoucher,CoEstado
END;	
