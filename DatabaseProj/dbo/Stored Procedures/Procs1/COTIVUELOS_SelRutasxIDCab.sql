﻿CREATE Procedure [dbo].[COTIVUELOS_SelRutasxIDCab]    
 @IDCab Int    
As    
 Set NoCount On    
     
 Select uo.Descripcion as Origen, ud.Descripcion as Destino, Tarifa As NetoPagar    
 From COTIVUELOS v Left Join MAUBIGEO uo On v.RutaO=uo.IDubigeo    
 Left Join MAUBIGEO ud On v.RutaD=ud.IDubigeo    
 Where IDCAB=@IDCab  And v.TipoTransporte = 'V' And EmitidoSetours = 1  
 Order by v.ID    
