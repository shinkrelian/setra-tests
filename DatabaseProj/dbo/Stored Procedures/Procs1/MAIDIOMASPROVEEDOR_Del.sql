﻿Create Procedure [dbo].[MAIDIOMASPROVEEDOR_Del]
	@IDIdioma	varchar(12), 
	@IDProveedor	char(6) 
	
As
	Set NoCount On
	
	Delete From MAIDIOMASPROVEEDOR 
	Where IDIdioma = @IDIdioma  And IDProveedor = @IDProveedor
