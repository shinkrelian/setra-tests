﻿Create Procedure dbo.DEBIT_MEMO_UpdEstadoAPendiente
@IDDebitMemo char(10),
@UserMod char(4)
As
	Set NoCount On
	Update DEBIT_MEMO 
		Set IDEstado = 'PD',
			UserMod = @UserMod,
			FecMod = GetDate()
	Where IDDebitMemo = @IDDebitMemo
