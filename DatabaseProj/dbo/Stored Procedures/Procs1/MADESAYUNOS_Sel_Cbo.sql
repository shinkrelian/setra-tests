﻿--HLF-20160125-@bDescTodos bit=1	case when @bDescTodos=1 then ...
CREATE Procedure [dbo].[MADESAYUNOS_Sel_Cbo]
	@bTodos bit,
	@bDescTodos bit=1
As  
  
 Set NoCount On  
   
 SELECT * FROM  
 (  
 Select '' as IDDesayuno,
	--'<TODOS>'
	case when @bDescTodos=1 then '<TODOS>' else '-----' end as Descripcion, 0 as Ord  
 Union  
 Select IDDesayuno,Descripcion,1 From MADESAYUNOS  
 ) AS X  
 Where (@bTodos=1 Or Ord<>0)  
 Order by Ord,2
