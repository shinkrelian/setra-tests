﻿CREATE Procedure [dbo].[MAOPCIONES_Sel_LvwxDescripcion]
	@ID	char(6),
	@Descripcion varchar(50)
As
	Set NoCount On
	
	Select Descripcion	
	From MAOPCIONES
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDOpc <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
