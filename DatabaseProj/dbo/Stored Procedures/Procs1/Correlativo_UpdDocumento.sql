﻿Create Procedure dbo.Correlativo_UpdDocumento
@Tabla varchar(30),
@IDTipoDoc char(3),
@CoSerie char(3),
@Correlativo int,
@UserMod char(4)
As
Set NoCount On
Update Correlativo
	set Correlativo = @Correlativo,
		UserMod = @UserMod,
		FecMod = GetDate()
Where Tabla = @Tabla and IDTipoDoc = @IDTipoDoc and CoSerie = @CoSerie
