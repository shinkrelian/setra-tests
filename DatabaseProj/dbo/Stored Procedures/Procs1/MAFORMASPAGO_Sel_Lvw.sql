﻿CREATE Procedure [dbo].[MAFORMASPAGO_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(50)
As
	Set NoCount On
	
	Select Descripcion	
	From MAFORMASPAGO
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IdFor<>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
