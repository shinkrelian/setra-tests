﻿--JRF-20130827-Correción en case del Titulo para reconocer el sexo.      
--JRF-20140603-Ajuste en el campo de la descripción de Tipos de IDENTIDAD.
CREATE Procedure [dbo].[COTIPAX_Sel_RptINC]         
@IDCab int        
as        
 Set NoCount On        
 select X.*,        
  case When Edad < 18   THEN '2' Else '1' End as Tipo          
  from(        
 select Apellidos,Nombres as Nombre,
  case when ti.IDIdentidad In ('004','003','002') then '1' else ti.Descripcion End as TipoDoc,         
  cp.NumIdentidad NroDoc,ISNULL(u.CodigoINC,'') as IDPais        
  , case upper(cp.Titulo) When 'MR.' then 'M' When 'MS.' Then 'F' End as Sexo,        
   --isnull( (((365* year(getdate()))-(365*(year(FecNacimiento))))+ (month(getdate())-month(FecNacimiento))*30        
   -- +(day(getdate()) - day(FecNacimiento)))/365,0) as Edad,        
  ISNULL(DATEDIFF(YEAR,FecNacimiento,getdate()),0) as Edad,        
   case ti.Descripcion         
    when 'PASAPORTE' Then 1        
    When 'DNI' then 2        
    When 'CARNET EXTRANJERIA' then 4        
    When 'CARNET UNIVERSITARIO' then 6        
   End as Categoria ,Orden  
 from COTIPAX cp Left Join MATIPOIDENT ti On cp.IDIdentidad= ti.IDIdentidad         
  Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo           
  where cp.IDCab=@IDCab        
  ) as X        
Order by X.Orden  
