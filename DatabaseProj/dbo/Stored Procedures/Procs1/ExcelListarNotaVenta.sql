﻿--JHD-20150707-Listar tabla VentasDebitMemoPendientes
--JHD-20150708-Nueva campo TicketFile para tabla VentasxCliente_Anual
--JHD-20151023-CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter,
--JHD-20151023-CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter1,  
--JHD-20151023-CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter2,
--JHD-20151023-CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter3
--JHD-20151222-Se agregaron nuevas tablas para clientes web
--JHD-20151228-Se agregaron las columnas TicketFile y TicketFileAnioAnter para las ventas por región
--JHD-20151228-Se agregaron nuevas tablas para ventas por region para clientes web
--JHD-20151229-VarImporteUSD= case when ImporteUSDAnioAnter=0 then 0 else VarImporteUSD end
--JHD-20160112-Descripcion=isnull(Descripcion,''),
--JHD-20160112-Supervisor=isnull(Supervisor,'')
--JRF-20160122-Agregar Tabla para "Zicasso" - Venta Directa
CREATE Procedure [dbo].[ExcelListarNotaVenta]
@Anio char(4),
@IDUser char(4)
As
Set NoCount On

Declare @AnioAnter char(4)=cast(@Anio as smallint)-1
select [FecOutPeru]
      ,[ImporteUSD]
      ,[ImporteUSDAnioAnter]
      ,[VarImporteUSD]
      ,[CantPax]
      ,[CantPaxAnioAnter]
      ,[VarCantPax]
      ,[CantFiles]
      ,[CantFilesAnioAnter]
      ,[VarCantFiles]
      ,[SumaProy]
      ,[Dias_Dif]
      ,[Edad_Prom]
from  VentasReceptivoMensual 
where ParamAnio=@Anio  order by FecOutPeru

select [FecOutPeru]
      ,[ImporteMesUSD]
      ,[ImporteMesUSDAnioAnter]
From  VentasReceptivoMensualAcumulado 
where ParamAnio=@Anio Order by cast(FecOutPeru as int)

select [Anio]
      ,[IDCliente]
      ,[DescCliente]
      ,[ImporteUSD]
      ,[ImporteUSDAnioAnter]
      ,[Crecimiento]
      ,[CantFiles]
      ,[CantFilesAnioAnter]
      ,[TicketFileAnioAnter]
      ,[PromMargen]
      ,[PromMargenAnioAnter]
      ,[Utilidad]
      ,[UtilidadAnioAnter]
      ,[Dias]
      ,[Edad_Prom]
	  ,[TicketFile]
from  VentasxCliente_Anual 
where ParamAnio=@Anio 
order by 4 desc

Select [IDUsuario]
      ,[NoEjecutiva]
      ,[EneCantFiles]
      ,[EneVentaMes]
      ,[FebCantFiles]
      ,[FebVentaMes]
      ,[MarCantFiles]
      ,[MarVentaMes]
      ,[AbrCantFiles]
      ,[AbrVentaMes]
      ,[MayCantFiles]
      ,[MayVentaMes]
      ,[JunCantFiles]
      ,[JunVentaMes]
      ,[JulCantFiles]
      ,[JulVentaMes]
      ,[AgoCantFiles]
      ,[AgoVentaMes]
      ,[SetCantFiles]
      ,[SetVentaMes]
      ,[OctCantFiles]
      ,[OctVentaMes]
      ,[NovCantFiles]
      ,[NovVentaMes]
      ,[DicCantFiles]
      ,[DicVentaMes]
From  VentasporEjecutivoMes2
where ParamAnio=@Anio order by NoEjecutiva

 Declare @SumTotal numeric(13,2)=(Select sum(VentaAnualUSD) From VentasporEjecutivoMes)      
 Declare @SumTotalAnioAnter numeric(13,2)=(Select sum(VentaAnualUSDAnioAnter) From VentasporEjecutivoAnio)
 SELECT *,      
 isnull(Case When @SumTotalAnioAnter<>0 Then (TotVendidoAnioAnter*100)/@SumTotalAnioAnter Else 0 End,0) as ParticipacionAnioAnter,
 VarEjecutivos=isnull((Case When TotVendido<>0 Then      
				 ((TotVendido-TotVendidoAnioAnter)*100)/TotVendido      
				 Else      
				 0
				end
				),0)
 FROM      
 (      
 Select *,      
	Case When @SumTotal<>0 Then(TotVendido*100)/@SumTotal Else 0 End as Participacion,    

 isnull((Select CantFilesAnioAnter From  VentasporEjecutivoAnio Where IDUsuario=X.IDUsuario And ParamAnio=@Anio),0) as CantFilesAnioAnter,      

 isnull((Select VentaAnualUSDAnioAnter From  VentasporEjecutivoAnio Where IDUsuario=X.IDUsuario And ParamAnio=@Anio),0) as TotVendidoAnioAnter       

 From      
 (      
 Select IDUsuario,Nombre as Ejecutivo,SUM(CantFiles) as CantFiles, SUM(VentaAnualUSD) as TotVendido    
 ,Prom_Margen=isnull(AVG (CASE WHEN Prom_Margen <> 0 THEN Prom_Margen ELSE NULL END),0)    
 From  VentasporEjecutivoMes      
 where ParamAnio=@Anio
 Group by idusuario,Nombre      
 ) as X      
 ) as Y      
 Order by 2

 PRINT 'HOJA COMPARATIVO'      
   
 Select FecOutPeru,  
 SumaProy as ImporteUSD,  
 ImporteUSDAnioAnter,      
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter1,       
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter2,       
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter3     
 From  VentasReceptivoMensual X  
 Where x.ParamAnio=@Anio
 Order by cast(FecOutPeru as int)           
      


 Select FecOutPeru,CantPax, CantPaxAnioAnter,      
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter1,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter2,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter3       
 From  VentasReceptivoMensual X  
 where ParamAnio=@Anio
 Order by cast(FecOutPeru as int)

  --CANT FILES      
 Select FecOutPeru,CantFiles, CantFilesAnioAnter,      
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
 CoMes=right(X.FecOutPeru,2)),0) as CantFilesAnioAnter1,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
 CoMes=right(X.FecOutPeru,2)),0) as CantFilesAnioAnter2,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
 CoMes=right(X.FecOutPeru,2)),0) as CantFilesAnioAnter3       
 From  VentasReceptivoMensual X 
 where ParamAnio=@Anio
 Order by cast(FecOutPeru as int)  

 --top 15 clientes      
       
 select top 15 IDCliente,      
    DescCliente,      
    ImporteUSD,      
    ImporteUSDAnioAnter,      
    isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter-1 And       
    CoCliente= VentasxCliente_Anual.IDCliente),0) as ImporteUSDAnioAnter1,       
          
    isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter-2 And       
    CoCliente= VentasxCliente_Anual.IDCliente),0) as ImporteUSDAnioAnter2,       
          
    isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter-3 And       
    CoCliente= VentasxCliente_Anual.IDCliente),0) as ImporteUSDAnioAnter3      
          
 from  VentasxCliente_Anual    
 where ParamAnio=@Anio  
 order by importeUSD desc      


 Select CoCliente,
 NoCliente=case when NoCliente='WEB (UNITED STATES OF AMERICA)' then 'WEB (USA)' when NoCliente='EVER BEST TRAVEL SERVICE (TOUCAN HOLIDAYS)' then 'EVER BEST TRAVEL SERVICE' else NoCliente END,
 CoPais,
 NoPais=case when nopais='UNITED STATES OF AMERICA' then 'USA' else NoPais END,
  EneVentaMes=ISNULL(EneVentaMes,0),  
  FebVentaMes=ISNULL(FebVentaMes,0),  
  MarVentaMes=ISNULL(MarVentaMes,0),  
  AbrVentaMes=ISNULL(AbrVentaMes,0),  
  MayVentaMes=ISNULL(MayVentaMes,0),  
  JunVentaMes=ISNULL(JunVentaMes,0),  
  JulVentaMes=ISNULL(JulVentaMes,0),  
  AgoVentaMes=ISNULL(AgoVentaMes,0),  
  SetVentaMes=ISNULL(SetVentaMes,0),  
  OctVentaMes=ISNULL(OctVentaMes,0),  
  NovVentaMes=ISNULL(NovVentaMes,0),  
  dicVentaMes=ISNULL(dicVentaMes,0),
  Total=  ISNULL(Total,0)
  From  VentasporClienteMes Where ParamAnio=@Anio  Order by NoPais, NoCliente 

print 'HOJA FILES' 
   Select [FecInicio]
		  ,[FecOut]
		  ,[FecOutPeru]
		  ,[IDCAB]
		  ,[IDFile]
		  ,[Titulo]
		  ,[ValorVenta]
		  ,[PoConcretVta]
		  ,[NroPax]
		  ,[ESTADO]
		  ,[IDUsuario]
		  ,[Responsable]
		  ,[IDCliente]
		  ,[CoPais]
		  ,[DescPaisCliente]
		  ,[DescCliente]
		  ,[MargenGan]
		  ,[Utilidad]
		  ,[Total_Proy]
		  ,[Total_Concret]
		  ,[SumaProy]
		  ,[SumaProy2]
		  ,[Dias]
		  ,[Edad_Prom]
		  ,[Total_Programado] 
 ,Usuario=isnull((select nombre from MAUSUARIOS where IDUsuario= VentasReceptivoMensualDet.IDUsuario),0)
 ,Mes=case when month(fecout)=1 then 'January'
 when month(fecout)=2 then 'February'
 when month(fecout)=3 then 'March'
 when month(fecout)=4 then 'April'
 when month(fecout)=5 then 'May'
 when month(fecout)=6 then 'June'
 when month(fecout)=7 then 'July'
 when month(fecout)=8 then 'August'
 when month(fecout)=9 then 'September'
 when month(fecout)=10 then 'October'
 when month(fecout)=11 then 'November'
 when month(fecout)=12 then 'December'
 else ''
 end
 --
 ,isnull(TxCategoria,'') as TxCategoria
 --
 --,CoTipoVenta = (select CoTipoVenta from COTICAB where IDCAB = VentasReceptivoMensualDet.IDCAB)
 --,SumTotalDM = IsNull((select Sum(DEBIT_MEMO.Total) from DEBIT_MEMO Where DEBIT_MEMO.IDCab= VentasReceptivoMensualDet.IDCAB And DEBIT_MEMO.IDEstado <> 'AN'),0)
  From  VentasReceptivoMensualDet  
  where (idusuario=@IDUser or @IDUser='') And ParamAnio=@Anio  
  order by Usuario,FecOutPeru,FecOut,FecInicio  

print 'HOJA ZICASSO' 
  Select * from (
   Select [FecInicio]
		  ,[FecOut]
		  ,[FecOutPeru]
		  ,[IDCAB]
		  ,[IDFile]
		  ,[Titulo]
		  ,[ValorVenta]
		  ,[PoConcretVta]
		  ,[NroPax]
		  ,[ESTADO]
		  ,[IDUsuario]
		  ,[Responsable]
		  ,[IDCliente]
		  ,[CoPais]
		  ,[DescPaisCliente]
		  , (select RazonComercial from MACLIENTES where IDCliente = (select IDCliente from COTICAB c Where c.IDCAB=VentasReceptivoMensualDet.IDCAB)) [DescCliente]
		  ,[MargenGan]
		  ,[Utilidad]
		  ,[Total_Proy]
		  ,[Total_Concret]
		  ,[SumaProy]
		  ,[SumaProy2]
		  ,[Dias]
		  ,[Edad_Prom]
		  ,[Total_Programado] 
 ,Usuario=isnull((select nombre from MAUSUARIOS where IDUsuario= VentasReceptivoMensualDet.IDUsuario),0)
 ,Mes=case when month(fecout)=1 then 'January'
 when month(fecout)=2 then 'February'
 when month(fecout)=3 then 'March'
 when month(fecout)=4 then 'April'
 when month(fecout)=5 then 'May'
 when month(fecout)=6 then 'June'
 when month(fecout)=7 then 'July'
 when month(fecout)=8 then 'August'
 when month(fecout)=9 then 'September'
 when month(fecout)=10 then 'October'
 when month(fecout)=11 then 'November'
 when month(fecout)=12 then 'December'
 else ''
 end
 --
 ,isnull(TxCategoria,'') as TxCategoria
 --
 ,CoTipoVenta = (select CoTipoVenta from COTICAB where IDCAB = VentasReceptivoMensualDet.IDCAB)
 ,SumTotalDM = IsNull((select Sum(DEBIT_MEMO.Total) from DEBIT_MEMO Where DEBIT_MEMO.IDCab= VentasReceptivoMensualDet.IDCAB And DEBIT_MEMO.IDEstado <> 'AN'),0)
 ,FecPagoDM = IsNull(Convert(char(10),(SELECT Top(1) ifz.FeAcreditada FROM INGRESO_FINANZAS ifz 
			   Inner Join INGRESO_DEBIT_MEMO idm On ifz.NuIngreso=idm.NuIngreso
			   Inner Join DEBIT_MEMO idm2 oN idm.IDDebitMemo=idm2.IDDebitMemo
			   Where idm2.IDCab= VentasReceptivoMensualDet.IDCAB Order By ifz.FeAcreditada desc),103),'')
  From  VentasReceptivoMensualDet  
  where (idusuario=@IDUser or @IDUser='') And ParamAnio=@Anio) as X
  Where x.CoTipoVenta = 'DR'
  order by Usuario,FecOutPeru,FecOut,FecInicio  

  print 'SIN APT' 
  select  [FecOutPeru]
		  ,[ImporteUSD]
		  ,[ImporteUSDAnioAnter]
		  ,[VarImporteUSD]
		  ,[SumaProy] 
  from  VentasReceptivoMensual_Sin_APT Where ParamAnio=@Anio Order by cast(FecOutPeru as int)  
  
   print 'ACUMULADO SIN APT' 
   select [FecOutPeru]
      ,[ImporteMesUSD]
      ,[ImporteMesUSDAnioAnter] 
   From  VentasReceptivoMensualAcumulado_Sin_APT Where ParamAnio=@Anio Order by cast(FecOutPeru as int)      
 
   print 'COMPARATIVO EDADES' 
	 Select FecOutPeru,  
	 Edad_Prom ,  
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter,      
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter1,       
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter2,       
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter3       
	 From  VentasReceptivoMensual X Where ParamAnio=@Anio  Order by cast(FecOutPeru as int)     
 
   print 'COMPARATIVO DIAS' 
 	 Select FecOutPeru,  
	 Dias_Dif ,  
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter,      
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter1,       
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter2,       
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter3       
	 From  VentasReceptivoMensual X Where ParamAnio=@Anio   Order by cast(FecOutPeru as int)    

select [Posicion]
      ,[Posicion_AnioAnt]
      ,[CoPais]
      ,[NoPais]
      ,[CoRegion]
      ,[Region]
      ,[Total]
      ,[Total_AnioAnt]
      ,[Crecimiento]
      ,[Dias_Actual]
      ,[Edad_Prom_Actual]
      ,[Dias]
      ,[Edad_Prom]
      ,[Cant_Clientes]
      ,[Cant_Clientes_Ant]
from  VentasporPais4
Where ParamAnio=@Anio
order by Posicion_AnioAnt

declare @Total_Region as decimal(15,2)=isnull((select SUM(Total) from  VentasporPais4 Where ParamAnio=@Anio),0)
declare @Total_Region_Ant as decimal(15,2)=isnull((select SUM(Total_AnioAnt) from  VentasporPais4 Where ParamAnio=@Anio),0)

print 'resumen por region'
select --CoRegion,
	   Region, 
	   SUM(Total) as Total,
	   SUM(Total_AnioAnt) as Total_AnioAnt,
	   --PCT= case when @Total_Region =0 then 0 else cast(round((SUM(Total)*100)/@Total_Region,0) as int) end,
	   --PCT_Ant= case when @Total_Region_Ant =0 then 0 else cast(round((SUM(Total_AnioAnt)*100)/@Total_Region_Ant,0) as int) end,
	   PCT= case when @Total_Region =0 then 0 else cast(round((SUM(Total)*100)/@Total_Region,2) as decimal(5,2)) end,
	   PCT_Ant= case when @Total_Region_Ant =0 then 0 else cast(round((SUM(Total_AnioAnt)*100)/@Total_Region_Ant,2) as decimal(5,2)) end,
	   Edad_Prom_Actual = isnull(AVG(CASE WHEN Edad_Prom_Actual <> 0 THEN Edad_Prom_Actual ELSE NULL END),0) ,
	    Edad_Prom = isnull(AVG(CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0) ,
	    Dias_Actual = isnull(AVG(CASE WHEN Dias_Actual <> 0 THEN Dias_Actual ELSE NULL END),0),
		Dias = isnull(AVG(CASE WHEN Dias <> 0 THEN Dias ELSE NULL END),0),
		Cant_Clientes= SUM(Cant_Clientes),
	    Cant_Clientes_Ant= SUM(Cant_Clientes_Ant),
	   orden=case when Region='Otros' then 1 else 0 end,
	   TicketFile=isnull(sum(TicketFile),0),
	   TicketFileAnioAnter=isnull(sum(TicketFileAnioAnter),0)
from  VentasporPais4
Where ParamAnio=@Anio
group by --coregion,
	   Region
order by orden,Region

print 'top 15 destinos'
 select top 15 p.IDubigeo as CoPais
     ,p.Descripcion 
 ,u.IDubigeo as CoCiudad
     ,u.Descripcion as Ciudad
,COUNT(IDDET) as Cant_Servicios,
     isnull((Select isnull(QtServicios,0) From dbo.RPT_VENTA_DESTINO Where NuAnio=@AnioAnter And       
 --CoUbigeo=p.IDubigeo),0) as Cant_ServiciosAnioAnter,  
 CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter,  
      isnull((Select isnull(QtServicios,0) From dbo.RPT_VENTA_DESTINO Where NuAnio=@AnioAnter-1 And       
 --CoUbigeo=p.IDubigeo),0) as Cant_ServiciosAnioAnter1,       
 CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter1,       
 isnull((Select isnull(QtServicios,0) From RPT_VENTA_DESTINO Where NuAnio=@AnioAnter-2 And       
--CoUbigeo=p.IDubigeo),0) as Cant_ServiciosAnioAnter2,
 CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter2,
 isnull((Select isnull(QtServicios,0) From RPT_VENTA_DESTINO Where NuAnio=@AnioAnter-3 And       
 CoUbigeo=u.IDubigeo),0) as Cant_ServiciosAnioAnter3
     
from cotidet c
left join MAUBIGEO u on c.IDubigeo=u.IDubigeo
left join MAUBIGEO p on u.IDPais=p.IDubigeo
where YEAR(dia)=@Anio
group by p.IDubigeo 
     ,p.Descripcion
     ,u.IDubigeo
     ,u.Descripcion 
     order by Cant_Servicios desc


 select copais,nopais, 
 EneVentaMes=sum(isnull(EneVentaMes,0)),
 FebVentaMes=sum(isnull(FebVentaMes,0)),
 MarVentaMes=sum(isnull(MarVentaMes,0)),
 AbrVentaMes=sum(isnull(AbrVentaMes,0)),
 MayVentaMes=sum(isnull(MayVentaMes,0)),
 JunVentaMes=sum(isnull(JunVentaMes,0)),
 JulVentaMes=sum(isnull(JulVentaMes,0)),
 AgoVentaMes=sum(isnull(AgoVentaMes,0)),
 SetVentaMes=sum(isnull(SetVentaMes,0)),
 OctVentaMes=sum(isnull(OctVentaMes,0)),
 NovVentaMes=sum(isnull(NovVentaMes,0)),
 DicVentaMes=sum(isnull(DicVentaMes,0)),
 Total=sum(isnull(Total,0))
  from  VentasporClienteMes
 Where ParamAnio=@Anio
 group by copais,nopais
 order by nopais 

select copais,
 nopais=(select Descripcion from MAUBIGEO where IDubigeo=copais), 
 EneVentaMes=isnull(EneVentaMes,0),
 FebVentaMes=isnull(FebVentaMes,0),
 MarVentaMes=isnull(MarVentaMes,0),
 AbrVentaMes=isnull(AbrVentaMes,0),
 MayVentaMes=isnull(MayVentaMes,0),
 JunVentaMes=isnull(JunVentaMes,0),
 JulVentaMes=isnull(JulVentaMes,0),
 AgoVentaMes=isnull(AgoVentaMes,0),
 SetVentaMes=isnull(SetVentaMes,0),
 OctVentaMes=isnull(OctVentaMes,0),
 NovVentaMes=isnull(NovVentaMes,0),
 DicVentaMes=isnull(DicVentaMes,0),
 Total=isnull(Total,0)
 from RPT_VENTA_PAIS
 where NuAnio=@AnioAnter 
 and CoPais in (select distinct copais COLLATE SQL_Latin1_General_CP1_CI_AS from  VentasporClienteMes Where ParamAnio=@Anio)
 union 
 select copais COLLATE SQL_Latin1_General_CP1_CI_AS,
 nopais COLLATE SQL_Latin1_General_CP1_CI_AS, 
 EneVentaMes=0,
 FebVentaMes=0,
 MarVentaMes=0,
 AbrVentaMes=0,
 MayVentaMes=0,
 JunVentaMes=0,
 JulVentaMes=0,
 AgoVentaMes=0,
 SetVentaMes=0,
 OctVentaMes=0,
 NovVentaMes=0,
 DicVentaMes=0,
 Total=0
 from  VentasporClienteMes
 where copais not in (select distinct copais COLLATE SQL_Latin1_General_CP1_CI_AS from RPT_VENTA_PAIS where NuAnio=@AnioAnter) And ParamAnio=@Anio
 order by nopais 

 
 select [FecOutPeru]
      ,[ImporteUSD]
      ,[ImporteUSDAnioAnter]
      ,[VarImporteUSD]
      ,[CantPax]
      ,[CantPaxAnioAnter]
      ,[VarCantPax]
      ,[CantFiles]
      ,[CantFilesAnioAnter]
      ,[VarCantFiles]
      ,[SumaProy]
      ,[Dias_Dif]
      ,[Edad_Prom]
 from VentasReceptivoMensual_Sin_Porc  
 Where ParamAnio=@Anio
 order by FecOutPeru


 select [FecOutPeru]
      ,[ImporteUSD]
      ,[ImporteUSDAnioAnter]
      ,[VarImporteUSD]
      ,[SumaProy]
 from VentasReceptivoMensual_Sin_APT_SinPorc 
 Where ParamAnio=@Anio
 order by FecOutPeru

 select
	IDFile ,
	DescCliente,
	Ejecutivo ,
	Total,
	Descripcion=isnull(Descripcion,''),
	--Supervisor=isnull(Supervisor,'')
	Supervisor=	
				isnull((select uj.Nombre +' ' + isnull(uj.TxApellidos,'') from MAUSUARIOS u
				inner join MAUSUARIOS uj on u.CoUserJefe=uj.IDUsuario
				where u.IDUsuario=@IDUser),'')
from VentasDebitMemoPendientes

--

print 'WEB' 
  select  [FecOutPeru]
		  ,[ImporteUSD]
		  ,[ImporteUSDAnioAnter]
		  --,[VarImporteUSD]
		  ,VarImporteUSD= case when ImporteUSDAnioAnter=0 then 0 else VarImporteUSD end
		  ,[SumaProy] 
  from  VentasReceptivoMensual_Web Where ParamAnio=@Anio Order by cast(FecOutPeru as int)  
  
   print 'ACUMULADO WEB' 
   select [FecOutPeru]
      ,[ImporteMesUSD]
      ,[ImporteMesUSDAnioAnter] 
   From  VentasReceptivoMensualAcumulado_Web Where ParamAnio=@Anio Order by cast(FecOutPeru as int)      

   select [FecOutPeru]
      ,[ImporteUSD]
      ,[ImporteUSDAnioAnter]
      --,[VarImporteUSD]
	  ,VarImporteUSD= case when ImporteUSDAnioAnter=0 then 0 else VarImporteUSD end
      ,[SumaProy]
	from VentasReceptivoMensual_Web_SinPorc 
	Where ParamAnio=@Anio
	order by FecOutPeru

--REGIONES CLIENTES WEB

declare @Total_Region_Web as decimal(15,2)=isnull((select SUM(Total) from  VentasporPais4_Web Where ParamAnio=@Anio),0)
declare @Total_Region_Web_Ant as decimal(15,2)=isnull((select SUM(Total_AnioAnt) from  VentasporPais4_Web Where ParamAnio=@Anio),0)

print 'resumen por region'
select --CoRegion,
	   Region, 
	   SUM(Total) as Total,
	   SUM(Total_AnioAnt) as Total_AnioAnt,
	   --PCT= case when @Total_Region =0 then 0 else cast(round((SUM(Total)*100)/@Total_Region,0) as int) end,
	   --PCT_Ant= case when @Total_Region_Ant =0 then 0 else cast(round((SUM(Total_AnioAnt)*100)/@Total_Region_Ant,0) as int) end,
	   PCT= case when @Total_Region_Web =0 then 0 else cast(round((SUM(Total)*100)/@Total_Region_Web,2) as decimal(5,2)) end,
	   PCT_Ant= case when @Total_Region_Web_Ant =0 then 0 else cast(round((SUM(Total_AnioAnt)*100)/@Total_Region_Web_Ant,2) as decimal(5,2)) end,
	   Edad_Prom_Actual = isnull(AVG(CASE WHEN Edad_Prom_Actual <> 0 THEN Edad_Prom_Actual ELSE NULL END),0) ,
	    Edad_Prom = isnull(AVG(CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0) ,
	    Dias_Actual = isnull(AVG(CASE WHEN Dias_Actual <> 0 THEN Dias_Actual ELSE NULL END),0),
		Dias = isnull(AVG(CASE WHEN Dias <> 0 THEN Dias ELSE NULL END),0),
		Cant_Clientes= SUM(Cant_Clientes),
	    Cant_Clientes_Ant= SUM(Cant_Clientes_Ant),
	   orden=case when Region='Otros' then 1 else 0 end,
	   TicketFile=isnull(sum(TicketFile),0),
	   TicketFileAnioAnter=isnull(sum(TicketFileAnioAnter),0)
from  VentasporPais4_Web
Where ParamAnio=@Anio
group by --coregion,
	   Region
order by orden,Region
