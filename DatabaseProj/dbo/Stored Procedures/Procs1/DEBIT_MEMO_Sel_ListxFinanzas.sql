﻿--JRF-20130811-Se debe agregar la Columna del País.          
--JRF-20131203-Considerar los tipos 'S' --> 'SELLS', 'C' --> 'COUNTER'      
--JRF-20140123-Agregar filtro por la columna FecVencim    
--JRF-20140311-Agregar nueva columna de CoTipoVenta  
--HLF-20140904-dm.Cliente as DescrCliente,
CREATE Procedure dbo.DEBIT_MEMO_Sel_ListxFinanzas              
@DescCliente varchar(80),                        
@Numero varchar(10),                        
@Referencia varchar(100),                        
@DescEjecutivo varchar(60),                        
@Tipo char(1),                        
@IDEstado char(2),                        
@IDCab int ,              
@IDPais varchar(6),              
@TotalRango1 numeric(10,2),              
@TotalRango2 numeric(10,2),    
@RangoFecVencim1 smalldatetime,    
@RangoFecVencim2 smalldatetime             
As              
Begin              
 Set NoCount On              
  if @TotalRango1=0 and @TotalRango2 > 0              
  Begin              
  set @TotalRango1 = (select MIN(total) from DEBIT_MEMO)              
  End              
              
   if @TotalRango2=0 and @TotalRango1 > 0              
  Begin              
  set @TotalRango2 = (select max(total) from DEBIT_MEMO)              
  End              
                
 select       
 Case dm.IDTipo       
 when 'R' Then 'RESUMEN'       
 when 'I' Then 'INDIVIDUAL'       
 when 'S' Then 'SELLS'      
 when 'C' Then 'COUNTER'      
  End as DescTipo,              
 dm.IDDebitMemo,              
 CONVERT(char(10),dm.Fecha,103) as FechaCobro,              
 CONVERT(char(10),dm.FecVencim,103) as FechaVencimiento,  
 Case c.CoTipoVenta  
 when 'RC' Then 'RECEPTIVO'  
 when 'CU' Then 'COUNTER'  
 when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'  
 when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'  
 End as DescTipoVenta,  
 case dm.IDTipo        
 When 'R' then dm.IDDebitMemo       
 when 'I' then SUBSTRING(dm.IDDebitMemo,1,8)+' - '+ SUBSTRING(dm.IDDebitMemo,9,10)          
 else SUBSTRING(dm.IDDebitMemo,1,3)+' - '+ SUBSTRING(dm.IDDebitMemo,4,10)          
 End as NroFileConCorrelativo,              
 '' as Correlativo,              
 --cl.RazonComercial as DescrCliente,              
 dm.Cliente as DescrCliente,
 ub.Descripcion as Pais,               
 dm.Titulo As Referencia,                        
 dm.Responsable As Ejecutivo,           
 dm.Total,          
 dm.Saldo,                                   
 dm.IDEstado,                      
 dm.FlImprimioDebit,                    
 Isnull(dm.IDCab,0) As IDCAB,                  
 Isnull(dm.IDDebitMemoResumen,'') as IDDebitMemoResumen,                  
 case when dm.IDTipo ='R' then (select top(1) c.IDCliente from DEBIT_MEMO dm2 LEFT join COTICAB c on dm2.IDCab = c.IDCAB                   
  where dm2.IDDebitMemoResumen = dm.IDDebitMemo) else '' end as IDClienteResumen               
 from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab = c.IDCAB                        
  Left Join MACLIENTES cl On dm.IDCliente = cl.IDCliente                        
  Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario                        
  Left Join MAUBIGEO ub on cl.IDCiudad = ub.IDubigeo              
  Where (LTRIM(rtrim(@DescCliente))='' Or cl.RazonComercial like  @DescCliente + '%')                        
  And (ltrim(rtrim(@Numero))='' Or dm.IDDebitMemo like '%' + @Numero + '%')                        
  And (ltrim(rtrim(@Referencia))='' Or dm.Titulo like '%' + @Referencia + '%')                         
  And (ltrim(rtrim(@DescEjecutivo))='' Or u.Nombre like '%' + @DescEjecutivo + '%')                        
  And (ltrim(rtrim(@Tipo))='' Or dm.IDTipo = @Tipo)                        
  And ((ltrim(rtrim(@IDEstado))='' Or dm.IDEstado = @IDEstado Or        
    (@IDEstado='PD' And dm.IDEstado = 'PP')))        
  And (ltrim(rtrim(@IDPais))='' Or cl.IDCiudad = @IDPais)              
  And (dm.IDCab = @IDCab Or @IDCab = 0)                        
  And ((@TotalRango1 = 0 and @TotalRango2 = 0) or (dm.Total >= @TotalRango1 and dm.Total <= @TotalRango2) )              
  And ((Convert(Char(10),@RangoFecVencim1,103)='01/01/1900' and Convert(Char(10),@RangoFecVencim2,103)='01/01/1900')    
  Or  dm.FecVencim between @RangoFecVencim1 and @RangoFecVencim2)    
  Order By dm.IDTipo,dm.FechaIn,SUBSTRING(dm.IDDebitMemo,0,9) ,SUBSTRING(dm.IDDebitMemo,9,9) Asc               
End    