﻿CREATE Procedure COTIVUELOS_InsCopiaIDCAB
@IDCab int,    
@IDCabCopia int,   
@IDDet int,
@IDDetCopia int,
@UserMod char(4)
As
	Set NoCount On
	Insert Into COTIVUELOS
				SELECT 
				   @IDDet
				  ,@IDCab
				  ,[Cotizacion]
				  ,[NombrePax]
				  ,[Ruta]
				  ,[Num_Boleto]
				  ,[RutaD]
				  ,[RutaO]
				  ,[Vuelo]
				  ,[Fec_Emision]
				  ,[Fec_Documento]
				  ,[Fec_Salida]
				  ,[Fec_Retorno]
				  ,[Salida]
				  ,[HrRecojo]
				  ,[PaxC]
				  ,[PCotizado]
				  ,[TCotizado]
				  ,[PaxV]
				  ,[PVolado]
				  ,[TVolado]
				  ,[Status]
				  ,[Comentario]
				  ,[Llegada]
				  ,[Emitido]
				  ,[TipoTransporte]
				  ,[CodReserva]
				  ,[Tarifa]
				  ,[PTA]
				  ,[ImpuestosExt]
				  ,[Penalidad]
				  ,[IGV]
				  ,[Otros]
				  ,[PorcComm]
				  ,[MontoContado]
				  ,[MontoTarjeta]
				  ,[IdTarjCred]
				  ,[NumeroTarjeta]
				  ,[Aprobacion]
				  ,[PorcOver]
				  ,[PorcOverInCom]
				  ,[MontComm]
				  ,[MontCommOver]
				  ,[IGVComm]
				  ,[IGVCommOver]
				  ,[PorcTarifa]
				  ,[NetoPagar]
				  ,[Descuento]
				  ,[IdLineaA]
				  ,[MontoFEE]
				  ,[IGVFEE]
				  ,[TotalFEE]
				  ,@UserMod
				  ,GetDate()
			  FROM [dbo].[COTIVUELOS]
			  where IDCAB = @IDCabCopia  And TipoTransporte <> 'V'
