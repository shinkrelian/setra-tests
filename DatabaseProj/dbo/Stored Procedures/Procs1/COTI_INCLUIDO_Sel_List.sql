﻿--JRF-20140312-Nuevo campo para el Orden
CREATE Procedure [dbo].[COTI_INCLUIDO_Sel_List] -- 59  
@IDCAB int  
as  
  
 Set NoCount On  
   
 select IDIncluido,Isnull(Orden,0) as OrdIncluido
 from COTI_INCLUIDO  
 Where IDCAB=@IDCAB -- And IDIncluido=@IdIncluido  
