﻿--HLF-20120926-Agregando campos cq.IDDET_Q,cq.CostoReal,cq.MargenAplicado, 
	
CREATE Procedure [dbo].[COTIDET_QUIEBRES_SelxIDCab_Q]	
	@IDCab_Q	int
As
	Set Nocount On
	
	Select cq.Total, cd.IDubigeo, cd.IDProveedor, cq.IDDET_Q, cq.CostoReal, cq.IDDet,
	cq.MargenAplicado 
	--cd.IDServicio_Det, cd.IDServicio, sd.Tipo, sd.Anio
	From COTIDET_QUIEBRES cq Left Join COTIDET cd On cq.IDDet=cd.IDDET
	--Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=cd.IDServicio_Det
	Where cq.IDCAB_Q=@IDCab_Q
