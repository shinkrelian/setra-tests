﻿
--HLF-20140227-Where IDEstado<>'AN'      
--JRF-20140310-Se debe considerar la descripción escrito o generada por el DebitMemo    
--HLF-20140423-Nuevo paramtro @FileFacturado, Case When @FileFacturado=1 Then...  
--HLF-20140430-Agregando columna FlPorAjustePrecio
--HLF-20151215-and not dm.IDDebitMemo in (Select IDDebitMemo From VENTA_ADICIONAL_DET ...
--HLF-20160419-CONVERT(char(10), isnull(i.FeAcreditada,''),103) As FeAcreditada, Left Join INGRESO_DEBIT_MEMO idm Left Join INGRESO_FINANZAS i 
CREATE Procedure dbo.DEBIT_MEMO_Sel_ListFacturacionxIDCab            
 @IDCab int,  
 @FileFacturado bit            
As            
 Set NoCount On            
 select ROW_NUMBER() Over (Order By SUBSTRING(dm.IDDebitMemo,9,9)) As Item,            
  SUBSTRING(dm.IDDebitMemo,0,9) As NroFile,              
  SUBSTRING(dm.IDDebitMemo,9,9) As Correlativo,            
  Convert(char(10),dm.Fecha,103) As Fecha,            
  dm.Total,        
  --cl.RazonComercial     
  dm.Cliente As DescCliente,            
  CONVERT(char(10), dm.FecVencim,103) As FecVencim,  
  CONVERT(char(10), isnull(i.FeAcreditada,''),103) As FeAcreditada,  
            
  Isnull((select SUM(idm.SsPago) from INGRESO_DEBIT_MEMO idm where idm.IDDebitMemo = dm.IDDebitMemo),0) as Monto,        
  --isnull(idm.SsPago,0) as Monto,
  Isnull((select SUM(i.SsBanco) from INGRESO_DEBIT_MEMO idm Left Join INGRESO_FINANZAS i On idm.NuIngreso= i.NuIngreso        
		where idm.IDDebitMemo = dm.IDDebitMemo),0) as ComisioBanco,        
  --isnull(i.SsBanco,0) as ComisioBanco, 
  Isnull((select SUM(idm.SsGastosTransferencia) from INGRESO_DEBIT_MEMO idm where idm.IDDebitMemo = dm.IDDebitMemo),0) as GastoTransfer,        
  dm.FlPorAjustePrecio,
  Case When @FileFacturado=1 Then  
  FlFacturado  
  Else  
  1  
  End as ValorDefault        
 from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab = c.IDCAB            
 Left Join MACLIENTES cl On cl.IDCliente = c.IDCliente            
 Left Join INGRESO_DEBIT_MEMO idm On dm.IDDebitMemo =  idm.IDDebitMemo        
 Left Join INGRESO_FINANZAS i On idm.NuIngreso = i.NuIngreso        
 Where dm.IDCab = @IDCab And dm.IDEstado<>'AN'      
	and not dm.IDDebitMemo in (Select IDDebitMemo From VENTA_ADICIONAL_DET vad 
					inner join VENTA_ADICIONAL va on vad.NuVtaAdi=va.NuVtaAdi
					Where va.IDCab=dm.IDCab)  
 Order by Item     

