﻿CREATE procedure COTICAB_QUIEBRES_SelRangos  
@IDCab int  
As  
 Set NoCount On  
 Create Table #tmpRangos(rango1 int,rango2 int null,FilAct bit)  
   
 Declare @Cont tinyint, @Pax int,@ContC int,@ContC2 int,@InsUnaVes bit,@Max int  
 set @Cont = 1 set @ContC = 1 set @ContC2 = 1 set @InsUnaVes = 1  
 Declare @SaltBloq4 bit
   
 Declare @NroReg int  
 set @NroReg = (select COUNT(Pax) from COTICAB_QUIEBRES where IDCAB= @IDCab)  
 set @Max = (select max(Pax) from COTICAB_QUIEBRES where IDCAB = @IDCab)  
   
 delete from #tmpRangos  
 Declare CurRangos Cursor for  
 select Pax from COTICAB_QUIEBRES   
 where IDCAB= @IDCab  
 order by Pax  
 Open CurRangos  
 Fetch Next From CurRangos into @Pax  
  While @@FETCH_STATUS = 0  
   Begin  
     
   if(@NroReg > 4)  
    begin  
     if (@Cont = 1)  
       begin  
      insert into #tmpRangos (rango1,FilAct) values(@Pax,0)  
       End  
    
     if ((@ContC * 4 = @Cont) and  (@ContC = 1))
      begin  
	   update #tmpRangos set rango2 = @Pax,FilAct = 1 where FilAct = 0  
       
       Declare @MaxContinuo tinyint
       set @MaxContinuo = (select top(1) Pax from COTICAB_QUIEBRES Where IDCAB = @IDCab and Pax> @Pax order by Pax Asc)
       insert into #tmpRangos(rango1,rango2,FilAct)  
				values (@MaxContinuo,null,0)  
       set @ContC = @ContC+1  
      End  
     if (@ContC2 * 5 = @Cont)
      begin
         if (@SaltBloq4=1)
          begin
			--update #tmpRangos set rango2 = @Pax , FilAct = 1 where FilAct = 0
			update #tmpRangos set rango2 = (select top(1) Pax from COTICAB_QUIEBRES WHERE IDCAB = @IDCab and Pax<@Pax Order By Pax Desc), FilAct = 1 where FilAct = 0
			insert into #tmpRangos values(@Pax,null,0)
          end
		  
		 set @ContC2 = @ContC2 + 1
		 set @SaltBloq4 = 1
      end
      
    end  
   else  
    begin  
     if @InsUnaVes = 1  
     begin  
     insert into #tmpRangos (rango1,rango2,FilAct)   
      values((select min(Pax) from COTICAB_QUIEBRES where IDCAB = @IDCab),@Max ,3)       
         set @InsUnaVes = 0  
     end  
  
    end  
   Fetch Next From CurRangos Into @Pax  
   set @Cont=@Cont+1     
   End  
 Close CurRangos          
 Deallocate CurRangos   
   
 update #tmpRangos set rango1=rango1,rango2 = @Max ,FilAct = 1 where rango2 is null  
 select * from #tmpRangos where rango1 is not null order by rango1   
 --drop table #tmpRangos  
