﻿
Create Procedure ANTICIPOS_SelExistexIDFileCorr
@NroSerie char(8),
@Correlativo char(2),
@pExiste bit output
As
	Set NoCount On
	if exists(Select IDAnticipo from ANTICIPOS where Numero = @NroSerie And ANTICIPOS.Correlativo = @Correlativo)
		set @pExiste = 1
	else
		set @pExiste = 0
