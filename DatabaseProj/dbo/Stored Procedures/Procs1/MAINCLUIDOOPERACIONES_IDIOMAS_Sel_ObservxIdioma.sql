﻿
Create Procedure dbo.MAINCLUIDOOPERACIONES_IDIOMAS_Sel_ObservxIdioma
@IDIdioma varchar(12),
@FlInterno bit
As
	Set NoCount On
	select oi.NoObserAbrev,idio.NoObservacion
	from MAINCLUIDOOPERACIONES_IDIOMAS idio 
	Left Join MAINCLUIDOOPERACIONES oi ON idio.NuIncluido = oi.NuIncluido
	where IDIdioma = @IDIdioma and oi.FlInterno = @FlInterno
	Order by oi.NuIncluido
