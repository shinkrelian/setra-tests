﻿Create Procedure dbo.MAENTREVISTAPROVEEDOR_Upd
@IDProveedor char(6),
@IDIdioma varchar(12),
@FlConocimiento bit,
@FeEntrevita smalldatetime,
@IDUserEntre char(4),
@TxComentario varchar(max),
@Estado char(1),
@UserMod char(4)
As
Set Nocount On
UPDATE [dbo].[MAENTREVISTAPROVEEDOR]
   SET FeEntrevista = Case When Convert(Char(10),@FeEntrevita,103)='01/01/1900' Then Null Else @FeEntrevita End
	  ,[FlConocimiento] = @FlConocimiento
      ,[IDUserEntre] = Case When Ltrim(Rtrim(@IDUserEntre))='' Then Null Else Ltrim(Rtrim(@IDUserEntre)) End
      ,[TxComentario] = Case When Ltrim(Rtrim(@TxComentario))='' Then Null Else Ltrim(Rtrim(@TxComentario)) End
      ,[Estado] = Case When Ltrim(Rtrim(@Estado))='' then Null Else @Estado End
      ,[UserMod] = @UserMod
      ,[FecMod] = getdate()
 WHERE [IDProveedor] = @IDProveedor
       And [IDIdioma] = @IDIdioma
