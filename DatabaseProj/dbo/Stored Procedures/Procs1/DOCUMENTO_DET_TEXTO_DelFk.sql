﻿
Create Procedure dbo.DOCUMENTO_DET_TEXTO_DelFk
	@NuDocum char(10),    
	@IDTipoDoc char(3)
As
	Set NoCount on
	
	Delete From DOCUMENTO_DET_TEXTO Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc
