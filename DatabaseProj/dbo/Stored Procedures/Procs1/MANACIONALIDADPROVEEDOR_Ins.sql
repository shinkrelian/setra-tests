﻿Create Procedure [Dbo].[MANACIONALIDADPROVEEDOR_Ins]
@IDubigeo Char(6) ,
@IDProveedor char(6) ,
@UserMod char(4) 
As
	Set NoCount On
	Insert Into MANACIONALIDADPROVEEDOR (IDubigeo,IDProveedor,UserMod)
		Values (@IDubigeo,@IDProveedor,@UserMod)
