﻿
CREATE Procedure [dbo].[CORREOFILE_Sel_ListxClienteBE_ExistsOutput]
	@IDCab	int,
	@CoUsrCliDe	char(6),
	@CoArea char(2),
	@pExists bit output
As	
	Set NoCount On
	
	Set @pExists = 0

	If Exists(
		Select 
		c.NuCorreo
		From .BDCORREOSETRA..CORREOFILE c
		Where IDCab=@IDCab	And 
		(CoUsrCliDe=@CoUsrCliDe) And
		CoTipoBandeja='BE'
		And (EXISTS(Select dc1.CoUsrIntPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 Inner Join MAUSUARIOS u1 On dc1.CoUsrIntPara=u1.IDUsuario 
			And u1.IdArea=@CoArea And dc1.NuCorreo=c.NuCorreo)))
			Set @pExists = 1

