﻿Create Procedure dbo.ACOMODO_VEHICULO_EXT_DET_PAX_Del
	@NuGrupo tinyint,
	@IDDet int,
	@NuPax int
As
	Set Nocount On

	Delete From ACOMODO_VEHICULO_EXT_DET_PAX
	Where NuGrupo=@NuGrupo And IDDet=@IDDet and NuPax=@NuPax

