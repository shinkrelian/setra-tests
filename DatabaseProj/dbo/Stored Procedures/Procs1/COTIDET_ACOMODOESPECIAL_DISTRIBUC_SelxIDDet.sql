﻿CREATE PROCEDURE [COTIDET_ACOMODOESPECIAL_DISTRIBUC_SelxIDDet]
	@IDDet INT
AS
BEGIN
-- =====================================================================
-- Author	   : Dony Tinoco
-- Create date : 17.12.2013
-- Description : SP recupera la distribucion de un acomodo especial
-- =====================================================================
	SET NOCOUNT ON
   
	SELECT	IDDet, CoCapacidad, IDPax, IDHabit, UserMod, FecMod 
	FROM	COTIDET_ACOMODOESPECIAL_DISTRIBUC
	WHERE	IDDet = @IDDet
END
