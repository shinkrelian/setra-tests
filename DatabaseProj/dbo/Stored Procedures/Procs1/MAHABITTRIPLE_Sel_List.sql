﻿CREATE Procedure MAHABITTRIPLE_Sel_List
@IdHabitTriple char(3),
@Abreviacion varchar(4),
@Descripcion varchar(50)
as

	Set NoCount On
	Select IdHabitTriple,Abreviacion,Descripcion from MAHABITTRIPLE
	Where (ltrim(rtrim(@IdHabitTriple))='' Or IdHabitTriple = @IdHabitTriple ) And
		  (LTRIM(rtrim(@Abreviacion))='' or Abreviacion like '%'+@Abreviacion+'%') And
		  (LTRIM(rtrim(@Descripcion))='' Or Descripcion like '%'+@Descripcion+'%') And 
		  IdHabitTriple <> '000'
