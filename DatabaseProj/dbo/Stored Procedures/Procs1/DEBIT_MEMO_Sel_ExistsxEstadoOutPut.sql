﻿Create Procedure dbo.DEBIT_MEMO_Sel_ExistsxEstadoOutPut
@IDCab int,
@IDEstado char(2),
@pExiste bit output 
as
Set NoCount On
Set @pExiste = 0
If Exists(select IDDebitMemo from DEBIT_MEMO where IDTipo = 'I' and IDCab =@IDCab and IDEstado = @IDEstado)
	Set @pExiste = 1
