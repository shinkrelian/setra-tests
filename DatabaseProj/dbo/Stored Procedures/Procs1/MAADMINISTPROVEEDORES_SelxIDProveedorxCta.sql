﻿Create Procedure dbo.MAADMINISTPROVEEDORES_SelxIDProveedorxCta
@IDProveedor char(6),
@Cta varchar(30),
@pNombreBanco varchar(80) output
As
Set NoCount On
Set @pNombreBanco = ''
Select @pNombreBanco = IsNull(BancoExtranjero,'')
from MAADMINISTPROVEEDORES
where IDProveedor=@IDProveedor and Ltrim(Rtrim(CtaCte))=Ltrim(Rtrim(@Cta))
