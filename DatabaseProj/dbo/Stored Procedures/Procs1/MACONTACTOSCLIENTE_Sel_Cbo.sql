﻿CREATE Procedure [dbo].[MACONTACTOSCLIENTE_Sel_Cbo]	
	@IDCliente char(6)	
As
	Set NoCount On
	
	Select IDContacto, Nombres+' '+Apellidos as Nombre
	FROM MACONTACTOSCLIENTE 
    WHERE IDCliente=@IDCliente 
    Order by 2
