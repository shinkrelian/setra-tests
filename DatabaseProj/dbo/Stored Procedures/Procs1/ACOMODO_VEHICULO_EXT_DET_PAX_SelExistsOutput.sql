﻿Create Procedure dbo.ACOMODO_VEHICULO_EXT_DET_PAX_SelExistsOutput
	@NuGrupo tinyint,
	@IDDet int,
	@NuPax int,
	@pExists bit Output
As
	Set Nocount on
	Set @pExists = 0

	If Exists(Select NuGrupo From ACOMODO_VEHICULO_EXT_DET_PAX
		Where NuGrupo=@NuGrupo And IDDet=@IDDet and NuPax=@NuPax)
		Set @pExists = 1

