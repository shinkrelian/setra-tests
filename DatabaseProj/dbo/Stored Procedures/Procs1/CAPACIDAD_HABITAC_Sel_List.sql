﻿Create Procedure dbo.CAPACIDAD_HABITAC_Sel_List
@NoCapacidad varchar(50)
As
	Set NoCount On
	Select CoCapacidad,NoCapacidad,QtCapacidad 
	from CAPACIDAD_HABITAC
	where (ltrim(rtrim(@NoCapacidad))='' Or NoCapacidad like '%'+@NoCapacidad+'%')
