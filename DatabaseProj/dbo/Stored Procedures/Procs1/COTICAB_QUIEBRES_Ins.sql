﻿CREATE Procedure [dbo].[COTICAB_QUIEBRES_Ins]		
    @IDCAB int,
    @Pax smallint,
    @Liberados smallint,
    @Tipo_Lib char(1),
    @UserMod	char(4),
    @pIDCab_Q	int Output
As

	Set NoCount On
		
	INSERT INTO COTICAB_QUIEBRES
           (IDCAB
           ,Pax
           ,Liberados
           ,Tipo_Lib
           ,UserMod)
     VALUES
           (
            @IDCAB ,
            @Pax ,
            Case When @Liberados=0 Then Null Else @Liberados End,
            Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End,
            @UserMod
            )
	
	Declare @IDCab_Q int=(Select MAX(IDCAB_Q) From COTICAB_QUIEBRES)
	Set @pIDCab_Q=@IDCab_Q
