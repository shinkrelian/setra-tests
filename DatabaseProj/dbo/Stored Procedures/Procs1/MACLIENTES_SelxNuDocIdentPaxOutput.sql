﻿
CREATE Procedure dbo.MACLIENTES_SelxNuDocIdentPaxOutput
	@CoTipDocIdentPax char(3),
	@NuDocIdentPax varchar(15),
	@pCoCliente char(6) output
As
	Set Nocount on

	Set @pCoCliente=''

	Select @pCoCliente=IDCliente From MACLIENTES 
	Where isnull(CoTipDocIdentPax,IDIdentidad)=@CoTipDocIdentPax 
		And isnull(NuDocIdentPax,NumIdentidad)=@NuDocIdentPax

