﻿
--BN

Create Procedure ANTICIPOS_Sel_List
@NombreCliente varchar(100),
@NumeroDoc varchar(8),
@NroFile varchar(8),
@IDSerie char(3),
@IDBanco char(3),
@FechaDoc1 smalldatetime,
@FechaDoc2 smalldatetime,
@FechaOUT1 smalldatetime,
@FechaOUT2 smalldatetime
As
	Set NoCount On
	select IDAnticipo,u.Nombre As EjecutivoResp,Convert(char(10),At.Fecha,103) As Fecha,
	c.Cotizacion,At.IDFile,cl.RazonComercial As Cliente
	from ANTICIPOS At Left Join COTICAB c On At.IDCab = c.IDCAB
	Left Join MACLIENTES cl On At.IDCliente = cl.IDCliente
	Left Join MAUSUARIOS u On At.IDUsuarioResponsable = u.IDUsuario	
	Where (LTRIM(rtrim(@NombreCliente))='' Or cl.RazonComercial like '%' + @NombreCliente + '%')
	And (LTRIM(rtrim(@NumeroDoc))='' Or At.Numero like '%' + @NumeroDoc + '%')
	And (LTRIM(rtrim(@NroFile))='' Or At.IDFile like '%' + @NroFile + '%')
	And (LTRIM(rtrim(@IDSerie))='' Or At.IdSerie = @IDSerie)
	And (LTRIM(rtrim(@IDBanco))='000' Or At.IDbanco = @IDBanco)
	And ((ltrim(rtrim(CONVERT(char(10),@FechaDoc1,103)))='01/01/1900' And ltrim(rtrim(CONVERT(char(10),@FechaDoc2,103)))='01/01/1900') Or
		  At.Fecha Between @FechaDoc1 and @FechaDoc2)
	And ((ltrim(rtrim(CONVERT(char(10),@FechaOUT1,103)))='01/01/1900' And ltrim(rtrim(CONVERT(char(10),@FechaOUT2,103)))='01/01/1900') Or
		  At.FechaOut Between @FechaOUT1 and @FechaOUT2)
