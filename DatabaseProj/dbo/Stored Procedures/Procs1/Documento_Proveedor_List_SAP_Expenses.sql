﻿--JHD-20150813-CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),'00000000'),
--JHD-20150817-TaxCode='EXE_IGV',
--JHD-20150818-Se agrego union para los otros gastos adicionales
--FK-20151006 - TaxCode='OTROS' PERCXP
CREATE PROCEDURE [dbo].[Documento_Proveedor_List_SAP_Expenses]
@nudocumprov int
as
begin
select
ExpenseCode= case when (d.SSPercepcion is not null and d.SSPercepcion>0) then 1 else 0 end,
LineTotal=d.SSPercepcion,
Remarks='',
DistributionMethod=0,
TaxCode='PERCXP',--'EXE_IGV',--'',
DistributionRule=d.cocecos,
DistributionRule2=d.cocecon,
DistributionRule3=isnull((select idfile from coticab where idcab=d.idcab),'00000000')
from DOCUMENTO_PROVEEDOR d
where d.nudocumprov=@nudocumprov
and (d.SSPercepcion is not null and d.SSPercepcion>0)

union 

select
ExpenseCode= isnull((select cosap from MAGASTOSADICIONALES where CoGasto=d.cogasto),0),
LineTotal=d.SsOtrCargos,
Remarks='',
DistributionMethod=0,
TaxCode='OTROS',--'EXE_IGV',--'',
DistributionRule=d.cocecos,
DistributionRule2=d.cocecon,
DistributionRule3=isnull((select idfile from coticab where idcab=d.idcab),'00000000')
from DOCUMENTO_PROVEEDOR d
where d.nudocumprov=@nudocumprov
and ((d.SsOtrCargos is not null and d.SsOtrCargos>0) and (d.cogasto is not null))

end;
