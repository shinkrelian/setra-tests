﻿Create Procedure dbo.MACUESTIONARIO_Upd
@NuCuest tinyint,
@IDTipoProv char(3),
@NoDescripcion varchar(Max),
@UserMod char(4)
as
Set NoCount On
	Update MACUESTIONARIO
		Set IDTipoProv=Case When Ltrim(Rtrim(@IDTipoProv))='' Then Null Else @IDTipoProv End,
			NoDescripcion=@NoDescripcion,
			UserMod=@UserMod,
			FecMod=Getdate()
	Where NuCuest=@NuCuest
