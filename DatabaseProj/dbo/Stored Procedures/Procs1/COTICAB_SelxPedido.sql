﻿--JRF-20160411-Cambiar columna Ruta {IsNull(NoRuta,'') as Ruta,}
CREATE Procedure dbo.COTICAB_SelxPedido
	@NuPedInt int
As
	Set Nocount on

	Select u.Nombre+isnull(' '+u.TxApellidos,'') as EjecutivoVtas, cc.Fecha, cc.FecInicio, 
	cast(cc.NroPax as nvarchar(5))+isnull('+'+cast(cc.NroLiberados as nvarchar(3)),'') as Pax,
	cc.Cotizacion, isnull(cc.IDFile,'') as NuFile, cc.IDCAB, cc.Titulo, Cast((DATEDIFF(d,cc.FecInicio,cc.FechaOut)+1) as varchar(2))+' '+ 
		--dbo.FnDestinosDia(cc.IDCAB,0) as Ruta,
		IsNull(NoRuta,'') as Ruta,
		cc.Estado as CoEstado,
		Case cc.Estado
			When 'P' then 'PENDIENTE'
			When 'A' then 'ACEPTADO'
			When 'N' then 'NO ACEPTADO'
			When 'X' then 'ANULADO'
			When 'E' then 'ELIMINADO'
			When 'C' then 'CERRADO'
		End as DescEstado
	From COTICAB cc Left Join MAUSUARIOS u On cc.IDUsuario=u.IDUsuario
	Where cc.NuPedInt=@NuPedInt
	Order by cc.IDCAB
