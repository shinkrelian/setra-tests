﻿Create Procedure dbo.MAFORMASPAGO_SelCbo
@Tipo char(3),
@Todos bit
As
	Set NoCount On
	Select * from (
	select ''As IDBIdanco,'<TODOS>'As Descripcion,0 As Ord  
	Union
	select '' as Id,'---' as Descripcion,1 as Ord where @Todos = 0
	Union
	select IdFor as Id,Descripcion,2 as Ord from MAFORMASPAGO 
	where Tipo = @Tipo)
	as X
	Where (@Todos=1 Or Ord <> 0)  
