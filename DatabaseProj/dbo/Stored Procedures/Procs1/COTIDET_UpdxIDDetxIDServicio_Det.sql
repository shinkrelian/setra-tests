﻿Create Procedure dbo.COTIDET_UpdxIDDetxIDServicio_Det
@Dia smalldatetime,
@IDDet int,
@IDServicio_Det int	
As
Set NoCount On
Set Language Spanish
Update COTIDET
Set Dia=@Dia,
	DiaNombre=DATENAME(Dw,@Dia)
Where IDDET=@IDDet And IDServicio_Det=@IDServicio_Det
