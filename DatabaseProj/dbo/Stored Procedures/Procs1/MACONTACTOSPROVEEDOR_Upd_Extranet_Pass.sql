﻿create Procedure [dbo].[MACONTACTOSPROVEEDOR_Upd_Extranet_Pass]  

 @PasswordLogeo varchar(max)='',  
 @UsuarioLogeo_Hash varchar(max),
-- @FechaPassword datetime,   
 @UserMod char(4)
AS
BEGIN

declare  @IDContacto char(3),  
		 @IDProveedor char(6),
		 @FechaHash datetime

		 select @IDContacto=idcontacto,@IDProveedor=IDProveedor,@FechaHash=FechaHash from MACONTACTOSPROVEEDOR where UsuarioLogeo_Hash=@UsuarioLogeo_Hash
		 	
 if rtrim(ltrim(@IDContacto))<>''
	begin
			if getdate()<=@FechaHash
				begin
					UPDATE MACONTACTOSPROVEEDOR Set  
				   Password=Case When ltrim(rtrim(@PasswordLogeo))='' Then Null Else @PasswordLogeo End  
				   --,UsuarioLogeo_Hash=Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
				   --,FechaHash=DATEADD(day,14, getdate())
				   ,UserMod=@UserMod             
				   ,FecMod=GETDATE()
					 WHERE  
				   IDContacto=@IDContacto And IDProveedor=@IDProveedor  
						end

			else
				begin
					RAISERROR ('La fecha límite expiró', 16, 1);
					RETURN
				end
		
	end
else
	begin
			RAISERROR ('Token no se encuentra registrado', 16, 1);
			RETURN
	end
END;
