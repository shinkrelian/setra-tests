﻿
Create Procedure dbo.MAINCLUIDOOPERACIONES_IDIOMAS_SelOutput
@NuIncluido int,
@IDIdioma varchar(12),
@pExiste bit output
As
	Set NoCount On
	set @pExiste = 0
	If Exists(select IDIdioma from MAINCLUIDOOPERACIONES_IDIOMAS where NuIncluido = @NuIncluido and IDIdioma = @IDIdioma)
		set @pExiste = 1
