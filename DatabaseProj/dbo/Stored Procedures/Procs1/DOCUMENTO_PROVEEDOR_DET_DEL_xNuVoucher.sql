﻿
---------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_DET_DEL_xNuVoucher]
	@NuVoucher int
AS
BEGIN
	Delete [dbo].[DOCUMENTO_PROVEEDOR_DET]
	Where 
		[NuDocumProv] in (select distinct [NuDocumProv] from DOCUMENTO_PROVEEDOR where NuVoucher=@NuVoucher)
END;
