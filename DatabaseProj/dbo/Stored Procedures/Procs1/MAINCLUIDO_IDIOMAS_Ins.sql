﻿CREATE PROCEDURE dbo.MAINCLUIDO_IDIOMAS_Ins
	@IDIncluido	int,
	@IDIdioma	varchar(12),
	@Descripcion	varchar(MAX),
	@UserMod	char(4)
As
	Set NoCount On
	
	Insert Into MAINCLUIDO_IDIOMAS(IDIncluido,IDIdioma,Descripcion,UserMod)
	Values(@IDIncluido,@IDIdioma,@Descripcion,@UserMod)

