﻿
--HLF-20141209-If @CoSerie = '006' And @IDTipoDoc = 'BLE'...
CREATE Procedure dbo.Correlativo_SelTextoImpresion  
	@IDTipoDoc char(3),  
	@CoSerie char(3),  
	@IDCab int,
	@pTxTextoImpresion varchar(80) output  
As  
 Set Nocount on  
   
 Set @pTxTextoImpresion=''  
   
 Select @pTxTextoImpresion=isnull(TxTextoImpresion,'')  
 From Correlativo  
 Where IDTipoDoc=@IDTipoDoc and CoSerie=@CoSerie  

 If @CoSerie = '006' And @IDTipoDoc = 'BLE'
	Begin
	Declare @DescPais varchar(60),@DescCiudad varchar(60),@DescPaises varchar(250)=''
	Declare curPaises cursor for 
	Select distinct up.Descripcion as DescPais, u.Descripcion as DescCiudad
	From COTIDET cd 
	Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=cd.IDServicio_Det
		And cd.IDCAB=@IDCab
	Inner Join MASERVICIOS s On s.IDServicio=sd.IDServicio 
	Inner Join MAUBIGEO u On s.IDubigeo=u.IDubigeo And U.IDPais<>'000323'
	Left Join MAUBIGEO up On u.IDPais=up.IDubigeo 
	Order by 1,2
	
	
	Open curPaises
	Fetch Next From curPaises Into @DescPais, @DescCiudad
	While @@FETCH_STATUS=0
		Begin
		Set @DescPaises+=@DescCiudad + '-' + @DescPais+', '
		Fetch Next From curPaises Into @DescPais, @DescCiudad
		End
	Close curPaises
	Deallocate curPaises
	
	If @DescPaises<>''
		Set @DescPaises=Left(@DescPaises,LEN(@DescPaises)-1)
	
	Set @pTxTextoImpresion=@pTxTextoImpresion+@DescPaises
	
	End
