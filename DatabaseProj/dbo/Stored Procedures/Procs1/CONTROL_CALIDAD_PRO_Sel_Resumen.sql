﻿CREATE Procedure dbo.CONTROL_CALIDAD_PRO_Sel_Resumen
@IDProveedor char(6)
As
Set NoCount On
Declare @IDTipoProv char(3)='',@RoomNight smallint = 0,@CantServicios smallint=0
Select @IDTipoProv=IDTipoProv
from MAPROVEEDORES where IDProveedor=@IDProveedor

--Obtener RoomNights
--Select @RoomNight=count(distinct dia)
--from COTIDET cd Left Join COTICAB c On cd.IDCAB=c.IDCAB
--Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
--Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
--Where ((p.IDTipoProv='001' and sd.PlanAlimenticio=0) Or(p.IDTipoProv<>'001' and sd.ConAlojamiento=1)) 
--And cd.IDProveedor=@IDProveedor And c.Estado='A'

--Select @RoomNight=Count(distinct dia)
--from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc Inner Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad
--Inner Join COTIDET cd On cd.IDCab=ccp.IDCab And cd.IDProveedor=ccpc.IDProveedor
--Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
--Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
--Where ccpc.IDProveedor=@IDProveedor --And rd.Anulado=0
--And ((p.IDTipoProv='001' and sd.PlanAlimenticio=0) Or(p.IDTipoProv<>'001' and sd.ConAlojamiento=1))

Select @RoomNight=IsNull(sum(Y.CantidadxFile),0)
from (
Select 
	X.CantidadxFile
	from (
		Select Count(distinct cd.Dia) as CantidadxFile,c.IDFile
		from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc 
		Inner Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad 
		And ccpc.NuCuest=5 And (ccpc.IDProveedor=@IDProveedor And @IDProveedor <> '001836')
		Inner Join COTIDET cd On cd.IDCab=ccp.IDCab And cd.IDProveedor=ccpc.IDProveedor
		Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
		Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
		Left Join COTICAB c On cd.IDCAB=c.IDCAB
		Where --ccpc.IDProveedor='000336' --And rd.Anulado=0
		p.IDTipoOper='E' And
		Exists(Select NuControlCalidad from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc2 
			   Where ccpc2.NuControlCalidad=ccpc.NuControlCalidad and ccpc2.NuCuest=ccpc2.NuCuest And ccpc2.IDProveedor=ccpc.IDProveedor
			   And (ccpc2.FlExcelent=1 Or ccpc2.FlVeryGood=1 Or ccpc2.FlGood=1 Or ccpc2.FlAverage=1 Or ccpc2.FlPoor=1))  And
		((p.IDTipoProv='001' and IsNull(sd.PlanAlimenticio,0)=0) Or(p.IDTipoProv<>'001' and sd.ConAlojamiento=1))
		Group By c.IDFile
		) as X
	) 
as Y


Select @CantServicios=Count(*)
from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc Inner Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad
Inner Join RESERVAS r On r.IDCab=ccp.IDCab And r.IDProveedor=ccpc.IDProveedor
Where ccpc.IDProveedor=@IDProveedor And r.Estado<>'XL' And r.Anulado=0 --And rd.Anulado=0
And (FlExcelent=1 Or FlVeryGood=1 Or FlGood=1 Or FlAverage=1 Or FlPoor=1)

Select *--X.SumaAverage+X.SumaExcelente+X.SumaGood+X.SumaPoor+X.SumaVeryGood As CantFiles
from (
	Select Sum(Cast(FlExcelent as tinyint)) as SumaExcelente,
		   Sum(Cast(FlVeryGood as tinyint)) as SumaVeryGood,
		   Sum(Cast(FlGood as tinyint)) as SumaGood,
		   Sum(Cast(FlAverage as tinyint)) as SumaAverage,
		   Sum(Cast(FlPoor as tinyint)) as SumaPoor,
		   Count(distinct ccp.IDCab) as CantFiles,
		   @RoomNight as RoomNights,
		   @CantServicios as CantidadServicios
	from (Select * from CONTROL_CALIDAD_PRO_MACUESTIONARIO where IDProveedor=@IDProveedor And
		  (FlExcelent=1 Or FlVeryGood=1 Or FlGood=1 Or FlAverage=1 Or FlPoor=1)) ccpc
	Left Join MAPROVEEDORES p On ccpc.IDProveedor=p.IDProveedor
	Left Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad
) as X
