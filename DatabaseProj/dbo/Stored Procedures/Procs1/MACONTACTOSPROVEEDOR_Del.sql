﻿Create Procedure [dbo].[MACONTACTOSPROVEEDOR_Del]
	@IDContacto char(3),
	@IDProveedor char(6)
As
	Set NoCount On
	
	Delete From MACONTACTOSPROVEEDOR 
    WHERE
           IDContacto=@IDContacto And IDProveedor=@IDProveedor
