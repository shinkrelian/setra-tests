﻿--JRF-20150419-Agregar Columna PaxHastaCostoReal
--JRF-20151130-Agregar Columna EsCostoTC
CREATE Procedure dbo.COTIDET_SelxIDDetxNroPax
@IDDet int,
@NroPax tinyint,
@IDMoneda char(3)
As
select  cds.IDServicio_Det,sd.CoMoneda,IsNull(sd.Monto_sgl,0) as MontoReal,IsNull(sd.Monto_sgls,0) as MontoSegunTC,
		IsNull((select sdc.Monto from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det 
		 and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) as MontoCostoReal,
		 IsNull((select sdc.PaxHasta from MASERVICIOS_DET_COSTOS sdc where sdc.IDServicio_Det=cds.IDServicio_Det 
		 and @NroPax between sdc.PaxDesde and sdc.PaxHasta),0) as PaxHastaCostoReal,
		 Isnull(IsNull(sd.TC,(select tc.SsTipCam from MATIPOSCAMBIO_USD tc where CoMoneda=@IDMoneda)),0) as TC,
		 sd.Afecto,sd.PoliticaLiberado,IsNull(sd.Liberado,0) CantMinLiberada,IsNuLL(sd.TipoLib,'') As TipoLib,
		 IsNull(sd.MaximoLiberado,0) as CantidadLiberada,IsNull(sd.MontoL,0) as MontoLiberado,
		 IsNull(sd.DiferSS,0) as DiferSS,IsNull(sd.DiferST,0) as DiferST,Cast(0 as bit) as EsCostoTC
from COTIDET cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det
where IDDET=@IDDet and CostoReal >0
