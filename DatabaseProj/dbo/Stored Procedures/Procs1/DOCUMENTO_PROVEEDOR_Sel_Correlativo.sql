﻿
----------------------------------------------------------------------------------


create PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_Correlativo]
	
AS
BEGIN
	declare @NuDocumProv int
	set @NuDocumProv=isnull((select MAX(NuDocumProv) from DOCUMENTO_PROVEEDOR),0)+1
	select @NuDocumProv
END;
