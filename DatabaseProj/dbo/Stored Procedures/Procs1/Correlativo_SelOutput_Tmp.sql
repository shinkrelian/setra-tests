﻿   
--CREATE
CREATE Procedure [dbo].[Correlativo_SelOutput_Tmp]        
	@Tabla varchar(30),        
	@Upd Bit,        
	@Tamanio TinyInt,    
	@pCorrelativo varchar(10) output,    
	@IDTipoDoc char(3)='',    
	@CoSerie char(3)='',
	@Aumento tinyint=0
   
As        
	Set NoCount On        
	Declare @iCorrelativo int, @vcCorrelativo varchar(10)        
  
	If ltrim(rtrim(@IDTipoDoc))=''   
		Begin    
		Select @iCorrelativo=Correlativo+@Aumento       
		From Correlativo Where Tabla=@Tabla        

		Set @vcCorrelativo=isnull(@iCorrelativo,0)
		if @vcCorrelativo<>'0'
			Set @vcCorrelativo=REPLICATE('0',@Tamanio-LEN(@vcCorrelativo))+@vcCorrelativo        
		else
			Set @vcCorrelativo=''	

		If @Upd=1        
			Update Correlativo Set Correlativo=@iCorrelativo,        
			FecMod=GETDATE()        
			Where Tabla=@Tabla        
		  
		Set @pCorrelativo=@vcCorrelativo      
		End    
	Else     
		Begin    
		Select @iCorrelativo=Correlativo+@Aumento       
		From Correlativo Where Tabla=@Tabla And CoSerie=@CoSerie And (IDTipoDoc=@IDTipoDoc Or IDTipoDoc2=@IDTipoDoc)   

		Set @vcCorrelativo=isnull(@iCorrelativo,0)    
		if @vcCorrelativo<>'0'    
			Set @vcCorrelativo=@CoSerie+REPLICATE('0',(@Tamanio-3)-LEN(@vcCorrelativo))+@vcCorrelativo        
		else
			Set @vcCorrelativo=''	

		If @Upd=1        
			Update Correlativo Set Correlativo=@iCorrelativo,        
			FecMod=GETDATE()        
			Where Tabla=@Tabla And CoSerie=@CoSerie And (IDTipoDoc=@IDTipoDoc Or IDTipoDoc2=@IDTipoDoc)   
			  
		Set @pCorrelativo=@vcCorrelativo      
		End    