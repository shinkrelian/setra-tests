﻿CREATE Procedure [dbo].[COTIVUELOS_Sel_RptxBoletosCorrelativos]   
@FecInicial smalldatetime,  
@FecFinal smalldatetime,  
@IDProveedor char(6)  
as  
    --sp_Help COTIVUELOS  
 Set NoCount On  
  
 select ID  
     ,p.NombreCorto  
     ,Num_Boleto  
     ,Tarifa  
     ,Otros,PTA  
     ,IGV  
     ,ImpuestosExt  
     ,NetoPagar  
     ,PorcComm,MontComm,IGVComm --Comisión  
     ,MontoTarjeta --  
     ,PorcOver,MontCommOver,IGVCommOver -- Cambiar Over x Servicios   
     --TotalPagar = Total - MontoTarjeta - Comisión - Servicios  
     ,TotalPagar = NetoPagar - MontoTarjeta - (PorcComm + MontComm +IGVComm) - (PorcOver + MontCommOver + IGVCommOver)  
     ,IdFile = (select IDFile from COTICAB where IDCAB=cv.IDCAB) -- File  
     ,NombrePax  
     ,Ruta  
  from COTIVUELOS cv Left Join MAPROVEEDORES p On cv.IdLineaA= p.IDProveedor  
  where Fec_Emision between @FecInicial  and @FecFinal  
   And (ltrim(rtrim(@IDProveedor))='' or IdLineaA = @IDProveedor)  
  Order By p.NombreCorto
