﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*

--DOCUMENTO_PROVEEDOR_Sel_Cabecera_OrdenServicio '14010143000467'
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--JRF-20141111-Agregar el IDTipoProv
--JRF-20141112-Agregar el SsMontoTotal
--JHD-20141201-Agregar el RutaDocSustento
create Procedure dbo.DOCUMENTO_PROVEEDOR_Sel_Cabecera_OrdenServicio  
@NuVoucher char(14)=''  
As  
Set NoCount On  
Select c.IDFile,
SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,    
 os.IDProveedor,
 p.NombreCorto as DescProveedor,
 cl.RazonComercial as Cliente,
 c.NroPax as Pax,
 Isnull(c.NroLiberados,0) as Liberados,    
TipoPax=case when c.TipoPax='EXT' then 'EXTRANJERO' when c.TipoPax='NCR' then 'NACIONAL CON RUC' when TipoPax='NSR' then 'NACIONAL SIN RUC' else '' end,    
Tipo_lib=case when c.Tipo_Lib='S' then 'SIMPLE' when c.Tipo_Lib='D' then 'DOBLE' else 'NINGUNO' end,    
c.Titulo,
uVent.Nombre as UsuarioVen,
uOper.Nombre as UsuarioOpe,
c.ObservacionVentas,
c.ObservacionGeneral,
c.FecInicio as FechaInicio,
c.FechaOut as FechaFin,  
os.NuOrden_Servicio as NuOrdenServicio,  
os.CoEstado,
Isnull(os.SsSaldo,0) as SaldoPendiente,
ISNULL(os.SsDifAceptada,0) as SsDifAceptada,  
IsNull(os.TxObsDocumento,'') as TxObsDocumento ,
p.IDTipoProv,
os.SsMontoTotal
--,'' RutaDocSustento
,RutaDocSustento=isnull((select top 1 RutaDocSustento from COTIDET  where IDServicio_Det in (select IDServicio_Det from ORDEN_SERVICIO_DET where NuOrden_Servicio=os.NuOrden_Servicio)   order by RutaDocSustento desc),'')
from ORDEN_SERVICIO os Left Join COTICAB c On os.IDCab=c.IDCAB  
Left Join MAPROVEEDORES p On os.IDProveedor=p.IDProveedor  
Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente  
Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario  
Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario  
where CoOrden_Servicio=@NuVoucher  


go
*/
----------------------------------------------------------------------------------------------------------
--DOCUMENTO_PROVEEDOR_Sel_Cabecera_OrdenPago '918'
--create
CREATE Procedure dbo.DOCUMENTO_PROVEEDOR_Sel_Cabecera_OrdenPago  
@NuVoucher char(14)=''  
As  
Set NoCount On  
Select c.IDFile,
--SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,    

'OP-'+(CAST(os.IDOrdPag as varchar(5)))  as IDVoucher,

 r.IDProveedor,
 p.NombreCorto as DescProveedor,
 cl.RazonComercial as Cliente,
 c.NroPax as Pax,
 Isnull(c.NroLiberados,0) as Liberados,    
TipoPax=case when c.TipoPax='EXT' then 'EXTRANJERO' when c.TipoPax='NCR' then 'NACIONAL CON RUC' when TipoPax='NSR' then 'NACIONAL SIN RUC' else '' end,    
Tipo_lib=case when c.Tipo_Lib='S' then 'SIMPLE' when c.Tipo_Lib='D' then 'DOBLE' else 'NINGUNO' end,    
c.Titulo,
uVent.Nombre as UsuarioVen,
uOper.Nombre as UsuarioOpe,
c.ObservacionVentas,
c.ObservacionGeneral,
c.FecInicio as FechaInicio,
c.FechaOut as FechaFin,  
os.IDOrdPag as NuOrdenServicio,  
os.CoEstado,
Isnull(os.SsSaldo,0) as SaldoPendiente,
ISNULL(os.SsDifAceptada,0) as SsDifAceptada,  
IsNull(os.TxObsDocumento,'') as TxObsDocumento ,
p.IDTipoProv,
os.SsMontoTotal
--,'' RutaDocSustento
,RutaDocSustento=''--isnull((select top 1 RutaDocSustento from COTIDET  where IDServicio_Det in (select IDServicio_Det from ORDEN_SERVICIO_DET where NuOrden_Servicio=os.NuOrden_Servicio)   order by RutaDocSustento desc),'')
from ORDENPAGO os Left Join COTICAB c On os.IDCab=c.IDCAB  
Left Join RESERVAS R On os.IDReserva=r.IDReserva  
Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor  
Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente  
Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario  
Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario  
where rtrim(ltrim(cast(IDOrdPag as varchar(5))))=rtrim(ltrim(@NuVoucher))

