﻿CREATE PROCEDURE [dbo].[MANIVELOPC_InsxNivel]
	@IDNivel	char(3),
	@IDUsuario	char(4)
As
	Set NoCount On
	
	Insert Into MANIVELOPC(IDOpc,IDNivel,Consultar, Grabar, Actualizar, Eliminar, UserMod)
	Select IDOPc,@IDNivel,0,0,0,0,@IDUsuario  From MAOPCIONES
