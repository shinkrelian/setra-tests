﻿
Create Procedure [dbo].[COTITRANSP_PAX_DelxIDTransporte]
	@IDTransporte	int
	
As
	Set Nocount on

	Delete From COTITRANSP_PAX 
	Where IDTransporte=@IDTransporte
