﻿Create Procedure dbo.CORREOFILE_ExistsxIDCabxConvertionIndex
@IDCab int,
@ConvertionIndex varchar(Max),
@pExisteMail bit output
As 
Set NoCount On
Set @pExisteMail = 0
Select @pExisteMail = 1
from BDCORREOSETRA.DBO.CORREOFILE
Where IDCab=@IDCab And CoCorreoOutlook = @ConvertionIndex
