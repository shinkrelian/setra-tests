﻿
CREATE PROCEDURE [dbo].[DOCUMENTO_FORMA_EGRESO_InsertIDOutput]
	@TipoFormaEgreso	char(3),
	@CoMoneda			char(3),
	@CoEstado			char(2),
	@SsMontoTotal		numeric(14,4),
	@SsSaldo			numeric(14,4),
	@SsDifAceptada		numeric(14,4),
	@UserMod			char(4),
	@TxObsDocumento		varchar(max),
	@CoUbigeo_Oficina	char(6),
	@IDCab				int,
	@pIDDocFormaEgresoInt int output	
AS
BEGIN
	Set Nocount on
	Set @pIDDocFormaEgresoInt = 0
	SELECT @pIDDocFormaEgresoInt = DF.IDDocFormaEgreso FROM DOCUMENTO_FORMA_EGRESO AS DF INNER JOIN DOCUMENTO_PROVEEDOR AS DP
			  ON DF.IDDocFormaEgreso = DP.IDDocFormaEgreso AND DF.TipoFormaEgreso = DP.CoObligacionPago AND DF.CoMoneda = DP.CoMoneda_FEgreso
			  WHERE (DF.TipoFormaEgreso = @TipoFormaEgreso) AND (DF.CoMoneda = @CoMoneda) AND (DP.IDCab = @IDCab)
	SELECT @pIDDocFormaEgresoInt = ISNULL(@pIDDocFormaEgresoInt,0)
	IF @pIDDocFormaEgresoInt = 0
	BEGIN
		SELECT @pIDDocFormaEgresoInt = ISNULL(MAX(IDDocFormaEgreso),0) + 1 FROM DOCUMENTO_FORMA_EGRESO Where TipoFormaEgreso = @TipoFormaEgreso-- AND CoMoneda = @CoMoneda
		INSERT INTO DOCUMENTO_FORMA_EGRESO(IDDocFormaEgreso, TipoFormaEgreso, CoMoneda, CoEstado, SsMontoTotal, SsSaldo,
										   SsDifAceptada, UserMod, FecMod, TxObsDocumento, FeCreacion, CoUbigeo_Oficina)
		VALUES(@pIDDocFormaEgresoInt, @TipoFormaEgreso, @CoMoneda, @CoEstado, @SsMontoTotal, @SsSaldo,
			   @SsDifAceptada, @UserMod, GETDATE(), @TxObsDocumento, GETDATE(), @CoUbigeo_Oficina)
	END
	Set Nocount off
END
