﻿--JHD-20141103-Se Cambio la condicion del campo NuDocum2 para mostrar los guiones  
--HLF-20141106-NuDocum2=dbo.FnFormatearDocumProvee(dp.NuDocum)
 --JHD-20141205-Se cambio el tipo de dato de @NuDocum a varchar(30)
 --JHD-20150518-NuDocum=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
 --JHD-20150518-NuDocum2=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
 --JHD-20150812-NuDocInterno= dp.CardCodeSAP,
  --JHD-20150826- NuDocum= case when FlCorrelativo=1 then '' else
				-- case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
				--end,
 --JHD-20150826- NuDocum2=case when FlCorrelativo=1 then '' else
				-- case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			    -- end, 
 --JHD-20151118- @CoMoneda char(3)=''
 --JHD-20151118- and (dp.CoMoneda_FEgreso=@CoMoneda or @CoMoneda='')
 --JRF-20160330- ..Agregar @IDProveedorSetours
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_ListxNuOrdenServicio]  
 @NuDocumProv int=0,    
 @NuVoucher char(14)='',    
 @NuDocum varchar(15)='',    
 @CoTipoDoc char(3)='',     
 @FlActivo bit=0,
 @CoMoneda char(3)='',
 @IDProveedorSetours char(6)
AS    
BEGIN   
SET NOCOUNT ON   
SELECT     dp.NuDocumProv,    
   dp.NuVoucher,    
   --dp.NuDocum,    
   --NuDocum=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
    NuDocum= case when FlCorrelativo=1 then '' else
				 case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,
   --NuDocum2=dbo.FnFormatearDocumProvee(dp.NuDocum),
   --NuDocum2=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
     NuDocum2=case when FlCorrelativo=1 then '' else
				case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,  

   NuDocInterno= dp.CardCodeSAP,    
   dp.CoTipoDoc,     
          td.Descripcion,    
          dp.FeEmision,    
          dp.FeRecepcion,    
          dp.CoTipoDetraccion,     
          PorcentajeDet=(select isnull(SsTasa,0)*100 from dbo.MATIPODETRACCION where CoTipoDetraccion=dp.CoTipoDetraccion),    
          dp.CoMoneda,    
          mo.Descripcion AS Moneda,    
          SsNeto,    
          SsOtrCargos,    
          SsDetraccion,    
          dp.SsIGV,    
          dp.SSTotalOriginal,     
           dp.SSTipoCambio,    
          dp.SSTotal,    
          --dp.CoEstado,    
          --Estado=case when dp.CoEstado='RE' THEN 'Recepcionado' when dp.CoEstado='PG' then 'Pagado' when dp.CoEstado='AN' THEN 'Anulado' else '' end    
          dp.FlActivo    
FROM         DOCUMENTO_PROVEEDOR dp  
--INNER JOIN  VOUCHER_OPERACIONES ON dp.NuVoucher = VOUCHER_OPERACIONES.IDVoucher   
INNER JOIN  MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc INNER JOIN    
                      MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda    
WHERE     (dp.NuDocumProv = @NuDocumProv or @NuDocumProv=0) AND    
(dp.NuOrden_Servicio In (select NuOrden_Servicio from ORDEN_SERVICIO os where ltrim(rtrim(os.CoOrden_Servicio)) Like @NuVoucher+'%' 
						 And Ltrim(Rtrim(IsNull(os.IDProveedorSetours,'')))=Ltrim(Rtrim(@IDProveedorSetours)))) AND    
(ltrim(rtrim(@NuDocum))='' OR dp.NuDocum LIKE '%'+@NuDocum+'%' ) AND     
                      (dp.CoTipoDoc = @CoTipoDoc or ltrim(rtrim(@CoTipoDoc))='') AND    
                        (dp.FlActivo = @FlActivo or @FlActivo =0)-- AND    
                      --(dp.CoEstado = @CoEstado or @CoEstado='')    
					  and (dp.CoMoneda_FEgreso=@CoMoneda or @CoMoneda='')
END;
