﻿
----------------------------------------------------------------------------



-----------------------------------------------------------------------------------------
--create
create procedure MACLIENTES_Upd_Tipo
@IDCliente char(6),
@UserMod char(4)
as
begin
declare @tipo char(2)
select @tipo=tipo from  MACLIENTES where IDCliente=@IDCliente

if @tipo='IT'
begin
update MACLIENTES
set Tipo='CL',
UserMod=@UserMod,
FecMod=getdate()
where IDCliente=@IDCliente
end

end;
