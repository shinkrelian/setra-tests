﻿--JRF-20150525-(Select cd2.CostoRealEditado from COTIDET cd2 where cd2.IDDET=cds.IDDET And cd2.IDServicio_Det=cds.IDServicio_Det)..
--JRF-20150528-No Considerar los generados por acomodo transporte (... and rd.IDReserva_DetOrigAcomVehi is null And)
--JRF-20150619-No considerar los sub-servicios cuyo proveedor del servicio que lo relacione sea Peru Rail (IDServicio_Det_V)
--JRF-20150717-No considerar servicios anulados
--JRF-20160311- ... IsNull(c.IDDetAntiguo,0) as IDDetAntiguo
--JRF-20160413- .. Case When IsNull(rd.IDReserva_Det,0)= 0 then ... {generación correcta del servicio y hora}
CREATE Procedure dbo.COTIDET_DETSERVICIOS_Sel_xIDCabxUpdReserva
 @IDCAB int,                                                  
 @ConCopias bit,                                                
 @SoloparaRecalcular bit
As                                                  
 Set NoCount On  
 SELECT       
 Case When (((p.IDTipoProv='001'and sd.PlanAlimenticio=0) Or(p.IDTipoProv<>'001' and sd.ConAlojamiento=1)))      
 Then ROW_NUMBER()Over(Partition By c.IDProveedor,c.IDServicio_Det,c.IncGuia,dbo.FnAcomodosEspecialesxIDDet(c.IDDet) Order by c.Dia, c.Item ,rd.CapacidadHab desc ,rd.EsMatrimonial asc)       
 else 0 End as ItemAlojamiento,      
 c.IDDET,                      
 c.Item,c.Dia,                                                  
 upper(substring(DATENAME(dw,c.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,c.Dia,103))) as DiaFormat,
 Case When p.IDTipoProv='001' And sd.PlanAlimenticio=0 Then ''                                             
 Else                                             
 substring(convert(varchar,c.Dia,108),1,5)
 End as Hora,                                                                           
 u.IDPais, c.IDUbigeo, u.Descripcion as DescCiudad ,p.IDTipoProv,p.IDTipoOper, s.Dias,
 c.IDProveedor,                                                  
 p.NombreCorto as DescProveedor,                                       
                               
 pinternac.NombreCorto as DescProvInternac,
                                    
 c.Servicio As DescServicioDet,                                                             
 Case When p.IDTipoProv='001' or p.IDTipoProv='002' Then  --hoteles o restaurantes                                                
  sd.Descripcion                                                   
 Else                                                  
  s.Descripcion                                                  
 End As DescServicioDetBup,                                                   
 c.IDServicio, c.IDServicio_Det,                              
 s.IDTipoServ, ts.Descripcion as DescTipoServ , sd.Anio,sd.Tipo,                  
 IsNull(sd.DetaTipo,'') as DescVariante,                                          
 c.NroPax,                                       
                                       
 CAST(c.NroPax AS VARCHAR(3))+                                          
 Case When isnull(c.NroLiberados,0)=0 then '' else '+'+CAST(c.NroLiberados AS VARCHAR(3)) end                                          
 as PaxmasLiberados,                                            
                                      
 c.IncGuia, c.NroLiberados, c.Tipo_Lib,                                                  
 c.CostoReal,isnull(c.CostoRealAnt,0) as CostoRealAnt, c.CostoRealImpto, c.Margen,                                                   
 c.MargenImpto, c.CostoLiberado, c.CostoLiberadoImpto,                                                   
 c.MargenLiberado, c.MargenLiberadoImpto, c.MargenAplicado, isnull(c.MargenAplicadoAnt,0) as MargenAplicadoAnt,                                                  
 c.SSCR, c.SSCRImpto,                                                   
 c.SSMargen, c.SSMargenImpto, c.SSCL,                                                   
 c.SSCLImpto, c.SSMargenLiberado, c.SSMargenLiberadoImpto,                                                   
 c.SSTotal, c.SSTotalOrig, c.STCR, c.STCRImpto,                                       
 c.STMargen, c.STMargenImpto, c.STTotal, c.STTotalOrig,           
 c.TotImpto, c.Especial, c.MotivoEspecial, c.RutaDocSustento,                                                    
 c.Desayuno, c.Lonche, c.Almuerzo, c.Cena,                                                  
 c.Transfer,          
 ISNull(c.IDDetTransferOri,0) as IDDetTransferOri, ISNull(c.IDDetTransferDes,0) as IDDetTransferDes,                    
 ISNull(c.TipoTransporte,'') as TipoTransporte,                                                  
 c.IDUbigeoOri,                                                 
                                      
 --uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                  
 uo.Descripcion+'          |'+IsNull(uo.DC,'') as DescUbigeoOri,                                                  
 c.IDUbigeoDes,                                                 
                                      
 --ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                               
 ud.Descripcion+'          |'+IsNull(ud.DC,'') as DescUbigeoDes,                                               
                                      
 c.IDIdioma,Id.Siglas, sd.ConAlojamiento,IsNull(cc.Margen,0) as MargenCotiCab,                                                   
 c.Cotizacion,isnull(c.Servicio,'') as Servicio, isnull(c.IDDetRel,0) as IDDetRel ,                                                  
 isnull(c.IDDetCopia,0) as IDDetCopia,c.Total,c.TotalOrig,                                                
 0 as PorReserva, '' as IDReserva,                                                
 c.ExecTrigger, c.FueradeHorario   as FueradeHorario,                                              
 c.CostoRealEditado
 , c.CostoRealImptoEditado, c.MargenAplicadoEditado,                                             
 c.ServicioEditado, c.RedondeoTotal, sd.PlanAlimenticio,                                            
 --isnull(cc.TipoCambio,0) as TipoCambioCotiCab,                
 isnull(ctc.SsTipCam,0)  as TipoCambioCotiCab,                
 c.DebitMemoPendiente ,                                          
 Case When                     
 Exists(select sd2.IDServicio from MASERVICIOS_DET SD2 Left Join MASERVICIOS S2 on SD2.IDServicio = S2.IDServicio                    
  where s2.IDCabVarios = @IDCAB and sd2.IDServicio_Det =c.IDServicio_Det) then 1 else 0 End As ServicioVarios,                                    
 Case When ht.IdHabitTriple = '000' Or ht.IdHabitTriple = '002'                                    
 Then ''                                   
 else                                   
 Case When sd.IDServicio_DetxTriple is Not null                                   
  then (select sd3.Descripcion                                   
    from MASERVICIOS_DET sd3 where sd3.IDServicio_Det = sd.IDServicio_DetxTriple)                                  
    +' ('+ ht.Descripcion+')' else                                  
 ht.Descripcion                                   
 End                                   
 End as TipoHabTriple,                                  
  Case When rd.IDReserva_Det is null and (p.IDTipoProv='001' Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)) Then       
  IsNull((Select Top(1) rd2.IDReserva_Det from RESERVAS_DET rd2 Left Join RESERVAS r2 On rd2.IDReserva=r2.IDReserva      
     Left Join MAPROVEEDORES p3 On r2.IDProveedor=p3.IDProveedor Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det=sd2.IDServicio_Det      
  where rd2.IDDet=c.IDDet and r2.Anulado=0 and rd2.Anulado=0 and (p3.IDTipoProv='001' Or (p3.IDTipoProv='003' and sd2.ConAlojamiento=1))      
  and r2.IDCab=@IDCAB and r2.IDProveedor=p.IDProveedor and rd2.IDServicio_Det=c.IDServicio_Det and rd2.FechaOut Is Not Null    
  and c.Dia between rd2.Dia and DATEADD(D,-1,rd2.FechaOut)),0)    
      
  else rd.IDReserva_Det End    
  as IDReserva_Det,        
 CalculoTarifaPendiente,                        
c.AcomodoEspecial  ,                      
 --Case when (p.IDTipoProv='001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv = '003' and sd.ConAlojamiento = 1)                      
 dbo.FnDescripcionFechaEspecial(c.Dia,c.IDubigeo) as DescFechaEsp,             
 sd.CoMoneda as CoMonedaServ,               
 c.FlSSCREditado, c.FlSTCREditado ,      
 IsNull(Case When rd.CapacidadHab is null and (p.IDTipoProv='001' Or (p.IDTipoProv='003' and sd.ConAlojamiento=1)) Then       
    IsNull((Select Top(1) rd2.CapacidadHab from RESERVAS_DET rd2 Left Join RESERVAS r2 On rd2.IDReserva=r2.IDReserva      
     Left Join MAPROVEEDORES p3 On r2.IDProveedor=p3.IDProveedor Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det=sd2.IDServicio_Det      
  where r2.Anulado=0 and rd2.Anulado=0 and (p3.IDTipoProv='001' Or (p3.IDTipoProv='003' and sd2.ConAlojamiento=1))       
  and r2.IDCab=@IDCAB and r2.IDProveedor=p.IDProveedor and rd2.IDServicio_Det=c.IDServicio_Det and rd2.FechaOut Is Not Null      
  and c.Dia between rd2.Dia and DATEADD(D,-1,rd2.FechaOut)),0)    
 else rd.CapacidadHab End,0)  
  as CapacidadHab,      
 IsNull(Case When  rd.EsMatrimonial is null and (p.IDTipoProv='001' Or (p.IDTipoProv='003' and sd.ConAlojamiento=1))  Then       
    IsNull((Select Top(1) rd2.EsMatrimonial from RESERVAS_DET rd2 Left Join RESERVAS r2 On rd2.IDReserva=r2.IDReserva      
     Left Join MAPROVEEDORES p3 On r2.IDProveedor=p3.IDProveedor Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det=sd2.IDServicio_Det      
  where r2.Anulado=0 and rd2.Anulado=0 and (p3.IDTipoProv='001' Or (p3.IDTipoProv='003' and sd2.ConAlojamiento=1))       
  and r2.IDCab=@IDCAB and r2.IDProveedor=p.IDProveedor and rd2.IDServicio_Det=c.IDServicio_Det and rd2.FechaOut Is Not Null      
  and c.Dia between rd2.Dia and DATEADD(D,-1,rd2.FechaOut)),0)      
  else rd.EsMatrimonial End,0) as EsMatrimonial,      
 (Select COUNT(*) from COTIDET_ACOMODOESPECIAL ca where ca.IDDet=c.IDDET) as CantidadAcomodos,      
 Isnull(rd.FlServicioParaGuia,0) as ResServGuia,Ltrim(Rtrim(dbo.FnAcomodosEspecialesxIDDet(c.IDDet))) as DescAcomodosEspeciales,
 ISNULL(rd.FlServicioNoShow,0) as FlServicioNoShow,IsNull(rd.FlServicioIngManual,0) as FlServicioIngManual ,
 IsNull(C.FlServicioTC,0) As FlServicioTC,IsNull(c.IDDetAntiguo,0) as IDDetAntiguo
 FROM COTIDET c Left Join MAPROVEEDORES p On c.IDProveedor=p.IDProveedor                                                   
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo                                                  
 Left Join MAUBIGEO uo On c.IDUbigeoOri=uo.IDubigeo                                                  
 Left Join MAUBIGEO ud On c.IDUbigeoDes=ud.IDubigeo                                              
 Left Join MASERVICIOS_DET sd On c.IDServicio_Det=sd.IDServicio_Det                                                  
 Left Join MASERVICIOS s On s.IDServicio=sd.IDServicio                                                  
 Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ                                                  
 Left Join COTICAB cc On c.IDCAB=cc.IDCAB                                                  
 Left Join MAIDIOMAS Id On c.IDidioma = Id.IDidioma
 Left Join MAHABITTRIPLE ht On sd.IdHabitTriple = ht.IdHabitTriple
 Left Join MAPROVEEDORES pinternac On p.IDProveedorInternacional=pinternac.IDProveedor
 --Left Join MATIPOPROVEEDOR tp On p.IDTipoProv = tp.IDTipoProv                      
 Left Join COTICAB_TIPOSCAMBIO ctc On sd.CoMoneda=ctc.CoMoneda and ctc.IDCab=c.IDCAB
 Left Join RESERVAS_DET rd On c.IDDET=rd.IDDet and IsNull(rd.Anulado,0)=0
 and (rd.IDReserva = (select Top(1) IDReserva from RESERVAS r2 where r2.IDProveedor=c.IDProveedor and r2.IDCab=@IDCAB and r2.Anulado=0))
 Left Join RESERVAS r On rd.IDReserva = r.IDReserva
 WHERE r.IDReservaProtecc is null and c.IDCAB=@IDCAB And IsNull(rd.Anulado,0)=0 and        
 (c.IDDetCopia Is Null Or @ConCopias=1) And                 
 (@SoloparaRecalcular=0 Or c.Recalcular=1) --and c.IDProveedor='000541' --and(rd.CapacidadHab is not null and rd.EsMatrimonial is not null)        
And Not Exists(select  IDServicio_Det from COTIDET_DETSERVICIOS cds where cds.IDDET= c.IDDET)  --And rd.IDReserva_DetOrigAcomVehi is null 
and sd.Estado = 'A'
 Union all
select 0 as ItemAlojamiento,cds.IDDET,cd.Item,
--cd.Dia,
Case IsNull(rd.IDReserva_Det,0) 
When 0 Then IsNull((select cd4.Dia from COTIDET cd4 
					where cd4.IDCab=cd.IDCab And cd4.IDProveedor=cd.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det), 
					Cast(Convert(char(10),cd.Dia,103) as smalldatetime))
Else
	Case When rd.FeUpdateRow is null then rd.Dia
	Else 
		IsNull((select cd4.Dia from COTIDET cd4 
				where cd4.IDCab=cd.IDCab And cd4.IDProveedor=cd.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det),
				Cast(Convert(char(10),cd.Dia,103) as smalldatetime))
	End
End As Dia,
upper(substring(DATENAME(dw,
Case IsNull(rd.IDReserva_Det,0) 
When 0 Then IsNull((select cd4.Dia from COTIDET cd4 
					where cd4.IDCab=cd.IDCab And cd4.IDProveedor=cd.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det), 
					Cast(Convert(char(10),cd.Dia,103) as smalldatetime))
Else
	Case When rd.FeUpdateRow is null then rd.Dia
	Else 
		IsNull((select cd4.Dia from COTIDET cd4 
				where cd4.IDCab=r.IDCab And cd4.IDProveedor=r.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det),
				Cast(Convert(char(10),cd.Dia,103) as smalldatetime))
	End
End
),1,3))+' '+ltrim(rtrim(convert(varchar,
Case IsNull(rd.IDReserva_Det,0) 
When 0 Then IsNull((select cd4.Dia from COTIDET cd4 
					where cd4.IDCab=cd.IDCab And cd4.IDProveedor=cd.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det), 
					Cast(Convert(char(10),cd.Dia,103) as smalldatetime))
Else
	Case When rd.FeUpdateRow is null then rd.Dia
	Else 
		IsNull((select cd4.Dia from COTIDET cd4 
				where cd4.IDCab=cd.IDCab And cd4.IDProveedor=cd.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det),
				Cast(Convert(char(10),cd.Dia,103) as smalldatetime))
	End
End
,103))) as DiaFormat,
substring(convert(varchar,cd.Dia,108),1,5) as Hora,
u.IDPais, cd.IDUbigeo, u.Descripcion as DescCiudad ,p.IDTipoProv,p.IDTipoOper, s.Dias,
cd.IDProveedor,p.NombreCorto as DescProveedor,
pinternac.NombreCorto as DescProvInternac,
cd.Servicio as DescServicioDet,
s.Descripcion as DescServicioDetBup,
sd.IDServicio, sd.IDServicio_Det,
s.IDTipoServ, ts.Descripcion as DescTipoServ , sd.Anio,sd.Tipo,                  
 IsNull(sd.DetaTipo,'') as DescVariante,                                          
 cd.NroPax,
  CAST(cd.NroPax AS VARCHAR(3))+                                          
 Case When isnull(cd.NroLiberados,0)=0 then '' else '+'+CAST(cd.NroLiberados AS VARCHAR(3)) end                                          
 as PaxmasLiberados,
 cd.IncGuia, cd.NroLiberados, cd.Tipo_Lib,                                                  
 cds.CostoReal,isnull(cds.CostoReal,0) as CostoRealAnt, cds.CostoRealImpto, cds.Margen,                                                   
 cds.MargenImpto, cds.CostoLiberado, cds.CostoLiberadoImpto,                                                   
 cds.MargenLiberado, cds.MargenLiberadoImpto, cds.MargenAplicado, isnull(cds.MargenAplicado,0) as MargenAplicadoAnt,                                                  
 cds.SSCR, cds.SSCRImpto,                                                   
 cds.SSMargen, cds.SSMargenImpto, cds.SSCL,                                                   
 cds.SSCLImpto, cds.SSMargenLiberado, cds.SSMargenLiberadoImpto,                                                   
 cds.SSTotal, cds.SSTotal as SSTotalOrig, cds.STCR, cds.STCRImpto,                                         
 cds.STMargen, cds.STMargenImpto, cds.STTotal, cds.STTotal as STTotalOrig,           
 cds.TotImpto, cd.Especial, cd.MotivoEspecial, cd.RutaDocSustento,                                                    
 sd.Desayuno, sd.Lonche, sd.Almuerzo, sd.Cena,
 s.Transfer,
 ISNull(cd.IDDetTransferOri,0) as IDDetTransferOri, ISNull(cd.IDDetTransferDes,0) as IDDetTransferDes,                                         
 ISNull(s.TipoTransporte,'') as TipoTransporte,                                                  
 cd.IDUbigeoOri,
 uo.Descripcion+'          |'+IsNull(uo.DC,'') as DescUbigeoOri,                                                  
 cd.IDUbigeoDes,                                                 
 ud.Descripcion+'          |'+IsNull(ud.DC,'') as DescUbigeoDes,
 cd.IDIdioma,Id.Siglas, Case When Exists(select Id from COTIVUELOS where IDCab=@IDCAB And IDDET=cds.IDDET) Then 0 Else sd.ConAlojamiento End as ConAlojamiento,
 IsNull(cc.Margen,0) as MargenCotiCab,
 cc.Cotizacion,
Case IsNull(rd.IDReserva_Det,0) 
When 0 Then IsNull((select cd4.Servicio from COTIDET cd4 
					where cd4.IDCab=cd.IDCab And cd4.IDProveedor=cd.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det),sd.Descripcion)
Else
	Case When rd.FeUpdateRow is null then rd.Servicio
	Else 
		IsNull((select cd4.Servicio from COTIDET cd4 
				where cd4.IDCab=cd.IDCab And cd4.IDProveedor=cd.IDProveedor And cd4.IDDET=cds.IDDET And cd4.IDServicio_Det=cds.IDServicio_Det),sd.Descripcion)
	End
End as Servicio,
 isnull(cd.IDDetRel,0) as IDDetRel,
 isnull(cd.IDDetCopia,0) as IDDetCopia,cds.Total,cds.Total as TotalOrig,
 0 as PorReserva, '' as IDReserva,
 cd.ExecTrigger, cd.FueradeHorario   as FueradeHorario,                                              
 IsNull((Select cd2.CostoRealEditado from COTIDET cd2 where cd2.IDDET=cds.IDDET And cd2.IDServicio_Det=cds.IDServicio_Det),0) as CostoRealEditado, --cd.CostoRealEditado, 
 cd.CostoRealImptoEditado, cd.MargenAplicadoEditado,                                            
 cd.ServicioEditado, cds.RedondeoTotal, sd.PlanAlimenticio,                
 isnull(ctc.SsTipCam,0)  as TipoCambioCotiCab,                
 cd.DebitMemoPendiente,
  Case When                     
 Exists(select sd2.IDServicio from MASERVICIOS_DET SD2 Left Join MASERVICIOS S2 on SD2.IDServicio = S2.IDServicio                    
  where s2.IDCabVarios = cd.IDCAB and sd2.IDServicio_Det =cd.IDServicio_Det) then 1 else 0 End As ServicioVarios,                                    
 Case When ht.IdHabitTriple = '000' Or ht.IdHabitTriple = '002'                                    
 Then ''                                   
 else                                   
 Case When sd.IDServicio_DetxTriple is Not null                                   
  then (select sd3.Descripcion                                   
    from MASERVICIOS_DET sd3 where sd3.IDServicio_Det = sd.IDServicio_DetxTriple)                                  
    +' ('+ ht.Descripcion+')' else                                 
 ht.Descripcion                                   
 End                                   
 End as TipoHabTriple,
 IsNull(rd.IDReserva_Det,0) as IDReserva_Det,
 cd.CalculoTarifaPendiente,                        
 cd.AcomodoEspecial,
 dbo.FnDescripcionFechaEspecial(cd.Dia,cd.IDubigeo) as DescFechaEsp,                  
 sd.CoMoneda as CoMonedaServ,               
 cd.FlSSCREditado, cd.FlSTCREditado,
 IsNull(rd.CapacidadHab,0) as CapacidadHab,
 IsNull(rd.EsMatrimonial,0) as EsMatrimonial,
 (Select COUNT(*) from COTIDET_ACOMODOESPECIAL ca where ca.IDDet=cd.IDDET) as CantidadAcomodos,      
 Isnull(rd.FlServicioParaGuia,0) as ResServGuia,Ltrim(Rtrim(dbo.FnAcomodosEspecialesxIDDet(cd.IDDet))) as DescAcomodosEspeciales,
 ISNULL(rd.FlServicioNoShow,0) as FlServicioNoShow,IsNull(rd.FlServicioIngManual,0) as FlServicioIngManual ,
 IsNull(cd.FlServicioTC,0) As FlServicioTC,0 as IDDetAntiguo
from COTIDET_DETSERVICIOS cds --Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det
Left Join RESERVAS_DET rd On cds.IDDET=rd.IDDet and rd.IDServicio_Det=cds.IDServicio_Det and IsNull(rd.Anulado,0)=0
Left Join RESERVAS r On rd.IDReserva = r.IDReserva
Inner Join COTIDET cd On cds.IDDET=cd.IDDET 
Left Join MAUBIGEO u On cd.IDubigeo=u.IDubigeo
Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det
Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
Left Join MAHABITTRIPLE ht On sd.IdHabitTriple = ht.IdHabitTriple
Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ
Left Join MAPROVEEDORES pinternac On p.IDProveedorInternacional=pinternac.IDProveedor
Left Join MAUBIGEO uo On cd.IDUbigeoOri=uo.IDubigeo
Left Join MAUBIGEO ud On cd.IDUbigeoDes=ud.IDubigeo
Left Join MAIDIOMAS Id On cd.IDidioma = Id.IDidioma
Left Join COTICAB_TIPOSCAMBIO ctc On sd.CoMoneda=ctc.CoMoneda and ctc.IDCab=cd.IDCAB
Left Join COTICAB cc On cd.IDCAB=cc.IDCAB
--where cds.IDDET = 457661
Left Join MASERVICIOS_DET cdsCot On cds.IDServicio_Det=cdsCot.IDServicio_Det                
Left Join MASERVICIOS_DET sdCot On cdsCot.IDServicio_Det_V=sdCot.IDServicio_Det
Left Join MASERVICIOS sCot On sdCot.IDServicio=scot.IDServicio
WHERE r.IDReservaProtecc is null and cd.IDCAB=@IDCAB And 
IsNull(rd.Anulado,0)=0 and rd.IDReserva_DetOrigAcomVehi is null And IsNull(SCOT.IDProveedor,'') <> '001836' And
 (cd.IDDetCopia Is Null Or @ConCopias=1) And --cd.IDProveedor='000541' And
 (@SoloparaRecalcular=0 Or cd.Recalcular=1) and sd.Estado = 'A'
ORDER BY Dia, Item ,CapacidadHab desc ,EsMatrimonial asc
