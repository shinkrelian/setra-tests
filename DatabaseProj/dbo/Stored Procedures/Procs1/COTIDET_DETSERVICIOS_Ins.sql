﻿--JRF-20121010-Nuevo Campo Redondeo.    }
--JRF-20130227-Nueva longitud del los campos Numeric(8,2)a Numeric(12,4)
CREATE Procedure [dbo].[COTIDET_DETSERVICIOS_Ins]    
 @IDDET int,    
 @IDServicio_Det int,     
 @CostoReal numeric(12,4),    
 @CostoLiberado numeric(12,4),    
 @Margen numeric(12,4),    
 @MargenAplicado numeric(6,2),    
 @MargenLiberado numeric(12,4),    
 @Total numeric(8,2),    
 @RedondeoTotal numeric(12,4)= 0,  
 @SSCR numeric(12,4),    
 @SSCL numeric(12,4),    
 @SSMargen numeric(12,4),    
 @SSMargenLiberado numeric(12,4),    
 @SSTotal numeric(12,4),    
 @STCR numeric(12,4),    
 @STMargen numeric(12,4),    
 @STTotal numeric(12,4),    
 @CostoRealImpto numeric(12,4),    
 @CostoLiberadoImpto numeric(12,4),    
 @MargenImpto numeric(12,4),    
 @MargenLiberadoImpto numeric(12,4),    
 @SSCRImpto numeric(12,4),    
 @SSCLImpto numeric(12,4),    
 @SSMargenImpto numeric(12,4),    
 @SSMargenLiberadoImpto numeric(12,4),    
 @STCRImpto numeric(12,4),    
 @STMargenImpto numeric(12,4),    
 @TotImpto numeric(12,4),
 @UserMod char(4)    
As    
 Set Nocount On    
      
 INSERT INTO COTIDET_DETSERVICIOS    
           (IDDet    
           ,IDServicio_Det    
           ,CostoReal    
           ,CostoLiberado    
           ,Margen    
           ,MargenAplicado    
           ,MargenLiberado    
           ,Total    
           ,RedondeoTotal  
           ,SSCR    
           ,SSCL    
           ,SSMargen    
           ,SSMargenLiberado    
           ,SSTotal    
           ,STCR    
           ,STMargen    
           ,STTotal    
           ,CostoRealImpto    
           ,CostoLiberadoImpto    
           ,MargenImpto    
           ,MargenLiberadoImpto    
           ,SSCRImpto    
           ,SSCLImpto    
           ,SSMargenImpto    
           ,SSMargenLiberadoImpto    
           ,STCRImpto    
           ,STMargenImpto    
           ,TotImpto                          
           ,UserMod)    
     VALUES    
           (@IDDet,    
           @IDServicio_Det,    
           @CostoReal ,    
           @CostoLiberado ,    
           @Margen ,    
           @MargenAplicado ,    
           @MargenLiberado ,    
           @Total ,  
           @RedondeoTotal ,    
           @SSCR ,    
           @SSCL ,    
           @SSMargen ,    
           @SSMargenLiberado ,    
           @SSTotal ,    
           @STCR ,    
           @STMargen ,    
           @STTotal ,    
           @CostoRealImpto ,    
           @CostoLiberadoImpto ,    
           @MargenImpto ,    
           @MargenLiberadoImpto ,    
           @SSCRImpto ,    
           @SSCLImpto ,    
           @SSMargenImpto ,    
           @SSMargenLiberadoImpto ,    
           @STCRImpto ,    
           @STMargenImpto ,    
           @TotImpto ,               
           @UserMod)    
