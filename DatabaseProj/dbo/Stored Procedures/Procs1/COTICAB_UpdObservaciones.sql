﻿CREATE PROCEDURE [dbo].[COTICAB_UpdObservaciones]
				@IDCab int,
				@ObservacionVentas varchar(Max)
AS
BEGIN
	Set NoCount On                          
    	UPDATE COTICAB SET ObservacionVentas = @ObservacionVentas
		WHERE IDCAB=@IDCab                           
END

