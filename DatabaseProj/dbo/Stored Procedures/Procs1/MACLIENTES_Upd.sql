﻿--ALTER
--JRF-20131205-Nuevo campo para Reservas        
--JRF-20140428-Nuevos campos (Complejidad,Frecuencia,Solvencia)      
--MLL-20140529-Nuevos Campos @CoStarSoft , @NuCuentaComision      
--JRF-20140711-Aumentar un caracter en el campo de Tipo.
--JHD-20141113-Nuevo campo FlTop
--JHD-20141114-Nuevos campos: FePerfilUpdate,CoUserPerfilUpdate,txPerfilInformacion,txPerfilOperatividad
--JHD-20141124-se cambió para que FePerfilUpdate se grabe con update en caso haya modificaciones
--JHD-20141218- Campos txPerfilInformacion,txPerfilOperatividad aumentados a varchar(max)
--JHD-20150116- Se agregó el campo FlProveedor
--JHD-20150408- Se agregó el campo NuPosicion tinyint
--JHD-20150603-Se cambio el campo @NuCuentaComision a varchar(14)
CREATE Procedure [dbo].[MACLIENTES_Upd]            
 @IDCliente char(6),            
 @RazonSocial varchar(80),          
 @RazonComercial varchar(80),            
 @Persona char(1),            
 @Nombre varchar(60),            
 @ApPaterno varchar(60),            
 @ApMaterno varchar(60),            
 @Domiciliado bit,            
 @Ciudad varchar(60),          
 @Direccion varchar(200),            
 @Tipo char(2),            
 @IDIdentidad char(3),            
 @NumIdentidad varchar(15),            
 @Telefono1 varchar(30),            
 @Telefono2 varchar(30),            
 @Celular varchar(25),            
 @Fax varchar(25),            
 @Comision decimal(6,2),            
 @Notas text,            
 @IDCiudad char(6),              
 @IDvendedor char(6),            
 @Web varchar(100),            
 @CodPostal varchar(50),             
 @Credito bit,            
 @MostrarEnReserva bit,      
 @Complejidad char(1),      
 @Frecuencia char(1),      
 @Solvencia tinyint,       
 @UserMod char(4),      
 @CoStarSoft char(11),      
 @NuCuentaComision varchar(14),
 @FlTop bit=0,
 --@FePerfilUpdate datetime,        
 @CoUserPerfilUpdate varchar(4)='',        
 @txPerfilInformacion varchar(MAX)='',        
 @txPerfilOperatividad varchar(MAX)='',
 @FlProveedor bit=0,
 @NuPosicion tinyint=0
As            
 Set NoCount On            
 Declare @RazonSocialExist varchar(60)            
 UPDATE MACLIENTES SET                       
           Persona=@Persona            
           ,RazonComercial =@RazonComercial          
           ,RazonSocial=@RazonSocial            
           ,Nombre=case when LTRIM(RTRIM(@Nombre))='' then Null Else @Nombre End            
           ,ApPaterno=case when LTRIM(RTRIM(@ApPaterno))='' then Null Else @ApPaterno End            
           ,ApMaterno=Case When LTRIM(RTRIM(@ApMaterno))='' then Null Else @ApMaterno End            
           ,Domiciliado=@Domiciliado            
           ,Ciudad=@Ciudad          
           ,Direccion=@Direccion            
     ,IDIdentidad=@IDIdentidad            
     ,NumIdentidad=Case When ltrim(rtrim(@NumIdentidad))='' Then Null Else @NumIdentidad End             
     ,Tipo=@Tipo            
           ,Telefono1=Case When ltrim(rtrim(@Telefono1))='' Then Null Else @Telefono1 End             
           ,Telefono2=Case When ltrim(rtrim(@Telefono2))='' Then Null Else @Telefono2 End             
           ,Celular=Case When ltrim(rtrim(@Celular))='' Then Null Else @Celular End             
           ,Fax=Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End             
           ,Comision=Case When @Comision=0 Then Null Else @Comision End             
           ,Notas=Case When ltrim(rtrim(Cast(@Notas as varchar(max))))='' Then Null Else @Notas End            
           ,IDCiudad=@IDCiudad            
           ,IDvendedor=@IDvendedor            
           ,Web=Case When ltrim(rtrim(@Web))='' Then Null Else @Web End             
           ,CodPostal=Case When ltrim(rtrim(@CodPostal))='' Then Null Else @CodPostal End             
           ,Credito=@Credito          
           ,MostrarEnReserva =  @MostrarEnReserva       
           ,Complejidad = @Complejidad      
           ,Frecuencia = @Frecuencia      
  ,Solvencia = @Solvencia      
           ,UserMod=@UserMod            
           ,FecMod=getdate()       
           ,CoStarSoft=case when ltrim(rtrim(@CoStarSoft))= '' then null else  @CoStarSoft end      
     ,NuCuentaComision=case when ltrim(rtrim(@NuCuentaComision))= '' OR ltrim(rtrim(@NuCuentaComision))= '000000'    
    then null else  @NuCuentaComision end,
			FlTop=@FlTop
			--,FePerfilUpdate=@FePerfilUpdate      
			--,FePerfilUpdate=case when CoUserPerfilUpdate<>@CoUserPerfilUpdate then getdate() else FePerfilUpdate end
			,FePerfilUpdate=case when @CoUserPerfilUpdate<>'' then getdate() else FePerfilUpdate end
		    --,CoUserPerfilUpdate=case when ltrim(rtrim(@CoUserPerfilUpdate))= '' then null else  @CoUserPerfilUpdate end    --,CoUserPerfilUpdate     
		    ,CoUserPerfilUpdate=case when ltrim(rtrim(@CoUserPerfilUpdate))= '' then CoUserPerfilUpdate else  @CoUserPerfilUpdate end    --,CoUserPerfilUpdate     
		    ,txPerfilInformacion=case when ltrim(rtrim(@txPerfilInformacion))= '' then null else  @txPerfilInformacion end--,txPerfilInformacion
		    ,txPerfilOperatividad=case when ltrim(rtrim(@txPerfilOperatividad))= '' then null else  @txPerfilOperatividad end    --,txPerfilOperatividad     
		    , FlProveedor =isnull(@FlProveedor,cast(0 as bit))
			,NuPosicion=Case When @NuPosicion=0 Then Null Else @NuPosicion End
     WHERE            
           IDCliente = @IDCliente;
