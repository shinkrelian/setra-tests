﻿CREATE Procedure dbo.CORRREOFILE_DelxNuCorreoNoDestNoAdjun
@NuCorreo int
As
Begin
Set NoCount On
If Not Exists(select NuCorreo from BDCORREOSETRA.dbo.DESTINATARIOSCORREOFILE Where NuCorreo=@NuCorreo) 
	Begin
		Exec dbo.CORRREOFILE_DelxNuCorreo @NuCorreo
	End
End