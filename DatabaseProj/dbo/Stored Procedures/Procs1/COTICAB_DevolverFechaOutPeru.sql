﻿--JHD-20151202-Se agrego la condicion que si no encuentra fecha Out Peru, que devuelva la Fecha Out del File
CREATE procedure COTICAB_DevolverFechaOutPeru
@IDCab int=null
as

declare @Dia smalldatetime=null

--Select Top 1 Dia From COTIDET Where IDCAB=@IDCab And IDubigeo in (select IDubigeo from MAUBIGEO where IDPais='000323') Order by dia desc
set @Dia=(Select Top 1 Dia From COTIDET Where IDCAB=@IDCab And IDubigeo in (select IDubigeo from MAUBIGEO where IDPais='000323') Order by dia desc)

if (@Dia is null)
	set @Dia=(Select Top 1 Dia From COTIDET Where IDCAB=@IDCab /*And IDubigeo in (select IDubigeo from MAUBIGEO where IDPais='000323')*/ Order by dia desc)

Select Dia=@Dia

