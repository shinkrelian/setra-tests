﻿--HLF-20160218--ISNULL(NroLiberados,0),
CREATE Procedure [dbo].[COTIDET_Upd_NroPax]    
 @IDDet int,    
 @UserMod char(4)    
As    
 Set NoCount On    
     
 Update COTIDET Set   
    NroPax=(Select COUNT(*) From COTIDET_PAX Where IDDET=@IDDet)
      ---(select isnull(NroLiberados,0) from COTICAB 
      --where IDCAB=(select IDCAB from COTIDET where IDDET = @IDDet)),
	  -ISNULL(NroLiberados,0),
    UserMod=@UserMod, FecMod=GETDATE()     
 Where IDDET=@IDDet 