﻿--HL -20130103-Nuevo parametro @IDFile                   
--JRF-20120829-Nuevo campo MargenCero.                  
--JRF-20130227-Nuevos campos Acomodos x Residente.                
--JRF-20130409-Nuevo campo DiasReconfirmacion.              
--JRF-20140127-Nuevo campo ObservacionGeneral            
--JRF-20140225-Nuevo campo ObservacionVentas         
--JRF-20140313-Nuevo campos DocWordVersion, DocWordFecha         
--JRF-20140318-Nuevo campo de @CoTipoVenta      
--JRF-20140505-Nuevo campo NuSerieCliente  
--JRF-20140703-Nuevos campos [FeAnulacion,IDUsuarioAn]:Se actualizan cuando el estado esta en ANULADO
--JRF-20150512-Nuevo campo CoSerieWeb
--PPMG-20160321-@DatoSeg varchar(max)
--PPMG-20160420-Se agreco el campo @IDContactoCliente
CREATE PROCEDURE [dbo].[COTICAB_Upd]
 @IDCab int,                    
 @Estado char(1),                    
 @IDCliente char(6),                    
 @Titulo varchar(100),                    
 @TipoPax char(3),                    
 @CotiRelacion char(8),                    
 @IDUsuario char(4),                    
 @NroPax smallint,                    
 @NroLiberados smallint,                    
 @Tipo_Lib char(1),                    
 @Margen numeric(8,2),                    
 @IDBanco char(3),                    
 @IDFile char(8),                    
 @IDTipoDoc char(3),                    
 @Observaciones text,                    
 @FecInicio smalldatetime,                    
 @FechaOut smalldatetime,                    
 @Cantidad_dias tinyint,                    
 @FechaSeg smalldatetime,                    
 @DatoSeg varchar(max),                    
 @TipoCambio numeric(8,2),                    
 @DocVta int,                    
 @TipoServicio char(2),                    
 @IDIdioma varchar(12),                    
 @Simple tinyint,                    
 @Twin tinyint,                    
 @Matrimonial tinyint,                    
 @Triple tinyint,                    
 @SimpleResidente tinyint,                    
 @TwinResidente tinyint,                    
 @MatrimonialResidente tinyint,                    
 @TripleResidente tinyint,                   
 @IDubigeo char(6),                    
 @Notas text,                    
 @Programa_Desc varchar(100),                    
 @PaxAdulE tinyint,                    
 @PaxAdulN tinyint,                    
 @PaxNinoE tinyint,                    
 @PaxNinoN tinyint,                    
 @Aporte bit,                    
 @AporteMonto numeric(8,2),                    
 @AporteTotal numeric(8,2),                    
 @Redondeo numeric(3,1),                    
 @Categoria varchar(20),                    
 @MargenCero bit ,                  
 @DiasReconfirmacion tinyint,             
 @ObservacionGeneral varchar(Max),             
 @ObservacionVentas varchar(Max),         
 @DocWordVersion tinyint,        
 @DocWordFecha smalldatetime,        
 @CoTipoVenta char(2),      
 @NuSerieCliente int,  
 @CoSerieWeb varchar(6),
 @UserMod char(4),
 @IDContactoCliente char(3) = NULL
As
BEGIN
 Set NoCount On                    
                  
 UPDATE COTICAB SET                                     
 Estado=@Estado,                    
 IDCliente=@IDCliente,
 IDContactoCliente = CASE WHEN ISNULL(@IDContactoCliente,'')='' THEN NULL ELSE @IDContactoCliente END,
 Titulo=@Titulo,                    
 TipoPax=@TipoPax,                    
 CotiRelacion=Case When ltrim(rtrim(@CotiRelacion))='' Then Null Else @CotiRelacion End,                    
 IDUsuario=@IDUsuario,                    
 NroPax=@NroPax,                    
 NroLiberados=Case When @NroLiberados=0 Then Null Else @NroLiberados End,                    
 Tipo_Lib=Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End,                    
 Margen=Case When @Margen=0 Then Null Else @Margen End,                    
 IDBanco=@IDBanco,                    
 IDFile=                  
 Case When @Estado='A' Then                   
 IDFile                   
 Else                  
 Case When ltrim(rtrim(@IDFile))='' Then Null Else @IDFile End                 
 End,                
 IDTipoDoc=@IDTipoDoc,                    
 Observaciones=Case When ltrim(rtrim(Cast (@Observaciones as varchar(max))))='' Then Null Else @Observaciones End,                    
 FecInicio=@FecInicio,                    
 FechaOut=@FechaOut,                    
 Cantidad_dias=@Cantidad_dias,                    
 FechaSeg=Case When Convert(char(10),@FechaSeg,103)='01/01/1900' Then Null Else @FechaSeg End,                    
 DatoSeg=Case When ltrim(rtrim(@DatoSeg))='' Then Null Else @DatoSeg End,                    
 TipoCambio=Case When @TipoCambio=0 Then Null Else @TipoCambio End,                  
 DocVta=Case When @DocVta=0 Then Null Else @DocVta End,                    
 TipoServicio=@TipoServicio,                    
 IDIdioma=@IDIdioma,                    
 Simple=Case When @Simple=0 Then Null Else @Simple End,                    
 Twin=Case When @Twin=0 Then Null Else @Twin End,                    
 Matrimonial=Case When @Matrimonial=0 Then Null Else @Matrimonial End,                    
 Triple=Case When @Triple=0 Then Null Else @Triple End,                    
                 
 SimpleResidente=Case When @SimpleResidente=0 Then Null Else @SimpleResidente End,                    
 TwinResidente=Case When @TwinResidente=0 Then Null Else @TwinResidente End,                    
 MatrimonialResidente=Case When @MatrimonialResidente=0 Then Null Else @MatrimonialResidente End,                    
 TripleResidente=Case When @TripleResidente=0 Then Null Else @TripleResidente End,                    
                 
 IDubigeo=@IDubigeo,                    
 Notas=Case When ltrim(rtrim(Cast(@Notas as varchar(max))))='' Then Null Else @Notas End,                    
 Programa_Desc=Case When ltrim(rtrim(@Programa_Desc))='' Then Null Else @Programa_Desc End,                    
 PaxAdulE=Case When @PaxAdulE=0 Then Null Else @PaxAdulE End,                    
 PaxAdulN=Case When @PaxAdulN=0 Then Null Else @PaxAdulN End,                    
 PaxNinoE=Case When @PaxNinoE=0 Then Null Else @PaxNinoE End,                    
 PaxNinoN=Case When @PaxNinoN=0 Then Null Else @PaxNinoN End,                    
 Aporte=@Aporte,                    
 AporteMonto=Case When @AporteMonto=0 Then Null Else @AporteMonto End,                    
 AporteTotal=Case When @AporteTotal=0 Then Null Else @AporteTotal End,                    
 Redondeo=@Redondeo,                    
 Categoria=Case When ltrim(rtrim(@Categoria))='' Then Null Else @Categoria End,                    
 MargenCero = @MargenCero,                  
 DiasReconfirmacion = @DiasReconfirmacion,              
 ObservacionGeneral = Case When ltrim(rtrim(@ObservacionGeneral))='' Then Null Else @ObservacionGeneral End,            
 ObservacionVentas = Case When LTRIM(rtrim(@ObservacionVentas))='' Then Null Else @ObservacionVentas End,          
 DocWordVersion = @DocWordVersion,        
 DocWordFecha = @DocWordFecha,        
 CoTipoVenta = @CoTipoVenta,      
 NuSerieCliente = Case When @NuSerieCliente =0 then Null Else @NuSerieCliente End,  
 UserMod=@UserMod, 
 FeAnulacion = Case When COTICAB.FeAnulacion is null and @Estado = 'X' then GETDATE() else COTICAB.FeAnulacion End,                   
 IDUsuarioAn = Case When COTICAB.IDUsuarioAn is null and @Estado = 'X' then @UserMod else COTICAB.IDUsuarioAn End,
 CoSerieWeb = Case When Ltrim(Rtrim(@CoSerieWeb)) = '' Then Null Else @CoSerieWeb End,
 FecMod=GETDATE()                    
 Where  IDCAB=@IDCab                    
END
