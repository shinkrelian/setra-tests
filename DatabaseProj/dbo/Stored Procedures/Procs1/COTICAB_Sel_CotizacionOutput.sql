﻿Create Procedure [dbo].[COTICAB_Sel_CotizacionOutput]
	@IDCab	int,
	@pCotizacion char(8)	Output
As

	Set NoCount On
	
	Select @pCotizacion=IsNull(Cotizacion,'') From COTICAB Where IDCab=@IDCab
