﻿
Create Procedure [dbo].[MAESPECIALIDADPROVEEDOR_Del]
@IDEspecialidad char(2),
@IDProveedor Char(6)
As
	Set NoCount On
	Delete from MAESPECIALIDADPROVEEDOR 
		Where IDEspecialidad = @IDEspecialidad And IDProveedor = @IDProveedor
