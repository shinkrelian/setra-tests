﻿Create Procedure [dbo].[MACONTACTOSPROVEEDOR_Sel_Pk]
	@IDContacto char(3),
	@IDProveedor char(6)
As
	Set NoCount On
	
	Select * From MACONTACTOSPROVEEDOR 
    WHERE
           IDContacto=@IDContacto And IDProveedor=@IDProveedor
