﻿

--JRF-20140407-Agregar los campos de Lectura (Cabecera de Edición)        
--HLF-20140416-d.TxTitulo as Titulo,    
--isnull(cl.NumIdentidad,'') as NumIdentidad, isnull(u.Descripcion,'') as DescPais        
--HLF-20140421-Agregando columnas SsIGV,SsTotalDocumUSD,  
--MLL-20140526-Agregado parametro IDTipoDocVinc,NuDocumVinc  
--HLF-20140529-Quitando isnull a IDTipoDocVinc,NuDocumVinc  
--MLL-20140530-Agregando Parametro CoCtaContable,FeEmisionVinc
--MLL-20140609-Agregado Parametro NuFileLibre
--JHD-20140911-Agregado campo NuNtaVta
--HLF-20150909- ,ISNULL(d.CoSAP,'') as CoSAP
CREATE Procedure [dbo].[DOCUMENTO_Sel_Pk]          
 @NuDocum char(10),        
 @IDTipoDoc char(3)        
As          
 Set NoCount On          
 Select d.CoEstado,D.SsTotalDocumUSD,D.IDMoneda,D.FeDocum,D.IDCab,    
 D.IDTipoDoc,D.NuDocum        
 ,Isnull(c.IDFile,'') as IDFile,Isnull(c.Cotizacion,'') as Cotizacion,         
 case  Isnull(c.CoTipoVenta,'')        
 When 'RC' Then 'RECEPTIVO'        
 when 'CU' Then 'COUNTER'          
 when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'          
 when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'          
 End as TipoVenta,    
 Isnull(cl.RazonComercial,'') As DescCliente,    
 Isnull(Isnull(d.IDCliente,c.IDCliente),'') as IDCliente,        
 mo.Descripcion as Moneda,    
 --Isnull(c.Titulo,'') as Titulo,    
 d.TxTitulo as Titulo,    
 Isnull(d.CoTipoVenta,'') as CoTipoVenta,       -- Isnull(c.CoTipoVenta,'') as CoTipoVenta,     
 Isnull(cl.Direccion,'') as DireccionCliente,    
 isnull(cl.NumIdentidad,'') as NumIdentidad, isnull(u.Descripcion,'') as DescPais,    
 d.SsTotalDocumUSD, d.SSIGV  ,convert(char(10),c.FecInicio,103) as FechaIn,  
 Convert(Char(10),c.FechaOut,103) as FechaOut,  
 --isnull(IDTipoDocVinc,'') as IDTipoDocVinc ,  
 --isnull(NuDocumVinc,'') as NuDocumVinc   
 IDTipoDocVinc,NuDocumVinc,CoCtaContable,FeEmisionVinc,CoCtroCosto,NuFileLibre
 ,NuNtaVta
 ,ISNULL(d.CoSAP,'') as CoSAP
  
 From DOCUMENTO d Left Join COTICAB c On d.IDCab = c.IDCAB        
 Left Join MACLIENTES cl On Isnull(d.IDCliente,c.IDCliente) = cl.IDCliente         
 Left Join MAMONEDAS mo On d.IDMoneda = mo.IDMoneda        
 Left Join MAUBIGEO u On cl.IDCiudad=u.IDubigeo        
 Where NuDocum = @NuDocum  and d.IDTipoDoc =@IDTipoDoc        


