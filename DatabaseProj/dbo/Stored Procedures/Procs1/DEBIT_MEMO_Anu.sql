﻿Create Procedure dbo.DEBIT_MEMO_Anu
@IDDebitMemo char(10),
@UserMod char(4)
As
 Set NoCount On  
 Update [dbo].[DEBIT_MEMO]   
	set IDEstado = 'AN',
		UserMod = @UserMod,
		FecMod = GETDATE()
 where [IDDebitMemo] = @IDDebitMemo  
