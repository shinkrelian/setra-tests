﻿Create Procedure dbo.DOCUMENTO_DET_TEXTO_Del
@NuDocum char(10),
@IDTipoDoc char(3),
@NuDetalle tinyint
As
Set NoCount On
Delete from DOCUMENTO_DET_TEXTO WHERE NuDocum = @NuDocum and IDTipoDoc = @IDTipoDoc 
 and NuDetalle = @NuDetalle
