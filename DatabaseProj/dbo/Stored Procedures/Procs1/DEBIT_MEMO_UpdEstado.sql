﻿--HLF-20160321-FlTransCurso = Case When @IDEstado='TC' Then 1 Else FlTransCurso End
CREATE Procedure [dbo].[DEBIT_MEMO_UpdEstado]
@IDDebitMemo char(10),  
@IDEstado char(2),
@UserMod char(4)  
As  
 Set NoCount On  
 Update DEBIT_MEMO   
  Set IDEstado = @IDEstado,
   FlTransCurso = Case When @IDEstado='TC' Then 1 Else FlTransCurso End,
   UserMod = @UserMod,  
   FecMod = GetDate()  
 Where IDDebitMemo = @IDDebitMemo  

