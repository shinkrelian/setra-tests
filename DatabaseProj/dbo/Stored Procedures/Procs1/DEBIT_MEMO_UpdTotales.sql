﻿CREATE Procedure dbo.DEBIT_MEMO_UpdTotales  
 @IDDebitMemo char(10),  
 @SubTotal numeric(8,2),  
 @TotalIGV numeric(8,2),  
 @Total numeric(8,2),  
 @UserMod char(4)  
As  
 Set NoCount On  
   
 Update DEBIT_MEMO Set   
 SubTotal=@SubTotal,  
 TotalIGV=@TotalIGV,  
 Total=@Total,  
 Saldo = @Total,
 UserMod=@UserMod,  
 FecMod=getdate()  
 Where IDDebitMemo=@IDDebitMemo  
