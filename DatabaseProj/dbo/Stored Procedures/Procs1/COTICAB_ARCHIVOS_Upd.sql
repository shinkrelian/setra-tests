﻿create PROCEDURE COTICAB_ARCHIVOS_Upd
	@IDArchivo int,
	@IDCAB int=0,
	@Nombre varchar(200)='',
	@RutaOrigen varchar(200)='',
	@RutaDestino varchar(200)='',
	@CoTipo char(3)='',
	@FlCopiado bit=0,
	@CoUsuarioCreacion char(4)='',
	@UserMod char(4)=''
AS
BEGIN
	Update dbo.COTICAB_ARCHIVOS
	Set
 		Nombre = @Nombre,
 		RutaOrigen = @RutaOrigen,
 		RutaDestino = @RutaDestino,
 		CoTipo = @CoTipo,
 		FlCopiado = @FlCopiado,
 		UserMod = @UserMod,
 		FecMod = getdate()
 	Where 
		IDArchivo = @IDArchivo
		AND IDCAB = @IDCAB
END;
