﻿CREATE proc [dbo].[MADESAYUNOS_Sel_List] 
@IDDesayuno char(2),
@Descripcion varchar(50)
as
	Set NoCount On
	
	select IDDesayuno,Descripcion from MADESAYUNOS
	Where (Descripcion like '%'+@Descripcion+'%' Or LTRIM(rtrim(@Descripcion))='')
	And (IDDesayuno=@IDDesayuno Or LTRIM(RTRIM(@IDDesayuno))='')  
	Order by IDDesayuno
