﻿
CREATE Procedure [dbo].[CORREOFILE_Sel_ListxProveedorBExPEdido_ExistsOutput]
	@NuPedInt	int,
	@CoUsrPrvDe	char(6),
	@CoArea char(2),
	@pExists bit output
As	
	Set NoCount On
		
	Set @pExists = 0

	If Exists(
		Select c.NuCorreo
		From .BDCORREOSETRA..CORREOFILE c
		Where IDCab in (Select IDCab From COTICAB where nupedint=@nupedint) And   
		(CoUsrPrvDe=@CoUsrPrvDe)  And
		CoTipoBandeja='BE'
		And (EXISTS(Select dc1.CoUsrIntPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 Inner Join MAUSUARIOS u1 On dc1.CoUsrIntPara=u1.IDUsuario 
			And u1.IdArea=@CoArea And dc1.NuCorreo=c.NuCorreo) ))
		Set @pExists = 1

