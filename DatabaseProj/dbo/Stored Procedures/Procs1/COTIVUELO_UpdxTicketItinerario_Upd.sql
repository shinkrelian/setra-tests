﻿Create Procedure dbo.COTIVUELO_UpdxTicketItinerario_Upd
@IDTrans int,
@RutaD char(6),
@RutaO char(6),
@Ruta varchar(100),
@FecEmision smalldatetime,
@FechaSalida smalldatetime,
@Salida varchar(20),
@Llegada varchar(20),
@IDLineaAerea char(6),
@NumVuelo varchar(20),
@NombrePax varchar(100),
@TarifaBase numeric(10,2),
@CodReserva varchar(20)
As
Set NoCount On
Update COTIVUELOS
	Set RutaD=@RutaD,
		RutaO=@RutaO,
		Ruta= @Ruta,
		Fec_Emision=@FecEmision,
		IdLineaA=@IDLineaAerea,
		Vuelo=@NumVuelo,
		Tarifa=@TarifaBase,
		Fec_Salida=cast(convert(char(10),@FechaSalida,103) as smalldatetime),
		Salida=@Salida,
		Llegada=CONVERT(char(5),@Llegada,108),
		CodReserva=Case When Ltrim(Rtrim(@CodReserva))='' Then Null else @CodReserva End
Where ID=@IDTrans
