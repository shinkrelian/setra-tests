﻿CREATE Procedure [dbo].[COTIDET_Upd_NuevoIDServicio]
	@IDServicio char(8),		
	@IDServicioNue char(8),
	@UserMod char(4)
As
	Set NoCount On
	
	Update COTIDET
		SET IDServicio=@IDServicioNue, FecMod=GETDATE(), UserMod=@UserMod
	Where IDServicio=@IDServicio
