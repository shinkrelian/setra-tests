﻿

CREATE Procedure [dbo].[DESTINATARIOSCORREOFILE_SelExiste]
	@NuCorreo	int,
	@NoCuentaPara	varchar(150),
	@pbExists bit Output  
As
	Set Nocount On
	
	Set @pbExists = 0  
   
	If Exists(Select NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE 
		Where NuCorreo=@NuCorreo And NoCuentaPara=@NoCuentaPara)  
		
		Set @pbExists = 1  
	
