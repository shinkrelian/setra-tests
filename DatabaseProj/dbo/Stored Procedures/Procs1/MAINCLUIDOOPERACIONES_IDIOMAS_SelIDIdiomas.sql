﻿
Create Procedure dbo.MAINCLUIDOOPERACIONES_IDIOMAS_SelIDIdiomas
As
	Set NoCount On
	select distinct id.Cultura ,inc.IDIdioma 
	from MAINCLUIDOOPERACIONES_IDIOMAS inc Left Join MAIDIOMAS id On inc.IDIdioma = id.IDidioma
	order by inc.IDIdioma
