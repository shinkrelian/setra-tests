﻿CREATE Procedure dbo.DEBIT_MEMO_SelxIDDebitMemoResumen  
@IDDebitMemoResumen char(10)  
As  
 Set NoCount On  
 Select dm.IDDebitMemo,'REF  : '+dm.Titulo+' DEBIT MEMO : '+SUBSTRING(dm.IDDebitMemo,0,9)+'-'+SUBSTRING(dm.IDDebitMemo,9,9)+CHAR(10)+  
     'FILE : '+ SUBSTRING(dm.IDDebitMemo,0,9) +' IN : ' +CONVERT(Char(10),dm.FechaIn,103) + ' OUT : ' +CONVERT(Char(10),dm.FechaOut,103) + ' N°PAX :' + cast(dm.Pax as varchar(8))  
     As Descripcion,  
     (select Isnull(SUM(Total),0) from DEBIT_MEMO_DET dmd where dmd.IDDebitMemo = dm.IDDebitMemo) as Total,c.IDCliente  
 from DEBIT_MEMO dm Left Join COTICAB c on c.IDCab = dm.IDCab and (dm.IDEstado = 'PD' Or dm.IDEstado ='PP' Or dm.IDEstado = 'PG' and dm.IDDebitMemoResumen is null)  
 where dm.IDDebitMemoResumen = @IDDebitMemoResumen  
 order by CAST(dm.FechaIn as smalldatetime)  
