﻿
Create Procedure dbo.DOCUMENTOBSP_Del
	@NuDocumBSP char(11),
	@CoTipDocBSP char(3)
As
	Set Nocount On
	
	Delete From DOCUMENTOBSP	
	Where NuDocumBSP=@NuDocumBSP and CoTipDocBSP=@CoTipDocBSP

