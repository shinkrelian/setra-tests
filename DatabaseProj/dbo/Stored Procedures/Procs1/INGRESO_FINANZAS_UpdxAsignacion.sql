﻿Create Procedure dbo.INGRESO_FINANZAS_UpdxAsignacion
@NuIngreso int,
@TxObservacion varchar(max),
@SsOrdenado numeric(10,2),
@SsGastosTransferencia numeric(10,2),
@SsRecibido numeric(10,2),
@SsBanco numeric(10,2),
@SsNeto numeric(10,2),
@UserMod char(4)
as
Begin
	Update INGRESO_FINANZAS 
		set TxObservacion = case when ltrim(rtrim(@TxObservacion))='' then null else @TxObservacion End,
			SsOrdenado = case when @SsOrdenado = 0 then Null else @SsOrdenado End,
			SsGastosTransferencia = case when @SsGastosTransferencia = 0 then Null Else @SsGastosTransferencia End,
			SsRecibido = @SsRecibido,
			SsBanco = @SsBanco,
			SsNeto = @SsNeto,
			UserMod = @UserMod,
			FecMod = GetDate()
	where NuIngreso = @NuIngreso
End
