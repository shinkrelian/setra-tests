﻿
Create Procedure dbo.MAMONEDAS_SelxPais
	@CoPais char(6)
As
	Set Nocount on
	Select m.IDMoneda, m.Descripcion From MAMONEDAS m Where IDMoneda='USD'
	UNION
	Select m.IDMoneda, m.Descripcion From MAMONEDAS m Inner Join MAUBIGEO u On m.IDMoneda=u.CoMoneda
	And u.IDubigeo=@CoPais
	WHERE m.Activo='A'
	
