﻿

--HLF-20150903-CoSAP,
--HLF-20150910-And ISNULL(CoSAP,'')<>''
--HLF-20151005-,IDMoneda,SsTotalDocumUSD,d.CoTipoVenta, isnull(d.idcliente,c.IDCliente) as CoCliente,
--isnull(d.IDCab,0) as IDCab,d.FeDocum
CREATE Procedure dbo.DOCUMENTO_SelxCoDebitMemoAnticipo
	@CoDebitMemoAnticipo char(10) 
As
	Set NoCount On

	Select d.IDTipoDoc,d.NuDocum,d.CoSAP,d.IDMoneda,d.SsTotalDocumUSD,
	d.CoTipoVenta, isnull(d.idcliente,c.IDCliente) as CoCliente,
	isnull(d.IDCab,0) as IDCab,d.FeDocum
	From DOCUMENTO d Left Join COTICAB c On d.IDCab = c.IDCAB 
	Where CoDebitMemoAnticipo=@CoDebitMemoAnticipo 
		And CoEstado<>'AN' And ISNULL(CoSAP,'')<>''



