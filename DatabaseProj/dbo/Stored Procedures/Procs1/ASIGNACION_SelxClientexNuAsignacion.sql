﻿

--JRF-20131121-Subdividir IDFile -Correlativo (DebitMemo)        
--JRF-20131128-En caso quede un monto x asignar mostrar los debitMemosRestantes.      
--JRF-20140206-Se agrego el IDCab  
--HLF-20150512-	Case When dm.IDCliente=@CoClienteVtasAdicAPT Then
--isnull((Select Top 1 isnull(NuDocum,'') From VENTA_ADICIONAL_DET Where IDDebitMemo=dm.IDDebitMemo),'')
--HLF-20150904-,ISNULL(idm.CoSAP,'') as CoSAP
--HLF-20150922-ISNULL(idm.SsComisionBanco,0) as SsComisionBanco,
--HLF-20150928-and @ImportexAsignar <> 0    ) as X  
CREATE Procedure dbo.ASIGNACION_SelxClientexNuAsignacion          
	@IDCliente varchar(6),          
	@NuIngreso int,      
	@NuAsignacion int          
As          
         
	Set NoCount On          
	Declare @CoClienteVtasAdicAPT char(6)=(Select CoClienteVtasAdicAPT From PARAMETRO)

	Declare @ImportexAsignar numeric(10,2) = (select 
	Case When IDMoneda = 'SOL' Then SsRecibido / ssTC  else SsRecibido End
	- ISNULL((select SUM(SsPago) from INGRESO_DEBIT_MEMO where SsPago > 0 and NuIngreso = @NuIngreso),0) -    
	ISNULL((select SUM(SsPago) from INGRESO_DEBIT_MEMO where SsPago < 0 and NuIngreso = @NuIngreso) * -1,0)        
	from INGRESO_FINANZAS where NuIngreso = @NuIngreso)      
       
	Select * from (  
	select Isnull(dm.IDCab,0) as IDCab,dm.IDDebitMemo as IDDebitMemoCod,      
	Case When dm.IDTipo = 'I' Then SUBSTRING(dm.IDDebitMemo,1,8)+'-'+SUBSTRING(dm.IDDebitMemo,9,10)       
	Else SUBSTRING(dm.IDDebitMemo,1,3)+'-'+SUBSTRING(dm.IDDebitMemo,4,10) End      
	as IDDebitMemo,      
	Case When dm.IDCliente=@CoClienteVtasAdicAPT Then
		isnull((Select Top 1 isnull(NuDocum,'') From VENTA_ADICIONAL_DET Where IDDebitMemo=dm.IDDebitMemo),'')
	Else
		''
	End as DocVentasAdicAPT,
	dm.IDEstado,dm.IDTipo,dm.Titulo,Case When dm.IDTipo = 'I' Then Convert(Char(10),dm.Fecha,103) else '' End as FechaCobro,          
	Case When dm.IDTipo = 'I' Then CONVERT(Char(10),dm.FecVencim,103) Else '' End as FechaVencimiento,          
	Case When dm.IDTipo = 'I' Then CONVERT(Char(10),dm.FechaIn,103) Else '' End as FechaIN,      
	Case When dm.IDTipo = 'I' Then CONVERT(Char(10),dm.FechaOut,103) else '' End as FechaOUT,          
	dm.Total,idm.SsPago+idm.SsGastosTransferencia+idm.SsSaldoNuevo as Saldo,          
	1 as ExistePago,          
	Isnull(idm.SsPago,0) as SsPago ,Isnull(idm.SsGastosTransferencia,0) as GastoTransfer ,          
	Case When CAST(SUBSTRING(dm.IDDebitMemo,9,10) as tinyint) =0 then          
	Isnull((select Sum(Total*NroPax) from COTIDET cd where cd.IDCAB = dm.IDCab           
	and (cd.IDServicio ='LIM00234' Or cd.IDServicio = 'LIM00461')),0) else 0 End          
	as GastoTransferCotiz,          
	ISNULL(idm.SsComisionBanco,0) as SsComisionBanco,
	idm.SsSaldoNuevo as SaldoNuevo,          
	Case When a.NuAsignacion Is Null Or dm.IDEstado = 'PP' then 'N' else 'M' End As Accion,          
	Isnull(dm.IDDebitMemoResumen,'') as IDDebitMemoResumen        ,  
	Case When dm.IDTipo = 'S' Or dm.IDTipo = 'C' then 2 Else 1 End as Ord  
	,ISNULL(idm.CoSAP,'') as CoSAP
	from INGRESO_DEBIT_MEMO idm Left Join DEBIT_MEMO dm On idm.IDDebitMemo = dm.IDDebitMemo          
	Left Join ASIGNACION a On idm.NuAsignacion = a.NuAsignacion           
	Left Join COTICAB c On dm.IDCab = c.IDCAB          
	Where (dm.IDTipo <> 'R') And dm.IDCliente = @IDCLiente and dm.IDEstado <> 'AN'       
	and idm.NuAsignacion = @NuAsignacion and idm.NuIngreso = @NuIngreso      
	Union      
	select Isnull(dm.IDCab,0) as IDCab,dm.IDDebitMemo as IDDebitMemoCod,      
	Case When dm.IDTipo = 'I' Then SUBSTRING(dm.IDDebitMemo,1,8)+'-'+SUBSTRING(dm.IDDebitMemo,9,10) Else      
	SUBSTRING(dm.IDDebitMemo,1,3)+'-'+SUBSTRING(dm.IDDebitMemo,4,10) End as IDDebitMemo,      
	Case When dm.IDCliente=@CoClienteVtasAdicAPT Then
		isnull((Select Top 1 isnull(NuDocum,'') From VENTA_ADICIONAL_DET Where IDDebitMemo=dm.IDDebitMemo),'')
	Else
		''
	End as DocVentasAdicAPT,
	dm.IDEstado,dm.IDTipo,dm.Titulo,      
	Case When dm.IDTipo = 'I' Then CONVERT(Char(10),dm.Fecha,103)else '' End as FechaCobro,      
	Case When dm.IDTipo = 'I' Then CONVERT(Char(10),dm.FecVencim,103)else '' End as FechaVencimiento,          
	Case When dm.IDTipo = 'I' Then CONVERT(Char(10),dm.FechaIn,103)else '' End as FechaIN,      
	Case When dm.IDTipo = 'I' Then CONVERT(Char(10),dm.FechaOut,103)else '' End as FechaOUT,      
	dm.Total,dm.Total - Isnull((select SUM(SsPago) from INGRESO_DEBIT_MEMO idm2 where idm2.IDDebitMemo = dm.IDDebitMemo and idm2.NuAsignacion<>@NuAsignacion),0) as Saldo,0 as ExistePago,0 as SsPago,0 as GastoTransfer,      
	Case When CAST(SUBSTRING(dm.IDDebitMemo,9,10) as tinyint) =0 then          
	Isnull((select Sum(Total*NroPax) from COTIDET cd where cd.IDCAB = dm.IDCab           
	and (cd.IDServicio ='LIM00234' Or cd.IDServicio = 'LIM00461')),0) else 0 End          
	as GastoTransferCotiz,      
	0 as SsComisionBanco,
	dm.Total - Isnull((select SUM(SsPago) from INGRESO_DEBIT_MEMO idm2 where idm2.IDDebitMemo = dm.IDDebitMemo and idm2.NuAsignacion<>@NuAsignacion),0) as SaldoNuevo,'N' as Accion,Isnull(dm.IDDebitMemoResumen,'') as IDDebitMemoResumen    ,  
	Case When dm.IDTipo = 'S' Or dm.IDTipo = 'C' then 2 Else 1 End as Ord  
	,'' as CoSAP
	from DEBIT_MEMO dm      
	where dm.IDCliente = @IDCliente and Not Exists(select IDDebitMemo from INGRESO_DEBIT_MEMO idm where idm.IDDebitMemo = dm.IDDebitMemo and idm.NuIngreso = @NuIngreso)      
	and dm.IDEstado <> 'AN' and dm.IDTipo <> 'R' and dm.Saldo <> 0 
	--and @ImportexAsignar > 0    ) as X  
	and @ImportexAsignar <> 0    ) as X  
	Order by X.Ord  



