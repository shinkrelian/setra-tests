﻿--JRF-20130622-Colocandole el Total del DEBITMEMO        
--JRF-20130704-Replicando cambios de Nuevos Campos      
--JRF-20130829-Agregar el campo de IDCliente  
CREATE Procedure DBO.DEBIT_MEMO_GenPDFIndividual          
@IDCab int,          
@IDDetMemo char(10)          
As          
 Set NoCount On          
 select           
 c.IDFile,SUBSTRING(dm.IDDebitMemo,9,9) As Correlativo,dm.FecVencim          
 ,upper(dm.Cliente) As Client,isnull(cl.NumIdentidad,'') As RucDI,          
 dm.Titulo As Ref,    
 upper(cl.Direccion) + Isnull(' - '+upper(cl.CodPostal),'') + Isnull(' - '+upper(cl.Ciudad),'') + Isnull(' - '+ub.Descripcion,'') As Direcc,          
 Convert(char(10),dm.FechaIn,103) As FechaIn, --(select Convert(char(10),Min(Dia),103) from COTIDET where IDCab = @IDCab) As FechaIn,          
 Convert(char(10),dm.FechaOut,103) As FechaOut, --(select Convert(char(10),Max(Dia),103) from COTIDET where IDCab = @IDCab) As FechaOut,          
 dm.Pax as NroPax,bc.NroCuenta          
 ,isnull(bc.SWIFT,'') As Swift,bc.Descripcion As NomBanc,          
 isnull(bc.Direccion,'') As Direccion,          
 (select SUM(dmd2.Total) from DEBIT_MEMO_DET dmd2 Left Join DEBIT_MEMO dm2 On dm2.IDDebitMemo = dmd2.IDDebitMemo        
 where dmd2.IDDebitMemo = @IDDetMemo )As TotalDebitMemo,        
 dmd.IDDebitMemoDet As Ord, dmd.Descripcion,dmd.Total          
 from DEBIT_MEMO dm  Left Join COTICAB c On dm.IDCab = c.IDCAB          
 Left Join MACLIENTES cl On dm.IDCliente  = cl.IDCliente          
 Left Join DEBIT_MEMO_DET dmd On dm.IDDebitMemo = dmd.IDDebitMemo          
 Left Join MABANCOS bc On c.IDBanco = bc.IDBanco          
 Left Join MAUBIGEO ub On cl.IDCiudad = ub.IDubigeo    
 Where dm.IDCab = @IDCab and dm.IDDebitMemo = @IDDetMemo          
 And Exists(select IDDebitMemoDet from DEBIT_MEMO_DET dmd2 where dmd2.IDDebitMemo = @IDDetMemo)          
 order by dmd.IDDebitMemoDet          
