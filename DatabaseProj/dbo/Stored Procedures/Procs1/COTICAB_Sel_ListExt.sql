﻿--JRF-20160126-Agregar NumAPT
CREATE Procedure dbo.COTICAB_Sel_ListExt
@IDFile varchar(8),
@IDCliente char(6),
@Titulo varchar(100),
@fecin_desde varchar(10),
@fecin_hasta varchar(10),
@fecout_desde varchar(10),
@fecout_hasta varchar(10)
As
Set NoCount On
Set dateformat mdy
if Ltrim(Rtrim(@fecin_desde))='' set @fecin_desde = '01/01/1900'
if Ltrim(Rtrim(@fecin_hasta))='' set @fecin_hasta = '01/01/1900'
if Ltrim(Rtrim(@fecout_desde))='' set @fecout_desde = '01/01/1900'
if Ltrim(Rtrim(@fecout_hasta))='' set @fecout_hasta = '01/01/1900'

select IDFile as NroFile,c.Titulo,CONVERT(varchar(10),FecInicio,103) as FechaIn,CONVERT(varchar(10),FechaOut,103) as FechaOut,
	   cp.IDPax,cp.Titulo as TituloPax,cp.TxTituloAcademico as TitAcademico,Apellidos,Nombres,
	   --IsNull(convert(varchar(10),cp.FecNacimiento,103),'') as FecNacimiento,
	   REPLICATE('0',2-Len(Cast(MONTH(CP.FecNacimiento) as varchar(2))))+ Cast(MONTH(CP.FecNacimiento) as varchar(2))+'/'+
	   REPLICATE('0',2-Len(Cast(DAY(CP.FecNacimiento) as varchar(2))))+ Cast(DAY(CP.FecNacimiento) as varchar(2))+'/'+
	   Cast(YEAR(CP.FecNacimiento) as varchar(4))
	   As FecNacimiento,
	   --cp.FecNacimiento as FecNacimiento,
	   Cast(IsNull(Peso,0) as numeric(8,2)) as Peso,IsNull(cp.ObservEspecial,'') as ObservEspecial,
	   cp.IDIdentidad,IsNull(cp.NumIdentidad,'') as NumIdentidad,ti.Descripcion as DescIdentidad,
	   cp.IDNacionalidad,IsNull(ub.Descripcion,'') as DescNacionalidad,
	   cp.IDPaisResidencia,IsNull(ub2.Descripcion,'') as DescPaisResidencia,
	   Case When c.IDCliente In ('000054') Then IsNull(cp.NumberAPT,'') Else cp.NumberAPT End as NumberAPT
from COTIPAX cp Inner Join COTICAB c On c.IDCAB=cp.IDCab
Left Join MATIPOIDENT ti On cp.IDIdentidad=ti.IDIdentidad
Left Join MAUBIGEO ub On cp.IDNacionalidad = ub.IDubigeo
Left Join MAUBIGEO ub2 On cp.IDPaisResidencia = ub2.IDubigeo
Where FlHistorico = 0 And IDCliente=@IDCliente
And (Ltrim(Rtrim(@Titulo))='' Or c.Titulo Like '%'+@Titulo+'%')
And (Ltrim(Rtrim(@IDFile))='' Or c.IDFile Like '%'+@IDFile+'%')
And (FecInicio between @fecin_desde and @fecin_hasta Or @fecin_desde='01/01/1900')
And ((FechaOut between @fecout_desde and @fecout_hasta Or @fecout_desde='01/01/1900') And FechaOut > GETDATE())
And c.Estado='A' --And c.IDFile=''
Order By Cast(c.IDFile As int),cp.Orden asc
