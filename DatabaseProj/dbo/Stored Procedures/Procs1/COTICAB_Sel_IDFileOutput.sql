﻿CREATE Procedure [dbo].[COTICAB_Sel_IDFileOutput]
	@IDCab	int,
	@pIDFile varchar(8)	Output
As

	Set NoCount On
	
	Select @pIDFile=IsNull(IDFile,'') From COTICAB Where IDCab=@IDCab
