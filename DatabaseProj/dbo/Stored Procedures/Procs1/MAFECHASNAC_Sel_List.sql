﻿--JRF-20140129-Considerar el nuevo campo NoConsideraAnio
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-...And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=MAAEROPUERTO.CoUbigeo)=@CoPais)
--JRF-20151023-.., FlNoConsiderarAnio
CREATE Procedure [dbo].[MAFECHASNAC_Sel_List]      
 @Fecha smalldatetime,      
 @IDUbigeo char(6),
 @CoPais char(6)
As      
 Set NoCount On      
     
 Select Case When NoConsideraAnio = 1 then CONVERT(Char(5),Fecha,103) Else CONVERT(char(10),Fecha,103) 
 End as Fecha, Descripcion, IDUbigeo, NoConsideraAnio As FlNoConsiderarAnio
 From MAFECHASNAC      
 Where (Fecha=@Fecha Or @Fecha='01/01/1900') And       
    (IDUbigeo=@IDUbigeo Or LTRIM(RTRIM(@IDUbigeo))='')  
And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=MAFECHASNAC.idUbigeo)=@CoPais )
 Order By NoConsideraAnio asc,Fecha desc
