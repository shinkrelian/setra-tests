﻿Create Procedure dbo.DOCUMENTO_DET_TEXTO_Upd
@NuDocum char(10),
@IDTipoDoc char(3),
@NuDetalle tinyint,
@NoTexto varchar(Max),
@SsTotal numeric(8,2),
@UserMod char(4)
As
Set NoCount On
UPDATE [dbo].[DOCUMENTO_DET_TEXTO]
   SET [NoTexto] = @NoTexto
      ,[SsTotal] = @SsTotal
      ,[UserMod] = @UserMod
      ,[FecMod] = GetDate()
 WHERE NuDocum = @NuDocum and IDTipoDoc = @IDTipoDoc 
 and NuDetalle = @NuDetalle
