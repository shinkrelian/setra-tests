﻿Create Procedure dbo.COTICAB_COSTOSTC_Sel_Existe
@IDCab int,
@CoTipoHon char(3),
@pExiste bit output
As
Set NoCount On
Set @pExiste = 0
Select @pExiste=1 from COTICAB_COSTOSTC
where IDCab=@IDCab and CoTipoHon=@CoTipoHon
