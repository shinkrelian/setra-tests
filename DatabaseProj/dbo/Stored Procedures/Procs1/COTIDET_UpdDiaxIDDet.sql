﻿Create Procedure dbo.COTIDET_UpdDiaxIDDet
@IDdet int,
@Dia smalldatetime,
@UserMod char(4)
As
	Set NoCount On
	UPDATE COTIDET
		Set Dia = @Dia,
			UserMod = @UserMod,
			FecMod = GETDATE()
	Where IDDET = @IDdet
