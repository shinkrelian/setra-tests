﻿Create Procedure dbo.Correlativo_UpdCorrelativoDoc
@IDTipoDoc char(3),
@CoSerie char(3)
as
Set NoCount On
Update Correlativo
	--set Correlativo = Correlativo-1
	set Correlativo = (select Isnull(Max(CAST(right(NuDocum,7) as int)),0) from DOCUMENTO 
					   where IDTipoDoc = Correlativo.IDTipoDoc and LEFT(NuDocum,3)=Correlativo.CoSerie)
where Tabla = 'DOCUMENTO' and IDTipoDoc = @IDTipoDoc and CoSerie = @CoSerie
