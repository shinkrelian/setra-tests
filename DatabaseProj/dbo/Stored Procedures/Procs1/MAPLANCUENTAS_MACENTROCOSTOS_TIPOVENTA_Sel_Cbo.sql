﻿--create
CREATE Procedure dbo.MAPLANCUENTAS_MACENTROCOSTOS_TIPOVENTA_Sel_Cbo
@CoTipoVenta char(2)='',
@CoCeCos char(6)=''--,
--@FlProvAdmin Char(1)=''
as
select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion,
 TxDefinicion=''
from MAPLANCUENTAS_MACENTROCOSTOS_TIPOVENTA pc
inner join MAPLANCUENTAS c on pc.CtaContable=c.CtaContable
where
(pc.CoCeCos=@CoCeCos Or LTRIM(rtrim(@CoCeCos))='')  
and (pc.CoTipoVenta=@CoTipoVenta or @CoTipoVenta='') 
and c.FlFacturacion=1
 Order by c.CtaContable+ ' - ' + c.Descripcion

