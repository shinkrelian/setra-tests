﻿Create Procedure MAMODOSERVICIO_Sel_Lvw
@CodModoServ char(2),
@Descripcion varchar(30)
As
	Set NoCount On
	
	Select Descripcion
	From MAMODOSERVICIO
	Where (Descripcion Like '%'+@Descripcion+'%')
	And  (CodModoServ <> @CodModoServ Or LTRIM(rtrim(@CodModoServ))='')
	Order By Descripcion
