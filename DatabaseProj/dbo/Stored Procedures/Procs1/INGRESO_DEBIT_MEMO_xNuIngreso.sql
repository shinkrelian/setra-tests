﻿
--HLF-20150916-dbo.FnTipoDeCambioVtaUltimoxFecha(ifi.FeAcreditada) as U_SYP_TC,
--Case When U_SYP_TC=0 then dbo.FnTipoDeCambioVtaUltimoxFecha(getdate()) else U_SYP_TC end as U_SYP_TC,... AS DocRate
--HLF-20150917-TCHoy
--Case When IDCliente='000054' ...
--Declare @U_SYP_NUMOPER char(10)=(Select isnull((se...
--HLF-20150922-BankChargeAmount
--ido.SsPago-ido.SsComisionBanco as TransferSum, as TransferSumFc
--HLF-20150925abs(
CREATE Procedure dbo.INGRESO_DEBIT_MEMO_xNuIngreso
@NuIngreso int,
@IDDebitMemo char(10)
As
	Set NoCount On
	Declare @U_SYP_NUMOPER char(10)=(Select isnull((select max(cosap) from INGRESO_DEBIT_MEMO where IDDebitMemo=@IDDebitMemo and isnull(CoSAP,0)<>0),@IDDebitMemo))

	Select IDCAB,IDFile,DocDate,TaxDate,DueDate,CardCodeSAP,DocType,TransferAccount,
	
	dbo.FnCambioMoneda(TransferSum,CoMonedaSetra,'SOL',U_SYP_TC)-- - dbo.FnCambioMoneda(BankChargeAmount,CoMonedaSetra,'SOL',U_SYP_TC)
	as TransferSum,	
	dbo.FnCambioMoneda(TransferSumFc,CoMonedaSetra,'USD',U_SYP_TC) ---dbo.FnCambioMoneda(BankChargeAmount,CoMonedaSetra,'USD',U_SYP_TC)
	as TransferSumFc,
	BankChargeAmount,
	BridgeAccount,DocCurrency,--DocRate,
	Case When U_SYP_TC=0 then TCHoy else U_SYP_TC end as DocRate,
	U_SYP_MPPG,U_SYP_NUMOPER,	
	Case When U_SYP_TC=0 then TCHoy else U_SYP_TC end as U_SYP_TC,
	U_SYP_TIPOCOB,U_SYP_NFILE,TransferReference,
	Case When IDCliente='000054' Then--APT 
		'13121002'
	Else
		Case When CoMonedaSetra='SOL' Then
			'12121001'
		Else
			'12122001'
		End
	End as ControlAccount
	From
		(
		Select c.IDCAB,c.IDFile,ifi.FeAcreditada as DocDate,ifi.FeAcreditada as TaxDate,ifi.FeAcreditada as DueDate,cli.CardCodeSAP,
		0 as DocType,dbo.FnDevuelveTransferAccount(ifi.IDBanco,ifi.IDMoneda) as TransferAccount,
		--ABS(ido.SsPago-isnull(ido.SsComisionBanco,0)) as TransferSum,
		(ido.SsPago-isnull(ido.SsComisionBanco,0)) as TransferSum,
		--ABS(dbo.FnCambioMoneda(ido.SsPago-isnull(ido.SsComisionBanco,0),ifi.IDMoneda,'USD',IFI.ssTC)) As TransferSumFc,
		(dbo.FnCambioMoneda(ido.SsPago-isnull(ido.SsComisionBanco,0),ifi.IDMoneda,'USD',IFI.ssTC)) As TransferSumFc,
		ido.SsGastosTransferencia+isnull(ido.SsComisionBanco,0) as BankChargeAmount,
		'10391001' As BridgeAccount,
		ifi.IDMoneda As CoMonedaSetra,
		M.CoSAP As DocCurrency,
		--IsNull(ifi.ssTC,(select ctc.SsTipCam from COTICAB_TIPOSCAMBIO ctc where ctc.CoMoneda='SOL' And ctc.IDCab=dm.IDCab)) as DocRate,
		fp.CoSUNAT_SAP as U_SYP_MPPG,
		
		--dm.IDDebitMemo as U_SYP_NUMOPER,
		--right(dm.IDDebitMemo,2) + Cast(ido.NuIngreso as varchar(8)) as U_SYP_NUMOPER,
		@U_SYP_NUMOPER as U_SYP_NUMOPER,
		
		--IsNull(ifi.ssTC,(select ctc.SsTipCam from COTICAB_TIPOSCAMBIO ctc where ctc.CoMoneda='SOL' And ctc.IDCab=dm.IDCab)) as U_SYP_TC,
		dbo.FnTipoDeCambioVtaUltimoxFecha(ifi.FeAcreditada) as U_SYP_TC,
		dbo.FnTipoDeCambioVtaUltimoxFecha(getdate()) as TCHoy,
		--1 as U_SYP_TIPOCOB,
		Case When ifi.IDMoneda in (SELECT DISTINCT IDMoneda From DOCUMENTO where IDCab=dm.IDCab) then 0 else 1 end as U_SYP_TIPOCOB,
		c.IDFile as U_SYP_NFILE, isnull(ifi.CoOperacionBan,'') as TransferReference,
		cli.IDCliente
		From INGRESO_DEBIT_MEMO iDo Inner Join INGRESO_FINANZAS iFi On iDo.NuIngreso=iFi.NuIngreso
		Inner Join MACLIENTES cLi On iFi.IDCliente=cLi.IDCliente
		Inner Join DEBIT_MEMO dm On ido.IDDebitMemo=dm.IDDebitMemo
		Inner Join MAFORMASPAGO fp On ifi.IDFormaPago=fp.IdFor
		Inner Join COTICAB c On dm.IDCab=c.IDCAB And c.Estado <> 'X'
		LEFT JOIN MAMONEDAS M ON IFI.IDMoneda=M.IDMoneda		
		Where ifi.NuIngreso=@NuIngreso And CardCodeSAP is not Null
			and ido.IDDebitMemo=@IDDebitMemo
		) as X
	Order By IDFile


