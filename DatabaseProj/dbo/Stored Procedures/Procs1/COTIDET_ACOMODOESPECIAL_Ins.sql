﻿
CREATE PROCEDURE [COTIDET_ACOMODOESPECIAL_Ins]
	@IDDet INT,
	@CoCapacidad CHAR(2),  
	@QtPax INT,
	@UserMod CHAR(4)
AS
BEGIN
-- =====================================================================
-- Author	   : Dony Tinoco
-- Create date : 02.12.2013
-- Description : SP que inserta el acomodo especial para la cotizacion
-- =====================================================================
	SET NOCOUNT ON
   
	INSERT INTO COTIDET_ACOMODOESPECIAL (
		IDDet,   
		CoCapacidad, 
		QtPax,
		UserMod)  
	VALUES (
		@IDDet,
		@CoCapacidad,
		@QtPax,
		@UserMod)  
END
