﻿Create Procedure dbo.MACORREOSBIBLIAPROVEEDOR_Ins
@IDProveedor char(6),
@Correo varchar(150),
@Descripcion varchar(Max),
@UserMod char(4)
As
Set NoCount On
Insert into MACORREOSBIBLIAPROVEEDOR(IDProveedor,Correo,Descripcion,UserMod,FecMod)
values(@IDProveedor,@Correo,
	   Case when ltrim(rtrim(@Descripcion))='' then Null else @Descripcion End,@UserMod,getdate())
