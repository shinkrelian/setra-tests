﻿--HLF-20150206-and Estado='A'
--2do Update
CREATE Procedure dbo.COTICAB_UpdPoConcretVtaxFechaInJOB  
 @UserMod char(4)   
 As  
 Set Nocount On  
 --Set dateformat dmy
   
	Update COTICAB Set PoConcretVta=100,FlPoConcretAutoUpd=1,  
	FecMod=GETDATE(), UserMod=@UserMod    
	Where DATEDIFF(d,GETDATE(),FecInicio) = 15 
	and Estado='A'

	Update COTICAB set PoConcretVta=100, FlPoConcretAutoUpd=1,  UserMod=@UserMod, FecMod=GETDATE()
	Where DATEDIFF(d,GETDATE(),FecInicio) < 15 
	and PoConcretVta<>100 and Estado='A'
