﻿Create Procedure [dbo].[COTIDET_ACOMODOESPECIAL_SelxIDDetxCapacidad]
 @IDDet int,
 @Capacidad tinyint,
 @EsMatrimonial bit
As      
 Set Nocount On      
 Select cdd.IDCAB,cad.IDPax,cd.CoCapacidad, cd.QtPax, cap.QtCapacidad, cap.NoCapacidad,cap.FlMatrimonial ,cap.FlGuide
 From COTIDET_ACOMODOESPECIAL cd       
 Inner Join CAPACIDAD_HABITAC cap On cd.CoCapacidad=cap.CoCapacidad      
 Left Join COTIDET cdd On cd.IDDet = cdd.IDDET  
 Inner Join COTIDET_ACOMODOESPECIAL_DISTRIBUC cad On cd.IDDet=cad.IDDet And cd.CoCapacidad=cad.CoCapacidad
 Where cd.IDDet=@IDDet And cap.QtCapacidad=@Capacidad And cap.FlMatrimonial=@EsMatrimonial
 Order by cap.FlMatrimonial asc,cd.CoCapacidad ,cd.QtPax
