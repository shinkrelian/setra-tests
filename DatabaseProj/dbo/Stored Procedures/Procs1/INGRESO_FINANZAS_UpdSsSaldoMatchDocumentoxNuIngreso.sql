﻿
Create Procedure dbo.INGRESO_FINANZAS_UpdSsSaldoMatchDocumentoxNuIngreso	
	@NuIngreso int,
	@UserMod char(4)
As
	Set Nocount On

	Update INGRESO_FINANZAS 
	Set SsSaldoMatchDocumento=SsRecibido, UserMod=@UserMod, FecMod=GETDATE()
	Where NuIngreso = @NuIngreso
	
	