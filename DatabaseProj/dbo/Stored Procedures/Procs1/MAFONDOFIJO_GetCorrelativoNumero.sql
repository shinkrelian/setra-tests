﻿CREATE PROCEDURE MAFONDOFIJO_GetCorrelativoNumero
@CoPrefijo char(4)=''
as
SELECT RIGHT('000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,3)+1 as int)),1) as char(3)))),3) +'-'+substring(cast(year(getdate()) as char(4)),3,4) as correlativo FROM MAFONDOFIJO where coanio=cast(year(getdate()) as char(4)) and rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo))
