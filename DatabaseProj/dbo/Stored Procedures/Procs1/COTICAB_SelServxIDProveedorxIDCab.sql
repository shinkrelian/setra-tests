﻿Create Procedure dbo.COTICAB_SelServxIDProveedorxIDCab
@IDCab int,
@IDProveedor char(6)
As
Set NoCount On
Select p.IDTipoOper,p.IDTipoProv
from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
Where cd.IDCAB=@IDCab
