﻿create Procedure [dbo].[MACONTACTOSPROVEEDOR_Upd_Extranet_Restablecer_Pass]  
@IDContacto char(3),  
 @IDProveedor char(6),   
 
 @UsuarioLogeo_Hash varchar(max),
-- @FechaPassword datetime,   
 @UserMod char(4)
AS
BEGIN

 Set NoCount On
 UPDATE MACONTACTOSPROVEEDOR Set  
        
		   UsuarioLogeo_Hash=Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
		   ,FechaHash=DATEADD(HOUR,4, getdate())
           ,UserMod=@UserMod             
           ,FecMod=GETDATE()
		   ,FlAccesoWeb = 1 
     WHERE  
           IDContacto=@IDContacto And IDProveedor=@IDProveedor  
END;
