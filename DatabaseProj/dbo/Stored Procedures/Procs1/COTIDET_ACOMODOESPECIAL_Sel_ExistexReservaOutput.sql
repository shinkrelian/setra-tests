﻿

CREATE PROCEDURE [dbo].[COTIDET_ACOMODOESPECIAL_Sel_ExistexReservaOutput]
	@IDReserva INT,	
	@pExiste BIT OUTPUT
AS
	SET NOCOUNT ON
	
	SET @pExiste=0  	
	IF EXISTS(SELECT IDDet FROM COTIDET_ACOMODOESPECIAL 
		WHERE IDDet In (Select IDDet From RESERVAS_DET Where 
			IDReserva=@IDReserva And Anulado=0))				
		SET @pExiste=1
	

