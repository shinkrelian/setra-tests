﻿--JRF-20160329-...And cc.NuPedInt=@NuPedInt (2ndo Query)
--JRF-20160408- ..Agregar IDCliente
CREATE Procedure dbo.COTICAB_SelxPedidoAddIn
	@NuPedInt int,
	@CoCorreoOutlook varchar(Max)
As
	Set Nocount on

	Select u.Nombre+isnull(' '+u.TxApellidos,'') as EjecutivoVtas, cc.Fecha, cc.FecInicio, 
	cast(cc.NroPax as nvarchar(5))+isnull('+'+cast(cc.NroLiberados as nvarchar(3)),'') as Pax,
	cc.Cotizacion, isnull(cc.IDFile,'') as NuFile, cc.IDCAB, cc.Titulo, Cast((DATEDIFF(d,cc.FecInicio,cc.FechaOut)+1) as varchar(2))+' '+ 
		dbo.FnDestinosDia(cc.IDCAB,0) as Ruta,0 as NuCorreoDet,IsNull(cc.IDCliente,'') as IDClienteCot
	From COTICAB cc Left Join MAUSUARIOS u On cc.IDUsuario=u.IDUsuario
	Where @CoCorreoOutlook ='' And cc.NuPedInt=@NuPedInt
	Union
	Select u.Nombre+isnull(' '+u.TxApellidos,'') as EjecutivoVtas, cc.Fecha, cc.FecInicio, 
	cast(cc.NroPax as nvarchar(5))+isnull('+'+cast(cc.NroLiberados as nvarchar(3)),'') as Pax,
	cc.Cotizacion, isnull(cc.IDFile,'') as NuFile, cc.IDCAB, cc.Titulo, Cast((DATEDIFF(d,cc.FecInicio,cc.FechaOut)+1) as varchar(2))+' '+ 
		dbo.FnDestinosDia(cc.IDCAB,0) as Ruta,c.NuCorreo as NuCorreoDet,IsNull(cc.IDCliente,'') as IDClienteCot
	From COTICAB cc Inner Join .BDCORREOSETRA..CORREOFILE c On cc.IDCAB=c.IDCab And cc.NuPedInt=@NuPedInt And c.CoArea='VNT' And IsNull(c.CoCorreoOutlook,'') <> ''
	Left Join MAUSUARIOS u On cc.IDUsuario=u.IDUsuario
	Where c.CoCorreoOutlook=@CoCorreoOutlook
	Order by cc.IDCAB
