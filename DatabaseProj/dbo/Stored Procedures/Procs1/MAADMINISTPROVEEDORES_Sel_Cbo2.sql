﻿

--JHD-20150408- Agregar parametro @IDMoneda
--JHD-20150408- and (IDMoneda=@IDMoneda or ltrim(rtrim(@idmoneda))='')
--alter

CREATE Procedure dbo.MAADMINISTPROVEEDORES_Sel_Cbo2  
@IDProveedor char(6),
@IDMoneda char(3)=''
As  
 Set NoCount On  
 select IDAdminist As ID,
 CtaCte + SPACE(100) +'|'+Banco as CtaCte
 from MAADMINISTPROVEEDORES   
 Where IDProveedor = @IDProveedor  
 and (IDMoneda=@IDMoneda or ltrim(rtrim(@idmoneda))='')
 Order By CtaCte  

