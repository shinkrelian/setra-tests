﻿
--HLF-20151103-@IDServicio_Det_V_Cot int
--IDServicio=(select IDServicio from MASERVICIOS_DET ...
--HLF-20160114-@QtUtilizada smallint, (antes tinyint)
CREATE Procedure dbo.COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS_Ins
@NuTckEntIng smallint,
@IDServicio_Det_V_Cot int,
@IDCab int,
@QtUtilizada smallint,
@UserMod char(4)
As
Set NoCount On
Declare @NuStockFile int = IsNull((select Max(NuStockFile) from COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS),0)+1

Declare @IDServicio char(8)=(select IDServicio from MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det_V_Cot)

Insert into COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS(NuStockFile,NuTckEntIng,IDServicio,IDCab,QtUtilizada,UserMod)
Values(@NuStockFile,@NuTckEntIng,@IDServicio,@IDCab,@QtUtilizada,@UserMod)


