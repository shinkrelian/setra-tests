﻿CREATE Procedure [dbo].[MACORREOREVPROVEEDOR_Ins]
@IDProveedor char(6),
@Correo varchar(150),
@Descripcion varchar(max),
@UserMod char(4)
As
	Set Nocount On
	Insert into MACORREOREVPROVEEDOR (IDProveedor,Correo,Descripcion,UserMod) 
	values (@IDProveedor,@Correo,@Descripcion,@UserMod)
