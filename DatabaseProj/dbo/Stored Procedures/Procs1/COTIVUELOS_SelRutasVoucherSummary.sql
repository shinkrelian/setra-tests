﻿--JRF-20150616-...IsNull(uOrigen.Descripcio...
CREATE Procedure dbo.COTIVUELOS_SelRutasVoucherSummary
@IDCab int
as
SELECT (SELECT COUNT(*) FROM COTITRANSP_PAX WHERE IDTransporte=cv.ID ) as CantPax,
        pr.NombreCorto as DescProveedor,IsNull(uOrigen.Descripcion,'') as Origen,
        IsNull(uDestino.Descripcion,'') as Destino,cv.Fec_Emision,cv.Salida
FROM COTIVUELOS cv Left Join MAPROVEEDORES pr On cv.IdLineaA= pr.IDProveedor
Left Join MAUBIGEO uOrigen On cv.RutaO = uOrigen.IDUbigeo
Left Join MAUBIGEO uDestino On cv.RutaD = uDestino.IDUbigeo
WHERE IDCAB = @IDCab and (EmitidoSetours = 1 Or EmitidoOperador = 1)
Order by cast((convert(char(10),Fec_Emision,103)+' '+Isnull(cv.Salida,'00:00'))  as smalldatetime)
