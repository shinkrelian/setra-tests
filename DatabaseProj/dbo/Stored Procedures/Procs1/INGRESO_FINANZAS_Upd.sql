﻿--JRF-20131108-Nuevo campo IDPais,CodTar,NoRutaDocumento y SsOrdenado.          
--JRF-20131112-Nuevo campo SsGastosTransferencia      
--JRF-20131127-Cambiar los nombres de la columna, Nuevo campo FePresentacion    
--JRF-20140326-Aumento del tamaño del Tipo de cambio.  
--JRF-20140721-Nuevos campos [NoBancoOrdendante,NoBancoCorresponsal1,NoBancoCorresponsal2]
--JRF-20140721-Nuevos campos [IDUbigeoBancoOrde,IDUbigeoBancoCor1,NoBancoCorresponsal2]
CREATE Procedure dbo.INGRESO_FINANZAS_Upd            
@NuIngreso int,            
@FeAcreditada smalldatetime,            
@FePresentacion smalldatetime,    
@IDBanco char(3),            
@IDFormaPago char(3),          
@CoOperacionBan varchar(20),            
@IDCliente char(6),            
@IDPais char(6),          
@CoEstado char(2),            
@NoRutaDocumento varchar(200),        
@TxObservacion varchar(max),            
@IDMoneda char(3),            
@SsTc numeric(12,4),            
@SsOrdenado numeric(10,2),        
@SsGastosTransferencia numeric(10,2),      
@SsRecibido numeric(10,2),            
@SsBanco numeric(8,2),            
@SsNeto numeric(10,2),     
@NoBancoOrdendante varchar(120),
@NoBancoCorresponsal1 varchar(120),
@NoBancoCorresponsal2 varchar(120),
@IDUbigeoBancoOrde Char(6),
@IDUbigeoBancoCor1 char(6),
@IDUbigeoBancoCor2 char(6),       
@UserMod char(4)            
As            
Begin            
 Update INGRESO_FINANZAS            
  Set FeAcreditada = case when ltrim(rtrim(convert(char(10),@FeAcreditada,103)))='01/01/1900' then null else @FeAcreditada End,    
   FePresentacion = case when ltrim(rtrim(convert(char(10),@FePresentacion,103)))='01/01/1900' then null else @FePresentacion End,    
   IDBanco = case when ltrim(rtrim(@IDBanco))='' Or ltrim(rtrim(@IDBanco))='000' Then null Else @IDBanco End,            
   IDFormaPago = case when ltrim(rtrim(@IDFormaPago))='' Then null Else @IDFormaPago End,          
   CoOperacionBan = Case When ltrim(rtrim(@CoOperacionBan))='' then null else @CoOperacionBan End,            
   IDCliente = case when ltrim(rtrim(@IDCliente))='' then Null Else @IDCliente End,            
   IDPais = @IDPais,          
   CoEstado = @CoEstado,            
   NoRutaDocumento = Case When ltrim(rtrim(@NoRutaDocumento))= '' then null else @NoRutaDocumento End,        
   TxObservacion = case when ltrim(rtrim(@TxObservacion))='' then null else @TxObservacion End,            
   IDMoneda = @IDMoneda,            
   ssTC = case when @SsTc = 0 then null else @SsTc End,            
   SsOrdenado = case when @SsOrdenado = 0 then null else @SsOrdenado End,        
   SsGastosTransferencia = case when @SsGastosTransferencia = 0 then null else @SsGastosTransferencia End,      
   SsRecibido = @SsRecibido,            
   SsBanco = @SsBanco,            
   SsNeto = @SsNeto,   
   NoBancoOrdendante = Case When ltrim(rtrim(@NoBancoOrdendante))='' then Null Else @NoBancoOrdendante End,
   NoBancoCorresponsal1 = Case When LTRIM(rtrim(@NoBancoCorresponsal1))='' then Null Else @NoBancoCorresponsal1 End,
   NoBancoCorresponsal2 = Case When ltrim(rtrim(@NoBancoCorresponsal2))='' then Null Else @NoBancoCorresponsal2 End,
   IDUbigeoBancoOrde = Case When ltrim(rtrim(@IDUbigeoBancoOrde))='' then Null Else @IDUbigeoBancoOrde End,
   IDUbigeoBancoCor1 = Case When ltrim(rtrim(@IDUbigeoBancoCor1))='' then Null Else @IDUbigeoBancoCor1 End,
   IDUbigeoBancoCor2 = Case When LTRIM(rtrim(@IDUbigeoBancoCor2))='' then Null Else @IDUbigeoBancoCor2 End,                   
   UserMod = @UserMod,            
   FecMod = Getdate()            
 Where NuIngreso = @NuIngreso            
End            
