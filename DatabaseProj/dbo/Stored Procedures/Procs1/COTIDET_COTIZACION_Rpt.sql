﻿--JRF-20130409-Nuevo Campo de Fecha.    
--JRF-20131028-Correción de Títulos de Cabecera(Quiebres).      
--JRF-20131122-Correción de Título de Cabecera(Principal). 
--JRF-20140526-Nuevo campo [DocWordVersion]
CREATE Procedure [dbo].[COTIDET_COTIZACION_Rpt]                              
 @IDCab int,                      
 @PaxInic int,                      
 @PaxFin int ,                    
 @blnSiguRango bit                    
as                              
 set nocount On                              
 set language us_English                            
                            
 declare @Total numeric(8,2),@IDDet varchar(30),@servicio varchar(max),@Tipo char(5),@Lang varchar(12)                            
  ,@Pax varchar(10),@TOTALCOTIDET decimal(8,2),@TotalCotiQ  varchar(15),@Cont tinyint=1,                              
   @IDDetTmp Int,@PaxCotiDet  varchar(20),@fecha varchar(25),@Ubigeo varchar(60),@Cotizacion varchar(8),                              
   @Cliente varchar(90),@Titulo varchar(MAX),@Responsable varchar(60),@TipoCambio numeric(8,2),                              
   @TipoPax varchar(15),@FechaInicio varchar(25),@FechaDiaReconfirmacion varchar(25),@FechaFinal varchar(25),@IDTipoProv varchar(15),@PlanAlimenticio varchar(1),           
   @ConAlojamiento varchar(1),@Dia smalldatetime,@Item Numeric(6,2),@NroLiberados Tinyint,@NroLiberadosQuie varchar(5),                            
   @PrecioSimple varchar(15),@PrecioDoble varchar(15),@PrecioTriple varchar(15),@SSCR numeric(8,2),            
   @SSTotal numeric(8,2),@STCR numeric(8,2),@STTotal numeric(8,2),                            
   @PrecioSimpleQui varchar(15),@PrecioDobleQui varchar(15),@PrecioTripleQui varchar(15),@Notas varchar(max),@Especial bit                               
  ,@SSCRQuie numeric(8,2),@SSTotalQuie numeric(8,2),@STCRQuie numeric(8,2),@STTotalQuie numeric(8,2),@DocWordVersion tinyint
                            
 Delete From RptCotizacion                              
                            
  Declare curCoti cursor for                              
                              
  SELECT  CD.IDDet,CD.Dia,CD.Item,CD.Cotizacion,                            
  Case When CD.NroLiberados is null then cc.NroLiberados else CD.NroLiberados End As NroLiberados                            
  ,pr.IDTipoProv,c.RazonComercial,cc.Titulo,u.Nombre,isnull(cc.TipoCambio,0) As TipoCambio                            
  ,cc.TipoPax,convert(varchar(10),cc.FecInicio,103) as FechaInicial, Convert(varchar(10),DATEADD(DD,-1*cc.DiasReconfirmacion,cc.FecInicio),103) As FechaReConf      
   , Convert(varchar(10),cc.FechaOut,103) as FechaFinal                               
  ,UPPER(substring(Datename(dw,CD.Dia),1,3))+' '+ Convert(varchar(60),CD.Dia,103) as Fecha,                              
  ub.Descripcion as Ubigeo,                            
  Case When (Pr.IDTipoProv = '001' Or Pr.IDTipoProv='002') Then Pr.NombreCorto+' ('+ CD.Servicio +')' Else CD.Servicio End  + Case When cd.IncGuia = 1 then ' - Guide' else '' End As Servicio                            
  ,Case When S.IDTipoServ = 'NAP' Then '---' Else S.IDTipoServ End As IDTipoServ                            
  ,Idio.Siglas,sd.PlanAlimenticio  ,          
          
    Case When pr.IDTipoProv = '003' and sd.ConAlojamiento = 1 then          
 Case When sd.ConAlojamiento = 1 then         
  Case When isnull(cd.IDDetRel,0)= 0 then 1 else 0 End        
 else        
  0 End        
  else          
  Case When pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then        
  sd.ConAlojamiento        
 else 0 End        
  End As ConAlojamiento          
          
  ,Case When CD.IncGuia = 1 then cc.NroPax else cd.nropax End As nropax            
  ,CABQ.Pax                            
  ,CABQ.Liberados            
  ,isnull(CD.TOTAL,0) as TotalCotiDet,                            
  CD.SSTotal As PrecioSimple ,            
  CD.CostoReal As PrecioDoble,             
  CD.STTotal As PrecioTriple,            
  CD.SSCR,            
  CD.SSTotal,            
  CD.STCR,            
  CD.STTotal                       
              
  ,isnull(CQ.Total,0) Total, CQ.SSTotal As PrecioSimpleQ, CQ.CostoReal As PrecioDobleQ, CQ.STTotal As PrecioTripleQ,                 
  isnull(CQ.SSCR,0) As SSCR,            
  isnull(CQ.SSTotal,0) AS SSTotal,            
  isnull(CQ.STCR,0) As STCR,            
  isnull(CQ.STTotal,0) As  STTotal,            
  cast(cc.Notas as Varchar) as Notas ,CD.Especial ,cc.DocWordVersion                            
  FROM COTIDET_QUIEBRES CQ Right Join                               
    (select top(5)* from COTICAB_QUIEBRES where IDCAB=@IDCab And Pax between @PaxInic And @PaxFin Order by Pax) CABQ ON CQ.IDCAB_Q=CABQ.IDCAB_Q   
   Right Join COTIDET CD ON CQ.IDDET=CD.IDDET Left Join MAUBIGEO ub On cd.IDUbigeo=ub.IDUbigeo                               
   Left Join COTICAB cc  On CD.IDCAB=cc.IDCAB Left Join MAUSUARIOS u On cc.IDUsuario=u.IDUsuario                              
   Left Join MACLIENTES c On cc.IDCLiente= c.IDCliente Left Join MAPROVEEDORES pr On CD.IDProveedor=pr.IDProveedor                              
   Left Join MASERVICIOS S On CD.IDServicio = S.IDServicio                            
   Left Join MASERVICIOS_DET sd On sd.IDServicio_Det = cd.IDServicio_Det            
   Left Join MAIDIOMAS Idio On Idio.IDIdioma = CD.IDIdioma                            
  WHERE CD.IDDet In (SELECT IDDET FROM COTIDET WHERE IDCAB=@IDCab)                             
  ORDER BY CQ.IDDet,CABQ.Pax                           
  Open curCoti                              
 Fetch Next From curCoti Into @IDDet,@Dia,@Item,@Cotizacion,@NroLiberados,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax                              
   ,@FechaInicio,@FechaDiaReconfirmacion,@FechaFinal,@fecha,@Ubigeo,@servicio,@Tipo,@Lang,@PlanAlimenticio,@ConAlojamiento,@PaxCotiDet,@Pax,@NroLiberadosQuie,@TOTALCOTIDET,@PrecioSimple,                            
   @PrecioDoble,@PrecioTriple,@SSCR,@SSTotal,@STCR,@STTotal,@TotalCotiQ,@PrecioSimpleQui,@PrecioDobleQui,@PrecioTripleQui            
   ,@SSCRQuie,@SSTotalQuie,@STCRQuie,@STTotalQuie,@Notas,@Especial,@DocWordVersion                            
 While  @@fetch_status = 0                              
 Begin                              
                    
if(@blnSiguRango = 0)                    
Begin                    
                    
    SELECT  @Total=COTQ.Total, @NroLiberadosQuie = CABQ.Liberados ,                            
        @PrecioSimpleQui = ( SELECT  TotalSimple FROM (                              
        SELECT Total+TotalSimple AS TotalSimple                                
        FROM                              
        (                              
        Select Sum(dq.Total) as Total,                              
         SUM(dq.SSTotal) as TotalSimple                             
        From COTICAB_QUIEBRES cq Left Join COTIDET_QUIEBRES dq On dq.IDCAB_Q=cq.IDCAB_Q                              
        Where cq.IDCAB = @IDCab and cq.Pax = @Pax                            
        ) AS X                              
       ) AS Y ),                            
  @PrecioDobleQui = ( SELECT Total FROM ( SELECT                               
      TotalCostoReal,                              
      TotalImpuesto,                              
      Total  FROM  (                              
 Select Sum(dq2.Total) as Total,                                
      Sum(dq2.CostoReal) as TotalCostoReal,                              
      SUm(dq2.TotImpto) as TotalImpuesto                                
      From COTICAB_QUIEBRES cq2 Left Join COTIDET_QUIEBRES dq2 On dq2.IDCAB_Q=cq2.IDCAB_Q                              
      Where cq2.IDCAB=@IDCab and cq2.Pax = @Pax                            
      ) AS X                              
      ) AS Y  ),                            
    @PrecioTripleQui = (  SELECT TotalTriple FROM (  SELECT                              
        Total+TotalTriple AS TotalTriple,Total                              
        FROM                              
        (        
        Select Sum(dq3.Total) as Total,                               
        SUM(dq3.STTotal) as TotalTriple                            
                                      
        From COTICAB_QUIEBRES cq3 Left Join COTIDET_QUIEBRES dq3 On dq3.IDCAB_Q=cq3.IDCAB_Q                              
        Where cq3.IDCAB=@IdCab and cq3.Pax = @Pax                              
        ) AS X                              
       ) AS Y  ) ,            
       @SSCRQuie = COTQ.SSCR,            
       @SSTotalQuie = COTQ.SSTotal,            
   @STCRQuie = COTQ.STCR,            
    @STTotalQuie = COTQ.STTotal                        
    FROM COTIDET_QUIEBRES COTQ LEFT JOIN COTICAB_QUIEBRES CABQ ON COTQ.IDCAB_Q=CABQ.IDCAB_Q                              
    WHERE COTQ.IDDet=@IDDet AND CABQ.Pax=@Pax                                      
                    
    if @Cont=1                                
   begin                              
    if Not Exists (select IDDet from RptCotizacion where FilTitulo=1)                         
    Begin                              
                              
    insert into RptCotizacion(IDDet,Dia,Item,Cotizacion,NroLiberados,IDTipoProv,Cliente,Titulo,Responsable,TipoCambio,TipoPax,                              
      FechaInicio,FechaConfirmacion,FechaFinal,Fecha,Ubigeo,Servicio,Tipo,Lang,TotalCotIdet,C1,FilTitulo,Notas,DocWordVersion)
    values('IDDet','01/01/1900','0.000','Cotización','0','IDTipoProv',@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax,@FechaInicio,@FechaDiaReconfirmacion,@FechaFinal                              
     ,'Date','City','Service','Type','Lang.',(select cast(NroPax as varchar(5)) from coticab where idcab = @Idcab) + Isnull(' + ' +Cast((select NroLiberados from coticab where idcab = @Idcab) As varchar(5)),''),                            
    Case when @PaxInic = 0 and @PaxFin = 0 then '' Else                      
     Case when @Pax is null then (select top(1) LTRIM(rtrim(cast(Pax as CHAR(5)))) +  ISNULL(' + ' + ltrim(rtrim(Cast(Liberados As CHAR(5)))),'') from COTICAB_QUIEBRES WHERE IDCAB =@IDCAB order by Pax)             
     else @Pax +isnull(' + '+@NroLiberadosQuie,'') End end,1,'Notas',@DocWordVersion)                                
    End                     
   insert into RptCotizacion                             
    values (@IDDet,@Dia,(select Item from COTIDET Where Iddet = @IDDet),@Cotizacion,@NroLiberados,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax                                
    ,@FechaInicio,@FechaDiaReconfirmacion,@FechaFinal,@fecha,@Ubigeo,@servicio,@Tipo,@Lang,@PlanAlimenticio,@ConAlojamiento,@TOTALCOTIDET,                                          
    ( SELECT TotalSimple FROM  (                              
   SELECT                               
   Total+TotalSimple AS TotalSimple,Total                              
   FROM                              
   (                               
   select Sum(d.Total) as Total,                              
  SUM(d.SSTotal) as TotalSimple From COTIDET d                             
   Where d.IDCAB=@IdCab                              
   ) AS X                              
     ) AS Y                              
    )                            
    --,@PrecioDoble                            
      ,(  SELECT Total FROM (                           
     SELECT                             
     TotalCostoReal,                              
     TotalImpuesto,                              
     Total                              
     FROM  (                              
     Select                             
     Sum(d2.Total) as Total,                              
     Sum(d2.CostoReal) as TotalCostoReal,                              
     SUm(d2.TotImpto) as TotalImpuesto                              
     From COTIDET d2                             
     Where d2.IDCAB=@IDCAB                              
     ) AS X                              
    ) AS Y  )                            
    --,@PrecioTriple                            
    ,( SELECT TotalTriple FROM (                              
   SELECT  Total+TotalTriple AS TotalTriple                            
   FROM  (Select Sum(d3.Total) as Total, SUM(d3.STTotal) as TotalTriple                             
     From COTIDET d3 Where d3.IDCAB=@IDCab                              
   ) AS X                              
     ) AS Y                              
                                 
    ),            
    @SSCR,@SSTotal,@STCR,@STTotal,            
    @TotalCotiQ,                            
    @PrecioSimpleQui,                            
    @PrecioDobleQui,                            
    @PrecioTripleQui,            
    @SSCRQuie,@SSTotalQuie,@STCRQuie,@STTotalQuie  ,            
    '','','','','0.000','0.000','0.000','0.000'            
    ,'','','','','0.000','0.000','0.000','0.000'            
    ,'','','','','0.000','0.000','0.000','0.000',0,@Notas,@DocWordVersion)                                
     End                     
                    
   if @Cont=2                                
     begin                                   
   Update RptCotizacion  set C2=@Pax + ISNULL(' + '+CAST(@NroLiberadosQuie As Varchar(5)),'') where FilTitulo=1 and C2 is null --And not @Pax is  null                            
   Update RptCotizacion  set TotalCotIdet=@TOTALCOTIDET,C2= @TotalCotiQ   ,servicio=@servicio                             
   ,PrecioSimpleC2 = @PrecioSimpleQui                            
   , PrecioDobleC2= @PrecioDobleQui,                            
   PrecioTripleC2 = @PrecioTripleQui,            
   SSCRC2 = @SSCRQuie,            
   SSTotalC2 = @SSTotalQuie,            
   STCRC2 = @STCRQuie,            
   STTotalC2 = @STTotalQuie            
     where FilTitulo=0  and IDDet=@IDDet                              
     end                              
   if @Cont=3                               
     begin                              
   Update RptCotizacion  set C3=@Pax + ISNULL(' + '+CAST(@NroLiberadosQuie As Varchar(5)),'') where FilTitulo=1 and C3 is null --And not @Pax is null                              
   Update RptCotizacion  set TotalCotIdet=@TOTALCOTIDET,C3=  @TotalCotiQ   ,servicio=@servicio                             
    ,PrecioSimpleC3 = @PrecioSimpleQui                            
   , PrecioDobleC3=@PrecioDobleQui,                            
    PrecioTripleC3 = @PrecioTripleQui  ,            
    SSCRC3 = @SSCRQuie,            
   SSTotalC3 = @SSTotalQuie,            
   STCRC3 = @STCRQuie,            
   STTotalC3 = @STTotalQuie                          
    where FilTitulo=0  and IDDet=@IDDet                              
    End                              
   If @Cont=4                               
     begin                              
   Update RptCotizacion  set C4=@Pax + ISNULL(' + '+CAST(@NroLiberadosQuie As Varchar(5)),'') where FilTitulo=1 and C4 is null --And  not @Pax is null                              
   Update RptCotizacion  set TotalCotIdet=@TOTALCOTIDET,C4= @TotalCotiQ  ,servicio=@servicio                            
   ,PrecioSimpleC4 = @PrecioSimpleQui,                             
   PrecioDobleC4= @PrecioDobleQui                            
   ,PrecioTripleC4 =@PrecioTripleQui ,            
   SSCRC4 = @SSCRQuie,            
   SSTotalC4 = @SSTotalQuie,            
   STCRC4 = @STCRQuie,            
   STTotalC4 = @STTotalQuie                           
     where FilTitulo=0 and IDDet=@IDDet                              
     End                       
End                           
else                    
Begin                     
 SELECT  @PaxCotiDet = CABQ.Pax ,@TotalCotiQ=COTQ.Total, @NroLiberadosQuie = CABQ.Liberados ,                            
        @PrecioSimpleQui = ( SELECT  TotalSimple FROM (                              
        SELECT Total+TotalSimple AS TotalSimple  FROM (                              
        Select Sum(dq.Total) as Total, SUM(dq.SSTotal) as TotalSimple                             
        From COTICAB_QUIEBRES cq Left Join COTIDET_QUIEBRES dq On dq.IDCAB_Q=cq.IDCAB_Q                              
        Where cq.IDCAB = @IDCab and cq.Pax = @Pax                            
        ) AS X                              
       ) AS Y ),                            
  @PrecioDobleQui = ( SELECT Total FROM ( SELECT                               
      TotalCostoReal,                              
      TotalImpuesto,                              
      Total  FROM  (                              
 Select Sum(dq2.Total) as Total,                          
      Sum(dq2.CostoReal) as TotalCostoReal,                              
      SUm(dq2.TotImpto) as TotalImpuesto                                
      From COTICAB_QUIEBRES cq2 Left Join COTIDET_QUIEBRES dq2 On dq2.IDCAB_Q=cq2.IDCAB_Q                              
      Where cq2.IDCAB=@IDCab and cq2.Pax = @Pax                            
      ) AS X                              
      ) AS Y  ),                            
    @PrecioTripleQui = (  SELECT TotalTriple FROM (  SELECT                              
        Total+TotalTriple AS TotalTriple,Total                              
        FROM                    
        (                              
        Select Sum(dq3.Total) as Total,                               
        SUM(dq3.STTotal) as TotalTriple                     
                                      
        From COTICAB_QUIEBRES cq3 Left Join COTIDET_QUIEBRES dq3 On dq3.IDCAB_Q=cq3.IDCAB_Q                              
        Where cq3.IDCAB=@IdCab and cq3.Pax = @Pax                              
        ) AS X                              
       ) AS Y  ),            
       @SSCRQuie = COTQ.SSCR,            
       @SSTotalQuie = COTQ.SSTotal,            
       @STCRQuie = COTQ.STCR,            
    @STTotalQuie = COTQ.STTotal                      
    FROM COTIDET_QUIEBRES COTQ LEFT JOIN COTICAB_QUIEBRES CABQ ON COTQ.IDCAB_Q=CABQ.IDCAB_Q                              
    WHERE COTQ.IDDet=@IDDet AND CABQ.Pax=@Pax                      
                        
 --print 'Test'                    
 if @Cont = 1                    
 Begin                    
 If Not Exists(select IDDet from RptCotizacion where FilTitulo=1)                    
  Begin                    
  insert into RptCotizacion(IDDet,Dia,Item,Cotizacion,NroLiberados,IDTipoProv,Cliente,Titulo,Responsable,TipoCambio,TipoPax,                              
    FechaInicio,FechaConfirmacion,FechaFinal,Fecha,Ubigeo,Servicio,Tipo,Lang,TotalCotIdet,FilTitulo,Notas,DocWordVersion)                               
   values('IDDet','01/01/1900','0.000','Cotización','0','IDTipoProv',@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax,@FechaInicio,@FechaDiaReconfirmacion,@FechaFinal                          
   ,'Date','City','Service','Type','Lang.',@PaxCotiDet + Isnull(' + ' +Cast(@NroLiberadosQuie As varchar(5)),''),1,'Notas',@DocWordVersion)                       
  End                    
                     
                     
 insert into RptCotizacion                             
    values (@IDDet,@Dia,(select Item from COTIDET Where Iddet = @IDDet),@Cotizacion,@NroLiberados,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax                                
    ,@FechaInicio,@FechaDiaReconfirmacion,@FechaFinal,@fecha,@Ubigeo,@servicio,@Tipo,@Lang,@PlanAlimenticio,@ConAlojamiento,@TotalCotiQ,@PrecioSimpleQui,@PrecioDobleQui,@PrecioTripleQui,@SSCRQuie            
    ,@SSTotalQuie,@STCRQuie,@STTotalQuie,            
    '','','','','0.000','0.000','0.000','0.000',            
    '','','','','0.000','0.000','0.000','0.000',            
    '','','','','0.000','0.000','0.000','0.000',            
    '','','','','0.000','0.000','0.000','0.000',0,@Notas,@DocWordVersion)                                
 End                    
 if @Cont=2                                
     begin                                   
   Update RptCotizacion  set C1=@PaxCotiDet + ISNULL(' + '+CAST(@NroLiberadosQuie As Varchar(5)),'') where FilTitulo=1 and C1 is null --And not @Pax is  null         
   Update RptCotizacion  set C1= @TotalCotiQ ,servicio=@servicio                             
   ,PrecioSimpleC1 = @PrecioSimpleQui                            
   , PrecioDobleC1= @PrecioDobleQui,                            
   PrecioTripleC1 = @PrecioTripleQui            
   ,SSCRC1 = @SSCRQuie,            
   SSTotalC1 = @SSTotalQuie,            
   STCRC1 = @STCRQuie,            
   STTotalC1 = @STTotalQuie                            
     where FilTitulo=0  and IDDet=@IDDet                              
 end                      
 if @Cont=3                               
    begin                              
  Update RptCotizacion  set C2=@PaxCotiDet + ISNULL(' + '+CAST(@NroLiberadosQuie As Varchar(5)),'') where FilTitulo=1 and C2 is null --And not @Pax is null                              
  Update RptCotizacion  set C2= @TotalCotiQ  ,servicio=@servicio                             
   ,PrecioSimpleC2 = @PrecioSimpleQui                            
  , PrecioDobleC2=@PrecioDobleQui,                            
   PrecioTripleC2 = @PrecioTripleQui                            
   ,SSCRC2 = @SSCRQuie,            
   SSTotalC2 = @SSTotalQuie,            
   STCRC2 = @STCRQuie,            
   STTotalC2 = @STTotalQuie              
   where FilTitulo=0  and IDDet=@IDDet                              
   End                              
 If @Cont=4                               
    begin                              
  Update RptCotizacion  set C3=@PaxCotiDet + ISNULL(' + '+CAST(@NroLiberadosQuie As Varchar(5)),'') where FilTitulo=1 and C3 is null --And  not @Pax is null                              
  Update RptCotizacion  set C3= @TotalCotiQ ,servicio=@servicio                            
  ,PrecioSimpleC3 = @PrecioSimpleQui,                             
  PrecioDobleC3= @PrecioDobleQui                            
  ,PrecioTripleC3 =@PrecioTripleQui                            
   ,SSCRC3 = @SSCRQuie,            
   SSTotalC3 = @SSTotalQuie,            
   STCRC3 = @STCRQuie,            
   STTotalC3 = @STTotalQuie              
    where FilTitulo=0 and IDDet=@IDDet                              
 End                    
 If @Cont=5                               
    begin                              
  Update RptCotizacion  set C4=@PaxCotiDet + ISNULL(' + '+CAST(@NroLiberadosQuie As Varchar(5)),'') where FilTitulo=1 and C4 is null --And  not @Pax is null                              
  Update RptCotizacion  set C4=   @TotalCotiQ   ,servicio=@servicio                            
  ,PrecioSimpleC4 = @PrecioSimpleQui,                             
  PrecioDobleC4= @PrecioDobleQui                            
  ,PrecioTripleC4 =@PrecioTripleQui                            
  ,SSCRC4 = @SSCRQuie,            
   SSTotalC4 = @SSTotalQuie,            
   STCRC4 = @STCRQuie,            
   STTotalC4 = @STTotalQuie              
    where FilTitulo=0 and IDDet=@IDDet                              
 End                        
End                    
                              
    Set @IDDetTmp=@IDDet                              
    Fetch Next From curCoti Into @IDDet,@Dia,@Item,@Cotizacion,@NroLiberados,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax                              
   ,@FechaInicio,@FechaDiaReconfirmacion,@FechaFinal,@fecha,@Ubigeo,@servicio,@Tipo,@Lang,@PlanAlimenticio,@ConAlojamiento,@PaxCotiDet,@Pax,@NroLiberadosQuie,@TOTALCOTIDET,@PrecioSimple,                            
   @PrecioDoble,@PrecioTriple,@SSCR,@SSTotal,@STCR,@STTotal,@TotalCotiQ,@PrecioSimpleQui,@PrecioDobleQui,@PrecioTripleQui            
   ,@SSCRQuie,@SSTotalQuie,@STCRQuie,@STTotalQuie,@Notas,@Especial,@DocWordVersion
                              
    If @IDDet=@IDDetTmp                                  
   Set @Cont=@Cont+1                                  
    Else                                  
   Set @Cont=1                              
                              
 End                                 
 Close curCoti                              
 Deallocate curCoti                              
 select * From RptCotizacion Order by FilTitulo Desc ,Dia ASC,Item ASC                             
