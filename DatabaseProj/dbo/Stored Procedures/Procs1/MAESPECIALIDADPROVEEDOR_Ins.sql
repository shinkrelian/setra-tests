﻿
Create Procedure [dbo].[MAESPECIALIDADPROVEEDOR_Ins]
@IDEspecialidad char(2),
@IDProveedor Char(6),
@UserMod char(4) 
As
	Set NoCount On
	Insert Into MAESPECIALIDADPROVEEDOR (IDEspecialidad,IDProveedor,UserMod) 
			values (@IDEspecialidad,@IDProveedor,@UserMod)
