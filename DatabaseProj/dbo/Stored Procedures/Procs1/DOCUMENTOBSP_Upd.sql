﻿--JRF-20140708-Agregar los nuevos campos [FeEmision,CoPeriodo]
CREATE Procedure dbo.DOCUMENTOBSP_Upd  
 @NuDocumBSP char(11),  
 @CoTipDocBSP char(3),  
 @FeEmision smalldatetime,
 @CoPeriodo char(9),
 @SSTotalDocum numeric(9,2),  
 @UserMod char(4)  
As  
 Set Nocount On  
   
 Update DOCUMENTOBSP  
 Set FeEmision = @FeEmision,
	 CoPeriodo = @CoPeriodo,
	 SSTotalDocum=@SSTotalDocum,
	 UserMod=@UserMod,
	 fecmod=getdate()  
 Where NuDocumBSP=@NuDocumBSP and CoTipDocBSP=@CoTipDocBSP  
