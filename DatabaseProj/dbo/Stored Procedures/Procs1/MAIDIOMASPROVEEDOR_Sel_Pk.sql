﻿Create Procedure [dbo].[MAIDIOMASPROVEEDOR_Sel_Pk]
	@IDProveedor	char(6),
	@IDIdioma	varchar(12)
As
	Set Nocount On
	
	Select *
	From MAIDIOMASPROVEEDOR 
	Where IDProveedor=@IDProveedor And IDIdioma = @IDIdioma
