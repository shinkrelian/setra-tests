﻿Create Procedure dbo.COTICAB_Upd_FechaEntregaKitPax
@IDCab int,
@FechaEntregaKitPax smalldatetime,
@UserMod char(4)
as
Set NoCount On
Update COTICAB set 
	FechaEntregaKitPax = Case When CONVERT(char(10),@FechaEntregaKitPax,103)='01/01/1900' then null else @FechaEntregaKitPax End , 
	UserMod = @UserMod,
	FecMod = GETDATE()
Where IDCAB = @IDCab
