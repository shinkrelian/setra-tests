﻿
--HLF-20130315-Agregando UsEjecutivoVentas, UsEjecutivoReservas, UsEjecutivoOperaciones                    
--HLF-20130520-Agregando CorreoEjecutOper, EjecutivoOperaciones                  
--JRF-20131018-Agregando la columna FechaAsigReserva                
--JRF-20131205-Agregando una columna Nueva.              
--HLF-20140401-Agregando TipoVenta          
--JRF-20140515-Agregar el campo del cliente solvencia     
--JRF-20140922-Case When Exists(select IDPax from COTIPAX where IDCab = cc.IDCAB and Residente=1) then 'NCR' Else 'EXT' End as TipoPaxNac    
--JHD-20141114-Agregar el campo Cliente Top    
--HLF-20150205-@CoUbigeo_Oficina char(6)=''  
--Case When @CoUbigeo_Oficina In ('000113','000110') Then --B. Aires , Santiago....  
--JRF-20150323-Si Existe En TOUR CONDUCTOR
--JHD-20150408-NuPosicion=isnull(c.NuPosicion,0)
--JRF-20151223-Agregar Correos Reservas,Estado
--JRF-PMM-20160601-..FlCorreoEliEnviado as blnCorreoEliminadoEnv..
--HLF-20160216-isnull(pd.NuPedido,'') as NuPedido Left Join PEDIDO pd On cc.NuPedInt=pd.NuPedInt
--JRF-20160301-..IsNull(c.CardCodeSAP,'') as CardCodeSAP,IsNull(c.RazonComercial,'') as CardName
--PPMG-201603121- When 'DR' Then 'ZICASSO'
CREATE Procedure [dbo].[COTICAB_Sel_Pk]
 @IDCab int,  
 @CoUbigeo_Oficina char(6)=''  
As                          
 Set NoCount On                          
 Declare @UsuarioSupReservas varchar(100)='',@CorreoSupReservas varchar(200)=''
 Declare @UsuarioSupOperacio varchar(100)='',@CorreoSupOperacio varchar(200)=''

 If Not Exists(select * from MAUSUARIOS where CoCargo=56)
 Begin
	select @UsuarioSupReservas = Nombre+' '+IsNull(TxApellidos,''),
		   @CorreoSupReservas = IsNull(Correo,'')
	from MAUSUARIOS where IDUsuario='0133' and FlTrabajador_Activo=1--'0133'
 End
 else
 Begin
	select @UsuarioSupReservas = Nombre+' '+IsNull(TxApellidos,''),
		   @CorreoSupReservas = IsNull(Correo,'')
	from MAUSUARIOS where CoCargo=56 and FlTrabajador_Activo=1
 End

 select @UsuarioSupOperacio = Nombre+' '+IsNull(TxApellidos,''),
		@CorreoSupOperacio = IsNull(Correo,'')
 from MAUSUARIOS where CoCargo = 53 and FlTrabajador_Activo=1


 Select cc.*,  
 --c.RazonComercial as DescCliente,  
 Case When @CoUbigeo_Oficina In ('000113','000110') Then --B. Aires , Santiago  
 (Select NombreCorto From MAPROVEEDORES Where IDProveedor='001554')--Set. Lima  
 Else  
 c.RazonComercial   
 End as DescCliente,  
 --c.Direccion as DireccionCliente,                          
Case When @CoUbigeo_Oficina In ('000113','000110') Then --B. Aires , Santiago  
 (Select Direccion From MAPROVEEDORES Where IDProveedor='001554')--Set. Lima  
 Else  
 c.Direccion   
 End as DireccionCliente,   
 cc.IDubigeo as IDPais,                          
 cc.IDUsuarioRes,                          
 usVtas.Nombre as EjecutivoVentas,                    
                     
 Case When cc.IDUsuario='0000' Then '' Else usVtas.Usuario End as UsEjecutivoVentas,                    
 Case When cc.IDUsuarioRes='0000' Then '' Else usRes.Usuario End as UsEjecutivoReservas,              
 usRes.Nombre as EjecutivoReserva,                  
 Case When cc.IDUsuarioOpe='0000' Then '' Else usOpe.Usuario End as UsEjecutivoOperaciones,                    
 IsNull(usVtas.Correo,'') as CorreoEjecutVtas  ,                        
                      
 Case When                        
 --NroPax > 15                         
 Exists(select IDDET from COTIDET cd Left Join MAUBIGEO Ub On cd.IDubigeo =Ub.IDubigeo                        
 where IDCAB = @IDCab and ub.IDPais <> '000007' and ub.IDPais <> '000323')                        
 then                         
 'M'                         
 Else                         
 Case When cc.NroPax > 8 Then 'G' Else 'F' End                        
 End As EsMulti_Fits_Group,  ub.Descripcion as DescPais,                
                   
 usOpe.Correo as CorreoEjecutOper,                  
 usOpe.Nombre as EjecutivoOperaciones  ,                
 cast(cc.FechaAsigReserva as smalldatetime) as FechaAsigReserva  ,              
 c.MostrarEnReserva,          
 cc.CoTipoVenta,          
 Case cc.CoTipoVenta           
 When 'RC' Then 'RECEPTIVO'          
 When 'CU' Then 'COUNTER'          
 When 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'          
 When 'VC' Then 'VIAJE INSPECCION (CLIENTE)'          
 When 'DR' Then 'ZICASSO'
 End AS DescTipoVenta ,c.Solvencia  ,      
 cc.IDUsuario as IDUserV,cc.IDUsuarioRes as IDUserR,cc.IDUsuarioOpe as IDUserO,      
 usVtas.Usuario as UserV,usRes.Usuario as UserR, usOpe.Usuario as UserO,    
 Isnull(cc.Margen,0) as MargenCoti,    
 Case When Exists(select IDPax from COTIPAX where IDCab = cc.IDCAB and Residente=1) then 'NCR' Else 'EXT' End as TipoPaxNac    
 ,FlTop=isnull(c.FlTop,CAST(0 as bit)),
 Case When Exists(select ctc.IDCab from COTICAB_TOURCONDUCTOR ctc Where ctc.IDCab=cc.IDCAB) Then 1 Else 0 End as FlIncluirTC
 ,NuPosicion=isnull(c.NuPosicion,0),
 @UsuarioSupReservas as SupervisorReservas,@CorreoSupReservas as CorreoSupervisorReservas,
 @UsuarioSupOperacio as SupervisorOperaciones,@CorreoSupOperacio as CorreoSupervisorOperaciones,
 IsNull(usRes.Correo,'') as CorreoEjecutivoReservas,usRes.Nombre+IsNull(usRes.TxApellidos,'') as NombreEjecutivoReservas,
 Case cc.Estado
	When 'P' Then 'PENDIENTE'
	When 'A' Then 'ACEPTADO'
	When 'N' Then 'NO ACEPTADO'
	When 'X' Then 'ANULADO'
	When 'E' Then 'ELIMINADO'
	When 'C' Then 'CERRADO'
 End as EstadoFile,FlCorreoEliEnviado as blnCorreoEliminadoEnv,
 isnull(pd.NuPedido,'') as NuPedido,IsNull(c.CardCodeSAP,'') as CardCodeSAP,IsNull(c.RazonComercial,'') as CardName
 From COTICAB cc Left Join MACLIENTES c On cc.IDCliente=c.IDCliente                          
 Left Join MAUSUARIOS usVtas On cc.IDUsuario=usVtas.IDUsuario                          
 Left Join MAUSUARIOS usRes On cc.IDUsuarioRes=usRes.IDUsuario                          
 Left Join MAUSUARIOS usOpe On cc.IDUsuarioOpe=usOpe.IDUsuario                
 Left Join MAUBIGEO ub On c.IDCiudad= ub.IDubigeo           
 Left Join PEDIDO pd On cc.NuPedInt=pd.NuPedInt           
 Where cc.IDCAB=@IDCab                           

