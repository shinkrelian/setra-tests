﻿--HLF-20151013-@NoHotel varchar(100),@TxWebHotel varchar(150),
--If Len(@IDIdioma)>=9 ...
--PPMG-20151102-Se modifico de 2 a 3 cast(ISNULL(@tiCant,1) as nvarchar(3))
--HLF-20160128-@TxDireccHotel,@TxTelfHotel1,@TxTelfHotel2,@FeHoraChkInHotel,@FeHoraChkOutHotel,@CoTipoDesaHotel,@CoCiudadHotel
--SELECT @tiCant=NroDia FROM .....
CREATE Procedure [dbo].[COTIDET_DESCRIPSERV_Ins]
	@IDDet	int,
	@IDIdioma	varchar(12),
	--@DescCorta	varchar(100),
	@DescLarga	text,

	@NoHotel varchar(100)='',
	@TxWebHotel varchar(150)='',

	@TxDireccHotel varchar(150)='',
	@TxTelfHotel1 varchar(50)='',
	@TxTelfHotel2 varchar(50)='',
	@FeHoraChkInHotel smalldatetime='01/01/1900',
	@FeHoraChkOutHotel smalldatetime='01/01/1900',
	@CoTipoDesaHotel char(2)='', 
	@CoCiudadHotel char(6)='',

	@UserMod	char(4)	
As

	Set NoCount On
	
	If Len(@IDIdioma)>=9
		Begin
		If Left(@IDIdioma,9)='HOTEL/WEB'
			Begin
			Declare @tiCant tinyint		
			
			--Select @tiCant=count(*)+1 From COTIDET_DESCRIPSERV cds Inner Join COTIDET cd On cds.IDDet=cd.IDDET 
			--	And cd.IDDET=@IDDet
			--	and cd.IDServicio=(Select idservicio from COTIDET Where IDDet=@IDDet)
			--	and left(cds.IDIdioma,9)='HOTEL/WEB'	
			
			SELECT @tiCant=NroDia FROM 
			(
			SELECT IDDET,CASE WHEN IDDetRel=0 THEN 1 ELSE NRO+1 END AS NroDia
			FROM 
			(
					SELECT ROW_NUMBER() OVER(PARTITION BY CD.IDCAB,ISNULL(CD.IDDetRel,0) ORDER BY CD.IDDet) AS NRO,
						ISNULL(CD.IDDetRel,0) AS IDDetRel, IDDET
					FROM COTIDET CD 
					WHERE CD.IDDET IN(SELECT IDDet FROM COTIDET 
						WHERE IDServicio IN (SELECT IDServicio FROM MASERVICIOS_DIA WHERE IDIdioma='HOTEL/WEB')
							AND IDCAB=(Select IDCAB From COTIDET Where IDDet=@IDDet))
				) AS X
			) AS Y WHERE IDDET=@IDDet

			Set @IDIdioma='HOTEL/WEB'+cast(ISNULL(@tiCant,1) as nvarchar(3))
			End
		End
	Insert Into COTIDET_DESCRIPSERV(IDDet,IDIdioma,DescLarga,
		   NoHotel
		   ,TxWebHotel
		   ,TxDireccHotel,
		   TxTelfHotel1,
		   TxTelfHotel2,
		   FeHoraChkInHotel,
		   FeHoraChkOutHotel,
		   CoTipoDesaHotel,
		   CoCiudadHotel
			,UserMod)
	Values(@IDDet,@IDIdioma,
	--Case When ltrim(rtrim(@DescCorta))='' Then Null Else @DescCorta End,
	Case When ltrim(rtrim(Cast(@DescLarga as varchar(max))))='' Then Null Else @DescLarga End,
	--@DescLarga,
		   Case When ltrim(rtrim(@NoHotel))='' Then Null Else @NoHotel End,
		   Case When ltrim(rtrim(@TxWebHotel))='' Then Null Else @TxWebHotel End,
		   Case When ltrim(rtrim(@TxDireccHotel))='' Then Null Else @TxDireccHotel End ,
			Case When ltrim(rtrim(@TxTelfHotel1))='' Then Null Else @TxTelfHotel1 End ,
			Case When ltrim(rtrim(@TxTelfHotel2))='' Then Null Else @TxTelfHotel2 End ,
			Case When @FeHoraChkInHotel='01/01/1900' Then Null Else @FeHoraChkInHotel End ,
			Case When @FeHoraChkOutHotel='01/01/1900' Then Null Else @FeHoraChkOutHotel End, 
			Case When ltrim(rtrim(@CoTipoDesaHotel))='' Then Null Else @CoTipoDesaHotel End ,
			Case When ltrim(rtrim(@CoCiudadHotel))='' Then Null Else @CoCiudadHotel End ,
	@UserMod )