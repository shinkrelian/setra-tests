﻿Create Procedure dbo.MAFECHASNAC_Sel_DescripcionFechaEspecialOutPut
@Fecha smalldatetime,
@IDUbigeo char(6),
@pDescripcion varchar(Max) output
As
Set NoCount On
Begin
	Set @pDescripcion = dbo.FnDescripcionFechaEspecial(@Fecha,@IDUbigeo)
End
