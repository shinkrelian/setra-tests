﻿Create Procedure [dbo].[MAOPCIONES_InsaNiveles]
	@IDOpc	char(6),
	@UserMod	char(4)
As
	Set NoCount On
	
	Insert Into MANIVELOPC(IDOpc, IDNivel, Consultar, Grabar, Actualizar, Eliminar, UserMod)
	Select @IDOpc,IDNivel,0,0,0,0,@UserMod From MANIVELUSUARIO
