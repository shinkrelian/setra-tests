﻿
Create Procedure [Dbo].[MAESPECIALIDAD_Upd]
@IDEspecialidad char(2),
@Descripcion varchar(150),
@UserMod char(2)
As
	Set NoCount On
	Update MAESPECIALIDAD 
		Set Descripcion = @Descripcion
		Where IDEspecialidad = @IDEspecialidad
