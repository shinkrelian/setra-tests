﻿
--JRF-20130409-Nuevo Campo DiasReconfirmación
--JRF-20150114-Ajustar el Campo del IDCabCopia
--JHD-20150114-Agregar campo NuSerieCliente
--HLF-20160209- @NuPedInt int,	case when @NuPedInt=0 then NuPedInt else @NuPedInt end
--HLF-20160215-If @NuPedInt<>0		case when @NuPedInt=0 then ....3 veces
--PPMG-20160420-Se agrego el campo @IDContactoCliente char(3)
CREATE PROCEDURE [dbo].[COTICAB_Ins_CopiarNuevoUsuario]          
	@IDCab int,          
	@IDUsuarioNue char(4),          
	@UserMod char(4),          
	@NuPedInt int=0,
	@pIDCab int output          
As          
	Set NoCount On          
           
	Declare @FechaMesActual varchar(4)=SubString((convert(varchar,getdate(),112)),3,4)          
	Declare @siCotizacion int=IsNull((Select MAX(Cotizacion) From COTICAB           
	Where Left(Cotizacion,4)=@FechaMesActual), CAST(@FechaMesActual+'0000' AS INT)  )+1          
	Declare @Cotizacion varchar(8)=Cast(@siCotizacion as varchar(8))           
	Declare @DocWordVersion tinyint=0
	Declare @CoClientePedido char(6)=''
	If @NuPedInt<>0
		Begin
		Exec PEDIDO_SelVersionCotizacion @NuPedInt,@DocWordVersion output 		
		Select @CoClientePedido=CoCliente from PEDIDO where NuPedInt=@NuPedInt
		End                

	INSERT INTO COTICAB       
	([Cotizacion]      
			,[Estado]      
			,[IDCliente]
			,[IDContactoCliente]
			,[Titulo]      
			,[TipoPax]      
			,[CotiRelacion]      
			,[IDUsuario]      
			,[NroPax]      
			,[NroLiberados]      
			,[Tipo_Lib]      
			,[Margen]      
			,[IDBanco]      
			,[IDFile]      
			,[IDTipoDoc]      
			,[Observaciones]      
			,[FecInicio]      
			,[FechaOut]      
			,[Cantidad_dias]      
			,[FechaSeg]      
			,[DatoSeg]      
			,[TipoCambio]      
			,[DocVta]      
			,[TipoServicio]      
			,[IDIdioma]      
			,[Simple]      
			,[Twin]      
			,[Matrimonial]      
			,[Triple]      
			,[IDubigeo]      
			,[Notas]      
			,[Programa_Desc]      
			,[PaxAdulE]      
			,[PaxAdulN]      
			,[PaxNinoE]      
			,[PaxNinoN]      
			,[Aporte]      
			,[AporteMonto]      
			,[AporteTotal]      
			,[Redondeo]      
			,[IDCabCopia]      
			,[DocWordIDIdioma]      
			,[DocWordRutaImagen]      
			,[DocWordSubTitulo]      
			,[DocWordTexto]      
			,[IdCoti_Serie]      
			,[Categoria]      
			,[IDUsuarioRes]      
			,[IDUsuarioOpe]      
			,[FechaReserva]      
			,[FechaOperaciones]     
			,[DiasReconfirmacion]   
			,[coTipoVenta]   
			,[NuSerieCliente]
			,NuPedInt
			,DocWordVersion
			,[UserMod]      
			,[FecMod])      
	SELECT @Cotizacion      
		,'P'      
		,case when @NuPedInt=0 then IDCliente else @CoClientePedido end
		,case when @NuPedInt=0 then IDContactoCliente else NULL end
		,[Titulo]      
		,[TipoPax]      
		,[CotiRelacion]      
		,@IDUsuarioNue      
		,[NroPax]      
		,[NroLiberados]      
		,[Tipo_Lib]      
		,[Margen]      
		,[IDBanco]      
		,''      
		,[IDTipoDoc]      
		,[Observaciones]      
		,[FecInicio]      
		,[FechaOut]      
		,[Cantidad_dias]      
		,[FechaSeg]      
		,[DatoSeg]      
		,[TipoCambio]      
		,[DocVta]      
		,[TipoServicio]      
		,[IDIdioma]      
		,[Simple]      
		,[Twin]      
		,[Matrimonial]      
		,[Triple]      
		,[IDubigeo]      
		,[Notas]      
		,[Programa_Desc]      
		,[PaxAdulE]      
		,[PaxAdulN]      
		,[PaxNinoE]      
		,[PaxNinoN]      
		,[Aporte]      
		,[AporteMonto]      
		,[AporteTotal]      
		,[Redondeo]      
		,@IDCab As [IDCabCopia]
		,[DocWordIDIdioma]      
		,[DocWordRutaImagen]      
		,[DocWordSubTitulo]      
		,[DocWordTexto]      
		,[IdCoti_Serie]      
		,[Categoria]      
		,'0000'      
		,'0000'      
		,Null      
		,Null      
		,[DiasReconfirmacion]    
		,[CoTipoVenta]  
		,[NuSerieCliente]
		,case when @NuPedInt=0 then NuPedInt else @NuPedInt end
		,case when @NuPedInt=0 then DocWordVersion else @DocWordVersion end 
		,@UserMod      
		,GETDATE()      
	From COTICAB Where IDCAB=@IDCab          
           
	Set @pIDCab =(Select MAX(IDCAB) From COTICAB)          


