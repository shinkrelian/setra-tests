﻿Create Procedure dbo.DOCUMENTO_UpdTotalesAjustePrecio
	@IDCab int,    
	@IDDebitMemo char(10),
	@UserMod char(4)
As
	Set Nocount On
	Declare @TotalCD_FAC numeric(10,2)=0
	Declare @TotalCD_BOL numeric(10,2)=0
	Declare @TotalCD_BLE numeric(10,2)=0
	Declare @TotalImptoCD_FAC numeric(10,2)=0
	Declare @TotalImptoCD_BOL numeric(10,2)=0
	Declare @TotalImptoCD_BLE numeric(10,2)=0
	
	Select @TotalCD_FAC=isnull(sum(Total),0),@TotalImptoCD_FAC=isnull(sum(TotalImpto),0) From
	(
		Select Case When sd.idTipoOC='006' Or sd.idTipoOC='001' Then 'FAC' 
				Else 
				Case When sd.idTipoOC='011' Then
					'BLE'
				Else
					'BOL' 
				End
			End as TipoDoc,

		cd.Total*cd.NroPax as Total, 
		cd.TotImpto*cd.NroPax as TotalImpto 
		From COTIDET cd Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
		And cd.IDCAB=@IDCab And cd.IDDebitMemo=@IDDebitMemo
	) as X
	Where TipoDoc='FAC'
	
	Select @TotalCD_BOL=isnull(sum(Total),0),@TotalImptoCD_BOL=isnull(sum(TotalImpto),0) From
	(
		Select Case When sd.idTipoOC='006' Or sd.idTipoOC='001' Then 'FAC' 
				Else 
				Case When sd.idTipoOC='011' Then
					'BLE'
				Else
					'BOL' 
				End
			End as TipoDoc,
		cd.Total*cd.NroPax as Total, 
		cd.TotImpto*cd.NroPax as TotalImpto 
		From COTIDET cd Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
		And cd.IDCAB=@IDCab And cd.IDDebitMemo=@IDDebitMemo
	) as X
	Where TipoDoc='BOL'
	
	Select @TotalCD_BLE=isnull(sum(Total),0),@TotalImptoCD_BLE=isnull(sum(TotalImpto),0) From
	(
		Select Case When sd.idTipoOC='006' Or sd.idTipoOC='001' Then 'FAC' 
				Else 
				Case When sd.idTipoOC='011' Then
					'BLE'
				Else
					'BOL' 
				End
			End as TipoDoc,

		cd.Total*cd.NroPax as Total, 
		cd.TotImpto*cd.NroPax as TotalImpto 
		From COTIDET cd Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
		And cd.IDCAB=@IDCab And cd.IDDebitMemo=@IDDebitMemo
	) as X
	Where TipoDoc='BLE'
	
	Declare @TotalDocBOL numeric(9,2)=0	
	Declare @TotalImptoDocBOL numeric(8,2)=0	
	Declare @TotalDocFAC numeric(9,2)=0	
	Declare @TotalImptoDocFAC numeric(8,2)=0	
	Declare @TotalDocBLE numeric(9,2)=0	
	Declare @TotalImptoDocBLE numeric(8,2)=0	

	Declare @NuDocumBOL char(10), @NuDocumFAC char(10), @NuDocumBLE char(10),
	@TotalDM numeric(8,2)=0, @TotalIGVDM numeric(8,2)=0
	
	
	Select --Top 1 --@TotalDocBOL=SsTotalDocumUSD, @TotalImptoDocBOL=SsIGV, 
	@NuDocumBOL=NuDocum 
	From DOCUMENTO Where IDCab=@IDCab And FlPorAjustePrecio=1 
		And FlAjustePrecioUpdMontos=0
		--And IDTipoDoc In('NDB','BOL') Order by NuDocum
		And ((IDTipoDoc='NDB' And IDTipoDocVinc='BOL') Or (IDTipoDoc='BOL' And IDTipoDocVinc is null))
		
	Select --Top 1 --@TotalDocFAC=SsTotalDocumUSD, @TotalImptoDocFAC=SsIGV, 
	@NuDocumFAC=NuDocum 
	From DOCUMENTO Where IDCab=@IDCab And FlPorAjustePrecio=1 
		And FlAjustePrecioUpdMontos=0
		--And IDTipoDoc In('NDB','FAC') Order by NuDocum Desc
		And ((IDTipoDoc='NDB' And IDTipoDocVinc='FAC') Or (IDTipoDoc='FAC' And IDTipoDocVinc is null))

	Select --Top 1 --@TotalDocFAC=SsTotalDocumUSD, @TotalImptoDocFAC=SsIGV, 
	@NuDocumBLE=NuDocum 
	From DOCUMENTO Where IDCab=@IDCab And FlPorAjustePrecio=1 
		And FlAjustePrecioUpdMontos=0
		--And IDTipoDoc In('NDB','FAC') Order by NuDocum Desc
		And ((IDTipoDoc='NDB' And IDTipoDocVinc='BLE') Or (IDTipoDoc='BLE' And IDTipoDocVinc is null))

		
	Select @TotalDM=Total, @TotalIGVDM=TotalIGV From DEBIT_MEMO Where IDDebitMemo=@IDDebitMemo
	
	--(@TotalCD_FAC+@TotalCD_BOL)	@TotalDM
	--@TotalCD_FAC	x
	If (@TotalCD_FAC+@TotalCD_BOL+@TotalCD_BLE)>0
		Begin
		Set @TotalDocFAC=(@TotalCD_FAC*@TotalDM)/(@TotalCD_FAC+@TotalCD_BOL+@TotalCD_BLE)
	
	--(@TotalCD_FAC+@TotalCD_BOL)	@TotalDM
	--@TotalCD_BOL	x
	
		Set @TotalDocBOL=(@TotalCD_BOL*@TotalDM)/(@TotalCD_FAC+@TotalCD_BOL+@TotalCD_BLE)
		Set @TotalDocBLE=(@TotalCD_BLE*@TotalDM)/(@TotalCD_FAC+@TotalCD_BOL+@TotalCD_BLE)
		End
	--(@TotalImptoCD_FAC+@TotalImptoCD_BOL)	@TotalIGVDM
	--@TotalImptoCD_FAC	x
	If (@TotalImptoCD_FAC+@TotalImptoCD_BOL+@TotalImptoCD_BLE)>0
		Begin
		Set @TotalImptoDocFAC=(@TotalImptoCD_FAC*@TotalDM)/(@TotalImptoCD_FAC+@TotalImptoCD_BOL+@TotalImptoCD_BLE)
	
	--(@TotalImptoCD_FAC+@TotalImptoCD_BOL)	@TotalIGVDM
	--@TotalImptoCD_BOL	x	
	
		Set @TotalImptoDocBOL=(@TotalImptoCD_BOL*@TotalDM)/(@TotalImptoCD_FAC+@TotalImptoCD_BOL+@TotalImptoCD_BLE)
		Set @TotalImptoDocBLE=(@TotalImptoCD_BLE*@TotalDM)/(@TotalImptoCD_FAC+@TotalImptoCD_BOL+@TotalImptoCD_BLE)
		End
	
	
	
	If @TotalDocFAC>0
		Begin
		Update DOCUMENTO_DET_TEXTO Set SsTotal=@TotalDocFAC
		Where NuDocum+' '+IDTipoDoc In (Select NuDocum+' '+IDTipoDoc 
			From DOCUMENTO 			
				Where NuDocum=@NuDocumFAC 
				And FlPorAjustePrecio=1 
				And FlAjustePrecioUpdMontos=0				
				And ((IDTipoDoc='NDB' And IDTipoDocVinc='FAC') Or (IDTipoDoc='FAC' And IDTipoDocVinc is null))						
				)
		And SsTotal<>0	
		
		Update DOCUMENTO Set SsTotalDocumUSD=@TotalDocFAC, SsIGV=@TotalImptoDocFAC, 
		FecMod=GETDATE(), UserMod=@UserMod, FlAjustePrecioUpdMontos=1
		Where NuDocum=@NuDocumFAC --And IDTipoDoc='NDB'
		And FlPorAjustePrecio=1 
		And FlAjustePrecioUpdMontos=0
		--And IDTipoDoc In('NDB','FAC')
		And ((IDTipoDoc='NDB' And IDTipoDocVinc='FAC') Or (IDTipoDoc='FAC' And IDTipoDocVinc is null))
		End
	If @TotalDocBOL>0
		Begin
		Update DOCUMENTO_DET_TEXTO Set SsTotal=@TotalDocBOL
		Where NuDocum+' '+IDTipoDoc In (Select NuDocum+' '+IDTipoDoc 
			From DOCUMENTO 			
				Where NuDocum=@NuDocumBOL 
				And FlPorAjustePrecio=1 
				And FlAjustePrecioUpdMontos=0				
				And ((IDTipoDoc='NDB' And IDTipoDocVinc='BOL') Or (IDTipoDoc='BOL' And IDTipoDocVinc is null))						
				)
		And SsTotal<>0		
		
		Update DOCUMENTO Set SsTotalDocumUSD=@TotalDocBOL, SsIGV=@TotalImptoDocBOL, 
		FecMod=GETDATE(), UserMod=@UserMod, FlAjustePrecioUpdMontos=1
		Where NuDocum=@NuDocumBOL --And IDTipoDoc='NDB'	
		And FlPorAjustePrecio=1 
		And FlAjustePrecioUpdMontos=0
		--And IDTipoDoc In('NDB','BOL')
		And ((IDTipoDoc='NDB' And IDTipoDocVinc='BOL') Or (IDTipoDoc='BOL' And IDTipoDocVinc is null))
		End
	If @TotalDocBLE>0
		Begin
		Update DOCUMENTO_DET_TEXTO Set SsTotal=@TotalDocBLE
		Where NuDocum+' '+IDTipoDoc In (Select NuDocum+' '+IDTipoDoc 
			From DOCUMENTO 			
				Where NuDocum=@NuDocumBLE 
				And FlPorAjustePrecio=1 
				And FlAjustePrecioUpdMontos=0				
				And ((IDTipoDoc='NDB' And IDTipoDocVinc='BLE') Or (IDTipoDoc='BLE' And IDTipoDocVinc is null))						
				)
		And SsTotal<>0
	
		Update DOCUMENTO Set SsTotalDocumUSD=@TotalDocBLE, SsIGV=@TotalImptoDocBLE, 
		FecMod=GETDATE(), UserMod=@UserMod, FlAjustePrecioUpdMontos=1
		Where NuDocum=@NuDocumBLE --And IDTipoDoc='NDB'	
		And FlPorAjustePrecio=1 
		And FlAjustePrecioUpdMontos=0
		--And IDTipoDoc In('NDB','BOL')
		And ((IDTipoDoc='NDB' And IDTipoDocVinc='BLE') Or (IDTipoDoc='BLE' And IDTipoDocVinc is null))
		End
