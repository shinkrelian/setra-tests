﻿Create Procedure dbo.MAENTREVISTAPROVEEDOR_SelxIDProveedor 
@IDProveedor char(6)
As
Select idio.IDidioma,IsNull(etp.FlConocimiento,0) as FlConocimiento,IsNull(etp.IDProveedor,@IDProveedor) as IDProveedor,
	   IsNull(Convert(char(10),etp.FeEntrevista,103),'') as FeEntrevita,
	   IsNull(etp.IDUserEntre,'') as IDUserEntre,IsNull(uEnt.Usuario,'') as UsuarioEntrevistador,IsNull(etp.TxComentario,'') as TxComentario,
	   IsNull(etp.Estado,'') as Estado
from MAIDIOMAS idio Left Join MAENTREVISTAPROVEEDOR etp On idio.IDidioma=etp.IDIdioma And etp.IDProveedor= @IDProveedor
Left Join MAUSUARIOS uEnt On etp.IDUserEntre=uEnt.IDUsuario
Where idio.FlReqEntrevista=1 
