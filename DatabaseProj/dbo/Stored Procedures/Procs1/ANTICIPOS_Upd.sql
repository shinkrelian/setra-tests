﻿	
Create Procedure ANTICIPOS_Upd
@IDAnticipo int,
@Fecha smalldatetime,
@IdTipoDoc char(3),
@IdSerie char(3),
@Numero char(8),
@Correlativo char(2),
@IGV numeric(6,2),
@IDMoneda char(3),
@TC numeric(8,2),
@Anulado bit,
@IDCab int,
@IDFile char(8),
@IDCliente char(6),
@NroPax tinyint,
@Referencia varchar(100),
@IDUsuarioResponsable char(4),
@IDbanco char(3),
@FechaIN smalldatetime,
@FechaOut smalldatetime,
@Exportacion numeric(8,2),
@Gravado numeric(8,2),
@Nogravado numeric(8,2),
@UserMod char(4)
As
	Set NoCount On
	UPDATE [dbo].[ANTICIPOS]
	   SET [Fecha] = @Fecha
		  ,[IdSerie] = Case When ltrim(rtrim(@IdSerie))='' Then Null Else @IdSerie End
		  ,[Numero] = Case When ltrim(rtrim(@Numero))='' Then Null Else @Numero End
		  ,[Correlativo] = Case When ltrim(rtrim(@Correlativo))='' Then Null Else @Correlativo End
		  ,[IGV] = Case When @IGV=0 Then Null Else @IGV End
		  ,[IDMoneda] = @IDMoneda
		  ,[TC] = Case When @TC=0 Then Null Else @TC End
		  ,[Anulado] = @Anulado
		  ,[IDCab] = @IDCab
		  ,[IDFile] = @IDFile
		  ,[IDCliente] = @IDCliente 
		  ,[NroPax] = @NroPax
		  ,[Referencia] = Case When ltrim(rtrim(@Referencia))='' Then Null Else @Referencia End
		  ,[IDUsuarioResponsable] = @IDUsuarioResponsable
		  ,[IDbanco] = @IDbanco
		  ,[FechaIN] = @FechaIN
		  ,[FechaOut] = @FechaOut
		  ,[Exportacion] = Case When @Exportacion = 0 Then Null Else @Exportacion End
		  ,[Gravado] = Case When @Gravado = 0 Then Null Else @Gravado End
		  ,[Nogravado] = Case When @Nogravado = 0 Then Null Else @Nogravado End
		  ,[UserMod] = @UserMod
		  ,[FecMod] = GetDate()
	 WHERE [IDAnticipo]= @IDAnticipo
