﻿Create Procedure dbo.MABANCOS_Sel_CboList 
@Todos bit
As
Set NoCount On
Select '' as IDBanco,'<TODOS>' as Descripcion
Where @Todos = 1
Union
select IDBanco, Sigla as Descripcion
from MABANCOS where IDBanco <> '000'
