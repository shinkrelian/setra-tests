﻿Create Procedure dbo.MACORREOSBIBLIAPROVEEDOR_Sel_ExistsPK
@IDProveedor char(6),
@Correo varchar(150),
@pExists bit Output
as
Set NoCount On
	Set @pExists = 0
	if Exists(select IDProveedor from MACORREOSBIBLIAPROVEEDOR where IDProveedor = @IDProveedor and Correo = @Correo)
		Set @pExists = 1
