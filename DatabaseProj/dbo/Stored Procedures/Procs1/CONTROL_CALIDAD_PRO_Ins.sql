﻿--JRF-20150216-Agregar Nuevos campos [TxComentario,IDPax]
--JHD-20150216-Agregar Nuevos campos [TxComentario,IDPax]
--JRF-20151125-Ampliar TxComentario a TEXT
CREATE Procedure dbo.CONTROL_CALIDAD_PRO_Ins  
@IDCab int,
@TxComentario Text,
@IDPax int,
@UserMod char(4),  
@CoProveedor char(6),
@CoCliente char(6),  
@pNuControlCalidad int OutPut  
As  
 Set NoCount On  
 Declare @NuControlCalidad int = (Select IsNull(Max(NuControlCalidad),0) from CONTROL_CALIDAD_PRO)+1  
 Insert into CONTROL_CALIDAD_PRO  
   (NuControlCalidad,  
    IDCab,
    TxComentario,  
    IDPax,
	CoProveedor,
	CoCliente,
    UserMod,  
    FecMod)  
  values(@NuControlCalidad,  
      Case When @IDCab = 0 Then Null Else @IDCab End,
      Case When Ltrim(Rtrim(Cast(@TxComentario as varchar(Max)))) = '' Then Null Else @TxComentario End,
      Case When @IDPax = 0 Then Null Else @IDPax End,
	  Case When Ltrim(Rtrim(@CoProveedor)) = '' Then Null Else Ltrim(Rtrim(@CoProveedor)) End,
	  Case When Ltrim(Rtrim(@CoCliente)) = '' Then Null Else Ltrim(Rtrim(@CoCliente)) End,
      @UserMod,  
      Getdate())  
 Set @pNuControlCalidad=@NuControlCalidad  
