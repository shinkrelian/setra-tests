﻿CREATE procedure [dbo].[MACLIENTES_sel_RptGeneral] 
@Persona char(1),
@Tipo char(1),
@IDVendedor char(6),
@IDPais char(6),
@IDCiudad char (6),
@Domiciliado bit 
as
	Set NoCount On
	
	select distinct c.IDCliente,c.NumIdentidad,
	    case Persona when 'J' then 'JURIDICA' when 'N' then 'NATURAL' End as Persona
	,c.RazonSocial,c.Direccion,
	Telefono=	
		Case When Right(isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,''),1)='/' Then 
		isnull(c.Telefono1,'')
		Else
			Case When Left(isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,''),1)='/' Then 
			isnull(c.Telefono2,'')
			else
			isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,'')
			End
		End
	,c.Web,u.Descripcion as Ubigeo,us.Nombre , 
	(select COUNT(*) from MACONTACTOSCLIENTE where IDCliente=c.IDCliente) as CantContactos --, c.*
	From MACLIENTES c Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo
	Left Join MAUBIGEO up On u.IDPais=up.IDubigeo
	Left Join MACONTACTOSCLIENTE cc On c.IDCliente=cc.IDCliente 
	--Left join MAMONEDAS	 mo On mo.IDMoneda=c.IDMoneda 
	Left Join MAUSUARIOS us On c.IDvendedor=us.IDUsuario
	  where (c.Persona=@Persona Or LTRIM(RTRIM(@Persona))='') And 
			(c.Tipo=@Tipo Or LTRIM(RTRIM(@Tipo))='') And 
			(c.IDvendedor=@IDVendedor Or LTRIM(RTRIM(@IDVendedor))='') And
			(u.IDPais=@IDPais Or LTRIM(RTRIM(@IDPais))='') And
			(c.IDCiudad=@IDCiudad Or LTRIM(RTRIM(@IDCiudad))='') And
			(c.Domiciliado=@Domiciliado) And c.Activo='A'
