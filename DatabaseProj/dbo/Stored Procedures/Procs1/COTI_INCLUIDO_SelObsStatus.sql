﻿Create Procedure dbo.COTI_INCLUIDO_SelObsStatus
@IDCab int
As
Set NoCount On 
SELECT Isnull(i.TxObsStatus,'') as TxObsStatus
FROM COTI_INCLUIDO ci Left Join MAINCLUIDO i On ci.IDIncluido = i.IDIncluido
Where ci.IDCAB = @IDCab and cast(Isnull(i.TxObsStatus,'') as varchar(Max)) <> ''
