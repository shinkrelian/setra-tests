﻿Create Procedure [dbo].[COTIDET_PAX_Sel_ExisteOutput]
	@IDDet	int,
	@IDPax	int,
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDDet From COTIDET_PAX
		WHERE IDDet=@IDDet	And IDPax=@IDPax)
		Set @pExiste=1
	Else
		Set @pExiste=0
