﻿ --JHD-20150518-NuDocum=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
 --JHD-20150518-NuDocum2=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,  
  --JHD-20150812-NuDocInterno= dp.CardCodeSAP,
   --JHD-20150826- NuDocum= case when FlCorrelativo=1 then '' else
				-- case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
				--end,
 --JHD-20150826- NuDocum2=case when FlCorrelativo=1 then '' else
				-- case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			    -- end, 
--HLF-20151222-or dp.NuDocumProv in 	(Select NuDocum_Multiple ...   And NuDocum_Multiple is null
CREATE Procedure [dbo].[DOCUMENTO_PROVEEDOR_Sel_ListxNuPresupuesto]  
 @NuDocumProv int,        
 @NuVoucher INT,  
 @NuDocum varchar(15),  
 @CoTipoDoc char(3),         
 @FlActivo bit     
AS        
  
SET NOCOUNT ON       
SELECT     dp.NuDocumProv,        
   dp.NuVoucher,        
   --dp.NuDocum,        
   --NuDocum2=dbo.FnFormatearDocumProvee(dp.NuDocum),    
   --NuDocum=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
    NuDocum= case when FlCorrelativo=1 then '' else
				 case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,
   --NuDocum2=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
     NuDocum2=case when FlCorrelativo=1 then '' else
				case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,  

   --dp.NuDocInterno,        
    NuDocInterno= dp.CardCodeSAP,        
   dp.CoTipoDoc,         
          td.Descripcion,        
          dp.FeEmision,        
          dp.FeRecepcion,        
          dp.CoTipoDetraccion,         
          PorcentajeDet=(select isnull(SsTasa,0)*100 from dbo.MATIPODETRACCION where CoTipoDetraccion=dp.CoTipoDetraccion),        
          dp.CoMoneda,        
          mo.Descripcion AS Moneda,        
          SsNeto,        
          SsOtrCargos,        
          SsDetraccion,        
          dp.SsIGV,        
          dp.SSTotalOriginal,         
           dp.SSTipoCambio,        
          dp.SSTotal,        
          dp.FlActivo        
FROM         DOCUMENTO_PROVEEDOR dp       
INNER JOIN  MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc INNER JOIN        
                      MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda        
WHERE     (dp.NuDocumProv = @NuDocumProv or @NuDocumProv=0) AND        
(dp.NuPreSob=@NuVoucher or 
	dp.NuDocumProv in 	(Select NuDocum_Multiple From DOCUMENTO_PROVEEDOR Where NuPreSob=@NuVoucher)
	)  
And NuDocum_Multiple is null
AND    
(ltrim(rtrim(@NuDocum))='' OR dp.NuDocum LIKE '%'+@NuDocum+'%' ) AND         
                      (dp.CoTipoDoc = @CoTipoDoc or ltrim(rtrim(@CoTipoDoc))='') AND        
                        (dp.FlActivo = @FlActivo or @FlActivo =0)      
