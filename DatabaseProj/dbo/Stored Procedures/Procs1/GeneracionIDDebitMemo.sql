﻿--HLF-20131210-Cambiando tipo de dato @siCorr smallint  
--HLF-20150414- if @IDTipo = 'A'  ....
--PPMG-20151215-En el If @IDTipo='I' se aumento el Else del case when
--PPMG-20151217-Se aumento en la condición AND SUBSTRING(IDDebitMemo,1,8)=@IDFile
CREATE Procedure [dbo].[GeneracionIDDebitMemo]
@IDCab int,  
@IDTipo char(1),  
@IDDebitMemo varchar(10) output  
AS
BEGIN
 Set NoCount On  
 Declare @IDFile char(8) = (select IDFile from coticab where idcab = @IDCab)  
 --Declare @siCorr tinyint
 Declare @siCorr smallint  
 declare @iCorrTabla int   
  
 If @IDTipo='I'              
   Begin            
	   Set @IDDebitMemo = @IDFile              
	   --Set @siCorr = ISNull((Select SUBSTRING(Max(IDDebitMemo),9,2) From DEBIT_MEMO Where IDCab=@IDCab),-1)              
	   Set @siCorr = -1
	   if Exists(Select * From DEBIT_MEMO Where IDCab=@IDCab AND IDTipo='I')
	   Begin
			Set @siCorr =CONVERT(smallint,ISNull((Select SUBSTRING(ISNULL(Max(IDDebitMemo),'00'),9,2) From DEBIT_MEMO Where IDCab=@IDCab AND IDTipo='I'),-1))
	   End
	   Set @siCorr += 1                
	   Set @IDDebitMemo = @IDDebitMemo + Case When @siCorr<=9 Then '0' Else '' End + CAST(@siCorr as CHAR(2))              
   End              
 Else
	Begin
	 If @IDTipo='R'              
		Begin                 
		 Execute dbo.Correlativo_SelOutput 'DEBIT_MEMO',1,4,@iCorrTabla output                 
		 Set @IDDebitMemo = CAST(YEAR(GETDATE()) AS CHAR(4))+ REPLICATE('0',4-LEN(Cast(@iCorrTabla as varchar(10))))+ Cast(@iCorrTabla as varchar(10))              
		End  
	else  
		Begin  
			if @IDTipo = 'C'  
				Begin  
					Execute dbo.Correlativo_SelOutput 'DEBIT_MEMO_COUNTER',1,7,@iCorrTabla output  
					Set @IDDebitMemo = 'CTR'+REPLICATE('0',7-LEN(ltrim(rtrim(CAST(@iCorrTabla AS varchar(10)))))) +CAST(@iCorrTabla AS varchar(10))  
				End  
  
			if @IDTipo = 'S'  
				Begin  
					Execute dbo.Correlativo_SelOutput 'DEBIT_MEMO_SELLS',1,7,@iCorrTabla output  
					Set @IDDebitMemo = 'SLS'+REPLICATE('0',7-LEN(ltrim(rtrim(CAST(@iCorrTabla As varchar(10)))))) +CAST(@iCorrTabla AS varchar(10))  
				End  
 
			if @IDTipo = 'A'  
				Begin  
					Execute dbo.Correlativo_SelOutput 'DEBIT_MEMO_VTAS_ADIC_APT',1,7,@iCorrTabla output  
					Set @IDDebitMemo = 'APT'+REPLICATE('0',7-LEN(ltrim(rtrim(CAST(@iCorrTabla As varchar(10)))))) +CAST(@iCorrTabla AS varchar(10))  
				End  

		 End   
	End
END
