﻿
Create Procedure ANTICIPOS_Sel_Pk
@IDAnticipo int
As
	Set NoCount On
	Select [IDAnticipo]
      ,a.[Fecha]
      ,a.[IdTipoDoc]
      ,isnull([IdSerie],'') As IdSerie
      ,isnull([Numero],'') As Numero
      ,isnull([Correlativo],'') As Correlativo
      ,isnull([IGV],'0.00') As IGV
      ,[IDMoneda]
      ,isnull([TC],'0.00') As TC
      ,[Anulado]
      ,a.[IDCab]
      ,a.[IDFile]
      ,a.idCliente
      ,c.RazonComercial As Cliente
      ,a.[NroPax]
      ,isnull([Referencia],'') As Referencia
      ,[IDUsuarioResponsable]
      ,a.[IDbanco]
      ,[FechaIN]
      ,a.[FechaOut]
      ,isnull([Exportacion],'0.00') As Exportacion
      ,isnull([Gravado],'0.00') As Gravado
      ,isnull([Nogravado],'0.00') As Nogravado
    from ANTICIPOS a Left Join MACLIENTES c On a.IDCliente = c.IDCliente
    where IDAnticipo = @IDAnticipo
