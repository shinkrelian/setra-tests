﻿--JRF-20160405-(IDNacionalidad = '000323' Or Residente=1)
CREATE Procedure dbo.COTIDET_PAX_SoloPeruanos
@IDDet int,
@pSoloPer bit output
As
Set NoCount On
Set @pSoloPer = 0
select @pSoloPer = 1
from COTIDET_PAX cdp Left Join COTIPAX cp On cdp.IDPax=cp.IDPax
where IDDET=@IDDet And (IDNacionalidad = '000323' Or Residente=1)
