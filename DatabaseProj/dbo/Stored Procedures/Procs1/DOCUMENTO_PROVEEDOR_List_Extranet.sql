﻿--JHD-20151214-Se corrigio el Estado de los documentos
--JHD-20151216-Se agregaron las columnas NuSerie,NuDocum2,CoTipoDoc,Correo
--JHD-20151216-Se agrego inner join MAPROVEEDORES p on p.IDProveedor=d.CoProveedor  en todos los unions
--JHD-20151217-Se agregaron las columnas: ,Total_Detraccion, NoTipoDetraccion ,Moneda_Detraccion,Fecha_Det,Constancia_Det
--JHD-20151223-FlReferencia= case when (select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)='000323' then cast(0 as bit) else cast(1 as bit) end
--JHD-20151223-,Referencia=isnull(case when FlReferencia=1 then (select CounterRef from [SAP].[SBO_SETO_PROD_F1].[dbo].[OVPM] where DocNum=DocNum_Pago) else '' end,'')

--JHD-20160303-Se agrego la logica para cadena de proveedores
--JHD-20160303-Se agrego un union para prepagos con vouchers
--JHD-20160308-and year(d.FeRecepcion)>=2016
--JHD-20160308-d.FeEmision,
--JHD-20160308-order by FeEmision,FeRecepcion
CREATE procedure [dbo].[DOCUMENTO_PROVEEDOR_List_Extranet]
 @NuVoucher varchar(14),   
 @NuDocum varchar(30),                   
 @CoProveedor char(6),
 @FecEmisionInicio smalldatetime,              
 @FecEmisionFinal smalldatetime,
 @CoEstado char(2),
 @CoOperacion varchar(14)
 AS
BEGIN

 declare @EsCadena bit =0

if @CoProveedor<>''
	set @EsCadena = (select EsCadena from MAPROVEEDORES where IDProveedor=@CoProveedor)


select   cast( NuVoucher as varchar(30)) as NuVoucher ,
	NuDocum,
	TipoDoc,
	Moneda,
	Total,
	FeEmision,
	FeRecepcion,
	FeVencimiento,
	FePago,
	Importe_Pagado,
	NoOperacion,
	Estado

	,NuSerie
	,NuDocum2
	,CoTipoDoc
	,Correo

	--,EsDetraccion
	,Total_Detraccion
	,NoTipoDetraccion
	,Moneda_Detraccion
	,Fecha_Det
	,Constancia_Det
	,Referencia
	--,DocNum_Pago
FROM (
select   NuVoucher,
	
	NuDocum,
	TipoDoc,
	Moneda,
	Total,
	FeEmision,
	FeRecepcion,
	FeVencimiento,
	FePago,
	Importe_Pagado,
	NoOperacion,
	CoEstado=case when  (NoOperacion is not null) then 'PG'  ELSE (case when CoEstado ='OB' THEN 'OB' ELSE 'PD' END) END,
	Estado=case when  (NoOperacion is not null) then 'PAGADO'  ELSE (case when CoEstado ='OB' THEN 'OBSERVADO' ELSE 'PENDIENTE' END) END
	,NuSerie
	,NuDocum2
	,CoTipoDoc
	,Correo

	,EsDetraccion
	,Total_Detraccion
	,NoTipoDetraccion
	,Moneda_Detraccion
	,Fecha_Det
	,Constancia_Det

	,DocNum_Pago
	,Referencia
	from 
(
select NuVoucher,
	NuSerie,
	NuDocum,
	TipoDoc,
	Moneda,
	Total,
	FeEmision,
	FeRecepcion,
	FeVencimiento,
	FePago,
	Importe_Pagado,
	NoOperacion=(select TRSFRREF from [SAP].[SBO_SETO_PROD_F1].[dbo].[OVPM] where DocNum=DocNum_Pago),
	CoEstado,--=case when  ((select TRSFRREF from [SAP].[SBO_SETO_PROD_F1].[dbo].[OVPM] where DocNum=DocNum_Pago) is not null) then 'PG'  ELSE (case when CoEstado ='OB' THEN 'OB' ELSE 'PD' END) END,
	--Estado= case when  (NoOperacion is not null) then 'PAGADO' ELSE '' END,
	DocNum_Pago
	,EsDetraccion
	,NuDocum2
	,CoTipoDoc
	,Correo
	,Total_Detraccion
	,NoTipoDetraccion
	,Moneda_Detraccion
	,Fecha_Det=(select top 1 u_syp_dpfc from sap.[SBO_SETO_PROD_F1].dbo.opch where receiptnum=DocNum_Pago and u_syp_dpnm is not null)
	,Constancia_Det=(select top 1 u_syp_dpnm from sap.[SBO_SETO_PROD_F1].dbo.opch where receiptnum=DocNum_Pago and u_syp_dpnm is not null)
	,Referencia=isnull(case when FlReferencia=1 then (select CounterRef from [SAP].[SBO_SETO_PROD_F1].[dbo].[OVPM] where DocNum=DocNum_Pago) else '' end,'')
	from
(
select 
	NuVoucher,
	NuSerie,
	NuDocum,
	TipoDoc,
	Moneda,
	Total,
	FeEmision,
	FeRecepcion,
	FeVencimiento,
	FePago=(select top 1 DocDate from PAGOSEFECTUADOS_SAP p inner join PAGOSEFECTUADOS_SAP_DET pd on p.IDPagoEfectuado=pd.IDPagoEfectuado
	where pd.TransaccionSAP=CardCodeSAP),
		--where pd.CoTipoDoc=CoSAP and pd.NuSerieDoc=NuSerie and pd.NuDocum=NuDocum),
	Importe_Pagado=(select top 1 pd.SsMontoPago from PAGOSEFECTUADOS_SAP p inner join PAGOSEFECTUADOS_SAP_DET pd on p.IDPagoEfectuado=pd.IDPagoEfectuado
		where pd.TransaccionSAP=CardCodeSAP),
	NoOperacion,
	CoEstado,
	DocNum_Pago=(select top 1 p.DocNum from PAGOSEFECTUADOS_SAP p inner join PAGOSEFECTUADOS_SAP_DET pd on p.IDPagoEfectuado=pd.IDPagoEfectuado
		where pd.TransaccionSAP=CardCodeSAP)
	,EsDetraccion

	,NuDocum2
	,CoTipoDoc
	,Correo
	,Total_Detraccion
	,NoTipoDetraccion
	,Moneda_Detraccion
	,FlReferencia
 FROM
(
-- DOCUMENTOS DE VOUCHER
	select distinct
	--NuVoucher=case when d.CoObligacionPago='VOU' THEN cast(d.NuVoucher as varchar(14)) when d.CoObligacionPago='OSV' THEN ( SELECT SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) FROM ORDEN_SERVICIO os where os.NuOrden_Servicio=d.NuOrden_Servicio ) ELSE '' END , 
	NuVoucher=cast(d.NuVoucher as varchar(14)),
	isnull(d.NuSerie,'') as NuSerie,
	NuDocum= case when d.NuSerie is null then '' else d.NuSerie+'-' end + d.NuDocum,
	TipoDoc=td.Descripcion,
	isnull(m.Simbolo,'') as Moneda,
	--isnull(d.SSTotal,0) as Total,
	Total = case when isnull(d.SsDetraccion,0)>0 then isnull(d.SsNeto,0)+isnull(d.SsIGV,0) else isnull(d.SSTotal,0) end,
	d.FeEmision,
	d.FeRecepcion,
	d.FeVencimiento,
	FePago='',
	Importe_Pagado=0,
	NoOperacion=0,

	CoEstado=vd.CoEstado,
	isnull(td.CoSAP,'') as CoSAP,
	d.CardCodeSAP,
	EsDetraccion=cast(0 as bit)
	--
	,NuDocum2=d.NuDocum
	,td.CoSAP as CoTipoDoc
	,Correo= case when vd.CoEstado='OB' THEN ( case when (select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)='000323' then 'maria.mamani@setours.com' else 'keyla.medina@setours.com' end )
			 else 'pagos1@setours.com'
			 end

	-----
	,isnull(d.SsDetraccion,0) as Total_Detraccion
	--
	,NoTipoDetraccion=isnull((select cast(cast(round(SsTasa*100,2) as numeric(5,2)) as varchar(10))+'% - '+[NoTipoBien] From MATIPODETRACCION Where  CoTipoDetraccion =d.CoTipoDetraccion),'')
 	,case when  isnull(d.SsDetraccion,0) =0 then '' else  'S/.' end as Moneda_Detraccion

	,FlReferencia= case when (select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)='000323' then cast(0 as bit) else cast(1 as bit) end
	from DOCUMENTO_PROVEEDOR d
	inner join MATIPODOC td on d.CoTipoDoc=td.IDtipodoc
	inner JOIN VOUCHER_OPERACIONES_DET VD on vd.IDVoucher=d.NuVoucher and vd.CoMoneda=d.CoMoneda_FEgreso
	inner join mamonedas m on m.IDMoneda=d.CoMoneda
	inner join MAPROVEEDORES p on p.IDProveedor=d.CoProveedor
	where d.CoObligacionPago in ('VOU')
	and d.FlActivo=1

	--and (d.CoProveedor=@CoProveedor or @CoProveedor='')

	and 
		((d.CoProveedor=@CoProveedor or @CoProveedor='')
		
		or (@EsCadena = 1 and d.CoProveedor in (select IDProveedor from MAPROVEEDORES where IDProveedor_Cadena=@CoProveedor) )

		)

	and (
			((Convert(Char(10),@FecEmisionInicio,103)='01/01/1900' and Convert(Char(10),@FecEmisionFinal,103)='01/01/1900') Or              
			(CAST(CONVERT(CHAR(10),d.FeRecepcion,103)AS SMALLDATETIME) between @FecEmisionInicio and @FecEmisionFinal))      
		)
	and (@NuDocum='' OR d.NuDocum LIKE '%'+@NuDocum+'%' )-- AND      
	and (@NuVoucher='' OR d.NuVoucher LIKE '%'+@NuVoucher+'%' )

	and year(d.FeRecepcion)>=2016

	--ORDENES DE SERVICIO
	UNION 

	select distinct
	--NuVoucher=case when d.CoObligacionPago='VOU' THEN cast(d.NuVoucher as varchar(14)) when d.CoObligacionPago='OSV' THEN ( SELECT SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) FROM ORDEN_SERVICIO os where os.NuOrden_Servicio=d.NuOrden_Servicio ) ELSE '' END , 
	NuVoucher=( SELECT SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) ),
	isnull(d.NuSerie,'') as NuSerie,
	NuDocum= case when d.NuSerie is null then '' else d.NuSerie+'-' end + d.NuDocum,
	TipoDoc=td.Descripcion,
	isnull(m.Simbolo,'') as Moneda,
	--isnull(d.SSTotal,0) as Total,
	Total = case when isnull(d.SsDetraccion,0)>0 then isnull(d.SsNeto,0)+isnull(d.SsIGV,0) else isnull(d.SSTotal,0) end,
	d.FeEmision,
	d.FeRecepcion,
	d.FeVencimiento,
	FePago='',
	Importe_Pagado=0,
	NoOperacion=0,

	CoEstado=osm.CoEstado,
	isnull(td.CoSAP,'') as CoSAP,
	d.CardCodeSAP,
	EsDetraccion=cast(0 as bit)
	--
	,NuDocum2=d.NuDocum
	,td.CoSAP as CoTipoDoc
	,Correo= case when osm.CoEstado='OB' THEN ( case when (select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)='000323' then 'maria.mamani@setours.com' else 'keyla.medina@setours.com' end )
			 else 'pagos1@setours.com'
			 end
	,isnull(d.SsDetraccion,0) as Total_Detraccion
		--
	,NoTipoDetraccion=isnull((select cast(cast(round(SsTasa*100,2) as numeric(5,2)) as varchar(10))+'% - '+[NoTipoBien] From MATIPODETRACCION Where  CoTipoDetraccion =d.CoTipoDetraccion),'')
 	,case when  isnull(d.SsDetraccion,0) =0 then '' else  'S/.' end as Moneda_Detraccion
	,FlReferencia= case when (select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)='000323' then cast(0 as bit) else cast(1 as bit) end
	from DOCUMENTO_PROVEEDOR d
	inner join MATIPODOC td on d.CoTipoDoc=td.IDtipodoc
	inner JOIN ORDEN_SERVICIO_DET_MONEDA osm on osm.NuOrden_Servicio=d.NuOrden_Servicio and osm.CoMoneda=d.CoMoneda_FEgreso
	inner JOIN ORDEN_SERVICIO os on os.NuOrden_Servicio=osm.NuOrden_Servicio
	inner join mamonedas m on m.IDMoneda=d.CoMoneda
	inner join MAPROVEEDORES p on p.IDProveedor=d.CoProveedor
	where d.CoObligacionPago in ('OSV')
	and d.FlActivo=1

	--and (d.CoProveedor=@CoProveedor or @CoProveedor='')

	and 
		((d.CoProveedor=@CoProveedor or @CoProveedor='')
		
		or (@EsCadena = 1 and d.CoProveedor in (select IDProveedor from MAPROVEEDORES where IDProveedor_Cadena=@CoProveedor) )

		)

	and (
			((Convert(Char(10),@FecEmisionInicio,103)='01/01/1900' and Convert(Char(10),@FecEmisionFinal,103)='01/01/1900') Or              
			(CAST(CONVERT(CHAR(10),d.FeRecepcion,103)AS SMALLDATETIME) between @FecEmisionInicio and @FecEmisionFinal))      
		)
	and (@NuDocum='' OR d.NuDocum LIKE '%'+@NuDocum+'%' )-- AND      
	and (@NuVoucher='' OR d.NuOrden_Servicio LIKE '%'+@NuVoucher+'%' )
	and year(d.FeRecepcion)>=2016

	UNION

	-- DOCUMENTOS DE PREPAGO CON VOUCHER
		select distinct
	--NuVoucher=case when d.CoObligacionPago='VOU' THEN cast(d.NuVoucher as varchar(14)) when d.CoObligacionPago='OSV' THEN ( SELECT SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) FROM ORDEN_SERVICIO os where os.NuOrden_Servicio=d.NuOrden_Servicio ) ELSE '' END , 
	NuVoucher=v.idvoucher, --cast(d.NuVoucher as varchar(14)),
	isnull(d.NuSerie,'') as NuSerie,
	NuDocum= case when d.NuSerie is null then '' else d.NuSerie+'-' end + d.NuDocum,
	TipoDoc=td.Descripcion,
	isnull(m.Simbolo,'') as Moneda,
	--isnull(d.SSTotal,0) as Total,
	Total = case when isnull(d.SsDetraccion,0)>0 then isnull(d.SsNeto,0)+isnull(d.SsIGV,0) else isnull(d.SSTotal,0) end,
	d.FeEmision,
	d.FeRecepcion,
	d.FeVencimiento,
	FePago='',
	Importe_Pagado=0,
	NoOperacion=0,

	--CoEstado=vd.CoEstado,
	CoEstado=v.CoEstado,--(select vd1.CoEstado from VOUCHER_OPERACIONES_DET vd1 where vd1.idvoucher=d.nuvoucher and vd1.CoMoneda=d.CoMoneda_FEgreso),
	isnull(td.CoSAP,'') as CoSAP,
	d.CardCodeSAP,
	EsDetraccion=cast(0 as bit)
	--
	,NuDocum2=d.NuDocum
	,td.CoSAP as CoTipoDoc
	,Correo= case when v.CoEstado='OB' THEN ( case when (select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)='000323' then 'maria.mamani@setours.com' else 'keyla.medina@setours.com' end )
			 else 'pagos1@setours.com'
			 end

	,isnull(d.SsDetraccion,0) as Total_Detraccion
	--
	,NoTipoDetraccion=isnull((select cast(cast(round(SsTasa*100,2) as numeric(5,2)) as varchar(10))+'% - '+[NoTipoBien] From MATIPODETRACCION Where  CoTipoDetraccion =d.CoTipoDetraccion),'')
 	,case when  isnull(d.SsDetraccion,0) =0 then '' else  'S/.' end as Moneda_Detraccion

	,FlReferencia= case when (select IDPais from MAUBIGEO where IDubigeo=p.IDCiudad)='000323' then cast(0 as bit) else cast(1 as bit) end
		
	from (
		select distinct vo.IDVoucher,o.IDOperacion,o.IDReserva,vo.CoEstado
from VOUCHER_OPERACIONES vo
inner join OPERACIONES_DET od on vo.IDVoucher=od.IDVoucher
inner join operaciones o on o.IDOperacion=od.IDOperacion
inner join MAPROVEEDORES p on o.IDProveedor=p.IDProveedor
where p.IDProveedor=@CoProveedor and vo.IDVoucher<>0
and o.IDReserva is not null
			) as v
inner join ordenpago op on v.IDReserva=op.IDReserva
inner join DOCUMENTO_PROVEEDOR d on d.CoOrdPag=op.IDOrdPag
inner join MATIPODOC td on d.CoTipoDoc=td.IDtipodoc
inner join MAPROVEEDORES p on p.IDProveedor=d.CoProveedor
inner join mamonedas m on m.IDMoneda=d.CoMoneda
	where --d.CoObligacionPago in ('VOU')
	--and 
	d.FlActivo=1

	--and (d.CoProveedor=@CoProveedor or @CoProveedor='')

	and 
		((d.CoProveedor=@CoProveedor or @CoProveedor='')
		
		or (@EsCadena = 1 and d.CoProveedor in (select IDProveedor from MAPROVEEDORES where IDProveedor_Cadena=@CoProveedor) )

		)

	and (
			((Convert(Char(10),@FecEmisionInicio,103)='01/01/1900' and Convert(Char(10),@FecEmisionFinal,103)='01/01/1900') Or              
			(CAST(CONVERT(CHAR(10),d.FeRecepcion,103)AS SMALLDATETIME) between @FecEmisionInicio and @FecEmisionFinal))      
		)
	and (@NuDocum='' OR d.NuDocum LIKE '%'+@NuDocum+'%' )-- AND      
	and (@NuVoucher='' OR d.NuVoucher LIKE '%'+@NuVoucher+'%' )
	and year(d.FeRecepcion)>=2016
) AS X
) as y
) as z
) as z1
  where 	(@CoEstado='' OR CoEstado = @CoEstado ) AND      
			(@CoOperacion='' OR NoOperacion = @CoOperacion )
			--and
			--NoOperacion is not null
			--and EsDetraccion=1
order by FeEmision,FeRecepcion

END;
