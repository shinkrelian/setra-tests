﻿Create Procedure dbo.CORRREOFILE_DelxNuCorreo
@NuCorreo int
As
Begin
Set NoCount On
delete from BDCORREOSETRA.dbo.DESTINATARIOSCORREOFILE Where NuCorreo=@NuCorreo
delete from BDCORREOSETRA.dbo.ADJUNTOSCORREOFILE Where NuCorreo=@NuCorreo
delete from BDCORREOSETRA.dbo.CORREOFILE Where NuCorreo=@NuCorreo

Update Correlativo Set Correlativo = (SELECT Max(NuCorreo) FROM BDCORREOSETRA.DBO.CORREOFILE) Where Tabla='CORREOFILE'
End
