﻿
Create Procedure dbo.DEBIT_MEMO_Sel_ExistexFileOutput
	@IDCab	int,
	@pExiste	bit Output
As
	Set Nocount On
	Set @pExiste = 0
	If Exists(Select IDDebitMemo From DEBIT_MEMO Where 
		IDCab=@IDCab And IDEstado<>'AN')
		Set @pExiste = 1
		
