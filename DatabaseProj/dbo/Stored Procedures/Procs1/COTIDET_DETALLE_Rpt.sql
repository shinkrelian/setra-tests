﻿
--JRF-20121022-Cambio de Posicion de la Columna Total y Neto          
--JRF-20140422-Cuando sea Nulo tomar en cuenta el IDServicio_Det de COTIDET_DETSERVICIOS
--			   Left Join MASERVICIOS_DET d2 On ISNULL(d.IDServicio_Det_V,cdd.IDServicio_Det)= d2.IDServicio_Det  
--JRF-20140808-Agregar el Tipo de Cambio de la tabla COTICAB_TIPOSCAMBIO
--HLF-20150715-Comentando )-- And IDServicio_Det = @IDServicio_Det)              
CREATE Procedure [dbo].[COTIDET_DETALLE_Rpt]
@IDCAB int                
As              
 Set NoCount On              
 Declare @Iddet varchar(6),@IDServicio_Det varchar(6),@Dia datetime,@Item numeric(6,3),              
 @Cotizacion varchar(8),@IDTipoProv char(3),@Cliente varchar(max),@Titulo varchar(max),              
 @Responsable varchar(100),@TipoCambio varchar(9),@TipoPax varchar(3),@FechaInicial varchar(10),              
 @FechaFinal varchar(10),@DiaFormat varchar(50),@NroLiberados char(2),@NroPax char(2),@Notas varchar(max),              
 @Ubigeo varchar(200),@DescProveedor varchar(max),@Servicio varchar(max),@TipoServ char(5),@Anio char(4),@Variante char(6),@CostoNetoCal varchar(15),              
 @CostoLiberado varchar(15),@Impuestos varchar(15),@CostoTotal varchar(15),@Margen varchar(15),              
 @Redondeo varchar(15),@CostoReal varchar(15)              
               
 ,@Cont tinyint=1,@IDDetTmp int              
               
 delete from RptDetCostosCotizacion              
 Declare curInt cursor for              
 select c.IDDET,c.IDServicio_Det,c.Dia,c.Item, cc.Cotizacion,p.IDTipoProv,cl.RazonComercial As Cliente,cc.Titulo,us.Nombre As Responsable,
 --isnull(cc.TipoCambio,0) as TipoCambio
 isnull((select SsTipCam from COTICAB_TIPOSCAMBIO ctc where ctc.IDCab = cc.IDCab and CoMoneda = 'SOL'),0) as TipoCambio
 ,cc.TipoPax                
  ,convert(varchar(10),cc.FecInicio,103) as FechaInicial , Convert(varchar(10),cc.FechaOut,103) as FechaFinal                 
  ,upper(SUBSTRING(DATENAME(dw,Convert(char(10),c.Dia,103)),1,3))+ ' '+ltrim(rtrim(convert(varchar,c.Dia,103))) as DiaFormat,                
  isnull(cc.NroLiberados,0) As NroLiberados, cc.NroPax,cast(cc.Notas as Varchar) as Notas ,Ub.Descripcion As Ubigeo,              
  p.Nombrecorto As DescProveedor,c.Servicio --(select sd.Descripcion from MASERVICIOS_DET sd where sd.IDServicio_Det = c.IDServicio_Det) As DescripServicioDet --c.Servicio              
  ,Case When s.IDTipoServ ='NAP' Then '---' Else s.IDTipoServ End As TipoSer,sd.Anio,sd.Tipo As Vari              
  ,c.CostoReal As CostoNeto,c.CostoLiberado,c.TotImpto As Impuestos              
  ,(c.CostoReal + c.CostoLiberado+c.TotImpto) As CostoTotal,c.Margen,c.RedondeoTotal As Redondeo,c.Total As TotalVenta              
  FROM COTIDET c --Left Join COTIDET_DETSERVICIOS cdd On c.IDDET = c.IDDET              
   Left Join MAPROVEEDORES p On c.IDProveedor=p.IDProveedor                 
   Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo                
   Left Join MASERVICIOS_DET sd On c.IDServicio_Det=sd.IDServicio_Det                
   Left Join MASERVICIOS s On s.IDServicio=sd.IDServicio                
   Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ                
   Left Join COTICAB cc On c.IDCAB=cc.IDCAB                
   Left Join MACLIENTES cl On cc.IDCLiente= cl.IDCliente                
   Left Join MAUSUARIOS us On cc.IDUsuario=us.IDUsuario                
   Left Join MAUBIGEO Ub On c.IDubigeo = Ub.IDubigeo              
  WHERE c.IDCAB=@IDCAB               
  Order By c.DIA ASC,c.Item Asc              
  Open curInt              
  Fetch Next From curInt Into @Iddet,@IDServicio_Det,@Dia,@Item,@Cotizacion,@IDTipoProv,@Cliente,@Titulo              
  ,@Responsable,@TipoCambio,@TipoPax,@FechaInicial,@FechaFinal,@DiaFormat,@NroLiberados,@NroPax,@Notas              
  ,@Ubigeo,@DescProveedor,@Servicio,@TipoServ,@Anio,@Variante,@CostoTotal,@CostoLiberado,@Impuestos,@CostoNetoCal,@Margen,@Redondeo              
  ,@CostoReal              
  Begin              
   while @@FETCH_STATUS = 0              
     Begin              
        If @Cont = 1              
         Begin              
            If Not Exists(select Iddet from RptDetCostosCotizacion where FilTitulo='T')              
             Begin              
    Insert Into RptDetCostosCotizacion              
    Values ('Iddet','IDServicio_Det','01/01/1900','0.000',@Cotizacion,'Tip',@Cliente,@Titulo,@Responsable,              
      @TipoCambio,@TipoPax,@FechaInicial,@FechaFinal,'Fecha',@NroLiberados,@NroPax,@Notas,'','','','','','',              
      'Total por '+@NroPax+(Case When @NroLiberados <> 0 then ' + '+ @NroLiberados Else '' End),'','','','','','','T')              
             End        
             If Not Exists(select Iddet from RptDetCostosCotizacion where FilTitulo='S')              
    Begin              
    Insert Into RptDetCostosCotizacion              
    Values ('Iddet','IDServicio_Det','01/01/1900','0.000','Cotizacion','Tip','Cliente','Titulo','Responsable',              
      'TipoC','TPx','FecInicio','FecFinal','Fecha','Lb','Px','Notas','Ubigeo','Proveedor','Descripción','Tipo','Año','Var',              
    'Neto','Liberado','Impuesto','Total','Margen','Redondeo','Ventas','S')              
             End              
         End              
               
        Insert Into RptDetCostosCotizacion              
         Values (@Iddet,@IDServicio_Det,@Dia,@Item,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax,@FechaInicial,              
         @FechaFinal,@DiaFormat,@NroLiberados,@NroPax,@Notas,@Ubigeo,@DescProveedor,@Servicio,@TipoServ,@Anio,@Variante,@CostoTotal,@CostoLiberado,@Impuestos,@CostoNetoCal,              
         @Margen,@Redondeo,@CostoReal,'R')              
                       
    If Exists (select IDDET from COTIDET_DETSERVICIOS where IDDET = @Iddet)-- And IDServicio_Det = @IDServicio_Det)              
    begin              
   Declare @IDServicio_Det_V int,@IDServicio varchar(8)          
   set @IDServicio_Det_V =isnull((select IDServicio_Det_V from MASERVICIOS_DET where IDServicio_Det =  @IDServicio_Det),0)          
   set @IDServicio =(select IDServicio from COTIDET where IDDET = @Iddet and IDCAB = @IDCAB)        
                   
   If @IDServicio_Det_V <> 0          
   Begin           
     Insert Into RptDetCostosCotizacion              
     select cdd.IDDET,cdd.IDServicio_Det,@Dia,@Item,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax,@FechaInicial,              
     @FechaFinal,@DiaFormat,@NroLiberados,@NroPax,@Notas,@Ubigeo              
     ,p.NombreCorto             
     ,d2.Descripcion             
     ,'',d2.Anio             
     ,d2.Tipo            
     ,cdd.CostoReal,cdd.CostoLiberado,cdd.TotImpto,cdd.CostoReal+cdd.CostoLiberado+cdd.TotImpto,              
     cdd.Margen,cdd.RedondeoTotal,cdd.Total,'D'              
     from COTIDET_DETSERVICIOS cdd Left Join MASERVICIOS_DET d On cdd.IDServicio_Det = d.IDServicio_Det              
     Left Join MASERVICIOS_DET d2 On ISNULL(d.IDServicio_Det_V,cdd.IDServicio_Det)= d2.IDServicio_Det              
     Left Join MASERVICIOS s2 On d2.IDServicio = s2.IDServicio              
     Left Join MAPROVEEDORES p On s2.IDProveedor = p.IDProveedor              
     where IDDET = @Iddet and cdd.IDServicio_Det In (select IDServicio_Det from MASERVICIOS_DET where IDServicio = @IDServicio)        
     And (p.NombreCorto is not null) And (d2.Descripcion is not null)            
     And (d2.Anio is not null) And (d2.Tipo is not null)           
     End          
      end                      
  Set @IDDetTmp=@IDDet                
  --(select d3.Descripcion from MASERVICIOS_DET d3 where IDServicio_Det =@IDServicio_Det)              
  Fetch Next From curInt Into @Iddet,@IDServicio_Det,@Dia,@Item,@Cotizacion,@IDTipoProv,@Cliente,@Titulo              
    ,@Responsable,@TipoCambio,@TipoPax,@FechaInicial,@FechaFinal,@DiaFormat,@NroLiberados,@NroPax,@Notas              
    ,@Ubigeo,@DescProveedor,@Servicio,@TipoServ,@Anio,@Variante,@CostoTotal,@CostoLiberado,@Impuestos,@CostoNetoCal,@Margen,@Redondeo              
    ,@CostoReal               
                  
  If @IDDet=@IDDetTmp                    
   Begin              
    Set @Cont=@Cont+1                
              
   End                
  Else                    
   Set @Cont=1              
     End              
 Close curInt                
 Deallocate curInt                
 Select * From RptDetCostosCotizacion order by Dia Asc,Item Asc             
End              

