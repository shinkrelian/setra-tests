﻿CREATE Procedure MAHABITTRIPLE_Ins
	@Abreviacion char(4),
	@Descripcion varchar(50),
	@UserMod char(4),
	@pIdHabitTriple char(3) OutPut
As

	Set NoCount On
	
	Declare @IdHabitTriple char(3)
	Execute Correlativo_SelOutput 'MAHABITTRIPLE',1,3,@IdHabitTriple Output
	
	Insert into MAHABITTRIPLE
			([IdHabitTriple],
			 [Abreviacion],
			 [Descripcion],
			 [UserMod])
			 Values 
			(@IdHabitTriple,
			 @Abreviacion,
			 @Descripcion,
			 @UserMod)
	Set @pIdHabitTriple = @IdHabitTriple
