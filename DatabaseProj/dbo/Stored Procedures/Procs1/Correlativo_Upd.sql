﻿Create Procedure dbo.Correlativo_Upd
	@Tabla	varchar(30),
	@Cantidad	tinyint
As
	Set Nocount On
	
	Update Correlativo Set Correlativo=Correlativo+@Cantidad
	Where Tabla=@Tabla
	
