﻿CREATE Procedure [dbo].[COTIDET_COSTOS_Rpt] 
@IDCAB int
as

	Set NoCount On
	--Modificar los subtitulos en la consulta
	declare @Total numeric(8,2),@IDDet varchar(30),@servicio varchar(30),@Pax varchar(10),
			@TOTALCOTIDET decimal(8,2),@TotalCotiQ  decimal(8,2),@Cont tinyint=1,
			@IDDetTmp Int,@PaxCotiDet  varchar(20),@fecha varchar(25),@Ubigeo varchar(60),@Cotizacion varchar(8),
			@Cliente varchar(60),@Titulo varchar(25),@Responsable varchar(60),@TipoCambio numeric(8,2),
			@TipoPax varchar(15),@FechaInicio varchar(25),@FechaFinal varchar(25),@IDTipoProv varchar(15),
			@Notas varchar(max),@Margen decimal(8,2),@CostoReal decimal(8,2),@NroLiberados smallint
			
	--Create table  dbo.RptCtInterna (IDDet varchar(30),Cotizacion varchar(12),IDTipoProv varchar(15),Cliente varchar(60),Titulo varchar(100)
	--			,Responsable varchar(60) ,TipoCambio varchar(30),TipoPax varchar(15),FechaInicio varchar(25),
	--			FechaFinal varchar(25),Fecha varchar(25),NroLiberados smallint,Ubigeo varchar(60),Servicio varchar(30),TotalCotIdet varchar(20),
	--			VvTotCot varchar(8),MargTotCot varchar(8),TotCot varchar(8),
	--			VvC2 varchar(8), MargC2 varchar(8),TotC2 varchar(8),
	--			VvC3 varchar(8), MargC3 varchar(8),TotC3 varchar(8),
	--			VvC4 varchar(8), MargC4 varchar(8),TotC4 varchar(8),
	--			VvC5 varchar(8), MargC5 varchar(8),TotC5 varchar(8),
	--			FilTitulo char(1),Notas varchar(max))
	
	Delete from RptCtInterna
	
	declare curCtInterna cursor for
	
	SELECT  CD.IDDet,CD.Cotizacion,pr.IDTipoProv,c.RazonSocial,cc.Titulo,u.Nombre,isnull(cc.TipoCambio,0),cc.TipoPax
		,convert(varchar(10),cc.FecInicio,103) as FechaInicial , Convert(varchar(10),cc.FechaOut,103) as FechaFinal 
		,Convert(varchar(60),CD.Dia,103)+char(10)+char(13)+CD.DiaNombre as Fecha,cc.NroLiberados,
		ub.Descripcion as Ubigeo,CD.Servicio,cd.nropax,	CABQ.Pax,isnull(CD.TOTAL,0) as TotalCotiDet,isnull(CD.Margen,0) Margen,
		isnull(CD.CostoReal,0) CostoReal, isnull(CQ.Total,0) Total,		cast(cc.Notas as Varchar) as Notas
		FROM COTIDET_QUIEBRES CQ Right Join 
		  (select top (4)* from COTICAB_QUIEBRES where IDCAB=@IDCAB) CABQ ON CQ.IDCAB_Q=CABQ.IDCAB_Q
			Right Join COTIDET CD ON CQ.IDDET=CD.IDDET Left Join MAUBIGEO ub On cd.IDUbigeo=ub.IDUbigeo 
			Left Join COTICAB cc  On CD.IDCAB=cc.IDCAB Left Join MAUSUARIOS u On cc.IDUsuario=u.IDUsuario
			Left Join MACLIENTES c On cc.IDCLiente= c.IDCliente Left Join MAPROVEEDORES pr On CD.IDProveedor=pr.IDProveedor
		WHERE CD.IDDet In (SELECT IDDET FROM COTIDET WHERE IDCAB=@IDCAB) 					
			--CQ.IDDet =23
		ORDER BY CD.IDDet,CABQ.Pax
		
	Open curCtInterna
	
	Fetch Next From curCtInterna into @IDDet,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax
			,@FechaInicio,@FechaFinal,@fecha,@NroLiberados,@Ubigeo,@servicio,@PaxCotiDet,@Pax,@TOTALCOTIDET,@Margen,@CostoReal,@TotalCotiQ,@Notas
	
	while @@FETCH_STATUS=0
	Begin
		SELECT @Total = COTQ.Total--,@Margen=COTQ.Margen,@CostReal=COTQ.CostoReal
		  FROM COTIDET_QUIEBRES COTQ LEFT JOIN COTICAB_QUIEBRES CABQ ON COTQ.IDCAB_Q=CABQ.IDCAB_Q
		  WHERE COTQ.IDDet=@IDDet AND CABQ.Pax=@Pax    
		If @Cont=1
	     Begin
		  If Not Exists(select IDDet From RptCtInterna where FilTitulo='T')
		    Begin 
				insert into RptCtInterna (IDDet,Cotizacion,IDTipoProv,Cliente,Titulo,Responsable,TipoCambio,TipoPax,
					 FechaInicio,FechaFinal,Fecha,NroLiberados,Ubigeo,Servicio,TotalCotIdet,VvTotCot,MargTotCot,TotCot,FilTitulo,Notas)
				 values
				('IDDet','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal
					,'Fecha',@NroLiberados,'Ubigeo','Descripcion','','',@Pax,'','T','Notas')
		    End
		  If Not Exists (select IDDet from RptCtInterna where FilTitulo='S')
		    Begin
				insert into RptCtInterna (IDDet,Cotizacion,IDTipoProv,Cliente,Titulo,Responsable,TipoCambio,TipoPax,
					 FechaInicio,FechaFinal,Fecha,NroLiberados,Ubigeo,Servicio,TotalCotIdet,VvTotCot,MargTotCot,TotCot,FilTitulo,Notas)
				 values 
				('IDDet','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal
					,'Fecha',@NroLiberados,'Ubigeo','Descripción','%','Costo','Venta','Ganancia','S','Notas')
		    End
		  insert into RptCtInterna -- select*from RptCt (IDDet,Servicio,Pax,TotalCotIdet,C1,C2,C3,C4,titulo) 
					values (@IDDet,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax
					,@FechaInicio,@FechaFinal,@fecha,@NroLiberados,@Ubigeo,@servicio,@TOTALCOTIDET,@CostoReal,@Margen,@TotalCotiQ,'','',''
					,'','','','','','','','','','R',@Notas)
	     End
	    If @Cont=2
	     Begin
	       Update RptCtInterna  set MargC2=@Pax where FilTitulo='T' and MargC2 is null
	       if (@Pax is not null)
	          Begin
	            update RptCtInterna set VvC2='Costo', MargC2='Venta',TotC2='Ganancia' where FilTitulo='S' and MargC2 is null
	          End
		   Update RptCtInterna  set TotalCotIdet=@TOTALCOTIDET,VvC2=@CostoReal,MargC2=@Margen,TotC2=@TotalCotiQ ,servicio=@servicio 
		   where FilTitulo='R' and IDDet=@IDDet 
	     End
	     
	   If @Cont=3
	     Begin
	       Update RptCtInterna  set MargC3=@Pax where FilTitulo='T'  and MargC3 is null
	       if (@Pax is not null)
	          Begin
	            update RptCtInterna set VvC3='Costo', MargC3='Venta',TotC3='Ganancia' where FilTitulo='S' and MargC3 is null
	          End
		   Update RptCtInterna  set TotalCotIdet=@TOTALCOTIDET,VvC3=@CostoReal,MargC3=@Margen,TotC3=@TotalCotiQ ,servicio=@servicio 
		   where FilTitulo='R' and IDDet=@IDDet 
	     End
	   
	   If @Cont=4
	     Begin
	       Update RptCtInterna  set MargC4=@Pax where FilTitulo='T' and MargC4 is null
	       if (@Pax is not null)
	          Begin
	            update RptCtInterna set VvC4='Costo', MargC4='Venta',TotC4='Ganancia' where FilTitulo='S' and MargC4 is null
	          End
		   Update RptCtInterna  set TotalCotIdet=@TOTALCOTIDET,VvC4=@CostoReal,MargC4=@Margen,TotC4=@TotalCotiQ ,servicio=@servicio 
		   where FilTitulo='R' and IDDet=@IDDet
	     End
	     
	   If @Cont=5
	     Begin
	       Update RptCtInterna  set MargC5=@Pax where FilTitulo='T' and MargC5 is null
	       if (@Pax is not null)
	          Begin
	            update RptCtInterna set VvC5='Costo', MargC5='Venta',TotC5='Ganancia' where FilTitulo='S' and MargC5 is null
	          End
		   Update RptCtInterna  set TotalCotIdet=@TOTALCOTIDET,VvC5=@CostoReal,MargC5=@Margen,TotC5=@TotalCotiQ ,servicio=@servicio 
		   where FilTitulo='R' and IDDet=@IDDet 
	     End
	     
	   Set @IDDetTmp=@IDDet
	   
	   Fetch Next From curCtInterna into @IDDet,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax
			,@FechaInicio,@FechaFinal,@fecha,@NroLiberados,@Ubigeo,@servicio,@PaxCotiDet,@Pax,@TOTALCOTIDET,@Margen,@CostoReal,@TotalCotiQ,@Notas
			
		If @IDDet=@IDDetTmp		  
			Set @Cont=@Cont+1		  
		  Else		  
			Set @Cont=1
			 
	End	
		Close curCtInterna
		Deallocate curCtInterna

Select * From RptCtInterna Order by FilTitulo desc,Fecha
