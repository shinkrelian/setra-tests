﻿
CREATE Procedure [dbo].[DESTINATARIOSCORREOFILE_Del]
@NuCorreo int,
@NoCuentaPara varchar(150)
As
	Set NoCount On
	Delete From .BDCORREOSETRA..DESTINATARIOSCORREOFILE where NuCorreo = @NuCorreo and NoCuentaPara = @NoCuentaPara
