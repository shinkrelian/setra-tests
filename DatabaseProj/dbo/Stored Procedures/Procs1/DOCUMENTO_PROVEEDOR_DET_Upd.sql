﻿
--alter
--JHD-20150806-Se agregaron campos nuevos para SAP
--JHD-20150928-@QtCantidad int=0,
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_DET_Upd]
	@NuDocumProv int,
	@NuDocumProvDet int,
	@IDServicio_Det int=0,
	@TxServicio varchar(max)='',
	@SsMonto numeric(9,2)=0,
	--@QtCantidad tinyint=0,
	@QtCantidad int=0,
	--@FlActivo bit,
	@UserMod char(4)='',

	@IDCab int=0,
	@CoProducto char(6)='',
	@CoAlmacen char(6)='',
	@SsIGV numeric(8,2)=0,
	@CoTipoOC char(3)='',
	@CoCtaContab varchar(14)='',
	@CoCeCos char(6)='',
	@CoCeCon char(6)='',
	@SSPrecUni numeric(9,2)=0,
	@SSTotal numeric(9,2)
AS
BEGIN
	Update [dbo].[DOCUMENTO_PROVEEDOR_DET]
	Set
		[IDServicio_Det] = Case When @IDServicio_Det=0 Then Null Else @IDServicio_Det End,
 		[TxServicio] = case when LTRIM(RTRIM(@TxServicio)) ='' then null else @TxServicio end,
 		[SsMonto] = @SsMonto,
 		QtCantidad= Case When @QtCantidad=0 Then Null Else @QtCantidad End,--@QtCantidad,
 		[UserMod] = @UserMod,
 		[FecMod] = getdate(),
		IDCab=Case When @IDCab=0 Then Null Else @IDCab End,
 		CoProducto=	case when LTRIM(RTRIM(@CoProducto)) ='' then null else @CoProducto end,
 		CoAlmacen=	case when LTRIM(RTRIM(@CoAlmacen)) ='' then null else @CoAlmacen end,
 		SsIGV=	Case When @SsIGV=0 Then Null Else @SsIGV End,
 		CoTipoOC=	case when LTRIM(RTRIM(@CoTipoOC)) ='' then null else @CoAlmacen end,
 		CoCtaContab=	case when LTRIM(RTRIM(@CoCtaContab)) ='' then null else @CoAlmacen end,
 		CoCeCos=	case when LTRIM(RTRIM(@CoCeCos)) ='' then null else @CoAlmacen end,
 		CoCeCon=	case when LTRIM(RTRIM(@CoCeCon)) ='' then null else @CoAlmacen end,
 		SSPrecUni=	Case When @SSPrecUni=0 Then Null Else @SSPrecUni End,
		SSTotal =	Case When @SSTotal=0 Then Null Else @SSTotal End
 	Where 
		[NuDocumProv] = @NuDocumProv And 
		[NuDocumProvDet] = @NuDocumProvDet
END;
