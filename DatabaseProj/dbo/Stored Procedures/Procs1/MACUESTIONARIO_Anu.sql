﻿Create Procedure dbo.MACUESTIONARIO_Anu
@NuCuest tinyint,
@UserMod char(4)
As
Set NoCount On
	Update MACUESTIONARIO
		Set Activo=0,
			UserMod=@UserMod,
			FecMod=GetDate()
	Where NuCuest=@NuCuest
