﻿CREATE Procedure [dbo].[MAIDIOMAS_Sel_Lvw]
	@ID	varchar(12),
	@Descripcion varchar(12)
As
	Set NoCount On
	
	Select IDIdioma	
	From MAIDIOMAS
	Where (IDIdioma Like '%'+@Descripcion+'%')
	And (IDIdioma<>@ID Or ltrim(rtrim(@ID))='')
	Order by IDIdioma
