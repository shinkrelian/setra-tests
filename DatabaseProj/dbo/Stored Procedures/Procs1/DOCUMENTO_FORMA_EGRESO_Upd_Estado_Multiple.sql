﻿
CREATE PROCEDURE [dbo].[DOCUMENTO_FORMA_EGRESO_Upd_Estado_Multiple]
	@IDDocFormaEgreso	int,
	@TipoFormaEgreso	char(3),
	@CoEstado char(2),
	@CoMoneda_FEgreso char(3),
	@UserMod char(4)
AS
BEGIN

	Update DOCUMENTO_FORMA_EGRESO
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  
	where IDDocFormaEgreso = @IDDocFormaEgreso AND TipoFormaEgreso = @TipoFormaEgreso AND CoMoneda = @CoMoneda_FEgreso

END;
