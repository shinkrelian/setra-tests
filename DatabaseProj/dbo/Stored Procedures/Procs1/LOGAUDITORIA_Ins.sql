﻿Create Procedure [Dbo].[LOGAUDITORIA_Ins]
@Tabla varchar(30) ,
@UserMod char(4) ,
@RegistroPK1 varchar(10),
@RegistroPK2 varchar(10),
@RegistroPK3 varchar(10),
@RegistroPK4 varchar(10),
@Accion char(1)
As
	Set NoCount On
	Insert into LOGAUDITORIA 
			(Equipo,Tabla,UserMod,RegistroPK1,RegistroPK2,RegistroPK3,RegistroPK4,Accion)
		   Values 
		    (HOST_NAME(),@Tabla,@UserMod,
			 	Case When ltrim(rtrim(@RegistroPK1))='' Then Null Else @RegistroPK1 End,
				Case When ltrim(rtrim(@RegistroPK2))='' Then Null Else @RegistroPK2 End,
				Case When ltrim(rtrim(@RegistroPK3))='' Then Null Else @RegistroPK3 End,
				Case When ltrim(rtrim(@RegistroPK4))='' Then Null Else @RegistroPK4 End,
				@Accion)
