﻿CREATE Procedure [dbo].[MADETALLE_Sel_List]
	@IDDetalle char(3), 
	@Descripcion text
As
	Set NoCount On
	Declare @vcDescripcion	varchar(MAX)=Cast(@Descripcion as varchar(max))
	
	Select u.Usuario ,Descri, IDIdioma, IDDetalle
	From MADETALLE md Left Join MAUSUARIOS u On md.IDUsuario=u.IDUsuario
	Where (Descripcion Like '%'+@vcDescripcion+'%' Or LTRIM(RTRIM(@vcDescripcion))='')
	and (IDDetalle=@IDDetalle Or LTRIM(RTRIM(@IDDetalle))='')
