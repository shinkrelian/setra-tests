﻿CREATE Procedure [dbo].[EnviarCorreo_Extranet_Pax]
	@Perfil	nvarchar(128),
	@IDCab int=0,
	@Formato	varchar(4),
	@Adjuntos	nvarchar(max)
	
As	
BEGIN

DECLARE @Destinatarios	varchar(max),
	@DestinatariosCopia	varchar(max),
	@DestinatariosCopiaOculta	varchar(max)='',
	@Mensaje	nvarchar(max),
	@Titulo	nvarchar(255)

	,@IDFile char(8)=''
	
	,@NombreCliente varchar(80)=''


	select @Destinatarios=isnull(uv.Correo,''),@DestinatariosCopia=isnull(ur.Correo,'')
	 from COTICAB c 
	 left join MAUSUARIOS uv on c.IDUsuario=uv.IDUsuario
	 left join MAUSUARIOS ur on c.IDUsuarioRes=ur.IDUsuario
	 where c.IDCAB=@IDCab


	 select @IDFile=IDFile,@NombreCliente=RazonComercial from COTICAB c 
	 left join maclientes cl on cl.IDCliente=c.IDCliente
	  where c.IDCAB=@IDCab



	 --set @Mensaje='Se han realizado cambios en los datos del Pax exitosamente.'
	 set @Mensaje='El ejecutivo de la empresa '+@NombreCliente+' ha modificado la información de un PAX.'
	 --set @Titulo='Notificación de cambio de datos de Pax'
	 set @Titulo='Modificación de PAX en el File '+@IDFile

	 set @Destinatarios='jorge.huaman@setours.com'
	 set @DestinatariosCopia='juan.carlos.cruz@setours.com'

	if exists(SELECT* FROM msdb.dbo.sysmail_profile where name=@Perfil ) 	   
		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = @Perfil,
		@recipients = @Destinatarios,
		@copy_recipients= @DestinatariosCopia,
		@blind_copy_recipients = @DestinatariosCopiaOculta,
		@body = @Mensaje,
		@body_format = @Formato,
		@subject = @Titulo,
		@file_attachments = @Adjuntos
END;
