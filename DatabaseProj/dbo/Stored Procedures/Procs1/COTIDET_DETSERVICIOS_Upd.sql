﻿--JRF-20121010-Nuevo Campo Redondeo.    
--JRF-20130227-Nueva longitud del los campos Numeric(8,2)a Numeric(12,4)
CREATE Procedure [dbo].[COTIDET_DETSERVICIOS_Upd]
 @IDDet int,    
 @IDServicio_Det int,    
 @CostoReal Numeric(12,4),    
 @CostoLiberado Numeric(12,4),    
 @Margen Numeric(12,4),    
 @MargenAplicado numeric(6,2),    
 @MargenLiberado Numeric(12,4),    
 @Total Numeric(8,2),    
 @RedondeoTotal Numeric(12,4)=0,  
 @SSCR Numeric(12,4),    
 @SSCL Numeric(12,4),    
 @SSMargen Numeric(12,4),    
 @SSMargenLiberado Numeric(12,4),    
 @SSTotal Numeric(12,4),    
 @STCR Numeric(12,4),    
 @STMargen Numeric(12,4),    
 @STTotal Numeric(12,4),    
 @CostoRealImpto Numeric(12,4),    
 @CostoLiberadoImpto Numeric(12,4),    
 @MargenImpto Numeric(12,4),    
 @MargenLiberadoImpto Numeric(12,4),    
 @SSCRImpto Numeric(12,4),    
 @SSCLImpto Numeric(12,4),    
 @SSMargenImpto Numeric(12,4),    
 @SSMargenLiberadoImpto Numeric(12,4),    
 @STCRImpto Numeric(12,4),    
 @STMargenImpto Numeric(12,4),    
 @TotImpto Numeric(12,4),    
 @UserMod char(4)    
As    
 Set Nocount On    
     
 UPDATE COTIDET_DETSERVICIOS SET               
           CostoReal=@CostoReal    
           ,CostoLiberado=@CostoLiberado    
           ,Margen=@Margen    
           ,MargenAplicado=@MargenAplicado    
           ,MargenLiberado=@MargenLiberado    
           ,Total=@Total    
           ,RedondeoTotal = @RedondeoTotal  
           ,SSCR=@SSCR    
           ,SSCL=@SSCL    
           ,SSMargen=@SSMargen    
           ,SSMargenLiberado=@SSMargenLiberado    
           ,SSTotal=@SSTotal    
           ,STCR=@STCR    
           ,STMargen=@STMargen    
           ,STTotal=@STTotal    
           ,CostoRealImpto=@CostoRealImpto    
           ,CostoLiberadoImpto=@CostoLiberadoImpto    
           ,MargenImpto=@MargenImpto    
           ,MargenLiberadoImpto=@MargenLiberadoImpto    
           ,SSCRImpto=@SSCRImpto    
           ,SSCLImpto=@SSCLImpto    
           ,SSMargenImpto=@SSMargenImpto    
           ,SSMargenLiberadoImpto=@SSMargenLiberadoImpto    
           ,STCRImpto=@STCRImpto    
           ,STMargenImpto=@STMargenImpto    
           ,TotImpto=@TotImpto               
           ,UserMod=@UserMod    
           ,FecMod=getdate()    
     WHERE IDDet=@IDDet And IDServicio_Det=@IDServicio_Det    
