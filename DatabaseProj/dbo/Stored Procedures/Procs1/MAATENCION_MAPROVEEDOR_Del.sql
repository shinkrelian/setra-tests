﻿Create Procedure MAATENCION_MAPROVEEDOR_Del
@IDAtencion tinyint,
@IDProveedor char(6)
As
Begin
	Set NoCount On
	delete from MAATENCION_MAPROVEEDOR where IDAtencion = @IDAtencion and IDProveedor = @IDProveedor
End
