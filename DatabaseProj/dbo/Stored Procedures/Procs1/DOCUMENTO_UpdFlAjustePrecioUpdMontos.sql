﻿Create Procedure dbo.DOCUMENTO_UpdFlAjustePrecioUpdMontos
	@IDCab int,    
	@IDDebitMemo char(10),
	@IDTipoDoc char(3),
	@UserMod char(4)

As

	Set Nocount On
	
	Update DOCUMENTO Set FlAjustePrecioUpdMontos=1,
		FecMod=GETDATE(), UserMod=@UserMod
	Where idcab=@IDCab And FlPorAjustePrecio=1 
		And FlAjustePrecioUpdMontos=0
		And IDTipoDoc In(@IDTipoDoc)


