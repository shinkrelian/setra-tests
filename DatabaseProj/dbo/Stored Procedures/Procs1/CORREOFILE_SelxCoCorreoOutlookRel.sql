﻿Create Procedure dbo.CORREOFILE_SelxCoCorreoOutlookRel
@CoCorreoOutlookRel varchar(Max)
As
Set NoCount On
Select distinct IDCab
from BDCORREOSETRA.dbo.CORREOFILE 
Where IDCab is not Null 
And Charindex(@CoCorreoOutlookRel,CoCorreoOutlook)>0
