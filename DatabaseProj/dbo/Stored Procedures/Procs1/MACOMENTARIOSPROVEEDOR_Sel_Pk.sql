﻿Create Procedure [dbo].[MACOMENTARIOSPROVEEDOR_Sel_Pk]
	@IDComentario char(3),
	@IDProveedor	char(6)
	
As
	Set NoCount On	

	Select  * From	MACOMENTARIOSPROVEEDOR 
	Where IDProveedor = @IDProveedor And IDComentario=@IDComentario
