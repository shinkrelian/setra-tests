﻿
CREATE PROCEDURE  [dbo].[COTIDET_COTIZACION_Montos_FOC_Rpt]       
						@IDCab int,
						@IDTemporada int
AS
--*/
BEGIN
/*
declare @IDCab int,
		@IDTemporada int
select @IDCab = 17197,
		@IDTemporada = 2
--*/
	SET NOCOUNT ON
	SET LANGUAGE us_English
	DECLARE @FechaIniR smalldatetime, @FechaFinR smalldatetime
	DECLARE @Tb_Cab TABLE(IDCAB	int,
						  IDFile char(8),
						  Titulo varchar(100),
						  NroPax smallint,
						  NroLiberados	smallint,
						  FlIncluirTC bit,
						  [Simple] tinyint,
						  Twin tinyint,
						  Matrimonial tinyint,
						  Triple tinyint,
						  NombreTC varchar(80),
						  TD_DAYS int,
						  C_SWB int,
						  C_TWIN int,
						  C_MAT int,
						  C_TRIP int,
						  IDDebitMemo char(10))
	--DROP TABLE #Tb_Det
	--CREATE TABLE #Tb_Det(IDCAB	int,
	DECLARE @Tb_Det TABLE(IDCAB	int,
						 Item	numeric,
						 sFecha	varchar(30),
						 Dia	smalldatetime,
						 DiaRes	smalldatetime,
						 Ubigeo	varchar(60),
						 Servicio varchar(150),
						 IDTipoServ char(3),
						 IDTipoProv char(3),
						 sNumerAPT varchar(max),
						 NroPax_RD smallint,
						 NroLiberados_RD smallint,
						 Total_RD numeric(18,2),
						 Monto numeric(10,4),
						 Total numeric(18,2),
						 DescripcionAlterna varchar(200),
						 TipG tinyint
						)
	INSERT INTO @Tb_Cab(IDCAB, IDFile, Titulo, NroPax, NroLiberados, FlIncluirTC, [Simple], Twin, Matrimonial, Triple, NombreTC, TD_DAYS, C_SWB, C_TWIN, C_MAT, C_TRIP, IDDebitMemo)
	SELECT CC.IDCAB, CC.IDFile, CC.Titulo, ISNULL(CC.NroPax, 0) AS NroPax_CC, ISNULL(CC.NroLiberados, 0) AS NroLiberados_CC,
	CASE WHEN ISNULL(CTC.IDCab, 0) = 0 THEN 0 ELSE 1 END AS FlIncluirTC, ISNULL(CC.Simple, 0) AS Simple, 
	ISNULL(CC.Twin, 0) AS Twin, ISNULL(CC.Matrimonial, 0) AS Matrimonial, ISNULL(CC.Triple, 0) AS Triple, CP.NombreTC,
	CASE WHEN CTC.FeDesde IS NOT NULL AND CTC.FeHasta IS NOT NULL THEN DATEDIFF(DAY, CTC.FeDesde, CTC.FeHasta) + 1 ELSE 0 END AS TD_DAYS,
	AC.C_SWB, AC.C_TWIN, AC.C_MAT, AC.C_TRIP, ISNULL(DM.IDDebitMemo,'') AS IDDebitMemo
	FROM COTICAB AS CC LEFT OUTER JOIN
	(SELECT IDCAB, MAX(CASE WHEN CapacidadHab = 1 THEN NPHab ELSE 0 END) AS C_SWB,
				   MAX(CASE WHEN CapacidadHab = 2 AND EsMatrimonial = 0 THEN NPHab ELSE 0 END) AS C_TWIN, 
				   MAX(CASE WHEN CapacidadHab = 2 AND EsMatrimonial = 1 THEN NPHab ELSE 0 END) AS C_MAT,
				   MAX(CASE WHEN CapacidadHab = 3 THEN NPHab ELSE 0 END) AS C_TRIP
	 FROM (SELECT IDCAB, CapacidadHab, COUNT(CapacidadHab) AS NPHab, EsMatrimonial FROM 
			(SELECT IDCAB, CapacidadHab, IDHabit AS NPHab, EsMatrimonial FROM ACOMODOPAX_FILE
			 WHERE IDCAB = @IDCab GROUP BY IDCAB, CapacidadHab, IDHabit, EsMatrimonial) TX1
		  GROUP BY IDCAB, CapacidadHab, EsMatrimonial) AS AC1
	 GROUP BY IDCAB) AS AC ON CC.IDCAB = AC.IDCAB LEFT OUTER JOIN
	(SELECT IDCab, MAX(Apellidos + ' ' + Nombres) AS NombreTC FROM COTIPAX
	 WHERE (FlTC = 1) GROUP BY IDCab) AS CP ON CC.IDCAB = CP.IDCab LEFT OUTER JOIN COTICAB_TOURCONDUCTOR AS CTC ON CC.IDCAB = CTC.IDCab
	LEFT OUTER JOIN (SELECT D1.IDCab, D1.IDDebitMemo FROM DEBIT_MEMO D1 INNER JOIN
							(SELECT IDCab, MAX(Total) AS Total FROM DEBIT_MEMO D0 WHERE IDCAB = 3898 GROUP BY IDCab) D2
					 ON D1.IDCab = D2.IDCab AND D1.Total = D2.Total) DM ON CC.IDCAB = DM.IDCab
	WHERE (CC.IDCAB = @IDCab)

	SELECT @FechaIniR = MIN(Dia), @FechaFinR = MAX(Dia) FROM COTIDET WHERE IDCAB = @IDCab

	INSERT INTO @Tb_Det(IDCAB, Item, sFecha, Dia, DiaRes, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total_RD, Monto, Total, DescripcionAlterna, TipG, IDTipoProv)
	SELECT IDCAB, Item, sFecha, Dia, DiaRes, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total, 
	CASE WHEN (FlRelacion = 1) THEN 0 ELSE MontoRango END AS Monto,
	CASE WHEN (FlRelacion = 1) THEN 0 ELSE NroPax_RD * MontoRango END AS Total, DescripcionAlterna, 1, IDTipoProv
	FROM (
	SELECT CC.IDCAB, CD.Item, UPPER(substring(Datename(dw,RD.FechaR),1,3))+' '+ Convert(varchar(60),RD.FechaR,3) as sFecha, RD.Dia, RD.FechaR AS DiaRes, UB.Descripcion AS Ubigeo,
	CASE WHEN (PS.IDTipoProv = '001' OR PS.IDTipoProv = '002') THEN PS.NombreCorto + ' (' + RD.Servicio + ')' ELSE RD.Servicio END + CASE WHEN CD.IncGuia = 1 THEN ' - Guide' ELSE '' END AS Servicio, 
	(CASE WHEN PS.IDTipoServ = 'NAP' THEN '---' ELSE PS.IDTipoServ END) AS IDTipoServ
	, CASE WHEN CD.NroPax <> CC.NroPax THEN [dbo].[FnNumberAPT_RESERVAS_DET_PAX](CC.IDCAB, RD.IDReserva_Det) ELSE '' END AS sNumerAPT
	, (CASE WHEN RD.CapacidadHab = 0 THEN RD.NroPax ELSE RD.CantidadAPagar * RD.CapacidadHab END) AS NroPax_RD --, RD.NroPax AS NroPax_RD
	, RD.NroLiberados AS NroLiberados_RD
	, RD.Total, CASE WHEN EXISTS(SELECT CDR.Total FROM COTIDET CDR WHERE CDR.IDDetRel = CD.IDDET AND CDR.Dia = RD.FechaR) THEN 1 ELSE 0 END AS FlRelacion
	, TR1.Monto AS MontoRango
	, TR1.DescripcionAlterna, PS.APTServicoOpcional, PS.IDTipoProv, RD.CapacidadHab, ISNULL(TR1.SingleSupplement,0) AS SingleSupplement, RD.EsMatrimonial
	FROM COTICAB AS CC INNER JOIN COTIDET AS CD ON CC.IDCAB = CD.IDCAB INNER JOIN
	(SELECT @IDCab AS IDCab, RDIF.IDDet, RDIF.Dia, RDIF.FechaOut, RDIF.Servicio, RDIF.IDServicio, RDIF.IDServicio_Det, RDIF.NroPax,
	 RDIF.NroLiberados, RDIF.Total, RDIF.CapacidadHab, F.FechaR, RDIF.IDReserva_Det, RDIF.EsMatrimonial, RDIF.CantidadAPagar
		FROM (
			SELECT RDF.IDDet, RDF.Dia, CASE WHEN RDF.FechaOut IS NULL THEN RDF.Dia ELSE RDF.FechaOut END AS FechaOut,
			RDF.Servicio,RDF.IDServicio, RDF.IDServicio_Det, RDF.NroPax, RDF.NroLiberados, RDF.Total, RDF.CapacidadHab, RDF.IDReserva_Det
			, RDF.EsMatrimonial, RDF.CantidadAPagar
			FROM COTICAB AS CF INNER JOIN COTIDET AS DF ON CF.IDCAB = DF.IDCAB INNER JOIN RESERVAS_DET AS RDF ON DF.IDDET = RDF.IDDet
			WHERE CF.IDCAB = @IDCab AND RDF.Anulado = 0 AND RDF.FechaOut IS NOT NULL
			) RDIF LEFT OUTER JOIN dbo.FnTableRangoFechas(@FechaIniR,@FechaFinR) F ON F.FechaR >= RDIF.Dia AND F.FechaR < RDIF.FechaOut
			WHERE F.FechaR IS NOT NULL
	 UNION ALL
	 SELECT @IDCab AS IDCab, RDIF.IDDet, RDIF.Dia, RDIF.FechaOut, RDIF.Servicio, RDIF.IDServicio, RDIF.IDServicio_Det, RDIF.NroPax,
	 RDIF.NroLiberados, RDIF.Total, RDIF.CapacidadHab, F.FechaR, RDIF.IDReserva_Det, RDIF.EsMatrimonial, RDIF.CantidadAPagar
		FROM (
			SELECT RDF.IDDet, RDF.Dia, CASE WHEN RDF.FechaOut IS NULL THEN RDF.Dia ELSE RDF.FechaOut END AS FechaOut,
			RDF.Servicio,RDF.IDServicio, RDF.IDServicio_Det, RDF.NroPax, RDF.NroLiberados, RDF.Total, RDF.CapacidadHab, RDF.IDReserva_Det
			, RDF.EsMatrimonial, RDF.CantidadAPagar
			FROM COTICAB AS CF INNER JOIN COTIDET AS DF ON CF.IDCAB = DF.IDCAB INNER JOIN RESERVAS_DET AS RDF ON DF.IDDET = RDF.IDDet
			WHERE CF.IDCAB = @IDCab AND RDF.Anulado = 0 AND RDF.FechaOut IS NULL
			) RDIF LEFT OUTER JOIN dbo.FnTableRangoFechas(@FechaIniR,@FechaFinR) F ON RDIF.Dia BETWEEN F.FechaR AND F.FechaRF
	) AS RD ON CD.IDCAB = RD.IDCab AND CD.IDDET = RD.IDDet AND CD.IDServicio = RD.IDServicio AND CD.IDServicio_Det = RD.IDServicio_Det
	INNER JOIN (SELECT MP1.IDProveedor, MP1.NombreCorto, MP1.IDTipoProv, MS1.IDServicio, MS1.IDTipoServ, MS1.APTServicoOpcional, MD1.IDServicio_Det, MD1.ConAlojamiento
	 FROM MAPROVEEDORES AS MP1 INNER JOIN MASERVICIOS AS MS1 ON MP1.IDProveedor = MS1.IDProveedor INNER JOIN MASERVICIOS_DET AS MD1 ON MS1.IDServicio = MD1.IDServicio
	 WHERE (MP1.IDTipoProv = '001') OR ((MP1.IDTipoProv = '003') AND (MD1.ConAlojamiento = 1))) AS PS
	ON CD.IDProveedor = PS.IDProveedor AND CD.IDServicio = PS.IDServicio AND CD.IDServicio_Det = PS.IDServicio_Det
	LEFT OUTER JOIN MAUBIGEO AS UB	ON CD.IDubigeo = UB.IDubigeo
	INNER JOIN (SELECT S2.IDServicio, M2.IDServicio_Det, M2.IDTemporada, T2.NombreTemporada, ISNULL(M2.DescripcionAlterna, '') AS DescripcionAlterna,
				D2.PaxDesde, CASE WHEN D2.PaxHasta = 0 THEN 9999999 ELSE D2.PaxHasta END PaxHasta, D2.Monto, ISNULL(M2.SingleSupplement,0.00) AS SingleSupplement
				FROM MATEMPORADA AS T2 INNER JOIN MASERVICIOS_DET_DESC_APT AS M2 ON T2.IDTemporada = M2.IDTemporada INNER JOIN
				MASERVICIOS_DET_RANGO_APT AS D2 ON M2.IDServicio_Det = D2.IDServicio_Det AND M2.IDTemporada = D2.IDTemporada INNER JOIN
				MASERVICIOS_DET AS S2 ON M2.IDServicio_Det = S2.IDServicio_Det
				WHERE T2.IDTemporada = @IDTemporada) TR1 ON RD.IDServicio = TR1.IDServicio AND RD.IDServicio_Det = TR1.IDServicio_Det AND
				CD.NroPax BETWEEN TR1.PaxDesde AND TR1.PaxHasta
	WHERE (CC.IDCAB = @IDCab)

	UNION ALL
	
	SELECT TC.IDCAB, TC.Item, UPPER(substring(Datename(dw,TC.Dia),1,3))+' '+ Convert(varchar(60),TC.Dia,3) as sFecha, TC.Dia, TC.RD_Dia AS DiaRes, UB.Descripcion AS Ubigeo,
	CASE WHEN (TC.IDTipoProv = '001' OR TC.IDTipoProv = '002') THEN TC.NombreCorto + ' (' + SR.Descripcion + ')' ELSE SR.Descripcion END + CASE WHEN TC.IncGuia = 1 THEN ' - Guide' ELSE '' END AS Servicio, 
	(CASE WHEN SR.IDTipoServ = 'NAP' THEN '---' ELSE SR.IDTipoServ END) AS IDTipoServ
	, sNumerAPT
	, TC.RD_NroPax AS NroPax_RD, TC.NroLiberados AS NroLiberados_RD
	, TC.Total, FlRelacion, TR2.Monto AS MontoRango, TR2.DescripcionAlterna, SR.APTServicoOpcional, TC.IDTipoProv, TC.CapacidadHab, 0.00 AS SingleSupplement, TC.EsMatrimonial
	FROM
	(SELECT CC.IDCAB, CD.Item, CD.Dia, MIN(RD.Dia) AS RD_Dia, CD.IncGuia, CD.NroPax AS CD_NroPax, CC.NroPax AS CC_NroPax,
			RD.NroPax AS RD_NroPax, RD.NroLiberados, SUM(RD.Total) AS Total, RD.CapacidadHab, RD.EsMatrimonial, RD.IDServicio
			, PS.IDProveedor, PS.NombreCorto, PS.IDTipoProv, CD.IDubigeo
			, CASE WHEN CD.NroPax <> CC.NroPax THEN [dbo].[FnNumberAPT_RESERVAS_DET_PAX](CC.IDCAB, RD.IDReserva_Det) ELSE '' END AS sNumerAPT
			, CASE WHEN EXISTS(SELECT CDR.Total FROM COTIDET CDR WHERE CDR.IDDET = CD.IDDetRel) THEN 1 ELSE 0 END AS FlRelacion
	 FROM COTICAB AS CC INNER JOIN COTIDET AS CD ON CC.IDCAB = CD.IDCAB INNER JOIN RESERVAS_DET AS RD ON CD.IDDET = RD.IDDet
	 --INNER JOIN (SELECT IDProveedor, NombreCorto, IDTipoProv FROM MAPROVEEDORES WHERE IDTipoProv <> '001') PR ON CD.IDProveedor = PR.IDProveedor
	 INNER JOIN (SELECT MP1.IDProveedor, MP1.NombreCorto, MP1.IDTipoProv, MS1.IDServicio, MD1.IDServicio_Det, MD1.ConAlojamiento
	 FROM MAPROVEEDORES AS MP1 INNER JOIN MASERVICIOS AS MS1 ON MP1.IDProveedor = MS1.IDProveedor INNER JOIN MASERVICIOS_DET AS MD1 ON MS1.IDServicio = MD1.IDServicio
	 WHERE (MP1.IDTipoProv <> '001' AND MP1.IDTipoProv <> '003') OR ((MP1.IDTipoProv = '003') AND (MD1.ConAlojamiento = 0))) AS PS
	 ON CD.IDProveedor = PS.IDProveedor AND CD.IDServicio = PS.IDServicio AND CD.IDServicio_Det = PS.IDServicio_Det
	WHERE (CC.IDCAB = @IDCab) AND RD.Anulado = 0
	GROUP BY CC.IDCAB, CD.Item, CD.Dia, CD.IncGuia, CD.NroPax, CC.NroPax, RD.NroPax,
			RD.NroLiberados, RD.CapacidadHab, RD.EsMatrimonial, RD.IDServicio
			, PS.IDProveedor, PS.NombreCorto, PS.IDTipoProv, CD.IDubigeo
			, CASE WHEN CD.NroPax <> CC.NroPax THEN [dbo].[FnNumberAPT_RESERVAS_DET_PAX](CC.IDCAB, RD.IDReserva_Det) ELSE '' END
			, CD.IDDetRel
	) TC
	INNER JOIN MASERVICIOS AS SR ON TC.IDServicio = SR.IDServicio LEFT OUTER JOIN MAUBIGEO AS UB ON TC.IDubigeo = UB.IDubigeo
	INNER JOIN (SELECT M1.IDServicio, M1.IDTemporada, T1.NombreTemporada, ISNULL(M1.DescripcionAlterna,'') AS DescripcionAlterna,
				D1.PaxDesde, CASE WHEN D1.PaxHasta = 0 THEN 9999999 ELSE D1.PaxHasta END PaxHasta, D1.Monto
				FROM MASERVICIOS_DESC_APT AS M1 INNER JOIN MASERVICIOS_RANGO_APT AS D1 ON M1.IDServicio = D1.IDServicio
				AND M1.IDTemporada = D1.IDTemporada INNER JOIN MATEMPORADA AS T1 ON M1.IDTemporada = T1.IDTemporada
				WHERE T1.IDTemporada = @IDTemporada AND M1.AgruparServicio = 0) TR2 ON TC.IDServicio = TR2.IDServicio AND
				TC.CD_NroPax BETWEEN TR2.PaxDesde AND TR2.PaxHasta

	) T
	ORDER BY Dia


	SET NOCOUNT OFF
	SELECT C.IDCAB, IDFile, Titulo, NroPax, NroLiberados, FlIncluirTC, [Simple], Twin, Matrimonial, Triple, NombreTC, TD_DAYS, C_SWB, C_TWIN, C_MAT, C_TRIP
	, IDDebitMemo, Item, sFecha, Dia, DiaRes, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total_RD, Monto, Total, DescripcionAlterna, TipG, IDTipoProv
	FROM @Tb_Cab C LEFT OUTER JOIN @Tb_Det D ON C.IDCAB = D.IDCAB
	ORDER BY D.TipG, D.DiaRes, D.Dia, Item

END
