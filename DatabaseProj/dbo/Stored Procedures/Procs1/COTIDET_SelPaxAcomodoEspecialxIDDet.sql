﻿--JRF-20140527-Considerar el titulo academico como prefijo al apellidos
--JRF-20140528-Considerar descTC para cuando el campo FlTC este activo.
CREATE Procedure dbo.COTIDET_SelPaxAcomodoEspecialxIDDet  
@IDDet int  
As  
Begin  
Set NoCount On  
Select px.Orden as NroOrd,cad.IDHabit as IDHabit,
    Case when ltrim(rtrim(px.TxTituloAcademico)) <> '---' then px.TxTituloAcademico else px.Titulo End as Titulo,px.Nombres+' '+px.Apellidos as NombrePax,SUBSTRING(id.Descripcion,1,3) as DescIdentidad,  
    Isnull(px.NumIdentidad,'') as NumIdentidad,Isnull(ub.DC,'') as NacionalidadPax,Isnull(CONVERT(Char(10),px.FecNacimiento,103),'') as FechNacimiento,  
    Isnull(dbo.[FnMailAcomodosReservas](ch.QtCapacidad,Case When Charindex('(MAT)',ch.NoCapacidad)> 0 then 1 else 0 End)+' ' + cast(cad.IDHabit as varchar(5)),'')  as ACC,  
    Isnull(px.ObservEspecial,'') as ObservEspecial,ch.QtCapacidad,Case When Charindex('(MAT)',ch.NoCapacidad)> 0 then 1 else 0 End as EsMatrimonial,cp.IDDet,
    Case When px.FlTC = 1 then 'TC' else '' End as DescTC
from COTIDET_PAX cp Left Join COTIPAX px On cp.IDPax = px.IDPax  
Left Join MATIPOIDENT id On px.IDIdentidad = id.IDIdentidad  
Left Join MAUBIGEO ub On px.IDNacionalidad = ub.IDubigeo  
Left Join COTIDET cd On cp.IDDet = cd.IDDET  
Left Join COTIDET_ACOMODOESPECIAL_DISTRIBUC cad On cp.IDDet =cad.IDDet and cad.IDPax = cp.IDPax  
Left Join CAPACIDAD_HABITAC ch On cad.CoCapacidad = ch.CoCapacidad  
Left Join COTIDET_ACOMODOESPECIAL ca On cd.IDDET = ca.IDDet and cad.CoCapacidad = ca.CoCapacidad   
Where cp.IDDet = @IDDet  
Order by cad.IDHabit,px.Orden  
End  
