﻿

-----------------------------------------------------------------------------------------------------------


CREATE PROCEDURE MAAEROPUERTO_INS

		   @CoAero char(3),
           @NoDescripcion varchar(200),
           @CoUbigeo char(6),
           @UserMod char(4)='',
           @pCoAero char(3) output
           
AS
BEGIN
INSERT INTO MAAEROPUERTO
           ([CoAero]
           ,[NoDescripcion]
           ,[CoUbigeo]
           ,[UserMod])
     VALUES
           (@CoAero,
           @NoDescripcion, 
           @CoUbigeo,
           Case When ltrim(rtrim(@UserMod))='' Then Null Else @UserMod End
           )
           
           set @pCoAero=@CoAero

END;           
