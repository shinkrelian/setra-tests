﻿CREATE Procedure [dbo].[COTIDET_PAX_Ins]
	@IDDet	int,
	@IDPax	int,
	@UserMod	char(4)
As
	Set NoCount On
	
	Insert into COTIDET_PAX(IDDet,IDPax,UserMod)
	Values(@IDDet,@IDPax,@UserMod)
	
	Update COTIPAX Set Actualizar=1,UserMod=@UserMod,FecMod=GETDATE() Where IDPax=@IDPax
