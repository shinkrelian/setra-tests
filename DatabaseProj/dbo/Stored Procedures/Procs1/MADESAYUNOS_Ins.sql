﻿CREATE Procedure [dbo].[MADESAYUNOS_Ins]
	@Descripcion nchar(50),
    @UserMod char(4),
	@pIDDesayuno char(2) Output
As
	Set NoCount On
	
	Declare @IDDesayuno char(2)
	
	Exec Correlativo_SelOutput 'MADESAYUNOS',1,2,@IDDesayuno output
	
	INSERT INTO MADESAYUNOS
           ([IDDesayuno]
           ,[Descripcion]
           ,[UserMod]
           )
     VALUES
           (@IDDesayuno,
            @Descripcion,
            @UserMod)

	Set @pIDDesayuno=@IDDesayuno
