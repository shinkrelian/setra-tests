﻿CREATE Procedure dbo.COTIDET_SelIDsxServicioFile
	@CoServicio char(8),
	@IDCab int
	
As
	Set Nocount on

	--Select X.*,isnull((Select od.IDOperacion_Det From OPERACIONES_DET od Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion 
	--			And o.IDCab=@IDCab 
	--			Where  od.IDReserva_Det=X.IDReserva_Det),0) as IDOperacion_Det
	--From
	--(
	--Select cd.IDDET, isnull((Select rd.IDReserva_Det From RESERVAS_DET rd Inner Join RESERVAS r On rd.IDReserva=r.IDReserva 
	--		And r.IDCab=@IDCab 
	--		Where  rd.IDDet=cd.IDDET and rd.Anulado=0),0) as IDReserva_Det
	--From COTIDET cd Where cd.IDCAB=@IDCab And cd.IDServicio=@CoServicio And cd.IncGuia=0
	--) as X


	Select cd.IDDET, isnull(rd.IDReserva_Det,0) as IDReserva_Det, isnull(IDOperacion_Det,0) as IDOperacion_Det
	From COTIDET cd Left Join RESERVAS_DET rd On cd.IDDET=rd.IDDet
	Left Join OPERACIONES_DET od On od.IDReserva_Det=rd.IDReserva_Det
	Where cd.IDCAB=@IDCab And cd.IDServicio=@CoServicio And cd.IncGuia=0 And rd.Anulado=0