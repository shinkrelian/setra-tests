﻿
CREATE PROCEDURE [dbo].[MACONTACTOSCLIENTE_Sel_Mail_xIDCliente]	
	@IDCliente char(6)
AS
BEGIN
	Set NoCount On
	
	Select IDContacto, Titulo+' '+Nombres+' '+Apellidos as Nombre
	FROM MACONTACTOSCLIENTE 
    WHERE IDCliente=@IDCliente AND ISNULL(Email,'') <> ''
    Order by 1
END

