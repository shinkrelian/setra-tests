﻿Create Procedure dbo.ASIGNACION_SelAjustePrecio
	@IDCab int
As
	Set Nocount On
	
	SELECT sum(idm.SsPago) AS SsPago
	FROM ASIGNACION asg 
	Inner Join INGRESO_DEBIT_MEMO idm On asg.NuAsignacion=idm.NuAsignacion
	Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo And dm.IDCab=@IDCab
	Inner Join COTICAB cc On dm.IDCab=cc.IDCAB And asg.FeAsignacion > cc.FeFacturac

