﻿--20120824-Nuevo Campo EmitidoSetours    
--HLF-20120920-Case when para @FecSalida,@FecRetorno,@HrRecojo    
--HLF-20120921-Nuevo campo Servicio    
--HLF-20130124-Nuevos Campos.  
--HLF-20130221-Agregando campo IdaVuelta
--JRF-20150616-..Case When Ltrim(rtrim(@RutaD))='' Th...
--PPMG-20151209-Se agrego dos campos FlMotivo y DescMotivo
CREATE Procedure [dbo].[COTIVUELOS_Upd]            
	@ID int,            
	@IDCAB int,            
	@Cotizacion Char(8),            
	@Servicio varchar(250),    
	@NombrePax varchar(100),          
	@Ruta Varchar(100),            
	@NumBoleto char(12),            
	@RutaD Char(6),            
	@RutaO Char(6),            
	@Vuelo varchar(20),            
	@FecEmision smalldatetime,            
	@FecDocumento smalldatetime,            
	@FecSalida smalldatetime,            
	@FecRetorno smalldatetime,            
	@Salida varchar(20),        
	@HrRecojo varchar(20),           
	@PaxC numeric(5),            
	@PCotizado numeric(10, 2),            
	@TCotizado numeric (10, 2),            
	@PaxV numeric (5, 0),            
	@PVolado numeric(10, 2),            
	@TVolado numeric(10, 2) ,            
	@Status char(2),            
	@Comentario text,            
	@Llegada varchar(20) ,            
	@Emitido char(1) ,            
	@Identificador char(1) ,            
	@CodReserva varchar(20),            
	@Tarifa numeric(10, 2),            
	@PTA numeric(10, 2) ,            
	@ImpuestosExt numeric(10, 2),            
	@Penalidad numeric(10, 2),            
	@IGV numeric(10, 2) ,            
	@Otros numeric(10, 2),            
	@PorcComm numeric(8, 2) ,            
	@MontoContado numeric(10, 2) ,            
	@MontoTarjeta numeric(10, 2),            
	@IdTarjCred char(3) ,            
	@NumeroTarjeta varchar(20),            
	@Aprobacion varchar(20) ,            
	@PorcOver numeric(8, 2) ,            
	@PorcOverInCom numeric(8, 2) ,            
	@MontComm numeric(10, 2)  ,            
	@MontCommOver numeric(10, 2) ,            
	@IGVComm numeric(10, 2),            
	@IGVCommOver numeric(10, 2),            
	@PorcTarifa numeric(10, 2),          
	@NetoPagar numeric(10, 2),            
	@Descuento numeric(10, 2),            
	@IdLineaA char(6),            
	@MontoFEE numeric(10, 2),            
	@IGVFEE numeric(10, 2),            
	@TotalFEE numeric(10, 2),     
	@EmitidoSetours bit,           
	@EmitidoOperador bit,  
	@IDProveedorOperador char(6),  
	@CostoOperador numeric(8,2),  
	@IdaVuelta	char(1),
	@UserMod char(4),
	@FlMotivo bit = 0,
	@DescMotivo varchar(200) = NULL
AS
BEGIN            
	Set NoCount On            
	Update COTIVUELOS            
	Set [IDCAB] = @IDCAB            
	,[Cotizacion] = @Cotizacion            
	,Servicio=Case when ltrim(rtrim(@Servicio))='' Then Null else @Servicio End            
	,[NombrePax] = @NombrePax          
	,[Ruta] = @Ruta            
	,[Num_Boleto]=@NumBoleto            
	,[RutaD] = Case When Ltrim(rtrim(@RutaD))='' Then Null Else Ltrim(rtrim(@RutaD)) End
	,[RutaO] = Case When Ltrim(rtrim(@RutaO))='' Then Null Else Ltrim(rtrim(@RutaO)) End
	,[Vuelo] = @Vuelo            
	,[Fec_Emision] = case when @FecEmision='01/01/1900' then null else @FecEmision end    
	,[Fec_Documento] = case when @FecDocumento='01/01/1900' then null else @FecDocumento end           
	,[Fec_Salida] = case when @FecSalida='01/01/1900' then null else @FecSalida end            
	,[Fec_Retorno] = case when @FecRetorno='01/01/1900' then null else @FecSalida end            
	,[Salida] = @Salida        
	,[HrRecojo] = case when ltrim(rtrim(@HrRecojo))='' then null else @HrRecojo end           
	,[PaxC] = @PaxC            
	,[PCotizado] = @PCotizado            
	,[TCotizado] = @TCotizado            
	,[PaxV] = @PaxV            
	,[PVolado] = @PVolado            
	,[TVolado] = @TVolado            
	,[Status] = @Status            
	,[Comentario] = @Comentario            
	,[Llegada] = @Llegada            
	,[Emitido] = @Emitido            
	,[TipoTransporte] = @Identificador        
	,[CodReserva] = @CodReserva            
	,[Tarifa] = @Tarifa            
	,[PTA] = @PTA            
	,[ImpuestosExt] = @ImpuestosExt            
	,[Penalidad] = @Penalidad     
	,[IGV] = @IGV            
	,[Otros] = @Otros            
	,[PorcComm] = @PorcComm            
	,[MontoContado] = @MontoContado            
	,[MontoTarjeta] = @MontoTarjeta            
	,[IdTarjCred] = @IdTarjCred            
	,[NumeroTarjeta] = @NumeroTarjeta            
	,[Aprobacion] = @Aprobacion            
	,[PorcOver] = @PorcOver            
	,[PorcOverInCom] = @PorcOverInCom            
	,[MontComm] = @MontComm            
	,[MontCommOver] = @MontCommOver            
	,[IGVComm] = @IGVComm            
	,[IGVCommOver] = @IGVCommOver            
	,[NetoPagar] = @NetoPagar            
	,[PorcTarifa]= @PorcTarifa          
	,[Descuento] = @Descuento            
	,[IdLineaA] = @IdLineaA            
	,[MontoFEE] = @MontoFEE            
	,[IGVFEE] = @IGVFEE            
	,[TotalFEE] = @TotalFEE        
	,[EmitidoSetours] = @EmitidoSetours        
	,[EmitidoOperador] = @EmitidoOperador  
	,[IDProveedorOperador] = Case when ltrim(rtrim(@IDProveedorOperador))='' then Null else @IDProveedorOperador End  
	,[CostoOperador] = Case When @CostoOperador = 0 then Null Else @CostoOperador End  
	,IdaVuelta= Case when ltrim(rtrim(@IdaVuelta))='' then Null else @IdaVuelta End
	,[FlMotivo] = ISNULL(@FlMotivo,0)
	,[DescMotivo] = @DescMotivo
	,[UserMod] = @UserMod            
	,FecMod=GETDATE()    
	WHERE ID=@ID     
END
