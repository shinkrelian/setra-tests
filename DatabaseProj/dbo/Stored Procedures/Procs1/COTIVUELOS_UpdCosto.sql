﻿--HLF-20150218-FlUpdExcelPRail=1,
--HLF-20150317- @IDCab int,  
--cv.IDCAB=@IDCab
--PPMG-20151210-Se agrego el campo CostoOperadorOriginal
CREATE Procedure [dbo].[COTIVUELOS_UpdCosto]  
 --@NuFile char(8),  
 @IDCab int,  
 @CoRutaOri varchar(5),  
 @CoRutaDes varchar(5),  
 @CoReserva varchar(20), 
 @CostoOperador numeric(8,2),  
 @UserMod char(4)  
AS
BEGIN
 Set Nocount on    

 UPDATE COTIVUELOS SET CostoOperador=@CostoOperador,  CostoOperadorOriginal = @CostoOperador,
 FlUpdExcelPRail=1,
 UserMod=@UserMod,  FecMod=GETDATE()  
 FROM COTIVUELOS cv --left join COTICAB c on c.IDCAB=cv.IDCAB  
 Left Join MAUBIGEO uo On cv.RutaO=uo.IDubigeo  
 Left Join MAUBIGEO ud On cv.RutaD=ud.IDubigeo  
 WHERE --isnull(c.IDFile,'')=@NuFile 
 cv.IDCAB=@IDCab
 AND cv.TipoTransporte='T' and cv.CodReserva=@CoReserva  
 And uo.CodigoPeruRail=@CoRutaOri And ud.CodigoPeruRail=@CoRutaDes  
END
