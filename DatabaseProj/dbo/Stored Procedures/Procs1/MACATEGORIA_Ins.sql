﻿
CREATE Procedure dbo.MACATEGORIA_Ins
	@Descripcion char(10),
	@Comision	numeric(6,2),
    @UserMod char(4),
	@pIdCat char(3) Output
As
	Set NoCount On
	
	Declare @IdCat char(3)
	
	Exec Correlativo_SelOutput 'MACATEGORIA',1,3,@IdCat output
	
	INSERT INTO MACATEGORIA
           ([IdCat]
           ,[Descripcion]
           ,Comision
           ,[UserMod]
           )
     VALUES
           (@IdCat,
            @Descripcion,
            @Comision,
            @UserMod)

	Set @pIdCat=@IdCat

