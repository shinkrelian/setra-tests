﻿--JRF-20140527-Considerar el titulo academico como prefijo al apellidos
CREATE PROCEDURE [dbo].[COTIPAX_Sel_ListxIDCabxNombre]                
 @IDCab int,  
 @Nombre varchar(100)                
As                
                
 Set NoCount On                 
                 
 Select          
 NroOrd, Nombres ,DescIdentidad,NumIdentidad, DescNacionalidad,          
 FecNacimiento,Peso,Residente ,ObservEspecial,  IDIdentidad,                 
 IDNacionalidad,IDPax,IDPaisResidencia,FecIngresoPais        
 From            
 (                 
 Select                    
 cp.Orden As NroOrd,          
 cp.IDIdentidad,di.Descripcion as DescIdentidad, ltrim(rtrim(isnull(cp.NumIdentidad,''))) NumIdentidad,                 
 --cp.Titulo+' '+cp.Apellidos +', '+ cp.Nombres as Nombres , cp.FecNacimiento,
 Case when ltrim(rtrim(cp.TxTituloAcademico)) <> '---' then cp.TxTituloAcademico else cp.Titulo End 
 +' '+cp.Apellidos +', '+ cp.Nombres as Nombres , cp.FecNacimiento,
 cp.IDNacionalidad,u.Descripcion as DescNacionalidad ,cp.IDPax ,Residente           
 ,isnull(cp.Peso,0) As Peso,Isnull(cp.ObservEspecial,'') As ObservEspecial,            
 Case When ISNUMERIC(SUBSTRING(Apellidos,4,3))=1 Then              
  SUBSTRING(Apellidos,4,3)            
 Else            
  Nombres            
 End as NroNombrePax,cp.IDPaisResidencia as IDPaisResidencia   
 ,cp.FecIngresoPais As FecIngresoPais  
 From COTIPAX cp Left Join MATIPOIDENT di On cp.IDIdentidad=di.IDIdentidad                
 Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo                
 Where cp.IDCab=@IDCab                
         
 ) as X            
 where (ltrim(rtrim(@Nombre))='' or X.Nombres like '%'+@Nombre+'%')  
 Order by NroOrd   
