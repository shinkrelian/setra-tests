﻿--JHD-20160203 - Se cambio la logica para validar la fecha out
--JHD-20160210 - se valido la fecha out por detalle
CREATE PROCEDURE [dbo].[Documento_Proveedor_List_SAP_Lines_INC] --769
@nudocumprov int
as
BEGIN
declare @CtaContable as varchar(14)=''
declare @CoCeCos as varchar(14)=''
declare @IdProveedor as char(6)=(select CoProveedor from DOCUMENTO_PROVEEDOR where NuDocumProv=@nudocumprov)
declare @CoMoneda as char(3)=(select CoMoneda from DOCUMENTO_PROVEEDOR where NuDocumProv=@nudocumprov)
declare @FlFechaOut as bit=0
declare @IdCab as int=(select top 1 idcab from ENTRADASINC_SAP where NuDocumProv=@nudocumprov)
declare @FechaOut as smalldatetime
--select @FechaOut=dbo.COTICAB_DevolverFechaOutPeru(@IDCab)

declare @FechaDoc as smalldatetime=(select FeRecepcion from DOCUMENTO_PROVEEDOR where NuDocumProv=@nudocumprov)

set @FechaOut=(Select Top 1 Dia From COTIDET Where IDCAB=@IDCab And IDubigeo in (select IDubigeo from MAUBIGEO where IDPais='000323') Order by dia desc)

if (@FechaOut is null)
	set @FechaOut=(Select Top 1 Dia From COTIDET Where IDCAB=@IDCab /*And IDubigeo in (select IDubigeo from MAUBIGEO where IDPais='000323')*/ Order by dia desc)


--if year(@FechaOut)>year(getdate())
if year(@FechaOut)>year(@FechaDoc)
	set @FlFechaOut=1
else
	begin
		--if month(@FechaOut)>month(getdate())
		if month(@FechaOut)>month(@FechaOut)
			set @FlFechaOut=1
	end

if @FlFechaOut=1 -- SI PASA LA FECHA OUT
	begin
			select @CtaContable= case when @CoMoneda='SOL' THEN '49711001' ELSE '49711002' END
	end
else
	begin
		if @IdProveedor in ('001552','001553')
			begin
				select @CtaContable= case when @CoMoneda='SOL' THEN '65931003' ELSE '63111005' END
				set @CoCeCos='900101'
			end
		else
			begin
				select @CtaContable= case when @CoMoneda='SOL' THEN '65931003' ELSE '63111005' END
				set @CoCeCos='900101'
			end
	end



select	ItemCode, 
ItemDescription,
WarehouseCode,
Quantity,
DiscountPercent, 
AccountCode= case when FlFechaOut = 1 then (case when @CoMoneda='SOL' THEN '49711001' ELSE '49711002' END)
			 else (case when @CoMoneda='SOL' THEN '65931003' ELSE '63111005' end)
			 end  ,
UnitPrice,
LineTotal,

TaxCode,

TaxTotal= 0,
WTLiable=0,
TaxOnly=0,
ProjectCode,
CostingCode=case when FlFechaOut = 1 then '' else '900101' end  ,
CostingCode2,
CostingCode3,
CostingCode4,
CostingCode5,
U_SYP_CONCEPTO

from (
select	ItemCode, 
ItemDescription,
WarehouseCode,
Quantity,
DiscountPercent, 
AccountCode,
UnitPrice,
LineTotal,

TaxCode,

TaxTotal= 0,
WTLiable=0,
TaxOnly=0,
ProjectCode,
CostingCode,
CostingCode2,
CostingCode3,
CostingCode4,
CostingCode5,
U_SYP_CONCEPTO,
--
FechaOutCusco,
FechaDoc ,
FlFechaOut=case when  year(FechaOutCusco)*100+month(FechaOutCusco)>year(FechaDoc)*100+month(FechaDoc) then 1 else 0 end
from (
select
ItemCode= '', 
ItemDescription='',
WarehouseCode='',
Quantity=1,
DiscountPercent=0, 
AccountCode=@CtaContable,--dd.CoCtaContab,
UnitPrice=round(abs(isnull(dd.SsMonto,0)),2),
LineTotal=round(abs(isnull(dd.SsMonto,0)),2),

TaxCode='EXE_IGV',
--TaxTotal=abs(isnull(dd.SsIGV,0)),
TaxTotal= 0,
WTLiable=0,--case when ((d.CoTipoDoc in ('RH','HTD') AND d.CoTipoOC in ('007','008','009')) or  isnull(SsDetraccion,0)>0) then 1 else 0 end,--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
TaxOnly=0,--cast(0 as bit),
ProjectCode='',
CostingCode=isnull(@CoCeCos,''),--isnull(dd.CoCeCos,''),
CostingCode2=isnull(dd.CoCeCon,'000000'),
CostingCode3=isnull((select idfile from coticab where idcab=dd.idcab),'00000000'),
CostingCode4='',
CostingCode5='',
U_SYP_CONCEPTO=dd.NuDocumProvDet,
--
FechaOutCusco=(Select Top 1 Dia From COTIDET Where IDCAB=e.IDCab And IDubigeo in (select IDubigeo from MAUBIGEO where IDPais='000323') Order by dia desc),
FechaDoc=d.FeRecepcion
from DOCUMENTO_PROVEEDOR d
inner join DOCUMENTO_PROVEEDOR_DET dd on d.NuDocumProv=dd.NuDocumProv
inner join ENTRADASINC_SAP e on e.NuDocumProv=dd.NuDocumProv and e.NuDocumProvDet=dd.NuDocumProvDet
Left Join COTICAB cc On cc.IDCAB=d.IDCab
where d.NuDocumProv=@nudocumprov
)as x
) as y
END;
