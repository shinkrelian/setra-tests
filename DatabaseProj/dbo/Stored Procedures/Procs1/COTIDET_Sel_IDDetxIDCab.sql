﻿--HLF-20130211-Agregando IDTipoProv
CREATE Procedure [dbo].[COTIDET_Sel_IDDetxIDCab]  
 @IDCAB int  
As  
 Set NoCount On  
   
 SELECT IDDET, p.IDTipoProv
 From COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
 where cd.IDCAB=@IDCAB  