﻿CREATE PROCEDURE [dbo].[LISTAR_DIA_MES]
@tipo char(1)
as
begin
	 declare @num char(2)
	 declare @ini tinyint
	 declare @fin tinyint
	 declare @temp table(
	 num char(2)
	 )	 
	 set @ini=1
	 set @fin=case when @tipo='D' then 31 else 12 end
	 while @ini<=@fin 
		begin
			set @num=case when LEN(LTRIM(@ini))=1 then '0'+LTRIM(@ini) else LTRIM(@ini) end
			set @ini=@ini+1
			insert into @temp(num) values(@num)		
		end
		select num id,num from @temp 
 end
