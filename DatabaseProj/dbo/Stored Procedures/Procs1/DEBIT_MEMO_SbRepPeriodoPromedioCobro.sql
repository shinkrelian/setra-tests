﻿

--------------------------------------------------------------------------------------------------

--JHD-20150203- Se corrigio problema de clientes repetidos, agrupando todo en una subconsulta y volviendo a sumar.
CREATE Procedure dbo.DEBIT_MEMO_SbRepPeriodoPromedioCobro
@IDCliente varchar(6),  
@IDPais varchar(6),  
@FecVencimientoIn smalldatetime,  
@FecVencimientoOut smalldatetime  
As  
Set NoCount On  

select Y.DescCliente,Y.DescPais,SUM(Y.ImporteUSD) ImporteUSD,sum(suma) as suma ,sum(contador) as contador,sum(suma) /sum(contador) as promedio

from( 

Select X.DescCliente,X.DescPais,SUM(X.ImporteUSD) ImporteUSD,sum(X.NroDiasDemora)as suma,COUNT(X.DescCliente) as contador,(sum(X.NroDiasDemora) /COUNT(X.DescCliente)) as Promedio from (

Select CLi.RazonComercial as DescCliente,  
SUBSTRING(dm.IDDebitMemo,1,8)+' - '+ SUBSTRING(dm.IDDebitMemo,9,10) As IDDebitMemo,  
dm.Titulo as Referencia,c.FecInicio,c.FechaOut,dm.Total as ImporteUSD,dm.Saldo,dm.Fecha as FechaCobro,dm.FecVencim,  
(select Top(1) ifz.FeAcreditada from INGRESO_FINANZAS ifz   
 where ifz.NuIngreso In(select NuIngreso from INGRESO_DEBIT_MEMO idm2 where idm2.IDDebitMemo=dm.IDDebitMemo)  
 Order by ifz.FeAcreditada asc) as FechaPago,  
DATEDIFF(D,dm.FecVencim,(select Top(1) ifz.FeAcreditada from INGRESO_FINANZAS ifz   
 where ifz.NuIngreso In(select NuIngreso from INGRESO_DEBIT_MEMO idm2 where idm2.IDDebitMemo=dm.IDDebitMemo)  
 Order by ifz.FeAcreditada asc)) AS NroDiasDemora,  
ub.Descripcion as DescPais  
from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab=c.IDCAB  
Left Join MACLIENTES CLi on dm.IDCliente=CLi.IDCliente  
Left Join MAUBIGEO ub On CLi.IDCiudad=ub.IDubigeo  
where IDTipo= 'I' And IDEstado='PG' --In ('PP','PD')  
and (Ltrim(rtrim(@IDCliente))='' Or dm.IDCliente=@IDCliente)  
and (Ltrim(rtrim(@IDPais))='' Or CLi.IDCiudad=@IDPais)  
 AND CLI.IDCliente<>'000054'
and (Ltrim(rtrim(Convert(Char(10),@FecVencimientoIn,103)))='01/01/1900' Or   
  dm.FecVencim between @FecVencimientoIn and @FecVencimientoOut)
  
  )
As X

Group By X.DescCliente,X.DescPais--,X.FecVencim

) as Y
group by Y.DescCliente,Y.DescPais

Order by DescCliente--,X.FecVencim
