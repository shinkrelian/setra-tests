﻿Create Procedure dbo.COTIVUELOS_Sel_TotalxPagar
@IDTransporte int,
@pSaldoPendiente numeric(8,2) output
As
Set NoCount On
Set @pSaldoPendiente = 0
Select @pSaldoPendiente=
	   Case When ISNULL(CostoOperador,0) =0 Then 0 Else
	   ISNULL(CostoOperador,0)*(select COUNT(*) from COTITRANSP_PAX cpx Where cpx.IDTransporte=c.ID) - 
	   IsNull((Select SUM(od.Total) from ORDENPAGO_DET od Left Join ORDENPAGO o On od.IDOrdPag=o.IDOrdPag
		where od.IDTransporte=c.ID and o.IDCab=c.IDCAB),0)
	   End
from COTIVUELOS c
where ID =@IDTransporte
