﻿
Create Procedure COTIPAX_Sel_ExisteOutput
@IDPax int,
@pExiste bit output
As
	Set NoCount On
	If Exists(select IDPax from COTIPAX where IDPax = @IDPax)
		set @pExiste = 1
	else
		set @pExiste = 0
