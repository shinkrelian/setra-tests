﻿CREATE Procedure DEBIT_MEMO_DET_SelxIDDebitMemo  
@IDDebitMemo char(10)  
As  
 Set NoCount On  
 SELECT [IDDebitMemo]  
    ,[IDDebitMemoDet]  
    ,[Descripcion]  
    ,[CapacidadHab]  
    ,[NroPax]  
    ,[CostoPersona]
    ,[SubTotal]  
    ,[TotalIGV]  
    ,[Total]  
  FROM [dbo].[DEBIT_MEMO_DET]  
  WHERE [dbo].[DEBIT_MEMO_DET].[IDDebitMemo] = @IDDebitMemo  
