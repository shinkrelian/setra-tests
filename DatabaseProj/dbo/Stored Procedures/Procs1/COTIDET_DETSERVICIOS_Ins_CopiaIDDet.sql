﻿CREATE Procedure [dbo].[COTIDET_DETSERVICIOS_Ins_CopiaIDDet]
	@IDDET int,
	@IDDETCopia int,
	@UserMod char(4)
As
	Set Nocount On
		
	INSERT INTO COTIDET_DETSERVICIOS
           (IDDet
           ,IDServicio_Det
           ,CostoReal
           ,CostoLiberado
           ,Margen
           ,MargenAplicado
           ,MargenLiberado
           ,Total
           ,SSCR
           ,SSCL
           ,SSMargen
           ,SSMargenLiberado
           ,SSTotal
           ,STCR
           ,STMargen
           ,STTotal
           ,CostoRealImpto
           ,CostoLiberadoImpto
           ,MargenImpto
           ,MargenLiberadoImpto
           ,SSCRImpto
           ,SSCLImpto
           ,SSMargenImpto
           ,SSMargenLiberadoImpto
           ,STCRImpto
           ,STMargenImpto
           ,TotImpto                      
           ,UserMod)
    Select
           @IDDet,
           IDServicio_Det,
           CostoReal ,
           CostoLiberado ,
           Margen ,
           MargenAplicado ,
           MargenLiberado ,
           Total ,
           SSCR ,
           SSCL ,
           SSMargen ,
           SSMargenLiberado ,
           SSTotal ,
           STCR ,
           STMargen ,
           STTotal ,
           CostoRealImpto ,
           CostoLiberadoImpto ,
           MargenImpto ,
           MargenLiberadoImpto ,
           SSCRImpto ,
           SSCLImpto ,
           SSMargenImpto ,
           SSMargenLiberadoImpto ,
           STCRImpto ,
           STMargenImpto ,
           TotImpto ,           
           @UserMod
	From COTIDET_DETSERVICIOS Where IDDet=@IDDETCopia
