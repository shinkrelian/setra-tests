﻿Create Procedure dbo.MAENTREVISTAPROVEEDOR_Ins
@IDProveedor char(6),
@IDIdioma varchar(12),
@FlConocimiento bit,
@FeEntrevita smalldatetime,
@IDUserEntre char(4),
@TxComentario varchar(max),
@Estado char(1),
@UserMod char(4) 
As
Set Nocount On
INSERT INTO [dbo].[MAENTREVISTAPROVEEDOR]
           ([IDProveedor]
           ,[IDIdioma]
		   ,FlConocimiento
           ,FeEntrevista
           ,[IDUserEntre]
           ,[TxComentario]
           ,[Estado]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@IDProveedor,
			@IDIdioma,
			@FlConocimiento,
			Case When Convert(Char(10),@FeEntrevita,103)='01/01/1900' Then Null Else @FeEntrevita End,
			Case When Ltrim(Rtrim(@IDUserEntre))='' Then Null Else Ltrim(Rtrim(@IDUserEntre)) End,
			Case When Ltrim(Rtrim(@TxComentario))='' Then Null Else Ltrim(Rtrim(@TxComentario)) End,
			Case When Ltrim(Rtrim(@Estado))='' then Null Else @Estado End,
			@UserMod,
			getdate())
