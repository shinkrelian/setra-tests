﻿--JRF-20130911-Correción del Orden
CREATE Procedure ACOMODOPAX_PROVEEDOR_Sel_ListxIDReserva  
@IDReserva int  
As  
 Set NoCount On  
 select IDReserva,ac.IDPax,IDHabit,CapacidadHab,EsMatrimonial 
 from ACOMODOPAX_PROVEEDOR ac Left Join COTIPAX cp On ac.IDPax = cp.IDPax 
 where IDReserva = @IDReserva
 Order by ac.CapacidadHab,ac.IDHabit,cp.Orden
