﻿--create
CREATE Procedure COTICAB_Upd_UserVentas
	@IDCab int,    
	@IDUsuario char(4),  
	@UserMod char(4)    
As     

 Set NoCount On    

 Update COTICAB     
  Set IDUsuario = @IDUsuario  
/*
  ,FechaAsigReserva = 
  case when LTRIM(rtrim(@IDUsuario))='0000' then 
	null 
  else 
	--GETDATE() 

	Case when COTICAB.FechaAsigReserva is null then 
		GETDATE() 
	else 
		COTICAB.FechaAsigReserva 
	End
	
  End  
  */
  ,UserMod = @UserMod,FecMod = GetDate()    
 Where IDCAB = @IDCab
