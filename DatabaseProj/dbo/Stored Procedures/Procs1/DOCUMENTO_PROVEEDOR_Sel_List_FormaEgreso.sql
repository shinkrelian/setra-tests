﻿
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_List_FormaEgreso]
					@NuDocumProv int=0,        
					@NuVoucher varchar(14)='',        
					@NuDocum varchar(30)='',
					@CoTipoDoc char(3)='',
					@FlActivo bit=0,
					@CoMoneda char(3)=''
AS
BEGIN
	SELECT DP.NuDocumProv, DP.IDDocFormaEgreso AS NuVoucher, CASE WHEN FlCorrelativo = 1 THEN '' ELSE 
													CASE WHEN DP.NuSerie IS NULL THEN '' ELSE DP.NuSerie + '-' END + DP.NuDocum END AS NuDocum,
	CASE WHEN FlCorrelativo = 1 THEN '' ELSE CASE WHEN DP.NuSerie IS NULL THEN '' ELSE DP.NuSerie + '-' END + DP.NuDocum END AS NuDocum2,  
    DP.CardCodeSAP AS NuDocInterno, DP.CoTipoDoc, MT.Descripcion, DP.FeEmision, DP.FeRecepcion, DP.CoTipoDetraccion,         
	PorcentajeDet=(select isnull(SsTasa,0)*100 from dbo.MATIPODETRACCION where CoTipoDetraccion=dp.CoTipoDetraccion),        
	DP.CoMoneda, MM.Descripcion AS Moneda, SsNeto, SsOtrCargos, SsDetraccion, DP.SsIGV, DP.SSTotalOriginal, DP.SSTipoCambio,
	DP.SSTotal, DP.FlActivo 
	FROM DOCUMENTO_PROVEEDOR AS DP
	INNER JOIN DOCUMENTO_FORMA_EGRESO AS DF ON DF.IDDocFormaEgreso = DP.IDDocFormaEgreso
	AND DF.TipoFormaEgreso = DP.CoObligacionPago AND DF.CoMoneda = DP.CoMoneda_FEgreso
	INNER JOIN MATIPODOC MT ON DP.CoTipoDoc = MT.IDtipodoc
	INNER JOIN MAMONEDAS MM ON DP.CoMoneda = MM.IDMoneda
	WHERE (DF.TipoFormaEgreso = 'ZIC') AND
	(DP.NuDocumProv = @NuDocumProv or @NuDocumProv = 0) AND (DP.IDDocFormaEgreso = @NuVoucher) AND        
	(@NuDocum = '' OR DP.NuDocum LIKE '%' + @NuDocum + '%' ) AND (DP.CoTipoDoc = @CoTipoDoc OR @CoTipoDoc = '') AND        
	(DP.FlActivo = @FlActivo OR @FlActivo = 0) AND (DP.CoMoneda_FEgreso = @CoMoneda OR @CoMoneda='')
END
