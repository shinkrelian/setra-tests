﻿--JRF-20130423-Orden x Pax                  
--JRF-20130429-Listar x Rangos                
--JRF-20130710-Nuevo campo             
--JRF-20131017-Correción de la cabecera principal(Titulo - T).      
--JRF-20140526-Nuevo campo [DocWordVersion]      
--JRF-20140808-Agregar el Tipo de Cambio de la tabla COTICAB_TIPOSCAMBIO    
--HLF-20140903-AND CABQ.Liberados=@NroLiberadosQuie  
--JRF-20140909-AND Isnull(CABQ.Liberados,0)=@NroLiberadosQuie  
--PMG-JRF-20151005-...ORDER BY CD.Item,CABQ.Pax asc,CABQ.Liberados asc
CREATE Procedure [dbo].[COTIDET_RptCostosCotizacion]                           
@IDCAB int ,                
@PaxInic int,                                  
@PaxFin int ,                                
@blnSiguRango bit                               
As                                
 Set NoCount On                                
                         
 Declare @IDDet varchar(30) ,                        
 @Dia smalldatetime ,                        
 @Item numeric(6, 3) ,                        
 @Cotizacion varchar(12) ,                        
 @IDTipoProv varchar(15) ,                        
 @Cliente varchar(60) ,                        
 @Titulo varchar(max) ,                        
 @Responsable varchar(60) ,                        
 @MargenGeneral varchar(60),            
             
 @TipoCambio varchar(30) ,                        
 @TipoPax varchar(15) ,                        
 @FechaInicio varchar(25) ,                        
 @FechaFinal varchar(25) ,                        
 @Fecha varchar(25) ,                        
 @Ubigeo varchar(60) ,                        
 @Proveedor varchar(100) ,                        
 @Servicio varchar(max) ,                        
 @Tipo char(5) ,                        
 @Anio char(4) ,                        
 @Var char(8) ,                        
 @Idioma varchar(12) ,                        
 @PlanAlimenticio varchar(1),                        
 @ContieneTPL varchar(1) ,                        
 @ConAlojamiento varchar(1),                      
                       
 @NroPax varchar(20) ,                        
 @NroLiberados varchar(20) ,                        
 @Margen varchar(20) ,                        
 @CostoTotalCal varchar(20) ,                        
 @CostoTotalDet varchar(20) ,                        
 @CostoNeto varchar(20) ,                        
 @Impuesto varchar(20) ,                        
 @CostoLiberado varchar(20) ,                        
 @SSCR varchar(20) ,                        
 @SSTotal varchar(20) ,                        
 @SSMargen varchar(20) ,                        
 @STCR varchar(20) ,                        
 @STTotal varchar(20) ,                        
 @STMargen varchar(20) ,                        
                         
 @NroPaxQuie varchar(20) ,                        
 @NroLiberadosQuie varchar(20) ,                        
 @MargenQuie varchar(20) ,                        
 @CostoTotalCalQuie varchar(20) ,                        
 @CostoTotalDetQuie varchar(20) ,                        
 @CostoNetoQuie varchar(20) ,                        
 @ImpuestoQuie varchar(20) ,                        
 @CostoLiberadoQuie varchar(20) ,                        
 @SSCRQuie varchar(20) ,                        
 @SSTotalQuie varchar(20) ,                        
 @SSMargenQuie varchar(20) ,                        
 @STCRQuie varchar(20) ,                        
 @STTotalQuie varchar(20) ,                        
 @STMargenQuie varchar(20) ,                        
                         
 @FilTitulo char(1) ,                        
 @Notas varchar(max) ,       
 @DocWordVersion tinyint,                       
 @Cont tinyint=1,@IDDetTmp int                                
                                    
 Delete from RptCtInterna                                  
 declare curCtInterna2 cursor for                                  
                                 
   SELECT  CD.IDDet,CD.Dia,CD.Item,cc.Cotizacion,pr.IDTipoProv,c.RazonComercial,cc.Titulo,u.Nombre,isnull(cc.margen,0) as MargenGeneral            
   --,isnull(cc.TipoCambio,0) As TipoCambio,    
   ,Isnull((select SsTipCam from COTICAB_TIPOSCAMBIO ctc where ctc.IDCab = cc.IDCab and CoMoneda = 'SOL'),0) As TipoCambio,    
   cc.TipoPax                                  
   ,convert(varchar(10),cc.FecInicio,103) as FechaInicial , Convert(varchar(10),cc.FechaOut,103) as FechaFinal                                   
   ,Upper(substring(datename(dw,CD.Dia),1,3))+' '+ Convert(varchar(60),CD.Dia,103) as Fecha,                                 
   Case When CD.NroLiberados is null then cc.NroLiberados else CD.NroLiberados End As NroLiberados,                                
   ub.Descripcion as Ubigeo,Pr.NombreCorto As Proveedor,         
   CD.Servicio + Case When CD.IncGuia = 1 then ' - Guide' else '' End As Servicio ,                                
   Case When S.IDTipoServ = 'NAP' Then '---' Else S.IDTipoServ End As Tipo                                
   ,Sd.Anio, Sd.Tipo As [Var],Idi.Siglas As Idioma,sd.PlanAlimenticio,                        
   Case When (sd.IDServicio_DetxTriple is null And sd.Monto_tri is null And                        
     sd.Monto_tris is null And sd.IdHabitTriple = '000') Then 0 Else 1 End As ContieneTPL ,                      
                        
    Case When pr.IDTipoProv = '003' and sd.ConAlojamiento = 1 then                      
 Case When sd.ConAlojamiento = 1 then                
  Case When isnull(cd.IDDetRel,0)= 0 then 1 else 0 End                    
 else                    
  0 End                    
  else                      
  Case When pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then                    
  sd.ConAlojamiento                    
 else 0 End                    
  End As ConAlojamiento                      
                            
   ,Case When CD.IncGuia = 1 then cc.NroPax else cd.nropax End As nropax,                                
    CABQ.Pax ,CABQ.Liberados as Liberados ,                           
   isnull(CD.MargenAplicado,0) as MargenAplicado,                                
   isnull(CD.CostoReal + CD.CostoLiberado + CD.TotImpto,0)As CostoCal,                                
   isnull(CD.Total,0) TotalDet,                                
   isnull(CD.CostoReal,0) CostoNeto,                                
   isnull(CD.TotImpto,0) TotImpto,                                
   isnull(CD.CostoLiberado,0) CostoLiberado,                                
   CD.SSCR ,                        
   CD.SSTotal,                        
   CD.SSMargen ,                          
   CD.STCR,                        
   CD.STTotal,                        
   CD.STMargen                         
   ,cast(cc.Notas as Varchar) as Notas  ,cc.DocWordVersion                                 
   FROM COTIDET_QUIEBRES CQ Right Join                                   
  (select top (5)* from COTICAB_QUIEBRES where IDCAB=@IDCAB And Pax between @PaxInic And @PaxFin Order By Pax) CABQ ON CQ.IDCAB_Q=CABQ.IDCAB_Q                                  
    Right Join COTIDET CD ON CQ.IDDET=CD.IDDET Left Join MAUBIGEO ub On cd.IDUbigeo=ub.IDUbigeo                                   
    Left Join COTICAB cc  On CD.IDCAB=cc.IDCAB Left Join MAUSUARIOS u On cc.IDUsuario=u.IDUsuario                                  
    Left Join MACLIENTES c On cc.IDCLiente= c.IDCliente Left Join MAPROVEEDORES pr On CD.IDProveedor=pr.IDProveedor                                  
    Left Join MAIDIOMAS Idi On CD.IDIdioma =  Idi.IDIdioma                                
    Left Join MASERVICIOS S On CD.IDServicio = S.IDServicio                                
    Left Join MASERVICIOS_DET Sd On CD.IDServicio_Det = Sd.IDServicio_Det                                
   WHERE CD.IDDet In (SELECT IDDET FROM COTIDET WHERE IDCAB=@IDCAB)                                          
   --And cd.IDDET=673648
   ORDER BY CD.Item,CABQ.Pax asc,CABQ.Liberados asc
                                 
  Open curCtInterna2                                  
  Fetch Next From curCtInterna2  Into @Iddet,@Dia ,@Item ,@Cotizacion,@IDTipoProv,@Cliente ,@Titulo ,@Responsable                         
   ,@MargenGeneral, @TipoCambio,@TipoPax ,@FechaInicio ,@FechaFinal ,@Fecha ,@NroLiberados ,@Ubigeo ,@Proveedor,@Servicio,@Tipo                         
   ,@Anio ,@Var ,@Idioma, @PlanAlimenticio, @ContieneTPL ,@ConAlojamiento ,@NroPax, @NroPaxQuie ,@NroLiberadosQuie                         
   ,@Margen,@CostoTotalCal ,@CostoTotalDet,@CostoNeto,@Impuesto,@CostoLiberado,     
    @SSCR,@SSTotal,@SSMargen,@STCR,@STTotal,@STMargen,@Notas,@DocWordVersion                           
                                 
 While  @@fetch_status = 0                                  
 Begin                    
if(@blnSiguRango=0)                
begin               
                
  SELECT  @MargenQuie = COTQ.MargenAplicado,                        
    @CostoTotalCalQuie =  (COTQ.CostoReal + COTQ.TotImpto + COTQ.CostoLiberado),                        
    @CostoTotalDetQuie = COTQ.Total,                        
    @CostoNetoQuie = COTQ.CostoReal,                        
    @ImpuestoQuie = COTQ.TotImpto,                        
    @CostoLiberadoQuie = COTQ.CostoLiberado,                        
    @SSCRQuie = COTQ.SSCR,                        
    @SSTotalQuie = COTQ.SSTotal,                        
    @SSMargenQuie = COTQ.SSMargen,                        
    @STCRQuie = COTQ.STCR,                        
    @STTotalQuie = COTQ.STTotal,                        
    @STMargenQuie = COTQ.STMargen                        
  FROM COTIDET_QUIEBRES COTQ LEFT JOIN COTICAB_QUIEBRES CABQ ON COTQ.IDCAB_Q=CABQ.IDCAB_Q                                  
  WHERE COTQ.IDDet=@IDDet AND CABQ.Pax=@NroPaxQuie                           
--HLF-20140903-I  
 AND Isnull(CABQ.Liberados,0)=Isnull(@NroLiberadosQuie,0)
--HLF-20140903-F  



  If @Cont = 1                                 
   Begin                                
    If Not Exists(select IDDet From RptCtInterna where FilTitulo='T')                                  
     Begin                                   
     insert into RptCtInterna ([IDDet],[Dia],[Item],[Cotizacion],[IDTipoProv],[Cliente],[Titulo],[Responsable]                        
      ,[MargenGeneral],[TipoCambio],[TipoPax],[FechaInicio],[FechaFinal],[Fecha],[Ubigeo],[Proveedor],[Servicio]                        
      ,[Tipo],[Anio],[Var],[Idioma],[PlanAlimenticio],[ContieneTPL],[ConAlojamiento],[NroPax],[NroLiberados],[Margen],[CostoTotalCal],[CostoTotalDet]                        
      ,[CostoNeto],[Impuesto],[CostoLiberado],[SSCR],[SSTotal],[SSMargen],[STCR],[STTotal],[STMargen]                        
      ,[FilTitulo],[Notas],[DocWordVersion])                                  
      values                                  
     ('IDDet','01/01/1900','0.000','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable                        
     ,@MargenGeneral,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal,'Fecha','Ubigeo','Proveedor','Descripción'                        
     ,'Tipo','Año','Var','Idi.','','','','','','',(select cast(NroPax as varchar(5)) from coticab where IDCab = @IDCab) + ISNULL(' + ' + (select case when NroLiberados is null then null else CAST(NroLiberados as varchar(5)) End as NroL from coticab where 

 
    
       
       
 IDCab = @IDCab),''),''                        
     ,'','','','','','','','',''                        
     ,'T','Notas',@DocWordVersion)                                
     End                                 
    If Not Exists (select IDDet from RptCtInterna where FilTitulo='S')                                  
     Begin                                  
     insert into RptCtInterna ([IDDet],[Dia],[Item],[Cotizacion],[IDTipoProv],[Cliente],[Titulo],[Responsable]                   
      ,[MargenGeneral],[TipoCambio],[TipoPax],[FechaInicio],[FechaFinal],[Fecha],[Ubigeo],[Proveedor],[Servicio]                        
      ,[Tipo],[Anio],[Var],[Idioma],[PlanAlimenticio],[ContieneTPL],[ConAlojamiento],[NroPax],[NroLiberados],[Margen],[CostoTotalCal],[CostoTotalDet]                        
      ,[CostoNeto],[Impuesto],[CostoLiberado],[SSCR],[SSTotal],[SSMargen],[STCR],[STTotal],[STMargen]                        
      ,[FilTitulo],[Notas],[DocWordVersion])                                  
      values                                  
     ('IDDet','01/01/1900','0.000','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable                        
     ,@MargenGeneral,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal,'Fecha','Ubigeo','Proveedor','Descripción'                        
     ,'Tipo','Año','Var','Idi.','','','','Nro. Pax','Nro. Lib.','%','Costo','Venta'                        
     ,'CostoNeto','Impuestos','CostoLib.','Costo','Venta','SSMargen','Costo','Venta','STMargen'                        
     ,'S','Notas',@DocWordVersion)                           
     End                        
    insert into RptCtInterna                                 
    values (@IDDet,@dia,@Item,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@MargenGeneral,@TipoCambio,@TipoPax                                  
   ,@FechaInicio,@FechaFinal,@fecha,@Ubigeo,@Proveedor,@servicio,@Tipo,@Anio,@Var,@Idioma,@PlanAlimenticio,@ContieneTPL  ,@ConAlojamiento                 
   ,@NroPax,@NroLiberados,@Margen,@CostoTotalCal,@CostoTotalDet,@CostoNeto,@Impuesto,@CostoLiberado,@SSCR,@SSTotal,@SSMargen,@STCR,@STTotal,@STMargen                        
   ,'','','','','','','','','','','','','',''                        
   ,'','','','','','','','','','','','','',''                        
   ,'','','','','','','','','','','','','',''                        
   ,'','','','','','','','','','','','','',''                        
   ,'R',@Notas,@DocWordVersion)                                   
   End                                
  If @Cont = 1                 
   Begin                                
    Update RptCtInterna  set CostoTotalCalC1=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC1 is null                                  
    if (@NroPaxQuie is not null)                                  
        Begin                                   
       update RptCtInterna                         
     set NroPaxC1 ='NroPax',NroLiberadosC1='Pax Lib.',MargenC1='%',                        
    CostoTotalCalC1='Costo', CostoTotalDetC1='Venta',CostoNetoC1= 'Neto',                        
    ImpuestoC1 ='Impuestos', CostoLiberadoC1 = 'Liberado',SSCRC1='Costo',                        
    SSTotalC1='Venta',SSMargenC1='SSMargen',STCRC1='Costo',STTotalC1='Venta',STMargenC1='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC1 is null                                  
        End                                  
     Update RptCtInterna                          
      set NroPaxC1 =@NroPaxQuie,NroLiberadosC1=@NroLiberadosQuie,MargenC1=@MargenQuie                        
      ,CostoTotalCalC1=@CostoTotalCalQuie ,CostoTotalDetC1=@CostoTotalDetQuie                           
      ,CostoNetoC1=@CostoNetoQuie ,ImpuestoC1 =@ImpuestoQuie,CostoLiberadoC1 = @CostoLiberadoQuie                           
      ,SSCRC1= @SSCRQuie,SSMargenC1=@SSMargenQuie,SSTotalC1=@SSTotalQuie,STCRC1=@STCRQuie,STTotalC1=@STTotalQuie,STMargenC1=@STMargenQuie,                        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                 
   End                               
                            
  If @Cont = 2                 
   Begin                                
    Update RptCtInterna  set CostoTotalCalC2=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC2 is null                                  
    if (@NroPaxQuie is not null)                                  
        Begin                                   
       update RptCtInterna                         
         set NroPaxC2 ='NroPax',NroLiberadosC2='Pax Lib.',MargenC2='%',                        
    CostoTotalCalC2='Costo', CostoTotalDetC2='Venta',CostoNetoC2= 'Neto',                        
    ImpuestoC2 ='Impuestos', CostoLiberadoC2 = 'Liberado',SSCRC2='Costo',                        
    SSTotalC2='Venta',SSMargenC2='SSMargen',STCRC2='Costo',STTotalC2='Venta',STMargenC2='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC2 is null                                  
        End                                  
     Update RptCtInterna                          
   set NroPaxC2 =@NroPaxQuie,NroLiberadosC2=@NroLiberadosQuie,MargenC2=@MargenQuie                        
      ,CostoTotalCalC2=@CostoTotalCalQuie ,CostoTotalDetC2=@CostoTotalDetQuie                           
      ,CostoNetoC2=@CostoNetoQuie ,ImpuestoC2 =@ImpuestoQuie,CostoLiberadoC2 = @CostoLiberadoQuie                           
      ,SSCRC2= @SSCRQuie,SSMargenC2=@SSMargenQuie,SSTotalC2=@SSTotalQuie,STCRC2=@STCRQuie,STTotalC2=@STTotalQuie,STMargenC2=@STMargenQuie,                        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                 
   End                                
  If @cont = 3                                
   Begin                                
   Update RptCtInterna  set CostoTotalCalC3=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC3 is null                                  
    if (@NroPaxQuie is not null)                                  
        Begin                                   
       update RptCtInterna                         
         set NroPaxC3 ='NroPax',NroLiberadosC3='Pax Lib.',MargenC3='%',                        
    CostoTotalCalC3='Costo', CostoTotalDetC3='Venta',CostoNetoC3= 'Neto',                        
    ImpuestoC3 ='Impuestos', CostoLiberadoC3 = 'Liberado',SSCRC3='Costo',                        
    SSTotalC3='Venta',SSMargenC3='SSMargen',STCRC3='Costo',STTotalC3='Venta',STMargenC3='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC3 is null                       
        End                                  
     Update RptCtInterna                          
   set NroPaxC3 =@NroPaxQuie,NroLiberadosC3=@NroLiberadosQuie,MargenC3=@MargenQuie                        
      ,CostoTotalCalC3=@CostoTotalCalQuie ,CostoTotalDetC3=@CostoTotalDetQuie                           
      ,CostoNetoC3=@CostoNetoQuie ,ImpuestoC3 =@ImpuestoQuie,CostoLiberadoC3 = @CostoLiberadoQuie                           
      ,SSCRC3= @SSCRQuie,SSMargenC3=@SSMargenQuie,SSTotalC3=@SSTotalQuie,STCRC3=@STCRQuie,STTotalC3=@STTotalQuie,STMargenC3=@STMargenQuie,                        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                      
   End                                   
  If @cont = 4                                
   Begin                                
   Update RptCtInterna  set CostoTotalCalC4=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC4 is null                                  
    if (@NroPaxQuie is not null)                                  
        Begin                                   
       update RptCtInterna                         
         set NroPaxC4 ='NroPax',NroLiberadosC4='Pax Lib.',MargenC4='%',                        
    CostoTotalCalC4='Costo', CostoTotalDetC4='Venta',CostoNetoC4= 'Neto',                        
    ImpuestoC4 ='Impuestos', CostoLiberadoC4 = 'Liberado',SSCRC4='Costo',                        
    SSTotalC4='Venta',SSMargenC4='SSMargen',STCRC4='Costo',STTotalC4='Venta',STMargenC4='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC4 is null                                  
        End                                  
     Update RptCtInterna                          
   set NroPaxC4 =@NroPaxQuie,NroLiberadosC4=@NroLiberadosQuie,MargenC4=@MargenQuie                        
      ,CostoTotalCalC4=@CostoTotalCalQuie ,CostoTotalDetC4=@CostoTotalDetQuie                           
      ,CostoNetoC4=@CostoNetoQuie ,ImpuestoC4 =@ImpuestoQuie,CostoLiberadoC4 = @CostoLiberadoQuie                           
      ,SSCRC4= @SSCRQuie,SSMargenC4=@SSMargenQuie,SSTotalC4=@SSTotalQuie,STCRC4=@STCRQuie,STTotalC4=@STTotalQuie,STMargenC4=@STMargenQuie,                        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                      
   End                 
End                              
                                  
else                
                
Begin                
  SELECT  @MargenQuie = COTQ.MargenAplicado,                        
    @CostoTotalCalQuie =  (COTQ.CostoReal + COTQ.TotImpto + COTQ.CostoLiberado),        
    @CostoTotalDetQuie = COTQ.Total,                        
    @CostoNetoQuie = COTQ.CostoReal,                        
    @ImpuestoQuie = COTQ.TotImpto,                        
    @CostoLiberadoQuie = COTQ.CostoLiberado,                        
    @SSCRQuie = COTQ.SSCR,                        
    @SSTotalQuie = COTQ.SSTotal,                        
    @SSMargenQuie = COTQ.SSMargen,                        
    @STCRQuie = COTQ.STCR,                        
    @STTotalQuie = COTQ.STTotal,                        
    @STMargenQuie = COTQ.STMargen                        
  FROM COTIDET_QUIEBRES COTQ LEFT JOIN COTICAB_QUIEBRES CABQ ON COTQ.IDCAB_Q=CABQ.IDCAB_Q                                  
  WHERE COTQ.IDDet=@IDDet AND CABQ.Pax=@NroPaxQuie     
--HLF-20140903-I  
 AND IsNull(CABQ.Liberados,0)=IsNull(@NroLiberadosQuie,0)
--HLF-20140903-F  
       
  If @Cont = 1                                 
   Begin                                
    If Not Exists(select IDDet From RptCtInterna where FilTitulo='T')                                  
     Begin                                   
     insert into RptCtInterna ([IDDet],[Dia],[Item],[Cotizacion],[IDTipoProv],[Cliente],[Titulo],[Responsable]                        
      ,[MargenGeneral],[TipoCambio],[TipoPax],[FechaInicio],[FechaFinal],[Fecha],[Ubigeo],[Proveedor],[Servicio]                        
      ,[Tipo],[Anio],[Var],[Idioma],[PlanAlimenticio],[ContieneTPL],[ConAlojamiento],[NroPax],[NroLiberados],[Margen],[CostoTotalCal],[CostoTotalDet]                        
      ,[CostoNeto],[Impuesto],[CostoLiberado],[SSCR],[SSTotal],[SSMargen],[STCR],[STTotal],[STMargen]                        
      ,[FilTitulo],[Notas],[DocWordVersion])                                  
      values                                  
     ('IDDet','01/01/1900','0.000','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable                        
     ,@MargenGeneral,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal,'Fecha','Ubigeo','Proveedor','Descripción'                        
     ,'Tipo','Año','Var','Idi.','','','','','','',@NroPaxQuie + ISNULL(' + ' + @NroLiberadosQuie,''),''                        
     ,'','','','','','','','',''                        
     ,'T','Notas',@DocWordVersion)                                
     End                                 
    If Not Exists (select IDDet from RptCtInterna where FilTitulo='S')                                  
     Begin                                  
     insert into RptCtInterna ([IDDet],[Dia],[Item],[Cotizacion],[IDTipoProv],[Cliente],[Titulo],[Responsable]                  
      ,[MargenGeneral],[TipoCambio],[TipoPax],[FechaInicio],[FechaFinal],[Fecha],[Ubigeo],[Proveedor],[Servicio]                        
      ,[Tipo],[Anio],[Var],[Idioma],[PlanAlimenticio],[ContieneTPL],[ConAlojamiento],[NroPax],[NroLiberados],[Margen],[CostoTotalCal],[CostoTotalDet]                        
      ,[CostoNeto],[Impuesto],[CostoLiberado],[SSCR],[SSTotal],[SSMargen],[STCR],[STTotal],[STMargen]                        
      ,[FilTitulo],[Notas],[DocWordVersion])                                  
      values                                  
     ('IDDet','01/01/1900','0.000','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable                        
     ,@MargenGeneral,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal,'Fecha','Ubigeo','Proveedor','Descripción'                        
     ,'Tipo','Año','Var','Idi.','','','','Nro. Pax','Nro. Lib.','%','Costo','Venta'                        
     ,'CostoNeto','Impuestos','CostoLib.','Costo','Venta','SSMargen','Costo','Venta','STMargen'                        
     ,'S','Notas',@DocWordVersion)                           
     End                        
    insert into RptCtInterna                                 
    values (@IDDet,@dia,@Item,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@MargenGeneral,@TipoCambio,@TipoPax                                  
   ,@FechaInicio,@FechaFinal,@fecha,@Ubigeo,@Proveedor,@servicio,@Tipo,@Anio,@Var,@Idioma,@PlanAlimenticio,@ContieneTPL,@ConAlojamiento                               
   ,@NroPaxQuie,@NroLiberadosQuie,@MargenQuie,@CostoTotalCalQuie,@CostoTotalDetQuie                
  ,@CostoNetoQuie,@ImpuestoQuie,@CostoLiberadoQuie,@SSCRQuie,@SSTotalQuie,@SSMargenQuie,@STCRQuie,@STTotalQuie,@STMargenQuie                
   ,'','','','','','','','','','','','','',''                        
   ,'','','','','','','','','','','','','',''                        
   ,'','','','','','','','','','','','','',''                        
   ,'','','','','','','','','','','','','',''                        
   ,'R',@Notas,@DocWordVersion)                                   
   End                                
  If @Cont = 2                                 
   Begin                                
    Update RptCtInterna  set CostoTotalCalC1=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC1 is null                 
    if (@NroPaxQuie is not null)                              
        Begin                                  
       update RptCtInterna                         
         set NroPaxC1 ='NroPax',NroLiberadosC1='Pax Lib.',MargenC1='%',                        
    CostoTotalCalC1='Costo', CostoTotalDetC1='Venta',CostoNetoC1= 'Neto',                        
    ImpuestoC1 ='Impuestos', CostoLiberadoC1 = 'Liberado',SSCRC1='Costo',                        
    SSTotalC1='Venta',SSMargenC1='SSMargen',STCRC1='Costo',STTotalC1='Venta',STMargenC1='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC1 is null                                  
        End                                  
     Update RptCtInterna                          
   set NroPaxC1 =@NroPaxQuie,NroLiberadosC1=@NroLiberadosQuie,MargenC1=@MargenQuie                        
      ,CostoTotalCalC1=@CostoTotalCalQuie ,CostoTotalDetC1=@CostoTotalDetQuie                           
      ,CostoNetoC1=@CostoNetoQuie ,ImpuestoC1 =@ImpuestoQuie,CostoLiberadoC1 = @CostoLiberadoQuie                           
      ,SSCRC1= @SSCRQuie,SSMargenC1=@SSMargenQuie,SSTotalC1=@SSTotalQuie,STCRC1=@STCRQuie,STTotalC1=@STTotalQuie,STMargenC1=@STMargenQuie,                        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                 
   End                               
                            
  If @Cont = 3                               
   Begin                        
    Update RptCtInterna  set CostoTotalCalC2=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC2 is null                                  
    if (@NroPaxQuie is not null)                    
        Begin                                   
       update RptCtInterna                         
         set NroPaxC2 ='NroPax',NroLiberadosC2='Pax Lib.',MargenC2='%',                        
    CostoTotalCalC2='Costo', CostoTotalDetC2='Venta',CostoNetoC2= 'Neto',                        
    ImpuestoC2 ='Impuestos', CostoLiberadoC2 = 'Liberado',SSCRC2='Costo',                        
    SSTotalC2='Venta',SSMargenC2='SSMargen',STCRC2='Costo',STTotalC2='Venta',STMargenC2='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC2 is null            
        End                                  
     Update RptCtInterna                          
   set NroPaxC2 =@NroPaxQuie,NroLiberadosC2=@NroLiberadosQuie,MargenC2=@MargenQuie                        
      ,CostoTotalCalC2=@CostoTotalCalQuie ,CostoTotalDetC2=@CostoTotalDetQuie                           
      ,CostoNetoC2=@CostoNetoQuie ,ImpuestoC2 =@ImpuestoQuie,CostoLiberadoC2 = @CostoLiberadoQuie                           
      ,SSCRC2= @SSCRQuie,SSMargenC2=@SSMargenQuie,SSTotalC2=@SSTotalQuie,STCRC2=@STCRQuie,STTotalC2=@STTotalQuie,STMargenC2=@STMargenQuie,                        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                 
   End                                
  If @cont = 4                                
   Begin                                
   Update RptCtInterna  set CostoTotalCalC3=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC3 is null                                  
    if (@NroPaxQuie is not null)                                  
        Begin                                   
       update RptCtInterna                         
         set NroPaxC3 ='NroPax',NroLiberadosC3='Pax Lib.',MargenC3='%',                        
    CostoTotalCalC3='Costo', CostoTotalDetC3='Venta',CostoNetoC3= 'Neto',                        
    ImpuestoC3 ='Impuestos', CostoLiberadoC3 = 'Liberado',SSCRC3='Costo',                        
    SSTotalC3='Venta',SSMargenC3='SSMargen',STCRC3='Costo',STTotalC3='Venta',STMargenC3='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC3 is null                                  
        End                                  
     Update RptCtInterna                          
   set NroPaxC3 =@NroPaxQuie,NroLiberadosC3=@NroLiberadosQuie,MargenC3=@MargenQuie                        
      ,CostoTotalCalC3=@CostoTotalCalQuie ,CostoTotalDetC3=@CostoTotalDetQuie                           
      ,CostoNetoC3=@CostoNetoQuie ,ImpuestoC3 =@ImpuestoQuie,CostoLiberadoC3 = @CostoLiberadoQuie                           
      ,SSCRC3= @SSCRQuie,SSMargenC3=@SSMargenQuie,SSTotalC3=@SSTotalQuie,STCRC3=@STCRQuie,STTotalC3=@STTotalQuie,STMargenC3=@STMargenQuie,                        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                 
   End                                   
  If @cont = 5                                
   Begin                                
   Update RptCtInterna  set CostoTotalCalC4=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC4 is null                                  
    if (@NroPaxQuie is not null)                                  
        Begin                                   
       update RptCtInterna                         
         set NroPaxC4 ='NroPax',NroLiberadosC4='Pax Lib.',MargenC4='%',                        
    CostoTotalCalC4='Costo', CostoTotalDetC4='Venta',CostoNetoC4= 'Neto',                        
    ImpuestoC4 ='Impuestos', CostoLiberadoC4 = 'Liberado',SSCRC4='Costo',                        
    SSTotalC4='Venta',SSMargenC4='SSMargen',STCRC4='Costo',STTotalC4='Venta',STMargenC4='STMargen'                        
    where FilTitulo='S' and CostoTotalCalC4 is null                                  
        End                                  
     Update RptCtInterna                          
   set NroPaxC4 =@NroPaxQuie,NroLiberadosC4=@NroLiberadosQuie,MargenC4=@MargenQuie                        
      ,CostoTotalCalC4=@CostoTotalCalQuie ,CostoTotalDetC4=@CostoTotalDetQuie                           
      ,CostoNetoC4=@CostoNetoQuie ,ImpuestoC4 =@ImpuestoQuie,CostoLiberadoC4 = @CostoLiberadoQuie                           
      ,SSCRC4= @SSCRQuie,SSMargenC4=@SSMargenQuie,SSTotalC4=@SSTotalQuie,STCRC4=@STCRQuie,STTotalC4=@STTotalQuie,STMargenC4=@STMargenQuie,        
   servicio=@servicio                                  
     where FilTitulo='R' and IDDet=@IDDet                                      
   End                  
End                                   
                  
  Set @IDDetTmp=@IDDet      
  --Set @NroLiberadosQuie=''                            
  Fetch Next From curCtInterna2  Into @Iddet,@Dia ,@Item ,@Cotizacion,@IDTipoProv,@Cliente ,@Titulo ,@Responsable                         
   ,@MargenGeneral, @TipoCambio,@TipoPax ,@FechaInicio ,@FechaFinal ,@Fecha ,@NroLiberados ,@Ubigeo ,@Proveedor,@Servicio,@Tipo                         
   ,@Anio ,@Var ,@Idioma, @PlanAlimenticio, @ContieneTPL ,@ConAlojamiento ,@NroPax, @NroPaxQuie ,@NroLiberadosQuie                         
   ,@Margen,@CostoTotalCal ,@CostoTotalDet,@CostoNeto,@Impuesto,@CostoLiberado,                        
    @SSCR,@SSTotal,@SSMargen,@STCR,@STTotal,@STMargen,@Notas,@DocWordVersion                          
                                  
                                   
  If @IDDet=@IDDetTmp      
  Set @Cont=@Cont+1                                      
  Else                                      
    Set @Cont=1                                  
 End                                
                                        
 Close curCtInterna2                                  
 Deallocate curCtInterna2                                  
 Select *,0 as NroDebitMemoOrd From RptCtInterna Order by FilTitulo Desc ,Dia ASC,Item ASC 
