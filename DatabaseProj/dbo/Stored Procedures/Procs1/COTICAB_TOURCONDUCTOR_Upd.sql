﻿--JRF-20150828-Campos nuevos [SsGEBPrimerDia,SsGEBUltimoDia]
CREATE Procedure dbo.COTICAB_TOURCONDUCTOR_Upd
@IDCab int ,
@FeDesde smalldatetime ,
@FeHasta smalldatetime ,
@FlIncluiNochesAntDesp bit,
@SsMontoRealHotel numeric(8,2),
@SsMontoTotalHotel numeric(8,2) ,
@FlIncluirVuelo bit,
@SsVuelo numeric(8,2) ,
@CoTipoHon char(3) ,
@SsHonorario numeric(8,2) ,
@PoMargen numeric(5,2) ,
@SsViaticosSOL numeric(8,2) ,
@SsViaticosDOL numeric(8,2) ,
@SsTicketAereos numeric(8,2) ,
@SsIGVHoteles numeric(8,2) ,
@SsTarjetaTelef numeric(8,2) ,
@SsTaxi numeric(8,2),
@SsOtros numeric(8,2),
@SsGEBPrimerDia numeric(8,2),
@SsGEBUltimoDia numeric(8,2),
@Total numeric(8,2),
@UserMod char(4)
As
Set NoCount On
UPDATE [dbo].[COTICAB_TOURCONDUCTOR]
   SET [FeDesde] = @FeDesde
      ,[FeHasta] = @FeHasta
      ,[FlIncluiNochesAntDesp] = @FlIncluiNochesAntDesp
      ,[SsMontoRealHotel] = @SsMontoRealHotel
      ,[SsMontoTotalHotel] = @SsMontoTotalHotel
      ,[FlIncluirVuelo]=@FlIncluirVuelo
      ,[SsVuelo] = @SsVuelo
      ,[CoTipoHon] = @CoTipoHon
      ,[SsHonorario] = @SsHonorario
      ,[PoMargen] = @PoMargen
      ,[SsViaticosSOL] = @SsViaticosSOL
      ,[SsViaticosDOL] = @SsViaticosDOL
      ,[SsTicketAereos] = @SsTicketAereos
      ,[SsIGVHoteles] = @SsIGVHoteles
      ,[SsTarjetaTelef] = @SsTarjetaTelef
      ,[SsTaxi] = @SsTaxi
      ,[SsOtros] = @SsOtros
      ,[Total] = @Total
      ,[UserMod] = @UserMod
	  ,[SsGEBPrimerDia] = @SsGEBPrimerDia
	  ,[SsGEBUltimoDia] = @SsGEBUltimoDia
      ,[FecMod] = getdate()
 WHERE IDCab=@IDCab
