﻿Create Procedure dbo.COTIVUELOS_Sel_TotalxPagado
@IDTransporte int,
@pSaldoPagado numeric(8,2) output
As
Set NoCount On
Set @pSaldoPagado = 0
Declare @IDCab int = (select IDCAB from COTIVUELOS where ID=@IDTransporte)
Select @pSaldoPagado=IsNull(
		(select SUM(Total) from ORDENPAGO_DET od Left Join ORDENPAGO o On od.IDOrdPag=o.IDOrdPag
		 where IDTransporte=@IDTransporte and o.IDCab=@IDCab),0)
