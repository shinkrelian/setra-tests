﻿--JRF-20130704-Agregar el campo IDEstado                      
--JRF-20130709-Agregar la columna Descripcion del Tipo                    
--JRF-20130927-Ordenar por Tipo,Fecha Ingreso                  
--JRF-20130927-Campos FechaIN, FechaOut convertir a Smalldatetime.                  
--JRF-20131029-Agregar Filtro por País y por Monto.                 
--JRF-20131108-Agregar la columna de País.            
--JRF-20140123-Agregar filtro por la columna FecVencim      
--JRF-20140311-Agregar nueva columna de CoTipoVenta      
--HLF-20140904-dm.Cliente as DescrCliente,  
--HLF-20141211-Case When c.EstadoFacturac<>'P' Then ...
--HLF-20150512-Case When dm.IDCliente=@CoClienteVtasAdicAPT Then...
CREATE Procedure DBO.DEBIT_MEMO_Sel_List     
	@DescCliente varchar(80),                          
	@Numero varchar(10),                          
	@Referencia varchar(100),                          
	@DescEjecutivo varchar(60),                          
	@Tipo char(1),                          
	@IDEstado char(2),                          
	@IDCab int ,                
	@IDPais varchar(6),                
	@TotalRango1 numeric(10,2),                
	@TotalRango2 numeric(10,2),        
	@RangoFecVencim1 smalldatetime,        
	@RangoFecVencim2 smalldatetime        
As          

	Set NoCount On                   
	if @TotalRango1=0 and @TotalRango2 > 0                
	Begin                
	set @TotalRango1 = (select MIN(total) from DEBIT_MEMO)                
	End                
          
	if @TotalRango2=0 and @TotalRango1 > 0                
	Begin                
	set @TotalRango2 = (select max(total) from DEBIT_MEMO)                
	End                

	Declare @CoClienteVtasAdicAPT char(6)=(Select CoClienteVtasAdicAPT From PARAMETRO)
             
	Select Case When dm.IDTipo ='R' Then 'RESUMEN' Else 'INDIVIDUAL' End as DescTipo                    
	,dm.IDDebitMemo,                          
	case when dm.IDTipo = 'R' then cast('01/01/1900' As smalldatetime) else Cast(CONVERT(char(10),dm.FechaIn,103) as smalldatetime) End as FechaIn,                      
	case when dm.IDTipo = 'R' then cast('01/01/1900' As smalldatetime) else Cast(CONVERT(char(10),dm.FechaOut,103) as smalldatetime) End as FechaOut,                      
	Case c.CoTipoVenta    
	when 'RC' Then 'RECEPTIVO'    
	when 'CU' Then 'COUNTER'    
	when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'    
	when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'    
	End as DescTipoVenta,    
	--SUBSTRING(dm.IDDebitMemo,0,9) 
	Case When dm.IDCliente=@CoClienteVtasAdicAPT Then
		c.IDFile
	Else
		SUBSTRING(dm.IDDebitMemo,0,9) 
	End As NroFile,
	SUBSTRING(dm.IDDebitMemo,9,9) As Correlativo,                          
	--cl.RazonComercial as DescrCliente,             
	dm.Cliente as DescrCliente,  
	ub.Descripcion as Pais,                     
	dm.Titulo As Referencia,                          
	dm.Responsable As Ejecutivo,                
	dm.Total,            
	dm.Saldo,                                   
	dm.IDEstado,                        
	dm.FlImprimioDebit,                      
	Isnull(dm.IDCab,0) As IDCAB,                    
	Isnull(dm.IDDebitMemoResumen,'') as IDDebitMemoResumen,                    
	case when dm.IDTipo ='R' then (select top(1) c.IDCliente from DEBIT_MEMO dm2 LEFT join COTICAB c on dm2.IDCab = c.IDCAB                     
		where dm2.IDDebitMemoResumen = dm.IDDebitMemo) else '' end as IDClienteResumen,          
	Case When Exists(select NuIngreso from INGRESO_DEBIT_MEMO idm 
		where idm.IDDebitMemo = dm.IDDebitMemo) then 	
		1 
	Else 
		Case When c.EstadoFacturac<>'P' Then 
			1
		Else
			0 
		End
	End as ExistePagos          
	from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab = c.IDCAB                          
	Left Join MACLIENTES cl On dm.IDCliente = cl.IDCliente                          
	Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario                          
	Left Join MAUBIGEO ub on cl.IDCiudad = ub.IDubigeo                
	Where (LTRIM(rtrim(@DescCliente))='' Or cl.RazonComercial like @DescCliente + '%')                          
	And (ltrim(rtrim(@Numero))='' Or dm.IDDebitMemo like '%' + @Numero + '%')                          
	And (ltrim(rtrim(@Referencia))='' Or dm.Titulo like '%' + @Referencia + '%')                           
	And (ltrim(rtrim(@DescEjecutivo))='' Or u.Nombre like '%' + @DescEjecutivo + '%')                          
	And (ltrim(rtrim(@Tipo))='' Or dm.IDTipo = @Tipo) and dm.IDTipo <> 'S' and dm.IDTipo <> 'C'          
	And (ltrim(rtrim(@IDEstado))='' Or dm.IDEstado = @IDEstado)                          
	And (ltrim(rtrim(@IDPais))='' Or cl.IDCiudad = @IDPais)                
	And (dm.IDCab = @IDCab Or @IDCab = 0)                          
	And ((@TotalRango1 = 0 and @TotalRango2 = 0) or (dm.Total >= @TotalRango1 and dm.Total <= @TotalRango2) )                
	And ((Convert(Char(10),@RangoFecVencim1,103)='01/01/1900' and Convert(Char(10),@RangoFecVencim2,103)='01/01/1900')        
	Or  dm.FecVencim between @RangoFecVencim1 and @RangoFecVencim2)        
	Order By dm.IDTipo,dm.FechaIn,SUBSTRING(dm.IDDebitMemo,0,9) ,SUBSTRING(dm.IDDebitMemo,9,9) Asc               
