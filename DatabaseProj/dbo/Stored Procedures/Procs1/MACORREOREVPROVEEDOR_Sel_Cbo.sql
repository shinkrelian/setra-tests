﻿Create procedure [dbo].[MACORREOREVPROVEEDOR_Sel_Cbo] 
@IDProveedor char(6)
as
	Set NoCount On
	Select IDProveedor,Correo from MACORREOREVPROVEEDOR
	
	where IDProveedor = @IDProveedor
	And Estado = 'A'
