﻿
--JRF-20141212-NumIdentidad=@NumIdentidad,
--JHD-20150305-FecNacimiento=Case When @FecNacimiento='01/01/1900' Then Null Else @FecNacimiento End,
--PPMG-20160428-Se aumento el lenght @NumIdentidad varchar(25)
CREATE procedure [dbo].[COTIPAX_UPD_EXCEL]  
@IDPax int,  
@Nombres varchar(50),  
@Apellidos varchar(50),  
@IDIdentidad char(3),  
@NumIdentidad varchar(25),  
@IDNacionalidad char(6),  
@FecNacimiento smalldatetime='01/01/1900',  
@UserMod char(4),  
--@FlUpdExcel bit=0,  
@NoRutaArchivoExcelPax varchar(250)='',  
@CoUserExcelPax char(4)='',  
@NoPCExcelPax varchar(80)=''  
AS  
BEGIN  
  
UPDATE COTIPAX  
SET  
Nombres=@Nombres,  
Apellidos=@Apellidos,  
IDIdentidad=@IDIdentidad,  
NumIdentidad=@NumIdentidad,
IDNacionalidad=@IDNacionalidad,  
FecNacimiento=Case When @FecNacimiento='01/01/1900' Then Null Else @FecNacimiento End,--@FecNacimiento,  
FlUpdExcel=1,  
NoRutaArchivoExcelPax =@NoRutaArchivoExcelPax,  
CoUserExcelPax =@CoUserExcelPax,  
NoPCExcelPax =@NoPCExcelPax,  
UserMod=@UserMod,  
FecMod=GETDATE()  
WHERE IDPax=@IDPax  
  
END;  


