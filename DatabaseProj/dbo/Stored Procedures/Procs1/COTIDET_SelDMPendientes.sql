﻿
--HLF-20151030-And Not exists(select NuDocum from DOCUMENTO where IDTipoDocVinc=Y.TipoDoc And NuDocumVinc=dv.NuDocum)
CREATE Procedure dbo.COTIDET_SelDMPendientes
	@IDCab int
As
	Set Nocount On
	
	Declare @IDDebitMemo char(10)='', @Total numeric(8,2)=0, @TotalIGV numeric(8,2)=0
	
	Select @IDDebitMemo=IDDebitMemo, @Total=Total, @TotalIGV=TotalIGV 
		From DEBIT_MEMO where IDCAB=@IDCab and FlPorAjustePrecio=1 
			And FlAjustePrecioenDocNota=0

	
	Select 
	Case When dv.IDTipoDoc is null Then Y.TipoDoc Else 'NDB' End as IDTipoDoc, 
	--SerieDoc,
	Case When dv.IDTipoDoc is null Then Y.SerieDoc Else 
		--'001' 
		Case When dv.IDTipoDoc='BLE' Then '006' Else '001' End
	End as SerieDoc,
	Y.Total, Y.TotalImpto,
	isnull(dv.NuDocum,'') as NuDocumVinc, isnull(dv.FeDocum,'01/01/1900') as FeDocumVinc, 
	isnull(dv.IDTipoDoc,'') as IDTipoDocVinc
	
	From
	(
	Select TipoDoc, SerieDoc,
		--SUM(Total) as Total, SUM(TotalImpto) as TotalImpto From
		@Total as Total, @TotalIGV as TotalImpto From
		(
		Select 
		Case When sd.idTipoOC='006' Or sd.idTipoOC='001' Then 
			'FAC' 
		Else
			 Case When sd.idTipoOC='011' Then
				'BLE'
			 Else
				'BOL' 
			 End
		End as TipoDoc,
		Case When sd.idTipoOC='006' Or sd.idTipoOC='001' Then 
			'001' 
		Else
			 Case When sd.idTipoOC='011' Then
				'006'
			 Else
				'001' 
			 End
		End as SerieDoc
		--cd.Total*cd.NroPax as Total, 
		--cd.TotImpto*cd.NroPax as TotalImpto 
		From COTIDET cd Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
			And cd.IDCAB=@IDCab
		 where cd.IDDebitMemo in
		 (@IDDebitMemo)
		--(select IDDebitMemo from DEBIT_MEMO where IDCAB=@IDCab and FlPorAjustePrecio=1 
		--		And FlAjustePrecioenDocNota=0)
		) as X
	Group by TipoDoc, SerieDoc
	) as Y Left Join 
	DOCUMENTO dv On IDCab=@IDCab And dv.IDTipoDoc=Y.TipoDoc --And LEFT(dv.NuDocum,3)='001'
		And dv.FlDocManual=0
		And Not exists(select NuDocum from DOCUMENTO where IDTipoDocVinc=Y.TipoDoc And NuDocumVinc=dv.NuDocum)
	Where Y.Total<>0
	Order by Y.TipoDoc
