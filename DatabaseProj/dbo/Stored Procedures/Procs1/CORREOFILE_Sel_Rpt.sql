﻿
--JRF-20130923-Agregar un campo Imagen para Mostrar el mensaje HTML.
CREATE Procedure [dbo].[CORREOFILE_Sel_Rpt]  
@NuCorreo int  
As  
 Set NoCount On  
 Select   
 Case When CoTipoBandeja = 'BE' Then pr.NombreCorto+'<'+cf.NoCuentaDe+'>'   
 else u.Nombre +'<'+u.Correo+'>' End As De,  
 dbo.FnDestinatariosCorreo(@NuCorreo,0) As Para  
 ,cf.NoAsunto,FeCorreo,Cast('' as binary) As Imagen  
 From .BDCORREOSETRA..CORREOFILE cf Left Join MAUSUARIOS u On cf.CoUsrIntDe =u.IDUsuario  
 Left Join MAPROVEEDORES pr On cf.CoUsrPrvDe = pr.IDProveedor  
 Where cf.NuCorreo = @NuCorreo  

