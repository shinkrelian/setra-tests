﻿Create Procedure dbo.MACORREOSBIBLIAPROVEEDOR_Del
@IDProveedor char(6),
@Correo varchar(150)
As
Set NoCount On
Delete from MACORREOSBIBLIAPROVEEDOR
Where IDProveedor = @IDProveedor and Correo = @Correo
