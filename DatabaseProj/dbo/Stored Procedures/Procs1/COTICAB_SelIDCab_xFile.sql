﻿--HLF-20150317-And c.Estado<>'P' And isnull(IDFile,'')<>''
CREATE Procedure COTICAB_SelIDCab_xFile  

@IDFile char(8)  

As  

 Set NoCount On  

 select IDCAB,cl.RazonComercial,NroPax,c.IDCliente,Titulo
 ,(select MIN(Dia) from COTIDET cd where cd.IDCAB =c.IDCAB) As FechaIn
 ,(select MAX(Dia) from COTIDET cd where cd.IDCAB =c.IDCAB) As FechaOut
 from COTICAB c Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente  
 where IDFile = @IDFile  And c.Estado<>'P' And isnull(IDFile,'')<>''
