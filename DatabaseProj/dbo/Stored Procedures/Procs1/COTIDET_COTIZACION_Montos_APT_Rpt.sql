﻿--JHD-20150306- Se coloco ISNULL al campo de Liberados
--PPMG-20151117-Cambio del reporte
--JRF-20160406- Insertar Dato a los TMP
--JRF-20160407- Optimización
--JRF-20160409- Insertar Temporales
--JRF-20160411- Solo Diferencias (Grabado-Nuevo)
CREATE Procedure [dbo].[COTIDET_COTIZACION_Montos_APT_Rpt]       
@IDCab int,
@IDTemporada int
AS
BEGIN
	SET NOCOUNT ON
	SET LANGUAGE us_English
	DECLARE @FechaIniR smalldatetime, @FechaFinR smalldatetime
	DECLARE @Tb_Cab TABLE(IDCAB	int,
						  IDFile char(8),
						  Titulo varchar(100),
						  NroPax smallint,
						  NroLiberados	smallint,
						  FlIncluirTC bit,
						  [Simple] tinyint,
						  Twin tinyint,
						  Matrimonial tinyint,
						  Triple tinyint,
						  NombreTC varchar(80),
						  TD_DAYS int,
						  C_SWB int,
						  C_TWIN int,
						  C_MAT int,
						  C_TRIP int,
						  IDDebitMemo char(10))
	--DROP TABLE #Tb_Det
	--CREATE TABLE #Tb_Det(IDCAB	int,
	DECLARE @Tb_Det TABLE(IDCAB	int,
					      IDDet int,
						  Item	numeric,
						  sFecha	varchar(30),
  						  Dia	smalldatetime,
						  DiaRes	smalldatetime,
						  Ubigeo	varchar(60),
						  Servicio varchar(150),
						  IDTipoServ char(3),
						  IDTipoProv char(3),
						  sNumerAPT varchar(max),
						  NroPax_RD smallint,
						  NroLiberados_RD smallint,
						  Total_RD numeric(18,2),
						  Monto numeric(10,4),
						  Total numeric(18,2),
						  DescripcionAlterna varchar(200),
						  TipG tinyint)

	INSERT INTO @Tb_Cab
	(IDCAB, IDFile, Titulo, NroPax, NroLiberados, FlIncluirTC, [Simple], Twin, Matrimonial, Triple, NombreTC, TD_DAYS, C_SWB, C_TWIN, C_MAT, C_TRIP, IDDebitMemo)
	SELECT CC.IDCAB, CC.IDFile, CC.Titulo, ISNULL(CC.NroPax, 0) AS NroPax_CC, ISNULL(CC.NroLiberados, 0) AS NroLiberados_CC,
	CASE WHEN ISNULL(CTC.IDCab, 0) = 0 THEN 0 ELSE 1 END AS FlIncluirTC, ISNULL(CC.Simple, 0) AS Simple, 
	ISNULL(CC.Twin, 0) AS Twin, ISNULL(CC.Matrimonial, 0) AS Matrimonial, ISNULL(CC.Triple, 0) AS Triple, CP.NombreTC,
	CASE WHEN CTC.FeDesde IS NOT NULL AND CTC.FeHasta IS NOT NULL THEN DATEDIFF(DAY, CTC.FeDesde, CTC.FeHasta) + 1 ELSE 0 END AS TD_DAYS,
	AC.C_SWB, AC.C_TWIN, AC.C_MAT, AC.C_TRIP, ISNULL(DM.IDDebitMemo,'') AS IDDebitMemo
	FROM COTICAB AS CC LEFT OUTER JOIN
	(SELECT IDCAB, MAX(CASE WHEN CapacidadHab = 1 THEN NPHab ELSE 0 END) AS C_SWB,
				   MAX(CASE WHEN CapacidadHab = 2 AND EsMatrimonial = 0 THEN NPHab ELSE 0 END) AS C_TWIN, 
				   MAX(CASE WHEN CapacidadHab = 2 AND EsMatrimonial = 1 THEN NPHab ELSE 0 END) AS C_MAT,
				   MAX(CASE WHEN CapacidadHab = 3 THEN NPHab ELSE 0 END) AS C_TRIP
	 FROM (SELECT IDCAB, CapacidadHab, COUNT(CapacidadHab) AS NPHab, EsMatrimonial FROM 
			(SELECT IDCAB, CapacidadHab, IDHabit AS NPHab, EsMatrimonial FROM ACOMODOPAX_FILE
			 WHERE IDCAB = @IDCab GROUP BY IDCAB, CapacidadHab, IDHabit, EsMatrimonial) TX1
		  GROUP BY IDCAB, CapacidadHab, EsMatrimonial) AS AC1
	 GROUP BY IDCAB) AS AC ON CC.IDCAB = AC.IDCAB LEFT OUTER JOIN
	(SELECT IDCab, MAX(Apellidos + ' ' + Nombres) AS NombreTC FROM COTIPAX
	 WHERE (FlTC = 1) GROUP BY IDCab) AS CP ON CC.IDCAB = CP.IDCab LEFT OUTER JOIN COTICAB_TOURCONDUCTOR AS CTC ON CC.IDCAB = CTC.IDCab
	LEFT OUTER JOIN (SELECT D1.IDCab, D1.IDDebitMemo FROM DEBIT_MEMO D1 INNER JOIN
							(SELECT IDCab, MAX(Total) AS Total FROM DEBIT_MEMO D0 WHERE IDCAB = @IDCab GROUP BY IDCab) D2
					 ON D1.IDCab = D2.IDCab AND D1.Total = D2.Total) DM ON CC.IDCAB = DM.IDCab
	WHERE (CC.IDCAB = @IDCab)
	
	SELECT @FechaIniR = MIN(Dia), @FechaFinR = MAX(Dia) FROM COTIDET WHERE IDCAB = @IDCab

	--select * from RptInvoiceRangoAPT_Tmp
	--select * from RptInvoiceRangoAPT

	--Borrando/Insetando Datos Cabecera Temporales (RptInvoiceRangoAPT_Tmp) - Inicio
	Declare @NuRptInvoiceAPT int = 0,@VersionRpt tinyint = 0
	Select @VersionRpt = IsNull(Max(VersionRpt),0)+1 from RptInvoiceRangoAPT Where IDCAB=@IDCab And IDTemporada=@IDTemporada
	Delete from RptInvoiceRangoAPT_Det_Tmp Where IDCAB=@IDCab And VersionRpt=@VersionRpt
	Delete from RptInvoiceRangoAPT_Tmp Where IDCab=@IDCab And IDTemporada=@IDTemporada And VersionRpt=@VersionRpt
	if @NuRptInvoiceAPT = 0 
	Begin
		Select @NuRptInvoiceAPT = IsNull(Max(NuRptInvoiceAPT),0)+1 from RptInvoiceRangoAPT
		if Exists(select * from RptInvoiceRangoAPT_Tmp where NuRptInvoiceAPT=@NuRptInvoiceAPT)
		Begin
			Delete from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT
			Delete from RptInvoiceRangoAPT_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT		
		End
	
		Insert into RptInvoiceRangoAPT_Tmp  
		Select @NuRptInvoiceAPT,IDCAB,@IDTemporada, IDFile, Titulo, NroPax, NroLiberados, FlIncluirTC, Simple, Twin, Matrimonial, Triple, NombreTC, TD_DAYS, C_SWB, 
		C_TWIN, C_MAT, C_TRIP, IDDebitMemo,@VersionRpt,1,0
		from @Tb_Cab
	End
	--Insetando Datos Temporales (RptInvoiceRangoAPT_Tmp) - Fin

	--select 'NUEVO',* from RptInvoiceRangoAPT_Tmp
	Declare @NuRptInvoiceAPTOld int = 0
	--Actualizando Cambios
	if Exists(select NuRptInvoiceAPT from RptInvoiceRangoAPT Where IDCAB=@IDCab And IDTemporada=@IDTemporada)
	Begin
		select Top(1) @NuRptInvoiceAPTOld=NuRptInvoiceAPT 
		from RptInvoiceRangoAPT Where IDCAB=@IDCab And IDTemporada=@IDTemporada Order By VersionRpt Desc

		--Actualizando el código relacionado
		Update RptInvoiceRangoAPT_Tmp Set NuRptInvoiceAPTAntiguo = @NuRptInvoiceAPTOld Where NuRptInvoiceAPT=@NuRptInvoiceAPT	
		
		--Actualizando la diferencia de Campos
		Update RptInvoiceRangoAPT_Tmp
			Set NroPax = old.NroPax-New.NroPax,
				NroLiberados = old.NroLiberados-new.NroLiberados,
				Simple = old.Simple-new.Simple,
				Twin = old.Twin-new.Twin,
				Matrimonial = old.Matrimonial-new.Matrimonial,
				Triple = old.Triple-new.Triple,
				TD_DAYS = old.TD_DAYS-new.TD_DAYS,
				C_SWB = old.C_SWB-new.C_SWB,
				C_TWIN = old.C_TWIN-new.C_TWIN,
				C_MAT = old.C_MAT-new.C_MAT,
				C_TRIP = old.C_TRIP-new.C_TRIP
		From RptInvoiceRangoAPT_Tmp New Inner Join 
		(select * from RptInvoiceRangoAPT_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPTOld) Old On New.NuRptInvoiceAPTAntiguo=Old.NuRptInvoiceAPT
		Where new.NuRptInvoiceAPT=@NuRptInvoiceAPT
	End
	--select 'ACTUALIZADO',* from RptInvoiceRangoAPT_Tmp

	INSERT INTO @Tb_Det
	(IDCAB,IDDet, Item, sFecha, Dia, DiaRes, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total_RD, Monto, Total, DescripcionAlterna, TipG, IDTipoProv)
	SELECT IDCAB,IDDet, Item, sFecha, Dia, DiaRes, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total, 
	CASE WHEN (APTServicoOpcional IN ('F','E')) OR (FlRelacion = 1) THEN 0 ELSE 
		CASE WHEN CapacidadHab = 1 AND EsMatrimonial = 0 THEN MontoRango + SingleSupplement ELSE MontoRango END END AS Monto,
	CASE WHEN (APTServicoOpcional IN ('F','E')) OR (FlRelacion = 1) THEN 0 ELSE NroPax_RD * 
		CASE WHEN CapacidadHab = 1 AND EsMatrimonial = 0 THEN MontoRango + SingleSupplement ELSE MontoRango END END AS Total, DescripcionAlterna, 1, IDTipoProv
	FROM (
	SELECT CC.IDCAB,CD.IDDET, CD.Item, UPPER(substring(Datename(dw,RD.FechaR),1,3))+' '+ Convert(varchar(60),RD.FechaR,3) as sFecha, RD.Dia, RD.FechaR AS DiaRes, UB.Descripcion AS Ubigeo,
	CASE WHEN (PS.IDTipoProv = '001' OR PS.IDTipoProv = '002') THEN 
		PS.NombreCorto + ' (' + RD.Servicio + ')' ELSE RD.Servicio END + CASE WHEN CD.IncGuia = 1 THEN ' - Guide' ELSE '' END AS Servicio, 
	(CASE WHEN PS.IDTipoServ = 'NAP' THEN '---' ELSE PS.IDTipoServ END) AS IDTipoServ
	, CASE WHEN CD.NroPax <> CC.NroPax THEN [dbo].[FnNumberAPT_RESERVAS_DET_PAX](CC.IDCAB, RD.IDReserva_Det) ELSE '' END AS sNumerAPT
	, (CASE WHEN RD.CapacidadHab = 0 THEN RD.NroPax
			WHEN RD.CapacidadHab = 1 THEN 
				CASE WHEN RD.CantidadAPagar < RD.Cantidad THEN RD.CantidadAPagar
					 ELSE (RD.CantidadAPagar * RD.CapacidadHab) - [dbo].[FnLiberadoAPT](PoliticaLiberado, Liberado, MaximoLiberado, CapacidadHab, EsMatrimonial, CD.NroPax + CD.NroLiberados)
						  END ELSE (RD.CantidadAPagar * RD.CapacidadHab) END) AS NroPax_RD--, RD.NroPax AS NroPax_RD
	, RD.NroLiberados AS NroLiberados_RD
	, RD.Total, CASE WHEN EXISTS(SELECT CDR.Total FROM COTIDET CDR WHERE CDR.IDDetRel = CD.IDDET AND CDR.Dia = RD.FechaR) THEN 1 ELSE 0 END AS FlRelacion
	, TR1.Monto AS MontoRango
	, TR1.DescripcionAlterna, PS.APTServicoOpcional, PS.IDTipoProv, RD.CapacidadHab, ISNULL(TR1.SingleSupplement,0) AS SingleSupplement, RD.EsMatrimonial
	FROM COTICAB AS CC INNER JOIN COTIDET AS CD ON CC.IDCAB = CD.IDCAB INNER JOIN
	(SELECT @IDCab AS IDCab, RDIF.IDDet, RDIF.Dia, RDIF.FechaOut, RDIF.Servicio, RDIF.IDServicio, RDIF.IDServicio_Det, RDIF.NroPax,
	 RDIF.NroLiberados, RDIF.Total, RDIF.CapacidadHab, F.FechaR, RDIF.IDReserva_Det, RDIF.EsMatrimonial, RDIF.CantidadAPagar, RDIF.Cantidad
		FROM (
			SELECT RDF.IDDet, RDF.Dia, CASE WHEN RDF.FechaOut IS NULL THEN RDF.Dia ELSE RDF.FechaOut END AS FechaOut,
			RDF.Servicio,RDF.IDServicio, RDF.IDServicio_Det, RDF.NroPax, RDF.NroLiberados, RDF.Total, RDF.CapacidadHab, RDF.IDReserva_Det
			, RDF.EsMatrimonial, RDF.CantidadAPagar, RDF.Cantidad
			FROM COTICAB AS CF INNER JOIN COTIDET AS DF ON CF.IDCAB = DF.IDCAB INNER JOIN RESERVAS_DET AS RDF ON DF.IDDET = RDF.IDDet
			WHERE CF.IDCAB = @IDCab AND RDF.Anulado = 0 AND RDF.FechaOut IS NOT NULL
			) RDIF LEFT OUTER JOIN dbo.FnTableRangoFechas(@FechaIniR,@FechaFinR) F ON F.FechaR >= RDIF.Dia AND F.FechaR < RDIF.FechaOut
			WHERE F.FechaR IS NOT NULL
	 UNION ALL
	 SELECT @IDCab AS IDCab, RDIF.IDDet, RDIF.Dia, RDIF.FechaOut, RDIF.Servicio, RDIF.IDServicio, RDIF.IDServicio_Det, RDIF.NroPax,
	 RDIF.NroLiberados, RDIF.Total, RDIF.CapacidadHab, F.FechaR, RDIF.IDReserva_Det, RDIF.EsMatrimonial, RDIF.CantidadAPagar, RDIF.Cantidad
		FROM (
			SELECT RDF.IDDet, RDF.Dia, CASE WHEN RDF.FechaOut IS NULL THEN RDF.Dia ELSE RDF.FechaOut END AS FechaOut,
			RDF.Servicio,RDF.IDServicio, RDF.IDServicio_Det, RDF.NroPax, RDF.NroLiberados, RDF.Total, RDF.CapacidadHab, RDF.IDReserva_Det
			, RDF.EsMatrimonial, RDF.CantidadAPagar, RDF.Cantidad
			FROM COTICAB AS CF INNER JOIN COTIDET AS DF ON CF.IDCAB = DF.IDCAB INNER JOIN RESERVAS_DET AS RDF ON DF.IDDET = RDF.IDDet
			WHERE CF.IDCAB = @IDCab AND RDF.Anulado = 0 AND RDF.FechaOut IS NULL
			) RDIF LEFT OUTER JOIN dbo.FnTableRangoFechas(@FechaIniR,@FechaFinR) F ON RDIF.Dia BETWEEN F.FechaR AND F.FechaRF
	) AS RD ON CD.IDCAB = RD.IDCab AND CD.IDDET = RD.IDDet AND CD.IDServicio = RD.IDServicio AND CD.IDServicio_Det = RD.IDServicio_Det
	--AND CD.Dia = RD.Dia
	--INNER JOIN (SELECT IDProveedor, NombreCorto, IDTipoProv FROM MAPROVEEDORES WHERE IDTipoProv IN ('001','003')) AS PR ON CD.IDProveedor = PR.IDProveedor
	--INNER JOIN MASERVICIOS AS SR ON CD.IDServicio = SR.IDServicio
	INNER JOIN (SELECT MP1.IDProveedor, MP1.NombreCorto, MP1.IDTipoProv, MS1.IDServicio, MS1.IDTipoServ, MS1.APTServicoOpcional, MD1.IDServicio_Det, MD1.ConAlojamiento
	 FROM MAPROVEEDORES AS MP1 INNER JOIN MASERVICIOS AS MS1 ON MP1.IDProveedor = MS1.IDProveedor INNER JOIN MASERVICIOS_DET AS MD1 ON MS1.IDServicio = MD1.IDServicio
	 WHERE (MP1.IDTipoProv = '001') OR ((MP1.IDTipoProv = '003') AND (MD1.ConAlojamiento = 1))) AS PS
	ON CD.IDProveedor = PS.IDProveedor AND CD.IDServicio = PS.IDServicio AND CD.IDServicio_Det = PS.IDServicio_Det
	LEFT OUTER JOIN MAUBIGEO AS UB	ON CD.IDubigeo = UB.IDubigeo
	INNER JOIN (SELECT S2.IDServicio, M2.IDServicio_Det, M2.IDTemporada, T2.NombreTemporada, ISNULL(M2.DescripcionAlterna, '') AS DescripcionAlterna,
				D2.PaxDesde, D2.PaxHasta, D2.Monto, ISNULL(M2.SingleSupplement,0.00) AS SingleSupplement
				, ISNULL(M2.PoliticaLiberado,0) AS PoliticaLiberado, ISNULL(M2.Liberado,0) AS Liberado, ISNULL(M2.MaximoLiberado,0) AS MaximoLiberado
				FROM MATEMPORADA AS T2 INNER JOIN MASERVICIOS_DET_DESC_APT AS M2 ON T2.IDTemporada = M2.IDTemporada INNER JOIN
				MASERVICIOS_DET_RANGO_APT AS D2 ON M2.IDServicio_Det = D2.IDServicio_Det AND M2.IDTemporada = D2.IDTemporada INNER JOIN
				MASERVICIOS_DET AS S2 ON M2.IDServicio_Det = S2.IDServicio_Det
				WHERE T2.IDTemporada = @IDTemporada) TR1 ON RD.IDServicio = TR1.IDServicio AND RD.IDServicio_Det = TR1.IDServicio_Det AND
				CD.NroPax BETWEEN TR1.PaxDesde AND TR1.PaxHasta
	WHERE (CC.IDCAB = @IDCab)

	UNION ALL
	
	SELECT TC.IDCAB, TC.IDDET, TC.Item, UPPER(substring(Datename(dw,TC.Dia),1,3))+' '+ Convert(varchar(60),TC.Dia,3) as sFecha, TC.Dia, TC.RD_Dia AS DiaRes, UB.Descripcion AS Ubigeo,
	CASE WHEN (TC.IDTipoProv = '001' OR TC.IDTipoProv = '002') THEN TC.NombreCorto + ' (' + SR.Descripcion + ')' ELSE SR.Descripcion END + 
		CASE WHEN TC.IncGuia = 1 THEN ' - Guide' ELSE '' END AS Servicio, 
	(CASE WHEN SR.IDTipoServ = 'NAP' THEN '---' ELSE SR.IDTipoServ END) AS IDTipoServ
	, sNumerAPT
	, TC.RD_NroPax AS NroPax_RD, TC.NroLiberados AS NroLiberados_RD
	, TC.Total, FlRelacion, TR2.Monto AS MontoRango, TR2.DescripcionAlterna, SR.APTServicoOpcional, TC.IDTipoProv, TC.CapacidadHab, 0.00 AS SingleSupplement, TC.EsMatrimonial
	FROM
	(SELECT CC.IDCAB, CD.IDDET ,CD.Item, CD.Dia, MIN(RD.Dia) AS RD_Dia, CD.IncGuia, CD.NroPax AS CD_NroPax, CC.NroPax AS CC_NroPax,
			RD.NroPax AS RD_NroPax, RD.NroLiberados, SUM(RD.Total) AS Total, RD.CapacidadHab, RD.EsMatrimonial, RD.IDServicio
			, PS.IDProveedor, PS.NombreCorto, PS.IDTipoProv, CD.IDubigeo
			, CASE WHEN CD.NroPax <> CC.NroPax THEN [dbo].[FnNumberAPT_RESERVAS_DET_PAX](CC.IDCAB, RD.IDReserva_Det) ELSE '' END AS sNumerAPT
			, CASE WHEN EXISTS(SELECT CDR.Total FROM COTIDET CDR WHERE CDR.IDDET = CD.IDDetRel) THEN 1 ELSE 0 END AS FlRelacion
	 FROM COTICAB AS CC INNER JOIN COTIDET AS CD ON CC.IDCAB = CD.IDCAB INNER JOIN RESERVAS_DET AS RD ON CD.IDDET = RD.IDDet
	 --INNER JOIN (SELECT IDProveedor, NombreCorto, IDTipoProv FROM MAPROVEEDORES WHERE IDTipoProv <> '001') PR ON CD.IDProveedor = PR.IDProveedor
	 INNER JOIN (SELECT MP1.IDProveedor, MP1.NombreCorto, MP1.IDTipoProv, MS1.IDServicio, MD1.IDServicio_Det, MD1.ConAlojamiento
	 FROM MAPROVEEDORES AS MP1 INNER JOIN MASERVICIOS AS MS1 ON MP1.IDProveedor = MS1.IDProveedor INNER JOIN MASERVICIOS_DET AS MD1 ON MS1.IDServicio = MD1.IDServicio
	 WHERE (MP1.IDTipoProv <> '001' AND MP1.IDTipoProv <> '003') OR ((MP1.IDTipoProv = '003') AND (MD1.ConAlojamiento = 0))) AS PS
	 ON CD.IDProveedor = PS.IDProveedor AND CD.IDServicio = PS.IDServicio AND CD.IDServicio_Det = PS.IDServicio_Det
	WHERE (CC.IDCAB = @IDCab) AND RD.Anulado = 0
	GROUP BY CC.IDCAB,cd.IDDET, CD.Item, CD.Dia, CD.IncGuia, CD.NroPax, CC.NroPax, RD.NroPax,
			RD.NroLiberados, RD.CapacidadHab, RD.EsMatrimonial, RD.IDServicio
			, PS.IDProveedor, PS.NombreCorto, PS.IDTipoProv, CD.IDubigeo
			, CASE WHEN CD.NroPax <> CC.NroPax THEN [dbo].[FnNumberAPT_RESERVAS_DET_PAX](CC.IDCAB, RD.IDReserva_Det) ELSE '' END
			, CD.IDDetRel
	) TC
	INNER JOIN MASERVICIOS AS SR ON TC.IDServicio = SR.IDServicio LEFT OUTER JOIN MAUBIGEO AS UB ON TC.IDubigeo = UB.IDubigeo
	INNER JOIN (SELECT M1.IDServicio, M1.IDTemporada, T1.NombreTemporada, ISNULL(M1.DescripcionAlterna,'') AS DescripcionAlterna, D1.PaxDesde, D1.PaxHasta, D1.Monto
				FROM MASERVICIOS_DESC_APT AS M1 INNER JOIN MASERVICIOS_RANGO_APT AS D1 ON M1.IDServicio = D1.IDServicio
				AND M1.IDTemporada = D1.IDTemporada INNER JOIN MATEMPORADA AS T1 ON M1.IDTemporada = T1.IDTemporada
				WHERE T1.IDTemporada = @IDTemporada AND M1.AgruparServicio = 0) TR2 ON TC.IDServicio = TR2.IDServicio AND
				TC.CD_NroPax BETWEEN TR2.PaxDesde AND TR2.PaxHasta
	) T
	ORDER BY Dia

	INSERT INTO @Tb_Det
	(IDCAB, IDDet,Item, sFecha, Dia, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total_RD, Monto, Total, DescripcionAlterna, TipG, IDTipoProv)
	SELECT CV.IDCAB,0 As IDDet, ROW_NUMBER() OVER(ORDER BY CV.ID ASC) AS Item, NULL AS sFecha, NULL AS Dia, NULL AS Ubigeo,
		VO.Descripcion + ' / ' + VD.Descripcion + ' - ' + PV.NombreCorto AS Ruta, NULL AS IDTipoServ, NULL AS sNumerAPT, 
		NV.NPAX, NULL AS NroLiberados_RD, NULL AS Total_RD, CV.Tarifa, NV.NPAX * CV.Tarifa AS Total, NULL AS DescripcionAlterna, 2 AS TipG, NULL AS IDTipoProv
	FROM COTIVUELOS AS CV INNER JOIN MAUBIGEO AS VO ON CV.RutaO = VO.IDubigeo INNER JOIN
	MAUBIGEO AS VD ON CV.RutaD = VD.IDubigeo INNER JOIN MAPROVEEDORES AS PV ON CV.IdLineaA = PV.IDProveedor INNER JOIN
	(SELECT IDTransporte, COUNT(IDTransporte) AS NPAX
	 FROM COTITRANSP_PAX
	 GROUP BY IDTransporte) AS NV ON CV.ID = NV.IDTransporte
	 WHERE (CV.IDCAB = @IDCab) AND (CV.TipoTransporte = 'V') AND (CV.EmitidoSetours = 1)

	INSERT INTO @Tb_Det
	(IDCAB, IDDet , Item, sFecha, Dia, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total_RD, Monto, Total, DescripcionAlterna, TipG, IDTipoProv)
	SELECT IDCab, 0 As IDDet,ITEM, NULL AS sFecha, NULL AS Dia, NULL AS Ubigeo, Descripcion, NULL AS IDTipoServ, NULL AS sNumerAPT, CantD, NULL AS NroLiberados_RD,
	NULL AS Total_RD, SsHonorario, Costo, NULL AS DescripcionAlterna, 4 AS TipG, NULL AS IDTipoProv
	FROM (
	SELECT IDCab, 1 AS ITEM, 'TD Salary - per day' AS Descripcion,
	CASE WHEN CTC.CoTipoHon = '002' THEN 170.00
	ELSE
		CASE WHEN CTC.FeDesde IS NOT NULL AND CTC.FeHasta IS NOT NULL THEN ISNULL(CTC.SsHonorario,0)/(DATEDIFF(DAY, CTC.FeDesde, CTC.FeHasta) + 1) ELSE 0 END
	END AS SsHonorario,
	CASE WHEN CTC.FeDesde IS NOT NULL AND CTC.FeHasta IS NOT NULL THEN DATEDIFF(DAY, CTC.FeDesde, CTC.FeHasta) + 1 ELSE 0 END AS CantD,
	CASE WHEN CTC.CoTipoHon = '002' THEN
		170.00 * (CASE WHEN CTC.FeDesde IS NOT NULL AND CTC.FeHasta IS NOT NULL THEN DATEDIFF(DAY, CTC.FeDesde, CTC.FeHasta) + 1 ELSE 0 END)
	ELSE
		CASE WHEN CTC.FeDesde IS NOT NULL AND CTC.FeHasta IS NOT NULL THEN 
			ISNULL(CTC.SsHonorario,0)/(DATEDIFF(DAY, CTC.FeDesde, CTC.FeHasta) + 1) 
		ELSE 0 END * (1+(ISNULL(CTC.PoMargen,0)/100))
	END AS Costo
	FROM COTICAB_TOURCONDUCTOR CTC where IDCAB = @IDCab
	UNION
	SELECT IDCab, 2 AS ITEM, 'meals not included in tour' AS Descripcion,
	ISNULL(CostoUSD,0) AS UCosto, ISNULL(COUNT(Item),0) AS NCant,
	ISNULL(SUM(ISNULL(CostoUSD,0)),0) AS Costo
	FROM VIATICOS_TOURCONDUCTOR where IDCAB = @IDCab AND FlActivo = 1
	GROUP BY IDCab, ISNULL(CostoUSD,0)
	UNION
	SELECT IDCab, 3 AS ITEM, 'Communication - per tour' AS Descripcion, NULL AS SsHonorario, NULL AS CantD, SsTarjetaTelef AS Costo
	FROM COTICAB_TOURCONDUCTOR CTC where IDCAB = @IDCab
	UNION
	SELECT IDCab, 4 AS ITEM, 'TD Enhancement - per tour' AS Descripcion, NULL AS SsHonorario, NULL AS CantD, SsTaxi AS Costo
	FROM COTICAB_TOURCONDUCTOR CTC where IDCAB = @IDCab
	) T

	INSERT INTO @Tb_Det
	(IDCAB, IDDet ,Item, sFecha, Dia, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total_RD, Monto, Total, DescripcionAlterna, TipG, IDTipoProv)
	SELECT IDCAB, IDDet, ROW_NUMBER() OVER(ORDER BY Servicio ASC) AS Item, NULL AS sFecha, NULL AS Dia, 
		   NULL AS Ubigeo, Servicio, NULL AS IDTipoServ, NUll AS sNumerAPT, NroPax_RD, NroLiberados_RD	, 
		   Total, MontoRango, NroPax_RD * MontoRango, NULL AS DescripcionAlterna, 3 AS TipG, NULL AS IDTipoProv
	FROM (
		SELECT CC.IDCAB,0 as IDDet, CASE WHEN ISNULL(TR2.DescripcionAlterna,'') = '' THEN RD.Servicio ELSE TR2.DescripcionAlterna END AS Servicio, 
		SUM(RD.NroPax) AS NroPax_RD, SUM(RD.NroLiberados) AS NroLiberados_RD, SUM(RD.Total) AS TotalRD, TR2.Monto AS MontoRango, SUM(RD.NroPax * TR2.Monto) AS Total
		FROM COTICAB AS CC INNER JOIN COTIDET AS CD ON CC.IDCAB = CD.IDCAB INNER JOIN
		RESERVAS_DET AS RD ON CD.IDDET = RD.IDDet INNER JOIN (SELECT IDProveedor, NombreCorto, IDTipoProv FROM MAPROVEEDORES WHERE IDTipoProv <> '001') AS PR
		ON CD.IDProveedor = PR.IDProveedor INNER JOIN MASERVICIOS AS SR ON CD.IDServicio = SR.IDServicio LEFT OUTER JOIN MAUBIGEO AS UB	ON CD.IDubigeo = UB.IDubigeo
		INNER JOIN (SELECT M1.IDServicio, M1.IDTemporada, T1.NombreTemporada, ISNULL(M1.DescripcionAlterna,'') AS DescripcionAlterna, D1.PaxDesde, D1.PaxHasta, D1.Monto
					FROM MASERVICIOS_DESC_APT AS M1 INNER JOIN MASERVICIOS_RANGO_APT AS D1 ON M1.IDServicio = D1.IDServicio
					AND M1.IDTemporada = D1.IDTemporada INNER JOIN MATEMPORADA AS T1 ON M1.IDTemporada = T1.IDTemporada
					WHERE T1.IDTemporada = @IDTemporada AND M1.AgruparServicio = 1) TR2 ON RD.IDServicio = TR2.IDServicio AND
					CD.NroPax BETWEEN TR2.PaxDesde AND TR2.PaxHasta
		WHERE (CC.IDCAB = @IDCab) AND RD.Anulado = 0
		GROUP BY CC.IDCAB, CASE WHEN ISNULL(TR2.DescripcionAlterna,'') = '' THEN RD.Servicio ELSE TR2.DescripcionAlterna END, 
		TR2.Monto
	) T

	--Insertando Detalles Temp (Inicio)
	if @NuRptInvoiceAPT > 0
	Begin
	Declare @MaximoDet int = 0
	Select @MaximoDet=IsNull(Max(NuRptInvoiceDetPT),0) from RptInvoiceRangoAPT_Det
	Select @MaximoDet= Case When IsNull(Max(NuRptInvoiceDetPT),0) > @MaximoDet then IsNull(Max(NuRptInvoiceDetPT),0) Else @MaximoDet End from RptInvoiceRangoAPT_Det_Tmp

	Insert into RptInvoiceRangoAPT_Det_Tmp
	Select @MaximoDet + ROW_NUMBER()Over(ORDER BY TipG, DiaRes, Dia, Item) As NuDet,
		   @NuRptInvoiceAPT,IDCAB,IDDet,ROW_NUMBER()Over(Partition By IDDet ORDER BY TipG, DiaRes, Dia, Item),
		   Item,sFecha,Dia,DiaRes,Ubigeo,Servicio,IDTipoServ,IDTipoProv,sNumerAPT,NroPax_RD,NroLiberados_RD,
		   Total_RD,Monto,Total,DescripcionAlterna,TipG,@VersionRpt,0,0
	from @Tb_Det
	End
	--select VersionRpt,* from RptInvoiceRangoAPT_Tmp
	--select VersionRpt,* from RptInvoiceRangoAPT_Det_Tmp
	--Insertando Detalles Temp (Fin)
	--select * from RptInvoiceRangoAPT_Det_Tmp
	--Select 'NUEVO',NuRptInvoiceDetPTAntiguo,* from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT
	If @NuRptInvoiceAPTOld > 0
	Begin		

		--Select 'ANTIGUO',* from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPTOld		
		Update RptInvoiceRangoAPT_Det_Tmp 
			Set NuRptInvoiceDetPTAntiguo= 
				Case TipG 
					When 1 Then (Select NuRptInvoiceDetPT from RptInvoiceRangoAPT_Det_Tmp 
								 Where NuRptInvoiceAPT=@NuRptInvoiceAPTOld And IDDet=tmpNew.IDDet And NroOrdenIDDet=tmpNew.NroOrdenIDDet And TipG=tmpNew.TipG)
					Else (Select NuRptInvoiceDetPT from RptInvoiceRangoAPT_Det_Tmp 
						  Where NuRptInvoiceAPT=@NuRptInvoiceAPTOld And NroOrdenIDDet=tmpNew.NroOrdenIDDet And TipG=tmpNew.TipG)
				End
		From RptInvoiceRangoAPT_Det_Tmp tmpNew
		Where tmpNew.NuRptInvoiceAPT=@NuRptInvoiceAPT
		--Select 'ACTUAL',* from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT
		
		Update RptInvoiceRangoAPT_Det_Tmp 
			Set NroPax_RD = old.NroPax_RD-new.NroPax_RD,
				NroLiberados_RD = old.NroLiberados_RD-new.NroLiberados_RD,
				Total_RD= old.Total_RD-new.Total_RD,
				Monto = old.Monto-new.Monto,
				Total = old.Total-new.Total,
				ChangedDet = Case When old.NroPax_RD<>new.NroPax_RD Or old.NroLiberados_RD<>new.NroLiberados_RD Or old.Total_RD<>new.Total_RD Or
									   old.Monto<>new.Monto Or old.Total<>new.Total
							 Then 1 Else 0 End
		From RptInvoiceRangoAPT_Det_Tmp New Inner Join 
		(select * from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPTOld) Old On New.NuRptInvoiceDetPTAntiguo =Old.NuRptInvoiceDetPT
		Where New.NuRptInvoiceAPT=@NuRptInvoiceAPT
	End
	--Select 'ACTUALIZADO',NuRptInvoiceDetPTAntiguo,* from RptInvoiceRangoAPT_Det_Tmp Where NuRptInvoiceAPT=@NuRptInvoiceAPT

	SET NOCOUNT OFF
	if Exists(select NuRptInvoiceAPT from RptInvoiceRangoAPT Where IDCAB=@IDCab And IDTemporada=@IDTemporada)
	Begin
		Select *
		from (select Top(1) * from RptInvoiceRangoAPT_Tmp Where IDCAB=@IDCab And IDTemporada=@IDTemporada Order By VersionRpt Desc) cab
		Left Outer Join RptInvoiceRangoAPT_Det_Tmp det On cab.NuRptInvoiceAPT=det.NuRptInvoiceAPT
		Where cab.Changed=1 And det.ChangedDet=1
		Order By det.TipG, det.DiaRes, det.Dia, det.Item
	End
	else
	Begin
		SELECT C.IDCAB,IDDet, IDFile, Titulo, NroPax, NroLiberados, FlIncluirTC, [Simple], Twin, Matrimonial, Triple, NombreTC, TD_DAYS, C_SWB, C_TWIN, C_MAT, C_TRIP
		, IDDebitMemo, Item, sFecha, Dia, DiaRes, Ubigeo, Servicio, IDTipoServ, sNumerAPT, NroPax_RD, NroLiberados_RD, Total_RD, Monto, Total, DescripcionAlterna, TipG, IDTipoProv
		FROM @Tb_Cab C LEFT OUTER JOIN @Tb_Det D ON C.IDCAB = D.IDCAB
		ORDER BY D.TipG, D.DiaRes, D.Dia, Item
	End

END
