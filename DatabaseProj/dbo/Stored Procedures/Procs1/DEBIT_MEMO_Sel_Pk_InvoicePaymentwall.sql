﻿
CREATE PROCEDURE [dbo].[DEBIT_MEMO_Sel_Pk_InvoicePaymentwall]	
	@IDDebitMemo char(10)
AS
BEGIN
	Set NoCount On
	
	SELECT IDDebitMemo AS invoice_number,
		   dbo.FnEquivalenciaMoneda_ISO_4217(IDMoneda) AS currency,
		   CONVERT(varchar,Fecha,103) AS [date],
		   CONVERT(varchar,FecVencim,103) AS due_date
	FROM DEBIT_MEMO WHERE IDDebitMemo = @IDDebitMemo
END
