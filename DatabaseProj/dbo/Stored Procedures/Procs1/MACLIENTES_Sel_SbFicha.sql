﻿create procedure [dbo].[MACLIENTES_Sel_SbFicha]
@IDCliente varchar(6)
as

	Set NoCount On
	
	select NroFila,IDCLiente,Nombre_Completo,Cargo,Celular,Email,Numero
	from(
	select ROW_NUMBER() over (order by Nombre_Completo) as NroFila ,y.*
	from (
	select cc.IDCliente,CAST(Titulo+' '+Nombres+' '+Apellidos as varchar) as Nombre_Completo,
	     Cargo,cc.Celular,cc.Email,
	     Numero=CAST(cc.Anexo+'-'+cc.Telefono as varchar)
	from MACONTACTOSCLIENTE cc 
	Where cc.IDCliente In (select c.IDCliente from MACLIENTES c
	Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo
	Left Join MAUBIGEO up On u.IDPais=up.IDubigeo
	Left Join MACONTACTOSCLIENTE cc On c.IDCliente=cc.IDCliente 
	--Left join MAMONEDAS mo On mo.IDMoneda=c.IDMoneda 
	where cc.IDCliente=@IDCliente
	)
	) as y
	) as x
