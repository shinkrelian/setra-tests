﻿
-------------------------------------------------------------------------------------------------------------


--DEBIT_MEMO_RepPeriodoPromedioCobro '','','01/01/2015','03/02/2015'
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JHD-20150203- No considerar cliente APT: AND CLI.IDCliente<>'000054'
--JHD-20150203- DATEDIFF(D,dm.Fecha,dm.FecVencim) AS NroDiasCredito,
CREATE Procedure dbo.DEBIT_MEMO_RepPeriodoPromedioCobro
@IDCliente varchar(6),  
@IDPais varchar(6),  
@FecVencimientoIn smalldatetime,  
@FecVencimientoOut smalldatetime  
As  
Set NoCount On  
  
Select CLi.RazonComercial as DescCliente,  
SUBSTRING(dm.IDDebitMemo,1,8)+' - '+ SUBSTRING(dm.IDDebitMemo,9,10) As IDDebitMemo,  
dm.Titulo as Referencia,c.FecInicio,c.FechaOut,dm.Total as ImporteUSD,dm.Saldo,dm.Fecha as FechaCobro,dm.FecVencim,  
(select Top(1) ifz.FeAcreditada from INGRESO_FINANZAS ifz   
 where ifz.NuIngreso In(select NuIngreso from INGRESO_DEBIT_MEMO idm2 where idm2.IDDebitMemo=dm.IDDebitMemo)  
 Order by ifz.FeAcreditada asc) as FechaPago,  
DATEDIFF(D,dm.FecVencim,(select Top(1) ifz.FeAcreditada from INGRESO_FINANZAS ifz   
 where ifz.NuIngreso In(select NuIngreso from INGRESO_DEBIT_MEMO idm2 where idm2.IDDebitMemo=dm.IDDebitMemo)  
 Order by ifz.FeAcreditada asc)) AS NroDiasDemora,  
 DATEDIFF(D,dm.Fecha,dm.FecVencim) AS NroDiasCredito,
ub.Descripcion as DescPais  
from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab=c.IDCAB  
Left Join MACLIENTES CLi on dm.IDCliente=CLi.IDCliente  
Left Join MAUBIGEO ub On CLi.IDCiudad=ub.IDubigeo  
where IDTipo= 'I' And IDEstado='PG' --In ('PP','PD')  
and (Ltrim(rtrim(@IDCliente))='' Or dm.IDCliente=@IDCliente)  
and (Ltrim(rtrim(@IDPais))='' Or CLi.IDCiudad=@IDPais)  
and (Ltrim(rtrim(Convert(Char(10),@FecVencimientoIn,103)))='01/01/1900' Or   
  dm.FecVencim between @FecVencimientoIn and @FecVencimientoOut)  
 AND CLI.IDCliente<>'000054'
Order by dm.FecVencim,DescCliente  

