﻿--JHD-20150429 - and (cp.Titulo <>'Chd.' or (cp.Titulo ='Chd.' and cp.FlAdultoINC=1))
--JHD-20150504- If @vcMPWP = 'RAQCHI' Set @IDServicioMPWP='CUZ00145'
--JHD-20150504- And ((cp.FlEntMP=1 And @vcMPWP = 'MAPI') Or (cp.FlEntWP=1 And @vcMPWP = 'WAYNA') OR  (@vcMPWP = 'RAQCHI'))
--JHD-20150504-	And ((cdp.FlEntMPGenerada=0 And @vcMPWP = 'MAPI') Or (cdp.FlEntWPGenerada=0 And @vcMPWP = 'WAYNA') OR  (@vcMPWP = 'RAQCHI'))
--JHD-20150504-And (cp.FlAutorizarMPWP=1 or @vcMPWP = 'RAQCHI')
--JHD-20150504- And ((@vcMPWP = 'MAPI') Or ( @vcMPWP = 'WAYNA') OR  (@vcMPWP = 'RAQCHI'))
--JHD-20150708- And dbo.FnServicioMPWP(cd.IDServicio_Det) IN(@IDServicioMPWP,'CUZ00133','CUZ00905')
CREATE Procedure dbo.COTIPAX_Sel_MPWP
	@IDCab int,
	@vcMPWP varchar(10)
As
	Set Nocount On

	Declare @IDServicioMPWP char(8)	

	If @vcMPWP = 'MAPI'
		Set @IDServicioMPWP='CUZ00132'
	If @vcMPWP = 'WAYNA'
		Set @IDServicioMPWP='CUZ00134'
	If @vcMPWP = 'RAQCHI'
		Set @IDServicioMPWP='CUZ00145'

	Select X.Apellidos,X.Nombre, X.TipoDoc, X.NroDoc, X.IDPais, X.Sexo, X.Edad, X.Categoria,     
	case When Edad < 18   THEN '2' Else '1' End as Tipo, IDDET, IDPax
	From(        
	select Apellidos,Nombres as Nombre,

	case when ti.IDIdentidad In ('004','003','002') then '1' else ti.Descripcion End as TipoDoc,         
	cp.NumIdentidad NroDoc,ISNULL(u.CodigoINC,'') as IDPais        
	, case upper(cp.Titulo) When 'MR.' then 'M' When 'MS.' Then 'F' End as Sexo,        
	ISNULL(DATEDIFF(YEAR,FecNacimiento,getdate()),0) as Edad,        
	case CP.IDIdentidad
	when '004' Then 1    --PASAPORTE
	When '002' then 2    --DNI
	When '003' then 4    --CARNET EXTRANJERIA
	When '006' then 6      --CARNET UNIVERSITARIO
	End as Categoria ,Orden, cd.IDDet, CP.IDPax
	from COTIPAX cp Left Join MATIPOIDENT ti On cp.IDIdentidad= ti.IDIdentidad           
	Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo           

	Inner Join COTIDET_PAX cdp On cdp.IDPax=cp.IDPax 
	Inner Join COTIDET cd On cdp.IDDet=cd.IDDet 
	
	--Inner Join MASERVICIOS_DET sd1 On cd.IDServicio_Det=sd1.IDServicio_Det
	--				Inner Join MASERVICIOS_DET sd2 On sd1.IDServicio_Det_V=sd2.IDServicio_Det
	--					And sd2.IDServicio IN(@IDServicioMPWP,'CUZ00133') 			
	And dbo.FnServicioMPWP(cd.IDServicio_Det) IN(@IDServicioMPWP,'CUZ00133','CUZ00905')
	--And ((cp.FlEntMP=1 And @vcMPWP = 'MAPI') Or (cp.FlEntWP=1 And @vcMPWP = 'WAYNA'))
	And ((cp.FlEntMP=1 And @vcMPWP = 'MAPI') Or (cp.FlEntWP=1 And @vcMPWP = 'WAYNA') OR  (@vcMPWP = 'RAQCHI'))
	--And cp.FlAutorizarMPWP=1
	And (cp.FlAutorizarMPWP=1 or @vcMPWP = 'RAQCHI')
	--And ((cdp.FlEntMPGenerada=0 And @vcMPWP = 'MAPI') Or (cdp.FlEntWPGenerada=0 And @vcMPWP = 'WAYNA'))
	--And ((cdp.FlEntMPGenerada=0 And @vcMPWP = 'MAPI') Or (cdp.FlEntWPGenerada=0 And @vcMPWP = 'WAYNA') OR  (@vcMPWP = 'RAQCHI'))
	And ((@vcMPWP = 'MAPI') Or ( @vcMPWP = 'WAYNA') OR  (@vcMPWP = 'RAQCHI'))
	and (cp.Titulo <>'Chd.' or (cp.Titulo ='Chd.' and cp.FlAdultoINC=1))
	where cp.IDCab=@IDCab        

	) as X        

Order by X.Orden  

