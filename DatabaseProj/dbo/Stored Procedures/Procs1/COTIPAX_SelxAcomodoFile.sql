﻿--JRF-20140527-Considerar el titulo academico como prefijo al apellidos
CREATE Procedure dbo.COTIPAX_SelxAcomodoFile  
@IDCab int,  
@IDHabit tinyint,  
@CapacidadHab tinyint,  
@EsMatrimonial bit  
As  
 Set NoCount On  
 select Case when ltrim(rtrim(cp.TxTituloAcademico)) <> '---' then cp.TxTituloAcademico else cp.Titulo End as Titulo,Apellidos  
 from ACOMODOPAX_FILE ac Left Join COTIPAX cp On ac.IDPax = cp.IDPax  
 where ac.IDCAB = @IDCab and IDHabit = @IDHabit and CapacidadHab = @CapacidadHab   
 and EsMatrimonial = @EsMatrimonial  
