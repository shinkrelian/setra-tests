﻿
CREATE PROCEDURE [COTIDET_ACOMODOESPECIAL_Upd]
	@IDDet INT,
	@CoCapacidad CHAR(2),  
	@QtPax INT,
	@UserMod CHAR(4)
AS
BEGIN
-- =====================================================================
-- Author	   : Dony Tinoco
-- Create date : 02.12.2013
-- Description : SP que actualiza el acomodo especial para la cotizacion
-- =====================================================================
	SET NOCOUNT ON  
	
	UPDATE COTIDET_ACOMODOESPECIAL SET
		QtPax = @QtPax,
		UserMod=@UserMod,
		FecMod=getdate()  
	WHERE IDDet = @IDDet And CoCapacidad = @CoCapacidad
END
