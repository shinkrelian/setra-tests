﻿Create Procedure dbo.COTIDET_PAX_UpdFlEntMPGenerada	
	@IDDet int,
	@IDPax int,
	@FlEntMPGenerada bit,	
	@UserMod char(4)
As
	Set nocount on

	Update COTIDET_PAX Set FlEntMPGenerada=@FlEntMPGenerada,
		UserMod=@UserMod, FecMod=GETDATE()
	Where IDDet=@IDDet And IDPax=@IDPax
