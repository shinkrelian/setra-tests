﻿--JRF-20160322-Quitar Filtro por Tipo Venta
CREATE PROCEDURE [dbo].[MACLIENTES_Sel_List_AddIn]
@RazonComercial varchar(60),
@IDPais char(6),
@Tipo char(2)
AS
BEGIN
	Set NoCount On
	SELECT RazonComercial, Ciudad, Pais, IDCliente
	FROM
		 (Select Distinct c.NumIdentidad, RazonComercial, c.Direccion, c.Ciudad, up.Descripcion as Pais,
			   up.IDubigeo as IDubigeoPais,	c.IDIdentidad, 
			   Case c.Tipo When 'IT' then 'INTERESADO' When 'CL' then 'CLIENTE' when 'ND' then 'NO DESEADO' End as [Status],
			   c.IDCliente
			   From MACLIENTES c Left Join MAUBIGEO up On c.IDCiudad=up.IDubigeo
			   Where (c.RazonComercial Like '%'+@RazonComercial+'%' Or ltrim(rtrim(@RazonComercial))='')
					 And c.Activo='A' And (ltrim(rtrim(@IDPais))='' Or c.IDCiudad=@IDPais)
					 And (c.Tipo = @Tipo Or ltrim(rtrim(@Tipo))='') 
					 --And (c.CoTipoVenta = @CoTipoVenta)
		 ) AS X
	ORDER BY RazonComercial     
END
