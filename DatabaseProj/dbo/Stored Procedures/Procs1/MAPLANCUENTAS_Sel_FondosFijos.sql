﻿ CREATE Procedure dbo.MAPLANCUENTAS_Sel_FondosFijos
as
select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion,
 TxDefinicion=ISNULL(c.TxDefinicion,'')
from  MAPLANCUENTAS c 
where
(c.FlFondoFijo=1 )   
 --and 
 --((@FlProvAdmin='S' and substring(c.CtaContable,0,3)<>'90') or ((@FlProvAdmin='N' and substring(c.CtaContable,0,3)='90')) or LTRIM(RTRIM(@FlProvAdmin)) ='')
 Order by c.CtaContable+ ' - ' + c.Descripcion
