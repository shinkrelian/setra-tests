﻿CREATE PROCEDURE [dbo].[MAFERIADOS_Sel_List]
@IDPais char(6),
@Descripcion varchar(100)
as
begin
	set nocount on
	select f.FechaFeriado,f.IDPais,u.descripcion pais,f.descripcion from dbo.MAFERIADOS f
	inner join dbo.MAUBIGEO u on f.IDPais=u.IDubigeo 
	Where (f.Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='') 
	and (f.IDPais=@IDPais Or @IDPais='')
	order by u.descripcion,f.FechaFeriado	 
end
