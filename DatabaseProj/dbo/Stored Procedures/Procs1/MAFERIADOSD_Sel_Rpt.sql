﻿CREATE PROCEDURE [dbo].[MAFERIADOSD_Sel_Rpt]
	@Descripcion varchar(100)
as

	set nocount on
	
	select f.FechaFeriado As MesDiaFeriado,u.descripcion,f.descripcion from dbo.MAFERIADOS f
	inner join dbo.MAUBIGEO u on f.IDPais=u.IDubigeo 	
	where (f.Descripcion Like '%'+@Descripcion+'%')
