﻿--JRF-20130821-Ordenar por el Numero de Orden
CREATE Procedure dbo.MAINCLUIDOOPERACIONES_IDIOMAS_SelxIDIdioma  
@IDIdioma varchar(12)  
As  
 Set NoCount On  
 select i.NuIncluido,i.FlPorDefecto,i.NoObserAbrev 
 from MAINCLUIDOOPERACIONES i Left Join MAINCLUIDOOPERACIONES_IDIOMAS idio On i.NuIncluido = idio.NuIncluido  
 where (ltrim(rtrim(@IDIdioma))='' or IDIdioma =@IDIdioma) and i.FlInterno = 0  
 order by NuOrden
