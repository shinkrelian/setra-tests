﻿
--HLF-20120823-Nuevo campo Siglas
CREATE Procedure [dbo].[MAIDIOMAS_Upd]
	@IDIdioma varchar(12),
	@IDIdiomaUpd varchar(12),
	@Siglas	char(3),
    @UserMod char(4)
	
As
	Set NoCount On
	
	Update MAIDIOMAS
	Set IDIdioma=@IDIdiomaUpd,
	Siglas=@Siglas,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where IDIdioma=@IDIdioma
	
