﻿CREATE Procedure [dbo].[EnviarCorreo_Contactos]
	@Perfil	nvarchar(128),
	@Destinatarios	varchar(max),
	@DestinatariosCopia	varchar(max),
	@UsuarioLogeo_Hash varchar(max),
	@Formato	varchar(4),
	@Adjuntos	nvarchar(max),
	@Tipo char(1)=''--G O R
	
As	
BEGIN
SET @Formato = 'HTML'
DECLARE 
	@DestinatariosCopiaOculta	varchar(max)='',
	@Mensaje	nvarchar(max),
	@Titulo	nvarchar(255)
	IF @TIPO='G'
	BEGIN
	--	 set @Mensaje='Se han generado un usuario para el acceso a nuestra Extranet. Por favor ingresar al siguiente link: http://extranet.setours.com/reset-password/' +@UsuarioLogeo_Hash
	 set @Mensaje = dbo.FnDevuelveTextoContactos(@UsuarioLogeo_Hash,@Destinatarios,@Tipo)
	 set @Titulo='Setours Extranet - Nuevo usuario generado'
	END

	IF @TIPO='R'
	BEGIN
	 set @Mensaje='Se va a restablecer la contraseña para el acceso a nuestra Extranet. Por favor ingresar al siguiente link: http://extranet.setours.com/change-password/' +@UsuarioLogeo_Hash
	 set @Titulo='Setours Extranet - Restablecer Password'
	END

	--set @Mensaje = @Mensaje + char(10)+' Correo {'+@Destinatarios+'}'
	
	set @Mensaje = REPLACE(@Mensaje,'?','')
	-- set @Destinatarios='jorge.huaman@setours.com'
	 --set @DestinatariosCopia='juan.carlos.cruz@setours.com'

	 --SELECT * FROM DB1.BDSETRA.dbo.maclientes

	if exists(SELECT* FROM msdb.dbo.sysmail_profile where name=@Perfil ) 	   
		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = @Perfil,
		@recipients = @Destinatarios,
		@copy_recipients= @DestinatariosCopia,
		@blind_copy_recipients = @DestinatariosCopiaOculta,
		@body = @Mensaje,
		@body_format = @Formato,
		@subject = @Titulo,
		@file_attachments = @Adjuntos
END;
