﻿

--HLF-20151013-@NoHotel varchar(100),@TxWebHotel varchar(150),
CREATE Procedure [dbo].[COTIDET_DESCRIPSERV_Upd]
	@IDDet	int,
	@IDIdioma	varchar(12),
	--@DescCorta	varchar(100),
	@DescLarga	text,
	@NoHotel varchar(100)='',
	@TxWebHotel varchar(150)='',
	@UserMod	char(4)	
As

	Set NoCount On
	
	Update COTIDET_DESCRIPSERV
		Set --DescCorta=Case When ltrim(rtrim(@DescCorta))='' Then Null Else @DescCorta End,
		DescLarga=Case When ltrim(rtrim(Cast(@DescLarga as varchar(max))))='' Then Null Else @DescLarga End,
		NoHotel=Case When ltrim(rtrim(@NoHotel))='' Then Null Else @NoHotel End,
		   TxWebHotel=Case When ltrim(rtrim(@TxWebHotel))='' Then Null Else @TxWebHotel End,
		UserMod=@UserMod
	Where IDDet=@IDDet And IDIdioma=@IDIdioma
