﻿--20140703-Aplicar Filtros enviados.    
--Germany    
--Austria    
--Switzerland : Agencias solo en Zurich y Baden    
--Netherlands: Sapa Pana    
--Ecuador: Surtreck, Salsa Reissen y Geo Reissen    
--Paraguay: Discovery Southamerica    
--JRF-201407-Agregar el filtro por CoTipoVenta  
CREATE Procedure [Dbo].[CorreosClientesTotales]    
As        
 Declare @Correo varchar(200)        
 Declare @vCorreos varchar(max)=''        
 Declare curCorreosClientes cursor For        
 SELECT distinct ISnull(Correo,'') as Correo FROM        
  (        
  Select        
  (row_number() over (order by c.idcliente)) as Nro,        
  c.Email+'; '+ isnull(c.Email2 +'; ','')+ isnull(c.Email3 +'; ','')  as Correo        
  FROM MACONTACTOSCLIENTE c Inner Join MACLIENTES m On c.IDCliente=m.IDCliente And m.Activo='A'        
  WHERE m.CoTipoVenta = 'RC' and c.EsAdminist=1  and c.Email is not null        
 -- ---------------------------------------------------------------------------------------------------------    
 --   --000960:Sapa Pana    
 ----000389:SURTREK TOUR OPERATOR    
 ----000357:SALSA REISEN CIA LTDA    
 ----001742:GEO REISEN ECUADOR    
 ----000109:DISCOVER SOUTH AMERICA SRL    
 --select * from(    
 --Select        
 --  (row_number() over (order by c.idcliente)) as Nro,m.IDCiudad,m.Ciudad,m.IDCliente,    
 --  Case When m.IDCiudad = '000059' then    
 -- case when  lower(ltrim(rtrim(m.Ciudad)))='Zurich' Or lower(ltrim(rtrim(m.Ciudad)))='Baden' then 1 Else 0 End    
 --   else 1 End as CiudadesVal,    
 --  c.Email+'; '+ isnull(c.Email2 +'; ','')+ isnull(c.Email3 +'; ','')  as Correo        
 --  FROM MACONTACTOSCLIENTE c Inner Join MACLIENTES m On c.IDCliente=m.IDCliente And m.Activo='A'          
 --WHERE c.EsAdminist=1  and c.Email is not null    
 --and m.IDCliente In('000960','000389','000357','001742','000109') Or m.IDCiudad In('000002','000005','000059')    
 --) as Y    
 --where Y.CiudadesVal = 1 and Y.Correo IS NOT NULL 
 -- --------------------------------------------------------------------------------------------------------
  -- Select        
  --(row_number() over (order by c.idcliente)) as Nro,        
  --c.Email+'; '+ isnull(c.Email2 +'; ','')+ isnull(c.Email3 +'; ','')  as Correo        
  --FROM MACONTACTOSCLIENTE c Inner Join MACLIENTES m On c.IDCliente=m.IDCliente And m.Activo='A'        
  --WHERE m.CoTipoVenta = 'RC' and c.EsAdminist=1  and c.Email is not null
  --and m.IDCiudad In( 
		--  select IDUbigeo from MAUBIGEO
		--	Where Descripcion In(
		--	--Continente Europeo
		--	'Alemania',
		--	--'Austria',
		--	'Bélgica',
		--	'Bulgaria',
		--	'Chipre',
		--	'Croacia',
		--	'Dinamarca',
		--	'Eslovaquia',
		--	'Eslovenia',
		--	'España',
		--	'Estonia',
		--	'Finlandia',
		--	'Francia',
		--	'Grecia',
		--	'Hungría',
		--	'Irlanda',
		--	'Italia',
		--	'Letonia',
		--	'Lituania',
		--	'Luxemburgo',
		--	'Malta',
		--	'Países Bajos',
		--	'Polonia',
		--	'Portugal',
		--	'Reino Unido',
		--	'República Checa',
		--	'Rumanía',
		--	'Suecia',
		--	--Continente Asiatico
		--		'Afganistán',
		--		'Arabia Saudí',
		--		'Armenia',
		--		'Azerbaiján',
		--		'Bahrein',
		--		'Bangladesh',
		--		'Brunei',
		--		'Bután',
		--		'Camboya',
		--		'China',
		--		'Chipre',
		--		'Corea del Norte',
		--		'Corea del Sur',
		--		'Emiratos Árabes Unidos',
		--		'Filipinas',
		--		'Franja de Gaza',
		--		'Georgia',
		--		'Hong Kong',
		--		'India',
		--		'Indonesia',
		--		'Irak',
		--		'Irán',
		--		'Islas Cocos',
		--		'Islas Paracel',
		--		'Islas Spratly',
		--		'Israel',
		--		'Japón',
		--		'Jordania',
		--		'Kazajstán',
		--		'Kuwait',
		--		'Kyrgyzstan',
		--		'Laos',
		--		'Líbano',
		--		'Macao',
		--		'Malasia',
		--		'Maldivas',
		--		'Mongolia',
		--		'Myanmar-Burma',
		--		'Nepal',
		--		'Oman',
		--		'Paquistán',
		--		'Qatar',
		--		'Rusia',
		--		'Singapur',
		--		'Siria',
		--		'Sri Lanka',
		--		'Tailandia',
		--		'Taiwán',
		--		'Tayikistán',
		--		'Timor del Este',
		--		'Turkmenistán',
		--		'Turquía',
		--		'Uzbekistán',
		--		'Vietnam',
		--		'West Bank',
		--		'Yemen'))
  ) as X        
 -- where Correo like 'venta%'        
 Open curCorreosClientes        
 Fetch Next From curCorreosClientes Into @Correo        
        
 While (@@FETCH_STATUS=0)        
  Begin        
  Set @Correo=REPLACE(@Correo,',',';')        
  Set @Correo=REPLACE(@Correo,'/',';')        
  Set @vCorreos=@vCorreos+@Correo        
  Fetch Next From curCorreosClientes Into @Correo        
  End        
          
 Close curCorreosClientes        
 Deallocate curCorreosClientes        
        
 --select @IniBetween        
 --select @FinBetween        
 select @vCorreos As CorreosClientes        
