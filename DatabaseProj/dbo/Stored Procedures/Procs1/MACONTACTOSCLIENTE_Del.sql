﻿Create Procedure [dbo].[MACONTACTOSCLIENTE_Del]
	@IDContacto char(3),
	@IDCliente char(6)
As
	Set NoCount On
	
	DELETE FROM MACONTACTOSCLIENTE 
    WHERE IDContacto=@IDContacto And IDCliente=@IDCliente
