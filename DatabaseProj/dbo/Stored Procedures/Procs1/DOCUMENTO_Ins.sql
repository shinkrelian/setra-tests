﻿--JRF-20140409-Agregando Cliente                        
--JRF-20140410-Agregando el FlDocManual bit de diferencia                      
--HLF-20140416-Agregando parametro @TxTitulo                  
--HLF-20140421-Agregando parametro @SsIGV                  
--HLF-20140424-Agregando parametro @NuDocumVinc                
--HLF-20140425-Agregando parametro @FeDocum                
--HLF-20140430-Agregando parametro @FlPorAjustePrecio              
--HLF-20140507-Agregando parametro @IDTipoDocVinc            
--MLL-20140526-Agregando parametro @CoTipoVenta            
--MLL-20140530-Agregando Parametro @CoCtaContable,@FeEmisionVinc          
--MLL-20140530-Agregando Parametro CoCtroCosto          
--MLL-20140609-Agregando Parametro NuFileLibre          
--HLF-20140620-Grabando campo SsSaldoCruceDMemo        
--JRF-20140719-Permitir campo de @NuDocumManual(Cancelado)      
--JHD-20140911-Agregado campo NuNtaVta
--JRF-20141216-Utilizar los case para detectar que ingrese la FeDocum recien ingresada o relacionada 
--			   Con su NuDocum relacionado.
--HLF-20150416-ISNULL((Select Titulo From COTICAB Where IDCAB=@IDCab),'')
--JHD-20150511-@NuItem_DM int=0,
--JHD-20150609-Ampliar tamaño para @CoCtaContable
--HLF-20150814-Insert.. ,SsSaldoMatchAsignac)
--HLF-20150818-If @IDTipoDoc='NCR'...
--If @IDTipoDoc='NDB'...
--HLF-20150827-Agregando @CoDebitMemoAnticipo
--HLF-20150902- OR LTRIM(RTRIM(@CoCtaContable))=''
--JRF-20151012-If @CoSerie = '005'.. (CUALQUIERA DOC. CONTABLE DE SERIE 005 DEBE CARGAR UNA CUENTA CONT. : 49... [ING. DIFERIDOS])
--JHD-20151112-Nueva condicion para tipo de Documento Tip Detail:  IF @IDTipoDoc='TIP' ...
--JHD-20151112- If @IDTipoDoc='TIP' Set @CoCtaContable='46995001'
--HLF-20151215-@FlVentaAdicional
CREATE Procedure [dbo].[DOCUMENTO_Ins]         
 @CoSerie char(3),                          
 @IDTipoDoc char(3),                          
 @IDCab int,                
 @FeDocum smalldatetime,                
 @IDCliente char(6)='',                        
 @IDMoneda char(3),                          
 @SsIGV numeric(8,2),                  
 @SsTotalDocumUSD numeric(9, 2),                          
 @FlDocManual bit,                      
 @TxTitulo varchar(100),                  
 @IDTipoDocVinc char(3)='',            
 @NuDocumVinc char(10),                
 @UserMod char(4),                   
 @FlPorAjustePrecio bit=0,              
 @CoTipoVenta char(3)='RC',           
 @CoCtaContable varchar(14)   ,          
 @FeEmisionVinc smalldatetime,          
 @CoCtroCosto char(6) ,          
 @NuFileLibre char(8),          
 @NuNtaVta CHAR(6)='',
 @NuItem_DM int=0,
 @CoDebitMemoAnticipo char(10),
 @FlVentaAdicional bit=0,
 @pNuDocum Char(10) Output                          
As                          
 Set NoCount On                          
 Declare @NuDocum Char(10)       
 --NUEVO
 IF @IDTipoDoc='TIP'
	SET @NuDocum =(SELECT IDFile FROM COTICAB where IDCAB=@IDCab)
 ELSE                   
	Execute dbo.Correlativo_SelOutput 'DOCUMENTO',1,10,@NuDocum output,@IDTipoDoc,@CoSerie                          
 
 If @CoSerie = '005'
 Begin
	Set @CoCtaContable='49611002'
 End
 Else
 Begin     
	 If @IDTipoDoc='NCR'
		Select @CoCtaContable=CoCtaContable From DOCUMENTO Where IDTipoDoc=@IDTipoDocVinc And NuDocum=@NuDocumVinc
	 If @IDTipoDoc='NDB' OR LTRIM(RTRIM(@CoCtaContable))=''
		Begin
		If @IDCliente='000054'--APT
			Begin
			If @CoSerie='006'--Servicios Exterior
				Set @CoCtaContable='70422001'
			Else
				Set @CoCtaContable='70421001'
			End
		Else
			Begin
			If @CoSerie='006'--Servicios Exterior
				Set @CoCtaContable='70411002'
			Else
				Set @CoCtaContable='70411001'
			End
		End
 End

 --
 	 If @IDTipoDoc='TIP'
		Set @CoCtaContable='46995001'

 --
 INSERT INTO DOCUMENTO                          
      (NuDocum                          
      ,IDTipoDoc                          
      ,IDCab                
      ,FeDocum                          
      ,IDCliente            
      ,IDMoneda                          
      ,SsIGV                   
      ,SsTotalDocumUSD                         
      ,FlDocManual                      
      ,TxTitulo                   
      ,IDTipoDocVinc            
      ,NuDocumVinc                
      ,FlPorAjustePrecio              
 ,UserMod               
      ,CoTipoVenta          
      ,CoCtaContable            
      ,FeEmisionVinc          
      ,CoCtroCosto            
      ,NuFileLibre                   
      --,SsSaldoCruceDMemo       
      ,NuNtaVta     
	  ,NuItem_DM  
	  ,SsSaldoMatchAsignac
	  ,CoDebitMemoAnticipo
	  ,FlVtaAdicional
      )                          
   VALUES                          
      (@NuDocum               
      ,@IDTipoDoc                          
      ,Case When @IDCab=0 Then Null Else @IDCab End                         
      ,Case When @FeDocum='01/01/1900' Then getdate() Else @FeDocum End                
      ,Case when LTRIM(rtrim(@IDCliente))='' then Null Else @IDCliente End                        
      ,@IDMoneda                          
      ,@SsIGV                   
      ,@SsTotalDocumUSD                         
      ,@FlDocManual                       
      ,case when LTRIM(rtrim(@TxTitulo))='' then           
	  --Select Titulo From COTICAB Where IDCAB=@IDCab        
	  ISNULL((Select Titulo From COTICAB Where IDCAB=@IDCab),'')
	   else @TxTitulo end                  
	  ,Case when LTRIM(rtrim(@IDTipoDocVinc))='' then Null Else @IDTipoDocVinc End                
      ,Case when LTRIM(rtrim(@NuDocumVinc))='' then Null Else @NuDocumVinc End                
      ,@FlPorAjustePrecio              
      ,@UserMod                
      ,case when LTRIM(RTRIM(@CoTipoVenta)) ='' then null else @CoTipoVenta end            
      ,case when LTRIM(RTRIM(@CoCtaContable)) ='' then null else @CoCtaContable end              
      --,case when @FeEmisionVinc ='01/01/1900' then null else @FeEmisionVinc end
      ,Case When (LTRIM(rtrim(@IDTipoDocVinc)) <>'' and LTRIM(rtrim(@NuDocumVinc))<>'') and @FlDocManual=0
	  	  Then (select FeDocum from DOCUMENTO where IDTipoDoc=@IDTipoDocVinc and NuDocum=@NuDocumVinc)
	   Else case when @FeEmisionVinc ='01/01/1900' then null else @FeEmisionVinc end
	   End
      ,case when ltrim(rtrim(@CoCtroCosto))='' then null else @CoCtroCosto end          
      , case when ltrim(rtrim(@NuFileLibre))='' then null else @NuFileLibre end          
      --,@SsTotalDocumUSD        
      ,case when ltrim(rtrim(@NuNtaVta))='' then null else @NuNtaVta end     
	   ,Case When @NuItem_DM=0 Then Null Else @NuItem_DM End         
	   ,@SsTotalDocumUSD   
	   ,case when ltrim(rtrim(@CoDebitMemoAnticipo))='' then null else @CoDebitMemoAnticipo end     
	   ,@FlVentaAdicional
		)                          
 Set @pNuDocum = @NuDocum                          


