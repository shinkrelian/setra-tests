﻿--HLF-20120726-Nuevo campo IncGuia              
--HLF-20120808-Nuevo parametro FueradeHorario              
--HLF-20120910-Nuevo Campo RedondeoTotal              
--HLF-20130128-Nuevos parametros FueradeHorario              
--HLF-20131206-Nuevo Campo AcomodoEspecial          
--JRF-20140127-Nuevo parametro ServicioEditado            
--JRF-20140227-Cambio de tamaño de decimales.     
--JRF-20140306-Agregar el campo de CalculoTarifaPendiente     
--HLF-20140828-Nuevos campos FlSSCREditado, FlSTCREditado
--JRF-20150324-Nuevo campo FlServicioTC
--JRF-20160311-Grabar el IDDet por el cual es reemplazado
CREATE Procedure [dbo].[COTIDET_Ins]                
 @IDCAB int,                
 @Cotizacion char(8),                
 @Item numeric(6, 3),                
 @Dia smalldatetime,                
 @IDubigeo char(6),                
 @IDProveedor char(6),                
 @IDServicio char(8),                
 @IDServicio_Det int,                
 @IDidioma varchar(12),                
 @Especial bit,                
 @MotivoEspecial varchar(200),                
 @RutaDocSustento varchar(200),                
            
 @Desayuno bit,                
 @Lonche bit,                
 @Almuerzo bit,                
 @Cena bit,                
 @Transfer bit,                
 --@OrigenTransfer varchar(250),              
 --@DestinoTransfer varchar(250),              
 @IDDetTransferOri int,              
 @IDDetTransferDes int,              
 @CamaMat tinyint=0,              
            
 @TipoTransporte char(1),                
            
 @IDUbigeoOri char(6),                
 @IDUbigeoDes char(6),                
            
            
 @NroPax smallint,                
 @NroLiberados smallint,                
 @Tipo_Lib char(1),                
            
 @CostoReal numeric(12,4),                
 @CostoLiberado numeric(12,4),                
 @Margen numeric(12,4),                
 @MargenAplicado numeric(6,2),                
 @MargenLiberado numeric(12,4),                
 @Total numeric(8,2),                
 @RedondeoTotal numeric(12,4),                
 @TotalOrig numeric(8,2)=0,                
 @SSCR numeric(12,4),                
 @SSCL numeric(12,4),                
 @SSMargen numeric(12,4),                
 @SSMargenLiberado numeric(12,4),                
 @SSTotal numeric(12,4),                
 @SSTotalOrig numeric(12,4),                
 @STCR numeric(12,4),                
 @STMargen numeric(12,4),                
 @STTotal numeric(12,4),                
 @STTotalOrig numeric(12,4)=0,                
 @CostoRealImpto numeric(12,4),                
 @CostoLiberadoImpto numeric(12,4),                
 @MargenImpto numeric(12,4),                
 @MargenLiberadoImpto numeric(12,4),                
 @SSCRImpto numeric(12,4),                
 @SSCLImpto numeric(12,4),                
 @SSMargenImpto numeric(12,4),                
 @SSMargenLiberadoImpto numeric(12,4),                
 @STCRImpto numeric(12,4),                
 @STMargenImpto numeric(12,4),                
 @TotImpto numeric(12,4),                
 @IDDetRel int,                
 @Servicio varchar(MAX),                
 @IDDetCopia int,                
 @Recalcular bit,              
 @PorReserva bit,              
 @IncGuia bit=0,              
 @FueradeHorario bit,              
 @UserMod char(4),                
 @CostoRealEditado bit,            
 @MargenAplicadoEditado bit,            
 @AcomodoEspecial bit,          
 @ServicioEditado bit,      
 @CalculoTarifaCotizacion bit,      
 @FlSSCREditado bit,   
 @FlSTCREditado bit,  
 @FlServicioTC bit,
 @IDDetAntiguo int =0,
   
 @pIDDet int Output                
As                
 Set Nocount On                
            
 INSERT INTO COTIDET                
 (IDCAB                
 ,Cotizacion
 ,Item                
 ,Dia                
 ,DiaNombre                           
 ,IDubigeo                
 ,IDProveedor                
 ,IDServicio          
 ,IDServicio_Det                
 ,IDidioma                
 ,Especial                
 ,MotivoEspecial                
 ,RutaDocSustento                
            
 ,Desayuno                
 ,Lonche                
 ,Almuerzo                
 ,Cena                    
 ,Transfer                     
 --,OrigenTransfer              
 --,DestinoTransfer              
 ,IDDetTransferOri              
 ,IDDetTransferDes              
            
 ,TipoTransporte                 
 ,CamaMat              
 ,IDubigeoOri                
 ,IDubigeoDes                
            
 ,NroPax                 
 ,NroLiberados                
 ,Tipo_Lib                           
            
 ,CostoReal                
 ,CostoLiberado                
 ,Margen                
 ,MargenAplicado                
 ,MargenLiberado                
 ,Total                
 ,RedondeoTotal               
 ,TotalOrig              
 ,SSCR                
 ,SSCL                
 ,SSMargen                
 ,SSMargenLiberado                
 ,SSTotal                
 ,SSTotalOrig              
 ,STCR                
 ,STMargen                
 ,STTotal                
 ,STTotalOrig              
 ,CostoRealImpto                
 ,CostoLiberadoImpto                
 ,MargenImpto                
 ,MargenLiberadoImpto                
 ,SSCRImpto                
 ,SSCLImpto                
 ,SSMargenImpto                
 ,SSMargenLiberadoImpto             
 ,STCRImpto                
 ,STMargenImpto                
 ,TotImpto                
 ,IDDetRel                           
 ,Servicio                
 ,IDDetCopia              
 ,Recalcular              
 ,PorReserva                
 ,IncGuia              
 ,FueradeHorario              
 ,ExecTrigger            
 ,CostoRealEditado            
 ,MargenAplicadoEditado              
 ,AcomodoEspecial          
 ,ServicioEditado        
 ,CalculoTarifaPendiente    
 ,FlSSCREditado  
 ,FlSTCREditado 
 ,FlServicioTC  
 ,IDDetAntiguo
 ,UserMod)                
 VALUES                
 (@IDCAB,                
 @Cotizacion,                
 @Item,                
 --Case When @Dia='01/01/1900' Then GETDATE() Else @Dia End,                
 @Dia,                
 DATENAME(dw,@Dia),                
 @IDubigeo ,                
 @IDProveedor ,                
 @IDServicio ,                
 @IDServicio_Det ,                
 @IDidioma ,                
 @Especial ,                
 Case When ltrim(rtrim(@MotivoEspecial))='' Then Null Else @MotivoEspecial End,                
 Case When ltrim(rtrim(@RutaDocSustento))='' Then Null Else @RutaDocSustento End,                
            
 @Desayuno,                
 @Lonche,                
 @Almuerzo,                
 @Cena,                
 @Transfer,               
 --Case When ltrim(rtrim(@OrigenTransfer))='' Then Null Else @OrigenTransfer End,              
 --Case When ltrim(rtrim(@DestinoTransfer))='' Then Null Else @DestinoTransfer End,               
 Case When @IDDetTransferOri=0 Then Null Else @IDDetTransferOri End,              
 Case When @IDDetTransferDes=0 Then Null Else @IDDetTransferDes End,               
            
 Case When ltrim(rtrim(@TipoTransporte))='' Then Null Else @TipoTransporte End,                
            
 @CamaMat,              
            
 @IDubigeoOri,                
 @IDubigeoDes,                
            
 @NroPax,                 
 Case When @NroLiberados=0 Then Null Else @NroLiberados End,                
 Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End,                
            
            
 @CostoReal ,                
 @CostoLiberado ,                
 @Margen ,                
 @MargenAplicado ,                
 @MargenLiberado ,                
 @Total ,                
 @RedondeoTotal ,              
 @TotalOrig ,                
 @SSCR ,                
 @SSCL ,  
 @SSMargen ,                
 @SSMargenLiberado ,                
 @SSTotal ,              
 @SSTotalOrig,                
 @STCR ,                
 @STMargen ,                
 @STTotal ,                
 @STTotalOrig,              
 @CostoRealImpto ,                
 @CostoLiberadoImpto ,                
 @MargenImpto ,                
 @MargenLiberadoImpto ,                
 @SSCRImpto ,                
 @SSCLImpto ,                
 @SSMargenImpto ,                
 @SSMargenLiberadoImpto ,                
 @STCRImpto ,                
 @STMargenImpto ,                
 @TotImpto ,                
 Case When @IDDetRel=0 Then Null Else @IDDetRel End,                
 Case When ltrim(rtrim(@Servicio))='' Then Null Else @Servicio End,                
 Case When @IDDetCopia=0 Then Null Else @IDDetCopia End                
 ,@Recalcular              
 ,@PorReserva              
 ,@IncGuia              
 ,@FueradeHorario              
 ,1               
 ,@CostoRealEditado            
 ,@MargenAplicadoEditado              
 ,@AcomodoEspecial            
 ,@ServicioEditado        
 ,@CalculoTarifaCotizacion    
 ,@FlSSCREditado  
 ,@FlSTCREditado  
 ,@FlServicioTC
 ,@IDDetAntiguo
 ,@UserMod)                
            
 Declare @IDDet Int=(Select MAX(IDDET) From COTIDET)                
 Set @pIDDet=@IDDet 
