﻿--JRF-20150227-No Considerar los Items NoShow
--JRF-20150527-Considerar solo hoteles que tengan alojamiento
--HLF-20160121-Union...
--HLF-20160126- uHtl.Descripcion as Ciudad, ...  Left Join MAUBIGEO uHtl On isnull(cdd.CoCiudadHotel,cd.IDCiudadProv)=uHtl.IDubigeo 
--HLF-20160203-comentando --And isnull(sdia.Dia,0) <>0
--INTO #HotelesRepBooking    select * from #HotelesRepBooking where (IDServicio in ....
CREATE Procedure dbo.COTIDET_SelHotelesxIDCab_Rpt  
@IDCab int                
As                
 Set NoCount On  
 Select X.Ord,X.FechaIn,X.IDProveedor,X.Ciudad,X.Hotel,X.DireccionTelefono,X.CheckINCheckOUT,X.SitioWeb,X.Latitud,X.Longitud,x.IDServicio,x.IDIdioma
 INTO #HotelesRepBooking
 from (  
 select distinct ROW_NUMBER()Over(Partition By o.IDProveedor order by o.IDProveedor)  As Ord,                
 o.Dia  as FechaIn,            
 o.IDProveedor,            
 u.Descripcion as Ciudad,            
 upper(p.NombreCorto) as Hotel,            
 p.Direccion + ISNULL('*Tel:' + p.TelefonoRecepcion1,'') + ISNULL(' - '+p.TelefonoRecepcion2,'') as DireccionTelefono ,      
 'Check In: '+Isnull(convert(char(5),s.HoraCheckIn,108),'')+' hrs'+'*'+                
 'Check Out: '+Isnull(convert(char(5),s.HoraCheckOut,108),'')+' hrs' + '*'+                 
  case when sd.TipoDesayuno = '00' then '' else 'Breakfast: ' + Isnull(ds.Descripcion,'') +'*' End             
 as CheckINCheckOUT,                   
 Isnull(p.Web,'') as SitioWeb ,      
 Isnull(TxLatitud,'') as Latitud,IsNull(TxLongitud,'') as Longitud,  
 Case When ((p.IDTipoProv <> '001' and sd.ConAlojamiento=1) Or (p.IDTipoProv='001' and sd.PlanAlimenticio=0))   
 Then   
   Case When ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0)=  
      (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)) And   
     ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0) > 0 And  
      (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=o.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)>0) 
  Then 1 else 0 End  
 Else   
  IsNull((select Top(1) rd.FlServicioNoShow from RESERVAS_DET rd Where o.IDDET=rd.IDDet and rd.Anulado=0),0)  
 End as ItemNoShow      
 ,s.IDServicio, '' as IDIdioma
 from COTIDET o                 
 Left Join MAPROVEEDORES p On o.IDProveedor = p.IDProveedor                
 Left Join MAUBIGEO u on p.IDCiudad = u.IDubigeo                 
 Left Join MASERVICIOS s on o.IDServicio = s.IDServicio                
 Left Join MASERVICIOS_DET sd on o.IDServicio_Det = sd.IDServicio_Det            
 Left Join MADESAYUNOS ds On sd.TipoDesayuno = ds.IDDesayuno            
 where o.IDCab = @IDCab and p.IDTipoProv = '001' 
 And Exists(select cd2.IDDET from COTIDET cd2 Left Join MASERVICIOS_DET sd2 On cd2.IDServicio_Det=sd2.IDServicio_Det
			Where cd2.IDCAB=@IDCab And cd2.IDProveedor=o.IDProveedor And sd.PlanAlimenticio=0
	)
 
 Union

 select distinct ROW_NUMBER()Over(Partition By cd.IDProveedor,Isnull(cdd.NoHotel,p.NombreCorto) order by cd.IDProveedor)  As Ord,            
 cd.Dia  as FechaIn,        
 cd.IDProveedor,        
 --cd.DescCiudad as Ciudad, 
 uHtl.Descripcion as Ciudad,        
  --upper(p.NombreCorto) as Hotel,        
  upper(Isnull(cdd.NoHotel,p.NombreCorto)) as Hotel,
isnull(cdd.TxDireccHotel,p.Direccion) + ISNULL('*Tel:' + isnull(cdd.TxTelfHotel1,p.TelefonoRecepcion1),'') + ISNULL(' - '+isnull(cdd.TxTelfHotel2,p.TelefonoRecepcion2),'') as DireccionTelefono ,  
 'Check In: '+Isnull(convert(char(5),isnull(cdd.FeHoraChkInHotel,cd.HoraCheckIn),108),'')+' hrs'+'*'+            
 'Check Out: '+Isnull(convert(char(5),isnull(cdd.FeHoraChkOutHotel,cd.HoraCheckOut),108),'')+' hrs' + '*'+             
  case when isnull(isnull(cdd.CoTipoDesaHotel,cd.TipoDesayuno),'00') = '00' then '' else 'Breakfast: ' + Isnull(ds.Descripcion,'') +'*' End         
 as CheckINCheckOUT,                  
 IsNull(cdd.TxWebHotel,IsNull(p.Web,'')) as SitioWeb,
 Isnull(TxLatitud,'') as Latitud,IsNull(TxLongitud,'') as Longitud,
 Case When ((p.IDTipoProv <> '001' and sd.ConAlojamiento=1) Or (p.IDTipoProv='001' and sd.PlanAlimenticio=0))   
 Then   
   Case When ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0)=  
      (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)) And   
     ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0) > 0 And  
      (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)>0) 
  Then 1 else 0 End  
 Else   
  IsNull((select Top(1) rd.FlServicioNoShow from RESERVAS_DET rd Where cd.IDDET=rd.IDDet and rd.Anulado=0),0)  
 End as ItemNoShow      
 ,sd.IDServicio, sdia.IDidioma as IDIdioma
 From 
 --COTIDET cd               
 (Select ROW_NUMBER() OVER (PARTITION BY cd1.IDServicio ORDER BY cd1.IDServicio) AS DiaServ,cd1.* ,
	--u1.Descripcion as DescCiudad, 
	ds1.Descripcion as DescDesayuno, sd1.TipoDesayuno, s1.HoraCheckIn, s1.HoraCheckOut, p1.IDCiudad as IDCiudadProv
  From COTIDET cd1
  Inner Join MAPROVEEDORES p1 On cd1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='003'        
  --Left Join MAUBIGEO u1 on p1.IDCiudad = u1.IDubigeo        
 Inner Join MASERVICIOS_DET sd1 On cd1.IDServicio_Det=sd1.IDServicio_Det And sd1.ConAlojamiento = 1    
 Inner Join MASERVICIOS s1 On sd1.IDServicio=s1.IDServicio  
 Left Join MADESAYUNOS ds1 On sd1.TipoDesayuno = ds1.IDDesayuno     
 Where IDCAB=@idcab and cd1.IncGuia = 0) cd      
    
Inner Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor And p.IDTipoProv='003' 
   Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det And sd.ConAlojamiento = 1  
   --Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
 --Inner Join MASERVICIOS_DIA sdia On sd.IDServicio=sdia.IDServicio 
 Left Join COTIDET_DESCRIPSERV cdd On cd.IDDET=cdd.IDDet And Left(ltrim(cdd.IDIdioma),9)='HOTEL/WEB' 
 And Right(rtrim(cdd.IDIdioma),1)=cd.DiaServ--=cast(sdia.dia as char(1))
 Left Join MADESAYUNOS ds On isnull(cdd.CoTipoDesaHotel,cd.TipoDesayuno) = ds.IDDesayuno
 --Left Join MASERVICIOS_ALIMENTACION_DIA sad On sad.IDServicio=sdia.IDServicio And sad.dia=sdia.dia
 Left Join MASERVICIOS_ALIMENTACION_DIA sad On sad.IDServicio=cd.IDServicio And sad.dia=cd.DiaServ
 Left Join MAUBIGEO u2 On sad.IDUbigeoDes=u2.IDubigeo        
 Left Join MASERVICIOS_DIA sdia On sdia.IDServicio=cd.IDServicio  And sdia.dia=cd.DiaServ And sdia.IDIdioma='HOTEL/WEB' 
 Left Join MAUBIGEO uHtl On isnull(cdd.CoCiudadHotel,cd.IDCiudadProv)=uHtl.IDubigeo     
 Where cd.IDCAB=@IDCab and cd.IncGuia = 0-- And IsNull(cd.IDDetRel,0)=0
	--And isnull(sdia.Dia,0) <>0
    

) as X  
 Where X.ItemNoShow = 0  

 Order by X.FechaIn                


  select * from #HotelesRepBooking 
  where (IDServicio in
	(select IDServicio from #HotelesRepBooking where IDIdioma='HOTEL/WEB') and not IDIdioma is null)
	or (not IDServicio in
	(select IDServicio from #HotelesRepBooking where IDIdioma='HOTEL/WEB') )
