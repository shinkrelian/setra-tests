﻿Create Procedure dbo.ACOMODO_VEHICULO_EXT_DET_PAX_Ins
	@NuGrupo tinyint,
	@IDDet int,	
	@NuPax int,	
	@UserMod char(4) 	
As
	Set Nocount On
	
	Insert into ACOMODO_VEHICULO_EXT_DET_PAX(NuGrupo, IDDet, NuPax,UserMod)
	Values(@NuGrupo, @IDDet, @NuPax,@UserMod)
