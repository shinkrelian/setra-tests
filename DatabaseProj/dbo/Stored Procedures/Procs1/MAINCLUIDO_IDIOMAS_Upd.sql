﻿
CREATE PROCEDURE dbo.MAINCLUIDO_IDIOMAS_Upd
	@IDIncluido	int,
	@IDIdioma	varchar(12),
	@Descripcion	varchar(MAX),
	@UserMod	char(4)
As
	Set NoCount On
	
	Update MAINCLUIDO_IDIOMAS Set Descripcion=@Descripcion ,UserMod=@UserMod, FecMod=getdate()
	Where IDIncluido=@IDIncluido And IDIdioma=@IDIdioma