﻿--JRF-20130320-Imitando Formato Sells.    
--JRF-20130806-Ordenando por fecha.  
--JRF-20130807-Diferenciando Guías.  
CREATE Procedure [dbo].[COTIVUELOS_Sel_ListSolicitudxTipoTransporte]           
@IDCAB int,      
@TipoTransporte char(1)      
As          
 Set NoCount On          
 select     
  isnull(u2.DC,'')+ ISNULL('/'+u.DC,'') as Ruta    
 ,(select COUNT(*) from COTITRANSP_PAX cpx where cpx.IDTransporte = v.ID and cpx.IDPax is not null) as CantidaPax  
 ,Vuelo    
 ,Case When @TipoTransporte = 'B' Then IsNull(ltrim(rtrim(convert(varchar(10),Fec_Emision,103))),'')
 Else IsNull(ltrim(rtrim(convert(varchar(10),Fec_Salida,103))),'')End As FecViaje          
 ,convert(char(5),Salida,108) Salida          
 ,convert(char(5),Llegada,108) Llegada          
 ,isnull(v.CodReserva,'') As CodReserva    
 ,Isnull(v.Status,'') as Status    
 from COTIVUELOS v Left Join MAPROVEEDORES p On v.IdLineaA = p.IDProveedor          
       Left Join MAUBIGEO u On v.RutaD = u.IDubigeo          
       Left Join MAUBIGEO u2 On v.RutaO = u2.IDubigeo          
  where IDCAB = @IDCAB  And TipoTransporte = @TipoTransporte          
  Order by cast (CONVERT(varchar(10),v.Fec_Salida,103)+' '+ convert(char(5),Salida,108) as datetime)  
