﻿--JHD-20150423- (Select Count(*) from COTIPAX cpx Where cpx.IDCab=cc.IDCAB and FlAutorizarMPWP=1 and FlNoShow=0) As CantAutorizadas,
--JHD-20150423- (Select Count(*) from COTIPAX cpx Where cpx.IDCab=cc.IDCAB and FlNoShow=0 And (FlEntMP=1 Or FlEntWP=1)) As CantPaxMPWP
--JHD-20150423- Case When CantPaxMPWP = CantAutorizadas and CantPaxMPWP > 0 Then 1 Else 0 End as FileAutorizado
--JHD-20150423-	And (@FilesAutorizados=0 Or y.FileAutorizado=@FilesAutorizados)
--JHD-20150504-	@FlRaqchi bit=0
--JHD-20150504-	(Select Top 1 Dia From COTIDET cd1 Where cd1.idcab=x.idcab And 
				--dbo.FnServicioMPWP(cd1.IDServicio_Det) IN('CUZ00145') Order by dia) as FechaINRaqchi,
--JHD-20150504- SELECT IDFile, IDCAB, Pax,FechaINCusco,FechaINMapi,FechaINWayna,FechaINRaqchi,DatosPaxIncompletos,
--JHD-20150503- And (@FechaIni='01/01/1900' Or 
				--(((isnull(FechaINMapi,'01/01/1900') between @FechaIni And @FechaFin) Or ((isnull(FechaINWayna,'01/01/1900') between @FechaIni And @FechaFin))) and @FlRaqchi=0)
				--or ((isnull(FechaINMapi,'01/01/1900') between @FechaIni And @FechaFin) and @FlRaqchi=1)
				--)
--JHD-20150504-  and ((NOT FechaINRaqchi IS NULL and @FlRaqchi=1) OR @FlRaqchi=0)
--JHD-20150702-  and ((NOT FechaINRaqchi IS NULL and @FlRaqchi=1) OR @FlRaqchi=0)
--JHD-20150702- dbo.FnServicioMPWP(cd1.IDServicio_Det) IN('CUZ00132','CUZ00133','CUZ00905') Order by dia) as FechaINMapi,
CREATE Procedure dbo.COTICAB_Sel_MPWP
	@FechaIni smalldatetime,
	@FechaFin smalldatetime,
	@NuFile varchar(8),
	@FilesAutorizados bit=0,
	@FlRaqchi bit=0
As
	Set Nocount On

	SELECT IDFile, IDCAB, Pax,FechaINCusco,FechaINMapi,FechaINWayna,FechaINRaqchi,DatosPaxIncompletos,
	Case When FechaINMapi is null then 0 Else 
		Case When dbo.FnDatosIncompletosxFileMAPI(y.IDCAB) = 1 Or dbo.FnDatosIncompletosxFileMAPI2(y.IDCAB) = 1 Then
			1
		Else
			0
		End
	End as DatosMPIncompletos,
	Case When FechaINWayna is null then 0 Else 
		Case When dbo.FnDatosIncompletosxFileWayna(y.IDCAB) = 1 Or dbo.FnDatosIncompletosxFileWayna2(y.IDCAB) = 1 Then
			1
		Else
			0
		End
	End as DatosWPIncompletos, 
	
	Case When FechaINMapi is null then 0 Else 
		Case When dbo.FnEntradasMPWPCompradasxFile_Total(y.IDCAB,'MAPI')=1 Then
			1
		Else
			0
		End
	End as CompradoMP,

	Case When FechaINWayna is null then 0 Else 
		Case When dbo.FnEntradasMPWPCompradasxFile_Total(y.IDCAB,'WAYNA')=1 Then
			1
		Else
			0
		End
	End as CompradoWP,

	Case When FechaINRaqchi is null then 0 Else 
		Case When dbo.FnEntradasMPWPCompradasxFile_Total(y.IDCAB,'RAQCHI')=1 Then
			1
		Else
			0
		End
	End as CompradoRQ,
	
	GenerarExcel

	FROM 
		(
		Select IDFile, IDCAB, Pax, 
		(Select Top 1 Dia From COTIDET Where IDCAB=x.IDCAB And IDubigeo='000066' Order by dia) as FechaINCusco,
		(Select Top 1 Dia From COTIDET cd1 Left Join MASERVICIOS_DET sd1 On cd1.IDServicio_Det=sd1.IDServicio_Det Where cd1.idcab=x.idcab And
			dbo.FnServicioMPWP(cd1.IDServicio_Det) --IN('CUZ00132','CUZ00133','CUZ00905') Order by dia) as FechaINMapi,
			--(select Top (1) IDServicio from MASERVICIOS_DET sd2 
			-- Where sd1.IDServicio_Det In (select IDServicio_Det_V from MASERVICIOS_DET sd3 where sd3.IDServicio=sd1.IDServicio And sd3.Anio=sd1.Anio And sd3.Tipo=sd1.Tipo
			-- And IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00912','CUZ00946'))) 
			 IN('CUZ00132','CUZ00133','CUZ00905') Order by dia) as FechaINMapi,
		(Select Top 1 Dia From COTIDET cd1 Left Join MASERVICIOS_DET sd1 On cd1.IDServicio_Det=sd1.IDServicio_Det Where cd1.idcab=x.idcab And 
			dbo.FnServicioMPWP(cd1.IDServicio_Det) 
			--(select Top (1) IDServicio from MASERVICIOS_DET sd2 
			-- Where sd1.IDServicio_Det In (select IDServicio_Det_V from MASERVICIOS_DET sd3 where sd3.IDServicio=sd1.IDServicio And sd3.Anio=sd1.Anio And sd3.Tipo=sd1.Tipo
			-- And IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00912','CUZ00946'))) 
			IN('CUZ00134','CUZ00133') Order by dia) as FechaINWayna,
		(Select Top 1 Dia From COTIDET cd1 Left Join MASERVICIOS_DET sd1 On cd1.IDServicio_Det=sd1.IDServicio_Det Where cd1.idcab=x.idcab And 
			dbo.FnServicioMPWP(cd1.IDServicio_Det)
			--(select Top (1) IDServicio from MASERVICIOS_DET sd2 
			-- Where sd1.IDServicio_Det In (select IDServicio_Det_V from MASERVICIOS_DET sd3 where sd3.IDServicio=sd1.IDServicio And sd3.Anio=sd1.Anio And sd3.Tipo=sd1.Tipo
			-- And IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00912','CUZ00946'))) 
			IN('CUZ00145') Order by dia) as FechaINRaqchi,
		dbo.FnDatosIncompletosPaxxFileINC(x.IDCAB) as DatosPaxIncompletos,
		0 as GenerarExcel

		,Case When CantPaxMPWP = CantAutorizadas and CantPaxMPWP > 0 Then 1 Else 0 End as FileAutorizado

		From
			(
			Select IDFile,IDCAB,NroPax+isnull(nroliberados,0) as Pax	

			,(Select Count(*) from COTIPAX cpx Where cpx.IDCab=COTICAB.IDCAB and FlAutorizarMPWP=1 and FlNoShow=0) As CantAutorizadas
			,(Select Count(*) from COTIPAX cpx Where cpx.IDCab=COTICAB.IDCAB and FlNoShow=0 And (FlEntMP=1 Or FlEntWP=1)) As CantPaxMPWP

			From COTICAB 
			Where IDCAB In 
				(Select cd.IDCAB
				From COTIDET cd Inner Join COTICAB cc On cd.IDCAB=cc.IDCAB And cc.Estado='A'				
				Where 
				(Dia between @FechaIni And @FechaFin Or @FechaIni='01/01/1900') And
				(cc.IDFile Like '%'+@NuFile+'%' Or LTRIM(rtrim(@NuFile))='')
				)
			) as X
			
		) AS Y
	Where (Not FechaINMapi Is Null Or Not FechaINWayna Is Null )
		 and ((NOT FechaINRaqchi IS NULL and @FlRaqchi=1) OR @FlRaqchi=0)
		And (@FechaIni='01/01/1900' Or 
		(((isnull(FechaINMapi,'01/01/1900') between @FechaIni And @FechaFin) Or ((isnull(FechaINWayna,'01/01/1900') between @FechaIni And @FechaFin))) and @FlRaqchi=0)
		or ((isnull(FechaINMapi,'01/01/1900') between @FechaIni And @FechaFin) and @FlRaqchi=1)
		)
	And (@FilesAutorizados=0 Or y.FileAutorizado=@FilesAutorizados)
	Order by FechaINCusco

