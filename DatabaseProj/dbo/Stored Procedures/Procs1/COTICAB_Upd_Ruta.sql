﻿Create Procedure dbo.COTICAB_Upd_Ruta
@IDCab int,
@UserMod char(4)
As
Set NoCount On
Update COTICAB 
	Set UserMod=@UserMod,
		NoRuta=dbo.FnDestinosDia(IDCAB,0)
Where IDCAB=@IDCab
