﻿

Create Procedure dbo.ACOMODO_VEHICULO_EXT_Ins
	@IDDet int ,
	@QtPax smallint ,
	@QtGrupo tinyint ,		
	@UserMod char(4) 
As
	Set Nocount On

	Insert Into ACOMODO_VEHICULO_EXT(IDDet,QtPax,QtGrupo,UserMod)
	Values(@IDDet,@QtPax,@QtGrupo,@UserMod)
	
