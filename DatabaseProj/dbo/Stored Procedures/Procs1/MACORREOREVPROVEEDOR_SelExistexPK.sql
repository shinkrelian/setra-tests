﻿Create Procedure dbo.MACORREOREVPROVEEDOR_SelExistexPK  
	@IDProveedor	char(6),
	@Correo	varchar(150),
	@pbExists bit Output  
As   
	Set NoCount On  
	Set @pbExists = 0  

	If Exists(Select Correo From MACORREOREVPROVEEDOR 
		Where IDProveedor=@IDProveedor And Correo=@Correo)
		Set @pbExists = 1  
    
