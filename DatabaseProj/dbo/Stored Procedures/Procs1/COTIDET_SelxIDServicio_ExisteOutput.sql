﻿--HL-20120815-cc.Estado<>'X'
CREATE Procedure dbo.COTIDET_SelxIDServicio_ExisteOutput
	@IDServicio	char(8),
	@pbExists	bit Output
As
	Set Nocount On
	
	Set @pbExists=0
	If Exists(Select IDServicio From COTIDET cd Inner Join COTICAB cc On cd.IDCAB=cc.IDCAB
		Where cd.IDServicio=@IDServicio And cc.Estado<>'X')
		Set @pbExists=1