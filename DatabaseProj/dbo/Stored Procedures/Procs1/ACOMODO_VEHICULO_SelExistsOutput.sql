﻿
Create Procedure dbo.ACOMODO_VEHICULO_SelExistsOutput
	@NuVehiculo smallint,
	@IDDet int,
	@pExists bit Output
As
	Set Nocount on
	Set @pExists = 0

	If Exists(Select NuVehiculo From ACOMODO_VEHICULO
		Where NuVehiculo=@NuVehiculo And IDDet=@IDDet)
		Set @pExists = 1

