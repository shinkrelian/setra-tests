﻿--HLF-20120726-Nuevo campo IncGuia                                          
--HLF-20120808-Nuevo campo FueradeHorario                                          
--HLF-20120820-Nuevos campos CostoRealEditado, CostoRealImptoEditado, MargenAplicadoEditado, ServicioEditado                                          
--JRF-20120823-Nuevo Campo Sigla Idioma                                        
--JRF-20120824-Nuevo Orden de Columnas                                        
--HLF-20120910-Nuevo Campo RedondeoTotal                                        
--HLF-20120924-Nueva condicion And sd.PlanAlimenticio=0 y nuevo campo PlanAlimenticio                                        
--HLF-20121006-Agregando isnull(cc.TipoCambio,0) as TipoCambioCotiCab                                        
--HLF-20121227-Agregando c.DebitMemoPendiente                                        
--HLF-20130104-Agregando IsNull(sd.DetaTipo,'') as DescVariante                                        
--JRF-20130128-Agregando Campo Varios[MASERVICIOS]                                     
--HLF-20130403-Cambiando uo.Codigo por uo.DC y ud.Codigo por ud.DC                                    
--HLF-20130522-Agregando PaxmasLiberados                                
--JRF-20130722-Agregando Fila para reconocimiento de triple.                                  
--HLF-20130829-Agregando @ConIDReserva_Det y subquery                              
--HLF-20130916-Agregando pinternac.NombreCorto as DescProvInternac,                          
--JRF-20131001-Agregando el nuevo campo CalculoTarifaPendiente                        
--HLF-20131011-Order by c.Dia, c.Item                      
--HLF-20131203-Agregando And Anulado=0),0) End a subquery IDReserva_Det                       
--HLF-20131128-Agregando c.AcomodoEspecial                    
--JRF-20140129-Considerar las fechas especiales(MAFECHASNAC).                  
--JRF-20140717-Agregar el campo de CoMoneda              
--HLF-20140717-Cambios para columna TipoCambioCotiCab            
--HLF-20140828-Agregando c.FlSSCREditado, c.FlSTCREditado          
--JRF-20141002-Se Ordena el Sub-Query para obtener el IDReserva_Det (Order By FechaOut,Capacidad desc,EsMatrimonial desc)        
--JRF-20150119-Agregar las Descripciones de AcomodosEspeciales.  
--JRF-20150324-Agregar Campo [FlServicioTC]
--HLF-20150326-, dbo.FnServicioMPWP(sd.IDServicio_Det) as IDServicio_V
--JRF-20150410-Considerar el IDTipoOper
--JRF-20150511-Agregar FlServicioUpdxAnio
--HLF-20150520-Case When Not Exists(Select IDDet From ACOMODO_VEHICULO Where IDDet=c.IDDET)
--	And Not Exists(Select IDDet From ACOMODO_VEHICULO_EXT Where IDDet=c.IDDET) Then 0 Else 1 
--HLF-20150703-dbo.FnMonedaTarifaNoUSD
--JRF-20151005-...FlServicioConVoucher
--HLF-20151228- Case When dbo.Fn_MASERVICIOS_Sel_VerificaDia(c.IDServicio, c.Dia) Then ...
--JRF-20160311-..,IsNull(c.IDDetAntiguo,0) As IDDetAntiguo
CREATE Procedure [dbo].[COTIDET_Sel_xIDCab]                                            
 @IDCAB int,                                              
 @ConCopias bit,                                            
 @SoloparaRecalcular bit,                              
 @ConIDReserva_Det bit = 0                                           
As                                              
 Set NoCount On                                              
                                  
 SELECT 
 0 As chkSelBloque ,c.IDDET,                  
 c.Item,c.Dia,                                              
 upper(substring(DATENAME(dw,c.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,c.Dia,103))) as DiaFormat,                                             
 Case When p.IDTipoProv='001' And sd.PlanAlimenticio=0 Then ''                                         
 Else                                         
 substring(convert(varchar,c.Dia,108),1,5)                                         
 End as Hora,                                             
                                  
                                  
 u.IDPais, c.IDUbigeo, u.Descripcion as DescCiudad ,p.IDTipoProv,p.IDTipoOper, s.Dias,                                 
 c.IDProveedor,                        
 p.NombreCorto as DescProveedor,                                   
                           
 pinternac.NombreCorto as DescProvInternac,   
              
 c.Servicio As DescServicioDet, 
 Case When p.IDTipoProv='001' or p.IDTipoProv='002' Then  --hoteles o restaurantes                                            
  sd.Descripcion                                             
 Else                                              
  s.Descripcion                                              
 End As DescServicioDetBup,                                               
 c.IDServicio, c.IDServicio_Det,                          
 s.IDTipoServ, ts.Descripcion as DescTipoServ , sd.Anio,sd.Tipo,              
 IsNull(sd.DetaTipo,'') as DescVariante,                                      
 c.NroPax,                                   
                                   
 CAST(c.NroPax AS VARCHAR(3))+                                      
 Case When isnull(c.NroLiberados,0)=0 then '' else '+'+CAST(c.NroLiberados AS VARCHAR(3)) end                                      
 as PaxmasLiberados,                                        
                                  
 c.IncGuia, c.NroLiberados, c.Tipo_Lib,                                              
 c.CostoReal,isnull(c.CostoRealAnt,0) as CostoRealAnt, c.CostoRealImpto, c.Margen,                                               
 c.MargenImpto, c.CostoLiberado, c.CostoLiberadoImpto,                                               
 c.MargenLiberado, c.MargenLiberadoImpto,
 Case When c.FlServicioTC=1 Then 
	(select IsNull(ctd.PoMargen,0) from COTICAB_TOURCONDUCTOR ctd where ctd.IDCab=cc.IDCAB) 
 Else c.MargenAplicado End As MargenAplicado,
 isnull(c.MargenAplicadoAnt,0) as MargenAplicadoAnt,                                              
 c.SSCR, c.SSCRImpto,                                               
c.SSMargen, c.SSMargenImpto, c.SSCL,                                               
 c.SSCLImpto, c.SSMargenLiberado, c.SSMargenLiberadoImpto,                                               
 c.SSTotal, c.SSTotalOrig, c.STCR, c.STCRImpto,                                     
 c.STMargen, c.STMargenImpto, c.STTotal, c.STTotalOrig,                                              
 c.TotImpto, c.Especial, c.MotivoEspecial, c.RutaDocSustento,                                                
 c.Desayuno, c.Lonche, c.Almuerzo, c.Cena,                                              
 c.Transfer,                               
 ISNull(c.IDDetTransferOri,0) as IDDetTransferOri, ISNull(c.IDDetTransferDes,0) as IDDetTransferDes,                                              
 ISNull(c.TipoTransporte,'') as TipoTransporte,                                              
 c.IDUbigeoOri,                                             
                                  
 --uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                              
 uo.Descripcion+'          |'+IsNull(uo.DC,'') as DescUbigeoOri,                                              
 c.IDUbigeoDes,                                             
                                  
 --ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                           
 ud.Descripcion+'          |'+IsNull(ud.DC,'') as DescUbigeoDes,                                           
                                  
 c.IDIdioma,Id.Siglas, sd.ConAlojamiento,
 Case When c.FlServicioTC=1 Then (select IsNull(ctd.PoMargen,0) from COTICAB_TOURCONDUCTOR ctd where ctd.IDCab=cc.IDCAB) else IsNull(cc.Margen,0) End as MargenCotiCab,                                               
 c.Cotizacion,isnull(c.Servicio,'') as Servicio, isnull(c.IDDetRel,0) as IDDetRel ,                                              
 isnull(c.IDDetCopia,0) as IDDetCopia,c.Total,c.TotalOrig,                     
 0 as PorReserva, '' as IDReserva,                                       
 c.ExecTrigger, 
 --c.FueradeHorario   as FueradeHorario,      
 Case When dbo.Fn_MASERVICIOS_Sel_VerificaDia(c.IDServicio, c.Dia)=1 Then
	Case When dbo.Fn_MASERVICIOS_Sel_VerificaHora(c.IDServicio, c.Dia)=1 Then
		0
	Else
		1
	End
 Else
	1
 End
 as FueradeHorario,
 c.CostoRealEditado, c.CostoRealImptoEditado, c.MargenAplicadoEditado,                              
 c.ServicioEditado, c.RedondeoTotal, sd.PlanAlimenticio,                                        
 --isnull(cc.TipoCambio,0) as TipoCambioCotiCab,            
 Case When c.FlServicioTC=1 Then 
	(select ctc2.SsTipCam from COTICAB_TIPOSCAMBIO ctc2 where ctc2.IDCab=c.IDCAB and ctc2.CoMoneda='SOL') 
 Else 
	--isnull(ctc.SsTipCam,0) 
	dbo.FnMonedaTarifaNoUSD(c.IDServicio_Det, c.IDCAB)
 End as TipoCambioCotiCab,            
 c.DebitMemoPendiente ,                                      
 Case When                 
 Exists(select sd2.IDServicio from MASERVICIOS_DET SD2 Left Join MASERVICIOS S2 on SD2.IDServicio = S2.IDServicio                
  where s2.IDCabVarios = @IDCAB and sd2.IDServicio_Det =c.IDServicio_Det) then 1 else 0 End As ServicioVarios,                                
 Case When ht.IdHabitTriple = '000' Or ht.IdHabitTriple = '002'                                
 Then ''                               
 else                               
 Case When sd.IDServicio_DetxTriple is Not null                               
  then (select sd3.Descripcion                               
    from MASERVICIOS_DET sd3 where sd3.IDServicio_Det = sd.IDServicio_DetxTriple)                              
    +' ('+ ht.Descripcion+')' else                              
 ht.Descripcion                               
 End                               
 End as TipoHabTriple,                              
                                
 Case When @ConIDReserva_Det=0 Then 0                              
 Else                              
  isnull((Select Top 1 isnull(IDReserva_Det,0) From RESERVAS_DET Where IDDet=c.IDDET         
  And Anulado=0 Order By FechaOut,Capacidad desc,EsMatrimonial desc),0)         
 --IsNull((select Top 1 isnull(rd.IDReserva_Det,0) from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva        
 --  Where rd.Anulado=0 and r.Anulado=0 and r.IDProveedor=c.IDProveedor Order By Dia)        
 --  ,0)         
 End as IDReserva_Det  ,        
 CalculoTarifaPendiente,                    
c.AcomodoEspecial  ,                  
 --Case when (p.IDTipoProv='001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv = '003' and sd.ConAlojamiento = 1)                  
 dbo.FnDescripcionFechaEspecial(c.Dia,c.IDubigeo) as DescFechaEsp,              
 sd.CoMoneda as CoMonedaServ,           
 c.FlSSCREditado, c.FlSTCREditado ,    
 Case When (((p.IDTipoProv='001'and sd.PlanAlimenticio=0) Or(p.IDTipoProv<>'001' and sd.ConAlojamiento=1)))      
 Then ROW_NUMBER()Over(Partition By c.IDProveedor,c.IDServicio_Det,c.IncGuia,dbo.FnAcomodosEspecialesxIDDet(c.IDDet) Order by c.Dia, c.Item)       
 else 0 End as ItemAlojamiento ,Ltrim(Rtrim(dbo.FnAcomodosEspecialesxIDDet(c.IDDet))) As DescAcomodosEspeciales  ,
 c.FlServicioTC, 
 dbo.FnServicioMPWP(sd.IDServicio_Det) as IDServicio_V,IsNull(cc.IDCabCopia,0) As IDCabCopia,
 dbo.FnExisteTarifasDif(c.IDDET) As FlServicioUpdxAnio,
 Case When Not Exists(Select IDDet From ACOMODO_VEHICULO Where IDDet=c.IDDET)
	And Not Exists(Select IDDet From ACOMODO_VEHICULO_EXT Where IDDet=c.IDDET) Then 0 Else 1 
	End FlAcomodosVehic,
 Cast(Case When cc.FechaOperaciones is not null then 
	Case When exists(select o.IDOperacion from OPERACIONES o Where o.IDCab=cc.IDCAB And o.IDProveedor=p.IDProveedor) then
	   Case When exists(select od.IDOperacion_Det from OPERACIONES_DET od Inner Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det 
						Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion 
						Inner Join COTIDET cd On rd.IDDet=cd.IDDET 
						where o.IDCab=c.IDCAB And o.IDProveedor=c.IDProveedor And convert(char(10),od.Dia,103)=convert(char(10),c.Dia,103)
						And rd.Anulado=0 And od.IDServicio_Det=c.IDServicio_Det And od.IDVoucher <> 0)
		then 1 else 0 End
	else 0 End 
 Else 0 End as bit) as FlServicioConVoucher,IsNull(c.IDDetAntiguo,0) As IDDetAntiguo
 FROM COTIDET c Left Join MAPROVEEDORES p On c.IDProveedor=p.IDProveedor                                               
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo                                              
 Left Join MAUBIGEO uo On c.IDUbigeoOri=uo.IDubigeo                                              
 Left Join MAUBIGEO ud On c.IDUbigeoDes=ud.IDubigeo                                          
 Left Join MASERVICIOS_DET sd On c.IDServicio_Det=sd.IDServicio_Det                                              
 Left Join MASERVICIOS s On s.IDServicio=sd.IDServicio                                  
 Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ                                              
 Left Join COTICAB cc On c.IDCAB=cc.IDCAB                                              
 Left Join MAIDIOMAS Id On c.IDidioma = Id.IDidioma                                        
 Left Join MAHABITTRIPLE ht On sd.IdHabitTriple = ht.IdHabitTriple                                
 Left Join MAPROVEEDORES pinternac On p.IDProveedorInternacional=pinternac.IDProveedor
 WHERE c.IDCAB=@IDCAB And                                          
 (c.IDDetCopia Is Null Or @ConCopias=1) And                                            
 (@SoloparaRecalcular=0 Or c.Recalcular=1)                                            
 ORDER BY c.Dia, c.Item  
