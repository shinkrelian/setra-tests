﻿CREATE Procedure [Dbo].[ACOMODOPAX_PROVEEDOR_Del]
@IDReserva int,
@IDPax int
As
	Set NoCount On
	Delete from ACOMODOPAX_PROVEEDOR Where IDReserva = @IDReserva
		And IDPax = @IDPax 
