﻿Create Procedure dbo.ASIGNACION_SelxNuAsignacion
@NuAsignacion int
as
Set NoCount On
Select X.IDDebitMemo,X.TotalPagado,case when X.TotalPagado = 0 then 'PD' Else 
	Case when X.Total - X.TotalPagado = 0 then 'PD' Else 'PP' End End As IDEstado
from (
select idm.IDDebitMemo,dm.Total,
	dm.Total - Isnull((Select SUM(SsPago+SsGastosTransferencia) from INGRESO_DEBIT_MEMO idm2 where idm2.IDDebitMemo = idm.IDDebitMemo and idm2.NuAsignacion <> idm.NuAsignacion),0) 
	as TotalPagado
from INGRESO_DEBIT_MEMO idm Left Join DEBIT_MEMO dm On idm.IDDebitMemo = dm.IDDebitMemo
where NuAsignacion = @NuAsignacion) as X
