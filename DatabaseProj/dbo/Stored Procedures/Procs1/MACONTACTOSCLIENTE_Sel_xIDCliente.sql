﻿CREATE Procedure [dbo].[MACONTACTOSCLIENTE_Sel_xIDCliente]	
	@IDCliente char(6)
As
	Set NoCount On
	
	Select --IDContacto, Nombres+' '+Apellidos As Nombre 
	Titulo+' '+Nombres+' '+Apellidos as Nombre,
	Isnull(Telefono,'')+' '+Isnull(Anexo,'') as Telefono,
	ISnull(Email,'') as Email 
	--,Cargo
	,IDContacto
	FROM MACONTACTOSCLIENTE 
    WHERE IDCliente=@IDCliente 
    Order by 1
