﻿
CREATE Procedure [dbo].[CORREOFILE_Sel_ListxInternoEE]  
 @IDCab int,  
 @CoUsrIntPara	char(4)
As   
 Set NoCount On  
   
 Select '' as IconoCorreo, '' as ImgAdjuntos,  
 c.NuCorreo,  
 isnull(QtAdjuntos,0) As CantAdjuntos,  
 CoCorreoOutlook as EntryID, NoCuentaDe as De,   
 dbo.FnDestinatariosCorreo(c.NuCorreo, 0) as Para,  
 NoAsunto as Asunto,   
 Case When CoFormato='T' Then TxMensaje Else '' End as Body,  
 Case When CoFormato='H' Then TxMensaje Else '' End as BodyHtml,  
 dbo.FnDestinatariosCorreo(c.NuCorreo, 1) as ConCopia,  
 FlImportancia as Importance,  
 FlLeido as EstadoLeido,  
 CoFormato as BodyFormat,  
 dbo.FnAdjuntosCorreo(c.NuCorreo) as RutasAdjuntos,  
 c.FeCorreo as Fecha  
 From .BDCORREOSETRA..CORREOFILE c  
 Where IDCab=@IDCab And   
 @CoUsrIntPara In (Select CoUsrIntPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE Where NuCorreo=c.NuCorreo) And  
 CoTipoBandeja='EE'  

