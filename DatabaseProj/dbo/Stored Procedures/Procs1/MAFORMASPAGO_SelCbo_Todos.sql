﻿
-------------------------------------------------------------

CREATE PROCEDURE dbo.MAFORMASPAGO_SelCbo_Todos
@Todos bit,
@Ninguno bit=0
As
	Set NoCount On
	Select * from (
	select ''As IDBIdanco,
	case when @Ninguno=1 then '' else '<TODOS>' end As Descripcion,0 As Ord  
	Union
	select '' as Id,'---' as Descripcion,1 as Ord where @Todos = 0
	Union
	select IdFor as Id,Descripcion,2 as Ord from MAFORMASPAGO 
	--where Tipo = @Tipo)
	)
	as X
	Where (@Todos=1 Or Ord <> 0)  
