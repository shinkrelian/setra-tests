﻿--HLF-20140416-Cambiando descripciones a EstadoDoc  
--HLF-20140416-d.TxTitulo as Titulo,  
--JRF-20140507-Agregar los campos e FechaIn,FechaOut,NroPax
CREATE Procedure [dbo].[DOCUMENTO_Sel_ListPDF]  
@IDCab int  
As  
Set NoCount On  
Select C.IDFile,C.Cotizacion,  
    case C.CoTipoVenta   
  When 'RC' Then 'RECEPTIVO'  
  When 'CU' Then 'COUNTER'  
  When 'VS' then 'VIAJE INSPECCION (PERSONAL SETOURS)'  
  When 'VC' Then 'VIAJE INSPECCION (CLIENTE)'  
  End as TipoVenta,uVentas.Nombre as UsVentas,uReserv.Nombre as UsReservas,uOperac.Nombre as UsOperaciones,  
  --c.Titulo,  
  d.TxTitulo as Titulo,  
  cl.RazonComercial as DescCliente,cl.Direccion,ub.Descripcion as Pais,d.IDTipoDoc,td.Descripcion as TipoDocumento,  
  substring(d.NuDocum,1,3)+'-'+SUBSTRING(d.NuDocum,4,10) as NuDocum,SsTotalDocumUSD,  
  Case d.CoEstado  
   When 'GD' Then 'GENERADO'   
   When 'IM' Then 'IMPRESO'   
   When 'AN' Then 'ANULADO'  
  End as EstadoDoc,d.NuDocum,CONVERT(Char(10),c.FecInicio,103) as FechaIn,CONVERT(char(10),c.FechaOut,103) as FechaOut,
  c.NroPax
FROM DOCUMENTO d Left Join COTICAB c on d.IDCab = c.IDCAB  
Left Join MAUSUARIOS uVentas On c.IDUsuario = uVentas.IDUsuario Left Join MAUSUARIOS uReserv On c.IDUsuarioRes = uReserv.IDUsuario  
LefT Join MAUSUARIOS uOperac On c.IDUsuarioOpe = uOperac.IDUsuario Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente  
Left Join MATIPODOC td On d.IDTipoDoc = td.IDtipodoc Left Join MAUBIGEO ub On cl.IDCiudad = ub.IDubigeo  
where d.IDCab = @IDCab  
