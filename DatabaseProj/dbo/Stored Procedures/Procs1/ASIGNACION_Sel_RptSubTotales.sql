﻿--JRF-20140801-Solo considerar el Gastos de Transferencia en el Primer DebitMemo válido.  
CREATE Procedure dbo.ASIGNACION_Sel_RptSubTotales          
@IDCliente char(6),          
@IDUbigeo char(6),          
@FechaOutRangInicio smalldatetime,          
@FechaOutRangFinal smalldatetime,          
@FechaPagoRangInicio smalldatetime,          
@FechaPagoRangFinal smalldatetime          
As          
Set NoCount On     
Select Y.DesCliente,SUM(Y.SsGastosTransferencia) as SsGastosTransferencia,    
    SUM(Y.SsBanco) as SsBanco,SUM(Y.SumaGastoTransfer) as SumaGastoTransfer,    
    SUM(Y.DifGastosTransBancos) as DifGastosTransBancos from     
(
Select distinct X.Item,X.Item2,X.DesCliente,X.IDDebitMemo,X.EjecutivoVentas,X.TotalDebitMemo,
	X.MontoAsignado,X.CoOperacionBan,X.Diferencia,X.SsGastosTransferencia,
	Case When x.Item2=1 then X.SsBanco else 0 End as SsBanco,
Case When X.Item =1 then X.SumaGastoTransfer else 0 End as SumaGastoTransfer,X.NuIngreso ,
 (X.SsGastosTransferencia+ --X.SsBanco)-Case When X.Item =1 then X.SumaGastoTransfer else 0 End as DifGastosTransBancos  
  Case When x.Item2=1 then X.SsBanco else 0 End)-Case When X.Item =1 then X.SumaGastoTransfer else 0 End as DifGastosTransBancos  
from (          
select distinct ROW_NUMBER()Over(Partition by idm.IDDebitMemo Order by idm.IDDebitMemo,idm.NuIngreso) as Item,
		ROW_NUMBER()Over(Partition by idm.NuIngreso Order by idm.IDDebitMemo,idm.NuIngreso) as Item2,
	  Case when Upper(ltrim(rtrim(cl.RazonComercial)))<> Upper(ltrim(rtrim(dm.Cliente))) then           
        Case when Upper(ltrim(rtrim(dm.Cliente))) = Upper(ltrim(rtrim(cl.RazonSocial))) then          
    UPPER(ltrim(rtrim(cl.RazonComercial))) Else Upper(ltrim(rtrim(dm.Cliente))) End          
    else Upper(ltrim(rtrim(cl.RazonComercial))) End as DesCliente,        
    Case When dm.IDTipo = 'I' Then SUBSTRING(idm.IDDebitMemo,1,8) +'-'+SUBSTRING(idm.IDDebitMemo,9,10) Else        
  SUBSTRING(idm.IDDebitMemo,1,3)+'-'+ SUBSTRING(idm.IDDebitMemo,4,10) End        
     as IDDebitMemo,        
    Isnull(uVentas.Nombre,'') as EjecutivoVentas,          
    dm.Total as TotalDebitMemo,       
    --Isnull(ifn.SsOrdenado,ifn.SsRecibido) as MontoOrdenado      
    idm.SsPago as MontoAsignado,ifn.CoOperacionBan,          
    dm.Total - idm.SsPago  as Diferencia,idm.SsGastosTransferencia,    
    --Case When idm.SsGastosTransferencia <> 0 then ifn.SsBanco else 0 end as SsBanco,          
      ifn.SsBanco,
    Case When (dbo.FnCorrelativoDMValido(c.IDCab) = SUBSTRING(idm.IDDebitMemo,9,10)) then   
    Isnull((Select SUM(Total*NroPax) from COTIDET cd           
    where cd.IDCAB = c.IDCAB and (cd.IDServicio = 'LIM00234' Or cd.IDServicio = 'LIM00461')),0) Else 0 End      
    as SumaGastoTransfer,idm.NuIngreso  
from ASIGNACION a Right Join INGRESO_DEBIT_MEMO idm On idm.NuAsignacion = a.NuAsignacion          
Right Join INGRESO_FINANZAS ifn On idm.NuIngreso = ifn.NuIngreso          
Left Join DEBIT_MEMO dm On idm.IDDebitMemo = dm.IDDebitMemo Left Join MACLIENTES cl On dm.IDCliente = cl.IDCliente          
Left Join COTICAB c On dm.IDCab = c.IDCAB Left Join MAUSUARIOS uVentas On c.IDUsuario = uVentas.IDUsuario          
Where idm.NuAsignacion is not null           
and (ltrim(rtrim(@IDCliente))='' Or dm.IDCliente = @IDCliente)           
and (ltrim(rtrim(@IDUbigeo))='' Or ifn.IDPais = @IDUbigeo)          
and ((CONVERT(char(10),@FechaOutRangInicio,103)='01/01/1900' and CONVERT(char(10),@FechaOutRangFinal,103)='01/01/1900') Or          
   Cast(convert(Char(10),dm.FechaOut,103)as smalldatetime) between  @FechaOutRangInicio and @FechaOutRangFinal)          
and ((CONVERT(char(10),@FechaPagoRangInicio,103)='01/01/1900' and CONVERT(char(10),@FechaPagoRangFinal,103)='01/01/1900') Or          
   Cast(CONVERT(Char(10),ifn.FeAcreditada,103) as smalldatetime) between @FechaPagoRangInicio and @FechaPagoRangFinal)          
and dm.IDTipo = 'I'      
)as X       
--Order By X.IDDebitMemo,X.NuIngreso
) as Y          
Group By Y.DesCliente    
