﻿CREATE Procedure [dbo].[MANIVELOPC_SelxNivel_Tvw]
	@IDNivel	char(3),
	@NivelOpc	tinyint	
As
	Set NoCount On
	
	Set @NivelOpc=@NivelOpc+@NivelOpc-1
	
	--Create Table #UsuarioAccion(IDUsuario	char(4), Accion	varchar(10), Activo Bit)
	--If @NivelOpc>1 
	--	Insert Into #UsuarioAccion Values(@IDUsuario,'Consultar',0),(@IDUsuario,'Grabar',0),(@IDUsuario,'Actualizar',0),
	--	(@IDUsuario,'Eliminar',0)
	
	Select X.* From
	(
	Select Case When @NivelOpc>1 Then SUBSTRING(UO.IDOpc,(@NivelOpc-2),2) Else SUBSTRING(UO.IDOpc,(@NivelOpc),2) End as NivelOpcUnion, 	
	SUBSTRING(UO.IDOpc,1,4) AS NivelOpcUnion2, 	
	SUBSTRING(UO.IDOpc,@NivelOpc,2) as NivelOpc, 	
	UO.IDOpc, MO.Descripcion As NombreOpcion,
	--UO.IDUsuario--, Accion, Activo 
	uo.Consultar as Activo
	From MANIVELOPC UO --Left Join #UsuarioAccion UA On UO.IDUsuario=UA.IDUsuario	
	Left Join MAOPCIONES MO On UO.IDOpc=MO.IDOPc
	Where UO.IDNivel=@IDNivel
	) as X 
	Where 
	(@NivelOpc=1 And right(X.IDOpc,4)='0000')	Or
	(NivelOpc<>'00' And @NivelOpc=3 And right(X.IDOpc,2)='00') Or 	
	(@NivelOpc=5 And NivelOpc<>'00')
	
	Order by IDOpc--, Accion
	
	
	--Drop Table #UsuarioAccion
