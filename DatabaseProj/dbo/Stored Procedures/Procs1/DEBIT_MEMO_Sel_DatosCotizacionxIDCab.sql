﻿--JRF-20130710-Agregando los liberados 
--JRF-20140213-Se debe utilizar la razón comercial 
CREATE Procedure dbo.DEBIT_MEMO_Sel_DatosCotizacionxIDCab    
@IDCab int    
as    
 Set NoCount On    
 Select (select Min(Dia) from COTIDET where IDCab = @IDCab) as FechaIn  ,    
     (select Max(Dia) from COTIDET where IDCab = @IDCab) as FechaOut ,    
     --Case when ltrim(rtrim(cl.RazonSocial))='' then cl.RazonComercial else cl.RazonSocial End 
     Ltrim(rtrim(cl.RazonComercial)) As Cliente,c.NroPax,c.Titulo,u.Nombre as Responsable,  
     Isnull(c.NroLiberados,0) as NroLiberados  
 from COTICAB c Left Join MACLIENTES cl on c.IDCliente = cl.IDCliente    
 Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario    
 where IDCAB = @IDCab    
