﻿Create Procedure MAHABITTRIPLE_Sel_Rpt
@Abreviacion varchar(4),
@Descripcion varchar(50)
As 

	Set NoCount On
	select IdHabitTriple,Abreviacion,Descripcion from MAHABITTRIPLE
	Where (LTRIM(rtrim(@Abreviacion))='' or Abreviacion like '%'+@Abreviacion+'%') And
		  (LTRIM(rtrim(@Descripcion))='' Or Descripcion like '%'+@Descripcion+'%')
