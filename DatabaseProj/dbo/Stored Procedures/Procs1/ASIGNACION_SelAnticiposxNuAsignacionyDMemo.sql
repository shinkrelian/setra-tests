﻿

--HLF-20150914-And DATEDIFF(m,ifz.FeAcreditada,(Select   ...
CREATE Procedure dbo.ASIGNACION_SelAnticiposxNuAsignacionyDMemo
	@NuAsignacion int,
	@IDDebitMemo char(10)
As        
	Set Nocount On        
      
	SELECT CC.IDCAB, --idm.SsPago     
	idm.SsPago + idm.SsGastosTransferencia as SsPago,  
	ifz.FeAcreditada,
	cc.CoTipoVenta, cc.IDCliente        
	FROM ASIGNACION asg         
	Inner Join INGRESO_DEBIT_MEMO idm On asg.NuAsignacion=idm.NuAsignacion       
	Inner Join INGRESO_FINANZAS ifz On idm.NuIngreso=ifz.NuIngreso  
	And asg.NuAsignacion=@NuAsignacion And idm.IDDebitMemo=@IDDebitMemo      
	Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo       
	Inner Join COTICAB cc On dm.IDCab=cc.IDCAB     	
	--And datediff(m,asg.FeAsignacion,cc.FechaOut)>=1    
	And DATEDIFF(m,ifz.FeAcreditada,(Select   
		 isnull((Select Max(Dia) From COTIDET cd1 Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo     
		  And u1.IDPais='000323'
		  Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det  
		  Where IDCAB=cc.IDCAB And SD1.idTipoOC<>'005'),cc.FechaOut) ))>=1
	And cc.EstadoFacturac='P'      
	Where idm.SsPago<>0    
   