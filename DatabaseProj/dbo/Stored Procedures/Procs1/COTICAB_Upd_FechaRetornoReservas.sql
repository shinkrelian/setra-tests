﻿Create Procedure dbo.COTICAB_Upd_FechaRetornoReservas
@IDCab int,
@FechaRetornoReservas smalldatetime,
@UserMod char(4)
as
Set NoCount On
Update COTICAB set 
	FechaRetornoReservas = Case when convert(char(10),@FechaRetornoReservas,103) ='01/01/1900' then Null else @FechaRetornoReservas End, 
	UserMod = @UserMod,
	FecMod = GETDATE()
Where IDCAB = @IDCab
