﻿--JRF-20131108-Nuevo campo IDPais,CodTar,NoRutaDocumento y SsOrdenado.          
--JRF-20131112-Nuevo campo SsGastosTransferencia      
--JRF-20131127-Cambiar los nombres de la columna, Nuevo campo FePresentacion    
--JRF-20140326-Aumento del tamaño del Tipo de cambio.  
--JRF-20140721-Nuevos campos [NoBancoOrdendante,NoBancoCorresponsal1,NoBancoCorresponsal2]
--JRF-20140721-Nuevos campos [IDUbigeoBancoOrde,IDUbigeoBancoCor1,NoBancoCorresponsal2]
CREATE Procedure dbo.INGRESO_FINANZAS_Ins            
@FeAcreditada smalldatetime,          
@FePresentacion smalldatetime,    
@IDBanco char(3),            
@IDFormaPago char(3),          
@CoOperacionBan varchar(20),            
@IDCliente char(6),          
@IDPais char(6),          
@CoEstado char(2),          
@NoRutaDocumento varchar(200),        
@TxObservacion varchar(max),            
@IDMoneda char(3),            
@SsTc numeric(12,4),            
@SsOrdenado numeric(10,2),        
@SsGastosTransferencia numeric(10,2),      
@SsRecibido numeric(10,2),            
@SsBanco numeric(8,2),            
@SsNeto numeric(10,2),   
@NoBancoOrdendante varchar(120),
@NoBancoCorresponsal1 varchar(120),
@NoBancoCorresponsal2 varchar(120),
@IDUbigeoBancoOrde Char(6),
@IDUbigeoBancoCor1 char(6),
@IDUbigeoBancoCor2 char(6),
@UserMod char(4),            
@pNuIngreso int output            
As            
Begin            
 Declare @NuIngreso int            
 Execute dbo.Correlativo_SelOutput 'INGRESO_FINANZAS',1,10,@NuIngreso output            
             
 Insert Into INGRESO_FINANZAS            
  (NuIngreso,            
   FeAcreditada,    
   FePresentacion,            
   IDBanco,            
   IDFormaPago,          
   CoOperacionBan,            
   IDCliente,            
   IDPais,          
   CoEstado,           
   NoRutaDocumento,         
   TxObservacion,            
   IDMoneda,            
   ssTC,            
   SsOrdenado,        
   SsGastosTransferencia,      
   SsRecibido,            
   SsBanco,            
   SsNeto,
   NoBancoOrdendante,
   NoBancoCorresponsal1,
   NoBancoCorresponsal2,
   IDUbigeoBancoOrde,
   IDUbigeoBancoCor1,
   IDUbigeoBancoCor2,
   UserMod,            
   FecMod)            
  values(            
   @NuIngreso,            
   case when ltrim(rtrim(convert(char(10),@FeAcreditada,103)))='01/01/1900' then Null Else @FeAcreditada End,            
   case when LTRIM(rtrim(convert(char(10),@FePresentacion,103)))='01/01/1900' then Null Else @FePresentacion End,    
   case when ltrim(rtrim(@IDBanco))='' Or ltrim(rtrim(@IDBanco))='000' then Null Else @IDBanco End,            
   case when ltrim(rtrim(@IDFormaPago))='' Then Null Else @IDFormaPago End,          
   case when ltrim(rtrim(@CoOperacionBan))='' then Null Else @CoOperacionBan End,            
   case when ltrim(rtrim(@IDCliente))='' then Null Else @IDCliente End,            
   @IDPais,          
   @CoEstado,            
   case when ltrim(rtrim(@NoRutaDocumento))='' then null else @NoRutaDocumento End,        
   case when ltrim(rtrim(@TxObservacion))='' then null else @TxObservacion End,            
   @IDMoneda,            
   Case When @SsTc = 0 then Null Else @SsTc End,            
   Case When @SsOrdenado = 0 then Null Else @SsOrdenado End,        
   Case When @SsGastosTransferencia = 0 then Null else @SsGastosTransferencia End,      
   @SsRecibido,            
   @SsBanco,            
   @SsNeto,  
   Case When ltrim(rtrim(@NoBancoOrdendante))='' then Null Else @NoBancoOrdendante End,
   Case When LTRIM(rtrim(@NoBancoCorresponsal1))='' then Null Else @NoBancoCorresponsal1 End,
   Case When ltrim(rtrim(@NoBancoCorresponsal2))='' then Null Else @NoBancoCorresponsal2 End,
   Case When ltrim(rtrim(@IDUbigeoBancoOrde))='' then Null Else @IDUbigeoBancoOrde End,
   Case When ltrim(rtrim(@IDUbigeoBancoCor1))='' then Null Else @IDUbigeoBancoCor1 End,
   Case When LTRIM(rtrim(@IDUbigeoBancoCor2))='' then Null Else @IDUbigeoBancoCor2 End,          
   @UserMod,            
   GETDATE())                 
  Set @pNuIngreso = @NuIngreso            
End            
