﻿--PPMG-20151221-Se agrego código para el Tipo I
CREATE Procedure [dbo].[DEBIT_MEMO_SelSgteCorrelativo]
@IDCab int,
@Tipo char(1),
@pCorrelativo varchar(8) OutPut
AS
BEGIN
	Set NoCount On
	Declare @NroCorrelativo tinyint
	set @NroCorrelativo =(select Isnull(MAX(Cast(SUBSTRING(IDDebitMemo,9,9) as tinyint)) +1,0)  from DEBIT_MEMO 
	where IDCAB = @IDCab)
	
	if @Tipo = 'I'
	begin
		set @NroCorrelativo = 0
		set @NroCorrelativo = (select Isnull(MAX(Cast(SUBSTRING(IDDebitMemo,9,9) as tinyint)) +1,0)  from DEBIT_MEMO 
							   where IDCAB = @IDCab AND IDTipo = 'I')
		set @pCorrelativo = REPLICATE('0',2-LEN(@NroCorrelativo))+ CAST(@NroCorrelativo AS VARCHAR(2))

	end
	Else if @Tipo = 'R'
	   if not exists(select IDDebitMemo from DEBIT_MEMO where LEN(IDDebitMemo)=8)
			set @pCorrelativo = (select cast(year(getdate()) as CHAR(4))+'0000')
	   else
			set @pCorrelativo = (select cast(year(getdate()) as CHAR(4)) +
										REPLICATE('0',4-LEN(MAX( Cast(SUBSTRING(IDDebitMemo,5,8) as int))+1 ))+
										CAST(( Max (Cast(SUBSTRING(IDDebitMemo,5,8) as int)) +1) as varchar(2))
										from DEBIT_MEMO where LEN(IDDebitMemo)=8)
END
