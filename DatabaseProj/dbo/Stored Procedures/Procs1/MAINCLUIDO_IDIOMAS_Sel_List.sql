﻿--JRF-20140312-Nuevo campo para el Orden.  
--JRF-20150303-Considera la existencia de los servicios incluidos en los servicios de ventas.  
CREATE Procedure [dbo].[MAINCLUIDO_IDIOMAS_Sel_List]          
 @IDIdioma varchar(12),  
 @IDCab int  
As        
 Set NoCount On      
 Select * from (    
 Select m.PorDefecto --Cast(Case When dbo.FnExisteServiciosIncluidosCotizados(@IDCab,m.IDIncluido)=1 Or m.PorDefecto=1   
 --then 1 Else 0 End as bit) as PorDefecto-- m.PorDefecto  
 ,0 as OrdIncluido,      
 mi.IDIncluido,        
 m.Tipo,  
 case m.Tipo when 'I' then 'Incluido' when 'N' then 'No Incluido' End as DescTipo,  
 mi.Descripcion,        
 DescAbrev    ,m.Orden  
 From MAINCLUIDO_IDIOMAS mi Inner Join MAINCLUIDO m On m.IDIncluido=mi.IDIncluido And mi.IDIdioma=@IDIdioma
 Where m.FlAutomatico=0
 Union
 Select 1 as PorDefecto,0 as OrdIncluido,mi.IDIncluido,m.Tipo,case m.Tipo when 'I' then 'Incluido' when 'N' then 'No Incluido' End as DescTipo,
 mi.Descripcion,DescAbrev,m.Orden
 From MAINCLUIDO_IDIOMAS mi Inner Join MAINCLUIDO m On m.IDIncluido=mi.IDIncluido And mi.IDIdioma=@IDIdioma
 Where m.FlAutomatico=1 and dbo.FnExisteServiciosIncluidosCotizados(@IDCab,m.IDIncluido)=1
  ) as X      
 Order by X.Tipo,X.PorDefecto Desc,X.Orden    
