﻿
-----------------------------------------------------------------------------------------------

--JHD-20150216- Se agrego el campo TxDefinicion
CREATE Procedure [dbo].[MAPLANCUENTAS_Sel_List] 
@Descripcion varchar(150)
As
	Set NoCount On
	Select CtaContable,Descripcion,CoCtroCosto,FlFacturacion,FlClientes--,TxDefinicion=ISNULL(TxDefinicion,'')
	from MAPLANCUENTAS
	Where (ltrim(rtrim(@Descripcion))='' Or Descripcion like '%'+ @Descripcion + '%') And CtaContable <> '000000'
	Order by 1
