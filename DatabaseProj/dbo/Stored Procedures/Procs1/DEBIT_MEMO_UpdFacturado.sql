﻿
Create Procedure dbo.DEBIT_MEMO_UpdFacturado
	@IDDebitMemo char(10),
	@FlFacturado bit,
	@UserMod char(4)
As
	Set Nocount on
	
	Update DEBIT_MEMO Set FlFacturado=@FlFacturado,
		UserMod=@UserMod,FecMod=GETDATE()
	Where IDDebitMemo=@IDDebitMemo
	
