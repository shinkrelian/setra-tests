﻿Create Procedure [dbo].[MAIDIOMASPROVEEDOR_Upd]
	@IDIdioma	varchar(12), 
	@IDProveedor	char(6) ,
	@Nivel	char(2)	,
	@UserMod	char(4) 
As
	Set NoCount On
	
	Update MAIDIOMASPROVEEDOR Set Nivel=@Nivel, UserMod=@UserMod, FechaMod=getdate()
	Where IDIdioma = @IDIdioma  And IDProveedor = @IDProveedor
