﻿--JRF-20140227-- Extensión de los decimales
CREATE Procedure [dbo].[COTIDET_QUIEBRES_Upd]    
 @IDDET_Q int,    
 @CostoReal numeric(12,4),    
 @CostoLiberado numeric(12,4),    
 @Margen numeric(12,4),    
 @MargenAplicado numeric(6,2),    
 @MargenLiberado numeric(6,2),    
 @Total numeric(8,2),    
 @RedondeoTotal numeric(12,4) = 0,  
 @SSCR numeric(12,4),    
 @SSCL numeric(12,4),    
 @SSMargen numeric(12,4),    
 @SSMargenLiberado numeric(12,4),    
 @SSTotal numeric(12,4),    
 @STCR numeric(12,4),    
 @STMargen numeric(12,4),    
 @STTotal numeric(12,4),    
 @CostoRealImpto numeric(12,4),    
 @CostoLiberadoImpto numeric(12,4),    
 @MargenImpto numeric(12,4),    
 @MargenLiberadoImpto numeric(6,2),    
 @SSCRImpto numeric(12,4),    
 @SSCLImpto numeric(12,4),    
 @SSMargenImpto numeric(12,4),    
 @SSMargenLiberadoImpto numeric(12,4),    
 @STCRImpto numeric(12,4),    
 @STMargenImpto numeric(12,4),    
 @TotImpto numeric(12,4),    
 @UserMod char(4)    
As    
 Set Nocount On    
     
 UPDATE COTIDET_QUIEBRES SET               
           CostoReal=@CostoReal    
           ,CostoLiberado=@CostoLiberado    
           ,Margen=@Margen    
           ,MargenAplicado=@MargenAplicado    
           ,MargenLiberado=@MargenLiberado    
           ,Total=@Total    
           ,RedondeoTotal=@RedondeoTotal  
           ,SSCR=@SSCR    
           ,SSCL=@SSCL    
           ,SSMargen=@SSMargen    
           ,SSMargenLiberado=@SSMargenLiberado    
           ,SSTotal=@SSTotal    
           ,STCR=@STCR    
           ,STMargen=@STMargen    
           ,STTotal=@STTotal    
           ,CostoRealImpto=@CostoRealImpto    
           ,CostoLiberadoImpto=@CostoLiberadoImpto    
           ,MargenImpto=@MargenImpto    
           ,MargenLiberadoImpto=@MargenLiberadoImpto    
           ,SSCRImpto=@SSCRImpto    
           ,SSCLImpto=@SSCLImpto    
           ,SSMargenImpto=@SSMargenImpto    
           ,SSMargenLiberadoImpto=@SSMargenLiberadoImpto    
           ,STCRImpto=@STCRImpto    
           ,STMargenImpto=@STMargenImpto    
           ,TotImpto=@TotImpto                          
           ,UserMod=@UserMod    
           ,FecMod=getdate()    
     WHERE IDDET_Q=@IDDet_Q    
