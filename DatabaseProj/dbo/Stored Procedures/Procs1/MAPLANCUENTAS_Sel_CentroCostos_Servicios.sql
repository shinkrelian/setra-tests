﻿ CREATE Procedure dbo.MAPLANCUENTAS_Sel_CentroCostos_Servicios
@CoCeCos char(6)='',
@FlProvAdmin Char(1)=''
as
select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion,
 TxDefinicion=ISNULL(pc.TxDefinicion,'')
from MAPLANCUENTAS_CENTROCOSTOS pc
inner join MAPLANCUENTAS c on pc.CtaContable=c.CtaContable
where
(pc.CoCeCos=@CoCeCos Or LTRIM(rtrim(@CoCeCos))='')   
 --and 
 --((@FlProvAdmin='S' and substring(c.CtaContable,0,3)<>'90') or ((@FlProvAdmin='N' and substring(c.CtaContable,0,3)='90')) or LTRIM(RTRIM(@FlProvAdmin)) ='')
 Order by c.CtaContable+ ' - ' + c.Descripcion

