﻿CREATE Procedure dbo.CONTROL_CALIDAD_PRO_MACUESTIONARIO_Ins
@NuControlCalidad int,
@NuCuest tinyint,
@IDProveedor char(6),
@FlExcelent bit,
@FlVeryGood bit,
@FlGood bit,
@FlAverage bit,
@FlPoor bit,
@TxComentario text,
@UserMod char(4)
As
Set NoCount On
INSERT INTO [dbo].[CONTROL_CALIDAD_PRO_MACUESTIONARIO]
           ([NuControlCalidad]
           ,[NuCuest]
           ,[IDProveedor]
           ,[FlExcelent]
           ,[FlVeryGood]
           ,[FlGood]
           ,[FlAverage]
           ,[FlPoor]
           ,[TxComentario]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@NuControlCalidad
           ,@NuCuest
           ,@IDProveedor
           ,@FlExcelent
           ,@FlVeryGood
           ,@FlGood
           ,@FlAverage
           ,@FlPoor
           ,@TxComentario
           ,@UserMod
           ,GetDate())
