﻿
Create Procedure dbo.ASIGNACION_Upd2
@NuAsignacion int,
@TxObservacion varchar(max),
@UserMod char(4)
As

	Set Nocount On
	Update ASIGNACION 
		Set TxObservacion = Case When ltrim(rtrim(@TxObservacion))='' then Null else @TxObservacion End,			
			UserMod = @UserMod,
			FecMod = GETDATE()
	where NuAsignacion = @NuAsignacion


