﻿Create Procedure dbo.COTIDET_DETSERVICIOS_InsDel_CostosGuiasVehiculos
	@IDCab int,
	@CoProveedor char(6),
	@UserMod char(4)
As
	--Set Nocount on
	
	Declare @tiCont tinyint=1
Inicio:

	Select	
	DISTINCT
	cd.IDDet,--cd.IDServicio_Det,

	(SELECT top 1 rds1.IDServicio_Det FROM COTIDET_DETSERVICIOS RDS1 INNER JOIN MASERVICIOS_DET SD1 ON RDS1.IDServicio_Det=sd1.IDServicio_Det
	INNER JOIN MASERVICIOS_DET SDV1 ON SD1.IDServicio_Det_V=sdV1.IDServicio_Det
	INNER JOIN MASERVICIOS S1 ON S1.IDServicio=SDV1.IDServicio
	INNER JOIN MAPROVEEDORES P1 ON S1.IDProveedor=P1.IDProveedor and p1.IDTipoProv in ('005','008')
	WHERE RDS1.IDDET=cd.IDDET) as IDServicio_Det,

	Servicio,
	NroPaX+ISNULL(NROLIBERADOS,0) AS Pax,
		(select COUNT(*) from ACOMODO_VEHICULO_DET_PAX where IDDet=cd.iddet and FlGuia=1
		) as CantGuiasVehi,

	(SELECT count(*) FROM COTIDET_DETSERVICIOS RDS INNER JOIN MASERVICIOS_DET SD ON RDS.IDServicio_Det=sd.IDServicio_Det
	INNER JOIN MASERVICIOS_DET SDV ON SD.IDServicio_Det_V=sdV.IDServicio_Det
	INNER JOIN MASERVICIOS S ON S.IDServicio=SDV.IDServicio
	INNER JOIN MAPROVEEDORES P ON S.IDProveedor=P.IDProveedor and p.IDTipoProv in ('005','008')
	WHERE RDS.IDDET=cd.IDDET) as CantGuiasSubServ
	Into #COTIDET_DETSERVICIOSTEMP
	From COTIDET cd inner join ACOMODO_VEHICULO av On cd.IDDET=av.IDDet
	Where cd.IDCAB=@IDCab and cd.IDProveedor=@CoProveedor
	 ORDER BY IDDet,IDServicio_Det
	
	INSERT INTO COTIDET_DETSERVICIOS
           ([IDDET]
           ,[IDServicio_Det]
           ,[CostoReal]
           ,[CostoLiberado]
           ,[Margen]
           ,[MargenAplicado]
           ,[MargenLiberado]
           ,[Total]
           ,[SSCR]
           ,[SSCL]
           ,[SSMargen]
           ,[SSMargenLiberado]
           ,[SSTotal]
           ,[STCR]
           ,[STMargen]
           ,[STTotal]
           ,[CostoRealImpto]
           ,[CostoLiberadoImpto]
           ,[MargenImpto]
           ,[MargenLiberadoImpto]
           ,[SSCRImpto]
           ,[SSCLImpto]
           ,[SSMargenImpto]
           ,[SSMargenLiberadoImpto]
           ,[STCRImpto]
           ,[STMargenImpto]
           ,[TotImpto]
           ,[UserMod]           
           ,[RedondeoTotal]           
           ,[NuNro])
     SELECT
           IDDET
           ,IDServicio_Det
           ,CostoReal
           ,CostoLiberado
           ,Margen
           ,MargenAplicado
           ,MargenLiberado
           ,Total 
           ,SSCR
           ,SSCL
           ,SSMargen
           ,SSMargenLiberado
           ,SSTotal
           ,STCR
           ,STMargen
           ,STTotal
           ,CostoRealImpto
           ,CostoLiberadoImpto
           ,MargenImpto
           ,MargenLiberadoImpto 
           ,SSCRImpto
           ,SSCLImpto
           ,SSMargenImpto
           ,SSMargenLiberadoImpto
           ,STCRImpto
           ,STMargenImpto
           ,TotImpto
           ,@UserMod
           ,RedondeoTotal           
           ,NuNro2
		FROM
		(--SELECT ROW_NUMBER() OVER(ORDER BY IDDET, IDServicio_Det) + 1 AS NuNro,
		SELECT
		*, --COTIDET_DETSERVICIOS.NuNro + 1 as NuNro2
		--NuNro+ROW_NUMBER() OVER(ORDER BY IDDET, IDServicio_Det)+@tiCont  AS NuNro2
		NuNro+@tiCont  AS NuNro2
		FROM COTIDET_DETSERVICIOS 
		WHERE CAST(IDDet AS VARCHAR(10))+' '+CAST(IDServicio_Det AS VARCHAR(10))
		IN
		(SELECT CAST(IDDet AS VARCHAR(10))+' '+CAST(IDServicio_Det AS VARCHAR(10)) 
		FROM 
			(
			--Select 
			--	DISTINCT
			--	cd.IDDet,cd.IDServicio_Det,
			--	Servicio,
			--	NroPaX+ISNULL(NROLIBERADOS,0) AS Pax,
			--	 (select COUNT(*) from ACOMODO_VEHICULO_DET_PAX where IDDet=cd.iddet and FlGuia=1
			--		) as CantGuiasVehi,

			--	(SELECT count(*) FROM COTIDET_DETSERVICIOS RDS INNER JOIN MASERVICIOS_DET SD ON RDS.IDServicio_Det=sd.IDServicio_Det
			--	INNER JOIN MASERVICIOS_DET SDV ON SD.IDServicio_Det_V=sdV.IDServicio_Det
			--	INNER JOIN MASERVICIOS S ON S.IDServicio=SDV.IDServicio
			--	INNER JOIN MAPROVEEDORES P ON S.IDProveedor=P.IDProveedor and p.IDTipoProv in ('005','008')
			--	WHERE RDS.IDDET=cd.IDDET) as CantGuiasSubServ
			--From COTIDET cd inner join ACOMODO_VEHICULO av On cd.IDDET=av.IDDet
			--Where cd.IDCAB=@IDCab and cd.IDProveedor=@CoProveedor
			SELECT * FROM #COTIDET_DETSERVICIOSTEMP
			) as X
		WHERE X.CantGuiasVehi>X.CantGuiasSubServ)
			AND (NuNro+@tiCont)-1<=@tiCont
		) as Y
		Order by IDDet,IDServicio_Det

		If @@ROWCOUNT>0
			Begin
			Set @tiCont+=1
			print @tiCont
			DROP TABLE #COTIDET_DETSERVICIOSTEMP
			goto Inicio
			End


	
	Declare curCOTIDET_DETSERVICIOSTEMP Cursor For 
	SELECT IDDet,IDServicio_Det,CantDel
	From
		(
		SELECT X.*, (X.CantGuiasSubServ-X.CantGuiasVehi) as CantDel
		FROM 
			(
			SELECT * FROM #COTIDET_DETSERVICIOSTEMP
			) as X
		WHERE X.CantGuiasVehi<X.CantGuiasSubServ
		) as Y

	Declare @IDDet int,@IDServicio_Det int,@CantDel tinyint, @NuNro tinyint
	
	Open curCOTIDET_DETSERVICIOSTEMP
	Fetch Next From curCOTIDET_DETSERVICIOSTEMP Into @IDDet,@IDServicio_Det,@CantDel
	Set @tiCont=@CantDel
	While @@FETCH_STATUS=0
		Begin
		While @tiCont >= @CantDel
			Begin
			Delete From COTIDET_DETSERVICIOS Where IDDET=@IDDet And IDServicio_Det=@IDServicio_Det And NuNro=@tiCont
			Set @tiCont-=1
			End				

		Fetch Next From curCOTIDET_DETSERVICIOSTEMP Into @IDDet,@IDServicio_Det,@CantDel
		End
	Close curCOTIDET_DETSERVICIOSTEMP
	Deallocate curCOTIDET_DETSERVICIOSTEMP

	DROP TABLE #COTIDET_DETSERVICIOSTEMP

