﻿--JHD-20160321-cast(Isnull(ccpc.TxComentario,'') as varchar(max)) as TxComentario   
CREATE procedure CONTROL_CALIDAD_PRO_Sel_CoProveedor_SinFile
@CoProveedor char(6)='',--'000098',  
@NuControlCalidad int=0    
as          
Set NoCount On    
Set NoCount On        
Select distinct X.*,
	   Case When Not Exists(select NuControlCalidad from CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc2 
						where ccpc2.NuControlCalidad=@NuControlCalidad and ccpc2.NuCuest=X.NuCuest and ccpc2.IDProveedor=X.IDProveedor) 
	   Then 'N' Else 'M' End as ValRegistro
from (    
--#Sección de Union de Hoteles (Hoteles, Operadores con Alojamiento)        
Select 
p.IDTipoProv,
ISNULL(ccpc.NuCuest,c.NuCuest) as NuCuest,
p.IDProveedor,
p.NombreCorto as DescProveedor,    
isnull(u.Descripcion,'') as Ciudad,
isnull((select descripcion from MAUBIGEO where IDubigeo=u.idpais),'') as Pais,
    Isnull(ccpc.FlExcelent,0) as FlExcelent,
	Isnull(ccpc.FlVeryGood,0) as FlVeryGood,
	ISNULL(ccpc.FlGood,0) FlGood,          
    Isnull(ccpc.FlAverage,0) as FlAverage,
	Isnull(ccpc.FlPoor,0) as FlPoor,
	--Isnull(ccpc.TxComentario,'') as TxComentario    
	cast(Isnull(ccpc.TxComentario,'') as varchar(max)) as TxComentario   
from --RESERVAS r Left Join
 MAPROVEEDORES p --On r.IDProveedor=p.IDProveedor    
Left Join MACUESTIONARIO c On p.IDTipoProv=c.IDTipoProv    
Left Join CONTROL_CALIDAD_PRO cc On cc.CoProveedor=p.IDProveedor and IsNull(cc.NuControlCalidad,0)=@NuControlCalidad    
Left Join CONTROL_CALIDAD_PRO_MACUESTIONARIO ccpc On cc.NuControlCalidad=ccpc.NuControlCalidad /*ccpc.IDProveedor=p.IDProveedor*/-- and IsNull(ccpc.NuControlCalidad,0)=@NuControlCalidad    
left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
where /*(r.IDCab=@IDCab and r.Anulado=0 and r.Estado<>'XL') and     */
   --p.IDTipoProv='001' --and   
   --and
    p.IDProveedor=@CoProveedor
   --(p.IDTipoProv='003' and Exists(select IDReserva from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det    
         -- Where rd.IDReserva=r.IDReserva and rd.Anulado=0 and sd.ConAlojamiento=1)))    
) as x

