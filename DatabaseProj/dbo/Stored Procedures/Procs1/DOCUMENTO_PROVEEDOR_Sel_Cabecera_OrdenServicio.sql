﻿--JRF-20141111-Agregar el IDTipoProv
--JRF-20141112-Agregar el SsMontoTotal
--JHD-20141201-Agregar el RutaDocSustento
--JHD-20151118-LEFT JOIN ORDEN_SERVICIO_DET_MONEDA osm on os.NuOrden_Servicio=osm.NuOrden_Servicio and osm.CoMoneda=@CoMoneda
--JHD-20151118-CoEstado = (osm.CoEstado), Isnull(osm.SsSaldo,0) as SaldoPendiente, ISNULL(osm.SsDifAceptada,0) as SsDifAceptada,  IsNull(osm.TxObsDocumento,'') as TxObsDocumento ,
--JHD-20151118-osm.SsMontoTotal
--JRF-20160330-Agregar @IDProveedorSetours
CREATE Procedure dbo.DOCUMENTO_PROVEEDOR_Sel_Cabecera_OrdenServicio  
@NuVoucher char(14),
@IDProveedorSetours char(6),
@CoMoneda char(3) 
As  
Set NoCount On  
Select c.IDFile,
SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,    
 os.IDProveedor,
 p.NombreCorto as DescProveedor,
 cl.RazonComercial as Cliente,
 c.NroPax as Pax,
 Isnull(c.NroLiberados,0) as Liberados,    
TipoPax=case when c.TipoPax='EXT' then 'EXTRANJERO' when c.TipoPax='NCR' then 'NACIONAL CON RUC' when TipoPax='NSR' then 'NACIONAL SIN RUC' else '' end,    
Tipo_lib=case when c.Tipo_Lib='S' then 'SIMPLE' when c.Tipo_Lib='D' then 'DOBLE' else 'NINGUNO' end,    
c.Titulo,
uVent.Nombre as UsuarioVen,
uOper.Nombre as UsuarioOpe,
c.ObservacionVentas,
c.ObservacionGeneral,
c.FecInicio as FechaInicio,
c.FechaOut as FechaFin,  
os.NuOrden_Servicio as NuOrdenServicio,  

CoEstado = (osm.CoEstado), Isnull(osm.SsSaldo,0) as SaldoPendiente, ISNULL(osm.SsDifAceptada,0) as SsDifAceptada,  IsNull(osm.TxObsDocumento,'') as TxObsDocumento ,

p.IDTipoProv,
osm.SsMontoTotal
--,'' RutaDocSustento
,RutaDocSustento=isnull((select top 1 RutaDocSustento from COTIDET  where IDServicio_Det in (select IDServicio_Det from ORDEN_SERVICIO_DET where NuOrden_Servicio=os.NuOrden_Servicio)   order by RutaDocSustento desc),''),IsNull(pSet.NombreCorto,'') as ProveedorSetours
from ORDEN_SERVICIO os Left Join COTICAB c On os.IDCab=c.IDCAB  
LEFT JOIN ORDEN_SERVICIO_DET_MONEDA osm on os.NuOrden_Servicio=osm.NuOrden_Servicio and osm.CoMoneda=@CoMoneda
Left Join MAPROVEEDORES p On os.IDProveedor=p.IDProveedor  
Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente  
Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario  
Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario  
Left Join MAPROVEEDORES pSet On os.IDProveedorSetours=pSet.IDProveedor
where CoOrden_Servicio=@NuVoucher And Ltrim(Rtrim(IsNull(os.IDProveedorSetours,'')))=@IDProveedorSetours
