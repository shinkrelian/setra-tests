﻿--JHD-20150730-SELECT isnull(@CtaContableX,@CtaContable) as 'CtaContable'  
CREATE PROC [dbo].[MACLIENTES_SelxCtaContable_CentroCostos]   
 @IDCliente char(6),    
 @CtaContable char(14),    
 @IDCAB INT    
    
AS         
Set NoCount On  
 Declare @CtaContableX char(14)  
 Declare @CoCtroCostoX char(6)  
    
 --IF (isnull((Select CoCtroCosto From MAPLANCUENTAS Where CtaContable = @CtaContable ),'') = '')  
 --IF @CtaContable ='750000'   
 IF LEFT(@CtaContable,2) ='75'   
  BEGIN  
   --IF (isnull((Select CoCtroCosto From MAPLANCUENTAS Where CtaContable =  
   --   (Select  NuCuentaComision From MACLIENTES Where IDCliente =  @IDCliente)),'')='' )  
    IF (isnull((Select top 1 CoCeCos From MAPLANCUENTAS_CENTROCOSTOS Where CtaContable =  
      (Select  NuCuentaComision From MACLIENTES Where IDCliente =  @IDCliente)),'')='' )  
    BEGIN   
     SET @CtaContableX= (Select NuCuentaComision From MACLIENTES Where IDCliente =   
            (Select IDCliente From COTICAB Where IDCAB  =@IDCAB))   
    -- SET @CoCtroCostoX= (Select CoCtroCosto From MAPLANCUENTAS Where CtaContable =@CtaContableX )    
    END       
   ELSE  
    BEGIN  
     SET @CtaContableX=(Select NuCuentaComision From MACLIENTES Where IDCliente =  @IDCliente)  
    -- SET @CoCtroCostoX=(Select CoCtroCosto From MAPLANCUENTAS Where CtaContable =@CtaContableX)   
    END  
  END  
 ELSE  
  BEGIN  
   SET @CtaContableX= @CtaContable  
  -- SET @CoCtroCostoX= (Select CoCtroCosto From MAPLANCUENTAS Where CtaContable = @CtaContable)   
  END  
    
 --SELECT /*@CoCtroCostoX as 'CoCtroCosto' ,*/@CtaContableX as 'CtaContable'  
 SELECT isnull(@CtaContableX,@CtaContable) as 'CtaContable'  
