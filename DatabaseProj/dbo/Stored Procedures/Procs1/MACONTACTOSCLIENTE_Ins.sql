﻿
--PPMG-20151125-Se valido que no se duplique el usuario.
--PPMG-20151126-Se agrego el campo FlAccesoWeb
--JHD-20160225- Se quito la insercion de usuario y contraseña
CREATE Procedure [dbo].[MACONTACTOSCLIENTE_Ins]  
 @IDCliente char(6),   
 @Titulo varchar(5),  
 @Nombres varchar(80),  
 @Apellidos varchar(80),  
 @Cargo varchar(100),  
 @Telefono varchar(30),  
 @Fax varchar(30),  
 @Celular varchar(30),  
 @Email varchar(200), 
 @Email2 varchar(200), 
 @Email3 varchar(200), 
 @Anexo varchar(15),  
 @UsuarioLogeo varchar(80)='',  
 @PasswordLogeo varchar(15)='',  
 @FechaPassword datetime,  
 @EsAdminist bit,  
 @Notas varchar(max),  
 @UserMod char(4),
 @FlAccesoWeb bit
AS
BEGIN
/*
	SET @UsuarioLogeo = ltrim(rtrim(@UsuarioLogeo))
	IF @UsuarioLogeo <> ''
	BEGIN
		IF [dbo].[FnExisteUsuarioProvClie]('', @IDCliente, 'C', @UsuarioLogeo)=1
		BEGIN
			RAISERROR ('Ya esiste el nombre del usuario a ingresar.', 16, 1);
			RETURN
		END
	END
	*/
 Set NoCount On  
 Declare @IDContacto char(3), @tiIDContacto tinyint  
  
 Select @tiIDContacto = IsNull(MAX(IDContacto),0)+1 From MACONTACTOSCLIENTE Where IDCliente=@IDCliente   
  
 Set @IDContacto=@tiIDContacto  
 Set @IDContacto=REPLICATE('0',3-LEN(@IDContacto))+@IDContacto  
  
 INSERT INTO MACONTACTOSCLIENTE  
           (IDContacto  
           ,IDCliente   
           ,Titulo  
           ,Nombres  
           ,Apellidos  
           ,Cargo  
           ,Telefono  
           ,Fax  
           ,Celular  
           ,Email 
           ,Email2 
           ,Email3
           ,Anexo  
           --,UsuarioLogeo  
           --,PasswordLogeo  
           ,FechaPassword  
           ,EsAdminist  
           ,Notas  
           ,UserMod
		   ,FlAccesoWeb
           )  
     VALUES  
           (@IDContacto,  
            @IDCliente,  
            @Titulo,  
            @Nombres,  
            @Apellidos,  
            Case When ltrim(rtrim(@Cargo))='' Then Null Else @Cargo End,  
            Case When ltrim(rtrim(@Telefono))='' Then Null Else @Telefono End,  
            Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End,  
            Case When ltrim(rtrim(@Celular))='' Then Null Else @Celular End,  
            Case When ltrim(rtrim(@Email))='' Then Null Else @Email End,  
            Case When LTRIM(rtrim(@Email2))='' then Null Else @Email2 End, 
            Case When LTRIM(rtrim(@Email3))='' then Null Else @Email3 End, 
            Case When ltrim(rtrim(@Anexo))='' Then Null Else @Anexo End,  
           -- Case When ltrim(rtrim(@UsuarioLogeo))='' Then Null Else @UsuarioLogeo End,  
           -- Case When ltrim(rtrim(@PasswordLogeo))='' Then Null Else @PasswordLogeo End,  
            Case When @FechaPassword='01/01/1900' Then Null Else @FechaPassword End,  
            @EsAdminist,  
            Case When ltrim(rtrim(@Notas))='' Then Null Else @Notas End,  
            @UserMod,
			@FlAccesoWeb
           )  
END;
