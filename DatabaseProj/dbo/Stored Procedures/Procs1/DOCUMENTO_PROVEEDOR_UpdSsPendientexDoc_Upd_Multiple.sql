﻿--JHD-20151002- Se cambio la consulta para actualizar el saldo segun forma de egreso
--JHD-20151102- select  top 1  @CoObligacionPago = CoObligacionPago from DOCUMENTO_PROVEEDOR where NuDocum_Multiple=@NuDocumProv
CREATE PROCEDURE DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc_Upd_Multiple
	@NuDocumProv int,
	@UserMod char(4)=''
as
begin

declare @CoObligacionPago char(3)=''
--select @CoObligacionPago = CoObligacionPago from DOCUMENTO_PROVEEDOR where NuDocumprov=@NuDocumProv
select  top 1  @CoObligacionPago = CoObligacionPago from DOCUMENTO_PROVEEDOR where NuDocum_Multiple=@NuDocumProv

if @CoObligacionPago='VOU'
	BEGIN
		update VOUCHER_OPERACIONES
		set SsSaldo=SsMontoTotal,CoEstado='PD',UserMod=@UserMod,FecMod=getdate()
		WHERE IDVoucher IN (SELECT NUVOUCHER FROM DOCUMENTO_PROVEEDOR WHERE NuDocum_Multiple=@NuDocumProv)

		update VOUCHER_OPERACIONES_DET
		set SsSaldo=SsMontoTotal,CoEstado='PD',UserMod=@UserMod,FecMod=getdate()
		WHERE IDVoucher IN (SELECT NUVOUCHER FROM DOCUMENTO_PROVEEDOR WHERE NuDocum_Multiple=@NuDocumProv)
		and CoMoneda=(select CoMoneda from DOCUMENTO_PROVEEDOR where NuDocumprov=@NuDocumProv)

	END

if @CoObligacionPago='OPA'
	BEGIN
		update ORDENPAGO
		set SsSaldo=SsMontoTotal,CoEstado='PD',UserMod=@UserMod,FecMod=getdate()
		WHERE IDOrdPag IN (SELECT IDOrdPag FROM DOCUMENTO_PROVEEDOR WHERE NuDocum_Multiple=@NuDocumProv)
	END

if @CoObligacionPago='OSV'
	BEGIN
		update ORDEN_SERVICIO
		set SsSaldo=SsMontoTotal,CoEstado='PD',UserMod=@UserMod,FecMod=getdate()
		WHERE NuOrden_Servicio IN (SELECT NuOrden_Servicio FROM DOCUMENTO_PROVEEDOR WHERE NuDocum_Multiple=@NuDocumProv)
	END

end;

