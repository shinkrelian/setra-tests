﻿Create Procedure dbo.MAMONEDAS_Sel_CboList
@Todos bit
as
Set NoCount On
select '' as IDMoneda,'<TODOS>' as Descripcion 
where @Todos=1
Union
Select IDMoneda,Descripcion from MAMONEDAS
where Activo = 'A'
Order by Descripcion
