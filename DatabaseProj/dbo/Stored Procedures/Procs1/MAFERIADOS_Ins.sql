﻿CREATE PROCEDURE [dbo].[MAFERIADOS_Ins]
@FechaFeriado date,
@IDPais char(6),
@Descripcion varchar(100),
@UserMod char(4)
as
begin
	insert into dbo.MAFERIADOS(FechaFeriado,IDPais,Descripcion,UserMod)
	values(@FechaFeriado,@IDPais,@Descripcion,@UserMod)	
end
