﻿--JRF-20141029-Campo a considerar 0 as NuOrdenServicio , y Campos Nuevos      
--JRF-20141111-Agregar el IDTipoProv      
--JRF-20141112-Agregar el MontoTotal      
--JHD-20141201-Agregar el RutaDocSustento 
--HLF-20150127-@CoProveedor char(6)='', @NuFile char(8) = '',    
--((@NuVoucher=0 AND @CoProveedor<>'001836')     
--AND (c.IDCAB=@IDCab Or @IDCab=0)    
--HLF-20150202-@NuVoucher a tipo varchar(14)  
--If Left(@NuVoucher,3)='PR-' Set @NuVoucher='0'  
--Case When @CoProveedor='001836' Then 'PR-'+IDFile Else IDVoucher End as IDVoucher,  
--  Case When @CoProveedor='001836' Then   
-- (Select SUM(TotalGen) From OPERACIONES_DET Where IDOperacion=X.idoperacion)  
--HLF-20150217-Case When vtr.SsTotal Is null Then...
--Left Join VOUCHER_PROV_TREN vtr On vtr.NuVouPTrenInt=o.NuVouPTrenInt
--HLF-20150226-Left Join VOUCHER_PROV_TREN vtr On vtr.NuVouPTrenInt=otren.NuVouPTrenInt
--Left Join OPERACIONES otren On otren.IDCab=c.IDCAB And otren.IDProveedor='001836'
--JHD-20150226-  SaldoPendiente=SaldoPendiente - isnull((select top 1 isnull(op2.SsMontoTotal,0)
				--from 
				--ordenpago op2
				--left join reservas r2 on op2.IDReserva=r2.IDReserva
				--left join OPERACIONES o2 on o2.IDReserva=r2.IDReserva
				--left join OPERACIONES_DET od2 on od2.IDOperacion=o2.IDOperacion
				--where od2.IDVoucher=@NuVoucher and o2.IDOperacion=X.IDOperacion AND OP2.IDEstado='PG'),0),
--JHD-20150522-   SaldoPendiente=SaldoPendiente-isnull((select sum(distinct isnull(op2.SsMontoTotal,0))
				--from 
				--ordenpago op2
				--inner join reservas r2 on op2.IDReserva=r2.IDReserva
				--inner join OPERACIONES o2 on o2.IDReserva=r2.IDReserva
				--inner join OPERACIONES_DET od2 on od2.IDOperacion=o2.IDOperacion
				--where od2.IDVoucher=@NuVoucher and o2.IDOperacion=X.IDOperacion AND OP2.IDEstado='PG'),0),
--JHD-20150831- case when isnull(p.IDProveedorInternacional,'')<>'' then p.IDProveedorInternacional else o.IDProveedor end as IDProveedor,
--JHD-20150831- isnull(case when isnull(p.IDProveedorInternacional,'')<>'' then (SELECT nombrecorto from MAPROVEEDORES pro where pro.idproveedor= p.IDProveedorInternacional) else P.NombreCorto end,'') as DescProveedor,
--JHD-20150925- Se cambio la consulta para listar los montos segun la tabla Detalle
----DOCUMENTO_PROVEEDOR_Sel_Cabecera_Voucher 13502,'429061','002397'
CREATE Procedure dbo.DOCUMENTO_PROVEEDOR_Sel_Cabecera_Voucher                
@IDCab int=0,    
--@Proveedor varchar(50) = ''--'Setours',
--@NuVoucher int=0,--400002--401402 
@NuVoucher varchar(14)='',
--@NuDocum varchar(10)='' 
@CoProveedor char(6) =''    
,@CoMoneda char(3)=''
As                                  

 
 Set Nocount On                     
    

 If Left(@NuVoucher,2)='PR' Set @NuVoucher='0'                  

 SELECT  DISTINCT                  
  IDFile,                          
  --IDVoucher,
   Case When @CoProveedor='001836' Then 'PR-'+IDFile Else Cast(IDVoucher as varchar(14)) End as IDVoucher,  
  IDProveedor,       
  IDTipoProv, 
  DescProveedor,
 RazonComercial as Cliente,                
  Pax,
    Liberados,
   TipoPax=case when TipoPax='EXT' then 'EXTRANJERO' when TipoPax='NCR' then 'NACIONAL CON RUC' when TipoPax='NSR' then 'NACIONAL SIN RUC' else '' end,                
   Tipo_lib=case when Tipo_lib='S' then 'SIMPLE' when Tipo_lib='D' then 'DOBLE' else 'NINGUNO' end,                
   Titulo,                
   UsuarioVen,
   UsuarioOpe,
 ObservacionVentas,
 ObservacionGeneral,
  FechaInicio,
  FechaFin  ,
  0 as NuOrdenServicio,
  CoEstado,
  --SaldoPendiente=SaldoPendiente - isnull((select top 1 isnull(op2.SsMontoTotal,0)
     
  ----SaldoPendiente=SaldoPendiente - isnull((select distinct sum(isnull( op2.SsMontoTotal,0))
		--							from 
		--							ordenpago op2
		--							inner join reservas r2 on op2.IDReserva=r2.IDReserva
		--							inner join OPERACIONES o2 on o2.IDReserva=r2.IDReserva
		--							inner join OPERACIONES_DET od2 on od2.IDOperacion=o2.IDOperacion
		--							where od2.IDVoucher=@NuVoucher and o2.IDOperacion=X.IDOperacion AND OP2.IDEstado='PG')),

	  SaldoPendiente=SaldoPendiente,
	  /*
	  -isnull((select sum(distinct isnull(op2.SsMontoTotal,0))

									from 
									ordenpago op2
									inner join reservas r2 on op2.IDReserva=r2.IDReserva
									inner join OPERACIONES o2 on o2.IDReserva=r2.IDReserva
									inner join OPERACIONES_DET od2 on od2.IDOperacion=o2.IDOperacion
									where od2.IDVoucher=@NuVoucher and o2.IDOperacion=X.IDOperacion AND OP2.IDEstado='PG'),0),
*/
  SsDifAceptada,TxObsDocumento,
  --Case When @CoProveedor='001836' Then   
	 -- Case When SaldoPR Is null Then
		--(Select SUM(TotalGen) From OPERACIONES_DET Where IDOperacion=X.idoperacion)  

	 -- Else

		--SaldoPR

	 -- End

  --Else  

 SsMontoTotal      

  --End 
  as SsMontoTotal   

  ,RutaDocSustento

 FROM                               
 (                                  

 Select           

 Case When od.IDVoucher=0 Or Exists(select vh2.IDVoucher from VOUCHER_HISTORICO vh2           

         where vh2.IDOperacion_Det=od.IDOperacion_Det and vh2.IDVoucher=@NuVoucher) then          

 vh.IDVoucher else od.IDVoucher End as IDVoucher,          

 o.IDFile,                

 --p.IDProveedor,     
 
 case when isnull(p.IDProveedorInternacional,'')<>'' then p.IDProveedorInternacional else o.IDProveedor end as IDProveedor,           

 --p.NombreCorto as DescProveedor,                                  
 isnull(case when isnull(p.IDProveedorInternacional,'')<>'' then (SELECT nombrecorto from MAPROVEEDORES pro where pro.idproveedor= p.IDProveedorInternacional) else P.NombreCorto end,'') as DescProveedor,

 --p.Direccion + isnull(char(13) + uPro.Descripcion,'') as Direccion ,                                   

 isnull(uPro.Descripcion,'') as Direccion ,                                   

 c.Titulo,                                  

      uv.Nombre as UsuarioVen,         

       uo.Nombre as UsuarioOpe,                  

   c.ObservacionVentas,                  

   c.ObservacionGeneral,                     

 --od.NroPax + Isnull(od.NroLiberados,0) as Pax,                      

   c.NroPax as Pax,                

   isnull(c.NroLiberados,0) as Liberados,                

   c.TipoPax,                          

   isnull(c.Tipo_lib,'') as Tipo_lib,                

   --c.Titulo,                

   c.FecInicio as FechaInicio,                

   c.FechaOut as FechaFin,                

                   

 us.Nombre as Responsable,                                  

 IsNull(r.CodReservaProv,'') as CodReservaProv,                                   

 c.IDIdioma,              

 od.IDIdioma as IdiomaServicio,                                  

 u.Descripcion as Nacionalidad,                                  

                                   

 od.Dia,                                    

 --(Select MAX(FechaOut) From OPERACIONES_DET Where IDOperacion=@IDOperacion And IDVoucher<>0) as FechaOut,                    

 GETDATE() as FechaOut,                                

 od.Cantidad,                

 od.Servicio,                           

 LTRIM(                                  

 Case When od.Desayuno=1 Then 'DESAYUNO ' Else '' End+
 Case When od.Lonche=1 Then 'BOX LUNCH ' Else '' End+
 Case When od.Almuerzo = 1 Then 'ALMUERZO ' Else '' End+
 Case When od.Cena=1 Then 'CENA' Else '' End) as Buffet,
 --dbo.FnUnionObservacionVoucherInternoBibliaxOperacion(@IDOperacion,od.IDVoucher) As ObservVoucher,                
 '' As ObservVoucher,
 Case when s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End as  TipoServ 
,(select ExtrNoIncluid from VOUCHER_OPERACIONES vo where vo.IDVoucher = od.IDVoucher )  as ExtraNoIncluido    ,                            
 od.ObservInterno ,                
 c.NroPax+Isnull(c.NroLiberados,0) as NroPaxCoti ,                  
 (select COUNT(distinct od2.IDServicio_Det)                
  from OPERACIONES_DET od2 where od2.IDOperacion = o.IDOperacion) as CantServicios,                  
 sd.DetaTipo as DescripcionTarifa                  
,cl.RazonComercial  ,            

Case When @CoProveedor<>'001836' Then     

 Case When od.IDVoucher=0 Or Exists(select vh2.IDVoucher from VOUCHER_HISTORICO vh2           

         where vh2.IDOperacion_Det=od.IDOperacion_Det and vh2.IDVoucher=@NuVoucher)          

  --then vo2.CoEstado Else vo.CoEstado End   
  then vo2.CoEstado Else (Isnull((select CoEstado from voucher_operaciones_det where IDVoucher=@NuVoucher and CoMoneda=@CoMoneda),0) ) End   

 Else

	ISNULL(vtr.CoEstado,'PD')

 End as CoEstado,          



 Case When @CoProveedor<>'001836' Then     

	 Case When od.IDVoucher=0 Or Exists(select vh2.IDVoucher from VOUCHER_HISTORICO vh2           

			 where vh2.IDOperacion_Det=od.IDOperacion_Det and vh2.IDVoucher=@NuVoucher)          

		then Isnull(vo2.SsSaldo,0) 

	 Else 

		--Isnull(vo.SsSaldo,0) 
		Isnull((select sssaldo from voucher_operaciones_det where IDVoucher=@NuVoucher and CoMoneda=@CoMoneda),0) 

	 End

 Else

	case when vtr.SsSaldo is null then

		(Select SUM(TotalGen) From OPERACIONES_DET Where IDOperacion=o.idoperacion)  

	else

		vtr.SsSaldo

	end

 End as SaldoPendiente,      
 
       

 Case When @CoProveedor<>'001836' Then     

	 Case When od.IDVoucher=0 Or Exists(select vh2.IDVoucher from VOUCHER_HISTORICO vh2           

			 where vh2.IDOperacion_Det=od.IDOperacion_Det and vh2.IDVoucher=@NuVoucher)          

		then Isnull(vo2.SsDifAceptada,0) 

	 Else 

		--IsNull(vo.SsDifAceptada,0) 
		Isnull((select SsDifAceptada from voucher_operaciones_det where IDVoucher=@NuVoucher and CoMoneda=@CoMoneda),0) 
	 End

 Else

	ISNULL(vtr.SsDifAceptada,0)

 End as SsDifAceptada,          



 Case When @CoProveedor<>'001836' Then  

	 Case When od.IDVoucher=0 Or Exists(select vh2.IDVoucher from VOUCHER_HISTORICO vh2           

			 where vh2.IDOperacion_Det=od.IDOperacion_Det and vh2.IDVoucher=@NuVoucher)          

		then Isnull(vo2.TxObsDocumento,'') 

	 Else 

		IsNull(vo.TxObsDocumento,'') 

	 End

 Else

	IsNull(vtr.TxObsDocumento,'') 

 End as TxObsDocumento      ,  

 p.IDTipoProv,
 
 --Case When @CoProveedor<>'001836' then vo.SsMontoTotal  else vtr.SsTotal end as SsMontoTotal 
 Case When @CoProveedor<>'001836' then Isnull((select SsMontoTotal from voucher_operaciones_det where IDVoucher=@NuVoucher and CoMoneda=@CoMoneda),0)   else vtr.SsTotal end as SsMontoTotal 

 --      

 ,RutaDocSustento=isnull((select top 1 RutaDocSustento from COTIDET  where IDServicio_Det=sd.IDServicio_Det and IDCab=c.IDCAB  order by RutaDocSustento desc),'')      

 ,o.IDOperacion,

 vtr.SsSaldo as SaldoPR

 FROM --OPERACIONES_DET od            

 VOUCHER_OPERACIONES vo Left Join OPERACIONES_DET od On vo.IDVoucher= od.IDVoucher          

 Left Join (select * from VOUCHER_HISTORICO where IDVoucher=@NuVoucher) vh On od.IDOperacion_Det=vh.IDOperacion_Det          
 Left Join VOUCHER_OPERACIONES vo2 On vh.IDVoucher= vo2.IDVoucher          
 Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion                                  
 Left Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor                             
 Left Join RESERVAS r On r.IDReserva=o.IDReserva        
 Left Join COTICAB c On c.IDCAB=o.IDCab                                  
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo                                   
 Left Join MAUSUARIOS us On c.IDUsuario=us.IDUsuario                                  
 Left Join MAUBIGEO uPro On p.IDCiudad = uPro.IDubigeo                                
 Left Join MASERVICIOS s On od.IDServicio = s.IDServicio                                
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det                  
 Left Join MACLIENTES cl On cl.IDCliente=c.IDCliente         
 Left Join MAUSUARIOS uv on c.IDUsuario=uv.IDUsuario                                          
 Left Join MAUSUARIOS uo On c.IDUsuarioOpe = uo.IDUsuario
Left Join OPERACIONES otren On otren.IDCab=c.IDCAB And otren.IDProveedor='001836'
Left Join VOUCHER_PROV_TREN vtr On vtr.NuVouPTrenInt=otren.NuVouPTrenInt

 --Where od.IDOperacion=@IDOperacion And od.IDVoucher<>0                                  

 --Where od.IDVoucher=@IDVoucher-- And od.IDVoucher<>0                           

 Where  --od.IDVoucher<>0     and                

-- (@IDFile='' or o.IDFile like '%'+@IDFile+'%') And                

 --(@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                 

 --and              

 ((@NuVoucher='0' AND @CoProveedor<>'001836') or (vh.IDVoucher=@NuVoucher Or od.IDVoucher=@NuVoucher Or vtr.NuVouPTrenInt=@NuVoucher)) --and                

 AND (c.IDCAB=@IDCab Or @IDCab=0)    

 --(@NuDocum='' or od.IDVoucher in (select distinct NuVoucher from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                

 --And o.IDProveedor=@IDProveedor        

 And (p.IDProveedor=@CoProveedor Or LTRIM(Rtrim(@CoProveedor))='')

 ) as X                                  

 order by  IDVoucher,                

 DescProveedor,RutaDocSustento desc            

