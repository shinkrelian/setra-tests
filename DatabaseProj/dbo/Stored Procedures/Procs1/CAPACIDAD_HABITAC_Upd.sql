﻿Create Procedure dbo.CAPACIDAD_HABITAC_Upd
@CoCapacidad char(2),
@NoCapacidad varchar(50),
@QtCapacidad tinyint,
@UserMod char(4)
As
	Set NoCount On
	Update CAPACIDAD_HABITAC
		Set NoCapacidad = @NoCapacidad,
			QtCapacidad = @QtCapacidad,
			UserMod = @UserMod,
			FecMod = Getdate()
	Where CoCapacidad = @CoCapacidad
