﻿Create Procedure dbo.COTIDET_Sel_ExistsAcomodoEspecial
@IDDet int,
@pAcomodoEspecial bit output
As
Set NoCount On
Set @pAcomodoEspecial = 0
Select @pAcomodoEspecial = AcomodoEspecial from COTIDET Where IDDET=@IDDet
