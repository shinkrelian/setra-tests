﻿Create Procedure dbo.MAENTREVISTAPROVEEDOR_Sel_Exists
@IDProveedor char(6),
@IDIdioma varchar(12),
@pExisteIdioma bit output
As
Set NoCount On
Set @pExisteIdioma = 0
If Exists(select FeEntrevista from MAENTREVISTAPROVEEDOR where IDProveedor=@IDProveedor And IDIdioma=@IDIdioma)
	Set @pExisteIdioma = 1
