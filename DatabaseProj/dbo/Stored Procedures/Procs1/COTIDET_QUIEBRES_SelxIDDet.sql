﻿CREATE Procedure [dbo].[COTIDET_QUIEBRES_SelxIDDet]
	@IDDET int
As
	Set Nocount On

	Select dq.*,cq.Pax,cq.Liberados,
	Case cq.Tipo_Lib When 'S' Then 'SIMPLE' When 'D' Then 'DOBLE' End AS Tipo_Lib
	From COTIDET_QUIEBRES dq Left Join COTICAB_QUIEBRES cq On dq.IDCAB_Q=cq.IDCAB_Q
	Where dq.IDDet=@IDDET
