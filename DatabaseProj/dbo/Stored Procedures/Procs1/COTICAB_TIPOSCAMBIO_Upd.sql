﻿Create Procedure dbo.COTICAB_TIPOSCAMBIO_Upd
 @IDCab int,
 @CoMoneda char(3),
 @SsTipCam decimal(6,3),
 @UserMod char(4)
As
	Set NoCount On
	Update COTICAB_TIPOSCAMBIO
		Set SsTipCam = @SsTipCam,
			UserMod = @UserMod,
			FecMod = Getdate()
	Where IDCab = @IDCab and CoMoneda = @CoMoneda
