﻿--alter
--HLF-20140416-isnull(u.Descripcion,'') as DescPais
--JHD-20141124- Usu_Perfil=(select nombre from MAUSUARIOS where IDUsuario=c.CoUserPerfilUpdate)
--JHD-20141229- Se muestran todos los campos de la tabla MACLIENTES para colocar isnull en donde sea necesario.
--JHD-20150116- Se agregó el campo FlProveedor
--JHD-20150408- Se agregó el campo NuPosicion
--JHD-20150427- ,CoUserPerfilUpdate=isnull(c.UserMod,'')
--JHD-20150427- Usu_Perfil=isnull((select nombre from MAUSUARIOS where IDUsuario=c.UserMod),'')
--JHD-20150513- ,c.FlTop=isnull(c.FlTop,cast(0 as bit))
CREATE Procedure [dbo].[MACLIENTES_Sel_Pk]  
 @IDCliente char(6)  
AS  
 Set NoCount On  
   
 Select --c.*,
 
 c.IDCliente
      ,c.Persona
      ,RazonSocial=isnull(c.RazonSocial,'')
      ,c.RazonComercial
      ,Nombre=isnull(c.Nombre,'')
      ,ApPaterno=isnull(c.ApPaterno,'')
      ,ApMaterno=isnull(c.ApMaterno,'')
      ,c.Domiciliado
      ,Ciudad=isnull(c.Ciudad,'')
      ,c.IDIdentidad
      ,NumIdentidad=isnull(c.NumIdentidad,'')
      ,c.Tipo
      ,c.Direccion
      ,Telefono1=isnull(c.Telefono1,'')
      ,Telefono2=isnull(c.Telefono2,'')
      ,Celular=isnull(c.Celular,'')
      ,Fax=isnull(c.Fax,'')
      ,Comision=isnull(c.Comision,0)
      ,Notas=isnull(c.Notas,'')
      ,c.IDCiudad
      ,c.IDvendedor
      ,Web=isnull(c.Web,'')
      ,CodPostal=isnull(c.CodPostal,'')
      ,c.Credito
      ,c.Activo
      ,c.UserMod
      ,c.FecMod
      ,c.MostrarEnReserva
      ,c.Complejidad
      ,c.Frecuencia
      ,c.Solvencia
      ,CoStarSoft=isnull(c.CoStarSoft,'')
      ,NuCuentaComision=isnull(c.NuCuentaComision,'')
      ,c.CoTipoVenta
      ,NuCuentaAnt=isnull(c.NuCuentaAnt,'')
      ,c.FlCtaCambiada
      ,FlTop=isnull(c.FlTop,cast(0 as bit))
      ,c.FePerfilUpdate
      --,CoUserPerfilUpdate=isnull(c.CoUserPerfilUpdate,'')
	  ,CoUserPerfilUpdate=isnull(c.UserMod,'')
      ,txPerfilInformacion=isnull(c.txPerfilInformacion,'')
      ,txPerfilOperatividad=isnull(c.txPerfilOperatividad,''),
 
 
 u.IDPais,--,cc.RazonSocial as DescRelacion    
 isnull(u.Descripcion,'') as DescPais,
 --Usu_Perfil=isnull((select nombre from MAUSUARIOS where IDUsuario=c.CoUserPerfilUpdate),'')
 Usu_Perfil=isnull((select nombre from MAUSUARIOS where IDUsuario=c.UserMod),'')
 ,FlProveedor=isnull(c.FlProveedor,CAST(0 as bit))
 ,NuPosicion=isnull(c.NuPosicion,0)
 From MACLIENTES c Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo  
 --Left Join MACLIENTES cc On c.Relacion=cc.IDCliente  
 --Left Join MAUBIGEO u2 On u.IDPais=u2.IDubigeo
 
 Where c.IDCliente = @IDCliente  

