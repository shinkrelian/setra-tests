﻿Create Procedure dbo.COTICAB_TIPOSCAMBIO_Del
 @IDCab int,  
 @CoMoneda char(3)
As
 Set NoCount On
 Delete from COTICAB_TIPOSCAMBIO
 where IDCab = @IDCab and CoMoneda = @CoMoneda
