﻿
CREATE PROCEDURE [dbo].[COTIDET_Update_Valor_Zicasso]
				@IDCab			int
AS
BEGIN
	DECLARE @IDProveedor_Zicasso char(6) = '003557',
			@IDServicio_Zicasso char(8)	= 'LAX00002',
			@Servicio_Det_Zicasso CHAR(4) = 'ZIC6'
	DECLARE @Tbl_ServDet Table(IDProveedorX char(6),
							   IDServicioX char(8),
							   IDServicio_DetX int);
	Set NoCount On
	INSERT INTO @Tbl_ServDet(IDProveedorX, IDServicioX, IDServicio_DetX)
	SELECT S.IDProveedor, D.IDServicio, D.IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
	WHERE TipoUsoSistema = @Servicio_Det_Zicasso AND S.IDProveedor = @IDProveedor_Zicasso AND D.IDServicio = @IDServicio_Zicasso
	Declare @pTotalParaZ numeric(10,2), @IDDET int = NULL
	Set NoCount On
	SET @IDDET = (SELECT TOP 1 IDDET FROM COTIDET D INNER JOIN @Tbl_ServDet T ON D.IDServicio_Det = T.IDServicio_DetX
				  AND IDProveedor = IDProveedorX AND IDServicio = IDServicioX WHERE IDCAB = @IDCab)
	IF ISNULL(@IDDET,0) <> 0
	BEGIN
		DECLARE @FechaOutC smalldatetime, @FechaOutD smalldatetime

		SELECT @FechaOutC = FechaOut FROM COTICAB WHERE IDCAB = @IDCab

		SELECT @pTotalParaZ = SUM(ISNULL(Total,0)), @FechaOutD = MAX(Dia)
		FROM COTIDET WHERE IDCAB = @IDCab AND IDDET <> @IDDET

		SET @pTotalParaZ = ISNULL(@pTotalParaZ,0)
		--SET @pTotalParaZ = ROUND(((@pTotalParaZ * 0.0638)),0)
		SET @pTotalParaZ = CEILING((@pTotalParaZ * 0.0638))

		IF @FechaOutD IS NOT NULL
		BEGIN
			IF @FechaOutC < @FechaOutD
				SET @FechaOutC = @FechaOutD
		END

		UPDATE COTIDET SET Dia = @FechaOutC, DiaNombre = DATENAME(dw,@FechaOutC), Total = @pTotalParaZ WHERE IDCAB = @IDCab AND IDDET = @IDDET
	END
END
