﻿
 --JRF-20130227-Agregar Filtro del estado            
--jrf-20130228-Agregar Columna de idusuarioOper            
--JRF-20130301-Mostrando el Estado.          
--HLF-20130916-Cambiando Convert(varchar,c.FechaOperaciones,103) as Fecha,         
--por subquery (Select top 1 dia From cotidet where idcab=c.idcab order by dia)        
--JRF-20131017- Agregando Filtro de Fechas Inicio de Cotizacion.      
--JRF-20131018-Agregando Filtro de Fechas por asignación de ejecutivo de Reservas.      
--JRF-20140325- Agregnado la columna DocWordVersion    
--JRF-HLF-20140514- Optimizar el --exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB x El IDCAB in (...)  
--JRF-20140519-Agregar la fecha de Asignación de Reservas
--JHD-20150523-Agregar las columnas IDUsuarioOper_Cusco y UsuarioOpe_Cusco
--JHD-20150425-Agregar la columna IDCliente
--JHD-20150716-Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,
--JHD-20150716- ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes,    
--JHD-20150716- uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe
--HLF-20151203-And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)        
--HLF-20160216-isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt, Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt
CREATE Procedure dbo.COTICAB_Sel_ListOperaciones                  
 @IDUsuario char(4),                  
 @Cotizacion char(8),                  
 @IDFile char(8),                  
 @DescCliente varchar(60),                  
 @Titulo varchar(100),                  
 @FilesAntiguos bit,                  
 @UsuarioNoAsignado bit ,              
 @NombrePax varchar(100),            
 @Estado char(1),      
 @FecIniciRango1 smalldatetime,                      
 @FecIniciRango2 smalldatetime,      
 @FecAsigReserRango1 smalldatetime,      
 @FecAsigReserRango2 smalldatetime,
 @FechaAsigOperacionesRango1 smalldatetime,      
 @FechaAsigOperacionesRango2 smalldatetime,
 @FlHistorico bit=1
As                  
 Set NoCount On                  
 Select UsuarioVen,UsuarioRes,CorreoEjecReservas,IDUsuarioOper,UsuarioOpe,
 IDUsuarioOper_Cusco=isnull(IDUsuarioOper_Cusco,'0000'),isnull(UsuarioOpe_Cusco,'') as UsuarioOpe_Cusco,
 Fecha,FechaAsigReserva,Cotizacion,IDFile, NuPedido , NuPedInt ,DescCliente,Titulo,      
 DocWordVersion,Ruta,Estado,IDCAB,NroPax    ,FecEntregaVentas  ,FechaAsigOperaciones,
 IDCliente
 from (          
 --Select uv.Nombre as UsuarioVen,
 Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,
 --ur.Nombre as UsuarioRes,                  
 ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes,
 IsNull(ur.Correo,'') as CorreoEjecReservas,                  
 uo.IDUsuario as IDUsuarioOper ,
 --uo.Nombre as UsuarioOpe                   
 uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe
 --
  ,uoc.IDUsuario as IDUsuarioOper_Cusco ,uoc.Nombre +' '+IsNull(uoc.TxApellidos,'') as UsuarioOpe_Cusco
 --
 ,         
 --Convert(varchar,c.FechaOperaciones,103) as Fecha,                  
 (Select Top 1 dia From COTIDET where IDCab=c.IDCab order by Dia) as Fecha,
 --Case When FechaAsigReserva is null then CAST('01/01/1900' as smalldatetime) else Cast(convert(char(10),FechaAsigReserva,103) as smalldatetime) End As FechaAsigReserva   ,            
 c.FechaAsigOperaciones as FechaAsigReserva ,
 c.FechaAsigOperaciones,
 c.Cotizacion,c.IDFile,
 isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt,  
 cl.RazonComercial as DescCliente, c.Titulo,                   
 '' As Ruta,                  
 c.Estado,                  
 c.IDCAB,                
 c.NroPax,      
 CAST(c.DocWordVersion as varchar(2)) as DocWordVersion   ,c.FechaEntregaKitPax as FecEntregaVentas 
 ,C.IDCliente
 From COTICAB c                    
 Left Join MAUSUARIOS uv on c.IDUsuario=uv.IDUsuario                  
 Left Join MAUSUARIOS ur on c.IDUsuarioRes=ur.IDUsuario                  
 Left Join MAUSUARIOS uo On c.IDUsuarioOpe = uo.IDUsuario        
 --
  Left Join MAUSUARIOS uoc On c.IDUsuarioOpe_Cusco = uoc.IDUsuario            
  --
 Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente 
  Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt                       
 Where       
 (c.Cotizacion Like '%'+ltrim(rtrim(@Cotizacion))+'%' Or ltrim(rtrim(@Cotizacion))='') And                  
 (c.IDUsuarioOpe=@IDUsuario Or ltrim(rtrim(@IDUsuario))='') And                  
 (ltrim(rtrim(@IDFile))='' Or c.IDFile like '%' + ltrim(rtrim(@IDFile)) + '%') And                  
 --Not c.IDFile Is Null And                  
 (cl.RazonSocial Like '%'+@DescCliente+'%' Or ltrim(rtrim(@DescCliente))='') And                  
 (c.Titulo Like '%'+@Titulo+'%' Or ltrim(rtrim(@Titulo))='') And                   
                   
 --c.Estado='A'                  
  (c.Estado=@Estado Or ltrim(rtrim(@Estado))='')            
 And (c.FechaOperaciones < GETDATE() Or @FilesAntiguos=0)                  
 And (@UsuarioNoAsignado=0 OR c.IDUsuarioOpe='0000')                  
 --And c.IDCAB In (select * from fnConsultarIdCabxNombrePax(@NombrePax))              
 --exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB         
 And c.IDCAB in (select px.IDCab from COTIPAX px Where px.IDCab=c.IDCAB                 
 And (px.Nombres+' '+px.Apellidos) Like '%'+@NombrePax+'%' Or ltrim(rtrim(@NombrePax))='')
 And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)) as X       
 Where ((LTRIM(rtrim(convert(char(10),@FecIniciRango1,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecIniciRango2,103)))='01/01/1900') Or      
   cast(convert(char(10),X.Fecha,103) as smalldatetime) between @FecIniciRango1 and @FecIniciRango2) And      
 ((LTRIM(rtrim(convert(char(10),@FecAsigReserRango1,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecAsigReserRango2,103)))='01/01/1900') Or      
  cast(convert(char(10),X.FechaAsigReserva,103) as smalldatetime) between @FecAsigReserRango1 and @FecAsigReserRango2) And
((LTRIM(rtrim(convert(char(10),@FechaAsigOperacionesRango1,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FechaAsigOperacionesRango2,103)))='01/01/1900') Or      
  cast(convert(char(10),X.FechaAsigOperaciones,103) as smalldatetime) between @FechaAsigOperacionesRango1 and @FechaAsigOperacionesRango2) 
 Order by  cast(X.Fecha as smalldatetime),X.Cotizacion --c.FechaOperaciones, Cotizacion                  

