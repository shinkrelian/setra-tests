﻿
CREATE PROCEDURE [dbo].[COTICAB_Sel_ObservacionVentas_Pk]
					@IDCAB int,
					@pObservacionVentas varchar(max) output
AS
BEGIN
	SET NOCOUNT ON
	Set @pObservacionVentas = ''
	SELECT @pObservacionVentas = ISNULL(ObservacionVentas,'') FROM COTICAB WHERE IDCAB = @IDCAB
END
