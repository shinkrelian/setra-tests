﻿
Create Procedure dbo.MAAEROPUERTO_SelCiudadOutput
	@CoAero char(3),
	@pCoCiudad char(3) output
As
	Set Nocount On
	
	Set @pCoCiudad=''
	
	Select @pCoCiudad=isnull(ub.CoCounter,'')
	From MAAEROPUERTO ae Inner Join MAUBIGEO ub On ub.IDubigeo=ae.CoUbigeo 
	Where ae.CoAero=@CoAero
	
