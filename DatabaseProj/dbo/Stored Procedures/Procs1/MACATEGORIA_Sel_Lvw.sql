﻿CREATE Procedure [dbo].[MACATEGORIA_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(10)
As
	Set NoCount On
	
	Select Descripcion	
	From MACATEGORIA
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IdCat<>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
