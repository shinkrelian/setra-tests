﻿create PROCEDURE COTICAB_ARCHIVOS_Sel_List_IDCAB
	@IDCAB int
AS
BEGIN
	Select
		IDArchivo,
 		IDCAB,
 		ca.Nombre,
 		RutaOrigen,
 		RutaDestino,
 		CoTipo,
		Tipo=case when CoTipo='VEN' THEN 'VENTAS'  when CoTipo='RES' THEN 'RESERVAS'
		 when CoTipo='OPE' THEN 'OPERACIONES' when CoTipo='FIN' THEN 'FINANZAS' ELSE '' END,
 		FlCopiado,
		ca.FecMod as FechaModificacion,
 		CoUsuarioCreacion,
		Usuario=u.Nombre +' '+u.TxApellidos
 	From dbo.COTICAB_ARCHIVOS ca
	--left join coticab c on ca.idcab=c.idcab
	left join mausuarios u on u.idusuario=ca.CoUsuarioCreacion
	Where 
		IDCAB = @IDCAB
END;
