﻿CREATE Procedure [dbo].[COTIDET_PAX_DelxIDCab]
	@IDCab	int
As
	Set NoCount On
	
	Delete From COTIDET_PAX Where 
	IDPax in (Select IDPax From COTIPAX Where IDCab=@IDCab)
