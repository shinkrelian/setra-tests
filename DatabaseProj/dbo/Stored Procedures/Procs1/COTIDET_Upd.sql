﻿
--HLF-20120726-Nuevo campo IncGuia          
--HLF-20120808-Nuevo parametro FueradeHorario          
--HLF-20120820-Nuevos campos CostoRealEditado, CostoRealImptoEditado, MargenAplicadoEditado, ServicioEditado          
--HLF-20120910-Nuevo Campo RedondeoTotal          
--JRF-20131001-Nuevo campo CalculoTarifaPendiente        
--HLF-20131203-Nuevo Campo AcomodoEspecial          
--HLF-20131209-case when acomodoespecial=1....      
--JRF-20140227-Aumento de Decimales para los Costos    
--JRF-20140822-Descomentar el cambio al actualizar el NroPax y NroLiberados.  
--HLF-20140828-Nuevos campos FlSSCREditado, FlSTCREditado
CREATE Procedure [dbo].[COTIDET_Upd]            
 @IDDet int,            
 @IDCAB int,            
 @Cotizacion char(8),            
 @Item numeric(6, 3),            
 @Dia smalldatetime,            
 @IDubigeo char(6),            
 @IDProveedor char(6),            
 @IDServicio char(8),            
 @IDServicio_Det int,            
 @IDidioma varchar(12),            
 @Especial bit,            
 @MotivoEspecial varchar(200),            
 @RutaDocSustento varchar(200),            
          
 @Desayuno bit,            
 @Lonche bit,            
 @Almuerzo bit,            
 @Cena bit,            
          
 @Transfer bit,            
 --@OrigenTransfer varchar(250),          
 --@DestinoTransfer varchar(250),          
 @IDDetTransferOri int,          
 @IDDetTransferDes int,          
          
          
 @CamaMat tinyint = 0,          
          
 @TipoTransporte char(1),            
          
 @IDUbigeoOri char(6),            
 @IDUbigeoDes char(6),            
          
 @NroPax smallint,            
 @NroLiberados smallint,            
 @Tipo_Lib char(1),            
          
 @CostoReal numeric(12,4),            
 @CostoRealAnt numeric(12,4),            
          
 @CostoLiberado numeric(12,4),            
 @Margen numeric(12,4),            
 @MargenAplicado numeric(6,2),            
 @MargenAplicadoAnt numeric(6,2),            
          
 @MargenLiberado numeric(12,4),            
 @Total numeric(8,2),            
 @RedondeoTotal numeric(12,4),            
 @TotalOrig numeric(8,2),            
 @SSCR numeric(12,4),            
 @SSCL numeric(12,4),            
 @SSMargen numeric(12,4),            
 @SSMargenLiberado numeric(12,4),            
 @SSTotal numeric(12,4),            
 @SSTotalOrig numeric(12,4)=0,            
 @STCR numeric(12,4),            
 @STMargen numeric(12,4),            
 @STTotal numeric(12,4),            
 @STTotalOrig numeric(12,4)=0,            
 @CostoRealImpto numeric(12,4),            
 @CostoLiberadoImpto numeric(12,4),            
 @MargenImpto numeric(12,4),            
 @MargenLiberadoImpto numeric(12,4),            
 @SSCRImpto numeric(12,4),            
 @SSCLImpto numeric(12,4),            
 @SSMargenImpto numeric(12,4),            
 @SSMargenLiberadoImpto numeric(12,4),            
 @STCRImpto numeric(12,4),            
 @STMargenImpto numeric(12,4),            
 @TotImpto numeric(12,4),            
 @Servicio varchar(max),            
 @Recalcular bit,          
 @ExecTrigger bit,          
 @IncGuia bit=0,          
 @FueradeHorario bit,          
 @CostoRealEditado bit,           
 @CostoRealImptoEditado bit,          
 @MargenAplicadoEditado bit,           
 @ServicioEditado bit,          
 @CalculoTarifaPendiente bit,        
 @AcomodoEspecial bit,      
 @FlSSCREditado bit, 
 @FlSTCREditado bit,
 @UserMod char(4)            
As            
 Set Nocount On            
 --Declare @CostoRealAnt numeric(12,4),@MargenAplicadoAnt numeric(6,2)            
 --Select @CostoRealAnt=CostoReal, @MargenAplicadoAnt=MargenAplicado             
 -- From COTIDET Where IDDet=@IDDet            
          
          
 UPDATE COTIDET SET                       
 Cotizacion=@Cotizacion            
 ,Item=@Item            
 ,Dia=@Dia            
 ,DiaNombre=DATENAME(dw,@Dia)            
 ,IDubigeo=@IDubigeo            
 ,IDProveedor=@IDProveedor            
 ,IDServicio=@IDServicio            
 ,IDServicio_Det=@IDServicio_Det            
 ,IDidioma=@IDidioma            
 ,Especial=@Especial            
 ,MotivoEspecial=Case When ltrim(rtrim(@MotivoEspecial))='' Then Null Else @MotivoEspecial End            
 ,RutaDocSustento=Case When ltrim(rtrim(@RutaDocSustento))='' Then Null Else @RutaDocSustento End            
          
 ,Desayuno=@Desayuno            
 ,Lonche=@Lonche            
 ,Almuerzo=@Almuerzo            
 ,Cena=@Cena            
 ,Transfer=@Transfer            
 --,OrigenTransfer=Case When ltrim(rtrim(@OrigenTransfer))='' Then Null Else @OrigenTransfer End          
 --,DestinoTransfer=Case When ltrim(rtrim(@DestinoTransfer))='' Then Null Else @DestinoTransfer End          
 ,IDDetTransferOri=Case When @IDDetTransferOri=0 Then Null Else @IDDetTransferOri End          
 ,IDDetTransferDes=Case When @IDDetTransferDes=0 Then Null Else @IDDetTransferDes End           
          
 ,CamaMat=@CamaMat          
          
 ,TipoTransporte=Case When ltrim(rtrim(@TipoTransporte))='' Then Null Else @TipoTransporte End            
 ,IDubigeoOri=@IDubigeoOri            
 ,IDubigeoDes=@IDUbigeoDes            
          
          
 ,NroPax=@NroPax      
 --case when acomodoespecial=1 Then NroPax Else @NroPax End      
 ,NroLiberados=Case When @NroLiberados=0 Then Null Else @NroLiberados End            
 --case when acomodoespecial=1 Then NroLiberados Else       
 -- Case When @NroLiberados=0 Then Null Else @NroLiberados End            
 --End       
 ,Tipo_Lib=Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End            
            
            
 ,CostoReal=@CostoReal            
 ,CostoRealAnt=Case When @CostoRealAnt=0 Then CostoRealAnt Else @CostoRealAnt End            
 ,CostoLiberado=@CostoLiberado            
 ,Margen=@Margen            
 ,MargenAplicado=@MargenAplicado            
 ,MargenAplicadoAnt=Case When @MargenAplicadoAnt=0 Then MargenAplicadoAnt Else @MargenAplicadoAnt End            
 ,MargenLiberado=@MargenLiberado            
 ,Total=@Total            
 ,RedondeoTotal=@RedondeoTotal           
 ,TotalOrig=@TotalOrig          
 ,SSCR=@SSCR            
 ,SSCL=@SSCL            
 ,SSMargen=@SSMargen            
 ,SSMargenLiberado=@SSMargenLiberado            
 ,SSTotal=@SSTotal            
 ,SSTotalOrig=@SSTotalOrig          
 ,STCR=@STCR            
 ,STMargen=@STMargen            
 ,STTotal=@STTotal            
 ,STTotalOrig=@STTotalOrig          
 ,CostoRealImpto=@CostoRealImpto           
 ,CostoLiberadoImpto=@CostoLiberadoImpto            
 ,MargenImpto=@MargenImpto            
 ,MargenLiberadoImpto=@MargenLiberadoImpto            
 ,SSCRImpto=@SSCRImpto            
 ,SSCLImpto=@SSCLImpto            
 ,SSMargenImpto=@SSMargenImpto            
 ,SSMargenLiberadoImpto=@SSMargenLiberadoImpto            
 ,STCRImpto=@STCRImpto            
 ,STMargenImpto=@STMargenImpto            
 ,TotImpto=@TotImpto                       
 ,Servicio=Case When ltrim(rtrim(@Servicio))='' Then Null Else @Servicio End            
 ,Recalcular=@Recalcular          
 ,ExecTrigger=@ExecTrigger          
 ,IncGuia=@IncGuia          
 ,FueradeHorario=@FueradeHorario          
           
 ,CostoRealEditado=@CostoRealEditado          
 ,CostoRealImptoEditado=@CostoRealImptoEditado          
 ,MargenAplicadoEditado=@MargenAplicadoEditado          
 ,ServicioEditado=@ServicioEditado          
 ,CalculoTarifaPendiente = @CalculoTarifaPendiente        
 ,AcomodoEspecial=@AcomodoEspecial      
 ,FlSSCREditado=@FlSSCREditado
 ,FlSTCREditado=@FlSTCREditado
 ,UserMod=@UserMod            
 ,FecMod=getdate()            
 WHERE IDDet=@IDDet             
  
