﻿--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-(u.IDPais=@CoPais Or LTRIM(RTRIM(@CoPais))='') And 
CREATE procedure [dbo].[MACONTACTOSPROVEEDOR_Sel_SbRpt]  
@Persona char(1),  
@IDTipoProv char(3),  
@IDTipoOper char(1),  
@IDCiudad char(6),  
@Domiciliado bit,
@CoPais char(6)=''  
as  
 Set NoCount On  
   
 select NroFila,IDProveedor,Nombre_Completo,Cargo,Celular,Email,Numero  
 from(  
 select ROW_NUMBER() over (order by Nombre_Completo) as NroFila ,y.*  
 from (
   select * from (  
	 select cc.IDProveedor,CAST(Titulo+' '+Nombres+' '+Apellidos as varchar) as Nombre_Completo,  
		  Cargo,cc.Celular,cc.Email,  
		  Numero=CAST(cc.Anexo+'-'+cc.Telefono as varchar) ,1 as Ord 
	 from MACONTACTOSPROVEEDOR cc   
	 Where cc.IDProveedor In (select pr.IDProveedor from MAPROVEEDORES pr  
	 Left Join MAUBIGEO u On pr.IDCiudad=u.IDubigeo  
	 Left Join MACONTACTOSPROVEEDOR cc On pr.IDProveedor=cc.IDProveedor  
	 where (pr.IDCiudad=@IDCiudad Or LTRIM(RTRIM(@IDCiudad))='') And   
		 (u.IDPais=@CoPais Or LTRIM(RTRIM(@CoPais))='') And   
		 (pr.Persona=@Persona Or LTRIM(RTRIM(@Persona))='') And   
		 (pr.Domiciliado=@Domiciliado) And pr.Activo='A' And  
		 (pr.IDTipoOper=@IDTipoOper Or LTRIM(rtrim(@IDTipoOper))='') And  
		 (pr.IDTipoProv=@IDTipoProv Or LTRIM(rtrim(@IDTipoProv))='')  
	 )
 Union
	Select *,2 as Ord from (
	select p.IDProveedor , p.NombreCorto As Nombre_Completo, 
		'GENERAL' as Cargo, Isnull(p.Email1,'') as Celular,Isnull(p.Email2,'') as Email,'' as Numero
	from MAPROVEEDORES p where ltrim(rtrim(Isnull(p.Email1,''))) <> '' or ltrim(rtrim(Isnull(p.Email1,''))) is not null
	and p.Activo ='A') as T
	Where T.Celular <> '' or T.Email <> ''  
 Union
	Select *,3 as Ord from (
		select p.IDProveedor , p.NombreCorto As Nombre_Completo, 
			'RESERVAS' as Cargo, Isnull(p.Email3,'') as Celular,Isnull(p.Email4,'') as Email,'' as Numero
		from MAPROVEEDORES p where ltrim(rtrim(Isnull(p.Email1,''))) <> '' or ltrim(rtrim(Isnull(p.Email1,''))) is not null
		and p.Activo ='A') as T
		Where T.Celular <> '' or T.Email <> ''  
 Union
	Select *,4 as Ord from (
		select p.IDProveedor , p.NombreCorto As Nombre_Completo, 
			'RECEPCIÓN' as Cargo, Isnull(p.EmailRecepcion1,'') as Celular,Isnull(p.EmailRecepcion2,'') as Email,'' as Numero
		from MAPROVEEDORES p where ltrim(rtrim(Isnull(p.Email1,''))) <> '' or ltrim(rtrim(Isnull(p.Email1,''))) is not null
		and p.Activo ='A') as T
		Where T.Celular <> '' or T.Email <> ''  
 Union
	Select *,5 as Ord from (
		select p.IDProveedor , p.NombreCorto As Nombre_Completo, 
			'ADMINISTRACIÓN' as Cargo, Isnull(p.EmailContactoAdmin,'') as Celular,'' as Email,'' as Numero
		from MAPROVEEDORES p where ltrim(rtrim(Isnull(p.Email1,''))) <> '' or ltrim(rtrim(Isnull(p.Email1,''))) is not null
		and p.Activo ='A') as T
		Where T.Celular <> '' or T.Email <> ''  
	) as y1
 ) as y  
 ) as x  
 order by x.Ord

