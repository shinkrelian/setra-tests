﻿--JRF-20140509-Permitir que guarde Null en el campo IDCab.  
--HLF-20141205-GETDATE(),
--HLF-20150521-@FeCorreo varchar(20)
--JRF-20150701-@CoAreaSaliente ...
--PPMG-20160111-@CoAreaSaliente char(3) = ''
--JRF-20160209-@NuPedInt int=0,
--JRF-20160329-Case When Ltrim(Rtrim(@CoCorreoOutlook))<>'' then ....
CREATE Procedure [dbo].[CORREOFILE_Ins]
 @IDCab int,     
 @CoUsrIntDe char(4),     
 @CoUsrPrvDe char(6),     
 @CoUsrCliDe char(6),      
 @NoCuentaDe varchar(150),    
 --@FeCorreo datetime,    
 @FeCorreo varchar(20),
 @CoTipoBandeja char(2),    
 @NoAsunto varchar(200),    
 @TxMensaje varchar(max),    
 @CoFormato char(1),    
 @QtAdjuntos tinyint,    
 @FlImportancia bit,     
 @CoCorreoOutlook varchar(255),
 @CoAreaSaliente char(3) = '',
 @NuPedInt int=0,
 @UserMod char(4),
 @pNuCorreo int output
AS
BEGIN
 Set Nocount On     
     
 Declare @NuCorreo int    
 Execute dbo.Correlativo_SelOutput 'CORREOFILE',1,10,@NuCorreo output    
     
 Insert Into .BDCORREOSETRA..CORREOFILE(     
 NuCorreo,    
 IDCab,     
 CoUsrIntDe,     
 CoUsrPrvDe,     
 CoUsrCliDe,      
 NoCuentaDe,    
 FeCorreo,    
 CoTipoBandeja,     
 NoAsunto,    
 TxMensaje,    
 CoFormato,    
 QtAdjuntos,    
 FlImportancia,     
 CoCorreoOutlook,    
 CoArea,
 NuPedInt,
 UserMod)    
 Values(    
 @NuCorreo,    
 Case When @IDCab = 0 Then Null Else @IDCab End,     
 Case When Ltrim(Rtrim(@CoUsrIntDe))='' Then Null Else @CoUsrIntDe End,     
 Case When Ltrim(Rtrim(@CoUsrPrvDe))='' Then Null Else @CoUsrPrvDe End,     
 Case When Ltrim(Rtrim(@CoUsrCliDe))='' Then Null Else @CoUsrCliDe End,     
 @NoCuentaDe,    
 --@FeCorreo,    
 GETDATE(),
 @CoTipoBandeja,     
 Case When Ltrim(Rtrim(@NoAsunto))='' Then Null Else @NoAsunto End,     
 @TxMensaje,    
 @CoFormato,    
 Case When @QtAdjuntos=0 Then Null Else @QtAdjuntos End,     
 @FlImportancia,     
 --@CoCorreoOutlook,    
 Case When Ltrim(Rtrim(@CoCorreoOutlook))<>'' then 
	Case When LEN(@CoCorreoOutlook)>44 And Not Exists(select NuCorreo from BDCORREOSETRA.DBO.CORREOFILE Where CoCorreoOutlook= SUBSTRING(@CoCorreoOutlook,1,44)And IDCab=@IDCab)
	Then SUBSTRING(@CoCorreoOutlook,1,44) else @CoCorreoOutlook End
  Else '' End ,
 Case When Ltrim(Rtrim(@CoAreaSaliente)) = '' then Null Else @CoAreaSaliente End,
 Case When @NuPedInt = 0 Then Null else @NuPedInt End,
 @UserMod)     
     
 Set @pNuCorreo=@NuCorreo    
END
