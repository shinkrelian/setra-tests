﻿
CREATE Procedure [dbo].[MACONTACTOSCLIENTE_Upd_Extranet]  
 @IDContacto char(3),  
 @IDCliente char(6),   
 
 @UsuarioLogeo varchar(80)='',  
 @PasswordLogeo varchar(max)='',  
 @UsuarioLogeo_Hash varchar(max),
-- @FechaPassword datetime,   
 @UserMod char(4)
AS
BEGIN
	SET @UsuarioLogeo = ltrim(rtrim(@UsuarioLogeo))
	IF @UsuarioLogeo <> ''
	BEGIN
		IF [dbo].[FnExisteUsuarioProvClie](@IDContacto, @IDCliente, 'C', @UsuarioLogeo)=1
		BEGIN
			RAISERROR ('Ya esiste el nombre del usuario a ingresar.', 16, 1);
			RETURN
		END
	END
 Set NoCount On
 UPDATE MACONTACTOSCLIENTE Set  
            UsuarioLogeo=Case When ltrim(rtrim(@UsuarioLogeo))='' Then Null Else @UsuarioLogeo End  
           ,PasswordLogeo=Case When ltrim(rtrim(@PasswordLogeo))='' Then Null Else @PasswordLogeo End  
           --,FechaPassword=Case When @FechaPassword='01/01/1900' Then Null Else @FechaPassword End  
		   ,UsuarioLogeo_Hash=Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
		   ,FechaHash=DATEADD(day,14, getdate())
           ,UserMod=@UserMod             
           ,FecMod=GETDATE()
		   ,FlAccesoWeb = 1 
     WHERE  
           IDContacto=@IDContacto And IDCliente=@IDCliente  
END;
