﻿Create Procedure dbo.DOCUMENTO_DET_TEXTO_Sel_List
@NuDocum char(10),
@IDTipoDoc char(3)
As
Set NoCount On
Select NuDetalle,NoTexto,SsTotal
from DOCUMENTO_DET_TEXTO
Where NuDocum = @NuDocum and IDTipoDoc = @IDTipoDoc
Order by NuDetalle
