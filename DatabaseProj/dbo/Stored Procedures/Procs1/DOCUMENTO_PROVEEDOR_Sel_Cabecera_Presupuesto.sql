﻿
--JHD-20150515-IDProveedor=isnull( p.IDProveedor,os.idpax),    
--JHD-20150515-DescProveedor=isnull(p.NombreCorto,(select isnull(cp.Titulo,'')+' '+isnull(cp.Apellidos,'')+', '+isnull(cp.Nombres,'') from cotipax cp where cp.idpax=os.idpax)),-- as DescProveedor,    
--JHD-20150615-CoEstadoFormaEgreso=isnull(os.CoEstadoFormaEgreso,''), 
--JHD-20150616-Isnull(os.SsSaldo_USD,0) as SaldoPendiente_USD,
--JHD-20150616-ISNULL(os.SsDifAceptada_USD,0) as SsDifAceptada_USD,     
--JHD-20150616-isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=os.NuPreSob AND IDMoneda='USD'),0) as SsMontoTotal_USD 
--JHD-20150616-,os.NuCodigo_ER
--HLF-20151223-and CoPago<>'TCK' as SsMontoTotal_USD , as SsMontoTotal
--HLF-20160107-Case When os.IDProveedor='000544' then...as SsMontoTotal, as SsMontoTotalUSD
--PPMG-20160328-Se agrego esta columna , c.CoTipoVenta
CREATE Procedure [dbo].[DOCUMENTO_PROVEEDOR_Sel_Cabecera_Presupuesto]  
@NuVoucher int    
As      
BEGIN
Set NoCount On      
Select c.IDFile,    
    
 --os.NuPreSobFile as IDVoucher,     
 Left(os.NuPreSobFile,4)+'-'+SUBSTRING(os.NuPreSobFile,5,8)+'-'+RIGHT(os.NuPreSobFile,2) as IDVoucher,   
IDProveedor=isnull( p.IDProveedor,os.idpax),    
 DescProveedor=isnull(p.NombreCorto,(select isnull(cp.Titulo,'')+' '+isnull(cp.Apellidos,'')+', '+isnull(cp.Nombres,'') from cotipax cp where cp.idpax=os.idpax)),-- as DescProveedor,    
 cl.RazonComercial as Cliente,    
 c.NroPax as Pax,    
 Isnull(c.NroLiberados,0) as Liberados,        
TipoPax=case when c.TipoPax='EXT' then 'EXTRANJERO' when c.TipoPax='NCR' then 'NACIONAL CON RUC' when TipoPax='NSR' then 'NACIONAL SIN RUC' else '' end,        
Tipo_lib=case when c.Tipo_Lib='S' then 'SIMPLE' when c.Tipo_Lib='D' then 'DOBLE' else 'NINGUNO' end,        
c.Titulo,    
uVent.Nombre as UsuarioVen,    
uOper.Nombre as UsuarioOpe,    
c.ObservacionVentas,    
c.ObservacionGeneral,    
c.FecInicio as FechaInicio,    
c.FechaOut as FechaFin,      
os.NuPreSob as NuOrdenServicio,      
os.CoEstado,    
CoEstadoFormaEgreso=isnull(os.CoEstadoFormaEgreso,''),   
Isnull(os.SsSaldo,0) as SaldoPendiente,    
Isnull(os.SsSaldo_USD,0) as SaldoPendiente_USD,    
ISNULL(os.SsDifAceptada,0) as SsDifAceptada,      
ISNULL(os.SsDifAceptada_USD,0) as SsDifAceptada_USD,      
isnull(os.TxObsDocumento,'') as TxObsDocumento,    
p.IDTipoProv,    
Case When os.IDProveedor='000544' then
	isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=os.NuPreSob AND IDMoneda='SOL' and CoPago='EFE'),0) 
else
	isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=os.NuPreSob AND IDMoneda='SOL' and CoPago<>'TCK'),0) 
End as SsMontoTotal,
Case When os.IDProveedor='000544' then
	isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=os.NuPreSob AND IDMoneda='USD' and CoPago='EFE'),0) 
else
	isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=os.NuPreSob AND IDMoneda='USD' and CoPago<>'TCK'),0) 
End as SsMontoTotal_USD
--,'' RutaDocSustento    
,RutaDocSustento=''--isnull((select top 1 RutaDocSustento from COTIDET  where IDServicio_Det in (select IDServicio_Det from ORDEN_SERVICIO_DET where NuOrden_Servicio=os.NuOrden_Servicio)   order by RutaDocSustento desc),'')    
,os.NuCodigo_ER
, c.CoTipoVenta
from PRESUPUESTO_SOBRE os Left Join COTICAB c On os.IDCab=c.IDCAB      
--Left Join RESERVAS R On os.IDReserva=r.IDReserva      
Left Join MAPROVEEDORES p On os.CoPrvGui=p.IDProveedor      
Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente      
Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario      
Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario      
where os.NuPreSob=@NuVoucher
END
