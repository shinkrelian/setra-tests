﻿CREATE Procedure dbo.DEBIT_MEMO_DET_Upd    
 @IDDebitMemo char(10),    
 @IDDebitMemoDet tinyint,    
 @Descripcion varchar(max),    
 @SubTotal numeric(8,2),    
 @TotalIGV numeric(8,2),    
 @Total numeric(8,2),    
 @CapacidadHab char(3),    
 @NroPax smallint,     
 @CostoPersona numeric(8,2),    
 @UserMod char(4)    
As    
 Set NoCount On    
 UPDATE [dbo].[DEBIT_MEMO_DET]    
    SET [Descripcion] = @Descripcion    
    ,[SubTotal] = @SubTotal    
    ,[TotalIGV] = @TotalIGV    
    ,[Total] = @Total    
    ,[CapacidadHab] = Case When ltrim(rtrim(@CapacidadHab)) = '' Then Null Else @CapacidadHab End    
    ,[NroPax] = @NroPax    
    ,CostoPersona=@CostoPersona  
    ,[UserMod] = @UserMod    
    ,[FecMod] = GetDate()    
  WHERE [IDDebitMemo] = @IDDebitMemo    
     And [IDDebitMemoDet] = @IDDebitMemoDet    
