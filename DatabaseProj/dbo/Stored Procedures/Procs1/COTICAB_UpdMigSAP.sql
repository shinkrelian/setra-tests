﻿Create Procedure dbo.COTICAB_UpdMigSAP
@IDCab int,
@FlFileMigA_SAP bit,
@UserMod char(4) 
As
Set NoCount On
Update COTICAB set FlFileMigA_SAP=@FlFileMigA_SAP,UserMod=@UserMod,FecMod=getdate()
where IDCAB=@IDCab
