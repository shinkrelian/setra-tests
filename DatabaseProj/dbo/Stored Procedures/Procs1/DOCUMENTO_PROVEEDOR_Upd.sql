﻿--JHD-20141028-Se agregaron campos de SsNeto,SsOtrCargos y SsDetraccion  
--JHD-20141029-Se agregaron campos de SSTipoCambio,SSTotal  
--JHD-20141029-Se cambio el nombre del campo de SSMonto por SSTotalOriginal  
--JHD-20141029-Se cambio el nombre del campo de SSTotal_Dolares por SSTotal  
--JHD-20141029-Se agrego campo de NuOrden_Servicio  
--JHD-20141030-Se agregaron campos de CoTipoOC,CoCtaContab  
--JHD-20141103-Se aumentó la longitud del parametro @NuDocum a varchar(15)  
--JHD-20141103-Se agrego campo de CoMoneda_Pago
--JRF-20141111-Nuevas columnas [SsTipoCambio_FEgreso,SsTotal_FEgreso]
--JHD-20141205-Se cambio el tipo de dato de @NuDocum a varchar(30)
--JHD-20141209-Se agrego campo de CoOrdPag
--JHD-20150518-Se agrego campo de NuSerie 
--JHD-20150601-Se agrego campo de CoCeCos 
--JHD-20150601-@CoCtaContab varchar(14)='',
--JHD-20150824-Nuevos campos faltantes
--JHD-20150826- Declare @NuDocum2 varchar(30)                              
				--Exec Correlativo_SelOutput 'DOCUMENTO_PROVEEDOR',1,10,@NuDocum2 output   
--JHD-20150826 -  [NuDocum] = case when FlCorrelativo=1 and @NuDocum='' then NuDocum else
				--case when rtrim(ltrim(nudocum))=rtrim(ltrim(@NuDocum)) then NuDocum else
			--		case when @NuDocum='' then @NuDocum2 else @NuDocum end
			--	 end 
			--   end,   
			  -- ,FlCorrelativo = case when rtrim(ltrim(nudocum))=rtrim(ltrim(@NuDocum)) then FlCorrelativo else
			  --  case when @NuDocum='' then cast(1 as bit) else cast(0 as bit) end
			  --  end
--JHD-20150826 - CoTipoDocSAP = @CoTipoDocSAP,
--JHD-20150827 -Se agrego el campo CoCtaContab_SAP varchar(14)
--JHD-20150831 -set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
--JHD-20150911 -select @CoMoneda_FEgreso=isnull(CoMoneda_FEgreso,'') from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv
--JHD-20150911-set @CoCtaContab_SAP= case when @CoMoneda in ('USD','SOL') then
									--isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
									--else
									--isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_FEgreso and CoProveedor=@CoProveedor_SAP),'')
									--end
--JHD-20150917-declare @CoTipoCompra char(2)='',@CoObligacionPago char(3),@NuFondoFijo int,@NuPreSob int
--JHD-20150917-select @CoObligacionPago=CoObligacionPago,@NuFondoFijo=NuFondoFijo,@NuPreSob=NuPreSob from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv
--JHD-20150917-set @CoTipoCompra= case when @CoObligacionPago='FFI' THEN isnull((select CoTipoFondoFijo from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo),'') ...
--JHD-20150917-CoTipoCompra=ISNULL(@CoTipoCompra,'*')
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Upd]  
 @NuDocumProv int,  
 @NuVoucher int=0,  
 @NuOrden_Servicio int=0,  
 --@NuDocum varchar(15),  
 @NuDocum varchar(30),  
 @CoTipoDoc char(3),  
 @FeEmision smalldatetime,  
 @FeRecepcion smalldatetime,  
 @CoTipoDetraccion CHAR(5),  
 @CoMoneda char(3),  
 @SsIGV numeric(8,2),  
 @SsNeto numeric(9,2),  
 @SsOtrCargos  numeric(9,2)=0,  
 @SsDetraccion numeric(9,2)=0,  
 @SSTotalOriginal numeric(9,2),  
 @SSTipoCambio decimal(6,3)=0,  
 @SSTotal numeric(9,2)=0,  
 --@CoEstado char(2),  
 @UserMod char(4),  
 @CoTipoOC char(3)='',  
 @CoCtaContab varchar(14)='',  
 @CoMoneda_Pago char(3)='',
 @SsTipoCambio_FEgreso numeric(6,3)=0,
 @SsTotal_FEgreso numeric(9,2)=0,
 @CoOrdPag int=0,
 @NuSerie varchar(10)='',
 @CoCeCos char(6)='',
 --
 @CoTipoFondoFijo char(3)='',
 @SSPercepcion numeric(12,2)=0,
 @TxConcepto varchar(max)='',
 @CoProveedor char(6)='',
 @IDCab int=0,
 @FeVencimiento datetime='01/01/1900',
 @CoTipoDocSAP tinyint=0,
 @CoFormaPago char(3)='',
 @CoTipoDoc_Ref char(3)='',
 @NuSerie_Ref varchar(10)='',
 @NuDocum_Ref varchar(30)='',
 @FeEmision_Ref datetime='01/01/1900',
 @CoCeCon char(6)='',
 @CoGasto varchar(12)=''

AS  
BEGIN  

 Declare @NuDocum2 varchar(30)                              
 Exec Correlativo_SelOutput 'DOCUMENTO_PROVEEDOR',1,10,@NuDocum2 output   

 declare @CoProveedor_SAP char(6)
declare @CoCtaContab_SAP varchar(14)
declare @CoTipoCompra char(2)='',@CoObligacionPago char(3),@NuFondoFijo int,@NuPreSob int

select @CoObligacionPago=CoObligacionPago,@NuFondoFijo=NuFondoFijo,@NuPreSob=NuPreSob from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv

declare @CoMoneda_FEgreso char(3)
select @CoMoneda_FEgreso=isnull(CoMoneda_FEgreso,'') from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv

--select @CoProveedor_SAP= case when @CoProveedor='' then '000000' else @CoProveedor end
select @CoProveedor_SAP= case when @CoProveedor not in('000467','000527','002315','002317')  then '000000' else @CoProveedor end

--set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_FEgreso and CoProveedor=@CoProveedor_SAP),'')
--set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')

set @CoCtaContab_SAP=
				case when @CoMoneda in ('USD','SOL') then
				isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda and CoProveedor=@CoProveedor_SAP),'')
				else
				isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_FEgreso and CoProveedor=@CoProveedor_SAP),'')
				end

set @CoTipoCompra= case when @CoObligacionPago='FFI' THEN isnull((select CoTipoFondoFijo from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo),'') -- 'CC'
					when @CoObligacionPago='PST' AND (SELECT TOP 1 NuCodigo_ER FROM PRESUPUESTO_SOBRE WHERE NuPreSob=@NuPreSob) is not null then 'ER'
					when @CoObligacionPago='PST' AND (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=@NuPreSob) is not null THEN 'ER'--'CC'
				   else (
							case when rtrim(ltrim(@CoTipoDoc)) in ('HTD','RH') then '05'
								 when rtrim(ltrim(@CoTipoDoc)) in ('RSP') then '04' 
							else
								case when (SELECT top 1 up.IDubigeo
											FROM MAPROVEEDORES p
											left join MAUBIGEO u on p.IDCiudad=u.IDubigeo
											left join MAUBIGEO up on u.IDPais=up.IDubigeo
											where p.IDProveedor=@CoProveedor)='000323' --peru
								then '01' else '02' end
							end
						)				
					end

--set @CoCtaContab_SAP=isnull((select top 1 CtaContable from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_SAP where CoTipodoc=@CoTipoDoc and cotipooc=@CoTipoOC and CoMoneda=@CoMoneda_Pago and CoProveedor=@CoProveedor_SAP),'')
      
 Update [dbo].[DOCUMENTO_PROVEEDOR]  
 Set  
  [NuVoucher] = Case When @NuVoucher=0 Then Null Else @NuVoucher End,  
  NuOrden_Servicio = Case When @NuOrden_Servicio=0 Then Null Else @NuOrden_Servicio End, 
  NuSerie=Case When Ltrim(Rtrim(@NuSerie))='' Then Null Else @NuSerie End, 
   --[NuDocum] = @NuDocum,  
   [NuDocum] = case when FlCorrelativo=1 and @NuDocum='' then NuDocum else
				case when rtrim(ltrim(nudocum))=rtrim(ltrim(@NuDocum)) then NuDocum else
					case when @NuDocum='' then @NuDocum2 else @NuDocum end
				 end 
			   end,  
   [CoTipoDoc] = @CoTipoDoc,  
   [FeEmision] = @FeEmision,  
   [FeRecepcion] = @FeRecepcion,  
   --[CoTipoDetraccion] = Case When @CoTipoDetraccion='' Then Null Else @CoTipoDetraccion End,  
   [CoTipoDetraccion] = @CoTipoDetraccion,  
   [CoMoneda] = @CoMoneda,  
   [SsIGV] = @SsIGV,  
   ssneto=@SsNeto,  
   SsOtrCargos=Case When @SsOtrCargos=0 Then Null Else @SsOtrCargos End,   
   SsDetraccion=Case When @SsDetraccion=0 Then Null Else @SsDetraccion End,   
   [SSTotalOriginal] = @SSTotalOriginal,  
   SSTipoCambio=Case When @SSTipoCambio=0 Then Null Else @SSTipoCambio End,  
   SSTotal=Case When @SSTotal=0 Then Null Else @SSTotal End,   
   CoTipoOC=Case When @CoTipoOC='' Then Null Else @CoTipoOC End,  
   CoCtaContab=Case When @CoCtaContab='' Then Null Else @CoCtaContab End,  
   CoMoneda_Pago=Case When @CoMoneda_Pago='' Then Null Else @CoMoneda_Pago End,  
   --[CoEstado] = @CoEstado,  
   SsTipoCambio_FEgreso= Case When @SsTipoCambio_FEgreso=0 then Null Else @SsTipoCambio_FEgreso End,
   SsTotal_FEgreso = Case When @SsTotal_FEgreso=0 then Null Else @SsTotal_FEgreso End,
   [UserMod] = @UserMod,  
   fecmod=GETDATE(),
   CoOrdPag= Case When @CoOrdPag=0 Then Null Else @CoOrdPag End,
   cocecos=Case When Ltrim(Rtrim(@CoCeCos))='' Then Null Else @CoCeCos End,
--NUEVOS CAMPOS
 CoTipoFondoFijo=Case When Ltrim(Rtrim(@CoTipoFondoFijo))='' Then Null Else @CoTipoFondoFijo End    ,
 SSPercepcion =Case When @SSPercepcion=0 Then Null Else @SSPercepcion End,
 TxConcepto=Case When Ltrim(Rtrim(@TxConcepto))='' Then Null Else @TxConcepto End    ,
 CoProveedor= Case When Ltrim(Rtrim(@CoProveedor))='' Then Null Else @CoProveedor End    ,
 IDCab = Case When @IDCab=0 Then Null Else @IDCab End,
 FeVencimiento = Case When @FeVencimiento='01/01/1900' Then getdate() Else @FeVencimiento End,
 CoTipoDocSAP = @CoTipoDocSAP, --Case When @CoTipoDocSAP=0 Then Null Else @CoTipoDocSAP End,
 CoFormaPago = Case When Ltrim(Rtrim(@CoFormaPago))='' Then Null Else @CoFormaPago End    ,
 CoTipoDoc_Ref = Case When Ltrim(Rtrim(@CoTipoDoc_Ref))='' Then Null Else @CoTipoDoc_Ref End    ,
 NuSerie_Ref = Case When Ltrim(Rtrim(@NuSerie_Ref))='' Then Null Else @NuSerie_Ref End    ,
 NuDocum_Ref = Case When Ltrim(Rtrim(@NuDocum_Ref))='' Then Null Else @NuDocum_Ref End    ,
 FeEmision_Ref = Case When @FeEmision_Ref='01/01/1900' Then getdate() Else @FeEmision_Ref End,
 CoCeCon = Case When Ltrim(Rtrim(@CoCeCon))='' Then Null Else @CoCeCon End    ,
 CoGasto  = Case When Ltrim(Rtrim(@CoGasto))='' Then Null Else @CoGasto End 

 ,FlCorrelativo = case when rtrim(ltrim(nudocum))=rtrim(ltrim(@NuDocum)) then FlCorrelativo else
					case when @NuDocum='' then cast(1 as bit) else cast(0 as bit) end
			   end  
 ,CoCtaContab_SAP=Case When Ltrim(Rtrim(@CoCtaContab_SAP))='' Then Null Else @CoCtaContab_SAP End,
 CoTipoCompra=ISNULL(@CoTipoCompra,'*')
  Where   
  [NuDocumProv] = @NuDocumProv  
END;  
