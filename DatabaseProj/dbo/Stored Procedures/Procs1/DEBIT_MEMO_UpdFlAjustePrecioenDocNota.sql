﻿
   
 Create Procedure dbo.DEBIT_MEMO_UpdFlAjustePrecioenDocNota
 @IDDebitMemo char(10),  
 @FlAjustePrecioenDocNota bit,  
 @UserMod char(4)  
 As  
 Set Nocount on  
   
 Update DEBIT_MEMO Set FlAjustePrecioenDocNota=@FlAjustePrecioenDocNota,  
  UserMod=@UserMod, FecMod=GETDATE()  
 Where IDDebitMemo=@IDDebitMemo  
   
   
