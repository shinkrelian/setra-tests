﻿
--HLF-20130208-Agregando parametro Orden      
--JRF-20130227-Agregando parametro Residente  
--JRF-20130515-Agregando Columnas [IDPaisResidencia,FecIngresoPais]  
--MLL-20140527-Agregado columnas  @TxTituloAcademico @FlTC
--JHD-20141009 Agregado columna NoShow
--HLF-20150318-Agregando ,@FlEntMP ,@FlEntWP ,@FlAutorizarMPWP
--PPMG-20151109-Se agrego el nuevo campo NumberAPT
--PPMG-20160428-Se aumento el lenght @NumIdentidad varchar(25)
CREATE Procedure [dbo].[COTIPAX_Ins]  
 @IDCab int,        
 @Orden smallint,   
 @IDIdentidad char(3),
 @NumIdentidad varchar(25),
 @Nombres varchar(50),    
 @Apellidos varchar(50),  
 @Titulo varchar(5),      
 @FecNacimiento smalldatetime,
 @IDNacionalidad char(6),
 @Peso Numeric(5,2) ,
 @ObservEspecial varchar(200) ,
 @Residente bit, 
 @IDPaisResidencia char(6)='',  
 @FecIngresoPais smalldatetime='01/01/1900',  
 @UserMod char(4),   
 @TxTituloAcademico varchar(5)=' ',
 @FlTC  bit ,
 @FlNoShow  bit =0,     

 @FlEntMP bit=0,
 @FlEntWP bit=0,
 @FlAutorizarMPWP bit=0,

 @NumberAPT varchar(20) = NULL,

 @pIDPax int Output        

AS
BEGIN
 Set NoCount On        


 INSERT INTO [COTIPAX]        
 ([IDCab]      
 ,[Orden]    
 ,[IDIdentidad]        
 ,[NumIdentidad] 
 ,[Nombres]  
 ,[Apellidos]
 ,[Titulo]   
 ,[FecNacimiento]        
 ,[IDNacionalidad]        
 ,[Peso]      
 ,[ObservEspecial]      
 ,[Total]         
 ,[SSTotal]    
 ,[STTotal]    
 ,[Residente]  
 ,[IDPaisResidencia]  
 ,[FecIngresoPais]  
 ,[UserMod]  
 ,TxTituloAcademico 
 ,FlTC
 ,FlNoShow
 ,FlEntMP
 ,FlEntWP
 ,FlAutorizarMPWP
 ,NumberAPT
 )        

 VALUES        

 (@IDCab, 
 @Orden,  
 @IDIdentidad        
 ,Case When ltrim(rtrim(@NumIdentidad))='' Then Null Else @NumIdentidad End        
 ,@Nombres
 ,@Apellidos        
 ,@Titulo        
 ,Case When @FecNacimiento='01/01/1900' Then Null Else @FecNacimiento End        
 ,@IDNacionalidad      
 ,Case When @Peso = 0 Then Null Else @Peso End      
 ,Case When @ObservEspecial = '' Then Null Else @ObservEspecial End      
 ,0        
 ,0       
 ,0       
 ,@Residente    
 ,@IDPaisResidencia  
 ,Case When @FecIngresoPais='01/01/1900' Then Null Else @FecIngresoPais End                  
 ,@UserMod
 ,@TxTituloAcademico 
 ,@FlTC  
 ,@FlNoShow 
 ,@FlEntMP
 ,@FlEntWP
 ,@FlAutorizarMPWP
 ,@NumberAPT
 )        

 Select Top 1 @pIDPax=IDPax From COTIPAX Order by IDPax Desc        
END

