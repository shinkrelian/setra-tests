﻿

--JRF-20140528-Agregar el filro por anio
CREATE Procedure [dbo].[CORREOFILE_SelExists]  
@IDOperador varchar(6),  
@IDTransportista varchar(6),  
@IDGuia varchar(6),  
@DescFecha varchar(50),  
@Anio int,
@pExisteMailEnviado bit output  
As  
Set NoCount On  
 Set @pExisteMailEnviado = 0  
 If Exists(select NuCorreo From .BDCORREOSETRA..CORREOFILE  
 where YEAR(FeCorreo)= @Anio and (NuCorreo In (select NuCorreo From .BDCORREOSETRA..DESTINATARIOSCORREOFILE where CoUsrPrvPara In(@IDOperador,@IDTransportista,@IDGuia))  
     Or LTRIM(rtrim(@IDOperador))='' Or LTRIM(rtrim(@IDTransportista))='' Or LTRIM(rtrim(@IDGuia))='')  
 and NoAsunto like '%'+@DescFecha+'%')  
  set @pExisteMailEnviado = 1  

