﻿--HLF-20140715-DECLARE @vcFecha VARCHAR(10)....
CREATE Procedure [dbo].[MAFECHASNAC_Del]  
	@Fecha smalldatetime,  
	@IDUbigeo char(6)  
As  
	Set NoCount On  

	DECLARE @vcFecha VARCHAR(10)=CONVERT(VARCHAR,@Fecha,103)
	If YEAR(@Fecha)=1900
		Set @vcFecha = SUBSTRING(@vcFecha,1,6)+'1900'
	   
	Delete From MAFECHASNAC  
	Where Fecha=@vcFecha And IDUbigeo=@IDUbigeo  
