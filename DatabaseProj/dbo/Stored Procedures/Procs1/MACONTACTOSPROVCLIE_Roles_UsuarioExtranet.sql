﻿
CREATE PROCEDURE [dbo].[MACONTACTOSPROVCLIE_Roles_UsuarioExtranet]
						@CodUsuario varchar(80)
AS
BEGIN
	/*
	001	ESTADO PAGOS
	002	ESTADO RESERVAS
	003	FEEDBACK
	004	INCIDENCIAS
	005	INFORMACION (ITENERARIO)
	006	TOUR CONDUCTOR
	007	ORDERNES DE SERVICIO
	*/
	DECLARE @TblReturn Table(IdMod		varchar(3),
							 NombreMod	varchar(80),
							 DescMod    varchar(1000),
							 slugMod	varchar(1000),
							 imagenMod	varchar(1000))
	DECLARE @TblFilter Table(IdMod		varchar(3))
	DECLARE @IDTipoProv char(3) = ''
	Set NoCount On
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('001', 'ESTADO PAGOS','','payments','estado-de-pagos')
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('002', 'ESTADO RESERVAS','','reservas','estado-de-reservas')
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('003', 'FEEDBACK','','feedback','feedback')
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('004', 'INCIDENCIAS','','incidencias','incidencias')
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('005', 'INFORMACION (ITENERARIO)','','info','itinerario')
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('006', 'TOUR CONDUCTOR','','tour-conductor','tour-de-conductor')
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('007', 'ORDERNES DE SERVICIO','','ordenes-servicio','ordenes-de-servicio')
	INSERT INTO @TblReturn(IdMod, NombreMod, DescMod, slugMod, imagenMod)	VALUES('008', 'TOURS','','tours','itinerario')

	IF EXISTS(SELECT Usuario FROM MACONTACTOSPROVEEDOR WHERE [Usuario] = @CodUsuario AND FlAccesoWeb = 1
			  UNION ALL
			  SELECT Usuario FROM MAPROVEEDORES WHERE [Usuario] = @CodUsuario AND FlAccesoWeb = 1)
	BEGIN
		IF EXISTS(SELECT Usuario FROM MACONTACTOSPROVEEDOR WHERE [Usuario] = @CodUsuario AND FlAccesoWeb = 1)
		BEGIN
			SELECT @IDTipoProv = P.IDTipoProv
			FROM MACONTACTOSPROVEEDOR AS C INNER JOIN MAPROVEEDORES AS P ON C.IDProveedor = P.IDProveedor
			WHERE C.[Usuario] = @CodUsuario AND C.FlAccesoWeb = 1
		END
		ELSE
		BEGIN
			SELECT @IDTipoProv = P.IDTipoProv
			FROM MAPROVEEDORES AS P
			WHERE P.[Usuario] = @CodUsuario AND P.FlAccesoWeb = 1
		END
		IF @IDTipoProv IN ('001','002','003','004')
		BEGIN
			INSERT INTO @TblFilter(IdMod)	VALUES('001')
			--INSERT INTO @TblFilter(IdMod)	VALUES('003')
		END
		IF @IDTipoProv = '005'
		BEGIN
			INSERT INTO @TblFilter(IdMod)	VALUES('001')
			--INSERT INTO @TblFilter(IdMod)	VALUES('003')
			--INSERT INTO @TblFilter(IdMod)	VALUES('007')
		END
		IF @IDTipoProv IN ('017','020')
		BEGIN
			INSERT INTO @TblFilter(IdMod)	VALUES('003')
		END
		IF @IDTipoProv = '021'
		BEGIN
			INSERT INTO @TblFilter(IdMod)	VALUES('001')
			--INSERT INTO @TblFilter(IdMod)	VALUES('003')
			--INSERT INTO @TblFilter(IdMod)	VALUES('006')
			--INSERT INTO @TblFilter(IdMod)	VALUES('007')
		END
		IF @IDTipoProv = '019'
		BEGIN
			INSERT INTO @TblFilter(IdMod)	VALUES('001')
		END
	END
	ELSE IF EXISTS(SELECT UsuarioLogeo FROM MACONTACTOSCLIENTE WHERE [UsuarioLogeo] = @CodUsuario AND FlAccesoWeb = 1)
	BEGIN
		--INSERT INTO @TblFilter(IdMod)	VALUES('002')
		--INSERT INTO @TblFilter(IdMod)	VALUES('003')
		--INSERT INTO @TblFilter(IdMod)	VALUES('004')
		--INSERT INTO @TblFilter(IdMod)	VALUES('005')
		INSERT INTO @TblFilter(IdMod)	VALUES('008')
	END
	SELECT R.IdMod, R.NombreMod, R.DescMod, R.slugMod, R.imagenMod
	FROM @TblReturn R INNER JOIN @TblFilter F ON R.IdMod = F.IdMod
END

