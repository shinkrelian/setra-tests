﻿Create Procedure dbo.CorreoHTMLEntradasMPWPOutput
	@IDCab int,
	@pvcHTML varchar(max) output
As
	Set Nocount on

	Declare @Nombres varchar(50), @Apellidos varchar(50), @Servicio varchar(max),
	@IDFile char(8)='', @Titulo varchar(100)=''
	Select @IDFile=ISNULL(IDFile,''),@Titulo=Titulo From COTICAB Where IDCAB=@IDCab

	Declare @vcSubTitulo varchar(100)='Entradas recientemente autorizadas del File: '+@IDFile+' - '+@Titulo
	
	Declare @vcHTML varchar(max)=''
	Declare @vcHTMLBase varchar(max)='<HTML>'
	+'<head runat="server">'
	+ '<style type="text/css">'
	
	+ ' .nuevoEstilo1'
	+ ' {font-family: "Century Gothic";'
	+ ' line-height: normal; border-style: groove; font-size: 13px; }'

	+ ' .style5'
	+ ' { width: 500px;'
	+ ' font-size: 13px;font-weight: bold; background-color: black; color: white;}'

	+ ' .style10'
	+ ' { font-size: 13px; text-align: center; font-family: "Century Gothic"; font-weight: bold; width: 30px; background-color: black; color: white;}'	

	+ '.styleTitulo { text-align: center; font-family: "Century Gothic"; '
	+' font-weight: bold; }'

	+'.styleTablaTotales {font-family: "Century Gothic"; font-size: 13px; padding-left: 25%;border-style: hidden;} '
	+'.styleTotales { width: 10px; font-size: 13px;font-weight: bold; background-color: black; color: white;} '
	+'.styleTotalesBlanco { width: 10px; font-size: 13px;font-weight: bold; background-color: white; color: white;} '
	+ '</style>'
	+ '</head>'
	+ '</html>'
	+ '<body>'
	+ '<form id="form1" runat="server">'
	+'<div class="styleTitulo">'


	Set @vcHTML = @vcHTMLBase  

	+@vcSubTitulo
	
	+'</div><br />'
	--+'<div class="styleTitulo">Files Nuevos</div><br />'
	
	+ '<table class="nuevoEstilo1">'
	+ '<tr>'
	+ '<td class="style5">Entrada</td>'
	+ '<td class="style5">Nombre Pax</td>'                
	+ '<td class="style5">Apellido Pax</td>'                
	+ '</tr>'

	Declare curEntradas cursor for
	SELECT Servicio, Nombres, Apellidos FROM
		(
		SELECT  Servicio, Nombres, Apellidos,
			Case When OkMP=1 Then FlEntMPGenerada Else 1 End as MPGenerada,
			Case When OkWP=1 Then FlEntWPGenerada Else 1 End as WPGenerada

		FROM
			(
			SELECT cd.Servicio, cp.Nombres, cp.Apellidos, 	
			cp.FlEntMP, cp.FlEntWP, cp.FlAutorizarMPWP, cdp.FlEntMPGenerada, cdp.FlEntWPGenerada,
			Case When cp.FlEntMP=1 And dbo.FnServicioMPWP(cd.IDServicio_Det) in ('CUZ00132','CUZ00133') And cp.FlAutorizarMPWP=1 Then 1 Else 0 End as OkMP,
			Case When cp.FlEntWP=1 And dbo.FnServicioMPWP(cd.IDServicio_Det) in ('CUZ00134','CUZ00133') And cp.FlAutorizarMPWP=1 Then 1 Else 0 End as OkWP
			FROM COTIDET cd inner join COTIDET_PAX cdp On cd.IDDET=cdp.IDDet 
			inner join COTIPAX cp On cdp.IDPax=cp.IDPax
			WHERE cd.IDCAB=@IDCab AND dbo.FnServicioMPWP(cd.IDServicio_Det) IN('CUZ00132','CUZ00133','CUZ00134')
			) AS X
		WHERE (OkMP=1 OR OkWP=1)
		) as Y
	WHERE (MPGenerada=0 OR WPGenerada=0)
	Order by Servicio,Apellidos, Nombres

	Declare @bHayDatos bit=0
	
	Open curEntradas
	Fetch Next From curEntradas Into  @Servicio, @Nombres, @Apellidos
	While @@FETCH_STATUS=0
		Begin

		Set @vcHTML = @vcHTML + '<tr>'
		 + '<td class="style2">'+isnull(@Servicio,'')+'</td>'
		 + '<td class="style2">'+@Nombres+'</td>'
		 + '<td class="style2" align="center">'+@Apellidos+'</td>'
		 + '</tr>' 

		Set @bHayDatos = 1
		Fetch Next From curEntradas Into  @Servicio, @Nombres, @Apellidos
		End
	Close curEntradas
	Deallocate curEntradas

	Set @vcHTML += '</table></form></body>'

	Set @pvcHTML=''
	If @bHayDatos = 1
		Set @pvcHTML=@vcHTML
	
		

