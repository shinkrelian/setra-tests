﻿

--HLF-20130625-Comentando actualización de IDFile y IDUsuarioRes, 
--agregando FecMod=GETDATE(), UserMod=@UserMod
CREATE Procedure dbo.COTICAB_UpdaPendientexIDCab  
 @IDCab int,
 @UserMod char(4)
As  
 Set Nocount On  
   
 UPDATE COTICAB 
 SET 
 --IDFile=NULL, 
 Estado='P',
 --IDUsuarioRes='0000'   
 FecMod=GETDATE(), UserMod=@UserMod
 WHERE IDCAB IN(@IDCab)  
  
  
  
