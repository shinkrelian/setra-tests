﻿--JHD-20151021- UnitPrice= 	 isnull(round(d.SsNeto,2),0), 
--JHD-20151021- LineTotal=  isnull(round(d.SsNeto,2),0),
--JHD-20151215-AccountCode=(CASE WHEN cast(convert(char(6), cc.FechaOut,112) as int)>cast(convert(char(6), D.FeEmision,112) as int) then
--HLF-20160126-case when CoObligacionPago='PTT' then 'EXE_IGV' else 
--JHD-20160208-CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),case when d.NuVoucher is not null then dbo.FnDevuelveIdFilePorVoucher(d.nuvoucher) else '00000000' end
CREATE PROCEDURE [dbo].[Documento_Proveedor_List_SAP_Lines_Multiple] --769
@nudocumprov int--=1392
as
BEGIN

select
ItemCode= '', --isnull((select cosap from maproductos where coproducto= dd.CoProducto),''),
ItemDescription='',
WarehouseCode='',
Quantity=1,
DiscountPercent=0,
--AccountCode=isnull(d.CoCtaContab,''),
AccountCode=(CASE WHEN cast(convert(char(6), cc.FechaOut,112) as int)>cast(convert(char(6), D.FeEmision,112) as int) then
				Case When d.CoMoneda = 'USD' Then '49711002' else '49711001' End
			Else
				isnull(d.CoCtaContab,'')
			End), 
--UnitPrice=abs(isnull(d.SSTotal,0))/*-abs(case when ((d.CoTipoDoc='RH' AND d.CoTipoOC='007') or  isnull(SsDetraccion,0)>0) then 1 else 0 end)-abs(isnull(d.SsOtrCargos,0))-abs(isnull(d.SSPercepcion,0))*/,
--UnitPrice= round(@ssNeto,2), --CASE WHEN @ssNeto_Dolares=0 THEN @ssNeto_Soles ELSE @ssNeto_Dolares END,
--UnitPrice= 	 isnull(round(d.SSTotal,2),0), 
UnitPrice= 	 isnull(round(d.SsNeto,2),0), 

--LineTotal=abs(isnull(d.SSTotal,0)),
--LineTotal= round(@ssNeto,2), --CASE WHEN @ssNeto_Dolares=0 THEN @ssNeto_Soles ELSE @ssNeto_Dolares END,
--LineTotal=  isnull(round(d.SSTotal,2),0),
LineTotal=  isnull(round(d.SsNeto,2),0),
--TaxCode=case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
TaxCode=--case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
			--case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
			case when CoObligacionPago='PTT' then 'EXE_IGV' else 
				CASE WHEN d.CoTipoOC='001' then 'IGV'
				WHEN d.CoTipoOC='012' then 'DMIX_IGV' 
				WHEN d.CoTipoOC='002' then 'DNGR_IGV' 
				WHEN d.CoTipoOC='003' then 'EXE_IGV' 
				WHEN d.CoTipoOC='011' then 'EXE_IGV' 
				else 'EXE_IGV' END
			end,
--TaxTotal=abs(isnull(d.SsIGV,0)),
TaxTotal= round(case when d.CoTipoOC in ('001','012','002','003') then abs(isnull(d.SsIGV,0)) else 0 end,2),
--WTLiable=case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
--WTLiable=case when ((d.CoTipoDoc='RH' AND d.CoTipoOC='007') or  isnull(SsDetraccion,0)>0) then 1 else 0 end,
WTLiable=case when ((d.CoTipoDoc in ('RH','HTD') AND d.CoTipoOC in ('007','008','009')) or  isnull(SsDetraccion,0)>0) then 1 else 0 end,--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
TaxOnly=cast(0 as bit),
ProjectCode='',
CostingCode=isnull(d.CoCeCos,''),
CostingCode2=isnull(d.CoCeCon,''),
--CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),'00000000'),
CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),case when d.NuVoucher is not null then dbo.FnDevuelveIdFilePorVoucher(d.nuvoucher) else '00000000' end),
CostingCode4='',
CostingCode5='',
U_SYP_CONCEPTO=d.NuDocumProv--,
--d.idcab
from DOCUMENTO_PROVEEDOR d
left join MAMONEDAS m on m.IDMoneda=d.CoMoneda
Left Join COTICAB cc On cc.IDCAB=d.IDCab
--inner join DOCUMENTO_PROVEEDOR_DET dd on d.NuDocumProv=dd.NuDocumProv
--left join MAPRODUCTOS p on p.CoProducto=dd.CoProducto
--left join MAALMACEN a on a.CoAlmacen=dd.CoAlmacen
where d.FlMultiple=1 and d.NuDocum_Multiple=@nudocumprov
--end
END;
