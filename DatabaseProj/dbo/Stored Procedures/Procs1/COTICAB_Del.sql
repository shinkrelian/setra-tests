﻿Create Procedure [dbo].[COTICAB_Del]
	@IDCab	int
As

	Set NoCount On	
	
	Delete From COTICAB Where IDCAB=@IDCab
