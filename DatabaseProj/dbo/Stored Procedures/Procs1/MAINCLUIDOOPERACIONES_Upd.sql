﻿--JRF-20130821-Nuevos campos FlInterno,NuOrden
CREATE Procedure dbo.MAINCLUIDOOPERACIONES_Upd  
@NuIncluido int,  
@FlInterno bit,
@NuOrden tinyint,
@NoObserAbrev varchar(100),  
@FlPorDefecto bit,  
@UserMod char(4)  
As  
 Set NoCount On  
 Update MAINCLUIDOOPERACIONES  
  set NoObserAbrev = @NoObserAbrev,  
   FlInterno = @FlInterno,
   NuOrden = @NuOrden,
   FlPorDefecto = @FlPorDefecto,  
   UserMod = @UserMod,  
   FecMod = GETDATE()  
 where NuIncluido = @NuIncluido  
