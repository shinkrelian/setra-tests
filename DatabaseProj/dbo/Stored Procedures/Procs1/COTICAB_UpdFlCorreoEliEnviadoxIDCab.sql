﻿Create Procedure dbo.COTICAB_UpdFlCorreoEliEnviadoxIDCab
@IDCab int,
@UserMod char(4)
As
Set NoCount On
Update COTICAB
	Set FlCorreoEliEnviado = 1,
		UserMod=@UserMod,
		FecMod=getdate()
where IDCAB=@IDCab
