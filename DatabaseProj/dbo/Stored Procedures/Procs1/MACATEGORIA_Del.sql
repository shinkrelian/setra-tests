﻿Create Procedure [dbo].[MACATEGORIA_Del]
	@IdCat char(3)	
As
	Set NoCount On
	
	Delete From MACATEGORIA
	Where IdCat=@IdCat
