﻿CREATE Procedure [dbo].[MAOPCIONES_Upd]
	@IDOpc char(6),
    @Nombre varchar(50),
    @Descripcion varchar(50),
    @EsMenu	bit,
    @UserMod char(4)
As
	Set NoCount On
	
	Update MAOPCIONES          
           Set 
           Descripcion=@Descripcion
           ,Nombre=@Nombre
           ,EsMenu=@EsMenu
           ,UserMod=@UserMod
           ,FecMod=GETDATE()
     Where
           IDOpc=@IDOpc
