﻿Create Procedure Dbo.DEBIT_MEMO_UpdDebitMemoResumen
@IDDebitMemoResumen char(10),
@IDDebitMemo char(10),
@UserMod char(4)
As
	Set NoCount On
	Update DEBIT_MEMO
		set IDDebitMemoResumen = case when ltrim(rtrim(@IDDebitMemoResumen))='' then Null Else @IDDebitMemoResumen End,
			UserMod = @UserMod,
			FecMod = GetDate()
	where IDDebitMemo = @IDDebitMemo
