﻿Create Procedure dbo.DEBIT_MEMO_RepDebitMemosPorCobrar
@IDCliente varchar(6),
@IDPais varchar(6),
@FecVencimientoIn smalldatetime,
@FecVencimientoOut smalldatetime
As
Set NoCount On
Select UPPER(DATENAME(DW,dm.FecVencim))+ ' ' + CONVERT(char(10),dm.FecVencim,103) as FechaGroup ,CLi.RazonComercial as DescCliente,
SUBSTRING(dm.IDDebitMemo,1,8)+' - '+ SUBSTRING(dm.IDDebitMemo,9,10) As IDDebitMemo,
dm.Titulo as Referencia,c.FecInicio,c.FechaOut,dm.Fecha as FechaCobro,dm.FecVencim,dm.Saldo as ImporteUSD,
ub.Descripcion as DescPais
from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab=c.IDCAB
Left Join MACLIENTES CLi on dm.IDCliente=CLi.IDCliente
Left Join MAUBIGEO ub On CLi.IDCiudad=ub.IDubigeo
where IDTipo= 'I' And IDEstado In ('PP','PD')
and (Ltrim(rtrim(@IDCliente))='' Or dm.IDCliente=@IDCliente)
and (Ltrim(rtrim(@IDPais))='' Or CLi.IDCiudad=@IDPais)
and (Ltrim(rtrim(Convert(Char(10),@FecVencimientoIn,103)))='01/01/1900' Or 
	 dm.FecVencim between @FecVencimientoIn and @FecVencimientoOut)
Order by dm.FecVencim,DescCliente
