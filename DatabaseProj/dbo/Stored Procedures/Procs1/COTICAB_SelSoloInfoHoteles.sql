﻿--JRF-20140318-No se permiten Guias '.. and cd.IncGuia = 0'
CREATE Procedure dbo.COTICAB_SelSoloInfoHoteles  
@IDCAB int  
As            
 Set NoCount On            
 select distinct ROW_NUMBER()Over(Partition By cd.IDProveedor order by cd.IDProveedor)  As Ord,  
 cd.Dia,        
 cd.IDProveedor,        
 u.Descripcion as Ciudad,        
 upper(p.NombreCorto) as Hotel,        
 p.Direccion + ISNULL('*Tel:' + p.TelefonoRecepcion1,'') + ISNULL(' - '+p.TelefonoRecepcion2,'') as DireccionTelefono ,          
         
 'Check In: '+Isnull(convert(char(5),s.HoraCheckIn,108),'')+' hrs'+'*'+            
 'Check Out: '+Isnull(convert(char(5),s.HoraCheckOut,108),'')+' hrs' + '*'+             
  case when sd.TipoDesayuno = '00' then '' else 'Breakfast: ' + Isnull(ds.Descripcion,'') +'*' End         
     
 as CheckINCheckOUT,         
         
 Isnull(p.Web,'') as SitioWeb            
 from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor            
 Left Join MAUBIGEO u on p.IDCiudad = u.IDubigeo             
 Left Join MASERVICIOS s on cd.IDServicio = s.IDServicio            
 Left Join MASERVICIOS_DET sd on cd.IDServicio_Det = sd.IDServicio_Det        
 Left Join MADESAYUNOS ds On sd.TipoDesayuno = ds.IDDesayuno        
 where (p.IDTipoProv = '001' Or (p.IDTipoProv='003' and sd.ConAlojamiento=1))
  and cd.IDCAB = @IDCAB and cd.IncGuia = 0
 Order By cd.Dia  
