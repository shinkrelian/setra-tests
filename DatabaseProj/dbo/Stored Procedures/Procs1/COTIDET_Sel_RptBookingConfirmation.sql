﻿--JRF-20141216-Considerar para los Hoteles sus desayunos-almuerzos-y otros.        
--JRF-20150105-Ingresar Sub-Query para obtener lo editado por Reservas        
--JRF-20150107-Mostrar las horas de los transportes relacionados.      
--JRF-20150116-Ajustar con Top(1) Por los casos de Peru Rail, en el que comparten un mismo IDDet.    
--JRF-20150116-No se debe Mostrar Guías, servicios Tips ni Gastos de Transferencia.    
--JRF-20150326-Servicios del TC. No deberían aparecer
--JRF-20150416-Verificar el servicio
--JRF-20150428-No se deben visualizar los servicios And ('CUZ00207','CUZ00208') de Setours Cusco
--JRF-20150518-No se deben mostrar datos de reservas
--JHD-20150713-Mostrar primero el servicio de la reserva y si no existe mostrar de la cotización
--JHD-20150714-,y.NombresGuias2
--JHD-20150714-,isnull(dbo.FnDevuelveGuias_ProgramacionxIDDet(@IDCab,sd.IDServicio_Det),isnull(dbo.FnDevuelve..
--JHD-20150715-case when dbo.FnDevuelveVehiculoxIDDet_Programacion(cd.IDDET,cd.IDSe...
--PPMG-20151002--Linea 76 (p.IDTipoProv='001' And sd.PlanAlimenticio=0)
--PPMG-20151002--Linea 80 al 104
--JHD-20160211-,NombresGuias2= case when isnull(dbo.FnDevuelveGuias_ProgramacionxIDDet(...
--JRF-20160219-...Case When Exists(select IDReserva_Det from RESERVAS_DET rd Where rd.Anulado=0 And rd.IDDet=cd.IDDET) Then
CREATE Procedure [dbo].[COTIDET_Sel_RptBookingConfirmation]
						@IDCab int,
						@IDIdioma varchar(12)
AS
SET NOCOUNT ON
Select
Y.ItemCotiDetalle2,
Y.CadenaDia2,
Y.NombreDia2,
Y.MesAnio2,
Y.DiaNombreLengOrig2,
Y.DiaServicio2,
Y.Hora2,
Y.Ciudad2,
Replace(Replace(Y.Servicio2,' - E1',''),' - E2','') As Servicio2,
Y.TipoServicio2,
Y.NombreVehiculo2,
Y.NroPax2,
Y.Voucher2,
Y.IDTipoProv2,
Y.ConAlojamiento2,

Y.IDDET2 ,
	Y.NombresPax2
	--,IDServicio_Det
	,y.NombresGuias2
from (        
	Select 
	cd.IDDET,
	sd.IDServicio_Det,
	Item as ItemCotiDetalle2,
	CONVERT(varchar(10),Dia,103)as CadenaDia2,        
	UPPER(datename(DW,Dia)) as NombreDia2,            
	cast(DAY(dia) as varchar(2))+ ' '+UPPER(DATENAME(m,dia))+' '+CAST(year(dia) as varchar(4)) as MesAnio2,            
	UPPER(datename(m,dia)) as DiaNombreLengOrig2,        
      
	Case When exists(select cv.ID from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET) Then      
	(select Top(1) Cast(convert(char(10),cd.Dia,103)+' '+SUBSTRING(IsNull(cv.Salida,'00:00'),1,5) as smalldatetime)      
	  from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET)      
	else      

	 Case When Exists(select IDReserva_Det from RESERVAS_DET rd Where rd.Anulado=0 And rd.IDDet=cd.IDDET) Then
		(select top(1) rd.Dia from RESERVAS_DET rd Where rd.Anulado=0 And rd.IDDet=cd.IDDET)
	  else
	Case When (p.IDTipoProv='001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv<>'001' and sd.ConAlojamiento=1) Then        
	 Cast(Convert(char(10),cd.Dia,103)+' 23:59' As smalldatetime)        
	Else cd.Dia End End --cd.Dia        
	End      
	as       
	DiaServicio2,       
      
	Case When exists(select cv.ID from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET) Then      
	(select Top(1) SUBSTRING(IsNull(cv.Salida,'00:00'),1,5)      
	  from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET)      
	else      

	 Case When Exists(select IDReserva_Det from RESERVAS_DET rd Where rd.Anulado=0 And rd.IDDet=cd.IDDET) Then
		(select top(1) CONVERT(char(5),rd.Dia,108) from RESERVAS_DET rd Where rd.Anulado=0 And rd.IDDet=cd.IDDET)
	  else
	Case When p.IDTipoProv='001' and PlanAlimenticio=0 then '' else --CONVERT(char(5),dia,108)        
	 Case When p.IDTipoProv='003' and sd.ConAlojamiento=1 Then CONVERT(char(5),cd.Dia,108)
	Else CONVERT(char(5),cd.Dia,108) End 
	End End End
	as Hora2,      
      
	ub.Descripcion as Ciudad2,   
	p.idtipoprov,sd.conalojamiento,     
	Case When (p.IDTipoProv='001' Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1)) Or p.IDTipoProv='002' Then
	--p.NombreCorto+' - '+cd.Servicio 
			Case When ((p.IDTipoProv='001' And sd.PlanAlimenticio=0) Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1)) Then dbo.FnDevuelveAcomodoBookingConfirmation(cd.IDDET,@IDCab)
			Else p.NombreCorto+' - '+sd.Descripcion  End 
		Else 
		 ----cd.Servicio    
		 --isnull((select top 1 servicio from RESERVAS_DET rd
			--inner join reservas r on rd.IDReserva=r.IDReserva
			--where r.idcab=@IDCab
			--and rd.IDServicio_Det=sd.IDServicio_Det
			--and rd.iddet=cd.iddet),isnull(cd.servicio,''))
			Case when cd.Transfer = 1 Then
			  isnull((select top 1 servicio from RESERVAS_DET rd
				inner join reservas r on rd.IDReserva=r.IDReserva
				where r.idcab=@IDCab and rd.IDServicio_Det=sd.IDServicio_Det
				and rd.iddet=cd.iddet and
				(((ISNULL(rd.IDDetTransferOri,0) = 0 and ISNULL(rd.IDDetTransferDes,0) > 0)
				 or
				 (ISNULL(rd.IDDetTransferOri,0) > 0 and ISNULL(rd.IDDetTransferDes,0) = 0))
				 or
				 (ISNULL(cd.IDDetTransferOri,0) = 0 and ISNULL(cd.IDDetTransferDes,0) = 0))
				 ),isnull(cd.servicio,''))
			Else
			 --sd.Descripcion    
			  isnull((select top 1 servicio from RESERVAS_DET rd
				inner join reservas r on rd.IDReserva=r.IDReserva
				where r.idcab=@IDCab
				and rd.IDServicio_Det=sd.IDServicio_Det
				and rd.iddet=cd.iddet),isnull(cd.servicio,''))
			End		 
		End           
		+ Case When p.IDTipoProv = '001' and sd.PlanAlimenticio=0 Then char(13)+dbo.FnDevuelveCadenaHotelServiciosAdicionales(cd.IDDET,@IDIdioma) else '' End    
	as Servicio2,    
	        
	Case When s.IDTipoServ='NAP' Then '-' Else s.IDTipoServ+ Case When idio.Siglas='---' Then '' Else ' ('+idio.Siglas+')' End End as TipoServicio2,            
	
	--dbo.FnDevuelveVehiculoxIDDet(cd.IDDET,cd.IDServicio_Det) As NombreVehiculo2,
	--isnull(dbo.FnDevuelveVehiculoxIDDet_Programacion(cd.IDDET,cd.IDServicio_Det),dbo.FnDevuelveVehiculoxIDDet(cd.IDDET,cd.IDServicio_Det)) As NombreVehiculo2,
	case when dbo.FnDevuelveVehiculoxIDDet_Programacion(cd.IDDET,cd.IDServicio_Det)='' then dbo.FnDevuelveVehiculoxIDDet(cd.IDDET,cd.IDServicio_Det) 
	else dbo.FnDevuelveVehiculoxIDDet_Programacion(cd.IDDET,cd.IDServicio_Det) end As NombreVehiculo2,
	
	Cast (cd.NroPax+ISNULL(cd.NroLiberados,0) as varchar)+' Pax' as NroPax2,'' as Voucher2,p.IDTipoProv as IDTipoProv2,sd.ConAlojamiento as ConAlojamiento2,
	cd.IDDET as IDDET2,    
  
	Case When ((p.IDTipoProv <> '001' and sd.ConAlojamiento=1) Or (p.IDTipoProv='001' and sd.PlanAlimenticio=0))     
	Then  
	 Case When ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0)=  
		  (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)) And   
		 ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0) > 0 And  
		  (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)>0)  
	 Then 1 else 0 End    
	Else     
	 IsNull((select Top(1) rd.FlServicioNoShow from RESERVAS_DET rd Where rd.IDDET=cd.IDDet and rd.IDServicio_Det=cd.IDServicio_Det and rd.Anulado=0),0)    
	End as ItemNoShow    ,

	Case When c.NroPax+IsNull(c.NroLiberados,0) <> cd.NroPax+IsNull(cd.NroLiberados,0) 
	Then dbo.FnDevuelveListaPaxxIDDet(cd.IDDet)
	else '' End as NombresPax2
	
	,NombresGuias2= case when isnull(dbo.FnDevuelveGuias_ProgramacionxIDDet(@IDCab,sd.IDServicio_Det),'')='' then 
	isnull(dbo.FnDevuelveGuias_ReservasxIDDet(@IDCab,cd.iddet,sd.IDServicio_Det),'') 
	else isnull(dbo.FnDevuelveGuias_ProgramacionxIDDet(@IDCab,sd.IDServicio_Det),'') end
	from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor            
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det            
	Left Join MAUBIGEO ub On cd.IDubigeo=ub.IDubigeo Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio            
	Left Join MAIDIOMAS idio On cd.IDidioma=idio.IDidioma    
	Left Join COTICAB c On cd.IDCab=c.IDCab
	Where cd.IDCAB=@IDCab and cd.IncGuia=0 --and Charindex('guide',Lower(cd.Servicio))=0 
	And Not cd.IDServicio_Det In (select IDServicio_Det from maservicios_Det where Estado='A'    
	And CharIndex('tips',Lower(Descripcion))>0) And cd.FlServicioTC=0
	And Not cd.IDServicio In ('CUZ00207','CUZ00208')) As Y    
	Where CharIndex('gastos de transferencia',Lower(Y.Servicio2))=0 and Y.ItemNoShow = 0
	
	Union
	
	Select 
		Y.ItemCotiDetalle2,Y.CadenaDia2,Y.NombreDia2,Y.MesAnio2,Y.DiaNombreLengOrig2,Y.DiaServicio2,Y.Hora2,Y.Ciudad2,
		Replace(Replace(Y.Servicio2,' - E1',''),' - E2','') As Servicio2,     
		Y.TipoServicio2,Y.NombreVehiculo2,Y.NroPax2,Y.Voucher2,Y.IDTipoProv2,Y.ConAlojamiento2,Y.IDDET2 ,Y.NombresPax2,
		--y.IDServicio_Det,
		y.NombresGuias2
	from (
	Select Item as ItemCotiDetalle2,CONVERT(varchar(10),Dia,103)as CadenaDia2,    sd.IDServicio_Det,    
		UPPER(datename(DW,Dia)) as NombreDia2,            
		cast(DAY(dia) as varchar(2))+ ' '+UPPER(DATENAME(m,dia))+' '+CAST(year(dia) as varchar(4)) as MesAnio2,            
		UPPER(datename(m,dia)) as DiaNombreLengOrig2,        
      
		Case When exists(select cv.ID from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET) Then      
		(select Top(1) Cast(convert(char(10),cd.Dia,103)+' '+SUBSTRING(IsNull(cv.Salida,'00:00'),1,5) as smalldatetime)      
		  from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET)      
		else      
		Case When (p.IDTipoProv='001' and sd.PlanAlimenticio=0) Or (p.IDTipoProv<>'001' and sd.ConAlojamiento=1) Then        
		 Cast(Convert(char(10),cd.Dia,103)+' 23:59' As smalldatetime)        
		Else cd.Dia  End --cd.Dia        
		End      
		as       
		DiaServicio2,       
      
		Case When exists(select cv.ID from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET) Then      
		(select Top(1) SUBSTRING(IsNull(cv.Salida,'00:00'),1,5)      
		  from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET)      
		else      
		Case When p.IDTipoProv='001' and PlanAlimenticio=0 then '' else --CONVERT(char(5),dia,108)        
		 Case When p.IDTipoProv='003' and sd.ConAlojamiento=1 Then CONVERT(char(5),dia,108)         
		Else CONVERT(Char(5),cd.Dia,108) End        
		End End      
		as Hora2,      
      
		ub.Descripcion as Ciudad2,        
		Case When (p.IDTipoProv='001' Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1)) Or p.IDTipoProv='002' Then
		--p.NombreCorto+' - '+sd.Descripcion 
		Case When (p.IDTipoProv='001' Or (p.IDTipoProv<>'001' And sd.ConAlojamiento=1)) Then dbo.FnDevuelveAcomodoBookingConfirmation(cd.IDDET,@IDCab)
			Else p.NombreCorto+' - '+sd.Descripcion  End 
		Else         
		 --sd.Descripcion    
		  isnull((select top 1 servicio from RESERVAS_DET rd
			inner join reservas r on rd.IDReserva=r.IDReserva
			where r.idcab=@IDCab
			and rd.IDServicio_Det=sd.IDServicio_Det
			and rd.iddet=cd.iddet),isnull(cd.servicio,''))

		End           
		+ Case When p.IDTipoProv = '001' and sd.PlanAlimenticio=0 Then char(13)+dbo.FnDevuelveCadenaHotelServiciosAdicionales(cd.IDDET,@IDIdioma) else '' End    
		as Servicio2,       
		     
		Case When s.IDTipoServ='NAP' Then '-' Else s.IDTipoServ+ Case When idio.Siglas='---' Then '' Else ' ('+idio.Siglas+')' End End as TipoServicio2,            
		--dbo.FnDevuelveVehiculoxIDDet(cd.IDDET,cd.IDServicio_Det) As NombreVehiculo2,
		case when dbo.FnDevuelveVehiculoxIDDet_Programacion(cd.IDDET,cd.IDServicio_Det)='' then dbo.FnDevuelveVehiculoxIDDet(cd.IDDET,cd.IDServicio_Det) else dbo.FnDevuelveVehiculoxIDDet_Programacion(cd.IDDET,cd.IDServicio_Det) end As NombreVehiculo2,
		Cast (cd.NroPax+ISNULL(cd.NroLiberados,0) as varchar)+' Pax' as NroPax2,'' as Voucher2,p.IDTipoProv as IDTipoProv2,sd.ConAlojamiento as ConAlojamiento2,cd.IDDET as IDDET2,    
  
		Case When ((p.IDTipoProv <> '001' and sd.ConAlojamiento=1) Or (p.IDTipoProv='001' and sd.PlanAlimenticio=0))     
		Then  
		 Case When ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0)=  
			  (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)) And   
			 ((select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0) > 0 And  
			  (select COUNT(*) from RESERVAS_DET rd where rd.IDDet=cd.IDDet and rd.Anulado=0 and rd.FlServicioNoShow=1)>0)  
		 Then 1 else 0 End    
		Else     
		 IsNull((select Top(1) rd.FlServicioNoShow from RESERVAS_DET rd Where rd.IDDET=cd.IDDet and rd.IDServicio_Det=cd.IDServicio_Det and rd.Anulado=0),0) 
		End as ItemNoShow    ,
		Case When c.NroPax+IsNull(c.NroLiberados,0) <> cd.NroPax+IsNull(cd.NroLiberados,0) 
		Then dbo.FnDevuelveListaPaxxIDDet(cd.IDDet)
		else '' End as NombresPax2,
		   (select count(*) from COTIDET_DETSERVICIOS cds2 Where cds.IDDET=cds2.IDDET) As NroItems,
		--isnull(dbo.FnDevuelveGuias_ProgramacionxIDDet(@IDCab,sd.IDServicio_Det),isnull(dbo.FnDevuelveGuias_ReservasxIDDet(@IDCab,cd.iddet,sd.IDServicio_Det),'')) as NombresGuias2
		NombresGuias2= case when isnull(dbo.FnDevuelveGuias_ProgramacionxIDDet(@IDCab,sd.IDServicio_Det),'')='' 
		then isnull(dbo.FnDevuelveGuias_ReservasxIDDet(@IDCab,cd.iddet,sd.IDServicio_Det),'') else 
		isnull(dbo.FnDevuelveGuias_ProgramacionxIDDet(@IDCab,sd.IDServicio_Det),'') end
	from COTIDET_DETSERVICIOS cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det
	Left Join COTIDET cd On cds.IDDET= cd.IDDET
	Left Join COTICAB c On cd.IDCAB=c.IDCAB
	Left Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor
	Left Join MAUBIGEO ub On cd.IDubigeo=ub.IDubigeo 
	Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
	Left Join MAIDIOMAS idio On cd.IDidioma=idio.IDidioma    
	Where cd.IDCAB=@IDCab And (sd.Almuerzo=1 Or sd.Cena=1 Or sd.Desayuno=1 Or sd.Lonche=1)
	
) as Y
Where Y.NroItems > 1
Order by  Y.DiaServicio2       
