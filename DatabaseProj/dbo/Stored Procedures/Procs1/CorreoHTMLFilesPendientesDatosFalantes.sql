﻿CREATE Procedure dbo.CorreoHTMLFilesPendientesDatosFalantes
@IDUsuarioRes char(4),
@FecINCusco smalldatetime,
@pHTML nvarchar(Max) output
As
Begin
	Declare @IDFileTemp char(8)='',@EjecutivoResTemp varchar(Max)='',@EjecutivoVenTemp varchar(Max)='',@CotizacionTemp char(8)='',
			@DescClienteTemp varchar(Max)='',@TituloTemp varchar(Max)='',@FecMAPITemp varchar(Max)='',@FecWaynaTemp varchar(max)=''
    Declare @vcSubTitulo varchar(100)='Files con datos incompletos'
	Declare @vcHTML nvarchar(max)=''
	Declare @vcHTMLBase nvarchar(max)='<HTML>'
	+'<head runat="server">'
	+ '<style type="text/css">'
	
	+ ' .nuevoEstilo1'
	+ ' {font-family: "Century Gothic";'
	+ ' line-height: normal; border-style: groove; font-size: 13px; }'

	+ ' .style5'
	+ ' { width: 500px;'
	+ ' font-size: 13px;font-weight: bold; background-color: black; color: white;}'

	+ ' .style10'
	+ ' { font-size: 13px; text-align: center; font-family: "Century Gothic"; font-weight: bold; width: 30px; background-color: black; color: white;}'	

	+ '.styleTitulo { text-align: center; font-family: "Century Gothic"; '
	+' font-weight: bold; }'

	+'.styleTablaTotales {font-family: "Century Gothic"; font-size: 13px; padding-left: 25%;border-style: hidden;} '
	+'.styleTotales { width: 10px; font-size: 13px;font-weight: bold; background-color: black; color: white;} '
	+'.styleTotalesBlanco { width: 10px; font-size: 13px;font-weight: bold; background-color: white; color: white;} '
	+ '</style>'
	+ '</head>'
	+ '</html>'
	+ '<body>'
	+ '<form id="form1" runat="server">'
	+ '<div class="styleTitulo">'
	+ @vcSubTitulo
	+ '</div>'
	+ '<br>'
	Set @vcHTML = @vcHTMLBase  
	+ '<table class="nuevoEstilo1">'
	+ '<tr>'
	+ '<td class="style5">Ejecutivo de Ventas</td>'                
	+ '<td class="style5">Ejecutivo de Reservas</td>'                
	+ '<td class="style5">Cotizaci&oacute;n</td>'                
	+ '<td class="style5">File</td>'
	+ '<td class="style5">Fecha MAPI</td>'
	+ '<td class="style5">Fecha Wayna</td>'
	+ '<td class="style5">Cliente</td>'
	+ '<td class="style5">T&iacute;tulo</td>'
	+ '</tr>'

	Declare curFsFalt cursor For
	Select Y.UserVentas As EjecutivoVentas,Y.UserReservas As EjecutivoReservas,Y.Cotizacion,Y.IDFile,Y.FecMAPI,Y.FecWayna,Y.DescCliente,Y.Titulo
	from (
		Select X.*, 
			   Case When DATEDIFF(D,Getdate(),X.FechaINCusco) > 0 Then 1 Else 0 End as EnviarMail
		from (
			Select dbo.FnDatosIncompletosPaxxFile(IDCab) As DatosIncompletos,c.FecInicio,
				   DATEADD(D,-45,Convert(char(10),(select Min(cd.Dia) From COTIDET cd Where cd.IDCAB=c.IDCAB And cd.IDubigeo='000066'),103)) as FechaINCusco,c.IDFile,
				   uVent.Nombre As UserVentas,uRes.Nombre As UserReservas,c.IDUsuarioRes,
				   C.Cotizacion,CL.RazonComercial As DescCliente,c.Titulo,
				   (Select Convert(Char(10),Min(CD2.Dia),103) from COTIDET cd2 Where cd2.IDCAB=c.IDCAB And dbo.FnServicioMPWP(cd2.IDServicio_Det)='CUZ00132') As FecMAPI,
				   (Select Convert(Char(10),Min(CD2.Dia),103) from COTIDET cd2 Where cd2.IDCAB=c.IDCAB And dbo.FnServicioMPWP(cd2.IDServicio_Det)='CUZ00134') As FecWayna
			from COTICAB c Left Join MAUSUARIOS uRes On c.IDUsuarioRes=uRes.IDUsuario
			Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario
			Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario
			Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente
			Where Estado='A' and uRes.Activo='A')
		As X
		Where X.DatosIncompletos=1 --And x.FechaINCusco=45
	) As Y
	Where y.EnviarMail=1 And Y.IDUsuarioRes=@IDUsuarioRes And Convert(Char(10),Y.FechaINCusco,103)=Convert(Char(10),@FecINCusco,103)
	Order By Y.IDUsuarioRes,Y.FechaINCusco asc
	Open curFsFalt
	Fetch Next From curFsFalt into @EjecutivoVenTemp,@EjecutivoResTemp,@CotizacionTemp,@IDFileTemp,@FecMAPITemp,@FecWaynaTemp,@DescClienteTemp,@TituloTemp
	Begin
		While @@FETCH_STATUS=0
		Begin
			Set @vcHTML = @vcHTML + '<tr>'
			+ '<td class="style2">'+@EjecutivoVenTemp+'</td>'
			+ '<td class="style2">'+@EjecutivoResTemp+'</td>'
			+ '<td class="style2">'+@CotizacionTemp+'</td>'
			+ '<td class="style2">'+@IDFileTemp+'</td>'
			+ '<td class="style2">'+IsNull(@FecMAPITemp,'')+'</td>'
			+ '<td class="style2">'+IsNull(@FecWaynaTemp,'')+'</td>'
			+ '<td class="style2">'+@DescClienteTemp+'</td>'
			+ '<td class="style2">'+@TituloTemp+'</td>'
			+ '</tr>'
			Fetch Next From curFsFalt into @EjecutivoVenTemp,@EjecutivoResTemp,@CotizacionTemp,@IDFileTemp,@FecMAPITemp,@FecWaynaTemp,@DescClienteTemp,@TituloTemp
		End
	End
	Close curFsFalt
	DealLocate curFsFalt


	Set @vcHTML += '</table></form></body>'
	Set @pHTML = @vcHTML
End
