﻿CREATE Procedure dbo.DOCUMENTO_DET_Sel_ListxNuDocumxTipoDoc    
@NuDocum char(10),    
@IDTipoDoc char(3)    
As    
Set NoCount On    
Select dd.NuDetalle,
    Case When dd.CoTipoDet = 'ML' Then NoServicio Else
    Isnull(p.NombreCorto,uTras.Nombre) End as Proveedor,
	dd.SsCompraNeto,dd.SsIGVSFE,dd.SsTotalCosto,dd.SsMargen,dd.SsTotalCostoUSD,dd.IDMonedaCosto,
	dd.SsTotalDocumUSD,dd.CoTipoDet  
  --CASE WHEN dd.IDTipoOC = '001' then    
  -- dd.SsTotalDocumUSD Else     
  --  CASE WHEN dd.IDTipoOC = '006' Then dd.SsTotalDocumUSD / 2 Else 0 End    
  -- End as FacturaExportacion,    
  --CASE WHEN dd.IDTipoOC = '001' then    
  -- 0 Else     
  --  CASE WHEN dd.IDTipoOC = '006' Then dd.SsTotalDocumUSD / 2 Else dd.SsTotalDocumUSD End End as BoletaNoExportacion    
from DOCUMENTO_DET dd Left Join MAPROVEEDORES p On dd.IDProveedor = p.IDProveedor    
Left Join MAUSUARIOS uTras On Ltrim(Rtrim(dd.IDProveedor))=uTras.IDUsuario    
Left Join MATIPOOPERACION toc on dd.IDTipoOC = toc.IdToc    
where dd.NuDocum = @NuDocum and dd.IDTipoDoc =@IDTipoDoc    
Order by dd.NuDetalle    
