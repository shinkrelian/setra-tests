﻿Create Procedure dbo.COTIDET_SelExisteAlojamiento
@IDProveedor char(6),
@IDCab int,
@pExisteAlojamiento bit Output
As
Set NoCount On
Set @pExisteAlojamiento = 0
Set @pExisteAlojamiento = dbo.FnExistenAlojamientoVentas(@IDProveedor,@IDCab)
