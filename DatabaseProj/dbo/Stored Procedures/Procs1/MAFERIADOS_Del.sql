﻿CREATE PROCEDURE [dbo].[MAFERIADOS_Del]
@FechaFeriado date,
@IDPais char(6)
as
begin
	set nocount on
	delete dbo.MAFERIADOS where FechaFeriado=@FechaFeriado and IDPais=@IDPais		
end
