﻿

--JHD-20150408 - cl.Solvencia
--JHD-20150408 - FlTop=isnull(cl.FlTop,CAST(0 as bit))
--JHD-20150408 - NuPosicion=isnull(cl.NuPosicion,0)
--JHD-20150408 - ,IDCliente=isnull(dm.IDCliente,'')
CREATE Procedure dbo.DEBIT_MEMO_SelxIDDebitMemo
@IDDebitMemo varchar(10)
As
	Set NoCount On
	Select Convert(Char(10),dm.Fecha,103) as FechaCobro,Convert(Char(10),dm.FecVencim,103) as FechaVencimiento,
		Isnull((select SUM(NroPax*Total) from COTIDET cd where cd.IDCab = dm.IDCab and cd.IDServicio in ('LIM00234','LIM00461')),0)
		as GastosTransfCoti,cl.RazonComercial as NombreCliente,dm.Titulo,
	    Case When dm.IDTipo = 'I' Then SUBSTRING(dm.IDDebitMemo,1,8)+'-'+SUBSTRING(dm.IDDebitMemo,9,10) Else dm.IDDebitMemo End As NroDebitMemo, dm.Total,dm.Saldo,
	    Isnull((select COUNT(IDDebitMemo) from INGRESO_DEBIT_MEMO idm Where idm.IDDebitMemo = @IDDebitMemo),0) as CantPagos,
	    Isnull((select sum(SsGastosTransferencia) from INGRESO_DEBIT_MEMO idm Where idm.IDDebitMemo = @IDDebitMemo),0) as SumaGastoTransfer
		,cl.Solvencia
		,FlTop=isnull(cl.FlTop,CAST(0 as bit))
		,NuPosicion=isnull(cl.NuPosicion,0)
		,IDCliente=isnull(dm.IDCliente,'')
	from DEBIT_MEMO dm Left Join MACLIENTES cl On dm.IDCliente = cl.IDCliente
	Where dm.IDDebitMemo = @IDDebitMemo

