﻿CREATE Procedure [dbo].[MACATEGORIA_Sel_Cbo]
	@bTodos	bit
As
	Set NoCount On
	
	SELECT * FROM
	(	
	Select '' as IdCat,'<TODOS>' as Descripcion, 0 as Ord
	Union
	Select IdCat, Descripcion,1 From MACATEGORIA
	) AS X
	Where (@bTodos=1 Or Ord<>0)
	Order by Ord,2
