﻿Create Procedure dbo.COTIDET_SelNroPaxOutPut
@IDDet int,
@pNroPax smallint output
As
Set NoCount On
Set @pNroPax = 0
Select @pNroPax = NroPax + IsNull(NroLiberados,0)
from COTIDET where IDDET =@IDDet
