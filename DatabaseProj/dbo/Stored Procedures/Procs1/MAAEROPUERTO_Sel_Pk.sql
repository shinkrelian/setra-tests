﻿


------------------------------------------------------------------------------------------------------------------



--[MAAEROPUERTO_Sel_Pk] '001'
CREATE Procedure [dbo].[MAAEROPUERTO_Sel_Pk]    
@CoAero char(3)
AS
BEGIN


SELECT      MAAEROPUERTO.CoAero,
			MAAEROPUERTO.NoDescripcion,
			MAAEROPUERTO.CoUbigeo,
			MAUBIGEO.Descripcion,
			coPais=MAUBIGEO.IDPais,
			Pais=(select Descripcion from maubigeo u where u.IDubigeo=MAUBIGEO.IDPais),
			MAAEROPUERTO.UserMod,
			MAAEROPUERTO.FecMod,
			MAAEROPUERTO.FlActivo
FROM         MAAEROPUERTO INNER JOIN
                      MAUBIGEO ON MAAEROPUERTO.CoUbigeo = MAUBIGEO.IDubigeo
WHERE  CoAero=@CoAero  

END;
