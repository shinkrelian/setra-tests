﻿Create Procedure [Dbo].[CorreosClientes] -- 1
@NroBloque smallint = 1
As
set nocount on
Declare @Correo varchar(200)
--Declare @NroBloque smallint = 1
Declare @FinBloque smallint = 450
Declare @vCorreos	varchar(max)=''
Declare @IniBetween smallint
Declare @FinBetween smallint=(@FinBloque*@NroBloque)
--If @NroBloque=1
--	Set @IniBetween =@NroBloque
--Else
	Set @IniBetween =(@FinBetween-@FinBloque)+1



	Declare curCorreosClientes cursor For
	SELECT ISnull(Correo,'') as Correo FROM
		(
		Select
		(row_number() over (order by c.idcliente)) as Nro,
		c.Email+'; ' as Correo
		FROM MACONTACTOSCLIENTE c Inner Join MACLIENTES m On c.IDCliente=m.IDCliente And m.Activo='A'
		WHERE c.EsAdminist=1
		) as X
	Where Nro Between @IniBetween and @FinBetween

	Open curCorreosClientes
	Fetch Next From curCorreosClientes Into @Correo

	While (@@FETCH_STATUS=0)
		Begin
		Set @Correo=REPLACE(@Correo,',',';')
		Set @Correo=REPLACE(@Correo,'/',';')
		Set @vCorreos=@vCorreos+@Correo
		Fetch Next From curCorreosClientes Into @Correo
		End
		
	Close curCorreosClientes
	Deallocate curCorreosClientes

	--select @IniBetween
	--select @FinBetween
	select @vCorreos As CorreosClientes
