﻿
Create Procedure dbo.COTIDET_UpdxTarifaCambiada
@IDServicio_Det int,
@UserMod char(4)
As
	Set NoCount On
	Update COTIDET 
		Set CalculoTarifaPendiente = 1,
			UserMod = @UserMod,
			FecMod = GETDATE()
	Where IDServicio_Det = @IDServicio_Det and CalculoTarifaPendiente = 0 and CostoRealEditado = 0
