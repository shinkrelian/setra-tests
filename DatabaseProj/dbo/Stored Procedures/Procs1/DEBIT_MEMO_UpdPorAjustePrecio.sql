﻿ 
 Create Procedure dbo.DEBIT_MEMO_UpdPorAjustePrecio
	@IDDebitMemo char(10),
	@FlPorAjustePrecio bit,
	@UserMod char(4)
 As
	Set Nocount on
	
	Update DEBIT_MEMO Set FlPorAjustePrecio=@FlPorAjustePrecio,
		UserMod=@UserMod, FecMod=GETDATE()
	Where IDDebitMemo=@IDDebitMemo
	
