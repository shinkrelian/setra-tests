﻿--DROP PROCEDURE MAFONDOFIJO_Upd_Activo
--GO
CREATE PROCEDURE MAFONDOFIJO_Upd_Activo
	@NuFondoFijo int=0,
	@FlActivo bit,
	@UserMod char(4)=''
AS
BEGIN
	Update [dbo].[MAFONDOFIJO]
	Set
		[FlActivo] = @FlActivo,
 		[UserMod] = @UserMod,
 		[FecMod] = getdate()
 	Where 
		[NuFondoFijo] = @NuFondoFijo
END;
