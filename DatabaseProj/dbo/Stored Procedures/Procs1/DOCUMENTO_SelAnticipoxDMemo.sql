﻿CREATE Procedure dbo.DOCUMENTO_SelAnticipoxDMemo
	@NuDebitMemo char(10),
	@pIDTipoDocNuDocumFeDocum nvarchar(25) output
As
	Set Nocount on

	Set @pIDTipoDocNuDocumFeDocum = ''

	Select @pIDTipoDocNuDocumFeDocum=IDTipoDoc+'|'+NuDocum+'|'+convert(varchar,FeDocum,103)
	From DOCUMENTO d      
	Where IDCab=(Select IDCab From DEBIT_MEMO Where IDDebitMemo=@NuDebitMemo) And       
	IDTipoDoc='BOL' And LEFT(NuDocum,3)='005'     
	And CoEstado='GD'      
	Group by IDTipoDoc,NuDocum,FeDocum   
