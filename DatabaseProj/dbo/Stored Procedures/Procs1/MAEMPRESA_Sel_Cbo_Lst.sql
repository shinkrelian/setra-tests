﻿--CREATE
CREATE PROCEDURE [dbo].[MAEMPRESA_Sel_Cbo_Lst]
@Todos bit
AS
BEGIN
Select '' as CoEmpresa,'<TODOS>' as Descripcion
Where @Todos = 1
Union
	Select
		[CoEmpresa],
 		[TxEmpresa] as Descripcion
 	
 	From [dbo].[MAEMPRESA]
	Where 
		 (FlActivo =1) 
END;
