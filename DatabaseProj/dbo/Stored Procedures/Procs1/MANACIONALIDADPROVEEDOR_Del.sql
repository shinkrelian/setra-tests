﻿


Create Procedure [dbo].[MANACIONALIDADPROVEEDOR_Del]
@IDubigeo Char(6) ,
@IDProveedor char(6) 
As
	Set NoCount On
	Delete from MANACIONALIDADPROVEEDOR 
		Where IDubigeo = @IDubigeo And IDProveedor = @IDProveedor
