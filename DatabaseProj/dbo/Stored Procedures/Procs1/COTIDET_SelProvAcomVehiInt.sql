﻿Create Procedure dbo.COTIDET_SelProvAcomVehiInt
	@IDCab int
As
	Set Nocount on
	
	Select distinct IDProveedor
	From
	(Select idproveedor,(Select COUNT(*) From ACOMODO_VEHICULO Where IDDet=cd.IDDET) as AcoVeh
	From COTIDET cd
	Where IDCAB=@IDCab
	) as X
	Where X.AcoVeh>0
