﻿--JRF-20130918-Agregar el CostoTotal    
--JRF-20140310-Se debe considerar los nombres escritos en los debit Memos.
CREATE Procedure dbo.DEBIT_MEMO_Sel_RptxIDCab    
@IDCab int    
As    
Set NoCount On    
select X.DescCliente,X.Titulo,X.IDFile,X.NombreBanco,X.FechaIN,X.Responsable,X.FechaOUT,X.NroPax,X.TipoCambio,    
X.Margen,X.Correlativo,X.Fecha,X.TotalDebitMemo,X.ClienteDebitMemo,X.FecPago,X.Monto,X.Comision,X.GTransfer,    
--X.TotalSimple+X.TotalDoble+X.TotalTriple as TotalCotiz ,   
ROUND(((X.TotalCmpTotal - X.TotalImpto-X.TotalCostoReal-X.TotalLib)/X.TotalCmpTotal)* 100,2) as MargenC1,  
ROUND(((X.TotalCmpTotal - X.TotalImpto-X.TotalCostoReal-X.TotalLib)/X.TotalCmpTotal),4) As CalculDebit  
from(    
select cl.RazonComercial As DescCliente,c.Titulo,c.IDFile,    
 b.Descripcion as NombreBanco,Convert(Char(10),c.FecInicio,103) as FechaIN,uVentas.Nombre as Responsable,    
 Convert(Char(10),c.FechaOut,103) as FechaOUT,c.NroPax,isnull(c.TipoCambio,0) as TipoCambio,    
 Isnull(c.Margen,0) as Margen,SUBSTRING(db.IDDebitMemo,9,10) As Correlativo,Convert(Char(10),db.Fecha,103) as Fecha,    
 db.Total as TotalDebitMemo,--cl.RazonComercial 
 db.Cliente as ClienteDebitMemo, CONVERT(Char(10),db.FecVencim,103) as FecPago,    
 0 as Monto, 0 as Comision,0 as GTransfer,    
 (select SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB)*((Isnull(c.Twin,0)+Isnull(c.Matrimonial,0))) *2 as TotalDoble,    
 (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB)* Isnull(c.Simple,0) as TotalSimple,    
 (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = c.IDCAB)* Isnull(c.Triple,0)* 3 as TotalTriple,    
 (select SUM(TotImpto) from COTIDET cd where cd.IDCAB = c.IDCAB) as TotalImpto,    
 (select SUM(CostoReal) from COTIDET cd where cd.IDCAB = c.IDCAB) as TotalCostoReal,    
 (select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = c.IDCAB) as TotalLib  ,  
 (select SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB) as TotalCmpTotal  
from DEBIT_MEMO db Left Join COTICAB c On db.IDCab =c.IDCAB    
Left Join MACLIENTES cl On db.IDCliente = cl.IDCliente    
Left Join MABANCOS b On db.IDBanco = b.IDBanco    
Left Join MAUSUARIOS uVentas On c.IDUsuario = uVentas.IDUsuario    
where db.IDCab = @IDCab and db.IDEstado <> 'AN') as X    
