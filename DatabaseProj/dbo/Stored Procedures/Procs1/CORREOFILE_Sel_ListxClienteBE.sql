﻿
--HLF-20151214-And (EXISTS(Select CoUsrIntPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 In ...
--or ltrim(rtrim(@CoUsrCliDe))='')
--JRF-20160107-Agregar los registros de la relacion vía Outlook.
--JRF-20160208-Agregar filtro @NoAsunto
--JRF-20160209-@NoAsuntoCuerpo | Agregar @NuPedInt
--HLF-20160213-((c.IDCab=@IDCab and @IDCab<>0 and @NuPedInt=0) or (c.idcab in (select idcab from coticab where NuPedInt=@NuPedInt) and @NuPedInt<>0 and @IDCab=0)) And
CREATE Procedure [dbo].[CORREOFILE_Sel_ListxClienteBE]
	@IDCab	int,
	@NuPedInt int,
	@CoUsrCliDe	char(6),
	@CoArea char(2),
	@NoAsuntoCuerpo varchar(200)
As	
	Set NoCount On
	
	Select '' as IconoCorreo, '' as ImgAdjuntos,
	c.NuCorreo,
	isnull(QtAdjuntos,0) As CantAdjuntos,
	CoCorreoOutlook as EntryID, NoCuentaDe as De, 
	dbo.FnDestinatariosCorreo(c.NuCorreo, 0) as Para,
	NoAsunto as Asunto, 
	Case When CoFormato='T' Then TxMensaje Else '' End as Body,
	Case When CoFormato='H' Then TxMensaje Else '' End as BodyHtml,
	dbo.FnDestinatariosCorreo(c.NuCorreo, 1) as ConCopia,
	FlImportancia as Importance,
	FlLeido as EstadoLeido,
	CoFormato as BodyFormat,
	dbo.FnAdjuntosCorreo(c.NuCorreo) as RutasAdjuntos,
	c.FeCorreo as Fecha
	From .BDCORREOSETRA..CORREOFILE c
	Where --(IDCab=@IDCab Or (IsNull(NuPedInt,0)=@NuPedInt And c.CoArea is not null)) And
	((c.IDCab=@IDCab and @IDCab<>0 and @NuPedInt=0) or (c.idcab in (select idcab from coticab where NuPedInt=@NuPedInt) and @NuPedInt<>0 and @IDCab=0)
	or (c.IDCab is null and c.NuPedInt=@NuPedInt and @NuPedInt<>0 and @IDCab=0)
	or (c.IDCab is null and @idcab in (select idcab from coticab where NuPedInt=c.NuPedInt) and @NuPedInt=0 and @IDCab<>0)
	) And
	(CoUsrCliDe=@CoUsrCliDe or ltrim(rtrim(@CoUsrCliDe))='') And
	((NoAsunto Like '%'+@NoAsuntoCuerpo+'%' Or TxMensaje Like '%'+@NoAsuntoCuerpo+'%') Or LTRIM(RTRIM(@NoAsuntoCuerpo))='') And
	CoTipoBandeja='BE'
	And ((EXISTS(Select dc1.CoUsrIntPara From .BDCORREOSETRA..DESTINATARIOSCORREOFILE dc1 Inner Join MAUSUARIOS u1 On dc1.CoUsrIntPara=u1.IDUsuario 
		And u1.IdArea=@CoArea And dc1.NuCorreo=c.NuCorreo) Or ltrim(rtrim(@CoArea))='') Or c.CoArea = 'VNT')

