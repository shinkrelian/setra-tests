﻿Create Procedure dbo.INGRESO_DEBIT_MEMO_SelExistexNuIngresoxIDDebitMemo
@NuIngreso int,
@IDDebitMemo char(10),
@pExiste bit output
As
Begin
	Set NoCount On
	set @pExiste = 0
	if Exists(select NuAsignacion from INGRESO_DEBIT_MEMO where NuIngreso =@NuIngreso and IDDebitMemo =@IDDebitMemo)
	Begin
		Set @pExiste = 1
	End
End
