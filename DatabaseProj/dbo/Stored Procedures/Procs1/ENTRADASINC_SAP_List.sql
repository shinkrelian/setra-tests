﻿--JHD-20160125- case MAPIWAYNRAQCCODE when 'CUZ00145' then 'RAQCHI' when 'CUZ00134' then 'WAYNA' when 'CUZ00133' then 'WAYNA' else 'MAPI' END AS Tipo ,
--JHD-20160126- left outer join ENTRADASINC_SAP e on Y.IDFile=e.IDFile and Y.Tipo=e.Tipo and Y.IDDET=e.IdDet
--JHD-20160126- isnull(e.NuCodigo_ER,'') as NuCodigo_ER,
--JHD-20160126-Monto=cast (Y.Monto as int),
--JHD-20160127-Se cambio la consulta para el mostrar solo un servicio cuando exista mas de uno en un solo dia.
--JHD-20160127-left outer join ENTRADASINC_SAP e on Y.IDFile=e.IDFile and Y.IDDET=e.IdDet
--JHD-20160129-Monto=CAST(isnull((select monto from ENTRADASINC_SAP where IdDet=t.iddet and tipo <> 'CONSETTUR' ),Monto) AS INT),
--JHD-20160129-from #tabla t
--JHD-20160202 - ,0 as NuDetPSo
--JHD-20160202 - left outer join ENTRADASINC_SAP e on Y.IDFile=e.IDFile and Y.IDDET=e.IdDet and e.Tipo not in (	'CONSETTUR','BTG')
--JHD-20160208 - case when exists(select IDDET from COTIDET cd2 where cd2.idcab=cd.IDCAB and dbo.FnServicioMPWP(cd2.IDServicio_Det)='CUZ00946' AND CD2.IDDET<>cd.IDDET) then 'CUZ00905' ELSE dbo.FnServicioMPWP(cd.IDServicio_Det) END as MAPIWAYNRAQCCODE ,
--JHD-20160211 - Se cambio la logica del store procedure para todos los casos de mapi y wayna
--JHD-20160415 - Se cambio a tabla temporal para optimizar el sp
CREATE Procedure [dbo].[ENTRADASINC_SAP_List]
@FePago1 smalldatetime='01/01/1900',
@FePago2 smalldatetime='01/01/1900',
@FechaINCusco1 smalldatetime='01/01/1900',
@FechaINCusco2 smalldatetime='01/01/1900'
AS

BEGIN
SET NOCOUNT ON
--DECLARE @tabla1 as table
create table #tabla1(idcab int)

insert into #tabla1
select x.idcab from(
select  Min(cd.Dia) as fecha,c.idcab from coticab c
inner join cotidet cd on c.idcab=cd.idcab
where c.flhistorico=0 and c.Estado='A' and cd.idubigeo='000066' -- and c.IDFile='16020144'
group by c.idcab
HAVING MIN(CD.DIA) Between @FechaINCusco1 And @FechaINCusco2 Or Convert(Char(10),@FechaINCusco1,103)='01/01/1900'
) as x

--select * from @tabla1
--return

declare @IDcab int=0,@IDFile char(8)='',@iddet int=0,@idservicio char(8),@Anio char(4), @MAPIWAYNRAQCCODE varchar(20),@MontoPago numeric(12,2),
@FechaINCusco smalldatetime,@Dia varchar(10)

--declare @tmp as table
create table #tmp
(
IDCAB int,
 IDFile char(8),
	iddet int ,
	idservicio char(8),
	anio char(4),
	
	 MAPIWAYNRAQCCODE char(8) ,
	  MontoPago decimal(10,2),
	FechaINCusco smalldatetime,
	  Dia VARCHAR(20)
)

insert into #tmp
select distinct 
c.IDCAB,
 c.IDFile,
	cd.iddet,
	cd.idservicio,
	sd.anio,
	
	 dbo.FnServicioMPWP(cd.IDServicio_Det) as MAPIWAYNRAQCCODE ,
	 0 as MontoPago,
	(Select Min(Dia) From COTIDET Where IDCAB=cd.IDCAB And IDubigeo='000066') as FechaINCusco,
	
	 CONVERT(VARCHAR(20), cd.Dia, 103) as Dia
	
--into @tmp
from cotidet cd
inner join coticab c on c.IDCAB=cd.IDCAB
inner join MASERVICIOS_DET sd on sd.IDServicio_Det=cd.IDServicio_Det

inner join #tabla1 t on t.idcab=c.idcab

where exists(select IDDet from COTIDET_PAX cdp where cdp.IDDet=cd.IDDET and (cdp.FlEntMPGenerada=1 or cdp.FlEntWPGenerada=1 or cdp.FlEntRQGenerada=1))



--select * from #tmp
--select IDCAB,IDFile,IDDET,IDServicio,Anio,MAPIWAYNRAQCCODE,MontoPago,FechaINCusco,Dia from #tmp
--return
DECLARE CUR_INC  cursor for
	select IDCAB,IDFile,IDDET,IDServicio,Anio,MAPIWAYNRAQCCODE,MontoPago,FechaINCusco,Dia from #tmp
open CUR_INC
	fetch next from CUR_INC into  @IDcab ,@IDFile ,@iddet ,@idservicio ,@Anio, @MAPIWAYNRAQCCODE ,@MontoPago,
					@FechaINCusco ,@Dia 
 While @@FETCH_STATUS=0      
 Begin     
			--select * from @tmp where IDCAB=@IDcab
  
		If ((select count(*) from #tmp where IDCAB=@IDcab And Dia=@Dia And MAPIWAYNRAQCCODE ='CUZ00134')=1 And
			(select count(*) from #tmp where IDCAB=@IDcab And Dia=@Dia And MAPIWAYNRAQCCODE ='CUZ00132')=1)
			Begin			
				--select IDCAB,IDFile,IDDET,IDServicio,Anio,'CUZ00133',dbo.FnDevuelveCostoINCxMAPIWAYNARAQ(IDDET,'CUZ00133',Anio,0,0),
			
				Insert into #tmp 
				select IDCAB,IDFile,IDDET,IDServicio,Anio,'CUZ00133',dbo.FnDevuelveCostoINCxMAPIWAYNARAQ_Total(IDDET,'CUZ00133',Anio),
				--select IDCAB,IDFile,IDDET,IDServicio,Anio,'CUZ00133',1,
				FechaINCusco,Dia
				from #tmp where IDCAB=@IDcab And Dia=@Dia And MAPIWAYNRAQCCODE ='CUZ00132'

				Delete from #tmp where IDCAB=@IDcab And Dia=@Dia And MAPIWAYNRAQCCODE In('CUZ00132','CUZ00134')
			End
		else
			begin
			
				update #tmp set MontoPago=dbo.FnDevuelveCostoINCxMAPIWAYNARAQ_Total(iddet,MAPIWAYNRAQCCODE,anio)
				where IDDET=@iddet
			end
 Fetch Next From CUR_INC Into @IDcab ,@IDFile ,@iddet ,@idservicio ,@Anio, @MAPIWAYNRAQCCODE ,@MontoPago,
					@FechaINCusco ,@Dia     
 End     
close CUR_INC
deallocate CUR_INC

--select 'a',* from #tmp   order by IDFile 


--select * 

select t.IDCAB,t.IDFile,
--Tipo,
case MAPIWAYNRAQCCODE when 'CUZ00145' then 'RAQCHI' when 'CUZ00134' then 'WAYNA' when 'CUZ00133' then 'WAYNA' else 'MAPI' END AS Tipo,
Monto=CAST(isnull((select monto from ENTRADASINC_SAP where IdDet=t.iddet and tipo <> 'CONSETTUR' ),t.MontoPago) AS INT),
-- Monto,
 FechaINCusco,
 FechaPago,
 isnull(NuCodigo_ER,'') as NuCodigo_ER,
 t.iddet
 ,0 as NuDetPSo

--from @tmp t 
from #tmp t 
left outer join ENTRADASINC_SAP e on t.IDFile=e.IDFile collate SQL_Latin1_General_CP1_CI_AS and t.IDDET=e.IdDet and e.Tipo not in ('CONSETTUR','BTG')
--Where (e.FechaPago Between @FePago1 And @FePago2 Or Convert(Char(10),@FePago1,103)='01/01/1900') 
Where (FechaPago Between @FePago1 And @FePago2 Or Convert(Char(10),@FePago1,103)='01/01/1900') 
order by t.IDFile
--drop table @tmp

drop table #tabla1
drop table #tmp


END;
