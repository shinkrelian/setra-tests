﻿--create
CREATE PROCEDURE [dbo].[MACENTROCOSTOS_Sel_List_Cbo_ServiciosVarios]
AS
BEGIN
	SELECT CoCeCos, CoCeCos + ' - ' + Descripcion as Descripcion
	FROM MACENTROCOSTOS
	WHERE (FlActivo = 1) AND (CoCeCos IN ('900101','900103'))
END;

