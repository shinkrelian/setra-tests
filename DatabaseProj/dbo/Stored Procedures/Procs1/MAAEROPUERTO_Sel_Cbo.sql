﻿
CREATE procedure dbo.MAAEROPUERTO_Sel_Cbo
As    
	Set NoCount On    
	
	Select CoAero, NoDescripcion from MAAEROPUERTO
	Where FlActivo=1
	Order by NoDescripcion
	
