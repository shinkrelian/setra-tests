﻿Create Procedure MAMODOSERVICIO_Upd
@CodModoServ char(2),
@Descripcion varchar(30),
@UserMod char(4)
As
	Set NoCount On
	Update MAMODOSERVICIO
		Set Descripcion=@Descripcion,
			UserMod = @UserMod		
		Where CodModoServ =@CodModoServ
