﻿Create Procedure dbo.MACORREOSBIBLIAPROVEEDOR_Sel_ListxIDProveedor  
@IDProveedor char(6)
As  
Select cbp.IDProveedor,Correo,Isnull(Descripcion,'') as Descripcion 
from MACORREOSBIBLIAPROVEEDOR cbp Left Join MAPROVEEDORES p On cbp.IDProveedor= p.IDProveedor
where cbp.IDProveedor = @IDProveedor and Activo = 'A'
