﻿--HLF-20121108-Cambiar a Order by Cast(SUBSTRING(cp.Apellidos,4,3) as tinyint)        
--HL-20121210-Cambiando Order by        
--HL-20130124-Cambiando Order by        
--HLF-20130208-Agregando Orden as NroOrd y Order by         
--JRF-20130304-Cambiando la posicion de columnas [Nombres x Apellido]      
--HLF-20130308-Agregando c.Residente    
--HLF-20150219-And c.FlNoShow=0
--JRF-20150227-Considerar FlNoShow para Reservas u Operaciones
CREATE PROCEDURE [dbo].[COTIPAX_Sel_xIDCab_Lvw]        
 @IDCab int,
 @NoShow bit = 0
As        
        
 Set NoCount On         
         
 Select Apellidos, Nombres, NumIdentidad, IDPax, IDNacionalidad, Residente    
 From         
 (        
 Select c.Apellidos, c.Nombres, c.NumIdentidad, c.IDPax, c.IDNacionalidad,        
 Case When ISNUMERIC(SUBSTRING(Apellidos,5,2))=1 Then          
  SUBSTRING(Apellidos,5,2)        
 Else        
  Nombres        
 End as NroNombrePax, c.Orden, c.Residente         
 From COTIPAX c Where c.IDCab=@IDCab        
 And c.FlNoShow=@NoShow 
 ) as X        
         
 --Order by Cast((Case When ISNUMERIC(NroNombrePax)=1 Then NroNombrePax Else 0 End) As smallint)        
 Order by Orden 
