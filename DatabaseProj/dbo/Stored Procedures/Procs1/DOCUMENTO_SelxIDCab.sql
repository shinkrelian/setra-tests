﻿
--HLF-20141226-Agregando columnas ,SsIGV ,SsTotalDocumUSD, LEFT(NuDocum,3) as SerieDoc	
CREATE Procedure [dbo].[DOCUMENTO_SelxIDCab]
	@IDCab int
As	
	Set NoCount On
	
	Select NuDocum,IDTipoDoc, FeDocum,SsIGV ,SsTotalDocumUSD, LEFT(NuDocum,3) as SerieDoc	
	From DOCUMENTO 
	Where IDCab=@IDCab 
