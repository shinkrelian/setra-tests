﻿


--HLF-20150604-And mt.CoTipo=@CoTipo
CREATE Procedure dbo.ACOMODO_VEHICULO_SelxIDDet
	@IDDet int,
	@CoUbigeo char(6),
	@CoCliente char(6),
	@CoVariante varchar(7),
	@CoTipo smallint
As	
	Set Nocount On

	Declare @CoMercado smallint=0
	If @CoCliente='000160'--Gebeco
		Set @CoMercado=2
	If @CoVariante='ASIA'
		Set @CoMercado=1
	

	Select isnull(av.QtVehiculos,0) as QtVehiculos,
	mt.NuVehiculo, mt.NoVehiculoCitado as NoVehiculo, mt.QtCapacidad,
	av.QtPax as QtPax, ISNULL(av.FlGuia,0) as FlGuia
	From MAVEHICULOS_TRANSPORTE mt Left Join ACOMODO_VEHICULO av On mt.NuVehiculo=av.NuVehiculo
		And av.IDDet=@IDDet
	Where mt.FlActivo=1 And mt.FlReservas=1
	And mt.CoUbigeo=@CoUbigeo 
	And isnull(mt.CoMercado,0)=@CoMercado
	And mt.CoTipo=@CoTipo
	Order by mt.NoVehiculoCitado

