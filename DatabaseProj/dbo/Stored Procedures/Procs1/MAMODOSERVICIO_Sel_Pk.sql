﻿Create Procedure MAMODOSERVICIO_Sel_Pk
@CodModoServ char(2)
As
	Set NoCount On
	
	Select *
	From MAMODOSERVICIO
	Where CodModoServ = @CodModoServ
