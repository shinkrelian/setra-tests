﻿Create Procedure dbo.COTIDET_PAX_UpdFlEntWPGenerada	
	@IDDet int,
	@IDPax int,	
	@FlEntWPGenerada bit,
	@UserMod char(4)
As
	Set nocount on

	Update COTIDET_PAX Set FlEntWPGenerada=@FlEntWPGenerada,
		UserMod=@UserMod, FecMod=GETDATE()
	Where IDDet=@IDDet And IDPax=@IDPax
