﻿
CREATE Procedure [dbo].[CORREOFILE_Sel_List_BibliaBE]    
@IDOperador varchar(6),    
@IDTransportista varchar(6),    
@IDGuia varchar(6),    
@DescFecha varchar(50),    
@CoTipoBandeja  char(2) 
As    
Set NoCount On    
 select '' as IconoCorreo,'' as ImgAdjuntos,    
   NuCorreo,(select COUNT(*) From .BDCORREOSETRA..ADJUNTOSCORREOFILE where NuCorreo = c.NuCorreo) as CantAdjuntos,    
   CoCorreoOutlook as EntryID, NoCuentaDe as De,         
   dbo.FnDestinatariosCorreo(c.NuCorreo, 0) as Para,        
   NoAsunto as Asunto,         
   Case When CoFormato='T' Then TxMensaje Else '' End as Body,        
   Case When CoFormato='H' Then TxMensaje Else '' End as BodyHtml,        
   dbo.FnDestinatariosCorreo(c.NuCorreo, 1) as ConCopia,        
   FlImportancia as Importance,        
   FlLeido as EstadoLeido,        
   CoFormato as BodyFormat,        
   dbo.FnAdjuntosCorreo(c.NuCorreo) as RutasAdjuntos,        
   c.FeCorreo as Fecha        
 From .BDCORREOSETRA..CORREOFILE c    
 where CoTipoBandeja = @CoTipoBandeja --and YEAR(FeCorreo) = @Anio 
	and NoAsunto like '%'+@DescFecha+'%' and  
  (@IDOperador+@IDTransportista+@IDGuia ='' Or 
  CoUsrPrvDe In (@IDOperador,@IDTransportista,@IDGuia))

