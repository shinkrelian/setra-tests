﻿CREATE Procedure [dbo].[COTIDET_BUP_Ins]
	@IDCab int,
	@UserMod	char(4)
As
	Set NoCount On	
	
	Insert Into COTIDET_BUP Select * From COTIDET Where IDCAB=@IDCab And 
	IDCAB Not In (Select IDCAB From COTIDET_BUP)

	If @@ROWCOUNT>0	
		Update COTIDET_BUP Set UserMod=@UserMod,FecMod=GETDATE() Where IDCAB=@IDCab
