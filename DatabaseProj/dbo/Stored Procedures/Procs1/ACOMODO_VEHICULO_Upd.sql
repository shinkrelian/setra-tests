﻿
Create Procedure dbo.ACOMODO_VEHICULO_Upd
	@NuVehiculo smallint,
	@IDDet int,
	@QtVehiculos tinyint,
	@QtPax tinyint,
	@FlGuia bit,
	@UserMod char(4)
As
	Set Nocount on

	Update ACOMODO_VEHICULO
	Set QtVehiculos=@QtVehiculos,
	QtPax=@QtPax,
	FlGuia=@FlGuia,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where NuVehiculo=@NuVehiculo And IDDet=@IDDet

