﻿Create Procedure dbo.COTICAB_Sel_RptTCambios
@IDCab int
As
Set NoCount On
Select c.IDFile,c.Cotizacion,mo.Descripcion +' ('+mo.Simbolo+')' as Moneda,ctc.* 
from COTICAB_TIPOSCAMBIO ctc Left Join COTICAB c On ctc.IDCab = c.IDCAB
Left Join MAMONEDAS mo On ctc.CoMoneda = mo.IDMoneda
where ctc.IDCab = @IDCab
