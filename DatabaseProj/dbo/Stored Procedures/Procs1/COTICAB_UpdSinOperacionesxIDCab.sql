﻿
--HLF-20130625-agregando FecMod=GETDATE(), UserMod=@UserMod
CREATE Procedure dbo.COTICAB_UpdSinOperacionesxIDCab  
	@IDCab int,
	@UserMod char(4)
As  
	Set Nocount On  
  
	UPDATE COTICAB SET IDUsuarioOpe='0000', FechaOperaciones=NULL,
	FecMod=GETDATE(), UserMod=@UserMod 
	WHERE IDCab=@IDCab  
  
  
