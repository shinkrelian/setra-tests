﻿--HL-20121210-Cambiando Order by  
--HL-20130528-Cambiando a Order by Orden
--JRF-20151117-Agregar el flNoShow
CREATE Procedure [Dbo].[ACOMODOPAX_FILE_TvwNdPax]    
 @IDCAB Int  
As  
 Set NoCount On  
   
 Select IDPax, Nombre  
 From  
 (  
 Select IDPax, Nombres + ' '+ Apellidos As Nombre,  
 Case When ISNUMERIC(SUBSTRING(Apellidos,5,2))=1 Then    
  SUBSTRING(Apellidos,5,2)  
 Else  
  Nombres  
 End as NroNombrePax, Orden  
 From COTIPAX  
 Where IDCAB = @IDCAB  And FlNoShow= 0
 --Order By Nombre  
 ) as X  
  Order by Orden
