﻿--JRF-20131108-Agregar la relacion con la Tabla MATARJETACREDITO      
--JRF-20131127-Se cambio de Nombre a la Columna FeDeposito a FeAcreditada  
--JRF-20131128-Se agrego la forma de pago.
--HLF-20140829-Left Join MAUBIGEO u On cl.IDCiudad = u.IDubigeo
--HLF-20150820-like '%'+@DescCliente+'%')   agregando primer %    
--like @CoOperacionBan+'%')  agregando ambos %    
CREATE Procedure dbo.INGRESO_FINANZAS_Sel_List        
@DescCliente varchar(80),        
@CoEstado char(2),        
@Banco char(3),        
@CoOperacionBan varchar(20),        
@RangoFecha1 smalldatetime,        
@RangoFecha2 smalldatetime        
As        

 Set NoCount On        
    
select * from (    
select distinct ifn.NuIngreso,ifn.FePresentacion, ifn.FeAcreditada as FeAcreditada,  
 --case when b.Sigla is null then t.Descripcion else b.Sigla End   
 Isnull(b.Sigla,'---') as SiglaBanco,     
 Isnull(fp.Descripcion,'---') as FormaPago,  
 mo.Descripcion as Moneda, Isnull(ifn.ssTC,0) as TipoCambio,        
 ifn.SsRecibido,ifn.SsBanco as ComisionBanco,ifn.SsNeto,ifn.CoOperacionBan,        
 cl.RazonComercial as DescCliente,
 u.Descripcion as UbigeoCliente,         
 ifn.TxObservacion as DetalleObservacion,  
 case ifn.CoEstado   
 when 'AS' then 'ASIGNADO'  
 when 'EP' then 'EN PROCESO'  
 when 'RG' then 'REGISTRADO'  
 End as CoEstado  
 ,Isnull(a.NuAsignacion,0) as NuAsignacion    
from ASIGNACION a Right Join INGRESO_DEBIT_MEMO idm on idm.NuAsignacion = a.NuAsignacion    
Right Join INGRESO_FINANZAS ifn On idm.NuIngreso = ifn.NuIngreso    
Left Join MACLIENTES cl On ifn.IDCliente = cl.IDCliente     
Left Join MABANCOS b On ifn.IDBanco = b.IDBanco        
--Left Join MAUBIGEO u On ifn.IDPais = u.IDubigeo        
Left Join MAUBIGEO u On cl.IDCiudad = u.IDubigeo        
Left Join MAMONEDAS mo On ifn.IDMoneda = mo.IDMoneda        
--Left Join MATARJETACREDITO t On ifn.CodTar = t.CodTar     
Left Join MAFORMASPAGO fp On ifn.IDFormaPago = fp.IdFor  
Where (ltrim(rtrim(@DescCliente))='' Or cl.RazonComercial like '%'+@DescCliente+'%')        
 And (LTRIM(rtrim(@CoEstado))='' or ifn.CoEstado = @CoEstado)        
 And (ltrim(rtrim(@Banco))='' Or ifn.IDBanco = @Banco)        
 And (ltrim(rtrim(@CoOperacionBan))='' or ifn.CoOperacionBan like '%'+@CoOperacionBan+'%')        
 And ((CONVERT(char(10),@RangoFecha1,103)='01/01/1900' and convert(char(10),@RangoFecha2,103)='01/01/1900')        
   Or ifn.FeAcreditada between @RangoFecha1 and @RangoFecha2)) as X    
Order by CAST(X.FeAcreditada As smalldatetime)       
