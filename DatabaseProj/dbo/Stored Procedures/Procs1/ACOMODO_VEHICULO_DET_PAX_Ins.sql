﻿
--HLF-20150708-Nuevos campos NuNroVehiculo, FlGuia
CREATE Procedure dbo.ACOMODO_VEHICULO_DET_PAX_Ins
	@NuVehiculo smallint,
	@NuNroVehiculo tinyint,
	@IDDet int,	
	@NuPax int,	
	@FlGuia bit,
	@UserMod char(4) 	
As
	Set Nocount On
	
	Insert into ACOMODO_VEHICULO_DET_PAX(NuVehiculo,NuNroVehiculo,IDDet, NuPax,FlGuia,UserMod)
	Values(@NuVehiculo,@NuNroVehiculo,@IDDet, @NuPax, @FlGuia,@UserMod)


