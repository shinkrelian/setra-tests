﻿--HLF-20150708-Nuevos campos NuNroVehiculo, FlGuia
CREATE Procedure dbo.ACOMODO_VEHICULO_DET_PAX_SelxIDDet
	@IDDet int
As
	Set Nocount On

	Select NuVehiculo, NuPax, NuNroVehiculo, FlGuia
	From ACOMODO_VEHICULO_DET_PAX
	Where IDDet=@IDDet

