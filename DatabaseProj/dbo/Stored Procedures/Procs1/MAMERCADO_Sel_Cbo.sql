﻿
--[MAMERCADO_Sel_Cbo] 0
--CREATE
CREATE Procedure [dbo].[MAMERCADO_Sel_Cbo]
 @bTodos bit
As
	Set NoCount On
	select * from
		(
		 Select 0 as CoMercado,'<TODOS>' as Descripcion, 0 as Ord  
		Union  
		Select
		[CoMercado],
 		[Descripcion]
		, 1 as Ord 
 	     From [dbo].[MAMERCADO]
		)
		as x
		Where (Cast(@bTodos as char(1))=1 Or Ord<>0)  
		Order by Ord,2
