﻿CREATE Procedure [dbo].[COTIDET_LOG_Upd_AtendidoxProveedor]	
	@IDCab	int,
	@IDProveedor	char(6),
	@Atendido	bit,
	@UserModAtendidoLog	char(4)
As
	Set NoCount On	
	
	Update COTIDET_LOG Set Atendido=@Atendido,
	UserModAtendidoLog=@UserModAtendidoLog,
	FecModAtendidoLog=GETDATE()
	Where 	
	IDCab=@IDCab And Not Atendido=@Atendido And	IDProveedor=@IDProveedor
	And Accion<>' '
