﻿
--JR-20130206- Correcion ISNULL
CREATE Procedure [dbo].[COTIVUELOS_Sel_ListSolicitud]   
@IDCAB int  
As  
 Set NoCount On  
 select   
 upper(substring(DATENAME(dw,Fec_Salida),1,3))+' '+ltrim(rtrim(convert(varchar,Fec_Salida,103))) As FecSalida  
 ,convert(char(5),Salida,108) Salida  
 ,ISNULL(upper(substring(DATENAME(dw,Fec_Retorno),1,3))+' '+ltrim(rtrim(convert(varchar,Fec_Retorno,103))),'01/01/1900') As FecRetorno  
 ,convert(char(5),Llegada,108) Llegada  
 ,p.NombreCorto  
 ,Vuelo  
 ,isnull(convert(char(5),HrRecojo,108),'00:00') HrRecojo  
 ,u2.Descripcion as Origen  
 ,u.Descripcion as Destino  
 from COTIVUELOS v Left Join MAPROVEEDORES p On v.IdLineaA = p.IDProveedor  
       Left Join MAUBIGEO u On v.RutaD = u.IDubigeo  
       Left Join MAUBIGEO u2 On v.RutaO = u2.IDubigeo  
  where IDCAB = @IDCAB  And TipoTransporte = 'V'  
  Order by 1  
