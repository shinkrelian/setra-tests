﻿/*
declare @pExisteZ		bit
execute COTIDET_Sel_Es_Zicasso '003557', 'LAX00002', 0, @pExisteZ output
select @pExisteZ
*/
CREATE PROCEDURE [dbo].[COTIDET_Sel_Es_Zicasso]
					--@IDCab			int,
					@IDProveedor	char(6),
					@IDServicio		char(8),
					@IDServicio_Det	int,
					@pExisteZ		bit output
AS
BEGIN
	DECLARE @Servicio_Det_Zicasso CHAR(4) = 'ZIC6'
			,@IDProveedor_Zicasso char(6) = '003557'
			,@IDServicio_Zicasso char(8)	= 'LAX00002'
			
	Set NoCount On
	Set @pExisteZ = 0
	IF EXISTS(SELECT IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
			  WHERE TipoUsoSistema = @Servicio_Det_Zicasso
			  AND (IDProveedor = @IDProveedor AND @IDProveedor_Zicasso = @IDProveedor)
			  AND (D.IDServicio = @IDServicio AND @IDServicio_Zicasso = @IDServicio) AND IDServicio_Det = @IDServicio_Det)
		SET @pExisteZ = 1
END
