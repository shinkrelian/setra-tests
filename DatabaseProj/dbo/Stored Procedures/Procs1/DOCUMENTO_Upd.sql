﻿  
CREATE Procedure dbo.DOCUMENTO_Upd  
	@NuDocum Char(10),   
	@IDTipoDoc char(3),   
	@IDMoneda char(3),	
	@SsTotalDocumUSD numeric(9, 2),  
	@UserMod char(4)  
As  
	Set NoCount On  

	Update DOCUMENTO   
	Set 
	IDMoneda=@IDMoneda,
	SsTotalDocumUSD=@SsTotalDocumUSD,  	
	UserMod  = @UserMod,  
	FecMod = GETDATE()    
	Where NuDocum = @NuDocum and IDTipoDoc = @IDTipoDoc  

