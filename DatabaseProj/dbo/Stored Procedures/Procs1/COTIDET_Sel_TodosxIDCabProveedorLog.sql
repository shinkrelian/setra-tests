﻿CREATE Procedure [dbo].[COTIDET_Sel_TodosxIDCabProveedorLog]
	@IDCab	int,
	@IDProveedor char(6)
As
	Set NoCount On
	
	Select c.*,p.IDTipoProv 
	From COTIDET c Left Join MAPROVEEDORES p On c.IDProveedor=p.IDProveedor
	Where c.IDCAB=@IDCab And c.IDProveedor=@IDProveedor 
	And c.IDDET In (Select IDDET From COTIDET_LOG Where Atendido=0)
