﻿CREATE Procedure [dbo].[COTIDET_QUIEBRES_SelxIDCab_Q_IDDet_QOutput]
	@IDCab_Q int,
	@pIDDet_Q int	output
As
	Set Nocount On

	Select @pIDDet_Q=IDDet_Q From COTIDET_QUIEBRES dq 
	Where IDCAB_Q=@IDCab_Q

	Set @pIDDet_Q=isnull(@pIDDet_Q,0)
