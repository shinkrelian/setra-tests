﻿
--ALTER
CREATE PROCEDURE MACONCEPTO_HOTEL_Sel_List_Servicios
	@CoConcepto tinyint=0,
	@CoTipo char(3)='',
	@TxDescripcion varchar(150)=''
AS
BEGIN
	Select
		CoConcepto,
 		TxDescripcion,
 		CoTipo,
 		FlServicio=CAST(0 as bit),
 		FlCosto= CAST(0 as bit),
 		FlFijoPortable= CAST(0 as bit),
 		CoTipoMenu=''
 		--FlActivo,
 		--UserMod,
 		--FecMod
 	From dbo.MACONCEPTO_HOTEL
	Where 
		(CoConcepto = @CoConcepto or @CoConcepto=0) and 
		(CoTipo = @CoTipo or @CoTipo='') and 
		(@TxDescripcion='' or TxDescripcion like '%'+@TxDescripcion+'%')
END
