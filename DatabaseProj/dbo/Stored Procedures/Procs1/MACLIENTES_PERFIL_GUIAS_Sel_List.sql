﻿
-------------------------------------------------------------

--create
CREATE PROCEDURE MACLIENTES_PERFIL_GUIAS_Sel_List
	@CoCliente char(6)='',
	@CoProveedor varchar(6)='',
	@IDCiudad char(6)='',
	@RazonSocial varchar(100)=''
	
AS
BEGIN

SELECT     MACLIENTES_PERFIL_GUIAS.CoProveedor,
			MAPROVEEDORES.NombreCorto,
			MAPROVEEDORES.IDCiudad
			
FROM         MACLIENTES_PERFIL_GUIAS INNER JOIN
                      MAPROVEEDORES ON MACLIENTES_PERFIL_GUIAS.CoProveedor = MAPROVEEDORES.IDProveedor
WHERE     
			(MACLIENTES_PERFIL_GUIAS.CoCliente = @CoCliente or @CoCliente='') and
			(MAPROVEEDORES.IDCiudad = @IDCiudad or @IDCiudad ='') AND
				(MACLIENTES_PERFIL_GUIAS.CoProveedor = @CoProveedor or @CoProveedor='') AND
						(@RazonSocial='' or MAPROVEEDORES.RazonSocial like '%'+@RazonSocial+'%') 

END;
