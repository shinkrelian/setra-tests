﻿
CREATE PROCEDURE [dbo].[DOCUMENTO_FORMA_EGRESO_UpdSsPendientexDoc]
	@IDDocFormaEgreso	int,
	@TipoFormaEgreso	char(3),
	@CoMoneda_FEgreso	char(3),
	@SsTotalOrig		numeric(9,2),  
	@UserMod			char(4)
AS
BEGIN
	UPDATE DOCUMENTO_FORMA_EGRESO SET SsSaldo = SsSaldo - @SsTotalOrig, UserMod = @UserMod, FecMod = GETDATE()
	WHERE IDDocFormaEgreso = @IDDocFormaEgreso AND TipoFormaEgreso = @TipoFormaEgreso AND CoMoneda = @CoMoneda_FEgreso
END;
