﻿--JHD-20150707-Nueva tabla para Debit Memos pendientes
--JHD-20150708-Nueva campo TicketFile para tabla VentasxCliente_Anual
--JHD-20150708-Cant_Clientes=count(distinct idcliente)
--HLF-20150916-Total_Proy=	Case When Estado='X' and IDCAB in ...
--SumaProy=Case When Estado='X' and IDCAB in ...SumaProy2=Case When Estado='X' and IDCAB in ...
--HLF-20150924-SumaProy=Case When Estado='X' and IDCAB in ...SumaProy2=Case When Estado='X' and IDCAB in ...
--ValorVenta=,Total_concret=,Utilidad=Case When Estado='X' and IDCAB in ...SumaProy2=Case When Estado='X' and IDCAB in ...
--JHD-20151201-,[dbo].[FnDevolverCategoriaPredominante_File](IDFile)
--JHD-20151222-Se agregaron nuevas tablas para clientes web
--JHD-20151228-Se agregaron las columnas TicketFile y TicketFileAnioAnter para las ventas por región
--JHD-20151228-Se agregaron nuevas tablas para ventas por region para clientes web
--JHD-20160112-	Comments=isnull((select top 1 Descripcion from DEBIT_MEMO_DET where IDDebitMemo=d.IDDebitMemo),''),
--JHD-20160112-Supervisor=
				--isnull((select uj.Nombre + isnull(uj.TxApellidos,'') from MAUSUARIOS u
				--inner join MAUSUARIOS uj on u.CoUserJefe=uj.IDUsuario
				--where u.IDUsuario=@IDUser),'')
--JRF-20160127-Agregar los files con diferente Tipo de venta [DR-ZICASSO]
--JHD-20160201-Case When ImporteUSDAnioAnter<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/ImporteUSDAnioAnter      
--JHD-20160307- +   IsNull(( select (IsNull(Sum(Total),0) - IsNull(Sum(IGV_SFE),0))  
--JHD-20160307-  IsNull(ZZ.CompraNetoUSD,0)+IsNull(ZZ.IGVCosto,0)+IsNull(ZZ.IGV_SFE,0) AS Total ,IsNull(ZZ.IGV_SFE,0) as  IGV_SF 
--JHD-20160401-(Select top 1 substring(CoOrden_Servicio,1,8)+'-'+substring(CoOrden_Servicio,9,6) From ORDEN_SERVICIO Where IDCab=CC4.IDCAB And IDProveedor=Z.IDProveedor) AS NroOrdenServicio,      
CREATE Procedure [dbo].[GrabarNotaVenta]
@Anio char(4),
@IDUser char(4)
As
Begin

--Declare @Anio char(4)='2015',@IDUser char(4)=''
Declare @AnioAnter char(4)=cast(@Anio as smallint)-1         
Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)
Declare @MargenInterno as numeric(6,2)=10
Declare @CoUbigeo_Oficina char(6)=''
   
 --BETWEEN '01/01/2015' AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/2015',103) as datetime)))--'31/12/2014 23:59:59'      
 
 -------------SIN APT-----------------------      
 
 delete from VentasporPais_AnioAnter2_Web where ParamAnio=@Anio
 delete from VentasporPais_AnioAnter_Web where ParamAnio=@Anio	 
 delete from VentasporPais_Web where ParamAnio=@Anio
 delete from VentasporPais2_Web where ParamAnio=@Anio
 delete from VentasporPais3_Web where ParamAnio=@Anio
 delete from VentasporPais4_Web where ParamAnio=@Anio

 delete from VentasporPais_AnioAnter2 where ParamAnio=@Anio
 delete from VentasporPais_AnioAnter where ParamAnio=@Anio	 
 delete from VentasporPais where ParamAnio=@Anio
 delete from VentasporPais2 where ParamAnio=@Anio
 delete from VentasporPais3 where ParamAnio=@Anio
 delete from VentasporPais4 where ParamAnio=@Anio
 delete from VentasReceptivoMensual where ParamAnio=@Anio
 delete from VentasReceptivoMensualAcumulado where ParamAnio=@Anio
 delete from VentasReceptivoMensualDET where ParamAnio=@Anio
 delete from VentasporEjecutivoMes where ParamAnio=@Anio     
 delete from VentasporEjecutivoMes2 where ParamAnio=@Anio
 delete from VentasporEjecutivoAnio where ParamAnio=@Anio
 delete from VentasxCliente_Anual where ParamAnio=@Anio
 delete from VentasporPaisClienteTemp where ParamAnio=@Anio
 delete from VentasporClienteMes where ParamAnio=@Anio
 delete from VentasReceptivoMensualDet_Sin_APT where ParamAnio=@Anio
 delete from VentasReceptivoMensual_Sin_APT where ParamAnio=@Anio
 delete from VentasReceptivoMensualAcumulado_SIN_APT where ParamAnio=@Anio
 delete from VentasReceptivoMensual_Sin_Porc where ParamAnio=@Anio
 delete from VentasReceptivoMensual_Sin_APT_SinPorc where ParamAnio=@Anio
 delete from VentasReceptivoMensual_Sin_APT_SinPorc where ParamAnio=@Anio
 delete from VentasDebitMemoPendientes

 delete from VentasReceptivoMensual_Web where ParamAnio=@Anio
 delete from VentasReceptivoMensual_Web_SinPorc where ParamAnio=@Anio
 delete from VentasReceptivoMensualAcumulado_Web where ParamAnio=@Anio
 delete from VentasReceptivoMensualDet_Web where ParamAnio=@Anio

 Insert into VentasReceptivoMensualDet_Sin_APT
 SELECT FecInicio,  
   FecOut,  
   FecOutPeru,  
   IDCAB,  
   IDFile,  
   Titulo,    
   

   ValorVenta=
   Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
   round(case when FecOut<GETDATE() /*pasado*/  
	   then  
		(  
		 --isnull(TotalDM,0)  
		  case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
		)  
	   else --futuro  
		(  
		 ISNULL(ValorVenta2,0)  
		)  
	   end,0)
   end,



   PoConcretVta,  
   NroPax,  
   ESTADO,  
   IDUsuario,  
   Responsable,  
   IDCliente,  
   CoPais,  
   DescPaisCliente,  
   DescCliente,  
   MargenGan=ISNULL(MargenGan,0),  
   Utilidad=ISNULL(ValorVenta2*(MargenGan/100),0)--,  
    
  -- , Total_Proy=case when FecInicio<GETDATE() /*pasado*/  
   , 
   Total_Proy=
	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		case when FecOut<GETDATE() /*pasado*/  
		then        
			case when PoConcretVta<100 then ISNULL(ValorVenta2,0)*(PoConcretVta/100) else 0 end        
		else --futuro        
			case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then 0 else TotalCotiz end        
		end  
	End
     
   
   , Total_Concret=
	Case When Estado='X' and IDCAB in            
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else

	   case when FecOut<GETDATE() /*pasado*/  
	   then  
		(  
		 case when PoConcretVta=100 then ISNULL(ValorVenta2,0) else 0 end  
		)  
	   else --futuro  
		(  
		 case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then TotalDM else 0 end  
		)  
	   end  
	End  
--,  SumaProy=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
,  SumaProy=

	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
	End
  
  ,  SumaProy2=
	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
	End
    ,@Anio
 FROM  
 (     
 SELECT Y.*,  
  
  ISNULL(Y.ValorVenta*(Y.MargenGan/100),0) AS Utilidad  
  ,ValorVenta2=case when Y.TotalDM>0 then (CASE WHEN ABS(Y.TotalDM-Y.TotalCotiz)>=Y.TotalDM_Porc THEN Y.TotalCotiz ELSE Y.TotalDM END) else Y.TotalCotiz end  

 FROM     
 (  
   SELECT       
   FecInicio,  
   FecOutPeru as FecOut,      
   Left(CONVERT(varchar,FecOutPeru,112),6) AS FecOutPeru,      
   IDCAB, IDFile, Titulo,     
   --isnull((Select sum(total*nropax) From Cotidet Where idcab=X.idcab),0) as ValorVenta       
    
   Case When EstadoFacturac='F' Then X.TotalDM    
   Else  --total de cotizacion  
     Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
       --isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
	   	isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
		   IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
			from COTIVUELOS cv 
			Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0)
      Else    
       --X.TotalSimple+X.TotalDoble+X.TotalTriple              
	   	case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz)+ -- dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
						  IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0) End
      End 
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
					((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
						  IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0)
     End     
   End 
   as ValorVenta ,    
     
   PoConcretVta,  
   --EsTotalxPax,ExisteHotel,       
   --NroPax,  
   NroPax=(NroPax+isnull(NroLiberados,0)),  
   Estado ,  
   IDUsuario,  
   Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
   , IDCliente, CoPais,  DescPaisCliente,  DescCliente,  
        
 case when X.TotalDobleNeto = 0 then 0         
   else                                                 
   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                                 
   End as MargenGan       
        
   ,TotalDM=isnull(X.TotalDM,0)  
   ,TotalDM_Porc=X.TotalDM*0.15  
   ,TotalCotiz=  Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
    isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
      Else    
			--X.TotalSimple+X.TotalDoble+X.TotalTriple              
				case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0) End
      End    
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
	  ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --
	  	  IsNull((Select Sum(Monto) from (
		   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
			(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
			from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
			Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
			),0)
     End     
       
   ,TotalCotiz_Porc=  (Case When ExisteHotel=1 Then    
          Case When EsTotalxPax=1 Then    
				isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
          Else    
				--X.TotalSimple+X.TotalDoble+X.TotalTriple              
					case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
						  IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0) End
          End    
         Else    
        -- X.NroPax * X.TotalCotiz       
        --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
        --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
		((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + -- dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
			  IsNull((Select Sum(Monto) from (
			   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
				(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
				from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
				Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
				),0)
         End     
      )*0.15  
   FROM       
   (      
   SELECT CC.*,      
   (Select SUM(Total) From DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN')
   --+ Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN') Then dbo.FnDevuelveMontoTotalxPaxVuelos(cc.IDCAB,1) Else 0 End as TotalDM,      
   + Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN')    Then 	  
   IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=cc.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0) Else 0 End as TotalDM, 
     
   cc.FechaOut as FecOutPeru,      
   --((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) *((Isnull(cc.Twin,0)+Isnull(cc.Matrimonial,0))) *2) + dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,1) as TotalDoble,
   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) *((Isnull(cc.Twin,0)+Isnull(cc.Matrimonial,0))) *2) 
   + 	  IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=CC.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0) as TotalDoble,

   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * Isnull(cc.Simple,0) as TotalSimple,                                                    
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * (Isnull(cc.Triple,0)*3) as TotalTriple,      
   (select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = CC.IDCAB) As TotalCotiz, --As TotalCotiz,
 --  dbo.FnEsTotalxPax(CC.IDCAB) as EsTotalxPax,      
  Cast((Case When (select sum(Total) from COTIPAX where IDCab=cc.IDCAB)=
				  ((NroPax+isnull(NroLiberados,0)) * (SELECT SUM(TOTAL) FROM COTIDET WHERE IDCAB=cc.IDCAB))
				  then 0 else 1 End
		) 
  as bit) as EsTotalxPax, 


   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                          
   Where cd.IDCAB = CC.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,      
        
   --(select SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,0) as TotalDobleNeto, --as TotalDobleNeto,      
  (select SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB)+
  IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
	    from COTIVUELOS cv 
		Where cv.IDCAB=CC.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0) as TotalDobleNeto, --as TotalDobleNeto,      

   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalSimpleNeto,--as TotalSimpleNeto,      
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalTripleNeto,--as TotalTripleNeto,
   (select SUM(TotImpto) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalImpto,      
   (select SUM(CostoReal) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalCostoReal,      
   (select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = cc.IDCAB) as TotalLib,  
   up.IDubigeo as CoPais,up.Descripcion as DescPaisCliente, c.RazonComercial as DescCliente  
   FROM COTICAB CC --Left Join DEBIT_MEMO db On db.IDCab = CC.IDCAB                   
    Inner Join MACLIENTES c On CC.IDCliente=c.IDCliente  
   Left Join MAUBIGEO uc On C.IDCiudad=uc.IDubigeo  
   Left Join MAUBIGEO up On uc.IDubigeo=up.IDubigeo  
  
   WHERE       
   
  c.IDCliente not in ('000054') AND
   
   cc.CoTipoVenta In('RC','DR') AND CC.Estado='A'       
   or             
   (cc.Estado='X' and cc.IDCAB in             
   (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG') )      
        
   --And ISNULL(db.IDEstado,'') <> 'AN'          
   ) AS X      
 WHERE x.FecOutPeru      
 --BETWEEN '01/01/2015' AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/2015',103) as datetime)))--'31/12/2014 23:59:59'      
 BETWEEN '01/01/'+@Anio/*'2015'*/ AND dateadd(S,
											 -1,
											  dateadd(d,
													  1,
													  cast(CONVERT(varchar,'31/12/'+@Anio/*2015'*/,103) as datetime))
											   )--'31/12/2014 23:59:59'      
 ) AS Y  
 ) AS Z  
 --Where Not Exists(select * from VentasReceptivoMensualDet_Sin_APT where ParamAnio=Cast(Z.FecOutPeru as char(4)))
 
 Print 'VentasReceptivoMensualDet_Sin_APT - ok'
 
 Insert into VentasReceptivoMensual_Sin_APT
 SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
  ----Case When ImporteUSD<>0 Then      
  ----((ImporteUSD-ImporteUSDAnioAnter)*100)/ImporteUSD      
  ----Else      
  ----100      
  ----End as VarImporteUSD,      
  Case When ImporteUSDAnioAnter<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/ImporteUSDAnioAnter
 --Case When SumaProy<>0 Then      
 --((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Else      
 100      
 End as VarImporteUSD,     
    
   SumaProy --=ROUND(SumaProy,0) 
   ,@Anio 
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Sin_APT,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy) as SumaProy  
 FROM       
 VentasReceptivoMensualDet_Sin_APT AS Y     
 Where Y.ParamAnio=@Anio    
 GROUP BY FecOutPeru       
 ) AS Z      
 --Where Not Exists(select * from VentasReceptivoMensualDet_Sin_APT where ParamAnio=@Anio)
 
 Print 'VentasReceptivoMensual_Sin_APT - ok'
-- ---------------------------------------------------------------------------------------------------------------------------
 
  Insert into VentasReceptivoMensual_Sin_APT_SinPorc
  SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
 
 --Case When SumaProy<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Case When ImporteUSDAnioAnter<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/ImporteUSDAnioAnter
 Else      
 100      
 End as VarImporteUSD,     
    
   SumaProy --=ROUND(SumaProy,0)    
   ,@Anio
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Sin_Porc_Sin_APT,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy2) as SumaProy  
 FROM       
 VentasReceptivoMensualDet_Sin_APT AS Y  
 Where Y.ParamAnio=@Anio  
 GROUP BY FecOutPeru		   
 ) AS Z   
 --Where Not Exists(select * from VentasReceptivoMensualDet_Sin_APT where ParamAnio=@Anio)

 Print 'VentasReceptivoMensual_Sin_APT_SinPorc - ok'
-- ---------------------------------------------------------------------------------------------------------------------------
 Declare @FecOutPeru1 char(6),@ImporteUSD1 numeric(12,2),@ImporteUSDAnioAnter1 numeric(12,2),       
 @ImporteMesUSD1 numeric(12,2)=0,@ImporteMesUSDAnioAnter1 numeric(12,2)=0,      
 @tiCont1 tinyint=1     

 --Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,ImporteUSD,ImporteUSDAnioAnter       
 Declare curVentasReceptivoMensual Cursor For 
 Select FecOutPeru,SumaProy,ImporteUSDAnioAnter     
 From VentasReceptivoMensual_Sin_APT where ParamAnio=@Anio Order by cast(FecOutPeru as int)      
 Open curVentasReceptivoMensual      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru1,@ImporteUSD1,@ImporteUSDAnioAnter1       
 While @@FETCH_STATUS=0      
 Begin      
 Set @ImporteMesUSD1+=@ImporteUSD1      
 Set @ImporteMesUSDAnioAnter1+=@ImporteUSDAnioAnter1     
 If @tiCont1=1      
 Insert into VentasReceptivoMensualAcumulado_SIN_APT
 Select @FecOutPeru1 as FecOutPeru, @ImporteMesUSD1 as ImporteMesUSD, @ImporteMesUSDAnioAnter1 as ImporteMesUSDAnioAnter,@Anio

 Else      
 Insert into VentasReceptivoMensualAcumulado_SIN_APT      
 Values(@FecOutPeru1, @ImporteMesUSD1, @ImporteMesUSDAnioAnter1,@Anio)      
      
 Set @tiCont1+=1      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru1,@ImporteUSD1,@ImporteUSDAnioAnter1       
 End      
      
 Close curVentasReceptivoMensual      
 Deallocate curVentasReceptivoMensual    
 
 Print 'Cursor #1 - ok'
 
-- -------------FIN SIN APT-----------------------      
       
 PRINT 'HOJA ACUMULADO'      
 
 Insert into VentasReceptivoMensualDet
 SELECT FecInicio,  
   FecOut,  
   FecOutPeru,  
   IDCAB,  
   IDFile,  
   Titulo,  
   
   ValorVenta=
   Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
   round(case when FecInicio<GETDATE() /*pasado*/  
   then  
    (  
     --isnull(TotalDM,0)  
	 case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
    )  
   else --futuro  
    (  
     ISNULL(ValorVenta2,0)  
    )  
   end,0)
   end,  
   PoConcretVta,  
   NroPax,  
   ESTADO,  
   IDUsuario,  
   Responsable,  
   IDCliente,  
   CoPais,  
   DescPaisCliente,  
   DescCliente,  
   --MargenGan=ISNULL(MargenGan,0),  
   --Utilidad=ISNULL(ValorVenta2*(MargenGan/100),0)--,  

    --MargenGan= case when FecInicio<GETDATE()
	MargenGan= case when FecOut<GETDATE()
			   then
				(((case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado)/(case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end ))*100
			   else
				ISNULL(MargenGan,0)  
			   end,
				
   --Utilidad= case when FecInicio<GETDATE()
   Utilidad= 
	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		case when FecOut<GETDATE()
			   then
				  (case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado
			   else
				  ISNULL(ValorVenta2*(MargenGan/100),0)
			   end
	End,
    
    --Total_Proy=case when FecInicio<GETDATE() /*pasado*/  
	Total_Proy=
	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		case when FecOut<GETDATE() /*pasado*/  
		then      
			case when PoConcretVta<100 then ISNULL(ValorVenta2,0)*(PoConcretVta/100) else 0 end        
		else --futuro        
			case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then 0 else TotalCotiz end        
		end  
	End 
   --, Total_Concret=case when FecInicio<GETDATE() /*pasado*/  
   , Total_Concret=
	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else

	   case when FecOut<GETDATE() /*pasado*/  
	   then  
		(  
		 case when PoConcretVta=100 then ISNULL(ValorVenta2,0) else 0 end  
		)  
	   else --futuro  
		(  
		 case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then TotalDM else 0 end  
		)  
	   end
	End,
  SumaProy=
  	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
	End
  ,  SumaProy2=
  	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else

		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
	End
--,  SumaProy=round(( round((case when FecInicio<GETDATE() then (isnull(TotalDM,0))  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
      ,Dias
   ,Edad_Prom
   ,Total_Programado,
   @Anio
   --
   ,[dbo].[FnDevolverCategoriaPredominante_File](IDFile)
   --
 FROM  
 (     
 SELECT Y.*,  
  
  ISNULL(Y.ValorVenta*(Y.MargenGan/100),0) AS Utilidad  
  ,ValorVenta2=case when Y.TotalDM>0 then (CASE WHEN ABS(Y.TotalDM-Y.TotalCotiz)>=Y.TotalDM_Porc THEN Y.TotalCotiz ELSE Y.TotalDM END) else Y.TotalCotiz end  
    
 FROM     
 (  
   SELECT       
   FecInicio,  
   FecOutPeru as FecOut, 
   Left(CONVERT(varchar,FecOutPeru,112),6) AS FecOutPeru,      
 IDCAB, IDFile, Titulo,     
   --isnull((Select sum(total*nropax) From Cotidet Where idcab=X.idcab),0) as ValorVenta       
    
   Case When EstadoFacturac='F' Then X.TotalDM    
   Else  --total de cotizacion  
     Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
       --isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
	   isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)
	   IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
	    from COTIVUELOS cv 
		Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0)
      Else    
		--X.TotalSimple+X.TotalDoble+X.TotalTriple              
			case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
						 IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0) End
      End    
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
	  ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
	  	  IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0)
     End     
   End 
   as ValorVenta ,    
     
   PoConcretVta,  
   --EsTotalxPax,ExisteHotel,       
   --NroPax,  
   NroPax=(NroPax+isnull(NroLiberados,0)),  
   Estado ,  
   IDUsuario,  
   Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
   ,Case When X.CoTipoVenta= 'DR' Then 'Z00001' Else IDCliente End as IDCliente, 
   CoPais,  DescPaisCliente,  
   Case When X.CoTipoVenta= 'DR' Then 'ZICASSO' Else DescCliente End as DescCliente, 
        
   case when X.TotalDobleNeto = 0 then 0                                                
   else                                                 
   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                                 
   End as MargenGan       
        
   ,TotalDM=isnull(X.TotalDM,0)  
   ,TotalDM_Porc=X.TotalDM*0.15  
   ,TotalCotiz=  Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
    isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
  Else    
		--X.TotalSimple+X.TotalDoble+X.TotalTriple              
			case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) 
						 IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0)
					end
      End    
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
	  ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz)+ --+ dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
	  	  IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0)
     End     
       
   ,TotalCotiz_Porc=  (Case When ExisteHotel=1 Then    
          Case When EsTotalxPax=1 Then    
				isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
          Else    
				--X.TotalSimple+X.TotalDoble+X.TotalTriple              
					case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) 
						  IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0)
					end
          End    
         Else    
 -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
        --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
		((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
			 IsNull((Select Sum(Monto) from (
			   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
				(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
				from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
				Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
				),0)
         End     
       )*0.15  
      
      ,Dias
   ,Edad_Prom
   ,Total_Programado
   FROM       
   (      
   SELECT cc4.*,      
   IsNull((Select SUM(Total) From DEBIT_MEMO Where IDCab=cc4.idcab and IDEstado<>'AN'),0)
   --+ Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc4.idcab and IDEstado<>'AN') Then dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) Else 0 End as TotalDM,    
   + Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc4.idcab and IDEstado<>'AN') Then --dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) Else 0 End as TotalDM,
   	  IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=CC4.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0) Else 0 End as TotalDM,    
   --isnull((Select Max(Dia) From COTIDET cd1        
   --Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo                 
   --And u1.IDPais='000323'       
   --Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det              
   --Where IDCAB=cc.IDCAB /*And SD1.idTipoOC<>'005'*/),cc.FechaOut) as FecOutPeru,      
     
   cc4.FechaOut as FecOutPeru,      
   --((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB) *((Isnull(cc4.Twin,0)+Isnull(cc4.Matrimonial,0))) *2) + dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) as TotalDoble,
   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB) *((Isnull(cc4.Twin,0)+Isnull(cc4.Matrimonial,0))) *2) + --dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) as TotalDoble,
   	  IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=CC4.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0) as TotalDoble,

   IsNull((select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) * Isnull(cc4.Simple,0) as TotalSimple,                                                    
   IsNull((select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) * (Isnull(cc4.Triple,0)*3) as TotalTriple,      
   IsNull((select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) As TotalCotiz, --As TotalCotiz,
   --dbo.FnEsTotalxPax(CC.IDCAB) as EsTotalxPax,    
     Cast((Case When (select sum(Total) from COTIPAX where IDCab=cc4.IDCAB)=
				  ((NroPax+isnull(NroLiberados,0)) * (SELECT SUM(TOTAL) FROM COTIDET WHERE IDCAB=cc4.IDCAB))
				  then 0 else 1 End
		) 
	as bit) as EsTotalxPax, 
   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                         
   Where cd.IDCAB = cc4.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,      
        
   --(select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,0) as TotalDobleNeto, --as TotalDobleNeto,      
   IsNull((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0)+ --dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,0) 
   IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
	    from COTIVUELOS cv 
		Where cv.IDCAB=CC4.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0) as TotalDobleNeto, --as TotalDobleNeto,      

   IsNull((select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) as TotalSimpleNeto,--as TotalSimpleNeto,      
   IsNull((select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) as TotalTripleNeto,--as TotalTripleNeto,
   IsNull((select SUM(TotImpto) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) as TotalImpto,      
   IsNull((select SUM(CostoReal) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) as TotalCostoReal,      
   IsNull((select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) as TotalLib,  
   up.IDubigeo as CoPais,up.Descripcion as DescPaisCliente, c.RazonComercial as DescCliente  
   
   ,Dias=IsNull(DATEDIFF(day,FecInicio,FechaOut),0)+1
   ,Edad_Prom=isnull(dbo.FnEdadPromedio_Pax(cc4.IDCab),0),
   --,Total_Programado=[dbo].[FnDevolverTotalProgramacion_Coti](cc.IDCAB,'USD')
   --,Total_Programado=dbo.FnDevolverTotal_Preliquidacion_CotiCab(cc.IDCAB,'')
   Total_Programado= IsNull(dbo.FnDevolverTotal_Entradas_Presupuesto_CotiCab(cc4.IDCAB,''),0)  --as Costo --7s
				     +IsNull(dbo.FnDevolverTotal_VuelosyBuses_CotiCab(cc4.IDCAB,''),0) --as Costo -- 0-5s
					 +IsNull(dbo.FnDevolverTotal_Interno_CotiCab(cc4.IDCAB,''),0) --as Costo --0-5s
					 +IsNull(dbo.FnDevolverTotal_Voucher_CotiCab(cc4.IDCAB,''),0) -- as Costo -- 4-8s
					-- +   IsNull((select IsNull(Sum(Total),0)
					 +   IsNull(( select (IsNull(Sum(Total),0) - IsNull(Sum(IGV_SFE),0))
                           FROM                  
       (         
                             SELECT                   
           -- IsNull(ZZ.CompraNetoUSD,0)+IsNull(ZZ.IGVCosto,0)+IsNull(ZZ.IGV_SFE,0) AS Total             
		   IsNull(ZZ.CompraNetoUSD,0)+IsNull(ZZ.IGVCosto,0)+IsNull(ZZ.IGV_SFE,0) AS Total ,IsNull(ZZ.IGV_SFE,0) as  IGV_SFE
                             FROM                  
                         (                 
      SELECT                   

                                  YY.NroOrdenServicio,                  
                                  YY.CompraNeto,YY.CompraNetoUSD, YY.IGVCosto,                   
                                  YY.IGV_SFE, 
                                  YY.Moneda, YY.idTipoOC,YY.Margen,                                  
                                  YY.CompraNetoUSD+IGVCosto as CostoBaseUSD                         
                              FROM                  
              (                  
                                  SELECT XX.IDProveedor,XX.NroOrdenServicio,                  
                                         XX.CompraNeto,XX.CompraNetoUSD, XX.IGVCosto,
                                                 Case When idTipoOC='001' Then                   
                                     XX.CompraNeto*(@IGV/100)                   
                                    Else                    
                                     Case When XX.idTipoOC='006' Then (XX.CompraNeto*(@IGV/100))*0.5 Else 0 End                  
                                    End as IGV_SFE,                   
                                   XX.Moneda, XX.idTipoOC,  XX.Margen        
                                  FROM                  
                                   (       
                                   SELECT Z.IDProveedor,
                                  --(Select substring(CoOrden_Servicio,1,8)+'-'+substring(CoOrden_Servicio,9,6) From ORDEN_SERVICIO Where IDCab=CC4.IDCAB And IDProveedor=Z.IDProveedor) AS NroOrdenServicio,                    
								  (Select top 1 substring(CoOrden_Servicio,1,8)+'-'+substring(CoOrden_Servicio,9,6) From ORDEN_SERVICIO Where IDCab=CC4.IDCAB And IDProveedor=Z.IDProveedor) AS NroOrdenServicio,                    
                           Z.CompraNeto, Z.CompraNetoUSD, Z.IGVCosto,Z.Moneda,                   
                           Z.idTipoOC,  Z.Margen                  
                           FROM          
                                    (                  
                                    SELECT  Y.IDProveedor,
                                    SUM(Y.CompraNeto) AS CompraNeto,   sum(Y.CompraNetoUSD) as CompraNetoUSD,              
                                    SUM(Y.IGVCosto) AS IGVCosto, Moneda,                                      
                                    Y.idTipoOC,                   
                                    AVG(Y.Margen) AS Margen                  
                                    FROM                  
                                     (                     
                                     SELECT X.*,                  
                                   Case When LTRIM(rtrim(''))='' Then 
                                        -- Cambio 2
                                              dbo.FnPromedioPonderadoMargen(CC4.IDCAB, X.IDProveedorOpe) 
                                         Else
                                               @MargenInterno
                                        End AS Margen,                      
                                        Case When idTipoOC='002' Then            
                              CompraNeto*(@IGV/100)             
                                  Else            
                              0            
                             End as IGVCosto            
                             FROM                  
                                        (                  
                                        SELECT ODS.IDProveedor_Prg as IDProveedor,                 
                                        ODS.IDOperacion_Det,                  
                            Case When ODS.IngManual=0 Then             
                                  Case When isnull(NetoCotizado,0) = 0 then  
          -- Cambio 1
                                        dbo.FnCambioMoneda(
                                                      IsNull((Select 
                                                           Case When sd6.Monto_sgl is null then 
                      (select Monto from MASERVICIOS_DET_COSTOS
                                                                          where IDServicio_Det=sd6.IDServicio_Det And
                                                                          (OD.NroPax+isnull(od.NroLiberados,0) between PaxDesde and PaxHasta)) 
                                                                   else
                                                                   IsNull(Monto_sgls,Monto_sgl) * (OD.NroPax+isnull(od.NroLiberados,0)) End
                                                      from MASERVICIOS_DET sd6 
                                                       where sd6.IDServicio_Det=sd.IDServicio_Det),0)
                                                      ,
                                        SD.CoMoneda,
                                        ods.IDMoneda,
                                        isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=CC2.IDCAB And     
                                                     CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))

                                  else                            
                              ods.NetoCotizado      
                                  End             
                            Else      
                             ODS.NetoProgram      
                            End      
                             --End            
                             As CompraNeto,                                        
                            Case When ODS.IngManual=0 Then             
  Case When isnull(NetoCotizado,0) = 0 then                            
                             -- Cambio 1
                    dbo.FnCambioMoneda(          
                                                      IsNull((Select 
                                                                   Case When sd6.Monto_sgl is null then 
                                                                          (select Monto from MASERVICIOS_DET_COSTOS
                                                                          where IDServicio_Det=sd6.IDServicio_Det And
                                                                          (OD.NroPax+isnull(od.NroLiberados,0) between PaxDesde and PaxHasta)) 
                                                                   else
                                                                   IsNull(Monto_sgls,Monto_sgl) * (OD.NroPax+isnull(od.NroLiberados,0)) End
                                                      from MASERVICIOS_DET sd6 
                                                       where sd6.IDServicio_Det=sd.IDServicio_Det),0),
                                  SD.CoMoneda,
                                  'USD',
                                  isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where  
                                              IDCab=cc2.IDCAB And CoMoneda=SD.CoMoneda),SD.TC))
                             else        
                             -- Cambio 1
                              dbo.FnCambioMoneda(isnull(ODS.NetoProgram,ODS.CostoReal),ODS.IDMoneda,'USD',    
                            isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=cc2.IDCAB And     
                             CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))
                             End            
                             --End     
                            Else      
             -- Cambio 1
                             dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',    
                            isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=CC2.IDCAB And     
                             CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))
End      
                             As CompraNetoUSD,                              
                                        ODS.TotImpto,                  
                                        ODS.IDMoneda as Moneda,              
                                        --ODS.CostoReal+ODS.TotImpto as Total ,               
                                        sd.idTipoOC,                  
                                        --SCot.IDProveedor as IDProveedorCot,          
                                        --PCot.NombreCorto as DescProveedorCot,                  
                                        --SDCot.IDServicio_Det                  
                                        ODS.IDServicio_Det,                  
                                        O.IDProveedor as IDProveedorOpe                  
                                        ,SD.IDServicio                  
  FROM OPERACIONES_DET_DETSERVICIOS ODS                      
                                        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det AND ODS.IDServicio=OD.IDServicio                  
                                        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=CC4.IDCAB And Year(CC4.FechaOut)= Cast(@Anio as int)               
                                        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              
                                        LEFT JOIN COTICAB CC2 ON CC2.IDCAB=O.IDCab                  
                                        LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor           
                                         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Cot=SD.IDServicio_Det                  
                                         LEFT join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc                 
                                        Left Join MAUBIGEO ubPI On p.IDCiudad=ubPI.IDubigeo  
                                        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo  
                                        Where not ODS.IDProveedor_Prg is null and not P.NombreCorto is null                  
                                         And isnull(p.IDUsuarioTrasladista,'0000')='0000'      
                                         And IsNull(ods.IDProveedor_Prg,'') <> '001935'--SETOURS CUSCO                  
                                         And IsNull(ODS.Activo,0)=1
                                        and CC2.estado<>'X' --And Year(cc2.FechaOut) = Cast(@Anio as int)
                                        ) AS X                  
                                     ) AS Y                  
                                    GROUP BY Y.IDProveedor,Y.Moneda,Y.idTipoOC       
                                    ) as Z                  
                                   ) AS XX    
                                  ) AS YY                  
                              ) AS ZZ                    
                                   WHERE ZZ.CompraNeto>0   and not ZZ.NroOrdenServicio is null
                             ) AS A   ) ,0)

   FROM COTICAB CC4 --Left Join DEBIT_MEMO db On db.IDCab = CC.IDCAB                 
   Inner Join MACLIENTES c On CC4.IDCliente=c.IDCliente And Year(CC4.FechaOut)= Cast(@Anio as int)
   Left Join MAUBIGEO uc On C.IDCiudad=uc.IDubigeo  
   Left Join MAUBIGEO up On uc.IDubigeo=up.IDubigeo  
   WHERE (cc4.CoTipoVenta='RC' And CC4.Estado='A' or (cc4.Estado='X' and cc4.IDCAB in (select distinct IDCAB from DEBIT_MEMO where IDCab=cc4.IDCAB and IDEstado='PG')))
   Or cc4.CoTipoVenta = 'DR'
   ) AS X      
 --WHERE --x.FecOutPeru
 --BETWEEN '01/01/2015' AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/2015',103) as datetime)))--'31/12/2014 23:59:59'      
 --BETWEEN '01/01/'+@Anio/*'2015'*/ AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/'+@Anio/*2015'*/,103) as datetime)))--'31/12/2014 23:59:59'      
 
 ) AS Y  
 ) AS Z  
 --where Not Exists(select * from VentasReceptivoMensualDet where ParamAnio=@Anio)

-- PRINT 'HOJA ACUMULARO - TIPO VENTA [DR-ZICASSO]'
-- Insert into VentasReceptivoMensualDet
-- SELECT FecInicio,  
--   FecOut,  
--   FecOutPeru,  
--   IDCAB,  
--   IDFile,  
--   Titulo,  
   
--   ValorVenta=
--   Case When Estado='X' and IDCAB in             
--	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
--		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
--	Else
--   round(case when FecInicio<GETDATE() /*pasado*/  
--   then  
--    (  
--     --isnull(TotalDM,0)  
--	 case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
--    )  
--   else --futuro  
--    (  
--     ISNULL(ValorVenta2,0)  
--    )  
--   end,0)
--   end,  
--   PoConcretVta,  
--   NroPax,  
--   ESTADO,  
--   IDUsuario,  
--   Responsable,  
--   'Z00001' As IDCliente,  
--   CoPais,  
--   DescPaisCliente,  
--   'ZICASSO' As DescCliente,  
--   --MargenGan=ISNULL(MargenGan,0),  
--   --Utilidad=ISNULL(ValorVenta2*(MargenGan/100),0)--,  

--    --MargenGan= case when FecInicio<GETDATE()
--	MargenGan= case when FecOut<GETDATE()
--			   then
--				(((case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado)/(case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end ))*100
--			   else
--				ISNULL(MargenGan,0)  
--			   end,
				
--   --Utilidad= case when FecInicio<GETDATE()
--   Utilidad= 
--	Case When Estado='X' and IDCAB in             
--	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
--		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
--	Else
--		case when FecOut<GETDATE()
--			   then
--				  (case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado
--			   else
--				  ISNULL(ValorVenta2*(MargenGan/100),0)
--			   end
--	End,
    
--    --Total_Proy=case when FecInicio<GETDATE() /*pasado*/  
--	Total_Proy=
--	Case When Estado='X' and IDCAB in             
--	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
--		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
--	Else
--		case when FecOut<GETDATE() /*pasado*/  
--		then      
--			case when PoConcretVta<100 then ISNULL(ValorVenta2,0)*(PoConcretVta/100) else 0 end        
--		else --futuro        
--			case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then 0 else TotalCotiz end        
--		end  
--	End 
--   --, Total_Concret=case when FecInicio<GETDATE() /*pasado*/  
--   , Total_Concret=
--	Case When Estado='X' and IDCAB in             
--	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
--		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
--	Else

--	   case when FecOut<GETDATE() /*pasado*/  
--	   then  
--		(  
--		 case when PoConcretVta=100 then ISNULL(ValorVenta2,0) else 0 end  
--		)  
--	   else --futuro  
--		(  
--		 case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then TotalDM else 0 end  
--		)  
--	   end
--	End,
--  SumaProy=
--  	Case When Estado='X' and IDCAB in             
--	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
--		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
--	Else
--		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
--	End
--  ,  SumaProy2=
--  	Case When Estado='X' and IDCAB in             
--	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
--		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
--	Else

--		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
--	End
----,  SumaProy=round(( round((case when FecInicio<GETDATE() then (isnull(TotalDM,0))  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
--      ,Dias
--   ,Edad_Prom
--   ,Total_Programado,
--   @Anio
--   --
--   ,[dbo].[FnDevolverCategoriaPredominante_File](IDFile)
--   --
-- FROM  
-- (     
-- SELECT Y.*,  
  
--  ISNULL(Y.ValorVenta*(Y.MargenGan/100),0) AS Utilidad  
--  ,ValorVenta2=case when Y.TotalDM>0 then (CASE WHEN ABS(Y.TotalDM-Y.TotalCotiz)>=Y.TotalDM_Porc THEN Y.TotalCotiz ELSE Y.TotalDM END) else Y.TotalCotiz end  
    
-- FROM     
-- (  
--   SELECT       
--   FecInicio,  
--   FecOutPeru as FecOut, 
--   Left(CONVERT(varchar,FecOutPeru,112),6) AS FecOutPeru,      
--   IDCAB, IDFile, Titulo,     
--   --isnull((Select sum(total*nropax) From Cotidet Where idcab=X.idcab),0) as ValorVenta       
    
--   Case When EstadoFacturac='F' Then X.TotalDM    
--   Else  --total de cotizacion  
--     Case When ExisteHotel=1 Then    
--      Case When EsTotalxPax=1 Then    
--       --isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
--	   isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)
--	   IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
--	    from COTIVUELOS cv 
--		Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0)
--      Else    
--		--X.TotalSimple+X.TotalDoble+X.TotalTriple              
--			case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
--					X.TotalSimple+X.TotalDoble+X.TotalTriple 
--					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
--					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
--						 IsNull((Select Sum(Monto) from (
--						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--							),0) End
--      End    
--     Else    
--    -- X.NroPax * X.TotalCotiz       
--    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
--      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
--	  ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
--	  	  IsNull((Select Sum(Monto) from (
--	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--		Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--		),0)
--     End     
--   End 
--   as ValorVenta ,    
     
--   PoConcretVta,  
--   --EsTotalxPax,ExisteHotel,       
--   --NroPax,  
--   NroPax=(NroPax+isnull(NroLiberados,0)),  
--   Estado ,  
--   IDUsuario,  
--   Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
--   , IDCliente, CoPais,  DescPaisCliente,  DescCliente,  
        
--   case when X.TotalDobleNeto = 0 then 0                  
--   else                                                 
--   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                                 
--   End as MargenGan       
        
--   ,TotalDM=isnull(X.TotalDM,0)  
-- ,TotalDM_Porc=X.TotalDM*0.15  
--   ,TotalCotiz=  Case When ExisteHotel=1 Then    
--      Case When EsTotalxPax=1 Then    
--    isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
--  Else    
--		--X.TotalSimple+X.TotalDoble+X.TotalTriple              
--			case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
--					X.TotalSimple+X.TotalDoble+X.TotalTriple 
--					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
--					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) 
--						 IsNull((Select Sum(Monto) from (
--						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--							),0)
--					end
--      End    
--     Else    
--    -- X.NroPax * X.TotalCotiz       
--    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
--      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
--	  ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz)+ --+ dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
--	  	  IsNull((Select Sum(Monto) from (
--	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--		Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--		),0)
--     End     
       
--   ,TotalCotiz_Porc=  (Case When ExisteHotel=1 Then    
--          Case When EsTotalxPax=1 Then    
--				isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
--          Else    
--				--X.TotalSimple+X.TotalDoble+X.TotalTriple              
--					case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
--					X.TotalSimple+X.TotalDoble+X.TotalTriple 
--					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
--					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) 
--						  IsNull((Select Sum(Monto) from (
--						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--							),0)
--					end
--          End    
--         Else    
-- -- X.NroPax * X.TotalCotiz       
--    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
--        --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
--		((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
--			 IsNull((Select Sum(Monto) from (
--			   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--				(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--				from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--				Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--				),0)
--         End     
--       )*0.15  
      
--      ,Dias
--   ,Edad_Prom
--   ,Total_Programado
--   FROM       
--   (      
--   SELECT cc4.*,      
--   IsNull((Select SUM(Total) From DEBIT_MEMO Where IDCab=cc4.idcab and IDEstado<>'AN'),0)
--   --+ Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc4.idcab and IDEstado<>'AN') Then dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) Else 0 End as TotalDM,    
--   + Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc4.idcab and IDEstado<>'AN') Then --dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) Else 0 End as TotalDM,
--   	  IsNull((Select Sum(Monto) from (
--	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--		Where cv.IDCAB=CC4.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--		),0) Else 0 End as TotalDM,    
--   --isnull((Select Max(Dia) From COTIDET cd1        
--   --Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo                 
--   --And u1.IDPais='000323'       
--   --Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det              
--   --Where IDCAB=cc.IDCAB /*And SD1.idTipoOC<>'005'*/),cc.FechaOut) as FecOutPeru,      
     
--   cc4.FechaOut as FecOutPeru,      
--   --((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB) *((Isnull(cc4.Twin,0)+Isnull(cc4.Matrimonial,0))) *2) + dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) as TotalDoble,
--   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB) *((Isnull(cc4.Twin,0)+Isnull(cc4.Matrimonial,0))) *2) + --dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,1) as TotalDoble,
--   	  IsNull((Select Sum(Monto) from (
--	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
--		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
--	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
--		Where cv.IDCAB=CC4.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
--		),0) as TotalDoble,

--   IsNull((select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) * Isnull(cc4.Simple,0) as TotalSimple,                                                    
--   IsNull((select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) * (Isnull(cc4.Triple,0)*3) as TotalTriple,      
--   IsNull((select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) As TotalCotiz, --As TotalCotiz,
--   --dbo.FnEsTotalxPax(CC.IDCAB) as EsTotalxPax,    
--     Cast((Case When (select sum(Total) from COTIPAX where IDCab=cc4.IDCAB)=
--				  ((NroPax+isnull(NroLiberados,0)) * (SELECT SUM(TOTAL) FROM COTIDET WHERE IDCAB=cc4.IDCAB))
--				  then 0 else 1 End
--		) 
--	as bit) as EsTotalxPax, 
--   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                         
--   Where cd.IDCAB = cc4.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,      
        
--   --(select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,0) as TotalDobleNeto, --as TotalDobleNeto,      
--   IsNull((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0)+ --dbo.FnDevuelveMontoTotalxPaxVuelos(cc4.IDCAB,0) 
--   IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
--	    from COTIVUELOS cv 
--		Where cv.IDCAB=CC4.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0) as TotalDobleNeto, --as TotalDobleNeto,      

--   IsNull((select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) as TotalSimpleNeto,--as TotalSimpleNeto,      
--   IsNull((select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc4.IDCAB),0) as TotalTripleNeto,--as TotalTripleNeto,
--   IsNull((select SUM(TotImpto) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) as TotalImpto,      
--   IsNull((select SUM(CostoReal) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) as TotalCostoReal,      
--   IsNull((select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = cc4.IDCAB),0) as TotalLib,  
--   up.IDubigeo as CoPais,up.Descripcion as DescPaisCliente, c.RazonComercial as DescCliente  
   
--   ,Dias=IsNull(DATEDIFF(day,FecInicio,FechaOut),0)+1
--   ,Edad_Prom=isnull(dbo.FnEdadPromedio_Pax(cc4.IDCab),0),
--   --,Total_Programado=[dbo].[FnDevolverTotalProgramacion_Coti](cc.IDCAB,'USD')
--   --,Total_Programado=dbo.FnDevolverTotal_Preliquidacion_CotiCab(cc.IDCAB,'')
--   Total_Programado= IsNull(dbo.FnDevolverTotal_Entradas_Presupuesto_CotiCab(cc4.IDCAB,''),0)  --as Costo --7s
--				     +IsNull(dbo.FnDevolverTotal_VuelosyBuses_CotiCab(cc4.IDCAB,''),0) --as Costo -- 0-5s
--					 +IsNull(dbo.FnDevolverTotal_Interno_CotiCab(cc4.IDCAB,''),0) --as Costo --0-5s
--					 +IsNull(dbo.FnDevolverTotal_Voucher_CotiCab(cc4.IDCAB,''),0) -- as Costo -- 4-8s
--					 +   IsNull((select IsNull(Sum(Total),0)
--                           FROM                  
--                             (         
--                             SELECT                   
--            IsNull(ZZ.CompraNetoUSD,0)+IsNull(ZZ.IGVCosto,0)+IsNull(ZZ.IGV_SFE,0) AS Total                  
--                             FROM                  
--                         (                 
--                              SELECT                   

--                                  YY.NroOrdenServicio,                  
--                                  YY.CompraNeto,YY.CompraNetoUSD, YY.IGVCosto,                   
--                                  YY.IGV_SFE, 
--                                  YY.Moneda, YY.idTipoOC,YY.Margen,                                  
--                                  YY.CompraNetoUSD+IGVCosto as CostoBaseUSD                         
--                              FROM                  
--              (                  
--                                  SELECT XX.IDProveedor,XX.NroOrdenServicio,                  
--                                         XX.CompraNeto,XX.CompraNetoUSD, XX.IGVCosto,
--                                                 Case When idTipoOC='001' Then                   
--                                     XX.CompraNeto*(@IGV/100)                   
--                                    Else                    
--                                     Case When XX.idTipoOC='006' Then (XX.CompraNeto*(@IGV/100))*0.5 Else 0 End                  
--                                    End as IGV_SFE,                   
--                                   XX.Moneda, XX.idTipoOC,  XX.Margen        
--                                  FROM                  
--                                   (       
--                                   SELECT Z.IDProveedor,
--                                  (Select substring(CoOrden_Servicio,1,8)+'-'+substring(CoOrden_Servicio,9,6) From ORDEN_SERVICIO Where IDCab=CC4.IDCAB And IDProveedor=Z.IDProveedor) AS NroOrdenServicio,                    
--                           Z.CompraNeto, Z.CompraNetoUSD, Z.IGVCosto,Z.Moneda,                   
--                           Z.idTipoOC,  Z.Margen                  
--                           FROM          
--                                    (                  
--                                    SELECT  Y.IDProveedor,
--                                    SUM(Y.CompraNeto) AS CompraNeto,   sum(Y.CompraNetoUSD) as CompraNetoUSD,              
--                                    SUM(Y.IGVCosto) AS IGVCosto, Moneda,                                      
--                                    Y.idTipoOC,                   
--                                    AVG(Y.Margen) AS Margen         
--                                    FROM                  
--                                     (                     
--                                     SELECT X.*,                  
--                                   Case When LTRIM(rtrim(''))='' Then 
--                -- Cambio 2
--                     dbo.FnPromedioPonderadoMargen(CC4.IDCAB, X.IDProveedorOpe) 
--                                         Else
--                                               @MargenInterno
--                                        End AS Margen,                      
--                                        Case When idTipoOC='002' Then            
--                              CompraNeto*(@IGV/100)             
--                                  Else            
--                              0            
--                             End as IGVCosto            
--                             FROM                  
--                                        (                  
--                                        SELECT ODS.IDProveedor_Prg as IDProveedor,                 
--                                        ODS.IDOperacion_Det,                  
--                            Case When ODS.IngManual=0 Then             
--                                  Case When isnull(NetoCotizado,0) = 0 then  
--          -- Cambio 1
--                                        dbo.FnCambioMoneda(
--                                                      IsNull((Select 
--                                                           Case When sd6.Monto_sgl is null then 
--                                                                          (select Monto from MASERVICIOS_DET_COSTOS
--                                                                          where IDServicio_Det=sd6.IDServicio_Det And
--                                                                          (OD.NroPax+isnull(od.NroLiberados,0) between PaxDesde and PaxHasta)) 
--                                                                   else
--                                                                   IsNull(Monto_sgls,Monto_sgl) * (OD.NroPax+isnull(od.NroLiberados,0)) End
--                                                      from MASERVICIOS_DET sd6 
--                                                       where sd6.IDServicio_Det=sd.IDServicio_Det),0)
--                                                      ,
--                                        SD.CoMoneda,
--                                        ods.IDMoneda,
--                                        isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=CC2.IDCAB And     
--                                                     CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))

--                                  else                            
--                              ods.NetoCotizado      
--                                  End             
--                            Else      
--                             ODS.NetoProgram      
--                            End      
--                             --End            
--                             As CompraNeto,                                        
--                            Case When ODS.IngManual=0 Then             
--  Case When isnull(NetoCotizado,0) = 0 then                            
--                             -- Cambio 1
--                    dbo.FnCambioMoneda(          
--                                                      IsNull((Select 
--                                                                   Case When sd6.Monto_sgl is null then 
--                                                                          (select Monto from MASERVICIOS_DET_COSTOS
--                                                                          where IDServicio_Det=sd6.IDServicio_Det And
--                                                     (OD.NroPax+isnull(od.NroLiberados,0) between PaxDesde and PaxHasta)) 
--                                                                   else
--           IsNull(Monto_sgls,Monto_sgl) * (OD.NroPax+isnull(od.NroLiberados,0)) End
--                             from MASERVICIOS_DET sd6 
--                                                       where sd6.IDServicio_Det=sd.IDServicio_Det),0),
--                                  SD.CoMoneda,
--                                  'USD',
--                                  isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where  
--                                              IDCab=cc2.IDCAB And CoMoneda=SD.CoMoneda),SD.TC))
--                             else        
--                             -- Cambio 1
--                              dbo.FnCambioMoneda(isnull(ODS.NetoProgram,ODS.CostoReal),ODS.IDMoneda,'USD',    
--                            isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=cc2.IDCAB And     
--                             CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))
--                             End            
--                             --End     
--                            Else      
--                            -- Cambio 1
--                             dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',    
--                            isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=CC2.IDCAB And     
--                             CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))
--                           End      
--                             As CompraNetoUSD,                              
--                                        ODS.TotImpto,                  
--                                        ODS.IDMoneda as Moneda,              
--                                        --ODS.CostoReal+ODS.TotImpto as Total ,               
--                                        sd.idTipoOC,                  
--                                        --SCot.IDProveedor as IDProveedorCot,          
--                                        --PCot.NombreCorto as DescProveedorCot,                  
--                                        --SDCot.IDServicio_Det                  
--                                        ODS.IDServicio_Det,                  
--                                        O.IDProveedor as IDProveedorOpe                  
--                                        ,SD.IDServicio                  
--  FROM OPERACIONES_DET_DETSERVICIOS ODS                      
--                                        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det AND ODS.IDServicio=OD.IDServicio                  
--                                        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=CC4.IDCAB And Year(CC4.FechaOut)= Cast(@Anio as int)               
--                                        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              
--                                        LEFT JOIN COTICAB CC2 ON CC2.IDCAB=O.IDCab                  
--                                        LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor           
--                                         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Cot=SD.IDServicio_Det                  
--                                         LEFT join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc                 
--                                        Left Join MAUBIGEO ubPI On p.IDCiudad=ubPI.IDubigeo  
--                                        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo  
--                                        Where not ODS.IDProveedor_Prg is null and not P.NombreCorto is null                  
--             And isnull(p.IDUsuarioTrasladista,'0000')='0000'      
--                                         And IsNull(ods.IDProveedor_Prg,'') <> '001935'--SETOURS CUSCO               
--                                         And IsNull(ODS.Activo,0)=1
--               and CC2.estado<>'X' --And Year(cc2.FechaOut) = Cast(@Anio as int)
--                                        ) AS X                  
--                                     ) AS Y                  
--                                    GROUP BY Y.IDProveedor,Y.Moneda,Y.idTipoOC       
--                                    ) as Z                  
--                                   ) AS XX    
--                                  ) AS YY                  
--                              ) AS ZZ                    
--                                   WHERE ZZ.CompraNeto>0   and not ZZ.NroOrdenServicio is null
--                             ) AS A   ) ,0)

--   FROM COTICAB CC4 
--   Inner Join MACLIENTES c On CC4.IDCliente=c.IDCliente And Year(CC4.FechaOut)= Cast(@Anio as int)
--   Left Join MAUBIGEO uc On C.IDCiudad=uc.IDubigeo  
--   Left Join MAUBIGEO up On uc.IDubigeo=up.IDubigeo  
--   WHERE cc4.CoTipoVenta='DR'
--   -- And CC4.Estado='A' or (cc4.Estado='X' and cc4.IDCAB in (select distinct IDCAB from DEBIT_MEMO where IDCab=cc4.IDCAB and IDEstado='PG'))     
--   ) AS X      
-- ) AS Y  
-- ) AS Z  


 Print 'VentasReceptivoMensualDet - ok'  

  
 Insert into VentasReceptivoMensual
 SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,            
 --Case When SumaProy<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Case When ImporteUSDAnioAnter<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/ImporteUSDAnioAnter      
 Else      
 100      
 End as VarImporteUSD,      
    
 CantPax,        
 CantPaxAnioAnter,      
 Case When CantPax<>0 Then      
 ((CantPax-CantPaxAnioAnter)*100)/CantPax      
 Else      
 100      
 End as VarCantPax,      
 CantFiles,        
 CantFilesAnioAnter,      
 Case When CantFiles<>0 Then      
 ((CantFiles-CantFilesAnioAnter)*100)/CantFiles      
 Else      
 100      
 End as VarCantFiles,  
  SumaProy --=ROUND(SumaProy,0) 
 ,Dias_Dif
 ,Edad_Prom   
 ,@Anio
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy) as SumaProy  
 ,AVG(dias) as Dias_Dif
 --,AVG(y.edad_prom) as Edad_Prom
 ,isnull(AVG(CASE WHEN y.edad_prom <> 0 THEN y.edad_prom ELSE NULL END),0) as Edad_Prom
 --AVG (CASE WHEN Value <> 0 THEN Value ELSE NULL END)
 FROM       
  VentasReceptivoMensualDet AS Y       
  WHERE Y.ParamAnio=@Anio
 GROUP BY FecOutPeru       
 ) AS Z      
  
  Print 'VentasReceptivoMensual - ok' 

 Insert into VentasReceptivoMensual_Sin_Porc
 SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
 --Case When SumaProy<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Case When ImporteUSDAnioAnter<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/ImporteUSDAnioAnter      
 Else      
 100      
 End as VarImporteUSD,      
    
 CantPax,        
 CantPaxAnioAnter,      
 Case When CantPax<>0 Then      
 ((CantPax-CantPaxAnioAnter)*100)/CantPax      
 Else      
 100      
 End as VarCantPax,      
 CantFiles,        
 CantFilesAnioAnter,      
 Case When CantFiles<>0 Then      
 ((CantFiles-CantFilesAnioAnter)*100)/CantFiles      
 Else      
 100      
 End as VarCantFiles,  
  SumaProy --=ROUND(SumaProy,0) 
 ,Dias_Dif
 ,Edad_Prom 
 ,@Anio  
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Sin_Porc,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,   
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy2) as SumaProy  
 ,AVG(dias) as Dias_Dif
 --,AVG(y.edad_prom) as Edad_Prom
 ,isnull(AVG(CASE WHEN y.edad_prom <> 0 THEN y.edad_prom ELSE NULL END),0) as Edad_Prom
 --AVG (CASE WHEN Value <> 0 THEN Value ELSE NULL END)
 FROM       
  VentasReceptivoMensualDet AS Y 
 Where Y.ParamAnio=@Anio      
 GROUP BY FecOutPeru    
 ) AS Z          

 Print 'VentasReceptivoMensual_Sin_Porc - ok'
      
 PRINT 'VENTA ANUAL USD'      
 
 Insert into VentasporEjecutivoAnio
 SELECT IDUsuario,Nombre, VentaAnualUSD, VentaAnualUSDAnioAnter,       
 Case When VentaAnualUSD<>0 then      
 ((VentaAnualUSD-VentaAnualUSDAnioAnter)*100)/VentaAnualUSD      
 Else      
 100      
 End as VarVentaAnualUSD,CantFilesAnioAnter      ,@Anio 
 FROM      
 (      
 --SELECT D.IDUsuario,U.Nombre, SUM(D.VALORVENTA) AS VentaAnualUSD,       
 SELECT D.IDUsuario,U.Nombre, SUM(D.SumaProy) AS VentaAnualUSD,       
isnull((Select isnull(v1.SsImporte,0) From RPT_VENTA_EJECUTIVO v1 Where v1.NuAnio=@AnioAnter/*'2014'*/ And v1.CoUserEjecutivo=D.IDUsuario),0) AS VentaAnualUSDAnioAnter ,      
 isnull((Select isnull(v1.QtNumFiles,0) From RPT_VENTA_EJECUTIVO v1 Where v1.NuAnio=@AnioAnter/*'2014'*/ And v1.CoUserEjecutivo=D.IDUsuario),0) AS CantFilesAnioAnter       
 FROM  VentasReceptivoMensualDet D       
 INNER JOIN MAUSUARIOS U ON D.IDUSUARIO=U.IDUsuario      
 AND ((IdArea='VS' AND Nivel='VTA') OR (IdArea='OP' AND Nivel='OPE') or Nivel='GER')       
 AND Activo='A' AND U.Siglas<>'MST'
 Where D.ParamAnio=@Anio     
 group by D.IDUsuario,U.Nombre       
 ) as X      
 order by 2 desc      
      
      
 Declare @FecOutPeru char(6),@ImporteUSD numeric(12,2),@ImporteUSDAnioAnter numeric(12,2),       
 @ImporteMesUSD numeric(12,2)=0,@ImporteMesUSDAnioAnter numeric(12,2)=0,      
 @tiCont tinyint=1     
    
 --Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,ImporteUSD,ImporteUSDAnioAnter       
 Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,SumaProy,ImporteUSDAnioAnter       
 From  VentasReceptivoMensual where ParamAnio=@Anio Order by cast(FecOutPeru as int)      
 Open curVentasReceptivoMensual      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru,@ImporteUSD,@ImporteUSDAnioAnter       
 While @@FETCH_STATUS=0      
 Begin      
 Set @ImporteMesUSD+=@ImporteUSD      
 Set @ImporteMesUSDAnioAnter+=@ImporteUSDAnioAnter      
 If @tiCont=1      
 Insert into VentasReceptivoMensualAcumulado
 Select @FecOutPeru as FecOutPeru, @ImporteMesUSD as ImporteMesUSD, @ImporteMesUSDAnioAnter as ImporteMesUSDAnioAnter,@Anio      
 --Into  VentasReceptivoMensualAcumulado
 Else      
 Insert into  VentasReceptivoMensualAcumulado      
 Values(@FecOutPeru, @ImporteMesUSD, @ImporteMesUSDAnioAnter,@Anio)      
      
 Set @tiCont+=1      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru,@ImporteUSD,@ImporteUSDAnioAnter       
 End      
      
 Close curVentasReceptivoMensual      
 Deallocate curVentasReceptivoMensual      
      
 --Hoja Turnover      
 PRINT 'HOJA Turnover'      
      
 Insert into VentasxCliente_Anual
 Select Anio,  
 IDCliente,  
 DescCliente,  
 ImporteUSD,  
 ImporteUSDAnioAnter,    

   Case When ImporteUSDAnioAnter<>0 Then            
	case when ImporteUSD>=ImporteUSDAnioAnter
	then
		(((ImporteUSD)*100)/ImporteUSDAnioAnter)-100      
	else
		((((ImporteUSDAnioAnter)*100)
		/
		--ImporteUSD
		Case When ImporteUSD = 0 then 1 else ImporteUSD End
		)*-1)+100
	end     
 Else      
 0    
 End as Crecimiento,

 CantFiles,       
 CantFilesAnioAnter,
 
 TicketFileAnioAnter= CASE WHEN CantFilesAnioAnter=0 THEN 0 ELSE ImporteUSDAnioAnter/CantFilesAnioAnter END ,
 
 PromMargen,  
 PromMargenAnioAnter,       
      
 (PromMargen /100) * ImporteUSD as Utilidad,      
 (PromMargenAnioAnter /100) * ImporteUSDAnioAnter as UtilidadAnioAnter      
 ,Dias
 ,Edad_Prom
 ,@Anio
 , TicketFile= CASE WHEN CantFiles=0 THEN 0 ELSE ImporteUSD/CantFiles END 
 From      
 (      
 select LEFT(fecoutperu,4) as Anio,CUR.IDCliente,      
 Case When ISNULL(c.RazonComercial,'')='' Then c.RazonSocial Else c.RazonComercial  End as DescCliente,       
 --SUM(cur.ValorVenta) as ImporteUSD,      
 SUM(cur.SumaProy) as ImporteUSD,      
 --SUM(cur.ValorVenta)+100 as ImporteUSDAnioAnter ,      
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter/*'2014'*/ And 
 CoCliente=cur.IDCliente),0) as ImporteUSDAnioAnter,       
      
 COUNT(*) as CantFiles,      
 
 CantFilesAnioAnter=isnull((SELECT distinct sum(QtNumFiles) FROM RPT_VENTA_CLIENTES where NuAnio=@AnioAnter And CoCliente=cur.IDCliente),0),
  
 isnull(AVG(ISNULL(MargenGan,0)),0) as PromMargen,      
 --isnull(AVG(MargenGan),0)+10 as PromMargenAnioAnter      
 isnull((Select isnull(SsPromedio,0) From RPT_VENTA_CLIENTES Where NuAnio= @AnioAnter/*'2014'*/ And       
 CoCliente=cur.IDCliente),0) as PromMargenAnioAnter      
 
 ,AVG(Dias) as Dias,
 --AVG(Edad_Prom) as Edad_Prom
isnull(AVG (CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0) as Edad_Prom
 From  (select * from VentasReceptivoMensualDet where IDCliente <> 'Z00001') cur 
 Inner Join MACLIENTES c On cur.IDCliente=c.IDCliente  --Or cur.IDCliente='Z00001')
 Where cur.ParamAnio=@Anio 
 group by cur.IDCliente,LEFT(cur.fecoutperu,4),c.RazonSocial,c.RazonComercial      
 ) as X     
 Union
 Select Anio,  
 IDCliente,  
 DescCliente,  
 ImporteUSD,  
 ImporteUSDAnioAnter,    

   Case When ImporteUSDAnioAnter<>0 Then            
	case when ImporteUSD>=ImporteUSDAnioAnter
	then
		(((ImporteUSD)*100)/ImporteUSDAnioAnter)-100      
	else
		((((ImporteUSDAnioAnter)*100)
		/
		--ImporteUSD
		Case When ImporteUSD = 0 then 1 else ImporteUSD End
		)*-1)+100
	end     
 Else      
 0    
 End as Crecimiento,

 CantFiles,       
 CantFilesAnioAnter,
 
 TicketFileAnioAnter= CASE WHEN CantFilesAnioAnter=0 THEN 0 ELSE ImporteUSDAnioAnter/CantFilesAnioAnter END ,
 
 PromMargen,  
 PromMargenAnioAnter,       
      
 (PromMargen /100) * ImporteUSD as Utilidad,      
 (PromMargenAnioAnter /100) * ImporteUSDAnioAnter as UtilidadAnioAnter      
 ,Dias
 ,Edad_Prom
 ,@Anio
 , TicketFile= CASE WHEN CantFiles=0 THEN 0 ELSE ImporteUSD/CantFiles END 
 From      
 (      
 select LEFT(fecoutperu,4) as Anio,CUR.IDCliente,      
 'ZICASSO' as DescCliente,       
 --SUM(cur.ValorVenta) as ImporteUSD,      
 SUM(cur.SumaProy) as ImporteUSD,      
 --SUM(cur.ValorVenta)+100 as ImporteUSDAnioAnter ,      
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter/*'2014'*/ And 
 CoCliente=cur.IDCliente),0) as ImporteUSDAnioAnter,       
      
 COUNT(*) as CantFiles,      
 
 CantFilesAnioAnter=isnull((SELECT distinct sum(QtNumFiles) FROM RPT_VENTA_CLIENTES where NuAnio=@AnioAnter And CoCliente=cur.IDCliente),0),
  
 isnull(AVG(ISNULL(MargenGan,0)),0) as PromMargen,      
 --isnull(AVG(MargenGan),0)+10 as PromMargenAnioAnter      
 isnull((Select isnull(SsPromedio,0) From RPT_VENTA_CLIENTES Where NuAnio= @AnioAnter/*'2014'*/ And       
 CoCliente=cur.IDCliente),0) as PromMargenAnioAnter      
 
 ,AVG(Dias) as Dias,
 --AVG(Edad_Prom) as Edad_Prom
isnull(AVG (CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0) as Edad_Prom
 From  (select * from VentasReceptivoMensualDet where IDCliente = 'Z00001') cur --Inner Join MACLIENTES c On cur.IDCliente=c.IDCliente  --Or cur.IDCliente='Z00001')
 Where cur.ParamAnio=@Anio --And cur.IDCliente='Z00001'
 group by cur.IDCliente,LEFT(cur.fecoutperu,4)--,c.RazonSocial,c.RazonComercial      
 ) as X      
 order by 3      
      
 --HOJA POR EJECUTIVA      
 PRINT 'HOJA POR EJECUTIVA'      
      
 Insert into VentasporEjecutivoMes
 SELECT D.IDUsuario,U.Nombre,D.FecOutPeru,COUNT(*) as CantFiles,SUM(SumaProy) AS VentaAnualUSD     
 ,Prom_Margen=isnull(AVG (CASE WHEN MargenGan <> 0 THEN MargenGan ELSE NULL END),0) ,@Anio
 FROM  VentasReceptivoMensualDet D       
 INNER JOIN MAUSUARIOS U ON D.IDUsuario=U.IDUsuario     
 --AND ((IdArea='VS' AND Nivel='VTA') OR (IdArea='OP' AND Nivel='OPE') or Nivel='GER')       
 AND ((IdArea='VS' AND Nivel in ('VTA','SVE','JAR')) OR (IdArea='OP' AND Nivel='OPE') or Nivel='GER')       
 AND Activo='A' AND U.Siglas<>'MST'    
 Where ParamAnio=@Anio  
 group by D.IDUSUARIO,U.Nombre, D.FecOutPeru      
 Order by U.Nombre, cast(D.FecOutPeru as int)   
       
      
      
Declare @AnioMes char(6), @tiContMes as tinyint, @IDUsuario char(4)      
      
 Declare curEjecutivas cursor for      
 Select distinct IDUsuario From  VentasporEjecutivoMes       
      
 Open curEjecutivas      
 Fetch Next From curEjecutivas Into @IDUsuario      
 While @@FETCH_STATUS=0      
 Begin      
 Set @tiContMes = 1      
 While (@tiContMes<=12)      
 Begin      
 If @tiContMes<10      
  Set @AnioMes=@Anio+'0'+CAST(@tiContMes as CHAR(1))      
 Else      
  Set @AnioMes=@Anio+CAST(@tiContMes as CHAR(2))      
 IF @tiContMes=1      
   
   
  begin  
  If Not Exists(Select Nombre From  VentasporEjecutivoMes Where IDUsuario=@IDUsuario and FecOutPeru=@AnioMes And ParamAnio=@Anio)  
      Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, EneCantFiles,EneVentaMes,ParamAnio)      
     Select distinct IDUsuario,Nombre,0,0.00,@Anio From  VentasporEjecutivoMes Where IDUsuario=@IDUsuario  And ParamAnio=@Anio  
  else  
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, EneCantFiles,EneVentaMes,ParamAnio)      
     Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And ParamAnio=@Anio
      And IDUsuario=@IDUsuario    
  end  
      
 IF @tiContMes=2      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario And ParamAnio=@Anio)      
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, FebCantFiles,FebVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
   --print'febrero'     
   Update  VentasporEjecutivoMes2      
   Set FebCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   FebVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=3      
  Begin    
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, MarCantFiles,MarVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
   --print'febrero'      
   Update  VentasporEjecutivoMes2      
   Set MarCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   MarVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=4     
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)      
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva,AbrCantFiles,AbrVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
   --print'febrero'      
   Update  VentasporEjecutivoMes2      
   Set AbrCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   AbrVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=5      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)      
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, MayCantFiles,MayVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
         
   Update  VentasporEjecutivoMes2      
   Set MayCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   MayVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=6      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, JunCantFiles,JunVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
   --print'febrero'      
   Update  VentasporEjecutivoMes2      
   Set JunCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   JunVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)     
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio     
  End      
 IF @tiContMes=7      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, JulCantFiles,JulVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
   --print'febrero'      
   Update  VentasporEjecutivoMes2      
   Set JulCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   JulVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=8      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, AgoCantFiles,AgoVentaMes,ParamAnio)  
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
   
   Update  VentasporEjecutivoMes2      
   Set AgoCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),
   AgoVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=9      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, SetCantFiles,SetVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
         
   Update  VentasporEjecutivoMes2      
   Set SetCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   SetVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=10      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, OctCantFiles,OctVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario  And ParamAnio=@Anio
  Else      
         
   Update  VentasporEjecutivoMes2      
   Set OctCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   OctVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=11      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva,NovCantFiles,NovVentaMes,ParamAnio)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio     
  Else      
         
   Update  VentasporEjecutivoMes2      
   Set NovCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   NovVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
 IF @tiContMes=12      
  Begin      
  If Not Exists(Select NoEjecutiva From  VentasporEjecutivoMes2 Where IDUsuario=@IDUsuario And ParamAnio=@Anio)
   Insert Into  VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, DicCantFiles,DicVentaMes,ParamAnio)
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD,@Anio From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio
  Else      
         
   Update  VentasporEjecutivoMes2      
Set DicCantFiles=isnull((Select CantFiles From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0),      
   DicVentaMes=isnull((Select VentaAnualUSD From  VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario And ParamAnio=@Anio),0)      
   Where IDUsuario=@IDUsuario And ParamAnio=@Anio
  End      
        
 Set @tiContMes+=1      
 End        
      
 Fetch Next From curEjecutivas Into @IDUsuario      
 End      
 Close curEjecutivas      
 Deallocate curEjecutivas      
      
      
 Update  VentasporEjecutivoMes2 Set EneCantFiles=ISNULL(EneCantFiles,0),      
 EneVentaMes=isnull(EneVentaMes,0),      
 FebCantFiles=isnull(FebCantFiles,0),      
 FebVentaMes=isnull(FebVentaMes,0),      
 MarCantFiles=isnull(MarCantFiles,0),      
 MarVentaMes=isnull(MarVentaMes,0),      
 AbrCantFiles=isnull(AbrCantFiles,0),      
 AbrVentaMes=isnull(AbrVentaMes,0),      
 MayCantFiles=isnull(MayCantFiles,0),      
 MayVentaMes=isnull(MayVentaMes,0),      
 JunCantFiles=isnull(JunCantFiles,0),      
 JunVentaMes=isnull(JunVentaMes,0),      
 JulCantFiles=isnull(JulCantFiles,0),      
 JulVentaMes=isnull(JulVentaMes,0),      
 AgoCantFiles=isnull(AgoCantFiles,0),      
 AgoVentaMes=isnull(AgoVentaMes,0),      
 SetCantFiles=isnull(SetCantFiles,0),      
 SetVentaMes =isnull(SetVentaMes,0),      
 OctCantFiles =isnull(OctCantFiles,0),      
 OctVentaMes =isnull(OctVentaMes,0),      
 NovCantFiles =isnull(NovCantFiles,0),      
 NovVentaMes =isnull(NovVentaMes,0),      
 DicCantFiles =isnull(DicCantFiles,0),      
 DicVentaMes =isnull(DicVentaMes,0)     
 Where ParamAnio=@Anio
    
 PRINT 'HOJA VENTAS POR CUENTA'      
    
 
 Insert into VentasporPaisClienteTemp
 Select CoPais,DescPaisCliente,IDCliente as CoCliente,DescCliente,FecOutPeru, SUM(SumaProy) as ValorVenta ,@Anio
 From  VentasReceptivoMensualDet  
 where ParamAnio=@Anio
 group by CoPais,DescPaisCliente,IDCliente,DescCliente,FecOutPeru  
 order by DescPaisCliente,DescCliente,FecOutPeru  


  
   --select 'b' 
     
 Declare @CoCliente char(6), @CoPais char(6)  
   
 Declare curClientes cursor for          
 Select Distinct CoPais,IDCliente From  VentasReceptivoMensualDet  
 where ParamAnio=@Anio
 Open curClientes  
 Fetch Next From curClientes Into @CoPais,@CoCliente      
 While @@FETCH_STATUS=0      
  Begin  
  Set @tiContMes = 1      
  While (@tiContMes<=12)      
   Begin  
   If @tiContMes<10      
    Set @AnioMes=@Anio+'0'+CAST(@tiContMes as CHAR(1))  
   Else      
    Set @AnioMes=@Anio+CAST(@tiContMes as CHAR(2))  
      
   IF @tiContMes=1           
     BEGIN  
     If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente and FecOutPeru=@AnioMes And ParamAnio=@Anio)
      Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,EneVentaMes,ParamAnio)  
       Select distinct CoCliente,DescCliente,CoPais,DescPaisCliente,0.00,@Anio
       From  VentasporPaisClienteTemp  
       Where /*FecOutPeru=@AnioMes And*/ CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
         
     Else  
             
      Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,EneVentaMes,ParamAnio)  
      --Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
	  Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
      From  VentasporPaisClienteTemp
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
     
     End  
      
   IF @tiContMes=2  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
  Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,FebVentaMes,ParamAnio)
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
     
    Else  
     Update  VentasporClienteMes Set FebVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
    End  
   IF @tiContMes=3  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
   Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,MarVentaMes,ParamAnio)
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
     
Else  
     Update  VentasporClienteMes Set MarVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
    End  
      
   IF @tiContMes=4  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,AbrVentaMes,ParamAnio)
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set AbrVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
    End  
    
   IF @tiContMes=5  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,MayVentaMes,ParamAnio)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set MayVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
    End          
      
  
   IF @tiContMes=6  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,JunVentaMes,ParamAnio)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set JunVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
    End  
      
   IF @tiContMes=7  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,JulVentaMes,ParamAnio)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set JulVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
    End          
      
   IF @tiContMes=8  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,AgoVentaMes,ParamAnio)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio 
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set AgoVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
    End  
      
   IF @tiContMes=9  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,SetVentaMes,ParamAnio)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set SetVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
    End           
      
   IF @tiContMes=10  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,OctVentaMes,ParamAnio)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set OctVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio  
    End  
      
   IF @tiContMes=11  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,NovVentaMes,ParamAnio)  
	 Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update  VentasporClienteMes Set NovVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  And ParamAnio=@Anio
    End           
   IF @tiContMes=12  
    Begin  
    If Not Exists(Select CoCliente From  VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Insert Into  VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,DicVentaMes,ParamAnio)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta,@Anio
     From  VentasporPaisClienteTemp  
   Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta 
    Else  
     Update  VentasporClienteMes Set DicVentaMes=(Select ValorVenta From  VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio)  
     Where CoPais=@CoPais And CoCliente=@CoCliente And ParamAnio=@Anio  
    End           
      
   Set @tiContMes+=1  
   End  
    
  Fetch Next From curClientes Into @CoPais,@CoCliente  
  End   
 Close curClientes  
 Deallocate curClientes  
   
 Update  VentasporClienteMes Set EneVentaMes=ISNULL(EneVentaMes,0),  
  FebVentaMes=ISNULL(FebVentaMes,0),  
  MarVentaMes=ISNULL(MarVentaMes,0),  
  AbrVentaMes=ISNULL(AbrVentaMes,0),  
  MayVentaMes=ISNULL(MayVentaMes,0),  
  JunVentaMes=ISNULL(JunVentaMes,0),  
  JulVentaMes=ISNULL(JulVentaMes,0),  
  AgoVentaMes=ISNULL(AgoVentaMes,0),  
  SetVentaMes=ISNULL(SetVentaMes,0),  
  OctVentaMes=ISNULL(OctVentaMes,0),  
  NovVentaMes=ISNULL(NovVentaMes,0),  
  dicVentaMes=ISNULL(dicVentaMes,0)  
 Where ParamAnio=@Anio
   
 Update  VentasporClienteMes Set Total=EneVentaMes+FebVentaMes+MarVentaMes+AbrVentaMes+MayVentaMes+JunVentaMes+JulVentaMes+  
  AgoVentaMes+SetVentaMes+OctVentaMes+NovVentaMes+DicVentaMes
 Where ParamAnio=@Anio  
 
	print 'ventas x pais'
	 Insert into VentasporPais
	 Select 
	 --Row_Number() over (Order By Total desc) As Posicion,
	 CoPais,
	 NoPais=DescPaisCliente,
	 CoRegion=(select CoRegion from dbo.MAUBIGEO where IDubigeo=CoPais),
	 --Total=SUM(ValorVenta),
	 Total=SUM(SumaProy),
	 --
	 Dias_Dif=AVG(Dias),
	 --Edad_Prom=AVG(Edad_Prom)
	 Edad_Prom=isnull(AVG (CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0),
	 --Cant_Clientes=count(idcliente)	,@Anio 
	 Cant_Clientes=count(distinct idcliente)	,@Anio 
	 ,TicketFile=isnull((select sum(ticketfile) from VentasxCliente_Anual v inner join MACLIENTES c on v.IDCliente=c.IDCliente  where anio=@Anio and c.IDCiudad =CoPais),0)
	 From  VentasReceptivoMensualDet
	 Where ParamAnio=@Anio
	 group by  CoPais,DescPaisCliente
	 Order by  Total desc
	
	Insert into VentasporPais_AnioAnter
	SELECT c1.IDCiudad,
		MAUBIGEO.Descripcion,
		MAUBIGEO.CoRegion,
		SsImporte=sum(IsNull(RPT_VENTA_CLIENTES.SsImporte,0)),
		QtDias=AVG(QtDias),
		QtEdad_Prom=isnull(AVG (CASE WHEN QtEdad_Prom <> 0 THEN QtEdad_Prom ELSE NULL END),0)--AVG(QtEdad_Prom)
		 --,Cant_Clientes=count(idcliente),
		 ,Cant_Clientes=count(distinct idcliente),
		 @Anio
		 ,TicketFile=isnull((select sum(ticketfile) from VentasxCliente_Anual v inner join MACLIENTES c on v.IDCliente=c.IDCliente  where anio=@AnioAnter and c.IDCiudad =c1.IDCiudad),0)
		FROM         RPT_VENTA_CLIENTES left JOIN
							  MACLIENTES c1 ON RPT_VENTA_CLIENTES.CoCliente = c1.IDCliente left JOIN
							  MAUBIGEO ON c1.IDCiudad = MAUBIGEO.IDubigeo
		--WHERE     (RPT_VENTA_CLIENTES.NuAnio = '2014')
		WHERE     (RPT_VENTA_CLIENTES.NuAnio = @AnioAnter)
		group by c1.IDCiudad,
		MAUBIGEO.Descripcion,
		MAUBIGEO.CoRegion
	order by sum(SsImporte) desc
	
    Insert into VentasporPais_AnioAnter2
	select
	Row_Number() over (Order By SsImporte desc) As Posicion,
	 IDCiudad,
	 Descripcion,
	 CoRegion,
	 SsImporte,
	 QtDias,
	 QtEdad_Prom
	 ,Cant_Clientes
	 ,@Anio
	 ,TicketFile
	from  VentasporPais_AnioAnter
	Where ParamAnio=@Anio
	Order by  SsImporte desc
	
	Insert into VentasporPais2
	select
	Row_Number() over (Order By Total desc) As Posicion,
	 CoPais,
	 NoPais,
	 CoRegion,
	 Total
	 --
	 ,Dias_Dif
	 ,Edad_Prom
	 ,Cant_Clientes
	 ,@Anio
	 ,TicketFile
	from  VentasporPais
	Where ParamAnio=@Anio
	union 
	
	select 
	Posicion=0,--(select isnull(max(posicion),0)+1 from  VentasporPais ),
	CoPais=IDCiudad,
	NoPais=Descripcion,
	CoRegion,
	Total=0
	--
	,Dias_Dif=0
	,Edad_Prom=0
	,Cant_Clientes
	,@Anio
	,TicketFile
	from 
	 VentasporPais_AnioAnter
	where idciudad not in (select distinct copais from  VentasporPais)	
	And ParamAnio=@Anio
	Order by  Total desc

declare @maxposicion int=(select isnull(max(posicion),0)+1 from  VentasporPais2 Where ParamAnio=@Anio)
declare @maxposicion2 int=(select isnull(max(posicion),0)+1 from  VentasporPais_AnioAnter2 Where ParamAnio=@Anio)

update  VentasporPais2 set posicion=@maxposicion where posicion=0 And ParamAnio=@Anio

	Insert into VentasporPais3
	select
	 Posicion,
	 Posicion_AnioAnt=isnull((select Posicion from  VentasporPais_AnioAnter2 where idciudad=CoPais  And ParamAnio=@Anio),@maxposicion2),
	 CoPais,
	 NoPais,
	 CoRegion,
	 Total,
	 Total_AnioAnt=isnull((select SsImporte from  VentasporPais_AnioAnter2 where idciudad=CoPais  And ParamAnio=@Anio),0),
	 
	 Dias_Actual=isnull(Dias_Dif,0),
	 Edad_Prom_Actual=isnull(Edad_Prom,0),
	 
	 Dias=isnull((select QtDias from  VentasporPais_AnioAnter2 where idciudad=CoPais  And ParamAnio=@Anio),0),
	 Edad_Prom=isnull((select QtEdad_Prom from  VentasporPais_AnioAnter2 where idciudad=CoPais  And ParamAnio=@Anio),0)
	 ,Cant_Clientes=isnull(Cant_Clientes,0)
	 ,Cant_Clientes_Ant=isnull((select Cant_Clientes from  VentasporPais_AnioAnter2 where idciudad=CoPais  And ParamAnio=@Anio),0)
	 ,@Anio
	 ,TicketFile
	 ,TicketFileAnioAnter=isnull((select TicketFile from  VentasporPais_AnioAnter2 where idciudad=CoPais  And ParamAnio=@Anio),0)
	from  VentasporPais2 
	Where ParamAnio=@Anio

	insert into VentasporPais4
	select
	 Posicion,
	 Posicion_AnioAnt,
	 CoPais,
	 NoPais,
	 CoRegion,
	 Region=case when CoRegion='AF' then 'Africa' when CoRegion='AS' then 'Asia'  when CoRegion='CA' then 'Latam'--'S&C-America'
				when CoRegion='SA' then 'Latam' when CoRegion='NA' then 'N-America' when CoRegion='EU' then 'Europe'
				when CoRegion='AU' then 'Australia' else 'Otros' end,
			
	 Total,
	 Total_AnioAnt,
	 Crecimiento=case when Total_AnioAnt=0 then 100 else cast(round(round(((Total*100)/Total_AnioAnt)-100,2),0) as int) end,
	 Dias_Actual,
	 Edad_Prom_Actual,
	 Dias,
	 Edad_Prom
	 ,Cant_Clientes
	 ,Cant_Clientes_Ant,
	 @Anio
	 ,TicketFile
	 ,TicketFileAnioAnter
	from  VentasporPais3
	Where ParamAnio=@Anio


	insert into VentasDebitMemoPendientes
	select c.IDFile,cl.RazonComercial as DescCliente ,d.Responsable as Ejecutivo,
	Total=cast( isnull(dbo.FnCambioMoneda(d.Total,d.IDMoneda,'USD',[dbo].[FnTipoDeCambioVtaUltimoxFecha](d.Fecha)),0) as numeric(8,2)),
	Comments=isnull((select top 1 Descripcion from DEBIT_MEMO_DET where IDDebitMemo=d.IDDebitMemo),''),
	Supervisor=
				isnull((select uj.Nombre + isnull(uj.TxApellidos,'') from MAUSUARIOS u
				inner join MAUSUARIOS uj on u.CoUserJefe=uj.IDUsuario
				where u.IDUsuario=@IDUser),'')
	from DEBIT_MEMO d
	LEFT JOIN COTICAB c on c.IDCAB=d.IDCab
	left join MACLIENTES cl on cl.IDCliente=d.IDCliente
	where IDEstado='PD' AND TOTAL<0
	order by IDFile


	--NUEVO 22/12/2015
	 Insert into VentasReceptivoMensualDet_Web
 SELECT FecInicio,  
   FecOut,  
   FecOutPeru,  
   IDCAB,  
   IDFile,  
   Titulo,    
   

   ValorVenta=
   Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
   round(case when FecOut<GETDATE() /*pasado*/  
	   then  
		(  
		 --isnull(TotalDM,0)  
		  case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
		)  
	   else --futuro  
		(  
		 ISNULL(ValorVenta2,0)  
		)  
	   end,0)
   end,



   PoConcretVta,  
   NroPax,  
   ESTADO,  
   IDUsuario,  
   Responsable,  
   IDCliente,  
   CoPais,  
   DescPaisCliente,  
   DescCliente,  
   MargenGan=ISNULL(MargenGan,0),  
   Utilidad=ISNULL(ValorVenta2*(MargenGan/100),0)--,  
    
  -- , Total_Proy=case when FecInicio<GETDATE() /*pasado*/  
   , 
   Total_Proy=
	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		case when FecOut<GETDATE() /*pasado*/  
		then        
			case when PoConcretVta<100 then ISNULL(ValorVenta2,0)*(PoConcretVta/100) else 0 end        
		else --futuro        
			case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then 0 else TotalCotiz end        
		end  
	End
     
   
   , Total_Concret=
	Case When Estado='X' and IDCAB in            
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else

	   case when FecOut<GETDATE() /*pasado*/  
	   then  
		(  
		 case when PoConcretVta=100 then ISNULL(ValorVenta2,0) else 0 end  
		)  
	   else --futuro  
		(  
		 case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then TotalDM else 0 end  
		)  
	   end  
	End  
--,  SumaProy=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
,  SumaProy=

	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
	End
  
  ,  SumaProy2=
	Case When Estado='X' and IDCAB in             
	(select distinct IDCAB from DEBIT_MEMO where IDCab=z.IDCAB and IDEstado='PG') Then
		(Select Sum(Total) From DEBIT_MEMO Where IDCab=z.IDCAB )
	Else
		round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
	End
    ,@Anio
 FROM  
 (     
 SELECT Y.*,  
  
  ISNULL(Y.ValorVenta*(Y.MargenGan/100),0) AS Utilidad  
  ,ValorVenta2=case when Y.TotalDM>0 then (CASE WHEN ABS(Y.TotalDM-Y.TotalCotiz)>=Y.TotalDM_Porc THEN Y.TotalCotiz ELSE Y.TotalDM END) else Y.TotalCotiz end  

 FROM     
 (  
   SELECT       
   FecInicio,  
   FecOutPeru as FecOut,      
   Left(CONVERT(varchar,FecOutPeru,112),6) AS FecOutPeru,      
   IDCAB, IDFile, Titulo,     
   --isnull((Select sum(total*nropax) From Cotidet Where idcab=X.idcab),0) as ValorVenta       
    
   Case When EstadoFacturac='F' Then X.TotalDM    
   Else  --total de cotizacion  
     Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
       --isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
	   	isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
		   IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
			from COTIVUELOS cv 
			Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0)
      Else    
       --X.TotalSimple+X.TotalDoble+X.TotalTriple              
	   	case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz)+ -- dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
						  IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0) End
      End 
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
					((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
						  IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0)
     End     
   End 
   as ValorVenta ,    
     
   PoConcretVta,  
   --EsTotalxPax,ExisteHotel,       
   --NroPax,  
   NroPax=(NroPax+isnull(NroLiberados,0)),  
   Estado ,  
   IDUsuario,  
   Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
   , IDCliente, CoPais,  DescPaisCliente,  DescCliente,  
        
 case when X.TotalDobleNeto = 0 then 0         
   else                                                 
   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                                 
   End as MargenGan       
        
   ,TotalDM=isnull(X.TotalDM,0)  
   ,TotalDM_Porc=X.TotalDM*0.15  
   ,TotalCotiz=  Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
    isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
      Else    
			--X.TotalSimple+X.TotalDoble+X.TotalTriple              
				case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0) End
      End    
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
	  ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --
	  	  IsNull((Select Sum(Monto) from (
		   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
			(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
			from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
			Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
			),0)
     End     
       
   ,TotalCotiz_Porc=  (Case When ExisteHotel=1 Then    
          Case When EsTotalxPax=1 Then    
				isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
          Else    
				--X.TotalSimple+X.TotalDoble+X.TotalTriple              
					case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + --dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
						  IsNull((Select Sum(Monto) from (
						   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
							(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
							from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
							Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
							),0) End
          End    
         Else    
        -- X.NroPax * X.TotalCotiz       
        --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
        --((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
		((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + -- dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
			  IsNull((Select Sum(Monto) from (
			   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
				(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
				from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
				Where cv.IDCAB=X.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
				),0)
         End     
      )*0.15  
   FROM       
   (      
   SELECT CC.*,      
   (Select SUM(Total) From DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN')
   --+ Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN') Then dbo.FnDevuelveMontoTotalxPaxVuelos(cc.IDCAB,1) Else 0 End as TotalDM,      
   + Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN')    Then 	  
   IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=cc.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0) Else 0 End as TotalDM, 
     
   cc.FechaOut as FecOutPeru,      
   --((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) *((Isnull(cc.Twin,0)+Isnull(cc.Matrimonial,0))) *2) + dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,1) as TotalDoble,
   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) *((Isnull(cc.Twin,0)+Isnull(cc.Matrimonial,0))) *2) 
   + 	  IsNull((Select Sum(Monto) from (
	   (select (IsNull(Tarifa,0)-IsNull(CostoOperador,0)) *
		(select COUNT(*) FROM COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) As Monto
	    from COTIVUELOS cv --Left Join COTITRANSP_PAX ctp On cv.ID=ctp.IDTransporte
		Where cv.IDCAB=CC.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0)) as X
		),0) as TotalDoble,

   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * Isnull(cc.Simple,0) as TotalSimple,                                                    
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * (Isnull(cc.Triple,0)*3) as TotalTriple,      
   (select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = CC.IDCAB) As TotalCotiz, --As TotalCotiz,
 --  dbo.FnEsTotalxPax(CC.IDCAB) as EsTotalxPax,      
  Cast((Case When (select sum(Total) from COTIPAX where IDCab=cc.IDCAB)=
				  ((NroPax+isnull(NroLiberados,0)) * (SELECT SUM(TOTAL) FROM COTIDET WHERE IDCAB=cc.IDCAB))
				  then 0 else 1 End
		) 
  as bit) as EsTotalxPax, 


   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                          
   Where cd.IDCAB = CC.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,      
        
   --(select SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,0) as TotalDobleNeto, --as TotalDobleNeto,      
  (select SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB)+
  IsNull((select Sum((IsNull(Tarifa,0)-IsNull(CostoOperador,0)))
	    from COTIVUELOS cv 
		Where cv.IDCAB=CC.IDCAB And cv.EmitidoSetours=1 and IsNull(cv.CostoOperador,0)>0 and IsNull(Tarifa,0)>0),0) as TotalDobleNeto, --as TotalDobleNeto,      

   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalSimpleNeto,--as TotalSimpleNeto,      
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalTripleNeto,--as TotalTripleNeto,
   (select SUM(TotImpto) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalImpto,      
   (select SUM(CostoReal) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalCostoReal,      
   (select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = cc.IDCAB) as TotalLib,  
   up.IDubigeo as CoPais,up.Descripcion as DescPaisCliente, c.RazonComercial as DescCliente  
   FROM COTICAB CC --Left Join DEBIT_MEMO db On db.IDCab = CC.IDCAB                   
    Inner Join MACLIENTES c On CC.IDCliente=c.IDCliente  
   Left Join MAUBIGEO uc On C.IDCiudad=uc.IDubigeo  
   Left Join MAUBIGEO up On uc.IDubigeo=up.IDubigeo  
  
   WHERE       
   
  --c.IDCliente not in ('000054') AND
   --c.Tipo='WB' 
   c.RazonComercial like 'WEB%'
   AND (
   cc.CoTipoVenta = 'RC' AND CC.Estado='A'       
   or             
   (cc.Estado='X' and cc.IDCAB in             
   (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG') )      )
        
   --And ISNULL(db.IDEstado,'') <> 'AN'          
   ) AS X      
 WHERE x.FecOutPeru      
 --BETWEEN '01/01/2015' AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/2015',103) as datetime)))--'31/12/2014 23:59:59'      
 BETWEEN '01/01/'+@Anio/*'2015'*/ AND dateadd(S,
											 -1,
											  dateadd(d,
													  1,
													  cast(CONVERT(varchar,'31/12/'+@Anio/*2015'*/,103) as datetime))
											   )--'31/12/2014 23:59:59'      
 ) AS Y  
 ) AS Z  


 Insert into VentasReceptivoMensual_Web
 SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
  --Case When ImporteUSD<>0 Then      
  --((ImporteUSD-ImporteUSDAnioAnter)*100)/ImporteUSD      
  --Else      
  --100      
  --End as VarImporteUSD,      
 --Case When SumaProy<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Case When ImporteUSDAnioAnter<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/ImporteUSDAnioAnter      
 Else      
 100      
 End as VarImporteUSD,     
    
   SumaProy --=ROUND(SumaProy,0) 
   ,@Anio 
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Web,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy) as SumaProy  
 FROM       
 VentasReceptivoMensualDet_Web AS Y     
 Where Y.ParamAnio=@Anio    
 GROUP BY FecOutPeru       
 ) AS Z     

 -----------------------------------------------------
 Declare @AnioMes1 char(6), @tiContMes1 as tinyint, @IDUsuario1 char(4)      
       
 Set @tiContMes1 = 1      
 While (@tiContMes1<=12)      
 Begin      
 If @tiContMes1<10      
  Set @AnioMes1=@Anio+'0'+CAST(@tiContMes1 as CHAR(1))      
 Else      
  Set @AnioMes1=@Anio+CAST(@tiContMes1 as CHAR(2))      

  --select @AnioMes

 IF @tiContMes1=1      
   
   
  begin  
  --Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
     -- select @AnioMes1
	  Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
 
  end  
      
 IF @tiContMes1=2 
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
 Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=3      
  Begin    
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=4      
  Begin      
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=5      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=6      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=7      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end    
 IF @tiContMes1=8      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=9      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end   
 IF @tiContMes1=10      
  Begin      
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=11      
  Begin      
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
 IF @tiContMes1=12      
 begin
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web Where FecOutPeru=@AnioMes1 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes1,0,0,100,0,@Anio
  end  
        
 Set @tiContMes1+=1      
 End        
 

 -----------------------------------------------------



 --
  Declare @FecOutPeru0 char(6),@ImporteUSD0 numeric(12,2),@ImporteUSDAnioAnter0 numeric(12,2),       
 @ImporteMesUSD0 numeric(12,2)=0,@ImporteMesUSDAnioAnter0 numeric(12,2)=0,      
 @tiCont0 tinyint=1     

 --Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,ImporteUSD,ImporteUSDAnioAnter       
 Declare curVentasReceptivoMensual Cursor For 
 Select FecOutPeru,SumaProy,ImporteUSDAnioAnter     
 From VentasReceptivoMensual_Web where ParamAnio=@Anio Order by cast(FecOutPeru as int)      
 Open curVentasReceptivoMensual      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru0,@ImporteUSD0,@ImporteUSDAnioAnter0       
 While @@FETCH_STATUS=0      
 Begin      
 Set @ImporteMesUSD0+=@ImporteUSD0      
 Set @ImporteMesUSDAnioAnter0+=@ImporteUSDAnioAnter0     
 If @tiCont0=1      
 Insert into VentasReceptivoMensualAcumulado_Web
 Select @FecOutPeru0 as FecOutPeru, @ImporteMesUSD0 as ImporteMesUSD, @ImporteMesUSDAnioAnter0 as ImporteMesUSDAnioAnter,@Anio

 Else      
 Insert into VentasReceptivoMensualAcumulado_Web      
 Values(@FecOutPeru0, @ImporteMesUSD0, @ImporteMesUSDAnioAnter0,@Anio)      
      
 Set @tiCont0+=1      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru0,@ImporteUSD0,@ImporteUSDAnioAnter0       
 End      
      
 Close curVentasReceptivoMensual      
 Deallocate curVentasReceptivoMensual    
 
 --

  Insert into VentasReceptivoMensual_Web_SinPorc
  SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
 
 --Case When SumaProy<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Case When ImporteUSDAnioAnter<>0 Then ((SumaProy-ImporteUSDAnioAnter)*100)/ImporteUSDAnioAnter      
 Else      
 100      
 End as VarImporteUSD,     
    
   SumaProy --=ROUND(SumaProy,0)    
   ,@Anio
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Sin_Porc_Web,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy2) as SumaProy  
 FROM       
 VentasReceptivoMensualDet_Web AS Y  
 Where Y.ParamAnio=@Anio  
 GROUP BY FecOutPeru		   
 ) AS Z   

 
Declare @AnioMes2 char(6), @tiContMes2 as tinyint, @IDUsuario2 char(4)      
       
 Set @tiContMes2 = 1      
 While (@tiContMes2<=12)      
 Begin      
 If @tiContMes2<10      
  Set @AnioMes2=@Anio+'0'+CAST(@tiContMes2 as CHAR(1))      
 Else      
  Set @AnioMes2=@Anio+CAST(@tiContMes2 as CHAR(2))      

  --select @AnioMes2

 IF @tiContMes2=1      
   
   
  begin  
 -- Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      --select @AnioMes2
	  Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
 
  end  
      
 IF @tiContMes2=2      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=3      
  Begin    
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=4      
  Begin      
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=5      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=6      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=7      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end    
 IF @tiContMes2=8      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=9      
  Begin      
  If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end   
 IF @tiContMes2=10      
  Begin      
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=11      
  Begin      
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
 IF @tiContMes2=12      
 begin
 If Not Exists(Select importeUSD From  VentasReceptivoMensual_Web_SinPorc Where FecOutPeru=@AnioMes2 And ParamAnio=@Anio)  
      Insert Into  VentasReceptivoMensual_Web_SinPorc([FecOutPeru],[ImporteUSD],[ImporteUSDAnioAnter],[VarImporteUSD],[SumaProy],ParamAnio)      
     Select  @AnioMes2,0,0,100,0,@Anio
  end  
        
 Set @tiContMes2+=1      
 End        
 
 --------------------------------------------------------
 
	print 'ventas x pais web'
	 Insert into VentasporPais_Web
	 Select 
	 --Row_Number() over (Order By Total desc) As Posicion,
	 CoPais,
	 NoPais=DescPaisCliente,
	 CoRegion=(select CoRegion from dbo.MAUBIGEO where IDubigeo=CoPais),
	 --Total=SUM(ValorVenta),
	 Total=SUM(SumaProy),
	 --
	 Dias_Dif=AVG(Dias),
	 --Edad_Prom=AVG(Edad_Prom)
	 Edad_Prom=isnull(AVG (CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0),
	 --Cant_Clientes=count(idcliente)	,@Anio 
	 Cant_Clientes=count(distinct v.idcliente)	,@Anio 
	 ,TicketFile=isnull((select sum(ticketfile) from VentasxCliente_Anual v inner join MACLIENTES c on v.IDCliente=c.IDCliente  where anio=@Anio  and c.RazonComercial like 'WEB%' and c.IDCiudad =CoPais ),0)
	 From  VentasReceptivoMensualDet v
	 inner join maclientes c1 on v.IDCliente=c1.IDCliente
	 Where ParamAnio=@Anio and c1.RazonComercial like 'WEB%'
	 group by  CoPais,DescPaisCliente
	 Order by  Total desc
	
	Insert into VentasporPais_AnioAnter_Web
	SELECT c1.IDCiudad,
		MAUBIGEO.Descripcion,
		MAUBIGEO.CoRegion,
		SsImporte=sum(IsNull(RPT_VENTA_CLIENTES.SsImporte,0)),
		QtDias=AVG(QtDias),
		QtEdad_Prom=isnull(AVG (CASE WHEN QtEdad_Prom <> 0 THEN QtEdad_Prom ELSE NULL END),0)--AVG(QtEdad_Prom)
		 --,Cant_Clientes=count(idcliente),
		 ,Cant_Clientes=count(distinct idcliente),
		 @Anio
		 ,TicketFile=isnull((select sum(ticketfile) from VentasxCliente_Anual v inner join MACLIENTES c on v.IDCliente=c.IDCliente  where anio=@AnioAnter  and c.RazonComercial like 'WEB%' and c.IDCiudad =c1.IDCiudad),0)
		FROM         RPT_VENTA_CLIENTES left JOIN
							  MACLIENTES c1 ON RPT_VENTA_CLIENTES.CoCliente = c1.IDCliente left JOIN
							  MAUBIGEO ON c1.IDCiudad = MAUBIGEO.IDubigeo
		--WHERE     (RPT_VENTA_CLIENTES.NuAnio = '2014')
		WHERE     (RPT_VENTA_CLIENTES.NuAnio = @AnioAnter)
		 and c1.RazonComercial like 'WEB%'
		group by c1.IDCiudad,
		MAUBIGEO.Descripcion,
		MAUBIGEO.CoRegion
	order by sum(SsImporte) desc
	
    Insert into VentasporPais_AnioAnter2_Web
	select
	Row_Number() over (Order By SsImporte desc) As Posicion,
	 IDCiudad,
	 Descripcion,
	 CoRegion,
	 SsImporte,
	 QtDias,
	 QtEdad_Prom
	 ,Cant_Clientes
	 ,@Anio
	 ,TicketFile
	from  VentasporPais_AnioAnter_Web
	Where ParamAnio=@Anio
	Order by  SsImporte desc
	
	Insert into VentasporPais2_Web
	select
	Row_Number() over (Order By Total desc) As Posicion,
	 CoPais,
	 NoPais,
	 CoRegion,
	 Total
	 --
	 ,Dias_Dif
	 ,Edad_Prom
	 ,Cant_Clientes
	 ,@Anio
	 ,TicketFile
	from  VentasporPais_Web
	Where ParamAnio=@Anio
	union 
	
	select 
	Posicion=0,--(select isnull(max(posicion),0)+1 from  VentasporPais ),
	CoPais=IDCiudad,
	NoPais=Descripcion,
	CoRegion,
	Total=0
	--
	,Dias_Dif=0
	,Edad_Prom=0
	,Cant_Clientes
	,@Anio
	,TicketFile
	from 
	 VentasporPais_AnioAnter_Web
	where idciudad not in (select distinct copais from  VentasporPais)	
	And ParamAnio=@Anio
	Order by  Total desc

--declare @maxposicion int=(select isnull(max(posicion),0)+1 from  VentasporPais2_Web Where ParamAnio=@Anio)
--declare @maxposicion2 int=(select isnull(max(posicion),0)+1 from  VentasporPais_AnioAnter2_Web Where ParamAnio=@Anio)
set @maxposicion =(select isnull(max(posicion),0)+1 from  VentasporPais2_Web Where ParamAnio=@Anio)
set @maxposicion2 =(select isnull(max(posicion),0)+1 from  VentasporPais_AnioAnter2_Web Where ParamAnio=@Anio)

update  VentasporPais2_Web set posicion=@maxposicion where posicion=0 And ParamAnio=@Anio

	Insert into VentasporPais3_Web
	select
	 Posicion,
	 Posicion_AnioAnt=isnull((select Posicion from  VentasporPais_AnioAnter2_Web where idciudad=CoPais  And ParamAnio=@Anio),@maxposicion2),
	 CoPais,
	 NoPais,
	 CoRegion,
	 Total,
	 Total_AnioAnt=isnull((select SsImporte from  VentasporPais_AnioAnter2_Web where idciudad=CoPais  And ParamAnio=@Anio),0),
	 
	 Dias_Actual=isnull(Dias_Dif,0),
	 Edad_Prom_Actual=isnull(Edad_Prom,0),
	 
	 Dias=isnull((select QtDias from  VentasporPais_AnioAnter2_Web where idciudad=CoPais  And ParamAnio=@Anio),0),
	 Edad_Prom=isnull((select QtEdad_Prom from  VentasporPais_AnioAnter2_Web where idciudad=CoPais  And ParamAnio=@Anio),0)
	 ,Cant_Clientes=isnull(Cant_Clientes,0)
	 ,Cant_Clientes_Ant=isnull((select Cant_Clientes from  VentasporPais_AnioAnter2_Web where idciudad=CoPais  And ParamAnio=@Anio),0)
	 ,@Anio
	 ,TicketFile
	 ,TicketFileAnioAnter=isnull((select TicketFile from  VentasporPais_AnioAnter2_Web where idciudad=CoPais  And ParamAnio=@Anio),0)
	from  VentasporPais2_Web
	Where ParamAnio=@Anio

	insert into VentasporPais4_Web
	select
	 Posicion,
	 Posicion_AnioAnt,
	 CoPais,
	 NoPais,
	 CoRegion,
	 Region=case when CoRegion='AF' then 'Africa' when CoRegion='AS' then 'Asia'  when CoRegion='CA' then 'Latam'--'S&C-America'
				when CoRegion='SA' then 'Latam' when CoRegion='NA' then 'N-America' when CoRegion='EU' then 'Europe'
				when CoRegion='AU' then 'Australia' else 'Otros' end,
			
	 Total,
	 Total_AnioAnt,
	 Crecimiento=case when Total_AnioAnt=0 then 100 else cast(round(round(((Total*100)/Total_AnioAnt)-100,2),0) as int) end,
	 Dias_Actual,
	 Edad_Prom_Actual,
	 Dias,
	 Edad_Prom
	 ,Cant_Clientes
	 ,Cant_Clientes_Ant,
	 @Anio
	 ,TicketFile
	 ,TicketFileAnioAnter
	from  VentasporPais3_Web
	Where ParamAnio=@Anio

End;

