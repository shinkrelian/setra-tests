﻿
--PPMG-20160428-Se aumento el lenght @NumIdentidad varchar(25)
CREATE PROCEDURE [dbo].[COTIPAX_UPD_EXCEL1]
@IDPax int,
@Nombres varchar(50),
@Apellidos varchar(50),
@CoTipoDoc char(3),
@NumIdentidad varchar(25),
@CoNacionalidad char(3),
@FecNacimiento smalldatetime
AS
BEGIN
declare @IDIdentidad char(3)
set @IDIdentidad=(select IDIdentidad from MATIPOIDENT where CodPeruRail=@CoTipoDoc)

declare @IDNacionalidad char(6)
set @IDNacionalidad=(select IDubigeo from MAUBIGEO where CodigoPeruRail=@CoNacionalidad)

UPDATE COTIPAX
SET
Nombres=@Nombres,
Apellidos=@Apellidos,
IDIdentidad=@IDIdentidad,
IDNacionalidad=@IDNacionalidad,
FecNacimiento=@FecNacimiento
WHERE IDPax=@IDPax

END;

