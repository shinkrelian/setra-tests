﻿Create Procedure [dbo].[MANIVELUSUARIO_Upd]
	@IDNivel char(3),
    @Descripcion varchar(50),
    @UserMod char(4)
As
	Set NoCount On
	
	UPDATE MANIVELUSUARIO
           SET Descripcion=@Descripcion
           ,UserMod=@UserMod
           ,FecMod=getdate()
    Where
           IDNivel=@IDNivel
