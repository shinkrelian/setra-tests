﻿Create Procedure dbo.COTICAB_Upd_FechaOperaciones
	@IDCab	int,
	@UserMod	char(4)
As
	Set NoCount On
	
	Update COTICAB Set FechaOperaciones=GETDATE(), FecMod=GETDATE(), UserMod=@UserMod
	Where IDCAB=@IDCab
	
