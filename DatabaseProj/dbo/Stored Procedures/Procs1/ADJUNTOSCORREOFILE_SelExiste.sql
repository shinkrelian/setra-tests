﻿
CREATE Procedure [dbo].[ADJUNTOSCORREOFILE_SelExiste]
	@NuCorreo	int,
	@NoArchAdjunto	varchar(150),
	@pbExists bit Output  
As
	Set Nocount On
	
	Set @pbExists = 0  
   
	If Exists(Select NuCorreo From .BDCORREOSETRA..ADJUNTOSCORREOFILE 
		Where NuCorreo=@NuCorreo And NoArchAdjunto=@NoArchAdjunto)  
		
		Set @pbExists = 1  

