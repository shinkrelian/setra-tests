﻿Create Procedure dbo.ASIGNACION_Upd
@NuAsignacion int,
@TxObservacion varchar(max),
@SsImportePagado numeric(10,2),
@UserMod char(4)
As
Begin
	Set Nocount On
	Update ASIGNACION 
		Set TxObservacion = Case When ltrim(rtrim(@TxObservacion))='' then Null else @TxObservacion End,
			SsImportePagado = @SsImportePagado,
			UserMod = @UserMod,
			FecMod = GETDATE()
	where NuAsignacion = @NuAsignacion
End
