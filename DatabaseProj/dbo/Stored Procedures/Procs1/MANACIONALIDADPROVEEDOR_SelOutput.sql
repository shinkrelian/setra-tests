﻿

Create Procedure [Dbo].[MANACIONALIDADPROVEEDOR_SelOutput]
@IDubigeo char(6),
@IDProveedor Char(6),
@pblnExiste bit output

As
	Set NoCount On
	if Exists(select IDProveedor from MANACIONALIDADPROVEEDOR Where IDubigeo = @IDubigeo And IDProveedor = @IDProveedor)
		set @pblnExiste = 1
	Else
		set @pblnExiste = 0
