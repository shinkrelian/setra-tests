﻿--JRF-20140129-Agregando el IDPais perteneciente al Ubigeo.
CREATE Procedure [dbo].[MAFECHASNAC_Sel_Pk]  
 @Fecha smalldatetime,  
 @IDUbigeo char(6)  
As  
 Set NoCount On  
   
 Select *,u.IDPais From MAFECHASNAC fn Left Join MAUBIGEO u On fn.IDUbigeo = u.IDubigeo
 Where Fecha=@Fecha And fn.IDUbigeo=@IDUbigeo  
