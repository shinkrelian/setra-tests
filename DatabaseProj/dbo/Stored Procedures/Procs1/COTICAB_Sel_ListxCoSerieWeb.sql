﻿CREATE Procedure dbo.COTICAB_Sel_ListxCoSerieWeb
@CoSerieWeb varchar(6)
As
Set NoCount On
Set Language English
Select x.[file_id],x.[day],x.[month],x.[year],
	Case When x.CantPaxCompletos = 0 Then 'open'
		 when x.CantPaxCompletos = x.TotalPax then 'closed' else 'confirmed' End as [status]
from (
Select IDFile as [file_id],day(FecInicio) As [day],Substring(DATENAME(MONTH,FecInicio),1,3) as [month],YEAR(FecInicio) as [year],
	   dbo.FnValidarSoloPaxNombreyApellidos(IDCab) As CantPaxCompletos,NroPax+IsNull(NroLiberados,0) as TotalPax,FecInicio
from COTICAB 
Where Estado='A' And CoSerieWeb=Ltrim(Rtrim(@CoSerieWeb))
And DATEDIFF(D,getdate(),FecInicio) > 0
) As X
Order By x.FecInicio asc
Set Language Spanish
