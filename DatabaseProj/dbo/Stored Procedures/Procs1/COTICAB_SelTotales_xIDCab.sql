﻿CREATE Procedure COTICAB_SelTotales_xIDCab     
@IDCab int    
As    
select IDCAB as IDCAB_Q,NroPax as Pax,NroLiberados as Liberados    
 , Tipo_Lib    
 ,(SELECT isnull( Case When Total=0 Then 0 Else  ((Total - TotalImpuesto - TotalCostoReal) / Total) * 100      
  End, 0) As TotalPorcMargen FROM (        
  SELECT     
  Total+TotalSimple AS TotalSimple,        
  Total+TotalTriple AS TotalTriple,        
  TotalCostoReal,        
  TotalImpuesto,        
  Total        
          
  FROM (Select     
  Sum(cd.Total) as Total,        
  SUM(cd.SSTotal) as TotalSimple,        
  SUM(cd.STTotal) as TotalTriple,        
  Sum(cd.CostoReal) as TotalCostoReal,        
  SUm(cd.TotImpto) as TotalImpuesto        
          
  From COTIDET cd     
  Where cd.IDCAB=@IDCab        
  ) AS X        
 ) AS Y  )    
   As TotalPorcMargen    
 ,     ( SELECT TotalSimple FROM  (          
    SELECT           
    Total+TotalSimple AS TotalSimple,Total          
    FROM          
    (           
    select Sum(d.Total) as Total,          
    SUM(d.SSTotal) as TotalSimple From COTIDET d         
    Where d.IDCAB=@IDCab          
    ) AS X          
   ) AS Y  ) As TotalSimple    
 ,( SELECT TotalTriple FROM (          
    SELECT  Total+TotalTriple AS TotalTriple        
    FROM  (Select Sum(d3.Total) as Total, SUM(d3.STTotal) as TotalTriple         
   From COTIDET d3 Where d3.IDCAB=@IDCab          
    ) AS X          
   ) AS Y ) As TotalTriple    
 , (  SELECT Total FROM (          
      SELECT         
      TotalCostoReal,          
      TotalImpuesto,          
      Total          
      FROM  (          
      Select         
      Sum(d2.Total) as Total,          
      Sum(d2.CostoReal) as TotalCostoReal,          
      SUm(d2.TotImpto) as TotalImpuesto          
      From COTIDET d2         
      Where d2.IDCAB=@IDCab          
      ) AS X          
     ) AS Y  )  As Total   
  ,(select SUM(cd.SSTotal)   
    
  from COTIDET cd Left Join MAPROVEEDORES pr On cd.IDProveedor=pr.IDProveedor   
  Left Join MASERVICIOS S On cd.IDServicio = S.IDServicio          
  Left Join MASERVICIOS_DET Sd On cd.IDServicio_Det = Sd.IDServicio_Det    
  where cd.IDCAB = @IDCab and 
  ((pr.IDTipoProv = '001' And Sd.PlanAlimenticio = 0) or 
   (pr.IDTipoProv = '003' and Sd.ConAlojamiento = 1))) As TotalSS    
 from COTICAB where IDCAB = @IDCab      
