﻿CREATE Procedure [dbo].[MACLIENTES_Sel_List_Correo]    
 @Email varchar(200)
AS    
  Set NoCount On    
  
  SELECT c.* 
  FROM MACLIENTES c left join MACONTACTOSCLIENTE cc on c.IDCliente=cc.IDCliente
  where c.Activo='A'  
  and (cc.Email=@Email OR CC.Email2=@Email OR CC.Email3=@Email)

