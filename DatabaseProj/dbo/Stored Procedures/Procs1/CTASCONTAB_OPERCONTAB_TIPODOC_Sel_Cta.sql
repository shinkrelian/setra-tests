﻿--HLF-20141113-Quitando @CoTipoDoc char(3)
--JHD-20150216- Se agrego el campo TxDefinicion  
--JHD-20150217- Se agrego el parametro @FlProvAdmin para filtrar las cuentas segun si el proveedor es administrativo o no.
--JHD-20150602- Se quito la condicion para proveedores administrativos
CREATE Procedure dbo.CTASCONTAB_OPERCONTAB_TIPODOC_Sel_Cta  
 @CoTipoOC char(3),
 @FlProvAdmin Char(1)=''
 --@CoTipoDoc char(3)  
As  
 Set Nocount On  
   
 Select co.CoCtaContab,co.CoCtaContab+ ' - ' + pc.Descripcion as Descripcion,
 TxDefinicion=ISNULL(TxDefinicion,'')
 From CTASCONTAB_OPERCONTAB_TIPODOC co  
 Inner Join MAPLANCUENTAS pc On co.CoCtaContab=pc.CtaContable  
 Where --(co.CoTipoDoc=@CoTipoDoc Or LTRIM(rtrim(@CoTipoDoc))='') And  
 (co.CoTipoOC=@CoTipoOC Or LTRIM(rtrim(@CoTipoOC))='')   
 --and ((@FlProvAdmin='S' and substring(co.CoCtaContab,0,3)<>'90') or ((@FlProvAdmin='N' and substring(co.CoCtaContab,0,3)='90')) or LTRIM(RTRIM(@FlProvAdmin)) ='')
 Order by pc.Descripcion

