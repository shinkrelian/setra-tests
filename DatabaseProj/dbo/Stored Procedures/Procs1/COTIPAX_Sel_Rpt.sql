﻿CREATE procedure [dbo].[COTIPAX_Sel_Rpt] 
@IDCab int
As
select cast(cp.Apellidos +' / '+ cp.Nombres+' '+cp.Titulo as varchar) as Nombre 
	,ti.Descripcion as TipoDoc,cp.NumIdentidad,u.Gentilicio as Nacionalidad,
	Convert(varchar(10),cp.FecNacimiento,103) as FechaNacimiento
	from COTIPAX cp Left Join MATIPOIDENT ti On cp.IDIdentidad=ti.IDIdentidad
	Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo
where IDCab=@IDCab
