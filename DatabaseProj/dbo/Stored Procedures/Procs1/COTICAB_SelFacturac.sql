﻿--HLF-20140220-Nueva columna cc.EstadoFacturac                    

--HLF-20140224-SUM(Total) From DEBIT_MEMO en vez de SUM(Saldo)                    

--HLF-20140227-Where IDEstado<>'AN'                    

--JRF-20140311-Agregar nueva columna de DescTipoVenta                  

--JRF-20140325-Agregar nueva columna de CoTipoVenta                  

--HLF-20140331-And (convert(varchar,@FechaOutIni,103)<>'01/01/1900' Or               

 --FechaOut <= convert(smalldatetime,convert(varchar,GETDATE(),103)))                     

--HLF-20140428-Agregando 1 era parte del Union y Where cc.EstadoFacturac='A'--Ajuste de Precio            

--HLF-20140430-@SoloAjustePrecio          

--HLF-20140609-Agregando and FlPorAjustePrecio=1 a subquery Total        

--HLF-20141030-WHERE X.Total>0      

--HLF-20141127-X.Total<>0      

--HLF-20141205-  isnull((Select Max(Dia) From COTIDET cd1 Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo   

--  And u1.IDPais='000323'  

--  Left Join MAPROVEEDORES p1 On cd1.IDProveedor=p1.IDProveedor  

--  Where IDCAB=cc.IDCAB And SD1.idTipoOC<>'005'),cc.FechaOut) as FechaOutPeru,  

--HLF-20141218-  (convert(varchar,@FechaOutIni,103)='01/01/1900' Or   

-- FechaOutPeru <= convert(smalldatetime,convert(varchar,GETDATE(),103)))  

-- And ((convert(varchar,@FechaOutIni,103)='01/01/1900' And convert(varchar,@FechaOutFin,103)='01/01/1900') Or  

-- FechaOutPeru Between @FechaOutIni And @FechaOutFin ) ) or EstadoFacturac='A')  

--HLF-20141219-and FlFacturado=0),0) as Total,  

--HLF-20141219- (convert(varchar,@FechaOutIni,103)='01/01/1900' And   

 --FechaOutPeru <= convert(smalldatetime,convert(varchar,GETDATE(),103)))  

 --Or ((convert(varchar,@FechaOutIni,103)<>'01/01/1900' And convert(varchar,@FechaOutFin,103)<>'01/01/1900') And  

 --FechaOutPeru Between @FechaOutIni And (dateadd(s,-1,dateadd(d,1,convert(varchar,@FechaOutFin,103)))) )   

 --)) or EstadoFacturac='A'  

--HLF-20141226-cc.FechaOut como 2da columna, ya no 1era, ordenar por X.FechaOutPeru Desc  

--HLF-20150205- subquerys (Select COUNT(*) From COTIDET cd1 Inner Join MAPROVEEDORES p1 On cd1.IDProveedor=p1.IDProveedor And 

	--p1.CoUbigeo_Oficina=@CoUbigeo_Oficina ....
--HLF-20150302-Declare @CoPaisFecOut char(6)=isnull((Select isnull(...
 --And u1.IDPais=@CoPaisFecOut 
--HLF-20150304-Case When @CoUbigeo_Oficina='000065' then...	
--Else	dbo.FnTotalFilePreLiquidacion(cc.idcab,@CoUbigeo_Oficina) End	as Total,  
CREATE Procedure dbo.COTICAB_SelFacturac                        

 @EstadoFacturac char(1),                        

 @IDFile varchar(8),                        

 @Titulo varchar(100),                        

 @FechaOutIni smalldatetime,                        

 @FechaOutFin smalldatetime,          

 @SoloAjustePrecio bit=0,

 @CoUbigeo_Oficina char(6)=''

As                        

 Set Nocount On                        

 Declare @CoPaisFecOut char(6)=isnull((Select isnull(IDPais,'') From MAUBIGEO Where IDUbigeo=@CoUbigeo_Oficina),'') 


 SELECT * FROM       

    (      

 Select   

 isnull((Select Max(Dia) From COTIDET cd1 Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo   

  --And u1.IDPais='000323'  
  And u1.IDPais=@CoPaisFecOut 

  Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det  

  Where IDCAB=cc.IDCAB And SD1.idTipoOC<>'005'),cc.FechaOut) as FechaOutPeru,      

  cc.FechaOut,   

 cc.IDCAB as IDCab,cc.CoTipoVenta ,                

 Case cc.CoTipoVenta                  

 when 'RC' Then 'RECEPTIVO'                  

 when 'CU' Then 'COUNTER'                  

 when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'                  

 when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'                  

 End as DescTipoVenta,cc.IDFile, c.RazonComercial as DescCliente,                        

 ub.Descripcion as DescPais, cc.Titulo as Referencia,                        

 --(Select SUM(Total) From COTIDET Where IDCAB=cc.idcab) as Total,                        

 --ISNULL((Select SUM(Total) From DEBIT_MEMO Where IDCAB=cc.idcab And IDEstado<>'AN'        

 --and FlPorAjustePrecio=1),0) as Total,                        

 --ISNULL((Select SUM(Total) From DEBIT_MEMO Where IDCAB=cc.idcab And IDEstado<>'AN'	and FlPorAjustePrecio=1 and FlFacturado=0),0) 
Case When @CoUbigeo_Oficina='000065' then
	ISNULL((Select SUM(Total) From DEBIT_MEMO Where IDCAB=cc.idcab And IDEstado<>'AN'        
	and FlPorAjustePrecio=1 and FlFacturado=0),0) 
Else
	dbo.FnTotalFilePreLiquidacion(cc.idcab,@CoUbigeo_Oficina)
End	as Total,  

 cc.EstadoFacturac,                    

 Case cc.EstadoFacturac                    

 When 'P' Then 'PENDIENTE'                     

 When 'F' Then 'FACTURADO'                     

 When 'A' Then 'AJUSTE DE PRECIO'                    

 End AS DescEstadoFacturac 

 From COTICAB cc Left Join MACLIENTES c On cc.IDCliente=c.IDCliente                        

 Left Join MAUBIGEO ub On cc.IDubigeo=ub.IDubigeo          

 Where cc.EstadoFacturac='A'--Ajuste de Precio            

 And ((Select COUNT(*) From COTIDET cd1 Inner Join MAPROVEEDORES p1 On cd1.IDProveedor=p1.IDProveedor And 

	p1.CoUbigeo_Oficina=@CoUbigeo_Oficina

	Where cd1.IDCAB=cc.IDCAB)>0 or LTRIM(rtrim(@CoUbigeo_Oficina))='')



 Union            

                      

 Select   

 isnull((Select Max(Dia) From COTIDET cd1 Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo   

  --And u1.IDPais='000323'  
  And u1.IDPais=@CoPaisFecOut 

  Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det  

  Where IDCAB=cc.IDCAB And SD1.idTipoOC<>'005'),cc.FechaOut) as FechaOutPeru,  

  cc.FechaOut,  

 cc.IDCAB as IDCab,cc.CoTipoVenta ,                

 Case cc.CoTipoVenta                  

 when 'RC' Then 'RECEPTIVO'                  

 when 'CU' Then 'COUNTER'                  

 when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'                  

 when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'                  

 End as DescTipoVenta,cc.IDFile, c.RazonComercial as DescCliente,                        

 ub.Descripcion as DescPais, cc.Titulo as Referencia,                        

 --(Select SUM(Total) From COTIDET Where IDCAB=cc.idcab) as Total,                        

 --ISNULL((Select SUM(Total) From DEBIT_MEMO Where IDCAB=cc.idcab And IDEstado<>'AN'),0) as Total,                        

 Case When @CoUbigeo_Oficina='000065' then
	ISNULL((Select SUM(Total) From DEBIT_MEMO Where IDCAB=cc.idcab And IDEstado<>'AN'        
	),0) 
Else
	dbo.FnTotalFilePreLiquidacion(cc.idcab,@CoUbigeo_Oficina)
End	as Total,  

 cc.EstadoFacturac,                    

 Case cc.EstadoFacturac                    

 When 'P' Then 'PENDIENTE'                     

 When 'F' Then 'FACTURADO'                     

 When 'A' Then 'AJUSTE DE PRECIO'                    

 End AS DescEstadoFacturac

   

 From COTICAB cc Left Join MACLIENTES c On cc.IDCliente=c.IDCliente                        

 Left Join MAUBIGEO ub On cc.IDubigeo=ub.IDubigeo                        

 Where                        

 (cc.EstadoFacturac=@EstadoFacturac Or LTRIM(rtrim(@EstadoFacturac))='') And                        

 (cc.IDFile Like '%'+@IDFile+'%' Or LTRIM(rtrim(@IDFile))='') And                         

 (cc.Titulo Like '%'+@Titulo+'%' Or LTRIM(rtrim(@Titulo))='')   

 --And (convert(varchar,@FechaOutIni,103)='01/01/1900' Or cc.FechaOut Between @FechaOutIni And @FechaOutFin)                        

                      

 --And (convert(varchar,@FechaOutIni,103)<>'01/01/1900' Or               

 --FechaOut <= convert(smalldatetime,convert(varchar,GETDATE(),103)))                        

 And Estado='A'        

 And cc.EstadoFacturac<>'A'            

 And @SoloAjustePrecio=0  

 

  And ((Select COUNT(*) From COTIDET cd1 Inner Join MAPROVEEDORES p1 On cd1.IDProveedor=p1.IDProveedor And 

	p1.CoUbigeo_Oficina=@CoUbigeo_Oficina

	Where cd1.IDCAB=cc.IDCAB)>0 or LTRIM(rtrim(@CoUbigeo_Oficina))='')

        

 ) AS X      

 WHERE --X.Total>0      

 X.Total<>0         

 And (convert(varchar,@FechaOutIni,103)='01/01/1900' Or (  

   

 (convert(varchar,@FechaOutIni,103)='01/01/1900' And   

 FechaOutPeru <= convert(smalldatetime,convert(varchar,GETDATE(),103)))  

 Or ((convert(varchar,@FechaOutIni,103)<>'01/01/1900' And convert(varchar,@FechaOutFin,103)<>'01/01/1900') And  

 FechaOutPeru Between @FechaOutIni And (dateadd(s,-1,dateadd(d,1,convert(varchar,@FechaOutFin,103)))) )   

 )) or EstadoFacturac='A'  

   

    

 Order by X.EstadoFacturac asc,  

 --X.FechaOut Desc                        

 X.FechaOutPeru Desc  

   
