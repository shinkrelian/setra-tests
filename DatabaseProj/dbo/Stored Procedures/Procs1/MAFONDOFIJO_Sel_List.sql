﻿--JHD-20150605 - (rtrim(ltrim(@NuCodigo))='' or MAFONDOFIJO.NuCodigo  like '%'+@NuCodigo+'%') AND
--JHD 20150608 - NuCodigo='FF. '+F.CoPrefijo+' N° ' +isnull(f.NuCodigo,''),
--JHD 20150608 - (rtrim(ltrim(@NuCodigo))='' or /*f.NuCodigo*/ 'FF. '+RTRIM(LTRIM(F.CoPrefijo))+' N° ' +isnull(f.NuCodigo,'')  like '%'+@NuCodigo+'%') AND
--JHD 20150610 - Solo mostrar los que no tienen reembolso y los reembolsados activos
--JHD 20150610 - Mostrar Saldo
--JHD 20150610 - order by NuFondoFijo
--JHD 20150610 - SSMonto=cast(round(f.SSMonto,2) as numeric(12,2)), 
--JHD 20150611 - Nuevo parametro @CoTipoFondoFijo char(2)=''
--JHD 20150611 - and (f.CoTipoFondoFijo = @CoTipoFondoFijo or rtrim(ltrim(@CoTipoFondoFijo))='') 
CREATE PROCEDURE [dbo].[MAFONDOFIJO_Sel_List]
@NuFondoFijo int=0,
	@NuCodigo varchar(14)='',
	@CoCeCos char(6)='',
	@CtaContable varchar(14)='',
	@Descripcion varchar(255)='',
	@CoUserResponsable char(4)='',
	@CoMoneda char(3)='',
	@CoTipoFondoFijo char(2)='',
	@FlActivo bit=0
AS
begin
SELECT      f.NuFondoFijo,
			NuCodigo='FF. '+RTRIM(LTRIM(F.CoPrefijo))+' N° ' +isnull(f.NuCodigo,''),
			isnull(f.CoCeCos,'') as CoCeCos,
			isnull(f.CoCeCos,'')+'-'+isnull(cc.Descripcion,'') AS Centro_Costos,
			isnull(f.CtaContable,'') as CtaContable, 
            isnull(f.CtaContable,'')+' - '+isnull(pc.Descripcion,'') AS Cuenta_Contable,
			f.Descripcion,
			f.CoUserResponsable,
			u.Usuario, 
            u.Nombre,
			f.CoMoneda,
			mo.Descripcion AS Moneda,
			mo.Simbolo,
			SSMonto=cast(round(f.SSMonto,2) as numeric(12,2)), 
            f.FlActivo,
			SsSaldo=isnull(f.SsSaldo,0)
FROM              MAFONDOFIJO f LEFT JOIN
                         MACENTROCOSTOS cc ON f.CoCeCos = cc.CoCeCos LEFT JOIN
                         MAPLANCUENTAS pc ON f.CtaContable = pc.CtaContable LEFT JOIN
                         MAUSUARIOS u ON f.CoUserResponsable = u.IDUsuario LEFT JOIN
                         MAMONEDAS mo ON f.CoMoneda = mo.IDMoneda
WHERE        (f.NuFondoFijo = @NuFondoFijo OR @NuFondoFijo=0) AND
--(f.NuCodigo = @NuCodigo or rtrim(ltrim(@NuCodigo))='') AND
(rtrim(ltrim(@NuCodigo))='' or /*f.NuCodigo*/ ('FF. '+RTRIM(LTRIM(F.CoPrefijo))+' N° ' +isnull(f.NuCodigo,''))  like '%'+@NuCodigo+'%') AND
(f.CoCeCos =  @CoCeCos or rtrim(ltrim(@CoCeCos))='') AND
(f.CtaContable = @CtaContable or rtrim(ltrim(@CtaContable))='') AND 
(rtrim(ltrim(@Descripcion))='' or f.Descripcion  like '%'+@Descripcion+'%') AND
(f.CoUserResponsable = @CoUserResponsable or rtrim(ltrim(@CoUserResponsable))='') AND
(f.CoMoneda = @CoMoneda or rtrim(ltrim(@CoMoneda))='') AND
(f.FlActivo = @FlActivo or @FlActivo=0)
and (select count(NuFondoFijo) from MAFONDOFIJO where NuFondoFijo_Reembolso=f.NuFondoFijo)=0
and (f.CoTipoFondoFijo = @CoTipoFondoFijo or rtrim(ltrim(@CoTipoFondoFijo))='') 
order by NuFondoFijo
end;
