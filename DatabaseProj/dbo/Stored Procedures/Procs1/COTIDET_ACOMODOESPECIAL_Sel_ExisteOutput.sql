﻿
CREATE PROCEDURE [COTIDET_ACOMODOESPECIAL_Sel_ExisteOutput]
	@IDDet INT,
	@CoCapacidad CHAR(2),
	@pExiste BIT OUTPUT
AS
BEGIN
-- =====================================================================
-- Author	   : Dony Tinoco
-- Create date : 04.12.2013
-- Description : SP que verifica la existencia de un acomodo especial
-- =====================================================================
	SET NOCOUNT ON
	
	IF EXISTS(SELECT IDDet FROM COTIDET_ACOMODOESPECIAL WHERE IDDet = @IDDet AND CoCapacidad = @CoCapacidad)
		SET @pExiste=1
	ELSE
		SET @pExiste=0  
END
