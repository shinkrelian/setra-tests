﻿CREATE Procedure [dbo].[MAOPCIONES_Sel_Tvw]	
	@NivelOpc	tinyint	
As
	Set NoCount On
	
	Set @NivelOpc=@NivelOpc+@NivelOpc-1
	
	Select X.* From
	(
	Select Case When @NivelOpc>1 Then SUBSTRING(MO.IDOpc,(@NivelOpc-2),2) Else SUBSTRING(MO.IDOpc,(@NivelOpc),2) End as NivelOpcUnion, 	
	SUBSTRING(MO.IDOpc,1,4) AS NivelOpcUnion2, 	
	SUBSTRING(MO.IDOpc,@NivelOpc,2) as NivelOpc, 	
	MO.IDOpc, MO.Descripcion As NombreOpcion	
	
	From MAOPCIONES MO
	
	) as X 
	Where 
	(@NivelOpc=1 And right(X.IDOpc,4)='0000')	Or
	(NivelOpc<>'00' And @NivelOpc=3 And right(X.IDOpc,2)='00') Or 	
	(@NivelOpc=5 And NivelOpc<>'00')
	
	Order by Cast(IDOpc as int)
