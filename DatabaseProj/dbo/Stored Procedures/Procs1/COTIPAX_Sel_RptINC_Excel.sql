﻿CREATE Procedure dbo.COTIPAX_Sel_RptINC_Excel  
@IDCab int  
As  
Set NoCount On  
Select cast(Orden as varchar(2)) as ITEM_RESERVA,Nombres AS PRIMER_NOMBRE,  
    Apellidos AS PRIMER_APELLIDO,Isnull(ti.CodPeruRail,'') as TIPO_DOC,  
    Isnull(NumIdentidad,'') as NRO_DOC,   
    Case when ub.CodigoPeruRail = 'NAP' Or ub.CodigoPeruRail Is null then '' Else ub.CodigoPeruRail End as NACIONALIDAD,  
    Isnull(CONVERT(varchar(10),FecNacimiento,103),'') as FECHA_NAC  
from COTIPAX cp Left Join MATIPOIDENT ti ON cp.IDIdentidad= ti.IDIdentidad  
Left Join MAUBIGEO ub On cp.IDNacionalidad = ub.IDubigeo  
where IDCab = @IDCab  
Order by Orden  
