﻿--HLF-20140929-Top 1 a      
 --Set @IDCab=(Select Top 1 isnull(dm.IDCab,0) From INGRESO_DEBIT_MEMO idm         
--HLF-20140930-     If Exists(Select nuingreso From INGRESO_DOCUMENTO Where NuIngreso=@NuIngreso And IDTipoDoc=@IDTipoDoc And NuDocum=@NuDocum) Delete...    
--JHD-20141217-Se comentó la condicion de @CoSerie antes del cursor --and LEFT(NuDocum,3)=@CoSerie        
--JHD-20141217- If ABS(@SsMontoMatch)>0        
--HLF-20150106-If @IDTipoDoc='NCR' and @CoSerie='001' ...  
--JHD-20150106- INSERTAR AL FINAL LAS NOTAS DE CREDITO CON AJUSTE DE PRECIO  
--HLF-20150107-ANTICIPOS
--HLF-20150302-Delete From INGRESO_DOCUMENTO Where NuIngreso=@NuIngreso And IDTipoDoc=@IDTipoDoc And NuDocum=@NuDocum    
--Delete From INGRESO_DOCUMENTO Where cast(nuingreso as varchar)+' '+IDTipoDoc+' '+NuDocum In ...
--HLF-20150311-Or(DOCUMENTO.IDTipoDoc IN ('NCA','BOL') and Left(DOCUMENTO.NuDocum,3)='005'),  
--If @IDTipoDoc='BLE' and @CoSerie='006'
--HLF-20150508-Declare curDocs cursor for...
--Case When DOCUMENTO.IDTipoDoc IN('NDB','BOL') Then ...
CREATE Procedure [dbo].[INGRESO_DOCUMENTO_InsMatch]
 @IDCab int,
 @NuIngresoPar int,
 @NuAsignacion int,
 @UserMod char(4)

As     

 Set NoCount On        

 Declare @SsRecibidoIng numeric(10,2), @CoMonedaIng char(3),        
 @SsTotalDocum numeric(9,2), @CoMonedaDoc char(3), @SsMontoMatch numeric(9,2),        
 @SaldoDoc numeric(9,2), @SaldoIng numeric(10,2)=0,        
 @NuDocum char(10),@NuIngreso int, @IDTipoDoc char(3)='BOL', @CoSerie char(3)='001',        
 @SsMontoDocAntic numeric(9,2)=0,
 @SsMontoInsert numeric(9,2),
 @SsRecibidoIngMayor numeric(10,2)=0, @NuIngresoMayor int=0
         

 If @IDCab=0        

  Begin         

  Set @IDCab=(Select Top 1 isnull(dm.IDCab,0) From INGRESO_DEBIT_MEMO idm         
  Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo        
  Where NuIngreso=@NuIngresoPar And NuAsignacion=@NuAsignacion)     

  print 'IDCAB: '+cast(isnull(@idcab,'0') as varchar(10))        
  End                  

 print 'IDCAB: '+cast(isnull(@idcab,'0') as varchar(10))        

         
InicioDoc:                


  Set @SsTotalDocum = 0                 

 --Select @NuDocum=NuDocum, 
 --@SsTotalDocum=SsTotalDocumUSD,
 --@CoMonedaDoc=IDMoneda         
 --From DOCUMENTO Where IDCab=@IDCab         
 --and (IDTipoDoc=@IDTipoDoc Or ltrim(rtrim(@IDTipoDoc))='' )         
 --and (SsTotalDocumUSD=@SsMontoDocAntic Or @SsMontoDocAntic=0)
 --and LEFT(NuDocum,3)=@CoSerie        

	Declare curDocs cursor for
	Select NuDocum,  SsTotalDocumUSD, IDMoneda         
	From DOCUMENTO Where IDCab=@IDCab         
	and (IDTipoDoc=@IDTipoDoc Or ltrim(rtrim(@IDTipoDoc))='' )         
	and (SsTotalDocumUSD=@SsMontoDocAntic Or @SsMontoDocAntic=0)
	and LEFT(NuDocum,3)=@CoSerie        
	Order by SsTotalDocumUSD Desc

	Open curDocs
	Fetch Next From curDocs into @NuDocum,  @SsTotalDocum, @CoMonedaDoc
	While @@FETCH_STATUS=0        
		Begin 
		

		If @SsTotalDocum > 0        
		  Begin  
		  PRINT 'NUMDOC: '+@NuDocum+' TotalDoc: '+ cast(@SsTotalDocum as varchar(15))        
		  Set @SaldoDoc=@SsTotalDocum        
       
		  DECLARE CurIngresos cursor for
		  SELECT * FROM        
		  (        
		  SELECT DISTINCT IFF.NuIngreso,        
			Case When @CoSerie='001' Then IFF.SsSaldoMatchDocumento        
		  Else IFF.SsRecibido        
		  End as SsRecibidoIng,
		  IFF.IDMoneda FROM INGRESO_DEBIT_MEMO IDM INNER JOIN DEBIT_MEMO DM         
		  ON IDM.IDDebitMemo=DM.IDDebitMemo And DM.IDCab=@IDCab         
		  INNER JOIN INGRESO_FINANZAS IFF ON IDM.NuIngreso=IFF.NuIngreso        
		  ) AS X        
		  WHERE (@SsMontoDocAntic=0 OR SsRecibidoIng=@SsMontoDocAntic)                

		  Open CurIngresos        
		  Fetch Next From CurIngresos Into @NuIngreso,@SsRecibidoIng, @CoMonedaIng        
		  While @@FETCH_STATUS=0        
			  Begin        
			  PRINT 'Total Ing: '+cast(@SsRecibidoIng as varchar(15))                

			  Set @SaldoIng=@SsRecibidoIng        
			  InicioIng:        

			  If @SaldoIng<=@SaldoDoc        
			   Begin        
			   Set @SsMontoMatch=@SaldoIng           
			   Set @SaldoDoc=@SaldoDoc-@SaldoIng     
			   Set @SaldoIng=0        
			   print '1'        
			   End        
			  Else       
			   Begin
			   Set @SsMontoMatch=@SaldoDoc           
			   Set @SaldoIng=@SaldoIng-@SaldoDoc   
			   Set @SaldoDoc=0        
			   print '2'        
			   End        

			  print 'SaldoDoc: '+ cast(@SaldoDoc as varchar(15))        
			  print 'SaldoIng: '+ cast(@SaldoIng as varchar(15))        
			  print '@SsMontoMatch: '+ cast(@SsMontoMatch as varchar(15))                  
			  If ABS(@SsMontoMatch)>0        
				Begin	
				--print 'INS: '+cast(@NuIngreso as varchar(10))+' '+@NuDocum+' '+cast(@SsMontoMatch as varchar(15))        
				print 'SaldoDoc Ins: '+ cast(@SaldoDoc as varchar(15))        
				print 'SaldoIng Ins: '+ cast(@SaldoIng as varchar(15))        
				print '@SsMontoMatch Ins: '+ cast(@SsMontoMatch as varchar(15))                  
				Set @SsMontoInsert=@SsMontoMatch		
				If @SsMontoInsert>0 		
					Begin
					If Exists(Select nuingreso From INGRESO_DOCUMENTO Where NuIngreso=@NuIngreso And IDTipoDoc=@IDTipoDoc And NuDocum=@NuDocum)    
					Delete From INGRESO_DOCUMENTO Where NuIngreso=@NuIngreso And IDTipoDoc=@IDTipoDoc And NuDocum=@NuDocum    

					Insert into INGRESO_DOCUMENTO(NuIngreso, IDTipoDoc, NuDocum, SsMonto, UserMod)        
					Values(@NuIngreso, @IDTipoDoc, @NuDocum, @SsMontoInsert--ABS(@SsMontoMatch)
					*Case When @IDTipoDoc='NCA' or @IDTipoDoc='NCR' Then -1 Else 1 End , @UserMod)        

					PRINT 'Restando SsSaldoMatchDocumento ...' 
					--Update INGRESO_FINANZAS Set SsSaldoMatchDocumento=SsSaldoMatchDocumento-ABS(@SsMontoMatch)
					Update INGRESO_FINANZAS Set SsSaldoMatchDocumento=SsSaldoMatchDocumento-@SsMontoInsert
					Where NuIngreso=@NuIngreso        
					End
				End        
	
			  print 'SaldoDoc2: '+ cast(@SaldoDoc as varchar(15))        
			  print 'SaldoIng2: '+ cast(@SaldoIng as varchar(15))        
			  If @SaldoDoc>0 and @SaldoIng>0         
			   Begin        
			   goto InicioIng        
			   print 'goto'        
			   End        

			  If @SsRecibidoIng>@SsRecibidoIngMayor
				Begin
				Set @SsRecibidoIngMayor=@SsRecibidoIng
				Set @NuIngresoMayor=@NuIngreso
				End
			  Fetch Next From CurIngresos Into @NuIngreso,@SsRecibidoIng, @CoMonedaIng        

			  End     
		  Close CurIngresos        
		  Deallocate CurIngresos   
		  End        
		 Else        
		  Close CurIngresos        
          --Deallocate CurIngresos   	

		Fetch Next From curDocs into @NuDocum,  @SsTotalDocum, @CoMonedaDoc
	End
	Close curDocs
	Deallocate curDocs


	If @IDTipoDoc='BOL' and @CoSerie='001'        
		Begin        

		print 'SaldoDoc Final BOL: '+ cast(@SaldoDoc as varchar(15))        
		print 'SaldoIng Final BOL: '+ cast(@SaldoIng as varchar(15))        
	 
		--If @SaldoIng>0         
			--Begin        		
			Set @IDTipoDoc='BLE'  
			Set @CoSerie='006'       
			goto InicioDoc        
			--End
		End         
 
	If @IDTipoDoc='BLE' and @CoSerie='006'
		Begin        

		print 'SaldoDoc Final BOL: '+ cast(@SaldoDoc as varchar(15))        
		print 'SaldoIng Final BOL: '+ cast(@SaldoIng as varchar(15))        
	 
		--If @SaldoIng>0         
			--Begin        		
			Set @IDTipoDoc='FAC'  
			Set @CoSerie='001'
			goto InicioDoc        
			--End
		End                   


	  PRINT '@NuIngresoMayor' + CAST(@NuIngresoMayor AS VARCHAR(10))

	  --JORGE  

	  --INSERTAR AL FINAL LAS NOTAS DE CREDITO CON AJUSTE DE PRECIO  
	 Delete From INGRESO_DOCUMENTO Where cast(nuingreso as varchar)+' '+IDTipoDoc+' '+NuDocum In
	 (SELECT  cast(INGRESO_FINANZAS.NuIngreso as varchar)+' '+ DOCUMENTO.IDTipoDoc+' '+  DOCUMENTO.NuDocum
	 FROM         DEBIT_MEMO INNER JOIN  
						  DOCUMENTO ON DEBIT_MEMO.IDCab = DOCUMENTO.IDCab INNER JOIN  
						  INGRESO_DEBIT_MEMO ON DEBIT_MEMO.IDDebitMemo = INGRESO_DEBIT_MEMO.IDDebitMemo INNER JOIN  
						  INGRESO_FINANZAS ON INGRESO_DEBIT_MEMO.NuIngreso = INGRESO_FINANZAS.NuIngreso 						 
	 WHERE     (DOCUMENTO.IDCab = @IDCab) AND (DOCUMENTO.IDTipoDoc IN ('NCR','NDB')
		Or(DOCUMENTO.IDTipoDoc IN ('NCA','BOL') and Left(DOCUMENTO.NuDocum,3)='005')
		) 
		And (INGRESO_FINANZAS.NuIngreso=@NuIngresoMayor Or @NuIngresoMayor=0)
	 )


	 INSERT INTO INGRESO_DOCUMENTO(NuIngreso,IDTipoDoc,NuDocum,SsMonto,UserMod)  
	 SELECT  DISTINCT    INGRESO_FINANZAS.NuIngreso,  
	   DOCUMENTO.IDTipoDoc,  
	   DOCUMENTO.NuDocum,  
	   --Case When DOCUMENTO.IDTipoDoc='NDB' Then DOCUMENTO.SsTotalDocumUSD Else DOCUMENTO.SsTotalDocumUSD*(-1) End,
	   Case When DOCUMENTO.IDTipoDoc IN('NDB','BOL') Then DOCUMENTO.SsTotalDocumUSD Else DOCUMENTO.SsTotalDocumUSD*(-1) End,
	   @UserMod
	 FROM         DEBIT_MEMO INNER JOIN  
						  DOCUMENTO ON DEBIT_MEMO.IDCab = DOCUMENTO.IDCab INNER JOIN  
						  INGRESO_DEBIT_MEMO ON DEBIT_MEMO.IDDebitMemo = INGRESO_DEBIT_MEMO.IDDebitMemo INNER JOIN  
						  INGRESO_FINANZAS ON INGRESO_DEBIT_MEMO.NuIngreso = INGRESO_FINANZAS.NuIngreso 
						  WHERE     (DOCUMENTO.IDCab = @IDCab) AND (DOCUMENTO.IDTipoDoc IN ('NCR','NDB') 
			Or(DOCUMENTO.IDTipoDoc IN ('NCA','BOL') and Left(DOCUMENTO.NuDocum,3)='005')
		) 	And (INGRESO_FINANZAS.NuIngreso=@NuIngresoMayor Or @NuIngresoMayor=0)
	   

