﻿
Create Procedure dbo.COTICAB_UpdEstadoRegeneracion
@IDCab int
As
	Set NoCount On
	Update COTICAB set Estado = 'A' where IDCAB = @IDCab
