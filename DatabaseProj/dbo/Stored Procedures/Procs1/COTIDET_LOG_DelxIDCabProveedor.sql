﻿Create Procedure [dbo].[COTIDET_LOG_DelxIDCabProveedor]
	@IDCab	int,
	@IDProveedor char(6)
As
	Set Nocount On
	
	DELETE FROM COTIDET_LOG WHERE IDCab = @IDCab And IDProveedor=@IDProveedor
		And Accion<>' '

