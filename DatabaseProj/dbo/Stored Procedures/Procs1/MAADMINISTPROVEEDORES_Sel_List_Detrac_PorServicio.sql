﻿CREATE Procedure MAADMINISTPROVEEDORES_Sel_List_Detrac_PorServicio
@IDServicio char(8)--'LIM00386'
as
BEGIN
declare @IDProveedor char(6)=(select idproveedor from MASERVICIOS where idservicio=@IDServicio)
select * from MAADMINISTPROVEEDORES where IDProveedor=@IDProveedor and TipoCuenta='DE'
END
