﻿CREATE Procedure dbo.COTITRANSP_PAX_Del  
	@IDTransporte int,  
	@IDPaxTransp smallint  
As  
	Set Nocount on  
  
	Delete From COTITRANSP_PAX   
	Where IDTransporte=@IDTransporte And 
	(IDPaxTransp=@IDPaxTransp Or @IDPaxTransp=0)
	
