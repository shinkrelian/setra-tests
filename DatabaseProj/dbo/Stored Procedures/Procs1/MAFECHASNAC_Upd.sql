﻿--PPMG-20151118-@Descripcion varchar(max)
CREATE Procedure [dbo].[MAFECHASNAC_Upd]
	@Fecha smalldatetime,
	@IDUbigeo char(6),
	@Descripcion varchar(max),
	@UserMod char(4)
AS
BEGIN
	Set NoCount On

	Update MAFECHASNAC
           Set Descripcion=@Descripcion,
           UserMod=@UserMod, FecMod=getdate()
    Where Fecha=@Fecha And IDUbigeo=@IDUbigeo
END
