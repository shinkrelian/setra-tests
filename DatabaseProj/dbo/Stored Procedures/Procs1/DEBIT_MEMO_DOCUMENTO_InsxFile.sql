﻿--HLF-20140714-and iff.SSRecibido<0  por and iff.SSRecibido<>0      
--@TotalMenor    
--NCA 005  
--HLF-20140724-BOL 005  
CREATE Procedure dbo.DEBIT_MEMO_DOCUMENTO_InsxFile        
  @IDCab int,           
  @UserMod char(4)          
As          
 ----Set NoCount On          
        
---- Declare @IDDebitMemo char(10), @TotalDM numeric(9,2),           
---- @TotalDoc numeric(9,2)=0, @SaldoDoc numeric(9,2)=0,         
---- @SaldoDM numeric(9,2)=0, @SaldoDocAnt numeric(9,2)=0,@SaldoDM_Ins numeric(9,2),          
---- @IDTipoDoc char(3),        
---- @NuDocum char(10)='', @CoSerie char(3)='',--='001',          
---- @NuDocumUpd char(10)='', @IDTipoDocUpd char(3)='' ,         
---- @tiRowsCursor tinyint=0,@tiContRowsCursor tinyint,        
---- @SumSsSaldoxFacturar numeric(8,2), @TotalMenor numeric(9,2)    
         
        
---- Declare curSeries cursor For          
---- Select sr.IDSerie From COTICAB cc Left Join           
---- MASERIES sr On cc.CoTipoVenta=sr.CoTipoVenta           
---- Where cc.IDCAB=@IDCab          
        
---- Open curSeries        
---- Fetch Next From curSeries Into @CoSerie          
---- While @@FETCH_STATUS=0          
----  Begin          
          
----  Declare curTipoDocums cursor For          
----  SELECT IDTipoDoc          
----  FROM MATIPODOC where NOT NuOrdenCruceDebMemo is null order by NuOrdenCruceDebMemo           
----  Open curTipoDocums           
----  Fetch Next From curTipoDocums Into @IDTipoDoc          
----  While @@FETCH_STATUS=0          
----   Begin          
           
----   If @IDTipoDoc='FAC' Or @IDTipoDoc='BOL'        
----    Begin        
----    Declare curDebitMemos cursor For          
----    Select * From        
----    (         
----    Select distinct dm.IDDebitMemo, --Total           
----    dm.SsSaldoxFacturar          
----    From DEBIT_MEMO dm         
----    Where dm.IDCab=@IDCab AND dm.IDEstado<>'AN'          
----    And dm.SsSaldoxFacturar>0        
        
----    ) as X        
----    Order by cast(X.IDDebitMemo as int)        
            
----    Set @SumSsSaldoxFacturar = 0        
----    Select distinct         
----    @SumSsSaldoxFacturar=sum(dm.SsSaldoxFacturar )        
----    From DEBIT_MEMO dm Inner Join INGRESO_DEBIT_MEMO idm On dm.IDDebitMemo=idm.IDDebitMemo        
----    Inner Join INGRESO_FINANZAS iff On idm.NuIngreso=iff.NuIngreso         
----    Where dm.IDCab=@IDCab AND dm.IDEstado<>'AN'          
            
----    and dm.SsSaldoxFacturar<>0 and iff.SSRecibido<>0            
            
----    print @IDTipoDoc +' - '+ cast(isnull(@SumSsSaldoxFacturar,0) as varchar(10))      
            
----    End        
----   If @IDTipoDoc='NCR'        
----    Begin        
----    Declare curDebitMemos cursor For          
----    Select * From        
----    (         
----    Select distinct dm.IDDebitMemo, --Total           
----    dm.SsSaldoxFacturar          
----    From DEBIT_MEMO dm         
----    Where dm.IDCab=@IDCab AND dm.IDEstado<>'AN'              
----    And dm.SsSaldoxFacturar<0            
----    And Not Exists(Select nudocum From documento Where idcab=@idcab         
----     and left(nudocum,3)='005' and (SsTotalDocumUSD*(-1))=dm.SsSaldoxFacturar        
----     and coestado<>'AN')        
        
----    ) as X        
----    Order by cast(X.IDDebitMemo as int)        
            
----    End        
              
----   Open curDebitMemos               
----   Set @tiContRowsCursor=1        
----   Set @SaldoDoc=0        
----   Fetch Next From curDebitMemos Into @IDDebitMemo, @TotalDM          
----   Set @tiRowsCursor=@@CURSOR_ROWS           
----   While @@FETCH_STATUS=0          
----    Begin        
            
----    If @IDTipoDoc='FAC' Or @IDTipoDoc='BOL'         
----  Begin    
----  Declare curDocums Cursor For        
----  --Select @TotalDoc=SsTotalDocumUSD,@NuDocum=NuDocum         
----  Select SsSaldoCruceDMemo,--SsTotalDocumUSD,        
----  NuDocum, 0 as TotalMenor         
----  From DOCUMENTO Where IDCab=@IDCab And           
----  CoEstado<>'AN'           
----  AND IDTipoDoc=@IDTipoDoc And LEFT(NuDocum,3)=@CoSerie                  
----  And SsSaldoCruceDMemo<>0        
----  Order by cast(NuDocum as int)        
----        End    
----    If @IDTipoDoc='NCR'    
----  Begin    
----  Declare curDocums Cursor For        
----  --Select @TotalDoc=SsTotalDocumUSD,@NuDocum=NuDocum     
----  Select * From     
----  (        
----  Select SsSaldoCruceDMemo,--SsTotalDocumUSD,          
----  NuDocum,    
----       SsSaldoCruceDMemo-(@TotalDM*(-1)) as TotalMenor    
----  From DOCUMENTO Where IDCab=@IDCab And           
----  CoEstado<>'AN'           
----  AND IDTipoDoc=@IDTipoDoc And LEFT(NuDocum,3)=@CoSerie                  
----  And SsSaldoCruceDMemo<>0        
----  ) as X    
----  Order by TotalMenor--cast(NuDocum as int)        
----        End    
            
----    Open curDocums        
----    Fetch Next From curDocums Into @TotalDoc, @NuDocum, @TotalMenor          
----    While @@FETCH_STATUS=0          
----     Begin          
----     --print '@SaldoDoc '+cast(@SaldoDoc as varchar(10))        
----     If @TotalDoc<0 and @IDTipoDoc='NCR' Set @TotalDoc=@TotalDoc*(-1)        
----     If @TotalDM<0 and @IDTipoDoc='NCR' Set @TotalDM=@TotalDM*(-1)        
             
            
             
----     If @tiRowsCursor=@tiContRowsCursor And (@IDTipoDoc='FAC' Or @IDTipoDoc='BOL')        
----      Begin        
----      Set @TotalDM+=@SumSsSaldoxFacturar        
              
----      End        
             
----     Set @SaldoDocAnt=@TotalDoc          
----     If @SaldoDoc <> 0        
----      Set @SaldoDocAnt=@SaldoDoc         
        
             
----     Set @SaldoDM_Ins=@TotalDoc        
----     If @SaldoDoc <> 0        
----      Set @SaldoDM_Ins=@SaldoDoc         
             
----     If @SaldoDoc=0        
----      Begin              
----      Set @SaldoDM=@TotalDM-@TotalDoc          
----      Set @SaldoDoc=@TotalDoc-@TotalDM          
----      End        
----     Else        
----      Begin        
----      Set @SaldoDM=@TotalDM-@SaldoDoc          
----      Set @SaldoDoc=@SaldoDoc-@TotalDM          
----      End        
             
----     If @SaldoDoc<0 Set @SaldoDoc=0          
----     If @SaldoDM<0 Set @SaldoDM=0            
----      If @SaldoDM<@SaldoDoc          
----       Begin        
----       Set @SaldoDM_Ins=@TotalDM          
           
----       If @IDTipoDoc='FAC' Or @IDTipoDoc='BOL'         
----        Begin        
----        If @tiRowsCursor=@tiContRowsCursor                
----         Begin           
----         Set @SaldoDM_Ins+=(@SumSsSaldoxFacturar * (-1))                            
----         End        
----        End        
        
----       End        
              
----     Set @NuDocumUpd=''          
----     Set @IDTipoDocUpd=''                   
    
----     If @SaldoDocAnt<>0         
----      Begin            
----      Set @NuDocumUpd=@NuDocum          
----      Set @IDTipoDocUpd=@IDTipoDoc          
----      print 'Upd: '+ @IDTipoDocupd+' '+@NuDocumupd+' '+cast(@SaldoDM_Ins as varchar(10))        
----      End        
             
               
----     --Set @TotalDoc=@SaldoDoc          
----     --print '@TotalDoc='+cast(@TotalDoc as varchar(10))        
----     Set @TotalDM=@SaldoDM          
             
----     --print 'DM Upd: @SaldoDM='+ cast(@SaldoDM as varchar(10))          
----     If @IDTipoDoc='FAC' Or @IDTipoDoc='BOL'         
----  Begin    
----  print @IDTipoDoc+' DM:'+@IDDebitMemo+' '+cast(@SaldoDM as varchar(10))    
----  Update DEBIT_MEMO     
----  --Set SsSaldoxFacturar=@SaldoDM,FecMod=GETDATE(),          
----  Set SsSaldoxFacturar=SsSaldoxFacturar-@SaldoDM_Ins,FecMod=GETDATE(),          
----  UserMod=@UserMod          
----  Where IDDebitMemo=@IDDebitMemo          
----  End    
----     If @IDTipoDoc='NCR'         
----  Begin    
----  print @IDTipoDoc+' DM:'+@IDDebitMemo+' '+cast(@SaldoDM*(-1) as varchar(10))    
----  Update DEBIT_MEMO     
----  --Set SsSaldoxFacturar=@SaldoDM*(-1),FecMod=GETDATE(),          
----  Set SsSaldoxFacturar=SsSaldoxFacturar-(@SaldoDM_Ins*(-1)),FecMod=GETDATE(),          
----  UserMod=@UserMod          
----  Where IDDebitMemo=@IDDebitMemo          
----        End    
               
----     If @NuDocumUpd<>'' And @SaldoDM_Ins<>0          
----      Begin        
----      If @IDTipoDoc='FAC' Or @IDTipoDoc='BOL'         
----       Begin        
----       Insert Into DEBIT_MEMO_DOCUMENTO(IDDebitMemo,IDTipoDoc,NuDocum,SsMonto,UserMod)          
----       Values(@IDDebitMemo,@IDTipoDocUpd,@NuDocumUpd,@SaldoDM_Ins,@UserMod)          
----       Update DOCUMENTO Set SsSaldoCruceDMemo=SsSaldoCruceDMemo-@SaldoDM_Ins,    
----       UserMod=@UserMod, FecMod=GETDATE()        
----       Where NuDocum=@NuDocumUpd And IDTipoDoc=@IDTipoDocUpd        
----       End        
----      If @IDTipoDoc='NCR'         
----       Begin        
----       Insert Into DEBIT_MEMO_DOCUMENTO(IDDebitMemo,IDTipoDoc,NuDocum,SsMonto,UserMod)          
----       Values(@IDDebitMemo,@IDTipoDocUpd,@NuDocumUpd,@SaldoDM_Ins*(-1),@UserMod)          
----       Update DOCUMENTO Set SsSaldoCruceDMemo=SsSaldoCruceDMemo-@SaldoDM_Ins--*(-1)        
----       ,UserMod=@UserMod, FecMod=GETDATE()    
----       Where NuDocum=@NuDocumUpd And IDTipoDoc=@IDTipoDocUpd        
----       End        
----      End        
             
----     Set @tiContRowsCursor+=1        
             
              
             
             
----     Fetch Next From curDocums Into @TotalDoc, @NuDocum, @TotalMenor          
----     End        
            
            
----    Close curDocums          
----    Deallocate curDocums          
                
            
            
----    Fetch Next From curDebitMemos Into @IDDebitMemo, @TotalDM          
----    End        
           
----   Close curDebitMemos          
----   Deallocate curDebitMemos          
        
----   Fetch Next From curTipoDocums Into @IDTipoDoc        
----   End          
          
----  Close curTipoDocums          
----  Deallocate curTipoDocums          
          
          
----  Fetch Next From curSeries Into @CoSerie          
----  End        
        
---- Close curSeries          
---- Deallocate curSeries          
            
------NCA            
            
---- Declare curDebitMemosNCA cursor For          
----    Select * From        
----    (         
----    Select distinct dm.IDDebitMemo, --Total           
----    dm.SsSaldoxFacturarNCA          
----    From DEBIT_MEMO dm         
----    Where dm.IDCab=@IDCab AND dm.IDEstado<>'AN'              
----    And dm.SsSaldoxFacturarNCA<>0            
----    And Exists(Select nudocum From documento Where idcab=@idcab         
----     and left(nudocum,3)='005' and SsTotalDocumUSD=dm.SsSaldoxFacturarNCA        
----     and coestado<>'AN')        
        
----    ) as X        
----    Order by cast(X.IDDebitMemo as int)        
     
---- Open curDebitMemosNCA    
         
----    Fetch Next From curDebitMemosNCA Into @IDDebitMemo, @TotalDM             
----    While @@FETCH_STATUS=0          
----    Begin        
      
----  Declare curDocumsNCA Cursor For        
----  --Select @TotalDoc=SsTotalDocumUSD,@NuDocum=NuDocum         
----  Select SsSaldoCruceDMemo,--SsTotalDocumUSD,        
----  NuDocum    
----  From DOCUMENTO Where IDCab=@IDCab And           
----  CoEstado<>'AN'           
----  AND IDTipoDoc='NCA' And LEFT(NuDocum,3)='005'                  
----  And SsSaldoCruceDMemo<>0        
----  Order by cast(NuDocum as int)        
      
      
----    Open curDocumsNCA    
----    Fetch Next From curDocumsNCA Into @TotalDoc, @NuDocum    
----    While @@FETCH_STATUS=0          
----     Begin          
    
             
----     Set @SaldoDocAnt=@TotalDoc          
----     If @SaldoDoc <> 0        
----      Set @SaldoDocAnt=@SaldoDoc         
        
             
----     Set @SaldoDM_Ins=@TotalDoc        
----     If @SaldoDoc <> 0        
----      Set @SaldoDM_Ins=@SaldoDoc         
             
----     If @SaldoDoc=0        
----      Begin              
----      Set @SaldoDM=@TotalDM-@TotalDoc          
----      Set @SaldoDoc=@TotalDoc-@TotalDM          
----      End        
----     Else        
----      Begin        
----      Set @SaldoDM=@TotalDM-@SaldoDoc          
----      Set @SaldoDoc=@SaldoDoc-@TotalDM          
----      End        
             
----     If @SaldoDoc<0 Set @SaldoDoc=0          
----     If @SaldoDM<0 Set @SaldoDM=0            
----     If @SaldoDM<@SaldoDoc Set @SaldoDM_Ins=@TotalDM                
        
                     
----     Set @NuDocumUpd=''          
----     Set @IDTipoDocUpd=''                   
    
----     If @SaldoDocAnt<>0         
----      Begin            
----      Set @NuDocumUpd=@NuDocum          
----      Set @IDTipoDocUpd='NCA'    
----      --print 'Upd: '+ @IDTipoDocupd+' '+@NuDocumupd+' '+cast(@SaldoDM_Ins as varchar(10))        
----      End        
             
               
----     Set @TotalDM=@SaldoDM          
             
----     --print 'DM Upd: @SaldoDM='+ cast(@SaldoDM as varchar(10))          
----  Update DEBIT_MEMO     
----  --Set SsSaldoxFacturar=@SaldoDM,FecMod=GETDATE(),          
----  Set SsSaldoxFacturarNCA=SsSaldoxFacturarNCA-@SaldoDM_Ins,FecMod=GETDATE(),          
----  UserMod=@UserMod          
----  Where IDDebitMemo=@IDDebitMemo          
      
               
----     If @NuDocumUpd<>'' And @SaldoDM_Ins<>0          
----      Begin        
----       Insert Into DEBIT_MEMO_DOCUMENTO(IDDebitMemo,IDTipoDoc,NuDocum,SsMonto,UserMod)          
----       Values(@IDDebitMemo,@IDTipoDocUpd,@NuDocumUpd,@SaldoDM_Ins,@UserMod)          
----       Update DOCUMENTO Set SsSaldoCruceDMemo=SsSaldoCruceDMemo-@SaldoDM_Ins,    
----       UserMod=@UserMod, FecMod=GETDATE()        
----       Where NuDocum=@NuDocumUpd And IDTipoDoc=@IDTipoDocUpd        
----      End        
             
                 
             
             
----     Fetch Next From curDocumsNCA Into @TotalDoc, @NuDocum    
----     End        
            
            
----    Close curDocumsNCA    
----    Deallocate curDocumsNCA          
                  
      
      
      
      
----  Fetch Next From curDebitMemosNCA Into @IDDebitMemo, @TotalDM             
----    End    
----    Close curDebitMemosNCA    
----    Deallocate curDebitMemosNCA          
            
            
------BOL 005  
  
---- Insert Into DEBIT_MEMO_DOCUMENTO(IDDebitMemo,IDTipoDoc,NuDocum,SsMonto,UserMod)               
---- Select dmd.IDDebitMemo,d.IDTipoDocVinc,d.NuDocumVinc, d.SsTotalDocumUSD, @UserMod  
---- From DEBIT_MEMO_DOCUMENTO dmd Inner Join DEBIT_MEMO dm   
---- On dmd.IDDebitMemo=dm.IDDebitMemo  
---- Inner Join DOCUMENTO d On d.NuDocum=dmd.NuDocum And d.IDTipoDoc=dmd.IDTipoDoc  
---- Where dmd.IDTipoDoc='NCA' And dm.IDCab=@IDCab         