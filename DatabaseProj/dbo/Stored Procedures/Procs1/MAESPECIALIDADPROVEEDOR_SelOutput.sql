﻿
Create Procedure [Dbo].[MAESPECIALIDADPROVEEDOR_SelOutput]
@IDEspecialidad char(2),
@IDProveedor Char(6),
@pblnExiste bit output

As
	Set NoCount On
	if Exists(select IDProveedor from MAESPECIALIDADPROVEEDOR Where IDEspecialidad = @IDEspecialidad And IDProveedor = @IDProveedor)
		set @pblnExiste = 1
	Else
		set @pblnExiste = 0
