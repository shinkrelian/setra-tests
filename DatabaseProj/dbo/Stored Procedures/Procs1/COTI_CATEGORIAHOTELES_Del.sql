﻿CREATE Procedure [dbo].[COTI_CATEGORIAHOTELES_Del]
	@IDCab	int,
	@Categoria varchar(20)
As
	Set NoCount On
	
	Delete From COTI_CATEGORIAHOTELES	
	WHERE IDCab=@IDCab And	
	Categoria=@Categoria
