﻿

--HL-20130107-Agregando Order by    
--HLF-20130318-Cambiando a Order by Orden  
--HLF-20140901-Case When FlTripleNA=1 then 'N/A' Else Cast(STTotal as varchar(10))End as TotTriple,
CREATE Procedure [dbo].[COTIPAX_Sel_MontosLvw]    
 @IDCab int    
As    
    
 Set NoCount On    
     
 Select  Nombre,     
 TotSimple,  TotDoble,  TotTriple    
 From    
 (    
 Select Titulo+' '+Nombres+' '+Apellidos as Nombre,     
 SSTotal as TotSimple,  Total as TotDoble,      
 --STTotal as TotTriple,
 Case When FlTripleNA=1 then 'N/A' Else Cast(STTotal as varchar(10))End as TotTriple,
 Case When ISNUMERIC(SUBSTRING(Apellidos,5,2))=1 Then      
  SUBSTRING(Apellidos,5,2)    
 Else    
  Nombres    
 End as NroNombrePax, Orden  
     
 From COTIPAX     
 Where IDCab=@IDCab    
 ) as X    
 Order by Orden
