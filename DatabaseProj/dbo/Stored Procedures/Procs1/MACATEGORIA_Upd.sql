﻿
CREATE Procedure dbo.MACATEGORIA_Upd
	@IdCat char(3),
	@Descripcion char(10),
	@Comision	numeric(6,2),
    @UserMod char(4)
	
As
	Set NoCount On
	
	Update MACATEGORIA
	Set Descripcion=@Descripcion,
	Comision=@Comision,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where IdCat=@IdCat

