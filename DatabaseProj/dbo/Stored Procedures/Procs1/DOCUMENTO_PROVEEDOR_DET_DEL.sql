﻿
------------------------------------------------------

CREATE PROCEDURE [DOCUMENTO_PROVEEDOR_DET_DEL]
	@NuDocumProv int,
	@NuDocumProvDet int
AS
BEGIN
	Delete [dbo].[DOCUMENTO_PROVEEDOR_DET]
	Where 
		[NuDocumProv] = @NuDocumProv And 
		[NuDocumProvDet] = @NuDocumProvDet
END;
