﻿Create Procedure dbo.DOCUMENTO_DET_InsManual      
 @NuDocum char(10),      
 @IDTipoDoc char(3),     
 @NoServicio varchar(230),  
 @IDMonedaCosto char(3),  
 @SsCompraNeto numeric(9, 2),           
 @SsIGVSFE numeric(5, 2),      
 @SsTotalCosto numeric(9, 2),          
 @SsMargen numeric(6, 2),      
 @SsTotalCostoUSD numeric(9, 2),      
 @SsTotalDocumUSD numeric(9, 2),     
 @CoTipoDet char(2),    
 @UserMod char(4)      
      
As      
 Set NoCount On      
       
 Declare @NuDetalle tinyint=isnull((Select MAX(NuDetalle) From DOCUMENTO_DET       
  Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc),0)+1      
       
 Insert Into DOCUMENTO_DET(      
  NuDocum,      
  IDTipoDoc,       
  NuDetalle,     
  NoServicio,       
  IDMonedaCosto,  
  SsCompraNeto,           
  SsIGVSFE,      
  SsTotalCosto,           
  SsMargen,      
  SsTotalCostoUSD,      
  SsTotalDocumUSD,       
  CoTipoDet, 
  FlDocumentoManual, 
  UserMod)      
 Values(      
  @NuDocum,      
  @IDTipoDoc,       
  @NuDetalle,  
  @NoServicio,         
  @IDMonedaCosto,  
  @SsCompraNeto,      
  @SsIGVSFE,      
  @SsTotalCosto,     
  @SsMargen,      
  @SsTotalCostoUSD,      
  @SsTotalDocumUSD,       
  @CoTipoDet,  
  1,
  @UserMod)
