﻿--JRF-20131211-Debe agregarse la columna de CoOperacionBan y SsRecibido  
CREATE Procedure dbo.INGRESO_DEBIT_MEMO_Sel_ListxIDDebitMemo    
@IDDebitMemo char(10)    
As    
 Set NoCount On    
 select Isnull(Convert(char(10),ifn.FeAcreditada,103),'') as FechaAsignacion,Isnull(f.Descripcion,'---') as FormaPago,    
 Isnull(b.Sigla,'---') as Banco,Isnull(CoOperacionBan,'') as CoOperacionBan,idm.SsPago,idm.SsGastosTransferencia, idm.SsPago+idm.SsGastosTransferencia as ImporteTotal,  
 ifn.SsRecibido    
 from INGRESO_DEBIT_MEMO idm Left Join ASIGNACION a On idm.NuAsignacion = a.NuAsignacion    
 Left Join INGRESO_FINANZAS ifn On idm.NuIngreso = ifn.NuIngreso     
 Left Join MAFORMASPAGO f On ifn.IDFormaPago = f.IdFor    
 Left Join MABANCOS b On ifn.IDBanco = b.IDBanco    
 where IDDebitMemo = @IDDebitMemo    
 Order by ifn.FeAcreditada    
