﻿
--DOCUMENTO_PROVEEDOR_Sel_Cabecera_FormaEgreso '', 13887
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_Cabecera_FormaEgreso]                
@NuVoucher varchar(14),
@IDCab int
AS
BEGIN
	DECLARE @IDProveedor_Zicasso char(6) = '003557',
			@IDServicio_Zicasso char(8)	= 'LAX00002',
			@Servicio_Det_Zicasso CHAR(4) = 'ZIC6'
	DECLARE @Tbl_ServDet Table(IDProveedorX char(6),
							   IDServicioX char(8),
							   IDServicio_DetX int);
	Set NoCount On
	INSERT INTO @Tbl_ServDet(IDProveedorX, IDServicioX, IDServicio_DetX)
	SELECT S.IDProveedor, D.IDServicio, D.IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
	WHERE TipoUsoSistema = @Servicio_Det_Zicasso AND S.IDProveedor = @IDProveedor_Zicasso AND D.IDServicio = @IDServicio_Zicasso
	Set NoCount On

	SELECT Cab.IDFIle,
	CASE WHEN Det.IDDocFormaEgreso IS NULL THEN '' ELSE CONVERT(varchar,ISNULL(Det.IDDocFormaEgreso,'')) END AS IDVoucher,
	D.IDProveedor, NombreCorto AS DescProveedor,
	S.RazonComercial as Cliente,
	Cab.NroPax as Pax, ISNULL(Cab.NroLiberados,0) AS Liberados,
	CASE WHEN Cab.TipoPax = 'EXT' THEN 'EXTRANJERO' WHEN Cab.TipoPax = 'NCR' THEN 'NACIONAL CON RUC' WHEN TipoPax = 'NSR' THEN 'NACIONAL SIN RUC' ELSE '' END AS TipoPax,    
	CASE WHEN Cab.Tipo_Lib = 'S' THEN 'SIMPLE' WHEN Cab.Tipo_Lib = 'D' THEN 'DOBLE' ELSE 'NINGUNO' END AS Tipo_lib,    
	Cab.Titulo, UVenta.Nombre as UsuarioVen, UOpera.Nombre as UsuarioOpe, Cab.ObservacionVentas, Cab.ObservacionGeneral,
	Cab.FecInicio as FechaInicio, Cab.FechaOut as FechaFin, ISNULL(Det.IDDocFormaEgreso,'') AS NuOrdenServicio,
	CASE WHEN Det.IDDocFormaEgreso IS NULL THEN 'PD' ELSE Det.CoEstado END AS CoEstado,
	CASE WHEN Det.IDDocFormaEgreso IS NULL THEN D.TotalDet ELSE Det.SsSaldo END AS SaldoPendiente,
	CASE WHEN Det.IDDocFormaEgreso IS NULL THEN 0.00 ELSE Det.SsDifAceptada END AS SsDifAceptada,
	CASE WHEN Det.IDDocFormaEgreso IS NULL THEN '' ELSE ISNULL(Det.TxObsDocumento,'') END AS TxObsDocumento,
	P.IDTipoProv,
	CASE WHEN Det.IDDocFormaEgreso IS NULL THEN D.TotalDet ELSE Det.SsMontoTotal END AS SsMontoTotal,
	'' AS RutaDocSustento
	FROM COTICAB Cab INNER JOIN (SELECT IDCAB, SUM(ISNULL(Total,0)) AS TotalDet, IDProveedor FROM COTIDET D INNER JOIN @Tbl_ServDet T
								 ON D.IDServicio_Det = T.IDServicio_DetX AND IDProveedor = IDProveedorX AND IDServicio = IDServicioX
							     GROUP BY IDCAB, IDProveedor) D
							   ON Cab.IDCAB = D.IDCAB
	INNER JOIN MAPROVEEDORES P ON D.IDProveedor = P.IDProveedor
	INNER JOIN MACLIENTES AS S ON Cab.IDCliente = S.IDCliente
	LEFT OUTER JOIN MAUSUARIOS UVenta On Cab.IDUsuario = UVenta.IDUsuario  
	LEFT OUTER JOIN MAUSUARIOS UOpera On Cab.IDUsuarioOpe = UOpera.IDUsuario  
	 LEFT OUTER JOIN
	(SELECT DF.IDDocFormaEgreso, DF.TipoFormaEgreso, DF.CoMoneda, DF.CoEstado, DF.SsMontoTotal, DF.SsSaldo, DF.SsDifAceptada, DF.CoUbigeo_Oficina,
	 DP.IDCab, EO.NoEstado, DF.TxObsDocumento
	FROM DOCUMENTO_FORMA_EGRESO AS DF INNER JOIN DOCUMENTO_PROVEEDOR AS DP ON DF.IDDocFormaEgreso = DP.IDDocFormaEgreso
	 AND DF.TipoFormaEgreso = DP.CoObligacionPago AND DF.CoMoneda = DP.CoMoneda_FEgreso
	 INNER JOIN ESTADO_OBLIGACIONESPAGO EO ON DF.CoEstado = EO.CoEstado
	WHERE (DF.TipoFormaEgreso = 'ZIC') AND (LTRIM(RTRIM(@NuVoucher)) = '' or DF.IDDocFormaEgreso = @NuVoucher)
	GROUP BY DF.IDDocFormaEgreso, DF.TipoFormaEgreso, DF.CoMoneda, DF.CoEstado, DF.SsMontoTotal, DF.SsSaldo, DF.SsDifAceptada, DF.CoUbigeo_Oficina,
	 DP.IDCab, EO.NoEstado, DF.TxObsDocumento) Det
	ON Cab.IDCAB = Det.IDCab
	WHERE (Cab.IDFile IS NOT NULL) AND Cab.IDCAB = @IDCab

END
