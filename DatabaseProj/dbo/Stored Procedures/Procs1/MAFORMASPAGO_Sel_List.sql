﻿CREATE Procedure [dbo].[MAFORMASPAGO_Sel_List]    
 @IdFor char(3),    
 @Descripcion varchar(50)    
As    
 Set NoCount On    
     
 Select Descripcion,IDFor    
 From MAFORMASPAGO    
 Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')    
  and (IdFor=@IdFor Or LTRIM(RTRIM(@IdFor))='') --And IdFor <> '003'  
 Order By IdFor asc  
