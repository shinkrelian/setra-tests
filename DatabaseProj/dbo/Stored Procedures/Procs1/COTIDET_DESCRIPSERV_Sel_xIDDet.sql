﻿--HLF-20151014-isnull(c.NoHotel,'') as NoHotel, IsNull(c.TxWebHotel,'') as TxWebHotel
--isnull(s.NoHotel,'') as NoHotel, IsNull(s.TxWebHotel,'') as TxWebHotel
CREATE Procedure [dbo].[COTIDET_DESCRIPSERV_Sel_xIDDet]
	@IDDet	int,
	@IDServicio	char(8),
	@Dia	tinyint
As

	Set NoCount On

	
	Select c.IDIdioma, c.DescCorta, Cast(c.DescLarga as varchar(max)) as DescLarga, 0 as Nuevo
	,isnull(c.NoHotel,'') as NoHotel, IsNull(c.TxWebHotel,'') as TxWebHotel
	From COTIDET_DESCRIPSERV c
	Where c.IDDet=@IDDet
	Union 
	
	--Select s.IDIdioma, s.Titulo as DescCorta, Cast(s.Texto as varchar(max)) as DescLarga, 1 as Nuevo
	--From MATEXTOSERVICIOS s 
	--Where 
	--s.IDIdioma Not In (	Select c.IDIdioma	From COTIDET_DESCRIPSERV c		
	--	Where c.IDDet=@IDDet)
	--And (s.IDServicio In (Select IDServicio From COTIDET Where IDDET=@IDDet) Or 
	--IDServicio=@IDServicio)
	

	Select s.IDIdioma, '' as DescCorta, Cast(s.Descripcion as varchar(max)) as DescLarga, 1 as Nuevo
	,isnull(s.NoHotel,'') as NoHotel, IsNull(s.TxWebHotel,'') as TxWebHotel
	From MASERVICIOS_DIA s 
	Where 
	s.IDIdioma Not In (	Select c.IDIdioma	From COTIDET_DESCRIPSERV c		
		Where c.IDDet=@IDDet)
	And (s.IDServicio In (Select IDServicio From COTIDET Where IDDET=@IDDet) Or 
	IDServicio=@IDServicio) 
	And Dia=@Dia
	
	
	Order by IDIdioma

