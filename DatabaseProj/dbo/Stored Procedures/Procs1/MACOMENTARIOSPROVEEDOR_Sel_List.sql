﻿CREATE Procedure [dbo].[MACOMENTARIOSPROVEEDOR_Sel_List]
	@IDProveedor	char(6),	
	@IDFile	char(8),
	@Comentario	varchar(100),
	@FechaIni	smalldatetime,
	@FechaFin	smalldatetime	
	
As
	Set NoCount On	

	Select   Fecha, Comentario, IDFile, Estado ,IDComentario
	From	MACOMENTARIOSPROVEEDOR 
	Where IDProveedor = @IDProveedor And
	(IDFile = @IDFile Or LTRIM(rtrim(@IDFile))='') And
	(Comentario Like '%'+@Comentario+'%' Or LTRIM(rtrim(@Comentario))='') And 
	(@FechaIni='01/01/1900' Or (Fecha Between @FechaIni And  DATEADD(MINUTE,-1, DATEADD(D,1,@FechaFin)) ))
	Order by Fecha
