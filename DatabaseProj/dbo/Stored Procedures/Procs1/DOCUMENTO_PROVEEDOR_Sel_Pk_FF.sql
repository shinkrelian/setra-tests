﻿--JHD-20150608-Se agrego el parametro @IDCab int=0
--JHD-20150722-Se agregaron campos nuevos: FeVencimiento, CoTipoDocSAP, CoFormaPago, CoTipoDoc_Ref, NuSerie_Ref, NuDocum_Ref, FeEmision_Ref
--JHD-20150723-Se agrego el campo: CoCeCon
--JHD-20150817-Nuevo Campo CoGasto
--JHD-20150826-NuDocum= case when FlCorrelativo=1 then '' else dp.NuDocum end,
--JHD-20150826-NuDocumFormat= case when FlCorrelativo=1 then '' else dbo.FnFormatearDocumProvee(dp.NuDocum) end,
--JHD-20150907-NuDocInterno=ISNULL(dp.CardCodeSAP,''),
--JHD-20151210-NuDocumFormat= case when FlCorrelativo=1 then '' else dp.NuDocum end,
CREATE Procedure [dbo].[DOCUMENTO_PROVEEDOR_Sel_Pk_FF]        
 @NuDocumProv int        
AS        
   
SEt Nocount On      
      
SELECT dp.NuDocumProv,        
    dp.NuVoucher, 
	--dp.NuDocum,         
	 NuDocum= case when FlCorrelativo=1 then '' else dp.NuDocum end,
   --dbo.FnFormatearDocumProvee(dp.NuDocum) as NuDocumFormat,    
    --NuDocumFormat= case when FlCorrelativo=1 then '' else dbo.FnFormatearDocumProvee(dp.NuDocum) end,
	NuDocumFormat= case when FlCorrelativo=1 then '' else dp.NuDocum end,
    --dp.NuDocInterno,        
	NuDocInterno=ISNULL(dp.CardCodeSAP,''),        
    dp.CoTipoDoc,         
          td.Descripcion,        
          dp.FeEmision,        
          dp.FeRecepcion,        
          dp.CoTipoDetraccion,         
          dp.CoMoneda,        
          mo.Descripcion AS Moneda,        
          dp.SsIGV,        
          SsNeto,        
          SsOtrCargos,        
          --oc.SSDetraccion, 
		  SSDetraccion=isnull(dp.SSDetraccion,0),               
          dp.SSTotalOriginal,         
          dp.FlActivo,        
          dp.SSTipoCambio,        
          dp.SSTotal,        
          dp.NuOrden_Servicio,        
          CoTipoOC,        
          CoCtaContab      
          --os.CoOrden_Servicio      
          ,dp.CoMoneda_Pago,  
          dp.CoMoneda_FEgreso,  
          dp.SsTipoCambio_FEgreso,  
          dp.SsTotal_FEgreso,
          isnull(dp.CoProveedor,'') as CoProveedor,
          isnull(poc.NombreCorto,'') as NoProveedor

		  ,NuSerie=isnull(dp.NuSerie,'')
		   ,cocecos=isnull(dp.cocecos,''),

		   NuFondoFijo=isnull(dp.NuFondoFijo,''),
		   CoTipoFondoFijo=isnull(dp.CoTipoFondoFijo,''),
		   SSPercepcion=isnull(dp.SSPercepcion,0),
		   TxConcepto=isnull(dp.TxConcepto,''),
		   IDCab=isnull(dp.IDCab,0),
		   IDFile=isnull((select idfile from coticab where idcab=dp.IDCab),'')
		   --
		   ,FeVencimiento
		   ,CoTipoDocSAP=isnull(CoTipoDocSAP,0)
		   ,CoFormaPago=isnull(CoFormaPago,'')
		   ,CoTipoDoc_Ref=isnull(CoTipoDoc_Ref,'')
		   ,NuSerie_Ref=isnull(NuSerie_Ref,'')
		   ,NuDocum_Ref=isnull(NuDocum_Ref,'')
		   ,FeEmision_Ref
		     ,CoCeCon=isnull(dp.CoCeCon,'')
			 ,CoGasto=isnull(dp.CoGasto,'')
FROM DOCUMENTO_PROVEEDOR dp      
--INNER JOIN  VOUCHER_OPERACIONES ON dp.NuVoucher = VOUCHER_OPERACIONES.IDVoucher       
INNER JOIN        
                      MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc INNER JOIN        
                      MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda        
          --Left Join ORDEN_SERVICIO os On isnull(dp.NuOrden_Servicio,0)=os.NuOrden_Servicio      
        
          Left Join MAPROVEEDORES poc On poc.IDProveedor=dp.CoProveedor
WHERE     (dp.NuDocumProv = @NuDocumProv);

