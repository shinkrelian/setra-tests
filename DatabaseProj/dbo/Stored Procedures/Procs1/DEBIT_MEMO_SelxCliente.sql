﻿--JRF-20141031-Agregar el filtro por Rango de Fechas.
CREATE Procedure dbo.DEBIT_MEMO_SelxCliente
@IDCliente varchar(8),
@FechaInRangoDe smalldatetime = '01/01/1900',
@FechaInRengoHasta smalldatetime = '01/01/1900'
As  
 Set NoCount On  
 Select dm.IDDebitMemo,'REF  : '+dm.Titulo+' DEBIT MEMO : '+SUBSTRING(dm.IDDebitMemo,0,9)+'-'+SUBSTRING(dm.IDDebitMemo,9,9)+CHAR(10)+  
     'FILE : '+c.IDFile +' IN : ' +CONVERT(Char(10),dm.FechaIn,103) + ' OUT : ' +CONVERT(Char(10),dm.FechaOut,103) + ' N°PAX :' + cast(dm.Pax as varchar(8))  
     As Descripcion,  
     (select Isnull(SUM(Total),0) from DEBIT_MEMO_DET dmd where dmd.IDDebitMemo = dm.IDDebitMemo) as Total     
 from DEBIT_MEMO dm Left Join COTICAB c on c.IDCab = dm.IDCab and (dm.IDEstado = 'PD' Or dm.IDEstado ='PP' and dm.IDDebitMemoResumen is null)  
 Left Join MACLIENTES cl on cl.IDCliente = c.IDCliente  
 where c.IDCliente = @IDCliente and dm.IDDebitMemoResumen is Null
 and (CONVERT(char(10),@FechaInRangoDe,103)='01/01/1900'Or (dm.FechaIn between @FechaInRangoDe and @FechaInRengoHasta))
 order by CAST(dm.FechaIn as smalldatetime)  
