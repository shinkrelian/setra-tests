﻿--HLF-20120921-Nuevas columnas TotalSimple, TotalTriple, TotalCostoReal, TotalPorcMargen      
--HLF-20121026-Case When Total=0 ....    
--JRF-20130117-Ordenarlo por Pax
CREATE Procedure [dbo].[COTICAB_QUIEBRES_Sel_xIDCab]      
 @IDCab Int      
As      
 Set NoCount On      
 SELECT IDCAB_Q, Pax, Liberados, Tipo_Lib,       
  isnull(    
  Case When Total=0 Then 0 Else    
  ((Total - TotalImpuesto - TotalCostoReal) / Total) * 100    
  End,    
  0) As TotalPorcMargen,      
  isnull(TotalSimple,0) As TotalSimple,       
   ISNULL(TotalTriple,0) As TotalTriple,    
  ISNULL(Total,0) As Total    
 FROM      
 (      
  SELECT IDCAB_Q, Pax, Liberados, Tipo_Lib,       
  Total+TotalSimple AS TotalSimple,      
  Total+TotalTriple AS TotalTriple,      
  TotalCostoReal,      
  TotalImpuesto,      
  Total      
        
  FROM      
  (      
  Select cq.IDCAB_Q, cq.Pax, cq.Liberados, cq.Tipo_Lib,       
  Sum(dq.Total) as Total,      
  SUM(dq.SSTotal) as TotalSimple,      
  SUM(dq.STTotal) as TotalTriple,      
  Sum(dq.CostoReal) as TotalCostoReal,      
  SUm(dq.TotImpto) as TotalImpuesto      
        
  From COTICAB_QUIEBRES cq Left Join COTIDET_QUIEBRES dq On dq.IDCAB_Q=cq.IDCAB_Q      
  Where cq.IDCAB=@IDCab      
  Group by cq.IDCAB_Q, cq.Pax, cq.Liberados, cq.Tipo_Lib      
  ) AS X      
 ) AS Y      
 Order by Pax Asc  
