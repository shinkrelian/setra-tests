﻿CREATE Procedure [dbo].[MACORREOREVPROVEEDOR_Sel_List]
@IDProveedor char(6),  
@Correo varchar(150),  
@Descripcion varchar(max)  
as  
 Set NoCount On  
 Select Correo,Descripcion from MACORREOREVPROVEEDOR  
 Where (LTRIM(rtrim(@IDProveedor))='' Or IDProveedor = @IDProveedor) And   
    (LTRIM(rtrim(@Correo))='' Or Correo like '%'+ @Correo + '%') And    
    (LTRIM(rtrim(@Descripcion))='' Or Descripcion like '%'+ @Descripcion + '%') And Estado = 'A'  
 Order By 1     