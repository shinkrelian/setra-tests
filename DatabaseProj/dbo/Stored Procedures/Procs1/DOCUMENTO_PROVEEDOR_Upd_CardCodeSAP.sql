﻿--CREATE
CREATE Procedure [dbo].[DOCUMENTO_PROVEEDOR_Upd_CardCodeSAP]
@CardCodeSAP varchar(15),
@NuDocumProv int
As      
 UPDATE  documento_proveedor
    SET CardCodeSAP=@CardCodeSAP
 WHERE NuDocumProv=@NuDocumProv
