﻿CREATE Procedure [dbo].[COTIDET_QUIEBRES_Ins_CopiaIDDet]	
	@IDDET int,
	@IDDETCopia int,
	@UserMod char(4)
As
	Set Nocount On
	
	INSERT INTO COTIDET_QUIEBRES
           (IDCAB_Q
			,IDDET 
           ,CostoReal
           ,CostoLiberado
           ,Margen
           ,MargenAplicado
           ,MargenLiberado
           ,Total
           ,SSCR
           ,SSCL
           ,SSMargen
           ,SSMargenLiberado
           ,SSTotal
           ,STCR
           ,STMargen
           ,STTotal
           ,CostoRealImpto
           ,CostoLiberadoImpto
           ,MargenImpto
           ,MargenLiberadoImpto
           ,SSCRImpto
           ,SSCLImpto
           ,SSMargenImpto
           ,SSMargenLiberadoImpto
           ,STCRImpto
           ,STMargenImpto
           ,TotImpto                      
           ,UserMod)
	Select           
           IDCAB_Q ,
			@IDDET ,           
           CostoReal ,
           CostoLiberado ,
           Margen ,
           MargenAplicado ,
           MargenLiberado ,
           Total ,
           SSCR ,
           SSCL ,
           SSMargen ,
           SSMargenLiberado ,
           SSTotal ,
           STCR ,
           STMargen ,
           STTotal ,
           CostoRealImpto ,
           CostoLiberadoImpto ,
           MargenImpto ,
           MargenLiberadoImpto ,
           SSCRImpto ,
           SSCLImpto ,
           SSMargenImpto ,
           SSMargenLiberadoImpto ,
           STCRImpto ,
           STMargenImpto ,
           TotImpto ,
           @UserMod
	From COTIDET_QUIEBRES Where IDDet=@IDDETCopia
