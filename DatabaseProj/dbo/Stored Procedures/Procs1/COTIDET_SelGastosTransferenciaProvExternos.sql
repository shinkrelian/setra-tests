﻿
Create Procedure dbo.COTIDET_SelGastosTransferenciaProvExternos
	@IDCab int
As
	Set Nocount on

	SELECT CD.IDProveedor, P.NombreCorto as DescProveedor,P.IDFormaPago, FP.Descripcion As DescFormaPago
	,dbo.FnGastosTransferencia(@IDCab,CD.IDProveedor,P.IDFormaPago) as MontoGT
	FROM MAPROVEEDORES P INNER JOIN MAUBIGEO UC ON P.IDCiudad=UC.IDubigeo
	AND UC.IDPais<>'000323' and p.CoUbigeo_Oficina='000065'
	INNER JOIN COTIDET CD ON CD.IDProveedor=P.IDProveedor AND CD.IDCAB=@IDCab And p.IDProveedorInternacional is null
	Inner JOIN MAFORMASPAGO FP ON P.IDFormaPago=FP.IdFor
	GROUP BY CD.IDProveedor, P.NombreCorto,P.IDFormaPago, FP.Descripcion
	Order by P.NombreCorto
	
