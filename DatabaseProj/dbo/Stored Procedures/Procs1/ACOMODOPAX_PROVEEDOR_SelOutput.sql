﻿Create Procedure [Dbo].[ACOMODOPAX_PROVEEDOR_SelOutput]
@IDReserva int,
@IDPax int,
@pblnExiste bit Output
As
	Set NoCount On
	if exists(select idreserva from ACOMODOPAX_PROVEEDOR Where IDReserva=@IDReserva And IDPax =@IDPax)
		Set @pblnExiste = 1
	Else
		Set @pblnExiste = 0
