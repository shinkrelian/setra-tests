﻿CREATE Procedure [dbo].[MADESAYUNOS_Upd]  
 @IDDesayuno char(2),  
 @Descripcion char(50),  
 @UserMod char(4)  
   
As  
 Set NoCount On  
   
 Update MADESAYUNOS  
 Set Descripcion=@Descripcion,  
 UserMod=@UserMod,  
 FecMod=GETDATE()  
 Where IDDesayuno=@IDDesayuno
