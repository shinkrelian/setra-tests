﻿Create Procedure dbo.COTIVUELOS_SelxNroVueloIDOutput
@IDCab int,
@CoLineaAerea varchar(5),
@NumVuelo varchar(5),
@pIDTransp int output
As
	Set NoCount On
	Set @pIDTransp=0
	Select @pIDTransp = ID from COTIVUELOS
	where IDCAB=@IDCab and (CHARINDEX(@CoLineaAerea,Isnull(Vuelo,''))>0 
		and CHARINDEX(@NumVuelo,Isnull(Vuelo,''))>0) and TipoTransporte='V'
