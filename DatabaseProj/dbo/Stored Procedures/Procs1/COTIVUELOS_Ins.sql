﻿--20120824-Nuevo campo EmitidoSetours.        
--HLF-20120920-Case when para @FecSalida,@FecRetorno,@HrRecojo        
--HLF-20120921-Nuevo campo Servicio        
--HLF-20130124-Nuevos Campos.      
--HLF-20130221-Agregando campo IdaVuelta    
--HLF-20130227-Agregando parametro Output  
--JRF-20141020-Case When ltrim(rtrim([@RutaO/@RutaD]))='' Then Null Else [@RutaO/@RutaD] End
--JRF-20141021-Nuevos campos para la relacion con TICKET_AMADEUS_ITINERARIO
CREATE Procedure [dbo].[COTIVUELOS_Ins]        
 @Iddet int,          
 @IDCAB int,                
 @Cotizacion Char(8),              
 @Servicio varchar(250),        
 @NombrePax varchar(100),              
 @Ruta Varchar(100),                
 @NumBoleto char(12),                
 @RutaD Char(6),                
 @RutaO Char(6),                
 @Vuelo varchar(20),                
 @FecEmision smalldatetime,                
 @FecDocumento smalldatetime,                
 @FecSalida smalldatetime,                
 @FecRetorno smalldatetime,                
 @Salida varchar(20),               
 @HrRecojo varchar(20),             
 @PaxC numeric(5),                
 @PCotizado numeric(10, 2),                
 @TCotizado numeric (10, 2),                
 @PaxV numeric (5, 0),                
 @PVolado numeric(10, 2),                
 @TVolado numeric(10, 2) ,                
 @Status char(2),                
 @Comentario text,                
 @Llegada varchar(20) ,                
 @Emitido char(1) ,                
 @Identificador char(1),            
 @CodReserva varchar(20),                
 @Tarifa numeric(10, 2),                
 @PTA numeric(10, 2) ,                
 @ImpuestosExt numeric(10, 2),                
 @Penalidad numeric(10, 2),                
 @IGV numeric(10, 2) ,                
 @Otros numeric(10, 2),                
 @PorcComm numeric(8, 2) ,                
 @MontoContado numeric(10, 2) ,                
 @MontoTarjeta numeric(10, 2),                
 @IdTarjCred char(3) ,                
 @NumeroTarjeta varchar(20),                
 @Aprobacion varchar(20) ,                
 @PorcOver numeric(8, 2) ,                
 @PorcOverInCom numeric(8, 2) ,                
 @MontComm numeric(10, 2)  ,                
 @MontCommOver numeric(10, 2) ,                
 @IGVComm numeric(10, 2),                
 @IGVCommOver numeric(10, 2),                
 @PorcTarifa numeric(10,2) ,              
 @NetoPagar numeric(10, 2),                
 @Descuento numeric(10, 2),                
 @IdLineaA char(6),                
 @MontoFEE numeric(10, 2),                
 @IGVFEE numeric(10, 2),                
 @TotalFEE numeric(10, 2),                
 @EmitidoSetours bit,        
 @EmitidoOperador bit,      
 @IDProveedorOperador char(6),      
 @CostoOperador numeric(8,2),      
 @IdaVuelta char(1),    
 @IDServicio_DetPeruRail int, 
 @CoTicket char(13),
 @NuSegmento tinyint, 
 @UserMod char(4),  
 @pIDTransporte int Output  
As                
                
 Set NoCount On                
      
 Insert into COTIVUELOS                
 ([IdDet]          
 ,[IDCAB]                
 ,[Cotizacion]                
 ,Servicio        
 ,[NombrePax]              
 ,[Ruta]                
 ,[Num_Boleto]                
 ,[RutaD]                
 ,[RutaO]                
 ,[Vuelo]                
 ,[Fec_Emision]                
 ,[Fec_Documento]                
 ,[Fec_Salida]                
 ,[Fec_Retorno]                
 ,[Salida]            
 ,[HrRecojo]                
 ,[PaxC]                
 ,[PCotizado]                
 ,[TCotizado]                
 ,[PaxV]                
 ,[PVolado]                
 ,[TVolado]                
 ,[Status]                
 ,[Comentario]                
 ,[Llegada]                
 ,[Emitido]             
 ,[TipoTransporte]               
 ,[CodReserva]                
 ,[Tarifa]                
 ,[PTA]                
 ,[ImpuestosExt]                
 ,[Penalidad]                
 ,[IGV]                
 ,[Otros]                
 ,[PorcComm]                
 ,[MontoContado]                
 ,[MontoTarjeta]                
 ,[IdTarjCred]                
 ,[NumeroTarjeta]                
 ,[Aprobacion]                
 ,[PorcOver]                
 ,[PorcOverInCom]                
 ,[MontComm]                
 ,[MontCommOver]                
 ,[IGVComm]      
 ,[IGVCommOver]   
 ,[PorcTarifa]              
 ,[NetoPagar]                
 ,[Descuento]                
 ,[IdLineaA]                
 ,[MontoFEE]                
 ,[IGVFEE]                
 ,[TotalFEE]         
 ,[EmitidoSetours]        
 ,[EmitidoOperador]      
 ,[IDProveedorOperador]      
 ,[CostoOperador]    
 ,IdaVuelta    
 ,IDServicio_DetPeruRail  
 ,CoTicket
 ,NuSegmento
 ,[UserMod]        
 ,FecMod)                
 values           
 (Case When ltrim(rtrim(@Iddet))='' then Null else @Iddet End               
 ,Case when ltrim(rtrim(@IDCAB))='' then Null else @IDCAB End                
 ,Case when ltrim(rtrim(@Cotizacion))='' Then Null else @Cotizacion End                
 ,Case when ltrim(rtrim(@Servicio))='' Then Null else @Servicio End                
 ,@NombrePax              
 ,@Ruta                
 ,case when ltrim(rtrim(@NumBoleto))='' Then Null else @NumBoleto End                
 ,Case When ltrim(rtrim(@RutaD))='' Then Null Else @RutaD End
 ,Case When ltrim(rtrim(@RutaO))='' Then Null Else @RutaO End
 ,@Vuelo                
 ,case when @FecEmision='01/01/1900' then null else @FecEmision end               
 ,case when @FecDocumento='01/01/1900' then null else @FecDocumento end               
 ,case when @FecSalida='01/01/1900' then null else @FecSalida end   ,case when @FecRetorno='01/01/1900' then null else @FecSalida end        
 ,@Salida              
 ,case when ltrim(rtrim(@HrRecojo))='' then null else @HrRecojo end        
 ,@PaxC                
 ,@PCotizado                
 ,@TCotizado                
 ,@PaxV                
 ,@PVolado                
 ,@TVolado                
 ,@Status                
 --,Case When ltrim(rtrim(@Comentario))='' Then Null else @Comentario End                
 ,@Comentario                
 ,@Llegada                
 ,@Emitido              
 ,@Identificador              
 ,Case When ltrim(rtrim(@CodReserva))='' Then Null else @CodReserva End                
 ,@Tarifa                
 ,@PTA                
 ,@ImpuestosExt                
 ,@Penalidad                
 ,@IGV                
 ,@Otros                
 ,@PorcComm                
 ,@MontoContado                
 ,@MontoTarjeta                
 ,@IdTarjCred                
 ,Case When ltrim(rtrim(@NumeroTarjeta))='' then Null Else @NumeroTarjeta End                
 ,@Aprobacion                
 ,@PorcOver                
 ,@PorcOverInCom                
 ,@MontComm                
 ,@MontCommOver                
 ,@IGVComm                
 ,@IGVCommOver               
 ,@PorcTarifa               
 ,@NetoPagar                
 ,@Descuento                
 ,@IdLineaA                
 ,@MontoFEE                
 ,@IGVFEE                
 ,@TotalFEE        
 ,@EmitidoSetours       
 ,@EmitidoOperador      
 ,Case when ltrim(rtrim(@IDProveedorOperador))='' then Null else @IDProveedorOperador End      
 ,Case When @CostoOperador = 0 then Null Else @CostoOperador End      
 ,Case when ltrim(rtrim(@IdaVuelta))='' then Null else @IdaVuelta End      
 ,Case when @IDServicio_DetPeruRail=0 then Null else @IDServicio_DetPeruRail End      
 ,Case When Ltrim(rtrim(@CoTicket))='' then Null Else @CoTicket End
 ,Case When @NuSegmento=0 then Null Else @NuSegmento End
 ,Case When ltrim(rtrim(@UserMod))='' then Null else @UserMod End        
 ,GETDATE())   
   
 Set @pIDTransporte=(Select Max(ID) From COTIVUELOS)
