﻿
--JRF-20120829-Nuevo campo MargenCero.              
--JRF-20130227-Nuevo campo Acomodos x Residente.            
--JRF-20130409-Nuevo campo DiasReconfirmacion.          
--JRF-20140127-Nuevo campo ObservacionGeneral        
--JRF-20140225-Nuevo campo ObservacionVentas      
--JRF-20140310-Nuevo campo CoTipoVenta    
--JRF-20140313-Nuevo campo DocWordVersion, DocWordFecha  
--JRF-20140505-Nuevo campo NuSerie
--JRF-20150512-Nuevo campo CoSerieWeb
--HLF-20160205-@NuPedInt
--HLF-20160208-If @NuPedInt<>0	Exec PEDIDO_SelVersionCotizacion @NuPedInt...
--PPMG-20160321-@DatoSeg varchar(max)
--PPMG-20160420-Se agrego un nuevo campo @IDContactoCliente char(3)
CREATE PROCEDURE [dbo].[COTICAB_Ins]     
 @IDCliente char(6),                
 @Titulo varchar(100),                
 @TipoPax char(3),                
 @CotiRelacion char(8),                
 @IDUsuario char(4),                
 @NroPax smallint,                
 @NroLiberados smallint,                
 @Tipo_Lib char(1),                
 @Margen numeric(8,2),                
 @IDBanco char(3),                      
 @IDTipoDoc char(3),                
 @Observaciones text,                
 @FecInicio smalldatetime,                
 @FechaOut smalldatetime,                
 @Cantidad_dias tinyint,                
 @FechaSeg smalldatetime,                
 @DatoSeg varchar(max),                
 @TipoCambio numeric(8,2),                
 @DocVta int,                
 @TipoServicio char(2),                
 @IDIdioma varchar(12),                
 @Simple tinyint,                
 @Twin tinyint,                
 @Matrimonial tinyint,                
 @Triple tinyint,               
 @SimpleResidente tinyint,                
 @TwinResidente tinyint,                
 @MatrimonialResidente tinyint,                
 @TripleResidente tinyint,               
 @IDubigeo char(6),                
 @Notas text,                
 @Programa_Desc varchar(100),                
 @PaxAdulE tinyint,                
 @PaxAdulN tinyint,                
 @PaxNinoE tinyint,                
 @PaxNinoN tinyint,                
 @Aporte bit,                
 @AporteMonto numeric(8,2),                
 @AporteTotal numeric(8,2),                
 @Redondeo numeric(3,1)=1.0,                
 @IDCabCopia int,                
 @Categoria varchar(20),                
 @MargenCero bit ,          
 @DiasReconfirmacion tinyint ,              
 @UserMod char(4),        
 @ObservacionGeneral varchar(Max),        
 @ObservacionVentas varchar(Max),     
 @CoTipoVenta char(2),    
 @DocWordVersion tinyint,  
 @DocWordFecha smalldatetime,  
 @NuSerieCliente int,
 @CoSerieWeb varchar(6),
 @NuPedInt int=0,
 @IDContactoCliente char(3) = NULL,
 @pIDCab int Output                
As
BEGIN              
 Set NoCount On                
 Declare @FechaMesActual varchar(4)=SubString((convert(varchar,getdate(),112)),3,4)                
 Declare @siCotizacion int=IsNull((Select MAX(Cotizacion) From COTICAB                 
  Where Left(Cotizacion,4)=@FechaMesActual), CAST(@FechaMesActual+'0000' AS INT)  )+1                
 Declare @Cotizacion varchar(8)=Cast(@siCotizacion as varchar(8))                 
 --Set @Cotizacion=@FechaMesActual + REPLICATE('0',(4-LEN(@Cotizacion))) + @Cotizacion      
 If @NuPedInt<>0
	Exec PEDIDO_SelVersionCotizacion @NuPedInt,@DocWordVersion output
       
 INSERT INTO COTICAB                
           (Cotizacion                
           --,Fecha                
           --,Estado                
           ,IDCliente
		   ,IDContactoCliente
           ,Titulo                
           ,TipoPax                
           ,CotiRelacion                
           ,IDUsuario                
           ,NroPax                
           ,NroLiberados                
           ,Tipo_Lib                
           ,Margen                
           ,IDBanco                
           --,IDFile                
           ,IDTipoDoc                
           ,Observaciones                
           ,FecInicio                
           ,FechaOut                
           ,Cantidad_dias                
           ,FechaSeg                
           ,DatoSeg                
           ,TipoCambio                
           ,DocVta        
           ,TipoServicio                
           ,IDIdioma                
           ,Simple                
           ,Twin                
           ,Matrimonial                
           ,Triple                
           ,SimpleResidente               
           ,TwinResidente     
           ,MatrimonialResidente               
           ,TripleResidente             
                       
           ,IDubigeo                
           ,Notas                
           ,Programa_Desc                
           ,PaxAdulE                
           ,PaxAdulN                
           ,PaxNinoE         
           ,PaxNinoN                
           --,Aporte                
           ,AporteMonto                
           ,AporteTotal                
           ,Redondeo                
           ,IDCabCopia                 
           ,Categoria                
           ,MargenCero                     
           ,DiasReconfirmacion          
           ,ObservacionGeneral       
           ,ObservacionVentas    
           ,CoTipoVenta  
           ,DocWordVersion  
           ,DocWordFecha 
           ,NuSerieCliente     
		   ,CoSerieWeb 
		   ,NuPedInt
           ,UserMod)                
     VALUES                
           (@Cotizacion,                
            --@Fecha,                
            --@Estado,                
            @IDCliente,
			CASE WHEN ISNULL(@IDContactoCliente,'')='' THEN NULL ELSE @IDContactoCliente END,
            @Titulo ,                
     @TipoPax ,                
            Case When ltrim(rtrim(@CotiRelacion))='' Then Null Else @CotiRelacion End,                
   @IDUsuario ,                
   @NroPax ,                
   Case When @NroLiberados=0 Then Null Else @NroLiberados End,                
   Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End,                
   Case When @Margen=0 Then Null Else @Margen End,                
   @IDBanco,                
   --Case When ltrim(rtrim(@IDFile))='' Then Null Else @IDFile End,                
   @IDTipoDoc,                
   Case When ltrim(rtrim(Cast (@Observaciones as varchar(max))))='' Then Null Else @Observaciones End,                
   @FecInicio,                
   @FechaOut,                
   @Cantidad_dias,                
   Case When @FechaSeg='01/01/1900' Then Null Else @FechaSeg End,                
   Case When ltrim(rtrim(@DatoSeg))='' Then Null Else @DatoSeg End,                
   Case When @TipoCambio=0 Then Null Else @TipoCambio End,                
   Case When @DocVta=0 Then Null Else @DocVta End,                
   @TipoServicio,                
   @IDIdioma,                
   Case When @Simple=0 Then Null Else @Simple End,                
   Case When @Twin=0 Then Null Else @Twin End,                
   Case When @Matrimonial=0 Then Null Else @Matrimonial End,                
   Case When @Triple=0 Then Null Else @Triple End,                
               
   Case When @SimpleResidente=0 Then Null Else @SimpleResidente End,                
   Case When @TwinResidente=0 Then Null Else @TwinResidente End,                
   Case When @MatrimonialResidente=0 Then Null Else @MatrimonialResidente End,                
   Case When @TripleResidente=0 Then Null Else @TripleResidente End,                
               
   @IDubigeo,                
   Case When ltrim(rtrim(Cast(@Notas as varchar(max))))='' Then Null Else @Notas End,                
   Case When ltrim(rtrim(@Programa_Desc))='' Then Null Else @Programa_Desc End,                
   Case When @PaxAdulE=0 Then Null Else @PaxAdulE End,                
   Case When @PaxAdulN=0 Then Null Else @PaxAdulN End,                
   Case When @PaxNinoE=0 Then Null Else @PaxNinoE End,                
   Case When @PaxNinoN=0 Then Null Else @PaxNinoN End,                
   --@Aporte,                
   @AporteMonto,                
   @AporteTotal,                
   @Redondeo,                
   Case When @IDCabCopia=0 Then Null Else @IDCabCopia End,                
   Case When ltrim(rtrim(@Categoria))='' Then Null Else @Categoria End,                
   @MargenCero,          
   @DiasReconfirmacion,          
   Case When LTRIM(rtrim(@ObservacionGeneral))='' Then Null Else @ObservacionGeneral End,            
   Case When Ltrim(rtrim(@ObservacionVentas))='' Then Null Else @ObservacionVentas End,      
   @CoTipoVenta,    
   @DocWordVersion,     
   @DocWordFecha,  
   Case When @NuSerieCliente = 0 Then Null Else @NuSerieCliente End,
   Case When Ltrim(Rtrim(@CoSerieWeb)) = '' Then Null Else @CoSerieWeb End,
   Case When @NuPedInt = 0 Then Null Else @NuPedInt End,
   @UserMod)                
      
 Set @pIDCab =(Select MAX(IDCAB) From COTICAB)  
END

