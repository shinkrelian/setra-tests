﻿Create Procedure [dbo].[MACOMENTARIOSPROVEEDOR_Del]
	@IDComentario char(3),
	@IDProveedor	char(6)
As
	Set NoCount On	

	Delete From MACOMENTARIOSPROVEEDOR
	Where IDProveedor = @IDProveedor And IDComentario = @IDComentario
