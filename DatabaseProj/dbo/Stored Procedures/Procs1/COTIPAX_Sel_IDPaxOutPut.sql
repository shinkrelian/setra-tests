﻿Create Procedure dbo.COTIPAX_Sel_IDPaxOutPut
@IDCab int,
@NuDocIdentidad varchar(15),
@pIDPax int output
As
	Set NoCount On
	Set @pIDPax =0
	Select @pIDPax=IDPax from COTIPAX
	Where IDCab=@IDCab and LTRIM(RTRIM(NumIdentidad))=LTRIM(RTRIM(@NuDocIdentidad))
