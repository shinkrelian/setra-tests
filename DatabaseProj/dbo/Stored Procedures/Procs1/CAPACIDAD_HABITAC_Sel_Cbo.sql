﻿--JRF-20140724-Ordernar por Capacidad y por Matrimonial
CREATE PROCEDURE [CAPACIDAD_HABITAC_Sel_Cbo]  
AS  
BEGIN  
-- =====================================================================  
-- Author    : Dony Tinoco  
-- Create date : 28.11.2013  
-- Description : SP que muestra listado de capacidad de habitaciones  
-- =====================================================================  
  
 SET NOCOUNT ON  
   
 SELECT CoCapacidad,   
   NoCapacidad,  
   QtCapacidad  
 FROM CAPACIDAD_HABITAC  
 ORDER BY QtCapacidad,FlMatrimonial asc 
END  
