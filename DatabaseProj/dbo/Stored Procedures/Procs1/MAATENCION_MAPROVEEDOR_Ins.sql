﻿Create Procedure dbo.MAATENCION_MAPROVEEDOR_Ins
@IDAtencion tinyint,
@IDProveedor char(6),
@Dia1 char(1),
@Dia2 char(1),
@Desde1 varchar(22),
@Hasta1 varchar(22),
@Desde2 varchar(22),
@Hasta2 varchar(22),
@UserMod char(4)
As
Begin
	Set NoCount On
	Insert Into MAATENCION_MAPROVEEDOR
	(IDAtencion,IDProveedor,Dia1,Dia2,Desde1,Hasta1,Desde2,Hasta2,UserMod,FecMod)
	values
	(@IDAtencion,@IDProveedor,@Dia1,@Dia2,@Desde1,@Hasta1,@Desde2,@Hasta2,@UserMod,GetDate())
End
