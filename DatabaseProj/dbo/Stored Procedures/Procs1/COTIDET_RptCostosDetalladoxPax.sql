﻿Create Procedure dbo.COTIDET_RptCostosDetalladoxPax
@IDCab int
as
Set NoCount On
Declare @IDDet varchar(30),@Dia varchar(25),@Item numeric(6, 3),@Cotizacion varchar(12),@Cliente varchar(60),
		@Titulo varchar(max),@Responsable varchar(60),@MargenGeneral varchar(60),@TipoCambio varchar(30),
		@Ubigeo varchar(60),@Proveedor varchar(100),@Servicio varchar(max),@Tipo char(5),@Anio char(4),
		@Var char(8),@Idioma varchar(12),@PlanAlimenticio varchar(1),--@IDTipoProv varchar(3),@ConAlojamiento varchar(1),
		@TipoPasajero varchar(20),@FechaInicio varchar(10),@FechaFin varchar(10),@ContieneTPL char(1),@Margen varchar(20),
		@NombrePax varchar(100),@Orden tinyint,
		@CostoPaxN1 varchar(20),@SSCostoPaxN1 varchar(20),@STCostoPaxN1 varchar(20),
		@VentaPaxN1 varchar(20),@SSVentaPaxN1 varchar(20),@STVentaPaxN1 varchar(20),
		--@CostoPax2 varchar(20),@VentaPax2 varchar(20),@CostoPax3 varchar(20),
		--@VentaPax3 varchar(20),@CostoPax4 varchar(20),@VentaPax4 varchar(20),@CostoPax5 varchar(20),@VentaPax5 varchar(20),
		@FilTitulo char(1),@Notas varchar(max),@DocWordVersion tinyint
		
--Declare @IDCab int =(select IDCAB from COTICAB where IDFile = '14100217')
--Declare @IDDetMaxLetras varchar(Max)=''

Delete from RptCostoDetalladoPax

Declare curReporteDetallePorPax cursor for
select  cd.IDDET,Upper(substring(datename(dw,cd.Dia),1,3))+' '+ Convert(varchar(20),cd.Dia,103) as Dia,cd.Item,c.Cotizacion,cl.RazonComercial,c.Titulo,u.Nombre as Responsable,
Isnull(c.Margen,0) as MargenGeneral,Isnull(c.TipoCambio,0) as TipoCambio,ub.Descripcion as Ubigeo,
p.NombreCorto as Proveedor,cd.Servicio + Case When cd.IncGuia = 1 then ' - Guide' else '' End As Servicio,
Case When S.IDTipoServ = 'NAP' Then '---' Else S.IDTipoServ End As Tipo,
sd.Anio, sd.Tipo As [Var],id.Siglas As Idioma,sd.PlanAlimenticio,c.TipoPax,Convert(varchar(10),c.FecInicio,103) as FechaInicio,
Convert(varchar(10),c.FechaOut,103) as FechaOut,
--Case When (sd.IDServicio_DetxTriple is null And sd.Monto_tri is null And sd.Monto_tris is null And sd.IdHabitTriple = '000') Then 0 Else 1 End 
dbo.FnTodosContieneTPLxIDCab(@IDCab) As ContieneTPL ,cd.MargenAplicado as Margen,
cp.Nombres + ' '+cp.Apellidos as NombrePax,cp.Orden,
--Row_number()Over(Partition by cd.IDDet Order by cd.Item,cp.Orden) as Orden,
isnull(cd.CostoReal + cd.CostoLiberado + cd.TotImpto,0)As CostoPax,Isnull(cd.SSCR + cd.SSCL + cd.SSCRImpto,0) as SSCostoPax,cd.STCR,
isnull(cd.Total,0) VentaPax,cd.SSTotal,cd.STTotal,
cast(c.Notas as Varchar) as Notas  ,c.DocWordVersion
from (select top(5)* from COTIPAX cp1 where IDCab = @IDCab Order by cp1.Orden asc) cp Left Join COTIDET_PAX cdp On cp.IDPax = cdp.IDPax
Left Join COTIDET cd On cdp.IDDet = cd.IDDET
Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
Left Join COTICAB c On cp.IDCab = c.IDCAB
Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario
Left Join MAUBIGEO ub On cd.IDUbigeo = ub.IDUbigeo
Left Join MAPROVEEDORES p ON cd.IDProveedor = p.IDProveedor
Left Join MASERVICIOS s On cd.IDServicio = s.IDServicio
Left Join MAIDIOMAS id On c.IDIdioma = id.IDidioma
Left Join MACLIENTES cl On c.IDCLiente= cl.IDCliente
where cp.IDCab = @IDCab--7895
Order by cd.Item,cp.Orden
Open curReporteDetallePorPax
Begin
Fetch Next From curReporteDetallePorPax into @IDDet,@Dia,@Item,@Cotizacion,@Cliente,@Titulo,@Responsable,@MargenGeneral,@TipoCambio,
											 @Ubigeo,@Proveedor,@Servicio,@Tipo,@Anio,@Var,@Idioma,@PlanAlimenticio,@TipoPasajero,@FechaInicio,
											 @FechaFin,@ContieneTPL,@Margen,@NombrePax,@Orden,@CostoPaxN1,@SSCostoPaxN1,@STCostoPaxN1,@VentaPaxN1,@SSVentaPaxN1,@STVentaPaxN1,@Notas,@DocWordVersion
	While @@FETCH_STATUS=0
	Begin
	--Insertando el Título
	If Not Exists(select IDDet from RptCostoDetalladoPax where FilTitulo='T')
		Insert into RptCostoDetalladoPax(Cliente,Titulo,Responsable,MargenGeneral,TipoCambio,TipoPasajero,
										 FechaInicio,FechaFin,DocWordVersion,Ubigeo,ContienteTPL,FilTitulo) 
								  values(@Cliente,@Titulo,@Responsable,@MargenGeneral,@TipoCambio,@TipoPasajero,
										 @FechaInicio,@FechaFin,@DocWordVersion,'Fecha',@ContieneTPL,'T')
	
	--Insertando el SubTitulo
	If Not Exists(select IDDet from RptCostoDetalladoPax where FilTitulo='S')
		Insert into RptCostoDetalladoPax(Dia,Ubigeo,Proveedor,Servicio,Tipo,Anio,[Var],Idioma,Margen,FilTitulo) 
					values('Fecha','Ubigeo','Proveedor','Servicio','Tipo','Año','Var','Idi.','%','S')
	
	--Insertando el Detalle
	If Not Exists(select IDDet from RptCostoDetalladoPax where IDDet=@IDDet and FilTitulo='R')
	Begin
		Insert into RptCostoDetalladoPax(IDDet,Dia,Item,Cotizacion,Cliente,Titulo,Responsable,MargenGeneral,
					TipoCambio,Ubigeo,Proveedor,Servicio,Tipo,Anio,[Var],Idioma,PlanAlimenticio,TipoPasajero,
					FechaInicio,FechaFin,ContienteTPL,Margen,FilTitulo) 
		values(@IDDet,@Dia,@Item,@Cotizacion,@Cliente,@Titulo,@Responsable,@MargenGeneral,
			   @TipoCambio,@Ubigeo,@Proveedor,@Servicio,@Tipo,@Anio,@Var,@Idioma,@PlanAlimenticio,
			   @TipoPasajero,@FechaInicio,@FechaFin,@ContieneTPL,@Margen,'R')
	End
	--else
	--Begin
	--Actualizando los Títulos, Sub-Títulos y Costos Dependiendo de la Posición del Pax
	if @Orden = 1
	Begin
		Update RptCostoDetalladoPax set CostoPax1 =@NombrePax where FilTitulo= 'T'
		Update RptCostoDetalladoPax set CostoPax1 ='Costo',VentaPax1='Venta' where FilTitulo='S'
		Update RptCostoDetalladoPax set CostoPax1 =cast(@CostoPaxN1 as numeric(8,2)),SSCostoPax1=CAST(@SSCostoPaxN1 as numeric(8,2)),STCostoPax1=CAST(@STCostoPaxN1 as numeric(8,2)),
										VentaPax1=@VentaPaxN1,SSVentaPax1=cast(@SSVentaPaxN1  as numeric(8,2)),STVentaPax1=CAST(@STVentaPaxN1 as numeric(8,2))
		where IDDet =@IDDet and FilTitulo= 'R'
	End
	if @Orden = 2
	Begin
		Update RptCostoDetalladoPax set CostoPax2 =@NombrePax where FilTitulo= 'T'
		Update RptCostoDetalladoPax set CostoPax2 ='Costo',VentaPax2='Venta' where FilTitulo='S'
		Update RptCostoDetalladoPax set CostoPax2 =cast(@CostoPaxN1 as numeric(8,2)),
										SSCostoPax2=CAST(@SSCostoPaxN1 as numeric(8,2)),VentaPax2=@VentaPaxN1,SSVentaPax2=cast(@SSVentaPaxN1 as numeric(8,2)),
										STCostoPax2=CAST(@STCostoPaxN1 As numeric(8,2)),STVentaPax2=CAST(@STVentaPaxN1 as numeric(8,2))
		where IDDet =@IDDet and FilTitulo= 'R'
	End
	if @Orden = 3
	Begin
		Update RptCostoDetalladoPax set CostoPax3 =@NombrePax where FilTitulo= 'T'
		Update RptCostoDetalladoPax set CostoPax3 ='Costo',VentaPax3='Venta' where FilTitulo='S'
		Update RptCostoDetalladoPax set CostoPax3 =cast(@CostoPaxN1 as numeric(8,2)),
										SSCostoPax3=CAST(@SSCostoPaxN1 as numeric(8,2)),VentaPax3=@VentaPaxN1,SSVentaPax3=cast(@SSVentaPaxN1 as numeric(8,2)),
										STCostoPax3=CAST(@STCostoPaxN1 As numeric(8,2)),STVentaPax3=CAST(@STVentaPaxN1 as numeric(8,2))
		where IDDet =@IDDet and FilTitulo= 'R'
	End
	if @Orden = 4
	Begin
		Update RptCostoDetalladoPax set CostoPax4 =@NombrePax where FilTitulo= 'T'
		Update RptCostoDetalladoPax set CostoPax4 ='Costo',VentaPax4='Venta' where FilTitulo='S'
		Update RptCostoDetalladoPax set CostoPax4 =cast(@CostoPaxN1 as numeric(8,2)),
										SSCostoPax4=CAST(@SSCostoPaxN1 as numeric(8,2)),VentaPax4=@VentaPaxN1,SSVentaPax4=cast(@SSVentaPaxN1 as numeric(8,2)),
										STCostoPax4=CAST(@STCostoPaxN1 As numeric(8,2)),STVentaPax4=CAST(@STVentaPaxN1 as numeric(8,2))
		where IDDet =@IDDet and FilTitulo= 'R'
	End
	if @Orden = 5
	Begin
		Update RptCostoDetalladoPax set CostoPax5 =@NombrePax where FilTitulo= 'T'
		Update RptCostoDetalladoPax set CostoPax5 ='Costo',VentaPax5='Venta' where FilTitulo='S'
		Update RptCostoDetalladoPax set CostoPax5 =cast(@CostoPaxN1 as numeric(8,2)),
										SSCostoPax5=CAST(@SSCostoPaxN1 as numeric(8,2)),VentaPax5=@VentaPaxN1,SSVentaPax5=cast(@SSVentaPaxN1 as numeric(8,2)),
										STCostoPax5=CAST(@STCostoPaxN1 As numeric(8,2)),STVentaPax5=CAST(@STVentaPaxN1 as numeric(8,2))
		where IDDet =@IDDet and FilTitulo= 'R'
	End
	--End
		
	Fetch Next From curReporteDetallePorPax into @IDDet,@Dia,@Item,@Cotizacion,@Cliente,@Titulo,@Responsable,@MargenGeneral,@TipoCambio,
												 @Ubigeo,@Proveedor,@Servicio,@Tipo,@Anio,@Var,@Idioma,@PlanAlimenticio,@TipoPasajero,@FechaInicio,
												 @FechaFin,@ContieneTPL,@Margen,@NombrePax,@Orden,@CostoPaxN1,@SSCostoPaxN1,@STCostoPaxN1,@VentaPaxN1,@SSVentaPaxN1,@STVentaPaxN1,@Notas,@DocWordVersion
	End
End
Close curReporteDetallePorPax
DealLocate curReporteDetallePorPax

Update RptCostoDetalladoPax set CostoPax1=ISNULL(RptCostoDetalladoPax.CostoPax1,0.00),
								SSVentaPax1=ISNULL(RptCostoDetalladoPax.SSVentaPax1,0.00),
								SSCostoPax1=ISNULL(RptCostoDetalladoPax.SSCostoPax1,0.00),
								STCostoPax1=ISNULL(RptCostoDetalladoPax.STCostoPax1,0.00),
								STVentaPax1=ISNULL(RptCostoDetalladoPax.STVentaPax1,0.00)
where Exists(select IDDet from RptCostoDetalladoPax where CostoPax1 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set VentaPax1=ISNULL(RptCostoDetalladoPax.VentaPax1,0.00)
where Exists(select IDDet from RptCostoDetalladoPax where VentaPax1 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set CostoPax2=ISNULL(RptCostoDetalladoPax.CostoPax2,0.00),
								SSVentaPax2=ISNULL(RptCostoDetalladoPax.SSVentaPax2,0.00),
								SSCostoPax2=ISNULL(RptCostoDetalladoPax.SSCostoPax2,0.00),
								STCostoPax2=ISNULL(RptCostoDetalladoPax.STCostoPax2,0.00),
								STVentaPax2=ISNULL(RptCostoDetalladoPax.STVentaPax2,0.00)
where Exists(select IDDet from RptCostoDetalladoPax where CostoPax2 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set VentaPax2=ISNULL(RptCostoDetalladoPax.VentaPax2,0.00) 
where Exists(select IDDet from RptCostoDetalladoPax where VentaPax2 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set CostoPax3=ISNULL(RptCostoDetalladoPax.CostoPax3,0.00),
								SSVentaPax3=ISNULL(RptCostoDetalladoPax.SSVentaPax3,0.00),
								SSCostoPax3=ISNULL(RptCostoDetalladoPax.SSCostoPax3,0.00),
								STCostoPax3=ISNULL(RptCostoDetalladoPax.STCostoPax3,0.00),
								STVentaPax3=ISNULL(RptCostoDetalladoPax.STVentaPax3,0.00)
where Exists(select IDDet from RptCostoDetalladoPax where CostoPax3 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set VentaPax3=ISNULL(RptCostoDetalladoPax.VentaPax3,0.00) 
where Exists(select IDDet from RptCostoDetalladoPax where VentaPax3 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set CostoPax4=ISNULL(RptCostoDetalladoPax.CostoPax4,0.00),
								SSVentaPax4=ISNULL(RptCostoDetalladoPax.SSVentaPax4,0.00),
								SSCostoPax4=ISNULL(RptCostoDetalladoPax.SSCostoPax4,0.00),
								STCostoPax4=ISNULL(RptCostoDetalladoPax.STCostoPax4,0.00),
								STVentaPax4=ISNULL(RptCostoDetalladoPax.STVentaPax4,0.00)
where Exists(select IDDet from RptCostoDetalladoPax where CostoPax4 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set VentaPax4=ISNULL(RptCostoDetalladoPax.VentaPax4,0.00) 
where Exists(select IDDet from RptCostoDetalladoPax where VentaPax4 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set CostoPax5=ISNULL(RptCostoDetalladoPax.CostoPax5,0.00),
								SSVentaPax5=ISNULL(RptCostoDetalladoPax.SSVentaPax5,0.00),
								SSCostoPax5=ISNULL(RptCostoDetalladoPax.SSCostoPax5,0.00),
								STCostoPax5=ISNULL(RptCostoDetalladoPax.STCostoPax5,0.00),
								STVentaPax5=ISNULL(RptCostoDetalladoPax.STVentaPax5,0.00)
where Exists(select IDDet from RptCostoDetalladoPax where CostoPax5 is Not Null) and FilTitulo='R'

Update RptCostoDetalladoPax set VentaPax5=ISNULL(RptCostoDetalladoPax.VentaPax5,0.00) 
where Exists(select IDDet from RptCostoDetalladoPax where VentaPax5 is Not Null) and FilTitulo='R'

select * from RptCostoDetalladoPax
Order by Item
