﻿CREATE Procedure dbo.COTIDET_SelExisteCostoDifCero
@IDDet int,
@pCostoDiferenteCero bit output
As
Set NoCount On
Set @pCostoDiferenteCero = 0
Select @pCostoDiferenteCero = Case When CostoRealEditado=1 And CostoReal = 0 then 0 else 1 End
from COTIDET Where IDDET=@IDDet
