﻿
--HLF-20150708-Nuevo campo NuNroVehiculo
CREATE Procedure dbo.ACOMODO_VEHICULO_DET_PAX_Del
	@NuVehiculo smallint,
	@NuNroVehiculo tinyint,
	@IDDet int,
	@NuPax int
As
	Set Nocount On

	Delete From ACOMODO_VEHICULO_DET_PAX
	Where NuVehiculo=@NuVehiculo And IDDet=@IDDet and NuPax=@NuPax
		And NuNroVehiculo=@NuNroVehiculo

