﻿Create Procedure [dbo].[MAINCLUIDO_Sel_Rpt] 
@Descripcion varchar(150),
@Tipo char(1)
as

	Set NoCount On
	
	Select 
	IDIncluido,
	case Tipo when 'I' then 'Incluido' when 'N' then 'No Incluido' End as Tipo,
	Descripcion
	from MAINCLUIDO
	Where (Descripcion like '%'+@Descripcion+'%' Or LTRIM(rtrim(@Descripcion))='')
	 And (Tipo=@Tipo Or LTRIM(RTRIM(@Tipo))='')
