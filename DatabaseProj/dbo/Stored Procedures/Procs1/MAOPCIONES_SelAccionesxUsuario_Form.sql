﻿CREATE Procedure [dbo].[MAOPCIONES_SelAccionesxUsuario_Form]
	@IDUsuario	char(4),
	@Form	varchar(50)	
As
	Set NoCount On
	--Declare @Nivel	tinyint
	--Declare @IDOPc char(6), @IDOPcPadre varchar(6)
	
	--Select @IDOPc = IDOPc From MAOPCIONES Where Nombre=@Form
	
	--If SUBSTRING(@IDOPc,3,4)='0000' 
	--	Begin
	--	Set @IDOPcPadre=LEFT(@IDOPc,2)
	--	Set @Nivel=2
		
	--	End
	--Else
	--	Begin
	--	If SUBSTRING(@IDOPc,5,2)='00' 
	--		Begin
	--		Set @IDOPcPadre=LEFT(@IDOPc,4)
	--		Set @Nivel=4
			
	--		End
	--	End
		
	Select mo.IDOPc,mo.Nombre,uo.Consultar, uo.Actualizar, uo.Eliminar, uo.Grabar
	From MAOPCIONES mo Left Join MAUSUARIOSOPC uo On mo.IDOPc=uo.IDOpc And uo.IDUsuario=@IDUsuario
	--Where SUBSTRING(mo.IDOpc,1,@Nivel) = @IDOPcPadre
	Where mo.Nombre=@Form
