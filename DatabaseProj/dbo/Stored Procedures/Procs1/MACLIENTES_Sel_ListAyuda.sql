﻿


--HLF-20130312-Agregando u.Descripcion as DescPais   
--JRF-20140711-Agregar el filtro de   
--HLF-20141119- isnull(p.FlTop,0) as Margen         
--JHD-20150408 - isnull(p.Nuposicion,0) as OperadorInt
CREATE Procedure [dbo].[MACLIENTES_Sel_ListAyuda]  
 @RazonSocial varchar(60),  
 @CoTipoVenta char(2)  
As        
 Set NoCount On        
 Select p.RazonComercial as Descripcion,        
 --p.IDCiudad, u.IDPais,        
 p.Ciudad as IDCiudad, p.IDCiudad as IDPais, u.Descripcion as DescPais,    
 p.IDCliente as Codigo,
 isnull(p.FlTop,0) as Margen,
 isnull(p.NuPosicion,0) as OperadorInt
 From MACLIENTES p         
 Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo        
 Where (p.RazonComercial Like '%'+@RazonSocial+'%' Or LTRIM(rtrim(@RazonSocial))='')         
 And p.Activo='A' and (LTRIM(RTRIM(@CoTipoVenta))='' Or p.CoTipoVenta = @CoTipoVenta)  
 Order by RazonComercial        

