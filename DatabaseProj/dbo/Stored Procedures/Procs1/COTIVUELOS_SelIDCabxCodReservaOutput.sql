﻿Create Procedure dbo.COTIVUELOS_SelIDCabxCodReservaOutput
	@CodReserva varchar(20),
	@TipoTransporte char(1),
	@pIDCab int output
As
	Set Nocount on

	Set @pIDCab=0

	Set @pIDCab=isnull((Select top 1 isnull(cv.IDCAB ,0) 
	From COTIVUELOS cv Inner Join COTIPAX cp On cv.IDCAB=cp.IDCab and charindex('Pax',cp.Nombres)=0 where 
	cv.TipoTransporte=@TipoTransporte  and cv.CodReserva=@CodReserva),0)

