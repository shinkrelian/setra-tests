﻿create procedure dbo.DEBIT_MEMO_DOCUMENTO_DelxFK
@NuDocum char(10),
@IDTipoDoc char(3)
As
Set NoCount On
Delete from DEBIT_MEMO_DOCUMENTO
where NuDocum = @NuDocum and IDTipoDoc=@IDTipoDoc
