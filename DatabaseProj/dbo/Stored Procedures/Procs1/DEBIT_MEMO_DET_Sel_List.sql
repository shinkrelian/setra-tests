﻿

Create Procedure dbo.DEBIT_MEMO_DET_Sel_List
@IDDebitMemo char(10)
As
	Set NoCount On
	SELECT [Descripcion]
		  ,[CapacidadHab]
		  ,[NroPax]
		  ,[SubTotal]
		  ,[TotalIGV]
		  ,[Total] As TotalDet
  FROM [dbo].[DEBIT_MEMO_DET]
  WHERE [dbo].[DEBIT_MEMO_DET].[IDDebitMemo] = @IDDebitMemo
