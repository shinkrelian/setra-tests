﻿
Create Procedure dbo.DEBIT_MEMO_UpdImprimir
@IDDebitMemo char(10),
@UserMod char(4)
As
	Set NoCount On
	Update DEBIT_MEMO
		Set FlImprimioDebit = 1,
			UserMod = @UserMod,
			FecMod = GetDate()
	where IDDebitMemo = @IDDebitMemo
