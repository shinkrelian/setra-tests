﻿CREATE Procedure dbo.COTICAB_SelCabeceraOrdenServicio
@IDCab int
As
	Set NoCount On
	select IDFile,Cotizacion,cl.RazonComercial as DescCliente,Titulo,
	 convert(char(10),c.FecInicio,103) as FechaIN,CONVERT(char(10),c.FechaOut,103) as FechaOUT,c.NroPax+IsNull(c.NroLiberados,0) as NroPax
	from COTICAB c Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente
	where IDCAB = @IDCab
