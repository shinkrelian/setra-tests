﻿CREATE Procedure [dbo].[MANIVELUSUARIO_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(50)
As
	Set NoCount On
	
	Select Descripcion	
	From MANIVELUSUARIO
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDNivel <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
