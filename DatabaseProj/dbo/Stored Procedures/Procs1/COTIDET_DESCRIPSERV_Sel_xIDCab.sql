﻿--HLF-20120822-Inner Join MASERVICIOS s On d.IDServicio=s.IDServicio And s.SinDescripcion=0                      
--HLF-20121012-and d1.IDProveedor=d.IDProveedor en los Case When                      
--HLF,JRF-20121017-Cambios Desayuno hoteles (quitando d1.IDProveedor=d.IDProveedor),                     
--Agregando subquerys sumando para hoteles en almuerzo, lonche, cena                    
--JRF-20121022-Agregar los dias de servicio que son solo de Hoteles.(1 Día o todos los dias son hoteles)                  
--HLF,JRF-20121029-Nuevos campos LoncheDescripcion,AlmuerzoDescripcion,CenaDescripcion para Descripcion y otros para Abreviación.                
--JRF-20130919-Cambio SubQuery Para saber Ubigeo del Hotel Anterior.        
--HLF-20131002-Cambiando Inner Join MASERVICIOS s por Left Join MASERVICIOS s    
--JRF-20131030-Considerando Tambien a los Hoteles con planAlimenticio.  
--JRF-20130313-Considerar Los Nuevo campos DocWordVersion y DocWordFecha
--JHD-20150710-cc.Cotizacion
--JRF-20151117-Agregar la columna.. [FnDevuelveTituloTablaHoteles]
CREATE Procedure [dbo].[COTIDET_DESCRIPSERV_Sel_xIDCab]
 @IDCab int,                      
 @IDIdioma varchar(12)                      
As                      
Begin                    
 Set NoCount On                                        
 Select cc.Titulo, ISNULL(us.Siglas,'') as UsuarioCotSigla, cc.DocWordVersion ,Convert(Char(10),Isnull(cc.DocWordFecha,Getdate()),103) as FechaWord,
 DATEDIFF(d,cc.FecInicio, cc.FechaOut)+1 as Dias, d.Dia As Fecha_dt,      
 CHARINDEX(CONVERT(Char(10),(select Max(Dia) from COTIDET cd2 Where cd2.IDCab = @IDCab),103)      
 ,CONVERT(Char(10),d.Dia,103)) as FlUltimoDia,      
 d.Item, substring(convert(varchar,d.Dia,112),1,6) As AnioMes,                    
                       
 --d.Desayuno,d.Lonche ,d.Almuerzo, d.Cena,                      
 Case When                       
  (                      
  (Select COUNT(*) From COTIDET d1                       
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv<>'001'                      
  Where IDCAB=@IDCab And d1.Dia=d.Dia And d1.Desayuno=1                     
 --and d1.IDProveedor=d.IDProveedor                   
  )                      
                  
  +                       
  (Select COUNT(*) From COTIDET d1                       
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'                      
  Where IDCAB=@IDCab And  d1.Dia=Dateadd(d,-1,d.Dia) And d1.Desayuno=1                     
 --and d1.IDProveedor=d.IDProveedor                    
 )                     
--  d1.IDProveedor In (select distinct IDProveedor from COTIDET d2 where IDCAB=@IDCab and d2.Dia=d1.Dia.Dia ) ) --d1.IDProveedor='001554')                    
  )>=1 Then 1                       
 Else 0                       
  End                      
 as Desayuno,                      
                       
 Case When (Select COUNT(*) From COTIDET d1                   
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv<>'001'                            
  Where IDCAB=@IDCab And d1.Dia=d.Dia And d1.Lonche=1)                    
  +                    
    (Select COUNT(*) From COTIDET d1                       
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'                      
  Where IDCAB=@IDCab And  d1.Dia=d.Dia And d1.Lonche=1                      
 )                       
                    
  >=1 Then 1 Else 0 End                      
                      
  as Lonche,                 
                  
                      
 Case When (Select COUNT(*) From COTIDET d1                       
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv<>'001'                        
  Where IDCAB=@IDCab And d1.Dia=d.Dia And d1.Almuerzo=1 )                    
    +                    
  (Select COUNT(*) From COTIDET d1                       
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'                      
  Where IDCAB=@IDCab And  d1.Dia=d.Dia And d1.Almuerzo=1                      
 )                       
                    
  >=1                     
                      
  Then 1 Else 0 End                      
                   
   as Almuerzo,                     
                        
 Case When                     
 (Select COUNT(*) From COTIDET d1                      
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv<>'001'                      
  Where IDCAB=@IDCab And d1.Dia=d.Dia And d1.Cena=1 )                    
  +                    
    (Select COUNT(*) From COTIDET d1                       
  Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'                      
  Where IDCAB=@IDCab And  d1.Dia=d.Dia And d1.Cena=1                     
                     
 )                       
                      
  >=1                     
                      
  Then 1 Else 0 End                      
           
   as Cena,                      
                   
---****                
 --d.Desayuno,d.Lonche ,d.Almuerzo, d.Cena,                      
 --0                    
 d.Desayuno  
 as DesayunoDescripcion,                      
                       
 --Case When (Select COUNT(*) From COTIDET d1   
 -- Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv<>'001'                            
 -- Where IDCAB=@IDCab And d1.Dia=d.Dia And d1.Lonche=1 and d1.IDProveedor = d.IDProveedor )                    
 -- +                    
 --   (Select COUNT(*) From COTIDET d1                       
 -- Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'                      
 -- Where IDCAB=@IDCab And  d1.Dia=d.Dia And d1.Lonche=1                      
 --)                       
 -- >=1 Then 1 Else 0 End                      
  d.Lonche                      
  as LoncheDescripcion,                 
                  
                      
 --Case When (Select COUNT(*) From COTIDET d1                       
 -- Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv<>'001'                        
 -- Where IDCAB=@IDCab And d1.Dia=d.Dia And d1.Almuerzo=1 and d1.IDProveedor = d.IDProveedor)                    
 --   +                    
 -- (Select COUNT(*) From COTIDET d1                       
 -- Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'                      
 -- Where IDCAB=@IDCab And  d1.Dia=d.Dia And d1.Almuerzo=1                      
 --)                       
                    
 -- >=1                     
                      
 -- Then 1 Else 0 End                      
 d.Almuerzo                    
   as AlmuerzoDescripcion,                     
                        
 --Case When                     
 --(Select COUNT(*) From COTIDET d1                      
 -- Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv<>'001'                      
 -- Where IDCAB=@IDCab And d1.Dia=d.Dia And d1.Cena=1 and d1.IDProveedor = d.IDProveedor)                    
 -- +                    
 --   (Select COUNT(*) From COTIDET d1                       
 -- Inner Join MAPROVEEDORES p1 On d1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'                      
 -- Where IDCAB=@IDCab And  d1.Dia=d.Dia And d1.Cena=1                     
                     
 --)                                  
 -- >=1                     
 -- Then 1 Else 0 End                      
  d.Cena                    
   as CenaDescripcion,                      
               
 upper(substring(DATENAME(dw,d.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,d.Dia,103))) as Dia,                         
  IsNull(IsNull(c.DescCorta,d.Servicio),'') as DescCorta,              
            
 Case When Exists(select IDidioma from COTIDET_DESCRIPSERV where IDidioma = @IDIdioma and IDDet = d.IDDET) then          
  IsNull(IsNull(Cast(c.DescLarga as varchar(max)),d.Servicio),'') else '' End as DescLarga,                      
 cc.DocWordSubTitulo, cc.DocWordTexto,  
        
 dbo.FnDescUbigeoHoteluOperAnterior(d.IDCAB,d.Dia,0) as Destino,                      
 dbo.FnDescUbigeoHoteluOperAnterior(d.IDCAB,d.Dia,1)          as AnteriorDestino,                      
                       
 p.IDTipoProv, d.IDDET, d.IDDetTransferOri, d.IDDetTransferDes, p.NombreCorto as DescProveedor,  
 sd.PlanAlimenticio,
 cc.Cotizacion,
 IsNull(dbo.FnDevuelveTituloTablaHoteles(cc.IDCAB),'') as TituloHotel
             
 From COTIDET d Left Join                       
 (Select IDDet, IDIdioma, DescCorta, DescLarga From COTIDET_DESCRIPSERV Where IDIdioma=@IDIdioma) c                          
 On d.IDDet=c.IDDET                       
 Left Join COTICAB cc On d.IDCAB=cc.IDCAB                      
 Left Join MAUBIGEO u On d.IDubigeo=u.IDubigeo                      
 Left Join MAPROVEEDORES p On d.IDProveedor=p.IDProveedor                      
 Left Join MASERVICIOS s On d.IDServicio=s.IDServicio And s.SinDescripcion=0                      
 Left Join MASERVICIOS_DET sd On d.IDServicio_Det = sd.IDServicio_Det  
 Left Join MAUSUARIOS us On cc.IDUsuario = us.IDUsuario
    Where d.IDCAB = @IDCab And --p.IDTipoProv<>'001'                  
    (p.IDTipoProv<>'001' Or (p.IDTipoProv = '001' and sd.PlanAlimenticio = 1) Or                  
     d.Dia In (select Dia from (select COUNT(Dia) As ServicioxDia,d.Dia,                  
      Case When (Select COUNT(d2.IDProveedor) from COTIDET d2 Left Join MAPROVEEDORES p On d2.IDProveedor=p.IDProveedor                   
    Where d2.Dia = d.Dia And IDCAB = @IDCab And p.IDTipoProv = '001') = (Select COUNT(Dia))                  
    then 'H' Else 'NH'                  
    End As Hoteles from COTIDET d where IDCAB = @IDCab Group by d.Dia ) As X Where Hoteles = 'H'                    
)) And d.IncGuia = 0                  
 Order by d.Dia, d.Item                    
End
