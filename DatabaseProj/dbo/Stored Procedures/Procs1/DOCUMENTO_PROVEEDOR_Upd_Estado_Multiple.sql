﻿CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Upd_Estado_Multiple]
	@NuDocumProv int,
	--@CoEstado char(2),
	@FlActivo bit=0,
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[DOCUMENTO_PROVEEDOR]
	Set
		FlActivo = @FlActivo,
 		[UserMod] = @UserMod,
 		fecmod=GETDATE()
 	Where 
		[NuDocumProv] = @NuDocumProv


	Update [dbo].[DOCUMENTO_PROVEEDOR]
	Set
		FlActivo = @FlActivo,
 		[UserMod] = @UserMod,
 		fecmod=GETDATE()
 	Where 
		[NuDocum_Multiple] = @NuDocumProv
END;
