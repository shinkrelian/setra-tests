﻿Create Procedure dbo.INGRESO_FINANZAS_UpdSsSaldoMatchDocumento
	@IDCab int,
	@UserMod char(4)
As
	Set Nocount On
	Update INGRESO_FINANZAS 
	Set SsSaldoMatchDocumento=SsRecibido, UserMod=@UserMod, FecMod=GETDATE()
	Where NuIngreso In (Select NuIngreso From INGRESO_DEBIT_MEMO idm 
	Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo
	Where dm.IDCab=@IDCab)
	
