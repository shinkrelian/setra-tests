﻿CREATE Procedure [dbo].[MACONTACTOSPROVEEDOR_Sel_List]	
	@IDProveedor char(6),
	@Nombres	varchar(100)
As
	Set NoCount On
	
	Select Nombres, Apellidos, Cargo, Telefono, Celular, IDContacto 
	FROM MACONTACTOSPROVEEDOR 
    WHERE IDProveedor=@IDProveedor And
    (LTRIM(RTRIM(@Nombres))='' Or (Nombres+' '+Apellidos Like '%'+@Nombres+'%'))
