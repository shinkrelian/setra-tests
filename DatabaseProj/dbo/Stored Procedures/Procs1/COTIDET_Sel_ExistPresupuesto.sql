﻿Create Procedure dbo.COTIDET_Sel_ExistPresupuesto
@IDDet int,
@pExistePresupuesto bit output
As
Set NoCount On
Set @pExistePresupuesto = 0
Select @pExistePresupuesto = 1
from COTIDET cd Inner Join RESERVAS_DET rd On cd.IDDet= rd.IDDet
Inner Join OPERACIONES_DET od On od.IDReserva_Det = rd.IDReserva_Det
Inner Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det And od.IDServicio=ods.IDServicio And activo=1
Inner Join PRESUPUESTO_SOBRE_DET psd On ods.IDOperacion_Det=psd.IDOperacion_Det And ods.IDServicio_Det=psd.IDServicio_Det
Inner Join PRESUPUESTO_SOBRE ps On psd.NuPreSob=ps.NuPreSob
Where rd.iddet= @IDDet And ps.CoEstado='EN'
