﻿
--HLF-20130208-Agregando parametro Orden      
--JRF-20130227-Agregando parametro Residente  
--JRF-20130515-Agregando Columnas [IDPaisResidencia,FecIngresoPais]  
--JRF-20131003-Agregando Colunmna [PassValidado]
--MLL-20140527-Agregando Colunmna TxTituloAcademico ,FlTC
--JHD-20141009 Agregado columna NoShow
--HLF-20150318-Agregando ,@FlEntMP ,@FlEntWP ,@FlAutorizarMPWP
--JHD-20150423-Agregando ,@FlAdultoINC
--PPMG-20151109-Se agrego el nuevo campo NumberAPT
--PPMG-20160428-Se aumento el lenght @NumIdentidad varchar(25)
CREATE Procedure [dbo].[COTIPAX_Upd]        
 @IDPax int,      
 @Orden smallint, 
 @IDIdentidad char(3),
 @NumIdentidad varchar(25),
 @Nombres varchar(50), 
 @Apellidos varchar(50),
 @Titulo varchar(5), 
 @FecNacimiento smalldatetime,
 @IDNacionalidad char(6), 
 @Peso Numeric(5,2), 
 @ObservEspecial varchar(200),
 @Residente bit, 
 @IDPaisResidencia char(6),
 @FecIngresoPais smalldatetime,
 @PassValidado bit,
 @UserMod char(4) ,
 @TxTituloAcademico varchar(5),
 @FlTC  bit , 
 @FlNoShow  bit =0,

 @FlEntMP bit=0,
 @FlEntWP bit=0,
 @FlAutorizarMPWP bit=0,

 @FlAdultoINC bit=0,
 @NumberAPT varchar(20) = NULL
AS
BEGIN
 Set NoCount On        

 Update COTIPAX Set                   
 IDIdentidad=@IDIdentidad        
 ,Orden=@Orden      
 ,NumIdentidad=Case When ltrim(rtrim(@NumIdentidad))='' Then Null Else @NumIdentidad End        
 ,Nombres=@Nombres        
 ,Apellidos=@Apellidos    
 ,Titulo=@Titulo        
 ,FecNacimiento=Case When @FecNacimiento='01/01/1900' Then Null Else @FecNacimiento End        
 ,IDNacionalidad=@IDNacionalidad        
 ,Peso = Case When @Peso = 0 then Null else @Peso End      
 ,ObservEspecial = Case When @ObservEspecial = '' Then Null Else @ObservEspecial End      
 ,Residente = @Residente   
 ,IDPaisResidencia = @IDPaisResidencia  
 ,FecIngresoPais = Case When @FecIngresoPais='01/01/1900' Then Null Else @FecIngresoPais End
 ,PassValidado = @PassValidado
 ,UserMod=@UserMod
 ,FecMod=GETDATE()
 ,TxTituloAcademico =@TxTituloAcademico  
 ,FlTC=@FlTC
 ,FlNoShow=@FlNoShow

 ,FlEntMP=@FlEntMP
 ,FlEntWP=@FlEntWP
 ,FlAutorizarMPWP=@FlAutorizarMPWP

 ,FlAdultoINC=@FlAdultoINC
 ,NumberAPT = @NumberAPT
  Where IDPax=@IDPax
END

