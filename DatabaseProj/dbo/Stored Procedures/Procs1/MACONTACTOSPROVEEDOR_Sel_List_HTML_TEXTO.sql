﻿--JRF-20130514-Solo se necesita la inf. de Emergencia del Proveedor
CREATE Procedure [dbo].[MACONTACTOSPROVEEDOR_Sel_List_HTML_TEXTO]       
 @IDProveedor char(6)      
As      
 Set NoCount On      
       
 --Select  Titulo +' ' + Nombres+' '+ Apellidos As Nombres, IsNull(Telefono,'') Telefono    
 --, IsNull(Celular,'') Celular,IsNull(Email,'') Email    
 --FROM MACONTACTOSPROVEEDOR       
 --WHERE IDProveedor=@IDProveedor  
 --union   
 select isnull(NomContacto,'') as Nombres,'' as Telefono,isnull(Celular,'') as Celular,'' as Email  
 from MAPROVEEDORES  
 where IDProveedor = @IDProveedor and Celular is not null  
