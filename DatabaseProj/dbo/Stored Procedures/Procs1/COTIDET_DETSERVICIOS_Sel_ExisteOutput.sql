﻿Create Procedure [dbo].[COTIDET_DETSERVICIOS_Sel_ExisteOutput]
	@IDDET int,
	@IDServicio_Det	int, 
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDDet From COTIDET_DETSERVICIOS
		WHERE IDDet=@IDDet	And IDServicio_Det=@IDServicio_Det)
		Set @pExiste=1
	Else
		Set @pExiste=0
