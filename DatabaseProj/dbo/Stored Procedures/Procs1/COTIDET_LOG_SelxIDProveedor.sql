﻿--HLF-20130628-Agregando campo Accion      
--HLF-20130701-Agregando campo IDTipoProv      
--HLF-20130702-Agregando campo IDServicio_Det       
--HLF-20130703-Agregando campos sd.PlanAlimenticio, sd.ConAlojamiento    
--HLF-20130827-Agregando Case When para IDServicio_Det  
--HLF-20130924-Agragando Top 1
CREATE Procedure [dbo].[COTIDET_LOG_SelxIDProveedor]        
 @IDCab int,        
 @IDProveedor char(6)        
As        
 Set NoCount On        
      
 Select l.IDDET, l.Accion, p.IDTipoProv,   
 --sd.IDServicio_Det,  
 Case When Accion='N' Then  
  isnull((Select Top 1 IDServicio_Det From COTIDET_LOG 
	Where Accion='B' And IDCAB=@IDCab And Item=l.Item
	Order by FechaLog Desc),sd.IDServicio_Det)  
 Else  
  sd.IDServicio_Det  
 End as IDServicio_Det,  
  
 sd.PlanAlimenticio, sd.ConAlojamiento       
 From COTIDET_LOG l       
 Left Join MASERVICIOS_DET sd On l.IDServicio_Det=sd.IDServicio_Det      
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio      
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor      
 Where Atendido=0 And l.IDProveedor=@IDProveedor        
 And IDCab=@IDCab And Accion<>' '        
