﻿
 Create Procedure dbo.COTIDET_PAX_UpdxIDPaxNoShow
	@IDCab int,
	@IDPax int,
	@FlDesactNoShow bit,
	@UserMod char(4)
As
	Set Nocount on
	
	Update COTIDET_PAX Set FlDesactNoShow=@FlDesactNoShow, UserMod=@UserMod, FecMod=GETDATE()
	Where IDPax=@IDPax And IDDet In (Select IDDet From COTIDET Where IDCAB=@IDCab)

