﻿
CREATE PROCEDURE [dbo].[DEBIT_MEMO_DET_Sel_List_InvoicePaymentwall]	
							@IDDebitMemo char(10)
AS
BEGIN
	Set NoCount On
	SELECT D.NroPax AS quantity,
		   D.CostoPersona AS unit_cost,
		   dbo.FnEquivalenciaMoneda_ISO_4217(C.IDMoneda) AS currency,
		   D.Descripcion AS title,
		   'percentage' AS typeTax,
		   CASE WHEN ISNULL(D.TotalIGV,0) = 0 THEN 0 ELSE ROUND(((D.TotalIGV/D.SubTotal)*100),2) END AS valueTax
  FROM DEBIT_MEMO_DET D INNER JOIN DEBIT_MEMO C ON D.IDDebitMemo = C.IDDebitMemo 
  WHERE D.IDDebitMemo = @IDDebitMemo
END
