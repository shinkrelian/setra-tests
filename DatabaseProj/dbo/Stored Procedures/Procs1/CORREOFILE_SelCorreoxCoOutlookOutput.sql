﻿--JRF-20160210-Filtrar solo por codigo Outlook
--JRF-20160219-Agregar la columna de CoEstado
--JRF-20160323-Agregar cotización/File
--JRF-20160408-Agregar IDCliente
CREATE Procedure [dbo].[CORREOFILE_SelCorreoxCoOutlookOutput]
	@CoCorreoOutlook varchar(255)
	--@NuPedInt int,
	--@pNuCorreo int Output
As	
	Set NoCount On
	--Set @pNuCorreo = 0
	Select [NuCorreo]
		  ,c.[IDCab]
		  ,[CoUsrIntDe]
		  ,[CoUsrPrvDe]
		  ,[CoUsrCliDe]
		  ,[NoCuentaDe]
		  ,[FeCorreo]
		  ,[CoTipoBandeja]
		  ,[NoAsunto]
		  ,[TxMensaje]
		  ,[CoFormato]
		  ,[QtAdjuntos]
		  ,[FlImportancia]
		  ,[FlLeido]
		  ,[CoCorreoOutlook]
		  ,[CoArea]
		  ,IsNull(co.[NuPedInt],0) as NuPedInt
		  ,co.Cotizacion
		  ,IsNull(co.IDFile,'') as IDFile
		  ,co.CoEstadoAddIn
		  ,IsNull(co.IDCliente,'') as IDCliente,
		  c.CoTipoBandeja
	From .BDCORREOSETRA..CORREOFILE c --Left Join PEDIDO p On c.NuPedInt=p.NuPedInt
	Inner Join COTICAB co On c.IDCab=co.IDCab
	Where CoCorreoOutlook=@CoCorreoOutlook --And IsNull(NuPedInt,0)=@NuPedInt
