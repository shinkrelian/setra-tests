﻿--HLF-20131218-Agregando And rd2.Anulado=0 as subquery Servicio      
--HLF-20131218-select Top 1 --distinct       
--JRF-20140528-Considerar descTC para cuando el campo FlTC este activo.  
--JRF-20140722-Ordenar por SoloGuia asc
CREATE Procedure dbo.ACOMODOPAX_PROVEEDOR_SelxIDCab            
@IDcab int            
As            
Select *, dbo.FnDescripcionAcomodosReservas(X.CapacidadHab,X.EsMatrimonial) as Acomodo    
from (select             
(select min(Dia) from reservas_det rd Where rd.IDReserva = ac.IDReserva and rd.Anulado =0) as FechaIn,            
ac.IDReserva ,c.Titulo,c.IDFile,u.Nombre as Responsable,p.NombreCorto,ac.CapacidadHab,ac.EsMatrimonial,ac.IDHabit,            
cp.Orden,          
          
(select Top 1 --distinct       
rd2.Servicio           
from RESERVAS_DET rd2 Left Join MASERVICIOS_DET sd On rd2.IDServicio_Det = sd.IDServicio_Det and rd2.Anulado = 0          
where rd2.IDReserva = ac.IDReserva and CapacidadHab = ac.CapacidadHab and EsMatrimonial = ac.EsMatrimonial and sd.PlanAlimenticio = 0      
And rd2.Anulado=0)          
as Servicio,          
(select Top 1 --distinct       
sd.Descripcion as Servicio           
from RESERVAS_DET rd2 Left Join MASERVICIOS_DET sd On rd2.IDServicio_Det = sd.IDServicio_Det and rd2.Anulado = 0          
where rd2.IDReserva = ac.IDReserva and CapacidadHab = ac.CapacidadHab and EsMatrimonial = ac.EsMatrimonial and sd.PlanAlimenticio = 0      
And rd2.Anulado=0)          
as Servicio2,    
    
cp.IDPax, cp.Titulo + ' '+cp.Nombres+' ' + cp.Apellidos as NombrePax,            
tid.Descripcion as TipoDoc,Isnull(cp.NumIdentidad,'') as NumIdentidad,            
case when cp.FecNacimiento is null then '' Else CONVERT(char(10),cp.FecNacimiento,103) End As FechNacimiento,            
cp.Peso,Isnull(Ub.DC,'') as NacionalidadPax,Isnull(cp.ObservEspecial,'') as ObservEspecial ,    
 case when (select COUNT(iddet) from cotidet cd      
 where AcomodoEspecial = 1 and cd.IncGuia = 0 and IDCab = @IDCab and r.IDProveedor = cd.IDProveedor)>0       
 then 1 Else 0 End as ExisteAcomodoEspecial , p.IDProveedor,r.IDCab  ,  
 Case When cp.FlTC = 1 then 'TC' else '' End as DescTC  ,
Case When Exists(select cd.IncGuia
from RESERVAS_DET rd Left Join COTIDET cd On rd.IDDet = cd.IDDET
where rd.IDReserva = r.IDReserva and rd.Anulado = 0 and Isnull(cd.IncGuia,0) = 0) then 0 Else 1 End as SoloGuias

from  ACOMODOPAX_PROVEEDOR ac Left Join RESERVAS r On ac.IDReserva = r.IDReserva            
Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor            
Left Join COTICAB c On r.IDCab = c.IDCAB Left Join MAUSUARIOS u On c.IDUsuarioRes = u.IDUsuario            
Left Join COTIPAX cp On ac.IDPax = cp.IDPax Left Join MAUBIGEO Ub On cp.IDNacionalidad = Ub.IDubigeo            
Left Join MATIPOIDENT tid On cp.IDIdentidad = tid.IDIdentidad            
where r.IDCab = @IDcab and p.IDTipoProv = '001' and (r.Estado = 'OK' Or r.Estado = 'RR'))             
As X            
Where X.FechaIn is not null  and X.Servicio is not null        
Order by SoloGuias asc,cast(convert(char(10),X.FechaIn,103) as smalldatetime),X.CapacidadHab,X.IDHabit,X.Orden           
