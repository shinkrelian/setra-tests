﻿  
CREATE Procedure dbo.COTICAB_UpdEStadoFacturac  
 @IDCab int,  
 @EstadoFacturac char(1),  
 @UserMod char(4)  
As  
 Set Nocount On  
   
 Update COTICAB Set EstadoFacturac=@EstadoFacturac,   
 FeFacturac=Case When @EstadoFacturac='F' Then GETDATE() Else FeFacturac End,
 UserMod=@UserMod, FecMod=GETDATE()  
 Where IDCAB=@IDCab  
   
