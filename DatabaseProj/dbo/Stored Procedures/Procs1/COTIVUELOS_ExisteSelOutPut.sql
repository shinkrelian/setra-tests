﻿CREATE Procedure COTIVUELOS_ExisteSelOutPut
@IDCAB int,
@IDDet int,
@pOk bit OutPut
As
	Set NoCount On
	If Exists(select ID from COTIVUELOS where IDCAB = @IDCAB And IdDet = @IDDet And TipoTransporte = 'T')
		Set @pOk = 1
	Else
		Set @pOk = 0
