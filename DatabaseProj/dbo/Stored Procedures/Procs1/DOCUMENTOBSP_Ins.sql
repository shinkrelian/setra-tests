﻿--JRF-20140708-Agregar los nuevos campos [FeEmision,CoPeriodo]
CREATE Procedure dbo.DOCUMENTOBSP_Ins  
 @NuDocumBSP char(11),  
 @CoTipDocBSP char(3),  
 @CoTicket char(13),  
 @FeEmision smalldatetime,
 @CoPeriodo char(9),
 @SSTotalDocum numeric(9,2),  
 @UserMod char(4)  
As  
 Set Nocount On  
   
 Insert Into DOCUMENTOBSP  
 (NuDocumBSP,CoTipDocBSP,CoTicket,FeEmision,CoPeriodo ,
 SSTotalDocum,UserMod)  
 Values  
 (@NuDocumBSP,@CoTipDocBSP,@CoTicket,@FeEmision,@CoPeriodo , 
 @SSTotalDocum,@UserMod)  
