﻿

----------------------------------------------------------------------

--JHD-20150429 - and (cp.Titulo <>'Chd.' or (cp.Titulo ='Chd.' and cp.FlAdultoINC=1))
--JDH-20150429 - 
					--declare @IDUsuarioRes char(4)=''
					--declare @Correo varchar(50)=''

					--select @IDUsuarioRes=IDUsuarioRes from coticab where idcab=@IDCab
					--select @Correo=Correo from MAUSUARIOS where IDUsuario=@IDUsuarioRes

					--set @Correo=isnull(@Correo,'')

--JHD-20150429 - Exec EnviarCorreo 'SETRA','pagos2@setours.com',@Correo,'',
CREATE Procedure dbo.EnvioCorreoEntradasMPWPAprobadas
	@IDCab int
As
	Set Nocount on
	
	Declare @vcMensaje varchar(max)=''
	
	declare @IDUsuarioRes char(4)=''
	declare @Correo varchar(50)=''

	select @IDUsuarioRes=IDUsuarioRes from coticab where idcab=@IDCab
	select @Correo=Correo from MAUSUARIOS where IDUsuario=@IDUsuarioRes

	set @Correo=isnull(@Correo,'')

	Exec CorreoHTMLEntradasMPWPOutput @IDCab, @vcMensaje output
	--pagos2@setours.com
	If @vcMensaje<>''
		Exec EnviarCorreo 'SETRA','pagos2@setours.com',@Correo,'',
		@vcMensaje,'Autorizaciones recientes de entradas a Machu/Wayna Picchu','HTML',''
