﻿
  
--HLF-20140430-Agregando columna FlPorAjustePrecio
CREATE Procedure dbo.DEBIT_MEMO_Sel_ListxIDCab    
	@IDCab int    
As    
	Set NoCount On    
	Select ROW_NUMBER() Over (Order By SUBSTRING(dm.IDDebitMemo,9,9)) As Item,    
	 SUBSTRING(dm.IDDebitMemo,0,9) As NroFile,      
	 SUBSTRING(dm.IDDebitMemo,9,9) As Correlativo,    
	 Convert(char(10),dm.Fecha,103) As Fecha,    
	 cl.RazonComercial As DescCliente,    
	 CONVERT(char(10), dm.FecVencim,103) As FecVencim,    
	 dm.SubTotal,  
	 dm.TotalIGV,      
	 dm.Total, 	 
	 dm.FlPorAjustePrecio,    
	 0 as FlFacturado   
	from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab = c.IDCAB    
	Left Join MACLIENTES cl On cl.IDCliente = c.IDCliente    
	Where dm.IDCab = @IDCab    
	Order by Item    
	
