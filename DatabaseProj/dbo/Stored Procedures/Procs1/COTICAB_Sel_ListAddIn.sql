﻿Create Procedure dbo.COTICAB_Sel_ListAddIn
@IDFile char(8),
@Cotizacion char(8),
@Titulo varchar(100),
@IDUsVentas char(4),
@Estado char(1),
@FlHistorico bit
As
Set NoCount On
select IDCAB,IDFile,Cotizacion,Titulo,uVent.Nombre+' '+IsNull(uVent.TxApellidos,'') as EjecutivoVent,
	   Case c.Estado 
		when 'P' Then 'PENDIENTE'
		when 'A' Then 'ACEPTADO'
		when 'N' Then 'NO ACEPTADO'
		when 'X' Then 'ANULADO'
		when 'E' Then 'ELIMINADO'
		when 'C' Then 'CERRADO'
	   End as DescEstado,c.IDCliente
from COTICAB c Left Join MAUSUARIOS uVent On uVent.IDUsuario=c.IDUsuario
where (Ltrim(Rtrim(@IDFile))='' Or IDFile=@IDFile)
And (Ltrim(Rtrim(@Cotizacion))='' Or Cotizacion=@Cotizacion)
And (Ltrim(Rtrim(@Titulo))='' Or Titulo Like '%'+@Titulo+'%')
And (@IDUsVentas='' Or c.IDUsuario=@IDUsVentas)
And (Ltrim(Rtrim(@Estado))='' Or Estado=@Estado)
And (FlHistorico=0 And FlHistorico=@FlHistorico)
