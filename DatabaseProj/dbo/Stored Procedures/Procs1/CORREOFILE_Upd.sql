﻿
Create Procedure dbo.CORREOFILE_Upd
@NuCorreo int,
@NoAsunto varchar(200),
@TxMensaje varchar(max),
@CoFormato char(1),
@QtAdjuntos tinyint,
@UserMod char(4)
As
	Set NoCount On
	Update [dbo].[CORREOFILE]
	   Set [NoAsunto] = @NoAsunto
		  ,[TxMensaje] = @TxMensaje
		  ,[CoFormato] = @CoFormato
		  ,[QtAdjuntos] = @QtAdjuntos
		  ,[UserMod] = @UserMod 
		  ,[FecMod] = GETDATE()
	 Where NuCorreo = @NuCorreo
