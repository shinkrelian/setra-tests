﻿Create PRocedure [dbo].[MAIDIOMASPROVEEDOR_Ins]
	@IDIdioma	varchar(12), 
	@IDProveedor	char(6) ,
	@Nivel	char(2)	,
	@UserMod	char(4) 
As
	Set NoCount On
	
	Insert Into MAIDIOMASPROVEEDOR(IDIdioma, IDProveedor, Nivel, UserMod)
	Values(@IDIdioma, @IDProveedor, @Nivel, @UserMod)
