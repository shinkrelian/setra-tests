﻿
CREATE procedure [dbo].[MACONTACTOS_CLIENTE_PROVEEDOR_UPD_Password]
  @UsuarioLogeo_Hash varchar(max)='',
  @PasswordLogeo varchar(max)='',
  @UserMod char(4)=''
 AS
BEGIN
	DECLARE @IDContacto CHAR(3)=''
	DECLARE @ID CHAR(6)=''
	DECLARE @Tipo char(1)=''
	DECLARE @Usuario varchar(80)=''
	SET NOCOUNT ON
	--1ERO PARA CLIENTES
	select @ID=IDCliente,@IDContacto=IDContacto,@Usuario=UsuarioLogeo
	from MACONTACTOSCLIENTE where 
	ltrim(rtrim(UsuarioLogeo_Hash))=ltrim(rtrim(@UsuarioLogeo_Hash))

	if RTRIM(ISNULL(@ID,'')) = ''
		begin 
			select @ID=IDProveedor,@IDContacto=IDContacto,@Usuario=Usuario 
			from MACONTACTOSPROVEEDOR where
			ltrim(rtrim(UsuarioLogeo_Hash))=ltrim(rtrim(@UsuarioLogeo_Hash))

				IF RTRIM(ISNULL(@ID,'')) <> ''
					BEGIN
							--ACTUALIZAR CONTACTOS PROVEEDOR
							SET @Tipo='P'
								UPDATE MACONTACTOSPROVEEDOR Set  
							   UsuarioLogeo_Hash=null--Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
							   ,FechaHash=null--DATEADD(HOUR,4, getdate())
							   ,Password=@PasswordLogeo
							   ,UserMod=@UserMod             
							   ,FecMod=GETDATE()
								WHERE  
								   IDContacto=@IDContacto And IDProveedor=@ID
					END
				ELSE
					BEGIN
						select @ID=IDProveedor,@Usuario=Usuario 
						from MAPROVEEDORES where
						ltrim(rtrim(UsuarioLogeo_Hash))=ltrim(rtrim(@UsuarioLogeo_Hash))
						IF RTRIM(ISNULL(@ID,'')) <> ''
						BEGIN
							--ACTUALIZAR CONTACTOS PROVEEDOR
							SET @Tipo='P'
								UPDATE MAPROVEEDORES Set  
								UsuarioLogeo_Hash=null--Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
								,FechaHash=null--DATEADD(HOUR,4, getdate())
								,Password=@PasswordLogeo
								,UserMod=@UserMod             
								,FecMod=GETDATE()
								WHERE  
									IDProveedor=@ID
						END
						ELSE
						BEGIN
							SET @TIPO=''
							SET @ID=''
							SET @IDContacto=''
							set @Usuario=''
							END
						END
		end
	else
		begin
			set @tipo='C'
				UPDATE MACONTACTOSCLIENTE Set  
			   UsuarioLogeo_Hash=null--Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
			   ,FechaHash=null--DATEADD(HOUR,4, getdate())
			   ,PasswordLogeo=@PasswordLogeo
			   ,UserMod=@UserMod             
			   ,FecMod=GETDATE()
			   --,FlAccesoWeb = 1 
				WHERE  
				   IDContacto=@IDContacto And IDCliente=@ID
		end
	SET NOCOUNT OFF

	SELECT @Tipo AS TIPO,@ID AS ID,@IDContacto AS IDContacto,@Usuario as Usuario,@PasswordLogeo as [Password]

END;
