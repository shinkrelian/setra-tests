﻿--JRF-20130821-Ordenarlo por el nuevo campo
CREATE Procedure dbo.MAINCLUIDOOPERACIONES_Sel_List  
@DescAbrev varchar(100)  
As  
 Set NoCount On  
 select FlPorDefecto,NoObserAbrev,NuIncluido,NuOrden from MAINCLUIDOOPERACIONES  
 Where (LTRIM(rtrim(@DescAbrev))='' or NoObserAbrev Like '%'+@DescAbrev+'%')  
 Order by NuOrden,NoObserAbrev
