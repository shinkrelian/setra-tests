﻿CREATE Procedure dbo.CONTROL_CALIDAD_PRO_MACUESTIONARIO_Upd
@NuControlCalidad int,
@NuCuest tinyint,
@IDProveedor char(6),
@FlExcelent bit,
@FlVeryGood bit,
@FlGood bit,
@FlAverage bit,
@FlPoor bit,
@TxComentario text,
@UserMod char(4)
As
Set NoCount On
UPDATE [dbo].[CONTROL_CALIDAD_PRO_MACUESTIONARIO]
   SET [FlExcelent] = @FlExcelent
      ,[FlVeryGood] = @FlVeryGood
      ,[FlGood] = @FlGood
      ,[FlAverage] = @FlAverage
      ,[FlPoor] = @FlPoor
      ,[TxComentario] = @TxComentario
      ,[UserMod] = @UserMod
      ,[FecMod] = GETDATE()
 WHERE [NuControlCalidad] = @NuControlCalidad and [NuCuest] = @NuCuest and [IDProveedor] = @IDProveedor
