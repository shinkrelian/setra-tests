﻿CREATE Procedure MAALMACEN_Sel_Cbo
 @bTodos bit,    
 @bDescTodos bit 
as
SELECT * FROM    
( 
Select '00000' as CoAlmacen,         
  --'' as Codigo,        
  Case When @bDescTodos = 0 then 'No Aplica'    
  else '<Todos>' End as NoTipoBien,         
          
  0 as Ord              
  Union             

select CoAlmacen,NoAlmacen, 1 as Ord   
from MAALMACEN
) as X
Where (@bTodos=1 Or Ord<>0)              
 Order by Ord,2     
