﻿--JRF-20160412-...And cp.FlNoShow = 0
CREATE Procedure dbo.COTIPAX_Sel_ListxIDCab_MailNoHotel
@IDCab int,
@IDReserva int
As
Set NoCount On
Select
 NroOrd, Apellidos, Nombres,Titulo,DescIdentidad,NumIdentidad, DescNacionalidad,                  
 FecNacimiento,Peso,Residente ,ObservEspecial,  IDIdentidad,                         
 IDNacionalidad,IDPax,ACC,CapacidadHab,EsMatrimonial,DescTC                 
 From                    
 (                         
 Select                   
 cp.Orden As NroOrd,                  
 cp.IDIdentidad,substring(di.Descripcion,1,3) as DescIdentidad, ltrim(rtrim(isnull(cp.NumIdentidad,''))) NumIdentidad,                         
 --cp.Titulo, cp.Nombres, cp.Apellidos, isnull(CONVERT(char(10),cp.FecNacimiento,103),'') As  FecNacimiento,          
 Case when ltrim(rtrim(cp.TxTituloAcademico)) <> '---' then cp.TxTituloAcademico else cp.Titulo End as Titulo, cp.Nombres, cp.Apellidos, isnull(CONVERT(char(10),cp.FecNacimiento,103),'') As  FecNacimiento,          
 cp.IDNacionalidad,isnull(u.DC,SUBSTRING(u.Descripcion,1,3)) as DescNacionalidad ,cp.IDPax ,Residente                   
 ,isnull(cp.Peso,0) As Peso,Isnull(cp.ObservEspecial,'') As ObservEspecial,                    
 Case When ISNUMERIC(SUBSTRING(Apellidos,4,3))=1 Then                      
  SUBSTRING(Apellidos,4,3)                    
 Else                    
  Nombres                    
 End as NroNombrePax,        
 '' As ACC, 0 as  CapacidadHab,0 as EsMatrimonial, Case When cp.FlTC = 1 then 'TC' Else '' End As DescTC                    
 From COTIPAX cp Left Join MATIPOIDENT di On cp.IDIdentidad=di.IDIdentidad                        
 Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo                        
 --Inner Join ACOMODOPAX_PROVEEDOR ap On cp.IDPax = ap.IDPax and ap.IDReserva = @IDReserva        
 Where cp.IDCab=@IDCab And cp.FlNoShow=0) as X                    
 Order by NroOrd
