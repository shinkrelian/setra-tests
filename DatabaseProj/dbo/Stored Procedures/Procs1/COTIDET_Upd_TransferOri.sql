﻿Create Procedure [dbo].[COTIDET_Upd_TransferOri]
	@IDDet	int,
	@IDDetTransferOri	int
As
	Set Nocount On  
	
	Update COTIDET Set IDDetTransferOri=@IDDetTransferOri
	Where IDDet=@IDDet
