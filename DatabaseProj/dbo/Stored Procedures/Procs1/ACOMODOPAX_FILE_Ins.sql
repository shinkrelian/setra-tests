﻿CREATE Procedure [Dbo].[ACOMODOPAX_FILE_Ins]  
@IDPax int,  
@CapacidadHab tinyint,  
@IDCAB int,  
@IDHabit tinyint,  
@EsMatrimonial bit,
@UserMod char(4)  
As  
 Set NoCount On  
 Insert into ACOMODOPAX_FILE (IDPax,CapacidadHab,IDCAB,IDHabit,EsMatrimonial,UserMod)  
    values(@IDPax,@CapacidadHab,@IDCAB,@IDHabit,@EsMatrimonial,@UserMod)  
