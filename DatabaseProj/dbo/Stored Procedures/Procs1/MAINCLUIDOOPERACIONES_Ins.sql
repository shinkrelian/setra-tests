﻿--JRF-20130821-Nuevos campos FlInterno,NuOrden
CREATE Procedure dbo.MAINCLUIDOOPERACIONES_Ins  
@FlInterno bit,
@NuOrden tinyint,
@NoObserAbrev varchar(100),  
@FlPorDefecto bit,  
@UserMod char(4),  
@pNuIncluido int output  
As  
 Set NoCount On  
 Declare @NuIncluido int  
 Execute dbo.Correlativo_SelOutput 'MAINCLUIDOOPERACIONES',1,10,@NuIncluido output  
   
 Insert Into MAINCLUIDOOPERACIONES 
  (NuIncluido,NoObserAbrev,FlInterno,NuOrden,FlPorDefecto,UserMod,FecMod) 
  values(@NuIncluido,@NoObserAbrev,@FlInterno,@NuOrden,@FlPorDefecto,@UserMod,GETDATE())  
 set @pNuIncluido = @NuIncluido  
