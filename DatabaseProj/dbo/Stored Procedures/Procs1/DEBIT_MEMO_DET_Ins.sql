﻿CREATE Procedure dbo.DEBIT_MEMO_DET_Ins    
 @IDDebitMemo char(10),     
 @Descripcion varchar(max),    
 @SubTotal numeric(8,2),    
 @TotalIGV numeric(8,2),    
 @Total numeric(8,2),    
 @CapacidadHab char(3),    
 @NroPax smallint,     
 @CostoPersona numeric(8,2),    
 @UserMod char(4)    
As    
 Set NoCount On    
     
 INSERT INTO [dbo].[DEBIT_MEMO_DET]    
      ([IDDebitMemo]    
      ,[IDDebitMemoDet]    
      ,[Descripcion]    
      ,[SubTotal]    
      ,[TotalIGV]    
      ,[Total]    
      ,[CapacidadHab]    
      ,[NroPax]          
      ,CostoPersona  
      ,[UserMod]    
      )    
   VALUES    
      (@IDDebitMemo    
      ,ISNull((Select Max(IDDebitMemoDet) From DEBIT_MEMO_DET     
     Where IDDebitMemo=@IDDebitMemo),0)+1    
      ,@Descripcion    
      ,@SubTotal    
      ,@TotalIGV    
      ,@Total    
      ,Case When ltrim(rtrim(@CapacidadHab)) = '' Then Null Else @CapacidadHab End    
      ,@NroPax          
      ,@CostoPersona  
      ,@UserMod)    
