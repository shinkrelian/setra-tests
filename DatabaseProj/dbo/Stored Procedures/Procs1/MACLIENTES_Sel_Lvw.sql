﻿--JRF-20140611-Agregando el Filtro de Activo='A'
CREATE Procedure [dbo].[MACLIENTES_Sel_Lvw]    
 @ID char(6),    
 @Descripcion varchar(60)    
As    
 Set NoCount On    
     
 Select RazonComercial    
 From MACLIENTES    
 Where Activo = 'A' and (RazonComercial Like '%'+@Descripcion+'%')    
 And (IDCliente <>@ID Or ltrim(rtrim(@ID))='')    
 Order by RazonComercial  
