﻿--JRF20130716-Agregando al usuario Programador.
CREATE Procedure dbo.MACONTACTOSPROVEEDOR_SelListxIDProveedor      
@IDProveedor varchar(6)      
As      
 Set NoCount On     
 select Nombre,Cargo,IDContacto,REPLACE(Replace(Correo,'/',';'),',',';') As Correo,Ord from (     
 select NombreCorto as Nombre,'Reservas' as Cargo,'E1' As IDContacto,Email3 As Correo,1 As Ord from MAPROVEEDORES where IDProveedor = @IDProveedor And Email3 is not null    
 union    
 select NombreCorto as Nombre,'Reservas' as Cargo,'E2' As IDContacto,Email4 As Correo,2 As Ord from MAPROVEEDORES where IDProveedor = @IDProveedor And Email4 is not null    
 Union    
 select NombreCorto as Nombre,'Recepción' as Cargo,'E3' As IDContacto,EmailRecepcion1 As Correo,3 As Ord  from MAPROVEEDORES where IDProveedor = @IDProveedor And EmailRecepcion1 is not null    
 union    
 select NombreCorto as Nombre,'Recepción' as Cargo,'E4' As IDContacto,EmailRecepcion2 As Correo,4 As Ord  from MAPROVEEDORES where IDProveedor = @IDProveedor And EmailRecepcion2 is not null     
 Union    
 select distinct Titulo +' '+ Nombres +' '+Apellidos As Nombre, Cargo , IDContacto , isnull(Email,'') As Correo  ,6 As Ord  from MACONTACTOSPROVEEDOR Where Email IS NOT NULL AND IDProveedor = @IDProveedor 
 Union
 select u.Nombre,'Programador' as Cargo,'E7' As IDContacto,Correo,7 as Ord from MAPROVEEDORES p Left Join MAUSUARIOS u on p.IDUsuarioProg = u.IDUsuario
 where IDProveedor = @IDProveedor and IDUsuarioProg is not null
 ) As X    
 Order by X.Ord asc    
