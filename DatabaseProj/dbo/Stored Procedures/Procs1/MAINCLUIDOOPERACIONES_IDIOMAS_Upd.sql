﻿
Create Procedure dbo.MAINCLUIDOOPERACIONES_IDIOMAS_Upd
@NuIncluido int,
@IDIdioma varchar(12),
@NoObservacion varchar(max),
@UserMod char(4)
As
	Set NoCount On
	Update MAINCLUIDOOPERACIONES_IDIOMAS
		set NoObservacion = @NoObservacion,
			UserMod = @UserMod,
			FecMod = GETDATE()
	where NuIncluido = @NuIncluido and IDIdioma = @IDIdioma
