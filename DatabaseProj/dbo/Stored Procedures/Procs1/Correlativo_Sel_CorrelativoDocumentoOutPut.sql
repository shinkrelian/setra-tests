﻿Create Procedure dbo.Correlativo_Sel_CorrelativoDocumentoOutPut
@IDTipoDoc char(3),
@CoSerie char(3),
@pCorrelativo int OutPut
as
Set NoCount On
Set @pCorrelativo = (select Correlativo from Correlativo 
					 where IDTipoDoc=@IDTipoDoc and CoSerie=@CoSerie and Tabla = 'DOCUMENTO')
Set @pCorrelativo = ISNULL(@pCorrelativo,0)
