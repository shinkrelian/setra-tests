﻿
--Execute COTICA_QUIEBRES_Sel_Copia 43
--Go

Create Procedure COTIDET_QUIEBRES_Sel_TodosxIDCab
@IDCab int
As
	Set NoCount On
	Select * from COTIDET_QUIEBRES 
	where IDDet In (select IDDet from COTIDET where IDCAB = @IDCab)
