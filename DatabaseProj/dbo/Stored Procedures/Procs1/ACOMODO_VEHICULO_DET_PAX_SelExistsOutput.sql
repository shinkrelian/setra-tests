﻿
--HLF-20150708-Nuevo campo NuNroVehiculo
CREATE Procedure dbo.ACOMODO_VEHICULO_DET_PAX_SelExistsOutput
	@NuVehiculo smallint,
	@NuNroVehiculo tinyint,
	@IDDet int,
	@NuPax int,
	@pExists bit Output
As
	Set Nocount on
	Set @pExists = 0

	If Exists(Select NuVehiculo From ACOMODO_VEHICULO_DET_PAX
		Where NuVehiculo=@NuVehiculo And IDDet=@IDDet and NuPax=@NuPax And NuNroVehiculo=@NuNroVehiculo)
		Set @pExists = 1

