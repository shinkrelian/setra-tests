﻿--HLF-20120914-Parametro @Adjuntos
--JHD-20150506-if exists(SELECT* FROM msdb.dbo.sysmail_profile where name=@Perfil ) 
CREATE Procedure [dbo].[EnviarCorreo]
	@Perfil	nvarchar(128),
	@Destinatarios	varchar(max),
	@DestinatariosCopia	varchar(max),
	@DestinatariosCopiaOculta	varchar(max),
	@Mensaje	nvarchar(max),
	@Titulo	nvarchar(255),
	@Formato	varchar(4),
	@Adjuntos	nvarchar(max)
	
As	
	if exists(SELECT* FROM msdb.dbo.sysmail_profile where name=@Perfil ) 	   
		EXEC msdb.dbo.sp_send_dbmail
		@profile_name = @Perfil,
		@recipients = @Destinatarios,
		@copy_recipients= @DestinatariosCopia,
		@blind_copy_recipients = @DestinatariosCopiaOculta,
		@body = @Mensaje,
		@body_format = @Formato,
		@subject = @Titulo,
		@file_attachments = @Adjuntos
