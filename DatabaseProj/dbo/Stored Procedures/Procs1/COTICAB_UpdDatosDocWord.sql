﻿CREATE Procedure [dbo].[COTICAB_UpdDatosDocWord]
	@IDCab	int,
	@DocWordIDIdioma	varchar(12),
	@DocWordRutaImagen	varchar(200),
	@DocWordSubTitulo	 varchar(max),
	@DocWordTexto	 varchar(max),
	@UserMod	char(4)
As
	Set NoCount On
	
	Update COTICAB Set 
	DocWordIDIdioma=@DocWordIDIdioma,
	DocWordRutaImagen=Case When ltrim(rtrim(@DocWordRutaImagen))='' Then Null Else @DocWordRutaImagen End,
	DocWordSubTitulo=Case When ltrim(rtrim(@DocWordSubTitulo ))='' Then Null Else @DocWordSubTitulo End,
	DocWordTexto=Case When ltrim(rtrim(@DocWordTexto ))='' Then Null Else @DocWordTexto End,
	FecMod=GETDATE(), UserMod=@UserMod	
	Where IDCab=@IDCab
