﻿--JHD-20160203 - Nuevos campos: ,@NuPreSob int=0, @NuDetPSo tinyint=0
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_DET_ENTRADASINC_SAP_Ins]
	
	@NuDocumProv int,
	--@NuDocumProvDet tinyint,
	@IDServicio_Det int=0,
	@TxServicio varchar(max)='',
	@SsMonto numeric(9,2)=0,
	--@QtCantidad tinyint=0,
	@QtCantidad int=0,
	@UserMod char(4)='',
	@IDCab int=0,
	@CoProducto char(6)='',
	@CoAlmacen char(6)='',
	@SsIGV numeric(8,2)=0,
	@CoTipoOC char(3)='',
	@CoCtaContab varchar(14)='',
	@CoCeCos char(6)='',
	@CoCeCon char(6)='',
	@SSPrecUni numeric(9,2)=0,
	@SSTotal numeric(9,2),

	@IDFile char(8)='',
	@Tipo varchar(10)='',
	@Monto numeric=0,
	@FechaPago smalldatetime='01/01/1900',
	@NuCodigo_ER varchar(30)='',
	@iddet int=0
	--@NuDocumProvDet tinyint=0
	,@NuPreSob int=0
	,@NuDetPSo tinyint=0
AS
BEGIN

Declare @NuDocumProvDet tinyint =(select Isnull(Max(NuDocumProvDet),0)+1 
								from DOCUMENTO_PROVEEDOR_DET where NuDocumProv=@NuDocumProv )


	Insert Into dbo.DOCUMENTO_PROVEEDOR_DET
		(
			NuDocumProv,
 			NuDocumProvDet,
 			IDServicio_Det,
 			TxServicio,
 			SsMonto,
 			QtCantidad,
 			UserMod,

			IDCab,
 			CoProducto,
 			CoAlmacen,
 			SsIGV,
 			CoTipoOC,
 			CoCtaContab,
 			CoCeCos,
 			CoCeCon,
 			SSPrecUni,
			SSTotal
 		)
	Values
		(
			@NuDocumProv,
 			@NuDocumProvDet,
 			Case When @IDServicio_Det=0 Then Null Else @IDServicio_Det End,
 			case when LTRIM(RTRIM(@TxServicio)) ='' then null else @TxServicio end,

 			Case When @SsMonto=0 Then Null Else @SsMonto End,
 			Case When @QtCantidad=0 Then Null Else @QtCantidad End,
 			@UserMod,
			Case When @IDCab=0 Then Null Else @IDCab End,
 			case when LTRIM(RTRIM(@CoProducto)) ='' then null else @CoProducto end,
 			case when LTRIM(RTRIM(@CoAlmacen)) ='' then null else @CoAlmacen end,
 			Case When @SsIGV=0 Then Null Else @SsIGV End,

 			case when LTRIM(RTRIM(@CoTipoOC)) ='' then null else @CoTipoOC end,
 			case when LTRIM(RTRIM(@CoCtaContab)) ='' then null else @CoCtaContab end,
 			case when LTRIM(RTRIM(@CoCeCos)) ='' then null else @CoCeCos end,
 			case when LTRIM(RTRIM(@CoCeCon)) ='' then null else @CoCeCon end,

 			Case When @SSPrecUni=0 Then Null Else @SSPrecUni End,
			Case When @SSTotal=0 Then Null Else @SSTotal End
 		)

	Insert Into [dbo].[ENTRADASINC_SAP]
		(
			[IDCab],
 			[IDFile],
 			[Tipo],
 			[Monto],
 			[FechaPago],
 			[NuCodigo_ER],
 			[NuDocumProv],
 			[FlActivo],
 			[UserMod],
 			[FecMod],
			NuDocumProvDet,
			iddet
			,NuPreSob 
			,NuDetPSo

 		)
	Values
		(
			Case When @IDCab=0 Then Null Else @IDCab End,
			@IDFile,
			case when LTRIM(RTRIM(@Tipo)) ='' then null else @Tipo end,
			@Monto,
			case when @FechaPago ='01/01/1900' then null else @FechaPago end,
			case when LTRIM(RTRIM(@NuCodigo_ER)) ='' then null else @NuCodigo_ER end,
			@NuDocumProv,
			1,
			@UserMod,
			GETDATE(),
			@NuDocumProvDet,
			Case When @iddet=0 Then Null Else @iddet End
			,Case When @NuPreSob=0 Then Null Else @NuPreSob End
			,Case When @NuDetPSo=0 Then Null Else @NuDetPSo End
 		)
END;
