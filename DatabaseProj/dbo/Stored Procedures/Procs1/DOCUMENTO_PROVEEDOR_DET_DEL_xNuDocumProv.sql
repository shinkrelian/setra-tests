﻿
------------------------------------------------------

CREATE PROCEDURE [DOCUMENTO_PROVEEDOR_DET_DEL_xNuDocumProv]
	@NuDocumProv int
AS
BEGIN
	Delete [dbo].[DOCUMENTO_PROVEEDOR_DET]
	Where 
		[NuDocumProv] = @NuDocumProv 
END;
