﻿--HLF-20140415-Where idm.SsPago>0    
--HLF-20140415-And datediff(m,asg.FeAsignacion,cc.FechaOut)>=1    
--HLF-20140415-idm.SsPago + idm.SsGastosTransferencia as SsPago    
--HLF-20140425- ifz.FeAcreditada, Inner Join INGRESO_FINANZAS ifz On idm.NuIngreso=ifz.NuIngreso    
--HLF-20140527-Agregando columna cc.CoTipoVenta, cc.IDCliente
--HLF-20140530-Cambiando Where idm.SsPago>0  por Where idm.SsPago<>0    
CREATE Procedure dbo.ASIGNACION_SelAnticiposxNuAsignacion      
 @NuAsignacion int       
As        
 Set Nocount On        
      
 SELECT CC.IDCAB, --idm.SsPago     
 idm.SsPago + idm.SsGastosTransferencia as SsPago,  
 ifz.FeAcreditada,
 cc.CoTipoVenta, cc.IDCliente        
 FROM ASIGNACION asg         
 Inner Join INGRESO_DEBIT_MEMO idm On asg.NuAsignacion=idm.NuAsignacion       
 Inner Join INGRESO_FINANZAS ifz On idm.NuIngreso=ifz.NuIngreso  
 And asg.NuAsignacion=@NuAsignacion       
 Inner Join DEBIT_MEMO dm On idm.IDDebitMemo=dm.IDDebitMemo       
 Inner Join COTICAB cc On dm.IDCab=cc.IDCAB     
 --And asg.FeAsignacion < cc.FechaOut        
 And datediff(m,asg.FeAsignacion,cc.FechaOut)>=1    
  And cc.EstadoFacturac='P'      
 Where idm.SsPago<>0    
   
   