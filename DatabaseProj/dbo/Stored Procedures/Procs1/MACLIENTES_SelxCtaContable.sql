﻿--HLF-20141118-IF LEFT(@CtaContable,2) ='75'    
--JHD-20150603-Se amplio el campo @CtaContable a char(14)  
--JRF-20151012-..Declare @CotipoVenta varchar(4)= (Select CoTipoVenta from ...
CREATE PROC [dbo].[MACLIENTES_SelxCtaContable]   
 @IDCliente char(6),    
 @CtaContable char(14),    
 @IDCAB INT    
    
AS         
Set NoCount On  
 Declare @CtaContableX char(14)  
 Declare @CoCtroCostoX char(6)  
 Declare @CotipoVenta varchar(4)= (Select CoTipoVenta from COTICAB where IDCAB=@IDCAB)
    
 --IF (isnull((Select CoCtroCosto From MAPLANCUENTAS Where CtaContable = @CtaContable ),'') = '')  
 --IF @CtaContable ='750000'   
 IF LEFT(@CtaContable,2) ='75'   
  BEGIN
   IF (isnull((Select CoCtroCosto From MAPLANCUENTAS Where CtaContable =  
      (Select  NuCuentaComision From MACLIENTES Where IDCliente =  @IDCliente)),'')='' )  
    BEGIN   
     SET @CtaContableX= (Select NuCuentaComision From MACLIENTES Where IDCliente =   
            (Select IDCliente From COTICAB Where IDCAB  =@IDCAB))   
     SET @CoCtroCostoX= (Select CoCtroCosto From MAPLANCUENTAS Where CtaContable =@CtaContableX )    
    END       
   ELSE  
    BEGIN  
     SET @CtaContableX=(Select NuCuentaComision From MACLIENTES Where IDCliente =  @IDCliente)  
     SET @CoCtroCostoX=(Select CoCtroCosto From MAPLANCUENTAS Where CtaContable =@CtaContableX)   
    END  
  END  
 ELSE  
  BEGIN  
   SET @CtaContableX= @CtaContable  
   SET @CoCtroCostoX= IsNull((Select CoCtroCosto From MAPLANCUENTAS Where CtaContable = @CtaContable),'')
  END  

 Set @CoCtroCostoX = Case When @CotipoVenta='RC' And @CoCtroCostoX='' Then '900101' else @CoCtroCostoX End
 SELECT @CoCtroCostoX as 'CoCtroCosto' ,@CtaContableX as 'CtaContable'  

