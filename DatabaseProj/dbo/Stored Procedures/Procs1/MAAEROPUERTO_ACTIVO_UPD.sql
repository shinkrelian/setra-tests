﻿

--------------------------------------------------------------------------------------------

CREATE PROCEDURE MAAEROPUERTO_ACTIVO_UPD

		    @CoAero char(3),
		   @FlActivo BIT,
           @UserMod char(4)
AS
BEGIN
UPDATE [MAAEROPUERTO]
   SET 
      FlActivo =@FlActivo
    
      ,[UserMod] = @UserMod
      ,[FecMod] =GETDATE()
      
      
 WHERE CoAero = @CoAero
 END;
