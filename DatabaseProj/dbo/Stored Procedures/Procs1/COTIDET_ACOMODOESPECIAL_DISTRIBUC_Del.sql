﻿
CREATE PROCEDURE dbo.COTIDET_ACOMODOESPECIAL_DISTRIBUC_Del
	@IDDet int ,
	@CoCapacidad char(2),	
	@IDPax int
As
	Set Nocount On
	
	Delete From COTIDET_ACOMODOESPECIAL_DISTRIBUC
	Where IDDet=@IDDet And
	CoCapacidad=@CoCapacidad And	
	IDPax=@IDPax 
	
