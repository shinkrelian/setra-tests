﻿Create Procedure [dbo].[MABANCOS_Sel_Pk]
	@IDBanco char(3)
As
	Set NoCount On
	
	Select * From MABANCOS 
	Where IDBanco = @IDBanco
