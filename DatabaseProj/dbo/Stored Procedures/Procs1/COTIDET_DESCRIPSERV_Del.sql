﻿Create Procedure [dbo].[COTIDET_DESCRIPSERV_Del]
	@IDDet	int,
	@IDIdioma	varchar(12)
As

	Set NoCount On
	
	Delete From COTIDET_DESCRIPSERV
	Where IDDet=@IDDet And IDIdioma=@IDIdioma
