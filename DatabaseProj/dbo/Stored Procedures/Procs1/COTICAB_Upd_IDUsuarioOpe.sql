﻿--JRF-20140519-Actualizar el campo de FechaAsigOperaciones  
--HLF-20141015-Case when LTRIM(rtrim(@IDUsuarioOpe))='0000' then Null
--JHD-20150323-Se agregó nuevo parametro: @IDUsuarioOpe_Cusco char(4)
--HLF-20150701-If ltrim(rtrim(@IDUsuarioOpe))='' Set @IDUsuarioOpe='0000'
CREATE Procedure dbo.COTICAB_Upd_IDUsuarioOpe    
 @IDCab int,    
 @IDUsuarioOpe char(4),    
 @UserMod char(4),
 @IDUsuarioOpe_Cusco char(4)=''
As    
 Set NoCount On    
     
 If ltrim(rtrim(@IDUsuarioOpe))='' Set @IDUsuarioOpe='0000'
 Update COTICAB Set IDUsuarioOpe=@IDUsuarioOpe, FecMod=GETDATE(),  
    FechaAsigOperaciones = 
    Case when LTRIM(rtrim(@IDUsuarioOpe))='0000' then 
		Null
	Else	
		Case when COTICAB.FechaAsigOperaciones is null then 
			GETDATE() 
		Else 
			COTICAB.FechaAsigOperaciones 
		End
	End,  
    UserMod=@UserMod    ,
    IDUsuarioOpe_Cusco=case when LTRIM(RTRIM(@IDUsuarioOpe_Cusco))='' then Null Else @IDUsuarioOpe_Cusco End
 Where IDCAB=@IDCab    
 
