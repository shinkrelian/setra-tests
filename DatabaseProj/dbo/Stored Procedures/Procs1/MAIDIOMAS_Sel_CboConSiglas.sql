﻿
--HLF-20130702-Agregando NO APLICA
CREATE Procedure [dbo].[MAIDIOMAS_Sel_CboConSiglas]     
As    
	Set NoCount On  

	SELECT * FROM
	( 
	Select IDidioma,'----                                                                |'+ Siglas as DescIdioma 
	From MAIDIOMAS    
	Where IDidioma='NO APLICA'
	Union
	Select IDidioma,IDidioma+ '                                                                |'+ Siglas as DescIdioma 
	From MAIDIOMAS    
	Where Activo='A'    
	) AS X
	ORDER BY DescIdioma 