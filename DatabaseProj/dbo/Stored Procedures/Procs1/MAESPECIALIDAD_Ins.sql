﻿
Create Procedure [Dbo].[MAESPECIALIDAD_Ins]
@Descripcion varchar(150),
@UserMod char(4),
@pIDEspecialidad char(2) Output
As
	Set NoCount On
	Declare @IDEspecialidad char(2) 
	Execute Correlativo_SelOutput 'MAESPECIALIDAD',1,2,@IDEspecialidad output
	
	Insert Into MAESPECIALIDAD(IDEspecialidad,Descripcion,UserMod) 
			Values (@IDEspecialidad,@Descripcion,@UserMod)
	Set @pIDEspecialidad = @IDEspecialidad
