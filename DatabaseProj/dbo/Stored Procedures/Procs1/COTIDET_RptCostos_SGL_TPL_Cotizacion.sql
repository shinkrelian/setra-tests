﻿--JRF-20130423-Orden x Pax     
--JRF-20131017-Correción de la cabecera principal(Titulo - T).  
--JRF-20140808-Agregar el Tipo de Cambio de la tabla COTICAB_TIPOSCAMBIO  
--JRF-20150310-Considerar el triple en caso que halla editado el servicio
CREATE Procedure [dbo].[COTIDET_RptCostos_SGL_TPL_Cotizacion]            
@IDCab int            
As             
 Set NoCount On                    
    Declare @IDDet varchar(30) ,            
 @Dia smalldatetime ,            
 @Item numeric(6, 3) ,            
 @Cotizacion varchar(12) ,            
 @IDTipoProv varchar(15) ,            
 @Cliente varchar(60) ,            
 @Titulo varchar(max) ,            
 @Responsable varchar(60) ,            
 @TipoCambio varchar(30) ,            
 @TipoPax varchar(15) ,            
 @FechaInicio varchar(25) ,            
 @FechaFinal varchar(25) ,            
 @Fecha varchar(25) ,            
 @Ubigeo varchar(60) ,            
 @Proveedor varchar(100) ,            
 @Servicio varchar(max) ,            
 @Tipo char(5) ,            
 @Anio char(4) ,            
 @Var char(8) ,            
 @Idioma varchar(12) ,            
 @PlanAlimenticio char(1),            
 @ContienteTPL Char(1),            
 @ConAlojamiento char(1),          
             
 @NroPax varchar(10),            
 @NroLiberados varchar(10),            
 @MargenAplicado varchar(15) ,            
 @CostoTotalCal varchar(15) ,            
 @CostoTotalDet varchar(15) ,            
 @CostoNeto varchar(15) ,            
 @Impuesto varchar(15) ,            
 @CostoLiberado varchar(15) ,            
 @SSCR varchar(15) ,            
 @SSTotal varchar(15) ,            
 @SSMargen varchar(15) ,            
 @STCR varchar(15) ,            
 @STTotal varchar(15) ,            
 @STMargen varchar(15) ,            
             
 @NroPaxQuie varchar(10),            
 @NroLiberadosQuie varchar(10),            
 @MargenAplicadoQuie varchar(15) ,            
 @CostoTotalCalQuie varchar(15) ,            
 @CostoTotalDetQuie varchar(15) ,            
 @CostoNetoQuie varchar(15) ,            
 @ImpuestoQuie varchar(15) ,            
 @CostoLiberadoQuie varchar(15) ,            
 @SSCRQuie varchar(15) ,            
 @SSTotalQuie varchar(15) ,            
 @SSMargenQuie varchar(15) ,            
 @STCRQuie varchar(15) ,            
 @STTotalQuie varchar(15) ,            
 @STMargenQuie varchar(15) ,            
             
 @Notas varchar(max),            
    @Cont tinyint=1,@IDDetTmp int                    
                        
 Delete from RptCtInterna_SGL_TPL                      
 declare curCtInterna2 cursor for                      
                     
 SELECT  CD.IDDet,CD.Dia,CD.Item,Cc.Cotizacion,pr.IDTipoProv,c.RazonComercial,cc.Titulo,u.Nombre,  
 --isnull(cc.TipoCambio,0) As TipoCambio  
 isnull((select SsTipCam from COTICAB_TIPOSCAMBIO ctc where ctc.IDCab = cc.IDCab and CoMoneda = 'SOL'),0) As TipoCambio  
 ,cc.TipoPax,convert(varchar(10),cc.FecInicio,103) as FechaInicial , Convert(varchar(10),cc.FechaOut,103) as FechaFinal                       
   ,Upper(substring(datename(dw,CD.Dia),1,3))+' '+ Convert(varchar(60),CD.Dia,103) as Fecha,                     
   Case When CD.NroLiberados is null then cc.NroLiberados else CD.NroLiberados End As NroLiberados,                    
   ub.Descripcion as Ubigeo,Pr.NombreCorto As Proveedor,                    
   CD.Servicio + Case When CD.IncGuia = 1 then ' - Guide' else '' End As Servicio ,                    
   Case When S.IDTipoServ = 'NAP' Then '---' Else S.IDTipoServ End As Tipo                    
   ,Sd.Anio, Sd.Tipo As [Var],Idi.Siglas As Idioma,sd.PlanAlimenticio,            
   Case When ((sd.IDServicio_DetxTriple is null And sd.Monto_tri is null And            
     sd.Monto_tris is null And sd.IdHabitTriple = '000') And cd.FlSTCREditado=0) Then 0 Else 1 End As ContieneTPL           
     ,
  Case When pr.IDTipoProv = '003' and sd.ConAlojamiento = 1 then          
 Case When sd.ConAlojamiento = 1 then         
  Case When isnull(cd.IDDetRel,0)= 0 then 1 else 0 End        
 else        
  0 End        
  else          
  Case When pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then        
  sd.ConAlojamiento        
 else 0 End        
  End As ConAlojamiento          
           
   --sd.IDServicio_DetxTriple , sd.Monto_tri ,            
   -- sd.Monto_tris  , sd.IdHabitTriple          
   ,Case When CD.IncGuia = 1 then cc.NroPax else cd.nropax End As nropax,                    
    CABQ.Pax ,CABQ.Liberados ,               
   isnull(CD.MargenAplicado,0) as MargenAplicado,                    
   isnull(CD.CostoReal + CD.CostoLiberado + CD.TotImpto,0)As CostoCal,                    
   isnull(CD.Total,0) TotalDet,                    
   isnull(CD.CostoReal,0) CostoNeto,                    
   isnull(CD.TotImpto,0) TotImpto,                    
   isnull(CD.CostoLiberado,0) CostoLiberado,                    
   CD.SSCR ,            
   CD.SSTotal,            
   CD.SSMargen ,              
   CD.STCR,            
   CD.STTotal,            
   CD.STMargen             
   ,cast(cc.Notas as Varchar) as Notas                      
   FROM COTIDET_QUIEBRES CQ Right Join                       
  (select top (2)* from COTICAB_QUIEBRES where IDCAB=@IDCab Order By Pax) CABQ ON CQ.IDCAB_Q=CABQ.IDCAB_Q                      
    Right Join COTIDET CD ON CQ.IDDET=CD.IDDET Left Join MAUBIGEO ub On cd.IDUbigeo=ub.IDUbigeo                       
    Left Join COTICAB cc  On CD.IDCAB=cc.IDCAB Left Join MAUSUARIOS u On cc.IDUsuario=u.IDUsuario                      
    Left Join MACLIENTES c On cc.IDCLiente= c.IDCliente Left Join MAPROVEEDORES pr On CD.IDProveedor=pr.IDProveedor                      
    Left Join MAIDIOMAS Idi On CD.IDIdioma =  Idi.IDIdioma                    
    Left Join MASERVICIOS S On CD.IDServicio = S.IDServicio                    
    Left Join MASERVICIOS_DET Sd On CD.IDServicio_Det = Sd.IDServicio_Det                    
   WHERE CD.IDDet In (SELECT IDDET FROM COTIDET WHERE IDCAB=@IDCab)                              
   ORDER BY cd.Item,Pax                   
                         
  Open curCtInterna2                      
  Fetch Next From curCtInterna2 Into @Iddet,@Dia ,@Item ,@Cotizacion,@IDTipoProv,@Cliente ,@Titulo ,@Responsable             
   ,@TipoCambio,@TipoPax ,@FechaInicio ,@FechaFinal ,@Fecha ,@NroLiberados ,@Ubigeo ,@Proveedor,@Servicio,@Tipo             
   ,@Anio ,@Var ,@Idioma, @PlanAlimenticio, @ContienteTPL,@ConAlojamiento ,@NroPax, @NroPaxQuie ,@NroLiberadosQuie             
   ,@MargenAplicado,@CostoTotalCal ,@CostoTotalDet,@CostoNeto,@Impuesto,@CostoLiberado,            
    @SSCR,@SSTotal,@SSMargen,@STCR,@STTotal,@STMargen,@Notas                     
                     
 While  @@fetch_status = 0                      
 Begin                     
  SELECT  @MargenAplicadoQuie = COTQ.MargenAplicado,            
    @CostoTotalCalQuie =  (COTQ.CostoReal + COTQ.TotImpto + COTQ.CostoLiberado),            
    @CostoTotalDetQuie = COTQ.Total,            
    @CostoNetoQuie = COTQ.CostoReal,            
    @ImpuestoQuie = COTQ.TotImpto,            
    @CostoLiberadoQuie = COTQ.CostoLiberado,            
    @SSCRQuie = COTQ.SSCR,            
    @SSTotalQuie = COTQ.SSTotal,            
    @SSMargenQuie = COTQ.SSMargen,            
    @STCRQuie = COTQ.STCR,            
    @STTotalQuie = COTQ.STTotal,            
    @STMargenQuie = COTQ.STMargen            
  FROM COTIDET_QUIEBRES COTQ LEFT JOIN COTICAB_QUIEBRES CABQ ON COTQ.IDCAB_Q=CABQ.IDCAB_Q                      
  WHERE COTQ.IDDet=@IDDet AND CABQ.Pax= @NroPaxQuie                      
              
  --print cast(@Cont as char(9)) +' -  ' + cast(@NroPaxQuie as char(9))            
  If @Cont = 1                     
   Begin                    
    If Not Exists(select IDDet From RptCtInterna_SGL_TPL where FilTitulo='T')                      
     Begin                       
     insert into RptCtInterna_SGL_TPL ([IDDet],[Dia],[Item],[Cotizacion],[IDTipoProv],[Cliente],[Titulo],[Responsable]            
      ,[TipoCambio],[TipoPax],[FechaInicio],[FechaFinal],[Fecha],[Ubigeo],[Proveedor],[Servicio]            
      ,[Tipo],[Anio],[Var],[Idioma],[PlanAlimenticio],[ContienteTPL],[ConAlojamiento],[NroPax],[NroLiberados],[MargenAplicado],[CostoTotalCal],[CostoTotalDet]            
      ,[CostoNeto],[Impuesto],[CostoLiberado],[SSCR],[SSTotal],[SSMargen],[STCR],[STTotal],[STMargen]            
      ,[FilTitulo],[Notas])                      
      values                      
     ('IDDet','01/01/1900','0.000','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable            
     ,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal,'Fecha','Ubigeo','Proveedor','Descripción'            
     ,'Tipo','Año','Var','Idi.','','','','','','',(select cast(NroPax as varchar(5)) from coticab where IDCab = @IDCab) + ISNULL(' + ' + (select case when NroLiberados is null then null else CAST(NroLiberados as varchar(5)) End as NroL from coticab where 
 
 IDCab = @IDCab),''),''            
     ,'','','','','','','','',''            
     ,'T','Notas')                      
     End                  
                    
    If Not Exists (select IDDet from RptCtInterna_SGL_TPL where FilTitulo='S')                      
     Begin                      
  insert into RptCtInterna_SGL_TPL ([IDDet],[Dia],[Item],[Cotizacion],[IDTipoProv],[Cliente],[Titulo],[Responsable]            
      ,[TipoCambio],[TipoPax],[FechaInicio],[FechaFinal],[Fecha],[Ubigeo],[Proveedor],[Servicio]            
      ,[Tipo],[Anio],[Var],[Idioma],[PlanAlimenticio],[ContienteTPL],[ConAlojamiento],[NroPax],[NroLiberados],[MargenAplicado],[CostoTotalCal],[CostoTotalDet]            
      ,[CostoNeto],[Impuesto],[CostoLiberado],[SSCR],[SSTotal],[SSMargen],[STCR],[STTotal],[STMargen]            
      ,[FilTitulo],[Notas])                      
      values                      
     ('IDDet','01/01/1900','0.000','Cotización','IDTipoProv',@Cliente,@Titulo,@Responsable            
     ,@TipoCambio,@TipoPax,@FechaInicio,@FechaFinal,'Fecha','Ubigeo','Proveedor','Descripción'            
     ,'Tipo','Año','Var','Idi.','','','','Nro. Pax','Nro. Lib.','%','Costo','Venta'            
     ,'CostoNeto','Impuestos','CostoLib.','Costo','Venta','SSMargen','Costo','Venta','STMargen'            
     ,'S','Notas')               
     End                       
                
     insert into RptCtInterna_SGL_TPL                     
     values (@IDDet,@dia,@Item,@Cotizacion,@IDTipoProv,@Cliente,@Titulo,@Responsable,@TipoCambio,@TipoPax                      
     ,@FechaInicio,@FechaFinal,@fecha,@Ubigeo,@Proveedor,@servicio,@Tipo,@Anio,@Var,@Idioma,@PlanAlimenticio,@ContienteTPL,@ConAlojamiento                     
     ,@NroPax,@NroLiberados,@MargenAplicado,@CostoTotalCal,@CostoTotalDet,@CostoNeto,@Impuesto,@CostoLiberado,@SSCR,@SSTotal,@SSMargen,@STCR,@STTotal,@STMargen            
     ,'','','','','','','','','','','','','',''            
     ,'','','','','','','','','','','','','',''            
     ,'R',@Notas)                     
    End            
                   
  If @Cont = 1                     
   Begin                     
              
    Update RptCtInterna_SGL_TPL  set CostoTotalCalC1=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC1 is null                      
    if (@NroPaxQuie is not null)                      
        Begin                       
       update RptCtInterna_SGL_TPL             
         set NroPaxC1 ='NroPax',NroLiberadosC1='Pax Lib.',MargenAplicadoC1='%',            
    CostoTotalCalC1='Costo', CostoTotalDetC1='Venta',CostoNetoC1= 'Neto',            
    ImpuestoC1 ='Impuestos', CostoLiberadoC1 = 'Liberado',SSCRC1='Costo',            
    SSTotalC1='Venta',SSMargenC1='SSMargen',STCRC1='Costo',STTotalC1='Venta',STMargenC1='STMargen'            
    where FilTitulo='S' and CostoTotalCalC1 is null                      
        End                      
     Update RptCtInterna_SGL_TPL              
   set NroPaxC1 =@NroPaxQuie,NroLiberadosC1=@NroLiberadosQuie,MargenAplicadoC1=@MargenAplicadoQuie            
      ,CostoTotalCalC1=@CostoTotalCalQuie ,CostoTotalDetC1=@CostoTotalDetQuie               
      ,CostoNetoC1=@CostoNetoQuie ,ImpuestoC1 =@ImpuestoQuie,CostoLiberadoC1 = @CostoLiberadoQuie               
      ,SSCRC1= @SSCRQuie,SSMargenC1=@SSMargenQuie,SSTotalC1=@SSTotalQuie,STCRC1=@STCRQuie,STTotalC1=@STTotalQuie,STMargenC1=@STMargenQuie,            
   servicio=@servicio                      
     where FilTitulo='R' and IDDet=@IDDet                       
   End                    
  If @Cont = 2                   
   Begin                  
    Update RptCtInterna_SGL_TPL  set CostoTotalCalC2=@NroPaxQuie + isnull( ' + ' +Cast(@NroLiberadosQuie As varchar(5)),'') where FilTitulo='T' and CostoTotalCalC2 is null                      
    if (@NroPaxQuie is not null)                      
        Begin                       
       update RptCtInterna_SGL_TPL             
         set NroPaxC2 ='NroPax',NroLiberadosC2='Pax Lib.',MargenAplicadoC2='%',            
    CostoTotalCalC2='Costo', CostoTotalDetC2='Venta',CostoNetoC2= 'Neto',            
    ImpuestoC2 ='Impuestos', CostoLiberadoC2 = 'Liberado',SSCRC2='Costo',            
    SSTotalC2='Venta',SSMargenC2='SSMargen',STCRC2='Costo',STTotalC2='Venta',STMargenC2='STMargen'            
    where FilTitulo='S' and CostoTotalCalC2 is null                      
        End                      
     Update RptCtInterna_SGL_TPL              
   set NroPaxC2 =@NroPaxQuie,NroLiberadosC2=@NroLiberadosQuie,MargenAplicadoC2=@MargenAplicadoQuie            
   ,CostoTotalCalC2=@CostoTotalCalQuie ,CostoTotalDetC2=@CostoTotalDetQuie               
      ,CostoNetoC2=@CostoNetoQuie ,ImpuestoC2 =@ImpuestoQuie,CostoLiberadoC2 = @CostoLiberadoQuie               
      ,SSCRC2= @SSCRQuie,SSMargenC2=@SSMargenQuie,SSTotalC2=@SSTotalQuie,STCRC2=@STCRQuie,STTotalC2=@STTotalQuie,STMargenC2=@STMargenQuie,            
   servicio=@servicio                     
     where FilTitulo='R' and IDDet=@IDDet                      
   End                  
                       
  Set @IDDetTmp=@IDDet                      
  Fetch Next From curCtInterna2 Into @Iddet,@Dia ,@Item ,@Cotizacion,@IDTipoProv,@Cliente ,@Titulo ,@Responsable             
   ,@TipoCambio,@TipoPax ,@FechaInicio ,@FechaFinal ,@Fecha ,@NroLiberados ,@Ubigeo ,@Proveedor,@Servicio,@Tipo             
   ,@Anio ,@Var ,@Idioma, @PlanAlimenticio, @ContienteTPL,@ConAlojamiento ,@NroPax, @NroPaxQuie ,@NroLiberadosQuie             
   ,@MargenAplicado,@CostoTotalCal ,@CostoTotalDet,@CostoNeto,@Impuesto,@CostoLiberado,            
    @SSCR,@SSTotal,@SSMargen,@STCR,@STTotal,@STMargen,@Notas                             
                       
  If @IDDet=@IDDetTmp                          
    Set @Cont=@Cont+1                          
  Else                          
    Set @Cont=1                      
 End                    
                            
 Close curCtInterna2                      
 Deallocate curCtInterna2                      
 Select * From RptCtInterna_SGL_TPL Order by FilTitulo Desc ,Dia ASC,Item ASC             
