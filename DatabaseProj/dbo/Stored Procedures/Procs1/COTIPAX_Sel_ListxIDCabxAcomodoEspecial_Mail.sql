﻿Create Procedure COTIPAX_Sel_ListxIDCabxAcomodoEspecial_Mail
@IDDet int
As
Set NoCount On
SELECT cad.IDDet,cp.Orden as NroOrd,cp.Apellidos,cp.Nombres,cp.Titulo,SUBSTRING(ti.Descripcion,1,3) as DescIdentidad,IsNull(cp.NumIdentidad,'') as NumIdentidad,
ub.Codigo as DescNacionalidad,Case When cp.FecNacimiento is null Then '' Else Convert(varchar(10),cp.FecNacimiento,103) End as FecNacimiento,
IsNull(cp.Peso,0) as Peso,cp.Residente,cp.IDIdentidad,cp.IDNacionalidad,cad.IDPax,
Case When ch.QtCapacidad=2 and ch.FlMatrimonial=1 Then 'MAT' Else
  Case ch.QtCapacidad
    When 1 then 'SGL'
	When 2 then 'TWIN'
	When 3 then 'TRPL'
	When 4 then 'CUAD'
	When 5 then 'QUIN'
	When 6 then 'SEXT'
	When 7 then 'SEPT'
	When 8 then 'OCTU'
  End 
 End + ' '+(CAST(cad.IDHabit as varchar(2))) as ACC,ch.QtCapacidad as CapacidadHab,ch.FlMatrimonial As EsMatrimonial,
 Case When cp.FlTC=1 then 'TC' else '' End as DescTC,IsNull(cp.ObservEspecial,'') as ObservEspecial
FROM COTIDET_ACOMODOESPECIAL_DISTRIBUC cad Left Join COTIPAX cp On cad.IDPax=cp.IDPax
Left Join MATIPOIDENT ti On cp.IDIdentidad=ti.IDIdentidad Left Join MAUBIGEO ub On cp.IDNacionalidad = ub.IDubigeo 
Left Join CAPACIDAD_HABITAC ch On cad.CoCapacidad=ch.CoCapacidad
Where cad.IDDet=@IDDet
Order By cad.IDHabit,cp.Orden
