﻿CREATE Procedure [dbo].[MAADMINISTCLIENTE_Ins]
	@IDAdminist char(2),
	@IDCliente char(6),
	@Banco varchar(30),
	@CtaCte varchar(30),
	@IDMoneda char(3),
	@SWIFT varchar(30),
	@IVAN varchar(30),
	@NombresTitular varchar(80),
	@ApellidosTitular varchar(80),
	@ReferenciaAdminist text,
	@CtaInter varchar(30),
	@BancoInterm varchar(30),
	@UserMod char(4)

As
	Set NoCount On
	--Declare @IDAdminist char(2), @tiIDAdminist tinyint
	
	--Select @tiIDAdminist = IsNull(MAX(IDAdminist),0)+1 From MAADMINISTCLIENTE Where IDCliente=@IDCliente
	--Set @IDAdminist=REPLICATE('0',2)
	
	--Set @IDAdminist=@tiIDAdminist
	--Set @IDAdminist=REPLICATE('0',2-LEN(@IDAdminist))+@IDAdminist
	
	INSERT INTO MAADMINISTCLIENTE
           ([IDAdminist]
           ,[IDCliente]
           ,[Banco]
           ,[CtaCte]
           ,[IDMoneda]
           ,[SWIFT]
           ,[IVAN]
           ,[NombresTitular]
           ,[ApellidosTitular]
           ,[ReferenciaAdminist]
           ,[CtaInter]
           ,[BancoInterm]
           ,[UserMod]
           )
     VALUES
           (@IDAdminist ,
            @IDCliente ,
            Case When RTRIM(ltrim(@Banco))= '' Then Null Else @Banco End,
            Case When RTRIM(ltrim(@CtaCte ))= '' Then Null Else @CtaCte End,
            @IDMoneda,
            Case When RTRIM(ltrim(@SWIFT))= '' Then Null Else @SWIFT End,
            Case When RTRIM(ltrim(@IVAN))= '' Then Null Else @IVAN End,
            Case When RTRIM(ltrim(@NombresTitular))= '' Then Null Else @NombresTitular End,
            Case When RTRIM(ltrim(@ApellidosTitular))= '' Then Null Else @ApellidosTitular End,
            Case When RTRIM(ltrim(Cast (@ReferenciaAdminist as varchar(max))))= '' Then Null Else @ReferenciaAdminist End,
            Case When RTRIM(ltrim(@CtaInter))= '' Then Null Else @CtaInter End,
            Case When RTRIM(ltrim(@BancoInterm))= '' Then Null Else @BancoInterm End,
            @UserMod)
