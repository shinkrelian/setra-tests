﻿--JRF-20160418-...if Not Exists(Select IDDet from COTIDET_ACOMO...
CREATE PROCEDURE dbo.COTIDET_ACOMODOESPECIAL_DISTRIBUC_Ins
	@IDDet int ,
	@CoCapacidad char(2),	
	@IDPax int,	
	@IDHabit tinyint,
	@UserMod char(4)
As
	Set Nocount On
	
	if Not Exists(Select IDDet from COTIDET_ACOMODOESPECIAL_DISTRIBUC Where IDDet=@IDDet And CoCapacidad=@CoCapacidad And IDPax=@IDPax)
	Begin
		Insert Into COTIDET_ACOMODOESPECIAL_DISTRIBUC
		(IDDet,
		CoCapacidad,	
		IDPax,	
		IDHabit,
		UserMod
		)
		Values
		(@IDDet,
		@CoCapacidad,	
		@IDPax,	
		@IDHabit,
		@UserMod)
	End
