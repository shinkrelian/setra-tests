﻿Create Procedure dbo.Correlativo_Sel_ListxTabla
@Tabla varchar(30)
As
Set NoCount On
select Tabla,td.Descripcion as Documento,Correlativo,c.IDTipoDoc, CoSerie,
sr.Descripcion +' - '+sr.IDSerie  as DescripcionSerie
from Correlativo c Left Join MATIPODOC td On c.IDTipoDoc = td.IDtipodoc
Left Join MASERIES sr On c.CoSerie = sr.IDSerie
where Tabla = @Tabla
Order by c.IDTipoDoc
