﻿--JHD-20150811- se coloco 'abs' a los montos para mostrar positivos en caso sea nota de debito o credito
--JHD-20150813-CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),'00000000'),
--JHD-20150910-Se cambio la condicion para TaxCode
--JHD-20150914-TaxTotal= case when d.CoTipoDoc in ('001','012','002','003') then abs(isnull(d.SsIGV,0)) else 0 end,
			--WTLiable=case when ((d.CoTipoDoc='RH' AND d.CoTipoOC='007') or  isnull(SsDetraccion,0)>0) then 1 else 0 end,
--JHD-20150914-Se modifico el SP para mostrar el neto y su conversion segun la moneda
--JHD-20150915-Redondear montos a 2 decimales
--JHD-20150915-WTLiable=case when ((d.CoTipoDoc in ('RH','HTD') AND d.CoTipoOC in ('007','008','009')) or  isnull(SsDetraccion,0)>0) then 1 else 0 end,--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
--HLF-20151002-AccountCode=CASE WHEN cast ....
--FLK-JRF-20151007-Case When d.CoMoneda = 'USD' Then '49711002' else '49711001' End.. -49711002
--JHD-20151027-DECLARE @SSTipoCambio_FEgreso decimal(6,3)=0
--JHD-20151027-@SSTipoCambio_FEgreso=SSTipoCambio_FEgreso
--JHD-20151027-SET @ssNeto=dbo.FnCambioMoneda(@ssNeto,@CoMoneda_Orig,'USD',ISNULL(@SSTipoCambio,isnull(@SSTipoCambio_FEgreso,isnull((select sstipcam from matiposcambio_usd where comoneda=@CoMoneda_Orig),0))))
--JHD-20151027-Case When @CoMoneda = 'USD' Then '49711002' else '49711001' End
--JHD-20151116-AccountCode=CASE WHEN cast(convert(char(6), cc.FechaOut,112) as int)>cast(convert(char(6), D.FeRecepcion,112) as int) then
--JHD-20151210-AccountCode= case when d.CoObligacionPago='PRL' THEN 
			--(CASE WHEN cast(convert(char(6), cc.FechaOut,112) as int)>cast(convert(char(6), D.FeRecepcion,112) as int) then
			--	Case When @CoMoneda = 'USD' Then '49711002' else '49711001' End
			--Else
			--	isnull(d.CoCtaContab,'')
			--End) ELSE isnull(d.CoCtaContab,'') END,
--JHD-20151210-CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),case when d.NuVoucher is not null then dbo.FnDevuelveIdFilePorVoucher(d.nuvoucher) else '00000000' end
CREATE PROCEDURE [dbo].[Documento_Proveedor_List_SAP_Lines] --769
@nudocumprov int
as
BEGIN
DECLARE @ssNeto numeric(9,2)=0
DECLARE @SSTipoCambio decimal(6,3)=0

DECLARE @SSTipoCambio_FEgreso decimal(6,3)=0

DECLARE @CoMoneda_SAP char(3)='',@CoMoneda char(3),@CoMoneda_Orig char(3),@FeEmision smalldatetime
set @ssNeto = (select SsNeto from DOCUMENTO_PROVEEDOR where NuDocumProv=@nudocumprov) 
declare @CoTipoDocSAP as tinyint=0

select @CoMoneda_SAP= isnull(rtrim(ltrim(m.CoSAP)),isnull((SELECT CoSAP from mamonedas where IDMoneda=D.CoMoneda_FEgreso),'')),
@CoMoneda=case when isnull(rtrim(ltrim(m.CoSAP)),'')='' then d.CoMoneda_FEgreso else d.CoMoneda end,
@CoMoneda_Orig=d.CoMoneda,
@SSTipoCambio=SSTipoCambio,
@FeEmision=FeEmision,
@SSTipoCambio_FEgreso=SSTipoCambio_FEgreso
from DOCUMENTO_PROVEEDOR d
left join MAMONEDAS m on m.IDMoneda=d.CoMoneda
where d.NuDocumProv=@nudocumprov

--
--SELECT @CoMoneda,@CoMoneda_SAP,@CoMoneda_Orig,@SSTipoCambio
--

if @CoMoneda_SAP='US$'
	BEGIN
			if @CoMoneda<>'USD'
				begin
					SET @ssNeto=dbo.FnCambioMoneda(@ssNeto,@CoMoneda,'USD',ISNULL(@SSTipoCambio,isnull((select ValVenta from matipocambio where fecha=@FeEmision),0)))
				end
			else
				begin
					if @CoMoneda_Orig not in ('SOL','USD')
					begin
						--SET @ssNeto=dbo.FnCambioMoneda(@ssNeto,@CoMoneda_Orig,'USD',ISNULL(@SSTipoCambio,isnull((select ValVenta from matipocambio where fecha=@FeEmision),0)))
						SET @ssNeto=dbo.FnCambioMoneda(@ssNeto,@CoMoneda_Orig,'USD',ISNULL(@SSTipoCambio,isnull(@SSTipoCambio_FEgreso,isnull((select sstipcam from matiposcambio_usd where comoneda=@CoMoneda_Orig),0))))
					end 
				end
	END
else --soles
	BEGIN
			if @CoMoneda<>'SOL'
				SET @ssNeto=dbo.FnCambioMoneda(@ssNeto,@CoMoneda,'SOL',ISNULL(@SSTipoCambio,isnull((select ValVenta from matipocambio where fecha=@FeEmision),0)))
	END

set @CoTipoDocSAP=(select cotipodocsap from DOCUMENTO_PROVEEDOR where NuDocumProv=@nudocumprov)
if @CoTipoDocSAP=0
begin
select
ItemCode= isnull(p.cosap,''), --isnull((select cosap from maproductos where coproducto= dd.CoProducto),''),
ItemDescription=isnull(p.NoProducto,''),
WarehouseCode=isnull(a.CoSAP,''),
Quantity=isnull(dd.QtCantidad,0),
DiscountPercent=0,
AccountCode='',
UnitPrice=round(abs(isnull(dd.SSPrecUni,0)),2),
LineTotal=round(abs(isnull(dd.SsMonto,0)),2),
--TaxCode=case when isnull(dd.SsIGV,0)=0 then 'EXE_IGV' else 'IGV' end,
TaxCode=--case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
			--case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
				CASE WHEN d.CoTipoOC='001' then 'IGV'
				WHEN d.CoTipoOC='012' then 'DMIX_IGV' 
				WHEN d.CoTipoOC='002' then 'DNGR_IGV' 
				WHEN d.CoTipoOC='003' then 'EXE_IGV' 
				WHEN d.CoTipoOC='011' then 'EXE_IGV' 
				else 'EXE_IGV' END,
--TaxTotal=abs(isnull(dd.SsIGV,0)),
TaxTotal= round(case when d.CoTipoOC in ('001','012','002','003') then abs(isnull(dd.SsIGV,0)) else 0 end,2),
--WTLiable=case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then 1 else 0 end,--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
--WTLiable=case when ((d.CoTipoDoc='RH' AND d.CoTipoOC='007') or  isnull(SsDetraccion,0)>0) then 1 else 0 end,--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
WTLiable=case when ((d.CoTipoDoc in ('RH','HTD') AND d.CoTipoOC in ('007','008','009')) or  isnull(SsDetraccion,0)>0) then 1 else 0 end,--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
TaxOnly=0,--cast(0 as bit),
ProjectCode='',
CostingCode=isnull(d.CoCeCos,''),
CostingCode2=isnull(d.CoCeCon,''),
--CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),'00000000'),
CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),case when d.NuVoucher is not null then dbo.FnDevuelveIdFilePorVoucher(d.nuvoucher) else '00000000' end),
CostingCode4='',
CostingCode5='',
U_SYP_CONCEPTO=dd.NuDocumProvDet
from DOCUMENTO_PROVEEDOR d
inner join DOCUMENTO_PROVEEDOR_DET dd on d.NuDocumProv=dd.NuDocumProv
left join MAPRODUCTOS p on p.CoProducto=dd.CoProducto
left join MAALMACEN a on a.CoAlmacen=dd.CoAlmacen
where d.NuDocumProv=@nudocumprov
end
else
begin 
select
ItemCode= '', --isnull((select cosap from maproductos where coproducto= dd.CoProducto),''),
ItemDescription='',
WarehouseCode='',
Quantity=1,
DiscountPercent=0, 
--AccountCode=isnull(d.CoCtaContab,''),
--AccountCode=CASE WHEN cast(convert(char(6), cc.FechaOut,112) as int)>cast(convert(char(6), D.FeRecepcion,112) as int) then
--		--'49711002'
--		--Case When d.CoMoneda = 'USD' Then '49711002' else '49711001' End
--		Case When @CoMoneda = 'USD' Then '49711002' else '49711001' End
--	Else
--		isnull(d.CoCtaContab,'')
--	End,

AccountCode= case when d.CoObligacionPago='PRL' THEN 
			--(CASE WHEN cast(convert(char(6), cc.FechaOut,112) as int)>cast(convert(char(6), D.FeRecepcion,112) as int) then
			(CASE WHEN cast(convert(char(6), cc.FechaOut,112) as int)>cast(convert(char(6), D.FeEmision,112) as int) then
				Case When @CoMoneda = 'USD' Then '49711002' else '49711001' End
			Else
				isnull(d.CoCtaContab,'')
			End) ELSE isnull(d.CoCtaContab,'') END,

--UnitPrice=abs(isnull(d.SSTotal,0))/*-abs(case when ((d.CoTipoDoc='RH' AND d.CoTipoOC='007') or  isnull(SsDetraccion,0)>0) then 1 else 0 end)-abs(isnull(d.SsOtrCargos,0))-abs(isnull(d.SSPercepcion,0))*/,
UnitPrice= round(@ssNeto,2), --CASE WHEN @ssNeto_Dolares=0 THEN @ssNeto_Soles ELSE @ssNeto_Dolares END,
--LineTotal=abs(isnull(d.SSTotal,0)),
LineTotal= round(@ssNeto,2), --CASE WHEN @ssNeto_Dolares=0 THEN @ssNeto_Soles ELSE @ssNeto_Dolares END,
--TaxCode=case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
TaxCode=--case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
			--case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 
				CASE WHEN d.CoTipoOC='001' then 'IGV'
				WHEN d.CoTipoOC='012' then 'DMIX_IGV' 
				WHEN d.CoTipoOC='002' then 'DNGR_IGV' 
				WHEN d.CoTipoOC='003' then 'EXE_IGV' 
				WHEN d.CoTipoOC='011' then 'EXE_IGV' 
				else 'EXE_IGV' END,
			--end,
--TaxTotal=abs(isnull(d.SsIGV,0)),
TaxTotal= round(case when d.CoTipoOC in ('001','012','002','003') then abs(isnull(d.SsIGV,0)) else 0 end,2),
--WTLiable=case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
--WTLiable=case when ((d.CoTipoDoc='RH' AND d.CoTipoOC='007') or  isnull(SsDetraccion,0)>0) then 1 else 0 end,
WTLiable=case when ((d.CoTipoDoc in ('RH','HTD') AND d.CoTipoOC in ('007','008','009')) or  isnull(SsDetraccion,0)>0) then 1 else 0 end,--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
TaxOnly=cast(0 as bit),
ProjectCode='',
CostingCode=isnull(d.CoCeCos,''),
CostingCode2=isnull(d.CoCeCon,''),
--CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),'00000000'),
CostingCode3=isnull((select idfile from coticab where idcab=d.idcab),case when d.NuVoucher is not null then dbo.FnDevuelveIdFilePorVoucher(d.nuvoucher) else '00000000' end),
CostingCode4='',
CostingCode5='',
U_SYP_CONCEPTO=''--dd.NuDocumProvDet
from DOCUMENTO_PROVEEDOR d
left join MAMONEDAS m on m.IDMoneda=d.CoMoneda
--inner join DOCUMENTO_PROVEEDOR_DET dd on d.NuDocumProv=dd.NuDocumProv
--left join MAPRODUCTOS p on p.CoProducto=dd.CoProducto
--left join MAALMACEN a on a.CoAlmacen=dd.CoAlmacen
Left Join COTICAB cc On cc.IDCAB=d.IDCab
where d.NuDocumProv=@nudocumprov
end
END;
