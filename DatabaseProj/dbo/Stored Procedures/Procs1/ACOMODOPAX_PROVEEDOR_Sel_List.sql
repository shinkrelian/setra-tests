﻿CREATE Procedure [Dbo].[ACOMODOPAX_PROVEEDOR_Sel_List]       
@IDReserva int      
As      
 Set NoCount On      
 Select IDReserva,IDPax,IDHabit,CapacidadHab,EsMatrimonial,UserMod       
  from ACOMODOPAX_PROVEEDOR     
  Where IDReserva = @IDReserva      
