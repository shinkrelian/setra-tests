﻿--JRF-20130919-Cambio de FechaSalida x Fecha de Emisión.
CREATE Procedure COTIVUELOS_SelTrenxIDCab                
@IDCab int                
As                
 Set NoCount On                
 select       
   ISNULL(Case When LEN(Cast(DAY(cv.Fec_Emision)As Char(2))) = 1 Then '0'+ Cast(DAY(cv.Fec_Emision)As Char(2)) Else  Cast(DAY(cv.Fec_Emision)As Char(2))End                
   +'.'+Case When LEN(Cast(MONTH(cv.Fec_Emision)As Char(2))) = 1 Then '0'+Cast(MONTH(cv.Fec_Emision)As Char(2)) Else Cast(MONTH(cv.Fec_Emision)As Char(2))End                  
 ,'01/01/1900') As Fecha       
  ,cv.Ruta , (select count(*) from COTITRANSP_PAX where IDTransporte = cv.ID) as NroPax , cv.Vuelo + ' '+ isnull(cv.Salida,'00:00') + ' - '+ isnull(cv.Llegada,'00:00') As ServiceWagon                
  ,ISNULL(cv.CodReserva,'') As Code ,    
  ISNULL(R.Estado,'') as Estado
 from COTIVUELOS cv    
 Left Join COTIDET cd On cv.IdDet = cd.IDDET    
 Left Join RESERVAS_DET rd On cd.IDDET = rd.IDDet Left Join RESERVAS r On rd.IDReserva = r.IDReserva    
 Where cv.IDCAB = @IDCab and cv.TipoTransporte = 'T' and rd.FlServicioNoShow = 0
 Order by CAST(Convert(char(10),cv.Fec_Salida,103)+' '+ isnull(cv.Salida,'00:00') as smalldatetime)
