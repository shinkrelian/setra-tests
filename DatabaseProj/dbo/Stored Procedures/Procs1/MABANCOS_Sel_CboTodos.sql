﻿CREATE Procedure MABANCOS_Sel_CboTodos  
@Todos bit  
As  
 Set NoCount On  
 Select * from (  
 select '000'As IDBanco,'<TODOS>'As Descripcion,0 As Ord  
 Union 
 select '000'As IDBanco,'---'As Descripcion,1 As Ord  where @Todos = 0
 Union  
 select IDBanco,Descripcion,2 As Ord from MABANCOS where IDBanco <> '000'  
 )As X  
 Where (@Todos=1 Or Ord <> 0)  
