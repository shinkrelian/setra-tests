﻿CREATE procedure [dbo].[MACLIENTES_Sel_Ficha]
@IDCliente varchar(6)
as
	Set NoCount On
	
	Select c.IDCliente,c.RazonSocial,
	case c.Persona when 'J' then 'Juridica' when 'N' then 'Natural' End as Persona,
	case CAST(c.Domiciliado as varchar) when '1' then 'Domiciliado'
		when '0' then 'No Domiciliado' End as Domiciliado,
		ISNULL(ti.Descripcion,'') as TipoIdentidad,isnull(c.NumIdentidad,''),
		case c.Tipo When 'I' then 'Interesado' when 'C' then 'Cliente' End as Tipo,
		c.Direccion,
		Numeros=	
		Case When Right(isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,''),1)='/' Then 
			isnull(c.Telefono1,'')
		Else
			Case When Left(isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,''),1)='/' Then 
				isnull(c.Telefono2,'')
				else
					isnull(c.Telefono1,'')+'/'+isnull(c.Telefono2,'')
			End
		End
		,isnull(c.Celular,'No dispone de un celular') Celular,isnull(c.Fax,'No dispone de un Fax') Fax,
		isnull(c.Notas,'No se encontraron Notas') Notas ,
		isnull(u.Descripcion,'') as Ciudad, up.Descripcion as Pais,
		--case cc.Activo when 'A' then 'Activo' when 'I' then 'Inactivo' End as Activo,
		--ISNULL(c.Email,'No dispone de un correo') Email,
		'' Email,
		ISNULL(c.Web,'No dispone de un Sitio Web') as SitioWeb,
		ISNULL(cast(e.Titulo+' '+ e.Apellidos+' '+e.Nombres as varchar),'') Vendedor,
		ISNULL(c.CodPostal,'') CodPostal, 
	    --m.Simbolo as Simbolo, 
	    '' as Simbolo, 
	    isnull(c.Credito,0) as Credito
	From MACLIENTES c Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo
		--Left Join MACLIENTES cc On c.Relacion=cc.IDCliente 
		Left join MATIPOIDENT ti
		On c.IDIdentidad=ti.IDIdentidad 
		Left Join MACONTACTOSCLIENTE e	On c.IDCliente=e.IDCliente Left Join MAUBIGEO up On u.IDPais=up.IDubigeo
		--Left join MAMONEDAS m on c.IDMoneda=m.IDMoneda
	Where c.IDCliente = @IDCliente
