﻿

Create Procedure dbo.COTICAB_SelTipoCambioxIDCab
@IDCab int,
@pTipoCambio numeric(8,2) OutPut
As
	set NoCount On
	set @pTipoCambio = (select IsNull(TipoCambio,0) As TipCambio from COTICAB where IDCAB = @IDCab)
