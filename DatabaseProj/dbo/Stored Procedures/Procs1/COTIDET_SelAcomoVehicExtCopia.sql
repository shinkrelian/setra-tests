﻿
--HLF-20150707-Comentando --and cd.IDubigeo=@CoUbigeo 
CREATE Procedure dbo.COTIDET_SelAcomoVehicExtCopia
	@IDCab int,
	--@CoUbigeo char(6),		
	@IDDetBase int
As
	Set Nocount On

	Select 
	upper(substring(DATENAME(dw,Dia),1,3))+' '+ltrim(rtrim(convert(varchar,Dia,103))) as Fecha,
	substring(convert(varchar,Dia,108),1,5) as Hora,
	cd.Servicio, 
	CAST(NroPax AS VARCHAR(3))+                                      
	Case When isnull(NroLiberados,0)=0 then '' else '+'+CAST(NroLiberados AS VARCHAR(3)) end                                      
	as Pax, 
	Case When Not Exists(Select IDDet From ACOMODO_VEHICULO Where IDDet=cd.IDDET)
		And Not Exists(Select IDDet From ACOMODO_VEHICULO_EXT Where IDDet=cd.IDDET) Then '' Else 'Ac.'
	End FlAcomodosVehic
	,cd.IDDET
	From COTIDET cd Inner join MASERVICIOS_DET sd On sd.IDServicio_Det=cd.IDServicio_Det
	Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
	Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
	Where cd.idcab=@IDCab 
		--and cd.IDubigeo=@CoUbigeo 
		And p.IDTipoProv<>'001' and p.IDTipoOper='E'
	And cd.IDDET<>@IDDetBase
	Order by cd.Dia


