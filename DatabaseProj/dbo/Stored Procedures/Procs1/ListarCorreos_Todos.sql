﻿create Procedure[dbo].[ListarCorreos_Todos]  
--declare
 @Descripcion varchar(100)  =''
As  
begin
select distinct nombres+' '+Apellidos as Nombre, email from MACONTACTOSPROVEEDOR c inner join MAPROVEEDORES p on c.IDProveedor=p.IDProveedor
where email is not null and p.Activo='a' and email <>'-'
and (nombres+' '+Apellidos+' '+email like '%'+@Descripcion+'%' or @Descripcion='')

union 

select distinct nombres+' '+Apellidos as Nombre,email from MACONTACTOSCLIENTE c inner join MACLIENTES p on c.IDCliente=p.IDCliente
where email is not null and p.Activo='a' and email <>'-'
and (nombres+' '+Apellidos+' '+email like '%'+@Descripcion+'%' or @Descripcion='')
union 

select distinct nombre+' '+txapellidos as Nombre, correo from MAUSUARIOS u 
where isnull(correo,'')<>'' and u.Activo='A' and correo <>'-'
and (nombre+' '+txapellidos+' '+correo like '%'+@Descripcion+'%' or @Descripcion='')
end;
