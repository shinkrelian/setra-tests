﻿CREATE Procedure [dbo].[COTICAB_QUIEBRES_Del]
	@IDCAB_Q	int    
As

	Set NoCount On
		
	Delete COTICAB_QUIEBRES	
    Where
           IDCAB_Q=@IDCAB_Q
