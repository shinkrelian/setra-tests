﻿--JRF-20141112-Agregar la columna CoCBU
--JRF-20150120-IVAN varchar(45),DireccionBancoPag varchar(80),NroCuentaBenef varchar(45)
CREATE Procedure [dbo].[MAADMINISTPROVEEDORES_Ins]    
 @IDAdminist char(2),    
 @IDProveedor char(6),    
 @Banco char(3),    
     
 @BancoExtranjero varchar(80),    
     
 @CtaCte varchar(30),    
 @IDMoneda char(3),    
 @SWIFT varchar(30),    
 @IVAN varchar(45),    
 @NombresTitular varchar(80),    
 @ApellidosTitular varchar(80),    
 @ReferenciaAdminist text,    
 @CtaInter varchar(30),    
 @BancoInterm char(3),    
     
 @BancoInterExtranjero varchar(80),    
 @TipoCuenta char(2),    
     
 @TitularBenef varchar(90) ,    
 @TipoCuentBenef char(1) ,    
 @NroCuentaBenef varchar(45) ,    
 @DireccionBenef varchar(70) ,    
     
 @NombreBancoPag varchar(80) ,    
 @PECBancoPag varchar(70) ,    
 @DireccionBancoPag varchar(80) ,    
 @SWIFTBancoPag varchar(10) ,    
     
 @EsOpcBancoInte bit,    
 @NombreBancoInte varchar(80) ,    
 @PECBancoInte varchar(90) ,    
 @SWIFTBancoInte varchar(10) ,    
 @CuentBncPagBancoInte varchar(15) ,  
 @CoCBU varchar(30),  
 @UserMod char(4)    
    
As    
 Set NoCount On    
     
 INSERT INTO MAADMINISTPROVEEDORES    
           ([IDAdminist]    
           ,[IDProveedor]    
           ,[Banco]    
               
           ,[BancoExtranjero]    
               
           ,[CtaCte]    
           ,[IDMoneda]    
           ,[SWIFT]    
           ,[IVAN]    
           ,[NombresTitular]    
           ,[ApellidosTitular]    
           ,[ReferenciaAdminist]    
           ,[CtaInter]    
           ,[BancoInterm]    
               
           ,[BancoInterExtranjero]    
           ,[TipoCuenta]    
               
           ,[TitularBenef]    
           ,[TipoCuentBenef]    
     ,[NroCuentaBenef]    
     ,[DireccionBenef]    
     
     ,[NombreBancoPag]    
     ,[PECBancoPag]    
     ,[DireccionBancoPag]    
     ,[SWIFTBancoPag]    
     
     ,[EsOpcBancoInte]    
     ,[NombreBancoInte]    
     ,[PECBancoInte]    
     ,[SWIFTBancoInte]    
     ,[CuentBncPagBancoInte]    
       ,[CoCBU]  
           ,[UserMod]    
           )    
     VALUES    
           (@IDAdminist ,    
            @IDProveedor ,    
            Case When RTRIM(ltrim(@Banco))= '' Then Null Else @Banco End,    
            Case When rtrim(ltrim(@BancoExtranjero))='' Then Null Else @BancoExtranjero End,    
            Case When RTRIM(ltrim(@CtaCte ))= '' Then Null Else @CtaCte End,    
            @IDMoneda,    
            Case When RTRIM(ltrim(@SWIFT))= '' Then Null Else @SWIFT End,    
            Case When RTRIM(ltrim(@IVAN))= '' Then Null Else @IVAN End,    
            Case When RTRIM(ltrim(@NombresTitular))= '' Then Null Else @NombresTitular End,    
            Case When RTRIM(ltrim(@ApellidosTitular))= '' Then Null Else @ApellidosTitular End,    
            Case When RTRIM(ltrim(Cast (@ReferenciaAdminist as varchar(max))))= '' Then Null Else @ReferenciaAdminist End,    
            Case When RTRIM(ltrim(@CtaInter))= '' Then Null Else @CtaInter End,    
            Case When RTRIM(ltrim(@BancoInterm))= '' Then Null Else @BancoInterm End,    
                
            Case When RTRIM(ltrim(@BancoInterExtranjero))= '' Then Null Else @BancoInterExtranjero End,    
            @TipoCuenta,    
                
            Case when RTRIM(LTRIM(@TitularBenef))='' Then Null Else @TitularBenef End,    
            Case When RTRIM(ltrim(@TipoCuentBenef))='' Then Null Else @TipoCuentBenef End,    
            Case When RTRIM(ltrim(@NroCuentaBenef))='' Then Null Else @NroCuentaBenef End,    
            Case When RTRIM(ltrim(@DireccionBenef))='' Then Null Else @DireccionBenef End,    
                
            Case When RTRIM(LTRIM(@NombreBancoPag))='' Then Null Else @NombreBancoPag End,    
            Case When RTRIM(ltrim(@PECBancoPag))='' Then Null Else @PECBancoPag End,    
            Case When RTRIM(ltrim(@DireccionBancoPag))='' Then Null Else @DireccionBancoPag End,    
            Case When RTRIM(ltrim(@SWIFTBancoPag))='' Then Null Else @SWIFTBancoPag End,    
                
            @EsOpcBancoInte,    
            Case When RTRIM(ltrim(@NombreBancoInte))='' Then Null Else @NombreBancoInte End,    
            Case When RTRIM(ltrim(@PECBancoInte))='' Then Null Else @PECBancoInte End,    
            Case When RTRIM(ltrim(@SWIFTBancoInte))='' Then Null Else @SWIFTBancoInte End,    
            Case When RTRIM(ltrim(@CuentBncPagBancoInte))='' Then Null Else @CuentBncPagBancoInte End,    
            Case When RTRIM(Ltrim(@CoCBU))='' Then Null Else @CoCBU End,  
            @UserMod)    
