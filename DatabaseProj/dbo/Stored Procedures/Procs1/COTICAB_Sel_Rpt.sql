﻿--JRF-20130415-Nuevos filtros(Usuario Reservas, Operaciones)                                                   
--JRF-20130711-Adicionar el total de cotización                                                   
--JRF-20130913-Obtener datos de los DebitMemo.                                                
--JRF-20130908-Mostrar el valor total de la Cotizacion.                                             
--JRF-20130924-Corrección Left Join DEBIT_MEMO                                           
--JRF-20130925-Agregar el Margen de Ganancia.                                          
--JRF-20130925-Cuando el total es 0 considerar como total el total en dbl por el nroPax.                                          
--HLF-20140121-Agregando EjVentas, EjReservas, EjOperaciones                                    
--HLF-20140122-Agregando Case When isnull(Y.Correlativo,'00')='00' para 4 campos                                  
--HLF-20140123-FecInicio As FechaIn,   c.FechaOut As FechaOut,  ya no Convert(char(10),FecInicio,103) As FechaIn,   Convert(char(10),c.FechaOut,103) As FechaOut,                                                      
--HLF-20140123-Agregando [FnSaldoFavorExportador]                              
--JRF-20140127-Considerar los Correlativos = ''                            
--JRF-20140131-No se debe mostrar los debitMemos Anulados.                          
--HLF-20140204-And ISNULL(db.IDEstado,'') <> 'AN'                                               
--HLF-20140205-And (ltrim(rtrim(@Estado))= 'P' Or isnull(c.IDFile,'') <> '')                                      
--JRF-20140317-Nuevos Filtro x Rango de Fecha [FechaAsigReserva]                    
--JRF-20140715-No validar Horas para el campo de FecAsgigReservas,c.Fecha,FecInicio,FechaOut              
--JRF-20140731-Columna Nueva [VerDatosCoti]:Solo los que esten activos en esta columna,              
--      deben mostrar los datos de Cotizacion              
--HLF-20140901-case when X.ExisteHotel = 1 then X.TotalSPLPax+X.TotalDBLPax+X.TotalTPLPax ...          
--dbo.FnEsTotalxPax(c.IDCAB) as EsTotalxPax          
--HLF-20140102-Comentando --And (ltrim(rtrim(@Estado))= 'P' Or isnull(c.IDFile,'') <> '')        
--JRF-20141211-Agregar nuevos filtros de rango de fechas de la generación del file      
--HLF-20141215-Agregar columna y.FechaReserva as FechaAceptado    
--JHD-20150303-Agregar columna DocWordVersion  
--JRF-20150311-Considerar el Total de Venta por todos los vuelos que ha emitido setours[FnDevuelveMontoTotalxPaxVuelos]
--HLF-20151203-And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)
--PPMG-20160208-Se agrego Tipo y Nota de Cliente

--Execute COTICAB_Sel_Rpt '','','','','','','20160101','20160131','01/01/1900','01/01/1900','01/01/1900','01/01/1900','01/01/1900','01/01/1900','01/01/1900','01/01/1900',0,'V',0

CREATE Procedure [dbo].[COTICAB_Sel_Rpt]      
@IDUsuarioResp char(4),                                                    
@IDUsuarioResv char(4),                                                    
@IDUsuarioOper char(4),                                                      
@IDCliente varchar(6),                                                      
@NroFile varchar(8),                                                      
@Estado char(1),                                                      
@FechaGenInicial smalldatetime,                                                      
@FechaGenFinal smalldatetime,                                                      
@FechaIngInicial smalldatetime,                                                      
@FechaIngFinal smalldatetime,                                                      
@FechaOutInicial smalldatetime,                                                      
@FechaOutFinal smalldatetime ,                       
@FechaAsgReservaInicial smalldatetime,                    
@FechaAsgReservaFinal smalldatetime,      
@FechaGenReservasInicial smalldatetime,      
@FechaGenReservasFinal smalldatetime,      
@NoExisteInfoOperacion bit,                                      
@Orderx char(1),
@FlHistorico bit=1
AS
BEGIN
 Set NoCount On   
 Declare @Igv numeric(5,2)=(Select NuIgv From PARAMETRO)/100
                             
 select Y.IDUsuario,Y.IDUsuarioRes,Y.IDUsuarioOpe,                                         
 Y.Mes,Y.IDFile,cast(Y.FechaIn as smalldatetime) as FechaIn,Y.FechaOut,    
 Y.FechaReserva as FechaAceptado,    
 Y.Cliente
 , Y.TipoCliente, Y.NotaCliente
 ,Y.Pais,Y.Titulo,                                          
 Y.NroPax,Y.Estado,                                    
 Y.NroLiberados,                                   Y.Nombre,                                    
 Y.EjVentas, Y.EjReservas, Y.EjOperaciones,                                    
 Y.Ord,                                  
 --Y.TotalCotiz,        
 Case When Y.VerDatosCoti = 1 Then Y.TotalCotiz Else 0 End as TotalCotiz,                                  
                                   
 --Y.TotalCostoCotiz,                                  
 Case When Y.VerDatosCoti = 1 Then Y.TotalCostoCotiz Else 0 End as TotalCostoCotiz,                                  
                                   
 Y.NroDebitMemoOrd,Y.IDDebitMemo,Y.Correlativo,Y.TotalDebitMemo,                                  
                                   
 --Y.MargenGan,                                          
 Case When Y.VerDatosCoti = 1 Then Y.MargenGan Else 0 End as MargenGan,                                  
                                   
 --cast(Round(((Y.MargenGan /100) * Y.TotalCostoCotiz),2) as Numeric(10,2)) As MargenNeto,                                   
 Case When Y.VerDatosCoti = 1 Then                                   
 cast(Round(((Y.MargenGan /100) * Y.TotalCostoCotiz),2) as Numeric(10,2))                                   
 Else 0 End as MargenNeto,                           
                                   
 Y.ExisteInfoOperaciones --,Y.NoExisteOperacionSLima,Y.NoExisteOperacionSCusco                              
 ,Y.SFE ,Y.FechaAsigReserva   ,Y.VerDatosCoti, EsTotalxPax         
 ,DocWordVersion              
 from          
  (                                          
  Select X.IDUsuario,X.IDUsuarioRes,X.IDUsuarioOpe,                                    
  --X.NomReserva,NomOperaciones,                                    
  Mes,X.IDFile,X.Cliente
  , X.TipoCliente, X.NotaCliente
  ,X.FechaIn,X.FechaOut,X.FechaReserva,    
  X.Pais,X.Titulo,X.NroPax,X.Estado,                                    
  X.NroLiberados,                                    
  X.Nombre,                                             
  X.EjVentas, X.EjReservas, x.EjOperaciones,                                    
  X.Ord,X.TotalCotiz+dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0) as TotalCotiz,                                          
  --case when X.ExisteHotel = 1 then X.TotalSimple+X.TotalDoble+X.TotalTriple else X.NroPax * X.TotalCotiz End as TotalCostoCotiz,                                              
  case when X.ExisteHotel = 1 then           
  Case When EsTotalxPax=1 Then          
   (Select SUM(Total) from COTIPAX Where IDCab=X.IDCAB)          
  Else          
   X.TotalSimple+X.TotalDoble+X.TotalTriple          
  End          
  else           
  (X.NroPax * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
  End 
  --+  dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
  as TotalCostoCotiz,          
  X.NroDebitMemoOrd,X.IDDebitMemo,X.Correlativo,X.TotalDebitMemo ,                                          
  case when X.TotalDobleNeto = 0 then 0                                          
  else                                           
   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                           
   End                                           
  as MargenGan ,X.ExisteInfoOperaciones                                      
  --X.NoExisteOperacionSLima,X.NoExisteOperacionSCusco                              
  ,X.SFE       ,X.FechaAsigReserva  ,               
   Case When (X.Correlativo = X.MinCorrelativoValido) then 1 else            
   case when X.MinCorrelativoValido is null then 1 else 0 End End    
   as VerDatosCoti, EsTotalxPax          
     ,DocWordVersion             
 from           
  (                                              
   select c.IDUsuario,c.IDUsuarioRes,c.IDUsuarioOpe,                          
   --isnull(ur.Nombre,'') As NomReserva,isnull(uo.Nombre,'') As NomOperaciones                                                    
   case when ISNULL(IDFile,'')='' then '' else                                                   
                                                   
   case when CAST(substring(IDFile,3,2) as tinyint) = 0 then '' else cast(CAST(substring(IDFile,3,2) as tinyint) as varchar(2)) End                                                  
   End as Mes                                                
                       
   ,isnull(IDFile,'') As IDFile,                                                    
   --Convert(char(10),FecInicio,103) As FechaIn,   Convert(char(10),c.FechaOut,103) As FechaOut,                                                      
   c.FecInicio As FechaIn,   c.FechaOut As FechaOut,    
   c.FechaReserva,    
   cl.RazonComercial As Cliente
   , case cl.Tipo when 'CO' then 'COUNTER' when 'WB' then 'WEB' when 'CL' then 'CLIENTE' when 'IT' then 'INTERESADO' when 'ND' then 'NODESEADO' else 'NO DEFINIDO' end AS TipoCliente
   , ISNULL(cl.Notas,'') AS NotaCliente
   ,ub.Descripcion As Pais,c.Titulo,c.NroPax,c.Estado,                                    
   ISNULL(c.NroLiberados,0) As NroLiberados,                                                      
   u.Usuario as Nombre ,            
   isnull(u.Nombre,'') as EjVentas,                                    
   isnull(ur.Nombre,'') as EjReservas,                                    
   isnull(uo.Nombre,'') As EjOperaciones,                                    
   Case  @Orderx                                                       
   When 'R' Then CAST(c.IDUsuarioRes as varchar(max))                                      
   When 'V' then Convert(Varchar(10),FecInicio,103)                                      
   When 'O' then CAST(c.IDUsuarioOpe as varchar(max)) --else  FecInicio                                          
    End As Ord  ,                                                  
   (select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = c.IDCAB) As TotalCotiz  ,                                                
  -- (select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = c.IDCAB)+(select ISNULL(SUM(SSTotal),0) from COTIDET cd where cd.IDCAB = c.IDCAB)                                                
    --As TotalCostoCotiz  ,                                                
    --------ISNULL(CAST(SUBSTRING(db.IDDebitMemo,Len(db.IDDebitMemo)-1,Len(db.IDDebitMemo)) AS tinyint),0) as NroDebitMemoOrd,                                                
    --------Isnull(SUBSTRING(db.IDDebitMemo,1,8),'') as IDDebitMemo,Isnull(SUBSTRING(db.IDDebitMemo,9,10),'') as Correlativo,                                                
    --------Isnull(db.Total,0) as TotalDebitMemo  ,
	0 as NroDebitMemoOrd, '' as IDDebitMemo, '' as Correlativo,
	ISNULL((SELECT SUM(Isnull(db.Total,0)) FROM DEBIT_MEMO db WHERE db.IDCab = c.IDCAB AND ISNULL(db.IDEstado,'') <> 'AN'), 0) as TotalDebitMemo,
                                                 
   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB) *((Isnull(c.Twin,0)+Isnull(c.Matrimonial,0))) *2)+ dbo.FnDevuelveMontoTotalxPaxVuelos(c.IDCAB,1) as TotalDoble,          
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB) * Isnull(c.Simple,0) as TotalSimple,                                              
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = c.IDCAB) * (Isnull(c.Triple,0)*3) as TotalTriple,                                            
                                             
   --(select SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB) as TotalDobleNeto,
   (select SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(c.IDCAB,0) as TotalDobleNeto,
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB) as TotalSimpleNeto,
   --(select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = c.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(c.IDCAB,0)as TotalSimpleNeto,
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = c.IDCAB) as TotalTripleNeto,
   --(select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = c.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(c.IDCAB,0) as TotalTripleNeto,
   
   (select SUM(TotImpto) from COTIDET cd where cd.IDCAB = c.IDCAB) as TotalImpto,                                              
   (select SUM(CostoReal) from COTIDET cd where cd.IDCAB = c.IDCAB) as TotalCostoReal,                                              
   (select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = c.IDCAB) as TotalLib,     
   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                                          
   Where cd.IDCAB = c.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,                
  Case when                                       
   Exists(select IDOperacion from OPERACIONES o where o.IDCab = c.IDCAB and o.IDProveedor = '001554') and                                       
   Exists(select IDOperacion from OPERACIONES o where o.IDCab = c.IDCAB and o.IDProveedor = '000544')                                      
  then 1 Else 0 End as ExisteInfoOperaciones ,
  
  --dbo.FnSaldoFavorExportador(c.IDCAB) as SFE,

  SFE=
		ISNULL((SELECT SUM(  
	CASE WHEN idTipoOC='001' THEN CD.CostoReal * @Igv
	ELSE  
		CASE WHEN idTipoOC='006' THEN (CD.CostoReal*@Igv)*0.5 
			ELSE 0 
	END    
	END * CD.NroPax) 
	FROM COTIDET CD INNER JOIN MASERVICIOS_DET SD ON CD.IDServicio_Det=SD.IDServicio_Det  
	INNER JOIN MASERVICIOS S ON S.IDServicio=SD.IDServicio
	INNER JOIN MAPROVEEDORES p ON S.IDProveedor=p.IDProveedor 
	
	WHERE CD.IDCab=c.IDCAB),0)
	
	+


	ISNULL((SELECT  SUM(  
	CASE WHEN X.idTipoOC='001' THEN X.CostoReal * @Igv
	ELSE  
		CASE WHEN X.idTipoOC='006' THEN (X.CostoReal*@Igv)*0.5 
			ELSE 0 
		END    
	END  * X.NroPax)
	
	FROM
		(SELECT DISTINCT CDS.CostoReal, SD.idTipoOC, CD.NroPax 
		FROM COTIDET_DETSERVICIOS CDS 
		INNER JOIN COTIDET CD ON CD.IDDET=CDS.IDDET And cd.IDCAB=C.IDCAB
		INNER JOIN MASERVICIOS_DET SD ON CDS.IDServicio_Det=SD.IDServicio_Det 
		) AS X
	),0)

	,

  c.FechaAsigReserva  ,
 
  
  --dbo.FnCorrelativoDMValido(c.IDCAB) as MinCorrelativoValido,          
  --------(select Top(1) substring(IDDebitMemo,9,10) as Correlativo 
  -------- from DEBIT_MEMO dm where dm.IDCab = c.IDCAB and IDEstado <> 'AN' 
  -------- Order by cast(substring(IDDebitMemo,9,10) as tinyint)) as MinCorrelativoValido,
  '' as MinCorrelativoValido,

  --dbo.FnEsTotalxPax(c.IDCAB) as EsTotalxPax, 
  Cast((Case When (select sum(Total) from COTIPAX where IDCab=c.IDCAB)=
				  ((NroPax+isnull(NroLiberados,0)) * (SELECT SUM(TOTAL) FROM COTIDET WHERE IDCAB=c.IDCAB))
				  then 0 else 1 End
		) 
  as bit) as EsTotalxPax, 
  c.IDCAB          
            
  --(Select SUM(SSTotal) from COTIPAX Where IDCab=c.IDCAB) as TotalSPLPax,          
  --(Select SUM(SSTotal) from COTIPAX Where IDCab=c.IDCAB) as TotalTPLPax          
  ,c.DocWordVersion  
                  
  From COTICAB c --------Left Join DEBIT_MEMO db On db.IDCab = c.IDCAB                                           
   Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente                                                       
   Left Join MAUBIGEO ub On c.IDubigeo = ub.IDubigeo                                               
   Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario                                                      
   Left Join MAUSUARIOS ur On c.IDUsuarioRes = ur.IDUsuario                                                    
   Left Join MAUSUARIOS uo On c.IDUsuarioOpe = uo.IDUsuario                                                     
   Where (ltrim(rtrim(@IDUsuarioResp))='' Or c.IDUsuario = @IDUsuarioResp)                                                      
   And (ltrim(rtrim(@IDCliente))='' Or cl.IDCliente = @IDCliente)                         
   --And db.IDEstado <> 'AN'                      
   --------And ISNULL(db.IDEstado,'') <> 'AN'                                                   
   And (ltrim(rtrim(@NroFile))='' Or c.IDFile like '%'+@NroFile+'%')                                                      
                         
   And (ltrim(rtrim(@Estado))= '' Or c.Estado = @Estado)                             
                              
   --And (ltrim(rtrim(@Estado))= 'P' Or isnull(c.IDFile,'') <> '')                                  
                         
   And (ltrim(rtrim(@IDUsuarioResv))='' Or c.IDUsuarioRes = @IDUsuarioResv)                                                    
   And (ltrim(rtrim(@IDUsuarioOper))='' Or c.IDUsuarioOpe = @IDUsuarioOper)                                                    
   And ((Convert(char(10),@FechaGenInicial,103)='01/01/1900' and Convert(char(10),@FechaGenFinal,103)='01/01/1900')                                                       
    Or Cast(Convert(Char(10),c.Fecha,103) As Smalldatetime)              
    between @FechaGenInicial and @FechaGenFinal)                                                      
   And ( (Convert(char(10),@FechaOutInicial,103)='01/01/1900' and Convert(char(10),@FechaOutFinal,103)='01/01/1900')                                                      
    Or Cast(Convert(char(10),c.FechaOut,103) as smalldatetime)             
    between @FechaOutInicial and @FechaOutFinal)                                                      
   And ((Convert(char(10),@FechaIngInicial,103)='01/01/1900' and Convert(char(10),@FechaIngFinal,103)='01/01/1900')                                                      
    Or Cast(Convert(char(10),FecInicio,103) as smalldatetime)               
    between @FechaIngInicial and @FechaIngFinal)                    
   And ((Convert(char(10),@FechaAsgReservaInicial,103)='01/01/1900' and Convert(char(10),@FechaAsgReservaFinal,103)='01/01/1900')                    
    Or cast(convert(char(10),FechaAsigReserva,103) As smalldatetime)               
   between @FechaAsgReservaInicial and @FechaAsgReservaFinal)      
   And ((Convert(char(10),@FechaGenReservasInicial,103)='01/01/1900'and CONVERT(char(10),@FechaGenReservasFinal,103)='01/01/1900')       
    Or Cast(Convert(Char(10),FechaReserva,103) As Smalldatetime)       
   between @FechaGenReservasInicial and @FechaGenReservasFinal)      
   And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)      
   ) as X                                       
   where (@NoExisteInfoOperacion = 0 Or X.ExisteInfoOperaciones = 0)                                      
   ) as Y                                          
--Where (@NoExisteInfoOperacion = 0 Or Y.NoExisteOperacionSCusco= @NoExisteInfoOperacion)                                      
 Order by --y.Ord                                  
  case @Orderx                                       
 When 'R' then CAST(Y.Ord as tinyint)                                      
 When 'V' then CAST(Y.Ord as smalldatetime)                                      
 when 'O' then CAST(Y.Ord as tinyint)End,                              
 CAST(Y.FechaIn AS SMALLDATETIME) asc,                               
 cast(substring(Y.IDFile,5,8) as int),Y.NroDebitMemoOrd ASC   
END
