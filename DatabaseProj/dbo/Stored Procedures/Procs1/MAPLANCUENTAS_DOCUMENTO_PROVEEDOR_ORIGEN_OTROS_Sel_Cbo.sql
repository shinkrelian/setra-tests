﻿--JHD-20151029 - if @CoCecos not in ('900101','940101','050102','900102','900103','900201','900202')
			   --set @CoCecos=''
--JHD-20151103 - if @CoCecos not in ('900101','940101','050102','900102','900103','900201','900202',...
--JHD-20151103 - union con cuentas que no tienen centro de costos
--JHD-20160129 - se quito el union con las cuentas que no tienen centro de costos
--JHD-20160129 - se quito la condicion: if @CoCecos not in ('900101','940101','050102','900102','900103','900201','900202',...
--JHD-20160129 - and c.FlCompra=1 and c.FlActivo=1
CREATE Procedure MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_ORIGEN_OTROS_Sel_Cbo
	@FlFile bit, --=0,
	@FlCoCecos bit, --=1,
	@FlCoCecon bit, --=1,
	@IdFile char(8) ='',
	@CoCecos char(6) =''
AS
BEGIN
/*
strCoCecos_Tmp = "900101" Or strCoCecos_Tmp = "940101" Or strCoCecos_Tmp = "050102" Or strCoCecos_Tmp = "900102" _
                                Or strCoCecos_Tmp = "900103" Or strCoCecos_Tmp = "900201" Or strCoCecos_Tmp = "900202"
*/

/*
if @CoCecos not in ('900101','940101','050102','900102','900103','900201','900202',
'010101','020103','020104','020105','020106','020201','030101','060201','060301','050102','050202','050301','050302','060101','060102','060103'
)
*/
	--set @CoCecos=''

select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion
from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_ORIGEN_OTROS d
inner join MAPLANCUENTAS c on c.CtaContable=d.CtaContable
where FlFile=@FlFile
and FlCoCecos=@FlCoCecos and FlCoCecon=@FlCoCecon
and (rtrim(ltrim(idfile))=rtrim(ltrim(@idfile)))
and (rtrim(ltrim(CoCecos))=rtrim(ltrim(@CoCecos)))
and c.FlCompra=1 and c.FlActivo=1
--order by c.CtaContable
/*
union 

select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion
from MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_ORIGEN_OTROS d
inner join MAPLANCUENTAS c on c.CtaContable=d.CtaContable
where FlFile=@FlFile
and FlCoCecos=@FlCoCecos and FlCoCecon=@FlCoCecon
and (rtrim(ltrim(idfile))=rtrim(ltrim(@idfile)))
--and (rtrim(ltrim(CoCecos))=rtrim(ltrim(@CoCecos)))
and CoCecos=''
*/
order by c.CtaContable
END;
