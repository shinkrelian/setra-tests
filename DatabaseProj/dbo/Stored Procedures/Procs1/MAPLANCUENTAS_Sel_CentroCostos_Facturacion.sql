﻿--MAPLANCUENTAS_Sel_CentroCostos_Facturacion '900101'
 CREATE Procedure dbo.MAPLANCUENTAS_Sel_CentroCostos_Facturacion
@CoCeCos char(6)=''--,
--@FlProvAdmin Char(1)=''
as
select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion,
 TxDefinicion=ISNULL(pc.TxDefinicion,'')
from MAPLANCUENTAS_CENTROCOSTOS pc
inner join MAPLANCUENTAS c on pc.CtaContable=c.CtaContable
where
(pc.CoCeCos=@CoCeCos Or LTRIM(rtrim(@CoCeCos))='')   
and c.FlFacturacion=1
 Order by c.CtaContable+ ' - ' + c.Descripcion
