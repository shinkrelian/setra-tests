﻿Create Procedure dbo.DOCUMENTO_DET_UpdManual 
 @NuDetalle tinyint,     
 @NuDocum char(10),    
 @NoServicio varchar(230),
 @IDTipoDoc char(3),       
 @IDMonedaCosto char(3),  
 @SsCompraNeto numeric(9, 2),           
 @SsIGVSFE numeric(5, 2),      
 @SsTotalCosto numeric(9, 2),          
 @SsMargen numeric(6, 2),      
 @SsTotalCostoUSD numeric(9, 2),      
 @SsTotalDocumUSD numeric(9, 2),     
 @CoTipoDet char(2),    
 @UserMod char(4)      
As      
 Set NoCount On          
 update DOCUMENTO_DET
	set IDMonedaCosto= @IDMonedaCosto,
		NoServicio = case when DOCUMENTO_DET.CoTipoDet <> 'ML' Then Null Else @NoServicio End,
		SsCompraNeto = @SsCompraNeto,
		SsIGVSFE = @SsIGVSFE,
		SsTotalCosto = @SsTotalCosto,
		SsMargen = @SsMargen,
		SsTotalCostoUSD = @SsTotalCostoUSD,
		SsTotalDocumUSD = @SsTotalDocumUSD,
		UserMod = @UserMod,
		FecMod = Getdate()
 where NuDocum = @NuDocum and IDTipoDoc = @IDTipoDoc and NuDetalle = @NuDetalle
