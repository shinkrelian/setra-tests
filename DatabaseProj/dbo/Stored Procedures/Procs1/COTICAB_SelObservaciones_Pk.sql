﻿CREATE PROCEDURE [dbo].[COTICAB_SelObservaciones_Pk]
				@IDCab int
AS
BEGIN
	Set NoCount On                          
                        
	Select Cotizacion, ObservacionVentas, ObservacionGeneral, Observaciones
	From COTICAB cc
	Where cc.IDCAB=@IDCab                           
END

