﻿CREATE Procedure [dbo].[MAFONDOFIJO_Sel_Pk]
	@NuFondoFijo INT
As
	Set NoCount On
BEGIN
	Select * From MAFONDOFIJO 
	Where NuFondoFijo = @NuFondoFijo
END;
