﻿CREATE Procedure [dbo].[MANIVELUSUARIO_Sel_List]
	@Descripcion varchar(50)
As
	Set NoCount On

	Select IDNivel,Descripcion From MANIVELUSUARIO Where 
	(Descripcion Like '%'+@Descripcion+'%' Or ltrim(rtrim(@Descripcion))='')
	And Activo='A'
