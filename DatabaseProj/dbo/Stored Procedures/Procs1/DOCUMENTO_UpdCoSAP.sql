﻿Create Procedure dbo.DOCUMENTO_UpdCoSAP
	@CoTipoDoc char(3),
	@NuDocum char(10),
	@CoSAP varchar(15),
	@UserMod char(4)
As
	Set Nocount on

	Update DOCUMENTO Set CoSAP = Case When ltrim(rtrim(@CoSAP))='' then null else @CoSAP end, UserMod=@UserMod, FecMod=GETDATE() 
	Where NuDocum = @NuDocum And IDTipoDoc = @CoTipoDoc 
