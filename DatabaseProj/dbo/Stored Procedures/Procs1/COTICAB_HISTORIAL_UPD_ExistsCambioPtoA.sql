﻿--JRF-20150106-Deshabilitar la función momentaneamente.
CREATE Procedure dbo.COTICAB_HISTORIAL_UPD_ExistsCambioPtoA
@IDCab int,
@pCambioEstado bit output
As
Set NoCount On
Declare @FlEnviado bit = (select FlCorreoEnviadoxCotAceptada from COTICAB where IDCAB=@IDCab)

Set @pCambioEstado = 0
select @pCambioEstado = 1 from COTICAB_HISTORIAL_UPD 
where NoCampo='Estado' and IDCab=@IDCab And (TxValorAntes='P' And TxValorDespues='A') And @FlEnviado=0
Set @pCambioEstado = 0
