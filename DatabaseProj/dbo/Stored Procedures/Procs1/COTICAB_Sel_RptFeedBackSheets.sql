﻿Create Procedure dbo.COTICAB_Sel_RptFeedBackSheets
@FechaRango1 smalldatetime,
@FechaRango2 smalldatetime
As
Set NoCount On
select c.IDFile,cl.RazonComercial as Cliente,c.Titulo,c.NroPax + IsNull(c.NroLiberados,0) as NroPax,count(ccp.idcab) As CantidadFBS
from COTICAB c Left Join CONTROL_CALIDAD_PRO ccp On ccp.IDCab=c.IDCAB
Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente
Where c.FlHistorico=0 And c.Estado='A'
--And Year(c.FecInicio)=2016  And Month(c.FecInicio) < 3
And (Convert(char(10),@FechaRango1,103)='01/01/1900' Or c.FecInicio between @FechaRango1 And @FechaRango2)
Group By c.IDFile,cl.RazonComercial,c.Titulo,c.NroPax,c.NroLiberados
--Order By c.FecInicio
