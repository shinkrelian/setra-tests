﻿--JRF-01.02.2013-- Solo Activos    
--HLF-20130403-Cambiando RazonSocial por NombreCorto
--JRF-20140625-Agregando la condicion para la descripcion de '<TODOS>'  
CREATE Procedure [dbo].[COTIVUELOS_Sel_CboAereo]           
 @bTodos bit,          
 @IdTipProv char(3),          
 @Sigla varchar(3),
 @bTodosDesc bit
As          
          
 Set NoCount On          
     
 SELECT * FROM          
 (          
 Select '' as IDProveedor,CASE WHEN @bTodosDesc = 0 THEN 'NO APLICA                                                                |' 
 ELSE '<TODOS>                                                                |'  END as RazonSocial, 0 as Ord          
 Union          
      
 select   
 --IDProveedor,RazonSocial + '                                                                |'+ Isnull(Sigla,'') As RazonSocial      
 IDProveedor,NombreCorto + '                                                                |'+ Isnull(Sigla,'') As RazonSocial      
 ,1 from MAPROVEEDORES          
 where (LTRIM(RTRIM(@Sigla))='' Or Sigla like '%'+@Sigla+'%')          
 and  ((LTRIM(@IdTipProv))='' Or  IDTipoProv=@IdTipProv)          
 and ltrim(rtrim(ISNULL(Sigla,''))) <>''  and Activo = 'A'    
 ) as X          
 Where (@bTodos=1 Or Ord<>0)          
 Order by Ord,2      
