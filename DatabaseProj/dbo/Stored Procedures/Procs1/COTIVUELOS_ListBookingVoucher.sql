﻿Create Procedure dbo.COTIVUELOS_ListBookingVoucher
@IDCab int
As
Set NoCount On
Select distinct Convert(char(10),Fec_Salida,103) as Fecha,
	   Ruta,Vuelo,Salida,Llegada,
	   Case When c.NroPax+IsNull(c.NroLiberados,0) <> (Select count(*) from COTITRANSP_PAX cpv where cpv.IDTransporte=cv.ID)
	   Then dbo.FnDevuelveListaPaxxIDTransporte(cv.ID) Else '' End As NombrePax
from COTIVUELOS cv Left Join COTICAB c On cv.IDCAB=c.IDCAB
where cv.IDCAB=@IDCab and 
EmitidoSetours=1 and TipoTransporte='V'
