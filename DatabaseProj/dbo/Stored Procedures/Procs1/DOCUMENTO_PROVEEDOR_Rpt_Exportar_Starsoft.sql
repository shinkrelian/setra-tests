﻿CREATE PROCEDURE DOCUMENTO_PROVEEDOR_Rpt_Exportar_Starsoft
@fecha1 smalldatetime='01/01/1900',
@fecha2 smalldatetime='01/01/1900'
as
BEGIN
select distinct NuDocumProv,
--DOCUMENTO_PROVEEDOR.NuVoucher,
 CoCtaContab,
Left(CONVERT(varchar,FeEmision,112),6) AS AnioMes,
'04' as subsidiario,
'' as correlativo,
FeEmision,
'03' as Tipo_Anexo,
isnull(MAPROVEEDORES.NumIdentidad,'') as RUC,
ISNULL(MATIPODOC.CoStarSoft_Compras,'') AS TipoDoc,
NuDocum,
NuDocum2=REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' '),
Fecha_Venc=DATEADD(day,isnull(maproveedores.DiasCredito,0),FeEmision)
,DiasCredito=isnull(maproveedores.DiasCredito,0)
,SsIGV
,ISNULL((SELECT TOP 1 NuIGV FROM PARAMETRO),0) as Tasa_IGV
,SSTotal
,'VTA' as Tipo_Conversion
,'' as Fecha_Registro
,'' Tipo_Cambio
,Glosa=ISNULL(MATIPODOC.CoStarSoft_Compras,'')+' '+REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' ')+'   /'
,CoTipoOC
,Porc_Oper_Mixtas=case when CoTipoOC='002' then '100' else '' end
,Valor_Cif=''
,TipoDoc_Ref=''
,NuDocum_Ref=''
,Centro_Costo=isnull((select top 1 CoCtroCosto from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')
,Afecto_Detraccion=case when SsDetraccion IS null then 0 else 1 end
,Num_Detraccion=''
,Fecha_Detraccion=''
,Fecha_DocReferencia=''
,Glosa_Movimiento=CoTipoOC+' '+substring(CoCtaContab,0,3)+' '+isnull((select Descripcion from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')  --?
,Doc_Anulado= case when DOCUMENTO_PROVEEDOR.FlActivo=0 then '0' else '1' end
,Igv_porAplicar=0
,DOCUMENTO_PROVEEDOR.CoTipoDetraccion
,Importacion=0
,Debe_Haber=(case when DOCUMENTO_PROVEEDOR.CoTipoDoc in ('NCR','NDB') THEN 'H' ELSE 'D' END)
,Tasa_Detraccion=(select SsTasa*100 from MATIPODETRACCION where CoTipoDetraccion=DOCUMENTO_PROVEEDOR.CoTipoDetraccion)
,SsDetraccion=ISNULL(SsDetraccion,0)
,IDFile=COTICAB.IDFile
,Orden=1
INTO #DOCUMENTOS_TMP
FROM DOCUMENTO_PROVEEDOR
 LEFT JOIN      
  VOUCHER_OPERACIONES ON DOCUMENTO_PROVEEDOR.NuVoucher = VOUCHER_OPERACIONES.IDVoucher   
  left join OPERACIONES_DET on OPERACIONES_DET.IDVoucher= VOUCHER_OPERACIONES.IDVoucher   
  left join OPERACIONES on OPERACIONES_DET.IDOperacion= OPERACIONES.IDOperacion  
  left join MAPROVEEDORES on MAPROVEEDORES.IDProveedor= OPERACIONES.IDProveedor  
  LEFT JOIN MATIPODOC ON MATIPODOC.IDtipodoc=DOCUMENTO_PROVEEDOR.CoTipoDoc
  left join COTICAB on OPERACIONES.IDCAB= COTICAB.IDCAB  

Where  DOCUMENTO_PROVEEDOR.NuVoucher<>0 
AND (@fecha1='01/01/1900' OR (FeEmision>@fecha1 and FeEmision<@fecha2+1))
--go

UNION ALL

--ORDEN DE SERVICIO
select distinct NuDocumProv,
--DOCUMENTO_PROVEEDOR.NuVoucher,
 CoCtaContab,
Left(CONVERT(varchar,FeEmision,112),6) AS AnioMes,
'04' as subsidiario,
'' as correlativo,
FeEmision,
'03' as Tipo_Anexo,
isnull(MAPROVEEDORES.NumIdentidad,'') as RUC,
ISNULL(MATIPODOC.CoStarSoft_Compras,'') AS TipoDoc,
NuDocum,
NuDocum2=REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' '),
Fecha_Venc=DATEADD(day,isnull(maproveedores.DiasCredito,0),FeEmision)
,DiasCredito=isnull(maproveedores.DiasCredito,0)
,SsIGV
,ISNULL((SELECT TOP 1 NuIGV FROM PARAMETRO),0) as Tasa_IGV
,SSTotal
,'VTA' as Tipo_Conversion
,'' as Fecha_Registro
,'' Tipo_Cambio
,Glosa=ISNULL(MATIPODOC.CoStarSoft_Compras,'')+' '+REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' ')+'   /'
,CoTipoOC
,Porc_Oper_Mixtas=case when CoTipoOC='002' then '100' else '' end
,Valor_Cif=''
,TipoDoc_Ref=''
,NuDocum_Ref=''
,Centro_Costo=isnull((select top 1 CoCtroCosto from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')
,Afecto_Detraccion=case when SsDetraccion IS null then 0 else 1 end
,Num_Detraccion=''
,Fecha_Detraccion=''
,Fecha_DocReferencia=''
,Glosa_Movimiento=CoTipoOC+' '+substring(CoCtaContab,0,3)+' '+isnull((select Descripcion from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')  --?
,Doc_Anulado= case when DOCUMENTO_PROVEEDOR.FlActivo=0 then '0' else '1' end
,Igv_porAplicar=0
,DOCUMENTO_PROVEEDOR.CoTipoDetraccion
,Importacion=0
,Debe_Haber=(case when DOCUMENTO_PROVEEDOR.CoTipoDoc in ('NCR','NDB') THEN 'H' ELSE 'D' END)
,Tasa_Detraccion=(select SsTasa*100 from MATIPODETRACCION where CoTipoDetraccion=DOCUMENTO_PROVEEDOR.CoTipoDetraccion)
,SsDetraccion=ISNULL(SsDetraccion,0)
,IDFile=COTICAB.IDFile
,Orden=1
FROM DOCUMENTO_PROVEEDOR
 LEFT JOIN      
 dbo.ORDEN_SERVICIO
 ON DOCUMENTO_PROVEEDOR.NuOrden_Servicio=ORDEN_SERVICIO.NuOrden_Servicio
 
  left join MAPROVEEDORES on MAPROVEEDORES.IDProveedor= ORDEN_SERVICIO.IDProveedor  
  LEFT JOIN MATIPODOC ON MATIPODOC.IDtipodoc=DOCUMENTO_PROVEEDOR.CoTipoDoc
  left join COTICAB on ORDEN_SERVICIO.IDCAB= COTICAB.IDCAB  

Where  DOCUMENTO_PROVEEDOR.NuOrden_Servicio IS NOT NULL
AND (@fecha1='01/01/1900' OR (FeEmision>@fecha1 and FeEmision<@fecha2+1))
--go

UNION ALL

--ORDEN DE PAGO

select distinct NuDocumProv,
--DOCUMENTO_PROVEEDOR.NuVoucher,
 CoCtaContab,
Left(CONVERT(varchar,FeEmision,112),6) AS AnioMes,
'04' as subsidiario,
'' as correlativo,
FeEmision,
'03' as Tipo_Anexo,
isnull(MAPROVEEDORES.NumIdentidad,'') as RUC,
ISNULL(MATIPODOC.CoStarSoft_Compras,'') AS TipoDoc,
NuDocum,
NuDocum2=REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' '),
Fecha_Venc=DATEADD(day,isnull(maproveedores.DiasCredito,0),FeEmision)
,DiasCredito=isnull(maproveedores.DiasCredito,0)
,SsIGV
,ISNULL((SELECT TOP 1 NuIGV FROM PARAMETRO),0) as Tasa_IGV
,SSTotal
,'VTA' as Tipo_Conversion
,'' as Fecha_Registro
,'' Tipo_Cambio
,Glosa=ISNULL(MATIPODOC.CoStarSoft_Compras,'')+' '+REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' ')+'   /'
,CoTipoOC
,Porc_Oper_Mixtas=case when CoTipoOC='002' then '100' else '' end
,Valor_Cif=''
,TipoDoc_Ref=''
,NuDocum_Ref=''
,Centro_Costo=isnull((select top 1 CoCtroCosto from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')
,Afecto_Detraccion=case when SsDetraccion IS null then 0 else 1 end
,Num_Detraccion=''
,Fecha_Detraccion=''
,Fecha_DocReferencia=''
,Glosa_Movimiento=CoTipoOC+' '+substring(CoCtaContab,0,3)+' '+isnull((select Descripcion from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')  --?
,Doc_Anulado= case when DOCUMENTO_PROVEEDOR.FlActivo=0 then '0' else '1' end
,Igv_porAplicar=0
,DOCUMENTO_PROVEEDOR.CoTipoDetraccion
,Importacion=0
,Debe_Haber=(case when DOCUMENTO_PROVEEDOR.CoTipoDoc in ('NCR','NDB') THEN 'H' ELSE 'D' END)
,Tasa_Detraccion=(select SsTasa*100 from MATIPODETRACCION where CoTipoDetraccion=DOCUMENTO_PROVEEDOR.CoTipoDetraccion)
,SsDetraccion=ISNULL(SsDetraccion,0)
,IDFile=c.IDFile
,Orden=1
FROM DOCUMENTO_PROVEEDOR
 LEFT JOIN      
 
 ORDENPAGO op        
 on op.IDOrdPag=DOCUMENTO_PROVEEDOR.CoOrdPag
 
 LEFT JOIN RESERVAS re on re.IDReserva=op.IDReserva      
  Left Join MAPROVEEDORES  On re.IDProveedor=MAPROVEEDORES.IDProveedor                     
 Left Join COTICAB c On op.IDCab=c.IDCAB   
   LEFT JOIN MATIPODOC ON MATIPODOC.IDtipodoc=DOCUMENTO_PROVEEDOR.CoTipoDoc

Where  DOCUMENTO_PROVEEDOR.CoOrdPag IS NOT NULL
AND (@fecha1='01/01/1900' OR (FeEmision>@fecha1 and FeEmision<@fecha2+1))
--go

UNION ALL

--Orden de Compra
select distinct NuDocumProv,
--DOCUMENTO_PROVEEDOR.NuVoucher,
 CoCtaContab,
Left(CONVERT(varchar,FeEmision,112),6) AS AnioMes,
'04' as subsidiario,
'' as correlativo,
FeEmision,
'03' as Tipo_Anexo,
isnull(MAPROVEEDORES.NumIdentidad,'') as RUC,
ISNULL(MATIPODOC.CoStarSoft_Compras,'') AS TipoDoc,
NuDocum,
NuDocum2=REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' '),
Fecha_Venc=DATEADD(day,isnull(maproveedores.DiasCredito,0),FeEmision)
,DiasCredito=isnull(maproveedores.DiasCredito,0)
,ORDENCOMPRA.SsIGV
,ISNULL((SELECT TOP 1 NuIGV FROM PARAMETRO),0) as Tasa_IGV
,DOCUMENTO_PROVEEDOR.SSTotal
,'VTA' as Tipo_Conversion
,'' as Fecha_Registro
,'' Tipo_Cambio
,Glosa=ISNULL(MATIPODOC.CoStarSoft_Compras,'')+' '+REPLACE(dbo.FnFormatearDocumProvee(DOCUMENTO_PROVEEDOR.NuDocum),'-',' ')+'   /'
,CoTipoOC
,Porc_Oper_Mixtas=case when CoTipoOC='002' then '100' else '' end
,Valor_Cif=''
,TipoDoc_Ref=''
,NuDocum_Ref=''
,Centro_Costo=isnull((select top 1 CoCtroCosto from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')
,Afecto_Detraccion=case when ORDENCOMPRA.SsDetraccion IS null then 0 else 1 end
,Num_Detraccion=''
,Fecha_Detraccion=''
,Fecha_DocReferencia=''
,Glosa_Movimiento=CoTipoOC+' '+substring(CoCtaContab,0,3)+' '+isnull((select Descripcion from dbo.MAPLANCUENTAS where CtaContable=CoCtaContab),'')  --?
,Doc_Anulado= case when DOCUMENTO_PROVEEDOR.FlActivo=0 then '0' else '1' end
,Igv_porAplicar=0
,DOCUMENTO_PROVEEDOR.CoTipoDetraccion
,Importacion=0
,Debe_Haber=(case when DOCUMENTO_PROVEEDOR.CoTipoDoc in ('NCR','NDB') THEN 'H' ELSE 'D' END)
,Tasa_Detraccion=(select SsTasa*100 from MATIPODETRACCION where CoTipoDetraccion=DOCUMENTO_PROVEEDOR.CoTipoDetraccion)
,SsDetraccion=ISNULL(ORDENCOMPRA.SsDetraccion,0)
,IDFile=''--COTICAB.IDFile
,Orden=1
FROM DOCUMENTO_PROVEEDOR
 LEFT JOIN      
 dbo.ORDENCOMPRA
 ON DOCUMENTO_PROVEEDOR.NuOrdComInt=ORDENCOMPRA.NuOrdComInt
 
  left join MAPROVEEDORES on MAPROVEEDORES.IDProveedor= ORDENCOMPRA.CoProveedor  
  LEFT JOIN MATIPODOC ON MATIPODOC.IDtipodoc=DOCUMENTO_PROVEEDOR.CoTipoDoc
  --left join COTICAB on ORDEN_SERVICIO.IDCAB= COTICAB.IDCAB  

Where  DOCUMENTO_PROVEEDOR.NuOrdComInt IS NOT NULL
AND (@fecha1='01/01/1900' OR (FeEmision>@fecha1 and FeEmision<@fecha2+1))

declare @NuDocumProv int=0
declare @Tasa_Detraccion numeric(10,4)=0
declare @contador_filas_Doc int=1

 Declare curDocumentos Cursor For Select NuDocumProv,Tasa_Detraccion from  #DOCUMENTOS_TMP order by NuDocumProv

 Open curDocumentos    
 Fetch Next From curDocumentos Into @NuDocumProv,@Tasa_Detraccion
 While @@FETCH_STATUS=0    
 Begin    
 
 set @contador_filas_Doc=+@contador_filas_Doc+1
   
 --si hay detraccion agregar una columna
 
if @Tasa_Detraccion>0
	begin
insert into #DOCUMENTOS_TMP
 select distinct NuDocumProv,
		 CoCtaContab,
		 AnioMes,
		subsidiario,
		correlativo,
		FeEmision,
		Tipo_Anexo,
		RUC,
		TipoDoc,
		NuDocum,
		NuDocum2,
		Fecha_Venc
		,DiasCredito
		,SsIGV
		,Tasa_IGV
		,SSTotal
		,Tipo_Conversion
		,Fecha_Registro
		,Tipo_Cambio
		,Glosa
		,CoTipoOC
		,Porc_Oper_Mixtas
		,Valor_Cif
		,TipoDoc_Ref
		,NuDocum_Ref
		,Centro_Costo
		,Afecto_Detraccion
		,Num_Detraccion
		,Fecha_Detraccion
		,Fecha_DocReferencia
		,Glosa_Movimiento
		,Doc_Anulado
		,Igv_porAplicar
		,CoTipoDetraccion
		,Importacion
		,Debe_Haber
		,Tasa_Detraccion
		,SsDetraccion
		,IDFile
		,Orden=@contador_filas_Doc
		FROM #DOCUMENTOS_TMP
		where nudocumprov=@NuDocumProv
		and Orden=1
		
 		update #DOCUMENTOS_TMP set SSTotal=ssDetraccion,SsIGV=0
		where NuDocumProv=@NuDocumProv
		and orden=  @contador_filas_Doc
		
		 set @contador_filas_Doc=+@contador_filas_Doc+1
		
	end

	 --igv
	 insert into #DOCUMENTOS_TMP
	 select distinct NuDocumProv,
	 '401111',
	 AnioMes,
	subsidiario,
	correlativo,
	FeEmision,
	Tipo_Anexo,
	RUC,
	TipoDoc,
	NuDocum,
	NuDocum2,
	Fecha_Venc
	,DiasCredito
	,SsIGV
	,Tasa_IGV
	,SSTotal
	,Tipo_Conversion
	,Fecha_Registro
	,Tipo_Cambio
	,Glosa
	,CoTipoOC
	,Porc_Oper_Mixtas
	,Valor_Cif
	,TipoDoc_Ref
	,NuDocum_Ref
	,Centro_Costo
	,Afecto_Detraccion
	,Num_Detraccion
	,Fecha_Detraccion
	,Fecha_DocReferencia
	,Glosa_Movimiento
	,Doc_Anulado
	,Igv_porAplicar
	,CoTipoDetraccion
	,Importacion
	,Debe_Haber
	,Tasa_Detraccion
	,SsDetraccion
	,IDFile
	,Orden=@contador_filas_Doc
	FROM #DOCUMENTOS_TMP
	where nudocumprov=@NuDocumProv
	and Orden=1

	update #DOCUMENTOS_TMP set SSTotal=SsIGV,SsIGV=0
	where NuDocumProv=@NuDocumProv
	and orden=@contador_filas_Doc

set @contador_filas_Doc=@contador_filas_Doc+1

--total
		 insert into #DOCUMENTOS_TMP
		 select distinct NuDocumProv,
		 '441221',
		 AnioMes,
		subsidiario,
		correlativo,
		FeEmision,
		Tipo_Anexo,
		RUC,
		TipoDoc,
		NuDocum,
		NuDocum2,
		Fecha_Venc
		,DiasCredito
		,SsIGV
		,Tasa_IGV
		,SSTotal
		,Tipo_Conversion
		,Fecha_Registro
		,Tipo_Cambio
		,Glosa
		,CoTipoOC
		,Porc_Oper_Mixtas
		,Valor_Cif
		,TipoDoc_Ref
		,NuDocum_Ref
		,Centro_Costo
		,Afecto_Detraccion
		,Num_Detraccion
		,Fecha_Detraccion
		,Fecha_DocReferencia
		,Glosa_Movimiento
		,Doc_Anulado
		,Igv_porAplicar
		,CoTipoDetraccion
		,Importacion
		,Debe_Haber=case when Debe_Haber='D' then 'H' else 'D' end
		,Tasa_Detraccion
		,SsDetraccion
		,IDFile
		,Orden=@contador_filas_Doc
		FROM #DOCUMENTOS_TMP
		where nudocumprov=@NuDocumProv
		and Orden=1

/*
,SsIGV
,Tasa_IGV
,SSTotal
*/


  update #DOCUMENTOS_TMP set SSTotal=SSTotal-SsIGV-ssDetraccion,SsIGV=0
where NuDocumProv=@NuDocumProv
and orden=1

set @contador_filas_Doc=1
 
 Fetch Next From curDocumentos Into @NuDocumProv,@Tasa_Detraccion
 End    
    
 Close curDocumentos    
 Deallocate curDocumentos   

select * from #DOCUMENTOS_TMP order by NuDocumProv,orden

DROP TABLE #DOCUMENTOS_TMP
END

