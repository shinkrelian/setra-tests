﻿

--PPMG-20151125-Se valido que no se duplique el usuario.
--PPMG-20151126-Se agrego el campo FlAccesoWeb
--JHD-20160225- Se quito la actualizacion de usuario y contraseña
CREATE Procedure [dbo].[MACONTACTOSCLIENTE_Upd]  
 @IDContacto char(3),  
 @IDCliente char(6),   
 @Titulo varchar(5),  
 @Nombres varchar(80),  
 @Apellidos varchar(80),  
 @Cargo varchar(100),  
 @Telefono varchar(30),  
 @Fax varchar(30),  
 @Celular varchar(30),  
 @Email varchar(200),  
 @Email2 varchar(200),  
 @Email3 varchar(200),  
 @Anexo varchar(15),   
 @UsuarioLogeo varchar(80)='',  
 @PasswordLogeo varchar(max)='',  
 @FechaPassword datetime,  
 @EsAdminist bit,  
 @Notas varchar(max),  
 @UserMod char(4),
 @FlAccesoWeb bit
AS
BEGIN
	--SET @UsuarioLogeo = ltrim(rtrim(@UsuarioLogeo))
	--IF @UsuarioLogeo <> ''
	--BEGIN
	--	IF [dbo].[FnExisteUsuarioProvClie](@IDContacto, @IDCliente, 'C', @UsuarioLogeo)=1
	--	BEGIN
	--		RAISERROR ('Ya esiste el nombre del usuario a ingresar.', 16, 1);
	--		RETURN
	--	END
	--END
 Set NoCount On
 UPDATE MACONTACTOSCLIENTE Set  
           Titulo=@Titulo  
           ,Nombres=@Nombres  
           ,Apellidos=@Apellidos  
           ,Cargo=Case When ltrim(rtrim(@Cargo))='' Then Null Else @Cargo End  
           ,Telefono=Case When ltrim(rtrim(@Telefono))='' Then Null Else @Telefono End  
           ,Fax=Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End  
           ,Celular=Case When ltrim(rtrim(@Celular))='' Then Null Else @Celular End  
           ,Email=Case When ltrim(rtrim(@Email))='' Then Null Else @Email End  
           ,Email2=Case When ltrim(rtrim(@Email2))='' Then Null Else @Email2 End  
           ,Email3=Case When ltrim(rtrim(@Email3))='' Then Null Else @Email3 End  
           ,Anexo=Case When ltrim(rtrim(@Anexo))='' Then Null Else @Anexo End  
           --,UsuarioLogeo=Case When ltrim(rtrim(@UsuarioLogeo))='' Then Null Else @UsuarioLogeo End  
           --,PasswordLogeo=Case When ltrim(rtrim(@PasswordLogeo))='' Then Null Else @PasswordLogeo End  
           ,FechaPassword=Case When @FechaPassword='01/01/1900' Then Null Else @FechaPassword End  
           ,EsAdminist=@EsAdminist  
           ,Notas=Case When ltrim(rtrim(@Notas))='' Then Null Else @Notas End  
           ,UserMod=@UserMod             
           ,FecMod=GETDATE()
		   ,FlAccesoWeb = @FlAccesoWeb 
     WHERE  
           IDContacto=@IDContacto And IDCliente=@IDCliente  
END
