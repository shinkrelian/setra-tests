﻿Create Procedure [dbo].[MACOMENTARIOSPROVEEDOR_Upd]
	@IDComentario char(3),
	@IDProveedor	char(6),	
	@IDFile	char(8),
	@Comentario	text,
	@Estado	char(1),
	@UserMod	char(4)	
As
	Set NoCount On	

	Update MACOMENTARIOSPROVEEDOR Set	
	IDFile=Case When LTRIM(rtrim(@IDFile))='' Then Null Else @IDFile End, 
	Comentario=@Comentario, Estado=@Estado, UserMod=@UserMod, FecMod=getdate()
	Where IDProveedor = @IDProveedor And IDComentario = @IDComentario
