﻿
--JRF-20131205-Nuevo campo para Reservas      
--JRF-20140428-Nuevos campos (Complejidad,Frecuencia,Solvencia)    
--MLL-20140529-Nuevos Campos @CoStarSoft , @NuCuentaComision    
--JRF-20140711-Nuevo campo CoTipoVenta y aumentar un caracter en el campo de Tipo.
--JHD-20141113-Nuevo campo FlTop
--JHD-20141114-Nuevos campos: FePerfilUpdate,CoUserPerfilUpdate,txPerfilInformacion,txPerfilOperatividad
--JHD-20141124- FePerfilUpdate que grabe del getdate()
--JHD-20141218- Campos txPerfilInformacion,txPerfilOperatividad aumentados a varchar(max)
--JHD-20150116- Se agregó el campo FlProveedor
--JHD-20150408- Se agregó el campo NuPosicion tinyint
--JHD-20150408- Se agregó el campo NuPosicion tinyint
--JHD-20150603-Se cambio el campo @NuCuentaComision a varchar(14)  
--HLF-20151007-Nuevos parametros  @CoTipDocIdentPax char(3), @NuDocIdentPax varchar(15),
CREATE Procedure [dbo].[MACLIENTES_Ins]         
 @RazonSocial varchar(80),        
 @RazonComercial varchar(80),        
 @Persona char(1),        
 @Nombre varchar(60),        
 @ApPaterno varchar(60),        
 @ApMaterno varchar(60),        
 @Domiciliado bit,        
 @Direccion varchar(200),        
 @Tipo char(2),        
 @Ciudad varchar(60),        
 @IDIdentidad char(3),        
 @NumIdentidad varchar(15),        
 @Telefono1 varchar(30),        
 @Telefono2 varchar(30),        
 @Celular varchar(25),        
 @Fax varchar(25),        
 @Comision decimal(6,2),        
 @Notas text,        
 @IDCiudad char(6),        
 @IDvendedor char(6),        
 @Web varchar(100),        
 @CodPostal varchar(50),        
 @Credito bit,        
 @MostrarEnReserva bit,      
 @Complejidad char(1),    
 @Frecuencia char(1),    
 @Solvencia tinyint,    
 @UserMod char(4),        
 @CoStarSoft char(11),    
 @NuCuentaComision varchar(14),
 @CoTipoVenta char(2),    
 @FlTop bit=0,
 --@FePerfilUpdate datetime,        
 @CoUserPerfilUpdate char(4)='',        
 @txPerfilInformacion varchar(max)='',        
 @txPerfilOperatividad varchar(max)='',        
 @FlProveedor bit=0,
 @NuPosicion tinyint=0,
 @CoTipDocIdentPax char(3)='',
 @NuDocIdentPax varchar(15)='',
 @pIDCliente char(6) Output        
As        
 Set NoCount On        
 Declare @IDCliente char(6)        
 Exec Correlativo_SelOutput 'MACLIENTES',1,6,@IDCliente output        
        
 INSERT INTO MACLIENTES        
           (IDCliente        
           ,RazonSocial        
           ,RazonComercial        
           ,Persona        
           ,Nombre        
           ,ApPaterno        
           ,ApMaterno        
           ,Domiciliado        
           ,Ciudad        
           ,Direccion        
           ,IDIdentidad        
           ,NumIdentidad        
           ,Tipo        
           ,Telefono1        
           ,Telefono2        
           ,Celular        
           ,Fax        
           ,Comision        
           ,Notas        
           ,IDCiudad        
           ,IDvendedor          
           ,Web        
           ,CodPostal        
           ,Credito        
           ,MostrarEnReserva      
           ,Complejidad    
           ,Frecuencia    
           ,Solvencia  
           ,CoTipoVenta  
           ,UserMod    
           ,CoStarSoft     
		   ,NuCuentaComision
		   ,FlTop
		   ,FePerfilUpdate      
		   ,CoUserPerfilUpdate     
		   ,txPerfilInformacion
		   ,txPerfilOperatividad
		   ,FlProveedor
		   ,NuPosicion
		   ,CoTipDocIdentPax 
		   ,NuDocIdentPax 
		   )   
     VALUES        
           (@IDCliente        
           ,@RazonSocial        
           ,@RazonComercial        
           ,@Persona        
           ,case when LTRIM(RTRIM(@Nombre))='' then Null Else @Nombre End        
           ,case when LTRIM(RTRIM(@ApPaterno))='' then Null Else @ApPaterno End        
           ,Case When LTRIM(RTRIM(@ApMaterno))='' then Null Else @ApMaterno End        
           ,@Domiciliado        
           ,@Ciudad        
           ,@Direccion         
           ,@IDIdentidad        
           ,Case When ltrim(rtrim(@NumIdentidad))='' Then Null Else @NumIdentidad End         
           ,@Tipo        
           ,Case When ltrim(rtrim(@Telefono1))='' Then Null Else @Telefono1 End         
           ,Case When ltrim(rtrim(@Telefono2))='' Then Null Else @Telefono2 End         
           ,Case When ltrim(rtrim(@Celular))='' Then Null Else @Celular End         
           ,Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End         
           ,Case When @Comision=0 Then Null Else @Comision End         
           ,Case When ltrim(rtrim(Cast(@Notas as varchar(max))))='' Then Null Else @Notas End         
		   ,@IDCiudad        
           ,@IDvendedor           
           ,Case When ltrim(rtrim(@Web))='' Then Null Else @Web End         
           ,Case When ltrim(rtrim(@CodPostal))='' Then Null Else @CodPostal End         
           ,@Credito        
           ,@MostrarEnReserva      
           ,@Complejidad    
		   ,@Frecuencia    
		   ,@Solvencia
		   ,@CoTipoVenta    
           ,@UserMod    
           ,case when ltrim(rtrim(@CoStarSoft))= '' then null else  @CoStarSoft end    
     ,case when ltrim(rtrim(@NuCuentaComision))= '' OR ltrim(rtrim(@NuCuentaComision))= '000000'    
    then null else  @NuCuentaComision end
		   ,@FlTop
		   --,@FePerfilUpdate      
		   --,GETDATE()
		   ,case when ltrim(rtrim(@CoUserPerfilUpdate))= '' then null else  GETDATE() end
		    ,case when ltrim(rtrim(@CoUserPerfilUpdate))= '' then null else  @CoUserPerfilUpdate end    --,CoUserPerfilUpdate     
		    ,case when ltrim(rtrim(@txPerfilInformacion))= '' then null else  @txPerfilInformacion end--,txPerfilInformacion
		    ,case when ltrim(rtrim(@txPerfilOperatividad))= '' then null else  @txPerfilOperatividad end    --,txPerfilOperatividad
		    ,isnull(@FlProveedor,cast(0 as bit))
			,Case When @NuPosicion=0 Then Null Else @NuPosicion End 

		    ,case when ltrim(rtrim(@CoTipDocIdentPax))= '' then null else  @CoTipDocIdentPax end--,txPerfilInformacion
		    ,case when ltrim(rtrim(@NuDocIdentPax))= '' then null else  @NuDocIdentPax end
			)        
 Set @pIDCliente = @IDCliente


