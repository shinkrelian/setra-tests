﻿Create Procedure dbo.MAINCLUIDO_SERVICIOS_Sel_ExistsxPK
@IDIncluido int,
@IDServicio char(8),
@pExist bit output
As
Set NoCount On
Set @pExist = 0
Select @pExist=1
from MAINCLUIDO_SERVICIOS 
where IDIncluido=@IDIncluido and IDServicio=@IDServicio
