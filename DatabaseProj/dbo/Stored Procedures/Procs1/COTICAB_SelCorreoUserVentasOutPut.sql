﻿Create Procedure dbo.COTICAB_SelCorreoUserVentasOutPut
@IDCab int,
@Modulo varchar(2),
@pCorreo varchar(200) output
As
Set NoCount On
Set @pCorreo = ''
Select @pCorreo = 
	   Case @Modulo
	   When 'VS' Then uVent.Correo
	   When 'RS' Then IsNull(uRes.Correo,'')
	   When 'OP' Then IsNull(uOpL.Correo,'')
	   When 'OC' Then IsNull(uOpC.Correo,'')
	    ELSE uVent.Correo
	   End
from COTICAB c Inner Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario
Left Join MAUSUARIOS uRes On c.IDUsuarioRes = uRes.IDUsuario
Left Join MAUSUARIOS uOpL On c.IDUsuarioOpe = uOpL.IDUsuario
Left Join MAUSUARIOS uOpC On c.IDUsuarioOpe_Cusco = uOpC.IDUsuario
Where IDCAB=@IDCab
