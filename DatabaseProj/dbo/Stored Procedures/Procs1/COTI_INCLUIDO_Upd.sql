﻿Create Procedure [dbo].[COTI_INCLUIDO_Upd]
 @IDCAB int,      
 @IDIncluido int,     
 @Orden tinyint,
 @UserMod char(4) 
 As
 Set NoCount On
 Update COTI_INCLUIDO
	Set Orden =@Orden,
		UserMod =@UserMod,
		FecMod =GETDATE()
 Where IDCAB = @IDCAB and IDIncluido=@IDIncluido
