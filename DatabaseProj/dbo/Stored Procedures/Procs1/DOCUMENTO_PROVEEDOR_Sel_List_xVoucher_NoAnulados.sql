﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
/*
--alter
ALTER PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_List]
	@NuDocumProv int=0,
	@NuVoucher int=0,
	@NuDocum varchar(10)='',
	@CoTipoDoc char(3)='',
	--@CoEstado char(2)='',
	@FlActivo bit=0
AS
BEGIN
SELECT     DOCUMENTO_PROVEEDOR.NuDocumProv,
			DOCUMENTO_PROVEEDOR.NuVoucher,
			DOCUMENTO_PROVEEDOR.NuDocum,
			NuDocum2=case when LEN(DOCUMENTO_PROVEEDOR.NuDocum)>=4 then (substring(DOCUMENTO_PROVEEDOR.NuDocum,0,4)+'-'+substring(DOCUMENTO_PROVEEDOR.NuDocum,4,len(DOCUMENTO_PROVEEDOR.NuDocum))) else DOCUMENTO_PROVEEDOR.NuDocum end,
			DOCUMENTO_PROVEEDOR.NuDocInterno,
			DOCUMENTO_PROVEEDOR.CoTipoDoc, 
          MATIPODOC.Descripcion,
          DOCUMENTO_PROVEEDOR.FeEmision,
          DOCUMENTO_PROVEEDOR.FeRecepcion,
          DOCUMENTO_PROVEEDOR.CoTipoDetraccion, 
          PorcentajeDet=(select isnull(SsTasa,0)*100 from dbo.MATIPODETRACCION where CoTipoDetraccion=DOCUMENTO_PROVEEDOR.CoTipoDetraccion),
          DOCUMENTO_PROVEEDOR.CoMoneda,
          MAMONEDAS.Descripcion AS Moneda,
          Neto=DOCUMENTO_PROVEEDOR.SsMonto- DOCUMENTO_PROVEEDOR.SsIGV,
          DOCUMENTO_PROVEEDOR.SsIGV,
          DOCUMENTO_PROVEEDOR.SsMonto, 
          --DOCUMENTO_PROVEEDOR.CoEstado,
          --Estado=case when DOCUMENTO_PROVEEDOR.CoEstado='RE' THEN 'Recepcionado' when DOCUMENTO_PROVEEDOR.CoEstado='PG' then 'Pagado' when DOCUMENTO_PROVEEDOR.CoEstado='AN' THEN 'Anulado' else '' end
          DOCUMENTO_PROVEEDOR.FlActivo
FROM         DOCUMENTO_PROVEEDOR INNER JOIN
                      VOUCHER_OPERACIONES ON DOCUMENTO_PROVEEDOR.NuVoucher = VOUCHER_OPERACIONES.IDVoucher INNER JOIN
                      MATIPODOC ON DOCUMENTO_PROVEEDOR.CoTipoDoc = MATIPODOC.IDtipodoc INNER JOIN
                      MAMONEDAS ON DOCUMENTO_PROVEEDOR.CoMoneda = MAMONEDAS.IDMoneda
WHERE     (DOCUMENTO_PROVEEDOR.NuDocumProv = @NuDocumProv or @NuDocumProv=0) AND
(DOCUMENTO_PROVEEDOR.NuVoucher = @NuVoucher or @NuVoucher=0) AND
(@NuDocum='' OR DOCUMENTO_PROVEEDOR.NuDocum LIKE '%'+@NuDocum+'%' ) AND 
                      (DOCUMENTO_PROVEEDOR.CoTipoDoc = @CoTipoDoc or @CoTipoDoc='') AND
                        (DOCUMENTO_PROVEEDOR.FlActivo = @FlActivo or @FlActivo =0)-- AND
                      --(DOCUMENTO_PROVEEDOR.CoEstado = @CoEstado or @CoEstado='')
END;

go
*/
-------------------------------------------------------------------------------------
--ALTER

/*
ALTER PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_Pk]
	@NuDocumProv int
AS
BEGIN
SELECT     DOCUMENTO_PROVEEDOR.NuDocumProv,
			DOCUMENTO_PROVEEDOR.NuVoucher,
			DOCUMENTO_PROVEEDOR.NuDocum,
			DOCUMENTO_PROVEEDOR.NuDocInterno,
			DOCUMENTO_PROVEEDOR.CoTipoDoc, 
          MATIPODOC.Descripcion,
          DOCUMENTO_PROVEEDOR.FeEmision,
          DOCUMENTO_PROVEEDOR.FeRecepcion,
          DOCUMENTO_PROVEEDOR.CoTipoDetraccion, 
          DOCUMENTO_PROVEEDOR.CoMoneda,
          MAMONEDAS.Descripcion AS Moneda,
          DOCUMENTO_PROVEEDOR.SsIGV,
          DOCUMENTO_PROVEEDOR.SsMonto, 
          DOCUMENTO_PROVEEDOR.FlActivo
FROM         DOCUMENTO_PROVEEDOR INNER JOIN
                      VOUCHER_OPERACIONES ON DOCUMENTO_PROVEEDOR.NuVoucher = VOUCHER_OPERACIONES.IDVoucher INNER JOIN
                      MATIPODOC ON DOCUMENTO_PROVEEDOR.CoTipoDoc = MATIPODOC.IDtipodoc INNER JOIN
                      MAMONEDAS ON DOCUMENTO_PROVEEDOR.CoMoneda = MAMONEDAS.IDMoneda
WHERE     (DOCUMENTO_PROVEEDOR.NuDocumProv = @NuDocumProv)
END;
GO
*/
------------------------------------------------------------------------------------------------------------------

--ALTER
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_List_xVoucher_NoAnulados]

	@NuVoucher int=0

AS
BEGIN
SELECT   *
FROM    DOCUMENTO_PROVEEDOR 
WHERE     
(DOCUMENTO_PROVEEDOR.NuVoucher = @NuVoucher or @NuVoucher=0)
                 -- AND    (DOCUMENTO_PROVEEDOR.CoEstado <>'AN')
END;
