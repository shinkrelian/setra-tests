﻿Create Procedure [dbo].[COTI_CATEGORIAHOTELES_Sel_ExisteOutput]
	@IDCAB int,
	@IDUbigeo	char(6),
	@Categoria	varchar(20),
	@IDProveedor	char(6),
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDCAB From COTI_CATEGORIAHOTELES
		WHERE IDCAB=@IDCAB	And IDUbigeo=@IDUbigeo 
		And Categoria=@Categoria And IDProveedor=@IDProveedor)
		
		Set @pExiste=1
	Else
		Set @pExiste=0
