﻿--JHD-20150930Set SsSaldo=SsSaldo-@SsTotal-@SsTotal_Docs
--HLF-20160129-FecMod=GETDATE(),UserMod=@UserMod
--select @CoEstado=CoEstado,  @NuFondoFijo=isnull(NuFondoFijo,0) from PRESUPUESTO_SOBRE Where NuPreSob=@NuPreSob
CREATE PROCEDURE [dbo].[MAFONDOFIJO_Upd_Saldo_Presupuesto]
@NuPreSob int,
@UserMod char(4)=''
AS
BEGIN
DECLARE @NuFondoFijo int=0
DECLARE @SsTotal numeric(14,2)
DECLARE @SsTotal_Docs numeric(14,2)
DECLARE @CoEstado char(2)=''

set @SsTotal=isnull((select sum(SsTotal) from PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob and copago='EFE'),0)
set @SsTotal_Docs=isnull((select sum(SsTotal) from DOCUMENTO_PROVEEDOR Where NuFondoFijo=@NuFondoFijo and FlActivo=1),0)
--set @NuFondoFijo=isnull((select NuFondoFijo from PRESUPUESTO_SOBRE Where NuPreSob=@NuPreSob),0)
--set @CoEstado=isnull((select COESTADO from PRESUPUESTO_SOBRE Where NuPreSob=@NuPreSob),'')
select @CoEstado=CoEstado, @NuFondoFijo=isnull(NuFondoFijo,0) from PRESUPUESTO_SOBRE Where NuPreSob=@NuPreSob

 IF @CoEstado='EN'
 BEGIN
		if @NuFondoFijo>0
			begin

				Update MAFONDOFIJO    
					Set SsSaldo=SsSaldo-@SsTotal-@SsTotal_Docs,
					FecMod=GETDATE(),UserMod=@UserMod
					Where NuFondoFijo=@NuFondoFijo  

			end
 END
END; 

