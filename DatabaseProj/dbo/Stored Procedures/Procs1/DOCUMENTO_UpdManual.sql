﻿--HLF-20140416-Agregando parametro @TxTitulo  
--HLF-20140421-Agregando parametro @SsIGV,@SsTotalDocumUSD  
--HLF-20140502-Agregando parametro @FeDocum
--MLL-20140526-Agregado parametro @IDTipoDocVinc
--MLL-20140526-Agregado parametro @NuDocumVinc
--MLL-20140526-Agregado parametro @CoTipoVenta
--MLL-20140530-Agregado Parametro @CoCtaContable,@FeEmisionVinc
--MLL-20140603-Agregado Parametro @CoCtroCosto
--MLL-20140609-Agregado Parametro NuFileLibre
--JHD-20140911-Agregado campo NuNtaVta
--JHD-20150609-Ampliar tamaño para @CoCtaContable
CREATE procedure [dbo].[DOCUMENTO_UpdManual]    
@NuDocum char(10),    
@IDTipoDoc char(3),    
@FeDocum smalldatetime,
@IDCab int,    
@IDCliente char(6),    
@IDMoneda char(3),    
@SsIGV numeric(8,2),  
@SsTotalDocumUSD numeric(9, 2),          
@CoEstado char(2),    
@TxTitulo varchar(100),  

@IDTipoDocVinc char(3),
@NuDocumVinc char(10),  
@CoTipoVenta char(3),
@UserMod char(4),
@CoCtaContable varchar(14)   ,
@FeEmisionVinc smalldatetime,
@CoCtroCosto char(6) ,
@NuFileLibre char(8),
@NuNtaVta CHAR(6)
As    
  
 Set NoCount On    
 Update DOCUMENTO     
 set IDCab = Case When @IDCab = 0 Then Null Else @IDCab End,    
 IDCliente = Case When LTRIM(rtrim(@IDCliente))='' then Null Else @IDCliente End,    
 IDMoneda = @IDMoneda,    
 CoEstado = @CoEstado,    
 FeDocum=@FeDocum,
 SsIGV = @SsIGV,  
 SsTotalDocumUSD = @SsTotalDocumUSD,        
  
 TxTitulo=@TxTitulo,   
 UserMod = @UserMod,    
 FecMod = GetDate() ,
 IDTipoDocVinc=Case When LTRIM(rtrim(@IDTipoDocVinc))='' then Null Else @IDTipoDocVinc End,
 NuDocumVinc=Case When LTRIM(rtrim(@NuDocumVinc))='' then Null Else @NuDocumVinc End,
 CoTipoVenta =case when ltrim(rtrim( @CoTipoVenta)) = '' then null else  @CoTipoVenta end,
 CoCtaContable= case when ltrim(rtrim(@CoCtaContable))='' then null else @CoCtaContable end,
 FeEmisionVinc=case when @FeEmisionVinc ='01/01/1900' then null else @FeEmisionVinc end  ,
 CoCtroCosto= case when ltrim(rtrim(@CoCtroCosto))='' then null else @CoCtroCosto end,
 NuFileLibre=case when ltrim(rtrim(@NuFileLibre))='' then null else @NuFileLibre end,
 NuNtaVta=case when ltrim(rtrim(@NuNtaVta))='' then null else @NuNtaVta end

 Where NuDocum = @NuDocum and IDTipoDoc =@IDTipoDoc    
  
