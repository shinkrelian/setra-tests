﻿Create Procedure [dbo].[ACOMODOPAX_FILE_SelOutput]
@IDPax int,
@CapacidadHab tinyint,
@pblnExiste bit Output
As
	If Exists(select IDCAB from ACOMODOPAX_FILE Where IDPax= @IDPax And CapacidadHab = @CapacidadHab)
		Set @pblnExiste = 1
	Else
		Set @pblnExiste = 0
