﻿Create Procedure dbo.GetUsuariosEmpleadosxJefe
@IDUsuario char(4)
As
Set NoCount On
Declare @tmpTablaResult table (IDUsuario char(4),NroOrden tinyint,Usuario varchar(15),Index2 smallint,Ruta varchar(100),Edad smallint,Nombre varchar(80),Apellidos varchar(100),Genero varchar(80),
Cumpleanios varchar(30),FechaIngreso varchar(50),CoEmpresa tinyint,TxEmpresa varchar(20),IdArea varchar(2),NoArea varchar(50),CoCargo int,TxCargo varchar(100),CoUserJefe char(4),
Correo varchar(200),Telefono varchar(20),Celular varchar(20),Anexo varchar(4),TxLema varchar(Max),Activo char(1),FecMod datetime,NuOrdenArea tinyint,IDUsuarioJefe varchar(4),
NombreJefe varchar(80),TxApellidosJefe varchar(100),MailJefe varchar(100),CoMarcador varchar(5),CoCeCos varchar(6),FlEsSupervisor bit,NumIdentidad varchar(15))

declare @tmpTabla table (IDusuario char(4),Usuario varchar(20))
declare @tmpTablaInsertada table (IDUsuarioNvl0 char(4),FlEsSupervisorNvl0 bit,UsuarioNvl0 varchar(20),IDUsuarioNvl1 char(4),UsuarioNvl1 varchar(20),
IDUsuarioNvl2 char(4),UsuarioNvl2 varchar(20),IDUsuarioNvl3 char(4),UsuarioNvl3 varchar(20),IDUsuarioNvl4 char(4),UsuarioNvl4 varchar(20),ItemOrden tinyint)
Declare @IDUsuarioNvl0 char(4)='',@FlEsSupervisorNvl0 bit=0,@UsuarioNvl0 varchar(20)='',@IDUsuarioNvl1 char(4)='',@UsuarioNvl1 varchar(20)='',
@IDUsuarioNvl2 char(4)='',@UsuarioNvl2 varchar(20)='',@IDUsuarioNvl3 char(4)='',@UsuarioNvl3 varchar(20)='',@IDUsuarioNvl4 char(4)='',@UsuarioNvl4 varchar(20)='',@ItemOrden tinyint = 0
Declare @RutaArchivoFoto varchar(200)=(select Replace(NoRutaUserPhoto,'\\wb1','wb1:81') from PARAMETRO)
Declare @IDUsuarioTmp char(4)='',@UsuarioTmp varchar(15)='',@Index2Tmp smallint=0,@RutaTmp varchar(100)='',@EdadTmp smallint,
@NombreTmp varchar(80)='',@ApellidosTmp varchar(100)='',@GeneroTmp varchar(80)='',@CumpleaniosTmp varchar(30)='',@FechaIngresoTmp varchar(50)='',@CoEmpresaTmp tinyint=0,
@TxEmpresaTmp varchar(20)='',@IdAreaTmp varchar(2)='',@NoAreaTmp varchar(50)='',@CoCargoTmp int=0,@TxCargoTmp varchar(100)='',@CoUserJefeTmp char(4),
@CorreoTmp varchar(200)='',@TelefonoTmp varchar(20)='',@CelularTmp varchar(20)='',@NuAnexoTmp varchar(4)='',@TxLemaTmp varchar(Max)='',@ActivoTmp char(1)='',
@FecModTmp datetime='01/01/1900',@NuOrdenAreaTmp tinyint=0,@IDUsuarioJefeTmp varchar(4)='',@NombreJefeTmp varchar(80)='',@TxApellidosJefeTmp varchar(100)='',
@MailJefeTmp varchar(100)='',@CoMarcadorTmp varchar(5)='',@CoCeCosTmp varchar(6)='',@FlEsSupervisorTmp bit=0,@NumIdentidadTmp varchar(15)=''

Declare @FlEsSupervisor bit = (select FlEsSupervisor from MAUSUARIOS where IDUsuario=@IDUsuario)

Insert into @tmpTablaInsertada
Select Y.IDUsuario,Y.FlEsSupervisor,y.Usuario,Y.JefeNivel1,Y.UsuarioJefeNivel1,Y.JefeNivel2,Y.UsuarioJefeNivel2,Y.JefeNivel3,Y.UsuarioJefeNivel3,
	   Y.JefeNivel4,Y.UsuarioJefeNivel4,Case When @IDUsuario='0073' Then Y.ItemOrden Else Y.ItemOrden-1 End as ItemOrden
	from (
	Select *,
		Case When X.JefeNivel1 is null and x.JefeNivel2 is null and x.JefeNivel3 is null and x.JefeNivel4 is null then  1 else 
			Case When x.JefeNivel2 is null and x.JefeNivel3 is null and x.JefeNivel4 is null then 2 else Case When x.JefeNivel3 is null and x.JefeNivel4 is null then 3 else 
			    Case When x.JefeNivel4 is null then 4 else 5 End End End End as ItemOrden 
	from (
	Select u.IDUsuario,u.FlEsSupervisor,u.Usuario, u.CoUserJefe As JefeNivel1,j1.Usuario as UsuarioJefeNivel1,
	j1.CoUserJefe As JefeNivel2,j2.Usuario as UsuarioJefeNivel2,
	j2.CoUserJefe As JefeNivel3,j3.Usuario as UsuarioJefeNivel3,
	j3.CoUserJefe As JefeNivel4,j4.Usuario as UsuarioJefeNivel4
	from MAUSUARIOS u Left Join MAUSUARIOS j1 On j1.IDUsuario=u.CoUserJefe
	Left Join MAUSUARIOS j2 On j2.IDUsuario=j1.CoUserJefe
	Left Join MAUSUARIOS j3 On j3.IDUsuario=j2.CoUserJefe
	Left Join MAUSUARIOS j4 On j4.IDUsuario=j3.CoUserJefe
	where u.Activo='A' And u.FlTrabajador_Activo=1 And u.CoEmpresa=1) as X
	Where (x.JefeNivel4 = @IDUsuario Or x.JefeNivel3 = @IDUsuario Or x.JefeNivel2=@IDUsuario Or x.JefeNivel1=@IDUsuario Or x.IDUsuario=@IDUsuario)
) as Y
Where @FlEsSupervisor=1
Order By Y.ItemOrden,Y.FlEsSupervisor desc

Declare curNivelesUser cursor for
select * from @tmpTablaInsertada
Open curNivelesUser
Begin
Fetch Next from curNivelesUser into @IDUsuarioNvl0,@FlEsSupervisorNvl0,@UsuarioNvl0,@IDUsuarioNvl1,@UsuarioNvl1,@IDUsuarioNvl2,@UsuarioNvl2,
@IDUsuarioNvl3,@UsuarioNvl3,@IDUsuarioNvl4,@UsuarioNvl4,@ItemOrden
while @@FETCH_STATUS =0 
Begin
	Select @IDUsuarioTmp=u.IDUsuario,
			@UsuarioTmp= u.Usuario,
			@Index2Tmp =ROW_NUMBER()Over (Order By u.IDUsuario)-1,
			@RutaTmp = Replace(@RutaArchivoFoto+u.Usuario+'\'+IsNull(u.NoArchivoFoto,''),'\','/'),
			@EdadTmp = Year(Getdate())-Year(IsNull(u.FechaNac,getdate())),
			@NombreTmp = u.Nombre,
			@ApellidosTmp = IsNull(u.TxApellidos,''),
			@GeneroTmp = Case When u.CoSexo='F' Then 'female' else 'male' End,
			@CumpleaniosTmp = IsNull(Substring(convert(char(10),u.FechaNac,103),1,2)+' de '+ DATENAME(MONTH,u.FechaNac) ,''),
			@FechaIngresoTmp = IsNull(Substring(convert(char(10),u.FecIngreso,103),1,2)+' de '+ DATENAME(MONTH,u.FecIngreso)+' de '+ Substring(convert(char(10),u.FecIngreso,103),7,10),''),
			@CoEmpresaTmp = IsNull(u.CoEmpresa,0),
			@TxEmpresaTmp = IsNull(e.TxEmpresa,''),
			@IdAreaTmp = u.IdArea,
			@NoAreaTmp = r.NoArea,
			@CoCargoTmp = IsNull(u.CoCargo,0),
			@TxCargoTmp = IsNull(c.TxCargo,''),
			@CoUserJefeTmp = IsNull(u.CoUserJefe,''),
			@CorreoTmp = u.Correo,
			@TelefonoTmp= IsNull(u.Telefono,''),
			@CelularTmp =IsNull(u.Celular,''),
			@NuAnexoTmp = IsNull(u.NuAnexo,''),
			@TxLemaTmp = IsNull(u.TxLema,''),
			@ActivoTmp = u.Activo,
			@FecModTmp = u.FecMod,
			@NuOrdenAreaTmp = isnull(u.NuOrdenArea,0),
			@IDUsuarioJefeTmp = IsNull(ujefe.IDUsuario,''),
			@NombreJefeTmp =  IsNull(ujefe.Nombre,''),
			@TxApellidosJefeTmp = IsNuLL(ujefe.TxApellidos,''),
			@MailJefeTmp = IsNull(ujefe.Correo,'') ,
			@CoMarcadorTmp = IsNull(u.CoMarcador,''),
			@CoCeCosTmp = r.CoCeCos,
			@FlEsSupervisorTmp = u.FlEsSupervisor,
			@NumIdentidadTmp = IsNull(u.NumIdentidad,'')
		from MAUSUARIOS u Left Join RH_AREA r On u.IdArea=r.CoArea
		Left Join MAEMPRESA e On u.CoEmpresa=e.CoEmpresa
		Left Join RH_CARGO c On u.CoCargo=c.CoCargo
		Left Join MAUSUARIOS ujefe On u.CoUserJefe=ujefe.IDUsuario
		Where  u.IDUsuario=@IDUsuarioNvl0

		--select @IDUsuarioTmp,@UsuarioTmp,@ItemOrden
		if @FlEsSupervisorNvl0 = 1 --And @ItemOrden = 1
		Begin
			 Insert into @tmpTablaResult values (@IDUsuarioTmp,1,
			   @UsuarioTmp,@Index2Tmp,@RutaTmp,@EdadTmp,@NombreTmp,@ApellidosTmp,@GeneroTmp,@CumpleaniosTmp,
			   @FechaIngresoTmp,@CoEmpresaTmp,@TxEmpresaTmp,@IdAreaTmp,@NoAreaTmp,@CoCargoTmp,@TxCargoTmp,@CoUserJefeTmp,@CorreoTmp,@TelefonoTmp,@CelularTmp,@NuAnexoTmp,@TxLemaTmp,
			   @ActivoTmp,@FecModTmp,@NuOrdenAreaTmp,@IDUsuarioJefeTmp,@NombreJefeTmp,@TxApellidosJefeTmp,@MailJefeTmp,@CoMarcadorTmp,@CoCeCosTmp,@FlEsSupervisorTmp,
			   @NumIdentidadTmp)

			--2ndo Cursor
			Declare @IDUsuarioNvl0_1 char(4)='',@FlEsSupervisorNvl0_1 bit=0,@UsuarioNvl0_1 varchar(20)='',@IDUsuarioNvl1_1 char(4)='',@UsuarioNvl1_1 varchar(20)='', @IDUsuarioNvl2_1 char(4)='',
			@UsuarioNvl2_1 varchar(20)='',@IDUsuarioNvl3_1 char(4)='',@UsuarioNvl3_1 varchar(20)='',@IDUsuarioNvl4_1 char(4)='',@UsuarioNvl4_1 varchar(20)='',@ItemOrden_1 tinyint = 0

			--if @ItemOrden = 1
			--Begin
				Declare curNivel1 cursor for
				select * from @tmpTablaInsertada Where IDUsuarioNvl1=@IDUsuarioNvl0
				Open curNivel1 
				Begin
				Fetch Next From curNivel1 into @IDUsuarioNvl0_1,@FlEsSupervisorNvl0_1,@UsuarioNvl0_1,@IDUsuarioNvl1_1,@UsuarioNvl1_1,@IDUsuarioNvl2_1,@UsuarioNvl2_1,@IDUsuarioNvl3_1,
											   @UsuarioNvl3_1,@IDUsuarioNvl4_1,@UsuarioNvl4_1,@ItemOrden_1
				While @@FETCH_STATUS=0
				Begin
					Select  @IDUsuarioTmp=u.IDUsuario,
							@UsuarioTmp= u.Usuario,
							@Index2Tmp =ROW_NUMBER()Over (Order By u.IDUsuario)-1,
							@RutaTmp = Replace(@RutaArchivoFoto+u.Usuario+'\'+IsNull(u.NoArchivoFoto,''),'\','/'),
							@EdadTmp = Year(Getdate())-Year(IsNull(u.FechaNac,getdate())),
							@NombreTmp = u.Nombre,
							@ApellidosTmp = IsNull(u.TxApellidos,''),
							@GeneroTmp = Case When u.CoSexo='F' Then 'female' else 'male' End,
							@CumpleaniosTmp = IsNull(Substring(convert(char(10),u.FechaNac,103),1,2)+' de '+ DATENAME(MONTH,u.FechaNac) ,''),
							@FechaIngresoTmp = IsNull(Substring(convert(char(10),u.FecIngreso,103),1,2)+' de '+ DATENAME(MONTH,u.FecIngreso)+' de '+ Substring(convert(char(10),u.FecIngreso,103),7,10),''),
							@CoEmpresaTmp = IsNull(u.CoEmpresa,0),
							@TxEmpresaTmp = IsNull(e.TxEmpresa,''),
							@IdAreaTmp = u.IdArea,
							@NoAreaTmp = r.NoArea,
							@CoCargoTmp = IsNull(u.CoCargo,0),
							@TxCargoTmp = IsNull(c.TxCargo,''),
							@CoUserJefeTmp = IsNull(u.CoUserJefe,''),
							@CorreoTmp = u.Correo,
							@TelefonoTmp= IsNull(u.Telefono,''),
							@CelularTmp =IsNull(u.Celular,''),
							@NuAnexoTmp = IsNull(u.NuAnexo,''),
							@TxLemaTmp = IsNull(u.TxLema,''),
							@ActivoTmp = u.Activo,
							@FecModTmp = u.FecMod,
							@NuOrdenAreaTmp = isnull(u.NuOrdenArea,0),
							@IDUsuarioJefeTmp = IsNull(ujefe.IDUsuario,''),
							@NombreJefeTmp =  IsNull(ujefe.Nombre,''),
							@TxApellidosJefeTmp = IsNuLL(ujefe.TxApellidos,''),
							@MailJefeTmp = IsNull(ujefe.Correo,'') ,
							@CoMarcadorTmp = IsNull(u.CoMarcador,''),
							@CoCeCosTmp = r.CoCeCos,
							@FlEsSupervisorTmp = u.FlEsSupervisor,
							@NumIdentidadTmp = IsNull(u.NumIdentidad,'')
						from MAUSUARIOS u Left Join RH_AREA r On u.IdArea=r.CoArea
						Left Join MAEMPRESA e On u.CoEmpresa=e.CoEmpresa
						Left Join RH_CARGO c On u.CoCargo=c.CoCargo
						Left Join MAUSUARIOS ujefe On u.CoUserJefe=ujefe.IDUsuario
						Where  u.IDUsuario=@IDUsuarioNvl0_1

					Insert into @tmpTablaResult values (@IDUsuarioTmp,2,@UsuarioTmp,@Index2Tmp,@RutaTmp,@EdadTmp,@NombreTmp,@ApellidosTmp,@GeneroTmp,@CumpleaniosTmp,
								@FechaIngresoTmp,@CoEmpresaTmp,@TxEmpresaTmp,@IdAreaTmp,@NoAreaTmp,@CoCargoTmp,@TxCargoTmp,@CoUserJefeTmp,@CorreoTmp,@TelefonoTmp,@CelularTmp,@NuAnexoTmp,@TxLemaTmp,
								@ActivoTmp,@FecModTmp,@NuOrdenAreaTmp,@IDUsuarioJefeTmp,@NombreJefeTmp,@TxApellidosJefeTmp,@MailJefeTmp,@CoMarcadorTmp,@CoCeCosTmp,@FlEsSupervisorTmp,
								@NumIdentidadTmp)
					
						--3er Cursor
						Declare @IDUsuarioNvl0_2 char(4)='',@FlEsSupervisorNvl0_2 bit=0,@UsuarioNvl0_2 varchar(20)='',@IDUsuarioNvl1_2 char(4)='',@UsuarioNvl1_2 varchar(20)='', @IDUsuarioNvl2_2 char(4)='',
						@UsuarioNvl2_2 varchar(20)='',@IDUsuarioNvl3_2 char(4)='',@UsuarioNvl3_2 varchar(20)='',@IDUsuarioNvl4_2 char(4)='',@UsuarioNvl4_2 varchar(20)='',@ItemOrden_2 tinyint = 0

						Declare curNivel2 cursor for
						select * from @tmpTablaInsertada where IDUsuarioNvl1=@IDUsuarioNvl0_1
						Open curNivel2
						Fetch Next From curNivel2 into @IDUsuarioNvl0_2,@FlEsSupervisorNvl0_2,@UsuarioNvl0_2,@IDUsuarioNvl1_2,@UsuarioNvl1_2,@IDUsuarioNvl2_2,@UsuarioNvl2_2,@IDUsuarioNvl3_2,
													   @UsuarioNvl3_2,@IDUsuarioNvl4_2,@UsuarioNvl4_2,@ItemOrden_2
						While @@FETCH_STATUS = 0
						Begin

						Select  @IDUsuarioTmp=u.IDUsuario,
								@UsuarioTmp= u.Usuario,
								@Index2Tmp =ROW_NUMBER()Over (Order By u.IDUsuario)-1,
								@RutaTmp = Replace(@RutaArchivoFoto+u.Usuario+'\'+IsNull(u.NoArchivoFoto,''),'\','/'),
								@EdadTmp = Year(Getdate())-Year(IsNull(u.FechaNac,getdate())),
								@NombreTmp = u.Nombre,
								@ApellidosTmp = IsNull(u.TxApellidos,''),
								@GeneroTmp = Case When u.CoSexo='F' Then 'female' else 'male' End,
								@CumpleaniosTmp = IsNull(Substring(convert(char(10),u.FechaNac,103),1,2)+' de '+ DATENAME(MONTH,u.FechaNac) ,''),
								@FechaIngresoTmp = IsNull(Substring(convert(char(10),u.FecIngreso,103),1,2)+' de '+ DATENAME(MONTH,u.FecIngreso)+
								' de '+ Substring(convert(char(10),u.FecIngreso,103),7,10),''),
								@CoEmpresaTmp = IsNull(u.CoEmpresa,0),
								@TxEmpresaTmp = IsNull(e.TxEmpresa,''),
								@IdAreaTmp = u.IdArea,
								@NoAreaTmp = r.NoArea,
								@CoCargoTmp = IsNull(u.CoCargo,0),
								@TxCargoTmp = IsNull(c.TxCargo,''),
								@CoUserJefeTmp = IsNull(u.CoUserJefe,''),
								@CorreoTmp = u.Correo,
								@TelefonoTmp= IsNull(u.Telefono,''),
								@CelularTmp =IsNull(u.Celular,''),
								@NuAnexoTmp = IsNull(u.NuAnexo,''),
								@TxLemaTmp = IsNull(u.TxLema,''),
								@ActivoTmp = u.Activo,
								@FecModTmp = u.FecMod,
								@NuOrdenAreaTmp = isnull(u.NuOrdenArea,0),
								@IDUsuarioJefeTmp = IsNull(ujefe.IDUsuario,''),
								@NombreJefeTmp =  IsNull(ujefe.Nombre,''),
								@TxApellidosJefeTmp = IsNuLL(ujefe.TxApellidos,''),
								@MailJefeTmp = IsNull(ujefe.Correo,'') ,
								@CoMarcadorTmp = IsNull(u.CoMarcador,''),
								@CoCeCosTmp = r.CoCeCos,
								@FlEsSupervisorTmp = u.FlEsSupervisor,
								@NumIdentidadTmp = IsNull(u.NumIdentidad,'')
							from MAUSUARIOS u Left Join RH_AREA r On u.IdArea=r.CoArea
							Left Join MAEMPRESA e On u.CoEmpresa=e.CoEmpresa
							Left Join RH_CARGO c On u.CoCargo=c.CoCargo
							Left Join MAUSUARIOS ujefe On u.CoUserJefe=ujefe.IDUsuario
							Where  u.IDUsuario=@IDUsuarioNvl0_2

							Insert into @tmpTablaResult values (@IDUsuarioTmp,3,@UsuarioTmp,@Index2Tmp,@RutaTmp,@EdadTmp,@NombreTmp,@ApellidosTmp,@GeneroTmp,@CumpleaniosTmp,
								@FechaIngresoTmp,@CoEmpresaTmp,@TxEmpresaTmp,@IdAreaTmp,@NoAreaTmp,@CoCargoTmp,@TxCargoTmp,@CoUserJefeTmp,@CorreoTmp,@TelefonoTmp,@CelularTmp,@NuAnexoTmp,@TxLemaTmp,
								@ActivoTmp,@FecModTmp,@NuOrdenAreaTmp,@IDUsuarioJefeTmp,@NombreJefeTmp,@TxApellidosJefeTmp,@MailJefeTmp,@CoMarcadorTmp,@CoCeCosTmp,@FlEsSupervisorTmp,
								@NumIdentidadTmp)

							
							--4to Cursor
							Declare @IDUsuarioNvl0_3 char(4)='',@FlEsSupervisorNvl0_3 bit=0,@UsuarioNvl0_3 varchar(20)='',@IDUsuarioNvl1_3 char(4)='',@UsuarioNvl1_3 varchar(20)='', 
							@IDUsuarioNvl2_3 char(4)='',@UsuarioNvl2_3 varchar(20)='',@IDUsuarioNvl3_3 char(4)='',@UsuarioNvl3_3 varchar(20)='',@IDUsuarioNvl4_3 char(4)='',
							@UsuarioNvl4_3 varchar(20)='',@ItemOrden_3 tinyint = 0
							Declare curNivel3 cursor for
							select * from @tmpTablaInsertada where IDUsuarioNvl1=@IDUsuarioNvl0_2
							Open curNivel3
							Fetch Next From curNivel3 into @IDUsuarioNvl0_3,@FlEsSupervisorNvl0_3,@UsuarioNvl0_3,@IDUsuarioNvl1_3,@UsuarioNvl1_3,@IDUsuarioNvl2_3,@UsuarioNvl2_3,@IDUsuarioNvl3_3,
								 						       @UsuarioNvl3_3,@IDUsuarioNvl4_3,@UsuarioNvl4_3,@ItemOrden_3
							While @@FETCH_STATUS = 0
							Begin
								Select  @IDUsuarioTmp=u.IDUsuario,
										@UsuarioTmp= u.Usuario,
										@Index2Tmp =ROW_NUMBER()Over (Order By u.IDUsuario)-1,
										@RutaTmp = Replace(@RutaArchivoFoto+u.Usuario+'\'+IsNull(u.NoArchivoFoto,''),'\','/'),
										@EdadTmp = Year(Getdate())-Year(IsNull(u.FechaNac,getdate())),
										@NombreTmp = u.Nombre,
										@ApellidosTmp = IsNull(u.TxApellidos,''),
										@GeneroTmp = Case When u.CoSexo='F' Then 'female' else 'male' End,
										@CumpleaniosTmp = IsNull(Substring(convert(char(10),u.FechaNac,103),1,2)+' de '+ DATENAME(MONTH,u.FechaNac) ,''),
										@FechaIngresoTmp = IsNull(Substring(convert(char(10),u.FecIngreso,103),1,2)+' de '+ DATENAME(MONTH,u.FecIngreso)+
										' de '+ Substring(convert(char(10),u.FecIngreso,103),7,10),''),
										@CoEmpresaTmp = IsNull(u.CoEmpresa,0),
										@TxEmpresaTmp = IsNull(e.TxEmpresa,''),
										@IdAreaTmp = u.IdArea,
										@NoAreaTmp = r.NoArea,
										@CoCargoTmp = IsNull(u.CoCargo,0),
										@TxCargoTmp = IsNull(c.TxCargo,''),
										@CoUserJefeTmp = IsNull(u.CoUserJefe,''),
										@CorreoTmp = u.Correo,
										@TelefonoTmp= IsNull(u.Telefono,''),
										@CelularTmp =IsNull(u.Celular,''),
										@NuAnexoTmp = IsNull(u.NuAnexo,''),
										@TxLemaTmp = IsNull(u.TxLema,''),
										@ActivoTmp = u.Activo,
										@FecModTmp = u.FecMod,
										@NuOrdenAreaTmp = isnull(u.NuOrdenArea,0),
										@IDUsuarioJefeTmp = IsNull(ujefe.IDUsuario,''),
										@NombreJefeTmp =  IsNull(ujefe.Nombre,''),
										@TxApellidosJefeTmp = IsNuLL(ujefe.TxApellidos,''),
										@MailJefeTmp = IsNull(ujefe.Correo,'') ,
										@CoMarcadorTmp = IsNull(u.CoMarcador,''),
										@CoCeCosTmp = r.CoCeCos,
										@FlEsSupervisorTmp = u.FlEsSupervisor,
										@NumIdentidadTmp = IsNull(u.NumIdentidad,'')
									from MAUSUARIOS u Left Join RH_AREA r On u.IdArea=r.CoArea
									Left Join MAEMPRESA e On u.CoEmpresa=e.CoEmpresa
									Left Join RH_CARGO c On u.CoCargo=c.CoCargo
									Left Join MAUSUARIOS ujefe On u.CoUserJefe=ujefe.IDUsuario
									Where  u.IDUsuario=@IDUsuarioNvl0_3

								Insert into @tmpTablaResult values (@IDUsuarioTmp,4,@UsuarioTmp,@Index2Tmp,@RutaTmp,@EdadTmp,@NombreTmp,@ApellidosTmp,@GeneroTmp,@CumpleaniosTmp,
								@FechaIngresoTmp,@CoEmpresaTmp,@TxEmpresaTmp,@IdAreaTmp,@NoAreaTmp,@CoCargoTmp,@TxCargoTmp,@CoUserJefeTmp,@CorreoTmp,@TelefonoTmp,@CelularTmp,@NuAnexoTmp,@TxLemaTmp,
								@ActivoTmp,@FecModTmp,@NuOrdenAreaTmp,@IDUsuarioJefeTmp,@NombreJefeTmp,@TxApellidosJefeTmp,@MailJefeTmp,@CoMarcadorTmp,@CoCeCosTmp,@FlEsSupervisorTmp,
								@NumIdentidadTmp)

								--5nto Cursor
								Declare @IDUsuarioNvl0_4 char(4)='',@FlEsSupervisorNvl0_4 bit=0,@UsuarioNvl0_4 varchar(20)='',@IDUsuarioNvl1_4 char(4)='',@UsuarioNvl1_4 varchar(20)='', 
								@IDUsuarioNvl2_4 char(4)='',@UsuarioNvl2_4 varchar(20)='',@IDUsuarioNvl3_4 char(4)='',@UsuarioNvl3_4 varchar(20)='',@IDUsuarioNvl4_4 char(4)='',
								@UsuarioNvl4_4 varchar(20)='',@ItemOrden_4 tinyint = 0

								
								--select * from @tmpTablaInsertada where IDUsuarioNvl1 = @IDUsuarioTmp
								--Fin 5nto Cursor

								--if @IDUsuarioNvl0_2 In ('0085','0105')
								--Begin
								--Insert into @tmpTablaResult values (@IDUsuarioTmp,4,@UsuarioTmp,@Index2Tmp,@RutaTmp,@EdadTmp,@NombreTmp,@ApellidosTmp,@GeneroTmp,@CumpleaniosTmp,
								--@FechaIngresoTmp,@CoEmpresaTmp,@TxEmpresaTmp,@IdAreaTmp,@NoAreaTmp,@CoCargoTmp,@TxCargoTmp,@CoUserJefeTmp,@CorreoTmp,@TelefonoTmp,@CelularTmp,@NuAnexoTmp,@TxLemaTmp,
								--@ActivoTmp,@FecModTmp,@NuOrdenAreaTmp,@IDUsuarioJefeTmp,@NombreJefeTmp,@TxApellidosJefeTmp,@MailJefeTmp,@CoMarcadorTmp,@CoCeCosTmp,@FlEsSupervisorTmp,
								--@NumIdentidadTmp)
								--End
								--if @IDUsuarioNvl0_2 In ('0051')
								--Begin
								--	Insert into @tmpTablaResult(IDUsuario) values (null)
								--End

								Fetch Next From curNivel3 into @IDUsuarioNvl0_3,@FlEsSupervisorNvl0_3,@UsuarioNvl0_3,@IDUsuarioNvl1_3,@UsuarioNvl1_3,@IDUsuarioNvl2_3,@UsuarioNvl2_3,@IDUsuarioNvl3_3,
								 						       @UsuarioNvl3_3,@IDUsuarioNvl4_3,@UsuarioNvl4_3,@ItemOrden_3
							End
							Close curNivel3
							DealLocate curNivel3
							--Fin 4to Cursor


							Fetch Next From curNivel2 into @IDUsuarioNvl0_2,@FlEsSupervisorNvl0_2,@UsuarioNvl0_2,@IDUsuarioNvl1_2,@UsuarioNvl1_2,@IDUsuarioNvl2_2,@UsuarioNvl2_2,@IDUsuarioNvl3_2,
														   @UsuarioNvl3_2,@IDUsuarioNvl4_2,@UsuarioNvl4_2,@ItemOrden_2
						End
						Close curNivel2
						DealLocate curNivel2
						--fin 3er Cursor
						

					Fetch Next From curNivel1 into @IDUsuarioNvl0_1,@FlEsSupervisorNvl0_1,@UsuarioNvl0_1,@IDUsuarioNvl1_1,@UsuarioNvl1_1,@IDUsuarioNvl2_1,@UsuarioNvl2_1,@IDUsuarioNvl3_1,
								@UsuarioNvl3_1,@IDUsuarioNvl4_1,@UsuarioNvl4_1,@ItemOrden_1
				End
				End
				Close curNivel1
				DealLocate curNivel1
			End
		End
	
	Goto FinCursor
	Fetch Next from curNivelesUser into @IDUsuarioNvl0,@FlEsSupervisorNvl0,@UsuarioNvl0,@IDUsuarioNvl1,@UsuarioNvl1,@IDUsuarioNvl2,@UsuarioNvl2,@IDUsuarioNvl3,
	@UsuarioNvl3,@IDUsuarioNvl4,@UsuarioNvl4,@ItemOrden
End
FinCursor:
--End
Close curNivelesUser
DealLocate curNivelesUser

--select * from MAUSUARIOS
--select * from MAUSUARIOS where IDUsuario='0073'

--select * from MAUSUARIOS u where u.Activo='A' And u.FlTrabajador_Activo=1 And u.CoEmpresa=1

select * from @tmpTablaResult --where NroOrden=4
Where IDUsuario <> @IDUsuario
--Order By IDUsuarioJefe, NroOrden
