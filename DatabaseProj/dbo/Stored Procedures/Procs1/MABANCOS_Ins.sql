﻿CREATE Procedure dbo.MABANCOS_Ins   
 @Descripcion varchar(60),  
 @Moneda char(1),  
 @NroCuenta varchar(20),  
 @Sigla varchar(8),  
 @CtaInter varchar(20),  
 @CtaCon varchar(20),  
 @Direccion varchar(100),  
 @SWIFT varchar(20),  
 @UserMod char(4),  
 @pIDBanco char(3) Output  
As  
  
 Set NoCount On  
   
 Declare @IDBanco char(3)   
 Exec Correlativo_SelOutput 'MABANCOS',1,3,@IDBanco output   
  
 INSERT INTO MABANCOS  
           ([IDBanco]  
           ,[Descripcion]  
           ,[Moneda]  
           ,[NroCuenta]  
           ,[Sigla]  
           ,[CtaInter]  
           ,[CtaCon]  
           ,[Direccion]  
           ,[SWIFT]  
           ,[UserMod]  
           ,[FecMod]
           )  
     VALUES  
           (@IDBanco,  
            @Descripcion ,  
            @Moneda ,  
            Case When LTRIM(RTRIM(@NroCuenta)) = '' Then Null Else @NroCuenta End,  
            Case When LTRIM(RTRIM(@Sigla)) = '' Then Null Else @Sigla  End,  
            Case When LTRIM(RTRIM(@CtaInter)) = '' Then Null Else @CtaInter End,  
            Case When LTRIM(RTRIM(@CtaCon)) = '' Then Null Else @CtaCon End,  
            Case When LTRIM(RTRIM(@Direccion)) = '' Then Null Else @Direccion End,  
            Case When LTRIM(RTRIM(@SWIFT)) = '' Then Null Else @SWIFT End,  
            @UserMod,
            GETDATE() )  
              
 Set @pIDBanco = @IDBanco   
