﻿--JRF-20131221-Agregar la columna de RazonSocial    
--JRF-20140711-Agregar filtro por el nuevo campo CoTipoVenta  
CREATE Procedure [dbo].[MACLIENTES_Sel_GeneracionExcel]          
 @RazonComercial varchar(60),          
 @IDPais char(6),          
 @Contacto varchar(100),          
 @Top tinyint,        
 @Tipo char(2),  
 @CoTipoVenta char(2)     
As      
 Set NoCount On          
 SELECT RazonComercial As [Razón Comercial],RazonSocial,Direccion,          
 Ciudad,Pais,Status,Titulo,Nombres,Apellidos,Cargo,Email,Notas      
 FROM           
 (          
 SELECT row_number() over (order by RazonComercial) as NroFila ,X.*           
 FROM           
 (            
 Select Distinct          
 RazonComercial,     
 RazonSocial,           
 c.Direccion,          
 c.Ciudad,        
 up.Descripcion as Pais,           
 Case c.Tipo When 'I' then 'INTERESADO' When 'C' then 'CLIENTE' End as Status,           
 isnull(cc.Titulo,'') Titulo,      
 isnull(cc.Nombres,'') Nombres,      
 isnull(cc.Apellidos,'') Apellidos,      
 isnull(cc.Cargo,'') Cargo,      
 isnull(cc.Email,'') Email,      
 isnull(cc.Notas,'') Notas      
 From MACLIENTES c         
 Left Join MAUBIGEO up On c.IDCiudad=up.IDubigeo          
 Left Join MACONTACTOSCLIENTE cc On c.IDCliente=cc.IDCliente          
 Where           
 (c.RazonComercial Like '%'+@RazonComercial+'%' Or ltrim(rtrim(@RazonComercial))='')           
 And c.Activo='A'          
 And (ltrim(rtrim(@IDPais))='' Or c.IDCiudad=@IDPais)          
 And ((cc.Nombres+ ' ' + cc.Apellidos Like '%'+@Contacto+'%' Or ltrim(rtrim(@Contacto))=''))          
 And (c.Tipo = @Tipo Or ltrim(rtrim(@Tipo))='')        
 And (c.CoTipoVenta = @CoTipoVenta)  
 ) AS X           
 ) AS Y           
 WHERE (Y.NROFILA<=@Top Or @Top=0)          
 Order by RazonComercial           
