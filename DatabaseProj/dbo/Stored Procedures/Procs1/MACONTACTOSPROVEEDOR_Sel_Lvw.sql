﻿Create Procedure [dbo].[MACONTACTOSPROVEEDOR_Sel_Lvw]
	@IDContacto	char(3),
	@IDProveedor	char(6),
	@Nombre varchar(80)
As
	Set NoCount On
	
	Select Nombres+' '+Apellidos as Nombre
	From MACONTACTOSPROVEEDOR
	Where (LTRIM(RTRIM(@Nombre))='' Or (Nombres+' '+Apellidos Like '%'+@Nombre+'%'))
	And (ltrim(rtrim(@IDContacto))='' Or (IDContacto <>@IDContacto And  IDProveedor <>@IDProveedor))
	Order by 1
