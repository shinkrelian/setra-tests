﻿
CREATE Procedure [dbo].[CORREOFILE_UpdLeido]
	@NuCorreo int,
	@FlLeido bit,
	@UserMod char(4)
As
	Set NoCount On
	
	Update .BDCORREOSETRA..CORREOFILE Set FlLeido=@FlLeido, FecMod=GETDATE(), UserMod=@UserMod
	Where NuCorreo=@NuCorreo

