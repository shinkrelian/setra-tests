﻿CREATE Procedure [dbo].[COTIPAX_Sel_RptDetalleServicio] -- 191
@IDCab int
as	
	Set NoCount On
	
	select distinct	Cdt.Iddet,dbo.FnNombres_Pax(Cdt.IDDET) As Pax,Dia,Servicio,Ctpx.Total,Cdt.IDServicio
	from COTIDET Cdt Left Join COTIDET_PAX Cdtpx On Cdt.IDDET= Cdtpx.IDDet
	     Left Join COTIPAX Ctpx On Ctpx.IDPax= Cdtpx.IDPax
	Where Cdt.IDCAB=@IDCab  And Ctpx.Nombres is not null -- and Cdtpx.IDPax=1035
	--group by  Cdt.IDDET,Ctpx.Nombres,Dia,Servicio,Ctpx.Total,Cdt.IDServicio
	Order By 2 desc--Cdt.IDServicio,1,3 desc
