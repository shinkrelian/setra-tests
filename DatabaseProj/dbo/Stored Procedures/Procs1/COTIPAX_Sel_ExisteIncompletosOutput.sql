﻿--HLF-20150323-And (FnNumeroEncontradoenCadena(Nombres)=1 Or FnNumeroEncontradoenCadena(Apellidos)=1 )
--HLF-20150401-Or FecNacimiento is null
CREATE Procedure [dbo].[COTIPAX_Sel_ExisteIncompletosOutput]
	@IDCab	int,
	@pExiste	bit Output
As
	Set Nocount On
	
	Set @pExiste=0
	
	If Exists(
		Select IDPax From COTIPAX Where IDCab=@IDCab And (
		(CHARINDEX('Pax',Nombres)>0 And dbo.FnNumeroEncontradoenCadena(Nombres)=1) Or
		(CHARINDEX('Pax',Apellidos)>0 And dbo.FnNumeroEncontradoenCadena(Apellidos)=1) Or
		len(isnull(NumIdentidad,''))<6 Or
		FecNacimiento is null
		)
			)
		
		Set @pExiste=1

