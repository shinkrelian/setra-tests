﻿--JHD-20150605-Se agregaron los campo FLEsReembolso,FeEmision_Reembolso,CoTipoDoc_Reembolso,NuDocum_Reembolso
--JHD-20150608-Nuevo campo NuFondoFijo_Reembolso en la inserción
--JHD-20150608-Nuevo campo @NuCodigo en la inserción
--JHD-20150608-Se agrego campo CoPrefijo
--JHD-20150608-Se agrego campo CoAnio
--JHD-20150627-Se agrego campo CoSap
--JHD-20150627-DECLARE @NuCodigo2 varchar(14)=(SELECT RIGHT('00000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,5)+1 as int)),1) as char(5)))),5)  as correlativo FROM MAFONDOFIJO where  rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
--JHD-20160111-declare @CoAnio2 char(4)=CAST(YEAR(GETDATE()) AS CHAR(4))
--JHD-20160111-DECLARE @NuCodigo varchar(14)=(SELECT RIGHT('000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,3)+1 as int)),1) as char(3)))),3) +'-'+substring(rtrim(ltrim(@CoAnio2)),3,4) as correlativo FROM MAFONDOFIJO where coanio=rtrim(ltrim(@CoAnio)) and rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
CREATE Procedure [dbo].[MAFONDOFIJO_Reembolsar]      
@NuFondoFijo_Origen int,          
@FeEmision_Reembolso smalldatetime='01/01/1900',
@CoTipoDoc_Reembolso char(3)='',
@NuDocum_Reembolso varchar(30)='',
@UserMod char(4)      
As      
Set NoCount On      
BEGIN
declare @CoPrefijo char(4)=(select coprefijo from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo_Origen)
declare @CoAnio char(4)=(select CoAnio from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo_Origen)
declare @CoAnio2 char(4)=CAST(YEAR(GETDATE()) AS CHAR(4))
--DECLARE @NuCodigo varchar(14)=(SELECT RIGHT('000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,3)+1 as int)),1) as char(3)))),3) +'-'+substring(rtrim(ltrim(@CoAnio)),3,4) as correlativo FROM MAFONDOFIJO where coanio=rtrim(ltrim(@CoAnio)) and rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
DECLARE @NuCodigo varchar(14)=(SELECT RIGHT('000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,3)+1 as int)),1) as char(3)))),3) +'-'+substring(rtrim(ltrim(@CoAnio2)),3,4) as correlativo FROM MAFONDOFIJO where coanio=rtrim(ltrim(@CoAnio)) and rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
--DECLARE @NuCodigo2 varchar(14)=(SELECT RIGHT('00000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,5)+1 as int)),1) as char(5)))),5)  as correlativo FROM MAFONDOFIJO where  rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
DECLARE @NuCodigo2 varchar(14)=(SELECT RIGHT('00000' + Ltrim(Rtrim(cast(ISNULL(MAX(cast(SUBSTRING(ISNULL(NUCODIGO,''),1,3)+1 as int)),1) as char(5)))),5)  as correlativo FROM MAFONDOFIJO where  rtrim(ltrim(coprefijo)) =rtrim(ltrim(@CoPrefijo)))
Update MAFONDOFIJO      
 Set --FlActivo=0,
  CoEstado='AC',
  UserMod=@UserMod,      
  FecMod=GETDATE()      
where NuFondoFijo=@NuFondoFijo_Origen
Declare @NuFondoFijo int =(select Isnull(Max(NuFondoFijo),0)+1         
        from MAFONDOFIJO )        
	Insert Into dbo.MAFONDOFIJO
		(
			NuFondoFijo,
 			NuCodigo,
 			CoCeCos,
 			CtaContable,
 			Descripcion,
 			CoUserResponsable,
 			CoMoneda,
 			SSMonto,
			SsSaldo,
			SsDifAceptada,
			FeEmision_Reembolso,
			FLEsReembolso,
			CoTipoDoc_Reembolso,
			NuDocum_Reembolso,
			NuFondoFijo_Reembolso,
			CoPrefijo,
			CoAnio,
 			UserMod,
 			FecMod,
			CoSap,
			CoTipoFondoFijo,
			NuCodigo_ER
 		)  
      select @NuFondoFijo,
 			@NuCodigo,
 			CoCeCos,
 			CtaContable,
 			Descripcion,
 			CoUserResponsable,
 			CoMoneda,
 			SSMonto,
			SSMonto,
			0,
			Case When @FeEmision_Reembolso='01/01/1900' Then getdate() Else @FeEmision_Reembolso End,  
			cast(1 as bit),
			Case When Ltrim(Rtrim(@CoTipoDoc_Reembolso))='' Then Null Else @CoTipoDoc_Reembolso End,
			Case When Ltrim(Rtrim(@NuDocum_Reembolso))='' Then Null Else @NuDocum_Reembolso End,
 			@NuFondoFijo_Origen,
			CoPrefijo,
			CoAnio,
			UserMod,
 			getdate(),
			@NuCodigo2,
			CoTipoFondoFijo,
			NuCodigo_ER
	from MAFONDOFIJO
	WHERE NuFondoFijo=@NuFondoFijo_Origen
END;
