﻿--create
CREATE Procedure dbo.MACENTROCOSTOS_TIPOVENTA_Sel_Cbo
@CoTipoVenta char(2)=''
As      
Set NoCount On      
begin
select c.CoCeCos, c.CoCeCos+ ' - '+Descripcion as Descripcion
from MACENTROCOSTOS_TIPOVENTA cct
left join MACENTROCOSTOS c on c.CoCeCos=cct.CoCeCos
where (cct.CoTipoVenta=@CoTipoVenta or @CoTipoVenta='') and c.FlActivo=1
end;
