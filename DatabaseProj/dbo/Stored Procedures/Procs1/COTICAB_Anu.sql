﻿Create Procedure [dbo].[COTICAB_Anu]
	@IDCab	int,
	@UserMod char(4)
As
	Set NoCount On
	
	Update COTICAB Set Estado='X', UserMod=@UserMod, FecMod=GETDATE()
	Where IDCAB=@IDCab
