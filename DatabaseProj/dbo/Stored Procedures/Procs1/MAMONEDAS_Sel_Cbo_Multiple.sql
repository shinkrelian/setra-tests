﻿CREATE Procedure [dbo].[MAMONEDAS_Sel_Cbo_Multiple]  
@IDMonedaNoCo char(3),
@bTodos bit =0
As  
 Set NoCount On  
 Select '' as IDMoneda,'<TODAS>' as Descripcion
where @bTodos=1
Union
 Select IDMoneda, Descripcion From MAMONEDAS  
 Where Activo='A' and IDMoneda <> @IDMonedaNoCo
