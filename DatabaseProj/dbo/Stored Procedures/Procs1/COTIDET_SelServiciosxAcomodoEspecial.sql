﻿--HLF-20140207-INNER Join COTIDET_ACOMODOESPECIAL ca On cd.IDDET = ca.IDDet      
--JRF-20141222-Agregar un filtro por días.  
--JRF-20150225-No Considerar el NoShow
--JRF-20150515-..And IsNULL(IDDetRel,0)=0
CREATE Procedure dbo.COTIDET_SelServiciosxAcomodoEspecial      
@IDCab int,      
@IDProveedor char(6),  
@FecDiaIng datetime='01/01/1900',  
@FecDiaOut datetime='01/01/1900'  
as      
 Set NoCount On      
 Select X.IDDET,X.QtCapacidad,X.EsMatrimonial,X.FechaIN,X.NoCapacidad,X.QtPax,X.AcomodoTitulo,      
  dbo.FnAcomodosEspecialesxIDDetxCapacidadxMat(X.IDDET,X.QtCapacidad,X.EsMatrimonial) as AcomodoTituloxCapacidad
 from (      
 Select cd.IDDet,ch.QtCapacidad,Case When Charindex('(MAT)',ch.NoCapacidad)> 0 Then 1 Else 0 End As EsMatrimonial,      
 Convert(char(10),cd.Dia,103) as FechaIN,ch.NoCapacidad,ca.QtPax,      
 dbo.FnAcomodosEspecialesxIDDet(cd.IDDET) as AcomodoTitulo  --,cd.IDProveedor    
 --case when (select COUNT(*) from COTIDET_ACOMODOESPECIAL_DISTRIBUC cad Where cad.IDDet = cd.IDDET) > 0       
 --then 1 Else 0 End as ExisteAcomodoxPax
 ,Case When Exists(select rd.IDReserva_Det from RESERVAS_DET rd LEFT Join RESERVAS r On rd.IDReserva=r.IDReserva 
				   LEFT Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
				   Where r.Anulado=0 and rd.Anulado=0 and rd.FlServicioNoShow=1
				   and r.IDProveedor=p.IDProveedor and rd.IDDet=cd.IDDET And
				   (Convert(char(10),rd.Dia,103)=Convert(char(10),@FecDiaIng,103) and Convert(char(10),rd.FechaOut,103)=Convert(char(10),@FecDiaOut,103))
				   and CHARINDEX(ch.NoCapacidad,rd.Servicio)>0) Then 1 Else 0 End
 As ExisteNoShow
 from COTIDET cd --Left Join RESERVAS_DET rd On cd.IDDET = rd.IDDet      
 --LEFT Join COTIDET_ACOMODOESPECIAL ca On cd.IDDET = ca.IDDet      
 INNER Join COTIDET_ACOMODOESPECIAL ca On cd.IDDET = ca.IDDet      
 Left Join CAPACIDAD_HABITAC ch On ca.CoCapacidad = ch.CoCapacidad      
 Where cd.IDProveedor = @IDProveedor and AcomodoEspecial = 1 and cd.IDCAB = @IDCab and   
    ((Convert(varchar(10),@FecDiaIng,103)='01/01/1900' and Convert(varchar(10),@FecDiaOut,103)='01/01/1900')  
    Or cd.Dia between @FecDiaIng and DATEADD(d,-1,@FecDiaOut)) And IsNULL(IDDetRel,0)=0) as X  
 Where X.ExisteNoShow = 0
 Order by CAST(X.FechaIN as smalldatetime)   
 
