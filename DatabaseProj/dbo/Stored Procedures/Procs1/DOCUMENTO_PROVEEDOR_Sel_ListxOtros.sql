﻿ --JHD-20150826- NuDocum= case when FlCorrelativo=1 then '' else
				-- case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
				--end,
 --JHD-20150826- NuDocum2=case when FlCorrelativo=1 then '' else
				-- case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			    -- end, 
--JHD-20150826- (dp.CoObligacionPago is null or dp.CoObligacionPago='OTR')
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_ListxOtros]  
 @CoProveedor char(6)=''
AS    
BEGIN   
SET NOCOUNT ON   
SELECT     dp.NuDocumProv,    
   dp.NuVoucher,    
   --dp.NuDocum,    
   --NuDocum2=dbo.FnFormatearDocumProvee(dp.NuDocum),
   --NuDocum=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
    NuDocum= case when FlCorrelativo=1 then '' else
				 case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,
   --NuDocum2=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum, 
     NuDocum2=case when FlCorrelativo=1 then '' else
				case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,  

   --dp.NuDocInterno,        
    NuDocInterno= dp.CardCodeSAP,    
   dp.CoTipoDoc,     
          td.Descripcion,    
          dp.FeEmision,    
          dp.FeRecepcion,    
          dp.CoTipoDetraccion,     
          PorcentajeDet=(select isnull(SsTasa,0)*100 from dbo.MATIPODETRACCION where CoTipoDetraccion=dp.CoTipoDetraccion),    
          dp.CoMoneda,    
          mo.Descripcion AS Moneda,    
          SsNeto,    
          SsOtrCargos,    
          SsDetraccion,    
          dp.SsIGV,    
          dp.SSTotalOriginal,     
           dp.SSTipoCambio,    
          dp.SSTotal,    
          dp.FlActivo    
FROM         DOCUMENTO_PROVEEDOR dp   
INNER JOIN  MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc INNER JOIN    
                      MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda    
WHERE     (dp.CoProveedor = @CoProveedor or @CoProveedor='') AND    
(dp.CoObligacionPago is null or dp.CoObligacionPago='OTR')
END;    
