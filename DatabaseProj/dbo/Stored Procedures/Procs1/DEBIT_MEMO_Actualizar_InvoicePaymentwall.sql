﻿
CREATE PROCEDURE [dbo].[DEBIT_MEMO_Actualizar_InvoicePaymentwall]	
							@IDDebitMemo char(10)
AS
BEGIN
	Set NoCount On
	UPDATE DEBIT_MEMO SET FlEnvioInvoice = 1
	WHERE IDDebitMemo = @IDDebitMemo
END
