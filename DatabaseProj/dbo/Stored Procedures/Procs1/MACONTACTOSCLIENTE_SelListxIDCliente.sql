﻿

Create Procedure MACONTACTOSCLIENTE_SelListxIDCliente
@IDCLiente varchar(6)
As
	Set NoCount On
	select distinct Titulo +' '+ Nombres +' '+Apellidos , IDContacto , isnull(Email,'') As Correo
	from MACONTACTOSCLIENTE 
	Where Email IS NOT NULL AND
	IDCliente = @IDCLiente
	
