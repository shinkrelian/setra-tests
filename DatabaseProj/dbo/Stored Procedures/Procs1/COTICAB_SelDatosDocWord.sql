﻿CREATE Procedure [dbo].[COTICAB_SelDatosDocWord]
	@IDCab	int
As
	Set NoCount On
	
	Select DocWordIDIdioma, DocWordRutaImagen, 
	DocWordSubTitulo, DocWordTexto From COTICAB Where IDCAB=@IDCab
