﻿
--------------------------------------------------------------------------------------------


CREATE PROCEDURE MAAEROPUERTO_UPD

		   @CoAero char(3),
           @NoDescripcion varchar(200),
           @CoUbigeo char(6),
           @UserMod char(4)=''
           
AS
BEGIN
UPDATE MAAEROPUERTO
SET   [NoDescripcion] = @NoDescripcion,
      [CoUbigeo] = @CoUbigeo,
      [UserMod] = @UserMod, 
      [FecMod] = GETDATE()
      WHERE [CoAero] = @CoAero

END;           
