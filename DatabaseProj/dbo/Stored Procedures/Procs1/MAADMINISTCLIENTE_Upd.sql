﻿Create Procedure [dbo].[MAADMINISTCLIENTE_Upd]
	@IDAdminist char(2),
	@IDCliente char(6),
	@Banco varchar(30),
	@CtaCte varchar(30),
	@IDMoneda char(3),
	@SWIFT varchar(30),
	@IVAN varchar(30),
	@NombresTitular varchar(80),
	@ApellidosTitular varchar(80),
	@ReferenciaAdminist text,
	@CtaInter varchar(30),
	@BancoInterm varchar(30),
	@UserMod char(4)

As
	Set NoCount On
	
	UPDATE MAADMINISTCLIENTE Set
           Banco=Case When RTRIM(ltrim(@Banco))= '' Then Null Else @Banco End,
           CtaCte=Case When RTRIM(ltrim(@CtaCte ))= '' Then Null Else @CtaCte End,
           IDMoneda=@IDMoneda,
           SWIFT=Case When RTRIM(ltrim(@SWIFT))= '' Then Null Else @SWIFT End,
           IVAN=Case When RTRIM(ltrim(@IVAN))= '' Then Null Else @IVAN End,
           NombresTitular=Case When RTRIM(ltrim(@NombresTitular))= '' Then Null Else @NombresTitular End,
           ApellidosTitular=Case When RTRIM(ltrim(@ApellidosTitular))= '' Then Null Else @ApellidosTitular End,
           ReferenciaAdminist=Case When RTRIM(ltrim(Cast (@ReferenciaAdminist as varchar(max))))= '' Then Null Else @ReferenciaAdminist End,
           CtaInter=Case When RTRIM(ltrim(@CtaInter))= '' Then Null Else @CtaInter End,
           BancoInterm=Case When RTRIM(ltrim(@BancoInterm))= '' Then Null Else @BancoInterm End,
           UserMod=@UserMod
     WHERE
           IDAdminist=@IDAdminist And IDCliente=@IDCliente
