﻿Create Procedure [dbo].[COTICAB_QUIEBRES_Ins_CopiaIDCab]
	@IDCab	int,
	@IDCabCopia	int,
	@UserMod	char(4)

As
	Set NoCount On
	
		INSERT INTO COTICAB_QUIEBRES
           (IDCAB
           ,Pax
           ,Liberados
           ,Tipo_Lib
           ,UserMod)
		Select           
            @IDCab ,
            Pax ,
            Liberados,
            Tipo_Lib,
            @UserMod
		From COTICAB_QUIEBRES
		Where IDCab=@IDCabCopia
