﻿Create Procedure MAMODOSERVICIO_Ins
@CodModoServ char(2),
@Descripcion varchar(30),
@UserMod char(4)
As
	Set NoCount On
	Insert into MAMODOSERVICIO(CodModoServ,Descripcion,UserMod) 
			values	(@CodModoServ,@Descripcion,@UserMod)
