﻿Create Procedure dbo.COTIDET_PAX_UpdFlEntRQGenerada	
	@IDDet int,
	@IDPax int,	
	@FlEntRQGenerada bit,
	@UserMod char(4)
As
	Set nocount on

	Update COTIDET_PAX Set FlEntRQGenerada=@FlEntRQGenerada,
		UserMod=@UserMod, FecMod=GETDATE()
	Where IDDet=@IDDet And IDPax=@IDPax

