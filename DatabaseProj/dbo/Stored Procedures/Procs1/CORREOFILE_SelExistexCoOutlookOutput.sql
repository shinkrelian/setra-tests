﻿
CREATE Procedure [dbo].[CORREOFILE_SelExistexCoOutlookOutput]
	@CoCorreoOutlook	varchar(255),
	@pbExists	bit Output
As	
	Set NoCount On
	Set @pbExists = 0
	
	If Exists(Select NuCorreo From .BDCORREOSETRA..CORREOFILE Where CoCorreoOutlook=@CoCorreoOutlook)
		Set @pbExists = 1
		
