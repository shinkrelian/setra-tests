﻿
--HLF-20130829-Agregando @FechaOutSelect=isnull(, If @FechaOut<>'01/01/1900'
--HLF-20130902-Comentando Else
CREATE Procedure [dbo].[COTIDET_LOG_SelxSiEsTodoElProveedorOutput]      
	@IDCab int,      
	@IDProveedor char(6),
	@FechaIn smalldatetime, 
	@FechaOut smalldatetime,
	@pOk bit Output    
As      
	Set NoCount On      

	Declare @FechaInSelect smalldatetime='01/01/1900', @FechaOutSelect smalldatetime='01/01/1900'

	Select --l.IDDET, l.Accion, p.IDTipoProv, sd.IDServicio_Det, sd.PlanAlimenticio, sd.ConAlojamiento     
	Top 1 @FechaInSelect=Dia
	From COTIDET_LOG l     
	Where Atendido=0 And l.IDProveedor=@IDProveedor      
	And l.IDCab=@IDCab And l.Accion='B'      
	Order by l.IDDET
	
	Set @pOk = 0
	
	If @FechaOut<>'01/01/1900'
		Begin
		
		Select --l.IDDET, l.Accion, p.IDTipoProv, sd.IDServicio_Det, sd.PlanAlimenticio, sd.ConAlojamiento     
		Top 1 @FechaOutSelect=isnull((Select isnull(FechaOut,'01/01/1900') From RESERVAS_DET Where IDDET=l.IDDET),'01/01/1900')
		From COTIDET_LOG l     
		Where Atendido=0 And l.IDProveedor=@IDProveedor      
		And l.IDCab=@IDCab And l.Accion='B'      
		Order by l.IDDET Desc
		
		If (@FechaInSelect=@FechaIn And @FechaOutSelect=@FechaOut) Or 
		Exists(Select idcab From COTIDET_LOG
		Where Atendido=0 And IDProveedor=@IDProveedor      
		And IDCab=@IDCab And Accion='N')
			Set @pOk = 1	
			
		End
	--Else
	--	If Exists(Select idcab From COTIDET_LOG
	--	Where Atendido=0 And IDProveedor=@IDProveedor      
	--	And IDCab=@IDCab And Accion='N')
	--		Set @pOk = 1	

