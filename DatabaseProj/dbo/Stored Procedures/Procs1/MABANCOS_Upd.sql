﻿Create Procedure [dbo].[MABANCOS_Upd]
	@IDBanco char(3),
	@Descripcion varchar(60),
	@Moneda char(1),
	@NroCuenta varchar(20),
	@Sigla varchar(8),
	@CtaInter varchar(20),
	@CtaCon varchar(20),
	@Direccion varchar(100),
	@SWIFT varchar(20),
	@UserMod char(4)

As

	Set NoCount On

	Update MABANCOS	Set 
	Descripcion=@Descripcion,
	Moneda=@Moneda,
	NroCuenta=Case When LTRIM(RTRIM(@NroCuenta)) = '' Then Null Else @NroCuenta End,
	Sigla=Case When LTRIM(RTRIM(@Sigla)) = '' Then Null Else @Sigla  End,
	CtaInter=Case When LTRIM(RTRIM(@CtaInter)) = '' Then Null Else @CtaInter End,
	CtaCon=Case When LTRIM(RTRIM(@CtaCon)) = '' Then Null Else @CtaCon End,
	Direccion=Case When LTRIM(RTRIM(@Direccion)) = '' Then Null Else @Direccion End,
	SWIFT=Case When LTRIM(RTRIM(@SWIFT)) = '' Then Null Else @SWIFT End,
	UserMod=@UserMod ,
	FecMod=GETDATE() 
	Where IDBanco = @IDBanco
