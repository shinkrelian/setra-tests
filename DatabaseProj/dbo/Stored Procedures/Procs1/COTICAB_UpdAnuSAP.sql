﻿Create Procedure dbo.COTICAB_UpdAnuSAP
@IDCab int,
@FlFileAnulaEn_SAP bit,
@UserMod char(4) 
As
Set NoCount On
Update COTICAB set FlFileAnulaEn_SAP=@FlFileAnulaEn_SAP,UserMod=@UserMod,FecMod=getdate()
where IDCAB=@IDCab
