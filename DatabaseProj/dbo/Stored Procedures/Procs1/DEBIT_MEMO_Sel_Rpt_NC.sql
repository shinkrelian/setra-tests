﻿--JHD-20160209 - Pax=isnull(c.NroPax+isnull(c.NroLiberados,0),0),
--JHD-20160209 - ,Supervisor=isnull((select uj.Usuario from MAUSUARIOS u inner join MAUSUARIOS uj on u.CoUserJefe=uj.IDUsuario where u.IDUsuario=c.IDUsuario),'')
--JHD-20160211 - Pax=isnull(c.Titulo,''), NroPax=isnull(c.NroPax+isnull(c.NroLiberados,0),0),
--JHD-20160211 - ,Motivo=isnull(d.IDMotivo,0)
--JHD-20160211 - Estado=case when d.IdEstado='PD' THEN 'P' ELSE 'D' END
--JHD-20160215 -,Supervisor=isnull((select u1.usuario from MAUSUARIOS u1 where u1.IDUsuario=d.IDSupervisor),'')
--JHD-20160215 -,TxObservacion=isnull(d.TxObservacion,'')
--JHD-20160215 - Ejecutivo=isnull((select usuario from MAUSUARIOS u1 where u1.IDUsuario=d.idresponsable),d.responsable),
CREATE Procedure [dbo].[DEBIT_MEMO_Sel_Rpt_NC]
@Fecha1 smalldatetime='01/01/1900',
@Fecha2 smalldatetime='01/01/1900'
as
BEGIN
	select 
	c.IDFile,
	d.Fecha,
	cl.RazonComercial as DescCliente ,
	Pax=isnull(c.Titulo,''),
	NroPax=isnull(c.NroPax+isnull(c.NroLiberados,0),0),
	--d.Responsable as Ejecutivo,
	Ejecutivo=isnull((select usuario from MAUSUARIOS u1 where u1.IDUsuario=d.idresponsable),d.responsable),
	Total=cast( isnull(dbo.FnCambioMoneda(d.Total,d.IDMoneda,'USD',[dbo].[FnTipoDeCambioVtaUltimoxFecha](d.Fecha)),0) as numeric(8,2)),
	Comments=isnull((select top 1 Descripcion from DEBIT_MEMO_DET where IDDebitMemo=d.IDDebitMemo),''),
	--Estado=case when d.IdEstado='PD' THEN 'Pendiente de descuento' ELSE 'Descontado' END
	Estado=case when d.IdEstado='PD' THEN 'P' ELSE 'D' END
	--,Supervisor=isnull((select uj.Usuario from MAUSUARIOS u inner join MAUSUARIOS uj on u.CoUserJefe=uj.IDUsuario where u.IDUsuario=c.IDUsuario),'')
	,Supervisor=isnull((select u1.usuario from MAUSUARIOS u1 where u1.IDUsuario=d.IDSupervisor),'')
	--,Supervisor=
	--			isnull((select uj.Nombre + isnull(uj.TxApellidos,'') from MAUSUARIOS u
	--			inner join MAUSUARIOS uj on u.CoUserJefe=uj.IDUsuario
	--			where u.IDUsuario=c.IDUsuario),'')
	,Motivo=isnull(d.IDMotivo,0)
	,TxObservacion=isnull(d.TxObservacion,'')
	from DEBIT_MEMO d
	LEFT JOIN COTICAB c on c.IDCAB=d.IDCab
	left join MACLIENTES cl on cl.IDCliente=d.IDCliente
	where --IDEstado='PD'
	 (d.Fecha Between @Fecha1 And @Fecha2 Or Convert(Char(10),@Fecha1,103)='01/01/1900')
	AND TOTAL<0
	and IDEstado in ('PD','PG')
	order by IDFile
END;
