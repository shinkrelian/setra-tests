﻿--JHD-20150815-Cambiar nombres a las columnas
--JHD-20150826-,U_SYP_MDSD=isnull(d.NuSerie,'000')
--JHD-20150910-,U_SYP_TCOMPRA=case when CoObligacionPago='FFI' THEN 'CC'
				--when CoObligacionPago='PST' AND (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null THEN 'CC'
				--else '*' END
--JHD-20150917-left join MAPROVEEDORES p on d.CoProveedor=p.IDProveedor
--JHD-20150917-CardCode=isnull(p.CardCodeSAP,''),
--JHD-20150917-,U_SYP_TCOMPRA= CASE WHEN d.cotipoCompra is not null then d.CoTipoCompra ...
--JHD-20150918-,U_SYP_STATUS= 'I'
--JHD-20150921-U_SYP_MDTD=isnull(td.CoSAP,'')
CREATE PROCEDURE [dbo].[Documento_Proveedor_List_SAP_PUT]
@nudocumprov int
as
select
CardCode=isnull(p.CardCodeSAP,''),
--U_SYP_MDTD=isnull(td.CoSunat,'')
U_SYP_MDTD=isnull(td.CoSAP,'')
,U_SYP_MDSD=isnull(d.NuSerie,'000')
,U_SYP_MDCD=isnull(d.NuDocum,'')
--,U_SYP_STATUS= 'A'
,U_SYP_STATUS= 'I'
,U_SYP_TCOMPRA= CASE WHEN d.cotipoCompra is not null then d.CoTipoCompra
				else 
					case when CoObligacionPago='FFI' THEN isnull((select CoTipoFondoFijo from MAFONDOFIJO where NuFondoFijo=d.NuFondoFijo),'') -- 'CC'
					when CoObligacionPago='PST' AND (SELECT TOP 1 NuCodigo_ER FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null then 'ER'
					when CoObligacionPago='PST' AND (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null THEN 'ER'--'CC'
					else '*' END
				end
--,U_SYP_TCOMPRA=case when CoObligacionPago='FFI' THEN 'CC' else 'ER' END
--,U_SYP_TCOMPRA=case when CoObligacionPago='FFI' THEN 'CC'
--				when CoObligacionPago='PST' AND (SELECT TOP 1 NuFondoFijo FROM PRESUPUESTO_SOBRE WHERE NuPreSob=D.NuPreSob) is not null THEN 'CC'
--				else '*' END

from DOCUMENTO_PROVEEDOR d
left join MAPROVEEDORES p on d.CoProveedor=p.IDProveedor
left join matipodoc td on td.IDtipodoc=d.CoTipoDoc
where d.nudocumprov=@nudocumprov;
