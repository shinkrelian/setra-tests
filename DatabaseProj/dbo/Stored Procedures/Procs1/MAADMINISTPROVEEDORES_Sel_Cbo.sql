﻿--HLF-20130710-Agregando  + SPACE(100) +'|'+Banco
CREATE Procedure dbo.MAADMINISTPROVEEDORES_Sel_Cbo  
@IDProveedor char(6)  
As  
 Set NoCount On  
 select IDMoneda As ID,
 CtaCte + SPACE(100) +'|'+Banco as CtaCte
 from MAADMINISTPROVEEDORES   
 Where IDProveedor = @IDProveedor  
 Order By CtaCte  
 
