﻿CREATE Procedure [dbo].[COTICAB_UpdSumaRestaNroPax]            
 @IDCab int,                    
 @NroPax smallint,                        
 @UserMod char(4)                    
As                    
 Set NoCount On                    
                  
 UPDATE COTICAB SET                       
 NroPax=NroPax+@NroPax,                     
 UserMod=@UserMod, 
 FecMod=GETDATE()                    
 Where  IDCAB=@IDCab                    

