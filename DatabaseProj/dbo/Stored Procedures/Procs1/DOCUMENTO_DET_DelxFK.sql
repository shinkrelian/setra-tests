﻿
Create Procedure dbo.DOCUMENTO_DET_DelxFK
	@NuDocum char(10),
	@IDTipoDoc char(3)
As	
	Set NoCount On
	
	Delete From DOCUMENTO_DET
	Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc
