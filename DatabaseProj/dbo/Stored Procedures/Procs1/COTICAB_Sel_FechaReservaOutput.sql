﻿CREATE Procedure [dbo].[COTICAB_Sel_FechaReservaOutput]
	@IDCab	int,
	@pFechaReserva smalldatetime Output
As

	Set NoCount On
	
	Select @pFechaReserva=IsNull(FechaReserva,'01/01/1900') From COTICAB Where IDCab=@IDCab
