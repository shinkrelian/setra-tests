﻿Create Procedure [dbo].[MACONTACTOSPROVEEDOR_Sel_Cbo]	
	@IDProveedor char(6)	
As
	Set NoCount On
	
	Select IDContacto, Nombres+' '+Apellidos as Nombre
	FROM MACONTACTOSPROVEEDOR
    WHERE IDProveedor=@IDProveedor
    Order by 2
