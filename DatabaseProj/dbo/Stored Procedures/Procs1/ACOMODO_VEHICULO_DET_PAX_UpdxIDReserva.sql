﻿Create Procedure [dbo].[ACOMODO_VEHICULO_DET_PAX_UpdxIDReserva]
	@IDReserva int,
	@UserMod char(4)
As
	Set Nocount on

	Update ACOMODO_VEHICULO_DET_PAX Set IDReserva_Det=null, UserMod=@UserMod, FecMod=GETDATE()
	Where isnull(IDReserva_Det,0) in (Select IDReserva_Det From RESERVAS_DET Where IDReserva=@IDReserva)

