﻿Create Procedure dbo.DOCUMENTO_Anu
@NuDocum Char(10),
@IDTipoDoc char(3),
@UserMod char(4)
As
Set NoCount On
Update DOCUMENTO 
	Set CoEstado = 'AN',
		UserMod  = @UserMod,
		FecMod = GETDATE()
Where NuDocum = @NuDocum and IDTipoDoc = @IDTipoDoc
