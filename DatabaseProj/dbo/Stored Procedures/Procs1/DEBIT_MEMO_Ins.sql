﻿--JRF-20130704-Quitando el campo de Serie.            
--JRF-20130709-Agregano el campo FlManual.        
--JRF-20130829-Agregando el campo IDCliente.        
--JRF-20131130-Se ha creado un nuevo tipo de DebitMemo.    
--HLF-20140620-Grabando campo SsSaldoCruceDMemo  
--HLF-20150414-,Case When ltrim(rtrim(@IDCliente))='' Then Null Else @IDCliente End
--JHD-20160211-Agregando el campo @IDMotivo int
--JHD-20160212-Agregando los campo @IDResponsable y @IDSupervisor
--JHD-20160215-Agregando el campo @TxObservacion varchar(max)
CREATE Procedure [dbo].[DEBIT_MEMO_Ins]            
 @Fecha smalldatetime,              
 @FecVencim smalldatetime,                
 @IDCab int,                
 @IDMoneda char(3),                
 @SubTotal numeric(8,2),                
 @TotalIGV numeric(8,2),                
 @Total numeric(8,2),                
 @IDBanco char(3),                
 @IDEstado char(2),                
 @IDTipo char(1),            
 @FechaIn smalldatetime,            
 @FechaOut smalldatetime,            
 @Cliente varchar(80),            
 @Pax smallint,            
 @Titulo varchar(100),            
 @Responsable varchar(80),            
 @FlImprimioDebit bit,            
 @FlManual bit,            
 @UserMod char(4),         
 @IDCliente char(6),        
 @IDMotivo int=0,
 @IDResponsable char(4)='',
 @IDSupervisor char(4)='',
 @TxObservacion varchar(max)='',
 @pIDDebitMemo char(10) Output                
                
As                
 Set NoCount On                
                 
 Declare @IDDebitMemo char(10)              
 Execute dbo.GeneracionIDDebitMemo @IDCab,@IDTipo,@IDDebitMemo output    
 INSERT INTO [dbo].[DEBIT_MEMO]                
      ([IDDebitMemo]            
      ,[Fecha]                 
      ,[FecVencim]                
      ,[IDCab]                
      ,[IDMoneda]                
      ,[SubTotal]                
      ,[TotalIGV]                
      ,[Total]         
      ,[Saldo]             
      ,[IDBanco]                
      ,[IDEstado]                
      ,[IDTipo]                
      ,[FechaIn]            
      ,[FechaOut]            
      ,[Cliente]            
      ,[Pax]            
      ,[Titulo]            
      ,[Responsable]            
      ,[IDCliente]        
      ,[FlImprimioDebit]           
      ,[FlManual]          
      ,[UserMod]  
	  ,IDMotivo
	  ,IDResponsable
	  ,IDSupervisor
	  ,TxObservacion
      --,SsSaldoxFacturar
      )                
   VALUES                
      (@IDDebitMemo             
      ,@Fecha             
      ,@FecVencim                
      ,Case When @IDCab = 0 Then Null Else @IDCab End          
      ,@IDMoneda                
      ,@SubTotal                
      ,@TotalIGV                
      ,@Total         
      ,@Total             
      ,@IDBanco                
      ,@IDEstado                
      ,@IDTipo              
      ,@FechaIn            
      ,@FechaOut            
      ,@Cliente            
      ,@Pax            
      ,@Titulo            
      ,@Responsable            
      ,Case When ltrim(rtrim(@IDCliente))='' Then Null Else @IDCliente End
      ,@FlImprimioDebit           
      ,@FlManual           
      ,@UserMod       
	  ,Case When @IDMotivo= 0 Then Null Else @IDMotivo End
	  ,Case When ltrim(rtrim(@IDResponsable))='' Then Null Else @IDResponsable End
	  ,Case When ltrim(rtrim(@IDSupervisor))='' Then Null Else @IDSupervisor End
	  ,Case When ltrim(rtrim(@TxObservacion))='' Then Null Else @TxObservacion End
      --,@Total           
      )                
                      
 Set @pIDDebitMemo=@IDDebitMemo                   

