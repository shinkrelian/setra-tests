﻿--JRF-20140317-Nuevo campo "Orden"    
--JRF-20140428-Nuevo campo TxObsStatus   
--JRF-20150304-Nuevo campo FlAutomatico
CREATE Procedure [dbo].[MAINCLUIDO_Upd]      
 @IDIncluido int,      
 @Tipo char(1),      
 @DescAbrev varchar(100),      
 @PorDefecto bit,      
 @FlAutomatico bit,
 @Orden tinyint,    
 @TxObsStatus text,  
 @UserMod char(4)      
As      
 Set NoCount On      
      
 Update MAINCLUIDO      
 Set Tipo=@Tipo,      
 DescAbrev=@DescAbrev,      
 PorDefecto=@PorDefecto,
 FlAutomatico=@FlAutomatico,      
 Orden=@Orden,    
 TxObsStatus = Case When ltrim(rtrim(CAST(@TxObsStatus as varchar(max))))='' then Null Else @TxObsStatus End,  
 UserMod=@UserMod,      
 FecMod=GETDATE()       
 Where IDIncluido=@IDIncluido      
