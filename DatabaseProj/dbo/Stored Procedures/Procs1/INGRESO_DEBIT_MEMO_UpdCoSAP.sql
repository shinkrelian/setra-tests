﻿Create Procedure dbo.INGRESO_DEBIT_MEMO_UpdCoSAP
	@NuIngreso int,
	@NuDebitMemo char(10),
	@CoSAP varchar(15),
	@UserMod char(4)
As
	Set Nocount on

	Update INGRESO_DEBIT_MEMO Set CoSAP = Case When ltrim(rtrim(@CoSAP))='' then null else @CoSAP end, UserMod=@UserMod, FecMod=GETDATE() 
	Where NuIngreso = @NuIngreso And IDDebitMemo = @NuDebitMemo

