﻿Create Procedure dbo.COTIDET_ACOMODOESPECIAL_DISTRIBUC_PAX_SoloPeruanos
@IDDet int,
@CapacidadHab  tinyint,
@EsMatrimonial bit,
@pSoloPeruano bit output
As
Set NoCount On
Set @pSoloPeruano = 0
select @pSoloPeruano = 1
from COTIDET_ACOMODOESPECIAL_DISTRIBUC cdac Left Join COTIPAX cp On cdac.IDPax=cp.IDPax
Inner Join CAPACIDAD_HABITAC cap On cdac.CoCapacidad=cap.CoCapacidad
Where cdac.IDDet=@IDDet And (cp.IDNacionalidad = '000323' Or cp.Residente=1) And cap.QtCapacidad=@CapacidadHab And cap.FlMatrimonial=@EsMatrimonial
