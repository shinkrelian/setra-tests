﻿Create Procedure dbo.MAATENCION_MAPROVEEDOR_Upd
@IDAtencion tinyint,
@IDProveedor char(6),
@Dia1 char(1),
@Dia2 char(1),
@Desde1 varchar(22),
@Hasta1 varchar(22),
@Desde2 varchar(22),
@Hasta2 varchar(22),
@UserMod char(4)
As
Begin
	Set NoCount On
	Update MAATENCION_MAPROVEEDOR
		Set Dia1 = @Dia1,
			Dia2 = @Dia2,
			Desde1 = @Desde1,
			Hasta1 = @Hasta1,
			Desde2 = @Desde2,
			Hasta2 = @Hasta2,
			UserMod = @UserMod,
			FecMod = GetDate()
	Where IDAtencion = @IDAtencion and IDProveedor = @IDProveedor
End
