﻿
--JRF-20130704-Quitando el campo de Serie.      
--JRF-20130708-Pasando a Nulo el campo IDCab    
--JRF-20130829-Agregando el campo IDCliente.  
--JRF-20140212-Actualizar el saldo dependeniendo del monto ya ingresado
--JHD-20160211-Agregando el campo @IDMotivo int
--JHD-20160212-Agregando los campo @IDResponsable y @IDSupervisor
--JHD-20160215-Agregando el campo --JHD-20160215-Agregando el campo @TxObservacion varchar(max)
--HLF-20160321-FlTransCurso = Case When @IDEstado='TC' Then 1 Else FlTransCurso End
CREATE Procedure [dbo].[DEBIT_MEMO_Upd]        
@IDDebitMemo char(10),         
@FecVencim smalldatetime,        
@IDCab int,        
@IDMoneda char(3),        
@SubTotal numeric(8,2),        
@TotalIGV numeric(8,2),        
@Total numeric(8,2),        
@IDBanco char(3),        
@IDEstado char(2),        
@IDTipo char(1),        
@IDCliente char(6),  
@FechaIn smalldatetime,      
@FechaOut smalldatetime,      
@Cliente varchar(80),      
@Pax smallint,      
@Titulo varchar(100),      
@Responsable varchar(80),      
@IDMotivo int=0,      
@IDResponsable char(4)='',
@IDSupervisor char(4)='',
@TxObservacion varchar(max)='',
@UserMod char(4)        
As        
 Set NoCount On        
 UPDATE [dbo].[DEBIT_MEMO]        
    SET [FecVencim] = @FecVencim        
    ,[IDCab] = Case When @IDCab = 0 Then Null Else @IDCab End    
    ,[IDMoneda] = @IDMoneda        
    ,[SubTotal] = @SubTotal        
    ,[TotalIGV] = @TotalIGV        
    ,[Total] = @Total        
    ,[IDBanco] = @IDBanco        
    ,[IDEstado] = @IDEstado        
    ,[IDTipo] = @IDTipo        
    ,[FechaIn] = @FechaIn      
    ,[FechaOut] = @FechaOut      
    ,[Cliente] = @Cliente      
    ,[Pax] = @Pax      
    ,[Titulo] = @Titulo      
    ,[Responsable] = @Responsable      
    ,[IDCliente] = @IDCliente  
    ,[Saldo] = @Total - Isnull((Select SUM(SsPago) from INGRESO_DEBIT_MEMO WHERE IDDebitMemo = @IDDebitMemo And SsPago > 0),0)
	,[IDMotivo] = Case When @IDMotivo = 0 Then Null Else @IDMotivo End    
	,IDResponsable = Case When rtrim(ltrim(@IDResponsable)) = '' Then Null Else @IDResponsable End   
	,IDSupervisor = Case When rtrim(ltrim(@IDSupervisor)) = '' Then Null Else @IDSupervisor End   
	,TxObservacion = Case When rtrim(ltrim(@TxObservacion)) = '' Then Null Else @TxObservacion End   
	,FlTransCurso = Case When @IDEstado='TC' Then 1 Else FlTransCurso End
    ,[UserMod] = @UserMod        
    ,[FecMod] = GetDate()        
  WHERE [IDDebitMemo] = @IDDebitMemo     


