﻿--JRF-20140711-Agregar el filtro por el nuevo campo. [CoTipoVenta]
--JHD-20150305- Case c.Tipo When 'IT' then 'INTERESADO' When 'CL' then 'CLIENTE' End as Status
--JHD-20150709- Case c.Tipo When 'IT' then 'INTERESADO' When 'CL' then 'CLIENTE' when 'ND' then 'NO DESEADO' End as Status, 
CREATE Procedure [dbo].[MACLIENTES_Sel_List]    
 @IDCliente char(6),    
 @RazonComercial varchar(60),    
 @IDPais char(6),    
 @Contacto varchar(100),    
 @Top tinyint,  
 @Tipo char(2),
 @CoTipoVenta char(2) 
AS    
  Set NoCount On    
  
 SELECT  NumIdentidad,RazonComercial,Direccion,    
 Ciudad,Pais,IDubigeoPais,IDIdentidad,Status,IDCliente    
 FROM     
 (    
 SELECT row_number() over (order by RazonComercial) as NroFila ,X.*     
 FROM     
 (      
 Select Distinct    
 c.NumIdentidad ,     
 RazonComercial,      
 c.Direccion,    
 --Ciudad = (select Descripcion from MAUBIGEO where  IDubigeo=c.IDCiudad ),    
 c.Ciudad,  
 up.Descripcion as Pais,     
 up.IDubigeo as IDubigeoPais,     
 c.IDIdentidad ,     
 --Case c.Tipo When 'I' then 'INTERESADO' When 'C' then 'CLIENTE' End as Status,     
 --Case c.Tipo When 'IT' then 'INTERESADO' When 'CL' then 'CLIENTE' End as Status,     
 Case c.Tipo When 'IT' then 'INTERESADO' When 'CL' then 'CLIENTE' when 'ND' then 'NO DESEADO' End as Status,     
 c.IDCliente  
 From MACLIENTES c   
 --Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo    
 Left Join MAUBIGEO up On c.IDCiudad=up.IDubigeo    
 Left Join MACONTACTOSCLIENTE cc On c.IDCliente=cc.IDCliente    
 Where     
 --(c.NumIdentidad = @NumIdentidad Or ltrim(rtrim(@NumIdentidad))='') And    
 (c.IDCliente=@IDCliente Or LTRIM(RTRIM(@IDCliente))='') And    
 (c.RazonComercial Like '%'+@RazonComercial+'%' Or ltrim(rtrim(@RazonComercial))='')     
 And c.Activo='A'    
 And (ltrim(rtrim(@IDPais))='' Or c.IDCiudad=@IDPais)    
 And ((cc.Nombres+ ' ' + cc.Apellidos Like '%'+@Contacto+'%' Or ltrim(rtrim(@Contacto))=''))    
 --Or (c.RazonSocial Like '%'+@Contacto+'%' Or ltrim(rtrim(@Contacto))=''))    
 And (c.Tipo = @Tipo Or ltrim(rtrim(@Tipo))='')  
 and (c.CoTipoVenta = @CoTipoVenta)
 ) AS X     
 ) AS Y     
 WHERE (Y.NROFILA<=@Top Or @Top=0)    
 Order by RazonComercial     