﻿CREATE Procedure [dbo].[MACORREOREVPROVEEDOR_Upd]
@IDProveedor char(6),
@Correo varchar(150),
@Descripcion varchar(max),
@UserMod char(4)
As
	Set NoCount On
	Update MACORREOREVPROVEEDOR
	Set Correo = @Correo,
		Descripcion = @Descripcion,
		UserMod = @UserMod
	Where IDProveedor = @IDProveedor And Correo = @Correo
