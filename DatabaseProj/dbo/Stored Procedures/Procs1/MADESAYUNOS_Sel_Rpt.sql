﻿CREATE procedure [dbo].[MADESAYUNOS_Sel_Rpt] 
@Descripcion char(50)
As
	Set NoCount on
	
	SELECT IDDesayuno,Descripcion 
	FROM MADESAYUNOS
	where (Descripcion like '%'+LTRIM(RTRIM(@Descripcion))+'%' or 
	LTRIM(RTRIM(@Descripcion))='') 
	Order By 1
