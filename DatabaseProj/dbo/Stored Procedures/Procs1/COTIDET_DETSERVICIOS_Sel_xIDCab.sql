﻿Create Procedure [dbo].[COTIDET_DETSERVICIOS_Sel_xIDCab]
 @IDCAB int,                                              
 @IDDet int,
 @ConCopias bit,                                            
 @SoloparaRecalcular bit,
 @ConIDReserva_Det bit=0 
As                                              
 Set NoCount On                                              
                                  
 SELECT                                           
 0 As chkSelBloque ,cds.IDDET,                  
 c.Item,c.Dia,                                              
 upper(substring(DATENAME(dw,c.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,c.Dia,103))) as DiaFormat,                                             
 Case When p.IDTipoProv='001' And sd.PlanAlimenticio=0 Then ''                                         
 Else                                         
 substring(convert(varchar,c.Dia,108),1,5)                                         
 End as Hora,                                             
                                  
                                  
 u.IDPais, c.IDUbigeo, u.Descripcion as DescCiudad ,p.IDTipoProv,p.IDTipoOper, s.Dias,                                             
 c.IDProveedor,                                              
 p.NombreCorto as DescProveedor,                                   
                           
 pinternac.NombreCorto as DescProvInternac, 
 Case When (SELECT COUNT(*) FROM COTIDET_DETSERVICIOS cd3 WHERE cd3.IDDET=cds.IDDet)>1 then
	sd.Descripcion --case When (SELECT COUNT(*) FROM COTIDET cd3 WHERE cd3.IDDET=cds.IDDet and cd3.IDServicio_Det=cds.IDServicio_Det)=1 Then c.Servicio Else sd.Descripcion End 
 else c.Servicio End As DescServicioDet, 
 Case When p.IDTipoProv='001' or p.IDTipoProv='002' Then  --hoteles o restaurantes                                            
  sd.Descripcion                                             
 Else                                              
  Case When (SELECT COUNT(*) FROM COTIDET_DETSERVICIOS cd3 WHERE cd3.IDDET=cds.IDDet)>1 then
	 case When (SELECT COUNT(*) FROM COTIDET cd3 WHERE cd3.IDDET=cds.IDDet and cd3.IDServicio_Det=cds.IDServicio_Det)=1 Then sd.Descripcion Else c.Servicio End 
 else c.Servicio End
 End As DescServicioDetBup,                                               
 sd.IDServicio, cds.IDServicio_Det,                          
 s.IDTipoServ, ts.Descripcion as DescTipoServ , sd.Anio,sd.Tipo,              
 IsNull(sd.DetaTipo,'') as DescVariante,                                      
 c.NroPax,                                   
                                   
 CAST(c.NroPax AS VARCHAR(3))+                                      
 Case When isnull(c.NroLiberados,0)=0 then '' else '+'+CAST(c.NroLiberados AS VARCHAR(3)) end                                      
 as PaxmasLiberados,                                        
                                  
 c.IncGuia, c.NroLiberados, c.Tipo_Lib,                                              
 cds.CostoReal,isnull(cds.CostoReal,0) as CostoRealAnt, cds.CostoRealImpto, cds.Margen,                                               
 cds.MargenImpto, cds.CostoLiberado, cds.CostoLiberadoImpto,                                               
 cds.MargenLiberado, cds.MargenLiberadoImpto,
 Case When c.FlServicioTC=1 Then (select IsNull(ctd.PoMargen,0) from COTICAB_TOURCONDUCTOR ctd where ctd.IDCab=cc.IDCAB) else cds.MargenAplicado End As MargenAplicado,
 isnull(cds.MargenAplicado,0) as MargenAplicadoAnt,                                              
 cds.SSCR, cds.SSCRImpto,                                               
 cds.SSMargen, cds.SSMargenImpto, cds.SSCL,                                               
 cds.SSCLImpto, cds.SSMargenLiberado, cds.SSMargenLiberadoImpto,                                               
 cds.SSTotal, cds.SSTotal as SSTotalOrig, cds.STCR, cds.STCRImpto,                                     
 cds.STMargen, c.STMargenImpto, c.STTotal, c.STTotalOrig,                                              
 cds.TotImpto, c.Especial, c.MotivoEspecial, c.RutaDocSustento,                                                
 sd.Desayuno, sd.Lonche, sd.Almuerzo, sd.Cena,                                              
 Case When Charindex(Lower('transfer'),
 Case When (SELECT COUNT(*) FROM COTIDET_DETSERVICIOS cd3 WHERE cd3.IDDET=cds.IDDet)>1 then
	 sd.Descripcion --case When (SELECT COUNT(*) FROM COTIDET cd3 WHERE cd3.IDDET=cds.IDDet and cd3.IDServicio_Det=cds.IDServicio_Det)=1 Then c.Servicio Else sd.Descripcion End 
 else c.Servicio End)> 0 Then s.Transfer Else 0 End as Transfer,
 ISNull(c.IDDetTransferOri,0) as IDDetTransferOri, ISNull(c.IDDetTransferDes,0) as IDDetTransferDes,                                              
 ISNull(c.TipoTransporte,'') as TipoTransporte,                                              
 c.IDUbigeoOri,
 uo.Descripcion+'          |'+IsNull(uo.DC,'') as DescUbigeoOri,                                              
 c.IDUbigeoDes,                                                                                       
 ud.Descripcion+'          |'+IsNull(ud.DC,'') as DescUbigeoDes,                                                                     
 c.IDIdioma,Id.Siglas, sd.ConAlojamiento,
 Case When c.FlServicioTC=1 Then (select IsNull(ctd.PoMargen,0) from COTICAB_TOURCONDUCTOR ctd where ctd.IDCab=cc.IDCAB) else IsNull(cc.Margen,0) End as MargenCotiCab,                                               
 c.Cotizacion,
 --isnull(c.Servicio,'') as Servicio, 
 Case When (SELECT COUNT(*) FROM COTIDET_DETSERVICIOS cd3 WHERE cd3.IDDET=cds.IDDet)>1 then
	sd.Descripcion --case When (SELECT COUNT(*) FROM COTIDET cd3 WHERE cd3.IDDET=cds.IDDet and cd3.IDServicio_Det=cds.IDServicio_Det)=1 Then c.Servicio Else sd.Descripcion End 
 else c.Servicio End as Servicio,
 isnull(c.IDDetRel,0) as IDDetRel ,                                              
 isnull(c.IDDetCopia,0) as IDDetCopia,cds.Total,cds.Total as TotalOrig,                                            
 0 as PorReserva, '' as IDReserva,                                            
 c.ExecTrigger, c.FueradeHorario   as FueradeHorario,                                          
 c.CostoRealEditado, c.CostoRealImptoEditado, c.MargenAplicadoEditado,                              
 c.ServicioEditado, cds.RedondeoTotal, sd.PlanAlimenticio,                                                   
 Case When c.FlServicioTC=1 Then (select ctc2.SsTipCam from COTICAB_TIPOSCAMBIO ctc2 where ctc2.IDCab=c.IDCAB and ctc2.CoMoneda='SOL') Else isnull(ctc.SsTipCam,0) End as TipoCambioCotiCab,            
 c.DebitMemoPendiente ,                                      
 Case When                 
 Exists(select sd2.IDServicio from MASERVICIOS_DET SD2 Left Join MASERVICIOS S2 on SD2.IDServicio = S2.IDServicio                
  where s2.IDCabVarios = @IDCAB and sd2.IDServicio_Det =c.IDServicio_Det) then 1 else 0 End As ServicioVarios,                                
 Case When ht.IdHabitTriple = '000' Or ht.IdHabitTriple = '002'                                
 Then ''                               
 else                               
 Case When sd.IDServicio_DetxTriple is Not null                               
  then (select sd3.Descripcion                               
    from MASERVICIOS_DET sd3 where sd3.IDServicio_Det = sd.IDServicio_DetxTriple)                              
    +' ('+ ht.Descripcion+')' else                              
 ht.Descripcion                               
 End                               
 End as TipoHabTriple,                              
                                
 Case When @ConIDReserva_Det=0 Then 0                              
 Else                              
  isnull((Select Top 1 isnull(IDReserva_Det,0) From RESERVAS_DET Where IDDet=cds.IDDET and IDServicio_Det=cds.IDServicio_Det        
  And Anulado=0 Order By FechaOut,Capacidad desc,EsMatrimonial desc),0)         
 End as IDReserva_Det  ,        
 CalculoTarifaPendiente,                    
c.AcomodoEspecial  ,                  
 dbo.FnDescripcionFechaEspecial(c.Dia,c.IDubigeo) as DescFechaEsp,              
 sd.CoMoneda as CoMonedaServ,           
 c.FlSSCREditado, c.FlSTCREditado ,    
 Case When (((p.IDTipoProv='001'and sd.PlanAlimenticio=0) Or(p.IDTipoProv<>'001' and sd.ConAlojamiento=1)))      
 Then ROW_NUMBER()Over(Partition By c.IDProveedor,c.IDServicio_Det,c.IncGuia,dbo.FnAcomodosEspecialesxIDDet(c.IDDet) Order by c.Dia, c.Item)       
 else 0 End as ItemAlojamiento ,Ltrim(Rtrim(dbo.FnAcomodosEspecialesxIDDet(c.IDDet))) As DescAcomodosEspeciales  ,
 c.FlServicioTC, 
 dbo.FnServicioMPWP(sd.IDServicio_Det) as IDServicio_V
 FROM COTIDET_DETSERVICIOS cds Left Join COTIDET c On cds.IDDET=c.IDDET
 Left Join MAPROVEEDORES p On c.IDProveedor=p.IDProveedor                                               
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo                                              
 Left Join MAUBIGEO uo On c.IDUbigeoOri=uo.IDubigeo                                              
 Left Join MAUBIGEO ud On c.IDUbigeoDes=ud.IDubigeo                                          
 Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det                                              
 Left Join MASERVICIOS s On s.IDServicio=sd.IDServicio                                              
 Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ                                              
 Left Join COTICAB cc On c.IDCAB=cc.IDCAB                                              
 Left Join MAIDIOMAS Id On c.IDidioma = Id.IDidioma                                        
 Left Join MAHABITTRIPLE ht On sd.IdHabitTriple = ht.IdHabitTriple                                
 Left Join MAPROVEEDORES pinternac On p.IDProveedorInternacional=pinternac.IDProveedor                          
 --Left Join MATIPOPROVEEDOR tp On p.IDTipoProv = tp.IDTipoProv                  
 Left Join COTICAB_TIPOSCAMBIO ctc On sd.CoMoneda=ctc.CoMoneda and ctc.IDCab=c.IDCAB
 --Left Join MASERVICIOS_DET sd2 On sd.IDServicio_Det_V=sd2.IDServicio_Det                       
 WHERE c.IDCAB=@IDCAB And cds.IDDET=@IDDet and                                 
 (c.IDDetCopia Is Null Or @ConCopias=1) And                                            
 (@SoloparaRecalcular=0 Or c.Recalcular=1)                                            
 ORDER BY c.Dia, c.Item  
