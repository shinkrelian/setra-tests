﻿--JHD-20151026-No se considera la cuenta de comision cuando se trate de translivik o apt
--Jhd-20151026-set @NuCuenta_Cliente=(select TOP 1 CtaContable from MAPLANCUENTAS_CENTROCOSTOS_DOCUMENTO
CREATE Procedure dbo.MAPLANCUENTAS_CENTROCOSTOS_DOCUMENTO_Sel_CtaContable
--declare
@CoCeCos char(6) ='',
@CoTipoVenta char(2)='',
@CoSerie char(3) ='',
@IDCliente char(6) =''
as
BEGIN
declare @NuCuentaComision varchar(14)
declare @NuCuenta_Cliente varchar(14)

	if @idcliente<>''
	begin
		set @NuCuentaComision=(select NuCuentaComision from MACLIENTES where IDCliente=@IDCliente)
		set @NuCuenta_Cliente=(select TOP 1 CtaContable from MAPLANCUENTAS_CENTROCOSTOS_DOCUMENTO
								where (CoCeCos=@CoCeCos or @CoCeCos='') and
								(CoTipoVenta=@CoTipoVenta or @CoTipoVenta='') and
								(CoSerie=@CoSerie or @CoSerie='') and (idcliente=@IDCliente))
		if @NuCuentaComision is not null
			begin
				if @IDCliente not in ('000054','001589')
					select @NuCuentaComision as CtaContable
				else
					begin

							if @NuCuenta_Cliente is not null
								select @NuCuenta_Cliente as CtaContable
							else
								select top 1  CtaContable
								from MAPLANCUENTAS_CENTROCOSTOS_DOCUMENTO
								where 
								(CoCeCos=@CoCeCos or @CoCeCos='') and
								(CoTipoVenta=@CoTipoVenta or @CoTipoVenta='') and
								(CoSerie=@CoSerie or @CoSerie='') and (idcliente='000000')
					end
			end
		else
			begin
				if @NuCuenta_Cliente is not null
					select @NuCuenta_Cliente as CtaContable
				else
					select top 1  CtaContable
					from MAPLANCUENTAS_CENTROCOSTOS_DOCUMENTO
					where 
					(CoCeCos=@CoCeCos or @CoCeCos='') and
					(CoTipoVenta=@CoTipoVenta or @CoTipoVenta='') and
					(CoSerie=@CoSerie or @CoSerie='') and (idcliente='000000')
			end
	end
	else
	begin
		select top 1  CtaContable
		from MAPLANCUENTAS_CENTROCOSTOS_DOCUMENTO
		where 
		(CoCeCos=@CoCeCos or @CoCeCos='') and
		(CoTipoVenta=@CoTipoVenta or @CoTipoVenta='') and
		(CoSerie=@CoSerie or @CoSerie='') and IDCLIENTE='000000'
		--(IDCliente=@IDCliente or (@IDCliente='' AND IDCLIENTE='000000')) 
	end
END;

