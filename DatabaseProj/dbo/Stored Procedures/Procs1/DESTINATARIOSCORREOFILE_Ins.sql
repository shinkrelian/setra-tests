﻿
CREATE Procedure [dbo].[DESTINATARIOSCORREOFILE_Ins]
@NuCorreo int,
@NoCuentaPara varchar(150),
@CoUsrIntPara char(4),
@CoUsrPrvPara char(6),
@CoUsrCliPara char(6),
@FlCuentaCopia	bit,
@UserMod char(4)
As
	Set NoCount On
	Insert Into .BDCORREOSETRA..DESTINATARIOSCORREOFILE (NuCorreo,NoCuentaPara,
	CoUsrIntPara,
	CoUsrPrvPara,
	CoUsrCliPara,
	FlCuentaCopia,
	UserMod)
	Values (@NuCorreo,@NoCuentaPara,
	Case When ltrim(rtrim(@CoUsrIntPara))='' Then Null Else @CoUsrIntPara End,
	Case When ltrim(rtrim(@CoUsrPrvPara))='' Then Null Else @CoUsrPrvPara End,
	Case When ltrim(rtrim(@CoUsrCliPara))='' Then Null Else @CoUsrCliPara End,
	@FlCuentaCopia,
	@UserMod)

