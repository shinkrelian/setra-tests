﻿CREATE Procedure [dbo].[MAOPCIONES_Ins]
	@IDOpc char(6),
    @Nombre varchar(50),
    @Descripcion varchar(50),
    @EsMenu	bit,
    @UserMod char(4)
As
	Set NoCount On
	
	INSERT INTO MAOPCIONES
           ([IDOpc]
           ,[Nombre]
           ,[Descripcion]
           ,EsMenu
           ,[UserMod]
           )
     VALUES
           (@IDOpc ,
            @Nombre,
            @Descripcion ,
            @EsMenu,
            @UserMod)
