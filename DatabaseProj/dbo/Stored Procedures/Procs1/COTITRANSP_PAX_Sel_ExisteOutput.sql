﻿
Create Procedure [dbo].[COTITRANSP_PAX_Sel_ExisteOutput]
	@IDTransporte	int,
	@IDPaxTransp	smallint,	
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDTransporte From COTITRANSP_PAX
		WHERE IDTransporte=@IDTransporte And IDPaxTransp=@IDPaxTransp)
		Set @pExiste=1
	Else
		Set @pExiste=0

