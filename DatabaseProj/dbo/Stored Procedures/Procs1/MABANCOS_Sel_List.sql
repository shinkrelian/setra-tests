﻿--JRF-20130618-Correcion de sintaxis.
CREATE Procedure [dbo].[MABANCOS_Sel_List]  
 @IDBanco char(3),  
 @Descripcion varchar(60)  
As  
 Set NoCount On  
   
 Select Sigla , Descripcion,   
 Case Moneda When 'N' Then 'Nacional' When 'E' Then 'Extranjera' End as Moneda,  
 IDBanco  
 From MABANCOS  
 Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')  
 and (IDBanco=@IDBanco Or LTRIM(RTRIM(@IDBanco))='')  
