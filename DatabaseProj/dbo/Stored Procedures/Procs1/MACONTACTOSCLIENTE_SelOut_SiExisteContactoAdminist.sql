﻿CREATE Procedure [dbo].[MACONTACTOSCLIENTE_SelOut_SiExisteContactoAdminist]
	@IDCliente char(6),
	@IDContacto char(3),
	@pbOk	Bit Output
As
	Set NoCount On
	
	If Exists(Select EsAdminist from MACONTACTOSCLIENTE Where IDCliente=@IDCliente
		And EsAdminist=1 And (IDContacto<>@IDContacto OR ltrim(rtrim(@IDContacto))=''))
		Set @pbOk = 1
	Else
		Set @pbOk = 0
