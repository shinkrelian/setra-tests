﻿CREATE PROCEDURE dbo.DOCUMENTO_MULTIPLE_Ins   
	--@NuDocum_Multiple int,
	@CoTipoDoc char(3)='',
	@NuSerie varchar(10)='',
	@NuDocum varchar(30)='',
	@FeEmision smalldatetime='01/01/1900',
	@FeRecepcion smalldatetime='01/01/1900',
	@FeVencimiento datetime='01/01/1900',
	@TxConcepto varchar(max)='',
	@CoProveedor char(6)='',
	@CoMoneda char(3)='',
	@SsIGV numeric(8,2)=0,
	@SsTotal numeric(9,2)=0,
	@SSTotal_Asignado numeric(9,2)=0,
	@UserMod char(4)=''
AS
BEGIN
Declare @NuDocum_Multiple int =(select Isnull(Max(NuDocum_Multiple),0)+1 from DOCUMENTO_MULTIPLE) 
 
 DECLARE @NuDocum2 varchar(30)=''

 if Ltrim(Rtrim(@NuDocum))=''                           
 begin
 Exec Correlativo_SelOutput 'DOCUMENTO_PROVEEDOR',1,10,@NuDocum2 output     
 end
  
	Insert Into dbo.DOCUMENTO_MULTIPLE
		(
			NuDocum_Multiple,
 			CoTipoDoc,
 			NuSerie,
 			NuDocum,
 			FeEmision,
 			FeRecepcion,
 			FeVencimiento,
 			TxConcepto,
 			CoProveedor,
 			CoMoneda,
 			SsIGV,
 			SsTotal,
			SSTotal_Asignado,
 			UserMod,
			Fl_Correlativo
 		)
	Values
		(
			@NuDocum_Multiple,
 			Case When Ltrim(Rtrim(@CoTipoDoc))='' Then Null Else @CoTipoDoc End,
 			Case When Ltrim(Rtrim(@NuSerie))='' Then Null Else @NuSerie End,
 			Case When Ltrim(Rtrim(@NuDocum))='' Then Null Else @NuDocum End,
 			Case When @FeEmision='01/01/1900' Then getdate() Else @FeEmision End,
 			Case When @FeRecepcion='01/01/1900' Then getdate() Else @FeRecepcion End,
 			Case When @FeVencimiento='01/01/1900' Then getdate() Else @FeVencimiento End,
 			Case When Ltrim(Rtrim(@TxConcepto))='' Then Null Else @TxConcepto End,
 			Case When Ltrim(Rtrim(@CoProveedor))='' Then Null Else @CoProveedor End,
 			Case When Ltrim(Rtrim(@CoMoneda))='' Then Null Else @CoMoneda End,
 			--Case When @SsIGV=0 Then Null Else @SsIGV End ,
			@SsIGV,
 			--Case When @SsTotal=0 Then Null Else @SsTotal End ,
			@SsTotal,
			Case When @SSTotal_Asignado=0 Then Null Else @SSTotal_Asignado End ,
 			@UserMod,
			Case When Ltrim(Rtrim(@NuDocum))='' Then CAST(1 AS BIT) Else CAST(0 AS BIT) End
 		)
END;
