﻿--JRF-20140804-Agregar Parametro para No Considerar la Moneda Ingresada
CREATE Procedure [dbo].[MAMONEDAS_Sel_Cbo]  
@IDMonedaNoCo char(3)
As  
 Set NoCount On  
 Select IDMoneda, Descripcion From MAMONEDAS  
 Where Activo='A' and IDMoneda <> @IDMonedaNoCo
