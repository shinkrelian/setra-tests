﻿
-----------------------------------------------------------------------------------------------------

--JHD-20150216- Se agrego el campo TxDefinicion
CREATE Procedure [dbo].[MAPLANCUENTAS_Ins]
@CtaContable varchar(8),
@Descripcion varchar(150),
@UserMod char(4),
@CoCtroCosto char(6),
@FlFacturacion bit,
@FlClientes bit,
@TxDefinicion varchar(255)=''
As
	Set NoCount On
	Insert Into MAPLANCUENTAS
		(CtaContable,
		Descripcion,
		UserMod,
		FecMod ,
		CoCtroCosto,
		FlFacturacion,
		FlClientes,
		TxDefinicion) 
		Values (@CtaContable,
		@Descripcion,
		@UserMod,
		GetDate(),
		Case When ltrim(rtrim(@CoCtroCosto))='' Then Null Else @CoCtroCosto End ,
		@FlFacturacion,
		@FlClientes,
		Case When ltrim(rtrim(@TxDefinicion))='' Then Null Else @TxDefinicion End
		)
