﻿CREATE PROCEDURE DBO.DOCUMENTO_INS_REEMPLAZO_NUDOCUM
@NuDocumAntes char(10), 
@NuDocumDespues char(10),
@IDTipoDoc char(3)
AS

--DECLARE @NuDocumDespues char(10)
 --Execute dbo.Correlativo_SelOutput 'DOCUMENTO',1,10,@NuDocumDespues output,@IDTipoDoc,'001'                


INSERT INTO DOCUMENTO(NuDocum,IDTipoDoc,IDCab,FeDocum,IDMoneda,SsTotalDocumUSD,CoEstado,UserMod,
IDCliente,FlDocManual,TxTitulo,SsIGV,NuDocumVinc,FlPorAjustePrecio,IDTipoDocVinc,
CoTipoVenta, CoCtaContable, FeEmisionVinc,CoCtroCosto,NuFileLibre, NuNtaVta, NuCuentaAnt,
FlCtaCambiada, FlAjustePrecioUpdMontos)
SELECT @NuDocumDespues,IDTipoDoc,IDCab,FeDocum,IDMoneda,SsTotalDocumUSD,'GD','0001',
IDCliente,FlDocManual,TxTitulo,SsIGV,NuDocumVinc,FlPorAjustePrecio,IDTipoDocVinc,
CoTipoVenta, CoCtaContable, FeEmisionVinc,CoCtroCosto,NuFileLibre, NuNtaVta, NuCuentaAnt,
FlCtaCambiada, FlAjustePrecioUpdMontos
 FROM DOCUMENTO WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes
 
PRINT @@ROWCOUNT 
 
INSERT INTO DOCUMENTO_DET_TEXTO(NuDocum,IDTipoDoc,NuDetalle,NoTexto,
SsTotal,UserMod)
SELECT @NuDocumDespues,IDTipoDoc,NuDetalle,NoTexto,
SsTotal,'0001' FROM DOCUMENTO_DET_TEXTO WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes

INSERT INTO DOCUMENTO_DET(NuDocum,IDTipoDoc,NuDetalle,IDProveedor,IDVoucher,NuOrdenServicio,
SsCompraNeto,SsIGVCosto,SsIGVSFE,SsTotalCosto,IDTipoOC,SsMargen,SsTotalCostoUSD,SsTotalDocumUSD,
UserMod,CoTipoDet,IDMonedaCosto,NoServicio,FlDocumentoManual)
SELECT @NuDocumDespues,IDTipoDoc,NuDetalle,IDProveedor,IDVoucher,NuOrdenServicio,
SsCompraNeto,SsIGVCosto,SsIGVSFE,SsTotalCosto,IDTipoOC,SsMargen,SsTotalCostoUSD,SsTotalDocumUSD,
'0001',CoTipoDet,IDMonedaCosto,NoServicio,FlDocumentoManual
FROM DOCUMENTO_DET
WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes

INSERT INTO INGRESO_DOCUMENTO(NuIngreso,NuDocum,IDTipoDoc,SsMonto,UserMod)
SELECT NuIngreso,@NuDocumDespues,IDTipoDoc,SsMonto,'0001' 
FROM INGRESO_DOCUMENTO WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes


If exists(Select NuDocum From DOCUMENTO Where IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes)-- And CoEstado<>'AN')
	Begin
	DELETE FROM INGRESO_DOCUMENTO WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes
	DELETE FROM DOCUMENTO_DET WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes
	DELETE FROM DOCUMENTO_DET_TEXTO WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes
	DELETE FROM DOCUMENTO WHERE IDTipoDoc=@IDTipoDoc AND NuDocum=@NuDocumAntes	
	End
Else
	Print @NuDocumAntes+' No Eliminado'

