﻿Create Procedure [dbo].[ACOMODOPAX_PROVEEDOR_InsxFile]
@IDCAB int,
@IDPax int,
@IDHabit tinyint,
@UserMod Char(4)
As
	Set NoCount On
	 Insert Into ACOMODOPAX_PROVEEDOR (IDReserva,IDPax,IDHabit,UserMod)
	 select IDReserva,@IDPax,@IDHabit,@UserMod from RESERVAS r Left Join MAPROVEEDORES p  On r.IDProveedor = p.IDProveedor
	 where IDCab = @IDCAB and p.IDTipoProv = '001'
