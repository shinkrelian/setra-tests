﻿Create Procedure [dbo].[MACONTACTOSCLIENTE_Sel_Pk]
	@IDContacto char(3),
	@IDCliente char(6)
As
	Set NoCount On
	
	Select * FROM MACONTACTOSCLIENTE 
    WHERE IDContacto=@IDContacto And IDCliente=@IDCliente
