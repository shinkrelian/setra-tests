﻿
Create Procedure COTICAB_UpdObserv
@IDCAB int,
@Observaciones text
As
	Set NoCount On
	Update COTICAB
		set Observaciones = Case When ltrim(rtrim(Cast (@Observaciones as varchar(max))))='' Then Null Else @Observaciones End
	Where IDCAB = @IDCAB
