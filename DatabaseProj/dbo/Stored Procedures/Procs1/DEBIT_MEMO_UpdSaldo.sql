﻿Create Procedure dbo.DEBIT_MEMO_UpdSaldo
@IDDebitMemo char(10),  
@Saldo numeric(10,2),
@UserMod char(4)  
As  
 Set NoCount On  
 Update DEBIT_MEMO   
  Set Saldo = @Saldo,  
   UserMod = @UserMod,  
   FecMod = GetDate()  
 Where IDDebitMemo = @IDDebitMemo  
