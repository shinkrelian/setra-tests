﻿
--JRF-20141107-Aplicando el Cambio de Moneda para ambos casos [Ordenes de Servicios-Voucher]    
--JHD-20141209-Agregar condicion para actualizar en Orden de Pago    
--HLF-20150212-@NuOrdComInt int=0,@NuVouOfiExtInt int=0, @NuPreSob int=0 ....
--JHD-20150815-Agregar ISNULL a las variables
--JHD-20150908-Agregar la renta de 4ta a los totales en recibos por honorarios
--JHD-20150928-@CoEstado CHAR(2)=''
--JHD-20150928-CoEstado= CASE WHEN @CoEstado<>'' then @CoEstado else  ( ...
--JHD-20150928-IF rtrim(ltrim(@MonedaFEgreso))<>'' ...
--JHD-20151022-Declare @SsTotalPST as numeric(9,2)=isnull((Select SUM(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob AND CoPago='EFE'),0)
--JHD-20151022-Declare @SumTotalOrig2 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then isnull(SsTotal_FEgreso,isnull(SsTotalOriginal,0)) Else 
--             isnull(SsTotalOriginal,0) End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR   
--JRF-JHD-20160330-...Update ORDEN_SERVICIO_DET_MONEDA set SsSal ...
--PPMG-20160412-@IDDocFormaEgreso int=0
--PPMG-20160412-IF @IDDocFormaEgreso>0 ...
CREATE Procedure [dbo].[DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc_Upd]      
@NuDocumProv int ,
@CoEstado CHAR(2)=''  
As      
Set NoCount On      
Declare @NuVoucher int=0,@NuOrdenServicio int=0,@MonedaFEgreso char(3)='',@CoOrdPag int=0,
	@NuOrdComInt int=0,@NuVouOfiExtInt int=0, @NuPreSob int=0,@SsIGV numeric(8,2)=0,@CoTipoDoc char(3)='',
	@IDDocFormaEgreso int=0
      
Select @NuVoucher=Isnull(NuVoucher,0),    
@NuOrdenServicio=Isnull(NuOrden_Servicio,0),    
@MonedaFEgreso=IsNull(CoMoneda_FEgreso,''),    
@CoOrdPag=Isnull(CoOrdPag,0),
@NuOrdComInt=ISNULL(NuOrdComInt,0),
@NuVouOfiExtInt=ISNULL(NuVouOfiExtInt,0),
@NuPreSob=ISNULL(NuPreSob,0),
@SsIGV=ISNULL(SsIGV,0),
@CoTipoDoc=ISNULL(CoTipoDoc,''),
@IDDocFormaEgreso=ISNULL(IDDocFormaEgreso,0)
from DOCUMENTO_PROVEEDOR where NuDocumProv=@NuDocumProv --and FlActivo=1    
      
      
if @NuVoucher > 0      
Begin    
--Declare @SumTotalOrig numeric(9,2)=isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) from DOCUMENTO_PROVEEDOR     
--           where FlActivo=1 and NuVoucher=@NuVoucher),0)

Declare @SumTotalOrig numeric(9,2)=
isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END )
from DOCUMENTO_PROVEEDOR     
           where FlActivo=1 and NuVoucher=@NuVoucher),0)

 Update VOUCHER_OPERACIONES       
 set SsSaldo= VOUCHER_OPERACIONES.SsMontoTotal-@SumTotalOrig-IsNull(VOUCHER_OPERACIONES.SsDifAceptada,0),    
 --CoEstado=Case When (VOUCHER_OPERACIONES.SsMontoTotal-@SumTotalOrig-IsNull(VOUCHER_OPERACIONES.SsDifAceptada,0)) > 0 and VOUCHER_OPERACIONES.CoEstado='AC' then 'PD' Else VOUCHER_OPERACIONES.CoEstado End      
 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (VOUCHER_OPERACIONES.SsMontoTotal-@SumTotalOrig-IsNull(VOUCHER_OPERACIONES.SsDifAceptada,0)) > 0 and VOUCHER_OPERACIONES.CoEstado='AC' then 'PD' Else VOUCHER_OPERACIONES.CoEstado End ) end     
 where IDVoucher=@NuVoucher   
 
 	IF rtrim(ltrim(@MonedaFEgreso))<>''
		Update VOUCHER_OPERACIONES_DET
		 --set SsSaldo= Isnull(VOUCHER_OPERACIONES_DET.SsSaldo,0)+@TotalOrig,
		 set SsSaldo= VOUCHER_OPERACIONES_DET.SsMontoTotal-@SumTotalOrig-IsNull(VOUCHER_OPERACIONES_DET.SsDifAceptada,0),    
		 --CoEstado= CASE WHEN @CoEstado<>'' then @CoEstado else  (Case When (Isnull(VOUCHER_OPERACIONES_DET.SsSaldo,0)+@TotalOrig) > 0 and VOUCHER_OPERACIONES_DET.CoEstado='AC' then 'PD' Else VOUCHER_OPERACIONES_DET.CoEstado End) END
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (VOUCHER_OPERACIONES_DET.SsMontoTotal-@SumTotalOrig-IsNull(VOUCHER_OPERACIONES_DET.SsDifAceptada,0)) > 0 and VOUCHER_OPERACIONES_DET.CoEstado='AC' then 'PD' Else VOUCHER_OPERACIONES_DET.CoEstado End ) end     
		 where IDVoucher=@NuVoucher AND CoMoneda=@MonedaFEgreso
End    

if @NuOrdenServicio > 0      
Begin      
 --Declare @SumTotalOrig2 numeric(9,2)=isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) from DOCUMENTO_PROVEEDOR     
 --Declare @SumTotalOrig2 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR    

 
 Declare @SumTotalOrig2 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then isnull(SsTotal_FEgreso,isnull(SsTotalOriginal,0)) Else isnull(SsTotalOriginal,0) End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR  where FlActivo=1 and NuOrden_Servicio=@NuOrdenServicio),0)      

 Update ORDEN_SERVICIO set SsSaldo= ORDEN_SERVICIO.SsMontoTotal- @SumTotalOrig2 - ISNULL(ORDEN_SERVICIO.SsDifAceptada,0),    
         --CoEstado=Case When (ORDEN_SERVICIO.SsMontoTotal- @SumTotalOrig2 - ISNULL(ORDEN_SERVICIO.SsDifAceptada,0)) > 0 and ORDEN_SERVICIO.CoEstado='AC' then 'PD' Else ORDEN_SERVICIO.CoEstado End      
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (ORDEN_SERVICIO.SsMontoTotal- @SumTotalOrig2 - ISNULL(ORDEN_SERVICIO.SsDifAceptada,0)) > 0 and ORDEN_SERVICIO.CoEstado='AC' then 'PD' Else ORDEN_SERVICIO.CoEstado End) END
 where NuOrden_Servicio=@NuOrdenServicio      

  Update ORDEN_SERVICIO_DET_MONEDA set SsSaldo= ORDEN_SERVICIO_DET_MONEDA.SsMontoTotal- @SumTotalOrig2 - ISNULL(ORDEN_SERVICIO_DET_MONEDA.SsDifAceptada,0),    
         --CoEstado=Case When (ORDEN_SERVICIO.SsMontoTotal- @SumTotalOrig2 - ISNULL(ORDEN_SERVICIO.SsDifAceptada,0)) > 0 and ORDEN_SERVICIO.CoEstado='AC' then 'PD' Else ORDEN_SERVICIO.CoEstado End      
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (ORDEN_SERVICIO_DET_MONEDA.SsMontoTotal- @SumTotalOrig2 - ISNULL(ORDEN_SERVICIO_DET_MONEDA.SsDifAceptada,0)) > 0 and ORDEN_SERVICIO_DET_MONEDA.CoEstado='AC' then 'PD' Else ORDEN_SERVICIO_DET_MONEDA.CoEstado End) END
 where NuOrden_Servicio=@NuOrdenServicio  and CoMoneda=@MonedaFEgreso

End      
if @CoOrdPag > 0      
Begin      
 --Declare @SumTotalOrig3 numeric(9,2)=isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) from DOCUMENTO_PROVEEDOR     
 Declare @SumTotalOrig3 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR     
           where FlActivo=1 and CoOrdPag=@CoOrdPag),0)
 Update ORDENPAGO set SsSaldo= ORDENPAGO.SsMontoTotal- @SumTotalOrig3 - ISNULL(ORDENPAGO.SsDifAceptada,0),    
         --CoEstado=Case When (ORDENPAGO.SsMontoTotal- @SumTotalOrig3 - ISNULL(ORDENPAGO.SsDifAceptada,0)) > 0 and ORDENPAGO.CoEstado='AC' then 'PD' Else ORDENPAGO.CoEstado End      
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (ORDENPAGO.SsMontoTotal- @SumTotalOrig3 - ISNULL(ORDENPAGO.SsDifAceptada,0)) > 0 and ORDENPAGO.CoEstado='AC' then 'PD' Else ORDENPAGO.CoEstado End) END
 where IDOrdPag=@CoOrdPag      
End      
  
if @NuOrdComInt > 0      
Begin      
 --Declare @SumTotalOrig4 numeric(9,2)=isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) from DOCUMENTO_PROVEEDOR     
 Declare @SumTotalOrig4 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR     








           where FlActivo=1 and NuOrdComInt=@NuOrdComInt),0)
 Update ORDENCOMPRA set SsSaldo= ORDENCOMPRA.SsTotal- @SumTotalOrig4 - ISNULL(ORDENCOMPRA.SsDifAceptada,0),    
         --CoEstado=Case When (ORDENCOMPRA.SsTotal- @SumTotalOrig4 - ISNULL(ORDENCOMPRA.SsDifAceptada,0)) > 0 and ORDENCOMPRA.CoEstado='AC' then 'PD' Else ORDENCOMPRA.CoEstado End      
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (ORDENCOMPRA.SsTotal- @SumTotalOrig4 - ISNULL(ORDENCOMPRA.SsDifAceptada,0)) > 0 and ORDENCOMPRA.CoEstado='AC' then 'PD' Else ORDENCOMPRA.CoEstado End) END
 where NuOrdComInt=@NuOrdComInt
End      

if @NuVouOfiExtInt > 0      
Begin      
 --Declare @SumTotalOrig5 numeric(9,2)=isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) from DOCUMENTO_PROVEEDOR     
 Declare @SumTotalOrig5 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR     








           where FlActivo=1 and NuVouOfiExtInt=@NuVouOfiExtInt),0)
 Update VOUCHER_OFICINA_EXTERNA set SsSaldo= VOUCHER_OFICINA_EXTERNA.SsTotal- @SumTotalOrig5 - ISNULL(VOUCHER_OFICINA_EXTERNA.SsDifAceptada,0),    
         --CoEstado=Case When (VOUCHER_OFICINA_EXTERNA.SsTotal- @SumTotalOrig5 - ISNULL(VOUCHER_OFICINA_EXTERNA.SsDifAceptada,0)) > 0 and VOUCHER_OFICINA_EXTERNA.CoEstado='AC' then 'PD' Else VOUCHER_OFICINA_EXTERNA.CoEstado End      
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (VOUCHER_OFICINA_EXTERNA.SsTotal- @SumTotalOrig5 - ISNULL(VOUCHER_OFICINA_EXTERNA.SsDifAceptada,0)) > 0 and VOUCHER_OFICINA_EXTERNA.CoEstado='AC' then 'PD' Else VOUCHER_OFICINA_EXTERNA.CoEstado End) END
 where NuVouOfiExtInt=@NuVouOfiExtInt
End      

if @NuPreSob > 0      
Begin      
 --Declare @SumTotalOrig6 numeric(9,2)=isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) from DOCUMENTO_PROVEEDOR     
 Declare @SumTotalOrig6 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR     

           where FlActivo=1 and NuPreSob=@NuPreSob),0)
 --Declare @SsTotalPST as numeric(9,2)=isnull((Select SUM(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob),0)
 Declare @SsTotalPST as numeric(9,2)=isnull((Select SUM(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob AND CoPago='EFE'),0)
 Update PRESUPUESTO_SOBRE set SsSaldo= @SsTotalPST- @SumTotalOrig6 - ISNULL(PRESUPUESTO_SOBRE.SsDifAceptada,0),    
         --CoEstado=Case When (@SsTotalPST- @SumTotalOrig6 - ISNULL(PRESUPUESTO_SOBRE.SsDifAceptada,0)) > 0 and PRESUPUESTO_SOBRE.CoEstado='AC' then 'PD' Else PRESUPUESTO_SOBRE.CoEstado End      
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (@SsTotalPST- @SumTotalOrig6 - ISNULL(PRESUPUESTO_SOBRE.SsDifAceptada,0)) > 0 and PRESUPUESTO_SOBRE.CoEstado='AC' then 'PD' Else PRESUPUESTO_SOBRE.CoEstado End) END
 where NuPreSob=@NuPreSob
End

IF @IDDocFormaEgreso>0
BEGIN
 Declare @SumTotalOrig7 numeric(9,2)=isnull((select Sum((Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End)+case when (RTRIM(LTRIM(cotipodoc))='RH' AND ISNULL(SSIGV,0)>0) THEN SSIGV ELSE 0 END) from DOCUMENTO_PROVEEDOR     
           where FlActivo=1 and IDDocFormaEgreso=@IDDocFormaEgreso),0)
 Update DOCUMENTO_FORMA_EGRESO set SsSaldo= DOCUMENTO_FORMA_EGRESO.SsMontoTotal- @SumTotalOrig7 - ISNULL(DOCUMENTO_FORMA_EGRESO.SsDifAceptada,0),    
         --CoEstado=Case When (ORDENPAGO.SsMontoTotal- @SumTotalOrig3 - ISNULL(ORDENPAGO.SsDifAceptada,0)) > 0 and ORDENPAGO.CoEstado='AC' then 'PD' Else ORDENPAGO.CoEstado End      
		 CoEstado=CASE WHEN rtrim(ltrim(@CoEstado))<>'' then @CoEstado else  (Case When (DOCUMENTO_FORMA_EGRESO.SsMontoTotal- @SumTotalOrig7 - ISNULL(DOCUMENTO_FORMA_EGRESO.SsDifAceptada,0)) > 0 and DOCUMENTO_FORMA_EGRESO.CoEstado='AC' then 'PD' Else DOCUMENTO_FORMA_EGRESO.CoEstado End) END
 where IDDocFormaEgreso=@IDDocFormaEgreso
 END
;


