﻿Create Procedure dbo.MAINCLUIDOOPERACIONES_IDIOMAS_Ins
@NuIncluido int,
@IDIdioma varchar(12),
@NoObservacion varchar(max),
@UserMod char(4)
As
	Set NoCount On
	Insert Into MAINCLUIDOOPERACIONES_IDIOMAS
		values (@NuIncluido,@IDIdioma,@NoObservacion,@UserMod,GETDATE())
