﻿Create Procedure dbo.COTICAB_UpdFlCorreoCotizacionAceptada
@IDCab int,
@UserMod char(4)
As
Set NoCount On
Update COTICAB 
	set FlCorreoEnviadoxCotAceptada=1,
		UserMod=@UserMod,
		FecMod=GETDATE()
where IDCAB=@IDCab
