﻿CREATE PROCEDURE [dbo].[ENTRADASINC_SAP_List_BTG]
@FePago1 smalldatetime='01/01/1900',
@FePago2 smalldatetime='01/01/1900',
@FechaINCusco1 smalldatetime='01/01/1900',
@FechaINCusco2 smalldatetime='01/01/1900'
AS
BEGIN
select Y.IDCAB,Y.IDFile,
Y.Tipo,
--Monto=cast(Y.Monto as int),
Monto=CAST(isnull((select  monto from ENTRADASINC_SAP where NuPreSob=y.NuPreSob and NuDetPSo=y.NuDetPSo and tipo = 'BTG' ),Y.Monto) AS INT),
 FechaINCusco,
 FechaPago,
 isnull(NuCodigo_ER,'') as NuCodigo_ER,
 y.NuPreSob,
 y.NuDetPSo

from (
select * from
(
select c.IDCAB,c.IDFile,
Tipo='BTG'
,Monto=isnull(pd.SsPreUni,0)*ISNULL(QtPax,0)
,(Select Min(Dia) From COTIDET Where IDCAB=c.IDCAB And IDubigeo='000066') as FechaINCusco
,pD.NuPreSob,pd.NuDetPSo 

from PRESUPUESTO_SOBRE_DET pd
inner join PRESUPUESTO_SOBRE p on p.NuPreSob=pd.NuPreSob
inner join MASERVICIOS_DET sd on sd.IDServicio_Det=pd.IDServicio_Det
inner join MASERVICIOS_DET sd1 on sd.IDServicio_Det_V=sd1.IDServicio_Det --and sd1.Tipo <> 'GUIDE'
inner join MASERVICIOS s on sd1.IDServicio=s.IDServicio
left join coticab c on p.IDCab=c.IDCAB

where pd.CoPago='PCM' AND s.IDServicio='CUZ00138' --TxServicio LIKE '%Entrance Fee Complete BTG%' 
and CoEstado='EN')
as x
where (FechaINCusco Between @FechaINCusco1 And @FechaINCusco2 Or Convert(Char(10),@FechaINCusco1,103)='01/01/1900')
) as Y left outer join ENTRADASINC_SAP e on Y.IDFile=e.IDFile and Y.Tipo=e.Tipo and Y.NuPreSob=e.NuPreSob and Y.NuDetPSo=e.NuDetPSo
Where (e.FechaPago Between @FePago1 And @FePago2 Or Convert(Char(10),@FePago1,103)='01/01/1900')
END;
