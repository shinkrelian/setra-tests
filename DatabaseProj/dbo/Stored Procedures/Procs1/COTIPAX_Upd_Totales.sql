﻿--HLF-20140825-Comentando todo y agregando   Select @Total=Sum(d.Total),  
--  @SSTotal=Sum(Case When p.IDTipoProv=@IDTipoProvHotel Then d.SSTotal Else d.Total End),  
--  @STTotal=Sum(Case When p.IDTipoProv=@IDTipoProvHotel Then d.STTotal Else d.Total End)  
--  From COTIDET d Inner Join COTIDET_PAX cdp On d.IDDET=cdp.IDDet And ...  
--Comentando:  
-- @IDDet int,  
-- @bSuma bit,    
-- @IDTipoProv char(3),  
--HLF-20140829-  
--           ,SSTotal=@SSTotal+@Total    
--           ,STTotal=@STTotal+@Total  
--@SSTotal=isnull(Sum(d.SSTotal),0),  
--@STTotal=isnull(Sum(d.STTotal),0)  
--Declare @ContieneTPL smallint ...  
--HLF-20140901-Actualizando campo FlTripleNA
CREATE Procedure [dbo].[COTIPAX_Upd_Totales]  
 @IDPax int,   
 @IDCab int,  
-- @IDDet int,  
-- @bSuma bit,  
-- @IDTipoProv char(3),  
 @UserMod char(4)  
As  
  
 Set NoCount On  
   
 Declare @Total numeric(8,2),@SSTotal numeric(12,4),@STTotal numeric(12,4)--@bProceder bit,   
 --@IDTipoProvHotel char(3)='001'  
  
  
 --If @IDPax=0     
 -- Begin  
 -- Select @Total=Sum(d.Total),@SSTotal=Sum(d.SSTotal),@STTotal=Sum(d.STTotal)  
 -- From COTIDET d   
 -- Where d.IDCAB=@IDCab  
    
 -- Update COTIPAX Set             
 --          Total=@Total  
 --          ,SSTotal=Case When @IDTipoProv=@IDTipoProvHotel Then @SSTotal Else @Total End  
 --          ,STTotal=Case When @IDTipoProv=@IDTipoProvHotel Then @STTotal Else @Total End  
 --          ,UserMod=@UserMod  
 --          ,FecMod=GETDATE()  
 --    Where IDCAB=@IDCab And (IDPax=@IDPax Or @IDPax=0)  
  
 -- End  
 --Else  
 -- Begin  
 -- Select @Total=d.Total,@SSTotal=d.SSTotal,@STTotal=d.STTotal  
 --  From COTIDET d   
 --  Where d.IDDET=@IDDet  
     
 -- If @bSuma=0  
 --  Begin  
 --  Set @Total=@Total*(-1)  
 --  Set @SSTotal=@SSTotal*(-1)  
 --  Set @STTotal=@STTotal*(-1)  
     
 --  --If Not Exists(Select IDPax From COTIDET_PAX Where IDPax=@IDPax)  
 --  If Exists(Select IDPax From COTIPAX Where IDPax=@IDPax And Actualizar=1)  
 --   Set @bProceder=1  
 --  Else  
 --   Set @bProceder=0  
  
     
 --  End  
 -- Else  
 --  Begin  
     
 --  --If Exists(Select IDPax From COTIDET_PAX Where IDPax=@IDPax)  
 --  If Exists(Select IDPax From COTIPAX Where IDPax=@IDPax And Actualizar=1)  
 --   Set @bProceder=1  
 --  Else  
 --   Set @bProceder=0  
  
 --  End  
 -- If @bProceder=1   
 --  Update COTIPAX Set             
 --  Total=Total+@Total  
 --  ,SSTotal=SSTotal+Case When @IDTipoProv=@IDTipoProvHotel Then @SSTotal Else @Total End  
 --  ,STTotal=STTotal+Case When @IDTipoProv=@IDTipoProvHotel Then @STTotal Else @Total End  
 --  ,Actualizar=0  
 --  ,UserMod=@UserMod  
 --  ,FecMod=GETDATE()  
 --  Where IDCAB=@IDCab And (IDPax=@IDPax Or @IDPax=0)     
       
 -- End  
  
  
  
  Select @Total=isnull(Sum(d.Total),0),  
  --@SSTotal=isnull(Sum(Case When p.IDTipoProv=@IDTipoProvHotel Then d.SSTotal Else d.Total End),0),  
  @SSTotal=isnull(Sum(d.SSTotal),0),  
  --@STTotal=isnull(Sum(Case When p.IDTipoProv=@IDTipoProvHotel Then d.STTotal Else d.Total End),0)  
  @STTotal=isnull(Sum(d.STTotal),0)  
  From COTIDET d Inner Join COTIDET_PAX cdp On d.IDDET=cdp.IDDet And   
  cdp.IDPax=@IDPax  
  Left Join MAPROVEEDORES p On d.IDProveedor=p.IDProveedor  
  Where d.IDCAB=@IDCab  
    
    
  --Declare @ContieneTPL smallint=(Select isnull(SUM(  
  --Case When (sd.IDServicio_DetxTriple is null And sd.Monto_tri is null And            
  --sd.Monto_tris is null And sd.IdHabitTriple = '000') Then 0 Else 1 End  
  --),0)  
  --From COTIDET cd Inner Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det  
  --Where IDCAB=@IDCab)  
  Declare @ContieneTPL smallint=dbo.FnTodosContieneTPLxIDCab(@IDCab)
      
  Update COTIPAX Set             
           Total=@Total  
           --,SSTotal=@SSTotal  
           ,SSTotal=@SSTotal+@Total  
           --,STTotal=@STTotal  
           --,STTotal=Case When @ContieneTPL=0 Then @STTotal+@Total Else 0 End  
           ,STTotal=@STTotal+@Total  
           ,FlTripleNA=Case When @ContieneTPL=0 Then 1 Else 0 End
           ,UserMod=@UserMod  
           ,FecMod=GETDATE()  
     Where IDCAB=@IDCab And IDPax=@IDPax   
     
     
