﻿--JRF-20160416- ...And CD.IncGuia=0
 CREATE Procedure dbo.COTIDET_UpdxDetPax
	@IDCab int,
	@UserMod char(4)
 As
	Set Nocount On
	
	Update COTIDET Set COTIDET.NroPax=X.PaxCotiPaxDet-ISNULL(COTIDET.NroLiberados,0),
		UserMod=@UserMod, FecMod=GETDATE()
	From
	(SELECT CD.IDDET, 
		(SELECT COUNT(*) FROM COTIDET_PAX  WHERE IDDet=CD.IDDET and FlDesactNoShow=0) as PaxCotiPaxDet
	FROM COTIDET CD 
	WHERE CD.IDCAB=@IDCab And CD.IncGuia=0
	) AS X
	WHERE COTIDET.IDCAB=@IDCab And COTIDET.IDDET=X.IDDET
