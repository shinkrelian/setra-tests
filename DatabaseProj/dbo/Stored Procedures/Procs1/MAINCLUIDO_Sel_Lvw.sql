﻿
CREATE Procedure [dbo].[MAINCLUIDO_Sel_Lvw]
	@IDIncluido int,
	@DescAbrev varchar(100)
as

	Set NoCount On
	
	Select DescAbrev
	from MAINCLUIDO
	Where (DescAbrev like '%'+@DescAbrev+'%' Or LTRIM(rtrim(@DescAbrev))='')
	And (IDIncluido=@IDIncluido Or @IDIncluido = 0)
	
