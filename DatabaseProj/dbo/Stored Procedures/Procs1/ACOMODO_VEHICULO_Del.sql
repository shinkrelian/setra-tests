﻿
Create Procedure dbo.ACOMODO_VEHICULO_Del
	@NuVehiculo smallint,
	@IDDet int
As
	Set Nocount on

	Delete From ACOMODO_VEHICULO
	Where NuVehiculo=@NuVehiculo And IDDet=@IDDet
