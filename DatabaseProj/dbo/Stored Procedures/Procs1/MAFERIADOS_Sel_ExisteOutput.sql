﻿CREATE Procedure [dbo].[MAFERIADOS_Sel_ExisteOutput]
	@FechaFeriado	date,
	@IDPais	char(6),
	@pbExists	Bit Output
As
	Set NoCount On
	
	If Exists(Select FechaFeriado From MAFERIADOS Where FechaFeriado=@FechaFeriado
		And IDPais=@IDPais) 
		
		Set @pbExists = 1
	Else
		Set @pbExists = 0
