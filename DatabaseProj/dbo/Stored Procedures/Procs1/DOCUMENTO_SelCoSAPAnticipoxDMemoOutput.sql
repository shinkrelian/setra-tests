﻿Create Procedure dbo.DOCUMENTO_SelCoSAPAnticipoxDMemoOutput
	@NuDebitMemo char(10),
	@pCoSAP varchar(30) output
As
	Set NoCount on

	Set @pCoSAP = ''
	Select @pCoSAP=CoSAP+'/'+Left(NuDocum,3)+'-'+right(NuDocum,7) From DOCUMENTO Where CoDebitMemoAnticipo=@NuDebitMemo And isnull(CoSAP,'')<>''
		
