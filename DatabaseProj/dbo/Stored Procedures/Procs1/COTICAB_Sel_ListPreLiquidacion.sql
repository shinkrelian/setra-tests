﻿
 
--JRF-20131017-Agregando Filtro de Fechas Inicio de Cotizacion.  
--JRF-20131018-Agregando Filtro de Fechas por asignación de ejecutivo de Reservas. 
--JRF-HLF-20140514- Optimizar el --exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB x El IDCAB in (...) 
--JHD-20150716-Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,
--JHD-20150716- ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes,    
--JHD-20150716- uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe
--HLF-20151203-And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)      
--HLF-20160216-isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt, Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt   
CREATE Procedure dbo.COTICAB_Sel_ListPreLiquidacion    
 @IDUsuario char(4),        
 @Cotizacion char(8),        
 @IDFile char(8),        
 @DescCliente varchar(60),        
 @Titulo varchar(100),         
 @NombrePax varchar(100),  
 @FecIniciRango1 smalldatetime,                  
 @FecIniciRango2 smalldatetime,  
 @FecAsigReserRango1 smalldatetime,  
 @FecAsigReserRango2 smalldatetime,
 @FlHistorico bit=1
As        
 Set NoCount On  
     
 SELECT UsuarioVen,UsuarioRes,CorreoEjecReservas,UsuarioOpe,Fecha,Cotizacion,IDFile,NuPedido,NuPedInt,DescCliente,Titulo,Ruta,Estado,  
 IDCAB,NroPax,IDNotaVenta  
 FROM    
 (        
 --Select uv.Nombre as UsuarioVen,
 Select (uv.Nombre+' '+ISNULL(UV.TxApellidos,'')) as UsuarioVen,
 --ur.Nombre as UsuarioRes,        
 ur.Nombre+' '+ISNULL(ur.TxApellidos,'') as UsuarioRes,
 IsNull(ur.Correo,'') as CorreoEjecReservas,        
 --uo.Nombre as UsuarioOpe         
 uo.Nombre+' '+ISNULL(uo.TxApellidos,'') as UsuarioOpe  
 , Convert(varchar,c.FechaOperaciones,103) as Fecha,     
 c.Cotizacion,c.IDFile, 
 isnull(pd.NuPedido,'') as NuPedido, isnull(c.NuPedInt,0) as NuPedInt,  
 cl.RazonComercial as DescCliente, c.Titulo,         
 '' As Ruta,        
 '' as Estado,        
 c.IDCAB,      
 c.NroPax,        
 (select IDNotaVenta from NOTA_VENTA Where IDCab=c.IDCAB And IDEstado<>'AN') as IDNotaVenta,  
 Case When FechaAsigReserva is null then CAST('01/01/1900' as smalldatetime) else Cast(convert(char(10),FechaAsigReserva,103) as smalldatetime) End As FechaAsigReserva             
 From COTICAB c          
 Left Join MAUSUARIOS uv on c.IDUsuario=uv.IDUsuario        
 Left Join MAUSUARIOS ur on c.IDUsuarioRes=ur.IDUsuario        
 Left Join MAUSUARIOS uo On c.IDUsuarioOpe = uo.IDUsuario        
 Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente        
 Left Join NOTA_VENTA nv On c.IDCAB=nv.IDCab    
 Left Join PEDIDO pd On c.NuPedInt=pd.NuPedInt    
 Where         
 (c.Cotizacion Like '%'+ltrim(rtrim(@Cotizacion))+'%' Or ltrim(rtrim(@Cotizacion))='') And        
 (c.IDUsuarioOpe=@IDUsuario Or ltrim(rtrim(@IDUsuario))='') And        
 (ltrim(rtrim(@IDFile))='' Or c.IDFile like '%' + ltrim(rtrim(@IDFile)) + '%') And    
 (cl.RazonSocial Like '%'+@DescCliente+'%' Or ltrim(rtrim(@DescCliente))='') And        
 (c.Titulo Like '%'+@Titulo+'%' Or ltrim(rtrim(@Titulo))='') And              
 c.Estado='A'         
 And c.IDUsuarioOpe<>'0000'    
 --And exists(select IDCab from COTIPAX px Where IDCab=c.IDCAB       
 And c.IDCAB in (select px.IDCab from COTIPAX px Where px.IDCab=c.IDCAB       
 And (px.Nombres+' '+px.Apellidos) Like '%'+@NombrePax+'%' Or ltrim(rtrim(@NombrePax))='')     
 And (c.FlHistorico=@FlHistorico Or @FlHistorico=1) 
 ) AS X    
 WHERE NOT IDNotaVenta IS NULL    
 And ((LTRIM(rtrim(convert(char(10),@FecIniciRango1,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecIniciRango2,103)))='01/01/1900') Or  
   cast(convert(char(10),X.Fecha,103) as smalldatetime) between @FecIniciRango1 and @FecIniciRango2)   
 And ((LTRIM(rtrim(convert(char(10),@FecAsigReserRango2,103)))='01/01/1900' and LTRIM(rtrim(convert(char(10),@FecAsigReserRango2,103)))='01/01/1900') Or  
   cast(convert(char(10),X.FechaAsigReserva,103) as smalldatetime) between @FecAsigReserRango1 and @FecAsigReserRango2)  
 Order by  Fecha,Cotizacion     




