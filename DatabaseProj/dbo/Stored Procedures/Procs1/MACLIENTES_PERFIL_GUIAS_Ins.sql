﻿

---------------------------------------------------------------------------------------------------------------

--create
CREATE PROCEDURE [MACLIENTES_PERFIL_GUIAS_Ins]
	@CoCliente char(6)='',
	@CoProveedor char(6)='',
	@UserMod char(4)
AS
BEGIN
	Insert Into [dbo].[MACLIENTES_PERFIL_GUIAS]
		(
			[CoCliente],
 			[CoProveedor],
 			[UserMod]
 		)
	Values
		(
			@CoCliente,
 			@CoProveedor,
 			@UserMod
 		)
END;
