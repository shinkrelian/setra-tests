﻿Create Procedure [dbo].[MACONTACTOSCLIENTE_Sel_Lvw]
	@IDContacto	char(3),
	@IDCliente	char(6),
	@Nombre varchar(80)
As
	Set NoCount On
	
	Select Nombres+' '+Apellidos as Nombre
	From MACONTACTOSCLIENTE
	Where (LTRIM(RTRIM(@Nombre))='' Or (Nombres+' '+Apellidos Like '%'+@Nombre+'%'))
	And (ltrim(rtrim(@IDContacto))='' Or (IDContacto <>@IDContacto And  IDCliente <>@IDCliente))
	Order by 1
