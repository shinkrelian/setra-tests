﻿
Create Procedure dbo.CORREOFILE_Upd_CorreoLeido
@NuCorreo int,
@UserMod char(4)
As
	Set NoCount On
	Update [dbo].[CORREOFILE]
		set  [FlLeido] = 1
			,[UserMod] = @UserMod
			,[FecMod] = GETDATE()
	where [NuCorreo] = @NuCorreo
