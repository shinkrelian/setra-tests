﻿--JRF-20140129-Nuevo campo NoConsideraAnio
--PPMG-20151118-@Descripcion varchar(max)
CREATE Procedure [dbo].[MAFECHASNAC_Ins]
 @Fecha smalldatetime,
 @IDUbigeo char(6),
 @Descripcion varchar(max),
 @NoConsideraAnio bit,
 @UserMod char(4)
AS
BEGIN
 Set NoCount On

 INSERT INTO MAFECHASNAC
           ([Fecha]
           ,[IDUbigeo]
           ,[Descripcion]
           ,[NoConsideraAnio]
		   ,[UserMod]
           )
     VALUES
           (@Fecha,
           @IDUbigeo,
           @Descripcion,
           @NoConsideraAnio,
           @UserMod)
END
