﻿--JHD-20150806-Se agregaron campos nuevos para SAP
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_DET_Sel_List]
	@NuDocumProv int=0,
	@NuDocumProvDet int=0
AS
BEGIN
SELECT        --dd.NuDocumProv,
dd.NuDocumProvDet,
dd.CoProducto,
Producto=isnull((select NoProducto from MAPRODUCTOS where CoProducto=dd.coproducto),''),
dd.coalmacen,
Almacen=isnull((select NoAlmacen from MAALMACEN where coalmacen=dd.coalmacen),''),
dd.CoCtaContab,
CuentaContable = isnull((select CtaContable+' - '+ Descripcion from MAPLANCUENTAS where CtaContable=dd.CoCtaContab),''),
QtCantidad=isnull(dd.QtCantidad,0),
SSPrecUni=isnull(dd.SSPrecUni,0),
SsIGV=isnull(dd.SsIGV,0),
SsMonto=isnull(dd.SsMonto,0),
SsTotal=isnull(dd.SsTotal,0)
FROM            DOCUMENTO_PROVEEDOR_DET dd INNER JOIN
                      DOCUMENTO_PROVEEDOR d ON dd.NuDocumProv = d.NuDocumProv 
WHERE     (dd.NuDocumProv = @NuDocumProv or @NuDocumProv =0 ) AND
		   (dd.NuDocumProvDet = @NuDocumProvDet or @NuDocumProvDet=0)
END;
