﻿--JRF-20130710-Nuevo formato de Direccinon    
--JRF-20130903-Agregar el campo de IDCliente
--JRF-20140930-Considerar al FecVencim del DebitMemoResumen.
CREATE Procedure DEBIT_MEMO_GenPDFResumen      
@IDDetMemoResumen char(10)      
As      
 Set NoCount On      
       
 Select db.IDDebitMemoResumen,c.IDFile,'  ' as Correlativo,
 --db.FecVencim,      
 (select dm2.FecVencim from DEBIT_MEMO dm2 where dm2.IDDebitMemo= db.IDDebitMemoResumen) as FecVencim,
 db.Cliente as Client,isnull(cl.NumIdentidad,'') As RucDI,      
 (select IsNull(Titulo,'') from DEBIT_MEMO where IDDebitMemo = @IDDetMemoResumen)      
 as Ref, cl.Direccion + Isnull(' - '+cl.CodPostal,'') + Isnull(' - '+cl.Ciudad,'') + Isnull(' - '+ub.Descripcion,'') as Direcc,      
 Convert(char(10),db.FechaIn,103) As FechaIn,      
 Convert(char(10),db.FechaOut,103) As FechaOut,      
 db.Pax as NroPax ,bc.NroCuenta,      
 isnull(bc.SWIFT,'') As Swift,bc.Descripcion As NomBanc,          
    isnull(bc.Direccion,'') As Direccion,      
    (select SUM(Total) from DEBIT_MEMO db2 where db2.IDDebitMemoResumen=@IDDetMemoResumen)TotalDebitMemo,      
          
    ROW_NUMBER() OVER(ORDER BY db.FechaIn) as Ord,      
    'REF  : '+db.Titulo+' DEBIT MEMO : '+SUBSTRING(db.IDDebitMemo,0,9)+'-'+SUBSTRING(db.IDDebitMemo,9,9)+CHAR(10)+      
     'FILE : '+c.IDFile +' IN : ' +CONVERT(Char(10),db.FechaIn,103) + ' OUT : ' +CONVERT(Char(10),db.FechaOut,103) + ' N°PAX :' + cast(db.Pax as varchar(8))      
 As Descripcion,db.Total      
          
 from DEBIT_MEMO db Left Join COTICAB c On db.IDCab = c.IDCAB      
 Left Join MACLIENTES cl On db.IDCliente = cl.IDCliente      
 Left Join MABANCOS bc On bc.IDBanco = c.IDBanco      
 Left Join MAUBIGEO ub On cl.IDCiudad = ub.IDubigeo    
 where IDDebitMemoResumen = @IDDetMemoResumen      
 order by CAST(db.FechaIn as smalldatetime)      
