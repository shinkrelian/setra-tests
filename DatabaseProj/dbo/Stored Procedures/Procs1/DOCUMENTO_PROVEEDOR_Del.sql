﻿
-------------------------------------------------------------------------

CREATE PROCEDURE [DOCUMENTO_PROVEEDOR_Del]
	@NuDocumProv int
AS
BEGIN
	Delete [dbo].[DOCUMENTO_PROVEEDOR]
	Where 
		[NuDocumProv] = @NuDocumProv
END;
