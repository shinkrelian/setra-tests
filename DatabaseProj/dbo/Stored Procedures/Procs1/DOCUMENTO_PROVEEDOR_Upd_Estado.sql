﻿
-----------------------------------------------------------------------------------------------------------------------------------------

/*
--alter
ALTER PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Upd]
	@NuDocumProv int,
	@NuVoucher int,
	@NuDocum char(10),
	@CoTipoDoc char(3),
	@FeEmision smalldatetime,
	@FeRecepcion smalldatetime,
	@CoTipoDetraccion CHAR(5),
	@CoMoneda char(3),
	@SsIGV numeric(8,2),
	@SsMonto numeric(9,2),
	--@CoEstado char(2),
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[DOCUMENTO_PROVEEDOR]
	Set
		[NuVoucher] = @NuVoucher,
 		[NuDocum] = @NuDocum,
 		[CoTipoDoc] = @CoTipoDoc,
 		[FeEmision] = @FeEmision,
 		[FeRecepcion] = @FeRecepcion,
 		--[CoTipoDetraccion] = Case When @CoTipoDetraccion='' Then Null Else @CoTipoDetraccion End,
 		[CoTipoDetraccion] = @CoTipoDetraccion,
 		[CoMoneda] = @CoMoneda,
 		[SsIGV] = @SsIGV,
 		[SsMonto] = @SsMonto,
 		--[CoEstado] = @CoEstado,
 		[UserMod] = @UserMod,
 		fecmod=GETDATE()
 	Where 
		[NuDocumProv] = @NuDocumProv
END;
GO
*/
------------------------------------------------------------------------------------------------------------

--ALTER
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Upd_Estado]
	@NuDocumProv int,
	--@CoEstado char(2),
	@FlActivo bit=0,
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[DOCUMENTO_PROVEEDOR]
	Set
		FlActivo = @FlActivo,
 		[UserMod] = @UserMod,
 		fecmod=GETDATE()
 	Where 
		[NuDocumProv] = @NuDocumProv
END;
