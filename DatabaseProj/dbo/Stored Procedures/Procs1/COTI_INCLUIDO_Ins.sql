﻿--JRF-20140312-Nuevo campo para el Orden
CREATE Procedure [dbo].[COTI_INCLUIDO_Ins]      
 @IDCAB int,      
 @IDIncluido int,     
 @Orden tinyint,
 @UserMod char(4)      
As      
 Set NoCount On  
      
 Insert into COTI_INCLUIDO      
 (IDCAB,Orden,IDIncluido,UserMod)      
 values      
 (@IDCAB,@Orden,@IDIncluido,@UserMod)  
