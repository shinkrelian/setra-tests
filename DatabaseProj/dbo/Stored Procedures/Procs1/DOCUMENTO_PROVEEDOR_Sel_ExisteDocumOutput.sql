﻿
CREATE Procedure dbo.DOCUMENTO_PROVEEDOR_Sel_ExisteDocumOutput
	@NuDocum varchar(30),
	@NuSerie varchar(10),
	@CoProveedor char(6),
	@IDCab	int,
	@pExists bit output
As

	Set NoCount On
	Set @pExists=0

	If Exists(Select NuDocum From DOCUMENTO_PROVEEDOR Where isnull(IDCab,0)=@IDCab 
		And NuDocum=@NuDocum And isnull(NuSerie,0)=@NuSerie
		And CoProveedor=@CoProveedor)
		Set @pExists=1

