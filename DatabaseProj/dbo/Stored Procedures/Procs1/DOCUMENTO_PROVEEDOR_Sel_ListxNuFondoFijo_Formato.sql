﻿--JHD 20150610 - Redondear a 2 decimales
--JHD 20150611 - From #TMP_Documentos where item>2 and FlActivo=1 Order by item    
--JHD 20150702 - isnull((select idfile from coticab where idcab=dp.IDCab),'') as IDFIle,
--JHD 20150702 - Percepcion=isnull(case when dp.CoMoneda='SOL' THEN DP.SSPercepcion
 --JHD-20150826- NuDocum= case when FlCorrelativo=1 then '' else
				-- case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
				--end,
 --JHD-20150907- Se agrego la columna Num_Interno, la cual contiene el codigo interno de SAP.
CREATE PROCEDURE [dbo].[DOCUMENTO_PROVEEDOR_Sel_ListxNuFondoFijo_Formato]
 
 @NuFondoFijo int--=6 
AS    
BEGIN   
SET NOCOUNT ON   

--REEMBOLSO

declare @item as int=0
declare @total as numeric(12,4)=0
declare @saldo as numeric(12,4)=0

select Item=0,
NuDocumProv=0,    

CoTipoFondoFijo='REE',
TipoFondoFijo='REEMBOLSO',

FeEmision=ff1.FeEmision_Reembolso,    
	 CoTipoDoc=ff1.CoTipoDoc_Reembolso,     
      
	NuDocum=ff1.NuDocum_Reembolso,
	Num_Interno='',
	 Centro_Costos='',
	'' as IDFIle,
	NumIdentidad='',
	NombreCorto='',
	TxConcepto='REEMBOLSO FONDO FIJO '+''+RTRIM(LTRIM(ff.CoPrefijo))+' N° ' +isnull(ff.NuCodigo,''),
	Neto_Soles=case when ff.CoMoneda='SOL' THEN (ff.SSMonto-isnull(ff.SsSaldo,0))
			   when ff.CoMoneda='USD' THEN DBO.FnCambioMoneda((ff.SSMonto-isnull(ff.SsSaldo,0)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso))
			   else DBO.FnCambioMoneda(DBO.FnCambioMoneda((ff.SSMonto-isnull(ff.SsSaldo,0)),ff.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=ff.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso))  
			   END,
	IGV=0,

	Percepcion=0,

	Total=0,

	Saldo=case when ff.CoMoneda='SOL' THEN (ff.SSMonto-isnull(ff.SsSaldo,0))
			   when ff.CoMoneda='USD' THEN DBO.FnCambioMoneda((ff.SSMonto-isnull(ff.SsSaldo,0)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso))
			   else DBO.FnCambioMoneda(DBO.FnCambioMoneda((ff.SSMonto-isnull(ff.SsSaldo,0)),ff.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=ff.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso))  
			   END,

	Orden_TipoFondoFijo=0,
	FlActivo=cast(1 as bit)
INTO #TMP_Documentos
from MAFONDOFIJO ff --1 --where NuFondoFijo=(select NuFondoFijo_Reembolso from MAFONDOFIJO f where f.NuFondoFijo=@NuFondoFijo)
left join mafondofijo ff1 on ff.NuFondoFijo=ff1.NuFondoFijo_Reembolso --6
where ff1.NuFondoFijo=@NuFondoFijo
UNION

--SALDO REEMBOLSO
select Item=1,
NuDocumProv=0,    

CoTipoFondoFijo='REE',
TipoFondoFijo='REEMBOLSO',

FeEmision=ff1.FeEmision_Reembolso,    
	 CoTipoDoc=ff1.CoTipoDoc_Reembolso,     
      
	NuDocum=ff1.NuDocum_Reembolso,
	Num_Interno='',
	 Centro_Costos='',
	'' as IDFIle,
	NumIdentidad='',
	NombreCorto='',
	TxConcepto='SALDO FONDO FIJO '+''+RTRIM(LTRIM(ff.CoPrefijo))+' N° ' +isnull(ff.NuCodigo,''),
	Neto_Soles=case when ff.CoMoneda='SOL' THEN (isnull(ff.SsSaldo,0))
			   when ff.CoMoneda='USD' THEN DBO.FnCambioMoneda((isnull(ff.SsSaldo,0)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso))
			   else DBO.FnCambioMoneda(DBO.FnCambioMoneda((isnull(ff.SsSaldo,0)),ff.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=ff.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso)) 

 
			   END,
	IGV=0,

	Percepcion=0,

	Total=0,

	Saldo=case when ff.CoMoneda='SOL' THEN (isnull(ff.SsSaldo,0))
			   when ff.CoMoneda='USD' THEN DBO.FnCambioMoneda((isnull(ff.SsSaldo,0)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso))
			   else DBO.FnCambioMoneda(DBO.FnCambioMoneda((isnull(ff.SsSaldo,0)),ff.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=ff.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff.FeEmision_Reembolso)) 

 
			   END,

	Orden_TipoFondoFijo=0,
	FlActivo=cast(1 as bit)

from MAFONDOFIJO ff --1 --where NuFondoFijo=(select NuFondoFijo_Reembolso from MAFONDOFIJO f where f.NuFondoFijo=@NuFondoFijo)
left join mafondofijo ff1 on ff.NuFondoFijo=ff1.NuFondoFijo_Reembolso --6
where ff1.NuFondoFijo=@NuFondoFijo

UNION
--TOTAL REEMBOLSO
select Item=2,
NuDocumProv=0,    

CoTipoFondoFijo='REE',
TipoFondoFijo='REEMBOLSO',

FeEmision=NULL,    
	 CoTipoDoc='',     
      
	NuDocum='',
	Num_Interno='',
	 Centro_Costos='',
	'' as IDFIle,
	NumIdentidad='',
	NombreCorto='',
	TxConcepto='',
	Neto_Soles=0,
	IGV=0,

	Percepcion=0,

	Total=0,

	Saldo=case when ff1.CoMoneda='SOL' THEN (isnull(ff1.SSMonto,0))
			   when ff1.CoMoneda='USD' THEN DBO.FnCambioMoneda((isnull(ff1.SSMonto,0)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff1.FeEmision_Reembolso))
			   else DBO.FnCambioMoneda(DBO.FnCambioMoneda((isnull(ff1.SSMonto,0)),ff1.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=ff1.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=ff1.FeEmision_Reembolso))  
			   END,

	Orden_TipoFondoFijo=0,
	FlActivo=cast(1 as bit)

from  mafondofijo ff1 
where ff1.NuFondoFijo=@NuFondoFijo

UNION

SELECT   Row_Number() over (Order By Orden_TipoFondoFijo,FeEmision,NuDocumProv)+2 As Item,*

FROM
(
 SELECT
  dp.NuDocumProv,    

DP.CoTipoFondoFijo,
TipoFondoFijo= case when DP.CoTipoFondoFijo='GPE' THEN 'GASTOS DE PERSONAL'
				when DP.CoTipoFondoFijo='SUM' THEN 'SUMINISTROS'
				when DP.CoTipoFondoFijo='COU' THEN 'COURIER'
				when DP.CoTipoFondoFijo='PUB' THEN 'PUBLICIDAD Y REPRESENTACIÓN'
				when DP.CoTipoFondoFijo='MAN' THEN 'MANTENIMIENTO, REPARACIÓN Y FERRETERIA'
				when DP.CoTipoFondoFijo='VAR' THEN 'VARIOS'
				when DP.CoTipoFondoFijo='CRI' THEN 'COSTO RECEPTIVO / INTERNO (FILE)'
				end,

dp.FeEmision,    
	 dp.CoTipoDoc,     
          --td.Descripcion,
	--NuDocum=case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum,
	 NuDocum= case when FlCorrelativo=1 then '' else
				 case when dp.NuSerie is null then '' else dp.NuSerie+'-' end + dp.NuDocum
			end,
	Num_Interno=isnull(dp.CardCodeSAP,''),
	isnull(dp.CoCeCos,'')+'-'+isnull(cc.Descripcion,'') AS Centro_Costos,
	--'' as IDFIle,
	isnull((select idfile from coticab where idcab=dp.IDCab),'') as IDFIle,
	pr.NumIdentidad,
	pr.NombreCorto,
	dp.TxConcepto,
	Neto_Soles=case when dp.CoMoneda='SOL' THEN DP.SsNeto
	when dp.CoMoneda='USD' THEN DBO.FnCambioMoneda(DP.SsNeto,'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))
	--when dp.CoMoneda='CLP' THEN DBO.FnCambioMoneda(DBO.FnCambioMoneda(DP.SsNeto,'CLP','USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda='CLP')),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))  
	else DBO.FnCambioMoneda(DBO.FnCambioMoneda(DP.SsNeto,dp.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=dp.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))  
	end,

	IGV=case when dp.CoMoneda='SOL' THEN DP.SsIGV
	when dp.CoMoneda='USD' THEN DBO.FnCambioMoneda(DP.SsIGV,'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))
	else DBO.FnCambioMoneda(DBO.FnCambioMoneda(DP.SsIGV,dp.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=dp.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))  
	end,

	Percepcion=isnull(case when dp.CoMoneda='SOL' THEN DP.SSPercepcion
	when dp.CoMoneda='USD' THEN DBO.FnCambioMoneda(DP.SSPercepcion,'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))
	else DBO.FnCambioMoneda(DBO.FnCambioMoneda(DP.SSPercepcion,dp.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=dp.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))  
	end,0),

	Total=case when dp.CoMoneda='SOL' THEN DP.SSTotal
	when dp.CoMoneda='USD' THEN DBO.FnCambioMoneda(DP.SSTotal,'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))
	else DBO.FnCambioMoneda(DBO.FnCambioMoneda(DP.SSTotal,dp.CoMoneda,'USD',(select top 1 SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=dp.CoMoneda)),'USD','SOL',(select top 1 valventa from MATIPOCAMBIO where fecha=dp.FeEmision))  
	end,

	Saldo=0,

	Orden_TipoFondoFijo= case when DP.CoTipoFondoFijo='GPE' THEN 1
				when DP.CoTipoFondoFijo='SUM' THEN 2
				when DP.CoTipoFondoFijo='COU' THEN 3
				when DP.CoTipoFondoFijo='PUB' THEN 4
				when DP.CoTipoFondoFijo='MAN' THEN 5
				when DP.CoTipoFondoFijo='VAR' THEN 6
				when DP.CoTipoFondoFijo='CRI' THEN 7
				end,
	dp.FlActivo

 
FROM         DOCUMENTO_PROVEEDOR dp   
INNER JOIN  MATIPODOC td ON dp.CoTipoDoc = td.IDtipodoc INNER JOIN    
                      MAMONEDAS mo ON dp.CoMoneda = mo.IDMoneda    
left join MACENTROCOSTOS cc on cc.CoCeCos=dp.CoCeCos
left join MAPROVEEDORES pr on pr.IDProveedor=dp.CoProveedor
WHERE   
(dp.NuFondoFijo = @NuFondoFijo ) 
)
AS X
order by ITem,Orden_TipoFondoFijo,FeEmision,NuDocumProv

set @saldo=(select saldo from #TMP_Documentos where item=2)

Declare curDocumentos Cursor For Select item,total
 From #TMP_Documentos where item>2 and FlActivo=1 Order by item     
 Open curDocumentos      
 Fetch Next From curDocumentos Into @item,@total    
 While @@FETCH_STATUS=0      
 Begin      
	update #TMP_Documentos set saldo=@saldo-@total
	where item=@item
	set @saldo=@saldo-@total
 Fetch Next From curDocumentos Into @item,@total
 End      
      
 Close curDocumentos      
 Deallocate curDocumentos   

SELECT
Item,
NuDocumProv,    
CoTipoFondoFijo,
TipoFondoFijo,
FeEmision,    
CoTipoDoc,     
NuDocum,
Num_Interno,
Centro_Costos,
IDFIle,
NumIdentidad,
NombreCorto,
TxConcepto,
Neto_Soles=cast(round(Neto_Soles,2) as numeric(12,2)),
IGV=cast(round(IGV,2) as numeric(12,2)),
Percepcion=cast(round(Percepcion,2) as numeric(12,2)),
Total=cast(round(Total,2) as numeric(12,2)),
Saldo=cast(round(Saldo,2) as numeric(12,2)),
Orden_TipoFondoFijo,
FlActivo
FROM #TMP_Documentos

drop table #TMP_Documentos
END;    
