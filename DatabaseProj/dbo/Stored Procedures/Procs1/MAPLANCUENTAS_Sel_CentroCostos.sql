﻿--JHD-20160129 - and c.FlCompra=1 and c.FlActivo=1
CREATE Procedure dbo.MAPLANCUENTAS_Sel_CentroCostos
@CoCeCos char(6)='',
@FlProvAdmin Char(1)=''
as
select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion
from MAPLANCUENTAS_CENTROCOSTOS pc
inner join MAPLANCUENTAS c on pc.CtaContable=c.CtaContable
where
(pc.CoCeCos=@CoCeCos Or LTRIM(rtrim(@CoCeCos))='')  
and c.FlCompra=1 and c.FlActivo=1
 --and 
 --((@FlProvAdmin='S' and substring(c.CtaContable,0,3)<>'90') or ((@FlProvAdmin='N' and substring(c.CtaContable,0,3)='90')) or LTRIM(RTRIM(@FlProvAdmin)) ='')
 Order by c.CtaContable+ ' - ' + c.Descripcion
