﻿
----------------------------------------------------------------------
--create
CREATE PROCEDURE [MACLIENTES_PERFIL_GUIAS_Del]
	@CoCliente char(6)='',
	@CoProveedor char(6)=''
AS
BEGIN
	Delete [dbo].[MACLIENTES_PERFIL_GUIAS]
	Where 
		[CoCliente] = @CoCliente And 
		[CoProveedor] = @CoProveedor
END;
