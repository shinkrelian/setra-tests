﻿CREATE Procedure [dbo].[MACONTACTOSCLIENTE_Sel_List]	
	@IDCliente char(6),
	@Nombres	varchar(100)
As
	Set NoCount On
	
	Select Nombres, Apellidos, Cargo, Telefono, Celular, IDContacto 
	FROM MACONTACTOSCLIENTE 
    WHERE IDCliente=@IDCliente And
    (LTRIM(RTRIM(@Nombres))='' Or (Nombres+' '+Apellidos Like '%'+@Nombres+'%'))
