﻿

-- BN

Create Procedure [dbo].[MANACIONALIDADPROVEEDOR_Sel_List]  
@IDProveedor char(6)
As  
	Set NoCount On  
	 Select IDubigeo From MANACIONALIDADPROVEEDOR
	 Where IDProveedor = @IDProveedor
