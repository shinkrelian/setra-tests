﻿CREATE Procedure dbo.DOCUMENTO_PROVEEDOR_Sel_Cabecera_VoucherOficExt
@NuVoucher int  
As    
Set NoCount On    
Select c.IDFile,  
--SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,      
  
 os.NuVouOfiExt  as IDVoucher,  
  
 p.IDProveedor,  
 p.NombreCorto as DescProveedor,  
 cl.RazonComercial as Cliente,  
 c.NroPax as Pax,  
 Isnull(c.NroLiberados,0) as Liberados,      
TipoPax=case when c.TipoPax='EXT' then 'EXTRANJERO' when c.TipoPax='NCR' then 'NACIONAL CON RUC' when TipoPax='NSR' then 'NACIONAL SIN RUC' else '' end,      
Tipo_lib=case when c.Tipo_Lib='S' then 'SIMPLE' when c.Tipo_Lib='D' then 'DOBLE' else 'NINGUNO' end,      
c.Titulo,  
uVent.Nombre as UsuarioVen,  
uOper.Nombre as UsuarioOpe,  
c.ObservacionVentas,  
c.ObservacionGeneral,  
c.FecInicio as FechaInicio,  
c.FechaOut as FechaFin,    
0 as NuOrdenServicio,    
os.CoEstado,  
Isnull(os.SsSaldo,0) as SaldoPendiente,  
ISNULL(os.SsDifAceptada,0) as SsDifAceptada,    
isnull(os.TxObsDocumento,'') as TxObsDocumento,  
p.IDTipoProv,  
os.SsTotal as SsMontoTotal  
--,'' RutaDocSustento  
,RutaDocSustento=''--isnull((select top 1 RutaDocSustento from COTIDET  where IDServicio_Det in (select IDServicio_Det from ORDEN_SERVICIO_DET where NuOrden_Servicio=os.NuOrden_Servicio)   order by RutaDocSustento desc),'')  
from VOUCHER_OFICINA_EXTERNA os Left Join COTICAB c On os.IDCab=c.IDCAB    
--Left Join RESERVAS R On os.IDReserva=r.IDReserva    
Left Join MAPROVEEDORES p On os.CoProveedor=p.IDProveedor    
Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente    
Left Join MAUSUARIOS uVent On c.IDUsuario=uVent.IDUsuario    
Left Join MAUSUARIOS uOper On c.IDUsuarioOpe=uOper.IDUsuario    
where os.NuVouOfiExtInt=@NuVoucher
  
