﻿
Create procedure MAUSUARIOS_SelAccountOutput
@Account varchar(max),
@pAccountExists bit output
As
	Set NoCount On
	set @pAccountExists = 0
	If Exists(select account_id from msdb.dbo.sysmail_account where email_address = @Account)
		set @pAccountExists = 1
