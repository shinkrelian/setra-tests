﻿--alter
--JHD-20150413-Tipo=case when v.CoTipo=1 then 'Programado' when v.CoTipo=2 then 'Transporte' when v.CoTipo=3 then 'Viajes' else '' end,
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505- and (up.IDubigeo =@CoPais or rtrim(ltrim(@CoPais))='')
CREATE Procedure [dbo].[MAVEHICULOS_TRANSPORTE_Sel_List]
@CoTipo smallint=0,
@NoVehiculoCitado varchar(200)='',
@CoUbigeo char(6)='',
@CoMercado smallint=0,
@CoPais char(6)=''
as
begin
SELECT        v.NuVehiculo,
		v.NuVehiculo_Anterior,
		v.NoVehiculoCitado,
		v.CoTipo,
		--Tipo=case when v.CoTipo=1 then 'Programado' when v.CoTipo=2 then 'No Programado' else '' end,
		Tipo=case when v.CoTipo=1 then 'Excursiones' when v.CoTipo=2 then 'Transporte' when v.CoTipo=3 then 'Viajes' else '' end,
		m.Descripcion AS Mercado,
		v.QtDe,
		v.QtHasta,
		--v.NoVehiculoCitado,
		v.QtCapacidad,
		v.FlOtraCiudad,
		ISNULL(up.IDubigeo,'') AS IDPais, 
		ISNULL(up.Descripcion,'') AS Pais,
		ISNULL(u.IDubigeo,'') AS IDubigeo,
		ISNULL(u.Descripcion,'') AS Descripcion,
		CoMercado=isnull(v.CoMercado,0)--,
		--m.Descripcion AS Mercado
FROM            MAVEHICULOS_TRANSPORTE AS v LEFT JOIN
                         MAMERCADO AS m ON m.CoMercado = v.CoMercado LEFT JOIN
                         MAUBIGEO AS u ON u.IDubigeo = v.CoUbigeo LEFT JOIN
                         MAUBIGEO AS up ON u.IDPais = up.IDubigeo
WHERE        (rtrim(ltrim(@NoVehiculoCitado))='' or rtrim(ltrim(v.NoVehiculoCitado)) like '%'+rtrim(ltrim(@NoVehiculoCitado))+'%') AND
			 (u.IDubigeo =@CoUbigeo or rtrim(ltrim(@CoUbigeo))='') AND (v.CoTipo = @CoTipo or @CoTipo=0) AND (v.CoMercado = @CoMercado or @CoMercado=0)	
			 and (up.IDubigeo =@CoPais or rtrim(ltrim(@CoPais))='')
			 and flActivo=1
end

