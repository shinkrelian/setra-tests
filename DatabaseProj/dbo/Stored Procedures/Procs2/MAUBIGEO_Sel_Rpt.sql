﻿CREATE Procedure [dbo].[MAUBIGEO_Sel_Rpt]
	@Descripcion varchar(50)
As
	Set NoCount On
	
	Select u.IDubigeo, u.Descripcion, u.Codigo,u.DC,
	p.Descripcion as Pais
	From MAUBIGEO u left join MAUBIGEO p on u.IDPais=p.IDubigeo 
	Where (u.Descripcion Like '%'+ @Descripcion +'%' Or LTRIM(RTRIM(@Descripcion))='')
	ORDER BY  u.Descripcion
