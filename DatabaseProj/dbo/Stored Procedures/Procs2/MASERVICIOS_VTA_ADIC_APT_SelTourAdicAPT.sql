﻿
--HLF-20160329-sd.IDServicio
CREATE Procedure dbo.MASERVICIOS_VTA_ADIC_APT_SelTourAdicAPT
	@CoCiudad char(6)
As
	Set Nocount On

	Select sd.CoServicio, sd.NoServicio as DescDet, sd.SsCosto, sd.IDServicio
	From MASERVICIOS_VTA_ADIC_APT sd 
	Where sd.CoUbigeo=@CoCiudad
	Order by NoServicio

