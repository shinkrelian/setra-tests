﻿CREATE procedure dbo.MAVEHICULOSPROGRAMACION_Sel_List  
@NoUnidadProg varchar(50)  
As  
 Set NoCount On  
 select NuVehiculoProg,NoUnidadProg,QtCapacidadPax  
 from MAVEHICULOSPROGRAMACION   
 where (LTRIM(rtrim(@NoUnidadProg))='' or NoUnidadProg like '%'+@NoUnidadProg+'%')  
 And FlActivo=1
