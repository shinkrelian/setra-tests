﻿CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Upd_NuevaTarifa
	@IDOperacion_Det int,
	@UserMod char(4)
As

	Set Nocount on
	
	Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)        

	Update OPERACIONES_DET_DETSERVICIOS set IDServicio_Det_V_Cot=y.NewIDServicio_Det_V_Cot,
		--IDServicio_Det=y.NewIDServicio_Det,
		TotImpto=
		Case When NewIDServicio_Det_V_Cot <> 0 Then 
			case when Afecto=1 then DBO.FnNetoMaServicio_Det(y.NewIDServicio_Det_V_Cot,y.QtPax)*(@NuIgv*0.01) else 0 end
		else
			TotImpto
		End,
		FecMod=GETDATE(), UserMod=@UserMod
	From	
	(Select IDServicio_Det, IDOperacion_Det, FlServicioNoShow, IDServicio_Det_V_Cot, 
	Case When NewIDServicio_Det_V_Cot = 0 Then IDServicio_Det_V_Cot Else NewIDServicio_Det_V_Cot End as NewIDServicio_Det_V_Cot,
	Case When NewIDServicio_Det = 0 Then IDServicio_Det Else NewIDServicio_Det End as NewIDServicio_Det,
	
	Afecto, QtPax
	From
		(Select ods.IDServicio_Det, ods.IDOperacion_Det, ods.FlServicioNoShow, ods.IDServicio_Det_V_Cot, 
		dbo.FnNuevaTarifa(sdv.IDServicio,sdv.Tipo,sdv.Anio,od.dia,ods.IDServicio_Det_V_Cot,sdv.Codigo) as NewIDServicio_Det_V_Cot,
		dbo.FnNuevaTarifa(sd.IDServicio,sd.Tipo,sd.Anio,od.dia,ods.IDServicio_Det,sd.Codigo) as NewIDServicio_Det,
		sdv.Afecto, ods.QtPax
		From OPERACIONES_DET_DETSERVICIOS ods Inner Join MASERVICIOS_DET sdv On sdv.IDServicio_Det=ods.IDServicio_Det_V_Cot
		Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=ods.IDServicio_Det
		Inner Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
		Where ods.IDOperacion_Det = @IDOperacion_Det And ods.Activo=1
			--In (Select IDOperacion_Det From OPERACIONES_DET Where IDOperacion = @IDOperacion)
		) as X	) as Y
		Where --y.IDServicio_Det_V_Cot<>y.NewIDServicio_Det_V_Cot
		y.IDOperacion_Det=OPERACIONES_DET_DETSERVICIOS.IDOperacion_Det 
		And y.IDServicio_Det=OPERACIONES_DET_DETSERVICIOS.IDServicio_Det
		And y.FlServicioNoShow=OPERACIONES_DET_DETSERVICIOS.FlServicioNoShow