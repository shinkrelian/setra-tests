﻿--HLF-20140731-Comentando FlTicket    
--HLF-20140801-Agregando @CoPago    
--HLF-20140811-Agregando @CoPrvPrg
--JRF-20150304-Agregando @IDMoneda
--JRF-20150409-Se debe permitir Null en el CoPrvGui
CREATE Procedure dbo.PRESUPUESTO_SOBRE_DET_Upd        
 @NuPreSob int,        
 @NuDetPSo int,        
 @CoTipPSo char(2),          
 @TxServicio varchar(250),        
 @FeDetPSo smalldatetime,      
 @QtPax smallint,               
 @SsPreUni numeric(12,4),        
 @SsTotal numeric(12,2),        
 @SsTotSus numeric(12,2),        
 @SsTotDev numeric(12,2),         
 @CoPago char(3),        
 @CoPrvPrg char(6), 
 @IDMoneda char(3), 
 @UserMod char(4)        
As        
 Set Nocount On        
         
 Update PRESUPUESTO_SOBRE_DET        
 Set CoTipPSo=@CoTipPSo,        
 FeDetPSo = @FeDetPSo,      
 TxServicio=@TxServicio,        
 QtPax=@QtPax,        
 SsPreUni=@SsPreUni,        
 SsTotal=@SsTotal,        
 SsTotSus=@SsTotSus,        
 SsTotDev=@SsTotDev,               
 CoPago=@CoPago,    
 CoPrvPrg=Case When Ltrim(rtrim(@CoPrvPrg))='' then Null else @CoPrvPrg End,  
 IDMoneda=Case When LTRIM(RTRIM(@IDMoneda))='' Then Null Else @IDMoneda End,
 UserMod=@UserMod,        
 FecMod=getdate()        
 Where NuPreSob=@NuPreSob And NuDetPSo=@NuDetPSo        
