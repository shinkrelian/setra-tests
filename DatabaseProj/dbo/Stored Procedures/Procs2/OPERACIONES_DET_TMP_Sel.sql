﻿Create Procedure dbo.OPERACIONES_DET_TMP_Sel	
	@IDOperacion int
 As
	Set Nocount On
	
	Select * From OPERACIONES_DET_TMP
	Where IDOperacion = @IDOperacion 
	order by Dia, FechaOut

