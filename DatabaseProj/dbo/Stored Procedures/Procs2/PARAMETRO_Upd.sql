﻿--JRF-20130422-Nuevo Campo (NoRutaPerfilCorreosSQL,NoServerCorreo,NoRutaPerfilCorreosSQLServer)
CREATE Procedure dbo.PARAMETRO_Upd  
@NoNombreEmpresa varchar(200),  
@NoRUCEmpresa  varchar(15),  
@NoRSocialEmpresa  varchar(200),   
@NoDireccEmpresa  varchar(200),   
@NuTelefEmpresa varchar(50),   
@NuFaxEmpresa varchar(50),   
@NoEmailEmpresa varchar(200),   
@NoWebEmpresa varchar(200),   
@NoRutaCorreoMarketing varchar(200),   
@NoRutaCorreoMarketingServer varchar(200),   
@NoRutaImagenes varchar(200),   
@NoRutaIniPdfProveedor varchar(200),   
@NoRutaIniFotoWordVtas varchar(200),   
@NoRutaVouchers varchar(200),   
@NoRutaPerfilCorreosSQL varchar(200),
@NoServerCorreo nvarchar(128),
@NoRutaPerfilCorreosSQLServer varchar(200),
@NuIGV numeric(5,2),   
@PoProrrata numeric(5,2),   
@UserMod char(4)  
As  
UPDATE [dbo].[PARAMETRO]  
   SET [NoNombreEmpresa] = @NoNombreEmpresa  
      ,[NoRUCEmpresa] = @NoRUCEmpresa  
      ,[NoRSocialEmpresa] = @NoRSocialEmpresa  
      ,[NoDireccEmpresa] = @NoDireccEmpresa  
      ,[NuTelefEmpresa] = @NuTelefEmpresa  
      ,[NuFaxEmpresa] = @NuFaxEmpresa  
      ,[NoEmailEmpresa] = @NoEmailEmpresa  
      ,[NoWebEmpresa] = @NoWebEmpresa  
      ,[NoRutaCorreoMarketing] = @NoRutaCorreoMarketing  
      ,[NoRutaCorreoMarketingServer] = @NoRutaCorreoMarketingServer  
      ,[NoRutaImagenes] = @NoRutaImagenes  
      ,[NoRutaIniPdfProveedor] = @NoRutaIniPdfProveedor  
      ,[NoRutaIniFotoWordVtas] = @NoRutaIniFotoWordVtas  
      ,[NoRutaVouchers] = @NoRutaVouchers  
      ,[NoRutaPerfilCorreosSQL] = @NoRutaPerfilCorreosSQL 
      ,[NoServerCorreo] = @NoServerCorreo
      ,[NoRutaPerfilCorreosSQLServer] = @NoRutaPerfilCorreosSQLServer
      ,[NuIGV] = @NuIGV  
      ,[PoProrrata] = @PoProrrata  
      ,[UserMod] = @UserMod  
      ,[FecMod] = GETDATE()  
