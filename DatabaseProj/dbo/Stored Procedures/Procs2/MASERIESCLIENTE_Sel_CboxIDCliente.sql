﻿Create Procedure dbo.MASERIESCLIENTE_Sel_CboxIDCliente
@IDCliente char(6)
As
Set NoCount On
Select 0 as NuSerie,'' as NoSerie,0 as Ord
Union
Select NuSerie,NoSerie,1 as Ord
from MASERIESCLIENTE
where IDCliente = @IDCliente
Order by Ord
