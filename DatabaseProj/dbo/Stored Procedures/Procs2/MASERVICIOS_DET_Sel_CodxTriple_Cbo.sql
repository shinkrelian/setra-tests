﻿--JRF-Se aumento los caracteres para los Montos.
CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_CodxTriple_Cbo]   
 @IDServicio char(8),  
 @Anio char(4)  
   
As  
 Set NoCount On  
  
 SELECT Codigo,Descripcion FROM  
 (  
 Select '' as Codigo, '' as Descripcion,0 as Ord, 0 as Correlativo  
 Union  
 Select IDServicio_Det As Codigo,   
 Descripcion + '('+ Cast(Monto_tri as varchar(15)) +')' as Descripcion,  
 1 as Ord, Correlativo  
 From  MASERVICIOS_DET  
 Where Anio=@Anio And IDServicio=@IDServicio And Not Monto_tri Is Null  
 ) as X  
 Order by Ord,Correlativo  
 
