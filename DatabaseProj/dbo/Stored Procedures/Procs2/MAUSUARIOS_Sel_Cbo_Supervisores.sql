﻿CREATE Procedure [dbo].[MAUSUARIOS_Sel_Cbo_Supervisores]
As
Set NoCount On
Set Language spanish

Select u.IDUsuario,
	   u.Nombre+' '+IsNull(u.TxApellidos,'') as Descripcion
from MAUSUARIOS u Left Join RH_AREA r On u.IdArea=r.CoArea
Left Join MAEMPRESA e On u.CoEmpresa=e.CoEmpresa
Left Join RH_CARGO c On u.CoCargo=c.CoCargo
Left Join MAUSUARIOS ujefe On u.CoUserJefe=ujefe.IDUsuario
Where  u.Activo='A' And u.FlTrabajador_Activo=1 And u.CoEmpresa=1 And u.FlEsSupervisor=1
And u.FecIngreso <= getdate()
Order By Descripcion asc
