﻿Create Procedure dbo.PEDIDO_SelCotizacionesFiles
	@NuPedInt int
As

	Set nocount on

	Select cast(c.IDCAB as nvarchar(10)) as dtParentValor,'Cot: '+c.Cotizacion+isnull(' File: '+c.IDFile,'')+' '+c.Titulo as NoTitulo
	From COTICAB c inner join PEDIDO p on c.NuPedInt=p.NuPedInt 
	Where p.NuPedInt=@NuPedInt
	order by p.NuPedint

