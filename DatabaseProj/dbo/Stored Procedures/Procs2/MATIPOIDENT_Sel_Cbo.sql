﻿--JRF-20150709-Ajustar el Combo para codigos SAP
CREATE Procedure [dbo].[MATIPOIDENT_Sel_Cbo]
@ConCodigoSAP bit
As
	Set NoCount On
	Select IDIdentidad,Descripcion From MATIPOIDENT
	Where @ConCodigoSAP=0 Or (@ConCodigoSAP=1 And(CoSAP Is Not Null Or IDIdentidadSAP Is Not Null))
	order by 2
