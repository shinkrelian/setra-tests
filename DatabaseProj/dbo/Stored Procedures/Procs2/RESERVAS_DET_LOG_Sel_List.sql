﻿--HLF-20130719-Agregando And (ltrim(rtrim(@Accion))='' Or l.Accion=@Accion)  
CREATE Procedure [dbo].[RESERVAS_DET_LOG_Sel_List]  
 @IDCab int,  
 @IDProveedor char(6),
 @IDServicio_Det	int,
 @Accion char(1)  
As  
 Set NoCount On   
   
 SELECT      
 --c.Dia as FechaLog, 
 c.FechaLog,
 us.Usuario,   
 Case c.Accion  
  When 'N' Then 'Nuevo'  
  When 'M' Then 'Modificado'  
  When 'B' Then 'Eliminado'  
  When ' ' Then 'Original'  
 End As Accion,  
 c.IDDET,  
 upper(substring(DATENAME(dw,c.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,c.Dia,103)  
 +' '+ substring(convert(varchar,c.Dia,108),1,5) ) ) as DiaFormat,     
 
 u.Descripcion as DescCiudad,  
 c.Servicio As DescServicioDet,      
   
 p.NombreCorto as DescProveedor,    
 ts.Descripcion as DescTipoServ ,     
 c.NroPax,   
 c.IDIdioma, sd.Anio,sd.Tipo, c.Total,  
 '' as Atendido,  
 --Case When c.Accion=' ' Then '' Else  
 -- Case When c.Atendido=1 Then 'Atendido' Else 'Pendiente' End  
 --End   
 '' As DescAtendido  
 FROM RESERVAS_DET_LOG c 
 Left Join RESERVAS r On c.IDReserva=r.IDReserva
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor     
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo     
 Left Join MASERVICIOS_DET sd On c.IDServicio_Det=sd.IDServicio_Det    
 Left Join MASERVICIOS s On s.IDServicio=sd.IDServicio    
 Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ     
 Left Join MAUSUARIOS us On c.UserMod=us.IDUsuario    
 WHERE --c.IDDET=@IDDet --And c.Atendido=0  
 r.IDCAB=@IDCab And r.IDProveedor=@IDProveedor And c.IDServicio_Det=@IDServicio_Det
 And c.Accion=' '  
   
 UNION  
  
 SELECT      
 c.FechaLog, us.Usuario,   
 Case c.Accion  
  When 'N' Then 'Nuevo'  
  When 'M' Then 'Modificado'  
  When 'B' Then 'Eliminado'  
  When ' ' Then 'Original'  
 End As Accion,  
 c.IDDET,  
 upper(substring(DATENAME(dw,c.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,c.Dia,103) 
 + ' '+ substring(convert(varchar,c.Dia,108),1,5) ) ) as DiaFormat,
 u.Descripcion as DescCiudad,  
 c.Servicio As DescServicioDet,      
 --rd.Servicio As DescServicioDet,      
 --REPLACE(rd.Servicio,(Select Servicio From COTIDET_LOG Where IDDet=c.IDDet And Accion=' '),c.Servicio),  
 p.NombreCorto as DescProveedor,    
 ts.Descripcion as DescTipoServ ,     
 c.NroPax,   
 c.IDIdioma, sd.Anio,sd.Tipo, c.Total,  
 c.Atendido,  
 Case When c.Accion=' ' Then '' Else  
  Case When c.Atendido=1 Then 'Atendido' Else 'Pendiente' End  
 End As DescAtendido  
 FROM RESERVAS_DET_LOG c 
 Left Join RESERVAS r On c.IDReserva=r.IDReserva
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor     
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo     
 Left Join MASERVICIOS_DET sd On c.IDServicio_Det=sd.IDServicio_Det    
 Left Join MASERVICIOS s On s.IDServicio=sd.IDServicio    
 Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ     
 Left Join MAUSUARIOS us On c.UserLog=us.IDUsuario   
 Left Join RESERVAS_DET rd On c.IDDET=rd.IDDet  
 WHERE --c.IDDET=@IDDet --And c.Atendido=0  
 r.IDCAB=@IDCab And r.IDProveedor=@IDProveedor   
 And (c.IDServicio_Det=@IDServicio_Det Or @IDServicio_Det=0)
 And c.Accion<>' '  
 And (ltrim(rtrim(@Accion))='' Or c.Accion=@Accion)     
 ORDER BY -- Accion Desc,  
 FechaLog Asc  
  

