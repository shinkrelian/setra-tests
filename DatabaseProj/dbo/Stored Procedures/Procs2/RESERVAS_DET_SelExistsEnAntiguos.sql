﻿CREATE Procedure dbo.RESERVAS_DET_SelExistsEnAntiguos
@IDReserva_Det int,
@pExisteEnAntiguos bit output
As
Set NoCount On
Set @pExisteEnAntiguos = 0
Select @pExisteEnAntiguos=1
from RESERVAS_DET where IDReserva_DetAntiguo=@IDReserva_Det And Anulado=0
