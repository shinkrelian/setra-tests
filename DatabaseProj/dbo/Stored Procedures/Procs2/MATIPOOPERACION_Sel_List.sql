﻿CREATE Procedure [dbo].[MATIPOOPERACION_Sel_List]
	@IdToc char(3),
	@Descripcion varchar(100)
As
	Set NoCount On
	
	Select Descripcion , IDToc
	From MATIPOOPERACION
	Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')
	and (IdToc=@IdToc Or LTRIM(RTRIM(@IdToc))='')
