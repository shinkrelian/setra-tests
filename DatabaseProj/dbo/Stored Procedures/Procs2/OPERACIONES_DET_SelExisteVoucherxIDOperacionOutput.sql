﻿Create Procedure [dbo].[OPERACIONES_DET_SelExisteVoucherxIDOperacionOutput]
	@IDOperacion  int,
	@pExiste bit output
As
	Set NoCount On
	
	set @pExiste = 0
	If Exists (Select IDVoucher from OPERACIONES_DET 
		Where IDOperacion=@IDOperacion And IDVoucher<>0)
		set @pExiste = 1
	
			
