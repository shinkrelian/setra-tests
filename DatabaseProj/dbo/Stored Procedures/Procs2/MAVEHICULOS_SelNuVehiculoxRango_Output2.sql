﻿--ALTER
--JHD-20150421-Se agregaron los parametros @CoProveedor y @Tipo
--JHD-20150421-Se cambiaron las condiciones para los rangos de vehiculos
--JHD-20150422 - Se comento el filtro de NuVehiculoAnterior
--JHD-20150422 - 	and FlActivo=1
--JHD-20150422 - and FlReservas=1
--JHD-20150422 - select @CoTipo = case when (@IDUbigeoOri in ('000066','000068') and @IDUbigeoDes in ('000066','000068') and @CoTipo=2) then 3 else ( case when @CoTipo=3 then 2 else @CoTipo end) end
--JHD-20150422 - and LTRIM(RTRIM(@IDTipoServ)) in ('PVT','OT')
--JHD-20150427 - Se agrego parametro @IDCliente
--JHD-20150427 - and ((@tipo in ('GEB','GER') and @IDCliente='000160' AND v.CoMercado=2) or (@tipo='ASIA' AND v.CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and v.CoMercado is null))		   
--JHD-20150427 --if @CoTipo=3 set @cotipo=1
--JHD-20150428 - and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND v.CoMercado=2) or (@tipo='ASIA' AND v.CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and v.CoMercado is null))		   
CREATE Procedure [dbo].[MAVEHICULOS_SelNuVehiculoxRango_Output2]    
--@flOtraCiudad bit,    
@CoUbigeo char(6),
@Cantidad tinyint,   
@CoTipo smallint,
@IDTipoServ char(3),
@CoProveedor char(6)='',
@Tipo varchar(7)='',
@IDUbigeoOri char(6)='',
@IDUbigeoDes char(6)='',
@IDCliente char(6)='',
@pNuVehiculo tinyint output    
As    
 Set NoCount On    

 BEGIN
DECLARE @CoPais char(6)
select @CoPais=IDPais from MAUBIGEO where IDubigeo=@CoUbigeo
if (@CoPais='000323')  -- peru
	if @CoProveedor in ('000544','001554') --and (@IDTipoServ='PVT' OR @CoTipo=3)
	begin
			select @CoUbigeo = case when @CoProveedor='000544' then '000066' when  @CoProveedor='001554' then '000065' else @CoUbigeo end
			select @CoTipo = case when (@IDUbigeoOri in ('000066','000068') and @IDUbigeoDes in ('000066','000068')) then 3 else ( case when @CoTipo=3 then 2 else @CoTipo end) end
			set @pNuVehiculo = (select Top(1) NuVehiculo    
			from MAVEHICULOS_TRANSPORTE  
			where CoUbigeo = @CoUbigeo /*and CoUbigeo=@CoUbigeo*/   and CoTipo=@CoTipo
			--and ((@tipo='GEB' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB') and CoMercado is null))
			and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			--and NuVehiculo_Anterior is null
			and LTRIM(RTRIM(@IDTipoServ)) in ('PVT','OT')
			and FlActivo=1
			and FlReservas=1
			and @Cantidad between QtDe and QtHasta
			)
	end
	else
	begin
			set @pNuVehiculo = (select Top(1) NuVehiculo    
			from MAVEHICULOS_TRANSPORTE  
			where CoUbigeo is null --= @CoUbigeo and CoTipo=@CoTipo
			--and ((@tipo='GEB' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB') and CoMercado is null))
			and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND CoMercado=2) or (@tipo='ASIA' AND CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and CoMercado is null))		   
			--and NuVehiculo_Anterior is null
			and FlActivo=1
			and LTRIM(RTRIM(@IDTipoServ)) in ('PVT','OT')
			and FlReservas=1
			and @Cantidad between QtDe and QtHasta
			)
	end
else
	begin

		if @CoTipo=3
				set @cotipo=1

		set @pNuVehiculo = (select Top(1) NuVehiculo    
		from MAVEHICULOS_TRANSPORTE  
		where CoUbigeo = @CoUbigeo /*and CoUbigeo=@CoUbigeo*/ 
		and FlReservas=1
		  and CoTipo=@CoTipo
		and @Cantidad between QtDe and QtHasta	and FlActivo=1)
		
		
	end

END

/*
 if @CoUbigeo in ('000065','000066') AND @CoTipo=2 
 begin
 set @pNuVehiculo = (select Top(1) NuVehiculo    
 from MAVEHICULOS_TRANSPORTE    
 where CoUbigeo = @CoUbigeo and @Cantidad between QtDe and QtHasta
 and CoTipo=@CoTipo)    
 end
 else
 begin
	--if @CoUbigeo in ('000070','000071') and @IDTipoServ='PVT' -- NASCA, PARACAS Y ES PRIVADO
	if @CoUbigeo in ('000070','000071','000377') and @IDTipoServ='PVT' AND @CoTipo=2 -- NASCA, PARACAS, SACRED VALLEY Y ES PRIVADO
	begin
		 set @pNuVehiculo = (select Top(1) NuVehiculo    
		 from MAVEHICULOS_TRANSPORTE    
		 where CoUbigeo = @CoUbigeo
		 and @Cantidad between QtDe and QtHasta and CoTipo=@CoTipo)    
	end
	else
	begin

			IF  @CoUbigeo in ('000066','000068') AND @CoTipo=3 --CUSCO, PUNO
				BEGIN
							 set @pNuVehiculo = (select Top(1) NuVehiculo    
							 from MAVEHICULOS_TRANSPORTE    
							 where CoUbigeo = @CoUbigeo
							 and @Cantidad between QtDe and QtHasta and CoTipo=@CoTipo)    
				END
				ELSE
				BEGIN
		
					 set @pNuVehiculo = (select Top(1) NuVehiculo    
					 from MAVEHICULOS_TRANSPORTE    
					 where CoUbigeo IS NULL or (CoUbigeo not in('000066','000068') and CoUbigeo=@CoUbigeo) --LIMA, CUSCO, PUNO, NASCA, PARACAS, SACRED VALLEY
					 and @Cantidad between QtDe and QtHasta and CoTipo=@CoTipo)
					 END    
	end


 end
 */
 set @pNuVehiculo = ISNULL(@pNuVehiculo,0)  

