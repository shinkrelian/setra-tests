﻿CREATE Procedure [dbo].[MAUSUARIOSOPC_Sel_Rpt] 
@IDUsuario char(4)
as
	Set NoCount On

	SELECT upc.IDOpc,u.Nombre,Cargo=nu.Descripcion,u.Correo,
		Descripcion=
		 case when SUBSTRING(upc.IDOpc,3,4)='0000' then
			op.Descripcion
		 Else
			case when SUBSTRING(upc.IDOpc,5,2)<>'00' then
			 '    '+op.Descripcion
			 else
			  --case when SUBSTRING(upc.IDOpc,3,2)<>'00' then
			   '  ' +op.Descripcion
			  --End
			End
		 End	 				
		,case op.EsMenu when 1 then 'Menu' when 0 then 'Pantalla' End as Pantalla_Menu
	 ,upc.Consultar,upc.Grabar,upc.Actualizar,upc.Eliminar
	FROM MAUSUARIOSOPC upc Left Join MAOPCIONES op On upc.IDOpc=op.IDOpc
	  Left Join MAUSUARIOS u On upc.IDUsuario=u.IDUsuario Left Join MANIVELUSUARIO nu On 
	  U.Nivel=nu.IDNivel WHERE upc.IDUsuario=@IDUsuario
	Order by upc.IDOpc
