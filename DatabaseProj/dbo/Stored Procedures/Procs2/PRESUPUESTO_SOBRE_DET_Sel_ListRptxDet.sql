﻿Create Procedure dbo.PRESUPUESTO_SOBRE_DET_Sel_ListRptxDet  
@NuDetPSo tinyint,  
@NuPreSob int    
as          
 Set NoCount On          
 select DISTINCT FeDetPSo as Fecha,c.IDFile,          
 SUBSTRING(psd.TxServicio,0,50) as Servicio1,SUBSTRING(psd.TxServicio,50,LEN(psd.TxServicio)) as Servicio2,          
 psd.SsTotal,psd.QtPax,          
  dbo.fnConverirNumeroEnLetra(psd.SsTotal) As TotalLetras          
 from PRESUPUESTO_SOBRE_DET psd Left Join PRESUPUESTO_SOBRE ps On psd.NuPreSob = ps.NuPreSob          
 Left Join COTICAB c On ps.IDCab = c.IDCAB          
 where psd.NuPreSob = @NuPreSob and psd.NuDetPSo= @NuDetPSo and psd.CoPago = 'TCK'      
