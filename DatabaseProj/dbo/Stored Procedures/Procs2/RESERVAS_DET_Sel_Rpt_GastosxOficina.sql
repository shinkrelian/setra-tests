﻿
--JHD-20150204- Or (@fecha1 = '01/01/1900' And @fecha2 = '01/01/1900')
--JHD-20150210- INNER JOIN MATIPOPROVEEDOR ON MATIPOPROVEEDOR.IDTipoProv=MAPROVEEDORES.IDTipoProv
--JHD-20150210- HAVING sum(RESERVAS_DET.TotalGen)>0
--JHD-20150210- MATIPOPROVEEDOR.Descripcion
--JHD-20150211- HAVING sum(RESERVAS_DET.TotalGen)>=0.10
--JHD-20150216- Se agrego el campo Total_Pax
--JHD-20150216- Se hizo el cambio para que cuando la moneda sea pesos solo aparezca en pesos y 0 en dolares, y viceversa.
--JHD-20150223- Se agrego el campo Pax_File
--JHD-20150223- Se agrego el campo Servicio para reemplazar los servicios Free con el texto de CotiDet
--JHD-20150223- Se cambio el campo TotalGen por NetoGen
--JHD-20150224- Se quito el UNION con las ordenes de servicio para evitar datos duplicados
--JHD-20150225- Se desgloso la data para mostrar por detalles de servicios
--JHD-20150227- Se cambió la consulta para mostrar los detalles de los servicios y sus montos desglosados.
--JHD-20150302- Se agrego el campo Servicio para reemplazar los servicios con el texto de CotiDet
--JHD-20150409- Se cambió la consulta para mostrar la data de RESERVAS_DET

--Exec RESERVAS_DET_Sel_Rpt_GastosxOficina '20151001', '20151031', '000110'
--/*
CREATE PROCEDURE [dbo].[RESERVAS_DET_Sel_Rpt_GastosxOficina]
--DECLARE
@fecha1 smalldatetime='01/01/1900',
@fecha2 smalldatetime='01/01/1900',
@CoUbigeo_Oficina char(6)=''--'000110'--'000113'
AS
--*/
BEGIN
set nocount on
/*
declare @fecha1 smalldatetime, @fecha2 smalldatetime, @CoUbigeo_Oficina char(6)
select @fecha1 = '20151001', @fecha2 = '20151031', @CoUbigeo_Oficina = '000110'
--*/

declare @Moneda char(3)=''
set @Moneda=case when @CoUbigeo_Oficina='000113' then 'ARS' when  @CoUbigeo_Oficina='000110' then 'CLP' ELSE '' END

 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)                            
 Declare @MargenInterno as numeric(6,2)=10

declare @tipocambio decimal(6,3)
								
SET @tipocambio =ISNULL((select SsTipCam from MATIPOSCAMBIO_USD where CoMoneda=@Moneda),0)

 --Declare @TipoCambio numeric(8,2)                                  
         
 --Select @TipoCambio=SsTipCam From COTICAB_TIPOSCAMBIO Where                                   
 --IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                    
 --AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=@IDOperacion_Det)        
         
 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)                                  

--nuevo

IF (@fecha1 ='01/01/1900' OR @fecha2 = '01/01/1900')
	SELECT @fecha1 = MIN(FecInicio), @fecha2 = MAX(FecInicio) FROM COTICAB

SELECT R.IDFile, R.Titulo, R.IDServicio, R.Servicio, R.Descripcion, R.IDServicio_Det, R.Servicio_Det,
R.IDMoneda, SUM(R.TotalGen) AS TotalGen, SUM(R.Total_Pesos) AS Total_Pesos, SUM(R.Total_Dolares) AS Total_Dolares,
R.FecInicio, R.NombreCorto, R.Tipo, R.FechaOut, --(select max(r.dia) from #rpt1 rl where rl.IDFile=R.IDFile) AS FechaOut,
SUM(R.Total_Pax) AS Total_Pax, R.Pax_File
----, IDReserva_Det
, 0 AS IDReserva_Det
, SUM(R.Total_PesosP)+SUM(R.Total_VP) AS Total_PesosPago, SUM(R.Total_DolaresP)+SUM(R.Total_VD) AS Total_DolaresPago
, ISNULL(ServicioDet,'') AS ServicioDet, ISNULL(ProveedorDet,'') AS ProveedorDet, ISNULL(TipoProvDet,'') AS TipoProvDet
, FlServicioNoShow
--select * 
--into #rpt1
FROM
(
SELECT c.IDCAB, c.IDFile, c.Titulo, s.IDServicio,
Servicio=case when p.IDTipoProv = '001' THEN s.Descripcion + ' - ' + sd.Descripcion ELSE
				case when ltrim(rtrim(sd.Descripcion))='Free'
					then
						(select top 1 servicio from cotidet cd where cd.IDCAB=c.IDCab and cd.IDServicio_Det=rd.IDServicio_Det and cd.IDDET = rd.IDDet)
						--'Free - ss'
					else sd.Descripcion end
		end,
s.Descripcion, rd.IDServicio_Det, Servicio_Det=sd.Descripcion, rd.IDMoneda, TotalGen=sum(rd.NetoGen),
Total_Pesos= sum(case when rd.IDMoneda <> 'USD' THEN rd.NetoGen ELSE 0 END),
Total_Dolares= sum(case when rd.IDMoneda = 'USD' THEN rd.NetoGen ELSE 0 END),
c.FecInicio, p.NombreCorto, tp.Descripcion as Tipo, c.FechaOut, Total_Pax=sum(rd.NroPax+ISNULL(rd.NroLiberados,0)),
Pax_File=c.NroPax+isnull(c.NroLiberados,0), rd.IDReserva_Det, rd.Dia
, Total_PesosP= sum(case when PA.IDMoneda <> 'USD' THEN PA.Total_Final ELSE 0 END),
Total_DolaresP= sum(case when PA.IDMoneda = 'USD' THEN PA.Total_Final ELSE 0 END), PA.IDMoneda AS IDMonedaPA
, PA.ServicioDet, PA.ProveedorDet, PA.TipoProvDet, RD.FlServicioNoShow
, Total_VP = sum(case when PA.IDCab IS NULL and rd.IDMoneda <> 'USD' AND ISNULL(IDVoucher,'') <> '' THEN OPD.TotalGen ELSE 0 END)
, Total_VD = sum(case when PA.IDCab IS NULL and rd.IDMoneda = 'USD' AND ISNULL(IDVoucher,'') <> '' THEN OPD.TotalGen ELSE 0 END)
from COTICAB c
inner join RESERVAS r ON r.IDCab = c.IDCAB
inner join RESERVAS_DET rd ON r.IDReserva = rd.IDReserva
inner Join (SELECT IDServicio_Det, Isnull(IDServicio_Det_V, IDServicio_Det) AS IDServicio_Det_V,
			Descripcion FROM MASERVICIOS_DET) sd On rd.IDServicio_Det = sd.IDServicio_Det      
inner Join MASERVICIOS_DET sdRel On sd.IDServicio_Det_V = sdRel.IDServicio_Det      
inner Join MASERVICIOS s On sdRel.IDServicio = s.IDServicio
inner Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor      
Left Join MAMONEDAS mo On rd.IDMoneda= mo.IDMoneda     
inner JOIN MATIPOPROVEEDOR tp ON tp.IDTipoProv=p.IDTipoProv
left outer join OPERACIONES_DET OPD ON rd.IDReserva_Det = OPD.IDReserva_Det AND rd.IDServicio_Det = OPD.IDServicio_Det
LEFT OUTER JOIN 
(
--*/
	SELECT OP.IDCab, OPD.IDReserva_Det, ODL.IDServicio_Det, OP.IDMoneda, SUM(ISNULL(OPD.Total_Final,0)) AS Total_Final, 'OP' AS Tipo
	, SDL.Descripcion AS ServicioDet, PRL.ProveedorDet, PRL.TipoProvDet
	FROM OPERACIONES_DET_DETSERVICIOS AS ODSL LEFT OUTER JOIN
         (SELECT P1L.IDProveedor, P1L.NombreCorto AS ProveedorDet, P1L.IDTipoProv, TP1L.Descripcion AS TipoProvDet
		  FROM MAPROVEEDORES AS P1L INNER JOIN MATIPOPROVEEDOR AS TP1L ON P1L.IDTipoProv = TP1L.IDTipoProv) AS PRL
		  ON ODSL.IDProveedor_Prg = PRL.IDProveedor LEFT OUTER JOIN
         MASERVICIOS_DET AS SDL ON ODSL.IDServicio_Det = SDL.IDServicio_Det RIGHT OUTER JOIN
         OPERACIONES_DET AS ODL ON ODSL.IDOperacion_Det = ODL.IDOperacion_Det RIGHT OUTER JOIN
         ORDENPAGO_DET AS OPD INNER JOIN
         ORDENPAGO AS OP ON OPD.IDOrdPag = OP.IDOrdPag ON ODL.IDReserva_Det = OPD.IDReserva_Det AND ODL.IDServicio_Det = OPD.IDServicio_Det
	--where ODSL.IDOperacion_Det = 93173
	GROUP BY OP.IDCab, OPD.IDReserva_Det, ODL.IDServicio_Det, OP.IDMoneda, SDL.Descripcion, PRL.ProveedorDet, PRL.TipoProvDet

UNION ALL

	SELECT OS.IDCab, ODSR.IDReserva_Det, ODSR.IDServicio_Det, ODSR.IDMoneda, SUM(ODSR.TotalGen) AS Total_Final, 'OS' AS Tipo
	, ServicioDet, ProveedorDet, TipoProvDet
	FROM ORDEN_SERVICIO AS OS INNER JOIN
	ORDEN_SERVICIO_DET AS OSD ON OS.NuOrden_Servicio = OSD.NuOrden_Servicio INNER JOIN
	(SELECT IDOperacion_Det, IDServicio_Det, IDOperacion, IDReserva_Det,
	 CASE WHEN IngManual = 1 THEN TotalProgram
							ELSE
								isnull(CASE WHEN isnull(TotalCotizadoCal, 0) = 0 THEN
																CAST(CASE WHEN Afecto = 1 THEN NetoGen + (NetoGen * (@NuIgv / 100))
																  ELSE NetoGen END AS numeric(12, 4))
										ELSE TotalCotizadoCal END, 0)
	 END AS TotalGen, IDMoneda
	 , ServicioDet, ProveedorDet, TipoProvDet
	FROM (SELECT ODSL.IDOperacion_Det, ODL.IDServicio_Det, ODL.IDOperacion, ODL.IDReserva_Det, ODSL.TotalCotizado AS TotalCotizadoCal,
					CASE WHEN isnull(ODSL.NetoCotizado, 0) = 0 THEN
							CASE WHEN dbo.FnNetoMaServicio_Det(ODSL.IDServicio_Det, ODL.NroPax + isnull(ODL.NroLiberados, 0)) = 0 OR ODL.FlServicioTC = 1 THEN
									ODSL.CostoReal
								ELSE
									CASE WHEN MDCOTL.IDServicio_Det IS NULL THEN
										dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(ODSL.IDServicio_Det, ODL.NroPax + isnull(ODL.NroLiberados, 0)), MDCOTL.CoMoneda, ODSL.IDMoneda, @tipocambio)
									ELSE
										dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(MDCOTL.IDServicio_Det, ODL.NroPax + isnull(ODL.NroLiberados, 0)), MDCOTL.CoMoneda, ODSL.IDMoneda, @tipocambio)
										END END
									ELSE ODSL.NetoCotizado
								END AS NetoGen,
				ISNULL(MDCOTL.Afecto, MDL.Afecto) AS Afecto, ODSL.IngManual, ISNULL(ODSL.NetoProgram, 0) AS NetoProgram,
				ISNULL(ODSL.IgvProgram, 0) AS IgvProgram, ISNULL(ODSL.TotalProgram, 0) AS TotalProgram, ODSL.IDMoneda
				, CASE WHEN MDCOTL.IDServicio_Det IS NULL THEN MDL.Descripcion ELSE MDCOTL.Descripcion END AS ServicioDet
				, PRL.ProveedorDet, PRL.TipoProvDet
		  FROM OPERACIONES_DET_DETSERVICIOS AS ODSL INNER JOIN
			   OPERACIONES_DET AS ODL ON ODSL.IDOperacion_Det = ODL.IDOperacion_Det INNER JOIN
			   MASERVICIOS_DET AS MDL ON MDL.IDServicio_Det = ODSL.IDServicio_Det LEFT OUTER JOIN
			   MASERVICIOS_DET AS MDCOTL ON ODSL.IDServicio_Det_V_Cot = MDCOTL.IDServicio_Det LEFT OUTER JOIN
			   (SELECT P1L.IDProveedor, P1L.NombreCorto AS ProveedorDet, P1L.IDTipoProv, TP1L.Descripcion AS TipoProvDet
				FROM MAPROVEEDORES AS P1L INNER JOIN MATIPOPROVEEDOR AS TP1L ON P1L.IDTipoProv = TP1L.IDTipoProv) AS PRL ON ODSL.IDProveedor_Prg = PRL.IDProveedor
		  WHERE (ODSL.Activo = 1)) AS ODSRL) AS ODSR ON OSD.IDOperacion_Det = ODSR.IDOperacion_Det AND OSD.IDServicio_Det = ODSR.IDServicio_Det
	--WHERE ODSR.IDReserva_Det = 197054
	GROUP BY OS.IDCab, ODSR.IDReserva_Det, ODSR.IDServicio_Det, ODSR.IDMoneda
	, ServicioDet, ProveedorDet, TipoProvDet
--/*
) PA ON c.IDCAB = PA.IDCab and rd.IDReserva_Det = PA.IDReserva_Det AND rd.IDServicio_Det = PA.IDServicio_Det
WHERE (p.CoUbigeo_Oficina = @CoUbigeo_Oficina)
----and (c.FecInicio>@fecha1 and c.FecInicio<@fecha2+1)
and (c.FecInicio between @fecha1 and @fecha2)
and c.IDFile is not null and c.Estado <> 'X' and
r.Anulado=0 and rd.Anulado=0 and r.Estado<>'XL'
--and c.IDFile = '15100219'
group by c.IDCAB, c.IDFile, c.Titulo, s.IDServicio, p.IDTipoProv, s.Descripcion, rd.IDMoneda, c.FecInicio, p.NombreCorto,
p.CoUbigeo_Oficina, tp.Descripcion, c.FechaOut, c.NroPax, c.NroLiberados, rd.IDReserva_Det
,c.idcab, rd.IDServicio_Det, sd.Descripcion, rd.dia, rd.IDDet
, PA.IDMoneda, PA.ServicioDet, PA.ProveedorDet, PA.TipoProvDet, RD.FlServicioNoShow
HAVING sum(rd.TotalGen)>=0.10
) AS R

GROUP BY R.IDFile, R.Titulo, R.IDServicio, R.Servicio, R.Descripcion, R.IDServicio_Det, R.Servicio_Det,
R.IDMoneda, R.FecInicio, R.FechaOut, R.NombreCorto, R.Tipo, R.Pax_File, ServicioDet, ProveedorDet, TipoProvDet
, FlServicioNoShow
ORDER BY R.IDFile
--set nocount off

--SELECT R.IDFile, R.Titulo, R.IDServicio, R.Servicio, R.Descripcion, R.IDServicio_Det, R.Servicio_Det,
--R.IDMoneda, SUM(R.TotalGen) AS TotalGen, SUM(R.Total_Pesos) AS Total_Pesos, SUM(R.Total_Dolares) AS Total_Dolares,
--R.FecInicio, R.NombreCorto, R.Tipo, R.FechaOut, --(select max(r.dia) from #rpt1 rl where rl.IDFile=R.IDFile) AS FechaOut,
--SUM(R.Total_Pax) AS Total_Pax, R.Pax_File
----, IDReserva_Det
--, 0 AS IDReserva_Det
--, SUM(R.Total_PesosP) AS Total_PesosPago, SUM(R.Total_DolaresP) AS Total_DolaresPago
--from #rpt1 R
--GROUP BY R.IDFile, R.Titulo, R.IDServicio, R.Servicio, R.Descripcion, R.IDServicio_Det, R.Servicio_Det,
--R.IDMoneda, R.FecInicio, R.FechaOut, R.NombreCorto, R.Tipo, R.Pax_File
----, IDReserva_Det
--drop table #rpt1
END
