﻿
--JRF-20120911- Nuevo Campo EsOficina    
--JRF-20130903- Nuevo Campo CodigoINC  
--JRF-20131014- Nuevo campo CodigoPeruRail
--MLL-20140522- Nuevo Campo CoInternacPais
CREATE Procedure [dbo].[MAUBIGEO_Ins]         
@Descripcion varchar(50),        
@UserMod char(4),        
@TipoUbig varchar(20),        
@Prove char(1),        
@NI char(1),        
@DC char(3),        
@IDPais char(6),    
@CodigoINC varchar(3),   
@CodigoPeruRail varchar(5),
@CoInternacPais char(2) , -- Nuevo campo desde 22/05/14 
@EsOficina bit ,      
@pIDUbigeo char(6) Output    
   
As        
        
 Set NoCount On        
         
 Declare @IDUbigeo char(6)         
 Exec Correlativo_SelOutput 'MAUBIGEO',1,6,@IDUbigeo output         
        
 INSERT INTO MAUBIGEO        
           (IDUbigeo        
           ,Descripcion        
           ,TipoUbig         
           ,Prove        
           ,NI        
           ,DC        
           ,IDPais       
           ,CodigoINC  
           ,CodigoPeruRail 
           ,CoInternacPais
           ,EsOficina    
           ,[UserMod]   
                
           )        
     VALUES        
   (@IDUbigeo,        
    @Descripcion         
   ,@TipoUbig        
   ,@Prove        
   ,@NI        
   ,Case When ltrim(rtrim(@DC))='' Then Null Else @DC End        
   ,Case When ltrim(rtrim(@IDPais))='' Then Null Else @IDPais End        
   ,Case When LTRIM(rtrim(@CodigoINC))='' Then Null Else @CodigoINC End  
   ,Case When ltrim(rtrim(@CodigoPeruRail))='' Then Null Else @CodigoPeruRail End
   ,Case When ltrim(rtrim(@CoInternacPais))='' Then Null Else @CoInternacPais End
   ,@EsOficina    
   ,@UserMod )        
                    
 Set @pIDUbigeo = @IDUbigeo         
