﻿

-----------------------------------------------------------------------------------------


--create
create Procedure [dbo].[MAPROVEEDORES_Sel_DocumentoProveedor_Pk]    
 @IDProveedor char(6)    
AS    
 Set NoCount On    
     
 Select p.*, 
 TipoIdentidad=(select Descripcion from dbo.MATIPOIDENT where IDIdentidad=p.IDIdentidad),
 u.IDPais, 
 Pais=(select Descripcion from MAUBIGEO where IDubigeo=u.IDPais),
 Ciudad=(select Descripcion from MAUBIGEO where IDubigeo=u.IDubigeo),
 Forma_Pago=(select Descripcion from MAFORMASPAGO where IdFor=p.IDFormaPago),
 Moneda=(select Descripcion from MAMONEDAS where IDMoneda=p.CoMonedaCredito),
 g.IDubigeo as Nacionalidad, 
 us.Usuario as UsuarioProg, us.Correo as CorreoProg,  
 us.Nombre as NombreUsuarioProg,
 tr.Correo as CorreoProgTransp,
 tr.Nombre as NombreUsuarioProgTransp
 From MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo    
 Left Join MAUBIGEO g On u.IDPais=g.IDubigeo    
 Left Join MAUSUARIOS us On isnull(p.IDUsuarioProg,'')=us.IDUsuario  
 Left Join MAUSUARIOS tr On isnull(p.IDUsuarioProgTransp,'')=tr.IDUsuario  
 Where IDProveedor = @IDProveedor    
  
