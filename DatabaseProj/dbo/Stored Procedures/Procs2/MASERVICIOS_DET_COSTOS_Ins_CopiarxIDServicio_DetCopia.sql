﻿--HLF-20121024-
CREATE Procedure [dbo].[MASERVICIOS_DET_COSTOS_Ins_CopiarxIDServicio_DetCopia]  
 --@IDServicio	char(8),
 @IDServicio_DetCopia int, 
 @IDServicio_Det int, 
 @PorcentDcto numeric(5,2),  
 @UserMod char(4)  
As  
 Set NoCount On  
  
 Insert Into MASERVICIOS_DET_COSTOS  
 (Correlativo, IDServicio_Det, PaxDesde, PaxHasta, Monto, UserMod)  
 Select * From 
 (
 Select c.Correlativo, 
 --20121024-I
 --(Select Top 1 
 --   IDServicio_Det From MASERVICIOS_DET 
	--Where IDServicio_DetCopia=@IDServicio_DetCopia And IDServicio=@IDServicio			
	--Order by FecMod Desc
	--) as IDServicio_DetNue, 
	@IDServicio_Det as IDServicio_Det,
--20121024-F
 c.PaxDesde, c.PaxHasta,   
 
 Case When @PorcentDcto=0 Then c.Monto Else c.Monto*(1-(@PorcentDcto/100)) End as Monto,  
 @UserMod  as UserMod
 From MASERVICIOS_DET_COSTOS c 
 Where 
 --20121024-I
 --	c.IDServicio_Det In 
	--(Select IDServicio_Det From MASERVICIOS_DET 
	--	Where IDServicio_Det=@IDServicio_DetCopia
	--	And IDServicio=@IDServicio
	--	)
	c.IDServicio_Det=@IDServicio_DetCopia 
--20121024-F
 
	 
) as X



