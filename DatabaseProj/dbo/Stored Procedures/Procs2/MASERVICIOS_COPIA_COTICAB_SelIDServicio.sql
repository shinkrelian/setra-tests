﻿Create Procedure dbo.MASERVICIOS_COPIA_COTICAB_SelIDServicio
@IDServicioAntiguo char(8),
@IDCab int,
@pIDServicio char(8) output
As
Set NoCount On
Set @pIDServicio = (Select Top(1) IDServicioNuevo 
					from MASERVICIOS_COPIA_COTICAB ms --Inner Join MASERVICIOS s On ms.IDServicioNuevo=s.IDServicio
					Inner Join MASERVICIOS_DET sd On ms.IDServicio_Det=sd.IDServicio_Det
					Inner Join MASERVICIOS s On ms.IDServicioNuevo = s.IDServicio
					Where ms.IDCab=@IDCab And ms.IDServicioAntiguo =@IDServicioAntiguo And s.cActivo='A')
Set @pIDServicio = IsNull(@pIDServicio,'')
