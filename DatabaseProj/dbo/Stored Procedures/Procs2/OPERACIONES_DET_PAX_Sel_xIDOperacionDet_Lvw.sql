﻿--JRF-20150227-Considerar los NoShow
CREATE PROCEDURE dbo.OPERACIONES_DET_PAX_Sel_xIDOperacionDet_Lvw  
 @IDOperacion_Det int,
 @NoShow bit =0
As  
  
 Set NoCount On   
   
 Select odp.IDPax  
 From OPERACIONES_DET_PAX odp Inner Join COTIPAX c On odp.IDPax=c.IDPax
 Where odp.IDOperacion_Det=@IDOperacion_Det and c.FlNoShow=@NoShow
 Order by odp.IDPax  
