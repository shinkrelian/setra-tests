﻿--JHD-20141212 - Se cambió tablas temporales por variables              
--JHD-20141217 - Se corrigió el error de la división entre cero:    Set @Growth= CASE WHEN @TotalAnioAnt=0 THEN 0 ELSE ((@TotalAnio-@TotalAnioAnt)*100)/@TotalAnioAnt   END            
--JHD-20141217 - Se agregó la condición que solo tome en cuenta files aceptados: and cc.Estado='A'            
--JHD-20141218 - Se corrigio para que cuando el % de debit memo no este en el rango de 15, se muestre el valor venta * el porcentaje de concretacion            
    --Se cambio: Case When Abs(((ValorVenta-TotalDM)*100)/ValorVenta)<=15 then               
    -- por:     Case When Abs(((TotalDM)*100)/ValorVenta)>=15 then              
    --se cambio:  ValorVenta, por:  ValorVenta*(PoConcretVta/100)              
--JHD-20141218 - se agrego un union para los files anulados pero que tienen debit memo pagados            
--JHD-20141218 - se agrego la condicion para que en el caso de los files anulados, no se respete el 15% y se muestre defrente el total de los debit memo            
--HLF-20141219-          
  --    and (cc.Estado='A' or           
  --(cc.Estado='X' and cc.IDCAB in           
  -- (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG')))    
--JHD-20141226 - Se comentó: /*And SD1.idTipoOC<>'005'*/          
--HLF-20141231-Left Join MACLIENTES c1 On dm1.IDCliente=c1.IDCliente a subquery TotalDM      
--JHD-20150106 - Se aumento la capacidad al campo @Growth      
--JHD 20150130 - Case When Abs( (case when valorventa=0 then 0 else (((TotalDM)*100)/ValorVenta) end) )>=15 then         
--JRF-20150205- Considerar Filtros de Limite de Cliente Top.    
--JRF-20150213- Considerar el filtro de año  
--JRF-20150311- Considerar la Tarifa ingresa para los vuelos
--JHD 20150421- Case When Abs( (case when valorventa=0 then 0 else (((TotalDM)*100)/ValorVenta) end) )>=15 then         
--JHD 20150422- Se cambio la consulta de insercion para la tabla temporal @Ventas para que cuadre con el reporte de nota de venta
--JHD 20150427- Order By anio desc,TotalVentaAnioPas desc,Cliente          
CREATE Procedure [dbo].[ReporteComparaVentasCliente]              
 @IDCliente char(6),    
 @Top tinyint,  
 @Anio char(4)  
As              
BEGIN              
 Set Nocount On                       
 Declare @AnioMes char(6), @TotalVta numeric(12,2), @Cliente varchar(80),@NroOrden int=1,@NroOrdenFetch tinyint,    
      @ClienteTmp varchar(80)=''    
               
 declare @Ventas table(    
 NroOrden tinyint null,    
 Cliente varchar(80) null,    
 AnioMes char(6) null,            
 TotalVenta numeric(12,2) null              
 )      
 

 INSERT INTO @Ventas       
 SELECT
 
   0 as NroOrden,DescCliente,FecOutPeru as AnioMes,
   
    SUM(SumaProy) as TotalVenta
from
(
select
  FecInicio,  
   FecOut,  
   FecOutPeru,  
   IDCAB,  
   IDFile,  
   Titulo,  
   --ValorVenta=ISNULL(ValorVenta2,0),  
   ValorVenta=round(case when FecInicio<GETDATE() /*pasado*/  
   then  
    (  
     --isnull(TotalDM,0)  
	 case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
    )  
   else --futuro  
    (  
     ISNULL(ValorVenta2,0)  
    )  
   end,0),  
   PoConcretVta,  
   NroPax,  
   ESTADO,  
   IDUsuario,  
   Responsable,  
   IDCliente,  
   CoPais,  
   DescPaisCliente,  
   DescCliente,  
   MargenGan=ISNULL(MargenGan,0),  
   Utilidad=ISNULL(ValorVenta2*(MargenGan/100),0)--,  
    
   , Total_Proy=case when FecInicio<GETDATE() /*pasado*/  
   then  
    (  
     case when PoConcretVta<100 then ISNULL(ValorVenta2,0)*(PoConcretVta/100) else 0 end  
    )  
   else --futuro  
    (  
     case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then 0 else TotalCotiz end  
    )  
   end  
     
   , Total_Concret=case when FecInicio<GETDATE() /*pasado*/  
   then  
    (  
     case when PoConcretVta=100 then ISNULL(ValorVenta2,0) else 0 end  
    )  
   else --futuro  
    (  
     case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then TotalDM else 0 end  
    )  
   end  
  
  ,  SumaProy=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  

      ,Dias
   ,Edad_Prom
   
 --INTO #VentasReceptivoMensualDet  
 FROM  
 (     
 SELECT Y.*,  
  
  ISNULL(Y.ValorVenta*(Y.MargenGan/100),0) AS Utilidad  
  ,ValorVenta2=case when Y.TotalDM>0 then (CASE WHEN ABS(Y.TotalDM-Y.TotalCotiz)>=Y.TotalDM_Porc THEN Y.TotalCotiz ELSE Y.TotalDM END) else Y.TotalCotiz end  

 FROM     
 (  
   SELECT       
   FecInicio,  
   FecOutPeru as FecOut,      
   Left(CONVERT(varchar,FecOutPeru,112),6) AS FecOutPeru,      
   IDCAB, IDFile, Titulo,     

   Case When EstadoFacturac='F' Then X.TotalDM    
   Else  --total de cotizacion  
     Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
       isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
      Else    
       X.TotalSimple+X.TotalDoble+X.TotalTriple              
      End    
     Else    

      ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
     End     
   End 
   as ValorVenta ,    
     
   PoConcretVta,  

   NroPax=(NroPax+isnull(NroLiberados,0)),  
   Estado ,  
   IDUsuario,  
   Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
   , IDCliente, CoPais,  DescPaisCliente,  DescCliente,  
        
   case when X.TotalDobleNeto = 0 then 0                                                
   else                                                 
   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                                 
   End as MargenGan       
        
   ,TotalDM=isnull(X.TotalDM,0)  
   ,TotalDM_Porc=X.TotalDM*0.15  
   ,TotalCotiz=  Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
    isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
      Else    
    X.TotalSimple+X.TotalDoble+X.TotalTriple              
      End    
     Else    

      ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
     End     
       
   ,TotalCotiz_Porc=  (Case When ExisteHotel=1 Then    
          Case When EsTotalxPax=1 Then    
				isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
          Else    
				X.TotalSimple+X.TotalDoble+X.TotalTriple              
          End    
         Else    

        ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
         End     
       )*0.15  
      
      ,Dias
   ,Edad_Prom
   FROM       
   (      
   SELECT CC.*,      
   (Select SUM(Total) From DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN')
   + Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN') Then dbo.FnDevuelveMontoTotalxPaxVuelos(cc.IDCAB,1) Else 0 End as TotalDM,    

     
   cc.FechaOut as FecOutPeru,      
   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) *((Isnull(cc.Twin,0)+Isnull(cc.Matrimonial,0))) *2) + dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,1) as TotalDoble,
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * Isnull(cc.Simple,0) as TotalSimple,                                                    
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * (Isnull(cc.Triple,0)*3) as TotalTriple,   
   (select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = CC.IDCAB) As TotalCotiz, --As TotalCotiz,
   dbo.FnEsTotalxPax(CC.IDCAB) as EsTotalxPax,    
   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                         
   Where cd.IDCAB = CC.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,      
        
   (select SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,0) as TotalDobleNeto, --as TotalDobleNeto,      
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalSimpleNeto,--as TotalSimpleNeto,      
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalTripleNeto,--as TotalTripleNeto,
   (select SUM(TotImpto) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalImpto,      
   (select SUM(CostoReal) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalCostoReal,      
   (select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = cc.IDCAB) as TotalLib,  
   up.IDubigeo as CoPais,up.Descripcion as DescPaisCliente, c.RazonComercial as DescCliente  
   
   ,Dias=DATEDIFF(day,FecInicio,FechaOut)+1
   ,Edad_Prom=isnull(dbo.FnEdadPromedio_Pax(cc.IDCab),0)
   FROM COTICAB CC --Left Join DEBIT_MEMO db On db.IDCab = CC.IDCAB                   
    Inner Join MACLIENTES c On CC.IDCliente=c.IDCliente  
   Left Join MAUBIGEO uc On C.IDCiudad=uc.IDubigeo  
   Left Join MAUBIGEO up On uc.IDubigeo=up.IDubigeo  
  
   WHERE       
   (
     cc.CoTipoVenta='RC' AND CC.Estado='A'       
   or             
   (cc.Estado='X' and cc.IDCAB in             
   (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG') )      
    )
	 and (Ltrim(Rtrim(@IDCliente))='' Or c.IDCliente=@IDCliente)    
   --And ISNULL(db.IDEstado,'') <> 'AN'          
   ) AS X      

   --where  (Ltrim(Rtrim(@IDCliente))='' Or x.IDCliente=@IDCliente) 
 /*
 WHERE x.FecOutPeru          
 BETWEEN '01/01/'+@Anio/*'2015'*/ AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/'+@Anio/*2015'*/,103) as datetime)))
 */
 ) AS Y  
 ) AS Z  

 ) as Z1

GROUP BY DescCliente,FecOutPeru
ORDER BY DescCliente,AnioMes
 
 /*              
 INSERT INTO @Ventas               
 SELECT 0,Cliente,AnioMes, SUM(TotalVenta) as TotalVenta              
 --Into #Ventas              
 FROM              
  (              
  SELECT Left(convert(varchar,FechaOutPeru,112),6) as AnioMes,              
   Case When datediff(d,FechaOutPeru,GETDATE())>0 Then              
  TotalDM              
   Else              
    Case When TotalDM=0 Then              
   ValorVenta*(PoConcretVta/100)              
    Else              
                   
    CASE WHEN Estado='X' THEN             
     TotalDM       
    ELSE            
     --Case When Abs(((TotalDM)*100)/ValorVenta)>=15 then              
       Case When Abs( (case when valorventa=0 then 0 else (((TotalDM)*100)/ValorVenta) end) )>=15 then         
		  TotalDM              
		 Else              
		   ValorVenta*(PoConcretVta/100)              
		 End                  
    END            
                 
    End              
   End as TotalVenta              
   --, FechaOutPeru, TotalDM, PoConcretVta, ValorVenta              
   ,Cliente    
   FROM               
   (              
    --agregar union con los files anulados pero que tienen debit memo pagados            
    -- agregar campo estado para validar afuera del select            
                
      Select cc.estado,cc.IDCAB, isnull((Select Max(Dia) From COTIDET cd1               
      Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo               
      And u1.IDPais='000323'              
      Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det              
      Where IDCAB=cc.IDCAB /*And SD1.idTipoOC<>'005'*/),cc.FechaOut) as FechaOutPeru,        
      cc.PoConcretVta,                
      cc.NroPax,                 
      isnull((Select sum(total*nropax) From Cotidet Where idcab=cc.idcab),0) + 
	  IsNull(dbo.FnDevuelveMontoTotalxPaxVuelos(cc.IDCAB,1),0) as ValorVenta,              
      ISNULL((Select SUM(dm1.Total) From DEBIT_MEMO dm1 Left Join MACLIENTES c1 On dm1.IDCliente=c1.IDCliente    
      Where dm1.IDCab=cc.idcab         
      And dm1.IDEstado<>'AN'       
      --And IDCliente=@IDCliente)      
      And CHARINDEX(c1.RazonComercial,dm1.Cliente)<>0)      
      ,0) as TotalDM     
      ,Ltrim(Rtrim(c2.RazonComercial)) as Cliente    
      From COTICAB cc Left Join MACLIENTES c2 On cc.IDCliente=c2.IDCliente    
      Where (Ltrim(Rtrim(@IDCliente))='' Or cc.IDCliente=@IDCliente)  
     And cc.CoTipoVenta='RC' --And cc.IDCliente=@IDCliente              
--     and (cc.Estado='A') or           
--  ((cc.Estado='X' and cc.IDCAB in (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG')))    
     and (cc.Estado='A' or (cc.Estado='X' and cc.IDCAB in (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG')))    
 ) as X    
   ) As Y              
  GROUP BY Y.Cliente,Y.AnioMes ORDER BY 1  
*/

--select 'a',* from @Ventas  
               
   DECLARE @ComparaVtasClientes TABLE(    
   NroOrden int,    
   Cliente varchar(80),    
   Anio char(4),              
   Ene numeric(12,2),              
   Feb numeric(12,2),              
   Mar numeric(12,2),              
   Abr numeric(12,2),              
   May numeric(12,2),              
   Jun numeric(12,2),              
   Jul numeric(12,2),              
   Ago numeric(12,2),              
   Seti numeric(12,2),              
   Oct numeric(12,2),              
   Nov numeric(12,2),              
   Dic numeric(12,2),      
   TotalVentaAnioPas numeric(12,2),          
   Total numeric(14,2),              
   Growth numeric(14,2))              
                
  --Insert Into #ComparaVtasClientes (Anio,Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Seti,Oct,Nov,Dic)               
  Insert Into @ComparaVtasClientes (Cliente,Anio,Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Seti,Oct,Nov,Dic,TotalVentaAnioPas)       
  Select X.Cliente,X.Anio,X.Ene,X.Feb,X.Mar,X.Abr,X.May,X.Jun,X.Jul,X.Ago,X.Seti,X.Oct,X.Nov,X.Dic,X.VentaAnioPas from (  
  Select Distinct Cliente,LEFT(AnioMes,4) as Anio,0 as Ene,0 as Feb,0 as Mar,0 as Abr,0 as May,0 as Jun,0 as Jul,0 as Ago,0 as Seti,0 as Oct,0 as Nov,0 as Dic,  
    (select Isnull(SUM(v1.TotalVenta),0) from @Ventas v1 where v1.Cliente=v2.Cliente 
		--and LEFT(v1.AnioMes,4)=Cast(CAST(@Anio as int) - 1 as CHAR(4))) as VentaAnioPas  
		and LEFT(v1.AnioMes,4)=Cast(CAST(LEFT(v2.AnioMes,4) as int) - 1 as CHAR(4))) as VentaAnioPas  
    From @Ventas v2 --#Ventas    
  Where 
  --LEFT(v2.AnioMes,4)=@Anio) as X  
  (LEFT(v2.AnioMes,4)=@Anio Or ltrim(rtrim(@Anio))='')
  ) as X  
  Order By X.VentaAnioPas desc,X.Cliente   
        
  --Select Distinct Cliente,LEFT(AnioMes,4),0,0,0,0,0,0,0,0,0,0,0,0 From @Ventas --#Ventas               
      
	  --select 'b',* from @ComparaVtasClientes
	      
	/*	  
	 Select 'c',X.NroOrden,X.Cliente,X.AnioMes,X.TotalVenta from (      
  Select *,(select Isnull(SUM(v1.TotalVenta),0) from @Ventas v1 where v1.Cliente=v2.Cliente and LEFT(v1.AnioMes,4)=
	--Cast(CAST(@Anio as int) - 1 as CHAR(4))) as TotalxClientAniPs     
	Cast(CAST(LEFT(v2.AnioMes,4) as int) - 1 as CHAR(4))) as TotalxClientAniPs     
 From @Ventas v2) as X    
  Where X.TotalxClientAniPs <> 0 
  --and LEFT(X.AnioMes,4)=@Anio  
  and (LEFT(X.AnioMes,4)=@Anio or ltrim(rtrim(@Anio))='' )
  Order by X.TotalxClientAniPs desc,X.Cliente    
	*/

  /*
  Select 'c1',X.NroOrden,X.Cliente,X.AnioMes,X.TotalVenta from (      
  Select *,(select Isnull(SUM(v1.TotalVenta),0) from @Ventas v1 where v1.Cliente=v2.Cliente and LEFT(v1.AnioMes,4)=
	--Cast(CAST(@Anio as int) - 1 as CHAR(4))) as TotalxClientAniPs     
	Cast(CAST(LEFT(v2.AnioMes,4) as int) - 1 as CHAR(4))) as TotalxClientAniPs     
  From @Ventas v2) as X    
  Where X.TotalxClientAniPs = 0 
  --and LEFT(X.AnioMes,4)=@Anio  
  and (LEFT(X.AnioMes,4)=@Anio or ltrim(rtrim(@Anio))='' )
  Order by X.TotalxClientAniPs desc,X.Cliente   
  */
	-----
	      
  Declare curVtas cursor for     
  --Select * From @Ventas Order By cliente              
  Select X.NroOrden,X.Cliente,X.AnioMes,X.TotalVenta from (      
  Select *,(select Isnull(SUM(v1.TotalVenta),0) from @Ventas v1 where v1.Cliente=v2.Cliente and LEFT(v1.AnioMes,4)=
	--Cast(CAST(@Anio as int) - 1 as CHAR(4))) as TotalxClientAniPs     
	Cast(CAST(LEFT(v2.AnioMes,4) as int) - 1 as CHAR(4))) as TotalxClientAniPs     
  From @Ventas v2) as X    
  Where X.TotalxClientAniPs <> 0 
  --and LEFT(X.AnioMes,4)=@Anio  
  and (LEFT(X.AnioMes,4)=@Anio or ltrim(rtrim(@Anio))='' )
  Order by X.TotalxClientAniPs desc,X.Cliente    
  Open curVtas              
  Fetch Next From curVtas Into @NroOrdenFetch,@Cliente,@AnioMes,@TotalVta    
  While @@FETCH_STATUS=0              
   Begin              
   If RIGHT(@AnioMes,2)='01'               
    Update @ComparaVtasClientes Set Ene=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente       
   If RIGHT(@AnioMes,2)='02'               
    Update @ComparaVtasClientes Set Feb=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente          
   If RIGHT(@AnioMes,2)='03'               
    Update @ComparaVtasClientes Set Mar=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='04'               
    Update @ComparaVtasClientes Set Abr=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='05'               
    Update @ComparaVtasClientes Set May=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='06'               
    Update @ComparaVtasClientes Set Jun=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='07'               
    Update @ComparaVtasClientes Set Jul=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='08'               
    Update @ComparaVtasClientes Set Ago=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='09'               
    Update @ComparaVtasClientes Set Seti=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='10'               
    Update @ComparaVtasClientes Set Oct=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='11'               
    Update @ComparaVtasClientes Set Nov=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='12'               
    Update @ComparaVtasClientes Set Dic=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
       
   If @ClienteTmp <> @Cliente    
   Begin    
     set @ClienteTmp = @Cliente    
     Update @ComparaVtasClientes Set NroOrden=@NroOrden Where Cliente=@Cliente and IsNull(NroOrden,0)=0    
     Set @NroOrden = @NroOrden+1    
   End    
       
   Fetch Next From curVtas Into @NroOrdenFetch,@Cliente,@AnioMes,@TotalVta     
 End              
                
                 
  Close curVtas              
  Deallocate curVtas              
   
  
   ---- cursor para año mas antiguo
   
    Declare curVtas1 cursor for     
  --Select * From @Ventas Order By cliente              
  Select X.NroOrden,X.Cliente,X.AnioMes,X.TotalVenta from (      
  Select *,(select Isnull(SUM(v1.TotalVenta),0) from @Ventas v1 where v1.Cliente=v2.Cliente and LEFT(v1.AnioMes,4)=
	--Cast(CAST(@Anio as int) - 1 as CHAR(4))) as TotalxClientAniPs     
	Cast(CAST(LEFT(v2.AnioMes,4) as int) - 1 as CHAR(4))) as TotalxClientAniPs     
  From @Ventas v2) as X    
  Where X.TotalxClientAniPs = 0 
  --and LEFT(X.AnioMes,4)=@Anio  
  and (LEFT(X.AnioMes,4)=@Anio or ltrim(rtrim(@Anio))='' )
  Order by X.TotalxClientAniPs desc,X.Cliente    
  Open curVtas1              
  Fetch Next From curVtas1 Into @NroOrdenFetch,@Cliente,@AnioMes,@TotalVta    
  While @@FETCH_STATUS=0              
   Begin              
   If RIGHT(@AnioMes,2)='01'               
    Update @ComparaVtasClientes Set Ene=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente       
   If RIGHT(@AnioMes,2)='02'               
    Update @ComparaVtasClientes Set Feb=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente          
   If RIGHT(@AnioMes,2)='03'               
    Update @ComparaVtasClientes Set Mar=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='04'               
    Update @ComparaVtasClientes Set Abr=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='05'               
    Update @ComparaVtasClientes Set May=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='06'               
    Update @ComparaVtasClientes Set Jun=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='07'               
    Update @ComparaVtasClientes Set Jul=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='08'               
    Update @ComparaVtasClientes Set Ago=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='09'               
    Update @ComparaVtasClientes Set Seti=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='10'               
    Update @ComparaVtasClientes Set Oct=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='11'               
    Update @ComparaVtasClientes Set Nov=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
   If RIGHT(@AnioMes,2)='12'               
    Update @ComparaVtasClientes Set Dic=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and Cliente=@Cliente    
       
   If @ClienteTmp <> @Cliente    
   Begin    
     set @ClienteTmp = @Cliente    
     Update @ComparaVtasClientes Set NroOrden=@NroOrden Where Cliente=@Cliente and IsNull(NroOrden,0)=0    
     Set @NroOrden = @NroOrden+1    
   End    
       
   Fetch Next From curVtas1 Into @NroOrdenFetch,@Cliente,@AnioMes,@TotalVta     
 End              
                
                 
  Close curVtas1              
  Deallocate curVtas1       

   ---- fin cursor
                
                
  Update @ComparaVtasClientes Set Total=Ene+Feb+Mar+Abr+May+Jun+Jul+Ago+Seti+Oct+Nov+Dic              
                

--select 'd',* from @ComparaVtasClientes
       
	   --select 'e',Cliente,Anio, Total From @ComparaVtasClientes     
	            
  Declare @Anio_2 char(4), @TotalAnio numeric(14,2),@TotalAnioAnt numeric(14,2),               
   @tiCont int=1, @Growth numeric(14,2), @Client varchar(80)    
  Declare curVtasTot cursor for Select Cliente,Anio, Total From @ComparaVtasClientes              
  Open curVtasTot              
  Fetch Next From curVtasTot Into @Client,@Anio_2,@TotalAnio               
  While @@FETCH_STATUS=0              
   Begin              
                
   Set @Growth=0              
   --If @tiCont % 2 = 0              
   If @tiCont > 1              
 
     Set @Growth= CASE WHEN @TotalAnioAnt=0 THEN 0.00 ELSE cast((((@TotalAnio-@TotalAnioAnt)*100)/@TotalAnioAnt ) as  numeric(14,2))   END               
           
   Update @ComparaVtasClientes Set Growth=@Growth Where Anio=@Anio_2 and Cliente=@Client             
                 
   Set @tiCont+=1              
   Set @TotalAnioAnt=@TotalAnio              
   Fetch Next From curVtasTot Into @Client,@Anio_2,@TotalAnio               
   End              
               
  Close curVtasTot              
  Deallocate curVtasTot  
     
update @ComparaVtasClientes
set Growth=isnull(round(case when TotalVentaAnioPas=0 then 0 else ((total*100)/TotalVentaAnioPas)-100 end,2),0)
	            
  select ROW_NUMBER()Over(Partition By cliente Order By Cliente,CAST(Anio as int) asc) As ClienteVisible,*   
  from @ComparaVtasClientes  
  Where (NroOrden <= @Top Or @Top=0)  
  Order By anio desc,TotalVentaAnioPas desc,Cliente          
END;             

