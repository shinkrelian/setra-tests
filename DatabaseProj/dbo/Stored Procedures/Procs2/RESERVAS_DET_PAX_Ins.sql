﻿--RESERVAS_DET_PAX

Create Procedure [dbo].[RESERVAS_DET_PAX_Ins]
@IDReserva_Det int,
@IDPax int,
@UserMod char(4)
as
Begin
	Set NoCount On
	insert into RESERVAS_DET_PAX (
								  [IDReserva_Det]
								  ,[IDPax]
								  ,[UserMod]
								  ,[FecMod])
				values (
						@IDReserva_Det
					   ,@IDPax
					   ,@UserMod
					   ,GetDate())
End
