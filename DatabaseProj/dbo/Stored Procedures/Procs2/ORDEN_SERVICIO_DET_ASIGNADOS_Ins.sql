﻿CREATE Procedure dbo.ORDEN_SERVICIO_DET_ASIGNADOS_Ins
@NuOrden_Servicio_Det int,
@IDProveedor char(6),
@NuVehiculoProg smallint,
@UserMod Char(4)
As
Set NoCount On
INSERT INTO [dbo].[ORDEN_SERVICIO_DET_ASIGNADOS]
           ([NuOrden_Servicio_Det]
           ,[IDProveedor]
           ,[NuVehiculoProg]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@NuOrden_Servicio_Det
           ,@IDProveedor
           ,Case When @NuVehiculoProg = 0 then Null Else @NuVehiculoProg End
           ,@UserMod
           ,GETDATE())
