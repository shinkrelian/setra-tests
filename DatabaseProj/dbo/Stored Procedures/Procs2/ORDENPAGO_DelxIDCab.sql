﻿
Create Procedure dbo.ORDENPAGO_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM ORDENPAGO 
	WHERE IDReserva IN (SELECT IDReserva FROM RESERVAS WHERE IDCab = @IDCab)
	
