﻿Create Procedure dbo.MAPROVEEDORES_SelExistsxCardCodeSAP
@CardCodeSAP varchar(15),
@ExisteProveedor bit output
As
Set NoCount On
Set @ExisteProveedor = 0
Select @ExisteProveedor = 1 
from MAPROVEEDORES where Ltrim(Rtrim(CardCodeSAP))=Ltrim(Rtrim(@CardCodeSAP))
