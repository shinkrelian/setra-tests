﻿--JRF-20151013-Agregar campos nuevos ([NoHotel],[TxWebHotel])
--JRF-20160125-Agregar campos nuevos ([TxDireccHotel],[TxTelfHotel1],[TxTelfHotel2],[FeHoraChkInHotel],[FeHoraChkOutHotel],[CoTipoDesaHotel],[CoCiudadHotel])
CREATE Procedure dbo.MASERVICIOS_DIAInsxIDServicio
@IDServicio char(8),
@IDServicioNuevo char(8)
As
Set NoCount On
Insert into MASERVICIOS_DIA
SELECT @IDServicioNuevo as [IDServicio]
      ,[Dia]
      ,[IDIdioma]
      ,[DescripCorta]
      ,[Descripcion]
      ,[Mig]
      ,[UserMod]
      ,[FechaMod]
	  ,[NoHotel]
	  ,[TxWebHotel]
	  ,[TxDireccHotel]
	  ,[TxTelfHotel1]
	  ,[TxTelfHotel2]
	  ,[FeHoraChkInHotel]
	  ,[FeHoraChkOutHotel]
	  ,[CoTipoDesaHotel]
	  ,[CoCiudadHotel]
  FROM [dbo].[MASERVICIOS_DIA]
Where IDServicio = @IDServicio
