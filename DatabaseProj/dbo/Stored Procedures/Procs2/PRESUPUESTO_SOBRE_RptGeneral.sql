﻿
--JRF-20150304-Filtro por Categoria del file.
--JRF-20150410-Filtrar por nombre del TC o del guía
--HLF-20150827-And (ImporteDOL<>0 or ImporteSOL<>0)
CREATE Procedure dbo.PRESUPUESTO_SOBRE_RptGeneral  
 @IDFile char(8),              
 @IDProveedor char(6),              
 @DescGuiaTC varchar(120),          
 @CoEstado char(2),          
 @FeDetRango1 smalldatetime,          
 @FeDetRango2 smalldatetime,
 @Categoria char(1)          
As              
 Set Nocount On              
 Select Distinct FechaIn,NuPreSob, FePreSob,FechaServ, IDCab ,IDFile, Referencia, NroPax,
  CoPrvPrg, DescGuia,ImporteDOL, ImporteSOL, EjecOper,              
  EjecFinan, IDProveedor ,CoEstado, DescEstado           
 from (          
 Select prd.NuPreSob, pr.FePreSob, pr.IDCab ,cc.IDFile, cc.Titulo as Referencia,    
 IsNull(prd.CoPrvPrg,'') as CoPrvPrg, IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescGuia, uope.Nombre as EjecOper,              
 ufin.Nombre as EjecFinan, pr.IDProveedor ,pr.CoEstado,               
 Case pr.CoEstado              
  When 'NV' Then 'NUEVO'              
  When 'PG' Then 'GENERADO'+CHAR(13)+'(Operaciones)'              
  When 'EL' Then 'ELABORADO'              
  When 'EN' Then 'ENTREGADO'              
  When 'EJ' Then 'EJECUTADO'              
  When 'AN' Then 'ANULADO'              
 End as DescEstado,          
 --(select MAX(prd.FeDetPSo) from PRESUPUESTO_SOBRE_DET prd where prd.NuPreSob =pr.NuPreSob) as FecDet          
 cc.FecInicio as FechaIn,     
 --(Select SUM(Case When sd1.CoMoneda='USD' Then     
 --   isnull(dbo.FnCambioMoneda(pd1.SsTotal,sd1.CoMoneda,'SOL',dbo.FnTipoDeCambioVtaUltimoxFecha(ps1.FePreSob) ),0)    
 --   Else    
 --   isnull(pd1.SsTotal,0)    
 --   End ) as SumaImporte    
 -- From PRESUPUESTO_SOBRE_DET pd1 Inner Join MASERVICIOS_DET sd1 On pd1.IDServicio_Det=sd1.IDServicio_Det    
 -- Inner Join PRESUPUESTO_SOBRE ps1 On pd1.NuPreSob=ps1.NuPreSob    
 --Where pd1.NuPreSob=prd.NuPreSob ) As ImporteSOL    ,
 IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE pr.NuPreSob=PD2.NuPreSob And IsNull(IDMoneda,'')='USD' and PD2.CoPago <> 'TCK'),0) As ImporteDOL,
 IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE pr.NuPreSob=PD2.NuPreSob And IsNull(IDMoneda,'')='SOL' and PD2.CoPago <> 'TCK'),0) As ImporteSOL,
 cc.NroPax+IsNull(cc.NroLiberados,0) as NroPax,
 Case When cc.NroPax+IsNull(cc.NroLiberados,0) <= 8 then 'F' Else 'G' End as Categoria,
 Convert(char(10),(select min(psd.FeDetPSo) from PRESUPUESTO_SOBRE_DET psd Where psd.NuPreSob = pr.NuPreSob),103) as FechaServ
 From PRESUPUESTO_SOBRE pr Inner Join COTICAB cc On pr.IDCab=cc.IDCAB              
 Inner Join PRESUPUESTO_SOBRE_DET prd On pr.NuPreSob=prd.NuPreSob        
 --Left Join MAPROVEEDORES p On pr.CoPrvGui=p.IDProveedor              
 Left Join MAPROVEEDORES p On prd.CoPrvPrg=p.IDProveedor              
 Left Join MAUSUARIOS uope On pr.CoEjeOpe=uope.IDUsuario              
 Left Join MAUSUARIOS ufin On pr.CoEjeFin=ufin.IDUsuario              
 Left Join COTIPAX cp On pr.IDPax=cp.IDPax
 Where (cc.IDFile Like '%' + @IDFile + '%' Or ltrim(rtrim(@IDFile))='')              
 And pr.IDProveedor=@IDProveedor              
 --And (pr.CoPrvGui=@IDProveedor_PrgGuia Or ltrim(rtrim(@IDProveedor_PrgGuia))='')              
 and (LTRIM(rtrim(@CoEstado))='' Or pr.CoEstado = @CoEstado))as X          
 where 
 (ltrim(rtrim(@DescGuiaTC))='' Or x.DescGuia Like '%'+ltrim(rtrim(@DescGuiaTC))+'%') And
 (Ltrim(Rtrim(@Categoria))='' Or x.Categoria=@Categoria) and
 (convert(char(10),@FeDetRango1,103)='01/01/1900'           
 Or X.FechaIn between @FeDetRango1 and @FeDetRango2)          
 And (ImporteDOL<>0 or ImporteSOL<>0)
 --Order by X.FePreSob              
 Order by X.FechaIn    

