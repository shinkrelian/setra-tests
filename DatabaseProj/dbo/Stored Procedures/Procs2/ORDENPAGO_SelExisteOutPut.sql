﻿Create Procedure dbo.ORDENPAGO_SelExisteOutPut
@IDReserva int,
@pbExiste bit OutPut
As
	Set NoCount On
	If Exists(select IDOrdPag from ORDENPAGO Where IDReserva = @IDReserva)
		Set @pbExiste = 1
	Else
		Set @pbExiste = 0
