﻿--JRF-20130819-Correcion tipo de desayuno.        
--JRF-20141022-Agregar nuevos campos [TxLatitud,TxLongitud]
--JRF-20150227-NoConsiderar los NoShow
--JRF-20150527-Considerar solo hoteles que tengan alojamiento
--HLF-20160120-Union...
--HLF-20160126- uHtl.Descripcion as Ciudad, ...  Left Join MAUBIGEO uHtl On isnull(cdd.CoCiudadHotel,cd.IDCiudadProv)=uHtl.IDubigeo 
--HLF-20160203-comentando --And isnull(sdia.Dia,0) <>0
--INTO #HotelesRepVoucherSum    select * from #HotelesRepVoucherSum where (IDServicio in ....
CREATE Procedure dbo.OPERACIONES_DET_SelHotelesxIDCab_Rpt            
@IDCab int            
As            
 Set NoCount On            
 
 Select distinct ROW_NUMBER()Over(Partition By o.IDProveedor order by o.IDProveedor)  As Ord,            
 od.Dia  as FechaIn,        
 o.IDProveedor,        
 u.Descripcion as Ciudad,        
 upper(p.NombreCorto) as Hotel,        
 p.Direccion + ISNULL('*Tel:' + p.TelefonoRecepcion1,'') + ISNULL(' - '+p.TelefonoRecepcion2,'') as DireccionTelefono ,  
 'Check In: '+Isnull(convert(char(5),s.HoraCheckIn,108),'')+' hrs'+'*'+            
 'Check Out: '+Isnull(convert(char(5),s.HoraCheckOut,108),'')+' hrs' + '*'+             
  case when sd.TipoDesayuno = '00' then '' else 'Breakfast: ' + Isnull(ds.Descripcion,'') +'*' End         
 as CheckINCheckOUT,               
 Isnull(p.Web,'') as SitioWeb ,  
 Isnull(TxLatitud,'') as Latitud,IsNull(TxLongitud,'') as Longitud  
 ,s.IDServicio, '' as IDIdioma
 INTO #HotelesRepVoucherSum
 from OPERACIONES o  Left Join OPERACIONES_DET od on od.IDOperacion = o.IDOperacion             
 Left Join MAPROVEEDORES p On o.IDProveedor = p.IDProveedor            
 Left Join MAUBIGEO u on p.IDCiudad = u.IDubigeo             
 Left Join MASERVICIOS s on od.IDServicio = s.IDServicio            
 Left Join MASERVICIOS_DET sd on od.IDServicio_Det = sd.IDServicio_Det        
 Left Join MADESAYUNOS ds On sd.TipoDesayuno = ds.IDDesayuno        
 where o.IDCab = @IDCab and p.IDTipoProv = '001' and od.Dia is not null and od.FlServicioNoShow=0
 And Exists(select od2.IDOperacion_Det from OPERACIONES_DET od2 Left Join MASERVICIOS_DET sd2 On od2.IDServicio_Det=sd2.IDServicio_Det 
			Where od2.IDOperacion=o.IDOperacion And sd2.PlanAlimenticio=0)

 Union

 select distinct ROW_NUMBER()Over(Partition By od.IDProveedor,Isnull(cdd.NoHotel,p.NombreCorto) order by od.IDProveedor)  As Ord,            
 od.Dia  as FechaIn,        
 od.IDProveedor,        
 --od.DescCiudad as Ciudad,      
 uHtl.Descripcion as Ciudad,    
 --upper(p.NombreCorto) as Hotel,        
  upper(Isnull(cdd.NoHotel,p.NombreCorto)) as Hotel,
  isnull(cdd.TxDireccHotel,p.Direccion) + ISNULL('*Tel:' + isnull(cdd.TxTelfHotel1,p.TelefonoRecepcion1),'') + ISNULL(' - '+isnull(cdd.TxTelfHotel2,p.TelefonoRecepcion2),'') as DireccionTelefono ,  
 'Check In: '+Isnull(convert(char(5),isnull(cdd.FeHoraChkInHotel,od.HoraCheckIn),108),'')+' hrs'+'*'+            
 'Check Out: '+Isnull(convert(char(5),isnull(cdd.FeHoraChkOutHotel,od.HoraCheckOut),108),'')+' hrs' + '*'+             
  case when isnull(isnull(cdd.CoTipoDesaHotel,od.TipoDesayuno),'00') = '00' then '' else 'Breakfast: ' + Isnull(ds.Descripcion,'') +'*' End         
 as CheckINCheckOUT,               
IsNull(cdd.TxWebHotel,IsNull(p.Web,'')) as SitioWeb,
 Isnull(TxLatitud,'') as Latitud,IsNull(TxLongitud,'') as Longitud  
 ,sdia.IDServicio, sdia.IDidioma as IDIdioma
 From 
 --COTIDET cd               
 (Select ROW_NUMBER() OVER (PARTITION BY cd1.IDServicio ORDER BY cd1.IDServicio) AS DiaServ,
	od1.*, o1.IDProveedor, cd1.IDDet, o1.IDCab, cd1.IncGuia, s1.HoraCheckIn, s1.HoraCheckOut,
	--u1.Descripcion as DescCiudad, --ds1.Descripcion as DescDesayuno, 
	sd1.TipoDesayuno, p1.IDCiudad as IDCiudadProv
	From OPERACIONES_DET od1
	Inner Join OPERACIONES o1 On o1.IDOperacion=od1.IDOperacion and o1.IDCAB=@idcab 
	Inner Join MAPROVEEDORES p1 On o1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='003'      
	--Left Join MAUBIGEO u1 on p1.IDCiudad = u1.IDubigeo             
	Left Join RESERVAS_DET rd1 on od1.IDReserva_Det=rd1.IDReserva_Det
	Left Join COTIDET cd1 on (rd1.IDDet=cd1.IDDET or cd1.IDDetRel=rd1.IDDet)
	Inner Join MASERVICIOS_DET sd1 On cd1.IDServicio_Det=sd1.IDServicio_Det And sd1.ConAlojamiento = 1
	Inner Join MASERVICIOS s1 On sd1.IDServicio=s1.IDServicio    
	Left Join MADESAYUNOS ds1 On sd1.TipoDesayuno = ds1.IDDesayuno       
	Where cd1.IncGuia = 0	) od      
    
Inner Join MAPROVEEDORES p On od.IDProveedor=p.IDProveedor And p.IDTipoProv='003' 
   --Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det and sd.ConAlojamiento = 1
 --Inner Join MASERVICIOS_DIA sdia On sd.IDServicio=sdia.IDServicio 
 Left Join COTIDET_DESCRIPSERV cdd On od.IDDET=cdd.IDDet And Left(ltrim(cdd.IDIdioma),9)='HOTEL/WEB' 
 And Right(rtrim(cdd.IDIdioma),1)=od.DiaServ--=cast(sdia.dia as char(1))
 Left Join MADESAYUNOS ds On isnull(cdd.CoTipoDesaHotel,od.TipoDesayuno) = ds.IDDesayuno
 --Left Join MASERVICIOS_ALIMENTACION_DIA sad On sad.IDServicio=sdia.IDServicio And sad.dia=sdia.dia
 Left Join MASERVICIOS_ALIMENTACION_DIA sad On sad.IDServicio=od.IDServicio And sad.dia=od.DiaServ
 Left Join MAUBIGEO u2 On sad.IDUbigeoDes=u2.IDubigeo        
 Left Join MASERVICIOS_DIA sdia On sdia.IDServicio=od.IDServicio  And sdia.dia=od.DiaServ And sdia.IDIdioma='HOTEL/WEB' 
 Left Join MAUBIGEO uHtl On isnull(cdd.CoCiudadHotel,od.IDCiudadProv)=uHtl.IDubigeo   
 Where od.IDCAB=@IDCab and od.IncGuia = 0-- And IsNull(cd.IDDetRel,0)=0
	--And isnull(sdia.Dia,0) <>0
 Order by FechaIn            

   select * from #HotelesRepVoucherSum 
  where (IDServicio in
	(select IDServicio from #HotelesRepVoucherSum where IDIdioma='HOTEL/WEB') and not IDIdioma is null)
	or (not IDServicio in
	(select IDServicio from #HotelesRepVoucherSum where IDIdioma='HOTEL/WEB') )
