﻿Create Procedure dbo.MASERIESCLIENTE_Upd
@NuSerie tinyint,
@IDCliente char(6),
@NoSerie varchar(Max),
@PoConcretizacion numeric(8,2),
@UserMod char(4)
As
Set NoCount On
UPDATE [dbo].[MASERIESCLIENTE]
   SET [NoSerie] = @NoSerie
      ,[PoConcretizacion] = @PoConcretizacion
      ,[UserMod] = @UserMod
      ,[FecMod] = GetDate()
 WHERE NuSerie = @NuSerie and IDCliente = @IDCliente
