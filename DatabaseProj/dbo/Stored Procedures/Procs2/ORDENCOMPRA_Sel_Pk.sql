﻿--JHD-20150921-SSTotal_Final=oc.SsTotal+isnull(SSDetraccion,0)
CREATE Procedure dbo.ORDENCOMPRA_Sel_Pk
	@NuOrdComInt int
As

	Set Nocount on
	
	Select oc.*, isnull(p.NombreCorto,'') as DescProveedor ,
	SSTotal_Final=oc.SsTotal+isnull(SSDetraccion,0)
	From ORDENCOMPRA oc Left Join MAPROVEEDORES p On p.IDProveedor=oc.CoProveedor
	Where NuOrdComInt=@NuOrdComInt;
