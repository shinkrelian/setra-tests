﻿--HLF-20130730-Agregando Case When @IDReserva_Det=0 Then Null para IDReserva_Det, IDServicio_Det
--JRF-20150411-Agregar una descripción para textos manuales (@TxDescripcion)
--JHD-20150909-Se agrego el campo Total_Final el cual es la diferencia del total con la detracción
--JHD-20150909-Agregar el campo SSDetraccion_USD
--JHD-20150909-@Total-(case when isnull(@SSDetraccion_USD,0)=0 then @SSDetraccion else @SSDetraccion_USD end),
CREATE Procedure [dbo].[ORDENPAGO_DET_Upd]  
@IDOrdPag_Det int,  
@IDOrdPag int,  
@IDReserva_Det int,  
@TxDescripcion varchar(Max),
@IDServicio_Det int,  
@NroPax smallint,  
@NroLiberados smallint,  
@Cantidad tinyint,  
@Noches tinyint,  
@Total numeric(12,2),  
@SSDetraccion numeric(12,2)=0,
@SSDetraccion_USD numeric(12,2)=0,
@SSOtrosDescuentos numeric(12,2)=0,
@UserMod char(4)  
As  
  Set NoCount On  
  UPDATE ORDENPAGO_DET  
     SET IDOrdPag = @IDOrdPag   
     ,IDReserva_Det = Case When @IDReserva_Det=0 Then Null Else @IDReserva_Det End
	 ,TxDescripcion = Case When Ltrim(rtrim(@TxDescripcion))='' Then Null else @TxDescripcion End
     ,IDServicio_Det =  Case When @IDServicio_Det=0 Then Null Else @IDServicio_Det End     
     ,NroPax = @NroPax   
     ,NroLiberados = @NroLiberados  
     ,Cantidad = @Cantidad  
     ,Noches = @Noches  
     ,Total = @Total  
	 ,SSDetraccion=@SSDetraccion
	 ,SSDetraccion_USD=@SSDetraccion_USD
	 ,SSOtrosDescuentos=isnull(@SSOtrosDescuentos,0)
	 --,Total_Final= @Total-(case when isnull(@SSDetraccion_USD,0)=0 then @SSDetraccion else @SSDetraccion_USD end)--@Total-@SSDetraccion
	 ,Total_Final= @Total-(case when isnull(@SSDetraccion_USD,0)=0 then @SSDetraccion else @SSDetraccion_USD end)-isnull(@SSOtrosDescuentos,0)--@Total-@SSDetraccion
     ,UserMod = @UserMod  
     ,FecMod = GetDate()  
   WHERE IDOrdPag_Det = @IDOrdPag_Det  
