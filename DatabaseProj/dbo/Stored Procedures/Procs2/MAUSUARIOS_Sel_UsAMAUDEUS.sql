﻿CREATE Procedure dbo.MAUSUARIOS_Sel_UsAMAUDEUS
as
Set NoCount On
select IDUsuario,Usuario+SPACE(50)+'|'+CoUsrAmd
from MAUSUARIOS 
where CoUsrAmd is not null and Activo = 'A'
Order by Nombre
