﻿Create Procedure dbo.OPERACIONES_DET_PAX_SelxIDOperacion
@IDOperacion_Det int
As
select p.Nombres +' '+p.Apellidos As NombreCompleto
from OPERACIONES_DET_PAX odp Left Join OPERACIONES_DET od On odp.IDOperacion_Det = od.IDOperacion_Det
	 Left Join COTIPAX p on odp.IDPax = p.IDPax
Where odp.IDOperacion_Det = @IDOperacion_Det
