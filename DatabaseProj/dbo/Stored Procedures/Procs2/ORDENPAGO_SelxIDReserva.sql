﻿CREATE Procedure dbo.ORDENPAGO_SelxIDReserva 
@IDReserva int ,
@IDCAB int
As
	Set NoCount On
	SELECT [IDOrdPag]
		  ,[IDReserva]
		  ,[IDCab]
		  ,[FechaPago]
		  ,[IDEstado]
		  ,[IDMoneda]
      FROM ORDENPAGO
      WHERE IDReserva = @IDReserva And IDCab = @IDCAB
