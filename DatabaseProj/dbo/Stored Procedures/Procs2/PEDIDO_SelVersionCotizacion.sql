﻿Create Procedure dbo.PEDIDO_SelVersionCotizacion
	@NuPedInt int,
	@pVersion tinyint output
As
	Set NoCount On

	Set @pVersion=isnull((Select Max(DocWordVersion) From COTICAB Where NuPedInt=@NuPedInt),0)+1
		

