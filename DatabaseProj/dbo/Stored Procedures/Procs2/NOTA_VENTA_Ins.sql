﻿--HLF-20130116-@IDNotaVenta a char(8)
CREATE Procedure dbo.NOTA_VENTA_Ins
	@IDSerie char(3),
	@IDCab int,
	@IDFile	char(8),
	@IDMoneda char(3),
	@SubTotal numeric(8,2),
	@TotalIGV numeric(8,2),
	@Total numeric(8,2),
	@IDEstado char(2),
	@UserMod char(4),
	@pIDNotaVenta char(8) output
As
	Set NoCount On
	
	Declare @IDNotaVenta char(8)  
    --Declare @siCorr smallint  
    --Declare @iCorrTabla int   

	Set @IDNotaVenta = @IDFile  
	--Set @siCorr =ISNull((Select SUBSTRING(Max(IDNotaVenta),9,2)   
	--From NOTA_VENTA Where IDCab=@IDCab),-1)  
	--Set @siCorr += 1    
	--Set @IDNotaVenta = @IDNotaVenta +    
	--Case When @siCorr<=9 Then '0' End + CAST(@siCorr as CHAR(2))  

	
	INSERT INTO NOTA_VENTA
           ([IDNotaVenta]
           ,[IDSerie]
           ,[IDCab]
           ,[IDMoneda]
           ,[SubTotal]
           ,[TotalIGV]
           ,[Total]
           ,[IDEstado]
           ,[UserMod]
           )
     VALUES
           (@IDNotaVenta,
            @IDSerie,
            @IDCab,
            @IDMoneda,
            @SubTotal,
            @TotalIGV,
            @Total,
            @IDEstado,
            @UserMod)
	
	Set @pIDNotaVenta=@IDNotaVenta 

