﻿--JRF-20141223-Order by.. ,rd.CapacidadHab,rd.EsMatrimonial    
--JRF-20150106-Debes validar solo hoteles sin plan alimenticio o No hoteles con alojamiento  
--JRF-20150219-Considerar nuevo cambio de ingresos manuales.
CREATE Procedure dbo.RESERVAS_DET_SelServiciosxAcomodoEspecial          
@IDCab int,          
@IDProveedor char(6)          
As          
Set NoCount On          
select rd.IDReserva_Det,rd.IDDet, CAST(rd.Cantidad as varchar(5))+' '+ rd.Servicio As TituloAcomodoxCapacidad  , convert(Char(10),rd.Dia,103) as FechaIn,convert(Char(10),rd.FechaOut,103) as FechaOut,           
    rd.Cantidad,          
    case when (select COUNT(iddet) from cotidet cd          
   where AcomodoEspecial = 1 and cd.IncGuia = 0 and IDCab = r.IDCab and r.IDProveedor = cd.IDProveedor          
   and cd.Dia between rd.Dia and rd.FechaOut)>0           
   then 1 Else 0 End as ExisteAcomodoEspecial,rd.CapacidadHab,rd.EsMatrimonial,          
   CAST(rd.Cantidad as varchar(5))+' ' + dbo.FnMailAcomodosReservas(rd.CapacidadHab,rd.EsMatrimonial) as TituloAcomodo,          
   dbo.FnAcomodosReservasxIDReserva(r.IDReserva,rd.Dia) as Acomodo,rd.Servicio          
from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva          
 Left Join MASERVICIOS_DET sd On sd.IDServicio_Det = rd.IDServicio_Det           
 Left Join COTIDET cd On rd.IDDet = cd.IDDET           
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor  
where rd.FlServicioIngManual=0 and r.IDCab = @IDCab and r.IDProveedor =@IDProveedor and r.Anulado = 0  and rd.Cantidad <> 0          
  and ((p.IDTipoProv='001' and sd.PlanAlimenticio = 0) Or  
    (p.IDTipoProv<>'001' and sd.ConAlojamiento = 1)) and rd.Anulado = 0 --and rd.FechaOut is not null          
Order by rd.Dia,rd.CapacidadHab,rd.EsMatrimonial    
