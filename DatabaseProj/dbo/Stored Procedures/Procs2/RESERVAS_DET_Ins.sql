﻿--JRF-20121211-Nuevos Campos                          
--JRF-20121218-Nuevos Campos (DescGuia, DescBus)                        
--HLF-20130508-Agregando parametro IDMoneda                      
--HLF-20130515-Cambiando @CantidadAPagar de smallint a numeric(5,2)                    
--JRF-20130618-Cambiando el campo DescGuia x un IDProveedor de Guia(IDGuiaProveedor).                   
--JRF-20130701-Cambiando el campo DescBus x un NuVehiculo de Vehiculos.                
--HLF-20131125-@IgvGen numeric(5,2) a numeric(6,2), @IgvHab numeric(5,2) a numeric(6,2)              
--JRF-20140227-Aumento de Longitud para las columnas con numeric(12,4)          
--JRF-20140905-Agregar el nuevo campo [EsGuia]        
--JRF-20141201-Nuevo campo [FeUpRow]  
--JRF-20150218-Nuevo campo [FlServicioNoShow]
--PPMG-20151014-CantidadAPagarEditado,NetoHabEditado,IgvHabEditado 
--JRF-20151113-Nuevo campo para Acomodos Residentes..
--JRF-20151118-Nuevo campo @NuViaticosTC
CREATE Procedure [dbo].[RESERVAS_DET_Ins]                              
 @IDReserva int,                              
 @IDFile int,                              
 @IDDet int,                              
 @Item numeric(5,3),
 @Dia smalldatetime,                              
 @FechaOut smalldatetime,                              
 @Noches tinyint,                              
 @IDubigeo char(6),                              
 @IDServicio char(8),                              
 @IDServicio_Det int,                              
 @Especial bit,                              
 @MotivoEspecial varchar(200),                              
 @RutaDocSustento varchar(200),                              
 @Desayuno bit,                              
 @Lonche bit,                              
 @Almuerzo bit,                              
 @Cena bit,                              
 @Transfer bit,                              
 @IDDetTransferOri int,                              
 @IDDetTransferDes int,                              
 @CamaMat tinyint=0,                              
 @TipoTransporte char(1),                              
 @IDUbigeoOri char(6),                              
 @IDUbigeoDes char(6),                              
 @IDIdioma varchar(12),                              
 @NroPax smallint,                              
 @NroLiberados smallint,                              
 @Tipo_Lib char(1),                              
 @Cantidad tinyint,                              
 @CantidadAPagar numeric(5,2),                          
 @CostoReal numeric(12,4),                              
 @CostoRealAnt numeric(12,4),                              
 @CostoLiberado numeric(12,4),                              
 @Margen numeric(12,4),                              
 @MargenAplicado numeric (6,2),                              
 @MargenAplicadoAnt numeric(12,4),                              
 @MargenLiberado numeric(12,4),                              
 @Total numeric(8,2),                              
 @TotalOrig numeric(12,4),                              
 @CostoRealImpto numeric(12,4),                              
 @CostoLiberadoImpto numeric(12,4),                              
 @MargenImpto numeric(12,4),                              
 @MargenLiberadoImpto numeric(12,4),                              
 @TotImpto numeric(12,4),                              
 @NetoHab numeric(12,4),                          
 @IgvHab numeric(12,4),                          
 @TotalHab numeric(12,4),                          
 @NetoGen numeric(12,4),                          
 @IgvGen numeric(12,4),                          
 @TotalGen numeric(12,4),                    
 @IDGuiaProveedor char(6),                      
 @NuVehiculo smallint,                
 @IDDetRel int,                              
 @Servicio varchar(max),                              
 @IDReserva_DetCopia int,                              
 @IDReserva_Det_Rel int,                   
 @IDEmailEdit varchar(255),                       
 @IDEmailNew varchar(255),                              
 @IDEmailRef varchar(255),                              
 @CapacidadHab tinyint,                            
 @EsMatrimonial bit,           
 @FlServicioParaGuia bit,     
 @FlServicioNoShow bit,     
 @FlServicioTC bit,
 @IDMoneda char(3),                      
 @UserMod char (4),
 @CantidadAPagarEditado bit,
 @NetoHabEditado bit,
 @IgvHabEditado bit,
 @AcomdoResidente bit,
 @NuViaticoTC int = 0,
 @pIDReserva_Det int Output                           
As                              
 Set NoCount On             
 Declare @IDReserva_Det int                              
 Execute dbo.Correlativo_SelOutput 'RESERVAS_DET',1,10,@IDReserva_Det output                              
                    
 INSERT INTO RESERVAS_DET                              
 ([IDReserva_Det]                              
 ,[IDReserva]                              
 ,[IDFile]                              
 ,[IDDet]                              
 ,[Item]                              
 ,[Dia]           
 ,[DiaNombre]                              
 ,[FechaOut]                        
 ,[Noches]                        
 ,[IDubigeo]                              
 ,[IDServicio]                              
 ,[IDServicio_Det]                              
 ,[Especial]                              
 ,[MotivoEspecial]                  
 ,[RutaDocSustento]                              
 ,[Desayuno]                              
 ,[Lonche]                              
 ,[Almuerzo]                              
 ,[Cena]                              
 ,[Transfer]                              
 ,[IDDetTransferOri]                              
 ,[IDDetTransferDes]                              
 ,[CamaMat]                              
 ,[TipoTransporte]                              
 ,[IDUbigeoOri]                              
 ,[IDUbigeoDes]                              
 ,[IDIdioma]                              
 ,[NroPax]                              
 ,[NroLiberados]                              
 ,[Tipo_Lib]                              
 ,Cantidad                            
 ,CantidadAPagar                          
 ,IDMoneda                      
 ,[CostoReal]                              
 ,[CostoRealAnt]                              
 ,[CostoLiberado]                              
 ,[Margen]                              
 ,[MargenAplicado]                              
 ,[MargenAplicadoAnt]                              
 ,[MargenLiberado]                              
 ,[Total]                     
 ,[TotalOrig]                              
 ,[CostoRealImpto]                              
 ,[CostoLiberadoImpto]                              
 ,[MargenLiberadoImpto]                              
 ,[MargenImpto]                              
 ,[TotImpto]                           
 ,[NetoHab]                            
 ,[IgvHab]                            
 ,[TotalHab]                            
 ,[NetoGen]                         
 ,[IgvGen]                            
 ,[TotalGen]                        
 ,[IDGuiaProveedor]  
 ,[NuVehiculo]  
 ,[IDDetRel]  
 ,[Servicio]  
 ,[IDReserva_DetCopia]  
 ,[IDReserva_Det_Rel]  
 ,[IDEmailEdit]  
 ,[IDEmailNew]  
 ,[IDEmailRef]  
 ,[CapacidadHab]  
 ,[EsMatrimonial]  
 ,[FlServicioParaGuia]  
 ,[FeUpdateRow]  
 ,[FlServicioNoShow]  
 ,[FlServicioIngManual]  
 ,[FlServicioTC]
 ,[UserMod]                              
 ,[FecMod]
 ,[CantidadAPagarEditado]
 ,[NetoHabEditado]
 ,[IgvHabEditado]
 ,[FlAcomodoResidente]
 ,[NuViaticoTC])
 VALUES                              
 (@IDReserva_Det                              
 ,@IDReserva                              
 ,@IDFile                              
 ,Case When @IDDet=0 Then Null Else @IDDet End                              
 ,@Item                              
 ,@Dia                       
 ,DATENAME(dw,@Dia)                              
 ,Case When @FechaOut='01/01/1900' Then Null Else @FechaOut End                              
 ,Case When @Noches=0 Then Null Else @Noches End                              
 ,@IDubigeo                              
 ,@IDServicio                              
 ,@IDServicio_Det                              
 ,@Especial                              
 ,Case When ltrim(rtrim(@MotivoEspecial))='' Then Null Else @MotivoEspecial End                              
 ,Case When ltrim(rtrim(@RutaDocSustento))='' Then Null Else @RutaDocSustento End                              
 ,@Desayuno                       
 ,@Lonche                              
 ,@Almuerzo                              
 ,@Cena                              
 ,@Transfer                              
 ,Case When LTRIM(rtrim(@IDDetTransferOri))='' Then Null Else @IDDetTransferOri End                              
 ,Case When ltrim(rtrim(@IDDetTransferDes))='' Then Null Else @IDDetTransferDes End                              
 ,Case When ltrim(rtrim(@CamaMat))='' then Null Else @CamaMat End                              
 ,Case When LTRIM(rtrim(@TipoTransporte))='' Then Null Else @TipoTransporte End                              
 ,Case When ltrim(rtrim(@IDUbigeoOri))='' Then Null Else @IDUbigeoOri End                              
 ,Case When LTRIM(rtrim(@IDUbigeoDes))='' Then Null Else @IDUbigeoDes End                              
 ,@IDIdioma                                    
 ,@NroPax                              
 ,Case When ltrim(rtrim(@NroLiberados))='' Then Null Else @NroLiberados End                              
 ,Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End                              
 ,@Cantidad                            
 ,@CantidadAPagar                          
 ,Case When ltrim(rtrim(@IDMoneda))='' Then Null Else @IDMoneda End                      
 ,@CostoReal                              
 ,Case When LTRIM(rtrim(@CostoRealAnt))='' Then Null Else @CostoRealAnt End                              
 ,@CostoLiberado                              
 ,@Margen                              
 ,@MargenAplicado                              
 ,Case When LTRIM(rtrim(@MargenAplicadoAnt))='' Then Null Else @MargenAplicadoAnt End                              
 ,@MargenLiberado                              
 ,@Total                  
 ,@TotalOrig                              
 ,@CostoRealImpto                              
 ,@CostoLiberadoImpto                              
 ,@MargenLiberadoImpto                              
 ,@MargenImpto                              
 ,@TotImpto                           
 ,@NetoHab                            
 ,@IgvHab                            
 ,@TotalHab                            
 ,@NetoGen                            
 ,@IgvGen                            
 ,@TotalGen                         
 ,Case When ltrim(rtrim(@IDGuiaProveedor))='' Then Null Else @IDGuiaProveedor End                        
 ,Case When @NuVehiculo = 0 Then Null Else @NuVehiculo End                        
 ,Case When LTRIM(rtrim(@IDDetRel))='' Then Null Else @IDDetRel End                              
 ,Case When ltrim(rtrim(@Servicio))='' then Null Else @Servicio End                              
 ,Case When LTRIM(rtrim(@IDReserva_DetCopia))='' Then Null Else @IDReserva_DetCopia End                              
 ,Case When LTRIM(rtrim(@IDReserva_Det_Rel))='' Then Null Else @IDReserva_Det_Rel End                              
 ,Case When LTRIM(rtrim(@IDEmailEdit))='' Then Null Else @IDEmailEdit End                              
 ,Case When LTRIM(rtrim(@IDEmailNew))='' Then Null Else @IDEmailNew End                              
 ,Case When LTRIM(rtrim(@IDEmailRef))='' Then Null Else @IDEmailRef End                              
 ,@CapacidadHab                            
 ,@EsMatrimonial            
 ,@FlServicioParaGuia         
 ,Null                
 ,@FlServicioNoShow      
 ,Case When @FlServicioNoShow=1 Or @FlServicioParaGuia=1 Or @NuViaticoTC > 0 Then 1 Else 0 End  
 ,@FlServicioTC
 ,@UserMod                              
 ,GetDate()
 ,@CantidadAPagarEditado
 ,@NetoHabEditado
 ,@IgvHabEditado
 ,@AcomdoResidente
 ,@NuViaticoTC)
 Set @pIDReserva_Det=@IDReserva_Det                              
