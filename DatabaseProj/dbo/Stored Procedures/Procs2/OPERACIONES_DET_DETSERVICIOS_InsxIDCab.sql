﻿--HLF-20130225-Agregando grabacion de TipoCambio,IDMoneda, IDServicio          
--HLF-20130809-Agregando al subquery And IDReserva In (Select r1.IDReserva From RESERVAS r1         
 --Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E'        
 -- Where IDCab=@IDCab         
--HLF-20130902-Agregando And Estado<>'XL'         
--HLF-20140122-Agregando  And r1.Anulado=0         
--HLF-20141013-SD.CoMoneda    
--HLF-20150630-QtPax ,od.NroPax+ISNULL(od.NroLiberados,0) as PaxLiberados
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_InsxIDCab            
 @IDCab int,             
 @UserMod char(4)            
As            
 Set Nocount On            
            
 Insert Into OPERACIONES_DET_DETSERVICIOS            
 (IDOperacion_Det,IDServicio_Det,IDServicio,IDServicio_Det_V_Cot,CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,            
 SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,            
 MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,            
 STMargenImpto,TotImpto,          
 TipoCambio,IDMoneda, QtPax,
 UserMod)              
 Select Distinct od.IDOperacion_Det, rs.IDServicio_Det,sd.IDServicio,sd.IDServicio_Det_V,           
 rs.CostoReal*od.NroPax,rs.CostoLiberado*od.NroPax,rs.Margen*od.NroPax,rs.MargenAplicado,rs.MargenLiberado*od.NroPax,rs.Total*od.NroPax,            
 rs.SSCR*od.NroPax,rs.SSCL*od.NroPax,rs.SSMargen*od.NroPax,rs.SSMargenLiberado*od.NroPax,rs.SSTotal*od.NroPax,rs.STCR*od.NroPax,            
 rs.STMargen*od.NroPax,rs.STTotal*od.NroPax,rs.CostoRealImpto*od.NroPax,rs.CostoLiberadoImpto*od.NroPax,rs.MargenImpto*od.NroPax,rs.MargenLiberadoImpto*od.NroPax,            
 rs.SSCRImpto*od.NroPax,rs.SSCLImpto*od.NroPax,rs.SSMargenImpto*od.NroPax,rs.SSMargenLiberadoImpto*od.NroPax,rs.STCRImpto*od.NroPax,rs.STMargenImpto*od.NroPax,            
 rs.TotImpto*od.NroPax,          
 sd.TC, --Case When sd.TC IS NULL Then 'USD' Else 'SOL' End,           
 sd.CoMoneda, od.NroPax+ISNULL(od.NroLiberados,0) as PaxLiberados,
 @UserMod
 From RESERVAS_DETSERVICIOS rs Inner Join RESERVAS_DET rd On rs.IDReserva_Det=rd.IDReserva_Det            
 Inner Join RESERVAS r On r.IDReserva=rd.IDReserva And IDCab=@IDCab            
 And r.IDReserva In (Select r1.IDReserva From RESERVAS r1         
 Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E' And r1.Estado<>'XL'      
 And r1.Anulado=0 Where IDCab=@IDCab)                 
 Inner Join OPERACIONES_DET od On rd.IDReserva_Det=od.IDReserva_Det            
 Inner Join MASERVICIOS_DET sd On rs.IDServicio_Det=sd.IDServicio_Det            
 Where IsNull(rs.Total,0)>0
