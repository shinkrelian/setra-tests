﻿CREATE PROCEDURE [dbo].[MAUSUARIOSOPC_InsxUsuarioyNivel]
	@IDUsuario	char(4),
	@IDNivel	char(3),
	@UserMod	char(4)
As
	Set NoCount On
	
	Insert Into MAUSUARIOSOPC(IDOpc,IDUsuario,Consultar, Grabar, Actualizar, Eliminar, UserMod)
	Select mo.IDOPc,@IDUsuario,mn.Consultar,mn.Grabar,mn.Actualizar,mn.Eliminar,@UserMod From MAOPCIONES mo 
	Inner Join MANIVELOPC mn On mo.IDOpc=mn.IDOpc And mn.IDNivel=@IDNivel
