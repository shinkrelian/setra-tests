﻿
--JRF-20130320-Nuevo campo CodTarifa      
--JRF-20130717-Nuevo campo PoliticaLibNivelDetalle    
--JRF-20131209-Aumentando el valor de los decimales a (numeric(10, 4))  
--HLF-20140715-Nuevo campo CoMoneda
--JHD-20150603-Nuevo campo CoCeCos
--JHD-20150610-Nuevo campo FlDetraccion
--HLF-@Monto_sgls, @Monto_dbls, @Monto_tris de n(10,4) a n(12,4)
CREATE Procedure [dbo].[MASERVICIOS_DET_Ins]            
 @IDServicio char(8),            
 @IDServicio_Det_V int,            
 @Anio char(4),            
 @Tipo varchar(7),             
 @DetaTipo text,            
 @VerDetaTipo bit,            
 @Codigo varchar(20),            
 @Descripcion varchar(200),            
 @Afecto bit,                
 @Monto_sgl numeric(10, 4),            
 @Monto_dbl numeric(10, 4),            
 @Monto_tri numeric(10, 4),            
 @cod_x_triple varchar(20),            
 @IDServicio_DetxTriple int=0,            
 @Monto_sgls numeric(12, 4),            
 @Monto_dbls numeric(12, 4),            
 @Monto_tris numeric(12, 4),            
 @TC numeric(10, 4),            
          
 @ConAlojamiento bit,            
 @PlanAlimenticio bit,         
       
 @CodTarifa varchar(50) ,      
          
 @DiferSS numeric(10, 4),            
 @DiferST numeric(10, 4),            
          
 @TipoGasto char(1),            
 @TipoDesayuno char(2),            
          
 @Desayuno bit,            
 @Lonche bit,            
 @Almuerzo bit,            
 @Cena bit,            
          
 @PoliticaLiberado bit,            
 @Liberado tinyint,            
 @TipoLib char(1),            
 @MaximoLiberado tinyint,            
 @LiberadoM tinyint,            
 @MontoL numeric(10, 4),     
         
 @PoliticaLibNivelDetalle bit,    
        
 @Tarifario bit,            
 @TituloGrupo varchar(30),            
 @idTipoOC char(3),            
 @CtaContable varchar(20),             
 @CtaContableC varchar(20),           
          
 @IdHabitTriple char(3),            
 @CoMoneda char(3),
 @UserMod char(4),  
 @CoCeCos char(6)='',          
 @FlDetraccion bit=0,
 @pIDServicio_Det int Output            
As            
 Set NoCount On            
 Declare @IDServicio_Det int = (Select IsNull(Max(IDServicio_Det),0)+1 From MASERVICIOS_DET)            
 Declare @Correlativo tinyint= (Select IsNull(Max(Correlativo),0)+1 From MASERVICIOS_DET             
 Where IDServicio=@IDServicio And Anio=@Anio And Tipo=@Tipo)            
          
 INSERT INTO MASERVICIOS_DET            
 (IDServicio_Det            
 ,IDServicio            
 ,IDServicio_Det_V            
 ,Anio            
 ,Tipo            
 ,Correlativo            
 ,DetaTipo            
 ,VerDetaTipo            
 ,Codigo            
 ,Descripcion            
 ,Afecto            
 --,IDMoneda            
 ,Monto_sgl            
 ,Monto_dbl            
 ,Monto_tri            
 ,cod_x_triple            
 ,IDServicio_DetxTriple            
 ,Monto_sgls            
 ,Monto_dbls            
 ,Monto_tris            
 ,TC            
            
 ,ConAlojamiento            
 ,PlanAlimenticio            
       
 ,CodTarifa      
       
 ,DiferSS            
 ,DiferST            
            
 ,TipoGasto             
 ,TipoDesayuno            
            
 ,Desayuno            
 ,Lonche            
 ,Almuerzo            
 ,Cena            
          
 ,PoliticaLiberado            
 ,Liberado            
 ,TipoLib            
 ,MaximoLiberado            
 ,LiberadoM            
 ,MontoL      
     
 ,PoliticaLibNivelDetalle    
           
 ,Tarifario            
 ,TituloGrupo            
 ,idTipoOC            
 ,CtaContable            
 ,CtaContableC           
 ,IdHabitTriple    
 ,CoMoneda                  
 ,UserMod
 ,CoCeCos
 ,FlDetraccion)            
 VALUES            
 (@IDServicio_Det,            
 @IDServicio,            
 Case When @IDServicio_Det_V=0 Then Null Else @IDServicio_Det_V End,            
 @Anio,            
 ltrim(rtrim(@Tipo)),            
 @Correlativo,            
 Case When ltrim(rtrim(Cast (@DetaTipo as varchar(max))))='' Then Null Else @DetaTipo End,            
 @VerDetaTipo,            
 @Codigo,            
 @Descripcion,            
 @Afecto,            
 --@IDMoneda,            
 Case When @Monto_sgl=0 Then Null Else @Monto_sgl End,            
 Case When @Monto_dbl=0 Then Null Else @Monto_dbl End,            
 Case When @Monto_tri=0 Then Null Else @Monto_tri End,            
 Case When ltrim(rtrim(@cod_x_triple))='' Then Null Else @cod_x_triple End,            
 Case When @IDServicio_DetxTriple=0 Then Null Else @IDServicio_DetxTriple End,            
 Case When @Monto_sgls=0 Then Null Else @Monto_sgls End,            
 Case When @Monto_dbls=0 Then Null Else @Monto_dbls End,            
 Case When @Monto_tris=0 Then Null Else @Monto_tris End,            
 Case When @TC=0 Then Null Else @TC End,            
             
 @ConAlojamiento,            
 @PlanAlimenticio,       
       
 Case When ltrim(rtrim(@CodTarifa))='' then Null else @CodTarifa End,      
            
 Case When @DiferSS=0 Then Null Else @DiferSS End,            
 Case When @DiferST=0 Then Null Else @DiferST End,                       
             
 Case When ltrim(rtrim(@TipoGasto))='' Then Null Else @TipoGasto End,        
 @TipoDesayuno,            
             
 @Desayuno,            
 @Lonche,            
 @Almuerzo,            
 @Cena,            
             
 @PoliticaLiberado,            
 Case When @Liberado=0 Then Null Else @Liberado End,            
 Case When @TipoLib='' Then Null Else @TipoLib End,            
 Case When @MaximoLiberado=0 Then Null Else @MaximoLiberado End,            
 Case When @LiberadoM=0 Then Null Else @LiberadoM End,            
 Case When @MontoL=0 Then Null Else @MontoL End,      
 @PoliticaLibNivelDetalle,    
           
 @Tarifario,            
 Case When ltrim(rtrim(@TituloGrupo))='' Then Null Else @TituloGrupo End,            
 @idTipoOC,            
 Case When ltrim(rtrim(@CtaContable))='' Then '000000' Else @CtaContable End,            
 Case When ltrim(rtrim(@CtaContableC))='' Then Null Else @CtaContableC End,          
 @IdHabitTriple,                        
 @CoMoneda,
 @UserMod ,
 Case When ltrim(rtrim(@CoCeCos))='' Then Null Else @CoCeCos End,
 @FlDetraccion       
 )            
          
 Set @pIDServicio_Det = @IDServicio_Det            

