﻿
--HLF-20130731-Agregando IDServicio_Det_V_Cot=Null,IDServicio_Det_V_Prg=Null,IDProveedor_Prg=Null,  
--JRF-20150109-Comentar el null del IDServicio_Det_V_Cot
--HLF-20150630--And FlServicioNoShow = @FlServicioNoShowWhere
CREATE Procedure [dbo].[OPERACIONES_DET_DETSERVICIOS_ActivarDesactivar]    
 @IDOperacion_Det int,    
 @IDServicio_Det int,  
 @FlServicioNoShowWhere bit,  
 @Activar bit,    
 @UserMod char(4)    
As    
 Set NoCount On    
  
 Update OPERACIONES_DET_DETSERVICIOS Set    
 UserMod=@UserMod,     
 Activo=@Activar,    
 IDServicio_Det_V_Cot=Case When @Activar=0 Then Null Else (select SD.IDServicio_Det_V from maservicios_det SD where SD.IDServicio_Det=@IDServicio_Det) End ,--Null,
 IDServicio_Det_V_Prg=Null,
 IDProveedor_Prg=Null, 
 IDEstadoSolicitud='NV',    
 FecMod=GETDATE()    
 From OPERACIONES_DET_DETSERVICIOS    
 Where IDOperacion_Det=@IDOperacion_Det     
 And IDServicio_Det=@IDServicio_Det
And FlServicioNoShow = @FlServicioNoShowWhere


