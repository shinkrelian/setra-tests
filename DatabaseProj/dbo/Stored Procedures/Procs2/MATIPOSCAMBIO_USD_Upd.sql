﻿CREATE PROCEDURE DBO.MATIPOSCAMBIO_USD_Upd
@CoMoneda char(3),
@FeTipCam smalldatetime,
@SsTipCam decimal(6,3),
@UserMod char(4)
as
SET NOCOUNT ON
UPDATE [dbo].[MATIPOSCAMBIO_USD]
   SET [SsTipCam] = @SsTipCam
      ,[UserMod] = @UserMod
      ,[FecMod] = GETDATE()
 WHERE CONVERT(CHAR(10),FeTipCam,103)=CONVERT(CHAR(10),@FeTipCam,103) AND
	   CoMoneda = @CoMoneda
