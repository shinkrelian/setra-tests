﻿CREATE Procedure [dbo].[MATEXTOSERVICIOS_Del]
	@IDServicio	char(8),
	@IDIdioma	varchar(12)
As
	Set NoCount On
	
	Delete From MATEXTOSERVICIOS
	Where IDServicio=@IDServicio And IDIdioma=@IDIdioma
