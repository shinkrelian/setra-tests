﻿--MLL-29052014-Modificado la consulta 
CREATE Procedure [dbo].[MATIPOCAMBIO_Sel_List]
	@FechaInicio	smalldatetime,
	@FechaFin	smalldatetime
As
	Set NoCount On
	
	--Select Fecha, ValCompra, ValVenta
	--From MATIPOCAMBIO
	--Where (Fecha = @Fecha Or @Fecha='01/01/1900')
	
	Select Fecha, ValCompra, ValVenta
	From MATIPOCAMBIO
	where (Fecha between @FechaInicio and  @FechaFin or @FechaInicio='01/01/1900' or @FechaFin='01/01/1900')
	

