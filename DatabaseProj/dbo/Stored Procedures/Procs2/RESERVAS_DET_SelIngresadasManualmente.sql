﻿--JRF-20150427- p.IDTipoProv
--JRF-20151119-.. And NuViaticoTC = 0
CREATE Procedure dbo.RESERVAS_DET_SelIngresadasManualmente
@IDReserva int  
as  
Set NoCount On  
select p.IDTipoProv,rd.*,r.*
from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva 
Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor  
where rd.Anulado=0 and r.Anulado=0 and rd.IDReserva=@IDReserva and rd.FlServicioIngManual=1  And NuViaticoTC = 0
