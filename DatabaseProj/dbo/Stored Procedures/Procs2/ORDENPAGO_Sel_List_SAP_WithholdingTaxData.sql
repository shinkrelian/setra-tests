﻿CREATE PROCEDURE ORDENPAGO_Sel_List_SAP_WithholdingTaxData
@IDOrdPag int
AS
begin
declare @IDTipoProv char(3)
select --@IDProveedor=r.IDProveedor,
@IDTipoProv=p.IDTipoProv
from ORDENPAGO op
inner join RESERVAS r on op.IDReserva=r.IDReserva
inner join MAPROVEEDORES p on p.IDProveedor=r.IDProveedor
where IDOrdPag=@IDOrdPag
select DISTINCT
WTCode= case when @IDTipoProv='004' then 'D026' else 'D037' end,
--isnull((select CoSAP from MATIPODETRACCION where CoTipoDetraccion=d.CoTipoDetraccion),''),
WTAmount=abs(isnull(op.SsDetraccion,0)),
WTAmountFC=0
from ORDENPAGO op
where op.IDOrdPag=@IDOrdPag
and (op.SsDetraccion>0 )
end;
