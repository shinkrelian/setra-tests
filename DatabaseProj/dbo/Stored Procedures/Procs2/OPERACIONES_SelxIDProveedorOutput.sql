﻿--HLF-20130923-Agregando Inner Join RESERVAS r On r.IDReserva=o.IDReserva And r.Anulado=0
CREATE PROCEDURE dbo.OPERACIONES_SelxIDProveedorOutput  
	@IDCab int,  
	@IDProveedor char(6),  
	@pbExists bit Output  
As  
  
	Set NoCount On    
	Set @pbExists=0  
	
	If Exists(Select o.IDProveedor From OPERACIONES o 
		Inner Join RESERVAS r On r.IDReserva=o.IDReserva And r.Anulado=0
		Where o.IDCab=@IDCab And 
		o.IDProveedor=@IDProveedor )  
		
		Set @pbExists=1  
			
