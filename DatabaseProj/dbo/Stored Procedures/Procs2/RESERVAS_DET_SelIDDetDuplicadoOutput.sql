﻿Create Procedure dbo.RESERVAS_DET_SelIDDetDuplicadoOutput
	@IDCab	int,
	@IDProveedor char(6),
	@pIDReserva_Det	int Output
As
	Set Nocount On
	
	Declare @IDDet	int 
	
	Select Top 1 @IDDet=IDDet From 
	(
	SELECT IDDet, Servicio, Dia,IDServicio_Det, COUNT(*) AS Cant
	FROM RESERVAS_DET WHERE IDReserva=(Select IDReserva From RESERVAS 
		Where IDCab=@IDCab And IDProveedor=@IDProveedor)
	GROUP BY IDDet, Servicio,dia,IDServicio_Det
	) as X
	Where Cant>1
	Order by Cant desc

	Set @IDDet=ISNULL(@IDDet,0)
	
	Set @pIDReserva_Det=0
	
	If @IDDet<>0
		Select Top 1 @pIDReserva_Det=IDReserva_Det 
		From RESERVAS_DET WHERE IDReserva=(Select IDReserva From RESERVAS 
			Where IDCab=@IDCab And IDProveedor=@IDProveedor)
		And IDDet=@IDDet Order by IDDet
	
	Set @pIDReserva_Det=isnull(@pIDReserva_Det,0)