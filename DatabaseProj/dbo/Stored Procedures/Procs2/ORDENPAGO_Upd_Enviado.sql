﻿create procedure ORDENPAGO_Upd_Enviado
@IDOrdPag int,
@UserMod char(4)
as
update ordenpago 
set FlEnviado=1,
fecmod=getdate(),
UserMod=@UserMod
where IDOrdPag=@IDOrdPag
