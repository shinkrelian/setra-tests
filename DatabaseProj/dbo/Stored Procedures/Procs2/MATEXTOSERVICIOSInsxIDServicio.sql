﻿Create Procedure dbo.MATEXTOSERVICIOSInsxIDServicio
@IDServicio char(8),
@IDServicioNuevo char(8)
As
Set NoCount On
Insert into MATEXTOSERVICIOS
SELECT @IDServicioNuevo as [IDServicio]
      ,[IDIdioma]
      ,[Titulo]
      ,[Texto]
      ,[UserMod]
      ,[FechaMod]
  FROM [dbo].[MATEXTOSERVICIOS]
Where IDServicio = @IDServicio
