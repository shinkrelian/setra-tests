﻿Create Procedure [dbo].[MASEGUIMCLIENTE_Upd]
	@IDSeguim int,    
    @IDContacto char(3),
    @Notas text,
    @UserMod char(4)
As

	Set NoCount On
		
	UPDATE MASEGUIMCLIENTE SET                      
           IDContacto=@IDContacto,
           Notas=@Notas,
           UserMod=@UserMod,
           FecMod=getdate()
     WHERE IDSeguim=@IDSeguim
