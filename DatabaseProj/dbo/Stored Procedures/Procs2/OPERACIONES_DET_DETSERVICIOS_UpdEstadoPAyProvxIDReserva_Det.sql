﻿CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_UpdEstadoPAyProvxIDReserva_Det
 @IDReserva_Det	int,
 
 @IDProveedor_Prg char(6),    
 @IDTipoProvProg char(3),         
 @UserMod char(4)    
As    
 Set NoCount On    
  
 Update OPERACIONES_DET_DETSERVICIOS Set    
 IDEstadoSolicitud='PA',    
 IDServicio_Det_V_Prg=IDServicio_Det_V_Cot,   
 IDProveedor_Prg=@IDProveedor_Prg,  
 Total_Prg=Total,    
 UserMod=@UserMod,     
 FecMod=GETDATE()    
 From OPERACIONES_DET_DETSERVICIOS od     
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det_V_Cot=sd.IDServicio_Det    
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio    
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor    
 Where IDOperacion_Det IN (SELECT IDOperacion_Det FROM OPERACIONES_DET  
 WHERE IDReserva_Det IN (@IDReserva_Det))   
 And p.IDTipoProv=@IDTipoProvProg  

