﻿Create Procedure [dbo].[RESERVAS_DET_Sel_ExisteOutput]
	@IDReserva int,
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDReserva_Det From RESERVAS_DET
		WHERE IDReserva=@IDReserva)
		Set @pExiste=1
	Else
		Set @pExiste=0
