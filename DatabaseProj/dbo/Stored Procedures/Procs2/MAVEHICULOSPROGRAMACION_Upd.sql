﻿
create procedure dbo.MAVEHICULOSPROGRAMACION_Upd
@NuVehiculoProg smallint,
@NoUnidadProg varchar(50),
@QtCapacidadPax tinyint,
@UserMod char(4)
As
	Set NoCount On
	Update MAVEHICULOSPROGRAMACION
		set NoUnidadProg = @NoUnidadProg,
			QtCapacidadPax = @QtCapacidadPax,
			UserMod = @UserMod,
			FecMod = GetDate()
	where NuVehiculoProg = @NuVehiculoProg
