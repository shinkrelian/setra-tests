﻿--JHD-20150626- isnull a los campos
CREATE PROCEDURE [dbo].[ORDENCOMPRA_DET_Sel_List_PDF]
@NuOrdComInt int
as
SELECT
		   Row_Number() over (Order By NuOrdComInt_Det) As Item, 
			NuOrdComInt_Det,
			--NuOrdComInt,
			isnull(TxServicio,'') as TxServicio,
			isnull(SSCantidad,0) as SSCantidad,
			isnull(SSPrecUnit,0) as SSPrecUnit,
			isnull(SSTotal,0) as SSTotal
FROM         ORDENCOMPRA_DET
WHERE   (NuOrdComInt = @NuOrdComInt or @NuOrdComInt=0)

