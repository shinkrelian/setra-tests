﻿--HLF-20130418-Agregando And sd.Estado='A'     
--JRF-20130802-Agregando los campos CodTarifa,VerDetaTipo     
--HLF-20140715-Agregando campo CoMoneda
--JRF-20150820-Agregando la columna FlDetracción
CREATE Procedure [dbo].[MASERVICIOS_Det_Ins_Copiar]        
 @IDServicio char(8),          
 @IDServicioNue char(8),        
 @Anio char(4),        
 @Tipo varchar(7),         
 @UserMod char(4)        
As        
 Set NoCount On        
 Declare @IDServicio_Det int = (Select IsNull(Max(IDServicio_Det),0) From MASERVICIOS_DET)        
         
 --Declare @Correlativo tinyint= (Select IsNull(Max(Correlativo),0) From MASERVICIOS_DET         
 -- Where IDServicio=@IDServicioNue and         
 -- (Anio=@Anio Or ltrim(rtrim(@Anio))='') And         
 -- Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')        
         
 Declare @Correlativo tinyint=0        
         
 INSERT INTO MASERVICIOS_DET        
           (IDServicio_Det        
           ,IDServicio                 
           ,IDServicio_Det_V          
           ,Anio        
           ,Tipo        
           ,Correlativo        
           ,DetaTipo        
           ,Codigo        
           ,Descripcion        
           ,Afecto                   
           ,Monto_sgl        
           ,Monto_dbl        
           ,Monto_tri        
           ,cod_x_triple        
           ,IdHabitTriple        
           ,Monto_sgls        
           ,Monto_dbls        
           ,Monto_tris        
           ,TC        
           ,PlanAlimenticio        
           ,ConAlojamiento        
           ,DiferSS        
           ,DiferST        
                   
           ,TipoGasto         
           ,TipoDesayuno        
                   
           ,Desayuno        
     ,Lonche        
     ,Almuerzo        
     ,Cena                   
                   
           ,PoliticaLiberado        
           ,Liberado        
           ,TipoLib        
           ,MaximoLiberado        
           ,LiberadoM        
           ,MontoL        
           ,Tarifario        
           ,TituloGrupo        
           ,idTipoOC        
           ,CtaContable        
           ,CtaContableC     
           ,CodTarifa  
           ,VerDetaTipo              
           ,IDServicio_DetCopia        
           ,CoMoneda  
		   ,CoCeCos
		   ,FlDetraccion
           ,UserMod)        
         
 Select        
   @IDServicio_Det + ((row_number() over (order by IDServicio_Det) ))        
           ,@IDServicioNue          
           ,IDServicio_Det_V                 
           ,Anio        
           ,Tipo        
           ,@Correlativo + ((row_number() over (order by Correlativo) ))        
           ,DetaTipo        
           ,Codigo        
           ,Descripcion        
           ,Afecto                   
           ,Monto_sgl        
           ,Monto_dbl        
           ,Monto_tri        
           ,cod_x_triple        
           ,IdHabitTriple        
           ,Monto_sgls        
           ,Monto_dbls        
           ,Monto_tris        
           ,TC        
           ,PlanAlimenticio        
           ,ConAlojamiento        
           ,DiferSS        
           ,DiferST        
        
           ,TipoGasto         
           ,TipoDesayuno        
                   
           ,Desayuno        
     ,Lonche        
     ,Almuerzo        
     ,Cena                
                   
           ,PoliticaLiberado        
           ,Liberado        
           ,TipoLib        
           ,MaximoLiberado        
           ,LiberadoM        
           ,MontoL        
           ,Tarifario        
           ,TituloGrupo        
           ,idTipoOC        
           ,CtaContable        
           ,CtaContableC     
           ,CodTarifa  
           ,VerDetaTipo        
           ,IDServicio_Det        
           ,CoMoneda
		   ,CoCeCos
		   ,FlDetraccion
           ,@UserMod        
 From MASERVICIOS_DET Where IDServicio=@IDServicio And         
   (Anio=@Anio Or ltrim(rtrim(@Anio))='') And         
  (Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')        
  And Estado='A'   
