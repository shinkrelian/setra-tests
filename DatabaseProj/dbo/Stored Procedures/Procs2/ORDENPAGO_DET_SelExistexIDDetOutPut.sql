﻿

Create Procedure dbo.ORDENPAGO_DET_SelExistexIDDetOutPut
	@IDDet int,
	@pExiste bit Output
As
	Set Nocount On
	
	Set @pExiste=0
	
	If Exists(Select IDOrdPag_Det From ORDENPAGO_DET Where IDReserva_Det In 
		(Select IDReserva_Det From RESERVAS_DET Where IDDet=@IDDet))
		
		Set @pExiste=1
		
