﻿--JHD-20150918-,U_SYP_STATUS= 'I'
CREATE PROCEDURE [dbo].[ORDENPAGO_List_SAP_PUT]
@IDOrdPag INT
as
select
U_SYP_MDTD='OP'--isnull(td.CoSunat,'')
,U_SYP_MDSD='000'--isnull(d.NuSerie,'')
,U_SYP_MDCD=isnull(O.IDOrdPag,'')
,U_SYP_STATUS= 'I'
,U_SYP_TCOMPRA='ER'
from ORDENPAGO o
where o.IDOrdPag=@IDOrdPag
