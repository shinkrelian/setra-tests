﻿

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE procedure NOTAVENTA_COUNTER_DETALLE_Upd
		   @NuNtaVta char(6),
		   @NuItem tinyint,
		   @NuDetalle tinyint,
           @NuDocumento char(20),
           
           --@CoMoneda char(3)='',
           --@SSTipoCambio numeric(8,2)=0,
           @CoSegmento char(1)='',
           @SSCosto numeric(10,2),
           @SSComision numeric(10,2),
           @SSValor numeric(10,2),
           @SSFEE_Emision numeric(10,2),
           @SSVenta numeric(10,2),
           @SSUtilidad numeric(10,2),
           @UserMod char(4)
as
BEGIN
Set NoCount On
	
update [NOTAVENTA_COUNTER_DETALLE]
SET 
         
           NuDocumento=@NuDocumento
           
          
           --,CoMoneda = @CoMoneda
           --,SSTipoCambio =@SSTipoCambio
           ,CoSegmento=@CoSegmento
           ,SSCosto=@SSCosto
           ,SSComision=@SSComision
           ,SSValor=@SSValor
           ,SSFEE_Emision=@SSFEE_Emision
           ,SSVenta=@SSVenta
           ,SSUtilidad=@SSUtilidad
           ,UserMod=@UserMod
           ,fecmod=GETDATE()
WHERE
			NuNtaVta=@NuNtaVta AND
            NuDetalle=@NuDetalle AND
            NuItem=@NuItem    
END

