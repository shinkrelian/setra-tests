﻿CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_Lvw]
	@IDServicio_Det	int,
	@IDServicio	char(8),
	@Descripcion varchar(100)
As
	Set NoCount On
	
	Select Descripcion	
	From MASERVICIOS_DET
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDServicio_Det <>@IDServicio_Det Or @IDServicio_Det=0)
	And IDServicio=@IDServicio
	Order by Descripcion
