﻿CREATE Procedure dbo.OPERACIONES_DET_TMP_Upd_NuevaTarifa
	@IDCab int,
	@IDProveedor int,
	@UserMod char(4)
As
	Set Nocount On

	Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)        
	
	Update OPERACIONES_DET_TMP Set 
		IDServicio_Det=NuevaTarifa, --dbo.FnNuevaTarifa(y.IDServicio,y.Tipo,y.Anio,y.Dia,y.IDServicio_Det, y.Codigo),
		NetoHab=(y.TotalGen-y.TotImpto)/(NroPax+ISNULL(NroLiberados,0)),
		TotalHab=y.TotalGen/(NroPax+ISNULL(NroLiberados,0)),
		IgvHab=y.TotImpto/(NroPax+ISNULL(NroLiberados,0)),
		NetoGen=y.TotalGen-y.TotImpto,
		IgvGen=y.TotImpto,
		TotalGen=y.TotalGen,
		FecMod=GETDATE(),
		UserMod=@UserMod
	From
		(
		Select  x.IDOperacion_Det,
		x.TotalGen, x.TotImpto, x.NuevaTarifa
		
		From 
		(Select ods.IDOperacion_Det, 
		Sum(dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(ods.IDServicio_Det_V_Cot,od.NroPax+isnull(od.NroLiberados,0) ),
		dCot.CoMoneda,'USD',ods.TipoCambio)*Case when dcot.Afecto=1 then 1+(@NuIgv*0.01)  else 1 end) as TotalGen,
		sum(ods.TotImpto) as TotImpto,
		sd.Tipo, sd.IDServicio, sd.Anio,
		dbo.FnNuevaTarifa(sd.IDServicio,sd.Tipo,sd.Anio,od.Dia,od.IDServicio_Det, sd.Codigo) as NuevaTarifa
		From OPERACIONES_DET_DETSERVICIOS_TMP ODS 
		Left Join MASERVICIOS_DET dCot On ods.IDServicio_Det_V_Cot=dCot.IDServicio_Det                                                
		Inner Join OPERACIONES_DET_TMP od On ods.IDOperacion_Det=od.IDOperacion_Det
		Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det        
		Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab And o.IDProveedor=@IDProveedor
		Group by ods.IDOperacion_Det,sd.Tipo, sd.IDServicio,od.Dia ,sd.Anio,od.IDServicio_Det, sd.Codigo
		) as X
		Inner Join OPERACIONES_DET_TMP od On od.IDOperacion_Det  In (Select IDOperacion_Det From OPERACIONES_DET_TMP od1 
		Inner Join OPERACIONES o1 On od1.IDOperacion=o1.IDOperacion And o1.IDCab=@IDCab And o1.IDProveedor=@IDProveedor)
		  
		) as Y INNER JOIN OPERACIONES_DET_TMP ON OPERACIONES_DET_TMP.IDOperacion_Det = y.IDOperacion_Det
	Where 
	--OPERACIONES_DET_TMP.IDOperacion_Det In (Select IDOperacion_Det From OPERACIONES_DET_TMP od1 
		--Inner Join OPERACIONES o1 On od1.IDOperacion=o1.IDOperacion And o1.IDCab=@IDCab And o1.IDProveedor=@IDProveedor)
	(OPERACIONES_DET_TMP.IDServicio_Det <> y.NuevaTarifa and Y.NuevaTarifa <> 0)
	