﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_List]
	@IDTemporada int,
	@NombreTemporada varchar(80)
AS
BEGIN
	Set NoCount On
	SET @IDTemporada = CASE WHEN ISNULL(@IDTemporada,0) = 0 THEN NULL ELSE @IDTemporada END
	Select IDTemporada, NombreTemporada, TipoTemporada
	From MATEMPORADA
	Where (NombreTemporada Like '%'+@NombreTemporada+'%' Or LTRIM(RTRIM(@NombreTemporada))='')
	and (IDTemporada = COALESCE(@IDTemporada,IDTemporada))
	ORDER BY IDTemporada
END
