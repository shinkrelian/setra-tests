﻿CREATE PROCEDURE [MAPROVEEDORES_MATIPODETRACCION_Ins]
	@IDProveedor char(6),
	@CoTipoDetraccion char(5),
	@UserMod char(4)
AS
BEGIN
	Insert Into [dbo].[MAPROVEEDORES_MATIPODETRACCION]
		(
			[IDProveedor],
 			[CoTipoDetraccion],
 			[UserMod],
 			[FecMod]
 		)
	Values
		(
			@IDProveedor,
 			@CoTipoDetraccion,
 			@UserMod,
 			getdate()
 		)
END
