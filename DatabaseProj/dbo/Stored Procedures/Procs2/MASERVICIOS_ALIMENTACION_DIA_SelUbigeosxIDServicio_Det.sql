﻿

Create Procedure dbo.MASERVICIOS_ALIMENTACION_DIA_SelUbigeosxIDServicio_Det
	@IDServicio_Det	int
As
	Set Nocount On
	
	Select IDUbigeoOri, IDUbigeoDes 
	From MASERVICIOS_ALIMENTACION_DIA 
	Where IDServicio in 
	(Select IDServicio from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det)

