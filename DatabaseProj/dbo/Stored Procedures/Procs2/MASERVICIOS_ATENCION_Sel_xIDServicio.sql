﻿CREATE Procedure dbo.MASERVICIOS_ATENCION_Sel_xIDServicio 
@IDServicio Char(8)
As
	Set NoCount On
	select IDAtencion,Dia1,Dia2,
	 Case When (CONVERT(char(5),Desde1,108) = '00:00' And CONVERT(char(5),Hasta1,108) = '00:00') Then '' Else CONVERT(Char(5),Desde1,108)End Desde1,
	 Case When CONVERT(Char(5),Hasta1,108) = '00:00' Then '' Else CONVERT(Char(5),Hasta1,108)End Hasta1,
	 Case When (CONVERT(Char(5),Desde2,108) = '00:00' And CONVERT(Char(5),Hasta2,108) = '00:00') Then '' Else CONVERT(Char(5),Desde2,108)End Desde2,
	 Case When CONVERT(Char(5),Hasta2,108) = '00:00' Then '' Else CONVERT(Char(5),Hasta2,108)End Hasta2 from MASERVICIOS_ATENCION
	Where IDservicio = @IDServicio
	Order By IDAtencion
