﻿--HLF-20150703-If @pCoMoneda='USD'...	
CREATE Procedure dbo.MASERVICIOS_DET_SelCoMonedaOutput
	@IDServicio_Det int,
	@pCoMoneda char(3) output
As
	Set Nocount On
	Set @pCoMoneda=''
	
	Declare @IDServicio char(8),@Variante varchar(7),@Anio char(4)

	Select @pCoMoneda=CoMoneda,@IDServicio=IDServicio,@Variante=tipo,
			@Anio = Anio
	From MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det

	If @pCoMoneda='USD'
		SELECT Top 1 @pCoMoneda = CoMoneda FROM MASERVICIOS_DET where IDServicio=@IDServicio and tipo=@Variante
			and Anio=@Anio and CoMoneda<>'USD'