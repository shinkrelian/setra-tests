﻿
-----------------------------------------------------------------------------------------------------
--MAUBIGEO_Devolver_Voucher_Oficina 422083
--CREATE
CREATE Procedure dbo.MAUBIGEO_Devolver_Voucher_Oficina
--@idproveedor char(6)='',
@IDVoucher int--=422083
as  
begin
	declare @idproveedor char(6)

	set @idproveedor=(select top 1 p.IDProveedor
						from
						OPERACIONES_DET od                   
						Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion                  
						Left Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor  
						where od.IDVoucher=@IDVoucher
						)

	declare @CoUbigeo_Oficina char(6)

	set @CoUbigeo_Oficina=(select isnull(CoUbigeo_Oficina,'') from VOUCHER_OPERACIONES where IDVoucher=@IDVoucher)

	if @CoUbigeo_Oficina=''
	begin
		set @CoUbigeo_Oficina=(select isnull(CoUbigeo_Oficina,'') from MAPROVEEDORES where idproveedor=@idproveedor)
	end

	SELECT CoUbigeo_Oficina=@CoUbigeo_Oficina
end;
