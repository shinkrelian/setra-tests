﻿Create Procedure [dbo].[MASERVICIOS_DIA_Upd_NuevoIDServicio]
	@IDServicio	char(8),
	@UserMod	char(4),
	@IDServicioNue char(8) 

As
	Set Nocount On
	
	UPDATE MASERVICIOS_DIA           
    SET IDServicio=@IDServicioNue, FechaMod=GETDATE(), UserMod=@UserMod
    
    Where IDServicio=@IDServicio
