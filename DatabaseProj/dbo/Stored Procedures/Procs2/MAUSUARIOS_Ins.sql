﻿--JRF-20130806-Nuevo campo de Ciudad
--JHD-20150701-Nuevos campos: TxApellidos,FecIngreso,CoSexo,CoTipoDoc,NumIdentidad,CoEmpresa,CoCargo,CoUserJefe,NuAnexo,TxLema,NoArchivoFoto
--JHD-20150713-@Direccion varchar(max)
--JHD-20150713-Agregar campo @NuOrdenArea 
--JHD-20150715-Agregar campo @FlTrabajador_Activo 
--JHD-20160218-@FlCompass bit=1,
--JHD-20160218-if @FlCompass =1 ...
--JHD-20160219-@FecCese smalldatetime,
--JRF-20160312-@CoUbicacionOficina varchar(15),...
--JHD-20160331-Agregar campo @Celular_Trabajo
CREATE Procedure [dbo].[MAUSUARIOS_Ins]  
 @Usuario varchar(20),  
 @Nombre varchar(60),  
 @Direccion varchar(max),  
 @Telefono varchar(12),  
 @Celular varchar(12),  
 @Nivel char(3),  
 @Password varchar(max),  
 @Siglas char(3),  
 @FechaNac smalldatetime,   
 @Directorio varchar(50),  
 @Correo varchar(50),  
 @IdArea char(2),  
 @EsVendedor bit,  
 @IDUbigeo char(6),
 @UserMod char(4),  
@TxApellidos varchar(60)='',
@FecIngreso smalldatetime='01/01/1900',
@CoSexo char(1)='',
@CoTipoDoc char(3)='',
@NumIdentidad varchar(15)='',
@CoEmpresa tinyint=0,
@CoCargo tinyint=0,
@CoUserJefe char(4)='',
@NuAnexo varchar(5)='',
@TxLema varchar(255)='',
@NoArchivoFoto varchar(200)='',
@NuOrdenArea tinyint=0,
@FlTrabajador_Activo bit=1,
@FlCompass bit=0,
@FecCese smalldatetime='01/01/1900',
@CoUbicacionOficina varchar(15),
@Celular_Trabajo varchar(12),
@pIDUsuario char(4) Output  
  
As  
  
 Set NoCount On  
 Declare @IDUsuario char(4)--, @vcCorrelativo varchar(10)  
   
   if @FlCompass =1
	  set @NuOrdenArea=isnull((select max(NuOrdenArea) from MAUSUARIOS where IdArea=@IdArea),0)+1

 Exec Correlativo_SelOutput 'MAUSUARIOS',1,4,@IDUsuario output  
 --Set @IDUsuario=Left(@vcCorrelativo,4)  
   
 INSERT INTO MAUSUARIOS  
           ([IDUsuario]  
           ,Usuario  
           ,[Nombre]  
           ,[Direccion]  
           ,[Telefono]  
           ,[Celular]  
           ,[Nivel]  
           ,[Password]  
           ,[Siglas]  
           ,[FechaNac]             
           ,[Directorio]  
           ,[Correo]  
           ,[IdArea]  
           ,EsVendedor  
           ,IDUbigeo
           ,[UserMod]
		   ,TxApellidos,
 			FecIngreso,
 			CoSexo,
 			CoTipoDoc,
 			NumIdentidad,
 			CoEmpresa,
 			CoCargo,
 			CoUserJefe,
 			NuAnexo,
 			TxLema,
 			NoArchivoFoto,
			NuOrdenArea,
			FlTrabajador_Activo
			,FecCese
			,CoUbicacionOficina
			,Celular_Trabajo
		   )             
     VALUES  
           (@IDUsuario  
           ,@Usuario  
           ,@Nombre  
           ,Case When Ltrim(Rtrim(@Direccion))='' Then Null Else @Direccion End  
           ,Case When Ltrim(Rtrim(@Telefono))='' Then Null Else @Telefono End  
           ,Case When Ltrim(Rtrim(@Celular))='' Then Null Else @Celular End  
           ,@Nivel  
           ,@Password  
           ,Case When Ltrim(Rtrim(@Siglas))='' Then Null Else @Siglas End  
           ,Case When @FechaNac='01/01/1900' Then Null Else @FechaNac End  
           ,Case When Ltrim(Rtrim(@Directorio))='' Then Null Else @Directorio End  
           ,@Correo  
           ,Case When Ltrim(Rtrim(@IdArea))='' Then Null Else @IdArea End  
           ,@EsVendedor 
           ,@IDUbigeo 
           ,@UserMod
		   ,Case When Ltrim(Rtrim(@TxApellidos))='' Then Null Else @TxApellidos End  
		   ,Case When @FecIngreso='01/01/1900' Then Null Else @FecIngreso End  
		   ,Case When Ltrim(Rtrim(@CoSexo))='' Then Null Else @CoSexo End  
		   ,Case When Ltrim(Rtrim(@CoTipoDoc))='' Then Null Else @CoTipoDoc End  
		   ,Case When Ltrim(Rtrim(@NumIdentidad))='' Then Null Else @NumIdentidad End  
		   ,Case When @CoEmpresa=0 Then Null Else @CoEmpresa End  
		   ,Case When @CoCargo=0 Then Null Else @CoCargo End  
		   ,Case When Ltrim(Rtrim(@CoUserJefe))='' Then Null Else @CoUserJefe End  
		   ,Case When Ltrim(Rtrim(@NuAnexo))='' Then Null Else @NuAnexo End  
		   ,Case When Ltrim(Rtrim(@TxLema))='' Then Null Else @TxLema End  
		   ,Case When Ltrim(Rtrim(@NoArchivoFoto))='' Then Null Else @NoArchivoFoto End  
		   ,Case When @NuOrdenArea=0 Then Null Else @NuOrdenArea End  --@NuOrdenArea
		   ,@FlTrabajador_Activo
		   ,Case When @FecCese='01/01/1900' Then Null Else @FecCese End   
		   ,Case  When Ltrim(Rtrim(@CoUbicacionOficina)) = '' then null else @CoUbicacionOficina End
		   ,Case When Ltrim(Rtrim(@Celular_Trabajo))='' Then Null Else @Celular_Trabajo End  
		   )  
 Set @pIDUsuario=@IDUsuario  
