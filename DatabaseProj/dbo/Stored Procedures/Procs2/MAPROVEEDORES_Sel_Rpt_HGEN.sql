﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=pr.IDCiudad)=@CoPais )
CREATE procedure [dbo].[MAPROVEEDORES_Sel_Rpt_HGEN]
@IDTipoProv char(3),
@IDCiudad char(6),
@CoPais char(6)=''
as
	Set NoCount On
	
	select tp.Descripcion as TipoProvedor,RazonSocial,Direccion,
	Telefonos=
	Case When Right(isnull(Telefono1,'')+'/'+isnull(Telefono2,''),1)='/' Then 
		isnull(Telefono1,'')
	Else
		Case When Left(isnull(Telefono1,'')+'/'+isnull(Telefono2,''),1)='/' Then 
			isnull(Telefono2,'')
			else
				isnull(Telefono1,'')+'/'+isnull(Telefono2,'')
		End
	End,
	isnull(Celular_Claro,'-') CelularClaro,ISNULL(Celular_Movistar,'-') CelularMovistar
	 ,ISNULL(Celular_Nextel,'-') CelularNextel,
	 Emergencias=
	 Case When Right(isnull(TelefonoReservas1,'')+'/'+isnull(TelefonoReservas2,''),1)='/' Then 
		isnull(TelefonoReservas1,'')
	Else
		Case When Left(isnull(TelefonoReservas1,'')+'/'+isnull(TelefonoReservas2,''),1)='/' Then 
			isnull(TelefonoReservas2,'')
			else
				isnull(TelefonoReservas1,'')+'/'+isnull(TelefonoReservas1,'')
		End
	End,SitioWeb= isnull(Web,'')
	 from MAPROVEEDORES pr Left Join MATIPOPROVEEDOR tp On pr.IDTipoProv=tp.IDTipoProv
	Where (LTRIM(RTRIM(@IDTipoProv))='' Or pr.IDTipoProv=@IDTipoProv)
	 And (LTRIM(RTRIM(@IDCiudad))='' Or IDCiudad=@IDCiudad) 	
	 And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=pr.IDCiudad)=@CoPais ) 
	Order BY 1
