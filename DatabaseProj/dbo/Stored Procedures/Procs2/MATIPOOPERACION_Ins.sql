﻿
------------------------------------------------------------------------------------------------


--JHD-20150216- Se agrego el campo TxDefinicion  
CREATE Procedure [dbo].[MATIPOOPERACION_Ins]   
 @Descripcion varchar(100),  
 @CtaContIGV varchar(8),
 @UserMod char(4),  
 @TxDefinicion varchar(400)='',
 @pIDToc char(3) output  
As  
  
 Set NoCount On  
 Declare @IDToc char(3)   
 Exec Correlativo_SelOutput 'MATIPOOPERACION',1,3,@IDToc output  
   
 INSERT INTO MATIPOOPERACION  
           ([IDToc]  
           ,[Descripcion]
           ,[CtaContIGV]             
           ,[UserMod]
           ,[FecMod]
           ,TxDefinicion)
     VALUES  
           (@IDToc,  
            @Descripcion,
            Case When @CtaContIGV = '' Then Null Else @CtaContIGV End,             
            @UserMod,
            Getdate()
            ,Case When ltrim(rtrim(@TxDefinicion))='' Then Null Else @TxDefinicion End )  
              
 Set @pIDToc=@IDToc              
