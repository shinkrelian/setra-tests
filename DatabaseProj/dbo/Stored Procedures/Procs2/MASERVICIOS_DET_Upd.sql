﻿
--HLF-20120817-Case When Anio      
--JRF-20130320-Nuevo Campo [CodTarifa]      
--JRF-20130717-Nuevo Campo [PoliticaLibNivelDetalle]    
--JRF-20131209-Aumentando el valor de los decimales a (numeric(10, 4))  
--HLF-20140715-Nuevo campo CoMoneda
--JHD-20150603-Nuevo campo CoCeCos
--JHD-20150610-Nuevo campo FlDetraccion
--HLF-@Monto_sgls, @Monto_dbls, @Monto_tris de n(10,4) a n(12,4)
CREATE Procedure [dbo].[MASERVICIOS_DET_Upd]            
 @IDServicio_Det int,            
          
 @IDServicio_Det_V int,            
 @Anio char(4),            
 @Tipo varchar(7),             
          
 @DetaTipo text,            
 @VerDetaTipo bit,            
 @Codigo varchar(20),            
 @Descripcion varchar(200),            
 @Afecto bit,              
 @Monto_sgl numeric(10, 4),            
 @Monto_dbl numeric(10, 4),            
 @Monto_tri numeric(10, 4),            
 @cod_x_triple varchar(20),            
 @IDServicio_DetxTriple int=0,            
 @Monto_sgls numeric(12, 4),            
 @Monto_dbls numeric(12, 4),            
 @Monto_tris numeric(12, 4),            
 @TC numeric(10, 4),            
          
 @ConAlojamiento bit,            
 @PlanAlimenticio bit,            
       
 @CodTarifa varchar(Max),      
       
 @DiferSS numeric(10, 4),            
 @DiferST numeric(10, 4),            
          
 @TipoGasto char(1),            
 @TipoDesayuno char(2),            
          
 @Desayuno bit,            
 @Lonche bit,            
 @Almuerzo bit,            
 @Cena bit,            
          
 @PoliticaLiberado bit,            
 @Liberado tinyint,            
 @TipoLib char(1),            
 @MaximoLiberado tinyint,            
 @LiberadoM tinyint,            
 @MontoL numeric(10, 4),        
 @PoliticaLibNivelDetalle bit,    
         
 @Tarifario bit,            
 @TituloGrupo varchar(30),            
 @idTipoOC char(3),            
 @CtaContable varchar(20),            
 @CtaContableC varchar(20),             
 @IdHabitTriple char(3),          
 --@AnioPreciosCopia char(4),
 @CoMoneda char(3),
 @UserMod char(4),
 @CoCeCos char(6)='',          
 @FlDetraccion bit=0
As            
 Set NoCount On            
          
 UPDATE MASERVICIOS_DET SET                       
 --Anio=@Anio            
 Anio=        
 Case When IDServicio_Det_V=@IDServicio_Det Then           
  Anio         
 Else          
  @Anio            
 End           
         
 --,Tipo=ltrim(rtrim(@Tipo))            
 ,Tipo=          
 Case When IDServicio_Det_V=@IDServicio_Det Then           
  Tipo          
 Else          
  ltrim(rtrim(@Tipo))            
 End          
           
 --,IDServicio_Det_V=Case When @IDServicio_Det_V=0 Then Null Else @IDServicio_Det_V End          
 ,IDServicio_Det_V=          
 Case When IDServicio_Det_V=@IDServicio_Det Then           
  IDServicio_Det_V          
 Else          
  Case When @IDServicio_Det_V=0 Then Null Else @IDServicio_Det_V End          
 End          
           
 --,DetaTipo=Case When ltrim(rtrim(Cast (@DetaTipo as varchar(max))))='' Then Null Else @DetaTipo End            
 ,DetaTipo=          
 Case When IDServicio_Det_V=@IDServicio_Det Then           
  DetaTipo          
 Else          
  Case When ltrim(rtrim(Cast (@DetaTipo as varchar(max))))='' Then Null Else @DetaTipo End            
 End          
           
 ,VerDetaTipo=@VerDetaTipo            
 ,Codigo=@Codigo            
 ,Descripcion=@Descripcion            
 ,Afecto=@Afecto            
 --,IDMoneda=@IDMoneda            
 ,Monto_sgl=Case When @Monto_sgl=0 Then Null Else @Monto_sgl End            
 ,Monto_dbl=Case When @Monto_dbl=0 Then Null Else @Monto_dbl End            
 ,Monto_tri=Case When @Monto_tri=0 Then Null Else @Monto_tri End            
 ,cod_x_triple=Case When ltrim(rtrim(@cod_x_triple))='' Then Null Else @cod_x_triple End            
 ,IDServicio_DetxTriple=Case When @IDServicio_DetxTriple=0 Then Null Else @IDServicio_DetxTriple End            
 ,Monto_sgls=Case When @Monto_sgls=0 Then Null Else @Monto_sgls End            
 ,Monto_dbls=Case When @Monto_dbls=0 Then Null Else @Monto_dbls End            
 ,Monto_tris=Case When @Monto_tris=0 Then Null Else @Monto_tris End            
 ,TC=Case When @TC=0 Then Null Else @TC End            
            
 ,ConAlojamiento = @ConAlojamiento            
 ,PlanAlimenticio=@PlanAlimenticio            
       
,CodTarifa = Case When Ltrim(Rtrim(@CodTarifa))='' Then Null Else @CodTarifa End      
 ,DiferSS = Case When @DiferSS=0 Then Null Else @DiferSS End            
 ,DiferST = Case When @DiferST=0 Then Null Else @DiferST End            
            
 ,TipoGasto=Case When ltrim(rtrim(@TipoGasto))='' Then Null Else @TipoGasto End            
 ,TipoDesayuno=@TipoDesayuno            
            
 ,Desayuno=@Desayuno            
 ,Lonche=@Lonche            
 ,Almuerzo=@Almuerzo            
 ,Cena=@Cena            
            
 ,PoliticaLiberado=@PoliticaLiberado            
 ,Liberado=Case When @Liberado=0 Then Null Else @Liberado End            
 ,TipoLib=Case When @TipoLib='' Then Null Else @TipoLib End            
 ,MaximoLiberado=Case When @MaximoLiberado=0 Then Null Else @MaximoLiberado End            
 ,LiberadoM=Case When @LiberadoM=0 Then Null Else @LiberadoM End            
 ,MontoL=Case When @MontoL=0 Then Null Else @MontoL End            
 ,PoliticaLibNivelDetalle = @PoliticaLibNivelDetalle    
     
 ,Tarifario=@Tarifario            
 ,TituloGrupo=Case When ltrim(rtrim(@TituloGrupo))='' Then Null Else @TituloGrupo End         
 ,idTipoOC=@idTipoOC            
 ,CtaContable=Case When ltrim(rtrim(@CtaContable))='' Then '000000' Else @CtaContable End            
 ,CtaContableC=Case When ltrim(rtrim(@CtaContableC))='' Then Null Else @CtaContableC End                       
 ,IdHabitTriple= @IdHabitTriple          
 --,AnioPreciosCopia=Case When ltrim(rtrim(@AnioPreciosCopia))='' Then Null Else @AnioPreciosCopia End          
 ,CoMoneda=@CoMoneda
 ,UserMod=@UserMod                       
 ,FecMod=GETDATE()          
 ,CoCeCos=Case When ltrim(rtrim(@CoCeCos))='' Then Null Else @CoCeCos End  
 ,FlDetraccion=@FlDetraccion
 WHERE            
 IDServicio_Det=@IDServicio_Det            
 Or IDServicio_Det_V=@IDServicio_Det

