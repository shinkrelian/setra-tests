﻿
-------------------------------------------------------------------------------------------

---MAUSUARIOS_Sel_CboxOpcion_Activos 'mnuIngCotizacion',0,'' --mnuIngOperacionesControlporFile
CREATE Procedure dbo.MAUSUARIOS_Sel_CboxOpcion_Activos
@DescOpcion varchar(50),  
@bTodos bit,
@Activo CHAR(1)
As  
 Set NoCount On  
 
 select IDUsuario,Usuario,Activo from (  
 select '' As IDUsuario,'<TODOS>' As Usuario,'A' as Activo,0 As Ord  
 union  
 select distinct u.IDUsuario,u.Usuario,u.Activo,1 As Ord  
 from MAUSUARIOSOPC uo Left Join MAUSUARIOS u On uo.IDUsuario = u.IDUsuario  
 Left Join MAOPCIONES o On uo.IDOpc=o.IDOpc  
 Where uo.Consultar = 1 --and u.Activo ='A'   
 and   (Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') 
 and LTRIM(rtrim(o.Nombre))=LTRIM(rtrim(@DescOpcion))  
 and IdArea <> 'SI'  
 Union
 Select IDUsuario,Usuario,Activo,1 as Ord from MAUSUARIOS   
 Where (Activo='I' and IdArea = 'UN') 
 ) As X  
 Where (@bTodos = 1 Or X.Ord <> 0)  
 and   (Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') 
 order by X.Usuario --X.IDUsuario  



