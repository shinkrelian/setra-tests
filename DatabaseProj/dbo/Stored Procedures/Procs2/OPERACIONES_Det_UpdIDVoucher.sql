﻿

--Execute [OPERACIONES_DET_SelxVoucher] '14'
--Go

--declare @Example bit
--Execute [OPERACIONES_DET_SelExisteVoucherOutput] '14',@Example Output
--select @Example



--Sp_helptext OPERACIONES_Det_UpdIDVoucher
--Go

CREATE Procedure dbo.OPERACIONES_Det_UpdIDVoucher   
 @IDVoucherNue int,
 @IDOperacion_Det int,  
 @UserMod char(4)  
As  
 Set NoCount On  
   
 UPDATE OPERACIONES_DET   
 Set IDVoucher=@IDVoucherNue, UserMod=@UserMod, FecMod=GETDATE()   
 Where IDOperacion_Det=@IDOperacion_Det
