﻿
--HLF-20130116-@IDNotaVenta a char(8)
CREATE Procedure dbo.NOTA_VENTA_DET_Ins
	@IDNotaVenta char(8),           
    @IDReserva_Det int,
    @Descripcion varchar(max),
    @SubTotal numeric(8,2),
    @TotalIGV numeric(8,2),
    @Total numeric(8,2),
    @UserMod char(4)
As
	Set NoCount On
	
	Declare @IDNotaVentaDet tinyint=Isnull((Select max(IDNotaVentaDet) From NOTA_VENTA_DET 
		Where IDNotaVenta=@IDNotaVenta),0)+1
	
	INSERT INTO NOTA_VENTA_DET
           ([IDNotaVenta]
           ,[IDNotaVentaDet]
           ,[IDReserva_Det]
           ,[Descripcion]
           ,[SubTotal]
           ,[TotalIGV]
           ,[Total]
           ,[UserMod]
           )
     VALUES
           (@IDNotaVenta
           ,@IDNotaVentaDet
           ,@IDReserva_Det
           ,@Descripcion
           ,@SubTotal
           ,@TotalIGV
           ,@Total
           ,@UserMod
           )

