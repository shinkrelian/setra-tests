﻿CREATE Procedure dbo.MASERVICIOS_DIA_Sel_xIDServicioDia  
 @IDServicio char(8),  
 @Dia tinyint  
As  
 Set NoCount On  
  
 Select Dia,IDIdioma,Descripcion,UserMod  
 From MASERVICIOS_DIA   
 Where IDServicio=@IDServicio And Dia=@Dia  
 Order by Dia  
