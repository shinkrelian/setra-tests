﻿--HLF-20140813-Or psd.CoPrvPrg=@IDProveedorGuia    
--Left Join MAPROVEEDORES pGuia On psd.CoPrvPrg = pGuia.IDProveedor    
--JRF-20150309-Considerar el activo de Operaciones  
--JRF-20150410-Considerar la relación con los Pax
--JRF-20150604-Considerar la nueva asignación [when 'STK' Then 'STOCK']
--HLF-20150626-and isnull(odd.Activo,1)=1
--HLF-20150902-Comentando parametros y agregando @NuPreSob, quitando Union
--HLF-20160126-Left Join MAUSUARIOS uGuia On Isnull(psd.CoPrvPrg,'0000') = uGuia.IDUsuario   
--isnull(ufin.Nombre+' '+ufin.TxApellidos,ufin.Nombre)  as EjecutivoFinanzas, 
 --IsNull(pGuia.NombreCorto,isnull(uGuia.Nombre+' '+uGuia.TxApellidos,uGuia.Nombre)) as NombreGuia,
--isnull(usOper.Nombre+' '+usOper.TxApellidos,usOper.Nombre)  as EjecutivoOperaciones,  
--HLF-20160209-isnull(psd.CoPrvPrg,ps.CoPrvGui)		Isnull(psd.CoPrvPrg,isnull(ps.CoPrvGui,'0000'))
CREATE Procedure [dbo].[PRESUPUESTO_SOBRE_sel_ListRpt]     
--@IDCab int,    
--@IDProveedor char(6),    
--@IDProveedorGuia char(6),
--@IDPax int
	@NuPreSob int
As    
Set NoCount On    
Select c.IDFile,
--usOper.Nombre as EjecutivoOperaciones,
isnull(usOper.Nombre+' '+usOper.TxApellidos,usOper.Nombre)  as EjecutivoOperaciones,  
ps.FePreSob,p.NombreCorto as NombreProveedor,    
 --IsNull(pGuia.NombreCorto,cp.Titulo+' '+cp.Apellidos+' '+cp.Nombres) as NombreGuia,
 IsNull(pGuia.NombreCorto,isnull(uGuia.Nombre+' '+uGuia.TxApellidos,uGuia.Nombre)) as NombreGuia,
	--ufin.Nombre as EjecutivoFinanzas,   
	isnull(ufin.Nombre+' '+ufin.TxApellidos,ufin.Nombre)  as EjecutivoFinanzas,    
 Case CoEstado    
  when 'NV' then 'NUEVO'    
  when 'EL' then 'ELABORADO'    
  when 'EN' then 'ENTREGADO'    
  when 'EJ' then 'EJECUTADO'    
  when 'AN' then 'ANULADO'
 End as DesEstado,    
 psd.FeDetPSo,psd.TxServicio,psd.QtPax,psd.SsPreUni,psd.SsTotal,psd.SsTotSus,psd.SsTotDev,    
 Case CoPago    
  when 'EFE' then 'EFECTIVO'    
  when 'PCM' Then 'PRECOMPRA'    
  when 'TCK' Then 'TICKET'   
  when 'STK' Then 'STOCK' 
 End as DescPago,
 Case WheN CoPago = 'STK' Then '' Else mo.Simbolo End as Moneda
from PRESUPUESTO_SOBRE_DET psd Left Join PRESUPUESTO_SOBRE ps On psd.NuPreSob=ps.NuPreSob    
Left Join COTICAB c On ps.IDCab = c.IDCAB     
Left Join MAUSUARIOS usOper On c.IDUsuarioOpe = usOper.IDUsuario    
Left Join MAPROVEEDORES p On ps.IDProveedor = p.IDProveedor    
--Left Join MAPROVEEDORES pGuia On ps.CoPrvGui = pGuia.IDProveedor   
--Left Join MAPROVEEDORES pGuia On psd.CoPrvPrg = pGuia.IDProveedor    
Left Join MAPROVEEDORES pGuia On isnull(psd.CoPrvPrg,ps.CoPrvGui) = pGuia.IDProveedor    
--Left Join MAUSUARIOS uGuia On Isnull(psd.CoPrvPrg,'0000') = uGuia.IDUsuario    
Left Join MAUSUARIOS uGuia On Isnull(psd.CoPrvPrg,isnull(ps.CoPrvGui,'0000')) = uGuia.IDUsuario    
Left Join MAUSUARIOS ufin On Isnull(ps.CoEjeFin,'0000') = ufin.IDUsuario    
	
Left Join OPERACIONES_DET_DETSERVICIOS odd On psd.IDOperacion_Det=odd.IDOperacion_Det 
and psd.IDServicio_Det=IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)
Left Join MAMONEDAS mo On psd.IDMoneda=mo.IDMoneda
Left Join COTIPAX cp On psd.IDPax=cp.IDPax
where 
--ps.IDCab = @IDCab and ps.IDProveedor = @IDProveedor    
--and IsNull(psd.CoPrvPrg,'')=Ltrim(Rtrim(@IDProveedorGuia)) and 
--IsNull(psd.IDPax,0)=@IDPax  and
ps.NuPreSob=@NuPreSob
and isnull(odd.Activo,1)=1
--Union
--Select c.IDFile,usOper.Nombre as EjecutivoOperaciones,ps.FePreSob,p.NombreCorto NombreProveedor,    
-- IsNull(pGuia.NombreCorto,cp.Titulo+' '+cp.Apellidos+' '+cp.Nombres) as NombreGuia,ufin.Nombre as EjecutivoFinanzas,    
-- Case CoEstado    
--  when 'NV' then 'NUEVO'    
--  when 'EL' then 'ELABORADO'    
--  when 'EN' then 'ENTREGADO'    
--  when 'EJ' then 'EJECUTADO'    
--  when 'AN' then 'ANULADO'
-- End as DesEstado,    
-- psd.FeDetPSo,psd.TxServicio,psd.QtPax,psd.SsPreUni,psd.SsTotal,psd.SsTotSus,psd.SsTotDev,    
-- Case CoPago    
--  when 'EFE' then 'EFECTIVO'    
--  when 'PCM' Then 'PRECOMPRA'    
--  when 'STK' Then 'STOCK'
--  when 'TCK' Then 'TICKET'    
-- End as DescPago,
-- Case WheN CoPago = 'STK' Then '' Else mo.Simbolo End as Moneda
--from PRESUPUESTO_SOBRE_DET psd Left Join PRESUPUESTO_SOBRE ps On psd.NuPreSob=ps.NuPreSob    
--Left Join COTICAB c On ps.IDCab = c.IDCAB     
--Left Join MAUSUARIOS usOper On c.IDUsuarioOpe = usOper.IDUsuario    
--Left Join MAPROVEEDORES p On ps.IDProveedor = p.IDProveedor    
----Left Join MAPROVEEDORES pGuia On ps.CoPrvGui = pGuia.IDProveedor   
--Left Join MAPROVEEDORES pGuia On psd.CoPrvPrg = pGuia.IDProveedor    
--Left Join MAUSUARIOS ufin On Isnull(ps.CoEjeFin,'0000') = ufin.IDUsuario    
----Left Join OPERACIONES_DET_DETSERVICIOS odd On psd.IDOperacion_Det=odd.IDOperacion_Det 
----and psd.IDServicio_Det=IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)
--Left Join MAMONEDAS mo On psd.IDMoneda=mo.IDMoneda
--Left Join COTIPAX cp On psd.IDPax=cp.IDPax
--Left Join MASERVICIOS_DET sd On psd.IDServicio_Det=sd.IDServicio_Det
--where 
----ps.IDCab = @IDCab and ps.IDProveedor = @IDProveedor    
----and IsNull(psd.CoPrvPrg,'')=Ltrim(Rtrim(@IDProveedorGuia)) and IsNull(psd.IDPax,0)=@IDPax  --and odd.Activo=1
----and sd.IDServicio='CUZ00179'
--ps.NuPreSob = @NuPreSob


