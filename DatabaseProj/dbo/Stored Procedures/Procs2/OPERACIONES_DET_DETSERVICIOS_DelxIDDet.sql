﻿
Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_DelxIDDet
	@IDDet	int
As
	Set Nocount On
	
	DELETE FROM OPERACIONES_DET_DETSERVICIOS WHERE IDOperacion_Det In (select IDOperacion_Det FROM OPERACIONES_DET 
	WHERE IDOperacion_Det in 
		(Select IDOperacion_Det FROM OPERACIONES_DET WHERE IDReserva_Det In 
			(Select IDReserva_Det FROM RESERVAS_DET Where IDDet=@IDDet)))

