﻿
Create Procedure dbo.OPERACIONES_Det_SelGastosOperat
	@IDCab	int,
	@IDUbigeoLima char(6),
	@IDUbigeoCusco char(6),
	@IDUbigeo	char(6)	
As
	Set Nocount On
	
	SELECT X.* FROM 
	(
	Select 
	Case When s.IDubigeo=@IDUbigeoCusco Then s.IDubigeo Else @IDUbigeoLima End as IDubigeoLimaCusco,
	sd.IDServicio_Det, od.Servicio, od.Total, sd.TC
		
	From OPERACIONES_DET od 
	Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab
	Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det And sd.TipoGasto='O'
	Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
	) AS X
	WHERE X.IDubigeoLimaCusco=@IDUbigeo
	
