﻿
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--JHD-20141230-Agregar Nuevos campos: CoTipoHab_Oper y TipoHab_Oper
CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_HABITACIONES_Sel_Pk]
	@CoServicio char(8),
	@NuHabitacion tinyint
AS
BEGIN
	Select
		CoServicio,
 		NuHabitacion,
 		CoTipoHab,
 		QtCantidad,
 		QtCantMatri,
 		CoTipCama_Matri,
 		QtCantTwin,
 		CoTipCama_Twin,
 		CoTipCama_Adic,
 		QtCantTriple,
 		CoTipCama_Triple
 		 ,CoTipoHab_Oper=ISNULL(CoTipoHab_Oper,'')
 		
 	From dbo.MASERVICIOS_HOTEL_HABITACIONES
	Where 
		(CoServicio = @CoServicio) And 
		(NuHabitacion = @NuHabitacion)
END;
