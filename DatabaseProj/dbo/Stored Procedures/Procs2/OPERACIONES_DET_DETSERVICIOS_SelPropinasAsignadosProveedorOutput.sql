﻿Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelPropinasAsignadosProveedorOutput
	@IDCab int,
	@IDProveedor char(6),
	@IDProveedor_Prg char(6),
	@pExisten bit Output
As
	Set Nocount On
	
	Set @pExisten=0
	
	If Exists(Select DISTINCT ods.* 
	From OPERACIONES_DET_DETSERVICIOS ods 
	Inner Join OPERACIONES_DET od On 
	ods.IDServicio=od.IDServicio And ods.IDOperacion_Det=od.IDOperacion_Det
	Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab
		And o.IDProveedor=@IDProveedor
	Inner Join MASERVICIOS s On ods.IDServicio=s.IDServicio And s.CoPago in('EFE','TCK')
	--And ods.IDServicio_Det_V_Cot is null
	And s.Descripcion Like 'Tips%'
	Where ods.IDProveedor_Prg = @IDProveedor_Prg)
		Set @pExisten=1

