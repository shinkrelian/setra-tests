﻿--HLF-20130201-Agregando campos IDTipoOC, CantidadAPagar, NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen              
--HLF-20130822-Agregando uso de funcion FnServicioHotelDiaAnt y FnServicioHotelDia            
--HLF-20130828-Agregando uso de funcion FnServicioRestaurantDia            
--Condicionando a que se ejecuten las funciones cuando no sea transfer ni IDTipoServ NAP            
 --HLF-20130905-Agregando Case When ... as VerBiblia          
--HLF-20130923-Agregando And Anulado=0        
--HLF-20131107-Agregando And Estado<>'XL'       
--HLF-20131121-Agregando Inner Join RESERVAS r1 On op1.IDReserva=r1.IDReserva and r1.Anulado=0    
--HLF-20131121-Agregando And d.Anulado=0    
--JRF-20150222-Agregando Nuevos Campos NoShow      
CREATE Procedure dbo.OPERACIONES_DET_InsxIDProveedor                
 @IDCab int,                 
 @IDProveedor char(6),                
 @UserMod char(4)                
As                
 Set Nocount On                
                 
 Declare @IDOperacion_Det int                
 Execute dbo.Correlativo_SelOutput 'OPERACIONES_DET',1,10,@IDOperacion_Det output                
                 
 Insert Into OPERACIONES_DET                
 (IDOperacion_Det, IDOperacion, IDReserva_Det, IDVoucher,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,                
 IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,                
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,                
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,                
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,IDTipoOC,UserMod,              
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,            
 ObservBiblia,VerObserBiblia,FlServicioNoShow,IDMoneda,FlServicioTC)                 
 Select                 
 ((row_number() over (order by IDReserva,IDReserva_Det))-1) + @IDOperacion_Det,                 
 (Select IDOperacion From OPERACIONES op1 Inner Join RESERVAS r1 On op1.IDReserva=r1.IDReserva and r1.Anulado=0    
 Where op1.IDCab=@IDCab And op1.IDReserva=d.IDReserva),                
 IDReserva_Det, 0,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,                
 IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,                
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,                
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,                
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,'NAP',@UserMod,              
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,            
 Case When @IDProveedor='000544'             
  And Not Exists(Select IDServicio From MASERVICIOS Where IDServicio=d.IDServicio             
  And Transfer=1)             
 And Not Exists(Select IDServicio,IDTipoServ From MASERVICIOS Where IDServicio=d.IDServicio             
  And IDTipoServ='NAP')               
  Then             
 dbo.FnServicioHotelDiaAnt(@IDCab,@IDProveedor,d.IDReserva_Det)  + CHAR(13)+CHAR(10)+             
 dbo.FnServicioHotelDia(@IDCab,@IDProveedor,d.IDReserva_Det)    + CHAR(13)+CHAR(10)+             
 dbo.FnServicioRestaurantDia(@IDCab,@IDProveedor,d.IDReserva_Det)            
 End,          
  Case When           
 (Select p1.IDTipoProv From MASERVICIOS_DET sd1 Inner Join MASERVICIOS s1 On s1.IDServicio=sd1.IDServicio          
  Inner Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor          
  Where sd1.IDServicio_Det=d.IDServicio_Det) In ('001','002')  Then 0 Else 1 End as VerBiblia ,FlServicioNoShow ,IDMoneda,d.FlServicioTC  
 From RESERVAS_DET d Where IDReserva In (Select IDReserva From RESERVAS         
 Where IDCab=@IDCab And IDProveedor=@IDProveedor        
 And Anulado=0 AND Estado<>'XL' ) And d.Anulado=0               
                 
 Declare @iRowsAff smallint=@@ROWCOUNT - 1                
 If @iRowsAff>0 Exec Correlativo_Upd 'OPERACIONES_DET',@iRowsAff                
