﻿

----------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE procedure NOTAVENTA_COUNTER_DETALLE_Del_xNuItem
		   @NuNtaVta char(6),
		   @NuItem tinyint
as
BEGIN
Set NoCount On
	
delete from [NOTAVENTA_COUNTER_DETALLE]

WHERE
			NuNtaVta=@NuNtaVta and 
			NuItem=@NuItem
END;
