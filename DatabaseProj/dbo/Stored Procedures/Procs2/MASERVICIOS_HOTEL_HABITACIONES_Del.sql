﻿--create
CREATE PROCEDURE [MASERVICIOS_HOTEL_HABITACIONES_Del]
	@CoServicio char(8),
	@NuHabitacion tinyint
AS
BEGIN
	Delete [dbo].[MASERVICIOS_HOTEL_HABITACIONES]
	Where 
		[CoServicio] = @CoServicio And 
		[NuHabitacion] = @NuHabitacion
END
