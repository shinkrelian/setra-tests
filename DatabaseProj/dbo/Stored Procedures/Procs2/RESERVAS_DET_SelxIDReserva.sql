﻿--HL-20121204-Agregando d.NroLiberados                                
--HL-20121219-Agregando cd.IncGuia                                
--JR-20130206-Agregando si existe Diferencia entre los Pax COTICAB y RESERVAS_DET.                              
--HLF-20130403-Agregando condiciones para campo Servicio                          
--JFR-20130502-Deshacer cambio para la condicion de servicios.                        
--JRF-20130504-Colocar el campo Hora, Tipo                      
--JRF-20130507-Convertir a varchar los valores de Numero [NroPax,NroLiberados].                    
--JRF-20130521-Evaluar si Es gasto de Transferencia                
--JRF-20130529-Agregar el Campo de Desayuno[TipoDesayuno]              
--JRF-20130702-Agregar el campo Dia para el orden.            
--JRF-20130726-Validar que no se encuentre Diferencias con Guía.          
--HLF-20130917-Agregando and d.Anulado=0      
--HLF-20141117-Agregando   Case When sd.VerDetaTipo=1 Then ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'')...  
--JRF-20150220-No se deberia considerar los NoShow [Not(d.FlServicioNoShow=1 and d.FlServicioIngManual=1)]  
--JRF-20150303-Concatenar las filas de Pax.
--JRF-20160216--[--case when s.IDTipoServ = 'NAP' then '---' else s.IDTipoServ End As Tipo,] Deshacer cuando la columna este agregada en la plantilla
--JHD-20160216- case when s.IDTipoServ = 'NAP' then '---' else s.IDTipoServ End As Tipo,] 
--JHD-20160216- --ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'')  As Tipo,
--JRF-20160328- ...Replace(d.Servicio,ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),''),'')  As Servicio,
CREATE Procedure [dbo].[RESERVAS_DET_SelxIDReserva]                                
      @IDReserva int,                                
      @IDEmail    varchar(255),                
      @GastTransfer bit                                
as                                    
	  Set NoCount On                                    
      select * from (                          
      Select  d.Dia,                               
      ltrim(rtrim(convert(varchar,d.Dia,103))) as FechaIn,                                 
      CONVERT(char(5),d.Dia,108) As Hora,                      
      ltrim(rtrim(convert(varchar,d.FechaOut,103))) as FechaOut,                                 
      d.Cantidad As Cantidad,                                 
      (Select Servicio From RESERVAS_DET Where IDReserva=@IDReserva And IDEmailRef=@IDEmail) as ServicioCambiado,                                     
      --d.Servicio ,
	  
	  Replace(d.Servicio,ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),''),'')  As Servicio,
      case when s.IDTipoServ = 'NAP' then '---' else s.IDTipoServ End As Tipo,
      --ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'')  As Tipo,

      IsNull(d.Noches,0) as Noches,                    
                                       
      cast(d.NroPax + IsNull(d.NroLiberados,0)  as varchar)                     
      + Case When cd.IncGuia =1 then '(Guía)' else '' End                      
                           
      As CantidadPax,                                
      IsNull(r.CodReservaProv,'') as CodReservaProv  ,                              
      case when d.NroPax <> c.NroPax and cd.IncGuia = 0  then 1 else 0 End As ExisteDifPax,                              
      d.IDReserva_Det,                      
      Id.Siglas As Idio,                
      Case When CHARINDEX('GASTOS DE TRANSFERENCIA',upper(d.servicio)) > 0 then 1 else 0 End as EvalSelecc                    
      ,case when Dey.Descripcion='NO APLICA' Then '' Else Dey.Descripcion End as Desayuno,        
      --Case When sd.VerDetaTipo=1 Then ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'') Else ''End 
	  ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'') as CodTarifa,
	  case when d.NroPax <> c.NroPax and cd.IncGuia = 0  
	  then dbo.FnDevuelveCadenaPaxxReservaDet(d.IDReserva_Det) else '' End as NombrePaxsCompletos
                    
      From RESERVAS_DET d Left Join RESERVAS r On d.IDReserva=r.IDReserva                                
      Left Join COTIDET cd On d.IDDet=cd.IDDET                                
      Left Join COTICAB c On R.IDCab = c.IDCAB                              
      Left Join MASERVICIOS_DET sd On d.IDServicio_Det=sd.IDServicio_Det          
      Left Join MADESAYUNOS Dey On sd.TipoDesayuno = Dey.IDDesayuno              
      Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio                     
      Left Join MAIDIOMAS Id On d.IDIdioma = Id.IDidioma                      
      Where d.IDReserva=@IDReserva                                
      And ((d.IDEmailEdit=@IDEmail Or d.IDEmailNew=@IDEmail) Or LTRIM(rtrim(@IDEmail))='')                 
      --and CHARINDEX(d.servicio,'Gastos de Transferencia')=0                
      --and sd.PlanAlimenticio = 0              
      and d.Anulado=0 and Not(d.FlServicioNoShow=1 and d.FlServicioIngManual=1)  
      ) As X                
      where (X.EvalSelecc = 0 Or @GastTransfer = 1)                
      order by Dia --cast(FechaIn as datetime)              
