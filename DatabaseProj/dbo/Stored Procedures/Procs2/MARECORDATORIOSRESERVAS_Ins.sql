﻿CREATE PROCEDURE [dbo].[MARECORDATORIOSRESERVAS_Ins]
	@Descripcion varchar(200),
	@Dias tinyint,
	@TipoCalculoTarea	char(2),
	@Activo bit,
	@UserMod char(4),
	@pIDRecordatorio smallint Output    
as

	set nocount on
	Declare @IDRecordatorio smallint
	Exec Correlativo_SelOutput 'MARECORDATORIOSRESERVAS',1,4,@IDRecordatorio output   	
	
	insert into MARECORDATORIOSRESERVAS(IDRecordatorio,Descripcion,Dias,TipoCalculoTarea,Activo,UserMod)
	values(@IDRecordatorio,@Descripcion,@Dias,@TipoCalculoTarea,@Activo,@UserMod)
	
	set @pIDRecordatorio=@IDRecordatorio
