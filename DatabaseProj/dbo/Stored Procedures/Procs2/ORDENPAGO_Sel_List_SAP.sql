﻿--JHD-20150831-No considerar dias de credito de proveedor para fecha de vencimiento
--JHD-20150901-ControlAccount=case when m.IDMoneda='SOL' THEN '42121001' else  '42122001' end,
--JHD-20150901-FederalTaxID = case when p.IDIdentidad='005' THEN P.CardCodeSAP ELSE p.NumIdentidad END,
--HLF-20150901-TaxDate=getdate(),
--JHD-20150901-,U_SYP_TCOMPRA='01'
--JHD-20150903-DocDate=getdate(),
--JHD-20150904-FederalTaxID= case when (SELECT IDPAIS FROM MAUBIGEO where IDubigeo=p.IDCiudad)<>'000323' THEN P.CardCodeSAP ELSE p.NumIdentidad END,
--JHD-20150907-,DocTotal=case when m.IDMoneda='SOL' THEN (op.SsMontoTotal-isnull(op.SSDetraccion,0)) else  dbo.FnCambioMoneda((op.SsMontoTotal-isnull(op.SSDetraccion,0)),op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
--JHD-20150907-DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then (op.SsMontoTotal-isnull(op.SSDetraccion,0)) else 0 end,0))
--JHD-20150916-TaxDate=op.FechaPago,
--JHD-20150916-DocDate=op.FechaPago,
--JHD-20150918-,DocTotal=case when m.IDMoneda='SOL' THEN (op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else  dbo.FnCambioMoneda((op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )),op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
--JHD-20150918-DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then (op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else 0 end,0))
--JHD-20150922-ControlAccount=case when m.IDMoneda='SOL' THEN '42211001' else  '42212001' end,
--JHD-20150922-declare @SSDetrac_Det as numeric(12,2)
--JHD-20150922-set @SSDetrac_Det=(select sum(isnull(ssdetraccion,0)) from ordenpago_det where IDOrdPag=@IDOrdPag)
--JHD-20150922-,DocTotal=case when m.IDMoneda='SOL' THEN (op.SsMontoTotal-isnull(@SSDetrac_Det,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else  dbo.FnCambioMoneda((op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )),op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
--JHD-20150922-DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then (op.SsMontoTotal-isnull(@SSDetrac_Det,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else 0 end,0))
--JHD-20150922-DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then (op.SsMontoTotal-isnull(@SSDetrac_Det,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else 0 end,0))
CREATE PROCEDURE [dbo].[ORDENPAGO_Sel_List_SAP]
@IDOrdPag int
AS
BEGIN

declare @SSDetrac_Det as numeric(12,2)
declare @CoMoneda as char(3)
set @CoMoneda=(select IDMoneda from ordenpago where IDOrdPag=@IDOrdPag)
set @SSDetrac_Det=(select sum(isnull( (case when @CoMoneda='SOL' THEN ssdetraccion ELSE SSDetraccion_USD END) ,0)) from ordenpago_det where IDOrdPag=@IDOrdPag)

select distinct
--TaxDate=op.FechaEmision,
--TaxDate=getdate(),
TaxDate=op.FechaPago,
--DocDate=op.FechaPago,
--DocDate=getdate(),
DocDate=op.FechaPago,
DocDueDate=op.FechaPago,--DATEADD(day,isnull((select NuDiasCredito from MAFORMASPAGO where IdFor=p.IDFormaPago),0), op.FechaPago),
CardCode=isnull(p.CardCodeSAP,''),
CardName=p.NombreCorto,
DocType=1, --servicios
--FederalTaxID=p.NumIdentidad,
--FederalTaxID = case when p.IDIdentidad='005' THEN P.CardCodeSAP ELSE p.NumIdentidad END,
FederalTaxID= case when (SELECT IDPAIS FROM MAUBIGEO where IDubigeo=p.IDCiudad)<>'000323' THEN P.CardCodeSAP ELSE p.NumIdentidad END,
DocCurrency=case when m.IDMoneda='SOL' THEN 'S/' else  m.Simbolo end,
--ControlAccount=d.CoCtaContab,
--ControlAccount=case when m.IDMoneda='SOL' THEN '42212001' else  '42211001' end,
--ControlAccount=case when m.IDMoneda='SOL' THEN '42211001' else  '42212001' end,
--ControlAccount='42211009',
--ControlAccount=case when m.IDMoneda='SOL' THEN '42121001' else  '42122001' end,
ControlAccount=case when m.IDMoneda='SOL' THEN '42211001' else  '42212001' end,
CashAccount=''
,Comments=isnull(op.TxObsDocumento,'')
,PaymentGroupCode=1--d.CoFormaPago
--,PaymentGroupCode=(select cosap from MAFORMASPAGO fp where fp.IdFor=p.IDFormaPago)--d.CoFormaPago
,DiscountPercent=0
--,DocTotal=case when m.IDMoneda='SOL' THEN op.SsMontoTotal else  dbo.FnCambioMoneda(op.SsMontoTotal,op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
--,DocTotal=case when m.IDMoneda='SOL' THEN (op.SsMontoTotal-isnull(op.SSDetraccion,0)) else  dbo.FnCambioMoneda((op.SsMontoTotal-isnull(op.SSDetraccion,0)),op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
--,DocTotal=case when m.IDMoneda='SOL' THEN (op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else  dbo.FnCambioMoneda((op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )),op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
--,DocTotal=case when m.IDMoneda='SOL' THEN (op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else  dbo.FnCambioMoneda((op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )),op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
,DocTotal=case when m.IDMoneda='SOL' THEN (op.SsMontoTotal-isnull(@SSDetrac_Det,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else  dbo.FnCambioMoneda((op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )),op.IDMoneda,'SOL',ISNULL(op.TCambio,isnull((select ValVenta from matipocambio where fecha=op.FechaEmision),0))) end,
--DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then op.SsMontoTotal else 0 end,0))
--DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then (op.SsMontoTotal-isnull(op.SSDetraccion,0)) else 0 end,0))
--DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then (op.SsMontoTotal-isnull(op.SSDetraccion,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else 0 end,0))
DocTotalFc=abs(isnull(case when op.IDMoneda<>'SOL' then (op.SsMontoTotal-isnull(@SSDetrac_Det,0)-(select sum(isnull(SSOtrosDescuentos,0)) from ORDENPAGO_DET where IDOrdPag=op.IDOrdPag )) else 0 end,0))
,U_SYP_MDTD='OP'--isnull(td.CoSunat,'')
,U_SYP_MDSD='000'--isnull(d.NuSerie,'')
,U_SYP_MDCD=isnull(OP.IDOrdPag,'')
,U_SYP_MDTO=''--ISNULL((select cosunat from matipodoc where idtipodoc=d.cotipodoc_Ref),'')
,U_SYP_MDSO=''--isnull(NuSerie_Ref,'')
,U_SYP_MDCO=''--isnull(NuDocum_Ref,'')
,U_SYP_STATUS= 'V' --case when op.CoEstado=1 then 'V' ELSE 'A' END
,U_SYP_FECHAREF='01/01/1900'-- CASE WHEN NuSerie_Ref IS NOT NULL THEN FeEmision_Ref ELSE '01/01/1900' END
,U_SYP_NUMOPER=OP.IDOrdPag--d.NuDocumProv
,U_SYP_TC=isnull(OP.TCambio,isnull((select ValVenta from matipocambio where fecha=OP.FechaEmision),0))
--,U_SYP_TC=CASE WHEN d.comoneda='SOL' THEN 0 ELSE isnull(d.SSTipoCambio,isnull((select ValVenta from matipocambio where fecha=d.feemision),0)) END
--,U_SYP_TCOMPRA='ER'--case when CoObligacionPago='FFI' THEN 'CC' else 'ER' END
,U_SYP_TCOMPRA='01'--case when CoObligacionPago='FFI' THEN 'CC' else 'ER' END
,U_SYP_NFILE=isnull((select idfile from coticab where idcab=r.idcab),'')
--,U_SYP_CODERCC=''
,U_SYP_CTAERCC=''

from ordenpago op
left join RESERVAS r on r.IDReserva=op.IDReserva
left join RESERVAS_DET rd on r.IDReserva=rd.IDReserva
left join MAPROVEEDORES p on p.IDProveedor=r.IDProveedor
left join MAMONEDAS m on m.IDMoneda=op.IDMoneda
left join MASERVICIOS_DET sd on sd.IDServicio_Det=rd.IDServicio_Det
where op.IDOrdPag=@IDOrdPag
END;
