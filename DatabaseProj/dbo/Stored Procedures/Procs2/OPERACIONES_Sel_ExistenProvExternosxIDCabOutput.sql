﻿CREATE Procedure dbo.OPERACIONES_Sel_ExistenProvExternosxIDCabOutput        
	@IDCab int,        
	@pbOk bit Output        
As        
	Set Nocount On        
    
	Set @pbOk = 0        
	If Exists(Select IDOperacion From OPERACIONES o 
		Inner Join MAPROVEEDORES p On o.IDCab=@IDCab And o.IDProveedor=p.IDProveedor 
		And p.IDTipoOper='E')
			  
		Set @pbOk = 1
