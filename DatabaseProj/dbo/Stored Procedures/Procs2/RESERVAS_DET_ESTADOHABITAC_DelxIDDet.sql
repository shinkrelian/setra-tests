﻿

--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet
CREATE PROCEDURE [dbo].[RESERVAS_DET_ESTADOHABITAC_DelxIDDet]
	@IDDet	int
As
	Set Nocount On
	
	Delete From RESERVAS_DET_ESTADOHABITAC 
	--Where IDReserva_Det In (Select IDReserva_Det From RESERVAS_DET Where IDDet=@IDDet)
	Where IDDet=@IDDet

