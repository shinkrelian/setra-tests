﻿Create Procedure [dbo].[MATIPOCAMBIO_Sel_Pk]
	@Fecha	smalldatetime
As
	Set NoCount On
	
	Select * From MATIPOCAMBIO 
	Where Fecha = @Fecha
