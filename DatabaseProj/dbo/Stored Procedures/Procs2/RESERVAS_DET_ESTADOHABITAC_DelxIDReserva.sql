﻿

--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet
CREATE Procedure [dbo].[RESERVAS_DET_ESTADOHABITAC_DelxIDReserva]
	@IDReserva int
As

	Set Nocount On
	
	Delete From RESERVAS_DET_ESTADOHABITAC 
	--Where IDReserva_Det In (Select IDReserva_Det From RESERVAS_DET Where IDReserva=@IDReserva)
	Where IDDet In (Select IDDet From RESERVAS_DET Where IDReserva=@IDReserva)

