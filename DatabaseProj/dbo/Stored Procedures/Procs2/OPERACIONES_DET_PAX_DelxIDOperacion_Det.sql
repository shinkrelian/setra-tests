﻿Create Procedure [Dbo].[OPERACIONES_DET_PAX_DelxIDOperacion_Det]
@IDOperacion_Det int
As
	Set NoCount On
		
	Delete from OPERACIONES_DET_PAX 
	Where IDOperacion_Det 
	In (select IDOperacion_Det from OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det)
