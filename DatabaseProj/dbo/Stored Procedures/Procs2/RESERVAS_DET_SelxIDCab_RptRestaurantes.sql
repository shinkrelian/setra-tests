﻿--JRF-20130701-Permitir tambien los hoteles con plan alimenticio.        
--JRF-20130828-Orden Cronologico.      
CREATE Procedure dbo.RESERVAS_DET_SelxIDCab_RptRestaurantes                
 @IDCab int                  
As                  
 Set NoCount On                  
                   
 Select Distinct rd.dia,              
  u.Descripcion  As DescCiudad,                
 Case When LEN(Cast(DAY(rd.Dia)As Char(2))) = 1 Then '0'+ Cast(DAY(rd.Dia)As Char(2)) Else  Cast(DAY(rd.Dia)As Char(2))End                
   +'.'+Case When LEN(Cast(MONTH(rd.Dia)As Char(2))) = 1 Then '0'+Cast(MONTH(rd.Dia)As Char(2)) Else Cast(MONTH(rd.Dia)As Char(2))End                  
 As Fecha,Convert(char(5),rd.Dia,108) As Hora, p.NombreCorto as DescProveedor, rd.Servicio,p.IDTipoProv,            
 Case When Isnull(cd.IncGuia,0)=1 Or CHARINDEX('Guide',rd.Servicio)>0 Then           
 'GUIA'          
 Else          
 Cast(rd.NroPax As varchar(15))          
 End As PaxTCGuia,          
 Case When Isnull(cd.IncGuia,0)=1  Then           
 'SI'          
 Else          
 'NO'          
 End As ComGui,r.Estado                
 From RESERVAS r Left Join RESERVAS_DET rd On r.IDReserva=rd.IDReserva                  
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor                  
 Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo                  
 Left Join COTICAB C On r.IDCab = C.IDCAB               
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det    
 Inner Join COTIDET cd On rd.IDDet = cd.IDDet     
 Where r.IDCab=@IDCab and rd.Anulado=0 and r.Anulado=0 
 And (p.IDTipoProv = '002' or (p.IDTipoProv='001' and sd.PlanAlimenticio = 1))    
 and rd.Dia is not null    
 Order by rd.dia,u.Descripcion   
 
