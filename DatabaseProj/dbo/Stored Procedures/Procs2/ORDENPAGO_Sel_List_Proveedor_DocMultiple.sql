﻿--JHD-20151002- Se quitaron algunas condiciones para listar las ordenes de pago
--JHD-20151002- Codigo=o.IDOrdPag,
--JHD-20151027- cambiar la busqueda incluyendo los proveedores que tienen como prov. internacional el @idproveedor
--JHD-20151104-IGV=0,
CREATE PROCEDURE ORDENPAGO_Sel_List_Proveedor_DocMultiple
@idproveedor char(6),--='000688',
@CoMoneda char(3)--='USD'
as
BEGIN

 declare @tablaproveedores as table(
 idprov char(6)
 )

 insert into @tablaproveedores values (@idproveedor)

 insert into @tablaproveedores 
 select distinct idproveedor from MAPROVEEDORES where IDProveedorInternacional=@idproveedor

select distinct 
Codigo=o.IDOrdPag,
IDVoucher=o.IDOrdPag,
Titulo=c.Titulo,
IDCAB=c.IDCAB,
IDFile=c.IDFile,
IDMoneda=o.idmoneda,
Simbolo=(select simbolo from MAMONEDAS where IDMoneda=o.idmoneda),
IGV=0,
Total=ssmontototal

from ORDENPAGO o
inner join reservas r on r.idreserva=o.idreserva
inner join coticab c on c.idcab=r.idcab

--where r.IDProveedor=@idproveedor
where r.IDProveedor in (select idprov from @tablaproveedores)
AND (o.idMoneda=@CoMoneda or @CoMoneda='')
----and o.IDOrdPag<>'0'
and o.IDOrdPag not in (select distinct CoOrdPag from DOCUMENTO_PROVEEDOR where CoOrdPag is not null and FlActivo=1)
--and o.IDEstado<>'PG'
--and o.CoSap is null
order by o.IDOrdPag
END;
