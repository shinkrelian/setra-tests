﻿Create Procedure [dbo].[MATIPOSERV_Del]
	@IDservicio char(3)
As
	
	Set NoCount On
	
	Delete From MATIPOSERV
    Where
           IDservicio=@IDservicio
