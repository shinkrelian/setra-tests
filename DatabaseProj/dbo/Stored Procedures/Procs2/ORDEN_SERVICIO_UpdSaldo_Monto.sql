﻿Create Procedure dbo.ORDEN_SERVICIO_UpdSaldo_Monto
@NuOrdenServicio int
As
Set NoCount On
Declare @MonedaFEgreso char(3)=(select Top(1) odds.IDMoneda 
							from ORDEN_SERVICIO_DET osd Inner Join OPERACIONES_DET_DETSERVICIOS odds On osd.IDOperacion_Det=odds.IDOperacion_Det
							and osd.IDServicio_Det=odds.IDServicio_Det
							Where osd.NuOrden_Servicio=@NuOrdenServicio)
Declare @NuIgv numeric(8,2)=(select NuIGV from PARAMETRO)
Declare @TotalDoc numeric(9,2)=Isnull((select Sum(Case When CoMoneda<>@MonedaFEgreso Then SsTotal_FEgreso Else SsTotalOriginal End) 
								 from DOCUMENTO_PROVEEDOR where NuOrden_Servicio=@NuOrdenServicio and FlActivo=1),0)

Declare @SsMontoTotal numeric(8,2)= dbo.FnDevuelveMontoTotalxNuOrdenServicio(@NuOrdenServicio)
Update ORDEN_SERVICIO
	set SsMontoTotal= @SsMontoTotal,
		SsSaldo=@SsMontoTotal-@TotalDoc-IsNull(ORDEN_SERVICIO.SsDifAceptada,0)
where NuOrden_Servicio=@NuOrdenServicio
