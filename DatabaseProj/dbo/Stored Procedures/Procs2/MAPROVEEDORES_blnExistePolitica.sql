﻿Create Procedure MAPROVEEDORES_blnExistePolitica
@IDProveedor char(6),
@pExistePolit bit output
As
	Set NoCount On
	If Exists(Select IDPolitica from MAPOLITICAPROVEEDORES	where IDProveedor = @IDProveedor)
	   set @pExistePolit = 1
	Else
	   set @pExistePolit = 0
