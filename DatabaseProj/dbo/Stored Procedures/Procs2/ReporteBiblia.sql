﻿--JRF-20130819-Agregar el campo  ObservIntern.                      
--JRF-20130902-Solo debe mostrarse una sola vez en caso de Hoteles.                    
--JRF-20130902-Agregar columnas del Nombre Trasladistas y Tipo de Servicio.                    
--JRF-20130902-Cambiar el IDIoma completo por su sigla.                    
--JRF-20130902-Agregar la función para el Codigo PNR.                  
--JRF-20130904-Agregar el Filtro por Operador.                  
--JRF-20131004-Corrección del Subquery para caso MULTIPLE              
--HLF-20140207-((Y.IDGuiaProgramado = @IDGuiaProg Or LTRIM(rtrim(@IDGuiaProg))='') Or Y.IDGuiaProgramado ='MULTIPLE') And              
--JRF-20140318-Filtro de cotizaciones x estado(No deben aparecer las Cotiz. de Estado 'E' o 'X')        
--JRF-20140507-Agregar la fechaIn y FechaOut como campo para poder leerlo en la generación del PDF.      
--JRF-20140522-Agregar el IDOperador.  
--JHD-20150411-Se cambio la tabla MAVEHICULOSPROGRAMACION por MAVEHICULOS_TRANSPORTE
--JRF-20150416-Se Aplico Ltrim(Rtrim( a los campos...) e IsNull a rd.Anulado
--JHD-20150420-
				/*
				,'')  + isnull(X.VehiculoProg,'')                           
				  End                            
				  as TransportistaProgramado
				*/
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-(u.IDPais=LTRIM(rtrim(@CoPais)) OR LTRIM(rtrim(@CoPais))='') And   

--HLF-20150917-case when @IDProveedor='001554' then  ... as Trasladista, as CoTrasladista
--JRF-20160426-And IsNull(od.FlServicioTC,0)=0 And IsNull(rd.NuViaticoTC,0) = 0
CREATE Procedure dbo.ReporteBiblia
 @Cotizacion varchar(8),                          
 @IDCab int,                          
 @IDGuiaProg char(6),                 
 @IDProveedor char(6),                         
 @IDTransProg char(6),                          
 @IDUsuarioOpe char(4),                          
 @IDIdioma varchar(12),                          
 @IDUbigeo char(6),                          
 @FechaIni smalldatetime,                          
 @FechaFin smalldatetime,
 @CoPais char(6)=''                          
As                          
 Set Nocount On                          
 Declare @Dia smalldatetime                          
                         
 DELETE FROM BIBLIATMP                        
                         
 INSERT INTO BIBLIATMP                            
SELECT Y.* --INTO BIBLIATMP                           
 FROM                          
  (                          
  SELECT --(ROW_NUMBER() Over (Order by Dia)) as NroOrd,                          
  Null as NroOrd,Null as NroOrd2,                          
  X.Cotizacion, X.IDFile, X.Titulo, X.DescCliente,X.Dia,Convert(smalldatetime,CONVERT(varchar,X.Dia,103),103) as Dia103,X.DiaFormat, X.Hora,                          
  X.DescUbigeo, X.PaxL, X.Servicio, X.ServicioHtl, X.IDIdioma, X.IDTipoProv, X.DescProvee, X.IDOperadorProgramado, X.ResponsableOpe,                           
  X.ObservBiblia,x.ObservInterno ,                      
  Case When CantGuiasProgramados>1 Then 'MULTIPLE'                            
  Else                            
  IsNull(                            
  (Select Distinct p1.NombreCorto  From OPERACIONES_DET_DETSERVICIOS od1                             
  Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                          
  Where od1.IDOperacion_Det=X.IDOperacion_Det)                            
  ,'')   + '/'                      
  End                            
  as GuiaProgramado,                          
                           
   --******                        
  -- isnull((Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                             
  --Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                          
  --Where od1.IDOperacion_Det=X.IDOperacion_Det),'')           
  X.IDGuiaProgramado As IDGuiaProgramado,                        
  --******                    
                           
  Case When CantTranspProgramados >1 Then 'MULTIPLE'                            
  Else                            
  IsNull(                            
  (Select Distinct p1.NombreCorto+CHAR(13)+ISNULL(p1.Movil1,'') From OPERACIONES_DET_DETSERVICIOS od1                             
  Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                            
  Where od1.IDOperacion_Det=X.IDOperacion_Det)                            
  ,'')  + isnull(X.VehiculoProg,'')                           
  End                            
  as TransportistaProgramado  ,               
                          
  --******                        
  --isnull((select Distinct IDProveedor_Prg from OPERACIONES_DET_DETSERVICIOS od1                        
  --Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg = p1.IDProveedor And p1.IDTipoProv = '004'                        
  --Where od1.IDOperacion_Det=X.IDOperacion_Det),'')  As IDTransportistaProgramado                     
  X.IDTransportistaProgramado                 
  --******                        
  ,X.CodigoPNR,                    
  X.Trasladista ,X.FechaInicio,X.FechaOUT,
  X.CoTrasladista
                          
  FROM                  
   (                          
   Select                           
   cc.Cotizacion, cc.IDFile, cc.Titulo, c.RazonComercial as DescCliente,                          
   od.Dia,                          
   upper(substring(DATENAME(dw,od.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,od.Dia,103))) as DiaFormat,                          
   substring(convert(varchar,od.Dia,108),1,5) as Hora,                          
   u.Descripcion as DescUbigeo,                          
  Cast(od.NroPax as varchar(3))+' / '+Cast(ISNULL(od.NroLiberados,0) as varchar(3)) as PaxL,                          
   od.Servicio,ISNULL(rd.Servicio,OD.Servicio) as ServicioHtl,                    
                         
   IsNull(case when s.IDTipoServ = 'NAP' then                     
		case when idi.Siglas = '---' then '-' else '('+idi.Siglas+')' End                    
   else                    
		s.IDTipoServ + case when idi.Siglas = '---' then '' else '('+idi.Siglas+')' End                    
   End,idi.Siglas) as IDIdioma                    
                        
    ,p.IDTipoProv, p.NombreCorto +' /' as DescProvee, p.IDProveedor as IDOperadorProgramado,                         
   ISNULL(us.Siglas,'') as ResponsableOpe, od.ObservBiblia, od.ObservInterno,                      
   (Select count(*) From                            
   (                            
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                             
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                          
   Where od1.IDOperacion_Det=od.IDOperacion_Det                            
   ) as x                              
   ) as CantGuiasProgramados,                        
                    
   --******                        
  Case When (Select count(Distinct IDProveedor_Prg) From OPERACIONES_DET_DETSERVICIOS od1                             
    Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                          
          Where od1.IDOperacion_Det=od.IDOperacion_Det)  > 1 then 'MULTIPLE'           
  Else          
  ISNULL((Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                             
  Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                          
  Where od1.IDOperacion_Det=od.IDOperacion_Det),'') End          
  As IDGuiaProgramado,                         
   --******                        
                           
   (Select count(*) From    
   (                            
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                             
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                            
   Where od1.IDOperacion_Det=od.IDOperacion_Det                            
   ) as x                              
   ) as CantTranspProgramados,                        
                           
   --******                   
  case when                             
    (Select count(Distinct IDProveedor_Prg) From OPERACIONES_DET_DETSERVICIOS od1                             
    Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                            
    Where od1.IDOperacion_Det=od.IDOperacion_Det ) > 1 then 'MULTIPLE'              
  Else              
   isnull((select Distinct IDProveedor_Prg from OPERACIONES_DET_DETSERVICIOS od1                        
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg = p1.IDProveedor And p1.IDTipoProv = '004'                        
   Where od1.IDOperacion_Det=od.IDOperacion_Det),'')       
  End              
  As IDTransportistaProgramado                        
  --******                        
   , od.IDOperacion_Det   ,dbo.FnPNRxIDCabxDia(o.IDCab,od.Dia) as CodigoPNR,                    
   --case when od.IDVehiculo_Prg is not null then                    '('+ vhp.NoUnidadProg + ' - '+ CAST(vhp.QtCapacidadPax as varchar(5)) +' Pax )'                    
   case when od.IDVehiculo_Prg is not null then                    '('+ vhp.NoVehiculoCitado + ' - '+ CAST(vhp.QtCapacidad as varchar(5)) +' Pax )'                    
   else '' End As VehiculoProg,                    
                
	Case when o.IDProveedor='001554' Then
		IsNull(                                              
		(Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                     
		Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='020'                                                                                      
		Where od1.IDOperacion_Det=od.IDOperacion_Det                                     
		--And od1.IDServicio=d.IDServicio                                    
		)                                                
		,'')   	
	Else
		(select distinct Isnull(uTrs.Nombre,'')                    
		from OPERACIONES_DET_DETSERVICIOS Odd2 Left Join MAUSUARIOS uTrs On Odd2.IDProveedor_Prg = uTrs.IDUsuario                    
		where Odd2.IDOperacion_Det = od.IDOperacion_Det and uTrs.Nombre is not null)              

	End as Trasladista    ,      
	Case when o.IDProveedor='001554' Then	  
		IsNull(                                              
		(Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                     
		Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='020'                                                                                      
		Where od1.IDOperacion_Det=od.IDOperacion_Det                                     
		--And od1.IDServicio=d.IDServicio                                    
		)                                                
		,'')   	
	Else
		'' 
	End as CoTrasladista,    

  CONVERT(Char(10),@FechaIni,103) as FechaInicio,CONVERT(Char(10),@FechaFin,103) as FechaOUT      
                          
   From OPERACIONES_DET od                           
   Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion                           
   Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor                          
   Left Join MAUBIGEO u On u.IDubigeo=od.IDubigeo                          
   Left Join COTICAB cc On cc.IDCAB=o.IDCab                          
   Left Join MACLIENTES c On cc.IDCliente=c.IDCliente                      
   Left Join MAUSUARIOS us On cc.IDUsuarioOpe=us.IDUsuario                          
   Left Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det --and rd.Anulado =0                        
   Left Join COTIDET cd On cd.IDDET=rd.IDDet                          
   Left Join MAIDIOMAS idi On od.IDIdioma = idi.IDidioma                    
   Left Join MASERVICIOS s On od.IDServicio = s.IDServicio                    
   --Left Join MAVEHICULOSPROGRAMACION vhp On od.IDVehiculo_Prg = vhp.NuVehiculoProg                    
   Left Join MAVEHICULOS_TRANSPORTE vhp On od.IDVehiculo_Prg = vhp.NuVehiculo                    
   Where (od.IDubigeo=LTRIM(rtrim(@IDUbigeo)) OR LTRIM(rtrim(@IDUbigeo))='') And                          
   (u.IDPais=LTRIM(rtrim(@CoPais)) OR LTRIM(rtrim(@CoPais))='') And   
   (cc.IDUsuarioOpe=LTRIM(rtrim(@IDUsuarioOpe)) OR LTRIM(rtrim(@IDUsuarioOpe))='') And                          
   (od.Dia Between @FechaIni And  DATEADD(S,-1,DATEADD(D,1, Convert(varchar,@FechaFin,103))) OR @FechaIni='01/01/1900') And                          
   (cc.IDCAB=@IDCab OR @IDCab=0) and Not cc.Estado In('E','X') and od.VerObserBiblia = 1 And                
   (LTRIM(rtrim(@IDIdioma))='' Or OD.IDIdioma = LTRIM(rtrim(@IDIdioma))) And                
   (ltrim(rtrim(@IDProveedor))='' Or o.IDProveedor = ltrim(rtrim(@IDProveedor))) And IsNull(rd.Anulado,0)=0 
   And IsNull(od.FlServicioTC,0)=0 And IsNull(rd.NuViaticoTC,0) = 0
   ) As X                          
  ) as Y                          
 WHERE (Y.Cotizacion Like '%' + @Cotizacion + '%' Or LTRIM(rtrim(@Cotizacion))='') And                                       
 ((Y.IDGuiaProgramado = @IDGuiaProg Or LTRIM(rtrim(@IDGuiaProg))='') Or Y.IDGuiaProgramado ='MULTIPLE' or y.CoTrasladista=@IDGuiaProg ) And                
 ((Y.IDTransportistaProgramado=@IDTransProg Or LTRIM(rtrim(@IDTransProg))='') Or Y.IDTransportistaProgramado ='MULTIPLE')--And              
 ORDER BY Y.Dia
                           
                           
                           
 --SELECT * INTO REPORTE_BIBLIA FROM #BIBLIA                           
 DELETE FROM REPORTE_BIBLIA                          
                         
 Declare @siCont smallint=1                          
 Declare curBiblia Cursor For Select Distinct convert(smalldatetime,convert(varchar,Dia103,103),103) From BIBLIATMP                           
 Open curBiblia                       
 Fetch Next From curBiblia Into @Dia                          
 While (@@FETCH_STATUS=0)                          
  Begin                          
                            
  Insert Into REPORTE_BIBLIA                           
  Select Top 1 * From BIBLIATMP Where Dia103=convert(smalldatetime,convert(varchar,dateadd(d,-1,@Dia),103),103) And IDTipoProv='001'                          
                            
  Update REPORTE_BIBLIA Set NroOrd=@siCont, NroOrd2=0, Servicio=ServicioHtl  Where NroOrd Is Null                          
                            
             
  ----                          
  Insert Into REPORTE_BIBLIA                           
  Select * From BIBLIATMP Where Dia103=@Dia And IDTipoProv<>'001'                          
                            
  Update REPORTE_BIBLIA Set NroOrd=@siCont, NroOrd2=1 Where NroOrd Is Null                          
                            
  ----                          
  Insert Into REPORTE_BIBLIA                           
  Select Top 1 * From BIBLIATMP Where Dia103=@Dia And IDTipoProv='001'                          
                            
  Update REPORTE_BIBLIA Set NroOrd=@siCont, NroOrd2=2, Servicio=ServicioHtl  Where NroOrd Is Null                          
                            
  Set @siCont+=1                          
  Fetch Next From curBiblia Into @Dia                          
  End                          
                           
 Close curBiblia                          
 Deallocate curBiblia      
 --DROP TABLE BIBLIATMP                         
 delete from REPORTE_BIBLIA where NroOrd2 = 0                
 SELECT distinct * FROM REPORTE_BIBLIA ORDER BY Dia,NroOrd, NroOrd2                          
 --Select Distinct Dia103,convert(smalldatetime,convert(varchar,dateadd(d,-1,Dia103),103),103) From #BIBLIA order by Dia103                          
                          
 --DROP TABLE REPORTE_BIBLIA                          
 --DELETE FROM REPORTE_BIBLIA           
