﻿--HLF-20140407-NoFiscal  
--HLF-20150205-@CoUbigeo_Oficina char(6)
--HLF-20151215-And IDtipodoc<>'TIP'
CREATE PROCEDURE dbo.MATIPODOC_SelPreLiquidacion    
    @CoUbigeo_Oficina char(6)
As    
 Set Nocount on    
    
 SELECT IDtipodoc,--Descripcion as DescTipoDoc   
 NoFiscal as DescTipoDoc   
 FROM MATIPODOC where NOT FlOrdenPreLiq is null 
 And CoUbigeo_Oficina=@CoUbigeo_Oficina 
 And IDtipodoc<>'TIP'
 order by FlOrdenPreLiq    
 

