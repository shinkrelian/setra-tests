﻿ 
 
 Create Procedure dbo.PRE_LIQUI_DET_SelOutput
	@IDCab	int,
	@pExiste bit Output
As
	Set Nocount On
	
	Set @pExiste=0
	If Exists(Select IDNotaVenta From PRE_LIQUI_DET Where IDNotaVenta IN 
		(Select IDNotaVenta From NOTA_VENTA Where IDCab=@IDCab And IDEstado<>'AN'))
		Set @pExiste=1

