﻿CREATE PROCEDURE [dbo].[MARECORDATORIOSRESERVAS_Del]
@IDRecordatorio smallint
as
begin
	set nocount on
	Delete dbo.MARECORDATORIOSRESERVAS where IDRecordatorio=@IDRecordatorio
end
