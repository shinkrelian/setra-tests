﻿
Create Procedure dbo.NOTA_VENTA_DET_RptCosto_NoGastoAdm 
@IDNotaVenta char(10)
As
	Set NoCount On
	 Select  toc.Orden,
			 sd.idTipoOC,  
			 toc.Descripcion as DescTipoOC,  
			 p.NombreCorto As DescProveedor,  
			 nvd.Descripcion As Servicio,  
			 nvd.SubTotal As Neto,  
			 nvd.TotalIGV As Igv,  
			 nvd.Total AS Total,
			 (select SUM(Total) from DEBIT_MEMO 
			  where IDCab = (select IDCab from NOTA_VENTA 
			  where IDNotaVenta= @IDNotaVenta)) As TotalDebitMemo
			  
	 From NOTA_VENTA_DET nvd   
	 Left Join RESERVAS_DET rd On nvd.IDReserva_Det=rd.IDReserva_Det  
	 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det  
	 Left Join MATIPOOPERACION toc On toc.IdToc=sd.idTipoOC  
	 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
	 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor  
	 Where nvd.IDNotaVenta =@IDNotaVenta  And sd.idTipoOC <> '005'
	 Order by toc.Orden  
