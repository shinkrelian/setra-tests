﻿CREATE Procedure [dbo].[MAPROVEEDORES_RANGO_APT_DelxIDProveedor]
	@IDProveedor int
As
	Set NoCount On
	
	Delete From MAPROVEEDORES_RANGO_APT 
	WHERE
           (IDProveedor=@IDProveedor
          )
