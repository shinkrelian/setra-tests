﻿Create Procedure dbo.MASERVICIOS_ExistsxIDServicio
@IDServicio char(8),
@IDCab int,
@pExist bit output
As
Set NoCount On
Set @pExist = 0
Select @pExist = 1 from MASERVICIOS Where IDCabVarios=@IDCab And IDServicio=@IDServicio And cActivo='A'
