﻿CREATE PROCEDURE [dbo].[MARECORDATORIOSRESERVAS_Sel_Lvw]
 @IDRecordatorio smallint,
 @Descripcion varchar(200)
 as
 begin
	set nocount on
	select descripcion from dbo.MARECORDATORIOSRESERVAS
	Where (Descripcion Like '%'+@Descripcion+'%')  
	And (IDRecordatorio<>@IDRecordatorio Or @IDRecordatorio=0)  
	Order by Descripcion  
 end
