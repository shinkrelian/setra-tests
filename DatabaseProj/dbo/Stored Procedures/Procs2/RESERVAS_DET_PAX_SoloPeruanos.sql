﻿Create Procedure dbo.RESERVAS_DET_PAX_SoloPeruanos
@IDReservas_Det int,
@pSoloPeruano bit output
As
Set NoCount On
Set @pSoloPeruano = 0
select @pSoloPeruano = 1
from RESERVAS_DET_PAX rdp Left Join COTIPAX cp On rdp.IDPax=cp.IDPax
where IDReserva_Det=@IDReservas_Det And (cp.IDNacionalidad = '000323' Or cp.Residente=1)
