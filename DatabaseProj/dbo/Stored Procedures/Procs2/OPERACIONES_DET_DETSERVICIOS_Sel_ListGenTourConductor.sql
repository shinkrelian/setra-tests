﻿--JRF-20150424-Considerar el Union
--JRF-20150917-@IDProvTourC..
--JRF-20151125-Ajsutar Stock
--JRF-20151125-Agregar filtro de moneda...
--JRF-20151207-Ordenar por Cena y Almuerzo
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Sel_ListGenTourConductor
@IDCab int,
@IDProveedor char(6),
@NuPreSob int,
@IDProvTourC varchar(6)
As
Declare @IDTipoHon char(3)= (select CoTipoHon from COTICAB_TOURCONDUCTOR where IDCab=@IDCab)

Set NoCount On
Select Y.IDServicio_Det,Y.IDServicio_Det_V_Cot,Y.IDOperacion_Det,Y.NuDetPSo,Y.NuSincerado_Det,Y.Dia,Y.Hora,Y.DescProveeGuia,Y.IDProveedorGuia,
	   Y.IDPax,Y.DescServicio,Y.Pax,Y.IDMoneda,Y.SimMoneda,Y.PreUnit,Y.Total,Y.Total as TotalPreCompra,Y.Sustentado,Y.Devuelto,Y.RegistradoPresup,Y.RegistradoSincerado,
	   Y.CoTipPSo,Y.DescCoTipPSo,Y.CoPago,Y.DescPago,Y.RegVal
from (
Select X.IDServicio_Det,X.IDServicio_Det_V_Cot,X.IDOperacion_Det,X.NuDetPSo,X.NuSincerado_Det,X.Dia,X.Hora,X.DescProveeGuia,X.IDProveedorGuia,X.IDPax,X.DescServicio,X.Pax,
	   X.IDMoneda,X.SimMoneda,X.PreUnit,X.Total,X.Total as TotalPreCompra,X.Sustentado,X.Devuelto,X.RegistradoPresup,X.RegistradoSincerado,X.CoTipPSo,
	 (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=X.CoTipPSo) as DescCoTipPSo, X.CoPago,X.DescPago,'R' as RegVal,X.Orden
from (
Select IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det) as IDServicio_Det,IsNull(odd.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,odd.IDOperacion_Det,
	   ROW_NUMBER()Over(Order By od.Dia) as NuDetPSo,0 as NuSincerado_Det,od.Dia,Convert(char(5),od.Dia,108) as Hora,
	   prvPrg.NombreCorto as DescProveeGuia,
	   '' as IDProveedorGuia,0 as IDPax,sd.Descripcion as DescServicio,od.NroPax+IsNull(od.NroLiberados,0) as Pax,
	   Case When @IDTipoHon = '002' Then 'USD' else 
		Case When isnull(s.CoPago,'EFE') ='PCM' Then '' Else odd.IDMoneda End End As IDMoneda,
	   Case When @IDTipoHon = '002' Then 'US$' else 
		Case When isnull(s.CoPago,'EFE') ='PCM' Then '' Else mo.Simbolo End End as SimMoneda, 
	   Case When @IDTipoHon='002' And odd.IDMoneda='SOL' then IsNull(odd.TotalProgram, odd.CostoReal) / IsNull(odd.TipoCambio,1)
		else IsNull(odd.TotalProgram, odd.CostoReal) End /(od.NroPax+IsNull(od.NroLiberados,0)) as PreUnit,
	   Case When isnull(s.CoPago,'EFE') ='PCM' Then 0 Else 
	   Case When od.FlServicioTC=1 Then 
			Case When @IDTipoHon='002' And odd.IDMoneda='SOL' then IsNull(odd.TotalProgram, odd.CostoReal) / IsNull(odd.TipoCambio,1) else 
			IsNull(odd.TotalProgram, odd.CostoReal) End
			else IsNull(odd.TotalProgram, odd.Total) * (od.NroPax+IsNull(od.NroLiberados,0)) End End as Total,
	   0 as Sustentado,0 as Devuelto,0 as RegistradoPresup,0 As RegistradoSincerado,
	   case when p4.IDTipoProv='006' Then 'EN' else 
		case when s.copago in('EFE','TCK') and s.Descripcion like 'Tips%' then 'PR' else 'VA' end end As CoTipPSo,s.CoPago,
	    Case  isnull(s.CoPago,'EFE')
		 When 'EFE' Then 'EFECTIVO'                  
		 When 'TCK' Then 'TICKET'                  
		 When 'PCM' Then 'PRECOMPRA ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=odd.IDServicio),0) As varchar(4))+')'              
		 End As DescPago,odd.IDServicio,p4.NombreCorto,p4.IDProveedor,Case When sd.CoTipoCostoTC In ('ALM','CEN') then 1 else 2 End as Orden
from OPERACIONES_DET_DETSERVICIOS odd Inner Join OPERACIONES_DET od 
On (od.IDServicio=odd.IDServicio and od.IDOperacion_Det=odd.IDOperacion_Det) and od.FlServicioTC=1
Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion
Inner Join MASERVICIOS_DET sd On odd.IDServicio_Det=sd.IDServicio_Det
Left Join MAMONEDAS mo On odd.IDMoneda=mo.IDMoneda
Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
Left Join MAPROVEEDORES p4 On s.IDProveedor=p4.IDProveedor
--Left Join (select Top(1)* from COTIPAX cp where CP.IDCab=@IDCab and cp.FlTC=1 Order By Orden) cp3 On o.IDCab=cp3.IDCab
Left Join MAPROVEEDORES prvPrg On odd.IDProveedor_Prg=prvPrg.IDProveedor
Where o.IDCab=@IDCab and o.IDProveedor=@IDProveedor and odd.Activo=1 and odd.IDProveedor_Prg = @IDProvTourC
	  and sd.CoTipoCostoTC In(select tct.CoTipoCostoTC from MATIPOCOSTOSTC tct WHERE Not tct.CoTipoCostoTC In('HON','VID','VIS') And tct.FlActivo=1)) as X
 Union
 Select IsNull(pd.IDServicio_Det,0) as IDServicio_Det,IsNull(sd.IDServicio_Det_V,0) as IDServicio_Det_V,IsNull(PD.IDOperacion_Det,0) as IDOperacion_Det,
 pd.NuDetPSo,0 as NuSincerado_Det,pd.FeDetPSo as Dia,Convert(char(5),pd.FeDetPSo,108) as Hora,
 IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescProveeGuia,
 IsNull(pd.CoPrvPrg,'') as CoPrvPrg,IsNull(pd.IDPax,0) as IDPax, pd.TxServicio,
 pd.QtPax,IsNull(pd.IDMoneda,'') as IDMoneda,IsNull(mo.Simbolo,'') as SimMoneda, pd.SsPreUni, pd.SsTotal as Total, pd.SsTotal as TotalPreCompra,
 pd.SsTotSus as Sustentado, pd.SsTotDev as Devuelto, 1 as RegistradoPresup,0 as RegistradoSincer,pd.CoTipPSo,es.NoTipPSo as DescCotipPSo,
 pd.CoPago,
 Case pd.CoPago           
 When 'EFE' Then 'EFECTIVO'                
 When 'TCK' Then 'TICKET'                
 When 'PCM' Then 'PRECOMPRA ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sd.IDServicio),0) As varchar(4))+')'
 --When '' Then 'EFECTIVO'                
 End As DescPago,'R',Case When sd.CoTipoCostoTC In ('ALM','CEN') then 1 else 2 End as Orden
 from PRESUPUESTO_SOBRE_DET pd Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor
 Left Join COTIPAX cp On pd.IDPax=cp.IDPax
 Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
 Left Join MAMONEDAS mo On pd.IDMoneda=mo.IDMoneda
 Left Join MASERVICIOS_DET sd On pd.IDServicio_Det=sd.IDServicio_Det
 Where pd.NuPreSob=@NuPreSob and pd.IDOperacion_Det is Null
 ) as Y
Order By Y.Orden,Y.Dia
