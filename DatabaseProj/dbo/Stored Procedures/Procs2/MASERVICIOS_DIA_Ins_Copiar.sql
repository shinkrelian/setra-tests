﻿
--HLF-20151020- nuevas columnas NoHotel,TxWebHotel
CREATE PROCEDURE dbo.MASERVICIOS_DIA_Ins_Copiar
	@IDServicio	char(8),
	@IDServicioNue	char(8),
	@UserMod	char(4)
As
	Set Nocount On
	
	Insert into MASERVICIOS_DIA(IDServicio,Dia,IDIdioma,DescripCorta,Descripcion,
		NoHotel,TxWebHotel,UserMod)
	Select @IDServicioNue,Dia,IDIdioma,DescripCorta,Descripcion,
		NoHotel,TxWebHotel,@UserMod 
	From MASERVICIOS_DIA
	Where IDServicio=@IDServicio