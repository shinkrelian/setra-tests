﻿--JRF-20150917--Agregar ..@UserAccess
CREATE Procedure [dbo].[MATIPOPROVEEDOR_Sel_Cbo]
	@bTodos	bit,
	@UserAccess varchar(4) =''
As
	Set NoCount On
	Declare @Grabar bit = (select Grabar from MAUSUARIOSOPC Uopc Left Join MAOPCIONES opc On Uopc.IDOpc=opc.IDOpc
						   where opc.Nombre='mnuAccesosMantTipoProvGuias' And Uopc.IDUsuario=@UserAccess)

	SELECT distinct IDTipoProv,Descripcion,Ord
	FROM
	(
	Select '' as IDTipoProv,'<TODOS>' as Descripcion, 0 as Ord,1 as EvalGuia
	Union
	
	Select IDTipoProv, Descripcion, 1 as Ord,
		   Case When @UserAccess = '' then 1 else 
			Case When IDTipoProv='005' then
			 Case when  @Grabar=1 then 1 else 0 End else 1 End End as  EvalGuia
	From MATIPOPROVEEDOR
	--where (@UserAccess<>'' and IDTipoProv='005')

	--Union
	--Select IDTipoProv, Descripcion, 1 as Ord From MATIPOPROVEEDOR
	--where (@Grabar=1 and IDTipoProv='005')

	) AS X
	Where (@bTodos=1 Or Ord<>0) And X.EvalGuia=1
	Order by Ord,2
