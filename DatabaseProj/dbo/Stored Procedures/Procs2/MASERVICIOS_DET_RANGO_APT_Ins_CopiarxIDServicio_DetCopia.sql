﻿
--PPMG-20151105-Se agrego el insertar MASERVICIOS_DET_DESC_APT
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_Ins_CopiarxIDServicio_DetCopia]  
 --@IDServicio	char(8),
 @IDServicio_DetCopia int, 
 @IDServicio_Det int, 
 @PorcentDcto numeric(5,2),  
 @UserMod char(4)  
AS
BEGIN
	Set NoCount On
	
	INSERT INTO MASERVICIOS_DET_DESC_APT(IDServicio_Det, IDTemporada, DescripcionAlterna, UserMod, FecMod)
	Select IDServicio_Det, IDTemporada, DescripcionAlterna, UserMod, FecMod From 
		(
			Select @IDServicio_Det as IDServicio_Det,
				IDTemporada, DescripcionAlterna
				, @UserMod as UserMod, GETDATE() as FecMod
			From MASERVICIOS_DET_DESC_APT c
			Where
				c.IDServicio_Det = @IDServicio_DetCopia 
		) as T

	Insert Into MASERVICIOS_DET_RANGO_APT  
	(Correlativo, IDServicio_Det, PaxDesde, PaxHasta, Monto, UserMod, IDTemporada)  
	Select Correlativo, IDServicio_Det, PaxDesde, PaxHasta, Monto, UserMod, IDTemporada From 
		(
			Select c.Correlativo, 
				--20121024-I
				--(Select Top 1 
				--   IDServicio_Det From MASERVICIOS_DET 
				--Where IDServicio_DetCopia=@IDServicio_DetCopia And IDServicio=@IDServicio			
				--Order by FecMod Desc
				--) as IDServicio_DetNue, 
				@IDServicio_Det as IDServicio_Det,
				--20121024-F
				c.PaxDesde, c.PaxHasta,   
 				Case When @PorcentDcto=0 Then c.Monto Else c.Monto*(1-(@PorcentDcto/100)) End as Monto,  
				@UserMod  as UserMod
				, IDTemporada
			From MASERVICIOS_DET_RANGO_APT c 
			Where 
				--20121024-I
				--	c.IDServicio_Det In 
				--(Select IDServicio_Det From MASERVICIOS_DET 
				--	Where IDServicio_Det=@IDServicio_DetCopia
				--	And IDServicio=@IDServicio
				--	)
				c.IDServicio_Det=@IDServicio_DetCopia 
				--20121024-F
		) as X
END
