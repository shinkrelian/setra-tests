﻿Create Procedure [dbo].[MATIPOPROVEEDOR_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(30)
As
	Set NoCount On
	
	Select Descripcion	
	From MATIPOPROVEEDOR
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDTipoProv <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
