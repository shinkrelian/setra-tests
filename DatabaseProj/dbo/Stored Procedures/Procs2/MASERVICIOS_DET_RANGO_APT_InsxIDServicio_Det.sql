﻿

--PPMG-20151105-Se agrego el insertar MASERVICIOS_DET_DESC_APT
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_InsxIDServicio_Det]
	@IDServicio_Det int,
	@IDServicio_DetNuevo int
AS
BEGIN
	Set NoCount On

	Insert into MASERVICIOS_DET_DESC_APT(IDServicio_Det, IDTemporada, DescripcionAlterna, UserMod, FecMod)
	SELECT @IDServicio_DetNuevo as [IDServicio_Det]
		  ,IDTemporada
		  ,DescripcionAlterna
		  ,[UserMod]
		  ,[FecMod]
	FROM [dbo].[MASERVICIOS_DET_DESC_APT]
	Where IDServicio_Det =@IDServicio_Det 

	Insert into MASERVICIOS_DET_RANGO_APT(Correlativo, IDServicio_Det, PaxDesde, PaxHasta, Monto, UserMod, FecMod, IDTemporada)
	SELECT [Correlativo]
		  ,@IDServicio_DetNuevo as [IDServicio_Det]
		  ,[PaxDesde]
		  ,[PaxHasta]
		  ,[Monto]
		  --,[Mig]
		  ,[UserMod]
		  ,[FecMod]
		  ,[IDTemporada]
	  FROM [dbo].[MASERVICIOS_DET_RANGO_APT]
	Where IDServicio_Det =@IDServicio_Det 
END
