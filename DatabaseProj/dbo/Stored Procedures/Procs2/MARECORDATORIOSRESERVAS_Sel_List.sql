﻿CREATE PROCEDURE [dbo].[MARECORDATORIOSRESERVAS_Sel_List]
 @IDRecordatorio smallint,
 @Descripcion varchar(200)
 as
 begin
	set nocount on
	select IDRecordatorio,Descripcion,Dias,Activo from dbo.MARECORDATORIOSRESERVAS
	Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')  
    and (IDRecordatorio=@IDRecordatorio Or @IDRecordatorio=0)	
 end
