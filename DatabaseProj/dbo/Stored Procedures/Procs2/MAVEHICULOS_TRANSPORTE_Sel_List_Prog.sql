﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JHD-20150420 - Se quito el filtro de tipo de vehiculo
--JHD-20150422 - Se agrego descripcion de rangos
--JHD-20150425 - Se agrego parametro @Tipo para filtrar por mercado
--JHD-20150427 - Se agrego parametro @IDCliente
--JHD-20150427 - and ((@tipo in ('GEB','GER') and @IDCliente='000160' AND v.CoMercado=2) or (@tipo='ASIA' AND v.CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and v.CoMercado is null))		   
--JHD-20150428 - and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND v.CoMercado=2) or (@tipo='ASIA' AND v.CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and v.CoMercado is null))		   
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505- AND (up.IDubigeo =@CoUbigeo or rtrim(ltrim(@CoPais))='') 
CREATE Procedure [dbo].[MAVEHICULOS_TRANSPORTE_Sel_List_Prog]
@NoVehiculoCitado varchar(200)='',
@CoUbigeo char(6)='',
@Tipo varchar(7)='',
@IDCliente char(6)='' ,
@CoPais char(6)='' 
as
begin
SELECT        v.NuVehiculo as NuVehiculoProg,
		      v.NoVehiculoCitado +' ('+cast(isnull(v.QtDe,0) as varchar(2)) +' - '+cast(isnull(v.QtHasta,0) as varchar(2))  +') Pax  - Cap. '+ case when Cast(isnull(QtCapacidad,'') as Varchar(2)) IN ('','0') then 'TBA' ELSE Cast(isnull(QtCapacidad,'') as Varchar(2)) END  as NoUnidadProg,
				isnull(v.QtCapacidad,0) as QtCapacidadPax
				--,v.coubigeo
		--m.Descripcion AS Mercado
FROM            MAVEHICULOS_TRANSPORTE AS v left JOIN
                         MAMERCADO AS m ON m.CoMercado = v.CoMercado left JOIN
                         MAUBIGEO AS u ON u.IDubigeo = v.CoUbigeo left JOIN
                         MAUBIGEO AS up ON u.IDPais = up.IDubigeo
WHERE        (rtrim(ltrim(@NoVehiculoCitado))='' or rtrim(ltrim(v.NoVehiculoCitado)) like '%'+rtrim(ltrim(@NoVehiculoCitado))+'%') AND
			 (u.IDubigeo =@CoUbigeo or rtrim(ltrim(@CoUbigeo))='') --AND (v.CoTipo = 1) 
			 and flActivo=1
			 and FlProgramacion=1
			-- and ((@tipo in ('GEB','GER') AND v.CoMercado=2) or (@tipo='ASIA' AND v.CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB','GER') and v.CoMercado is null))		   
			 and ((@tipo in ('GEB','GER','') and @IDCliente='000160' AND v.CoMercado=2) or (@tipo='ASIA' AND v.CoMercado=1) OR  (@tipo NOT IN ('ASIA','GEB'/*,'GER'*/) and @IDCliente<>'000160' and v.CoMercado is null))		   
			 AND (up.IDubigeo =@CoPais or rtrim(ltrim(@CoPais))='') 
end

