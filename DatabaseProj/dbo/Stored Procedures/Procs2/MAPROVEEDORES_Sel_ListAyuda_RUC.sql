﻿
-----------------------------------------------------------------------
--create
CREATE Procedure [dbo].[MAPROVEEDORES_Sel_ListAyuda_RUC]    
 @IDTipoProv char(3),    
 @RazonSocial varchar(100),    
 @IDCiudad char(6)='',    
 @IDCat char(3),    
 @Categoria tinyint,
 @NumIdentidad varchar(15)=''  
As    
 Set NoCount On    
    
 SELECT Distinct * FROM    
 (    
 Select     
 --Case When @IDTipoProv='001' Then p.NombreCorto Else p.RazonSocial End as Descripcion,    
 p.NombreCorto as Descripcion,    
 p.IDProveedor as Codigo,     
 p.IDTipoProv as CodTipo, t.Descripcion as DescTipo, p.IDCiudad,u.Descripcion as DescCiudad, 
 u.IDPais, IsNull(p.Margen,0) as Margen,
 Isnull(pInt.NombreCorto,'') as OperadorInt    
 From MAPROVEEDORES p Left Join MATIPOPROVEEDOR t On p.IDTipoProv=t.IDTipoProv    
 Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo    
 Left Join MASERVICIOS s On p.IDProveedor=s.IDProveedor    
 Left Join MAPROVEEDORES pInt On p.IDProveedorInternacional = pInt.IDProveedor
 Where     
 (p.IDTipoProv=@IDTipoProv Or LTRIM(rtrim(@IDTipoProv))='' )  
 And (p.IDCiudad=@IDCiudad Or LTRIM(rtrim(@IDCiudad))='')    
 And (s.IDCat=@IDCat Or LTRIM(rtrim(@IDCat))='')    
 And (s.Categoria=@Categoria OR @Categoria=0) And p.Activo = 'A'  
 and (p.NumIdentidad Like '%'+@NumIdentidad+'%' Or LTRIM(rtrim(@NumIdentidad))='')    
 ) AS X    
 Where (Descripcion Like '%'+@RazonSocial+'%' Or LTRIM(rtrim(@RazonSocial))='')    
 Order by Descripcion    

