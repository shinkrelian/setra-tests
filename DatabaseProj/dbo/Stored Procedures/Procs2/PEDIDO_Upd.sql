﻿--JRF-20160423-Case When FlCreadoDsdAddIn=1 Then PEDIDO.CoEjeVta...
CREATE Procedure dbo.PEDIDO_Upd
	@NuPedInt int,
	@CoCliente char(6) ,
	@FePedido smalldatetime ,
	@CoEjeVta char(4),
	@TxTitulo varchar(100),
	@CoEstado char(2),
	@UserMod char(4)
As
	Set Nocount on

	Update PEDIDO 
	Set CoCliente=@CoCliente,
	FePedido=@FePedido,
	CoEjeVta=Case When FlCreadoDsdAddIn=1 Then PEDIDO.CoEjeVta Else @CoEjeVta End,
	TxTitulo=@TxTitulo,
	CoEstado=@CoEstado,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where NuPedInt=@NuPedInt
