﻿--HLF-20120919-parametro aumenta a varchar(7)  
--HLF-20121003-agregando idTipoOC as OperacContable  
--HLF-20130314-agregando And Estado='A'  
--HLF-20130320-Agregando campo VerDetaTipo
--JRF-20150325-Nuevos campos [FlEsViaticosNac ,FlEsViaticosInt ,FlEsTarjetaTelefonica ,FlEsTaxi]
--HLF-20150717-@IDDet int para casos de Acomodos de Vehiculos
--JRF-20150828-Agregar campos nuevos.
--JRF-20151110-Agregar campo CoTipoCostoTC
CREATE Procedure [dbo].[MASERVICIOS_Det_Sel_xIDServicioAnio]  
 @IDServicio char(8),  
 @Anio char(4),  
 @Tipo varchar(7),
 @IDDet int
As  
  
Set NoCount On  
   
If @IDDet=0
	 Select IDServicio_Det, Monto_sgl,Monto_dbl,Monto_tri,PoliticaLiberado,TipoLib,Liberado,  
	 MontoL,MaximoLiberado,Afecto, ConAlojamiento, DiferSS, DiferST, TC, IDServicio_Det,  
	 Monto_sgls, Monto_dbls, Monto_tris, idTipoOC as OperacContable, VerDetaTipo,
	 FlEsViaticosNac ,FlEsViaticosInt ,FlEsTarjetaTelefonica ,FlEsTaxi,
	 FlEsHotelNocheAntesDesp,FlEsVuelo,FlEsHonorario,FlEsTicketAereros,FlEsIGVHoteles,FlEsOtros,FlEsGEBADS_PrimerDia,FlEsGEBADS_UltimoDia
	 ,CoTipoCostoTC
	 From MASERVICIOS_DET 
	 Where IDServicio=@IDServicio And   
	 Anio=@Anio And  
	 Tipo=@Tipo  
	 And Estado='A'
Else
	 Select sd.IDServicio_Det, Monto_sgl,Monto_dbl,Monto_tri,PoliticaLiberado,TipoLib,Liberado,  
	 MontoL,MaximoLiberado,Afecto, ConAlojamiento, DiferSS, DiferST, TC, sd.IDServicio_Det,  
	 Monto_sgls, Monto_dbls, Monto_tris, idTipoOC as OperacContable, VerDetaTipo,
	 FlEsViaticosNac ,FlEsViaticosInt ,FlEsTarjetaTelefonica ,FlEsTaxi,
	 FlEsHotelNocheAntesDesp,FlEsVuelo,FlEsHonorario,FlEsTicketAereros,FlEsIGVHoteles,FlEsOtros,FlEsGEBADS_PrimerDia,FlEsGEBADS_UltimoDia
	 ,CoTipoCostoTC
	 From MASERVICIOS_DET sd Inner Join COTIDET_DETSERVICIOS cds On sd.IDServicio_Det=cds.IDServicio_Det And cds.IDDET=@IDDet
	 Where IDServicio=@IDServicio And   
	 Anio=@Anio And  
	 Tipo=@Tipo  
	 And Estado='A'	
