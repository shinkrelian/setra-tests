﻿
--------------------------------------------------------------------------------------


--alter
--[MASERVICIOS_HOTEL_Sel_List] 'LIM00057'
--JHD-20141127-Agregar el Nuevo campo de TxPisos  
CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_Sel_List]
	@CoServicio char(8)='',
	@CoConcepto tinyint=0,
	@CoTipo char(3)=''
AS
BEGIN

SELECT     CoServicio=isnull(MASERVICIOS_HOTEL.CoServicio,0),
		   MACONCEPTO_HOTEL.CoConcepto,
		   MACONCEPTO_HOTEL.TxDescripcion,
		   MACONCEPTO_HOTEL.CoTipo, 
           FlServicio=ISNULL(MASERVICIOS_HOTEL.FlServicio,CAST(0 AS BIT)),
           FlCosto=ISNULL(MASERVICIOS_HOTEL.FlCosto,CAST(0 AS BIT)),
           CoTipoMenu=ISNULL(MASERVICIOS_HOTEL.CoTipoMenu,''),
           FlFijoPortable=ISNULL(MASERVICIOS_HOTEL.FlFijoPortable,CAST(0 AS BIT)),
           FlPortable=ISNULL(MASERVICIOS_HOTEL.FlPortable,CAST(0 AS BIT)),
           CoTipoBanio=ISNULL(MASERVICIOS_HOTEL.CoTipoBanio,''),
           CoEstadoHab=ISNULL(MASERVICIOS_HOTEL.CoEstadoHab,''),
           TxPisos=ISNULL(MASERVICIOS_HOTEL.TxPisos,'')
FROM       MACONCEPTO_HOTEL left outer JOIN
           MASERVICIOS_HOTEL ON MACONCEPTO_HOTEL.CoConcepto = MASERVICIOS_HOTEL.CoConcepto
WHERE     (MASERVICIOS_HOTEL.CoServicio = @CoServicio or @CoServicio ='') AND
		  (MASERVICIOS_HOTEL.CoConcepto = @CoConcepto or @CoConcepto=0) AND
		  (MACONCEPTO_HOTEL.CoTipo = @CoTipo or @CoTipo='')

END;
