﻿Create Procedure dbo.MASERVICIOS_SelFOCxCiudad
	@CoCiudad char(6),
	--@QtPax smallint,
	@IDCab int
As
	Set Nocount On
		
	Select distinct s.IDServicio as CoServicio, s.Descripcion as DescDet, 
	--isnull((Select max(Monto) From MASERVICIOS_RANGO_APT Where IDServicio=s.IDServicio And IDTemporada=6
	--	And ((@QtPax between PaxDesde And PaxHasta) OR (@QtPax >= PaxDesde And PaxHasta=0)))
	--	,0)
	0 as SsCosto
	From MASERVICIOS s Inner Join COTIDET cd On s.IDServicio=cd.IDServicio and cd.IDCAB=@IDCab
	Where s.APTServicoOpcional='F' and s.IDubigeo=@CoCiudad
	Order by 2

