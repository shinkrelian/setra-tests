﻿Create Procedure [dbo].[RESERVAS_DET_PAX_Udp]
@IDReserva_Det int,
@IDPax int,
@UserMod char(4)
as
Begin
	Set NoCount On
	Update RESERVAS_DET_PAX 
		Set  
		    IDPax= @IDPax
		   ,UserMod= @UserMod
		   ,FecMod = GetDate()
		Where IDReserva_Det=@IDReserva_Det
End
