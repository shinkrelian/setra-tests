﻿
CREATE PROCEDURE [dbo].[OPERACIONES_DET_SelxFormaEgreso]                          
	@IDVoucher varchar(14),
	@IDCab int
AS
BEGIN
	DECLARE @IDProveedor_Zicasso char(6) = '003557',
			@IDServicio_Zicasso char(8)	= 'LAX00002',
			@Servicio_Det_Zicasso CHAR(4) = 'ZIC6'
	DECLARE @Tbl_ServDet Table(IDProveedorX char(6),
							   IDServicioX char(8),
							   IDServicio_DetX int);
	Set NoCount On
	INSERT INTO @Tbl_ServDet(IDProveedorX, IDServicioX, IDServicio_DetX)
	SELECT S.IDProveedor, D.IDServicio, D.IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
	WHERE TipoUsoSistema = @Servicio_Det_Zicasso AND S.IDProveedor = @IDProveedor_Zicasso AND D.IDServicio = @IDServicio_Zicasso
	SET NOCOUNT ON

	 SELECT 0 AS IDReserva_Det, 0 AS IDOperacion_Det, 0 AS IDReserva, 0 AS IDOperacion, C.IDFile, D.IDDet, D.Item, D.Dia,                                                              
		CASE WHEN PR.IDTipoProv = '001' Then                                                            
				UPPER(SUBSTRING(DATENAME(dw,D.Dia),1,3)) + ' ' + LTRIM(RTRIM(CONVERT(varchar,D.Dia,103)))
			ELSE
				UPPER(SUBSTRING(DATENAME(dw,D.Dia),1,3)) + ' ' + LTRIM(RTRIM(CONVERT(varchar,D.Dia,103)))
		END AS DiaFormat,
		ISNULL(C.FechaOut,'') AS FechaOut,
		ISNULL(UPPER(SUBSTRING(DATENAME(dw,C.FechaOut),1,3)) + ' ' + LTRIM(RTRIM(CONVERT(varchar,C.FechaOut,103))),'') AS FechaOut,
		CASE WHEN PR.IDTipoProv = '001' AND SD.PlanAlimenticio = 0 THEN '' ELSE CONVERT(varchar(5),D.Dia,108) END AS Hora,
		UB.Descripcion AS Ubigeo, 0 AS Noches, PR.IDTipoProv, S.Dias, D.IDProveedor, PR.NombreCorto AS Proveedor,
		ISNULL(PR.Email3,'') AS CorreoProveedor, UB.IDPais, D.IDubigeo, 0 AS Cantidad, D.NroPax,
		CAST(D.NroPax AS VARCHAR(3)) + CASE WHEN ISNULL(D.NroLiberados,0) = 0 THEN '' ELSE '+' + CAST(D.NroLiberados AS VARCHAR(3)) END AS PaxmasLiberados,
		D.IDServicio, D.IDServicio_Det, D.Servicio As DescServicioDet,
		CASE WHEN PR.IDTipoProv = '001' OR PR.IDTipoProv = '002' Then  --hoteles o restaurantes
			SD.Descripcion
		ELSE
			S.Descripcion END AS DescServicioDetBup,
		D.Especial, D.MotivoEspecial, ISNULL(D.RutaDocSustento,'') AS RutaDocSustento, D.Desayuno, D.Lonche, D.Almuerzo, D.Cena,
		D.Transfer, D.IDDetTransferOri, D.IDDetTransferDes, D.CamaMat, D.TipoTransporte, D.IDUbigeoOri,
		UO.Descripcion + '          |' + ISNULL(UO.Codigo,'') AS DescUbigeoOri,
		D.IDUbigeoDes, UD.Descripcion + '          |' + ISNULL(UD.Codigo,'') AS DescUbigeoDes,
		IDIO.IDidioma AS CodIdioma, CASE WHEN S.IDTipoServ = 'NAP' THEN '---' ELSE S.IDTipoServ END AS IDTipoServ,
		IDIO.Siglas AS Idioma, SD.Anio, SD.Tipo, ISNULL(SD.DetaTipo,'') AS DetaTipo, 0 AS CantidadAPagar,                                              
		D.NroLiberados, D.Tipo_Lib, D.CostoRealAnt, D.Margen, D.MargenAplicado, D.MargenAplicadoAnt, D.MargenLiberado,
		D.TotalOrig, D.CostoRealImpto, D.CostoLiberadoImpto, D.MargenImpto, D.MargenLiberadoImpto, D.TotImpto,
		'0' AS IDDetRel, D.Total, '' AS IDVoucher, '0' AS IDReserva_DetCopia, '0' AS IDReserva_Det_Rel,
		'' AS IDEmailEdit, '' AS IDEmailNew, '' AS IDEmailRef, '' as ObservVoucher, '' AS ObservBiblia,
		'' AS ObservInterno, 0 as CapacidadHab, 0 As EsMatrimonial, SD.PlanAlimenticio, D.IncGuia,
		0 AS chkServicioExportacion, 0 AS VerObserVoucher, 0 AS VerObserBiblia, D.CostoReal, D.CostoLiberado,
		0 AS NetoHab, 0 AS IgvHab, 0 AS TotalHab, 0 AS NetoGen, 0 AS IgvGen, 'USD' AS IDMoneda,'$' AS SimboloMoneda,
		ISNULL(SD.TC,0) AS TipoCambio, D.Total AS TotalGen, '' AS IDGuiaProveedor, 0 As DescBus, SD.idTipoOC,
		0 AS InactivoDet, 0 AS ServHotelAlimentPuro, SD.ConAlojamiento, D.SSCR, dbo.FnPNRxIDCabxDia(D.IDCab,D.Dia) AS PNR,
		SD.CtaContable, ISNULL(D.ServicioEditado,CAST(0 as bit)) AS ServicioEditado, 0 AS ServicioVarios
	FROM COTICAB C INNER JOIN COTIDET D ON C.IDCAB = D.IDCAB
		INNER JOIN MAUBIGEO UB ON D.IDubigeo = UB.IDubigeo
		LEFT OUTER JOIN MAUBIGEO UO ON D.IDUbigeoOri = UO.IDubigeo
		LEFT OUTER JOIN MAUBIGEO UD ON D.IDUbigeoDes = UD.IDubigeo
		INNER JOIN MAPROVEEDORES PR ON D.IDProveedor = PR.IDProveedor
		INNER JOIN MASERVICIOS_DET SD ON D.IDServicio_Det = SD.IDServicio_Det
		INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
		INNER JOIN MAIDIOMAS IDIO ON D.IDIdioma = IDIO.IDidioma
		INNER JOIN @Tbl_ServDet T ON D.IDServicio_Det = T.IDServicio_DetX AND D.IDProveedor = T.IDProveedorX AND D.IDServicio = T.IDServicioX
	WHERE C.IDCab=@IDCab
	Order By D.Item Asc    
END
