﻿--JHD-20150901-AccountCode='42211009',
--JHD-20150901-CostingCode2='000000',
--JHD-20150910-CostingCode2=UnitPrice=abs(isnull(opd.Total_Final,0)),
--JHD-20150910-CostingCode2=LineTotal=abs(isnull(opd.Total_Final,0)),
CREATE PROCEDURE [dbo].[ORDENPAGO_Sel_List_SAP_Lines]
@IDOrdPag int
AS
BEGIN
select DISTINCT
ItemCode= '', --isnull((select cosap from maproductos where coproducto= dd.CoProducto),''),
ItemDescription='',
WarehouseCode='',
Quantity=1,
DiscountPercent=0,
--AccountCode=isnull(sd.CtaContable,''),
AccountCode='42211009',
--UnitPrice=abs(isnull(opd.Total,0)),
UnitPrice=abs(isnull(opd.Total_Final,0)),
--LineTotal=abs(isnull(opd.Total,0)),
LineTotal=abs(isnull(opd.Total_Final,0)),
TaxCode='EXE_IGV',--case when isnull(d.SsIGV,0)=0 then 'EXE_IGV' else 'IGV' end,
TaxTotal=0,--abs(isnull(d.SsIGV,0)),
WTLiable=cast(0 as bit),--case when (d.CoTipoDoc='RH' AND d.CoTipoOC='007') then cast(1 as bit) else cast(0 as bit) end,
TaxOnly=cast(0 as bit),
ProjectCode='',
CostingCode=isnull(sd.CoCeCos,''),
--CostingCode2='999999',
CostingCode2='000000',
CostingCode3=isnull((select idfile from coticab where idcab=r.idcab),'00000000'),
CostingCode4='',
CostingCode5='',
U_SYP_CONCEPTO=opd.IDOrdPag_Det--dd.NuDocumProvDet
from ordenpago op
left join ORDENPAGO_DET opd on op.IDOrdPag=opd.IDOrdPag
left join RESERVAS_DET rd on  opd.IDReserva_Det=rd.IDReserva_Det
left join RESERVAS r on r.IDReserva=op.IDReserva and r.IDReserva=rd.IDReserva
left join MAPROVEEDORES p on p.IDProveedor=r.IDProveedor
left join MAMONEDAS m on m.IDMoneda=op.IDMoneda
left join MASERVICIOS_DET sd on sd.IDServicio_Det=rd.IDServicio_Det

where opd.IDOrdPag=@IDOrdPag
END;
