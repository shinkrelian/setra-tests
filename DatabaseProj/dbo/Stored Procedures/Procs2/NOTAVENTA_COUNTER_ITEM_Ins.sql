﻿ 
--JHD-20150116- Se agregó el campo CoCliente

CREATE procedure [dbo].[NOTAVENTA_COUNTER_ITEM_Ins]
		   @NuNtaVta char(6),
           
           @CoTipoServicio char(3)='',
           @TxDetalle varchar(150)='',
           @CoProveedor char(6)='',
           @QtCantidad smallint=0,
           
           @SSUnidad numeric(10,2),
           @SSTotal numeric(10,2),
           @UserMod char(4),
           @CoCliente char(6)=''
as
BEGIN
Set NoCount On
	Declare @NuItem tinyint =(select Isnull(Max(NuItem),0)+1 
								from NOTAVENTA_COUNTER_ITEM where NuNtaVta =@NuNtaVta)

INSERT INTO [NOTAVENTA_COUNTER_ITEM]
           ([NuNtaVta]
           ,[NuItem]
                    
           ,[CoTipoServicio]
           ,[TxDetalle]
           ,[CoProveedor]
           ,[QtCantidad]
           --,[CoMoneda]
           --,[SSTipoCambio]
           
           ,SSUnidad
           ,SSTotal
          
           ,[UserMod]
           ,CoCliente)
     VALUES
           (@NuNtaVta
           ,@NuItem
         
           ,@CoTipoServicio 
		   ,@TxDetalle
		   --,@CoProveedor
		    ,case When ltrim(rtrim(@CoProveedor))='' Then Null Else @CoProveedor End
		   ,@QtCantidad
		   --,@CoMoneda
		   --,@SSTipoCambio 
           
           ,@SSUnidad
           ,@SSTotal
           ,@UserMod
           ,case When ltrim(rtrim(@CoCliente))='' Then Null Else @CoCliente End
           )
END

