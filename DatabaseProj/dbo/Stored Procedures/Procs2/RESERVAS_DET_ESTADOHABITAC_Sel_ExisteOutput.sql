﻿
--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet  
--HLF-20130617-Cambio Where a Where IDDet = @IDDet
--HLF-20130618-Agregando parametro @NuIngreso
CREATE Procedure [dbo].[RESERVAS_DET_ESTADOHABITAC_Sel_ExisteOutput]  
 @IDDet int,  
 @NuIngreso tinyint,
 @pExiste bit Output  
As  
 Set Nocount On  
   
 Set @pExiste=0  
 If Exists(Select IDDet From RESERVAS_DET_ESTADOHABITAC   
  --Where IDReserva_Det=@IDReserva_Det)  
  Where IDDet = @IDDet And NuIngreso=@NuIngreso)
  --(Select IDDet From RESERVAS_DET Where IDReserva_Det=@IDReserva_Det))  
  Set @pExiste=1  
