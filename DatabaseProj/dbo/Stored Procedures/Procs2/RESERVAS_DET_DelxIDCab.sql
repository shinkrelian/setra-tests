﻿Create Procedure dbo.RESERVAS_DET_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM RESERVAS_DET 
	WHERE IDReserva IN 
		(SELECT IDReserva FROM RESERVAS WHERE IDCab = @IDCab)
		
