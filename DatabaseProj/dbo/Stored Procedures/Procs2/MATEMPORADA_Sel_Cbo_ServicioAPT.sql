﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_Cbo_ServicioAPT]
					@IDServicio char(8)
AS
BEGIN
	Set NoCount On  
	SELECT T.IDTemporada, T.NombreTemporada + CASE WHEN ISNULL(A.NR, 0) = 0 THEN '' ELSE ' [ X ]' END AS NombreTemporada
	FROM MATEMPORADA AS T LEFT OUTER JOIN (SELECT D.IDTemporada, COUNT(R.Correlativo) AS NR
                                  FROM MASERVICIOS_DESC_APT AS D INNER JOIN
                                  MASERVICIOS_RANGO_APT AS R ON D.IDServicio = R.IDServicio AND D.IDTemporada = R.IDTemporada
								  WHERE D.IDServicio = @IDServicio
                                  GROUP BY D.IDTemporada) AS A ON T.IDTemporada = A.IDTemporada
END
