﻿

 -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--HLF-20130916-Agregando pinternac.NombreCorto as DescProvInternac,
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-AND (U.IDPais =@CoPais or rtrim(ltrim(@CoPais))='')
CREATE Procedure [dbo].[MAPROVEEDORES_Sel_List]    
 @IDProveedor char(6),    
 @NumIdentidad varchar(15),    
 @RazonSocial varchar(100),    
 @IDTipoProv char(3),    
 @RazonComercial varchar(100),    
 @IDCiudad char(6),  
 @Guia_Nacionalidad char(6) = '',
 @CoPais char(6)='' 
AS    
 Set NoCount On    
  
 Select Distinct c.NombreCorto, c.RazonSocial, 
 pinternac.NombreCorto as DescProvInternac,
 tp.Descripcion as TipoProv,tp.PideIdioma,    
 c.IDTipoProv,u.IDPais,c.IDCiudad,IsNull(c.IDTipoOper,'') as IDTipoOper,  
 IsNull(c.Margen,0) as Margen,  
 c.IDProveedor     
 From MAPROVEEDORES c     
 Left Join MATIPOPROVEEDOR tp On c.IDTipoProv=tp.IDTipoProv    
 --Left Join MASERVICIOS s On s.IDProveedor=c.IDProveedor    
 Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo    
 LEft Join MAPROVEEDORES pinternac On c.IDProveedorInternacional=pinternac.IDProveedor
 Where     
 (c.IDProveedor=@IDProveedor Or ltrim(rtrim(@IDProveedor))='') And    
 (c.NumIdentidad LIKE '%'+@NumIdentidad+'%' Or ltrim(rtrim(@NumIdentidad))='') And    
 (c.RazonSocial Like '%'+@RazonSocial+'%' Or ltrim(rtrim(@RazonSocial))='') And    
 (c.IDTipoProv=@IDTipoProv Or ltrim(rtrim(@IDTipoProv))='') And     
 --(s.Descripcion Like '%'+@DescServicio+'%' Or ltrim(rtrim(@DescServicio))='')    
 (c.NombreCorto Like '%'+@RazonComercial+'%' Or ltrim(rtrim(@RazonComercial))='')     
 And (c.IDCiudad=@IDCiudad Or ltrim(rtrim(@IDCiudad))='')    
 And (c.Guia_Nacionalidad=@Guia_Nacionalidad Or ltrim(rtrim(@Guia_Nacionalidad))='')    
 And c.Activo='A'     
 AND (U.IDPais =@CoPais or rtrim(ltrim(@CoPais))='') 
 Order by c.IDTipoProv,NombreCorto, RazonSocial    
  

