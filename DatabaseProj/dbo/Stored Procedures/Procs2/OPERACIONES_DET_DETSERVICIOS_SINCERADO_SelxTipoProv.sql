﻿
--HLF-20140812-COUNT(*) as CantServ .. Group by ..
--isnull((Select top 1 ps1.NuPreSob From PRESUPUESTO_SOBRE_DET pd1 ...	
--HLF-20140813-Or LTRIM(rtrim(@IDTipoProv))='') 
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SINCERADO_SelxTipoProv  
 @IDCab int,          
 @IDProveedor char(6),  
 @IDTipoProv char(3)  
 --@TodosServicios bit  
As          
	Set Nocount On           
 
	Select ods.IDProveedor_Prg, COUNT(*) as CantServ, 
	--isnull(ps.NuPreSob,0) as NuPreSob  
	isnull((Select top 1 ps1.NuPreSob From PRESUPUESTO_SOBRE_DET pd1 
		Inner Join PRESUPUESTO_SOBRE ps1 On ps1.NuPreSob=pd1.NuPreSob
		Where ps1.IDCab=o.IDCab And 
		pd1.CoPrvPrg=ods.IDProveedor_Prg And ps1.IDProveedor=o.IDProveedor),0) as NuPreSob  
	From  OPERACIONES_DET od            
	Inner Join OPERACIONES_DET_DETSERVICIOS ods On 
	od.IDOperacion_Det=ods.IDOperacion_Det          
	And od.IDServicio=ods.IDServicio And ods.Activo=1          
	Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab           
	And o.IDProveedor=@IDProveedor     
	--Left Join SINCERADO sn On sn.IDOperacion_Det=od.IDOperacion_Det             
	Left Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor   
	--and p.IDTipoProv in('005','008')  

	--Left Join PRESUPUESTO_SOBRE ps On o.IDCab=ps.IDCab And 
	--ps.CoPrvGui=ods.IDProveedor_Prg And ps.IDProveedor=o.IDProveedor  

	Where not ods.IDProveedor_Prg is null And  
	(p.IDTipoProv = @IDTipoProv Or LTRIM(rtrim(@IDTipoProv))='') 
	--And (@TodosServicios=1 Or not ods.IDProveedor_Prg is null)  
	--Order by ods.FecMod
	Group by ods.IDProveedor_Prg, o.IDCab, o.IDProveedor
	Order by CantServ desc
