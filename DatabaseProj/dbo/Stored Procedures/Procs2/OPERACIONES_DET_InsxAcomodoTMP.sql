﻿--HLF-20131126-Actualizando a Where  IDReserva_Det=@IDReserva_DetTMP y comentando parametros    
--JRF-20150415-Actualizando el Item de Costos TC
CREATE Procedure dbo.OPERACIONES_DET_InsxAcomodoTMP           
 @IDOperacion_DetTMP int,  
 @UserMod char(4),    
 @pIDOperacion_Det int output    
As      
 Set Nocount On      
       
 Declare @IDOperacion_Det int                    
    Execute dbo.Correlativo_SelOutput 'OPERACIONES_DET',1,10,@IDOperacion_Det output      
      
       
 Insert Into OPERACIONES_DET            
  (IDOperacion_Det, IDOperacion, IDReserva_Det, IDVoucher,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,            
  IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,            
  IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,            
  Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,            
  CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,IDTipoOC,UserMod,          
  NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,RegeneradoxAcomodo,IDOperacion_DetAcomodoTMP,FlServicioNoShow,FlServicioTC,IDMoneda)         
        
 Select       
 @IDOperacion_Det,       
 odt.IDOperacion, odt.IDReserva_Det, odt.IDVoucher,odt.Item,odt.Dia,odt.DiaNombre,odt.FechaOut,odt.Noches,odt.IDubigeo,odt.IDServicio,            
  odt.IDServicio_Det,odt.Servicio,odt.IDIdioma,odt.Especial,odt.MotivoEspecial,odt.RutaDocSustento,odt.Desayuno,odt.Lonche,odt.Almuerzo,odt.Cena,odt.Transfer,            
  odt.IDDetTransferOri,odt.IDDetTransferDes,odt.TipoTransporte,odt.IDUbigeoOri,odt.IDUbigeoDes,odt.NroPax,odt.NroLiberados,odt.Tipo_Lib,            
  odt.Cantidad,odt.CantidadAPagar,odt.CostoReal,odt.CostoRealAnt,odt.CostoLiberado,odt.Margen,odt.MargenAplicado,odt.MargenAplicadoAnt,odt.MargenLiberado,odt.Total,odt.TotalOrig,            
  odt.CostoRealImpto,odt.CostoLiberadoImpto,odt.MargenImpto,odt.MargenLiberadoImpto,odt.TotImpto,odt.IDTipoOC,@UserMod,          
  odt.NetoHab,odt.IgvHab,odt.TotalHab,odt.NetoGen,odt.IgvGen,odt.TotalGen,1,odt.IDOperacion_Det,(select FlServicioNoShow from RESERVAS_DET where IDReserva_Det=odt.IDReserva_Det and Anulado=0) FlServicioNoShow,
  Case When odt.IDServicio='LIM00918' Then 1 Else 0 End as ServicioTC,odt.IDMoneda
       
 From OPERACIONES_DET_TMP odt Left Join RESERVAS_DET_TMP rdd On odt.IDReserva_Det=rdd.IDReserva_Det      
 Where odt.IDOperacion_Det=@IDOperacion_DetTMP  
  --odt.IDOperacion=@IDOperacion      
  --And odt.IDServicio_Det=@IDServicio_Det And isnull(rdd.CapacidadHab,0)=@CapacidadHab      
  --And isnull(rdd.EsMatrimonial,0)=@EsMatrimonial       
  --And odt.Dia=@Dia      
      
  Set @pIDOperacion_Det=@IDOperacion_Det
