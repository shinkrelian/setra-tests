﻿--JRF-20121126-Agregando Nuevos Campos por Reserva                                      
--HLF-20130111-Correccion Agregando Nuevos Campos por Reserva                                      
--CantidadAPagar,NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen, sd.PlanAlimenticio ,  cd.IncGuia,                                        
--HLF-20130117-Agregando campo rd.IDTipoOC                                      
--HLF-20130221-Cambiando logica de columna DescServicioDetBup                                    
--JRF-20130304-Cambiando posicion de Columnas (Noche x Ciudad)                                  
--JRF-20130319-Cambiando posicion de Columnax (Cant.Pagar x Pax)                                
--JRF-20130425-Insertando el Campo (IDTipoServ) - MASERVICIOS                              
--JRF-20130425-Insertando el Campo (DetaTipo) - MASERVICIOS_DET                              
--HLF-20130507-Agregando CAST(rd.NroPax AS VARCHAR(3))+CAST( isnull(rd.NroLiberados,0) AS VARCHAR(3)) as PaxmasLiberados,                            
--HLF-20130508-Agregando columna isnull(IDMoneda,'') as IDMoneda,isnull(mon.Simbolo,'') as SimboloMoneda                            
--HLF-20130509-Modificando PaxmasLiberados                          
--JRF-20130618-Cambiando el campo DescGuia x un IDProveedor de Guia(IDGuiaProveedor).                       
--JRF-20130701-Cambiando el campo DescBus x un 0 (Tipo de Valor Smallint).                      
--HLF-20130813-Agregando columna PNR                   
--Reemplazando Rd.ObservVoucher por vo.ObservVoucher                  
--HLF-20130906-Agregando isnull(sd.TC,0) as TipoCambio,              
--JRF-20130910-Colocando los valores de CapacidadHab y EsMatrimonial            
--HLF-20140131-Agregando Top 1 en vez de Distinct a subquery IDGuiaProveedor        
--JRF-20140616-Agregando condicional de PlanAlimenticio para las horas.      
--JHD-20141211-Agregando IsNull(vo.CoUbigeo_Oficina,'') as CoUbigeo_Oficina      
--JRF-20150218-Agregar Columnas Nuevas de Reservas [FlServicioNoShow]   
--JHD-JRF-20150224-IsNull campos de Reservas  
--JRF-20150329-IsNuLL(Rd.FlServicioTC,0)=0
--HLF-20150406-0 as Tarifas_Anio,
--dbo.FnServicioMPWP(sd.IDServicio_Det) as IDServicio_V
--rdd2.iddet
--HLF-20150619-Nueva columna FlServicioNoCobrado
--HLF-20150622-Nueva columna SsTotalGenAntNoCobrado, QtCantidadAPagarAntNoCobrado
--PPMG-20151013--, 0 AS CantidadAPagarEditado, 0 AS NetoHabEditado, 0 AS IgvHabEditado
CREATE Procedure [dbo].[OPERACIONES_DET_SelxIDCab]                  
 @IDCab int                                                    
As                                                                
 Set NoCount On                                                          
                                                    
 Select ISNULL(rd.IDReserva_Det,0) AS IDReserva_Det,                                                    
 Rd.IDOperacion_Det,                                                    
 ISNULL(Rv.IDReserva,0) as IDReserva,                                                    
 rd.IDOperacion,                                                     
 rv.IDFile,                                                    
 --0 as IDDet,                                                    
 IsNull(rdd2.iddet,0) as iddet,
 rd.Item,                                                    
 Rd.Dia,                                                      
 Case When Pr.IDTipoProv='001' Then                                                    
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                     
 Else                                                    
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                      
 End as DiaFormat,                                                        
 IsNull(Rd.FechaOut,'') as FechaOut,                                                    
 IsNull(upper(substring(DATENAME(dw,Rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,Rd.FechaOut,103)/*+' '+ substring(convert(varchar,Rd.FechaOut,108),1,5)*/)),'') as FechaOut,                                  
 Case When Pr.IDTipoProv='001' and sd.PlanAlimenticio= 0 Then                                                     
 ''                                                  
 Else                                                     
  convert(varchar(5),rd.Dia,108)                                         
 End as Hora,                                                       
 Ub.Descripcion as Ubigeo,                                                    
 Isnull(Rd.Noches,0) as Noches,                       
 Pr.IDTipoProv,                                 
 --Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,                                                   
 s.Dias,                                                    
 rv.IDProveedor,                                                    
 Pr.NombreCorto as Proveedor,                  
 ISNull(pr.Email3,'') CorreoProveedor ,                                                    
 Ub.IDPais,                                                    
 Rd.IDubigeo,                                                    
 Rd.Cantidad,                                      
                                 
 Rd.NroPax,                              
 CAST(rd.NroPax AS VARCHAR(3))+                          
 Case When isnull(rd.NroLiberados,0)=0 then '' else '+'+CAST(rd.NroLiberados AS VARCHAR(3)) end              as PaxmasLiberados,                                   
 Rd.IDServicio,                                                    
 Rd.IDServicio_Det,                                                    
 Rd.Servicio As DescServicioDet,                                                        
                                                     
 --Rd.Servicio As DescServicioDetBup,                                                       
 Case When pr.IDTipoProv='001' or pr.IDTipoProv='002' Then  --hoteles o restaurantes                                          
 sd.Descripcion                                             
 Else                                        
 s.Descripcion                                            
 End As DescServicioDetBup,                                             
                                     
                                                     
 Rd.Especial,Rd.MotivoEspecial,rd.RutaDocSustento,Rd.Desayuno,Rd.Lonche,Rd.Almuerzo,Rd.Cena,                                                    
 Rd.Transfer,Rd.IDDetTransferOri,Rd.IDDetTransferDes,0 as CamaMat,Rd.TipoTransporte,                                                    
 Rd.IDUbigeoOri,                                                    
 uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                      
 Rd.IDUbigeoDes,                                                    
 ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                                      
 idio.IDidioma as CodIdioma ,          
 Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,          
 idio.Siglas as Idioma ,          
 sd.Anio,          
 sd.Tipo,                                                     
 Isnull(sd.DetaTipo,'') As DetaTipo ,                                          
                               
 Rd.CantidadAPagar,                                      
                                 
 Rd.NroLiberados,Rd.Tipo_Lib,Rd.CostoRealAnt,Rd.Margen,                                                    
 Rd.MargenAplicado,Rd.MargenAplicadoAnt,Rd.MargenLiberado,Rd.TotalOrig,Rd.CostoRealImpto,Rd.CostoLiberadoImpto                                              
 ,Rd.MargenImpto,Rd.MargenLiberadoImpto,Rd.TotImpto,                                                    
             
     
 '0' as IDDetRel ,                            
                             
 Rd.Total,                           
 Case When Cast(Rd.IDVoucher as varchar(10))='0' Then '' Else Cast(Rd.IDVoucher as varchar(10)) End as IDVoucher,                                         
 '0' as IDReserva_DetCopia ,'0' as IDReserva_Det_Rel,                                              
                                         
 '' as IDEmailEdit, '' as IDEmailNew, '' as IDEmailRef,                                                    
 --IsNull(Rd.FechaRecordatorio,'01/01/1900') as FechaRecordatorio, IsNull(Rd.Recordatorio,'') as Recordatorio,                                                   
 --IsNull(Rd.ObservVoucher,'') as ObservVoucher,                   
 IsNull(vo.ObservVoucher,'') as ObservVoucher,                   
 IsNull(Rd.ObservBiblia,'') as ObservBiblia,                                                     
 IsNull(Rd.ObservInterno,'') as ObservInterno  , isnull(rdd.CapacidadHab,0) as CapacidadHab ,     
 isnull(rdd.EsMatrimonial,0) As EsMatrimonial   ,                                               
 sd.PlanAlimenticio ,      
 isnull(cd.IncGuia,0) as IncGuia,                                        
 vo.ServExportac As chkServicioExportacion , Rd.VerObserVoucher ,Rd.VerObserBiblia  ,                                                       
                                       
 Rd.CostoReal, Rd.CostoLiberado,                                      
  Rd.NetoHab,Rd.IgvHab,Rd.TotalHab,Rd.NetoGen,Rd.IgvGen,                            
  rd.IDMoneda as IDMoneda,                            
  mon.Simbolo as SimboloMoneda,                            
  isnull(sd.TC,0) as TipoCambio,              
  Rd.TotalGen                                       
 ,                
 Case When Pr.IDTipoOper = 'I' Then                 
 Isnull((select Top 1 IDProveedor_Prg                 
 from OPERACIONES_DET_DETSERVICIOS odd2 Left Join MAPROVEEDORES pr On odd2.IDProveedor_Prg = pr.IDProveedor                
 where pr.IDTipoProv = '005' and odd2.IDOperacion_Det = rd.IDOperacion_Det Order by odd2.FecMod Desc ),'')                
 Else Isnull(Rd.IDGuiaProveedor,'') End As IDGuiaProveedor,                
 0 As DescBus,                                      
 rd.idTipoOC                          
                                     
 ,0 as InactivoDet                                            
 ,0 AS ServHotelAlimentPuro,                          
 sd.ConAlojamiento, cd.SSCR,                      
 dbo.FnPNRxIDCabxDia(@IDCab,Rd.Dia) as PNR                  
,IsNull(cd.AcomodoEspecial,0) as AcomodoEspecial,IsNull(rdd.FlServicioParaGuia,0) as FlServicioParaGuia,Rd.FlServicioNoShow    
, IsNull(vo.CoUbigeo_Oficina,'') as CoUbigeo_Oficina,
0 as Tarifas_Anio,
dbo.FnServicioMPWP(sd.IDServicio_Det) as IDServicio_V,
rd.FlServicioNoCobrado, isnull(rd.SsTotalGenAntNoCobrado,0) as SsTotalGenAntNoCobrado, isnull(rd.QtCantidadAPagarAntNoCobrado,0) as QtCantidadAPagarAntNoCobrado
 , Rd.CantidadAPagarEditado, Rd.NetoHabEditado, Rd.IgvHabEditado
 From OPERACIONES Rv                                                     
 Left Join OPERACIONES_DET Rd On Rd.IDOperacion=Rv.IDOperacion                                                    
 Left Join MAUBIGEO Ub On Rd.IDubigeo=Ub.IDubigeo                                                           
 Left Join MAUBIGEO uo On Rd.IDUbigeoOri=uo.IDubigeo                                                      
 Left Join MAUBIGEO ud On Rd.IDUbigeoDes=ud.IDubigeo                                                       
 Left Join MAPROVEEDORES Pr On  Pr.IDProveedor= Rv.IDProveedor                                                          
 Left Join MASERVICIOS_DET sd On  Rd.IDServicio_Det=sd.IDServicio_Det                                                    
 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio                                                    
 Inner Join VOUCHER_OPERACIONES vo On Rd.IDVoucher= vo.IDVoucher                                                  
 Left Join RESERVAS_DET rdd On Rd.IDReserva_Det=rdd.IDReserva_Det And (IsNull(Rd.IDReserva_Det,0)=0)
 Left Join RESERVAS_DET rdd2 On Rd.IDReserva_Det=rdd2.IDReserva_Det 
 Left Join COTIDET cd On rdd.IDDet=cd.IDDET                                        
 Left Join MAMONEDAS mon on rd.IDMoneda=mon.IDMoneda    
 Left Join MAIDIOMAS idio On Rd.IDIdioma = idio.IDidioma          
 --Where Rv.IDCab=@IDCab and IsNuLL(Rd.FlServicioTC,0)=0 
 Where Rv.IDCab=@IDCab and (IsNuLL(Rd.FlServicioTC,0)=0 Or (IsNuLL(Rd.FlServicioTC,0)=1 And Not sd.CoTipoCostoTC In ('ALM','CEN')))
Order By Rd.IDVoucher desc,Rd.Dia Asc   
