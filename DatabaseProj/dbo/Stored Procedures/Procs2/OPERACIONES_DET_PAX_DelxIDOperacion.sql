﻿CREATE Procedure [dbo].[OPERACIONES_DET_PAX_DelxIDOperacion]  
@IDOperacion int  
As  
 Set NoCount On  
   
 delete from OPERACIONES_DET_PAX   
 where IDOperacion_Det in   
 (select IDOperacion_Det from OPERACIONES_DET where IDOperacion = @IDOperacion and Not(FlServicioNoShow=1 and FlServInManualOper=1))
