﻿--JHD-20150316-  SsSaldo= Case When @SsSaldo=0 then @SsSaldo Else @SsSaldo End,   
--JHD-20150827- SsDifAceptada=Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,  
--JHD-20150914-SsDifAceptada= @SsDifAceptada ,          
CREATE PROCEDURE [dbo].[PRESUPUESTO_SOBRE_Upd_UpdxDocumento]    
	@NuPreSob int,    
	@CoEstado char(2)='',    
	@SsSaldo numeric(12,2)=0,    
	@SsDifAceptada numeric(12,2)=0,    
	@TxObsDocumento varchar(Max)='',    
	@FlDesdeOtraForEgreso bit=0,  
	@UserMod char(4)=''    
As    

	Set NoCount On    
	Update [dbo].[PRESUPUESTO_SOBRE]
	 Set CoEstadoFormaEgreso=@CoEstado,    
	  SsSaldo= Case When @SsSaldo=0 then @SsSaldo Else @SsSaldo End,    
	  --SsDifAceptada=SsDifAceptada, --Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,      
	  --SsDifAceptada=Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,      
	  SsDifAceptada= @SsDifAceptada ,      
	  TxObsDocumento=   
	 Case When @FlDesdeOtraForEgreso=0 Then  
	  Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End  
	 Else  
	  TxObsDocumento  
	 End,  
	  UserMod=@UserMod,    
	  FecMod=GETDATE(),    
	  --    
	  UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end    
	Where NuPreSob=@NuPreSob

