﻿--HLF-20130117-Agregando parametro @IDTipoOC        
--JRF-20130904-Nuevo campo IDGuiaProveedor      
--JRF-20140228-Aumento de Longitud para las columnas con numeric(12,4)  
--JRF-20150222-Agregar Nuevos campos FlServicioNoShow,FlServInManualOper  
--JHD-JRF-20150224-Agregar Campos Que no permiten Null. Y permitir el IDReservas_Det Null
--PPMG-20151014-CantidadAPagarEditado, NetoHabEditado, IgvHabEditado
CREATE Procedure [dbo].[OPERACIONES_DET_Ins]          
@IDOperacion int,          
@IDReserva_Det int,          
@Item tinyint,          
@Dia smalldatetime,          
@FechaOut smalldatetime,          
@Noches tinyint,          
@IDubigeo char(6),          
@IDServicio char(8),          
@IDServicio_Det int,          
@Especial bit,          
@MotivoEspecial varchar(200),          
@RutaDocSustento varchar(200),          
@Desayuno bit,          
@Lonche bit,          
@Almuerzo bit,          
@Cena bit,          
@Transfer bit,          
@IDDetTransferOri int,          
@IDDetTransferDes int,          
@TipoTransporte char(1),          
@IDUbigeoOri char(6),          
@IDUbigeoDes char(6),          
@IDIdioma varchar(12),          
@NroPax smallint,          
@NroLiberados smallint,          
@Tipo_Lib char(1),          
@Cantidad tinyint,          
@CantidadAPagar tinyint=0,          
@CostoReal numeric(12,4),          
@CostoRealAnt numeric (12,4),          
@CostoLiberado numeric (12,4),          
@Margen numeric(12,4),          
@MargenAplicado numeric (6,2),          
@MargenAplicadoAnt numeric (12,4),          
@MargenLiberado numeric (12,4),          
@Total numeric (8,2),          
@TotalOrig numeric (12,4),          
@CostoRealImpto numeric(12,4),          
@CostoLiberadoImpto numeric(12,4),          
@MargenImpto numeric(12,4),          
@MargenLiberadoImpto numeric(12,4),          
@TotImpto numeric (12,4),          
@Servicio varchar(max),          
@IDVoucher int,          
@ObservVoucher varchar(max),          
@ObservBiblia varchar(max),          
@ObservInterno varchar(max),          
@VerObserVoucher bit ,        
@VerObserBiblia bit,        
@IDTipoOC char(3),        
@IDGuiaProveedor char(6),   
@FlServicioNoShow bit,  
@FlServInManualOper bit,  
@NetoHab numeric(12,4),  
@IgvHab numeric(12,4),  
@TotalHab numeric(12,4),  
@NetoGen numeric(12,4),  
@IgvGen numeric(12,4),  
@TotalGen numeric(12,4),
@IDMoneda char(3), 
@UserMod char (4),
@CantidadAPagarEditado bit,
@NetoHabEditado bit,
@IgvHabEditado bit,
@pIDOperacion_Det int Output          
As          
 Set NoCount On          
           
           
 Declare @IDOperacion_Det int          
           
 Execute dbo.Correlativo_SelOutput 'OPERACIONES_DET',1,10,@IDOperacion_Det output          
           
 INSERT INTO OPERACIONES_DET          
           (IDOperacion_Det          
           ,IDOperacion                                
           ,[IDReserva_Det]          
           ,[Item]          
           ,[Dia]          
           ,[DiaNombre]          
           ,FechaOut           
           ,Noches          
           ,[IDubigeo]          
           ,[IDServicio]          
           ,[IDServicio_Det]          
           ,[Especial]          
           ,[MotivoEspecial]          
           ,[RutaDocSustento]          
           ,[Desayuno]          
           ,[Lonche]          
           ,[Almuerzo]          
           ,[Cena]          
           ,[Transfer]          
           ,[IDDetTransferOri]          
           ,[IDDetTransferDes]                     
           ,[TipoTransporte]          
           ,[IDUbigeoOri]          
           ,[IDUbigeoDes]                     
           ,IDIdioma                     
           ,[NroPax]          
           ,[NroLiberados]          
           ,[Tipo_Lib]          
           ,Cantidad          
           ,CantidadAPagar  
           ,[CostoReal]          
           ,[CostoRealAnt]          
           ,[CostoLiberado]          
           ,[Margen]          
           ,[MargenAplicado]          
           ,[MargenAplicadoAnt]          
           ,[MargenLiberado]          
           ,[Total]          
           ,[TotalOrig]          
     ,[CostoRealImpto]          
     ,[CostoLiberadoImpto]          
     ,[MargenLiberadoImpto]          
     ,[MargenImpto]          
           ,[TotImpto]         
           ,[Servicio]          
                     
           ,IDVoucher          
           --,FechaRecordatorio          
           --,Recordatorio          
           ,ObservBiblia          
           ,ObservInterno          
           ,ObservVoucher          
           ,VerObserVoucher        
           ,VerObserBiblia        
                 
           ,IDGuiaProveedor      
                   
             ,[NetoHab],  
    [IgvHab],  
    [TotalHab],  
    [NetoGen],  
    [IgvGen],  
    [TotalGen]  
  
                   
           ,[UserMod]          
,IDTipoOC        
,FlServicioNoShow  
,FlServInManualOper  
,IDMoneda
           ,[FecMod]
		   ,[CantidadAPagarEditado]
		   ,[NetoHabEditado]
		   ,[IgvHabEditado])
     VALUES          
           (@IDOperacion_Det          
           ,@IDOperacion          
           --,@IDReserva_Det          
           ,case when @IDReserva_Det=0 then null else @IDReserva_Det end  
           ,@Item          
           ,@Dia          
           ,DATENAME(dw,@Dia)          
           ,Case When @FechaOut='01/01/1900' Then Null Else @FechaOut End          
           ,Case When @Noches=0 Then Null Else @Noches End          
           ,@IDubigeo          
           ,@IDServicio          
           ,@IDServicio_Det          
           ,@Especial          
           ,Case When ltrim(rtrim(@MotivoEspecial))='' Then Null Else @MotivoEspecial End          
           ,Case When ltrim(rtrim(@RutaDocSustento))='' Then Null Else @RutaDocSustento End          
           ,@Desayuno           
           ,@Lonche          
		   ,@Almuerzo          
           ,@Cena          
           ,@Transfer          
           ,Case When LTRIM(rtrim(@IDDetTransferOri))='' Then Null Else @IDDetTransferOri End          
           ,Case When ltrim(rtrim(@IDDetTransferDes))='' Then Null Else @IDDetTransferDes End                     
           ,Case When LTRIM(rtrim(@TipoTransporte))='' Then Null Else @TipoTransporte End          
           ,Case When ltrim(rtrim(@IDUbigeoOri))='' Then Null Else @IDUbigeoOri End          
           ,Case When LTRIM(rtrim(@IDUbigeoDes))='' Then Null Else @IDUbigeoDes End          
                     
           ,@IDIdioma          
                                
           ,@NroPax          
           ,Case When ltrim(rtrim(@NroLiberados))='' Then Null Else @NroLiberados End          
           ,Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End          
           ,@Cantidad          
           ,@CantidadAPagar  
           ,@CostoReal          
           ,Case When LTRIM(rtrim(@CostoRealAnt))='' Then Null Else @CostoRealAnt End          
           ,@CostoLiberado          
           ,@Margen          
           ,@MargenAplicado          
           ,Case When LTRIM(rtrim(@MargenAplicadoAnt))='' Then Null Else @MargenAplicadoAnt End          
           ,@MargenLiberado          
           ,@Total          
           ,@TotalOrig          
		  ,@CostoRealImpto          
		  ,@CostoLiberadoImpto          
		  ,@MargenLiberadoImpto          
		  ,@MargenImpto          
          ,@TotImpto                 
           ,@Servicio          
			,@IDVoucher          
           --,Case When @FechaRecordatorio='01/01/1900' Then Null Else @FechaRecordatorio End          
           --,Case When LTRIM(rtrim(@Recordatorio))='' Then Null Else @Recordatorio End          
           ,Case When LTRIM(rtrim(@ObservBiblia))='' Then Null Else @ObservBiblia End                     
           ,Case When LTRIM(rtrim(@ObservInterno))='' Then Null Else @ObservInterno End          
           ,Case When LTRIM(rtrim(@ObservVoucher))='' Then Null Else @ObservVoucher End          
           ,@VerObserVoucher        
           ,@VerObserBiblia       
           ,Case When ltrim(rtrim(@IDGuiaProveedor))= '' Then Null Else @IDGuiaProveedor End          
            ,@NetoHab,  
			@IgvHab,  
			@TotalHab,  
			@NetoGen,  
			@IgvGen,  
			@TotalGen      
           ,@UserMod          
           ,@IDTipoOC        
           ,@FlServicioNoShow  
           ,@FlServInManualOper  
           ,@IDMoneda
           ,GetDate()
		   ,@CantidadAPagarEditado
		   ,@NetoHabEditado
		   ,@IgvHabEditado)          
                     
      Set @pIDOperacion_Det=@IDOperacion_Det          
