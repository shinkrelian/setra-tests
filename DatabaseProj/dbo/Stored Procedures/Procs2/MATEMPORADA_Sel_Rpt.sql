﻿
CREATE Procedure [dbo].[MATEMPORADA_Sel_Rpt]
	@Descripcion varchar(60)
As
BEGIN
	Set NoCount On
	
	Select IDTemporada, NombreTemporada, TipoTemporada
	From MATEMPORADA
	Where (NombreTemporada Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')
END
