﻿CREATE PROCEDURE dbo.NOTA_VENTA_DET_DelxIDReserva
	@IDReserva	int
As
	Set Nocount On
	
	DELETE FROM NOTA_VENTA_DET WHERE IDReserva_Det 
		In (Select IDReserva_Det From RESERVAS_DET Where IDReserva=@IDReserva)