﻿--MAUSUARIOS_Sel_CboxNivel_Activos 'COU',0,'A'
--CREATE
--MAUSUARIOS_Sel_CboxNivel_Activos 'RES',0,''
--JHD-20141125 - Nuevo procedimiento para listar los activos 
--JHD-20150516 - where @Nivel IN ('RES','OPE') and Nivel in('COU','SCO')
CREATE Procedure MAUSUARIOS_Sel_CboxNivel_Activos         
 @Nivel Char(3),                  
 @bTodos bit,
 @Activo CHAR(1)                  
As                  
                  
 Set NoCount On                  
                   
 Select IDUsuario, Usuario,Activo from                   
 (                  
    Select '' as IDUsuario,'<TODOS>' As Usuario,'A' AS Activo,0 as Ord,0 as OrdenNivel                   
    Union          
    Select '' as IDUsuario,'' As Usuario,'A' AS Activo,1 as Ord,0 as OrdenNivel            
    where @bTodos = 0                 
    Union                  
    Select IDUsuario,Usuario,u.Activo,2,n.Orden as OrdenNivel              
    From MAUSUARIOS u Left Join MANIVELUSUARIO n On u.Nivel=n.IDNivel              
    Where --u.Activo='A' And                   
        (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and         
   (ltrim(rtrim(@Nivel))='' Or Nivel=@Nivel)                  
   /*Cambio temporal*/              
   --u.Nivel IN ('GER','SRE','JAR','RES')                  
   --And u.IdArea in('RS','GR','OP')              
   /*---*/              
               
   Union            
 Select u.IDUsuario,u.Usuario,u.Activo,3,0 as OrdenNivel              
    From MAUSUARIOS u             
    Where --u.Activo='A' And             
        (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and         
    u.Nivel=(Case @Nivel When 'VTA' Then 'SVE'             
       When 'RES' Then 'SRE'             
       When 'OPE' Then 'SOP' End)            
    Or u.Nivel=(Case @Nivel         
       When 'RES' Then 'SOP'             
       End)            
                   
            
 UNION            
 Select u.IDUsuario,u.Usuario,u.Activo,4,0 as OrdenNivel              
    From MAUSUARIOS u             
    Where --u.Activo='A' And             
        (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and         
    u.Nivel='GER'            
            
               
Union    
    
        
    Select IDUsuario,Usuario,u.Activo,5,n.Orden as OrdenNivel              
    From MAUSUARIOS u Left Join MANIVELUSUARIO n On u.Nivel=n.IDNivel              
    Where --u.Activo='A' And                   
                (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and             
   (ltrim(rtrim(@Nivel))='' Or Nivel In ('RES','OPE'))                  
    
Union  
 Select  IDUsuario,Usuario,Activo,6,0 as OrdenNivel   
 from mausuarios  
 where @Nivel IN ('RES','OPE') and Nivel in('COU','SCO')
 --where @Nivel = 'RES' and Nivel in('COU','SCO')
               
 ) As X                  
 Where (@bTodos=1 Or Ord<>0)                 
 And (@Nivel in ('RES','OPE','COU') OR Ord<>5)    
 And (Not @Nivel in ('RES','OPE','COU') OR Ord<>2)    
  and   (Activo = @Activo or @Activo ='') 
 Order By --Usuario                
 Ord, OrdenNivel, Usuario            



