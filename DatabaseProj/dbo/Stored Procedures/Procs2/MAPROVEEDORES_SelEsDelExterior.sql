﻿
 --JRF-20160420- Not p.CoUbigeo_Oficina In ('000065','000066').. Oficinas Lim/Cus debe notificarse a pagos1...
 --PPMG20160427- Se cambio WHERE UC.IDPais<>'000323' and Not p.CoUbigeo_Oficina In ('000065','000066')
 CREATE Procedure [dbo].[MAPROVEEDORES_SelEsDelExterior]
	@CoProveedor char(6),
	@pDelExterior bit output
As
	Set Nocount on

	Set @pDelExterior=0
	If Exists(
		SELECT P.NombreCorto, P.RazonSocial,p.IDTipoProv FROM MAPROVEEDORES P INNER JOIN MAUBIGEO UC ON P.IDCiudad=UC.IDubigeo
		--WHERE UC.IDPais<>'000323' and Not p.CoUbigeo_Oficina In ('000065','000066')
		WHERE UC.IDPais<>'000323' and p.CoUbigeo_Oficina = '000065'
		and p.IDProveedor=@CoProveedor And Activo='A')
		Set @pDelExterior=1

