﻿--JRF-20131211-Debe mostrar cualquier servicio exceptuando a los Hotels,Restaurantes, Transportes y Guias.        
--JRF-20140317-Agregando el filtro de Ejecutivo de Ventas y Reserva.    
--JRF-20140616-Agregando el filtro por Proveedor  
--JRF-20150317-No permitir Free (And CHARINDEX('free',Lower(s.Descripcion))=0      )
--JRF-20150415-Permitir mostrar las botellas de agua.
--JHD-20150506-@CoPais char(6)=''
--JHD-20150506-And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=rd.IDubigeo)=@CoPais )
--JHD-20150522-Se cambio la consulta para mostrar las cotizaciones pendientes
--JHD-20150903-case when isnull(p.IDProveedorInternacional,'')='' then p.NombreCorto 
				--else (select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional) +': '+p.NombreCorto end as DescProv
--JRF-20151022-Agregar el parametro [@EstadoReserva]
--JHD-20151120-Se agrego un nuevo Select para listar las operaciones sin reservas
--HLF-20160127- 2do UNION (3er query)
--Left Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det
--Left Join MASERVICIOS_DET sdd On ods.IDServicio_Det_V_Cot=sdd.IDServicio_Det
--Left Join MASERVICIOS s On sdd.IDServicio = s.IDServicio
--HLF-20160203-Reemplazando Or cast(convert(char(10),isnull(rd.Dia,cd.Dia),103)as smalldatetime)   en los 3 selects
--quitando and /*o.IDReserva is null and*/ od.IDReserva_Det is null al 3er query
--HLF-20160204-DATEADD(DAY,1,DATEADD(SECOND,-1 ,@FechaRango2))
CREATE Procedure dbo.RESERVAS_DET_Sel_RptServicios          
@DescServicio varchar(max),          
@IDServicio char(8),          
@IDCliente char(6),          
@IDProveedor char(6),  
@IDUbigeo char(6),          
@Estado char(1),          
@IDUsuarioVentas char(4),    
@IDUsuarioReservas char(4),    
@FechaRango1 smalldatetime,          
@FechaRango2 smalldatetime,
@CoPais char(6),
@EstadoReserva char(2)
As
Set NoCount On

select ServicioGroup,
IDServicio,
 Fecha,
Cotizacion,          
    IDFile,
	DescCliente,
	Titulo,
	Estado,
	Fecha2,
	NroPax,          
   NroLiberados,
	TotalGen,
	--p.NombreCorto as DescProv          
  CambiosPendientes
from (
select distinct s.Descripcion as ServicioGroup,
s.IDServicio,
Convert(Char(10),isnull(rd.Dia,cd.Dia),103) as Fecha,
c.Cotizacion,          
    c.IDFile,
	cl.RazonComercial as DescCliente,
	c.Titulo,
	c.Estado,
	Convert(Char(10),isnull(rd.Dia,cd.Dia),103) as Fecha2,
	NroPax=isnull(rd.NroPax,cd.NroPax),          
    Isnull(rd.NroLiberados,ISNULL(cd.NroLiberados,0)) as NroLiberados,
	TotalGen=isnull(rd.TotalGen,isnull(cd.Total,0)),
	--p.NombreCorto as DescProv          
	case when isnull(p.IDProveedorInternacional,'')='' then p.NombreCorto 
		else (select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional) +': '+p.NombreCorto end as DescProv,
	
		--Case When ((p.IDTipoProv='001' And sd.PlanAlimenticio=0) Or (p.IDTipoProv='003' And sd.ConAlojamiento=1)) Then 
		--dbo.FnCambiosenVentasPostFileReservasSoloAlojamiento(rd.IDReserva) Else 
		--	Case When p.IDTipoOper='I' then dbo.FnCambiosenVentasPostFileReservasSoloServicios(rd.IDReserva) else 
		--	dbo.FnCambiosEnVentasPostFileSoloSubServicios(rd.IDReserva) End End As CambiosPendientes

		cast(0 as bit) AS CambiosPendientes
from COTICAB c 
left join cotidet cd on cd.IDCAB=c.IDCAB
Left Join RESERVAS r On r.IDCab = c.IDCAB  and r.Anulado = 0     
left Join RESERVAS_DET rd On r.IDReserva = rd.IDReserva and rd.Anulado = 0          
Left Join MAPROVEEDORES p On isnull(r.IDProveedor,cd.IDProveedor) = p.IDProveedor          
--Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det          
Left Join MASERVICIOS s On isnull(rd.IDServicio,cd.IDServicio) = s.IDServicio          
Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente          
Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
Where --(p.IDTipoProv <> '001' And p.IDTipoProv <> '002' --And p.IDTipoProv <> '004'        
    --And p.IDTipoProv <> '005')   
--r.Anulado = 0 and rd.Anulado = 0  and
/*and*/ (ltrim(rtrim(@Estado))='' Or c.Estado = @Estado)           
and (ltrim(rtrim(@DescServicio))='' Or s.Descripcion like '%'+@DescServicio+'%')          
and (ltrim(rtrim(@IDUbigeo))='' Or isnull(rd.IDubigeo,c.IDubigeo) = @IDUbigeo) --maservicio          
And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=isnull(rd.IDubigeo,cd.IDubigeo))=@CoPais )       
and (LTRIM(rtrim(@IDUsuarioVentas))='' Or c.IDUsuario = @IDUsuarioVentas)    
and (LTRIM(rtrim(@IDUsuarioReservas))='' Or c.IDUsuarioRes = @IDUsuarioReservas)    
and (ltrim(rtrim(@IDCliente))='' Or c.IDCliente = @IDCliente)          
and (ltrim(rtrim(@IDProveedor))='' Or p.IDProveedor = @IDProveedor)  --cotidet
and ((ltrim(rtrim(Convert(Char(10),@FechaRango1,103)))='01/01/1900')           
 And (ltrim(rtrim(Convert(Char(10),@FechaRango2,103)))='01/01/1900')   --cotidet        
  --Or cast(convert(char(10),isnull(rd.Dia,cd.Dia),103)as smalldatetime)          
	OR isnull(rd.Dia,cd.Dia)
   between @FechaRango1 and --@FechaRango2)          
   DATEADD(DAY,1,DATEADD(SECOND,-1 ,@FechaRango2)))
and (ltrim(rtrim(@IDServicio))='' Or s.IDServicio = @IDServicio)    --maservicio
And CHARINDEX('free',Lower(s.Descripcion))=0 
And p.IDTipoProv<>'001' And p.IDTipoProv <> '002' --And p.IDTipoProv='003'
And (Ltrim(Rtrim(@EstadoReserva))='' Or r.Estado=@EstadoReserva)
And s.cActivo='A' And sd.Estado='A'

UNION

select distinct s.Descripcion as ServicioGroup,
s.IDServicio,
Convert(Char(10),isnull(od.Dia,cd.Dia),103) as Fecha,
c.Cotizacion,          
    c.IDFile,
	cl.RazonComercial as DescCliente,
	c.Titulo,
	c.Estado,
	Convert(Char(10),isnull(od.Dia,cd.Dia),103) as Fecha2,
	NroPax=isnull(od.NroPax,cd.NroPax),          
    Isnull(od.NroLiberados,ISNULL(cd.NroLiberados,0)) as NroLiberados,
	TotalGen=isnull(od.TotalGen,isnull(cd.Total,0)),
	--p.NombreCorto as DescProv   
	 	case when isnull(p.IDProveedorInternacional,'')='' then p.NombreCorto 
		else (select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional) +': '+p.NombreCorto end as DescProv,      
	cast(0 as bit) As CambiosPendientes

from COTICAB c 
left join cotidet cd on cd.IDCAB=c.IDCAB
--Left Join RESERVAS r On r.IDCab = c.IDCAB  and r.Anulado = 0     
--left Join RESERVAS_DET rd On r.IDReserva = rd.IDReserva and rd.Anulado = 0          

Left Join OPERACIONES o On c.IDCAB = o.IDCab
Left Join OPERACIONES_DET od On o.IDOperacion = od.IDOperacion   

Left Join MAPROVEEDORES p On isnull(o.IDProveedor,cd.IDProveedor) = p.IDProveedor          
--Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det          
Left Join MASERVICIOS s On isnull(od.IDServicio,cd.IDServicio) = s.IDServicio          
Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente          
Left Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
Where --(p.IDTipoProv <> '001' And p.IDTipoProv <> '002' --And p.IDTipoProv <> '004'        
    --And p.IDTipoProv <> '005')   
--r.Anulado = 0 and rd.Anulado = 0  and
/*and*/ (ltrim(rtrim(@Estado))='' Or c.Estado = @Estado)           
and (ltrim(rtrim(@DescServicio))='' Or s.Descripcion like '%'+@DescServicio+'%')          
and (ltrim(rtrim(@IDUbigeo))='' Or isnull(od.IDubigeo,c.IDubigeo) = @IDUbigeo) --maservicio          
And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=isnull(od.IDubigeo,cd.IDubigeo))=@CoPais )       
and (LTRIM(rtrim(@IDUsuarioVentas))='' Or c.IDUsuario = @IDUsuarioVentas)    
and (LTRIM(rtrim(@IDUsuarioReservas))='' Or c.IDUsuarioRes = @IDUsuarioReservas)    
and (ltrim(rtrim(@IDCliente))='' Or c.IDCliente = @IDCliente)          
and (ltrim(rtrim(@IDProveedor))='' Or p.IDProveedor = @IDProveedor)  --cotidet
and ((ltrim(rtrim(Convert(Char(10),@FechaRango1,103)))='01/01/1900')           
 And (ltrim(rtrim(Convert(Char(10),@FechaRango2,103)))='01/01/1900')   --cotidet        
  --Or cast(convert(char(10),isnull(od.Dia,cd.Dia),103)as smalldatetime)          
  OR isnull(od.Dia,cd.Dia)
   between @FechaRango1 and --@FechaRango2)          
   DATEADD(DAY,1,DATEADD(SECOND,-1 ,@FechaRango2)))
and (ltrim(rtrim(@IDServicio))='' Or s.IDServicio = @IDServicio)    --maservicio
And CHARINDEX('free',Lower(s.Descripcion))=0 
And p.IDTipoProv<>'001' And p.IDTipoProv <> '002' --And p.IDTipoProv='003'
--And (Ltrim(Rtrim(@EstadoReserva))='' Or o.Estado=@EstadoReserva)
And s.cActivo='A' And sd.Estado='A'
and /*o.IDReserva is null and*/ od.IDReserva_Det is null

UNION

select distinct s.Descripcion as ServicioGroup,
s.IDServicio,
Convert(Char(10),isnull(od.Dia,cd.Dia),103) as Fecha,
c.Cotizacion,          
    c.IDFile,
	cl.RazonComercial as DescCliente,
	c.Titulo,
	c.Estado,
	Convert(Char(10),isnull(od.Dia,cd.Dia),103) as Fecha2,
	NroPax=isnull(od.NroPax,cd.NroPax),          
    Isnull(od.NroLiberados,ISNULL(cd.NroLiberados,0)) as NroLiberados,
	TotalGen=isnull(od.TotalGen,isnull(cd.Total,0)),
	--p.NombreCorto as DescProv   
	 	case when isnull(p.IDProveedorInternacional,'')='' then p.NombreCorto 
		else (select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional) +': '+p.NombreCorto end as DescProv,      
	cast(0 as bit) As CambiosPendientes

from COTICAB c 
left join cotidet cd on cd.IDCAB=c.IDCAB
--Left Join RESERVAS r On r.IDCab = c.IDCAB  and r.Anulado = 0     
--left Join RESERVAS_DET rd On r.IDReserva = rd.IDReserva and rd.Anulado = 0          

Left Join OPERACIONES o On c.IDCAB = o.IDCab
Left Join OPERACIONES_DET od On o.IDOperacion = od.IDOperacion   

Left Join MAPROVEEDORES p On isnull(o.IDProveedor,cd.IDProveedor) = p.IDProveedor          
--Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det          
    
Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente          
Left Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det

Left Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det
Left Join MASERVICIOS_DET sdd On ods.IDServicio_Det_V_Cot=sdd.IDServicio_Det
Left Join MASERVICIOS s On sdd.IDServicio = s.IDServicio      
Where --(p.IDTipoProv <> '001' And p.IDTipoProv <> '002' --And p.IDTipoProv <> '004'        
    --And p.IDTipoProv <> '005')   
--r.Anulado = 0 and rd.Anulado = 0  and
/*and*/ (ltrim(rtrim(@Estado))='' Or c.Estado = @Estado)           
and (ltrim(rtrim(@DescServicio))='' Or s.Descripcion like '%'+@DescServicio+'%')          
and (ltrim(rtrim(@IDUbigeo))='' Or isnull(od.IDubigeo,c.IDubigeo) = @IDUbigeo) --maservicio          
And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=isnull(od.IDubigeo,cd.IDubigeo))=@CoPais )       
and (LTRIM(rtrim(@IDUsuarioVentas))='' Or c.IDUsuario = @IDUsuarioVentas)    
and (LTRIM(rtrim(@IDUsuarioReservas))='' Or c.IDUsuarioRes = @IDUsuarioReservas)    
and (ltrim(rtrim(@IDCliente))='' Or c.IDCliente = @IDCliente)          
and (ltrim(rtrim(@IDProveedor))='' Or p.IDProveedor = @IDProveedor)  --cotidet
and 
((ltrim(rtrim(Convert(Char(10),@FechaRango1,103)))='01/01/1900')           
 And (ltrim(rtrim(Convert(Char(10),@FechaRango2,103)))='01/01/1900')   --cotidet        
 --Or cast(convert(char(10),isnull(od.Dia,cd.Dia),103)as smalldatetime)          
  OR   isnull(od.Dia,cd.Dia)
    between @FechaRango1 and 
	--@FechaRango2  )
	DATEADD(DAY,1,DATEADD(SECOND,-1 ,@FechaRango2)))
and (ltrim(rtrim(@IDServicio))='' Or s.IDServicio = @IDServicio)    --maservicio
And CHARINDEX('free',Lower(s.Descripcion))=0 
And p.IDTipoProv<>'001' And p.IDTipoProv <> '002' --And p.IDTipoProv='003'
--And (Ltrim(Rtrim(@EstadoReserva))='' Or o.Estado=@EstadoReserva)
And s.cActivo='A' And sd.Estado='A'
) as x order by ServicioGroup,Fecha




