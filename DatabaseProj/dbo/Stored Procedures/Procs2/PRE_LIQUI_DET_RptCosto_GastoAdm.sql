﻿
Create Procedure dbo.PRE_LIQUI_DET_RptCosto_GastoAdm
@IDNotaVenta char(10)
As
	Select   toc.Orden,
			 sd.idTipoOC,  
			 toc.Descripcion as DescTipoOC,  
			 p.NombreCorto As DescProveedor,  
			 pd.Descripcion As Servicio,  
			 pd.SubTotal As Neto,  
			 pd.TotalIGV As Igv,  
			 pd.Total AS Total,
			 (select SUM(Total) from DEBIT_MEMO 
			  where IDCab = (select IDCab from NOTA_VENTA 
			  where IDNotaVenta= @IDNotaVenta)) As TotalDebitMemo,
			  --Campo Calc C2
			  ((select SUM(Total) from DEBIT_MEMO 
			  where IDCab = (select IDCab from NOTA_VENTA 
			  where IDNotaVenta= @IDNotaVenta))
			  -
			   (select SUM(pd2.Total) from PRE_LIQUI_DET pd2
			    Left Join OPERACIONES_DET od2 On pd2.IDOperacion_Det=od2.IDOperacion_Det  
				Left Join MASERVICIOS_DET sd2 On od2.IDServicio_Det=sd2.IDServicio_Det
				Where pd2.IDNotaVenta =@IDNotaVenta and pd2.idTipoOC <> '005')
			  +
			  (select SUM(pd2.TotalIGV) from PRE_LIQUI_DET pd2
			    Left Join OPERACIONES_DET od2 On pd2.IDOperacion_Det=od2.IDOperacion_Det  
				Left Join MASERVICIOS_DET sd2 On od2.IDServicio_Det=sd2.IDServicio_Det
				Where pd2.IDNotaVenta =@IDNotaVenta and pd2.idTipoOC = '001')
			   ) As ColReptC2
			  
	from PRE_LIQUI_DET pd
	Left Join OPERACIONES_DET od On pd.IDOperacion_Det = od.IDOperacion_Det
	Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det
	Left Join MATIPOOPERACION toc On toc.IdToc = pd.idTipoOC
	Left Join MASERVICIOS s On sd.IDServicio = s.IDServicio
	Left Join MAPROVEEDORES p on s.IDProveedor = p.IDProveedor
	Where pd.IDNotaVenta = @IDNotaVenta And pd.idTipoOC = '005'
	Order By toc.Orden
