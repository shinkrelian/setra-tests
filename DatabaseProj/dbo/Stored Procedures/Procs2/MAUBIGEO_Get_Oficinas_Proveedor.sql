﻿
---------------------------------------------------------------------------------------------------------------------------
--MAUBIGEO_Get_Oficinas_Proveedor '000002'
--CREATE
create Procedure dbo.MAUBIGEO_Get_Oficinas_Proveedor
@idproveedor char(6)=''
as  

SELECT IDubigeo,Descripcion,
--Activo=(select isnull(CoUbigeo_Oficina,CAST(0 as bit)) from MAPROVEEDORES where IDProveedor=@idproveedor)
Activo= case when (select isnull(CoUbigeo_Oficina,'') from MAPROVEEDORES where IDProveedor=@idproveedor)=IDubigeo then (CAST(1 AS bit)) else (CAST(0 AS bit)) end
FROM MAUBIGEO WHERE IDubigeo IN (SELECT IDCiudad FROM MAPROVEEDORES WHERE IDTipoOper='I' AND IDTipoProv='003')

