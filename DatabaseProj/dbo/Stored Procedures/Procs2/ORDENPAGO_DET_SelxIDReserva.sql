﻿CREATE Procedure dbo.ORDENPAGO_DET_SelxIDReserva 
@IDReserva int,
@IDCAB int
As
	Set NoCount On
	select od.*
	from ORDENPAGO_DET od Left Join ORDENPAGO o On od.IDOrdPag = o.IDOrdPag 
		Left Join RESERVAS rv On o.IDReserva = rv.IDReserva
	where o.IDReserva = @IDReserva And o.IDCab = @IDCAB
