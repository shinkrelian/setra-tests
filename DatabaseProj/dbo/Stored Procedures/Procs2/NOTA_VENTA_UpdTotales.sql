﻿Create Procedure dbo.NOTA_VENTA_UpdTotales
	@IDNotaVenta char(10),
	@SubTotal numeric(8,2),
	@TotalIGV numeric(8,2),
	@Total numeric(8,2),
	@UserMod char(4)
As
	Set NoCount On
	
	Update NOTA_VENTA Set 
	SubTotal=@SubTotal,
	TotalIGV=@TotalIGV,
	Total=@Total,
	UserMod=@UserMod,
	FecMod=getdate()
	Where IDNotaVenta=@IDNotaVenta

