﻿Create Procedure dbo.OPERACIONES_DET_SelCotidetxIDDet
@IDDet int
as
Set NoCount On
Select cd.Item,CONVERT(char(10),cd.Dia,103) as CadenaDia,UPPER(cd.DiaNombre) as NombreDia,
	CAST(DATEPART(day,cd.Dia) as varchar(3))+ ' '+UPPER(DATENAME(MM,cd.Dia)) +' '+ CAST(DATEPART(YEAR,cd.Dia) AS VARCHAR(4)) as MesAnio, 
	UPPER(DATENAME(MM,cd.Dia)) As DiaNombreLengOrig,cd.Dia,'00:00' as Hora,ub.Descripcion as Ciudad,cd.Servicio,
	 Case When s.IDTipoServ = 'NAP' Then '-' Else s.IDTipoServ End               
  + Case When idio.Siglas ='---' Or idio.Siglas ='O.T' then '' else ' ('+idio.Siglas+')' End As TipoServicio,cast(cd.NroPax as varchar(2)) +' Pax' as NroPax,
  p.IDTipoProv,sd.ConAlojamiento,cd.IDDET  
from COTIDET cd Left Join MAUBIGEO ub On cd.IDubigeo = ub.IDubigeo
Left Join MASERVICIOS s On cd.IDServicio =s.IDServicio Left Join MAIDIOMAS idio On cd.IDidioma = idio.IDidioma
Left Join MAPROVEEDORES p On cd.IDProveedor =  p.IDProveedor Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det
where cd.IDDetRel = @IDDet
