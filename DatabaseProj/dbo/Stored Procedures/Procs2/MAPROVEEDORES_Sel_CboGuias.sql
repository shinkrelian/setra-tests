﻿--HLF-20150925-Union  Select IDProveedor, NombreCorto as DescProveedor,2 as Ord  From MAPROVEEDORES Where IDTipoProv = '020' and Activo = 'A'
CREATE Procedure dbo.MAPROVEEDORES_Sel_CboGuias
As    
 Set NoCount On    
   
 SELECT * FROM    
 (    
 Select '' as IDProveedor,'' as DescProveedor, 0 as Ord    
 Union    
     
 Select IDProveedor, NombreCorto as DescProveedor,1 as Ord  From MAPROVEEDORES    
 Where IDTipoProv = '005' and Activo = 'A'
 Union
  Select IDProveedor, NombreCorto as DescProveedor,2 as Ord  From MAPROVEEDORES    
 Where IDTipoProv = '020' and Activo = 'A'

 ) AS X       
 order by X.DescProveedor

