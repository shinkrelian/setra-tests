﻿
--JRF-20121126-Agregando Nuevos Campos Para Calcular.                                                    
--HLF-20121122-Agregando sd.PlanAlimenticio                                                        
--HLF-20121206-Agregando cd.IncGuia                                                    
--JRF-20121211-Agregando los nuevos campos.                                                    
--JRF-20121218-Agregando los nuevos campos (Guia y Buss).                                                 
--HLF-20130221-Cambiando logica de columna DescServicioDetBup                                              
--JRF-20130228-Ordenandolo x Fecha eh Item                                            
--JRF-20130304-Cambiando posicion de Columnas (Noche x Ciudad)                                          
--JRF-20130319-Cambiando posicion de Columnax (Cant.Pagar x Pax)                                        
--JRF-20130425-Insertando el Campo (IDTipoServ) - MASERVICIOS                                      
--JRF-20130425-Insertando el Campo (DetaTipo) - MASERVICIOS_DET                                      
--HLF-20130507-Agregando CAST(rd.NroPax AS VARCHAR(3))+CAST( isnull(rd.NroLiberados,0) AS VARCHAR(3)) as PaxmasLiberados,                                    
--HLF-20130508-Agregando columna isnull(IDMoneda,'') as IDMoneda,isnull(mon.Simbolo,'') as SimboloMoneda                                    
--HLF-20130509-Modificando PaxmasLiberados                                  
--HLF-20130515-Agregando uso de la funcion FnServicioHotelEsAlimenticioPuro                                
--HLF-20130522-Cambios a sd.Tipo                              
--JRF-20130525-Posición TipoServicio                            
--HLF-20130528-Agregando sd.ConAlojamiento, cd.SSCR                          
--HLF-20130607-Descomentando sd.Tipo                        
--HLF-20130617-Cambiando case when para columna Hora - Agregando AND sd.PlanAlimenticio=0                      
--JRF-20130618-Cambiando el campo DescGuia x un IDProveedor de Guia(IDGuiaProveedor).                     
--JRF-20130701-Cambiando el campo DescBus x un NuVehiculo de Vehiculos                  
--HLF-20130813-Agregando columna PNR                 
--HLF-20130828-Agregando And Rd.Anulado=0                
--HLF-20130906-Agregando isnull(sd.TC,0) as TipoCambio,              
--HLF-20131017-Cambiando a Order by IDReserva, Dia,FechaOut, DescServicioDet            
--HLF-20131022-Cambiando a Order by IDReserva, Dia,FechaOut, CapacidadHab, DescServicioDet            
--JRF-20131204-Mostrar la sigla del Idioma y la posición        
--HLF-20131230-Agregando cd.AcomodoEspecial        
--JRF-20140822-Agregar el DetaTipo para los Operadores con Alojamiento.      
--JRF-20140905-Agregar el Nuevo campo EsGuia (Campo que reconoce si fue agregado como guía)  
--JRF-20150218-Agregar el Nuevo campo FlServicioNoShow (Servicios No Show)  
--JRF-20150326-Agregar Filtro [And Rd.FlServicioTC=0]
--HLF-20150406-
--'' as CoUbigeo_Oficina, 0 as Tarifas_Anio,
--dbo.FnServicioMPWP(sd.IDServicio_Det) as IDServicio_V
--JRF-20150416-Case When pr.IDTipoOper ='I' Then s.Descripcion else sd.Descripcion End End      ..
--HLF-20150619-Nueva columna FlServicioNoCobrado
--HLF-20150622-Nueva columna SSTotalGenAntNoCobrado,QtCantidadAPagarAntNoCobrado
--PPMG-20151013--, Rd.CantidadAPagarEditado, Rd.NetoHabEditado, Rd.IgvHabEditado
--JRF-20151216--..Or sd.CoTipoCostoTC
--JRF-20160121--...Or (rd.FlServicioTC=1 And Not sd.CoTipoCostoTC In ('ALM','CEN'))) --And Not sd.CoTipoCostoTC In ('ALM','CEN')
--HLF-20160323-ISNULL(rd.RutaDocSustento,isnull(cd.RutaDocSustento, ... as RutaDocSustento,
--PPMG-20160413-Se modifico la llamada RutaDocSustento por una subconsulta en el FROM
CREATE Procedure [dbo].[RESERVAS_DET_SelxIDCab]                                                        
 @IDCab int                                                            
AS
BEGIN
 Set NoCount On                                                                  
                                              
  SELECT * FROM                   
 (                                
 Select Rd.IDReserva_Det, '' as IDOperacion_Det, rd.IDReserva, '' as IDOperacion, rd.IDFile, ISNull(Rd.Iddet,0) as IDDet,                                                            
 rd.Item, Rd.Dia, upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103))) as DiaFormat,               
 IsNull(Rd.FechaOut,'') as FechaOut, IsNull(upper(substring(DATENAME(dw,Rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,Rd.FechaOut,103))),'') as FechaOutFormat,                                                               
 Case When Pr.IDTipoProv='001' AND sd.PlanAlimenticio=0 Then '' Else convert(char(5),rd.Dia,108) End as Hora,                           
 Ub.Descripcion as Ubigeo, Isnull(Rd.Noches,0) as Noches, Pr.IDTipoProv, s.Dias, rv.IDProveedor, Pr.NombreCorto as Proveedor,                                                     
 ISNull(pr.Email3,'') As CorreoProveedor, Ub.IDPais, Rd.IDubigeo, Rd.Cantidad, Rd.NroPax,
 CAST(rd.NroPax AS VARCHAR(3)) + Case When isnull(rd.NroLiberados,0)=0 then '' else '+'+CAST(rd.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,                             
 Rd.IDServicio, Rd.IDServicio_Det, Rd.Servicio As DescServicioDet,
 Case When pr.IDTipoProv='001' or pr.IDTipoProv='002' Then  --hoteles o restaurantes                                                    
			sd.Descripcion                                                       
 Else                                                      
	Case When Pr.IDTipoProv='003' and sd.ConAlojamiento =1 then s.Descripcion +' '+cast(sd.DetaTipo as varchar(Max))
		else 
			Case When pr.IDTipoOper ='I' Then s.Descripcion else sd.Descripcion End End
 --s.Descripcion      
 End As DescServicioDetBup,                                                       
 Rd.Especial,Rd.MotivoEspecial,
 ----rd.RutaDocSustento,
 --ISNULL(rd.RutaDocSustento,isnull(cd.RutaDocSustento,
	--isnull((select top 1 RutaDocSustento from cotidet where idcab=@idcab and IDProveedor=rv.idproveedor 
	--	and not RutaDocSustento is null and cast(convert(varchar,dia,103) as smalldatetime) between 
	--	cast(convert(varchar,rd.dia,103) as smalldatetime) and cast(convert(varchar,rd.FechaOut,103) as smalldatetime)),'')
	--)) as RutaDocSustento,
 ISNULL(rd.RutaDocSustento,isnull(cd.RutaDocSustento,isnull(CRuta.RutaDocSustento,''))) as RutaDocSustento,
 Rd.Desayuno,Rd.Lonche,Rd.Almuerzo,Rd.Cena,                                                            
 Rd.Transfer,Rd.IDDetTransferOri,Rd.IDDetTransferDes,Rd.CamaMat,Rd.TipoTransporte,                                                            
 Rd.IDUbigeoOri,                                                            
 uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                              
 Rd.IDUbigeoDes,                                                    
 ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                                                                 
 idio.IDidioma as CodIdioma ,        
 Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,                                                   
 idio.Siglas as IDIdioma,                   
 sd.Anio,                              
 --sd.Tipo,                              
 --Case When sd.ConAlojamiento=1 And s.Dias>1 Then                          
 --'' Else sd.Tipo End as Tipo,                                   
 sd.Tipo,                              
 Isnull(sd.DetaTipo,'') As DetaTipo ,                                      
 Rd.CantidadAPagar,     --nueva pos (pos ant Rd.NroPax)                                                     
 Rd.NroLiberados,IsNull(Rd.Tipo_Lib,'') as Tipo_Lib,Rd.CostoRealAnt,Rd.Margen,                                                            
 Rd.MargenAplicado,Rd.MargenAplicadoAnt,Rd.MargenLiberado,Rd.TotalOrig,Rd.CostoRealImpto,Rd.CostoLiberadoImpto           
 ,Rd.MargenImpto,Rd.MargenLiberadoImpto,Rd.TotImpto,                                                            
 rd.IDDetRel ,Rd.Total,'' as IDVoucher,Rd.IDReserva_DetCopia ,Rd.IDReserva_Det_Rel                                                                  
 ,isnull(IDEmailEdit,'') as IDEmailEdit, isnull(IDEmailNew,'') as IDEmailNew,                                                            
 isnull(IDEmailRef,'') as IDEmailRef,                                                            
 --'01/01/1900' as FechaRecordatorio, '' as Recordatorio,                                    
 '' as ObservVoucher, '' as ObservBiblia, '' as ObservInterno,                                                      
 isnull(Rd.CapacidadHab,0) CapacidadHab ,Rd.EsMatrimonial, sd.PlanAlimenticio ,                                                    
 IsNull(cd.IncGuia,0) as IncGuia,
 0 as chkServicioExportacion                                                    
 ,0 as VerObserVoucher,0 as VerObserBiblia,                                                                 
 Rd.CostoReal,Rd.CostoLiberado                                                                
 ,Rd.NetoHab,Rd.IgvHab,Rd.TotalHab,Rd.NetoGen,Rd.IgvGen,                                    
 isnull(rd.IDMoneda,'') as IDMoneda,                                    
 isnull(mon.Simbolo,'') as SimboloMoneda,                                    
 isnull(sd.TC,0) as TipoCambio,              
 Rd.TotalGen                                                   
 ,Isnull(Rd.IDGuiaProveedor,'') As IDGuiaProveedor,Isnull(Rd.NuVehiculo , 0) as NuVehiculo --Isnull(Rd.DescBus,'') As DescBus                                      
 ,sd.idTipoOC                                
 ,0 as InactivoDet                                            
 ,CAse When Pr.IDTipoProv='001' Then                
		dbo.FnServicioHotelEsAlimenticioPuro (RV.IDReserva, Rd.Dia) 
  Else                                
		0                                
 End AS ServHotelAlimentPuro,                          
 IsNull(sd.ConAlojamiento,0) as ConAlojamiento, IsNull(cd.SSCR,0) AS SSCR, '' As PNR,                                  
 IsNull(cd.AcomodoEspecial,0) as AcomodoEspecial,Rd.FlServicioParaGuia,Rd.FlServicioNoShow,

 '' as CoUbigeo_Oficina,
 0 as Tarifas_Anio,
 dbo.FnServicioMPWP(sd.IDServicio_Det) as IDServicio_V,
 0 as FlServicioNoCobrado, 0 as SSTotalGenAntNoCobrado, 0 as QtCantidadAPagarAntNoCobrado
 , Rd.CantidadAPagarEditado, Rd.NetoHabEditado, Rd.IgvHabEditado
 From RESERVAS Rv                                                             
 Left Join RESERVAS_DET Rd On Rd.IDReserva=Rv.IDReserva                                                                  
 Left Join MAUBIGEO Ub On Rd.IDubigeo=Ub.IDubigeo                                                                   
 Left Join MAUBIGEO uo On Rd.IDUbigeoOri=uo.IDubigeo                                         
 Left Join MAUBIGEO ud On Rd.IDUbigeoDes=ud.IDubigeo                                                               
 Left Join MAPROVEEDORES Pr On  Pr.IDProveedor= Rv.IDProveedor                                                                  
 Left Join MASERVICIOS_DET sd On  Rd.IDServicio_Det=sd.IDServicio_Det                                              
 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio                                              
 Left Join COTIDET cd On Rd.IDDet=cd.IDDET                                                    
 Left Join MAMONEDAS mon on Rd.IDMoneda=mon.IDMoneda                                    
 Left Join MAIDIOMAS idio On Rd.IDIdioma = idio.IDidioma
 LEFT OUTER JOIN (SELECT IDCAB, IDProveedor, CONVERT(DATE, Dia) AS Dia, RutaDocSustento FROM COTIDET WHERE IDCAB = @IDCab AND RutaDocSustento IS NOT NULL) CRuta
				ON rv.IDCab = CRuta.IDCAB AND rv.IDProveedor = CRuta.IDProveedor AND
				CRuta.Dia BETWEEN CONVERT(DATE, rd.Dia) AND CONVERT(DATE, rd.FechaOut)
 Where Rv.IDCab=@IDCab And Rd.Anulado=0 And (Rd.FlServicioTC=0 Or (rd.FlServicioTC=1 And Not sd.CoTipoCostoTC In ('ALM','CEN'))) --And Not sd.CoTipoCostoTC In ('ALM','CEN')
 ) AS X                                
 --Where ServHotelAlimentPuro = 0                                
 --order by IDReserva, Dia,FechaOut, Item                     
 Order by IDReserva, Dia,FechaOut, CapacidadHab, DescServicioDet     
END
