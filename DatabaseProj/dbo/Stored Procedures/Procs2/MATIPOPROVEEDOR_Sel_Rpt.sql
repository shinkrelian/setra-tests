﻿create procedure [dbo].[MATIPOPROVEEDOR_Sel_Rpt]
@Descripcion varchar(30) 
As
	Set NoCount on
	
	select IDTipoProv,Descripcion 
	from MATIPOPROVEEDOR
	where (Descripcion like '%'+@Descripcion+'%' or 
	LTRIM(RTRIM(@Descripcion))='') 
	Order By IDTipoProv asc
