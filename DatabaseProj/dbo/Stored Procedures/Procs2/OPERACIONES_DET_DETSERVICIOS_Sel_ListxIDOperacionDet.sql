﻿--JRF-20141022-Considerar [... and ods.IDServicio=od.IDServicio]  
--JRF-20150220-Considerar Reservas_Det
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Sel_ListxIDOperacionDet    
@IDOperacion_Det int    
as    
Set NoCount On    
Select Cast(od.IDReserva_Det as varchar) as IDReserva_DetSb,sdRel.IDServicio_Det,p.NombreCorto as ProveedorCotizado,sd.Descripcion as Servicio,    
Isnull(sdRel.Tipo,'') as Variante,    
ods.CostoReal/od.NroPax as Neto,ods.TotImpto/od.NroPax as TotImpto,mo.Simbolo as Moneda,ods.Total/od.NroPax as Total    
--,ods.*    
from OPERACIONES_DET_DETSERVICIOS ods   
Inner Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det and ods.IDServicio=od.IDServicio  
Left Join MASERVICIOS_DET sd On ods.IDServicio_Det = sd.IDServicio_Det    
Left Join MASERVICIOS_DET sdRel On Isnull(sd.IDServicio_Det_V,sd.IDServicio_Det) = sdRel.IDServicio_Det    
Left Join MASERVICIOS s On sdRel.IDServicio = s.IDServicio Left Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor    
Left Join MAMONEDAS mo On ods.IDMoneda= mo.IDMoneda    
Where ods.IDOperacion_Det = @IDOperacion_Det    
