﻿
-----------------------------------------------------------------------------------------------------------

--JHD-20150116- Se agregó el campo CoCliente
CREATE PROCEDURE [dbo].[NOTAVENTA_COUNTER_ITEM_Sel_Pk]    

@NuNtaVta char(6),
@NuItem tinyint=0
AS
BEGIN
SELECT     
NOTAVENTA_COUNTER_ITEM.NuNtaVta,
NOTAVENTA_COUNTER_ITEM.NuItem,
NOTAVENTA_COUNTER_ITEM.CoTipoServicio, 
                      MATIPOSERVICIO_COUNTER.NoDescripcion,
                     --  NOTAVENTA_COUNTER_ITEM.NuDocumento,
                      NOTAVENTA_COUNTER_ITEM.TxDetalle,
                      --NOTAVENTA_COUNTER_ITEM.CoProveedor, 
                      --MAPROVEEDORES.NombreCorto,
                       CoProveedor=isnull(NOTAVENTA_COUNTER_ITEM.CoProveedor,''), 
                      NombreCorto=isnull((select nombrecorto from maproveedores where coproveedor=NOTAVENTA_COUNTER_ITEM.CoProveedor),''),
                      NOTAVENTA_COUNTER_ITEM.QtCantidad,
                      --NOTAVENTA_COUNTER_ITEM.CoMoneda,
                      --MAMONEDAS.Descripcion, 
                      --NOTAVENTA_COUNTER_ITEM.SSTipoCambio,
                     
                      NOTAVENTA_COUNTER_ITEM.SSUnidad, 
                      NOTAVENTA_COUNTER_ITEM.SSTotal,
                      
                      CoCliente=isnull(NOTAVENTA_COUNTER_ITEM.CoCliente,''),
                      Cliente=isnull((select RazonSocial from MACLIENTES where IDCliente=NOTAVENTA_COUNTER_ITEM.CoCliente),'')
                      
FROM         NOTAVENTA_COUNTER_ITEM INNER JOIN
                      MATIPOSERVICIO_COUNTER ON NOTAVENTA_COUNTER_ITEM.CoTipoServicio = MATIPOSERVICIO_COUNTER.CoTipoServicio --INNER JOIN
                      --MAPROVEEDORES ON NOTAVENTA_COUNTER_ITEM.CoProveedor = MAPROVEEDORES.IDProveedor
                      --INNER JOIN
                      --MAMONEDAS ON NOTAVENTA_COUNTER_ITEM.CoMoneda = MAMONEDAS.IDMoneda
WHERE    (NOTAVENTA_COUNTER_ITEM.NuNtaVta = @NuNtaVta) AND

	(NOTAVENTA_COUNTER_ITEM.NuItem =@NuItem ) 
			
END;

