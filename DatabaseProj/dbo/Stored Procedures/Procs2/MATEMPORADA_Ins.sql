﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Ins]	
	@NombreTemporada varchar(80),
	@TipoTemporada char(1),
	@UserMod char(4),
	@pIDTemporada char(3) output
AS
BEGIN
	Set NoCount On
	Declare @IDTemporada int
	SET @IDTemporada = 0
	SELECT @IDTemporada = ISNULL(MAX(IDTemporada),0) + 1 FROM MATEMPORADA
	SELECT @IDTemporada = CASE WHEN ISNULL(@IDTemporada,0) = 0 THEN 1 ELSE @IDTemporada END

	INSERT INTO MATEMPORADA
           ([IDTemporada]
           ,[NombreTemporada]
		   ,[TipoTemporada]           
           ,[UserMod]
		   ,[FecMod])
     VALUES
           (@IDTemporada,
            @NombreTemporada,
			@TipoTemporada,           
            @UserMod,
			GETDATE())
            
	Set @pIDTemporada=@IDTemporada
END
