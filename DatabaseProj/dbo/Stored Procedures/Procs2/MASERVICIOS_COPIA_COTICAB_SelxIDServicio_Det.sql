﻿Create Procedure dbo.MASERVICIOS_COPIA_COTICAB_SelxIDServicio_Det
@IDServicio_DetAntiguo int,
@IDCab int,
@pIDServicio_Det int output
As
Set NoCount On
Set @pIDServicio_Det = (Select Top(1) ms.IDServicio_DetNuevo 
					from MASERVICIOS_COPIA_COTICAB ms --Inner Join MASERVICIOS s On ms.IDServicioNuevo=s.IDServicio
					Inner Join MASERVICIOS_DET sd On ms.IDServicio_Det=sd.IDServicio_Det
					Inner Join MASERVICIOS s On ms.IDServicioNuevo=s.IDServicio
					Where ms.IDCab=@IDCab And ms.IDServicio_Det=@IDServicio_DetAntiguo And s.cActivo='A')
Set @pIDServicio_Det = IsNull(@pIDServicio_Det,0)
