﻿CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_CopiaxVariante]  
 @IDServicio char(8),  
 @TipoNue varchar(7)  
As  
   
 Set NoCount On  
   
 Select d.Anio, d.Tipo, d.Descripcion, d.Monto_sgl, d.Monto_dbl, d.Monto_tri,  
 d.IDServicio_Det  
 From MASERVICIOS_DET d   
 Left Join MASERVICIOS_DET d2 On d.IDServicio_Det_V=d2.IDServicio_Det  
 Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio  
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor  
 Where d.IDServicio=@IDServicio And d.Tipo=@TipoNue And Not d.IDServicio_DetCopia is Null  
 Order by Anio, Tipo, d.Correlativo  
