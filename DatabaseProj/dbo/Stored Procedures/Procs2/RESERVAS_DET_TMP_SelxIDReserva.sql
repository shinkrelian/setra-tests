﻿--HLF-20140114-Agregando campo AcomodoEspecial
CREATE Procedure dbo.RESERVAS_DET_TMP_SelxIDReserva               
 @IDReserva int              
As              
 Set NoCount On              
               
 Select rd.IDReserva_Det, rd.Dia, rd.FechaOut,               
 rd.Servicio,               
 --cd.Servicio,               
 rd.IDServicio_Det,              
 rd.IDDet, rd.Noches, rd.NroPax, rd.NroLiberados             
             
 ,r.IDProveedor,          
           
 sd.ConAlojamiento, s.Dias            
         
 ,sd.PlanAlimenticio, p.IDTipoProv,cd.AcomodoEspecial

 From RESERVAS_DET_TMP rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva               
 Inner Join COTIDET cd On rd.IDDet=cd.IDDET And cd.IncGuia=0            
 Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det             
           
 And sd.PlanAlimenticio=0            
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio          
 Left Join MASERVICIOS p On s.IDProveedor=p.IDProveedor        
 Where rd.IDReserva = @IDReserva               
 Order by --rd.Dia      
 --rd.IDServicio_Det, rd.Dia      
 --rd.Servicio    
 rd.Servicio, rd.Dia      
   
