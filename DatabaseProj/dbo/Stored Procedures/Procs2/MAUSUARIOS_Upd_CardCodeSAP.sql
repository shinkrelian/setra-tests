﻿/*
alter table mausuarios add FlTrabajador_Activo bit not null default((1))
go

update MAUSUARIOS set FlTrabajador_Activo=1
go
*/

--CREATE
CREATE Procedure dbo.MAUSUARIOS_Upd_CardCodeSAP
@CardCodeSAP varchar(15),
@IDUsuario char(4)
As      
 UPDATE MAUSUARIOS      
    SET CardCodeSAP=@CardCodeSAP
 WHERE IDUsuario=@IDUsuario     
