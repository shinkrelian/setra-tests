﻿
--HLF-20141202-1 As Ger
--PPMG-20151214-0 As Ger
CREATE Procedure [dbo].[MAUSUARIOS_Sel_CboxArea_Activos]  
@DescArea char(2),    
@bGerencial bit,    
@bDato bit,  
@bTodos bit,    
@Activo CHAR(1)   
As     
BEGIN   
 Select * from(    
 select Case When @bDato = 1 Then '0000' Else '' End As IDUsuario,    
        Case When @bDato = 1 Then '----' Else '<TODOS>' End As Usuario    
        ,'A' AS Activo  
        ,2 as Ord    
 Union    
 Select * from (    
  select IDUsuario,Usuario ,Activo, 1 As Ger 
  from MAUSUARIOS     
  Where IdArea = 'GR'  and  (Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') --Activo = 'A'  
  Union     
  select IDUsuario,Usuario ,Activo, 
  0 As Ger 
  --1 As Ger 
  from MAUSUARIOS     
  where IdArea = @DescArea  and (Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') --Activo = 'A'  
  ) As X    
 Where (X.Ger = @bGerencial Or X.Ger = 0)) As Y     
 Where (@bTodos=1 oR Y.Ord<>2)    
  and   (Activo = @Activo or @Activo ='')   
 Order by Y.Ord desc,Y.Usuario    
END;