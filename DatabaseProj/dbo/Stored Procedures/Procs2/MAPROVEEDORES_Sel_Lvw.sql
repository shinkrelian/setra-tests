﻿CREATE Procedure[dbo].[MAPROVEEDORES_Sel_Lvw]  
 @ID char(6),  
 @Descripcion varchar(100)  
As  
 Set NoCount On  
   
 Select NombreCorto As RazonSocial  
 From MAPROVEEDORES  
 Where (RazonSocial Like '%'+@Descripcion+'%')  
 And (IDProveedor <>@ID Or ltrim(rtrim(@ID))='')  And Activo='A'
 Order by RazonSocial  
