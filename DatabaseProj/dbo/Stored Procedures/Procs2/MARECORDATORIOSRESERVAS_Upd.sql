﻿CREATE PROCEDURE [dbo].[MARECORDATORIOSRESERVAS_Upd]
	@IDRecordatorio smallint,
	@Descripcion varchar(200),
	@TipoCalculoTarea	char(2),
	@Dias tinyint,
	@Activo bit,
	@UserMod char(4)
as

	set nocount on
	update dbo.MARECORDATORIOSRESERVAS
		set Descripcion=@Descripcion,
			Dias=@Dias,
			TipoCalculoTarea=@TipoCalculoTarea,
			Activo=@Activo,
			UserMod=@UserMod,
			FecMod=GETDATE()
		where IDRecordatorio=@IDRecordatorio
