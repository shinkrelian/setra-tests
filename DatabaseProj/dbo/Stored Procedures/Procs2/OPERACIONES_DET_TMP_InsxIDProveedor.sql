﻿--HLF-20131108-And d.Anulado=0                    
--HLF-20131126- IsNull((Select Top 1 IDOperacion_Det From OPERACIONES_DET od1 ....        
--HLF-20131219- Modificando subquery a (Select IDOperacion From OPERACIONES Where IDCab=@IDCab And IDProveedor=@IDProveedor)      
--HLF-20140123-Agregando a subquery: op1 Inner Join RESERVAS r1 On op1.IDReserva=r1.IDReserva And r1.Anulado=0    
--JRF-20150106-Agregando el subquery: para obtener la observacion biblia digitada por el usuario.
--JRF-20150221-Agregando Nuevo campos[FlServicioNoShow]
CREATE Procedure dbo.OPERACIONES_DET_TMP_InsxIDProveedor            
 @IDCab int,                       
 @IDProveedor char(6),                      
 @UserMod char(4)                      
As                      
 Set Nocount On                      
                       
 Declare @IDOperacion_Det int                      
 Execute dbo.Correlativo_SelOutput 'OPERACIONES_DET_TMP',1,10,@IDOperacion_Det output                      
                       
 Insert Into OPERACIONES_DET_TMP            
 (IDOperacion_Det, IDOperacion, IDReserva_Det, IDVoucher,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,                      
 IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,                      
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,                      
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,                      
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,IDTipoOC,UserMod,                    
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,                  
 ObservBiblia,VerObserBiblia,IDOperacion_DetReal,FlServicioNoShow,IDMoneda,IDOperacion_DetAntiguo)                       
 Select  Distinct                      
 ((row_number() over (order by IDReserva,IDReserva_Det))-1) + @IDOperacion_Det,                       
 --(Select IDOperacion From OPERACIONES Where IDCab=@IDCab And IDReserva=d.IDReserva),                      
 (Select IDOperacion From OPERACIONES op1 Inner Join RESERVAS r1 On op1.IDReserva=r1.IDReserva And r1.Anulado=0    
 Where op1.IDCab=@IDCab And op1.IDProveedor=@IDProveedor),                      
 IDReserva_Det, 0,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,                      
 IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,                      
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,                      
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,                      
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,'NAP',@UserMod,                    
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,  
          
 IsNull((Select Top 1 ObservBiblia From OPERACIONES_DET od1 Inner Join RESERVAS_DET rd1 On od1.IDReserva_Det=rd1.IDReserva_Det And rd1.Anulado=0        
 Where od1.IDReserva_Det=d.IDReserva_Det),  
            
 Case When @IDProveedor='000544' And Not Exists(Select IDServicio From MASERVICIOS Where IDServicio=d.IDServicio And Transfer=1)  
  And Not Exists(Select IDServicio,IDTipoServ From MASERVICIOS Where IDServicio=d.IDServicio And IDTipoServ='NAP')                     
  Then                   
 dbo.FnServicioHotelDiaAnt(@IDCab,@IDProveedor,d.IDReserva_Det)  + CHAR(13)+CHAR(10)+                   
 dbo.FnServicioHotelDia(@IDCab,@IDProveedor,d.IDReserva_Det)    + CHAR(13)+CHAR(10)+                   
 dbo.FnServicioRestaurantDia(@IDCab,@IDProveedor,d.IDReserva_Det)                  
 End),                
  Case When                 
 (Select p1.IDTipoProv From MASERVICIOS_DET sd1 Inner Join MASERVICIOS s1 On s1.IDServicio=sd1.IDServicio                
  Inner Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor                
  Where sd1.IDServicio_Det=d.IDServicio_Det) In ('001','002') Then 0 Else 1 End as VerBiblia,            
              
-- IsNull((Select IDOperacion_Det From OPERACIONES_DET Where IDReserva_Det=d.IDReserva_Det),0)             
 IsNull((Select Top 1 IDOperacion_Det From OPERACIONES_DET od1 Inner Join RESERVAS_DET rd1 On od1.IDReserva_Det=rd1.IDReserva_Det        
    And rd1.Anulado=0        
 Where od1.IDReserva_Det=d.IDReserva_Det ORder by od1.FecMod Desc),0),d.FlServicioNoShow ,d.IDMoneda,
 (select IDOperacion_Det from OPERACIONES_DET where IDReserva_Det = d.IDReserva_DetAntiguo)        
          
 From RESERVAS_DET d Where IDReserva In (Select IDReserva From RESERVAS               
 Where IDCab=@IDCab And IDProveedor=@IDProveedor              
 And Anulado=0)            
 And d.Anulado=0                    
                  
 Declare @iRowsAff smallint=@@ROWCOUNT - 1                      
 If @iRowsAff>0 Exec Correlativo_Upd 'OPERACIONES_DET_TMP',@iRowsAff                      
