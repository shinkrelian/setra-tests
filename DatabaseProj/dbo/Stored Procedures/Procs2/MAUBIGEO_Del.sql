﻿Create Procedure [dbo].[MAUBIGEO_Del]
	@IDUbigeo char(6)
As

	Set NoCount On

	Delete From MAUBIGEO
	Where IDUbigeo = @IDUbigeo
