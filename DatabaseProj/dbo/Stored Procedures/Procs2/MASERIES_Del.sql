﻿Create Procedure [dbo].[MASERIES_Del]
	@IDSerie char(3)    
As
	Set NoCount On

	Delete From MASERIES 
     Where 
           IDSerie=@IDSerie
