﻿Create Procedure dbo.ORDENPAGO_DET_SelxVoucher
	@IDCab int,
	@IDVoucher int
As
	Set Nocount On
	
	SELECT Distinct IDOrdPag
	FROM ORDENPAGO_DET WHERE IDOrdPag IN (SELECT IDOrdPag FROM ORDENPAGO 
		WHERE IDCab=@IDCab)
	AND IDServicio_Det In 
	(SELECT IDServicio_Det
	FROM OPERACIONES_DET WHERE IDOperacion IN (SELECT IDOperacion FROM OPERACIONES 
		WHERE IDCab=@IDCab)
	AND IDVoucher=@IDVoucher)
	
