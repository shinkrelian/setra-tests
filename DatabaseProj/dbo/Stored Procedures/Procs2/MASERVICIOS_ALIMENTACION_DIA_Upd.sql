﻿CREATE Procedure [dbo].[MASERVICIOS_ALIMENTACION_DIA_Upd]
	@IDServicio char(8),
	@Dia tinyint,
	@Desayuno bit,
	@Lonche bit,
	@Almuerzo bit,
	@Cena bit,
	@IDUbigeoOri	char(6),
	@IDUbigeoDes	char(6),
	@UserMod char(4)
As

	Set NoCount On
	
	Update MASERVICIOS_ALIMENTACION_DIA           
           Set Desayuno=@Desayuno
           ,Lonche=@Lonche
           ,Almuerzo=@Almuerzo
           ,Cena=@Cena
           ,IDUbigeoOri=@IDUbigeoOri
           ,IDUbigeoDes=@IDUbigeoDes
           ,UserMod=@UserMod
           ,FecMod=Getdate()
	Where IDServicio=@IDServicio And Dia=@Dia
