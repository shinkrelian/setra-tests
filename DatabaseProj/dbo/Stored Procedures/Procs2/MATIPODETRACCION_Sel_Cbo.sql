﻿--JHD-20150911- And CoTipoDetraccion not in ('00000','00001','00002')
CREATE PROCEDURE [dbo].[MATIPODETRACCION_Sel_Cbo]    
 @CoTipoDetraccion char(5)='',    
 @bTodos bit,    
 @bDescTodos bit          
AS    
BEGIN    
SELECT * FROM    
(    
 Select '00000' as CoTipoDetraccion,         
  --'' as Codigo,        
  Case When @bDescTodos = 0 then 'No Aplica'    
  else '<Todos>' End as NoTipoBien,         
          
  0 as Ord              
  Union              
    
 Select    
  [CoTipoDetraccion],    
   NoTipoBien=cast(cast(round(SsTasa*100,2) as numeric(5,2)) as varchar(10))+'% - '+[NoTipoBien],    
    1 as Ord       
  From [dbo].[MATIPODETRACCION]    
 Where     
  (@CoTipoDetraccion='' or [CoTipoDetraccion] like '%'+@CoTipoDetraccion+'%')     
 --  And CoTipoDetraccion<>'00000'        
   And CoTipoDetraccion not in ('00000','00001','00002','00003','00004')
 ) AS X      
 Where (@bTodos=1 Or Ord<>0)              
 Order by Ord,2        
END; 
