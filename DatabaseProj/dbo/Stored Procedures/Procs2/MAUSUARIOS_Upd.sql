﻿--JRF-20130802-Nuevo Campo FlSegPersonalizada.  
--JRF-20130806-Nuevo campo IDUbigeo
--JHD-20150701-Nuevos campos: TxApellidos,FecIngreso,CoSexo,CoTipoDoc,NumIdentidad,CoEmpresa,CoCargo,CoUserJefe,NuAnexo,TxLema,NoArchivoFoto
--JHD-20150713-@Direccion varchar(max)
--JHD-20150713-Agregar campo @NuOrdenArea 
--JHD-20150715-Agregar campo @FlTrabajador_Activo 
--JHD-20160219-@FecCese smalldatetime,
--JRF-20160312-,CoUbicacionOficina = Case When Ltrim(Rtrim(@CoUbicacionOficina))='' then Null else @CoUbicacionOficina End ...
--JHD-20160331-Agregar campo @Celular_Trabajo
CREATE Procedure [dbo].[MAUSUARIOS_Upd]               
 @IDUsuario char(4),      
 @Usuario varchar(20),      
 @Nombre varchar(60),      
 @Direccion varchar(max),      
 @Telefono varchar(12),      
 @Celular varchar(12),      
 @Nivel char(3),      
 @Password varchar(max),      
 @Siglas char(3),      
 @FechaNac smalldatetime,      
 @Directorio varchar(50),      
 @Correo varchar(50),      
 @IdArea char(2),     
 @FlSegPersonalizada bit,   
 @EsVendedor bit,      
 @IDUbigeo char(6),
 @CambioPassword bit,
 @UserMod char(4),
 --
 @TxApellidos varchar(60)='',
@FecIngreso smalldatetime='01/01/1900',
@CoSexo char(1)='',
@CoTipoDoc char(3)='',
@NumIdentidad varchar(15)='',
@CoEmpresa tinyint=0,
@CoCargo tinyint=0,
@CoUserJefe char(4)='',
@NuAnexo varchar(5)='',
@TxLema varchar(255)='',
@NoArchivoFoto varchar(200)='',
@FlTrabajador_Activo bit=1,    
@NuOrdenArea tinyint=0,
@FecCese smalldatetime='01/01/1900',
@CoUbicacionOficina varchar(15),
@Celular_Trabajo varchar(12)
As      
      
 Set NoCount On      
      
 UPDATE MAUSUARIOS      
    SET Usuario=@Usuario      
		   ,Nombre=@Nombre      
           ,Direccion=Case When Ltrim(Rtrim(@Direccion))='' Then Null Else @Direccion End      
           ,Telefono=Case When Ltrim(Rtrim(@Telefono))='' Then Null Else @Telefono End      
           ,Celular=Case When Ltrim(Rtrim(@Celular))='' Then Null Else @Celular End      
           ,Nivel=@Nivel      
           ,Password=@Password      
           ,Siglas=Case When Ltrim(Rtrim(@Siglas))='' Then Null Else @Siglas End      
           ,FechaNac=Case When @FechaNac='01/01/1900' Then Null Else @FechaNac End      
           ,Directorio=Case When Ltrim(Rtrim(@Directorio))='' Then Null Else @Directorio End      
           ,Correo=@Correo      
           ,IdArea=Case When Ltrim(Rtrim(@IdArea))='' Then Null Else @IdArea End      
           ,EsVendedor = @EsVendedor      
           ,FlSegPersonalizada = @FlSegPersonalizada  
           ,IDUbigeo = @IDUbigeo
           ,UserMod=@UserMod      
           ,CambioPassword = @CambioPassword
           ,FecMod=getdate()
		   ,TxApellidos=Case When Ltrim(Rtrim(@TxApellidos))='' Then Null Else @TxApellidos End  
		   ,FecIngreso=Case When @FecIngreso='01/01/1900' Then Null Else @FecIngreso End  
		   ,CoSexo=Case When Ltrim(Rtrim(@CoSexo))='' Then Null Else @CoSexo End  
		   ,CoTipoDoc=Case When Ltrim(Rtrim(@CoTipoDoc))='' Then Null Else @CoTipoDoc End  
		   ,NumIdentidad=Case When Ltrim(Rtrim(@NumIdentidad))='' Then Null Else @NumIdentidad End  
		   ,CoEmpresa=Case When @CoEmpresa=0 Then Null Else @CoEmpresa End  
		   ,CoCargo=Case When @CoCargo=0 Then Null Else @CoCargo End  
		   ,CoUserJefe=Case When Ltrim(Rtrim(@CoUserJefe))='' Then Null Else @CoUserJefe End  
		   ,NuAnexo=Case When Ltrim(Rtrim(@NuAnexo))='' Then Null Else @NuAnexo End  
		   ,TxLema=Case When Ltrim(Rtrim(@TxLema))='' Then Null Else @TxLema End  
		   ,NoArchivoFoto=Case When Ltrim(Rtrim(@NoArchivoFoto))='' Then Null Else @NoArchivoFoto End 
		   ,NuOrdenArea=Case When @NuOrdenArea=0 Then Null Else @NuOrdenArea End
		   ,FlTrabajador_Activo=@FlTrabajador_Activo
		   ,FecCese=Case When @FecCese='01/01/1900' Then Null Else @FecCese End 
		   ,CoUbicacionOficina = Case When Ltrim(Rtrim(@CoUbicacionOficina))='' then Null else @CoUbicacionOficina End 
		   ,Celular_Trabajo=Case When Ltrim(Rtrim(@Celular_Trabajo))='' Then Null Else @Celular_Trabajo End      
 WHERE IDUsuario=@IDUsuario     
