﻿CREATE Procedure [dbo].[RESERVAS_DET_TMP_Upd]
 @IDReserva_Det int,                  
 @IDReserva int,                  
 @IDFile int,                  
 @IDDet int,                  
 @Item tinyint,                  
 @Dia smalldatetime,                  
 @FechaOut smalldatetime,                  
 @Noches tinyint,                  
 @IDubigeo char(6),                  
 @IDServicio char(8),                  
 @IDServicio_Det int,                  
 @Especial bit,                  
 @MotivoEspecial varchar(200),                  
 @RutaDocSustento varchar(200),                  
 @Desayuno bit,                  
 @Lonche bit,                  
 @Almuerzo bit,                  
 @Cena bit,                  
 @Transfer bit,                  
 @IDDetTransferOri int,                  
 @IDDetTransferDes int,                  
 @CamaMat tinyint=0,                  
 @TipoTransporte char(1),                  
 @IDUbigeoOri char(6),                  
 @IDUbigeoDes char(6),                  
 @IDIdioma varchar(12),                  
 @NroPax smallint,                  
 @NroLiberados smallint,                  
 @Tipo_Lib char(1),                  
 @Cantidad tinyint,                  
 @CantidadAPagar numeric(5,2),              
 @CostoReal numeric(8,2),                  
 @CostoRealAnt numeric (8,2),                  
 @CostoLiberado numeric (8,2),                  
 @Margen numeric(8,2),                  
 @MargenAplicado numeric (6,2),                  
 @MargenAplicadoAnt numeric (6,2),                  
 @MargenLiberado numeric (6,2),                  
 @Total numeric (8,2),                  
 @TotalOrig numeric (8,2),                  
 @CostoRealImpto numeric(8,2),                  
 @CostoLiberadoImpto numeric(8,2),                  
 @MargenImpto numeric(8,2),                  
 @MargenLiberadoImpto numeric(8,2),                  
 @TotImpto numeric (8,2),                
 @NetoHab numeric(8,2),              
 @IgvHab numeric(5,2),              
 @TotalHab numeric(8,2),              
 @NetoGen numeric(8,2),              
 @IgvGen numeric(5,2),              
 @TotalGen numeric(8,2),                
 --@DescGuia varchar(250),            
 @IDGuiaProveedor char(6),      
 --@DescBus varchar(250),            
 @NuVehiculo smallint,    
 @IDDetRel int,                  
 @Servicio varchar(max),                  
 @IDReserva_DetCopia int,                  
 @IDReserva_Det_Rel int,                  
 @IDEmailEdit varchar(255),                  
 @IDEmailNew varchar(255),                  
 @IDEmailRef varchar(255),                  
 @CapacidadHab tinyint,                
 @EsMatrimonial bit,                
 @IDMoneda char(3),          
 @UserMod char (4)                  
As                  
 Set NoCount On                  
        
 Update RESERVAS_DET_TMP                   
 Set                   
 IDReserva= @IDReserva                  
 ,IDFile= @IDFile                  
 ,IDDet= Case When @IDDet=0 Then Null Else @IDDet End                  
 ,Item= @Item                  
 ,Dia= @Dia                  
 ,DiaNombre= DATENAME(dw,@Dia)                  
 ,FechaOut=Case When @FechaOut='01/01/1900' Then Null Else @FechaOut End                  
 ,Noches=Case When @Noches=0 Then Null Else @Noches End                  
 ,IDubigeo= @IDubigeo                  
 ,IDServicio= @IDServicio                  
 ,IDServicio_Det= @IDServicio_Det                  
 ,Especial= @Especial                  
 ,MotivoEspecial= Case When ltrim(rtrim(@MotivoEspecial))='' Then Null Else @MotivoEspecial End                  
 ,RutaDocSustento= Case When ltrim(rtrim(@RutaDocSustento))='' Then Null Else @RutaDocSustento End                  
 ,Desayuno= @Desayuno                   
 ,Lonche= @Lonche                  
 ,Almuerzo= @Almuerzo                  
 ,Cena= @Cena                  
 ,Transfer= @Transfer                  
 ,IDDetTransferOri= Case When LTRIM(rtrim(@IDDetTransferOri))='' Then Null Else @IDDetTransferOri End                  
 ,IDDetTransferDes= Case When ltrim(rtrim(@IDDetTransferDes))='' Then Null Else @IDDetTransferDes End                  
 ,CamaMat= Case When ltrim(rtrim(@CamaMat))='' then Null Else @CamaMat End                  
 ,TipoTransporte= Case When LTRIM(rtrim(@TipoTransporte))='' Then Null Else @TipoTransporte End                  
 ,IDUbigeoOri= Case When ltrim(rtrim(@IDUbigeoOri))='' Then Null Else @IDUbigeoOri End                  
 ,IDUbigeoDes= Case When LTRIM(rtrim(@IDUbigeoDes))='' Then Null Else @IDUbigeoDes End                  
        
 ,IDIdioma=@IDIdioma           
        
 ,NroPax= @NroPax                  
 ,NroLiberados= Case When ltrim(rtrim(@NroLiberados))='' Then Null Else @NroLiberados End                  
 ,Tipo_Lib= Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End                  
 ,Cantidad=@Cantidad                
 ,CantidadAPagar = @CantidadAPagar              
 ,IDMoneda = Case When ltrim(rtrim(@IDMoneda))='' Then Null Else @IDMoneda End          
 ,CostoReal= @CostoReal                  
 ,CostoRealAnt= Case When LTRIM(rtrim(@CostoRealAnt))='' Then Null Else @CostoRealAnt End                  
 ,CostoLiberado= @CostoLiberado                  
 ,Margen= @Margen                  
 ,MargenAplicado= @MargenAplicado                  
 ,MargenAplicadoAnt= Case When LTRIM(rtrim(@MargenAplicadoAnt))='' Then Null Else @MargenAplicadoAnt End                  
 ,MargenLiberado= @MargenLiberado                  
 ,Total= @Total                  
 ,TotalOrig= @TotalOrig                  
 ,CostoRealImpto=@CostoRealImpto                  
 ,CostoLiberadoImpto=@CostoLiberadoImpto                  
 ,MargenImpto=@MargenImpto                  
 ,MargenLiberadoImpto=@MargenLiberadoImpto                  
 ,TotImpto= @TotImpto                  
 ,NetoHab  = @NetoHab              
 ,IgvHab  = @IgvHab              
 ,TotalHab = @TotalHab               
 ,NetoGen  = @NetoGen              
 ,IgvGen  = @IgvGen              
 ,TotalGen = @TotalGen              
 ,IDGuiaProveedor = Case When ltrim(rtrim(@IDGuiaProveedor))= '' Then Null Else @IDGuiaProveedor End            
 ,NuVehiculo = Case When @NuVehiculo = 0 Then Null Else @NuVehiculo End            
 ,IDDetRel= Case When LTRIM(rtrim(@IDDetRel))='' Then Null Else @IDDetRel End                  
 ,Servicio= Case When ltrim(rtrim(@Servicio))='' then Null Else @Servicio End                  
 ,IDReserva_DetCopia= Case When LTRIM(rtrim(@IDReserva_DetCopia))='' Then Null Else @IDReserva_DetCopia End                  
 ,IDReserva_Det_Rel= Case When LTRIM(rtrim(@IDReserva_Det_Rel))='' Then Null Else @IDReserva_Det_Rel End                  
 ,IDEmailEdit=Case When LTRIM(rtrim(@IDEmailEdit))='' Then Null Else @IDEmailEdit End                  
 ,IDEmailNew=Case When LTRIM(rtrim(@IDEmailNew))='' Then Null Else @IDEmailNew End                  
 ,IDEmailRef=Case When LTRIM(rtrim(@IDEmailRef))='' Then Null Else @IDEmailRef End                  
 ,CapacidadHab = @CapacidadHab                 
 ,EsMatrimonial=@EsMatrimonial                
 ,UserMod= @UserMod                 
 ,execTrigger=1   
 ,FecMod= GetDate()                  
 Where IDReserva_Det= @IDReserva_Det         