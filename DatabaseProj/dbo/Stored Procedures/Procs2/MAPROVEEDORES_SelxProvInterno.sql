﻿--JRF-20150408-Agregar el Servicio Tour Conductor
CREATE Procedure [Dbo].[MAPROVEEDORES_SelxProvInterno]
@IDCab int =0 
As    
 Set NoCount On    
 select IDProveedor,NombreCorto,IDUsuarioProg,0 as FlServicioTC from MAPROVEEDORES     
 where IDTipoOper='I' And IDTipoProv='003' And Activo='A'  
 Union 
 select cd.IDProveedor,'Tour Conductor' As NombreCorto,p.IDUsuarioProg,FlServicioTC from COTIDET cd Left Join MAPROVEEDORES p on cd.IDProveedor=p.IDProveedor
 where IDCAB=@IDCab and FlServicioTC=1
 Order by 2
