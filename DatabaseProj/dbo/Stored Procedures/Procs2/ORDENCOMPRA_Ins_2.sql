﻿--ALTER
--JHD-20150309- Se agregaron los campos nuevos.
--JHD-20150310- Se agrego el campo CoTipoDetraccion.
--JHD-20150311- Se cambio el campo CoEstado por CoEstado_OC
--JHD-20150313- Permitir ingresar 0 para IGV
--JHD-20150313- Se agregaron los campos FeEntrega y FePago
--JHD-20150316- Case When ltrim(rtrim(@TxDescripcion))='' Then '' Else @TxDescripcion End,
--JHD-20150921- Calculo de Saldo:  @SsTotal+(isnull(@SSDetraccion,0)),
CREATE Procedure [dbo].[ORDENCOMPRA_Ins_2]   
 @CoProveedor char(6),  
 @FeOrdCom smalldatetime,  
 @CoMoneda char(3),  
 @SsImpuestos numeric(8,2) ,  
 @SsTotal numeric(8,2),  
 @UserMod char(4),  
 
 @TxDescripcion varchar(max)='',
	@CoArea tinyint=0,
	@IDContacto char(3)='',
	@CoRubro tinyint=0,
	@UserSolicitante char(4)='',
	@IDAdminist char(2)='',
	@CoCeCos char(6)='',
	@SSDetraccion numeric(8,2)=0,
	@SSIGV numeric(8,2)=0,
	@SSSubTotal numeric(8,2)=0,
    @CoTipoDetraccion CHAR(5)='',
 @CoEstado_OC char(2),
 @pNuOrdComInt int output  
  
As  
BEGIN
 Set Nocount On  
   
   
 Declare @AnioMes char(7)=cast(year(GETDATE()) as CHAR(4))+upper(CAST(GETDATE() AS nvarchar(3)))  
 Declare @vcNuOrdCom varchar(3)  
 Select @vcNuOrdCom=isnull(MAX(CAST(RIGHT(NuOrdCom,3) as smallint)),0)+1 From ORDENCOMPRA Where Left(NuOrdCom,7)=@AnioMes   
 Set @vcNuOrdCom=REPLICATE('0',3-len(@vcNuOrdCom)) +@vcNuOrdCom  
   
 Declare @NuOrdComInt int,@NuOrdCom char(13)=@AnioMes+'COM'+@vcNuOrdCom  
   
 Exec Correlativo_SelOutput 'ORDENCOMPRA',1,10,@NuOrdComInt output             
 --2015 ENECOM-023  
   
 --select convert(varchar,GETDATE(),126)  
 --select cast(year(GETDATE()) as CHAR(4))  
 --select upper(CAST(GETDATE() AS nvarchar(3)))  
  
  declare @IDFormaPago char(3)
  set @IDFormaPago=(select IDFormaPago from MAPROVEEDORES where IDProveedor=@CoProveedor)
  
  declare @DiasCredito tinyint=0
  
  if @IDFormaPago='001'
	  begin
		set @DiasCredito=(select isnull(DiasCredito,0) from MAPROVEEDORES where IDProveedor=@CoProveedor)
	  end
  
   
 Insert Into ORDENCOMPRA
 (NuOrdComInt,
 NuOrdCom,
 CoProveedor,
 FeOrdCom,
 CoMoneda,
 SsImpuestos,
 SsTotal,
 SsSaldo,
 SsDifAceptada,
 
 TxDescripcion,
 			CoArea,
 			IDContacto,
 			CoRubro,
 			UserSolicitante,
 			IDAdminist,
 			CoCeCos,
 			SSDetraccion,
 			SSIGV,
 			SSSubTotal,

 CoTipoDetraccion,
 CoEstado_OC,
 UserMod,
 FeEntrega,
 FePago)  
 
 Values(@NuOrdComInt,
 @NuOrdCom,
 @CoProveedor,
 @FeOrdCom,
 @CoMoneda,
 @SsImpuestos,
 @SsTotal,
 --@SsTotal,
 @SsTotal+(isnull(@SSDetraccion,0)),
 0,
 
 --Case When ltrim(rtrim(@TxDescripcion))='' Then Null Else @TxDescripcion End,
 Case When ltrim(rtrim(@TxDescripcion))='' Then '' Else @TxDescripcion End,
--@TxDescripcion,
--@CoArea,
Case When @CoArea=0 Then Null Else @CoArea End,
--@IDContacto,
Case When ltrim(rtrim(@IDContacto))='' Then Null Else @IDContacto End,
--@CoRubro,
Case When @CoRubro=0 Then Null Else @CoRubro End,
--@UserSolicitante,
Case When ltrim(rtrim(@UserSolicitante))='' Then Null Else @UserSolicitante End,
--@IDAdminist,
Case When ltrim(rtrim(@IDAdminist))='' Then Null Else @IDAdminist End,
--@CoCeCos,
Case When ltrim(rtrim(@CoCeCos))='' Then Null Else @CoCeCos End,
--@SSDetraccion,
Case When @SSDetraccion=0 Then Null Else round(@SSDetraccion,0) End,
@SSIGV,
--Case When @SSIGV=0 Then Null Else @SSIGV End,
--@SSSubTotal,
Case When @SSSubTotal=0 Then Null Else @SSSubTotal End,
 
 Case When ltrim(rtrim(@CoTipoDetraccion))='' Then Null Else @CoTipoDetraccion End,
 
 @CoEstado_OC,
 --@CoTipoDetraccion
 @UserMod
 ,case when @CoEstado_OC='EN' THEN GETDATE() ELSE NULL END  
 ,DATEADD(DAY,@DiasCredito,@FeOrdCom))
 Set @pNuOrdComInt=@NuOrdComInt  
END;
