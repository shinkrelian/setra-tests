﻿CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Del
	@IDOperacion_Det int,
	@IDServicio_Det	int
As  
	Set NoCount On  
	
	Delete from OPERACIONES_DET_DETSERVICIOS
	Where IDOperacion_Det=@IDOperacion_Det And IDServicio_Det=@IDServicio_Det
  