﻿
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_Sel_ExisteRangoOutput]    
    @IDServicio_Det int,      
    @NroPax smallint,    
    @NroLiberados smallint,    
    @pExiste bit Output    
As         
 Set Nocount On      
  
 Set @pExiste=0    
 If Exists(Select Monto From MASERVICIOS_DET_RANGO_APT      
 Where IDServicio_Det=@IDServicio_Det And      
 (@NroPax + @NroLiberados) >= PaxDesde And (@NroPax + @NroLiberados) <= PaxHasta)
  Set @pExiste=1  
 Else  
  If (Select Max(PaxHasta) From MASERVICIOS_DET_RANGO_APT      
  Where IDServicio_Det=@IDServicio_Det)>=40 and (@NroPax+@NroLiberados)>=40
   Set @pExiste=1
