﻿--HLF-20120912-Comentar IDServicio_Det=
--HLF-20120913-Nuevos campos al Update Set Monto_sgl,Monto_sgls,
--Monto_dbl,Monto_dbls,Monto_tri,Monto_tris
--HLF-20150911-@Monto_sgl, @Monto_dbl, @Monto_tri de n(8,2) a n(10,4)
--HLF-20150911-@Monto_sgls, @Monto_dbls, @Monto_tris de n(10,4) a n(12,4)
CREATE Procedure [dbo].[MASERVICIOS_DET_Upd_IDServicio_Det_V_Output]
	@IDServicio char(8),    
	@IDServicio_Det	int,
	@Anio	char(4),
	@Tipo varchar(7),   
	@Codigo	varchar(20),
	@UserMod	char(4),
	@pRegistrosAfectados	tinyint Output
As
	Set Nocount On
	
	Declare @Monto_sgl numeric(10,4),@Monto_sgls numeric(12,4),
	@Monto_dbl numeric(10,4),@Monto_dbls numeric(12,4),
	@Monto_tri numeric(10,4),@Monto_tris numeric(12,4)
	
	Select @Monto_sgl=Monto_sgl,@Monto_sgls=Monto_sgls,
	@Monto_dbl=Monto_dbl,@Monto_dbls=Monto_dbls,
	@Monto_tri=Monto_tri,@Monto_tris=Monto_tris
	From MASERVICIOS_DET
	Where IDServicio_Det=@IDServicio_Det
	
	Update MASERVICIOS_DET Set IDServicio_Det_V=@IDServicio_Det,
	Monto_sgl=@Monto_sgl,Monto_sgls=@Monto_sgls,
	Monto_dbl=@Monto_dbl,Monto_dbls=@Monto_dbls,
	Monto_tri=@Monto_tri,Monto_tris=@Monto_tris,
	UserMod=@UserMod,
	FecMod=GETDATE()	
	Where 
	--IDServicio_Det=
	--(Select Top 1 IDServicio_Det From MASERVICIOS_DET
	--Where IDServicio=@IDServicio And Tipo=@Tipo And Anio=@Anio-1 And Codigo=@Codigo
	--And Not IDServicio_Det_V Is Null 
	--Order by FecMod)
	IDServicio_Det_V		
	In (Select IDServicio_Det From MASERVICIOS_DET 
		Where IDServicio=@IDServicio And 
		Tipo=@Tipo And Anio=@Anio-1 And Codigo=@Codigo			
		)
	 And Anio=@Anio

	Set @pRegistrosAfectados=@@ROWCOUNT

