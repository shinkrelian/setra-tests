﻿
--JRF-20120913-Agregar Filtro de Tipo de Gasto            
--JRF-20120907-Agregar TC (Tipo cambio)              
--HL-20120719-IsNull(d.AnioPreciosCopia,'') as AnioPreciosCopia                
--HL-20120725-Quitar AnioPreciosCopia                
--HL-20120727-d.Codigo                
--HL-20120806-Order by                
--HLF-20120924-Nuevo campo PlanAlimenticio            
--JRF-20130311-Nuevo Campo dActivo           
--JRF-20130722-Campo para Identificar cambios (TipoTriple)        
--JRF-20130730-Cambio Orden De Servicios.      
--JRF-20131205-Agregar AfectoIGV    
--JRF-20140805-Agregar el campo de CoMoneda  
--JRF-20150225-Agregar idTipoOC
--HLF-20150327-dbo.FnServicioMPWP(d.IDServicio_Det) as IDServicio_V  
CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_ListxIDServicio]                  
 @IDServicio char(8),                  
 @Anio char(4),            
 @TipoGasto char(1),          
 @Estado char(1) =''            
As                  
                   
 Set NoCount On                  
                   
 Select d.Anio, d.Descripcion, d.Tipo, d.DetaTipo, d.VerDetaTipo,                 
 d.ConAlojamiento, d.Monto_sgl, d.Monto_dbl, d.Monto_tri,                   
 ISNULL(d2.Anio,'') as VieneAnio,                  
 isnull(d2.IDServicio,'') as VieneIDServicio, isnull(s.Descripcion,'') as VieneServicio,                  
 isnull(s.IDProveedor,'') as VieneIDProveedor, isnull(p.NombreCorto,'') as VieneProveedor,                  
 d2.Tipo as VieneVariante,                  
 d.IDServicio_Det,                  
 d.Desayuno, d.Lonche, d.Almuerzo, d.Cena, IsNull(d.IDServicio_Det_V,0) as IDServicio_Det_V,                
 d.Codigo  ,isnull(d.TC ,'0.00') TC, d.PlanAlimenticio,d.Estado  ,        
 Case When d.IdHabitTriple = '000' Or d.IdHabitTriple = '002'         
 Then ''         
 else         
 Case When d.IDServicio_DetxTriple is Not null         
  then (select sd3.Descripcion         
    from MASERVICIOS_DET sd3 where sd3.IDServicio_Det = d.IDServicio_DetxTriple)        
    +' ('+ ht.Descripcion+')' else        
 ht.Descripcion         
 End          
 End As TipoTriple    ,d.Afecto as AfectoIgv  ,d.CoMoneda,d.idTipoOC,
 dbo.FnServicioMPWP(d.IDServicio_Det) as IDServicio_V  
 From MASERVICIOS_DET d                   
 Left Join MASERVICIOS_DET d2 On ISNULL(d.IDServicio_Det_V,0)=d2.IDServicio_Det                  
 Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio                  
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor                  
 Left Join MAHABITTRIPLE ht On d.IdHabitTriple = ht.IdHabitTriple        
 Where d.IDServicio=@IDServicio And (d.Anio=@Anio Or ltrim(rtrim(@Anio))='')                  
 And (d.TipoGasto=@TipoGasto Or ltrim(rtrim(@TipoGasto))='')            
 And (d.Estado = @Estado Or ltrim(rtrim(@Estado))='')              
 Order by Anio Desc,d.Tipo desc ,d.Descripcion                


