﻿CREATE Procedure dbo.MASERVICIOS_DET_SelOut_Correlativo  
 @IDServicio char(8),   
 @Anio char(4),  
 @Tipo varchar(7),  
 @pCorrelativo tinyint Output  
  
As   
   
 Declare @Correlativo tinyint= (Select IsNull(Max(Correlativo),0)+1 From MASERVICIOS_DET   
  Where IDServicio=@IDServicio And Anio=@Anio And Tipo=@Tipo)  
  
 Set @pCorrelativo = @Correlativo   
