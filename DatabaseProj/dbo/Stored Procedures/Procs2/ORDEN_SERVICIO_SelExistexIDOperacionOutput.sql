﻿Create Procedure [dbo].[ORDEN_SERVICIO_SelExistexIDOperacionOutput]
	@IDCab int,
	@IDOperacion  int,
	@pExiste bit output
As
	Set NoCount On
	
	set @pExiste = 0
	If Exists (Select IDProveedor From ORDEN_SERVICIO Where IDCab=@IDCab And IDProveedor In
		(SELECT IDProveedor_Prg FROM OPERACIONES_DET_DETSERVICIOS WHERE IDOperacion_Det IN
			(SELECT IDOperacion_Det FROM OPERACIONES_DET WHERE IDOperacion=@IDOperacion)
				And Not IDProveedor_Prg Is Null))
		Set @pExiste = 1
	
