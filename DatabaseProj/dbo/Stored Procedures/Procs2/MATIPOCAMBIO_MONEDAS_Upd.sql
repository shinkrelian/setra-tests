﻿
CREATE PROCEDURE [dbo].[MATIPOCAMBIO_MONEDAS_Upd]
	@CoMoneda char(3),
	@Fecha smalldatetime,
	@ValCompra decimal(6,3),
	@ValVenta decimal(6,3),
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[MATIPOCAMBIO_MONEDAS]
	Set
		[ValCompra] = @ValCompra,
 		[ValVenta] = @ValVenta,
 		[UserMod] = @UserMod
 	Where 
		[CoMoneda] = @CoMoneda And 
		[Fecha] = @Fecha
	IF @CoMoneda = 'SOL'
	BEGIN
			UPDATE MATIPOCAMBIO SET ValCompra = @ValCompra,
									ValVenta = @ValVenta,
									UserMod = @UserMod
			WHERE Fecha = @Fecha
	END

END;
