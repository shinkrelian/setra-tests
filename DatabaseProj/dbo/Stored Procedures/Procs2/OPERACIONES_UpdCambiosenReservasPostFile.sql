﻿Create Procedure dbo.OPERACIONES_UpdCambiosenReservasPostFile
	@IDOperacion int,
	@CambiosenReservasPostFile bit,
	@UserMod char(4)
As
	Set Nocount On
	
	Update OPERACIONES Set CambiosenReservasPostFile=@CambiosenReservasPostFile, 
	UserMod=@UserMod, FecMod=GETDATE()
	Where IDOperacion=@IDOperacion
	
