﻿Create Procedure dbo.PEDIDO_SelTieneFileOutput
	@NuPedInt int,
	@IDCab int,
	@pYaTieneFile bit output
As
	Set NoCount On

	Set @pYaTieneFile = 0

	If Exists(Select IDCAB From COTICAB Where NuPedInt=@NuPedInt And Estado='A' And IDCAB<>@IDCab)
		Set @pYaTieneFile = 1

