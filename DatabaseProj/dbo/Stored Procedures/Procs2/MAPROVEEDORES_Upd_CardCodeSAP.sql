﻿--CREATE
CREATE Procedure dbo.MAPROVEEDORES_Upd_CardCodeSAP
@CardCodeSAP varchar(15),
@IDProveedor char(6)
As      
 UPDATE  MAPROVEEDORES
    SET CardCodeSAP=@CardCodeSAP
 WHERE IDProveedor=@IDProveedor     
