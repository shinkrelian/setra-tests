﻿Create Procedure [dbo].[MATIPOIDENT_Del]
	@IDIdentidad char(3)
As

	Set NoCount On

	Delete From MATIPOIDENT
	Where IDIdentidad=@IDIdentidad
