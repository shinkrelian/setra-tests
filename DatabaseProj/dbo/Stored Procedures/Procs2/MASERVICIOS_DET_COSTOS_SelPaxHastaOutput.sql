﻿Create Procedure dbo.MASERVICIOS_DET_COSTOS_SelPaxHastaOutput
	@IDServicio_Det int,
	@NroPax	smallint,
	@pPaxHasta	int output
As
	Set Nocount On
	
	Select @pPaxHasta=PaxHasta From MASERVICIOS_DET_COSTOS 
	Where IDServicio_Det=@IDServicio_Det And
	@NroPax Between PaxDesde And PaxHasta
	
	Set @pPaxHasta=ISNULL(@pPaxHasta,0)
