﻿
--JRF-20130726-Nuevo campo (Numeroformapago).
--JHD-20150611-Nuevo campo (SSDetraccion).
--HLF-20151111-CoResponsable
CREATE Procedure dbo.ORDENPAGO_Ins    
 @IDReserva int,    
 @IDCab int,    
 @IDFormaPago char(3),     
 @IDBanco char(3),  
 @FechaPago smalldatetime,      
 @Numeroformapago varchar(20),
 @Porcentaje numeric(5,2),      
 @TCambio numeric(5,2),      
 @Observaciones varchar(max),     
 @CtaCte varchar(30),     
 @IDMoneda char(3),     
 @UserMod char(4),
 @SSDetraccion numeric(12,2)=0,
 @pIDOrdPag int Output    
As    
 Set Nocount on    
 Declare @IDOrdPag int    
 Execute dbo.Correlativo_SelOutput 'ORDENPAGO',1,10,@IDOrdPag output    
     
 Insert  Into ORDENPAGO(IDOrdPag, IDReserva, IDCab, IDFormaPago ,IDBanco , FechaPago, Porcentaje,     
  TCambio, Observaciones, CtaCte, IDMoneda, IDEstado,Numeroformapago,SSDetraccion, UserMod, CoResponsable)    
 Values(@IDOrdPag, @IDReserva, @IDCab, @IDFormaPago ,@IDBanco , @FechaPago, @Porcentaje,     
  Case When @TCambio=0 Then Null Else @TCambio End,     
  Case When ltrim(rtrim(@Observaciones))='' Then Null Else @Observaciones End,        
  Case When ltrim(rtrim(@CtaCte))='' Then Null Else @CtaCte End,     
  @IDMoneda, 'GR',case when LTRIM(rtrim(@Numeroformapago))='' then null else @Numeroformapago End,
  @SSDetraccion,
   @UserMod, @UserMod)    
      
 Set @pIDOrdPag=@IDOrdPag  



