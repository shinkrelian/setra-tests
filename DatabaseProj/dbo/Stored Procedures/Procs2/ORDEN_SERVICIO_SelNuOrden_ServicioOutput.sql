﻿--HLF-20151119-Quitando el Exists, quien lo haya puesto mas vale que nunca lo diga...
--JRF-20160330-Agregando @IDProveedorSetours
CREATE Procedure [dbo].[ORDEN_SERVICIO_SelNuOrden_ServicioOutput]
	@IDCab int,
	@IDProveedor char(6),
	@IDProveedorSetours char(6),
	@pNuOrden_Servicio int output
As
	Set NoCount On
	Set @pNuOrden_Servicio = 0
	--If Exists(select CoOrden_Servicio from ORDEN_SERVICIO where IDCab =@IDCab and IDProveedor =@IDProveedor)
	--set @pNuOrden_Servicio = (select NuOrden_Servicio from ORDEN_SERVICIO where IDCab =@IDCab and IDProveedor =@IDProveedor)
	select @pNuOrden_Servicio=NuOrden_Servicio from ORDEN_SERVICIO 
	where IDCab =@IDCab and IDProveedor =@IDProveedor And Ltrim(Rtrim(IsNull(IDProveedorSetours,'')))=Ltrim(Rtrim(@IDProveedorSetours))
