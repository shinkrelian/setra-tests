﻿CREATE PROCEDURE [dbo].[RESERVAS_DET_BUP_Sel_xIDReserva]
	@IDReserva	int
	
As
	Set Nocount On
	
	Select 
	upper(substring(DATENAME(dw,d.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,d.Dia,103)))  as FechaIn, 
	IsNull(upper(substring(DATENAME(dw,d.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,d.FechaOut,103))),'') as FechaOut, 
	d.Cantidad, 	
	d.Servicio, 
	IsNull(d.Noches,0) as Noches, 
	d.NroPax,
	IsNull(d.CodReservaProv,'') as CodReservaProv	
	From RESERVAS_DET_BUP d 
	Where IDReserva=@IDReserva
