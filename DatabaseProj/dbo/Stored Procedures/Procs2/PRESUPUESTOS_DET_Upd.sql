﻿--JRF-20120906-Nuevo Campo FechaDetalle.  
CREATE Procedure PRESUPUESTOS_DET_Upd    
@IDPresupuesto_Det int,    
@IDPresupuesto int,    
@IDServicio_Det int,    
@Servicio varchar(100),    
@MontoSoles numeric(8,2),    
@MontoDolares numeric(8,2),    
@LiquidaSoles numeric(8,2),    
@LiquidaDolares numeric(8,2),    
@SaldoSoles numeric(8,2),    
@SaldoDolares numeric(8,2),    
@IDTipoDoc char(3),    
@NroSerieDocumento varchar(4),    
@NroDocumento varchar(12),    
@FechaDetalle smalldatetime,  
@UserMod char(4)    
As    
 Set NoCount On    
 UPDATE [dbo].[PRESUPUESTOS_DET]    
    SET [IDServicio_Det] =Case When @IDServicio_Det  = 0 Then Null Else @IDServicio_Det End
    ,[Servicio] = @Servicio    
    ,[MontoSoles] = Case When @MontoSoles = 0 then Null Else @MontoSoles End    
    ,[MontoDolares] = Case When @MontoDolares = 0 then Null Else @MontoDolares End    
    ,[LiquidaSoles] = Case When @LiquidaSoles = 0 then Null Else @LiquidaSoles End    
    ,[LiquidaDolares] = Case When @LiquidaDolares = 0 Then Null Else @LiquidaDolares End    
    ,[SaldoSoles] = Case When @SaldoSoles = 0 Then Null Else @SaldoSoles End    
    ,[SaldoDolares] = Case When @SaldoDolares = 0 Then Null Else @SaldoDolares End    
    ,[IDTipoDoc] = Case When ltrim(rtrim(@IDTipoDoc)) = '' Then Null Else @IDTipoDoc End    
    ,[NroSerieDocumento] = Case When ltrim(rtrim(@NroSerieDocumento))= '' Then Null Else @NroSerieDocumento End    
    ,[NroDocumento] = Case When ltrim(rtrim(@NroDocumento)) = '' Then Null Else @NroDocumento End    
    ,[FechaDetalle] = @FechaDetalle  
    ,[UserMod] = @UserMod    
    ,[FecMod] = Getdate()    
  WHERE [IDPresupuesto_Det] = @IDPresupuesto_Det    
