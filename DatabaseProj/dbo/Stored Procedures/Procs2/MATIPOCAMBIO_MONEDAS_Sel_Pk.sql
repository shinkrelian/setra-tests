﻿
CREATE PROCEDURE [dbo].[MATIPOCAMBIO_MONEDAS_Sel_Pk]
	@Fecha		smalldatetime,
	@CoMoneda	char(3)
AS
BEGIN
	Set NoCount On
	
	Select CoMoneda, Fecha, ValCompra, ValVenta, UserMod, FecMod From MATIPOCAMBIO_MONEDAS 
	Where CoMoneda = @CoMoneda AND Fecha = @Fecha
END
