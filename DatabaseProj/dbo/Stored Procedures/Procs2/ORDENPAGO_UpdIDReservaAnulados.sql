﻿--HLF-20141126-
  --(Select IDReserva_Det From RESERVAS_DET   
  --Where IDServicio_Det=opd.IDServicio_Det And Anulado=0  And NroPax=opd.NroPax And NroLiberados=opd.NroLiberados
  --And IDReserva=@IDReservaNue)        

--Where IDOrdPag In (SELECT IDOrdPag FROM ORDENPAGO WHERE IDReserva=@IDReservaNue)

--If Not @@IDReservaNue Is Null       
CREATE Procedure dbo.ORDENPAGO_UpdIDReservaAnulados        
 @IDProveedor char(6),        
 @IDCab int,        
 @UserMod char(4)        
As        
	Set Nocount On        
      
	Declare @IDReservaAnul int =        
	(Select Top 1 IDReserva From RESERVAS Where IDCab=@IDCab And         
	IDProveedor=@IDProveedor And Anulado=1 Order by IDReserva Desc)      
     
	--If Not @IDReservaAnul Is Null       
	--	Begin      
		Declare @IDReservaNue int =        
		(Select IDReserva From RESERVAS Where IDCab=@IDCab And         
		IDProveedor=@IDProveedor And Anulado=0)         
		     
		--Declare @IDOrdPag int=(SELECT IDOrdPag FROM ORDENPAGO WHERE IDReserva=@IDReservaNue)        
		--Declare @IDOrdPag int=(SELECT IDOrdPag FROM ORDENPAGO WHERE IDReserva=@IDReservaAnul)        
		   
		If Not @IDReservaNue Is Null    
			Begin
			Update ORDENPAGO Set IDReserva=@IDReservaNue,        
			UserMod=@UserMod, FecMod=GETDATE()        
			Where IDCab=@IDCab And IDReserva=@IDReservaAnul        
			   
			     
			--Set @IDReservaNue =        
			--(Select IDReserva From RESERVAS Where IDCab=@IDCab And         
			--IDProveedor=@IDProveedor And Anulado=0)         
			   
			--Declare @IDOrdPag INT=(SELECT IDOrdPag FROM ORDENPAGO WHERE IDReserva=@IDReservaNue)        
			   
			Update ORDENPAGO_DET Set IDReserva_Det=          
			--(Select Top 1 IDReserva_Det From RESERVAS_DET   
			--Where IDServicio_Det=opd.IDServicio_Det And Anulado=0        
			--And IDReserva=@IDReservaNue Order by IDReserva_Det Desc)          
			(Select IDReserva_Det From RESERVAS_DET   
			Where IDServicio_Det=opd.IDServicio_Det And Anulado=0  And NroPax=opd.NroPax And NroLiberados=opd.NroLiberados
			And IDReserva=@IDReservaNue)        

			,UserMod=@UserMod, FecMod=GETDATE()        
			From ORDENPAGO_DET opd         
			--Where IDOrdPag=@IDOrdPag        
			Where IDOrdPag In (SELECT IDOrdPag FROM ORDENPAGO WHERE IDReserva=@IDReservaNue)
		       
			End  
	   