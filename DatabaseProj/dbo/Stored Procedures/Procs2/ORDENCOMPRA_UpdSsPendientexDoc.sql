﻿--JHD-20150821-No actualizar estado a Aceptado por BD
CREATE Procedure dbo.ORDENCOMPRA_UpdSsPendientexDoc      
@NuOrdComInt int,      
@SsTotalOrig numeric(9,2),      
@UserMod char(4)      
As      
Set NoCount On      
Update ORDENCOMPRA      
 Set SsSaldo=SsSaldo-@SsTotalOrig,      
  UserMod=@UserMod,      
  FecMod=GETDATE()      
where NuOrdComInt=@NuOrdComInt  
      
Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from ORDENCOMPRA where NuOrdComInt=@NuOrdComInt)      
  
--if @SsSaldoPend = 0      
-- Update ORDENCOMPRA set CoEstado='AC' where NuOrdComInt=@NuOrdComInt      
