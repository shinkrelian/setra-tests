﻿CREATE PROCEDURE [dbo].[MAPROVEEDORES_MATIPODETRACCION_Sel_List_Prov]
@CoProveedor char(6)--='000035'
AS
select CoTipoDetraccion,
NoTipoBien=cast(cast(round(SsTasa*100,2) as numeric(5,2)) as varchar(10))+'% - '+[NoTipoBien],
Activo=case when ((select count(*) from MAPROVEEDORES_MATIPODETRACCION d where d.IDProveedor=@CoProveedor and d.CoTipoDetraccion=MATIPODETRACCION.CoTipoDetraccion))>0 then cast(1 as bit) else cast(0 as bit) end,
FlSAP=case when ((select count(*) from MAPROVEEDORES_MATIPODETRACCION d where d.IDProveedor=@CoProveedor and d.CoTipoDetraccion=MATIPODETRACCION.CoTipoDetraccion))>0 then cast(1 as bit) else cast(0 as bit) end
from MATIPODETRACCION
where cosap is not null
