﻿CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_List]
	@IDServicio char(8),
	@Anio	char(4),
	@Descripcion	varchar(200)
As
	
	Set NoCount On
	
	Select Anio, Tipo, Descripcion, Monto_sgl,IDServicio_Det From MASERVICIOS_DET 
	Where IDServicio=@IDServicio And 
	(Anio=@Anio or rtrim(ltrim(@Anio))='')And
	(Descripcion Like '%'+@Descripcion+'%' Or rtrim(ltrim(@Descripcion))='')
	Order by Anio, Tipo, Correlativo
