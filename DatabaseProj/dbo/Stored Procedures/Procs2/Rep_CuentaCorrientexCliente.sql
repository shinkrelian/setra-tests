﻿

--JHD-20141030-Se cambio el filtro de @FecEmisionRango1 y @FecEmisionRango2 para que sean opcionales
--JHD-20141216-Se cambio el Campo SsRecibido por SSOrdenado (SsRecibido=IFF.SsOrdenado)
--HLF-20150311-abs(z.SsTotalDocumUSD) - abs(z.SsMonto) as Saldo
--HLF-20150508-Case When DMD.IDTipoDoc IN ('NCA','NCR') ...
CREATE Procedure dbo.Rep_CuentaCorrientexCliente
@IDCliente char(6),              
@NuDocum char(10),              
@DiaVencidos smallint,          
@IDDebitMemo char(10),          
@IDBanco char(3),              
@FecEmisionRango1 smalldatetime,              
@FecEmisionRango2 smalldatetime,    
@FecIngresoRango1 smalldatetime,    
@FecIngresoRango2 smalldatetime   
As
Set NoCount On

Select  ROW_NUMBER()Over(Partition by Z.NumOperacion Order By Z.FechaOperacion, Z.IDTipoDoc) As Item,Z.*,
	ROW_NUMBER()Over(Partition by Z.Serie+Z.Numero Order By Z.FechaOperacion, Z.IDTipoDoc) as ItemSumTotalDocum,
	abs(z.SsTotalDocumUSD) - abs(z.SsMonto) as Saldo
From (

Select Distinct X.* From(
	SELECT 
	--Cobranzas
	'File : '+c.IDFile+' - '+c.Titulo +' ('+CL.RazonComercial+')'as DescrGrupo,BIF.Sigla as SiglaBancoCob,
	iff.CoOperacionBan As NumOperacion, iff.FeAcreditada as FechaOperacion,
	FPG.Descripcion as TipOperacion,
	SsRecibido=IFF.SsOrdenado,
	isnull(IFF.SsGastosTransferencia,0) as SsGastosTransferencia, IFF.SsBanco,
	IFF.IDMoneda as MonedaIngreso1,
	--Case when DATEDIFF(d,DM.FecVencim,@FecEmisionRango2) < 0 then 0 else DATEDIFF(d,DM.FecVencim,@FecEmisionRango2)End as DiasVencidos,
	Case When (Select DATEDIFF(d,FeDocum,@FecEmisionRango2) from DEBIT_MEMO DM2 Where DM2.IDDebitMemo=DM.IDDebitMemo)<0 then 0 
		 else (Select DATEDIFF(d,FeDocum,@FecEmisionRango2) from DEBIT_MEMO DM2 Where DM2.IDDebitMemo=DM.IDDebitMemo) End as DiasVencidos,
	--Documento
	DMD.IDTipoDoc,SUBSTRING(DMD.NuDocum,1,3) as Serie,SUBSTRING(DMD.NuDocum,4,10) as Numero, D.FeDocum,
	--Case When DMD.IDTipoDoc = 'NCA' Then D.SsTotalDocumUSD*-1 else D.SsTotalDocumUSD End as SsTotalDocumUSD, 
	Case When DMD.IDTipoDoc IN ('NCA','NCR') Then D.SsTotalDocumUSD*-1 else D.SsTotalDocumUSD End as SsTotalDocumUSD, 
	D.IDMoneda MonedaDocum, DMD.SsMonto, 
	IFF.IDMoneda as MonedaIngreso2
	FROM INGRESO_DEBIT_MEMO IDM 
	INNER JOIN DEBIT_MEMO DM ON IDM.IDDebitMemo=DM.IDDebitMemo  --and SUBSTRING(DM.IDDebitMemo,9,10)=dbo.FnCorrelativoDMValido(DM.IDCab)
	INNER JOIN INGRESO_FINANZAS IFF ON IDM.NuIngreso=IFF.NuIngreso
	inner join INGRESO_DOCUMENTO DMD ON IDM.NuIngreso=DMD.NuIngreso
	inner join DOCUMENTO D ON DMD.IDTipoDoc=D.IDTipoDoc AND DMD.NuDocum=D.NuDocum
	Left Join COTICAB c On D.IDCab = c.IDCAB Left Join MACLIENTES CL On IFF.IDCliente=CL.IDCliente
	Left Join MABANCOS BIF On IFF.IDBanco=BIF.IDBanco Left Join MAFORMASPAGO FPG on IFF.IDFormaPago= FPG.IdFor
	Where (ltrim(rtrim(@IDCliente))='' Or CL.IDCliente=@IDCliente) and
	(Ltrim(rtrim(@NuDocum))='' Or D.NuDocum =@NuDocum) and (Ltrim(RTRIM(@IDBanco))='000' Or IFF.IDBanco=@IDBanco) and
	(Ltrim(rtrim(@IDDebitMemo))='' Or DM.IDDebitMemo=@IDDebitMemo)
	-- and
	--D.FeDocum Between @FecEmisionRango1 And @FecEmisionRango2+' 23:59:00'        
) X

where (@DiaVencidos=0 Or X.DiasVencidos=@DiaVencidos) And 
(CONVERT(char(10),@FecIngresoRango1,103)='01/01/1900' Or  
X.FechaOperacion between @FecIngresoRango1 and @FecIngresoRango2)
AND
(CONVERT(char(10),@FecEmisionRango1,103)='01/01/1900' Or     
X.FeDocum between @FecEmisionRango1 and @FecEmisionRango2+' 23:59:59'    )
) Z
Order By Z.FechaOperacion, Z.IDTipoDoc
