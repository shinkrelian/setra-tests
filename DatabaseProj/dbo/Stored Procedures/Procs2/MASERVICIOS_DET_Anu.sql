﻿Create Procedure dbo.MASERVICIOS_DET_Anu
@IDServicio_Det int,
@Estado char(1),
@UserMod char(4)
As
	Set NoCount On
	Update MASERVICIOS_DET
		Set Estado=@Estado ,
			UserMod= @UserMod,
			FecMod= GetDate()
	Where IDServicio_Det = @IDServicio_Det
