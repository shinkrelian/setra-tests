﻿
------------------------------------------------------------------------------------------------

--ALTER
--JHD-20141127-Agregar Nuevos campos: QtCantTriple,CoTipCama_Triple
--JHD-20141230-Agregar Nuevo campo: CoTipoHab_Oper

CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_HABITACIONES_Ins]
	@CoServicio char(8),
	--@NuHabitacion tinyint,
	@CoTipoHab tinyint=0,
	@QtCantidad tinyint=0,
	@QtCantMatri tinyint=0,
	@CoTipCama_Matri tinyint=0,
	@QtCantTwin tinyint=0,
	@CoTipCama_Twin tinyint=0,
	@CoTipCama_Adic tinyint=0,
	@QtCantTriple tinyint=0,
	@CoTipCama_Triple tinyint=0,
	@UserMod char(4)='',
	@CoTipoHab_Oper varchar(7)=''
AS
BEGIN
	Declare @NuHabitacion tinyint =(select Isnull(Max(NuHabitacion),0)+1   
        from MASERVICIOS_HOTEL_HABITACIONES
        where CoServicio=@CoServicio )  

	Insert Into [dbo].[MASERVICIOS_HOTEL_HABITACIONES]
		(
			[CoServicio],
 			[NuHabitacion],
 			[CoTipoHab],
 			[QtCantidad],
 			[QtCantMatri],
 			[CoTipCama_Matri],
 			[QtCantTwin],
 			[CoTipCama_Twin],
 			[CoTipCama_Adic],
 			QtCantTriple,
			CoTipCama_Triple,
 			[UserMod],
 			CoTipoHab_Oper
 		)
	Values
		(
			@CoServicio,
 			@NuHabitacion,
 			--@CoTipoHab,
 			Case When @CoTipoHab=0 Then Null Else @CoTipoHab End,
 			@QtCantidad,
 			@QtCantMatri,
 			Case When @CoTipCama_Matri=0 Then Null Else @CoTipCama_Matri End,
 			@QtCantTwin,
 			Case When @CoTipCama_Twin=0 Then Null Else @CoTipCama_Twin End,
 			Case When @CoTipCama_Adic=0 Then Null Else @CoTipCama_Adic End,
 			@QtCantTriple,
 			Case When @CoTipCama_Triple=0 Then Null Else @CoTipCama_Triple End,
 			@UserMod,
 			Case When @CoTipoHab_Oper='' Then Null Else @CoTipoHab_Oper End
 		)
END;
