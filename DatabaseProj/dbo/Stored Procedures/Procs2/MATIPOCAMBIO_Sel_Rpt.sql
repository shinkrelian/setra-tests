﻿Create Procedure [dbo].[MATIPOCAMBIO_Sel_Rpt]
	@Fecha	smalldatetime
As
	Set NoCount On
	
	Select Fecha, ValCompra, ValVenta
	From MATIPOCAMBIO
	Where (Fecha = @Fecha Or @Fecha='01/01/1900')
	order by Fecha
