﻿--HLF-20130530-Agregando Order by        
--JRF-20130611-Cambio Formato Nombre (Apellidos, Nombre)      
--JRF-20130730-Cambio validar Variables.    
--HLF-20140811-Or ltrim(rtrim(@IDTipoProveedor)) IN('005','008')  
 --@IDTipoProveedor varchar(7) ya no char(3)  
--HLF-20140812-case When ltrim(rtrim(@IDTipoProveedor))='005' then...
CREATE Procedure dbo.MAPROVEEDORES_Sel_ListxIDTipoProv          
 @Descripcion varchar(100),          
 @IDUbigeo varchar(6),          
 @IDPais varchar(6),          
 @IDTipoProveedor varchar(7)          
as          
        
 Set Nocount on        
         
 Select IDProveedor,Descripcion         
 From 
 (          
	select IDProveedor,    
	case When ltrim(rtrim(@IDTipoProveedor))='005,008' Or ltrim(rtrim(@IDTipoProveedor)) IN('005','008')  Then 
		case When ltrim(rtrim(@IDTipoProveedor))='005' then
			replace(isnull(ApPaterno,'')+' '+isnull(ApMaterno,'')+', '+isnull(Nombre,''),'-','')     
		else
			NombreCorto
		end
	Else          
		case When ltrim(rtrim(@IDTipoProveedor))='004' Then NombreCorto Else NombreCorto End     
	End     
	As Descripcion          
	,isnull(u.IDPais,'') as Pais          
	from MAPROVEEDORES p Inner Join MAUBIGEO u On p.IDCiudad = u.IDubigeo          
	where (LTRIM(rtrim(@IDTipoProveedor))='' or CHARINDEX(p.IDTipoProv,@IDTipoProveedor) > 0)          
	and Activo = 'A' and (ltrim(rtrim(@IDUbigeo))='' or IDCiudad = @IDUbigeo)          
 ) as X          
 Where (LTRIM(rtrim(@Descripcion))='' or X.Descripcion like '%'+@Descripcion+'%')          
 and X.Pais = @IDPais          
 Order by Descripcion 
