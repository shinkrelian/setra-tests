﻿--HL-20120720-Case When @IDTipoProv=@IDTipoProvOperador   
--HL-20120807-Parametro @IDUbigeo  
--JRF-20130212-Considerar para las copias.
CREATE Procedure [dbo].[MASERVICIOS_Ins_Copiar]  
 @IDServicio char(8),  
 @Descripcion varchar(100),  
 @IDProveedor char(6),  
 @IDUbigeo char(6),  
 @UserMod char(4),  
 @pIDServicioNue char(8) Output  
As  
  
 Set NoCount On  
 --Declare @IDubigeo char(6)=(Select IDubigeo From MASERVICIOS Where IDServicio=@IDServicio)  
  
 Declare @DC char(3)=(Select DC From MAUBIGEO Where IDubigeo=@IDubigeo)   
 Declare @Correl smallint=Isnull((Select IsNull(Max(Right(IDServicio,5)),0) From MASERVICIOS Where IDubigeo=@IDubigeo),0)+1   
 Declare @IDServicioNue char(8)=@DC+ replicate('0',5-Len(Cast(@Correl as varchar(5)))) +Cast(@Correl as varchar(5))  
  
 Declare @IDTipoProv char(3)=(Select IDTipoProv From MAPROVEEDORES Where IDProveedor=@IDProveedor)  
 Declare @IDTipoProvOperador char(3)='003'  
    
 Insert Into MASERVICIOS  
 (IDServicio  
  ,IDProveedor  
  ,IDTipoProv             
  ,IDTipoServ   
  ,Transfer  
  ,TipoTransporte    
  ,Descripcion  
  ,Tarifario  
  ,Descri_tarifario                       
  ,FecCaducLectura  
  ,IDubigeo  
  ,Telefono  
  ,Celular  
  ,Comision  
  ,IDCat  
  ,Categoria  
  ,Capacidad  
  ,Observaciones  
  ,Lectura             
  
  ,AtencionLunes  
  ,AtencionMartes  
  ,AtencionMiercoles  
  ,AtencionJueves  
  ,AtencionViernes  
  ,AtencionSabado  
  ,AtencionDomingo  
  ,HoraDesde   
  ,HoraHasta              
  
  ,DesayunoHoraDesde  
  ,DesayunoHoraHasta  
  --,IDDesayuno   
  
  ,PreDesayunoHoraDesde  
  ,PreDesayunoHoraHasta  
  ,IDPreDesayuno   
    
  ,HoraCheckIn  
  ,HoraCheckOut  
  --,TelefonoRecepcion1   
  --,TelefonoRecepcion2   
  --,FaxRecepcion1   
  --,FaxRecepcion2   
  
  ,Dias  
    
  ,PoliticaLiberado  
  ,Liberado  
  ,TipoLib  
  ,MaximoLiberado   
  ,MontoL     
  --,IncluyeServiciosDias  
    
      
  ,IDServicioCopia  
  ,IDCabVarios
  ,UserMod)  
  Select @IDServicioNue  
    
  ,@IDProveedor  
  ,IDTipoProv             
  ,IDTipoServ   
  ,Transfer  
  ,TipoTransporte    
  ,@Descripcion  
  ,Tarifario  
  ,Descri_tarifario                       
  ,FecCaducLectura  
  ,@IDubigeo  
  ,Telefono  
  ,Celular  
  --,Comision  
  ,Case When @IDTipoProv=@IDTipoProvOperador Then   
   (Select Margen From MAPROVEEDORES Where IDProveedor=@IDProveedor)  
  Else  
   Comision  
  End  
  ,IDCat  
  ,Categoria  
  ,Capacidad  
  ,Observaciones  
  ,Lectura             
  
  ,AtencionLunes  
  ,AtencionMartes  
  ,AtencionMiercoles  
  ,AtencionJueves  
  ,AtencionViernes  
  ,AtencionSabado  
  ,AtencionDomingo  
  ,HoraDesde   
  ,HoraHasta              
  
  ,DesayunoHoraDesde  
  ,DesayunoHoraHasta  
  --,IDDesayuno   
  
  ,PreDesayunoHoraDesde  
  ,PreDesayunoHoraHasta  
  ,IDPreDesayuno   
  
  ,HoraCheckIn  
  ,HoraCheckOut  
  --,TelefonoRecepcion1   
  --,TelefonoRecepcion2   
  --,FaxRecepcion1   
  --,FaxRecepcion2   
  
  ,Dias    
  ,PoliticaLiberado  
  ,Liberado  
  ,TipoLib  
  ,MaximoLiberado   
  ,MontoL     
  --,IncluyeServiciosDias  
  ,@IDServicio 
  ,IDCabVarios 
  ,@UserMod  
  From MASERVICIOS Where IDServicio=@IDServicio  
    
  Set @pIDServicioNue=@IDServicioNue  
