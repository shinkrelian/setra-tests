﻿

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JHD-20141030-Se agrego el filtro de '@bTodos'
--JHD-20150216- Se agrego el campo TxDefinicion 
CREATE Procedure [dbo].[MATIPOOPERACION_Sel_Cbo]
@bTodos bit=0
As
	Set NoCount On
	
	SELECT IdToc, Descripcion,TxDefinicion FROM 
	(	
	Select '' as IdToc, '' as Descripcion,'' as TxDefinicion , 0 as Ord
	Union
	Select IdToc, Descripcion,TxDefinicion=ISNULL(TxDefinicion,''), 1 as Ord From MATIPOOPERACION	
	) As X
	 Where (@bTodos=1 Or Ord<>0)   
	Order by Ord,Descripcion

