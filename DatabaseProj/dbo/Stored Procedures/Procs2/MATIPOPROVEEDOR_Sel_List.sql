﻿CREATE Procedure [dbo].[MATIPOPROVEEDOR_Sel_List]
	@IDTipoProv char(3),
	@Descripcion varchar(30)	
	
As
	Set NoCount On
	
	Select IDTipoProv, Descripcion From MATIPOPROVEEDOR	
	Where (Descripcion Like '%'+@Descripcion+'%' Or ltrim(rtrim(@Descripcion))='')
	and (IDTipoProv=@IDTipoProv Or LTRIM(RTRIM(@IDTipoProv))='')
