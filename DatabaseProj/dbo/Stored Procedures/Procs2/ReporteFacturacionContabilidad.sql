﻿--[ReporteFacturacionContabilidad] '01/03/14','USD'
--Case When CoEstadoDoc='AN' Then 0 Else          
--HLF-20140616-Comentando filtros          
--HLF-20140626-Comentando DebitMemoPendiente=0          
--HLF-20140929-CONVERT(varchar(6), isnull(FeDocum,'01/01/1900'),112)=@cMes          
--Cast(NumeroDoc as int) por NumeroDoc      
--(dm.IDEstado='PG' OR dm.IDEstado IS NULL)      
--HLF-20141014-isnull(BaseImponible,0)+isnull(IGV,0) End AS ImporteTotal,               
--HLF-20141014-OrdTipoDoc desc, Cast(IDTipoDoc as SMALLINT),Cast(SerieDoc as SMALLINT),Cast(NumeroDoc as int)    
--JHD-20141024-TipoCambio=CASE WHEN @IDMoneda=IDMoneda then 1 else TipoCambio end  
--JHD-20141024-Case When IDTipoDoc='01' and SerieDoc='001' Then SsTotalDocumUSD Else 0 End as ValorFacturadoExport  
--JHD-20141024-se agrego campo IDTipoDoc2 para solucionar el orden de los documentos anulados que no tiene tipo de doc.  
--JHD-20141024-se agrego tabla temporal para corregir el orden de los documentos anulados. 
--HLF-20141222-isnull(ValorFacturadoExport,0)+isnull(BaseImponible,0)+isnull(Exonerada,0)+      
 --isnull(Inafecta,0)+isnull(ISC,0)+isnull(IGV,0)+isnull(OtrosTributos,0) END AS ImporteTotal,                 
 --HLF-20141222-SerieDoc 004
 --JHD-20141223-se agrego una condicion para cuando un cliente no es peruano, se muestre su razon comercial en caso no tenga razon social
		---RazonSocial= case when (select IDPais from MAUBIGEO where IDubigeo=cl.IDCiudad)	='000323' ...
		--- ...
--JHD-20141223- se creo un campo orden_doc para poder ordenar segun tipo de documento;
--				orden_doc = case when IDTipoDocSetra='FAC' THEN 1 WHEN IDTipoDocSetra='BOL' THEN 2 WHEN IDTipoDocSetra='NCR' THEN 3 WHEN IDTipoDocSetra='NCA' THEN 4 WHEN IDTipoDocSetra='NDB' THEN 5 ELSE 6 END
--JHD-20141223- se cambio el order by :  Cast(SerieDoc as SMALLINT),orden_doc,Cast(IDTipoDoc2 as SMALLINT),Cast(NumeroDoc as int)    
--JHD-20141226- Se agregaron las condiciones para los montos con documentos con CoSunat='08' (Notas de Débito)

--JHD-20141226- Se coloco isnull al idfile para que pueda mostrarse en caso que no tengan ingreso_Debit_memo
				-- IDFile=ISNULL(cc.IDFile,
				--		(SELECT IDFILE FROM COTICAB WHERE IDCab=d.idcab)
				--),          
 --JHD-20150408 - Se agrego una condicion para el campo Inafecta, que permitira mostrar los montos para documentos con serie 006:
					--When '006'  Then                
			        --Case When SsIGV = 0 Then SsTotalDocumUSD End            

 --JHD-20150409 - Se agrego una condicion para el campo Inafecta, que permitira mostrar los montos para documentos con serie 002:
					--When '002'  Then                
			        --Case When SsIGV = 0 Then SsTotalDocumUSD End   
 --JHD 20150602- CoCtaContable=CASE WHEN SerieDoc='006' then '704113' else CoCtaContable end					       
 --JHD 20151110- Se cambio la condicion para mostrar monto negativo en notas de credito
 --JHD 20151110- Se cambio la condicion para el tipo de cambio de notas de credito, segun la fecha vinculada
 --JHD 20151201- WHERE (dm.IDEstado IN('PG','PP') OR dm.IDEstado IS NULL)  and d.IDTipoDoc <> 'CBR'    
 --PPMG-20151209-Se cambio el order by algunos datos del campo IDTipoDoc2 no son convertibles a smallint 
CREATE Procedure [dbo].[ReporteFacturacionContabilidad]                
 @FechaMes smalldatetime,              
 @IDMoneda char(3)                
AS
BEGIN
 Set Nocount on                
               
 Declare @cMes char(6)=CONVERT(varchar(6), @FechaMes,112)                
 --Declare @cMesAnt char(6)=CONVERT(varchar(6), Dateadd(m,-1, @FechaMes),112)                
 Declare @Igv as numeric(5,2)=(Select NuIGV From PARAMETRO)              
              
 --Declare @IDMoneda char(3)='SOL'              
 select *  
 from(             
 SELECT SubDiario, NroRegistro, Periodo, FecEmision, FecVencim,              
 IDTipoDoc,IDTipoDocStarSoft,SerieDoc, NumeroDoc, TipoCliente, isnull(CoStarSoft,'') as CoStarSoft,           
           
 Case When CoEstadoDoc='AN' Then '**ANULADO**' Else RazonSocial End as RazonSocial,             
               
           
 Case When CoEstadoDoc='AN' Then 0 Else ValorFacturadoExport End as ValorFacturadoExport,              
               
 Case When CoEstadoDoc='AN' Then 0 Else BaseImponible End as BaseImponible,              
 Case When CoEstadoDoc='AN' Then 0 Else Exonerada End as Exonerada,              
 Case When CoEstadoDoc='AN' Then 0 Else Inafecta End as Inafecta,              
 Case When CoEstadoDoc='AN' Then 0 Else ISC End as ISC,              
 Case When CoEstadoDoc='AN' Then 0 Else IGV End as IGV,              
 Case When CoEstadoDoc='AN' Then 0 Else OtrosTributos End as OtrosTributos,              
 Case When CoEstadoDoc='AN' Then 0 Else          
   
 --isnull(BaseImponible,0)+isnull(IGV,0) End AS ImporteTotal,               
 isnull(ValorFacturadoExport,0)+isnull(BaseImponible,0)+isnull(Exonerada,0)+      
 isnull(Inafecta,0)+isnull(ISC,0)+isnull(IGV,0)+isnull(OtrosTributos,0) END AS ImporteTotal,               
   
 --TipoCambio,   
 TipoCambio=CASE WHEN @IDMoneda=IDMoneda then 1 else TipoCambio end,  
 isnull(CoCtroCosto,'') as CoCtroCosto, isnull(IDFile,'') as IDFile, FeEmisionVinc, IDTipoDocVinc, SerieDocVinc,NumeroDocVinc,              
 IDMoneda,CoTipoVenta, @Igv as IgvPar,              
 (Select TxTextoArchivoTxt From Correlativo Where Tabla='DOCUMENTO' AND CoSerie=Z.SerieDoc AND (IDTipoDoc=Z.IDTipoDocSetra or IDTipoDoc2=Z.IDTipoDocSetra)) as Texto,              
 Case When CoEstadoDoc='AN' Then '0' Else '1' End as Anulado,              
 Case When IDTipoDocSetra='FAC' and SerieDoc='001' Then '1' Else '0' End as Exportacion,              
  CoCtaContable=CASE WHEN SerieDoc='006' then '704113' else CoCtaContable end
 , FecEmision as FecRegistro,CoEstadoDoc,    
 Case When IDTipoDoc Is Null Then 0 Else 1 End OrdTipoDoc--,IDDebitMemo--,IDCAB    
 --jorge  
 ,IDTipoDoc2=case when IDTipoDoc is null  
     then dbo.FnDocumentoSunat_Obtener_RegistroVentas(cast(SerieDoc+''+NumeroDoc AS CHAR(10)))  
         
     else IDTipoDoc  
     end  
     
     ,IDTipoDocSetra
     ,orden_doc = case when IDTipoDocSetra='FAC' THEN 1 WHEN IDTipoDocSetra='BOL' THEN 2 WHEN IDTipoDocSetra='NCR' THEN 3 WHEN IDTipoDocSetra='NCA' THEN 4 WHEN IDTipoDocSetra='NDB' THEN 5 ELSE 6 END
 FROM              
  (              
  SELECT SubDiario, NroRegistro, Periodo, FecEmision, FecVencim,              
  IDTipoDoc, SerieDoc, NumeroDoc, TipoCliente, CoStarSoft, RazonSocial,              
  dbo.FnCambioMoneda(ValorFacturadoExport,IDMoneda,@IDMoneda,TipoCambio) as ValorFacturadoExport,              
  dbo.FnCambioMoneda(BaseImponible,IDMoneda,@IDMoneda,TipoCambio) as BaseImponible,              
  dbo.FnCambioMoneda(Exonerada,IDMoneda,@IDMoneda,TipoCambio) as Exonerada,              
  dbo.FnCambioMoneda(Inafecta,IDMoneda,@IDMoneda,TipoCambio) as Inafecta,              
  dbo.FnCambioMoneda(ISC,IDMoneda,@IDMoneda,TipoCambio) as ISC,              
  dbo.FnCambioMoneda(IGV,IDMoneda,@IDMoneda,TipoCambio) as IGV,              
  dbo.FnCambioMoneda(OtrosTributos,IDMoneda,@IDMoneda,TipoCambio) as OtrosTributos,              
                
  TipoCambio, CoCtroCosto, IDFile, FeEmisionVinc,   
  isnull(IDTipoDocVinc,'') as IDTipoDocVinc, isnull(SerieDocVinc,'') as SerieDocVinc,      
  isnull(NumeroDocVinc,'') as NumeroDocVinc,      
  IDMoneda,      
  isnull(IDTipoDocStarSoft,'') as IDTipoDocStarSoft,      
  CoTipoVenta,IDTipoDocSetra,CoEstado as CoEstadoDoc, isnull(CoCtaContable,'') as CoCtaContable    
  --,IDDebitMemo --,IDCAB    
  FROM               
   (               
   SELECT '03' as SubDiario,              
   dbo.FnFormatoCerosIzquierda(row_number() over (order by Cast(SerieDoc as SMALLINT),NumeroDoc),4) as NroRegistro,               
   @cMes as Periodo,                
   FeDocum as FecEmision, FeDocum as FecVencim,               IDTipoDoc ,SerieDoc,       
   --dbo.FnFormatoCerosIzquierda(NumeroDoc,6) as NumeroDoc,                
   NumeroDoc,     
   '02' as TipoCliente, CoStarSoft, RazonSocial,                
   --Case When IDTipoDoc='01' Then SsTotalDocumUSD Else 0 End as ValorFacturadoExport,                
   Case When IDTipoDoc='01' and SerieDoc='001' Then SsTotalDocumUSD Else 0 End as ValorFacturadoExport,                
                 
   Case When (SerieDoc = '003' OR SerieDoc = '004') And SsIGV>0 Then               
		--SsTotalDocumUSD-SsIGV               
		CASE WHEN IDTipoDocStarSoft='CC' THEN (SsTotalDocumUSD-SsIGV)*-1 ELSE (SsTotalDocumUSD-SsIGV) END
   Else              
    0              
   End As BaseImponible,                
              
   0 as Exonerada,                
   --Case When ((IDTipoDoc='03' Or IDTipoDoc='07') AND SerieDoc='005')  Then  --BOL Y NC 005         
   Case When ((IDTipoDoc='03' Or IDTipoDoc='07' Or IDTipoDoc='08') AND SerieDoc='005')  Then  --BOL Y NC 005              
		-- SsTotalDocumUSD              
			--case when IDTipoDoc in ('03','05') then SsTotalDocumUSD  *(-1) else SsTotalDocumUSD end
			case when IDTipoDoc in ('07') then SsTotalDocumUSD  *(-1) else SsTotalDocumUSD end
   Else                
    Case SerieDoc               
     When '001'  Then                
      --Case IDTipoDoc When '03' Then SsTotalDocumUSD               
      Case IDTipoDoc When '03' Then SsTotalDocumUSD    
			When '08' Then SsTotalDocumUSD     --se agregó condicion        
            When '07' Then SsTotalDocumUSD * (-1)              
      Else               
       0              
     End              
	 When '002'  Then                
      Case When SsIGV = 0 Then SsTotalDocumUSD End              
     When '003'  Then                
      --Case When SsIGV = 0 Then SsTotalDocumUSD End              
	  Case When SsIGV = 0 Then 
	     --Case IDTipoDoc When '07' Then (CASE WHEN IDTipoDocStarSoft='CC' THEN SsTotalDocumUSD * (-1) ELSE SsTotalDocumUSD END)  ELSE  SsTotalDocumUSD END
		 Case  When IDTipoDoc IN ('07','03') Then (CASE WHEN IDTipoDocStarSoft='CC' THEN SsTotalDocumUSD * (-1) ELSE SsTotalDocumUSD END)  ELSE  SsTotalDocumUSD END
	  End              

	When '004'  Then                
      --Case When SsIGV = 0 Then SsTotalDocumUSD End    
	    Case When SsIGV = 0 Then
		 --Case IDTipoDoc When '07' Then (CASE WHEN IDTipoDocStarSoft='CC' THEN SsTotalDocumUSD * (-1) ELSE SsTotalDocumUSD END)  ELSE  SsTotalDocumUSD END
		 Case  When IDTipoDoc IN ('07','03') Then (CASE WHEN IDTipoDocStarSoft='CC' THEN SsTotalDocumUSD * (-1) ELSE SsTotalDocumUSD END)  ELSE  SsTotalDocumUSD END
		 --ELSE 
		 --Case  When IDTipoDoc IN ('07') Then (CASE WHEN IDTipoDocStarSoft='CC' THEN SsTotalDocumUSD * (-1) ELSE SsTotalDocumUSD END)  ELSE  SsTotalDocumUSD END
		END
	When '005'  Then                
      --Case When SsIGV = 0 Then SsTotalDocumUSD End    
	    Case When SsIGV = 0 Then
		 --Case IDTipoDoc When '07' Then (CASE WHEN IDTipoDocStarSoft='CC' THEN SsTotalDocumUSD * (-1) ELSE SsTotalDocumUSD END)  ELSE  SsTotalDocumUSD END
		 Case  When IDTipoDoc IN ('07','03') Then (CASE WHEN RTRIM(LTRIM(IDTipoDocStarSoft))='CC' THEN SsTotalDocumUSD * (-1) ELSE SsTotalDocumUSD END)  ELSE  SsTotalDocumUSD END
		END
	When '006'  Then                
      Case When SsIGV = 0 Then SsTotalDocumUSD End      
    Else              
     0              
    End                 
   End as Inafecta,                
   0 as ISC,                
   --SsIGV as IGV,                
     CASE WHEN IDTipoDocStarSoft='CC' THEN (SsIGV)*-1 ELSE (SsIGV) END      as IGV,                   
   0 as OtrosTributos,                
   0 AS ImporteTotal,  IDMoneda,              
   --dbo.FnTipoDeCambioVtaUltimoxFecha(FeDocum) as TipoCambio,                
  -- dbo.FnTipoDeCambioVtaUltimoxFecha(FeDocum) as TipoCambio,             
   case when IDTipoDocStarSoft='CC' THEN
	    dbo.FnTipoDeCambioVtaUltimoxFecha(FeEmisionVinc) 
   ELSE dbo.FnTipoDeCambioVtaUltimoxFecha(FeDocum) 
   END
   as TipoCambio,
                  
   CoCtroCosto,                 
   IDFile,                
  FeEmisionVinc,IDTipoDocVinc,Left(NuDocumVinc,3) as SerieDocVinc,               
   Right(NuDocumVinc,7) as NumeroDocVinc ,IDTipoDocStarSoft,CoTipoVenta,              
   IDTipoDocSetra, CoEstado, CoCtaContable --,IDDebitMemo  --,IDCAB           
   FROM                
    (                
    Select --distinct                 
    --dm.IDCab                
    --idm.IDDebitMemo ,                
    --cc.IDFile,                 
    
       IDFile=ISNULL(cc.IDFile,
						(SELECT IDFILE FROM COTICAB WHERE IDCab=d.idcab)
				),          
              
    cc.Fecha as FechaIn, cc.FechaOut ,                
              
    (Select COUNT(*) From COTIDET Where IDCab=isnull(dm.IDCab,0) And DebitMemoPendiente=1) as DebitMemoPendiente,                
    cc.FeFacturac,                 
    Left(d.NuDocum,3) as SerieDoc, Right(d.NuDocum,7) as NumeroDoc,                
    d.FeDocum, td.CoSunat as IDTipoDoc, td.CoStarSoft as IDTipoDocStarSoft,              
    --cl.CoStarSoft, cl.RazonSocial,    
    IsNull(cl.CoStarSoft,    
     (Select clSq.CoStarSoft from MACLIENTES clSq    
    where clSq.IDCliente=(select Isnull(d2.IDCliente,c2.IDCliente) from DOCUMENTO d2 Left Join COTICAB c2 On d2.IDCab=c2.IDCAB    
           where d2.NuDocum=d.NuDocum and d2.IDTipoDoc=d.IDTipoDoc ))    
     ) as CoStarSoft,    


RazonSocial= case when (select IDPais from MAUBIGEO where IDubigeo=cl.IDCiudad)	='000323'
			--si es de peru entonces la condicion es la misma
			then
					Isnull(cl.RazonSocial,    
							 (Select clSq.RazonSocial from MACLIENTES clSq    
								where clSq.IDCliente=(select Isnull(d2.IDCliente,c2.IDCliente) from DOCUMENTO d2 Left Join COTICAB c2 On d2.IDCab=c2.IDCAB    
														where d2.NuDocum=d.NuDocum and d2.IDTipoDoc=d.IDTipoDoc ))     
						)
			else --si es extranjero entonces se muestra la razon comercial en caso no tenga razon social
					--Isnull(cl.RazonSocial,case when RTRIM(ltrim(cl.RazonSocial))='' then cl.RazonComercial Else cl.RazonSocial end)
					
					Isnull((case when rtrim(ltrim(cl.RazonSocial))='' then cl.RazonComercial else cl.RazonSocial end),  
							
					  
							 (Select RazonSocial=case when rtrim(ltrim(clSq.RazonSocial))='' then clSq.RazonComercial else clSq.RazonSocial end 
							    from MACLIENTES clSq    
								where clSq.IDCliente=(select Isnull(d2.IDCliente,c2.IDCliente) from DOCUMENTO d2 Left Join COTICAB c2 On d2.IDCab=c2.IDCAB    
														where d2.NuDocum=d.NuDocum and d2.IDTipoDoc=d.IDTipoDoc ))     
						)
					
			end ,

/*
 Isnull(cl.RazonSocial,    
     (Select clSq.RazonSocial from MACLIENTES clSq    
    where clSq.IDCliente=(select Isnull(d2.IDCliente,c2.IDCliente) from DOCUMENTO d2 Left Join COTICAB c2 On d2.IDCab=c2.IDCAB    
           where d2.NuDocum=d.NuDocum and d2.IDTipoDoc=d.IDTipoDoc ))     
     ) as RazonSocial,    
  */   
     
    D.SsIGV, d.CoCtroCosto, d.NuDocumVinc, d.FeEmisionVinc,tdV.CoSunat as IDTipoDocVinc,                
    d.SsTotalDocumUSD,d.IDMoneda,d.CoTipoVenta ,              
    --bn.Sigla as DescBanco,fp.Descripcion as DescFormaPago, --ifn.CoOperacionBan,                 
              
    --(Select top 1 FeAcreditada From INGRESO_FINANZAS Where NuIngreso=idm.NuIngreso Order by FeAcreditada Desc) as FecUltPago,                
    D.IDTipoDoc AS IDTipoDocSetra, d.CoEstado, d.CoCtaContable --,dm.IDDebitMemo    
    FROM DEBIT_MEMO dm                 
    Inner Join INGRESO_DEBIT_MEMO idm On idm.IDDebitMemo = dm.IDDebitMemo        
    and idm.SsSaldoNuevo=0    
    And RIGHT(idm.IDDebitMemo,2)='00'                
    Left Join COTICAB cc On isnull(dm.IDCab,0)=cc.IDCAB           
    Right Join DOCUMENTO d On cc.IDCAB=isnull(d.IDCab,0)    
    Left Join MATIPODOC td On d.IDTipoDoc=td.IDtipodoc                
    Left Join MATIPODOC tdV On d.IDTipoDocVinc=tdV.IDtipodoc        
    Left Join MACLIENTES cl On isnull(cc.IDCliente,d.IDCliente)=cl.IDCliente                
    --WHERE dm.IDEstado='PG'                 
    --WHERE (dm.IDEstado='PG' OR dm.IDEstado IS NULL)  and d.IDTipoDoc <> 'CBR'    
	WHERE (dm.IDEstado IN('PG','PP') OR dm.IDEstado IS NULL)  and d.IDTipoDoc <> 'CBR'    
    ) as X                
   WHERE                 
   --DebitMemoPendiente=0           
           
   --And                
   --(((CONVERT(varchar(6), FecUltPago,112)=@cMes OR                 
   --CONVERT(varchar(6), FecUltPago,112)=@cMesAnt))                
   --And CONVERT(varchar(6), isnull(FeFacturac,'01/01/1900'),112)=@cMes)                
   --Or                
   --(CONVERT(varchar(6), FecUltPago,112)=@cMes And                 
   --CONVERT(varchar(6), isnull(FeFacturac,'01/01/1900'),112)=@cMesAnt )                
             
   --And           
         
   --CONVERT(varchar(6), isnull(FeFacturac,'01/01/1900'),112)=@cMes          
   CONVERT(varchar(6), isnull(FeDocum,'01/01/1900'),112)=@cMes          
   ) AS Y              
  ) AS Z              
 ) AS W  
 Order by --SerieDoc,IDTipoDoc,Cast(NumeroDoc as int)          
 --Cast(SerieDoc as SMALLINT),OrdTipoDoc desc, Cast(IDTipoDoc as SMALLINT),Cast(NumeroDoc as int)    
 --OrdTipoDoc desc,  
 --Cast(IDTipoDoc2 as SMALLINT),Cast(SerieDoc as SMALLINT),Cast(NumeroDoc as int)    
  Cast(SerieDoc as SMALLINT),orden_doc,IDTipoDoc2,Cast(NumeroDoc as int)    
END
