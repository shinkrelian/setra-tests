﻿Create Procedure dbo.RESERVAS_DET_PAX_InsxAcomodos
	@IDCab	int,
	@UserMod	char(4)
As
	Set Nocount On
	
	Insert Into RESERVAS_DET_PAX
	(IDReserva_Det, IDPax, UserMod)
	SELECT 
	rd.IDReserva_Det,ap.IDPax,@UserMod
	FROM ACOMODOPAX_PROVEEDOR ap 
	Inner Join RESERVAS_DET rd On ap.IDReserva=rd.IDReserva And rd.CapacidadHab=1
	Inner Join COTIDET_PAX cp On rd.IDDet=cp.IDDet And ap.IDPax=cp.IDPax
	WHERE ap.IDReserva in (select IDReserva from RESERVAS where IDCab=@IDCab)
	AND ap.CapacidadHab=1
		
	Union		
	
	SELECT 
	rd.IDReserva_Det,ap.IDPax,@UserMod
	FROM ACOMODOPAX_PROVEEDOR ap 
	Inner Join RESERVAS_DET rd On ap.IDReserva=rd.IDReserva And rd.CapacidadHab=2
	Inner Join COTIDET_PAX cp On rd.IDDet=cp.IDDet And ap.IDPax=cp.IDPax
	WHERE ap.IDReserva in (select IDReserva from RESERVAS where IDCab=@IDCab)
	AND ap.CapacidadHab=2
	
	Union
	
	SELECT 
	rd.IDReserva_Det,ap.IDPax,@UserMod
	FROM ACOMODOPAX_PROVEEDOR ap 
	Inner Join RESERVAS_DET rd On ap.IDReserva=rd.IDReserva And rd.CapacidadHab=3
	Inner Join COTIDET_PAX cp On rd.IDDet=cp.IDDet And ap.IDPax=cp.IDPax
	WHERE ap.IDReserva in (select IDReserva from RESERVAS where IDCab=@IDCab)
	AND ap.CapacidadHab=3
	
