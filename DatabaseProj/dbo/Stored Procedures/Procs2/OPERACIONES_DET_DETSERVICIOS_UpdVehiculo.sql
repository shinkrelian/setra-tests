﻿
  
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_UpdVehiculo 
 @IDOperacion_Det int,  
 @IDServicio_Det int,  
 @IDVehiculo_Prg smallint,  
   
 --@Total numeric(8,2),  
 --@ObservEdicion varchar(250),  
   
 @UserMod char(4)  
As  
 Set NoCount On  
   
 Update OPERACIONES_DET_DETSERVICIOS Set  
 
 IDVehiculo_Prg=Case When ltrim(rtrim(@IDVehiculo_Prg))='' Then Null Else @IDVehiculo_Prg End,  

 UserMod=@UserMod,   
 FecMod=GETDATE()  
 From OPERACIONES_DET_DETSERVICIOS od   
 
 Where od.IDOperacion_Det=@IDOperacion_Det   
 And od.IDServicio_Det=@IDServicio_Det 
   
  