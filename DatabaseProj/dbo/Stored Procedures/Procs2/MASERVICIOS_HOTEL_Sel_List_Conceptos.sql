﻿
------------------------------------------------------------------------------------------



--MASERVICIOS_HOTEL_Sel_List_Conceptos 'LIM00057'
--JHD-20141127-Agregar el Nuevo campo de TxPisos  
CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_Sel_List_Conceptos]
	@CoServicio char(8)='',
	@CoConcepto tinyint=0,
	@CoTipo char(3)=''
AS
BEGIN


Select
		--CoServicio=case when @CoServicio='' then '0' else @CoServicio end,
		CoServicio=isnull((select CoServicio from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),'0') ,
		CoConcepto,
 		TxDescripcion,
 		CoTipo,
 		FlServicio= isnull((select FlServicio from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),CAST(0 as bit)) ,
 		FlCosto= isnull((select FlCosto from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),CAST(0 as bit)) ,
 		CoTipoMenu= isnull((select CoTipoMenu from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),'') ,
 		FlFijoPortable=  isnull((select FlFijoPortable from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),CAST(0 as bit)) ,
 		FlPortable=  isnull((select FlPortable from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),CAST(0 as bit)) ,
 		CoTipoBanio=isnull((select CoTipoBanio from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),'') ,
        CoEstadoHab=isnull((select CoEstadoHab from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),'') ,
        TxPisos=isnull((select TxPisos from dbo.MASERVICIOS_HOTEL where CoConcepto=dbo.MACONCEPTO_HOTEL.CoConcepto and CoServicio=@CoServicio),'') 
 		
 		
 	From dbo.MACONCEPTO_HOTEL
 	where 
 		(CoConcepto=@CoConcepto or @CoConcepto=0) and
 		(CoTipo=@CoTipo or @CoTipo='')
 	
END;
