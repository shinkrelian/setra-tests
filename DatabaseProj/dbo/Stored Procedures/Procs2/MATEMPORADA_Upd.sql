﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Upd]
	@IDTemporada int,
	@NombreTemporada varchar(80),
	@TipoTemporada char(1),
	@UserMod char(4)
AS
BEGIN
	Set NoCount On
	Update MATEMPORADA
	Set NombreTemporada = @NombreTemporada, TipoTemporada = @TipoTemporada,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where IDTemporada = @IDTemporada
END
