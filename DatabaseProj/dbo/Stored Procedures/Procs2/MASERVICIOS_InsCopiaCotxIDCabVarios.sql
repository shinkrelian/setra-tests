﻿--JHD-20151217- ,HoraPredeterminada
--JRF-20160409- ,FlNoPresupuestos
CREATE Procedure dbo.MASERVICIOS_InsCopiaCotxIDCabVarios    
@IDServicio char(8),    
@IDCabVariosNuevo int,    
@pIDServicioNuevo char(8) OutPut    
As    
Set NoCount On    
 Declare @IDUbigeo char(6)= (select IDubigeo from MASERVICIOS where IDServicio = @IDServicio)    
 Declare @DC char(3)=(Select DC From MAUBIGEO Where IDubigeo=@IDubigeo)       
 Declare @Correl smallint=Isnull((Select IsNull(Max(Right(IDServicio,5)),0) From MASERVICIOS Where IDubigeo=@IDubigeo),0)+1       
 Declare @IDServicioNue char(8)=@DC+ replicate('0',5-Len(Cast(@Correl as varchar(5)))) +Cast(@Correl as varchar(5))      
     
Insert into MASERVICIOS    
SELECT @IDServicioNue as [IDServicio]    
      ,[IDProveedor]    
      ,[IDTipoProv]    
      ,[IDTipoServ]    
      ,[Transfer]    
      ,[TipoTransporte]    
      ,[Activo]    
      ,[MotivoDesactivacion]    
      ,[Descripcion]    
      ,[Tarifario]    
      ,[Descri_tarifario]    
      ,[IDubigeo]    
      ,[Telefono]    
      ,[Celular]    
      ,[Comision]    
      ,[UpdComision]    
      ,[IDCat]    
      ,[Categoria]    
      ,[Capacidad]    
      ,[Observaciones]    
      ,[Lectura]    
      ,[FecCaducLectura]    
      ,[AtencionLunes]    
      ,[AtencionMartes]    
      ,[AtencionMiercoles]    
      ,[AtencionJueves]    
      ,[AtencionViernes]    
      ,[AtencionSabado]    
      ,[AtencionDomingo]    
      ,[HoraDesde]    
      ,[HoraHasta]    
      ,[DesayunoHoraDesde]    
      ,[DesayunoHoraHasta]    
      ,[PreDesayunoHoraDesde]    
      ,[PreDesayunoHoraHasta]    
      ,[IDPreDesayuno]    
      ,[ObservacionesDesayuno]    
      ,[HoraCheckIn]    
      ,[HoraCheckOut]    
      ,[cActivo]    
      ,[Dias]    
      ,[IDServicioCopia]    
      ,[PoliticaLiberado]    
      ,[Liberado]    
      ,[TipoLib]    
      ,[MaximoLiberado]    
      ,[MontoL]    
      ,[Mig]    
      ,[SinDescripcion]    
      ,[UserMod]    
      ,[FecMod]    
      ,[ServicioVarios]  
      ,@IDCabVariosNuevo as [IDCabVarios]   
      ,CoPago  
      ,IDDesayuno 
	  ,FlServicioSoloTC
	  ,APTServicoOpcional
	  ,HoraPredeterminada
	  ,FlNoPresupuestos
  FROM [dbo].[MASERVICIOS]    
  where IDServicio = @IDServicio    
  Set @pIDServicioNuevo = @IDServicioNue   

