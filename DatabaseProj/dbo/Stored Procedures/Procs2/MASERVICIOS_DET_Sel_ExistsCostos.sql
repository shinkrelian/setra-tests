﻿Create Procedure dbo.MASERVICIOS_DET_Sel_ExistsCostos
@IDServicio_Det int,
@pExistenRango bit output
As
Set NoCount On
Set @pExistenRango = 0
Select @pExistenRango = 1
from MASERVICIOS_DET_COSTOS where IDServicio_Det=@IDServicio_Det
