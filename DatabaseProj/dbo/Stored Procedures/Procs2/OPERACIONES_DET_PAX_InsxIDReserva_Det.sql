﻿
Create Procedure dbo.OPERACIONES_DET_PAX_InsxIDReserva_Det 
	@IDReserva_Det int,     
	@UserMod char(4)  
As  
	Set Nocount On  

	Insert Into OPERACIONES_DET_PAX(IDOperacion_Det,IDPax,UserMod)   
	Select  
	(Select IDOperacion_Det From  OPERACIONES_DET Where IDReserva_Det=d.IDReserva_Det),  
	IDPax,@UserMod From RESERVAS_DET_PAX d Where IDReserva_Det In   
	(@IDReserva_Det)  
	
