﻿
--------================================================================================
--------================================================================================


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
CREATE procedure NOTAVENTA_COUNTER_DETALLE_Ins
		   @NuNtaVta char(6),
		   @NuItem tinyint,
           @NuDocumento char(20),
           
           --@CoMoneda char(3)='',
           --@SSTipoCambio numeric(8,2)=0,
           @CoSegmento char(1)='',
           @SSCosto numeric(10,2),
           @SSComision numeric(10,2),
           @SSValor numeric(10,2),
           @SSFEE_Emision numeric(10,2),
           @SSVenta numeric(10,2),
           @SSUtilidad numeric(10,2),
           @UserMod char(4)
as
BEGIN
Set NoCount On
	Declare @NuDetalle tinyint =(select Isnull(Max(NuDetalle),0)+1 
								from NOTAVENTA_COUNTER_DETALLE where NuNtaVta =@NuNtaVta and NuItem=@NuItem)

INSERT INTO NOTAVENTA_COUNTER_DETALLE
           (NuNtaVta
           ,NuItem
           ,NuDetalle
           ,[NuDocumento]
           
           --,[CoMoneda]
           --,[SSTipoCambio]
           ,CoSegmento
           ,[SSCosto]
           ,[SSComision]
           ,[SSValor]
           ,[SSFEE_Emision]
           ,[SSVenta]
           ,[SSUtilidad]
           ,[UserMod])
     VALUES
           (@NuNtaVta
           ,@NuItem
           ,@NuDetalle
           ,@NuDocumento
           
     --      ,@CoMoneda
		   --,@SSTipoCambio 
           ,@CoSegmento
           ,@SSCosto
           ,@SSComision
           ,@SSValor
           ,@SSFEE_Emision
           ,@SSVenta
           ,@SSUtilidad
           ,@UserMod)
END

