﻿CREATE Procedure [dbo].[RESERVAS_DET_SelxIDCab_RptTEMP]
 @IDCab int,                                          
 @IDTipoProv char(3)                                          
AS

 Set NoCount On                                          

                                           
 Select  Distinct                                       
 r.IDReserva,  r.IDProveedor,
 case when (p.IDTipoProv='003' and sd.ConAlojamiento= 1) then
	isnull(case when isnull(uHtl.Codigo,'') = '' then Case When isnull(uHtl.DC,'') = '' Then SUBSTRING(uHtl.Descripcion,1,3) Else uHtl.DC End                                        
											 else uHtl.Codigo End,
	(SELECT (case when U1.Codigo is null or U.Codigo = '' then case when  isnull(u1.DC,'') = '' then SUBSTRING(U1.Descripcion,1,3) Else U1.DC End
		else U1.Codigo end) AS Ubigeo
	 FROM MASERVICIOS AS S1 INNER JOIN MAUBIGEO AS U1 ON S1.IDubigeo = U1.IDubigeo
	 WHERE S1.IDServicio = rd.IDServicio))
 else              
 	isnull(case when isnull(uHtl.Codigo,'') = '' then Case When isnull(uHtl.DC,'') = '' Then SUBSTRING(uHtl.Descripcion,1,3) Else uHtl.DC End                                        
											 else uHtl.Codigo End,
	case when isnull(u.Codigo,'') = '' then Case When isnull(u.DC,'') = '' Then SUBSTRING(u.Descripcion,1,3) Else u.DC End                                        
											 else u.Codigo End)
 end As DescCiudad                                        
 -- u.Descripcion as DescCiudad                                        
-- , p.NombreCorto as DescProveedor,
 --,case when (p.IDTipoProv='003' and sd.ConAlojamiento= 1) then isnull((select top 1 nohotel from MASERVICIOS_DIA where 
	--IDServicio=sd.IDServicio and NoHotel is not null order by FechaMod desc),isnull(p.NombreCorto,'')) else  p.NombreCorto end as DescProveedor,
 ,Isnull(cdd.NoHotel,p.NombreCorto) as DescProveedor,
 isnull(cdd.TxDireccHotel,p.Direccion) + ISNULL('*Tel:' + isnull(cdd.TxTelfHotel1,p.TelefonoRecepcion1),'') + ISNULL(' - '+isnull(cdd.TxTelfHotel2,p.TelefonoRecepcion2),'') as DireccionTelefono ,  
 'Check In: '+Isnull(convert(char(5),isnull(cdd.FeHoraChkInHotel,s.HoraCheckIn),108),'')+' hrs'+'*'+            
 'Check Out: '+Isnull(convert(char(5),isnull(cdd.FeHoraChkOutHotel,s.HoraCheckOut),108),'')+' hrs' + '*'+             
  case when isnull(isnull(cdd.CoTipoDesaHotel,sd.TipoDesayuno),'00') = '00' then '' else 'Breakfast: ' + Isnull(ds.Descripcion,'') +'*' End         
 as CheckINCheckOUT,                  
 IsNull(cdd.TxWebHotel,IsNull(p.Web,'')) as SitioWeb,
  --rd.Dia As FechaIn, 
  case when cd.IDDetRel is null then rd.Dia else cd.dia end as FechaIn,
  --rd.FechaOut, 
  case when cd.IDDetRel is null then 
	case when cdd.NoHotel is null then rd.FechaOut else
		DATEADD(DAY, 
		(Select count(*) From COTIDET_DESCRIPSERV Where IDDET=cdd.IDDet And Left(ltrim(IDIdioma),9)='HOTEL/WEB' and NoHotel=cdd.NoHotel)
		,rd.Dia) 
	end
  else 
	case when cdd.NoHotel is null then rd.FechaOut else
		DATEADD(DAY, 
		(Select count(*) From COTIDET_DESCRIPSERV Where IDDET=cdd.IDDet And Left(ltrim(IDIdioma),9)='HOTEL/WEB' and NoHotel=cdd.NoHotel)
		,cd.Dia) 
	end
  end as FechaOut,
  
 p.IDTipoProv, r.Estado, r.Estado+' - '+Case r.Estado             
  When 'NV' Then 'NUEVO'                                          
  When 'RQ' Then 'REQUERIDO'                         
  When 'OK' Then 'ACEPTADO'                                          
  When 'WL' Then 'WAITING LIST'                                          
  --When 'XL' Then 'CANCELADO'                                          
  When 'RR' Then 'RECONFIRMADO'                                          
 End As DescEstado,                                        
   ISNULL(r.CodReservaProv  ,'') As CodigoReservaProv,                                        
   Isnull(C.Simple,0)+ IsNull(C.SimpleResidente,0) as Simple,                                        
   Isnull(C.Twin,0)+Isnull(C.TwinResidente,0)as Twin,                                        
   Isnull(C.Matrimonial,0) + Isnull(C.MatrimonialResidente,0) as Matrimonial,                                        
   Isnull(C.Triple,0)+Isnull(C.TripleResidente,0) as Triple              
   ,0 as Cantidad                                        
   --,rd.CapacidadHab,rd.EsMatrimonial                                  
   ,Case When dbo.FnFechaLimtProvedor(p.IDProveedor,C.NroPax,1,Cast(Year(rd.Dia) as CHAR(4)),r.IDCab) = 0 then ''   
  Else   
  CONVERT(char(10),  
    DATEADD(dd,dbo.FnFechaLimtProvedor(p.IDProveedor,C.NroPax,1,Cast(Year(rd.Dia) as CHAR(4)),r.IDCab),rd.Dia),  
    103) End   
 As TimeLimitRooming                                
   ,Case When dbo.FnFechaLimtProvedor(p.IDProveedor,C.NroPax,0,Cast(Year(rd.Dia) as CHAR(4)),r.IDCab) = 0 
    Or p.IDFormaPago In ('001','015','016','017','018','019') then ''   
  Else   
  CONVERT(char(10),  
    DATEADD(dd,dbo.FnFechaLimtProvedor(p.IDProveedor,C.NroPax,0,Cast(Year(rd.Dia) as CHAR(4)),r.IDCab),rd.Dia),  
    103)   
  End As TimeLimitPrepago                                  
   ,u.Descripcion
   ,rd.Dia, sd.PlanAlimenticio,case when sd.PlanAlimenticio = 1 then rd.Servicio else '' End as ServicioAli,                      
   isnull((select IncGuia from COTIDET cd where cd.IDDet = rd.IDDet),0) As Guia ,                      
   Case When (select IncGuia from COTIDET cd where cd.IDDet = rd.IDDet)=1 then rd.Servicio else '' End as ServicioGui              ,        
   case when (select COUNT(iddet) from cotidet cd        
    where AcomodoEspecial = 1 and cd.IncGuia = 0 and IDCab = r.IDCab and r.IDProveedor = cd.IDProveedor        
    and cd.Dia between rd.Dia and rd.FechaOut)>0 then 1 else 0 End as ExisteAcomodoEspecial        ,s.IDServicio,sdia.IDIdioma--,cd.DiaServ
 INTO #HotelesRepEstadoReservas                                   
 From RESERVAS r Left Join RESERVAS_DET rd On r.IDReserva=rd.IDReserva                                          
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor                                     
 Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo                                    
 Left Join COTICAB C On r.IDCab = C.IDCAB         
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det      
  Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
 Left Join 
  (Select ROW_NUMBER() OVER (PARTITION BY cd1.IDServicio ORDER BY cd1.IDServicio) AS DiaServ,
	cd1.* From COTIDET cd1 
	Inner Join MASERVICIOS_DET sd1 On cd1.IDServicio_Det=sd1.IDServicio_Det And sd1.ConAlojamiento = 1
	Inner Join MASERVICIOS s1 On sd1.IDServicio=s1.IDServicio    
	Left Join MADESAYUNOS ds1 On sd1.TipoDesayuno = ds1.IDDesayuno       
	Where  cd1.IDCAB=@idcab and cd1.IncGuia = 0	) cd  On rd.IDDet=cd.IDDET or (cd.IDDetRel=rd.IDDet 
				and exists(select iddet from COTIDET_DESCRIPSERV where cd.IDDET=IDDet And Left(ltrim(IDIdioma),9)='HOTEL/WEB'))
 Left Join COTIDET_DESCRIPSERV cdd On cd.IDDET=cdd.IDDet And Left(ltrim(cdd.IDIdioma),9)='HOTEL/WEB' 
 And Right(rtrim(cdd.IDIdioma),1)=cd.DiaServ        
 Left Join MAUBIGEO uHtl On isnull(cdd.CoCiudadHotel,p.IDCiudad)=uHtl.IDubigeo         
 Left Join MASERVICIOS_DIA sdia On sdia.IDServicio=cd.IDServicio  And sdia.dia=cd.DiaServ And sdia.IDIdioma='HOTEL/WEB'     
 Left Join MADESAYUNOS ds On isnull(cdd.CoTipoDesaHotel,sd.TipoDesayuno) = ds.IDDesayuno       
 Where r.IDCab=@IDCab And (p.IDTipoProv=@IDTipoProv Or (p.IDTipoProv='003' and sd.ConAlojamiento= 1 --And isnull(sdia.Dia,0) <>0
	))     
 And IsNull(rd.FlServicioNoShow,0)=0    
 And Exists(select IDReserva_Det from RESERVAS_DET where IDReserva = r.IDReserva)                                       
 and sd.PlanAlimenticio = 0  
 and rd.FechaOut is not null                   
 and r.Estado <> 'XL'                        
 and r.Anulado=0 and rd.Anulado=0      

 Order by FechaIn,PlanAlimenticio desc,Guia desc     

 Select IDReserva, IDProveedor, DescCiudad, DescProveedor, DireccionTelefono, CheckINCheckOUT, 
	SitioWeb, min(FechaIn) as FechaIn, max(FechaOut) as FechaOut, IDTipoProv, Estado, DescEstado, CodigoReservaProv, Simple, Twin, 
	matrimonial, Triple, Cantidad, TimeLimitRooming, TimeLimitPrepago, Descripcion, Dia, PlanAlimenticio, 
	ServicioAli, Guia, ServicioGui, ExisteAcomodoEspecial, IDServicio ,IDIdioma
 From
 (
 select * from #HotelesRepEstadoReservas where (IDServicio in
	(select IDServicio from #HotelesRepEstadoReservas where IDIdioma='HOTEL/WEB') and not IDIdioma is null)
	or (not IDServicio in
	(select IDServicio from #HotelesRepEstadoReservas where IDIdioma='HOTEL/WEB') )
	) as X
 Group by IDReserva, IDProveedor, DescCiudad, DescProveedor, DireccionTelefono, CheckINCheckOUT, 
	SitioWeb, IDTipoProv, Estado, DescEstado, CodigoReservaProv, Simple, Twin, 
	matrimonial, Triple, Cantidad, TimeLimitRooming, TimeLimitPrepago, Descripcion, Dia, PlanAlimenticio, 
	ServicioAli, Guia, ServicioGui, ExisteAcomodoEspecial, IDServicio,IDIdioma
	Order by FechaIn,PlanAlimenticio desc,Guia desc    