﻿--JRF-20140414-Agregar la columna de Alojamiento      
--JRF-20141216-Agregar el filtro de idioma  
--JRF-20150223-Considerar [and od.FlServicioNoShow = 0]
--JRF-20150428-Mostrar la lista de Pax
--JRF-20160404-Agregar cod. de reservas
CREATE Procedure dbo.OPERACIONES_DET_SelHotelxRangoDias          
@IDCab int,        
@DiaServicio smalldatetime,  
@IDIdioma varchar(12)  
As          
 Set NoCount On          
 SELECT CAST(0 as numeric(8,3)) as ItemCotiDetalle,--,@DiaServicio,od.Dia,od.FechaOut,        
 convert(char(10),@DiaServicio,103) as CadenaDia,        
 UPPER(od.DiaNombre) as NombreDia,        
 cast(DATEPART(d,@DiaServicio) as varchar(2))+' '+ upper(DATENAME(m,@DiaServicio)) + ' ' +cast(DATEPART(YEAR,@DiaServicio) as varchar(4)) as MesAnio,        
 upper(DATENAME(m,@DiaServicio)) as DiaNombreLengOrig,CAST(convert(char(10),@DiaServicio,103) as smalldatetime) as DiaServicio,        
 '' as Hora,u.Descripcion as Ciudad,        
 pr.NombreCorto + IsNull(' ('+ r.CodReservaProv+') ' ,'') +' - '+ CAST(od.Cantidad as varchar(3)) + ' ' + od.Servicio +  
 Case When sd.PlanAlimenticio=0 then char(13)+dbo.FnDevuelveCadenaHotelServiciosAdicionales(cd.IDDET,@IDIdioma)Else '' End as Servicio,        
    Case When s.IDTipoServ = 'NAP' Then '-' Else s.IDTipoServ End             
   + Case When idio.Siglas ='---' Or idio.Siglas ='O.T' then '' else ' ('+idio.Siglas+')' End  as TipoServicio,        
   cast(od.NroPax as varchar(2)) +' Pax' as NroPax,od.IDVoucher as Voucher,'001' as IDTipoProv,sd.ConAlojamiento,cd.IDDET
   ,sd.PlanAlimenticio ,
   Case When c.NroPax+IsNull(c.NroLiberados,0) <>  od.NroPax+IsNull(od.NroLiberados,0) Then
   dbo.FnDevuelveListaPaxxIDDOperacionDet(od.IDOperacion_Det) Else '' End as NombresPax
           
   FROM OPERACIONES_DET od Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det        
   Left Join MASERVICIOS s On od.IDServicio = s.IDServicio Left Join MAUBIGEO u On od.IDubigeo = u.IDubigeo        
   Left Join MAIDIOMAS idio ON od.IDIdioma = idio.IDidioma Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion        
   Left Join MAPROVEEDORES pr On o.IDProveedor = pr.IDProveedor Left join COTICAB c ON o.IDCab = c.IDCAB        
   Inner Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det      
   Inner Join COTIDET cd On rd.IDDet = cd.IDDET      
   Inner Join RESERVAS r On rd.IDReserva=r.IDReserva
   where od.IDOperacion In (select IDOperacion from OPERACIONES o Left Join         
       MAPROVEEDORES pr On o.IDProveedor=pr.IDProveedor         
       where o.IDCab = @IDCab and pr.IDTipoProv = '001')        
   and od.FlServicioNoShow = 0
   and IDVoucher <> 0 and sd.PlanAlimenticio = 0 and CONVERT(char(10),c.FechaOut ,103) <> CONVERT(char(10),@DiaServicio ,103)        
   and @DiaServicio between od.Dia and dateadd(d,-1,od.FechaOut)
   order by od.Dia 
