﻿
--HLF-20130118-Cambiando tabla para el campo IDTipoOC
CREATE Procedure [Dbo].[OPERACIONES_DET_List_ProveeExternos]     	
	@IdCab Int    
As    
	Set NoCount On    

	Select p.NombreCorto as DescProveedor, tp.Descripcion as DescTipoProv,
	Case When Cast(od.IDVoucher as varchar(10))='0' Then '' Else Cast(od.IDVoucher as varchar(10)) End as IDVoucher,
	od.Servicio, od.NroPax,ISNULL(od.Noches,0) as Noches,od.NetoGen,od.TotalGen, 
	od.idTipoOC, toc.Descripcion as DescTipoOC
	From 
	OPERACIONES_DET od
	Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
	Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
	Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor And p.IDTipoOper='E'
	Left Join MATIPOPROVEEDOR tp On p.IDTipoProv=tp.IDTipoProv
	Left Join MATIPOOPERACION toc On toc.IdToc=od.idTipoOC
	Where od.IDOperacion In (Select IDOperacion From OPERACIONES Where IDCab=@IdCab)
	Order by p.NombreCorto
	
