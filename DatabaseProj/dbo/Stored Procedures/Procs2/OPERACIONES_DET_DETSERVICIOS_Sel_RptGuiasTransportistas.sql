﻿--JRF-20131211-Se debe filtra cuando sea trasladista solo los servicios de los trasladistas.      
--JRF-20140203-Se debe considera el reporte siempre en dolares.      
--JRF-20140203-No considerar las reservas_det.Anulado      
--JRF-20140414-Agregar el filtro de Vehículo    
--JRF-20141016-Ajuste para la duplicidad de datos (Mostrando información erronea)
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-and (LTRIM(rtrim(@CoPais))='' Or ub.IDPais = @CoPais) 
--JHD-20151028-Se agrego un nuevo Select para listar las operaciones sin reservas
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Sel_RptGuiasTransportistas                
--declare
@IDTipoProv char(3)='005',                
@IDProveedor char(6)='',                
@IDCiudad char(6)='',                
@IDIoma varchar(12)='',                
@Estado char(1)='',                
@FecRango1 smalldatetime='10/10/2015',                
@FecRango2 smalldatetime='10/10/2015',  
@Nuvehiculo tinyint=0,
@CoPais char(6)=''                
As                
Begin                
 Set NoCount On            
    
 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)                        
 Declare @TCambioRefe numeric(6,3)=(select ValCompra from MATIPOCAMBIO where convert(char(10),Fecha,103)='05/06/2015')--convert(char(10),getdate(),103))
 Declare @IDProveedorGuia char(6) = (select IDProveedor from MAPROVEEDORES pr2 Where pr2.IDUsuarioTrasladista = @IDProveedor)              
     
 Select X.IDProveedor_Prg,X.IDServicio_Det,X.IDServicio_Det_V_Cot,X.IDDet,X.ProveedorProgramado,X.Fecha,X.Cotizacion,X.IDFile,X.DescCliente,        
 X.Titulo,X.NroPax,X.NroLiberados,X.ServPrg,X.Estado,X.CodigoINC,  X.CostoRealImpto,      
 Case when X.AfectoIGV = 1 and X.CostoRealImpto = 0 Then (X.Total* 0.18) + X.Total Else X.Total End     
  as Total    
 , x.AfectoIGV,      
 -- X.Total  as Total, x.AfectoIGV,      
 --Case When x.IDMoneda = 'SOL' and X.TipoCambio <> 0 Then CAST((X.Total_Ejecutiva / X.TipoCambio) as numeric(8,2)) Else X.Total_Ejecutiva End     
 Case when X.TotalProgramado is null then    
 Cast(Case When Afecto=1 Then             
    NetoGen+(NetoGen*(@NuIgv/100))             
    Else             
    NetoGen             
    End as numeric(8,2))    
 Else    
 X.TotalProgramado End as Total_Ejecutiva 
 --,X.IDMoneda,X.TipoCambio        
 from (            
         
 select rd.IDReserva_Det, odd.IDProveedor_Prg,odd.IDServicio_Det,odd.IDServicio_Det_V_Cot,rd.IDDet,Isnull(UPPER(Us.Nombre),upper(p.NombreCorto)) as ProveedorProgramado,                
  Convert(Char(10),od.Dia,103) as Fecha,c.Cotizacion,c.IDFile,cl.RazonComercial as DescCliente, c.Titulo,                
  od.NroPax,Isnull(od.NroLiberados,0) as NroLiberados,--,od.Servicio,sCot.Descripcion as ServCot                
  sPrg.Descripcion as ServPrg,c.Estado,Isnull(ub.CodigoINC,'') as CodigoINC,  cds.CostoRealImpto,      
  IsNull((cds.CostoReal+cds.CostoLiberado+cds.CostoRealImpto+cds.CostoLiberadoImpto) * (od.NroPax + ISNULL(od.NroLiberados,0)),0)     
  as Total        
  ,Isnull(odd.TotalProgram,odd.Total_Prg) as Total_Ejecutiva  ,Isnull(sCot.Afecto,0) as AfectoIGV ,odd.IDMoneda,Isnull(odd.TipoCambio,0) as TipoCambio,    
       
  odd.TotalProgram as TotalProgramado,isnull(sCot.Afecto,d.Afecto) as Afecto,    
  Case When isnull(NetoCotizado,0) = 0 then                        
    Case When sCot.IDServicio_Det IS NULL Then       
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(odd.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),                          
    odd.IDMoneda,'USD',Case When odd.IDMoneda<>'SOL' Then IsNull(IsNull(odd.TipoCambio,sCot.TC),1) Else @TCambioRefe End) --@TipoCambio)                 
  Else      
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(sCot.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),                          
    odd.IDMoneda,'USD',Case When odd.IDMoneda<>'SOL' Then IsNull(IsNull(odd.TipoCambio,sCot.TC),1) Else @TCambioRefe End) --@TipoCambio)                 
  End      
   Else             
  NetoCotizado             
  End As NetoGen
 from OPERACIONES_DET od Inner Join OPERACIONES_DET_DETSERVICIOS odd On odd.IDOperacion_Det = od.IDOperacion_Det and odd.IDServicio=od.IDServicio
 Inner Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det
 Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion Left Join COTICAB c On o.IDCab = c.IDCAB                
 Left Join MAUSUARIOS Us On odd.IDProveedor_Prg = Us.IDUsuario             
 Left Join MAPROVEEDORES p On odd.IDProveedor_Prg = p.IDProveedor Left Join MATIPOPROVEEDOR Tp On p.IDTipoProv = Tp.IDTipoProv            
 Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente                
 Left Join MASERVICIOS_DET sCot On odd.IDServicio_Det_V_Cot = sCot.IDServicio_Det                
 Left Join MASERVICIOS_DET sPrg On odd.IDServicio_Det_V_Prg = sPrg.IDServicio_Det                
 Left Join MAUBIGEO ub On od.IDubigeo = ub.IDubigeo                
 Left Join COTIDET_DETSERVICIOS cds On rd.IDDet = cds.IDDET and odd.IDServicio_Det = cds.IDServicio_Det      
 Left Join MASERVICIOS_DET d  On odd.IDServicio_Det = d.IDServicio_Det    
 where (IDProveedor_Prg is not null) and odd.Activo = 1  and rd.Anulado = 0      
 and (odd.IDEstadoSolicitud <> 'NV')  and (@IDTipoProv = '017')-- Or Tp.IDTipoProv='005')            
 and (ltrim(rtrim(@IDProveedor))='' or odd.IDProveedor_Prg = @IDProveedorGuia Or ltrim(rtrim(odd.IDProveedor_Prg)) = ltrim(rtrim(@IDProveedor)))            
 and (LTRIM(rtrim(@IDCiudad))='' Or od.IDubigeo = @IDCiudad)                
 and (LTRIM(rtrim(@CoPais))='' Or ub.IDPais = @CoPais)                
 and (LTRIM(rtrim(@IDIoma))='' Or od.IDIdioma = @IDIoma)                
 and (LTRIM(rtrim(@Estado))='' Or c.Estado = @Estado)    
 and (@Nuvehiculo = 0 Or Isnull(odd.IDVehiculo_Prg,0) = @Nuvehiculo)              
 and Isnull(Us.IDUsuario,(select IDUsuarioTrasladista from MAPROVEEDORES p Where odd.IDProveedor_Prg = p.IDProveedor)) is not null          
 and (((LTRIM(rtrim(Convert(Char(10),@FecRango1,103)))='01/01/1900') and (LTRIM(rtrim(Convert(Char(10),@FecRango2,103)))='01/01/1900'))                
  Or cast(Convert(char(10),od.Dia,103) as smalldatetime) between @FecRango1 and @FecRango2)
 Union           
    
 select rd.IDReserva_Det, odd.IDProveedor_Prg,odd.IDServicio_Det,odd.IDServicio_Det_V_Cot,rd.IDDet,UPPER(p.NombreCorto) as ProveedorProgramado,                
  Convert(Char(10),od.Dia,103) as Fecha,c.Cotizacion,c.IDFile,cl.RazonComercial as DescCliente, c.Titulo,                
  od.NroPax,Isnull(od.NroLiberados,0) as NroLiberados,--,od.Servicio,sCot.Descripcion as ServCot                
  sPrg.Descripcion as ServPrg,c.Estado,Isnull(ub.CodigoINC,'') as CodigoINC,  cds.CostoRealImpto,      
  Isnull((cds.CostoReal+cds.CostoLiberado+cds.CostoRealImpto+cds.CostoLiberadoImpto)*(od.NroPax + ISNULL(od.NroLiberados,0)),0) as Total        
  ,Isnull(odd.TotalProgram,odd.Total_Prg) as Total_Ejecutiva ,Isnull(sCot.Afecto,0) as AfectoIGV,odd.IDMoneda,IsNull(odd.TipoCambio,0) as TipoCambio  ,    
      
  odd.TotalProgram as TotalProgramado,isnull(sCot.Afecto,d.Afecto) as Afecto,    
  Case When isnull(NetoCotizado,0) = 0 then                        
    Case When sCot.IDServicio_Det IS NULL Then       
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(odd.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),                          
    odd.IDMoneda,'USD',Case When odd.IDMoneda<>'SOL' Then IsNull(IsNull(odd.TipoCambio,sCot.TC),1) Else @TCambioRefe End) --@TipoCambio)                 
  Else      
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(sCot.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),                          
    odd.IDMoneda,'USD',Case When odd.IDMoneda<>'SOL' Then IsNull(IsNull(odd.TipoCambio,sCot.TC),1) Else @TCambioRefe End) --@TipoCambio)                 
  End      
   Else             
  NetoCotizado             
 End As NetoGen  
 from OPERACIONES_DET od Inner Join OPERACIONES_DET_DETSERVICIOS odd On odd.IDOperacion_Det = od.IDOperacion_Det  and odd.IDServicio=od.IDServicio
 Inner Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det
 Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion Left Join COTICAB c On o.IDCab = c.IDCAB                
 Inner Join MAPROVEEDORES p On Isnull(odd.IDProveedor_Prg,'') = p.IDProveedor 
 Left Join MATIPOPROVEEDOR TpPrg On p.IDTipoProv = TpPrg.IDTipoProv                
 Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente                
 Left Join MASERVICIOS_DET sCot On odd.IDServicio_Det_V_Cot = sCot.IDServicio_Det
 Left Join MASERVICIOS_DET sPrg On odd.IDServicio_Det_V_Prg = sPrg.IDServicio_Det                
 Left Join MAUBIGEO ub On od.IDubigeo = ub.IDubigeo                
 Left Join COTIDET_DETSERVICIOS cds On rd.IDDet = cds.IDDET and odd.IDServicio_Det = cds.IDServicio_Det --and cds.IDDET in (select IDDET from COTIDET where IDCab=c.IDCAB)    
 Left Join MASERVICIOS_DET d  On odd.IDServicio_Det = d.IDServicio_Det       
 where (IDProveedor_Prg is not null) and odd.Activo = 1 and rd.Anulado = 0            
 and (odd.IDEstadoSolicitud <> 'NV')             
 and (LTRIM(rtrim(@IDTipoProv))='' Or TpPrg.IDTipoProv = @IDTipoProv)                
 and (ltrim(rtrim(@IDProveedor))='' Or odd.IDProveedor_Prg = @IDProveedor)                
 and (LTRIM(rtrim(@IDCiudad))='' Or od.IDubigeo = @IDCiudad)         
 and (LTRIM(rtrim(@CoPais))='' Or ub.IDPais = @CoPais)          
 and (LTRIM(rtrim(@IDIoma))='' Or od.IDIdioma = @IDIoma)                
 and (LTRIM(rtrim(@Estado))='' Or c.Estado = @Estado)                
 and (@Nuvehiculo = 0 Or Isnull(odd.IDVehiculo_Prg,0) = @Nuvehiculo)              
 and (((LTRIM(rtrim(Convert(Char(10),@FecRango1,103)))='01/01/1900') and (LTRIM(rtrim(Convert(Char(10),@FecRango2,103)))='01/01/1900'))                
  Or cast(Convert(char(10),od.Dia,103) as smalldatetime) between @FecRango1 and @FecRango2)
  
  UNION
  --OPERADORES SIN RESERVAS
  select od.IDOperacion_Det as IDReserva_Det, -- rd.IDReserva_Det,
  odd.IDProveedor_Prg,
  odd.IDServicio_Det,
  odd.IDServicio_Det_V_Cot,
  --rd.IDDet,
  cds.IDDET,
  Isnull(UPPER(Us.Nombre),
  upper(p.NombreCorto)) as ProveedorProgramado,                
  Convert(Char(10),od.Dia,103) as Fecha,
  c.Cotizacion,
  c.IDFile,
  cl.RazonComercial as DescCliente, 
  c.Titulo,                
  od.NroPax,
  Isnull(od.NroLiberados,0) as NroLiberados,--,od.Servicio,sCot.Descripcion as ServCot                
  sPrg.Descripcion as ServPrg,
  c.Estado,
  Isnull(ub.CodigoINC,'') as CodigoINC,
  cds.CostoRealImpto,      
  IsNull((cds.CostoReal+cds.CostoLiberado+cds.CostoRealImpto+cds.CostoLiberadoImpto) * (od.NroPax + ISNULL(od.NroLiberados,0)),0)     
  as Total        
  ,Isnull(odd.TotalProgram,odd.Total_Prg) as Total_Ejecutiva  ,
  Isnull(sCot.Afecto,0) as AfectoIGV ,
  odd.IDMoneda,
  Isnull(odd.TipoCambio,0) as TipoCambio,    
       
  odd.TotalProgram as TotalProgramado,
  isnull(sCot.Afecto,d.Afecto) as Afecto,    
  Case When isnull(NetoCotizado,0) = 0 then                        
    Case When sCot.IDServicio_Det IS NULL Then       
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(odd.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),                          
    odd.IDMoneda,'USD',Case When odd.IDMoneda<>'SOL' Then IsNull(IsNull(odd.TipoCambio,sCot.TC),1) Else @TCambioRefe End) --@TipoCambio)                 
  Else      
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(sCot.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0)),                          
    odd.IDMoneda,'USD',Case When odd.IDMoneda<>'SOL' Then IsNull(IsNull(odd.TipoCambio,sCot.TC),1) Else @TCambioRefe End) --@TipoCambio)                 
  End      
   Else             
  NetoCotizado             
  End As NetoGen
 from OPERACIONES_DET od Inner Join
 OPERACIONES_DET_DETSERVICIOS odd On odd.IDOperacion_Det = od.IDOperacion_Det and odd.IDServicio=od.IDServicio
 
 --Inner Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det
 Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion Left Join COTICAB c On o.IDCab = c.IDCAB                
 Left Join MAUSUARIOS Us On odd.IDProveedor_Prg = Us.IDUsuario             
 Left Join MAPROVEEDORES p On odd.IDProveedor_Prg = p.IDProveedor Left Join MATIPOPROVEEDOR Tp On p.IDTipoProv = Tp.IDTipoProv            
 Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente                
 Left Join MASERVICIOS_DET sCot On odd.IDServicio_Det_V_Cot = sCot.IDServicio_Det                
 Left Join MASERVICIOS_DET sPrg On odd.IDServicio_Det_V_Prg = sPrg.IDServicio_Det                
 Left Join MAUBIGEO ub On od.IDubigeo = ub.IDubigeo                
 Left Join COTIDET_DETSERVICIOS cds On odd.IDServicio_Det = cds.IDServicio_Det  and cds.IDDET in (select IDDET from COTIDET where IDCab=c.IDCAB)    -- rd.IDDet = cds.IDDET     
 Left Join MASERVICIOS_DET d  On odd.IDServicio_Det = d.IDServicio_Det    

 where (IDProveedor_Prg is not null) and odd.Activo = 1 -- and rd.Anulado = 0      
 and (odd.IDEstadoSolicitud <> 'NV')  and (@IDTipoProv = '017')-- Or Tp.IDTipoProv='005')            
 and (ltrim(rtrim(@IDProveedor))='' or odd.IDProveedor_Prg = @IDProveedorGuia Or ltrim(rtrim(odd.IDProveedor_Prg)) = ltrim(rtrim(@IDProveedor)))            
 and (LTRIM(rtrim(@IDCiudad))='' Or od.IDubigeo = @IDCiudad)                
 and (LTRIM(rtrim(@CoPais))='' Or ub.IDPais = @CoPais)                
 and (LTRIM(rtrim(@IDIoma))='' Or od.IDIdioma = @IDIoma)                
 and (LTRIM(rtrim(@Estado))='' Or c.Estado = @Estado)    
 and (@Nuvehiculo = 0 Or Isnull(odd.IDVehiculo_Prg,0) = @Nuvehiculo)              
 and Isnull(Us.IDUsuario,(select IDUsuarioTrasladista from MAPROVEEDORES p Where odd.IDProveedor_Prg = p.IDProveedor)) is not null          
 and (((LTRIM(rtrim(Convert(Char(10),@FecRango1,103)))='01/01/1900') and (LTRIM(rtrim(Convert(Char(10),@FecRango2,103)))='01/01/1900'))                
  Or cast(Convert(char(10),od.Dia,103) as smalldatetime) between @FecRango1 and @FecRango2)

  and o.IDReserva is null
  and od.IDReserva_Det is null
  
  ) as X            
  where X.ProveedorProgramado is not null            
 Order by X.IDFile,Cast(X.Fecha as smalldatetime)        
End          

