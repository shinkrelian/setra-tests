﻿--JRF-20150223-Considerar Nuevo Campod [FlServicioNoShow]
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_UpdxAcomodoTMP      
 @IDOperacion_Det int,    
 @UserMod char(4)    
As  
 Set Nocount On  
   
 UPDATE OPERACIONES_DET_DETSERVICIOS  
    SET   
      [CostoReal] = Orig.CostoReal  
      ,[CostoLiberado] = Orig.CostoLiberado  
      ,[Margen] = Orig.Margen  
      ,[MargenAplicado] = Orig.MargenAplicado  
      ,[MargenLiberado] = Orig.MargenLiberado  
      ,[Total] = Orig.Total  
  
      ,[SSCR] = Orig.SSCR  
      ,[SSCL] = Orig.SSCL  
      ,[SSMargen] = Orig.SSMargen  
      ,[SSMargenLiberado] = Orig.SSMargenLiberado  
      ,[SSTotal] = Orig.SSTotal  
      ,[STCR] = Orig.STCR  
      ,[STMargen] = Orig.STMargen  
      ,[STTotal] = Orig.STTotal  
      ,[CostoRealImpto] = Orig.CostoRealImpto  
      ,[CostoLiberadoImpto] = Orig.CostoLiberadoImpto  
      ,[MargenImpto] = Orig.MargenImpto  
      ,[MargenLiberadoImpto] = Orig.MargenLiberadoImpto  
      ,[SSCRImpto] = Orig.SSCRImpto  
      ,[SSCLImpto] = Orig.SSCLImpto  
      ,[SSMargenImpto] = Orig.SSMargenImpto  
      ,[SSMargenLiberadoImpto] = Orig.SSMargenLiberadoImpto  
      ,[STCRImpto] = Orig.STCRImpto  
      ,[STMargenImpto] = Orig.STMargenImpto  
      ,[TotImpto] = Orig.TotImpto  
      ,[FlServicioNoShow] = Orig.FlServicioNoShow
        
      ,[IDEstadoSolicitud] =   
  Case When Dest.IDEstadoSolicitud <> 'NV' Then 'PA' Else 'NV' End        
        
      ,[UserMod] = @UserMod  
      ,[FecMod] = getdate()                    
        
 FROM OPERACIONES_DET_DETSERVICIOS Dest Inner Join OPERACIONES_DET_DETSERVICIOS_TMP Orig On     
 Orig.IDOperacion_DetReal = Dest.IDOperacion_Det And Orig.IDOperacion_Det=@IDOperacion_Det  
 WHERE Dest.IDOperacion_Det = @IDOperacion_Det  
