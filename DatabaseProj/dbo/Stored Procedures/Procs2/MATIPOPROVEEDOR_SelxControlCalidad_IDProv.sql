﻿--MATIPOPROVEEDOR_SelxControlCalidad_IDProv '000098'
Create Procedure dbo.MATIPOPROVEEDOR_SelxControlCalidad_IDProv
@CoProveedor char(6)=''
as
Set NoCount On
Select tp.IDTipoProv,Descripcion as TipoProv,c.NuCuest,c.NoDescripcion as Pregunta
from MATIPOPROVEEDOR tp Left Join MACUESTIONARIO c On tp.IDTipoProv=c.IDTipoProv
where FlTieneControlCalidad=1 and 
(@CoProveedor='' or c.IDTipoProv=(select IDTipoProv from MAPROVEEDORES where IDProveedor=@CoProveedor))
