﻿--HLF-20131126-IsNull((Select Top 1 IDOperacion_Det From OPERACIONES_DET od1 ....
CREATE Procedure dbo.OPERACIONES_DET_PAX_InsxIDOperacion       
 @IDOperacion int,      
 @UserMod char(4)      
As      
 Set Nocount On      
       
 Insert Into OPERACIONES_DET_PAX(IDOperacion_Det,IDPax,UserMod)       
 Select      
 --(Select IDOperacion_Det From  OPERACIONES_DET Where IDReserva_Det=d.IDReserva_Det),      
  (Select Top 1 IDOperacion_Det From OPERACIONES_DET od1 Inner Join RESERVAS_DET rd1 On od1.IDReserva_Det=rd1.IDReserva_Det
    And rd1.Anulado=0
	Where od1.IDReserva_Det=d.IDReserva_Det ORder by od1.FecMod Desc),

 IDPax,@UserMod From RESERVAS_DET_PAX d Inner Join RESERVAS_DET rd On d.IDReserva_Det=rd.IDReserva_Det And rd.Anulado=0    
 Where d.IDReserva_Det In       
 (Select IDReserva_Det From OPERACIONES_DET Where IDOperacion In       
 (Select IDOperacion From OPERACIONES Where IDOperacion=@IDOperacion))       
    
