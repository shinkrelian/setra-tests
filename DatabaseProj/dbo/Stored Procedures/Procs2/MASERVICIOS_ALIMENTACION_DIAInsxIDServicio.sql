﻿Create Procedure dbo.MASERVICIOS_ALIMENTACION_DIAInsxIDServicio
@IDServicio char(8),
@IDServicioNuevo char(8)
As
Set NoCount On
Insert into MASERVICIOS_ALIMENTACION_DIA
SELECT @IDServicioNuevo as [IDServicio]
      ,[Dia]
      ,[Desayuno]
      ,[Lonche]
      ,[Almuerzo]
      ,[Cena]
      ,[IDUbigeoOri]
      ,[IDUbigeoDes]
      ,[Mig]
      ,[UserMod]
      ,[FecMod]
  FROM [dbo].[MASERVICIOS_ALIMENTACION_DIA]
Where IDServicio = @IDServicio
