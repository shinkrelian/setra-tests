﻿Create Procedure dbo.MAPROVEEDORES_Sel_CboTodosNoGuiasNoTrans
 @bTodos bit,  
 @IDTipoProv char(3)    
As    
 Set NoCount On    
   
 SELECT * FROM    
 (    
 Select '' as IDProveedor,'<TODOS>' as DescProveedor, 0 as Ord    
 Union    
     
 Select IDProveedor, NombreCorto as DescProveedor,1 as Ord  From MAPROVEEDORES    
 Where (IDTipoProv=@IDTipoProv OR LTRIM(RTRIM(@IDTipoProv))='') and Activo = 'A'  
 and IDTipoProv<> '004' and IDTipoProv <> '005'
 ) AS X    
 Where (@bTodos=1 Or Ord<>0)     
 Order by Ord,2   
