﻿
Create Procedure [dbo].[OPERACIONES_DET_SelExisteVoucherxIDCabOutput]
	@IDCab  int,
	@pExiste bit output
As
	Set NoCount On
	
	set @pExiste = 0
	If Exists (Select IDVoucher from OPERACIONES_DET 
		Where IDOperacion in (Select IDOperacion From OPERACIONES Where IDCab=@IDCab)
		And IDVoucher<>0)
		set @pExiste = 1
	
			
