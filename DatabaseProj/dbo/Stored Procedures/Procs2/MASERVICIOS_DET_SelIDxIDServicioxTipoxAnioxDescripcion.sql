﻿--JRF-20140325-Se agrega un filtro adicional. [IDServicio_Det_V] - Cancelado
CREATE Procedure dbo.MASERVICIOS_DET_SelIDxIDServicioxTipoxAnioxDescripcion  
@IDServicio char(8),  
@Anio char(4),  
@Variante varchar(7),  
@Descripcion varchar(200),  
--@IDServicio_Det_V int,
@pIDServicio_Det int Output  
As  
Set NoCount On  
Set @pIDServicio_Det = (Select Top(1) IDServicio_Det from MASERVICIOS_DET   
Where IDServicio = @IDServicio  and Anio = @Anio and Lower(ltrim(rtrim(Isnull(Tipo,'')))) = Lower(ltrim(rtrim(@Variante)))  
 and LOWER(ltrim(rtrim(Descripcion))) =LOWER(ltrim(rtrim(@Descripcion))))--and isnull(IDServicio_Det_V,0) = @IDServicio_Det_V)  
