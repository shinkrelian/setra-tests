﻿--HLF-20150108-Left Join OPERACIONES_DET_DETSERVICIOS odds On rd.IDOperacion_Det=odds.IDOperacion_Det
CREATE Procedure OPERACIONES_DET_SelxCoOrdenPago               
 @IDVoucher varchar(14)='',--'918'  
 @IDCab int = 0--7396                                                        
As                                                                  
 Set NoCount On         
       
 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)      
 Select X.IDReserva_Det,  
 X.IDOperacion_Det,  
 X.IDReserva,  
 X.IDOperacion,  
 X.IDFile,  
 X.IDDet,  
 X.Item,  
 X.Dia,  
 X.DiaFormat,      
 X.FechaOutOper1,  
 X.FechaOutOper2,  
 X.Hora,  
 X.Ubigeo,  
 X.Noches,  
 X.IDTipoProv,  
 X.Dia,  
 X.IDProveedor,  
 X.Proveedor,      
 X.CorreoProveedor,  
 X.IDPais,X.Ubigeo,X.Cantidad,X.NroPax,X.PaxmasLiberados,x.IDServicio,x.IDServicio_Det,x.DescServicioDet,      
 x.DescServicioDetBup,x.Especial,x.MotivoEspecial,x.RutaDocSustento,x.Desayuno,x.Lonche,X.Almuerzo,X.Cena,X.Transfer,X.IDDetTransferOri,      
 X.IDDetTransferDes,X.CamaMat,X.TipoTransporte,X.IDUbigeoOri,X.DescUbigeoOri,X.IDUbigeoDes,X.DescUbigeoDes,X.CodIdioma,X.IDTipoServ,      
 X.Idioma,X.Anio,X.Tipo,X.DetaTipo,X.CantidadAPagar,X.NroLiberados,X.Tipo_Lib,X.CostoRealAnt,X.Margen,X.MargenAplicado,X.MargenAplicadoAnt,      
 X.MargenLiberado,x.TotalOrig,x.CostoRealImpto,x.CostoLiberadoImpto,x.MargenImpto,x.MargenLiberadoImpto,x.TotImpto,x.IDDetRel,X.Total,      
 x.IDVoucher,x.IDReserva_DetCopia,x.IDReserva_Det_Rel,x.IDEmailEdit,x.IDEmailNew,x.IDEmailRef,x.ObservVoucher,x.ObservBiblia,x.ObservInterno,      
 X.CapacidadHab,x.EsMatrimonial,x.PlanAlimenticio,x.IncGuia,x.chkServicioExportacion,x.VerObserVoucher,x.VerObserBiblia,x.CostoReal,      
 x.CostoLiberado,      
 --x.NetoHab as NetoHab,      
 (isnull(X.NetoGen,0))/x.NroPax as NetoHab,      
 --X.IgvHab as IgvHab,      
 (case when X.IgvCotizado is null then                          
 Cast(Case When X.Afecto=1 Then X.NetoGen*(@NuIgv/100) Else 0 End as numeric(8,2))                          
 else X.IgvProgram End) / x.NroPax as IgvHab,      
 --X.TotalHab as TotalHab,      
 ( isnull(Case when isnull(TotalCotizadoCal,0) = 0 then                          
  Cast(Case When Afecto=1 Then                       
  NetoGen+(NetoGen*(@NuIgv/100))                       
  Else                       
  NetoGen                       
  End as numeric(8,2))                           
 else                          
 TotalProgram                          
 End,0))/x.NroPax as TotalHab,      
 --X.NetoGen,      
 isnull(NetoGen,0) as NetoGen,      
       
 --X.IgvGen,      
 case when X.IgvCotizado is null then                          
 Cast(Case When X.Afecto=1 Then X.NetoGen*(@NuIgv/100) Else 0 End as numeric(8,2))                          
 else X.IgvProgram End as IgvGen,      
 X.IDMoneda,X.SimboloMoneda,X.TipoCambio,      
      
 isnull(Case when isnull(TotalCotizadoCal,0) = 0 then                          
  Cast(Case When Afecto=1 Then                       
  NetoGen+(NetoGen*(@NuIgv/100))                       
  Else                       
  NetoGen                       
  End as numeric(8,2))                           
 else                          
 TotalProgram                          
 End,0) as TotalGen,      
       
 X.IDGuiaProveedor,X.DescBus,X.IDTipoOC,X.InactivoDet,X.ServHotelAlimentPuro,X.ConAlojamiento,  
 0 as SSCR,  
 X.PNR,    
 X.CtaContable,  
 X.ServicioEditado,  
 X.ServicioVarios    
 From (      
   
   
  Select rd.IDReserva_Det,                                                      
 Rd.IDOperacion_Det,                                                      
 Rv.IDReserva as IDReserva,                                                      
 rd.IDOperacion,                                                       
 rv.IDFile,                                                      
 0 as IDDet,                                                      
 rd.Item,          
 Rd.Dia,                                                        
 Case When Pr.IDTipoProv='001' Then                                                     
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                       
 Else                                                
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                        
 End as DiaFormat,                                                          
 IsNull(Rd.FechaOut,'') as FechaOutOper1,                                                      
 IsNull(upper(substring(DATENAME(dw,Rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,Rd.FechaOut,103)/*+' '+ substring(convert(varchar,Rd.FechaOut,108),1,5)*/)),'') as FechaOutOper2,      
 Case When Pr.IDTipoProv='001' and sd.PlanAlimenticio= 0 Then                                                       
 ''                                                      
 Else                                                       
  convert(varchar(5),rd.Dia,108)                                                      
 End as Hora, Ub.Descripcion as Ubigeo, Isnull(Rd.Noches,0) as Noches, Pr.IDTipoProv,                                                 
 s.Dias, rv.IDProveedor, Pr.NombreCorto as Proveedor, ISNull(pr.Email3,'') CorreoProveedor ,       
 Ub.IDPais, Rd.IDubigeo,Rd.Cantidad,Rd.NroPax,CAST(rd.NroPax AS VARCHAR(3))+                            
 Case When isnull(rd.NroLiberados,0)=0 then '' else '+'+CAST(rd.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,      
 Rd.IDServicio, Rd.IDServicio_Det, sd.Descripcion as DescServicioDet,      
 --Rd.Servicio As DescServicioDetBup,                                                         
 Case When pr.IDTipoProv='001' or pr.IDTipoProv='002' Then  --hoteles o restaurantes                                            
 sd.Descripcion                                               
 Else                                          
 s.Descripcion                                              
 End As DescServicioDetBup,                                               
 Rd.Especial,Rd.MotivoEspecial,  
 --rd.RutaDocSustento  
 --cd.RutaDocSustento  
 RutaDocSustento=case when rd.RutaDocSustento IS null then (isnull(cd.RutaDocSustento,'')) else rd.RutaDocSustento end,  
 Rd.Desayuno,Rd.Lonche,Rd.Almuerzo,Rd.Cena,                                                      
 Rd.Transfer,Rd.IDDetTransferOri,Rd.IDDetTransferDes,0 as CamaMat,Rd.TipoTransporte,                                                      
 Rd.IDUbigeoOri,                                                      
 uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                        
 Rd.IDUbigeoDes,                                                      
 ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                                        
 idio.IDidioma as CodIdioma , Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,      
 idio.Siglas as Idioma , sd.Anio, sd.Tipo, Isnull(sd.DetaTipo,'') As DetaTipo ,                                 
 Rd.CantidadAPagar, Rd.NroLiberados,Rd.Tipo_Lib,Rd.CostoRealAnt,Rd.Margen, Rd.MargenAplicado,Rd.MargenAplicadoAnt,Rd.MargenLiberado,Rd.TotalOrig      
 ,Rd.CostoRealImpto,Rd.CostoLiberadoImpto      
 ,Rd.MargenImpto,Rd.MargenLiberadoImpto,Rd.TotImpto,                                                      
 '0' as IDDetRel ,                              
                               
 Rd.Total,                                                      
 --Case When Cast(Rd.IDVoucher as varchar(10))='0' Then '' Else Cast(Rd.IDVoucher as varchar(10)) End as IDVoucher,      
 --SUBSTRING(@IDVoucher,1,8)+'-'+SUBSTRING(@IDVoucher,9,LEN(@IDVoucher)) as IDVoucher,      
   
 'OP-'+(CAST(op.IDOrdPag as varchar(5))) as IDVoucher,      
   
 '0' as IDReserva_DetCopia ,'0' as IDReserva_Det_Rel,                                                
                                                       
 '' as IDEmailEdit, '' as IDEmailNew, '' as IDEmailRef,         
 --IsNull(Rd.FechaRecordatorio,'01/01/1900') as FechaRecordatorio, IsNull(Rd.Recordatorio,'') as Recordatorio,                                                     
 --IsNull(Rd.ObservVoucher,'') as ObservVoucher,                     
 --IsNull(vo.ObservVoucher,'') as ObservVoucher,                     
 '' as ObservVoucher,      
 IsNull(Rd.ObservBiblia,'') as ObservBiblia,                                                       
 IsNull(Rd.ObservInterno,'') as ObservInterno  , rdd.CapacidadHab as CapacidadHab , rdd.EsMatrimonial As EsMatrimonial   ,                                                 
 sd.PlanAlimenticio ,  cd.IncGuia,                                          
 0 As chkServicioExportacion , Rd.VerObserVoucher ,Rd.VerObserBiblia  ,                                                         
                                         
 Rd.CostoReal, Rd.CostoLiberado,                                        
  Rd.NetoHab,Rd.IgvHab,Rd.TotalHab,      
 dbo.FnNetoMaServicio_Det(sd.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)) a,      
       
 Case When isnull(odds.NetoCotizado,0) = 0 then                                                       
   Case When sd.IDServicio_Det IS NULL Then                 
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(odds.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)),                          
    sd.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)      
  Else                
    dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(sd.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)),                        
    sd.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)      
  End                
  Else                       
  odds.NetoProgram      
 End As NetoGen,      
        
  Rd.IgvGen,                              
  --isnull(odds.IDMoneda,'') as IDMoneda,               
  isnull(Rdd.IDMoneda,'') as IDMoneda,               
  ISNULL(mon.Simbolo,'') as SimboloMoneda,      
  isnull(odds.TipoCambio,0) as TipoCambio,      
  --Rd.TotalGen ,      
 Case When Pr.IDTipoOper = 'I' Then                   
 Isnull((select Top 1 IDProveedor_Prg                   
 from OPERACIONES_DET_DETSERVICIOS odd2 Left Join MAPROVEEDORES pr On odd2.IDProveedor_Prg = pr.IDProveedor                  
 where pr.IDTipoProv = '005' and odd2.IDOperacion_Det = rd.IDOperacion_Det Order by odd2.FecMod Desc ),'')                  
 Else Isnull(Rd.IDGuiaProveedor,'') End As IDGuiaProveedor,                  
 0 As DescBus,  
 --rd.idTipoOC   
 sd.idTipoOC  
 ,0 as InactivoDet, 0 AS ServHotelAlimentPuro,      
 sd.ConAlojamiento, cd.SSCR,dbo.FnPNRxIDCabxDia(/*@IDCab*/Rv.IDCab,Rd.Dia) as PNR      
 ,odds.TotalCotizado as TotalCotizadoCal,sd.Afecto, Isnull(odds.NetoProgram,0) as NetoProgram,                                    
 Isnull(odds.IgvProgram,0) as IgvProgram,                                    
 Isnull(odds.TotalProgram,0) as TotalProgram  ,odds.Total_PrgEditado,      
 odds.IDMoneda as IDMonedaOper,odds.IgvCotizado,sd.CtaContable     
   
 ,ServicioEditado=isnull(cd.ServicioEditado,CAST(0 as bit))  
 ,Case When             
 Exists(select sd2.IDServicio from MASERVICIOS_DET SD2 Left Join MASERVICIOS S2 on SD2.IDServicio = S2.IDServicio            
  where s2.IDCabVarios = @IDCAB and sd2.IDServicio_Det =cd.IDServicio_Det) then 1 else 0 End As ServicioVarios  
    
  
From   
   
 ORDENPAGO op  
 LEFT JOIN dbo.ORDENPAGO_DET Opd on op.IDOrdPag=Opd.IDOrdPag  
   
  --Left Join OPERACIONES_DET_DETSERVICIOS odds On Opd.IDServicio_Det=odds.IDServicio_Det   
   
 LEFT JOIN RESERVAS re on re.IDReserva=op.IDReserva  
   
 Left Join RESERVAS_DET rdd On re.IDReserva=rdd.IDReserva and opd.IDReserva_Det=rdd.IDReserva_Det         
   
 Left Join OPERACIONES_DET Rd   ON Rd.IDReserva_Det=rdd.IDReserva_Det     
 LEFT join     OPERACIONES Rv   On Rv.IDCab=@IDCab and Rd.IDOperacion=Rv.IDOperacion   
                                                      
 Left Join OPERACIONES_DET_DETSERVICIOS odds On rd.IDOperacion_Det=odds.IDOperacion_Det
   
 Left Join MAUBIGEO Ub On Rd.IDubigeo=Ub.IDubigeo                                                               
 Left Join MAUBIGEO uo On Rd.IDUbigeoOri=uo.IDubigeo                                                          
 Left Join MAUBIGEO ud On Rd.IDUbigeoDes=ud.IDubigeo                                                           
 Left Join MAPROVEEDORES Pr On  Pr.IDProveedor= Rv.IDProveedor                                                              
 Left Join MASERVICIOS_DET sd On  Rd.IDServicio_Det=sd.IDServicio_Det                                                        
 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio                                                        
 Inner Join VOUCHER_OPERACIONES vo On Rd.IDVoucher= vo.IDVoucher                                                      
                                    
                               
                                    
 Left Join COTIDET cd On rdd.IDDet=cd.IDDET                                            
 Left Join MAMONEDAS mon on Rdd.IDMoneda=mon.IDMoneda                                
 Left Join MAIDIOMAS idio On Rd.IDIdioma = idio.IDidioma       
   
   /*                    
 Select rd.IDReserva_Det,                                                      
 Rd.IDOperacion_Det,                                                      
 Rv.IDReserva as IDReserva,                                                      
 rd.IDOperacion,                                                       
 rv.IDFile,                                                      
 0 as IDDet,                                                      
 rd.Item,                                                      
 Rd.Dia,                                                        
 Case When Pr.IDTipoProv='001' Then                                                     
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                       
 Else                                                
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                        
 End as DiaFormat,                                                          
 IsNull(Rd.FechaOut,'') as FechaOutOper1,                                                      
 IsNull(upper(substring(DATENAME(dw,Rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,Rd.FechaOut,103)/*+' '+ substring(convert(varchar,Rd.FechaOut,108),1,5)*/)),'') as FechaOutOper2,      
 Case When Pr.IDTipoProv='001' and dCot.PlanAlimenticio= 0 Then                                                       
 ''                                                      
 Else                                                       
  convert(varchar(5),rd.Dia,108)                                                      
 End as Hora, Ub.Descripcion as Ubigeo, Isnull(Rd.Noches,0) as Noches, Pr.IDTipoProv,                                                 
 s.Dias, rv.IDProveedor, Pr.NombreCorto as Proveedor, ISNull(pr.Email3,'') CorreoProveedor ,       
 Ub.IDPais, Rd.IDubigeo,Rd.Cantidad,Rd.NroPax,CAST(rd.NroPax AS VARCHAR(3))+                            
 Case When isnull(rd.NroLiberados,0)=0 then '' else '+'+CAST(rd.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,      
 Rd.IDServicio, Rd.IDServicio_Det, dCot.Descripcion as DescServicioDet,      
 --Rd.Servicio As DescServicioDetBup,                                                         
 Case When pr.IDTipoProv='001' or pr.IDTipoProv='002' Then  --hoteles o restaurantes                                            
 dCot.Descripcion                                               
 Else         
 s.Descripcion                                              
 End As DescServicioDetBup,                                               
 Rd.Especial,Rd.MotivoEspecial,  
 --rd.RutaDocSustento  
 --cd.RutaDocSustento  
 RutaDocSustento=case when rd.RutaDocSustento IS null then (isnull(cd.RutaDocSustento,'')) else rd.RutaDocSustento end,  
 Rd.Desayuno,Rd.Lonche,Rd.Almuerzo,Rd.Cena,                                                      
 Rd.Transfer,Rd.IDDetTransferOri,Rd.IDDetTransferDes,0 as CamaMat,Rd.TipoTransporte,                                                      
 Rd.IDUbigeoOri,                                                      
 uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                        
 Rd.IDUbigeoDes,                                                      
 ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                                        
 idio.IDidioma as CodIdioma , Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,      
 idio.Siglas as Idioma , dCot.Anio, dCot.Tipo, Isnull(dCot.DetaTipo,'') As DetaTipo ,                                 
 Rd.CantidadAPagar, Rd.NroLiberados,Rd.Tipo_Lib,Rd.CostoRealAnt,Rd.Margen, Rd.MargenAplicado,Rd.MargenAplicadoAnt,Rd.MargenLiberado,Rd.TotalOrig      
 ,Rd.CostoRealImpto,Rd.CostoLiberadoImpto      
 ,Rd.MargenImpto,Rd.MargenLiberadoImpto,Rd.TotImpto,                                                      
 '0' as IDDetRel ,                              
                               
 Rd.Total,                                                      
 --Case When Cast(Rd.IDVoucher as varchar(10))='0' Then '' Else Cast(Rd.IDVoucher as varchar(10)) End as IDVoucher,      
 SUBSTRING(@IDVoucher,1,8)+'-'+SUBSTRING(@IDVoucher,9,LEN(@IDVoucher)) as IDVoucher,      
 '0' as IDReserva_DetCopia ,'0' as IDReserva_Det_Rel,                                                
                                                       
 '' as IDEmailEdit, '' as IDEmailNew, '' as IDEmailRef,         
 --IsNull(Rd.FechaRecordatorio,'01/01/1900') as FechaRecordatorio, IsNull(Rd.Recordatorio,'') as Recordatorio,                                                     
 --IsNull(Rd.ObservVoucher,'') as ObservVoucher,                     
 --IsNull(vo.ObservVoucher,'') as ObservVoucher,                     
 '' as ObservVoucher,      
 IsNull(Rd.ObservBiblia,'') as ObservBiblia,                                                       
 IsNull(Rd.ObservInterno,'') as ObservInterno  , rdd.CapacidadHab as CapacidadHab , rdd.EsMatrimonial As EsMatrimonial   ,                                                 
 dCot.PlanAlimenticio ,  cd.IncGuia,                                          
 0 As chkServicioExportacion , Rd.VerObserVoucher ,Rd.VerObserBiblia  ,                                                         
                                         
 Rd.CostoReal, Rd.CostoLiberado,                                        
  Rd.NetoHab,Rd.IgvHab,Rd.TotalHab,      
 dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)) a,      
       
 Case When isnull(odds.NetoCotizado,0) = 0 then                                                       
   Case When dCot.IDServicio_Det IS NULL Then                 
   dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(odds.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)),                          
    dCot.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)      
  Else                
    dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,Rd.NroPax+isnull(Rd.NroLiberados,0)),                        
    dCot.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)      
  End                
  Else                       
  odds.NetoProgram      
 End As NetoGen,      
        
  Rd.IgvGen,                              
  isnull(odds.IDMoneda,'') as IDMoneda,               
  ISNULL(monOdd.Simbolo,'') as SimboloMoneda,      
  isnull(odds.TipoCambio,0) as TipoCambio,      
  --Rd.TotalGen ,      
 Case When Pr.IDTipoOper = 'I' Then                   
 Isnull((select Top 1 IDProveedor_Prg                   
 from OPERACIONES_DET_DETSERVICIOS odd2 Left Join MAPROVEEDORES pr On odd2.IDProveedor_Prg = pr.IDProveedor                  
 where pr.IDTipoProv = '005' and odd2.IDOperacion_Det = rd.IDOperacion_Det Order by odd2.FecMod Desc ),'')                  
 Else Isnull(Rd.IDGuiaProveedor,'') End As IDGuiaProveedor,                  
 0 As DescBus,  
 --rd.idTipoOC   
 sd.idTipoOC  
 ,0 as InactivoDet, 0 AS ServHotelAlimentPuro,      
 dCot.ConAlojamiento, cd.SSCR,dbo.FnPNRxIDCabxDia(/*@IDCab*/Rv.IDCab,Rd.Dia) as PNR      
 ,odds.TotalCotizado as TotalCotizadoCal,dCot.Afecto, Isnull(odds.NetoProgram,0) as NetoProgram,                                    
 Isnull(odds.IgvProgram,0) as IgvProgram,                                    
 Isnull(odds.TotalProgram,0) as TotalProgram  ,odds.Total_PrgEditado,      
 odds.IDMoneda as IDMonedaOper,odds.IgvCotizado,sd.CtaContable     
   
 ,ServicioEditado=isnull(cd.ServicioEditado,CAST(0 as bit))  
 ,Case When             
 Exists(select sd2.IDServicio from MASERVICIOS_DET SD2 Left Join MASERVICIOS S2 on SD2.IDServicio = S2.IDServicio            
  where s2.IDCabVarios = @IDCAB and sd2.IDServicio_Det =cd.IDServicio_Det) then 1 else 0 End As ServicioVarios  
    
 From   
   
 ORDENPAGO op  
 LEFT JOIN dbo.ORDENPAGO_DET Opd on op.IDOrdPag=Opd.IDOrdPag  
   
 /*  
 ORDEN_SERVICIO os      
 Left Join ORDEN_SERVICIO_DET osd On os.NuOrden_Servicio=osd.NuOrden_Servicio      
 */  
   
   
   
 Left Join OPERACIONES_DET_DETSERVICIOS odds On Opd.IDServicio_Det=odds.IDServicio_Det and  
 --osd.IDOperacion_Det=odds.IDOperacion_Det and  
  odds.Activo=1      
   
 Left Join OPERACIONES_DET Rd On odds.IDOperacion_Det=Rd.IDOperacion_Det      
 Left Join OPERACIONES Rv On Rd.IDOperacion=Rv.IDOperacion      
 Left Join MAUBIGEO Ub On Rd.IDubigeo=Ub.IDubigeo                                                             
 Left Join MAUBIGEO uo On Rd.IDUbigeoOri=uo.IDubigeo                                                        
 Left Join MAUBIGEO ud On Rd.IDUbigeoDes=ud.IDubigeo                                                         
 Left Join MAPROVEEDORES Pr On  Pr.IDProveedor= Rv.IDProveedor                                                            
 Left Join MASERVICIOS_DET dCot On Isnull(odds.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det --Rd.IDServicio_Det=sd.IDServicio_Det      
 Inner Join MASERVICIOS s On dCot.IDServicio=s.IDServicio                                                 
 --Inner Join VOUCHER_OPERACIONES vo On Rd.IDVoucher= vo.IDVoucher      
 Left Join RESERVAS_DET rdd On Rd.IDReserva_Det=rdd.IDReserva_Det      
 Left Join COTIDET cd On rdd.IDDet=cd.IDDET                                          
 Left Join MAMONEDAS mon on Rdd.IDMoneda=mon.IDMoneda                              
 Left Join MAIDIOMAS idio On Rd.IDIdioma = idio.IDidioma      
 Left Join MAMONEDAS monOdd on odds.IDMoneda=monOdd.IDMoneda      
 Left Join MASERVICIOS_DET sd On Rd.IDServicio_Det=sd.IDServicio_Det   
*/  
  
where  --os.CoOrden_Servicio = @IDVoucher)      
op.IDOrdPag = @IDVoucher  
  
)      
  
as X      
--Order By Rd.IDVoucher desc,Rd.Item Asc      
Order By X.Dia      
  
