﻿
CREATE PROCEDURE dbo.PRORRATA_SelxFechasOutput
	
	@Fecha smalldatetime,
	@pQtProrrata numeric(8,2) output
As
	Set Nocount On
	
	Select @pQtProrrata=QtProrrata From PRORRATA
	Where @Fecha Between FeDesde And FeHasta
	
	Set @pQtProrrata=isnull(@pQtProrrata,0)
	
