﻿--JRF-20160219-Agregar columna [CoEstadoPedido]
--JRF-20160329-And x Or
--JRF-20160413-Agregar filtro [@DescCliente]
CREATE Procedure dbo.PEDIDO_Sel_ListAddIn
@NroPedido char(9),
@IDFile char(8),
@Cotizacion char(8),
@Titulo varchar(200),
@DescCliente varchar(200),
@IDUsVentas char(4),
@Estado char(2),
@FlHistorico bit
As
Set NoCount On
select x.NuPedidoInt,x.EjeVentPedidos,x.FechaPedido,x.NroPedido,X.IDClientePed,X.DescCliente,X.TituloPedido,X.CoEstadoPedido,X.EstadoPedidos --,x.FePedido
 from (
Select Distinct p.NuPedInt as NuPedidoInt,uVent.Nombre +' '+IsNull(uVent.TxApellidos,'') As EjeVentPedidos,
	   Convert(char(10),p.FePedido,103) as FechaPedido,p.NuPedido As NroPedido,p.CoCliente as IDClientePed,cl.RazonComercial as DescCliente,
	   TxTitulo as TituloPedido,p.CoEstado As CoEstadoPedido,
	   Case p.CoEstado
	    When 'PD' Then 'PENDIENTE'
		--When 'CE' Then 'COTIZACION ENVIADA'
		When 'AC' Then 'ACEPTADO'
		When 'AN' Then 'ANULADO' 
	   End as EstadoPedidos,p.FePedido
from PEDIDO p Left Join MAUSUARIOS uVent On p.CoEjeVta=uVent.IDUsuario
Left Join MACLIENTES cl On p.CoCliente=cl.IDCliente
Left Join COTICAB c On p.NuPedInt=c.NuPedInt
Where 
--Exists(select IDCab from coticab c where p.NuPedInt = c.NuPedInt 
--			 And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)
--			 And (c.IDFile = @IDFile Or Ltrim(Rtrim(@IDFile))='')
--			 And (c.Cotizacion = @Cotizacion Or Ltrim(Rtrim(@Cotizacion))='')) 
(IsNull(c.FlHistorico,0)=@FlHistorico Or @FlHistorico=1)
And (IsNull(c.IDFile,'') = @IDFile Or Ltrim(Rtrim(@IDFile))='')
And (IsNull(c.Cotizacion,'') = @Cotizacion Or Ltrim(Rtrim(@Cotizacion))='')
And (p.NuPedido=@NroPedido Or Ltrim(rtrim(@NroPedido))='')
And (p.CoEjeVta=@IDUsVentas Or Ltrim(RTrim(@IDUsVentas))='')
And (p.TxTitulo Like '%' +@Titulo + '%' Or Ltrim(Rtrim(@Titulo))='')
And (p.CoEstado=@Estado Or Ltrim(Rtrim(@Estado))='')
And (cl.RazonComercial Like '%'+@DescCliente+'%' Or Ltrim(Rtrim(@DescCliente))='')
) As X
Order By x.FePedido desc
