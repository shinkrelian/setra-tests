﻿
-----------------------------------------------------------------------------------------------

--JHD-20150216- Se agrego el campo TxDefinicion
CREATE Procedure [dbo].[MAPLANCUENTAS_Upd]
@CtaContable varchar(8),
@Descripcion varchar(150),
@UserMod char(4),
@CoCtroCosto char(6),
@FlFacturacion bit,
@FlClientes bit,
@TxDefinicion varchar(255)=''
As
	Set NoCount On
	Update MAPLANCUENTAS 
		Set Descripcion = @Descripcion,
			UserMod = @UserMod,
			CoCtroCosto =Case When ltrim(rtrim(@CoCtroCosto))='' Then Null Else @CoCtroCosto End ,
			FlFacturacion =@FlFacturacion,
			FlClientes =@FlClientes,
			TxDefinicion =Case When ltrim(rtrim(@TxDefinicion))='' Then Null Else @TxDefinicion End 
		Where CtaContable = @CtaContable
