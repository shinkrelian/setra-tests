﻿CREATE Procedure [dbo].[MAUSUARIOS_Upd_Jefe]               
 @IDUsuario char(4),         
 @CoUserJefe char(4)='',
 @UserMod char(4)
As      
      
 Set NoCount On      
      
 UPDATE MAUSUARIOS      
    SET		CoUserJefe=Case When Ltrim(Rtrim(@CoUserJefe))='' Then Null Else @CoUserJefe End  
           ,UserMod=@UserMod      
              
 WHERE cast(IDUsuario as int)=Cast(@IDUsuario as int)
