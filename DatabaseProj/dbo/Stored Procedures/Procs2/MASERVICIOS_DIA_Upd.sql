﻿

--HLF-20151012-@NoHotel varchar(100),@TxWebHotel varchar(150),
--HLF-20160122-@TxDireccHotel,@TxTelfHotel1,@TxTelfHotel2,@FeHoraChkInHotel,@FeHoraChkOutHotel,@CoTipoDesaHotel,@CoCiudadHotel
CREATE Procedure [dbo].[MASERVICIOS_DIA_Upd]
	@IDServicio char(8),
	@Dia tinyint,
	@IDIdioma varchar(12),
	--@DescripCorta varchar(100),
	@Descripcion text,
	@NoHotel varchar(100),
	@TxWebHotel varchar(150),

	@TxDireccHotel varchar(150),
	@TxTelfHotel1 varchar(50),
	@TxTelfHotel2 varchar(50),
	@FeHoraChkInHotel smalldatetime,
	@FeHoraChkOutHotel smalldatetime,
	@CoTipoDesaHotel char(2), 
	@CoCiudadHotel char(6),

	@UserMod char(4)
As
	Set NoCount On
	
	Update MASERVICIOS_DIA           
           Set --DescripCorta=@DescripCorta,
           Descripcion=Case When ltrim(rtrim(Cast(@Descripcion as varchar(max))))='' Then Null Else @Descripcion End 
		   ,NoHotel=Case When ltrim(rtrim(@NoHotel))='' Then Null Else @NoHotel End 
		   ,TxWebHotel=Case When ltrim(rtrim(@TxWebHotel))='' Then Null Else @TxWebHotel End 

			,TxDireccHotel=Case When ltrim(rtrim(@TxDireccHotel))='' Then Null Else @TxDireccHotel End 
			,TxTelfHotel1=Case When ltrim(rtrim(@TxTelfHotel1))='' Then Null Else @TxTelfHotel1 End 
			,TxTelfHotel2=Case When ltrim(rtrim(@TxTelfHotel2))='' Then Null Else @TxTelfHotel2 End 
			,FeHoraChkInHotel=Case When @FeHoraChkInHotel='01/01/1900' Then Null Else @FeHoraChkInHotel End 
			,FeHoraChkOutHotel=Case When @FeHoraChkOutHotel='01/01/1900' Then Null Else @FeHoraChkOutHotel End 
			,CoTipoDesaHotel=Case When ltrim(rtrim(@CoTipoDesaHotel))='' Then Null Else @CoTipoDesaHotel End 
			,CoCiudadHotel=Case When ltrim(rtrim(@CoCiudadHotel))='' Then Null Else @CoCiudadHotel End 

           ,UserMod=@UserMod
           ,FechaMod=getdate()           
     Where
           IDServicio=@IDServicio And
           Dia=@Dia And
           IDIdioma=@IDIdioma
           --(IDIdioma=@IDIdioma Or ltrim(rtrim(@IDIdioma))='')


