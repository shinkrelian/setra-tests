﻿--JRF-20120906-Nuevo Campo FechaDetalle.  
--HLF-20120911-FechaDetalle=GETDATE()
CREATE Procedure PRESUPUESTOS_DET_Ins    
	@IDPresupuesto int,    
	@IDServicio_Det int,    
	@Servicio varchar(100),    
	@MontoSoles numeric(8,2),    
	@MontoDolares numeric(8,2),    
	@LiquidaSoles numeric(8,2),    
	@LiquidaDolares numeric(8,2),    
	@SaldoSoles numeric(8,2),    
	@SaldoDolares numeric(8,2),    
	@IDTipoDoc char(3),    
	@NroSerieDocumento varchar(4),    
	@NroDocumento varchar(12),    	
	@UserMod char(4),    
	@pIDPresupuesto_Det int  OutPut 
As    
	Set NoCount On    
	Declare @IDPresupuesto_Det int    
	Execute dbo.Correlativo_SelOutput 'PRESUPUESTOS_DET',1,10,@IDPresupuesto_Det Output    
     
	INSERT INTO PRESUPUESTOS_DET
           ([IDPresupuesto_Det]    
           ,[IDPresupuesto]    
           ,[IDServicio_Det]    
           ,[Servicio]    
           ,[MontoSoles]    
           ,[MontoDolares]    
           ,[LiquidaSoles]    
           ,[LiquidaDolares]    
           ,[SaldoSoles]    
           ,[SaldoDolares]    
           ,[IDTipoDoc]    
           ,[NroSerieDocumento]    
           ,[NroDocumento]   
           ,[FechaDetalle]  
           ,[UserMod])    
     VALUES    
           (@IDPresupuesto_Det,    
           @IDPresupuesto,     
           Case when @IDServicio_Det = 0 then Null Else @IDServicio_Det End,    
           @Servicio,     
           Case When @MontoSoles = 0 then Null Else @MontoSoles End,    
           Case When @MontoDolares = 0 then Null Else @MontoDolares End,     
           Case When @LiquidaSoles = 0 then Null Else @LiquidaSoles End,      
           Case When @LiquidaDolares = 0 Then Null Else @LiquidaDolares End,     
           Case When @SaldoSoles = 0 Then Null Else @SaldoSoles End,     
           Case When @SaldoDolares = 0 Then Null Else @SaldoDolares End,     
           Case When ltrim(rtrim(@IDTipoDoc)) = '' Then Null Else @IDTipoDoc End,     
           Case When ltrim(rtrim(@NroSerieDocumento))= '' Then Null Else @NroSerieDocumento End,     
           Case When ltrim(rtrim(@NroDocumento)) = '' Then Null Else @NroDocumento End,    
           convert(smalldatetime,convert(varchar,GETDATE(),103)),  
           @UserMod)    
     Set @pIDPresupuesto_Det = @IDPresupuesto_Det    



