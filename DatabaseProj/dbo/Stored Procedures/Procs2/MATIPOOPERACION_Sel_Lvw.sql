﻿CREATE Procedure [dbo].[MATIPOOPERACION_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(100)
As
	Set NoCount On
	
	Select Descripcion	
	From MATIPOOPERACION
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IdToc <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
