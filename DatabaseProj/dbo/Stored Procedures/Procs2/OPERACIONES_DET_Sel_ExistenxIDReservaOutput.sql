﻿
				
CREATE Procedure dbo.OPERACIONES_DET_Sel_ExistenxIDReservaOutput  
 @IDReserva int,          
 @pbOk bit Output          
As          
 Set Nocount On          
  
 Set @pbOk = 0          
 If Exists(--Select IDOperacion_Det From OPERACIONES_DET_DETSERVICIOS       
 --Where IDOperacion_Det In 
 (Select IDOperacion_Det From OPERACIONES_DET       
 Where IDReserva_Det In (Select IDReserva_Det From RESERVAS_DET 
 Where IDReserva = @IDReserva And Anulado=0))    
 --And Not IDEstadoSolicitud In ('NV','PA')      
 )       
     
 Set @pbOk = 1        
   
