﻿Create Procedure dbo.OPERACIONES_DET_LOGPRESUPUESTOS_UpdAnulado
	@IDOperacion_Det int,
	@IDServicio_Det int,
	@FlAnulado bit,
	@CoAccion char(1),
	@UserMod char(4)
As
	Set Nocount on

	Update OPERACIONES_DET_LOGPRESUPUESTOS Set FlAnulado = @FlAnulado, FecMod=GETDATE(),
		UserMod=@UserMod 
	Where IDOperacion_Det=@IDOperacion_Det And CoAccion=@CoAccion And IDServicio_Det=@IDServicio_Det

