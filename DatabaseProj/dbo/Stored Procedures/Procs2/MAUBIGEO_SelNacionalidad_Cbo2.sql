﻿CREATE Procedure [dbo].[MAUBIGEO_SelNacionalidad_Cbo2]   
@bTodos bit  
As  
 Set NoCount On  
 select * from  
 (   
 Select '' as IDubigeo,'<TODOS>' as Gentilicio, 0 as Ord  
 Union  
 Select '' as IDubigeo,'' as Gentilicio, 1 as Ord  where @bTodos = 0
 Union
 Select IDubigeo, Descripcion,2 From MAUBIGEO  
  Where TipoUbig='PAIS'   
 ) AS X  
 Where (@bTodos=1 Or Ord<>0)  
 Order by Ord,2  
