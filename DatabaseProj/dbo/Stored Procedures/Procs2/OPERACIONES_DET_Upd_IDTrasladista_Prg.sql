﻿
CREATE PROCEDURE dbo.OPERACIONES_DET_Upd_IDTrasladista_Prg 
	@IDOperacion_Det	int,
	@IDTrasladista_Prg char(4),
	@UserMod char(4)
As
	Set Nocount On  
	
	Update OPERACIONES_DET 
	Set IDTrasladista_Prg=@IDTrasladista_Prg,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where IDOperacion_Det=@IDOperacion_Det
	
