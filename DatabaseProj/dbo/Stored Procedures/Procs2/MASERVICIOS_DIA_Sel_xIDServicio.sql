﻿
--HLF-20151012-isnull(NoHotel,'') as NoHotel, isnull(TxWebHotel,'') as TxWebHotel
--HLF-20151013-ISNULL(Descripcion,'') AS Descripcion
--HLF-20160122-TxDireccHotel,TxTelfHotel1,TxTelfHotel2,FeHoraChkInHotel,FeHoraChkOutHotel,CoTipoDesaHotel,CoCiudadHotel
CREATE Procedure [dbo].[MASERVICIOS_DIA_Sel_xIDServicio]
	@IDServicio	char(8)
As
	Set NoCount On

	Select Dia,IDIdioma,DescripCorta,
		--Descripcion,
		ISNULL(Descripcion,'') AS Descripcion,
		isnull(NoHotel,'') as NoHotel, 
		isnull(TxWebHotel,'') as TxWebHotel,
		isnull(TxDireccHotel,'') as TxDireccHotel,
		isnull(TxTelfHotel1,'') as TxTelfHotel1,
		isnull(TxTelfHotel2,'') as TxTelfHotel2,
		CONVERT(char(5),FeHoraChkInHotel,108) as FeHoraChkInHotel,
		CONVERT(char(5),FeHoraChkOutHotel,108) as FeHoraChkOutHotel,
		isnull(CoTipoDesaHotel,'') as CoTipoDesaHotel,
		isnull(CoCiudadHotel,'') as CoCiudadHotel
	From MASERVICIOS_DIA 
	Where IDServicio=@IDServicio
	Order by Dia

