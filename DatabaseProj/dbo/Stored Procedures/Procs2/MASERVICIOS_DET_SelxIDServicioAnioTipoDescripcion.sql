﻿--HLF-20140718-Agregando campo TipoCambioMoneda  
--JRF-20150331-Agregando el CoPago
--JRF-20150603-Cambiar la condición sel PCM >> STK
CREATE Procedure [dbo].[MASERVICIOS_DET_SelxIDServicioAnioTipoDescripcion]  
 @IDServicio char(8),  
 @Anio char(4),  
 @Tipo varchar(7),  
 @Descripcion varchar(200)  
As  
   
 Set NoCount On  
   
 Select p.IDTipoProv,sd.PlanAlimenticio,s.IDubigeo,  
 isnull(ub.IDPais,'') as IDPais,s.IDProveedor,  
 p.NombreCorto, sd.IDServicio, sd.IDServicio_Det, sd.Anio, sd.Tipo,  
 s.IDubigeo as IDCiudad,  
 ub.Descripcion as DescCiudad,  
 sd.Descripcion as DescServicioDet, s.IDTipoServ, sd.Desayuno, sd.Almuerzo,  
 sd.Lonche, sd.Cena, s.Transfer, isnull(s.TipoTransporte,'') as TipoTransporte,
 s.CoPago,Case isnull(s.CoPago,'EFE')
		 When 'EFE' Then 'EFECTIVO'                  
		 When 'TCK' Then 'TICKET' 
		 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio_Det=sd.IDServicio_Det),0) As varchar(4))+')'                 
		 When 'PCM' Then 'PRECOMPRA'-- ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio_Det=sd.IDServicio_Det),0) As varchar(4))+')'
		 End As DescPago
 From MASERVICIOS_DET sd   
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor  
 Left Join MAUBIGEO ub On s.IDubigeo=ub.IDubigeo  
 Where sd.IDServicio=@IDServicio And sd.Anio=@Anio And sd.Tipo=@Tipo   
 And sd.Descripcion=@Descripcion  
