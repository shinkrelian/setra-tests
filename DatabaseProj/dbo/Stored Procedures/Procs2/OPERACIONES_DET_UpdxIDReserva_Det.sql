﻿Create Procedure dbo.OPERACIONES_DET_UpdxIDReserva_Det
	@IDReserva_Det int,       
	@UserMod char(4)      
As      
	Set Nocount On      

	UPDATE OPERACIONES_DET  SET
	Dia=Ori.Dia ,DiaNombre=Ori.DiaNombre,FechaOut=Ori.FechaOut,Noches=Ori.Noches,IDubigeo=Ori.IDubigeo,
	IDServicio=Ori.IDServicio,IDServicio_Det=Ori.IDServicio_Det,
	Servicio=Ori.Servicio,
	IDIdioma=Ori.IDIdioma,
	Especial=Ori.Especial,
	MotivoEspecial=Ori.MotivoEspecial,
	RutaDocSustento=Ori.RutaDocSustento,
	Desayuno=Ori.Desayuno,Lonche=Ori.Lonche,Almuerzo=Ori.Almuerzo,Cena=Ori.Cena,Transfer=Ori.Transfer,      
	IDDetTransferOri=Ori.IDDetTransferOri,IDDetTransferDes=Ori.IDDetTransferDes,TipoTransporte=Ori.TipoTransporte,
	IDUbigeoOri=Ori.IDUbigeoOri,IDUbigeoDes=Ori.IDUbigeoDes,NroPax=Ori.NroPax,NroLiberados=Ori.NroLiberados,
	Tipo_Lib=Ori.Tipo_Lib,Cantidad=Ori.Cantidad,CantidadAPagar=Ori.CantidadAPagar,CostoReal=Ori.CostoReal,
	CostoRealAnt=Ori.CostoRealAnt,CostoLiberado=Ori.CostoLiberado,Margen=Ori.Margen,MargenAplicado=Ori.MargenAplicado,
	MargenAplicadoAnt=Ori.MargenAplicadoAnt,MargenLiberado=Ori.MargenLiberado,Total=Ori.Total,TotalOrig=Ori.TotalOrig,      
	CostoRealImpto=Ori.CostoRealImpto,CostoLiberadoImpto=Ori.CostoLiberadoImpto,MargenImpto=Ori.MargenImpto,
	MargenLiberadoImpto=Ori.MargenLiberadoImpto,TotImpto=Ori.TotImpto,UserMod=@UserMod,
	NetoHab=Ori.NetoHab,IgvHab=Ori.IgvHab,TotalHab=Ori.TotalHab,NetoGen=Ori.NetoGen,IgvGen=Ori.IgvGen,TotalGen=Ori.TotalGen       
	FROM RESERVAS_DET Ori Inner Join OPERACIONES_DET as Dest On Dest.IDReserva_Det=Ori.IDReserva_Det
	And Ori.IDReserva_Det = @IDReserva_Det
	
