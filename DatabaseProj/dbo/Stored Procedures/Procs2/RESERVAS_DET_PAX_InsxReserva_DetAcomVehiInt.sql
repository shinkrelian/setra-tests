﻿--HLF-20150709-And FlGuia=0
--HLF-20150710-Cambio a cursor
--HLF-20150713-If @NuPax<>0
CREATE Procedure [dbo].[RESERVAS_DET_PAX_InsxReserva_DetAcomVehiInt]
	--@IDReserva_Det	int,
	--@IDReserva_DetNew	int,
	@IDReserva int,
	@UserMod	char(4)
As
	Set Nocount On
	
	--Insert Into RESERVAS_DET_PAX
	--(IDReserva_Det, IDPax, UserMod)	
	--SELECT @IDReserva_DetNew, NuPax, @UserMod FROM ACOMODO_VEHICULO_DET_PAX WHERE IDDET IN 
	--(SELECT av.IDDet		
	--	FROM ACOMODO_VEHICULO av inner join RESERVAS_DET rd on av.IDDet=rd.IDDet
	--		And rd.IDReserva_Det in (@IDReserva_Det)
	--	and rd.IDReserva_DetOrigAcomVehi is null)
	--And NuVehiculo In (Select NuVehiculo From RESERVAS_DET Where IDReserva_Det=@IDReserva_DetNew)
	--	And FlGuia=0
	Declare @NuVehiculo smallint, @QtPax smallint, @IDDet int, @IDReserva_Det int
	Declare @CoProveedor as char(6)='', @IDCab int=0
	Select @CoProveedor=IDProveedor, @IDCab=IDCab From RESERVAS where IDReserva=@IDReserva

	Declare curResDet cursor For
	Select NuVehiculo,NroPax+ISNULL(nroliberados,0) as QtPax, IDDet, IDReserva_Det
	From RESERVAS_DET Where IDReserva=@IDReserva And Anulado=0 
	And IDDet in (Select av.IDDet From ACOMODO_VEHICULO_DET_PAX av 
				  Inner Join COTIDET cd On cd.iddet=av.iddet And cd.IDProveedor=@CoProveedor
					And cd.IDCAB=@IDCab)
	Order by NuVehiculo

	Open curResDet
	Fetch Next From curResDet Into @NuVehiculo, @QtPax, @IDDet, @IDReserva_Det
	While @@FETCH_STATUS=0
		Begin
		Declare @iQtPax smallint=1
		While @iQtPax<=@QtPax
			Begin
			Declare @NuPax int=0, @NuNroVehiculo tinyint=0
			SELECT Top 1 @NuPax=NuPax, @NuNroVehiculo=NuNroVehiculo
				FROM ACOMODO_VEHICULO_DET_PAX WHERE IDDet=@IDDet AND FlGuia=0 
				and NuVehiculo=@NuVehiculo
				And IDReserva_Det is Null

			If @NuPax<>0
				Begin
				Insert Into RESERVAS_DET_PAX(IDReserva_Det, IDPax, UserMod)	
				Values (@IDReserva_Det, @NuPax, @UserMod)

				Update ACOMODO_VEHICULO_DET_PAX Set IDReserva_Det=@IDReserva_Det, UserMod=@UserMod, FecMod=GETDATE()
				Where NuVehiculo=@NuVehiculo AND NuNroVehiculo=@NuNroVehiculo And IDDet=@IDDet And (NuPax=@NuPax OR FlGuia=1)
				End
			Set @iQtPax+=1
			End


		Fetch Next From curResDet Into @NuVehiculo, @QtPax, @IDDet, @IDReserva_Det
		End
	Close curResDet
	Deallocate curResDet
	

