﻿Create Procedure dbo.MASERVICIOS_DET_SelCoProveedorxIDServicio_DetOutput
	@IDServicio_Det int,
	@pCoProveedor char(6) output
As
	Set Nocount on
		
	Set @pCoProveedor=''
	Select @pCoProveedor=s.IDProveedor From MASERVICIOS_DET sd inner join MASERVICIOS s
		on sd.IDServicio=s.IDServicio	
	 Where IDServicio_Det=@IDServicio_Det

