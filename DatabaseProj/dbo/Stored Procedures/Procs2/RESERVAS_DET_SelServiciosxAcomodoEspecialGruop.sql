﻿Create Procedure dbo.RESERVAS_DET_SelServiciosxAcomodoEspecialGruop 
@IDCab int
As  
Set NoCount On  
select rd.IDReserva_Det,rd.IDDet, CAST(rd.Cantidad as varchar(5))+' '+ rd.Servicio As TituloAcomodoxCapacidad  , convert(Char(10),rd.Dia,103) as FechaIn,convert(Char(10),rd.FechaOut,103) as FechaOut,   
    rd.Cantidad,  
    case when (select COUNT(iddet) from cotidet cd  
   where AcomodoEspecial = 1 and cd.IncGuia = 0 and IDCab = r.IDCab and r.IDProveedor = cd.IDProveedor  
   and cd.Dia between rd.Dia and rd.FechaOut)>0   
   then 1 Else 0 End as ExisteAcomodoEspecial,rd.CapacidadHab,rd.EsMatrimonial,  
   CAST(rd.Cantidad as varchar(5))+' ' + dbo.FnMailAcomodosReservas(rd.CapacidadHab,rd.EsMatrimonial) as TituloAcomodo,  
   dbo.FnAcomodosReservasxIDReserva(r.IDReserva,rd.Dia) as Acomodo,rd.Servicio  
from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva  
 Left Join MASERVICIOS_DET sd On sd.IDServicio_Det = rd.IDServicio_Det   
 Left Join COTIDET cd On rd.IDDet = cd.IDDET   
where r.IDCab = @IDCab and r.Anulado = 0  and rd.Cantidad <> 0  
  and sd.PlanAlimenticio = 0 and rd.Anulado = 0 and cd.IncGuia = 0 --and rd.FechaOut is not null  
Order by rd.Dia  
