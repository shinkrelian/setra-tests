﻿
--HLF-20150630- @FlServicioNoShow
CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_Ins
@NuOrden_Servicio int,
@IDOperacion_Det int,
@IDServicio_Det int,
@FlServicioNoShow bit,
@UserMod char(4),
@pNuOrden_Servicio_Det int output
As
Set NoCount On

Declare @NuOrden_Servicio_Det int = (select Isnull(Max(NuOrden_Servicio_Det),0)+1 from ORDEN_SERVICIO_DET)

INSERT INTO [dbo].[ORDEN_SERVICIO_DET]
           ([NuOrden_Servicio_Det]
           ,[NuOrden_Servicio]
           ,[IDOperacion_Det]
           ,[IDServicio_Det]
		   ,FlServicioNoShow
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@NuOrden_Servicio_Det
           ,@NuOrden_Servicio
           ,@IDOperacion_Det
           ,@IDServicio_Det
		   ,@FlServicioNoShow
           ,@UserMod
           ,GETDATE())
Set @pNuOrden_Servicio_Det = @NuOrden_Servicio_Det

