﻿--JRF-20160308-Eliminar si no existe un ingreso manual
--JRF-20160309-La Eliminación de la cabezera sera depués de crear los OPERACIONES_DET_DETSERVICIOS
--HLF-20160315-Delete from PRESUPUESTO_SOBRE_DET Where IDOperacion_Det In  (Select IDOperacion_Det FROM OPERACIONES_DET WHERE IDReserva_....
CREATE Procedure dbo.PRESUPUESTO_SOBRE_DET_DelxIDReserva_Det
 @IDReserva_Det int    
As    
 Set Nocount On
 --Declare @IDOperacion_Det int = (Select IDOperacion_Det FROM OPERACIONES_DET WHERE IDReserva_Det = @IDReserva_Det)
	
 --Declare @NuPreSob int = 0
 --select top(1) @NuPreSob = NuPreSob from PRESUPUESTO_SOBRE_DET where IDOperacion_Det=@IDOperacion_Det 
 --Delete from PRESUPUESTO_SOBRE_DET Where IDOperacion_Det = @IDOperacion_Det And NuPreSob=@NuPreSob
  

Delete from PRESUPUESTO_SOBRE_DET Where IDOperacion_Det In  (Select IDOperacion_Det FROM OPERACIONES_DET WHERE IDReserva_Det = @IDReserva_Det)