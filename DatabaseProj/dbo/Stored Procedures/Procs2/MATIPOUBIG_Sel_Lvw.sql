﻿CREATE Procedure [dbo].[MATIPOUBIG_Sel_Lvw]
	@ID	varchar(20),
	@Descripcion varchar(20)
As
	Set NoCount On
	
	Select Descripcion	
	From MATIPOUBIG
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (Descripcion <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
