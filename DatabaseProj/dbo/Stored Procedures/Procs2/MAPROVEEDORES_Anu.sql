﻿CREATE Procedure [dbo].[MAPROVEEDORES_Anu]
	@IDProveedor char(6),
	@UserMod char(4)	
As
	Set NoCount On
	
	UPDATE MAPROVEEDORES SET Activo='I', UserMod=@UserMod, FecMod=GETDATE()
    WHERE IDProveedor = @IDProveedor
