﻿CREATE Procedure [dbo].[MASERVICIOS_DET_COSTOS_Sel_xIDServicio_Det]	
    @IDServicio_Det int
As
	Set NoCount On
	
	Select Correlativo, PaxDesde, PaxHasta, Monto 
	From MASERVICIOS_DET_COSTOS
	Where IDServicio_Det=@IDServicio_Det
	Order by PaxDesde
