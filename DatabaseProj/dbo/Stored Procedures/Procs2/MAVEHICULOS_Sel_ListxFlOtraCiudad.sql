﻿--JRF-20130731-Admitir valor vacío.
CREATE Procedure dbo.MAVEHICULOS_Sel_ListxFlOtraCiudad  
@FlOtraCiudad bit  
As  
select * from (
select 0 as NuVehiculo,'' AS Descripcion,0 as QtDe
Union
Select NuVehiculo,NoVehiculoCitado+' ('+cast(QtDe as varchar(2)) +' - '+cast(QtHasta as varchar(2))  +') Pax - Cap. '+ Cast(QtCapacidad as Varchar(2)) As Descripcion ,QtDe  
from MAVEHICULOS  
where FlOtraCiudad = @FlOtraCiudad  )as X
Order By X.QtDe  
