﻿--HLF-20131114-Agregando Join con Reservas para campo Anulado        
--HLF-20140128-Agregando And r.Estado<>'XL'      
--JRF-20140910-[Ajustando funcion...]Agregando la Funcion dbo.FnCambiosenReservasPostFileOperaciones(..)    
CREATE Procedure dbo.OPERACIONES_SelCambioReservaOutPut          
@IDCab int,          
@IDProveedor char(6),          
@pCambiosenReservasPostFile bit output          
As          
Begin          
      Set NoCount On          
      Set @pCambiosenReservasPostFile =(select dbo.FnCambiosenReservasPostFileOperaciones(op.IDOperacion) --op.CambiosenReservasPostFile     
      from OPERACIONES op Inner Join RESERVAS r On op.IDReserva=r.IDReserva And r.Anulado=0        
     And r.Estado<>'XL'      
                  where op.IDCab = @IDCab and op.IDProveedor = @IDProveedor)          
      Set @pCambiosenReservasPostFile = ISNULL(@pCambiosenReservasPostFile,0)          
End      
