﻿CREATE Procedure [dbo].[MASERVICIOS_Sel_Lvw]  
 @ID char(8),  
 @Descripcion varchar(100)  
As  
 Set NoCount On  
   
 Select Descripcion   
 From MASERVICIOS  
 Where (Descripcion Like '%'+@Descripcion+'%')  
 And (IDServicio <>@ID Or ltrim(rtrim(@ID))='') and IDCabVarios Is Null
 Order by Descripcion  
