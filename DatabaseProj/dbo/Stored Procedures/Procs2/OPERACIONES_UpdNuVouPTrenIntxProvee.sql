﻿
Create Procedure dbo.OPERACIONES_UpdNuVouPTrenIntxProvee
	@CoProveedor char(6),
	@IDCab int,
	@NuVouPTrenInt int,
	@UserMod char(4)

As

	Set NoCount On	

	UPDATE OPERACIONES SET NuVouPTrenInt=@NuVouPTrenInt, UserMod=@UserMod, FecMod=GETDATE()
	WHERE IDOperacion In
	(Select o.IDOperacion From OPERACIONES o Inner Join RESERVAS r On r.IDReserva=o.IDReserva And r.Anulado=0
	 Where  o.IDProveedor=@CoProveedor And o.IDCab=@IDCab)

	
