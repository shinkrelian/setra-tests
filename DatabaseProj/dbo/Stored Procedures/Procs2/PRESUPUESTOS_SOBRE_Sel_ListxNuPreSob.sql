﻿

--JRF-20150409-Agregar nuevo campo IDPax
--JRF-20150424-Union con los servicios manuales
--JRF-20150603-(.. When 'STK' The...)
--JRF-20150608-Agregar el NuPreSob
--JRF-20150917-..IDServicio_Det_V
--HLF-20151027-Columna ps.CoEstado
--HLF-20151109-..(Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sdd.IDServicio..)
--HLF-20151221-Else us.Nombre+' '+ISNULL(us.TxApellidos,'') ...,  Case When len(odd.CoPrvPrg)=6 then..
--HLF-20151228-TotalPreCompra
--HLF-20160204-isnull(PD.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot
CREATE Procedure dbo.PRESUPUESTOS_SOBRE_Sel_ListxNuPreSob
@NuPreSob int
As
Set NoCount On
Select X.IDServicio_Det,x.IDServicio_Det_V,X.IDOperacion_Det,X.NuDetPSo,X.NuSincerado_Det,X.Dia,X.Hora,X.DescProveeGuia,X.IDProveedorGuia,X.IDPax,X.DescServicio,X.Pax,
	   X.IDMoneda,X.SimMoneda,X.PreUnit,X.Total,X.Total as TotalPreCompra,X.Sustentado,X.Devuelto,X.RegistradoPresup,X.RegistradoSincerado,X.CoTipPSo,
	 (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=X.CoTipPSo) as DescCoTipPSo, X.CoPago,X.DescPago,'R' as RegVal,X.NuPreSob, x.CoEstado
from (
Select odd.IDServicio_Det as IDServicio_Det,
--IsNull(sd.IDServicio_Det_V,0) as IDServicio_Det_V,
isnull(odd.IDServicio_Det_V_Cot,0) as IDServicio_Det_V,
odd.IDOperacion_Det,
	   odd.NuDetPSo as NuDetPSo,0 as NuSincerado_Det,od.Dia,Convert(char(5),od.Dia,108) as Hora,
	   IsNull(odd.IDPax,0) as IDPax,
	   --IsNull((select NombreCorto from MAPROVEEDORES pGuia where pGuia.IDProveedor=odd.CoPrvPrg),
			 -- cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescProveeGuia,
	 Case When len(odd.CoPrvPrg)=6 then
		IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) 
	 Else
 		us.Nombre+' '+ISNULL(us.TxApellidos,'')
	 End as DescProveeGuia,
	   IsNull(odd.CoPrvPrg,'') as IDProveedorGuia,odd.TxServicio as DescServicio,odd.QtPax as Pax,
	   odd.IDMoneda As IDMoneda,mo.Simbolo as SimMoneda, 
	   IsNull(odd.SsPreUni,0) as PreUnit,
	   IsNull(odd.SsTotal,0) as Total,
	   odd.SsTotSus as Sustentado,odd.SsTotDev as Devuelto,1 as RegistradoPresup,0 As RegistradoSincerado,
	   odd.CoTipPSo As CoTipPSo,odd.CoPago,
	    Case  isnull(odd.CoPago,'EFE')
		 When 'EFE' Then 'EFECTIVO'                  
		 When 'TCK' Then 'TICKET'                  
		 When 'PCM' Then 'PRECOMPRA' -- ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio_Det=odd.IDServicio_Det),0) As varchar(4))+')'
		 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio=sd.IDServicio),0) As varchar(4))+')'
		 End As DescPago,'R' as RegVAL,odd.NuPreSob,ps.CoEstado
from PRESUPUESTO_SOBRE_DET odd Inner Join OPERACIONES_DET od On od.IDOperacion_Det=odd.IDOperacion_Det
Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion
Inner Join MASERVICIOS_DET sd On IsNull(odd.IDServicio_Det,0)=sd.IDServicio_Det
Left Join MAMONEDAS mo On odd.IDMoneda=mo.IDMoneda
Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
Left Join MAPROVEEDORES p4 On s.IDProveedor=p4.IDProveedor
Left Join COTIPAX cp On odd.IDPax=cp.IDPax
Inner Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=odd.NuPreSob
Left Join MAPROVEEDORES p On odd.CoPrvPrg=p.IDProveedor
 Left Join MAUSUARIOS us On odd.CoPrvPrg=us.IDUsuario
Where odd.NuPreSob=@NuPreSob) as X
 Union
 Select IsNull(pd.IDServicio_Det,0) as IDServicio_Det,IsNull(sd.IDServicio_Det_V,0) as IDServicio_Det_V,IsNull(PD.IDOperacion_Det,0) as IDOperacion_Det,
 pd.NuDetPSo,0 as NuSincerado_Det,pd.FeDetPSo as Dia,Convert(char(5),pd.FeDetPSo,108) as Hora,
 Case When len(pd.CoPrvPrg)=6 then
	IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) 
 Else
 	us.Nombre+' '+ISNULL(us.TxApellidos,'')
 End as DescProveeGuia,
 IsNull(pd.CoPrvPrg,'') as CoPrvPrg,IsNull(pd.IDPax,0) as IDPax, pd.TxServicio,
 pd.QtPax,IsNull(pd.IDMoneda,'') as IDMoneda,IsNull(mo.Simbolo,'') as SimMoneda, pd.SsPreUni, pd.SsTotal as Total, pd.SsTotal as TotalPreCompra, 
 pd.SsTotSus as Sustentado, pd.SsTotDev as Devuelto, 1 as RegistradoPresup,0 as RegistradoSincer,pd.CoTipPSo,es.NoTipPSo as DescCotipPSo,
 pd.CoPago,
 Case pd.CoPago           
 When 'EFE' Then 'EFECTIVO'                
 When 'TCK' Then 'TICKET'                
 When 'PCM' Then 'PRECOMPRA' --('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio_Det=pd.IDServicio_Det),0) As varchar(4))+')'
 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio=sd.IDServicio),0) As varchar(4))+')'
 --When '' Then 'EFECTIVO'                
 End As DescPago,'R' as RegVal,PD.NuPreSob,ps.CoEstado
 from PRESUPUESTO_SOBRE_DET pd Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor
 Left Join MAUSUARIOS us On pd.CoPrvPrg=us.IDUsuario
 Left Join COTIPAX cp On pd.IDPax=cp.IDPax
 Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
 Left Join MAMONEDAS mo On pd.IDMoneda=mo.IDMoneda
 Left jOIN MASERVICIOS_DET sd On pd.IDServicio_Det=sd.IDServicio_Det
 Inner Join PRESUPUESTO_SOBRE ps On ps.NuPreSob=pd.NuPreSob
 Where pd.NuPreSob=@NuPreSob and pd.IDOperacion_Det is Null



