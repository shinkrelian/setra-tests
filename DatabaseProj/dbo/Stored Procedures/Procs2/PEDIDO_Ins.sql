﻿Create Procedure dbo.PEDIDO_Ins	
	@CoCliente char(6) ,
	@FePedido smalldatetime ,
	@CoEjeVta char(4),
	@TxTitulo varchar(100),
	@CoEstado char(2),
	@UserMod char(4),
	@pNuPedido varchar(20) output
As
	Set Nocount on
	Declare @NuPedInt int
	Execute dbo.Correlativo_SelOutput 'PEDIDO',1,10,@NuPedInt output        
	Declare @NuPedido char(9)
	Exec NuPedido_SelOutput @FePedido,@NuPedido output

	Insert Into PEDIDO 
	(NuPedInt,
	NuPedido,
	CoCliente,
	FePedido,
	CoEjeVta,
	TxTitulo,
	CoEstado,
	UserMod)
	Values 
	(@NuPedInt,
	@NuPedido,
	@CoCliente,
	@FePedido,
	@CoEjeVta,
	@TxTitulo,
	@CoEstado,
	@UserMod)

	Set @pNuPedido=cast(@NuPedInt as nvarchar(10))+'|'+@NuPedido
