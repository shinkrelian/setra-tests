﻿--JHD-20160122- Declare @NuOrdComInt_Det int=isnull((Select MAX(NuOrdComInt_Det) From ORDENCOMPRA_DET),0)+1    
CREATE PROCEDURE [dbo].[ORDENCOMPRA_DET_Ins]
	--@NuOrdComInt_Det int,
	@NuOrdComInt int,
	@TxServicio varchar(max),
	@SSCantidad numeric(8,2),
	@SSPrecUnit numeric(8,2),
	@SSTotal numeric(8,2),
	@UserMod char(4)
AS
BEGIN
 --Declare @NuOrdComInt_Det tinyint=isnull((Select MAX(NuOrdComInt_Det) From ORDENCOMPRA_DET),0)+1          
 Declare @NuOrdComInt_Det int=isnull((Select MAX(NuOrdComInt_Det) From ORDENCOMPRA_DET),0)+1          
 --Where NuDocum=@NuDocum And IDTipoDoc=@IDTipoDoc),0)+1      

	Insert Into [dbo].[ORDENCOMPRA_DET]
		(
			[NuOrdComInt_Det],
 			[NuOrdComInt],
 			[TxServicio],
 			[SSCantidad],
 			[SSPrecUnit],
 			[SSTotal],
 			[UserMod],
 			[FecMod]
 		)
	Values
		(
			@NuOrdComInt_Det,
 			@NuOrdComInt,
 			@TxServicio,
 			@SSCantidad,
 			@SSPrecUnit,
 			@SSTotal,
 			@UserMod
 			,GETDATE()
 		)
END;
