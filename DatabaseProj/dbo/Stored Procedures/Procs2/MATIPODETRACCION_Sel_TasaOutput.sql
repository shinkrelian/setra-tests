﻿
CREATE Procedure [dbo].[MATIPODETRACCION_Sel_TasaOutput]
	@CoTipoDetraccion char(5),
	@pSsTasa	numeric(5,4) Output		
As

	Set NoCount On
	Set @pSsTasa = 0

	Select @pSsTasa = SsTasa From MATIPODETRACCION Where CoTipoDetraccion=@CoTipoDetraccion
