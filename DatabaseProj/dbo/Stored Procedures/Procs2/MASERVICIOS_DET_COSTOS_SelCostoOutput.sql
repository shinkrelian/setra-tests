﻿--JHD-20150602-Aumentando el valor de los decimales a (numeric(15, 4))
CREATE Procedure [dbo].[MASERVICIOS_DET_COSTOS_SelCostoOutput]
	@IDServicio_Det int,
	@NroPax	smallint,
	@pMonto	numeric(15,4) output
As
	Set Nocount On
	
	Select @pMonto=Monto From MASERVICIOS_DET_COSTOS 
	Where IDServicio_Det=@IDServicio_Det And
	@NroPax Between PaxDesde And PaxHasta
	
	Set @pMonto=ISNULL(@pMonto,0)
