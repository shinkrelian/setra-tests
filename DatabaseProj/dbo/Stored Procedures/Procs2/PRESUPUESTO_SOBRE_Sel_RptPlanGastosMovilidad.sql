﻿--HLF-20140815-and psd.CoPrvPrg=@CoPrvPrg  
--JRF-20150309-Considerar los activos
--JRF-20150410-Considerar los Tour Condustor
CREATE Procedure dbo.PRESUPUESTO_SOBRE_Sel_RptPlanGastosMovilidad      
@IDCab int,      
@IDProvGuia char(6),      
@IDProveedor char(6),
@IDPax int
As      
Set NoCount On      
Select ROW_NUMBER()Over(Order by psd.FeDetPSo) as Item,IsNull(p.NombreCorto,cp.Titulo+' '+cp.Apellidos+' '+cp.Nombres) as NombreGuia,      
    Case When p.IDIdentidad = '001' then p.NumIdentidad Else '' End as RUC,      
    c.Titulo as NombreDelFile,c.IDFile,      
    CONVERT(char(10),psd.FeDetPSo,103) as DescFecha,CONVERT(Char(5),psd.FeDetPSo,108) as DescHora,      
    psd.TxServicio,psd.SsTotal      
from PRESUPUESTO_SOBRE ps Left Join COTICAB c on ps.IDCab = c.IDCAB      
Left Join MAPROVEEDORES p On ps.CoPrvGui = p.IDProveedor      
Left Join PRESUPUESTO_SOBRE_DET psd On ps.NuPreSob = psd.NuPreSob      
Left Join OPERACIONES_DET_DETSERVICIOS odd On psd.IDOperacion_Det=odd.IDOperacion_Det 
and psd.IDServicio_Det=IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)
Left Join COTIPAX cp On ps.IDPax=cp.IDPax
where c.IDCAB = @IDCab   
--and ps.CoPrvGui=@IDProvGuia  
And IsNull(psd.CoPrvPrg,'')=Ltrim(Rtrim(@IDProvGuia)) And IsNULL(psd.IDPax,0)=@IDPax
and ps.IDProveedor = @IDProveedor and charindex('taxi',lower(psd.TxServicio))>0    and odd.Activo=1
