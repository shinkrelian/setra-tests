﻿--JRF-20140125-Agregar la validación del campo Activo
CREATE Procedure MAUSUARIOS_Sel_CboxArea  
@DescArea char(2),  
@bGerencial bit,  
@bTodos bit,  
@bDato bit  
As    
 Select * from(  
 select Case When @bDato = 1 Then '0000' Else '' End As IDUsuario,  
        Case When @bDato = 1 Then '----' Else '<TODOS>'End As Usuario  
        ,2 as Ord  
 Union  
 Select * from (  
  select IDUsuario,Usuario , 1 As Ger from MAUSUARIOS   
  Where IdArea = 'GR'  and Activo = 'A'
  Union   
  select IDUsuario,Usuario , 0 As Ger from MAUSUARIOS   
  where IdArea = @DescArea  and Activo = 'A'
  ) As X  
 Where (X.Ger = @bGerencial Or X.Ger = 0)) As Y   
 Where (@bTodos=1 oR Y.Ord<>2)  
 Order by Y.Ord desc,Y.Usuario  
