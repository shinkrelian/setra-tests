﻿
CREATE procedure dbo.MAUBIGEO_Sel_CboSelCounter    
As    
	Set NoCount On    
	
	Select CoCounter,Descripcion from MAUBIGEO     
	where TipoUbig = 'CIUDAD' And NOT CoCounter is null
	Order by Descripcion  
