﻿--JRF-20140319-Mostrar el Total Generado de RESERVAS_DET y agregar la columna La Moneda.      
--JRF-20140410-Agregar el filtro de Usuarios (Ventas y Reservas)    
--JRF-20140414-Agregar el filtro de Estados por Reserva  
--JHD-20141124-Ordenar por room night: Order by X.DescProveedor,X.DescServicioDet,X.RN desc,CAST(X.FechaDia AS SMALLDATETIME)     
--JHD-20141124-Se comento el filtro: Or (p.IDTipoProv = '003' and sd.ConAlojamiento=1))  
--JHD-20141124-Se modifico el filtro de plan alimenticio: and (@PlanAlimenticio = 1 Or (sd.PlanAlimenticio = @PlanAlimenticio and p.IDTipoProv<>'002'))         
--JRF-20150223-Left Join MAMONEDAS ms On rd.IDMoneda=ms.IDMoneda
--JHD-20150506-@CoPais char(6)=''
--JHD-20150506-And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=rd.IDubigeo)=@CoPais )
--JHD-20150525- Se agregaron parámetros: @VerHoteles bit=1, @VerRestaurantes bit=1
--JHD-20150525-where ((p.IDTipoProv = '001' and @VerHoteles=1) Or (p.IDTipoProv = '002' and @VerRestaurantes=1) )
--JHD-20150525-and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
			--+REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
			--+Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%water bottle%')
--JHD-20150525-and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
			--+REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
			--+Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%water of bottle%')
--JHD-20150525-and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
			--+REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
			--+Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%bottle of water%')
--JHD-20150525-and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
			--+REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
			--+Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%glass of wine%')
--JRF-20150611-...((p.IDTipoProv = '003' and sd.ConAlojamiento=1) And @VerHoteles=1)
--JHD-20150903-case when isnull(p.IDProveedorInternacional,'')='' then p.NombreCorto 
			--else (select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional) +': '+p.NombreCorto end as DescProveedor,
--JHD-20151028-Se agrego un nuevo Select para listar las operaciones sin reservas
--HLF-20151116-case when IDMoneda='USD' then ROUND(X.TotalGen,2)...
--case when IDMoneda='USD' then SiglaMonedaTMP...
--JRF-20160209-Numeric(8,2) --> CAST((rd.NetoHab*(rd.MargenAplicado/100)+rd.NetoHab) as Numeric(12,2)) as CM,
CREATE Procedure [dbo].[RESERVAS_DET_Sel_RptHotelesRestaurantes]          
 @IDProveedor varchar(6),          
 @IDCliente varchar(6),          
 @PlanAlimenticio bit ,          
 @IDCiudad varchar(6),          
 @Estado varchar(1),     
 @EstadoReserva char(2),  
 @IDUsuarioVentas char(4),    
 @IDUsuarioReservas char(4),         
 @FechaRango1 smalldatetime,          
 @FechaRango2 smalldatetime,
 @CoPais char(6),
 @VerHoteles bit,
 @VerRestaurantes bit
As       
begin   
Set NoCount On  
Select *,
case when IDMoneda='USD' then SiglaMonedaTMP
else
	SiglaMonedaTMP + ' ('+cast(TCambio as nvarchar(10))+')'
end as SiglaMoneda,
--ROUND(X.TotalGen,2) AS Total,
case when IDMoneda='USD' then ROUND(X.TotalGen,2)
else
	dbo.FnCambioMoneda(X.TotalGen,IDMoneda,'USD',TCambio)
end as Total
From (          
Select p.IDProveedor,
--p.NombreCorto as DescProveedor,
case when isnull(p.IDProveedorInternacional,'')='' then p.NombreCorto 
	else (select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional) +': '+p.NombreCorto end as DescProveedor,
Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
    +REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
    +Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End          
    as DescServicioDet,          
    Convert(Char(10),rd.Dia,103) as FechaDia,c.Cotizacion,c.IDFile,          
    cl.RazonComercial as DescCliente,c.Titulo,c.Estado,Isnull(uv.Siglas,'') as SiglaUser,          
    Isnull(cast(od.IDVoucher as varchar(8)),'') as IDVoucher,          
    cast(rd.CantidadAPagar as smallint) as CantidadPagar,Isnull(rd.Noches,1) as Noches,          
     cast(rd.CantidadAPagar as smallint) * Isnull(rd.Noches,1) as RN,          
     rd.NetoHab,rd.MargenAplicado as Margen ,          
    CAST((rd.NetoHab*(rd.MargenAplicado/100)+rd.NetoHab) as Numeric(12,2)) as CM,      
    rd.TotalGen,
	rd.IDMoneda,
	case when rd.IDMoneda<>'USD' then dbo.FnTipoDeCambioxMonedaFile(rd.IDMoneda,c.IDCAB) else 0 end as TCambio,
	ms.Simbolo as SiglaMonedaTMP		--Case when rd.IDMoneda = 'USD' Then 'US$' Else 'S/.' End as SiglaMoneda      

from COTICAB c Left Join RESERVAS r On c.IDCAB = r.IDCab Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor           
Inner Join RESERVAS_DET rd On r.IDReserva = rd.IDReserva and ISNULL(rd.Transfer,0) = 0 --and rd.Anulado = 0          
Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det         
Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente Left Join MAUSUARIOS uv On c.IDUsuario = uv.IDUsuario          
Left Join OPERACIONES_DET od On rd.IDReserva_Det = od.IDReserva_Det          
Left Join MASERVICIOS s On sd.IDServicio = s.IDServicio          
Left Join MAMONEDAS ms On rd.IDMoneda=ms.IDMoneda
--where (p.IDTipoProv = '001' Or p.IDTipoProv = '002' )--Or (p.IDTipoProv = '003' and sd.ConAlojamiento=1)) --and r.Estado <> 'XL'          
where ((p.IDTipoProv = '001' and @VerHoteles=1) Or (p.IDTipoProv = '002' and @VerRestaurantes=1) Or
	   ((p.IDTipoProv = '003' and sd.ConAlojamiento=1) And @VerHoteles=1))
and ((ltrim(rtrim(@IDProveedor))='' Or r.IDProveedor = @IDProveedor) 
	 Or (@IDProveedor='003440' And p.IDProveedor='001920'))
and (ltrim(rtrim(@IDCliente))='' Or c.IDCliente = @IDCliente)          
--and (@PlanAlimenticio = 1 Or sd.PlanAlimenticio = @PlanAlimenticio)          
and (@PlanAlimenticio = 1 Or (sd.PlanAlimenticio = @PlanAlimenticio and p.IDTipoProv<>'002'))
--	and (p.IDTipoProv<>'006'))
and (LTRIM(rtrim(@IDUsuarioVentas))='' Or c.IDUsuario = @IDUsuarioVentas)    
and (LTRIM(rtrim(@IDUsuarioReservas))='' Or c.IDUsuarioRes = @IDUsuarioReservas)    
and (ltrim(rtrim(@IDCiudad))='' or rd.IDubigeo = @IDCiudad)          
And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=rd.IDubigeo)=@CoPais )       
and (ltrim(rtrim(@Estado))='' or c.Estado = @Estado)          
and (ltrim(rtrim(@EstadoReserva))='' or r.Estado = @EstadoReserva) --r.Estado <> 'XL'   
And rd.Anulado = 0 and r.Anulado = 0        

--and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
--    +REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
--    +Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%water bottle%')

--and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
--    +REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
--    +Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%water of bottle%')

--and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
--    +REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
--    +Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%bottle of water%')

--and not ((Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else rd.Servicio End          
--    +REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End    
--    +Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End  ) like '%glass of wine%')
		
and (((LTRIM(rtrim(convert(char(10),@FechaRango1,103)))='01/01/1900') and (LTRIM(rtrim(convert(char(10),@FechaRango2,103)))='01/01/1900'))          
  Or cast(Convert(Char(10),rd.Dia,103) as smalldatetime) between @FechaRango1 and @FechaRango2)    
  
  
UNION 
--OPERACIONES SIN RESERVAS
Select p.IDProveedor,
--p.NombreCorto as DescProveedor,
case when isnull(p.IDProveedorInternacional,'')='' then p.NombreCorto 
	else (select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional) +': '+p.NombreCorto end as DescProveedor,
Case When p.IDTipoProv ='003' and sd.ConAlojamiento=1 then sd.Descripcion else od.Servicio End          
    +REPLICATE(' ',3)+ sd.anio --+Case when sd.Tipo is null then '' else REPLICATE(' ',3)+sd.Tipo End           
    +Case when sd.DetaTipo is null then '' else replicate(' ',3)+ cast(sd.DetaTipo as varchar(max))End          
    as DescServicioDet,          
    Convert(Char(10),od.Dia,103) as FechaDia,c.Cotizacion,c.IDFile,          
    cl.RazonComercial as DescCliente,c.Titulo,c.Estado,Isnull(uv.Siglas,'') as SiglaUser,          
    Isnull(cast(od.IDVoucher as varchar(8)),'') as IDVoucher,          
    cast(od.CantidadAPagar as smallint) as CantidadPagar,Isnull(od.Noches,1) as Noches,          
     cast(od.CantidadAPagar as smallint) * Isnull(od.Noches,1) as RN,          
     od.NetoHab,od.MargenAplicado as Margen ,          
    CAST((od.NetoHab*(od.MargenAplicado/100)+od.NetoHab) as Numeric(8,2)) as CM,      
    od.TotalGen,
	od.IDMoneda,
	case when od.IDMoneda<>'USD' then dbo.FnTipoDeCambioxMonedaFile(od.IDMoneda,c.IDCAB) else 0 end as TCambio,
	ms.Simbolo as SiglaMonedaTMP--Case when rd.IDMoneda = 'USD' Then 'US$' Else 'S/.' End as SiglaMoneda      
from COTICAB c 
Left Join OPERACIONES o On c.IDCAB = o.IDCab
Left Join OPERACIONES_DET od On o.IDOperacion = od.IDOperacion   
--Left Join RESERVAS r On c.IDCAB = r.IDCab
 Left Join MAPROVEEDORES p On o.IDProveedor = p.IDProveedor           
--Inner Join RESERVAS_DET rd On r.IDReserva = rd.IDReserva and ISNULL(rd.Transfer,0) = 0 --and rd.Anulado = 0          
Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det         
Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente Left Join MAUSUARIOS uv On c.IDUsuario = uv.IDUsuario          
--Left Join OPERACIONES_DET od On rd.IDReserva_Det = od.IDReserva_Det          
Left Join MASERVICIOS s On sd.IDServicio = s.IDServicio          
Left Join MAMONEDAS ms On od.IDMoneda=ms.IDMoneda
--where (p.IDTipoProv = '001' Or p.IDTipoProv = '002' )--Or (p.IDTipoProv = '003' and sd.ConAlojamiento=1)) --and r.Estado <> 'XL'          
where ((p.IDTipoProv = '001' and @VerHoteles=1) Or (p.IDTipoProv = '002' and @VerRestaurantes=1) Or
	   ((p.IDTipoProv = '003' and sd.ConAlojamiento=1) And @VerHoteles=1))
and ((ltrim(rtrim(@IDProveedor))='' Or o.IDProveedor = @IDProveedor) 
	Or (@IDProveedor='003440' And p.IDProveedor='001920')
	)
and (ltrim(rtrim(@IDCliente))='' Or c.IDCliente = @IDCliente)          
--and (@PlanAlimenticio = 1 Or sd.PlanAlimenticio = @PlanAlimenticio)          
and (@PlanAlimenticio = 1 Or (sd.PlanAlimenticio = @PlanAlimenticio and p.IDTipoProv<>'002'))
--	and (p.IDTipoProv<>'006'))
and (LTRIM(rtrim(@IDUsuarioVentas))='' Or c.IDUsuario = @IDUsuarioVentas)    
and (LTRIM(rtrim(@IDUsuarioReservas))='' Or c.IDUsuarioRes = @IDUsuarioReservas)    
and (ltrim(rtrim(@IDCiudad))='' or od.IDubigeo = @IDCiudad)          
And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=od.IDubigeo)=@CoPais )       
and (ltrim(rtrim(@Estado))='' or c.Estado = @Estado)          
and o.IDReserva is null and od.IDReserva_Det is null
      
and (((LTRIM(rtrim(convert(char(10),@FechaRango1,103)))='01/01/1900') and (LTRIM(rtrim(convert(char(10),@FechaRango2,103)))='01/01/1900'))          
Or cast(Convert(Char(10),OD.Dia,103) as smalldatetime) between @FechaRango1 and @FechaRango2)    
    
	    
) as X          
--Order by X.DescProveedor,X.DescServicioDet,CAST(X.FechaDia AS SMALLDATETIME)          
where Not X.DescServicioDet like '%water bottle%' And Not X.DescServicioDet like '%water of bottle%' And
	  Not X.DescServicioDet like '%bottle of water%' And Not X.DescServicioDet like '%glass of wine%'
Order by X.DescProveedor,X.DescServicioDet,X.RN desc,CAST(X.FechaDia AS SMALLDATETIME)          
end
