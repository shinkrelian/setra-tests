﻿Create Procedure [dbo].[MATARJETACREDITO_Sel_Rpt]
@Descripcion varchar(50)
as

	Set NoCount On
	select CodTar,Descripcion
	from MATARJETACREDITO
	Where (Descripcion like '%'+LTRIM(RTRIM(@Descripcion))+'%' or 
	LTRIM(RTRIM(@Descripcion))='') 
	Order By 1
