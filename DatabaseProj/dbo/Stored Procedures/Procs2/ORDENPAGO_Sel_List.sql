﻿--HLF-20130705-Agregando parametro @IDReserva                
--HLF-20130708-Agregando columna Total                
--HLF-20130709-Agregando parametro @IDCab                
--HLF-20130709-Agregando columna TotalUSD                
--HLF-20130711-Quitando columna DescFormaPago                
--HLF-20130716-Agregando campo op.IDReserva                 
--JRF-20130726-Nuevo Filtro para el nuevo campo (Fecha Emisión)            
--JRF-20130726-Nuevo Filtro Descripcion proveedor.            
--HLF-20130730-Comentar TotalUSD1          
--convert(smalldatetime,convert(varchar,op.FechaEmision,103))          
--JRF-20140220-Ordenarlo en forma ascendente x la fecha de pago        
--JRF-20140311-Agregar nueva columna de CoTipoVenta    
--JRF-20140812-Agregar los filtros de Moneda y Banco , agregar la columna Banco    
--JRF-20140813-Agregar la columna de correo    
--JRF-20140910-Agregar las columnas [IDCAB,CantidadAdjuntos]  
--JRF-20140917-Campo por default para el envio de correo copia
--HLF-20150113-@CoUbigeo_Oficina char(6), And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')
--JHD-20150228-Se agregó el correo 'pagos2@setours.com' para el envio de correo copia
--JHD-20150320-Se han puesto condiciones para el correo copia segun la oficina de Ubigeo
--JHD-20150428-left join MAPROVEEDORES pint on p.IDProveedorInternacional=pint.IDProveedor          
--JHD-20150428-And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')      
--JHD-20150626- Pais=isnull((select Descripcion from MAUBIGEO where IDubigeo=u.IDPais),''),       
--JHD-20150626- left join MAUBIGEO u on u.IDubigeo=p.IDCiudad  
--JRF-20150805-.. Or (@IDPais='000003' And (p.CoUbigeo_Oficina<>@CoUbigeo....
--JHD-20150814-MailCopia= CASE WHEN @CoUbigeo_Oficina='000113' THEN 'eugenia.rosas@setours.com' WHEN @CoUbigeo_Oficina='000110' THEN 'marcela.barriga@setours.com' else 'cobranzas@setours.com' end 
--JHD-20150903-CoSap=isnull(OP.CoSap,'')
--JHD-20150903-and (op.FlEnviado=@FlEnviado or @FlEnviado=0)
--JHD-20150905-and (op.FlEnviado=@FlEnviado)
--HLF-20150908-and (op.FlEnviado=@FlEnviado or @FlEnviado=0)
--HLF-20151028-Columna op.FlEnviado
--JRF-20160317-Nuevo campo [FlEnviadoSupResv]
CREATE Procedure [dbo].[ORDENPAGO_Sel_List] 
 @IDFile varchar(8),                    
 @DescripcionProveedor varchar(200),            
 @FechaPagoIni smalldatetime,                    
 @FechaPagoFin smalldatetime,                 
 @FechaEmisionIni smalldatetime,            
 @FechaEmisionFin smalldatetime,               
 @IDEstado char(2),                
 @IDReserva int,                
 @IDMoneda char(3),    
 @IDBanco char(3),    
 @IDCab int,
 @CoUbigeo_Oficina char(6),
 @FlEnviado bit=0
As                    
                    
Set Nocount On                    
              
 Declare @IDPais char(6)=(select IDPais from MAUBIGEO where IDubigeo=@CoUbigeo_Oficina)       
 Select c.IDCAB,  
 case when op.FechaEmision is null then '' else convert(varchar,op.FechaEmision,103)End as FechaEmision,            
 convert(varchar,op.FechaPago,103) as FechaPago, op.IDOrdPag,                  
 Case c.CoTipoVenta      
 when 'RC' Then 'RECEPTIVO'      
 when 'CU' Then 'COUNTER'      
 when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'      
 when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'      
 End as DescTipoVenta,c.IDFile As IDFile,                  
 p.NombreCorto as DescProvee,   
 Pais=isnull((select Descripcion from MAUBIGEO where IDubigeo=u.IDPais),''),        
 b.Sigla as SiglaBanco ,    
 p.IDProveedor As IDProveedor,                 
 op.IDReserva ,              
 p.IDTipoProv As IDTipoProv ,                  
            
 M.IDMoneda As IDMoneda,                  
 m.Descripcion as DescMoneda,                    
 op.IDEstado,                    
 Case op.IDEstado                     
 When 'GR' Then 'GENERADO'                    
 When 'PD' Then 'PENDIENTE'                    
 When 'PG' Then 'PAGADO'             
 End as DescEstado          
 ,(Select Sum(Total) From ORDENPAGO_DET Where IDOrdPag=op.IDOrdPag) as Total          
                 
 --(Select Sum(Case When op1.TCambio IS NULL Then Total                 
 -- Else 
 --  dbo.FnCambioMoneda(Total,'SOL','USD',op1.TCambio ) End )                 
 -- From ORDENPAGO_DET od1 Inner Join ORDENPAGO op1 On od1.IDOrdPag=op1.IDOrdPag                
 -- Where od1.IDOrdPag=op.IDOrdPag) as TotalUSD         
 ,Isnull(p.EmailContactoAdmin,'') as EmailAdmin ,  
 (select COUNT(*) from ORDENPAGO_ADJUNTOS oad Where oad.IDOrdPag=op.IDOrdPag) CantidadAdjuntos ,
 --'contabilidad3@setours.com' as MailCopia
 --'contabilidad3@setours.com; pagos2@setours.com' as MailCopia
 --MailCopia= CASE WHEN @CoUbigeo_Oficina='000113' THEN 'eugenia.rosas@setours.com' WHEN @CoUbigeo_Oficina='000110' THEN 'marcela.barriga@setours.com' else 'contabilidad3@setours.com; pagos2@setours.com' end
 MailCopia= CASE WHEN @CoUbigeo_Oficina='000113' THEN 'victoria.ontanilla@setours.com' WHEN @CoUbigeo_Oficina='000110' THEN 'marcela.barriga@setours.com' else 'cobranzas@setours.com' end,
 CoSap=isnull(OP.CoSap,''), op.FlEnviado,op.FlEnviadoSupResv
 From ORDENPAGO op                     
 Inner Join COTICAB c On op.IDCab=c.IDCAB                    
 Left Join RESERVAS r On op.IDReserva=r.IDReserva                    
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor                
 left join MAPROVEEDORES pint on p.IDProveedorInternacional=pint.IDProveedor                
 Left Join MAMONEDAS m On op.IDMoneda=m.IDMoneda                    
 Left Join MABANCOS b On op.IDBanco = b.IDBanco    
 left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
 Where (op.FechaPago Between @FechaPagoIni And @FechaPagoFin Or @FechaPagoIni='01/01/1900')                    
 And (convert(smalldatetime,convert(varchar,op.FechaEmision,103))          
 between @FechaEmisionIni And @FechaEmisionFin Or @FechaEmisionIni='01/01/1900')            
 And (c.IDFile Like '%'+ltrim(rtrim(@IDFile))+'%' Or ltrim(rtrim(@IDFile))='')                    
 And (op.IDEstado=@IDEstado Or ltrim(rtrim(@IDEstado))='')                    
 And (op.IDReserva=@IDReserva Or @IDReserva=0)             
 And (p.NombreCorto like '%'+@DescripcionProveedor+'%' or pint.NombreCorto like '%'+@DescripcionProveedor+'%' or LTRIM(rtrim(@DescripcionProveedor))='')            
 And (c.IDCAB=@IDCab Or @IDCab=0)                
 And (op.IDBanco = @IDBanco Or ltrim(rtrim(@IDBanco))='')    
 And (op.IDMoneda = @IDMoneda Or ltrim(rtrim(@IDMoneda))='')    
 And ((p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='') Or
	 (@IDPais='000003' And (p.CoUbigeo_Oficina<>@CoUbigeo_Oficina And u.IDPais=@IDPais))
 )
and (op.FlEnviado=@FlEnviado or @FlEnviado=0)
-- and (op.FlEnviado=@FlEnviado)
 Order by op.FechaPago asc --,op.IDOrdPag           
