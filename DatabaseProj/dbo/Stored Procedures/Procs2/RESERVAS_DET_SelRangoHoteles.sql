﻿--JHD-20151125- ,rd.Servicio, sd.Descripcion
--JRF-20160412- ...And r.Anulado=0 And r.Estado <> 'XL'..
CREATE Procedure dbo.RESERVAS_DET_SelRangoHoteles      
@IDReserva int,      
@IDCab int      
As      
 Set NoCount On      
 
 Declare @FechaIn smalldatetime, @FechaMx smalldatetime--, @IDUbigeo char(6)

 Select @FechaIn = cast(convert(char(10),Min(dia),103) as smalldatetime),
		@FechaMx=cast(convert(char(10),Max(dia),103) as smalldatetime)
 from RESERVAS_DET 
 Where IDReserva=@IDReserva And Anulado=0
 
 --Select @IDUbigeo=p.IDCiudad
 --from RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
 --Where IDReserva=@IDReserva
     
 select  * from(    
 select Distinct p.NombreCorto as DescProveedores , convert(char(10),rd.dia,103) as FechaIN  ,convert(char(10),rd.FechaOut,103) as FechaOut  
 ,rd.Servicio, sd.Descripcion
 from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva =r.IDReserva 
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det  
 Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor    
 where (p.IDTipoProv = '001' and sd.PlanAlimenticio = 0) and rd.Anulado = 0 
 And r.Anulado=0 And r.Estado <> 'XL' --And rd.IDubigeo=@IDUbigeo
 and r.IDCab = @IDCab and r.IDReserva <> @IDReserva and rd.Dia between @FechaIn and @FechaMx)  
 As X    
 Where X.FechaOut Is Not Null
 Order by cast(convert(char(10),X.FechaIN,103) as smalldatetime)  
