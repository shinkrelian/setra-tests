﻿Create Procedure ORDENPAGO_ADJUNTOS_Ins
@IDOrdPag int,
@NoArchivo varchar(200),
@UserMod char(4)
As
Set NoCount On
Declare @NuOrdenAdjunto int =(select Isnull(MAX(NuOrdenAdjunto),0) from ORDENPAGO_ADJUNTOS where IDOrdPag=@IDOrdPag)+1
INSERT INTO [dbo].[ORDENPAGO_ADJUNTOS]
           ([NuOrdenAdjunto]
           ,[IDOrdPag]
           ,[NoArchivo]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@NuOrdenAdjunto,
            @IDOrdPag,
            @NoArchivo,
            @UserMod,
            GETDATE())
