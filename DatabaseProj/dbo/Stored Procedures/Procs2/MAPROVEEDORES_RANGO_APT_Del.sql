﻿CREATE Procedure [dbo].[MAPROVEEDORES_RANGO_APT_Del]
	@Correlativo tinyint,
	@NuAnio CHAR(4),
    @IDProveedor int
As
	Set NoCount On
	
	Delete From MAPROVEEDORES_RANGO_APT 
	WHERE
           Correlativo=@Correlativo And
           (IDProveedor=@IDProveedor)
		   and NuAnio=@NuAnio
