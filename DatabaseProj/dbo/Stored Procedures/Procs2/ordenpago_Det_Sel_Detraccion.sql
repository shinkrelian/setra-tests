﻿--JHD-20150909-Se agrego el campo @Total_Det
--JHD-20150909set @SSDetraccion=(@Total_Det*@TCambio)*0.1
CREATE procedure ordenpago_Det_Sel_Detraccion
 @IDOrdPag int,  
 @IDServicio_Det int,
 @Total_Det numeric(12,2)=0
as
begin
declare @FlDetraccion as bit = 0
declare @IDMoneda as char(3)=''
declare @TCambio as numeric(5,2)=0
declare @Total numeric(12,2)=0
declare @FechaEmision datetime
declare @SSDetraccion as numeric(12,2)=0,
@SSDetraccion_USD as numeric(12,2)=0,
@SSDetraccion_Cab as numeric(12,2)=0
--set @IDMoneda=(select idmoneda from ordenpago where IDOrdPag=@IDOrdPag)
select @IDMoneda=idmoneda,@TCambio=TCambio,@Total=SsMontoTotal,@FechaEmision=FechaEmision,@SSDetraccion_Cab=isnull(SSDetraccion,0) from ordenpago where IDOrdPag=@IDOrdPag

set @FlDetraccion=(select FlDetraccion from MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det)
if (@FlDetraccion=1 and @SSDetraccion_Cab>0)
	begin
		if @IDMoneda='SOL'
			begin
				set @SSDetraccion=@Total_Det*0.10
				set @SSDetraccion_USD=0
			end
		else 
			if @IDMoneda='USD'
				begin
					set @SSDetraccion_USD=@Total_Det*0.10
					if isnull(@TCambio,0)=0
						--set @SSDetraccion=dbo.FnCambioMoneda(@Total,@IDMoneda,'SOL',ISNULL(@TCambio,isnull((select ValVenta from matipocambio where fecha=@FechaEmision),0)))
						set @SSDetraccion=(dbo.FnCambioMoneda(@Total_Det,@IDMoneda,'SOL',isnull((select ValVenta from matipocambio where fecha=@FechaEmision),0)))*0.1
					else
						set @SSDetraccion=(@Total_Det*@TCambio)*0.1
				end
		end
select SSDetraccion=isnull(@SSDetraccion,0),SSDetraccion_USD=isnull(@SSDetraccion_USD,0)
end;
