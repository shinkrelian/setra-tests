﻿CREATE Procedure [dbo].[MATIPODOC_Sel_Cbo] 
@bTodos bit = 0
As  
 Set NoCount On  
 select * from 
 (
 Select '' As IDtipodoc, 'NO APLICA' as Descripcion,0 as Ord
 Union 
 Select IDtipodoc,Descripcion,1   
 From MATIPODOC  
 ) As X
 Where (@bTodos=1 Or Ord <> 0)
