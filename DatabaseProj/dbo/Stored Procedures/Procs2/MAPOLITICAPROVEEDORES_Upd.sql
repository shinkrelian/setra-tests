﻿--JRF-20130319-Nuevo Campo [DiasSinPenalidad]        
--JRF-20130515-Nuevo Campo [Observacion]      
--JRF-20130520-Eliminar Campo [Observacion]      
--JRF-20130711-Nuevo Campo [PoliticaFechaReserva]
--JRF-20150309-Nuevo campo [Anio]
CREATE procedure [dbo].[MAPOLITICAPROVEEDORES_Upd]          
 @IDPolitica int,          
 @IDProveedor char(6),          
 @Anio char(4),
 @Nombre varchar(120),          
 @DefinDia smallint,          
 @DefinHasta smallint,          
 @DefinTipo char(6),          
 @RoomPreeliminar smallint,          
 @RoomActualizado smallint,          
 @RoomFinal smallint,          
 @DiasSinPenalidad smallint,   
 @PoliticaFechaReserva bit,       
 @PrePag1_Dias smallint,          
 @PrePag1_Porc numeric(5,2),          
 @PrePag1_Mont numeric(8,2),          
 @PrePag2_Dias smallint,          
 @PrePag2_Porc numeric(5,2),          
 @PrePag2_Mont numeric(8,2),          
 @Saldo_Dias smallint,          
 @Saldo_Porc numeric(5,2),          
 @Saldo_Mont numeric(8,2),          
 @UserMod char(4)          
as          
          
 Set NoCount On          
          
 UPDATE [MAPOLITICAPROVEEDORES]          
    SET [IDProveedor] = @IDProveedor     
  ,[Anio]=@Anio        
  ,[Nombre] = @Nombre          
  ,[DefinDia] = @DefinDia          
  ,[DefinHasta] = @DefinHasta          
  ,[DefinTipo] = @DefinTipo          
  ,[RoomPreeliminar] = @RoomPreeliminar          
  ,[RoomActualizado] = @RoomActualizado          
  ,[RoomFinal] = @RoomFinal          
  ,[DiasSinPenalidad] = @DiasSinPenalidad        
  ,[PoliticaFechaReserva] = @PoliticaFechaReserva  
  ,[PrePag1_Dias] = @PrePag1_Dias          
  ,[PrePag1_Porc] = @PrePag1_Porc          
  ,[PrePag1_Mont] = @PrePag1_Mont          
  ,[PrePag2_Dias] = @PrePag2_Dias          
  ,[PrePag2_Porc] = @PrePag2_Porc          
  ,[PrePag2_Mont] = @PrePag2_Mont          
  ,[Saldo_Dias] = @Saldo_Dias          
  ,[Saldo_Porc] = @Saldo_Porc          
  ,[Saldo_Mont] = @Saldo_Mont       
  ,[UserMod] = @UserMod          
  ,[FecMod] = GETDATE()          
  WHERE IDPolitica=@IDPolitica          
