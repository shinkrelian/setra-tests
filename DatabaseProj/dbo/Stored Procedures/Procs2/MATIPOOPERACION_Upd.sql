﻿
--------------------------------------------------------------------------------------

--JHD-20150216- Se agrego el campo TxDefinicion 
CREATE Procedure [dbo].[MATIPOOPERACION_Upd]  
 @IDToc char(3),  
 @Descripcion varchar(100),   
 @CtaContIGV varchar(8),
 @TxDefinicion varchar(400)='',
 @UserMod char(4)  
  
As  
  
 Set NoCount On  
  
 Update MATIPOOPERACION  
 Set Descripcion=@Descripcion,   
	CtaContIGV = case When @CtaContIGV = '' Then Null Else @CtaContIGV End,
 UserMod=@UserMod,  
 TxDefinicion=Case When ltrim(rtrim(@TxDefinicion))='' Then Null Else @TxDefinicion End ,
 FecMod=GETDATE()   
 Where IDToc=@IDToc  
