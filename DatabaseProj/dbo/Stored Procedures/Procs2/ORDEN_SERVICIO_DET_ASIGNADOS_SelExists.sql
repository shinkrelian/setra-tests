﻿CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_ASIGNADOS_SelExists
@NuOrden_Servicio_Det int,
@IDProveedor char(6),
@pExits bit output
As
Set NoCount On
set @pExits = 0
If Exists(select NuOrden_Servicio_Det from ORDEN_SERVICIO_DET_ASIGNADOS 
		  where NuOrden_Servicio_Det = @NuOrden_Servicio_Det and IDProveedor =@IDProveedor)
	Set @pExits = 1

