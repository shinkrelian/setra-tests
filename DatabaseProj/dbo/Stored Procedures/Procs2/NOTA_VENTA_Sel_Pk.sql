﻿
--HLF-20130117-Quitando guion a IDNotaVenta
CREATE Procedure dbo.NOTA_VENTA_Sel_Pk  
	@IDNotaVenta char(10)  
As  
	Set NoCount On  
	SELECT [IDNotaVenta]  
    ,[IDSerie]  
    ,CONVERT(char(10),nv.[Fecha],103) As Fecha  
    ,nv.[IDCab]  
    ,[IDMoneda]  
    ,[SubTotal]  
    ,[TotalIGV]  
    ,[Total]  
    ,[IDEstado]  
    --,SUBSTRING(nv.IDNotaVenta,0,9) +' - '+  SUBSTRING(nv.IDNotaVenta,9,9)  As NotaVenta  
    ,nv.IDNotaVenta
    ,(select CONVERT(char(10),Min(Dia),103) from COTIDET cd Where cd.IDCAB = c.IDCAB) As FechaIn  
    ,(select CONVERT(char(10),MAX(Dia),103) from COTIDET cd Where cd.IDCAB = c.IDCAB) As FechaOut  
    ,cl.RazonComercial As DescCliente  
    ,c.NroPax  
    ,c.Titulo as  Referencia  
    ,u.Nombre As Responsable  
   FROM NOTA_VENTA nv Left Join COTICAB c On nv.IDCab = c.IDCAB  
   Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente  
   Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario  
   where nv.IDNotaVenta = @IDNotaVenta
