﻿
------------------------------------------------------------------------

CREATE procedure NOTAVENTA_COUNTER_ITEM_Del
		   @NuNtaVta char(6),
		   @NuItem tinyint
as
BEGIN
Set NoCount On
	
delete from [NOTAVENTA_COUNTER_ITEM]

WHERE
			NuNtaVta=@NuNtaVta AND
			NuItem=@NuItem
END;
