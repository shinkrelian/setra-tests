﻿Create Procedure [dbo].[ORDEN_SERVICIO_SelExistexIDCabOutput]
	@IDCab int,	
	@pExiste bit output
As
	Set NoCount On
	
	set @pExiste = 0
	If Exists (Select IDProveedor From ORDEN_SERVICIO Where IDCab=@IDCab)
		Set @pExiste = 1
	
