﻿
-----------------------------------------------------------------------------------

--CREATE
CREATE Procedure MASERVICIOS_DET_Sel_Cbo_Tipo_IDServicio
@IDServicio char(8)=null
as
SELECT distinct Tipo
      ,DetaTipo=Tipo+' - '+CAST(DetaTipo as varchar(max))
     
  FROM [MASERVICIOS_DET]
  where IDServicio=@IDServicio
  and Tipo<>'' and Tipo is not null
  order by Tipo
