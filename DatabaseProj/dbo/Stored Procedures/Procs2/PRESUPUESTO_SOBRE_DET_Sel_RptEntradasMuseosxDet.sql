﻿--JRF-20150410-Left Join COTIPAX cp On psd.IDPax=cp.IDPax
--JRF-20150604-Dividir el monto en soles y Dólares
CREATE Procedure dbo.PRESUPUESTO_SOBRE_DET_Sel_RptEntradasMuseosxDet  
@NuPreSob int,  
@NuDetPreSob int  
as  
Set NoCount On  
select c.IDFile,c.Titulo,c.NroPax,Isnull(c.NroLiberados,0) as NroLiberados,
    (select Min(cd.Dia) from COTIDET cd where cd.IDCAB = ps.IDCab And cd.IDProveedor=ps.IDProveedor) as FecInicio,
	cL.RazonComercial as Cliente,  
    psd.FeDetPSo,psd.TxServicio,QtPax,SsPreUni,
	Case When psd.CoPago='STK' Then 0 else 
		Case When psd.IDMoneda = 'USD' Then psd.SsTotal Else 0 End
	End as SsTotal,
	Case When psd.CoPago='STK' Then 0 else 
		Case When psd.IDMoneda = 'SOL' Then psd.SsTotal Else 0 End
	End as SsTotalSoles
	,SsTotSus,SsTotDev,ps.CoEstado ,tps.NoTipPSo ,
    mo.Simbolo,IsNull(p.NombreCorto,cp.Titulo+' '+cp.Apellidos+' '+cp.Nombres) as DescTC_G
from PRESUPUESTO_SOBRE_DET psd Left Join PRESUPUESTO_SOBRE ps On psd.NuPreSob=ps.NuPreSob  
Left Join COTICAB c On ps.IDCab = c.IDCAB Left Join MACLIENTES cL on c.IDCliente=cL.IDCliente  
Left Join MATIPOPRESUPUESTO_SOBRE tps On psd.CoTipPSo=tps.CoTipPSo  
Left Join MAMONEDAS mo On IsNull(psd.IDMoneda,'')=mo.IDMoneda
Left Join MAPROVEEDORES p On psd.CoPrvPrg=p.IDProveedor
Left Join COTIPAX cp On cp.IDPax=psd.IDPax
where psd.NuPreSob = @NuPreSob and psd.NuDetPSo= @NuDetPreSob  
--and(CHARINDEX('taxi',TxServicio)=0)  
