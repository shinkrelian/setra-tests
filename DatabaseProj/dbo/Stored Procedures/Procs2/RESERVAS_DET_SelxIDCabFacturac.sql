﻿
Create Procedure dbo.RESERVAS_DET_SelxIDCabFacturac
	@IDCab int
As
	Set Nocount on
	
	Select rd.IDReserva_Det,rd.IDDet,r.IDProveedor,p.NombreCorto as DescProveedor,
	rd.Servicio,sd.idTipoOC,rd.Total
	From RESERVAS_DET rd Inner Join RESERVAS r On rd.IDReserva=r.IDReserva
	Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
	Where rd.IDReserva In (Select IDReserva From RESERVAS Where IDCab=@IDCab)
	And rd.Anulado=0 
	ORder by r.IDProveedor, rd.Item

