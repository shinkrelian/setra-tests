﻿--alter
CREATE Procedure [dbo].[MAVEHICULOS_TRANSPORTE_Upd]
	@NuVehiculo smallint,
	@CoTipo smallint,
	@QtDe tinyint,
	@QtHasta tinyint,
	@NoVehiculoCitado varchar(200),
	@QtCapacidad tinyint,
	@FlOtraCiudad bit,
	@CoUbigeo char(6)='',
	@CoMercado smallint=0,
	@Usermod char(4),
	@FlReservas bit=0,
	@FlProgramacion bit=0
As
	Set NoCount On
	
BEGIN
	Update [dbo].[MAVEHICULOS_TRANSPORTE]
	Set
 		[CoTipo] = @CoTipo,
 		[QtDe] = @QtDe,
 		[QtHasta] = @QtHasta,
 		[NoVehiculoCitado] = @NoVehiculoCitado,
 		[QtCapacidad] =case when @QtCapacidad=0 then Null Else @QtCapacidad End,
 		[FlOtraCiudad] = @FlOtraCiudad,
 		[CoUbigeo] = case when LTRIM(RTRIM(@CoUbigeo))='' then Null Else @CoUbigeo End,
 		[CoMercado] = case when @CoMercado=0 then Null Else @CoMercado End,-- @CoMercado,
 		[Usermod] = @Usermod,
 		Fecmod = GetDate(),
		FlReservas=@FlReservas,
		FlProgramacion=@FlProgramacion
 	Where 
		[NuVehiculo] = @NuVehiculo
END
