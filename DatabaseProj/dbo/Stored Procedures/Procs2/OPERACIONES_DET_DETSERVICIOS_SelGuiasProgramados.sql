﻿
--HLF-20140731-@IDTipoProv char(3)    
--Case When pPrg.IDTipoProv='005' Then...    
--HLF-20140811-SELECT '',''  Union  
--HLF-20140813-Or LTRIM(rtrim(@IDTipoProv))=''
--and not odd.IDProveedor_Prg is null
--JRF-20150409-Agregar filtro para lista de pax cuando el Presupuesto es TC
--JRF-20150603-Ajustar el trasladista asignado
--HLF-20151125-'Ninguno'
--HLF-20151221-Union Select IDUsuario, Nombre+' '+ISNULL(TxApellidos,'') as DescUsuario
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelGuiasProgramados      
 @IDCab int,      
 @IDProveedorInt char(6),    
 @IDTipoProv char(3),
 @FlServicioTC bit
As      
    
 Set NoCount On      
    
 SELECT '','Ninguno'--'Varios'
 Union  
 Select distinct dbo.FnDevuelveIDProveedorxIDUserTrasladista(odd.IDProveedor_Prg) as IDProveedor_Prg,       
 IsNull(
 Case When pPrg.IDTipoProv in('005','008') Then    
  Case when pPrg.ApPaterno is null Or ltrim(rtrim(pPrg.ApPaterno)) = '-' then '' else pPrg.ApPaterno End +' '+      
  Case When pPrg.ApMaterno is null Or ltrim(rtrim(pPrg.ApMaterno)) = '-' then '' else pPrg.ApMaterno End +', '+      
  pPrg.Nombre     
 Else    
  pPrg.NombreCorto    
 End 
 ,pPrg.NombreCorto) as NombreGuia      
 from OPERACIONES_DET_DETSERVICIOS odd       
 Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det      
 Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion      
 Left Join MAPROVEEDORES pPrg On dbo.FnDevuelveIDProveedorxIDUserTrasladista(odd.IDProveedor_Prg) = pPrg.IDProveedor      
 where o.IDCab = @IDCab and o.IDProveedor = Ltrim(Rtrim(@IDProveedorInt)) and odd.Activo = 1      
 and (pPrg.IDTipoProv = @IDTipoProv Or LTRIM(rtrim(@IDTipoProv))='')--'005'      
 and not odd.IDProveedor_Prg is null
 Union
 select Cast(IDPax as varchar(5)) as IDProveedor_Prg ,
 Titulo+', '+Apellidos+ ' '+Nombres as NombreGuia
 from COTIPAX 
 where IDCab=@IDCab And @FlServicioTC=1
 Union
 Select IDUsuario, Nombre+' '+ISNULL(TxApellidos,'') as DescUsuario
 from MAUSUARIOS where IdArea='OC' and Activo='A' And IDUbigeo='000066'
 
