﻿
-------------------------------------------------------------------------------------------------------

--JHD-20150116- Se agregó el campo CoCliente
CREATE procedure [dbo].[NOTAVENTA_COUNTER_ITEM_Upd]
		   @NuNtaVta char(6),
		   @NuItem tinyint,
          
           @CoTipoServicio char(3)='',
           @TxDetalle varchar(150)='',
           @CoProveedor char(6)='',
           @QtCantidad smallint=0,
           --@CoMoneda char(3)='',
           --@SSTipoCambio numeric(8,2)=0,
           
           @SSUnidad numeric(10,2),
           @SSTotal numeric(10,2),
           @UserMod char(4),
           @CoCliente char(6)=''
as
BEGIN
Set NoCount On
	
update [NOTAVENTA_COUNTER_ITEM]
SET 
         
           CoTipoServicio =@CoTipoServicio
		   ,TxDetalle =@TxDetalle
           ,CoProveedor =case When ltrim(rtrim(@CoProveedor))='' Then Null Else @CoProveedor End--@CoProveedor
           ,QtCantidad =@QtCantidad
           --,CoMoneda = @CoMoneda
           --,SSTipoCambio =@SSTipoCambio
           
           ,SSUnidad=@SSUnidad
           ,SSTotal=@SSTotal
           
           ,UserMod=@UserMod
           ,fecmod=GETDATE()
           ,CoCliente=case When ltrim(rtrim(@CoCliente))='' Then Null Else @CoCliente End
WHERE
			NuNtaVta=@NuNtaVta AND
            NuItem=@NuItem      
END

