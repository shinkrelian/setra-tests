﻿--HLF-20150701-Union	Select 4 as Orden,'NO SHOW - PROVEEDORES'...
--JRF-FKL-20160331-...And od.FlServInManualOper=1 (DETALLE DE GASTOS)
CREATE Procedure dbo.Reporte_NoShowxFile
	@IDCab int
As
	Set Nocount on

	Select 1 as Orden,'NO SHOW - PROVEEDORES' as Seccion,p.NombreCorto as DescProveedor, od.Servicio, 
	'USD' as IDMoneda, 
	dbo.FnCambioMoneda(od.NetoGen,od.IDMoneda,'USD',sd.TC) as NetoGen, 
	dbo.FnCambioMoneda(od.IgvGen,od.IDMoneda,'USD',sd.TC) as IgvGen,
	dbo.FnCambioMoneda(od.TotalGen,od.IDMoneda,'USD',sd.TC) as TotalGen
	From
	OPERACIONES_DET od Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
	Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
	Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
	where o.IDCab=@IDCab And od.FlServicioNoShow=1

	Union

	Select 2 as Orden,'NO SHOW - PROVEEDORES' as Seccion,p.NombreCorto as DescProveedor, 
	IsNull(Case When cv.TipoTransporte='V' Then 'Ticket '+ Ltrim(Rtrim(cv.Ruta))+' - '+Vuelo 
	Else cv.Servicio End,cd.Servicio)  As Servicio,
	'USD' as IDMoneda,
	IsNull(cv.CostoOperador,0)-IsNull((select SUM(IsNull(cp3.SsReembolso,0)) from COTITRANSP_PAX cp3 Where cp3.IDTransporte=cv.ID),0) as NetoGen,
	0 as IgvGen,
	IsNull(cv.CostoOperador,0)-IsNull((select SUM(IsNull(cp3.SsReembolso,0)) from COTITRANSP_PAX cp3 Where cp3.IDTransporte=cv.ID),0) as TotalGen
	From
	COTIVUELOS cv 
	Left Join MAPROVEEDORES p On cv.IdLineaA=p.IDProveedor
	Left Join COTIDET cd On cv.IdDet=cd.IDDET
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	where cv.IDCab=@IDCab 
	And Exists(select IDPax from COTIPAX cp Where cp.IDCab=cv.IDCAB and cp.FlNoShow=1)

	Union

	Select 3 as Orden,'NO SHOW - PROVEEDORES' as Seccion,DescProveedor, Servicio, 'USD' as IDMoneda,
		dbo.FnCambioMoneda(X.NetoProgramado,IDMoneda,'USD',TC) as NetoGen, 		
		dbo.FnCambioMoneda(x.IgvProgramado,IDMoneda,'USD',TC) as IgvGen, 
		dbo.FnCambioMoneda(x.NetoProgramado+x.IgvProgramado,IDMoneda,'USD',TC) as TotalGen
	   
	From
		(
		Select p.NombreCorto as DescProveedor, od.Servicio, 		
			Round(IsNuLL(ods.NetoProgram, dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0))),2) As NetoProgramado ,
			Round(IsNuLL(ods.IgvProgram,
			IsNuLL(ods.NetoProgram, dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0))) * 
			Case When sd.Afecto = 1 Then (select NuIGV from PARAMETRO)/100 else 0 End),2) As IgvProgramado, ods.IDMoneda, --sd.TC
			isnull(sd.TC,(select SsTipCam from COTICAB_TIPOSCAMBIO where IDCab=o.IDCab and CoMoneda=ods.IDMoneda)) as TC
		From OPERACIONES_DET_DETSERVICIOS ods inner Join 
		OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
		Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
		Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
		Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
		where o.IDCab=@IDCab And ods.FlServicioNoShow=1 and ods.Activo=1
		) as X

	Union
	Select 4 as Orden,'NO SHOW - PROVEEDORES' as Seccion,p.NombreCorto as DescProveedor, od.Servicio, 
	'USD' as IDMoneda, 
	dbo.FnCambioMoneda(od.NetoGen,od.IDMoneda,'USD',sd.TC) as NetoGen, 
	dbo.FnCambioMoneda(od.IgvGen,od.IDMoneda,'USD',sd.TC) as IgvGen,
	dbo.FnCambioMoneda(od.TotalGen,od.IDMoneda,'USD',sd.TC) as TotalGen
	From
	RESERVAS_DET od Inner Join RESERVAS o On od.IDReserva=o.IDReserva
	Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
	Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
	where o.IDCab=@IDCab And od.FlServicioNoShow=1


	UNION

	Select 5 as Orden,'DETALLE DE GASTOS' as Seccion,p.NombreCorto as DescProveedor, od.Servicio, 
	--od.IDMoneda, od.NetoGen,  od.IgvGen,  od.TotalGen  
	'USD' as IDMoneda, 
	dbo.FnCambioMoneda(od.NetoGen,od.IDMoneda,'USD',sd.TC) as NetoGen, 
	dbo.FnCambioMoneda(od.IgvGen,od.IDMoneda,'USD',sd.TC) as IgvGen,
	dbo.FnCambioMoneda(od.TotalGen,od.IDMoneda,'USD',sd.TC) as TotalGen
	From
	OPERACIONES_DET od Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
	Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
	Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
	where o.IDCab=@IDCab And od.IDReserva_Det Is Null And od.FlServInManualOper=1
	--And (od.NetoHabEditado=1 Or od.IgvHabEditado=1)

	UNION

	Select 6 as Orden,'DETALLE DE RECUPERACIONES' as Seccion,DescProveedor, Servicio, 
	'USD' as IDMoneda, 
	dbo.FnCambioMoneda(NetoGen,IDMoneda,'USD',TC)*(-1) as NetoGen, 
	--Case When Afecto=1 then TotalGen-NetoGen else 0 end*(-1) as IgvGen, 	
	Case When Afecto=1 then dbo.FnCambioMoneda(TotalGen-NetoGen,IDMoneda,'USD',TC)*(-1) else 0 end as IgvGen, 
	dbo.FnCambioMoneda(TotalGen,IDMoneda,'USD',TC)*(-1) as TotalGen
	From
	(
	Select  DescProveedor, Servicio, IDMoneda, 
	Case When Afecto=1 then TotalGen/(1+((select NuIGV from PARAMETRO)/100)) Else TotalGen End  as NetoGen,
	TotalGen, Afecto, TC
	From
		(
		Select p.NombreCorto as DescProveedor, od.Servicio, od.IDMoneda, od.NetoGen,  od.IgvGen,  sd.Afecto,
		isnull(od.SSTotalGenAntNoCobrado,0)-od.TotalGen as TotalGen, sd.TC
		From
		OPERACIONES_DET od Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
		Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
		Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
		where o.IDCab=@IDCab And od.FlServicioNoCobrado=1
		) as X
	) as Y
	
	Union
	
	Select 7 as Orden,'DETALLE DE RECUPERACIONES' as Seccion,DescProveedor, Servicio, 
	'USD' as IDMoneda, 
	dbo.FnCambioMoneda(NetoGen,IDMoneda,'USD',TC)*(-1) as NetoGen, 
	--Case When Afecto=1 then TotalGen-NetoGen else 0 end*(-1) as IgvGen, 	
	Case When Afecto=1 then dbo.FnCambioMoneda(TotalGen-NetoGen,IDMoneda,'USD',TC)*(-1) else 0 end as IgvGen, 
	dbo.FnCambioMoneda(TotalGen,IDMoneda,'USD',TC)*(-1) as TotalGen
	From
	(
	Select  DescProveedor, Servicio, IDMoneda, 
	Case When Afecto=1 then TotalGen/(1+((select NuIGV from PARAMETRO)/100)) Else TotalGen End  as NetoGen,
	TotalGen, Afecto, TC
	From
		(
		Select p.NombreCorto as DescProveedor, od.Servicio, od.IDMoneda, ods.NetoProgram as NetoGen,   sd.Afecto,
		isnull(ods.SSTotalProgramAntNoCobrado,0)-ods.TotalProgram as TotalGen, sd.TC
		From
		OPERACIONES_DET_DETSERVICIOS ods 
		inner Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
		Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
		Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
		Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
		where o.IDCab=@IDCab And ods.FlServicioNoCobrado=1
		) as X
	) as Y

	Order by 1
