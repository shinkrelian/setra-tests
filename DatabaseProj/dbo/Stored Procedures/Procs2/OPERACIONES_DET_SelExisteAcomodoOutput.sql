﻿Create Procedure dbo.OPERACIONES_DET_SelExisteAcomodoOutput   
 @IDOperacion int,  
 @IDServicio_Det int,  
 @CapacidadHab tinyint,  
 @EsMatrimonial bit,  
 @Dia smalldatetime,  
 @pExiste bit Output  
 As  
 Set Nocount On  
   
 Set @pExiste = 0  
   
 If Exists(Select IDOperacion From OPERACIONES_DET od 
  Inner Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det
  Where od.IDOperacion=@IDOperacion  
  And od.IDServicio_Det=@IDServicio_Det And rd.CapacidadHab=@CapacidadHab  
  And rd.EsMatrimonial=@EsMatrimonial  
  And od.Dia=@Dia)  
   
  Set @pExiste = 1  
    
