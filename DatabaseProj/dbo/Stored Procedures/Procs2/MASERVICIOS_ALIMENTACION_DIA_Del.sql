﻿Create Procedure [dbo].[MASERVICIOS_ALIMENTACION_DIA_Del]
	@IDServicio char(8),
	@Dia tinyint
As

	Set NoCount On
	
	Delete From MASERVICIOS_ALIMENTACION_DIA           
	Where IDServicio=@IDServicio And Dia=@Dia
