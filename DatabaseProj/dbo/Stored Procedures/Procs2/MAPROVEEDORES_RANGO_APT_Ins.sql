﻿
--create
CREATE Procedure [dbo].[MAPROVEEDORES_RANGO_APT_Ins]   
    @IDProveedor int,  
	@NuAnio CHAR(4),
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(10,4),  
    @UserMod char(4)  
As  
 Set NoCount On  
 Declare @Correlativo tinyint=(Select IsNull(MAX(Correlativo),0)+1   
  From MAPROVEEDORES_RANGO_APT Where IDProveedor=@IDProveedor and NuAnio=@NuAnio)  
   
 INSERT INTO MAPROVEEDORES_RANGO_APT  
           ([Correlativo]  
           ,[IDProveedor]  
		   ,NuAnio
           ,[PaxDesde]  
           ,[PaxHasta]  
           ,[Monto]  
           ,[UserMod]  
           )  
     VALUES  
           (@Correlativo,  
            @IDProveedor,  
			@NuAnio,
            @PaxDesde,  
            @PaxHasta,  
            @Monto,  
            @UserMod  
            )  

