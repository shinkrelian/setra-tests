﻿CREATE Procedure dbo.MAUSUARIOS_Sel_IdPasswordOutput
 @Usuario varchar(20), 
 --@Password varchar(max),
 @pPassword varchar(max) Output  
As  
 Set NoCount On     
 If Exists(Select IDUsuario From MAUSUARIOS WHERE Usuario= @Usuario )  
  Set @pPassword = (select password from MAUSUARIOS Where Usuario= @Usuario )
 Else  
  Set @pPassword = ''
