﻿
CREATE PROCEDURE [dbo].[MAUBIGEO_Sel_IDNacionalidadOutput]
				@Nacionalidad varchar(60),
				@pIDNacionalidad char(6) output

AS
BEGIN
	Set NoCount On
	Set @pIDNacionalidad = ''
	SELECT @Nacionalidad = CASE @Nacionalidad WHEN 'CHILEAN' THEN 'CHILE'
											  WHEN 'AUSTRALIAN' THEN 'AUSTRALIA'
											  WHEN 'UNITED STATES' THEN 'UNITED STATES OF AMERICA'
											  WHEN 'MONTENEGRIN' THEN 'MONTENEGRO'
											  WHEN 'BRITISH' THEN 'GREAT BRITAIN'
											  WHEN 'GERMAN' THEN 'GERMANY'
											  WHEN 'MALAYSIAN' THEN 'MALAYSIA'
											  WHEN 'SWEDISH' THEN 'SWEDEN'
											  WHEN 'ARGENTINIAN' THEN 'ARGENTINA'
											  WHEN 'AUSTRIAN' THEN 'AUSTRIA'
											  WHEN 'FINNISH' THEN 'FINLAND'
											  ELSE @Nacionalidad
											  END
	SELECT @pIDNacionalidad = IDubigeo FROM MAUBIGEO WHERE Descripcion = @Nacionalidad AND TipoUbig = 'PAIS'
END;
