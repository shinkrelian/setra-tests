﻿
  
--HLF-20130625-Agregando criterio  Not IDEstadoSolicitud In ('NV','PA')  
--HLF-20130901-Comentando   Not IDEstadoSolicitud In ('NV','PA')  
CREATE Procedure dbo.OPERACIONES_Sel_ExistenxIDCabOutput      
 @IDCab int,      
 @pbOk bit Output      
As      
 Set Nocount On      
  
 Set @pbOk = 0      
 If Exists(Select IDOperacion_Det From OPERACIONES_DET_DETSERVICIOS   
  Where IDOperacion_Det In (Select IDOperacion_Det From OPERACIONES_DET   
   Where IDOperacion In (Select IDOperacion From OPERACIONES   
    Where IDCab=@IDCab)  )    
  --And Not IDEstadoSolicitud In ('NV','PA')  
  )   
    
  Set @pbOk = 1    
    
