﻿Create Procedure [dbo].[MATIPOCAMBIO_Del]
	@Fecha	smalldatetime
As

	Set NoCount On

	Delete From MATIPOCAMBIO
	Where Fecha=@Fecha
