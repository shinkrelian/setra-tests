﻿CREATE Procedure [dbo].[MASERVICIOS_Det_Upd_NuevoIDServicio]
	@IDServicio char(8),		
	@IDServicioNue char(8),
	@UserMod char(4)
As
	Set NoCount On
	
	Update MASERVICIOS_DET
		SET IDServicio=@IDServicioNue, FecMod=GETDATE(), UserMod=@UserMod
	Where IDServicio=@IDServicio 


--	Select IDServicio_Det From MASERVICIOS_DET Where IDServicio=@IDServicioNue
