﻿Create Procedure [dbo].[MATARJETACREDITO_Ins]
@CodTar char(3),
@Descripcion varchar(50),
@UserMod char(4)
as

	Set NoCount On
	insert into MATARJETACREDITO
		([CodTar]
		 ,[Descripcion]
		 ,[UserMod])
		 values
		 (@CodTar
		 ,@Descripcion
		 ,@UserMod)
