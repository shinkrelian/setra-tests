﻿CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_ASIGNADOS_DelxNuOrden_Servicio
@NuOrden_Servicio int
As
Set NoCount On
Delete from ORDEN_SERVICIO_DET_ASIGNADOS 
	where NuOrden_Servicio_Det In (select NuOrden_Servicio_Det from ORDEN_SERVICIO_DET 
									where NuOrden_Servicio = @NuOrden_Servicio)
