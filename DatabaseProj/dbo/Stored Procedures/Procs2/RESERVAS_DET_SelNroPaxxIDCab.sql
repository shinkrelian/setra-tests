﻿
--HLF-20121122-Agregando NroLiberados
CREATE Procedure [dbo].[RESERVAS_DET_SelNroPaxxIDCab]
	@IDCab	int,	
	@IDDet	int
As
	Set NoCount On
	
	Select rd.NroPax, IsNull(rd.NroLiberados,0) as NroLiberados, 
	rd.IDReserva_Det, rd.IDDet, rd.UserMod 
	From RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	Where rd.IDReserva In (Select IDReserva From RESERVAS Where IDCab=@IDCab)	
	And rd.IDDet=@IDDet


