﻿--JHD-20160413-@CoMoneda char(3)=''
--JHD-20160413-Update ORDEN_SERVICIO_DET_MONEDA ...
CREATE Procedure dbo.ORDEN_SERVICIO_Upd_Estado_Multiple
@nuorden_Servicio int,
@CoEstado char(2),
@UserMod char(4),
@CoMoneda char(3)=''
As  
Set NoCount On  
BEGIN

	Update ORDEN_SERVICIO
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()
	where nuorden_Servicio=@NuOrden_Servicio

	Update ORDEN_SERVICIO_DET_MONEDA
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()
	where nuorden_Servicio=@NuOrden_Servicio and CoMoneda=@CoMoneda
END;
