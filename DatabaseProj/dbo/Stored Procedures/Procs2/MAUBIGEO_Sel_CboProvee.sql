﻿CREATE Procedure [dbo].[MAUBIGEO_Sel_CboProvee]
	@TipoUbig	varchar(20),
	@IDPais	char(6),
	@bTodos	bit
As

	Set NoCount On
	
	SELECT * FROM
	(
	
	Select '' as IDUbigeo,'<TODOS>' as Descripcion, 0 as Ord
	
	Union
	
	Select IDubigeo,Descripcion,1 From MAUBIGEO
	Where (TipoUbig=@TipoUbig Or LTRIM(RTRIM(@TipoUbig))='') And
	--(ISNULL(IDPais,'')=@IDPais Or LTRIM(RTRIM(@IDPais))='')
	(IDPais=@IDPais Or LTRIM(RTRIM(@IDPais))='')
	And Prove='S'
	) AS X
	
	Where (@bTodos=1 Or Ord<>0) 
	Order by Ord,2
