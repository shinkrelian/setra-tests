﻿

--PPMG-20151105-Se agrego el parametro @IDTemporada
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_Upd]  
 @Correlativo tinyint,  
    @IDServicio_Det int,  
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(10,4),  
    @UserMod char(4),
	@IDTemporada int
AS
BEGIN
Set NoCount On  
UPDATE MASERVICIOS_DET_RANGO_APT  
        Set PaxDesde=@PaxDesde   
        ,PaxHasta=@PaxHasta  
        ,Monto=@Monto  
        ,UserMod=@UserMod  
    WHERE  
        Correlativo=@Correlativo And
		IDTemporada = @IDTemporada AND
        (IDServicio_Det=@IDServicio_Det Or   
        IDServicio_Det In   
        (Select IDServicio_Det From MASERVICIOS_DET  
        Where IDServicio_Det_V=@IDServicio_Det ))  
END
