﻿
---------------------------------------------------------------------------------------------
CREATE PROCEDURE NOTAVENTA_COUNTER_UPD

		   @NuNtaVta char(6),
           @FeNtaVta smalldatetime,
           @SsNtaVta numeric(10,2),
           @CoCliente char(6),
           --@NoNtaVta varchar(150),
           --@QtPax smallint,
           --@TxDetalle varchar(150),
           --@CoSegmento char(1),
           --@SsCosto numeric(10,2),
           --@SsUtilidad1 numeric(10,2),
           --@SsUtilidad2 numeric(10,2),
           --@CoForPag char(3)='',
           --@SsPago numeric(10,2)='',
          -- @CoAeroLinea char(6),
           @CoEjecutivo char(4),
          -- @TxObservaciones varchar(200)='',
           @UserMod char(4),
           @CoMoneda char(3)='',
		   @SSTipoCambio numeric(8,2)=0
AS
BEGIN
UPDATE [NOTAVENTA_COUNTER]
   SET 
      [FeNtaVta] =@FeNtaVta
      ,[SsNtaVta] = @SsNtaVta
      ,[CoCliente] = @CoCliente
      --,[NoNtaVta] = @NoNtaVta
      --,[QtPax] = @QtPax
      --,[TxDetalle] =@TxDetalle
      --,[CoSegmento] =@CoSegmento
      --,[SsCosto] = @SsCosto
      --,[SsUtilidad1] = @SsUtilidad1
      --,[SsUtilidad2] =@SsUtilidad2
      --,[CoForPag] =  Case When ltrim(rtrim(@CoForPag))='' Then Null Else @CoForPag End 
      --,[SsPago] = Case When ltrim(rtrim(@SsPago))='' Then Null Else @SsPago End 
      --,[CoAeroLinea] =@CoAeroLinea
      ,[CoEjecutivo] = @CoEjecutivo
      --,[TxObservaciones] = Case When ltrim(rtrim(@TxObservaciones))='' Then Null Else @TxObservaciones End
    
      ,[UserMod] = @UserMod
      ,[FecMod] =GETDATE()
      ,CoMoneda= Case When ltrim(rtrim(@CoMoneda))='' Then Null Else @CoMoneda End
      ,SSTipoCambio= Case When SSTipoCambio=0 Then Null Else @SSTipoCambio End
      
 WHERE [NuNtaVta] = @NuNtaVta
 END;

