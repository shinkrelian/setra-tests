﻿
Create Procedure dbo.OPERACIONES_DET_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM OPERACIONES_DET WHERE IDOperacion In (select IDOperacion FROM OPERACIONES WHERE IDCab=@IDCab)

