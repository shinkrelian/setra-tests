﻿
CREATE PROCEDURE dbo.MASERVICIOS_ALIMENTACION_DIA_Ins_Copiar
	@IDServicio	char(8),
	@IDServicioNue	char(8),
	@UserMod	char(4)
As
	Set Nocount On
	
	Insert into MASERVICIOS_ALIMENTACION_DIA(IDServicio,Dia,Desayuno,Lonche,Almuerzo,Cena,IDUbigeoOri,IDUbigeoDes,UserMod)
	Select @IDServicioNue,Dia,Desayuno,Lonche,Almuerzo,Cena,IDUbigeoOri,IDUbigeoDes,@UserMod 
	From MASERVICIOS_ALIMENTACION_DIA
	Where IDServicio=@IDServicio
	
