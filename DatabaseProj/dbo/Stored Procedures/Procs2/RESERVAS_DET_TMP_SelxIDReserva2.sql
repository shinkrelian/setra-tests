﻿ 
--HLF-20131118-isnull(cc.TipoCambio,0)
--HLF-20140718-Cambios para columna TipoCambioCotiCab
CREATE Procedure [dbo].[RESERVAS_DET_TMP_SelxIDReserva2]    
 @IDReserva int    
As    
    
 Set NoCount On    
 Select rd.*,   
 sd.Tipo, sd.Anio, cd.IncGuia, p.IDTipoProv,   
 cc.Margen As MargenCotiCab, 
 --isnull(cc.TipoCambio,0) as TipoCambioCotiCab,  
 isnull(ctc.SsTipCam,0)  as TipoCambioCotiCab, 
 cc.Cotizacion, s.IDProveedor, cd.SSTotalOrig, cd.STTotalOrig, cd.FueradeHorario,  
 cd.CostoRealEditado, cd.CostoRealImptoEditado, cd.MargenAplicadoEditado,  
 cd.ServicioEditado  
 From RESERVAS_DET_TMP rd   
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det  
 Left Join COTIDET cd On rd.IDDet=cd.IDDET  
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
 LEft Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor  
 LEft Join COTICAB cc On cd.IDCAB=cc.IDCAB  
 Left Join COTICAB_TIPOSCAMBIO ctc On sd.CoMoneda=ctc.CoMoneda and ctc.IDCab=cd.IDCAB                     
 Where rd.IDReserva=@IDReserva    
 Order by rd.IDReserva_Det desc    
   

