﻿--MLL-030614-Agregado la columnas (NoFiscal ,CoSunat ,CoStarSoft)
CREATE Procedure [dbo].[MATIPODOC_Sel_List]
	@IDtipodoc	char(3),
	@Descripcion varchar(60)
As	
	Set NoCount On

	Select IDtipodoc,Descripcion, Sunat  ,NoFiscal,CoSunat,CoStarSoft 
	From MATIPODOC Where 
	(IDtipodoc=@IDtipodoc Or ltrim(rtrim(@IDtipodoc))='') And
	(Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='')

