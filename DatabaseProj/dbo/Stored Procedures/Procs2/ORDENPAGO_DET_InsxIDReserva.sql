﻿--HLF-20120803-Nuevo parametro @TCambio     
--HLF-20130708-Cambiar Total por TotalGen para la columna Total  
--HLF-20130716-Agregar resta de pagos
--JRF-20150411-Agrgar campo Texto [...and rd.Anulado=0]
CREATE Procedure dbo.ORDENPAGO_DET_InsxIDReserva    
 @IDReserva int,    
 @IDOrdPag int,    
 @Porcentaje numeric(5,2),     
 @Monto numeric(8,2),     
     
 @TCambio numeric(5,2),    
     
 @UserMod char(4)      
As    
 Set Nocount on    
 Declare @IDOrdPag_Det int    
 Execute dbo.Correlativo_SelOutput 'ORDENPAGO_DET',1,10,@IDOrdPag_Det output    
    
 Insert Into ORDENPAGO_DET(IDOrdPag_Det, IDOrdPag, IDReserva_Det,TxDescripcion, IDServicio_Det, NroPax, NroLiberados,     
  Cantidad, Noches, Total, UserMod)    
 Select IDOrdPag_Det, IDOrdPag, IDReserva_Det ,Servicio , IDServicio_Det, NroPax, NroLiberados,     
  Cantidad, Noches,     
  Case When @TCambio>0 Then Total*@TCambio Else Total End as Total,     
  UserMod     
 From    
  (    
  Select (@IDOrdPag_Det + ROW_NUMBER() Over(Order by IDReserva_Det))-1 as IDOrdPag_Det, @IDOrdPag as IDOrdPag,     
  IDReserva_Det, IDServicio_Det, NroPax,    
  NroLiberados, Cantidad, Noches,    
  Case When @Porcentaje>0 Then    
   (TotalGen-
   Isnull(
		(Select SUM(
		Case When op1.TCambio IS NULL Then od1.Total Else   
			dbo.FnCambioMoneda(od1.Total,'SOL','USD',op1.TCambio ) End )
		From ORDENPAGO_DET od1 
		Inner Join ORDENPAGO op1 On od1.IDOrdPag=op1.IDOrdPag  
		Where od1.IDReserva_Det=rd.IDReserva_Det
		),0))   *(@Porcentaje/100)    
  Else    
   Case When @Monto>0 Then    
     @Monto    
   Else    
    0    
   End    
  End as Total,    
  @UserMod as UserMod,
  rd.Servicio    
  From RESERVAS_DET rd   
  Where IDReserva=@IDReserva and rd.Anulado=0 
  ) as X    
 Where x.Total>0    
     
 Declare @iRowsAff smallint=@@ROWCOUNT - 1       
 If @iRowsAff>0 Exec Correlativo_Upd 'ORDENPAGO_DET',@iRowsAff
