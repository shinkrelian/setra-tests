﻿Create Procedure dbo.RESERVAS_DET_TMP_Del_DetalleIgual
@IDReserva int
As
Begin
Set NoCount On
Declare @tmpDeleteReservaDet table(IDReserva_Det int)

Insert into @tmpDeleteReservaDet
Select IDReserva_Det from (
Select *,ROW_NUMBER()Over(Partition by x.Descripcion Order By x.Descripcion) as NroItem from (
select IDReserva_Det,convert(char(10),dia,103)+' '+convert(char(10),dia,108)+' | '+convert(char(10),FechaOut,103)+' '+convert(char(10),FechaOut,108)+ ' | '+
	   IDubigeo+' | '+cast(NroPax as varchar(2))+' | '+cast(Noches as varchar(2))+' | '+Cast(Cantidad as varchar(2))+' | '+Servicio+' | '+' | '+IDIdioma+ ' | '+
	   cast(IDServicio_Det as varchar(7))+' | '+cast(CantidadAPagar as varchar(8)) as Descripcion
from RESERVAS_DET_TMP where IDReserva=@IDReserva)
As X ) As Y
Where Y.NroItem=2

Delete from RESERVAS_DET_TMP where IDReserva_Det In (select IDReserva_Det from @tmpDeleteReservaDet)
End
