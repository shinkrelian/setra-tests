﻿--JRF-20150506-Nueva condición para el MAPI & Mountain [CUZ00905]
--JRF-20150526-Calcular el Costo * las entradas pendientes
--JRF-20150527-No considerar los servicios de Mountain en Wayna.. Solo en MAPI
--JRF-20150527-Considerar el costo de niños, solo si @FilesConNinios esta activo
--JRF-20150604-Se debe considerar la hora de reserva
--JRF-20150604-Separa los niños de los adultos campo [NroPax]
--JRF-20150605-Ajuste para el cod. CUZ00946
--JRF-20150615-Adicionar el RAQCHI x DetallePax
--JRF-20150616-Si no existe @..MP y Existe @..WP no 
--JRF-20150817- ... Where (Y.FechaINCusco Between @FechaIni And @FechaFin Or Convert(Char(10),@FechaIni,103)='01/01/1900')
--HLF-20150918--If Upper(@vcMPWPRQ)='WAYNA'		
	--If (Select Count(distinct Convert(Char(10),FechaMPWP,103)) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)) = 1 And
	--	(Select Count(*) from RPT_ENTRADASINC Where IDCab=@IDCabTemp) > 1
--HLF-20150922-where CodMapioWaynaoRaqchioMyW='CUZ00132' or 
--JRF-20160128-Si MAPI + WAYNA (Mismo dia) .. se suman los costos
--JRF-20160219-..if Exists(select IDFile from RPT_ENTRADASINC where IDCab=@IDCabTemp And FlPostGenerado=0  And FileAutorizado=1)...
--JRF-20160307-Case When X.EntPendientesMAPI > 0 Then 0 Else 1 End --1 aS Autorizado
CREATE Procedure dbo.Reporte_INC_MPWP
@FechaIni smalldatetime ,
@FechaFin smalldatetime ,
@NuFile varchar(8) ,
@vcMPWPRQ varchar(10) ,
@IDEjecutivoVta char(4) ,
@IDEjecutivoRes char(4) ,
@Estado varchar(30) ,
@EstadoDatosPax char(1),
@FilesConNinios bit,
@FilesAutorizados bit
As
Begin
Set NoCount On
Declare @IDFileTemp char(8)='',
@IDCabTemp int=0,
@DescClienteTemp varchar(Max)='',
@TituloTemp varchar(Max)='',
@FechaINCuscoTemp smalldatetime='01/01/1900',
@FechaMAPWPTemp smalldatetime='01/01/1900',
@PaxTemp smallint=0,
@EntCompradasMAPITemp smallint=0,
@EntPendientesMAPITemp smallint=0,
@EntCompradasWAYNATemp smallint=0,
@EntPendientesWAYNATemp smallint=0,
@EntCompradas smallint =0,
@ntPendientes smallint =0,
@TarifaTemp numeric(12,4)=0,
@DescServicioTemp varchar(Max)='',
@EjecReservasTemp varchar(100)='',
@DatosPaxIncompletosTemp bit=0,
@ExisteNinioTemp bit=0,
@CantidadNiniosTemp smallint=0,
@FileAutorizadoTemp bit=0,
@CodMapioWaynaoRaqchioMyWTemp char(8)='',
@IDDetTemp int=0,
@EstadoTemp varchar(Max)='',
@FlPostGeneradoTemp bit =0

--Declare @FechaIni smalldatetime='01/01/1900',
--@FechaFin smalldatetime='01/01/1900',
--@NuFile varchar(8)='16020124',
--@vcMPWPRQ varchar(10)='Mapi',
--@IDEjecutivoVta char(4)='',
--@IDEjecutivoRes char(4)='',
--@Estado varchar(30)='',
--@EstadoDatosPax char(1)='',
--@FilesConNinios bit=0,
--@FilesAutorizados bit=0

Delete from RPT_ENTRADASINC

Insert into RPT_ENTRADASINC
Select Y.IDFile,Y.IDCab,Y.DescCliente,Y.Titulo,Y.FechaINCusco,Y.FecServicioINC,Y.NroPax,Y.EntCompradasMAPI,Y.EntPendientesMAPI,
	   Y.EntCompradasWAYNA,Y.EntPendientesWAYNA,Y.EntCompradas,Y.EntPendientes,Y.Tarifa,Y.Servicio,Y.EjecutivoReservas,Y.DatosPaxInCompletos,
	   Y.ExisteNinio,Y.CantNinios,
	   Y.FileAutorizado,Y.MAPIWAYNARAQCHI,Y.IDDET,
	   Case When (Y.EntCompradas=Y.NroPax Or Y.EntCompradas=Y.CantNinios) And Y.EntCompradas>0 Then 'COMPRADA'
		Else Case When Y.EntCompradas= 0 Then 'PENDIENTE' Else 
			Case When Y.EntPendientes > 0 Then 'PARCIAL' Else '' End End
	    End As Estado,
	   Y.PostGenerado
	from (


	Select  X.IDFile,X.IDCAB,X.DescCliente,X.Titulo,X.FechaINCusco,X.FecServicioINC,
		--(X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN) As NroPax,
		X.NroPax + X.NroPaxADCAN As NroPax,
	    X.EntCompradasMAPI,X.EntPendientesMAPI,X.EntCompradasWAYNA,X.EntPendientesWAYNA,
		
		Case 
		When X.MAPIWAYNARAQCHI = 'CUZ00132' Then
			--X.EntCompradasMAPI
			Case When @FilesConNinios = 0 Then X.EntCompradasMAPIAdulto+x.EntCompradasMAPIAdultoCAN Else X.EntCompradasMAPINinio+X.EntCompradasMAPINinioCAN End
		When X.MAPIWAYNARAQCHI ='CUZ00905' Or X.MAPIWAYNARAQCHI ='CUZ00946' Then
			X.EntCompradasMAPISUPPMountain
		When X.MAPIWAYNARAQCHI = 'CUZ00134' Then
			--X.EntCompradasWAYNA
			Case When @FilesConNinios = 0 Then X.EntCompradasWAYNAAdulto+x.EntCompradasWAYNAAdultoCAN Else X.EntCompradasWAYNANinio+X.EntCompradasWAYNANinioCAN End
		When X.MAPIWAYNARAQCHI = 'CUZ00133' Then
			--X.EntCompradasWAYNA
			Case When @FilesConNinios = 0 Then X.EntCompradasWAYNAAdulto+x.EntCompradasWAYNAAdultoCAN Else X.EntCompradasWAYNANinio+X.EntCompradasWAYNANinioCAN End
		When X.MAPIWAYNARAQCHI = 'CUZ00145' Then
			Case When @FilesConNinios = 0 Then X.EntCompradasRAQCHIAdulto+x.EntCompradasRAQCHIAdultoCAN Else X.EntCompradasRAQCHINinio+X.EntCompradasRAQCHINinioCAN End
		else 0
		End
		
		 As EntCompradas,
		Case 
		When X.MAPIWAYNARAQCHI = 'CUZ00132'   Then
			--X.EntPendientesMAPI
			Case When @FilesConNinios = 0 Then X.EntPendientesMAPIAdulto+x.EntPendientesMAPIAdultoCAN Else X.EntPendientesMAPINinio+X.EntPendientesMAPINinioCAN End
		When X.MAPIWAYNARAQCHI ='CUZ00905' Or X.MAPIWAYNARAQCHI ='CUZ00946' Then
			(X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN) - X.EntCompradasMAPISUPPMountain
		When X.MAPIWAYNARAQCHI = 'CUZ00134' Then
			--X.EntPendientesWAYNA
			Case When @FilesConNinios = 0 Then X.EntPendientesWAYNAAdulto+x.EntPendientesWAYNAAdultoCAN Else X.EntPendientesWAYNANinio+X.EntPendientesWAYNANinioCAN End
		When X.MAPIWAYNARAQCHI = 'CUZ00133' Then
			--X.EntPendientesWAYNA
			Case When @FilesConNinios = 0 Then X.EntPendientesWAYNAAdulto+x.EntPendientesWAYNAAdultoCAN Else X.EntPendientesWAYNANinio+X.EntPendientesWAYNANinioCAN End
		When X.MAPIWAYNARAQCHI = 'CUZ00145' Then
			--1
			Case When @FilesConNinios = 0 Then X.EntPendientesRAQCHIAdulto+x.EntPendientesRAQCHIAdultoCAN Else X.EntPendientesRAQCHINinio+X.EntPendientesRAQCHINinioCAN End
		else 0
		End As EntPendientes,
		Case When @FilesConNinios=0 Then 
	    (dbo.FnDevuelveTarifaINC(X.MAPIWAYNARAQCHI,Year(X.FecServicioINC),Case When X.MAPIWAYNARAQCHI='CUZ00145' Then '' Else 'AD'End) 
		--* Case Upper(@vcMPWPRQ) When 'WAYNA' Then X.EntPendientesWAYNAAdulto  When 'MAPI' Then X.EntPendientesMAPIAdulto When 'RAQC' then X.NroPax End
		* Case When X.MAPIWAYNARAQCHI ='CUZ00134' Or X.MAPIWAYNARAQCHI='CUZ00133' Then  X.EntPendientesWAYNAAdulto Else 
			Case When X.MAPIWAYNARAQCHI In ('CUZ00132','CUZ00905','CUZ00912','CUZ00946') Then X.EntPendientesMAPIAdulto Else 
			  Case When X.MAPIWAYNARAQCHI = 'CUZ00145' Then x.EntPendientesRAQCHIAdulto Else X.NroPax End End End)
		+ (dbo.FnDevuelveTarifaINC(X.MAPIWAYNARAQCHI,Year(X.FecServicioINC),Case When X.MAPIWAYNARAQCHI='CUZ00145' Then '' Else 'CAN'End) 
		--* X.NroPaxADCAN
		* Case When X.MAPIWAYNARAQCHI ='CUZ00134' Or X.MAPIWAYNARAQCHI='CUZ00133' Then  X.EntPendientesWAYNAAdultoCAN Else 
			Case When X.MAPIWAYNARAQCHI In ('CUZ00132','CUZ00905','CUZ00912','CUZ00946') Then X.EntPendientesMAPIAdultoCAN Else 
			   Case When X.MAPIWAYNARAQCHI = 'CUZ00145' Then x.EntPendientesRAQCHIAdultoCAN Else X.NroPaxADCAN End End End)
		else
		(dbo.FnDevuelveTarifaINC(X.MAPIWAYNARAQCHI,Year(X.FecServicioINC),Case When X.MAPIWAYNARAQCHI='CUZ00145' Then '' Else 'CHD'End) 
		--* X.NroPaxNinio
		* Case When X.MAPIWAYNARAQCHI ='CUZ00134' Or X.MAPIWAYNARAQCHI='CUZ00133' Then  X.EntPendientesMAPINinio Else 
			Case When X.MAPIWAYNARAQCHI In ('CUZ00132','CUZ00905','CUZ00912','CUZ00946') Then X.EntPendientesWAYNANinio Else 
			  Case When X.MAPIWAYNARAQCHI = 'CUZ00145' Then X.EntPendientesRAQCHINinio else X.NroPaxNinio End End End)
		+ (dbo.FnDevuelveTarifaINC(X.MAPIWAYNARAQCHI,Year(X.FecServicioINC),Case When X.MAPIWAYNARAQCHI='CUZ00145' Then '' Else 'CHD-CAN'End) 
		--* X.NroPaxNinioCAN
		* Case When X.MAPIWAYNARAQCHI ='CUZ00134' Or X.MAPIWAYNARAQCHI='CUZ00133' Then  X.EntPendientesMAPINinioCAN Else 
			Case When X.MAPIWAYNARAQCHI In ('CUZ00132','CUZ00905','CUZ00912','CUZ00946') Then X.EntPendientesWAYNANinioCAN Else 
			  Case When X.MAPIWAYNARAQCHI = 'CUZ00145' Then X.EntPendientesRAQCHINinioCAN else X.NroPaxNinio End End End)
	    End
		 * Case When X.MAPIWAYNARAQCHI = 'CUZ00905' Then (X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN) - X.EntCompradasMAPISUPPMountain Else 1 End
		As Tarifa,
		X.Servicio,X.EjecutivoReservas,dbo.FnDatosIncompletosPaxxFile(x.IDCAB) as DatosPaxInCompletos,
		X.ExisteNinio As ExisteNinio,X.NroPaxNinioCAN + x.NroPaxNinio As CantNinios,
		--Case When X.EntCompradas = X.NroPax and EntCompradas > 0 Then 1 Else 0 End as FileAutorizado,
		Case 
		When X.MAPIWAYNARAQCHI = 'CUZ00132' Or X.MAPIWAYNARAQCHI='CUZ00905' Or X.MAPIWAYNARAQCHI ='CUZ00946' Then
			Case When (X.EntCompradasMAPI= (X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN) And 
					  (Case When @FilesConNinios = 0 Then X.EntCompradasMAPIAdulto+x.EntCompradasMAPIAdultoCAN Else X.EntCompradasMAPINinio+X.EntCompradasMAPINinioCAN End) > 0)
			Or (X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN)=X.CantPaxAutorizado Then 1 Else 0 End 
		When X.MAPIWAYNARAQCHI = 'CUZ00134' Then
			Case When (X.EntCompradasWAYNA= (X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN) And X.EntCompradasWAYNA > 0) 
			Or (X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN)=X.CantPaxAutorizado Then 1 Else 0 End 
		When X.MAPIWAYNARAQCHI = 'CUZ00133' Then
			Case When X.EntCompradasWAYNA = (X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN) And
					  X.EntCompradasWAYNA > 0 Or (X.NroPax + X.NroPaxADCAN + x.NroPaxNinio + X.NroPaxNinioCAN)=X.CantPaxAutorizado Then 1 Else 0 End
		When X.MAPIWAYNARAQCHI = 'CUZ00145' Then
			Case When X.EntPendientesMAPI > 0 Then 0 Else 1 End --1
		else 0
		End
		as FileAutorizado,
		X.MAPIWAYNARAQCHI,X.IDDET 
		, 0 As PostGenerado
		from (


		Select  c1.IDFile,c1.IDCab,cd1.Servicio,
				(Select Min(Dia) From COTIDET Where IDCAB=c1.IDCAB And IDubigeo='000066') as FechaINCusco,
				cl1.RazonComercial as DescCliente,c1.Titulo,
				IsNull(rd.Dia,cd1.Dia) As FecServicioINC,cd1.IDDET,
				--rd.Dia As FecServicioINC,cd1.IDDET,
				
				(Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlDesactNoShow=0 and Not cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) as NroPax,
				
				(Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) as NroPaxADCAN,
				 
				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
				  Not cp.IDNacionalidad In ('000323','000018','000013','000007')) As NroPaxNinio,
				 
				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
				  And cp.IDNacionalidad In ('000323','000018','000013','000007')) As NroPaxNinioCAN,

				 (Select COUNT(*) From COTIDET_PAX cdp2 Where cdp2.IDDET=cd1.IDDet And FlEntMPGenerada=1 And FlDesactNoShow=0) as EntCompradasMAPISUPPMountain,
				 (Select COUNT(*) From COTIDET_PAX cdp2 Where cdp2.IDDET=cd1.IDDet And FlEntMPGenerada=1 And FlDesactNoShow=0) as EntCompradasMAPI,
				 --#Entradas Compradas Adultos, Adultos CAN, Niños, Niños CAN
				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax 
				 Where IDDET=cd1.IDDet And FlEntMPGenerada=1 And FlDesactNoShow=0 and Not cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntCompradasMAPIAdulto,

				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlEntMPGenerada=1 And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntCompradasMAPIAdultoCAN,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntMPGenerada=1 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
				  Not cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntCompradasMAPINinio,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntMPGenerada=1 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
				  And cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntCompradasMAPINinioCAN,

				  --#Fin Entradas Compradas Adultos, Adultos CAN, Niños, Niños CAN
				 (Select COUNT(*) From COTIDET_PAX cdp3 Where cdp3.IDDET=cd1.IDDet And FlEntMPGenerada=0 And FlDesactNoShow=0) as EntPendientesMAPI,
				 --Desdoblar por Adultos,Adultos Can,Niños, Niños CAN
				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax 
				 Where IDDET=cd1.IDDet And FlEntMPGenerada=0 And FlDesactNoShow=0 and Not cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntPendientesMAPIAdulto,

				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlEntMPGenerada=0 And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntPendientesMAPIAdultoCAN,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntMPGenerada=0 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
				  Not cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntPendientesMAPINinio,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntMPGenerada=0 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
				  And cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntPendientesMAPINinioCAN,

				 --Fin
				 (Select COUNT(*) From COTIDET_PAX cdp2 Where cdp2.IDDET=cd1.IDDet And FlEntWPGenerada=1 And FlDesactNoShow=0) as EntCompradasWAYNA,
				 --#Entradas Compradas WAYNA
				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax 
				 Where IDDET=cd1.IDDet And FlEntWPGenerada=1 And FlDesactNoShow=0 and Not cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntCompradasWAYNAAdulto,

				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlEntWPGenerada=1 And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntCompradasWAYNAAdultoCAN,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntWPGenerada=1 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
				  Not cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntCompradasWAYNANinio,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntWPGenerada=1 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
				  And cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntCompradasWAYNANinioCAN,
				 
				 --#Fin Entradas Compradas WAYNA
				 (Select COUNT(*) From COTIDET_PAX cdp3 Where cdp3.IDDET=cd1.IDDet And FlEntWPGenerada=0 And FlDesactNoShow=0) as EntPendientesWAYNA,
				 --Desdoblar por Adultos,Adultos Can,Niños, Niños CAN

				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax 
				 Where IDDET=cd1.IDDet And FlEntWPGenerada=0 And FlDesactNoShow=0 and Not cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntPendientesWAYNAAdulto,

				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlEntWPGenerada=0 And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntPendientesWAYNAAdultoCAN,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntWPGenerada=0 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
				  Not cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntPendientesWAYNANinio,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntWPGenerada=0 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
				  And cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntPendientesWAYNANinioCAN,

				 --fin

				 --Nuevo #Entradas Compradas RAQCHI
				  (Select COUNT(*) From COTIDET_PAX cdp2 Where cdp2.IDDET=cd1.IDDet And FlEntRQGenerada=1 And FlDesactNoShow=0) as EntCompradasRAQCHI,
				 --#Entradas Compradas RAQCHI
				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax 
				 Where IDDET=cd1.IDDet And FlEntRQGenerada=1 And FlDesactNoShow=0 and Not cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntCompradasRAQCHIAdulto,

				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlEntRQGenerada=1 And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntCompradasRAQCHIAdultoCAN,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntRQGenerada=1 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
				  Not cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntCompradasRAQCHINinio,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntRQGenerada=1 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
				  And cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntCompradasRAQCHINinioCAN,
				 
				 --#Fin #Entradas Compradas RAQCHI
				 
				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax 
				 Where IDDET=cd1.IDDet And FlEntRQGenerada=0 And FlDesactNoShow=0 and Not cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntPendientesRAQCHIAdulto,

				 (Select COUNT(*) From COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				 Where IDDET=cd1.IDDet And FlEntRQGenerada=0 And FlDesactNoShow=0 and cp.IDNacionalidad In('000323','000018','000013','000007')
				 and Not(Lower(Titulo)='chd.' and FlAdultoINC=0)) As EntPendientesRAQCHIAdultoCAN,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntRQGenerada=0 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0) And
				  Not cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntPendientesRAQCHINinio,

				 (Select Count(*) from COTIDET_PAX cpx Left Join COTIPAX cp on cpx.IDPax=cp.IDPax
				  Where IDDET=cd1.IDDet And FlEntRQGenerada=0 And cpx.FlDesactNoShow=0 and (Lower(Titulo)='chd.' and FlAdultoINC=0)
				  And cp.IDNacionalidad In ('000323','000018','000013','000007')) As EntPendientesRAQCHINinioCAN,

				 --fin
				 uRes.Nombre as EjecutivoReservas,
				 (Select Count(*) from COTIPAX cpx Where cpx.IDCab=c1.IDCAB and FlAutorizarMPWP=1 and FlNoShow=0) As CantAutorizadas,
				 (Select Count(*) from COTIPAX cpx Where cpx.IDCab=c1.IDCAB and FlNoShow=0 And (FlEntMP=1 Or FlEntWP=1) 
				 --And (Lower(Titulo)='chd.' and FlAdultoINC=0)
				 ) As CantPaxMPWP,
				 (Select Count(*) from COTIPAX cpx Where cpx.IDCab=c1.IDCAB and FlNoShow=0 And FlAutorizarMPWP=1) As CantPaxAutorizado,
				 Case When Exists(select * from COTIPAX where IDCAB= cd1.IDCAB And Titulo='chd.' And FlAdultoINC=0) Then 1 Else 0 End as ExisteNinio,
				 --dbo.FnServicioMPWP(cd1.IDServicio_Det) As MAPIWAYNARAQCHI
				 (select Top(1) IDServicio from MASERVICIOS_DET sd5
				  where sd5.IDServicio_Det In (select IDServicio_Det_V from MASERVICIOS_DET sd6  where sd6.IDServicio=sd2.IDServicio And sd6.Anio=sd2.Anio And sd6.Tipo=sd2.Tipo) And 
IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00912','CUZ00946'))
				 As MAPIWAYNARAQCHI
		from COTICAB c1 Left Join MACLIENTES cl1 On c1.IDCliente=cl1.IDCliente
		Left Join COTIDET cd1 On cd1.IDCAB=c1.IDCAB 
		--Inner Join COTIDET_DETSERVICIOS cds1 On cd1.IDServicio_Det=cds1.IDServicio_Det
		Inner Join MASERVICIOS_DET sd On cd1.IDServicio_Det=sd.IDServicio_Det
		And 
		Exists (Select sdRe.IDServicio 
				from COTIDET_DETSERVICIOS cds Inner Join MASERVICIOS_DET sd On cds.IDDET=cd1.IDDET And cds.IDServicio_Det=sd.IDServicio_Det
				Left Join MASERVICIOS_DET sdRe On sd.IDServicio_Det_V= sdRe.IDServicio_Det
				Where sdRe.IDServicio In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145','CUZ00905','CUZ00946','CUZ00912'))
		--And dbo.FnServicioMPWP(sd.IDServicio_Det) In ('CUZ00132','CUZ00133','CUZ00134','CUZ00145')
		--Inner Join MASERVICIOS_DET sdRel On sd.IDServicio_Det_V= sdRel.IDServicio_Det
		Left Join MAUSUARIOS uRes On c1.IDUsuarioRes=uRes.IDUsuario
		Left Join RESERVAS_DET rd On cd1.IDDet=rd.IDDet and rd.IDReserva_DetOrigAcomVehi is null
		Left Join MASERVICIOS_DET sd2 On cd1.IDServicio_Det=sd2.IDServicio_Det
		Where c1.Estado='A' And cd1.IncGuia=0 and c1.IDFile is not null And IsNull(rd.Anulado,0)=0
		And (Ltrim(Rtrim(@NuFile))='' Or c1.IDFile=@NuFile)
		And (c1.IDUsuario=@IDEjecutivoVta Or LTRIM(rtrim(@IDEjecutivoVta))='')
		And (c1.IDUsuarioRes=@IDEjecutivoRes Or LTRIM(rtrim(@IDEjecutivoRes))='')
		
		) as X
	Where Ltrim(Rtrim(X.MAPIWAYNARAQCHI)) <> '' --and x.IDFile='15050165'


) As Y
Where (Y.FechaINCusco Between @FechaIni And @FechaFin Or Convert(Char(10),@FechaIni,103)='01/01/1900')
Order By Y.IDCAB
option (recompile)

--Select * from RPT_ENTRADASINC
Declare curEntradasINC cursor for 
Select * from RPT_ENTRADASINC
--option (recompile)
Open curEntradasINC
Begin
	Fetch Next From curEntradasINC into @IDFileTemp,@IDCabTemp,@DescClienteTemp,@TituloTemp,@FechaINCuscoTemp,@FechaMAPWPTemp,@PaxTemp,@EntCompradasMAPITemp,@EntPendientesMAPITemp,@EntCompradasWAYNATemp,@EntPendientesWAYNATemp,@EntCompradas,
										@ntPendientes,@TarifaTemp,@DescServicioTemp,@EjecReservasTemp,@DatosPaxIncompletosTemp,@ExisteNinioTemp,@CantidadNiniosTemp,@FileAutorizadoTemp,@CodMapioWaynaoRaqchioMyWTemp,@IDDetTemp,@EstadoTemp,@FlPostGeneradoTemp
	While @@FETCH_STATUS=0
	Begin
		If Upper(@vcMPWPRQ)='WAYNA'
		Begin
			If (Select Count(distinct Convert(Char(10),FechaMPWP,103)) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)) = 1 And
			   (Select Count(*) from RPT_ENTRADASINC Where IDCab=@IDCabTemp) > 1
			Begin
			 
			--Select Count(*) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)
			If (Select Count(*) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)) > 1
			Begin
				If Exists(Select IDCab from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103) And CodMapioWaynaoRaqchioMyW='CUZ00905')
				Begin
					Delete from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103) And (CodMapioWaynaoRaqchioMyW = 'CUZ00905') --Or FlPostGenerado=0)
				End
				Else
				Begin
					If Not Exists(select IDCab from RPT_ENTRADASINC Where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW='CUZ00133' And FlPostGenerado=1 And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103))
					Begin
						Insert into RPT_ENTRADASINC
						Select [IDFile],[IDCab],[DescCliente],[Titulo],[FechaINCusco],[FechaMPWP],[Pax],[EntCompradasMAPI],[EntPendientesMAPI],[EntCompradasWAYNA],[EntPendientesWAYNA],[EntCompradas],
							   [EntPendientes],dbo.FnDevuelveCostoINCxMAPIWAYNARAQ([IDDet],'CUZ00133',YEAR(FechaMPWP),0,@FilesConNinios) As [Tarifa],
						(select Descripcion from MASERVICIOS Where IDServicio='CUZ00133') As [DescServicio],[EjecReservas],Cast([DatosPaxIncompletos] as int),
						[ExisteNinio],[CantNinios],Cast([FileAutorizado] as int),'CUZ00133' As [CodMapioWaynaoRaqchioMyW],[IDDet],[Estado],1 As [FlPostGenerado]
						from RPT_ENTRADASINC Where CodMapioWaynaoRaqchioMyW='CUZ00134' And IDCab=@IDCabTemp
					End					

					Delete from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103) And CodMapioWaynaoRaqchioMyW <> 'CUZ00133' And FlPostGenerado=0
				End
			End
			Else
			Begin
			  Declare @RegMAPI tinyint=(select Count(distinct Convert(char(10),FechaMPWP,103)) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW='CUZ00132')
			  Declare @RegSPWY tinyint=(select Count(distinct Convert(char(10),FechaMPWP,103)) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW='CUZ00134')

			  --select @RegMAPI,@RegSPWY
			  if @RegMAPI>=1 And @RegSPWY>=1
			  Begin
			    if Exists(select * from RPT_ENTRADASINC where CodMapioWaynaoRaqchioMyW = 'CUZ00132' 
				And IDCab=@IDCabTemp And CONVERT(char(10),FechaMPWP,103) In (select CONVERT(char(10),FechaMPWP,103) from RPT_ENTRADASINC where CodMapioWaynaoRaqchioMyW = 'CUZ00134' And IDCab=@IDCabTemp))
				Begin
					Insert into RPT_ENTRADASINC
					Select [IDFile],[IDCab],[DescCliente],[Titulo],[FechaINCusco],[FechaMPWP],[Pax],[EntCompradasMAPI],[EntPendientesMAPI],[EntCompradasWAYNA],[EntPendientesWAYNA],[EntCompradas],
					   [EntPendientes],
					   dbo.FnDevuelveCostoINCxMAPIWAYNARAQ([IDDet],'CUZ00132',YEAR(FechaMPWP),0,@FilesConNinios) + dbo.FnDevuelveCostoINCxMAPIWAYNARAQ([IDDet],'CUZ00134',YEAR(FechaMPWP),0,@FilesConNinios) 
					    As [Tarifa],
					   (select Descripcion from MASERVICIOS Where IDServicio='CUZ00133') As [DescServicio],[EjecReservas],Cast([DatosPaxIncompletos] as int),
						[ExisteNinio],[CantNinios],Cast([FileAutorizado] as int),'CUZ00133' As [CodMapioWaynaoRaqchioMyW],[IDDet],[Estado],1 As [FlPostGenerado]
					from RPT_ENTRADASINC Where CodMapioWaynaoRaqchioMyW='CUZ00134' And IDCab=@IDCabTemp

					Delete from RPT_ENTRADASINC where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW <> 'CUZ00134' And FlPostGenerado=0
				End
			  End

			  if @RegMAPI=1 And @RegSPWY=1
			  Begin
				Declare @FechMAPI char(10)=(select distinct Convert(char(10),FechaMPWP,103) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW='CUZ00132')
				Declare @FechSPWY char(10)=(select distinct Convert(char(10),FechaMPWP,103) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW='CUZ00134')

				if @FechMAPI=@FechSPWY
				Begin
					Insert into RPT_ENTRADASINC
					Select [IDFile],[IDCab],[DescCliente],[Titulo],[FechaINCusco],[FechaMPWP],[Pax],[EntCompradasMAPI],[EntPendientesMAPI],[EntCompradasWAYNA],[EntPendientesWAYNA],[EntCompradas],
					   [EntPendientes],dbo.FnDevuelveCostoINCxMAPIWAYNARAQ([IDDet],'CUZ00133',YEAR(FechaMPWP),0,@FilesConNinios) As [Tarifa],
					   (select Descripcion from MASERVICIOS Where IDServicio='CUZ00133') As [DescServicio],[EjecReservas],Cast([DatosPaxIncompletos] as int),
						[ExisteNinio],[CantNinios],Cast([FileAutorizado] as int),'CUZ00133' As [CodMapioWaynaoRaqchioMyW],[IDDet],[Estado],1 As [FlPostGenerado]
					from RPT_ENTRADASINC Where CodMapioWaynaoRaqchioMyW='CUZ00134' And IDCab=@IDCabTemp

					Delete from RPT_ENTRADASINC where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW <> 'CUZ00134' And FlPostGenerado=0
				End
			  End
			  if @RegMAPI=0 And @RegSPWY=1 
			  Begin
				Delete from RPT_ENTRADASINC where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW = 'CUZ00134' --And FlPostGenerado=0
			  End


			  Delete from RPT_ENTRADASINC where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW = 'CUZ00132' --And FlPostGenerado=0
			End
		End
		End
		--Select Count(distinct Convert(Char(10),FechaMPWP,103)) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)
		--Select Count(*) from RPT_ENTRADASINC Where IDCab=@IDCabTemp
		if Upper(@vcMPWPRQ)='MAPI'
		Begin
			If (Select Count(distinct Convert(Char(10),FechaMPWP,103)) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)) = 1 And
			   (Select Count(*) from RPT_ENTRADASINC Where IDCab=@IDCabTemp) > 1
			Begin
				if(Select Count(*) from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)) > 1
				Begin
					--Si Existe MAPI + SUPP. DE MONTAÑA
					If Exists(Select IDFile from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)
							  And CodMapioWaynaoRaqchioMyW = 'CUZ00132') And 
					   Exists(Select IDFile from RPT_ENTRADASINC Where IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)
							  And CodMapioWaynaoRaqchioMyW = 'CUZ00946')
					Begin
						Insert into RPT_ENTRADASINC
						Select [IDFile],[IDCab],[DescCliente],[Titulo],[FechaINCusco],[FechaMPWP],[Pax],[EntCompradasMAPI],[EntPendientesMAPI],[EntCompradasWAYNA],[EntPendientesWAYNA],[EntCompradas],
							   [EntPendientes],dbo.FnDevuelveCostoINCxMAPIWAYNARAQ([IDDet],'CUZ00905',YEAR(FechaMPWP),0,0) As [Tarifa],
						(select Descripcion from MASERVICIOS Where IDServicio='CUZ00905') As [DescServicio],[EjecReservas],Cast([DatosPaxIncompletos] as int),
						[ExisteNinio],[CantNinios],Cast([FileAutorizado] as int),'CUZ00905' As [CodMapioWaynaoRaqchioMyW],[IDDet],[Estado],1 As [FlPostGenerado]
						from RPT_ENTRADASINC Where CodMapioWaynaoRaqchioMyW='CUZ00132' And IDCab=@IDCabTemp And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)
					End
					Delete from RPT_ENTRADASINC where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW <> 'CUZ00133' And FlPostGenerado = 0 And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)
				End
				Else
				Begin
				    if Exists(select IDFile from RPT_ENTRADASINC where IDCab=@IDCabTemp And FlPostGenerado=0  And FileAutorizado=1)
					Begin
						Update RPT_ENTRADASINC Set FileAutorizado=1 where IDCab=@IDCabTemp
					End

					Delete from RPT_ENTRADASINC where IDCab=@IDCabTemp And CodMapioWaynaoRaqchioMyW = 'CUZ00133' And FlPostGenerado = 0 And Convert(char(10),FechaMPWP,103)=Convert(Char(10),@FechaMAPWPTemp,103)
				End
				--SELECT * from RPT_ENTRADASINC 
			End
		End

		if Upper(@vcMPWPRQ)='RAQC'
		Begin
			delete from RPT_ENTRADASINC Where CodMapioWaynaoRaqchioMyW <> 'CUZ00145' --And FlPostGenerado=0
			Goto CursorEnd
		End
	Fetch Next From curEntradasINC into @IDFileTemp,@IDCabTemp,@DescClienteTemp,@TituloTemp,@FechaINCuscoTemp,@FechaMAPWPTemp,@PaxTemp,@EntCompradasMAPITemp,@EntPendientesMAPITemp,@EntCompradasWAYNATemp,@EntPendientesWAYNATemp,@EntCompradas,
										@ntPendientes,@TarifaTemp,@DescServicioTemp,@EjecReservasTemp,@DatosPaxIncompletosTemp,@ExisteNinioTemp,@CantidadNiniosTemp,@FileAutorizadoTemp,@CodMapioWaynaoRaqchioMyWTemp,@IDDetTemp,@EstadoTemp,@FlPostGeneradoTemp
	End
End
CursorEnd:
Close curEntradasINC
DealLocate curEntradasINC

--select * from RPT_ENTRADASINC
If Upper(@vcMPWPRQ)='MAPI'
Begin
	Delete from RPT_ENTRADASINC where CodMapioWaynaoRaqchioMyW = 'CUZ00133' Or CodMapioWaynaoRaqchioMyW='CUZ00145'
End

If Upper(@vcMPWPRQ)= 'WAYNA'
Begin
	Delete from RPT_ENTRADASINC where CodMapioWaynaoRaqchioMyW='CUZ00132' or CodMapioWaynaoRaqchioMyW = 'CUZ00905' Or CodMapioWaynaoRaqchioMyW='CUZ00946' Or CodMapioWaynaoRaqchioMyW='CUZ00145'
End

Select * from RPT_ENTRADASINC
Where --(FechaINCusco Between @FechaIni And @FechaFin Or Convert(Char(10),@FechaIni,103)='01/01/1900')
(DatosPaxIncompletos=@EstadoDatosPax Or @EstadoDatosPax=0)
And (@FilesConNinios=0 Or ExisteNinio=@FilesConNinios)
And (@FilesAutorizados=0 Or FileAutorizado=@FilesAutorizados)
And ((Estado=@Estado Or ltrim(rtrim(@Estado))='') Or (@Estado='PENDIENTE' And Estado='PARCIAL'))
Order By FechaINCusco
--option (recompile)
End
