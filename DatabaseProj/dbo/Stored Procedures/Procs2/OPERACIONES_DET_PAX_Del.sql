﻿Create Procedure dbo.OPERACIONES_DET_PAX_Del
	@IDOperacion_Det int, 
	@IDPax int
As
	Set NoCount On
	
	Delete From OPERACIONES_DET_PAX
	Where IDOperacion_Det=@IDOperacion_Det And IDPax=@IDPax
