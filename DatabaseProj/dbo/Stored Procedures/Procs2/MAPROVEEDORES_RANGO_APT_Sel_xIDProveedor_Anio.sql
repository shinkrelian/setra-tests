﻿create Procedure [dbo].[MAPROVEEDORES_RANGO_APT_Sel_xIDProveedor_Anio]	
    @IDProveedor int,
	@NuAnio CHAR(4)
As
	Set NoCount On
	
	Select Correlativo,NuAnio, PaxDesde, PaxHasta, Monto 
	From MAPROVEEDORES_RANGO_APT
	Where IDProveedor=@IDProveedor and NuAnio=@NuAnio
	Order by PaxDesde
