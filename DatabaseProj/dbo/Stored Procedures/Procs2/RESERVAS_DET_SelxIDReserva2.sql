﻿--HL-20121203-Reemplazando rd.Servicio por cd.Servicio                
--HL-20121203-Agregando rd.NroPax, rd.NroLiberados                
--HL-20130327-Agregando rd.IDServicio_Det                
--HLF-20130510-Agregando Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det And sd.PlanAlimenticio=0              
--y And cd.IncGuia=0              
--HLF-20130522-Agregando sd.ConAlojamiento, s.Dias            
--HLF-20130703-Agregando sd.PlanAlimenticio, p.IDTipoProv          
--HLF-20130815-Cambiando Order by a rd.IDServicio_Det, rd.Dia        
--HLF-20130926-Cambiando Order by a rd.Servicio, rd.Dia        
--HLF-20131030-Cambiando Order by a rd.Servicio, sd.Tipo ,rd.Dia        
--HLF-20131230-Agregando paramtro @AcomodoEspecial y And cd.AcomodoEspecial=@AcomodoEspecial   
--HLF-20140109-Quitando paramtro @AcomodoEspecial y And cd.AcomodoEspecial=@AcomodoEspecial   
--HLF-20140110-Agregando campo AcomodoEspecial
--JRF-20160321-Agregando .. And rd.Anulado=0 And r.Anulado=0
CREATE Procedure dbo.RESERVAS_DET_SelxIDReserva2                 
 @IDReserva int
As                
 Set NoCount On                
              
 Select rd.IDReserva_Det, rd.Dia, rd.FechaOut,                 
 rd.Servicio,                 
 rd.IDServicio_Det,                
 rd.IDDet, rd.Noches, rd.NroPax, rd.NroLiberados               
 ,r.IDProveedor,            
 sd.ConAlojamiento, s.Dias              
 ,sd.PlanAlimenticio, p.IDTipoProv          
 ,cd.AcomodoEspecial
 From RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva                 
 Inner Join COTIDET cd On rd.IDDet=cd.IDDET And cd.IncGuia=0    
 Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det And sd.PlanAlimenticio=0              
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio            
 Left Join MASERVICIOS p On s.IDProveedor=p.IDProveedor          
 Where rd.IDReserva = @IDReserva And rd.Anulado=0 And r.Anulado=0
 Order by   
 rd.Servicio, sd.Tipo ,rd.Dia        
 --rd.Dia, cd.Item        
