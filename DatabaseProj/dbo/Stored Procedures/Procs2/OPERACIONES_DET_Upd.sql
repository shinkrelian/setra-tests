﻿--JRF-20130904-Nuevo Campo IDGuiaProveedor   
--JRF-20140228-Aumento de Longitud para las columnas con numeric(12,4)    
--JRF-20150221-Agregar el campo @FlServicioNoShow
--HLF-20150619-Agregar el campo FlServicioNoCobrado
--HLF-20150622-Agregar el campo SsTotalGenAntNoCobrado,QtCantidadAPagarAntNoCobrado
--PPMG-20151014-CantidadAPagarEditado, NetoHabEditado, IgvHabEditado
CREATE Procedure [dbo].[OPERACIONES_DET_Upd]        
@IDOperacion_Det int,        
@Dia smalldatetime,        
@FechaOut smalldatetime,        
@Noches tinyint,        
@Especial bit,        
@MotivoEspecial varchar(200),        
@RutaDocSustento varchar(200),        
@Desayuno bit,        
@Lonche bit,        
@Almuerzo bit,        
@Cena bit,        
@IDDetTransferOri int,        
@IDDetTransferDes int,        
@TipoTransporte char(1),        
@IDUbigeoOri char(6),        
@IDUbigeoDes char(6),        
@IDIdioma varchar(12),        
@NroPax smallint,        
@NroLiberados smallint,        
@Tipo_Lib char(1),        
@Cantidad tinyint,        
@CostoReal numeric(12,4),        
@CostoRealAnt numeric (12,4),        
@CostoLiberado numeric (12,4),        
@Margen numeric(12,4),        
@MargenAplicado numeric (6,2),        
@MargenAplicadoAnt numeric (6,2),        
@MargenLiberado numeric (12,4),        
@Total numeric (8,2),        
@TotalOrig numeric (12,4),        
@CostoRealImpto numeric(12,4),        
@CostoLiberadoImpto numeric(12,4),        
@MargenImpto numeric(12,4),        
@MargenLiberadoImpto numeric(12,4),        
@TotImpto numeric (12,4),        
@Servicio varchar(max),        
@IDVoucher int,          
@ObservVoucher varchar(max),        
@ObservBiblia varchar(max),        
@ObservInterno varchar(max),        
@VerObserVoucher bit ,      
@VerObserBiblia bit,      
@IDGuiaProveedor char(6), 
@FlServicioNoShow bit, 
@CantidadAPagar tinyint,
@NetoHab numeric(12,4),
@IgvHab numeric(12,4),
@TotalHab numeric(12,4),
@NetoGen numeric(12,4),
@IgvGen numeric(12,4),
@TotalGen numeric(12,4),
@IDMoneda char(3),
@FlServicioNoCobrado bit,
@SSTotalGenAntNoCobrado numeric(12,4)=0,
@QtCantidadAPagarAntNoCobrado tinyint=0,
@UserMod char (4),
@CantidadAPagarEditado bit,
@NetoHabEditado bit,
@IgvHabEditado bit
As        
	Set NoCount On        
         
	 Update OPERACIONES_DET        
	  Set         
		Dia= @Dia        
		,DiaNombre= DATENAME(dw,@Dia)        
		,FechaOut=Case When @FechaOut='01/01/1900' Then Null Else @FechaOut End        
		,Noches=Case When @Noches=0 Then Null Else @Noches End        
		,Especial= @Especial        
		,MotivoEspecial= Case When ltrim(rtrim(@MotivoEspecial))='' Then Null Else @MotivoEspecial End        
		,RutaDocSustento= Case When ltrim(rtrim(@RutaDocSustento))='' Then Null Else @RutaDocSustento End        
		,Desayuno= @Desayuno         
		,Lonche= @Lonche        
		,Almuerzo= @Almuerzo        
		,Cena= @Cena        
                   
		,IDDetTransferOri= Case When LTRIM(rtrim(@IDDetTransferOri))='' Then Null Else @IDDetTransferOri End        
		,IDDetTransferDes= Case When ltrim(rtrim(@IDDetTransferDes))='' Then Null Else @IDDetTransferDes End        
                   
		,TipoTransporte= Case When LTRIM(rtrim(@TipoTransporte))='' Then Null Else @TipoTransporte End        
		,IDUbigeoOri= Case When ltrim(rtrim(@IDUbigeoOri))='' Then Null Else @IDUbigeoOri End        
		,IDUbigeoDes= Case When LTRIM(rtrim(@IDUbigeoDes))='' Then Null Else @IDUbigeoDes End        
                   
		,IDIdioma=@IDIdioma        
                        
		,NroPax= @NroPax        
		,NroLiberados= Case When ltrim(rtrim(@NroLiberados))='' Then Null Else @NroLiberados End        
		,Tipo_Lib= Case When ltrim(rtrim(@Tipo_Lib))='' Then Null Else @Tipo_Lib End        
		,Cantidad=@Cantidad        
		,CostoReal= @CostoReal        
		,CostoRealAnt= Case When LTRIM(rtrim(@CostoRealAnt))='' Then Null Else @CostoRealAnt End        
		,CostoLiberado= @CostoLiberado        
		,Margen= @Margen        
		,MargenAplicado= @MargenAplicado        
		,MargenAplicadoAnt= Case When LTRIM(rtrim(@MargenAplicadoAnt))='' Then Null Else @MargenAplicadoAnt End        
		,MargenLiberado= @MargenLiberado        
		,Total= @Total        
		,TotalOrig= @TotalOrig        
		,CostoRealImpto=@CostoRealImpto        
		,CostoLiberadoImpto=@CostoLiberadoImpto        
		,MargenImpto=@MargenImpto        
		,MargenLiberadoImpto=@MargenLiberadoImpto        
		,TotImpto= @TotImpto                   
		,Servicio= @Servicio        
		,IDVoucher=@IDVoucher        
		--,FechaRecordatorio=Case When @FechaRecordatorio='01/01/1900' Then Null Else @FechaRecordatorio End        
		--,Recordatorio=Case When LTRIM(rtrim(@Recordatorio))='' Then Null Else @Recordatorio End        
		,ObservVoucher=Case When LTRIM(rtrim(@ObservVoucher))='' Then Null Else @ObservVoucher End        
		,ObservBiblia=Case When LTRIM(rtrim(@ObservBiblia))='' Then Null Else @ObservBiblia End        
		,ObservInterno=Case When LTRIM(rtrim(@ObservInterno))='' Then Null Else @ObservInterno End        
		,VerObserVoucher =@VerObserVoucher                 
		,VerObserBiblia =@VerObserBiblia     
		,IDGuiaProveedor = Case When ltrim(rtrim(@IDGuiaProveedor))='' Then Null Else @IDGuiaProveedor End
		,FlServicioNoShow =@FlServicioNoShow
		,CantidadAPagar=@CantidadAPagar
		,NetoHab=@NetoHab
		,IgvHab=@IgvHab
		,TotalHab=@TotalHab
		,NetoGen=@NetoGen
		,IgvGen=@IgvGen
		,TotalGen=@TotalGen
		,IDMoneda=@IDMoneda
		,FlServicioNoCobrado=@FlServicioNoCobrado
		,SSTotalGenAntNoCobrado=Case When @SSTotalGenAntNoCobrado=0 Then Null Else @SSTotalGenAntNoCobrado End
		,QtCantidadAPagarAntNoCobrado=Case When @QtCantidadAPagarAntNoCobrado=0 Then Null Else @QtCantidadAPagarAntNoCobrado End
		,UserMod= @UserMod        
		,FecMod= GetDate()        
		,FeUpdateRowReservas=Null
		,CantidadAPagarEditado = @CantidadAPagarEditado
		,NetoHabEditado = @NetoHabEditado
		,IgvHabEditado = @IgvHabEditado
      Where IDOperacion_Det = @IDOperacion_Det        

