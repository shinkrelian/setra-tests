﻿
Create Procedure dbo.OPERACIONES_UpdEstadoxProveedor
	@IDCab	int,
	@IDProveedor char(6),
	@Estado char(2),
	@UserMod char(4)
As 
	Set NoCount On  

	Update OPERACIONES_DET_DETSERVICIOS Set  
	IDEstadoSolicitud=@Estado,  
	UserMod=@UserMod,   
	FecMod=GETDATE()  
	Where IDOperacion_Det IN (SELECT IDOperacion_Det FROM OPERACIONES_DET
		Where IDOperacion IN (SELECT IDOperacion FROM OPERACIONES WHERE IDCab=@IDCab And IDProveedor=@IDProveedor)) 
	
	
