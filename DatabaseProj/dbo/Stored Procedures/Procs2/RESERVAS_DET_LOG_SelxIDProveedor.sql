﻿
--HLF-20130718-Agregando campo l.IDReserva_Det   
--HLF-20130719-Agregando And (ltrim(rtrim(@Accion))='' Or l.Accion=@Accion)
CREATE Procedure [dbo].[RESERVAS_DET_LOG_SelxIDProveedor]        
 @IDCab int,        
 @IDProveedor char(6),
 @Accion char(1)
As        
 Set NoCount On        
         
 Select l.IDDET, l.Accion, p.IDTipoProv, sd.IDServicio_Det, sd.PlanAlimenticio, sd.ConAlojamiento,l.IDReserva_Det       
 From RESERVAS_DET_LOG l       
 Left Join MASERVICIOS_DET sd On l.IDServicio_Det=sd.IDServicio_Det      
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio      
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor      
 Where Atendido=0 And p.IDProveedor=@IDProveedor        
 And Accion<>' '        
 And l.IDReserva In (Select IDReserva From RESERVAS Where IDCab = @IDCab)  
 And (ltrim(rtrim(@Accion))='' Or l.Accion=@Accion)
