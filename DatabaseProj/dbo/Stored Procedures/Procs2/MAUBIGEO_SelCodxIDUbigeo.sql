﻿Create Procedure dbo.MAUBIGEO_SelCodxIDUbigeo
@IDUbigeo char(6),
@pCod char(3) output
As
	Set NoCount On
	Set @pCod =''
	If Exists(select Codigo from MAUBIGEO where IDubigeo=@IDUbigeo and Codigo is not null)
		Set @pCod = (select Isnull(Codigo,'') from MAUBIGEO where IDubigeo=@IDUbigeo)
