﻿CREATE Procedure dbo.ORDENPAGO_Upd_Estado_Multiple
@IDOrdPag int,
@CoEstado char(2),
@UserMod char(4)
As  
Set NoCount On  
BEGIN

	Update ORDENPAGO
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  

where IDOrdPag=@IDOrdPag 
END;
