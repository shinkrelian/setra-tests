﻿--JRF-20160423- Nuevo campo FlCreadoDsdAddIn
CREATE Procedure [dbo].[PEDIDO_InsxAddIn]
	@CoCliente char(6) ,
	@FePedido smalldatetime ,
	@CoEjeVta char(4),
	@TxTitulo varchar(100),
	@CoEstado char(2),
	@UserMod char(4),
	@pNuPedido varchar(20) output
As
	Set Nocount on
	Declare @NuPedInt int
	Execute dbo.Correlativo_SelOutput 'PEDIDO',1,10,@NuPedInt output        
	Declare @NuPedido char(9)
	Exec NuPedido_SelOutput @FePedido,@NuPedido output

	Insert Into PEDIDO 
	(NuPedInt,
	NuPedido,
	CoCliente,
	FePedido,
	CoEjeVta,
	TxTitulo,
	CoEstado,
	FlCreadoDsdAddIn,
	UserMod)
	Values 
	(@NuPedInt,
	@NuPedido,
	@CoCliente,
	@FePedido,
	@CoEjeVta,
	@TxTitulo,
	@CoEstado,
	1,
	@UserMod)

	Set @pNuPedido=cast(@NuPedInt as nvarchar(10))+'|'+@NuPedido
