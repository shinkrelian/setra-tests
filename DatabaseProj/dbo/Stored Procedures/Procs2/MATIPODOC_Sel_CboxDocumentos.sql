﻿Create Procedure [dbo].[MATIPODOC_Sel_CboxDocumentos]
@bTodos bit  
As    
 Set NoCount On    
 select * from   
 (  
 Select '' As IDtipodoc, '<TODOS>' as Descripcion,0 as Ord  
 Union   
 Select IDtipodoc,Descripcion,FlOrdenPreLiq as Ord
 From MATIPODOC 
 where FlOrdenPreLiq is not null   
 ) As X  
 Where (@bTodos=1 Or Ord <> 0)  
 Order by X.Ord
