﻿CREATE Procedure dbo.MASERVICIOS_DET_Sel_TodosxIDServicioAnioTipo
	@IDServicio char(8),
	@Anio	char(4),
	@Tipo	varchar(7)
As
	
	Set NoCount On
	
	Select * From MASERVICIOS_DET 
	Where IDServicio=@IDServicio And Anio=@Anio And Tipo=@Tipo
	Order by Correlativo
