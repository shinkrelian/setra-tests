﻿--HLF-20130225-Agregando grabacion de TipoCambio,IDMoneda,IDServicio         
--HLF-20130923-Agregando And Anulado=0      
--HLF-20131107-Agregando And Estado<>'XL'     
--HLF-20140723-SD.CoMoneda  
--JRF-20150409-Para los costos del TC, se calculan desde Ventas.
--HLF-20150630-QtPax ,od.NroPax+ISNULL(od.NroLiberados,0) as PaxLiberados
--JRF-20151130-..SubQuery Viaticos (CoMoneda)
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_InsxIDProveedor          
 @IDCab int,           
 @IDProveedor char(6),          
 @UserMod char(4)          
As          
 Set Nocount On           
          
 Insert Into OPERACIONES_DET_DETSERVICIOS          
 (IDOperacion_Det,IDServicio_Det, IDServicio,        
 IDServicio_Det_V_Cot,          
 CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,          
 SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,          
 MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,          
 STMargenImpto,TotImpto,        
 TipoCambio,IDMoneda,QtPax,
 UserMod)            
 Select X.IDOperacion_Det, x.IDServicio_Det, x.IDServicio ,X.IDServicio_Det_V,
 Case When x.NuViaticoTC > 0 then IsNull(x.TotalViatico,0) else X.CostoReal*X.NroPax End,
 X.CostoLiberado*X.NroPax,X.Margen*X.NroPax,X.MargenAplicado,X.MargenLiberado*X.NroPax,X.Total*X.NroPax,          
 X.SSCR*X.NroPax,X.SSCL*X.NroPax,X.SSMargen*X.NroPax,X.SSMargenLiberado*X.NroPax,X.SSTotal*X.NroPax,X.STCR*X.NroPax,          
 X.STMargen*X.NroPax,X.STTotal*X.NroPax,X.CostoRealImpto*X.NroPax,X.CostoLiberadoImpto*X.NroPax,          
 X.MargenImpto*X.NroPax,X.MargenLiberadoImpto*X.NroPax,X.SSCRImpto*X.NroPax,X.SSCLImpto*X.NroPax,          
 X.SSMargenImpto*X.NroPax,X.SSMargenLiberadoImpto*X.NroPax,X.STCRImpto*X.NroPax,X.STMargenImpto*X.NroPax,          
 X.TotImpto*X.NroPax,        
 X.TC,X.CoMoneda,PaxLiberados,
 X.UserMod
 from (
 Select Distinct od.IDOperacion_Det, rs.IDServicio_Det, sd.IDServicio,           
 --(Select IDServicio_Det_V From MASERVICIOS_DET Where IDServicio_Det=rs.IDServicio_Det),          
 sd.IDServicio_Det_V,        
 --rs.CostoReal*od.NroPax,rs.CostoLiberado*od.NroPax,rs.Margen*od.NroPax,rs.MargenAplicado,rs.MargenLiberado*od.NroPax,rs.Total*od.NroPax,          
 --rs.SSCR*od.NroPax,rs.SSCL*od.NroPax,rs.SSMargen*od.NroPax,rs.SSMargenLiberado*od.NroPax,rs.SSTotal*od.NroPax,rs.STCR*od.NroPax,          
 --rs.STMargen*od.NroPax,rs.STTotal*od.NroPax,rs.CostoRealImpto*od.NroPax,rs.CostoLiberadoImpto*od.NroPax,          
 --rs.MargenImpto*od.NroPax,rs.MargenLiberadoImpto*od.NroPax,rs.SSCRImpto*od.NroPax,rs.SSCLImpto*od.NroPax,          
 --rs.SSMargenImpto*od.NroPax,rs.SSMargenLiberadoImpto*od.NroPax,rs.STCRImpto*od.NroPax,rs.STMargenImpto*od.NroPax,          
 --rs.TotImpto*od.NroPax,    
 rs.CostoReal,rs.CostoLiberado,rs.Margen,rs.MargenAplicado,rs.MargenLiberado,rs.Total,          
 rs.SSCR,rs.SSCL,rs.SSMargen,rs.SSMargenLiberado,rs.SSTotal,rs.STCR,          
 rs.STMargen,rs.STTotal,rs.CostoRealImpto,rs.CostoLiberadoImpto,          
 rs.MargenImpto,rs.MargenLiberadoImpto,rs.SSCRImpto,rs.SSCLImpto,          
 rs.SSMargenImpto,rs.SSMargenLiberadoImpto,rs.STCRImpto,rs.STMargenImpto,          
 rs.TotImpto,       
 Case When rd.FlServicioTC=1 Then IsNull((select ctp.SsTipCam from COTICAB_TIPOSCAMBIO ctp where ctp.IDCab=@IDCab and ctp.CoMoneda=sd.CoMoneda),sd.TC) else sd.TC End as TC
 ,   
 --Case When sd.TC IS NULL Then 'USD' Else 'SOL' End,         
 --SD.CoMoneda,  
  Case When IsNull(rd.NuViaticoTC,0) > 0 then 
	Case When (select vt.CostoSOL from VIATICOS_TOURCONDUCTOR vt where vt.NuViaticoTC= IsNull(rd.NuViaticoTC,0)) > 0 then 'SOL' Else 'USD' End
  else sd.CoMoneda End as CoMoneda,
 --Case When rd.FlServicioTC=1 Then 1 Else od.NroPax End as NroPax,
 od.NroPax,od.NroPax+ISNULL(od.NroLiberados,0) as PaxLiberados,
 @UserMod As UserMod,IsNull(rd.NuViaticoTC,0) as NuViaticoTC,
 (select vt.CostoSOL + vt.CostoUSD from VIATICOS_TOURCONDUCTOR vt where vt.NuViaticoTC= IsNull(rd.NuViaticoTC,0)) as TotalViatico
 From RESERVAS_DETSERVICIOS rs Inner Join RESERVAS_DET rd On rs.IDReserva_Det=rd.IDReserva_Det          
 Inner Join RESERVAS r On r.IDReserva=rd.IDReserva And r.IDCab=@IDCab       
 And r.IDProveedor=@IDProveedor And r.Anulado=0  And r.Estado<>'XL'     
 Inner Join OPERACIONES_DET od On rd.IDReserva_Det=od.IDReserva_Det          
 Inner Join MASERVICIOS_DET sd On rs.IDServicio_Det=sd.IDServicio_Det          
 Where rs.Total>0  
 ) As X
