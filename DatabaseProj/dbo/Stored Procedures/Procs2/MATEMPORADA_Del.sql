﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Del]
	@IDTemporada int
AS
BEGIN
	Set NoCount On

	Delete From MATEMPORADA
	Where IDTemporada = @IDTemporada
END
