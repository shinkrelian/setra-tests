﻿--JRF-20160108-betty.sterin@setours.com' X eugenia.rosas@setours.com'
--JRF-20160408-victoria.ontanilla' x betty.sterin@setours.com
--HLF-20160414-Exec MAPROVEEDORES_SelEsDelExterior @IDProveedor, @DelExterior output		If @DelExterior=1...
--JRF-20160420-cpago.exterior@setours.com [Contabilidad no influye en el flujo del envio]
CREATE Procedure dbo.MAPROVEEDORES_SelRazSocialCoUbigeo_OficinaOutput  
	@IDProveedor char(6),  
	@pvcRazonSocial varchar(100) output  
As  
	Set Nocount on  
   
	Declare @CoUbigeo_Oficina char(6)=(Select isnull(CoUbigeo_Oficina,'') From MAPROVEEDORES   
	Where IDProveedor=@IDProveedor)   
   
	--Set @pvcRazonSocial ='SETOURS S.A.|pagos1@setours.com'  
   
	If @CoUbigeo_Oficina='000113'--Bueno Aires  
		Begin  
		Select @pvcRazonSocial=RazonSocial From MAPROVEEDORES Where IDProveedor='002315'  
		--Set @pvcRazonSocial+='SETOURS S.A.|eugenia.rosas@setours.com'  
		Set @pvcRazonSocial+='|victoria.ontanilla@setours.com;'-- cpago.exterior@setours.com;'
		End  
	Else  
		Begin  
		If @CoUbigeo_Oficina='000110'--Santiago  
			Begin  
			Select @pvcRazonSocial=RazonSocial From MAPROVEEDORES Where IDProveedor='002317'         
			Set @pvcRazonSocial+='|marcela.barriga@setours.com;'-- cpago.exterior@setours.com;'
			End  
		Else  
			Begin
			Set @pvcRazonSocial ='SETOURS S.A.|pagos1@setours.com'   
			--Declare @DelExterior bit
			--Exec MAPROVEEDORES_SelEsDelExterior @IDProveedor, @DelExterior output
			--If @DelExterior=1
			--	Set @pvcRazonSocial ='SETOURS S.A.|cpago.exterior@setours.com'
			End
		End    
