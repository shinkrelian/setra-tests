﻿
CREATE PROCEDURE [dbo].[MATIPOCAMBIO_MONEDAS_Sel_Rpt]
	@FechaInicio	smalldatetime,
	@FechaFin		smalldatetime,
	@CoMoneda	char(3)
AS
BEGIN
	Set NoCount On
	
	Select CoMoneda, Descripcion AS NombreMoneda, Simbolo, Fecha, ValCompra, ValVenta
	From MATIPOCAMBIO_MONEDAS MM INNER JOIN MAMONEDAS M ON MM.CoMoneda = M.IDMoneda
	Where CoMoneda = COALESCE(@CoMoneda,CoMoneda) AND
	(Fecha between @FechaInicio and  @FechaFin or @FechaInicio='01/01/1900' or @FechaFin='01/01/1900')
	order by CoMoneda, Fecha
END
