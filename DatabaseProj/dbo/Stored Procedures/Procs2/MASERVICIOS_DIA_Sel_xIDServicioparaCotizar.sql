﻿--HLF-20130403-Cambiando uo.Codigo por uo.DC y ud.Codigo por ud.DC    
--JRF-20140609-Cambiando el Select Para obtener la información de los dias reales.  
--HLF-20141021-From MASERVICIOS_ALIMENTACION_DIA al Left Join MASERVICIOS_DIA sd On       
--Where al.IDServicio=@IDServicio And (sd.IDIdioma=@IDIdioma Or ltrim(rtrim(@IDIdioma))='' )
--If Exists ...
CREATE Procedure [dbo].[MASERVICIOS_DIA_Sel_xIDServicioparaCotizar]
 @IDServicio char(8)      
As      
 Set NoCount On      
 Declare @IDIdioma varchar(12)=Isnull((Select Top 1 IDIdioma From MASERVICIOS_DIA       
 Where IDServicio=@IDServicio),'')  

 If Exists(select IDIdioma from MASERVICIOS_DIA where IDIdioma = @IDIdioma and IDServicio = @IDServicio)
	 
	 Select       
	 sd.DescripCorta, al.Desayuno, al.Lonche, al.Almuerzo, al.Cena, al.IDUbigeoOri, al.IDUbigeoDes,      
	 --uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,        
	 uo.Descripcion+'          |'+IsNull(uo.DC,'') as DescUbigeoOri,        
	 --ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes      
	 ud.Descripcion+'          |'+IsNull(ud.DC,'') as DescUbigeoDes      
	 --From MASERVICIOS_DIA sd Left Join MASERVICIOS_ALIMENTACION_DIA al On         
	 From MASERVICIOS_DIA sd Left Join MASERVICIOS_ALIMENTACION_DIA al On       
	 sd.IDServicio=al.IDServicio And sd.Dia=al.Dia      
	 Left Join MAUBIGEO uo On al.IDUbigeoOri=uo.IDubigeo      
	 Left Join MAUBIGEO ud On al.IDUbigeoDes=ud.IDubigeo        
	 --Where sd.IDServicio=@IDServicio And  sd.IDIdioma=@IDIdioma    
	 Where sd.IDServicio=@IDServicio And (sd.IDIdioma=@IDIdioma Or ltrim(rtrim(@IDIdioma))='' )
	 Order by sd.Dia   
	 
 Else
 
	 Select       
	 sd.DescripCorta, al.Desayuno, al.Lonche, al.Almuerzo, al.Cena, al.IDUbigeoOri, al.IDUbigeoDes,      
	 --uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,        
	 uo.Descripcion+'          |'+IsNull(uo.DC,'') as DescUbigeoOri,        
	 --ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes      
	 ud.Descripcion+'          |'+IsNull(ud.DC,'') as DescUbigeoDes      
	 --From MASERVICIOS_DIA sd Left Join MASERVICIOS_ALIMENTACION_DIA al On         
	 From MASERVICIOS_ALIMENTACION_DIA al Left Join MASERVICIOS_DIA sd On       
	 sd.IDServicio=al.IDServicio And sd.Dia=al.Dia      
	 Left Join MAUBIGEO uo On al.IDUbigeoOri=uo.IDubigeo      
	 Left Join MAUBIGEO ud On al.IDUbigeoDes=ud.IDubigeo        
	 --Where sd.IDServicio=@IDServicio And  sd.IDIdioma=@IDIdioma    
	 Where al.IDServicio=@IDServicio And (sd.IDIdioma=@IDIdioma Or ltrim(rtrim(@IDIdioma))='' )
	 Order by sd.Dia       
