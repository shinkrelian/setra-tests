﻿--HLF-20140123-Modificando Left Join RESERVAS_DET rdd On Rd.IDReserva_Det=rdd.IDReserva_Det     
--a Inner Join RESERVAS_DET rdd On Rd.IDReserva_Det=rdd.IDReserva_Det And rdd.Anulado=0    
--JRF-20140303-Se muestra el IDGuiaProgramado('') de OPERACIONES_DET  
--HLF-20140321-Agregando And rdd.Anulado=1
--HLF-20140321-Comentando And rdd.Anulado=1
--HLF-20150528-Order by ..., DescServicioDet
--HLF-20150703-Case When dbo.FnExisteNuevaTarifa(rd.IDOperacion_Det)=0 then...
--JRF-20151123-Agregar NuViaticoTC..
CREATE Procedure [dbo].[OPERACIONES_DET_SelxIDCabIDProveedor]
 @IDCab int,                                      
 @IDProveedor char(6)      
As                                                            
 Set NoCount On
                                                
 Select Case When sd.CoTipoCostoTC In('ALM','CEN') then 1 else IsNull(rdd.NuViaticoTC,0) End as NuViaticoTC, rd.IDReserva_Det,                                                
 Rd.IDOperacion_Det,                                                
 Rv.IDReserva as IDReserva,                                                
 rd.IDOperacion,                                                 
 rv.IDFile,                                                
 0 as IDDet,                                                
 rd.Item,                                                
 Rd.Dia,                                                  
 Case When Pr.IDTipoProv='001' Then                                                
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                 
 Else                                                
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                  
 End as DiaFormat,                                                    
 IsNull(Rd.FechaOut,'') as FechaOut,                                                
 IsNull(upper(substring(DATENAME(dw,Rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,Rd.FechaOut,103)/*+' '+ substring(convert(varchar,Rd.FechaOut,108),1,5)*/)),'') as FechaOut,                                                   
 Case When Pr.IDTipoProv='001' Then                                                 
 ''                                                
 Else                                                 
  convert(varchar(5),rd.Dia,108)                                                
 End as Hora,                                                   
 Ub.Descripcion as Ubigeo,                                                
  CAST(rd.NroPax AS VARCHAR(3))+                      
 Case When isnull(rd.NroLiberados,0)=0 then '' else '+'+CAST(rd.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,      
      
 Isnull(Rd.Noches,0) as Noches,                                                 
 Pr.IDTipoProv,                             
 --Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,                                               
 s.Dias,                                                
 rv.IDProveedor,                                                
 Pr.NombreCorto as Proveedor,                                                
 ISNull(pr.Email3,'') CorreoProveedor ,                                                
 Ub.IDPais,                                                
 Rd.IDubigeo,                                                
 Rd.Cantidad,                                  
                             
 Rd.NroPax,                          
 Rd.IDServicio,                                                
 Rd.IDServicio_Det,                                                
 Rd.Servicio As DescServicioDet,                                                    
                                                 
 --Rd.Servicio As DescServicioDetBup,                      
 Case When pr.IDTipoProv='001' or pr.IDTipoProv='002' Then  --hoteles o restaurantes                
 sd.Descripcion                                         
 Else                                    
 s.Descripcion                                        
 End As DescServicioDetBup,                                         
                                 
                                                 
 Rd.Especial,Rd.MotivoEspecial,rd.RutaDocSustento,Rd.Desayuno,Rd.Lonche,Rd.Almuerzo,Rd.Cena,                                                
 Rd.Transfer,Rd.IDDetTransferOri,Rd.IDDetTransferDes,0 as CamaMat,Rd.TipoTransporte,                                                
 Rd.IDUbigeoOri,                                                
 uo.Descripcion+'      |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                  
 Rd.IDUbigeoDes,                                                
 ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                                  
 rd.IDIdioma,                    
 Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,                                                                        
 sd.Anio,sd.Tipo,                                                 
 Isnull(sd.DetaTipo,'') As DetaTipo ,                                      
                           
 Rd.CantidadAPagar,                                  
                             
 Rd.NroLiberados,Rd.Tipo_Lib,Rd.CostoRealAnt,Rd.Margen,                                                
 Rd.MargenAplicado,Rd.MargenAplicadoAnt,Rd.MargenLiberado,Rd.TotalOrig,Rd.CostoRealImpto,Rd.CostoLiberadoImpto                                          
 ,Rd.MargenImpto,Rd.MargenLiberadoImpto,Rd.TotImpto,                                                
                                                 
                                                 
 '0' as IDDetRel ,                        
                         
 Rd.Total,                                                
 Case When Cast(Rd.IDVoucher as varchar(10))='0' Then '' Else Cast(Rd.IDVoucher as varchar(10)) End as IDVoucher,                                                
 '0' as IDReserva_DetCopia ,'0' as IDReserva_Det_Rel,                                          
                                                 
 '' as IDEmailEdit, '' as IDEmailNew, '' as IDEmailRef,                                                
 --IsNull(Rd.FechaRecordatorio,'01/01/1900') as FechaRecordatorio, IsNull(Rd.Recordatorio,'') as Recordatorio,                                               
 --IsNull(Rd.ObservVoucher,'') as ObservVoucher,               
 IsNull(vo.ObservVoucher,'') as ObservVoucher,               
 IsNull(Rd.ObservBiblia,'') as ObservBiblia,                                                 
 IsNull(Rd.ObservInterno,'') as ObservInterno  , 
 rdd.CapacidadHab as CapacidadHab , rdd.EsMatrimonial As EsMatrimonial   ,                                           
 sd.PlanAlimenticio ,  cd.IncGuia,                                    
 vo.ServExportac As chkServicioExportacion , Rd.VerObserVoucher ,Rd.VerObserBiblia  ,                                                   
                                   
 Rd.CostoReal, Rd.CostoLiberado,                                  
  Rd.NetoHab,Rd.IgvHab,Rd.TotalHab,Rd.NetoGen,Rd.IgvGen,                        
  isnull(rdd.IDMoneda,'') as IDMoneda,                        
  isnull(mon.Simbolo,'') as SimboloMoneda,                        
  isnull(sd.TC,0) as TipoCambio,          
  Rd.TotalGen                                   
 ,            
 --Case When Pr.IDTipoOper = 'I' Then             
 --Isnull((select distinct IDProveedor_Prg             
 --from OPERACIONES_DET_DETSERVICIOS odd2 Left Join MAPROVEEDORES pr On odd2.IDProveedor_Prg = pr.IDProveedor            
 --where pr.IDTipoProv = '005' and odd2.IDOperacion_Det = rd.IDOperacion_Det),'')   
 --Else Isnull(Rd.IDGuiaProveedor,'') End   
  --Isnull(Rd.IDGuiaProveedor,'')  
  '' As IDGuiaProveedor,            
 0 As DescBus,                                  
 rd.idTipoOC                         
 ,0 as InactivoDet                                        
 ,0 AS ServHotelAlimentPuro,                      
 sd.ConAlojamiento, cd.SSCR,                   dbo.FnPNRxIDCabxDia(@IDCab,Rd.Dia) as PNR,
 Case When dbo.FnExisteNuevaTarifa(rd.IDOperacion_Det)=0 then
	'OK'
 Else
	'PD'
 End as CoEstadoTarifa, '' as NoEstadoTarifa
 From OPERACIONES Rv                                                 
 Left Join OPERACIONES_DET Rd On Rd.IDOperacion=Rv.IDOperacion                                                
 Left Join MAUBIGEO Ub On Rd.IDubigeo=Ub.IDubigeo                                                       
 Left Join MAUBIGEO uo On Rd.IDUbigeoOri=uo.IDubigeo                                                  
 Left Join MAUBIGEO ud On Rd.IDUbigeoDes=ud.IDubigeo                                                   
 Left Join MAPROVEEDORES Pr On  Pr.IDProveedor= Rv.IDProveedor                                                      
 Left Join MASERVICIOS_DET sd On  Rd.IDServicio_Det=sd.IDServicio_Det                                                
 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio                                                
 Inner Join VOUCHER_OPERACIONES vo On Rd.IDVoucher= vo.IDVoucher                                              
 Inner Join RESERVAS_DET rdd On Rd.IDReserva_Det=rdd.IDReserva_Det --And rdd.Anulado=1                                 
 Left Join COTIDET cd On rdd.IDDet=cd.IDDET                                    
 Left Join MAMONEDAS mon on Rdd.IDMoneda=mon.IDMoneda                        
 Where Rv.IDCab=@IDCab And Rv.IDProveedor=@IDProveedor --And rdd.Anulado=0      
--Order By Rd.IDVoucher desc,Rd.Item Asc                                             
Order by IDReserva, Dia, rd.FechaOut, CapacidadHab, DescServicioDet
