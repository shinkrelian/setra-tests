﻿Create Procedure dbo.ORDENPAGO_CORREOFILE_SelExistsxIDOrdPagxNuCorreo
@NuCorreo int,
@IDOrdPag int,
@pExisteOrden bit output
as
Set NoCount On
set @pExisteOrden = 0
If Exists(select NuCorreo from ORDENPAGO_CORREOFILE where NuCorreo=@NuCorreo and IDOrdPag=@IDOrdPag)
	Set @pExisteOrden = 1
