﻿--HLF-20140131-Agregar    Else  
  --If (Select Max(PaxHasta) From MASERVICIOS_DET_COSTOS      
--JRF-20141211-Agregar and (@NroPax+@NroLiberados)>=40
--PPMG20160404-Se agrego el filtro para Zicasso
--JRF-20160418-Set @pExiste = IsNull(@pExiste,0)
CREATE PROCEDURE [dbo].[MASERVICIOS_DET_COSTOS_Sel_ExisteRangoOutput]    
    @IDServicio_Det int,      
    @NroPax smallint,    
    @NroLiberados smallint,    
    @pExiste bit Output    
AS
BEGIN
	Declare @Servicio_Det_Zicasso CHAR(4) = 'ZIC6',
			@IDProveedor_Zicasso char(6) = '003557',
			@IDServicio_Zicasso char(8)	= 'LAX00002'
	Set Nocount On
    
	IF EXISTS(SELECT IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
			  WHERE TipoUsoSistema = @Servicio_Det_Zicasso AND IDServicio_Det=@IDServicio_Det
			  AND S.IDProveedor = @IDProveedor_Zicasso AND D.IDServicio = @IDServicio_Zicasso)
		Set @pExiste=1
	ELSE
	BEGIN
		If Exists(Select Monto From MASERVICIOS_DET_COSTOS      
				  Where IDServicio_Det=@IDServicio_Det And      
						(@NroPax + @NroLiberados) >= PaxDesde And (@NroPax + @NroLiberados) <= PaxHasta)
			Set @pExiste=1
		Else
			If (Select Max(PaxHasta) From MASERVICIOS_DET_COSTOS      
				Where IDServicio_Det=@IDServicio_Det)>=40 and (@NroPax+@NroLiberados)>=40
				Set @pExiste=1
	END

	Set @pExiste = IsNull(@pExiste,0)
END
