﻿
Create Procedure dbo.MASERVICIOS_DET_Sel_CodTarifa	
	@IDServicio_Det int
As
	Set Nocount On
	
	Select CodTarifa From MASERVICIOS_DET
	Where IDServicio_Det = @IDServicio_Det
