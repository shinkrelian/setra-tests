﻿
--JRF-20140719-Agregar la Edad.  
--HLF-20140724-Cambios a cp.FecNacimiento cuando es nulo
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Sel_List_PresupuestosPax    
@IDOperacion_Det int    
As    
Set NoCount On    
select odp.IDPax,Case When ltrim(rtrim(cp.TxTituloAcademico))<>'---' then cp.TxTituloAcademico Else cp.Titulo End     
+' '+ cp.Apellidos+', '+ cp.Nombres+
--cast(DATEDIFF(YEAR,cp.FecNacimiento,getdate())as varchar(5)) 
Case When isnull(cp.FecNacimiento,'01/01/1900') = '01/01/1900' Then 
	''
Else
	' - '+Cast(DATEDIFF(YEAR,cp.FecNacimiento,getdate()) as varchar(5))
End
as Nombre,  
Case When isnull(cp.FecNacimiento,'01/01/1900') = '01/01/1900' Then 
	0
Else
	DATEDIFF(YEAR,cp.FecNacimiento,getdate()) 
End As Edad    
from OPERACIONES_DET_PAX odp Left Join COTIPAX cp On odp.IDPax = cp.IDPax    
where IDOperacion_Det = @IDOperacion_Det    
Order by cp.Orden    

