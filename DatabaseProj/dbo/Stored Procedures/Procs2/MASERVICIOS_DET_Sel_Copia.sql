﻿
--HLF-20121015-Agregando el campo d.IDServicio_DetCopia
--HLF-20121019-Agregando el campo d.IDServicio_Det_V
CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_Copia]
	@IDServicio char(8),
	@AnioNue	char(4)
As
	
	Set NoCount On
	
	Select d.Anio, d.Tipo, d.Descripcion, d.Monto_sgl, d.Monto_dbl, d.Monto_tri,
	d.IDServicio_Det, d.IDServicio_DetCopia, d.IDServicio_Det_V
	From MASERVICIOS_DET d 
	Left Join MASERVICIOS_DET d2 On d.IDServicio_Det_V=d2.IDServicio_Det
	Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio
	Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
	Where d.IDServicio=@IDServicio And d.Anio=@AnioNue And Not d.IDServicio_DetCopia is Null
	--And d.IDServicio_Det Not In (Select IDServicio_Det From MASERVICIOS_DET_COSTOS)
	Order by Anio, Tipo, d.Correlativo

