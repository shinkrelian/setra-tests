﻿--JRF-20130618-Validar el Activo
CREATE Procedure dbo.MAPROVEEDORES_Sel_Cbo    
 @IDTipoProv char(3)    
As    
 Set NoCount On    
 
 Select IDProveedor, NombreCorto as DescProveedor From MAPROVEEDORES    
 Where (IDTipoProv=@IDTipoProv OR LTRIM(RTRIM(@IDTipoProv))='')  
 and Activo = 'A'
 Order by DescProveedor   
