﻿--JRF-20140522-Cambio de Tabla - Obtener datos de la tabla [MACORREOSBIBLIAPROVEEDOR]
CREATE Procedure dbo.MAPROVEEDOR_SelIDProveedorxMail  
@eMail varchar(150),  
@pIDProveedor char(6) output  
As  
Set NoCount On  
set @pIDProveedor = (select top(1) IDProveedor from MACORREOSBIBLIAPROVEEDOR   
      where lower(Isnull(Correo,'')) = lower(ltrim(rtrim(@eMail))))  
Set @pIDProveedor = ISNULL(@pIDProveedor,'')  
