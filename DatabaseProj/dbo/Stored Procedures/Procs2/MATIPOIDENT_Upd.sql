﻿
--MLL-20140522- Nuevo Campo CodPeruRail
CREATE Procedure [dbo].[MATIPOIDENT_Upd]
	@IDIdentidad char(3),
	@Descripcion text,	
	@UserMod char(4) ,
	@CodPeruRail char(3) 

As

	Set NoCount On

	Update MATIPOIDENT
	Set Descripcion=@Descripcion,	
	UserMod=@UserMod,
	FecMod=GETDATE() ,
	CodPeruRail= @CodPeruRail
	Where IDIdentidad=@IDIdentidad
