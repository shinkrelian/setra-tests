﻿
  
Create Procedure dbo.PRESUPUESTO_SOBRE_DET_DelxIDDet  
 @IDDet int  
As  
 Set Nocount On  
 
 DELETE FROM PRESUPUESTO_SOBRE_DET Where IDOperacion_Det  in  
 (Select IDOperacion_Det  OPERACIONES_DET   
  WHERE IDOperacion_Det in   
    (Select IDOperacion_Det FROM OPERACIONES_DET WHERE IDReserva_Det In   
     (Select IDReserva_Det FROM RESERVAS_DET Where IDDet=@IDDet))  )
       
