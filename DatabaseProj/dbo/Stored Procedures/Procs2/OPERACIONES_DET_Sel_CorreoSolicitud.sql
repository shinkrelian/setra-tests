﻿Create Procedure dbo.OPERACIONES_DET_Sel_CorreoSolicitud
	@IDOperacion_Det	int
As
	Set Nocount On      
	
	Select upper(substring(DATENAME(dw,od.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,od.Dia,103))) As Dia,      
    substring(convert(varchar,od.Dia,108),1,5) As Hora,      
	od.Servicio as DescServicio, s.IDTipoServ, od.NroPax,
	c.IDIdioma, od.ObservBiblia
	From OPERACIONES_DET od 	
	Left Join MASERVICIOS s On od.IDServicio=s.IDServicio
	Left Join OPERACIONES o On o.IDOperacion=od.IDOperacion
	Left Join COTICAB c On o.IDCab=c.IDCAB
	Where od.IDOperacion_Det=@IDOperacion_Det
	
	
	
