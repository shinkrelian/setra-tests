﻿CREATE Procedure [dbo].[MASERVICIOS_Sel_Copia]
	@IDServicioCopia	char(8)
As
	Set NoCount On
	
	Select s.FecMod, s.Descripcion,
	p.NombreCorto  as Proveedor,	
	s.IDServicio	 
	From MASERVICIOS s 
	Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor		
	Where s.IDServicioCopia=@IDServicioCopia
	Order by s.FecMod
