﻿--JRF-20130603-Permitir 0 Días.  
--HLF-20130712-Agregando campo IDPolitica
--JRF-20151201-Agregando Filtro por Anio
CREATE Procedure dbo.MAPOLITICASPROVEEDORES_SelPrepagosxCantidad  
	@IDProveedor char(6),  
	@Cantidad tinyint,  
	@FechaInicio smalldatetime  
As    
	Set Nocount On    
    Declare @Anio char(4)=year(@FechaInicio)

	Select DATEADD(d,X.Dias*(-1),@FechaInicio) As FechaPago,X.Porcentaje,X.Monto, X.IDPolitica
	from (  
	select PrePag1_Dias As Dias ,PrePag1_Porc as Porcentaje,PrePag1_Mont as Monto, IDPolitica  
	from MAPOLITICAPROVEEDORES  
	where Anio=@Anio And IDProveedor = @IDProveedor and @Cantidad between DefinDia and DefinHasta  
	union  
	select PrePag2_Dias As Dias,PrePag2_Porc as Porcentaje,PrePag2_Mont as Monto, IDPolitica
	from MAPOLITICAPROVEEDORES  
	where Anio=@Anio And IDProveedor = @IDProveedor and @Cantidad between DefinDia and DefinHasta  
	union  
	select Saldo_Dias As Dias,Saldo_Porc as Porcentaje,Saldo_Mont as Monto, IDPolitica
	from MAPOLITICAPROVEEDORES   
	where Anio=@Anio And IDProveedor = @IDProveedor and @Cantidad between DefinDia and DefinHasta  
	) As X  
	where X.Dias <> 0 Or (X.Monto+X.Porcentaje) <> 0  
	Order By X.Dias  
