﻿CREATE Procedure dbo.RESERVAS_DET_SelListAcomodoPaxxIDDet  
@IDReserva int  
As  
Set NoCount On  
select rd.IDDet,Cast(rd.Cantidad as varchar(5))+' '+rd.Servicio as TituloCab,rdp.IDPax,  
px.Orden as NroOrd,0 as IDHabit,Case when ltrim(rtrim(px.TxTituloAcademico)) <> '---' then px.TxTituloAcademico else px.Titulo End as Titulo,  
px.Nombres+' '+px.Apellidos as NombrePax,SUBSTRING(id.Descripcion,1,3) as DescIdentidad,Case When px.FlTC = 1 then 'TC' else '' End as DescTC,  
Isnull(px.ObservEspecial,'') as ObservEspecial,ltrim(rtrim(REPLACE(rd.Servicio,sd.Descripcion,''))) as ACC,ISNULL(px.NumIdentidad,'') as NumIdentidad,  
Case When px.FecNacimiento is Null Then '' Else Convert(Varchar(10),px.FecNacimiento,103) End as FechNacimiento,IsNull(ub.Codigo,'') as NacionalidadPax  
from RESERVAS_DET_PAX rdp Left Join RESERVAS_DET rd On rdp.IDReserva_Det=rd.IDReserva_Det  
Left Join COTIPAX px On rdp.IDPax=px.IDPax Left Join MATIPOIDENT id On px.IDIdentidad = id.IDIdentidad  
Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det  
Left Join MAUBIGEO ub On px.IDNacionalidad=ub.IDubigeo  
where rd.Anulado=0 and rd.IDReserva=@IDReserva  
