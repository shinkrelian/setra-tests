﻿Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Sel_ExisteEnOtraOficina
@IDCab int,
@IDProveedor char(6),
@IDProvProgram char(6),
@pExiste bit output
As
Set NoCount On
Set @pExiste = 0
Select Top(1) @pExiste = 1 
from OPERACIONES_DET_DETSERVICIOS ods Inner Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det And ods.IDServicio=od.IDServicio
Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
Where o.IDCab =@IDCab And ods.IDProveedor_Prg=@IDProvProgram And o.IDProveedor<>@IDProveedor And ods.Activo=1
