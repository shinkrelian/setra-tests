﻿--HLF-20130920-Agregando And Anulado=0  
--HLF-20130923-Agregando Update Correlativo  
--HLF-20131107-Agregando And Estado<>'XL' 
CREATE Procedure dbo.OPERACIONES_InsxIDProveedor    
 @IDCab int,    
 @IDProveedor char(6),    
 @UserMod char(4)    
As    
 Set Nocount On    
  
 Declare @IDOperacion int    
 Execute dbo.Correlativo_SelOutput 'OPERACIONES',0,10,@IDOperacion output    
  
 Insert Into OPERACIONES    
 (IDOperacion,IDReserva,IDCab,IDFile,IDProveedor,UserMod)    
 Select      
 ((row_number() over (order by IDReserva))-1) + @IDOperacion,     
 IDReserva,IDCab,IDFile,IDProveedor,@UserMod    
 From RESERVAS Where IDCab=@IDCab And IDProveedor=@IDProveedor  
 And Anulado=0 And Estado<>'XL' 
   
   
 Update Correlativo Set Correlativo=@IDOperacion+(@@ROWCOUNT-1) Where Tabla='OPERACIONES'  
 
