﻿--JHD-20150821-No actualizar estado a Aceptado por BD
CREATE Procedure dbo.PRESUPUESTO_SOBRE_UpdSsPendientexDoc_USD      
 @NuPreSob int,      
 @SsTotalOrig numeric(9,2),      
 @UserMod char(4)      
As      
 Set NoCount On      
 Update PRESUPUESTO_SOBRE      
  Set SsSaldo_USD=SsSaldo_USD-@SsTotalOrig,      
   UserMod=@UserMod,      
   FecMod=GETDATE()      
 where NuPreSob=@NuPreSob
       
 Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from PRESUPUESTO_SOBRE where NuPreSob=@NuPreSob)
 Declare @SsSaldoPend_USD numeric(8,2)=(select Isnull(SsSaldo_USD,0) from PRESUPUESTO_SOBRE where NuPreSob=@NuPreSob)
  
 --if @SsSaldoPend+@SsSaldoPend_USD = 0      
 -- Update PRESUPUESTO_SOBRE set CoEstadoFormaEgreso='AC' where NuPreSob=@NuPreSob
