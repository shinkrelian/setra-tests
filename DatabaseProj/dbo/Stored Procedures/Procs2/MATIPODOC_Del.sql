﻿Create Procedure [dbo].[MATIPODOC_Del]
	@IDtipodoc char(3)
As
	
	Set NoCount On
	
	Delete From MATIPODOC
    Where
           IDtipodoc=@IDtipodoc
