﻿--JRF-20150414-Aumentar tamaño @Monto...
--JHD-20150910-SsMontoTotal_SinDetrac=@monto-isnull(SSDetraccion,0),
CREATE Procedure dbo.ORDENPAGO_Upd_Saldo_Total
@IDOrdPag int ,
@UserMod char(4)
as  
BEGIN

	Declare @Monto numeric(12,2)

	set @Monto=isnull((select SUM(total) from ORDENPAGO_DET where IDOrdPag=@IDOrdPag),0)

	Update ORDENPAGO set SsSaldo= @Monto,
	SsMontoTotal=@Monto,
	SsMontoTotal_SinDetrac=@monto-isnull(SSDetraccion,0),
	UserMod=@UserMod,
	FecMod=GETDATE()
	where IDOrdPag=@IDOrdPag  
END;
