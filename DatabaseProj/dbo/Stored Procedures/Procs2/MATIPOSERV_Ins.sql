﻿CREATE Procedure [dbo].[MATIPOSERV_Ins]	
            @Descripcion varchar(60),
            @CtaCon varchar(12),
            @Ingles varchar(60),
            @SubDiario varchar(4),
            @CtaVta varchar(12),
            @SdVta varchar(4),
            @IdFactu char(5),
            @UserMod char(4),
            @pIDservicio char(3) Output
As
	
	Set NoCount On	
	Declare @IDservicio char(3)
	Exec Correlativo_SelOutput 'MATIPOSERV',1,3,@IDservicio output
		
	INSERT INTO MATIPOSERV
           ([IDservicio]
           ,[Descripcion]
           ,[CtaCon]
           ,[Ingles]
           ,[SubDiario]
           ,[CtaVta]
           ,[SdVta]
           ,[IdFactu]
           ,[UserMod])
     VALUES
           (@IDservicio,
            @Descripcion ,
            Case When ltrim(rtrim(@CtaCon))='' Then Null Else @CtaCon End,
            Case When ltrim(rtrim(@Ingles))='' Then Null Else @Ingles End,
            Case When ltrim(rtrim(@SubDiario))='' Then Null Else @SubDiario End,
            Case When ltrim(rtrim(@CtaVta))='' Then Null Else @CtaVta End,
            Case When ltrim(rtrim(@SdVta))='' Then Null Else @SdVta End,
            Case When ltrim(rtrim(@IdFactu))='' Then Null Else @IdFactu End,
            @UserMod)
  
    Set @pIDservicio=@IDservicio
