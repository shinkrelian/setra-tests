﻿/*
alter table [dbo].[MASERVICIOS_DET_COSTOS] alter column [Monto] numeric(15,4) not null;
go
*/

--alter table MAPLANCUENTAS_CENTROCOSTOS add TxDefinicion varchar(255) null;
--go


--
--JRF-20131209-Aumentando el valor de los decimales a (numeric(10, 4))
--JHD-20150602-Aumentando el valor de los decimales a (numeric(15, 4))
CREATE Procedure [dbo].[MASERVICIOS_DET_COSTOS_Ins]   
    @IDServicio_Det int,  
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(15,4),  
    @UserMod char(4)  
As  
 Set NoCount On  
 Declare @Correlativo tinyint=(Select IsNull(MAX(Correlativo),0)+1   
  From MASERVICIOS_DET_COSTOS Where IDServicio_Det=@IDServicio_Det)  
   
 INSERT INTO MASERVICIOS_DET_COSTOS  
           ([Correlativo]  
           ,[IDServicio_Det]  
           ,[PaxDesde]  
           ,[PaxHasta]  
           ,[Monto]  
           ,[UserMod]  
           )  
     VALUES  
           (@Correlativo,  
            @IDServicio_Det,  
            @PaxDesde,  
            @PaxHasta,  
            @Monto,  
            @UserMod  
            )  
