﻿--create
create Procedure PRESUPUESTO_Rpt_Consolidado_Cusco
 @IDCAB int,            
 
 @FeDetRango1 smalldatetime='01/01/1900',        
 @FeDetRango2 smalldatetime='01/01/1900'
 
As            
begin
 Set Nocount On            
 
 declare @presupuestos as table
 (
 NuPreSob int not null
 )

 insert into @presupuestos
 Select Distinct NuPreSob
 from (        
 Select prd.NuPreSob, pr.FePreSob, pr.IDCab ,cc.IDFile, cc.Titulo as Referencia,  
 IsNull(prd.CoPrvPrg,'') as CoPrvPrg, IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescGuia, uope.Nombre as EjecOper,            
 ufin.Nombre as EjecFinan, pr.IDProveedor ,pr.CoEstado,             
 Case pr.CoEstado            
  When 'NV' Then 'NUEVO'            
  When 'PG' Then 'GENERADO(Operaciones)'            
  When 'EL' Then 'ELABORADO'            
  When 'EN' Then 'ENTREGADO'            
  When 'EJ' Then 'EJECUTADO'            
  When 'AN' Then 'ANULADO'            
 End as DescEstado,           
 cc.FecInicio as FechaIn,   
 IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE pr.NuPreSob=PD2.NuPreSob And IsNull(IDMoneda,'')='USD'),0) As ImporteDOL,
 IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE pr.NuPreSob=PD2.NuPreSob And IsNull(IDMoneda,'')='SOL'),0) As ImporteSOL,
 cc.NroPax+IsNull(cc.NroLiberados,0) as NroPax,
 Case When cc.NroPax+IsNull(cc.NroLiberados,0) <= 8 then 'F' Else 'G' End as Categoria,
 IsNull(pr.IDPax,0) as IDPax
 From PRESUPUESTO_SOBRE pr Inner Join COTICAB cc On pr.IDCab=cc.IDCAB            
 Inner Join PRESUPUESTO_SOBRE_DET prd On pr.NuPreSob=prd.NuPreSob      
 --Left Join MAPROVEEDORES p On pr.CoPrvGui=p.IDProveedor            
 Left Join MAPROVEEDORES p On prd.CoPrvPrg=p.IDProveedor            
 Left Join MAUSUARIOS uope On pr.CoEjeOpe=uope.IDUsuario            
 Left Join MAUSUARIOS ufin On pr.CoEjeFin=ufin.IDUsuario            
 Left Join COTIPAX cp On pr.IDPax=cp.IDPax
 Where (cc.IDCAB = @IDCAB Or @IDCAB=0)            
  And pr.IDProveedor='000544'           
)as X        
 where 

 (convert(char(10),@FeDetRango1,103)='01/01/1900'         
 Or X.FechaIn between @FeDetRango1 and @FeDetRango2)      
 --Order by X.FePreSob            
 Order by NuPreSob--X.FechaIn  

 --select *  from @presupuestos

select /*soles*/Compras_Efectivo=isnull((

select sum(isnull(pd.SsTotal,0)) from
PRESUPUESTO_SOBRE_DET pd 
left join MASERVICIOS_DET sd on pd.IDServicio_Det=sd.IDServicio_Det
where NuPreSob in (select nupresob from @presupuestos) and pd.CoPago='EFE' and pd.IDMoneda='SOL'
and sd.IDServicio not in ('CUZ00137')

),0),


Compras_Efectivo_Dol=isnull((select isnull(sum(pd.SsTotal),0) from
PRESUPUESTO_SOBRE_DET pd 
left join MASERVICIOS_DET sd on pd.IDServicio_Det=sd.IDServicio_Det
where NuPreSob in (select nupresob from @presupuestos) and pd.CoPago='EFE' and pd.IDMoneda='USD'
and sd.IDServicio not in ('CUZ00137')),0),

	   /*soles*/BTG_Entero=ISNULL((select sum(isnull((pd.QtPax*pd.SsPreUni),0)) from PRESUPUESTO_SOBRE_DET pd inner join maservicios_det sd on pd.IDServicio_Det=sd.IDServicio_Det and pd.NuPreSob in (select nupresob from @presupuestos) and sd.IDServicio='CUZ00179'  where sd.CoMoneda='SOL'),0),
	   BTG_Entero_Dolares=ISNULL((select sum(isnull((pd.QtPax*pd.SsPreUni),0)) from PRESUPUESTO_SOBRE_DET pd inner join maservicios_det sd on pd.IDServicio_Det=sd.IDServicio_Det and pd.NuPreSob in (select nupresob from @presupuestos) and sd.IDServicio='CUZ00179' where sd.CoMoneda='USD'),0),
	   /*soles*/MAPI_Soles=ISNULL((select isnull(sum(pd.SsTotal),0) from
							PRESUPUESTO_SOBRE_DET pd 
							left join MASERVICIOS_DET sd on pd.IDServicio_Det=sd.IDServicio_Det
							where NuPreSob in (select nupresob from @presupuestos) and pd.CoPago='EFE' and pd.IDMoneda='SOL'
							and sd.IDServicio  in ('CUZ00135')),0),
	   /*dolares*/MAPI_Dolares=isnull((select isnull(sum(pd.SsTotal),0) from
							PRESUPUESTO_SOBRE_DET pd 
							left join MASERVICIOS_DET sd on pd.IDServicio_Det=sd.IDServicio_Det
							where NuPreSob in (select nupresob from @presupuestos) and pd.CoPago='EFE' and pd.IDMoneda='USD'
							and sd.IDServicio  in ('CUZ00135')),0)
	,Compras_Girar='Flor de Maria Montes Huallpamayta'   
	,BTG_Entero_Girar='Cosituc'   
	,MAPI_Soles_Girar='Consettur Machupicchu SAC'   
	,MAPI_Dolares_Girar='Consettur Machupicchu SAC' 
end;  
