﻿--JHD-20150506-Se cambió la tabla y la consulta
CREATE Procedure dbo.MAVEHICULOSPROGRAMACION_Sel_Cbo
As
Set NoCount On
select 0 as NuVehiculoProg,'<TODOS>' As NoUnidadProg
Union
select NuVehiculo as NuVehiculoProg, NoVehiculoCitado +' ('+cast(isnull(QtDe,0) as varchar(2)) +' - '+cast(isnull(QtHasta,0) as varchar(2))  +') Pax  - Cap. '+ case when Cast(isnull(QtCapacidad,'') as Varchar(2)) IN ('','0') then 'TBA' ELSE Cast(isnull(QtCapacidad,'') as Varchar(2)) END +''+ case when CoUbigeo is null then '' else ' - '+ (select descripcion from MAUBIGEO where IDubigeo=CoUbigeo) end as NoUnidadProg
from [dbo].[MAVEHICULOS_TRANSPORTE]
--select NuVehiculoProg,NoUnidadProg 
--from MAVEHICULOSPROGRAMACION
