﻿CREATE Procedure [dbo].[MATIPOPROVEEDOR_Sel_PideIdiomaOutput]
	@IDTipoProv	char(3),
	@pPideIdioma	bit Output
As

	Set Nocount On
	
	Declare @PideIdioma bit=(Select PideIdioma From MATIPOPROVEEDOR Where IDTipoProv=@IDTipoProv)
		
	Set @pPideIdioma = IsNull(@PideIdioma,0)
