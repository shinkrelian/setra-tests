﻿

--PPMG-20151105-Se agrego el insertar MASERVICIOS_DET_DESC_APT
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_Ins_Copiar]  
 @IDServicio char(8),    
 @Anio char(4),  
 @Tipo varchar(7),   
 @PorcentDcto numeric(5,2),  
 @UserMod char(4)  
AS
BEGIN
	Set NoCount On
 
	INSERT INTO MASERVICIOS_DESC_APT(IDServicio, IDTemporada, DescripcionAlterna, UserMod, FecMod)
	Select dNue.IDServicio_Det, c.IDTemporada, c.DescripcionAlterna, @UserMod, GETDATE()
	From MASERVICIOS_DET_DESC_APT c Inner Join MASERVICIOS_DET d On c.IDServicio_Det=d.IDServicio_Det
	And d.IDServicio=@IDServicio And
	(d.Anio=@Anio Or ltrim(rtrim(@Anio))='') And
	(d.Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')
	Inner Join MASERVICIOS_DET dNue On d.IDServicio_Det=dNue.IDServicio_DetCopia
	Where Not dNue.IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET_DESC_APT)
	GROUP BY dNue.IDServicio_Det, c.IDTemporada, c.DescripcionAlterna

	Insert Into MASERVICIOS_DET_RANGO_APT(Correlativo, IDServicio_Det, PaxDesde, PaxHasta, Monto, UserMod, IDTemporada)
	Select c.Correlativo, dNue.IDServicio_Det , c.PaxDesde, c.PaxHasta,   
	--c.Monto,   
	Case When @PorcentDcto=0 Then c.Monto Else c.Monto*(1-(@PorcentDcto/100)) End,  
	@UserMod, c.IDTemporada
	From MASERVICIOS_DET_RANGO_APT c Inner Join MASERVICIOS_DET d On c.IDServicio_Det=d.IDServicio_Det  
	And d.IDServicio=@IDServicio And   
	(d.Anio=@Anio Or ltrim(rtrim(@Anio))='') And   
	(d.Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')   
	Inner Join MASERVICIOS_DET dNue On d.IDServicio_Det=dNue.IDServicio_DetCopia   
	--And d.IDServicio_DetCopia Is Null   
	Where --d.IDServicio_DetCopia Is Null And   
	Not dNue.IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET_RANGO_APT)
END
