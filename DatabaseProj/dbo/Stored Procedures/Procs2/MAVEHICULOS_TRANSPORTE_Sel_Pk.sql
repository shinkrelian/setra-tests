﻿
--create
create Procedure [dbo].[MAVEHICULOS_TRANSPORTE_Sel_Pk]
@NuVehiculo smallint
As
	Set NoCount On
	Select t.*,g.IDPais from MAVEHICULOS_TRANSPORTE t
	Left Join MAUBIGEO g On t.CoUbigeo=g.IDubigeo    
	where NuVehiculo = @NuVehiculo
