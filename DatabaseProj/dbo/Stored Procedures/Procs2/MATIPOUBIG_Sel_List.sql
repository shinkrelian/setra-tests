﻿CREATE Procedure [dbo].[MATIPOUBIG_Sel_List]
	@Descripcion varchar(20)
As	
	Set NoCount On

	Select  Descripcion as IdTipoUbigeo
	From MATIPOUBIG Where (Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='')
