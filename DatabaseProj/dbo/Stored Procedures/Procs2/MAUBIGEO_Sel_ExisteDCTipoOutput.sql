﻿Create Procedure MAUBIGEO_Sel_ExisteDCTipoOutput
@DC char(3),
@TipoUbig varchar(20),
@pblnExiste bit output
As

	Set NoCount On
	if exists(select IDubigeo from MAUBIGEO Where TipoUbig=@TipoUbig And DC=@DC)
		set @pblnExiste = 1
	else
		set @pblnExiste = 0
