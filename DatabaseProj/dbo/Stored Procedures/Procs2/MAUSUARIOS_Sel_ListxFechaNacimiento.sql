﻿--JRF-20151022-...CoCeCos
--JRF-20160312--...u.CoUbicacionOficina
--JHD-20160331-,IsNull(u.Celular_Trabajo,'') as Celular_Trabajo
CREATE Procedure [dbo].[MAUSUARIOS_Sel_ListxFechaNacimiento]
@FechaNacimiento smalldatetime
As
Set NoCount On
Set Language spanish
Declare @RutaArchivoFoto varchar(200)=(select Replace(NoRutaUserPhoto,'\\wb1','wb1:81') from PARAMETRO)
Select u.IDUsuario,u.Usuario,ROW_NUMBER()Over (Order By u.IDUsuario)-1 as Index2,
	   Replace(@RutaArchivoFoto+u.Usuario+'\'+IsNull(u.NoArchivoFoto,''),'\','/') as Ruta,
	   Year(Getdate())-Year(IsNull(u.FechaNac,getdate())) as Edad,
	   u.Nombre,IsNull(u.TxApellidos,'') as Apellidos,
	   Case When u.CoSexo='F' Then 'female' else 'male' End as Genero,
	   IsNull(Substring(convert(char(10),u.FechaNac,103),1,2)+' de '+
			  DATENAME(MONTH,u.FechaNac)--+' de '+
			  --Substring(convert(char(10),FechaNac,103),7,10)
	   ,'') As Cumpleanios,
	   IsNull(Substring(convert(char(10),u.FecIngreso,103),1,2)+' de '+
			  DATENAME(MONTH,u.FecIngreso)+' de '+
			  Substring(convert(char(10),u.FecIngreso,103),7,10)
	   ,'') As FechaIngreso,
	   IsNull(u.CoEmpresa,0) as CoEmpresa,IsNull(e.TxEmpresa,'') as TxEmpresa,
	   u.IdArea,r.NoArea,
	   IsNull(u.CoCargo,0) as CoCargo,IsNull(c.TxCargo,'') as TxCargo,
	   IsNull(u.CoUserJefe,'') as CoUserJefe,u.Correo,IsNull(u.Telefono,'') as Telefono,
	   IsNull(u.Celular,'') as Celular,IsNull(u.NuAnexo,'') as Anexo,IsNull(u.TxLema,'') as TxLema,u.Activo,u.FecMod,
	   isnull(u.NuOrdenArea,0) as NuOrdenArea,IsNull(ujefe.IDUsuario,'') as IDUsuarioJefe,
	   IsNull(ujefe.Nombre,'') as NombreJefe,IsNuLL(ujefe.TxApellidos,'') As TxApellidosJefe,IsNull(ujefe.Correo,'') as MailJefe,
	   IsNull(u.CoMarcador,'') as CoMarcador,r.CoCeCos,u.CoUbicacionOficina
	   ,IsNull(u.Celular_Trabajo,'') as Celular_Trabajo
from MAUSUARIOS u Left Join RH_AREA r On u.IdArea=r.CoArea
Left Join MAEMPRESA e On u.CoEmpresa=e.CoEmpresa
Left Join RH_CARGO c On u.CoCargo=c.CoCargo
Left Join MAUSUARIOS ujefe On u.CoUserJefe=ujefe.IDUsuario
Where  u.Activo='A' And u.FlTrabajador_Activo=1 And u.CoEmpresa=1
And Convert(char(5),u.FechaNac,103)=Convert(char(5),@FechaNacimiento,103)
Order By NuOrdenArea asc
