﻿CREATE Procedure [dbo].[MAUSUARIOS_Sel_IdOutput]
	@Usuario varchar(20),
	@pExiste	bit	Output
As
	Set NoCount On
	
	If Exists(Select IDUsuario From MAUSUARIOS	WHERE	Usuario=@Usuario)
		Set @pExiste = 1
	Else
		Set @pExiste = 0
