﻿--JHD-20150721- Se agrego la condicion de los usuarios FlTrabajador_Activo=1
--JHD-20150721- Se agrego apellidos a los nombres de los ejecutivos FlTrabajador_Activo=1
--PPMG-20160126- and FlTrabajador_Activo=1 al select de CUSCO 2
--PPMG-20160126-,'SETOURS LIMA (HEADQUARTERS)' as Proveedor,--UPPER(NombreCorto) as Proveedor,
--PPMG-20160126-, 'emergency@setours.com' as Email,
--JHD-20160314- where IDUsuario = Case When o.IDProveedor='002315' then '0174' else '0132' End) 
--JHD-20160314- where IDUsuario = Case When o.IDProveedor='002315' then '0174' else '0132' End) else pr.Celular End as Celular1,   
--JRF-20160405- Cambiar nombre de ejecutivo por Texto de Sebastian ('Emergency Number 24 hrs.')
--JRF-20160406- Cambiar (+51 987 52 4620) Fijo
--JRF-20160408- Declare @ExisteServBA bit = 0 ,@ExisteServCL bit = 0...
Create Procedure [dbo].[OPERACIONES_DET_SelOperadoresxIDCab_Rpt]
@IDCab int
As
BEGIN
 Set NoCount On                
 Declare @ExisteServBA bit = 0 ,@ExisteServCL bit = 0

 If Not Exists(select IDOperacion from OPERACIONES Where IDCab=@IDCab And IDProveedor='002317')
 Begin
	 Select @ExisteServCL = 1 --uPais.Descripcion,u.Descripcion ,od.*
	 from OPERACIONES_DET od Left Join MAUBIGEO u On od.IDubigeo=u.IDubigeo
	 Left Join MAUBIGEO uPais On u.IDPais=uPais.IDubigeo
	 Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
	 Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
	 Where o.IDCab=@IDCab And uPais.IDubigeo='000011' And o.IDProveedor<>'002317'
 End
 
 If Not Exists(select IDOperacion from OPERACIONES Where IDCab=@IDCab And IDProveedor='002315')
 Begin
 Select @ExisteServBA = 1 --uPais.Descripcion,u.Descripcion ,od.*
 from OPERACIONES_DET od Left Join MAUBIGEO u On od.IDubigeo=u.IDubigeo
 Left Join MAUBIGEO uPais On u.IDPais=uPais.IDubigeo
 Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
 Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
 Where o.IDCab=@IDCab And uPais.IDubigeo='000003' And o.IDProveedor<>'002315' --And p.IDTipoOper<>'I'
 End

 select Ord, Ciudad, IDProveedor, Proveedor, Telefonos, Email,
 ISNULL(Nombre1,'') AS Nombre1, ISNULL(Celular1,'') AS Celular1, ISNULL(Nombre2,'') AS Nombre2, ISNULL(Celular2,'') AS Celular2
 from(                    
select 1 As Ord,'MAIN OFFICE' as Ciudad,IDProveedor,'SETOURS LIMA (HEADQUARTERS)' as Proveedor,--UPPER(NombreCorto) as Proveedor,                
 pr.Telefono1  as Telefonos, 'emergency@setours.com' as Email,
-- CASE WHEN (select LTRIM(RTRIM(Nombre + IsNull(TxApellidos,''))) from MAUSUARIOS where IDUsuario = (select IDUsuarioOpe from COTICAB where IDCAB = @IDCab))='' THEN '	'
--ELSE (select LTRIM(RTRIM(Nombre + ' ' + IsNull(TxApellidos,''))) from MAUSUARIOS where IDUsuario = (select IDUsuarioOpe from COTICAB where IDCAB = @IDCab)) END as Nombre1,  
'Emergency Number 24 hrs.' as Nombre1,          
 --ISNULL((select '+51 '+ Celular from MAUSUARIOS where IDUsuario = (select IDUsuarioOpe from COTICAB where IDCAB = @IDCab)),'+51 987 52 4620') as Celular1,
 '+51 987 524 620' as Celular1,
 --(select Top(1) Nombre from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad Order by IDUsuario) as Nombre2,            
 (select Top(1) rtrim(ltrim(Nombre))+' '+rtrim(ltrim(TxApellidos)) from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) as Nombre2,            
 --(select Top(1) '+51 ' + Celular from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad Order by IDUsuario) as Celular2            
 (select Top(1) '+51 ' + Celular from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) as Celular2
 from MAPROVEEDORES pr                
 where IDProveedor = '001554'             
 Union                  
 select distinct 2 as Ord,'CUSCO' as Ciudad,p4.IDProveedor,upper(p4.NombreCorto) as Proveedor, p4.Telefono1 as Telefonos, '' as Email,
 rtrim(ltrim(u.Nombre))+' '+rtrim(ltrim(IsNull(u.TxApellidos,''))) as Nombre1,'+51 '+u.Celular as Celular1,
 Null as Nombre2,
 Null as Celular2     
 from OPERACIONES_DET_DETSERVICIOS odd Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det      
 Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion      
 Left Join MAPROVEEDORES p On odd.IDProveedor_Prg = p.IDProveedor      
 Left Join MAUSUARIOS u On p.IDUsuarioTrasladista = u.IDUsuario      
 Left Join MAPROVEEDORES p4 On o.IDProveedor = p4.IDProveedor      
 where o.IDCab = @IDCab and o.IDProveedor ='000544' and p.IDUsuarioTrasladista is not null       
 and IDProveedor_Prg is not null      
 Union      
 select distinct 2 as Ord,'CUSCO' As Ciudad,'000544' as IDProveedor,'SETOURS CUSCO' as Proveedor,'+51 (84) 225571' as Telefono1, '' as Email,
 rtrim(ltrim(Nombre))+' '+rtrim(ltrim(TxApellidos)) as Nombre1,    
 --hlf-20140904-I    
 '+51 '+Celular as Celular1,    
  (select rtrim(ltrim(Nombre))+' '+rtrim(ltrim(TxApellidos)) from MAUSUARIOS where IDUsuario = 
		(select IDUsuarioOpe_Cusco from COTICAB where IDCAB = @IDCab) and FlTrabajador_Activo=1) as Nombre2,          
  (select '+51 '+ Celular from MAUSUARIOS where IDUsuario = (select IDUsuarioOpe_Cusco from COTICAB where IDCAB = @IDCab) and FlTrabajador_Activo=1) as Celular2
 from MAUSUARIOS where Nivel = 'SOP'       
 and IDUbigeo = '000066' and Activo ='A' and FlTrabajador_Activo=1
 Union      
 Select 3,u.Descripcion as Ciudad,p.IDProveedor,UPPER(p.NombreCorto) as Proveedor,p.Telefono1 as Telefono1,'' As Email,
 (select Nombre +''+IsNull(TxApellidos,'') from MAUSUARIOS where IDUsuario='0174') as Nombre1,
 (select ISNull(''+Celular,'') from MAUSUARIOS where IDUsuario='0174') As Celular1, '' as Nombre2,'' As Celular2
 from MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo
 Where IDProveedor='002315' And @ExisteServBA=1
 Union      
 Select 3,u.Descripcion as Ciudad,p.IDProveedor,UPPER(p.NombreCorto) as Proveedor,p.Telefono1 as Telefono1,'' As Email,
 (select Nombre+' '+IsNull(TxApellidos,'') from MAUSUARIOS Where IDUsuario='0132') as Nombre1,
 (select '+56 9 '+Celular from MAUSUARIOS Where IDUsuario='0132') As Celular1, 
 (select Nombre+' '+IsNull(TxApellidos,'') from MAUSUARIOS Where IDUsuario='0103') as Nombre2,
 (select '+56 9 '+Celular from MAUSUARIOS Where IDUsuario='0103') As Celular2
 from MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo
 Where IDProveedor='002317' And @ExisteServCL=1
 Union
 Select Y.Ord,Y.Ciudad,Y.IDProveedor,Y.Proveedor,Y.Telefonos, Y.Email,Y.Nombre1,Y.Celular1,Y.Nombre2,Y.Celular2
 from (      
 select 3 As Ord,u.Descripcion as Ciudad,o.IDProveedor,UPPER(NombreCorto) as Proveedor,                
 pr.Telefono1  as Telefonos, '' as Email,
 Case When o.IDProveedor In ('002315','002317') Then (select rtrim(ltrim(Nombre))+' '+rtrim(ltrim(TxApellidos)) from MAUSUARIOS 
	where IDUsuario = Case When o.IDProveedor='002315' then '0174' else '0132' End) 
 else pr.NomContacto End as Nombre1,            
 Case When o.IDProveedor = '002315' Then '+54 11 9 ' else Case When o.IDProveedor='002317' then '+56 9 ' Else '' End End+
 Case When o.IDProveedor In ('002315','002317') Then (select Celular from MAUSUARIOS             
 where IDUsuario = Case When o.IDProveedor='002315' then '0174' else '0132' End) else pr.Celular End as Celular1,      
 Case When o.IDProveedor In ('002315','002317') Then (select Top(1) rtrim(ltrim(Nombre))+' '+rtrim(ltrim(TxApellidos)) from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' 
 and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) Else Null End as Nombre2,            
 Case When o.IDProveedor = '002315' Then '+54 11 9 ' else Case When o.IDProveedor='002317' then '+56 9 ' Else '' End End+
 Case When o.IDProveedor In ('002315','002317') Then 
 (select Top(1) Celular from MAUSUARIOS u where Activo = 'A' and Nivel ='SOP' and u.IDUbigeo = pr.IDCiudad and FlTrabajador_Activo=1 Order by IDUsuario) 
 else Null End as Celular2,
 Case When (select COUNT(*) from OPERACIONES_DET od where od.IDOperacion=o.IDOperacion) = (select COUNT(*) from OPERACIONES_DET od where od.IDOperacion=o.IDOperacion and od.FlServicioNoShow=1)
 Then 1 else 0 End as ItemNoShow
 from OPERACIONES o Left Join MAPROVEEDORES pr On o.IDProveedor = pr.IDProveedor            
 Left Join MAUBIGEO u On pr.IDCiudad = u.IDubigeo            
 where o.IDCab = @IDCab and pr.IDTipoProv = '003' and Not o.IDProveedor in('001554','000544')
  ) as Y
  where Y.ItemNoShow=0
 ) as X            
 Order by X.Ord  
END

