﻿Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelProvProg_Output		
	@IDCab int,
	@IDProveedorInt char(6),
	@IDProveedor_Prg char(6),
	@Dia smalldatetime,
	@pOk bit output
As
	Set Nocount On
	
	Set @pOk = 0
	
	If Exists(SELECT od.IDOperacion_Det FROM 
		OPERACIONES_DET od                
		Inner Join OPERACIONES_DET_DETSERVICIOS ods On od.IDOperacion_Det=ods.IDOperacion_Det              
		And od.IDServicio=ods.IDServicio And ods.Activo=1 
		Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab
		And o.IDProveedor=@IDProveedorInt
		Where ods.IDProveedor_Prg=@IDProveedor_Prg And 
		convert(varchar,od.Dia,103)=convert(varchar,@Dia,103)
		)
		Set @pOk=1
		
