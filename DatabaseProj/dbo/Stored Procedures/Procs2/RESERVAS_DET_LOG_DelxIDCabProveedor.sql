﻿
--HLF-20130719-Agregando And (ltrim(rtrim(@Accion))='' Or l.Accion=@Accion)  
CREATE Procedure [dbo].[RESERVAS_DET_LOG_DelxIDCabProveedor]    
 @IDCab int,    
 @IDProveedor char(6),
 @Accion	char(1)
As    
 Set Nocount On    
  
 DELETE FROM RESERVAS_DET_LOG   
 WHERE ID In (Select id FROM RESERVAS_DET_LOG l   
 Inner Join RESERVAS r On l.IDReserva=r.IDReserva And r.IDCab=@IDCab  
 Inner Join MASERVICIOS_DET sd On l.IDServicio_Det=sd.IDServicio_Det      
 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio      
 Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor And s.IDProveedor=@IDProveedor  
 WHERE Accion<>' ' )  
 And (ltrim(rtrim(@Accion))='' Or Accion=@Accion)
  