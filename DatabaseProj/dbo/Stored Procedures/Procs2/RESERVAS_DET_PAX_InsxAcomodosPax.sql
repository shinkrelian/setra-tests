﻿CREATE Procedure dbo.RESERVAS_DET_PAX_InsxAcomodosPax
	@IDCab	int,
	@IDPax	int,
	@UserMod	char(4)
As
	Set Nocount On
	
	Insert Into RESERVAS_DET_PAX
	(IDReserva_Det, IDPax, UserMod)
	
	SELECT 
	IDReserva_Det,IDPax,UserMod
	FROM
	(
	SELECT 
	rd.IDReserva_Det,ap.IDPax,@UserMod as UserMod
	FROM ACOMODOPAX_PROVEEDOR ap 
	Left Join RESERVAS_DET rd On ap.IDReserva=rd.IDReserva And rd.CapacidadHab=1
	Left Join COTIDET_PAX cp On rd.IDDet=cp.IDDet And ap.IDPax=cp.IDPax And ap.IDPax=@IDPax
	WHERE ap.IDReserva in (select IDReserva from RESERVAS where IDCab=@IDCab)
	AND ap.CapacidadHab=1
		
	Union		
	
	SELECT 
	rd.IDReserva_Det,ap.IDPax,@UserMod
	FROM ACOMODOPAX_PROVEEDOR ap 
	Left Join RESERVAS_DET rd On ap.IDReserva=rd.IDReserva And rd.CapacidadHab=2
	Left Join COTIDET_PAX cp On rd.IDDet=cp.IDDet And ap.IDPax=cp.IDPax And ap.IDPax=@IDPax
	WHERE ap.IDReserva in (select IDReserva from RESERVAS where IDCab=@IDCab)
	AND ap.CapacidadHab=2
	
	Union
	
	SELECT 
	rd.IDReserva_Det,ap.IDPax,@UserMod
	FROM ACOMODOPAX_PROVEEDOR ap 
	Left Join RESERVAS_DET rd On ap.IDReserva=rd.IDReserva And rd.CapacidadHab=3
	Left Join COTIDET_PAX cp On rd.IDDet=cp.IDDet And ap.IDPax=cp.IDPax And ap.IDPax=@IDPax
	WHERE ap.IDReserva in (select IDReserva from RESERVAS where IDCab=@IDCab)
	AND ap.CapacidadHab=3
	) as X

	WHERE Not Cast(IDReserva_Det as varchar)+' '+Cast(IDPax as varchar)
		In (Select Cast(IDReserva_Det as varchar)+' '+Cast(IDPax as varchar) 
		From RESERVAS_DET_PAX)
	
