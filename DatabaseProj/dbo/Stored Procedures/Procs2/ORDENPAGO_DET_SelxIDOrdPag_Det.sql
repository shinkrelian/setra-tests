﻿CREATE Procedure dbo.ORDENPAGO_DET_SelxIDOrdPag_Det
@IDOrdPag_Det int
As
	Set NoCount On
	SELECT [IDOrdPag_Det]
      ,[IDOrdPag]
      ,[IDReserva_Det]
      ,[IDServicio_Det]
      ,[NroPax]
      ,[NroLiberados]
      ,[Cantidad]
      ,[Noches]
      ,[Total]
  FROM [dbo].[ORDENPAGO_DET]
  WHERE IDOrdPag_Det = @IDOrdPag_Det
