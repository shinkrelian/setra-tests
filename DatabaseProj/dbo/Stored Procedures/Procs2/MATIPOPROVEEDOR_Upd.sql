﻿
CREATE Procedure dbo.MATIPOPROVEEDOR_Upd
	@IDTipoProv char(3),
	@Descripcion varchar(30),
	@PideIdioma	bit,
	@Comision	numeric(6,2),
	@UserMod	char(4)	
	
As
	Set NoCount On
	
	Update MATIPOPROVEEDOR
	Set Descripcion=@Descripcion,PideIdioma=@PideIdioma,
	Comision=@Comision,
	UserMod=@UserMod,FecMod=getdate()
	Where IDTipoProv=@IDTipoProv
	

