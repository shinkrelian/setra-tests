﻿--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet  
CREATE Procedure dbo.RESERVAS_DET_ESTADOHABITAC_SelxIDReserva    
 @IDReserva int    
As    
 Set Nocount On  
   
 Select IDDET,   
 ROW_NUMBER() over(Order by IDDET) as Nro,     
 FechaIn, FechaOut, Simple, Twin, Matrimonial, Triple    
 From  
  (  
  Select Distinct rd.IDDet,   
    
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103))) as FechaIn,    
  upper(substring(DATENAME(dw,rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,rd.FechaOut,103))) as FechaOut,      
  re.Simple,re.Twin,re.Matrimonial,re.Triple    
  From 
  RESERVAS_DET rd Left Join  RESERVAS_DET_ESTADOHABITAC re On rd.IDDet=re.IDDet     
  
  
  Where rd.IDReserva=@IDReserva     
  ) as X  
 Where Not FechaOut Is Null  
 Order by IDDET    
 
