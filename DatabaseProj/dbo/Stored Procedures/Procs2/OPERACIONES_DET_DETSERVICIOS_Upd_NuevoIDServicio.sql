﻿CREATE Procedure [dbo].[OPERACIONES_DET_DETSERVICIOS_Upd_NuevoIDServicio]
	@IDServicio char(8),		
	@IDServicioNue char(8),
	@UserMod char(4)
As
	Set NoCount On
	
	Update OPERACIONES_DET_DETSERVICIOS
		SET IDServicio=@IDServicioNue, FecMod=GETDATE(), UserMod=@UserMod
	Where IDServicio=@IDServicio

