﻿Create Procedure [dbo].[MATIPOOPERACION_Sel_Rpt]
	@Descripcion varchar(100)
As
	Set NoCount On
	
	Select  IDToc,Descripcion
	From MATIPOOPERACION
	Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')
	order by 1
