﻿--JRF-20120910- Nuevos Campos FechaPago,IDUbigeo 
--HLF-20120911-Fecha=GETDATE()
CREATE Procedure PRESUPUESTOS_Ins  
@IDCab int,  
@IDUsuarioPre char(4),  
@IDGuia char(6),  
@IDEstado char(2),  
@FechaPago smalldatetime,
@IDUbigeo char(6),
@UserMod char(4),  
@pIDPresupuesto int Output  
As  
 Set NoCount On  
 Declare @IDPresupuesto int  
 Execute dbo.Correlativo_SelOutput 'PRESUPUESTOS',1,10,@IDPresupuesto OutPut   
   
 INSERT INTO [PRESUPUESTOS]  
      ([IDPresupuesto]  
      ,[IDCab]  
      ,[Fecha]  
      ,[IDUsuarioPre]  
      ,[IDGuia]  
      ,[IDEstado] 
      ,[FechaPago]
      ,[IDUbigeo]
      ,[UserMod])  
   VALUES  
      (@IDPresupuesto,   
      @IDCab,    
      convert(smalldatetime,convert(varchar,GETDATE(),103)),   
      @IDUsuarioPre,   
      @IDGuia,    
      @IDEstado,   
      @FechaPago,
      @IDUbigeo,
      @UserMod)  
 set @pIDPresupuesto = @IDPresupuesto  



