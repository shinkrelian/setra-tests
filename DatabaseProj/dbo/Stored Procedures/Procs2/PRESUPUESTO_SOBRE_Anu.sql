﻿Create Procedure dbo.PRESUPUESTO_SOBRE_Anu
@NuPreSob int,
@CoEstado char(2),
@UserMod char(4)
As
Set NoCount On
Update PRESUPUESTO_SOBRE
	Set CoEstado = @CoEstado,
		UserMod = @UserMod,
		FecMod = Getdate()
Where NuPreSob = @NuPreSob
