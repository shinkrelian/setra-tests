﻿--JRF-20130308-Isnull en los cambios.
CREATE Procedure dbo.RESERVAS_DET_SelPaxxIDReserva_Det  
@IDReserva_Det int  
As  
 Set NoCount On  
 select Convert(char(10),rd.Dia,103) As Dia,Convert(char(10),rd.FechaOut,103) As DiaFin  
    ,pr.NombreCorto,rd.Servicio,rd.NroPax ,  
    isnull(cp.Nombres,'') + ' ' + isnull(cp.Apellidos,'') As NombreCompleto  
 from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva  
   Left Join COTICAB c on r.IDCab = c.IDCAB  
   Left Join MAPROVEEDORES pr On r.IDProveedor = pr.IDProveedor  
   Left Join RESERVAS_DET_PAX rdp On rd.IDReserva_Det = rdp.IDReserva_Det  
   Left Join COTIPAX cp on cp.IDPax = rdp.IDPax  
 Where rd.IDReserva_Det = @IDReserva_Det and rd.NroPax <> c.NroPax  
 Order by rd.Dia  
