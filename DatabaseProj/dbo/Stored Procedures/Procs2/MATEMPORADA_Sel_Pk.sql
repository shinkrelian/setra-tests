﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_Pk] 
			@IDTemporada	int
AS
BEGIN
	Set NoCount On
	Select IDTemporada, NombreTemporada, TipoTemporada
	From MATEMPORADA
	WHERE IDTemporada = @IDTemporada
END
