﻿CREATE Procedure [dbo].[MASERIES_Sel_List]
	@IDSerie char(3),
	@Descripcion varchar(60)
As	
	Set NoCount On

	Select IDSerie,Descripcion 
	From MASERIES Where (Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='') And (IDSerie=@IDSerie
		 Or LTRIM(rtrim(@IDSerie))='')
