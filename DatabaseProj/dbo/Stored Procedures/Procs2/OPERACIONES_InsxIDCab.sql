﻿
--HLF-20130809-Agregando subquery And IDReserva In (Select r1.IDReserva From RESERVAS r1     
 --Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E'    
 -- Where IDCab=@IDCab   
--HLF-20130902-Agregando And Estado<>'XL'   
--HLF-20130912-Agregando And Anulado = 0  
--HLF-20140122-Agregando  And r1.Anulado=0     
CREATE Procedure [dbo].[OPERACIONES_InsxIDCab]      
 @IDCab int,      
 @UserMod char(4)      
AS
BEGIN
 DECLARE @IDProveedor_Zicasso char(6) = '003557'
 Set Nocount On      
       
 Declare @IDOperacion int      
 Execute dbo.Correlativo_SelOutput 'OPERACIONES',1,10,@IDOperacion output      
       
 Insert Into OPERACIONES      
 (IDOperacion,IDReserva,IDCab,IDFile,IDProveedor,UserMod)      
 Select        
 ((row_number() over (order by IDReserva))-1) + @IDOperacion,       
 IDReserva,IDCab,IDFile,IDProveedor,@UserMod      
 From RESERVAS   
 Where IDCab=@IDCab   And IDReserva In (Select r1.IDReserva From RESERVAS r1     
 Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E' And r1.Anulado=0    
 Where IDCab=@IDCab AND p1.IDProveedor <> @IDProveedor_Zicasso)    
 And Estado<>'XL'  
 And Anulado=0  
       
 Declare @iRowsAff smallint=@@ROWCOUNT - 1      
 If @iRowsAff>0 Exec Correlativo_Upd 'OPERACIONES',@iRowsAff      
END
