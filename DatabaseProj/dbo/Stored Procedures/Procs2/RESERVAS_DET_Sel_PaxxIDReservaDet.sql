﻿--JRF-20140527-Considerar el titulo academico como prefijo al apellidos
--JRF-20140528-Considerar descTC para cuando el campo FlTC este activo.
CREATE Procedure dbo.RESERVAS_DET_Sel_PaxxIDReservaDet  
@IDReserva_Det int  
As  
 Set NoCount On  
 --select p.Orden as NroOrd,p.Apellidos,p.Nombres,p.Titulo,Substring(id.Descripcion,1,3) as DescIdentidad,  
 select p.Orden as NroOrd,p.Apellidos,p.Nombres,
 Case when ltrim(rtrim(p.TxTituloAcademico)) <> '---' then p.TxTituloAcademico else p.Titulo End as Titulo,Substring(id.Descripcion,1,3) as DescIdentidad,  
 Isnull(p.NumIdentidad,'') as NumIdentidad,Isnull(ub.DC,'') as DescNacionalidad,  
 Isnull(Convert(char(10),p.FecNacimiento,103),'') as FecNacimiento,ISNULL(p.Peso,0.00) as Peso,  
 p.Residente,Isnull(p.ObservEspecial,'') as ObservEspecial,p.IDIdentidad,IDNacionalidad,rdp.IDPax,  
 Case rd.CapacidadHab   
  when 1 then 'SGL'  
  when 2 then case When rd.EsMatrimonial = 1 then 'MAT' Else 'TWIN'End  
  when 3 then 'TPL'  
  when 4 then 'QUAD'  
  else CAST(rd.Cantidad as varchar(5))+'PAX'  
 End as ACC,  
 cast(rd.CapacidadHab as varchar(5)) as CapacidadHab,  
 cast(rd.EsMatrimonial as CHAR(1)) as EsMatrimonial  ,
 Case when p.FlTC = 1 then 'TC' Else '' End as DescTC
 from RESERVAS_DET_PAX rdp Left Join COTIPAX p On rdp.IDPax = p.IDPax  
 Left Join MATIPOIDENT id On p.IDIdentidad = id.IDIdentidad  
 Left Join MAUBIGEO ub On p.IDNacionalidad = ub.IDubigeo  
 Left Join RESERVAS_DET rd On rdp.IDReserva_Det = rd.IDReserva_Det  
 where rdp.IDReserva_Det = @IDReserva_Det  
 Order by p.Orden  
