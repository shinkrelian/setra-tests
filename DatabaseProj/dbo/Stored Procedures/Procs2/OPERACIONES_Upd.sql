﻿Create Procedure dbo.OPERACIONES_Upd
	@IDOperacion  int,
	@Observaciones	varchar(250),
	@UserMod	char(4)
As
	Set Nocount On	

	Update OPERACIONES 
	Set Observaciones=Case When LTRIM(rtrim(@Observaciones))='' Then Null Else @Observaciones End,
	Usermod=@UserMod, FecMod=getdate()
	Where IDOperacion=@IDOperacion
	
