﻿
-----------------------------------------------------------------------------
--[NuDocInterno_SelOutput] 2,'01/10/2014',''
--create
create Procedure [dbo].[NuDocInterno_SelOutput]    
 @NuDocumProv int,  
 @FeEmision smalldatetime,  
 @pNuDocumProv varchar(8) Output    
As    
begin
 Set NoCount On    
  
 Declare @NuDocInterno varchar(8)=(Select IsNull(NuDocInterno,'') From DOCUMENTO_PROVEEDOR Where NuDocumProv=@NuDocumProv)    
  --select @NuDocInterno
 If @NuDocInterno=''     
  Begin    
  Declare @FecInicio varchar(4)=(Select SubString((convert(varchar,@FeEmision,112)),3,4)) --From DOCUMENTO_PROVEEDOR Where NuDocumProv=@NuDocumProv)    
  --select @FecInicio
  Declare @iNuDocInterno int=IsNull((Select MAX(NuDocInterno) From DOCUMENTO_PROVEEDOR     
  Where Left(NuDocInterno,4)=@FecInicio), CAST(@FecInicio+'0000' AS INT)  )+1    
   
   --select @iNuDocInterno
   
  Declare @vcNuDocInterno varchar(8)=Cast(@iNuDocInterno as varchar(8))  
  
  --select @vcNuDocInterno
  
  --If Cast(Right(@vcNuDocInterno,4) as int)<100   
  -- Set @pNuDocumProv = left(@vcNuDocInterno,4) + '0100'   
  --Else  
  Set @pNuDocumProv = Cast(@iNuDocInterno as varchar(8))     
     
  End    
 Else    
  Set @pNuDocumProv = @NuDocInterno  
  
  select  @pNuDocumProv
end;
