﻿
CREATE Procedure dbo.OPERACIONES_SelIDOperacionxIDReservaOutput
	@IDReserva int,
	@pIDOperacion int Output
As    
	Set Nocount On    

		
	Select @pIDOperacion=IDOperacion From OPERACIONES Where IDReserva=@IDReserva	
	Set @pIDOperacion=IsNull(@pIDOperacion,0)
	
