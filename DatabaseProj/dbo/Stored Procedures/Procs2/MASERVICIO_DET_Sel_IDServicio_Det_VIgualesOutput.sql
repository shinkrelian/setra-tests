﻿
Create Procedure dbo.MASERVICIO_DET_Sel_IDServicio_Det_VIgualesOutput
	@IDServicio_Det	int,
	@IDServicio_Det_V	int,
	@pbIguales	bit Output
As
	Set Nocount on
	
	Set @pbIguales=0
	If Exists(Select IDServicio From MASERVICIOS_DET Where IDServicio_Det=@IDServicio_Det And 
		IDServicio_Det_V=@IDServicio_Det_V)
		Set @pbIguales=1

