﻿
Create Procedure dbo.MASERVICIOS_DET_COSTOS_DelxIDServicio_DetCopia
 @IDServicio	char(8),
 @IDServicio_DetCopia int 
As  
 Set NoCount On  

 Delete From MASERVICIOS_DET_COSTOS
 Where IDServicio_Det In
 (Select IDServicio_Det From MASERVICIOS_DET 
	Where IDServicio_DetCopia=@IDServicio_DetCopia And IDServicio=@IDServicio)
