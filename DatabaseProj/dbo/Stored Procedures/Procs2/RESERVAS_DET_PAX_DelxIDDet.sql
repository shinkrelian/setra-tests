﻿Create Procedure [dbo].[RESERVAS_DET_PAX_DelxIDDet]
	@IDDet int
As

	Set NoCount On
	Delete from RESERVAS_DET_PAX Where IDReserva_Det In 
		(Select IDReserva_Det From RESERVAS_DET Where IDDet = @IDDet)
