﻿

--JRF-20130520-Nuevo campo para Observ. de Politicas            
--JRF-20130520-Nuevo campo IDUsuarioProgTransp.          
--JRF-20130709-Nuevo campo IDProveedorInternacional        
--JRF-20131116-Nuevo campo IDUsuarioTrasladista        
--JRF-20140313-Aumento de Longitud del campo Direccion      
--JRF-20140512-Agregar los nuevos campos EmailBiblia1,EmailBiblia2    
--JRF-20140526-Desagregandlo los campos de Biblia, al haber creado   
--      una tabla para estos correos.  
--JRF-20140908-Agregar nuevo campo [SsMontoCredito]
--JHD-20140910-Agregar nueva columna CoMonedaCredito
--JHD-20141022-Agregar columnas TxLatitud y TxLongitud 
--JHD-20141025-Se cambio longitud de varchar en columnas TxLatitud y TxLongitud 
--JHD-20141022-Agregar columna SSTipoCambio
--JHD-20141210-Agregar columna CoUbigeo_Oficina
--JHD-20150814-Agregar columna FlExcluirSAP
--HLF-20151029-Agregar columna CoTarjCredAcept
--PPMG20151207-Agregar Usuario, Password y FlAccesoWeb
CREATE Procedure [dbo].[MAPROVEEDORES_Upd]              
 @IDProveedor char(6),                                 
 @IDProveedor_Cadena char(6),                                
 @EsCadena bit,                                
 @Persona char(1),                                
 @RazonSocial varchar(100),                                
 @Nombre varchar(60),                                
 @ApPaterno varchar(60),                                
 @ApMaterno varchar(60),                                
 @Domiciliado bit,                                
 @NombreCorto varchar(100),                                
 @Sigla char(3) = '',                  
 @Direccion varchar(150),                                
 @IDCiudad char(6),                                
 @IDIdentidad char(3),                                
 @NumIdentidad varchar(15),                                
 @Telefono1 varchar(50),                                
 @Telefono2 varchar(50),                                
 @TelefonoReservas1 varchar(50),                                
 @TelefonoReservas2 varchar(50),                                
                            
 @Celular_Claro varchar(20),                                
 @Celular_Movistar varchar(20),                                
 @Celular_Nextel varchar(20),                                
 @NomContacto varchar(80),                              
 @Celular varchar(20),                              
 @Fax varchar(50),                                
 @FaxReservas1 varchar(50),                                
 @FaxReservas2 varchar(50),                                 
                            
 @Email1 varchar(100),                                
 @Email2 varchar(100),                                 
 @Email3 varchar(100),                                
 @Email4 varchar(100),                                
                          
 @TelefonoRecepcion1 varchar(50),                               
 @TelefonoRecepcion2 varchar(50),                               
 @FaxRecepcion varchar(60),                              
 @EmailRecepcion1 varchar(100),                              
 @EmailRecepcion2 varchar(100),                                 
                            
 @Web varchar(150),                                
 @Adicionales text,                                
 @PrePago numeric(8,2),                                
 @PlazoDias tinyint,                                
          
 @IDProveedorInternacional char(6),          
                          
 @IDFormaPago char(3),                                
 @TipoMuseo char(3),                                
 @Guia_Nacionalidad char(6),                                
 @Guia_Especialidad char(3),                                
 @Guia_Estado char(1),                                
 @Guia_Ubica_CV varchar(10),                                
 @Guia_PoliticaMovilidad bit,                                
 @Guia_Asociacion char(3),                                
 @Guia_CarnetApotur bit, 
 @Guia_CarnetGuiaOficial bit,                                
 @Guia_CertificadoPBIP bit,                       
 @Guia_CursoPBIP bit,                                
 @Guia_ExperienciaTC bit,                                
 @Guia_LugaresTC varchar(150),                             
                          
 @DiasCredito tinyint,                            
 @EmailContactoAdmin varchar(150),                            
 @TelefonoContactoAdmin varchar(50),                            
                          
 @DocPdfRutaPdf varchar(200),                        
 @Margen numeric(6,2),                         
 @IDUsuarioProg char(4),                      
                            
 @Disponible text ,                    
 @DocFoto varchar(max),                    
 @DocCurriculo varchar (max) ,                    
 @RUC varchar(12),                    
 @FecNacimiento smalldatetime,                    
 @Movil1 varchar(30),                    
 @Movil2 varchar(30),                    
 @OtrosEspecialidad varchar(110),                  
 @OtrosNacionalidad varchar(110),                    
 @PreferFITS  bit,                    
 @PreferGroups  bit,                    
 @PreferFamilias  bit,                    
 @Prefer3raEdad  bit,                    
 @PreferOtros Text ,                
 @EntrBriefing char(2),                    
 @EntrCultGeneral char(2),                    
 @EntrInfHistorica char(2),                    
 @EntrActitud char(2),                    
 @EntrOtros varchar(150),                    
 @MeetingPoint varchar(MAX),             
 @ObservacionPolitica Text,             
 @IDUsuarioProgTransp char(4),        
 @IDUsuarioTrasladista char(4),       
 @SsMontoCredito numeric(8,2),  
 @CoMonedaCredito char(3),
 @TxLatitud varchar(12)='',
 @TxLongitud varchar(12)='',
 @SSTipoCambio decimal(6,3)=0,
 @CoUbigeo_Oficina CHAR(6)='',
 @FlExcluirSAP bit=0,
 @CoTarjCredAcept char(3),
 @UserMod char(4),
 @Usuario	varchar(80)=null,
 @Password	varchar(255)=null,
 @FlAccesoWeb	bit=0
AS
BEGIN
	SET @Usuario = ltrim(rtrim(@Usuario))
	IF @Usuario <> ''
	BEGIN
		IF [dbo].[FnExisteUsuarioProvClie]('', @IDProveedor, 'V', @Usuario)=1
		BEGIN
			RAISERROR ('Ya esiste el nombre del usuario a ingresar.', 16, 1);
			RETURN
		END
	END
 Set NoCount On                                
                    
 UPDATE MAPROVEEDORES                 
 SET                                                      
 IDProveedor_Cadena=Case When ltrim(rtrim(@IDProveedor_Cadena))='' Then Null Else @IDProveedor_Cadena End                                
 ,EsCadena=@EsCadena                                
 ,Persona=@Persona                                
 ,RazonSocial=@RazonSocial                                
 ,Nombre=case when LTRIM(rtrim(@Nombre))='' then Null else @Nombre End                                
 ,ApPaterno= case when LTRIM(rtrim(@ApPaterno))='' then Null else @ApPaterno End                                
 ,ApMaterno= case when LTRIM(rtrim(@ApMaterno))='' then Null else @ApMaterno End                                
 ,Domiciliado=@Domiciliado                                
 ,NombreCorto=@NombreCorto                                
 ,Sigla = Case When ltrim(rtrim(@Sigla))='' Then Null Else @Sigla End                  
 ,Direccion=@Direccion                                
 ,IDCiudad=@IDCiudad                                
 ,IDIdentidad=@IDIdentidad                                
 ,NumIdentidad=@NumIdentidad                                
 ,Telefono1=Case When ltrim(rtrim(@Telefono1))='' Then Null Else @Telefono1 End                                
 ,Telefono2=Case When ltrim(rtrim(@Telefono2))='' Then Null Else @Telefono2 End                                
                    
 ,TelefonoReservas1=Case When ltrim(rtrim(@TelefonoReservas1))='' Then Null Else @TelefonoReservas1 End                                
 ,TelefonoReservas2=Case When ltrim(rtrim(@TelefonoReservas2))='' Then Null Else @TelefonoReservas2 End                                
                    
 ,Celular_Claro=Case When ltrim(rtrim(@Celular_Claro))='' Then Null Else @Celular_Claro  End                                
 ,Celular_Movistar=Case When ltrim(rtrim(@Celular_Movistar))='' Then Null Else @Celular_Movistar  End                                
 ,Celular_Nextel=Case When ltrim(rtrim(@Celular_Nextel))='' Then Null Else @Celular_Nextel End                                
 ,Celular=Case When LTRIM(RTRIM(@Celular))='' Then Null Else @Celular End                              
 ,NomContacto=Case When LTRIM(RTRIM(@NomContacto))='' Then Null Else @NomContacto End                              
 ,Fax=Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End                                
 ,FaxReservas1 = Case When ltrim(rtrim(@FaxReservas1))='' Then Null Else @FaxReservas1 End                                 
 ,FaxReservas2 = Case When ltrim(rtrim(@FaxReservas2))='' Then Null Else @FaxReservas2 End                                 
 ,Email1=Case When ltrim(rtrim(@Email1))='' Then Null Else @Email1 End                                
 ,Email2=Case When ltrim(rtrim(@Email2))='' Then Null Else @Email2 End                                 
 ,Email3=Case When ltrim(rtrim(@Email3))='' Then Null Else @Email3 End                                
 ,Email4=Case When ltrim(rtrim(@Email4))='' Then Null Else @Email4 End              
                    
 ,TelefonoRecepcion1= case when LTRIM(rtrim(@TelefonoRecepcion1))='' then Null else @TelefonoRecepcion1 End                            
 ,TelefonoRecepcion2= case when LTRIM(rtrim(@TelefonoRecepcion2))='' then Null else @TelefonoRecepcion2 End                            
 ,FaxRecepcion=Case When ltrim(rtrim(@FaxRecepcion))='' Then Null Else @FaxRecepcion End                    
 ,EmailRecepcion1=Case When ltrim(rtrim(@EmailRecepcion1))='' Then Null Else @EmailRecepcion1 End                                
 ,EmailRecepcion2=Case When ltrim(rtrim(@EmailRecepcion2))='' Then Null Else @EmailRecepcion2 End                                
                    
                    
 ,Web=Case When ltrim(rtrim(@Web))='' Then Null Else @Web End                                
 ,Adicionales=Case When ltrim(rtrim(Cast (@Adicionales as varchar(max))))='' Then Null Else @Adicionales End                                
 ,PrePago=Case When @PrePago=0 Then Null Else @PrePago End                                
 ,PlazoDias=Case When @PlazoDias=0 Then Null Else @PlazoDias End                                
                    
 ,IDProveedorInternacional = Case When ltrim(rtrim(@IDProveedorInternacional))='' then Null else @IDProveedorInternacional End          
 ,IDFormaPago=@IDFormaPago                                
 ,TipoMuseo=Case When ltrim(rtrim(@TipoMuseo))='' Then Null Else @TipoMuseo End                                
 ,Guia_Nacionalidad=@Guia_Nacionalidad                                
 ,Guia_Especialidad=Case When ltrim(rtrim(@Guia_Especialidad))='' Then Null Else @Guia_Especialidad End                                
 ,Guia_Estado=Case When ltrim(rtrim(@Guia_Estado))='' Then Null Else @Guia_Estado End                                
 ,Guia_Ubica_CV=Case When ltrim(rtrim(@Guia_Ubica_CV))='' Then Null Else @Guia_Ubica_CV End                                
 ,Guia_PoliticaMovilidad=@Guia_PoliticaMovilidad                                
 ,Guia_Asociacion=@Guia_Asociacion                                
 ,Guia_CarnetApotur=@Guia_CarnetApotur                                
 ,Guia_CarnetGuiaOficial=@Guia_CarnetGuiaOficial                                
 ,Guia_CertificadoPBIP=@Guia_CertificadoPBIP                                
 ,Guia_CursoPBIP=@Guia_CursoPBIP                                
 ,Guia_ExperienciaTC=@Guia_ExperienciaTC                                
 ,Guia_LugaresTC=@Guia_LugaresTC                                
 ,DiasCredito=Case when @DiasCredito=0 then Null else @DiasCredito End                            
 ,EmailContactoAdmin=Case When LTRIM(rtrim(@EmailContactoAdmin))='' then Null else @EmailContactoAdmin End                            
 ,TelefonoContactoAdmin=Case When ltrim(rtrim(@TelefonoContactoAdmin))='' Then Null else @TelefonoContactoAdmin End                            
 ,DocPdfRutaPdf= Case When LTRIM(RTRIM(@DocPdfRutaPdf))='' Then Null Else @DocPdfRutaPdf End                 
 ,Margen =  Case When @Margen=0 then Null else @Margen End                       
 ,IDUsuarioProg = Case When LTRIM(rtrim(@IDUsuarioProg))='' Then Null Else @IDUsuarioProg End                      
          
 ,Disponible = @Disponible                    
 ,DocFoto = Case When LTRIM(rtrim(@DocFoto))='' Then Null else @DocFoto End                    
 ,DocCurriculo = Case When LTRIM(rtrim(@DocCurriculo))='' Then Null else @DocCurriculo End                    
 ,RUC = Case When LTRIM(rtrim(@RUC))='' Then Null else  @RUC End                    
 ,FecNacimiento = Case When LTRIM(RTRIM(@FecNacimiento))= '01/01/1900' Then Null Else @FecNacimiento End         
 ,Movil1 = Case When ltrim(rtrim(@Movil1))='' Then Null Else @Movil1 End                     
 ,Movil2 = Case When ltrim(rtrim(@Movil2))='' Then Null Else @Movil2 End                     
 ,OtrosEspecialidad = Case When ltrim(rtrim(@OtrosEspecialidad))='' Then Null Else @OtrosEspecialidad End                     
 ,OtrosNacionalidad = Case When ltrim(rtrim(@OtrosNacionalidad))='' Then Null Else @OtrosNacionalidad End                    
 ,PreferFITS = @PreferFITS                    
 ,PreferGroups = @PreferGroups                    
 ,PreferFamilias = @PreferFamilias                    
 ,Prefer3raEdad = @Prefer3raEdad                    
 ,PreferOtros = @PreferOtros                    
 ,EntrBriefing =Case When ltrim(rtrim(@EntrBriefing))='' Then Null Else @EntrBriefing  End                     
 ,EntrCultGeneral = Case When ltrim(rtrim(@EntrCultGeneral))='' Then Null Else @EntrCultGeneral  End                     
 ,EntrInfHistorica = Case When ltrim(rtrim(@EntrInfHistorica))='' Then Null Else @EntrInfHistorica  End                     
 ,EntrActitud = Case When ltrim(rtrim(@EntrActitud))='' Then Null Else @EntrActitud  End                    
 ,EntrOtros = Case When ltrim(rtrim(@EntrOtros))='' Then Null Else @EntrOtros  End                     
 ,MeetingPoint = Case When ltrim(rtrim(@MeetingPoint))='' Then Null Else @MeetingPoint  End                     
 ,ObservacionPolitica = Case When LTRIM(rtrim(cast(@ObservacionPolitica as varchar(max))))='' Then Null Else @ObservacionPolitica End            
 ,IDUsuarioProgTransp = Case When LTRIM(RTRIM(@IDUsuarioProgTransp))='' Then Null Else @IDUsuarioProgTransp End            
 ,IDUsuarioTrasladista = case when ltrim(rtrim(@IDUsuarioTrasladista))='' then Null Else @IDUsuarioTrasladista End        
 ,SsMontoCredito = Case When @SsMontoCredito=0 then Null else @SsMontoCredito End
 ,CoMonedaCredito = case when ltrim(rtrim(@CoMonedaCredito))='' then Null Else @CoMonedaCredito End    
 ,TxLatitud = Case When ltrim(rtrim(@TxLatitud))='' Then Null Else @TxLatitud End
 ,TxLongitud = Case When ltrim(rtrim(@TxLongitud))='' Then Null Else @TxLongitud End
 ,SSTipoCambio=Case When @SSTipoCambio=0 then Null Else @SSTipoCambio End
 ,CoUbigeo_Oficina=Case When ltrim(rtrim(@CoUbigeo_Oficina))='' Then Null Else @CoUbigeo_Oficina End
 ,FlExcluirSAP=@FlExcluirSAP
 ,CoTarjCredAcept=Case When ltrim(rtrim(@CoTarjCredAcept))='' Then Null Else @CoTarjCredAcept End

 ,[Usuario] = Case When ltrim(rtrim(ISNULL(@Usuario,'')))='' Then NULL Else @Usuario End
 ,[Password] = Case When ltrim(rtrim(ISNULL(@Password,'')))='' Then NULL Else @Password End
 ,[FlAccesoweb] = ISNULL(@FlAccesoWeb,0)

 ,UserMod=@UserMod                                           
 ,FecMod=GETDATE()                                
 WHERE  IDProveedor = @IDProveedor
END

