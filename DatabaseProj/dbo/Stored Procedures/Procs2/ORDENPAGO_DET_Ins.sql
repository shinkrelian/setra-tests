﻿--JRF-20150305-Agregar Columna para la relacion con el Transporte (IDTransporte)
--JRF-20150411-Agregar una descripción para textos manuales (@TxDescripcion)
--JHD-20150908-Agregar el campo SSDetraccion
--JHD-20150909-Se agrego el campo Total_Final el cual es la diferencia del total con la detracción
--JHD-20150909-Agregar el campo SSDetraccion_USD
--JHD-20150909-@Total-(case when isnull(@SSDetraccion_USD,0)=0 then @SSDetraccion else @SSDetraccion_USD end),
CREATE Procedure [dbo].[ORDENPAGO_DET_Ins]
 @IDOrdPag int,  
 @IDReserva_Det int,  
 @TxDescripcion varchar(Max),
 @IDTransporte int,
 @IDServicio_Det int,  
 @NroPax smallint,  
 @NroLiberados smallint,  
 @Cantidad tinyint,  
 @Noches tinyint,  
 @Total numeric(12,2),  
 @SSDetraccion numeric(12,2)=0,
 @SSDetraccion_USD numeric(12,2)=0,
 @UserMod char(4)  
As  
 Set Nocount On  
   
 Declare @IDOrdPag_Det int        
 Execute dbo.Correlativo_SelOutput 'ORDENPAGO_DET',1,10,@IDOrdPag_Det output        

 INSERT INTO [ORDENPAGO_DET]  
           ([IDOrdPag_Det]  
           ,[IDOrdPag]  
           ,[IDReserva_Det]  
		   ,[TxDescripcion]
           ,[IDServicio_Det]  
           ,[NroPax]  
           ,[NroLiberados]  
           ,[Cantidad]  
           ,[Noches]  
           ,[Total] 
           ,[IDTransporte] 
		   ,SSDetraccion
		   ,SSDetraccion_USD
		   ,Total_Final
           ,[UserMod]  
           )  
     VALUES  
           (@IDOrdPag_Det,  
           @IDOrdPag,   
           Case When @IDReserva_Det=0 Then Null Else @IDReserva_Det End,              
		   Case When Ltrim(rtrim(@TxDescripcion))='' Then Null else Ltrim(rtrim(@TxDescripcion)) End,
           Case When @IDServicio_Det=0 Then Null Else @IDServicio_Det End,  
           @NroPax,   
           @NroLiberados,  
           @Cantidad,  
           @Noches,  
           @Total,   
           Case When @IDTransporte = 0 Then Null Else @IDTransporte End,
		   Case When @SSDetraccion = 0 Then Null Else @SSDetraccion End,
		   Case When @SSDetraccion_USD = 0 Then Null Else @SSDetraccion_USD End,
		   --@Total-@SSDetraccion,
		   @Total-(case when isnull(@SSDetraccion_USD,0)=0 then @SSDetraccion else @SSDetraccion_USD end),
           @UserMod)  

