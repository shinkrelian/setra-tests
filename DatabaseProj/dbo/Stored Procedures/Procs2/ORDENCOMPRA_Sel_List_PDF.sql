﻿---[ORDENCOMPRA_Sel_List_PDF] 2
----JHD-20150310- Se agrego el campo CoTipoDetraccion.
----JHD-20150311- Se agrego el campo Detraccion.
----JHD-20150311- Centro_Costos=(select CoCeCos+' - '+Descripcion from dbo.MACENTROCOSTOS where CoCeCos=o.CoCeCos)
--JHD-20150408- Se quitaron los campos referentes al Area
--JHD-20150408- Se hizo LEFT JOIN a la tabla MARUBRO
--JHD-20150408- Titular_cta=isnull((select NombresTitular+' '+ApellidosTitular from dbo.MAADMINISTPROVEEDORES where IDAdminist=o.IDAdminist and IDProveedor=o.CoProveedor),''),
--JHD-20150408- CoRubro=isnull(o.CoRubro,0),
--JHD-20150408- NoRubro=ISNULL(r.NoRubro,''),
--JHD-20150408- SSDetraccion=isnull(o.SSDetraccion,0),
--JHD-20150626- isnull a los campos
--alter
CREATE PROCEDURE [dbo].[ORDENCOMPRA_Sel_List_PDF]
 @NuOrdComInt int  
 as
BEGIN
SELECT     o.NuOrdComInt,
o.FeOrdCom,
o.NuOrdCom,
--proveedor
o.CoProveedor,
isnull(p.NumIdentidad,'') as NumIdentidad,
isnull(p.NombreCorto,'') as NombreCorto,
isnull(p.Direccion,'') Direccion,
isnull(p.Telefono1,'') Telefono1,
isnull(o.IDContacto,'') IDContacto,
Contacto=isnull((select Titulo+' '+Nombres+' '+Apellidos from dbo.MACONTACTOSPROVEEDOR where IDContacto=o.IDContacto and IDProveedor=o.CoProveedor),''),
isnull(o.TxDescripcion,'') TxDescripcion, 
isnull(o.CoEstado,'') CoEstado,
Estado=isnull((select NoEstado from ESTADO_OBLIGACIONESPAGO where CoEstado=o.coestado),''),--case when o.CoEstado='A' THEN 'ACEPTADO' when o.CoEstado='X' THEN 'ANULADO' ELSE '' END,
--a.CoArea,
--a.NoArea,
--o.CoRubro,
CoRubro=isnull(o.CoRubro,0),
NoRubro=ISNULL(r.NoRubro,''),
--r.NoRubro,
o.SSSubTotal, 
o.SSIGV,
--o.SSDetraccion,
SSDetraccion=round(isnull(o.SSDetraccion,0),0),
o.SsTotal,
isnull(m.IDMoneda,'') IDMoneda,
isnull(m.Descripcion,'') as Moneda,
isnull(o.CoTipoDetraccion,'') CoTipoDetraccion,
Porc_Detraccion=isnull((select cast(round(SsTasa*100,2) as numeric(5,2)) from MATIPODETRACCION where CoTipoDetraccion=o.cotipodetraccion),00),
Forma_Pago=(select Descripcion from MAFORMASPAGO where IdFor=p.IDFormaPago) + (case when p.IDFormaPago='001' then ' '+cast(p.DiasCredito AS varchar(3))+' DIAS' Else '' end),
Titular_cta=isnull((select NombresTitular+' '+ApellidosTitular from dbo.MAADMINISTPROVEEDORES where IDAdminist=o.IDAdminist and IDProveedor=o.CoProveedor),''),
Banco=isnull((SELECT     MABANCOS.Descripcion
		FROM         MAADMINISTPROVEEDORES INNER JOIN
							  MABANCOS ON MAADMINISTPROVEEDORES.Banco = MABANCOS.IDBanco
		WHERE     (MAADMINISTPROVEEDORES.IDAdminist = o.IDAdminist) AND (MAADMINISTPROVEEDORES.IDProveedor = o.CoProveedor)
		),''),
Cuenta=isnull((SELECT  MAADMINISTPROVEEDORES.CtaCte
		FROM         MAADMINISTPROVEEDORES INNER JOIN
							  MABANCOS ON MAADMINISTPROVEEDORES.Banco = MABANCOS.IDBanco
		WHERE     (MAADMINISTPROVEEDORES.IDAdminist = o.IDAdminist) AND (MAADMINISTPROVEEDORES.IDProveedor = o.CoProveedor)
		),''),
Cuenta_Detraccion=isnull((SELECT top 1 MAADMINISTPROVEEDORES.CtaCte
		FROM         MAADMINISTPROVEEDORES INNER JOIN
							  MABANCOS ON MAADMINISTPROVEEDORES.Banco = MABANCOS.IDBanco
		WHERE      (MAADMINISTPROVEEDORES.IDProveedor = o.CoProveedor) and TipoCuenta='DE'
		),''),
		
Banco_Detraccion=isnull((SELECT top 1 MABANCOS.Descripcion
		FROM         MAADMINISTPROVEEDORES INNER JOIN
							  MABANCOS ON MAADMINISTPROVEEDORES.Banco = MABANCOS.IDBanco
		WHERE      (MAADMINISTPROVEEDORES.IDProveedor = o.CoProveedor) and TipoCuenta='DE'
		),''),
Detraccion=isnull((select cast( cast(round(SsTasa*100,0) as int) as varchar(10))+'% - '+ TxDescripcion from MATIPODETRACCION where CoTipoDetraccion=o.cotipodetraccion),''),
Centro_Costos=(select CoCeCos+' - '+Descripcion from dbo.MACENTROCOSTOS where CoCeCos=o.CoCeCos),
o.FeEntrega,
o.FePago		
FROM         ORDENCOMPRA o INNER JOIN
              MAMONEDAS m ON o.CoMoneda = m.IDMoneda LEFT JOIN
             -- RH_AREA a ON o.CoArea = a.CoArea INNER JOIN
              MARUBRO r ON o.CoRubro = r.CoRubro INNER JOIN
              MAPROVEEDORES p ON o.CoProveedor = p.IDProveedor
WHERE     
( o.NuOrdComInt=@NuOrdComInt Or @NuOrdComInt=0)

END;

