﻿

--JRF-20130520-Nuevo campo para Observ. de Politicas              
--JRF-20130520-Nuevo campo IDUsuarioProgTransp              
--JRF-20130709-Nuevo campo IDProveedorInternacional          
--JRF-20131116-Nuevo campo IDUsuarioTrasladista          
--JRF-20140313-Aumento de Tamaño (Direccion)        
--JRF-20140512-Agregar el campo de EmailBiblia1,EmailBiblia2      
--JRF-20140526-Desagregandlo los campos de Biblia, al haber creado     
--      una tabla para estos correos.    
--JRF-20140603-Corrección.  
--JRF-20140908-Agregar nueva columna SsMontoCredito
--JHD-20140910-Agregar nueva columna CoMonedaCredito
--JHD-20141022-Agregar columnas TxLatitud y TxLongitud 
--JHD-20141025-Se cambio longitud de varchar en columnas TxLatitud y TxLongitud 
--JHD-20141022-Agregar columna SSTipoCambio
--JHD-20141210-Agregar columna CoUbigeo_Oficina
--JHD-20150311-Se cambio la longitud del parametro @TelefonoContactoAdmin a varchar(50) 
--JHD-20150814-Agregar columna FlExcluirSAP
--HLF-20151029-Agregar columna CoTarjCredAcept
--PPMG20151207-Agregar Usuario, Password y FlAccesoWeb
CREATE PROCEDURE [dbo].[MAPROVEEDORES_Ins]                           
 @IDTipoProv char(3),                              
 @IDTipoOper char(1),                              
 @IDProveedor_Cadena char(6),                              
 @EsCadena bit,                              
 @Persona char(1),                              
 @RazonSocial varchar(100),                              
 @Nombre varchar(60),                              
 @ApPaterno varchar(60),                              
 @ApMaterno varchar(60),                              
 @Domiciliado bit,                              
 @NombreCorto varchar(100),                              
 @Sigla char(3),                
 @Direccion varchar(150),                              
 @IDCiudad char(6),                              
 @IDIdentidad char(3),                              
 @NumIdentidad varchar(15),                              
 @Telefono1 varchar(50),                              
 @Telefono2 varchar(50),                              
                       
 @TelefonoReservas1 varchar(50),                              
 @TelefonoReservas2 varchar(50),                              
                       
 @Celular_Claro varchar(20),                              
 @Celular_Movistar varchar(20),                              
 @Celular_Nextel varchar(20),                            
 @NomContacto varchar(80),                            
 @Celular varchar(20)  ,                            
 @Fax varchar(50),                              
                       
 @FaxReservas1 varchar(50),                              
 @FaxReservas2 varchar(50),                           
                       
 @TelefonoRecepcion1 varchar(50),                           
 @TelefonoRecepcion2 varchar(50),                           
 @FaxRecepcion varchar(60),                          
 @EmailRecepcion1 varchar(100),                          
 @EmailRecepcion2 varchar(100),                          
                       
 @Email1 varchar(100),                              
 @Email2 varchar(100),                              
 @Email3 varchar(100),                              
 @Email4 varchar(100),                              
                       
 @Web varchar(150),                              
 @Adicionales text,                              
 @PrePago numeric(8,2),                              
 @PlazoDias tinyint,                              
            
 @IDProveedorInternacional char(6),                       
            
 @IDFormaPago char(3),                              
 @TipoMuseo char(3),                              
 @Guia_Nacionalidad char(6),                              
 @Guia_Especialidad char(3),                              
 @Guia_Estado char(1),                              
 @Guia_Ubica_CV varchar(10),                              
 @Guia_PoliticaMovilidad bit,            
 @Guia_Asociacion char(3),                        
 @Guia_CarnetApotur bit,                              
 @Guia_CarnetGuiaOficial bit,                              
 @Guia_CertificadoPBIP bit,                              
 @Guia_CursoPBIP bit,                              
 @Guia_ExperienciaTC bit,                              
 @Guia_LugaresTC varchar(150),         
 @UserMod char(4),         
                       
 @DocPdfRutaPdf varchar(200),                        
 @Margen decimal(6,2),                    
 @IDUsuarioProg char(4),                    
                       
 @DiasCredito tinyint,                          
 @EmailContactoAdmin varchar(150),                          
 --@TelefonoContactoAdmin varchar(10),                          
 @TelefonoContactoAdmin varchar(50),
                  
 -- Nuevos Campos Guía                  
 @Disponible text ,                  
 @DocFoto varchar(max),                  
 @DocCurriculo varchar (max) ,                  
 @RUC varchar(12),                  
 @FecNacimiento smalldatetime,                  
 @Movil1 varchar(30),                  
 @Movil2 varchar(30),                 
 @OtrosEspecialidad varchar(110),                  
 @OtrosNacionalidad varchar(110),                  
 @PreferFITS  bit,                  
 @PreferGroups  bit,                  
 @PreferFamilias  bit,                  
 @Prefer3raEdad  bit,                  
 @PreferOtros Text ,                  
 @EntrBriefing char(2),                  
 @EntrCultGeneral char(2),                  
 @EntrInfHistorica char(2),                  
 @EntrActitud char(2),                  
 @EntrOtros varchar(150),                  
 @MeetingPoint varchar(MAX),           
 @ObservacionPolitica Text,              
 @IDUsuarioProgTransp char(4),            
 @IDUsuarioTrasladista char(4),          
 @SsMontoCredito numeric(8,2),
 @CoMonedaCredito char(3),
 
 @TxLatitud varchar(12)='',
 @TxLongitud varchar(12)='',
 @SSTipoCambio decimal(6,3)=0,
 @CoUbigeo_Oficina char(6)='',
 @FlExcluirSAP bit=0,
 @CoTarjCredAcept char(3),
 @Usuario	varchar(80)=null,
 @Password	varchar(255)=null,
 @FlAccesoWeb	bit=0,
 @pIDProveedor char(6) Output                              
AS
BEGIN
 Declare @IDProveedor char(6)                              
	SET @Usuario = ltrim(rtrim(@Usuario))
	IF @Usuario <> ''
	BEGIN
		IF [dbo].[FnExisteUsuarioProvClie]('', @IDProveedor, 'V', @Usuario)=1
		BEGIN
			RAISERROR ('Ya esiste el nombre del usuario a ingresar.', 16, 1);
			RETURN
		END
	END
 Set NoCount On                              
                            
 Exec Correlativo_SelOutput 'MAPROVEEDORES',1,6,@IDProveedor output                              
                              
 INSERT INTO MAPROVEEDORES                              
 (IDProveedor                              
 ,IDTipoProv                              
 ,IDTipoOper                              
 ,IDProveedor_Cadena                              
 ,EsCadena                
 ,Persona                              
 ,RazonSocial                              
 ,Nombre                              
 ,ApPaterno                              
 ,ApMaterno                              
 ,Domiciliado                              
 ,NombreCorto                 
 ,Sigla                             
 ,Direccion                              
 ,IDCiudad                              
 ,IDIdentidad                              
 ,NumIdentidad                              
 ,Telefono1                              
 ,Telefono2                      
 ,TelefonoReservas1                              
 ,TelefonoReservas2                              
                              
 ,Celular_Claro                              
 ,Celular_Movistar                              
 ,Celular_Nextel                              
 ,Celular                            
 ,NomContacto                            
 ,Fax                              
 ,FaxReservas1                              
 ,FaxReservas2                              
                              
 ,Email1                           
 ,Email2                              
 ,Email3                          
 ,Email4                              
 ,TelefonoRecepcion1   
 ,TelefonoRecepcion2                          
 ,FaxRecepcion               
 ,EmailRecepcion1                             
 ,EmailRecepcion2                        
                              
 ,Web                              
 ,Adicionales                              
 ,PrePago                              
 ,PlazoDias                              
                           
 ,IDProveedorInternacional            
                
 ,IDFormaPago                              
 ,TipoMuseo                              
 ,Guia_Nacionalidad                         
 ,Guia_Especialidad                              
 ,Guia_Estado                              
 ,Guia_Ubica_CV                              
 ,Guia_PoliticaMovilidad                              
 ,Guia_Asociacion                              
 ,Guia_CarnetApotur                              
 ,Guia_CarnetGuiaOficial                              
 ,Guia_CertificadoPBIP                              
 ,Guia_CursoPBIP                              
 ,Guia_ExperienciaTC                          
 ,Guia_LugaresTC                           
                          
 ,DiasCredito                          
 ,EmailContactoAdmin                          
 ,TelefonoContactoAdmin                          
                    
 ,DocPdfRutaPdf                    
 ,Margen                                
 ,IDUsuarioProg                    
                  
 ,Disponible                  
 ,DocFoto                  
 ,DocCurriculo                  
 ,RUC                  
 ,FecNacimiento                  
 ,Movil1                  
 ,Movil2                  
 ,OtrosEspecialidad                   
 ,OtrosNacionalidad                   
 ,PreferFITS                  
 ,PreferGroups                  
 ,PreferFamilias                  
 ,Prefer3raEdad                 
 ,PreferOtros                  
 ,EntrBriefing                  
 ,EntrCultGeneral                  
 ,EntrInfHistorica                  
 ,EntrActitud                  
 ,EntrOtros                  
 ,MeetingPoint               
 ,ObservacionPolitica               
 ,IDUsuarioProgTransp            
 ,IDUsuarioTrasladista          
 ,SsMontoCredito   
 ,CoMonedaCredito    
 ,TxLatitud
 ,TxLongitud
 ,SSTipoCambio
 ,CoUbigeo_Oficina
 ,UserMod              
 ,FlExcluirSAP    
 ,CoTarjCredAcept
 ,Usuario
 ,Password
 ,FlAccesoWeb
 )                              
 VALUES                              
 (@IDProveedor ,                              
 @IDTipoProv ,                              
 Case When ltrim(rtrim(@IDTipoOper))='' Then Null Else @IDTipoOper End,                    
 Case When ltrim(rtrim(@IDProveedor_Cadena))='' Then Null Else @IDProveedor_Cadena End,                              
 @EsCadena,                              
 @Persona,                              
 @RazonSocial ,                              
 case when LTRIM(RTRIM(@Nombre))='' then Null Else @Nombre End,                              
 case when LTRIM(RTRIM(@ApPaterno))='' then Null Else @ApPaterno End,                              
 Case When LTRIM(RTRIM(@ApMaterno))='' then Null Else @ApMaterno End,                              
 @Domiciliado,                              
 @NombreCorto ,                    
 Case When LTRIM(rtrim(@Sigla))='' Then Null Else @Sigla End,                          
 @Direccion ,                              
 @IDCiudad ,                              
 @IDIdentidad ,                              
 @NumIdentidad,                              
 Case When ltrim(rtrim(@Telefono1))='' Then Null Else @Telefono1 End ,                              
 Case When ltrim(rtrim(@Telefono2))='' Then Null Else @Telefono2 End  ,                              
 Case When ltrim(rtrim(@TelefonoReservas1))='' Then Null Else @TelefonoReservas1 End ,                              
 Case When ltrim(rtrim(@TelefonoReservas2))='' Then Null Else @TelefonoReservas2 End ,       
                  
 Case When ltrim(rtrim(@Celular_Claro))='' Then Null Else @Celular_Claro  End,                   
 Case When ltrim(rtrim(@Celular_Movistar))='' Then Null Else @Celular_Movistar  End,                              
 Case When ltrim(rtrim(@Celular_Nextel))='' Then Null Else @Celular_Nextel End,                              
 Case When LTRIM(rtrim(@Celular))='' then null else @Celular End,                      
 @NomContacto,                            
 Case When ltrim(rtrim(@Fax))='' Then Null Else @Fax End,                              
 Case When ltrim(rtrim(@FaxReservas1))='' Then Null Else @FaxReservas1 End ,                              
 Case When ltrim(rtrim(@FaxReservas2))='' Then Null Else @FaxReservas2 End ,                              
                              
 Case When ltrim(rtrim(@Email1))='' Then Null Else @Email1 End,                              
 Case When ltrim(rtrim(@Email2))='' Then Null Else @Email2 End,                                  
 Case When ltrim(rtrim(@Email3))='' Then Null Else @Email3 End,                              
 Case When ltrim(rtrim(@Email4))='' Then Null Else @Email4 End,                                  
                          
 Case when LTRIM(rtrim(@TelefonoRecepcion1))='' then Null Else @TelefonoRecepcion1 End,                          
 Case when LTRIM(rtrim(@TelefonoRecepcion2))='' then Null Else @TelefonoRecepcion2 End,                          
 Case When ltrim(rtrim(@FaxRecepcion))='' Then Null Else @FaxRecepcion End,                              
 Case When ltrim(rtrim(@EmailRecepcion1))='' Then Null Else @EmailRecepcion1 End,                              
 Case When ltrim(rtrim(@EmailRecepcion2))='' Then Null Else @EmailRecepcion2 End,                              
                              
 Case When ltrim(rtrim(@Web))='' Then Null Else @Web End,                      
 Case When ltrim(rtrim(Cast (@Adicionales as varchar(max))))='' Then Null Else @Adicionales End,                              
 Case When @PrePago=0 Then Null Else @PrePago End,                              
 Case When @PlazoDias=0 Then Null Else @PlazoDias End,                              
                  
 Case When ltrim(rtrim(@IDProveedorInternacional))='' Then Null Else @IDProveedorInternacional End,            
             
 @IDFormaPago ,                              
 Case When ltrim(rtrim(@TipoMuseo))='' Then Null Else @TipoMuseo End,                              
 @Guia_Nacionalidad ,                              
                              
 Case When ltrim(rtrim(@Guia_Especialidad))='' Then Null Else @Guia_Especialidad End,                            
 Case When ltrim(rtrim(@Guia_Estado))='' Then Null Else @Guia_Estado End,                              
 Case When ltrim(rtrim(@Guia_Ubica_CV))='' Then Null Else @Guia_Ubica_CV End,                              
 @Guia_PoliticaMovilidad ,                              
 @Guia_Asociacion ,                              
 @Guia_CarnetApotur ,                         @Guia_CarnetGuiaOficial ,                              
 @Guia_CertificadoPBIP ,                              
 @Guia_CursoPBIP ,                              
 @Guia_ExperienciaTC ,                              
 @Guia_LugaresTC ,                              
 Case When @DiasCredito=0 then null else @DiasCredito End,                          
 Case When LTRIM(rtrim(@EmailContactoAdmin))='' then null else @EmailContactoAdmin End,                          
 Case When LTRIM(rtrim(@TelefonoContactoAdmin))='' then null else @TelefonoContactoAdmin End,                          
 Case When LTRIM(rtrim(@DocPdfRutaPdf))='' then null else @DocPdfRutaPdf End,                      
 Case When @Margen=0 then Null else @Margen End,        
 Case When ltrim(rtrim(@IDUsuarioProg))='' Then Null Else @IDUsuarioProg End,                    
                  
 @Disponible ,                  
 Case When LTRIM(rtrim(@DocFoto))='' Then Null else @DocFoto End,                   
 Case When LTRIM(rtrim(@DocCurriculo))='' Then Null else @DocCurriculo End,                    
 Case When LTRIM(rtrim(@RUC))='' Then Null else  @RUC End,                  
 Case When LTRIM(RTRIM(@FecNacimiento))= '01/01/1900' Then Null Else @FecNacimiento End ,                  
 Case When ltrim(rtrim(@Movil1))='' Then Null Else @Movil1 End ,                  
  Case When ltrim(rtrim(@Movil2))='' Then Null Else @Movil2 End ,                  
  Case When ltrim(rtrim(@OtrosEspecialidad))='' Then Null Else @OtrosEspecialidad End ,                  
 Case When ltrim(rtrim(@OtrosNacionalidad))='' Then Null Else @OtrosNacionalidad End,                  
  @PreferFITS  ,                  
  @PreferGroups  ,                  
  @PreferFamilias  ,                  
  @Prefer3raEdad  ,                  
   @PreferOtros ,                  
  Case When ltrim(rtrim(@EntrBriefing))='' Then Null Else @EntrBriefing  End ,                  
  Case When ltrim(rtrim(@EntrCultGeneral))='' Then Null Else @EntrCultGeneral  End ,                  
  Case When ltrim(rtrim(@EntrInfHistorica))='' Then Null Else @EntrInfHistorica  End ,              
  Case When ltrim(rtrim(@EntrActitud))='' Then Null Else @EntrActitud  End ,                  
  Case When ltrim(rtrim(@EntrOtros))='' Then Null Else @EntrOtros  End ,                  
  Case When ltrim(rtrim(@MeetingPoint))='' Then Null Else @MeetingPoint  End ,                  
  Case When LTRIM(rtrim(cast(@ObservacionPolitica as varchar(max))))='' Then Null Else @ObservacionPolitica End,              
  Case When LTRIM(rtrim(@IDUsuarioProgTransp))='' Then Null Else @IDUsuarioProgTransp End,             
  Case When ltrim(rtrim(@IDUsuarioTrasladista))='' Then Null Else @IDUsuarioTrasladista End,           
  Case When @SsMontoCredito=0 then Null Else @SsMontoCredito End,
  Case When ltrim(rtrim(@CoMonedaCredito))='' Then Null Else @CoMonedaCredito End,           
  Case When ltrim(rtrim(@TxLatitud))='' Then Null Else @TxLatitud End,         
  Case When ltrim(rtrim(@TxLongitud))='' Then Null Else @TxLongitud End,         
  Case When @SSTipoCambio=0 then Null Else @SSTipoCambio End,
  Case When ltrim(rtrim(@CoUbigeo_Oficina))='' Then Null Else @CoUbigeo_Oficina End,         
  @UserMod,
  @FlExcluirSAP,
  Case When ltrim(rtrim(@CoTarjCredAcept))='' Then Null Else @CoTarjCredAcept End,
  Case When ltrim(rtrim(ISNULL(@Usuario,'')))='' Then NULL Else @Usuario End,
  Case When ltrim(rtrim(ISNULL(@Password,'')))='' Then NULL Else @Password End,
  ISNULL(@FlAccesoWeb,0))

 Set @pIDProveedor=@IDProveedor
END

