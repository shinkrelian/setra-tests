﻿CREATE Procedure [dbo].[MAUSUARIOS_Anu]
	@IDUsuario char(4),
	@UserMod char(4)

As

	Set NoCount On

	UPDATE MAUSUARIOS
    SET	Activo='I', UserMod=@UserMod, FecMod=GETDATE()
	WHERE	IDUsuario=@IDUsuario
