﻿--HLF-20120906-Nuevo parametro @ServicioVarios      
--HLF-20130123-Nuevo parametro @IDCabVarios    
--JRF-20131209-Aumentando el valor de los decimales a (numeric(10, 4))    
--JRF-20140730-Agregar el Nuevo campo de CoPago  
--JHD-20141127-Agregar el Nuevo campo de IDDesayuno  
--JHD-20141202-Se corrigio para insertar IDDesayuno nulo
--PPMG-20151106-Columna APTServicoOpcional
--PPMG-20151106-Columna HoraPredeterminada
--JRF-20151221-@HoraPredeterminada datetime a [@HoraPredeterminada varchar(20),]
CREATE Procedure [dbo].[MASERVICIOS_Ins]          
 @IDProveedor char(6),          
 @IDTipoProv char(3),             
 @IDTipoServ char(3),             
 @Descripcion varchar(100),                 
 @Tarifario bit,          
 @Descri_tarifario varchar(100),              
 @FecCaducLectura smalldatetime,          
 @Activo bit,        
 @IDubigeo char(6),          
 @Telefono varchar(50),          
 @Celular varchar(50),              
 @Comision numeric(10,4),          
 @UpdComision bit,          
 @IDCat char(3),          
 @Categoria tinyint,          
 @Capacidad smallint,          
 @Observaciones text,          
 @Lectura text,          
 @Transfer bit,          
 @TipoTransporte char(1),          
 @AtencionLunes bit,          
 @AtencionMartes bit,          
 @AtencionMiercoles bit,          
 @AtencionJueves bit,          
 @AtencionViernes bit,          
 @AtencionSabado bit,          
 @AtencionDomingo bit,          
 @HoraDesde smalldatetime,          
 @HoraHasta smalldatetime,          
      
 @DesayunoHoraDesde smalldatetime,          
 @DesayunoHoraHasta smalldatetime,          
 @IDDesayuno char(2)='',          
      
 @PreDesayunoHoraDesde smalldatetime,          
 @PreDesayunoHoraHasta smalldatetime,          
 @IDPreDesayuno char(2),          
 @ObservacionesDesayuno varchar(250),          
      
 @HoraCheckIn smalldatetime,          
 @HoraCheckOut smalldatetime,          
      
 --@TelefonoRecepcion1 varchar(25),          
 --@TelefonoRecepcion2 varchar(25),          
 --@FaxRecepcion1 varchar(25),          
 --@FaxRecepcion2 varchar(25),          
      
 --@IncluyeServiciosDias bit,           
 @Dias tinyint=1,          
      
 @PoliticaLiberado bit,          
 @Liberado tinyint,          
 @TipoLib char(1),          
 @MaximoLiberado tinyint,           
 @MontoL numeric(10,4),           
 @SinDescripcion bit = 0,      
 @ServicioVarios bit,          
 @UserMod char(4),          
 @IDCabVarios int,      
 @CoTipoPago char(3)='',
 @APTServicoOpcional char(1)='N',
 @HoraPredeterminada varchar(20),
 @pIDServicio char(8) output          
          
AS
BEGIN
 Set NoCount On         
 
 --Declare @ts timestamp = Cast(@HoraPredeterminada as timestamp)
 Set @HoraPredeterminada = Case When charindex('01/01/0001',@HoraPredeterminada) > 0 then '01/01/1900 00:00:00' else @HoraPredeterminada End


 Declare @DC char(3)=(Select DC From MAUBIGEO Where IDubigeo=@IDubigeo)           
 Declare @Correl smallint=Isnull((Select IsNull(Max(Right(IDServicio,5)),0) From MASERVICIOS Where IDubigeo=@IDubigeo),0)
Increment:
 Set @Correl = @Correl+1         
 Declare @IDServicio char(8)=@DC+ replicate('0',5-Len(Cast(@Correl as varchar(5)))) +Cast(@Correl as varchar(5))          

 If Exists(select IDServicio from MASERVICIOS Where IDServicio=@IDServicio)
 Begin
	goto Increment
 End
      
 INSERT INTO MASERVICIOS
 (IDServicio          
 ,IDProveedor          
 ,IDTipoProv                     
 ,IDTipoServ           
 ,Descripcion          
 ,Tarifario          
 ,Descri_tarifario                               
 ,FecCaducLectura          
 ,Activo        
 ,IDubigeo          
 ,Telefono          
 ,Celular          
 ,Comision          
 ,UpdComision          
 ,IDCat          
 ,Categoria          
 ,Capacidad          
 ,Observaciones          
 ,Lectura                     
 ,Transfer          
 ,TipoTransporte          
 ,AtencionLunes          
 ,AtencionMartes          
 ,AtencionMiercoles          
 ,AtencionJueves          
 ,AtencionViernes          
 ,AtencionSabado          
 ,AtencionDomingo          
 ,HoraDesde           
 ,HoraHasta                      

 ,DesayunoHoraDesde          
 ,DesayunoHoraHasta          
 ,IDDesayuno
      
 ,PreDesayunoHoraDesde          
 ,PreDesayunoHoraHasta          
 ,IDPreDesayuno           
 ,ObservacionesDesayuno          
      
 ,HoraCheckIn          
 ,HoraCheckOut          
 --,TelefonoRecepcion1           
 --,TelefonoRecepcion2           
 --,FaxRecepcion1           
 --,FaxRecepcion2           
 --,IncluyeServiciosDias          
 ,Dias          
      
 ,PoliticaLiberado          
 ,Liberado          
 ,TipoLib          
 ,MaximoLiberado           
 ,MontoL             
      
 ,SinDescripcion       
 ,ServicioVarios      
 ,IDCabVarios   
 ,CoPago
 , APTServicoOpcional
 , HoraPredeterminada
,UserMod          
    )          
 VALUES          
 (@IDServicio ,          
 @IDProveedor ,          
 @IDTipoProv ,                     
 @IDTipoServ,          
 @Descripcion ,          
 @Tarifario,          
 Case When ltrim(rtrim(@Descri_tarifario))='' Then Null Else @Descri_tarifario End,          
 Case When @FecCaducLectura='01/01/1900' Then Null Else @FecCaducLectura End,          
 @Activo,        
 @IDubigeo ,          
 Case When ltrim(rtrim(@Telefono))='' then Null Else @Telefono End ,          
 Case When ltrim(rtrim(@Celular))='' then Null Else @Celular End ,                  
 Case When @Comision=0 then Null Else @Comision End ,          
 @UpdComision,          
 @IDCat,          
 Case When @Categoria=0 then Null Else @Categoria End ,          
 Case When @Capacidad=0 then Null Else @Capacidad End ,          
 Case When ltrim(rtrim(Cast(@Observaciones as varchar(max))))='' then Null Else @Observaciones End ,          
 Case When ltrim(rtrim(Cast(@Lectura as varchar(max))))='' then Null Else @Lectura End ,          
 @Transfer,          
 Case When ltrim(rtrim(@TipoTransporte))='' then Null Else @TipoTransporte End,          
 @AtencionLunes ,          
 @AtencionMartes ,          
 @AtencionMiercoles ,          
 @AtencionJueves ,          
 @AtencionViernes,          
 @AtencionSabado,          
 @AtencionDomingo,          
 @HoraDesde ,          
 @HoraHasta ,          
                    
 Case When @DesayunoHoraDesde='01/01/1900 00:00:00' Then Null Else @DesayunoHoraDesde End,          
 Case When @DesayunoHoraHasta='01/01/1900 00:00:00' Then Null Else @DesayunoHoraHasta End,          
 Case When ltrim(rtrim(@IDDesayuno))='' then Null Else @IDDesayuno End ,          
      
 Case When @PreDesayunoHoraDesde='01/01/1900 00:00:00' Then Null Else @PreDesayunoHoraDesde End,          
 Case When @PreDesayunoHoraHasta='01/01/1900 00:00:00' Then Null Else @PreDesayunoHoraHasta End,          
 Case When ltrim(rtrim(@IDPreDesayuno))='' then Null Else @IDPreDesayuno End ,          
 Case When ltrim(rtrim(@ObservacionesDesayuno))='' then Null Else @ObservacionesDesayuno End ,          
 Case When @HoraCheckIn='01/01/1900 00:00:00' Then Null Else @HoraCheckIn End,          
 Case When @HoraCheckOut='01/01/1900 00:00:00' Then Null Else @HoraCheckOut End,          
 --Case When ltrim(rtrim(@TelefonoRecepcion1))='' then Null Else @TelefonoRecepcion1 End ,          
 --Case When ltrim(rtrim(@TelefonoRecepcion2))='' then Null Else @TelefonoRecepcion2 End ,          
 --Case When ltrim(rtrim(@FaxRecepcion1))='' then Null Else @FaxRecepcion1 End ,          
 --Case When ltrim(rtrim(@FaxRecepcion2))='' then Null Else @FaxRecepcion2 End ,          
 --@IncluyeServiciosDias,          
 @Dias,          
      
 @PoliticaLiberado,          
 Case When @Liberado=0 Then Null Else @Liberado End,          
 Case When ltrim(rtrim(@TipoLib))='' Then Null Else @TipoLib End,          
 Case When @MaximoLiberado=0 Then Null Else @MaximoLiberado End,          
 Case When @MontoL=0 Then Null Else @MontoL End,          
 @SinDescripcion,      
 @ServicioVarios,      
 Case When @IDCabVarios=0 Then Null Else @IDCabVarios End,        
 Case When LTRIM(rtrim(@CoTipoPago))='' Then Null Else @CoTipoPago End,
 @APTServicoOpcional,
 Cast(@HoraPredeterminada as datetime),
 @UserMod           
 )          
 Set @pIDServicio = @IDServicio         
END
