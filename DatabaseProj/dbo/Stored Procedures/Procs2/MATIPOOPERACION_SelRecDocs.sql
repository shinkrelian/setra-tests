﻿--JHD-20150513- order by Descripcion
CREATE Procedure dbo.MATIPOOPERACION_SelRecDocs
As
	Set Nocount On  
	Select IdToc, Descripcion From MATIPOOPERACION Where Not CoDestOper Is Null order by Descripcion
