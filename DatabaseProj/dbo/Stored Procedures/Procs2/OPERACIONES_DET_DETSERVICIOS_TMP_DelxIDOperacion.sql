﻿Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_TMP_DelxIDOperacion
	@IDOperacion int
As
	Set Nocount On
	
	Delete From OPERACIONES_DET_DETSERVICIOS_TMP Where IDOperacion_Det In
	(Select IDOperacion_Det From OPERACIONES_DET_TMP Where IDOperacion=@IDOperacion)
	
