﻿--JRF-20140815-Ordenar por el día del Servicio.  
CREATE Procedure dbo.OPERACIONES_DET_Sel_GuiasxIDCab_Rpt    
@IDCab int    
As    
 Set NoCount On    
 --+51     
 select distinct  
  u.Descripcion as CiudadGuia, p.ApPaterno+', '+ p.Nombre as NombreGuia,    
     'Mobile: '+ case when charindex('+51',Isnull(p.Movil1,''))> 0 Then '' else '+51 'End+ Isnull(p.Movil1,'') as Celular,
		(select top(1) od3.Dia
		 from OPERACIONES_DET_DETSERVICIOS odd3 Left Join OPERACIONES_DET od3 On od3.IDOperacion_Det = odd3.IDOperacion_Det --and odd3.IDServicio_Det= odd.IDServicio_Det
		 Left Join OPERACIONES o3 On od3.IDOperacion = o3.IDOperacion 
		 where o3.IDOperacion=o.IDOperacion and odd3.IDProveedor_Prg =p.IDProveedor and o3.IDCab=@IDCab 
		 Order by od3.Dia) as FirtDayService
 from OPERACIONES_DET_DETSERVICIOS odd     
 Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det    
 Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion     
 Left Join MAPROVEEDORES p On odd.IDProveedor_Prg = p.IDProveedor     
 Left Join MAUBIGEO u On p.IDCiudad = u.IDubigeo    
 where IDCab = @IDCab and p.IDTipoProv = '005' and p.Persona = 'N'    
Order by FirtDayService
