﻿--JHD-20141230-Agregar Nuevos campos: CoTipoHab_Oper y TipoHab_Oper  
--JHD-20150107-Se comento filtros de NuHabitacion y CoServicio para evitar mostrar todos con ''  
--JHD-JRF-20150316-Mostrar el texto del tipo de Habitacion (case when isnull((select top 1 cast....)
CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_HABITACIONES_Sel_List]    
 @CoServicio char(8)='',    
 @NuHabitacion tinyint=0    
AS    
BEGIN    
 Select    
  --CoServicio,    
   NuHabitacion,    
   --CoTipoHab=ISNULL(CoTipoHab,0),    
   --TipoHab=isnull((select TxDescripcion from dbo.MACONCEPTO_HOTEL where CoConcepto=CoTipoHab),''),    
   
   CoTipoHab=ISNULL(CoTipoHab,0),    
   TipoHab=isnull((select TxDescripcion from dbo.MACONCEPTO_HOTEL where CoConcepto=CoTipoHab),''),    
   
   QtCantidad,    
   QtCantMatri,    
   CoTipCama_Matri=ISNULL(CoTipCama_Matri,0),    
   TipCama_Matri=isnull((select TxDescripcion from dbo.MACONCEPTO_HOTEL where CoConcepto=CoTipCama_Matri),''),    
   QtCantTwin=IsNull(QtCantTwin,0),    
   CoTipCama_Twin=ISNULL(CoTipCama_Twin,0),    
   TipCama_Twin=isnull((select TxDescripcion from dbo.MACONCEPTO_HOTEL where CoConcepto=CoTipCama_Twin),''),    
   CoTipCama_Adic=ISNULL(CoTipCama_Adic,0),    
   TipCama_Adic=isnull((select TxDescripcion from dbo.MACONCEPTO_HOTEL where CoConcepto=CoTipCama_Adic),'')    
    
  ,QtCantTriple=IsNull(QtCantTriple,0),    
   CoTipCama_Triple=ISNULL(CoTipCama_Triple,0),    
   TipCama_Triple=isnull((select TxDescripcion from dbo.MACONCEPTO_HOTEL where CoConcepto=CoTipCama_Triple),'')    
   ,CoTipoHab_Oper=ISNULL(CoTipoHab_Oper,'')  
   --,TipoHab_Oper=isnull((select top 1 DetaTipo from MASERVICIOS_DET where IDServicio=@CoServicio and Tipo=CoTipoHab_Oper),'')  
   ,TipoHab_Oper= case when isnull((select top 1 cast(DetaTipo as varchar(max)) from MASERVICIOS_DET where IDServicio=@CoServicio and Tipo=CoTipoHab_Oper),'')  =''
					then isnull((select TxDescripcion from dbo.MACONCEPTO_HOTEL where CoConcepto=CoTipoHab_Oper),'')
				 else isnull((select top 1 DetaTipo from MASERVICIOS_DET where IDServicio=@CoServicio and Tipo=CoTipoHab_Oper),'')  end
  From dbo.MASERVICIOS_HOTEL_HABITACIONES    
 Where     
  (CoServicio = @CoServicio) /*or @CoServicio='') And     
  (NuHabitacion = @NuHabitacion or @NuHabitacion=0)*/  --borrar  
END;    
