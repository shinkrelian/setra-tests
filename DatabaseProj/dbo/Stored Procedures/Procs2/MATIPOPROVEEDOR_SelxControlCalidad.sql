﻿Create Procedure dbo.MATIPOPROVEEDOR_SelxControlCalidad
as
Set NoCount On
Select tp.IDTipoProv,Descripcion as TipoProv,c.NuCuest,c.NoDescripcion as Pregunta
from MATIPOPROVEEDOR tp Left Join MACUESTIONARIO c On tp.IDTipoProv=c.IDTipoProv
where FlTieneControlCalidad=1
