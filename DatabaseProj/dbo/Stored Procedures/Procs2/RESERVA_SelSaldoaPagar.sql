﻿CREATE Procedure dbo.RESERVA_SelSaldoaPagar
	@IDReserva	int,
	@pSaldoaPagar	numeric(12,2) Output
As
	Set Nocount On
		
	Declare @LimitePago	numeric(12,2)=isnull((Select 
		SUM (Case When IDMoneda='SOL' Then 
			dbo.FnCambioMoneda(TotalGen,'SOL','USD',sd.TC )
		Else TotalGen End ) 	
		From RESERVAS_DET rd Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
		Where rd.IDReserva=@IDReserva),0)
	Declare @Pagado	numeric(8,2)=isnull((Select 
		SUM (Case When o.TCambio IS NULL Then Total 
		Else 
			dbo.FnCambioMoneda(Total,'SOL','USD',o.TCambio ) End ) 	
		From ORDENPAGO_DET od Inner Join ORDENPAGO o On od.IDOrdPag=o.IDOrdPag
	Where od.IDReserva_Det In
	(Select IDReserva_Det From RESERVAS_DET Where IDReserva = @IDReserva)),0)
	
	
	Set @pSaldoaPagar=@LimitePago-@Pagado
