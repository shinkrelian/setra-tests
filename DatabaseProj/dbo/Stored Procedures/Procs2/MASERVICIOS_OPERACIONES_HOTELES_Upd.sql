﻿
--------------------------------------------------------------


--alter
CREATE PROCEDURE [dbo].[MASERVICIOS_OPERACIONES_HOTELES_Upd]
	@CoServicio char(8),
	@FlRecepcion bit,
	@FlEstacionamiento bit,
	@FlBoxDesayuno bit,
	@FlRestaurantes bit,
	@FlRestaurantesSoloDesayuno bit,
	@QtRestCantidad tinyint=0,
	@QtRestCapacidad tinyint=0,
	@QtTriples tinyint=0,
	@QtNumHabitaciones tinyint=0,
	@CoTipoCama char(3)='',
	@TxObservacion varchar(250)='',
	@UserMod char(4)=''
AS
BEGIN
	Update [dbo].[MASERVICIOS_OPERACIONES_HOTELES]
	Set
		[FlRecepcion] = @FlRecepcion,
 		[FlEstacionamiento] = @FlEstacionamiento,
 		[FlBoxDesayuno] = @FlBoxDesayuno,
 		[FlRestaurantes] = @FlRestaurantes,
 		[FlRestaurantesSoloDesayuno] = @FlRestaurantesSoloDesayuno,
 		[QtRestCantidad] = 	Case When @QtRestCantidad=0 Then Null Else @QtRestCantidad End,
 		[QtRestCapacidad] = Case When @QtRestCapacidad=0 Then Null Else @QtRestCapacidad End,
 		[QtTriples] =	Case When @QtTriples=0 Then Null Else @QtTriples End,
 		[QtNumHabitaciones] = Case When @QtNumHabitaciones=0 Then Null Else @QtNumHabitaciones End,
 		--[CoTipoCama] = Case When @CoTipoCama='' Then Null Else @CoTipoCama End,
 		[CoTipoCama] =Case When RTRIM(LTRIM(@CoTipoCama))='' Then Null Else @CoTipoCama End,
 		TxObservacion =Case When RTRIM(LTRIM(@TxObservacion))='' Then Null Else @TxObservacion End,
 		[UserMod] = Case When @UserMod='' Then Null Else @UserMod End
 	Where 
		[CoServicio] = @CoServicio
END;
