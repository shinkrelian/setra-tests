﻿CREATE Procedure [dbo].[MASERVICIOS_OPERACIONES_HOTELES_Upd_NuevoIDServicio]  
 @IDServicio char(8),    
 @IDServicioNue char(8),  
 @UserMod char(4)  
As  
 Set NoCount On  
   
 Update MASERVICIOS_OPERACIONES_HOTELES
  SET CoServicio=@IDServicioNue, FecMod=GETDATE(), UserMod=@UserMod  
 Where CoServicio=@IDServicio   