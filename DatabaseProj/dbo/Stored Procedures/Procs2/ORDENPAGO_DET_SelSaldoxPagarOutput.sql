﻿--HLF-20130730-Agregando cambio a Dolares a rd.TotalGen              
--HLF-20130906-Agregando parametros @IDMoneda, @TCambio y cambio de moneda al final         
--HLF-20130910-Agregando And rd1.Anulado=0      
--JRF-20150414-numeric(12,4) output       
CREATE Procedure dbo.ORDENPAGO_DET_SelSaldoxPagarOutput                
 @IDReserva_Det int,                 
 @IDMoneda char(3),            
 @TCambio numeric(8,2),            
 @pSaldo numeric(12,2) output                
As                
 Set Nocount On                 
              
 Select @pSaldo=              
              
 --Case When rd.IDMoneda=@IDMoneda Then dbo.FnCambioMoneda(rd.TotalGen ,rd.IDMoneda,@IDMoneda,sd.TC) Else--rd.TotalGen Else               
 --dbo.FnCambioMoneda(rd.TotalGen ,@IDMoneda,'USD',sd.TC)
 --End   
 dbo.FnCambioMoneda(rd.TotalGen ,rd.IDMoneda,@IDMoneda,sd.TC)           
 -                
 Isnull(                
 (Select   
 SUM(Case When op1.IDMoneda=@IDMoneda   
  Then od1.Total Else dbo.FnCambioMoneda(od1.Total,'SOL','USD',Isnull(sd1.TC,op1.TCambio)) End
  )                  
  From ORDENPAGO_DET od1                 
  Inner Join ORDENPAGO op1 On od1.IDOrdPag=op1.IDOrdPag                  
  Left Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                
  Inner Join RESERVAS_DET rd1 On od1.IDReserva_Det=rd1.IDReserva_Det And rd1.Anulado=0        
  Where od1.IDReserva_Det=rd.IDReserva_Det                
  ),0)  
 From RESERVAS_DET rd               
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det                
 Where rd.IDReserva_Det=@IDReserva_Det                
           
 --If @IDMoneda='SOL' And @TCambio>0           
 --Set @pSaldo = dbo.FnCambioMoneda(@pSaldo,'USD','SOL',@TCambio)          
    
 Set @pSaldo = ISNULL(@pSaldo,0)  
