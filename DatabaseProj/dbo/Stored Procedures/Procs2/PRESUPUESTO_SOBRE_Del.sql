﻿--Create Procedure dbo.PRESUPUESTO_SOBRE_Ins
--	@IDCab int,
----	@CoPrvInt char(6),
--	@CoPrvGui char(6),
--	@CoEjeOpe char(4),
--	@CoEjeFin char(4),
--	@IDProveedor char(6),
--	@CoEstado char(2),
--	@UserMod char(4) 
--As
--	Set Nocount On
	
--	Declare @NuPreSob int
--	Execute dbo.Correlativo_SelOutput 'PRESUPUESTO_SOBRE',1,10,@NuPreSob output
	
--	Insert Into PRESUPUESTO_SOBRE(NuPreSob,IDCab,
--	--CoPrvInt,
--		CoPrvGui,
--		CoEjeOpe,
--		CoEjeFin,IDProveedor,CoEstado,UserMod)
--	Values(@NuPreSob,@IDCab,
--	--@CoPrvInt,
--		@CoPrvGui,@CoEjeOpe,
--		Case When ltrim(ltrim(@CoEjeFin))='' Then Null Else @CoEjeFin End,
--		@IDProveedor,
--		@CoEstado,@UserMod)
--go

--Create Procedure dbo.PRESUPUESTO_SOBRE_Upd
--	@NuPreSob int,	
--	--@CoPrvInt char(6),
--	@CoPrvGui char(6),
--	@CoEjeOpe char(4),
--	@CoEjeFin char(4),
--	@CoEstado char(2),
--	@UserMod char(4) 
--As
--	Set Nocount On
	
--	Update PRESUPUESTO_SOBRE
--	Set --CoPrvInt=@CoPrvInt,
--		CoPrvGui=@CoPrvGui,CoEjeOpe=@CoEjeOpe,
--		CoEjeFin=Case When ltrim(ltrim(@CoEjeFin))='' Then Null Else @CoEjeFin End,
--		CoEstado=@CoEstado,UserMod=@UserMod, FecMod=GETDATE()
--	Where NuPreSob=@NuPreSob
--go

Create Procedure dbo.PRESUPUESTO_SOBRE_Del
	@NuPreSob int
As
	Set Nocount On
	
	Delete From PRESUPUESTO_SOBRE
	Where NuPreSob=@NuPreSob
