﻿
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_Sel_xIDServicio_Det]	
    @IDServicio_Det int
AS
BEGIN
	Set NoCount On
	
	Select Correlativo, PaxDesde, PaxHasta, Monto, R.IDTemporada,
	ISNULL(D.DescripcionAlterna,'') AS DescripcionAlterna,
	ISNULL(D.SingleSupplement,0) AS SingleSupplement,
	ISNULL(D.PoliticaLiberado,0) AS PoliticaLiberado,
	ISNULL(D.Liberado,0) AS Liberado, ISNULL(D.TipoLib,'') AS TipoLib, ISNULL(MaximoLiberado,0) AS MaximoLiberado, ISNULL(MontoL,0) AS MontoL
	From MASERVICIOS_DET_RANGO_APT R INNER JOIN MASERVICIOS_DET_DESC_APT D
	ON R.IDServicio_Det = D.IDServicio_Det AND R.IDTemporada = D.IDTemporada
	Where R.IDServicio_Det=@IDServicio_Det
	Order by R.IDTemporada, PaxDesde
END

