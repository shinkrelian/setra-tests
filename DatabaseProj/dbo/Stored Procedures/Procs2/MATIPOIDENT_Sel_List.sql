﻿
--MLL-20140522- Nuevo Campo CodPeruRail
CREATE Procedure [dbo].[MATIPOIDENT_Sel_List]
	@IDIdentidad char(3),
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion, IDIdentidad , CodPeruRail
	From MATIPOIDENT
	Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')
	and (IDIdentidad=@IDIdentidad Or LTRIM(RTRIM(@IDIdentidad))='')
