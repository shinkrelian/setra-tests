﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_CboXTipoServ]
				@IDServicio char(8)
AS
BEGIN
	Set NoCount On
	Select IDTemporada, NombreTemporada, TipoTemporada
	From MATEMPORADA T INNER JOIN MASERVICIOS S ON T.TipoTemporada = S.APTServicoOpcional
	Where IDServicio = @IDServicio
	Order by NombreTemporada
END
