﻿--HLF-20140320-Comentando @IDOperacion, If not @IDReservaNue is null  
--HLF-20140321-Agregando And rd.Anulado=0  
--HLF-20140321-Agregando cursor  
--JRF-20140815-Agregando los Isnull a la FechaOut y colocando la longitud varchar(10)
CREATE Procedure [dbo].[OPERACIONES_UpdIDsReservasPostAnulacion]    
 @IDProveedor char(6),     
 @IDCab int,    
 @UserMod char(4)    
As    
 Set Nocount On    
 Declare @IDReservaNue int, @IDReservaAnt int,@IDReserva_Det int,   
 @IDServicio_Det int, @Dia smalldatetime, @FechaOut smalldatetime  
   
    
 --Reserva Nueva    
 Select @IDReservaNue = IDReserva    
 From RESERVAS     
 Where IDProveedor=@IDProveedor And IDCab=@IDCab And Anulado=0    
    
 If not @IDReservaNue is null  
 Begin  
  --Reserva Antigua    
    
  Select Top 1 @IDReservaAnt = IDReserva    
  From RESERVAS     
  Where IDProveedor=@IDProveedor And IDCab=@IDCab And Anulado=1    
  Order by IDReserva Desc    
    
     
  UPDATE OPERACIONES Set IDReserva=@IDReservaNue, UserMod=@UserMod, FecMod=GETDATE()    
  Where IDProveedor=@IDProveedor And IDCab=@IDCab And IDReserva=@IDReservaAnt    
     
     
  --Update OPERACIONES_DET     
  --Set IDReserva_Det=rd.IDReserva_Det,    
  --UserMod=@UserMod, FecMod=GETDATE()     
  --From OPERACIONES_DET op     
  --Inner Join RESERVAS r On r.IDCab=@IDCab and r.IDProveedor=@IDProveedor and r.Anulado=0     
  --Inner Join OPERACIONES o On r.IDReserva=o.IDReserva And op.IDOperacion=o.IDOperacion    
  --Inner Join RESERVAS_DET rd On r.IDReserva=rd.IDReserva And rd.Anulado=0  
    
  Declare curReservasDet cursor for  
  Select IDReserva_Det,IDServicio_Det,Dia,FechaOut From RESERVAS_DET Where IDReserva=@IDReservaNue  
  And Anulado=0  
    
  Open curReservasDet   
  FETCH NEXT FROM curReservasDet Into @IDReserva_Det,@IDServicio_Det,@Dia,@FechaOut  
  While @@FETCH_STATUS=0  
  Begin  
  Update OPERACIONES_DET Set IDReserva_Det=@IDReserva_Det, FecMod=GETDATE(), UserMod=@UserMod  
  Where   
     
   IDOperacion_Det =   
   (Select Top 1 IDOperacion_Det From OPERACIONES_DET od   
   Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab and o.IDProveedor=@IDProveedor  
   Where  
   IDReserva_Det Not In   
    (Select IDReserva_Det From RESERVAS_DET rd1   
    Inner Join RESERVAS r1 On r1.IDReserva=rd1.IDReserva And r1.IDCab=@IDCab     
    And r1.IDProveedor=@IDProveedor  
     And rd1.Anulado=0 and r1.Anulado=0  
    ))  
   And  
   IDServicio_Det=@IDServicio_Det And CONVERT(varchar(10),Dia,103)=CONVERT(varchar(10),@Dia,103)  
    And Isnull(CONVERT(varchar(10),FechaOut,103),'01/01/1900')=Isnull(CONVERT(varchar(10),@FechaOut,103),'01/01/1900')
      
      
    --print @@rowcount  
    FETCH NEXT FROM curReservasDet Into @IDReserva_Det,@IDServicio_Det,@Dia,@FechaOut  
  End  
 Close curReservasDet  
 Deallocate curReservasDet  
   
 End    
