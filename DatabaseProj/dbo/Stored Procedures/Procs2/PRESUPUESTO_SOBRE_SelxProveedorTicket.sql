﻿Create Procedure dbo.PRESUPUESTO_SOBRE_SelxProveedorTicket
	@CoProveedor char(6)
 As
	Set Nocount on
	
	Select NuPreSob as Codigo, NuPreSob as IDVoucher, c.Titulo, p.IDCab, c.IDFile, 
	'SOL' as IDMoneda, 'S/.' as Simbolo, p.SsSaldo as Total, 0 as IGV
	From PRESUPUESTO_SOBRE p inner join COTICAB c on p.IDCab=c.IDCAB
	Where (p.CoPrvGui=@CoProveedor or ltrim(rtrim(@CoProveedor))='') 
		And exists(Select NuPreSob From PRESUPUESTO_SOBRE_DET where NuPreSob=p.NuPreSob and CoPago='TCK')

