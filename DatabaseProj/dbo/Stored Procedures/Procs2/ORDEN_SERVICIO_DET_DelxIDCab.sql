﻿CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_DelxIDCab
@IDCab int
As
Set NoCount On
Delete from ORDEN_SERVICIO_DET 
where NuOrden_Servicio = (select NuOrden_Servicio from ORDEN_SERVICIO where IDCab = @IDCab)
