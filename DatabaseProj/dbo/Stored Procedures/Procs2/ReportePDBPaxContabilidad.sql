﻿ --JRF-20140529-Se debe considerar la fecha de inicio de los servicios, mas no la fecha de la cotizacion campo [COTICAB].[FecInicio]  
 --JHD-20141015-Se agrego filtro de pax
 --JHD-20141031-Se corrigio el campo FecIngresoPais para evitar aparecer nulo.
 --JHD-20141031-Se quito el filtro de   And RIGHT(idm.IDDebitMemo,2)='00' para la tabla de INGRESO_DEBIT_MEMO
 --JHD-20141106-Se quito el filtro para mostrar todos los estados (dm.IDEstado='PG') y se agrego una columna para mostrar el estado
 --JHD-20141106-Se cambio el FROM copiandose por el del reporte PDBPAX original para que salga la misma cantidad de registros
 --JHD-20141120-Se cambio el valor del Total y total x pax para mostrar en soles segun la fecha de facturación.
 --JHD-20141120-Se agrego un isnull al campo FeFacturac para mostrar la fecha de la tabla Documento.
 --JHD-20150105-Se agrego un filtro para no mostrar anulados: And dm.IDEstado<>'AN'    
 --JHD-20150105-Se cambio el orden del isnull para mostrar el campo FeFacturac: ISNULL(d.FeDocum,cc.FeFacturac), y x ende se arregla el monto segun el tipo de cambio correcto
 --JHD-20150105-Se agrego un distinct en el 1er select para no mostrar datos repetidos:  SELECT distinct IDFile,NumIdentidad, ... 
 --JHD-20150106-Se quitaron los top 1 del Select interno.
 --JHD-20150106-Se hizo inner join con tabla INGRESO_DOCUMENTO para evitar campos repetidos.
 --JHD-20150106-Se colocó top 1 para los campos DescFormaPago, DescBanco, CoOperacionBan,  Mes, Anio, en la consulta externa, para mostrar los datos del codigo de operación que tiene mayor monto. 
 --HLF-20150410-and d.CoEstado<>'AN'  
 --JRF-20151217-Left Join MAUBIGEO ub On {cp.IDPaisResidencia}...
 CREATE Procedure dbo.ReportePDBPaxContabilidad    
 @FechaMes smalldatetime
 As    
  
  Set Nocount on    
      
  Declare @cMes char(6)=CONVERT(varchar(6), @FechaMes,112)    
  Declare @cMesAnt char(6)=CONVERT(varchar(6), Dateadd(m,-1, @FechaMes),112)    
      
  SELECT distinct IDFile,NumIdentidad,--apellidos,
  ApellidoPaterno,ApellidoMaterno,
  --nombres,
  PrimerNombre,SegundoNombre,    
  Residencia,CoInternacPais,FecIngresoPais,FechaIn,FechaOut,  
  dbo.FnNumeroConDecimalesExactos(cast(TotalxPax as varchar(Max)),2) as TotalxPax,    
  Left(NuDocum,3) as SerieDoc, cast(Right(NuDocum,7) as int) as NumeroDoc,    
  FeFacturac,SsTotalDocumUSD,
  
  /*
  DescFormaPago,
   DescBanco,
  CoOperacionBan,    
  month(FecUltPago) as Mes,
  year(FecUltPago) as Anio     ,
 */
 
 (Select top 1 fp1.Descripcion From INGRESO_FINANZAS if1    
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso  
  Left Join MAFORMASPAGO fp1 On if1.IDFormaPago=fp1.IdFor    
  Where  id1.NuDocum=x.NuDocum and id1.IDTipoDoc=x.IDTipoDoc  Order by /*FeAcreditada*/ id1.SsMonto Desc) as DescFormaPago,    
    
   (Select top 1 bn1.Sigla From INGRESO_FINANZAS if1    
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso 
  Left Join MABANCOS bn1 On if1.IDBanco=bn1.IDBanco     
  Where  id1.NuDocum=x.NuDocum and id1.IDTipoDoc=x.IDTipoDoc  Order by /*FeAcreditada*/ id1.SsMonto Desc) as DescBanco,    
      
 
  (Select top 1 if1.CoOperacionBan From INGRESO_FINANZAS if1   
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso     
  Where  id1.NuDocum=x.NuDocum and id1.IDTipoDoc=x.IDTipoDoc Order by /*FeAcreditada*/ id1.SsMonto Desc) as CoOperacionBan,  
 
 month((Select top 1 if1.FeAcreditada From INGRESO_FINANZAS if1   
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso     
  Where  id1.NuDocum=x.NuDocum and id1.IDTipoDoc=x.IDTipoDoc Order by /*FeAcreditada*/ id1.SsMonto Desc)) as Mes,    
   
   year((Select top 1 if1.FeAcreditada From INGRESO_FINANZAS if1   
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso     
  Where  id1.NuDocum=x.NuDocum and id1.IDTipoDoc=x.IDTipoDoc Order by /*FeAcreditada*/ id1.SsMonto Desc)) as Anio
          
   /* 
  (Select top 1 FeAcreditada From INGRESO_FINANZAS if1
   inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso  
   Where id1.NuDocum=x.NuDocum  and id1.IDTipoDoc=x.IDTipoDoc Order by /*FeAcreditada*/ id1.SsMonto Desc) as FecUltPago    
   */
 
/* 
  DescFormaPago,
  
  CoOperacionBan,    
  month(FecUltPago) as Mes,
  year(FecUltPago) as Anio     ,
  */
  ,EstadoDebit
--  ,NuIngreso

  FROM    
  (    
  Select distinct     
  --dm.IDCab    
  --idm.IDDebitMemo ,    
  cc.IDFile, cp.NumIdentidad,     
  cp.Apellidos,    


  Case When CHARINDEX(' ',cp.Apellidos)=0 And CHARINDEX('-',cp.Apellidos)=0 Then     
  cp.Apellidos    
  Else    
	Case When CHARINDEX('-',cp.Apellidos)=0 Then 
		rtrim(ltrim(Left(cp.Apellidos,CHARINDEX(' ',cp.Apellidos)-1) ))    
	Else
		rtrim(ltrim(Left(cp.Apellidos,CHARINDEX('-',cp.Apellidos)-1) ))    
	End
  End as ApellidoPaterno ,     
  Case When CHARINDEX(' ',cp.Apellidos)=0  And CHARINDEX('-',cp.Apellidos)=0 Then        
  ''    
  Else    
  	Case When CHARINDEX('-',cp.Apellidos)=0 Then 
  		rtrim(ltrim(substring(cp.Apellidos,CHARINDEX(' ',cp.Apellidos)+1,100) ))    
  	else
  		rtrim(ltrim(substring(cp.Apellidos,CHARINDEX('-',cp.Apellidos)+1,100) ))    
  	end

  
  End as ApellidoMaterno ,     


  cp.Nombres,     
      
  Case When CHARINDEX(' ',cp.Nombres)=0 And CHARINDEX('-',cp.Nombres)=0 Then     
  cp.Nombres    
  Else    
	Case When CHARINDEX('-',cp.Nombres)=0 Then 
		rtrim(ltrim(Left(cp.Nombres,CHARINDEX(' ',cp.Nombres)-1) ))    
	Else
		rtrim(ltrim(Left(cp.Nombres,CHARINDEX('-',cp.Nombres)-1) ))    
	End
  End as PrimerNombre ,     
  Case When CHARINDEX(' ',cp.Nombres)=0  And CHARINDEX('-',cp.Nombres)=0 Then        
  ''    
  Else    
  	Case When CHARINDEX('-',cp.Nombres)=0 Then 
  		rtrim(ltrim(substring(cp.Nombres,CHARINDEX(' ',cp.Nombres)+1,100) ))    
  	else
  		rtrim(ltrim(substring(cp.Nombres,CHARINDEX('-',cp.Nombres)+1,100) ))    
  	end

  
  End as SegundoNombre ,     
    
      
  Case When ub.IDubigeo='000001' then '' else ub.Descripcion End as Residencia,
  IsNull(ub.CoInternacPais,'') as CoInternacPais,
  --cp.FecIngresoPais,
  Isnull(cp.FecIngresoPais,cc.FecInicio) As FecIngresoPais,
  cc.FecInicio as FechaIn, cc.FechaOut ,    
      
  (Select COUNT(*) From COTIDET Where IDCab=isnull(dm.IDCab,0) And DebitMemoPendiente=1) as DebitMemoPendiente,    
  
  --cc.FeFacturac
  
  --FeFacturac=ISNULL(cc.FeFacturac,d.FeDocum)
   FeFacturac=ISNULL(d.FeDocum,cc.FeFacturac)
  
  , d.NuDocum, 

  --jorge
  --d.SsTotalDocumUSD,
  --SsTotalDocumUSD=dbo.FnCambioMoneda(d.SsTotalDocumUSD,d.IDMoneda,'SOL',DBO.FnTipoDeCambioVtaUltimoxFecha(ISNULL(cc.FeFacturac,d.FeDocum)))
   SsTotalDocumUSD=dbo.FnCambioMoneda(d.SsTotalDocumUSD,d.IDMoneda,'SOL',DBO.FnTipoDeCambioVtaUltimoxFecha(ISNULL(d.FeDocum,cc.FeFacturac)))
   ,
   
   --d.SsTotalDocumUSD/(cc.NroPax+ISNULL(cc.NroLiberados,0)) as TotalxPax,    
   --TotalxPax=(dbo.FnCambioMoneda(d.SsTotalDocumUSD,d.IDMoneda,'SOL',DBO.FnTipoDeCambioVtaUltimoxFecha(ISNULL(cc.FeFacturac,d.FeDocum))))/(cc.NroPax+ISNULL(cc.NroLiberados,0)),    
  TotalxPax=(dbo.FnCambioMoneda(d.SsTotalDocumUSD,d.IDMoneda,'SOL',DBO.FnTipoDeCambioVtaUltimoxFecha(ISNULL(d.FeDocum,cc.FeFacturac))))/(cc.NroPax+ISNULL(cc.NroLiberados,0)),    
  --bn.Sigla as DescBanco,fp.Descripcion as DescFormaPago, --ifn.CoOperacionBan,     
   
   
   --nuevos campos
  /*
   ban.sigla as DescBanco,
   f.Descripcion as DescFormaPago,
   ing.CoOperacionBan as CoOperacionBan,
   ing.FeAcreditada  as FecUltPago,
*/

    /*
 (Select top 1 bn1.Sigla From INGRESO_FINANZAS if1    
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso 
  Left Join MABANCOS bn1 On if1.IDBanco=bn1.IDBanco     
  Where if1.NuIngreso=idm.NuIngreso and id1.NuDocum=d.NuDocum and id1.IDTipoDoc=d.IDTipoDoc  Order by /*FeAcreditada*/ id1.SsMonto Desc) as DescBanco,    
    
    
 (Select top 1 fp1.Descripcion From INGRESO_FINANZAS if1    
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso  
  Left Join MAFORMASPAGO fp1 On if1.IDFormaPago=fp1.IdFor    
  Where if1.NuIngreso=idm.NuIngreso and id1.NuDocum=d.NuDocum and id1.IDTipoDoc=d.IDTipoDoc  Order by /*FeAcreditada*/ id1.SsMonto Desc) as DescFormaPago,    
    
    
 (Select top 1 if1.CoOperacionBan From INGRESO_FINANZAS if1   
 inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso     
  Where if1.NuIngreso=idm.NuIngreso and id1.NuDocum=d.NuDocum and id1.IDTipoDoc=d.IDTipoDoc Order by /*FeAcreditada*/ id1.SsMonto Desc) as CoOperacionBan,    
    
    
  (Select top 1 FeAcreditada From INGRESO_FINANZAS if1
   inner join INGRESO_DOCUMENTO id1 on if1.NuIngreso=id1.NuIngreso  
   Where if1.NuIngreso=idm.NuIngreso and id1.NuDocum=d.NuDocum  and id1.IDTipoDoc=d.IDTipoDoc Order by /*FeAcreditada*/ id1.SsMonto Desc) as FecUltPago    
   */
   --jorge 
   --,
    case dm.IDEstado     
	when 'PD' Then 'PENDIENTE'
	when 'PG' Then 'PAGADO'
	when 'AN' Then 'ANULADO'
 Else '' End as EstadoDebit
  --
 -- ,idm.NuIngreso
  ,d.IDTipoDoc
  FROM
   INGRESO_DEBIT_MEMO idm Right Join DEBIT_MEMO dm On idm.IDDebitMemo = dm.IDDebitMemo    
 Left Join COTICAB cc On dm.IDCab = cc.IDCAB Left Join COTIPAX cp On cc.IDCAB = cp.IDCab    
 Left Join MAUBIGEO ub On cp.IDPaisResidencia = ub.IDubigeo 
 Left Join MATIPOIDENT ti On cp.IDIdentidad = ti.IDIdentidad      
 Left Join INGRESO_FINANZAS ing On idm.NuIngreso = ing.NuIngreso    
 Left Join MAFORMASPAGO f On ing.IDFormaPago = f.IdFor    
 Left Join MABANCOS ban On ing.IDBanco = ban.IDBanco    
  
  left Join DOCUMENTO d On cc.IDCAB=isnull(d.IDCab,0)
  
  inner join INGRESO_DOCUMENTO id on id.NuIngreso=ing.NuIngreso
  and id.IDTipoDoc=d.IDTipoDoc and id.NuDocum=d.NuDocum
  
  --And d.IDTipoDoc='FAC' And Left(d.NuDocum,3)='001'    
  
  /*  
  FROM DEBIT_MEMO dm     
  Inner Join INGRESO_DEBIT_MEMO idm On idm.IDDebitMemo = dm.IDDebitMemo  and idm.SsSaldoNuevo=0        
  --And RIGHT(idm.IDDebitMemo,2)='00'    
  --Inner Join INGRESO_FINANZAS ifn On idm.NuIngreso=ifn.NuIngreso    
  --Inner Join COTIDET cd On dm.IDCab     
  Inner Join COTICAB cc On isnull(dm.IDCab,0)=cc.IDCAB and cc.Estado='A'    
  Inner Join DOCUMENTO d On cc.IDCAB=isnull(d.IDCab,0)
  And d.IDTipoDoc='FAC' And Left(d.NuDocum,3)='001'    
  --jorge
  --And d.CoEstado<>'AN'    
  Inner Join COTIPAX cp On cp.IDCab=cc.IDCAB    
  Left Join MAUBIGEO ub On cp.IDNacionalidad=ub.IDubigeo    
  --Left Join MABANCOS bn On ifn.IDBanco=bn.IDBanco    
  --Left Join MAFORMASPAGO fp On ifn.IDFormaPago=fp.IdFor    
  */
  --jorge 
  --WHERE dm.IDEstado='PG'     
  
  WHERE 
   --and 
   cp.flnoshow=0
   
   --JORGE
   and
     cc.Estado='A'    
   
    And dm.IDEstado<>'AN'    
   
   AND
   
   d.IDTipoDoc='FAC' And Left(d.NuDocum,3)='001'  and d.CoEstado<>'AN'  
   
  ) as X    
  WHERE     
  --DebitMemoPendiente=0 And    
  --(((CONVERT(varchar(6), FecUltPago,112)=@cMes OR     
  --CONVERT(varchar(6), FecUltPago,112)=@cMesAnt))    
  -- And CONVERT(varchar(6), isnull(FeFacturac,'01/01/1900'),112)=@cMes)    
  --Or    
  --(CONVERT(varchar(6), FecUltPago,112)=@cMes And     
  --CONVERT(varchar(6), isnull(FeFacturac,'01/01/1900'),112)=@cMesAnt )    
  --order by IDFile    
  
    MONTH(X.FecIngresoPais)=MONTH(@FechaMes) And YEAR(x.FecIngresoPais)=YEAR(@FechaMes)

Order By X.FecIngresoPais,X.IDFile
