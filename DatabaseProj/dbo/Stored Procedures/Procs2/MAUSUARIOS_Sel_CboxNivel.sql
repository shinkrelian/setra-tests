﻿--HLF-20130624-Volver a activar (ltrim(rtrim(@Nivel))='' Or Nivel=@Nivel)                  
--JRF-20131116-Agregar Vacio.          
--HLF-20131122-    Or u.Nivel=(Case @Nivel        When 'RES' Then 'SOP'            End)            
--HLF-20140109-Union 5    
-- And (@Nivel in ('RES','OPE') OR Ord<>5)    
-- And (Not @Nivel in ('RES','OPE') OR Ord<>2)  
--JRF-20140526-Agregar el nivel de Supervisora de Counter al Query.  
CREATE Procedure [dbo].[MAUSUARIOS_Sel_CboxNivel]                 
 @Nivel Char(3),                  
 @bTodos bit                  
As                  
                  
 Set NoCount On                  
                   
 Select IDUsuario, Usuario from                   
 (                  
    Select '' as IDUsuario,'<TODOS>' As Usuario,0 as Ord,0 as OrdenNivel                   
    Union          
    Select '' as IDUsuario,'' As Usuario,1 as Ord,0 as OrdenNivel            
    where @bTodos = 0                 
    Union                  
    Select IDUsuario,Usuario,2,n.Orden as OrdenNivel              
    From MAUSUARIOS u Left Join MANIVELUSUARIO n On u.Nivel=n.IDNivel              
    Where u.Activo='A' And                   
                
   (ltrim(rtrim(@Nivel))='' Or Nivel=@Nivel)                  
   /*Cambio temporal*/              
   --u.Nivel IN ('GER','SRE','JAR','RES')                  
   --And u.IdArea in('RS','GR','OP')              
   /*---*/              
               
   Union            
 Select u.IDUsuario,u.Usuario,3,0 as OrdenNivel              
    From MAUSUARIOS u             
    Where u.Activo='A' And             
    u.Nivel=(Case @Nivel When 'VTA' Then 'SVE'             
       When 'RES' Then 'SRE'             
       When 'OPE' Then 'SOP' End)            
    Or u.Nivel=(Case @Nivel         
       When 'RES' Then 'SOP'             
       End)            
                   
            
 UNION            
 Select u.IDUsuario,u.Usuario,4,0 as OrdenNivel              
    From MAUSUARIOS u             
    Where u.Activo='A' And             
    u.Nivel='GER'            
            
               
Union    
    
        
    Select IDUsuario,Usuario,5,n.Orden as OrdenNivel              
    From MAUSUARIOS u Left Join MANIVELUSUARIO n On u.Nivel=n.IDNivel              
    Where u.Activo='A' And                   
                
   (ltrim(rtrim(@Nivel))='' Or Nivel In ('RES','OPE'))                  
    
Union  
 Select  IDUsuario,Usuario,6,0 as OrdenNivel   
 from mausuarios  
 where @Nivel = 'RES' and Nivel in('COU','SCO')
               
 ) As X                  
 Where (@bTodos=1 Or Ord<>0)                 
 And (@Nivel in ('RES','OPE') OR Ord<>5)    
 And (Not @Nivel in ('RES','OPE') OR Ord<>2)    
 Order By --Usuario                
 Ord, OrdenNivel, Usuario            
