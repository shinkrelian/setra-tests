﻿
--HLF-20140224-Top 1, Top 1                
--HLF-20140819-  SD.CoMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where     
--  IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))                     
--   dbo.FnCambioMoneda(isnull(ODS.NetoProgram,ODS.CostoReal),ODS.IDMoneda,'USD',    
-- isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                   
--HLF-20140822-Cambios para hallar NroOrdenServicio    
--HLF-20141204-DescPaisProveedor  
--HLF-20141205-Case When idTipoOC='011' Then 0  
--Case When idTipoOC='011' Then VentaUSD Else 0 End as BoletaServicioExterior  
--HLF-20150205-@CoUbigeo_Oficina char(6)
--And p.CoUbigeo_Oficina=@CoUbigeo_Oficina
--HLF-20150206-Forzar a Factura cuando es @CoUbigeo_Oficina<>'' (Buenos Aires, Santiago)
--HLF-20150211-Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) ...
--HLF-20150303-and not NroOrdenServicio is null
--PPMG-20160407--Group By CoOrden_Servicio
CREATE Procedure [dbo].[OPERACIONES_SelOrdenesServicioPreLiquidacion]                  
 @IDCab int,
 @CoUbigeo_Oficina char(6)              
As
BEGIN                  
 Set Nocount On                  
 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)                  
 --Declare @TipoCambio as numeric(8,2) =(Select TipoCambio From COTICAB Where IDCAB=@IDCab)                
 Declare @MargenInterno as numeric(6,2)=10
 SELECT A.*,  
 Case When idTipoOC='011' and @CoUbigeo_Oficina='' Then 0   
 Else   
	Case When @CoUbigeo_Oficina='' Then
		Case When idTipoOC='001' Then VentaUSD Else 0 End  
	Else
		VentaUSD
	End
 End As FacturaExportacion,                  
 Case When idTipoOC='011' or @CoUbigeo_Oficina<>'' Then 0   
 Else  
 Case When idTipoOC='001' Then 0 Else VentaUSD End   
 End As BoletaNoExportacion,  
 Case When idTipoOC='011' and @CoUbigeo_Oficina='' Then VentaUSD Else 0 End as BoletaServicioExterior           
 FROM                  
  (                  
  SELECT                   
   ZZ.IDProveedor, ZZ.DescProveedor, ZZ.DescProveedorInternac,   
   DescPaisProveedor,ZZ.NroOrdenServicio,                  
   ZZ.CompraNeto, ZZ.IGVCosto,                   
   ZZ.IGV_SFE, ZZ.Moneda,                   
   ZZ.CompraNetoUSD+ZZ.IGVCosto+ZZ.IGV_SFE AS Total,                  
   ZZ.idTipoOC, ZZ.DescOperacionContab, ZZ.Margen,                  
   ZZ.CostoBaseUSD,                   
   ROUND( ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD                  
   --,YY.FacturaExportacion,YY.BoletaNoExportacion                  
  FROM                  
   (                  
   SELECT                   
    YY.IDProveedor, YY.DescProveedor, YY.DescProveedorInternac,   
    DescPaisProveedor,YY.NroOrdenServicio,                  
    YY.CompraNeto,YY.CompraNetoUSD, YY.IGVCosto,                   
    YY.IGV_SFE, --YY.CompraNetoUSD+YY.IGVCosto+YY.IGV_SFE AS Total,                  
    YY.Moneda, YY.idTipoOC, YY.DescOperacionContab, YY.Margen,                  
    --YY.CompraNeto+IGVCosto+IGV_SFE as CostoBaseUSD                   
    YY.CompraNetoUSD+IGVCosto as CostoBaseUSD                   
    --YY.VentaUSD,                  
    --YY.FacturaExportacion,YY.BoletaNoExportacion                  
   FROM                  
    (                  
    SELECT XX.IDProveedor, DescProveedor, DescProveedorInternac,   
     DescPaisProveedor,NroOrdenServicio,                  
     CompraNeto,CompraNetoUSD, IGVCosto,                   
     Case When idTipoOC='001' Then                   
       CompraNeto*(@IGV/100)                   
      Else                    
       Case When idTipoOC='006' Then (CompraNeto*(@IGV/100))*0.5 Else 0 End                  
      End as IGV_SFE,                   
     Moneda, idTipoOC, DescOperacionContab, Margen                   
    FROM                  
     (       
     SELECT IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,  
      --(SELECT Top 1--DISTINCT                  
      --LTRIM(RTRIM(SUBSTRING(NoArchAdjunto,CHARINDEX('.',NoArchAdjunto,0)-13,13)))                  
      -- FROM ADJUNTOSCORREOFILE WHERE NuCorreo IN                  
      --(                  
      --SELECT NuCorreo FROM CORREOFILE WHERE NuCorreo IN                  
      -- (                  
      -- SELECT NuCorreo FROM DESTINATARIOSCORREOFILE WHERE NuCorreo IN                  
      --  (                  
      --  SELECT NuCorreo FROM CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'                    
      --  AND (SELECT Top 1 ISNULL(p1.Email3,'')+','+ISNULL(p1.Email4,'')+','+                  
      --  ISNULL(p1.EmailRecepcion1,'')+','+ISNULL(p1.EmailRecepcion2,'')                  
      --  FROM MAPROVEEDORES p1                   
      --   WHERE p1.IDProveedor=Z.IDProveedor) LIKE '%'+NoCuentaPara+'%'                  
      --  )                  
      -- Union                  
      -- SELECT NuCorreo FROM DESTINATARIOSCORREOFILE WHERE NuCorreo IN                  
      --  (                       
      --  SELECT NuCorreo FROM CORREOFILE WHERE IDCab=@IDCab AND CoTipoBandeja='EE'  and                   
      --   NoCuentaPara In (select correo from MACORREOREVPROVEEDOR where IDProveedor=Z.IDProveedor)                  
      --  )                  
      -- )                  
      --)                   
      --) AS NroOrdenServicio,                    
      (Select substring(CoOrden_Servicio,1,8)+'-'+substring(CoOrden_Servicio,9,6) From ORDEN_SERVICIO Where IDCab=@IDCab And IDProveedor=Z.IDProveedor
	   Group By CoOrden_Servicio) AS NroOrdenServicio,                    
  CompraNeto, CompraNetoUSD, IGVCosto,Moneda,                   
      --Total,                   
      idTipoOC, DescOperacionContab, Margen                  
      --CostoBaseUSD,                  
      --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD                  
     FROM                  
      (                  
      SELECT IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,  
      SUM(CompraNeto) AS CompraNeto,   sum(CompraNetoUSD) as CompraNetoUSD,              
      SUM(IGVCosto) AS IGVCosto, Moneda,                   
      --SUM(Total) AS Total,                   
      idTipoOC, DescOperacionContab,                  
      AVG(Margen) AS Margen                  
      --SUM(Total) as CostoBaseUSD                  
      FROM                  
       (                  
       --Declare @IDCab int=2309                  
       SELECT X.*,                  
       --(SELECT AVG(MargenAplicado) FROM COTIDET cd1                   
       -- WHERE cd1.IDCAB=@IDCab AND cd1.IDProveedor=X.IDProveedorOpe  And isnull(cd1.IDDetRel,0)=0            
       -- ) AS Margen,            
       --dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) AS Margen,            
		 Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 
			dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) 
		 Else
			@MargenInterno
		 End AS Margen,                      
        Case When idTipoOC='002' Then             
   CompraNeto*(@IGV/100)             
    Else            
   0            
  End as IGVCosto            
  FROM                  
        (                  
        SELECT ODS.IDProveedor_Prg as IDProveedor, pint.NombreCorto as DescProveedorInternac,                  
        P.NombreCorto as DescProveedor,                     
        paPI.Descripcion As DescPaisProveedor,  
        ODS.IDOperacion_Det,                  
 Case When ODS.IngManual=0 Then             
    Case When isnull(NetoCotizado,0) = 0 then                            
 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,          
    OD.NroPax+isnull(od.NroLiberados,0)),                             
    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,ods.IDMoneda,isnull(@TipoCambio,SD.TC))                     
    SD.CoMoneda,ods.IDMoneda,isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     
  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                     
    else                 
  --isnull(ODS.NetoProgram,ODS.CostoReal)            
   ods.NetoCotizado      
    End             
 Else      
  ODS.NetoProgram      
 End      
  --End            
  As CompraNeto,                              
  --Case When ODS.IDMoneda='USD' Then            
  -- isnull(ODS.NetoProgram,ODS.CostoReal)            
  --Else             
 Case When ODS.IngManual=0 Then             
  Case When isnull(NetoCotizado,0) = 0 then                            
  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(od.NroLiberados,0)),                             
  --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,'USD',isnull(@TipoCambio,SD.TC))                     
  SD.CoMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where     
  IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))                     
  else                 
   --dbo.FnCambioMoneda(isnull(ODS.NetoProgram,ODS.CostoReal),ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))                   
   dbo.FnCambioMoneda(isnull(ODS.NetoProgram,ODS.CostoReal),ODS.IDMoneda,'USD',    
 isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     
  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                   
  End            
  --End      
 Else      
  --dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))      
  dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',    
 isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     
  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))      
 End      
  As CompraNetoUSD,                              
        ODS.TotImpto,                  
        ODS.IDMoneda as Moneda,              
        --ODS.CostoReal+ODS.TotImpto as Total ,               
        sd.idTipoOC,                  
        tox.Descripcion as DescOperacionContab,                  
        --SCot.IDProveedor as IDProveedorCot,                  
        --PCot.NombreCorto as DescProveedorCot,                  
        --SDCot.IDServicio_Det                  
        ODS.IDServicio_Det,                  
        O.IDProveedor as IDProveedorOpe                  
        ,SD.IDServicio                  
        FROM OPERACIONES_DET_DETSERVICIOS ODS                      
        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det                   
   AND OD.IDServicio=ODS.IDServicio                  
        --And not ODS.TotalCotizado is null              
        --AND OD.IDServicio=ODS.IDServicio                  
        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=@IDCab                  
        --INNER JOIN MASERVICIOS S ON OD.IDServicio=S.IDServicio AND S.IDProveedor=O.IDProveedor                  
        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              
        LEFT JOIN COTICAB CC ON CC.IDCAB=O.IDCab                  
        LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor                  
         --INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det                      
         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Cot=SD.IDServicio_Det                  
         LEFT join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc                  
                         --LEFT JOIN MASERVICIOS_DET SDCot ON ODS.IDServicio_Det=SDCot.IDServicio_Det                  
        --LEFT JOIN MASERVICIOS SCot ON SDCot.IDServicio=SCot.IDServicio                  
        --LEFT JOIN MAPROVEEDORES PCot ON SCot.IDProveedor=PCot.IDProveedor                  
        Left Join MAPROVEEDORES pint On pint.IDProveedor=p.IDProveedorInternacional                  
        Left Join MAUBIGEO ubPI On p.IDCiudad=ubPI.IDubigeo  
        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo  
        Where not ODS.IDProveedor_Prg is null and not P.NombreCorto is null                  
         And isnull(p.IDUsuarioTrasladista,'0000')='0000'              
         --And p.IDUsuarioTrasladista is null              
         And ODS.IDProveedor_Prg <> '001935'--SETOURS CUSCO                  
         And ODS.Activo=1-- and ODS.TotalProgram>0          
         And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')
        ) AS X                  
       ) AS Y                  
      GROUP BY IDProveedor,DescProveedor,DescProveedorInternac,DescPaisProveedor,Moneda,idTipoOC,DescOperacionContab                  
      ) as Z                  
     ) AS XX                  
    ) AS YY                  
   ) AS ZZ                    
  ) AS A                    
      WHERE CompraNeto>0   and not NroOrdenServicio is null
END
