﻿
--HLF-20150108-Case When @FlDesdeOtraForEgreso=0 Then...
CREATE Procedure dbo.ORDENPAGO_UpdxDocumento  
@IDOrdPag int,  
@CoEstado char(2),  
@SsSaldo numeric(8,2),  
@SsDifAceptada numeric(8,2),  
@TxObsDocumento varchar(Max),  
@FlDesdeOtraForEgreso bit,
@UserMod char(4)  
As  
BEGIN  
Set NoCount On  
Update ORDENPAGO  
 Set CoEstado=@CoEstado,  
  SsSaldo= Case When @SsSaldo=0 then Null Else @SsSaldo End,  
  SsDifAceptada= Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,  
  --TxObsDocumento= Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End,  
  TxObsDocumento= 
	Case When @FlDesdeOtraForEgreso=0 Then
		Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
	Else
		TxObsDocumento
	End,
  UserMod=@UserMod,  
  FecMod=GETDATE(),  
  --  
  UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end  
Where IDOrdPag= @IDOrdPag  
END;  
  
  
