﻿
--Sp_HelpText PRE_LIQUI_DET_Sel_List Go

CREATE Procedure dbo.PRE_LIQUI_DET_Sel_List    
@IDNotaVenta char(10)    
As    
 Set NoCount On    
 SELECT [IDNotaVenta]    
    ,[IDPreLiquiDet]    
    ,isnull(pl.[IDOperacion_Det]  ,0) As IDOperacion_Det  
    ,tp.Descripcion As DescTipoOper  
    ,p.NombreCorto As DescProveedor    
    ,pl.[Descripcion]    
    ,pl.[IDTipoOC]  
    ,[SubTotal]    
    ,[TotalIGV]    
    ,pl.[Total]    
 from PRE_LIQUI_DET pl Left Join OPERACIONES_DET od On pl.IDOperacion_Det = od.IDOperacion_Det    
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det    
 Left Join MASERVICIOS s on sd.IDServicio = s.IDServicio    
 Left Join MAPROVEEDORES p on s.IDProveedor = p.IDProveedor    
 Left Join MATIPOOPERACION tp On pl.IDTipoOC = tp.IdToc  
 Where IDNotaVenta = @IDNotaVenta  
 Order by tp.Orden    
