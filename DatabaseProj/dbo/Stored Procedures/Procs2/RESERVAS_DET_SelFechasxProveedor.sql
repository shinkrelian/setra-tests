﻿
Create Procedure dbo.RESERVAS_DET_SelFechasxProveedor  
 @IDCab int,  
 @IDProveedor char(6) 
As  
 Set Nocount On  
   
 SELECT Distinct Dia,FechaOut FROM RESERVAS_DET  
 WHERE IDReserva IN (SELECT IDReserva FROM RESERVAS WHERE IDCab=@IDCab   
 AND IDProveedor=@IDProveedor)   
 Order by Dia, FechaOut
