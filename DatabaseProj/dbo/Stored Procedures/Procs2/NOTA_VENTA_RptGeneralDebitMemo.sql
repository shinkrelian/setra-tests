﻿CREATE Procedure NOTA_VENTA_RptGeneralDebitMemo    
@IDNotaVenta char(10)  
As  
 Set NoCount On  
 Declare @IDCab int  
 set @IDCab = (select IDCab from NOTA_VENTA where IDNotaVenta = @IDNotaVenta)  
   
 select   
     cl.RazonComercial As DescCliente,Convert(char(10),Getdate(),103) As Emisiom,  
     c.Titulo,(select CONVERT(char(10),MIN(Dia),103) from COTIDET where IDCab = @IDCab) As FechaIn,  
     c.IDFile,(select CONVERT(Char(10),Max(Dia),103) from COTIDET where IDCab = @IDCab) As FechaOut,  
     u.Nombre As DescNombre,c.NroPax,b.Descripcion As DescBanco,isnull(c.TipoCambio,0.00) As TipoCambio,  
       
        substring(dm.IDDebitMemo,0,9) +'  '+substring(dm.IDDebitMemo,9,9) As DEBIT_MEMO,  
        Convert(char(10),dm.Fecha,103) As Fecha,dm.Total,cl.RazonComercial,  
        Convert(char(10),dm.FecVencim,103) As FecVencim,  
        0.00 As Monto,0.00 As Comision,0.00 As GTransfer,  
        (select COUNT(*) from COTIVUELOS where IDCab = @IDCab and TipoTransporte = 'T') As RegTren,  
        (select COUNT(*) From NOTA_VENTA_DET nvd     
    Left Join RESERVAS_DET rd On nvd.IDReserva_Det=rd.IDReserva_Det    
    Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det    
        where IDNotaVenta = @IDNotaVenta and sd.idTipoOC <> '005') As RegDetalle,  
        (select COUNT(*) From NOTA_VENTA_DET nvd     
    Left Join RESERVAS_DET rd On nvd.IDReserva_Det=rd.IDReserva_Det    
    Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det    
        where IDNotaVenta = @IDNotaVenta and sd.idTipoOC = '005') As RegDetalleGastosAdm  ,
--Datos PreLiquidet
        (select COUNT(*) From PRE_LIQUI_DET pd     
    Left Join OPERACIONES_DET od On pd.IDOperacion_Det = od.IDOperacion_Det
    Left Join MASERVICIOS_DET sd On od.IDServicio_Det =sd.IDServicio_Det    
        where IDNotaVenta = @IDNotaVenta and pd.idTipoOC <> '005') As RegDetalleLiquid,  
        (select COUNT(*) From PRE_LIQUI_DET pd     
    Left Join OPERACIONES_DET od On pd.IDOperacion_Det = od.IDOperacion_Det
    Left Join MASERVICIOS_DET sd On od.IDServicio_Det =sd.IDServicio_Det    
        where IDNotaVenta = @IDNotaVenta and sd.idTipoOC = '005') As RegDetalleGastosAdmLiquid  
                
 from DEBIT_MEMO dm Left Join COTICAB c On dm.IDCab = c.IDCAB  
 Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente  
 Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario  
 Left Join MABANCOS b On c.IDBanco = b.IDBanco  
 Where dm.IDCab = @IDCab  
