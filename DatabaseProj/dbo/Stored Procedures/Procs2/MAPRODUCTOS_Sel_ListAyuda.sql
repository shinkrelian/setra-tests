﻿CREATE Procedure MAPRODUCTOS_Sel_ListAyuda  
 @NoProducto varchar(100),
 @CoRubro tinyint=0
As    
 Set NoCount On    
    
 SELECT Distinct * FROM    
 (    
 Select     
 p.NoProducto as Descripcion,    
 p.CoProducto as Codigo,     
 p.CoRubro as CodTipo, r.NoRubro as DescTipo,
 '' as IDCiudad,
 '' as DescCiudad, 
 '' as IDPais, 
 '' as Margen,
'' as OperadorInt    
 From MAPRODUCTOS p Left Join MARUBRO r On p.CoRubro=r.CoRubro    

 Where     
  (p.CoRubro=@CoRubro OR @CoRubro=0) And p.FlActivo = 1
 ) AS X    
 Where (Descripcion Like '%'+@NoProducto+'%' Or LTRIM(rtrim(@NoProducto))='')    
 Order by Descripcion    
