﻿--JRF-20120911- Nuevo Campo EsOficina    
--JRF-20130903- Nuevo campo CodigoINC  
--JRF-20131014- Nuevo campo CodigoPeruRail
--MLL-20140522- Nuevo Campo CoInternacPais
CREATE Procedure [dbo].[MAUBIGEO_Upd]        
@IDUbigeo char(6),        
@Descripcion varchar(50),         
@TipoUbig varchar(20),          
@Prove char(1),        
@NI char(1),        
@DC char(3),               
@IDPais char(6),     
@CodigoINC varchar(3),
@CodigoPeruRail varchar(5),     
@CoInternacPais char(2) , -- Nuevo campo desde 22/05/14 
@EsOficina bit ,      
@UserMod char(4)        
As        
        
 Set NoCount On        
        
 Update MAUBIGEO Set         
 Descripcion=@Descripcion,        
 TipoUbig=@TipoUbig,        
 Prove=@Prove,        
 NI=@NI,        
 DC=Case When ltrim(rtrim(@DC))='' Then Null Else @DC End,          
 IDPais=Case When ltrim(rtrim(@IDPais))='' Then Null Else @IDPais End,        
 CodigoINC =Case When ltrim(rtrim(@CodigoINC))='' Then Null Else @CodigoINC End,  
 CodigoPeruRail = Case When Ltrim(Rtrim(@CodigoPeruRail))='' Then Null Else @CodigoPeruRail End,
 CoInternacPais = Case When Ltrim(Rtrim(@CoInternacPais))='' Then Null Else @CoInternacPais End,
 EsOficina=@EsOficina,    
 UserMod=@UserMod ,        
 FecMod=GETDATE()         
 Where IDUbigeo = @IDUbigeo         
