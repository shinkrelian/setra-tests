﻿--JRF-07062014-Considerar la moneda (del Ingreso y lo Facturado)      
--HLF-20140612-DEBIT_MEMO_DOCUMENTO        
--Left Join MACLIENTES cl On cl.IDCliente=isnull(d.IDCliente,cc.IDCliente)      
--HLF-20140613-and d.FeDocum Between @FecEmisionRango1 And @FecEmisionRango2+' 23:59:59'    
--and (LTRIM(rtrim(@IDBanco))='000' Or dm.IDBanco = @IDBanco)         
--Agregando columnas IDTipoDoc, CoEstado 
--JRF-20140721-Left Join INGRESO_DEBIT_MEMO idm ...
--JRF-20140721-Agregar Filtro de fecha Ingreso
CREATE Procedure [dbo].[Reporte_CuentaCorrientexCliente]        
@IDCliente char(6),          
@NuDocum char(10),          
@DiaVencidos smallint,          
@IDDebitMemo char(10),          
@IDBanco char(3),          
@FecEmisionRango1 smalldatetime,          
@FecEmisionRango2 smalldatetime,
@FecIngresoRango1 smalldatetime,
@FecIngresoRango2 smalldatetime
AS          
Set NoCount On          
--select 'La Route des Andes' as NombreCliente,'001' as Serie,'021327' as Numero,CAST('01/04/2014' as smalldatetime) as FechaEmision,          
-- CAST('06/06/2014' as smalldatetime) as FechaVencimiento,          
-- 0 as DiasVencidos,1609.0 as TotalFacturado,'14060131-00' as DebitMemo,          
-- 'BCP' as NombreBanco,'0100121' as NumeroOperacion,           
-- CAST('20/04/2014' as smalldatetime) as FechaOperacion,          
-- 'Transferencia' as TipoOperacion,1609.0 as MontoDeposito,0.0 as SaldoPorCobrar          
          
Select distinct *,
	  ROW_NUMBER()Over (Partition by NumeroOperacion Order by NumeroOperacion desc,NombreCliente,DebitMemo, FechaEmision) as PrimerItem
	  from (          
select cl.RazonComercial as NombreCliente,LEFT(d.NuDocum,3) as Serie,RIGHT(d.NuDocum,7) as Numero,           
    d.FeDocum as FechaEmision,dm.FecVencim as FechaVencimiento,          
    --Case when DATEDIFF(d,dm.FecVencim,d.FeDocum) < 0 then 0 else DATEDIFF(d,dm.FecVencim,d.FeDocum)End as DiasVencidos,          
    --DATEDIFF(d,dm.FecVencim,@FecEmisionRango2) as DiasVencidos,  --@FecEmisionRango2 as Fecha2 ,        
    Case when DATEDIFF(d,dm.FecVencim,@FecEmisionRango2) < 0 then 0 else DATEDIFF(d,dm.FecVencim,@FecEmisionRango2)End as DiasVencidos,          
    d.SsTotalDocumUSD as TotalFacturado,left(dm.IDDebitMemo,8)+'-'+RIGHT(dm.IDDebitMemo,2) as DebitMemo,          
    b.Descripcion as NombreBanco,ifn.CoOperacionBan as NumeroOperacion,ifn.FeAcreditada as FechaOperacion, 
    Isnull(ifn.SsGastosTransferencia,0) as SsGastosTransferencia,Isnull(ifn.SsBanco,0) as SsComisionBanco,
    f.Descripcion as TipoOperacion,        
    --idm.SsPago  +idm.SsGastosTransferencia as MontoDeposito,        
    ifn.SsRecibido  as MontoDeposito,        
    idm.SsSaldoNuevo as SaldoPorCobrar  ,ifn.IDMoneda  as MonedaCobranza,d.IDMoneda as MonedaDocumento,  
    d.IDTipoDoc, d.CoEstado  
from DEBIT_MEMO dm               
--Inner Join INGRESO_DEBIT_MEMO idm On idm.IDDebitMemo = dm.IDDebitMemo and dm.IDEstado <> 'AN'--and idm.SsSaldoNuevo=0                  
Left Join INGRESO_DEBIT_MEMO idm On idm.IDDebitMemo = dm.IDDebitMemo and dm.IDEstado <> 'AN'--and idm.SsSaldoNuevo=0                  

--And RIGHT(idm.IDDebitMemo,2)='00'              
Left Join COTICAB cc On isnull(dm.IDCab,0)=cc.IDCAB and cc.Estado='A'              
--Right Join DOCUMENTO d On cc.IDCAB=isnull(d.IDCab,0)       
Inner Join DEBIT_MEMO_DOCUMENTO dmd On dm.IDDebitMemo=dmd.IDDebitMemo      
Left Join DOCUMENTO d On dmd.NuDocum=d.NuDocum And dmd.IDTipoDoc=d.IDTipoDoc      
--Left Join MACLIENTES cl On cl.IDCliente=isnull(cc.IDCliente,d.IDCliente)            
Left Join MACLIENTES cl On cl.IDCliente=isnull(d.IDCliente,cc.IDCliente)            
Left Join INGRESO_FINANZAS ifn On idm.NuIngreso = ifn.NuIngreso         
Left Join MABANCOS b on ifn.IDBanco = b.IDBanco          
Left Join MAFORMASPAGO f On ifn.IDFormaPago = f.IdFor          
where --d.CoEstado <> 'AN'           
       (LTRIM(RTRIM(@IDCliente))='' Or cl.IDCliente = @IDCliente)           
   and (LTRIM(rtrim(@NuDocum))='' Or d.NuDocum = @NuDocum)           
   and (LTRIM(rtrim(@IDBanco))='000' Or dm.IDBanco = @IDBanco)          
   and (LTRIM(rtrim(@IDDebitMemo))='' Or dm.IDDebitMemo = @IDDebitMemo)          
   and d.FeDocum Between @FecEmisionRango1 And @FecEmisionRango2+' 23:59:59'    
) as X          
Where (@DiaVencidos = 0 Or X.DiasVencidos = @DiaVencidos) and
(CONVERT(char(10),@FecIngresoRango1,103)='01/01/1900' Or 
X.FechaOperacion between @FecIngresoRango1 and @FecIngresoRango2)
order by NumeroOperacion desc,NombreCliente,DebitMemo, FechaEmision      
