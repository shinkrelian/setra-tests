﻿CREATE Procedure [dbo].[MASERVICIOS_Sel_Rpt]   --'000066','',0,'','','',0
	@año char(4), 
	@IDCiudad varchar(6),
	@IDTipoProv varchar(3),
	@Activo bit,
	@Descripcion varchar(100),
	@Proveedor varchar(100),
	@IDCat varchar(3),
	@Categoria tinyint
as
	Set NoCount On
--	declare @Descripcion varchar(100)
	SELECT Distinct * FROM
	(
	Select s.Descripcion,-- p.RazonSocial, 
	--Case When p.IDTipoProv='001' Then p.NombreCorto Else p.RazonSocial End as RazonSocial,
	p.NombreCorto  as RazonSocial,
	s.IDServicio
	--IsNull(p.IDTipoOper,'') as IDTipoOper
	--Case ISNULL(IDTipoOper,'') When 'I' then 'INTERNO' When 'E' then 'EXTERNO'
	-- when '' then 'NO APLICA' End as IDTipoOper 
	From MASERVICIOS s 
	Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
	Left Join MASERVICIOS_DET sd On s.IDServicio=sd.IDServicio 
	Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo
	--Left Join MAUBIGEO up On up.IDPais select*from maservicios
	Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ
	Where 
	(LTRIM(rtrim(@Descripcion))='' Or s.Descripcion Like '%'+@Descripcion+'%')
	And (LTRIM(RTRIM(@IDTipoProv))=''Or s.IDTipoProv=@IDTipoProv)
	And (LTRIM(rtrim(@Proveedor))='' Or p.NombreCorto Like '%'+@Proveedor+'%')
	And (@Activo=0 Or s.Activo=1)
	And  s.cActivo='A'			
	--And (LTRIM(RTRIM(@IDPais))='' Or u.IDPais=@IDPais)
	And (sd.Anio=@año Or LTRIM(RTRIM(@año))='')
	
	And (p.IDCiudad=@IDCiudad Or LTRIM(rtrim(@IDCiudad))='')
	And (Categoria=@Categoria Or @Categoria=0)
	And (s.IDCat=@IDCat Or LTRIM(rtrim(@IDCat))='')
	
	) AS X		
	Order by Descripcion
