﻿CREATE Procedure dbo.RESERVAS_DET_SelServiciosxAcomodoEspecialGroup   
@IDCab int  
As    
Set NoCount On 
Select *,dbo.FnAcomodosReservasxIDReserva(X.IDReserva,X.Dia) as Acomodo from (
select rd.IDReserva_Det,rd.IDDet, CAST(rd.Cantidad as varchar(5))+' '+ rd.Servicio As TituloAcomodoxCapacidad  , convert(Char(10),rd.Dia,103) as FechaIn,convert(Char(10),rd.FechaOut,103) as FechaOut,     
    rd.Cantidad,   rd.Dia, R.IDReserva,
    case when (select COUNT(iddet) from cotidet cd    
    where AcomodoEspecial = 1 and Dia between rd.Dia and dateadd(d,-1,rd.FechaOut) and cd.IncGuia = 0 and IDCab = r.IDCab and r.IDProveedor = cd.IDProveedor    
   and cd.Dia between rd.Dia and rd.FechaOut)>0     
   then 1 Else 0 End as ExisteAcomodoEspecial,rd.CapacidadHab,rd.EsMatrimonial,    
   CAST(rd.Cantidad as varchar(5))+' ' + dbo.FnMailAcomodosReservas(rd.CapacidadHab,rd.EsMatrimonial) as TituloAcomodo,    
  rd.Servicio  ,r.IDProveedor,  
   dbo.FnDescripcionAcomodosReservas(rd.CapacidadHab,rd.EsMatrimonial) as Acomodo  
from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva    
 Left Join MASERVICIOS_DET sd On sd.IDServicio_Det = rd.IDServicio_Det     
 Left Join COTIDET cd On rd.IDDet = cd.IDDET     
where r.IDCab = @IDCab and r.Anulado = 0  and rd.Cantidad <> 0    
  and sd.PlanAlimenticio = 0 and rd.Anulado = 0 and cd.IncGuia = 0 --and rd.FechaOut is not null    
  ) as X
where X.ExisteAcomodoEspecial = 1
Order by X.Dia    
