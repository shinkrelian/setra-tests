﻿--JHD-JRF-Case When @IDReserva = 0 Then Null Else 0 End
CREATE Procedure dbo.OPERACIONES_Ins   
 @IDReserva int,  
 @IDCab int,  
 @IDFile char(8),  
 @IDProveedor char(6),  
 @Observaciones varchar(250),  
 @UserMod char(4),  
 @pIDOperacion int output  
As  
 Set Nocount On  
   
 Declare @IDOperacion int  
 Execute dbo.Correlativo_SelOutput 'OPERACIONES',1,10,@IDOperacion output  
   
 Insert Into OPERACIONES  
 (IDOperacion,IDReserva,IDCab,IDFile,IDProveedor,Observaciones,UserMod)  
 Values  
 (@IDOperacion, Case When @IDReserva = 0 Then Null Else 0 End,@IDCab,@IDFile,@IDProveedor,  
 Case When LTRIM(rtrim(@Observaciones))='' Then Null Else @Observaciones End,  
 @UserMod)  
   
 Set @pIDOperacion=@IDOperacion  
