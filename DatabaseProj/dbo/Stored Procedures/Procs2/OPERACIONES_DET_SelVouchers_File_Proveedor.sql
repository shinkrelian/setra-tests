﻿--JHD-20141015-Se agrego filtro de voucher y documento                        
--JHD-20141028-Se corrigio filtro de file para poder buscar por like                      
--JRF-20141028-Unir los registros guardados en ORDENES DE SERVICIOS                      
--HLF-20141030-@NuVoucher varchar(14)                      
--JRF-20141103-Agregar campo de CoEstado,Saldo Pendiente,Dif. Aceptada y filtrado por CoEstado                      
--JRF-20141104-Agregar campo Total                      
--HLF-20141105-Cambiar subquery para MontoTotal                
--JRF-20141106-SubQuery ajustado (MontoTotal)                
--HLF-20141106-Agregar IDProveedor              
--HLF-20141110-isnull(od.NuVoucherBup,od.IDVoucher) as IDVoucher,              
--isnull(od.NuVoucherBup,od.IDVoucher) in               
--od.IDVoucher as NuVoucherSel,              
--JHD-20141209-Unir los registros guardados en ORDENES DE PAGO                  
--JHD-20141209-Filtro por tipo de forma de egreso              
--JHD-20141210-Agregar CoEstado              
--HLF-20141210-1er Union Voucher_Historico:            
--comentando--(Ltrim(Rtrim(@CoEstado))='' Or vo.CoEstado=@CoEstado) and                        
--'AN' as CoEstado, 'ANULADO' as Estado            
--(ltrim(rtrim(@NuVoucher))='' or vh.IDVoucher Like @NuVoucher +'%') and               
--Quitando Voucher_Historico del 2do Union            
--JHD-20141212-Corrigiendo texto 'ANULxADO' por 'ANULADO'            
--JHD-20150108-Agregar columna 'orden_estado' para mostrar primero las formas de pago obsevadas:            
--  orden_estado=case when CoEstado='OB' THEN 0 ELSE 1 END            
--JHD-20150108-Se corrigio el filtro por estado: AND (CoEstado=@CoEstado OR @CoEstado='')            
--JHD-20150108-Se agrego la columna 'orden_estado' en el order by: order by Orden_Estado, ...            
--JHD-20150108- Se modifico la consulta, haciendo un union para los registros con estado observado y los estados restantes.            
--HLF-20150126- OR p.IDProveedor='001836'--PeruRail            
--IDOperacion,          
--JHD-20150202-@CoUbigeo_Oficina char(6), And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
--HLF-20150202- Case When p.IDProveedor='001836' Then 'PRL'        
--Case When TipoVouOrd='PRL' Then 'PR-        
--Case When p.IDProveedor<>'001836' Then vo.SsMontoTotal        
--HLF-20150203-Agregando Unions Orden de Compra      
--HLF-20150209-Agregando Union VOUCHER_OFICINA_EXTERNA      
--JHD-20150210-Se quito select duplicado        
--HLF-20150211-Agregando Union PRESUPUESTOS    
--HLF-20150212-Comentando --And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
-- en Union VOUCHER_OFICINA_EXTERNA      
--HLF-20150213-Case When @CoUbigeo_Oficina='000065' and o.IDProveedor='001554' then 0 else 1 end as OkLima  
--JHD-20150217-Se agrego el campo CoMoneda para todas las subconsultas y para el select general    
--HLF-20150217-Case When vtr.SsTotal Is null Then...
--Left Join VOUCHER_PROV_TREN vtr On vtr.NuVouPTrenInt=o.NuVouPTrenInt
--JHD-20150311- MOSTRAR ORDENES DE COMPRA CON ESTADO INTERNO "ACEPTADO": and oc.CoEstado_OC='AC'
--JHD-20150505- IDVoucher=ISNULL(IDVoucher,'')
--JHD-20150513- Se agregó nuevo parámetro @NumIdentidad varchar(15)=''
--JHD-20150515- (@IDFile='' or c.IDFile like '%'+@IDFile+'%')  
--JHD-20150515-Agregando Union PRESUPUESTOS TC
--JHD-20150518-IDPax=cast( oc.IDPax as char(6)),
--JHD-20150518-and (ltrim(rtrim(@NuVoucher))='' or SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) Like @NuVoucher +'%')
--JHD-20150520- IsNull(vo.SsSaldo,0) - (select top 1 isnull(op2.SsMontoTotal,0) from ordenpago op2
--JHD-20150604- Agregando Union FONDOS FIJOS
--JHD-20150605- oc.NuCodigo,
--JHD-20150610- Mostrar nombre de Fondo Fijo en vez del usuario,
--JHD-20150616- Agregar columnas   MontoTotal_USD, SaldoPendiente_USD, DifAceptada_USD, para usarse en Presupuesto
--JHD-20150617- oc.CoEstado='EN' AND
--JHD-20150709- Se quito ubigeo para fondos fijos
--JHD-20150715- case when od.FlServInManualOper=1 then (select IDMoneda from OPERACIONES_DET where IDOperacion_Det=od.IDOperacion_Det)   else  (select IDMoneda from RESERVAS_DET where IDReserva_Det=od.IDReserva_Det)  end
--JHD-20150820- Agregando Union OTROS
--JHD-20150831- case when isnull(p.IDProveedorInternacional,'')<>'' then p.IDProveedorInternacional else o.IDProveedor end as IDProveedor,
--JHD-20150831- isnull(case when isnull(p.IDProveedorInternacional,'')<>'' then (SELECT nombrecorto from MAPROVEEDORES pro where pro.idproveedor= p.IDProveedorInternacional) else P.NombreCorto end,'') as DescProveedor,
--JHD-20150831- Se comento el calculo de la resta del monto de ordenes de pago al saldo pendiente
--JHD-20150904- Se agrego para los vouchers la suma de las ordenes de pago
--JHD-20150921- (oc.SsTotal+isnull(oc.SSDetraccion,0)) as MontoTotal,   
--JHD-20150925- Se cambio la consulta para vouchers, agregando la tabla VOUCHER_OPERACIONES_DET para separar los montos por moneda
--JHD-20150928-  Case When p.IDProveedor<>'001836' Then        
--JHD-20150928-    Case When p.IDProveedor<>'001836' Then        
				 -- ( select noestado from ESTADO_OBLIGACIONESPAGO  where coestado= (SELECT isnull(CoEstado,'') FROM VOUCHER_OPERACIONES_DET WHERE IDVoucher=VO.IDVoucher and comoneda=(case when od.FlServInManualOper=1 then (select top 1 IDMoneda from OPERACIONES_DET 



--JHD-20150930- and p.IDProveedor in (select CoProveedor from DOCUMENTO_PROVEEDOR where (CoObligacionPago IS NULL OR CoObligacionPago in ('','OTR')) AND FlMultiple=0)      
--JHD-20151118- Se cambió la condición para listar las ordenes de servicio, agregando la tabla ORDEN_SERVICIO_DET_MONEDA para separar los montos por moneda
--HLF-20151127- UNION --PRESUPUESTOS TKT 
--HLF-20151203-And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1) 
--HLF-20151209-And exists(Select NuPreSob From PRESUPUESTO_SOBRE_DET where NuPreSob=p.NuPreSob and CoPago='TCK')
--HLF-20160104-And (isnull(p.CoUbigeo_Oficina,us.IDUbigeo)=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
--JHD-20160107-cast(oc.NuPreSob as varchar) as NuPreSobFile,     
--JHD-20160112- (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')    
--HLF-20160113-od.idmoneda en vez de (select IDMoneda from OPERACIONES_DET where IDOperacion_Det=od.IDOperacion_Det)   
--JHD-20160201-    if @CoUbigeo_Oficina = '000066' ...
--HLF-20160202-comentando case when isnull(p.CoUbigeo_Oficina,us.IDUbigeo)='000066' then '000065' ...
--JHD-20160210-isnull(p.NombreCorto,us.Nombre+ ' ' +isnull(us.TxApellidos,'')) as DescProveedor ,      
--JHD-20160318- Poner "and p.IDProveedor not in ('002315')" en formas de egreso
--JHD-20160328- and IDProveedor not in ('002315')
--JRF-20160330- Agregar columnas {'' As IDProvSetoursOS,'' As DescProvSetoursOS}
--JHD-20160331-	and IDProveedor not in ('002315','002317')
--JHD-20160405- and (IDProveedor not in ('002315','002317') OR @CoUbigeo_Oficina not in ('000110','000113') )
--PPMG20160406-Se agrego al final la consulta para el tipo ZICASSO
--JRF-20160416- Optimizar Query Voucher-Peru Rail
CREATE PROCEDURE [dbo].[OPERACIONES_DET_SelVouchers_File_Proveedor]                                          
@IDFile varchar(8),
@Proveedor varchar(50),
@NuVoucher varchar(14),
@NuDocum varchar(10)='',
@CoEstado char(2),              
@TipoVouOrd varchar(3),
@SoloObservados bit,          
@CoUbigeo_Oficina char(6),
@NumIdentidad varchar(15),
@FlHistorico bit
AS	
BEGIN
       -- si esta en cusco pasarlo a lima, porque la pantalla de cusco no esta implementada aun
	 --  if @CoUbigeo_Oficina = '000066'
		--set @CoUbigeo_Oficina = '000065'
	DECLARE @IDProveedor_Zicasso char(6) = '003557',
			@IDServicio_Zicasso char(8)	= 'LAX00002',
			@Servicio_Det_Zicasso CHAR(4) = 'ZIC6'
	DECLARE @Tbl_ServDet Table(IDProveedorX char(6),
							   IDServicioX char(8),
							  IDServicio_DetX int);
	Set NoCount On
	INSERT INTO @Tbl_ServDet(IDProveedorX, IDServicioX, IDServicio_DetX)
	SELECT S.IDProveedor, D.IDServicio, D.IDServicio_Det FROM MASERVICIOS_DET D INNER JOIN MASERVICIOS S ON D.IDServicio = S.IDServicio
	WHERE TipoUsoSistema = @Servicio_Det_Zicasso AND S.IDProveedor = @IDProveedor_Zicasso AND D.IDServicio = @IDServicio_Zicasso
	Set NoCount Off

SELECT DISTINCT
  IDCAB,              
  IDFile,        
  IDVoucher=ISNULL(IDVoucher,''),               
  --Case When TipoVouOrd='PRL' Then 'PR-'+IDFile Else IDVoucher End as IDVoucher,        
  NuVoucherSel,        
  --Case When TipoVouOrd='PRL' Then 'PR-'+IDFile Else NuVoucherSel End as NuVoucherSel,        
IDProveedor,          
  IDOperacion,              
  DescProveedor ,                
   TipoVouOrd ,              
   CoEstado,              
   Estado,              
      CoMoneda=isnull((select Simbolo from MAMONEDAS where IDMoneda=CoMoneda),''),  
   MontoTotal,              
   SaldoPendiente,              
   DifAceptada  ,  
      MontoTotal_USD,  
SaldoPendiente_USD,                      
  DifAceptada_USD,               
   Orden_Estado,
   IDProvSetoursOS,
   DescProvSetoursOS    
FROM            
            
(                      
             
   SELECT DISTINCT        
  IDCAB,  
  IDFile,                       
IDVoucher,               
  NuVoucherSel,              
IDProveedor,       
  IDOperacion,             
  DescProveedor ,              
   TipoVouOrd ,   
   CoEstado,              
   Estado,              
   MontoTotal,              
   SaldoPendiente,              
   DifAceptada     , 
    MontoTotal_USD,                             
SaldoPendiente_USD,                      
  DifAceptada_USD,                    
   Orden_Estado=case when CoEstado='OB' THEN 0 ELSE 1 END,
   CoMoneda             ,IDProvSetoursOS,DescProvSetoursOS
   FROM (                    
                      
 SELECT DISTINCT                      
  IDCAB,IDFile,                      
                
  --Cast(IDVoucher as varchar) as IDVoucher,               
 Case When NuVoucherBup Is Null Then              
  Cast(IDVoucher as varchar)              
 Else              
  Cast(IDVoucher as varchar)+'(antes '+Cast(NuVoucherBup as varchar)+')'              
 End as IDVoucher,               
                
  Cast(NuVoucherSel as varchar) as NuVoucherSel,              
  IDProveedor,  IDOperacion,            
  DescProveedor , TipoVouOrd ,CoEstado,Estado,MontoTotal,SaldoPendiente,DifAceptada    
  
   ,MontoTotal_USD,  
SaldoPendiente_USD,                      
  DifAceptada_USD    
             
  ,CoMoneda, IDProvSetoursOS,DescProvSetoursOS               
 FROM                                          
                                           
 (                                           
 
 Select 
	Vou.IDCab,Vou.IDFile,Vou.IDVoucher,Vou.NuVoucherSel,Vou.NuVoucherBup,Vou.IDProveedor,Vou.IDOperacion,Vou.DescProveedor,Vou.TipoVouOrd,
	Vou.CoEstado,Vou.Estado,Vou.MontoTotal,Vou.MontoTotal-Vou.TotalDocumento-IsNull(Vou.TotalDocumentoOP,0) As SaldoPendiente,Vou.DifAceptada, 
	0 as MontoTotal_USD,0 as SaldoPendiente_USD,0 as DifAceptada_USD,
	Vou.OkLima,Vou.CoMoneda,'' As IDProvSetoursOS,'' As DescProvSetoursOS
from (
Select Distinct o.IDCab,o.IDFile,
				Cast(voDet.IDVoucher as varchar(6)) as IDVoucher ,
				IsNull(voDet.IDVoucher,0) As NuVoucherSel,Cast(od.NuVoucherBup as varchar(6)) As NuVoucherBup,o.IDProveedor,o.IDOperacion,
				IsNull(pInt.NombreCorto,p.NombreCorto) as DescProveedor,'VOU' As TipoVouOrd,voDet.CoEstado,oEstPag.NoEstado as Estado,
				IsNull(Sum(od.TotalGen),0) as MontoTotal, IsNull(docRelVouDet.TotalDocVO,0) TotalDocumento,voDet.CoMoneda,
				IsNull(voDet.SsDifAceptada,0) As DifAceptada,
				docRelOP.TotalDocOP As TotalDocumentoOP,Case When @CoUbigeo_Oficina='000065' and o.IDProveedor='001554' then 0 else 1 end As OkLima,
				Case When Ltrim(Rtrim(@NuDocum))='' then 1 Else 
					Case When Exists(select dProv.NuDocum from DOCUMENTO_PROVEEDOR dProv 
									 Where IsNull(dProv.NuVoucher,0) = voDet.IDVoucher --And IsNull(dProv.NuVouPTrenInt,0)=IsNull(voTren.NuVouPTrenInt,0)
									 And Charindex(Ltrim(Rtrim(@NuDocum)),dProv.NuDocum) > 0) Then 1 Else 0 End
				End As ExistNuDocumFiltrado
from VOUCHER_OPERACIONES_DET voDet Inner Join VOUCHER_OPERACIONES voCab On voDet.IDVoucher=voCab.IDVoucher And (@TipoVouOrd='VOU' Or Ltrim(Rtrim(@TipoVouOrd))='')
Inner Join OPERACIONES_DET od On voDet.IDVoucher=od.IDVoucher And voDet.CoMoneda=od.IDMoneda
Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor And o.IDProveedor<>'001836'
--Left Join VOUCHER_PROV_TREN voTren On o.NuVouPTrenInt =voTren.NuVouPTrenInt And voTren.IDCab=o.IDCab
Left Join MAPROVEEDORES pInt On p.IDProveedorInternacional=pInt.IDProveedor
Left Join ESTADO_OBLIGACIONESPAGO oEstPag On voDet.CoEstado=oEstPag.CoEstado
Left Join (Select NuVoucher,Sum(Case When IsNull(SsTotal_FEgreso,0)=0 Then SSTotal Else SsTotal_FEgreso End) As TotalDocVO from DOCUMENTO_PROVEEDOR 
		   Where FlActivo=1 And IsNull(NuVoucher,0) <> 0 Group By NuVoucher) docRelVouDet On voDet.IDVoucher = docRelVouDet.NuVoucher
--Left Join (Select NuVouPTrenInt,Sum(Case When IsNull(SsTotal_FEgreso,0)=0 Then SSTotal Else SsTotal_FEgreso End) As TotalDocPR from DOCUMENTO_PROVEEDOR 
--		   Where FlActivo=1 And IsNull(NuVoucher,0) = 0 And IsNull(NuVouPTrenInt,0) <> 0 Group By NuVouPTrenInt) docRelVouTren
--On voTren.NuVouPTrenInt = docRelVouTren.NuVouPTrenInt --And voTren.CoMoneda=docRelVouTren.CoMoneda
Left Join (Select r.IDProveedor,r.IDCab,IsNull(Sum(Case When IsNull(SsTotal_FEgreso,0)=0 Then SSTotal Else SsTotal_FEgreso End),0) As TotalDocOP
		   from DOCUMENTO_PROVEEDOR docOP Inner Join ORDENPAGO op On docOP.CoOrdPag=op.IDOrdPag
		   Inner Join RESERVAS r On op.IDReserva=r.IDReserva And r.Anulado=0
		   Where IsNull(NuVoucher,0) = 0
		   Group By r.IDProveedor,r.IDCab) docRelOP On o.IDProveedor=docRelOP.IDProveedor And o.IDCab=docRelOP.IDCab
Inner Join COTICAB c On o.IDCab=c.IDCAB
Where voDet.IDVoucher <> 0 And (@FlHistorico=1 Or c.FlHistorico=@FlHistorico) --And o.IDCab=10873
And (Ltrim(Rtrim(@CoUbigeo_Oficina))='' Or p.CoUbigeo_Oficina=@CoUbigeo_Oficina)
And (Ltrim(Rtrim(@NumIdentidad))='' Or p.NumIdentidad Like '%'+@NumIdentidad+'%')
Group By o.IDCab,o.IDFile,voDet.IDVoucher,od.NuVoucherBup,o.IDProveedor,o.IDOperacion,p.NombreCorto,pInt.NombreCorto,
		 voDet.CoEstado,oEstPag.NoEstado,voDet.CoMoneda,docRelVouDet.TotalDocVO,voDet.SsDifAceptada,
		 docRelOP.TotalDocOP
) As Vou
Where (Ltrim(Rtrim(@IDFile))='' Or Vou.IDFile=@IDFile)
And(Ltrim(Rtrim(@Proveedor))='' Or Vou.DescProveedor Like '%'+@Proveedor+'%')
And(Ltrim(Rtrim(@NuVoucher))='' Or Vou.IDVoucher Like '%'+@NuVoucher+'%')
And(Ltrim(Rtrim(@CoEstado))='' Or Vou.CoEstado=@CoEstado)
And (Ltrim(Rtrim(@TipoVouOrd))='' Or Vou.TipoVouOrd=@TipoVouOrd)
And Vou.ExistNuDocumFiltrado=1
Union All
Select
	PRL.IDCab,PRL.IDFile,PRL.IDVoucher,PRL.NuVoucherSel,PRL.NuVoucherBup,PRL.IDProveedor,PRL.IDOperacion,PRL.DescProveedor,PRL.TipoVouOrd,
	PRL.CoEstado,PRL.Estado,PRL.MontoTotal,PRL.MontoTotal-IsNull(PRL.TotalDocumento,0)-IsNull(PRL.TotalOP,0) As SaldoPendiente,PRL.DifAceptada, 
	0 as MontoTotal_USD,0 as SaldoPendiente_USD,0 as DifAceptada_USD,
	PRL.OkLima,PRL.CoMoneda,'' As IDProvSetoursOS,'' As DescProvSetoursOS
from (
Select o.IDCab,o.IDFile,'PR-'+o.IDFile as IDVoucher,IsNull(voTren.NuVouPTrenInt,0) as NuVoucherSel,
	   Cast(od.NuVoucherBup as varchar(6)) As NuVoucherBup,o.IDProveedor,o.IDOperacion,p.NombreCorto as DescProveedor,'PRL' As TipoVouOrd,
	   IsNull(oEstPag.CoEstado,'PD') As CoEstado,IsNull(oEstPag.NoEstado,'PENDIENTE') as Estado,IsNull(docRelVouTren.TotalDocPR,Sum(od.TotalGen)) as MontoTotal,
	   IsNull(docRelVouTren.TotalDocPR,0) TotalDocumento,IsNull(docRelOP.TotalDocOP,0) As TotalOP,od.IDMoneda as CoMoneda,
	   Case When @CoUbigeo_Oficina='000065' and o.IDProveedor='001554' then 0 else 1 end as OkLima,IsNull(voTren.SsDifAceptada,0) as DifAceptada
from OPERACIONES o 
Inner Join OPERACIONES_DET od On o.IDOperacion=od.IDOperacion And (@TipoVouOrd='PRL' Or Ltrim(Rtrim(@TipoVouOrd))='')
Inner Join COTICAB c On o.IDCab=c.IDCAB 
Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor And o.IDProveedor='001836'
Left Join VOUCHER_PROV_TREN voTren On o.NuVouPTrenInt =voTren.NuVouPTrenInt And voTren.IDCab=o.IDCab
Left Join (Select NuVouPTrenInt,Sum(Case When IsNull(SsTotal_FEgreso,0)=0 Then SSTotal Else SsTotal_FEgreso End) As TotalDocPR from DOCUMENTO_PROVEEDOR 
		   Where FlActivo=1 And IsNull(NuVoucher,0) = 0 And IsNull(NuVouPTrenInt,0) <> 0 Group By NuVouPTrenInt) docRelVouTren
On voTren.NuVouPTrenInt = docRelVouTren.NuVouPTrenInt 
Left Join (Select r.IDProveedor,r.IDCab,IsNull(Sum(Case When IsNull(SsTotal_FEgreso,0)=0 Then SSTotal Else SsTotal_FEgreso End),0) As TotalDocOP
		   from DOCUMENTO_PROVEEDOR docOP Inner Join ORDENPAGO op On docOP.CoOrdPag=op.IDOrdPag
		   Inner Join RESERVAS r On op.IDReserva=r.IDReserva And r.Anulado=0
		   Where IsNull(NuVoucher,0) = 0
		   Group By r.IDProveedor,r.IDCab) docRelOP On o.IDProveedor=docRelOP.IDProveedor And o.IDCab=docRelOP.IDCab
Left Join ESTADO_OBLIGACIONESPAGO oEstPag On voTren.CoEstado=oEstPag.CoEstado
Where (Ltrim(Rtrim(@IDFile))='' Or o.IDFile=@IDFile)
And (@FlHistorico=1 Or c.FlHistorico=@FlHistorico)
Group By o.IDCab,o.IDFile,voTren.NuVouPTrenInt,od.NuVoucherBup,o.IDProveedor,o.IDOperacion,p.NombreCorto,oEstPag.CoEstado,oEstPag.NoEstado,
		 docRelVouTren.TotalDocPR,docRelVouTren.TotalDocPR,docRelOP.TotalDocOP,od.IDMoneda,voTren.SsDifAceptada
) as PRL
Where (Ltrim(Rtrim(@CoEstado))='' Or PRL.CoEstado=@CoEstado)


 ) as X2  
 Where X2.OkLima=1  

--ordenes de servicio
 Union   

 select distinct c.IDCAB,              
 c.IDFile,                    
 SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,              
 SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as NuVoucherSel,               
 os.IDProveedor, 0 as IDOperacion,             
 p.NombreCorto as DescProveedor ,'OSV' as TipoVouOrd,               
 osm.CoEstado,                     
 eop.NoEstado as Estado,                  
              
 Isnull(osm.SsMontoTotal,0) as MontoTotal,                
                   
 IsNull(osm.SsSaldo,0) as SaldoPendiente,                      
 IsNull(osm.SsDifAceptada,0) as DifAceptada

   ,0 as MontoTotal_USD,                             
0 as SaldoPendiente_USD,                      
  0 as DifAceptada_USD  

 --,CoMoneda=isnull((SELECT   top 1  RESERVAS_DET.IDMoneda 
 --   FROM         ORDEN_SERVICIO INNER JOIN  
 --          ORDEN_SERVICIO_DET ON ORDEN_SERVICIO.NuOrden_Servicio = ORDEN_SERVICIO_DET.NuOrden_Servicio INNER JOIN  
 --          OPERACIONES_DET ON ORDEN_SERVICIO_DET.IDOperacion_Det = OPERACIONES_DET.IDOperacion_Det INNER JOIN  
 --          RESERVAS_DET ON OPERACIONES_DET.IDReserva_Det = RESERVAS_DET.IDReserva_Det  
 --   WHERE     (ORDEN_SERVICIO.NuOrden_Servicio = os.NuOrden_Servicio)),'')  

 ,CoMoneda=osm.CoMoneda
 ,Rtrim(Ltrim(IsNull(pSet.IDProveedor,''))) As IDProvSetoursOS,Rtrim(Ltrim(IsNull(pSet.NombreCorto,''))) As DescProvSetoursOS
 
 from ORDEN_SERVICIO os
 left join ORDEN_SERVICIO_DET_MONEDA osm on os.NuOrden_Servicio=osm.NuOrden_Servicio
 Left Join MAPROVEEDORES p On os.IDProveedor=p.IDProveedor                           
 Left Join COTICAB c On os.IDCab=c.IDCAB                      
 Left Join ESTADO_OBLIGACIONESPAGO eop On osm.CoEstado=eop.CoEstado      
 Left Join MAPROVEEDORES pSet On os.IDProveedorSetours=pSet.IDProveedor                
 where             
     
  (@IDFile='' or c.IDFile like '%'+@IDFile+'%')                       
  and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                         
   and  (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%')                       
                       
  and (ltrim(rtrim(@NuVoucher))='' or SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) Like @NuVoucher +'%')                         
                                 
  and (ltrim(rtrim(@NuDocum))='' or os.NuOrden_Servicio in                       
  (select distinct NuOrden_Servicio from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                                 
            
    And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
	And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)  
	--and p.IDProveedor not in ('002315')                          
   /*                 
 select distinct c.IDCAB,              
 c.IDFile,                    
 SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,              
 SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as NuVoucherSel,               
 os.IDProveedor, 0 as IDOperacion,             
 p.NombreCorto as DescProveedor ,'OSV' as TipoVouOrd,               
 os.CoEstado,                     
 eop.NoEstado as Estado,                  
              
 Isnull(os.SsMontoTotal,0) as MontoTotal,                
                   
 IsNull(os.SsSaldo,0) as SaldoPendiente,                      
 IsNull(os.SsDifAceptada,0) as DifAceptada

   ,0 as MontoTotal_USD,                             
0 as SaldoPendiente_USD,                 
  0 as DifAceptada_USD  

 ,CoMoneda=isnull((SELECT   top 1  RESERVAS_DET.IDMoneda 
    FROM         ORDEN_SERVICIO INNER JOIN  
           ORDEN_SERVICIO_DET ON ORDEN_SERVICIO.NuOrden_Servicio = ORDEN_SERVICIO_DET.NuOrden_Servicio INNER JOIN  
           OPERACIONES_DET ON ORDEN_SERVICIO_DET.IDOperacion_Det = OPERACIONES_DET.IDOperacion_Det INNER JOIN  
           RESERVAS_DET ON OPERACIONES_DET.IDReserva_Det = RESERVAS_DET.IDReserva_Det  
    WHERE     (ORDEN_SERVICIO.NuOrden_Servicio = os.NuOrden_Servicio)),'')  
 
 from ORDEN_SERVICIO os Left Join MAPROVEEDORES p On os.IDProveedor=p.IDProveedor                           
 Left Join COTICAB c On os.IDCab=c.IDCAB                      
 Left Join ESTADO_OBLIGACIONESPAGO eop On os.CoEstado=eop.CoEstado                      
 where             
       
  --os.CoEstado<>'OB'             
  --and       
  (@IDFile='' or c.IDFile like '%'+@IDFile+'%')                       
  and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                         
   and  (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%')                       
  --and (@NuVoucher=0 or cast(od.IDVoucher as varchar(10)) Like cast(@NuVoucher as varchar(10))+'%')    
  --and (ltrim(rtrim(@NuVoucher))='' or os.CoOrden_Servicio Like @NuVoucher +'%')                         
  and (ltrim(rtrim(@NuVoucher))='' or SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) Like @NuVoucher +'%')                         
  --SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)
                        
  --and (@NuDocum='' or od.IDVoucher in (select distinct NuVoucher from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                                    
  and (ltrim(rtrim(@NuDocum))='' or os.NuOrden_Servicio in                       
  (select distinct NuOrden_Servicio from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                                 
            
    And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
 --order by  IDVoucher,DescProveedor                 
 
 */              
               
 --AGREGAR LISTA DE ORDENES DE PAGO       
              
UNION              
              
select distinct c.IDCAB,          
 c.IDFile,         
 IDVoucher='OP-'+(CAST(op.IDOrdPag as varchar(5))),              
 NuVoucherSel='OP-'+(CAST(op.IDOrdPag as varchar(5))),              
 --SUBSTRING(op.CoOrden_Servicio,1,8)+'-'+SUBSTRING(op.CoOrden_Servicio,9,Len(op.CoOrden_Servicio)) as IDVoucher,              
 --SUBSTRING(op.CoOrden_Servicio,1,8)+'-'+SUBSTRING(op.CoOrden_Servicio,9,Len(op.CoOrden_Servicio)) as NuVoucherSel,               
 re.IDProveedor,  0 as IDOperacion,            
 p.NombreCorto as DescProveedor ,'OPA' as TipoVouOrd,                   
 op.CoEstado,               
 eop.NoEstado as Estado,                  
           
 Isnull(op.SsMontoTotal,0) as MontoTotal,                
                   
 IsNull(op.SsSaldo,0) as SaldoPendiente,                      
 IsNull(op.SsDifAceptada,0) as DifAceptada,

   0 as MontoTotal_USD,                       
0 as SaldoPendiente_USD, 
  0 as DifAceptada_USD,  

 CoMoneda=op.IDMoneda ,
 '' As IDProvSetoursOS,'' As DescProvSetoursOS
 from ORDENPAGO op              
 LEFT JOIN RESERVAS re on re.IDReserva=op.IDReserva              
  Left Join MAPROVEEDORES p On re.IDProveedor=p.IDProveedor                           
 Left Join COTICAB c On op.IDCab=c.IDCAB                      
 Left Join ESTADO_OBLIGACIONESPAGO eop On op.CoEstado=eop.CoEstado                   
                  
 where --(Ltrim(Rtrim(@CoEstado))='' Or op.CoEstado=@CoEstado)                       
-- op.CoEstado<>'OB'            
  --and       
  (@IDFile='' or c.IDFile like '%'+@IDFile+'%')          
  and (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                         
  and  (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%')  
  --and (@NuVoucher=0 or cast(od.IDVoucher as varchar(10)) Like cast(@NuVoucher as varchar(10))+'%')                         
 -- and (ltrim(rtrim(@NuVoucher))='' or 'OP-'+(CAST(op.IDOrdPag as varchar(5))) Like @NuVoucher +'%')                         
  and (ltrim(rtrim(@NuVoucher))='' or (CAST(op.IDOrdPag as varchar(5))) Like @NuVoucher +'%')                         
                    
  --and (@NuDocum='' or od.IDVoucher in (select distinct NuVoucher from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                                    
  and (ltrim(rtrim(@NuDocum))='' or op.IDOrdPag in                       
  (select distinct NuOrden_Servicio from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                 
         And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
      And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)  
--and p.IDProveedor not in ('002315')                          
--Orden de Compra      
Union      
      
 Select 0 as IDCab, '' as IDFile, oc.NuOrdCom, cast(oc.NuOrdComInt as varchar), oc.CoProveedor,      
 0 as IDOperacion, p.NombreCorto as DescProveedor ,'OCP' as TipoVouOrd,             
 oc.CoEstado, eop.NoEstado as Estado,      
 --oc.SsTotal as MontoTotal,                             
 --oc.SsTotal as MontoTotal,                             
 (oc.SsTotal+isnull(oc.SSDetraccion,0)) as MontoTotal,                             
    oc.SsSaldo as SaldoPendiente,                      
    oc.SsDifAceptada as DifAceptada,

	  0 as MontoTotal_USD,                             
0 as SaldoPendiente_USD,                      
  0 as DifAceptada_USD,  

    oc.CoMoneda,'' As IDProvSetoursOS,'' As DescProvSetoursOS
 From ORDENCOMPRA oc Left Join MAPROVEEDORES p On oc.CoProveedor=p.IDProveedor      
 Left Join ESTADO_OBLIGACIONESPAGO eop On oc.CoEstado=eop.CoEstado           
 Where      
 --oc.CoEstado<>'OB'            
 --and      
  (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%') 
  and (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%')                                            
 and (ltrim(rtrim(@NuVoucher))='' or oc.NuOrdCom Like @NuVoucher +'%')      
 and (ltrim(rtrim(@NuDocum))='' or oc.NuOrdComInt in  
 (select NuOrdComInt from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                 
   And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
  --
  and oc.CoEstado_OC='AC'    
   --and p.IDProveedor not in ('002315')                        
Union      
--VOUCHER_OFICINA_EXTERNA      
      
 Select oc.IDCab, cc.IDFile as IDFile, oc.NuVouOfiExt, cast(oc.NuVouOfiExtInt as varchar), oc.CoProveedor,      
 0 as IDOperacion, p.NombreCorto as DescProveedor ,      
 Case p.CoUbigeo_Oficina When '000113' Then 'VBA'      
 When '000110' Then 'VSA'      
 End as TipoVouOrd,     
 oc.CoEstado, eop.NoEstado as Estado,      
 oc.SsTotal as MontoTotal,                             
    oc.SsSaldo as SaldoPendiente,                      
    oc.SsDifAceptada as DifAceptada,

	  0 as MontoTotal_USD,                             
0 as SaldoPendiente_USD,                      
  0 as DifAceptada_USD,  

    oc.CoMoneda    ,'' As IDProvSetoursOS,'' As DescProvSetoursOS
 From VOUCHER_OFICINA_EXTERNA oc Left Join MAPROVEEDORES p On oc.CoProveedor=p.IDProveedor      
 Left Join ESTADO_OBLIGACIONESPAGO eop On oc.CoEstado=eop.CoEstado           
 Left Join COTICAB cc On oc.IDCab=cc.IDCAB      
 Where       
 (@IDFile='' or cc.IDFile like '%'+@IDFile+'%') AND
 (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')   
 and  (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%')                                         
 and (ltrim(rtrim(@NuVoucher))='' or oc.NuVouOfiExt Like @NuVoucher +'%')      
 and (ltrim(rtrim(@NuDocum))='' or oc.NuVouOfiExtInt in      
(select NuVouOfiExtInt from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                 
   --And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
    
    And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1)    
	--and p.IDProveedor not in ('002315')                        
Union    
--PRESUPUESTOS    
    
  Select oc.IDCab, cc.IDFile as IDFile, 
  --Left(oc.NuPreSobFile,4)+'-'+SUBSTRING(oc.NuPreSobFile,5,8)+'-'+RIGHT(oc.NuPreSobFile,2) as NuPreSobFile,    
  cast(oc.NuPreSob as varchar) as NuPreSobFile,     
  cast(oc.NuPreSob as varchar) as NuPreSob,     
  oc.CoPrvGui,      
  0 as IDOperacion, 
  --p.NombreCorto as DescProveedor ,      
   isnull(p.NombreCorto,us.Nombre+ ' ' +isnull(us.TxApellidos,'')) as DescProveedor ,      
  'PST' as TipoVouOrd,      
  oc.CoEstadoFormaEgreso, eop.NoEstado as Estado,      
--
  isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=oc.NuPreSob and IDMoneda='SOL'),0) as MontoTotal,                             
isnull(oc.SsSaldo,0) as SaldoPendiente,                      
  isnull(oc.SsDifAceptada,0) as DifAceptada

  , isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=oc.NuPreSob and IDMoneda='USD'),0) as MontoTotal_USD,                             
 isnull(oc.SsSaldo_USD,0) as SaldoPendiente_USD,                      
  isnull(oc.SsDifAceptada_USD,0) as DifAceptada_USD

--
   ,CoMoneda=isnull((SELECT   top 1  RESERVAS_DET.IDMoneda  
    FROM         PRESUPUESTO_SOBRE_DET  INNER JOIN  
           OPERACIONES_DET ON PRESUPUESTO_SOBRE_DET.IDOperacion_Det = OPERACIONES_DET.IDOperacion_Det INNER JOIN  
           RESERVAS_DET ON OPERACIONES_DET.IDReserva_Det = RESERVAS_DET.IDReserva_Det  
    WHERE     (PRESUPUESTO_SOBRE_DET.NuPreSob = oc.NuPreSob)),'')  
    ,'' As IDProvSetoursOS,'' As DescProvSetoursOS
  
  From PRESUPUESTO_SOBRE oc Left Join MAPROVEEDORES p On oc.CoPrvGui=p.IDProveedor      
	Left Join MAUSUARIOS us on ltrim(rtrim(oc.CoPrvGui))=us.IDUsuario
  Left Join ESTADO_OBLIGACIONESPAGO eop On oc.CoEstadoFormaEgreso=eop.CoEstado           
  Left Join COTICAB cc On oc.IDCab=cc.IDCAB      
  Where
  (oc.CoEstado='EN' AND
  (@IDFile='' or cc.IDFile like '%'+@IDFile+'%') and       
  (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                                           
   and (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%') 
  and (ltrim(rtrim(@NuVoucher))='' or oc.NuPreSobFile Like @NuVoucher +'%')      
  and (ltrim(rtrim(@NuDocum))='' or oc.NuPreSob in      
  (select NuPreSob from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                 
    --And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          
	--And (isnull(p.CoUbigeo_Oficina,us.IDUbigeo)=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))=''
	
	--And ( (case when isnull(p.CoUbigeo_Oficina,us.IDUbigeo)='000066' then '000065' 
	--	else isnull(p.CoUbigeo_Oficina,us.IDUbigeo) end) =@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))=''
		And  (isnull(p.CoUbigeo_Oficina,us.IDUbigeo)  =@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))=''
	)
--	and (p.CoUbigeo_Oficina='000066' or @CoUbigeo_Oficina='000065')
  ) 
	And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1)
	--and p.IDProveedor not in ('002315')                            
  Union    
--PRESUPUESTOS TC 
    
  Select oc.IDCab, cc.IDFile as IDFile,
  --Left(oc.NuPreSobFile,4)+'-'+SUBSTRING(oc.NuPreSobFile,5,8)+'-'+RIGHT(oc.NuPreSobFile,2) as NuPreSobFile,    
  cast(oc.NuPreSob as varchar) as NuPreSobFile,    
  cast(oc.NuPreSob as varchar) as NuPreSob,     
  --oc.CoPrvGui,      
  --oc.IDPax,
  IDPax=cast( oc.IDPax as char(6)),
  0 as IDOperacion, isnull(cp.Titulo,'')+' '+isnull(cp.Apellidos,'')+', '+isnull(cp.Nombres,'') as DescProveedor ,      
  'PST' as TipoVouOrd,      
  oc.CoEstadoFormaEgreso, eop.NoEstado as Estado,      
 
 --
  isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=oc.NuPreSob and IDMoneda='SOL'),0) as MontoTotal,                             
  isnull(oc.SsSaldo,0) as SaldoPendiente,                      
  isnull(oc.SsDifAceptada,0) as DifAceptada

  , isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=oc.NuPreSob and IDMoneda='USD'),0) as MontoTotal_USD,                             
 isnull(oc.SsSaldo_USD,0) as SaldoPendiente_USD,                      
  isnull(oc.SsDifAceptada_USD,0) as DifAceptada_USD
  --
   ,CoMoneda=isnull((SELECT   top 1  RESERVAS_DET.IDMoneda  
    FROM         PRESUPUESTO_SOBRE_DET  INNER JOIN  
           OPERACIONES_DET ON PRESUPUESTO_SOBRE_DET.IDOperacion_Det = OPERACIONES_DET.IDOperacion_Det INNER JOIN  
           RESERVAS_DET ON OPERACIONES_DET.IDReserva_Det = RESERVAS_DET.IDReserva_Det  
    WHERE     (PRESUPUESTO_SOBRE_DET.NuPreSob = oc.NuPreSob)),'')  
    
  ,'' As IDProvSetoursOS,'' As DescProvSetoursOS
  From PRESUPUESTO_SOBRE oc inner Join
   --MAPROVEEDORES p On oc.CoPrvGui=p.IDProveedor      
   COTIPAX cp on cp.idpax=oc.idpax
  Left Join ESTADO_OBLIGACIONESPAGO eop On oc.CoEstadoFormaEgreso=eop.CoEstado           
  Left Join COTICAB cc On oc.IDCab=cc.IDCAB      
  /* Left Join
   MAPROVEEDORES p On oc.idproveedor=p.IDProveedor      */

  Where
  oc.CoEstado='EN' AND
  (@IDFile='' or cc.IDFile like '%'+@IDFile+'%') and       
  (@Proveedor='' or isnull(cp.Titulo,'')+' '+isnull(cp.Apellidos,'')+', '+isnull(cp.Nombres,'') like '%'+@Proveedor+'%') 
   and (@NumIdentidad='' or cp.NumIdentidad like '%'+@NumIdentidad+'%') 
  and (ltrim(rtrim(@NuVoucher))='' or oc.NuPreSobFile Like @NuVoucher +'%')      
  and (ltrim(rtrim(@NuDocum))='' or oc.NuPreSob in      
  (select NuPreSob from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))  
    And ((select isnull(CoUbigeo_Oficina,'') from MAPROVEEDORES where IDProveedor=oc.IDProveedor)=@CoUbigeo_Oficina 
		Or LTRIM(rtrim(@CoUbigeo_Oficina))='')  
	And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1)     
                 
 UNION 

 --PRESUPUESTOS TKT   
    
  Select oc.IDCab, cc.IDFile as IDFile,
   --Left(oc.NuPreSobFile,4)+'-'+SUBSTRING(oc.NuPreSobFile,5,8)+'-'+RIGHT(oc.NuPreSobFile,2) as NuPreSobFile,    
   cast(oc.NuPreSob as varchar) as NuPreSobFile,
  cast(oc.NuPreSob as varchar) as NuPreSob,     
  oc.CoPrvGui,      
  0 as IDOperacion, p.NombreCorto as DescProveedor ,      
  'PTT' as TipoVouOrd,      
  oc.CoEstadoFormaEgreso, eop.NoEstado as Estado,      
--
  isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=oc.NuPreSob and IDMoneda='SOL'),0) as MontoTotal,                             
isnull(oc.SsSaldo,0) as SaldoPendiente,                      
  isnull(oc.SsDifAceptada,0) as DifAceptada

  , isnull((Select Sum(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=oc.NuPreSob and IDMoneda='USD'),0) as MontoTotal_USD,                             
 isnull(oc.SsSaldo_USD,0) as SaldoPendiente_USD,                      
  isnull(oc.SsDifAceptada_USD,0) as DifAceptada_USD

--
   ,CoMoneda='SOL'  
    
  ,'' As IDProvSetoursOS,'' As DescProvSetoursOS
  From PRESUPUESTO_SOBRE oc Left Join MAPROVEEDORES p On oc.CoPrvGui=p.IDProveedor And @TipoVouOrd='PTT'
  Left Join MAUSUARIOS us on ltrim(rtrim(oc.CoPrvGui))=us.IDUsuario
  Left Join ESTADO_OBLIGACIONESPAGO eop On oc.CoEstadoFormaEgreso=eop.CoEstado           
  Left Join COTICAB cc On oc.IDCab=cc.IDCAB      
  Where
  (oc.CoEstado='EN' AND
  (@IDFile='' or cc.IDFile like '%'+@IDFile+'%') and       
  (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%')                      
   and (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%') 
  and (ltrim(rtrim(@NuVoucher))='' or oc.NuPreSobFile Like @NuVoucher +'%')      
  and (ltrim(rtrim(@NuDocum))='' or oc.NuPreSob in      
  (select NuPreSob from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                 
    And (isnull(p.CoUbigeo_Oficina,us.IDUbigeo)=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='' or oc.CoPrvGui is null)          
  )
   --And @TipoVouOrd='PTT'
   And exists(Select NuPreSob From PRESUPUESTO_SOBRE_DET where NuPreSob=oc.NuPreSob and CoPago='TCK')
  And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1)       
  --and p.IDProveedor not in ('002315')                     
  Union  
 --FONDOS FIJOS
 Select 0 as IDCab, 
'' as IDFile,
 oc.NuCodigo,
  cast(oc.NuFondoFijo as varchar),
   oc.CoUserResponsable,      
 0 as IDOperacion, 
 --p.Nombre as DescProveedor ,
 oc.Descripcion as DescProveedor ,
 'FFI' as TipoVouOrd,             
 oc.CoEstado, 
 eop.NoEstado as Estado,      
 oc.SSMonto as MontoTotal,                             
    oc.SsSaldo as SaldoPendiente,                      
    oc.SsDifAceptada as DifAceptada,

	  0 as MontoTotal_USD,                             
0 as SaldoPendiente_USD,                      
  0 as DifAceptada_USD,  

    oc.CoMoneda,'' As IDProvSetoursOS,'' As DescProvSetoursOS
 From MAFONDOFIJO oc Left Join
 MAUSUARIOS p On oc.CoUserResponsable=p.IDUsuario      
 Left Join ESTADO_OBLIGACIONESPAGO eop On oc.CoEstado=eop.CoEstado           
 Where      
 --oc.CoEstado<>'OB'            
 --and      
  (@Proveedor='' or p.Nombre like '%'+@Proveedor+'%') 
  --and (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%')                                            
 and (ltrim(rtrim(@NuVoucher))='' or oc.CtaContable Like @NuVoucher +'%')      
 and (ltrim(rtrim(@NuDocum))='' or oc.NuFondoFijo in      
 (select NuOrdComInt from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))                 
   /*And (p.IDUbigeo=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='')          */
-- --and p.IDProveedor not in ('002315')                     
 UNION 
  
   --OTROS
 Select 0 as IDCab, 
'' as IDFile,
 '' as NuCodigo,--oc.NuCodigo,
  '' as NuFondoFijo,-- cast(oc.NuFondoFijo as varchar),
p.IDProveedor,--   oc.CoUserResponsable,      
 0 as IDOperacion, 
 --p.Nombre as DescProveedor ,
 p.NombreCorto as DescProveedor ,
 'OTR' as TipoVouOrd,             
 'PD' AS CoEstado,--oc.CoEstado, 
 '' as Estado,-- eop.NoEstado as Estado,      
 0 as MontoTotal,                             
   0 as SaldoPendiente,                      
    0 as DifAceptada,

	  0 as MontoTotal_USD,                             
0 as SaldoPendiente_USD,                      
  0 as DifAceptada_USD,  

   '' as CoMoneda,'' As IDProvSetoursOS,'' As DescProvSetoursOS
 From
 MAPROVEEDORES p
     
 Where      
 --oc.CoEstado<>'OB'            
 --and      
    (@Proveedor='' or p.NombreCorto like '%'+@Proveedor+'%') 
  and (@NumIdentidad='' or p.NumIdentidad like '%'+@NumIdentidad+'%')         
  and p.IDProveedor in (select CoProveedor from DOCUMENTO_PROVEEDOR where (CoObligacionPago IS NULL OR CoObligacionPago in ('','OTR')) AND FlMultiple=0)
   --and p.IDProveedor not in ('002315')   


UNION ALL

--ZICASSO
SELECT Cab.IDCAB, Cab.IDFIle,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN '' ELSE CONVERT(varchar,Det.IDDocFormaEgreso) END AS IDVoucher,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN '' ELSE CONVERT(varchar,Det.IDDocFormaEgreso) END AS NuVoucherSel, Cab.IDProveedor,
Cab.IDOperacion, Cab.DescProveedor, 'ZIC' AS TipoVouOrd,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN 'PD' ELSE Det.CoEstado END AS CoEstado,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN 'PENDIENTE' ELSE Det.NoEstado END Estado,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN Cab.TotalDet ELSE Det.SsMontoTotal END AS MontoTotal,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN Cab.TotalDet ELSE Det.SsSaldo END AS SaldoPendiente,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN 0.00 ELSE Det.SsDifAceptada END AS DifAceptada,
0.00 AS MontoTotal_USD, 0.00 AS SaldoPendiente_USD, 0.00 AS DifAceptada_USD,
CASE WHEN Det.IDDocFormaEgreso IS NULL THEN 'USD' ELSE Det.CoMoneda END CoMoneda,
'' As IDProvSetoursOS,'' As DescProvSetoursOS
FROM 
(SELECT C.IDCAB, IDFIle, D.IDProveedor, 0 AS IDOperacion, P.NombreCorto AS DescProveedor, SUM(ISNULL(Total,0)) AS TotalDet, P.NumIdentidad
FROM COTICAB C INNER JOIN COTIDET D ON C.IDCAB = D.IDCAB
INNER JOIN MAPROVEEDORES P ON D.IDProveedor = P.IDProveedor
INNER JOIN @Tbl_ServDet T ON D.IDServicio_Det = T.IDServicio_DetX AND D.IDProveedor = T.IDProveedorX AND D.IDServicio = T.IDServicioX
WHERE --(@TipoVouOrd = '' OR @TipoVouOrd = 'ZIC') 
(C.IDFile IS NOT NULL)
AND (C.FlHistorico = @FlHistorico OR @FlHistorico = 1) AND (@IDFile = '' OR C.IDFile LIKE '%' + @IDFile + '%')
AND (@Proveedor = '' OR P.NombreCorto LIKE '%' + @Proveedor + '%') AND (@NumIdentidad = '' OR P.NumIdentidad LIKE '%' + @NumIdentidad + '%')
AND (P.CoUbigeo_Oficina = @CoUbigeo_Oficina OR LTRIM(RTRIM(@CoUbigeo_Oficina)) = '') 
GROUP BY C.IDCAB, IDFIle, D.IDProveedor, P.NombreCorto, P.NumIdentidad) Cab LEFT OUTER JOIN
(SELECT DF.IDDocFormaEgreso, DF.TipoFormaEgreso, DF.CoMoneda, DF.CoEstado, DF.SsMontoTotal, DF.SsSaldo, DF.SsDifAceptada, DF.CoUbigeo_Oficina, DP.IDCab, EO.NoEstado
FROM DOCUMENTO_FORMA_EGRESO AS DF INNER JOIN DOCUMENTO_PROVEEDOR AS DP ON DF.IDDocFormaEgreso = DP.IDDocFormaEgreso
 AND DF.TipoFormaEgreso = DP.CoObligacionPago AND DF.CoMoneda = DP.CoMoneda_FEgreso
 INNER JOIN ESTADO_OBLIGACIONESPAGO EO ON DF.CoEstado = EO.CoEstado
WHERE (DF.TipoFormaEgreso = 'ZIC') AND (LTRIM(RTRIM(@NuVoucher)) = '' or DF.IDDocFormaEgreso Like @NuVoucher +'%')
AND (@NuDocum = '' OR NuDocum LIKE '%' + @NuDocum + '%')
GROUP BY DF.IDDocFormaEgreso, DF.TipoFormaEgreso, DF.CoMoneda, DF.CoEstado, DF.SsMontoTotal, DF.SsSaldo, DF.SsDifAceptada, DF.CoUbigeo_Oficina, DP.IDCab, EO.NoEstado)
 Det ON Cab.IDCAB = Det.IDCab And @TipoVouOrd='ZIC'
) AS Y2            
WHERE (TipoVouOrd=@TipoVouOrd OR  @TipoVouOrd='')             
 AND ((CoEstado=@CoEstado OR @CoEstado='')  OR CoEstado='OB'    )      
) AS Z    
where ((@SoloObservados=1 and orden_estado=0) or @SoloObservados=0)   
	and (IDProveedor not in ('002315','002317') OR @CoUbigeo_Oficina not in ('000110','000113') )
order by              
--CoEstado,            
Orden_Estado,            
IDVoucher,DescProveedor                 
END;


