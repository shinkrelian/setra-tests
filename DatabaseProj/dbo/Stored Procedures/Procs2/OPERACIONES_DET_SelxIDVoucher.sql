﻿--JRF-20141030-Agregar el IDCab como parametro            
--HLF-20141125-sd.CtaContable        
--JHD-20141201-Se agregó condición para mostrar RutaDocSustento, si no se encuentra en Operaciones_Det, mostrar de COTIDET        
--JHD-20141202-Se agregaron campos de ServicioEditado y ServicioVarios        
--HLF-20141205-sd.idTipoOC      
--JHD-20141205-Se agrego isnull a ServicioEditado      
--HLF-20150127-@CoProveedor char(6) =''    
 --(Rd.IDVoucher <> 0 or (@CoProveedor='001836' and Rd.IDVoucher = 0)) and     
 --(Rd.IDVoucher=@IDVoucher and @CoProveedor<>'001836')    
 --and (Rv.IDProveedor=@CoProveedor or LTRIM(rtrim(@CoProveedor))='')    
 --and Rv.IDCab=@IDCab    
 --Left Join VOUCHER_OPERACIONES vo On Rd.IDVoucher= vo.IDVoucher en vez de INNER    
 --HLF-20150202-@NuVoucher a tipo varchar(14)  
 --If Left(@IDVoucher,2)='PR' Set @IDVoucher='0'  

--JHD-20150716-case when rd.FlServInManualOper=1 then isnull(rd.IDMoneda,'')   else  isnull(rdd.IDMoneda,'')  end as IDMoneda,
--JHD-20150716-case when rd.FlServInManualOper=1 then isnull((select simbolo from MAMONEDAS where IDMoneda=rd.IDMoneda),'')   else  isnull((select simbolo from MAMONEDAS where IDMoneda=rdd.IDMoneda),'')  end as SimboloMoneda,                                  

CREATE Procedure [dbo].[OPERACIONES_DET_SelxIDVoucher]                          
 --@IDVoucher int,            
 @IDVoucher varchar(14),            
 @IDCab int =0,    
 @CoProveedor char(6) =''                                                         
As                                                                        
 Set NoCount On                           
 If Left(@IDVoucher,2)='PR' Set @IDVoucher='0'  
                                                            
 Select rd.IDReserva_Det,                                                            
 Rd.IDOperacion_Det,                                                            
 Rv.IDReserva as IDReserva,                                                            
 rd.IDOperacion,                                                             
 rv.IDFile,                                                            
 0 as IDDet,                                                            
 rd.Item,                                                            
 Rd.Dia,                                                              
 Case When Pr.IDTipoProv='001' Then                                                            
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                             
 Else                                                            
  upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                              
 End as DiaFormat,                                                                
 IsNull(Rd.FechaOut,'') as FechaOut,                                                            
 IsNull(upper(substring(DATENAME(dw,Rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,Rd.FechaOut,103)/*+' '+ substring(convert(varchar,Rd.FechaOut,108),1,5)*/)),'') as FechaOut,                                                               
 Case When Pr.IDTipoProv='001' and sd.PlanAlimenticio= 0 Then                                                             
 ''                                                            
 Else                                                             
  convert(varchar(5),rd.Dia,108)                                                            
 End as Hora,                                                               
 Ub.Descripcion as Ubigeo,                                                            
 Isnull(Rd.Noches,0) as Noches,                               
 Pr.IDTipoProv,                                         
 --Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,                                                           
 s.Dias,                                                            
 rv.IDProveedor,                                                            
 Pr.NombreCorto as Proveedor,                          
 ISNull(pr.Email3,'') CorreoProveedor ,                                                            
 Ub.IDPais,                                                    
 Rd.IDubigeo,                                                            
 Rd.Cantidad,                                              
                                         
 Rd.NroPax,                                  
 CAST(rd.NroPax AS VARCHAR(3))+                                  
 Case When isnull(rd.NroLiberados,0)=0 then '' else '+'+CAST(rd.NroLiberados AS VARCHAR(3)) end              as PaxmasLiberados,                                           
 Rd.IDServicio,                                                            
 Rd.IDServicio_Det,                                                            
 Rd.Servicio As DescServicioDet,                                                                
                                                            
 --Rd.Servicio As DescServicioDetBup,                                                               
 Case When pr.IDTipoProv='001' or pr.IDTipoProv='002' Then  --hoteles o restaurantes                                                  
 sd.Descripcion               
 Else                                
 s.Descripcion                                                    
 End As DescServicioDetBup,                                           
                                             
                           
 Rd.Especial,Rd.MotivoEspecial,        
 --rd.RutaDocSustento,        
 --cd.RutaDocSustento,        
         
 --RutaDocSustento=case when rd.RutaDocSustento IS null then (case when cd.RutaDocSustento is null then '' else cd.RutaDocSustento end) else rd.RutaDocSustento end,        
 RutaDocSustento=case when rd.RutaDocSustento IS null then (isnull(cd.RutaDocSustento,'')) else rd.RutaDocSustento end,        
         
 Rd.Desayuno,Rd.Lonche,Rd.Almuerzo,Rd.Cena,                                                            
 Rd.Transfer,Rd.IDDetTransferOri,Rd.IDDetTransferDes,0 as CamaMat,Rd.TipoTransporte,                                                            
 Rd.IDUbigeoOri,                                                            
 uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                              
 Rd.IDUbigeoDes,                                                            
 ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                                              
 idio.IDidioma as CodIdioma ,                  
 Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,                  
 idio.Siglas as Idioma ,                  
 sd.Anio,                  
 sd.Tipo,                                                             
 Isnull(sd.DetaTipo,'') As DetaTipo ,                                                  
                                       
 Rd.CantidadAPagar,                                              
                                         
 Rd.NroLiberados,Rd.Tipo_Lib,Rd.CostoRealAnt,Rd.Margen,                                                            
 Rd.MargenAplicado,Rd.MargenAplicadoAnt,Rd.MargenLiberado,Rd.TotalOrig,Rd.CostoRealImpto,Rd.CostoLiberadoImpto                                                      
 ,Rd.MargenImpto,Rd.MargenLiberadoImpto,Rd.TotImpto,                                                            
                                                             
                                                             
 '0' as IDDetRel ,                                    
                                     
 Rd.Total,                                                            
 Case When Cast(Rd.IDVoucher as varchar(10))='0' Then '' Else Cast(Rd.IDVoucher as varchar(10)) End as IDVoucher,                                                            
 '0' as IDReserva_DetCopia ,'0' as IDReserva_Det_Rel,                                                      
                                                             
 '' as IDEmailEdit, '' as IDEmailNew, '' as IDEmailRef,                                             
 --IsNull(Rd.FechaRecordatorio,'01/01/1900') as FechaRecordatorio, IsNull(Rd.Recordatorio,'') as Recordatorio,                                                           
 --IsNull(Rd.ObservVoucher,'') as ObservVoucher,                           
 IsNull(vo.ObservVoucher,'') as ObservVoucher,                        
 IsNull(Rd.ObservBiblia,'') as ObservBiblia,                                                             
 IsNull(Rd.ObservInterno,'') as ObservInterno  , rdd.CapacidadHab as CapacidadHab , rdd.EsMatrimonial As EsMatrimonial   ,                                                       
 sd.PlanAlimenticio ,  cd.IncGuia,                                                
 vo.ServExportac As chkServicioExportacion , Rd.VerObserVoucher ,Rd.VerObserBiblia  ,                                                               
                                               
 Rd.CostoReal, Rd.CostoLiberado,                                        
  Rd.NetoHab,Rd.IgvHab,Rd.TotalHab,Rd.NetoGen,Rd.IgvGen,                                    
  --isnull(rdd.IDMoneda,'') as IDMoneda,                                                                 
  case when rd.FlServInManualOper=1 then isnull(rd.IDMoneda,'')   else  isnull(rdd.IDMoneda,'')  end as IDMoneda,
  --isnull(mon.Simbolo,'') as SimboloMoneda,    
  case when rd.FlServInManualOper=1 then isnull((select simbolo from MAMONEDAS where IDMoneda=rd.IDMoneda),'')   else  isnull((select simbolo from MAMONEDAS where IDMoneda=rdd.IDMoneda),'')  end as SimboloMoneda,                                  
  
  isnull(sd.TC,0) as TipoCambio,                      
  Rd.TotalGen                                               
 ,                        
 Case When Pr.IDTipoOper = 'I' Then                         
 Isnull((select Top 1 IDProveedor_Prg                         
 from OPERACIONES_DET_DETSERVICIOS odd2 Left Join MAPROVEEDORES pr On odd2.IDProveedor_Prg = pr.IDProveedor                        
 where pr.IDTipoProv = '005' and odd2.IDOperacion_Det = rd.IDOperacion_Det Order by odd2.FecMod Desc ),'')              
 Else Isnull(Rd.IDGuiaProveedor,'') End As IDGuiaProveedor,                        
 0 As DescBus,                                              
 --rd.idTipoOC         
 sd.idTipoOC      
                                   
 ,0 as InactivoDet                                                   
 ,0 AS ServHotelAlimentPuro,                                  
 sd.ConAlojamiento, cd.SSCR,                              
 dbo.FnPNRxIDCabxDia(/*@IDCab*/Rv.IDCab,Rd.Dia) as PNR,        
 sd.CtaContable             
        
 --,cd.ServicioEditado        
  ,ServicioEditado=isnull(cd.ServicioEditado,CAST(0 as bit))      
 ,Case When                   
 Exists(select sd2.IDServicio from MASERVICIOS_DET SD2 Left Join MASERVICIOS S2 on SD2.IDServicio = S2.IDServicio                  
  where s2.IDCabVarios = @IDCAB and sd2.IDServicio_Det =cd.IDServicio_Det) then 1 else 0 End As ServicioVarios                                          
 From OPERACIONES Rv                                          
 Left Join OPERACIONES_DET Rd On Rd.IDOperacion=Rv.IDOperacion                                                            
 Left Join MAUBIGEO Ub On Rd.IDubigeo=Ub.IDubigeo                                                                   
 Left Join MAUBIGEO uo On Rd.IDUbigeoOri=uo.IDubigeo                                                              
 Left Join MAUBIGEO ud On Rd.IDUbigeoDes=ud.IDubigeo                                                               
 Left Join MAPROVEEDORES Pr On  Pr.IDProveedor= Rv.IDProveedor                                                                  
 Left Join MASERVICIOS_DET sd On  Rd.IDServicio_Det=sd.IDServicio_Det                                                            
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio                                                            
 Left Join VOUCHER_OPERACIONES vo On Rd.IDVoucher= vo.IDVoucher                                                          
 Left Join RESERVAS_DET rdd On Rd.IDReserva_Det=rdd.IDReserva_Det                                              
 Left Join COTIDET cd On rdd.IDDet=cd.IDDET                                                
 Left Join MAMONEDAS mon on Rdd.IDMoneda=mon.IDMoneda                                    
 Left Join MAIDIOMAS idio On Rd.IDIdioma = idio.IDidioma                  
 --Where Rv.IDCab=@IDCab                                                            
 where --((vh.IDVoucher=@IDVoucher Or Rd.IDVoucher=@IDVoucher) and Rd.IDVoucher<>0)            
 (Rd.IDVoucher <> 0 or (@CoProveedor='001836' and Rd.IDVoucher = 0)) and     
 (Rd.IDVoucher=@IDVoucher or @CoProveedor='001836')    
 and (Rv.IDProveedor=@CoProveedor or LTRIM(rtrim(@CoProveedor))='')    
 and Rv.IDCab=@IDCab    
Order By Rd.IDVoucher desc,Rd.Item Asc    

