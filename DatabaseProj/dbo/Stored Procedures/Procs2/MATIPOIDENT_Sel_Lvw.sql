﻿CREATE Procedure [dbo].[MATIPOIDENT_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion	
	From MATIPOIDENT
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDIdentidad <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
