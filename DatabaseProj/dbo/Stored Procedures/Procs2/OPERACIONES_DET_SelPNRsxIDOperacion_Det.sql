﻿--JRF-20130801-Considerar si existen datos en la cotitrans_pax, en caso de no existir colocar el codReserva
CREATE Procedure dbo.OPERACIONES_DET_SelPNRsxIDOperacion_Det  
@IDCab int,  
@Dia smalldatetime  
As  
 Set NoCount On  
 if Exists(select CodReservaTransp from COTITRANSP_PAX ctp Left Join COTIPAX cp On ctp.IDPax =cp.IDPax  
	where IDTransporte In (select ID from COTIVUELOS where IDCAB = @IDCab   
	and CONVERT(char(10),Fec_Salida,103) = CONVERT(char(10),@Dia,103)) and CodReservaTransp is not null)
	 Begin
	 select cp.Apellidos,CodReservaTransp,COUNT(CodReservaTransp) as Pax   
	 from COTITRANSP_PAX ctp Left Join COTIPAX cp   
	 On ctp.IDPax =cp.IDPax  
	 where IDTransporte In (select ID from COTIVUELOS where IDCAB = @IDCab   
	 and CONVERT(char(10),Fec_Salida,103) = CONVERT(char(10),@Dia,103) )  
	 and CodReservaTransp is not null  
	 group by cp.Apellidos,CodReservaTransp  
	 End
 else
 Begin	 
	 select distinct '' as Apellidos,isnull(CodReserva,'') as CodReservaTransp from COTIVUELOS where IDCAB = @IDCab   
	 and CONVERT(char(10),Fec_Salida,103) = CONVERT(char(10),@Dia,103)
 End
