﻿CREATE Procedure dbo.MAUBIGEO_Sel_Cbo  
 @TipoUbig varchar(100),  
 @IDPais varchar(6),  
 @bTodos bit  
As  
  
 Set NoCount On  
 set quoted_identifier off  
   
   
 Declare @TipoUbig2 varchar(40)=REPLACE(@TipoUbig,'","','')  
 --Set @TipoUbig2=REPLACE(@TipoUbig2,'"',char(39))  
 --Set @TipoUbig=REPLACE(@TipoUbig,'"',char(39))  
   
   
 Declare @vSQL varchar(max)='SELECT * FROM  
 (  
 Select "" as IDUbigeo,"<TODOS>" as Descripcion, 0 as Ord  
 Union  
 Select IDubigeo,Descripcion + space(100) + char(124) + isnull(DC,"") as Descripcion,1 From MAUBIGEO  
 Where TipoUbig IN('+@TipoUbig+') Or LTRIM(RTRIM('+@TipoUbig2+'))="" And  
 (IDPais="'+@IDPais+'" Or LTRIM(RTRIM("'+@IDPais+'"))="")  
 ) AS X  
 Where ('+Cast(@bTodos as char(1))+'=1 Or Ord<>0)  
 Order by Ord,2'  
  
 Execute(@vSQL)  
 --Print @vSQL  
