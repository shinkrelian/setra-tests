﻿--JRF-20150304-... and FlServicioNoShow=0
CREATE Procedure [dbo].[RESERVAS_DET_PAX_DelxIDReserva]  
 @IDReserva int  
As  
  
 Set NoCount On  
 Delete from RESERVAS_DET_PAX Where IDReserva_Det In   
  (Select IDReserva_Det From RESERVAS_DET Where IDReserva=@IDReserva and FlServicioNoShow=0)
