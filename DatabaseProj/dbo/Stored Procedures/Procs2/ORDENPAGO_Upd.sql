﻿--JRF-20130726-Nuevo campo Numeroformapago
--JHD-20150611-Nuevo campo (SSDetraccion).
CREATE Procedure dbo.ORDENPAGO_Upd    
 @IDOrdPag int,    
 @IDFormaPago char(3),     
 @IDBanco char(3),  
 @FechaPago smalldatetime,     
 @FechaEnvio smalldatetime,     
 @Porcentaje numeric(5,2),      
 @TCambio numeric(5,2),      
 @Numeroformapago varchar(20),
 @Observaciones varchar(max),     
 @CtaCte varchar(30),     
 @IDMoneda char(3),     
 @IDEstado char(2),
 @SSDetraccion numeric(12,2)=0,  
 @UserMod char(4)    
As    
 Set Nocount on    
     
 Update ORDENPAGO Set IDFormaPago=@IDFormaPago,  
  IDBanco = @IDBanco,     
  FechaPago=@FechaPago,     
  FechaEnvio=Case When @IDEstado='PD' Then GETDATE() Else Case When @FechaEnvio = '01/01/1900' Then Null Else @FechaEnvio End End,     
  Porcentaje=@Porcentaje,     
  Numeroformapago = Case When ltrim(rtrim(@Numeroformapago))='' then Null else @Numeroformapago End,
  TCambio=Case When @TCambio=0 Then Null Else @TCambio End,     
  Observaciones=Case When ltrim(rtrim(@Observaciones))='' Then Null Else @Observaciones End,        
  CtaCte=Case When ltrim(rtrim(@CtaCte))='' Then Null Else @CtaCte End,       
  IDMoneda=@IDMoneda, IDEstado=@IDEstado, UserMod=@UserMod,
  SSDetraccion=@SSDetraccion,
   FecMod=GETDATE()    
 Where IDOrdPag=@IDOrdPag    

