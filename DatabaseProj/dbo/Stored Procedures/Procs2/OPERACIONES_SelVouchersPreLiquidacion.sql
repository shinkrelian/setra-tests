﻿
--HLF-20140819-dbo.FnCambioMoneda(CompraNeto,IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Y.IDMoneda),TC))        
--JRF-20141009-Left Join RESERVAS r On O.IDReserva=r.IDReserva (WHERE ... and r.Anulado=0 and RD.Anulado=0)    
--HLF-20141204-DescPaisProveedor  
--HLF-20141205-Case When idTipoOC='011' Then 0  
--Case When idTipoOC='011' Then VentaUSD Else 0 End as BoletaServicioExterior  
--HLF-20150205-@CoUbigeo_Oficina char(6)
--And p.CoUbigeo_Oficina=@CoUbigeo_Oficina
--HLF-20150206-Forzar a Factura cuando es @CoUbigeo_Oficina<>'' (Buenos Aires, Santiago)
--HLF-20150211-And (P.IDTipoOper='E' Or LTRIM(rtrim(@CoUbigeo_Oficina))<>'') 
--WHERE CostoBaseUSD<>0
--Declare @MargenInterno as numeric(6,2)=10
--Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 	dbo.FnPromedioPonderadoMargen(@IDCab, Z.IDProveedor) ...
--HLF-20150303-And (@CoUbigeo_Oficina<>'000113' Or P2.IDProveedor<>'002329') --Transporte Buenos Aires
--HLF-20150304-(SELECT isnull(sum(dbo.FnCambioMoneda(ODS2.Total,ods2.IDMoneda,'USD',isnull(tca.SsTipCam,sd2.TC))),0)
--Case When @CoUbigeo_Oficina='000113' and o.IDProveedor='002315' Then
CREATE Procedure dbo.OPERACIONES_SelVouchersPreLiquidacion              
 @IDCab int,
 @CoUbigeo_Oficina char(6)
As              
             

 Set Nocount On              

 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)              
 Declare @MargenInterno as numeric(6,2)=10
 --Declare @TipoCambio as numeric(8,2) =(Select TipoCambio From COTICAB Where IDCAB=@IDCab)                  
 
 --VOUCHERS              

 SELECT                

  IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,IDVoucher,CompraNeto,              

  IGVCosto,IGV_SFE,Moneda,Total, idTipoOC,              

  DescOperacionContab, Margen,                   

  CostoBaseUSD,            

              

  VentaUSD,               



		

	Case When idTipoOC='011' and @CoUbigeo_Oficina='' Then 

		0  

	Else  

		Case When @CoUbigeo_Oficina='' Then

			Case When idTipoOC='006' Then   

				VentaUSD/2        

			Else        

				Case When idTipoOC='001' Then VentaUSD Else 0 End         

			End   

		Else

			VentaUSD

		End

	End As FacturaExportacion,              

    

  Case When idTipoOC='011' or @CoUbigeo_Oficina<>'' Then 0  

  Else        

   Case When idTipoOC='006' Then VentaUSD/2        

   Else        

    Case When idTipoOC='001' Then 0   

    Else         

   --(Case When IDProveedor='001836' Then VentaUSD Else VentaUSDxPax End)         

   VentaUSD        

    End   

   End        

  End As BoletaNoExportacion,  

  Case When idTipoOC='011' and @CoUbigeo_Oficina='' Then VentaUSD Else 0 End as BoletaServicioExterior  

              

 FROM              

  (              

  SELECT               

    IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,IDVoucher,CompraNeto,              

    IGVCosto,IGV_SFE,Moneda,        

    CompraNetoUSD+IGVCostoUSD+IGV_SFEUSD AS Total,        

    idTipoOC,              

    DescOperacionContab, Margen,               

    --CompraNetoUSD+IGVCostoUSD+IGV_SFEUSD AS CostoBaseUSD,                   

 CompraNetoUSD+IGVCostoUSD AS CostoBaseUSD,               

                

    --ROUND( (CompraNetoUSD+IGVCostoUSD+IGV_SFEUSD ) /(1 - (Margen/100)) ,0) AS VentaUSD        

    ROUND( (CompraNetoUSD+IGVCostoUSD) /(1 - (Margen/100)) ,0) AS VentaUSD        

    --ROUND( (CompraNetoxPax+IGVCostoxPax+IGV_SFExPax) /(1 - (Margen/100)) ,0) AS VentaUSDxPax              

                  

   FROM               

   (              

   SELECT YY.IDProveedor,YY.DescProveedor, YY.DescProveedorInternac,DescPaisProveedor,  

   YY.IDVoucher,              

    YY.CompraNeto,--YY.CompraNetoxPax,        

    CompraNetoUSD,        

    YY.IGVCosto, --yy.IGVCostoxPax,             

    IGVCostoUSD,        

    Case When YY.idTipoOC='001' Then               

     YY.CompraNeto*(@IGV/100)              

    Else                

     Case When YY.idTipoOC='006' Then (YY.CompraNeto*(@IGV/100))*0.5 Else 0 End              

    End as IGV_SFE,              

            

    Case When YY.idTipoOC='001' Then               

     YY.CompraNetoUSD*(@IGV/100)              

    Else                

     Case When YY.idTipoOC='006' Then (YY.CompraNetoUSD*(@IGV/100))*0.5 Else 0 End              

    End as IGV_SFEUSD,              

            

    --Case When YY.idTipoOC='001' Then               

    -- YY.CompraNetoxPax*(@IGV/100)              

    --Else                

    -- Case When YY.idTipoOC='006' Then (YY.CompraNetoxPax*(@IGV/100))*0.5 Else 0 End              

    --End as IGV_SFExPax,              

    yy.Moneda, --YY.Total,               

    idTipoOC,YY.DescOperacionContab, YY.Margen              

    --YY.VentaUSD,                

    --Case When YY.idTipoOC='001' Then VentaUSD Else 0 End As FacturaExportacion,              

    --Case When YY.idTipoOC='001' Then 0 Else VentaUSD End As BoletaNoExportacion              

   FROM              

    (              

    SELECT XX.*              

     --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD              

      --CostoBaseUSD /(1 - (Margen/100)) AS VentaUSD              

    FROM              

     (              

     SELECT Z.*,               

     --(SELECT AVG(MargenAplicado) FROM COTIDET WHERE IDCAB=@IDCab AND IDProveedor=Z.IDProveedor And isnull(IDDetRel,0)=0) AS Margen,              

     --dbo.FnPromedioPonderadoMargen(@IDCab, Z.IDProveedor) AS Margen

     Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 

		dbo.FnPromedioPonderadoMargen(@IDCab, Z.IDProveedor) 

	 Else

		@MargenInterno

	 End AS Margen

     --(SELECT SUM(CostoReal*NroPax) FROM COTIDET WHERE IDCAB=@IDCab AND IDProveedor=Z.IDProveedor) as CostoBaseUSD              

     FROM              

      (              

      SELECT IDProveedor, DescProveedorInternac,              

      DescProveedor,DescPaisProveedor,IDVoucher,              

                

                

      --dbo.FnCambioMoneda(CompraNeto,'USD',IDMoneda,isnull(@TipoCambio,TC)) as CompraNeto,          

      CompraNeto as CompraNeto,          

      dbo.FnCambioMoneda(CompraNeto,IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Y.IDMoneda),TC)) as CompraNetoUSD,          

      --dbo.FnCambioMoneda(CompraNetoxPax,'USD',IDMoneda,isnull(@TipoCambio,TC)) as CompraNetoxPax,          

      --dbo.FnCambioMoneda(IGVCosto,'USD',IDMoneda,isnull(@TipoCambio,TC)) as IGVCosto,          

      IGVCosto as IGVCosto,          

      dbo.FnCambioMoneda(IGVCosto,IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Y.IDMoneda),TC)) as IGVCostoUSD,          

      --dbo.FnCambioMoneda(IGVCostoxPax,'USD',IDMoneda,isnull(@TipoCambio,TC)) as IGVCostoxPax,          

                  

                

      IDMoneda As Moneda ,               

      --CompraNeto+IGVCosto As Total,               

      idTipoOC,DescOperacionContab              

      FROM               

       (              

              

       SELECT IDProveedor,DescProveedorInternac, DescProveedor,DescPaisProveedor,  

       IDVoucher,              

       (SUM(CostoReal)) as CompraNeto,               

       --(SUM(CostoRealxPax)) as CompraNetoxPax,               

       SUM(TotImpto) as IGVCosto,            

       --SUM(TotImptoxPax) as IGVCostoxPax,            

       idTipoOC,DescOperacionContab,IDMoneda,TC          

       FROM              

        (              

        SELECT O.IDProveedor,               

        pint.NombreCorto as  DescProveedorInternac,              

        P.NombreCorto AS DescProveedor,   

        paPI.Descripcion as DescPaisProveedor,  

        OD.IDVoucher, OD.NroPax,                                   

                      

        Case When SD.ConAlojamiento=1 Or p.IDProveedor='001836' Then         

	   --OD.CostoReal * (OD.NroPax  + ISNULL(OD.NroLiberados,0)     )        

	   od.NetoGen        

	  Else        

	   Case When sd.idTipoOC In ('006','001') Then--Proporcion o Exportacion        

		od.NetoGen        

	   else        
	    Case When @CoUbigeo_Oficina<>'000113' Then 
			od.TotalGen			
		Else--
			
			Case when o.IDProveedor='002315' Then --Setours Buenos Aires
				(SELECT isnull(sum(dbo.FnCambioMoneda(ODS2.Total,ods2.IDMoneda,'USD',isnull(tca.SsTipCam,sd2.TC))),0)
				FROM OPERACIONES_DET_DETSERVICIOS ODS2 
				INNER JOIN OPERACIONES_DET OD2 ON ODS2.IDOperacion_Det=OD2.IDOperacion_Det AND OD2.IDServicio=ODS2.IDServicio
				INNER JOIN OPERACIONES O2 ON OD2.IDOperacion=O2.IDOperacion AND O2.IDCab=@IDCab AND O2.IDProveedor=o.IDProveedor--Setours Buenos Aires
				INNER JOIN MASERVICIOS_DET SD2 ON ODS2.IDServicio_Det_V_Cot=SD2.IDServicio_Det
				INNER JOIN MASERVICIOS S2 ON SD2.IDServicio=S2.IDServicio 
				INNER JOIN MAPROVEEDORES P2 ON S2.IDProveedor=P2.IDProveedor AND P2.IDProveedor<>'002329'--Transporte Buenos Aires 
				LEft Join COTICAB_TIPOSCAMBIO tca On tca.IDCab=o2.IDCab And tca.CoMoneda=ods2.IDMoneda
				WHERE ODS2.IDOperacion_Det IN(od.IDOperacion_Det))
			Else
				od.TotalGen
			End


		End
	   End        

	  End as CostoReal,          


        Case When sd.idTipoOC in('002','006') Then               

   Case When SD.ConAlojamiento=1 Or p.IDProveedor='001836' Then         

    --OD.CostoReal *(@IGV/100)* (OD.NroPax  + ISNULL(OD.NroLiberados,0)     )        

    OD.NetoGen*(@IGV/100)        

   Else        

   Case When sd.idTipoOC = '002' Then        

    OD.TotalGen*(@IGV/100)         

   Else--Proporcion 006        

    (OD.NetoGen*(@IGV/100) )/2        

   End        

  End         

  Else              

         0              

        End               

        As TotImpto,              

        sd.idTipoOC,              

        tox.Descripcion as DescOperacionContab, 

		Case When @CoUbigeo_Oficina='000113' and o.IDProveedor='002315' Then
			'USD'
		Else
			RD.IDMoneda
		End as IDMoneda, 


		SD.TC              

                      

        FROM OPERACIONES O INNER JOIN MAPROVEEDORES P ON O.IDProveedor=P.IDProveedor              

        --And P.IDTipoOper='E'

        And (P.IDTipoOper='E' Or LTRIM(rtrim(@CoUbigeo_Oficina))<>'') 

			And (P.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='' )

        INNER JOIN OPERACIONES_DET OD ON O.IDOperacion=OD.IDOperacion                

        AND (OD.IDVoucher<>0 OR O.IDProveedor='001836' )--PERURAIL              

        INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              

        INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det              

        Inner join MATIPOOPERACION tox on sd.idTipoOC=tox.IdToc              

        Left Join MAPROVEEDORES pint On pint.IDProveedor=p.IDProveedorInternacional              

          

        Left Join MAUBIGEO ubPI On p.IDCiudad=ubPI.IDubigeo  
        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo 
        Left Join RESERVAS r On O.IDReserva=r.IDReserva    





        WHERE O.IDCab=@IDCab and r.Anulado=0 and RD.Anulado=0 --AND (OD.IDVoucher<>0 OR O.IDProveedor='001836' )              

		
        ) AS X              

       GROUP BY IDProveedor,DescProveedorInternac,DescProveedor,DescPaisProveedor,idTipoOC,DescOperacionContab,IDVoucher,IDMoneda,TC          

       ) AS Y              

      ) AS Z          

     ) AS XX              

    ) AS YY              

   ) AS ZZ              

  ) AS ZZ1              

 WHERE CostoBaseUSD<>0

 ORDER BY IDVoucher              


