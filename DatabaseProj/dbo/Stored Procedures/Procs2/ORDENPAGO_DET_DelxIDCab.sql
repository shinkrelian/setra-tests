﻿
Create Procedure dbo.ORDENPAGO_DET_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM ORDENPAGO_DET 
	WHERE IDReserva_Det IN 
		(SELECT IDReserva_Det FROM RESERVAS_DET WHERE IDReserva IN 
			(SELECT IDReserva FROM RESERVAS WHERE IDCab = @IDCab))
	
