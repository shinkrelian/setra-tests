﻿--HLF-20141106-if @SsSaldoPend = 0  
--JHD-20150821-No actualizar estado a Aceptado por BD
--JHD-20151118-@CoMoneda_FEgreso char(3)=''
--JHD-20151118-Actualizar en ORDEN_SERVICIO_DET_MONEDA
CREATE Procedure dbo.ORDEN_SERVICIO_UpdSsPendientexDoc  
@NuOrdenServicio int,  
@SsTotalOrig numeric(9,2),  
@UserMod char(4),
@CoMoneda_FEgreso char(3)=''  
As  
Set NoCount On  
Update ORDEN_SERVICIO  
 Set SsSaldo= ORDEN_SERVICIO.SsSaldo-@SsTotalOrig,  
  UserMod=@UserMod,  
  FecMod=GETDATE()  
where NuOrden_Servicio=@NuOrdenServicio  

if rtrim(ltrim(@CoMoneda_FEgreso))<>''
begin
	Update ORDEN_SERVICIO_DET_MONEDA 
	Set SsSaldo= ORDEN_SERVICIO_DET_MONEDA.SsSaldo-@SsTotalOrig,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  
where NuOrden_Servicio=@NuOrdenServicio  and CoMoneda=@CoMoneda_FEgreso
end 

Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from ORDEN_SERVICIO where NuOrden_Servicio=@NuOrdenServicio)  
----if @SsSaldoPend <= 0  
--if @SsSaldoPend = 0  
-- Update ORDEN_SERVICIO set CoEstado='AC' where NuOrden_Servicio=@NuOrdenServicio  

