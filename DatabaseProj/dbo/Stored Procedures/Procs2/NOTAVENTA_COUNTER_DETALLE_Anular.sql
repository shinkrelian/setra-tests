﻿


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE procedure NOTAVENTA_COUNTER_DETALLE_Anular
		   @NuNtaVta char(6),
		   @NuItem tinyint,
		   @NuDetalle tinyint,
		   @FlActivo bit,
           @UserMod char(4)=''
as
BEGIN
Set NoCount On
	
update [NOTAVENTA_COUNTER_DETALLE]
SET 
           FlActivo=@FlActivo
           ,UserMod=@UserMod
           ,FecMod=GETDATE()
WHERE
			NuNtaVta=@NuNtaVta AND
			NuItem=@NuItem AND
            NuDetalle=@NuDetalle      
            
END;
