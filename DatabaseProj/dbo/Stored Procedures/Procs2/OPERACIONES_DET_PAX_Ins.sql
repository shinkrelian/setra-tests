﻿CREATE Procedure dbo.OPERACIONES_DET_PAX_Ins  
 @IDOperacion_Det int,   
 @IDPax int,   
 @UserMod char(4)  
As  
 Set NoCount On  
 If Exists(select IDOperacion_Det from OPERACIONES_DET where IDOperacion_Det = @IDOperacion_Det)
 Begin
 Insert Into OPERACIONES_DET_PAX(IDOperacion_Det, IDPax, UserMod)  
 Values(@IDOperacion_Det, @IDPax, @UserMod)  
 End
