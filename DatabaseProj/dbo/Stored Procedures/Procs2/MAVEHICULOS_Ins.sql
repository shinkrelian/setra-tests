﻿Create Procedure dbo.MAVEHICULOS_Ins
@QtDe tinyint,
@QtHasta tinyint,
@NoVehiculoCitado varchar(50),
@QtCapacidad tinyint,
@FlOtraCiudad bit,
@UserMod char(4)
As
	Set NoCount On
	Declare @NuVehiculo smallint
	Execute dbo.Correlativo_SelOutput 'MAVEHICULOS',1,10,@NuVehiculo output
	
	Insert Into MAVEHICULOS
	Values (@NuVehiculo,@QtDe,@QtHasta,@NoVehiculoCitado
	,@QtCapacidad,@FlOtraCiudad,@UserMod,GETDATE())
