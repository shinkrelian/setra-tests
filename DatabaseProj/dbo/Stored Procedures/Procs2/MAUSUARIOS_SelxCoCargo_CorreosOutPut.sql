﻿Create Procedure dbo.MAUSUARIOS_SelxCoCargo_CorreosOutPut
@CoCargo tinyint,
@pCorreos varchar(100) output
As
Set NoCount On
Declare @Correo varchar(100)=''
Set @pCorreos = ''

ReEval:
Declare curUsers cursor for
select Correo from MAUSUARIOS where CoCargo=@CoCargo And FlTrabajador_Activo=1
Open curUsers
Fetch Next from curUsers into @Correo
While @@FETCH_STATUS=0
Begin
	Set @pCorreos = @pCorreos + @Correo + '; '

	Fetch Next from curUsers into @Correo
End
Close curUsers
DealLocate curUsers

if @CoCargo = 56 and Ltrim(Rtrim(@pCorreos)) = ''
Begin
	set @CoCargo = 19
	goto ReEval
End
