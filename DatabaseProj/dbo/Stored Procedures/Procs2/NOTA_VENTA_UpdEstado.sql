﻿
Create Procedure  dbo.NOTA_VENTA_UpdEstado
	@IDNotaVenta char(8),
	@IDEstado char(2),
	@UserMod char(4)
	
As
	Set Nocount On
	
	Update NOTA_VENTA Set IDEstado=@IDEstado, UserMod=@UserMod, FecMod=GETDATE()
	Where IDNotaVenta=@IDNotaVenta
	
