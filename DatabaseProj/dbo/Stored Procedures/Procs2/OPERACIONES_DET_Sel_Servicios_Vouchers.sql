﻿

--OPERACIONES_DET_Sel_Servicios_Vouchers 400002,'0010001'
 --JHD-20141016-SE AGREGO COLUMNA IDOperacion_Det
 --ALTER
CREATE Procedure [dbo].[OPERACIONES_DET_Sel_Servicios_Vouchers]                
@IDVoucher int --416726

As                  
 Set Nocount On                  
BEGIN                  
 SELECT                   
 --IDVoucher,
 --IDFile,
 Dia,
 Ciudad=Direccion,   
 Servicio, 
 Pax,
 TipoServ,
IDOperacion_Det
 /*,
 DescProveedor,
               
 Titulo,
 
 Responsable,
 CodReservaProv,
 IDIdioma,
 IdiomaServicio,
 Nacionalidad,                  
  
 CASE WHen LEN(DATEPART(DD,Dia)) = 1 then '0'+CAST(DATEPART(DD,Dia)as varchar(10)) else                
 CAST(DATEPART(DD,Dia) as varchar(10)) End + ' ' +                
 SUBSTRING(DATENAME(MM,Dia),1,3)                
 +' ' + SUBSTRING(CAST(DATEPART(YEAR,Dia) AS VARCHAR(10)),3,4) as FechaIn,                    
                 
 IsNull(upper(substring(DATENAME(dw,FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,FechaOut,103))),'') as FechaOut,                     
 substring(convert(varchar,Dia,108),1,5) as Hora,                  
                   
 Cantidad,
 
 Buffet,
 ObservVoucher,

 ExtraNoIncluido,
 ObservInterno,

 NroPaxCoti,
 CantServicios,  
 DescripcionTarifa  
 */
 FROM                  
                   
 (                  
 Select od.IDVoucher,
 o.IDFile,
 p.NombreCorto as DescProveedor,                  
 --p.Direccion + isnull(char(13) + uPro.Descripcion,'') as Direccion ,                   
 isnull(uPro.Descripcion,'') as Direccion ,                   
 c.Titulo,                  
         
 od.NroPax + Isnull(od.NroLiberados,0) as Pax,                  
 us.Nombre as Responsable,                  
 IsNull(r.CodReservaProv,'') as CodReservaProv,                   
 c.IDIdioma,
 od.IDIdioma as IdiomaServicio,                  
 u.Descripcion as Nacionalidad,                  
                   
 od.Dia,                    
 --(Select MAX(FechaOut) From OPERACIONES_DET Where IDOperacion=@IDOperacion And IDVoucher<>0) as FechaOut,    
 GETDATE() as FechaOut,                
 od.Cantidad,
 od.Servicio,           
 LTRIM(                  
 Case When od.Desayuno=1 Then 'DESAYUNO ' Else '' End+                  
 Case When od.Lonche=1 Then 'BOX LUNCH ' Else '' End+                   
 Case When od.Almuerzo = 1 Then 'ALMUERZO ' Else '' End+                   
 Case When od.Cena=1 Then 'CENA' Else '' End) as Buffet,                  

 --dbo.FnUnionObservacionVoucherInternoBibliaxOperacion(@IDOperacion,od.IDVoucher) As ObservVoucher,
 '' As ObservVoucher,
 Case when s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End as  TipoServ                
 ,(select ExtrNoIncluid from VOUCHER_OPERACIONES vo where vo.IDVoucher = od.IDVoucher )  as ExtraNoIncluido    ,            
 od.ObservInterno ,
 c.NroPax+Isnull(c.NroLiberados,0) as NroPaxCoti ,  
	(select COUNT(distinct od2.IDServicio_Det)
	 from OPERACIONES_DET od2 where od2.IDOperacion = o.IDOperacion) as CantServicios,  
 sd.DetaTipo as DescripcionTarifa,  
 OD.IDOperacion_Det        
 FROM OPERACIONES_DET od                   
 Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion                  
 Left Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor                  
 Left Join RESERVAS r On r.IDReserva=o.IDReserva                  
 Left Join COTICAB c On c.IDCAB=o.IDCab                  
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo                   
 Left Join MAUSUARIOS us On c.IDUsuario=us.IDUsuario                  
 Left Join MAUBIGEO uPro On p.IDCiudad = uPro.IDubigeo                
 Left Join MASERVICIOS s On od.IDServicio = s.IDServicio                
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det  
 --Where od.IDOperacion=@IDOperacion And od.IDVoucher<>0                  
 Where od.IDVoucher=@IDVoucher  And od.IDVoucher<>0         
 --and (@NuDocum='' or od.IDVoucher in (select distinct NuVoucher from DOCUMENTO_PROVEEDOR where NuDocum like '%'+@NuDocum+'%'))
 
          
 ) as X                  
 order by X.Dia
 END;
