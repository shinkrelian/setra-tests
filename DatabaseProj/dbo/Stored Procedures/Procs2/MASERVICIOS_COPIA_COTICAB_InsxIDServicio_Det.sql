﻿Create Procedure dbo.MASERVICIOS_COPIA_COTICAB_InsxIDServicio_Det
@IDServicioAnt char(8),
@IDServicioNuev char(8),
@IDCab int,
@UserMod char(4)
As
Set NoCount On

Insert into MASERVICIOS_COPIA_COTICAB
select @IDServicioAnt,@IDServicioNuev,sd.IDServicio_Det,Null,@IDCab,@UserMod,Getdate()
from MASERVICIOS_DET sd
Where sd.IDServicio = @IDServicioAnt And sd.Estado='A' 
And Not sd.IDServicio_Det In (select IDServicio_Det from MASERVICIOS_COPIA_COTICAB Where IDCab=@IDCab)
