﻿Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_TMP_Upd_ProveedoPrg
@IDCab int,
@IDProveedor char(6),
@UserMod char(4)
As
Set NoCount On
Update OPERACIONES_DET_DETSERVICIOS_TMP 
	Set IDProveedor_Prg = Case When pCot.IDTipoProv In ('005','008') Then 
							Case When IsNull((select count(distinct IDProveedor_Prg)
											  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
											  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv In ('005','008')),0)=1 
							 then (select distinct IDProveedor_Prg
								   from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
								   where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv In ('005','008'))
							 Else Null End
						  Else
							Case When IsNull((select count(distinct IDProveedor_Prg)
											  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
											  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv =pCot.IDTipoProv),0)=1
							 then (select distinct IDProveedor_Prg
								  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
								  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv =pCot.IDTipoProv)
							 End
						  End,
		IDEstadoSolicitud=IsNull(
							Case When pCot.IDTipoProv In ('005','008') Then 
							Case When IsNull((select count(distinct IDEstadoSolicitud)
											  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
											  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv In ('005','008')),0)=1 
							 then (select distinct IDEstadoSolicitud
								   from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
								   where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv In ('005','008'))
							 Else Null End
						  Else
							Case When IsNull((select count(distinct IDEstadoSolicitud)
											  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
											  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv =pCot.IDTipoProv),0)=1
							 then (select distinct IDEstadoSolicitud
								  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
								  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv =pCot.IDTipoProv)
							 End
						  End
						  ,'NV'),
		IDVehiculo_Prg = Case When IsNull((select count(distinct IDVehiculo_Prg)
											  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
											  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv =pCot.IDTipoProv),0)=1
							 then (select distinct IDVehiculo_Prg
								  from OPERACIONES_DET_DETSERVICIOS ods Inner Join MAPROVEEDORES p On ods.IDProveedor_Prg=p.IDProveedor
								  where IDOperacion_Det=odTmp.IDOperacion_DetAntiguo And IDProveedor_Prg is not null And p.IDTipoProv =pCot.IDTipoProv)
							 End,
		UserMod = @UserMod
From OPERACIONES_DET_DETSERVICIOS_TMP odsTmp
Inner Join OPERACIONES_DET_TMP odTmp On odsTmp.IDOperacion_Det=odTmp.IDOperacion_Det
Inner Join OPERACIONES o2 On odTmp.IDOperacion=o2.IDOperacion And o2.IDCab=@IDCab And o2.IDProveedor=@IDProveedor
Inner Join MASERVICIOS_DET sd On odsTmp.IDServicio_Det = sd.IDServicio_Det
Inner Join MASERVICIOS_DET sdCot On sd.IDServicio_Det_V = sdCot.IDServicio_Det
Inner Join MASERVICIOS sCot On sdCot.IDServicio=sCot.IDServicio
Inner Join MAPROVEEDORES pCot On sCot.IDProveedor=pCot.IDProveedor
Where odsTmp.IDOperacion_DetReal = 0
