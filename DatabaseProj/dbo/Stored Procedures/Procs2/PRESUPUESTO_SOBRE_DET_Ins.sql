﻿--HLF-20140731-@IDOperacion_Det int,    
--HLF-20140731-Comentando FlTicket    
--HLF-20140801-Agregando @CoPago    
--HLF-20140811-Agregando @CoPrvPrg    
--JRF-20150304-Agregando @IDMoneda
--JRF-20150408-Agregar el campo IDPax
--JRF-20150423-Considerar la posibilidad de grabar Null en OperacionesDet.
--HLF-20151103-Agregando @IDServicio_Det_V_Cot
CREATE Procedure dbo.PRESUPUESTO_SOBRE_DET_Ins
 @NuPreSob int,        
 @FeDetPSo smalldatetime,      
 @CoTipPSo char(2),        
 @IDServicio_Det int,        
 @IDOperacion_Det int,    
 @NuSincerado_Det int,        
 @TxServicio varchar(250),        
 @QtPax smallint,        
 @SsPreUni numeric(12,4),        
 @SsTotal numeric(12,2),        
 @SsTotSus numeric(12,2),        
 @SsTotDev numeric(12,2),           
 @CoPago char(3),    
 @CoPrvPrg char(6),
 @IDPax int,
 @IDMoneda char(3),
 @IDServicio_Det_V_Cot int,
 @UserMod char(4)        
As        
 Set Nocount On        
 Declare @NuDetPSo int=isnull((Select MAX(NuDetPSo)+1         
  From PRESUPUESTO_SOBRE_DET         
  Where NuPreSob=@NuPreSob),1)        
          
 INSERT INTO PRESUPUESTO_SOBRE_DET        
           ([NuDetPSo]        
           ,[NuPreSob]        
           ,[FeDetPSo]      
           ,[CoTipPSo]                   
           ,[IDServicio_Det]       
           ,IDOperacion_Det     
           ,[NuSincerado_Det]        
           ,[TxServicio]        
           ,[QtPax]               
           ,[SsPreUni]        
           ,[SsTotal]        
           ,[SsTotSus]        
           ,[SsTotDev]         
           ,CoPago      
           ,CoPrvPrg  
		   ,IDPax
           ,IDMoneda
		   ,IDServicio_Det_V_Cot
           ,[UserMod]        
           )        
     VALUES        
           (@NuDetPSo,        
           @NuPreSob,        
           @FeDetPSo,      
           @CoTipPSo,        
           Case When @IDServicio_Det=0 Then Null Else @IDServicio_Det End,     
           Case When @IDOperacion_Det=0 Then Null Else @IDOperacion_Det End,     
           Case When @NuSincerado_Det=0 Then Null Else @NuSincerado_Det End,        
           @TxServicio,         
           @QtPax,             
           @SsPreUni,        
           @SsTotal,         
           @SsTotSus,        
           @SsTotDev,           
           @CoPago,    
           Case When ltrim(rtrim(@CoPrvPrg))='' Then Null Else @CoPrvPrg End,
		   Case When @IDPax =0 Then Null Else @IDPax End, 
           Case When ltrim(rtrim(@IDMoneda))='' Then Null Else @IDMoneda End, 
		   Case When @IDServicio_Det_V_Cot=0 Then Null Else @IDServicio_Det_V_Cot End, 
           @UserMod)
