﻿--JRF-20150409-IsNull(Round(...))-opd.Total...
--JRF-20150411-..,IsNull(opd.TxDescripcion,isnull(rd...
--JRF-20150611-Adicionar la detraccion 
--JHD-20150706-case when dbo.FnCambioMoneda(o.SsMontoTotal,rd.IDMoneda,'SOL',isnull(o.TCambio,([dbo].[FnTipoDeCambioVtaUltimoxFecha](o.FechaEmision))))>=700 then IsNull(o.SSDetraccion,0) else 0 end As SSDetraccion
--JRF-20160401-Descontar la suma de lo ya pagado(-IsNull((select Sum(Total) from ORDENPAGO_DET opd2 Inner Join ORDENPAGO o2 On opd2.IDOrdPag...)
CREATE Procedure dbo.ORDENPAGO_DET_Sel_MailConfirmacion
 @IDOrdPag int
As          
 Set NoCount On          
 SELECT [IDOrdPag_Det]          
   ,opd.[IDOrdPag]          
   ,isnull(opd.[IDReserva_Det],0) as IDReserva_Det         
   ,isnull(opd.[IDServicio_Det] ,0) as IDServicio_Det    
   ,IsNull(opd.TxDescripcion,isnull(rd.Servicio,'')) as Servicio  
   ,opd.[NroPax]          
   ,opd.[NroLiberados]          
   ,isnull(opd.[Cantidad],0) as Cantidad  
   ,Isnull(opd.[Noches],0) Noches          
   ,isnull(rd.TotalGen,opd.Total) as TotalServicio,        
   mo.Simbolo as CoMoneda,     
    isnull(opd.Total,0) as Total,
	Case When o.IDEstado='PG' Then 0 Else
	IsNull( 
	Round(     
	 Case When o.IDMoneda=rd.IDMoneda Then    
	  rd.TotalGen    
	 Else    
	  dbo.FnCambioMoneda(rd.TotalGen,rd.IDMoneda,o.IDMoneda,o.TCambio)    
	 End,2),opd.Total)    
	 -opd.Total 
	 -IsNull((select Sum(Total) from ORDENPAGO_DET opd2 Inner Join ORDENPAGO o2 On opd2.IDOrdPag=o2.IDOrdPag
	   Where o2.IDReserva=rd.IDReserva And o2.IDMoneda=o.IDMoneda And o2.IDOrdPag<>o.IDOrdPag And o2.IDEstado='PG'),0)
	 End 
	 as SaldoxPagar,
	 --Total_Soles=dbo.FnCambioMoneda(o.SsMontoTotal,rd.IDMoneda,'SOL',o.TCambio) ,
	 --o.SsMontoTotal,
	case when dbo.FnCambioMoneda(o.SsMontoTotal,rd.IDMoneda,'SOL',isnull(o.TCambio,([dbo].[FnTipoDeCambioVtaUltimoxFecha](o.FechaEmision))))>=700 then IsNull(o.SSDetraccion,0) else 0 end As SSDetraccion
 FROM [dbo].[ORDENPAGO_DET] opd         
 Left Join RESERVAS_DET rd On opd.IDReserva_Det = rd.IDReserva_Det          
 Left Join ORDENPAGO o On opd.IDOrdPag=o.IDOrdPag        
 Left Join MAMONEDAS mo On o.IDMoneda = mo.IDMoneda
 WHERE opd.IDOrdPag = @IDOrdPag 
