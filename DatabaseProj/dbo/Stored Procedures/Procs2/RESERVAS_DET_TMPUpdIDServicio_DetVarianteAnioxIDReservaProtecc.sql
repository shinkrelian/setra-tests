﻿Create Procedure dbo.RESERVAS_DET_TMPUpdIDServicio_DetVarianteAnioxIDReservaProtecc
	--@IDReservaOrig int,
	@IDReservaProtecc int, 
	@UserMod char(4)
As
	Set Nocount On
	
	Declare @IDServicio_Det int, @Variante varchar(7), @Anio varchar(4),
	@IDServicio char(8)
	
	Select @IDServicio_Det=IDServicio_Det, @Variante=Tipo, @Anio=Anio, @IDServicio=IDServicio 
	From MASERVICIOS_DET 
	Where IDServicio_Det In (Select IDServicio_Det From RESERVAS_DET 
		Where IDReserva=@IDReservaProtecc)
	
	Update RESERVAS_DET_TMP Set IDServicio_Det=@IDServicio_Det, IDServicio=@IDServicio,
		UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva = @IDReservaProtecc 
	
