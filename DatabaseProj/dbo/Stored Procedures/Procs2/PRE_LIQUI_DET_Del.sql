﻿
Create Procedure dbo.PRE_LIQUI_DET_Del
@IDNotaVenta char(10),
@IDPreLiquiDet tinyint
As
	Set NoCount On
	delete from PRE_LIQUI_DET 
	Where IDNotaVenta = @IDNotaVenta And IDPreLiquiDet=@IDPreLiquiDet
