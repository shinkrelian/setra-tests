﻿Create Procedure dbo.MAVEHICULOS_Del
@NuVehiculo smallint
As
	Set NoCount On
	Delete from MAVEHICULOS 
	where NuVehiculo = @NuVehiculo
