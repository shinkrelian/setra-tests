﻿

--PPMG-20151105-Se agrego el eliminar MASERVICIOS_DET_DESC_APT
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_DelxIDServicio_Det]
	@IDServicio_Det int
AS
BEGIN
	Set NoCount On
	
	Delete From MASERVICIOS_DET_RANGO_APT 
	WHERE
           (IDServicio_Det=@IDServicio_Det Or 
           IDServicio_Det In 
           (Select IDServicio_Det From MASERVICIOS_DET
            Where IDServicio_Det_V=@IDServicio_Det ))

	IF NOT EXISTS(SELECT IDServicio_Det, IDTemporada From MASERVICIOS_DET_RANGO_APT 
				  WHERE (IDServicio_Det=@IDServicio_Det Or 
				  IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET Where IDServicio_Det_V=@IDServicio_Det))
				  GROUP BY IDServicio_Det, IDTemporada)
	BEGIN
		DELETE FROM MASERVICIOS_DET_DESC_APT
		WHERE (IDServicio_Det=@IDServicio_Det Or 
				  IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET Where IDServicio_Det_V=@IDServicio_Det))
	END
END
