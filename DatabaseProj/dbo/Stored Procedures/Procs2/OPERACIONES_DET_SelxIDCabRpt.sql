﻿--JRF-20130821-Solo los detalles que tienen Voucher.                        
--JRF-20130909-Considerar Los días de Solo Hoteles.                   
--JRF-20130912-Considerar los dias de cotizacion que no figuren en el detalle Operación                  
--JRF-20140414-Agregar el IDDet para reconocer los hijos para Operadores con Alojamiento (COTIDET)              
--JRF-20140519-El nro de Pax debe considerar los liberados.          
--JRF-20141216-Agregar el Idioma para las descripciones de Comida.  
--JRF-20150107-Considerar la fecha y hora de salida ingresada en transportes para los items del mismo tipo  
--JRF-20150223-[... and od.FlServicioNoShow=0] No Considerar los NoShow
--JRF-20150428-No considerar a los guías
--JRF-20150428-No se deben visualizar los servicios And ('CUZ00207','CUZ00208') de Setours Cusco
--JRF-20160404-Union.. (Detalles no relacionados)
--JRF-20160404-Agregar cod. de reservas
CREATE Procedure dbo.OPERACIONES_DET_SelxIDCabRpt                            
@IDCab int,    
@IDIdioma varchar(12)    
As                            
Set NoCount On    
                       
Delete from tmpIntervaloDias                  
Declare @Dias smalldatetime                  
Declare curDias cursor for                  
select distinct cast(convert(char(10),cd.Dia,103) as smalldatetime) as DiaCotiDet                  
from OPERACIONES_DET od Left Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det                  
Right Join COTIDET cd On rd.IDDet = cd.IDDET                  
where cd.IDCAB = @IDCab and rd.Anulado =0 and rd.IDDet Is Null                   
Open curDias                   
Fetch Next From curDias into @Dias                  
 While @@FETCH_STATUS = 0                  
 Begin                  
  Insert Into tmpIntervaloDias                  
  select od.Item as ItemCotiDetalle,CONVERT(char(10),@Dias,103) as CadenaDia,                  
   UPPER(od.DiaNombre) as NombreDia,                  
   CAST(DATEPART(day,@Dias) as varchar(3))+ ' '+UPPER(DATENAME(MM,@Dias)) +' '+ CAST(DATEPART(YEAR,@Dias) AS VARCHAR(4)) as MesAnio,                            
    UPPER(DATENAME(MM,@Dias)) As DiaNombreLengOrig,                        
   --case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then DATEADD(s,-1,DATEADD(d,1, @Dias)) else od.Dia End as DiaServicio,                          
   case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then CAST((CONVERT(char(10),od.Dia,103)+ ' 23:59:00') as smalldatetime) else od.Dia End as DiaServicio,      
   Case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then '' else CONVERT(char(5),od.Dia,108) End As                             
   Hora,u.Descripcion as Ciudad,                          
   case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then pr.NombreCorto+' - ' + CAST(od.Cantidad As varchar(5))+' '+od.Servicio                         
   else                         
   case when (pr.IDTipoProv = '002' Or (pr.IDTipoProv = '001' and sd.PlanAlimenticio = 1)) then pr.NombreCorto
   + IsNull(' ('+ r.CodReservaProv+') ' ,'')+' - ' +od.Servicio else od.Servicio End                        
   End as Servicio,                            
   Case When s.IDTipoServ = 'NAP' Then '-' Else s.IDTipoServ End                             
   + Case When idio.Siglas ='---' Or idio.Siglas ='O.T' then '' else ' ('+idio.Siglas+')' End As TipoServicio,                            
   CAST(od.NroPax as varchar(4)) +' Pax' As NroPax,case when od.IDVoucher = 0 then '-' else CAST(od.IDVoucher as varchar(7))End as Voucher  ,pr.IDTipoProv,sd.ConAlojamiento,              
   cd.IDDET,sd.PlanAlimenticio ,
   Case When c.NroPax+IsNull(c.NroLiberados,0) <>  od.NroPax+IsNull(od.NroLiberados,0) Then
   dbo.FnDevuelveListaPaxxIDDOperacionDet(od.IDOperacion_Det) Else '' End as NombresPax
                    
  from OPERACIONES_DET od Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion                  
  Left Join MAPROVEEDORES pr On o.IDProveedor = pr.IDProveedor                  
  Left Join MASERVICIOS_DET sd On sd.IDServicio_Det = od.IDServicio_Det                  
  Left Join MAUBIGEO u On od.IDubigeo = u.IDubigeo                  
  Left Join MASERVICIOS s oN od.IDServicio = s.IDServicio                  
  Left Join MAIDIOMAS idio On od.IDIdioma = idio.IDidioma                  
  Inner Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det              
  Inner Join COTIDET cd On rd.IDDet = cd.IDDET              
  Left Join COTICAB c On o.IDCab=c.IDCab
  Left Join RESERVAS r On rd.IDReserva=r.IDReserva
  where o.IDCab = @IDCab and od.FlServicioNoShow=0
  And Not cd.IDServicio In ('CUZ00207','CUZ00208')
  And cd.IncGuia=0 --And CHARINDEX('guide',Lower(cd.Servicio))=0
  And (pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0) and od.IDVoucher <> 0                  
  And CONVERT(char(10),od.dia,103) <> CONVERT(char(10),@Dias,103) and                  
  @Dias between od.Dia and dateadd(d,-1,od.FechaOut)                  
                    
  Fetch Next From curDias into @Dias                  
 End                  
Close curDias                  
DealLocate curDias                  
                          
 SELECT X.ItemCotiDetalle,X.CadenaDia,X.NombreDia,X.MesAnio,X.DiaNombreLengOrig,X.DiaServicio,    
 X.Hora,X.Ciudad,X.Servicio+Case When X.IDTipoProv = '001' and X.PlanAlimenticio=0 Then char(13)+dbo.FnDevuelveCadenaHotelServiciosAdicionales(X.IDDET,@IDIdioma) else '' End as Servicio    
 ,Isnull(X.TipoServicio,'-') as TipoServicio,X.NroPax,X.Voucher,                
 Isnull(X.IDTipoProv,'001') as IDTipoProv,ConAlojamiento,IDDET ,X.PlanAlimenticio,X.NombresPax
 FROM (                        
 SELECT                             
 (select Item from COTIDET cd where cd.IDDET = (select IDDET from RESERVAS_DET rd where rd.IDReserva_Det = od.IDReserva_Det)) as ItemCotiDetalle,                            
 CONVERT(char(10),od.Dia,103) as CadenaDia,                        
 UPPER(od.DiaNombre) As NombreDia , CAST(DATEPART(day,od.Dia) as varchar(3))+ ' '+UPPER(DATENAME(MM,od.Dia)) +' '+ CAST(DATEPART(YEAR,od.Dia) AS VARCHAR(4)) as MesAnio,                            
 UPPER(DATENAME(MM,od.Dia)) As DiaNombreLengOrig,                        
 --case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then DATEADD(s,-1,DATEADD(d,1, od.Dia)) else od.Dia End      
   
 Case When exists(select cv.ID from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET) Then  
(select Top(1) Cast(convert(char(10),cv.Fec_Salida,103)+' '+SUBSTRING(IsNull(cv.Salida,'00:00'),1,5) as smalldatetime)  
  from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET)  
 else  
 case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then CAST((CONVERT(char(10),od.Dia,103)+ ' 23:59:00') as smalldatetime) else od.Dia End      
 End as DiaServicio,  
        
 Case When exists(select cv.ID from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET) Then  
(select Top(1) SUBSTRING(IsNull(cv.Salida,'00:00'),1,5)  
  from COTIVUELOS cv where IDCAB=cd.IDCAB and cv.IdDet=cd.IDDET)  
 else                   
 Case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then '' else CONVERT(char(5),od.Dia,108) End End As Hora,  
 u.Descripcion as Ciudad,                                                   
 case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then  pr.NombreCorto + IsNull(' ('+ r.CodReservaProv+') ' ,'') +' - ' + CAST(od.Cantidad As varchar(5))+' '+od.Servicio                         
 else                         
 case when (pr.IDTipoProv = '002' Or (pr.IDTipoProv = '001' and sd.PlanAlimenticio = 1)) then
  pr.NombreCorto + IsNull(' ('+ r.CodReservaProv+') ' ,'') +' - ' +od.Servicio else od.Servicio End                        
 End as                          
 Servicio,                            
 Case When s.IDTipoServ = 'NAP' Then '-' Else s.IDTipoServ End                             
  + Case When idio.Siglas ='---' Or idio.Siglas ='O.T' then '' else ' ('+idio.Siglas+')' End As TipoServicio,                            
 CAST((od.NroPax + ISNULL(od.NroLiberados,0)) as varchar(4)) +' Pax' As NroPax,case when od.IDVoucher = 0 then '-' else CAST(od.IDVoucher as varchar(7))End as Voucher  ,pr.IDTipoProv,sd.ConAlojamiento,              
 cd.IDDET  ,sd.PlanAlimenticio ,
  Case When c.NroPax+IsNull(c.NroLiberados,0) <>  od.NroPax+IsNull(od.NroLiberados,0) Then
   dbo.FnDevuelveListaPaxxIDDOperacionDet(od.IDOperacion_Det) Else '' End as NombresPax            
                                
 FROM OPERACIONES_DET od Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion                            
 Left Join MAUBIGEO u On od.IDubigeo= u.IDubigeo Left Join MASERVICIOS s On od.IDServicio = s.IDServicio                            
 Left Join MAPROVEEDORES pr On s.IDProveedor = pr.IDProveedor      
 Left Join MAIDIOMAS idio On od.IDIdioma  = idio.IDidioma --Left Join VOUCHER_OPERACIONES vo On od.IDVoucher = vo.IDVoucher
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det
 Left Join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det              
 Inner Join COTIDET cd On rd.IDDet = cd.IDDET              
 Left Join COTICAB c On o.IDCab = c.IDCAB
 Left Join RESERVAS r On rd.IDReserva=r.IDReserva
 where  o.IDCab = @IDCab  and IsNull(od.FlServicioNoShow,0)=0 
 And Not cd.IDServicio In ('CUZ00207','CUZ00208')
 And IsNull(cd.IncGuia,0)=0 --And CHARINDEX('guide',Lower(cd.Servicio))=0)               
 Union                    
 select * from tmpIntervaloDias           
 Union
 SELECT                    
 0 as ItemCotiDetalle,                            
 CONVERT(char(10),od.Dia,103) as CadenaDia,                        
 UPPER(od.DiaNombre) As NombreDia , CAST(DATEPART(day,od.Dia) as varchar(3))+ ' '+UPPER(DATENAME(MM,od.Dia)) +' '+ CAST(DATEPART(YEAR,od.Dia) AS VARCHAR(4)) as MesAnio,                            
 UPPER(DATENAME(MM,od.Dia)) As DiaNombreLengOrig,                        
 --case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then DATEADD(s,-1,DATEADD(d,1, od.Dia)) else od.Dia End      
   
 Case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then CAST((CONVERT(char(10),od.Dia,103)+ ' 23:59:00') as smalldatetime) else od.Dia
 End as DiaServicio,  
 Case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then '' else CONVERT(char(5),od.Dia,108) End As Hora,  
 u.Descripcion as Ciudad,                                                   
 case when pr.IDTipoProv = '001' and sd.PlanAlimenticio = 0 then pr.NombreCorto+' - ' + CAST(od.Cantidad As varchar(5))+' '+od.Servicio                         
 else                         
 case when (pr.IDTipoProv = '002' Or (pr.IDTipoProv = '001' and sd.PlanAlimenticio = 1)) then pr.NombreCorto +' - ' +od.Servicio else od.Servicio End                        
 End as                          
 Servicio,                            
 Case When s.IDTipoServ = 'NAP' Then '-' Else s.IDTipoServ End                             
  + Case When idio.Siglas ='---' Or idio.Siglas ='O.T' then '' else ' ('+idio.Siglas+')' End As TipoServicio,                            
 CAST((od.NroPax + ISNULL(od.NroLiberados,0)) as varchar(4)) +' Pax' As NroPax,case when od.IDVoucher = 0 then '-' else CAST(od.IDVoucher as varchar(7))End as Voucher  ,pr.IDTipoProv,sd.ConAlojamiento,              
 0 As IDDet  ,sd.PlanAlimenticio ,
  Case When c.NroPax+IsNull(c.NroLiberados,0) <>  od.NroPax+IsNull(od.NroLiberados,0) Then
   dbo.FnDevuelveListaPaxxIDDOperacionDet(od.IDOperacion_Det) Else '' End as NombresPax            
                                
 FROM OPERACIONES_DET od Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion                            
 Left Join MAUBIGEO u On od.IDubigeo= u.IDubigeo Left Join MASERVICIOS s On od.IDServicio = s.IDServicio                            
 Left Join MAPROVEEDORES pr On s.IDProveedor = pr.IDProveedor      
 Left Join MAIDIOMAS idio On od.IDIdioma  = idio.IDidioma --Left Join VOUCHER_OPERACIONES vo On od.IDVoucher = vo.IDVoucher
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det
 Left Join COTICAB c On o.IDCab = c.IDCAB
 Where IsNull(od.IDReserva_Det,0)=0 And o.IDCab=@IDCab       
 ) AS X                        
 Where X.Voucher <> 0                        
 Order By DiaServicio,ItemCotiDetalle                           
