﻿
Create Procedure dbo.RESERVAS_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM RESERVAS 
	WHERE IDReserva IN (SELECT IDReserva FROM RESERVAS WHERE IDCab = @IDCab)
	
