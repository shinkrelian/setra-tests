﻿--HLF-20150408-,ValorVentaOld-ValorVenta as ValorVentaDif
--,(ValorVentaOld*(PoConcretVtaOld/100))-(ValorVenta*(PoConcretVta/100)) as ValorProyDif
--(Pax<>PaxOld Or (Estado<>EstadoOld And (EstadoOld='A' And Estado<>'P')) Or PoConcretVta<>PoConcretVtaOld Or ValorVenta<>ValorVentaOld)
--Comentando And (EstadoOld='A' And Estado='A')
--HLF-20150415-Case When ccc.Estado In('E','X') Then 0 Else dbo.FnValorVentaFile(CC.IDCAB) End as ValorVenta
--Case When Estado In('E','X') Then dbo.FnValorVentaFile(x.IDCAB) Else ValorVenta End ) as ValorVentaOld
--HLF-20150416-,ValorVenta-ValorVentaOld as ValorVentaDif
--,(ValorVenta*(PoConcretVta/100))-(ValorVentaOld*(PoConcretVtaOld/100)) as ValorProyDif
--HLF-20150513-Order by FechaOut
--Select * From #FILESSEMANAMES2 Where Estado='X' Order by FechaOut
--Select * From #FILESSEMANAMES2 Where Estado<>'X' Order by FechaOut
--HLF-20150514-And ccc.FechaOut>=@FecFin
--			(@Anio,'2',@Ene2 ,@Feb2 ,@Mar2 ,@Abr2 ,@May2 ,@Jun2 ,@Jul2 ,@Ago2 ,@Sep2 ,@Oct2 ,@Nov2 ,@Dic2 , @Total),
--			(@Anio,'3',@Ene3 ,@Feb3 ,@Mar3 ,@Abr3 ,@May3 ,@Jun3 ,@Jul3 ,@Ago3 ,@Sep3 ,@Oct3 ,@Nov3 ,@Dic3 , @Total)
--HLF-20150601-Select * From #FILESSEMANAMES2 Where Estado IN('X','E') Order by FechaOut
--Select * From #FILESSEMANAMES2 Where Estado NOT IN('X','E') Order by FechaOut
--HLF-20160225-dbo.FnMargenFile(cc.IDCAB) as MargenGan, MargenNominal, us.Usuario,
--HLF-20160307-And CONVERT(varchar,@FecFin,103)='01/01/1900'
--Set DateFormat dmy	SET DATEFIRST 1
--HLF-20160428-and Estado in('X','E')	and not Estado in('X','E') para todos los meses en curFILESSEMANAMES (antes Estado in('X')	and not Estado in('X'))
CREATE Procedure [dbo].[ReporteWeeklyNetSales]
	@FecIni smalldatetime,
	@FecFin smalldatetime,
	@SemanaMes char(1),
	@NroQuery char(1)
As
	Set Nocount On
	Set DateFormat dmy
	SET DATEFIRST 1

	If @SemanaMes='W' And CONVERT(varchar,@FecFin,103)='01/01/1900'
		Begin		
		Declare @FecSemanaPasadaHoy smalldatetime = dateadd(WEEK,-1, @FecIni)
		Declare @NroSemanaHoy tinyint = Datepart(dw, @FecSemanaPasadaHoy)
		Set @FecIni=dateadd(DAY,(@NroSemanaHoy-1)*(-1), @FecSemanaPasadaHoy)
		Set @FecFin=dateadd(DAY,6-(@NroSemanaHoy), @FecSemanaPasadaHoy)
		End

	If @SemanaMes='M' And CONVERT(varchar,@FecFin,103)='01/01/1900'
		Begin		
		Declare @FecMesPasadaHoy smalldatetime = dateadd(MONTH,-1, @FecIni)		
		Set @FecIni=dateadd(DAY,(day(@FecMesPasadaHoy)-1)*(-1), @FecMesPasadaHoy)
		Set @FecFin=dateadd(day,-1,dateadd(MONTH,1,@FecIni))		
		End

	--Select @FecIni, @FecFin

	If @NroQuery='1' or @NroQuery='3'
		Begin
		SELECT *,ValorVenta*(MargenGan/100) as MargenNominal,
			ValorVenta*(PoConcretVta/100) as ValorProyectado 
		INTO #FILESSEMANAMES
		FROM
		(
		Select --cc.idcab,
		cc.IDFile, cc.Fecha ,cc.FecInicio, cc.FechaOut, c.RazonComercial as DescCliente, up.Descripcion as DescPais, cc.Titulo, 
		--cc.NroPax + isnull(cc.NroLiberados,0) as Pax, 
		cc.NroPax as Pax, 
		--cc.Estado, 
		us.Usuario as Executive,
		isnull(cc.PoConcretVta,0) as PoConcretVta,
			dbo.FnValorVentaFile(cc.IDCAB) as ValorVenta, dbo.FnMargenFile(cc.IDCAB) as MargenGan
		From COTICAB cc Left Join MACLIENTES c On cc.IDCliente=c.IDCliente
			Left Join MAUBIGEO up On c.IDCiudad=up.IDubigeo
			Left Join MAUSUARIOS us On cc.IDUsuario=us.IDUsuario
		Where 
		CONVERT(smalldatetime ,CONVERT(varchar, cc.Fecha ,103)) Between @FecIni And 
			DATEADD(S,-1, DATEADD(d,1,CONVERT(varchar,@FecFin,103)) )
			--@FecFin + ' 23:59:59'
			And cc.Estado='A'
		) as X
		Order by FechaOut

		
		SELECT * FROM #FILESSEMANAMES
		End
	If @NroQuery='2' or @NroQuery='3'
		Begin
		SELECT IDFile, Fecha, FecInicio, FechaOut,DescCliente, DescPais, Titulo, Pax, Estado, PoConcretVta, 
		ValorVenta, 
			ValorVenta*(PoConcretVta/100) as ValorProyectado,
			PaxOld, EstadoOld, PoConcretVtaOld, ValorVentaOld
			,ValorVentaOld*(PoConcretVtaOld/100) as ValorProyectadoOld
			--,ValorVentaOld-ValorVenta as ValorVentaDif
			,ValorVenta-ValorVentaOld as ValorVentaDif
			--,(ValorVentaOld*(PoConcretVtaOld/100))-(ValorVenta*(PoConcretVta/100)) as ValorProyDif
			,(ValorVenta*(PoConcretVta/100))-(ValorVentaOld*(PoConcretVtaOld/100)) as ValorProyDif,
			Executive, MargenGan, ValorVentaOld*(MargenGan/100) as MargenNominal
		INTO #FILESSEMANAMES2
		FROM
		(
		SELECT distinct --x.idcab,
		xcc.IDFile,		
		xcc.Fecha, xcc.FecInicio, xcc.FechaOut,
		DescCliente, DescPais, x.Titulo,

		XCC.NroPax as Pax, XCC.Estado, isnull(XCC.PoConcretVta,0) as PoConcretVta,
			--dbo.FnValorVentaFile(XCC.IDCAB) as ValorVenta,
		
			ValorVenta,

		isnull((Select Top 1 txvalorantes From COTICAB_HISTORIAL_UPD Where IDCab=x.IDCab And NoCampo='NroPax' 
		And CONVERT(smalldatetime ,CONVERT(varchar, FecMod ,103)) Between @FecIni And 
		DATEADD(S,-1, DATEADD(d,1,CONVERT(varchar,@FecFin,103)) )
		order by FecMod desc),XCC.NroPax) as PaxOld,

		isnull((Select Top 1 txvalorantes From COTICAB_HISTORIAL_UPD Where IDCab=x.IDCab And NoCampo='Estado' 
			And CONVERT(smalldatetime ,CONVERT(varchar, FecMod ,103)) Between @FecIni And 
			DATEADD(S,-1, DATEADD(d,1,CONVERT(varchar,@FecFin,103)) )
			order by FecMod desc),XCC.Estado) as EstadoOld,

		isnull(cast( (Select Top 1 txvalorantes From COTICAB_HISTORIAL_UPD Where IDCab=x.IDCab And NoCampo='PoConcretVta' 
			And CONVERT(smalldatetime ,CONVERT(varchar, FecMod ,103)) Between @FecIni And 
			DATEADD(S,-1, DATEADD(d,1,CONVERT(varchar,@FecFin,103)) )
			order by FecMod desc ) as numeric(5,2)),XCC.PoConcretVta) as PoConcretVtaOld,

		isnull(cast( (Select Top 1 txvalorantes From COTICAB_HISTORIAL_UPD Where IDCab=x.IDCab And NoCampo='Total' 
			And CONVERT(smalldatetime ,CONVERT(varchar, FecMod ,103)) Between @FecIni And 
			DATEADD(S,-1, DATEADD(d,1,CONVERT(varchar,@FecFin,103)) )
			order by FecMod desc) as numeric(13,2)),
			--ValorVenta) as ValorVentaOld
			Case When Estado In('E','X') Then dbo.FnValorVentaFile(x.IDCAB) Else ValorVenta End ) as ValorVentaOld
			,Executive, MargenGan
		FROM
		(
		Select cc.IDCab,--cc.IDFile, 
		--ccc.Fecha ,
		--ccc.FecInicio, cc.FechaOut, 
		c.RazonComercial as DescCliente, up.Descripcion as DescPais, ccc.Titulo, 
		--dbo.FnValorVentaFile(CC.IDCAB) as ValorVenta
		Case When ccc.Estado In('E','X') Then 0 Else dbo.FnValorVentaFile(CC.IDCAB) End as ValorVenta,us.Usuario as Executive,
		dbo.FnMargenFile(cc.IDCAB) as MargenGan
		From COTICAB_HISTORIAL_PVW cc Left Join COTICAB ccc On cc.IDCab=ccc.IDCAB
		Left Join MACLIENTES c On ccc.IDCliente=c.IDCliente
			Left Join MAUBIGEO up On c.IDCiudad=up.IDubigeo
			Left Join MAUSUARIOS us On ccc.IDUsuario=us.IDUsuario
		Where 
		CONVERT(smalldatetime ,CONVERT(varchar, cc.FecMod ,103)) Between @FecIni And 
			DATEADD(S,-1, DATEADD(d,1,CONVERT(varchar,@FecFin,103)) )
			--@FecFin + ' 23:59:59'
			And ccc.FechaOut>=@FecFin
			And cc.CoAccion='U'	and ccc.Estado <> 'P'


		) as X Left Join COTICAB XCC On x.IDCab=xcc.IDCAB
		) AS Y 
		WHERE		
		--(Pax<>PaxOld Or Estado<>EstadoOld Or PoConcretVta<>PoConcretVtaOld Or ValorVenta<>ValorVentaOld)
		  (Pax<>PaxOld Or Estado<>EstadoOld Or PoConcretVta<>PoConcretVtaOld Or ValorVenta<>ValorVentaOld)
			And (EstadoOld='A')-- And Estado<>'A')
			--And (EstadoOld='P' And Estado='A')
			
		--Select * From #FILESSEMANAMES2 Order by FechaOut
		PRINT 'CANCELADOS 1'
		--Select * From #FILESSEMANAMES2 Where Estado='X' Order by FechaOut
		Select * From #FILESSEMANAMES2 Where Estado IN('X','E') Order by FechaOut
		PRINT 'MODIFICACIONES'
		--Select * From #FILESSEMANAMES2 Where Estado<>'X' Order by FechaOut		
		Select * From #FILESSEMANAMES2 Where Estado NOT IN('X','E') Order by FechaOut		

		End
	If @NroQuery='3'
		Begin
		Declare @Anio char(4),@Ene numeric(13,0)=0,@Feb numeric(13,0)=0,@Mar numeric(13,0)=0,@Abr numeric(13,0)=0,
			@May numeric(13,0)=0,@Jun numeric(13,0)=0,@Jul numeric(13,0)=0,@Ago numeric(13,0)=0,
			@Sep numeric(13,0)=0,@Oct numeric(13,0)=0,@Nov numeric(13,0)=0,@Dic numeric(13,0)=0, @Total numeric(14,2)=0,
			@Ene2 numeric(13,0)=0,@Feb2 numeric(13,0)=0,@Mar2 numeric(13,0)=0,@Abr2 numeric(13,0)=0,
			@May2 numeric(13,0)=0,@Jun2 numeric(13,0)=0,@Jul2 numeric(13,0)=0,@Ago2 numeric(13,0)=0,
			@Sep2 numeric(13,0)=0,@Oct2 numeric(13,0)=0,@Nov2 numeric(13,0)=0,@Dic2 numeric(13,0)=0, 

			@Ene3 numeric(13,0)=0,@Feb3 numeric(13,0)=0,@Mar3 numeric(13,0)=0,@Abr3 numeric(13,0)=0,
			@May3 numeric(13,0)=0,@Jun3 numeric(13,0)=0,@Jul3 numeric(13,0)=0,@Ago3 numeric(13,0)=0,
			@Sep3 numeric(13,0)=0,@Oct3 numeric(13,0)=0,@Nov3 numeric(13,0)=0,@Dic3 numeric(13,0)=0
			
		Create Table #FILESSEMANAMESxANIO(
			Anio char(4), Tipo char(1),
			Ene numeric(13,0),Feb numeric(13,0),Mar numeric(13,0),Abr numeric(13,0),
			May numeric(13,0),Jun numeric(13,0),Jul numeric(13,0),Ago numeric(13,0),
			Sep numeric(13,0),Oct numeric(13,0),Nov numeric(13,0),Dic numeric(13,0), 
			Total numeric(14,2))

		Declare curFILESSEMANAMES cursor for Select distinct year(FechaOut) From #FILESSEMANAMES
		Open curFILESSEMANAMES
		Fetch Next From curFILESSEMANAMES Into @Anio
		While (@@FETCH_STATUS=0)
			Begin
			Set @Ene =0
			Set @Feb =0
			Set @Mar =0
			Set @Abr =0
			Set @May =0
			Set @Jun =0
			Set @Jul =0
			Set @Ago =0
			Set @Sep =0
			Set @Oct =0
			Set @Nov =0
			Set @Dic =0
			Set @Ene2 =0
			Set @Feb2 =0
			Set @Mar2 =0
			Set @Abr2 =0
			Set @May2 =0
			Set @Jun2 =0
			Set @Jul2 =0
			Set @Ago2 =0
			Set @Sep2 =0
			Set @Oct2 =0
			Set @Nov2 =0
			Set @Dic2 =0
			Set @Ene3 =0
			Set @Feb3 =0
			Set @Mar3 =0
			Set @Abr3 =0
			Set @May3 =0
			Set @Jun3 =0
			Set @Jul3 =0
			Set @Ago3 =0
			Set @Sep3 =0
			Set @Oct3 =0
			Set @Nov3 =0
			Set @Dic3 =0
			Set @Total=0

			Set @Ene=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'01'),0)
			Set @Ene2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'01' and Estado in('X','E')),0)
			Set @Ene3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'01' and not Estado in('X','E')
				--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'01')),0)
			),0)
			Set @Feb=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'02'),0)			
			Set @Feb2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'02' and Estado in('X','E')),0)
			Set @Feb3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'02' and not Estado in('X','E')
				--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'02')),0)
				),0)
			
			Set @Mar=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'03'),0)
			Set @Mar2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'03' and Estado in('X','E')),0)
			Set @Mar3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'03' and not Estado in('X','E')
				--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'03')),0)
				),0)
			Set @Abr=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'04'),0)
			Set @Abr2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'04' and Estado in('X','E')),0)
			Set @Abr3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'04' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'04')),0)
			Set @May=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'05'),0)
			Set @May2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'05' and Estado in('X','E')),0)
			Set @May3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'05' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'05')),0)
			Set @Jun=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'06'),0)
			Set @Jun2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'06' and Estado in('X','E')),0)
			Set @Jun3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'06' and not Estado in('X','E')
			),0)--	And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'06')),0)
			Set @Jul=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'07'),0)
			Set @Jul2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'07' and Estado in('X','E')),0)
			Set @Jul3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'07' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'07')),0)
			Set @Ago=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'08'),0)
			Set @Ago2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'08' and Estado in('X','E')),0)
			Set @Ago3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'08' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'08')),0)
			Set @Sep=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'09'),0)
			Set @Sep2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'09' and Estado in('X','E')),0)
			Set @Sep3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'09' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'09')),0)
			Set @Oct=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'10'),0)
			Set @Oct2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'10' and Estado in('X','E')),0)
			Set @Oct3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'10' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'10')),0)
			Set @Nov=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'11'),0)
			Set @Nov2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'11' and Estado in('X','E')),0)
			Set @Nov3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'11' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'11')),0)
			Set @Dic=isnull((Select isnull(sum(ValorProyectado),0) From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'12'),0)
			Set @Dic2=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'12' and Estado in('X','E')),0)
			Set @Dic3=isnull((Select isnull(sum(ValorProyDif),0) From #FILESSEMANAMES2 Where CONVERT(varchar(6),fechaout,112)=@Anio+'12' and not Estado in('X','E')
				),0)--And IDFile In (Select IDFile From #FILESSEMANAMES Where CONVERT(varchar(6),fechaout,112)=@Anio+'12')),0)

			Insert Into #FILESSEMANAMESxANIO(Anio,Tipo, Ene ,Feb ,Mar ,Abr ,May ,Jun ,Jul ,Ago ,Sep ,Oct ,Nov ,Dic , Total)
			Values(@Anio,'1',@Ene ,@Feb ,@Mar ,@Abr ,@May ,@Jun ,@Jul ,@Ago ,@Sep ,@Oct ,@Nov ,@Dic , @Total),
			(@Anio,'2',@Ene2 ,@Feb2 ,@Mar2 ,@Abr2 ,@May2 ,@Jun2 ,@Jul2 ,@Ago2 ,@Sep2 ,@Oct2 ,@Nov2 ,@Dic2 , @Total),
			(@Anio,'3',@Ene3 ,@Feb3 ,@Mar3 ,@Abr3 ,@May3 ,@Jun3 ,@Jul3 ,@Ago3 ,@Sep3 ,@Oct3 ,@Nov3 ,@Dic3 , @Total),
			(@Anio,'4',@Ene+@Ene2+@Ene3 ,@Feb+@Feb2+@Feb3 ,@Mar+@Mar2+@Mar3 ,@Abr+@Abr2+@Abr3 ,@May+@May2+@May3 ,@Jun+@Jun2+@Jun3 ,@Jul+@Jul2+@Jul3 ,@Ago+@Ago2+@Ago3 ,@Sep+@Sep2+@Sep3 ,@Oct+@Oct2+@Oct3 ,@Nov+@Nov2+@Nov3 ,@Dic+@Dic2+@Dic3 , @Total)

			Fetch Next From curFILESSEMANAMES Into @Anio

			End
		Close curFILESSEMANAMES
		Deallocate curFILESSEMANAMES

		UPDATE #FILESSEMANAMESxANIO SET Total=Ene+Feb+MAR+Abr+MAY+Jun+JUL+AGO+SEP+OCT+NOV+DIC

		Select * From #FILESSEMANAMESxANIO order by cast(Anio as smallint),cast(tipo as tinyint)

		Drop Table #FILESSEMANAMESxANIO
		End
		

	If exists(select name from sysobjects where name='#FILESSEMANAMES') DROP TABLE #FILESSEMANAMES
	If exists(select name from sysobjects where name='#FILESSEMANAMES2') DROP TABLE #FILESSEMANAMES2

