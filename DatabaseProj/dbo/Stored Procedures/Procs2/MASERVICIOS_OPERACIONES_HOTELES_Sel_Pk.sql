﻿
--------------------------------------------------------------


CREATE PROCEDURE [dbo].[MASERVICIOS_OPERACIONES_HOTELES_Sel_Pk]
	@CoServicio char(8)=''
AS
BEGIN
	Select
		CoServicio,
 		FlRecepcion,
 		FlEstacionamiento,
 		FlBoxDesayuno,
 		FlRestaurantes,
 		FlRestaurantesSoloDesayuno,
 		QtRestCantidad=ISNULL(QtRestCantidad,0),
 		QtRestCapacidad=ISNULL(QtRestCapacidad,0),
 		QtTriples=ISNULL(QtTriples,0),
 		QtNumHabitaciones=ISNULL(QtNumHabitaciones,0),
 		CoTipoCama,
 		TxObservacion=ISNULL(TxObservacion,'')
 	From dbo.MASERVICIOS_OPERACIONES_HOTELES
	Where 
		CoServicio = @CoServicio
END
