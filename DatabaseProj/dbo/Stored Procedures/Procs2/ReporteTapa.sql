﻿
--JRF-20120905-Alteracion de Filtros:Se agrego un rango de fechas para el dia del servicio. Campo(DiaServicio[Dia - OPERACION_DET])                                             
--JRF-20130802-Se debe imprimir los usuarios de las respectivas areas.                                                                
--JRF-20130802-Obtener la información desde reservas(Antes era de Operaciones[Operaciones_Det])                                                              
--JRF-20130805-Agregar el acomodo por Hotel.                                                              
--JRF-20130808-Agregar el campo Tipo de servicio, Orden para mostra Hoteles.                                                            
--HLF-20130814-Agregando al subquery DescProveeHotel                                                          
--JRF-20130823-Agregando días Solo Hotel.                                                   
--JRF-20130828-Agregando el Ubigeo de Arequipa y Colca.                                                 
--JRF-20131016-Agregando Filtro para controlar los Gastos de Transferencia.                                      
--JRF-20131029-Considerar solo Hoteles netos.(Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det) 3er Union                                  
--JRF-20131031-No considerar Guias en la descripcion de los Hoteles.                                
--JRF-20140317-Si debe filtrar anulado a nivel de la reserva(Tabla RESERVAS).              
--JRF-20140404-Se condiciona en la ultima Union que el dia no exista en servicio no hoteles.                  
--HLF-20140902-and r.IDProveedor=cd.IDProveedor , And cd2.IDProveedor=pr.IDProveedor          
--JRF-20141002-(Segunda Union )cd.IDCAB=@IDCab and ...      
--JRF-20141222-Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor [3er SuQuery]    
--JRF-20150223-No Considerar el NoShow de Reservas_Det
--JHD-20150411-Se cambio la tabla MAVEHICULOS por MAVEHICULOS_TRANSPORTE
--JHD-20150506-@CoPais char(6)=''
--JHD-20150506-and (u.IDPais=@CoPais Or ltrim(rtrim(@CoPais))='')  
--JRF-20150630-1er Query > No debe mostrar los Oper. Con Alojamiento - 3er Query > Ajuste en el ROW_NUMBER .. Partition By Idse..
--HLF-20150720-If Exists(SELECT sd.CoMoneda FROM COTIDET cd inner join MASERVICIOS_DET sd on ...
--@SSTipCam as TipoCambio, 
--JRF-20150303-(... rd.IDDet = cd.IDDetRel Or cd.IDDET=rd.IDDet) 2ndo Script.  
--HLF-20150918-Left Join MASERVICIOS_DET sd2 On cd.IDServicio_Det = sd2.IDServicio_Det                                             
--and (p.IDTipoProv ='003' and sd2.ConAlojamiento=1)
--JRF-20151129-Agregar ... And IsNull(rd.NuViaticoTC,0)=0
--JRF-20160411- Agregar los Trasladistas...
--JRF-20160411- ...Case When Exists(select distinct Dia from RESERVAS_DET rd Where rd.IDDet=cd.IDDet And rd.Anulado=0) and IsNull(cd.IDDetRel,..
--JRF-20160415- ..And (@ConGastosTransferencias = 1 Or CHARIN...
--PPMG-20160429-Se aumento esta condición r.Estado <> 'XL' and 
CREATE Procedure [dbo].[ReporteTapa]                                                           
 @IDCab int,
 @IDUbigeo char(6),
 @RangoFecIni smalldatetime,
 @RangoFecFin smalldatetime,
 @ConGastosTransferencias bit,
 @CoPais char(6)
AS
BEGIN
 Set Nocount On            
 
 Declare @SSTipCam as decimal(6,3)=0
 If Exists(SELECT sd.CoMoneda FROM COTIDET cd inner join MASERVICIOS_DET sd on 
	cd.IDServicio_Det=sd.IDServicio_Det WHERE cd.IDCAB=@IDCab And sd.CoMoneda='SOL')
	Select @SSTipCam = SSTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda='SOL'

 Else
	If Exists(SELECT sd.CoMoneda FROM COTIDET cd inner join MASERVICIOS_DET sd on 
	cd.IDServicio_Det=sd.IDServicio_Det WHERE cd.IDCAB=@IDCab And sd.CoMoneda='ARS')	
		Select @SSTipCam = SSTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda='ARS'
	Else
		If Exists(SELECT sd.CoMoneda FROM COTIDET cd inner join MASERVICIOS_DET sd on 
			cd.IDServicio_Det=sd.IDServicio_Det WHERE cd.IDCAB=@IDCab And sd.CoMoneda='CLP')			
				Select @SSTipCam = SSTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda='CLP'
                                          
 SELECT distinct * FROM (  
      
 Select                             
 Z.Ord,Z.DesCliente, Z.IDFile, Z.Cotizacion, Z.Titulo, Z.NroPax, Z.NroLiberados, Z.Nacionalidad, Z.DescModoServ,                                                          
 Z.FecInicio, Z.FechaOut, Z.Fecha, Z.Nombre, Z.IDIdioma,                                                            
 Z.Acomodacion,Z.TipoCambio,cast(Z.Observaciones as varchar(max)) as Observaciones,Z.UsuarioReserva,Z.UsuarioOperaciones,Z.Desayuno,                                                            
 Case when LEN(Z.DescProveeHotel)-2 < 0 then '' else  Left(Z.DescProveeHotel,LEN(Z.DescProveeHotel)-2) End As DescProveeHotel,                                                          
 Case when len(Z.DescProveeHotel)=0 then '' else RIGHT(Z.DescProveeHotel,2) End as Estado,         
 Z.DescUbigeo,Z.DiaCompleto,Z.DiaServicio,Z.DiaFormatFecha,Z.Hora,Z.Servicio,Z.TipoServ,Z.DescProvee,          
Z.GuiaTransPrg --,Z.NroOrden3                                                         
                                                            
 from (                                        
 SELECT Y.Ord,
 Y.DesCliente, Y.IDFile, Y.Cotizacion, Y.Titulo, Y.NroPax, Y.NroLiberados, Y.Nacionalidad, Y.DescModoServ,                                                                      
 Y.FecInicio, Y.FechaOut, Y.Fecha, Y.Nombre, Y.IDIdioma,                                                             
                                                                       
 Case When Y.Simple>0 Then Cast(Y.Simple AS varchar(3))+' SIMPLE ' Else '' End+                                                                 
 Case When Y.Twin>0 Then Cast(Y.Twin AS varchar(3))+' TWIN ' Else '' End+                                      
 Case When Y.Matrimonial>0 Then Cast(Y.Matrimonial AS varchar(3))+' MATRIMONIAL ' Else'' End+                                                                      
 Case When Y.Triple>0 Then Cast(Y.Triple AS varchar(3))+' TRIPLE ' Else '' End As Acomodacion,                                                                      
                                                        
                                                                       
 Y.TipoCambio, Y.Observaciones, Y.UsuarioReserva ,Y.UsuarioOperaciones,                                                                
 --Y.DiaFormat,                                                     
 Y.Sigla as Desayuno,                               
                                                                    
 case when Y.DescProveeHotel is null   --Left(Y.DescProveeHotel,LEN(Y.DescProveeHotel)-2) is null                                
 then                                                             
   ''                                                             
 else                                                       
 Y.DescProveeHotel -- Left(Y.DescProveeHotel,LEN(Y.DescProveeHotel)-2)                                                           
 End as DescProveeHotel,                                                                  
                                                             
 --RIGHT(Y.DescProveeHotel,2) as Estado,                                                          
                                                                       
 Y.DescUbigeo,Y.DiaCompleto , Y.Dia As DiaServicio,Y.DiaFormatFecha,Y.Hora,Y.Servicio,Y.TipoServ,Y.DescProvee, 
 --ltrim(RTRIM(Y.GuiaProgramado))+
 Case When ltrim(RTRIM(Y.GuiaProgramado)) = '' Then ltrim(RTRIM(Y.TrasladistaProgramado)) Else ltrim(RTRIM(Y.GuiaProgramado)) End +
 '/'+LTRIM(RTRIM(Y.TransportistaProgramado))                                                              
 --+'/' + ltrim(rtrim(Y.Vehiculo))               
as GuiaTransPrg                 
,Y.OrdenOperAloj as NroOrden3                 
 FROM                                                                      
  (    
  SELECT                                                                       
  ''as Sigla,                          
  dbo.FnDescripcionAcomodo_ReporteTapa(@IDCab,CONVERT(char(10),X.Dia,103)) as DescProveeHotel,                                                                                                 
  X.*                                                                      
  ,                                                              
                                                               
  Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                                                        
	  Else                             
	  case when X.IntExterno = 'I'  Then                                                          
	   IsNull(   
	   (Select Distinct p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                            
	   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')          
	   Where od1.IDOperacion_Det=X.IDOperacion_Det)                                                                        
	   ,'')                                                    
	  else                                                               
	  X.Guia 
   End                                                              
  End                                                                                           
  as GuiaProgramado,

  Case When CantTrasladistasProgramados>1 Then 'MULTIPLE'
	Else
	case When x.IntExterno='I' then
		IsNull(   
	   (Select Distinct p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                            
	   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('020')          
	   Where od1.IDOperacion_Det=X.IDOperacion_Det)                                                                        
	   ,'')                                                    

	 else
	  ''
	End
  End As TrasladistaProgramado,
                                
  Case When CantTranspProgramados >1 Then 'MULTIPLE'                                                    
  Else                      
  case when X.IntExterno = 'I'  Then                                                           
                                                                          
  IsNull(                                            
  (Select Distinct p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                                        
  Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                                                        
  Where od1.IDOperacion_Det=X.IDOperacion_Det)                                                                        
  ,'')                                                           
   else                                    
     ''                                                             
   End                                                                        
  End                                                                        
  as TransportistaProgramado                            
                                                                
        FROM                                                                      
   (                                                                      
   Select                                                    
		ROW_NUMBER()Over(Partition By convert(varchar,rd.Dia,103) order by rd.Dia) As Ord ,  rd.dia as DiaCompleto,        
    rd.Dia  as Dia,                               
   DATENAME(DAY,rd.Dia) + ' ' + SUBSTRING(DATENAME(MM,rd.Dia),1,3) + CHAR(13) + ' '+ UPPER(DATENAME(DW,rd.Dia))                                                       
 As DiaFormatFecha,                                                                    
   u.Descripcion as DescUbigeo,                                                                       
   substring(convert(varchar(5),rd.Dia,108),1,5) as Hora,       
                                                         
   case when p.IDTipoProv = '003' and sd.ConAlojamiento =1 then cd.Servicio else     
   rd.Servicio End As Servicio,                              
                                                         
  p.NombreCorto+'/'+ISNULL(r.CodReservaProv,'') as DescProvee,                                                                      
                                                                 
   (Select count(*) From                                                                        
   (                                                                        
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                                         
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                           
   Where od1.IDOperacion_Det=od.IDOperacion_Det                                                                        
   ) as x                                                                          
   ) as CantGuiasProgramados,                                             

   
   (Select count(*) From                                                                        
   (                                                                        
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                                         
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('020')                                           
   Where od1.IDOperacion_Det=od.IDOperacion_Det                                                                        
   ) as x                                                                          
   ) as CantTrasladistasProgramados,
                                                                        
   (Select count(*) From                                                                        
   (                                                           
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                                         
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                                    
   Where od1.IDOperacion_Det=od.IDOperacion_Det                                                                        
   ) as x                                                                          
   ) as CantTranspProgramados,                                                       
                                                                        
   od.IDOperacion, od.IDOperacion_Det,                                                               
   Isnull(vh.NoVehiculoCitado,'') as Vehiculo        ,                                               
                                                                         
   c.RazonComercial as DesCliente,                                                                      
   cc.IDFile, cc.Cotizacion, cc.Titulo, cc.NroPax, isnull(cc.NroLiberados,0) as NroLiberados,                                                                       
      nc.Descripcion as Nacionalidad,                                                    
                       
       ms.Descripcion as DescModoServ,  
   cc.FecInicio, cc.FechaOut, cc.Fecha, us.Nombre, cc.IDIdioma,                         
   isnull(cc.Simple,0) as Simple, isnull(cc.Twin,0) as Twin,                                                                       
   isnull(cc.Matrimonial,0) as Matrimonial, isnull(cc.Triple,0) as Triple,                                                               
                                                                         
   --isnull(cc.TipoCambio,0) as TipoCambio, 
   @SSTipCam as TipoCambio, 
   cc.Observaciones   ,ur.Nombre as UsuarioReserva, uo.Nombre as UsuarioOperaciones                                                                
   ,isnull(p.IDTipoOper,'') as IntExterno,                                                              
                                                           
   Case when s.IDTipoServ = 'NAP' then '---' else s.IDTipoServ End as TipoServ,                                                            
                                                               
   isnull(p3.Nombre,'') +' '+isnull(p3.ApPaterno,'') +' '+ISNULL(p3.ApMaterno,'') as Guia,                     
                                               
   rd.IDubigeo   ,             
   rd.IDReserva_Det    ,                
   case when p.IDTipoProv = '003' and sd.ConAlojamiento =1 then                
  ROW_NUMBER()Over(Partition By cd.Servicio order by cd.Servicio) else 1 End as OrdenOperAloj                        
                                                          
   From RESERVAS_DET  rd                                                 
   Inner Join RESERVAS r On rd.IDReserva=r.IDReserva And r.IDCab=@IDCab and rd.Anulado = 0                                    
   Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor --And (p.IDTipoProv<>'001')                 
   Left Join MAUBIGEO u On u.IDubigeo=rd.IDubigeo                              
   Left Join COTICAB cc On cc.IDCAB=r.IDCab                                   
   Left Join MAUBIGEO nc On cc.IDubigeo=nc.IDubigeo                                                                      
   Left Join MACLIENTES c On cc.IDCliente=c.IDCliente                                                                      
   Left Join MAMODOSERVICIO ms On cc.TipoServicio=ms.CodModoServ                                                                      
   Left Join MAUSUARIOS us On cc.IDUsuario=us.IDUsuario                                                    
   left Join MAUSUARIOS ur On cc.IDUsuarioRes = ur.IDUsuario                                                                
   Left Join MAUSUARIOS uo on cc.IDUsuarioOpe = uo.IDUsuario                                                                
   Left Join OPERACIONES_DET od on  od.IDReserva_Det = rd.IDReserva_Det                                                       
   --Left Join MAVEHICULOS vh on rd.NuVehiculo = vh.NuVehiculo                                                              
   Left Join MAVEHICULOS_TRANSPORTE vh on rd.NuVehiculo = vh.NuVehiculo      
                                                           
   Left Join MAPROVEEDORES p3 on rd.IDGuiaProveedor = p3.IDProveedor                                                              
   Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det                                                            
   Left Join MASERVICIOS s on rd.IDServicio = s.IDServicio                                                      
   Left Join COTIDET cd On rd.IDDet = cd.IDDET  and cd.IDDetRel is null                                                  
                                                  
   Where IsNull(rd.FlServicioNoShow,0)=0 and r.Anulado=0 and (rd.IDubigeo=@IDUbigeo Or ltrim(rtrim(@IDUbigeo))='')  And IsNull(rd.NuViaticoTC,0)=0        
     and (u.IDPais=@CoPais Or ltrim(rtrim(@CoPais))='')  
	 and ((p.IDTipoProv<>'001' and sd.ConAlojamiento=0) or (p.IDTipoProv = '001' and sd.PlanAlimenticio = 1))                
   And ((rd.Dia between @RangoFecIni and DATEADD(s,-1,DATEADD(d,1,@RangoFecFin)))                                                           
   Or (@RangoFecIni = '01/01/1900' And @RangoFecFin = '01/01/1900'))                                                                  
   And r.Estado <> 'XL'  
   And (@ConGastosTransferencias = 1 Or CHARINDEX(upper('Gastos de Transferencia'),UPPER(rd.Servicio)) = 0)
    ) AS X                                                                       
   ) AS Y                                                             
  ) As Z                   
  where Z.Servicio is not null and z.NroOrden3=1                                                                  
                                                        
  Union                                                      
                                           
 Select                                                           
 Z.Ord, 
 Z.DesCliente, Z.IDFile, Z.Cotizacion, Z.Titulo, Z.NroPax, Z.NroLiberados, Z.Nacionalidad, Z.DescModoServ,                                                                      
 Z.FecInicio, Z.FechaOut, Z.Fecha, Z.Nombre, Z.IDIdioma,                               
 Z.Acomodacion,Z.TipoCambio,cast(Z.Observaciones as varchar(max)) as Observaciones,Z.UsuarioReserva,Z.UsuarioOperaciones,Z.Desayuno,                                           
 Case when LEN(Z.DescProveeHotel)-2 < 0 then '' else  Left(Z.DescProveeHotel,LEN(Z.DescProveeHotel)-2) End As DescProveeHotel,                                                          
 Case when len(Z.DescProveeHotel)=0 then '' else RIGHT(Z.DescProveeHotel,2) End as Estado,                                                     
 Z.DescUbigeo,Z.DiaCompleto,Z.DiaServicio,Z.DiaFormatFecha,Z.Hora,Z.Servicio,Z.TipoServ,Z.DescProvee,                                  
 Z.GuiaTransPrg                                                       
                                          
 from (                                 
 SELECT Y.Ord,
 Y.DesCliente, Y.IDFile, Y.Cotizacion, Y.Titulo, Y.NroPax, Y.NroLiberados, Y.Nacionalidad, Y.DescModoServ,                                                                      
 Y.FecInicio, Y.FechaOut, Y.Fecha, Y.Nombre, Y.IDIdioma,        
                                                                       
 Case When Y.Simple>0 Then Cast(Y.Simple AS varchar(3))+' SIMPLE ' Else '' End+                                                                      
 Case When Y.Twin>0 Then Cast(Y.Twin AS varchar(3))+' TWIN ' Else '' End+                  
 Case When Y.Matrimonial>0 Then Cast(Y.Matrimonial AS varchar(3))+' MATRIMONIAL ' Else'' End+                                                                      
 Case When Y.Triple>0 Then Cast(Y.Triple AS varchar(3))+' TRIPLE ' Else '' End As Acomodacion,                                                             
                                                                       
                          
 Y.TipoCambio, Y.Observaciones, Y.UsuarioReserva ,Y.UsuarioOperaciones,                                                                             
 Y.Sigla as Desayuno,                                                               
                                                                    
 case when Y.DescProveeHotel is null  then '' else  Y.DescProveeHotel End as DescProveeHotel,                                      
 Y.DescUbigeo,Y.DiaCompleto , Y.Dia As DiaServicio,Y.DiaFormatFecha,Y.Hora,Y.Servicio,Y.TipoServ,Y.DescProvee, ltrim(RTRIM(Y.GuiaProgramado))+'/'+LTRIM(RTRIM(Y.TransportistaProgramado))

 as GuiaTransPrg                                                                      
 FROM                                                                 
  (                                 
  SELECT '' as Sigla,         
  dbo.FnDescripcionAcomodo_ReporteTapa(@IDCab,CONVERT(char(10),X.Dia,103)) as DescProveeHotel,                       
                                                                        
  X.*                             
  ,                                                              
                                                                
  Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                                  
  Else                                                                      
  case when X.IntExterno = 'I'  Then                                                              
   IsNull(                                       
   (Select Distinct p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                                     
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                                                      
   Where od1.IDOperacion_Det=X.IDOperacion_Det and od1.FlServicioNoShow=0)                 
   ,'')                                                                        
else                                           
  X.Guia                                  
   End                                                              
  End                                                                 
     
  as GuiaProgramado,                                                                
    
  Case When CantTranspProgramados >1 Then 'MULTIPLE'                                            
  Else                            
  case when X.IntExterno = 'I'  Then                  
                                                                          
  IsNull(                                                                        
  (Select Distinct p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                            
  Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                                                        
  Where od1.IDOperacion_Det=X.IDOperacion_Det and od1.FlServicioNoShow=0)                                                                        
  ,'')                                                           
   else                                                         
     ''                                                             
   End                                                                        
  End                                                                        
  as TransportistaProgramado                                                                      
                                                                
        FROM              
   (                                        
   Select                                                                          
   ROW_NUMBER()Over(Partition By cd.IDServicio_Det,convert(char(10),cd.Dia,103) order by cd.IDServicio_Det,cd.Dia)  As Ord ,  rd.dia as DiaCompleto,                                                      
    --cd.Dia  as Dia,
	Case When Exists(select distinct Dia from RESERVAS_DET rd 
					 Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
					 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
					 Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
					 Where rd.IDDet=cd.IDDet And rd.Anulado=0 And (p.IDTipoProv='003' And sd.ConAlojamiento=1)) and IsNull(cd.IDDetRel,0) = 0 Then
		(select Top(1) Dia from RESERVAS_DET rd 
					 Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
					 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
					 Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
		 Where rd.IDDet=cd.IDDet And rd.Anulado=0 And (p.IDTipoProv='003' And sd.ConAlojamiento=1))
	Else cd.Dia End as Dia,
	               
 DATENAME(DAY,cd.Dia) + ' ' + SUBSTRING(DATENAME(MM,cd.Dia),1,3) + CHAR(13) + ' '+ UPPER(DATENAME(DW,cd.Dia))                                     
 As DiaFormatFecha,                                                                    
   u.Descripcion as DescUbigeo,                                                                       
   --substring(convert(varchar(5),cd.Dia,108),1,5) as Hora,                                                      
   substring(convert(varchar(5),
   Case When Exists(select distinct Dia from RESERVAS_DET rd 
   					 Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
					 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
					 Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
					Where rd.IDDet=cd.IDDet And rd.Anulado=0 And (p.IDTipoProv='003' And sd.ConAlojamiento=1)) and IsNull(cd.IDDetRel,0) = 0 Then
		(select Top(1) Dia from RESERVAS_DET rd 
							 Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
							 Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
							 Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
		Where rd.IDDet=cd.IDDet And rd.Anulado=0 And (p.IDTipoProv='003' And sd.ConAlojamiento=1))
	Else cd.Dia End 
	 ,108),1,5) As Hora,
                                                         
   cd.Servicio,                                                        
                             
   p.NombreCorto+'/'+ISNULL(r.CodReservaProv,'') as DescProvee,                                                                      
                                                                 
   (Select count(*) From                                                                        
   (                                                            
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                                         
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                                                      
   Where od1.IDOperacion_Det=od.IDOperacion_Det and od1.FlServicioNoShow = 0          
   ) as x                                                                          
   ) as CantGuiasProgramados,                                                               
                                                                        
   (Select count(*) From                               
   (                           
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1 
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'              
   Where od1.IDOperacion_Det=od.IDOperacion_Det and od1.FlServicioNoShow=0                                              
   ) as x                                
   ) as CantTranspProgramados,                    
                                                                
   od.IDOperacion, od.IDOperacion_Det,                                                               
   Isnull(vh.NoVehiculoCitado,'') as Vehiculo        ,                                                              
                                                                        
   c.RazonComercial as DesCliente,                                   
   cc.IDFile, cc.Cotizacion, cc.Titulo, cc.NroPax, isnull(cc.NroLiberados,0) as NroLiberados,                                                                       
      nc.Descripcion as Nacionalidad, ms.Descripcion as DescModoServ,                                                                      
   cc.FecInicio, cc.FechaOut, cc.Fecha, us.Nombre, cc.IDIdioma,       
   isnull(cc.Simple,0) as Simple, isnull(cc.Twin,0) as Twin,                                                                   
   isnull(cc.Matrimonial,0) as Matrimonial, isnull(cc.Triple,0) as Triple,                                                                      
            
   --isnull(cc.TipoCambio,0) as TipoCambio, 
   @SSTipCam as TipoCambio, 
   cc.Observaciones   ,ur.Nombre as UsuarioReserva, uo.Nombre as UsuarioOperaciones       
   ,isnull(p.IDTipoOper,'') as IntExterno,                                                                                                                          
   Case when s.IDTipoServ = 'NAP' then '---' else s.IDTipoServ End as TipoServ,                                                                                                                          
   isnull(p3.Nombre,'') +' '+isnull(p3.ApPaterno,'') +' '+ISNULL(p3.ApMaterno,'') as Guia,                                                              
                                                              
   rd.IDubigeo
   From COTIDET cd Left Join RESERVAS_DET rd On (rd.IDDet = cd.IDDetRel Or cd.IDDET=rd.IDDet) and IsNull(rd.Anulado,0) = 0                                            
   Inner Join RESERVAS r On rd.IDReserva=r.IDReserva And r.IDCab=@IDCab                                                                      
   Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor and p.IDTipoProv <> '001'                                                     
   Left Join MAUBIGEO u On u.IDubigeo=rd.IDubigeo                                                                   
   Left Join COTICAB cc On cc.IDCAB=r.IDCab                                                                   
   Left Join MAUBIGEO nc On cc.IDubigeo=nc.IDubigeo                                                                      
   Left Join MACLIENTES c On cc.IDCliente=c.IDCliente                                                                      
   Left Join MAMODOSERVICIO ms On cc.TipoServicio=ms.CodModoServ                                                                      
   Left Join MAUSUARIOS us On cc.IDUsuario=us.IDUsuario     
   left Join MAUSUARIOS ur On cc.IDUsuarioRes = ur.IDUsuario                                                                
   Left Join MAUSUARIOS uo on cc.IDUsuarioOpe = uo.IDUsuario                                                                
   Left Join OPERACIONES_DET od on  od.IDReserva_Det = rd.IDReserva_Det                                                              
   --Left Join MAVEHICULOS vh on rd.NuVehiculo = vh.NuVehiculo                
   Left Join MAVEHICULOS_TRANSPORTE vh on rd.NuVehiculo = vh.NuVehiculo              
   Left Join MAPROVEEDORES p3 on rd.IDGuiaProveedor = p3.IDProveedor                                                              
   Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det       
   Left Join MASERVICIOS s on rd.IDServicio = s.IDServicio
   --Left Join COTIDET cd2 On rd.IDDET = cd2.IDDet
   Left Join MASERVICIOS_DET sd2 On cd.IDServicio_Det = sd2.IDServicio_Det                                             
   Where IsNull(rd.FlServicioNoShow,0)=0 and cd.IDCAB=@IDCab and r.Anulado=0 and  rd.Anulado = 0 And IsNull(rd.NuViaticoTC,0)=0
    and (rd.IDubigeo=@IDUbigeo Or ltrim(rtrim(@IDUbigeo))='')                                                    
    and (u.IDPais=@CoPais Or ltrim(rtrim(@CoPais))='')  
	--and (p.IDTipoProv ='003' and sd.ConAlojamiento=1)
	and (p.IDTipoProv ='003' and sd2.ConAlojamiento=1)
    And ((rd.Dia between @RangoFecIni and DATEADD(s,-1,DATEADD(d,1,@RangoFecFin)))                 
    Or (@RangoFecIni = '01/01/1900' And @RangoFecFin = '01/01/1900'))                                                                  
    And r.Estado <> 'XL' and r.Anulado=0                                                      
    ) AS X                                                                      
   ) AS Y                                                             
  ) As Z                                   
   where Z.Ord = 1                                        
  Union                
  
  Select            
 Z.Ord,                          
 Z.DesCliente, Z.IDFile, Z.Cotizacion, Z.Titulo, Z.NroPax, Z.NroLiberados, Z.Nacionalidad, Z.DescModoServ,                                                                      
 Z.FecInicio, Z.FechaOut, Z.Fecha, Z.Nombre, Z.IDIdioma,                                                            
 Z.Acomodacion,Z.TipoCambio,cast(Z.Observaciones as varchar(max)) as Observaciones,Z.UsuarioReserva,Z.UsuarioOperaciones,Z.Desayuno,                                           
 Case when LEN(Z.DescProveeHotel)-2 < 0 then '' else  Left(Z.DescProveeHotel,LEN(Z.DescProveeHotel)-2) End As DescProveeHotel,                                                          
 Case when len(Z.DescProveeHotel)=0 then '' else RIGHT(Z.DescProveeHotel,2) End as Estado,                                                     
 Z.DescUbigeo,Z.DiaCompleto,Z.DiaServicio,Z.DiaFormatFecha,Z.Hora,Z.Servicio,Z.TipoServ,Z.DescProvee,                                                  
 Z.GuiaTransPrg                                                            
                                                            
 from (                                 
 SELECT Y.Ord,                                                                   
 Y.DesCliente, Y.IDFile, Y.Cotizacion, Y.Titulo, Y.NroPax, Y.NroLiberados, Y.Nacionalidad, Y.DescModoServ,                                                                      
 Y.FecInicio, Y.FechaOut, Y.Fecha, Y.Nombre, Y.IDIdioma,        
                                                                       
 Case When Y.Simple>0 Then Cast(Y.Simple AS varchar(3))+' SIMPLE ' Else '' End+                                                                      
 Case When Y.Twin>0 Then Cast(Y.Twin AS varchar(3))+' TWIN ' Else '' End+                  
 Case When Y.Matrimonial>0 Then Cast(Y.Matrimonial AS varchar(3))+' MATRIMONIAL ' Else'' End+                                                                   
 Case When Y.Triple>0 Then Cast(Y.Triple AS varchar(3))+' TRIPLE ' Else '' End As Acomodacion,                                                             
                                                                       
                          
 Y.TipoCambio, Y.Observaciones, Y.UsuarioReserva ,Y.UsuarioOperaciones,                                                                   
 Y.Sigla as Desayuno,                                                    
                                                      
 case when Y.DescProveeHotel is null  then '' else  Y.DescProveeHotel End as DescProveeHotel,             
Y.DescUbigeo,Y.DiaCompleto , Y.Dia As DiaServicio,Y.DiaFormatFecha,Y.Hora,Y.Servicio,Y.TipoServ,Y.DescProvee, ltrim(RTRIM(Y.GuiaProgramado))+'/'+LTRIM(RTRIM(Y.TransportistaProgramado))                                                                   
 as GuiaTransPrg                                                     
 FROM                                                                 
  (                                                                      
  SELECT '' as Sigla,                                                                                                  
  dbo.FnDescripcionAcomodo_ReporteTapa(@IDCab,CONVERT(char(10),X.Dia,103)) as DescProveeHotel,                                                                      
                                                                        
  X.*                             
  ,                                                              
                                                                
  Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                                  
  Else                                                                      
  case when X.IntExterno = 'I'  Then                                                              
   IsNull(                                       
   (Select Distinct p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                                         
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                                                      
   Where od1.IDOperacion_Det=X.IDOperacion_Det and od1.FlServicioNoShow=0)                                                                   
   ,'')                                                                        
  else                                           
  X.Guia                                                              
   End                                                              
  End               
     
  as GuiaProgramado,                                                                
                                                                
  Case When CantTranspProgramados >1 Then 'MULTIPLE'                                                   
  Else                              
  case when X.IntExterno = 'I'  Then                                                           
                                                                          
  IsNull(                                                                        
  (Select Distinct p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                            
  Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                                                        
  Where od1.IDOperacion_Det=X.IDOperacion_Det and od1.FlServicioNoShow=0)                                                                        
  ,'')                                                           
   else          
     ''                                                             
   End                                                                        
  End                                                                        
  as TransportistaProgramado                                                                      
                                                                
        FROM              
   (          
   Select                                                                          
   ROW_NUMBER()Over(Partition By convert(varchar,cd.Dia,103) order by rd.Dia)  As Ord ,  rd.dia as DiaCompleto,                                                      
 cd.Dia  as Dia,             
 DATENAME(DAY,cd.Dia) + ' ' + SUBSTRING(DATENAME(MM,cd.Dia),1,3) + CHAR(13) + ' '+ UPPER(DATENAME(DW,cd.Dia))                     
 As DiaFormatFecha,                                                       
   u.Descripcion as DescUbigeo,                                                                       
   substring(convert(varchar(5),cd.Dia,108),1,5) as Hora,                                                      
                                                         
   cd.Servicio,                                                        
                             
   p.NombreCorto+'/'+ISNULL(r.CodReservaProv,'') as DescProvee,                                                                      
                                                                 
   (Select count(*) From                                                                        
   (                                                            
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                                         
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                                                      
   Where od1.IDOperacion_Det=od.IDOperacion_Det and od1.FlServicioNoShow = 0                                               
   ) as x                                                                          
   ) as CantGuiasProgramados,                                                               
                                                                        
   (Select count(*) From                               
   (                                   
   Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                                         
   Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'              
   Where od1.IDOperacion_Det=od.IDOperacion_Det and od1.FlServicioNoShow=0                                                                     
   ) as x                                
   ) as CantTranspProgramados,                                                               
                                                                
   od.IDOperacion, od.IDOperacion_Det,                                                               
   Isnull(vh.NoVehiculoCitado,'') as Vehiculo        ,                                                              
                                                                        
   c.RazonComercial as DesCliente,                                   
   cc.IDFile, cc.Cotizacion, cc.Titulo, cc.NroPax, isnull(cc.NroLiberados,0) as NroLiberados,                                                                       
      nc.Descripcion as Nacionalidad, ms.Descripcion as DescModoServ,                                                                      
   cc.FecInicio, cc.FechaOut, cc.Fecha, us.Nombre, cc.IDIdioma,       
   isnull(cc.Simple,0) as Simple, isnull(cc.Twin,0) as Twin,             
   isnull(cc.Matrimonial,0) as Matrimonial, isnull(cc.Triple,0) as Triple,                                                                      
                                                                         
   --isnull(cc.TipoCambio,0) as TipoCambio, 
   @SSTipCam as TipoCambio, 
   cc.Observaciones   ,ur.Nombre as UsuarioReserva, uo.Nombre as UsuarioOperaciones       
   ,isnull(p.IDTipoOper,'') as IntExterno,                                                              
                                                 
   Case when s.IDTipoServ = 'NAP' then '---' else s.IDTipoServ End as TipoServ, 
                                                        
   isnull(p3.Nombre,'') +' '+isnull(p3.ApPaterno,'') +' '+ISNULL(p3.ApMaterno,'') as Guia,                                                              
                                  
   rd.IDubigeo        
   From COTIDET cd Left Join RESERVAS_DET rd On rd.IDDet = cd.IDDET And rd.IDServicio_Det=cd.IDServicio_Det and rd.Anulado = 0                                            
   Inner Join RESERVAS r On rd.IDReserva=r.IDReserva And r.IDCab=@IDCab                                                                      
   Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor and (p.IDTipoProv <> '001')                                                      
   Left Join MAUBIGEO u On u.IDubigeo=rd.IDubigeo                                                                   
   Left Join COTICAB cc On cc.IDCAB=r.IDCab                                                                   
   Left Join MAUBIGEO nc On cc.IDubigeo=nc.IDubigeo                                                                      
   Left Join MACLIENTES c On cc.IDCliente=c.IDCliente                                                                      
   Left Join MAMODOSERVICIO ms On cc.TipoServicio=ms.CodModoServ                                                                      
   Left Join MAUSUARIOS us On cc.IDUsuario=us.IDUsuario                                                                 
   left Join MAUSUARIOS ur On cc.IDUsuarioRes = ur.IDUsuario                                                                
   Left Join MAUSUARIOS uo on cc.IDUsuarioOpe = uo.IDUsuario                     
   Left Join OPERACIONES_DET od on  od.IDReserva_Det = rd.IDReserva_Det                                                              
   --Left Join MAVEHICULOS vh on rd.NuVehiculo = vh.NuVehiculo                                                       
   Left Join MAVEHICULOS_TRANSPORTE vh on rd.NuVehiculo = vh.NuVehiculo                                                          
   Left Join MAPROVEEDORES p3 on rd.IDGuiaProveedor = p3.IDProveedor                                                              
   Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det                        
   Left Join MASERVICIOS s on rd.IDServicio = s.IDServicio                          
                                                                
   Where IsNull(rd.FlServicioNoShow,0)=0 and cd.IDCAB=@IDCab and r.Anulado=0 and  rd.Anulado = 0 And IsNull(rd.NuViaticoTC,0)=0
    and (rd.IDubigeo=@IDUbigeo Or ltrim(rtrim(@IDUbigeo))='')                                                    
    and (u.IDPais=@CoPais Or ltrim(rtrim(@CoPais))='')  
	--and (p.IDTipoProv ='003' and sd.ConAlojamiento=1)                                                      
	and ((p.IDTipoProv ='003' and sd.ConAlojamiento=1) and cd.IDDetRel is not null)
    And ((rd.Dia between @RangoFecIni and DATEADD(s,-1,DATEADD(d,1,@RangoFecFin)))                 
    Or (@RangoFecIni = '01/01/1900' And @RangoFecFin = '01/01/1900'))                                                                  
    And r.Estado <> 'XL'                                                              
    ) AS X                             
   ) AS Y                                                             
  ) As Z                                                                
    where Z.Ord = 1  
  
  Union                                    
                                                      
SELECT                                                     
 ROW_NUMBER()Over(Partition By convert(varchar,cd.Dia,103) order by cd.Dia)  As Ord,                                                    
 cl.RazonComercial as DescCliente,c.IDFile,c.Cotizacion,c.Titulo,c.NroPax,Isnull(c.NroLiberados,0) as NroLiberados,                   
 u.Descripcion as Nacionalidad,tp.Descripcion as DescModoServ,c.FecInicio as FecInicio,c.FechaOut as FechaOut,                                   
 c.Fecha as Fecha,uven.Nombre,c.IDIdioma,                                                    
 Case When Simple>0 Then Cast( Simple AS varchar(3))+' SIMPLE ' Else '' End+                                                            
  Case When  Twin>0 Then Cast( Twin AS varchar(3))+' TWIN ' Else '' End+                                                                      
  Case When  Matrimonial>0 Then Cast( Matrimonial AS varchar(3))+' MATRIMONIAL ' Else'' End+                                                                      
  Case When  Triple>0 Then Cast(Triple AS varchar(3))+' TRIPLE ' Else '' End As Acomodacion,                                                    
  --Isnull(c.TipoCambio,0) as TipoCambio,
  @SSTipCam as TipoCambio,
  Cast(Isnull(c.Observaciones,'') as varchar(max)) as Observaciones,ures.Nombre as UsuarioReserva,uope.Nombre as UsuarioOperaciones,                                                    
  '' as Desayuno,                                                 
  REPLACE(REPLACE(dbo.FnDescripcionAcomodo_ReporteTapa(@IDCab,CONVERT(char(10),cd.Dia,103)),'RR',''),'OK','') as DescProveeHotel,                                                    
                               
   (select distinct r.Estado from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva  and rd.Anulado = 0    
   Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor   
   where r.anulado=0 and (r.Estado ='OK' Or r.Estado ='RR') and r.IDCab = @IDCab and cd.Dia between rd.Dia and dateadd(d,-1,rd.FechaOut)    
   and p.IDTipoProv='001') as Estado    
                                          
  ,ub2.Descripcion as DescUbigeo, cd.Dia as DiaCompleto,cd.Dia as DiaServicio,                                                    
  DATENAME(DAY,cd.Dia) + ' ' + SUBSTRING(DATENAME(MM,cd.Dia),1,3) + CHAR(13) + ' '+ UPPER(DATENAME(DW,cd.Dia)) As DiaFormatFecha,                                        
     convert(varchar(5),cd.Dia,108) as Hora,cd.Servicio,                                                    
     case when tp.IDTipoServ = 'NAP' Then '---' Else tp.IDTipoServ End as TipoServ,                        
     pr.NombreCorto +'/'+ Isnull((select distinct r.CodReservaProv from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva and rd.Anulado = 0    
     Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor    
  where r.anulado=0 and (r.Estado ='OK' Or r.Estado ='RR') and r.IDCab = @IDCab and cd.Dia between rd.Dia and dateadd(d,-1,rd.FechaOut) and p.IDTipoProv='001'    
--HLF-20140902-I             
 and r.IDProveedor=cd.IDProveedor          
--HLF-20140902-F           
 ),'') As DescProvee,'/' as GuiaTransPrg                                                    
           
FROM COTIDET cd Left Join COTICAB c On cd.IDCAB = c.IDCAB Left Join MACLIENTES cl On c.IDCliente= cl.IDCliente                                              
Left Join MAUBIGEO u ON c.IDubigeo = u.IDubigeo Left Join MASERVICIOS s On cd.IDServicio = s.IDServicio                                                    
Left Join MATIPOSERVICIOS tp on s.IDTipoServ = tp.IDTipoServ Left Join MAUSUARIOS uven on c.IDUsuario = uven.IDUsuario                                  
Left Join MAUSUARIOS ures on c.IDUsuarioRes = ures.IDUsuario Left Join MAUSUARIOS uope on c.IDUsuarioOpe = uope.IDUsuario                                                    
Left Join MAPROVEEDORES pr on cd.IDProveedor = pr.IDProveedor                                                    
Left Join MAUBIGEO ub2 On cd.IDubigeo = ub2.IDubigeo                                         
                                                    
WHERE   
  ((Convert(varchar(10),@RangoFecIni,103)='01/01/1900' and Convert(varchar(10),@RangoFecFin,103)='01/01/1900') Or  
  cd.Dia between @RangoFecIni and @RangoFecFin) and  
 cd.IDDET In             
 (select IDDet from (                                  
       select COUNT(distinct convert(char(10),cd.Dia,103)) As Cantidad,convert(char(10),cd.Dia,103) Fec,                                  
       (select count(distinct cd2.IDServicio_Det) from cotidet cd2           
   where convert(char(10),cd2.Dia,103) = convert(char(10),cd.Dia,103)                    
   and cd2.IDCAB = @IDCab and Not cd2.Servicio like '%free%' and           
   Not cd2.IDServicio In ('LIM00234','LIM00461','IPC00002')             
--HLF-20140902-I            
   And cd2.IDProveedor=pr.IDProveedor          
--HLF-20140902-F          
   ) as Cant ,          
             
   cd.IDDet                                                    
       from COTIDET cd Left Join RESERVAS_DET rd2 On rd2.IDDet = cd.IDDET   and rd2.Anulado = 0                                                 
       Left Join MAPROVEEDORES pr On cd.IDProveedor = pr.IDProveedor -- and pr.IDProveedor = '001'                                
       Left Join MASERVICIOS_DET sd On cd.IDServicio_Det = sd.IDServicio_Det                                  
       where IsNull(rd2.FlServicioNoShow,0)=0 and cd.IDCAB =@IDCab and (pr.IDTipoProv = '001' and sd.PlanAlimenticio =0)                                  
       group by cd.Dia,convert(char(10),cd.Dia,103),cd.IDDet          
       ,pr.IDProveedor                                               
       ) as X                                                    
       where X.Cantidad = 1   
       and X.Cant = X.Cantidad)
       and not Convert(char(10),cd.Dia,103) in (select distinct CONVERT(char(10),rd.Dia,103) from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva
       Left JOin MAPROVEEDORES p On r.IDProveedor = p.IDProveedor               
       where IsNull(rd.FlServicioNoShow,0) = 0 and r.IDCab =@IDCab and rd.Anulado =0 and r.Anulado = 0 and p.IDTipoProv <> '001' and r.Estado <> 'XL' and --PPMG20160429
       Not rd.IDServicio In ('LIM00234','LIM00461','IPC00002'))
 ) As XZ                                                    
 Where(@ConGastosTransferencias = 1 Or CHARINDEX(upper('Gastos de Transferencia'),UPPER(XZ.Servicio)) = 0)
 --and (Convert(varchar(10),@RangoFecIni,103)='01/01/1900' Or XZ.DiaCompleto between @RangoFecIni And @RangoFecFin)    
 Order by XZ.DiaServicio,XZ.DiaCompleto,XZ.Ord,XZ.Servicio
END
