﻿
Create Procedure dbo.OPERACIONES_DET_SelVoucherxOrdenServicio
	@IDCab int,
	@NuOrden_Servicio int
As
	Set Nocount On
	
	SELECT Distinct IDVoucher
	FROM OPERACIONES_DET WHERE IDOperacion IN (SELECT IDOperacion FROM OPERACIONES 
		WHERE IDCab=@IDCab)
	AND IDServicio_Det In 
	(SELECT IDServicio_Det
	FROM ORDEN_SERVICIO_DET WHERE NuOrden_Servicio IN (SELECT NuOrden_Servicio FROM ORDEN_SERVICIO
		WHERE IDCab=@IDCab)
	AND NuOrden_Servicio=@NuOrden_Servicio)


