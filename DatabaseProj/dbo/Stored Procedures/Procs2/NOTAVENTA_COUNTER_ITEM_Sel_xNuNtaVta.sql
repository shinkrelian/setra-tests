﻿
---------------------------------------------------------------------------------------------------------------------------


--JHD-20150116- Se agregó el campo CoCliente
CREATE PROCEDURE [dbo].[NOTAVENTA_COUNTER_ITEM_Sel_xNuNtaVta]    

@NuNtaVta char(6),
@NuItem tinyint=0,
@CoTipoServicio char(3)='',
@TxDetalle VARCHAR(150)='',
@CoProveedor char(6)=''
AS
BEGIN
SELECT     
NOTAVENTA_COUNTER_ITEM.NuItem,
NOTAVENTA_COUNTER_ITEM.CoTipoServicio, 
                      MATIPOSERVICIO_COUNTER.NoDescripcion,
                     --  NOTAVENTA_COUNTER_ITEM.NuDocumento,
                      NOTAVENTA_COUNTER_ITEM.TxDetalle,
                      CoProveedor=isnull(NOTAVENTA_COUNTER_ITEM.CoProveedor,''), 
                      --MAPROVEEDORES.NombreCorto,
                      NombreCorto=
								CASE WHEN
								isnull((select nombrecorto from maproveedores where IDProveedor=NOTAVENTA_COUNTER_ITEM.CoProveedor),'')
								='' THEN isnull((select RazonSocial from MACLIENTES where IDCliente=NOTAVENTA_COUNTER_ITEM.CoCliente),'')
								ELSE isnull((select nombrecorto from maproveedores where IDProveedor=NOTAVENTA_COUNTER_ITEM.CoProveedor),'') END
								,
                      NOTAVENTA_COUNTER_ITEM.QtCantidad,
                    
                     
                      NOTAVENTA_COUNTER_ITEM.SSUnidad, 
                      NOTAVENTA_COUNTER_ITEM.SSTotal
                      
                      ,CoCliente=isnull(NOTAVENTA_COUNTER_ITEM.CoCliente,''),
                      Cliente=isnull((select RazonSocial from MACLIENTES where IDCliente=NOTAVENTA_COUNTER_ITEM.CoCliente),'')
                      
                      
FROM         NOTAVENTA_COUNTER_ITEM INNER JOIN
                      MATIPOSERVICIO_COUNTER ON NOTAVENTA_COUNTER_ITEM.CoTipoServicio = MATIPOSERVICIO_COUNTER.CoTipoServicio --INNER JOIN
                     -- MAPROVEEDORES ON NOTAVENTA_COUNTER_ITEM.CoProveedor = MAPROVEEDORES.IDProveedor
                      --INNER JOIN
                      --MAMONEDAS ON NOTAVENTA_COUNTER_ITEM.CoMoneda = MAMONEDAS.IDMoneda
WHERE    (NOTAVENTA_COUNTER_ITEM.NuNtaVta = @NuNtaVta) AND

	(@NuItem=0 Or NOTAVENTA_COUNTER_ITEM.NuItem =@NuItem ) AND
			
			(ltrim(rtrim(@CoTipoServicio))='' Or NOTAVENTA_COUNTER_ITEM.CoTipoServicio like '%' +@CoTipoServicio + '%') AND
			(ltrim(rtrim(@TxDetalle))='' Or NOTAVENTA_COUNTER_ITEM.TxDetalle like '%' +@TxDetalle + '%') AND
			(ltrim(rtrim(@CoProveedor))='' Or NOTAVENTA_COUNTER_ITEM.CoProveedor like '%' +@CoProveedor + '%')
	
END;

