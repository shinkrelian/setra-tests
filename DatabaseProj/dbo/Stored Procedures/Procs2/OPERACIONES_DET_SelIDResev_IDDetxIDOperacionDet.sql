﻿--HLF-20140304-isnull(rd.IDDet,0) as IDDet  
--JRF-20150308-IsNull(od.IDReserva_Det,0) as IDReserva_Det
CREATE Procedure dbo.OPERACIONES_DET_SelIDResev_IDDetxIDOperacionDet    
@IDOperacion_Det int    
As    
 Set NoCount On    
 Select IsNull(od.IDReserva_Det,0) as IDReserva_Det,isnull(rd.IDDet,0) as IDDet  
 from OPERACIONES_DET od left join RESERVAS_DET rd On od.IDReserva_Det = rd.IDReserva_Det    
 where IDOperacion_Det = @IDOperacion_Det 
