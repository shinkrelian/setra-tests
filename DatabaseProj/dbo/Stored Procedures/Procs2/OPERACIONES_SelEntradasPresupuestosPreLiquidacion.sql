﻿    
--HLF-20140819-SD.CoMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))    
--dbo.FnCambioMoneda(NetoCotizado,ODS.IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                     
--HLF-20141204-DescPaisProveedor  
--HLF-20141205-Case When idTipoOC='011' Then 0  
--Case When idTipoOC='011' Then VentaUSD Else 0 End as BoletaServicioExterior  
--HLF-20150205-@CoUbigeo_Oficina char(6)
--And p.CoUbigeo_Oficina=@CoUbigeo_Oficina
--HLF-20150206-Forzar a Factura cuando es @CoUbigeo_Oficina<>'' (Buenos Aires, Santiago)
--HLF-20150211-Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) ...
CREATE Procedure dbo.OPERACIONES_SelEntradasPresupuestosPreLiquidacion              
 @IDCab int,
 @CoUbigeo_Oficina char(6)          
As              
              
 Set Nocount On              
              
 Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)              
 --Declare @TipoCambio as numeric(8,2) =(Select TipoCambio From COTICAB Where IDCAB=@IDCab)         
 Declare @MargenInterno as numeric(6,2)=10               
         
 SELECT A.*,  
 Case When idTipoOC='011' and @CoUbigeo_Oficina='' Then 0  
 Else  
	Case When @CoUbigeo_Oficina='' Then
		Case When idTipoOC='001' Then VentaUSD Else 0 End  
	Else
		VentaUSD
	End
 End As FacturaExportacion,              
 Case When idTipoOC='011' or @CoUbigeo_Oficina<>'' Then 0  
 Else  
 Case When idTipoOC='001' Then 0 Else VentaUSD End   
 End As BoletaNoExportacion,  
 Case When idTipoOC='011' and @CoUbigeo_Oficina='' Then VentaUSD Else 0 End as BoletaServicioExterior  
 FROM              
  (              
  SELECT               
   ZZ.IDProveedor, ZZ.DescProveedor, ZZ.DescProveedorInternac,DescPaisProveedor,  
   ZZ.CompraNeto, ZZ.IGVCosto,               
   ZZ.IGV_SFE, ZZ.Moneda,               
   ZZ.Total,              
   ZZ.idTipoOC, ZZ.DescOperacionContab, ZZ.Margen,              
   ZZ.CostoBaseUSD,               
   ROUND( ZZ.CostoBaseUSD /(1 - (ZZ.Margen/100)) ,0) AS VentaUSD              
   --,YY.FacturaExportacion,YY.BoletaNoExportacion              
  FROM              
   (              
   SELECT               
    YY.IDProveedor, YY.DescProveedor, YY.DescProveedorInternac,DescPaisProveedor,  
    YY.CompraNeto, YY.IGVCosto,               
    YY.IGV_SFE,             
    --YY.CompraNeto+YY.IGVCosto+YY.IGV_SFE AS Total,              
    YY.Total,            
    YY.Moneda, YY.idTipoOC, YY.DescOperacionContab, YY.Margen,              
    CompraNetoUSD+TotImptoUSD+IGV_SFE as CostoBaseUSD               
    --YY.VentaUSD,              
    --YY.FacturaExportacion,YY.BoletaNoExportacion              
   FROM              
    (              
    SELECT XX.IDProveedor, DescProveedor, DescProveedorInternac,DescPaisProveedor,  
     CompraNeto, IGVCosto,               
     --Case When idTipoOC='001' Then               
     --  CompraNeto*(@IGV/100)               
     -- Else                
     --  Case When idTipoOC='006' Then (CompraNeto*(@IGV/100))*0.5 Else 0 End              
     -- End as IGV_SFE,               
     0 AS IGV_SFE,            
                 
     Moneda, idTipoOC, DescOperacionContab, Margen, Total,            
     CompraNetoUSD, TotImptoUSD               
     --CostoBaseUSD, VentaUSD,              
     --Case When XX.idTipoOC='001' Then VentaUSD Else 0 End As FacturaExportacion,              
     --Case When XX.idTipoOC='001' Then 0 Else VentaUSD End As BoletaNoExportacion              
    FROM              
     (                
     SELECT IDProveedor,DescProveedor, DescProveedorInternac,DescPaisProveedor,  
                   
      CompraNeto, IGVCosto,Moneda,               
      --CompraNetoUSD+TotImptoUSD as Total,               
      CompraNetoUSD as Total,               
      idTipoOC, DescOperacionContab, Margen, CompraNetoUSD, TotImptoUSD              
      --CostoBaseUSD,              
      --ROUND( CostoBaseUSD /(1 - (Margen/100)) ,0) AS VentaUSD              
     FROM              
      (              
      SELECT IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,  
      SUM(CompraNeto) AS CompraNeto,              
      SUM(IGVCosto) AS IGVCosto, Moneda,               
      --SUM(CompraNeto+IGVCosto) AS Total,               
      idTipoOC, DescOperacionContab,              
      AVG(Margen) AS Margen, sum(CompraNetoUSD) as CompraNetoUSD, sum(TotImptoUSD) as TotImptoUSD            
      --SUM(Total) as CostoBaseUSD              
      FROM              
       (              
       SELECT X.*,              
       --(SELECT AVG(MargenAplicado) FROM COTIDET cd1                           
       -- WHERE cd1.IDCAB=@IDCab AND cd1.IDProveedor=X.IDProveedorOpe  And isnull(IDDetRel,0)=0            
       -- ) AS Margen              

        --dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe) AS Margen          
		 Case When LTRIM(rtrim(@CoUbigeo_Oficina))='' Then 
			dbo.FnPromedioPonderadoMargen(@IDCab, X.IDProveedorOpe)
		 Else
			@MargenInterno
		 End AS Margen

        FROM              
   (              
        SELECT PCot.IDProveedor as IDProveedor, pint.NombreCorto as DescProveedorInternac,              
        PCot.NombreCorto as DescProveedor,       
        paPI.Descripcion as DescPaisProveedor,            
        ODS.IDOperacion_Det,              
        --isnull(ODS.NetoProgram ,ODS.CostoReal) as CompraNeto,              
                      
        --Case When ODS.IDMoneda = 'USD' Then isnull(Ods.CostoReal ,0)               
        --Else dbo.FnCambioMoneda(isnull(Ods.CostoReal ,0),'USD','SOL',SD.TC)              
        --End --* OD.NroPax                       
        --as CompraNeto,              
        --ODS.CostoReal as CompraNeto,              
                    
                    
    --    Case When isnull(NetoCotizado,0) = 0 then             
    --dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(OD.NroLiberados,0)),                           
    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,odS.IDMoneda,SD.TC)                   
    --else               
    --NetoCotizado               
    --End As CompraNeto,              
                    
                    
    --    isnull(Ods.CostoReal ,0) as CompraNetoUSD,            
                
        
 Case When ODS.IngManual=0 Then               
    Case When isnull(NetoCotizado,0) = 0 then                              
           
    dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,            
    OD.NroPax+isnull(od.NroLiberados,0)),                               
    --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,ods.IDMoneda,isnull(@TipoCambio,SD.TC))                       
    SD.CoMoneda,ods.IDMoneda,isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And     
  CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                       
    else                   
   ods.NetoCotizado        
    End               
 Else        
  ODS.NetoProgram        
 End        
        
 As CompraNeto,                                
              
 Case When ODS.IngManual=0 Then               
  Case When isnull(NetoCotizado,0) = 0 then                              
  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(SD.IDServicio_Det,OD.NroPax+isnull(od.NroLiberados,0)),                               
  --Case When isnull(SD.TC,0) = 0 Then 'USD' Else 'SOL' End,'USD',isnull(@TipoCambio,SD.TC))                       
  SD.CoMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab And CoMoneda=SD.CoMoneda),SD.TC))    
  else                   
   --dbo.FnCambioMoneda(NetoCotizado,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))                     
   dbo.FnCambioMoneda(NetoCotizado,ODS.IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
 And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))                     
  End              
        
 Else        
  --dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull(@TipoCambio,SD.TC))        
  dbo.FnCambioMoneda(ods.NetoProgram,ODS.IDMoneda,'USD',isnull((Select SsTipCam From COTICAB_TIPOSCAMBIO Where IDCab=@IDCab     
 And CoMoneda=Case When ods.IDMoneda='USD' Then SD.CoMoneda Else ods.IDMoneda End),SD.TC))        
 End        
  As CompraNetoUSD,                                
                
        
                
        --ODS.TotImpto as IGVCosto,                      
        Case When ODS.IDMoneda = 'USD' Then ODS.TotImpto               
        Else dbo.FnCambioMoneda(ODS.TotImpto,'USD','SOL',SD.TC)              
        End as IGVCosto,            
        ODS.TotImpto  AS TotImptoUSD,            
                       
                    
        ODS.IDMoneda as Moneda,            
                    
        --ODS.CostoReal+ODS.TotImpto as Total ,              
                    
        sd2.idTipoOC,              
        tox.Descripcion as DescOperacionContab,              
        --SCot.IDProveedor as IDProveedorCot,              
        --PCot.NombreCorto as DescProveedorCot,              
        --SDCot.IDServicio_Det              
        ODS.IDServicio_Det,              
        O.IDProveedor as IDProveedorOpe       
        ,SD.IDServicio              
        FROM OPERACIONES_DET_DETSERVICIOS ODS                  
        INNER JOIN OPERACIONES_DET OD ON ODS.IDOperacion_Det=OD.IDOperacion_Det               
        AND OD.IDServicio=ODS.IDServicio              
        INNER JOIN OPERACIONES O ON O.IDOperacion=OD.IDOperacion And O.IDCab=@IDCab              
        --INNER JOIN MASERVICIOS S ON OD.IDServicio=S.IDServicio AND S.IDProveedor=O.IDProveedor              
        --INNER JOIN RESERVAS_DET RD ON RD.IDReserva_Det=OD.IDReserva_Det              
                      
        LEFT JOIN COTICAB CC ON CC.IDCAB=O.IDCab              
        --LEFT JOIN MAPROVEEDORES P ON ODS.IDProveedor_Prg=P.IDProveedor              
         --INNER JOIN MASERVICIOS_DET SD ON OD.IDServicio_Det=SD.IDServicio_Det                  
                           
         inner JOIN MASERVICIOS_DET SD ON ODS.IDServicio_Det_V_Cot=SD.IDServicio_Det              
         Left JOIN MASERVICIOS_DET SD2 ON ODS.IDServicio_Det=SD2.IDServicio_Det              
                   
         LEFT join MATIPOOPERACION tox on sd2.idTipoOC=tox.IdToc              
                       
              
        --INNER JOIN MASERVICIOS_DET SDCot ON ODS.IDServicio_Det_V_Cot=SDCot.IDServicio_Det              
        INNER JOIN MASERVICIOS SCot ON SD.IDServicio=SCot.IDServicio              
        INNER JOIN MAPROVEEDORES PCot ON SCot.IDProveedor=PCot.IDProveedor             
        And (PCot.IDTipoProv in ('006') OR PCot.IDProveedor='001593' ) --MUSEO  O CONSETTUR TRANSPORTISTA          
			And (PCot.CoUbigeo_Oficina=@CoUbigeo_Oficina OR LTRIM(rtrim(@CoUbigeo_Oficina))='')

        Left Join MAPROVEEDORES pint On pint.IDProveedor=pCOT.IDProveedorInternacional              
        --Where --not ODS.IDProveedor_Prg is null and not P.NombreCorto is null              
         --ODS.IDProveedor_Prg <> '001935'--SETOURS CUSCO     
  
  Left Join MAUBIGEO ubPI On PCot.IDCiudad=ubPI.IDubigeo  
        Left Join MAUBIGEO paPI On ubPI.IDPais=paPI.IDubigeo           
                 
         Where ODS.Activo=1          
        ) AS X              
       ) AS Y              
      GROUP BY IDProveedor,DescProveedor,DescProveedorInternac,  
      DescPaisProveedor,  
      Moneda,idTipoOC,DescOperacionContab              
      ) as Z              
     ) AS XX              
    ) AS YY              
   ) AS ZZ                
  ) AS A                
      
