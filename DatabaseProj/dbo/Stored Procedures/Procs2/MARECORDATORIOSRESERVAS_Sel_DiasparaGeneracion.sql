﻿CREATE PROCEDURE [dbo].[MARECORDATORIOSRESERVAS_Sel_DiasparaGeneracion]

As
	Set NoCount On
	
	Select IDRecordatorio,Descripcion,Dias,TipoCalculoTarea From MARECORDATORIOSRESERVAS Where Activo=1
