﻿--JRF-20131105-Se deben unir los proveedores de Brazil o Argentina.
CREATE Procedure dbo.MAPROVEEDORES_Sel_CboxIDPais    
 @IDTipoProv char(3),    
 @IDPais char(6)    
As          
 Set NoCount On          
 Select '' as IDProveedor,'---' as DescProveedor       
 Union  
 Select IDProveedor, NombreCorto as DescProveedor     
 From MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad = u.IDubigeo    
 Where (IDTipoProv=@IDTipoProv OR LTRIM(RTRIM(@IDTipoProv))='')        
 and Activo = 'A' and (u.IDPais = '000008' Or u.IDPais = '000003')
 and u.IDPais <> @IDPais  -- and  (ltrim(rtrim(@IDPais))='' or u.IDPais = @IDPais) 
 and (@IDPais = '000008' Or @IDPais = '000003')
 Union
 Select IDProveedor, NombreCorto as DescProveedor     
 From MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad = u.IDubigeo    
 Where (IDTipoProv=@IDTipoProv OR LTRIM(RTRIM(@IDTipoProv))='')        
 and Activo = 'A'  and  (ltrim(rtrim(@IDPais))='' or u.IDPais = @IDPais)    
 Order by DescProveedor         
