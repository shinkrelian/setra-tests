﻿Create Procedure [dbo].[MATIPOCAMBIO_Ins]	
	@Fecha	smalldatetime,
	@ValCompra	decimal(6,3),
	@ValVenta	decimal(6,3),
	@UserMod char(4)	
As

	Set NoCount On

	INSERT INTO MATIPOCAMBIO
           (Fecha,
            ValCompra,
            ValVenta,
            UserMod)
     VALUES
           (@Fecha,
            @ValCompra,
            @ValVenta,
            @UserMod)
