﻿CREATE Procedure [dbo].[ORDEN_SERVICIO_DET_MONEDA_Upd_Estado_Multiple]
@NuOrden_Servicio int,
@CoEstado char(2),
@CoMoneda_FEgreso char(3),
@UserMod char(4)
As  
Set NoCount On  
BEGIN

	Update ORDEN_SERVICIO 
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  
	where NuOrden_Servicio=@NuOrden_Servicio

	Update ORDEN_SERVICIO_DET_MONEDA 
	Set CoEstado= @CoEstado,  
	UserMod=@UserMod,  
	FecMod=GETDATE()  
where NuOrden_Servicio=@NuOrden_Servicio  and CoMoneda=@CoMoneda_FEgreso
END;
