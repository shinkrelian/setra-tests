﻿CREATE Procedure [dbo].[MATIPOSERV_Upd]
	@IDservicio char(3),
    @Descripcion varchar(60),
    @CtaCon varchar(12),
    @Ingles varchar(60),
    @SubDiario varchar(4),
    @CtaVta varchar(12),
    @SdVta varchar(4),
    @IdFactu char(5),
    @UserMod char(4)            
As
	
	Set NoCount On	
		
	Update MATIPOSERV Set
           Descripcion=@Descripcion,
           CtaCon=Case When ltrim(rtrim(@CtaCon))='' Then Null Else @CtaCon End,
           Ingles=Case When ltrim(rtrim(@Ingles))='' Then Null Else @Ingles End,
           SubDiario=Case When ltrim(rtrim(@SubDiario))='' Then Null Else @SubDiario End,
           CtaVta=Case When ltrim(rtrim(@CtaVta))='' Then Null Else @CtaVta End,
           SdVta=Case When ltrim(rtrim(@SdVta))='' Then Null Else @SdVta End,
           IdFactu=Case When ltrim(rtrim(@IdFactu))='' Then Null Else @IdFactu End,
           UserMod=@UserMod,
           FecMod=getdate()
	Where	IDservicio=@IDservicio
