﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JRF-20140127-Agregando los filtro de clientes,file y titulo    
--JRF-20140226-Agregando la columna de Fecha Wuayna Picchu      
--JRF-20140310-Agregando la columna de Idioma y Tipo Servicio.  
--JRF-20140409-Agregando el ltrim(rtrim(@IDUbigeo))=''
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-Convert(char(10),(select Min(Dia) from cotidet cd2 where ( (ltrim(rtrim(@IDUbigeo))='' Or cd2.IDUbigeo = @IDUbigeo) and (ltrim(rtrim(@CoPais))='' Or (select top 1 IDPais from maubigeo u where u.IDubigeo=cd2.idUbigeo) = @CoPais) ) and cd2.IDCab = c.IDCab),103) as FecInicio,
CREATE Procedure dbo.REPORTE_FECHAIN_UBIGEO           
@IDCliente char(6),      
@IDFile varchar(8),      
@Titulo varchar(100),      
@IDUbigeo char(6),            
@FecInicioRango1 smalldatetime,            
@FecInicioRango2 smalldatetime,
@CoPais char(6)=''               
As            
Select ROW_NUMBER()Over(Partition By Estado order by cast(FecInicio as smalldatetime)) As Item,        
* from (          
select            
c.IDFile,
c.IDCliente,
c.Estado,
cl.RazonComercial,
c.Titulo,
idi.Siglas as SiglaIdioma,
ms.Descripcion as TipoServicio,  

--Convert(char(10),(select Min(Dia) from cotidet cd2 where (ltrim(rtrim(@IDUbigeo))='' Or cd2.IDUbigeo = @IDUbigeo) and cd2.IDCab = c.IDCab),103) as FecInicio,         
Convert(char(10),(select Min(Dia) from cotidet cd2 where ( (ltrim(rtrim(@IDUbigeo))='' Or cd2.IDUbigeo = @IDUbigeo) and (ltrim(rtrim(@CoPais))='' Or (select top 1 IDPais from maubigeo u where u.IDubigeo=cd2.idUbigeo) = @CoPais) ) and cd2.IDCab = c.IDCab),103) as FecInicio,         
   
case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00280')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00280')             
Else             
 case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00184')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00184')            
   Else            
 case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00185')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00185')            
   Else            
 case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00282')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00282')            
   Else            
 case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00283')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00283')            
   Else            
 case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00284')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00284')            
   Else             
 case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00285')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00285')            
   Else            
 case when (select COUNT(*) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00381')= 1             
  then (select convert(char(10),Dia,103) from COTIDET cd where cd.IDCAB=c.IDCAB and IDServicio = 'CUZ00381')            
   Else            
 ''            
   End             
  End            
 End End End End End End as FechaMP   ,    
 Case When exists(Select IDDET from COTIDET cd LEFT JOIN MASERVICIOS s On cd.IDServicio = s.IDServicio where cd.IDCAB = c.IDCAB and cd.IDProveedor = '000544' and lower(s.Descripcion) like '%wayna%')    
 Then (Select Convert(Char(10),MIN(cd.Dia),103) from COTIDET cd LEFT JOIN MASERVICIOS s On cd.IDServicio = s.IDServicio where cd.IDCAB = c.IDCAB and cd.IDProveedor = '000544' and lower(s.Descripcion) like '%wayna%')    
 Else '' End as FechaWP    
,c.NroPax,ISNULL(c.NroLiberados,0) as NroLiberados,u.Nombre            
from COTICAB c --Left Join OPERACIONES o On c.IDCAB = o.IDCab            
Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente            
Left Join MAUSUARIOS u On c.IDUsuarioRes = u.IDUsuario            
Left Join MAIDIOMAS idi On c.IDIdioma = idi.IDidioma  
Left Join MAMODOSERVICIO ms On c.TipoServicio = ms.CodModoServ  
where c.Estado = 'A' AND c.IDUsuarioOpe is not null and c.IDUsuarioRes is not null         
and Exists (select IDCAB from OPERACIONES o3 where o3.IDCab = c.IDCAB)        
 )as X             
where (ltrim(rtrim(@IDCliente))='' Or X.IDCliente = @IDCliente) and      
   (ltrim(rtrim(@IDFile))='' Or X.IDFile like @IDFile+'%') and      
   (ltrim(rtrim(@Titulo))='' Or X.Titulo like @Titulo+'%') and      
((convert(char(10),@FecInicioRango1,103)='01/01/1900' and convert(char(10),@FecInicioRango2,103)='01/01/1900') or           
X.FecInicio Between @FecInicioRango1 and @FecInicioRango2)          
Order by cast(X.FecInicio as smalldatetime)          

