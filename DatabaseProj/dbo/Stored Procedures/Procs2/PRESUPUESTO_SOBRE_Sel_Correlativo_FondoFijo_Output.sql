﻿

--HLF-20160202-select @pNuCodigo_ER=isnull(NuCodigo_ER,'')+'-01-...
--HLF-20160216-Utilizando FnFondoFijo_ER
CREATE Procedure [dbo].[PRESUPUESTO_SOBRE_Sel_Correlativo_FondoFijo_Output]    
	@CoProveedor char(6),
	@pNuCodigo_ER varchar(30) output
 as
	Set Nocount On    
	Set @pNuCodigo_ER=''
 
	--Declare @NuFondoFijo int=0

	--If @CoProveedor='001554' --Set. Lima
	--	Begin
	--	Set @NuFondoFijo=isnull((select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoTipoFondoFijo='ER' 
	--	and NuCodigo_ER='ER40897783' ORDER BY CAST(COSAP AS INT) DESC),0)

	--	If @NuFondoFijo<>0
	--		select @pNuCodigo_ER=isnull(NuCodigo_ER,'')+'-'+isnull(CoSap,'') from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo

	--	End
	--If @CoProveedor='000544' --Set. Cusco
	--	Begin
	--	Set @NuFondoFijo=isnull((select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoTipoFondoFijo='ER' 
	--	and NuCodigo_ER='ER44587677' ORDER BY CAST(COSAP AS INT) DESC),0)

	--	If @NuFondoFijo<>0
	--	--select @pNuCodigo_ER=isnull(NuCodigo_ER,'')+'-'+isnull(CoSap,'') from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo
	--		select @pNuCodigo_ER=isnull(NuCodigo_ER,'')+'-01-'+isnull(CoSap,'') from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo

	--	End
	
	Set @pNuCodigo_ER=dbo.FnFondoFijo_ER (@CoProveedor)
