﻿
Create Procedure [dbo].[MAPROVEEDORES_SelxProvInternoOutput]    
	@IDProveedor char(6),
	@pbInterno bit output
As    
 Set NoCount On    
 
 Set @pbInterno=0
 
 If Exists(Select idproveedor from MAPROVEEDORES      
	where IDProveedor=@IDProveedor And
	IDTipoOper='I' And IDTipoProv='003' And Activo='A'  )
	Set @pbInterno=1	
 