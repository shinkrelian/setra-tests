﻿--HLF-20120921-Or LTRIM(rtrim(@IDTipoProv))='' )  
--JRF-20140530-Agregar el Operador Internacional
--JRF-20151202-Case When p.IDProveedorInternacional is not null then IsNull(pInt.Margen,0) else IsNull(p.Margen,0) End..
CREATE Procedure [dbo].[MAPROVEEDORES_Sel_ListAyuda]    
 @IDTipoProv char(3),    
 @RazonSocial varchar(100),    
 @IDCiudad char(6)='',    
 @IDCat char(3),    
 @Categoria tinyint    
As    
 Set NoCount On    
    
 SELECT Distinct * FROM    
 (    
 Select     
 --Case When @IDTipoProv='001' Then p.NombreCorto Else p.RazonSocial End as Descripcion,    
 p.NombreCorto as Descripcion,    
 p.IDProveedor as Codigo,     
 p.IDTipoProv as CodTipo, t.Descripcion as DescTipo, p.IDCiudad,u.Descripcion as DescCiudad, 
 u.IDPais,
 Case When p.IDProveedorInternacional is not null then IsNull(pInt.Margen,0) else IsNull(p.Margen,0) End as Margen,
 Isnull(pInt.NombreCorto,'') as OperadorInt    
 From MAPROVEEDORES p Left Join MATIPOPROVEEDOR t On p.IDTipoProv=t.IDTipoProv    
 Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo    
 Left Join MASERVICIOS s On p.IDProveedor=s.IDProveedor    
 Left Join MAPROVEEDORES pInt On p.IDProveedorInternacional = pInt.IDProveedor
 Where     
 (p.IDTipoProv=@IDTipoProv Or LTRIM(rtrim(@IDTipoProv))='' )  
 And (p.IDCiudad=@IDCiudad Or LTRIM(rtrim(@IDCiudad))='')    
 And (s.IDCat=@IDCat Or LTRIM(rtrim(@IDCat))='')    
 And (s.Categoria=@Categoria OR @Categoria=0) And p.Activo = 'A'  
 ) AS X    
 Where (Descripcion Like '%'+@RazonSocial+'%' Or LTRIM(rtrim(@RazonSocial))='')    
 Order by Descripcion    
