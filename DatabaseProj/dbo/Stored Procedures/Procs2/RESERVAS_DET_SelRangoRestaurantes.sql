﻿--JRF-20130911-Cambio de Cuerpo. Consultar por Fecha Maxima y Minima de la reserva.  
--JRF-20140517-Agregar la hora del servicio
--JHD-20150602-Agregar condición para que se muestren los Hoteles con Plan Alimenticio
--JRF-20160412- ...And r.Anulado=0 And r.Estado <> 'XL'..
CREATE Procedure dbo.RESERVAS_DET_SelRangoRestaurantes    
@IDReserva int,    
@IDCab int    
As    
 Set NoCount On    
 Declare @FechaIn smalldatetime, @FechaMx smalldatetime--, @IDUbigeo char(6)

 Select @FechaIn = cast(convert(char(10),Min(dia),103) as smalldatetime),
		@FechaMx=cast(convert(char(10),Max(dia),103) as smalldatetime)
 from RESERVAS_DET 
 Where IDReserva=@IDReserva And Anulado=0
 
 --Select @IDUbigeo=p.IDCiudad
 --from RESERVAS r Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
 --Where IDReserva=@IDReserva
   
 select * from (  
 select Distinct p.NombreCorto as DescProveedores , convert(char(10),rd.dia,103) as FechaIN ,convert(char(5),rd.dia,108) as HoraIN 
 from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva =r.IDReserva and rd.Anulado = 0  
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det 
 Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor  
 --where p.IDTipoProv = '002' and r.IDCab = @IDCab and r.IDReserva <> @IDReserva and rd.Dia between @FechaIn and @FechaMx)  
 where (p.IDTipoProv = '002' or (p.IDTipoProv='001' and sd.PlanAlimenticio=1)) and r.IDCab = @IDCab and r.IDReserva <> @IDReserva 
 And r.Anulado=0 And r.Estado <> 'XL' --And rd.IDubigeo=@IDUbigeo
 and rd.Dia between @FechaIn and @FechaMx)  
 As X  
 Order by cast(convert(char(10),X.FechaIN,103) as smalldatetime)   
