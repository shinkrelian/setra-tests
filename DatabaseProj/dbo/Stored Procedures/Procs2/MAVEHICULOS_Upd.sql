﻿Create Procedure dbo.MAVEHICULOS_Upd
@NuVehiculo smallint,
@QtDe tinyint,
@QtHasta tinyint,
@NoVehiculoCitado varchar(50),
@QtCapacidad tinyint,
@FlOtraCiudad bit,
@Usermod char(4)
As
	Set NoCount On
	
	Update dbo.MAVEHICULOS
		set QtDe = @QtDe,
			QtHasta = @QtHasta,
			NoVehiculoCitado = @NoVehiculoCitado,
			QtCapacidad = @QtCapacidad,
			FlOtraCiudad = @FlOtraCiudad,
			Fecmod = GetDate()
	Where NuVehiculo = @NuVehiculo
