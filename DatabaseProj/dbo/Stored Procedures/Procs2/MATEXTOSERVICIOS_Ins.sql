﻿CREATE Procedure [dbo].[MATEXTOSERVICIOS_Ins]
	@IDServicio	char(8),
	@IDIdioma	varchar(12),
	@Titulo	varchar(100),
	@Texto	text,
	@UserMod	char(4)
As
	Set NoCount On
	
	If Not Exists(Select IDServicio From MATEXTOSERVICIOS Where IDServicio=@IDServicio 
		And @IDIdioma=IDIdioma)
	
		Insert Into MATEXTOSERVICIOS (IDServicio, IDIdioma, 
		Titulo,Texto, UserMod)
		Values(@IDServicio, @IDIdioma, 
		Case When ltrim(rtrim(@Titulo))='' Then Null Else @Titulo End,
		@Texto, @UserMod)
	Else
		Update MATEXTOSERVICIOS Set Texto=@Texto, 
		Titulo=Case When ltrim(rtrim(@Titulo))='' Then Null Else @Titulo End,
		UserMod=@UserMod, FechaMod=getdate()
		Where IDServicio=@IDServicio And IDIdioma=@IDIdioma
