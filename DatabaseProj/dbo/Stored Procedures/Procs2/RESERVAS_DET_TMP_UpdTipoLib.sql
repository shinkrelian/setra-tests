﻿  
Create Procedure dbo.RESERVAS_DET_TMP_UpdTipoLib  
 @IDCab int,  
 @UserMod char(4)   
As  
 Set Nocount On  
   
 Update RESERVAS_DET_TMP Set RESERVAS_DET_TMP.Tipo_Lib=Null, 
 RESERVAS_DET_TMP.UserMod=@UserMod, RESERVAS_DET_TMP.FecMod=GETDATE()  
 Where ISNULL(RESERVAS_DET_TMP.NroLiberados,0)=0  
 And RESERVAS_DET_TMP.IDReserva In (Select IDReserva From RESERVAS Where IDCab=@IDCab)  
 
