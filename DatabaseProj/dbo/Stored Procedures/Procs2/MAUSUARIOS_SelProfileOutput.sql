﻿Create Procedure dbo.MAUSUARIOS_SelProfileOutput
@NameProfile varchar(20),
@pProfileExists bit output
As
	Set NoCount On
	set @pProfileExists = 0
	if Exists(select profile_id from msdb.dbo.sysmail_profile where name=@NameProfile)
		set @pProfileExists = 1
