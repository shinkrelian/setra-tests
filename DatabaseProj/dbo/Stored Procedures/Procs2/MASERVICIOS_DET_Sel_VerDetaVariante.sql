﻿Create Procedure dbo.MASERVICIOS_DET_Sel_VerDetaVariante
	@IDServicio_Det int
As
	Set Nocount On
	
	Select VerDetaTipo,DetaTipo 
	From MASERVICIOS_DET
	Where IDServicio_Det = @IDServicio_Det
