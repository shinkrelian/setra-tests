﻿--HLF-20151027-@IDProveedor char(6)... And IDProveedor=@IDProveedor 
--JRF-20151126-@TourConductor
CREATE Procedure dbo.PRESUPUESTO_SOBRE_SelUltimoEstado
	@IDCab int,
	@IDProveedor char(6),
	@TourConductor bit,
	@pCoEstado char(2) output
As
	Set Nocount on

	Set @pCoEstado=''
	SELECT @pCoEstado=isnull(max(CoEstado),'') FROM PRESUPUESTO_SOBRE WHERE IDCab=@IDCab
		And IDProveedor=@IDProveedor And IsNull(FlTourConductor,0)=@TourConductor
