﻿Create PROCEDURE DBO.MATIPOSCAMBIO_USD_Sel_List
@FecRango1 smalldatetime,
@FecRango2 smalldatetime
as
Set NoCount On
Select tcu.FeTipCam,tcu.CoMoneda,m.Descripcion as Moneda,m.Simbolo,tcu.SsTipCam
from MATIPOSCAMBIO_USD tcu Left Join MAMONEDAS m On tcu.CoMoneda=m.IDMoneda
Where (CONVERT(char(10),@FecRango1,103)='01/01/1900' Or
	   tcu.FeTipCam between @FecRango1 and @FecRango2)
