﻿CREATE Procedure [dbo].[MAUSUARIOS_Sel_Lvw]
	@ID	char(4),
	@Descripcion varchar(20)
As
	Set NoCount On
	
	Select Usuario	
	From MAUSUARIOS
	Where (Usuario Like '%'+@Descripcion+'%')
	And (IDUsuario <>@ID Or ltrim(rtrim(@ID))='')
	Order by Usuario
