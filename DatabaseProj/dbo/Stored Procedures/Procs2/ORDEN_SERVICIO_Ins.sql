﻿--JRF-20160330-Agregando @IDProveedorSetours
--JHD-20160405-Se agregó campo FeCreacion=getdate()
CREATE Procedure [dbo].[ORDEN_SERVICIO_Ins]
@IDCab int,
@IDProveedor char(6),
@IDUsuarioResponsable char(4),
@NuVehiculoProg smallint,
@CoOrden_Servicio char(14),
@IDProveedorSetours char(6),
@UserMod char(4),
@pNuOrden_Servicio int Output
As
Set NoCount On

Declare @NuOrden_Servicio int = (select Isnull(Max(NuOrden_Servicio),0)+1 from ORDEN_SERVICIO)
INSERT INTO[dbo].[ORDEN_SERVICIO]
           ([NuOrden_Servicio]
           ,[IDCab]
           ,[IDProveedor]
           ,[IDUsuarioResponsable]
           ,[NuVehiculoProg]
           ,[CoOrden_Servicio]
		   ,[IDProveedorSetours]
           ,[UserMod]
           ,[FecMod]
		   ,FeCreacion)
     VALUES
           (@NuOrden_Servicio
           ,@IDCab
           ,@IDProveedor
           ,@IDUsuarioResponsable
           ,Case When @NuVehiculoProg = 0 then Null Else @NuVehiculoProg End
           ,@CoOrden_Servicio
		   ,Case When Ltrim(Rtrim(@IDProveedorSetours))='' then Null Else @IDProveedorSetours End
           ,@UserMod
           ,GETDATE()
		   ,GETDATE())
Set @pNuOrden_Servicio = @NuOrden_Servicio
