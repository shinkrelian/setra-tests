﻿CREATE Procedure [Dbo].[MASERVICIOS_Upd_MargenxIDProveedor]
@IDProveedor char(6),
@Comision numeric(6,2),
@UserMod char(4)
As
	Set NoCount On
UPDATE MASERVICIOS 
	Set Comision=@Comision, UserMod=@UserMod, FecMod=GETDATE()
	Where IDProveedor=@IDProveedor And UpdComision=0
