﻿--JRF-20160219-Agregar p.CoEstado as CoEstadoPedido
--JRF-20160323-...Inner Join COTICAB co On c.IDCab=CO.IDCAB
CREATE Procedure [dbo].[PEDIDO_Sel_ListxCoOutlookAddIn]
@CoOutlook varchar(max)
As
select Distinct IsNull(co.NuPedInt,0) as NuPedidoInt,uVent.Nombre +' '+IsNull(uVent.TxApellidos,'') As EjeVentPedidos,
	   Convert(char(10),p.FePedido,103) as FechaPedido,p.NuPedido As NroPedido,cl.RazonComercial as DescCliente,
	   TxTitulo as TituloPedido,p.CoEstado as CoEstadoPedido,
	   Case p.CoEstado
	    When 'PD' Then 'PENDIENTE'
		When 'CE' Then 'COTIZACION ENVIADA'
		When 'AC' Then 'ACEPTADO'
		When 'AN' Then 'ANULADO' 
	   End as EstadoPedidos,0 as NuCorreo
From .BDCORREOSETRA..CORREOFILE c Inner Join COTICAB co On c.IDCab=CO.IDCAB
Inner Join PEDIDO p On co.NuPedInt=p.NuPedInt
Left Join MAUSUARIOS uVent On co.IDUsuario = uVent.IDUsuario
Left Join MACLIENTES cl On co.IDCliente=cl.IDCliente
where c.CoCorreoOutlook = @CoOutlook
