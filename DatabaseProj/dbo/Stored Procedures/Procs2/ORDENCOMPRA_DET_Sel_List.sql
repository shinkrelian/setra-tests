﻿
--JHD-20150313- Se muestra cantidad convertido a int
CREATE PROCEDURE [dbo].[ORDENCOMPRA_DET_Sel_List]
 @NuOrdComInt_Det int=0,                    
 @NuOrdComInt int=0                    
as
SELECT     NuOrdComInt_Det,
			--NuOrdComInt,
			TxServicio,
			SSCantidad=cast(round(SSCantidad,0) as int),
			SSPrecUnit,
			SSTotal
FROM         ORDENCOMPRA_DET
WHERE     (NuOrdComInt_Det = @NuOrdComInt_Det or @NuOrdComInt_Det=0)
AND (NuOrdComInt = @NuOrdComInt or @NuOrdComInt=0);
