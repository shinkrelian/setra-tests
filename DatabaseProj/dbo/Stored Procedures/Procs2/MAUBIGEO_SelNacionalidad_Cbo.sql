﻿CREATE Procedure [dbo].[MAUBIGEO_SelNacionalidad_Cbo]  
  
As  
 Set NoCount On  
  
 Select *  From  
 (  
 Select IDubigeo, Descripcion AS Nacionalidad, 0 as Ord  
 From MAUBIGEO  
 Where Descripcion='NO APLICA'  
   
 Union  
   
 Select IDubigeo, Descripcion, 1 as Ord From MAUBIGEO  
 Where TipoUbig='PAIS' -- And Not Gentilicio Is Null  
   
 ) as X  
 Order by Ord, Nacionalidad
