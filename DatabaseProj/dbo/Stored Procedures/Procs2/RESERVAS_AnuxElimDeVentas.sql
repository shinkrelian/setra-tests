﻿--JRF-20150528-Pasar a estado XL solo los que ya no hallan servicios.
CREATE Procedure dbo.RESERVAS_AnuxElimDeVentas
@IDCab int,
@UserMod char(4)
As
Begin
Set NoCount On
Update RESERVAS
	   Set Estado=Case When Not Exists(select IDDet from COTIDET where IDCAB=@IDCab And IDProveedor=r.IDProveedor) Then 'XL' Else r.Estado End,
		   UserMod=@UserMod,
		   FecMod=GETDATE()
from RESERVAS r --Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
Inner Join RESERVAS_DET rd On r.IDReserva=rd.IDReserva
Left Join COTIDET cd On rd.IDDet=cd.IDDET
Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
where r.IDCab=@IDCab and r.Anulado=0 and Not r.Estado In('XL') And r.IDReservaProtecc is null And p.IDTipoProv <> '001'
and (cd.IDDET is null and 
Not Exists(select rd2.IDReserva_Det from RESERVAS_DET rd2 
where r.IDReserva=rd2.IDReserva and rd2.IDReserva_Det<>rd.IDReserva_Det and rd2.Anulado=0))

Update RESERVAS
	  Set IDReservaProtecc = Null,
		  UserMod=@UserMod,
		  FecMod=GETDATE()
Where IDCab=@IDCab And IDReservaProtecc is not null
	And IDProveedor In (select Distinct IDProveedor from COTIDET where IDCAB=@IDCab)
End
