﻿

CREATE PROCEDURE dbo.PRESUPUESTO_SOBRE_SelxProveedorOutput
	@IDCab int,
	@IDProveedor char(6),
	@pCoEstado char(2) output
AS
	Set Nocount On        
	Set @pCoEstado=''
	
	Select @pCoEstado=CoEstado
	From PRESUPUESTO_SOBRE Where IDCab=@IDCab And IDProveedor=@IDProveedor
	