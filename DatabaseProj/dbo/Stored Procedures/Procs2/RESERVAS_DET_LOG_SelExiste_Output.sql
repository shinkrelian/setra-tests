﻿Create Procedure dbo.RESERVAS_DET_LOG_SelExiste_Output
	@IDCab	int,
	@IDProveedor  char(6),
	@Accion	char(1),
	@pExists	bit output
As
	Set Nocount On

	Set @pExists=0
	
	If Exists(Select IDDet From RESERVAS_DET_LOG l1 Inner Join RESERVAS r1 On r1.IDReserva=l1.IDReserva   
	Where  r1.IDProveedor=@IDProveedor  
	And r1.IDCab=@IDCab And l1.Atendido=0 And l1.Accion=@Accion)
		
		Set @pExists=1
		
