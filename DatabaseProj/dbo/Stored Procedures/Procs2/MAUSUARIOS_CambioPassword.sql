﻿--JRF-20121031- Nuevo campo al cambiar la contraseña.
CREATE Procedure [dbo].[MAUSUARIOS_CambioPassword]  
@IdUsuario char(4),  
@PasswordNuev varchar(max),
@CambioPassword bit
As  
 Set NoCount On  
 Update MAUSUARIOS  
  Set Password=@PasswordNuev,  
	  CambioPassword=@CambioPassword, 
   UserMod=@IdUsuario,  
   FecMod=GETDATE()  
 Where IDUsuario= @IdUsuario   
