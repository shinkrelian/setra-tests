﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JHD-20150415 - set @NuVehiculo=isnull((select MAX(NuVehiculo) from MAVEHICULOS_TRANSPORTE),0)+1
--create
CREATE Procedure [dbo].[MAVEHICULOS_TRANSPORTE_Ins]
--@NuVehiculo_Anterior smallint,
@CoTipo smallint,
@QtDe tinyint,
@QtHasta tinyint,
@NoVehiculoCitado varchar(200),
@QtCapacidad tinyint,
@FlOtraCiudad bit,
@CoUbigeo char(6)='',
@CoMercado smallint=0,
@UserMod char(4),
@FlReservas bit=0,
@FlProgramacion bit=0
As
	
BEGIN

Set NoCount On
	Declare @NuVehiculo smallint
	--Execute dbo.Correlativo_SelOutput 'MAVEHICULOS_TRANSPORTE',1,10,@NuVehiculo output
	set @NuVehiculo=isnull((select MAX(NuVehiculo) from MAVEHICULOS_TRANSPORTE),0)+1
	
	Insert Into [dbo].[MAVEHICULOS_TRANSPORTE]
		(
			[NuVehiculo],
 			--[NuVehiculo_Anterior],
 			[CoTipo],
 			[QtDe],
 			[QtHasta],
 			[NoVehiculoCitado],
 			[QtCapacidad],
 			[FlOtraCiudad],
 			[CoUbigeo],
 			[CoMercado],
 			[Usermod],
			FlReservas,
			FlProgramacion
 		)
	Values
		(
			@NuVehiculo,
 			--@NuVehiculo_Anterior,
 			@CoTipo,
 			@QtDe,
 			@QtHasta,
 			@NoVehiculoCitado,
 			--@QtCapacidad,
			case when @QtCapacidad=0 then Null Else @QtCapacidad End,
 			@FlOtraCiudad,
			case when LTRIM(RTRIM(@CoUbigeo))='' then Null Else @CoUbigeo End,
			case when @CoMercado=0 then Null Else @CoMercado End,
 			--@CoMercado,
 			@Usermod,
			@FlReservas,
			@FlProgramacion
 		)
END
