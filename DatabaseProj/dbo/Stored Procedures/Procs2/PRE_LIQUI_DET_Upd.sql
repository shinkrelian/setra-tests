﻿
--HLF-20130116-@IDNotaVenta a char(8)
CREATE Procedure dbo.PRE_LIQUI_DET_Upd
@IDNotaVenta char(8),
@IDPreLiquiDet tinyint,
@Descripcion varchar(max),
@IDTipoOC char(3),
@SubTotal numeric(8,2),
@TotalIGV numeric(8,2),
@Total numeric(8,2),
@UserMod char(4)
As
	Set NoCount On
	UPDATE [dbo].[PRE_LIQUI_DET]
	   SET [Descripcion] = @Descripcion
		  ,[IDTipoOC]= @IDTipoOC 
		  ,[SubTotal] = @SubTotal
		  ,[TotalIGV] = @TotalIGV
		  ,[Total] = @Total
		  ,[UserMod] = @UserMod
		  ,[FecMod] = GetDate()
	 WHERE IDNotaVenta = @IDNotaVenta And IDPreLiquiDet=@IDPreLiquiDet

