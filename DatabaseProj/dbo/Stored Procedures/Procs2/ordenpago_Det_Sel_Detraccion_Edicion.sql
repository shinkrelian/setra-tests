﻿create procedure ordenpago_Det_Sel_Detraccion_Edicion
 @IDOrdPag int,  
 @IDOrdPag_Det int,
 @SSDetraccion numeric(12,2)
as
begin
declare @FlDetraccion as bit = 0
declare @IDMoneda as char(3)=''
declare @TCambio as numeric(5,2)=0
declare @Total numeric(12,2)=0
declare @FechaEmision datetime,
@SSDetraccion_SOL as numeric(12,2)=0,
@SSDetraccion_USD as numeric(12,2)=0,
@SSDetraccion_Cab as numeric(12,2)=0
--set @IDMoneda=(select idmoneda from ordenpago where IDOrdPag=@IDOrdPag)
select @IDMoneda=idmoneda,@TCambio=TCambio,@Total=SsMontoTotal,@FechaEmision=FechaEmision,@SSDetraccion_Cab=isnull(SSDetraccion,0) from ordenpago where IDOrdPag=@IDOrdPag

declare @Total_Det numeric(12,2)=0
--select @SSDetraccion=ssdetr from ORDENPAGO_DET 

		if @IDMoneda='SOL'
			begin
				set @SSDetraccion_SOL=@SSDetraccion
				set @SSDetraccion_USD=0
			end
		else 
			if @IDMoneda='USD'
				begin
					set @SSDetraccion_USD=@SSDetraccion
					if isnull(@TCambio,0)=0
						--set @SSDetraccion=dbo.FnCambioMoneda(@Total,@IDMoneda,'SOL',ISNULL(@TCambio,isnull((select ValVenta from matipocambio where fecha=@FechaEmision),0)))
						set @SSDetraccion_SOL=(dbo.FnCambioMoneda(@SSDetraccion,@IDMoneda,'SOL',isnull((select ValVenta from matipocambio where fecha=@FechaEmision),0)))
					else
						set @SSDetraccion_SOL=(@SSDetraccion*@TCambio)
				end

select SSDetraccion=isnull(@SSDetraccion_SOL,0),SSDetraccion_USD=isnull(@SSDetraccion_USD,0)
end;
