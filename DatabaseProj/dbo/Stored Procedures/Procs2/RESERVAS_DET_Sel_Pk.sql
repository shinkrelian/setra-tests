﻿Create Procedure dbo.RESERVAS_DET_Sel_Pk
	@IDReserva_Det	int
As
	Set NoCount On
	
	Select rd.*,
	--s.dias, sd.ConAlojamiento, 
	cd.SSCR, p.IDProveedor, p.IDTipoProv,
	--sd.PlanAlimenticio, 
	cd.IncGuia
	From RESERVAS_DET rd 
	Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
	Left Join COTIDET cd On rd.IDDet=cd.IDDET
	Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
	Where rd.IDReserva_Det=@IDReserva_Det
	
