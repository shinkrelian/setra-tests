﻿
Create Procedure dbo.RESERVAS_DET_PAX_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM RESERVAS_DET_PAX 
	WHERE IDReserva_Det IN 
			(SELECT IDReserva_Det FROM RESERVAS_DET WHERE IDReserva IN 
				(SELECT IDReserva FROM RESERVAS WHERE IDCab = @IDCab))
				
