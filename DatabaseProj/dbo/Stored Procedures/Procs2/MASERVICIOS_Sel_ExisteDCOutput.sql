﻿Create Procedure MASERVICIOS_Sel_ExisteDCOutput  
@IdUbigeo char(6),  
@pblnExiste bit output  
As  
  
 Set NoCount On  
 if exists(select IDubigeo from MAUBIGEO Where IDubigeo=@IdUbigeo And DC is not null)  
  set @pblnExiste = 1  
 else  
  set @pblnExiste = 0  
