﻿
------------------------------------------------------------------------------------------------------

--alter

--JHD-20141127-Agregar el Nuevo campo de TxPisos  
CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_Ins]
	@CoServicio char(8),
	@CoConcepto tinyint,
	@FlServicio bit=null,
	@FlCosto bit=null,
	@CoTipoMenu char(3)='',
	@FlFijoPortable bit=null,
	@FlPortable bit=null,
	@CoTipoBanio char(3)='',
	@CoEstadoHab char(3)='',
	@TxPisos varchar(20),
	@UserMod char(4)=''
AS
BEGIN
	Insert Into [dbo].[MASERVICIOS_HOTEL]
		(
			[CoServicio],
 			[CoConcepto],
 			[FlServicio],
 			[FlCosto],
 			[CoTipoMenu],
 			[FlFijoPortable],
 			FlPortable,
 			[CoTipoBanio],
 			[CoEstadoHab],
 			TxPisos,
 			[UserMod]
 		)
	Values
		(
			@CoServicio,
 			@CoConcepto,
 			@FlServicio,
 			@FlCosto,
 			Case When @CoTipoMenu='' Then Null Else @CoTipoMenu End,
 			@FlFijoPortable,
 			@FlPortable,
 			Case When @CoTipoBanio='' Then Null Else @CoTipoBanio End,
 			Case When @CoEstadoHab='' Then Null Else @CoEstadoHab End,
 			Case When @TxPisos='' Then Null Else @TxPisos End,
			Case When @UserMod='' Then Null Else @UserMod End
 		)
END;
