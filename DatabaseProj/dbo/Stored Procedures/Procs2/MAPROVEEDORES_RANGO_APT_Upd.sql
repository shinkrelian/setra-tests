﻿CREATE Procedure [dbo].[MAPROVEEDORES_RANGO_APT_Upd]  
 @Correlativo tinyint,  
    @IDProveedor int,  
	@NuAnio CHAR(4),
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(10,4),  
    @UserMod char(4)  
As  
 Set NoCount On  
   
 UPDATE MAPROVEEDORES_RANGO_APT  
           Set PaxDesde=@PaxDesde   
           ,PaxHasta=@PaxHasta  
           ,Monto=@Monto  
           ,UserMod=@UserMod  
             
     WHERE  
           Correlativo=@Correlativo And  
           (IDProveedor=@IDProveedor)  
		   and NuAnio=@NuAnio
