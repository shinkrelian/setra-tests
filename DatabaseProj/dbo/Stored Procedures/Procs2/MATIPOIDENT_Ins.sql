﻿--MLL-20140522- Nuevo Campo CodPeruRail

CREATE Procedure [dbo].[MATIPOIDENT_Ins]	
	@Descripcion varchar(60),
	@UserMod char(4),
	@CodPeruRail char(3) ,
	@pIDIdentidad char(3) output
As

	Set NoCount On
	Declare @IDIdentidad char(3)	
	Exec Correlativo_SelOutput 'MATIPOIDENT',1,3,@IDIdentidad output
	
	INSERT INTO MATIPOIDENT
           ([IDIdentidad]
           ,[Descripcion]           
           ,[UserMod]
           ,CodPeruRail)
     VALUES
           (@IDIdentidad,
            @Descripcion,           
            @UserMod,
            @CodPeruRail)
            
	Set @pIDIdentidad=@IDIdentidad
