﻿Create Procedure dbo.MASERVICIOS_COPIA_COTICAB_UpdIDServicio_DetNuevo
@IDCab int,
@IDServicio_Det int,
@IDServicio_DetNuevo int
As
Set NoCount On
Update MASERVICIOS_COPIA_COTICAB Set IDServicio_DetNuevo=@IDServicio_DetNuevo
Where IDCab = @IDCab And IDServicio_Det=@IDServicio_Det
