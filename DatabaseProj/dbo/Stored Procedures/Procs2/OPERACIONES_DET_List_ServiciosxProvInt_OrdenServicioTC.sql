﻿--JRF-20160308-Declare @IDTipoHon char(3) = IsNull((select CoTipoHon from COTICAB_TOURCONDUCTOR where IDCab=@IDCab),'')
CREATE Procedure dbo.OPERACIONES_DET_List_ServiciosxProvInt_OrdenServicioTC
@IDCab int
As
Set NoCount On
Declare @IDTipoHon char(3) = IsNull((select CoTipoHon from COTICAB_TOURCONDUCTOR where IDCab=@IDCab),'')

SELECT Dia,Hora, IDOperacion, DescServicio, IDTipoServ, Variante,                                     
NroPax, PaxmasLiberados, IDServicio, IDServicio_Det ,                                            
Y.IDCiudad,Y.IDTipoProv,Y.DescTipoServ,Y.IDProveedor,                        
ISNULL(CorreoReservas,'') As CorreoReservas,IDPais,                                              
Desayuno,Lonche,Almuerzo,Cena,Transfer,                                             
IDOperacion_Det,                                            
NroLiberados, Tipo_Lib, TipoPax,Isnull(TotalCotizado,0) as TotalCotizado,Isnull(TotalProgramado,0)as TotalProgramado,       
EstadoGuia,                    
Case When isnull(IDTrasladistaProgramado,'')='' Then 'NV' Else 'PA' End EstadoTrasladista,                        
EstadoVehiculo,EstadoTransportista,                        
ObservVoucher,ObservBiblia,ObservInterno,                                          
IDGuiaProgramado,IDTrasladistaProgramado,Case When ltrim(rtrim(IDVehiculoProgramado)) = '' then 0 else IDVehiculoProgramado End as IDVehiculoProgramado,    
IDTransportistaProgramado,                                            
ISNULL(Case When ISNULL(PG.Email3,'')='' Then PG.Email4 Else PG.Email3 End,'') as CorreoReservasGuiaPrg,                                           
ISNULL((Select Correo From MAUSUARIOS Where IDUsuario=Y.IDTrasladistaProgramado),'') as CorreoReservasTrasladistaPrg,                          
ISNULL(Case When ISNULL(PT.Email3,'')='' Then PT.Email4 Else PT.Email3 End,'') as CorreoReservasTranspPrg,                                          
GuiaProgramado,                    
ISNULL((Select Nombre From MAUSUARIOS Where IDUsuario=Y.IDTrasladistaProgramado),'') as TrasladistaProgramado,                    
VehiculoProgramado,TransportistaProgramado,                       
IDIdioma, MotivoEspecial,                            
'' as Del,                                                                            
Case When Exists(Select IDDet From RESERVAS_DET_LOG l1 Inner Join RESERVAS r1 On r1.IDReserva=l1.IDReserva                             
Where  r1.IDProveedor=Y.IDProveedor  And l1.IDServicio_Det=Y.IDServicio_Det                                                  
And r1.IDCab=@IdCab And l1.Atendido=0 And Accion='M') Then                                            
'M'                                                   
Else                                
Case When Exists(Select IDDet From RESERVAS_DET_LOG l1 Inner Join RESERVAS r1 On r1.IDReserva=l1.IDReserva                             
Where  r1.IDProveedor=Y.IDProveedor And l1.IDServicio_Det=Y.IDServicio_Det                          
And r1.IDCab=@IdCab And l1.Atendido=0 And Accion='B') Then                                            
'E'                                                    
Else                                                    
'O'                                                    
End     
End As CambiosReservasLog      ,ISNULL(CodigoPnr,'') as CodigoPnr  ,NombreTourConductor
                
                          
FROM (                                            
                             
SELECT DiaFormatFecha,                                    
Dia,Hora, IDOperacion, DescServicio, Variante, IDTipoServ,IDServicio,                                            
IDCiudad,IDTipoProv,DescTipoServ,IDProveedor,CorreoReservas,IDPais,                                              
Desayuno,Lonche,Almuerzo,Cena,Transfer,                                           
--Anio, Variante,                                            
IDOperacion_Det,                                            
NroPax, PaxmasLiberados, NroLiberados, Tipo_Lib, TipoPax,TotalCotizado,TotalProgramado,                                            
                                      
Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                            
Else                                            
IsNull(                                            
(Select Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                                             
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                    
Where od1.IDOperacion_Det=X.IDOperacion_Det                            
                 
And od1.IDServicio=X.IDServicio                            
)-- and od1.IDServicio_Det=x.IDServicio_Det)                                            
,'NV')                                            
End as EstadoGuia,                                            
                      
                  
Case When IDVehiculo_Prg is null Then 'NV' Else 'PA' End EstadoVehiculo,                        
                                  
Case When CantTranspProgramados>1 Then 'MULTIPLE'                                            
Else                                            
IsNull(                                            
(Select Distinct Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                                             
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                    
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                  
Where od1.IDOperacion_Det=X.IDOperacion_Det                            
                 
And od1.IDServicio=X.IDServicio                
                 
)-- and od1.IDServicio_Det=x.IDServicio_Det)                                            
,'NV')                                            
End as EstadoTransportista,                         
                      
ObservVoucher,ObservBiblia,ObservInterno,                                                            
                                       
Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                            
Else                                            
IsNull(                                            
(Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                             
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                          
                                 
Where od1.IDOperacion_Det=X.IDOperacion_Det                            
And od1.IDServicio=X.IDServicio                 
)-- and od1.IDServicio_Det=x.IDServicio_Det)                                            
,'')   
End                                            
as IDGuiaProgramado,                                            
                                       
Case When CantTrasladistasProgramados >1 Then 'MULTIPLE'                                            
Else                               
IsNull(                                            
(Select Distinct Top 1 IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                              
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det_V_Prg=sd1.IDServicio_Det                            
Inner Join MASERVICIOS s1 On s1.IDServicio=sd1.IDServicio                     
Inner Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='017'                    
Where od1.IDOperacion_Det=X.IDOperacion_Det                     
And od1.IDServicio=X.IDServicio                             
                  
)                    
,'')                         
End                                            
as IDTrasladistaProgramado,                             
                  
IsNull(cast (X.IDVehiculo_Prg as varchar(3)),'') As IDVehiculoProgramado,                                                       
Case When CantTranspProgramados >1 Then 'MULTIPLE'                                            
Else                                            
IsNull(                                            
(Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                               
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                  
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                          
                         
Where od1.IDOperacion_Det=X.IDOperacion_Det                            
And od1.IDServicio=X.IDServicio                            
)-- and od1.IDServicio_Det=x.IDServicio_Det)                                            
,'')                                            
End                                            
as IDTransportistaProgramado,                                          

Case When CantGuiasProgramados>1 Then 'MULTIPLE'               Else                                            
IsNull(                                            
(Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                             
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                 
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                          
                                  
Where od1.IDOperacion_Det=X.IDOperacion_Det                            
And od1.IDServicio=X.IDServicio                            
)-- and od1.IDServicio_Det=x.IDServicio_Det)                                                       
,'')                                            
End                                            
as GuiaProgramado,                                                     
IsNull((Select NoVehiculoCitado + ' - ' + CAST(QtCapacidad as varchar(3))+' Pax' From MAVEHICULOS_TRANSPORTE Where NuVehiculo = X.IDVehiculo_Prg),'') As VehiculoProgramado,                        
                                       
Case When CantTranspProgramados >1 Then 'MULTIPLE'                               
Else                                            
IsNull(                                            
(Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                             
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                              
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                          
Where od1.IDOperacion_Det=X.IDOperacion_Det                             
And od1.IDServicio=X.IDServicio        
)-- and od1.IDServicio_Det=x.IDServicio_Det)                                    
,'')                                            
End                                            
as TransportistaProgramado,                                    
IdiomaSiglaOD as IDIdioma        
, MotivoEspecial,                          
X.IDServicio_Det  ,X.CodigoPnr ,x.NombreTourConductor
FROM                                            
(                                            
Select Distinct D.Dia as DiaFormatFecha,                                          
upper(substring(DATENAME(dw,D.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,D.Dia,103)/*+' '+ substring(convert(varchar,D.Dia,108),1,5)*/)) As Dia                                            
,convert(varchar(5),D.Dia,108) As Hora                                          
,D.IDOperacion,                                    
--S.Descripcion as DescServicio                                    
--rd.Servicio as DescServicio                                    
d.Servicio as DescServicio                           
,ST.Tipo as Variante,S.IDTipoServ,              
               
D.IDServicio                                              
               
,D.IDubigeo as IDCiudad,S.IDTipoProv,ts.Descripcion as DescTipoServ,S.IDProveedor ,                                              
Case When ISNULL(P.Email3,'')='' Then P.Email4 Else P.Email3 End as CorreoReservas,u.IDPais,                                              
D.Desayuno,D.Lonche,D.Almuerzo,D.Cena,D.Transfer ,                                            
--ST.Anio, IsNull(ST.Tipo,'') as Variante,                                            
D.IDOperacion_Det,                                             
D.NroPax,                                     
--CAST(d.NroPax AS VARCHAR(3))+                                        
--Case When isnull(d.NroLiberados,0)=0 then '' else '+'+CAST(d.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,                                          
d.NroPax+isnull(d.NroLiberados,0) as PaxmasLiberados,                                          
D.NroLiberados, D.Tipo_Lib, cc.TipoPax,   
                                       
(Select SUM(Total) From OPERACIONES_DET_DETSERVICIOS Where IDOperacion_Det=D.IDOperacion_Det and                                             
Not IDServicio_Det_V_Cot Is Null) as TotalCotizado,                                            
(Select SUM(Total) From OPERACIONES_DET_DETSERVICIOS Where IDOperacion_Det=D.IDOperacion_Det and                                             
Not IDServicio_Det_V_Prg Is Null) as TotalProgramado,                                             
                            
st.IDServicio_Det ,                               
                                       
(Select count(*) From                                            
(                                            
Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                             
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                      
Where od1.IDOperacion_Det=D.IDOperacion_Det --and od1.IDServicio_Det=st.IDServicio_Det                                           
And od1.IDServicio=D.IDServicio                            
) as x                                              
) as CantGuiasProgramados,       
                                       
(Select count(*) From                                            
(                                            
Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                             
Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                            
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                            
Where od1.IDOperacion_Det=D.IDOperacion_Det --and od1.IDServicio_Det=st.IDServicio_Det                                                        
And od1.IDServicio=D.IDServicio                            
) as x                                              
) as CantTranspProgramados,                          
                  
(Select count(*) From                                            
(                                            
Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                              
Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det_V_Prg=sd1.IDServicio_Det                            
Inner Join MASERVICIOS s1 On s1.IDServicio=sd1.IDServicio                     
Inner Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='017'                    
Where od1.IDOperacion_Det=D.IDOperacion_Det                     
And od1.IDServicio=D.IDServicio                            
) as x                                              
) as CantTrasladistasProgramados,                          
                                  
isnull(D.ObservVoucher,'') As ObservVoucher,                                          
isnull(D.ObservBiblia,'') As ObservBiblia ,isnull(D.ObservInterno,'') As  ObservInterno,                                  
isnull(cd.MotivoEspecial,'') as MotivoEspecial,                        
D.IDTrasladista_Prg, D.IDVehiculo_Prg ,dbo.FnPNRxIDCabxDia(o.IDCab,D.Dia) as CodigoPnr,        
idioD.Siglas as IdiomaSiglaOD        ,
IsNull(cp3.Titulo+' '+cp3.Apellidos + ' '+cp3.Nombres,'') as NombreTourConductor
--,Case When @IDTipoHon='002' And IsNull(rd.NuViaticoTC,0)<>0 then 0 Else 1 End As ValidoAPT

FROM OPERACIONES_DET D                                               
Inner JOIN MASERVICIOS S ON D.IDServicio=S.IDServicio And S.IDTipoProv<>'016'
Inner JOIN MAPROVEEDORES P On S.IDProveedor = P.IDProveedor                                               
Inner JOIN MATIPOSERVICIOS ts On S.IDTipoServ=ts.IDTipoServ                                                  
Inner JOIN MAUBIGEO u On p.IDCiudad=u.IDubigeo               
Inner JOIN MASERVICIOS_DET ST On D.IDServicio_Det = ST.IDServicio_Det
Left Join OPERACIONES_DET_DETSERVICIOS od On D.IDOperacion_Det = od.IDOperacion_Det
Inner Join OPERACIONES o On d.IDOperacion=o.IDOperacion And o.IDCab=@IdCab                                  
Inner Join COTICAB cc On o.IDCab=cc.IDCAB          
Left Join RESERVAS_DET rd On d.IDReserva_Det=rd.IDReserva_Det                                          
Left Join COTIDET cd On rd.IDDet=cd.IDDET            
Left Join MAIDIOMAS idioD on IsNull(rd.IDIdioma,D.IDIdioma) = idioD.IDidioma        
Left Join (select Top(1)* from COTIPAX cp where CP.IDCab=@IDCab and cp.FlTC=1 Order By Orden) cp3 On o.IDCab=cp3.IDCab                        
where (@IDTipoHon<>'002' And d.FlServicioTC=1 And IsNull(rd.NuViaticoTC,0)=0)
Or (@IDTipoHon='002' And (d.FlServicioTC=1 Or IsNull(rd.NuViaticoTC,0)<>0))
) as X  
--Where (x.ValidoAPT=1 And @IDTipoHon='002') Or x.ValidoAPT=1
) AS Y                                          
Left Join MAPROVEEDORES PG On Y.IDGuiaProgramado=PG.IDProveedor                            
Left Join MAPROVEEDORES PT On Y.IDTransportistaProgramado=PT.IDProveedor                                    
Order by DiaFormatFecha
