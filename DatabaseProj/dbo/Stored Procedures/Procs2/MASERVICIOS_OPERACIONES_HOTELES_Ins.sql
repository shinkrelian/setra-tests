﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

--alter
CREATE PROCEDURE [MASERVICIOS_OPERACIONES_HOTELES_Ins]
	@CoServicio char(8),
	@FlRecepcion bit,
	@FlEstacionamiento bit,
	@FlBoxDesayuno bit,
	@FlRestaurantes bit,
	@FlRestaurantesSoloDesayuno bit,
	@QtRestCantidad tinyint=0,
	@QtRestCapacidad tinyint=0,
	@QtTriples tinyint=0,
	@QtNumHabitaciones tinyint=0,
	@CoTipoCama char(3)='',
	@TxObservacion varchar(250)='',
	@UserMod char(4)=''
AS
BEGIN
	Insert Into [dbo].[MASERVICIOS_OPERACIONES_HOTELES]
		(
			[CoServicio],
 			[FlRecepcion],
 			[FlEstacionamiento],
 			[FlBoxDesayuno],
 			[FlRestaurantes],
 			[FlRestaurantesSoloDesayuno],
 			[QtRestCantidad],
 			[QtRestCapacidad],
 			[QtTriples],
 			[QtNumHabitaciones],
 			[CoTipoCama],
 			TxObservacion,
 			[UserMod]
 		)
	Values
		(
			@CoServicio,
 			@FlRecepcion,
 			@FlEstacionamiento,
 			@FlBoxDesayuno,
 			@FlRestaurantes,
 			@FlRestaurantesSoloDesayuno,
 			Case When @QtRestCantidad=0 Then Null Else @QtRestCantidad End,
 			Case When @QtRestCapacidad=0 Then Null Else @QtRestCapacidad End,
 			Case When @QtTriples=0 Then Null Else @QtTriples End,
 			Case When @QtNumHabitaciones=0 Then Null Else @QtNumHabitaciones End,
 			Case When RTRIM(LTRIM(@CoTipoCama))='' Then Null Else @CoTipoCama End,
 			Case When RTRIM(LTRIM(@TxObservacion))='' Then Null Else @TxObservacion End,
 			Case When @UserMod='' Then Null Else @UserMod End
 		)
END;

