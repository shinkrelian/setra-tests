﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_Lvw]
	@IDTemporada int,
	@NombreTemporada varchar(80)
AS
BEGIN
	Set NoCount On
	
	Select NombreTemporada	
	From MATEMPORADA
	Where (NombreTemporada Like '%'+@NombreTemporada+'%')
	And (IDTemporada <>@IDTemporada Or @IDTemporada=0)
	Order by NombreTemporada
END
