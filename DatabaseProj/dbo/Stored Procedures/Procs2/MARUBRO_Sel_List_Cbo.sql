﻿
--create
create PROCEDURE MARUBRO_Sel_List_Cbo
 @CoRubro tinyint=0,                    
 @NoRubro varchar(150)=''                   
as
SELECT     CoRubro, NoRubro
FROM         MARUBRO
WHERE     (CoRubro = @CoRubro or @CoRubro =0) AND
(@NoRubro='' or NoRubro like '%'+@NoRubro+'%' ) AND
(FlActivo = 1)
