﻿CREATE Procedure [dbo].[MASERIES_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion	
	From MASERIES
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDSerie <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
