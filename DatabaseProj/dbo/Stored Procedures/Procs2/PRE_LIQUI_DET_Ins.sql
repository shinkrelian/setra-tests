﻿
--HLF-20130116-@IDNotaVenta a char(8)
CREATE Procedure dbo.PRE_LIQUI_DET_Ins  
@IDNotaVenta char(8),  
@IDOperacion_Det int,  
@Descripcion varchar(max),  
@IDTipoOC char(3),
@SubTotal numeric(8,2),  
@TotalIGV numeric(8,2),  
@Total numeric(8,2),  
@UserMod char(4)  
As  
 Set NoCount On   
 declare @IDPreLiquiDet Tinyint  
 set @IDPreLiquiDet = (select isnull(Max(IDPreLiquiDet),0)+1 from PRE_LIQUI_DET where IDNotaVenta = @IDNotaVenta)  
 INSERT INTO [dbo].[PRE_LIQUI_DET]  
      ([IDNotaVenta]  
      ,[IDPreLiquiDet]  
      ,[IDOperacion_Det]  
      ,[Descripcion]  
      ,[IDTipoOC]
      ,[SubTotal]  
      ,[TotalIGV]  
      ,[Total]  
      ,[UserMod]  
      ,[FecMod])  
   VALUES  
      (@IDNotaVenta  
      ,@IDPreLiquiDet  
      ,Case When @IDOperacion_Det = 0 Then Null Else @IDOperacion_Det End 
      ,@Descripcion 
      ,@IDTipoOC 
      ,@SubTotal  
      ,@TotalIGV  
      ,@Total  
      ,@UserMod  
      ,GETDATE())  

