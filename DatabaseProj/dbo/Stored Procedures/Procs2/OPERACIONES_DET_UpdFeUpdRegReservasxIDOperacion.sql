﻿Create Procedure dbo.OPERACIONES_DET_UpdFeUpdRegReservasxIDOperacion
@IDOperacion int,
@UserMod char(4)
As
Set NoCount On
Update OPERACIONES_DET
	set FeUpdateRowReservas = Null,
		UserMod=@UserMod
Where IDOperacion=@IDOperacion
