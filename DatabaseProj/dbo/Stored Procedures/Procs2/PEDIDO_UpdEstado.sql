﻿--JRF-20160219-Agregar FePedidoEnviado,FePedidoAceptado1
CREATE Procedure dbo.PEDIDO_UpdEstado
@NuPedInt int,
@CoEstado char(2),
@UserMod char(4)
As
Set NoCount On
Update PEDIDO 
	Set CoEstado = @CoEstado,
		FePedidoEnviado = Case When @CoEstado='CE' And PEDIDO.FePedidoEnviado is Null then GETDATE() Else PEDIDO.FePedidoEnviado End,
		FePedidoAceptado = Case When @CoEstado='AC' And PEDIDO.FePedidoAceptado is Null then GETDATE() Else PEDIDO.FePedidoAceptado End,
		UserMod = @UserMod,
		FecMod = GETDATE()
Where NuPedInt = @NuPedInt
