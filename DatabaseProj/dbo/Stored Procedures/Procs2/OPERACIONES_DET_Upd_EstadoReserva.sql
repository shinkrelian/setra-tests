﻿--CREATE
create Procedure dbo.OPERACIONES_DET_Upd_EstadoReserva
	@IDOperacion_Det	int=0,
	@IDServicio_Det	int=0,
	@IDEstadoReserva	char(2)='',
	@UserMod	char(4)=''
As
	Set NoCount On
	Update OPERACIONES_DET Set
	Estado_Reserva=@IDEstadoReserva,
	Fec_NV=case when @IDEstadoReserva='NV' THEN GETDATE() ELSE Fec_NV END,
	Fec_RQ=case when @IDEstadoReserva='RQ' THEN GETDATE() ELSE Fec_RQ END,
	Fec_OK=case when @IDEstadoReserva='OK' THEN GETDATE() ELSE Fec_OK END,
	Fec_WL=case when @IDEstadoReserva='WL' THEN GETDATE() ELSE Fec_WL END,
	Fec_XL=case when @IDEstadoReserva='XL' THEN GETDATE() ELSE Fec_XL END,
	Fec_RR=case when @IDEstadoReserva='RR' THEN GETDATE() ELSE Fec_RR END,
	UserMod=@UserMod,	
	FecMod=GETDATE()
	WHERE IDOperacion_Det=@IDOperacion_Det and IDServicio_Det=@IDServicio_Det
