﻿--HLF-20140815-and psd.CoPrvPrg=@CoPrvPrg
--JRF-20150410-Considera IDPax
CREATE Procedure dbo.PRESUPUESTO_SOBRE_DET_Sel_ListRpt      
	@NuPreSob int,
	@CoPrvPrg char(6),
	@IDPax int
as      
	Set NoCount On      
	select DISTINCT FeDetPSo as Fecha,c.IDFile,      
	SUBSTRING(psd.TxServicio,0,50) as Servicio1,SUBSTRING(psd.TxServicio,50,LEN(psd.TxServicio)) as Servicio2,      
	psd.SsTotal,psd.QtPax,      
	 dbo.fnConverirNumeroEnLetra(psd.SsTotal) As TotalLetras      
	from PRESUPUESTO_SOBRE_DET psd Left Join PRESUPUESTO_SOBRE ps On psd.NuPreSob = ps.NuPreSob      
	Left Join COTICAB c On ps.IDCab = c.IDCAB      
	where psd.NuPreSob = @NuPreSob and psd.CoPago = 'TCK'  
	And IsNull(psd.CoPrvPrg,'')=Ltrim(rtrim(@CoPrvPrg)) and IsNull(psd.IDPax,0)=@IDPax
