﻿Create Procedure dbo.OPERACIONES_DET_UpFecLogdxIDoperacion
@IDOperacion int,
@UserMod char(4)
As
Set NoCount On
Update OPERACIONES_DET
	Set FeUpdateRowReservas=Null,
		UserMod=@UserMod,
		FecMod=Getdate()
Where IDOperacion=@IDOperacion
