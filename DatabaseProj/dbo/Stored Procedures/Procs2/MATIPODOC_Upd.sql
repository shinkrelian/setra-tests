﻿--MLL-030614-Agregado la columnas (NoFiscal ,CoSunat ,CoStarSoft)
CREATE Procedure [dbo].[MATIPODOC_Upd]
	@IDtipodoc char(3),
	@Descripcion varchar(60),
	@Sunat char(3),
	@UserMod char(4),
	@NoFiscal  varchar(60),
	@CoSunat char(2),
	@CoStarSoft	 char(2)	
As
	
	Set NoCount On
	
	Update MATIPODOC
    Set Descripcion=@Descripcion
           ,Sunat=@Sunat
           ,UserMod=@UserMod
           ,FecMod=getdate()
           ,NoFiscal=@NoFiscal 
		   ,CoSunat = Case When ltrim(rtrim(@CoSunat))='' Then Null Else @CoSunat End 
		 ,CoStarSoft=Case When ltrim(rtrim(@CoStarSoft))='' Then Null Else @CoStarSoft End

    Where
           IDtipodoc=@IDtipodoc

