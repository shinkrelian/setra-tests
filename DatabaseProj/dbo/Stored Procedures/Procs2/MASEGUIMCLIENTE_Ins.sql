﻿CREATE Procedure [dbo].[MASEGUIMCLIENTE_Ins]
    @IDCliente char(6),    
    @IDContacto char(3),
    @Notas text,
    @UserMod char(4),
    @pIDSeguim	int output
As

	Set NoCount On
	Declare @IDSeguim int=(Select Isnull(MAX(IDSeguim),0)+1 From MASEGUIMCLIENTE)	
	
	INSERT INTO MASEGUIMCLIENTE
           ([IDSeguim]
           ,[IDCliente]           
           ,[IDContacto]
           ,[Notas]
           ,[UserMod]
           )
     VALUES
           (@IDSeguim ,
            @IDCliente,            
            @IDContacto ,
            @Notas ,
            @UserMod )
           
	Set @pIDSeguim=@IDSeguim
