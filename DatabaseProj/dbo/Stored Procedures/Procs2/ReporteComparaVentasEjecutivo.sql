﻿Create Procedure dbo.ReporteComparaVentasEjecutivo
@IDUsuarioVenta char(4),
@Anio char(4),
@Estado char(1),
@TopCliente tinyint
As
Set NoCount On
Declare @AnioMes char(6), @TotalVta numeric(12,2), @Cliente varchar(80)
Declare @Ventas table(
IDCliente char(6) null,
AnioMes char(6) null,
TotalGeneral numeric(12,2) null,
Participacion numeric(8,2) null)

Insert into @Ventas
SELECT Cliente,AnioMes, SUM(TotalVenta) as TotalVenta,0
 FROM          
  (          
  SELECT Left(convert(varchar,FechaOutPeru,112),6) as AnioMes,          
   Case When datediff(d,FechaOutPeru,GETDATE())>0 Then          
  TotalDM          
   Else          
    Case When TotalDM=0 Then          
   ValorVenta*(PoConcretVta/100)          
    Else          
               
    CASE WHEN Estado='X' THEN         
     TotalDM        
    ELSE        
       Case When Abs( (case when valorventa=0 then 0 else (((TotalDM)*100)/ValorVenta) end) )>=15 then     
      TotalDM          
     Else          
       ValorVenta*(PoConcretVta/100)          
     End              
    End    
    End          
   End as TotalVenta       
   ,Cliente
   FROM           
   (                 
      Select cc.estado,cc.IDCAB, isnull((Select Max(Dia) From COTIDET cd1           
      Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo           
      And u1.IDPais='000323'          
      Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det          
      Where IDCAB=cc.IDCAB ),cc.FechaOut) as FechaOutPeru,          
      cc.PoConcretVta,            
      cc.NroPax,             
      isnull((Select sum(total*nropax) From Cotidet Where idcab=cc.idcab),0) as ValorVenta,          
      ISNULL((Select SUM(dm1.Total) From DEBIT_MEMO dm1 Left Join MACLIENTES c1 On dm1.IDCliente=c1.IDCliente
			   Where dm1.IDCab=cc.idcab     
			   And dm1.IDEstado<>'AN'   
			   And CHARINDEX(c1.RazonComercial,dm1.Cliente)<>0)  
			   ,0) as TotalDM 
			   ,c2.IDCliente as Cliente
      From COTICAB cc Left Join MACLIENTES c2 On cc.IDCliente=c2.IDCliente
      Where cc.CoTipoVenta='RC' and cc.IDUsuario=@IDUsuarioVenta
		    and ((Ltrim(Rtrim(@Estado))='' Or cc.Estado=@Estado) and cc.IDCAB in (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG'))
	) as X
   ) As Y        
  Where (Left(Y.AnioMes,4)=@Anio Or Left(Y.AnioMes,4)= Cast(CAST(@Anio as int)-1 as CHAR(4)))
  GROUP BY Y.Cliente,Y.AnioMes ORDER BY 1 

--select * from @Ventas

DECLARE @ComparaVtasClientes TABLE(
  IDCliente char(6),
  Anio char(4),          
  Ene numeric(12,2),          
  Feb numeric(12,2),          
  Mar numeric(12,2),          
  Abr numeric(12,2),          
  May numeric(12,2),          
  Jun numeric(12,2),          
  Jul numeric(12,2),          
  Ago numeric(12,2),          
  Seti numeric(12,2),          
  Oct numeric(12,2),          
  Nov numeric(12,2),          
  Dic numeric(12,2),          
  Total numeric(14,2),      
  Participacion numeric(14,2),    
  Anio_Anterior char(4),          
  Ene_Anterior numeric(12,2),          
  Feb_Anterior numeric(12,2),          
  Mar_Anterior numeric(12,2),          
  Abr_Anterior numeric(12,2),          
  May_Anterior numeric(12,2),          
  Jun_Anterior numeric(12,2),          
  Jul_Anterior numeric(12,2),          
  Ago_Anterior numeric(12,2),          
  Seti_Anterior numeric(12,2),          
  Oct_Anterior numeric(12,2),          
  Nov_Anterior numeric(12,2),          
  Dic_Anterior numeric(12,2),          
  Total_Anterior numeric(14,2),
  Part_Anterior numeric(14,2)) 
  
Insert Into @ComparaVtasClientes (IDCliente,Anio,Ene,Feb,Mar,Abr,May,Jun,Jul,Ago,Seti,Oct,Nov,Dic,Total,Participacion,
							  Anio_Anterior,Ene_Anterior,Feb_Anterior,Mar_Anterior,Abr_Anterior,May_Anterior,Jun_Anterior,
							  Jul_Anterior,Ago_Anterior,Seti_Anterior,Oct_Anterior,Nov_Anterior,Dic_Anterior,Total_Anterior,Part_Anterior)
Select Distinct IDCliente,LEFT(AnioMes,4),0,0,0,0,0,0,0,0,0,0,0,0,0,0,
					  Cast(CAST(@Anio as int)-1 as CHAR(4)),0,0,0,0,0,0,0,0,0,0,0,0,0,0
							   From @Ventas where LEFT(AnioMes,4)=@Anio --#Ventas

 Declare curVtas cursor for Select IDCliente,AnioMes,TotalGeneral From @Ventas --Order By cliente          
 Open curVtas          
 Fetch Next From curVtas Into @Cliente,@AnioMes,@TotalVta
 While @@FETCH_STATUS=0          
  Begin          
  If RIGHT(@AnioMes,2)='01'           
  Begin
   Update @ComparaVtasClientes Set Ene=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Ene_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='02'
  Begin        
   Update @ComparaVtasClientes Set Feb=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente      
   Update @ComparaVtasClientes Set Feb_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='03'
  Begin           
   Update @ComparaVtasClientes Set Mar=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Mar_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='04'
  Begin         
   Update @ComparaVtasClientes Set Abr=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Abr_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='05'           
  Begin
   Update @ComparaVtasClientes Set May=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set May_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='06'  
  Begin         
   Update @ComparaVtasClientes Set Jun=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Jun_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='07'
  Begin   
   Update @ComparaVtasClientes Set Jul=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Jul_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='08'
  Begin
   Update @ComparaVtasClientes Set Ago=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Ago_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='09'
  Begin
   Update @ComparaVtasClientes Set Seti=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Seti_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='10'
  Begin
   Update @ComparaVtasClientes Set Oct=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Oct_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='11'
  Begin
   Update @ComparaVtasClientes Set Nov=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Nov_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  If RIGHT(@AnioMes,2)='12'
  Begin
   Update @ComparaVtasClientes Set Dic=isnull(@TotalVta,0) Where Anio=LEFT(@AnioMes,4) and IDCliente=@Cliente
   Update @ComparaVtasClientes Set Dic_Anterior=isnull(@TotalVta,0) Where Anio_Anterior=LEFT(@AnioMes,4) and IDCliente=@Cliente   
  End
  Fetch Next From curVtas Into @Cliente,@AnioMes,@TotalVta 
End          
           
            
 Close curVtas          
 Deallocate curVtas  

if @TopCliente = 0
	Set @TopCliente = (select COUNT(*) from @ComparaVtasClientes)
	 
 Update @ComparaVtasClientes Set Total=Ene+Feb+Mar+Abr+May+Jun+Jul+Ago+Seti+Oct+Nov+Dic,
								 Total_Anterior=Ene_Anterior+Feb_Anterior+Mar_Anterior+Abr_Anterior+May_Anterior+Jun_Anterior+Jul_Anterior+
												Ago_Anterior+Seti_Anterior+Oct_Anterior+Nov_Anterior+Dic_Anterior
 Declare @TotalClientes numeric(12,2)=(select Top(@TopCliente) SUM(Total) from @ComparaVtasClientes)
 Declare @TotalClientes2 numeric(12,2)=(select Top(@TopCliente) SUM(Total_Anterior) from @ComparaVtasClientes)
 Update @ComparaVtasClientes Set Participacion = round((Total / @TotalClientes) * 100,0),
								 Part_Anterior = round((Total_Anterior / @TotalClientes2) * 100,0)
	 


select Top(@TopCliente) (select Usuario from mausuarios u2 where u2.IDUsuario=@IDUsuarioVenta) as UsuarioSelect,c.IDCliente,
	   Case When Total > 0 and Total_Anterior = 0 Then 1 Else 0 End as NuevoCliente,c.RazonComercial,u.Usuario as UsuarioCuenta,Case When CHARINDEX('WEB (',c.RazonComercial) > 0 Then 1 Else 0 End as ClienteWeb,
	   cv.Anio,cv.Ene,cv.Feb,cv.Mar,cv.Abr,cv.May,cv.Jun,cv.Jul,cv.Ago,cv.Seti,cv.Oct,cv.Nov,cv.Dic,cv.Total,cv.Participacion,
	   cv.Anio_Anterior,cv.Total_Anterior
from @ComparaVtasClientes cv Left Join MACLIENTES c On cv.IDCliente=c.IDCliente
Left Join MAUSUARIOS u On c.IDvendedor=u.IDUsuario
where cv.Total <> 0
