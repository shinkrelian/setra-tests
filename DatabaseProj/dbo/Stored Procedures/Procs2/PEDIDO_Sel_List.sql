﻿--HLF-20160218-Desc al Order by
CREATE Procedure dbo.PEDIDO_Sel_List
	@CoEjeVta char(4),
	@NuPedido varchar(9),
	@NuFile varchar(8),
	@NuCotizacion varchar(8),
	@TxTitulo varchar(100),
	@RazonComercial varchar(80),
	@FlHistorico bit
As
	Set Nocount on

	Select Distinct u.Nombre+isnull(' '+u.TxApellidos,'') as EjecutivoVtas, p.FePedido, p.NuPedido, p.NuPedInt,
	c.RazonComercial, p.TxTitulo, 
	p.CoEstado,
	Case p.CoEstado	When 'PD' then 'PENDIENTE'
	When 'AC' then 'ACEPTADO'
	When 'AN' then 'ANULADO'
	When 'EL' then 'ELIMINADO'
	When 'NA' then 'NO ACEPTADO'
	End as DescEstado			
	From PEDIDO p Left Join MAUSUARIOS u On p.CoEjeVta=u.IDUsuario
	Inner Join MACLIENTES c On p.CoCliente=c.IDCliente
	Left Join COTICAB cc On p.NuPedInt=cc.NuPedInt
	Where (p.CoEjeVta=@CoEjeVta Or ltrim(rtrim(@CoEjeVta))='') And
	(p.NuPedido Like '%'+@NuPedido+'%' Or @NuPedido='') And
	(cc.IDFile Like '%'+@NuFile+'%' Or @NuFile='') And
	(cc.Cotizacion Like '%'+@NuCotizacion+'%' Or @NuCotizacion='') And
	(p.TxTitulo Like '%'+@TxTitulo+'%' Or @TxTitulo='') And
	(c.RazonComercial Like '%'+@RazonComercial+'%' Or @RazonComercial='') And
	(((cc.FlHistorico=@FlHistorico and not cc.IDCAB is null) Or @FlHistorico=0) Or @FlHistorico=1)
	Order by p.FePedido Desc

