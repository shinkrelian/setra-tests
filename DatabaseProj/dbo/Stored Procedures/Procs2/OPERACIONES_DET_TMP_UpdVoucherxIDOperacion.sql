﻿Create Procedure dbo.OPERACIONES_DET_TMP_UpdVoucherxIDOperacion
@IDOperacion int
As
Set NoCount On

--Declare @IDOperacion int = 15076
--select odTemp.IDOperacion_Det,odTemp.IDVoucher,odOrig.IDVoucher,odOrig.IDOperacion_Det
----Update OPERACIONES_DET_TMP set OPERACIONES_DET_TMP.IDVoucher=odOrig.IDVoucher
--from OPERACIONES_DET_TMP odTemp Right Join OPERACIONES_DET odOrig On odTemp.IDReserva_Det=odOrig.IDReserva_Det
--Where odTemp.IDOperacion=@IDOperacion--(odTemp.IDOperacion=@IDOperacion and odOrig.IDOperacion=@IDOperacion)
--Order By odTemp.IDOperacion_Det

--select IDVoucher,IDReserva_Det,odt.IDServicio_Det,odt.IDServicio,sd.Tipo,sd.Anio,convert(char(10),dia,103) as Dia
--from OPERACIONES_DET_TMP odt Left Join MASERVICIOS_DET sd On odt.IDServicio_Det=sd.IDServicio_Det
--where IDOperacion=@IDOperacion Order By Dia

--select IDVoucher,IDReserva_Det,odt.IDServicio_Det,odt.IDServicio,sd.Tipo,sd.Anio,convert(char(10),dia,103) as Dia
--from OPERACIONES_DET odt Left Join MASERVICIOS_DET sd On odt.IDServicio_Det=sd.IDServicio_Det
--where IDOperacion=@IDOperacion Order By Dia

Declare @TipoProv char(3)=''
Select @TipoProv=p.IDTipoProv
from OPERACIONES o left Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
where o.IDOperacion=@IDOperacion

--select odTemp.IDVoucher,odOrig.IDVoucher
Update OPERACIONES_DET_TMP set OPERACIONES_DET_TMP.IDVoucher=IsNull(odOrig.IDVoucher,0)
from OPERACIONES_DET_TMP odTemp Inner Join OPERACIONES_DET odOrig On odTemp.IDServicio=odOrig.IDServicio
--Left Join OPERACIONES
Where (odTemp.IDOperacion=@IDOperacion and odOrig.IDOperacion=@IDOperacion) And @TipoProv <> '001'

--select odTemp.IDOperacion_Det,odTemp.IDVoucher,odOrig.IDVoucher,odOrig.IDOperacion_Det
Update OPERACIONES_DET_TMP set OPERACIONES_DET_TMP.IDVoucher=IsNull(odOrig.IDVoucher,0)
from OPERACIONES_DET_TMP odTemp Inner Join OPERACIONES_DET odOrig On odTemp.IDServicio=odOrig.IDServicio
And (convert(char(10),odTemp.Dia,103)=convert(char(10),odOrig.Dia,103) And convert(char(10),odTemp.FechaOut,103)=convert(char(10),odOrig.FechaOut,103))
Where (odTemp.IDOperacion=@IDOperacion and odOrig.IDOperacion=@IDOperacion) And @TipoProv = '001'
