﻿--JHD-20151027- cambiar la busqueda incluyendo los proveedores que tienen como prov. internacional el @idproveedor
--JHD-20151104-IGV=0,
--JRF-20151203-..from ORDEN_SERVICIO_DET_MONEDA os2 Left Join ORDEN_SERVICIO os On os2.NuOrden_Servicio=os.NuOrden_Servicio
--JHD-20151214-Se corrigio el tema de monedas
CREATE PROCEDURE ORDEN_SERVICIO_Sel_List_Proveedor_DocMultiple
@idproveedor char(6),
@CoMoneda char(3)
as
BEGIN
/*
select distinct 
IDVoucher=o.IDOrdPag,
Titulo=c.Titulo,
IDCAB=c.IDCAB,
IDFile=c.IDFile,
IDMoneda=o.idmoneda,
Simbolo=(select simbolo from MAMONEDAS where IDMoneda=o.idmoneda),
Total=ssmontototal
*/

 declare @tablaproveedores as table(
 idprov char(6)
 )

 insert into @tablaproveedores values (@idproveedor)

 insert into @tablaproveedores 
 select distinct idproveedor from MAPROVEEDORES where IDProveedorInternacional=@idproveedor

 select distinct 
 Codigo=os.nuorden_Servicio,
SUBSTRING(os.CoOrden_Servicio,1,8)+'-'+SUBSTRING(os.CoOrden_Servicio,9,Len(os.CoOrden_Servicio)) as IDVoucher,
Titulo=c.Titulo,
 c.IDCAB,              
 c.IDFile,                     
--IDMoneda=isnull((SELECT   top 1  RESERVAS_DET.IDMoneda 
--    FROM         ORDEN_SERVICIO INNER JOIN  
--           ORDEN_SERVICIO_DET ON ORDEN_SERVICIO.NuOrden_Servicio = ORDEN_SERVICIO_DET.NuOrden_Servicio INNER JOIN  
--           OPERACIONES_DET ON ORDEN_SERVICIO_DET.IDOperacion_Det = OPERACIONES_DET.IDOperacion_Det INNER JOIN  
--           RESERVAS_DET ON OPERACIONES_DET.IDReserva_Det = RESERVAS_DET.IDReserva_Det  
--    WHERE     (ORDEN_SERVICIO.NuOrden_Servicio = os.NuOrden_Servicio)),''),  

IDMoneda=isnull(os2.comoneda,''),

 Simbolo=(select simbolo from MAMONEDAS where IDMoneda=isnull(os2.comoneda,'')),    
IGV=0,
 Isnull(os2.SsMontoTotal,0) as Total
  
 from ORDEN_SERVICIO_DET_MONEDA os2 Left Join ORDEN_SERVICIO os On os2.NuOrden_Servicio=os.NuOrden_Servicio
 Left Join MAPROVEEDORES p On os.IDProveedor=p.IDProveedor                           
 Left Join COTICAB c On os.IDCab=c.IDCAB                      
 
--where os.IDProveedor=@idproveedor
where os.IDProveedor in (select idprov from @tablaproveedores)

--AND ((isnull((SELECT   top 1  RESERVAS_DET.IDMoneda 
--    FROM         ORDEN_SERVICIO INNER JOIN  
--           ORDEN_SERVICIO_DET ON ORDEN_SERVICIO.NuOrden_Servicio = ORDEN_SERVICIO_DET.NuOrden_Servicio INNER JOIN  
--           OPERACIONES_DET ON ORDEN_SERVICIO_DET.IDOperacion_Det = OPERACIONES_DET.IDOperacion_Det INNER JOIN  
--           RESERVAS_DET ON OPERACIONES_DET.IDReserva_Det = RESERVAS_DET.IDReserva_Det  
--    WHERE     (ORDEN_SERVICIO.NuOrden_Servicio = os.NuOrden_Servicio)),''))=@CoMoneda or @CoMoneda='')

AND (os2.comoneda=@CoMoneda or @CoMoneda='')

----and o.IDOrdPag<>'0'
and os.NuOrden_Servicio not in (select distinct NuOrden_Servicio from DOCUMENTO_PROVEEDOR where NuOrden_Servicio is not null and FlActivo=1)

order by IDVoucher
END;
