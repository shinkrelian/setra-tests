﻿--HLF-20130924-Agregando And Anulado=0
CREATE Procedure dbo.OPERACIONES_DET_PAX_InsxIDProveedor  
 @IDCab int,  
 @IDProveedor char(6),  
 @UserMod char(4)  
As  
 Set Nocount On  
   
 Insert Into OPERACIONES_DET_PAX(IDOperacion_Det,IDPax,UserMod)   
 Select  
 (Select IDOperacion_Det From  OPERACIONES_DET Where IDReserva_Det=d.IDReserva_Det),  
 IDPax,@UserMod From RESERVAS_DET_PAX d Inner Join RESERVAS_DET rd On d.IDReserva_Det=rd.IDReserva_Det And rd.Anulado=0
 Where d.IDReserva_Det In   
 (Select IDReserva_Det From OPERACIONES_DET Where IDOperacion In   
 (Select IDOperacion From OPERACIONES Where IDCab=@IDCab And IDProveedor=@IDProveedor))  	

