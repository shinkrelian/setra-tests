﻿--alter
----JHD-20150310- Se agrego el campo CoTipoDetraccion.
--JHD-20150311- Se cambio el campo CoEstado por CoEstado_OC
--JHD-20150313- Permitir ingresar 0 para IGV
--JHD-20150313- Se agregaron los campos FeEntrega y FePago
--JHD-20150617- TxDescripcion = 	Case When ltrim(rtrim(@TxDescripcion))='' Then '' Else @TxDescripcion End,
CREATE PROCEDURE [dbo].[ORDENCOMPRA_Upd]
	@NuOrdComInt int,
	@NuOrdCom char(13),
	@CoProveedor char(6),
	@FeOrdCom smalldatetime,
	@CoMoneda char(3),
	@SsTotal numeric(8,2),
	@CoEstado_OC char(2),
	@UserMod char(4),
	@TxDescripcion varchar(max),
	@CoArea tinyint,
	@IDContacto char(3),
	@CoRubro tinyint,
	@UserSolicitante char(4),
	@IDAdminist char(2),
	@CoCeCos char(6),
	@SSDetraccion numeric(8,2),
	@SSIGV numeric(8,2),
	@SSSubTotal numeric(8,2),
	@CoTipoDetraccion CHAR(5)=''
AS
BEGIN

	  declare @IDFormaPago char(3)
	  set @IDFormaPago=(select IDFormaPago from MAPROVEEDORES where IDProveedor=@CoProveedor)
	  
	  declare @DiasCredito tinyint=0
	  
	  if @IDFormaPago='001'
		  begin
			set @DiasCredito=(select isnull(DiasCredito,0) from MAPROVEEDORES where IDProveedor=@CoProveedor)
		  end

	Update [dbo].[ORDENCOMPRA]
	Set
		[NuOrdCom] = @NuOrdCom,
 		[CoProveedor] = Case When ltrim(rtrim(@CoProveedor))='' Then Null Else @CoProveedor End, --@CoProveedor,
 		[FeOrdCom] = @FeOrdCom,
 		[CoMoneda] = Case When ltrim(rtrim(@CoMoneda))='' Then Null Else @CoMoneda End,
 		--[SsImpuestos] = @SsImpuestos,
 		[SsTotal] = Case When @SsTotal=0 Then Null Else @SsTotal End, 
 		--[SsSaldo] = Case When @SsTotal=0 Then Null Else @SsTotal End, --@SsSaldo,
		[SsSaldo] = Case When @SsTotal=0 Then Null Else (@SsTotal+isnull(@SSDetraccion,0)) End, --@SsSaldo,
 		--[SsDifAceptada] = @SsDifAceptada,
 		--[TxObsDocumento] = @TxObsDocumento,
 		CoEstado_OC = Case When ltrim(rtrim(@CoEstado_OC))='' Then Null Else @CoEstado_OC End,
 		[UserMod] = @UserMod,
 		[FecMod] = GETDATE(),
 		
 		 		
[TxDescripcion] = 	Case When ltrim(rtrim(@TxDescripcion))='' Then '' Else @TxDescripcion End,
[CoArea] =Case When @CoArea=0 Then Null Else @CoArea End,
[IDContacto] =Case When ltrim(rtrim(@IDContacto))='' Then Null Else @IDContacto End,
[CoRubro] =Case When @CoRubro=0 Then Null Else @CoRubro End,
[UserSolicitante] =Case When ltrim(rtrim(@UserSolicitante))='' Then Null Else @UserSolicitante End,
[IDAdminist] =Case When ltrim(rtrim(@IDAdminist))='' Then Null Else @IDAdminist End,
[CoCeCos] =Case When ltrim(rtrim(@CoCeCos))='' Then Null Else @CoCeCos End,
[SSDetraccion] = Case When @SSDetraccion=0 Then Null Else round(@SSDetraccion,0) End,
[SSIGV] =@SSIGV,--Case When @SSIGV=0 Then Null Else @SSIGV End,
[SSSubTotal] =Case When @SSSubTotal=0 Then Null Else @SSSubTotal End,
 		
 CoTipoDetraccion=Case When ltrim(rtrim(@CoTipoDetraccion))='' Then Null Else @CoTipoDetraccion End,
 
  FeEntrega=case when (@CoEstado_OC='EN' and CoEstado<>'EN') THEN GETDATE() ELSE FeEntrega END  
 ,FePago=DATEADD(DAY,@DiasCredito,@FeOrdCom)
 	Where 
		[NuOrdComInt] = @NuOrdComInt
END;
