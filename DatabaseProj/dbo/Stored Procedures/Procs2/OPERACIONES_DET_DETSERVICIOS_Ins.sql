﻿
--JRF-20130603-Nuevos Campos [NetoProgram,IgvProgram,TotalProgram]            
--HLF-20130820-Nuevo parametro @IngManual          
--JRF-20130926-Agregando el parametro IDEstadoSolicitud,@IDProveedor_Prg        
--JRF-20131205-Agregando el parametro Total_PrgEditado      
--JRF-20140228-Aumento de Longitud para las columnas con numeric(12,4)
--JRF-20150223-Considerar [FlServicioNoShow]
--HLF-20150625-Nuevo parametro @QtPax ,od.NroPax+ISNULL(od.NroLiberados,0) as PaxLiberados
CREATE Procedure  [dbo].[OPERACIONES_DET_DETSERVICIOS_Ins]                
 @IDOperacion_Det int,                
 @IDServicio_Det int,                
 @IDServicio char(8),                
 @IDServicio_Det_V_Cot int,              
 @CostoReal numeric(12,4),                
 @CostoLiberado numeric(12,4),                
 @Margen numeric(12,4),                
 @MargenAplicado numeric(6,2),                
 @MargenLiberado numeric(12,4),                
 @Total numeric(12,4),                
 @SSCR numeric(12,4),                
 @SSCL numeric(12,4),                
 @SSMargen numeric(12,4),                
 @SSMargenLiberado numeric(12,4),                
 @SSTotal numeric(12,4),                
 @STCR numeric(12,4),                
 @STMargen numeric(12,4),                
 @STTotal numeric(12,4),                
 @CostoRealImpto numeric(12,4),                
 @CostoLiberadoImpto numeric(12,4),                
 @MargenImpto numeric(12,4),                
 @MargenLiberadoImpto numeric(12,4),                
 @SSCRImpto numeric(12,4),                
 @SSCLImpto numeric(12,4),                
 @SSMargenImpto numeric(12,4),                
 @SSMargenLiberadoImpto numeric(12,4),                
 @STCRImpto numeric(12,4),                
 @STMargenImpto numeric(12,4),                
 @TotImpto numeric(12,4),                
 @IDMoneda char(3),                
 @TipoCambio numeric(12,4),               
 @NetoProgram numeric(12,4),             
 @IgvProgram numeric(12,4),            
 @TotalProgram numeric(12,4),    
   
 @NetoCotiz numeric(12,4),             
 @IgvCotiz numeric(12,4),            
 @TotalCotiz numeric(12,4),   
           
 @IngManual bit,          
 @IDEstadoSolicitud char(2),        
 @IDProveedor_Prg varchar(6),        
 @Total_PrgEditado bit,      
 @Activo bit,   
 @FlServicioNoShow bit,
 @QtPax smallint,
 @UserMod char(4)                
As                
 Set NoCount On                
 INSERT INTO [OPERACIONES_DET_DETSERVICIOS]                
      ([IDOperacion_Det]                
      ,[IDServicio_Det]              
      ,IDServicio                
      ,IDServicio_Det_V_Cot              
      ,[CostoReal]                
      ,[CostoLiberado]                
      ,[Margen]                
      ,[MargenAplicado]                
      ,[MargenLiberado]                
      ,[Total]                
      ,[SSCR]                
      ,[SSCL]                
      ,[SSMargen]                
      ,[SSMargenLiberado]                
      ,[SSTotal]                
      ,[STCR]                
      ,[STMargen]                
      ,[STTotal]                
      ,[CostoRealImpto]                
      ,[CostoLiberadoImpto]                
      ,[MargenImpto]                
      ,[MargenLiberadoImpto]                
      ,[SSCRImpto]                
      ,[SSCLImpto]                
      ,[SSMargenImpto]                
      ,[SSMargenLiberadoImpto]                
      ,[STCRImpto]                
      ,[STMargenImpto]                
      ,[TotImpto]                
      ,[IDMoneda]      
      ,[TipoCambio]      
      ,[NetoProgram]            
      ,[IgvProgram]             
      ,[TotalProgram]            
      ,[IngManual]      
      ,[NetoCotizado]  
      ,[IgvCotizado]  
      ,[TotalCotizado]  
      ,[IDEstadoSolicitud]      
      ,[IDProveedor_Prg]      
      ,[Total_PrgEditado]      
      ,[Activo]     
      ,[FlServicioNoShow] 
	  ,QtPax
      ,[UserMod]                      
      ,[FecMod])  
   VALUES                
      (@IDOperacion_Det,                 
      @IDServicio_Det,                 
      @IDServicio,              
      Case When @IDServicio_Det_V_Cot=0 Then Null Else @IDServicio_Det_V_Cot End,              
      @CostoReal,                 
      @CostoLiberado,                
      @Margen,                 
      @MargenAplicado,                
      @MargenLiberado,                
      @Total,                 
      Case When @SSCR=0 Then Null Else @SSCR End,                 
      Case When @SSCL=0 Then Null Else @SSCL End,                 
      Case When @SSMargen=0 Then Null Else @SSMargen End,                    
      Case When @SSMargenLiberado=0 Then Null Else @SSMargenLiberado End,                      
      Case When @SSTotal=0 Then Null Else @SSTotal End,              
      Case When @STCR=0 Then Null Else @STCR End,                 
      Case When @STMargen=0 Then Null Else @STMargen End,              
      Case When @STTotal=0 Then Null Else @STTotal End,              
      @CostoRealImpto,                
      @CostoLiberadoImpto,                
      @MargenImpto,                 
      @MargenLiberadoImpto,                 
      Case When @SSCRImpto=0 Then Null Else @SSCRImpto End,                 
      Case When @SSCLImpto=0 Then Null Else @SSCLImpto End,                 
      Case When @SSMargenImpto=0 Then Null Else @SSMargenImpto End,                 
      Case When @SSMargenLiberadoImpto=0 Then Null Else @SSMargenLiberadoImpto End,                 
      Case When @STCRImpto=0 Then Null Else @STCRImpto End,                 
      Case When @STMargenImpto=0 Then Null Else @STMargenImpto End,                 
      @TotImpto,                 
      @IDMoneda,              
      Case When @TipoCambio=0 Then Null Else @TipoCambio End,              
   Case When @NetoProgram = 0 then Null Else @NetoProgram End,            
      Case When @IgvProgram = 0 Then Null Else @IgvProgram End,            
      Case When @TotalProgram = 0 then Null Else @TotalProgram End,            
      @IngManual,      
      Case When @NetoCotiz = 0 then Null Else @NetoCotiz End,            
      Case When @IgvCotiz = 0 Then Null Else @IgvCotiz End,            
      Case When @TotalCotiz = 0 then Null Else @TotalCotiz End,          
      @IDEstadoSolicitud,        
      case when ltrim(rtrim(@IDProveedor_Prg))='' Then Null else @IDProveedor_Prg end,        
      @Total_PrgEditado,      
      @Activo,     
      @FlServicioNoShow, 
	  @QtPax,
      @UserMod,                 
      GETDATE())         
