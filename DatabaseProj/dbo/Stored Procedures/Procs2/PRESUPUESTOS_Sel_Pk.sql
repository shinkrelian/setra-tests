﻿CREATE Procedure DBO.PRESUPUESTOS_Sel_Pk 
@IDPresupuesto int  
As  
 Set NoCount On  
 select c.IDFile ,c.Cotizacion   
    ,Pr.NombreCorto,Us.Nombre As NombreUsuario, Ub.Descripcion,Ps.*   
 from PRESUPUESTOS Ps Left Join COTICAB c On Ps.IDCab = c.IDCAB  
      Left Join MAPROVEEDORES Pr On Ps.IDGuia = Pr.IDProveedor  
      Left Join  MAUBIGEO Ub On Ub.IDubigeo = Ps.IDUbigeo
      Left Join MAUSUARIOS Us On Us.IDUsuario = Ps.IDUsuarioPre
 Where IDPresupuesto = @IDPresupuesto  
