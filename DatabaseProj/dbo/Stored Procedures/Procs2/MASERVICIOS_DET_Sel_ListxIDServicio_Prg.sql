﻿--JRF-20130223- Nuevos campos(bit de Edición de los campos[])                                            
--HLF-20130226-Agregando IDServicio y cambiando el Where                                          
--HLF-20130523-Cambiando columna DescProveedor_Prg a posicion 9                                        
--Agregando columnas NetoGen, IgvGen, SimboloMoneda, TotalGen                                        
--HLF-20130524-Agregando PaxmasLiberados                                        
--JRF-20130603-Nuevos Campos [NetoProgram,IgvProgram,TotalProgram]                                    
--HLF-20130603-Uso de dbo.FnCambioMoneda para NetoGen, IgvGen, TotalGen                                  
--HLF-20130603-Uso de FnNetoMaServicio_Det para NetoGen                                  
--HLF-20130606-Cambiando PaxmasLiberados                                
--HLF-20130723-Agregando campo od.Activo                              
--HLF-20130807-Agregando campos Vehiculos                            
--JRF-20130808-Agregando valor Moneda Cotizada.                          
--Hlf-20130815-Cambiando DescProveedor_Prg,                         
--agregando Left Join MAUSUARIOS pPrgTrsl On od.IDProveedor_Prg=pPrgTrsl.IDUsuario                        
--HLF-20130820-Cambiando Case When NetoCotizado is null then Por Case When isnull(NetoCotizado,0) = 0 then                      
--Lo mismo para TotalCotizadoCal                      
--Agregando campo IngManual                      
--HLF-20140320-isnull(,0) para NetoGen y TotalGen                  
--HLF-20140422-Case When dCot.IDServicio_Det IS NULL Then                 
--isnull(dCot.Afecto,d.Afecto) as Afecto                
--JRF-HLF-20140513-Reutilizar la relación de la tabla [MASERVICIOS_DET]:[dCot] para el campo Tipo.              
--JRF-20140621-Agregar el campo FlMontosSincerados          
--JRF-20140623-Agregar el campo CoEstadoSincerado          
--HLF-20140723-Select @TipoCambio=SsTipCam From ...        
--Case When isnull(pCot.IDTipoProv,'') NOT IN ('004','005','008')        
--dCot.CoMoneda as IDMonedaGen,        
--dCot.CoMoneda,od.IDMoneda,@TipoCambio)                           
--HLF-20140730-s1.CoPago in('EFE','TKC')...        
--HLF-20140801-,'TKC') And s1.Descripcion Like 'Tips%'        
--HLF-20140804-Se vuelve a isnull(pCot.IDTipoProv,'') as IDTipoProv        
--JRF-20150106-Solo para casos de Translivik cargar ambos correos.  
--JRF-20150223-Considerar [FlServicioNoShow]  
--JHD-20150411-IsNull((Select NoVehiculoCitado + ' - ' + CAST(QtCapacidad as varchar(3))+' Pax' From MAVEHICULOS_TRANSPORTE Where NoVehiculoCitado = X.IDVehiculo_Prg),'') As VehiculoProgramado,                              
--JRF,JHD-20150429-  End as numeric(12,4))   
--HLF-20150624-od.FlServicioNoCobrado, isnull(od.SSTotalProgramAntNoCobrado,0) as SSTotalProgramAntNoCobrado  
--HLF-20150625-ods.QtPax as PaxmasLiberados,
--HLF-20150701-,X.FlServicioNoShow as FlServicioNoShowWhere
--HLF-20150706-isnull(dCot.Anio,d1.Anio) as Anio_Cot,
--HLF-20150709-dCot.Anio as Anio_Cot,
--HLF-20150908-Case When ISNULL(s1.FlServicioSoloTC,0)=0 Then
--JRF-20160415-Case When IsNull(od.IngManual,0)=1 Then  isnull(pCot.IDTipoProv,p1.IDTipoProv) else isnull(...
CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_ListxIDServicio_Prg]        

 @IDServicio char(8),                                         
 @IDOperacion_Det int                                           
As                                                
 Set NoCount On                                                
                                                  
 Declare @TipoCambio numeric(8,2)                                  
 --Select @TipoCambio=TipoCambio From COTICAB Where                                   
 --IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                                  
         
 Select @TipoCambio=SsTipCam From COTICAB_TIPOSCAMBIO Where                                   
 IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                    
 AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=@IDOperacion_Det)        
         
 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)                                  
                                                  
                                                  
 SELECT IDOperacion_Det, DescProveedor_Cot, DescServicio_Cot, IDEstadoSolicitud,                             
 Case When IDVehiculo_Prg is null Then 'NV' Else 'PA' End as IDEstadoVehiculo,          
 Activo,IDServicio_Prg,                                  
 IDServicio, IDServicio_Det, Variante_Prg, Anio_Cot,DescProveedor_Prg,             
 --IsNull((Select NoUnidadProg + ' - ' + CAST(QtCapacidadPax as varchar(3))+' Pax' From MAVEHICULOSPROGRAMACION Where NuVehiculoProg = X.IDVehiculo_Prg),'') As VehiculoProgramado,                              
 IsNull((Select NoVehiculoCitado + ' - ' + CAST(QtCapacidad as varchar(3))+' Pax' From MAVEHICULOS_TRANSPORTE Where NuVehiculo = X.IDVehiculo_Prg),'') As VehiculoProgramado,                              
 Anio_Prg, IDTipoProv, IDProveedor_Prg,             
 IsNull(cast (X.IDVehiculo_Prg as varchar(3)),'') As IDVehiculoProgramado,                               
 CorreoReservasProveedor_Prg, IDServicio_Det_V_Cot, IDServicio_Det_V_Prg,TotalCotizado, IDMoneda,IDMonedaGen, TipoCambio, Total_PrgEditado,                                  
 TotalProgramado, ObservEdicion, CostoRealEditado,CostoReal,CostoLiberado,Margen,MargenAplicadoEditado,                                           
 MargenAplicado,MargenLiberado,CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,                                              
 TotImpto,Upd,PaxmasLiberados,                                   
                       
 isnull(NetoGen,0) as NetoGen,                  
 isnull(case when IgvCotizado is null then                          
 Cast(Case When Afecto=1 Then NetoGen*(@NuIgv/100) Else 0 End as numeric(8,2))                          
 else IgvCotizado End,0) as IgvGen,                           
                 
 SimboloMoneda,                                  
                   
 isnull(Case when isnull(TotalCotizadoCal,0) = 0 then                          
  Cast(Case When Afecto=1 Then                       
  NetoGen+(NetoGen*(@NuIgv/100))                       
  Else                       
  NetoGen                       
  --End as numeric(8,2))                           
  End as numeric(12,4))                           
 else                          
 TotalCotizadoCal                          
 End,0) as TotalGen,                                  
                   
 NetoProgram, IgvProgram, TotalProgram,                      
 IngManual,                      
 Activo      ,            
 FlMontosSincerados ,          
 Isnull(CoEstadoSincerado,'') as CoEstadoSincerado   ,X.FlServicioNoShow       
 ,X.FlServicioNoShow as FlServicioNoShowWhere
,FlServicioNoCobrado, SSTotalProgramAntNoCobrado  
 FROM                                  
 (                           
 Select od.IDOperacion_Det,   od.IgvCotizado,   od.TotalCotizado as TotalCotizadoCal,                          
 isnull(pCot.NombreCorto,p1.NombreCorto) as DescProveedor_Cot,                                              
 isnull(sCot.Descripcion,d1.Descripcion) as DescServicio_Cot,                                              
 --s.Descripcion as DescServicio_Prg,                                              
 od.IDEstadoSolicitud,                                              
 od.Activo, s.IDServicio as IDServicio_Prg,                                              
 d.IDServicio,                                          
 d.IDServicio_Det,              
 --d2.Tipo as Variante_Prg,                    
 --isnull(dCot.Anio,d1.Anio) as Anio_Cot,
 dCot.Anio as Anio_Cot,
 isnull(dCot.tipo,d1.Tipo) as Variante_Prg,              
               
 isnull(pPrg.NombreCorto,pPrgTrsl.Nombre) as DescProveedor_Prg,                                         
 d2.Anio as Anio_Prg,                                       
 --isnull(pCot.IDTipoProv,'') as IDTipoProv,        
 --Case When s1.CoPago in('EFE','TKC') And s1.Descripcion Like 'Tips%' Then--And isnull(pCot.IDTipoProv,'')='' Then        
-- '004,005,008,017'        
 --Else        
 --isnull(pCot.IDTipoProv,'')
 Case When ISNULL(s1.FlServicioSoloTC,0)=0 Then
	--isnull(pCot.IDTipoProv,'')
	Case When IsNull(od.IngManual,0)=1 Then  isnull(pCot.IDTipoProv,p1.IDTipoProv) else isnull(pCot.IDTipoProv,'') End
 Else
	'021'
 End         
 as IDTipoProv,        
         
 isnull(pPrg.IDProveedor,'') as IDProveedor_Prg,        
 Case When pPrg.IDProveedor = '000467' Or pPrg.IDProveedor='000527' Then ISNULL(pPrg.Email3,'')+Isnull('; '+ pPrg.Email4+';','') else       
 Case When ISNULL(pPrg.Email3,'')='' Then pPrg.Email4 Else pPrg.Email3 End  End      
       
 as CorreoReservasProveedor_Prg,                                            
 isnull(od.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,                                              
 isnull(od.IDServicio_Det_V_Prg,0) as IDServicio_Det_V_Prg,                                              
 od.Total As TotalCotizado,                                              
                                             
 od.IDMoneda As IDMoneda,                                            
 isnull(od.tipoCambio ,0) As TipoCambio,                                            
 --(select TipoCambio from OPERACIONES_DET where IDOperacion_Det = od.IDOperacion_Det) As TipoCambio ,                                             
                                             
 od.Total_PrgEditado ,                                            
 IsNull(od.Total_Prg,0.00) as TotalProgramado,                                              
 IsNull(od.ObservEdicion,'') as ObservEdicion,                                              
 0 as CostoRealEditado,                                             
 od.CostoReal,od.CostoLiberado,od.Margen,                                            
 0 as MargenAplicadoEditado,                                      od.MargenAplicado,od.MargenLiberado,                                              
 --od.SSCR,od.SSCL,od.SSMargen,od.SSMargenLiberado,od.SSTotal,od.STCR,od.STMargen,od.STTotal,                                              
 od.CostoRealImpto                                              
 ,od.CostoLiberadoImpto,od.MargenImpto,od.MargenLiberadoImpto,                                              
 --od.SSCRImpto,od.SSCLImpto,od.SSMargenImpto,od.SSMargenLiberadoImpto,od.STCRImpto,od.STMargenImpto,                                              
 od.TotImpto,'' as Upd,                                 
                                 
 --CAST(op.NroPax AS VARCHAR(3))+                                            
 --Case When isnull(op.NroLiberados,0)=0 then '' else '+'+CAST(op.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,                                

 --op.NroPax+isnull(op.NroLiberados,0) as PaxmasLiberados,
 od.QtPax as PaxmasLiberados,
 --Case When od.IngManual = 0 Then                                   
 Case When isnull(NetoCotizado,0) = 0 then                                  
--  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                                    
--   Case When isnull(dCot.TC,0) = 0 Then 'USD' Else 'SOL' End,od.IDMoneda,@TipoCambio)                           
    Case When dbo.FnNetoMaServicio_Det(od.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)) = 0 Or op.FlServicioTC=1 Then
    od.CostoReal
    Else
    Case When dCot.IDServicio_Det IS NULL Then                 
	 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(od.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                                    
     --Case When isnull(dCot.TC,0) = 0 Then 'USD' Else 'SOL' End,od.IDMoneda,@TipoCambio)                    
     dCot.CoMoneda,od.IDMoneda,@TipoCambio) 
    Else                
     dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                                    
     --Case When isnull(dCot.TC,0) = 0 Then 'USD' Else 'SOL' End,od.IDMoneda,@TipoCambio)                           
     dCot.CoMoneda,od.IDMoneda,@TipoCambio)                           
    End 
    End
                   
  Else                       
 NetoCotizado                       
End 
 As NetoGen,                                    
 --Else                       
 --0                      
 --End As NetoGen,                                    
 --Case When isnull(dCot.TC,0) = 0 Then 'USD' Else 'SOL' End as IDMonedaGen,                          
 ISNULL(dCot.CoMoneda,'') as IDMonedaGen,                          
 isnull(mon.Simbolo,'') as SimboloMoneda,             
 --dCot.Afecto as Afecto,                                    
 isnull(dCot.Afecto,d.Afecto) as Afecto,                                   
 --dbo.FnCambioMoneda(dbo.FnCalculoValorReglade3(rd.TotalGen,rd.CostoReal,od.CostoReal/op.NroPax),rd.IDMoneda,od.IDMoneda,@TipoCambio) as TotalGen ,                                   
                                   
 Isnull(od.NetoProgram,0) as NetoProgram,                                    
 Isnull(od.IgvProgram,0) as IgvProgram,                                    
 Isnull(od.TotalProgram,0) as TotalProgram                              
                                        
 ,od.IDVehiculo_Prg                      
 ,od.IngManual          ,            
 od.FlMontosSincerados  ,          
 Case When od.CoEstadoSincerado is null then           
 --Case When isnull(pCot.IDTipoProv,'')= '006' then 'SP' Else od.CoEstadoSincerado End End as CoEstadoSincerado          
 Case When isnull(pCot.IDTipoProv,'') NOT IN ('004','005','008') then 'SP' Else od.CoEstadoSincerado End End as CoEstadoSincerado,  
 od.FlServicioNoShow,
 od.FlServicioNoCobrado, isnull(od.SSTotalProgramAntNoCobrado,0) as SSTotalProgramAntNoCobrado  
 From OPERACIONES_DET_DETSERVICIOS od Left Join MASERVICIOS_DET d                                                 
 On d.IDServicio_Det = od.IDServicio_Det --and od.IDOperacion_Det = @IDOperacion_Det                                                
 --Left Join MASERVICIOS_DET d2 On ISNULL(od.IDServicio_Det_V_Cot,0)=d2.IDServicio_Det                                                
 Left Join MASERVICIOS_DET d2 On ISNULL(od.IDServicio_Det_V_Prg,0)=d2.IDServicio_Det                                                
 Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio                                                
 --Left Join MAPROVEEDORES pPrg On s.IDProveedor=pPrg.IDProveedor                                           
 Left Join MAPROVEEDORES pPrg On od.IDProveedor_Prg=pPrg.IDProveedor                                        
 Left Join MAUSUARIOS pPrgTrsl On od.IDProveedor_Prg=pPrgTrsl.IDUsuario                        
 --Left Join OPERACIONES_DET_DETSERVICIOS od On d.IDServicio_Det = od.IDServicio_Det and od.IDOperacion_Det = @IDOperacion_Det                                              
 Left Join MASERVICIOS_DET dCot On ISNULL(od.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det                                                
 Left Join MASERVICIOS sCot  On sCot.IDServicio=dCot.IDServicio                                                
 Left Join MAPROVEEDORES pCot On sCot.IDProveedor=pCot.IDProveedor                                                
 Left Join MASERVICIOS_DET d1 On d1.IDServicio_Det=od.IDServicio_Det                                                
 Left Join MASERVICIOS s1  On d1.IDServicio=s1.IDServicio                                                
 Left Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor                                            
 Left Join OPERACIONES_DET op On od.IDOperacion_Det=op.IDOperacion_Det                                            
 Left Join RESERVAS_DET rd On op.IDReserva_Det=rd.IDReserva_Det                                        
 Left Join MAMONEDAS mon on od.IDMoneda=mon.IDMoneda                                                
 Where --d.IDServicio=@IDServicio                                               
 od.IDServicio=@IDServicio AND od.IDOperacion_Det = @IDOperacion_Det                                          
) AS X                         
