﻿CREATE Procedure [dbo].[MATIPOIDENT_Sel_Rpt]
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion, IDIdentidad
	From MATIPOIDENT
	Where (Descripcion Like '%'+@Descripcion+'%' Or LTRIM(RTRIM(@Descripcion))='')
