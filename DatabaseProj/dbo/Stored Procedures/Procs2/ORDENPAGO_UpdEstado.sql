﻿
CREATE Procedure [dbo].[ORDENPAGO_UpdEstado]  
	@IDOrdPag int,  
	@IDEstado char(2),  
	@UserMod char(4)  
As  
	Set Nocount on  
   
	Update ORDENPAGO Set 
		IDEstado=@IDEstado, UserMod=@UserMod, FecMod=GETDATE()  
	Where IDOrdPag=@IDOrdPag  

