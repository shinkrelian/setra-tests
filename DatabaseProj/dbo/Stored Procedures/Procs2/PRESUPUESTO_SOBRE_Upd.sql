﻿--JRF-20150304-Se debe permitir Null en la columna de "CoEjeOpe"
--JRF-20150409-Se debe permitir Null en el CoPrvGui
--JHD-20150826-Actualizar código de Fondo Fijo cuando sea Entregado.
--JHD-20150905-Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and /*CoPrefijo='FO'*/ CtaContable='10211003' ORDER BY FECMOD DESC)
--JHD-20150914-NuCodigo_ER=Case When ltrim(ltrim(@NuCodigo_ER))=''  Then Null Else @NuCodigo_ER End
--JHD-20150914-IF @CoEstado='EN' and isnull(@NuCodigo_ER,'')=''
--JHD-20150922-Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and /*CoPrefijo='FO'*/ NuCodigo_ER='ER40897783' ORDER BY FECMOD DESC)
--JHD-20151020-Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoTipoFondoFijo='ER' and /*CoPrefijo='FO'*/ NuCodigo_ER='ER40897783' ORDER BY CAST(COSAP AS INT) DESC)
--HLF-20160129- @CoProveedor char(6), If @CoProveedor='001554'...'000544'
--HLF-20160311-Exec PRESUPUESTO_SOBRE_Sel_Correlativo_FondoFijo_Output @CoProveedor, @NuCodigo_ER output		NuCodigo_ER=@NuCodigo_ER,
CREATE Procedure [dbo].[PRESUPUESTO_SOBRE_Upd]    
 @NuPreSob int,     
 --@CoPrvInt char(6),    
 @CoPrvGui char(6),    
 @CoEjeOpe char(4),    
 @CoEjeFin char(4),    
 @CoEstado char(2),    
 @FePreSob smalldatetime,  
 @NuCodigo_ER varchar(30)='',
 @CoProveedor char(6),
 @UserMod char(4)     
As    
 Set Nocount On    
     
 Update PRESUPUESTO_SOBRE    
 Set --CoPrvInt=@CoPrvInt,    
  CoPrvGui=Case When Ltrim(Rtrim(@CoPrvGui))='' then Null ELSE @CoPrvGui End,CoEjeOpe=Case When ltrim(rtrim(@CoEjeOpe))='' then Null Else @CoEjeOpe End,    
  CoEjeFin=Case When ltrim(ltrim(@CoEjeFin))='' Then Null Else @CoEjeFin End,    
  CoEstado=@CoEstado,UserMod=@UserMod,FePreSob=@FePreSob, FecMod=GETDATE(),
  NuCodigo_ER=Case When ltrim(ltrim(@NuCodigo_ER))=''  Then Null Else @NuCodigo_ER End   
 Where NuPreSob=@NuPreSob    

 IF @CoEstado='EN' and isnull(@NuCodigo_ER,'')=''
 BEGIN
	DECLARE @NuFondoFijo int
	set @NuFondoFijo=isnull((select NuFondoFijo from PRESUPUESTO_SOBRE Where NuPreSob=@NuPreSob),0)
	if @NuFondoFijo=0
		begin
			--Update PRESUPUESTO_SOBRE    
			----Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and /*CoPrefijo='FO'*/ CtaContable='10211003' ORDER BY FECMOD DESC)
			---- Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and /*CoPrefijo='FO'*/ NuCodigo_ER='ER40897783' ORDER BY FECMOD DESC)
			--Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoTipoFondoFijo='ER' and /*CoPrefijo='FO'*/ NuCodigo_ER='ER40897783' ORDER BY CAST(COSAP AS INT) DESC)
			--Where NuPreSob=@NuPreSob  
			Declare @NuCodigo_ERLimaCusco varchar(30)=''
			If @CoProveedor='001554' --Set. Lima
				Set @NuCodigo_ERLimaCusco='ER40897783'
			If @CoProveedor='000544' --Set. Cusco
				Set @NuCodigo_ERLimaCusco='ER44587677'			
				
			Exec PRESUPUESTO_SOBRE_Sel_Correlativo_FondoFijo_Output @CoProveedor, @NuCodigo_ER output

			Update PRESUPUESTO_SOBRE    
			Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoTipoFondoFijo='ER' and NuCodigo_ER=@NuCodigo_ERLimaCusco
				ORDER BY CAST(COSAP AS INT) DESC),
				NuCodigo_ER=@NuCodigo_ER,
				UserMod=@UserMod,FecMod=GETDATE()
			Where NuPreSob=@NuPreSob  				
		end
 END;


