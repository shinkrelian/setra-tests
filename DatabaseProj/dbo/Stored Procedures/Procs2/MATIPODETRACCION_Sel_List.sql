﻿
----------------------------------------------------------------------------------------------------


CREATE PROCEDURE [MATIPODETRACCION_Sel_List]
	@CoTipoDetraccion char(5)='',
	@NoTipoBien varchar(100)='',
	@TxDescripcion varchar(max)=''

AS
BEGIN
	Select
		[CoTipoDetraccion],
 		[NoTipoBien],
 		[TxDescripcion],
 		[SsTasa],
 		[SsMayor],
 		[flActivo],
 		[UserMod],
 		[FecMod]
 	From [dbo].[MATIPODETRACCION]
	Where 
		(@CoTipoDetraccion='' or [CoTipoDetraccion] like '%'+@CoTipoDetraccion+'%') and
		(@NoTipoBien='' or NoTipoBien like '%'+@NoTipoBien+'%') and
		(@TxDescripcion='' or TxDescripcion like '%'+@TxDescripcion+'%')
END;
