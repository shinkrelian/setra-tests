﻿--HLF-20131126-Actualizando a Where  IDReserva_Det=@IDReserva_DetTMP y comentando parametros  
--JRF-20150409-Agregar FlServicioTC
--HLF-20150528-IDReserva_DetOrigAcomVehi
--JRF-20160311-IDReserva_DetAntiguo..
CREATE Procedure dbo.RESERVAS_DET_InsxAcomodoTMP    
 @IDReserva_DetTMP int,  
 @UserMod char(4),    
 @pIDReserva_Det varchar(50) output    
As    
 Set Nocount On    
     
 Declare @IDReserva_Det int                  
    Execute dbo.Correlativo_SelOutput 'RESERVAS_DET',1,10,@IDReserva_Det output                  
    
 Insert Into RESERVAS_DET     
 (IDReserva_Det,IDReserva,IDFile,IDDet,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,    
 IDServicio,IDServicio_Det,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,    
 Lonche,Almuerzo,Cena,Transfer,IDDetTransferOri,IDDetTransferDes,CamaMat,TipoTransporte,    
 IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,Cantidad,CantidadAPagar,CostoReal,    
 CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,    
 TotalOrig,CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,    
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,IDDetRel,Servicio,IDReserva_DetCopia,    
 IDReserva_Det_Rel,IDEmailRef,IDEmailEdit,IDEmailNew,CapacidadHab,EsMatrimonial,    
 UserMod,IDMoneda,IDGuiaProveedor,NuVehiculo,ExecTrigger,Anulado,RegeneradoxAcomodo,IDReserva_DetAcomodoTMP,
 FlServicioTC,IDReserva_DetOrigAcomVehi,NuViaticoTC,FlServicioIngManual,IDReserva_DetAntiguo)    
 Select --(row_number() over (order by IDReserva,IDReserva_Det))-1 + @IDReserva_Det,    
 @IDReserva_Det,    
 IDReserva,IDFile,IDDet,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,    
 IDServicio,IDServicio_Det,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,    
 Lonche,Almuerzo,Cena,Transfer,IDDetTransferOri,IDDetTransferDes,CamaMat,TipoTransporte,    
 IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,Cantidad,CantidadAPagar,CostoReal,    
 CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,    
 TotalOrig,CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,    
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,IDDetRel,Servicio,IDReserva_DetCopia,    
 IDReserva_Det_Rel,IDEmailRef,IDEmailEdit,IDEmailNew,CapacidadHab,EsMatrimonial,    
 @UserMod,IDMoneda,IDGuiaProveedor,NuVehiculo,ExecTrigger,Anulado,1,IDReserva_Det,Case When IsNull(NuViaticoTC,0) > 0 then 1 else FlServicioTC End,
 IDReserva_DetOrigAcomVehi,NuViaticoTC,Case When IsNull(NuViaticoTC,0) > 0 then 1 else 0 End,IDReserva_DetAntiguo
 From RESERVAS_DET_TMP    
 Where  IDReserva_Det=@IDReserva_DetTMP              
     
 Declare @IDReserva_DetNue varchar(25)=@IDReserva_Det     
     
 Declare @IDReserva_DetTmpOut varchar(25)=    
  (Select IDReserva_DetAcomodoTMP    
 From RESERVAS_DET    
 Where IDReserva_Det=@IDReserva_Det)    
      
 Set @pIDReserva_Det=isnull(@IDReserva_DetTmpOut,'')+'|'+isnull(@IDReserva_DetNue,'')    
