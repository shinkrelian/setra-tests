﻿
Create Procedure dbo.MAUSUARIOS_UpdxProfile_Account
@Profile_id int,
@Account_id int,
@IDUsuario char(4),
@UserMod char(4)
As
	Set NoCount On
	Update MAUSUARIOS
		set Profile_id = @Profile_id,
			Account_id = @Account_id,
			UserMod=@UserMod,
			FecMod=GETDATE()
	Where IDUsuario = @IDUsuario
