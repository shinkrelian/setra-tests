﻿

------------------------------------------------------------------------------------------------------


--JHD-20141127-Agregar el Nuevo campo de TxPisos  
CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_Upd]
	@CoServicio char(8),
	@CoConcepto tinyint,
	@FlServicio bit,
	@FlCosto bit,
	@CoTipoMenu char(3),
	@FlFijoPortable bit,
	@FlPortable bit=null,
	@CoTipoBanio char(3),
	@CoEstadoHab char(3),
	@TxPisos varchar(20),
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[MASERVICIOS_HOTEL]
	Set
		[FlServicio] = @FlServicio,
 		[FlCosto] = @FlCosto,
 		[CoTipoMenu] = @CoTipoMenu,
 		[FlFijoPortable] = @FlFijoPortable,
 		[FlPortable] = @FlPortable,
 		[CoTipoBanio] = @CoTipoBanio,
 		[CoEstadoHab] = @CoEstadoHab,
 		TxPisos = @TxPisos,
 		[UserMod] = @UserMod
 	Where 
		[CoServicio] = @CoServicio And 
		[CoConcepto] = @CoConcepto
END;
