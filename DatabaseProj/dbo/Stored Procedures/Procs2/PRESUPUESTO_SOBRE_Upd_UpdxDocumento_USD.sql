﻿--JHD-20150827- SsDifAceptada_USD=Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,      
CREATE PROCEDURE [dbo].[PRESUPUESTO_SOBRE_Upd_UpdxDocumento_USD]    
	@NuPreSob int,    
	@CoEstado char(2)='',    
	@SsSaldo numeric(12,2)=0,    
	@SsDifAceptada numeric(12,2)=0,    
	@TxObsDocumento varchar(Max)='',    
	@FlDesdeOtraForEgreso bit=0,  
	@UserMod char(4)=''    
As    

	Set NoCount On    
	Update [dbo].[PRESUPUESTO_SOBRE]
	 Set CoEstadoFormaEgreso=@CoEstado,    
	  SsSaldo_USD= Case When @SsSaldo=0 then @SsSaldo Else @SsSaldo End,    
	  --SsDifAceptada_USD=SsDifAceptada, --Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,      
	  SsDifAceptada_USD=Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,      
	  TxObsDocumento=   
	 Case When @FlDesdeOtraForEgreso=0 Then  
	  Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End  
	 Else  
	  TxObsDocumento  
	 End,  
	  UserMod=@UserMod,    
	  FecMod=GETDATE(),    
	  --    
	  UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end    
	Where NuPreSob=@NuPreSob

