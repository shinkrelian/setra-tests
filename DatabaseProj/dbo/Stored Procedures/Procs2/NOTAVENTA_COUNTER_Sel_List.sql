﻿




CREATE PROCEDURE [dbo].[NOTAVENTA_COUNTER_Sel_List]    
@NuNtaVta varchar(6),
@FeNtaVtaInicio smalldatetime,
@FeNtaVtaFin smalldatetime,
@Cliente varchar(80),
@NoNtaVta VARCHAR(150)='',
@CoForPag char(3)='',
@CoAeroLinea char(6)='',
@CoEjecutivo char(4),
@FlActivo bit=0
AS
BEGIN

SELECT     NOTAVENTA_COUNTER.NuNtaVta,
NOTAVENTA_COUNTER.FeNtaVta,
NOTAVENTA_COUNTER.SsNtaVta,
NOTAVENTA_COUNTER.CoCliente,
MACLIENTES.RazonComercial, 
                      --NOTAVENTA_COUNTER.NoNtaVta,
                      --NOTAVENTA_COUNTER.QtPax, 
                      --NOTAVENTA_COUNTER.TxDetalle,
                      --NOTAVENTA_COUNTER.CoSegmento,
                      --NOTAVENTA_COUNTER.SsCosto,
                      --NOTAVENTA_COUNTER.SsUtilidad1, 
                      --NOTAVENTA_COUNTER.SsUtilidad2,
                      
                      --MAFORMASPAGO.Descripcion as FormaPago,
                      --NOTAVENTA_COUNTER.SsPago, 
                      --MAPROVEEDORES.NombreCorto AS Aerolinea,
                     MAUSUARIOS.Nombre AS Ejecutivo, 
                      NOTAVENTA_COUNTER.FlActivo,
                     -- NOTAVENTA_COUNTER.TxObservaciones,
                      CoMoneda=isnull(NOTAVENTA_COUNTER.CoMoneda,''),
                      Moneda=isnull((select Descripcion from MAMONEDAS where IDMoneda=NOTAVENTA_COUNTER.CoMoneda),''),
                      SSTipoCambio=isnull(NOTAVENTA_COUNTER.SSTipoCambio,0)
FROM         NOTAVENTA_COUNTER LEFT JOIN
                      MACLIENTES ON NOTAVENTA_COUNTER.CoCliente = MACLIENTES.IDCliente LEFT JOIN
                      MAPROVEEDORES ON NOTAVENTA_COUNTER.CoAeroLinea = MAPROVEEDORES.IDProveedor LEFT JOIN
                      MAUSUARIOS ON NOTAVENTA_COUNTER.CoEjecutivo = MAUSUARIOS.IDUsuario LEFT JOIN
                      MAFORMASPAGO ON NOTAVENTA_COUNTER.CoForPag = MAFORMASPAGO.IdFor
WHERE

  --(ltrim(rtrim(@NuNtaVta))='' Or ltrim(rtrim(NuNtaVta)) like '%' +ltrim(rtrim(@NuNtaVta)) + '%') and     
  (ltrim(rtrim(@NuNtaVta))='' Or NuNtaVta like '%' +@NuNtaVta + '%') and
    ((Convert(Char(10),@FeNtaVtaInicio,103)='01/01/1900' and Convert(Char(10),@FeNtaVtaFin,103)='01/01/1900') Or          
    (CAST(CONVERT(CHAR(10),FeNtaVta,103)AS SMALLDATETIME) between @FeNtaVtaInicio and @FeNtaVtaFin)) and        
(ltrim(rtrim(@Cliente))='' Or MACLIENTES.RazonComercial like '%' +@Cliente + '%') and     
(ltrim(rtrim(@NoNtaVta))='' Or NOTAVENTA_COUNTER.NoNtaVta like '%' +@NoNtaVta + '%') and     
(ltrim(rtrim(@CoForPag))='' Or CoForPag like '%' +@CoForPag + '%') and     
  (NOTAVENTA_COUNTER.FlActivo = @FlActivo or @FlActivo =0) AND
  (ltrim(rtrim(@CoEjecutivo))='' Or CoEjecutivo like '%' +@CoEjecutivo + '%') and     
  (ltrim(rtrim(@CoAeroLinea))='' Or CoAeroLinea like '%' +@CoAeroLinea + '%')

END;

