﻿--JRF-20140505-Agregar Select de los que tienen acceso a Cotización
CREATE Procedure [dbo].[MAUSUARIOS_Sel_Cbo_UsVentasAsig]  
@bTodos bit     
As    
    
 Set NoCount On    
 Select * from     
 (    
    Select '0000' as IDUsuario,case when @bTodos = 1 then '<TODOS>' Else '' End As Usuario,0 as Ord    
    Union    
    select IDUsuario,Usuario,1 from MAUSUARIOS where Activo='A' and IdArea = 'GR'  
    Union  
    Select IDUsuario,Usuario,1 from MAUSUARIOS   
    Where (Activo='A' and IdArea = 'VS') Or (Activo='I' and IdArea = 'UN')  
    Union
    select distinct u.IDUsuario,u.Usuario,1 As Ord  
	from MAUSUARIOSOPC uo Left Join MAUSUARIOS u On uo.IDUsuario = u.IDUsuario  
	Left Join MAOPCIONES o On uo.IDOpc=o.IDOpc  
	Where uo.Consultar = 1 and u.Activo ='A' and o.Nombre ='mnuIngCotizacion'
	and Not IdArea  In ('SI','VS','UN')
 ) As X     
 Order By Ord,Usuario    
