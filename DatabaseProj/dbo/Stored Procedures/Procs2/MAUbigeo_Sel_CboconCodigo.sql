﻿--HLF-20120920-Cambiar campo Codigo por campo DC    
--JRF-20140625-Agregando la condicion para la descripcion de '<TODOS>'  
CREATE Procedure [dbo].[MAUbigeo_Sel_CboconCodigo]    
 @TipoUbig varchar(20),          
 @IDPais char(6),          
 @bTodos bit,
 @bDescTodos bit        
As    
 Set NoCount On    
     
 SELECT * FROM          
 (          
 Select '000001' as IDUbigeo,     
 --'' as Codigo,    
 Case When @bDescTodos = 0 then 'NO APLICA                                                  |'
 else '<TODOS>                                                  |' End as Descripcion,     
     
 0 as Ord          
 Union          
 Select IDubigeo,    
 --Descripcion+'                                                  |'+Isnull(Codigo,'') as Descripcion,     
 Descripcion+'                                                  |'+Isnull(DC,'') as Descripcion,     
 1 as Ord    
 From MAUBIGEO          
 Where (TipoUbig=@TipoUbig Or LTRIM(RTRIM(@TipoUbig))='') And          
 (IDPais=@IDPais Or LTRIM(RTRIM(@IDPais))='')          
 And IDubigeo<>'000001'    
 ) AS X          
 Where (@bTodos=1 Or Ord<>0)          
 Order by Ord,2    
