﻿create Procedure [dbo].[MAUSUARIOS_Upd_FecCese]               
 @IDUsuario char(4),         
 @FecCese smalldatetime='01/01/1900',
 @UserMod char(4)
As      
      
 Set NoCount On      
      
 UPDATE MAUSUARIOS      
    SET		FecCese=Case When @FecCese='01/01/1900' Then Null Else @FecCese End   
           ,UserMod=@UserMod      
              
 WHERE cast(IDUsuario as int)=Cast(@IDUsuario as int)
