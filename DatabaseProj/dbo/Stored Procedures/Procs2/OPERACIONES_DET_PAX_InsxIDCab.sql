﻿
       
--HLF-20130809-Agregando al subquery And IDReserva In (Select r1.IDReserva From RESERVAS r1       
 --Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E'      
 -- Where IDCab=@IDCab       
 --HLF-20130902-Agregando And Estado<>'XL'     
 --HLF-20140122-Agregando  And r1.Anulado=0       
CREATE Procedure dbo.OPERACIONES_DET_PAX_InsxIDCab        
 @IDCab int,        
 @UserMod char(4)        
As        
 Set Nocount On        
         
 Insert Into OPERACIONES_DET_PAX(IDOperacion_Det,IDPax,UserMod)         
 Select        
 (Select IDOperacion_Det From  OPERACIONES_DET Where IDReserva_Det=d.IDReserva_Det),        
 IDPax,@UserMod From RESERVAS_DET_PAX d Where IDReserva_Det In         
 (Select IDReserva_Det From OPERACIONES_DET Where IDOperacion In         
 (Select IDOperacion From OPERACIONES Where IDCab=@IDCab       
       
  And IDReserva In (Select r1.IDReserva From RESERVAS r1       
 Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E'  And r1.Estado<>'XL'    
 And r1.Anulado=0   
  Where IDCab=@IDCab)        
       
 ))        
       
              