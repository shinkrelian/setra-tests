﻿Create Procedure dbo.RESERVAS_DET_TMP_SelExisteAcomodoOutput	
	@IDReserva int,
	@IDServicio_Det int,
	@CapacidadHab tinyint,
	@EsMatrimonial bit,
	@Dia smalldatetime,
	@pExiste bit Output
 As
	Set Nocount On
	
	Set @pExiste = 0
	
	If Exists(Select IDReserva_Det From RESERVAS_DET_TMP 
		Where IDReserva=@IDReserva 
		And IDServicio_Det=@IDServicio_Det And CapacidadHab=@CapacidadHab
		And EsMatrimonial=@EsMatrimonial
		And Dia=@Dia)
	
		Set @pExiste = 1
		
