﻿Create Procedure dbo.MAPLANCUENTAS_Sel_PK
@CtaContable varchar(8)
As
	Set NoCount On
	Select * from MAPLANCUENTAS
	Where CtaContable = @CtaContable
