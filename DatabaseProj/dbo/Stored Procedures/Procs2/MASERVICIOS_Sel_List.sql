﻿
--JRF-20120913-Agregar Filtro de Tipo de Gasto                  
--HLF-20120906-Nuevos parametro @IDCab y Where                    
--HLF-20121105-Cambiar u.IDPais por us.IDPais                  
--HLF-20130123-IDCabVarios                
--HLF-20130305-Agregando And ((@IDCab=0 And s.IDCabVarios is null) Or s.IDCabVarios=@IDCab)                    
--HLF-20130315-Agregando IDFormaPago            
--JRF-20130717-Validando proveedores Activos.          
--HLF-20130916-Agregando pinternac.NombreCorto as DescProvInternac,        
--JRF-20140403-Agregar la columna de IDCabVarios.      
--HLF-20140708-Subquery SsTipCam    
--HLF-20140715-CoMonedaPais, SimboloMonedaPais  
--JRF-20150306-Agregar Filtro x CoPago
--JRF-20150423-Agregar el CoPago
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-And (us.IDPais=@CoPais Or LTRIM(rtrim(@CoPais))='')
--PPMG-20151110-DocPdfRutaPdf
--PPMG-20151214-HoraPredeterminada
--HLF-20160125-Web, Direccion, Telefono1, Telefono2, HoraCheckIn, HoraCheckOut, IDDesayuno
CREATE Procedure [dbo].[MASERVICIOS_Sel_List]                     
 @IDServicio char(8),                      
 @IDProveedor char(6),                      
 @RazonSocial varchar(100),                      
 @Descripcion varchar(100),                      
 @SoloActivos bit,                       
 @IDTipoProv char(3),                      
 @Anio char(4),                      
                    
 @IDCiudad char(6),                      
 @Categoria tinyint,                      
 @IDCat char(3),                    
 @IDCab int  = 0  ,                  
 @TipoGasto char(1) = '',
 @CoPago char(3),
 @CoPais char(6)='' 
As                      
BEGIN
 Set NoCount On                      
                    
 SELECT Distinct Descripcion, IDTipoServ,                      
 RazonSocial, DescProvInternac,          
 IDServicio, IDCiudad,DescCiudad,Lectura,IDTipoProv,                       
 DescTipoServ,IDProveedor,CorreoReservas,CorreoReservas2,IDPais, IDTipoOper,                      
 Desayuno, Lonche, Almuerzo, Cena, Transfer, TipoTransporte,                      
 isnull(IDUbigeoOri,'') as IDUbigeoOri, isnull(IDUbigeoDes,'') as IDUbigeoDes,            
 IDFormaPago ,IDCabVarios,SsTipCam,CoMonedaPais,SimboloMonedaPais , IsNull(CoPago,'') as CoPago
 , DocPdfRutaPdf, HoraPredeterminada,
 Web, Direccion, Telefono1, Telefono2, HoraCheckIn, HoraCheckOut, IDDesayuno
 FROM                      
 (                      
 Select s.Descripcion, s.IDTipoServ,                      
 p.NombreCorto  as RazonSocial,                
 pinternac.NombreCorto as DescProvInternac,        
 s.IDServicio, s.IDubigeo as IDCiudad , Cast(s.Lectura as varchar(max)) as Lectura,p.IDTipoProv,                      
 us.Descripcion as DescCiudad,                    
 ts.Descripcion as DescTipoServ,s.IDProveedor,                    
 --ISNull(p.Email3,'') as CorreoReservas,                    
 ISNULL(p.Email3,'') as CorreoReservas,                    
 ISNULL(p.Email4,'') as CorreoReservas2,                    
 us.IDPais, IsNull(p.IDTipoOper,'') as IDTipoOper,                      
 ISNULL(sd.Anio,'') as Anio,                      
 Isnull(al.Desayuno,0) as Desayuno, Isnull(al.Lonche,0) as Lonche,                       
 Isnull(al.Almuerzo,0) as Almuerzo, Isnull(al.Cena,0) as Cena,                      
 s.Transfer, Isnull(s.TipoTransporte,'') as TipoTransporte,                      
 al.IDUbigeoOri, al.IDUbigeoDes, s.ServicioVarios,            
 p.IDFormaPago  , Isnull(s.IDCabVarios,0) as IDCabVarios,    
 ISNULL((SELECT TOP 1 SsTipCam FROM MATIPOSCAMBIO_USD WHERE CoMoneda IN    
 (SELECT CoMoneda FROM MAUBIGEO WHERE IDubigeo=US.IDPais)    
 ORDER BY FeTipCam DESC),0) AS SsTipCam,   
 usp.CoMoneda as CoMonedaPais, mn.Simbolo as SimboloMonedaPais    ,s.CoPago
 , ISNULL(p.DocPdfRutaPdf,'') AS DocPdfRutaPdf, ISNULL(s.HoraPredeterminada,CONVERT(datetime,'19000101 00:00:00')) AS HoraPredeterminada
 ,isnull(p.Web,'') as Web,isnull(p.Direccion,'') as Direccion ,isnull(p.Telefono1,'') as Telefono1, isnull(p.Telefono2,'') as Telefono2, 
 isnull(CONVERT(char(5),s.HoraCheckIn,108),'') as HoraCheckIn, 
 isnull(CONVERT(char(5),s.HoraCheckOut,108),'') as HoraCheckOut, 
 isnull(s.IDDesayuno,'') as IDDesayuno
 From MASERVICIOS s                       
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor                      
 LEft Join MAPROVEEDORES pinternac On p.IDProveedorInternacional=pinternac.IDProveedor        
 Left Join MASERVICIOS_DET sd On s.IDServicio=sd.IDServicio                       
 --Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo                      
 Left Join MAUBIGEO us On s.IDubigeo=us.IDubigeo                    
 Left Join MATIPOSERVICIOS ts On s.IDTipoServ=ts.IDTipoServ   
 Left Join (Select IDServicio,IDUbigeoOri, IDUbigeoDes,                    
 Desayuno, Lonche, Almuerzo, Cena                       
 From MASERVICIOS_ALIMENTACION_DIA                       
 Where Dia=1) al On al.IDServicio=s.IDServicio         
 Left Join MAUBIGEO usp On us.IDPais=usp.IDubigeo  
 Left Join MAMONEDAS mn On usp.CoMoneda=mn.IDMoneda               
 Where                       
 (s.IDServicio=@IDServicio Or LTRIM(rtrim(@IDServicio))='') And                      
 (s.IDProveedor=@IDProveedor Or LTRIM(rtrim(@IDProveedor))='') And                      
 (LTRIM(rtrim(@Descripcion))='' Or s.Descripcion Like '%'+@Descripcion+'%')                      
 And (@SoloActivos=0 Or s.Activo=1)                      
 And s.cActivo='A' and p.Activo = 'A'                       
 And (LTRIM(rtrim(@IDTipoProv))='' Or p.IDTipoProv=@IDTipoProv)                       
                    
 And (s.IDubigeo=@IDCiudad Or LTRIM(rtrim(@IDCiudad))='')                      
 And (s.Categoria=@Categoria Or @Categoria=0)                      
 And (s.IDCat=@IDCat Or LTRIM(rtrim(@IDCat))='')                      
 
  And (us.IDPais=@CoPais Or LTRIM(rtrim(@CoPais))='')  
                     
 --And ((@IDCab=0 And sd.ServicioVarios=0))                          
 And (@IDCab=0 Or s.IDCabVarios=@IDCab)                     
 And (@IDCab=0 And s.IDCabVarios is null) Or (s.IDCabVarios=@IDCab And s.cActivo='A')
 ) AS X                       
 WHERE                      
 (LTRIM(rtrim(@RazonSocial))='' Or RazonSocial Like '%'+@RazonSocial+'%')                       
 And (Anio=@Anio Or LTRIM(rtrim(@Anio))='' Or Anio='')                      
 --And (@IDCab>0 OR ServicioVarios=0)                               
 And (LTRIM(rtrim(@TipoGasto))='' Or                   
 Exists(select IDServicio_Det from MASERVICIOS_DET dt2 Where TipoGasto = @TipoGasto and X.IDServicio = dt2.IDServicio))                  
 And (Ltrim(Rtrim(@CoPago))='' Or X.CoPago=@CoPago)         
 Order by Descripcion                          
END

