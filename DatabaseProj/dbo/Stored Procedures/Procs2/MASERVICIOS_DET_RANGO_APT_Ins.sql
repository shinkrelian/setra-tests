﻿

--PPMG-20151105-Se agrego el parametro @IDTemporada
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_Ins]   
    @IDServicio_Det int,  
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(10,4),  
    @UserMod char(4),
	@IDTemporada int
AS
BEGIN
Set NoCount On
IF NOT EXISTS(SELECT IDServicio_Det, IDTemporada FROM MASERVICIOS_DET_DESC_APT WHERE IDServicio_Det=@IDServicio_Det AND IDTemporada = @IDTemporada)
BEGIN
	INSERT INTO MASERVICIOS_DET_DESC_APT(IDServicio_Det, IDTemporada, DescripcionAlterna, UserMod, FecMod)
	VALUES(@IDServicio_Det, @IDTemporada, '', @UserMod, GETDATE())
END
Declare @Correlativo tinyint=(Select IsNull(MAX(Correlativo),0)+1   
From MASERVICIOS_DET_RANGO_APT Where IDServicio_Det=@IDServicio_Det AND IDTemporada = @IDTemporada)
   
INSERT INTO MASERVICIOS_DET_RANGO_APT  
        ([Correlativo]  
        ,[IDServicio_Det]  
        ,[PaxDesde]  
        ,[PaxHasta]  
        ,[Monto]  
        ,[UserMod]
		,[IDTemporada]
        )  
    VALUES  
        (@Correlativo,  
        @IDServicio_Det,  
        @PaxDesde,  
        @PaxHasta,  
        @Monto,  
        @UserMod,
		@IDTemporada
        )  
END
