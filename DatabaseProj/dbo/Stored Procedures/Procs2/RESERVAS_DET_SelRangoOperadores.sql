﻿--JRF-20160412- ...And r.Anulado=0 And r.Estado <> 'XL'..
CREATE Procedure dbo.RESERVAS_DET_SelRangoOperadores
@IDReserva int,
@IDCab int
As
	Set NoCount On
	select * from(
	select distinct p.NombreCorto ,convert(char(10),rdb.Dia,103) as FechaIn from RESERVAS_DET rdb Left Join RESERVAS r On rdb.IDReserva = r.IDReserva
	Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor
	where cast(CONVERT(char(10),rdb.Dia,103) as smalldatetime) In (
	select cast(CONVERT(char(10),rd.Dia,103) as smalldatetime) as Dia
	from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva 
		 Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor
		 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det
	Where r.IDCab = @IDCab and r.IDReserva = @IDReserva) 
	AND R.IDCab = @IDCab and r.IDReserva <> @IDReserva and p.IDTipoProv <> '001'--and sd.PlanAlimenticio = 0)
	And r.Anulado=0 And r.Estado <> 'XL'
	and p.IDTipoProv <> '002'
	) as X
	order by CAST(x.FechaIn as smalldatetime)
