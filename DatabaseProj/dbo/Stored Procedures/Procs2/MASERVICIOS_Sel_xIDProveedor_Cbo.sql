﻿CREATE Procedure [dbo].[MASERVICIOS_Sel_xIDProveedor_Cbo]
	@IDProveedor	char(6)
As
	Set NoCount On
	
	Select s.IDServicio, s.Descripcion  From MASERVICIOS s 	
	Where s.IDProveedor=@IDProveedor And s.cActivo='A'
	Order by s.Descripcion
