﻿
Create Procedure dbo.NOTA_VENTA_DET_RptCosto_GastoAdm 
@IDNotaVenta char(10)
As
	Set NoCount On
	
	 Select  toc.Orden,sd.idTipoOC,  
			 toc.Descripcion as DescTipoOC,  
			 p.NombreCorto As DescProveedor,  
			 nvd.Descripcion As Servicio,  
			 nvd.SubTotal As Neto,  
			 nvd.TotalIGV As Igv,  
			 nvd.Total AS Total,
			 (select SUM(Total) from DEBIT_MEMO 
			  where IDCab = (select IDCab from NOTA_VENTA 
			  where IDNotaVenta= @IDNotaVenta)) As TotalDebitMemo,
			  --Campo Calc C2
			  ((select SUM(Total) from DEBIT_MEMO 
			  where IDCab = (select IDCab from NOTA_VENTA 
			  where IDNotaVenta= @IDNotaVenta))
			  -
			   (select SUM(nvd2.Total) from NOTA_VENTA_DET nvd2
			    Left Join RESERVAS_DET rd2 On nvd2.IDReserva_Det=rd2.IDReserva_Det  
				Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det=sd2.IDServicio_Det
				Where nvd2.IDNotaVenta =@IDNotaVenta and sd2.idTipoOC <> '005')
			  +
			  (select SUM(nvd2.TotalIGV) from NOTA_VENTA_DET nvd2
			    Left Join RESERVAS_DET rd2 On nvd2.IDReserva_Det=rd2.IDReserva_Det  
				Left Join MASERVICIOS_DET sd2 On rd2.IDServicio_Det=sd2.IDServicio_Det
				Where nvd2.IDNotaVenta =@IDNotaVenta and sd2.idTipoOC = '001')
			   ) As ColReptC2
			   
	 From NOTA_VENTA_DET nvd   
	 Left Join RESERVAS_DET rd On nvd.IDReserva_Det=rd.IDReserva_Det  
	 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det  
	 Left Join MATIPOOPERACION toc On toc.IdToc=sd.idTipoOC  
	 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
	 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor  
	 Where nvd.IDNotaVenta = @IDNotaVenta And sd.idTipoOC = '005'
	 Order by toc.Orden  
