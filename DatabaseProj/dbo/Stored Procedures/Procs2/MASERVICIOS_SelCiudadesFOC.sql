﻿Create Procedure dbo.MASERVICIOS_SelCiudadesFOC
	@IDCab int,
	@CoTipoAPT char(2)
As
	Set Nocount on

	SELECT X.IDubigeo,X.DescCiudad  + space(100) + '|'+isnull(up.Descripcion,'') as  DescCiudad 
	FROM
	(
	SELECT s.IDubigeo,u.Descripcion as  DescCiudad, u.IDPais 
	FROM MASERVICIOS s inner join MAUBIGEO u 
		on u.IDubigeo=s.IDubigeo 		
	 Inner Join COTIDET cd On s.IDServicio=cd.IDServicio And cd.IDCAB=@IDCab
	 WHERE APTServicoOpcional=Case @CoTipoAPT When 'SE' Then 'E' When 'FC' then 'F' End
	group by s.IDubigeo,u.Descripcion, u.IDPais
	) AS X Left Join MAUBIGEO up On X.IDPais=up.IDubigeo
	Order by 2

