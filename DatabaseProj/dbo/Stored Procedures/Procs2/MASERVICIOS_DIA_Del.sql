﻿Create Procedure [dbo].[MASERVICIOS_DIA_Del]
	@IDServicio char(8),
	@Dia tinyint,
	@IDIdioma varchar(12)
As
	Set NoCount On
	
	Delete From MASERVICIOS_DIA           
     Where
           IDServicio=@IDServicio And
           Dia=@Dia And
           IDIdioma=@IDIdioma
