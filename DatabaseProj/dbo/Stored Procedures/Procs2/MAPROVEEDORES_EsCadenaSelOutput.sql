﻿Create Procedure dbo.MAPROVEEDORES_EsCadenaSelOutput
@IDProveedor char(6),
@pblnEsCadena bit Output
As
 Set @pblnEsCadena = (Select EsCadena from MAPROVEEDORES Where IDProveedor = @IDProveedor)
