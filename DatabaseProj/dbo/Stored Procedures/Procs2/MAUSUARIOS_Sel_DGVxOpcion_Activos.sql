﻿---MAUSUARIOS_Sel_CboxOpcion_Activos 'mnuIngCotizacion',0,'' --mnuIngOperacionesControlporFile
---MAUSUARIOS_Sel_DGVxOpcion_Activos 'mnuIngCotizacion',0,1 --mnuIngOperacionesControlporFile
CREATE Procedure [dbo].[MAUSUARIOS_Sel_DGVxOpcion_Activos]
@DescOpcion varchar(50),  
@bTodos bit,
@FlTrabajador_Activo bit
As  
 Set NoCount On  
 
 select IDUsuario,Usuario,FlTrabajador_Activo AS Activo from (  

 select distinct u.IDUsuario,u.Usuario,u.FlTrabajador_Activo,1 As Ord  
 from MAUSUARIOSOPC uo Left Join MAUSUARIOS u On uo.IDUsuario = u.IDUsuario  
 Left Join MAOPCIONES o On uo.IDOpc=o.IDOpc  
 Where uo.Consultar = 1 --and u.Activo ='A'   
 and   (FlTrabajador_Activo = @FlTrabajador_Activo or @FlTrabajador_Activo=0) 
 and LTRIM(rtrim(o.Nombre))=LTRIM(rtrim(@DescOpcion))  
 and IdArea <> 'SI'  

 Union
 Select IDUsuario,Usuario,FlTrabajador_Activo,1 as Ord from MAUSUARIOS   
 Where (Activo='I' and IdArea = 'UN') 

 ) As X  
 Where (@bTodos = 1 Or X.Ord <> 0)  
 --and   (FlTrabajador_Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') 
 and   (FlTrabajador_Activo = @FlTrabajador_Activo or @FlTrabajador_Activo=0) 
 order by X.Usuario --X.IDUsuario  
