﻿Create Procedure dbo.ORDENCOMPRA_Ins   
 @CoProveedor char(6),  
 @FeOrdCom smalldatetime,  
 @CoMoneda char(3),  
 @SsImpuestos numeric(8,2) ,  
 @SsTotal numeric(8,2),  
 @UserMod char(4),  
 @pNuOrdComInt int output  
  
As  
 Set Nocount On  
   
   
 Declare @AnioMes char(7)=cast(year(GETDATE()) as CHAR(4))+upper(CAST(GETDATE() AS nvarchar(3)))  
 Declare @vcNuOrdCom varchar(3)  
 Select @vcNuOrdCom=isnull(MAX(CAST(RIGHT(NuOrdCom,3) as smallint)),0)+1 From ORDENCOMPRA Where Left(NuOrdCom,7)=@AnioMes   
 Set @vcNuOrdCom=REPLICATE('0',3-len(@vcNuOrdCom)) +@vcNuOrdCom  
   
 Declare @NuOrdComInt int,@NuOrdCom char(13)=@AnioMes+'COM'+@vcNuOrdCom  
   
 Exec Correlativo_SelOutput 'ORDENCOMPRA',1,10,@NuOrdComInt output             
 --2015 ENECOM-023  
   
 --select convert(varchar,GETDATE(),126)  
 --select cast(year(GETDATE()) as CHAR(4))  
 --select upper(CAST(GETDATE() AS nvarchar(3)))  
   
 Insert Into ORDENCOMPRA(NuOrdComInt, NuOrdCom, CoProveedor,FeOrdCom, CoMoneda, SsImpuestos, SsTotal, SsSaldo, SsDifAceptada, UserMod)  
 Values(@NuOrdComInt, @NuOrdCom, @CoProveedor, @FeOrdCom, @CoMoneda, @SsImpuestos, @SsTotal, @SsTotal, 0, @UserMod)  
   
 Set @pNuOrdComInt=@NuOrdComInt  
