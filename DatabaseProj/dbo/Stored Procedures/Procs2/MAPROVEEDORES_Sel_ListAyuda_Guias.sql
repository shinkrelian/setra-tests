﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505- and ((IDPais)=@CoPais or (rtrim(ltrim(@CoPais))=''))
--MAPROVEEDORES_Sel_ListAyuda_Guias
--alter
CREATE Procedure [dbo].[MAPROVEEDORES_Sel_ListAyuda_Guias]    
 --@IDTipoProv char(3),    
 @RazonSocial varchar(100)='',    
 @IDCiudad char(6)='' ,
 @CoPais char(6)=''    
 --@IDCat char(3)='',    
 --@Categoria tinyint =0
As    
 Set NoCount On    
    
 SELECT Distinct * FROM    
 (    
 Select     
 --Case When @IDTipoProv='001' Then p.NombreCorto Else p.RazonSocial End as Descripcion,    
 p.NombreCorto as Descripcion,    
 p.IDProveedor as Codigo,     
 p.IDTipoProv as CodTipo, t.Descripcion as DescTipo, p.IDCiudad, u.IDPais,
 Ciudad=u.Descripcion,
 Pais=(select Descripcion from MAUBIGEO where IDubigeo=u.idpais),
  IsNull(p.Margen,0) as Margen,
 Isnull(pInt.NombreCorto,'') as OperadorInt    
 From MAPROVEEDORES p Left Join MATIPOPROVEEDOR t On p.IDTipoProv=t.IDTipoProv    
 Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo    
 Left Join MASERVICIOS s On p.IDProveedor=s.IDProveedor    
 Left Join MAPROVEEDORES pInt On p.IDProveedorInternacional = pInt.IDProveedor
 Where     
 --(p.IDTipoProv=@IDTipoProv Or LTRIM(rtrim(@IDTipoProv))='' )  
 p.IDTipoProv in ('005','008')
 And (p.IDCiudad=@IDCiudad Or LTRIM(rtrim(@IDCiudad))='')    
 --And (s.IDCat=@IDCat Or LTRIM(rtrim(@IDCat))='')    
 --And (s.Categoria=@Categoria OR @Categoria=0) 
 And p.Activo = 'A'  
 ) AS X    
 Where (Descripcion Like '%'+@RazonSocial+'%' Or LTRIM(rtrim(@RazonSocial))='')    
  and ((IDPais)=@CoPais or (rtrim(ltrim(@CoPais))=''))
 Order by Descripcion    

