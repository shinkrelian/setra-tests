﻿--HLF-20130718-Agregando  and od.IDReserva_Det=rs.IDReserva_Det al Inner Join OPERACIONES_DET od  
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_InsxIDReserva_Det     
 @IDReserva_Det int,         
 @UserMod char(4)        
As        
 Set Nocount On        
      
    
 Insert Into OPERACIONES_DET_DETSERVICIOS        
 (IDOperacion_Det,IDServicio_Det, IDServicio,      
 IDServicio_Det_V_Cot,        
 CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,        
 SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,        
 MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,        
 STMargenImpto,TotImpto,      
 TipoCambio,IDMoneda,      
 UserMod)          
 Select Distinct od.IDOperacion_Det, rs.IDServicio_Det, sd.IDServicio,         
 --(Select IDServicio_Det_V From MASERVICIOS_DET Where IDServicio_Det=rs.IDServicio_Det),        
 sd.IDServicio_Det_V,      
 rs.CostoReal*od.NroPax,rs.CostoLiberado*od.NroPax,rs.Margen*od.NroPax,rs.MargenAplicado,rs.MargenLiberado*od.NroPax,rs.Total*od.NroPax,        
 rs.SSCR*od.NroPax,rs.SSCL*od.NroPax,rs.SSMargen*od.NroPax,rs.SSMargenLiberado*od.NroPax,rs.SSTotal*od.NroPax,rs.STCR*od.NroPax,        
 rs.STMargen*od.NroPax,rs.STTotal*od.NroPax,rs.CostoRealImpto*od.NroPax,rs.CostoLiberadoImpto*od.NroPax,        
 rs.MargenImpto*od.NroPax,rs.MargenLiberadoImpto*od.NroPax,rs.SSCRImpto*od.NroPax,rs.SSCLImpto*od.NroPax,        
 rs.SSMargenImpto*od.NroPax,rs.SSMargenLiberadoImpto*od.NroPax,rs.STCRImpto*od.NroPax,rs.STMargenImpto*od.NroPax,        
 rs.TotImpto*od.NroPax,      
 sd.TC, Case When sd.TC IS NULL Then 'USD' Else 'SOL' End,       
 @UserMod        
 From RESERVAS_DETSERVICIOS rs Inner Join RESERVAS_DET rd On rs.IDReserva_Det=@IDReserva_Det    
 --Inner Join RESERVAS r On r.IDReserva=rd.IDReserva And r.IDCab=@IDCab And r.IDProveedor=@IDProveedor        
 Inner Join OPERACIONES_DET od On rd.IDReserva_Det=od.IDReserva_Det and od.IDReserva_Det=rs.IDReserva_Det  
 Inner Join MASERVICIOS_DET sd On rs.IDServicio_Det=sd.IDServicio_Det        
 where rs.Total>0        
