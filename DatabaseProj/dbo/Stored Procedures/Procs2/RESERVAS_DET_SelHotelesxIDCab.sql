﻿




--HLF-20130510-Agregando Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det And sd.PlanAlimenticio=0  
--y Inner Join COTIDET cd On rd.IDDet=cd.IDDET And cd.IncGuia=0  
--HLF-20130521-Agregando subquery OperadorConAlojam y WHERE X.IDTipoProv='001' OR (X.IDTipoProv='003' And X.OperadorConAlojam>=1)
CREATE Procedure dbo.RESERVAS_DET_SelHotelesxIDCab    
 @IDCab int     
As    
 Set NoCount On    
  
 SELECT distinct IDReserva    
 FROM
 (
 Select r.IDReserva,p.IDTipoProv,
 
  (Select COUNT(*) From RESERVAS_DET rd1               
 Inner Join RESERVAS r1 On rd1.IDReserva=r1.IDReserva And r1.IDReserva=r.IDReserva              
 Inner Join MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor And p1.IDTipoProv='003'              
 Inner Join MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det And sd1.ConAlojamiento=1              
            
 ) as OperadorConAlojam             
     
 From RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva     
    Inner Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det And sd.PlanAlimenticio=0  
    Inner Join COTIDET cd On rd.IDDet=cd.IDDET And cd.IncGuia=0 
    Inner Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor  
 Where r.IDCab=@IDCab
 --rd.IDReserva in     
 --(Select r1.IDReserva From RESERVAS r1     
 --Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoProv='001'     
 --Where r1.IDCab=@IDCab ) 
 ) AS X
 WHERE X.IDTipoProv='001' OR (X.IDTipoProv='003' And X.OperadorConAlojam>=1)              
 
