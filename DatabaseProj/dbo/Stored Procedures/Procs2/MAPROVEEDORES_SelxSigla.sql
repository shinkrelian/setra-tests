﻿
CREATE PROCEDURE MAPROVEEDORES_SelxSigla
	@Sigla char(3)
AS      
BEGIN
	SET NOCOUNT ON   
       
	SELECT IDProveedor, NombreCorto
	FROM MAPROVEEDORES
	WHERE Sigla = @Sigla
END
