﻿--drop proc OPERACIONES_DET_SelxVoucher
--go
CREATE Procedure [dbo].[OPERACIONES_DET_SelExisteVoucherOutput]
	@IDVoucher  int,
	@pExiste bit output
As
	Set NoCount On
	
	if exists (select IDVoucher from OPERACIONES_DET Where IDVoucher=@IDVoucher)
		set @pExiste = 1
	Else
		set @pExiste = 0	
