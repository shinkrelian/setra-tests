﻿
Create Procedure dbo.RESERVAS_DET_SelFechasxServicio
	@IDCab	int,
	@IDProveedor char(6),
	@IDServicio_Det int
As
	Set Nocount On
	
	SELECT Distinct Dia,FechaOut FROM RESERVAS_DET
	WHERE IDReserva IN (SELECT IDReserva FROM RESERVAS WHERE IDCab=@IDCab 
	AND IDProveedor=@IDProveedor)
	and IDServicio_Det=@IDServicio_Det
