﻿

--create
CREATE PROCEDURE ORDENCOMPRA_DET_Upd
	@NuOrdComInt_Det int,
	@NuOrdComInt int,
	@TxServicio varchar(max),
	@SSCantidad numeric(8,2),
	@SSPrecUnit numeric(8,2),
	@SSTotal numeric(8,2),
	@UserMod char(4)
AS
BEGIN
	Update [dbo].[ORDENCOMPRA_DET]
	Set
		[NuOrdComInt] = @NuOrdComInt,
 		[TxServicio] = @TxServicio,
 		[SSCantidad] = @SSCantidad,
 		[SSPrecUnit] = @SSPrecUnit,
 		[SSTotal] = @SSTotal,
 		[UserMod] = @UserMod,
 		[FecMod] = GETDATE()
 	Where 
		[NuOrdComInt_Det] = @NuOrdComInt_Det
END
