﻿--JRF-20150312- Considerar la Tarifa ingresa para los vuelos
CREATE Procedure dbo.ReporteComparaVentasCliente_SubInfxClientexMes    
@IDCliente char(6),    
@Top tinyint    
As    
Set NoCount On    
select --DATENAME(M,Cast(Right(Left(convert(varchar,X.FechaOutPeru,112),6),2) as tinyint)) as NombreMes,    
    X.Cliente,Cast(Year(x.FechaOutPeru) as varchar) as Anio,    
    SubString(DATENAME(M,X.FechaOutPeru),1,3)as NombreMes,    
    X.Cotizacion,X.NroFile_Cotizacion,X.Titulo,X.NroPaxTotales,X.PoConcretVta,    
    Case When DATEDIFF(d,X.FechaOutPeru,GetDate())>0 Then    
  X.TotalDebitMemo    
    else    
     Case When X.TotalDebitMemo=0 Then X.TotalVenta * (X.PoConcretVta/100)    
     Else    
   Case When X.Estado='X' Then X.TotalDebitMemo    
   Else    
    Case When ABS((Case When X.TotalVenta=0 Then 0 Else (((X.TotalDebitMemo)*100)/X.TotalVenta) End )) >=15 Then X.TotalDebitMemo    
    Else    
     X.TotalVenta*(X.PoConcretVta/100)    
    End    
   End    
     End    
    End as TotalVenta    
from (    
Select cc.estado,cc.IDCAB, isnull((Select Max(Dia) From COTIDET cd1               
      Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo               
      And u1.IDPais='000323'              
      Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det              
      Where IDCAB=cc.IDCAB /*And SD1.idTipoOC<>'005'*/),cc.FechaOut) as FechaOutPeru,              
      cc.PoConcretVta,                
      cc.NroPax,                 
      isnull((Select sum(total*nropax) From Cotidet Where idcab=cc.idcab),0)+IsNull(dbo.FnDevuelveMontoTotalxPaxVuelos(cc.IDCAB,1),0) as TotalVenta,  
      ISNULL((Select SUM(dm1.Total) From DEBIT_MEMO dm1 Left Join MACLIENTES c1 On dm1.IDCliente=c1.IDCliente    
      Where dm1.IDCab=cc.idcab         
      And dm1.IDEstado<>'AN'       
      --And IDCliente=@IDCliente)      
      And CHARINDEX(c1.RazonComercial,dm1.Cliente)<>0)      
      ,0) as TotalDebitMemo     
      ,Ltrim(Rtrim(c2.RazonComercial)) as Cliente,  
      cc.Cotizacion,cc.IDFile as NroFile_Cotizacion,cc.Titulo,cc.NroPax+ISNULL(cc.NroLiberados,0) as NroPaxTotales  
      From COTICAB cc Left Join MACLIENTES c2 On cc.IDCliente=c2.IDCliente    
      Where (Ltrim(Rtrim(@IDCliente))='' Or cc.IDCliente=@IDCliente)  
     And cc.CoTipoVenta='RC' --And cc.IDCliente=@IDCliente              
--     and (cc.Estado='A') or           
--  ((cc.Estado='X' and cc.IDCAB in (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG')))    
     and (cc.Estado='A' or (cc.Estado='X' and cc.IDCAB in (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG')))  
) as X    
Order By X.FechaOutPeru    
--select * from COTICAB    
