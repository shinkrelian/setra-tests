﻿--JRF-20151109-Ajustar la razon social...
CREATE Procedure dbo.MAPROVEEDORES_GetMassiveAPI
As
Set NoCount On
select CardCodeSAP,
Case When p.Persona='N' then p.Nombre+' '+p.ApPaterno+ ' '+ Replace(Replace(p.ApMaterno,'-',''),'?','')
Else Case When p.Persona='J' And Replace(IsNull(p.RazonSocial,''),'-','') ='' then p.NombreCorto else p.RazonSocial End End as CardName,
1 as CardType,dbo.FnDevuelvoGroupCode('P',case when u.IDPais != '000323' Then 'PEX' Else 'P' End) As GroupCode,
--SUBSTRING(CardCodeSAP,1,9) as FederalTaxID,
Case When charindex('EX',p.CardCodeSAP) > 0 then CardCodeSAP else
Case When P.IDTipoProv In('005','017','020') then
  Case When p.RUC is not null then p.RUC else
  Case When p.IDIdentidad In ('003','004','005') then IsNull(p.NumIdentidad,'') Else IsNull(P.RUC ,'') End End else IsNull(p.NumIdentidad,'') End
End As FederalTaxID,
'##' as Currency,Case When p.Domiciliado=0 then 'SND' Else 'TP'+p.Persona End as U_SYP_BPTP, 
Case When p.IDTipoProv='005' Then 6 Else IsNull(id.CoSAP,IsNull((select CoSAP from MATIPOIDENT tSap where tSap.IDIdentidad=id.IDIdentidadSAP),0)) End As U_SYP_BPTD,
Replace(IsNull(Ltrim(Rtrim(p.ApPaterno)),' '),'-',' ') as U_SYP_BPAP, Replace(IsNull(Ltrim(Rtrim(p.ApMaterno)),'' ),'-',' ') as U_SYP_BPAM,
dbo.FnDevuelveFirstOrLastName(IsNull(Nombre,''),0) As U_SYP_BPN2,dbo.FnDevuelveFirstOrLastName(IsNull(Nombre,''),1) As U_SYP_BPNO,
IsNull(Email1,'') as EmailAddress,
Case When Len(Ltrim(Rtrim(IsNull(p.Telefono1,'')))) > 20 then '' else Ltrim(Rtrim(IsNull(p.Telefono1,''))) End as Phone1,
Case When Len(Ltrim(Rtrim(IsNull(p.Telefono2,'')))) > 20 then '' else Ltrim(Rtrim(IsNull(p.Telefono2,''))) End as Phone2, 
SubjectToWithholdingTax= case when (select count(*) from MAPROVEEDORES_MATIPODETRACCION pd where pd.IDProveedor=p.IDProveedor) >0 then 1 else 0 end,
Case When p.Activo='A' then 1 else 0 End as Valid
from MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo
Left Join MAUBIGEO uPais On u.IDPais=uPais.IDubigeo
left join MATIPOIDENT id on p.IDIdentidad = id.IDIdentidad
left Join MATIPOPROVEEDOR tp On p.IDTipoProv=tp.IDTipoProv
left Join MAFORMASPAGO fp On p.IDFormaPago=fp.IdFor
--where LTRIM(RTRIM(uPais.Descripcion)) <> 'PERU' And Activo='A' 
--And CardCodeSAP is not null And Persona='J' And Domiciliado=1  And FlExcluirSAP=0
Where p.FlUpdMasivoSAPInterfaz = 1 --And CardCodeSAP='P1001324904601'
And p.Activo='A'
