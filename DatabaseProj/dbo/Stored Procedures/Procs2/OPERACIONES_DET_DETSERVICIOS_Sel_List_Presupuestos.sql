﻿ 
--JRF-20140623-Agregando el campo CoEstadoSincerado  
--HLF-20140721-and p.IDTipoProv Not In ('004','005','008')   
--HLF-20140725-and p.IDTipoProv In ('006')         
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Sel_List_Presupuestos    
@IDServicio char(8),    
@IDOperacion_Det int    
as    
Set NoCount On    
select IsNull(odds.NuSincerado,0) as NuSincerado,p.IDProveedor,p.NombreCorto as DescProveedor    
  ,sd.IDServicio,odd.IDServicio_Det,  
  Isnull(Isnull((select SUM(SsCostoNeto) from SINCERADO_DET Where NuSincerado =odds.NuSincerado),odd.NetoCotizado),0)  
   as SsCostoNeto,  --Isnull(odds.SsCostoNeto,0) as SsCostoNeto,    
  --Isnull(odds.SsCostoIgv,0) as SsCostoIgv,ISNULL(odds.SsCostoTotal,0) as SsCostoTotal    
  --Isnull(odds.SsCostoIgv,Isnull(odd.IgvCotizado,0))  
  Isnull(Isnull((select SUM(SsCostoIgv) from SINCERADO_DET Where NuSincerado =odds.NuSincerado),odd.IgvCotizado),0)  
   as SsCostoIgv,--ISNULL(odds.SsCostoTotal,Isnull(odd.TotalCotizado,0))   
     
  Isnull(Isnull((select SUM(SsCostoTotal) from SINCERADO_DET Where NuSincerado =odds.NuSincerado),odd.TotalCotizado),0)  
   as SsCostoTotal    
    --,sd.IDServicio_Det,sd.Anio,sd.Tipo,sd.Descripcion    
    ,Isnull(c.Margen,0) as MargenCoti,Isnull(odd.CoEstadoSincerado,'SP') as CoEstadoSincerado--,Isnull(sd.TC,0) as TipoCambioCoti,sdCot.TC    
from OPERACIONES_DET_DETSERVICIOS odd     
Left Join MASERVICIOS_DET sd On odd.IDServicio_Det_V_Cot =sd.IDServicio_Det    
Left Join MASERVICIOS s On s.IDServicio = sd.IDServicio    
Left Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor    
Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det    
Left Join OPERACIONES o On od.IDOperacion = o.IDOperacion    
Left Join COTICAB c On o.IDCab = c.IDCAB    
Left Join MASERVICIOS_DET sdCot On odd.IDServicio_Det = sdCot.IDServicio_Det    
Left Join SINCERADO odds On odd.IDOperacion_Det= odds.IDOperacion_Det and odd.IDServicio_Det = odds.IDServicio_Det    
where odd.IDOperacion_Det = @IDOperacion_Det and odd.IDServicio = @IDServicio    
and p.IDTipoProv = '006' 
--and p.IDTipoProv Not In ('004','005','008')      
and odd.Activo = 1    

