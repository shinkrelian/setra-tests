﻿
---------------------------------------------------------------------------------
--NOTAVENTA_COUNTER_DETALLE_Sel_xNuNtaVta '003123',0,'','',''
CREATE PROCEDURE [dbo].[NOTAVENTA_COUNTER_DETALLE_Sel_xNuNtaVta]    

@NuNtaVta char(6),
@NuDetalle tinyint=0,
@CoTipoServicio char(3)='',
@TxDetalle VARCHAR(150)='',
@CoProveedor char(6)=''
AS
BEGIN
SELECT     
--NOTAVENTA_COUNTER_DETALLE.NuNtaVta,
NOTAVENTA_COUNTER_DETALLE.NuDetalle,
NOTAVENTA_COUNTER_DETALLE.CoTipoServicio, 
                      MATIPOSERVICIO_COUNTER.NoDescripcion,
                       NOTAVENTA_COUNTER_DETALLE.NuDocumento,
                      NOTAVENTA_COUNTER_DETALLE.TxDetalle,
                      NOTAVENTA_COUNTER_DETALLE.CoProveedor, 
                      MAPROVEEDORES.NombreCorto,
                      NOTAVENTA_COUNTER_DETALLE.QtCantidad,
                      NOTAVENTA_COUNTER_DETALLE.CoMoneda,
                      MAMONEDAS.Descripcion, 
                      NOTAVENTA_COUNTER_DETALLE.SSTipoCambio,
                     
                      NOTAVENTA_COUNTER_DETALLE.SSCosto, 
                      NOTAVENTA_COUNTER_DETALLE.SSComision,
                      NOTAVENTA_COUNTER_DETALLE.SSValor,
                      NOTAVENTA_COUNTER_DETALLE.SSFEE_Emision, 
                      NOTAVENTA_COUNTER_DETALLE.SSVenta,
                      NOTAVENTA_COUNTER_DETALLE.SSUtilidad--,
                      --NOTAVENTA_COUNTER_DETALLE.FlActivo
FROM         NOTAVENTA_COUNTER_DETALLE INNER JOIN
                      MATIPOSERVICIO_COUNTER ON NOTAVENTA_COUNTER_DETALLE.CoTipoServicio = MATIPOSERVICIO_COUNTER.CoTipoServicio INNER JOIN
                      MAPROVEEDORES ON NOTAVENTA_COUNTER_DETALLE.CoProveedor = MAPROVEEDORES.IDProveedor INNER JOIN
                      MAMONEDAS ON NOTAVENTA_COUNTER_DETALLE.CoMoneda = MAMONEDAS.IDMoneda
WHERE    (NOTAVENTA_COUNTER_DETALLE.NuNtaVta = @NuNtaVta) AND

	(@NuDetalle=0 Or NOTAVENTA_COUNTER_DETALLE.NuDetalle =@NuDetalle ) AND
			
			(ltrim(rtrim(@CoTipoServicio))='' Or NOTAVENTA_COUNTER_DETALLE.CoTipoServicio like '%' +@CoTipoServicio + '%') AND
			(ltrim(rtrim(@TxDetalle))='' Or NOTAVENTA_COUNTER_DETALLE.TxDetalle like '%' +@TxDetalle + '%') AND
			(ltrim(rtrim(@CoProveedor))='' Or NOTAVENTA_COUNTER_DETALLE.CoProveedor like '%' +@CoProveedor + '%')
	
END;
