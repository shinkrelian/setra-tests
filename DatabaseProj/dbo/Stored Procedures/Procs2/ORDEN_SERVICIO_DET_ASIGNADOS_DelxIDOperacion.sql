﻿CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDOperacion
@IDOperacion int
As
Set NoCount On
Delete from ORDEN_SERVICIO_DET_ASIGNADOS where 
NuOrden_Servicio_Det In (select osd.NuOrden_Servicio_Det from ORDEN_SERVICIO_DET osd 
							where osd.IDOperacion_Det In (select od.IDOperacion_Det from OPERACIONES_DET od  where od.IDOperacion = @IDOperacion))
