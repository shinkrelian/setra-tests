﻿

CREATE PROCEDURE [dbo].[NOTA_VENTA_DET_DelxIDDet]
	@IDDet	int
As
	Set Nocount On
	
	DELETE FROM NOTA_VENTA_DET WHERE IDReserva_Det 
		In (Select IDReserva_Det From RESERVAS_DET Where IDDet=@IDDet)
