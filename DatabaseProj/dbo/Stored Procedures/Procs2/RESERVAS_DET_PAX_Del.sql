﻿Create Procedure [dbo].[RESERVAS_DET_PAX_Del]
@IDReserva_Det int,
@IDPax int
As
Begin 
	Set NoCount On
	Delete from RESERVAS_DET_PAX Where IDReserva_Det=@IDReserva_Det And IDPax=@IDPax
End
