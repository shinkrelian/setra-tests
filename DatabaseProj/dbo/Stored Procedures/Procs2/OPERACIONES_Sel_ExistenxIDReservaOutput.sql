﻿
Create Procedure dbo.OPERACIONES_Sel_ExistenxIDReservaOutput    
	@IDReserva int,    
	@pbOk bit Output    
As    
	Set Nocount On    

	Set @pbOk = 0    
	If Exists(Select IDOperacion_Det From OPERACIONES_DET_DETSERVICIOS 
		Where IDOperacion_Det In (Select IDOperacion_Det From OPERACIONES_DET 
			Where IDOperacion In (Select IDOperacion From OPERACIONES 
				Where IDReserva=@IDReserva)  )  
		And Not IDEstadoSolicitud In ('NV','PA')
		)	
		
		Set @pbOk = 1  
		
