﻿Create Procedure [dbo].[MASERVICIOS_Anu]
	@IDServicio char(8),
    @UserMod char(4)

As
	Set NoCount On

	UPDATE MASERVICIOS SET cActivo='I',UserMod=@UserMod,FecMod=GETDATE()           
		WHERE IDServicio=@IDServicio
