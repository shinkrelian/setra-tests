﻿
--HLF-20150701-Union RESERVAS_DET od
CREATE Procedure dbo.Reporte_NoShowxFechas
	@FecIni smalldatetime,
	@FecFin smalldatetime
As
	Set Nocount on

	Select c.IDFile,c.Titulo,p.NombreCorto as DescProveedor, od.Servicio, od.IDMoneda, od.NetoGen,  od.IgvGen,  od.TotalGen  
	From
	OPERACIONES_DET od Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
	Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
	Inner Join COTICAB c On o.IDCab=c.IDCAB
	Where c.FecInicio between @FecIni And @FecFin And od.FlServicioNoShow=1

	Union

	Select c.IDFile,c.Titulo,p.NombreCorto as DescProveedor, 
	IsNull(Case When cv.TipoTransporte='V' Then 'Ticket '+ Ltrim(Rtrim(cv.Ruta))+' - '+Vuelo 
	Else cv.Servicio End,cd.Servicio)  As Servicio,
	'USD' as IDMoneda,
	IsNull(cv.CostoOperador,0)-IsNull((select SUM(IsNull(cp3.SsReembolso,0)) from COTITRANSP_PAX cp3 Where cp3.IDTransporte=cv.ID),0) as NetoGen,
	0 as IgvGen,
	IsNull(cv.CostoOperador,0)-IsNull((select SUM(IsNull(cp3.SsReembolso,0)) from COTITRANSP_PAX cp3 Where cp3.IDTransporte=cv.ID),0) as TotalGen
	From
	COTIVUELOS cv 
	Left Join MAPROVEEDORES p On cv.IdLineaA=p.IDProveedor
	Left Join COTIDET cd On cv.IdDet=cd.IDDET
	Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
	Inner Join COTICAB c On cv.IDCAB=c.IDCAB
	where c.FecInicio between @FecIni And @FecFin
	And Exists(select IDPax from COTIPAX cp Where cp.IDCab=cv.IDCAB and cp.FlNoShow=1)

	Union

	Select c.IDFile,c.Titulo,p.NombreCorto as DescProveedor, od.Servicio, od.IDMoneda, od.NetoGen,  od.IgvGen,  od.TotalGen  
	From
	RESERVAS_DET od Inner Join RESERVAS o On od.IDReserva=o.IDReserva
	Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
	Inner Join COTICAB c On o.IDCab=c.IDCAB
	Where c.FecInicio between @FecIni And @FecFin And od.FlServicioNoShow=1

	Union

	Select IDFile,Titulo,DescProveedor, Servicio, 'USD' as IDMoneda,
		dbo.FnCambioMoneda(X.NetoProgramado,IDMoneda,'USD',TC) as NetoGen, 		
		dbo.FnCambioMoneda(x.IgvProgramado,IDMoneda,'USD',TC) as IgvGen, 
		dbo.FnCambioMoneda(x.NetoProgramado+x.IgvProgramado,IDMoneda,'USD',TC) as TotalGen
	   
	From
		(
		Select c.IDFile,c.Titulo,p.NombreCorto as DescProveedor, od.Servicio, 		
			Round(IsNuLL(ods.NetoProgram, dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0))),2) As NetoProgramado ,
			Round(IsNuLL(ods.IgvProgram,
			IsNuLL(ods.NetoProgram, dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0))) * 
			Case When sd.Afecto = 1 Then (select NuIGV from PARAMETRO)/100 else 0 End),2) As IgvProgramado, ods.IDMoneda, --sd.TC
			isnull(sd.TC,(select SsTipCam from COTICAB_TIPOSCAMBIO where IDCab=o.IDCab and CoMoneda=ods.IDMoneda)) as TC
		From OPERACIONES_DET_DETSERVICIOS ods inner Join 
		OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
		Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion
		Inner Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor
		Inner Join MASERVICIOS_DET sd On od.IDServicio_Det=sd.IDServicio_Det
		Inner Join COTICAB c On o.IDCAB=c.IDCAB
		where  c.FecInicio between @FecIni And @FecFin And ods.FlServicioNoShow=1 and ods.Activo=1
		) as X
	Order by 1



