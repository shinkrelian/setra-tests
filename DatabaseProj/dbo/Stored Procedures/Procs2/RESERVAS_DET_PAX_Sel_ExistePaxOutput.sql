﻿		
Create Procedure [dbo].[RESERVAS_DET_PAX_Sel_ExistePaxOutput]	
	@IDPax	int, 
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDReserva_Det From RESERVAS_DET_PAX
		WHERE IDPax=@IDPax)
		Set @pExiste=1
	Else
		Set @pExiste=0

	
		
