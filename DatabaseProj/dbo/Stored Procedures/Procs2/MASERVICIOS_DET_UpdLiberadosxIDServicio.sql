﻿--JRF-20130717-Nuevo Campo
CREATE Procedure dbo.MASERVICIOS_DET_UpdLiberadosxIDServicio  
 @IDServicio char(8),  
 @PoliticaLiberado bit,  
 @Liberado tinyint,  
 @TipoLib char(1),  
 @MaximoLiberado tinyint,  
 @MontoL numeric(8,2),  
 @UserMod char(4)  
As  
 Set NoCount On  
   
 Update MASERVICIOS_DET Set   
 PoliticaLiberado=@PoliticaLiberado,  
 Liberado=Case When @Liberado=0 Then Null Else @Liberado End,  
 TipoLib=Case When @TipoLib='' Then Null Else @TipoLib End,  
 MaximoLiberado=Case When @MaximoLiberado=0 Then Null Else @MaximoLiberado End,  
 MontoL=Case When @MontoL=0 Then Null Else @MontoL End,  
 UserMod=@UserMod,  
 FecMod=GETDATE()  
 Where IDServicio=@IDServicio and PoliticaLibNivelDetalle = 0
