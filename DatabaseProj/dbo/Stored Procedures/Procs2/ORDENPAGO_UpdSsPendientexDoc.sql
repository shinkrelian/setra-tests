﻿--JHD-20150821-No actualizar estado a Aceptado por BD
--JHD-20150904-actualizar saldo al voucher anexado a la orden
--JHD-20150911-set @IDVoucher= isnull( (select TOP 1 od2.IDVoucher
CREATE Procedure dbo.ORDENPAGO_UpdSsPendientexDoc  
@IDOrdPag int,  
@SsTotalOrig numeric(9,2),  
@UserMod char(4)  
As  
Set NoCount On  
Update ORDENPAGO  
 Set SsSaldo= ORDENPAGO.SsSaldo-@SsTotalOrig,  
  UserMod=@UserMod,  
  FecMod=GETDATE()  
where IDOrdPag=@IDOrdPag  
  
Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from ORDENPAGO where IDOrdPag=@IDOrdPag)  
----if @SsSaldoPend <= 0  
--if @SsSaldoPend = 0  
-- Update ORDENPAGO set CoEstado='AC' where IDOrdPag=@IDOrdPag  

DECLARE @IDVoucher int

--set @IDVoucher= isnull( (select distinct od2.IDVoucher
set @IDVoucher= isnull( (select TOP 1 od2.IDVoucher
												from 
												ordenpago op2
												inner join reservas r2 on op2.IDReserva=r2.IDReserva
												inner join OPERACIONES o2 on o2.IDReserva=r2.IDReserva
												inner join OPERACIONES_DET od2 on od2.IDOperacion=o2.IDOperacion
												inner join DOCUMENTO_PROVEEDOR dc on dc.CoOrdPag=op2.IDOrdPag
												where op2.IDOrdPag=@IDOrdPag--2690
				 ),0)
if @IDVoucher<>0
begin
	declare @total_voucher numeric(8,2)
declare @total_docs_op numeric(9,2)
set @total_voucher=(select SUM(SsMontoTotal) from VOUCHER_OPERACIONES where IDVoucher=@idvoucher)
set @total_docs_op= (select sum (ssmontototal) from ordenpago where IDOrdPag in
(select distinct		op2.IDOrdPag
from 
									ordenpago op2
									inner join reservas r2 on op2.IDReserva=r2.IDReserva
									inner join OPERACIONES o2 on o2.IDReserva=r2.IDReserva
									inner join OPERACIONES_DET od2 on od2.IDOperacion=o2.IDOperacion --and od2.IDVoucher=435201
									inner join RESERVAS_DET rd on rd.IDReserva=r2.IDReserva
									and od2.IDReserva_Det=rd.IDReserva_Det
									inner join DOCUMENTO_PROVEEDOR dc on dc.CoOrdPag=op2.IDOrdPag
									where od2.IDVoucher=@IDVoucher and op2.IDOrdPag in (select IDOrdPag from DOCUMENTO_PROVEEDOR where FlActivo=1) )
)

--select @total_voucher,@total_docs_op
if @total_voucher=@total_docs_op
	begin
		update VOUCHER_OPERACIONES set SsSaldo=@total_voucher-@total_docs_op, CoEstado='AC' where IDVoucher=@idvoucher
	end
	else
	begin
		update VOUCHER_OPERACIONES set SsSaldo=@total_voucher-@total_docs_op where IDVoucher=@idvoucher
	end
end

