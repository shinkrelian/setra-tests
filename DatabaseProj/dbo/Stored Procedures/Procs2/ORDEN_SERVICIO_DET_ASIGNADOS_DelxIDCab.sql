﻿CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDCab
@IDCab int
As
Set NoCount On
Delete from ORDEN_SERVICIO_DET_ASIGNADOS where 
NuOrden_Servicio_Det In (select osd.NuOrden_Servicio_Det from ORDEN_SERVICIO_DET osd 
						 where osd.NuOrden_Servicio In (select os.NuOrden_Servicio from ORDEN_SERVICIO os 
														where IDCab = @IDCab))
