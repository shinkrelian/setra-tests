﻿create Procedure [dbo].[MAPROVEEDORES_RANGO_APT_Sel_xIDProveedor]	
    @IDProveedor int
As
	Set NoCount On
	
	Select Correlativo, NuAnio,PaxDesde, PaxHasta, Monto 
	From MAPROVEEDORES_RANGO_APT
	Where IDProveedor=@IDProveedor
	Order by PaxDesde
