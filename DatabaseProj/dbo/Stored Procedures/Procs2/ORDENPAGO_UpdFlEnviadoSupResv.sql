﻿Create Procedure dbo.ORDENPAGO_UpdFlEnviadoSupResv
@IDOrdPag int,
@FlEnviadoSupResv bit,
@UserMod char(4)
As
Set NoCount On
Update ORDENPAGO
	Set FlEnviadoSupResv=@FlEnviadoSupResv
where IDOrdPag = @IDOrdPag
