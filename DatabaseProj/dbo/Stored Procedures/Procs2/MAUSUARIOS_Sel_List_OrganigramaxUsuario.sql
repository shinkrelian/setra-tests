﻿
CREATE Procedure [dbo].[MAUSUARIOS_Sel_List_OrganigramaxUsuario]
@IDUsuario char(4)
As
Begin
Set NoCount On
Set @IDUsuario = Case When @IDUsuario='0000' Then '0073' else @IDUsuario End
Declare @RutaArchivoFoto varchar(200)=(select Replace(NoRutaUserPhoto,'\\wb1','wb1:81') from PARAMETRO)
Declare @UsuariosOrdenados table (usuario1 varchar(100),foto varchar(100),staff bit, sexo char(1) ,cargo1 varchar(100),
								  usuario2 varchar(100),cargo2 varchar(100),usuario3 varchar(100),cargo3 varchar(100),
								  usuario4 varchar(100),cargo4 varchar(100),usuario5 varchar(100),cargo5 varchar(100),usuario6 varchar(100),cargo6 varchar(100),
								  NroOrden tinyint)

Insert into @UsuariosOrdenados
select uNorm.Nombre+' '+uNorm.TxApellidos As name,Replace(@RutaArchivoFoto+uNorm.Usuario+'\'+IsNull(uNorm.NoArchivoFoto,''),'\','/'),uNorm.FlEsStaff, uNorm.CoSexo,cr.TxCargo as cargo,
	   uJefe1.Nombre+' '+uJefe1.TxApellidos as parent1,cr1.TxCargo as cargoSuperior1,
	   uJefe2.Nombre+' '+uJefe2.TxApellidos as parent2,cr2.TxCargo as cargoSuperior2,
	   uJefe3.Nombre+' '+uJefe3.TxApellidos as parent3,cr3.TxCargo as cargoSuperior3,
	   uJefe4.Nombre+' '+uJefe4.TxApellidos as parent4,cr4.TxCargo as cargoSuperior4,
	   uJefe5.Nombre+' '+uJefe5.TxApellidos as parent5,cr5.TxCargo as cargoSuperior5,
	   Case When uJefe1.IDUsuario is null then 1 else Case When uJefe2.IDUsuario is null then 2 else 
		 Case When uJefe3.IDUsuario is null then 3 else Case When uJefe4.IDUsuario is null then 4 else 
		   Case When uJefe5.IDUsuario is null then 5 else 0 End End End End End as NroOrden
from MAUSUARIOS uNorm Left Join MAUSUARIOS uJefe1 On uNorm.CoUserJefe=uJefe1.IDUsuario
Left Join MAUSUARIOS uJefe2 On uJefe1.CoUserJefe=uJefe2.IDUsuario
Left Join MAUSUARIOS uJefe3 On uJefe2.CoUserJefe=uJefe3.IDUsuario
Left Join MAUSUARIOS uJefe4 On uJefe3.CoUserJefe=uJefe4.IDUsuario
Left Join MAUSUARIOS uJefe5 On uJefe4.CoUserJefe=uJefe5.IDUsuario
Left Join RH_CARGO cr On uNorm.CoCargo = cr.CoCargo
Left Join RH_CARGO cr1 On uJefe1.CoCargo = cr1.CoCargo
Left Join RH_CARGO cr2 On uJefe2.CoCargo = cr2.CoCargo
Left Join RH_CARGO cr3 On uJefe3.CoCargo = cr3.CoCargo
Left Join RH_CARGO cr4 On uJefe4.CoCargo = cr4.CoCargo
Left Join RH_CARGO cr5 On uJefe5.CoCargo = cr5.CoCargo
where uNorm.FlTrabajador_Activo=1 And uNorm.FecCese is null And uNorm.Activo='A'
And (uNorm.IDUsuario=@IDUsuario Or uJefe1.IDUsuario=@IDUsuario Or uJefe2.IDUsuario=@IDUsuario 
	 Or uJefe3.IDUsuario=@IDUsuario Or uJefe4.IDUsuario=@IDUsuario Or uJefe5.IDUsuario=@IDUsuario)
Order By cargoSuperior1,cargoSuperior2,cargoSuperior3,cargoSuperior4,cargoSuperior5--,FlEsStaff asc

select * from @UsuariosOrdenados Order By NroOrden,staff,usuario2,cargo1 desc
End

