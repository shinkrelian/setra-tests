﻿--HLF-20150408-ValorVentaDif,ValorProyectadoDif
--HLF-20160229-Agregando columnas MargenGan,MargenNominal, Executive; cambiando nombres de columnas a ingles
--HLF-20160307- And CONVERT(varchar,@FecFin,103)='01/01/1900'
--Set DateFormat dmy	SET DATEFIRST 1
CREATE Procedure dbo.ReporteWeeklyNetSalesHTML
	@FecIni smalldatetime,
	@FecFin smalldatetime,
	@SemanaMes char(1),
	@pvcHTML1 varchar(max) output
	--@pvcHTML2 varchar(max) output
As
	Set Nocount On
	Set DateFormat dmy
	SET DATEFIRST 1

	Declare @IDCab int,@IDFile char(8),@Fecha smalldatetime,@FecInicio smalldatetime,@FechaOut smalldatetime,@RazonComercial varchar(80),
	@DescPais varchar(60),@Titulo varchar(100),	@Pax smallint,	@Estado char(1),@PoConcretVta numeric(5,2),	@ValorVenta numeric(13,2),
	@ValorProyectado numeric(13,2),@PaxOld smallint,@EstadoOld char(1),@PoConcretVtaOld numeric(5,2),@ValorVentaOld numeric(13,2),
	@ValorProyectadoOld numeric(13,2),@ValorVentaDif numeric(13,2),@ValorProyectadoDif numeric(13,2),@MargenGan numeric(5,2),
	@MargenNominal numeric(13,4), @Executive varchar(20)

	--If @SemanaMes='W' And @FecFin='01/01/1900'
	If @SemanaMes='W' And CONVERT(varchar,@FecFin,103)='01/01/1900'
		Begin		
		Declare @FecSemanaPasadaHoy smalldatetime = dateadd(WEEK,-1, @FecIni)
		Declare @NroSemanaHoy tinyint = Datepart(dw, @FecSemanaPasadaHoy)
		Set @FecIni=dateadd(DAY,(@NroSemanaHoy-1)*(-1), @FecSemanaPasadaHoy)
		Set @FecFin=dateadd(DAY,6-(@NroSemanaHoy), @FecSemanaPasadaHoy)
		End

	--If @SemanaMes='M' And @FecFin='01/01/1900'
	If @SemanaMes='M' And CONVERT(varchar,@FecFin,103)='01/01/1900'
		Begin		
		Declare @FecMesPasadaHoy smalldatetime = dateadd(MONTH,-1, @FecIni)		
		Set @FecIni=dateadd(DAY,(day(@FecMesPasadaHoy)-1)*(-1), @FecMesPasadaHoy)
		Set @FecFin=dateadd(day,-1,dateadd(MONTH,1,@FecIni))		
		End

	Declare @vcSubTitulo varchar(100)=''
	If @SemanaMes='W'
		Set @vcSubTitulo='Semana Del '+CONVERT(varchar,@FecIni,103)+' al '+CONVERT(varchar,@FecFin,103)
	If @SemanaMes='M'
		Set @vcSubTitulo='Mes Del '+CONVERT(varchar,@FecIni,103)+' al '+CONVERT(varchar,@FecFin,103)

		
	
	--Declare @pvcHTML1 varchar(max) = ''
	--Declare @pvcHTML2 varchar(max) = ''

	Create Table #FilesSemanaMesHTML(
	--IDCab int,
	IDFile char(8),
	Fecha smalldatetime,
	FecInicio smalldatetime,
	FechaOut smalldatetime,
	RazonComercial varchar(80),
	DescPais varchar(60),
	Titulo varchar(100),
	Pax smallint,
	Executive varchar(20),
	--Estado char(1),
	PoConcretVta numeric(5,2),
	ValorVenta numeric(13,2),
	MargenGan numeric(5,2),
	MargenNominal numeric(13,4),
	ValorProyectado numeric(13,2))

	Insert Into #FilesSemanaMesHTML
	Exec ReporteWeeklyNetSales @FecIni,@FecFin,@SemanaMes,'1'

	Create Table #FilesSemanaMesHTML2(
	--IDCab int,
	IDFile char(8),
	Fecha smalldatetime,
	FecInicio smalldatetime,
	FechaOut smalldatetime,
	RazonComercial varchar(80),
	DescPais varchar(60),
	Titulo varchar(100),
	Pax smallint,
	Estado char(1),
	PoConcretVta numeric(5,2),
	ValorVenta numeric(13,2),
	ValorProyectado numeric(13,2),

	PaxOld smallint,
	EstadoOld char(1),
	PoConcretVtaOld numeric(5,2),
	ValorVentaOld numeric(13,2),
	ValorProyectadoOld numeric(13,2),
	ValorVentaDif numeric(13,2),
	ValorProyectadoDif numeric(13,2),
	Executive varchar(20),
	MargenGan numeric(5,2),
	MargenNominal numeric(13,4),
	)

	Insert Into #FilesSemanaMesHTML2
	Exec ReporteWeeklyNetSales @FecIni,@FecFin,@SemanaMes,'2'

	Declare @vcHTML varchar(max)=''

	Declare @vcHTMLBase varchar(max)='<HTML>'
	+'<head runat="server">'
	+ '<style type="text/css">'
	
	+ ' .st1'
	+ ' {font-family: "Century Gothic";'
	+ ' line-height: normal; border-style: groove; font-size: 13px; }'

	+ ' .st5'
	+ ' { width: 50px;'
	+ ' font-size: 13px;font-weight: bold; background-color: black; color: white;}'

	+ ' .st10'
	+ ' { font-size: 13px; text-align: center; font-family: "Century Gothic"; font-weight: bold; width: 30px; background-color: black; color: white;}'	

	+ '.stTitulo { text-align: center; font-family: "Century Gothic"; '
	+' font-weight: bold; }'

	+'.stTablaTotales {font-family: "Century Gothic"; font-size: 13px; padding-left: 25%;border-style: hidden;} '
	+'.stTotales { width: 10px; font-size: 13px;font-weight: bold; background-color: black; color: white;} '
	+'.stTotalesBlanco { width: 10px; font-size: 13px;font-weight: bold; background-color: white; color: white;} '
	+ '</style>'
	+ '</head>'
	+ '</html>'
	+ '<body>'
	+ '<form id="form1" runat="server">'
	+'<div class="stTitulo">'
	


	Set @vcHTML = @vcHTMLBase  
	--+'Files Nuevos</div><br />'
	--+'<div class="stTitulo">'+@vcSubTitulo+'</div><br />'

	+@vcSubTitulo+'</div><br />'
	+'<div class="stTitulo">Files Nuevos</div><br />'

	+ '<table class="st1">'
	+ '<tr>'
	+ '<td class="st5">File</td>'
	--+ '<td class="st5">Fecha In</td>'                
	+ '<td class="st5">Out</td>'                
	 + '<td class="st5">Client</td>'
	 --+ '<td class="st5">Pais</td>'
	 + '<td class="st5">Pax/Group</td>'
	 + '<td class="st10" align="center">#Pax</td>'
	 --+ '<td class="st10" align="center">St</td>'
	 + '<td class="st10" align="center">Executive</td>'
	 + '<td class="st10" align="right">Likelihood (P)</td>'
	+ '<td class="st10" align="right">Revenue</td>'
	 + '<td class="st10" align="right">Revenue * P</td>'      	  
	 + '<td class="st10" align="right">Margin Nominal</td>'                    
	+ '<td class="st10" align="right">Margin %</td>'
	+ '</tr>'


	Declare @ValorVentaTot numeric(14,2)=0, @ValorProyectadoTot numeric(14,2)=0

	Declare curFilesSemanaMesHTML cursor for Select * From #FilesSemanaMesHTML
	Open curFilesSemanaMesHTML
	Fetch Next From curFilesSemanaMesHTML Into  @IDFile,@Fecha,@FecInicio,@FechaOut,@RazonComercial,@DescPais,@Titulo,@Pax,
		@Executive,@PoConcretVta,@ValorVenta,@MargenGan,@MargenNominal,@ValorProyectado
	While @@FETCH_STATUS=0
		Begin
		
		Set @vcHTML = @vcHTML + '<tr>'
		 + '<td class="st1">'+isnull(@IDFile,'')+'</td>'
		 --+ '<td class="st1">'+convert(varchar,@FecInicio,103)+'</td>'                
		 + '<td class="st1">'+convert(varchar,@FechaOut,103)+'</td>'                
		 + '<td class="st1">'+ltrim(rtrim(@RazonComercial))+'</td>'
		 --+ '<td class="st1">'+ltrim(rtrim(@DescPais))+'</td>'
		 + '<td class="st1">'+ltrim(rtrim(@Titulo))+'</td>'
		 + '<td class="st1" align="center">'+Cast(@Pax as varchar(3))+'</td>'
		 --+ '<td class="st1" align="center">'+@Estado+'</td>'
		 + '<td class="st1" align="center">'+@Executive+'</td>'
		 + '<td class="st1" align="right">'+Cast(@PoConcretVta as varchar(7))+'</td>'
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorVenta,0)),1)+'</td>'
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorProyectado,0)),1)+'</td>'   
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@MargenNominal,0)),1)+'</td>'   	
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@MargenGan,0)),1)+'</td>'
		 	                  
		 + '</tr>' 

		Set @ValorVentaTot+=@ValorVenta
		Set @ValorProyectadoTot+=@ValorProyectado

		Fetch Next From curFilesSemanaMesHTML Into  @IDFile,@Fecha,@FecInicio,@FechaOut,@RazonComercial,@DescPais,@Titulo,@Pax,
		@Executive,@PoConcretVta,@ValorVenta,@MargenGan,@MargenNominal,@ValorProyectado
		End
	Close curFilesSemanaMesHTML
	Deallocate curFilesSemanaMesHTML
	
	Set @vcHTML += '</table>'	
	+'<br />'
	+'<table class="stTablaTotales">'
	+'<tr>'
	+'<td class="stTotalesBlanco"></td>'
	+'<td class="stTotales">Total Valor Venta</td>'
    +'<td class="stTotales">Total Valor Proyectado</td>'
	+'</tr>'
	+'<tr>'
	+'<td class="stTotales">Total Sales:</td>'
	+'<td class="st1">'+CONVERT(varchar, convert(money, ROUND(@ValorVentaTot,0)),1)+'</td>'
    +'<td class="st1">'+CONVERT(varchar, convert(money, ROUND(@ValorProyectadoTot,0)),1)+'</td>'
	+'</tr>'
	+ '</table>'	
	+'</form></body>'
	
	--Set @pvcHTML1 = @vcHTML


	Declare @ValorVentaTot2 numeric(14,2)=0, @ValorProyectadoTot2 numeric(14,2)=0
	Declare @ValorVentaTotOld numeric(14,2)=0, @ValorProyectadoTotOld numeric(14,2)=0

	--Set @vcHTML = @vcHTMLBase  
	Set @vcHTML += '<br />'
	+'<div class="stTitulo">Files Modificados/Anulados</div><br />'
	--+'<div class="stTitulo">'+@vcSubTitulo+'</div><br />'
	+ '<table class="st1">'
	+ '<tr>'
	+ '<td class="st5">File</td>'
	--+ '<td class="st5">Fecha In</td>'                
	+ '<td class="st5">Out</td>'                
	 + '<td class="st5">Client</td>'
	-- + '<td class="st5">Pais</td>'
	 + '<td class="st5">Pax/Group</td>'
	  + '<td class="st10" align="center">#Pax</td>'
	  + '<td class="st5">Executive</td>'
	 --+ '<td class="st10" align="center">New Pax</td>'
	 --+ '<td class="st10" align="center">New St</td>'
	 + '<td class="st10" align="right">New Likelihood (P)</td>'
	+ '<td class="st10" align="right">New Revenue</td>'
	 + '<td class="st10" align="right">New Revenue * P</td>'                    
	 --+ '<td class="st10" align="center">Old Pax</td>'
	 --+ '<td class="st10" align="center">Old St</td>'
	 + '<td class="st10" align="right">Old Likelihood (P)</td>'
	+ '<td class="st10" align="right">Old Revenue</td>'
	 + '<td class="st10" align="right">Old Revenue * P</td>'                    
	+ '<td class="st10" align="right">Dif. Revenue</td>'
	 + '<td class="st10" align="right">Dif. Revenue * P</td>'                    
	+ '</tr>'

	Declare curFilesSemanaMesHTML2 cursor for Select * From #FilesSemanaMesHTML2
	Open curFilesSemanaMesHTML2
	Fetch Next From curFilesSemanaMesHTML2 Into  @IDFile,@Fecha,@FecInicio,@FechaOut,@RazonComercial,@DescPais,@Titulo,@Pax,
		@Estado,@PoConcretVta,@ValorVenta,@ValorProyectado,@PaxOld,@EstadoOld,@PoConcretVtaOld,@ValorVentaOld,@ValorProyectadoOld,
		@ValorVentaDif,@ValorProyectadoDif,@Executive, @MargenGan, @MargenNominal 
	While @@FETCH_STATUS=0
		Begin
		
		Set @vcHTML += '<tr>'
		 + '<td class="st1">'+isnull(@IDFile,'')+'</td>'
		 --+ '<td class="st1">'+convert(varchar,@FecInicio,103)+'</td>'                
		 + '<td class="st1">'+convert(varchar,@FechaOut,103)+'</td>'                
		 + '<td class="st1">'+ltrim(rtrim(@RazonComercial))+'</td>'
		 --+ '<td class="st1">'+ltrim(rtrim(@DescPais))+'</td>'
		+ '<td class="st1">'+ltrim(rtrim(@Titulo))+'</td>'
		 + '<td class="st1" align="center">'+Cast(@Pax as varchar(3))+'</td>'
		 --+ '<td class="st1" align="center">'+@Estado+'</td>'
		 + '<td class="st1" align="center">'+@Executive+'</td>'
		 + '<td class="st1" align="right">'+Cast(@PoConcretVta as varchar(7))+'</td>'
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorVenta,0)),1)+'</td>'
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorProyectado,0)),1)+'</td>'                    
		 --+ '<td class="st1" align="center">'+Cast(@PaxOld as varchar(3))+'</td>'
		 --+ '<td class="st1" align="center">'+@EstadoOld+'</td>'
		 + '<td class="st1" align="right">'+Cast(@PoConcretVtaOld as varchar(7))+'</td>'
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorVentaOld,0)),1)+'</td>'
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorProyectadoOld,0)),1)+'</td>'                    
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorVentaDif,0)),1)+'</td>'
		 + '<td class="st1" align="right">'+CONVERT(varchar, convert(money, ROUND(@ValorProyectadoDif,0)),1)+'</td>'                    

		 + '</tr>' 

		Set @ValorVentaTot2+=@ValorVenta
		Set @ValorProyectadoTot2+=@ValorProyectado
		Set @ValorVentaTotOld+=@ValorVentaOld
		Set @ValorProyectadoTotOld+=@ValorProyectadoOld

		Fetch Next From curFilesSemanaMesHTML2 Into  @IDFile,@Fecha,@FecInicio,@FechaOut,@RazonComercial,@DescPais,@Titulo,@Pax,
		@Estado,@PoConcretVta,@ValorVenta,@ValorProyectado,@PaxOld,@EstadoOld,@PoConcretVtaOld,@ValorVentaOld,@ValorProyectadoOld,
		@ValorVentaDif,@ValorProyectadoDif,@Executive, @MargenGan, @MargenNominal 
		End
	Close curFilesSemanaMesHTML2
	Deallocate curFilesSemanaMesHTML2

	--Set @vcHTML = @vcHTML + cast('</table></form></body>' as varchar(max))
	
	Set @vcHTML += '</table>'	
	+'<br />'
	+'<table class="stTablaTotales">'
	+'<tr>'
	+'<td class="stTotalesBlanco"></td>'
	+'<td class="stTotales">Total Valor Venta</td>'
    +'<td class="stTotales">Total Valor Proyectado</td>'
	+'</tr>'
	+'<tr>'
	+'<td class="stTotales">Net Value:</td>'
	+'<td class="st1">'+CONVERT(varchar, convert(money,ROUND(@ValorVentaTot2-@ValorVentaTotOld,0)),1)+'</td>'
    +'<td class="st1">'+CONVERT(varchar, convert(money,ROUND(@ValorProyectadoTot2-@ValorProyectadoTotOld,0)),1)+'</td>'
	+'</tr>'
	
	+'<tr>'
	+'<td class="stTotales">Total Net Sales:</td>'
	+'<td class="st1">'+CONVERT(varchar, convert(money,ROUND(ROUND(@ValorVentaTot2-@ValorVentaTotOld,0)+ROUND(@ValorVentaTot,0),0)),1)+'</td>'
    +'<td class="st1">'+CONVERT(varchar, convert(money,ROUND(ROUND(@ValorProyectadoTot2-@ValorProyectadoTotOld,0)+ROUND(@ValorProyectadoTot,0),0)),1)+'</td>'
	+'</tr>'
	+ '</table>'	
	+'</form></body>'

	--Set @pvcHTML2 = @vcHTML
	Set @pvcHTML1 = @vcHTML

	--Select @pvcHTML1, @pvcHTML2

	Drop Table #FilesSemanaMesHTML
	Drop Table #FilesSemanaMesHTML2



