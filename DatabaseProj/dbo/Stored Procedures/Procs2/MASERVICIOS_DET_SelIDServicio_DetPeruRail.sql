﻿
Create Procedure dbo.MASERVICIOS_DET_SelIDServicio_DetPeruRail
	@IDservicio char(8),    
	@Anio char(4),    
	@Variante varchar(7)    
	--@pIDServicio_Det int OutPut    
As    
	Set NoCount On    

	--Select @pIDServicio_Det=IsNull(d2.IDServicio_Det,0)  
	--From MASERVICIOS_DET d             
	--Left Join MASERVICIOS_DET d2 On ISNULL(d.IDServicio_Det_V,0)=d2.IDServicio_Det            
	--Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio            
	----Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor            
	--Where d.IDServicio=@IDservicio And d.Anio=@Anio and d.Tipo = @Variante    
	--and s.IDProveedor In ('001836')    

	--Set @pIDServicio_Det=ISnull(@pIDServicio_Det,0)  

	SELECT sdv.IDServicio_Det, s.IDProveedor,p.NombreCorto as DescProveedor,sdv.Descripcion,
	ISNULL(ub.IDUbigeoOri,'000001') as IDUbigeoOri,
	ISNULL(ubo.DC,'') as AbrevUbigeoOri,
	ISNULL(ub.IDUbigeoDes,'000001') as IDUbigeoDes,
	ISNULL(ubd.DC,'') as AbrevUbigeoDes
	FROM MASERVICIOS_DET sd
	Inner Join MASERVICIOS_DET sdV On sd.IDServicio_Det_V=sdV.IDServicio_Det
	Inner Join MASERVICIOS s On sdV.IDServicio=s.IDServicio And s.IDProveedor='001836'
	Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
	Left Join (Select * From MASERVICIOS_ALIMENTACION_DIA 
		Where Dia=1) as ub On s.IDServicio=ub.IDServicio
	Left Join MAUBIGEO ubO On ub.IDUbigeoOri=ubO.IDubigeo
	Left Join MAUBIGEO ubD On ub.IDUbigeoDes=ubD.IDubigeo
	WHERE sd.IDServicio=@IDservicio and sd.Anio=@Anio and sd.Tipo=@Variante
	
