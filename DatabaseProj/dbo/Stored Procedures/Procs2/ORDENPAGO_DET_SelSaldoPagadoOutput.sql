﻿Create Procedure dbo.ORDENPAGO_DET_SelSaldoPagadoOutput
 @IDReserva_Det int,                 
 @IDMoneda char(3),            
 @TCambio numeric(8,2),            
 @pSaldo numeric(8,2) output                
As                
 Set Nocount On         
 Set @pSaldo=(select Sum(dbo.FnCambioMoneda(od.Total,op.IDMoneda,@IDMoneda,IsNull(op.TCambio,@TCambio)))
 from ORDENPAGO_DET od Inner Join ORDENPAGO op On od.IDOrdPag=op.IDOrdPag
 where IDReserva_Det=@IDReserva_Det)
 Set @pSaldo=ISNULL(@pSaldo,0)
