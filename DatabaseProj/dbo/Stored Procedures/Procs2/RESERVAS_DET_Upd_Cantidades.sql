﻿--HLF-20121128-Cambiando los where Servicio Like y agregando And CapacidadHab
CREATE Procedure dbo.RESERVAS_DET_Upd_Cantidades
	@IDCab	int,
	
	@Simple	tinyint,
	@Twin	tinyint,
	@Matrim	tinyint,
	@Triple	tinyint,
	
 	@UserMod	char(4)
 	
As
	Set NoCount On

	Update RESERVAS_DET Set Cantidad=@Twin, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva in (Select IDReserva From RESERVAS Where IDCAB=@IDCab)
	--And NroPax=2
	And Servicio Like '%(TWIN)%'
	And CapacidadHab=2
	
	Update RESERVAS_DET Set Cantidad=@Matrim, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva in (Select IDReserva From RESERVAS Where IDCAB=@IDCab)
	--And NroPax=2
	And Servicio Like '%(MAT)%'
	And CapacidadHab=2
	
	Update RESERVAS_DET Set Cantidad=@Triple, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva in (Select IDReserva From RESERVAS Where IDCAB=@IDCab)
	--And NroPax=3
	--And Servicio Like '%TRIPLE%'
	And CapacidadHab=3
	
	Update RESERVAS_DET Set Cantidad=@Simple, UserMod=@UserMod, FecMod=GETDATE()
	Where IDReserva in (Select IDReserva From RESERVAS Where IDCAB=@IDCab)
	--And NroPax=1
	--And Servicio Like '%SIMPLE%'
	And CapacidadHab=1
