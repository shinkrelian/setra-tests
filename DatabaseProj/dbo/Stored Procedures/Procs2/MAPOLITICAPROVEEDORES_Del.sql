﻿CREATE Procedure [dbo].[MAPOLITICAPROVEEDORES_Del]
@IDPolitica int,
@IDProveedor char(6)
as

	Set NoCount On
	DELETE FROM [MAPOLITICAPROVEEDORES]
		WHERE IDPolitica = @IDPolitica and IDProveedor = @IDProveedor
