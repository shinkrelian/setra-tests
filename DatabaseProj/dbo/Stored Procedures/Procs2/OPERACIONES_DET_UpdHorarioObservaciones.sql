﻿Create Procedure dbo.OPERACIONES_DET_UpdHorarioObservaciones
@IDOperacion_Det int,
@Dia smalldatetime,
@ObservacionVoucher varchar(max),
@ObservacionBiblia varchar(max),
@ObservacionInterno varchar(max),
@UserMod char(4)
As
	Set NoCount On
	Update OPERACIONES_DET
		set Dia = @Dia,
			ObservVoucher = case when LTRIM(rtrim(@ObservacionVoucher))='' then null else @ObservacionVoucher End,
			ObservBiblia = case when ltrim(rtrim(@ObservacionBiblia))='' then null else @ObservacionBiblia End,
			ObservInterno = case when LTRIM(rtrim(@ObservacionInterno))='' then null else @ObservacionInterno End,
			UserMod = @UserMod,
			FecMod = GETDATE()
	Where IDOperacion_Det = @IDOperacion_Det
