﻿--HLF-20130718-Cambiando subquery de IDOperacion
--JRF-20150221-Nuevo campo [FlServicioNoShow]
CREATE Procedure dbo.OPERACIONES_DET_InsxIDReserva_Det        
 @IDReserva_Det int,         
 @UserMod char(4)        
As        
 Set Nocount On        
      
 Declare @IDOperacion_Det int        
 Execute dbo.Correlativo_SelOutput 'OPERACIONES_DET',1,10,@IDOperacion_Det output        
      
 Insert Into OPERACIONES_DET        
 (IDOperacion_Det, IDOperacion, IDReserva_Det, IDVoucher,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,        
 IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,        
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,        
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,        
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,IDTipoOC,UserMod,      
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,FlServicioNoShow,IDMoneda)         
 Select         
 ((row_number() over (order by IDReserva,IDReserva_Det))-1) + @IDOperacion_Det,         
 --(Select IDOperacion From OPERACIONES_DET Where IDReserva_Det = @IDReserva_Det  ),        
 (Select IDOperacion From OPERACIONES Where IDReserva =(Select IDReserva From RESERVAS_DET Where IDReserva_Det=@IDReserva_Det ) ),        
 IDReserva_Det, 0,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,        
 IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,        
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,        
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,        
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,'NAP',@UserMod,      
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,FlServicioNoShow,IDMoneda     
 From RESERVAS_DET d Where IDReserva_Det In (@IDReserva_Det)        
      
 Declare @iRowsAff smallint=@@ROWCOUNT - 1        
 If @iRowsAff>0 Exec Correlativo_Upd 'OPERACIONES_DET',@iRowsAff        
