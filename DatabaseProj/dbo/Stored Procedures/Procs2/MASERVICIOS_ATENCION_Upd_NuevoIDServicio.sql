﻿CREATE Procedure [dbo].[MASERVICIOS_ATENCION_Upd_NuevoIDServicio]
	@IDServicio char(8),		
	@IDServicioNue char(8),
	@UserMod char(4)
As
	Set NoCount On
	
	Update MASERVICIOS_ATENCION
		SET IDServicio=@IDServicioNue, FecMod=GETDATE(), UserMod=@UserMod
	Where IDServicio=@IDServicio 
