﻿
CREATE Procedure PRESUPUESTOS_Upd    
@IDPresupuesto int ,    
@IDEstado char(2),    
@UserMod char(4)    
As    
 Set NoCount On    
 UPDATE [dbo].[PRESUPUESTOS]    
    SET [IDEstado] = @IDEstado    
    ,[UserMod] = @UserMod    
    ,[FecMod] = Getdate()    
  WHERE [IDPresupuesto] = @IDPresupuesto    
