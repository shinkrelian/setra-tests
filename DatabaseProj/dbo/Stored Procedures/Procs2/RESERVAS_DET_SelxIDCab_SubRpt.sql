﻿CREATE PROCEDURE dbo.RESERVAS_DET_SelxIDCab_SubRpt
	@IDCab	int
As
	Set NoCount On
	
	Select rd.Dia, uOri.Descripcion as DescOrigen, uDes.Descripcion as DescDestino,
	
	Case When p.IDTipoProv='005' Then
		Cast(rd.NroPax + ISNULL(rd.NroLiberados,0) As varchar(10)) + ' PAX + 1 GUIA'
	Else
		Cast(rd.NroPax + ISNULL(rd.NroLiberados,0) As varchar(10)) + ' PAX'
	End as NroPax, 
	
	rd.Servicio,
	r.Estado,
	r.Estado+' - '+
	Case r.Estado 
		When 'NV' Then 'NUEVO'
		When 'RQ' Then 'REQUERIDO'
		When 'OK' Then 'ACEPTADO'
		When 'WL' Then 'WAITING LIST'
		When 'XL' Then 'CANCELADO'
		When 'RR' Then 'RECONFIRMADO'
    End As DescEstado	
	From RESERVAS r Left Join RESERVAS_DET rd On r.IDReserva=rd.IDReserva
	Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor
	Left Join COTIDET cd On rd.IDDet=cd.IDDET
	Left Join MAUBIGEO uOri On cd.IDUbigeoOri=uOri.IDubigeo
	Left Join MAUBIGEO uDes On cd.IDUbigeoDes=uDes.IDubigeo
	--Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	Where r.IDCab=@IDCab And p.IDTipoProv<>'001' and cd.TipoTransporte='T'
	Order by rd.Dia


