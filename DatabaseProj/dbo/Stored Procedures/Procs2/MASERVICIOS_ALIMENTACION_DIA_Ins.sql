﻿CREATE Procedure [dbo].[MASERVICIOS_ALIMENTACION_DIA_Ins]
	@IDServicio char(8),
	@Dia tinyint,
	@Desayuno bit,
	@Lonche bit,
	@Almuerzo bit,
	@Cena bit,
	@IDUbigeoOri	char(6),
	@IDUbigeoDes	char(6),
	@UserMod char(4)
As

	Set NoCount On
	
	INSERT INTO MASERVICIOS_ALIMENTACION_DIA
           ([IDServicio]
           ,[Dia]
           ,[Desayuno]
           ,[Lonche]
           ,[Almuerzo]
           ,[Cena]
           ,IDUbigeoOri
           ,IDUbigeoDes
           ,[UserMod])
     VALUES
           (@IDServicio,
           @Dia ,
           @Desayuno ,
           @Lonche ,
           @Almuerzo ,
           @Cena ,
           @IDUbigeoOri,
           @IDUbigeoDes,           
           @UserMod )
