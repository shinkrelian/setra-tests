﻿
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--MAUSUARIOS_Sel_xIDUbigeo '','','','000323'

--JRF-20140724- And Activo = 'A'
--JHD-20150505-@CoPais char(6)=''
--JHD-20150505- and ((rtrim(ltrim(@CoPais))='') or (select IDPais from maubigeo u where u.IDubigeo=MAUSUARIOS.IDUbigeo)=@CoPais )
CREATE Procedure dbo.MAUSUARIOS_Sel_xIDUbigeo    
 @IDUbigeo char(6),    
 @IDArea char(2),   
 @Nivel   char(3) ,
@CoPais char(6)=''  
As    
 Set Nocount On    
     
 Select IDUsuario, Nombre     
 From MAUSUARIOS Where   
 (IDUbigeo=@IDUbigeo or rtrim(ltrim(@IDUbigeo))='')   
 And (IdArea=@IDArea  Or ltrim(rtrim(@IDArea)) = '')  
 And (Nivel=@Nivel  Or ltrim(rtrim(@Nivel)) = '')  
 And Activo = 'A'
 and ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=MAUSUARIOS.IDUbigeo)=@CoPais )

