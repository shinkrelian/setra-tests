﻿--HLF-20141016-pr.NombreCorto as DescTC_G    
--Left Join MAPROVEEDORES pr On ps.CoPrvGui=pr.IDProveedor  
--JRF-20150109-Agrupar por el Tipo de Sobre [Left Join MATIPOPRESUPUESTO_SOBRE tps On psd.CoTipPSo=tps.CoTipPSo]  
--JRF-20150410-Considerar filtro x Pax (Nuevo Campo)
--JRF-20150604-Considerar el STK...
--JRF-20150604-Dividir el monto en soles y Dólares
--JRF-20150605-Por el momento No imprimir Ticket's
--HLF-20150701-psd.QtPax,
--HLF-20151221-us.Nombre+' '+ISNULL(us.TxApellidos,'')))...
--HLF-20151228-Case When psd.CoPago in('STK','PCM') Then 0 else 
--HLF-20160429-Comentando --and(CHARINDEX('taxi',Lower(TxServicio))=0) 
CREATE Procedure dbo.PRESUPUESTO_SOBRE_DET_Sel_RptEntradasMuseos      
@NuPreSob int,      
@IDProveedor char(6),
@IDPax int
as      
Set NoCount On      
select c.IDFile,c.Titulo,c.NroPax,Isnull(c.NroLiberados,0) as NroLiberados,
	--c.FecInicio,
	(select Min(cd.Dia) from COTIDET cd where cd.IDCAB = ps.IDCab And cd.IDProveedor=@IDProveedor) as FecInicio,
	cL.RazonComercial as Cliente,      
    psd.FeDetPSo,psd.TxServicio,
	--QtPax,
	psd.QtPax,
	SsPreUni,
    --Case When psd.CoPago='STK' Then 0 else 
	Case When psd.CoPago in('STK','PCM') Then 0 else 
		Case When psd.IDMoneda = 'USD' Then psd.SsTotal Else 0 End
	End as SsTotal,
	--Case When psd.CoPago='STK' Then 0 else 
	Case When psd.CoPago in('STK','PCM') Then 0 else 
		Case When psd.IDMoneda = 'SOL' Then psd.SsTotal Else 0 End
	End as SsTotalSoles,SsTotSus,SsTotDev, 
	IsNull(IsNull(pr.NombreCorto,isnull(cp.Titulo+' '+cp.Apellidos+' '+cp.Nombres,us.Nombre+' '+ISNULL(us.TxApellidos,''))),'') as DescTC_G,  
    Case psd.CoPago
	When 'PCM' Then 'PRECOMPRA' 
	When 'STK' Then 'STOCK'
	When 'EFE' then 'EFECTIVO'
	When 'TCK' Then 'TICKET' End As NoTipPSo,ps.CoEstado,mo.Simbolo
from PRESUPUESTO_SOBRE_DET psd Left Join PRESUPUESTO_SOBRE ps On psd.NuPreSob=ps.NuPreSob      
Left Join COTICAB c On ps.IDCab = c.IDCAB Left Join MACLIENTES cL on c.IDCliente=cL.IDCliente      
--Left Join MAPROVEEDORES pr On ps.CoPrvGui=pr.IDProveedor    
Left Join MAPROVEEDORES pr On psd.CoPrvPrg=pr.IDProveedor    
Left Join MAUSUARIOS us On psd.CoPrvPrg=us.IDUsuario
Left Join MATIPOPRESUPUESTO_SOBRE tps On psd.CoTipPSo=tps.CoTipPSo  
Left Join MAMONEDAS mo On IsNull(psd.IDMoneda,'')=mo.IDMoneda
Left Join OPERACIONES_DET_DETSERVICIOS odd On psd.IDOperacion_Det=odd.IDOperacion_Det 
and psd.IDServicio_Det=IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)
Left Join COTIPAX cp On psd.IDPax=cp.IDPax
where psd.NuPreSob = @NuPreSob and ps.IDProveedor = @IDProveedor and IsNull(psd.IDPax,0)=@IDPax
--and(CHARINDEX('taxi',Lower(TxServicio))=0) 
and (odd.Activo=1 Or odd.Activo is null) And psd.CoPago <> 'TCK'
Order By 16,psd.FeDetPSo     


