﻿
--create
CREATE PROCEDURE [ORDENCOMPRA_DET_Del_Todos]
	@NuOrdComInt int
AS
BEGIN
	Delete [dbo].[ORDENCOMPRA_DET]
	Where 
		[NuOrdComInt] = @NuOrdComInt
END;
