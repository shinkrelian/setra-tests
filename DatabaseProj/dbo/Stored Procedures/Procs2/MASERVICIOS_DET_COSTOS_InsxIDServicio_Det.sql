﻿Create Procedure dbo.MASERVICIOS_DET_COSTOS_InsxIDServicio_Det
@IDServicio_Det int,
@IDServicio_DetNuevo int
As
Set NoCount On
Insert into MASERVICIOS_DET_COSTOS
SELECT [Correlativo]
      ,@IDServicio_DetNuevo as [IDServicio_Det]
      ,[PaxDesde]
      ,[PaxHasta]
      ,[Monto]
      ,[Mig]
      ,[UserMod]
      ,[FecMod]
  FROM [dbo].[MASERVICIOS_DET_COSTOS]
Where IDServicio_Det =@IDServicio_Det 
