﻿ --JRF-20131209-Aumentando el valor de los decimales a (numeric(10, 4))  
--JRF-20140730-Agregar el Nuevo campo de CoPago
--JHD-20141127-Agregar el Nuevo campo de IDDesayuno  
--JHD-20141202-Se corrigio para actualizar IDDesayuno nulo
--PPMG-20151106-Se agrego columna APTServicoOpcional
--PPMG-20151106-Columna HoraPredeterminada
CREATE Procedure [dbo].[MASERVICIOS_Upd]        
 @IDServicio char(8),        
 @IDProveedor char(6),        
 @IDTipoProv char(3),        
 @IDTipoServ char(3),           
 @Activo bit,           
 @MotivoDesactivacion varchar(200),        
 @Descripcion varchar(100),        
 @Tarifario bit,        
 @Descri_tarifario varchar(100),        
 @FecCaducLectura smalldatetime,        
 @IDubigeo char(6),        
 @Telefono varchar(50),        
 @Celular varchar(50),        
 @Comision numeric(10,4),        
 @UpdComision bit,      
 @IDCat char(3),        
 @Categoria tinyint,        
 @Capacidad smallint,        
 @Observaciones text,        
 @Lectura text,        
 @Transfer bit,        
 @TipoTransporte char(1),        
         
 @AtencionLunes bit,        
 @AtencionMartes bit,        
 @AtencionMiercoles bit,        
 @AtencionJueves bit,        
 @AtencionViernes bit,        
 @AtencionSabado bit,        
 @AtencionDomingo bit,        
         
 @HoraDesde smalldatetime,        
 @HoraHasta smalldatetime,            
         
 @DesayunoHoraDesde smalldatetime,        
 @DesayunoHoraHasta smalldatetime,        
 @IDDesayuno char(2)='',        
      
 @PreDesayunoHoraDesde smalldatetime,        
 @PreDesayunoHoraHasta smalldatetime,        
 @IDPreDesayuno char(2),        
 @ObservacionesDesayuno varchar(250),      
      
      
 @HoraCheckIn smalldatetime,        
 @HoraCheckOut smalldatetime,        
 --@TelefonoRecepcion1 varchar(25),        
 --@TelefonoRecepcion2 varchar(25),        
 --@FaxRecepcion1 varchar(25),        
 --@FaxRecepcion2 varchar(25),        
 --@IncluyeServiciosDias bit,        
 @Dias tinyint=1,        
      
 @PoliticaLiberado bit,        
 @Liberado tinyint,        
 @TipoLib char(1),        
 @MaximoLiberado tinyint,         
 @MontoL numeric(10,4),         
 @SinDescripcion bit = 0,
 @CoTipoPago char(3),
 @UserMod char(4),
 @APTServicoOpcional char(1)='N',
 @HoraPredeterminada	smalldatetime = '19000101 00:00:00'
AS
BEGIN
 Set NoCount On        
        
 UPDATE MASERVICIOS SET                   
 IDProveedor=@IDProveedor        
 ,IDTipoProv=@IDTipoProv        
 ,IDTipoServ=@IDTipoServ        
 ,Activo=@Activo        
        
 ,MotivoDesactivacion=Case When ltrim(rtrim(@MotivoDesactivacion))='' then Null Else @MotivoDesactivacion End        
 ,Descripcion=@Descripcion        
 ,Tarifario=@Tarifario                   
 ,Descri_tarifario=Case When ltrim(rtrim(@Descri_tarifario))='' then Null Else @Descri_tarifario End        
 ,FecCaducLectura = Case When @FecCaducLectura='01/01/1900' Then Null Else @FecCaducLectura End        
 ,IDubigeo=Case When ltrim(rtrim(@IDubigeo))='' Then IDubigeo Else @IDubigeo End      
 ,Telefono=Case When ltrim(rtrim(@Telefono))='' then Null Else @Telefono End        
 ,Celular=Case When ltrim(rtrim(@Celular))='' then Null Else @Celular End        
 ,Comision=Case When @Comision=0 then Null Else @Comision End        
 ,UpdComision=@UpdComision      
 ,IDCat=@IDCat        
 ,Categoria=Case When @Categoria=0 then Null Else @Categoria End        
 ,Capacidad=Case When @Capacidad=0 then Null Else @Capacidad End        
 ,Observaciones=Case When ltrim(rtrim(Cast(@Observaciones as varchar(max))))='' then Null Else @Observaciones End        
 ,Lectura=Case When ltrim(rtrim(Cast(@Lectura as varchar(max))))='' then Null Else @Lectura End        
   ,Transfer=@Transfer        
   ,TipoTransporte=Case When ltrim(rtrim(@TipoTransporte))='' then Null Else @TipoTransporte End        
 ,AtencionLunes=@AtencionLunes         
 ,AtencionMartes=@AtencionMartes         
 ,AtencionMiercoles=@AtencionMiercoles         
 ,AtencionJueves=@AtencionJueves         
 ,AtencionViernes=@AtencionViernes  
 ,AtencionSabado=@AtencionSabado         
 ,AtencionDomingo=@AtencionDomingo        
 ,HoraDesde=@HoraDesde         
 ,HoraHasta=@HoraHasta                    
      
      
 ,DesayunoHoraDesde=Case When @DesayunoHoraDesde='01/01/1900 00:00:00' Then Null Else @DesayunoHoraDesde End        
 ,DesayunoHoraHasta=Case When @DesayunoHoraHasta='01/01/1900 00:00:00' Then Null Else @DesayunoHoraHasta End   
 ,IDDesayuno=Case When ltrim(rtrim(@IDDesayuno))='' then Null Else @IDDesayuno End         
      
 ,PreDesayunoHoraDesde=Case When @PreDesayunoHoraDesde='01/01/1900 00:00:00' Then Null Else @PreDesayunoHoraDesde End        
 ,PreDesayunoHoraHasta=Case When @PreDesayunoHoraHasta='01/01/1900 00:00:00' Then Null Else @PreDesayunoHoraHasta End          
 ,IDPreDesayuno=Case When ltrim(rtrim(@IDPreDesayuno))='' then Null Else @IDPreDesayuno End         
 ,ObservacionesDesayuno=Case When ltrim(rtrim(@ObservacionesDesayuno))='' then Null Else @ObservacionesDesayuno End       
      
 ,HoraCheckIn=Case When @HoraCheckIn='01/01/1900 00:00:00' Then Null Else @HoraCheckIn End        
 ,HoraCheckOut=Case When @HoraCheckOut='01/01/1900 00:00:00' Then Null Else @HoraCheckOut End        
 --,TelefonoRecepcion1=Case When ltrim(rtrim(@TelefonoRecepcion1))='' then Null Else @TelefonoRecepcion1 End         
 --,TelefonoRecepcion2=Case When ltrim(rtrim(@TelefonoRecepcion2))='' then Null Else @TelefonoRecepcion2 End         
 --,FaxRecepcion1=Case When ltrim(rtrim(@FaxRecepcion1))='' then Null Else @FaxRecepcion1 End         
 --,FaxRecepcion2=Case When ltrim(rtrim(@FaxRecepcion2))='' then Null Else @FaxRecepcion2 End         
 --,IncluyeServiciosDias=@IncluyeServiciosDias        
 ,Dias=@Dias        
        
 ,PoliticaLiberado=@PoliticaLiberado        
 ,Liberado=Case When @Liberado=0 Then Null Else @Liberado End        
 ,TipoLib=Case When ltrim(rtrim(@TipoLib))='' Then Null Else @TipoLib End        
 ,MaximoLiberado=Case When @MaximoLiberado=0 Then Null Else @MaximoLiberado End        
 ,MontoL=Case When @MontoL=0 Then Null Else @MontoL End           
        
 ,SinDescripcion =  @SinDescripcion   
 ,CoPago = Case When LTRIM(rtrim(@CoTipoPago))='' Then Null Else @CoTipoPago End  
 ,UserMod=@UserMod        
 ,FecMod=GETDATE()
 , APTServicoOpcional = @APTServicoOpcional     
 , HoraPredeterminada = @HoraPredeterminada 
  WHERE IDServicio=@IDServicio       
END
