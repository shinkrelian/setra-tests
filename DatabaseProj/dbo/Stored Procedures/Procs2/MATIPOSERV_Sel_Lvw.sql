﻿CREATE Procedure [dbo].[MATIPOSERV_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion	
	From MATIPOSERV
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDservicio <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
