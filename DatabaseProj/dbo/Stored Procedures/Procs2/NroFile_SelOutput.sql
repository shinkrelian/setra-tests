﻿--HLF-20130610-Agregando If Cast(Right(@vcIDFile,4) as int)<100     
--JRF-20130923-Agregando la fecha de la cual se generara el File.  
--JRF-20150114-Obtener el Máximo File desde Ventas.
CREATE Procedure [dbo].[NroFile_SelOutput]      
 @IDCab int,    
 @FecInicioCoti smalldatetime,    
 @pIDFile varchar(8) Output      
As      
 Set NoCount On      
    
 Declare @IDFile varchar(8)=(Select IsNull(IDFile,'') From COTICAB Where IDCAB=@IDCab)      
    
 If @IDFile=''       
  Begin      
  Declare @FecInicio varchar(4)=(Select SubString((convert(varchar,@FecInicioCoti,112)),3,4)) --From COTICAB Where IDCAB=@IDCab)      
    
  Declare @iIDFile int=IsNull((Select MAX(IDFile) From COTICAB --RESERVAS       
  Where Left(IDFile,4)=@FecInicio), CAST(@FecInicio+'0000' AS INT))+1      
      
  Declare @vcIDFile varchar(8)=Cast(@iIDFile as varchar(8))    
  If Cast(Right(@vcIDFile,4) as int)<100     
   Set @pIDFile = left(@vcIDFile,4) + '0100'     
  Else    
   Set @pIDFile = Cast(@iIDFile as varchar(8))       
       
  End      
 Else      
  Set @pIDFile = @IDFile
