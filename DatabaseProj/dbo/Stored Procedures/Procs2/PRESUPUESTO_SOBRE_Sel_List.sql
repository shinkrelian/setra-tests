﻿--JRF-20140723-Agregar los filtros por Rango de Fecha y CoEstado.        
--HLF-20140813-Inner Join PRESUPUESTO_SOBRE_DET prd On pr.NuPreSob=prd.NuPreSob      
--Distinct       
--Left Join MAPROVEEDORES p On prd.CoPrvPrg=p.IDProveedor            
--HLF-20140915- DescEstado       
--When 'PG' Then 'GENERADO(Operaciones)'            
--HLF-20141016-cc.FecInicio as FechaIn  
--Order by X.FechaIn  
--cc.Titulo as Referencia,  
--Subquery ImporteSOL  
--JRF-20150304-Agregar un filtro para Categoria de File
--JRF-20150305-Considerar las columnas soles y dólares
--JRF-20150410-Considerar el nuevo campo relacionado al pax
--JRF-20150410-Filtrar por nombre del TC o del guía
--JHD-20150710-Se agrego el parámetro @Modificado para mostrar los presupuestos nuevos después del estado entregado.
--JHD-20150710-and  ((@Modificado=1 and Modificado=1) or @Modificado=0)   
--JHD-20150710-Modificado=case when CoEstado='NV' THEN 
		--		(
		--			case when (SELECT COUNT(*) as contador FROM PRESUPUESTO_SOBRE PP WHERE PP.IDCAB=pr.IDCab and pp.NuPreSob<>pr.NuPreSob and pp.CoEstado='EN')>0
		--			then cast(1 as bit) else cast(0 as bit) end
		--		) 
		--ELSE CAST(0 AS BIT) END
--JRF-Agregar el filtro x @NuPreSob
--HLF-20150827-
--Order by x.Modificado desc  
--HLF-20151113-Case When @CoCiudadCusco='' Then
--JRF-20151126-Agregar TConductor
--HLF-20151207-And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1) 
--HLF-20151217-Case When @IDProveedor='000544' Then ... (2 veces)
--HLF-20160126-DATEADD(DAY,1,DATEADD(SECOND,-1 ,@FeDetRango2)))    
CREATE Procedure dbo.PRESUPUESTO_SOBRE_Sel_List            
 @IDFile char(8),            
 @IDProveedor char(6),            
 @DescGuiaTC varchar(120),
 @CoEstado char(2),        
 @FeDetRango1 smalldatetime,        
 @FeDetRango2 smalldatetime,
 @Categoria char(1),
 @Modificado bit,
 @NuPreSob int,
 @FlHistorico bit=1 
As            
 Set Nocount On            

 Declare @CoCiudadCusco char(6)=''
 If @IDProveedor='000544'
	Set @CoCiudadCusco=(select IDCiudad From MAPROVEEDORES where IDProveedor=@IDProveedor)

 Select Distinct FechaIn, FePreSob,FechaServ, IDCab ,IDFile,NuPreSob , Referencia, NroPax,
  CoPrvPrg, 
  --DescGuia,
  Case When @IDProveedor='000544' Then
	(Select Nombre+' '+ISNULL(TxApellidos,'') from MAUSUARIOS where IDUsuario=x.CoPrvPrg)
  Else
	DescGuia
  End as  DescGuia,

  ImporteDOL, ImporteSOL, EjecOper,            
  EjecFinan, IDProveedor ,CoEstado, DescEstado ,IDPax, Modificado,IsNull(TConductor,0) as TConductor
 from (        
 Select prd.NuPreSob, pr.FePreSob, pr.IDCab ,cc.IDFile, cc.Titulo as Referencia,  
 --IsNull(prd.CoPrvPrg,'') as CoPrvPrg, 
 Case When @IDProveedor='000544' Then
	IsNull(cc.IDUsuarioOpe_Cusco,'')
 Else
	IsNull(prd.CoPrvPrg,'')
 End as CoPrvPrg, 
 IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescGuia, 
 uope.Nombre as EjecOper,            
 ufin.Nombre as EjecFinan, pr.IDProveedor ,pr.CoEstado,             
 Case pr.CoEstado            
  When 'NV' Then 'NUEVO'            
  When 'PG' Then 'GENERADO(Operaciones)'            
  When 'EL' Then 'ELABORADO'            
  When 'EN' Then 'ENTREGADO'            
  When 'EJ' Then 'EJECUTADO'            
  When 'AN' Then 'ANULADO'            
 End as DescEstado,        
 
 --cc.FecInicio as FechaIn,   
 Case When @CoCiudadCusco='' Then
	cc.FecInicio 
 Else
	isnull((Select Min(Dia) From COTIDET cd1   
	Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det  
	Where cd1.IDubigeo=@CoCiudadCusco And IDCAB=cc.IDCAB And SD1.idTipoOC<>'005'),cc.FecInicio)
 End  as FechaIn, 

 --(Select SUM(Case When sd1.CoMoneda='USD' Then   
 --   isnull(dbo.FnCambioMoneda(pd1.SsTotal,sd1.CoMoneda,'SOL',dbo.FnTipoDeCambioVtaUltimoxFecha(ps1.FePreSob) ),0)  
 --   Else  
 --   isnull(pd1.SsTotal,0)  
 --   End ) as SumaImporte  
 -- From PRESUPUESTO_SOBRE_DET pd1 Inner Join MASERVICIOS_DET sd1 On pd1.IDServicio_Det=sd1.IDServicio_Det  
 -- Inner Join PRESUPUESTO_SOBRE ps1 On pd1.NuPreSob=ps1.NuPreSob  
 --Where pd1.NuPreSob=prd.NuPreSob) As ImporteSOL  ,
 IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE pr.NuPreSob=PD2.NuPreSob And IsNull(IDMoneda,'')='USD' and PD2.CoPago <> 'TCK'),0) As ImporteDOL,
 IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE pr.NuPreSob=PD2.NuPreSob And IsNull(IDMoneda,'')='SOL' and PD2.CoPago <> 'TCK'),0) As ImporteSOL,
 cc.NroPax+IsNull(cc.NroLiberados,0) as NroPax,
 Case When cc.NroPax+IsNull(cc.NroLiberados,0) <= 8 then 'F' Else 'G' End as Categoria,
 IsNull(pr.IDPax,0) as IDPax,
 Modificado=case when CoEstado='NV' THEN 
	(
		case when (SELECT COUNT(*) as contador FROM PRESUPUESTO_SOBRE PP WHERE PP.IDCAB=pr.IDCab 
			and pp.NuPreSob<>pr.NuPreSob and pp.CoEstado='EN'
			--and pp.CoPrvGui<>pr.CoPrvGui
			)>0
		then cast(1 as bit) else cast(0 as bit) end
	) 
		ELSE CAST(0 AS BIT) END,
 Convert(char(10),(select min(psd.FeDetPSo) from PRESUPUESTO_SOBRE_DET psd Where psd.NuPreSob = pr.NuPreSob),103) as FechaServ,IsNull(pr.FlTourConductor,0) as TConductor
From PRESUPUESTO_SOBRE pr Inner Join COTICAB cc On pr.IDCab=cc.IDCAB            
 Inner Join PRESUPUESTO_SOBRE_DET prd On pr.NuPreSob=prd.NuPreSob      
 --Left Join MAPROVEEDORES p On pr.CoPrvGui=p.IDProveedor            
 Left Join MAPROVEEDORES p On prd.CoPrvPrg=p.IDProveedor            
 Left Join MAUSUARIOS uope On pr.CoEjeOpe=uope.IDUsuario            
 Left Join MAUSUARIOS ufin On pr.CoEjeFin=ufin.IDUsuario            
 Left Join COTIPAX cp On pr.IDPax=cp.IDPax
 Where (cc.IDFile Like '%' + @IDFile + '%' Or ltrim(rtrim(@IDFile))='')            
 And pr.IDProveedor=@IDProveedor            
 --And (pr.CoPrvGui=@IDProveedor_PrgGuia Or ltrim(rtrim(@IDProveedor_PrgGuia))='')            
 and (LTRIM(rtrim(@CoEstado))='' Or pr.CoEstado = @CoEstado)
 And (cc.FlHistorico=@FlHistorico Or @FlHistorico=1)  
 )as X        
 where 
 (ltrim(rtrim(@DescGuiaTC))='' Or x.DescGuia Like '%'+ltrim(rtrim(@DescGuiaTC))+'%') And
 (Ltrim(Rtrim(@Categoria))='' Or x.Categoria=@Categoria) and
 (convert(char(10),@FeDetRango1,103)='01/01/1900'         
 Or X.FechaIn between @FeDetRango1 and DATEADD(DAY,1,DATEADD(SECOND,-1 ,@FeDetRango2)))    
 and  ((@Modificado=1 and Modificado=1) or @Modificado=0)     
 And (x.NuPreSob=@NuPreSob Or @NuPreSob=0)
  
 --Order by X.FePreSob            
 Order by x.Modificado desc,X.FechaIn

