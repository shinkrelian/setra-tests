﻿--HLF-20120823-Where D.Total>0                                                      
--HLF-20121122-Agregando Idioma                                                      
--HLF-20130225-Agregando And S.IDTipoProv<>'016'                                                  
--HLF-20130523-Agregando columna DiaFormatFecha y Order DiaFormatFecha                                                
--Agregar columna ST.Tipo as Variante                                                
--Cambiar NroPax a posicion 7                                                
--HLF-20130524-Agregando PaxmasLiberados                                                
--Reemplazando S.Descripcion por rd.Servicio                                              
--HLF-20130604-Agregando columna MotivoEspecial y Left Join con Cotidet                                              
--HLF-20130606-Cambiando PaxmasLiberados                                             
--HLF-20130704-Agregando and od1.IDServicio_Det=x.IDServicio_Det a todos los subqueys                                           
--HLF-20130717-Deshaciendo cambio del día 20130704 y remplazándolo por And sd1.IDServicio=X.IDServicio                                        
--HLF-20130718-Agregando '' as Del                                        
--HLF-20130718-Agregando And l1.IDServicio_Det=Y.IDServicio_Det a los subquerys de CambiosReservasLog                                       
--HLF-20130718-Agregando columna IDServicio_Det                                      
--HLF-20130718-Cambiando  rd.Servicio por d.Servicio                                       
--HLF-20130805-Agregando campos Trasladista                                    
--HLF-20130815-Agregando subquerys para Trasladistas                                
--HLF-20130820-Comentando And sd1.IDServicio=X.IDServicio y agregando Top 1 en subquerys                                        
--HLF-20130828-Descomentando subquery de RESERVAS_DET_LOG para Accion B                              
--HLF-20130903-Descomentando And sd1.IDServicio=X.IDServicio   para EstadoGuia, EstadoTransportista                            
--HLF-20130903-Cambiando INNER JOIN OPERACIONES_DET_DETSERVICIOS od On D.IDOperacion_Det = od.IDOperacion_Det                                                        
-- por LEFT JOIN OPERACIONES_DET_DETSERVICIOS od On D.IDOperacion_Det = od.IDOperacion_Det                                                        
--HLF-20130904-Reemplazando od.IDOperacion_Det por d.IDOperacion_Det                          
--HLF-20130904-Reemplazando And sd1.IDServicio=X.IDServicio por And od1.IDServicio=X.IDServicio                                        
--HLF-20130926-Comentando Where D.Total>0                         
--HLF-20131023-Agregando Case When Rv.CambiosenReservasPostFile                     
--HLF-20131114-Inner Join RESERVAS r On o.IDReserva=r.IDReserva And r.Anulado=0                    
--JRF-20140623-Ingresar el Campo de CoEstadoPresupuesto.            
--HLF-20140721-and p.IDTipoProv Not In ('004','005','008')                
--HLF-20140725-and p.IDTipoProv In ('006')          
--JRF-20140918-Case When dbo.FnCambiosenReservasPostFileOperaciones(IDOperacion) = 1 Then            
--JRF-20150106-Solo para casos de Translivik cargar ambos correos.  
--JRF-20150326-Agregar Where FlServicioTC=0
--JRF-20150330-Agregar Columnas para la generación de Presupuesto Cusco (IDUsuarioTrasladista]
--JRF-20150408-Agregar filtro para la segmentación del Tour Conductor
--JHD-20150411-IsNull((Select NoVehiculoCitado + ' - ' + CAST(QtCapacidad as varchar(3))+' Pax' From MAVEHICULOS_TRANSPORTE Where NuVehiculo = X.IDVehiculo_Prg),'') As VehiculoProgramado,                 
--JHD-20150527- ,o.Estado_Reserva
--JHD-20150527- ,isnull(Estado_Reserva,'NV') as Estado_Reserva,	 
--JHD-20150527- Estado_Reserva, 
--OPERACIONES_DET_List_ServiciosxProvInt '000544',10822,0
--HLF-20150907-in Trasladistas Lima '020'
--HLF-20150908- Case When @FlServicioTC=0 Then
--JRF-20150914-AND (S.IDProveedor=@IDProveedor Or (s.IDProveedor In (select IDProveedor from MAPROVEEDORES p5 Where p5.Activo='A' and p5.IDTipoProv='021') And d.FlServicioTC=1)) And (S.IDTipoProv<>'016' Or (S.IDTipoProv='021' And d.FlServicioTC=1))...
--HLF-20150921-cambios en as IDTrasladistaProgramado,
--PPMG-20151002-Se agrego un proveedor mas para juntar los email 3 y 4 IDTransportistaProgramado='002574'
CREATE Procedure [dbo].[OPERACIONES_DET_List_ServiciosxProvInt]                                                           
 @IDProveedor char(6),                                                          
 @IdCab Integer,
 @FlServicioTC bit =0                                                        
As                                                          
 Set NoCount On                                                          
                           
 SELECT Dia,Hora, IDOperacion, DescServicio, IDTipoServ,IDIdioma, Variante,                                                 
 NroPax, PaxmasLiberados, IDServicio, IDServicio_Det ,                                                        
 Y.IDCiudad,Y.IDTipoProv,Y.DescTipoServ,Y.IDProveedor,                                    
 ISNULL(CorreoReservas,'') As CorreoReservas,IDPais,                                                          
 Desayuno,Lonche,Almuerzo,Cena,Transfer,             
 IDOperacion_Det,                                                        
 NroLiberados, Tipo_Lib, TipoPax,TotalCotizado,TotalProgramado,                    
 EstadoGuia,
 Case When Y.IDTipoProv='017' Then
	Case When isnull(IDTrasladistaProgramado,'')='' Then 'NV' Else 'PA' End 
 Else
	EstadoTrasladista
 End EstadoTrasladista,                                    
 EstadoVehiculo,EstadoTransportista, 
 Estado_Reserva,                                   
 ObservVoucher,ObservBiblia,ObservInterno,                                   
 IDGuiaProgramado,IDTrasladistaProgramado,IDVehiculoProgramado,                                  
 IDTransportistaProgramado,                                             
 ISNULL(Case When ISNULL(PG.Email3,'')='' Then PG.Email4 Else PG.Email3 End,'') as CorreoReservasGuiaPrg,                                                       
 Case When Y.IDTipoProv='017' Then
	ISNULL((Select Correo From MAUSUARIOS Where IDUsuario=Y.IDTrasladistaProgramado),'') 
 Else
	ISNULL(Case When ISNULL(PTL.Email3,'')='' Then PTL.Email4 Else PTL.Email3 End,'')
 End as CorreoReservasTrasladistaPrg,                                      
 Case When IDTransportistaProgramado = '000467'
		Or IDTransportistaProgramado='000527'
		Or IDTransportistaProgramado='002574' Then ISNULL(PT.Email3,'')+Isnull('; '+ PT.Email4+';','') else     
  ISNULL(Case When ISNULL(PT.Email3,'')='' Then PT.Email4 Else PT.Email3 End,'') End as CorreoReservasTranspPrg,                                                 
 GuiaProgramado,                                
 Case When Y.IDTipoProv='017' Then
	ISNULL((Select Nombre From MAUSUARIOS Where IDUsuario=Y.IDTrasladistaProgramado),'') 
 else 
	TrasladistaProgramado
 End as TrasladistaProgramado,                                
 VehiculoProgramado,TransportistaProgramado,                                   
 MotivoEspecial,                                        
 '' as Del,                                                          
  Case When dbo.FnCambiosenReservasPostFileOperaciones(IDOperacion) = 1 Then      
    'M'                      
   Else                                                         
    'O'                                                        
   End                      
                      
 As CambiosReservasLog,                 
 Y.EstadoPresupuesto As EstadoPresupuesto    ,                
 Y.Anio_OperDet            ,
 Y.UserTrasGuiaProgramado    
 FROM (                                                        
                                                   
 SELECT DiaFormatFecha,                                                
 Dia,Hora, IDOperacion, DescServicio, Variante, IDTipoServ,IDServicio,                                                        
 IDCiudad,IDTipoProv,DescTipoServ,IDProveedor,CorreoReservas,IDPais,                                     
 Desayuno,Lonche,Almuerzo,Cena,Transfer,                                                        
 --Anio, Variante,                            
 IDOperacion_Det,                                                        
 NroPax, PaxmasLiberados, NroLiberados, Tipo_Lib, TipoPax,TotalCotizado,TotalProgramado,                    
                                                  
 Case When @FlServicioTC=0 Then
	 Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                                        
	 Else                                                        
	 IsNull(                                                        
	 (Select Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                                              
	 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                            
	 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det
	 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
                             
	 And od1.IDServicio=X.IDServicio                                        
	 ) ,'NV')
	 End
 Else
	 Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                                        
	 Else                                                        
	 IsNull(                                                        
	 (Select Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                                              
	 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('021')	 
	 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
	 ) ,'NV')
	 End
 End as EstadoGuia,                                                        
                        
                              
 Case When IDVehiculo_Prg is null Then 'NV' Else 'PA' End EstadoVehiculo,                                    
                                              
 Case When CantTranspProgramados>1 Then 'MULTIPLE'                                                      
 Else                                                        
 IsNull(                                                        
 (Select Distinct Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                                                         
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                                
 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                              
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
                             
 And od1.IDServicio=X.IDServicio                            
                             
 )-- and od1.IDServicio_Det=x.IDServicio_Det)                                                        
 ,'NV')                                                        
 End as EstadoTransportista,                              
       
Case When CantTrasladistasProgramados>1 Then 'MULTIPLE'                                                        
 Else                                                        
 IsNull(                                                        
 (Select Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                                              
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('020')
 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                                
 Where od1.IDOperacion_Det=X.IDOperacion_Det
 And od1.IDServicio=X.IDServicio)
 ,'NV')                                     
 End as EstadoTrasladista,    

--jorge
isnull(Estado_Reserva,'NV') as Estado_Reserva,	   
	                              
 ObservVoucher,ObservBiblia,ObservInterno,                                                        
 Case When @FlServicioTC=0 Then                    
	 Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                                        
	 Else                                                        
	 IsNull(                                                        
	 (Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                         
	 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                              
	 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                                                                   
	 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
	 And od1.IDServicio=X.IDServicio                             
	 )
	 ,'')                                                        
	 End                                                        
 Else
 	 Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                                        
	 Else                                                        
	 IsNull(                                                        
	 (Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                         
	 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('021')
	 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                                                                   
	 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
	 And od1.IDServicio=X.IDServicio                             
	 )
	 ,'')                                                        
	 End                                                        
 End
 as IDGuiaProgramado,                                  
                                                   
 --IsNull(X.IDTrasladista_Prg,'') As IDTrasladistaProgramado,           
                     
                              
 Case When CantTrasladistasProgramados >1 Then 'MULTIPLE'                                                        
 Else                                                        
 IsNull(                                                        
 (Select Distinct Top 1 IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1           
 -- Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det_V_Prg=sd1.IDServicio_Det                    
 --Inner Join MASERVICIOS s1 On s1.IDServicio=sd1.IDServicio                                 
 --Inner Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor And p1.IDTipoProv in('017','020')                               
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('017','020')
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det  
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                 
 And od1.IDServicio=X.IDServicio                                         
                              
 )                                
 ,'')                                     
 End                                                        
 as IDTrasladistaProgramado,
 IsNull(cast (X.IDVehiculo_Prg as varchar(3)),'') As IDVehiculoProgramado,                                    
                                                   
 Case When CantTranspProgramados >1 Then 'MULTIPLE'                                                        
 Else                                                        
 IsNull(    
 (Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                           
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                              
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                      
                      
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
 And od1.IDServicio=X.IDServicio                                        
 )-- and od1.IDServicio_Det=x.IDServicio_Det)                                                        
 ,'')                                                        
 End                                                        
 as IDTransportistaProgramado,                                                      
                                                   
                                                   
                                                   
 Case When @FlServicioTC=0 Then                                        
	 Case When CantGuiasProgramados>1 Then 'MULTIPLE'               Else                                                        
	 IsNull(                                                        
	 (Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                         
	 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                             
	 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                                                                    
	 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
	 And od1.IDServicio=X.IDServicio                                        
	 )-- and od1.IDServicio_Det=x.IDServicio_Det)                                                                   
	 ,'')                                 
	 End
 Else
	 Case When CantGuiasProgramados>1 Then 'MULTIPLE'               Else                                                        
	 IsNull(                                                        
	 (Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                         
	 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('021')
	 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                                                                    
	 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
	 And od1.IDServicio=X.IDServicio                                        
	 )-- and od1.IDServicio_Det=x.IDServicio_Det)                                                                   
	 ,'')                                 
	 End
 End as GuiaProgramado,
                                  
                                  
 --IsNull((Select Nombre From MAUSUARIOS Where IDUsuario = X.IDTrasladista_Prg),'') As TrasladistaProgramado,                                
 --IsNull((Select NoUnidadProg + ' - ' + CAST(QtCapacidadPax as varchar(3))+' Pax' From MAVEHICULOSPROGRAMACION Where NuVehiculoProg = X.IDVehiculo_Prg),'') As VehiculoProgramado,                                    
 IsNull((Select NoVehiculoCitado + ' - ' + CAST(QtCapacidad as varchar(3))+' Pax' From MAVEHICULOS_TRANSPORTE Where NuVehiculo = X.IDVehiculo_Prg),'') As VehiculoProgramado,                                    
                                                   
 Case When CantTranspProgramados >1 Then 'MULTIPLE'                                                        
 Else                                                        
 IsNull( 
 (Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                   
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                       
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                      
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                         
 And od1.IDServicio=X.IDServicio                                        
 )-- and od1.IDServicio_Det=x.IDServicio_Det)                                              
 ,'')                                                        
 End                                                        
 as TransportistaProgramado,                                          
                                  
 
 Case When CantTrasladistasProgramados >1 Then 'MULTIPLE'                                                        
 Else                                                        
 IsNull(                                                        
 (Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                   
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('020')
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                      
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                         
 And od1.IDServicio=X.IDServicio                                        
 )
 ,'')                                                        
 End                                                        
 as TrasladistaProgramado,                                          
 
                                 
                                              
 X.Idioma as IDIdioma, MotivoEspecial,                                      
 X.IDServicio_Det,                      
 X.CambiosenReservasPostFile   ,                
 X.EstadoPresupuesto     ,                
 X.Anio_OperDet   ,
 
 Case When CantGuiasProgramados>1 Then 'MULTIPLE'               Else                                                        
 IsNull(                                                        
 (Select Distinct Top 1 Ltrim(Rtrim(IsNull(p1.IDUsuarioTrasladista,''))) From OPERACIONES_DET_DETSERVICIOS od1                                                         
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                             
 --Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                      
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                        
 And od1.IDServicio=X.IDServicio                                        
 )-- and od1.IDServicio_Det=x.IDServicio_Det)                                                                   
 ,'')                                 
 End
 as UserTrasGuiaProgramado--,IDTipoProv             
 FROM                                                        
 (                                                        
 Select Distinct D.Dia as DiaFormatFecha,                                                      
 upper(substring(DATENAME(dw,D.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,D.Dia,103)/*+' '+ substring(convert(varchar,D.Dia,108),1,5)*/)) As Dia                                                        
 ,convert(varchar(5),D.Dia,108) As Hora                                                      
 ,D.IDOperacion,                                                
 --S.Descripcion as DescServicio                                                
 --rd.Servicio as DescServicio                                
 d.Servicio as DescServicio                                       
 ,ST.Tipo as Variante,idio.Siglas as Idioma,S.IDTipoServ,                      
                           
 D.IDServicio                                                          
                           
 ,D.IDubigeo as IDCiudad,S.IDTipoProv,ts.Descripcion as DescTipoServ,S.IDProveedor ,                                                          
 Case When ISNULL(P.Email3,'')='' Then P.Email4 Else P.Email3 End as CorreoReservas,u.IDPais,                                                          
 D.Desayuno,D.Lonche,D.Almuerzo,D.Cena,D.Transfer ,           
 --ST.Anio, IsNull(ST.Tipo,'') as Variante,                   
 D.IDOperacion_Det,                                                         
 D.NroPax,                                                 
 --CAST(d.NroPax AS VARCHAR(3))+                                                    
 --Case When isnull(d.NroLiberados,0)=0 then '' else '+'+CAST(d.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,                                                      
 d.NroPax+isnull(d.NroLiberados,0) as PaxmasLiberados,                                                      
 D.NroLiberados, D.Tipo_Lib, cc.TipoPax,                                                        
                                                   
 (Select SUM(Total) From OPERACIONES_DET_DETSERVICIOS Where IDOperacion_Det=D.IDOperacion_Det and                                      
 Not IDServicio_Det_V_Cot Is Null) as TotalCotizado,                                                        
 (Select SUM(Total) From OPERACIONES_DET_DETSERVICIOS Where IDOperacion_Det=D.IDOperacion_Det and                                                         
 Not IDServicio_Det_V_Prg Is Null) as TotalProgramado,                                             
                                        
 st.IDServicio_Det ,                                           

Case When @FlServicioTC=0 Then                                                   
 (Select count(*) From                                                        
 (                         
 Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                         
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                                      
 Where od1.IDOperacion_Det=D.IDOperacion_Det --and od1.IDServicio_Det=st.IDServicio_Det                                        
 And od1.IDServicio=D.IDServicio                                        
 ) as x                                                          
 ) 
 Else
(Select count(*) From                                                        
 (                         
 Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                         
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('021')
 Where od1.IDOperacion_Det=D.IDOperacion_Det 
 And od1.IDServicio=D.IDServicio                                        
 ) as x                                                          
 )
 End as CantGuiasProgramados,                                                        
                                                   
 (Select count(*) From                                                        
 (                                                        
 Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                         
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                                        
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                        
 Where od1.IDOperacion_Det=D.IDOperacion_Det --and od1.IDServicio_Det=st.IDServicio_Det                                                                    
 And od1.IDServicio=D.IDServicio                  
 ) as x                                                          
 ) as CantTranspProgramados,                                      
               
 (Select count(*) From                                            
 (                                                        
 Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det_V_Prg=sd1.IDServicio_Det                                        
 Inner Join MASERVICIOS s1 On s1.IDServicio=sd1.IDServicio                                 
 Inner Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor And p1.IDTipoProv in('020')
 Where od1.IDOperacion_Det=D.IDOperacion_Det                             
 And od1.IDServicio=D.IDServicio                                        
 ) as x                                                          
 ) as CantTrasladistasProgramados,                                      
                   
 isnull(D.ObservVoucher,'') As ObservVoucher,                                                      
 isnull(D.ObservBiblia,'') As ObservBiblia ,isnull(D.ObservInterno,'') As  ObservInterno,                                              
 isnull(cd.MotivoEspecial,'') as MotivoEspecial,                                    
 D.IDTrasladista_Prg, D.IDVehiculo_Prg,                      
 o.CambiosenReservasPostFile ,                
                 
 Case when Exists(select p.NombreCorto                  
     from OPERACIONES_DET_DETSERVICIOS odd                 
     Left Join MASERVICIOS_DET sd On odd.IDServicio_Det_V_Cot =sd.IDServicio_Det                
     Left Join MASERVICIOS s On s.IDServicio = sd.IDServicio                
     Left Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor                
     where IDOperacion_Det = D.IDOperacion_Det and odd.IDServicio = D.IDServicio           
  and p.IDTipoProv = '006')                
  --and p.IDTipoProv Not In ('004','005','008'))                
Then                 
 --Case When D.IDServicio_Det = 16485 then 'SA' Else 'SP' End                
 --'SP'                
 Case when Exists(select p.NombreCorto                  
     from OPERACIONES_DET_DETSERVICIOS odd                 
     Left Join MASERVICIOS_DET sd On odd.IDServicio_Det_V_Cot =sd.IDServicio_Det                
     Left Join MASERVICIOS s On s.IDServicio = sd.IDServicio                
     Left Join MAPROVEEDORES p On s.IDProveedor = p.IDProveedor                
     where IDOperacion_Det = D.IDOperacion_Det and odd.IDServicio = D.IDServicio            
     and odd.Activo = 1 and p.IDTipoProv = '006'           
     --and p.IDTipoProv Not In ('004','005','008')                
     and odd.FlMontosSincerados=1             
     AND odd.CoEstadoSincerado IN ('PS','SA')) then             
  --'SA'               
    dbo.FnEstadosSinceradoxIDOperacionDetxIDServicio(D.IDOperacion_Det,D.IDServicio)            
    Else 'SP' End              
Else 'NA' End as EstadoPresupuesto,                
 ST.Anio as Anio_OperDet     
 --jorge           
 ,d.Estado_Reserva
 FROM OPERACIONES_DET D                                                           
 Inner JOIN MASERVICIOS S ON D.IDServicio=S.IDServicio AND (S.IDProveedor=@IDProveedor Or (s.IDProveedor In (select IDProveedor from MAPROVEEDORES p5 Where p5.Activo='A' and p5.IDTipoProv='021') And d.FlServicioTC=1)) --S.IDProveedor=@IDProveedor        
                           
 And (S.IDTipoProv<>'016' Or (S.IDTipoProv='021' And d.FlServicioTC=1)) --S.IDTipoProv<>'016'                                                  
 Inner JOIN MAPROVEEDORES P On S.IDProveedor = P.IDProveedor                                                           
 Inner JOIN MATIPOSERVICIOS ts On S.IDTipoServ=ts.IDTipoServ                                                              
 Inner JOIN MAUBIGEO u On p.IDCiudad=u.IDubigeo                        
 Inner JOIN MASERVICIOS_DET ST On D.IDServicio_Det = ST.IDServicio_Det                            
 --Inner  JOIN OPERACIONES_DET_DETSERVICIOS od On D.IDOperacion_Det = od.IDOperacion_Det                                                       
 LEFT JOIN OPERACIONES_DET_DETSERVICIOS od On D.IDOperacion_Det = od.IDOperacion_Det                                                        
 Inner Join OPERACIONES o On d.IDOperacion=o.IDOperacion And o.IDCab=@IdCab --Or o.IDReserva is Null            
 Left Join RESERVAS r On o.IDReserva=r.IDReserva And IsNull(r.Anulado,0)=0 
 Inner Join COTICAB cc On o.IDCab=cc.IDCAB                                                  
 Left Join RESERVAS_DET rd On d.IDReserva_Det=rd.IDReserva_Det                                                      
 Left Join COTIDET cd On rd.IDDet=cd.IDDET                                              
 Left Join MAIDIOMAS idio On D.IDIdioma = idio.IDidioma                  
 --Where D.Total>0                                                      
 Where D.FlServicioTC=@FlServicioTC           
 ) as X  ) AS Y                                                 
 Left Join MAPROVEEDORES PG On Y.IDGuiaProgramado=PG.IDProveedor                          
 Left Join MAPROVEEDORES PT On Y.IDTransportistaProgramado=PT.IDProveedor                                                
 Left Join MAPROVEEDORES PTL On Y.IDTrasladistaProgramado=PTL.IDProveedor                                                
 Order by DiaFormatFecha


