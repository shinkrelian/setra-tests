﻿

Create Procedure dbo.OPERACIONES_DET_Sel_ExistenxIDDetOutput
	@IDDet int,        
	@pbOk bit Output        
As        
	Set Nocount On        

	Set @pbOk = 0        
	If Exists(Select IDOperacion_Det From OPERACIONES_DET_DETSERVICIOS     
	Where IDOperacion_Det In (Select IDOperacion_Det From OPERACIONES_DET     
	Where IDReserva_Det In (Select IDReserva_Det From RESERVAS_DET Where IDDet=@IDDet) )      	
	)   	  
		Set @pbOk = 1      
		
