﻿
CREATE Procedure [dbo].[MASERVICIOS_RANGO_APT_Del]
	@Correlativo tinyint,
    @IDServicio char(8),
	@IDTemporada int
AS
BEGIN
	Set NoCount On
	
	Delete From MASERVICIOS_RANGO_APT 
	WHERE
           Correlativo = @Correlativo And IDTemporada = @IDTemporada AND
           (IDServicio = @IDServicio)
	IF NOT EXISTS(SELECT IDServicio, IDTemporada From MASERVICIOS_RANGO_APT 
				  WHERE IDTemporada = @IDTemporada AND (IDServicio = @IDServicio))
	BEGIN
		DELETE FROM MASERVICIOS_DESC_APT
		WHERE IDTemporada = @IDTemporada AND (IDServicio = @IDServicio)
	END
END
