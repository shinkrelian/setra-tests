﻿
CREATE PROCEDURE [dbo].[NOTA_VENTA_DET_DelxIDCab]
	@IDCab	int
As
	Set Nocount On
	
	DELETE FROM NOTA_VENTA_DET WHERE IDReserva_Det 
		In (Select IDReserva_Det From RESERVAS_DET Where IDDet In (Select IDDet From RESERVAS Where IDCab=@IDCab))
