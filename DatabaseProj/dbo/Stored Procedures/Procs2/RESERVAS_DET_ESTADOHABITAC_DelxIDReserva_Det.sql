﻿
--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet
CREATE PROCEDURE [dbo].[RESERVAS_DET_ESTADOHABITAC_DelxIDReserva_Det]
	@IDReserva_Det	int
As
	Set Nocount On
	
	Delete From RESERVAS_DET_ESTADOHABITAC 
		--Where IDReserva_Det=@IDReserva_Det
		Where IDDet In (Select IDDet From RESERVAS_DET Where IDReserva_Det=@IDReserva_Det)

