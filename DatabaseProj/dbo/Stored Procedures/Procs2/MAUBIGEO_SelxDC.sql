﻿  
CREATE PROCEDURE MAUBIGEO_SelxDC
	@DC char(3)
AS      
BEGIN
	SET NOCOUNT ON   
       
	SELECT	IDubigeo, Descripcion
	FROM	MAUBIGEO
	Where	DC = @DC
END
