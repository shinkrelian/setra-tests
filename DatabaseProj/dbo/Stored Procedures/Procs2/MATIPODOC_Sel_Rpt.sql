﻿CREATE Procedure [dbo].[MATIPODOC_Sel_Rpt]
	@Descripcion varchar(60)
As	
	Set NoCount On

	Select IDtipodoc,Descripcion, Sunat 
	From MATIPODOC Where (Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='')
	order by Descripcion
