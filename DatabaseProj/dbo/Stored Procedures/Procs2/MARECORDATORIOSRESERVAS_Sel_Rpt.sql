﻿CREATE PROCEDURE [dbo].[MARECORDATORIOSRESERVAS_Sel_Rpt]
 @Descripcion varchar(200)
 as
 begin
	set nocount on
	select descripcion,dias,case when activo=0 OR activo=null then 'INACTIVO' else 'Activo' end Activo
	from dbo.MARECORDATORIOSRESERVAS 
	Where (Descripcion Like '%'+@Descripcion+'%' Or @Descripcion=0)
 end
