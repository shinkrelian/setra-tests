﻿

-----------------------------------------------------


---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE NOTAVENTA_COUNTER_INS

		   @NuNtaVta char(6),
           @FeNtaVta smalldatetime,
           @SsNtaVta numeric(10,2),
           @CoCliente char(6),
           --@NoNtaVta varchar(150),
           --@QtPax smallint,
           --@TxDetalle varchar(150),
           --@CoSegmento char(1),
           --@SsCosto numeric(10,2),
           --@SsUtilidad1 numeric(10,2),
           --@SsUtilidad2 numeric(10,2),
           --@CoForPag char(3)='',
           --@SsPago numeric(10,2)='',
           --@CoAeroLinea char(6),
           @CoEjecutivo char(4),
           --@TxObservaciones varchar(200)='',
           @UserMod char(4),
           @PNuNtaVta char(6) output,
		   @CoMoneda char(3)='',
		   @SSTipoCambio numeric(8,2)=0
AS
BEGIN
INSERT INTO [NOTAVENTA_COUNTER]
           ([NuNtaVta]
           ,[FeNtaVta]
           ,[SsNtaVta]
           ,[CoCliente]
           --,[NoNtaVta]
           --,[QtPax]
           --,[TxDetalle]
           --,[CoSegmento]
           --,[SsCosto]
           --,[SsUtilidad1]
           --,[SsUtilidad2]
           --,[CoForPag]
           --,[SsPago]
           --,[CoAeroLinea]
           ,[CoEjecutivo]
           --,[TxObservaciones]
           ,[UserMod]
           ,CoMoneda
           ,SSTipoCambio)
     VALUES
           (@NuNtaVta,
           @FeNtaVta, 
           @SsNtaVta,
           @CoCliente, 
           --@NoNtaVta,
           --@QtPax,
           --@TxDetalle,
           --@CoSegmento, 
           --@SsCosto,
           --@SsUtilidad1,
           --@SsUtilidad2, 
           --Case When ltrim(rtrim(@CoForPag))='' Then Null Else @CoForPag End,
           --Case When ltrim(rtrim(@SsPago))='' Then Null Else @SsPago End,
           --@CoAeroLinea, 
           @CoEjecutivo, 
           --Case When ltrim(rtrim(@TxObservaciones))='' Then Null Else @TxObservaciones End,
           @UserMod,
           case When ltrim(rtrim(@CoMoneda))='' Then Null Else @CoMoneda End,
           Case When @SSTipoCambio = 0 Then Null Else @SSTipoCambio End)
           
           set @PNuNtaVta=@NuNtaVta

END;           

