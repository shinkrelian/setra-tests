﻿
  
--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet  
--HLF-20130618-Agregando parametro @NuIngreso
CREATE Procedure [dbo].[RESERVAS_DET_ESTADOHABITAC_Upd]  
 @IDDet int,   
 @NuIngreso tinyint,
 @Simple tinyint,  
 @Twin tinyint,  
 @Matrimonial tinyint ,  
 @Triple tinyint,   
 @UserMod char(4)  
As  
  
 Set Nocount On  
   
 Update RESERVAS_DET_ESTADOHABITAC  
 Set Simple=Case When @simple=0 Then Null Else @simple End,  
  Twin=Case When @Twin=0 Then Null Else @Twin End,  
  Matrimonial=Case When @Matrimonial=0 Then Null Else @Matrimonial End,  
  Triple=Case When @Triple=0 Then Null Else @Triple End,  
  UserMod=@UserMod,FecMod=getdate()  
 Where IDDet=@IDDet And NuIngreso = @NuIngreso   
    
