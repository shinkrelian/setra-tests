﻿Create Procedure [dbo].[MATIPOUBIG_Ins]
	@Descripcion varchar(20),	
    @UserMod char(4)
As
	Set NoCount On
	
	
	INSERT INTO MATIPOUBIG
           ([Descripcion]
           ,[UserMod]
           )
     VALUES
           (@Descripcion,            
            @UserMod)
