﻿


Create Procedure [dbo].[ORDENPAGO_SelExistexFileOutPut]
@IDCab int,
@pbExiste bit OutPut
As
	Set NoCount On
	If Exists(select IDOrdPag from ORDENPAGO Where IDCab = @IDCab)
		Set @pbExiste = 1
	Else
		Set @pbExiste = 0

