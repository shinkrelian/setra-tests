﻿Create Procedure dbo.MASERVICIOS_DET_SelTieneSubServiciosGuiasOutput
	@IDServicio_Det int,
	@pSubservGuias bit output
As
	Set Nocount on

	Set @pSubservGuias=0

	If exists(Select sd1.IDServicio_Det FROM MASERVICIOS_DET SD1 
	INNER JOIN MASERVICIOS_DET SDV ON SD1.IDServicio_Det_V=SDV.IDServicio_Det
	INNER JOIN MASERVICIOS SV ON SDV.IDServicio=SV.IDServicio
	INNER JOIN MAPROVEEDORES PV ON SV.IDProveedor=PV.IDProveedor AND PV.IDTipoProv IN('005','008')
		and pv.IDProveedor NOT IN('000470','001829')
	Where sd1.IDServicio+' '+sd1.Anio+' '+sd1.Tipo IN (Select IDServicio+' '+Anio+' '+Tipo 
		From MASERVICIOS_DET where IDServicio_Det=@IDServicio_Det))
		Set @pSubservGuias=1
