﻿--20120928-JRF- Nuevo campo IdiomaPc (Generación del Excel)
--20150716-JHD- Mostrar todos los campos de usuario para poder concaterar nombres + apellidos
CREATE Procedure dbo.MAUSUARIOS_Sel_xUsuario  
 @Usuario varchar(20)  
As  
 Set NoCount On  
   
 Select --u.*
 IDUsuario
      ,Usuario
      --,Nombre=Nombre+' '+isnull(TxApellidos,'')
	  ,Nombre=dbo.FnDevuelveFirstOrLastName(Nombre,1)+' '+isnull(TxApellidos,'') 
      ,Direccion
      ,Telefono
      ,Celular
      ,Nivel
      ,Password
      ,u.Siglas
      ,FechaNac
      ,u.Activo
      ,Directorio
      ,Correo
      ,IdArea
      ,IdiomaPc
      ,EsVendedor
      ,CambioPassword
      ,u.UserMod
      ,u.FecMod
      ,Profile_id
      ,Account_id
      ,FlSegPersonalizada
      ,IDUbigeo
      ,CoUsrAmd
      ,CoUsrAmd2
      ,TxApellidos
      ,FecIngreso
      ,CoSexo
      ,CoTipoDoc
      ,NumIdentidad
      ,CoEmpresa
      ,CoCargo
      ,CoUserJefe
      ,NuAnexo
      ,TxLema
      ,NoArchivoFoto
      ,NuOrdenArea
	  --,CardCodeSAP
	  ,FlTrabajador_Activo
 ,i.Cultura 
 From MAUSUARIOS u Left Join MAIDIOMAS i On u.IdiomaPc = i.IDidioma
 Where Usuario=@Usuario And u.Activo='A'  

