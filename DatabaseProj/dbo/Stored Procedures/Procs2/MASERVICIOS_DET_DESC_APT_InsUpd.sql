﻿
CREATE Procedure [dbo].[MASERVICIOS_DET_DESC_APT_InsUpd]   
    @IDServicio_Det int,
	@IDTemporada int,
	@DescripcionAlterna varchar(200),
	@SingleSupplement numeric(10,4),
	@PoliticaLiberado bit,
	@Liberado tinyint,
	@TipoLib char(1),
	@MaximoLiberado tinyint,
	@MontoL numeric(10,4),
    @UserMod char(4)  
AS
BEGIN
	Set NoCount On
	IF ISNULL(@PoliticaLiberado,0) = 0
	BEGIN
		SELECT @Liberado = NULL, @TipoLib = NULL, @MaximoLiberado = NULL, @MontoL = NULL
	END
	IF NOT EXISTS(SELECT IDServicio_Det, IDTemporada FROM MASERVICIOS_DET_DESC_APT
			   WHERE IDServicio_Det = @IDServicio_Det AND IDTemporada = @IDTemporada)
	BEGIN
		INSERT INTO MASERVICIOS_DET_DESC_APT
			([IDServicio_Det]
			,[IDTemporada]
			,[DescripcionAlterna]
			,[SingleSupplement]
			,[PoliticaLiberado]
			,[Liberado]
			,[TipoLib]
			,[MaximoLiberado]
			,[MontoL]
			,[FecMod]
			,[UserMod]
			)
		VALUES
			(@IDServicio_Det,
			@IDTemporada,
			@DescripcionAlterna,
			@SingleSupplement,
			@PoliticaLiberado,
			@Liberado,
			@TipoLib,
			@MaximoLiberado,
			@MontoL,
			GETDATE(),
			@UserMod
			)
	END
	ELSE
	BEGIN
		UPDATE MASERVICIOS_DET_DESC_APT SET DescripcionAlterna = @DescripcionAlterna,
											SingleSupplement = @SingleSupplement,
											PoliticaLiberado = @PoliticaLiberado,
											Liberado = @Liberado,
											TipoLib = @TipoLib,
											MaximoLiberado = @MaximoLiberado,
											MontoL = @MontoL,
											UserMod = @UserMod,
											FecMod = GETDATE()
		WHERE IDServicio_Det = @IDServicio_Det AND IDTemporada = @IDTemporada
	END
END

