﻿

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JRF-20140401-Se agrego el tipo de vehiculo
--JRF-20150411-Se cambio la tabla MAVEHICULOSPROGRAMACION por MAVEHICULOS_TRANSPORTE
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelProveedores_Prog  
@IDSetours varchar(6),  
@IDCab int  
As  
Set NoCount On  
select distinct odd.IDOperacion_Det , p.NombreCorto + Isnull(' ('+v.NoVehiculoCitado +' - ' + CAST(v.QtCapacidad as varchar(3))+' Pax)','') as NombreCorto
	,p.IDTipoProv,ISNULL(v.NuVehiculo,0) as IDVehiculoProgramado,p.IDProveedor
from OPERACIONES_DET_DETSERVICIOS odd Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det  
Left Join OPERACIONES o On o.IDOperacion = od.IDOperacion Left Join MAPROVEEDORES p On odd.IDProveedor_Prg= p.IDProveedor  
--Left Join MAVEHICULOSPROGRAMACION v On odd.IDVehiculo_Prg = v.NuVehiculoProg  
Left Join MAVEHICULOS_TRANSPORTE v On odd.IDVehiculo_Prg = v.NuVehiculo 
where o.IDCab = @IDCab and o.IDProveedor = @IDSetours and odd.IDProveedor_Prg is not null  

