﻿--JRF-20130319-Nuevo Campo [DiasSinPenalidad]        
--JRF-20130515-Nuevo Campo [Observacion]      
--JRF-20130520-Elminar Campo [Observacion]    
--JRF-20130711-Nuevo Campo [PoliticaFechaReserva]  
--JRF-20150309-Nuevo Campo [Anio]
CREATE Procedure [dbo].[MAPOLITICAPROVEEDORES_SelxIDProveedor]            
 @IDProveedor char(6)            
as            
 Set NoCount On
          
 select IDPolitica,IDProveedor,Nombre,IsNull(Anio,'') as Anio,DefinDia,DefinHasta,DefinTipo,          
 RoomPreeliminar,RoomActualizado,RoomFinal,DiasSinPenalidad,PoliticaFechaReserva,PrePag1_Dias,PrePag1_Porc      
 ,PrePag1_Mont,PrePag2_Dias,PrePag2_Porc,PrePag2_Mont,Saldo_Dias,Saldo_Porc,Saldo_Mont    
 From MAPOLITICAPROVEEDORES            
 Where IDProveedor = @IDProveedor          
