﻿Create Procedure dbo.MASERVICIOS_ATENCION_Upd
@IDAtencion tinyint,
@IDservicio Char(8) ,
@Dia1 char(1) ,
@Dia2 char(1) ,
@Desde1 varchar(22) ,
@Hasta1 varchar(22) ,
@Desde2 varchar(22) ,
@Hasta2 varchar(22) ,
@UserMod Char(4) 
As
	Set NoCount On
	Update MASERVICIOS_ATENCION
		Set Dia1 = @Dia1 ,
			Dia2 = @Dia2 ,
			Desde1 = @Desde1 ,
			Hasta1 = @Hasta1 ,
			Desde2 = @Desde2 ,
			Hasta2 = @Hasta2 ,
			UserMod = @UserMod,
			FecMod = GETDATE()
	Where IDAtencion = @IDAtencion And IDservicio = @IDservicio
