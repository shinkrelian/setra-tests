﻿CREATE PROCEDURE [dbo].[MAUSUARIOSOPC_InsxOpc]
	@IDOpc	char(6),
	@IDUsuario	char(4)
As
	Set NoCount On
	
	Insert Into MAUSUARIOSOPC(IDOpc,IDUsuario,Consultar, Grabar, Actualizar, Eliminar, UserMod)
	Select @IDOPc,IDUsuario,0,0,0,0,@IDUsuario From MAUSUARIOS
