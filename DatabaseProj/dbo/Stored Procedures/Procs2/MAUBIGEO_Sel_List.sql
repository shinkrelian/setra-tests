﻿
--MLL-20140522- Nuevo Campo CoInternacPais
CREATE Procedure [dbo].[MAUBIGEO_Sel_List] 
	@IDUbigeo char(6),
	@Descripcion varchar(50)
As
	Set NoCount On
	
	Select Descripcion, TipoUbig, Codigo, 
	DC, 
	Case When NI='N' Then 'Nacional' Else 'Internacional' End as NI, 
	Llamar, IDUbigeo , CoInternacPais
	From MAUBIGEO u 
	Where (Descripcion Like '%'+ @Descripcion +'%' Or LTRIM(RTRIM(@Descripcion))='')
	and (IDubigeo=@IDUbigeo Or LTRIM(rtrim(@IDUbigeo))='')
	ORDER BY TipoUbig, Descripcion
	
	select*from MAUBIGEO
