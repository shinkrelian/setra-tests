﻿
--HLF-20150630-And FlServicioNoShow=@FlServicioNoShow
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_UpdEstado
	@IDOperacion_Det	int,
	@IDServicio_Det	int,
	@IDEstadoSolicitud	char(2),
	@IDServicio_Det_V_Prg	int,
	@IDCorreoEstadoAC	varchar(max),
	@IDProveedor_Prg	char(6),
	@IDTipoProvProgBloque varchar(10),		
	
	--@Total	numeric(8,2),
	--@ObservEdicion	varchar(250),
	@FlServicioNoShow bit,
	@UserMod	char(4)
As
	Set NoCount On
	
	Update OPERACIONES_DET_DETSERVICIOS Set
	IDEstadoSolicitud=@IDEstadoSolicitud,
	IDServicio_Det_V_Prg=Case When @IDServicio_Det_V_Prg=0 Then Null Else @IDServicio_Det_V_Prg End,
	IDCorreoEstadoAC=Case When ltrim(rtrim(@IDCorreoEstadoAC))='' Then Null Else @IDCorreoEstadoAC End,
	IDProveedor_Prg=Case When ltrim(rtrim(@IDProveedor_Prg))='' Then Null Else @IDProveedor_Prg End,
	Total_Prg=Case When Total_Prg Is Null Then Total Else Total_Prg End,
	UserMod=@UserMod,	
	FecMod=GETDATE()
	From OPERACIONES_DET_DETSERVICIOS od 
	Left Join MASERVICIOS_DET sd On od.IDServicio_Det_V_Cot=sd.IDServicio_Det
	--Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
	--Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
	Left Join MAPROVEEDORES p On od.IDProveedor_Prg=p.IDProveedor
	Where od.IDOperacion_Det=@IDOperacion_Det 
	And (od.IDServicio_Det=@IDServicio_Det Or @IDServicio_Det=0)
	And (CHARINDEX(p.IDTipoProv,@IDTipoProvProgBloque)>0 Or LTRIM(RTRIM(@IDTipoProvProgBloque))='')
	And FlServicioNoShow=@FlServicioNoShow
	


