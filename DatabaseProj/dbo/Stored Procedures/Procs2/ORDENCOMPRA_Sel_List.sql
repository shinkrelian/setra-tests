﻿----JHD-20150310- Se agrego el campo CoTipoDetraccion.
--JHD-20150311- Se cambio el campo CoEstado por CoEstado_OC
--JHD-20150313- Estado= CASE WHEN o.CoEstado_OC='EN' THEN 'ENTREGADO' ELSE isnull((select NoEstado from ESTADO_OBLIGACIONESPAGO where CoEstado=o.CoEstado_OC),'') END,
--JHD-20150408- Se quito el parametro @CoArea y los campos referentes al Area
--JHD-20150408- Se hizo LEFT JOIN a la tabla MARUBRO
--JHD-20150626- round(o.SSDetraccion,0) as SSDetraccion,
--JHD-20151104- order by NuOrdComInt
CREATE PROCEDURE [dbo].[ORDENCOMPRA_Sel_List]
 
 @NuOrdCom char(13)='',                    
 @DescripcionProveedor varchar(200)='',            
 @Descripcion varchar(200)='',            
 @FechaIni smalldatetime='01/01/1900',                    
 @FechaFin smalldatetime='01/01/1900',                 
 @CoEstado_OC char(2)='',                
 --@CoArea tinyint=0,
 @CoMoneda char(3)=''            
 as
BEGIN
SELECT     o.NuOrdComInt,
o.FeOrdCom,
o.NuOrdCom,
o.CoProveedor,
p.NombreCorto,
o.TxDescripcion, 
--o.CoEstado,
o.CoEstado_OC,
--Estado=isnull((select NoEstado from ESTADO_OBLIGACIONESPAGO where CoEstado=o.coestado),''),--case when o.CoEstado='A' THEN 'ACEPTADO' when o.CoEstado='X' THEN 'ANULADO' ELSE '' END,
Estado= CASE WHEN o.CoEstado_OC='EN' THEN 'ENTREGADO' ELSE isnull((select NoEstado from ESTADO_OBLIGACIONESPAGO where CoEstado=o.CoEstado_OC),'') END,
--a.CoArea,
--a.NoArea,
o.CoRubro,
r.NoRubro,
o.SSSubTotal, 
o.SSIGV,
round(o.SSDetraccion,0) as SSDetraccion,
o.SsTotal,
m.IDMoneda,
m.Descripcion as Moneda,
o.CoTipoDetraccion

FROM         ORDENCOMPRA o INNER JOIN
              MAMONEDAS m ON o.CoMoneda = m.IDMoneda
			  left JOIN
              --RH_AREA a ON o.CoArea = a.CoArea INNER JOIN
              MARUBRO r ON o.CoRubro = r.CoRubro INNER JOIN
              MAPROVEEDORES p ON o.CoProveedor = p.IDProveedor
WHERE     
(o.NuOrdCom Like '%'+ltrim(rtrim(@NuOrdCom))+'%' Or ltrim(rtrim(@NuOrdCom))='') and
(p.NombreCorto like '%'+@DescripcionProveedor+'%' or LTRIM(rtrim(@DescripcionProveedor))='') and
(o.TxDescripcion like '%'+@Descripcion+'%' or LTRIM(rtrim(@Descripcion))='') and
(o.CoEstado_OC=@CoEstado_OC Or ltrim(rtrim(@CoEstado_OC))='') AND 
(o.CoMoneda=@CoMoneda Or ltrim(rtrim(@CoMoneda))='') AND 
(o.FeOrdCom Between @FechaIni And @FechaFin Or @FechaIni='01/01/1900') --and
--(a.CoArea=@CoArea Or @CoArea=0)
order by NuOrdComInt
END;

