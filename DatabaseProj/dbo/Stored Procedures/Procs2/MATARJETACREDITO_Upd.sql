﻿Create Procedure [dbo].[MATARJETACREDITO_Upd]
@CodTar char(3),
@Descripcion varchar(50),
@UserMod char(4)
as

	Set NoCount On
	Update MATARJETACREDITO
		set Descripcion=@Descripcion,
			UserMod=@UserMod
	Where CodTar=@CodTar
