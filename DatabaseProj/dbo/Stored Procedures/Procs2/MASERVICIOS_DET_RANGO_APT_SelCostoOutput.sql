﻿

CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_SelCostoOutput]
	@IDServicio_Det int,
	@NroPax	smallint,
	@pMonto	numeric(8,2) output
As
	Set Nocount On
	
	Select @pMonto=Monto From MASERVICIOS_DET_RANGO_APT 
	Where IDServicio_Det=@IDServicio_Det And
	@NroPax Between PaxDesde And PaxHasta
	
	Set @pMonto=ISNULL(@pMonto,0)
	
