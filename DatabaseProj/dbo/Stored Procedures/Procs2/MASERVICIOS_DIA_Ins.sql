﻿
--HLF-20151012-@NoHotel varchar(100),@TxWebHotel varchar(150),
--HLF-20160122-@TxDireccHotel,@TxTelfHotel1,@TxTelfHotel2,@FeHoraChkInHotel,@FeHoraChkOutHotel,@CoTipoDesaHotel,@CoCiudadHotel
CREATE Procedure [dbo].[MASERVICIOS_DIA_Ins]
	@IDServicio char(8),
	@Dia tinyint,
	@IDIdioma varchar(12),
	--@DescripCorta varchar(100),
	@Descripcion text,

	@NoHotel varchar(100),
	@TxWebHotel varchar(150),

	@TxDireccHotel varchar(150),
	@TxTelfHotel1 varchar(50),
	@TxTelfHotel2 varchar(50),
	@FeHoraChkInHotel smalldatetime,
	@FeHoraChkOutHotel smalldatetime,
	@CoTipoDesaHotel char(2), 
	@CoCiudadHotel char(6),

	@UserMod char(4)
As
	Set NoCount On
	
	INSERT INTO MASERVICIOS_DIA
           ([IDServicio]
           ,[Dia]
           ,[IDIdioma]
           --,[DescripCorta]
           ,[Descripcion]
		   ,NoHotel
		   ,TxWebHotel
			,TxDireccHotel
			,TxTelfHotel1 
			,TxTelfHotel2 
			,FeHoraChkInHotel 
			,FeHoraChkOutHotel
			,CoTipoDesaHotel 
			,CoCiudadHotel
           ,[UserMod]
           )
     VALUES
           (@IDServicio
           ,@Dia 
           ,@IDIdioma 
           --,@DescripCorta
           ,Case When ltrim(rtrim(Cast(@Descripcion as varchar(max))))='' Then Null Else @Descripcion End 
		   ,Case When ltrim(rtrim(@NoHotel))='' Then Null Else @NoHotel End 
		   ,Case When ltrim(rtrim(@TxWebHotel))='' Then Null Else @TxWebHotel End 

		   ,Case When ltrim(rtrim(@TxDireccHotel))='' Then Null Else @TxDireccHotel End 
		   ,Case When ltrim(rtrim(@TxTelfHotel1))='' Then Null Else @TxTelfHotel1 End 
		   ,Case When ltrim(rtrim(@TxTelfHotel2))='' Then Null Else @TxTelfHotel2 End 
		   ,Case When @FeHoraChkInHotel='01/01/1900' Then Null Else @FeHoraChkInHotel End 
		   ,Case When @FeHoraChkOutHotel='01/01/1900' Then Null Else @FeHoraChkOutHotel End 
		   ,Case When ltrim(rtrim(@CoTipoDesaHotel))='' Then Null Else @CoTipoDesaHotel End 
		   ,Case When ltrim(rtrim(@CoCiudadHotel))='' Then Null Else @CoCiudadHotel End 

           ,@UserMod 
           )
		   
