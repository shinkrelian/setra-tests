﻿
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_InsServTrasladistaxIDProveedor
	@IDCab int,     
	@IDProveedor char(6),
	@IDServicioTrasladista char(8),
	@UserMod char(4)    
As    
	Set Nocount On         
 
	Declare @IDOperacion int, @Anio char(4)
	
	Select @IDOperacion=IDOperacion, @Anio=year(Fecha)
	From OPERACIONES Where IDCab=@IDCab And IDProveedor=@IDProveedor

	Declare @IDServicio_DetTrasladista int=
		(Select IDServicio_Det 
		From MASERVICIOS_DET Where IDServicio=@IDServicioTrasladista And Anio=@Anio)

	Insert Into OPERACIONES_DET_DETSERVICIOS    
	(IDOperacion_Det,IDServicio_Det,IDServicio_Det_V_Cot,IDServicio,  	
	CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,    
	CostoRealImpto,CostoLiberadoImpto, MargenImpto, MargenLiberadoImpto,   	    
	TotImpto,  TipoCambio,	IDMoneda,  	
	NetoCotizado, IgvCotizado, TotalCotizado,
	UserMod)      


	Select IDOperacion_Det, @IDServicio_DetTrasladista,@IDServicio_DetTrasladista,od.IDServicio,
	Monto_sgl*od.NroPax, 0, 0, 0, 0, Monto_sgl*od.NroPax, 
	0,0,0,0, 
	0, sd.TC, Case When sd.TC IS NULL Then 'USD' Else 'SOL' End, 
	Monto_sgl*od.NroPax, 0, Monto_sgl*od.NroPax,
	@UserMod
	From OPERACIONES_DET od 
	Left Join MASERVICIOS_DET sd On sd.IDServicio_Det=@IDServicio_DetTrasladista
	Where od.IDOperacion = @IDOperacion And od.Total>0

