﻿CREATE Procedure [dbo].[NuPedido_SelOutput]      
	@FecPedido smalldatetime,    
	@pNuPedido char(9) Output      
As      
	Set NoCount On      
    
	Declare @FecInicio varchar(4)=(Select SubString((convert(varchar,@FecPedido,112)),3,4)) 
    
	Declare @iNuPedido int=IsNull((Select MAX(right(NuPedido,8)) From PEDIDO
	Where Left(NuPedido,5)='P'+@FecInicio), CAST(@FecInicio+'0000' AS INT))+1      
      
	Declare @vcNuPedido nvarchar(8)=Cast(@iNuPedido as nvarchar(8))    
	If Cast(Right(@vcNuPedido,4) as int)<100     
	Set @pNuPedido = 'P' + left(@vcNuPedido,4) + '0100'     
	Else    
	Set @pNuPedido = 'P' + Cast(@iNuPedido as nvarchar(8))       
       
