﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_Cbo_DetServicioAPT]
					@IDServicio_Det int
AS
BEGIN
	Set NoCount On  
	SELECT T.IDTemporada, T.NombreTemporada + CASE WHEN ISNULL(A.NR, 0) = 0 THEN '' ELSE ' [ X ]' END AS NombreTemporada
	FROM MATEMPORADA AS T LEFT OUTER JOIN (SELECT D.IDTemporada, COUNT(R.Correlativo) AS NR
											FROM MASERVICIOS_DET_DESC_APT AS D INNER JOIN
                                                 MASERVICIOS_DET_RANGO_APT AS R ON D.IDTemporada = R.IDTemporada AND D.IDServicio_Det = R.IDServicio_Det
											WHERE D.IDServicio_Det = @IDServicio_Det
											GROUP BY D.IDTemporada) AS A ON T.IDTemporada = A.IDTemporada
END
