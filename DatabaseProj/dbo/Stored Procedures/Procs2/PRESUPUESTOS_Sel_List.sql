﻿--JRF-20120912-Nuevos Filtros(x Rango de Fecha de Pago y Fecha de Generacion)
CREATE Procedure dbo.PRESUPUESTOS_Sel_List    
 @IDFile varchar(8),    
 @IDEstado char(2),    
 @FechaGen1 smalldatetime,    
 @FechaGen2 smalldatetime,
 @FechaPago1 smalldatetime,
 @FechaPago2 smalldatetime,
 @Guia varchar(80),    
 @Titulo varchar(100),  
 @IDUsuarioPre char(4),  
 @SoloConPresupuesto bit  
As    
 Set NoCount On    
   
 Select uOp.Nombre as DescUsuarioOper,C.IDFile,C.Titulo,  
 P.IDPresupuesto,C.IDCab,U.Nombre as DescUsuarioPres,  
 Convert(varchar(10),P.Fecha ,103) As Fecha, 
 CONVERT(varchar(10),P.FechaPago,103) As FechaPago,
 Pr.NombreCorto as DescGuia,   
 P.IDEstado,  
 Case P.IDEstado When 'OP' Then 'EN OPERACIONES'  
  When 'AD' Then 'EN ADMINISTRACIÓN'  
 End As DescEstado  
 From COTICAB C Left Join PRESUPUESTOS P On C.IDCAB = P.IDCab    
 Left Join MAPROVEEDORES Pr On P.IDGuia = Pr.IDProveedor    
 Left Join MAUSUARIOS U On U.IDUsuario = P.IDUsuarioPre    
 Left Join MAUSUARIOS uOp On C.IDUsuarioOpe=uOp.IDUsuario  
 Where c.IDUsuarioOpe<>'0000'  
 And (ltrim(rtrim(@IDFile)) = '' Or c.IDFile like '%'+ @IDFile +'%')    
 And (ltrim(rtrim(@Titulo)) = '' Or C.Titulo like '%'+ @Titulo +'%')    
 And (LTRIM(rtrim(@IDEstado))='' Or P.IDEstado = @IDEstado)    
 And (LTRIM(rtrim(@Guia)) = '' Or Pr.NombreCorto like '%' + @Guia + '%')    
 --And (Convert(varchar(10),P.Fecha,103)=@Fecha Or @Fecha = '01/01/1900')    
 And ((P.Fecha Between @FechaGen1 And @FechaGen2) Or (@FechaGen1='01/01/1900' And @FechaGen2='01/01/1900'))
 And ((P.FechaPago Between @FechaPago1 And @FechaPago2) Or (@FechaPago1='01/01/1900' And @FechaPago2='01/01/1900'))
 And (ltrim(rtrim(@IDUsuarioPre)) = '' Or P.IDUsuarioPre=@IDUsuarioPre)  
 And (@SoloConPresupuesto=0 Or Not P.IDPresupuesto is null)  
 Order by c.Fecha, P.Fecha  

