﻿CREATE Procedure [dbo].[MASERVICIOS_ALIMENTACION_DIA_Sel_xIDServicio]
	@IDServicio char(8)
As
	Set NoCount On
	
	Select Dia, Desayuno, Lonche, Almuerzo, Cena, IDUbigeoOri, IDUbigeoDes
	From MASERVICIOS_ALIMENTACION_DIA
	Where IDServicio=@IDServicio
