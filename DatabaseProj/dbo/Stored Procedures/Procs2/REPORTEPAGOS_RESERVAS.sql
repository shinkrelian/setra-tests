﻿CREATE PROCEDURE REPORTEPAGOS_RESERVAS
@IDUsuarioResp char(4),                                                    
@IDUsuarioResv char(4),                                                    
@IDUsuarioOper char(4),                                                      
--@IDCliente varchar(6),                                                      
@NroFile varchar(8),                                                      
@Estado char(1),                                                      
@FechaGenInicial smalldatetime,                                                      
@FechaGenFinal smalldatetime,                                                      
@FechaIngInicial smalldatetime,                                                      
@FechaIngFinal smalldatetime,                                                      
@FechaOutInicial smalldatetime,                                                      
@FechaOutFinal smalldatetime,                       
@FechaAsgReservaInicial smalldatetime,                    
@FechaAsgReservaFinal smalldatetime,      
@FechaGenReservasInicial smalldatetime,      
@FechaGenReservasFinal smalldatetime--,      
--@NoExisteInfoOperacion bit=0,                                      
--@Orderx char(1)=''                                   
AS
BEGIN
select distinct
c.idfile,
op.IDOrdPag,
c.FecInicio,
 c.FechaOut,
Fecha_Aceptado= isnull(	case when (c.idfile is not null or ltrim(rtrim(c.idfile))='') then
			(select TOP 1 fecmod from COTICAB_HISTORIAL_UPD where idcab=c.IDCAB AND nocampo='Estado' and txvalorantes='P' AND txvalordespues='A' ORDER BY FECMOD)
			else 0 end,c.FechaReserva),
--r.IDProveedor,
p.NombreCorto,
Moneda=(select Simbolo from MAMONEDAS where IDMoneda=op.idmoneda),
op.SsMontoTotal,
Total_Dolares=ISNULL(dbo.FnCambioMoneda(op.SsMontoTotal,op.idmoneda,'USD',OP.TCambio),0),
EsPrepago= case when p.IDFormaPago='002' THEN 'Si' else 'No' end,
op.FechaEmision,
op.FechaPago
from ORDENPAGO op
left join RESERVAS r on r.IDReserva=op.IDReserva
left join coticab c on c.idcab=op.IDCab
left join MAPROVEEDORES p on r.IDProveedor=p.IDProveedor

   Where (ltrim(rtrim(@IDUsuarioResp))='' Or c.IDUsuario = @IDUsuarioResp)                                                      
		 --  And (ltrim(rtrim(@IDCliente))='' Or cl.IDCliente = @IDCliente)                         
		   --And db.IDEstado <> 'AN'                      
		   --And ISNULL(db.IDEstado,'') <> 'AN'                                                   
		   And (ltrim(rtrim(@NroFile))='' Or c.IDFile like '%'+@NroFile+'%')                                                      
                         
		  And (ltrim(rtrim(@Estado))= '' Or c.Estado = @Estado)                             
                              
		   --And (ltrim(rtrim(@Estado))= 'P' Or isnull(c.IDFile,'') <> '')                                  
                         
		   And (ltrim(rtrim(@IDUsuarioResv))='' Or c.IDUsuarioRes = @IDUsuarioResv)                                                    
		   And (ltrim(rtrim(@IDUsuarioOper))='' Or c.IDUsuarioOpe = @IDUsuarioOper)                                                    
		   And ((Convert(char(10),@FechaGenInicial,103)='01/01/1900' and Convert(char(10),@FechaGenFinal,103)='01/01/1900')                                                       
			Or Cast(Convert(Char(10),c.Fecha,103) As Smalldatetime)              
			between @FechaGenInicial and @FechaGenFinal)                                                      
		   And ( (Convert(char(10),@FechaOutInicial,103)='01/01/1900' and Convert(char(10),@FechaOutFinal,103)='01/01/1900')                                                      
			Or Cast(Convert(char(10),c.FechaOut,103) as smalldatetime)             
			between @FechaOutInicial and @FechaOutFinal)                                                      
		   And ((Convert(char(10),@FechaIngInicial,103)='01/01/1900' and Convert(char(10),@FechaIngFinal,103)='01/01/1900')                                                      
			Or Cast(Convert(char(10),FecInicio,103) as smalldatetime)               
			between @FechaIngInicial and @FechaIngFinal)                    
		   And ((Convert(char(10),@FechaAsgReservaInicial,103)='01/01/1900' and Convert(char(10),@FechaAsgReservaFinal,103)='01/01/1900')                    
			Or cast(convert(char(10),FechaAsigReserva,103) As smalldatetime)               
		   between @FechaAsgReservaInicial and @FechaAsgReservaFinal)      
		   And ((Convert(char(10),@FechaGenReservasInicial,103)='01/01/1900'and CONVERT(char(10),@FechaGenReservasFinal,103)='01/01/1900')       
			Or Cast(Convert(Char(10),FechaReserva,103) As Smalldatetime)       
		   between @FechaGenReservasInicial and @FechaGenReservasFinal)    

order by c.IDFile
END
