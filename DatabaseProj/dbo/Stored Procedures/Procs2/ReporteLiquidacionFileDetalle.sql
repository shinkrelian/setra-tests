﻿--HLF-20160414-Case When z.IGV_SFE>0 .... Case When z.IGVCosto>0 ...
CREATE Procedure dbo.ReporteLiquidacionFileDetalle
	@IDCab int
As
	Set Nocount on

	--Create Table #COSTOS_PRELIQUIDACION(
	--	IDProveedor char(6) collate Modern_Spanish_CI_AS, DescProveedor varchar(100), DescProveedorInternac varchar(100), DescPaisProveedor varchar(60),
	--	NuObligPago varchar(15) collate Modern_Spanish_CI_AS, CompraNeto numeric(13,4), IGVCosto numeric(10,4), IGV_SFE numeric(10,4), Moneda char(3) collate Modern_Spanish_CI_AS,
	--	CostoTotalPreLiq numeric(13,4), IDTipoOC char(3), DescOperacionContab varchar(100), Margen numeric(5,2), 
	--	CostoBaseUSD numeric(13,4), VentaUSD numeric(13,4), FacturaExportacion numeric(13,4),
	--	BoletaNoExportacion numeric(13,4),BoletaServicioExterior numeric(13,4), CoObligPago char(2), RutayCodReserva varchar(100), Servicio varchar(100)
	--)
	Delete From REP_COSTOS_PRELIQUIDACION_DET
	
	Insert Into REP_COSTOS_PRELIQUIDACION_DET(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		NuObligPago, CompraNeto, IGVCosto, IGV_SFE, Moneda,CostoTotalPreLiq, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelVouchersPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION_DET Set CoObligPago='VO' Where CoObligPago Is Null
	
	Insert Into REP_COSTOS_PRELIQUIDACION_DET(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		NuObligPago, CompraNeto, IGVCosto, IGV_SFE, Moneda,CostoTotalPreLiq, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelOrdenesServicioPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION_DET Set CoObligPago='OS' Where CoObligPago Is Null
	
	Insert Into REP_COSTOS_PRELIQUIDACION_DET(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		CompraNeto, IGVCosto, IGV_SFE, Moneda,CostoTotalPreLiq, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelEntradasPresupuestosPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION_DET Set CoObligPago='EP' Where CoObligPago Is Null

	Insert Into REP_COSTOS_PRELIQUIDACION_DET(IDProveedor, DescProveedor, DescProveedorInternac, 
		RutayCodReserva, DescPaisProveedor, CompraNeto, IGVCosto, IGV_SFE, CostoTotalPreLiq, Moneda, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, Servicio, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec COTIVUELOS_SelBusesVuelosPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION_DET Set CoObligPago='BV' Where CoObligPago Is Null

	Insert Into REP_COSTOS_PRELIQUIDACION_DET(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		NuObligPago, CompraNeto, IGVCosto, IGV_SFE, Moneda,CostoTotalPreLiq, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelTraladistasyMovilidadPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION_DET Set CoObligPago='IN' Where CoObligPago Is Null

	
	Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)/100
	
	SELECT Z1.*,
		case when CostoTotalCotiUSD=0 then 0 else (MargenCoti/CostoTotalCotiUSD)*100 end AS PorcMargenCotiUSD,
		case when CostoTotalPreLiqUSD=0 then 0 else (MargenPreLiq/CostoTotalPreLiqUSD)*100 end AS PorcMargenPreLiqUSD,
		case when CostoTotalSustentadoUSD=0 then 0 else (MargenSust/CostoTotalSustentadoUSD)*100 end AS PorcMargenSustentadoUSD
	FROM
	(
	SELECT Y1.*, 
		round(CostoBaseCotiUSD*(Margen/100),0) as MargenCoti,
		round(CostoBasePreLiqUSD*(Margen/100),0) as MargenPreLiq,
		round(CostoBaseSustentadoUSD*(Margen/100),0) as MargenSust,
		CostoBaseCotiUSD*(1+(Margen/100)) as VentaCoti,
		CostoBasePreLiqUSD*(1+(Margen/100)) as VentaPreLiq,
		CostoBaseSustentadoUSD*(1+(Margen/100)) as VentaSust		
	FROM
		(
		SELECT X1.*, 
			CostoCotiSOL+IGV_SFECotiSOL+IGVCostoCoti As CostoTotalCotiSOL,
			CostoPreLiqSOL+IGV_SFEPreLiqSOL+IGVCostoPreLiq As CostoTotalPreLiqSOL,
			CostoSustentadoSOL+IGV_SFESustentadoSOL+IGVCostoSusten As CostoTotalSustentadoSOL,
			Case When TCCoti=0 then 0 else round((CostoCotiSOL+IGV_SFECotiSOL+IGVCostoCoti)/TCCoti,0) end As CostoTotalCotiUSD,
			Case When TCFact=0 then 0 else round((CostoPreLiqSOL+IGV_SFEPreLiqSOL+IGVCostoPreLiq)/TCFact,0) end As CostoTotalPreLiqUSD,
			Case When TCSust=0 then 0 else round((CostoSustentadoSOL+IGV_SFESustentadoSOL+IGVCostoSusten)/TCSust,0) end As CostoTotalSustentadoUSD,
			Case When TCCoti=0 then 0 else (CostoCotiSOL+IGVCostoCoti)/TCCoti end As CostoBaseCotiUSD,
			Case When TCFact=0 then 0 else (CostoPreLiqSOL+IGVCostoPreLiq)/TCFact end As CostoBasePreLiqUSD,
			Case When TCSust=0 then 0 else (CostoSustentadoSOL+IGVCostoSusten)/TCSust end As CostoBaseSustentadoUSD

		FROM
			(
			SELECT Z.*,
				--CostoCotiSOL*@IGV As IGV_SFECotiSOL,CostoPreLiqSOL*@IGV As IGV_SFEPreLiqSOL,CostoSustentadoSOL*@IGV As IGV_SFESustentadoSOL
				Case When z.IGV_SFE>0 then CostoCotiSOL*@IGV else 0 end As IGV_SFECotiSOL,
				Case When z.IGV_SFE>0 then CostoPreLiqSOL*@IGV else 0 end As IGV_SFEPreLiqSOL,
				Case When z.IGV_SFE>0 then CostoSustentadoSOL*@IGV else 0 end As IGV_SFESustentadoSOL,
				
				Case When z.IGVCosto>0 then CostoCoti*@IGV else 0 end As IGVCostoCoti,
				Case When z.IGVCosto>0 then CostoTotalPreLiq*@IGV else 0 end As IGVCostoPreLiq,
				Case When z.IGVCosto>0 then CostoSustentado*@IGV else 0 end As IGVCostoSusten
			FROM 
				(
				SELECT Y.*,
				dbo.FnCambioMoneda(Y.CostoCoti,'USD','SOL', Y.TCCoti) as CostoCotiSOL,
				dbo.FnCambioMoneda(Y.CostoTotalPreLiq,Y.Moneda,'SOL', Y.TCFact) as CostoPreLiqSOL,
				dbo.FnCambioMoneda(Y.CostoSustentado,'USD','SOL', Y.TCSust) as CostoSustentadoSOL
				--cast (0.00 as numeric(5,2)) as IGVCostoCoti, cast (0.00 as numeric(5,2)) as IGVCostoPreLiq, cast (0.00 as numeric(5,2)) as IGVCostoSusten
				FROM
					(
					SELECT X.NuObligPago,x.IDProveedor,x.DescProveedor,X.DescPaisProveedor,X.NoCiudadOficFact,X.TCCoti, X.TCFact, X.TCSust, X.CompraNeto, X.Moneda, X.DescOperacionContab, X.Margen,
						X.CostoCoti, X.CostoTotalPreLiq, X.IGV_SFE, X.IGVCosto,	--X.CostoSustentado
						isnull((Select sum(dbo.FnCambioMoneda(d.SSTotal,d.CoMoneda,'USD', dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeEmision))) From DOCUMENTO_PROVEEDOR d Where d.IDCab=@IDCab And d.CoProveedor=X.IDProveedor
							and (
							(cast(d.NuVoucher as varchar(15))=X.NuObligPago and X.CoObligPago='VO') or 
							(cast(d.NuOrden_Servicio as varchar(14))=replace(X.NuObligPago,'-','') and X.CoObligPago='OS')
							) 
							and d.FlActivo=1
						),0) as CostoSustentado
					FROM
						(
						SELECT pl.*, ub.Descripcion as NoCiudadOficFact,
						IsNull((Select sum(rd.TotalGen) From RESERVAS_DET rd Inner Join RESERVAS r On r.IDReserva=rd.IDReserva 
						Where r.IDProveedor=pl.IDProveedor And r.IDCab=@IDCab and r.Anulado=0 and rd.Anulado=0
						and 
							(
								(rd.IDReserva_Det in (Select IDReserva_Det From OPERACIONES_DET od1 inner join OPERACIONES o1 on od1.IDOperacion=o1.IDOperacion
									Where o1.IDCab=@IDCab And od1.IDVoucher=pl.NuObligPago) and pl.CoObligPago='VO')
									Or
									(rd.IDReserva_Det in (Select IDReserva_Det From OPERACIONES_DET od1 inner join OPERACIONES o1 on od1.IDOperacion=o1.IDOperacion
									Where o1.IDCab=@IDCab And od1.IDOperacion_Det in 
									(Select IDOperacion_Det From ORDEN_SERVICIO_DET osd1 Inner Join ORDEN_SERVICIO os1 On osd1.NuOrden_Servicio=os1.NuOrden_Servicio And
										os1.IDCab=@IDCab And os1.IDProveedor=pl.IDProveedor) 
									) and pl.CoObligPago='OS')
							) 
						),0)
						as CostoCoti,
						--isnull((Select sum(d.SsTotal) From DOCUMENTO_PROVEEDOR d Where d.IDCab=@IDCab And d.CoProveedor=pl.IDProveedor
						--	and (
						--	(cast(d.NuVoucher as varchar(15))=pl.NuObligPago and pl.CoObligPago='VO') or 
						--	(cast(d.NuOrden_Servicio as varchar(14))=replace(pl.NuObligPago,'-','') and pl.CoObligPago='OS')
						--	) 
						--	and d.FlActivo=1
						--),0) as CostoSustentado,

						isnull((Select tc.SsTipCam From COTICAB_TIPOSCAMBIO tc Where tc.IDCAB=@IDCab and 
						tc.CoMoneda=case when pl.Moneda='USD' then 'SOL' else pl.Moneda end),0) as TCCoti,
						isnull((Select top 1 dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeDocum) from DOCUMENTO d Inner Join DOCUMENTO_DET dd On d.NuDocum=dd.NuDocum and d.IDTipoDoc=dd.IDTipoDoc
						Where d.IDCab=@IDCab and (dd.IDVoucher=pl.NuObligPago or dd.NuOrdenServicio=replace(pl.NuObligPago,'-','')) 
						and d.CoEstado<>'AN' order by d.FeDocum desc),0) as TCFact,
						isnull((Select top 1 dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeEmision) from DOCUMENTO_PROVEEDOR d Where d.IDCab=@IDCab 
						and (cast(d.NuVoucher as varchar(15))=pl.NuObligPago or cast(d.NuOrden_Servicio as varchar(14))=replace(pl.NuObligPago,'-','')) 
						and d.FlActivo=1 order by FeEmision desc),0) as TCSust
						FROM REP_COSTOS_PRELIQUIDACION_DET pl Inner Join MAPROVEEDORES pv On pl.IDProveedor=pv.IDProveedor
							And pv.IDTipoOper='E'
							Left Join MAUBIGEO ub On pv.CoUbigeo_Oficina=ub.IDubigeo
						) AS X
					) AS Y
				) AS Z
			) AS X1
		) AS Y1
	) AS Z1	

