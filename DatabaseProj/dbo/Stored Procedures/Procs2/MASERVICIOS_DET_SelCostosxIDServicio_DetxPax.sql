﻿Create Procedure dbo.MASERVICIOS_DET_SelCostosxIDServicio_DetxPax
@IDServicio_Det int,
@NroPax smallint,
@pUnitPrice numeric(12,4) output
As
Set NoCount On
Set @pUnitPrice = 0
Set @pUnitPrice = dbo.FnDevuelveCostoRealxPaxDetServicio(@IDServicio_Det,@NroPax)
