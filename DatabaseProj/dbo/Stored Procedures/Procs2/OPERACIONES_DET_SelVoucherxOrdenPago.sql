﻿
Create Procedure dbo.OPERACIONES_DET_SelVoucherxOrdenPago
	@IDCab int,
	@IDOrdPag int
As
	Set Nocount On
	
	SELECT Distinct IDVoucher
	FROM OPERACIONES_DET WHERE IDOperacion IN (SELECT IDOperacion FROM OPERACIONES 
		WHERE IDCab=@IDCab)
	AND IDServicio_Det In 
	(SELECT IDServicio_Det
	FROM ORDENPAGO_DET WHERE IDOrdPag IN (SELECT IDOrdPag FROM ORDENPAGO 
		WHERE IDCab=@IDCab)
	AND IDOrdPag=@IDOrdPag)

