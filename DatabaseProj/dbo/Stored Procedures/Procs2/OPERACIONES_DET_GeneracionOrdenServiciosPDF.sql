﻿---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
--JRF-20130801-Agregando el filtro de Estado                
--JRF-20130809-Correción del PDF.(Muestra montos incorrectos.            
--JRF-20130821-Agregar campo Obs. Biblia         
--JRF-20130826-Agregar los vehículos a la descripción.         
--JRF-20130826-Agregar Guía a los Transportistas.        
--HLF-20130904-Agregando Distinct a subquery de COTIVUELOS      
--JRF-20131106-Se comento la descripcion de ruta para los Transfer.   
--      Puesto que ya la contiene la descripcion del servicio.    
--HLF-20140320-And od.Activo=1
--JHD-20150411-Se cambio la tabla MAVEHICULOSPROGRAMACION por MAVEHICULOS_TRANSPORTE
CREATE Procedure dbo.OPERACIONES_DET_GeneracionOrdenServiciosPDF                                  
 @IDServicio varchar(8),                                  
 @IDOperacion_Det int,                
 @IDProveedor_Prg char(6)                
As                                    
 Set NoCount On                   
 Declare @TipoCambio numeric(8,2)                      
 Select @TipoCambio=TipoCambio From COTICAB Where                       
 IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                      
 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)                       
                        
SELECT FechaDetalle,substring(DescripTipoProv,0,LEN(DescripTipoProv)) as DescripTipoProv,            
 Ciudad,HoraDetalle,IDFile,Cotizacion,DescCliente,Titulo,        
 Servicio, FechaIN,FechaOUT,NroPax_Cot,ResponsableVentas                  
,Siglas,              
Isnull(PNR,'') As PNR,               
FormatoCadenaIDOperacion_Det,                  
 DescProveedor_Cot,DescServicio_Cot, IDEstadoSolicitud, Variante_Prg, DescProveedor_Prg, Anio_Prg, IDTipoProv,                  
 TotalCotizado, TipoCambio, Total_PrgEditado,                      
 TotalProgramado, ObservEdicion, TotImpto,NroPax,Liberados,                       
 SimboloMoneda, NetoProgram,             
             
 case when IgvCotizado is null then            
 Cast(Case When Afecto=1 Then NetoProgram*(@NuIgv/100) Else 0 End as numeric(8,2))            
 else IgvProgram End AS IgvProgram,            
             
 Case when TotalCotizadoCal is null then            
 Cast(Case When Afecto=1 Then NetoProgram+(NetoProgram*(@NuIgv/100)) Else NetoProgram End as numeric(8,2))             
 else            
 TotalProgram            
 End AS TotalProgram  ,ObservacionOpe, IDProveedorProgramado  ,ObservacionBiblia    ,IDTipoServ          
 ,GuiaProg,TrasProg        
         
 FROM                      
 (                               
 Select CONVERT(varchar(10),op.Dia,103) as FechaDetalle,ub2.DC as Ciudad,CONVERT(varchar(5),op.Dia,108) as HoraDetalle,c.IDFile,c.Cotizacion,cl.RazonComercial as DescCliente,c.Titulo,        
  op.Servicio            
 ,(select Convert(varchar(10),MIN(Dia),103) from OPERACIONES_DET od2 where od2.IDOperacion=op.IDOperacion) As FechaIN                  
 ,(select Convert(varchar(10),MAX(Dia),103) from OPERACIONES_DET od2 where od2.IDOperacion=op.IDOperacion) As FechaOUT                  
 ,c.NroPax as NroPax_Cot,us.Usuario as ResponsableVentas,idio.Siglas,                  
 REPLICATE('0',4-len(op.IDOperacion_Det)) + CAST(op.IDOperacion_Det as varchar(5)) as FormatoCadenaIDOperacion_Det,                  
       od.IgvCotizado,   od.TotalCotizado as TotalCotizadoCal,            
 dbo.FnPNRxIDCabxDia(o.IDCab,op.Dia)            
 as PNR    ,              
       dCot.Afecto,             
 isnull(pCot.NombreCorto,p1.NombreCorto) as DescProveedor_Cot,                                  
 isnull(sCot.Descripcion,d1.Descripcion) as DescServicio_Cot,                                             
 od.IDEstadoSolicitud,d2.Tipo as Variante_Prg,        
 isnull(pPrg.NombreCorto,'')+        
 case when vh.NoVehiculoCitado is Not null then        
   ' ('+ ISNULL(vh.NoVehiculoCitado,'') + ' - ' +CAST(ISNULL(vh.QtCapacidad,'') AS VARCHAR(5)) +        
 case when vh.NoVehiculoCitado is null then '' else ' Pax' End +')'         
 else '' End         
 as DescProveedor_Prg,         
                             
 d2.Anio as Anio_Prg,isnull(pCot.IDTipoProv,'') as IDTipoProv,od.Total As TotalCotizado,                                            
 isnull(od.tipoCambio ,0) As TipoCambio,od.Total_PrgEditado ,IsNull(od.Total_Prg,0.00) as TotalProgramado,                                  
 IsNull(od.ObservEdicion,'') as ObservEdicion,od.TotImpto,op.NroPax,isnull(op.NroLiberados,0) as Liberados,                                        
 isnull(mon.Simbolo,'') as SimboloMoneda,            
             
 Case When NetoCotizado is null then                    
 dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                     
 Case When isnull(dCot.TC,0) = 0 Then 'USD' Else 'SOL' End,od.IDMoneda,@TipoCambio)             
 else od.NetoProgram End             
 as NetoProgram,            
                   
 Isnull(od.IgvProgram,0) as IgvProgram,                        
 Isnull(od.TotalProgram,0) as TotalProgram ,                
 Isnull(op.ObservInterno,'') as ObservacionOpe, pPrg.IDProveedor as IDProveedorProgramado ,tp.Descripcion as DescripTipoProv     ,          
 Isnull(op.ObservBiblia,'') as ObservacionBiblia,          
 s3.IDTipoServ ,        
         
 Isnull((select distinct prGuia.ApPaterno +', ' +prGuia.Nombre from OPERACIONES_DET_DETSERVICIOS od2 Left Join MAPROVEEDORES prGuia On od2.IDProveedor_Prg = prGuia.IDProveedor        
  where (LTRIM(rtrim(@IDProveedor_Prg))='' Or od2.IDProveedor_Prg = @IDProveedor_Prg) and  od2.IDOperacion_Det = OD.IDOperacion_Det and prGuia.IDTipoProv = '005'),'O.T.') as GuiaProg,        
 Isnull((select distinct NombreCorto from OPERACIONES_DET_DETSERVICIOS od2 Left Join MAPROVEEDORES prTras On od2.IDProveedor_Prg = prTras.IDProveedor        
 where (LTRIM(rtrim(@IDProveedor_Prg))='' Or od2.IDProveedor_Prg = @IDProveedor_Prg) and od2.IDOperacion_Det = od.IDOperacion_Det and prTras.IDTipoProv = '004'),'O.T.') As TrasProg        
         
           
 From OPERACIONES_DET_DETSERVICIOS od Left Join MASERVICIOS_DET d                                     
 On d.IDServicio_Det = od.IDServicio_Det                   
 Left Join MASERVICIOS_DET d2 On ISNULL(od.IDServicio_Det_V_Prg,0)=d2.IDServicio_Det                                    
 Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio Left Join MAPROVEEDORES pPrg On od.IDProveedor_Prg=pPrg.IDProveedor                                    
 Left Join MASERVICIOS_DET dCot On ISNULL(od.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det Left Join MASERVICIOS sCot  On sCot.IDServicio=dCot.IDServicio                                    
 Left Join MAPROVEEDORES pCot On sCot.IDProveedor=pCot.IDProveedor Left Join MASERVICIOS_DET d1 On d1.IDServicio_Det=od.IDServicio_Det                                    
 Left Join MASERVICIOS s1  On d1.IDServicio=s1.IDServicio Left Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor                                
 Left Join OPERACIONES_DET op On od.IDOperacion_Det=op.IDOperacion_Det                                
 Left Join RESERVAS_DET rd On op.IDReserva_Det=rd.IDReserva_Det Left Join MAMONEDAS mon on od.IDMoneda=mon.IDMoneda                   
 Left Join OPERACIONES o On op.IDOperacion = O.IDOperacion Left Join COTICAB c on o.IDCab = c.IDCab                  
 Left Join MACLIENTES cl on c.IDCliente=cl.IDCliente Left Join MAUSUARIOS us on c.IDUsuario=us.IDUsuario                  
 Left Join MAUBIGEO ub2 On op.IDubigeo = ub2.IDubigeo Left Join MAIDIOMAS idio On op.IDIdioma = idio.IDidioma                  
 Left Join MATIPOPROVEEDOR tp On pPrg.IDTipoProv = tp.IDTipoProv            
 Left Join MASERVICIOS s3 On d.IDServicio = s3.IDServicio          
 --Left Join MAVEHICULOSPROGRAMACION vh On op.IDVehiculo_Prg = vh.NuVehiculoProg        
 Left Join MAVEHICULOS_TRANSPORTE vh On op.IDVehiculo_Prg = vh.NuVehiculo        
 Where od.IDServicio =@IDServicio AND od.IDOperacion_Det =@IDOperacion_Det                
 And od.Activo=1
 ) AS X                  
 WHERE (LTRIM(RTRIM(@IDProveedor_Prg))='' OR  X.IDProveedorProgramado = @IDProveedor_Prg) 

