﻿
CREATE Procedure [dbo].[MASERVICIOS_RANGO_APT_Ins]
    @IDServicio char(8),  
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(10,4),  
    @UserMod char(4),
	@IDTemporada int
AS
BEGIN
Set NoCount On
IF NOT EXISTS(SELECT IDServicio, IDTemporada FROM MASERVICIOS_DESC_APT WHERE IDServicio=@IDServicio AND IDTemporada = @IDTemporada)
BEGIN
	INSERT INTO MASERVICIOS_DESC_APT(IDServicio, IDTemporada, DescripcionAlterna, UserMod, FecMod)
	VALUES(@IDServicio, @IDTemporada, '', @UserMod, GETDATE())
END
Declare @Correlativo tinyint=(Select IsNull(MAX(Correlativo),0)+1   
From MASERVICIOS_RANGO_APT Where IDServicio = @IDServicio AND IDTemporada = @IDTemporada)
   
INSERT INTO MASERVICIOS_RANGO_APT
        ([Correlativo]  
        ,[IDServicio]  
        ,[PaxDesde]  
        ,[PaxHasta]  
        ,[Monto]  
        ,[UserMod]
		,[IDTemporada]
        )  
    VALUES  
        (@Correlativo,  
        @IDServicio,  
        @PaxDesde,  
        @PaxHasta,  
        @Monto,  
        @UserMod,
		@IDTemporada
        )  
END
