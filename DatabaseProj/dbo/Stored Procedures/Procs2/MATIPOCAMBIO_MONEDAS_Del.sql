﻿
CREATE PROCEDURE [dbo].[MATIPOCAMBIO_MONEDAS_Del]
	@Fecha		smalldatetime,
	@CoMoneda	char(3)
AS
BEGIN
	Set NoCount On

	Delete From MATIPOCAMBIO_MONEDAS
	Where CoMoneda = @CoMoneda AND Fecha=@Fecha

	IF @CoMoneda = 'SOL'
	BEGIN
		IF EXISTS(SELECT Fecha FROM MATIPOCAMBIO WHERE Fecha = @Fecha)
		BEGIN
			DELETE FROM MATIPOCAMBIO WHERE Fecha = @Fecha
		END
	END
END
