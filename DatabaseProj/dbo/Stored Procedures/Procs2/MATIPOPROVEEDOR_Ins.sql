﻿
CREATE Procedure dbo.MATIPOPROVEEDOR_Ins
	@Descripcion varchar(30),
	@PideIdioma	bit,
	@Comision	numeric(6,2),
	@UserMod	char(4),
	@pIDTipoProv char(3)	output
	
As
	Set NoCount On
	Declare @IDTipoProv char(3)	
	Exec Correlativo_SelOutput 'MATIPOPROVEEDOR',1,3,@IDTipoProv output
	
	Insert Into MATIPOPROVEEDOR(IDTipoProv,Descripcion,PideIdioma,Comision,UserMod)
	Values(@IDTipoProv,@Descripcion,@PideIdioma,@Comision,@UserMod)
	
	Set @pIDTipoProv=@IDTipoProv

