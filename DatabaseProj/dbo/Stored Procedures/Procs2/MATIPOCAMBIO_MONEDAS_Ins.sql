﻿
CREATE PROCEDURE [dbo].[MATIPOCAMBIO_MONEDAS_Ins]
	@CoMoneda char(3),
	@Fecha smalldatetime,
	@ValCompra decimal(6,3),
	@ValVenta decimal(6,3),
	@UserMod char(4)
AS
BEGIN
	Insert Into [dbo].[MATIPOCAMBIO_MONEDAS]
		(
			[CoMoneda],
 			[Fecha],
 			[ValCompra],
 			[ValVenta],
 			[UserMod]
 		)
	Values
		(
			@CoMoneda,
 			@Fecha,
 			@ValCompra,
 			@ValVenta,
 			@UserMod
 		)
	IF @CoMoneda = 'SOL'
	BEGIN
		IF NOT EXISTS(SELECT Fecha FROM MATIPOCAMBIO WHERE Fecha = @Fecha)
		BEGIN
			INSERT INTO MATIPOCAMBIO(Fecha, ValCompra, ValVenta, UserMod, FecMod)
			VALUES(@Fecha, @ValCompra, @ValVenta, @UserMod, GETDATE())
		END
	END
END;
