﻿Create Procedure dbo.PRESUPUESTO_SOBRE_DelxExistenciaDet
@IDOperacion int
As
Set NoCount On
Declare @IDCab int = 0,@IDProveedor char(6)=''
Select @IDCab = IDCab, @IDProveedor = IDProveedor from OPERACIONES Where IDOperacion = @IDOperacion

Delete from PRESUPUESTO_SOBRE
Where IDCab=@IDCab And IDProveedor = @IDProveedor
And Not Exists(select NuDetPSo from PRESUPUESTO_SOBRE_DET psd Where psd.NuPreSob = PRESUPUESTO_SOBRE.NuPreSob)
