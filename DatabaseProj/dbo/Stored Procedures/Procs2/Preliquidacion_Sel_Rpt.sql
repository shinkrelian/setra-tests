﻿--HLF-20140818-GROUP BY       
--HLF-20140821-Quitar columnas EjVentas, IDDebitMemo    
--Order by FechaOut    
--Nuevas columnas C1, C2, PorcC1, PorcC2    
--HLF-20140822-TotalCostosBase, TotalCostosBasesinIN  
--TotalDM de DEBIT_MEMO ya no de DEBIT_MEMO_DOCUMENTO  
--and cc.Estado<>'E'    
CREATE Procedure dbo.Preliquidacion_Sel_Rpt                                                    
 @IDCliente char(6),                                                    
 @IDFile char(8),                                                    
 @FechaOutInicial smalldatetime,                                                    
 @FechaOutFinal smalldatetime                 
As                                                    
 Set NoCount On                                          
        
 SELECT  *,    
 TotalDM-TotalCostosBasesinIN as C1, ((TotalDM-TotalCostosBasesinIN)*100)/TotalDM as PorcC1,   
 TotalDM-TotalCostosBase as C2, ((TotalDM-TotalCostosBase)*100)/TotalDM as PorcC2    
 --TotalDM-TotalCostos AS Margen,((TotalDM-TotalCostos)*100)/TotalDM as PorcMargen        
 FROM      
  (      
  SELECT IDFile, FecInicio, FechaOut, DescCliente, DescPais,        
    Titulo, Pax, Estado, --EjVentas, IDDebitMemo,     
    --SUM(TotalDM) AS TotalDM,  
     (Select sum(Total) From DEBIT_MEMO         
    Where IDCab=A.IDCAB AND IDEstado<>'AN') as TotalDM,      
    SUM(TotalCostosBase) AS TotalCostosBase,  
    SUM(TotalCostosBasesinIN) AS TotalCostosBasesinIN  
  FROM      
   (      
   SELECT *--,TotalDM-TotalCostos AS Margen,        
   --((TotalDM-TotalCostos)*100)/TotalDM as PorcMargen        
   FROM        
    (        
    SELECT DISTINCT IDFile, IDCAB,FecInicio, FechaOut, DescCliente, DescPais,        
    Titulo, Pax, Estado,     
    --EjVentas,            
    --(Select top 1 IDDebitMemo From DEBIT_MEMO_DOCUMENTO         
    --Where NuDocum=Y.NuDocum And IDTipoDoc=Y.IDTipoDoc Order by IDDebitMemo) as IDDebitMemo,            
  
    --(Select sum(SsMonto) From DEBIT_MEMO_DOCUMENTO         
    --Where NuDocum=Y.NuDocum And IDTipoDoc=Y.IDTipoDoc) as TotalDM,    
    --(Select sum(Total) From DEBIT_MEMO         
    --Where IDCab=Y.IDCAB AND IDEstado<>'AN') as TotalDM,        
    TotalCostosBase, TotalCostosBasesinIN        
      
      
    FROM         
     (        
      
     SELECT DISTINCT IDFile,IDCAB ,FecInicio, FechaOut, DescCliente, DescPais,        
     Titulo, Pax, Estado,     
     --EjVentas,  IDDebitMemo    
     NuDocum, IDTipoDoc,        
       
  
     isnull((Select SUM(dd1.SsTotalCostoUSD) From DOCUMENTO_DET dd1         
     Where dd1.NuDocum+dd1.IDTipoDoc in (Select NuDocum+IDTipoDoc From DOCUMENTO Where IDCab=X.IDCAB)  
       
     --Inner Join DEBIT_MEMO_DOCUMENTO dmd1 On dd1.NuDocum=dmd1.NuDocum         
     --And dd1.IDTipoDoc=dmd1.IDTipoDoc And dmd1.IDDebitMemo=X.IDDebitMemo         
     --Where   dmd1.NuDocum=X.NuDocum and dmd1.IDTipoDoc=X.IDTipoDoc And         
     --dmd1.IDDebitMemo=X.IDDebitMemo         
     --Inner Join DEBIT_MEMO_DOCUMENTO dmd1 On dd1.NuDocum=dmd1.NuDocum         
     --And dd1.IDTipoDoc=dmd1.IDTipoDoc And dmd1.IDDebitMemo=X.IDDebitMemo              
     --dmd1.IDDebitMemo=X.IDDebitMemo         
  
     ),0) as TotalCostosBase,       
       
     isnull((Select SUM(dd1.SsTotalCostoUSD) From DOCUMENTO_DET dd1         
     --Inner Join DEBIT_MEMO_DOCUMENTO dmd1 On dd1.NuDocum=dmd1.NuDocum         
     --And dd1.IDTipoDoc=dmd1.IDTipoDoc And dmd1.IDDebitMemo=X.IDDebitMemo         
     --Where   dmd1.NuDocum=X.NuDocum and dmd1.IDTipoDoc=X.IDTipoDoc And         
     --dmd1.IDDebitMemo=X.IDDebitMemo and dd1.CoTipoDet<>'IN'  
     Where dd1.NuDocum+dd1.IDTipoDoc in (Select NuDocum+IDTipoDoc From DOCUMENTO Where IDCab=X.IDCAB)   
		and dd1.CoTipoDet<>'IN'   
     ),0) as TotalCostosBasesinIN  
       
       
     FROM        
      (        
      Select cc.IDFile,cc.IDCAB ,cc.FecInicio, cc.FechaOut, c.RazonComercial as DescCliente, ub.Descripcion as DescPais,        
      cc.Titulo, cc.NroPax + ISNULL(cc.NroLiberados,0) as Pax, cc.Estado,     
      --us.Usuario as EjVentas,     
      dm.IDDebitMemo,    
      --dm.Total as TotalDM,   
      d.NuDocum, d.IDTipoDoc        
      From COTICAB cc Inner Join DEBIT_MEMO dm On cc.IDCAB=dm.IDCab and cc.Estado<>'E'       
      Left Join MACLIENTES c On c.IDCliente=cc.IDCliente        
      Left Join MAUBIGEO ub On cc.IDubigeo=ub.IDubigeo        
      --Left Join MAUSUARIOS us On us.IDUsuario=cc.IDUsuario        
      LEft Join DOCUMENTO d On d.IDCab=cc.IDCAB        
      --Left Join DEBIT_MEMO_DOCUMENTO dmd On dmd.NuDocum=d.NuDocum And         
      -- dmd.IDTipoDoc=d.IDTipoDoc --And dmd.IDDebitMemo=dm.IDDebitMemo        
      Where (cc.IDCliente=@IDCliente Or LTRIM(rtrim(@IDCliente))='') And        
      (cc.IDFile=@IDFile Or LTRIM(rtrim(@idfile))='') And         
      (cc.FechaOut BEtween @FechaOutInicial And @FechaOutFinal Or @FechaOutInicial='01/01/1900')        
      And left(d.NuDocum,3)<>'005'      
      ) AS X        
      
     ) AS Y        
    --WHERE TotalCostos<>0        
    ) AS Z        
   ) AS A      
  Group by IDFile, IDCab, FecInicio, FechaOut, DescCliente, DescPais,        
    Titulo, Pax, Estado--, EjVentas, IDDebitMemo      
  ) AS B      
        
 Order by FechaOut    
 --IDFile,FecInicio        
