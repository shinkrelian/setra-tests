﻿CREATE Procedure [dbo].[MAUSUARIOSOPC_Upd]
	@IDOpc	char(6),
	@IDUsuario	char(4),
	@Accion	varchar(10),
	@Consultar	bit,
	@Grabar	bit,
	@Actualizar	bit,
	@Eliminar	bit,
	@UserMod	char(4)
As
	Set NoCount On
	
	UPDATE MAUSUARIOSOPC 
	Set Consultar=Case When @Accion='Consultar' Then @Consultar Else Consultar End, 
	Grabar=Case When @Accion='Grabar' Then @Grabar Else Grabar End,  
	Actualizar=Case When @Accion='Actualizar' Then @Actualizar Else Actualizar End,
	Eliminar=Case When @Accion='Eliminar' Then @Eliminar Else Eliminar End, 
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where IDOpc=@IDOpc And IDUsuario=@IDUsuario
