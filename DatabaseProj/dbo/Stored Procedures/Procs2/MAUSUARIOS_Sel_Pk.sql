﻿--JHD-20150428-,idpais=(select idpais from MAUBIGEO where IDubigeo=MAUSUARIOS.IDUbigeo) 
CREATE Procedure [dbo].[MAUSUARIOS_Sel_Pk]
	@IDUsuario char(4)
As
	Set NoCount On
	
	Select *
	,idpais=(select idpais from MAUBIGEO where IDubigeo=MAUSUARIOS.IDUbigeo)
	 From MAUSUARIOS Where IDUsuario=@IDUsuario

