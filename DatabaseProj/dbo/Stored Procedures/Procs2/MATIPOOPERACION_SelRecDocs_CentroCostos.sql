﻿--JHD-201506212- Mostrar Todos los Centros de costo
CREATE procedure MATIPOOPERACION_SelRecDocs_CentroCostos
@CoCeCos char(6)=''
as
SELECT IdToc, Descripcion,TxDefinicion FROM 
	(	
	Select '' as IdToc, '' as Descripcion,'' as TxDefinicion , 0 as Ord
	Union
	Select IdToc, Descripcion,TxDefinicion=ISNULL(TxDefinicion,''), 1 as Ord From MATIPOOPERACION	
	) As X
	 Where ( Ord<>0)   
	Order by Ord,Descripcion
 /*
select distinct  IdToc, Descripcion
from MAPLANCUENTAS_CENTROCOSTOS pc
inner join CTASCONTAB_OPERCONTAB_TIPODOC co
on co.CoCtaContab=pc.CtaContable
inner join MATIPOOPERACION toc on toc.IdToc=co.CoTipoOC
where CoCeCos=@cocecos and 
Not CoDestOper Is Null 
order by toc.Descripcion
*/
