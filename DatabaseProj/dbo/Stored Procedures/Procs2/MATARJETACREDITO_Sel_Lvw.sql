﻿Create Procedure [dbo].[MATARJETACREDITO_Sel_Lvw]  
 @CodTar char(3),  
 @Descripcion varchar(50)  
As  
 Set NoCount On  
   
 Select Descripcion   
 From MATARJETACREDITO  
 Where (Descripcion Like '%'+@Descripcion+'%')  
 And (CodTar<>@CodTar Or ltrim(rtrim(@CodTar))='')  
 Order by Descripcion
