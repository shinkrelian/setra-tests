﻿CREATE Procedure [dbo].[RESERVAS_DET_PAX_SelxIDCab]
@Idcab int
as

	Set NoCount On
	Select  cp.IDIdentidad,di.Descripcion as DescIdentidad, cp.NumIdentidad,   
		cp.Titulo, cp.Nombres, cp.Apellidos, cp.FecNacimiento,  
		cp.IDNacionalidad,u.Gentilicio as DescNacionalidad ,cp.IDPax  
	from RESERVAS_DET_PAX Rdp Left Join COTIPAX cp On Rdp.IDPax=cp.IDPax
							  Left Join MATIPOIDENT di On cp.IDIdentidad=di.IDIdentidad
							  Left Join MAUBIGEO u On cp.IDNacionalidad=u.IDubigeo
	Where Rdp.IDPax in (select IDPax from cotipax where idcab=@Idcab)
