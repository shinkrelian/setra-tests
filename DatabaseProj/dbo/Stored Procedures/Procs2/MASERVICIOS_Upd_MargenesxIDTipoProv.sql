﻿

Create Procedure dbo.MASERVICIOS_Upd_MargenesxIDTipoProv
	@IDTipoProv char(3),	
	@Comision	numeric(6,2),
    @UserMod char(4)

As
	Set NoCount On
	
	UPDATE MASERVICIOS 
	Set Comision=@Comision, UserMod=@UserMod, FecMod=GETDATE()
	Where IDTipoProv=@IDTipoProv And UpdComision=0 --And IDTipoProv='001'
	