﻿CREATE Procedure [dbo].[MATIPOSERV_Sel_List]
	@IDservicio char(3),
	@Descripcion varchar(60)
As	
	Set NoCount On

	Select IDservicio,Descripcion, Ingles
	From MATIPOSERV Where (Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='') and 
		(IDservicio=@IDservicio Or LTRIM(RTRIM(@IDservicio))='')
	Order by 2
