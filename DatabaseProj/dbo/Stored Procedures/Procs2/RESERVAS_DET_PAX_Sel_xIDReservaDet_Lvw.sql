﻿--JRF-Considerar NoShow
CREATE PROCEDURE dbo.RESERVAS_DET_PAX_Sel_xIDReservaDet_Lvw  
 @IDReserva_Det int,
 @NoShow bit
As  
  
 Set NoCount On   
   
 Select d.IDPax  
 From RESERVAS_DET_PAX d Inner Join COTIPAX c On c.IDPax=d.IDPax --And d.IDReserva_Det=@IDReserva_Det  
 Where d.IDReserva_Det=@IDReserva_Det and c.FlNoShow=@NoShow
 Order by d.IDPax
