﻿CREATE PROCEDURE DBO.MATIPOSCAMBIO_USD_Sel_Pk
@FeTipCam smalldatetime,
@CoMoneda char(3)
as
Set NoCount On
Select *
from MATIPOSCAMBIO_USD 
where CoMoneda = @CoMoneda and 
CONVERT(CHAR(10),FeTipCam,103)=CONVERT(CHAR(10),@FeTipCam,103)
