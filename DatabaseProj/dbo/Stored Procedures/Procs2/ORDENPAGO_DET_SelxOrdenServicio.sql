﻿
Create Procedure dbo.ORDENPAGO_DET_SelxOrdenServicio
	@IDCab int,
	@NuOrden_Servicio int
As
	Set Nocount On
	
	SELECT Distinct IDOrdPag
	FROM ORDENPAGO_DET WHERE IDOrdPag IN (SELECT IDOrdPag FROM ORDENPAGO 
		WHERE IDCab=@IDCab)
	AND IDServicio_Det In 
	(SELECT IDServicio_Det
	FROM ORDEN_SERVICIO_DET WHERE NuOrden_Servicio IN (SELECT NuOrden_Servicio FROM ORDEN_SERVICIO
		WHERE IDCab=@IDCab)
	AND NuOrden_Servicio=@NuOrden_Servicio)



