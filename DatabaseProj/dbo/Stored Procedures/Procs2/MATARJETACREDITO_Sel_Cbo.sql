﻿Create Procedure [dbo].[MATARJETACREDITO_Sel_Cbo]
@bTodos bit
as

	Set NoCount On
	
		SELECT * FROM
	(
	Select '' as IDTipoProv,'------' as Descripcion, 0 as Ord
	Union
	
	Select CodTar, Descripcion, 1 as Ord From MATARJETACREDITO

	) AS X
	Where (@bTodos=1 Or Ord<>0)
	Order by Ord,2
