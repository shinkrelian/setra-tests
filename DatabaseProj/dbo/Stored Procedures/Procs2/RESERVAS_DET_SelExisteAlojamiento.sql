﻿Create Procedure dbo.RESERVAS_DET_SelExisteAlojamiento
@IDReserva int,
@pExisteAlojamiento bit Output
As
Set NoCount On
Set @pExisteAlojamiento = 0
Set @pExisteAlojamiento = dbo.FnExistenAlojamiento(@IDReserva)
