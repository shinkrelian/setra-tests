﻿--JRF-20150410-Left Join COTIPAX cp On psd.IDPax=cp.IDPax
CREATE Procedure dbo.PRESUPUESTO_SOBRE_Sel_RptPlanGastosMovilidadxDet
@NuDetPSo tinyint,
@NuPreSob int
As
Set NoCount On
Select ROW_NUMBER()Over(Order by psd.FeDetPSo) as Item,
	IsNull(p.NombreCorto,cp.Titulo+' '+cp.Apellidos+' '+cp.Nombres) as NombreGuia,      
    Case When p.IDIdentidad = '001' then p.NumIdentidad Else '' End as RUC,      
    c.Titulo as NombreDelFile,c.IDFile,      
    CONVERT(char(10),psd.FeDetPSo,103) as DescFecha,CONVERT(Char(5),psd.FeDetPSo,108) as DescHora,      
    psd.TxServicio,psd.SsTotal      
from PRESUPUESTO_SOBRE ps Left Join COTICAB c on ps.IDCab = c.IDCAB      
Left Join MAPROVEEDORES p On ps.CoPrvGui = p.IDProveedor      
Left Join PRESUPUESTO_SOBRE_DET psd On ps.NuPreSob = psd.NuPreSob      
Left Join COTIPAX cp On psd.IDPax=cp.IDPax
where psd.NuPreSob=@NuPreSob and psd.NuDetPSo= @NuDetPSo
and charindex('taxi',lower(psd.TxServicio))>0
