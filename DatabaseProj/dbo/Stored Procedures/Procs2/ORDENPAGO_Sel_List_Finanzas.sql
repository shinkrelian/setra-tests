﻿--JRF-20151020-Deshacer cambios[...(@IDPais='000003' And (p.CoUbigeo_Oficina<>@CoUbigeo_Oficina And u.IDPais=@IDPais
--HLF-20151207-And (c.FlHistorico=@FlHistorico Or @FlHistorico=1) 
--PPMG-20160226--Se cambio el corroe de eugenia.rosas@setours.com por victoria.ontanilla@setours.com
--JHD-20160405--MailCopia= CASE WHEN @CoUbigeo_Oficina='000113' THEN 'victoria.ontanilla@setours.com' WHEN @CoUbigeo_Oficina='000110' THEN 'marcela.barriga@setours.com' else 'pagos2@setours.com' end,
CREATE Procedure [dbo].[ORDENPAGO_Sel_List_Finanzas] 
 @IDFile varchar(8),                    
 @DescripcionProveedor varchar(200),            
 @FechaPagoIni smalldatetime,                    
 @FechaPagoFin smalldatetime,                 
 @FechaEmisionIni smalldatetime,            
 @FechaEmisionFin smalldatetime,               
 @IDEstado char(2),                
 @IDReserva int,                
 @IDMoneda char(3),    
 @IDBanco char(3),    
 @IDCab int,
 @CoUbigeo_Oficina char(6),
 @FlEnviado bit=0,
 @FlHistorico bit=1  
As                    
                    
Set Nocount On                    
              
 Declare @IDPais char(6)=(select IDPais from MAUBIGEO where IDubigeo=@CoUbigeo_Oficina)       
 Select c.IDCAB,  
 case when op.FechaEmision is null then '' else convert(varchar,op.FechaEmision,103)End as FechaEmision,            
 convert(varchar,op.FechaPago,103) as FechaPago, op.IDOrdPag,                  
 Case c.CoTipoVenta      
 when 'RC' Then 'RECEPTIVO'      
 when 'CU' Then 'COUNTER'      
 when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'      
 when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'      
 End as DescTipoVenta,c.IDFile As IDFile,                  
 p.NombreCorto as DescProvee,   
 Pais=isnull((select Descripcion from MAUBIGEO where IDubigeo=u.IDPais),''),        
 b.Sigla as SiglaBanco ,    
 p.IDProveedor As IDProveedor,                 
 op.IDReserva ,              
 p.IDTipoProv As IDTipoProv ,                  
            
 M.IDMoneda As IDMoneda,                  
 m.Descripcion as DescMoneda,                    
 op.IDEstado,                    
 Case op.IDEstado                     
 When 'GR' Then 'GENERADO'                    
 When 'PD' Then 'PENDIENTE'                    
 When 'PG' Then 'PAGADO'                    
 End as DescEstado          
 --,(Select Sum(Total) From ORDENPAGO_DET Where IDOrdPag=op.IDOrdPag) as Total          
 ,(Select Sum(isnull(Total_Final,0)) From ORDENPAGO_DET Where IDOrdPag=op.IDOrdPag) as Total          
                 
 --(Select Sum(Case When op1.TCambio IS NULL Then Total                 
 -- Else 
 --  dbo.FnCambioMoneda(Total,'SOL','USD',op1.TCambio ) End )                 
 -- From ORDENPAGO_DET od1 Inner Join ORDENPAGO op1 On od1.IDOrdPag=op1.IDOrdPag                
 -- Where od1.IDOrdPag=op.IDOrdPag) as TotalUSD         
 ,Isnull(p.EmailContactoAdmin,'') as EmailAdmin ,  
 (select COUNT(*) from ORDENPAGO_ADJUNTOS oad Where oad.IDOrdPag=op.IDOrdPag) CantidadAdjuntos ,
 --'contabilidad3@setours.com' as MailCopia
 --'contabilidad3@setours.com; pagos2@setours.com' as MailCopia
 --MailCopia= CASE WHEN @CoUbigeo_Oficina='000113' THEN 'eugenia.rosas@setours.com' WHEN @CoUbigeo_Oficina='000110' THEN 'marcela.barriga@setours.com' else 'contabilidad3@setours.com; pagos2@setours.com' end
 --MailCopia= CASE WHEN @CoUbigeo_Oficina='000113' THEN 'victoria.ontanilla@setours.com' WHEN @CoUbigeo_Oficina='000110' THEN 'marcela.barriga@setours.com' else 'cobranzas@setours.com' end,
 MailCopia= CASE WHEN @CoUbigeo_Oficina='000113' THEN 'victoria.ontanilla@setours.com' WHEN @CoUbigeo_Oficina='000110' THEN 'marcela.barriga@setours.com' else 'pagos2@setours.com' end,
 CoSap=isnull(OP.CoSap,'')
 From ORDENPAGO op                     
 Inner Join COTICAB c On op.IDCab=c.IDCAB                    
 Left Join RESERVAS r On op.IDReserva=r.IDReserva                    
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor                
 left join MAPROVEEDORES pint on p.IDProveedorInternacional=pint.IDProveedor                
 Left Join MAMONEDAS m On op.IDMoneda=m.IDMoneda                    
 Left Join MABANCOS b On op.IDBanco = b.IDBanco    
 left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
 Where (op.FechaPago Between @FechaPagoIni And @FechaPagoFin Or @FechaPagoIni='01/01/1900')                    
 And (convert(smalldatetime,convert(varchar,op.FechaEmision,103))          
 between @FechaEmisionIni And @FechaEmisionFin Or @FechaEmisionIni='01/01/1900')            
 And (c.IDFile Like '%'+ltrim(rtrim(@IDFile))+'%' Or ltrim(rtrim(@IDFile))='')                    
 And (op.IDEstado=@IDEstado Or ltrim(rtrim(@IDEstado))='')                    
 And (op.IDReserva=@IDReserva Or @IDReserva=0)             
 And (p.NombreCorto like '%'+@DescripcionProveedor+'%' or pint.NombreCorto like '%'+@DescripcionProveedor+'%' or LTRIM(rtrim(@DescripcionProveedor))='')            
 And (c.IDCAB=@IDCab Or @IDCab=0)                
 And (op.IDBanco = @IDBanco Or ltrim(rtrim(@IDBanco))='')    
 And (op.IDMoneda = @IDMoneda Or ltrim(rtrim(@IDMoneda))='')    
 And (p.CoUbigeo_Oficina=@CoUbigeo_Oficina Or LTRIM(rtrim(@CoUbigeo_Oficina))='') --Or
	 --(@IDPais='000003' And (p.CoUbigeo_Oficina<>@CoUbigeo_Oficina And u.IDPais=@IDPais)))
--and (op.FlEnviado=@FlEnviado or @FlEnviado=0)
 and (op.FlEnviado=@FlEnviado)
 And (c.FlHistorico=@FlHistorico Or @FlHistorico=1)  
 Order by op.FechaPago asc --,op.IDOrdPag           


