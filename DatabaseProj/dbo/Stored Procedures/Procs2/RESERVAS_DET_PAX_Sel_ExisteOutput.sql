﻿Create Procedure [dbo].[RESERVAS_DET_PAX_Sel_ExisteOutput]
	@IDReserva_Det int,
	@IDPax	int, 
	@pExiste	bit Output
As
	Set NoCount On
	
	If Exists(Select IDReserva_Det From RESERVAS_DET_PAX
		WHERE IDReserva_Det=@IDReserva_Det	And IDPax=@IDPax)
		Set @pExiste=1
	Else
		Set @pExiste=0
