﻿
--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet
CREATE Procedure [dbo].[RESERVAS_DET_ESTADOHABITAC_InsxIDReserva]
	@IDReserva int,		
	@UserMod	char(4)
As

	Set Nocount On
	
	Insert Into RESERVAS_DET_ESTADOHABITAC
	(IDDet,Simple,Twin,Matrimonial,Triple,UserMod)
	
	Select IDDet,
	Case When Simple=0 Then Null Else Simple End as Simple,
	Case When Twin=0 Then Null Else Twin End as Twin,
	Case When Matrimonial=0 Then Null Else Matrimonial End as Matrimonial,
	Case When Triple=0 Then Null Else Triple End as Triple,
	UserMod
	From (
	Select IDDet, 
	isnull(c.Simple,0)+ISNULL(c.SimpleResidente,0) as Simple,
	isnull(c.Twin,0)+ISNULL(c.TwinResidente,0) as Twin,
	isnull(c.Matrimonial,0)+ISNULL(c.MatrimonialResidente,0) as Matrimonial,
	isnull(c.Triple,0)+ISNULL(c.TripleResidente,0) as Triple,
	@UserMod as UserMod
	From RESERVAS_DET rd 
	Left Join RESERVAS r On rd.IDReserva=r.IDReserva
	Left Join COTICAB c On r.IDCab=c.IDCAB
	Where rd.IDReserva=@IDReserva)as X
