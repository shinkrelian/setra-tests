﻿Create Procedure dbo.MAUBIGEO_SelCiudadesTourAdicAPT
	
As
	Set Nocount On

	Select ub.IDubigeo, ub.Descripcion + space(100) + '|'+isnull(up.Descripcion,'') as DescCiudad
	From MAUBIGEO ub Left Join MAUBIGEO up On ub.IDPais=up.IDubigeo
	Where ub.FlTourAdicAPT=1

