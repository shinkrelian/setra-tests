﻿--Declare @TipoServicio varchar(15) = ''
--Execute MASERVICIOS_DIA_GetAPIDescripciones 'CUZ00324','SPANISH',@TipoServicio output
--Select @TipoServicio
--Go

CREATE Procedure dbo.MASERVICIOS_DIA_GetAPIDescripciones
@IDServicio char(8),
@IDIdioma varchar(12),
@TipoServicio varchar(15) output
As
BEGIN
	Set NoCount On
	--select * from MASERVICIOS_DIA where IDServicio='IQT00053' And IDIdioma='ENGLISH'
	--select * from MASERVICIOS_DIA where IDServicio='AQP00080' And IDIdioma='SPANISH'
	Set @TipoServicio = 'empty'
	Declare @CantMaxima int = IsNull((select Max(Dia) from MASERVICIOS_DIA where IDServicio=@IDServicio And IDIdioma=@IDIdioma),0)
	if @CantMaxima > 1
	begin
		Set @TipoServicio = 'multiple'
		select Dia as nro, case when ltrim(rtrim(convert(varchar,Descripcion))) = '-' then 'No description' else Descripcion end as [description]
		from MASERVICIOS_DIA 
		where IDServicio=@IDServicio And IDIdioma=@IDIdioma
		return
	end
	else if @CantMaxima = 1
	begin	
		if ISNULL((select top 1 ltrim(rtrim(convert(varchar,Descripcion))) as [description]
			from MASERVICIOS_DIA where IDServicio=@IDServicio And IDIdioma=@IDIdioma),'-') <> '-'
		begin
			Set @TipoServicio = 'single'
			select Dia as nro, ltrim(rtrim(convert(varchar,Descripcion))) as [description]
			from MASERVICIOS_DIA 
			where IDServicio=@IDServicio And IDIdioma=@IDIdioma
			return
		end
	end
	select convert(tinyint,'0') as nro, convert(varchar(max),'') as [description] where 0<>0
END

