﻿
Create Procedure dbo.MASERVICIOS_Upd_MargenesxIDCat
	@IdCat char(3),	
	@Comision	numeric(6,2),
    @UserMod char(4)

As
	Set NoCount On
	
	UPDATE MASERVICIOS 
	Set Comision=@Comision, UserMod=@UserMod, FecMod=GETDATE()
	Where IDCat=@IdCat And UpdComision=0 And IDTipoProv='001'
	
