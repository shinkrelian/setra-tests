﻿create Procedure [dbo].[MAUSUARIOS_Sel_Coticab] 
@IDCab int
As
Set NoCount On
begin

Select u.IDUsuario,
IsNull(u.Nombre,'') +' '+isNuLL(u.TxApellidos,'') As Usuario,
u.CoUserJefe,
IsNull(ujefe.Nombre,'') +' '+isNuLL(ujefe.TxApellidos,'') As Supervisor
from MAUSUARIOS u inner join coticab c on c.IDUsuario=u.IDUsuario
Left Join MAUSUARIOS ujefe On u.CoUserJefe=ujefe.IDUsuario
Where c.IDCAB=@IDCab

end
