﻿CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_TMP_Upd_NuevaTarifa
	@IDCab int,
	@IDProveedor int,
	@UserMod char(4)
As

	Set Nocount on
	
	Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)        

	Update OPERACIONES_DET_DETSERVICIOS_TMP set IDServicio_Det_V_Cot=y.NewIDServicio_Det_V_Cot,
		TotImpto=case when Afecto=1 then DBO.FnNetoMaServicio_Det(y.NewIDServicio_Det_V_Cot,y.QtPax)*(@NuIgv*0.01) else 0 end,
		FecMod=GETDATE(), UserMod=@UserMod
	From	
	(Select IDServicio_Det, IDOperacion_Det, FlServicioNoShow, IDServicio_Det_V_Cot, 
	Case When NewIDServicio_Det_V_Cot = 0 Then Null Else NewIDServicio_Det_V_Cot End as NewIDServicio_Det_V_Cot,
	Afecto, QtPax
	From
		(Select ods.IDServicio_Det, ods.IDOperacion_Det, ods.FlServicioNoShow, ods.IDServicio_Det_V_Cot, 
		dbo.FnNuevaTarifa(sd.IDServicio,sd.Tipo,sd.Anio,od.dia,ods.IDServicio_Det_V_Cot, sd.Codigo) as NewIDServicio_Det_V_Cot,
		sd.Afecto, od.NroPax+isnull(od.NroLiberados,0) as  QtPax
		From OPERACIONES_DET_DETSERVICIOS_TMP ods Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=ods.IDServicio_Det_V_Cot
		Inner Join OPERACIONES_DET_TMP od On ods.IDOperacion_Det=od.IDOperacion_Det
		Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab And o.IDProveedor=@IDProveedor
		Where ods.Activo=1
			--In (Select IDOperacion_Det From OPERACIONES_DET Where IDOperacion = @IDOperacion)
		) as X	) as Y
		Where (y.IDServicio_Det_V_Cot<>y.NewIDServicio_Det_V_Cot And y.NewIDServicio_Det_V_Cot <> 0)
		And y.IDOperacion_Det=OPERACIONES_DET_DETSERVICIOS_TMP.IDOperacion_Det 
		And y.IDServicio_Det=OPERACIONES_DET_DETSERVICIOS_TMP.IDServicio_Det
		And y.FlServicioNoShow=OPERACIONES_DET_DETSERVICIOS_TMP.FlServicioNoShow
