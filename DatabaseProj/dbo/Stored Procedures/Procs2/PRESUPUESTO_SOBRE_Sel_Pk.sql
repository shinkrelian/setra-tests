﻿--HLF-20151217-cc.IDUsuarioOpe_Cusco
--HLF-20160216-Utilizando FnFondoFijo_ER     case when ps.CoEstado='EN'
CREATE Procedure dbo.PRESUPUESTO_SOBRE_Sel_Pk
@NuPreSob int
As
Set NoCount On
select ps.*, cc.IDUsuarioOpe_Cusco,
	Case When ps.CoEstado='EN' then
		dbo.FnFondoFijo_ER (ps.IDProveedor)
	Else
		''
	End as NuCodigo_ER_Ok
from PRESUPUESTO_SOBRE ps Inner Join COTICAB cc On ps.IDCab=cc.IDCAB	
where ps.NuPreSob = @NuPreSob
