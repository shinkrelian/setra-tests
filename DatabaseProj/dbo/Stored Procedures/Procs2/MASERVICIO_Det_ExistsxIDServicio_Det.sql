﻿Create Procedure dbo.MASERVICIO_Det_ExistsxIDServicio_Det
@IDServicio_Det int,
@IDCab int,
@pExists bit Output
As
Set NoCount On
Set @pExists = 0
select @pExists = 1 
from MASERVICIOS_DET sd Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
Where s.IDCabVarios=@IDCab And sd.IDServicio_Det=@IDServicio_Det
