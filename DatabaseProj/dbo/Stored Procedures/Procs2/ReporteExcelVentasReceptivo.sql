﻿
--JHD-20150120-Se agregaron las tablas de comparativo por Cant. Files y Top 15 clientes      
--JHD-20150120-Se agrego la tabla temporal #VentasxCliente_Anual para calcular el Top 15 clientes      
--HLF-20150122-Case When EstadoFacturac='F' Then X.TotalDM ...    
--(Select SUM(Total) From DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN') as TotalDM,    
--Comentando isnull((Select Max(Dia) From COTIDET cd1  ... as FecOutPeru          
--JHD-20150123- Case When ISNULL(c.RazonComercial,'')='' Then c.RazonSocial Else c.RazonComercial  End as DescCliente,      
--HLF-20150129-HOJA VENTAS POR CUENTA  
--JHD-20150129-  FecOutPeru as FecOut,    
--JHD-20150129-  order by FecOutPeru,FecOut,FecInicio  
--JHD-20150129-  Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
  
--JHD-20150203-  (X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz  
--JHD-20150203-  NroPax=(NroPax+isnull(NroLiberados,0))  
--JHD-20150205-  Se corrigio el cursor curClientes, insertando los clientes que no tenian Valor Venta en el mes de Enero.  
--JHD-20150205-  Se corrigio el cursor curEjecutivas, insertando los ejecutivos que no tenian Ventas en el mes de Enero.  
--JHD-20150206-   select * from #VentasxCliente_Anual order by 4 desc  
  
--JHD-20150206-   Se agregaron campos y se cambio las consultas internas en la tabla temporal #VentasReceptivoMensualDet para recalcular el valor venta y agregar el total concretado y proyectado.  
--JHD-20150223-   Para files en el pasado siempre tomar valor de la suma de los DM:  
      --ValorVenta=case when FecInicio<GETDATE()   
      --then  
      -- (  
      --  isnull(TotalDM,0)  
      -- )  
      --else --futuro  
      -- (  
      --  ISNULL(ValorVenta2,0)  
      -- )  
      --end  
  
--JHD-20150225-   Se agregó el campo SumaProy el cual es el valor venta multiplicado x el porcentaje de concretizacion  
--JHD-20150225-    Case When SumaProy<>0 Then      
     --((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
     --Else      
     --100      
     --End as VarImporteUSD,     
--JHD-20150225-  Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,SumaProy,ImporteUSDAnioAnter            
--JHD-20150225-  Redondear los valores de ValorVenta a 0 decimales           
--JHD-20150227-  Reemplazar el campo ValorVenta por SumaProy para las consultas de: Ventas por cuenta, Turnover y comparativo  
--JRF-20150311-  Considerar la + dbo.FnDevuelveMontoTotalxPaxVuelos(c.IDCAB,1)
--JHD-20150318 -  CantFilesAnioAnter=isnull((SELECT distinct sum(QtNumFiles) FROM RPT_VENTA_CLIENTES
--JHD-20150318 -  TicketFileAnioAnter= CASE WHEN CantFilesAnioAnter=0 THEN 0 ELSE ImporteUSDAnioAnter/CantFilesAnioAnter END ,
--JHD-20150318 -  Se agregó parámetro @@IDUser char(4)='' 
--JHD-20150319 -  Se agregaron tablas temporales para las ventas sin apt
--JHD-20150319 - Se agregaron las columnas dias y edad_prom:  ,Dias=DATEDIFF(day,FecInicio,FechaOut)+1 ,Edad_Prom=isnull(dbo.FnEdadPromedio_Pax(cc.IDCab),0)
--JHD-20150320 - Se agregaron tablas temporales para cuentas por pais y top 15 destinos
--JHD-20150323 - Se corrigió el problema del promedio de las edades, excluyendo los ceros.
--JHD-20150408 -  select * from #VentasReceptivoMensual  order by FecOutPeru
--JHD-20150409 -   JunVentaMes=isnull(JunVentaMes,0)
--JHD-20150409 -  Se agrego una condicion para los montos, cuando no existan debit memos de un File que se muestre el Valor Venta
				-- case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
				-- SumaProy=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
--JHD-20150409 - VarEjecutivos=(Case When TotVendido<>0 Then      
				-- ((TotVendido-TotVendidoAnioAnter)*100)/TotVendido      
				-- Else      
				-- 0
				--end
				--)
--JHD-20150414 - Se cambio Europe por 'Europa' y 'S&C-America' por 'Latam'
--JHD-20150415 - Se agregaron los campos Cant_Clientes y Cant_Clientes_Ant para las tablas de Cuentas por Pais y Cuentas por Region
--JHD-20150415 - Se cambio el orden by para mostrar montos acumulados de los ejecutivos 
--JHD-20150416 - Select CoPais,DescPaisCliente,IDCliente as CoCliente,DescCliente,FecOutPeru, SUM(ValorVenta) as ValorVenta
--JHD-20150417 - Case When ImporteUSDAnioAnter<>0 Then            
					--case when ImporteUSD>=ImporteUSDAnioAnter
					--then
					--	((ImporteUSD)*100)/ImporteUSDAnioAnter      
					--else
					--	(((ImporteUSDAnioAnter)*100)/ImporteUSD/*-100*/)*-1
					--end     
				 --Else      
				 --0    
				 --End as Crecimiento,
--JHD-20150422 - Se cambio calculo del crecimiento para la tabla TurnOver
--JHD 20150511 - select * from #VentasReceptivoMensual_Sin_APT Order by cast(FecOutPeru as int)  
--JHD 20150512 ,Total_Programado=[dbo].[FnDevolverTotalProgramacion_Coti](cc.IDCAB,'USD')
--JHD 20150512 -   MargenGan= case when FecInicio<GETDATE()
				--   then
				--	(((case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado)/(case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end ))*100
				--   else
				--	ISNULL(MargenGan,0)  
				--   end,
				
		--Utilidad= case when FecInicio<GETDATE()
				--   then
				--	  (case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado
				--   else
				--	  ISNULL(ValorVenta2*(MargenGan/100),0)
				--   end,
--JHD 20150512 - 	case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					--X.TotalSimple+X.TotalDoble+X.TotalTriple 
					--else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
--JHD 20150512 - Se agregaron nuevas tablas para mostrar las ventas sin porcentaje de concretización
--JHD 20150515 - ,Total_Programado=dbo.FnDevolverTotal_Preliquidacion_CotiCab(cc.IDCAB,'')
--JHD 20150522 -  AND ((IdArea='VS' AND Nivel in ('VTA','SVE','JAR')) OR (IdArea='OP' AND Nivel='OPE') or Nivel='GER')    
--ReporteExcelVentasReceptivo '2015',''
CREATE Procedure dbo.ReporteExcelVentasReceptivo      
--declare
 @Anio char(4),
 @IDUser char(4)=''      
As      
Set nocount on      
   
   
  Declare @AnioAnter char(4)=cast(@Anio as smallint)-1         
       
 -------------SIN APT-----------------------      
 
 
 SELECT FecInicio,  
   FecOut,  
   FecOutPeru,  
   IDCAB,  
   IDFile,  
   Titulo,  
   --ValorVenta=ISNULL(ValorVenta2,0),  
   --ValorVenta=round(case when FecInicio<GETDATE() /*pasado*/  
   ValorVenta=round(case when FecOut<GETDATE() /*pasado*/  
   then  
    (  
     --isnull(TotalDM,0)  
	  case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
    )  
   else --futuro  
    (  
     ISNULL(ValorVenta2,0)  
    )  
   end,0),  
   PoConcretVta,  
   NroPax,  
   ESTADO,  
   IDUsuario,  
   Responsable,  
   IDCliente,  
   CoPais,  
   DescPaisCliente,  
   DescCliente,  
   MargenGan=ISNULL(MargenGan,0),  
   Utilidad=ISNULL(ValorVenta2*(MargenGan/100),0)--,  
    
  -- , Total_Proy=case when FecInicio<GETDATE() /*pasado*/  
   , Total_Proy=case when FecOut<GETDATE() /*pasado*/  
   then  
    (  
     case when PoConcretVta<100 then ISNULL(ValorVenta2,0)*(PoConcretVta/100) else 0 end  
    )  
   else --futuro  
    (  
     case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then 0 else TotalCotiz end  
    )  
   end  
     
   --, Total_Concret=case when FecInicio<GETDATE() /*pasado*/  
   , Total_Concret=case when FecOut<GETDATE() /*pasado*/  
   then  
    (  
     case when PoConcretVta=100 then ISNULL(ValorVenta2,0) else 0 end  
    )  
   else --futuro  
    (  
     case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then TotalDM else 0 end  
    )  
   end  
  
--,  SumaProy=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
,  SumaProy=round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
  --,  SumaProy2=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
  ,  SumaProy2=round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
--,  SumaProy=round(( round((case when FecInicio<GETDATE() then (isnull(TotalDM,0))  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
    
 INTO #VentasReceptivoMensualDet_Sin_APT  
 FROM  
 (     
 SELECT Y.*,  
  
  ISNULL(Y.ValorVenta*(Y.MargenGan/100),0) AS Utilidad  
  ,ValorVenta2=case when Y.TotalDM>0 then (CASE WHEN ABS(Y.TotalDM-Y.TotalCotiz)>=Y.TotalDM_Porc THEN Y.TotalCotiz ELSE Y.TotalDM END) else Y.TotalCotiz end  

 FROM     
 (  
   SELECT       
   FecInicio,  
   FecOutPeru as FecOut,      
   Left(CONVERT(varchar,FecOutPeru,112),6) AS FecOutPeru,      
   IDCAB, IDFile, Titulo,     
   --isnull((Select sum(total*nropax) From Cotidet Where idcab=X.idcab),0) as ValorVenta       
    
   Case When EstadoFacturac='F' Then X.TotalDM    
   Else  --total de cotizacion  
     Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
       isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
      Else    
       --X.TotalSimple+X.TotalDoble+X.TotalTriple              
	   	case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
      End 
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
     End     
   End 
   as ValorVenta ,    
     
   PoConcretVta,  
   --EsTotalxPax,ExisteHotel,       
   --NroPax,  
   NroPax=(NroPax+isnull(NroLiberados,0)),  
   Estado ,  
   IDUsuario,  
   Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
   , IDCliente, CoPais,  DescPaisCliente,  DescCliente,  
        
   case when X.TotalDobleNeto = 0 then 0         
   else                                                 
   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                                 
   End as MargenGan       
        
   ,TotalDM=isnull(X.TotalDM,0)  
   ,TotalDM_Porc=X.TotalDM*0.15  
   ,TotalCotiz=  Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
    isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
      Else    
			--X.TotalSimple+X.TotalDoble+X.TotalTriple              
				case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
      End    
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
     End     
       
   ,TotalCotiz_Porc=  (Case When ExisteHotel=1 Then    
          Case When EsTotalxPax=1 Then    
				isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
          Else    
				--X.TotalSimple+X.TotalDoble+X.TotalTriple              
					case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
          End    
         Else    
        -- X.NroPax * X.TotalCotiz       
        --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
        ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
         End     
      )*0.15  
   FROM       
   (      
   SELECT CC.*,      
   (Select SUM(Total) From DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN')
   + Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN') Then dbo.FnDevuelveMontoTotalxPaxVuelos(cc.IDCAB,1) Else 0 End as TotalDM,    
   --isnull((Select Max(Dia) From COTIDET cd1                
   --Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo                 
   --And u1.IDPais='000323'                
   --Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det                                                  
   --Where IDCAB=cc.IDCAB /*And SD1.idTipoOC<>'005'*/),cc.FechaOut) as FecOutPeru,      
     
   cc.FechaOut as FecOutPeru,      
   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) *((Isnull(cc.Twin,0)+Isnull(cc.Matrimonial,0))) *2) + dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,1) as TotalDoble,
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * Isnull(cc.Simple,0) as TotalSimple,                                                    
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * (Isnull(cc.Triple,0)*3) as TotalTriple,      
   (select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = CC.IDCAB) As TotalCotiz, --As TotalCotiz,
   dbo.FnEsTotalxPax(CC.IDCAB) as EsTotalxPax,      
   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                          
   Where cd.IDCAB = CC.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,      
        
   (select SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,0) as TotalDobleNeto, --as TotalDobleNeto,      
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalSimpleNeto,--as TotalSimpleNeto,      
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalTripleNeto,--as TotalTripleNeto,
   (select SUM(TotImpto) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalImpto,      
   (select SUM(CostoReal) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalCostoReal,      
   (select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = cc.IDCAB) as TotalLib,  
   up.IDubigeo as CoPais,up.Descripcion as DescPaisCliente, c.RazonComercial as DescCliente  
   FROM COTICAB CC --Left Join DEBIT_MEMO db On db.IDCab = CC.IDCAB                   
    Inner Join MACLIENTES c On CC.IDCliente=c.IDCliente  
   Left Join MAUBIGEO uc On C.IDCiudad=uc.IDubigeo  
   Left Join MAUBIGEO up On uc.IDubigeo=up.IDubigeo  
  
   WHERE       
   
  c.IDCliente not in ('000054') AND
   
   cc.CoTipoVenta='RC' AND CC.Estado='A'       
   or             
   (cc.Estado='X' and cc.IDCAB in             
   (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG') )      
        
   --And ISNULL(db.IDEstado,'') <> 'AN'          
   ) AS X      
 WHERE x.FecOutPeru      
 --BETWEEN '01/01/2015' AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/2015',103) as datetime)))--'31/12/2014 23:59:59'      
 BETWEEN '01/01/'+@Anio/*'2015'*/ AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/'+@Anio/*2015'*/,103) as datetime)))--'31/12/2014 23:59:59'      
 ) AS Y  
 ) AS Z  
 
 
 SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
  --Case When ImporteUSD<>0 Then      
  --((ImporteUSD-ImporteUSDAnioAnter)*100)/ImporteUSD      
  --Else      
  --100      
  --End as VarImporteUSD,      
 Case When SumaProy<>0 Then      
 ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Else      
 100      
 End as VarImporteUSD,     
    
   SumaProy --=ROUND(SumaProy,0)    
 INTO #VentasReceptivoMensual_Sin_APT   
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Sin_APT,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy) as SumaProy  
 FROM       
 #VentasReceptivoMensualDet_Sin_APT AS Y         
 GROUP BY FecOutPeru       
 ) AS Z      
 
 -------------
 
  SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
 
 Case When SumaProy<>0 Then      
 ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Else      
 100      
 End as VarImporteUSD,     
    
   SumaProy --=ROUND(SumaProy,0)    
 INTO #VentasReceptivoMensual_Sin_APT_SinPorc   
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Sin_Porc_Sin_APT,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy2) as SumaProy  
 FROM       
 #VentasReceptivoMensualDet_Sin_APT AS Y         
 GROUP BY FecOutPeru       
 ) AS Z   

 --------------
 Declare @FecOutPeru1 char(6),@ImporteUSD1 numeric(12,2),@ImporteUSDAnioAnter1 numeric(12,2),       
 @ImporteMesUSD1 numeric(12,2)=0,@ImporteMesUSDAnioAnter1 numeric(12,2)=0,      
 @tiCont1 tinyint=1     
    
 --Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,ImporteUSD,ImporteUSDAnioAnter       
 Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,SumaProy,ImporteUSDAnioAnter       
 From #VentasReceptivoMensual_Sin_APT Order by cast(FecOutPeru as int)      
 Open curVentasReceptivoMensual      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru1,@ImporteUSD1,@ImporteUSDAnioAnter1       
 While @@FETCH_STATUS=0      
 Begin      
 Set @ImporteMesUSD1+=@ImporteUSD1      
 Set @ImporteMesUSDAnioAnter1+=@ImporteUSDAnioAnter1     
 If @tiCont1=1      
 Select @FecOutPeru1 as FecOutPeru, @ImporteMesUSD1 as ImporteMesUSD, @ImporteMesUSDAnioAnter1 as ImporteMesUSDAnioAnter      
 Into #VentasReceptivoMensualAcumulado_SIN_APT      
 Else      
 Insert into #VentasReceptivoMensualAcumulado_SIN_APT      
 Values(@FecOutPeru1, @ImporteMesUSD1, @ImporteMesUSDAnioAnter1)      
      
 Set @tiCont1+=1      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru1,@ImporteUSD1,@ImporteUSDAnioAnter1       
 End      
      
 Close curVentasReceptivoMensual      
 Deallocate curVentasReceptivoMensual    
 
 
 
 -------------FIN SIN APT-----------------------      
       
       
 PRINT 'HOJA ACUMULADO'      
   
 SELECT FecInicio,  
   FecOut,  
   FecOutPeru,  
   IDCAB,  
   IDFile,  
   Titulo,  
   --ValorVenta=ISNULL(ValorVenta2,0),  
   ValorVenta=round(case when FecInicio<GETDATE() /*pasado*/  
   then  
    (  
     --isnull(TotalDM,0)  
	 case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end
    )  
   else --futuro  
    (  
     ISNULL(ValorVenta2,0)  
    )  
   end,0),  
   PoConcretVta,  
   NroPax,  
   ESTADO,  
   IDUsuario,  
   Responsable,  
   IDCliente,  
   CoPais,  
   DescPaisCliente,  
   DescCliente,  
   --MargenGan=ISNULL(MargenGan,0),  
   --Utilidad=ISNULL(ValorVenta2*(MargenGan/100),0)--,  

    --MargenGan= case when FecInicio<GETDATE()
	MargenGan= case when FecOut<GETDATE()
			   then
				(((case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado)/(case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end ))*100
			   else
				ISNULL(MargenGan,0)  
			   end,
				
   --Utilidad= case when FecInicio<GETDATE()
   Utilidad= case when FecOut<GETDATE()
			   then
				  (case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )-Total_Programado
			   else
				  ISNULL(ValorVenta2*(MargenGan/100),0)
			   end,
    
    --Total_Proy=case when FecInicio<GETDATE() /*pasado*/  
	Total_Proy=case when FecOut<GETDATE() /*pasado*/  
   then  
    (  
     case when PoConcretVta<100 then ISNULL(ValorVenta2,0)*(PoConcretVta/100) else 0 end  
    )  
   else --futuro  
    (  
     case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then 0 else TotalCotiz end  
    )  
   end  
     
   --, Total_Concret=case when FecInicio<GETDATE() /*pasado*/  
   , Total_Concret=case when FecOut<GETDATE() /*pasado*/  
   then  
    (  
     case when PoConcretVta=100 then ISNULL(ValorVenta2,0) else 0 end  
    )  
   else --futuro  
    (  
     case when (TotalDM-(TotalCotiz-TotalCotiz_Porc))>0 then TotalDM else 0 end  
    )  
   end  
  
  --,  SumaProy=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
  ,  SumaProy=round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
  --,  SumaProy2=round(( round((case when FecInicio<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
  ,  SumaProy2=round(( round((case when FecOut<GETDATE() then ( case when isnull(TotalDM,0)=0 then ISNULL(ValorVenta2,0) else TotalDM end )  else (ISNULL(ValorVenta2,0)) end),0)),0)  
--,  SumaProy=round(( round((case when FecInicio<GETDATE() then (isnull(TotalDM,0))  else (ISNULL(ValorVenta2,0)) end),0)* poconcretvta)/100,0)  
      ,Dias
   ,Edad_Prom
   ,Total_Programado
 INTO #VentasReceptivoMensualDet  
 FROM  
 (     
 SELECT Y.*,  
  
  ISNULL(Y.ValorVenta*(Y.MargenGan/100),0) AS Utilidad  
  ,ValorVenta2=case when Y.TotalDM>0 then (CASE WHEN ABS(Y.TotalDM-Y.TotalCotiz)>=Y.TotalDM_Porc THEN Y.TotalCotiz ELSE Y.TotalDM END) else Y.TotalCotiz end  
    
  --INTO #VentasReceptivoMensualDet  
 FROM     
 (  
   SELECT       
   FecInicio,  
   FecOutPeru as FecOut,      
   Left(CONVERT(varchar,FecOutPeru,112),6) AS FecOutPeru,      
   IDCAB, IDFile, Titulo,     
   --isnull((Select sum(total*nropax) From Cotidet Where idcab=X.idcab),0) as ValorVenta       
    
   Case When EstadoFacturac='F' Then X.TotalDM    
   Else  --total de cotizacion  
     Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
       isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)  + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,0)   
      Else    
		--X.TotalSimple+X.TotalDoble+X.TotalTriple              
			case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
      End    
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
     End     
   End 
   as ValorVenta ,    
     
   PoConcretVta,  
   --EsTotalxPax,ExisteHotel,       
   --NroPax,  
   NroPax=(NroPax+isnull(NroLiberados,0)),  
   Estado ,  
   IDUsuario,  
   Responsable=(select top 1 u.Usuario from MAUSUARIOS u where u.IDUsuario=X.IDUsuario)  
   , IDCliente, CoPais,  DescPaisCliente,  DescCliente,  
        
   case when X.TotalDobleNeto = 0 then 0                                                
   else                                                 
   CAST(ROUND(((X.TotalDobleNeto - X.TotalImpto - X.TotalCostoReal - X.TotalLib) /X.TotalDobleNeto) * 100,2) as Numeric(10,2))                                                 
   End as MargenGan       
        
   ,TotalDM=isnull(X.TotalDM,0)  
   ,TotalDM_Porc=X.TotalDM*0.15  
   ,TotalCotiz=  Case When ExisteHotel=1 Then    
      Case When EsTotalxPax=1 Then    
    isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
  Else    
		--X.TotalSimple+X.TotalDoble+X.TotalTriple              
			case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
      End    
     Else    
    -- X.NroPax * X.TotalCotiz       
    --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
      ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
     End     
       
   ,TotalCotiz_Porc=  (Case When ExisteHotel=1 Then    
          Case When EsTotalxPax=1 Then    
				isnull((Select sum(total) From COTIPAX Where idcab=X.idcab),0)     
          Else    
				--X.TotalSimple+X.TotalDoble+X.TotalTriple              
					case when (X.TotalSimple+X.TotalDoble+X.TotalTriple)>0 then 
					X.TotalSimple+X.TotalDoble+X.TotalTriple 
					else ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1) end
          End    
         Else    
 -- X.NroPax * X.TotalCotiz       
        --(X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz
        ((X.NroPax+isnull(X.NroLiberados,0)) * X.TotalCotiz) + dbo.FnDevuelveMontoTotalxPaxVuelos(X.IDCAB,1)
         End     
       )*0.15  
      
      ,Dias
   ,Edad_Prom
   ,Total_Programado
   FROM       
   (      
   SELECT CC.*,      
   (Select SUM(Total) From DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN')
   + Case When Not Exists(select IDDebitMemo from DEBIT_MEMO Where IDCab=cc.idcab and IDEstado<>'AN') Then dbo.FnDevuelveMontoTotalxPaxVuelos(cc.IDCAB,1) Else 0 End as TotalDM,    
   --isnull((Select Max(Dia) From COTIDET cd1        
   --Inner Join MAUBIGEO u1 On cd1.IDubigeo=u1.IDubigeo                 
   --And u1.IDPais='000323'                
   --Left Join MASERVICIOS_DET SD1 On cd1.IDServicio_Det=SD1.IDServicio_Det                                                  
   --Where IDCAB=cc.IDCAB /*And SD1.idTipoOC<>'005'*/),cc.FechaOut) as FecOutPeru,      
     
   cc.FechaOut as FecOutPeru,      
   ((select SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) *((Isnull(cc.Twin,0)+Isnull(cc.Matrimonial,0))) *2) + dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,1) as TotalDoble,
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * Isnull(cc.Simple,0) as TotalSimple,                                                    
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = cc.IDCAB) * (Isnull(cc.Triple,0)*3) as TotalTriple,      
   (select ISNULL(SUM(Total),0) from COTIDET cd where cd.IDCAB = CC.IDCAB) As TotalCotiz, --As TotalCotiz,
   dbo.FnEsTotalxPax(CC.IDCAB) as EsTotalxPax,    
   case when Exists(select IDCab from COTIDET cd Left Join MAPROVEEDORES p On cd.IDProveedor = p.IDProveedor                         
   Where cd.IDCAB = CC.IDCAB and p.IDTipoProv = '001') then 1 Else 0 End as ExisteHotel,      
        
   (select SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB)+ dbo.FnDevuelveMontoTotalxPaxVuelos(CC.IDCAB,0) as TotalDobleNeto, --as TotalDobleNeto,      
   (select SUM(SSTotal)+SUM(Total) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalSimpleNeto,--as TotalSimpleNeto,      
   (select SUM(Total)+SUM(STTotal) from COTIDET cd Where cd.IDCAB = CC.IDCAB) as TotalTripleNeto,--as TotalTripleNeto,
   (select SUM(TotImpto) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalImpto,      
   (select SUM(CostoReal) from COTIDET cd where cd.IDCAB = CC.IDCAB) as TotalCostoReal,      
   (select SUM(CostoLiberado) from COTIDET cd where cd.IDCAB = cc.IDCAB) as TotalLib,  
   up.IDubigeo as CoPais,up.Descripcion as DescPaisCliente, c.RazonComercial as DescCliente  
   
   ,Dias=DATEDIFF(day,FecInicio,FechaOut)+1
   ,Edad_Prom=isnull(dbo.FnEdadPromedio_Pax(cc.IDCab),0)
   --,Total_Programado=[dbo].[FnDevolverTotalProgramacion_Coti](cc.IDCAB,'USD')
   ,Total_Programado=dbo.FnDevolverTotal_Preliquidacion_CotiCab(cc.IDCAB,'')
   FROM COTICAB CC --Left Join DEBIT_MEMO db On db.IDCab = CC.IDCAB                 
    Inner Join MACLIENTES c On CC.IDCliente=c.IDCliente  
   Left Join MAUBIGEO uc On C.IDCiudad=uc.IDubigeo  
   Left Join MAUBIGEO up On uc.IDubigeo=up.IDubigeo  
  
   WHERE       
   cc.CoTipoVenta='RC' AND CC.Estado='A'       
   or             
   (cc.Estado='X' and cc.IDCAB in             
   (select distinct IDCAB from DEBIT_MEMO where IDCab=cc.IDCAB and IDEstado='PG') )      
        
   --And ISNULL(db.IDEstado,'') <> 'AN'          
   ) AS X      
 WHERE x.FecOutPeru   
 --BETWEEN '01/01/2015' AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/2015',103) as datetime)))--'31/12/2014 23:59:59'      
 BETWEEN '01/01/'+@Anio/*'2015'*/ AND dateadd(S,-1,dateadd(d,1,cast(CONVERT(varchar,'31/12/'+@Anio/*2015'*/,103) as datetime)))--'31/12/2014 23:59:59'      
 ) AS Y  
 ) AS Z  
 --Select * from #VentasReceptivoMensualDet          
 --Select * from #VentasReceptivoMensual      
 --Select idfile,idproveedor,COUNT(*) from #VentasReceptivoMensualDet group by idfile,idproveedor order by 3 desc      
      
    --prueba  
   -- select 'a1',* from #VentasReceptivoMensualDet    
    --fin prueba  
      
 SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
  --Case When ImporteUSD<>0 Then      
  --((ImporteUSD-ImporteUSDAnioAnter)*100)/ImporteUSD      
  --Else      
  --100      
  --End as VarImporteUSD,      
 Case When SumaProy<>0 Then      
 ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Else      
 100      
 End as VarImporteUSD,      
    
 CantPax,        
 CantPaxAnioAnter,      
 Case When CantPax<>0 Then      
 ((CantPax-CantPaxAnioAnter)*100)/CantPax      
 Else      
 100      
 End as VarCantPax,      
 CantFiles,        
 CantFilesAnioAnter,      
 Case When CantFiles<>0 Then      
 ((CantFiles-CantFilesAnioAnter)*100)/CantFiles      
 Else      
 100      
 End as VarCantFiles,  
  SumaProy --=ROUND(SumaProy,0) 
 ,Dias_Dif
 ,Edad_Prom   
 INTO #VentasReceptivoMensual      
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy) as SumaProy  
 ,AVG(dias) as Dias_Dif
 --,AVG(y.edad_prom) as Edad_Prom
 ,isnull(AVG(CASE WHEN y.edad_prom <> 0 THEN y.edad_prom ELSE NULL END),0) as Edad_Prom
 --AVG (CASE WHEN Value <> 0 THEN Value ELSE NULL END)
 FROM       
 #VentasReceptivoMensualDet AS Y       
 GROUP BY FecOutPeru       
 ) AS Z      
  
  
  --ventas mensual sin porcentaje concretizacion
  SELECT FecOutPeru, ImporteUSD,      
 ImporteUSDAnioAnter,      
    
 Case When SumaProy<>0 Then      
 ((SumaProy-ImporteUSDAnioAnter)*100)/SumaProy      
 Else      
 100      
 End as VarImporteUSD,      
    
 CantPax,        
 CantPaxAnioAnter,      
 Case When CantPax<>0 Then      
 ((CantPax-CantPaxAnioAnter)*100)/CantPax      
 Else      
 100      
 End as VarCantPax,      
 CantFiles,        
 CantFilesAnioAnter,      
 Case When CantFiles<>0 Then      
 ((CantFiles-CantFilesAnioAnter)*100)/CantFiles      
 Else      
 100      
 End as VarCantFiles,  
  SumaProy --=ROUND(SumaProy,0) 
 ,Dias_Dif
 ,Edad_Prom   
 INTO #VentasReceptivoMensual_Sin_Porc      
 FROM      
 (       
 SELECT FecOutPeru,SUM(Y.ValorVenta) as ImporteUSD,      
 isnull((Select isnull(SsImporte_Sin_Porc,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0) as ImporteUSDAnioAnter,       
 SUM(Y.NroPax) as CantPax,   
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And     
 CoMes=right(y.FecOutPeru,2)),0)  as CantPaxAnioAnter,       
 COUNT(*) as CantFiles,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
 CoMes=right(y.FecOutPeru,2)),0)  as CantFilesAnioAnter ,  
 sum(SumaProy2) as SumaProy  
 ,AVG(dias) as Dias_Dif
 --,AVG(y.edad_prom) as Edad_Prom
 ,isnull(AVG(CASE WHEN y.edad_prom <> 0 THEN y.edad_prom ELSE NULL END),0) as Edad_Prom
 --AVG (CASE WHEN Value <> 0 THEN Value ELSE NULL END)
 FROM       
 #VentasReceptivoMensualDet AS Y       
 GROUP BY FecOutPeru       
 ) AS Z          
      
--JORGE--      
      
--SELECT 'AA',* FROM #VentasReceptivoMensualDet      
--      
 select * from #VentasReceptivoMensual  order by FecOutPeru
      
 PRINT 'VENTA ANUAL USD'      
      
 SELECT IDUsuario,Nombre, VentaAnualUSD, VentaAnualUSDAnioAnter,       
 Case When VentaAnualUSD<>0 then      
 ((VentaAnualUSD-VentaAnualUSDAnioAnter)*100)/VentaAnualUSD      
 Else      
 100      
 End as VarVentaAnualUSD,CantFilesAnioAnter       
 INTO #VentasporEjecutivoAnio      
 FROM      
 (      
 --SELECT D.IDUsuario,U.Nombre, SUM(D.VALORVENTA) AS VentaAnualUSD,       
 SELECT D.IDUsuario,U.Nombre, SUM(D.SumaProy) AS VentaAnualUSD,       
isnull((Select isnull(v1.SsImporte,0) From RPT_VENTA_EJECUTIVO v1 Where v1.NuAnio=@AnioAnter/*'2014'*/ And v1.CoUserEjecutivo=D.IDUsuario),0) AS VentaAnualUSDAnioAnter ,      
 isnull((Select isnull(v1.QtNumFiles,0) From RPT_VENTA_EJECUTIVO v1 Where v1.NuAnio=@AnioAnter/*'2014'*/ And v1.CoUserEjecutivo=D.IDUsuario),0) AS CantFilesAnioAnter       
 FROM #VentasReceptivoMensualDet D       
 INNER JOIN MAUSUARIOS U ON D.IDUSUARIO=U.IDUsuario      
 AND ((IdArea='VS' AND Nivel='VTA') OR (IdArea='OP' AND Nivel='OPE') or Nivel='GER')       
 AND Activo='A' AND U.Siglas<>'MST'      
 group by D.IDUsuario,U.Nombre       
 ) as X      
 order by 2 desc      
      
      
      
 --select * from #VentasReceptivoMensualdet      
      
      
 Declare @FecOutPeru char(6),@ImporteUSD numeric(12,2),@ImporteUSDAnioAnter numeric(12,2),       
 @ImporteMesUSD numeric(12,2)=0,@ImporteMesUSDAnioAnter numeric(12,2)=0,      
 @tiCont tinyint=1     
    
 --Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,ImporteUSD,ImporteUSDAnioAnter       
 Declare curVentasReceptivoMensual Cursor For Select FecOutPeru,SumaProy,ImporteUSDAnioAnter       
 From #VentasReceptivoMensual Order by cast(FecOutPeru as int)      
 Open curVentasReceptivoMensual      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru,@ImporteUSD,@ImporteUSDAnioAnter       
 While @@FETCH_STATUS=0      
 Begin      
 Set @ImporteMesUSD+=@ImporteUSD      
 Set @ImporteMesUSDAnioAnter+=@ImporteUSDAnioAnter      
 If @tiCont=1      
 Select @FecOutPeru as FecOutPeru, @ImporteMesUSD as ImporteMesUSD, @ImporteMesUSDAnioAnter as ImporteMesUSDAnioAnter      
 Into #VentasReceptivoMensualAcumulado      
 Else      
 Insert into #VentasReceptivoMensualAcumulado      
 Values(@FecOutPeru, @ImporteMesUSD, @ImporteMesUSDAnioAnter)      
      
 Set @tiCont+=1      
 Fetch Next From curVentasReceptivoMensual Into @FecOutPeru,@ImporteUSD,@ImporteUSDAnioAnter       
 End      
      
 Close curVentasReceptivoMensual      
 Deallocate curVentasReceptivoMensual      
      
 select * From #VentasReceptivoMensualAcumulado Order by cast(FecOutPeru as int)      
      
 --Hoja Turnover      
 PRINT 'HOJA Turnover'      
      
      
 Select Anio,  
 IDCliente,  
 DescCliente,  
 ImporteUSD,  
 ImporteUSDAnioAnter,     
 /* 
 Case When ImporteUSD<>0 Then      
 ((ImporteUSD-ImporteUSDAnioAnter)*100)/ImporteUSD      
 Else      
 100      
 End as Crecimiento,
 */

 -- Case When ImporteUSDAnioAnter<>0 Then            
	--case when ImporteUSD>=ImporteUSDAnioAnter
	--then
	--	((ImporteUSD)*100)/ImporteUSDAnioAnter      
	--else
	--	(((ImporteUSDAnioAnter)*100)/ImporteUSD/*-100*/)*-1
	--end     
 --Else      
 --0    
 --End as Crecimiento,

   Case When ImporteUSDAnioAnter<>0 Then            
	case when ImporteUSD>=ImporteUSDAnioAnter
	then
		(((ImporteUSD)*100)/ImporteUSDAnioAnter)-100      
	else
		((((ImporteUSDAnioAnter)*100)/ImporteUSD/*-100*/)*-1)+100
	end     
 Else      
 0    
 End as Crecimiento,

 CantFiles,       
 CantFilesAnioAnter,
 
 TicketFileAnioAnter= CASE WHEN CantFilesAnioAnter=0 THEN 0 ELSE ImporteUSDAnioAnter/CantFilesAnioAnter END ,
 
 PromMargen,       
 PromMargenAnioAnter,       
      
 (PromMargen /100) * ImporteUSD as Utilidad,      
 (PromMargenAnioAnter /100) * ImporteUSDAnioAnter as UtilidadAnioAnter      
 ,Dias
 ,Edad_Prom
      
 into #VentasxCliente_Anual      
 From      
 (      
 select LEFT(fecoutperu,4) as Anio,CUR.IDCliente,      
 Case When ISNULL(c.RazonComercial,'')='' Then c.RazonSocial Else c.RazonComercial  End as DescCliente,       
 --SUM(cur.ValorVenta) as ImporteUSD,      
 SUM(cur.SumaProy) as ImporteUSD,      
 --SUM(cur.ValorVenta)+100 as ImporteUSDAnioAnter ,      
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter/*'2014'*/ And 
 CoCliente=cur.IDCliente),0) as ImporteUSDAnioAnter,       
      
 COUNT(*) as CantFiles,      
 
 CantFilesAnioAnter=isnull((SELECT distinct sum(QtNumFiles) FROM RPT_VENTA_CLIENTES where NuAnio=@AnioAnter And CoCliente=cur.IDCliente),0),
  
 isnull(AVG(ISNULL(MargenGan,0)),0) as PromMargen,      
 --isnull(AVG(MargenGan),0)+10 as PromMargenAnioAnter      
 isnull((Select isnull(SsPromedio,0) From RPT_VENTA_CLIENTES Where NuAnio= @AnioAnter/*'2014'*/ And       
 CoCliente=cur.IDCliente),0) as PromMargenAnioAnter      
 
 ,AVG(Dias) as Dias,
 --AVG(Edad_Prom) as Edad_Prom
isnull(AVG (CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0) as Edad_Prom
      
 From #VentasReceptivoMensualDet cur Inner Join MACLIENTES c On cur.IDCliente=c.IDCliente      
 group by cur.IDCliente,LEFT(cur.fecoutperu,4),c.RazonSocial,c.RazonComercial      
 ) as X      
 order by 3      
      
 select * from #VentasxCliente_Anual order by 4 desc     
      
 --HOJA POR EJECUTIVA      
 PRINT 'HOJA POR EJECUTIVA'      
      
 --SELECT D.IDUsuario,U.Nombre,D.FecOutPeru,COUNT(*) as CantFiles,SUM(VALORVENTA) AS VentaAnualUSD     
 SELECT D.IDUsuario,U.Nombre,D.FecOutPeru,COUNT(*) as CantFiles,SUM(SumaProy) AS VentaAnualUSD     
 ,Prom_Margen=isnull(AVG (CASE WHEN MargenGan <> 0 THEN MargenGan ELSE NULL END),0) 
 Into #VentasporEjecutivoMes      
 FROM #VentasReceptivoMensualDet D       
 INNER JOIN MAUSUARIOS U ON D.IDUsuario=U.IDUsuario     
 --AND ((IdArea='VS' AND Nivel='VTA') OR (IdArea='OP' AND Nivel='OPE') or Nivel='GER')       
 AND ((IdArea='VS' AND Nivel in ('VTA','SVE','JAR')) OR (IdArea='OP' AND Nivel='OPE') or Nivel='GER')       
 AND Activo='A' AND U.Siglas<>'MST'      
 group by D.IDUSUARIO,U.Nombre, D.FecOutPeru      
 Order by U.Nombre, cast(D.FecOutPeru as int)      
      
   --select 'a',* from #VentasporEjecutivoMes   
      
 Create Table #VentasporEjecutivoMes2(      
 IDUsuario char(4),      
 NoEjecutiva varchar(60),      
 EneCantFiles tinyint,      
 EneVentaMes numeric(10,2),      
 FebCantFiles tinyint,      
 FebVentaMes numeric(10,2),       
 MarCantFiles tinyint,      
 MarVentaMes numeric(10,2),      
 AbrCantFiles tinyint,      
 AbrVentaMes numeric(10,2),      
 MayCantFiles tinyint,      
 MayVentaMes numeric(10,2),       
 JunCantFiles tinyint,      
 JunVentaMes numeric(10,2),      
JulCantFiles tinyint,      
 JulVentaMes numeric(10,2),      
 AgoCantFiles tinyint,      
 AgoVentaMes numeric(10,2),      
 SetCantFiles tinyint,      
 SetVentaMes numeric(10,2),      
 OctCantFiles tinyint,      
 OctVentaMes numeric(10,2),      
 NovCantFiles tinyint,      
 NovVentaMes numeric(10,2),      
 DicCantFiles tinyint,      
 DicVentaMes numeric(10,2))      
      
      
 Declare @AnioMes char(6), @tiContMes as tinyint, @IDUsuario char(4)      
      
 Declare curEjecutivas cursor for      
 Select distinct IDUsuario From #VentasporEjecutivoMes       
      
 Open curEjecutivas      
 Fetch Next From curEjecutivas Into @IDUsuario      
 While @@FETCH_STATUS=0      
 Begin      
 Set @tiContMes = 1      
 While (@tiContMes<=12)      
 Begin      
 If @tiContMes<10      
  Set @AnioMes=@Anio+'0'+CAST(@tiContMes as CHAR(1))      
 Else      
  Set @AnioMes=@Anio+CAST(@tiContMes as CHAR(2))      
 IF @tiContMes=1      
   
   
  begin  
  If Not Exists(Select Nombre From #VentasporEjecutivoMes Where IDUsuario=@IDUsuario and FecOutPeru=@AnioMes)  
      Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, EneCantFiles,EneVentaMes)      
     Select distinct IDUsuario,Nombre,0,0.00 From #VentasporEjecutivoMes Where IDUsuario=@IDUsuario    
  else  
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, EneCantFiles,EneVentaMes)      
     Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes      
      And IDUsuario=@IDUsuario    
  end  
    
  /*  
     Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, EneCantFiles,EneVentaMes)      
     Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes      
      And IDUsuario=@IDUsuario    
  */      
 IF @tiContMes=2      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, FebCantFiles,FebVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
   --print'febrero'     
   Update #VentasporEjecutivoMes2      
   Set FebCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   FebVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=3      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, MarCantFiles,MarVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
   --print'febrero'      
   Update #VentasporEjecutivoMes2      
   Set MarCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   MarVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=4      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva,AbrCantFiles,AbrVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
   --print'febrero'      
   Update #VentasporEjecutivoMes2      
   Set AbrCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   AbrVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=5      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, MayCantFiles,MayVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
         
   Update #VentasporEjecutivoMes2      
   Set MayCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   MayVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=6      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, JunCantFiles,JunVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
   --print'febrero'      
   Update #VentasporEjecutivoMes2      
   Set JunCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   JunVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=7      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, JulCantFiles,JulVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
   --print'febrero'      
   Update #VentasporEjecutivoMes2      
   Set JulCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   JulVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=8      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
 IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, AgoCantFiles,AgoVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
         
   Update #VentasporEjecutivoMes2      
   Set AgoCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   AgoVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=9      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, SetCantFiles,SetVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
         
   Update #VentasporEjecutivoMes2      
   Set SetCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   SetVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=10      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, OctCantFiles,OctVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
         
   Update #VentasporEjecutivoMes2      
   Set OctCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   OctVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=11      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva,NovCantFiles,NovVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
         
   Update #VentasporEjecutivoMes2      
   Set NovCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   NovVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
 IF @tiContMes=12      
  Begin      
  If Not Exists(Select NoEjecutiva From #VentasporEjecutivoMes2 Where       
    IDUsuario=@IDUsuario)      
   Insert Into #VentasporEjecutivoMes2(IDUsuario,NoEjecutiva, DicCantFiles,DicVentaMes)      
   Select IDUsuario,Nombre,CantFiles,VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario      
  Else      
         
   Update #VentasporEjecutivoMes2      
Set DicCantFiles=isnull((Select CantFiles From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0),      
   DicVentaMes=isnull((Select VentaAnualUSD From #VentasporEjecutivoMes Where FecOutPeru=@AnioMes And IDUsuario=@IDUsuario),0)      
   Where IDUsuario=@IDUsuario      
  End      
        
 Set @tiContMes+=1      
 End        
      
 Fetch Next From curEjecutivas Into @IDUsuario      
 End      
 Close curEjecutivas      
 Deallocate curEjecutivas      
      
      
 Update #VentasporEjecutivoMes2 Set EneCantFiles=ISNULL(EneCantFiles,0),      
 EneVentaMes=isnull(EneVentaMes,0),      
 FebCantFiles=isnull(FebCantFiles,0),      
 FebVentaMes=isnull(FebVentaMes,0),      
 MarCantFiles=isnull(MarCantFiles,0),      
 MarVentaMes=isnull(MarVentaMes,0),      
 AbrCantFiles=isnull(AbrCantFiles,0),      
 AbrVentaMes=isnull(AbrVentaMes,0),      
 MayCantFiles=isnull(MayCantFiles,0),      
 MayVentaMes=isnull(MayVentaMes,0),      
 JunCantFiles=isnull(JunCantFiles,0),      
 JunVentaMes=isnull(JunVentaMes,0),      
 JulCantFiles=isnull(JulCantFiles,0),      
 JulVentaMes=isnull(JulVentaMes,0),      
 AgoCantFiles=isnull(AgoCantFiles,0),      
 AgoVentaMes=isnull(AgoVentaMes,0),      
 SetCantFiles=isnull(SetCantFiles,0),      
 SetVentaMes =isnull(SetVentaMes,0),      
 OctCantFiles =isnull(OctCantFiles,0),      
 OctVentaMes =isnull(OctVentaMes,0),      
 NovCantFiles =isnull(NovCantFiles,0),      
 NovVentaMes =isnull(NovVentaMes,0),      
 DicCantFiles =isnull(DicCantFiles,0),      
 DicVentaMes =isnull(DicVentaMes,0)     
      
      
 --Select * From #VentasporEjecutivoMes    
 Select * From #VentasporEjecutivoMes2 order by NoEjecutiva      
      
 --Select * From #VentasReceptivoMensualDet      
 --Select * From #VentasReceptivoMensual       
 --SELECT * FROM RPT_VENTA_EJECUTIVO    
 --SELECT * FROM RPT_VENTA_APT      
 Declare @SumTotal numeric(13,2)=(Select sum(VentaAnualUSD) From #VentasporEjecutivoMes)      
 Declare @SumTotalAnioAnter numeric(13,2)=(Select sum(VentaAnualUSDAnioAnter) From #VentasporEjecutivoAnio)     
 --SELECT @SumTotal      
      
 SELECT *,      
 isnull(Case When @SumTotalAnioAnter<>0 Then (TotVendidoAnioAnter*100)/@SumTotalAnioAnter Else 0 End,0) as ParticipacionAnioAnter,
 VarEjecutivos=isnull((Case When TotVendido<>0 Then      
				 ((TotVendido-TotVendidoAnioAnter)*100)/TotVendido      
				 Else      
				 0
				end
				),0)
 FROM      
 (      
 Select *,      
	Case When @SumTotal<>0 Then(TotVendido*100)/@SumTotal Else 0 End as Participacion,    

 isnull((Select CantFilesAnioAnter From #VentasporEjecutivoAnio Where IDUsuario=X.IDUsuario),0) as CantFilesAnioAnter,      

 isnull((Select VentaAnualUSDAnioAnter From #VentasporEjecutivoAnio Where IDUsuario=X.IDUsuario),0) as TotVendidoAnioAnter       

 From      
 (      
 Select IDUsuario,Nombre as Ejecutivo,SUM(CantFiles) as CantFiles, SUM(VentaAnualUSD) as TotVendido    
 ,Prom_Margen=isnull(AVG (CASE WHEN Prom_Margen <> 0 THEN Prom_Margen ELSE NULL END),0)    
 From #VentasporEjecutivoMes      
 Group by idusuario,Nombre      
 ) as X      
 ) as Y      
 Order by 2--3 desc      
      
      
 --select * from #VentasporEjecutivoAnio      
      
      
      
      
      
 --select * from #VentasReceptivoMensual      
      
 --select * from #VentasReceptivoMensualAcumulado      
      
 --select * from #VentasReceptivoMensualDET      
      
 --select * from #VentasporEjecutivoMes      
 --select * from #VentasporEjecutivoMes2      
 --select * from #VentasporEjecutivoAnio      
      
 --HOJA COMPARATIVO      
 PRINT 'HOJA COMPARATIVO'      
      
 Select FecOutPeru,  
 SumaProy as ImporteUSD,  
 ImporteUSDAnioAnter,      
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter1,       
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter2,       
 isnull((Select isnull(SsImporte,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter3     
 From #VentasReceptivoMensual X  Order by cast(FecOutPeru as int)           
      
 Select FecOutPeru,CantPax, CantPaxAnioAnter,      
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter1,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter2,       
 isnull((Select isnull(QtPax,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
 CoMes=right(X.FecOutPeru,2)),0) as ImporteUSDAnioAnter3       
 From #VentasReceptivoMensual X   Order by cast(FecOutPeru as int)            
       
 --CANT FILES      
 Select FecOutPeru,CantFiles, CantFilesAnioAnter,      
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
 CoMes=right(X.FecOutPeru,2)),0) as CantFilesAnioAnter1,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
 CoMes=right(X.FecOutPeru,2)),0) as CantFilesAnioAnter2,       
 isnull((Select isnull(QtNumFiles,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
 CoMes=right(X.FecOutPeru,2)),0) as CantFilesAnioAnter3       
 From #VentasReceptivoMensual X   Order by cast(FecOutPeru as int)            
      
 --top 15 clientes      
       
 select top 15 IDCliente,      
    DescCliente,      
    ImporteUSD,      
    ImporteUSDAnioAnter,      
    isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter-1 And       
    CoCliente=#VentasxCliente_Anual.IDCliente),0) as ImporteUSDAnioAnter1,       
          
    isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter-2 And       
    CoCliente=#VentasxCliente_Anual.IDCliente),0) as ImporteUSDAnioAnter2,       
          
    isnull((Select isnull(SsImporte,0) From RPT_VENTA_CLIENTES Where NuAnio=@AnioAnter-3 And       
    CoCliente=#VentasxCliente_Anual.IDCliente),0) as ImporteUSDAnioAnter3      
          
 from #VentasxCliente_Anual      
 order by importeUSD desc      
    
 PRINT 'HOJA VENTAS POR CUENTA'      
    
 --Select CoPais,DescPaisCliente,IDCliente as CoCliente,DescCliente,FecOutPeru, SUM(ValorVenta) as ValorVenta  
 --Select CoPais,DescPaisCliente,IDCliente as CoCliente,DescCliente,FecOutPeru, SUM(SumaProy) as ValorVenta  --,AVG(Dias) as Dias_Dif,AVG(Edad_Prom) as Edad_Prom
 --Select CoPais,DescPaisCliente,IDCliente as CoCliente,DescCliente,FecOutPeru, SUM(ValorVenta) as ValorVenta  
 Select CoPais,DescPaisCliente,IDCliente as CoCliente,DescCliente,FecOutPeru, SUM(SumaProy) as ValorVenta  
 Into #VentasporPaisClienteTemp  
 From #VentasReceptivoMensualDet  
 group by CoPais,DescPaisCliente,IDCliente,DescCliente,FecOutPeru  
 order by DescPaisCliente,DescCliente,FecOutPeru  
   
  --select 'a',* from #VentasporPaisClienteTemp
   
 Create Table #VentasporClienteMes(      
 CoCliente char(6),      
 NoCliente varchar(80),       
 CoPais char(6),  
 NoPais varchar(60),       
 EneVentaMes numeric(10,2),       
 FebVentaMes numeric(10,2),        
 MarVentaMes numeric(10,2),       
 AbrVentaMes numeric(10,2),       
 MayVentaMes numeric(10,2),  
 JunVentaMes numeric(10,2),   
 JulVentaMes numeric(10,2),   
 AgoVentaMes numeric(10,2),   
 SetVentaMes numeric(10,2),   
 OctVentaMes numeric(10,2),   
 NovVentaMes numeric(10,2),   
 DicVentaMes numeric(10,2),  
 Total numeric(13,2)--,
 --Dias_Dif int,
 --Edad_Prom int
 )      
  
   --select 'b' 
     
 Declare @CoCliente char(6), @CoPais char(6)  
   
 Declare curClientes cursor for          
 Select Distinct CoPais,IDCliente From #VentasReceptivoMensualDet  
 Open curClientes  
 Fetch Next From curClientes Into @CoPais,@CoCliente      
 While @@FETCH_STATUS=0      
  Begin  
  Set @tiContMes = 1      
  While (@tiContMes<=12)      
   Begin  
   If @tiContMes<10      
    Set @AnioMes=@Anio+'0'+CAST(@tiContMes as CHAR(1))  
   Else      
    Set @AnioMes=@Anio+CAST(@tiContMes as CHAR(2))  
      
   IF @tiContMes=1          
    /*  
    Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,EneVentaMes)  
    Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta  
    From #VentasporPaisClienteTemp  
 Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
    */  
     BEGIN  
     If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente and FecOutPeru=@AnioMes)  
      Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,EneVentaMes)  
       Select distinct CoCliente,DescCliente,CoPais,DescPaisCliente,0.00
       From #VentasporPaisClienteTemp  
       Where /*FecOutPeru=@AnioMes And*/ CoPais=@CoPais And CoCliente=@CoCliente  
           
     Else  
             
      Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,EneVentaMes)  
      --Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
	  Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
      From #VentasporPaisClienteTemp
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
     
     End  
      
   IF @tiContMes=2  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,FebVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
     
    Else  
     Update #VentasporClienteMes Set FebVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End  
   IF @tiContMes=3  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
   Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,MarVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
     
Else  
     Update #VentasporClienteMes Set MarVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End  
      
   IF @tiContMes=4  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,AbrVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set AbrVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End  
      
   IF @tiContMes=5  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,MayVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set MayVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End          
      
  
   IF @tiContMes=6  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,JunVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set JunVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End  
      
   IF @tiContMes=7  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,JulVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set JulVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End          
      
   IF @tiContMes=8  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,AgoVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set AgoVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End  
      
   IF @tiContMes=9  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,SetVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set SetVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End           
      
   IF @tiContMes=10  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,OctVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set OctVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End  
      
   IF @tiContMes=11  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,NovVentaMes)  
 Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente  
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
    Else  
     Update #VentasporClienteMes Set NovVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End           
   IF @tiContMes=12  
    Begin  
    If Not Exists(Select CoCliente From #VentasporPaisClienteTemp Where CoPais=@CoPais And CoCliente=@CoCliente)  
     Insert Into #VentasporClienteMes(CoCliente,NoCliente,CoPais,NoPais,DicVentaMes)  
     Select CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta
     From #VentasporPaisClienteTemp  
     Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente 
        --  
      group by CoCliente,DescCliente,CoPais,DescPaisCliente,ValorVenta 
    Else  
     Update #VentasporClienteMes Set DicVentaMes=(Select ValorVenta From #VentasporPaisClienteTemp   
      Where FecOutPeru=@AnioMes And CoPais=@CoPais And CoCliente=@CoCliente)  
     Where CoPais=@CoPais And CoCliente=@CoCliente  
    End           
      
   Set @tiContMes+=1  
   End  
    
  Fetch Next From curClientes Into @CoPais,@CoCliente  
  End   
 Close curClientes  
 Deallocate curClientes  
   
 Update #VentasporClienteMes Set EneVentaMes=ISNULL(EneVentaMes,0),  
  FebVentaMes=ISNULL(FebVentaMes,0),  
  MarVentaMes=ISNULL(MarVentaMes,0),  
  AbrVentaMes=ISNULL(AbrVentaMes,0),  
  MayVentaMes=ISNULL(MayVentaMes,0),  
  JunVentaMes=ISNULL(JunVentaMes,0),  
  JulVentaMes=ISNULL(JulVentaMes,0),  
  AgoVentaMes=ISNULL(AgoVentaMes,0),  
  SetVentaMes=ISNULL(SetVentaMes,0),  
  OctVentaMes=ISNULL(OctVentaMes,0),  
  NovVentaMes=ISNULL(NovVentaMes,0),  
  dicVentaMes=ISNULL(dicVentaMes,0)  
   
 Update #VentasporClienteMes Set Total=EneVentaMes+FebVentaMes+MarVentaMes+AbrVentaMes+MayVentaMes+JunVentaMes+JulVentaMes+  
  AgoVentaMes+SetVentaMes+OctVentaMes+NovVentaMes+DicVentaMes  
    
      --WEB (UNITED STATES OF AMERICA)
 Select CoCliente,
 NoCliente=case when NoCliente='WEB (UNITED STATES OF AMERICA)' then 'WEB (USA)' when NoCliente='EVER BEST TRAVEL SERVICE (TOUCAN HOLIDAYS)' then 'EVER BEST TRAVEL SERVICE' else NoCliente END,
 CoPais,
 NoPais=case when nopais='UNITED STATES OF AMERICA' then 'USA' else NoPais END,
  EneVentaMes=ISNULL(EneVentaMes,0),  
  FebVentaMes=ISNULL(FebVentaMes,0),  
  MarVentaMes=ISNULL(MarVentaMes,0),  
  AbrVentaMes=ISNULL(AbrVentaMes,0),  
  MayVentaMes=ISNULL(MayVentaMes,0),  
  JunVentaMes=ISNULL(JunVentaMes,0),  
  JulVentaMes=ISNULL(JulVentaMes,0),  
  AgoVentaMes=ISNULL(AgoVentaMes,0),  
  SetVentaMes=ISNULL(SetVentaMes,0),  
  OctVentaMes=ISNULL(OctVentaMes,0),  
  NovVentaMes=ISNULL(NovVentaMes,0),  
  dicVentaMes=ISNULL(dicVentaMes,0),
  Total=  ISNULL(Total,0)
  From #VentasporClienteMes Order by NoPais, NoCliente  

  print 'HOJA FILES'  
   Select * 
 ,Usuario=isnull((select nombre from MAUSUARIOS where IDUsuario=#VentasReceptivoMensualDet.IDUsuario),0)
 ,Mes=case when month(fecout)=1 then 'January'
 when month(fecout)=2 then 'February'
 when month(fecout)=3 then 'March'
 when month(fecout)=4 then 'April'
 when month(fecout)=5 then 'May'
 when month(fecout)=6 then 'June'
 when month(fecout)=7 then 'July'
 when month(fecout)=8 then 'August'
 when month(fecout)=9 then 'September'
 when month(fecout)=10 then 'October'
 when month(fecout)=11 then 'November'
 when month(fecout)=12 then 'December'
 else ''
 end
  From #VentasReceptivoMensualDet  
  where (idusuario=@IDUser or @IDUser='')
  order by Usuario,FecOutPeru,FecOut,FecInicio  
 --Select * 
 -- From #VentasReceptivoMensualDet  
 -- where (idusuario=@IDUser or @IDUser='')
 -- order by FecOutPeru,FecOut,FecInicio  
  
    print 'SIN APT' 
  select * from #VentasReceptivoMensual_Sin_APT Order by cast(FecOutPeru as int)  
  
   print 'ACUMULADO SIN APT' 
   select * From #VentasReceptivoMensualAcumulado_Sin_APT Order by cast(FecOutPeru as int)      
 
   print 'COMPARATIVO EDADES' 
	 Select FecOutPeru,  
	 Edad_Prom ,  
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter,      
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter1,       
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter2,       
	 isnull((Select isnull(QtEdad_Prom,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Edad_PromAnter3       
	 From #VentasReceptivoMensual X  Order by cast(FecOutPeru as int)     
 
   print 'COMPARATIVO DIAS' 
 	 Select FecOutPeru,  
	 Dias_Dif ,  
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter,      
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-1 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter1,       
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-2 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter2,       
	 isnull((Select isnull(QtDias,0) From RPT_VENTA_APT Where NuAnio=@AnioAnter-3 And       
	 CoMes=right(X.FecOutPeru,2)),0) as Dias_DifAnter3       
	 From #VentasReceptivoMensual X   Order by cast(FecOutPeru as int)    
 
 
	print 'ventas x pais'
	 Select 
	 --Row_Number() over (Order By Total desc) As Posicion,
	 CoPais,
	 NoPais=DescPaisCliente,
	 CoRegion=(select CoRegion from dbo.MAUBIGEO where IDubigeo=CoPais),
	 --Total=SUM(ValorVenta),
	 Total=SUM(SumaProy),
	 --
	 Dias_Dif=AVG(Dias),
	 --Edad_Prom=AVG(Edad_Prom)
	 Edad_Prom=isnull(AVG (CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0),
	 Cant_Clientes=count(idcliente)
	 into #VentasporPais
	 From #VentasReceptivoMensualDet
	 group by  CoPais,DescPaisCliente
	 Order by  Total desc
	
	
	SELECT     MACLIENTES.IDCiudad,
		MAUBIGEO.Descripcion,
		MAUBIGEO.CoRegion,
		SsImporte=sum(RPT_VENTA_CLIENTES.SsImporte),
		QtDias=AVG(QtDias),
		QtEdad_Prom=isnull(AVG (CASE WHEN QtEdad_Prom <> 0 THEN QtEdad_Prom ELSE NULL END),0)--AVG(QtEdad_Prom)
		 ,Cant_Clientes=count(idcliente)
	into #VentasporPais_AnioAnter
		FROM         RPT_VENTA_CLIENTES left JOIN
							  MACLIENTES ON RPT_VENTA_CLIENTES.CoCliente = MACLIENTES.IDCliente left JOIN
							  MAUBIGEO ON MACLIENTES.IDCiudad = MAUBIGEO.IDubigeo
		WHERE     (RPT_VENTA_CLIENTES.NuAnio = '2014')
		group by MACLIENTES.IDCiudad,
		MAUBIGEO.Descripcion,
		MAUBIGEO.CoRegion
	order by sum(SsImporte) desc
	
	select
	Row_Number() over (Order By SsImporte desc) As Posicion,
	 IDCiudad,
	 Descripcion,
	 CoRegion,
	 SsImporte,
	 QtDias,
	 QtEdad_Prom
	 ,Cant_Clientes
	into #VentasporPais_AnioAnter2
	from #VentasporPais_AnioAnter
	Order by  SsImporte desc
	
	
	select
	Row_Number() over (Order By Total desc) As Posicion,
	 CoPais,
	 NoPais,
	 CoRegion,
	 Total
	 --
	 ,Dias_Dif
	 ,Edad_Prom
	 ,Cant_Clientes
	into #VentasporPais2
	from #VentasporPais
	
	union 
	
	select 
	Posicion=0,--(select isnull(max(posicion),0)+1 from #VentasporPais ),
	CoPais=IDCiudad,
	NoPais=Descripcion,
	CoRegion,
	Total=0
	--
	,Dias_Dif=0
	,Edad_Prom=0
	,Cant_Clientes
	from 
	#VentasporPais_AnioAnter
	where idciudad not in (select distinct copais from #VentasporPais)
	
	Order by  Total desc

declare @maxposicion int=(select isnull(max(posicion),0)+1 from #VentasporPais2)
declare @maxposicion2 int=(select isnull(max(posicion),0)+1 from #VentasporPais_AnioAnter2)

update #VentasporPais2 set posicion=@maxposicion where posicion=0 

	select
	 Posicion,
	 Posicion_AnioAnt=isnull((select Posicion from #VentasporPais_AnioAnter2 where idciudad=CoPais),@maxposicion2),
	 CoPais,
	 NoPais,
	 CoRegion,
	 Total,
	 Total_AnioAnt=isnull((select SsImporte from #VentasporPais_AnioAnter2 where idciudad=CoPais),0),
	 
	 Dias_Actual=isnull(Dias_Dif,0),
	 Edad_Prom_Actual=isnull(Edad_Prom,0),
	 
	 Dias=isnull((select QtDias from #VentasporPais_AnioAnter2 where idciudad=CoPais),0),
	 Edad_Prom=isnull((select QtEdad_Prom from #VentasporPais_AnioAnter2 where idciudad=CoPais),0)
	 ,Cant_Clientes=isnull(Cant_Clientes,0)
	 ,Cant_Clientes_Ant=isnull((select Cant_Clientes from #VentasporPais_AnioAnter2 where idciudad=CoPais),0)
	into #VentasporPais3
	from #VentasporPais2 
	
	select
	 Posicion,
	 Posicion_AnioAnt,
	 CoPais,
	 NoPais,
	 CoRegion,
	 Region=case when CoRegion='AF' then 'Africa' when CoRegion='AS' then 'Asia'  when CoRegion='CA' then 'Latam'--'S&C-America'
				when CoRegion='SA' then 'Latam' when CoRegion='NA' then 'N-America' when CoRegion='EU' then 'Europe'
				when CoRegion='AU' then 'Australia' else 'Otros' end,
			
	 Total,
	 Total_AnioAnt,
	 Crecimiento=case when Total_AnioAnt=0 then 100 else cast(round(round(((Total*100)/Total_AnioAnt)-100,2),0) as int) end,
	 Dias_Actual,
	 Edad_Prom_Actual,
	 Dias,
	 Edad_Prom
	 ,Cant_Clientes
	 ,Cant_Clientes_Ant
	into #VentasporPais4
	from #VentasporPais3
	

select * from #VentasporPais4
order by Posicion_AnioAnt

declare @Total_Region as decimal(15,2)=isnull((select SUM(Total) from #VentasporPais4),0)
declare @Total_Region_Ant as decimal(15,2)=isnull((select SUM(Total_AnioAnt) from #VentasporPais4),0)

print 'resumen por region'
select --CoRegion,
	   Region, 
	   SUM(Total) as Total,
	   SUM(Total_AnioAnt) as Total_AnioAnt,
	   --PCT= case when @Total_Region =0 then 0 else cast(round((SUM(Total)*100)/@Total_Region,0) as int) end,
	   --PCT_Ant= case when @Total_Region_Ant =0 then 0 else cast(round((SUM(Total_AnioAnt)*100)/@Total_Region_Ant,0) as int) end,
	   PCT= case when @Total_Region =0 then 0 else cast(round((SUM(Total)*100)/@Total_Region,2) as decimal(5,2)) end,
	   PCT_Ant= case when @Total_Region_Ant =0 then 0 else cast(round((SUM(Total_AnioAnt)*100)/@Total_Region_Ant,2) as decimal(5,2)) end,
	   Edad_Prom_Actual = isnull(AVG(CASE WHEN Edad_Prom_Actual <> 0 THEN Edad_Prom_Actual ELSE NULL END),0) ,
	    Edad_Prom = isnull(AVG(CASE WHEN Edad_Prom <> 0 THEN Edad_Prom ELSE NULL END),0) ,
	    Dias_Actual = isnull(AVG(CASE WHEN Dias_Actual <> 0 THEN Dias_Actual ELSE NULL END),0),
		Dias = isnull(AVG(CASE WHEN Dias <> 0 THEN Dias ELSE NULL END),0),
		Cant_Clientes= SUM(Cant_Clientes),
	    Cant_Clientes_Ant= SUM(Cant_Clientes_Ant),
	   orden=case when Region='Otros' then 1 else 0 end
from #VentasporPais4
group by --coregion,
	   Region
order by orden,Region
 
 print 'top 15 destinos'
 select top 15 p.IDubigeo as CoPais
     ,p.Descripcion 
     ,u.IDubigeo as CoCiudad
     ,u.Descripcion as Ciudad
     ,COUNT(IDDET) as Cant_Servicios,
     isnull((Select isnull(QtServicios,0) From dbo.RPT_VENTA_DESTINO Where NuAnio=@AnioAnter And       
 CoUbigeo=p.IDubigeo),0) as Cant_ServiciosAnioAnter,  
      isnull((Select isnull(QtServicios,0) From dbo.RPT_VENTA_DESTINO Where NuAnio=@AnioAnter-1 And       
 CoUbigeo=p.IDubigeo),0) as Cant_ServiciosAnioAnter1,       
 isnull((Select isnull(QtServicios,0) From RPT_VENTA_DESTINO Where NuAnio=@AnioAnter-2 And       
CoUbigeo=p.IDubigeo),0) as Cant_ServiciosAnioAnter2,
 isnull((Select isnull(QtServicios,0) From RPT_VENTA_DESTINO Where NuAnio=@AnioAnter-3 And       
 CoUbigeo=p.IDubigeo),0) as Cant_ServiciosAnioAnter3
     
from cotidet c
left join MAUBIGEO u on c.IDubigeo=u.IDubigeo
left join MAUBIGEO p on u.IDPais=p.IDubigeo
where YEAR(dia)=@Anio
group by p.IDubigeo 
     ,p.Descripcion
     ,u.IDubigeo
     ,u.Descripcion 
     order by Cant_Servicios desc
 
 --select 'a',copais,DescPaisCliente,FecOutPeru,sum(valorventa)
 --from #VentasporPaisClienteTemp
 --group by copais,DescPaisCliente,FecOutPeru
 --order by DescPaisCliente,FecOutPeru
 
  select copais,nopais, 
 EneVentaMes=sum(isnull(EneVentaMes,0)),
 FebVentaMes=sum(isnull(FebVentaMes,0)),
 MarVentaMes=sum(isnull(MarVentaMes,0)),
 AbrVentaMes=sum(isnull(AbrVentaMes,0)),
 MayVentaMes=sum(isnull(MayVentaMes,0)),
 JunVentaMes=sum(isnull(JunVentaMes,0)),
 JulVentaMes=sum(isnull(JulVentaMes,0)),
 AgoVentaMes=sum(isnull(AgoVentaMes,0)),
 SetVentaMes=sum(isnull(SetVentaMes,0)),
 OctVentaMes=sum(isnull(OctVentaMes,0)),
 NovVentaMes=sum(isnull(NovVentaMes,0)),
 DicVentaMes=sum(isnull(DicVentaMes,0)),
 Total=sum(isnull(Total,0))
  from #VentasporClienteMes
 group by copais,nopais
 order by nopais 

 
 select copais,
 nopais=(select Descripcion from MAUBIGEO where IDubigeo=copais), 
 EneVentaMes=isnull(EneVentaMes,0),
 FebVentaMes=isnull(FebVentaMes,0),
 MarVentaMes=isnull(MarVentaMes,0),
 AbrVentaMes=isnull(AbrVentaMes,0),
 MayVentaMes=isnull(MayVentaMes,0),
 JunVentaMes=isnull(JunVentaMes,0),
 JulVentaMes=isnull(JulVentaMes,0),
 AgoVentaMes=isnull(AgoVentaMes,0),
 SetVentaMes=isnull(SetVentaMes,0),
 OctVentaMes=isnull(OctVentaMes,0),
 NovVentaMes=isnull(NovVentaMes,0),
 DicVentaMes=isnull(DicVentaMes,0),
 Total=isnull(Total,0)
 from RPT_VENTA_PAIS
 where NuAnio=@AnioAnter
 and CoPais in (select distinct copais COLLATE SQL_Latin1_General_CP1_CI_AS from #VentasporClienteMes )

 union 

 select copais COLLATE SQL_Latin1_General_CP1_CI_AS,
 nopais COLLATE SQL_Latin1_General_CP1_CI_AS, 
 EneVentaMes=0,
 FebVentaMes=0,
 MarVentaMes=0,
 AbrVentaMes=0,
 MayVentaMes=0,
 JunVentaMes=0,
 JulVentaMes=0,
 AgoVentaMes=0,
 SetVentaMes=0,
 OctVentaMes=0,
 NovVentaMes=0,
 DicVentaMes=0,
 Total=0
 from #VentasporClienteMes
 where copais not in (select distinct copais COLLATE SQL_Latin1_General_CP1_CI_AS from RPT_VENTA_PAIS where NuAnio=@AnioAnter)
 order by nopais 

 --ventas mensual sin porcentaje concretizacion
 select * from #VentasReceptivoMensual_Sin_Porc  order by FecOutPeru
 select * from #VentasReceptivoMensual_Sin_APT_SinPorc  order by FecOutPeru

 drop table #VentasporPais_AnioAnter2
 drop table #VentasporPais_AnioAnter	 
 drop table #VentasporPais	 
 drop table #VentasporPais2
 drop table #VentasporPais3
 drop table #VentasporPais4
 drop table #VentasReceptivoMensual      
      
 drop table #VentasReceptivoMensualAcumulado      
      
 drop table #VentasReceptivoMensualDET      
     
 drop table #VentasporEjecutivoMes      
 drop table #VentasporEjecutivoMes2      
 drop table #VentasporEjecutivoAnio      
      
 drop table #VentasxCliente_Anual      
 drop table #VentasporPaisClienteTemp   
 drop table #VentasporClienteMes  
 drop table #VentasReceptivoMensualDet_Sin_APT
 drop table #VentasReceptivoMensual_Sin_APT
 drop table #VentasReceptivoMensualAcumulado_SIN_APT
 drop table #VentasReceptivoMensual_Sin_Porc
 drop table #VentasReceptivoMensual_Sin_APT_SinPorc

