﻿--JRF-20141015-Considerar el IDServicio_Det_V para los costos. 
CREATE Procedure dbo.MASERVICIOS_DET_SelxIDServicio  
@IDServicio char(8)  
As  
Set NoCount On   
select IDServicio_Det,Isnull(IDServicio_Det_V,0) as IDServicio_Det_V
from MASERVICIOS_DET 
where IDServicio = @IDServicio  
