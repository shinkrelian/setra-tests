﻿--JRF-20130930-Agregar el Tipo de OperacionContable.
--JRF-20131002-Agregar el valor de DefinicionTriple.
CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_PkSinAnul]  
 @IDServicio_Det int  
As  
   
 Set NoCount On  
   
 Select sd.*, s.TipoLib, s.Liberado,sd.idTipoOC as OperacContable 
 ,DefTrp.Monto_tri as MontoTrixCodigo,DefTrp.Monto_tris as MontoTrisxCodigo,DefTrp.TC as TipoCambioDefTrip
 From MASERVICIOS_DET sd Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
 Left Join MASERVICIOS_DET DefTrp On sd.IDServicio_DetxTriple = DefTrp.IDServicio_Det
 Where sd.IDServicio_Det=@IDServicio_Det
 And sd.Estado='A'
