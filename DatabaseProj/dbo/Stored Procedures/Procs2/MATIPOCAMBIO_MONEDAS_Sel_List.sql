﻿
CREATE PROCEDURE [dbo].[MATIPOCAMBIO_MONEDAS_Sel_List]
	@FechaInicio	smalldatetime,
	@FechaFin		smalldatetime,
	@CoMoneda		char(3)
AS
BEGIN
	Set NoCount On
	SET @CoMoneda = CASE WHEN ISNULL(@CoMoneda,'')='' THEN NULL ELSE @CoMoneda END
	Select CoMoneda, Fecha, ValCompra, ValVenta
	From MATIPOCAMBIO_MONEDAS
	where CoMoneda = COALESCE(@CoMoneda,CoMoneda) AND
	(Fecha between @FechaInicio and  @FechaFin or @FechaInicio='01/01/1900' or @FechaFin='01/01/1900')
	Order By CoMoneda, Fecha
END	
