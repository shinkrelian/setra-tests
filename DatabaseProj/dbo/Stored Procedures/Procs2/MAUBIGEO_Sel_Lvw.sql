﻿CREATE Procedure [dbo].[MAUBIGEO_Sel_Lvw]
	@ID	char(6),
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion	
	From MAUBIGEO
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDubigeo <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
