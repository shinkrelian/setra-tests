﻿CREATE Procedure [dbo].[MASERVICIOS_Sel_xTipoTransporte_Cbo] 
@TipoTransporte char(1),
@btodos bit
as

	Set NoCount On
	
	select * from (
	select '' IDProveedor,'--------' NombreCorto,0 Ord
	union 
	select distinct p.IDProveedor,p.NombreCorto,1 from MASERVICIOS s 
		Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
		where s.Transfer=1 and TipoTransporte =@TipoTransporte
	) as X
	where (@btodos=1 or Ord<>0)
	Order by Ord,2
