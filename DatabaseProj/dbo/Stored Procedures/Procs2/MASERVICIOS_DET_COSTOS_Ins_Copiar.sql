﻿CREATE Procedure [dbo].[MASERVICIOS_DET_COSTOS_Ins_Copiar]  
 @IDServicio char(8),    
 @Anio char(4),  
 @Tipo varchar(7),   
 @PorcentDcto numeric(5,2),  
 @UserMod char(4)  
As  
 Set NoCount On  
  
 Insert Into MASERVICIOS_DET_COSTOS  
 (Correlativo, IDServicio_Det, PaxDesde, PaxHasta, Monto, UserMod)  
 Select c.Correlativo, dNue.IDServicio_Det , c.PaxDesde, c.PaxHasta,   
 --c.Monto,   
 Case When @PorcentDcto=0 Then c.Monto Else c.Monto*(1-(@PorcentDcto/100)) End,  
 @UserMod  
 From MASERVICIOS_DET_COSTOS c Inner Join MASERVICIOS_DET d On c.IDServicio_Det=d.IDServicio_Det  
 And d.IDServicio=@IDServicio And   
   (d.Anio=@Anio Or ltrim(rtrim(@Anio))='') And   
  (d.Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')   
 Inner Join MASERVICIOS_DET dNue On d.IDServicio_Det=dNue.IDServicio_DetCopia   
  --And d.IDServicio_DetCopia Is Null   
 Where --d.IDServicio_DetCopia Is Null And   
 Not dNue.IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET_COSTOS)
