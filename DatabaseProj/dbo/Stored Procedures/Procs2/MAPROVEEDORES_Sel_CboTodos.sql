﻿--JRF-20130904-Agregando Activo = 'A'  
--JRF-20131113-Agregnado la Union para Trasladistas. 
--HLF-20150917-Union --Trasl Lima
-- Union --los antes guías (temporal)
CREATE Procedure dbo.MAPROVEEDORES_Sel_CboTodos      
 @bTodos bit,    
 @IDTipoProv char(3)      
As      
 Set NoCount On      
     
 SELECT * FROM      
 (      
 Select '' as IDProveedor,'<TODOS>' as DescProveedor, 0 as Ord      
 Union      
       
 Select IDProveedor, NombreCorto as DescProveedor,1 as Ord  From MAPROVEEDORES      
 Where (IDTipoProv=@IDTipoProv OR LTRIM(RTRIM(@IDTipoProv))='')  and Activo = 'A'    
 
 Union
 select IDUsuario AS IDProveedor, Nombre as DescProveedor,2 as Ord from MAUSUARIOS
 where Nivel ='TRL' AND Activo = 'A' and @IDTipoProv = '017'

 Union --Trasl Lima
 Select IDProveedor, NombreCorto as DescProveedor,3 as Ord  From MAPROVEEDORES      
 Where IDTipoProv='020'  and Activo = 'A'    
 Union --los antes guías (temporal)
 SELECT IDProveedor, '* '+NombreCorto + '(Antes Guía)' as DescProveedor,4 as Ord  From MAPROVEEDORES      
		    WHERE IDTipoProv='005' AND (rtrim(ltrim(ApPaterno))+' '+rtrim(ltrim(ApMaterno)) IN
			('BARBERIS Nuñez del Prado','SARMENTO -','Neciosup TORRES','AYALA Vasquez','morales nolasco','hippauf -','aguilar anaya','bringas dextre') or NombreCorto='Poppe-, Daniel')


 
 ) AS X      
 Where (@bTodos=1 Or Ord<>0)       
 Order by Ord,2     


