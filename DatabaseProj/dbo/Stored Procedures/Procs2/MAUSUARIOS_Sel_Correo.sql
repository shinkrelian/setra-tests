﻿
CREATE PROCEDURE [MAUSUARIOS_Sel_Correo]
	@Correo VARCHAR(50)
AS
BEGIN
	SET NOCOUNT ON
	
	SELECT	IDUsuario, Usuario 
	FROM	MAUSUARIOS 
	WHERE	LOWER(Correo) = LOWER(@Correo) AND Activo='A'
END
