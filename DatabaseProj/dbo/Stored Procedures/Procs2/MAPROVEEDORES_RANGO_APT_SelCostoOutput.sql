﻿create Procedure [dbo].[MAPROVEEDORES_RANGO_APT_SelCostoOutput]
	@IDProveedor int,
	@NuAnio CHAR(4),
	@NroPax	smallint,
	@pMonto	numeric(8,2) output
As
	Set Nocount On
	
	Select @pMonto=Monto From MAPROVEEDORES_RANGO_APT 
	Where IDProveedor=@IDProveedor And NuAnio=@NuAnio and
	@NroPax Between PaxDesde And PaxHasta
	
	Set @pMonto=ISNULL(@pMonto,0)
	
