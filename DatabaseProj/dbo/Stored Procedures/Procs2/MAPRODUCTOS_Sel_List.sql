﻿CREATE PROCEDURE MAPRODUCTOS_Sel_List
@NoProducto nvarchar(100)='',
@CoRubro tinyint=0
as
SELECT        MAPRODUCTOS.CoProducto,
MAPRODUCTOS.NoProducto,
MAPRODUCTOS.CoRubro,
MARUBRO.NoRubro,
MAPRODUCTOS.CoSAP
FROM            MAPRODUCTOS left JOIN
                         MARUBRO ON MAPRODUCTOS.CoRubro = MARUBRO.CoRubro
WHERE        (@NoProducto = '' or rtrim(ltrim(MAPRODUCTOS.NoProducto)) like '%'+@NoProducto+'%') AND (MAPRODUCTOS.CoRubro = @CoRubro or @CoRubro=0);
