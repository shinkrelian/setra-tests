﻿CREATE Procedure dbo.NOTA_VENTA_DET_Sel_ListxIDNotaVenta  
 @IDNotaVenta char(10)  
As  
 Set NoCount On  
   
 Select sd.idTipoOC   ,  
		toc.Descripcion as DescTipoOC,  
		p.NombreCorto As DescProveedor,  
		nvd.Descripcion As Servicio,  
		nvd.SubTotal As Monto,  
		nvd.TotalIGV As Igv,  
		nvd.Total AS TotalCostoVenta  
      
 From NOTA_VENTA_DET nvd   
 Left Join RESERVAS_DET rd On nvd.IDReserva_Det=rd.IDReserva_Det  
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det  
 Left Join MATIPOOPERACION toc On toc.IdToc=sd.idTipoOC  
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio  
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor  
 Where nvd.IDNotaVenta = @IDNotaVenta  
 Order by toc.Orden  
