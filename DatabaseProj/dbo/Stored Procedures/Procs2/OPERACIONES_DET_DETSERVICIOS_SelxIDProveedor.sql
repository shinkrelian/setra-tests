﻿CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelxIDProveedor
	@IDCab	int,
	@IDProveedor char(6)
As
	Set NoCount On  
	
	--Select upper(substring(DATENAME(dw,d.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,d.Dia,103))) As Fecha,
	--substring(convert(varchar,d.Dia,108),1,5) As Hora,
	--ISNull(sd.Descripcion,sd2.Descripcion) as Servicio, 	
	--d.NroPax, c.IDIdioma, ds.Total
	--From OPERACIONES_DET_DETSERVICIOS ds 
	--Left Join OPERACIONES_DET d On ds.IDOperacion_Det=d.IDOperacion_Det
	--Left Join MASERVICIOS_DET sd On ds.IDServicio_Det_V_Cot=sd.IDServicio_Det
	--Left Join MASERVICIOS_DET sd2 On d.IDServicio_Det=sd2.IDServicio_Det
	--Left Join OPERACIONES o On o.IDOperacion=d.IDOperacion And o.IDProveedor=@IDProveedor
	--Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio 
	--Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor	
	--Inner Join COTICAB c On o.IDCab=c.IDCAB And c.IDCAB=@IDCab
	

	SELECT upper(substring(DATENAME(dw,d.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,d.Dia,103))) As Fecha,
	substring(convert(varchar,d.Dia,108),1,5) As Hora,
	ISNull(sd.Descripcion,sd2.Descripcion) as Servicio, 	
	d.NroPax, 
	(Select IDIdioma From COTICAB Where IDCAB=@IDCab) as IDIdioma, 
	ds.Total, p.NombreCorto as DescProveedor_Prg, IsNull(d.ObservBiblia,'') as ObservBiblia
	FROM OPERACIONES_DET_DETSERVICIOS ds 
	Left Join OPERACIONES_DET d On ds.IDOperacion_Det=d.IDOperacion_Det
	Left Join MASERVICIOS_DET sd On ds.IDServicio_Det_V_Cot=sd.IDServicio_Det
	Left Join MASERVICIOS_DET sd2 On ds.IDServicio_Det=sd2.IDServicio_Det
	Left Join MAPROVEEDORES p On ds.IDProveedor_Prg=p.IDProveedor
	WHERE ds.IDOperacion_Det In 
	(select IDOperacion_Det From OPERACIONES_DET Where IDOperacion in 
	(select IDOperacion from OPERACIONES where IDCab=@IDCab and IDProveedor=@IDProveedor))
--	And ds.IDEstadoSolicitud<>'NV'
	Order by d.Dia
	
