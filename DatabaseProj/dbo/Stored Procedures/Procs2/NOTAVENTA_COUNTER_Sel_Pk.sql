﻿

--------------------------------------------------------------------------------------------------------------


--[NOTAVENTA_COUNTER_Sel_Pk] '000099'
--JHD-20141226- Se corrigio agregando los campos faltantes
--JHD-20150119-  Direccion=isnull(maclientes.Direccion,'')
--JHD-20150119-  NumIdentidad=isnull(maclientes.NumIdentidad,'')
CREATE PROCEDURE [dbo].[NOTAVENTA_COUNTER_Sel_Pk]    
@NuNtaVta char(6)
AS
BEGIN

SELECT     NOTAVENTA_COUNTER.NuNtaVta,
NOTAVENTA_COUNTER.FeNtaVta,
NOTAVENTA_COUNTER.SsNtaVta,
NOTAVENTA_COUNTER.CoCliente,
MACLIENTES.RazonComercial, 
                      NOTAVENTA_COUNTER.NoNtaVta,
                      NOTAVENTA_COUNTER.QtPax, 
                      NOTAVENTA_COUNTER.TxDetalle,
                      NOTAVENTA_COUNTER.CoSegmento,
                      NOTAVENTA_COUNTER.SsCosto,
                      NOTAVENTA_COUNTER.SsUtilidad1, 
                      NOTAVENTA_COUNTER.SsUtilidad2,
                      NOTAVENTA_COUNTER.CoForPag,
                      MAFORMASPAGO.Descripcion,
                      NOTAVENTA_COUNTER.SsPago, 
                      NOTAVENTA_COUNTER.CoAeroLinea,
                      MAPROVEEDORES.RazonSocial AS Aerolinea,
                      NOTAVENTA_COUNTER.CoEjecutivo,
                      MAUSUARIOS.Nombre AS Ejecutivo, 
                      NOTAVENTA_COUNTER.TxObservaciones,
                      NOTAVENTA_COUNTER.FlActivo,
                      NOTAVENTA_COUNTER.UserMod,
                      NOTAVENTA_COUNTER.FecMod,
                      CoMoneda=isnull(NOTAVENTA_COUNTER.CoMoneda,''),
                      Moneda=isnull((select Descripcion from MAMONEDAS where IDMoneda=NOTAVENTA_COUNTER.CoMoneda),''),
                      SSTipoCambio=isnull(NOTAVENTA_COUNTER.SSTipoCambio,0),
                      Direccion=isnull(maclientes.Direccion,''),
                      NumIdentidad=isnull(maclientes.NumIdentidad,''),
                      Tipo_Documento=isnull((select Descripcion from MATIPOIDENT where IDIdentidad=MACLIENTES.IDIdentidad),'')
FROM         NOTAVENTA_COUNTER LEFT JOIN
                      MACLIENTES ON NOTAVENTA_COUNTER.CoCliente = MACLIENTES.IDCliente LEFT JOIN
                      MAPROVEEDORES ON NOTAVENTA_COUNTER.CoAeroLinea = MAPROVEEDORES.IDProveedor LEFT JOIN
                      MAUSUARIOS ON NOTAVENTA_COUNTER.CoEjecutivo = MAUSUARIOS.IDUsuario LEFT JOIN
                      MAFORMASPAGO ON NOTAVENTA_COUNTER.CoForPag = MAFORMASPAGO.IdFor
WHERE NuNtaVta = @NuNtaVta

END;

