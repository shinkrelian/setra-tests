﻿

Create Procedure dbo.NOTA_VENTA_SelNoAnulOutput
	@IDCab	int,
	@pExiste bit Output
As
	Set Nocount On
	
	Set @pExiste=0
	If Exists(Select IDNotaVenta From NOTA_VENTA Where IDCab=@IDCab And IDEstado<>'AN')
		Set @pExiste=1
		
