﻿--HLF-20160303-CantidadAPagar=(Select COUNT(*) From OPERACIONES_DET_PAX Where IDOperacion_Det=@IDOperacion_Det),
CREATE Procedure [dbo].[OPERACIONES_DET_Upd_NroPax]    
 @IDOperacion_Det int,    
 @UserMod char(4)    
As    
 Set NoCount On    
     
 Update OPERACIONES_DET Set   
    NroPax=(Select COUNT(*) From OPERACIONES_DET_PAX Where IDOperacion_Det=@IDOperacion_Det)
     	  -ISNULL(NroLiberados,0),
	CantidadAPagar=(Select COUNT(*) From OPERACIONES_DET_PAX Where IDOperacion_Det=@IDOperacion_Det),
    UserMod=@UserMod, FecMod=GETDATE()     
 Where IDOperacion_Det=@IDOperacion_Det 