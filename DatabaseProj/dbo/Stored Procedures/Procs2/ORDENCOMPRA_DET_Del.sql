﻿
--create
CREATE PROCEDURE [ORDENCOMPRA_DET_Del]
	@NuOrdComInt_Det int
AS
BEGIN
	Delete [dbo].[ORDENCOMPRA_DET]
	Where 
		[NuOrdComInt_Det] = @NuOrdComInt_Det
END;
