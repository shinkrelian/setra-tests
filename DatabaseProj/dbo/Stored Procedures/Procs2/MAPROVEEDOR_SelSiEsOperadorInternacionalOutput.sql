﻿

Create Procedure dbo.MAPROVEEDOR_SelSiEsOperadorInternacionalOutput
	@IDProveedor char(6),
	@pbOk bit output
As
	Set Nocount On
 
	Set @pbOk = 0
	If Exists(Select IDProveedor From MAPROVEEDORES Where 
		IDProveedorInternacional=@IDProveedor)
		Set @pbOk = 1
		
