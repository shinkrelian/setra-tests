﻿--JRF-20150612-Condicionar que halla tipo de Lib.
CREATE Procedure dbo.RESERVAS_DET_TMP_UpdNroLiberadosxCapacidadHabxReserva  
 @IDReserva int,  
 @CapacidadHab tinyint,  
 @NroLiberados smallint,  
 @UserMod char(4)  
As  
 Set NoCount On  
   
 UPDATE RESERVAS_DET_TMP
 Set NroPax=rd.NroPax-@NroLiberados,  
 NroLiberados=@NroLiberados,   
 UserMod=@UserMod, FecMod=GETDATE()  
 From RESERVAS_DET_TMP rd Left Join COTIDET cd On rd.IDDet=cd.IDDET  
 WHERE IDReserva IN (@IDReserva)  
 And CapacidadHab=@CapacidadHab  
 And rd.NroPax>=@NroLiberados  
 And cd.IncGuia=0 And rd.Tipo_Lib is not null
