﻿--HLF-20130709-Agregando columna TotalUSD        
--HLF-20130716-Agregando columna rd.NetoGen as TotalServicio        
--HLF-20130716-Agregando columna SaldoxPagar      
--HLF-20130730-Comentando cambio a USD en columna TotalUSD y agregandola en SaldoxPagar    
--Quitando columnas TotalACambiar, TotalPago    
--JRF-20150411-...IsNull(opd.TxDescripcion,isnull(r...
--JHD-20150908-isnull(opd.SSDetraccion,0) as SSDetraccion,  
--JHD-20150908-case when isnull(opd.SSDetraccion_USD,0)= 0 then  isnull(opd.SSDetraccion,0) else isnull(opd.SSDetraccion_USD,0) end as SSDetraccion,      
--JHD-20150909-isnull(opd.Total_Final,0) as Total_Final,
--JHD-20150918-isnull(opd.SSOtrosDescuentos,0) as SSOtrosDescuentos,
--HLF-20160329--  -isnull((Select  sum(SsMontoTotal) ...
CREATE Procedure dbo.ORDENPAGO_DET_SelxIDOrdPag          
 @IDOrdPag int          
As          
 Set NoCount On          
 SELECT [IDOrdPag_Det]          
   ,opd.[IDOrdPag]          
   ,isnull(opd.[IDReserva_Det],0) as IDReserva_Det         
   ,isnull(opd.[IDServicio_Det] ,0) as IDServicio_Det    
   ,Case When opd.IDTransporte is not null then 'Ticket Aereo '+cv.Ruta else IsNull(opd.TxDescripcion,isnull(rd.Servicio,''))End as Servicio  
   ,opd.[NroPax]          
   ,opd.[NroLiberados]          
   ,isnull(opd.[Cantidad],0) as Cantidad  
   ,Isnull(opd.[Noches],0) Noches          
   ,Case When opd.IDTransporte is not null then IsNull(cv.CostoOperador,0)*(select COUNT(*) from COTITRANSP_PAX ctp Where ctp.IDTransporte=opd.IDTransporte)
    Else isnull(rd.TotalGen,0) End
    as TotalServicio,      
 isnull(opd.Total,0) as Total,    
 --rd.TotalGen    

 --isnull(opd.SSDetraccion,0) as SSDetraccion,    
case when isnull(opd.SSDetraccion_USD,0)= 0 then  isnull(opd.SSDetraccion,0) else isnull(opd.SSDetraccion_USD,0) end as SSDetraccion,    

--nuevo campo
isnull(opd.SSOtrosDescuentos,0) as SSOtrosDescuentos,

 isnull(opd.Total_Final,0) as Total_Final,

 Case When opd.IDTransporte is not null Then 
	IsNull(cv.CostoOperador,0)*(select COUNT(*) from COTITRANSP_PAX ctp Where ctp.IDTransporte=opd.IDTransporte)
 else
	 Case When o.IDMoneda=rd.IDMoneda Then    
		rd.TotalGen    
	 Else    
		dbo.FnCambioMoneda(rd.TotalGen,rd.IDMoneda,o.IDMoneda,isnull(o.TCambio,0))    
	 End    
 End
 -opd.Total 
	-isnull((Select  sum(SsMontoTotal) --SUM(dbo.FnCambioMoneda(o1.SsMontoTotal,o1.IDMoneda,o.IDMoneda,isnull(o.TCambio,0))) 
		From ORDENPAGO o1
		Where o1.IDCab=o.IDCab and o1.IDReserva=o.IDReserva and o1.IDOrdPag<>o.IDOrdPag and o1.idestado<>'EA'),0)
 as SaldoxPagar      
 FROM [dbo].[ORDENPAGO_DET] opd         
 Left Join RESERVAS_DET rd On opd.IDReserva_Det = rd.IDReserva_Det          
 Left Join ORDENPAGO o On opd.IDOrdPag=o.IDOrdPag   
 Left Join COTIVUELOS cv On opd.IDTransporte=cv.ID     
 WHERE opd.IDOrdPag = @IDOrdPag 


