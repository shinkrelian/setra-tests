﻿--HLF-20160215-Utilizar PRESUPUESTO_SOBRE_Sel_Correlativo_FondoFijo_Output
CREATE Procedure [dbo].[PRESUPUESTO_SOBRE_Sel_FondoFijo_ER]    
@NuPreSob int
 as
  Set Nocount On    

	declare  
		@NuCodigo_ER varchar(30)=''
    

	DECLARE --@NuFondoFijo int, 
	@CoProveedor char(6)
	--Set @NuFondoFijo=(select NuFondoFijo from PRESUPUESTO_SOBRE where NuPreSob=@NuPreSob)
			 
	--set @NuCodigo_ER=(select isnull(NuCodigo_ER,'')+'-'+isnull(CoSap,'') from MAFONDOFIJO where NuFondoFijo=@NuFondoFijo)
	Select @CoProveedor=IDProveedor From PRESUPUESTO_SOBRE Where NuPreSob=@NuPreSob

	Exec PRESUPUESTO_SOBRE_Sel_Correlativo_FondoFijo_Output @CoProveedor, @NuCodigo_ER output

	select isnull(@NuCodigo_ER,'') as NuCodigo_ER
