﻿ Create Procedure dbo.RESERVAS_DET_TMP_Sel	
	@IDReserva int
 As
	Set Nocount On
	
	Select * From RESERVAS_DET_TMP
	Where IDReserva = @IDReserva 
	order by Dia, FechaOut
	
