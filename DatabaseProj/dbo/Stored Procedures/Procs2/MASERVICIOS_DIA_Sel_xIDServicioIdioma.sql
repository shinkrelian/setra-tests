﻿CREATE Procedure [dbo].[MASERVICIOS_DIA_Sel_xIDServicioIdioma]
	@IDServicio	char(8),
	@IDIdioma	varchar(12)
As
	Set NoCount On

	Select Dia,IDIdioma,DescripCorta,Descripcion 
	From MASERVICIOS_DIA 
	Where IDServicio=@IDServicio And IDIdioma=@IDIdioma
	Order by Dia
