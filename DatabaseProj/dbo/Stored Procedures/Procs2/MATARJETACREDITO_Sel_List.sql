﻿CREATE Procedure [dbo].[MATARJETACREDITO_Sel_List]
@CodTar char(3),
@Descripcion varchar(50)
as
	Set NoCount On
	select CodTar,Descripcion
	from MATARJETACREDITO
	Where (LTRIM(RTRIM(@Descripcion))='' Or Descripcion like '%'+@Descripcion+'%')
	And (LTRIM(rtrim(@CodTar))='' Or CodTar=@CodTar)
