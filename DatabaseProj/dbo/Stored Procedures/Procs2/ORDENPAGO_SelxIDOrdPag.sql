﻿--JRF-20140516-Agregar los campos de cliente(RazonComercial y Solvencia).
--JRF-20150114-[Case When p.IDFormaPago='002' Then 1 Else 0 End as ProveedorPrepago]
CREATE Procedure dbo.ORDENPAGO_SelxIDOrdPag      
@IDOrdPag int      
As      
 Set NoCount On      
 SELECT cl.RazonComercial as DescCliente,cl.Solvencia,p.NombreCorto as DescProveedor,p.IDTipoProv,  
 (select min(rd.Dia) from RESERVAS_DET rd where rd.Anulado=0 and   
    rd.IDReserva=(select Top(1) r2.IDReserva from RESERVAS r2 Where  r2.Anulado =0 and   
         r2.IDCab=op.IDCab and r2.IDProveedor =p.IDProveedor)) as FechaIncio,  
 IsNull(
 Case When p.IDTipoProv = '001' Or   
    (p.IDTipoProv = '003' And Exists(select IDReserva_Det from RESERVAS_DET rd Left Join MASERVICIOS_DET sd On sd.IDServicio_Det=rd.IDServicio_Det  
            where sd.ConAlojamiento=1 and rd.Anulado=0 and rd.IDReserva=r.IDReserva)) Then  
 (select max(rd.FechaOut) from RESERVAS_DET rd where rd.Anulado=0 and   
  rd.IDReserva=(select Top(1) r2.IDReserva from RESERVAS r2 Where  r2.Anulado =0 and   
         r2.IDCab=op.IDCab and r2.IDProveedor =p.IDProveedor)) 
 Else  
 (select max(rd.Dia) from RESERVAS_DET rd where rd.Anulado=0 and   
  rd.IDReserva=(select Top(1) r2.IDReserva from RESERVAS r2 Where  r2.Anulado =0 and   
         r2.IDCab=op.IDCab and r2.IDProveedor =p.IDProveedor)) End 
 ,	 
 (select min(rd.Dia) from RESERVAS_DET rd where rd.Anulado=0 and   
    rd.IDReserva=(select Top(1) r2.IDReserva from RESERVAS r2 Where  r2.Anulado =0 and   
         r2.IDCab=op.IDCab and r2.IDProveedor =p.IDProveedor)))	 as FechaOut,  
 c.IDFile,c.NroPax,IsNull(r.CodReservaProv,'') as CodReservaProv,c.Titulo,r.IDProveedor,
 Case When p.IDFormaPago='002' Then 1 Else 0 End as ProveedorPrepago,op.*
 FROM ORDENPAGO op Left Join COTICAB c On op.IDCab = c.IDCAB  
 Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente    
 Left Join RESERVAS r On op.IDReserva = r.IDReserva  
 Left Join MAPROVEEDORES p On r.IDProveedor = p.IDProveedor  
WHERE IDOrdPag = @IDOrdPag      
