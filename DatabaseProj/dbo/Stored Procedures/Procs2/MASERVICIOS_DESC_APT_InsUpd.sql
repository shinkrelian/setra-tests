﻿
CREATE Procedure [dbo].[MASERVICIOS_DESC_APT_InsUpd]   
    @IDServicio char(8),
	@IDTemporada int,
	@DescripcionAlterna varchar(200),
	@AgruparServicio bit,
	@PoliticaLiberado bit,
	@Liberado tinyint,
	@TipoLib char(1),
	@MaximoLiberado tinyint,
	@MontoL numeric(10,4),
    @UserMod char(4)  
AS
BEGIN
	Set NoCount On
	IF ISNULL(@PoliticaLiberado,0) = 0
	BEGIN
		SELECT @Liberado = NULL, @TipoLib = NULL, @MaximoLiberado = NULL, @MontoL = NULL
	END
	IF NOT EXISTS(SELECT IDServicio, IDTemporada FROM MASERVICIOS_DESC_APT
			   WHERE IDServicio = @IDServicio AND IDTemporada = @IDTemporada)
	BEGIN
		INSERT INTO MASERVICIOS_DESC_APT
			([IDServicio]
			,[IDTemporada]
			,[DescripcionAlterna]
			,[AgruparServicio]
			,[PoliticaLiberado]
			,[Liberado]
			,[TipoLib]
			,[MaximoLiberado]
			,[MontoL]
			,[FecMod]
			,[UserMod]
			)
		VALUES
			(@IDServicio,
			@IDTemporada,
			@DescripcionAlterna,
			@AgruparServicio,
			@PoliticaLiberado,
			@Liberado,
			@TipoLib,
			@MaximoLiberado,
			@MontoL,
			GETDATE(),
			@UserMod
			)
	END
	ELSE
	BEGIN
		UPDATE MASERVICIOS_DESC_APT SET DescripcionAlterna = @DescripcionAlterna,
										AgruparServicio = @AgruparServicio,
										PoliticaLiberado = @PoliticaLiberado,
										Liberado = @Liberado,
										TipoLib = @TipoLib,
										MaximoLiberado = @MaximoLiberado,
										MontoL = @MontoL,
										UserMod = @UserMod
		WHERE IDServicio = @IDServicio AND IDTemporada = @IDTemporada
	END
END

