﻿CREATE Procedure [dbo].[MATIPOUBIG_Upd]
	@Descripcion varchar(20),
	@DescripcionUpd varchar(20),
    @UserMod char(4)
	
As
	Set NoCount On
	
	Update MATIPOUBIG
	Set Descripcion=@DescripcionUpd ,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where Descripcion=@Descripcion
