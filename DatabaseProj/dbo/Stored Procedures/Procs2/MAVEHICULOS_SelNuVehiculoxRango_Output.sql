﻿--JRF-Isnull a la variable de Retorno  
--JRF-20150304-Top(1)...
CREATE Procedure dbo.MAVEHICULOS_SelNuVehiculoxRango_Output    
@flOtraCiudad bit,    
@Cantidad tinyint,    
@pNuVehiculo tinyint output    
As    
 Set NoCount On    
 set @pNuVehiculo = (select Top(1) NuVehiculo    
 from MAVEHICULOS    
 where FlOtraCiudad = @flOtraCiudad and @Cantidad between QtDe and QtHasta)    
 set @pNuVehiculo = ISNULL(@pNuVehiculo,0)  
