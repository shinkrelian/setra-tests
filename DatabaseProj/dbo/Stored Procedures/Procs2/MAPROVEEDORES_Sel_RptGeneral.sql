﻿--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-(u.IDPais=@CoPais Or LTRIM(RTRIM(@CoPais))='') And 
--JRF-20150518-Agregar la columna Proveedor Internacional
CREATE proc [dbo].[MAPROVEEDORES_Sel_RptGeneral]  
@Persona char(1),  
@IDTipoProv char(3),  
@IDTipoOper char(1),  
@IDCiudad char(6),  
@Domiciliado bit ,
@CoPais char(6)='' 
as  
 Set NoCount On  
  
 select pr.IDProveedor,pr.NumIdentidad,pr.NombreCorto as RazonComercial,pint.NombreCorto as ProveedorInter,pr.Direccion,  
  Telefono=   
   Case When Right(isnull(pr.Telefono1,'')+'/'+isnull(pr.Telefono2,''),1)='/' Then   
    isnull(pr.Telefono1,'')  
   Else  
    Case When Left(isnull(pr.Telefono1,'')+'/'+isnull(pr.Telefono2,''),1)='/' Then   
      isnull(pr.Telefono2,'')  
     else  
      isnull(pr.Telefono1,'')+'/'+isnull(pr.Telefono2,'')  
    End  
   End,
   case when pr.IDTipoProv <> '005' then pr.Web else Isnull(pr.Email1,'') End as Web,u.Descripcion as Ciudad,  
   (select COUNT(*) from MACONTACTOSPROVEEDOR where IDProveedor=pr.IDProveedor) as CantContactos  
 from MAPROVEEDORES pr Left Join MAUBIGEO u On pr.IDCiudad=u.IDubigeo  
 Left Join MAPROVEEDORES pInt On pInt.IDProveedor=pr.IDProveedorInternacional
 where (pr.IDCiudad=@IDCiudad Or LTRIM(RTRIM(@IDCiudad))='') And 
    (u.IDPais=@CoPais Or LTRIM(RTRIM(@CoPais))='') And   
    (pr.Persona=@Persona Or LTRIM(RTRIM(@Persona))='') And   
    (pr.Domiciliado=@Domiciliado) And pr.Activo='A' And  
    (pr.IDTipoOper=@IDTipoOper Or LTRIM(rtrim(@IDTipoOper))='') And  
    (pr.IDTipoProv=@IDTipoProv Or LTRIM(rtrim(@IDTipoProv))='')  
