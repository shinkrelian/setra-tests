﻿--JRF-20160429-.And Activo='A'
CREATE Procedure dbo.MAPROVEEDORES_Sel_OutPutxDominio
@dominio varchar(100),
@pIDProveedor varchar(6) output
As
Set NoCount On
Set @pIDProveedor = ''
Select top(1) @pIDProveedor = IDProveedor
from MAPROVEEDORES 
where CHARINDEX(@dominio,Email1) > 0 Or CHARINDEX(@dominio,Email2) > 0 
Or CHARINDEX(@dominio,Email3) > 0 Or CHARINDEX(@dominio,Email4) > 0
Or CHARINDEX(@dominio,EmailContactoAdmin) > 0 Or CHARINDEX(@dominio,EmailRecepcion1) > 0 Or CHARINDEX(@dominio,EmailRecepcion2) > 0
And Activo='A'
