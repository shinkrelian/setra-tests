﻿
-------------------------------------------------------------------
CREATE PROCEDURE [dbo].[NOTAVENTA_COUNTER_DETALLE_Sel_Pk]    

@NuNtaVta char(6),
@NuItem tinyint=0,
@NuDetalle tinyint=0

AS
BEGIN
SELECT     
NOTAVENTA_COUNTER_DETALLE.NuNtaVta,
NOTAVENTA_COUNTER_DETALLE.NuDetalle,
NOTAVENTA_COUNTER_DETALLE.NuItem,

                      NOTAVENTA_COUNTER_DETALLE.NuDocumento,
                      

                      --NOTAVENTA_COUNTER_DETALLE.CoMoneda,
                      --MAMONEDAS.Descripcion, 
                      --NOTAVENTA_COUNTER_DETALLE.SSTipoCambio,
                      NOTAVENTA_COUNTER_DETALLE.CoSegmento,
                      NOTAVENTA_COUNTER_DETALLE.SSCosto, 
                      NOTAVENTA_COUNTER_DETALLE.SSComision,
                      NOTAVENTA_COUNTER_DETALLE.SSValor,
                      NOTAVENTA_COUNTER_DETALLE.SSFEE_Emision, 
                      NOTAVENTA_COUNTER_DETALLE.SSVenta,
                      NOTAVENTA_COUNTER_DETALLE.SSUtilidad--,
                      --NOTAVENTA_COUNTER_DETALLE.FlActivo
FROM         NOTAVENTA_COUNTER_DETALLE 
			--INNER JOIN MAMONEDAS ON NOTAVENTA_COUNTER_DETALLE.CoMoneda = MAMONEDAS.IDMoneda
WHERE    (NOTAVENTA_COUNTER_DETALLE.NuNtaVta = @NuNtaVta) AND
		(NOTAVENTA_COUNTER_DETALLE.NuItem = @NuItem) AND
		(NOTAVENTA_COUNTER_DETALLE.NuDetalle =@NuDetalle ) 
	
		
END;
