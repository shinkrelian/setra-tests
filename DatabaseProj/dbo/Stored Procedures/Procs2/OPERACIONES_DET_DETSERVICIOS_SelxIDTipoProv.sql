﻿--JRF-20130226- Nueva columna IDMoneda,TipoCambio        
--JRF-20130208- Diferencia de Pax del Detalle con la Cabezera.          
--JRF-20130207- Nombre de Columna        
--HLF-20130606- Agregando +ISNULL(d.NroLiberados,0)          
--JRF-20130607- Igualar al correo Reservas. Adicionar Campo(Tipo), Nombre del servicio    
--HLF-20130626- Reemplazando d.Servicio as DescServicio, por IsNull(sd.Descripcion,sd2.Descripcion) as DescServicio
--HLF-20150630-ds.QtPax as NroPax, 
--JRF-20151209-..Case When @FlServicioTC = 1 then Cast(@DifDias as varchar(2))+ ' días de servicio del TC' else
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelxIDTipoProv                 
 @IDTipoProv char(3),                      
 @IDOperacion_Det int,                  
 @IDEstadoSolicitud char(2),
 @IDServicio	char(8)
As                  
 Set NoCount On    
 Declare @FlTourConductor bit = Case When Exists(select * from OPERACIONES_DET where IDOperacion_Det=@IDOperacion_Det And FlServicioTC=1) 
								then 1 else 0 End
 Declare @NroItems int = Case When @FlTourConductor = 1 then 1 else 100 End
 Declare @IdCab int = (select distinct o.IDCab from OPERACIONES_DET od Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion 
					   where IDOperacion_Det = @IDOperacion_Det)
 Declare @DifDias int = 0
 select @DifDias = DATEDIFF(day,FeDesde,FeHasta) from COTICAB_TOURCONDUCTOR where IDCab=@IdCab
                   
 Select upper(substring(DATENAME(dw,d.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,d.Dia,103))) As Dia,            
 substring(convert(varchar,d.Dia,108),1,5) As Hora,                  
 --d.Servicio as DescServicio,                    
 --IsNull(sd.Descripcion,sd2.Descripcion) as DescServicio, 
 Case When @FlTourConductor = 1 then Cast(@DifDias as varchar(2))+ ' días de servicio del TC' 
 else IsNull(sd.Descripcion,sd2.Descripcion) End as DescServicio,
 --d.NroPax+ISNULL(d.NroLiberados,0) as NroPax, 
 ds.QtPax as NroPax, 
 i.Siglas As IDidioma, ds.Total_Prg As Total,      
 ds.IDMoneda, ISNULL(ds.TipoCambio,0) As TipoCambio , ISNULL(OD.ObservBiblia,'') as ObservBiblia,          
 Case When c.NroPax <> d.NroPax then 1 Else 0 End As ExistDiferPax,d.IDOperacion_Det,    
 s.IDTipoServ    
                
 From OPERACIONES_DET_DETSERVICIOS ds                   
 Left Join OPERACIONES_DET d On ds.IDOperacion_Det=d.IDOperacion_Det                  
 Left Join MASERVICIOS_DET sd On ds.IDServicio_Det_V_Cot=sd.IDServicio_Det                  
 Left Join MASERVICIOS_DET sd2 On ds.IDServicio_Det=sd2.IDServicio_Det                  
 Left Join OPERACIONES o On o.IDOperacion=d.IDOperacion                   
 Inner Join MAPROVEEDORES p On p.IDProveedor=ds.IDProveedor_Prg And p.IDTipoProv in (@IDTipoProv)                  
 Left Join COTICAB c On o.IDCab=c.IDCAB                  
 Inner Join OPERACIONES_DET od On ds.IDOperacion_Det = od.IDOperacion_Det                
 Inner Join MASERVICIOS s On sd2.IDServicio = s.IDServicio    
 Inner Join MAIDIOMAS i on c.IDIdioma = i.IDidioma    
 Where ds.IDOperacion_Det=@IDOperacion_Det And ds.IDEstadoSolicitud=@IDEstadoSolicitud                   
 And ds.IDServicio=@IDServicio
