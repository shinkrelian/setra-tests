﻿CREATE Procedure dbo.MASERVICIOS_Sel_VerificaDiaOutPut
@IDServicio char(8),    
@FechaDia varchar(20),    
@pOk Bit Output    
As  
 Set NoCount On  
 
	Declare @NumDiaFechaDia Tinyint, @Rows Tinyint
	Set @FechaDia=CONVERT(SMALLDATETIME,@FechaDia,103)
	   
	Set @NumDiaFechaDia = dbo.FnDiaSemana(Case When SUBSTRING(DATENAME(dw,@FechaDia),0,2) = 'M' Then   
              Case when SUBSTRING(DATENAME(dw,@FechaDia),0,3) = 'MI'  
               Then 'X'  
              Else SUBSTRING(DATENAME(dw,@FechaDia),0,2) End  
            Else SUBSTRING(DATENAME(dw,@FechaDia),0,2) End)  
    set @Rows = (select COUNT(*) from MASERVICIOS_ATENCION Where IDservicio = @IDServicio)
    if @Rows <> 0
		If Exists(select IDAtencion 
					  from MASERVICIOS_ATENCION Where IDservicio =@IDServicio  
					  And (@NumDiaFechaDia >= dbo.FnDiaSemana(Dia1) And @NumDiaFechaDia <= dbo.FnDiaSemana(Dia2)))
			set @pOk = 1
		Else
			set @pOk = 0		
	else
		set @pOk = 1				  
