﻿
--HLF-20130118-Cambiando tabla para el campo IDTipoOC
CREATE Procedure [Dbo].[OPERACIONES_DET_DETSERVICIOS_List_ProveeInternos]     	
	@IdCab Int    
As    
	Set NoCount On    
	
	Select 
	pvp.NombreCorto as DescProveedorProg, 
	Case When Cast(od.IDVoucher as varchar(10))='0' Then '' Else Cast(od.IDVoucher as varchar(10)) End as IDVoucher,
	sd.Descripcion as DescServicio, od.NroPax, od.TotalGen,
	od.idTipoOC, toc.Descripcion as DescTipoOC, 
	sd.TipoGasto, p_v.IDTipoProv 

	From OPERACIONES_DET_DETSERVICIOS odd 
	Inner Join OPERACIONES_DET od 	On odd.IDOperacion_Det=od.IDOperacion_Det
	Inner Join MASERVICIOS_DET sd On odd.IDServicio_Det=sd.IDServicio_Det
	Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
	Inner Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor And p.IDTipoOper='I'
	
	Left Join MAPROVEEDORES pvp On odd.IDProveedor_Prg=pvp.IDProveedor	

	Left Join MASERVICIOS_DET sd_V On odd.IDServicio_Det_V_Cot=sd_V.IDServicio_Det
	Inner Join MASERVICIOS s_v On sd_v.IDServicio=s_v.IDServicio
	Inner Join MAPROVEEDORES p_v On s_v.IDProveedor=p_v.IDProveedor 
	Left Join MATIPOOPERACION toc On toc.IdToc=od.idTipoOC
	
	Where od.IDOperacion In (Select IDOperacion From OPERACIONES Where IDCab=@IdCab)
	--) AS X) as Y
	--Left Join MAPROVEEDORES PG On Y.IDGuiaProgramado=PG.IDProveedor
	--Left Join MAPROVEEDORES PT On Y.IDTransportistaProgramado=PT.IDProveedor
	Order by Orden

