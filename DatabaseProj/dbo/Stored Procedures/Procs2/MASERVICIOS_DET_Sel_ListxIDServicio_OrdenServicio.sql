﻿--JRF-20140520-Ajuste del IDVehiculoProgramado      
--JRF-20140809-Agregar el Ttipo de Opera. Contable.    
--JRF-20140927-Ajustes en el tipo de cambio (Nuevas Monedas)  
--JHD-20150411-IsNull((Select NoVehiculoCitado + ' - ' + CAST(QtCapacidad as varchar(3))+' Pax' From MAVEHICULOS_TRANSPORTE Where NuVehiculo = X.IDVehiculo_Prg),'') As VehiculoProgramado,                          
--HLF-20150630-od.QtPax as PaxmasLiberados
--,od.FlServicioNoShow
--od.QtPax - Isnull(op.NroLiberados,0) as NroPax,
CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_ListxIDServicio_OrdenServicio]
 @IDServicio char(8),                                          
 @IDOperacion_Det int                                          
As                                            
 Set NoCount On                                            
                                              
 Declare @TipoCambio numeric(8,2)                              
 --Select @TipoCambio=TipoCambio From COTICAB Where                               
 --IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                              
  
 Select @TipoCambio=SsTipCam From COTICAB_TIPOSCAMBIO Where                               
 IDCAB In(select IDCAB from OPERACIONES where IDOperacion in (Select IDOperacion From OPERACIONES_DET Where IDOperacion_Det=@IDOperacion_Det))                              
 AND CoMoneda=(Select CoMoneda From MASERVICIOS_DET sd Inner Join OPERACIONES_DET op On sd.IDServicio_Det=op.IDServicio_Det And op.IDOperacion_Det=@IDOperacion_Det)    
   
   
 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)                              
                                              
                                              
 SELECT cast(IDOperacion_Det as varchar(10)) as IDOperacion_Det, DescProveedor_Cot, DescServicio_Cot,                 
 case when IDTipoServ = 'NAP' Then '-' Else IDTipoServ End as IDTipoServ,
 NroPax,nroLiberados,IDioma,                
 IDEstadoSolicitud,                         
 Case When IDVehiculo_Prg is null Then 'NV' Else 'PA' End as IDEstadoVehiculo,                          
 Activo,IDServicio_Prg,                              
 IDServicio, IDServicio_Det, Variante_Prg, DescProveedor_Prg,                         
 --IsNull((Select NoUnidadProg + ' - ' + CAST(QtCapacidadPax as varchar(3))+' Pax' From MAVEHICULOSPROGRAMACION Where NuVehiculoProg = X.IDVehiculo_Prg),'') As VehiculoProgramado,                          
 IsNull((Select NoVehiculoCitado + ' - ' + CAST(QtCapacidad as varchar(3))+' Pax' From MAVEHICULOS_TRANSPORTE Where NuVehiculo = X.IDVehiculo_Prg),'') As VehiculoProgramado,                          
 Anio_Prg, IDTipoProv, IDProveedor_Prg,                             
 IsNull(cast (X.IDVehiculo_Prg as varchar(3)),0) As IDVehiculoProgramado,                           
 CorreoReservasProveedor_Prg, IDServicio_Det_V_Cot, IDServicio_Det_V_Prg,TotalCotizado, IDMoneda,IDMonedaGen, TipoCambio, Total_PrgEditado,                              
 TotalProgramado, ObservEdicion, CostoRealEditado,CostoReal,CostoLiberado,Margen,MargenAplicadoEditado,                                       
 MargenAplicado,MargenLiberado,CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,                                          
 TotImpto,Upd,PaxmasLiberados,                               
                   
 NetoGen,                               
 case when IgvCotizado is null then                      
 Cast(Case When Afecto=1 Then NetoGen*(@NuIgv/100) Else 0 End as numeric(8,2))                      
 else IgvCotizado End as IgvGen,                       
                       
 SimboloMoneda,                              
 Case when isnull(TotalCotizadoCal,0) = 0 then                      
  Cast(Case When Afecto=1 Then                   
  NetoGen+(NetoGen*(@NuIgv/100))                   
  Else                   
  NetoGen                   
  End as numeric(8,2))                       
 else                      
 TotalCotizadoCal                      
 End as TotalGen,                              
 Isnull(CASE WHEN Total_PrgEditado = 0 THEN                 
 CASE WHEN NetoProgram is not null then NetoProgram else NetoGen End ELSE NetoProgram END,0) AS NetoProgram                
 ,                 
 Isnull(Case when  Total_PrgEditado = 0 then                 
  case when IgvProgram is not null then                 
 IgvProgram else                
 case when IgvCotizado is null then                      
  Cast(Case When Afecto=1 Then NetoGen*(@NuIgv/100) Else 0 End as numeric(8,2))                      
  else IgvCotizado End End                
 else IgvProgram End,0) as IgvProgram,                 
 Isnull(case when Total_PrgEditado =0 then                
 case when TotalProgram is not null then TotalProgram else                
  Case when isnull(TotalCotizadoCal,0) = 0 then                      
   Cast(Case When Afecto=1 Then                   
   NetoGen+(NetoGen*(@NuIgv/100))                   
   Else                   
   NetoGen                   
   End as numeric(8,2))         
  else                      
  TotalCotizadoCal                      
  End End                
 else TotalProgram End,0) As TotalProgram,                  
 IngManual,                  
 Activo  ,TipoOperContable                
,FlServicioNoShow
 FROM                              
 (                                       
 Select od.IDOperacion_Det,   od.IgvCotizado,   od.TotalCotizado as TotalCotizadoCal,                      
 isnull(pCot.NombreCorto,p1.NombreCorto) as DescProveedor_Cot,                                          
 isnull(sCot.Descripcion,d1.Descripcion) as DescServicio_Cot,                                          
 --sCot.IDTipoServ,
 s4.IDTipoServ,
 --op.NroPax,
 od.QtPax - Isnull(op.NroLiberados,0) as NroPax,
 Isnull(op.NroLiberados,0) as NroLiberados,idio.Siglas as IDioma,                
 od.IDEstadoSolicitud,                                          
 od.Activo,                          
 s.IDServicio as IDServicio_Prg,               
 d.IDServicio,                                      
 d.IDServicio_Det,                                           
 d2.Tipo as Variante_Prg,                                         
 isnull(pPrg.NombreCorto,pPrgTrsl.Nombre) as DescProveedor_Prg,                                     
 d2.Anio as Anio_Prg,                                          
 isnull(pCot.IDTipoProv,'') as IDTipoProv,                                           
 isnull(pPrg.IDProveedor,'') as IDProveedor_Prg,                                          
 Case When ISNULL(pPrg.Email3,'')='' Then pPrg.Email4 Else pPrg.Email3 End as CorreoReservasProveedor_Prg,                                          
 isnull(od.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,                                          
 isnull(od.IDServicio_Det_V_Prg,0) as IDServicio_Det_V_Prg,                                          
 od.Total As TotalCotizado,                                          
                                         
 od.IDMoneda As IDMoneda,                                        
 isnull(od.tipoCambio ,0) As TipoCambio,                                        
 --(select TipoCambio from OPERACIONES_DET where IDOperacion_Det = od.IDOperacion_Det) As TipoCambio ,                                         
                                         
 od.Total_PrgEditado ,                                        
 IsNull(od.Total_Prg,0.00) as TotalProgramado,                                          
 IsNull(od.ObservEdicion,'') as ObservEdicion,                                          
 0 as CostoRealEditado,        
 od.CostoReal,od.CostoLiberado,od.Margen,                                        
 0 as MargenAplicadoEditado,                                       
 od.MargenAplicado,od.MargenLiberado,                                          
 --od.SSCR,od.SSCL,od.SSMargen,od.SSMargenLiberado,od.SSTotal,od.STCR,od.STMargen,od.STTotal,                                          
 od.CostoRealImpto                                          
 ,od.CostoLiberadoImpto,od.MargenImpto,od.MargenLiberadoImpto,                                          
 --od.SSCRImpto,od.SSCLImpto,od.SSMargenImpto,od.SSMargenLiberadoImpto,od.STCRImpto,od.STMargenImpto,                
 od.TotImpto,'' as Upd,                             
                             
 --CAST(op.NroPax AS VARCHAR(3))+                                        
 --Case When isnull(op.NroLiberados,0)=0 then '' else '+'+CAST(op.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,                            
 --op.NroPax+isnull(op.NroLiberados,0) as PaxmasLiberados,                        
od.QtPax as PaxmasLiberados,
                              
  --Case When isnull(NetoCotizado,0) = 0 then                              
  --dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                               
  --Case When isnull(dCot.TC,0) = 0 Then 'USD' Else 'SOL' End,od.IDMoneda,@TipoCambio)                       
  --else                   
  --NetoCotizado                   
  --End As NetoGen,    
  
  Case When isnull(NetoCotizado,0) = 0 then                              
   Case When dCot.IDServicio_Det IS NULL Then             
  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(od.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                                
   dCot.CoMoneda,od.IDMoneda,@TipoCambio)                       
 Else            
  dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,op.NroPax+isnull(op.NroLiberados,0)),                                
   dCot.CoMoneda,od.IDMoneda,@TipoCambio)                       
 End            
  Else                   
 NetoCotizado                   
  End As NetoGen,                              
                               
 Case When isnull(dCot.TC,0) = 0 Then 'USD' Else 'SOL' End as IDMonedaGen,                      
 isnull(mon.Simbolo,'') as SimboloMoneda,                               
 dCot.Afecto,                               
 --dbo.FnCambioMoneda(dbo.FnCalculoValorReglade3(rd.TotalGen,rd.CostoReal,od.CostoReal/op.NroPax),rd.IDMoneda,od.IDMoneda,@TipoCambio) as TotalGen ,                         
                               
 od.NetoProgram as NetoProgram,                                
 od.IgvProgram as IgvProgram,                                
 od.TotalProgram as TotalProgram                          
                                    
 ,od.IDVehiculo_Prg                  
 ,od.IngManual             
 ,Isnull(too.Descripcion,'') as TipoOperContable       
 ,od.FlServicioNoShow
 From OPERACIONES_DET_DETSERVICIOS od Left Join MASERVICIOS_DET d                                             
 On d.IDServicio_Det = od.IDServicio_Det --and od.IDOperacion_Det = @IDOperacion_Det                                            
 Left Join MASERVICIOS_DET d2 On ISNULL(od.IDServicio_Det_V_Prg,0)=d2.IDServicio_Det                                            
 Left Join MASERVICIOS s  On d2.IDServicio=s.IDServicio                                            
 Left Join MAPROVEEDORES pPrg On od.IDProveedor_Prg=pPrg.IDProveedor                                            
 Left Join MAUSUARIOS pPrgTrsl On od.IDProveedor_Prg=pPrgTrsl.IDUsuario                    
 Left Join MASERVICIOS_DET dCot On ISNULL(od.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det                                            
 Left Join MASERVICIOS sCot  On sCot.IDServicio=dCot.IDServicio                                            
 Left Join MAPROVEEDORES pCot On sCot.IDProveedor=pCot.IDProveedor                                            
 Left Join MASERVICIOS_DET d1 On d1.IDServicio_Det=od.IDServicio_Det                                       
 Left Join MASERVICIOS s1  On d1.IDServicio=s1.IDServicio                                         
 Left Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor                                        
 Left Join OPERACIONES_DET op On od.IDOperacion_Det=op.IDOperacion_Det                                        
 Left Join RESERVAS_DET rd On op.IDReserva_Det=rd.IDReserva_Det                                    
 Left Join MAMONEDAS mon on od.IDMoneda=mon.IDMoneda                                     
 Left Join MAIDIOMAS idio On IsNull(rd.IDIdioma,op.IDIdioma) = idio.IDidioma                
 Left Join MATIPOOPERACION too On d.idTipoOC = too.IdToc
 Left Join MASERVICIOS s4 On d.IDServicio=s4.IDServicio
 Where --d.IDServicio=@IDServicio                                           
 od.IDServicio=@IDServicio AND od.IDOperacion_Det = @IDOperacion_Det and od.Activo = 1                                   
) AS X                         
--where X.TotalProgram > 0                            
