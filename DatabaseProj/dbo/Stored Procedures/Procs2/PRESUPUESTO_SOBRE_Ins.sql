﻿--HLF-20140731-@CoTipoProv char(3)  
--JRF-20150304-Se debe permitir Null en la columna de "CoEjeOpe"
--JRF-20150408-Nuevo campo IDPax (Caso Tour Conductor)
--JHD-20150826-Actualizar código de Fondo Fijo cuando sea Entregado.
--JRF-20151129-Ingresar el bit de separación del tourconduto
CREATE Procedure dbo.PRESUPUESTO_SOBRE_Ins      
 @IDCab int,            
 @CoPrvGui char(6),   
 @IDPax int,
 @CoEjeOpe char(4),      
 @CoEjeFin char(4),      
 @IDProveedor char(6),      
 @CoEstado char(2),      
 @UserMod char(4),    
 @FePreSob smalldatetime,    
 @CoTipoProv char(3),  
 @FlTourConductor bit,
 @pNuPreSob int output    
As      
BEGIN
 Set Nocount On      
       
 Declare @NuPreSob int      
 Execute dbo.Correlativo_SelOutput 'PRESUPUESTO_SOBRE',1,10,@NuPreSob output      
       
 Insert Into PRESUPUESTO_SOBRE(NuPreSob,IDCab,CoPrvGui,IDPax,CoEjeOpe,
							   CoEjeFin,IDProveedor,CoEstado,FePreSob,CoTipoProv,FlTourConductor,UserMod)      
 Values(@NuPreSob,@IDCab,      
 --@CoPrvInt,      
  Case When Ltrim(rtrim(@CoPrvGui))='' Then Null Else @CoPrvGui End,Case When @IDPax=0 Then Null Else @IDPax End,
  Case When ltrim(rtrim(@CoEjeOpe))='' Then Null Else @CoEjeOpe End,      
  Case When ltrim(ltrim(@CoEjeFin))='' Then Null Else @CoEjeFin End,      
  @IDProveedor,      
  @CoEstado,@FePreSob,Case When Ltrim(Rtrim(@CoTipoProv))='' Then Null Else @CoTipoProv End,@FlTourConductor,@UserMod)      
  Set @pNuPreSob = @NuPreSob

	IF @CoEstado='EN'
	BEGIN
		DECLARE @NuFondoFijo int
		set @NuFondoFijo=isnull((select NuFondoFijo from PRESUPUESTO_SOBRE Where NuPreSob=@NuPreSob),0)
		if @NuFondoFijo=0
			begin
					Update PRESUPUESTO_SOBRE    
					Set NuFondoFijo=(select top 1 NuFondoFijo from MAFONDOFIJO where FlActivo=1 and CoPrefijo='FO' ORDER BY FECMOD DESC)
					Where NuPreSob=@NuPreSob  
			end
	END
END;
