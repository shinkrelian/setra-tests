﻿create Procedure dbo.MAPLANCUENTAS_Sel_Sin_CentroCostos
as
select distinct c.CtaContable,c.CtaContable+ ' - ' + c.Descripcion as Descripcion
from 
MAPLANCUENTAS c
where c.flexcluircc =1
Order by c.CtaContable+ ' - ' + c.Descripcion
