﻿--HLF-20120807-Comentar campos IDDesayuno, TelefonoRecepcion2, FaxRecepcion1, FaxRecepcion2, IncluyeServiciosDias
--HLF-20120809-,@IDubigeoNue
--JRF-20151207-Cargar IDCabVarios
CREATE Procedure [dbo].[MASERVICIOS_Ins_NuevoIDServicio]
	@IDServicio	char(8),
	@IDubigeoNue	char(6),
	@UserMod	char(4),
	@pIDServicioNue char(8) Output
As

	Set NoCount On
	
	Declare @DC char(3)=(Select DC From MAUBIGEO Where IDubigeo=@IDubigeoNue)	
	Declare @Correl	smallint=Isnull((Select IsNull(Max(Right(IDServicio,5)),0) From MASERVICIOS Where IDubigeo=@IDubigeoNue),0)+1	
	Declare @IDServicioNue char(8)=@DC+ replicate('0',5-Len(Cast(@Correl as varchar(5)))) +Cast(@Correl as varchar(5))
	
	Insert Into MASERVICIOS
	(IDServicio
		,IDProveedor
		,IDTipoProv           
		,IDTipoServ 
		,Transfer
		,TipoTransporte
		,Descripcion
		,Tarifario
		,Descri_tarifario                     
		,FecCaducLectura
		,IDubigeo
		,Telefono
		,Celular
		,Comision
		,IDCat
		,Categoria
		,Capacidad
		,Observaciones
		,Lectura           

		,AtencionLunes
		,AtencionMartes
		,AtencionMiercoles
		,AtencionJueves
		,AtencionViernes
		,AtencionSabado
		,AtencionDomingo
		,HoraDesde	
		,HoraHasta	           

		,DesayunoHoraDesde
		,DesayunoHoraHasta
		--,IDDesayuno	

		,PreDesayunoHoraDesde
		,PreDesayunoHoraHasta
		,IDPreDesayuno	
		
		,HoraCheckIn
		,HoraCheckOut
		--,TelefonoRecepcion1	
		--,TelefonoRecepcion2	
		--,FaxRecepcion1	
		--,FaxRecepcion2	

		,Dias
		
		,PoliticaLiberado
		,Liberado
		,TipoLib
		,MaximoLiberado	
		,MontoL 		
		--,IncluyeServiciosDias
		
	  	
		,IDServicioCopia
		,IDCabVarios
		,UserMod)
		Select @IDServicioNue		
		,IDProveedor
		,IDTipoProv           
		,IDTipoServ 
		,Transfer
		,TipoTransporte

		,Descripcion
		,Tarifario
		,Descri_tarifario                     
		,FecCaducLectura
		,@IDubigeoNue
		,Telefono
		,Celular
		,Comision
		,IDCat
		,Categoria
		,Capacidad
		,Observaciones
		,Lectura           

		,AtencionLunes
		,AtencionMartes
		,AtencionMiercoles
		,AtencionJueves
		,AtencionViernes
		,AtencionSabado
		,AtencionDomingo
		,HoraDesde	
		,HoraHasta	           

		,DesayunoHoraDesde
		,DesayunoHoraHasta
		--,IDDesayuno	

		,PreDesayunoHoraDesde
		,PreDesayunoHoraHasta
		,IDPreDesayuno	

		,HoraCheckIn
		,HoraCheckOut
		--,TelefonoRecepcion1	
		--,TelefonoRecepcion2	
		--,FaxRecepcion1	
		--,FaxRecepcion2	

		,Dias
		

		,PoliticaLiberado
		,Liberado
		,TipoLib
		,MaximoLiberado	
		,MontoL 		
		--,IncluyeServiciosDias
		
	   	
		
		,@IDServicio
		,IDCabVarios
		,@UserMod
		From MASERVICIOS Where IDServicio=@IDServicio
		
		Set @pIDServicioNue=@IDServicioNue
