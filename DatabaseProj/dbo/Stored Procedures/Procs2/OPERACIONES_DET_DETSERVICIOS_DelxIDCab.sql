﻿
Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_DelxIDCab
	@IDCab	int
As
	Set Nocount On

	DELETE FROM OPERACIONES_DET_DETSERVICIOS WHERE IDOperacion_Det In (select IDOperacion_Det FROM OPERACIONES_DET 
	WHERE idoperacion in (Select IDOperacion FROM OPERACIONES WHERE IDCab=@IDCab))
