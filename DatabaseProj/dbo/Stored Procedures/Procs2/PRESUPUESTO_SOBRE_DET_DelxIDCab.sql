﻿
Create Procedure dbo.PRESUPUESTO_SOBRE_DET_DelxIDCab
	@IDCab int
As
	Set Nocount on
	
	DELETE FROM PRESUPUESTO_SOBRE_DET Where IDOperacion_Det 
		In (Select IDOperacion_Det From OPERACIONES_DET Where IDOperacion 
			in (SElect IDOperacion From OPERACIONES Where IDCab=@IDCab))
			
