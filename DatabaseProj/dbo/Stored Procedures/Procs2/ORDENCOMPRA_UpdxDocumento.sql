﻿--JHD-20150917-SsSaldo= @SsSaldo,
--JHD-20150917-SsDifAceptada= @SsDifAceptada ,    
CREATE Procedure [dbo].[ORDENCOMPRA_UpdxDocumento]    
	@NuOrdComInt int,    
	@CoEstado char(2),    
	@SsSaldo numeric(8,2)=0,    
	@SsDifAceptada numeric(8,2),    
	@TxObsDocumento varchar(Max),    
	@FlDesdeOtraForEgreso bit,  
	@UserMod char(4)    
As    

	Set NoCount On    
	Update ORDENCOMPRA
	 Set CoEstado=@CoEstado,    
	  --SsSaldo= Case When @SsSaldo=0 then Null Else @SsSaldo End,    
	  SsSaldo= @SsSaldo,
	  --SsDifAceptada= Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,      
	  SsDifAceptada= @SsDifAceptada ,      
	  TxObsDocumento=   
	 Case When @FlDesdeOtraForEgreso=0 Then  
	  Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End  
	 Else  
	  TxObsDocumento  
	 End,  
	  UserMod=@UserMod,    
	  FecMod=GETDATE(),    
	  --    
	  UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end    
	Where NuOrdComInt=@NuOrdComInt;
