﻿create Procedure dbo.PRESUPUESTO_SOBRE_Sel_ID_xIDOperacion
	@IDOperacion int
As
begin
Select distinct NuPreSob From PRESUPUESTO_SOBRE_DET Where IDOperacion_Det In 
	(Select IDOperacion_Det From OPERACIONES_DET Where IDOperacion=@IDOperacion)
end
