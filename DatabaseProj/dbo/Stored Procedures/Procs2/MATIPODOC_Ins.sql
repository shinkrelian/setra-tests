﻿--MLL-030614-Agregado la columnas (NoFiscal ,CoSunat ,CoStarSoft)
CREATE Procedure [dbo].[MATIPODOC_Ins]	
	@IdTipoDoc char(3),
	@Descripcion varchar(60),
	@Sunat char(3),
	@UserMod char(4),
	@NoFiscal  varchar(60),
	@CoSunat char(2),
	@CoStarSoft	 char(2)
As
	
	Set NoCount On	
		
	INSERT INTO MATIPODOC
           ([IDtipodoc]
           ,[Descripcion]
           ,[Sunat]
           ,[UserMod]
           ,NoFiscal
		   ,CoSunat
			,CoStarSoft	)
     VALUES
           (@IDtipodoc,
            @Descripcion,
            @Sunat,
            @UserMod,
            @NoFiscal,
		    Case When ltrim(rtrim(@CoSunat))='' Then Null Else @CoSunat End,
			Case When ltrim(rtrim(@CoStarSoft))='' Then Null Else @CoStarSoft End	)
