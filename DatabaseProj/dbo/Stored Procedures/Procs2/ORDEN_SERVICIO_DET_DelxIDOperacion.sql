﻿CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_DelxIDOperacion
@IDOperacion int
As
Set NoCount On
Delete from ORDEN_SERVICIO_DET where IDOperacion_Det In (select od.IDOperacion_Det from OPERACIONES_DET od  where od.IDOperacion = @IDOperacion)
