﻿--HLF-20121212-Agregando s.PoliticaLiberado, s.Liberado      
--HLF-20130408-Agregando s.IDProveedor,p.NombreCorto as DescProveedor      
--HLF-20130522-Agregando s.Descripcion as DescServicio    
--HLF-20130612-Agregando  s.Dias    
--HLF-20140715-SimboloMonedaPais  
--JHD-20150603- ,CoCeCos1=isnull((select top 1 CoCeCos from MAPLANCUENTAS_CENTROCOSTOS where CtaContable=sd.CtaContable),'')
--JHD-20150619- ,CoCeCos1='900101'
--JHD-20160115- ,isnull(FlDetraccion,cast(0 as bit)) AS FlDetraccion1
CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_Pk]      
 @IDServicio_Det int      
As      
       
 Set NoCount On      
       
 Select sd.*, s.TipoLib, s.Liberado,s.IDProveedor, p.NombreCorto as DescProveedor,    
 s.Descripcion as DescServicio, s.Dias,  
 mn.Simbolo as SimboloMonedaPais,
 Case When (select p3.IDTipoProv from MASERVICIOS_DET sdRel Left Join MASERVICIOS s2 On sdRel.IDServicio=s2.IDServicio
 Left Join MAPROVEEDORES p3 On s2.IDProveedor=p3.IDProveedor
 Where sdRel.IDServicio_Det = IsNull(sd.IDServicio_Det_V,0))='004' Then 1 Else 0 End as EsTransportistaCot
 --,CoCeCos1=isnull((select top 1 CoCeCos from MAPLANCUENTAS_CENTROCOSTOS where CtaContable=sd.CtaContable),'')
 ,CoCeCos1='900101'--isnull((select top 1 CoCeCos from MAPLANCUENTAS_CENTROCOSTOS where CtaContable=sd.CtaContable),'')
 ,isnull(FlDetraccion,cast(0 as bit)) AS FlDetraccion1
 From MASERVICIOS_DET sd       
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio      
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor      
 Left Join MAMONEDAS mn On sd.CoMoneda=mn.IDMoneda                 
    
 Where sd.IDServicio_Det=@IDServicio_Det;


