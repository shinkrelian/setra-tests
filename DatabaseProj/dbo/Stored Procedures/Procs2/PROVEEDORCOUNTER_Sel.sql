﻿

-------------------------------------------------



--PROVEEDORCOUNTER_Sel ''

create PROCEDURE PROVEEDORCOUNTER_Sel
@CoProveedor CHAR(6)
AS      
BEGIN
SELECT @CoProveedor= CASE WHEN RTRIM(LTRIM(@CoProveedor))='' THEN NULL ELSE @CoProveedor END

SELECT CoProveedor,
NoRazonSocial,
CoGDS_ID
FROM dbo.PROVEEDORCOUNTER
where flactivo=1
and (CoProveedor=@CoProveedor or @CoProveedor is null)
END;
