﻿
CREATE PROCEDURE [dbo].[MAPROVEEDORES_Upd_Extranet]  
						@IDProveedor char(6),
						@UsuarioLogeo varchar(80)='',  
						@PasswordLogeo varchar(max)='',  
						@UsuarioLogeo_Hash varchar(max),
						@UserMod char(4)
AS
BEGIN
	SET @UsuarioLogeo = ltrim(rtrim(@UsuarioLogeo))
	IF @UsuarioLogeo <> ''
	BEGIN
		IF [dbo].[FnExisteUsuarioProvClie]('', @IDProveedor, 'V', @UsuarioLogeo)=1
		BEGIN
			RAISERROR ('Ya existe el nombre del usuario a ingresar.', 16, 1);
			RETURN
		END
	END
 Set NoCount On
 UPDATE MAPROVEEDORES Set  
            Usuario=Case When ltrim(rtrim(@UsuarioLogeo))='' Then Null Else @UsuarioLogeo End  
           ,Password=Case When ltrim(rtrim(@PasswordLogeo))='' Then Null Else @PasswordLogeo End  
           --,FechaPassword=Case When @FechaPassword='01/01/1900' Then Null Else @FechaPassword End  
		   ,UsuarioLogeo_Hash=Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
		   ,FechaHash=DATEADD(day,14, getdate())
           ,UserMod=@UserMod             
           ,FecMod=GETDATE()
		   ,FlAccesoWeb = 1 
     WHERE  
           IDProveedor=@IDProveedor 
END;
