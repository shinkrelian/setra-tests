﻿
-----------------------------------------------------------

CREATE PROCEDURE NOTAVENTA_COUNTER_ACTIVO_UPD

		   @NuNtaVta char(6),
		   @FlActivo BIT,
           @UserMod char(4)
AS
BEGIN
UPDATE [NOTAVENTA_COUNTER]
   SET 
      FlActivo =@FlActivo
    
      ,[UserMod] = @UserMod
      ,[FecMod] =GETDATE()
      
      
 WHERE [NuNtaVta] = @NuNtaVta
 END;
