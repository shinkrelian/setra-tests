﻿--HLF-20120905-Nuevos campos Email4                                 
--HLF-20130204-Nuevos campos IDFormaPago,OperadorConAlojam,FechaIn                                
--HLF-20130220-Agregando campo CambiosVtasAceptados                              
--HLF-20130304-Agregando Reservas.Estado                          
--HLF-20130320-Agregando (...) as PlanAlimenticio, And (X.IDTipoProv<>'001' OR (X.IDTipoProv='001' And PlanAlimenticio=1)) y Order by                        
--JRF-20130619-Agregando Columnas Nuevas.                      
--JRF-20130808-Agregando col CodTarifa                  
--HLF-20130905-Agregando subquerys para columna CotiDetLog                
--HLF-20130912-Agregando And r.Anulado = 0                
--HLF-20130917-Agregando pinternac.NombreCorto as DescProvInternac,              
--HLF-20130923-Comentando And r.Anulado = 0               
--HLF-20131010-Agregando columna RegeneradoxAcomodo            
--HLF-20131021-Agregando Case When Rv.CambiosenReservasPostFile           
--HLF-20140407-Agregando And r.Anulado = 0       
--JRF-20140911-Ajustes en la Funcion [FnCambiosenReservasPostFileOperaciones]  
--JRF-20150115-Agregar el campo que identifica la Oficina de Fact. [CoUbigeo_Oficina]   
--JRF-20150304-Agregar IsNull(r.Anulado=0)
--JHD-20150612- and r.Estado<>'XL' 
--JRF-20150613-..IsNull(r.Estado,'')
--JRF-20151125-.. And FlServicioTC=0 [SubQuery:fecha]
--JRF-20151210- Agregar la obs. Admin
CREATE Procedure [Dbo].[OPERACIONES_Sel_ListxIDCabNoHoteles]                                
 @IDCab int                                
As                                
 Set NoCount On                                
 Declare @TextAdmin varchar(Max)=''
 Set @TextAdmin = char(13)+'Al *status* esta reserva, le solicitamos adjuntar la copia de la FACTURA / INVOICE  que corresponde a los servicios contratados.'+char(13)
 + 'El documento (FACTURA / INVOICE) debe consignar la siguiente información:'+char(13)
 + 'Nombre comercial: SETOURS S.A.'+char(13)
 + 'RUC: 20100939887'+char(13)
 + 'Dirección: Av. Comandante Espinar 229 - Miraflores, Lima - Perú'+char(13)
 + 'Igualmente debe consignar: Servicio contratado / número de file / importa a pagar en la moneda pactada con SETOURS.'
     
 SELECT * FROM                         
 (                        
 Select Rv.IDProveedor,Pr.NombreCorto as DescProveedor,--  '' as btn,                       
 isnull(pinternac.NombreCorto,'') as DescProvInternac,                      
 Pr.IDTipoProv,                                
 --ISNull(pr.Email3,''),                                
 IsNull(pr.Email3,'') as EmailReservas1,                            
 IsNull(pr.Email4,'') as EmailReservas2,                                
 u.IDPais,                                
 pr.IDCiudad,                                
 u.Descripcion as DescCiudad,                                
 IsNull(r.Estado,'RR') as IDEstado,                                
 IsNuLL(rv.IDReserva,0) As IDReserva, Rv.IDOperacion , Rv.Observaciones,'' As CodReservaProv,                                
 Pr.IDTipoOper,                                 
 '' as IDFormaPago,                                
 (Select COUNT(*) From RESERVAS_DET rd1                                     
 Inner Join RESERVAS r1 On rd1.IDReserva=r1.IDReserva And r1.IDReserva=Rv.IDReserva                                    
 Inner Join MAPROVEEDORES p1 On p1.IDProveedor=r1.IDProveedor And p1.IDTipoProv='003'                                    
 Inner Join MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det And sd1.ConAlojamiento=1                                    
 Where rd1.Anulado=0                       
 ) as OperadorConAlojam  ,                                  
 (Select Top(1) Convert(char(10),rd.Dia,103) from OPERACIONES_DET rd where rd.IDOperacion = rv.IDOperacion and rd.FlServicioTC=0  order by rd.Dia) As FechaIn,                                
                           
 --Case When Exists(Select IDDet From RESERVAS_DET_LOG rdl1 Inner Join RESERVAS r                
 -- On rdl1.IDReserva=r.IDReserva                
 -- Where IDProveedor=Rv.IDProveedor                                              
 --And IDCab=Rv.IDCab And Atendido=0 And Accion='N') Then                                              
 --'N'                                              
 --Else                                                    
 --Case When Exists(Select IDDet From RESERVAS_DET_LOG rdl1 Inner Join RESERVAS r                
 -- On rdl1.IDReserva=r.IDReserva  
 --Where IDProveedor=Rv.IDProveedor        
 --And IDCab=Rv.IDCab And Atendido=0 And Accion='M') Then                              
 --'M'                                              
 --Else                                              
 --Case When Exists(Select IDDet From RESERVAS_DET_LOG rdl1 Inner Join RESERVAS r                
 -- On rdl1.IDReserva=r.IDReserva                
 --Where IDProveedor=Rv.IDProveedor                                              
 --And IDCab=Rv.IDCab And Atendido=0 And Accion='B') Then                                      
 --'E'                                              
 --Else                                              
 --'O'                     
 --End                                      
 --End                                          
 --End            
 --Case When Rv.CambiosenReservasPostFile = 1 Then           
 Case When dbo.FnCambiosenReservasPostFileOperaciones(Rv.IDOperacion) = 1 Then      
    'M'          
   Else                                             
    'O'                                            
   End          
 As CotiDetLog,             
  0 AS CambiosVtasAceptados  ,                            
 --'' As DescEstado,                        
 Case When Exists                        
 (Select rd1.IDReserva From                         
 RESERVAS_DET rd1 Inner Join MASERVICIOS_DET sd1 On rd1.IDServicio_Det=sd1.IDServicio_Det                        
 And sd1.PlanAlimenticio=1                        
 Where rd1.IDReserva=Rv.IDReserva And rd1.Anulado=0) Then 1 Else 0 End as PlanAlimenticio                 
  ,IsNull(Pr.ObservacionPolitica,'') as ObservacionPolitica,                      
 IsNull(Pr.DocPdfRutaPdf,'') as DocPdfRutaPdf   ,                    
 '' as EstadoSolicitud  ,'' as CodTarifa,            
 0 as RegeneradoxAcomodo, IsNull(Pr.CoUbigeo_Oficina,'') as CoUbigeo_Oficina  ,dbo.FnExistenAlojamiento(RV.IDReserva) as ExistAlojamiento,
 Case When Pr.CoUbigeo_Oficina='000065' And Pr.IDFormaPago = '002' Then @TextAdmin else '' End As ObservacionAdmHotel
 From OPERACIONES Rv Left Join MAPROVEEDORES Pr On Rv.IDProveedor= Pr.IDProveedor                                
 Left Join MAUBIGEO u On pr.IDCiudad=u.IDubigeo                          
 Left Join RESERVAS r On Rv.IDReserva=r.IDReserva                     
 Left Join MAPROVEEDORES pinternac On pr.IDProveedorInternacional=pinternac.IDProveedor                   
 Where rv.IDCab=@IDCab --And Pr.IDTipoProv<>'001'                           
 And IsNull(r.Anulado,'') = 0    
  --jorge
 and IsNull(r.Estado,'')<>'XL'                      
 ) AS X                        
 WHERE  (X.IDTipoProv<>'001' OR (X.IDTipoProv='001' And PlanAlimenticio=1))                                    
 --Order by u.Descripcion                         
 Order by Cast(X.FechaIn as smalldatetime)                          
