﻿CREATE Procedure [dbo].[MAPROVEEDORES_Sel_LvwxRazonComercial]
	@ID	char(6),
	@Descripcion varchar(100)
As
	Set NoCount On
	
	Select NombreCorto
	From MAPROVEEDORES
	Where (NombreCorto Like '%'+@Descripcion+'%')
	And (IDProveedor <>@ID Or ltrim(rtrim(@ID))='')
	Order by NombreCorto
