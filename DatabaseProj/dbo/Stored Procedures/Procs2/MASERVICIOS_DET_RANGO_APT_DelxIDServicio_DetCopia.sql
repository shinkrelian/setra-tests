﻿

--PPMG-20151105-Se agrego el eliminar MASERVICIOS_DET_DESC_APT
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_DelxIDServicio_DetCopia]
	@IDServicio	char(8),
	@IDServicio_DetCopia int
AS
BEGIN
	Set NoCount On

	Delete From MASERVICIOS_DET_RANGO_APT
	Where IDServicio_Det In
	(Select IDServicio_Det From MASERVICIOS_DET 
	Where IDServicio_DetCopia=@IDServicio_DetCopia And IDServicio=@IDServicio)

	IF NOT EXISTS(SELECT IDServicio_Det, IDTemporada From MASERVICIOS_DET_RANGO_APT
				  Where IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET 
										   Where IDServicio_DetCopia=@IDServicio_DetCopia And IDServicio=@IDServicio)
				  GROUP BY IDServicio_Det, IDTemporada)
	BEGIN
		DELETE FROM MASERVICIOS_DET_DESC_APT
		Where IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET 
										   Where IDServicio_DetCopia=@IDServicio_DetCopia And IDServicio=@IDServicio)
	END
END
