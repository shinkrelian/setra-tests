﻿--JRF-20140505-Ordenarlo por el nombre del Usuario y Agregar el Usuario POOL
CREATE Procedure dbo.MAUSUARIOS_Sel_CboxOpcion  
@DescOpcion varchar(50),  
@bTodos bit  
As  
 Set NoCount On  
 select IDUsuario,Usuario from (  
 select '' As IDUsuario,'<TODOS>' As Usuario,0 As Ord  
 union  
 select distinct u.IDUsuario,u.Usuario,1 As Ord  
 from MAUSUARIOSOPC uo Left Join MAUSUARIOS u On uo.IDUsuario = u.IDUsuario  
 Left Join MAOPCIONES o On uo.IDOpc=o.IDOpc  
 Where uo.Consultar = 1 and u.Activo ='A'   
 and LTRIM(rtrim(o.Nombre))=LTRIM(rtrim(@DescOpcion))  
 and IdArea <> 'SI'  
 Union
 Select IDUsuario,Usuario,1 as Ord from MAUSUARIOS   
 Where (Activo='I' and IdArea = 'UN') 
 ) As X  
 Where (@bTodos = 1 Or X.Ord <> 0)  
 order by X.Usuario --X.IDUsuario  
