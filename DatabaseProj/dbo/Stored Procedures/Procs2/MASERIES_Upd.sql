﻿Create Procedure [dbo].[MASERIES_Upd]
	@IDSerie char(3),
    @Descripcion varchar(60),
    @UserMod char(4)    
As
	Set NoCount On

	Update MASERIES Set            
           Descripcion=@Descripcion,
           UserMod=@UserMod,
           FecMod=getdate()
     Where 
           IDSerie=@IDSerie
