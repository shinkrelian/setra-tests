﻿CREATE Procedure [dbo].[OPERACIONES_SelProvExternosxIDCabExistsOutput]  
 @IDCab int,        
 @pbExists bit Output        
As        
 Set Nocount On        
    
 Set @pbExists = 0        
 If Exists(Select IDOperacion From OPERACIONES o   
 Inner Join RESERVAS r On r.IDReserva=o.IDReserva And    
  r.IDCab=o.IDCab And r.Anulado=0   
  Where o.IDCab=@IDCab And o.IDProveedor In (Select IDProveedor   
  From MAPROVEEDORES Where IDTipoOper ='E' and Activo='A')  )          
  
 Set @pbExists = 1      
