﻿--JRF-20150409-Considerar como filtro el Nuevo campo
CREATE Procedure dbo.PRESUPUESTO_SOBRE_SelxIDCabxIDProveedorxIDProvGuia
@IDcab int,
@IDProveedor char(6),
@CoPrvGuia char(6),
@IDPax int
As
Set NoCount On
Select * 
from PRESUPUESTO_SOBRE 
where IDCab=@IDcab and IDProveedor=@IDProveedor 
and IsNull(CoPrvGui,'')=Ltrim(rtrim(@CoPrvGuia)) and IsNull(IDPax,0)=@IDPax
