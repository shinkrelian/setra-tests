﻿
  
Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_DelxIDReserva_Det  
	@IDReserva_Det int  
As  
	Set Nocount On  
   
	 DELETE FROM OPERACIONES_DET_DETSERVICIOS WHERE 
		IDOperacion_Det In (select IDOperacion_Det FROM OPERACIONES_DET   
			WHERE IDOperacion_Det in   
			  (Select IDOperacion_Det FROM OPERACIONES_DET WHERE IDReserva_Det In   
			   (@IDReserva_Det)))  

