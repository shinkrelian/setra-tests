﻿CREATE PROCEDURE MAPROVEEDORES_MATIPODETRACCION_Del
	@IDProveedor char(6),
	@CoTipoDetraccion char(5)
AS
BEGIN
	Delete [dbo].[MAPROVEEDORES_MATIPODETRACCION]
	Where 
		[IDProveedor] = @IDProveedor And 
		[CoTipoDetraccion] = @CoTipoDetraccion
END
