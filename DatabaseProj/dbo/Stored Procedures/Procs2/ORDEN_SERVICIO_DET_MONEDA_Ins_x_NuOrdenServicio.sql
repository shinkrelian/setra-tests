﻿CREATE Procedure ORDEN_SERVICIO_DET_MONEDA_Ins_x_NuOrdenServicio
 @NuOrden_Servicio int,  
 @UserMod char(4) 
 AS
 BEGIN

 Declare @NuIgv numeric(5,2)=(Select NuIGV from PARAMETRO)   

	delete from ORDEN_SERVICIO_DET_MONEDA where NuOrden_Servicio=@NuOrden_Servicio
	INSERT INTO ORDEN_SERVICIO_DET_MONEDA
	select 
		NuOrden_Servicio,
		IDMoneda,
		CoEstado,
			SUM(    
			isnull(Case when isnull(TotalCotizadoCal,0) = 0 then 
			Cast(Case When Afecto=1 Then NetoGen+(NetoGen*(@NuIgv/100))
			 Else NetoGen End as numeric(8,2)) else TotalProgram End,0)
			) as TotalGen,
		SsSaldo=Sum(isnull((isnull(Case when isnull(TotalCotizadoCal,0) = 0 then Cast(Case When Afecto=1 Then NetoGen+(NetoGen*(@NuIgv/100)) Else NetoGen End as numeric(8,2)) else TotalProgram End,0)),0))-isnull((select sum(isnull(sstotal,0)) from DOCUMENTO_PROVEEDOR where NuOrden_Servicio=X.NuOrden_Servicio and FlActivo=1 and CoMoneda=X.IDMoneda),0),
			--SSDifAceptada=case when v.CoEstado='AC' THEN (Sum(TotalGen)-(select sum(sstotal) from DOCUMENTO_PROVEEDOR where NuVoucher=@IDVoucher and FlActivo=1 and CoMoneda=od.IDMoneda)) ELSE 0 END,
			SSDifAceptada=case when X.CoEstado='AC' THEN (Sum(isnull(isnull(Case when isnull(TotalCotizadoCal,0) = 0 then Cast(Case When Afecto=1 Then NetoGen+(NetoGen*(@NuIgv/100)) Else NetoGen End as numeric(8,2)) else TotalProgram End,0),0))-isnull((select sum(isnull(sstotal,0)) from DOCUMENTO_PROVEEDOR where NuVoucher=X.NuOrden_Servicio and FlActivo=1 and CoMoneda=X.IDMoneda),0)) ELSE 0 END,
			UserMod='0001',
			FecMod=getdate(),
			''
		/*
		TotalProgram,
		TotalCotizadoCal,
		Afecto,
		NetoGen
		*/
 from (
select --odds.*
		osv.NuOrden_Servicio,odds.IDMoneda,osv.CoEstado,
		isnull(TotalProgram,0) as TotalProgram,
		isnull(odds.TotalCotizado,0) as TotalCotizadoCal,
		dCot.Afecto,
		
		Case When isnull(odds.NetoCotizado,0) = 0 then                                                     
			Case When dCot.IDServicio_Det IS NULL Then               
			dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(odds.IDServicio_Det,os.NroPax+isnull(os.NroLiberados,0)),                        
			dCot.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)    
			Else              
			dbo.FnCambioMoneda(dbo.FnNetoMaServicio_Det(dCot.IDServicio_Det,os.NroPax+isnull(os.NroLiberados,0)),                      
			dCot.CoMoneda,odds.IDMoneda,odds.TipoCambio) --@TipoCambio)    
			End              
			Else                     
			odds.NetoProgram    
		End As NetoGen

	From OPERACIONES_DET_DETSERVICIOS odds    
	inner Join ORDEN_SERVICIO_DET osd On odds.IDServicio_Det=osd.IDServicio_Det
	and odds.IDOperacion_Det=osd.IDOperacion_Det
	inner Join ORDEN_SERVICIO osv On osv.NuOrden_Servicio=osd.NuOrden_Servicio
	inner join operaciones_det os on os.IDOperacion_Det=odds.IDOperacion_Det
	inner join operaciones o on o.IDOperacion=os.IDOperacion
	inner Join MASERVICIOS_DET dCot On Isnull(odds.IDServicio_Det_V_Cot,0)=dCot.IDServicio_Det
	--inner Join RESERVAS_DET rdd On os.IDReserva_Det=rdd.IDReserva_Det  
	where osv.NuOrden_Servicio=@NuOrden_Servicio --osv.IDProveedor='002574' and osv.IDCab=10911
	--where odds.IDMoneda is null
			--Left Join ORDEN_SERVICIO os On osd.IDServicio_Det=odds.IDServicio_Det and osd.IDOperacion_Det=odds.IDOperacion_Det and odds.Activo=1    
		) as x
		group by NuOrden_Servicio,
		IDMoneda,
		CoEstado
		order by NuOrden_Servicio
END;

