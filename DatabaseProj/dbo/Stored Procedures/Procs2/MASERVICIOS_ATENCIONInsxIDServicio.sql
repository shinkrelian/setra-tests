﻿Create Procedure dbo.MASERVICIOS_ATENCIONInsxIDServicio
@IDServicio char(8),
@IDServicioNuevo char(8)
As
Set NoCount On
Insert into MASERVICIOS_ATENCION
SELECT [IDAtencion]
      ,@IDServicioNuevo as [IDservicio]
      ,[Dia1]
      ,[Dia2]
      ,[Desde1]
      ,[Hasta1]
      ,[Desde2]
      ,[Hasta2]
      ,[Mig]
      ,[UserMod]
      ,[FecMod]
  FROM [dbo].[MASERVICIOS_ATENCION]
Where IDServicio = @IDServicio
