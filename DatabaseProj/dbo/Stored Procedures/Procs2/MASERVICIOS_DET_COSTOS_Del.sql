﻿CREATE Procedure [dbo].[MASERVICIOS_DET_COSTOS_Del]
	@Correlativo tinyint,
    @IDServicio_Det int
As
	Set NoCount On
	
	Delete From MASERVICIOS_DET_COSTOS 
	WHERE
           Correlativo=@Correlativo And
           (IDServicio_Det=@IDServicio_Det Or 
           IDServicio_Det In 
           (Select IDServicio_Det From MASERVICIOS_DET
            Where IDServicio_Det_V=@IDServicio_Det ))
