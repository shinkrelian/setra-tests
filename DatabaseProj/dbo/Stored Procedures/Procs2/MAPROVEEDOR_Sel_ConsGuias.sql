﻿--JHD-20150505-@CoPais char(6)=''
--JHD-20150505-.. AND (up.IDubigeo =@CoUbigeo or rtrim(ltrim(@CoPais))='') 
--JRF-20151012-Mayusculas nombres..
--JRF-20151013-Dif. Guía - Trasladista (Lima/Cusco)
--JRF-20151026-..+ Case When Ltrim(Rtrim(@IDUbigeo))='' then up.Descripcion Else '' End
--JRF-20151026-Insert into @tmpControlCalidad...
CREATE Procedure dbo.MAPROVEEDOR_Sel_ConsGuias  
 @IDUbigeo char(6),  
 @IDIdioma varchar(12),  
 @Guia_Estado char(1),  
 @Nombre varchar(100),
 @CoPais char(6),
 @IDTipoProv char(3),
 @RatingMin smallint,
 @RatingMax smallint
As  
 Set NoCount On  
 --0 to 2
	if @RatingMax = 0
		Set @RatingMax = 5

  select * from dbo.FnDevuelveSelectTableFeedBack(@RatingMin,@RatingMax,@IDUbigeo,@IDIdioma,@Guia_Estado,@Nombre,@CoPais, @IDTipoProv)
