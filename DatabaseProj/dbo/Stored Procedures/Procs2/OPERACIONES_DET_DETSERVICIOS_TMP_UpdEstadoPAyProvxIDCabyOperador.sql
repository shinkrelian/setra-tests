﻿CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_TMP_UpdEstadoPAyProvxIDCabyOperador  
 @IDCab int,  
 @IDProveedorOper char(6),      
 @IDProveedor_Prg char(6),    
 @IDTipoProvProg char(3),         
 @UserMod char(4)    
As    
 Set NoCount On    
  
 Update OPERACIONES_DET_DETSERVICIOS_TMP Set    
 IDEstadoSolicitud='PA',    
 IDServicio_Det_V_Prg=IDServicio_Det_V_Cot,   
 IDProveedor_Prg=@IDProveedor_Prg,  
 Total_Prg=Total,    
 UserMod=@UserMod,     
 FecMod=GETDATE()    
 From OPERACIONES_DET_DETSERVICIOS_TMP od     
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det_V_Cot=sd.IDServicio_Det    
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio    
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor    
 Where IDOperacion_Det IN (SELECT IDOperacion_Det FROM OPERACIONES_DET_TMP  
 WHERE IDOperacion IN (SELECT IDOperacion FROM OPERACIONES WHERE IDCab=@IDCab And IDProveedor=@IDProveedorOper))   
 And p.IDTipoProv=@IDTipoProvProg  
     
