﻿Create Procedure [Dbo].[OPERACIONES_DET_DETSERVICIOS_DelxIDOperacion]
@IDOperacion int
As
	Set NoCount On
	Delete from OPERACIONES_DET_DETSERVICIOS 
	where IDOperacion_Det in (select IDOperacion_Det from OPERACIONES_DET where IDOperacion = @IDOperacion)
