﻿
--HLF-20160106-dp.SSTotal as Total, inner join DOCUMENTO_PROVEEDOR dp on p.NuPreSob=dp.NuPreSob
--HLF-20160107-... as SaldoIni
 CREATE Procedure dbo.PRESUPUESTO_SOBRE_SelTicketxNuDocumProv
	@NuDocumProv int
 As
	Set Nocount on
	
	Select p.NuPreSob as Codigo, p.NuPreSob as IDVoucher, c.Titulo, p.IDCab, c.IDFile, 
	'SOL' as IDMoneda, 'S/.' as Simbolo, 
	IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE PD2.NuPreSob=p.NuPreSob 
		--And IsNull(IDMoneda,'')='SOL' and PD2.CoPago = 'TCK'
		),0) As SaldoIni,
	--p.SsSaldo as Total, 
	dp.SSTotal as Total, 
	0 as IGV, p.CoEstadoFormaEgreso
	From PRESUPUESTO_SOBRE p inner join COTICAB c on p.IDCab=c.IDCAB
	inner join DOCUMENTO_PROVEEDOR dp on p.NuPreSob=dp.NuPreSob
	Where  p.NuPreSob in (Select NuPreSob From DOCUMENTO_PROVEEDOR Where NuDocum_Multiple=@NuDocumProv)

