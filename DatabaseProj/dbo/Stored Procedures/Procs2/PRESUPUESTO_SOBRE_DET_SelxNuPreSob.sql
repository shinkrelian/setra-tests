﻿Create Procedure dbo.PRESUPUESTO_SOBRE_DET_SelxNuPreSob
@NuPreSob int
as
Set NoCount On
Select IDServicio_Det,NuDetPSo,Isnull(NuSincerado_Det,0) as NuSincerado_Det,FeDetPSo,'' as DescProveeGuia,TxServicio,QtPax,SsPreUni,
	   SsTotal,SsTotSus,SsTotDev,0 as RegistradoPresup,0 as RegistradoSincer,
	   psd.CoTipPSo,tpp.NoTipPSo as DescCoTipPSo,FlTicket
from PRESUPUESTO_SOBRE_DET psd Left Join MATIPOPRESUPUESTO_SOBRE tpp On psd.CoTipPSo = tpp.CoTipPSo
where psd.NuPreSob = @NuPreSob
Order by FeDetPSo
