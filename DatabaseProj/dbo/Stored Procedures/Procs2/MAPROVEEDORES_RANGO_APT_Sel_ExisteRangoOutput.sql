﻿
create Procedure [dbo].[MAPROVEEDORES_RANGO_APT_Sel_ExisteRangoOutput]    
    @IDProveedor int,     
	@NuAnio CHAR(4), 
    @NroPax smallint,    
    @NroLiberados smallint,    
    @pExiste bit Output    
As         
 Set Nocount On      
  
 Set @pExiste=0    
 If Exists(Select Monto From MAPROVEEDORES_RANGO_APT      
 Where IDProveedor=@IDProveedor and NuAnio=@NuAnio And      
 (@NroPax + @NroLiberados) >= PaxDesde And (@NroPax + @NroLiberados) <= PaxHasta)
  Set @pExiste=1  
 Else  
  If (Select Max(PaxHasta) From MAPROVEEDORES_RANGO_APT      
  Where IDProveedor=@IDProveedor and (NuAnio=@NuAnio))>=40 and (@NroPax+@NroLiberados)>=40 
   Set @pExiste=1
