﻿Create Procedure [dbo].[MAPLANCUENTAS_Sel_Lvw]    
 @CtaContable varchar(8),    
 @Descripcion varchar(60)    
As    
 Set NoCount On    
     
 Select Descripcion     
 From MAPLANCUENTAS    
 Where (Descripcion Like '%'+@Descripcion+'%')    
 And (CtaContable<>@CtaContable Or ltrim(rtrim(@CtaContable))='')    
 Order by Descripcion    
