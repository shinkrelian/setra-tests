﻿--JHD-20150819-Mostrar nombre de almacén
CREATE procedure MAPRODUCTOS_Sel_Pk
@CoProducto char(6)
as
select p.*,
NoAlmacen=isnull(a.NoAlmacen,'')
from MAPRODUCTOS p
left join MAALMACEN a on p.CoAlmacen=a.CoAlmacen
where  ( rtrim(ltrim(p.CoProducto)) = @CoProducto );
