﻿Create Procedure [dbo].[MASERVICIOS_Det_Sel_xIDServicioAnioServicio]    
 @IDServicio char(8),    
 @Anio char(4),    
 @Tipo varchar(7),
 @Servicio varchar(200)
As    
    
 Set NoCount On    
     
 Select IDServicio_Det, Monto_sgl,Monto_dbl,Monto_tri,PoliticaLiberado,TipoLib,Liberado,    
 MontoL,MaximoLiberado,Afecto, ConAlojamiento, DiferSS, DiferST, TC, IDServicio_Det,    
 Monto_sgls, Monto_dbls, Monto_tris, idTipoOC as OperacContable, VerDetaTipo  
 From MASERVICIOS_DET    
 Where IDServicio=@IDServicio And     
 Anio=@Anio And    
 Tipo=@Tipo And LTRIM(rtrim(upper(Descripcion)))=LTRIM(rtrim(upper(@Servicio)))   
 And Estado='A'    
