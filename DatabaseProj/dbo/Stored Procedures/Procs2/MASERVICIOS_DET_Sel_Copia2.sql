﻿
Create Procedure [dbo].[MASERVICIOS_DET_Sel_Copia2]
	@IDServicio char(8),
	@AnioNue	char(4),
	@Tipo	varchar(7)
As
	
	Set NoCount On
	
	Select 
	d.IDServicio_Det, d.IDServicio_Det_V, d.IDServicio_DetCopia
	From MASERVICIOS_DET d 				
	Where d.IDServicio=@IDServicio And   
		d.Anio=@AnioNue And
		(d.Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')   
		And Not d.IDServicio_DetCopia is Null
		And d.IDServicio_Det Not In (Select IDServicio_Det From MASERVICIOS_DET_COSTOS)
	Order by Anio, Tipo, d.Correlativo




