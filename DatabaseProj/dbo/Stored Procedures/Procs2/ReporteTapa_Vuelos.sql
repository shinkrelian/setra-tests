﻿--JRF-20120905-Alteracion de Filtros:Se agrego un rango de fechas para el dia del servicio. Campo(Fec_Salida [COTIVUELOS])                
--JRF-20130806-Orden de Vuelos en la tapa.              
--JRF-20130826-Mostrar los Buses.        
--JRF-20130919-Cambio de FechaSalida x Fecha de Emisión.      
--JRF-20150218-IsNull(Fec_Salida,Fec_Emision)
--JRF-20150223-Not(rd.FlServicioIngManual=1 and rd.FlServicioNoShow=1) No Considerar NoShow
--JHD-20150506-@CoPais char(6)=''
--JHD-20150506-And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=rd.IDubigeo)=@CoPais )
--JRF-20150805-..IsNull((select count(*) from COTITRANSP_PAX Where IDTransporte=Cv.ID),0) as CantPax
CREATE Procedure ReporteTapa_Vuelos                   
@IDCab int,                  
@IDUbigeo char(6) ,                
@RangoFecIni smalldatetime,                   
@RangoFecFin smalldatetime,
@CoPais char(6)                          
As                   
 Set NoCount On                  
 SELECT * FROM(                  
 select distinct               
 case when cv.TipoTransporte = 'V' then 0         
 else         
 case when cv.TipoTransporte='B' then 1 else 2 End        
 end as Ord,cv.TipoTransporte, cc.IDFile,cc.Cotizacion,cc.Titulo,Ruta,              
 Case when p.Sigla is null then p.NombreCorto else p.Sigla End  As LineaAerea, Vuelo,  
 --Case when Fec_Emision is null then Fec_Salida else Fec_Emision End as Fec_Salida,  
 IsNull(Fec_Salida,Fec_Emision) as Fec_Salida,  
 CONVERT(CHAR(5),Salida,108) Salida                  
 ,CONVERT(CHAR(5),Llegada,108) Llegada,CodReserva,Status,Emitido,           
 cast(cc.Observaciones As Varchar(max)) Observaciones,IsNull((select count(*) from COTITRANSP_PAX Where IDTransporte=Cv.ID),0) as CantPax
 from RESERVAS_DET rd                   
 Inner Join RESERVAS r On rd.IDReserva=r.IDReserva And r.IDCab=@IDCab                     
 Left Join COTICAB cc On cc.IDCAB=r.IDCab                    
 Left Join COTIVUELOS cv On cc.IDCAB = cv.IDCAB                   
 Left Join MAPROVEEDORES p On cv.IdLineaA = p.IDProveedor                  
 Where ISNULL(rd.FlServicioNoShow,0)=0 and (rd.IDubigeo=@IDUbigeo Or ltrim(rtrim(@IDUbigeo))='')   
 And ((rtrim(ltrim(@CoPais))='') or (select top 1 IDPais from maubigeo u where u.IDubigeo=rd.IDubigeo)=@CoPais )   
 And ((rd.Dia between @RangoFecIni and DATEADD(s,-1,DATEADD(d,1,@RangoFecFin)))          
 Or (@RangoFecIni = '01/01/1900' And @RangoFecFin = '01/01/1900'))                
 ) AS X              
 Order by X.Ord, cast ((CONVERT(varchar(10),X.Fec_Salida,103)+' '+ Salida) as datetime)                
