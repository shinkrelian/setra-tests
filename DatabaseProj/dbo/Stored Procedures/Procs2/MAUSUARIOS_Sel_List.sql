﻿CREATE Procedure [dbo].[MAUSUARIOS_Sel_List]
	@Nombre	Varchar(60)
As
	Set NoCount On
	
	Select Usuario, Nombre, Correo, Nivel, IDUsuario
	From MAUSUARIOS
	Where (Nombre Like '%'+ @Nombre +'%' Or LTRIM(RTRIM(@Nombre))='')
	And Activo='A'
