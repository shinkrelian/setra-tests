﻿--HLF-20151125-ISNULL(CoPrvGui,''), ISNULL(p.NombreCorto,'Tickets')
CREATE Procedure dbo.PRESUPUESTO_SOBRE_SelxIDCabxIDProveedor
@IDCab int,
@IDProveedor char(6),
@EsTourConductor bit
As
Set NoCount On
select Cast(NuPreSob as varchar(5))+'|'+
	   Case When ps.IDPax is not null then Cast(ps.IDPax As varchar) else ISNULL(CoPrvGui,'') End as CodPresupuesto,
	   --'PRESUPUESTO #'+Cast(ROW_NUMBER()Over(Order By FePreSob) as varchar(2)) as DescripcionPresub
	   Case When ps.IDPax is not null then tc.Titulo+' '+tc.Apellidos+' '+tc.Nombres else ISNULL(p.NombreCorto,'Tickets') End
	   +' ('+Convert(char(10),ps.FePreSob,103)+')' as DescripcionPresub
from PRESUPUESTO_SOBRE ps Left Join MAPROVEEDORES p On dbo.FnDevuelveIDProveedorxIDUserTrasladista(ps.CoPrvGui)=p.IDProveedor
Left Join (Select top(1)* from COTIPAX where IDCab=@IDCab and FlTC=1) tc On ps.IDPax=tc.IDPax
Where ps.IDCab=@IDCab And ps.IDProveedor=@IDProveedor
And Cast(Case When ps.IDPax is not null then 1 Else 0 End as bit)= @EsTourConductor
Order By FePreSob

