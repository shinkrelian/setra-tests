﻿--create
CREATE PROC MAPLANCUENTAS_Sel_List_FlFacturacion_CentroCostos
@CoCeCos char(6)=''
as
/*
SELECT CtaContable,Descripcion 
FROM MAPLANCUENTAS WHERE FlFacturacion=1 order by CtaContable
*/
select distinct c.CtaContable,isnull(c.Descripcion,'') as Descripcion
from MAPLANCUENTAS_CENTROCOSTOS pc
inner join MAPLANCUENTAS c on pc.CtaContable=c.CtaContable
where
(pc.CoCeCos=@CoCeCos Or LTRIM(rtrim(@CoCeCos))='')   
and c.FlFacturacion=1
 Order by c.CtaContable
