﻿
Create Procedure dbo.ORDEN_SERVICIO_DET_SelxVoucher
	@IDCab int,
	@IDVoucher int
As
	Set Nocount On
	
	SELECT Distinct NuOrden_Servicio
	FROM ORDEN_SERVICIO_DET WHERE NuOrden_Servicio IN (SELECT NuOrden_Servicio FROM ORDEN_SERVICIO
		WHERE IDCab=@IDCab)
	AND IDServicio_Det In 
	(SELECT IDServicio_Det
	FROM OPERACIONES_DET WHERE IDOperacion IN (SELECT IDOperacion FROM OPERACIONES 
		WHERE IDCab=@IDCab)
	AND IDVoucher=@IDVoucher)
	
