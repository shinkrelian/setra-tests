﻿CREATE Procedure [dbo].[RESERVAS_DET_SelPendientesxIDCabyIDProvee]
	@IDCab	int,
	@IDProveedor	char(6)
As

	Set NoCount On
	
	Select Distinct rd.IDReserva, --rd.Servicio 
	sd.Descripcion as Servicio
	From RESERVAS_DET rd Inner Join RESERVAS r On rd.IDReserva=r.IDReserva
	And r.IDCab=@IDCab And r.IDProveedor=@IDProveedor
	Left Join MASERVICIOS_DET sd On rd.IDServicio_Det=sd.IDServicio_Det
	Where rd.IDDet IS Null --And (Not IDEmailEdit Is Null Or Not IDEmailNew Is Null )
