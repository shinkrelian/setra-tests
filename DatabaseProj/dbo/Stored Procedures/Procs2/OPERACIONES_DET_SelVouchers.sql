﻿--JRF-20130812-Cambios Pdf. Segun Peter(Screen)                  
--HLF-20130816-Cambiando subquery para ObservVoucher                  
--JRF-20130821-Colocar la observacion de Detalle.              
--JRF-20130910-Orden por Fecha IN.            
--JRF-20140516-Ajuste en el NroPax de la Cotizacion        
--JRF-20140816-Considerar +Isnull(c.NroLiberados,0)      
--JRF-20140912-Considerar la Observacion Biblia como ObsVoucher  
--JRF-20140915-Unir las Observaciones de Biblia  
--HLF-20141119-Case When sd.VerDetaTipo=1 Then
--		ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'')
--	Else
--		''
--	End as CodTarifaDet
--JRF-20150416-Considerar (NoShow como texto en el Descriptivo)
--JRF-20150617-Siempre considerar la tarifa por default
--JHD-20150829-Nuevos campos: DescProveedor_Int, Direccion_Int   
--JHD-20150829-case when isnull(p.IDProveedorInternacional,'')<>'' then p.NombreCorto+ ': ' + od.Servicio+ Case When od.FlServicioNoShow=1 Then ' (No Show)' else '' End else  od.Servicio+ Case When od.FlServicioNoShow=1 Then ' (No Show)' else '' End end as Servicio,
CREATE Procedure dbo.OPERACIONES_DET_SelVouchers                    
 @IDOperacion int                    
As                    
 Set Nocount On                    
                    
 SELECT                     
 IDVoucher, IDFile, DescProveedor,Direccion,                     
 Titulo,Pax,Responsable, CodReservaProv, IDIdioma, IdiomaServicio, Nacionalidad,                    
                     
 CASE WHen LEN(DATEPART(DD,Dia)) = 1 then '0'+CAST(DATEPART(DD,Dia)as varchar(10)) else                  
 CAST(DATEPART(DD,Dia) as varchar(10)) End + ' ' +                  
 SUBSTRING(DATENAME(MM,Dia),1,3)                  
 +' ' + SUBSTRING(CAST(DATEPART(YEAR,Dia) AS VARCHAR(10)),3,4) as FechaIn,                      
                   
 IsNull(upper(substring(DATENAME(dw,FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,FechaOut,103))),'') as FechaOut,                       
 substring(convert(varchar,Dia,108),1,5) as Hora,                    
                     
 Cantidad, Servicio, Buffet, ObservVoucher  ,TipoServ, ExtraNoIncluido    ,ObservInterno  ,Dia    ,NroPaxCoti    ,CantServicios,    
 DescripcionTarifa, CodTarifaDet,
	DescProveedor_Int,
	Direccion_Int      
 FROM                    
                     
 (                    
 Select od.IDVoucher, o.IDFile, p.NombreCorto as DescProveedor,                    
 p.Direccion + isnull(char(13) + uPro.Descripcion,'') as Direccion ,                     
 c.Titulo,                    
           
 --(Select MAX(NroPax) From OPERACIONES_DET Where IDOperacion=@IDOperacion And IDVoucher<>0) as Pax,                    
 --cast(od.NroPax as varchar(5))+ case when Isnull(od.NroLiberados,'0') = '0' then '' else '+'+ cast(od.NroLiberados as varchar(5))End as Pax,                    
 od.NroPax + Isnull(od.NroLiberados,0) as Pax,                    
 us.Nombre as Responsable,                    
 IsNull(r.CodReservaProv,'') as CodReservaProv,                     
 c.IDIdioma, od.IDIdioma as IdiomaServicio,                    
 u.Descripcion as Nacionalidad,                    
                     
 od.Dia,                      
 (Select MAX(FechaOut) From OPERACIONES_DET Where IDOperacion=@IDOperacion And IDVoucher<>0) as FechaOut,                      
 od.Cantidad, 
 --od.Servicio+ Case When od.FlServicioNoShow=1 Then ' (No Show)' else '' End as Servicio, 
 	case when isnull(p.IDProveedorInternacional,'')<>''
	 then p.NombreCorto+ ': ' + od.Servicio+ Case When od.FlServicioNoShow=1 Then ' (No Show)' else '' End
	 else  od.Servicio+ Case When od.FlServicioNoShow=1 Then ' (No Show)' else '' End
	 end
	 as Servicio,
	
 LTRIM(                    
 Case When od.Desayuno=1 Then 'DESAYUNO ' Else '' End+                    
 Case When od.Lonche=1 Then 'BOX LUNCH ' Else '' End+                     
 Case When od.Almuerzo = 1 Then 'ALMUERZO ' Else '' End+                     
 Case When od.Cena=1 Then 'CENA' Else '' End) as Buffet,                    
 --(Select Top 1 IsNull(ObservVoucher,'') From OPERACIONES_DET Where IDOperacion=@IDOperacion And IDVoucher=od.IDVoucher                    
 --Order by IDOperacion_Det Desc) as ObservVoucher  ,                  
 --(select ObservVoucher from VOUCHER_OPERACIONES vo where vo.IDVoucher = od.IDVoucher )  as ObservVoucher  ,  
 --Isnull(od.ObservBiblia,'') as ObservVoucher  ,  
 dbo.FnUnionObservacionVoucherInternoBibliaxOperacion(@IDOperacion,od.IDVoucher) As ObservVoucher,  
 Case when s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End as  TipoServ      
 ,(select ExtrNoIncluid from VOUCHER_OPERACIONES vo where vo.IDVoucher = od.IDVoucher )  as ExtraNoIncluido    ,              
 od.ObservInterno ,c.NroPax+Isnull(c.NroLiberados,0) as NroPaxCoti ,    
 (select COUNT(distinct od2.IDServicio_Det) from OPERACIONES_DET od2 where od2.IDOperacion = o.IDOperacion) as CantServicios,    
 sd.DetaTipo as DescripcionTarifa,
 
 --Case When sd.VerDetaTipo=1 Then
	--	ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'')
	--Else
	--	''
	--End 
	ISNULL(ISNULL(sd.CodTarifa, sd.DetaTipo),'') as CodTarifaDet,
	DescProveedor_Int=isnull((select pint.NombreCorto from MAPROVEEDORES pint where IDProveedor=p.IDProveedorInternacional),''),
	Direccion_Int=isnull((select pint.Direccion + isnull(char(13) + ub.Descripcion,'') from MAPROVEEDORES pint left join MAUBIGEO ub on ub.IDubigeo=pint.IDCiudad where IDProveedor=p.IDProveedorInternacional),'')
                  
 FROM OPERACIONES_DET od                     
 Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion                    
 Left Join MAPROVEEDORES p On o.IDProveedor=p.IDProveedor                    
 Left Join RESERVAS r On r.IDReserva=o.IDReserva                    
 Left Join COTICAB c On c.IDCAB=o.IDCab                    
 Left Join MAUBIGEO u On c.IDubigeo=u.IDubigeo                     
 Left Join MAUSUARIOS us On c.IDUsuario=us.IDUsuario                    
 Left Join MAUBIGEO uPro On p.IDCiudad = uPro.IDubigeo                  
 Left Join MASERVICIOS s On od.IDServicio = s.IDServicio                  
 Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det    
 Where od.IDOperacion=@IDOperacion And od.IDVoucher<>0                    
 ) as X                    
 order by X.Dia            

