﻿--HLF-20150527-Order by ..., DescServicioDet
--HLF-20150529-,rd.IDReserva_DetOrigAcomVehi
--HLF-20150703-'' As CoEstadoTarifa, '' as NoEstadoTarifa,
--JRF-20151123-Agregar NuViaticoTC
CREATE Procedure [dbo].[RESERVAS_DET_SelxIDCabIDProveedor]
	@IDCab int,
	@IDProveedor char(6)
As                                                              
	Set NoCount On                                                        
	                                
	SELECT * FROM                       
	(                      
	Select Rd.NuViaticoTC,Rd.IDReserva_Det,                                                  
	'' as IDOperacion_Det,                                                  
	rd.IDReserva,                                                  
	'' as IDOperacion,                                                  
	rd.IDFile,                                                  
	ISNull(Rd.Iddet,0) as IDDet,                                                  
	rd.Item,                                                  
	Rd.Dia,                                                    
	Case When Pr.IDTipoProv='001' Then                                                  
	upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                   
	Else                                                  
	upper(substring(DATENAME(dw,rd.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,rd.Dia,103)/*+' '+ substring(convert(varchar,rd.Dia,108),1,5)*/))                                                    
	End as DiaFormat,                                                      
	IsNull(Rd.FechaOut,'') as FechaOut,                                                  
	IsNull(upper(substring(DATENAME(dw,Rd.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,Rd.FechaOut,103)/*+' '+ substring(convert(varchar,Rd.FechaOut,108),1,5)*/)),'') as FechaOutFormat,                                                     
	Case When Pr.IDTipoProv='001' AND sd.PlanAlimenticio=0 Then                                                 
	''                                                
	Else                                             
	convert(char(5),rd.Dia,108)                                                
	End as Hora,                 
	Ub.Descripcion as Ubigeo,                                            

	CAST(rd.NroPax AS VARCHAR(3))+                        
	Case When isnull(rd.NroLiberados,0)=0 then '' else '+'+CAST(rd.NroLiberados AS VARCHAR(3)) end                        	             
	as PaxmasLiberados,                   
		
	
	Isnull(Rd.Noches,0) as Noches,                                                  
	Pr.IDTipoProv,                        
	                
	s.Dias,                                                  
	rv.IDProveedor,                                                  
	Pr.NombreCorto as Proveedor,                                           
	ISNull(pr.Email3,'') As CorreoProveedor,                                                  
	Ub.IDPais,                                                  
	Rd.IDubigeo,                                        
	Rd.Cantidad,                                            
	--Nuevo Campo                                          
	                  
	Rd.NroPax,     --nueva pos (pos ant Rd.CantidadAPagar)                              
	             
	                  
	Rd.IDServicio,                                                  
	Rd.IDServicio_Det,                                                  
	Rd.Servicio As DescServicioDet,                   
	                                
	--Rd.Servicio As DescServicioDetBup,                                                     
	Case When pr.IDTipoProv='001' or pr.IDTipoProv='002' Then  --hoteles o restaurantes                                          
	sd.Descripcion                                             
	Else                                            
	s.Descripcion                                            
	End As DescServicioDetBup,                                             
	          
	Rd.Especial,Rd.MotivoEspecial,rd.RutaDocSustento,Rd.Desayuno,Rd.Lonche,Rd.Almuerzo,Rd.Cena,                                                  
	Rd.Transfer,Rd.IDDetTransferOri,Rd.IDDetTransferDes,Rd.CamaMat,Rd.TipoTransporte,                                                  
	Rd.IDUbigeoOri,                                                  
	uo.Descripcion+'          |'+IsNull(uo.Codigo,'') as DescUbigeoOri,                                                    
	Rd.IDUbigeoDes,                                                  
	ud.Descripcion+'          |'+IsNull(ud.Codigo,'') as DescUbigeoDes,                                                    
	rd.IDIdioma,                    
	Case When s.IDTipoServ = 'NAP' Then '---' Else s.IDTipoServ End As IDTipoServ,                                                 
	sd.Anio,                    
	             
	--sd.Tipo,                    
	--Case When sd.ConAlojamiento=1 And s.Dias>1 Then                    
	--'' Else sd.Tipo End as Tipo,                         
	sd.Tipo,                    
	                        
	Isnull(sd.DetaTipo,'') As DetaTipo ,                            
	                                 
	Rd.CantidadAPagar,     --nueva pos (pos ant Rd.NroPax)                                           
	             
	                  
	Rd.NroLiberados,Rd.Tipo_Lib,Rd.CostoRealAnt,Rd.Margen,                                                  
	Rd.MargenAplicado,Rd.MargenAplicadoAnt,Rd.MargenLiberado,Rd.TotalOrig,Rd.CostoRealImpto,Rd.CostoLiberadoImpto                                                  
	,Rd.MargenImpto,Rd.MargenLiberadoImpto,Rd.TotImpto,                                                  
	             
	                                
	rd.IDDetRel ,Rd.Total,'' as IDVoucher,Rd.IDReserva_DetCopia ,Rd.IDReserva_Det_Rel                                                        
	,isnull(IDEmailEdit,'') as IDEmailEdit, isnull(IDEmailNew,'') as IDEmailNew,                                                  
	isnull(IDEmailRef,'') as IDEmailRef,                                                  
	--'01/01/1900' as FechaRecordatorio, '' as Recordatorio,                          
	'' as ObservVoucher, '' as ObservBiblia, '' as ObservInterno,                                            
	isnull(Rd.CapacidadHab,0) CapacidadHab ,Rd.EsMatrimonial, sd.PlanAlimenticio ,                                          
	cd.IncGuia,                                          
	0 as chkServicioExportacion                                          
	,0 as VerObserVoucher,0 as VerObserBiblia,                                                       
	Rd.CostoReal,Rd.CostoLiberado                                                      
	,Rd.NetoHab,Rd.IgvHab,Rd.TotalHab,Rd.NetoGen,Rd.IgvGen,                          
	isnull(rd.IDMoneda,'') as IDMoneda,                          
	isnull(mon.Simbolo,'') as SimboloMoneda,                          
	isnull(sd.TC,0) as TipoCambio,    
	Rd.TotalGen                                         
	,Isnull(Rd.IDGuiaProveedor,'') As IDGuiaProveedor,Isnull(Rd.NuVehiculo , 0) as NuVehiculo --Isnull(Rd.DescBus,'') As DescBus                            
	,sd.idTipoOC                      
	,0 as InactivoDet                                  
	,CAse When Pr.IDTipoProv='001' Then                       
	dbo.FnServicioHotelEsAlimenticioPuro (RV.IDReserva, Rd.Dia)                       
	Else                      
	0                      
	End AS ServHotelAlimentPuro,                
	sd.ConAlojamiento, cd.SSCR, '' As PNR, 
	'' As CoEstadoTarifa, '' as NoEstadoTarifa,
	isnull(rd.IDReserva_DetOrigAcomVehi,0) as IDReserva_DetOrigAcomVehi                        
	From RESERVAS Rv                                                   
	Left Join RESERVAS_DET Rd On Rd.IDReserva=Rv.IDReserva                                                     
	Left Join MAUBIGEO Ub On Rd.IDubigeo=Ub.IDubigeo                                                    
	Left Join MAUBIGEO uo On Rd.IDUbigeoOri=uo.IDubigeo                               
	Left Join MAUBIGEO ud On Rd.IDUbigeoDes=ud.IDubigeo                                                     
	Left Join MAPROVEEDORES Pr On  Pr.IDProveedor= Rv.IDProveedor                                                        
	Left Join MASERVICIOS_DET sd On  Rd.IDServicio_Det=sd.IDServicio_Det                                    
	Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio                                                  
	Left Join COTIDET cd On Rd.IDDet=cd.IDDET                                          
	Left Join MAMONEDAS mon on Rd.IDMoneda=mon.IDMoneda                          
	Where Rv.IDCab=@IDCab And Rv.IDProveedor=@IDProveedor
	And Rd.Anulado=0      
	) AS X                      
	--Where ServHotelAlimentPuro = 0                      
	--order by IDReserva, Dia,FechaOut, Item           
	Order by IDReserva, Dia,FechaOut, CapacidadHab , DescServicioDet
