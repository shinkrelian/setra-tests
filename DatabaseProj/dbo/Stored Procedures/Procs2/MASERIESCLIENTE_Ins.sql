﻿CREATE Procedure dbo.MASERIESCLIENTE_Ins  
@IDCliente char(6),  
@NoSerie varchar(Max),  
@PoConcretizacion numeric(8,2),  
@UserMod char(4)  
As  
Set NoCount On  
 Declare @NuSerie int  
 Set @NuSerie = (select (Isnull(Max(NuSerie),0)+1) from MASERIESCLIENTE)  
 INSERT INTO [dbo].[MASERIESCLIENTE]  
           ([NuSerie]  
           ,[IDCliente]  
           ,[NoSerie]  
           ,[PoConcretizacion]  
           ,[UserMod]  
           ,[FecMod])  
     VALUES  
           (@NuSerie  
           ,@IDCliente  
           ,@NoSerie  
           ,@PoConcretizacion  
           ,@UserMod  
           ,GetDate())  
