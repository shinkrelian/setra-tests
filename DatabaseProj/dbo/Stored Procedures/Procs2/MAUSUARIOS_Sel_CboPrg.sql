﻿

--select * from MAPROVEEDORES Where IDTipoOper = 'I' And IDTipoProv = 003
--select * from MAPROVEEDORES where Activo = 'I'
--select * from sys.sysobjects Where type = 'P' And name like 'MAUBIGEO%'
--Select * from MASERVICIOS Where UpdComision = 1
--select * from MASERVICIOS Where IDProveedor='000131'
--select * from MAUSUARIOS

CREATE Procedure [Dbo].[MAUSUARIOS_Sel_CboPrg] 
	@bTodos bit
As

	Set NoCount On
	
	Select * from 
	(
	   Select '' as IDUsuario,'' As Usuario,0 as Ord
	   Union
	   Select IDUsuario,Usuario,1 from MAUSUARIOS Where Activo='A'
	) As X
	Where (@bTodos=1 Or Ord<>0) 
	Order By Ord,Usuario
