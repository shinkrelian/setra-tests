﻿--JRF-HLF-20140602-Adaptarlo con los cambios del sp[RESERVAS_DET_UpdNoches]
CREATE Procedure dbo.RESERVAS_DET_TMP_UpdNoches         
 @IDDet int,        
 @FechaOut smalldatetime,        
 @UserMod char(4)        
As        
 Set NoCount On        
        
 Update RESERVAS_DET_TMP Set --Noches=@Noches,        
 FechaOut=@FechaOut, UserMod=@UserMod, FecMod=GETDATE()        
 Where IDDet=@IDDet   and Anulado = 0  
        
 Update RESERVAS_DET_TMP Set Noches=Case When DATEDIFF(D,Dia,FechaOut) < 0 Then 0 Else DATEDIFF(D,Dia,FechaOut) End,        
 UserMod=@UserMod, FecMod=GETDATE()        
 Where IDDet=@IDDet      and Anulado = 0  
         
 If Not Exists(Select cd.IDDET From COTIDET cd     
 Inner Join MASERVICIOS_DET sd On sd.IDServicio_Det=cd.IDServicio_Det     
 And sd.ConAlojamiento=1    
 Inner Join MASERVICIOS s On s.IDServicio=sd.IDServicio And s.Dias>1    
 Where IDDET=@IDDet)        
     
 Begin    
    
 Update RESERVAS_DET_TMP Set --NetoGen = NetoGen * Noches,      
 NetoGen = CantidadAPagar * TotalHab * Noches,   
 UserMod=@UserMod, FecMod=GETDATE()        
 Where IDDet=@IDDet        and Anulado = 0      
    
 Update RESERVAS_DET_TMP Set TotalGen = NetoGen + IgvGen,      
 UserMod=@UserMod, FecMod=GETDATE()        
 Where IDDet=@IDDet   and Anulado = 0      
 End    
