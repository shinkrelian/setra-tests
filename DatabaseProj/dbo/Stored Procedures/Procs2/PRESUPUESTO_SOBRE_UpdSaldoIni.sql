﻿--JHD-20160314-Set SsSaldo=isnull((Select SUM(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob AND idmoneda='SOL'),0),
--JHD-20160314-SsSaldo_USD=isnull((Select SUM(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob AND idmoneda='USD'),0),
CREATE Procedure dbo.PRESUPUESTO_SOBRE_UpdSaldoIni
	@NuPreSob int,
	@UserMod char(4)
As
	Set Nocount On
	
	Update PRESUPUESTO_SOBRE 
	Set SsSaldo=isnull((Select SUM(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob AND idmoneda='SOL'),0),
	SsSaldo_USD=isnull((Select SUM(SsTotal) From PRESUPUESTO_SOBRE_DET Where NuPreSob=@NuPreSob AND idmoneda='USD'),0),
	UserMod=@UserMod, FecMod=GETDATE()
	Where NuPreSob=@NuPreSob
