﻿--HLF-20120808-      
--Tipo=(Select Tipo From MASERVICIOS_DET Where IDServicio_Det=sd.IDServicio_Det_V) And      
-- Codigo=(Select Codigo From MASERVICIOS_DET Where IDServicio_Det=sd.IDServicio_Det_V)       
--HLF-20120808-Parametro @PorcentDcto      
--HLF-20120822-Case When IDServicio_Det_V2 Is Null Then Monto_sgl,Monto_dbl, Monto_tri,Monto_sgls,Monto_dbls, Monto_tris ...      
--HLF-20130418-Agregando And sd.Estado='A'     
--JRF-20130802-Agregando CodTarifa,VerDetaTipo  
--HLF-20140715-Agregando campo CoMoneda
--JRF-20150820-Agregnado campo FlDetraccion
CREATE Procedure [dbo].[MASERVICIOS_Det_Ins_CopiarNuevoAnio]        
 @IDServicio char(8),          
 @PorcentDcto numeric(5,2),        
 @AnioNue char(4),        
 @Anio char(4),        
 --@AnioPreciosCopia char(4),         
 @Tipo varchar(7),         
 @UserMod char(4)        
As        
 Set NoCount On        
 Declare @IDServicio_Det int = (Select IsNull(Max(IDServicio_Det),0) From MASERVICIOS_DET)        
      
 --Declare @Correlativo tinyint= (Select IsNull(Max(Correlativo),0) From MASERVICIOS_DET         
 -- Where IDServicio=@IDServicio and         
 -- (Anio=@AnioNue Or ltrim(rtrim(@AnioNue))='') And         
 -- Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')        
 Declare @Correlativo tinyint=0        
      
 INSERT INTO MASERVICIOS_DET        
 (IDServicio_Det        
 ,IDServicio        
 ,IDServicio_Det_V        
 ,Anio        
 ,Tipo        
 ,Correlativo        
 ,DetaTipo        
 ,Codigo        
 ,Descripcion        
 ,Afecto                   
 ,Monto_sgl        
 ,Monto_dbl        
 ,Monto_tri        
 ,cod_x_triple        
 ,IdHabitTriple        
 ,Monto_sgls        
 ,Monto_dbls        
 ,Monto_tris        
 ,TC        
 ,PlanAlimenticio        
 ,ConAlojamiento        
 ,DiferSS        
 ,DiferST        
               
 ,TipoGasto         
 ,TipoDesayuno        
      
 ,Desayuno        
 ,Lonche        
 ,Almuerzo        
 ,Cena                
      
 ,PoliticaLiberado        
 ,Liberado        
 ,TipoLib        
 ,MaximoLiberado        
 ,LiberadoM        
 ,MontoL        
 ,Tarifario        
 ,TituloGrupo        
 ,idTipoOC        
 ,CtaContable        
 ,CtaContableC                 
 ,CodTarifa  
 ,VerDetaTipo  
 ,IDServicio_DetCopia          
 --,AnioPreciosCopia        
 ,CoMoneda
 ,CoCeCos
 ,FlDetraccion
 ,UserMod)        
      
      
 SELECT IDServicio_Det, IDServicio,       
 IsNull(IDServicio_Det_V2,IDServicio_Det_V1),       
 Anio, Tipo, Correlativo, DetaTipo,      
 Codigo, Descripcion, Afecto,       
 Case When IDServicio_Det_V2 Is Null Then Monto_sgl Else       
  (Select Case When @PorcentDcto=0 Then Monto_sgl Else Monto_sgl*(1-(@PorcentDcto/100)) End       
   From MASERVICIOS_DET Where IDServicio_Det=IDServicio_Det_V2) End as Monto_sgl,       
 Case When IDServicio_Det_V2 Is Null Then Monto_dbl Else       
  (Select Case When @PorcentDcto=0 Then Monto_dbl Else Monto_dbl*(1-(@PorcentDcto/100)) End        
   From MASERVICIOS_DET Where IDServicio_Det=IDServicio_Det_V2) End as Monto_dbl,       
 Case When IDServicio_Det_V2 Is Null Then Monto_tri Else       
  (Select Case When @PorcentDcto=0 Then Monto_tri Else Monto_tri*(1-(@PorcentDcto/100)) End        
   From MASERVICIOS_DET Where IDServicio_Det=IDServicio_Det_V2) End as Monto_tri,         
 cod_x_triple, IdHabitTriple,      
 Case When IDServicio_Det_V2 Is Null Then Monto_sgls Else       
  (Select Case When @PorcentDcto=0 Then Monto_sgls Else Monto_sgls*(1-(@PorcentDcto/100)) End       
   From MASERVICIOS_DET Where IDServicio_Det=IDServicio_Det_V2) End as Monto_sgls,       
 Case When IDServicio_Det_V2 Is Null Then Monto_dbls Else       
  (Select Case When @PorcentDcto=0 Then Monto_dbls Else Monto_dbls*(1-(@PorcentDcto/100)) End       
   From MASERVICIOS_DET Where IDServicio_Det=IDServicio_Det_V2) End as Monto_dbls,       
 Case When IDServicio_Det_V2 Is Null Then Monto_tris Else       
  (Select Case When @PorcentDcto=0 Then Monto_tris Else Monto_tris*(1-(@PorcentDcto/100)) End       
   From MASERVICIOS_DET Where IDServicio_Det=IDServicio_Det_V2) End as Monto_tris,      
 TC, PlanAlimenticio, ConAlojamiento, DiferSS, DiferST,      
 TipoGasto, TipoDesayuno, Desayuno, Lonche, Almuerzo, Cena, PoliticaLiberado, Liberado,        
 TipoLib  ,MaximoLiberado  ,LiberadoM  ,MontoL  ,Tarifario  ,TituloGrupo  ,idTipoOC  ,CtaContable,      
 CtaContableC,CodTarifa,VerDetaTipo,IDServicio_DetCopia,CoMoneda,CoCeCos,FlDetraccion,UserMod      
        
 FROM       
 (      
  Select        
  @IDServicio_Det + ((row_number() over (order by IDServicio_Det) ))  as IDServicio_Det      
  ,@IDServicio as IDServicio                        
  ,      
  (        
  Select Top 1 IDServicio_Det From MASERVICIOS_DET Where         
  IDServicio=(Select IDServicio From MASERVICIOS_DET Where IDServicio_Det=sd.IDServicio_Det_V) And         
  Anio=@AnioNue And       
  Tipo=(Select Tipo From MASERVICIOS_DET Where IDServicio_Det=sd.IDServicio_Det_V) And      
  Codigo=(Select Codigo From MASERVICIOS_DET Where IDServicio_Det=sd.IDServicio_Det_V)       
  Order by FecMod Desc) as IDServicio_Det_V2       
  ,sd.IDServicio_Det_V as IDServicio_Det_V1      
                               
  ,@AnioNue  as Anio      
  ,Tipo        
  ,@Correlativo + ((row_number() over (order by Correlativo) ))  as Correlativo      
  ,DetaTipo        
  ,Codigo        
  ,Descripcion        
  ,Afecto                     
  ,Case When @PorcentDcto=0 Then Monto_sgl Else Monto_sgl*(1-(@PorcentDcto/100)) End  as Monto_sgl        
  ,Case When @PorcentDcto=0 Then Monto_dbl Else Monto_dbl*(1-(@PorcentDcto/100)) End  as Monto_dbl      
  ,Case When @PorcentDcto=0 Then Monto_tri Else Monto_tri*(1-(@PorcentDcto/100)) End  as Monto_tri      
  ,cod_x_triple        
  ,IdHabitTriple        
  ,Case When @PorcentDcto=0 Then Monto_sgls Else Monto_sgls*(1-(@PorcentDcto/100)) End  as Monto_sgls       
  ,Case When @PorcentDcto=0 Then Monto_dbls Else Monto_dbls*(1-(@PorcentDcto/100)) End  as Monto_dbls       
  ,Case When @PorcentDcto=0 Then Monto_tris Else Monto_tris*(1-(@PorcentDcto/100)) End  as Monto_tris      
  ,TC        
  ,PlanAlimenticio        
  ,ConAlojamiento        
  ,DiferSS        
  ,DiferST                   
  ,TipoGasto         
  ,TipoDesayuno        
  ,Desayuno        
  ,Lonche        
  ,Almuerzo        
  ,Cena                
  ,PoliticaLiberado        
  ,Liberado        
  ,TipoLib        
  ,MaximoLiberado        
  ,LiberadoM        
  ,MontoL        
  ,Tarifario        
  ,TituloGrupo        
  ,idTipoOC        
  ,CtaContable        
  ,CtaContableC       
  ,CodTarifa   
  ,VerDetaTipo  
  ,IDServicio_Det  as IDServicio_DetCopia      
  ,CoMoneda
  ,CoCeCos
  ,FlDetraccion
  ,@UserMod  as UserMod      
  From MASERVICIOS_DET sd Where IDServicio=@IDServicio And         
  (Anio=@Anio Or ltrim(rtrim(@Anio))='') And         
  (Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')        
  And sd.Estado='A'     
 ) AS X      
