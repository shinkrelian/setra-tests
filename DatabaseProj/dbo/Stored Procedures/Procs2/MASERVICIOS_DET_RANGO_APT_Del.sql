﻿

--PPMG-20151105-Se agrego el eliminar en la tabla MASERVICIOS_DET_DESC_APT y nuevo parametro IDTemporada
CREATE Procedure [dbo].[MASERVICIOS_DET_RANGO_APT_Del]
	@Correlativo tinyint,
    @IDServicio_Det int,
	@IDTemporada int
AS
BEGIN
	Set NoCount On
	
	Delete From MASERVICIOS_DET_RANGO_APT 
	WHERE
           Correlativo = @Correlativo And IDTemporada = @IDTemporada AND
           (IDServicio_Det=@IDServicio_Det Or 
           IDServicio_Det In 
           (Select IDServicio_Det From MASERVICIOS_DET
            Where IDServicio_Det_V=@IDServicio_Det ))
	IF NOT EXISTS(SELECT IDServicio_Det, IDTemporada From MASERVICIOS_DET_RANGO_APT 
				  WHERE IDTemporada = @IDTemporada AND (IDServicio_Det=@IDServicio_Det Or 
				  IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET Where IDServicio_Det_V=@IDServicio_Det))
				  GROUP BY IDServicio_Det, IDTemporada)
	BEGIN
		DELETE FROM MASERVICIOS_DET_DESC_APT
		WHERE IDTemporada = @IDTemporada AND (IDServicio_Det=@IDServicio_Det Or 
				  IDServicio_Det In (Select IDServicio_Det From MASERVICIOS_DET Where IDServicio_Det_V=@IDServicio_Det))
	END
END
