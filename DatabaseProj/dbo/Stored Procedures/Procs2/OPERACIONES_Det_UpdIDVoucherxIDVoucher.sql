﻿ 
 Create Procedure dbo.OPERACIONES_Det_UpdIDVoucherxIDVoucher
	 @IDVoucherNue	int,
	 @IDVoucherAnt	int,
	 @UserMod	char(4)
 
 As
	Set Nocount On
	
	Update OPERACIONES_DET Set IDVoucher=@IDVoucherNue, UserMod=@UserMod, FecMod=GETDATE()
	Where IDVoucher=@IDVoucherAnt
 
