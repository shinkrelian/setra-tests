﻿CREATE Procedure [dbo].[MAPROVEEDORES_SelCadenas_Cbo]
As
	Set NoCount On
	
	Select *  From
	(
	Select '' as IDProveedor, 'NINGUNA' AS RazonSocial, 0 as Ord
	Union
	Select IDProveedor, NombreCorto, 1 as Ord
	From MAPROVEEDORES Where EsCadena=1
	) as X
	Order by Ord, RazonSocial
