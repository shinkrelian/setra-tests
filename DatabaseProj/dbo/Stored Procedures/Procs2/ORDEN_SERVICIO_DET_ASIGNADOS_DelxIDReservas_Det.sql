﻿CREATE Procedure dbo.ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDReservas_Det
@IDReserva_Det int
as
delete from ORDEN_SERVICIO_DET_ASIGNADOS
where NuOrden_Servicio_Det In(
select osd.NuOrden_Servicio_Det
from ORDEN_SERVICIO_DET osd Inner Join OPERACIONES_DET_DETSERVICIOS odd On
osd.IDOperacion_Det = osd.IDOperacion_Det and odd.IDServicio_Det = osd.IDServicio_Det
Left Join OPERACIONES_DET od On odd.IDOperacion_Det = od.IDOperacion_Det 
Left Join ORDEN_SERVICIO ord On osd.NuOrden_Servicio = ord.NuOrden_Servicio
where od.IDReserva_Det = @IDReserva_Det and ord.IDCab = (select distinct r.IDCab from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva
 where IDReserva_Det = @IDReserva_Det))
