﻿CREATE Procedure dbo.PRESUPUESTOS_DET_SelxIDPresupuesto      
@IDPresupuesto int      
As      
 Set NoCount On      
 SELECT [IDPresupuesto_Det]      
      ,[IDPresupuesto]      
      ,[IDServicio_Det]      
      ,[Servicio]      
      ,ISNULL([MontoSoles],0) [MontoSoles]      
      ,ISNULL([MontoDolares],0) [MontoDolares]      
      ,ISNULL([LiquidaSoles],0) [LiquidaSoles]      
      ,ISNULL([LiquidaDolares],0) [LiquidaDolares]  
      ,ISNULL([SaldoSoles],0) [SaldoSoles]      
      ,ISNULL([SaldoDolares],0) [SaldoDolares]      
      ,Isnull([IDTipoDoc],'') [IDTipoDoc]
      ,isnull([NroSerieDocumento],'') [NroSerieDocumento]      
      ,isnull([NroDocumento],'') [NroDocumento]      
      ,[FechaDetalle]      
      ,Case When MontoDolares <> 0 Then 'USD' Else 'SOL' End as strTipoMoneda    
  FROM [dbo].[PRESUPUESTOS_DET]      
  where IDPresupuesto = @IDPresupuesto      
