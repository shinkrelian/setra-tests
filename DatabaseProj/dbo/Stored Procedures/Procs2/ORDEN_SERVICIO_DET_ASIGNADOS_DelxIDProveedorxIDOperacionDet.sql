﻿CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDProveedorxIDOperacionDet
@IDProveedor char(6),
@IDOperacion_Det int
As
Set NoCount On 
delete from ORDEN_SERVICIO_DET_ASIGNADOS
where IDProveedor = @IDProveedor and 
	  NuOrden_Servicio_Det In (select osd.NuOrden_Servicio_Det from ORDEN_SERVICIO_DET osd 
			where osd.IDOperacion_Det=@IDOperacion_Det)
