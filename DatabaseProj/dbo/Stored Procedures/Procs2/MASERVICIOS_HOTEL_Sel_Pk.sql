﻿
-----------------------------------------------------------------------------------------------------

--JHD-20141127-Agregar el Nuevo campo de TxPisos  
CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_Sel_Pk]
	@CoServicio char(8)=null,
	@CoConcepto tinyint=null
AS
BEGIN
	Select
		[CoServicio],
 		[CoConcepto],
 		[FlServicio],
 		[FlCosto],
 		[CoTipoMenu],
 		[FlFijoPortable],
 		[FlPortable],
 		[CoTipoBanio],
 		[CoEstadoHab],
 		TxPisos,
 		[FlActivo]
 	From [dbo].[MASERVICIOS_HOTEL]
	Where 
		[CoServicio] = @CoServicio And 
		[CoConcepto] = @CoConcepto
END;
