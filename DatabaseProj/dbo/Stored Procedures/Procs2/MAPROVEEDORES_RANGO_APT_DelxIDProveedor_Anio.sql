﻿CREATE Procedure [dbo].[MAPROVEEDORES_RANGO_APT_DelxIDProveedor_Anio]
	@IDProveedor int,
	@NuAnio CHAR(4)
As
	Set NoCount On
	
	Delete From MAPROVEEDORES_RANGO_APT 
	WHERE
           (IDProveedor=@IDProveedor
		   and NuAnio=@NuAnio
          )
