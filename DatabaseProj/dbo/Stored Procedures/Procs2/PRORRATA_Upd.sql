﻿
Create Procedure dbo.PRORRATA_Upd
@NuCodigoPro smallint,
@FeDesde smalldatetime,
@FeHasta smalldatetime,
@QtProrrata numeric(8,2),
@UserMod char(4)
As 
	Set NoCount On
	Update PRORRATA 
		Set FeDesde    = @FeDesde,
			FeHasta	   = @FeHasta,
			QtProrrata = @QtProrrata,
			UserMod    = @UserMod,
			FecMod     = GetDate()
	Where NuCodigoPro  = @NuCodigoPro
