﻿

--HLF-20160413-Select sum(case when rd.IDMoneda='USD' then rd.TotalGen else dbo.FnCambioMoneda( ...  as Costo
--order by y.DescProveedor
--dbo.FnCambioMoneda(d.SSTotal,d.CoMoneda,'USD', dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeEmision)) ... and d.FlActivo=1  as CostoSustentado
--HLF-20160414-... From COTIVUELOS cv   ... as DescPaisProv  ... as NuObligPago
--HLF-20160415-cp.CoObligPago, Update REP_COSTOS_PRELIQUIDACION Set CoObligPago=...
--HLF-20160418-Select ... as NuDocumProv
--case when Costo=0 then CostoTotalUSD else ...	case when Venta=0 then VentaUSD else ...
--HLF-20160419- Left Join MAUSUARIOS us on cp.IDProveedor=us.IDUsuario   isnull(p.NombreCorto,us.Nombre+' '+us.TxApellidos) as DescProveedor, isnull(...
--case when CostoSustentado1=0  ... @TCCoti As TCCoti,@TCFact as TCFact,@TCSust as TCSust,@TCSust2 as TCSust2
CREATE Procedure dbo.ReporteLiquidacionFile
	@IDCab int
As
	Set Nocount on

	Declare @NuFile char(8), @NuCotizacion char(8), @NoTipoVta varchar(40), @FechaIn smalldatetime, @FechaOut smalldatetime,
	@NoTitulo varchar(100), @NoCliente varchar(80), @TxDireccion varchar(200), @QtPax smallint, 
	@QtLiberados smallint, @NoPais varchar(60), 
	@NoEjecVtas varchar(20), @NoEjecRes varchar(20), @NoEjecOpe varchar(20), @TCCoti decimal(6,3), @TCFact decimal(6,3), 
	@TCSust decimal(6,3),@TCSust2 decimal(6,3)
	--Declare @TCambio decimal(6,3)=(dbo.FnTipoDeCambioVtaUltimoxFecha(getdate()))
	Declare @IGV as numeric(5,2)=(Select NuIGV From PARAMETRO)/100   
	
	Select @NuFile=isnull(c.IDFile,''), @NuCotizacion=c.Cotizacion,
	@NoTipoVta=Case c.CoTipoVenta 
		When 'RC' Then 'RECEPTIVO'
		When 'CU' Then 'COUNTER'
		When 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'
		When 'VC' Then 'VIAJE CLIENTES (FAM.OFRECIDO)'
		When 'OP' Then 'OPERACIONES'
		When 'TE' Then 'TESORERÍA'
		When 'DR' Then 'ZICASSO'
	End, 
	@FechaIn=c.FecInicio, @FechaOut=c.FechaOut, @NoEjecVtas=uc.Usuario, 
	@NoEjecRes=ur.Usuario, @NoEjecOpe=uo.Usuario, @NoTitulo=c.Titulo, @NoCliente=cl.RazonComercial,
	@TxDireccion=cl.Direccion, @QtPax=c.NroPax, @QtLiberados=isnull(c.NroLiberados,0),
	@NoPais=ub.Descripcion,
	@TCCoti=isnull((Select tc.SsTipCam From COTICAB_TIPOSCAMBIO tc Where tc.IDCAB=@IDCab and 
						tc.CoMoneda='SOL'),0),
	@TCFact=dbo.FnTipoDeCambioVtaUltimoxFecha(c.FeFacturac),
	@TCSust=isnull((Select top 1 dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeEmision) from DOCUMENTO_PROVEEDOR d Where d.IDCab=@IDCab 
						--and (cast(d.NuVoucher as varchar(15))=pl.NuObligPago or cast(d.NuOrden_Servicio as varchar(14))=replace(pl.NuObligPago,'-','')) 
						and d.FlActivo=1 order by FeEmision asc),0),
	@TCSust2=isnull((Select top 1 dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeEmision) from DOCUMENTO_PROVEEDOR d Where d.IDCab=@IDCab 
						--and (cast(d.NuVoucher as varchar(15))=pl.NuObligPago or cast(d.NuOrden_Servicio as varchar(14))=replace(pl.NuObligPago,'-','')) 
						and d.FlActivo=1 order by FeEmision desc),0)
	From COTICAB c Left Join MAUSUARIOS uc On c.IDUsuario=uc.IDUsuario
	Left Join MAUSUARIOS ur On c.IDUsuarioRes=ur.IDUsuario
	Left Join MAUSUARIOS uo On c.IDUsuario=uo.IDUsuario
	Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente
	Left Join MAUBIGEO ub On cl.IDCiudad=ub.IDubigeo
	Where c.IDCAB=@IDCab

	--Create Table #COSTOS_PRELIQUIDACION(
	--	IDProveedor char(6) collate Modern_Spanish_CI_AS, DescProveedor varchar(100), DescProveedorInternac varchar(100), DescPaisProveedor varchar(60),
	--	NuObligPago varchar(15), CompraNeto numeric(13,4), IGVCosto numeric(10,4), IGV_SFE numeric(10,4), Moneda char(3),
	--	Total numeric(13,4), IDTipoOC char(3), DescOperacionContab varchar(100), Margen numeric(5,2), 
	--	CostoBaseUSD numeric(13,4), VentaUSD numeric(13,4), FacturaExportacion numeric(13,4),
	--	BoletaNoExportacion numeric(13,4),BoletaServicioExterior numeric(13,4), CoObligPago char(2), RutayCodReserva varchar(100), Servicio varchar(100)
	--)
	DELETE FROM REP_COSTOS_PRELIQUIDACION
	
	Insert Into REP_COSTOS_PRELIQUIDACION(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		NuObligPago, CompraNeto, IGVCosto, IGV_SFE, Moneda,Total, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelVouchersPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION Set CoObligPago='AV' Where CoObligPago Is Null
	
	Insert Into REP_COSTOS_PRELIQUIDACION(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		NuObligPago, CompraNeto, IGVCosto, IGV_SFE, Moneda,Total, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelOrdenesServicioPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION Set CoObligPago='BO' Where CoObligPago Is Null
	
	Insert Into REP_COSTOS_PRELIQUIDACION(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		CompraNeto, IGVCosto, IGV_SFE, Moneda,Total, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelEntradasPresupuestosPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION Set CoObligPago='CE' Where CoObligPago Is Null

	Insert Into REP_COSTOS_PRELIQUIDACION(IDProveedor, DescProveedor, DescProveedorInternac, 
		RutayCodReserva, DescPaisProveedor, CompraNeto, IGVCosto, IGV_SFE, Total, Moneda, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, Servicio, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec COTIVUELOS_SelBusesVuelosPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION Set CoObligPago='DB' Where CoObligPago Is Null

	Insert Into REP_COSTOS_PRELIQUIDACION(IDProveedor, DescProveedor, DescProveedorInternac, DescPaisProveedor,
		NuObligPago, CompraNeto, IGVCosto, IGV_SFE, Moneda,Total, IDTipoOC, DescOperacionContab, Margen, 
		CostoBaseUSD, VentaUSD, FacturaExportacion,BoletaNoExportacion,BoletaServicioExterior )
	Exec OPERACIONES_SelTraladistasyMovilidadPreLiquidacion @IDCab,''
	Update REP_COSTOS_PRELIQUIDACION Set CoObligPago='EI' Where CoObligPago Is Null

	Select Y.*,
	 
	MargenSustentado-Margen as MargenDif, PorcMargenSustentado-PorcMargen as PorcMargenDif
	From
		(		
		Select 
		@NuFile as NuFile, @NuCotizacion as NuCotizacion, @NoTipoVta as NoTipoVta,	
		@FechaIn as FechaIn, @FechaOut as FechaOut, @NoEjecVtas as NoEjecVtas, 
		@NoEjecRes as NoEjecResUsuario, @NoEjecOpe as NoEjecResOpe, @NoTitulo as NoTitulo, @NoCliente as NoCliente,
		@TxDireccion as TxDireccion, @QtPax as QtPax, @QtLiberados as QtLiberados,
		@NoPais as NoPais,@TCCoti As TCCoti,@TCFact as TCFact,@TCSust as TCSust,@TCSust2 as TCSust2,
		IDProveedor,DescProveedor,DescPaisProv,NuObligPago,
		Venta,Redondeo,CoObligPago,
		Costo, Venta-Costo as Margen, 
		case when Venta>0 then ((Venta-Costo)/Venta)*100 else 0 end as PorcMargen, 
		CostoLiquidac, Venta-CostoLiquidac as MargenPreLiq, 
		case when Venta>0 then ((Venta-CostoLiquidac)/Venta)*100 else 0 end as PorcMargenPreLiq, 
		NuDocumProv, 
		CostoSustentado, CostoSustentado1, Venta-CostoSustentado as MargenSustentado, 
		case when Venta>0 then ((Venta-CostoSustentado)/Venta)*100 else 0 end as PorcMargenSustentado
		--,(Venta-CostoSustentado)-(Venta-Costo) as MargenDif, (((Venta-CostoSustentado)-(Venta-Costo))/(Venta-Costo))*100 as PorcMargenDif	
		From
			(Select X1.*,case when CostoSustentado1=0 then CostoLiquidac else CostoSustentado1 end as CostoSustentado
				From

				(
				Select 
				case when Costo1=0 then CostoTotalUSD else Costo1 end as Costo,
				case when Venta1=0 then VentaUSD else Venta1 end as Venta,
				X.*
				From
					(
					Select cp.IDProveedor,isnull(p.NombreCorto,us.Nombre+' '+us.TxApellidos) as DescProveedor, isnull(up.Descripcion,upc.Descripcion) as DescPaisProv,
					isnull((Select top 1 NuObligPago From REP_COSTOS_PRELIQUIDACION Where IDProveedor=cp.idproveedor),'') as NuObligPago,
					ROUND(
					isnull(sum(cd.Total*(cd.NroPax+(isnull(cd.NroLiberados,0)))) -sum(cd.RedondeoTotal*(cd.NroPax+(isnull(cd.NroLiberados,0)))),0) 
					+
					isnull((Select isnull(sum(isnull(CostoReal,0)),0) from
						(
						SELECT Case When isnull(cv.CostoOperador,0)=0 Then      
						(isnull(cv.Tarifa,0)*(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID))/(1+(@IGV)) 
							 Else      
						  isnull(cv.CostoOperador,0)*(Select count(*) From COTITRANSP_PAX Where IDTransporte=cv.ID)/(1+(@IGV))      
							 End as CostoReal  
						FROM 
						COTIVUELOS cv WHERE ISNULL(IDCAB,0)=@IDCab AND isnull(IdDet,0)=0 and IdLineaA=cp.idproveedor AND CV.EmitidoSetours=1
						) as a
					),0) 
					--+
					--isnull((Select isnull(sum(cd1.Total*(cd1.NroPax+(isnull(cd1.NroLiberados,0))))-sum(cd1.RedondeoTotal*(cd1.NroPax+(isnull(cd1.NroLiberados,0)))),0)
					--From COTIDET cd1 Inner Join RESERVAS_DET rd1 on cd1.IDDET=rd1.IDDet Inner Join OPERACIONES_DET od1 on od1.IDReserva_Det=rd1.IDReserva_Det
					--	And cd1.IDProveedor=cp.IDProveedor
					--),0)
					,0)
					as Venta1,
	
				
					--isnull((Select sum(rd.TotalGen) From RESERVAS_DET rd 
					--	Inner Join RESERVAS r On r.IDReserva=rd.IDReserva Where r.IDProveedor=cp.IDProveedor And r.IDCab=@IDCab and r.Anulado=0 and rd.Anulado=0),0) as Costo,

					isnull((Select sum(case when rd.IDMoneda='USD' then rd.TotalGen else dbo.FnCambioMoneda(rd.TotalGen,rd.IDMoneda,'USD',tc.SsTipCam) end) From RESERVAS_DET rd 
						Inner Join RESERVAS r On r.IDReserva=rd.IDReserva 
						Left Join COTICAB_TIPOSCAMBIO tc On tc.IDCab=r.IDCab And tc.CoMoneda=rd.IDMoneda
						Where r.IDProveedor=cp.IDProveedor And r.IDCab=@IDCab and r.Anulado=0 and rd.Anulado=0),0) as Costo1,
		


					--isnull((Select CostoBaseUSD From REP_COSTOS_PRELIQUIDACION Where IDProveedor=cp.idproveedor),0) as CostoLiquidac,
					--isnull((Select sum(CostoBaseUSD) From REP_COSTOS_PRELIQUIDACION Where IDProveedor=cp.idproveedor and Moneda='USD'),0) as CostoLiquidacUSD,
					--isnull((Select sum(CostoBaseUSD) From REP_COSTOS_PRELIQUIDACION Where IDProveedor=cp.idproveedor and Moneda='SOL'),0) as CostoLiquidacSOL,
					isnull((Select sum(CostoBaseUSD) From REP_COSTOS_PRELIQUIDACION Where IDProveedor=cp.idproveedor),0) as CostoLiquidac,

					--(Select sum(SsTotalCostoUSD) From DOCUMENTO_DET dd Inner Join DOCUMENTO d On d.NuDocum=dd.NuDocum And 
					--	d.IDTipoDoc=dd.IDTipoDoc And d.IDCab=@IDCab And dd.IDProveedor=cd.IDProveedor) as CostoSustentado
				
					--isnull((Select sum(SsTotal) From DOCUMENTO_PROVEEDOR d Where d.IDCab=@IDCab And d.CoProveedor=cp.IDProveedor),0) as CostoSustentado
					isnull((Select top 1 d.NuDocum From DOCUMENTO_PROVEEDOR d 
						Where d.IDCab=@IDCab And d.CoProveedor=cp.IDProveedor and d.FlActivo=1),'PENDIENTE') as NuDocumProv,
					isnull((Select sum(dbo.FnCambioMoneda(d.SSTotal,d.CoMoneda,'USD', dbo.FnTipoDeCambioVtaUltimoxFecha(d.FeEmision))) From DOCUMENTO_PROVEEDOR d 
						Where d.IDCab=@IDCab And d.CoProveedor=cp.IDProveedor and d.FlActivo=1),0) as CostoSustentado1
					,isnull(sum(cd.RedondeoTotal*(cd.NroPax+(isnull(cd.NroLiberados,0)))),0) as Redondeo, cp.CoObligPago,
					sum(cp.VentaUSD) as VentaUSD, sum(cp.Total) as CostoTotalUSD
					From --OPERACIONES_DET od left join RESERVAS_DET rd on od.IDReserva_Det=rd.IDReserva_Det
					--left join 
					--COTIDET cd --on cd.IDDET=rd.IDDet	Inner Join OPERACIONES o on o.IDOperacion=od.IDOperacion
					--Inner Join MAPROVEEDORES p On cd.IDProveedor=p.IDProveedor And p.IDTipoOper='E'
					REP_COSTOS_PRELIQUIDACION cp Left Join COTIDET cd On cd.IDProveedor=cp.IDProveedor And cd.IDCAB=@IDCab
					Left Join MAPROVEEDORES p On cp.IDProveedor=p.IDProveedor And p.IDTipoOper='E'
					Left Join MAUSUARIOS us on cp.IDProveedor=us.IDUsuario
					Left Join MAUBIGEO uc On uc.IDubigeo=p.IDCiudad
					Left Join MAUBIGEO up On uc.IDPais=up.IDubigeo
					Left Join MAUBIGEO ucc On ucc.IDubigeo=us.IDUbigeo
					Left Join MAUBIGEO upc On ucc.IDPais=upc.IDubigeo
					--Where cd.IDCAB=@IDCab
					group by cp.CoObligPago,cp.IDProveedor,p.NombreCorto, up.Descripcion, us.Nombre, us.TxApellidos, upc.Descripcion
					) as X
				) as X1
			) as X2
		) as Y
	order by y.DescProveedor
