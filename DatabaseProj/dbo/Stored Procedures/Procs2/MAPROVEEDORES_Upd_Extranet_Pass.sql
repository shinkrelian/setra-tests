﻿
CREATE PROCEDURE [dbo].[MAPROVEEDORES_Upd_Extranet_Pass]  
						@PasswordLogeo varchar(max)='',  
						@UsuarioLogeo_Hash varchar(max),
						@UserMod char(4)
AS
BEGIN
	declare @IDProveedor char(6),
			@FechaHash datetime

	select @IDProveedor=IDProveedor,@FechaHash=FechaHash from MAPROVEEDORES where UsuarioLogeo_Hash=@UsuarioLogeo_Hash

	if rtrim(ltrim(@IDProveedor))<>''
	begin
			if getdate()<=@FechaHash
				begin
					UPDATE MAPROVEEDORES Set  
					Password=Case When ltrim(rtrim(@PasswordLogeo))='' Then Null Else @PasswordLogeo End  
					--,UsuarioLogeo_Hash=Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
					--,FechaHash=DATEADD(day,14, getdate())
					,UserMod=@UserMod             
					,FecMod=GETDATE()
					WHERE  
						IDProveedor = @IDProveedor  
				end
			else
				begin
					RAISERROR ('La fecha límite expiró', 16, 1);
					RETURN
				end
	end
	else
	begin
			RAISERROR ('Token no se encuentra registrado', 16, 1);
			RETURN
	end
END;
