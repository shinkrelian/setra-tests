﻿--HLF-20130418-Agregando And sd.Estado='A'     
--JRF-20130802-Agregando los CodTarifa ,VerDetaTipo  
--HLF-20140715-Agregando campo CoMoneda
CREATE Procedure [dbo].[MASERVICIOS_Det_Ins_CopiarNuevoVariante]        
 @IDServicio char(8),          
 @TipoNue varchar(7),        
 @DetaTipoNue text,        
 @PorcentDcto numeric(5,2),        
 @Anio char(4),        
 @Tipo varchar(7),         
 @UserMod char(4)        
As        
 Set NoCount On        
 Declare @IDServicio_Det int = (Select IsNull(Max(IDServicio_Det),0) From MASERVICIOS_DET)        
         
 --Declare @Correlativo tinyint= (Select IsNull(Max(Correlativo),0) From MASERVICIOS_DET         
 -- Where IDServicio=@IDServicio and         
 -- Tipo=@TipoNue  And         
 -- (Anio=@Anio Or ltrim(rtrim(@Anio))=''))        
         
 Declare @Correlativo tinyint=0        
         
 INSERT INTO MASERVICIOS_DET        
           (IDServicio_Det        
           ,IDServicio              
           ,IDServicio_Det_V             
           ,Anio        
           ,Tipo        
           ,Correlativo        
           ,DetaTipo        
           ,Codigo        
           ,Descripcion        
           ,Afecto                   
           ,Monto_sgl        
           ,Monto_dbl        
           ,Monto_tri        
           ,cod_x_triple        
           ,IdHabitTriple        
           ,Monto_sgls        
           ,Monto_dbls        
           ,Monto_tris        
           ,TC        
           ,PlanAlimenticio        
        
     ,ConAlojamiento        
           ,DiferSS        
           ,DiferST                   
                   
           ,TipoGasto         
           ,TipoDesayuno        
        
     ,Desayuno        
     ,Lonche        
     ,Almuerzo        
     ,Cena                
                   
                   
           ,PoliticaLiberado        
           ,Liberado        
           ,TipoLib        
           ,MaximoLiberado        
           ,LiberadoM        
           ,MontoL        
           ,Tarifario        
           ,TituloGrupo        
           ,idTipoOC        
           ,CtaContable        
           ,CtaContableC      
           ,CodTarifa   
     ,VerDetaTipo             
           ,IDServicio_DetCopia        
           ,CoMoneda  
		   ,CoCeCos
           ,UserMod)        
         
 Select        
   @IDServicio_Det + ((row_number() over (order by IDServicio_Det) ))        
           ,@IDServicio                   
           ,IDServicio_Det_V        
           ,Anio        
           ,@TipoNue        
           ,@Correlativo + ((row_number() over (order by Correlativo) ))        
           ,Case When LTRIM(RTRIM( Cast(@DetaTipoNue As varchar(Max))))='' Then Null Else @DetaTipoNue End        
           ,Codigo        
           ,Descripcion        
           ,Afecto                   
           ,Case When @PorcentDcto=0 Then Monto_sgl Else Monto_sgl*(1-(@PorcentDcto/100)) End        
           ,Case When @PorcentDcto=0 Then Monto_dbl Else Monto_dbl*(1-(@PorcentDcto/100)) End        
           ,Case When @PorcentDcto=0 Then Monto_tri Else Monto_tri*(1-(@PorcentDcto/100)) End        
           ,cod_x_triple        
           ,IdHabitTriple        
           ,Case When @PorcentDcto=0 Then Monto_sgls Else Monto_sgls*(1-(@PorcentDcto/100)) End        
           ,Case When @PorcentDcto=0 Then Monto_dbls Else Monto_dbls*(1-(@PorcentDcto/100)) End        
           ,Case When @PorcentDcto=0 Then Monto_tris Else Monto_tris*(1-(@PorcentDcto/100)) End        
           ,TC        
     ,PlanAlimenticio        
     ,ConAlojamiento        
           ,DiferSS        
           ,DiferST                   
                   
           ,TipoGasto         
           ,TipoDesayuno        
        
     ,Desayuno        
     ,Lonche        
     ,Almuerzo        
     ,Cena                
                  
                   
           ,PoliticaLiberado        
           ,Liberado        
           ,TipoLib        
           ,MaximoLiberado        
           ,LiberadoM        
           ,MontoL        
           ,Tarifario        
           ,TituloGrupo     
           ,idTipoOC        
           ,CtaContable        
           ,CtaContableC    
           ,CodTarifa   
           ,VerDetaTipo      
           ,IDServicio_Det        
           ,CoMoneda
		   ,CoCeCos
           ,@UserMod        
 From MASERVICIOS_DET Where IDServicio=@IDServicio And         
   (Anio=@Anio Or ltrim(rtrim(@Anio))='') And         
  (Tipo=@Tipo Or ltrim(rtrim(@Tipo))='*SF*')         
  And Estado='A'     
