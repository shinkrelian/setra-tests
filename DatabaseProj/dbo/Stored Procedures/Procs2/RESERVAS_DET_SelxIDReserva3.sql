﻿--JRF-20150413-...and Anulado=0 y (...rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva Where ...)
--JRF-20160321-..IsNull(cd.AcomodoEspecial,0) as AcomodoEspecial
CREATE Procedure dbo.RESERVAS_DET_SelxIDReserva3  
 @IDReserva int  
As  
  
 Set NoCount On  
 Select r.IDProveedor,IsNull(cd.AcomodoEspecial,0) as AcomodoEspecial,rd.* 
 From RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva=r.IDReserva 
 Left Join COTIDET cd On rd.IDDet=cd.IDDET
 Where rd.IDReserva=@IDReserva and rd.Anulado=0 and r.Anulado=0
 Order by IDReserva_Det desc  
