﻿Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelxIDOperacionDetxIDServicio
@IDOperacionDet int,
@IDServicio char(8)
As
Set NoCount On
Select 0 as IDDet_DetCotiServ,cds.IDServicio_Det,cds.IDServicio,cds.CostoLiberado,cds.CostoLiberadoImpto,cds.CostoReal,cds.CostoRealImpto,cds.Margen,
	   cds.MargenAplicado,cds.MargenImpto,cds.MargenLiberado,cds.MargenLiberadoImpto,SSCL,SSCLImpto,SSCR,SSCRImpto,SSMargen,STMargenImpto,SSMargenLiberado as SSML,
	   SSMargenLiberadoImpto,SSTotal,STCR,STCRImpto,STMargen,STMargenImpto,STTotal,cds.Total,0 as RedondeoTotal,cds.TotImpto,cds.IDMoneda,
	   IsNull(sd.IDServicio_Det_V,0) as IDServicio_Det_V,IsNull(sd.TC,0) as TCambio,
	   Case When IsNull(p.IDTipoProv,'')='004' and o.IDProveedor='001554' Then '000467' else '' End as IDProveeProgramado,
	   Case When IsNull(p.IDTipoProv,'')='004' and o.IDProveedor='001554' Then 'PA' else 'NV' End as IDEstadoSolicitud
from OPERACIONES_DET_DETSERVICIOS cds Left Join MASERVICIOS_DET sd On cds.IDServicio_Det=sd.IDServicio_Det 
Left Join MASERVICIOS_DET sdRel On IsNull(sd.IDServicio_Det_V,0)=sdRel.IDServicio_Det
Left Join MASERVICIOS s On sdRel.IDServicio=s.IDServicio
Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
Left Join OPERACIONES_DET od On cds.IDOperacion_Det=od.IDOperacion_Det
Left Join OPERACIONES o On od.IDOperacion=o.IDOperacion
where cds.IDOperacion_Det=@IDOperacionDet and cds.IDServicio=@IDServicio and cds.Activo=1
