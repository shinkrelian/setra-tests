﻿

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

CREATE procedure NOTAVENTA_COUNTER_DETALLE_Del
		   @NuNtaVta char(6),
		   @NuItem tinyint,
		   @NuDetalle tinyint
as
BEGIN
Set NoCount On
	
delete from [NOTAVENTA_COUNTER_DETALLE]

WHERE
			NuNtaVta=@NuNtaVta AND
			NuItem=@NuItem AND
            NuDetalle=@NuDetalle      
END;
