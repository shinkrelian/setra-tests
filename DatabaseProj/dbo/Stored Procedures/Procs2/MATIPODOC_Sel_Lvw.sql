﻿CREATE Procedure [dbo].[MATIPODOC_Sel_Lvw]
	@ID	char(3),
	@Descripcion varchar(60)
As
	Set NoCount On
	
	Select Descripcion	
	From MATIPODOC
	Where (Descripcion Like '%'+@Descripcion+'%')
	And (IDtipodoc <>@ID Or ltrim(rtrim(@ID))='')
	Order by Descripcion
