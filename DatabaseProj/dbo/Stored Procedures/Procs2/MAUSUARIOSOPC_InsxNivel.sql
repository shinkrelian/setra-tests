﻿Create PROCEDURE [dbo].[MAUSUARIOSOPC_InsxNivel]
	@IDNivel	char(3),	
	@UserMod	char(4)
As
	Set NoCount On
	
	Insert Into MANIVELOPC(IDOpc,IDNivel,Consultar, Grabar, Actualizar, Eliminar, UserMod)
	Select mo.IDOPc,@IDNivel,0,0,0,0,@UserMod From MAOPCIONES mo
