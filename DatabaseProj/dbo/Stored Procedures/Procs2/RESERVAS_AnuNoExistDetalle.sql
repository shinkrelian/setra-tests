﻿Create Procedure dbo.RESERVAS_AnuNoExistDetalle
@IDReserva int,
@UserMod char(4)
As
Set NoCount On
Update RESERVAS
	Set Anulado=1,
		UserMod=@UserMod,
		FecMod=GETDATE()
Where IDReserva=@IDReserva
And Not Exists(select IDReserva_Det from RESERVAS_DET where IDReserva=RESERVAS.IDReserva and RESERVAS_DET.Anulado=0)
