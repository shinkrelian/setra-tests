﻿
-----------------------------------------------------------------------------------
--MAUSUARIOS_Sel_CboxNivelAsignacion 'RES',1
--MAUSUARIOS_Sel_CboxNivelAsignacion_Activos 'RES',1,''
--create
--JHD-20141125 - Nuevo procedimiento para listar los activos    
CREATE Procedure [dbo].[MAUSUARIOS_Sel_CboxNivelAsignacion_Activos]       
 @Nivel Char(3),        
 @bFilaNula bit,
 @Activo CHAR(1)      
As        
        
 Set NoCount On        
 Declare @DescOpcion varchar(max) = ''  
 if @Nivel='RES'  
 set @DescOpcion = 'mnuIngReservasControlporFile'  
 if @Nivel='OPE'  
 set @DescOpcion = 'mnuIngOperacionesControlporFile'  
 if @Nivel='VTA'  
 set @DescOpcion = 'mnuIngCotizacion'  
         
 Select distinct IDUsuario, Usuario,Activo from         
 (        
 Select '0000' as IDUsuario,'----' As Usuario,'A' AS Activo,0 as Ord,0 as OrdenNivel       
 Union        
 Select u.IDUsuario,u.Usuario,u.Activo,1,n.Orden as OrdenNivel      
 From MAUSUARIOS u Left Join MANIVELUSUARIO n On u.Nivel=n.IDNivel      
 Where --u.Activo='A' And            
     (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and         
(ltrim(rtrim(@Nivel))='' Or Nivel=@Nivel)         
 union  
 select distinct u.IDUsuario,u.Usuario,u.Activo,2, 0 as OrdenNivel    
 from MAUSUARIOSOPC uo Left Join MAUSUARIOS u On uo.IDUsuario = u.IDUsuario    
 Left Join MAOPCIONES o On uo.IDOpc=o.IDOpc    
 Where uo.Consultar = 1  and uo.Grabar=1 and uo.Actualizar = 1 and --u.Activo ='A'     
     (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='')
 and LTRIM(rtrim(o.Nombre))=LTRIM(rtrim(@DescOpcion))    
 and IdArea <> 'SI' and Nivel <> @Nivel AND Nivel <> 'GER' and Nivel <> 'JAF'  
 union  
 Select u.IDUsuario,u.Usuario,u.Activo,3,0 as OrdenNivel      
    From MAUSUARIOS u     
    Where --u.Activo='A' And     
        (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and         
    u.Nivel=(Case @Nivel When 'VTA' Then 'SVE'     
       When 'RES' Then 'SRE'     
       When 'OPE' Then 'SOP' End)    
 UNION    
 Select u.IDUsuario,u.Usuario,u.Activo,4,0 as OrdenNivel      
    From MAUSUARIOS u     
    Where --u.Activo='A' And     
        (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and         
    u.Nivel='GER'    
    
Union 
 Select u.IDUsuario,u.Usuario,u.Activo,5,n.Orden as OrdenNivel      
 From MAUSUARIOS u Left Join MANIVELUSUARIO n On u.Nivel=n.IDNivel      
 Where --u.Activo='A' And            
     (u.Activo = @Activo or LTRIM(RTRIM(@Activo)) ='') and         
(ltrim(rtrim(@Nivel))='' Or Nivel In ('RES','OPE'))              

    
 ) As X        
 Where (@bFilaNula=1 Or Ord<>0)       
 And (@Nivel in ('RES','OPE') OR Ord<>5)
 And (Not @Nivel in ('RES','OPE') OR Ord<>1)
  and   (Activo = @Activo or @Activo ='') 
 Order By Usuario      
 

