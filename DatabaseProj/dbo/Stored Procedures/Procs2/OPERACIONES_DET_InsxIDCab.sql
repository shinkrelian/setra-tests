﻿

--HLF-20130111-CantidadAPagar,NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,            
--HLF-20130117-Agregando campo IDTipoOC            
--HLF-20130809-Agregando al subquery And IDReserva In (Select r1.IDReserva From RESERVAS r1           
--Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E'          
-- Where IDCab=@IDCab          
--HLF-20130902-Agregando And Estado<>'XL'         
--HLF-20130904-Agregando Case When ... as VerBiblia      
--HLF-20130912-Agregando And Anulado = 0      
--HLF-20140122-Agregando  And r1.Anulado=0         
--JRF-20150326-Agregando columna FlServicioTC
--PPMG-20160218-Se quito el IDTipoProv = '002'
CREATE Procedure [dbo].[OPERACIONES_DET_InsxIDCab]            
 @IDCab int,             
 @UserMod char(4)            
AS
BEGIN
 Set Nocount On            
             
 Declare @IDOperacion_Det int            
 Execute dbo.Correlativo_SelOutput 'OPERACIONES_DET',1,10,@IDOperacion_Det output            
             
 Insert Into OPERACIONES_DET            
 (IDOperacion_Det, IDOperacion, IDReserva_Det, IDVoucher,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,            
 IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,            
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,            
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,            
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,            
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,IDTipoOC,        
 VerObserBiblia,FlServicioNoShow ,IDMoneda,  FlServicioTC,
 UserMod)             
 Select             
 ((row_number() over (order by IDReserva,IDReserva_Det))-1) + @IDOperacion_Det,             
 (Select IDOperacion From OPERACIONES Where IDCab=@IDCab And IDReserva=d.IDReserva          
 And IDReserva In (Select r1.IDReserva From RESERVAS r1           
 Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E' And r1.Anulado=0         
  Where IDCab=@IDCab)          
 ),            
 IDReserva_Det, 0,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,d.IDServicio,            
 d.IDServicio_Det,Servicio,IDIdioma,Especial,MotivoEspecial,RutaDocSustento,d.Desayuno,d.Lonche,d.Almuerzo,d.Cena,Transfer,            
 IDDetTransferOri,IDDetTransferDes,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,NroLiberados,Tipo_Lib,            
 Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,            
 CostoRealImpto,CostoLiberadoImpto,MargenImpto,MargenLiberadoImpto,TotImpto,            
 NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,          
 (Select IDTipoOC From MASERVICIOS_DET Where IDServicio_Det=d.IDServicio_Det),            
       
 Case When       
 (Select p1.IDTipoProv From MASERVICIOS_DET sd1 Inner Join MASERVICIOS s1 On s1.IDServicio=sd1.IDServicio      
  Inner Join MAPROVEEDORES p1 On s1.IDProveedor=p1.IDProveedor      
  --Where sd1.IDServicio_Det=d.IDServicio_Det) In ('001','002')  Then 0 Else 1 End as VerBiblia,   FlServicioNoShow,   
  Where sd1.IDServicio_Det=d.IDServicio_Det) In ('001')  Then 0 Else 1 End as VerBiblia,   FlServicioNoShow,   
      IDMoneda, d.FlServicioTC, 
 @UserMod            
 From RESERVAS_DET d       
 Where IDReserva In (Select IDReserva From RESERVAS Where IDCab=@IDCab And Estado<>'XL'        
 And IDReserva In (Select r1.IDReserva From RESERVAS r1           
 Inner Join MAPROVEEDORES p1 On r1.IDProveedor=p1.IDProveedor And p1.IDTipoOper='E'        
 And Anulado=0        
  Where IDCab=@IDCab))            
 And Anulado=0      
 Declare @iRowsAff smallint=@@ROWCOUNT - 1            
 If @iRowsAff>0 Exec Correlativo_Upd 'OPERACIONES_DET',@iRowsAff            

END
