﻿CREATE Procedure [dbo].[MATEXTOSERVICIOS_Upd]
	@IDServicio	char(8),
	@IDIdioma	varchar(12),
	@Titulo	varchar(100),
	@Texto	text,
	@UserMod	char(4)
As
	Set NoCount On
	
	Update MATEXTOSERVICIOS Set Texto=@Texto, 
	Titulo=Case When ltrim(rtrim(@Titulo))='' Then Null Else @Titulo End,
	UserMod=@UserMod, FechaMod=getdate()
	Where IDServicio=@IDServicio And IDIdioma=@IDIdioma
