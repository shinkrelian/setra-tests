﻿CREATE PROCEDURE dbo.OPERACIONES_DET_PAX_Sel_ExisteOutput
	@IDOperacion_Det int,
	@IDPax int,
	@pExiste	bit Output
As

	Set NoCount On	
	
	If Exists(Select IDPax From OPERACIONES_DET_PAX Where IDPax=@IDPax And IDOperacion_Det=@IDOperacion_Det)
		Set @pExiste=1
	Else
		Set @pExiste=0

