﻿
----------------------------------------------------------------------------------------



--HLF-20130520-Agregando columnnas CorreoProgTransp, NombreUsuarioProgTransp con su Left Join
CREATE Procedure [dbo].[MAPROVEEDORES_Sel_Pk]    
 @IDProveedor char(6)    
AS    
 Set NoCount On    
     
 Select p.*, u.IDPais, g.IDubigeo as Nacionalidad, 
 us.Usuario as UsuarioProg, us.Correo as CorreoProg,  
 us.Nombre as NombreUsuarioProg,
 tr.Correo as CorreoProgTransp,
 tr.Nombre as NombreUsuarioProgTransp
 From MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo    
 Left Join MAUBIGEO g On u.IDPais=g.IDubigeo    
 Left Join MAUSUARIOS us On isnull(p.IDUsuarioProg,'')=us.IDUsuario  
 Left Join MAUSUARIOS tr On isnull(p.IDUsuarioProgTransp,'')=tr.IDUsuario  
 Where IDProveedor = @IDProveedor    
  
