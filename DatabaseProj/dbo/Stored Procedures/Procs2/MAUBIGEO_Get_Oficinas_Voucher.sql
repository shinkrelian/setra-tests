﻿
-----------------------------------------------------------------------------------------------------------------------

--CREATE
--drop Procedure dbo.MAUBIGEO_Get_Oficinas_Voucher
CREATE Procedure dbo.MAUBIGEO_Get_Oficinas_Voucher
@idproveedor char(6)='',
@IDVoucher int=0
as  

SELECT IDubigeo,
Descripcion,
Activo=  case when (select CoUbigeo_Oficina from VOUCHER_OPERACIONES where IDVoucher=@IDVoucher) IS null
		 then (
				case when (select isnull(CoUbigeo_Oficina,'') from MAPROVEEDORES where IDProveedor=@idproveedor)=IDubigeo 
				then (CAST(1 AS bit))
				else (CAST(0 AS bit)) end
			  )	
		 else(CAST(0 AS bit)) end,
		 --ELSE (case when (select isnull(CoUbigeo_Oficina,'') from VOUCHER_OPERACIONES where IDVoucher=@IDVoucher)=IDubigeo then (CAST(1 AS bit)) else (CAST(0 AS bit)) end)
		 --END,
		 
Activo_Voucher= case when (select isnull(CoUbigeo_Oficina,'') from VOUCHER_OPERACIONES where IDVoucher=@IDVoucher)=IDubigeo then (CAST(1 AS bit)) else (CAST(0 AS bit)) end
FROM MAUBIGEO
WHERE IDubigeo IN (SELECT IDCiudad FROM MAPROVEEDORES WHERE IDTipoOper='I' AND IDTipoProv='003')

