﻿

--JHD-20141230-Se quito el filtro para mostrar todos los registros de la tabla operaciones de dicho file
CREATE Procedure [dbo].[OPERACIONES_Sel_x_IDCab_Oper_Internos]      
 @IDCab int
As  
Select *
From OPERACIONES   
Where IDCab=@IDCab
--and IDProveedor in (select IDProveedor from MAPROVEEDORES where IDTipoOper='I')
