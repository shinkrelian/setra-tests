﻿--HLF-20150630- and FlServicioNoShow=@FlServicioNoShow
--Comentando if exists
CREATE PROCEDURE DBO.ORDEN_SERVICIO_DET_SelNuOrden_Servicio_DetOutput
	@NuOrden_Servicio int,
	@IDOperacion_Det int,
	@IDServicio_Det int,
	@FlServicioNoShow bit,
	@pNuOrden_Servicio_Det int output
As
	Set NoCount On
	Set @pNuOrden_Servicio_Det = 0
 --If Exists(select NuOrden_Servicio_Det from ORDEN_SERVICIO_DET 
	--	   where NuOrden_Servicio = @NuOrden_Servicio and IDOperacion_Det = @IDOperacion_Det 
	--	   and IDServicio_Det = @IDServicio_Det and FlServicioNoShow=@FlServicioNoShow)
	--set @pNuOrden_Servicio_Det = (select NuOrden_Servicio_Det from ORDEN_SERVICIO_DET 
	--	   where NuOrden_Servicio = @NuOrden_Servicio and IDOperacion_Det = @IDOperacion_Det 
	--	   and IDServicio_Det = @IDServicio_Det and FlServicioNoShow=@FlServicioNoShow)
	Select @pNuOrden_Servicio_Det=NuOrden_Servicio_Det from ORDEN_SERVICIO_DET 
	where NuOrden_Servicio = @NuOrden_Servicio and IDOperacion_Det = @IDOperacion_Det 
	and IDServicio_Det = @IDServicio_Det and FlServicioNoShow=@FlServicioNoShow

