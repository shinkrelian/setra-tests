﻿
Create Procedure dbo.PRE_LIQUI_DET_RptCostosNoGastoAdm
@IDNotaVenta char(10)
As
	Set NoCount On
	Select   toc.Orden,
			 pd.idTipoOC,  
			 toc.Descripcion as DescTipoOC,  
			 p.NombreCorto As DescProveedor,  
			 pd.Descripcion As Servicio,  
			 pd.SubTotal As Neto,  
			 pd.TotalIGV As Igv,  
			 pd.Total AS Total,
			 (select SUM(Total) from DEBIT_MEMO 
			  where IDCab = (select IDCab from NOTA_VENTA 
			  where IDNotaVenta= @IDNotaVenta)) As TotalDebitMemo
	from PRE_LIQUI_DET pd
	Left Join OPERACIONES_DET od On pd.IDOperacion_Det = od.IDOperacion_Det
	Left Join MASERVICIOS_DET sd On od.IDServicio_Det = sd.IDServicio_Det
	Left Join MATIPOOPERACION toc On toc.IdToc = pd.idTipoOC
	Left Join MASERVICIOS s On sd.IDServicio = s.IDServicio
	Left Join MAPROVEEDORES p on s.IDProveedor = p.IDProveedor
	Where pd.IDNotaVenta = @IDNotaVenta And pd.IDTipoOC <> '005'
	Order By toc.Orden
