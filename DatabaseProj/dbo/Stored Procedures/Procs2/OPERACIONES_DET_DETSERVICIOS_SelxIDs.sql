﻿
--JRF-20130226-Nueva columna IDMoneda,TipoCambio  
--JRF-20130207-Nombre de Columna    
--JRF-20130724-Agregando  IDTipoServ
--HLF-20150630-ds.QtPax - isnull(d.NroLiberados,0) as NroPax, 
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_SelxIDs        
 @IDOperacion_Det int,        
 @IDServicio_Det int        
As        
 Set NoCount On          
         
 Select upper(substring(DATENAME(dw,d.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,d.Dia,103))) As Dia,        
 substring(convert(varchar,d.Dia,108),1,5) As Hora,        
 ISNull(sd.Descripcion,sd2.Descripcion) as DescServicio,          
 --d.NroPax, 
 ds.QtPax - isnull(d.NroLiberados,0) as NroPax, 
 c.IDIdioma, ds.Total_Prg As Total,ds.IDMoneda, ISNULL(ds.TipoCambio,0) As TipoCambio,  
 Case when c.NroPax <> d.NroPax then 1 else 0 End As ExistDiferPax  
  ,isnull(d.ObservBiblia,'') As ObservBiblia   ,s.IDTipoServ  
 From OPERACIONES_DET_DETSERVICIOS ds         
 Left Join OPERACIONES_DET d On ds.IDOperacion_Det=d.IDOperacion_Det        
 Left Join MASERVICIOS_DET sd On ds.IDServicio_Det_V_Cot=sd.IDServicio_Det        
 Left Join MASERVICIOS_DET sd2 On d.IDServicio_Det=sd2.IDServicio_Det        
 Left Join OPERACIONES o On o.IDOperacion=d.IDOperacion        
 Left Join MASERVICIOS s On s.IDServicio=d.IDServicio      
 Left Join COTICAB c On o.IDCab=c.IDCAB        
 Where ds.IDOperacion_Det=@IDOperacion_Det And         
 ds.IDServicio_Det_V_Cot=@IDServicio_Det 

