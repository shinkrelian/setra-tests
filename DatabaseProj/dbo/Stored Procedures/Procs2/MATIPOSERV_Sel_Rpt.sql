﻿Create Procedure [dbo].[MATIPOSERV_Sel_Rpt]
	@Descripcion varchar(60)
As	
	Set NoCount On

	Select IDservicio,Descripcion, Ingles
	From MATIPOSERV Where (Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='')
	Order by 2
