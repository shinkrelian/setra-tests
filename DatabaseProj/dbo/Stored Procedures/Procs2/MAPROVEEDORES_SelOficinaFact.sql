﻿--MAPROVEEDORES_SelOficinaFact '000113'
--JHD-20150126 - TipoDoc_Identidad=isnull((select t.Descripcion from MATIPOIDENT t where t.IDIdentidad=MAPROVEEDORES.IDIdentidad),'')
--JRF-20151104-...And IDCiudad=@CoUbigeo_Oficina
CREATE Procedure dbo.MAPROVEEDORES_SelOficinaFact
@CoUbigeo_Oficina char(6)
As
Set NoCount On

Select *,
TipoDoc_Identidad=isnull((select t.Descripcion from MATIPOIDENT t where t.IDIdentidad=MAPROVEEDORES.IDIdentidad),'')
from MAPROVEEDORES 
Where IDTipoOper='I' and IDTipoProv='003' and CoUbigeo_Oficina=@CoUbigeo_Oficina And IDCiudad=@CoUbigeo_Oficina
