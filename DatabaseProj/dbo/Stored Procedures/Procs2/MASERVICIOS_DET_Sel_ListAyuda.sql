﻿CREATE Procedure [dbo].[MASERVICIOS_DET_Sel_ListAyuda]
	@IDTipoProv	char(3),
	@IDProveedor	char(6),
	@IDServicio	char(8),
	@Categoria	tinyint,
	@IDCat	char(3)
	
As	
	Set NoCount On
	
	Select p.NombreCorto as DescProveedor, 
	d.Tipo,
	s.Descripcion as DescServicio, 
	d.Descripcion as DescDetServicio,
	d.IDServicio_Det  
	From MASERVICIOS_DET d 
	Left Join MASERVICIOS s On d.IDServicio=s.IDServicio
	Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
	Where s.IDTipoProv=@IDTipoProv And 
	(s.IDProveedor=@IDProveedor Or ltrim(rtrim(@IDProveedor))='') And 
	(d.IDServicio=@IDServicio Or ltrim(rtrim(@IDServicio))='')
	And (s.Categoria=@Categoria Or @Categoria=0)
	And (s.IDCat=@IDCat Or LTRIM(rtrim(@IDCat))='')

	Order by p.RazonSocial, s.Descripcion
