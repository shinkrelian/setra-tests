﻿
CREATE Procedure [dbo].[MASERVICIOS_RANGO_APT_Upd]
	@Correlativo tinyint,  
    @IDServicio char(8),  
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(10,4),  
    @UserMod char(4),
	@IDTemporada int
AS
BEGIN
Set NoCount On  
UPDATE MASERVICIOS_RANGO_APT  
        Set PaxDesde=@PaxDesde   
        ,PaxHasta=@PaxHasta  
        ,Monto=@Monto  
        ,UserMod=@UserMod  
    WHERE  
        Correlativo=@Correlativo And
		IDTemporada = @IDTemporada AND
        (IDServicio = @IDServicio)
END
