﻿
CREATE PROCEDURE [dbo].[MAPROVEEDORES_Upd_Extranet_Restablecer_Pass]  
						@IDProveedor char(6),   
						@UsuarioLogeo_Hash varchar(max),
						@UserMod char(4)
AS
BEGIN
	Set NoCount On
	UPDATE MAPROVEEDORES Set  
		UsuarioLogeo_Hash=Case When ltrim(rtrim(@UsuarioLogeo_Hash))='' Then Null Else @UsuarioLogeo_Hash End  
		,FechaHash=DATEADD(HOUR,4, getdate())
		,UserMod=@UserMod             
		,FecMod=GETDATE()
		,FlAccesoWeb = 1 
	WHERE  
		IDProveedor = @IDProveedor  
END;
