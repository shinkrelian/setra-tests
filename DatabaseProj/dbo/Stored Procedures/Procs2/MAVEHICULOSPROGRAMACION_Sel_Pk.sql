﻿

--JHD-20150422 - Se cambio la tabla por MAVEHICULOS_TRANSPORTE
CREATE procedure dbo.MAVEHICULOSPROGRAMACION_Sel_Pk
@NuVehiculoProg smallint
As
	Set NoCount On
	--select * from MAVEHICULOSPROGRAMACION 
	select *,NoVehiculoCitado as NoUnidadProg from MAVEHICULOS_TRANSPORTE
	where NuVehiculo = @NuVehiculoProg
