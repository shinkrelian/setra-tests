﻿
 Create Procedure dbo.MASERVICIOS_RANGO_APT_SelxCiudadFOC
 	@CoCiudad char(6)
 As
	Set Nocount on

	Select  rn.IDServicio, rn.PaxDesde, rn.PaxHasta, rn.Monto
	From MASERVICIOS_RANGO_APT rn Inner Join MASERVICIOS s 
	On rn.IDServicio=s.IDServicio And rn.IDTemporada=6 And s.IDubigeo=@CoCiudad
		And s.APTServicoOpcional='F' 
