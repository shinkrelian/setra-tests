﻿Create Procedure dbo.MASERVICIOS_Sel_ServiciosTCxAnio
@Anio char(4)
As
Set NoCount On
Select * from (
Select ROW_NUMBER()Over(Partition By sd.IDServicio Order By sd.CoMoneda desc) As FirstItem,
	   sd.*,ub.IDPais,s.IDubigeo as IDCiudad,p.IDTipoProv,p.IDProveedor,p.NombreCorto as DescProveedor,
	   s.Descripcion as DescDet,ub.Descripcion as DescCiudad,s.Transfer,IsNull(s.TipoTransporte,'') TipoTransporte,
	   s.Descripcion as DescServicio,s.IDTipoServ
from MASERVICIOS_DET sd Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio
Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor
Left Join MAUBIGEO ub On s.IDubigeo=ub.IDubigeo
Where s.FlServicioSoloTC=1 and Anio=@Anio
) as X
Where X.FirstItem = 1
