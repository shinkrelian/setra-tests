﻿
Create Procedure dbo.ORDEN_SERVICIO_DET_SelxOrdenPago
	@IDCab int,
	@IDOrdPag int
As
	Set Nocount On
	
	SELECT Distinct NuOrden_Servicio
	FROM ORDEN_SERVICIO_DET WHERE NuOrden_Servicio IN (SELECT NuOrden_Servicio FROM ORDEN_SERVICIO 
		WHERE IDCab=@IDCab)
	AND IDServicio_Det In 
	(SELECT IDServicio_Det
	FROM ORDENPAGO_DET WHERE IDOrdPag IN (SELECT IDOrdPag FROM ORDENPAGO 
		WHERE IDCab=@IDCab)
	AND IDOrdPag=@IDOrdPag)



