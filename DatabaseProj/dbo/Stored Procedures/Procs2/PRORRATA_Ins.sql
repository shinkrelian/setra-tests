﻿Create Procedure dbo.PRORRATA_Ins
@FeDesde smalldatetime,
@FeHasta smalldatetime,
@QtProrrata numeric(8,2),
@UserMod char(4)
As
	Set NoCount On
	Declare @NuCodigoPro smallint
	Execute dbo.Correlativo_SelOutput 'PRORRATA',1,10,@NuCodigoPro out
	
	Insert Into PRORRATA values (@NuCodigoPro,@FeDesde,@FeHasta,@QtProrrata,@UserMod,GetDate())
