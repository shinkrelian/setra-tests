﻿CREATE procedure [dbo].[MAUSUARIOS_Sel_Rpt] 
@Nombre varchar(60)
as
	Set NoCount On
	select Usuario,Nombre,nu.Descripcion as Cargo,Direccion,Telefono,Celular,
	convert(char(10),FechaNac,103) as FechaNac
	from MAUSUARIOS u Left Join MANIVELUSUARIO nu On u.Nivel=nu.IDNivel
	where (LTRIM(rtrim(@Nombre))='' Or Nombre like '%'+@Nombre+'%')
