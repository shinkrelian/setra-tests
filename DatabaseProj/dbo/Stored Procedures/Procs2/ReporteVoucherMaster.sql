﻿CREATE Procedure [dbo].[ReporteVoucherMaster]
	@IDCab	int
As
	Set Nocount On
	
	SELECT X.FechaIniFinFile, X.Titulo, X.IDFile, 
	X.IDVoucher, X.IDTipoProv, X.DescTipoProvee, X.DescUbigeo, X.DescProvee,
	Case X.IDTipoProv
		When '001' Then	X.FechaIn+ (Case When X.FechaOut='' Then '' Else ' - '+X.FechaOut End) 
		When '002' Then X.FechaIn
		Else X.FechaIn+' '+X.HoraIn
		 
	End As DiaIniFin,
	X.Cantidad, X.Servicio, X.Buffet, X.NroPax, X.Idioma 
	FROM
	(
	Select convert(varchar,cc.Fecha,103)+' - '+ convert(varchar,cc.FechaOut,103) as FechaIniFinFile, cc.Titulo ,o.IDFile, od.IDVoucher, 
	--od.Dia,od.FechaOut,
	upper(substring(DATENAME(dw,od.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,od.Dia,103))) as FechaIn,
	substring(convert(varchar,od.Dia,108),1,5) as HoraIn,  
	IsNull(upper(substring(DATENAME(dw,od.FechaOut),1,3))+' '+ltrim(rtrim(convert(varchar,od.FechaOut,103))),'') as FechaOut,
	od.Cantidad, od.Servicio,
	LTRIM(Case When od.Desayuno=1 Then 'DESAYUNO ' Else '' End+
	Case When od.Lonche=1 Then 'BOX LUNCH ' Else '' End+	
	Case When od.Almuerzo = 1 Then 'ALMUERZO ' Else '' End+	
	Case When od.Cena=1 Then 'CENA' Else '' End) as Buffet,
	p.IDTipoProv,
	tp.Descripcion as DescTipoProvee, u.Descripcion as DescUbigeo,
	p.NombreCorto as DescProvee,
	
	Cast(od.NroPax as varchar(3)) + ' Pax' as NroPax, '('+Left(od.IDIdioma,3)+')' as Idioma
	From OPERACIONES_DET od 
	Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion And o.IDCab=@IDCab And od.IDVoucher<>0
	Left Join MAPROVEEDORES p On p.IDProveedor=o.IDProveedor
	Left Join MATIPOPROVEEDOR tp On p.IDTipoProv=tp.IDTipoProv
	Left Join MAUBIGEO u On od.IDubigeo=u.IDubigeo
	Left Join COTICAB cc On o.IDCab=cc.IDCAB
	) AS X
	
	Order by X.DescTipoProvee Desc, X.IDVoucher
	



