﻿Create Procedure dbo.OPERACIONES_DET_Upd_IDVehiculo_Prg
	@IDOperacion_Det	int,
	@IDVehiculo_Prg  smallint,
	@UserMod char(4)
As
	Set Nocount On  
	
	Update OPERACIONES_DET 
	Set IDVehiculo_Prg =@IDVehiculo_Prg ,
	UserMod=@UserMod,
	FecMod=GETDATE()
	Where IDOperacion_Det=@IDOperacion_Det
	
