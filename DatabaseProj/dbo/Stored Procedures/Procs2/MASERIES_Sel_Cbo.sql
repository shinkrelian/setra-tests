﻿Create Procedure MASERIES_Sel_Cbo
@ConDescrip bit,
@DescrTodo bit,
@Todos bit
As
	Set NoCount On
	select * from (
	select '' As IDSerie,Case When @DescrTodo = 1 then '<TODOS>' Else '---'End As Descripcion,0 As Ord
	Union 
	Select IDSerie, Case When @ConDescrip = 1 then Descripcion Else IDSerie End AS Descripcion 
	,1 As Ord from MASERIES
	)As X
	Where (@Todos = 1 Or Ord <> 0)

