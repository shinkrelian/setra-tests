﻿CREATE procedure dbo.MAUBIGEO_Sel_CboSelCodigo  
As  
Set NoCount On  
Select ISNULL(dc,'') as Cod,Descripcion from MAUBIGEO   
where TipoUbig = 'CIUDAD'   
Order by Descripcion
