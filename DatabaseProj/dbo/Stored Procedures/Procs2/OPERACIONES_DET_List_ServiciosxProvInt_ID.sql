﻿--create
create Procedure [Dbo].[OPERACIONES_DET_List_ServiciosxProvInt_ID]                
 @IDProveedor char(6)='',                                      
 @IdCab Integer=0,
 @FlServicioTC bit=0                                      
As                                      
 Set NoCount On                                      
      
/*
declare @tmpOperaciones as table
(
IDOperacion_Det int null,
IDServicio_Det int null
)

insert into @tmpOperaciones	
*/
	                            
 SELECT distinct IDOperacion_Det,IDServicio_Det                                            
                           
 FROM (                                              
                                   
 SELECT DiaFormatFecha,                                      
 Dia,Hora,DiaReal ,Item, IDOperacion, DescServicio, Variante, IDTipoServ,IDServicio,                                              
 IDCiudad,IDTipoProv,DescTipoServ,IDProveedor,CorreoReservas,IDPais,                  
 Desayuno,Lonche,Almuerzo,Cena,Transfer,                                              
 --Anio, Variante,                                              
 IDOperacion_Det,                                              
 NroPax, PaxmasLiberados, NroLiberados, Tipo_Lib, TipoPax,TotalCotizado,TotalProgramado,                             
                                           
 Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                              
 Else                                              
 IsNull(                   
                                              
(Select Distinct Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                             
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                        
   Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                            
 Where od1.IDOperacion_Det=X.IDOperacion_Det                   
    And od1.IDServicio=X.IDServicio                                    
)                         
                        
 ,'NV')                                              
 End as EstadoGuia,                                              
                                            
 Case When CantTranspProgramados>1 Then 'MULTIPLE'                                              
 Else                                              
 IsNull(                                              
  (Select Distinct Top 1 od1.IDEstadoSolicitud From OPERACIONES_DET_DETSERVICIOS od1                                                     
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                            
   Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                          
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                    
    And od1.IDServicio=X.IDServicio                                    
)                        
 ,'NV')                                              
 End as EstadoTransportista  , ObservVoucher,ObservBiblia,ObservInterno,                                            
                                            
                                            
 Case When CantGuiasProgramados>1 Then 'MULTIPLE'                                     
 Else                                              
 IsNull(                                              
 (Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                                     
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                          
    Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                  
                                            
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                    
   And od1.IDServicio=X.IDServicio                                    
)                                      
 ,'')                                              
 End                                              
 as IDGuiaProgramado,                                              
                                            
                                            
 Case When CantTranspProgramados >1 Then 'MULTIPLE'                                              
 Else                                              
 IsNull(                                              
 (Select Distinct Top 1 od1.IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                       
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                          
  Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                  
                                    
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                    
  And od1.IDServicio=X.IDServicio      
)                         
 ,'')                                              
 End                                              
 as IDTransportistaProgramado,                                            
                                            
                                            
             
                          
 Case When CantGuiasProgramados>1 Then 'MULTIPLE'               Else                                              
 IsNull(                                              
(Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                     
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                         
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                  
                                             
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                   
   And od1.IDServicio=X.IDServicio                                    
 )                                                       
 ,'')                                              
 End                                              
 as GuiaProgramado,                                              
                                            
                               
 Case When CantTranspProgramados >1 Then 'MULTIPLE'                                              
 Else                                              
 IsNull(                                              
 (Select Distinct Top 1 p1.NombreCorto From OPERACIONES_DET_DETSERVICIOS od1                                                     
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                      
  Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                                                  
 Where od1.IDOperacion_Det=X.IDOperacion_Det                                     
 And od1.IDServicio=X.IDServicio                                    
 )                                                
 ,'')                                              
 End                                              
 as TransportistaProgramado,                           
 --(Select idio3.Siglas From COTICAB cc4 Left Join MAIDIOMAS idio3 On cc4.IDIdioma = idio3.IDidioma Where IDCAB=@IDCab)           
 IdiomaSiglaOD          
 as IDIdioma,          
 X.ExistDiferPax, MotivoEspecial,                            
 X.IDServicio_Det ,X.RutaTraslado  , X.VehiculoProg ,X.NombreTrasladista  ,X.CodigoPNR,            
 X.CambiosenReservasPostFile              
                 
                    
 FROM                                              
 (                                              
 Select Distinct D.Dia as DiaFormatFecha,  D.Dia As DiaReal   ,                            
                       
 (SELECT Item FROM COTIDET WHERE IDDET = (SELECT IDDET FROM RESERVAS_DET rd3 WHERE rd3.IDReserva_Det = D.IDReserva_Det)) As Item,                                          
 upper(substring(DATENAME(dw,D.Dia),1,3))+' '+ltrim(rtrim(convert(varchar,D.Dia,103)/*+' '+ substring(convert(varchar,D.Dia,108),1,5)*/)) As Dia                                              
 ,convert(varchar(5),D.Dia,108) As Hora                                            
 ,D.IDOperacion,                                      
 --S.Descripcion as DescServicio                                      
 --rd.Servicio as DescServicio                                      
 d.Servicio as DescServicio                             
 ,ST.Tipo as Variante,S.IDTipoServ,D.IDServicio                                            
 ,D.IDubigeo as IDCiudad,S.IDTipoProv,ts.Descripcion as DescTipoServ,S.IDProveedor ,                                                
 Case When ISNULL(P.Email3,'')='' Then P.Email4 Else P.Email3 End as CorreoReservas,u.IDPais,                                                
 D.Desayuno,D.Lonche,D.Almuerzo,D.Cena,D.Transfer ,                                              
 --ST.Anio, IsNull(ST.Tipo,'') as Variante, 
 --od.IDOperacion_Det,                          
 D.IDOperacion_Det,                                               
 D.NroPax,                                       
 --CAST(d.NroPax AS VARCHAR(3))+                                          
 --Case When isnull(d.NroLiberados,0)=0 then '' else '+'+CAST(d.NroLiberados AS VARCHAR(3)) end as PaxmasLiberados,                                            
 d.NroPax+isnull(d.NroLiberados,0) as PaxmasLiberados,                                            
 D.NroLiberados, D.Tipo_Lib, cc.TipoPax,                                              
                                            
 (Select SUM(Total) From OPERACIONES_DET_DETSERVICIOS Where IDOperacion_Det=D.IDOperacion_Det and                                               
 Not IDServicio_Det_V_Cot Is Null) as TotalCotizado,                                              
 (Select SUM(Total) From OPERACIONES_DET_DETSERVICIOS Where IDOperacion_Det=D.IDOperacion_Det and                                               
 Not IDServicio_Det_V_Prg Is Null) as TotalProgramado,                                               
                                 
 st.IDServicio_Det ,                                 
                                            
 (Select count(*) From                                              
 (                                              
 Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                               
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv in('005','008')                                            
 Where od1.IDOperacion_Det=D.IDOperacion_Det --and od1.IDServicio_Det=st.IDServicio_Det                                             
 ) as x                                                
 ) as CantGuiasProgramados,                                              
                                            
 (Select count(*) From                                              
 (                                              
 Select Distinct IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1                                               
 Inner Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor And p1.IDTipoProv='004'                                              
 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det                              
 Where od1.IDOperacion_Det=D.IDOperacion_Det --and od1.IDServicio_Det=st.IDServicio_Det                                                          
 And od1.IDServicio=D.IDServicio                              
 ) as x                                            
 ) as CantTranspProgramados,                                            
 isnull(D.ObservVoucher,'') As ObservVoucher,                                            
 isnull(D.ObservBiblia,'') As ObservBiblia ,isnull(D.ObservInterno,'') As  ObservInterno,                                    
 Case when D.NroPax <> cc.NroPax then 1 else 0 End As ExistDiferPax,                            
 isnull(cd.MotivoEspecial,'') as MotivoEspecial ,                          
                           
 ----IsNull((select distinct Ruta from COTIVUELOS cv Left Join MAUBIGEO ub On cv.RutaD = ub.IDubigeo                          
 ----    where IDCAB = @IdCab                           
 ----    and convert(char(10),Fec_Salida,103)= CONVERT(char(10),D.Dia,103)                          
 ----    and convert(char(5),D.Dia,108) =Llegada),'')       
  ''   As RutaTraslado     ,                
 case when D.IDVehiculo_Prg Is Null then '' else                
  vhp.NoVehiculoCitado+' - '+ CAST(vhp.QtCapacidad as varchar(5))+' Pax'                
  End As VehiculoProg,                
                  
  ISNULL((select distinct  us.Nombre                
   from OPERACIONES_DET_DETSERVICIOS odd2 Inner Join MAUSUARIOS us On LTRIM(rtrim(odd2.IDProveedor_Prg))=us.IDUsuario          
   where odd2.IDOperacion_Det= D.IDOperacion_Det),'')                
   as NombreTrasladista ,              
   dbo.FnPNRxIDCabxDia(o.IDCab,D.Dia) As CodigoPNR,            
   o.CambiosenReservasPostFile  ,          
   idioD.Siglas as IdiomaSiglaOD          
               
 FROM OPERACIONES_DET D  Inner JOIN MASERVICIOS S ON D.IDServicio=S.IDServicio AND S.IDProveedor=@IDProveedor                                         
 And S.IDTipoProv<>'016'                                        
 Inner JOIN MAPROVEEDORES P On S.IDProveedor = P.IDProveedor                                                 
 Inner JOIN MATIPOSERVICIOS ts On S.IDTipoServ=ts.IDTipoServ                                                    
 Inner JOIN MAUBIGEO u On p.IDCiudad=u.IDubigeo                                         
 Inner JOIN MASERVICIOS_DET ST On D.IDServicio_Det = ST.IDServicio_Det                                              
 --Inner  JOIN OPERACIONES_DET_DETSERVICIOS od On D.IDOperacion_Det = od.IDOperacion_Det                                              
 Left JOIN OPERACIONES_DET_DETSERVICIOS od On D.IDOperacion_Det = od.IDOperacion_Det                                              
 Inner Join OPERACIONES o On d.IDOperacion=o.IDOperacion And o.IDCab=@IdCab       
 Inner Join COTICAB cc On o.IDCab=cc.IDCAB                    
 LEFT Join RESERVAS_DET rd On d.IDReserva_Det=rd.IDReserva_Det And rd.Anulado=0            
 Left Join RESERVAS r On rd.IDReserva=r.IDReserva And IsNull(r.Anulado,0)=0            
 Left Join COTIDET cd On rd.IDDet=cd.IDDET                                    
 --Left Join MAVEHICULOSPROGRAMACION vhp On D.IDVehiculo_Prg = vhp.NuVehiculoProg                
 Left Join MAVEHICULOS_TRANSPORTE vhp On D.IDVehiculo_Prg = vhp.NuVehiculo                
 --Left Join MAUSUARIOS uTras On D.IDTrasladista_Prg = uTras.IDUsuario                
 Left Join MAIDIOMAS idioD on rd.IDIdioma = idioD.IDidioma          
 Where D.FlServicioTC = @FlServicioTC                                       
 ) as X  ) AS Y                                            
 Left Join MAPROVEEDORES PG On Y.IDGuiaProgramado=PG.IDProveedor                                            
 Left Join MAPROVEEDORES PT On Y.IDTransportistaProgramado=PT.IDProveedor                                      
-- Order by Y.DiaReal,Y.Item  
