﻿
CREATE Procedure [dbo].[MASERVICIOS_RANGO_APT_Sel_xIDServicio]	
    @IDServicio char(8)
AS
BEGIN
	Set NoCount On
	
	Select Correlativo, PaxDesde, PaxHasta, Monto, R.IDTemporada, ISNULL(D.DescripcionAlterna,'') AS DescripcionAlterna, ISNULL(AgruparServicio,0) AS AgruparServicio,
	ISNULL(D.PoliticaLiberado,0) AS PoliticaLiberado,
	ISNULL(D.Liberado,0) AS Liberado, ISNULL(D.TipoLib,'') AS TipoLib, ISNULL(MaximoLiberado,0) AS MaximoLiberado, ISNULL(MontoL,0) AS MontoL
	From MASERVICIOS_RANGO_APT R INNER JOIN MASERVICIOS_DESC_APT D
	ON R.IDServicio = D.IDServicio AND R.IDTemporada = D.IDTemporada
	Where R.IDServicio = @IDServicio
	Order by R.IDTemporada, PaxDesde
END

