﻿--JRF-20150408-Se agrego los proveedores ...'001551','001512','001815','001821') y ...In('CUZ00216')
--JRF-20150424-Considerar la unión entre los servicios manuales.
--JRF-20150603-Implementar función para registros válidos(Anio,tipo y variante)
--JRF-20150603-Cambiar el cód. de Pre-Compra por Stock.
--JRF-20150604-Agregar el costo por Guía
--HLF-20151027-0 como 2da columna
--HLF-20151028-case when sd.Tipo='GUIDE' then 1 else od.NroPax+IsNull(od.NroLiberados,0) end
--HLF-20151109-..(Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sdd.IDServicio..)
--HLF-20151204-(p4.IDProveedor ...parentesis
--HLF-20151214-IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)=sd.IDServicio_Det
--HLF-20151215-When 'STK' Then 'STOCK ('+Cast...
--HLF-20151215-And Not s.IDServicio in ('CUZ00136')
--And Not (s.IDServicio in ('CUZ00135') And not isnull(sdCot.Tipo,'') IN('GUIDE','GUIDE2','GU2-APT'))
--HLF-20151217-... where pGuia.IDProveedor=ODD.IDProveedor_Prg) as DescProveeGuia,ODD.IDProveedor_Prg as IDProveedorGuia
--HLF-20151221- isnull(ODD.IDProveedor_Prg,(Select Distinct Top 1 IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS o ...  as IDProveedorGuia
--Case When isnull(s.CoPago,'EFE') ='TCK' Then 'SOL' 
--Case When sd.IDServicio='CUZ00416' then 
--HLF-20151226-TotalPreCompra
--HLF-20160120-Comentando dbo.FnRegistroOperacionesDetServiciosValido...
--HLF-20160204-isnull(PD.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot .... x.IDServicio_Det_V_Cot
--HLF-20160216-'003473' a p4.idproveedor in(
--HLF-20160223-'002699' a p4.idproveedor in(
--JHD-20160310- Or odd.IDServicio In('CUZ00216') OR  s.CoPago In('TCK')
--HLF-20160315- agregando 'CUZ00185','CUZ00906','CUZ00913','CUZ00184','CUZ00198' a Or odd.IDServicio In('CUZ00216',...) 
--HLF-20160316-And Not Exists(Select IDOperacion_Det From PRESUPUESTO_SOBRE_DET ...
-- And @NuPreSob=0		And @NuPreSob<>0
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_Sel_ListGenPresCuscoTMP
@IDCab int,
@IDProveedor char(6),
--@GuiaProv char(6),
@NuPreSob int
As
Set NoCount On
Select X.IDServicio_Det,x.IDServicio_Det_V_Cot,X.IDOperacion_Det,X.NuDetPSo,X.NuSincerado_Det,X.Dia,X.Hora,
Case When X.IDProveedorGuia<>'' then
	Case When len(X.IDProveedorGuia)=6 then
		isnull((select NombreCorto from MAPROVEEDORES pGuia where pGuia.IDProveedor=X.IDProveedorGuia),'') 
	Else
		isnull((select Nombre+' '+ISNULL(TxApellidos,'') From MAUSUARIOS Where IDUsuario=X.IDProveedorGuia),'') 
	End 
Else 
''
End as DescProveeGuia,
X.IDProveedorGuia,X.IDPax,X.DescServicio,X.Pax,
	   X.IDMoneda,X.SimMoneda,X.PreUnit,X.Total,
	   x.Total as TotalPreCompra,
	   X.Sustentado,X.Devuelto,X.RegistradoPresup,X.RegistradoSincerado,X.CoTipPSo,
	 (select NoTipPSo from MATIPOPRESUPUESTO_SOBRE where CoTipPSo=X.CoTipPSo) as DescCoTipPSo, X.CoPago,Cast(X.DescPago as varchar(50)) as DescPago,'R' as RegVal--,X.IDServicio,X.NombreCorto,X.IDProveedor
from (
Select IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det) as IDServicio_Det,
		isnull(odd.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,
		odd.IDOperacion_Det,
	   ROW_NUMBER()Over(Order By od.Dia) as NuDetPSo,0 as NuSincerado_Det,od.Dia,Convert(char(5),od.Dia,108) as Hora,
	   --(select NombreCorto from MAPROVEEDORES pGuia where pGuia.IDProveedor=@GuiaProv) as DescProveeGuia,
	   --isnull((select NombreCorto from MAPROVEEDORES pGuia where pGuia.IDProveedor=ODD.IDProveedor_Prg),'') as DescProveeGuia,
	   --@GuiaProv as IDProveedorGuia,
	   isnull(isnull(ODD.IDProveedor_Prg,(Select Distinct Top 1 IDProveedor_Prg From OPERACIONES_DET_DETSERVICIOS od1           
		 Left Join MAPROVEEDORES p1 On od1.IDProveedor_Prg=p1.IDProveedor 
		 Left Join MAUSUARIOS u1 On od1.IDProveedor_Prg=u1.IDUsuario 
		 Inner Join MASERVICIOS_DET sd1 On od1.IDServicio_Det=sd1.IDServicio_Det  
		 Where od1.IDOperacion_Det=odd.IDOperacion_Det                                 
		 And od1.IDServicio=odd.IDServicio
		 And (p1.IDTipoProv in('005','008') or not u1.IDUsuario is null)
		 )),'') as IDProveedorGuia,
	   0 as IDPax,sd.Descripcion as DescServicio,
	   case when sd.Tipo='GUIDE' then 1 else od.NroPax+IsNull(od.NroLiberados,0) end as Pax,

	   Case When isnull(s.CoPago,'EFE') ='TCK' Then 'SOL' Else
			Case When isnull(s.CoPago,'EFE') ='STK' Then '' Else odd.IDMoneda End End As IDMoneda,
	   Case When isnull(s.CoPago,'EFE') ='TCK' Then 'S/.' Else
			Case When isnull(s.CoPago,'EFE') ='STK' Then '' Else mo.Simbolo End End as SimMoneda, 

	   Case When isnull(s.CoPago,'EFE') ='TCK' Then 
			Case When sd.IDServicio='CUZ00416' then --Tips Airport
				1.5
			Else
				1 
			End
	   Else 
		   IsNull(odd.TotalProgram, IsNull(sd.Monto_sgls, IsNull(sd.Monto_sgl,
			(select Monto from MASERVICIOS_DET_COSTOS Where IDServicio_Det=sd.IDServicio_Det And 
				(case when sd.Tipo='GUIDE' then 1 else od.NroPax+IsNull(od.NroLiberados,0) end)
				 between PaxDesde And PaxHasta) / case when sd.Tipo='GUIDE' then 1 else od.NroPax+IsNull(od.NroLiberados,0) end
		   ))) 
	   End as PreUnit,
	   Case When isnull(s.CoPago,'EFE') ='STK' Then 0 Else 
	   --Case When isnull(s.CoPago,'EFE') in('STK','PCM') Then 0 Else 
			Case When isnull(s.CoPago,'EFE') ='TCK' Then 
				Case When sd.IDServicio='CUZ00416' then --Tips Airport
					1.5
				Else
					1 
				End
			else

			   IsNull(odd.TotalProgram, IsNull(sd.Monto_sgls, IsNull(sd.Monto_sgl,
				(select Monto from MASERVICIOS_DET_COSTOS Where IDServicio_Det=sd.IDServicio_Det And 
					case when sd.Tipo='GUIDE' then 1 else od.NroPax+IsNull(od.NroLiberados,0) end 
					between PaxDesde And PaxHasta) / case when sd.Tipo='GUIDE' then 1 else od.NroPax+IsNull(od.NroLiberados,0) end))) 

			End	

			* case when sd.Tipo='GUIDE' then 1 else od.NroPax+IsNull(od.NroLiberados,0) end End as Total,
	   0 as Sustentado,0 as Devuelto,0 as RegistradoPresup,0 As RegistradoSincerado,
	   case when p4.IDTipoProv='006' Then 'EN' else 
		case when s.copago in('EFE','TCK') and s.Descripcion like 'Tips%' then 'PR' else 'VA' end end As CoTipPSo,ISNULL(s.CoPago,'') AS CoPago,
	    Case  isnull(s.CoPago,'EFE')
		 When 'EFE' Then 'EFECTIVO'                  
		 When 'TCK' Then 'TICKET'   
		 When 'PCM' Then 'PRECOMPRA'               
		 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS stk Where stk.IDServicio=sd.IDServicio),0) As varchar(4))+')'              
		 End As DescPago,odd.IDServicio,p4.NombreCorto,p4.IDProveedor,
		 --dbo.FnRegistroOperacionesDetServiciosValido(odd.IDOperacion_Det,odd.IDServicio_Det) as DatoValido
		 1 as DatoValido
from OPERACIONES_DET_DETSERVICIOS odd Inner Join OPERACIONES_DET od 
On od.IDServicio=odd.IDServicio and od.IDOperacion_Det=odd.IDOperacion_Det
Inner Join OPERACIONES o On od.IDOperacion=o.IDOperacion --and o.IDCab=@IDCab and o.IDProveedor=@IDProveedor
Inner Join MASERVICIOS_DET sd On --IsNull(odd.IDServicio_Det_V_Cot,0)=sd.IDServicio_Det
	IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)=sd.IDServicio_Det
Left Join MAMONEDAS mo On odd.IDMoneda=mo.IDMoneda
Inner Join MASERVICIOS s On sd.IDServicio=s.IDServicio
Left Join MAPROVEEDORES p4 On s.IDProveedor=p4.IDProveedor
Left Join MASERVICIOS_DET sdCot On odd.IDServicio_Det_V_Cot=sdCot.IDServicio_Det

Where o.IDCab=@IDCab and o.IDProveedor=@IDProveedor and odd.Activo=1 and 
--Charindex('entrance fee',Lower(sd.Descripcion)) > 0) As X
--p4.IDProveedor In('001524','001553','001550','001551','001512','001815','001821','001819','001593') Or odd.IDServicio In('CUZ00216')
(p4.IDProveedor In('001524','001553','001550','001551','001512','001815','001821','001819','001593','003473','002699') 
	--Or odd.IDServicio In('CUZ00216') 
	Or odd.IDServicio In('CUZ00216','CUZ00185','CUZ00906','CUZ00913','CUZ00184','CUZ00198','CUZ00175') 
	OR  s.CoPago In('TCK')
And Not s.IDServicio in ('CUZ00136')
And Not (s.IDServicio in ('CUZ00135') And not isnull(sdCot.Tipo,'') IN('GUIDE','GUIDE2','GU2-APT')))

	--And Not Exists(Select pd1.IDOperacion_Det From PRESUPUESTO_SOBRE_DET pd1 
	--		--inner join PRESUPUESTO_SOBRE p1 on pd1.NuPreSob=p1.NuPreSob And p1.IDCab=@IDCab And p1.IDProveedor=@IDProveedor
 --           Where 
	--		pd1.IDOperacion_Det=od.IDOperacion_Det --And isnull(CoPrvPrg,'')=ltrim(rtrim(@IDProveedor_PrgGuia))
	--			And isnull(pd1.IDServicio_Det_V_Cot,pd1.IDServicio_Det)=IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)
	--			)
	 And @NuPreSob=0
) as X
Where x.DatoValido=1
 Union
 Select IsNull(pd.IDServicio_Det,0) as IDServicio_Det, isnull(PD.IDServicio_Det_V_Cot,0) as IDServicio_Det_V_Cot,IsNull(PD.IDOperacion_Det,0) as IDOperacion_Det,
 pd.NuDetPSo,0 as NuSincerado_Det,pd.FeDetPSo as Dia,Convert(char(5),pd.FeDetPSo,108) as Hora,
 IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as DescProveeGuia,
 IsNull(pd.CoPrvPrg,'') as CoPrvPrg,IsNull(pd.IDPax,0) as IDPax, pd.TxServicio,
 pd.QtPax,IsNull(pd.IDMoneda,'') as IDMoneda,IsNull(mo.Simbolo,'') as SimMoneda, pd.SsPreUni, pd.SsTotal as Total, 
 pd.SsTotal as TotalPreCompra,
 pd.SsTotSus as Sustentado, pd.SsTotDev as Devuelto, 1 as RegistradoPresup,0 as RegistradoSincer,pd.CoTipPSo,es.NoTipPSo as DescCotipPSo,
 pd.CoPago,
 Case pd.CoPago           
 When 'EFE' Then 'EFECTIVO'        
 When 'TCK' Then 'TICKET'                
 When 'PCM' Then 'PRECOMPRA'
 --When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where IDServicio_Det=pd.IDServicio_Det),0) As varchar(4))+')'            
 When 'STK' Then 'STOCK ('+Cast(isnull((Select qtstkent From STOCK_TICKET_ENTRADAS Where 
	IDServicio=(select IDServicio from MASERVICIOS_DET where IDServicio_Det = pd.IDServicio_Det_V_Cot)),0) As varchar(4))+')'
 End As DescPago,'R' as RegVal
 from PRESUPUESTO_SOBRE_DET pd Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor
 Left Join COTIPAX cp On pd.IDPax=cp.IDPax
 Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
 Left Join MAMONEDAS mo On pd.IDMoneda=mo.IDMoneda
 Where pd.NuPreSob=@NuPreSob and pd.IDOperacion_Det is Null
 And @NuPreSob<>0
Order By Dia


