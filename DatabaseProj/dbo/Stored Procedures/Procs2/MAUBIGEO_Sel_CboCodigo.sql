﻿CREATE Procedure [dbo].[MAUBIGEO_Sel_CboCodigo]  -- 'Ciudad','',1
 @TipoUbig varchar(20),      
 @IDPais char(6),      
 @bTodos bit      
As      
      
 Set NoCount On      
       
 SELECT * FROM      
 (      
 Select '' as IDUbigeo, '' as Codigo,'-------' as Descripcion, 0 as Ord      
 Union      
 Select IDubigeo,Codigo,Descripcion,1 From MAUBIGEO      
 Where TipoUbig=@TipoUbig Or LTRIM(RTRIM(@TipoUbig))='' And      
 (IDPais=@IDPais Or LTRIM(RTRIM(@IDPais))='')      
 ) AS X      
 Where (@bTodos=1 Or Ord<>0)      
 Order by Ord,3
