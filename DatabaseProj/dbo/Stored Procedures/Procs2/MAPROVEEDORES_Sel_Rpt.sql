﻿--JRF-20130917-Agregar la columna forma de Pago.
--JHD-20150525-Filtro por pais y ciudad
CREATE Procedure [dbo].[MAPROVEEDORES_Sel_Rpt]   
 @NumIdentidad varchar(15),  
 @RazonSocial varchar(100),  
 @IDTipoProv char(3),  
 @RazonComercial varchar(100)  ,
 @IDCiudad char(6)='',
 @CoPais char(6)=''
AS  
 Set NoCount On  
   
 Select Distinct c.NombreCorto as Nombre, tp.Descripcion as TipoProv,  
 u.Descripcion AS Ciudad,  
 Case ISNULL(IDTipoOper,'') When 'I' then 'INTERNO' When 'E' then 'EXTERNO'  
  when '' then 'NO APLICA' End as IDTipoOper  ,
  f.Descripcion as FormaDePago
 From MAPROVEEDORES c   
 Left Join MATIPOPROVEEDOR tp On c.IDTipoProv=tp.IDTipoProv  
 Left Join MAUBIGEO u On c.IDCiudad=u.IDubigeo  
 Left Join MAFORMASPAGO f On c.IDFormaPago= f.IdFor
 Where   
 (c.NumIdentidad LIKE '%'+@NumIdentidad+'%' Or ltrim(rtrim(@NumIdentidad))='') And  
 (c.RazonSocial Like '%'+@RazonSocial+'%' Or ltrim(rtrim(@RazonSocial))='') And  
 (c.IDTipoProv=@IDTipoProv Or ltrim(rtrim(@IDTipoProv))='') And   
 (c.NombreCorto Like '%'+@RazonComercial+'%' Or ltrim(rtrim(@RazonComercial))='')  
 and
 (c.IDCiudad=@IDCiudad Or LTRIM(RTRIM(@IDCiudad))='') And 
    (u.IDPais=@CoPais Or LTRIM(RTRIM(@CoPais))='') 
 And c.Activo='A'  
 Order by Nombre  

