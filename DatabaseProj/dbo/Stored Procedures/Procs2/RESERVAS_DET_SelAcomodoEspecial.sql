﻿Create Procedure dbo.RESERVAS_DET_SelAcomodoEspecial
@IDReserva_Det int
As
Set NoCount On
select p.Orden as NroOrd,p.Apellidos,p.Nombres,p.Titulo,SUBSTRING(id.Descripcion,1,3) as DescIdentidad,
Isnull(p.NumIdentidad,'') as NumIdentidad,Isnull(ub.DC,'') as DescNacionalidad,
Isnull(CONVERT(Char(10),p.FecNacimiento,103),'') as FecNacimiento,ISNULL(p.Peso,0.00) as Peso,p.Residente,ISNULL(p.ObservEspecial,'') as ObservEspecial,
p.IDIdentidad,p.IDNacionalidad,p.IDPax, ch.QtCapacidad,
dbo.FnMailAcomodosReservas(ch.QtCapacidad,Case When CHARINDEX('(MAT)',ch.NoCapacidad) > 0 then 1 Else 0 End) +' '+ CAST(cad.IDHabit as varchar(3))
as ACC
from COTIDET_ACOMODOESPECIAL_DISTRIBUC cad Left Join CAPACIDAD_HABITAC ch On cad.CoCapacidad = ch.CoCapacidad
Left Join COTIPAX p On cad.IDPax = p.IDPax Left Join MATIPOIDENT id On p.IDIdentidad = id.IDIdentidad
Left Join MAUBIGEO ub On p.IDNacionalidad = ub.IDubigeo Left Join COTIDET cd On cad.IDDet = cd.IDDET
Left Join RESERVAS_DET rd On cd.IDDET = rd.IDDet
where rd.IDReserva_Det = @IDReserva_Det
