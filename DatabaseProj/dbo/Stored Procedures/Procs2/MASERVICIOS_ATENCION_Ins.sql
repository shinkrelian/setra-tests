﻿Create Procedure dbo.MASERVICIOS_ATENCION_Ins
@IDAtencion tinyint,
@IDservicio Char(8) ,
@Dia1 char(1) ,
@Dia2 char(1) ,
@Desde1 varchar(22) ,
@Hasta1 varchar(22) ,
@Desde2 varchar(22) ,
@Hasta2 varchar(22) ,
@UserMod Char(4) 
As
	Set NoCount On
	--Declare @IDAtencion tinyint
	--set @IDAtencion = (select ISNULL(MAX(IDAtencion),0)+1 from MASERVICIOS_ATENCION Where IDservicio = @IDservicio)
	Insert Into MASERVICIOS_ATENCION 
				(IDAtencion,
				 IDservicio,
				 Dia1,
				 Dia2,
				 Desde1,
				 Hasta1,
				 Desde2,
				 Hasta2,
				 UserMod)
				 Values
				 (@IDAtencion,
				  @IDservicio,
				  @Dia1 ,
				  @Dia2 ,
				  @Desde1 ,
				  @Hasta1 ,
				  @Desde2 ,
				  @Hasta2 ,
				  @UserMod)
