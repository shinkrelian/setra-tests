﻿Create Procedure [dbo].[MATIPOCAMBIO_Upd]
	@Fecha	smalldatetime,
	@ValCompra	decimal(6,3),
	@ValVenta	decimal(6,3),
	@UserMod char(4)	

As

	Set NoCount On

	Update MATIPOCAMBIO
	Set ValCompra=@ValCompra,
	ValVenta=@ValVenta,
	UserMod=@UserMod,
	FecMod=GETDATE() 
	Where Fecha=@Fecha
