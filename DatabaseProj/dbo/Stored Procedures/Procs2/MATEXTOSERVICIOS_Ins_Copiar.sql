﻿CREATE Procedure [dbo].[MATEXTOSERVICIOS_Ins_Copiar]
	@IDServicio	char(8),
	@UserMod	char(4),
	@IDServicioNue	char(8)
As
	Set NoCount On
	
	Insert Into MATEXTOSERVICIOS (IDServicio, IDIdioma, 
		Titulo,Texto, UserMod)
	Select @IDServicioNue, IDIdioma, Titulo,Texto, @UserMod
		From MATEXTOSERVICIOS
	Where IDServicio=@IDServicio
