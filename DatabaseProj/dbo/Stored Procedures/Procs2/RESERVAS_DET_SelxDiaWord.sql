﻿--JFR-20130823-Correcion Muestra EsMatrimonial.  
--JRF-20151130-Anulado=0
--JHD-20160113-Servicio= case when ((select IDTipoProv from MAPROVEEDORES where IDProveedor=r.IDProveedor) ...
--HLF-20160229-distinct Left Join COTIDET cd On (rd.IDDet=cd.IDDET or cd.IDDetRel=rd.IDDet)	
--And (CONVERT(char(10),cd.Dia,103)=CONVERT(char(10),@Dia,103) or CONVERT(char(10),rd.Dia,103)=CONVERT(char(10),@Dia,103))
--isnull(sd.Descripcion,''),'')))
CREATE Procedure RESERVAS_DET_SelxDiaWord          
@IDReserva int,          
@Dia smalldatetime          
As          
 Set NoCount On             
 select  distinct
	--Servicio, 
	Servicio= case when (p.IDTipoProv ='003' and sd.ConAlojamiento=1) then
	  rtrim(ltrim(replace(rd.Servicio,
	  --isnull((select Descripcion from MASERVICIOS where IDServicio=sd.IDServicio),''),'')))
	  isnull(sd.Descripcion,''),'')))
else rd.Servicio end, 
  rd.Cantidad,rd.CapacidadHab,    
    Case When isnull(rd.EsMatrimonial,0) > 0 Then 1 else 0 End As EsMatrimonial,  --rd.EsMatrimonial,    
    C.Simple ,                        
    C.Twin,                        
    C.Matrimonial,                        
    C.Triple  
 from RESERVAS_DET rd Left Join RESERVAS r On rd.IDReserva = r.IDReserva           
 Left Join COTICAB c On r.IDCab = c.IDCAB          
 Left Join MASERVICIOS_DET sd On rd.IDServicio_Det = sd.IDServicio_Det    
 Left Join MAPROVEEDORES p on p.IDProveedor=r.IDProveedor    
 Left Join COTIDET cd On (rd.IDDet=cd.IDDET or cd.IDDetRel=rd.IDDet)
 where rd.Anulado=0 And IsNull(rd.FlServicioNoShow,0)=0 and rd.IDReserva = @IDReserva 
	--And CONVERT(char(10),rd.Dia,103)=CONVERT(char(10),@Dia,103)
	And (CONVERT(char(10),cd.Dia,103)=CONVERT(char(10),@Dia,103) or CONVERT(char(10),rd.Dia,103)=CONVERT(char(10),@Dia,103))
	--) As X         
 /*And sd.PlanAlimenticio = 0*/
  and ((p.IDTipoProv<>'001' and sd.ConAlojamiento=1) or (p.IDTipoProv='001' and sd.PlanAlimenticio=0)) and ((select IncGuia from COTIDET     
 where IDDet = rd.IDDet) = 0   or Not exists(select IncGuia from COTIDET where IDDet = rd.IDDet))  
 Order By CapacidadHab,Cantidad        

