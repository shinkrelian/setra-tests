﻿
Create Procedure dbo.MAVEHICULOS_Sel_List
@Descripcion varchar(50)
as
	Set NoCount On
	select NuVehiculo,NoVehiculoCitado,QtCapacidad
	from MAVEHICULOS
	where (ltrim(rtrim(@Descripcion))='' Or NoVehiculoCitado Like '%'+@Descripcion+'%')
