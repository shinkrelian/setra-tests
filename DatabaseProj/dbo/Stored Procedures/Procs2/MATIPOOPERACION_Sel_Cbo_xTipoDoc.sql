﻿create Procedure [dbo].[MATIPOOPERACION_Sel_Cbo_xTipoDoc]
@CoTipoDoc char(3)=''
As
	Set NoCount On
	
	
	Select distinct IdToc, Descripcion
	From MATIPOOPERACION	
	INNER JOIN dbo.CTASCONTAB_OPERCONTAB_TIPODOC ON
	MATIPOOPERACION.IdToc=CTASCONTAB_OPERCONTAB_TIPODOC.CoTipoOC
	WHERE (CTASCONTAB_OPERCONTAB_TIPODOC.CoTipoDoc=@CoTipoDoc OR @CoTipoDoc='')
	Order by Descripcion
