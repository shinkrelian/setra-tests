﻿CREATE Procedure [dbo].[MASERIES_Ins]	
	@IDSerie char(3),
    @Descripcion varchar(60),
    @UserMod char(4)
As
	Set NoCount On
	
	INSERT INTO MASERIES
           ([IDSerie]
           ,[Descripcion]
           ,[UserMod]
           )
     VALUES
           (@IDSerie,
           @Descripcion ,
           @UserMod)
