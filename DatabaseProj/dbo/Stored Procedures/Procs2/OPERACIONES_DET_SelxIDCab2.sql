﻿
--HLF-20130117-Cambiando tabla para el campo IDTipoOC
CREATE Procedure dbo.OPERACIONES_DET_SelxIDCab2
	@IDCab	int
As
	Set Nocount On
	
	Select od.IDOperacion_Det, od.Servicio as DescServicioDet, od.NetoGen, od.IgvGen, od.idTipoOC
	From OPERACIONES_DET od 
	Where od.IDOperacion In (Select IDOperacion From OPERACIONES Where IDCab=@IDCab)

