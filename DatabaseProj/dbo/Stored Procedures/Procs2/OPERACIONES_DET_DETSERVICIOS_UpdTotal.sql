﻿--JRF-20130226- Nueva columna IDMoneda,TipoCambio                
--JRF-20130223- Nuevos Parametros y Campos (x Cambio de Moneda)                
--JRF-20130603-Nuevos Campos [NetoProgram,IgvProgram,TotalProgram]              
--HLF-20130717-Comentando Case When para permitir grabar 0 en Montos Prog            
--JRF-20130809-Grabar los campos calculados.          
--HLF-20130917-Agregando parametro @IngManual        
--JRF-20130926-Agregando el parametro @IDEstadoSolicitud      
--JRF-20140621-AGREGAR EL CAMPO @FlMontosSincerados    
--JRF-20140623-Agregando el campo CoEstadoSincerado  
--JRF-20150223-Considerar [FlServicioNoShow]
--HLF-20150624-Agregando FlServicioNoCobrado, SSTotalProgramAntNoCobrado
--HLF-20150625-Agregando QtPax
--And FlServicioNoShow = @FlServicioNoShowWhere
--HLF-20150805-numeric(13,4)
CREATE Procedure [dbo].[OPERACIONES_DET_DETSERVICIOS_UpdTotal]                  
 @IDOperacion_Det int,                  
 @IDServicio_Det int,                  
 @CostoReal numeric(13,4),                
 @CostoLiberado numeric(13,2),                
 @CostoRealImpto numeric(13,2),                
 @CostoLiberadoImpto numeric(13,2),                
 @Margen numeric(13,4),                
 @MargenAplicado numeric(13,2),                
 @MargenLiberado numeric(13,2),                
 @MargenImpto numeric(13,2),                
 @MargenLiberadoImpto numeric(13,2),                
 @Total numeric(13,4),                
 @Total_PrgEditado bit,         
 @IDEstadoSolicitud char(2),             
                 
 @IDMoneda char(3),                
 @TipoCambio numeric(13,2),                
               
 @NetoProgram numeric(12,4),               
 @IgvProgram numeric(12,4),              
 @TotalProgram numeric(12,4),                     
          
 @NetoCotizado numeric(12,4),          
 @IgvCotizado numeric(12,4),          
 @TotalCotizado numeric(12,4),          
 @IngManual bit,        
 @FlMontosSincerados bit,    
 @CoEstadoSincerado  char(2),  
 @Total_Prg numeric(8,2),                  
 @ObservEdicion varchar(250),         
 @FlServicioNoShow bit,          
 @FlServicioNoCobrado bit=0, 
 @SSTotalProgramAntNoCobrado numeric(12,4)=0,
 @QtPax smallint,
 @FlServicioNoShowWhere bit=0,          
 @UserMod char(4)                  
As                  
 Set NoCount On                  
                   
 Update OPERACIONES_DET_DETSERVICIOS Set                  
                 
 CostoReal = @CostoReal,                
 CostoLiberado = @CostoLiberado,                
 CostoRealImpto = @CostoRealImpto ,                
 CostoLiberadoImpto = @CostoLiberadoImpto ,                
 Margen = @Margen ,                
 MargenAplicado = @MargenAplicado ,                
 MargenLiberado = @MargenLiberado,                
 MargenImpto = @MargenImpto,                
 MargenLiberadoImpto = @MargenLiberadoImpto,                
 Total_PrgEditado = @Total_PrgEditado,                
 TipoCambio = @TipoCambio,                
 IDMoneda = @IDMoneda,                
 Total = @Total,                
 [NetoProgram] = @NetoProgram,--Case When @NetoProgram = 0 then Null Else @NetoProgram End,              
 [IgvProgram] = @IgvProgram,--Case When @IgvProgram = 0 Then Null Else @IgvProgram End,              
 [TotalProgram] = @TotalProgram,--Case When @TotalProgram = 0 then Null Else @TotalProgram End,              
 Total_Prg=@Total_Prg,--Case When @Total_Prg=0 Then Null Else @Total_Prg End,                  
 ObservEdicion=Case When ltrim(rtrim(@ObservEdicion))='' Then Null Else @ObservEdicion End,                   
 NetoCotizado = @NetoCotizado,        
 IDEstadoSolicitud = @IDEstadoSolicitud,        
 IgvCotizado= @IgvCotizado,          
 TotalCotizado = @TotalCotizado,          
 IngManual=@IngManual,        
 FlMontosSincerados = @FlMontosSincerados,  
 CoEstadoSincerado  = @CoEstadoSincerado ,   
 FlServicioNoShow = @FlServicioNoShow,
 FlServicioNoCobrado=@FlServicioNoCobrado, 
 SSTotalProgramAntNoCobrado=@SSTotalProgramAntNoCobrado ,
 QtPax=@QtPax,
 UserMod=@UserMod,                   
 FecMod=GETDATE()                  
 Where IDOperacion_Det=@IDOperacion_Det And IDServicio_Det=@IDServicio_Det                  
	And FlServicioNoShow = @FlServicioNoShowWhere 


