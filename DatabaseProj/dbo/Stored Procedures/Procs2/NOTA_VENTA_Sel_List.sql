﻿--JRF-20130704-Agregar el campo IDEstado      
--JRF-20130709-Igualando campos del debitmemo  
--JRF-20130811-Se debe agregar la Columna del País.
--JRF-20140311-Agregar nueva columna de CoTipoVenta
CREATE Procedure dbo.NOTA_VENTA_Sel_List        
@DescCliente varchar(100),        
@Numero varchar(10),        
@Referencia varchar(100),        
@DescEjecutivo varchar(150),        
@IDEstado char(2),        
@IDCab int        
As        
 Set NoCount On        
 select '' as  DescTipo,nv.IDNotaVenta,        
  (select CONVERT(char(10),Min(Dia),103) from COTIDET where IDCAB = c.IDCAB) As FechaIn,          
  (select CONVERT(Char(10),Max(Dia),103) from COTIDET where IDCAB = c.IDCAB) As FechaOut,        
   Case c.CoTipoVenta
	when 'RC' Then 'RECEPTIVO'
	when 'CU' Then 'COUNTER'
	when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'
	when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'
  End as DescTipoVenta,c.IDFile,'' As Correlativo,cl.RazonComercial As DescrCliente,     
  Ub.Descripcion as DescPais,     
  c.Titulo As Referencia,        
  u.Nombre As Ejecutivo,  
  nv.Total,   
  0 as Saldo,             
  nv.IDEstado,      
  0 as FlImprimioDebit,      
  nv.IDCab        
 from NOTA_VENTA nv Left Join COTICAB c On nv.IDCab = c.IDCAB          
  Left Join MACLIENTES cl On c.IDCliente = cl.IDCliente          
  Left Join MAUSUARIOS u On c.IDUsuario = u.IDUsuario          
  Left Join MAUBIGEO ub On cl.IDCiudad = ub.IDubigeo  
  Where (LTRIM(rtrim(@DescCliente))='' Or cl.RazonComercial like '%' + @DescCliente + '%')          
  And (ltrim(rtrim(@Numero))='' Or c.IDFile like '%' + @Numero + '%')          
  And (ltrim(rtrim(@Referencia))='' Or c.Titulo like '%' + @Referencia + '%')           
  And (ltrim(rtrim(@DescEjecutivo))='' Or u.Nombre like '%' + @DescEjecutivo + '%')          
  And (ltrim(rtrim(@IDEstado))='' Or nv.IDEstado = @IDEstado)        
  And (@IDCab = 0 Or c.IDCAB =@IDCab)        
