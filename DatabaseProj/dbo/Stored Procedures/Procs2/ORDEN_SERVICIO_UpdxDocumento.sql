﻿--JHD-20141205-Se agrego el campo UserNuevo y se actualizará cuando tenga el estado Aceptado y tenga diferencia aceptada  
--HLF-20150108-Case When @FlDesdeOtraForEgreso=0 Then...
--JHD-20151118-@CoMoneda_FEgreso char(3)=''
--JHD-20151118-Actualizar en ORDEN_SERVICIO_DET_MONEDA
CREATE Procedure dbo.ORDEN_SERVICIO_UpdxDocumento  
@NuOrdenServicio int,  
@CoEstado char(2),  
@SsSaldo numeric(8,2),  
@SsDifAceptada numeric(8,2),  
@TxObsDocumento varchar(Max),  
@FlDesdeOtraForEgreso bit,
@UserMod char(4),
@CoMoneda_FEgreso char(3)='' 
As  
BEGIN  
Set NoCount On  
Update ORDEN_SERVICIO  
 Set CoEstado=@CoEstado,  
  SsSaldo= Case When @SsSaldo=0 then Null Else @SsSaldo End,  
  SsDifAceptada= Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,  
  --TxObsDocumento=Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End,  
  TxObsDocumento=
	Case When @FlDesdeOtraForEgreso=0 Then
		Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
	Else
		TxObsDocumento
	End,
  UserMod=@UserMod,  
  FecMod=GETDATE(),  
  --  
  UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end  
Where NuOrden_Servicio= @NuOrdenServicio 


if rtrim(ltrim(@CoMoneda_FEgreso))<>''
	BEGIN
			Update ORDEN_SERVICIO_DET_MONEDA  
			 Set CoEstado=@CoEstado,  
			  SsSaldo= Case When @SsSaldo=0 then Null Else @SsSaldo End,  
			  SsDifAceptada= Case When @SsDifAceptada=0 then Null Else @SsDifAceptada End,  
			  --TxObsDocumento=Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
			  TxObsDocumento= 
				Case When @FlDesdeOtraForEgreso=0 Then
					Case When LTRIM(rtrim(@TxObsDocumento))='' Then Null Else @TxObsDocumento End
				Else
					TxObsDocumento
				End,
			  UserMod=@UserMod,  
			  FecMod=GETDATE()--,  
			 -- UserNuevo=case when (@CoEstado='AC' AND @SsDifAceptada>0) THEN @UserMod else UserNuevo end  
			Where NuOrden_Servicio= @NuOrdenServicio  AND COMONEDA=@CoMoneda_FEgreso
	END
END;  
