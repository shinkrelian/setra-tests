﻿CREATE Procedure [dbo].[MASEGUIMCLIENTE_Sel_List]
	@IDCliente	char(6)
As
	Set NoCount On

	Select s.Fecha,c.Nombres+' '+c.Apellidos as Nombre,s.Notas,s.IDSeguim
	From MASEGUIMCLIENTE s Left Join MACONTACTOSCLIENTE c On s.IDCliente=c.IDCliente 
	And s.IDContacto=c.IDContacto
	Where s.IDCliente=@IDCliente
	Order by s.Fecha Desc
