﻿
Create Procedure dbo.MAVEHICULOSPROGRAMACION_Ins
@NoUnidadProg varchar(50),
@QtCapacidadPax tinyint,
@UserMod char(4)
as
	Set NoCount On
	Declare @NuVehiculoProg smallint
	Execute dbo.Correlativo_SelOutput 'MAVEHICULOSPROGRAMACION',1,10,@NuVehiculoProg output
	
	Insert into MAVEHICULOSPROGRAMACION
	values(@NuVehiculoProg,@NoUnidadProg,@QtCapacidadPax,@UserMod,getdate())
