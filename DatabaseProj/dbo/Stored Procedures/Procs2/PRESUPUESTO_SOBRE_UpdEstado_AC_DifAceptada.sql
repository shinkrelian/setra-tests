﻿CREATE Procedure dbo.PRESUPUESTO_SOBRE_UpdEstado_AC_DifAceptada
 @NuPreSob int,      
 @SsTotal numeric(9,2),      
 @UserMod char(4)      
As      
 Set NoCount On      
 Update PRESUPUESTO_SOBRE      
  Set SsDifAceptada=SsSaldo-@SsTotal,      
   UserMod=@UserMod,      
   FecMod=GETDATE()      
 where NuPreSob=@NuPreSob
       
 Update PRESUPUESTO_SOBRE set CoEstadoFormaEgreso='AC', SsSaldo=0 where NuPreSob=@NuPreSob


