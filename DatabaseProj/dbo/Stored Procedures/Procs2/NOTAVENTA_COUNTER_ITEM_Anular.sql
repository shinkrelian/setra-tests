﻿
----------------------------------------------------------------------------------------

CREATE procedure NOTAVENTA_COUNTER_ITEM_Anular
		   @NuNtaVta char(6),
		   @NuItem tinyint,
		   @FlActivo bit,
           @UserMod char(4)=''
as
BEGIN
Set NoCount On
	
update [NOTAVENTA_COUNTER_ITEM]
SET 
           FlActivo=@FlActivo
           ,UserMod=@UserMod
           ,FecMod=GETDATE()
WHERE
			NuNtaVta=@NuNtaVta AND
			NuItem=@NuItem   
            
END;
