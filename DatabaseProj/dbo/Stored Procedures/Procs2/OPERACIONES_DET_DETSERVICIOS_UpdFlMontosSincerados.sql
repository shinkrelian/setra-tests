﻿Create Procedure dbo.OPERACIONES_DET_DETSERVICIOS_UpdFlMontosSincerados
@IDOperacion_Det int,
@IDServicio_Det int
as
Set NoCount On
Update OPERACIONES_DET_DETSERVICIOS
	set FlMontosSincerados = 0
where IDOperacion_Det = @IDOperacion_Det and IDServicio_Det = @IDServicio_Det
