﻿
--HLF-20160107-... as SaldoIni
 CREATE Procedure dbo.PRESUPUESTO_SOBRE_SelxTicket
	
 As
	Set Nocount on
	
	Select NuPreSob as Codigo, NuPreSob as IDVoucher, c.Titulo, p.IDCab, c.IDFile, 
	'SOL' as IDMoneda, 'S/.' as Simbolo, 
	IsNull((SELECT SUM(PD2.SsTotal) FROM PRESUPUESTO_SOBRE_DET PD2 WHERE PD2.NuPreSob=p.NuPreSob 
		--And IsNull(IDMoneda,'')='SOL' and PD2.CoPago = 'TCK'
		),0) As SaldoIni,
	p.SsSaldo as Total, 0 as IGV
	From PRESUPUESTO_SOBRE p inner join COTICAB c on p.IDCab=c.IDCAB
	Where  exists(Select NuPreSob From PRESUPUESTO_SOBRE_DET where NuPreSob=p.NuPreSob and CoPago='TCK')
	And p.CoEstadoFormaEgreso='PD' And p.CoEstado='EN'

