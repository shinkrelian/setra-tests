﻿--JRF-20150223-Considerar Nuevo Campod [FlServicioNoShow]
--JRF-20150701-Agregar la columna QtPax
--JRF-20160308-Agregar {IDProveedor_Prg}
CREATE Procedure dbo.OPERACIONES_DET_DETSERVICIOS_InsxAcomodoTMP
 @IDOperacion_Det int,      
 @IDOperacion_DetTMP int,  
 @UserMod char(4)      
As      
 Set Nocount On      
       
 Insert Into OPERACIONES_DET_DETSERVICIOS      
 (IDOperacion_Det,IDServicio_Det, IDServicio,        
 IDServicio_Det_V_Cot,          
 CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,          
 SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,          
 MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,          
 STMargenImpto,TotImpto,        
 TipoCambio,IDMoneda,FlServicioNoShow,  QtPax,IDProveedor_Prg, IDEstadoSolicitud, IDVehiculo_Prg,
 UserMod)      
 Select @IDOperacion_Det,IDServicio_Det, IDServicio,        
 IDServicio_Det_V_Cot,          
 CostoReal,CostoLiberado,Margen,MargenAplicado,MargenLiberado,Total,          
 SSCR,SSCL,SSMargen,SSMargenLiberado,SSTotal,STCR,STMargen,STTotal,CostoRealImpto,CostoLiberadoImpto,          
 MargenImpto,MargenLiberadoImpto,SSCRImpto,SSCLImpto,SSMargenImpto,SSMargenLiberadoImpto,STCRImpto,          
 STMargenImpto,TotImpto,        
 TipoCambio,IDMoneda,    FlServicioNoShow,  (select NroPax+ISNULL(NroLiberados,0) from OPERACIONES_DET where IDOperacion_Det=@IDOperacion_Det)  , IDProveedor_Prg,
 IDEstadoSolicitud,IDVehiculo_Prg ,@UserMod      
 From OPERACIONES_DET_DETSERVICIOS_TMP       
 Where IDOperacion_Det In      
  (Select IDOperacion_Det From OPERACIONES_DET_TMP odt       
  Left Join RESERVAS_DET_TMP rdd On odt.IDReserva_Det=rdd.IDReserva_Det      
  Where odt.IDOperacion_Det=@IDOperacion_DetTMP
  )
