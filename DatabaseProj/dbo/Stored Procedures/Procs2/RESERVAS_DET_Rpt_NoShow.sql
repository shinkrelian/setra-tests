﻿--JRF-20150416-Considerar la información de los sub-servicios de operaciones
--JRF-20150416-Solo considerar la info. de Operaciones
--JRF-20150421-Considerar todos los estados
CREATE PROCEDURE RESERVAS_DET_Rpt_NoShow  
@fecha1 smalldatetime='01/01/1900',  
@fecha2 smalldatetime='01/01/1900',  
@IDCAB int=0  
AS  
SELECT COTICAB.IDFile,  
COTICAB.Titulo,  
Cliente=isnull(MACLIENTES.RazonComercial,MACLIENTES.RazonSocial),  
RESERVAS_DET.NroPax,  
NroLiberados=ISNULL(RESERVAS_DET.NroLiberados,0),  
MAPROVEEDORES.NombreCorto,  
MASERVICIOS.IDServicio,  
Servicio=MASERVICIOS.Descripcion,  
Detalle=MASERVICIOS_DET.Descripcion + ' (NoShow)',  
RESERVAS_DET.IDMoneda,  
RESERVAS_DET.NetoGen,  
RESERVAS_DET.IgvGen,  
RESERVAS_DET.TotalGen  
FROM RESERVAS INNER JOIN  
      RESERVAS_DET ON RESERVAS.IDReserva = RESERVAS_DET.IDReserva INNER JOIN  
      COTICAB ON RESERVAS.IDCab = COTICAB.IDCAB INNER JOIN  
      MASERVICIOS ON RESERVAS_DET.IDServicio = MASERVICIOS.IDServicio INNER JOIN  
      MAPROVEEDORES ON MASERVICIOS.IDProveedor = MAPROVEEDORES.IDProveedor INNER JOIN  
      MATIPOPROVEEDOR ON MATIPOPROVEEDOR.IDTipoProv=MAPROVEEDORES.IDTipoProv  
      inner join MASERVICIOS_DET on RESERVAS_DET.IDServicio_Det = MASERVICIOS_DET.IDServicio_Det  
      inner join MACLIENTES on MACLIENTES.IDCliente=COTICAB.IDCliente  
WHERE ((COTICAB.FecInicio>@fecha1 and COTICAB.FecInicio<@fecha2+1)  
  Or (@fecha1 = '01/01/1900' And @fecha2 = '01/01/1900'))  
and (COTICAB.IDCAB=@IDCAB or @IDCAB=0)  
and COTICAB.IDFile is not null  
and COTICAB.Estado<>'X'  
and RESERVAS.Anulado=0  
and RESERVAS_DET.Anulado=0  
and RESERVAS_DET.FlServicioNoShow=1  
Union
select c.IDFile,
	   c.Titulo,
	   IsNull(Cl.RazonComercial,Cl.RazonSocial) as Cliente,
	   (select COUNT(*) from COTITRANSP_PAX ctp Where ctp.IDTransporte=cv.ID) as NroPax,
	   0 as NroLiberados,
	   p.NombreCorto,
	   Ltrim(Rtrim(IsNull(sd.IDServicio,''))) as IDServicio,
	   IsNull(Case When cv.TipoTransporte='V' Then 'Ticket '+ Ltrim(Rtrim(cv.Ruta))+' - '+Vuelo 
	   Else cv.Servicio End,cd.Servicio) + ' (NoShow)' As Servicio,
	   IsNull(sd.Descripcion,'') as Detalle,
	   'USD' as IDMoneda,
	   IsNull(cv.CostoOperador,0)-IsNull((select SUM(IsNull(cp3.SsReembolso,0)) from COTITRANSP_PAX cp3 Where cp3.IDTransporte=cv.ID),0) as NetoGen,
	   0 as IgvGen,
	   IsNull(cv.CostoOperador,0)-IsNull((select SUM(IsNull(cp3.SsReembolso,0)) from COTITRANSP_PAX cp3 Where cp3.IDTransporte=cv.ID),0) as TotalGen
from COTIVUELOS cv Left Join COTICAB c On cv.IDCAB=c.IDCAB
Left Join MAPROVEEDORES p On cv.IdLineaA=p.IDProveedor
Left Join MACLIENTES Cl On c.IDCliente=Cl.IDCliente
Left Join COTIDET cd On cv.IdDet=cd.IDDET
Left Join MASERVICIOS_DET sd On cd.IDServicio_Det=sd.IDServicio_Det
Where Exists(select IDPax from COTIPAX cp Where cp.IDCab=c.IDCAB and cp.FlNoShow=1)
And (c.FecInicio between @fecha1 and @fecha2
	 Or (CONVERT(char(10),@fecha1,103)='01/01/1900' And CONVERT(char(10),@fecha2,103)='01/01/1900'))
And (cv.IDCAB=@IDCAB Or @IDCAB=0) --and c.Estado <> 'X'
Union
Select X.IDFile,X.Titulo,X.Cliente,X.NroPax,X.NroLiberados,X.NombreCorto ,
	   X.IDServicio,X.Servicio,X.Detalle,X.IDMoneda,X.NetoProgramado,x.IgvProgramado,
	   x.NetoProgramado+x.IgvProgramado as TotalProgramado
from (
Select c.IDFile,c.Titulo,isnull(cl.RazonComercial,cl.RazonSocial) as Cliente,
       od.NroPax,IsNull(od.NroLiberados,0) as NroLiberados,
	   p.NombreCorto,ods.IDServicio,
	   s.Descripcion as Servicio,sd.Descripcion + ' (NoShow)' as Detalle,ods.IDMoneda,
	    Round(IsNuLL(ods.NetoProgram, dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0))),2) As NetoProgramado ,
		Round(IsNuLL(ods.IgvProgram,
		IsNuLL(ods.NetoProgram, dbo.FnNetoMaServicio_Det(ods.IDServicio_Det,od.NroPax+isnull(od.NroLiberados,0))) * 
		Case When sd.Afecto = 1 Then (select NuIGV from PARAMETRO)/100 else 0 End),2) As IgvProgramado
from OPERACIONES_DET_DETSERVICIOS ods Left Join OPERACIONES_DET od On ods.IDOperacion_Det=od.IDOperacion_Det
Inner Join RESERVAS_DET rd On od.IDReserva_Det=rd.IDReserva_Det and rd.FlServicioNoShow=0
Left Join OPERACIONES o On od.IDOperacion= o.IDOperacion
Left Join COTICAB c On o.IDCab = c.IDCAB
Left Join MACLIENTES cl On c.IDCliente=cl.IDCliente
Left Join MASERVICIOS_DET sd On IsNull(ods.IDServicio_Det_V_Cot,ods.IDServicio_Det) = sd.IDServicio_Det
Left Join MASERVICIOS s On ods.IDServicio = s.IDServicio
Left Join MAPROVEEDORES p On s.IDProveedor= p.IDProveedor
Where (o.IDCAB=@IDCAB or @IDCAB=0)  and ods.FlServicioNoShow=1 and ods.Activo=1
And (c.FecInicio between @fecha1 and @fecha2 Or Convert(char(10),@fecha1,103)='01/01/1900')
) as X
