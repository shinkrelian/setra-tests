﻿--JRF-20131209-Aumentando el valor de los decimales a (numeric(10, 4))
--JHD-20150602-Aumentando el valor de los decimales a (numeric(15, 4))
CREATE Procedure [dbo].[MASERVICIOS_DET_COSTOS_Upd]  
 @Correlativo tinyint,  
    @IDServicio_Det int,  
    @PaxDesde smallint,  
    @PaxHasta smallint,  
    @Monto numeric(15,4),  
    @UserMod char(4)  
As  
 Set NoCount On  
   
 UPDATE MASERVICIOS_DET_COSTOS  
           Set PaxDesde=@PaxDesde   
           ,PaxHasta=@PaxHasta  
           ,Monto=@Monto  
           ,UserMod=@UserMod  
             
     WHERE  
           Correlativo=@Correlativo And  
           (IDServicio_Det=@IDServicio_Det Or   
           IDServicio_Det In   
           (Select IDServicio_Det From MASERVICIOS_DET  
            Where IDServicio_Det_V=@IDServicio_Det ))  
