﻿
  
--HLF-20130513-Cambio radical campo IDReserva_Det reemplazandolo por IDDet  
--HLF-20130618-Agregando parametro @NuIngreso
CREATE Procedure [dbo].[RESERVAS_DET_ESTADOHABITAC_Ins]  
	@IDDet int,   
	@NuIngreso tinyint,
	@Simple tinyint,  
	@Twin tinyint,  
	@Matrimonial tinyint ,  
	@Triple tinyint,   
	@UserMod char(4)  
As  
  
	Set Nocount On  

	Insert Into RESERVAS_DET_ESTADOHABITAC  
	(IDDet,NuIngreso,Simple,Twin,Matrimonial,Triple,UserMod)  
	Values(@IDDet,@NuIngreso,  
	Case When @simple=0 Then Null Else @simple End,  
	Case When @Twin=0 Then Null Else @Twin End,  
	Case When @Matrimonial=0 Then Null Else @Matrimonial End,  
	Case When @Triple=0 Then Null Else @Triple End,  
	@UserMod)  

