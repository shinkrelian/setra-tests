﻿Create Procedure dbo.MASERVICIOS_DET_Sel_ListxIDServicioxAnio_Prg
@IDServicio char(8),
@Anio char(4)
As
Set NoCount On
select IDServicio_Det,
	   Case when (Tipo + '('+cast(DetaTipo as varchar(Max))+')') is null then s.Descripcion else 
		(Tipo + '('+cast(DetaTipo as varchar(Max))+')') End as Servicio
 from MASERVICIOS_DET sd Left Join MASERVICIOS s On sd.IDServicio = s.IDServicio
where Anio =@Anio and sd.IDServicio = @IDServicio
