﻿--HLF-20140813-and pd.CoPrvPrg=@CoPrvPrg      
--JRF-20140911-Agregar el Guia a Nivel de detalle    
--JRF-20150308-Considerar el activo de Operaciones  
--JRF-20150410-Considerar el filtro por Pax
CREATE Procedure dbo.PRESUPUESTO_SOBRE_DET_Sel_List          
@NuPreSob int,      
@CoPrvPrg char(6),
@IDPax int         
As          
Set Nocount On          
    
Select pd.NuDetPSo, pd.FeDetPSo, pd.TxServicio, IsNull(pd.CoPrvPrg,'') as CoPrvPrg, 
IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as Guia,        
pd.QtPax, pd.SsPreUni, pd.SsTotal, pd.SsTotSus, pd.SsTotDev,          
es.NoTipPSo as DescEstado--,odd.Activo  
From PRESUPUESTO_SOBRE_DET pd           
Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor    
Left Join OPERACIONES_DET_DETSERVICIOS odd On pd.IDOperacion_Det=odd.IDOperacion_Det 
and pd.IDServicio_Det=IsNull(odd.IDServicio_Det_V_Cot,odd.IDServicio_Det)
Left Join COTIPAX cp On pd.IDPax=cp.IDPax
--Inner Join MASERVICIOS_DET sd On pd.IDServicio_Det=sd.IDServicio_Det
where NuPreSob = @NuPreSob and IsNull(pd.CoPrvPrg,'')=Ltrim(rtrim(@CoPrvPrg)) and odd.Activo=1      
And IsNull(pd.IDPax,0)=@IDPax And IsNull(pd.IDOperacion_Det,0)>0
Union
Select pd.NuDetPSo, pd.FeDetPSo, pd.TxServicio, IsNull(pd.CoPrvPrg,'') as CoPrvPrg, 
IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as Guia,        
pd.QtPax, pd.SsPreUni, pd.SsTotal, pd.SsTotSus, pd.SsTotDev,          
es.NoTipPSo as DescEstado
from PRESUPUESTO_SOBRE_DET pd Left Join MASERVICIOS_DET sd On pd.IDServicio_Det=sd.IDServicio_Det
Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor
Left Join COTIPAX cp On pd.IDPax=cp.IDPax
where sd.IDServicio='CUZ00179' and pd.NuPreSob=@NuPreSob And IsNull(pd.IDOperacion_Det,0)>0
Union
Select pd.NuDetPSo,pd.FeDetPSo, pd.TxServicio, IsNull(pd.CoPrvPrg,'') as CoPrvPrg, 
IsNull(p.NombreCorto,cp.Titulo+', '+cp.Apellidos+' '+cp.Nombres) as Guia,        
pd.QtPax, pd.SsPreUni, pd.SsTotal, pd.SsTotSus, pd.SsTotDev,          
es.NoTipPSo as DescEstado
from PRESUPUESTO_SOBRE_DET pd Left Join MAPROVEEDORES p On pd.CoPrvPrg=p.IDProveedor
Left Join COTIPAX cp On pd.IDPax=cp.IDPax
Left Join MATIPOPRESUPUESTO_SOBRE es On pd.CoTipPSo=es.CoTipPSo          
Where pd.NuPreSob=@NuPreSob
Order by pd.NuDetPSo        
