﻿Create Procedure RESERVAS_DET_PAX_InsxReserva_DetAcomVehiExt
	@IDReserva_Det	int,
	@IDReserva_DetNew	int,
	@UserMod	char(4)
As
	Set Nocount On

	Insert Into RESERVAS_DET_PAX
	(IDReserva_Det, IDPax, UserMod)	
	SELECT @IDReserva_DetNew, NuPax, @UserMod 
	FROM ACOMODO_VEHICULO_EXT_DET_PAX WHERE IDDET IN 
	(SELECT av.IDDet		
		FROM ACOMODO_VEHICULO_EXT av inner join RESERVAS_DET rd on av.IDDet=rd.IDDet
			And rd.IDReserva_Det in (@IDReserva_Det)
		and rd.IDReserva_DetOrigAcomVehi is null)
	And charindex('Grupo '+Cast(NuGrupo as varchar(3)),
		(Select Servicio From RESERVAS_DET Where IDReserva_Det=@IDReserva_DetNew))>0


