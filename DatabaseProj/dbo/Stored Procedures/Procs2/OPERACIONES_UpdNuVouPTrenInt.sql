﻿
Create Procedure dbo.OPERACIONES_UpdNuVouPTrenInt
	@IDOperacion int,
	@NuVouPTrenInt int,
	@UserMod char(4)
As
	Set NoCount On
	
	UPDATE OPERACIONES SET NuVouPTrenInt=@NuVouPTrenInt, UserMod=@UserMod, FecMod=GETDATE()
	WHERE IDOperacion=@IDOperacion
	
