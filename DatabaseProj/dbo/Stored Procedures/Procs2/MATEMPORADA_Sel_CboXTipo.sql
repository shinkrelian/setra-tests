﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_CboXTipo]
				@TipoTemporada char(1)
AS
BEGIN
	Set NoCount On
	Select IDTemporada, NombreTemporada, TipoTemporada
	From MATEMPORADA
	Where TipoTemporada = @TipoTemporada
	Order by NombreTemporada
END
