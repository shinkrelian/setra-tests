﻿

Create Procedure PRESUPUESTOS_DET_Del
@IDPresupuesto_Det int
As
	Set NoCount On
	Delete from [dbo].[PRESUPUESTOS_DET]
		Where [IDPresupuesto_Det] = @IDPresupuesto_Det
