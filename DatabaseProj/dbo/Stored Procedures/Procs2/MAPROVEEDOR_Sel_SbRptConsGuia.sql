﻿--JHD-20150505-@CoPais char(6)=''
--JHD-20150505- AND (up.IDubigeo =@CoUbigeo or rtrim(ltrim(@CoPais))='') 
--JRF-20151026-Cambiar la consulta completa.
--JRF-20151028-Colocar la descripción del nivelfeedback por registro
CREATE Procedure dbo.MAPROVEEDOR_Sel_SbRptConsGuia 
 @IDProveedor char(6),
 @NivelFeedBack varchar(30)
As  
	Set NoCount On  
	Declare @FlExcelent bit =0,@FlVeryGood bit = 0,@FlGood bit = 0,@FlAverage bit = 0,@FlPoor bit =0
	if Ltrim(Rtrim(Lower(@NivelFeedBack)))='excellent'
		Set @FlExcelent=1
	if Ltrim(Rtrim(Lower(@NivelFeedBack)))='verygood'
		Set @FlVeryGood=1
	if Ltrim(Rtrim(Lower(@NivelFeedBack)))='good'
		Set @FlGood=1
	if Ltrim(Rtrim(Lower(@NivelFeedBack)))='average'
		Set @FlAverage=1
	if Ltrim(Rtrim(Lower(@NivelFeedBack)))='poor'
		Set @FlPoor=1
	
	if Ltrim(Rtrim(Lower(@NivelFeedBack)))=''
	Begin
		Select ccpc.IDProveedor,c.IDFile,c.Titulo,IsNull(ccpc.TxComentario,'') as Comentario,us.Nombre+' '+ IsNull(us.TxApellidos,'') as Nombre,
			   dbo.FnDevuelveDescripcionFeedBack(ccpc.FlExcelent,ccpc.FlVeryGood ,ccpc.FlGood ,ccpc.FlAverage ,ccpc.FlPoor) As NivelFeedBack,ccpc.FecMod,
			   dbo.FnDevuelveEspecialidades(ccpc.IDProveedor) as Especialidades
		from (Select * from CONTROL_CALIDAD_PRO_MACUESTIONARIO) ccpc
		Left Join MAPROVEEDORES p On ccpc.IDProveedor=p.IDProveedor
		Left Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad
		Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo Left Join MAUBIGEO up On u.IDPais=up.IDubigeo  
		Left Join COTICAB c On ccp.IDCab=c.IDCAB Left Join MAUSUARIOS us On ccpc.UserMod=us.IDUsuario
		Where (ccpc.IDProveedor=@IDProveedor Or Ltrim(Rtrim(@IDProveedor))='') And
		p.Activo = 'A' --And ccpc.IDProveedor='000706'
		--Group By ccpc.IDProveedor
		Order By ccpc.FecMod desc
	End
	else
	Begin
		Select ccpc.IDProveedor,c.IDFile,c.Titulo,IsNull(ccpc.TxComentario,'') as Comentario,us.Nombre+' '+ IsNull(us.TxApellidos,'') as Nombre,
			   Lower(@NivelFeedBack) As NivelFeedBack,ccpc.FecMod,
			   dbo.FnDevuelveEspecialidades(ccpc.IDProveedor) as Especialidades
		from (Select * from CONTROL_CALIDAD_PRO_MACUESTIONARIO where
			  (FlExcelent=@FlExcelent And FlVeryGood=@FlVeryGood And FlGood=@FlGood And FlAverage=@FlAverage And FlPoor=@FlPoor)) ccpc
		Left Join MAPROVEEDORES p On ccpc.IDProveedor=p.IDProveedor
		Left Join CONTROL_CALIDAD_PRO ccp On ccpc.NuControlCalidad=ccp.NuControlCalidad
		Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo Left Join MAUBIGEO up On u.IDPais=up.IDubigeo  
		Left Join COTICAB c On ccp.IDCab=c.IDCAB Left Join MAUSUARIOS us On ccpc.UserMod=us.IDUsuario
		Where (ccpc.IDProveedor=@IDProveedor Or Ltrim(Rtrim(@IDProveedor))='') And
		p.Activo = 'A' --And ccpc.IDProveedor='000706'
		--Group By ccpc.IDProveedor
		Order By ccpc.FecMod desc
	End
  --  Select p.IDProveedor,cm.Comentario,cm.IDFile,us.Nombre,Convert(varchar(10),cm.FecMod,103) As FecMod 
	 --From MAPROVEEDORES p Left Join MAUBIGEO u On p.IDCiudad=u.IDubigeo  
	 --Left Join MAUBIGEO up On u.IDPais=up.IDubigeo  
	 --Left Join MATIPOIDENT tdi On p.IDIdentidad=tdi.IDIdentidad  
	 --Left Join MACOMENTARIOSPROVEEDOR cm On cm.IDProveedor = p.IDProveedor
	 --Left Join MAUSUARIOS us on cm.UserMod = us.IDUsuario
	 --Where p.IDTipoProv='005'  
	 --And (@IDIdioma In (Select ididioma From MAIDIOMASPROVEEDOR Where IDProveedor=p.IDProveedor)  
	 -- Or ltrim(rtrim(@IDIdioma))='')  
	 --And (p.Guia_Estado=@Guia_Estado Or ltrim(rtrim(@Guia_Estado))='')  
	 --And (p.Nombre+p.ApPaterno Like '%'+@Nombre+'%' Or ltrim(rtrim(@Nombre))='')  
	 --And (p.IDCiudad=@IDUbigeo Or ltrim(rtrim(@IDUbigeo))='')  
	 --And p.Activo = 'A'
	 --AND (up.IDubigeo =@CoPais or rtrim(ltrim(@CoPais))='') 
	 --Order by Nombre  
