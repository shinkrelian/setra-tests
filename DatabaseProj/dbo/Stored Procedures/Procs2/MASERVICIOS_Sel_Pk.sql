﻿

--JRF-20130325- Columna IdTipoProveedor del Proveedor  
--JRF-20131009- Columna Margen para los proveedores que tengan un operador internaciones
--PPMG-20151106-Columna APTServicoOpcional
CREATE Procedure [dbo].[MASERVICIOS_Sel_Pk]    
 @IDServicio char(8)    
AS
BEGIN
	Set NoCount On    
     
	Select s.*, u.IDPais, p.RazonSocial,p.NombreCorto, t.Descripcion as DescTipoProv  
	,p.IDTipoProv As IdTipoProveedor, 
	case when p.IDProveedorInternacional is not null then 1 Else 0 End as TieneProvInternacional,
	case when p.IDProveedorInternacional is not null then pOpIn.Margen else 0 End as MargenProvInt
	, s.APTServicoOpcional
	From MASERVICIOS s Left Join MAUBIGEO u On s.IDubigeo=u.IDubigeo    
	Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor    
	Left Join MATIPOPROVEEDOR t On p.IDTipoProv=t.IDTipoProv   
	Left Join MAPROVEEDORES pOpIn On p.IDProveedorInternacional = pOpIn.IDProveedor
	Where IDServicio=@IDServicio    
END

