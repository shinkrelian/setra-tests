﻿--JHD-20150626- Pais=isnull((select Descripcion from MAUBIGEO where IDubigeo=u.IDPais),''),       
--JHD-20150626- left join MAUBIGEO u on u.IDubigeo=p.IDCiudad  
CREATE Procedure dbo.ORDENPAGO_Sel_Rpt                
 @IDFile varchar(8),                
 @DescripcionProveedor varchar(200),        
 @FechaPagoIni smalldatetime,                
 @FechaPagoFin smalldatetime,             
 @FechaEmisionIni smalldatetime,        
 @FechaEmisionFin smalldatetime,           
 @IDEstado char(2),            
 @IDReserva int,            
 @IDMoneda char(3),
 @IDBanco char(3),
 @IDCab int                
As                
                
 Set Nocount On                
                 
 Select         
 case when op.FechaEmision is null then '' else convert(varchar,op.FechaEmision,103)End as FechaEmision,        
 convert(varchar,op.FechaPago,103) as FechaPago, op.IDOrdPag,              
 Case c.CoTipoVenta  
 when 'RC' Then 'RECEPTIVO'  
 when 'CU' Then 'COUNTER'  
 when 'VS' Then 'VIAJE INSPECCION (PERSONAL SETOURS)'  
 when 'VC' Then 'VIAJE INSPECCION (CLIENTE)'  
 End as DescTipoVenta,c.IDFile As IDFile,              
 p.NombreCorto as DescProvee,    
 Pais=isnull((select Descripcion from MAUBIGEO where IDubigeo=u.IDPais),''),  
 b.Sigla as SiglaBanco ,
 p.IDProveedor As IDProveedor,             
 op.IDReserva ,          
 p.IDTipoProv As IDTipoProv ,              
        
 M.IDMoneda As IDMoneda,              
 m.Descripcion as DescMoneda,                
 op.IDEstado,                
 Case op.IDEstado                 
 When 'GR' Then 'GENERADO'                
 When 'PD' Then 'PENDIENTE'                
 When 'PG' Then 'PAGADO'                
 End as DescEstado            
 ,(Select Sum(Total) From ORDENPAGO_DET Where IDOrdPag=op.IDOrdPag) as Total              
              
 From ORDENPAGO op                 
 Inner Join COTICAB c On op.IDCab=c.IDCAB                
 Left Join RESERVAS r On op.IDReserva=r.IDReserva                
 Left Join MAPROVEEDORES p On r.IDProveedor=p.IDProveedor                
 Left Join MAMONEDAS m On op.IDMoneda=m.IDMoneda                
 Left Join MABANCOS b On op.IDBanco = b.IDBanco
  left join MAUBIGEO u on u.IDubigeo=p.IDCiudad
 Where (op.FechaPago Between @FechaPagoIni And @FechaPagoFin Or @FechaPagoIni='01/01/1900')                
 And (convert(smalldatetime,convert(varchar,op.FechaEmision,103))      
 between @FechaEmisionIni And @FechaEmisionFin Or @FechaEmisionIni='01/01/1900')        
 And (c.IDFile Like '%'+ltrim(rtrim(@IDFile))+'%' Or ltrim(rtrim(@IDFile))='')                
 And (op.IDEstado=@IDEstado Or ltrim(rtrim(@IDEstado))='')                
 And (op.IDReserva=@IDReserva Or @IDReserva=0)         
 And (p.NombreCorto like '%'+@DescripcionProveedor+'%' or LTRIM(rtrim(@DescripcionProveedor))='')        
 And (c.IDCAB=@IDCab Or @IDCab=0)            
 And (op.IDBanco = @IDBanco Or ltrim(rtrim(@IDBanco))='')
 And (op.IDMoneda = @IDMoneda Or ltrim(rtrim(@IDMoneda))='')
 Order by op.FechaPago asc        

