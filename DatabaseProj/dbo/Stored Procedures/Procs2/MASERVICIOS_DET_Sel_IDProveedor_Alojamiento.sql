﻿
-------------------------------------------------------------------------------
--drop procedure MASERVICIOS_DET_Sel_IDProveedor
--create
CREATE Procedure MASERVICIOS_DET_Sel_IDProveedor_Alojamiento
 @IDProveedor char(6)=''    
As    
     
 Set NoCount On    
     
 Select sd.*, s.TipoLib, s.Liberado,s.IDProveedor, p.NombreCorto as DescProveedor,  
 s.Descripcion as DescServicio, s.Dias,
 mn.Simbolo as SimboloMonedaPais  
 From MASERVICIOS_DET sd     
 Left Join MASERVICIOS s On sd.IDServicio=s.IDServicio    
 Left Join MAPROVEEDORES p On s.IDProveedor=p.IDProveedor    
 
 Left Join MAMONEDAS mn On sd.CoMoneda=mn.IDMoneda               
 
 Where p.IDProveedor=@IDProveedor 
 and sd.ConAlojamiento=1
