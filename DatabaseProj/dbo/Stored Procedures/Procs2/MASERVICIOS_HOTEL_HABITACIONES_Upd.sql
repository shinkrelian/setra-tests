﻿
------------------------------------------------------------------------------------------------


--JHD-20141127-Agregar Nuevos campos: QtCantTriple,CoTipCama_Triple
--JHD-20141230-Agregar Nuevo campo: CoTipoHab_Oper

CREATE PROCEDURE [dbo].[MASERVICIOS_HOTEL_HABITACIONES_Upd]
	@CoServicio char(8),
	@NuHabitacion tinyint,
	@CoTipoHab tinyint=0,
	@QtCantidad tinyint=0,
	@QtCantMatri tinyint=0,
	@CoTipCama_Matri tinyint=0,
	@QtCantTwin tinyint=0,
	@CoTipCama_Twin tinyint=0,
	@CoTipCama_Adic tinyint=0,
	@QtCantTriple tinyint=0,
	@CoTipCama_Triple tinyint=0,
	@UserMod char(4)='',
	@CoTipoHab_Oper varchar(7)=''
AS
BEGIN
	Update dbo.MASERVICIOS_HOTEL_HABITACIONES
	Set
		CoTipoHab = Case When @CoTipoHab=0 Then Null Else @CoTipoHab End,
 		QtCantidad = @QtCantidad,
 		QtCantMatri = @QtCantMatri,
 		CoTipCama_Matri = Case When @CoTipCama_Matri=0 Then Null Else @CoTipCama_Matri End,
 		QtCantTwin = @QtCantTwin,
 		CoTipCama_Twin =Case When @CoTipCama_Twin=0 Then Null Else @CoTipCama_Twin End,
 		CoTipCama_Adic = Case When @CoTipCama_Adic=0 Then Null Else @CoTipCama_Adic End,
 		QtCantTriple = @QtCantTriple,
 		CoTipCama_Triple =Case When @CoTipCama_Triple=0 Then Null Else @CoTipCama_Triple End,
 		UserMod = @UserMod,
 		CoTipoHab_Oper =Case When @CoTipoHab_Oper='' Then Null Else @CoTipoHab_Oper End
 	Where 
		CoServicio = @CoServicio And 
		NuHabitacion = @NuHabitacion
END;
