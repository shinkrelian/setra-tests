﻿Create Procedure dbo.PRESUPUESTO_SOBRE_UpdSsPendientexDoc_Detalle_USD      
 @NuPreSob int,      
 @NuDetPSo tinyint,
 @SsTotalOrig numeric(9,2),      
 @UserMod char(4)      
As      
 Set NoCount On     
 
 declare @monto_orig numeric(12,2)=(select SsTotSus from [dbo].[PRESUPUESTO_SOBRE_DET] where nupresob=@NuPreSob and NuDetPSo=@NuDetPSo)
  
 Update PRESUPUESTO_SOBRE      
  Set SsSaldo_USD=(SsSaldo_USD+@monto_orig)-@SsTotalOrig,      
   UserMod=@UserMod,      
   FecMod=GETDATE()      
 where NuPreSob=@NuPreSob
       
 Declare @SsSaldoPend numeric(8,2)=(select Isnull(SsSaldo,0) from PRESUPUESTO_SOBRE where NuPreSob=@NuPreSob)
 Declare @SsSaldoPend_USD numeric(8,2)=(select Isnull(SsSaldo_USD,0) from PRESUPUESTO_SOBRE where NuPreSob=@NuPreSob)

 if @SsSaldoPend+@SsSaldoPend_USD = 0    
  Update PRESUPUESTO_SOBRE set CoEstadoFormaEgreso='AC' where NuPreSob=@NuPreSob

