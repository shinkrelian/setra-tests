﻿CREATE Procedure [dbo].[MATIPOUBIG_Sel_Rpt]
	@Descripcion varchar(20)
As	
	Set NoCount On

	Select  Descripcion as IdTipoUbigeo
	From MATIPOUBIG Where (Descripcion Like '%'+@Descripcion+'%'
	Or ltrim(rtrim(@Descripcion))='')
	order by 1
