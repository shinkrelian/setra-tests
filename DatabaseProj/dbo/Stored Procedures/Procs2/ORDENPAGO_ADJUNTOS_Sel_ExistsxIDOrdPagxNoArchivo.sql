﻿Create Procedure dbo.ORDENPAGO_ADJUNTOS_Sel_ExistsxIDOrdPagxNoArchivo
@IDOrdPag int,
@NombreArchivo varchar(200),
@pExisteArchivo bit output
As
Set NoCount On
Set @pExisteArchivo = 0
If Exists(select * from ORDENPAGO_ADJUNTOS 
		  where IDOrdPag=@IDOrdPag and CHARINDEX(@NombreArchivo,NoArchivo)>0)
Set @pExisteArchivo = 1
