﻿CREATE PROCEDURE [dbo].[RESERVAS_DET_BUP_Ins_xIDReserva]
	@IDReserva	int,
	@UserMod	char(4)
As
	Set Nocount On
	
	Insert Into RESERVAS_DET_BUP(IDReserva, IDReserva_Det, Dia, FechaOut, Noches, 
		Servicio, Cantidad, NroPax, CodReservaProv, UserMod)
	Select d.IDReserva, d.IDReserva_Det, d.Dia, d.FechaOut, d.Noches, 
		d.Servicio, d.Cantidad, d.NroPax ,r.CodReservaProv, @UserMod
	From RESERVAS_DET d Left Join RESERVAS r On r.IDReserva=d.IDReserva
	Where d.IDReserva=@IDReserva
