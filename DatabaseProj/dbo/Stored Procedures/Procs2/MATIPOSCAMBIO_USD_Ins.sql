﻿CREATE PROCEDURE DBO.MATIPOSCAMBIO_USD_Ins
@CoMoneda char(3),
@FeTipCam smalldatetime,
@SsTipCam decimal(6,3),
@UserMod char(4)
as
SET NOCOUNT ON
INSERT INTO [dbo].[MATIPOSCAMBIO_USD]
           ([CoMoneda]
           ,[FeTipCam]
           ,[SsTipCam]
           ,[UserMod]
           ,[FecMod])
     VALUES
           (@CoMoneda
           ,@FeTipCam
           ,@SsTipCam
           ,@UserMod
           ,GETDATE())
