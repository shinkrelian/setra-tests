﻿CREATE Procedure [dbo].[MAUSUARIOS_Sel_Cbo] 
	@bTodos bit
As

	Set NoCount On
	
	Select * from 
	(
	   Select '' as IDUsuario,'<TODOS>' As Usuario,0 as Ord
	   Union
	   Select IDUsuario,Usuario,1 from MAUSUARIOS Where Activo='A'
	) As X
	Where (@bTodos=1 Or Ord<>0) 
	Order By Ord,Usuario
