﻿
CREATE Procedure dbo.RESERVAS_DET_InsAcomodoVehicExt	
	@IDReserva int,
	@TablaTmp_M bit,
	@UserMod char(4)	
As
	Set Nocount On
	Declare @IDReserva_Det int, @Cantidad tinyint,  
		@QtPax smallint, @QtGrupo tinyint, @IDReserva_DetNew int

	If @TablaTmp_M=0
		Begin
		--SELECT av.QtGrupo,
		--	(select max(IDReserva_Det) from RESERVAS_DET where iddet=av.IDDet 
		--		And Anulado=0 And IDReserva_DetOrigAcomVehi is null) as IDReservas_Det,
		--	av.QtPax
		--INTO #ACOMODO_VEHICULO_EXT_TMP
		--FROM ACOMODO_VEHICULO_EXT av 
		--WHERE IDDet IN (SELECT rd1.IDDET FROM RESERVAS_DET rd1 
		--	Inner join RESERVAS r1 On r1.IDReserva=@IDReserva
		--	And r1.IDReserva=rd1.IDReserva
		--	)

		SELECT av.QtGrupo,rd.IDReserva_Det,av.QtPax
		INTO #ACOMODO_VEHICULO_EXT_TMP
		FROM ACOMODO_VEHICULO_EXT av inner join RESERVAS_DET rd on av.IDDet=rd.IDDet
		inner join RESERVAS r on r.IDReserva=rd.IDReserva and r.IDReserva=@IDReserva 
		and rd.IDReserva_DetOrigAcomVehi is null

		Declare curAcomExt cursor for
		Select * From #ACOMODO_VEHICULO_EXT_TMP

		End
	Else
		Begin
		--SELECT av.QtGrupo,
		--	ISNULL(
		--	(select IDReserva_Det from RESERVAS_DET where iddet=av.IDDet 
		--		And Anulado=1 and IDReserva_DetOrigAcomVehi is null), 
		--	(select max(IDReserva_Det) from RESERVAS_DET where iddet=av.IDDet 
		--		And Anulado=0 and IDReserva_DetOrigAcomVehi is null)
		--		) as IDReservas_Det,
		--	av.QtPax
		--INTO #ACOMODO_VEHICULO_EXT_TMP2
		--FROM ACOMODO_VEHICULO_EXT av 
		--WHERE IDDet IN (SELECT rd1.IDDET FROM RESERVAS_DET rd1 
		--	Inner join RESERVAS r1 On r1.IDReserva=@IDReserva
		--	And r1.IDReserva=rd1.IDReserva
		--	)
		SELECT av.QtGrupo,rd.IDReserva_Det,av.QtPax
		INTO #ACOMODO_VEHICULO_EXT_TMP2
		FROM ACOMODO_VEHICULO_EXT av inner join RESERVAS_DET rd on av.IDDet=rd.IDDet
		inner join RESERVAS r on r.IDReserva=rd.IDReserva and r.IDReserva=@IDReserva 
		and rd.IDReserva_DetOrigAcomVehi is null
		If @@ROWCOUNT=0
			Insert INTO #ACOMODO_VEHICULO_EXT_TMP2
			SELECT av.QtGrupo,rd.IDReserva_Det,av.QtPax			
			FROM ACOMODO_VEHICULO_EXT av inner join RESERVAS_DET rd on av.IDDet=rd.IDDet
			inner join RESERVAS r on r.IDReserva=rd.IDReserva and r.IDReserva=@IDReserva 
			and not rd.IDReserva_DetOrigAcomVehi is null
		

		Declare curAcomExt cursor for
		Select * From #ACOMODO_VEHICULO_EXT_TMP2

		End

	Create Table #ReservasDetEliminarExt(IDReserva_Det int, IDReserva_DetNew int)

	Open curAcomExt
	Fetch Next From curAcomExt Into @QtGrupo, @IDReserva_Det, @QtPax
	While @@FETCH_STATUS=0
		Begin

		Declare @QtGruposAcom tinyint=1
		While @QtGruposAcom<=@QtGrupo
			Begin
			     
			If @TablaTmp_M=0
				Begin				                          
				                       
				Execute dbo.Correlativo_SelOutput 'RESERVAS_DET',1,10,@IDReserva_DetNew output

				INSERT INTO RESERVAS_DET                              
					 ([IDReserva_Det]                              
					 ,[IDReserva]                              
					 ,[IDFile]                              
					 ,[IDDet]                              
					 ,[Item]                              
					 ,[Dia]           
					 ,[DiaNombre]                              
					 ,[FechaOut]                        
					 ,[Noches]                        
					 ,[IDubigeo]                              
					 ,[IDServicio]                              
					 ,[IDServicio_Det]                              
					 ,[Especial]                              
					 ,[MotivoEspecial]                  
					 ,[RutaDocSustento]                              
					 ,[Desayuno]                              
					 ,[Lonche]                              
					 ,[Almuerzo]                              
					 ,[Cena]                              
					 ,[Transfer]                              
					 ,[IDDetTransferOri]                              
					 ,[IDDetTransferDes]                              
					 ,[CamaMat]     
					 ,[TipoTransporte]                              
					 ,[IDUbigeoOri]                              
					 ,[IDUbigeoDes]                              
					 ,[IDIdioma]                              
					 ,[NroPax]                              
					 ,[NroLiberados]                              
					 ,[Tipo_Lib]                              
					 ,Cantidad                            
					 ,CantidadAPagar                          
					 ,IDMoneda                      
					 ,[CostoReal]                              
					 ,[CostoRealAnt]                              
					 ,[CostoLiberado]                              
					 ,[Margen]                              
					 ,[MargenAplicado]                              
					 ,[MargenAplicadoAnt]                              
					 ,[MargenLiberado]                              
					 ,[Total]                     
					 ,[TotalOrig]                              
					 ,[CostoRealImpto]                              
					 ,[CostoLiberadoImpto]                              
					 ,[MargenLiberadoImpto]                              
					 ,[MargenImpto]                              
					 ,[TotImpto]                           
					 ,[NetoHab]                            
					 ,[IgvHab]                            
					 ,[TotalHab]                            
					 ,[NetoGen]                         
					 ,[IgvGen]                            
					 ,[TotalGen]                        
					 ,[IDGuiaProveedor]  
					 ,[NuVehiculo]  
					 ,[IDDetRel]  
					 ,[Servicio]  
					 ,[IDReserva_DetCopia]  
					 ,[IDReserva_Det_Rel]  
					 ,[IDEmailEdit]  
					 ,[IDEmailNew]  
					 ,[IDEmailRef]  
					 ,[CapacidadHab]  
					 ,[EsMatrimonial]  
					 ,[FlServicioParaGuia]  
					 --,[FeUpdateRow]  
					 ,[FlServicioNoShow]  
					 ,[FlServicioIngManual]  
					 ,[FlServicioTC]
					 ,IDReserva_DetOrigAcomVehi
					 ,[UserMod])
				Select @IDReserva_DetNew--+(ROW_NUMBER() OVER(ORDER BY idreserva_det))-1
					 ,[IDReserva]                              
					 ,[IDFile]                              
					 ,[IDDet]                              
					 ,[Item]                              
					 ,[Dia]           
					 ,[DiaNombre]                              
					 ,[FechaOut]                        
					 ,[Noches]                        
					 ,[IDubigeo]                              
					 ,[IDServicio]                              
					 ,[IDServicio_Det]                              
					 ,[Especial]                              
					 ,[MotivoEspecial]                  
					 ,[RutaDocSustento]                              
					 ,[Desayuno]                              
					 ,[Lonche]                              
					 ,[Almuerzo]                              
					 ,[Cena]                              
					 ,[Transfer]                              
					 ,[IDDetTransferOri]                              
					 ,[IDDetTransferDes]                              
					 ,[CamaMat]                              
					 ,[TipoTransporte]                              
					 ,[IDUbigeoOri]                              
					 ,[IDUbigeoDes]                              
					 ,[IDIdioma]                              
					 ,(@QtPax/@QtGrupo)-ISNULL(NroLiberados,0)
					 ,[NroLiberados]                              
					 ,[Tipo_Lib]                              
					 ,(@QtPax/@QtGrupo)
					 ,Case When @QtGruposAcom=1 Then
						(@QtPax/@QtGrupo)-((NroPax+isnull(NroLiberados,0))-CantidadAPagar)
					 Else
						(@QtPax/@QtGrupo)
					  End
					 ,IDMoneda                      
					 ,[CostoReal]                              
					 ,[CostoRealAnt]                              
					 ,[CostoLiberado]                              
					 ,[Margen]                              
					 ,[MargenAplicado]                              
					 ,[MargenAplicadoAnt]                              
					 ,[MargenLiberado]                              
					 ,[Total]                     
					 ,[TotalOrig]                              
					 ,[CostoRealImpto]                              
					 ,[CostoLiberadoImpto]                              
					 ,[MargenLiberadoImpto]                              
					 ,[MargenImpto]                              
					 ,[TotImpto]                           
					 ,[NetoHab]                            
					 ,[IgvHab]                            
					 ,[TotalHab]                            
					 ,[NetoHab]*(@QtPax/@QtGrupo)--[NetoGen] 
					 ,[IgvHab]*(@QtPax/@QtGrupo)--[IgvGen]
					 ,[TotalHab]*(@QtPax/@QtGrupo)--[TotalGen]
					 ,[IDGuiaProveedor]  
					 ,NuVehiculo
					 ,[IDDetRel]  
					 ,[Servicio]+' (Grupo '+ cast(@QtGruposAcom as varchar(3)) +')' 
					 ,[IDReserva_DetCopia]  
					 ,[IDReserva_Det_Rel]  
					 ,[IDEmailEdit]  
					 ,[IDEmailNew]  
					 ,[IDEmailRef]  
					 ,[CapacidadHab]  
					 ,[EsMatrimonial]  
					 ,[FlServicioParaGuia]  
					 --,[FeUpdateRow]  
					 ,[FlServicioNoShow]  
					 ,[FlServicioIngManual]  
					 ,[FlServicioTC]
					 ,@IDReserva_Det
					 ,@UserMod
				From RESERVAS_DET where IDReserva_Det=@IDReserva_Det

				Insert Into #ReservasDetEliminarExt Values(@IDReserva_Det, @IDReserva_DetNew)

				Update RESERVAS_DET Set Anulado=1,FecMod=GETDATE(),UserMod=@UserMod 
					Where IDReserva_Det = @IDReserva_Det

				Set @QtGruposAcom+=1
				End
			Else
				Begin
				Execute dbo.Correlativo_SelOutput 'RESERVAS_DET_TMP',1,10,@IDReserva_DetNew output

				INSERT INTO RESERVAS_DET_TMP
					 ([IDReserva_Det]                              
					 ,[IDReserva]                              
					 ,[IDFile]                              
					 ,[IDDet]                              
					 ,[Item]                              
					 ,[Dia]           
					 ,[DiaNombre]                              
					 ,[FechaOut]                        
					 ,[Noches]                        
					 ,[IDubigeo]                              
					 ,[IDServicio]                              
					 ,[IDServicio_Det]                              
					 ,[Especial]                              
					 ,[MotivoEspecial]                  
					 ,[RutaDocSustento]                              
					 ,[Desayuno]                              
					 ,[Lonche]                              
					 ,[Almuerzo]                              
					 ,[Cena]                              
					 ,[Transfer]                              
					 ,[IDDetTransferOri]                              
					 ,[IDDetTransferDes]                              
					 ,[CamaMat]                              
					 ,[TipoTransporte]                              
					 ,[IDUbigeoOri]                              
					 ,[IDUbigeoDes]                              
					 ,[IDIdioma]                              
					 ,[NroPax]                              
					 ,[NroLiberados]                              
					 ,[Tipo_Lib]                              
					 ,Cantidad                            
					 ,CantidadAPagar                          
					 ,IDMoneda                      
					 ,[CostoReal]                              
					 ,[CostoRealAnt]                              
					 ,[CostoLiberado]                              
					 ,[Margen]                              
					 ,[MargenAplicado]                              
					 ,[MargenAplicadoAnt]                              
					 ,[MargenLiberado]                              
					 ,[Total]                     
					 ,[TotalOrig]                              
					 ,[CostoRealImpto]                              
					 ,[CostoLiberadoImpto]                              
					 ,[MargenLiberadoImpto]                              
					 ,[MargenImpto]                              
					 ,[TotImpto]                           
					 ,[NetoHab]                            
					 ,[IgvHab]                            
					 ,[TotalHab]                            
					 ,[NetoGen]                         
					 ,[IgvGen]                          
					 ,[TotalGen]                        
					 ,[IDGuiaProveedor]  
					 ,[NuVehiculo]  
					 ,[IDDetRel]  
					 ,[Servicio]  
					 ,[IDReserva_DetCopia]  
					 ,[IDReserva_Det_Rel]  
					 ,[IDEmailEdit]  
					 ,[IDEmailNew]  
					 ,[IDEmailRef]  
					 ,[CapacidadHab]  
					 ,[EsMatrimonial]  
					 ,[FlServicioTC]

					 ,[IDReserva_DetReal]  
					 ,IDReserva_DetOrigAcomVehi
					 
					 ,[UserMod])
				Select @IDReserva_DetNew--+(ROW_NUMBER() OVER(ORDER BY idreserva_det))-1
					 ,[IDReserva]                              
					 ,[IDFile]                              
					 ,[IDDet]                              
					 ,[Item]                              
					 ,[Dia]           
					 ,[DiaNombre]                              
					 ,[FechaOut]                        
					 ,[Noches]                        
					 ,[IDubigeo]                              
					 ,[IDServicio]                              
					 ,[IDServicio_Det]                              
					 ,[Especial]                              
					 ,[MotivoEspecial]                  
					 ,[RutaDocSustento]                              
					 ,[Desayuno]                              
					 ,[Lonche]                              
					 ,[Almuerzo]                              
					 ,[Cena]                              
					 ,[Transfer]                              
					 ,[IDDetTransferOri]                              
					 ,[IDDetTransferDes]                              
					 ,[CamaMat]                              
					 ,[TipoTransporte]                              
					 ,[IDUbigeoOri]                              
					 ,[IDUbigeoDes]                              
					 ,[IDIdioma]                              
					 ,(@QtPax/@QtGrupo)-ISNULL(NroLiberados,0)
					 ,[NroLiberados]                              
					 ,[Tipo_Lib]                              
					 ,(@QtPax/@QtGrupo)
					 --,(@QtPax/@QtGrupo)
					,Case When @QtGruposAcom=1 Then
						(@QtPax/@QtGrupo)-((NroPax+isnull(NroLiberados,0))-CantidadAPagar)
					 Else
						(@QtPax/@QtGrupo)
					  End
					 ,IDMoneda                      
					 ,[CostoReal]                              
					 ,[CostoRealAnt]                              
					 ,[CostoLiberado]                              
					 ,[Margen]                              
					 ,[MargenAplicado]                              
					 ,[MargenAplicadoAnt]                              
					 ,[MargenLiberado]                              
					 ,[Total]                     
					 ,[TotalOrig]                              
					 ,[CostoRealImpto]                              
					 ,[CostoLiberadoImpto]                              
					 ,[MargenLiberadoImpto]                              
					 ,[MargenImpto]                              
					 ,[TotImpto]                           
					 ,[NetoHab]                            
					 ,[IgvHab]                            
					 ,[TotalHab]                            
					 ,[NetoHab]*(@QtPax/@QtGrupo)--[NetoGen] 
					 ,[IgvHab]*(@QtPax/@QtGrupo)--[IgvGen]
					 ,[TotalHab]*(@QtPax/@QtGrupo)--[TotalGen]
					 ,[IDGuiaProveedor]  
					 ,NuVehiculo
					 ,[IDDetRel]  
					 ,--[Servicio]+' (Grupo '+ cast(@QtGruposAcom as varchar(3)) +')' 
					 Case When CHARINDEX('(Grupo',Servicio)=0 then
						[Servicio]+' (Grupo '+ cast(@QtGruposAcom as varchar(3)) +')' 
					 else
						[Servicio]
					 end
					 ,[IDReserva_DetCopia]  
					 ,[IDReserva_Det_Rel]  
					 ,[IDEmailEdit]  
					 ,[IDEmailNew]  
					 ,[IDEmailRef]  
					 ,[CapacidadHab]  
					 ,[EsMatrimonial]  
					 ,[FlServicioTC]

						,@IDReserva_Det

					 ,isnull(
					 (Select max(IDReserva_Det) From RESERVAS_DET Where IDReserva_DetOrigAcomVehi=@IDReserva_Det),
						@IDReserva_Det)

					 ,@UserMod
				From RESERVAS_DET where IDReserva_Det=@IDReserva_Det and IDReserva_DetOrigAcomVehi is null

				If @@ROWCOUNT=0
					INSERT INTO RESERVAS_DET_TMP
						 ([IDReserva_Det]                              
						 ,[IDReserva]                              
						 ,[IDFile]                             
						 ,[IDDet]                              
						 ,[Item]                              
						 ,[Dia]           
						 ,[DiaNombre]                              
						 ,[FechaOut]                        
						 ,[Noches]                        
						 ,[IDubigeo]                              
						 ,[IDServicio]                              
						 ,[IDServicio_Det]                              
						 ,[Especial]                              
						 ,[MotivoEspecial]                  
						 ,[RutaDocSustento]                              
						 ,[Desayuno]                              
						 ,[Lonche]                              
						 ,[Almuerzo]                              
						 ,[Cena]                              
						 ,[Transfer]                              
						 ,[IDDetTransferOri]                              
						 ,[IDDetTransferDes]                              
						 ,[CamaMat]                              
						 ,[TipoTransporte]                              
						 ,[IDUbigeoOri]                              
						 ,[IDUbigeoDes]                              
						 ,[IDIdioma]                              
						 ,[NroPax]                              
						 ,[NroLiberados]                              
						 ,[Tipo_Lib]                              
						 ,Cantidad                            
						 ,CantidadAPagar                          
						 ,IDMoneda                      
						 ,[CostoReal]                              
						 ,[CostoRealAnt]                              
						 ,[CostoLiberado]                              
						 ,[Margen]                              
						 ,[MargenAplicado]                              
						 ,[MargenAplicadoAnt]                              
						 ,[MargenLiberado]                              
						 ,[Total]                     
						 ,[TotalOrig]                              
						 ,[CostoRealImpto]                              
						 ,[CostoLiberadoImpto]                              
						 ,[MargenLiberadoImpto]                              
						 ,[MargenImpto]                              
						 ,[TotImpto]                           
						 ,[NetoHab]                            
						 ,[IgvHab]                            
						 ,[TotalHab]                            
						 ,[NetoGen]                         
						 ,[IgvGen]                            
						 ,[TotalGen]                        
						 ,[IDGuiaProveedor]  
						 ,[NuVehiculo]  
						 ,[IDDetRel]  
						 ,[Servicio]  
						 ,[IDReserva_DetCopia]  
						 ,[IDReserva_Det_Rel]  
						 ,[IDEmailEdit]  
						 ,[IDEmailNew]  
						 ,[IDEmailRef]  
						 ,[CapacidadHab]  
						 ,[EsMatrimonial]  
						 ,[FlServicioTC]

						 ,[IDReserva_DetReal]  
						 ,IDReserva_DetOrigAcomVehi
					 
						 ,[UserMod])

					--Select @IDReserva_DetNew+(ROW_NUMBER() OVER(ORDER BY IDReserva_DetReal))-1,
					--* From
					--(
					Select --distinct
						@IDReserva_DetNew--+(ROW_NUMBER() OVER(ORDER BY idreserva_det))-1,
						 ,[IDReserva]                              
						 ,[IDFile]                              
						 ,[IDDet]                              
						 ,[Item]                              
						 ,[Dia]           
						 ,[DiaNombre]                              
						 ,[FechaOut]                        
						 ,[Noches]                        
						 ,[IDubigeo]                              
						 ,[IDServicio]                              
						 ,[IDServicio_Det]                              
						 ,[Especial]                              
						 ,[MotivoEspecial]                  
						 ,[RutaDocSustento]                              
						 ,[Desayuno]                              
						 ,[Lonche]                              
						 ,[Almuerzo]                              
						 ,[Cena]                              
						 ,[Transfer]                              
						 ,[IDDetTransferOri]                              
						 ,[IDDetTransferDes]                              
						 ,[CamaMat]                              
						 ,[TipoTransporte]                              
						 ,[IDUbigeoOri]                              
						 ,[IDUbigeoDes]                              
						 ,[IDIdioma]                              
						 ,(@QtPax/@QtGrupo)-ISNULL(NroLiberados,0) as NroPax
						 ,[NroLiberados]                              
						 ,[Tipo_Lib]                              
						 ,(@QtPax/@QtGrupo) as Cantidad
						 --,(@QtPax/@QtGrupo) as CantidadAPagar
						,Case When @QtGruposAcom=1 Then
							(@QtPax/@QtGrupo)-((NroPax+isnull(NroLiberados,0))-CantidadAPagar)
						Else
							(@QtPax/@QtGrupo)
						End
						 ,IDMoneda                      
						 ,[CostoReal]                              
						 ,[CostoRealAnt]                              
						 ,[CostoLiberado]                              
						 ,[Margen]                              
						 ,[MargenAplicado]                              
						 ,[MargenAplicadoAnt]                              
						 ,[MargenLiberado]                              
						 ,[Total]                     
						 ,[TotalOrig]                              
						 ,[CostoRealImpto]                              
						 ,[CostoLiberadoImpto]                              
						 ,[MargenLiberadoImpto]                              
						 ,[MargenImpto]                              
						 ,[TotImpto]                           
						 ,[NetoHab]                            
						 ,[IgvHab]                            
						 ,[TotalHab]                            
						 ,[NetoHab]*(@QtPax/@QtGrupo) as NetoGen
						 ,[IgvHab]*(@QtPax/@QtGrupo) as IgvGen
						 ,[TotalHab]*(@QtPax/@QtGrupo) as TotalGen
						 ,[IDGuiaProveedor]  
						 ,NuVehiculo
						 ,[IDDetRel]  
						 ,--[Servicio]+' (Grupo '+ cast(@QtGruposAcom as varchar(3)) +')' 
						 Case When CHARINDEX('(Grupo',Servicio)=0 then
							[Servicio]+' (Grupo '+ cast(@QtGruposAcom as varchar(3)) +')' 
						 else
							[Servicio]
						 end as Servicio
						 ,[IDReserva_DetCopia]  
						 ,[IDReserva_Det_Rel]  
						 ,[IDEmailEdit]  
						 ,[IDEmailNew]  
						 ,[IDEmailRef]  
						 ,[CapacidadHab]  
						 ,[EsMatrimonial]  
						 ,[FlServicioTC]

							,@IDReserva_Det as IDReserva_DetReal

						 ,isnull(
						 (Select max(IDReserva_Det) From RESERVAS_DET Where IDReserva_DetOrigAcomVehi=@IDReserva_Det),
							@IDReserva_Det) as IDReserva_DetOrigAcomVehi

						 ,@UserMod as UserMod
					From RESERVAS_DET where IDReserva_Det=@IDReserva_Det and not IDReserva_DetOrigAcomVehi is null
					--) as x				


				Insert Into #ReservasDetEliminarExt Values(@IDReserva_Det, @IDReserva_DetNew)

				Update RESERVAS_DET_TMP Set Anulado=1,FecMod=GETDATE(),UserMod=@UserMod 
					Where IDReserva_DetReal = @IDReserva_Det and IDReserva_DetOrigAcomVehi is null

				Set @QtGruposAcom+=1

				End
			End
		Fetch Next From curAcomExt Into @QtGrupo, @IDReserva_Det, @QtPax
		End
	Close curAcomExt
	Deallocate curAcomExt

	Select * From #ReservasDetEliminarExt

	If @TablaTmp_M=0	
		Drop Table #ACOMODO_VEHICULO_EXT_TMP
	Else
		Begin
		Drop Table #ACOMODO_VEHICULO_EXT_TMP2

		If exists(Select * From #ReservasDetEliminarExt)
			Begin
			Select * Into #RESERVAS_DET_TMP_TEMP 
			From RESERVAS_DET_TMP WHERE IDReserva=@IDReserva

			Update #RESERVAS_DET_TMP_TEMP Set IDReserva_Det=0, FecMod=GETDATE()

			Select DISTINCT * Into #RESERVAS_DET_TMP_TEMPDISTINCT
			From #RESERVAS_DET_TMP_TEMP 
				
			Delete From RESERVAS_DET_TMP where IDReserva=@IDReserva

			Insert Into RESERVAS_DET_TMP
			(idreserva_det,
			 IDReserva,IDFile,IDDet,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,IDServicio_Det,
			 IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,
			 IDDetTransferOri,IDDetTransferDes,CamaMat,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,
			 NroLiberados,Tipo_Lib,Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,
			 MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,CostoRealImpto,CostoLiberadoImpto,
			 MargenImpto,MargenLiberadoImpto,TotImpto,NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,IDDetRel,
			 Servicio,IDReserva_DetCopia ,IDReserva_Det_Rel ,IDEmailRef,IDEmailEdit,IDEmailNew,CapacidadHab,
			 EsMatrimonial ,UserMod ,IDMoneda ,IDGuiaProveedor ,NuVehiculo ,ExecTrigger ,Anulado ,
			 IDReserva_DetReal,FlServicioTC ,IDReserva_DetOrigAcomVehi)
			Select @IDReserva_DetNew+(ROW_NUMBER() OVER(ORDER BY idreserva_det))-1,
			 IDReserva,IDFile,IDDet,Item,Dia,DiaNombre,FechaOut,Noches,IDubigeo,IDServicio,IDServicio_Det,
			 IDIdioma,Especial,MotivoEspecial,RutaDocSustento,Desayuno,Lonche,Almuerzo,Cena,Transfer,
			 IDDetTransferOri,IDDetTransferDes,CamaMat,TipoTransporte,IDUbigeoOri,IDUbigeoDes,NroPax,
			 NroLiberados,Tipo_Lib,Cantidad,CantidadAPagar,CostoReal,CostoRealAnt,CostoLiberado,Margen,
			 MargenAplicado,MargenAplicadoAnt,MargenLiberado,Total,TotalOrig,CostoRealImpto,CostoLiberadoImpto,
			 MargenImpto,MargenLiberadoImpto,TotImpto,NetoHab,IgvHab,TotalHab,NetoGen,IgvGen,TotalGen,IDDetRel,
			 Servicio,IDReserva_DetCopia ,IDReserva_Det_Rel ,IDEmailRef,IDEmailEdit,IDEmailNew,CapacidadHab,
			 EsMatrimonial ,UserMod,IDMoneda ,IDGuiaProveedor ,NuVehiculo ,ExecTrigger ,Anulado ,
			 IDReserva_DetReal,FlServicioTC ,IDReserva_DetOrigAcomVehi
			FROM #RESERVAS_DET_TMP_TEMPDISTINCT 

			Drop table #RESERVAS_DET_TMP_TEMP
			Drop table #RESERVAS_DET_TMP_TEMPDISTINCT 
			End
		End

	Drop Table #ReservasDetEliminarExt

