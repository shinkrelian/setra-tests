﻿
CREATE PROCEDURE [dbo].[MATEMPORADA_Sel_Cbo] 
AS
BEGIN
	Set NoCount On  
	Select IDTemporada, NombreTemporada, TipoTemporada
	From MATEMPORADA
	Order by NombreTemporada
END
