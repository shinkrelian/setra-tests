﻿
Imports System.IO
Imports System.Web.Script.Serialization
Imports ComSEToursBL2
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports NUnit.Framework

<TestFixture>
Public Class clsCotizacionBNTests

    <SetUp()>
    Public Sub RestaurarBD()
        'Para asegurar la integridad de los datos restauramos la BD
        Dim restore As New DatabaseRestore
        restore.restoreDB()

    End Sub

    'Test que lee datos desde archivo json
    'ejecuta tantos test como lineas de datos existen en el archivo
    <Test, TestCaseSource(GetType(FileDataClass), "TestData")>
    Public Sub CalculoCotizacionTest(test As String, datoTest As DatoTest)
        'Esta llamada permite debuggear el test
        'System.Diagnostics.Debugger.Launch()

        Dim sut As New clsCotizacionBN
        Dim result = sut.CalculoCotizacion(
            datoTest.vstrIDServicio,
            datoTest.vintIDServicio_Det,
            datoTest.vstrAnio,
            datoTest.vstrTipo,
            datoTest.vstrTipoProv,
            datoTest.vintPax,
            datoTest.vintLiberados,
            datoTest.vstrTipo_Lib,
            datoTest.vdblMargenCotiCab,
            datoTest.vdblTCambioCotiCab,
            datoTest.vdblCostoReal,
            datoTest.vdblMargenAplicado,
            datoTest.vstrTipoPax,
            datoTest.vsglRedondeo,
            datoTest.rdttDetCotiServ,
            datoTest.vsglIGV,
            datoTest.vstrIDDet_DetCotiServ,
            datoTest.vblnIncGuia,
            datoTest.vblnMargenCero,
            datoTest.vCnn,
            datoTest.vCnn2,
            datoTest.vdatFechaServicio,
            datoTest.vdblSSCR,
            datoTest.vdblSTCR,
            datoTest.vblnServicioTC,
            datoTest.vstrIDTipoHonario,
            datoTest.vdicValsCostoRealTC,
            datoTest.vblnAcomVehic)

        'Tanto los valores para el calculo como el valor esperado son leidos del archivo
        Assert.That(datoTest.CostoRealEspearado, [Is].EqualTo(result.Item("CostoReal")))

    End Sub

End Class

'Esta clase genera los datos
'el archivo es copiado al la ruta C:\Datos.txt como un proceso de pre-build
Public Class FileDataClass
    Public Shared ReadOnly Iterator Property TestData() As IEnumerable
        Get
            Dim dato As New DatoTest()
            Dim fileName As String = "C:\Datos.txt"
            Dim objReader As New System.IO.StreamReader(fileName)
            Do While objReader.Peek() <> -1
                dato = JsonConvert.DeserializeObject(Of DatoTest)(objReader.ReadLine())
                dato.rdttDetCotiServ = dttCreaDttDetCotiServ()
                Yield New TestCaseData(dato.caso, dato)
            Loop
        End Get
    End Property

    Private Shared Function dttCreaDttDetCotiServ() As DataTable
        Dim dttDetCotiServ As New DataTable("DetCotiServ")
        With dttDetCotiServ
            .Columns.Add("IDDet")
            .Columns.Add("IDServicio")
            .Columns.Add("IDServicio_Det")
            .Columns.Add("CostoReal")
            .Columns.Add("Margen")
            .Columns.Add("CostoLiberado")
            .Columns.Add("MargenLiberado")
            .Columns.Add("MargenAplicado")
            .Columns.Add("Total")
            .Columns.Add("RedondeoTotal")
            .Columns.Add("SSCR")
            .Columns.Add("SSMargen")
            .Columns.Add("SSCL")
            .Columns.Add("SSML")
            .Columns.Add("SSTotal")
            .Columns.Add("STCR")
            .Columns.Add("STMargen")
            .Columns.Add("STTotal")

            .Columns.Add("CostoRealImpto")
            .Columns.Add("CostoLiberadoImpto")
            .Columns.Add("MargenImpto")
            .Columns.Add("MargenLiberadoImpto")
            .Columns.Add("SSCRImpto")
            .Columns.Add("SSCLImpto")
            .Columns.Add("SSMargenImpto")
            .Columns.Add("SSMargenLiberadoImpto")
            .Columns.Add("STCRImpto")
            .Columns.Add("STMargenImpto")
            .Columns.Add("TotImpto")
        End With


        Return dttDetCotiServ
    End Function

End Class

'Esta clase es la estructura de datos para enviar los datos
Public Class DatoTest
    Public caso As String 'Titulo del caso
    Public CostoRealEspearado As Double 'Valor esperado del calculo

    Public vstrIDServicio As String
    Public vintIDServicio_Det As Int32
    Public vstrAnio As String
    Public vstrTipo As String
    Public vstrTipoProv As String
    Public vintPax As Int16
    Public vintLiberados As Int16
    Public vstrTipo_Lib As Char
    Public vdblMargenCotiCab As Double
    Public vdblTCambioCotiCab As Single
    Public vdblCostoReal As Double
    Public vdblMargenAplicado As Double
    Public vstrTipoPax As String
    Public vsglRedondeo As Single
    Public rdttDetCotiServ As DataTable
    Public vsglIGV As Single
    Public vstrIDDet_DetCotiServ As String
    Public vblnIncGuia As Boolean
    Public vblnMargenCero As Boolean
    Public vCnn As SqlClient.SqlConnection
    Public vCnn2 As SqlClient.SqlConnection
    Public vdatFechaServicio As Date
    Public vdblSSCR As Double
    Public vdblSTCR As Double
    Public vblnServicioTC As Boolean
    Public vstrIDTipoHonario As String
    Public vdicValsCostoRealTC As Dictionary(Of String, Double)
    Public vblnAcomVehic As Boolean
End Class
