﻿
Imports System.Data.SqlClient
Imports ComSEToursBE2
Imports ComSEToursBL2
Imports Moq
Imports NUnit.Framework


<TestFixture>
Public Class clsDocumentoProveedorBTTests

    <SetUp()>
    Public Sub RestaurarBD()
        'Para asegurar la integridad de los datos restauramos la BD

        Dim restore As New DatabaseRestore
        restore.restoreDB()

    End Sub


    <Test()>
    Public Sub TestsGrabarDocumentoOK()

        'System.Diagnostics.Debugger.Launch()

        Dim sut As New clsDocumentoProveedorBT
        Dim doc As clsDocumentoProveedorBE = Me.CrearDocumentoProveedorBE
        Dim sapMoq As New Mock(Of intJsonInterfaceBT)

        sapMoq.Setup(Sub(x) x.
                         EnviarDatosRESTFulJson(It.IsAny(Of Object), It.IsAny(Of String), It.IsAny(Of String), It.IsAny(Of String()))).Callback(
            Sub()
                Dim result = New ResponseStatusSAP()
                result.Id = "001"
                result.ErrMsg = Nothing
                modJson.objResponseSAP = result
            End Sub
        )

        sut.intJsonMaster = sapMoq.Object
        sut.Grabar(doc, True)

        Assert.That(doc.NuDocumProv, [Is].Not.EqualTo(0))

    End Sub

    <Test()>
    Public Sub TestsGrabarDocumentoFail()
        'Esta llamada permite debuggear el test
        'System.Diagnostics.Debugger.Launch()

        Dim sut As New clsDocumentoProveedorBT
        Dim doc As clsDocumentoProveedorBE = Me.CrearDocumentoProveedorBE
        Dim sapMoq As New Mock(Of intJsonInterfaceBT)

        sapMoq.Setup(Sub(x) x.
            EnviarDatosRESTFulJson(It.IsAny(Of Object), It.IsAny(Of String), It.IsAny(Of String), It.IsAny(Of String()))) _
            .Throws(Of ApplicationException)()


        sut.intJsonMaster = sapMoq.Object


        Assert.Throws(Of ApplicationException)(
            Sub()
                sut.Grabar(doc, True)
            End Sub)

    End Sub

    Private Function CrearDocumentoProveedorBE() As clsDocumentoProveedorBE
        Dim objBe = New clsDocumentoProveedorBE()
        objBe.AceptarPresupuestoTicket = False
        objBe.CardCodeSAP = "1"
        objBe.CoCeCon = "200101"
        objBe.CoCecos = "020104"
        objBe.CoCtaContab = "63111002"
        objBe.CoEstado = Nothing
        objBe.CoFormaPago = "012"
        objBe.CoGasto = Nothing
        objBe.CoMoneda = "USD"
        objBe.CoMoneda_FEgreso = "USD"
        objBe.CoMoneda_Pago = "USD"
        objBe.CoObligacionPago = "OTR"
        objBe.CoOrdPag = 0
        objBe.CoProvSetours = Nothing
        objBe.CoProveedor = "001720"
        objBe.CoTipDocSusEnt = Nothing
        objBe.CoTipoDetraccion = "00000"
        objBe.CoTipoDoc = "FAC"
        objBe.CoTipoDocSAP = 1
        objBe.CoTipoDocVta = Nothing
        objBe.CoTipoDoc_Ref = Nothing
        objBe.CoTipoFondoFijo = Nothing
        objBe.CoTipoOC = "001"
        objBe.CoUbigeo_Oficina = "000065"
        objBe.ErrorSAP = Nothing
        objBe.ErrorSetra = Nothing
        objBe.FeEmision = New DateTime(2016, 4, 1)
        objBe.FeEmision_Ref = New DateTime(1900, 1, 1)
        objBe.FeRecepcion = New DateTime(2016, 5, 9)
        objBe.FeVencimiento = New DateTime(2016, 5, 9)
        objBe.FecMod = New DateTime(1900, 1, 1)
        objBe.FlActivo = False
        objBe.FlMultiple = False
        objBe.IDCab = 0
        objBe.IDCab_FF = 0
        objBe.IDDocFormaEgreso = 0
        objBe.IDOperacion = 0
        objBe.IngresoManual = True
        objBe.NuDocInterno = "16011453"
        objBe.NuDocum = "123123"
        objBe.NuDocumProv = 0
        objBe.NuDocumSusEnt = Nothing
        objBe.NuDocumVta = Nothing
        objBe.NuDocum_Multiple = 0
        objBe.NuDocum_Ref = Nothing
        objBe.NuFondoFijo = 0
        objBe.NuOperBanSusEnt = Nothing
        objBe.NuOrdComInt = 0
        objBe.NuOrden_Servicio = 0
        objBe.NuPreSob = 0
        objBe.NuSerie = "001"
        objBe.NuSerie_Ref = Nothing
        objBe.NuVouOfiExtInt = 0
        objBe.NuVouPTrenInt = 0
        objBe.NuVoucher = 0
        objBe.SSPercepcion = 0.00
        objBe.SSTipoCambio = 0
        objBe.SSTotal = 118
        objBe.SSTotalOrdServ = 0
        objBe.SSTotalOriginal = 118
        objBe.SSTotal_MN = 0
        objBe.SsDetraccion = 0
        objBe.SsIGV = 18
        objBe.SsNeto = 100
        objBe.SsOtrCargos = 0
        objBe.SsTipoCambio_FEgreso = 2
        objBe.SsTotal_FEgreso = 236
        objBe.TxConcepto = ""
        objBe.UserMod = "0001"
        objBe.UserNuevo = "0001"
        Return objBe
    End Function


End Class


Public Class DatabaseRestore
    Dim con As SqlConnection
    Dim cmd As SqlCommand
    Dim dread As SqlDataReader


    Sub query(ByVal que As String)

        con = New SqlConnection("Data Source=.\SQLEXPRESS14;Initial Catalog=Master;Integrated Security=True;Persist Security Info=False;Pooling=False;MultipleActiveResultSets=False;Connect Timeout=60;Encrypt=False;TrustServerCertificate=True")
        con.Open()
        cmd = New SqlCommand(que, con)
        cmd.ExecuteNonQuery()
        con.Close()
    End Sub


    Public Sub restoreDB()
        'Restaura la BD desde un backup en disco
        'las rutas son fijas pero podrían hacerse configurables
        query("ALTER DATABASE Sandbox " +
              " SET OFFLINE WITH ROLLBACK IMMEDIATE; " +
              " DROP DATABASE Sandbox; " +
              " RESTORE FILELISTONLY " +
              " FROM disk='C:\temp\BDSETRABuilder_Test2.bak' " +
              " RESTORE DATABASE Sandbox " +
              " FROM disk='C:\temp\BDSETRABuilder_Test2.bak' " +
              " WITH REPLACE, " +
              " MOVE 'BDSETRA_Data' TO 'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS14\MSSQL\DATA\Sandbox_Data.mdf', " +
              " MOVE 'BDSETRA_Log' TO 'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS14\MSSQL\DATA\Sandbox_Log.ldf'; "
              )
    End Sub
End Class