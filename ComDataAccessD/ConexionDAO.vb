﻿Imports System.Configuration
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class ConexionDAO

    Inherits ServicedComponent
    Shared Function GetCnx() As String

        Dim strCnx As String = _
        ConfigurationManager.ConnectionStrings("MiConexion").ConnectionString

        If strCnx Is String.Empty Then
            Return String.Empty
        Else
            Return strCnx
        End If
    End Function

    Shared Function GetCnx2() As String

        Dim strCnx As String = _
        ConfigurationManager.ConnectionStrings("MiConexion2").ConnectionString

        If strCnx Is String.Empty Then
            Return String.Empty
        Else
            Return strCnx
        End If
    End Function

End Class
