﻿Imports System.Data.SqlClient
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

'<JustInTimeActivation(True), _
'Synchronization(SynchronizationOption.Required), _
'Transaction(TransactionOption.Required)> _
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDataAccessDT
    Inherits ServicedComponent

    Dim strConnect As String = ConexionDAO.GetCnx '& "Pooling=false;"
    'Dim strConnect As String = "Server=192.168.30.50;DataBase=BaseSP2;uid=sa;pwd=schwarze;"

    Public Function GetConnection() As SqlConnection

        Try
            Dim cnn As New SqlConnection(strConnect)
            cnn.Open()
            Return cnn
        Catch ex As Exception
            Throw
        Finally

        End Try
    End Function

    Public Function GetDataTable(ByVal vstrSP As String, _
                                 ByVal vdicNamesValues As Dictionary(Of String, String), _
                                 Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable

        Dim cnn As SqlConnection = Nothing

        Try
            'Conectar:
            If vCnn Is Nothing Then
                cnn = New SqlConnection(strConnect)
            Else
                cnn = vCnn
            End If

            Dim dt As New DataTable
            Using cmm As New SqlCommand(vstrSP, cnn)
                Dim par As SqlParameter
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        'For i As Int16 = 0 To vListNames.Count - 1
                        par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                        .Parameters.Add(par)
                    Next
                End With


                If vCnn Is Nothing Then cnn.Open()

                'dt.Load(cmm.ExecuteReader(CommandBehavior.CloseConnection))
                dt.Load(cmm.ExecuteReader())
            End Using

            Return dt
        Catch ex As Exception
            'Dim objErrType As Object = ex.GetType
            'If objErrType.Attributes = 1056769 Then
            '    SqlConnection.ClearPool(cnn)
            '    GoTo Conectar
            'Else
            '    Throw
            'End If
            Throw
        Finally
            If vCnn Is Nothing Then cnn.Close()
        End Try
    End Function

    Public Function GetDataReader(ByVal vstrSP As String, _
                                  ByVal vdicNamesValues As Dictionary(Of String, String), _
                                  Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlDataReader

        Try
            'Conectar:
            Dim cnn As SqlConnection
            Dim dr As SqlDataReader

            If vCnn Is Nothing Then
                cnn = New SqlConnection(strConnect)
            Else
                cnn = vCnn
            End If

            Using cmm As New SqlCommand(vstrSP, cnn)
                Dim par As SqlParameter
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        'For i As Int16 = 0 To vListNames.Count - 1
                        par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                        .Parameters.Add(par)
                    Next
                End With

                If vCnn Is Nothing Then cnn.Open()
                'dr = cmm.ExecuteReader(CommandBehavior.CloseConnection)
                dr = cmm.ExecuteReader()
            End Using

            Return dr
        Catch ex As Exception
            'ContextUtil.SetAbort()
            'Dim objErrType As Object = ex.GetType
            'If objErrType.Attributes = 1056769 Then
            '    SqlConnection.ClearPool(cnn)
            '    GoTo Conectar
            'Else
            '    Throw
            'End If
            Throw
        Finally
            'cnn.Close()
        End Try
    End Function

    Public Sub ExecuteSP(ByVal vstrSP As String, ByVal vdicNamesValues As Dictionary(Of String, String))

        Using cnn As New SqlConnection(strConnect)
            Using cmm As New SqlCommand(vstrSP, cnn)
                Dim par As SqlParameter
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                        .Parameters.Add(par)
                    Next
                End With

                Try
                    cnn.Open()
                    cmm.ExecuteNonQuery()

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                Finally
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try
            End Using
        End Using

    End Sub

    Public Function ExecuteSPOutput(ByVal vstrSP As String, ByVal vdicNamesValues As Dictionary(Of String, String)) As String
        Dim par As SqlParameter
        Dim parOut As SqlParameter = Nothing
        Dim strParOutputName As String, strParametroSalida As String

        Using cnn As New SqlConnection(strConnect)
            Using cmm As New SqlCommand(vstrSP, cnn)
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        If strNamVal.Key.Substring(1, 1) <> "p" Then
                            par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                            .Parameters.Add(par)
                            strParametroSalida = strNamVal.Key
                        Else
                            parOut = New SqlParameter(strNamVal.Key, SqlDbType.VarChar, 255)
                            .Parameters.Add(parOut)
                            '.Parameters.Add(parOut).Direction = ParameterDirection.Output
                            parOut.Direction = ParameterDirection.Output
                            strParOutputName = strNamVal.Key
                        End If
                    Next
                    '.Parameters.Add(New SqlClient.SqlParameter(strParametroSalida, SqlDbType.Char, 11))
                    '.Parameters(strParametroSalida).Direction = ParameterDirection.Output
                End With

                Try
                    cnn.Open()
                    cmm.ExecuteNonQuery()

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                Finally
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try

            End Using
        End Using

        Return parOut.Value
    End Function

    Public Function GetSPOutputValue(ByVal vstrSP As String, ByVal vdicNamesValues As Dictionary(Of String, String)) As String

        Dim par As SqlParameter
        Dim parOut As SqlParameter = Nothing
        Dim strParOutputName As String, strParametroSalida As String

        Using cnn As New SqlConnection(strConnect)
            Using cmm As New SqlCommand(vstrSP, cnn)
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        If strNamVal.Key.Substring(1, 1) <> "p" Then
                            par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                            .Parameters.Add(par)
                            strParametroSalida = strNamVal.Key
                        Else
                            parOut = New SqlParameter(strNamVal.Key, SqlDbType.VarChar, 255)
                            .Parameters.Add(parOut)
                            '.Parameters.Add(parOut).Direction = ParameterDirection.Output
                            parOut.Direction = ParameterDirection.Output
                            strParOutputName = strNamVal.Key
                        End If
                    Next
                    '.Parameters.Add(New SqlClient.SqlParameter(strParametroSalida, SqlDbType.Char, 11))
                    '.Parameters(strParametroSalida).Direction = ParameterDirection.Output
                End With

                Try
                    cnn.Open()
                    cmm.ExecuteNonQuery()
                Catch ex As Exception
                    Throw
                Finally
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try

            End Using
        End Using

        Return parOut.Value
    End Function
End Class
