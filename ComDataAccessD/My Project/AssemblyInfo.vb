﻿Imports System
Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.EnterpriseServices

' La información general sobre un ensamblado se controla mediante el siguiente 
' conjunto de atributos. Cambie estos atributos para modificar la información
' asociada con un ensamblado.

' Revisar los valores de los atributos del ensamblado

<Assembly: AssemblyTitle("ComDataAccessD2")> 
<Assembly: AssemblyDescription("")> 
<Assembly: AssemblyCompany("")> 
<Assembly: AssemblyProduct("ComDataAccessD2")> 
<Assembly: AssemblyCopyright("Copyright ©  2016")> 
<Assembly: AssemblyTrademark("")> 

<Assembly: ComVisible(True)> 

'El siguiente GUID sirve como identificador de typelib si este proyecto se expone a COM
'<Assembly: Guid("fccf37dd-7175-45f5-bbde-928e8389caa8")> 
<Assembly: Guid("BC51822D-1647-4FDE-AAB3-87BA824C2B29")> 

' La información de versión de un ensamblado consta de los cuatro valores siguientes:
'
'      Versión principal
'      Versión secundaria 
'      Número de compilación
'      Revisión
'
' Puede especificar todos los valores o usar los valores predeterminados de número de compilación y de revisión 
' mediante el asterisco ('*'), como se muestra a continuación:
' <Assembly: AssemblyVersion("1.0.*")> 

<Assembly: AssemblyVersion("1.0.0.0")> 
<Assembly: AssemblyFileVersion("1.0.0.0")> 
<Assembly: ApplicationName("SETRA_Testing")> 
<Assembly: ApplicationActivation(ActivationOption.Library)> 
<Assembly: ApplicationAccessControl(True)> 