﻿Imports System.Data.SqlClient
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDataAccessDN
    Inherits ServicedComponent

    Dim strConnect As String = ConexionDAO.GetCnx & "Pooling=false;"
    'Dim strConnect As String = "Server=192.168.30.50;DataBase=BaseSP2;uid=sa;pwd=schwarze;"

    Public Function GetConnection() As SqlConnection

        Try
            Dim cnn As New SqlConnection(strConnect)
            cnn.Open()
            Return cnn
        Catch ex As Exception
            Throw
        Finally

        End Try
    End Function

    Public Function GetDataTable(ByVal vblnSP As Boolean, _
                                 ByVal vstrSP As String, _
                                 ByVal vdicNamesValues As Dictionary(Of String, String), _
                                 Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable

        Dim cnn As SqlConnection = Nothing

        Try
            'cnn.ClearPool(cnn)
            'Conectar:
            If vCnn Is Nothing Then
                cnn = New SqlConnection(strConnect)
            Else
                cnn = vCnn
            End If

            Dim dt As New DataTable
            Using cmm As New SqlCommand(vstrSP, cnn)
                Dim par As SqlParameter
                With cmm
                    If vblnSP Then .CommandType = CommandType.StoredProcedure

                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                        .Parameters.Add(par)
                    Next
                End With

                If vCnn Is Nothing Then cnn.Open()
                'jorge
                'cmm.CommandTimeout = 0
                'dt.Load(cmm.ExecuteReader(CommandBehavior.CloseConnection))
                dt.Load(cmm.ExecuteReader())
            End Using

            Return dt
        Catch ex As Exception

            'Dim objErrType As Object = ex.GetType
            'If objErrType.Attributes = 1056769 Then
            '    SqlConnection.ClearPool(cnn)
            '    GoTo Conectar
            'Else
            '    Throw
            'End If
            Throw
        Finally
            If vCnn Is Nothing Then cnn.Close()

        End Try
    End Function

    Public Function GetDataReader(ByVal vstrSP As String, _
                                  ByVal vdicNamesValues As Dictionary(Of String, String), _
                                  Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlDataReader

        Try
            'Conectar:
            Dim cnn As SqlConnection
            Dim dr As SqlDataReader

            If vCnn Is Nothing Then
                cnn = New SqlConnection(strConnect)
            Else
                cnn = vCnn
            End If

            Using cmm As New SqlCommand(vstrSP, cnn)
                Dim par As SqlParameter
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                        .Parameters.Add(par)
                    Next
                End With

                If vCnn Is Nothing Then cnn.Open()
                'jorge
                'cmm.CommandTimeout = 900
                dr = cmm.ExecuteReader(CommandBehavior.CloseConnection)
                'dr = cmm.ExecuteReader()
            End Using

            Return dr
        Catch ex As Exception
            'Dim objErrType As Object = ex.GetType
            'If objErrType.Attributes = 1056769 Then
            '    SqlConnection.ClearPool(cnn)
            '    GoTo Conectar
            'Else
            '    Throw
            'End If
            Throw
        Finally
            'cnn.Close()
        End Try
    End Function

    Public Sub ExecuteSP(ByVal vstrSP As String, ByVal vdicNamesValues As Dictionary(Of String, String))

        Using cnn As New SqlConnection(strConnect)
            Using cmm As New SqlCommand(vstrSP, cnn)
                Dim par As SqlParameter
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                        .Parameters.Add(par)
                    Next
                End With

                Try
                    cnn.Open()
                    cmm.ExecuteNonQuery()
                Catch ex As Exception
                    Throw
                Finally
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try
            End Using
        End Using

    End Sub

    Public Function GetSPOutputValue(ByVal vstrSP As String, ByVal vdicNamesValues As Dictionary(Of String, String)) As String


        Dim par As SqlParameter
        Dim parOut As SqlParameter = Nothing
        Dim strParOutputName As String, strParametroSalida As String

        Using cnn As New SqlConnection(strConnect)
            Using cmm As New SqlCommand(vstrSP, cnn)
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        If strNamVal.Key.Substring(1, 1) <> "p" Then
                            par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                            .Parameters.Add(par)
                            strParametroSalida = strNamVal.Key
                        Else
                            parOut = New SqlParameter(strNamVal.Key, SqlDbType.VarChar, -1)
                            .Parameters.Add(parOut)
                            '.Parameters.Add(parOut).Direction = ParameterDirection.Output
                            parOut.Direction = ParameterDirection.Output
                            strParOutputName = strNamVal.Key
                        End If
                    Next
                    '.Parameters.Add(New SqlClient.SqlParameter(strParametroSalida, SqlDbType.Char, 11))
                    '.Parameters(strParametroSalida).Direction = ParameterDirection.Output
                End With

                Try
                    cnn.Open()

                    cmm.ExecuteNonQuery()
                Catch ex As Exception
                    Throw
                Finally
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try
                'Return cmm.Parameters(strParOutputName).Value
            End Using
        End Using

        Return parOut.Value

    End Function

    Public Function GetDataSet(ByVal vstrSPParent As String, _
                               ByVal vstrSPChild As String, ByVal vstrField As String, _
                               Optional ByVal vstrSPChild2 As String = "", _
                               Optional ByVal vstrField2 As String = "", _
                               Optional ByVal vstrCriterio As String = "", _
                               Optional ByVal vblnChild2 As Boolean = False) As DataSet


        'Dim cnn As New SqlConnection("server=(local);Integrated Security = true;Initial Catalog=NorthWind")
        'Dim cmd As New SqlCommand(NombreTabla + "_select_all", cnn)
        'cmd.CommandType = CommandType.StoredProcedure

        'Dim ds As New DataSet
        'Dim i As Integer

        'Dim da As New SqlDataAdapter(cmd)
        'da.Fill(ds, "Orders")

        'Dim oArray() As String

        'If ds.Tables.Count > 1 Then
        '    If ds.Tables(ds.Tables.Count - 1).Rows(0).ItemArray(0).ToString() = "collection" Then
        '        oArray = Split(ds.Tables(ds.Tables.Count - 1).Rows(0).ItemArray(1).ToString(), ";")
        '        For i = 0 To ds.Tables.Count - 2
        '            ds.Tables(i).TableName = oArray(i).ToString()
        '        Next
        '    End If
        'End If

        'Return ds


        Using cnn As New SqlConnection(strConnect)
            Using ds As New DataSet

                Dim dt2 As DataTable
                Dim daParent As New SqlDataAdapter(vstrSPParent, cnn)
                'Dim daChild As New SqlDataAdapter(vstrSPChild, cnn)

                Dim daChild2 As SqlDataAdapter
                Dim dt As DataTable

                cnn.Open()

                If vstrCriterio = "" Then
                    daParent.Fill(ds, "dtParent")
                Else
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add(vstrField, vstrCriterio)
                    dt = GetDataTable(True, vstrSPParent, dc)
                    dt.TableName = "dtParent"
                    ds.Tables.Add(dt)
                End If
                'daChild.Fill(ds, "dtChild")

                Dim dc2 As New Dictionary(Of String, String)
                dc2.Add(vstrField, vstrCriterio)
                dc2.Add("@bDir", 1)
                dt2 = GetDataTable(True, vstrSPChild, dc2)
                dt2.TableName = "dtChild"
                ds.Tables.Add(dt2)

                If vstrSPChild2 <> "" Then
                    daChild2 = New SqlDataAdapter(vstrSPChild2, cnn)
                    daChild2.Fill(ds, "dtChild2")

                End If


                cnn.Close()

                If dt2.Rows.Count > 0 Then
                    ds.Relations.Add("ParentChild", ds.Tables("dtParent").Columns(vstrField), ds.Tables("dtChild").Columns(vstrField))


                    'If vblnChild2 Then
                    '    Dim dc3 As New Dictionary(Of String, String)
                    '    Dim dt3 As DataTable
                    '    dc3.Add(vstrField, vstrCriterio)
                    '    dt3 = GetDataTable(True, vstrSPChild2, dc3)
                    '    If dt3.Rows.Count > 0 Then
                    '        ds.Relations.Add("ParentChildOpc", ds.Tables("dtParent").Columns(vstrField), ds.Tables("dtChild2").Columns(vstrField))
                    '    End If
                    'End If

                    If vstrSPChild2 <> "" Then


                        Dim dc3 As New Dictionary(Of String, String)
                        Dim dt3 As DataTable
                        dc3.Add(vstrField, vstrCriterio)
                        dc3.Add("@bDir", 0)
                        dt3 = GetDataTable(True, vstrSPChild, dc3)
                        If dt3.Rows.Count > 0 Then
                            'ds.Relations.Add("ParentChildOpc", ds.Tables("dtParent").Columns(vstrField), ds.Tables("dtChild2").Columns(vstrField))
                            ds.Relations.Add("ChildGrandson", ds.Tables("dtChild").Columns(vstrField2), ds.Tables("dtChild2").Columns(vstrField2))
                        End If



                        'ds.Relations.Add("ChildGrandson", ds.Tables("dtChild").Columns(vstrField2), ds.Tables("dtChild2").Columns(vstrField2))

                    End If
                End If

                Return ds
            End Using
        End Using
    End Function

    'jorge
    Public Function GetDataSet2(ByVal NombreTabla As String, ByVal vblnSP As Boolean, _
                                 ByVal vstrSP As String, _
                                 ByVal vdicNamesValues As Dictionary(Of String, String)) As DataSet

        Dim cnn As New SqlConnection(strConnect)
        Dim cmd As New SqlCommand(vstrSP, cnn)
        'cmd.CommandType = CommandType.StoredProcedure

        If vblnSP Then cmd.CommandType = CommandType.StoredProcedure
        Dim par As SqlParameter
        For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
            par = New SqlParameter(strNamVal.Key, strNamVal.Value)
            cmd.Parameters.Add(par)
        Next

        Dim ds As New DataSet
        'Dim i As Integer

        'cmd.CommandTimeout = 900
        Try
            Dim da As New SqlDataAdapter(cmd)
            da.Fill(ds, NombreTabla)
        Catch ex As Exception
            Throw
        Finally
            If cnn.State = ConnectionState.Open Then
                cnn.Close()
            End If
        End Try

        'Dim da As New SqlDataAdapter(cmd)
        'da.Fill(ds, NombreTabla)

        'Dim oArray() As String

        'If ds.Tables.Count > 1 Then
        '    If ds.Tables(ds.Tables.Count - 1).Rows(0).ItemArray(0).ToString() = "collection" Then
        '        oArray = Split(ds.Tables(ds.Tables.Count - 1).Rows(0).ItemArray(1).ToString(), ";")
        '        For i = 0 To ds.Tables.Count - 2
        '            ds.Tables(i).TableName = oArray(i).ToString()
        '        Next
        '    End If
        'End If

        Return ds

    End Function


    Public Function ExecuteSPOutput(ByVal vstrSP As String, ByVal vdicNamesValues As Dictionary(Of String, String)) As String

        Dim par As SqlParameter
        Dim parOut As SqlParameter = Nothing
        Dim strParOutputName As String, strParametroSalida As String

        Using cnn As New SqlConnection(strConnect)
            Using cmm As New SqlCommand(vstrSP, cnn)
                With cmm
                    .CommandType = CommandType.StoredProcedure
                    For Each strNamVal As KeyValuePair(Of String, String) In vdicNamesValues
                        If strNamVal.Key.Substring(1, 1) <> "p" Then
                            par = New SqlParameter(strNamVal.Key, strNamVal.Value)
                            .Parameters.Add(par)
                            strParametroSalida = strNamVal.Key
                        Else
                            parOut = New SqlParameter(strNamVal.Key, SqlDbType.VarChar, 255)
                            .Parameters.Add(parOut)
                            '.Parameters.Add(parOut).Direction = ParameterDirection.Output
                            parOut.Direction = ParameterDirection.Output
                            strParOutputName = strNamVal.Key
                        End If

                    Next
                    '.Parameters.Add(New SqlClient.SqlParameter(strParametroSalida, SqlDbType.Char, 11))
                    '.Parameters(strParametroSalida).Direction = ParameterDirection.Output
                End With

                Try
                    cnn.Open()

                    cmm.ExecuteNonQuery()
                Catch ex As Exception
                    Throw
                Finally
                    If cnn.State = ConnectionState.Open Then
                        cnn.Close()
                    End If
                End Try

            End Using
        End Using
        'Return cmm.Parameters(strParOutputName).Value
        Return parOut.Value

    End Function
End Class
