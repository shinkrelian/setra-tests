﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsOperacionBaseBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN

    Public Overridable Function ConsultarPk(ByVal vintIDReserva As Integer) As SqlClient.SqlDataReader
        Return Nothing
    End Function

    Public Overridable Function ConsultarList() As DataTable
        Return Nothing
    End Function

    Public Overridable Function ConsultarListxIDCab(ByVal vintIDCab As Integer) As DataTable
        Return Nothing
    End Function

    Public Overridable Function ConsultarxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Return Nothing
    End Function

    Public Overridable Function ConsultarxID() As SqlClient.SqlDataReader
        Return Nothing
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOperacionBN
    Inherits clsOperacionBaseBN

    Public Function ConsultarListxIDCab_HotelyEspc(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_Sel_ListxIDCabHotelyEspc", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarListxIDCab_NoHoteles(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_Sel_ListxIDCabNoHoteles", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function intDevuelveCorrelativo() As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Tabla", "VOUCHER_OPERACIONES")
            dc.Add("@Upd", 0)
            dc.Add("@Tamanio", 10)
            dc.Add("@pCorrelativo", 0)

            Return objADO.ExecuteSPOutput("Correlativo_SelOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDOperacion_RecepcionDocs(ByVal vintIDOperacion As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion", vintIDOperacion)

            Return objADO.GetDataTable(True, "OPERACIONES_SelCambioReserva_RecepcionDocumentos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteProveedor(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As Boolean

        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@pbExists", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_SelxIDProveedorOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteProveedoresExternosxFile(ByVal vintIDCab As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pbExists", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_SelProvExternosxIDCabExistsOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenCambiosPendientesReservas(ByVal vintIDCab As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pExtCambioPend", False)

            Return objADO.ExecuteSPOutput("RESERVAS_ExistenCambiosPendientes", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteDetalle(ByVal vintIDCab As Integer, ByVal vstrIDIdioma As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDIdioma", vstrIDIdioma)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxIDCabRpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarBookingConfirmation(ByVal vintIDCab As Integer, ByVal vstrIDIdioma As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDIdioma", vstrIDIdioma.Trim)

            Return objADO.GetDataTable(True, "COTIDET_Sel_RptBookingConfirmation", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarBookingVoucher_Vuelos(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "COTIVUELOS_ListBookingVoucher", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteHotelxDia(ByVal vintIDCab As Integer, ByVal vdatDiaServicio As Date, _
                                              ByVal vstrIDIdioma As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@DiaServicio", vdatDiaServicio)
            'dc.Add("@NombreDia", vstrNombreDias)
            dc.Add("@IDIdioma", vstrIDIdioma)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelHotelxRangoDias", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteServiciosHijosxIDDet(ByVal vintIDDet As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelCotidetxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteCambiosReserva(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As Boolean
        Try
            Dim blnExiste As Boolean = False
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@pCambiosenReservasPostFile", False)

            blnExiste = objADO.GetSPOutputValue("OPERACIONES_SelCambioReservaOutPut", dc)

            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenVouchers(ByVal vintIDOperacion As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("OPERACIONES_DET_SelExisteVoucherxIDOperacionOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenOrdenesServicio(ByVal vintIDCab As Integer, ByVal vintIDOperacion As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("ORDEN_SERVICIO_SelExistexIDOperacionOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExistenVouchersxIDCab(ByVal vintIDCab As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("OPERACIONES_DET_SelExisteVoucherxIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function blnExistenOrdenesServicioxIDCab(ByVal vintIDCab As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("ORDEN_SERVICIO_SelExistexIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteVoucherxIDOperacion(ByVal vintIDOperacion As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("VOUCHER_OPERACIONES_Sel_ExistexIDOperacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenVouchersuOrdenesServicioxIDCab(ByVal vintIDCab As Integer) As Boolean
        Try

            If Not blnExistenVouchersxIDCab(vintIDCab) Then
                Return blnExistenOrdenesServicioxIDCab(vintIDCab)
            Else
                Return True
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteHoteles(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelHotelesxIDCab_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

   

    Public Function ConsultarBookingHoteles(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "COTIDET_SelHotelesxIDCab_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteOperadores(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelOperadoresxIDCab_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteNoShowxFile(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "Reporte_NoShowxFile", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteNoShowxFechas(ByVal vdatFecIni As Date, ByVal vdatFecFin As Date) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@FecIni", vdatFecIni)
            dc.Add("@FecFin", vdatFecFin)

            Return objADO.GetDataTable(True, "Reporte_NoShowxFechas", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarBookingOperadores(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "COTIDET_SelOperadoresxIDCab_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteGuias(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_Sel_GuiasxIDCab_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxIDCab_Listar_Oper_Internos(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_Sel_x_IDCab_Oper_Internos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenOperacionesxIDCab(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_Sel_ExistenxIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function blnExistenOperacionesProvExternosxIDCab(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_Sel_ExistenProvExternosxIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function blnExistenOperacionesxIDReserva(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_Sel_ExistenxIDReservaOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxIDReserva(ByVal vintIDReserva As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDReserva", vintIDReserva)

        Return objADO.GetDataReader("OPERACIONES_SelxIDReserva", dc)
    End Function
    Public Overrides Function ConsultarListxIDCab(ByVal vintIDCab As Integer) As DataTable
        Return Nothing
    End Function

    'Public Function ConsultarLogReservas(ByVal vintIDCab As Integer, _
    '                                     ByVal vstrIDProveedor As String, _
    '                                     ByVal vchrAccion As Char) As DataTable
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDProveedor", vstrIDProveedor)
    '        dc.Add("@Accion", vchrAccion)

    '        Return objADO.GetDataTable(True, "RESERVAS_DET_LOG_SelxIDProveedor", dc)

    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    'Public Function blnExisteLogxAccion(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, ByVal vchrAccion As Char) As Boolean
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDProveedor", vstrIDProveedor)
    '        dc.Add("@Accion", vchrAccion)
    '        dc.Add("@pExists", 0)

    '        Return objADO.ExecuteSPOutput("RESERVAS_DET_LOG_SelExiste_Output", dc)

    '    Catch ex As Exception
    '        Throw
    '    End Try


    'End Function

    Public Function strTextoSolicitudxProveedoryTipo(ByVal vstrIDProveedor As String, ByVal vstrIDTipoProv As String) As String
        Try
            Dim strReturn As String = ""
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@CoTipoProv", vstrIDTipoProv)

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("TEXTO_SOLICITUD_SERVICIO_SelTexto", dc)
                If dr.HasRows Then
                    dr.Read()
                    strReturn = dr("TxTexto")
                End If
                dr.Close()
            End Using

            Return strReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleOperacionBN
    Inherits clsOperacionBN

    Public Overrides Function ConsultarListxIDCab(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxIDCab", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overrides Function ConsultarxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataReader("OPERACIONES_DET_SelxIDCab2", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistePresupuesto(ByVal vintIDOperacion_Det As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@pExistePresupuesto", False)

            Return objADO.GetSPOutputValue("OPERACIONES_DET_Sel_ExistPresupuesto", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteProvProgramadoOf(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                              ByVal vstrIDProveedorPrg As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDProvProgram", vstrIDProveedorPrg)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("OPERACIONES_DET_DETSERVICIOS_Sel_ExisteEnOtraOficina", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'Public Function ConsultarDetallexProveedor(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer, ByVal vstrIDProSetours As String) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@IDProveedor", vstrIDProveedor)
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDProSetours", vstrIDProSetours)

    '        Return objADO.GetDataTable(True, "OPERACIONES_DET_Sel_ListxIDProveedorxIDCab", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    'Public Function ConsultarSubDetallexProveedor(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer, _
    '                                              ByVal vstrIDProSetours As String) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@IDProveedor", vstrIDProveedor)
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDProSetours", vstrIDProSetours)

    '        Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_SelxIDProveedorxIDCab", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    'Public Function ConsultarTransportistasxIDProveedorxIDCab(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer, _
    '                                                          ByVal vstrIDSetours As String) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        With dc
    '            .Add("@IDProveedorGuia", vstrIDProveedor)
    '            .Add("@IDCab", vintIDCab)
    '            .Add("@IDProSetours", vstrIDSetours)
    '        End With
    '        Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_SelxListTransportista", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    'Public Function ConsultarGuiasxIDProveedorxIDCab(ByVal vintIDCab As Integer, _
    '                                                 ByVal vstrIDSetours As String) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        With dc
    '            .Add("@IDCab", vintIDCab)
    '            .Add("@IDProSetours", vstrIDSetours)
    '        End With
    '        Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_SelxListGuias", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function
    Private Function ConsultarPkCorreoSolicitud(ByVal vintIDOperacion_Det As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)

            Return objADO.GetDataReader("OPERACIONES_DET_Sel_CorreoSolicitud", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarOrdenServicioPDF(ByVal vstrIDservicio As String, ByVal vintIDOperacion_Det As Integer, _
                                              ByVal vstrIDEstadoSolicitud As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", vstrIDservicio)
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDProveedor_Prg", vstrIDEstadoSolicitud)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_GeneracionOrdenServiciosPDF", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxProvInt(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer, ByVal vblnServiciosTC As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IdCab", vintIDCab)
            dc.Add("@FlServicioTC", vblnServiciosTC)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_List_ServiciosxProvInt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxProvInt2(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IdCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_List_ServiciosxProvInt_OrdenServicio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxProvIntTC(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IdCab", vintIDCab)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_List_ServiciosxProvInt_OrdenServicioTC", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDCabIDProveedor(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxIDCabIDProveedor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarListxIDCabIDProveedorTMP(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_TMP_SelxIDCabIDProveedor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtDevuelveDatosAcomodosEspeciales(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataTable(True, "COTIPAX_Sel_ListxIDCabxAcomodoEspecial_Mail", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    ' ''Public Function strDevuelveHTMLPaxAcomodoEspecial(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As String
    ' ''    Try
    ' ''        Dim dc As New Dictionary(Of String, String)
    ' ''        dc.Add("@IDCab", vintIDCab)
    ' ''        dc.Add("@IDProveedor", vstrIDProveedor)
    ' ''        Dim bln1erRegistro As Boolean = True
    ' ''        Dim strLine As String = ""

    ' ''        Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIPAX_Sel_ListxIDCabxAcomodoEspecial_Mail", dc)
    ' ''            While dr.Read
    ' ''                If bln1erRegistro Then
    ' ''                    strLine += "<tr>" & vbCrLf
    ' ''                End If
    ' ''                bln1erRegistro = False

    ' ''                Dim strTD As String = "<td align='left'>"
    ' ''                strLine += strTD & dr("NroOrd").ToString & "</td>"

    ' ''                strTD = "<td align='left'>"
    ' ''                Dim strNombre As String = dr("Titulo") & " " & dr("Nombres") & " " & dr("Apellidos")
    ' ''                strLine += strTD & strNombre & "</td>" & vbCrLf

    ' ''                strTD = "<td align='left'>"
    ' ''                strLine += strTD & dr("DescIdentidad") & ": " & dr("NumIdentidad") & "</td>" & vbCrLf

    ' ''                strTD = "<td align='left'>"
    ' ''                strLine += strTD & dr("DescNacionalidad") & "</td>" & vbCrLf

    ' ''                strTD = "<td align='left'>"
    ' ''                strLine += strTD & dr("FecNacimiento").ToString & "</td>" & vbCrLf

    ' ''                'If vblnEsProveedorHotel Then
    ' ''                strTD = "<td align='left'>"
    ' ''                strLine += strTD & dr("ACC").ToString & "</td>" & vbCrLf
    ' ''                'End If

    ' ''                strTD = "<td align='left'>"
    ' ''                strLine += strTD & dr("ObservEspecial").ToString & "</td>" & vbCrLf

    ' ''                strLine += "</tr>" & vbCrLf
    ' ''            End While
    ' ''            dr.Close()
    ' ''        End Using
    ' ''        Return strLine
    ' ''    Catch ex As Exception
    ' ''        Throw
    ' ''    End Try
    ' ''End Function

    Public Function strDevuelveHTMLPax(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, _
                                       ByVal vblnEsProveedorHotel As Boolean) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCAB", vintIDCab)
            dc.Add("@IDReserva", vintIDReserva)

            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIPAX_Sel_ListxIDCab_Mail", dc)
                While dr.Read
                    If bln1erRegistro Then
                        strLine += "<tr>" & vbCrLf
                    End If
                    bln1erRegistro = False

                    Dim strTD As String = "<td align='left'>"
                    strLine += strTD & dr("NroOrd").ToString & "</td>"

                    strTD = "<td align='left'>"
                    Dim strNombre As String = dr("Titulo") & " " & dr("Nombres") & " " & dr("Apellidos")
                    strLine += strTD & strNombre & "</td>" & vbCrLf

                    strTD = "<td align='left'>"
                    strLine += strTD & dr("DescIdentidad") & ": " & dr("NumIdentidad") & "</td>" & vbCrLf

                    strTD = "<td align='left'>"
                    strLine += strTD & dr("DescNacionalidad") & "</td>" & vbCrLf

                    strTD = "<td align='left'>"
                    strLine += strTD & dr("FecNacimiento").ToString & "</td>" & vbCrLf

                    If vblnEsProveedorHotel Then
                        strTD = "<td align='left'>"
                        strLine += strTD & dr("ACC").ToString & "</td>" & vbCrLf
                    End If

                    strTD = "<td align='left'>"
                    strLine += strTD & dr("ObservEspecial").ToString & "</td>" & vbCrLf

                    strTD = "<td align='left'>"
                    strLine += strTD & dr("DescTC").ToString & "</td>" & vbCrLf

                    strLine += "</tr>" & vbCrLf
                End While
                dr.Close()
            End Using

            Return strLine

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strCargarTitulosListadoPax(ByVal vblnEsProveedorHotel As Boolean) As String
        Try
            Dim strLine As String = ""
            strLine += vbCrLf
            strLine += ("Lista de Pax" & vbCrLf).ToString
            strLine += (StrDup(114, "*") & vbCrLf).ToString
            strLine += strCampoCorreoTexto(7, "")
            strLine += strCampoCorreoTexto(45, "")
            strLine += strCampoCorreoTexto(19, "Doc. Ident.")
            strLine += strCampoCorreoTexto(4, "Nat")
            strLine += strCampoCorreoTexto(11, "DOB")
            If vblnEsProveedorHotel Then
                strLine += strCampoCorreoTexto(7, "ACC")
                strLine += strCampoCorreoTexto(18, "OBS") '& vbCrLf
            Else
                strLine += strCampoCorreoTexto(25, "OBS") '& vbCrLf
            End If
            strLine += strCampoCorreoTexto(3, "TC") & vbCrLf
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strCargarListaPaxxIDDetxCapacidadxEsMatrimonial(ByVal vintIDDet As Integer, ByVal vBytCapacidad As Byte, _
                                                                     ByVal vblnEsMatrimonial As Boolean) As String
        Try
            Dim strLine As String = ""
            Dim objDet As New clsCotizacionBN.clsDetalleCotizacionBN
            Dim dttListaPax As DataTable = objDet.ConsultarPaxAcomodoEspecial(vintIDDet)

            Dim strCapacidad As String = vBytCapacidad.ToString
            Dim strEsMatrimonial As String = If(vblnEsMatrimonial = True, "1", "0")
            Dim blnCargoTitulo As Boolean = False

            Dim dttFiltrado As Data.DataTable = dtFiltrarDataTable(dttListaPax, "QtCapacidad = '" & strCapacidad & "' and EsMatrimonial = '" & strEsMatrimonial & "'")
            For Each dRow As DataRow In dttFiltrado.Rows
                Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("NombrePax").ToString
                strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                strLine += strCampoCorreoTexto(49, strNombre)
                strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                strLine += strCampoCorreoTexto(4, dRow("NacionalidadPax").ToString)
                strLine += strCampoCorreoTexto(11, dRow("FechNacimiento"))
                strLine += strCampoCorreoTexto(7, dRow("ACC"))
                strLine += strCampoCorreoTexto(21, dRow("ObservEspecial")) & vbCrLf
                blnCargoTitulo = True
            Next

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    'RESERVAS_DET_Sel_PaxxIDReservaDet
    Private Function strCargarListaPaxxIDReservaDetxCapacidadxEsMatrimonial(ByVal vintIDReservaDet As Integer, ByVal vBytCapacidad As Byte, _
                                                                     ByVal vblnEsMatrimonial As Boolean) As String
        Try
            Dim strLine As String = ""
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva_Det", vintIDReservaDet)
            Dim dttListaPax As DataTable = objADO.GetDataTable(True, "RESERVAS_DET_Sel_PaxxIDReservaDet", dc)

            Dim strCapacidad As String = vBytCapacidad.ToString
            Dim strEsMatrimonial As String = If(vblnEsMatrimonial = True, "1", "0")
            Dim blnCargoTitulo As Boolean = False

            Dim dttFiltrado As Data.DataTable = dtFiltrarDataTable(dttListaPax, "CapacidadHab = '" & strCapacidad & "' and EsMatrimonial = '" & strEsMatrimonial & "'")
            For Each dRow As DataRow In dttFiltrado.Rows
                Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("Nombres").ToString & " " & dRow("Apellidos").ToString
                strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                strLine += strCampoCorreoTexto(45, strNombre)
                strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                strLine += strCampoCorreoTexto(4, dRow("DescNacionalidad").ToString)
                strLine += strCampoCorreoTexto(11, dRow("FecNacimiento"))
                strLine += strCampoCorreoTexto(7, dRow("ACC"))
                strLine += strCampoCorreoTexto(18, dRow("ObservEspecial")) '& vbCrLf
                strLine += strCampoCorreoTexto(18, dRow("DescTC")) & vbCrLf
                blnCargoTitulo = True
            Next

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strCargarListaPaxxIDReservaxCapacidadxEsMatrimonial(ByVal vintIDReserva As Integer, ByVal vBytCapacidad As Byte, _
                                                                     ByVal vblnEsMatrimonial As Boolean) As String
        Try
            Dim strLine As String = ""
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            Dim dttListaPax As DataTable = objADO.GetDataTable(True, "RESERVAS_Sel_PaxxAcomodo", dc)

            Dim strCapacidad As String = vBytCapacidad.ToString
            Dim strEsMatrimonial As String = If(vblnEsMatrimonial = True, "1", "0")
            Dim blnCargoTitulo As Boolean = False

            Dim dttFiltrado As Data.DataTable = dtFiltrarDataTable(dttListaPax, "CapacidadHab = '" & strCapacidad & "' and EsMatrimonial = '" & strEsMatrimonial & "'")
            For Each dRow As DataRow In dttFiltrado.Rows
                Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("Nombres").ToString & " " & dRow("Apellidos").ToString
                strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                strLine += strCampoCorreoTexto(49, strNombre)
                strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                strLine += strCampoCorreoTexto(4, dRow("DescNacionalidad").ToString)
                strLine += strCampoCorreoTexto(11, dRow("FecNacimiento"))
                strLine += strCampoCorreoTexto(7, dRow("ACC"))
                strLine += strCampoCorreoTexto(18, dRow("ObservEspecial")) '& vbCrLf
                strLine += strCampoCorreoTexto(3, dRow("DescTC")) & vbCrLf
                blnCargoTitulo = True
            Next

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function bytDevuelveCapacidadxCadena(ByVal vstrCadenaDescrip As String) As Byte
        Try
            Dim bytReturn As Byte = 0
            If InStr(vstrCadenaDescrip, "SINGLE") > 0 Then
                bytReturn = 1
            ElseIf InStr(vstrCadenaDescrip, "DOUBLE (MAT)") > 0 Or InStr(vstrCadenaDescrip, "TWIN") > 0 Then
                bytReturn = 2
                If InStr(vstrCadenaDescrip, "TWIN + (EXCHILD)") > 0 Then
                    bytReturn = 3
                End If
            ElseIf InStr(vstrCadenaDescrip, "TRIPLE") > 0 Then
                bytReturn = 3
            ElseIf InStr(vstrCadenaDescrip, "CUADRUPLE") > 0 Then
                bytReturn = 4
            ElseIf InStr(vstrCadenaDescrip, "QUINTUPLE") > 0 Then
                bytReturn = 5
            ElseIf InStr(vstrCadenaDescrip, "SEXTUPLE") > 0 Then
                bytReturn = 6
            ElseIf InStr(vstrCadenaDescrip, "SEPTUPLE") > 0 Then
                bytReturn = 7
            ElseIf InStr(vstrCadenaDescrip, "OCTUPLE") > 0 Then
                bytReturn = 8
            End If
            Return bytReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoPaxAcomodoEspecialHTML(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, _
                                                      ByVal vstrIDProveedor As String)
        Dim strLine As String = ""
        Try
            Dim strTitulos As String = strCargarTitulosListadoPax(True)
            strLine += strTitulos

            Dim dtDatosComparados As New DataTable
            With dtDatosComparados
                .Columns.Add("IDDet")
                .Columns.Add("FechaIn")
                .Columns.Add("FechaOut")
                .Columns.Add("AcomodoCab")
            End With

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            'Dim dtServiciosEspCoti As Data.DataTable = objADO.GetDataTable(True, "COTIDET_SelServiciosxAcomodoEspecial", dcGen)
            Dim dtServiciosReserva As Data.DataTable = objADO.GetDataTable(True, "RESERVAS_DET_SelServiciosxAcomodoEspecial", dc)
            Dim objBL As New clsReservaBN.clsAcomodoPaxProveedorBN
            'Dim objBLC As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
            Dim DtAcomodoIng As DataTable = dtDevuelvePaxAcomodos(vintIDCab, vintIDReserva) 'objBL.ConsultarListxIDReserva(vintIDReserva)

            'Union de detalles
            For Each Item As DataRow In dtServiciosReserva.Rows
                Dim strAcomodos As String = "", strFechaInOutCab As String = Item("FechaIn").ToString & "|" & Item("FechaOut").ToString
                Dim dtAcomodos As DataTable = dtFiltrarDataTable(dtServiciosReserva, "IDDet=" & Item("IDDet") & "")
                For Each dAcm As DataRow In dtAcomodos.Rows
                    strAcomodos &= dAcm("Cantidad").ToString & " " & dAcm("Servicio") & ";"
                Next

                If dtFiltrarDataTable(dtDatosComparados, "AcomodoCab='" & strAcomodos & "'").Rows.Count = 0 Then
                    dtDatosComparados.Rows.Add(Item("IDDet"), Item("FechaIn"), Item("FechaOut"), strAcomodos)
                Else
                    For Each dItemDCompare As DataRow In dtDatosComparados.Rows
                        If dItemDCompare("AcomodoCab").ToString = strAcomodos Then
                            Dim strFechaInOut As String = dItemDCompare("FechaIn").ToString & "|" & dItemDCompare("FechaOut").ToString
                            If InStr(strFechaInOut.Trim, strFechaInOutCab.Trim) = 0 Then
                                dItemDCompare("FechaOut") = dItemDCompare("FechaOut") & "|" & strFechaInOutCab
                            End If
                        End If
                    Next
                End If
            Next

            For Each dRowDataOptm As DataRow In dtDatosComparados.Rows
                Dim DtAcomodoEspIng As DataTable = dtDevuelvePaxAcomodosEspeciales(dRowDataOptm("IDDet")) 'objBLC.ConsultarListxIDDet(CInt(dRowDataOptm("IDDet")))
                Dim strLineaSubTituloDia As String = dRowDataOptm("FechaIn") & " - " & dRowDataOptm("FechaOut")
                Dim strLineaSubAcomdo As String = Replace(dRowDataOptm("AcomodoCab").ToString, ";", " + ").Trim
                If InStr(dRowDataOptm("FechaOut").ToString(), "|") > 0 Then 'Se arma el Titulo agrupado para los mismos acomodos.
                    Dim arrFechas() As String = Split(dRowDataOptm("FechaOut").ToString(), "|")
                    For i As Byte = 0 To arrFechas.Length - 1
                        If i = 0 Then
                            strLineaSubTituloDia = "[" & dRowDataOptm("FechaIn") & " - " & arrFechas(i).ToString() & "] ,"
                        Else
                            If Not (i Mod 2 = 0) Then
                                strLineaSubTituloDia += "[" & arrFechas(i).ToString() & " - "
                            Else
                                strLineaSubTituloDia += arrFechas(i).ToString() + "] ,"
                            End If
                        End If
                    Next
                    strLineaSubTituloDia = strLineaSubTituloDia.Substring(0, strLineaSubTituloDia.Length - 1).Trim
                End If

                strLine += strLineaSubTituloDia & vbCrLf
                strLine += strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1) & vbCrLf

                If DtAcomodoEspIng.Rows.Count > 0 Then 'Para Acomodos Especiales 
                    Dim ArrAcomodos() As String = Split(dRowDataOptm("AcomodoCab").ToString, ";")
                    For i As Byte = 0 To ArrAcomodos.Length - 1
                        If ArrAcomodos(i).ToString.Trim() <> "" Then
                            Dim strAcmd As String = ArrAcomodos(i).ToString.Trim
                            If strAcmd <> strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1).Trim Then strLine += strAcmd & vbCrLf
                            Dim bytMatrimonial As String = If(InStr(strAcmd, "(MAT)") > 0, 1, 0)

                            DtAcomodoIng = dtFiltrarDataTable(DtAcomodoEspIng, "", "CapacidadHab,EsMatrimonial")
                            For Each dRow As DataRow In dtFiltrarDataTable(DtAcomodoIng, "CapacidadHab=" & bytDevuelveCapacidadxCadena(strAcmd) _
                                                                           & " and EsMatrimonial=" & bytMatrimonial & "").Rows
                                Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("Apellidos").ToString & " " & dRow("Nombres").ToString
                                strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                                strLine += strCampoCorreoTexto(49, strNombre)
                                strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                                strLine += strCampoCorreoTexto(4, dRow("DescNacionalidad").ToString)
                                strLine += strCampoCorreoTexto(11, dRow("FecNacimiento"))
                                strLine += strCampoCorreoTexto(7, dRow("ACC"))
                                strLine += strCampoCorreoTexto(21, dRow("ObservEspecial")) & vbCrLf
                            Next
                        End If
                    Next
                ElseIf DtAcomodoIng.Rows.Count > 0 Then 'Para Acomodos Normales
                    Dim ArrAcomodos() As String = Split(dRowDataOptm("AcomodoCab").ToString, ";")
                    For i As Byte = 0 To ArrAcomodos.Length - 1
                        If ArrAcomodos(i).ToString.Trim() <> "" Then
                            Dim strAcmd As String = ArrAcomodos(i).ToString.Trim
                            If strAcmd <> strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1).Trim Then strLine += strAcmd & vbCrLf
                            Dim bytMatrimonial As String = If(InStr(strAcmd, "(MAT)") > 0, 1, 0)

                            DtAcomodoIng = dtFiltrarDataTable(DtAcomodoIng, "", "CapacidadHab,EsMatrimonial")
                            For Each dRow As DataRow In dtFiltrarDataTable(DtAcomodoIng, "CapacidadHab=" & bytDevuelveCapacidadxCadena(strAcmd) _
                                                                           & " and EsMatrimonial=" & bytMatrimonial & "").Rows
                                Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("Apellidos").ToString & " " & dRow("Nombres").ToString
                                strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                                strLine += strCampoCorreoTexto(49, strNombre)
                                strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                                strLine += strCampoCorreoTexto(4, dRow("DescNacionalidad").ToString)
                                strLine += strCampoCorreoTexto(11, dRow("FecNacimiento"))
                                strLine += strCampoCorreoTexto(7, dRow("ACC"))
                                strLine += strCampoCorreoTexto(21, dRow("ObservEspecial")) & vbCrLf
                            Next
                        End If
                    Next
                Else 'Otros Casos - Carga Los Pax generados Por Item
                    dc = New Dictionary(Of String, String)
                    dc.Add("@IDReserva", vintIDReserva)
                    Dim DtAcomodosRes As DataTable = objADO.GetDataTable(True, "RESERVAS_DET_SelListAcomodoPaxxIDDet", dc)
                    DtAcomodosRes = dtFiltrarDataTable(DtAcomodosRes, "IDDet=" & dRowDataOptm("IDDet"))

                    Dim strTituloACC As String = ""
                    For Each dRow As DataRow In DtAcomodosRes.Rows
                        If strTituloACC <> dRow("TituloCab").ToString Then
                            strTituloACC = dRow("TituloCab").ToString
                            If strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1).Trim <> strTituloACC Then strLine += strTituloACC & vbCrLf
                        End If
                        Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("NombrePax").ToString
                        strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                        strLine += strCampoCorreoTexto(49, strNombre)
                        strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                        strLine += strCampoCorreoTexto(4, dRow("NacionalidadPax").ToString)
                        strLine += strCampoCorreoTexto(11, dRow("FechNacimiento"))
                        strLine += strCampoCorreoTexto(7, dRow("ACC"))
                        strLine += strCampoCorreoTexto(21, dRow("ObservEspecial")) & vbCrLf
                    Next
                End If
            Next

            strLine += (StrDup(114, "*") & vbCrLf).ToString
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoPaxAcomodoEspecial(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, _
                                                       ByVal vstrIDProveedor As String)
        Dim strLine As String = ""
        Try
            Dim strTitulos As String = strCargarTitulosListadoPax(True)
            strLine += strTitulos

            Dim dtDatosComparados As New DataTable
            With dtDatosComparados
                .Columns.Add("IDDet")
                .Columns.Add("FechaIn")
                .Columns.Add("FechaOut")
                .Columns.Add("AcomodoCab")
            End With

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            'Dim dtServiciosEspCoti As Data.DataTable = objADO.GetDataTable(True, "COTIDET_SelServiciosxAcomodoEspecial", dcGen)
            Dim dtServiciosReserva As Data.DataTable = objADO.GetDataTable(True, "RESERVAS_DET_SelServiciosxAcomodoEspecial", dc)
            Dim objBL As New clsReservaBN.clsAcomodoPaxProveedorBN
            'Dim objBLC As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
            Dim DtAcomodoIng As DataTable = dtDevuelvePaxAcomodos(vintIDCab, vintIDReserva) 'objBL.ConsultarListxIDReserva(vintIDReserva)

            'Union de detalles
            For Each Item As DataRow In dtServiciosReserva.Rows
                Dim strAcomodos As String = "", strFechaInOutCab As String = Item("FechaIn").ToString & "|" & Item("FechaOut").ToString
                Dim dtAcomodos As DataTable = dtFiltrarDataTable(dtServiciosReserva, "IDDet=" & Item("IDDet") & "")
                For Each dAcm As DataRow In dtAcomodos.Rows
                    strAcomodos &= dAcm("Cantidad").ToString & " " & dAcm("Servicio") & ";"
                Next

                strAcomodos = Replace(strAcomodos, "'", " ")
                If dtFiltrarDataTable(dtDatosComparados, "AcomodoCab='" & strAcomodos & "'").Rows.Count = 0 Then
                    dtDatosComparados.Rows.Add(Item("IDDet"), Item("FechaIn"), Item("FechaOut"), strAcomodos)
                Else
                    For Each dItemDCompare As DataRow In dtDatosComparados.Rows
                        If dItemDCompare("AcomodoCab").ToString = strAcomodos Then
                            Dim strFechaInOut As String = dItemDCompare("FechaIn").ToString & "|" & dItemDCompare("FechaOut").ToString
                            If InStr(strFechaInOut.Trim, strFechaInOutCab.Trim) = 0 Then
                                dItemDCompare("FechaOut") = dItemDCompare("FechaOut") & "|" & strFechaInOutCab
                            End If
                        End If
                    Next
                End If
            Next

            For Each dRowDataOptm As DataRow In dtDatosComparados.Rows
                Dim DtAcomodoEspIng As DataTable = dtDevuelvePaxAcomodosEspeciales(dRowDataOptm("IDDet")) 'objBLC.ConsultarListxIDDet(CInt(dRowDataOptm("IDDet")))
                Dim strLineaSubTituloDia As String = dRowDataOptm("FechaIn") & " - " & dRowDataOptm("FechaOut")
                Dim strLineaSubAcomdo As String = Replace(dRowDataOptm("AcomodoCab").ToString, ";", " + ").Trim
                If InStr(dRowDataOptm("FechaOut").ToString(), "|") > 0 Then 'Se arma el Titulo agrupado para los mismos acomodos.
                    Dim arrFechas() As String = Split(dRowDataOptm("FechaOut").ToString(), "|")
                    For i As Byte = 0 To arrFechas.Length - 1
                        If i = 0 Then
                            strLineaSubTituloDia = "[" & dRowDataOptm("FechaIn") & " - " & arrFechas(i).ToString() & "] ,"
                        Else
                            If Not (i Mod 2 = 0) Then
                                strLineaSubTituloDia += "[" & arrFechas(i).ToString() & " - "
                            Else
                                strLineaSubTituloDia += arrFechas(i).ToString() + "] ,"
                            End If
                        End If
                    Next
                    strLineaSubTituloDia = strLineaSubTituloDia.Substring(0, strLineaSubTituloDia.Length - 1).Trim
                End If

                strLine += strLineaSubTituloDia & vbCrLf
                strLine += strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1) & vbCrLf

                If DtAcomodoEspIng.Rows.Count > 0 Then 'Para Acomodos Especiales 
                    Dim ArrAcomodos() As String = Split(dRowDataOptm("AcomodoCab").ToString, ";")
                    For i As Byte = 0 To ArrAcomodos.Length - 1
                        If ArrAcomodos(i).ToString.Trim() <> "" Then
                            Dim strAcmd As String = ArrAcomodos(i).ToString.Trim
                            If strAcmd <> strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1).Trim Then strLine += strAcmd & vbCrLf
                            Dim bytMatrimonial As String = If(InStr(strAcmd, "(MAT)") > 0, 1, 0)

                            Dim dttTablaTemp As DataTable = dtFiltrarDataTable(DtAcomodoEspIng, "", "CapacidadHab,EsMatrimonial")
                            For Each dRow As DataRow In dtFiltrarDataTable(dttTablaTemp, "CapacidadHab=" & bytDevuelveCapacidadxCadena(strAcmd) _
                                                                           & " and EsMatrimonial=" & bytMatrimonial & "").Rows
                                Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("Apellidos").ToString & " " & dRow("Nombres").ToString
                                strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                                strLine += strCampoCorreoTexto(49, strNombre)
                                strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                                strLine += strCampoCorreoTexto(4, dRow("DescNacionalidad").ToString)
                                strLine += strCampoCorreoTexto(11, dRow("FecNacimiento"))
                                strLine += strCampoCorreoTexto(7, dRow("ACC"))
                                strLine += strCampoCorreoTexto(21, dRow("ObservEspecial")) & vbCrLf
                            Next
                        End If
                    Next
                ElseIf DtAcomodoIng.Rows.Count > 0 Then 'Para Acomodos Normales
                    Dim ArrAcomodos() As String = Split(dRowDataOptm("AcomodoCab").ToString, ";")
                    For i As Byte = 0 To ArrAcomodos.Length - 1
                        If ArrAcomodos(i).ToString.Trim() <> "" Then
                            Dim strAcmd As String = ArrAcomodos(i).ToString.Trim
                            If strAcmd <> strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1).Trim Then strLine += strAcmd & vbCrLf
                            Dim bytMatrimonial As String = If(InStr(strAcmd, "(MAT)") > 0, 1, 0)

                            Dim dttDatosTmp As DataTable = dtFiltrarDataTable(DtAcomodoIng, "", "CapacidadHab,EsMatrimonial")
                            For Each dRow As DataRow In dtFiltrarDataTable(dttDatosTmp, "CapacidadHab=" & bytDevuelveCapacidadxCadena(strAcmd) _
                                                                           & " and EsMatrimonial=" & bytMatrimonial & "").Rows
                                Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("Apellidos").ToString & " " & dRow("Nombres").ToString
                                strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                                strLine += strCampoCorreoTexto(49, strNombre)
                                strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                                strLine += strCampoCorreoTexto(4, dRow("DescNacionalidad").ToString)
                                strLine += strCampoCorreoTexto(11, dRow("FecNacimiento"))
                                strLine += strCampoCorreoTexto(7, dRow("ACC"))
                                strLine += strCampoCorreoTexto(21, dRow("ObservEspecial")) & vbCrLf
                            Next
                        End If
                    Next
                Else 'Otros Casos - Carga Los Pax generados Por Item
                    dc = New Dictionary(Of String, String)
                    dc.Add("@IDReserva", vintIDReserva)
                    Dim DtAcomodosRes As DataTable = objADO.GetDataTable(True, "RESERVAS_DET_SelListAcomodoPaxxIDDet", dc)
                    DtAcomodosRes = dtFiltrarDataTable(DtAcomodosRes, "IDDet=" & dRowDataOptm("IDDet"))

                    Dim strTituloACC As String = ""
                    For Each dRow As DataRow In DtAcomodosRes.Rows
                        If strTituloACC <> dRow("TituloCab").ToString Then
                            strTituloACC = dRow("TituloCab").ToString
                            If strLineaSubAcomdo.Substring(0, strLineaSubAcomdo.Length - 1).Trim <> strTituloACC Then strLine += strTituloACC & vbCrLf
                        End If
                        Dim strNombre As String = dRow("Titulo").ToString & " " & dRow("NombrePax").ToString
                        strLine += strCampoCorreoTexto(3, dRow("NroOrd").ToString)
                        strLine += strCampoCorreoTexto(49, strNombre)
                        strLine += strCampoCorreoTexto(19, dRow("DescIdentidad").ToString & ": " & dRow("NumIdentidad").ToString)
                        strLine += strCampoCorreoTexto(4, dRow("NacionalidadPax").ToString)
                        strLine += strCampoCorreoTexto(11, dRow("FechNacimiento"))
                        strLine += strCampoCorreoTexto(7, dRow("ACC"))
                        strLine += strCampoCorreoTexto(21, dRow("ObservEspecial")) & vbCrLf
                    Next
                End If
            Next

            strLine += (StrDup(114, "*") & vbCrLf).ToString
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtDevuelvePaxAcomodos(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCab)
            dc.Add("@IDReserva", vintIDReserva)

            Return objADO.GetDataTable(True, "COTIPAX_Sel_ListxIDCab_Mail", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtDevuelvePaxAcomodosEspeciales(ByVal vintIDDet As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            Return objADO.GetDataTable(True, "COTIPAX_Sel_ListxIDCabxAcomodoEspecial_Mail", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoPax(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, _
                                        ByVal vblnEsProveedorHotel As Boolean) As String
        Try
            Dim strTitulos As String = strCargarTitulosListadoPax(vblnEsProveedorHotel)
            Dim dc2 As New Dictionary(Of String, String)
            dc2.Add("@IDCAB", vintIDCab)
            dc2.Add("@IDReserva", vintIDReserva)

            Dim strLine As String = ""
            strLine += strTitulos
            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIPAX_Sel_ListxIDCab_MailNoHotel", dc2)
                While dr.Read
                    Dim strNombre As String = dr("Titulo").ToString & " " & dr("Nombres").ToString & " " & dr("Apellidos").ToString
                    strLine += strCampoCorreoTexto(3, dr("NroOrd").ToString)
                    strLine += strCampoCorreoTexto(49, strNombre)
                    strLine += strCampoCorreoTexto(19, dr("DescIdentidad").ToString & ": " & dr("NumIdentidad").ToString)
                    strLine += strCampoCorreoTexto(4, dr("DescNacionalidad").ToString)
                    strLine += strCampoCorreoTexto(11, dr("FecNacimiento"))
                    If vblnEsProveedorHotel Then
                        strLine += strCampoCorreoTexto(7, dr("ACC"))
                        strLine += strCampoCorreoTexto(18, dr("ObservEspecial")) '& vbCrLf
                    Else
                        strLine += strCampoCorreoTexto(25, dr("ObservEspecial")) '& vbCrLf
                    End If
                    strLine += strCampoCorreoTexto(3, dr("DescTC")) & vbCrLf
                End While
                dr.Close()
            End Using

            strLine += (StrDup(114, "*") & vbCrLf).ToString
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLVuelos(ByVal vintIDCab As Integer) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCAB", vintIDCab)

            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIVUELOS_Sel_ListSolicitud", dc)
                While dr.Read
                    If bln1erRegistro Then
                        strLine += "<tr>" & vbCrLf
                    End If
                    bln1erRegistro = False
                    Dim strTD As String = "<td align='left'>"
                    strLine += strTD & dr("FecSalida") & "</td>" & vbCrLf
                    strTD = "<td align='rigth'>"
                    strLine += strTD & dr("Salida") & "</td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("FecRetorno") & "</td>" & vbCrLf
                    strTD = "<td align='rigth'>"
                    strLine += strTD & dr("Llegada") & "</td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("NombreCorto") & "</td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("Vuelo") & "</td>" & vbCrLf
                    strTD = "<td align='rigth'>"
                    strLine += strTD & dr("HrRecojo") & "</td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("Origen") & "</td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("Destino") & "</td>" & vbCrLf
                    strLine += "</tr>" & vbCrLf
                End While
                dr.Close()
            End Using

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLDatosContactos(ByVal vstrIDProveedor As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Dim strLine As String = ""
            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("MACONTACTOSPROVEEDOR_Sel_List_HTML_TEXTO", dc)
                While dr.Read
                    strLine += "<tr>" & vbCrLf
                    Dim strTD As String = "<td align='left'>"
                    strLine += strTD & dr("Nombres") & "</td>" & vbCrLf
                    strLine += strTD & dr("Telefono") & "</td>" & vbCrLf
                    strLine += strTD & dr("Celular") & "</td>" & vbCrLf
                    strLine += strTD & dr("Email") & "</td>" & vbCrLf
                    strLine += "</tr>"
                End While
                dr.Close()
            End Using
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLVuelosxTipoTransporte(ByVal vintIDCab As Integer, _
                                                         ByVal vstrTipoTransporte As String, _
                                                         ByRef rCantidadFilas As Byte) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCAB", vintIDCab)
            dc.Add("@TipoTransporte", vstrTipoTransporte)

            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIVUELOS_Sel_ListSolicitudxTipoTransporte", dc)
                While dr.Read
                    If bln1erRegistro Then
                        strLine += "<tr>" & vbCrLf
                    End If
                    bln1erRegistro = False
                    Dim strTD As String = "<td align='left'>"
                    strLine += strTD & dr("Ruta") & "</td>" & vbCrLf

                    Dim bytNroPax As Byte = CByte(dr("CantidaPax"))
                    If bytNroPax > 0 Then
                        strTD = "<td align='center'>"
                        strLine += strTD & "Paxs" & "</td>" & vbCrLf
                    Else
                        strTD = "<td align='center'>"
                        strLine += strTD & "Gu&iacute;a" & "</td>" & vbCrLf
                    End If

                    strTD = "<td align='center'>"
                    strLine += strTD & dr("Vuelo") & "</td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("FecViaje") & "</td>" & vbCrLf
                    strTD = "<td align='center'>"
                    strLine += strTD & dr("Salida") & "</td>" & vbCrLf
                    strTD = "<td align='center'>"
                    strLine += strTD & dr("Llegada") & "</td>" & vbCrLf
                    strTD = "<td align='center'>"
                    strLine += strTD & dr("CodReserva") & "</td>" & vbCrLf
                    strTD = "<td align='center'>"
                    strLine += strTD & dr("Status") & "</td>" & vbCrLf
                    strLine += "</tr>"
                    rCantidadFilas += 1
                End While
                dr.Close()
            End Using

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLHotelesxServicio(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As String
        Try
            Dim strLine As String = ""
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCab", vintIDCab)

            strLine = "<table class='nuevoEstilo1'>"
            'strLine &= "<tr> <td><b>Hotel</b></td> <td><b>Fecha IN</b></td> <td><b>Fecha OUT</b></td> </tr>"
            strLine &= "<tr> <td><b>Hotel</b></td> <td><b>Tipo Hab.</b></td> <td><b>Fecha IN</b></td> <td><b>Fecha OUT</b></td> </tr>"

            Using drHotel As SqlClient.SqlDataReader = objADO.GetDataReader("RESERVAS_DET_SelRangoHoteles", dc)
                While drHotel.Read
                    strLine &= "<tr> <td>" & drHotel("DescProveedores") & "</td> <td>" & drHotel("Servicio") & "</td> <td>" & CDate(drHotel("FechaIN")).ToShortDateString & _
                    "</td> <td>" & CDate(drHotel("FechaOut")).ToShortDateString & "</td></tr>"
                End While
                drHotel.Close()
            End Using
            strLine &= "</table>"

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLRestaurantexServicio(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As String
        Try
            Dim strLine As String = ""
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCab", vintIDCab)

            strLine = "<table class='nuevoEstilo1'>"
            strLine &= "<tr> <td><b>Restaurante</b></td> <td><b>Fecha IN</b></td> <td><b>Hora</b></td> </tr>"

            Using drHotel As SqlClient.SqlDataReader = objADO.GetDataReader("RESERVAS_DET_SelRangoRestaurantes", dc)
                While drHotel.Read
                    strLine &= "<tr> <td>" & drHotel("DescProveedores") & "</td> <td>" & CDate(drHotel("FechaIN")).ToShortDateString & _
                    "</td> <td>" & drHotel("HoraIN") & "</td>  </tr>"
                End While
                drHotel.Close()
            End Using
            strLine &= "</table>"

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLOperadoresxServicio(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As String
        Try
            Dim strLine As String = ""
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCab", vintIDCab)

            strLine = "<table class='nuevoEstilo1'>"
            strLine &= "<tr> <td><b>Operador</b></td> <td><b>Fecha IN</b></td> </tr>"

            Using drHotel As SqlClient.SqlDataReader = objADO.GetDataReader("RESERVAS_DET_SelRangoOperadores", dc)
                While drHotel.Read
                    strLine &= "<tr> <td>" & drHotel("NombreCorto") & "</td> <td>" & CDate(drHotel("FechaIn")).ToShortDateString & _
                    "</td> </tr>"
                End While
                drHotel.Close()
            End Using
            strLine &= "</table>"

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoOperadoresxServicio(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCab", vintIDCab)

            Dim strLine As String = ""
            strLine += StrDup(55, "*") & vbCrLf
            strLine += strCampoCorreoTexto(45, "Operador")
            strLine += strCampoCorreoTexto(11, "Fecha IN") & vbCrLf

            Using drHotel As SqlClient.SqlDataReader = objADO.GetDataReader("RESERVAS_DET_SelRangoOperadores", dc)
                While drHotel.Read
                    strLine &= strCampoCorreoTexto(45, drHotel("NombreCorto"))
                    strLine &= strCampoCorreoTexto(11, CDate(drHotel("FechaIn")).ToShortDateString) & vbCrLf
                End While
                drHotel.Close()
            End Using
            strLine += StrDup(55, "*")

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoRestaurantesxServicio(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCab", vintIDCab)

            Dim strLine As String = ""
            strLine += StrDup(65, "*") & vbCrLf
            strLine += strCampoCorreoTexto(45, "Restaurante")
            strLine += strCampoCorreoTexto(11, "Fecha IN")
            strLine += strCampoCorreoTexto(8, "Hora") & vbCrLf

            Using drHotel As SqlClient.SqlDataReader = objADO.GetDataReader("RESERVAS_DET_SelRangoRestaurantes", dc)
                While drHotel.Read
                    strLine &= strCampoCorreoTexto(45, drHotel("DescProveedores"))
                    strLine &= strCampoCorreoTexto(11, CDate(drHotel("FechaIN")).ToShortDateString)
                    strLine &= strCampoCorreoTexto(8, drHotel("HoraIN")) & vbCrLf
                End While
                drHotel.Close()
            End Using
            strLine += StrDup(65, "*")

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoHotelesxServicio(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCab", vintIDCab)

            Dim strLine As String = ""
            ' strLine += StrDup(66, "*") & vbCrLf
            strLine += StrDup(106, "*") & vbCrLf
            strLine += strCampoCorreoTexto(45, "Hotel")
            strLine += strCampoCorreoTexto(40, "Tipo Hab.")
            strLine += strCampoCorreoTexto(11, "Fecha IN")
            strLine += strCampoCorreoTexto(11, "Fecha OUT") & vbCrLf


            Using drHotel As SqlClient.SqlDataReader = objADO.GetDataReader("RESERVAS_DET_SelRangoHoteles", dc)
                While drHotel.Read
                    strLine &= strCampoCorreoTexto(45, drHotel("DescProveedores"))
                    strLine &= strCampoCorreoTexto(40, drHotel("Servicio"))
                    strLine &= strCampoCorreoTexto(11, CDate(drHotel("FechaIN")).ToShortDateString)
                    strLine &= strCampoCorreoTexto(11, CDate(drHotel("FechaOut")).ToShortDateString) & vbCrLf

                End While
                drHotel.Close()
            End Using
            'strLine += StrDup(66, "*")
            strLine += StrDup(106, "*")

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoVuelos(ByVal vintIDCab As Integer) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCAB", vintIDCab)

            Dim strLine As String = ""
            strLine += strCampoCorreoTexto(17, "Fecha de Salida")
            strLine += strCampoCorreoTexto(8, "Hora")
            strLine += strCampoCorreoTexto(19, "Fecha de Retorno")
            strLine += strCampoCorreoTexto(6, "Hora")
            strLine += strCampoCorreoTexto(30, "Proveedor")
            strLine += strCampoCorreoTexto(20, "Num. de Vuelo")
            strLine += strCampoCorreoTexto(8, "Recojo")
            strLine += strCampoCorreoTexto(30, "Origen")
            strLine += strCampoCorreoTexto(30, "Destino") & vbCrLf

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIVUELOS_Sel_ListSolicitud", dc)
                While dr.Read
                    strLine += strCampoCorreoTexto(17, dr("FecSalida"))
                    strLine += strCampoCorreoTexto(8, dr("Salida"))
                    strLine += strCampoCorreoTexto(19, dr("FecRetorno"))
                    strLine += strCampoCorreoTexto(6, dr("Llegada"))
                    strLine += strCampoCorreoTexto(30, dr("NombreCorto"))
                    strLine += strCampoCorreoTexto(20, dr("Vuelo"))
                    strLine += strCampoCorreoTexto(8, dr("HrRecojo"))
                    strLine += strCampoCorreoTexto(30, dr("Origen"))
                    strLine += strCampoCorreoTexto(30, dr("Destino")) & vbCrLf
                End While
                dr.Close()
            End Using

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoVuelosxTipoTransporte(ByVal vintIDCab As Integer, _
                                                          ByVal vstrTipoTransporte As String, _
                                                          ByRef rbytCantidadFila As Byte) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCAB", vintIDCab)
            dc.Add("@TipoTransporte", vstrTipoTransporte)

            Dim strTitulo As String = ""
            If vstrTipoTransporte = "V" Then
                strTitulo = "Vuelo"
            ElseIf vstrTipoTransporte = "B" Then
                strTitulo = "Bus"
            ElseIf vstrTipoTransporte = "T" Then
                strTitulo = "Tren"
            End If

            Dim strLine As String = ""
            'strLine += StrDup(75, "*") & vbCrLf
            strLine += "Inf. de " & strTitulo & vbCrLf
            strLine += StrDup(82, "*") & vbCrLf
            strLine += strCampoCorreoTexto(15, "Ruta")
            strLine += strCampoCorreoTexto(7, "Tipo")
            strLine += strCampoCorreoTexto(12, "Nro " & strTitulo)
            strLine += strCampoCorreoTexto(11, "F. Viaje")
            strLine += strCampoCorreoTexto(7, "Salida")
            strLine += strCampoCorreoTexto(9, "Llegada")
            strLine += strCampoCorreoTexto(15, "Cod. Reserva")
            strLine += strCampoCorreoTexto(6, "Status") & vbCrLf

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIVUELOS_Sel_ListSolicitudxTipoTransporte", dc)
                While dr.Read
                    strLine += strCampoCorreoTexto(15, dr("Ruta"))
                    Dim CantidaPax As Byte = CByte(dr("CantidaPax"))
                    If CantidaPax > 0 Then
                        strLine += strCampoCorreoTexto(7, "Paxs")
                    Else
                        strLine += strCampoCorreoTexto(7, "Guía")
                    End If
                    strLine += strCampoCorreoTexto(12, dr("Vuelo"))
                    strLine += strCampoCorreoTexto(11, dr("FecViaje"))
                    strLine += strCampoCorreoTexto(7, dr("Salida"))
                    strLine += strCampoCorreoTexto(9, dr("Llegada"))
                    strLine += strCampoCorreoTexto(15, dr("CodReserva"))
                    strLine += strCampoCorreoTexto(6, dr("Status")) & vbCrLf
                    rbytCantidadFila += 1
                End While
                dr.Close()
            End Using
            strLine += StrDup(82, "*")

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoDatosContactos(ByVal vstrIDProveedor As String, ByVal vstrDescProveedor As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Dim strLine As String = ""
            strLine += vbCrLf
            'strLine += (StrDup(115, "*") & vbCrLf).ToString
            strLine += ("Inf. de contactos de " & vstrDescProveedor.Trim & "(Emergencias)" & vbCrLf).ToString
            strLine += (StrDup(115, "*") & vbCrLf).ToString
            strLine += strCampoCorreoTexto(40, "Nombre")
            strLine += strCampoCorreoTexto(20, "Teléfono")
            strLine += strCampoCorreoTexto(20, "Célular")
            strLine += strCampoCorreoTexto(35, "Email") & vbCrLf

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("MACONTACTOSPROVEEDOR_Sel_List_HTML_TEXTO", dc)
                While dr.Read
                    strLine += strCampoCorreoTexto(40, dr("Nombres"))
                    strLine += strCampoCorreoTexto(20, dr("Telefono"))
                    strLine += strCampoCorreoTexto(20, dr("Celular"))
                    strLine += strCampoCorreoTexto(35, dr("Email")) & vbCrLf
                End While
                dr.Close()
            End Using
            strLine += (StrDup(115, "*") & vbCrLf).ToString

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLCorreoSolicitud(ByVal vintIDOperacion_Det As Integer, _
                                          ByVal vintIDServicio_Det As Integer, _
                                          ByVal vstrTipoProv As String, _
                                          ByVal vintIDCab As Integer, _
                                          ByVal vstrIDProveedor As String, _
                                          ByVal vblnServicioTC As Boolean, _
                                          ByVal vstrIDServicio As String, _
                                          Optional ByVal vstrModuloCorreoInterno As String = "") As String
        Try
            Dim objDetServBN As New clsDetalleServiciosOperacionBN
            Dim dr As SqlClient.SqlDataReader

            If vstrIDProveedor = "" Then
                If vstrTipoProv = "" Then
                    dr = objDetServBN.ConsultarxID(vintIDOperacion_Det, vintIDServicio_Det)
                Else
                    dr = objDetServBN.ConsultarxIDTipoProv(vstrTipoProv, vintIDOperacion_Det, "PA", vstrIDServicio)
                End If
            Else
                'dr = objDetServBN.ConsultarxIDProveedor(vstrIDProveedor, vintIDCab)
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@FlServicioTC", vblnServicioTC)
                dr = objADO.GetDataReader("OPERACIONES_DET_List_ServiciosxProvInt_HTML", dc)
            End If

            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""
            Dim strCeldaVacia As String = "<td>&nbsp;</td>"

            While dr.Read
                If bln1erRegistro Then
                    strLine += "<tr>" & vbCrLf
                End If
                bln1erRegistro = False
                Dim strTD As String = "<td align='left'>"
                strLine += strTD & dr("Dia") & "</td>" & vbCrLf
                strTD = "<td align='left'>"
                strLine += strTD & dr("Hora") & "</td>" & vbCrLf
                strTD = "<td align='left'>"
                strLine += strTD & dr("DescServicio") & "</td>" & vbCrLf
                strTD = "<td align='center'>"
                strLine += strTD & dr("IDTipoServ") & "</td>" & vbCrLf
                strTD = "<td align='center'>"
                strLine += strTD & dr("NroPax").ToString & "</td>" & vbCrLf
                strTD = "<td align='left'>"
                strLine += strTD & dr("IDIdioma").ToString & "</td>" & vbCrLf
                strLine += "</tr>" & vbCrLf

                If vstrIDProveedor <> "" Then
                    If CBool(dr("ExistDiferPax")) Then

                        Dim objBnPax As New clsPaxDetalleOperacionBN
                        Dim dtt As DataTable = objBnPax.ConsultarDetPaxxIDOperacion(CInt(dr("IDOperacion_Det")))

                        strLine += "<tr>" & vbCrLf
                        strLine += "<td coslpan='6' align='left'>Pax :</td>" & vbCrLf
                        strLine += "</tr>" & vbCrLf

                        For Each dtr As DataRow In dtt.Rows
                            strLine += "<tr>" & vbCrLf
                            strLine += "<td coslpan='6' align='left'>" & dtr("NombreCompleto") & "</td>" & vbCrLf
                            strLine += "</tr>" & vbCrLf
                        Next

                    End If
                End If


                If vstrModuloCorreoInterno = "" Then
                    strLine += "<tr>" & vbCrLf
                    strLine += "<td colspan='6'>" & vbCrLf
                    strLine += vbTab & "<table width='100%' border='0'>" & vbCrLf
                    strLine += vbTab & vbTab & "<tr>" & vbCrLf
                    strLine += vbTab & vbTab & vbTab & "<td width='7%' style='font-weight: bold; width=60px'>Observaci&oacute;n:</td>" & vbCrLf
                    strLine += vbTab & vbTab & vbTab & "<td width='93%'>" & If(dr("ObservBiblia").ToString = "", "&nbsp;", dr("ObservBiblia").ToString) & "</td>" & vbCrLf
                    strLine += vbTab & vbTab & "</tr>" & vbCrLf
                    strLine += vbTab & "</table>" & vbCrLf
                    strLine += vbTab & "</br>" & vbCrLf
                    strLine += "</td>" & vbCrLf
                    strLine += "</tr>" & vbCrLf
                End If
            End While
            dr.Close()

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function strDevuelveTextoCorreoSolicitud(ByVal vintIDOperacion_Det As Integer, _
                                          ByVal vintIDServicio_Det As Integer, _
                                          ByVal vstrTipoProv As String, _
                                          ByVal vintIDCab As Integer, _
                                          ByVal vstrIDProveedor As String, _
                                          ByVal vstrIDServicio As String, _
                                          Optional ByVal vstrModuloCorreoInterno As String = "") As String
        Try
            Dim objDetServBN As New clsDetalleServiciosOperacionBN
            Dim dr As SqlClient.SqlDataReader

            If vstrIDProveedor = "" Then
                If vstrTipoProv = "" Then
                    If vintIDServicio_Det = 0 Then
                        dr = ConsultarPkCorreoSolicitud(vintIDOperacion_Det)
                    Else
                        dr = objDetServBN.ConsultarxID(vintIDOperacion_Det, vintIDServicio_Det)
                    End If
                Else
                    dr = objDetServBN.ConsultarxIDTipoProv(vstrTipoProv, vintIDOperacion_Det, "PA", vstrIDServicio)
                End If
            Else
                dr = objDetServBN.ConsultarxIDProveedor(vstrIDProveedor, vintIDCab)
            End If

            Dim strLine As String = ""
            While dr.Read

                strLine += strCampoCorreoTexto(16, dr("Dia").ToString)
                strLine += strCampoCorreoTexto(7, dr("Hora").ToString)
                strLine += strCampoCorreoTexto(75, dr("DescServicio").ToString)
                strLine += strCampoCorreoTexto(5, dr("IDTipoServ").ToString)
                strLine += strCampoCorreoTexto(5, dr("NroPax").ToString)
                strLine += strCampoCorreoTexto(3, dr("IDIdioma").ToString)

                If vstrIDProveedor <> "" Then
                    If CBool(dr("ExistDiferPax")) Then
                        Dim objBnPax As New clsPaxDetalleOperacionBN
                        Dim dtt As DataTable = objBnPax.ConsultarDetPaxxIDOperacion(CInt(dr("IDOperacion_Det")))

                        For Each dtr As DataRow In dtt.Rows
                            strLine += vbCrLf
                            strLine += "Pax :" & vbCrLf
                            strLine += dtr("NombreCompleto").ToString
                        Next
                    End If
                End If

                If vstrModuloCorreoInterno = "" Then
                    strLine += vbCrLf
                    strLine += strCampoCorreoTexto(16, "Observación:")
                    strLine += If(dr("ObservBiblia").ToString = "", "", dr("ObservBiblia").ToString)
                    strLine += vbCrLf
                End If
            End While
            dr.Close()

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function strCampoCorreoTexto(ByVal vbytTamanioColumna As Byte, ByVal vstrValor As String) As String
        Try
            Dim strValorRet As String = ""

            If vbytTamanioColumna <= vstrValor.Trim.Length Then
                strValorRet = vstrValor.Substring(0, vbytTamanioColumna)
            Else
                strValorRet = vstrValor.Trim & Space(vbytTamanioColumna - vstrValor.Trim.Length)
            End If

            Return strValorRet
        Catch ex As System.Exception
            Throw
        End Try

    End Function

    Public Function strDevuelveHTMLCorreoInternoaReservas(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                          ByVal vblnServicioTC As Boolean) As String
        Try
            'Dim objDetServBN As New clsDetalleServiciosOperacionBN

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@FlServicioTC", vblnServicioTC)

            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""
            Dim strCeldaVacia As String = "<td>&nbsp;</td>"

            Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("OPERACIONES_DET_List_ServiciosxProvInt_HTML", dc)
                While dr.Read
                    If bln1erRegistro Then
                        strLine += "<tr>" & vbCrLf
                    End If
                    bln1erRegistro = False
                    Dim strTD As String = "<td align='left'>"
                    strLine += strTD & dr("Dia") & "<!--Fec2--></td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("Hora") & "<!--Ho2--></td>" & vbCrLf
                    strTD = "<td align='left'>"
                    strLine += strTD & dr("DescServicio") & "<!--Serv" & (70 - (dr("DescServicio").ToString.Trim.Length)) & "--></td>" & vbCrLf

                    strTD = "<td align='left'>"
                    strLine += strTD & dr("RutaTraslado") & "<!--Rut" & (10 - (dr("RutaTraslado").ToString.Trim.Length)) & "--></td>" & vbCrLf

                    strTD = "<td align='center'>"
                    strLine += strTD & dr("DescTipoServ") & "<!--Tip" & (5 - (dr("DescTipoServ").ToString.Trim.Length)) & "--></td>" & vbCrLf

                    Dim bytNroLib As Byte = If(IsDBNull(dr("NroLiberados")) = True, 0, CByte(dr("NroLiberados")))
                    strTD = "<td align='center'>"
                    If bytNroLib = 0 Then
                        strLine += strTD & dr("NroPax").ToString & "<!--pxa" & (4 - (dr("NroPax").ToString.Trim.Length)) & "--></td>" & vbCrLf
                    Else
                        strLine += strTD & dr("NroPax").ToString & "+" & bytNroLib.ToString & "<!--pxa" & (4 - (dr("NroPax").ToString.Trim.Length + 2)) & "--></td>" & vbCrLf
                    End If

                    strTD = "<td align='center'>"
                    strLine += strTD & dr("IDIdioma") & "<!--idi" & (4 - (dr("IDIdioma").ToString.Trim.Length)) & "--></td>" & vbCrLf

                    Dim dblnServicioTourConductor As Boolean = Convert.ToBoolean(dr("FlServicioTC"))
                    If Not dblnServicioTourConductor Then
                        strTD = "<td align='left'>"
                        If dr("NombreTrasladista").ToString.Trim <> "" Then
                            strLine += strTD & dr("GuiaProgramado").ToString & "/<!--gui" & (20 - (dr("GuiaProgramado").ToString.Trim.Length)) & "--><br />" & dr("NombreTrasladista").ToString.Trim & "</td>" & vbCrLf
                        Else
                            strLine += strTD & dr("GuiaProgramado").ToString & "<!--gui" & (20 - (dr("GuiaProgramado").ToString.Trim.Length)) & "--></td>" & vbCrLf
                        End If

                        strTD = "<td align='left'>"
                        If dr("VehiculoProg").ToString.Trim <> "" Then
                            strLine += strTD & dr("TransportistaProgramado").ToString & "/<br />" & dr("VehiculoProg").ToString & "</td>" & vbCrLf
                        Else
                            strLine += strTD & dr("TransportistaProgramado").ToString & "</td>" & vbCrLf
                        End If
                        strLine += "</tr>" & vbCrLf
                    Else
                        strTD = "<td align='left'>" & dr("NumDias") & "</td>" & vbCrLf
                    End If

                    If CBool(dr("ExistDiferPax")) And Not dblnServicioTourConductor Then
                        Dim objBnPax As New clsPaxDetalleOperacionBN
                        Dim dtt As DataTable = objBnPax.ConsultarDetPaxxIDOperacion(CInt(dr("IDOperacion_Det")))

                        strLine += "<tr>" & vbCrLf
                        strLine += vbTab & "<td colspan='8'>Pax: </td>" & vbCrLf
                        strLine += "</tr>" & vbCrLf

                        For Each drt As DataRow In dtt.Rows
                            strLine += "<tr>" & vbCrLf
                            strLine += vbTab & "<td colspan='8'>" & drt("NombreCompleto") & "</td>" & vbCrLf
                            strLine += "</tr>" & vbCrLf
                        Next
                    End If




                    'If dr("ObservBiblia").ToString.Trim <> "" Then
                    strLine += "<tr><td coslpan='2' align='right'><b>Obs. Biblia&nbsp;&nbsp;&nbsp;:</b></td> <td colspan='8'>" & dr("ObservBiblia") & "</td></tr>"
                    'End If

                    'If dr("ObservInterno").ToString.Trim <> "" Then
                    strLine += "<tr><td coslpan='2' align='right'><b>Obs. Interno&nbsp;:</b></td> <td colspan='8'>" & dr("ObservInterno") & "</td></tr>"
                    'End If

                    Dim strSoloCodigo As String = ""
                    If dr("CodigoPNR").ToString <> "" Then
                        strSoloCodigo = Replace(dr("CodigoPNR").ToString, "PNR: ", "")
                    End If
                    strLine += "<tr><td coslpan='2' align='right'><b>C&oacute;d. PNR &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:</b></td> <td colspan='8'>" & strSoloCodigo & "</td></tr>"
                End While
                dr.Close()
            End Using

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function strDevuelveHTMLCorreoOrden(ByVal vintIDOperacion_Det As Integer, _
                                          ByVal vintIDServicio_Det As Integer, _
                                          ByVal vstrTipoProv As String, _
                                          ByVal vstrIDServicio As String, _
                                          ByRef rdblSumaDolares As Double, _
                                          ByRef rdblSumaSoles As Double) As String
        Try
            Dim objDetServBN As New clsDetalleServiciosOperacionBN

            'Dim dr As SqlClient.SqlDataReader = objDetServBN.ConsultarxID(vintIDOperacion_Det, vintIDServicio_Det)
            Dim dr As SqlClient.SqlDataReader
            If vstrTipoProv = "" Then
                dr = objDetServBN.ConsultarxID(vintIDOperacion_Det, vintIDServicio_Det)
            Else
                dr = objDetServBN.ConsultarxIDTipoProv(vstrTipoProv, vintIDOperacion_Det, "AC", vstrIDServicio)
            End If


            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""
            Dim strCeldaVacia As String = "<td>&nbsp;</td>"

            While dr.Read
                If bln1erRegistro Then
                    strLine += "<tr>" & vbCrLf
                End If
                bln1erRegistro = False

                Dim dblTotalDolares As Double = CDbl(dr("Total").ToString)
                Dim dblTotalSoles As Double = CDbl(dr("Total").ToString)
                Dim sglTipoCambio As Single = CSng(dr("TipoCambio").ToString)

                If dr("IDMoneda").ToString = "USD" Then
                    dblTotalSoles = dblTotalSoles * sglTipoCambio
                ElseIf dr("IDMoneda").ToString = "SOL" Then
                    dblTotalDolares = dblTotalDolares / sglTipoCambio
                End If
                Dim strTD As String = "<td align='left'>"
                strLine += strTD & dr("Dia") & "</td>" & vbCrLf
                strTD = "<td align='left'>"
                strLine += strTD & dr("Hora") & "</td>" & vbCrLf
                strTD = "<td align='left'>"
                strLine += strTD & dr("DescServicio") & "</td>" & vbCrLf
                strTD = "<td align='center'>"
                strLine += strTD & dr("NroPax").ToString & "</td>" & vbCrLf
                strTD = "<td align='left'>"
                strLine += strTD & dr("IDIdioma").ToString & "</td>" & vbCrLf
                strTD = "<td align='right'>"
                strLine += strTD & Format(dblTotalDolares, "####0.00") & "</td>" & vbCrLf
                strTD = "<td align='right'>"
                strLine += strTD & Format(dblTotalSoles, "####0.00") & "</td>" & vbCrLf
                strLine += "</tr>" & vbCrLf

                rdblSumaDolares += dblTotalDolares
                rdblSumaSoles += dblTotalSoles
                If CBool(dr("ExistDiferPax")) Then
                    Dim objBnPax As New clsPaxDetalleOperacionBN
                    Dim dtt As DataTable = objBnPax.ConsultarDetPaxxIDOperacion(CInt(dr("IDOperacion_Det")))

                    strLine += "<tr>" & vbCrLf
                    strLine += vbTab & "<td colspan='6'>Pax: </td>" & vbCrLf
                    strLine += "</tr>" & vbCrLf

                    For Each drt As DataRow In dtt.Rows
                        strLine += "<tr>" & vbCrLf
                        strLine += vbTab & "<td colspan='6'>" & drt("NombreCompleto") & "</td>" & vbCrLf
                        strLine += "</tr>" & vbCrLf
                    Next
                End If


                strLine += "<tr>"
                strLine += strCeldaVacia & strCeldaVacia & strCeldaVacia
                strLine += "<td colspan='4'><fieldset><legend style='font-weight: bold;'>Observaci&oacute;n</legend>"
                strLine += If(dr("ObservBiblia").ToString = "", "&nbsp;", dr("ObservBiblia").ToString)
                strLine += "</fieldset></td>"
                strLine += "</tr>"

            End While
            dr.Close()
            strLine += "<tr>" & strCeldaVacia & "</tr>"

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoCorreoOrden(ByVal vintIDOperacion_Det As Integer, _
                                      ByVal vintIDServicio_Det As Integer, _
                                      ByVal vstrTipoProv As String, _
                                      ByRef rdblSumaTotalDol As Double, ByRef rdblSumaTotalSol As Double, _
                                      ByVal vstrIDServicio As String) As String
        Try
            Dim objDetServBN As New clsDetalleServiciosOperacionBN

            Dim dr As SqlClient.SqlDataReader
            If vstrTipoProv = "" Then
                dr = objDetServBN.ConsultarxID(vintIDOperacion_Det, vintIDServicio_Det)
            Else
                dr = objDetServBN.ConsultarxIDTipoProv(vstrTipoProv, vintIDOperacion_Det, "AC", vstrIDServicio)
            End If

            Dim strMoneda As String
            Dim strLine As String = ""
            While dr.Read
                strMoneda = ""
                Dim dblTotalDolares As Double = CDbl(dr("Total").ToString)
                Dim dblTotalSoles As Double = CDbl(dr("Total").ToString)
                Dim dblTipoCambio As Double = CDbl(dr("TipoCambio").ToString)
                If dr("IDMoneda").ToString = "USD" Then
                    strMoneda = "US$"
                    dblTotalSoles = dblTotalSoles * dblTipoCambio
                ElseIf dr("IDMoneda").ToString = "SOL" Then
                    strMoneda = "S/."
                    dblTotalDolares = dblTotalDolares / dblTipoCambio
                End If

                strLine += strCampoCorreoTexto(16, dr("Dia").ToString)
                strLine += strCampoCorreoTexto(7, dr("Hora").ToString)
                strLine += strCampoCorreoTexto(60, dr("DescServicio").ToString)
                strLine += strCampoCorreoTexto(5, dr("NroPax").ToString)
                strLine += strCampoCorreoTexto(14, dr("IDIdioma").ToString)
                strLine += StrDup(11 - Format(dblTotalDolares, "####0.00").ToString.Length, " ") & Format(dblTotalDolares, "####0.00")
                strLine += StrDup(11 - Format(dblTotalSoles, "####0.00").ToString.Length, " ") & Format(dblTotalSoles, "####0.00")

                rdblSumaTotalDol += dblTotalDolares
                rdblSumaTotalSol += dblTotalSoles
                If CBool(dr("ExistDiferPax")) Then
                    Dim objBnPax As New clsPaxDetalleOperacionBN
                    Dim dtt As DataTable = objBnPax.ConsultarDetPaxxIDOperacion(CInt(dr("IDOperacion_Det")))

                    strLine += vbCrLf & "Pax :" & vbCrLf
                    For Each drt As DataRow In dtt.Rows
                        strLine += drt("NombreCompleto") & vbCrLf
                    Next
                End If
                If Not CBool(dr("ExistDiferPax")) Then strLine += vbCrLf
                strLine += strCampoCorreoTexto(16, "Observación:")
                strLine += If(dr("ObservBiblia").ToString = "", "", dr("ObservBiblia").ToString) & vbCrLf
            End While
            dr.Close()

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListProveeExternosxIDCab(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Return objADO.GetDataTable(True, "OPERACIONES_DET_List_ProveeExternos", dc)

    End Function

    Public Function blnExistenOperaciones_DetxIDReserva_Det(ByVal vintIDReserva_Det As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_Sel_ExistenxIDReserva_DetOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsControlCalidadProBN
    Inherits clsOperacionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrIDFile As String, ByVal vstrProveedor As String, _
                                            ByVal vstrIDTipoProv As String, Optional ByVal vstrCliente As String = "", _
                                            Optional ByVal vblnFlHistorico As Boolean = False) As System.Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDFile", vstrIDFile)
                .Add("@Proveedor", vstrProveedor)
                .Add("@IDTipoProv", vstrIDTipoProv)
                .Add("@Cliente", vstrCliente)
                .Add("@FlHistorico", vblnFlHistorico)

            End With

            Return objADO.GetDataTable(True, "CONTROL_CALIDAD_PRO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vintNuControlCalidad As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuControlCalidad", vintNuControlCalidad)

            Return objADO.GetDataReader("CONTROL_CALIDAD_PRO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarResumen(ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataReader("CONTROL_CALIDAD_PRO_Sel_Resumen", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRpt(ByVal vstrIDCliente As String, ByVal vstrIDProveedor As String, _
                                           ByVal vdatInRango1 As Date, ByVal vdatInRango2 As Date, _
                                           ByVal vbytRatingDe As Byte, ByVal vbytRatingHasta As Byte, _
                                           ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@FecIngRango1", Format(vdatInRango1, "dd/MM/yyyy"))
                .Add("@FecIngRango2", Format(vdatInRango2, "dd/MM/yyyy"))
                .Add("@RatingDe", vbytRatingDe)
                .Add("@RatingHasta", vbytRatingHasta)
                .Add("@IDCab", vintIDCab)
            End With
            Return objADO.GetDataTable(True, "CONTROL_CALIDAD_PRO_MACUESTIONARIO_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    ' ''Public Overloads Function ConsultarListxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
    ' ''    Dim dc As New Dictionary(Of String, String)
    ' ''    Try
    ' ''        dc.Add("@IDProveedor", vstrIDProveedor)

    ' ''        Return objADO.GetDataTable(True, "CONTROL_CALIDAD_PRO_Sel_ListxIDProveedor", dc)
    ' ''    Catch ex As Exception
    ' ''        Throw
    ' ''    End Try
    ' ''End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsControlCalidadPro_CuestionarioBN
    Inherits clsOperacionBaseBN
    Public Overloads Function ConsultarListxID(ByVal vintNuControlCalidad As Integer, _
                                               ByVal vstrIDTipoProv As String, _
                                               ByVal vstrNombreProv As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuControlCalidad", vintNuControlCalidad)
                .Add("@IDTipoPrv", vstrIDTipoProv)
                .Add("@NombreProv", vstrNombreProv)
            End With

            Return objADO.GetDataTable(True, "CONTROL_CALIDAD_PRO_MACUESTIONARIO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsVoucherOperacionBN
    Inherits clsOperacionBaseBN

    Public Overrides Function ConsultarxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataReader("VOUCHER_OPERACIONES_SelxIDCab", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListVouchersxVAnulado(ByVal vintNuVoucher As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDVoucher", vintNuVoucher)

            Return objADO.GetDataTable(True, "VOUCHER_OPERACIONES_Sel_ListxVoucherAnulado", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxIDOperacion(ByVal vintIDOperacion As Integer, ByVal vstrIDTipoProveedor As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Dim dttReturn As New DataTable
        Try
            dc.Add("@IDOperacion", vintIDOperacion)

            If vstrIDTipoProveedor = "001" Then
                dttReturn = objADO.GetDataTable(True, "OPERACIONES_DET_SelVouchersHoteles", dc)
            Else
                If vstrIDTipoProveedor = "002" Then
                    dttReturn = objADO.GetDataTable(True, "OPERACIONES_DET_SelVouchersRestaurantes", dc)
                Else
                    dttReturn = objADO.GetDataTable(True, "OPERACIONES_DET_SelVouchers", dc)
                End If
            End If

            Return dttReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptVoucherMaster(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "ReporteVoucherMaster", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListVouchersxProveedor(ByVal vstrCoProveedor As String, Optional ByVal vstrCoMoneda As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrCoProveedor)
            dc.Add("@CoMoneda", vstrCoMoneda)

            Return objADO.GetDataTable(True, "VOUCHER_OPERACIONES_Sel_List_Proveedor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''PPMG20151027
    Public Function ConsultarVoucherxIDDetEstado(ByVal vintIDDet As Integer, ByVal vstrTipoTabla As String, ByVal vstrEstado As String) As String
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDDet", vintIDDet)
        dc.Add("@CoEstado", vstrEstado)
        dc.Add("@TipoTable", vstrTipoTabla)
        dc.Add("@pValido", "")

        Dim strEstadoVal As String = objADO.ExecuteSPOutput("VOUCHER_OPERACIONES_Val_VoucherxIDDetEstado", dc)

        Return strEstadoVal
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPaxDetalleOperacionBN
    Inherits clsOperacionBaseBN

    Public Function ConsultarDetPaxxIDOperacionDetLvw(ByVal vintIDOperacion_Det As Int32, _
                                                      ByVal vblnNoShow As Boolean) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@NoShow", vblnNoShow)

            Return objADO.GetDataReader("OPERACIONES_DET_PAX_Sel_xIDOperacionDet_Lvw", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarDetPaxxIDOperacion(ByVal vintIDOperacion_Det As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_PAX_SelxIDOperacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsSincerado_DetBN
    Inherits clsOperacionBaseBN

    Public Overloads Function ConsultarxNuSincerado(ByVal vintNuSincerado As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado", vintNuSincerado)

            Return objADO.GetDataTable(True, "SINCERADO_DET_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarListPresupuestoSobre(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                            ByVal vstrIDProveedor_PrgGuia As String, ByVal vintNuPreSob As Integer, _
                                                            ByVal vblnSoloModific As Boolean) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDProveedor_PrgGuia", vstrIDProveedor_PrgGuia)
                .Add("@NuPreSob", vintNuPreSob)
                .Add("@SoloModific", vblnSoloModific)
            End With

            Return objADO.GetDataTable(True, "SINCERADO_DET_SelEntradasPresupuestoSobre", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarListPresupuestoSobreTC(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, ByVal vintNuPreSob As Integer, _
                                                              ByVal vstrIDProvTour As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@NuPreSob", vintNuPreSob)
                .Add("@IDProvTourC", vstrIDProvTour)
            End With
            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_Sel_ListGenTourConductor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarListPresupuestoCusco(ByVal vintNuPres As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPres)

            Return objADO.GetDataTable(True, "PRESUPUESTOS_SOBRE_Sel_ListxNuPreSob", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsSincerado_Det_PaxBN
    Inherits clsOperacionBaseBN

    Public Overloads Function ConsultarxNuSinceradoDet(ByVal vintNuSincerado_Det As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado_Det", vintNuSincerado_Det)

            Return objADO.GetDataTable(True, "SINCERADO_DET_PAX_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleServiciosOperacionBN
    Inherits clsOperacionBaseBN

    Public Overloads Function ConsultarxID(ByVal vintIDOperacion_Det As Integer, _
                                            ByVal vintIDServicio_Det As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)

            Return objADO.GetDataReader("OPERACIONES_DET_DETSERVICIOS_SelxIDs", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDetallexIDServicio_Programacion(ByVal vstrIDServicio As String, ByVal vstrIDOperacion_Det As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", vstrIDServicio)
            'dc.Add("@Tipo", vstrTipo)
            'dc.Add("@Anio", vstrAnio)
            dc.Add("@IDOperacion_Det", vstrIDOperacion_Det)

            Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_ListxIDServicio_Prg", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarGuiasProgramados(ByVal vintIDCab As Integer, ByVal vstrIDProveedorInterno As String, _
                                              ByVal vstrIDTipoProv As String, ByVal vblnServicioTC As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedorInt", vstrIDProveedorInterno)
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@FlServicioTC", vblnServicioTC)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_SelGuiasProgramados", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnConsultarProvProgramado(ByVal vintIDCab As Integer, _
                                               ByVal vstrIDProveedorInt As String, _
                                              ByVal vstrIDProveedor_Prg As String, _
                                              ByVal vdatDia As Date) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedorInt", vstrIDProveedorInt)
            dc.Add("@IDProveedor_Prg", vstrIDProveedor_Prg)
            dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy"))

            dc.Add("@pOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_DETSERVICIOS_SelProvProg_Output", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPresupuestos_Programacion(ByVal vstrIDServicio As String, ByVal vintIDOperacion_Det As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_Sel_List_Presupuestos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListaPaxxIDOperacion_Det(ByVal vintIDOperacion_Det As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_Sel_List_PresupuestosPax", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListaServicios(ByVal vstrIDServicio As String, ByVal vstrAnio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@Anio", vstrAnio)

            Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_ListxIDServicioxAnio_Prg", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDetallexIDServicio_Programacion2(ByVal vstrIDServicio As String, ByVal vstrIDOperacion_Det As Integer, _
                                                              ByVal vblnOrdenServicioTourConductor As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@IDOperacion_Det", vstrIDOperacion_Det)

            If Not vblnOrdenServicioTourConductor Then
                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_ListxIDServicio_OrdenServicio", dc)
            Else
                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_ListxIDServicio_OrdenServicioTourConductor", dc)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPNRxIDOperacion_Det(ByVal vintIDCab As Integer, ByVal vdatFechaVuelo As DateTime) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@Dia", Format(vdatFechaVuelo, "dd/MM/yyyy"))

            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelPNRsxIDOperacion_Det", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxIDTipoProv(ByVal vstrIDTipoProv As String, _
                                         ByVal vstrIDOperacion_Det As Integer, _
                                         ByVal vstrIDEstado As String, _
                                         ByVal vstrIDServicio As String) As SqlClient.SqlDataReader

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@IDOperacion_Det", vstrIDOperacion_Det)
            dc.Add("@IDEstadoSolicitud", vstrIDEstado)
            dc.Add("@IDServicio", vstrIDServicio)

            Return objADO.GetDataReader("OPERACIONES_DET_DETSERVICIOS_SelxIDTipoProv", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarProveedoresProgramados(ByVal vstrIDSetours As String, ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSetours", vstrIDSetours)
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_SelProveedores_Prog", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDOperacionDetyIDServicio(ByVal vintIDOperacion_Det As Integer, ByVal vstrIDServicio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacionDet", vintIDOperacion_Det)
            dc.Add("@IDServicio", vstrIDServicio)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_SelxIDOperacionDetxIDServicio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarSincerado_SelxTipoProv(ByVal vintIDCab As Integer, _
                                   ByVal vstrIDProveedor As String, _
                                   ByVal vstrIDTipoProv As String) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDTipoProv", vstrIDTipoProv)
            End With

            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_SINCERADO_SelxTipoProv", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function blnConsultarPropinasPendientesProgramar(ByVal vintIDCab As Integer, _
                                   ByVal vstrIDProveedor As String) As Boolean

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@pExisten", 0)
            End With

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_DETSERVICIOS_SelPropinasSinAsignarOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function blnConsultarPropinasProgramadasProveedor(ByVal vintIDCab As Integer, _
                                   ByVal vstrIDProveedor As String, _
                                   ByVal vstrIDProveedor_Prg As String) As Boolean

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDProveedor_Prg", vstrIDProveedor_Prg)
                .Add("@pExisten", 0)
            End With

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_DETSERVICIOS_SelPropinasAsignadosProveedorOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxIDProveedor(ByVal vstrIDProveedor As String, _
                                     ByVal vintIDCab As Integer) As SqlClient.SqlDataReader

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab ", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataReader("OPERACIONES_DET_DETSERVICIOS_SelxIDProveedor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListProveeInternosxIDCab(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_List_ProveeInternos", dc)

    End Function

    Public Function ConsultarRptxGuiasTransportistas(ByVal vstrIDTipoProveedor As String, ByVal vstrIDProveedor As String, _
                                                     ByVal vstrIDUbigeo As String, ByVal vstrIDioma As String, ByVal vstrEstado As String, _
                                                     ByVal vdatFechaRango1 As Date, ByVal vdatFechaRango2 As Date, ByVal vbytNuVehiculo As Byte, Optional ByVal vstrCoPais As String = "") As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDTipoProv", vstrIDTipoProveedor)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDCiudad", vstrIDUbigeo)
                .Add("@IDIoma", vstrIDioma)
                .Add("@Estado", vstrEstado)
                .Add("@FecRango1", vdatFechaRango1)
                .Add("@FecRango2", vdatFechaRango2)
                .Add("@Nuvehiculo", vbytNuVehiculo)
                .Add("@CoPais", vstrCoPais)
            End With
            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_Sel_RptGuiasTransportistas", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListSubServiciosxIDOperacionDet(ByVal vintIDOperacion_Det As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_Sel_ListxIDOperacionDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'para recepcion de documentos
    Public Function ConsultarListSubServiciosxIDOperacionDet_IDServicio(ByVal vintIDOperacion_Det As Integer, ByVal vstrIDServicio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDServicio", vstrIDServicio)
            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_Sel_ListxIDOperacionDet_IDServicio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListGenerarPresCusco(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                  ByVal vintNuPreSob As Integer) As DataTable

        'ByVal vstrProvGuia As String, _
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            'dc.Add("@GuiaProv", vstrProvGuia)
            dc.Add("@NuPreSob", vintNuPreSob)

            Return objADO.GetDataTable(True, "OPERACIONES_DET_DETSERVICIOS_Sel_ListGenPresCusco", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class
