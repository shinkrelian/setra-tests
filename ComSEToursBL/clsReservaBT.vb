﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports System.Globalization

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsReservaBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Dim gstrPeru As String = "000323"
    Private gstrTipoProvHotel As String = "001"
    Private gstrTipoProvRestaurante As String = "002"
    Private gstrTipoProvOper As String = "003"
    Private gstrTipoProvTransp As String = "004"
    Private gsglIGV As Single = 18
    Private gstrIDProveedorPeruRail As String = "001836"
    Private gstrIDProveedorSetoursLima As String = "001554"
    Private gstrIDProveedorSetoursCusco As String = "000544"
    Private gstrIDProveedorSetoursBuenosAires As String = "002315"
    Private gstrIDProveedorSetoursSantiago As String = "002317"
    Private gstrIDTipoOperInterno As String = "I"
    Private gstrIDTipoOperExterno As String = "E"
    Private strNombreClase As String = "clsReservaBT"
    ''PPMG20160330
    Private strIDProveedorZicasso As String = "003557"

    Private Structure stDetReservas
        Dim IDCab As Integer
        Dim IDDet As Integer
        Dim IDReserva_Det As Integer
        Dim IDProveedor As String
        Dim Servicio As String
        Dim NroPax As Int16
        Dim NroPaxUpd As Int16
        Dim UserMod As String
    End Structure

    Public Structure stProveedoresReservas
        Dim IDReserva As Integer
        Dim IDProveedor As String
        Dim EsAlojamiento As Boolean
        Dim EsPlanAlimenticio As Boolean
        Dim RegeneradoxAcomodo As Boolean
    End Structure

    Private _ListaProvReservas As List(Of stProveedoresReservas)
    Public Property ListaProvReservas() As List(Of stProveedoresReservas)
        Get
            Return _ListaProvReservas
        End Get
        Set(ByVal value As List(Of stProveedoresReservas))
            _ListaProvReservas = value
        End Set
    End Property

    Private Structure stReservasFechaOut
        Dim IDDet1ero As Integer
        Dim IDDet As Integer
        Dim IDReserva_Det As Integer
        Dim IDReserva_Det1ero As Integer
        Dim FechaOut As Date
        Dim ConAlojamiento As Boolean
    End Structure

    Private Structure stAcomodoEspecial
        Dim CoCapacidad As String
        Dim QtCapacidad As Byte
        Dim QtHabitaciones As Byte
        Dim EsMatrimonial As Boolean
        Dim EsSevicioGuia As Boolean
        Dim PorAtender As Boolean
    End Structure

    Private Function datFechaMenorProveedorDetCotizacion(ByVal BECot As clsCotizacionBE, ByVal vstrIDProveedor As String) As Date
        Try

            Dim datMenor As Date = "31/12/9999"

            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                If Item.IDProveedor = vstrIDProveedor Then
                    If Item.Dia < datMenor Then
                        datMenor = Item.Dia
                    End If

                End If
            Next

            Return datMenor

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function LlenarBECot(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, ByVal vstrUserMod As String) As clsCotizacionBE
        Try
            Dim objBECot As New clsCotizacionBE

            Using objBLCot As New clsCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBLCot.ConsultarPk(vintIDCab)
                    dr.Read()
                    objBECot.IdCab = vintIDCab
                    objBECot.Simple = If(IsDBNull(dr("Simple")) = True, 0, dr("Simple"))
                    objBECot.Twin = If(IsDBNull(dr("Twin")) = True, 0, dr("Twin"))
                    objBECot.Matrimonial = If(IsDBNull(dr("Matrimonial")) = True, 0, dr("Matrimonial"))
                    objBECot.Triple = If(IsDBNull(dr("Triple")) = True, 0, dr("Triple"))
                    objBECot.UserMod = vstrUserMod
                    dr.Close()
                End Using
            End Using

            Dim objItem As New clsCotizacionBE.clsDetalleCotizacionBE
            Dim ListaDetalleCotiz As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Using objBLDetCot As New clsCotizacionBN.clsDetalleCotizacionBN
                Using drDet As SqlClient.SqlDataReader = objBLDetCot.ConsultarTodosxIDCabProveedorLog(vintIDCab, vstrIDProveedor)
                    While drDet.Read
                        With objItem
                            .Almuerzo = drDet("Almuerzo")
                            .Cena = drDet("Cena")
                            .CostoLiberado = drDet("CostoLiberado")
                            .CostoLiberadoImpto = drDet("CostoLiberadoImpto")
                            .CostoReal = drDet("CostoReal")
                            .CostoRealAnt = If(IsDBNull(drDet("CostoRealAnt")) = True, 0, drDet("CostoRealAnt"))
                            .CostoRealImpto = drDet("CostoRealImpto")
                            .Cotizacion = drDet("Cotizacion")
                            .Desayuno = drDet("Desayuno")
                            .Dia = drDet("Dia")
                            .Especial = drDet("Especial")
                            .IDCab = drDet("IDCab")
                            .IDDet = drDet("IDDet")
                            .IDDetCopia = If(IsDBNull(drDet("IDDetCopia")) = True, 0, drDet("IDDetCopia"))
                            .IDDetRel = If(IsDBNull(drDet("IDDetRel")) = True, 0, drDet("IDDetRel"))
                            .IDDetTransferDes = If(IsDBNull(drDet("IDDetTransferDes")) = True, 0, drDet("IDDetTransferDes"))
                            .IDDetTransferOri = If(IsDBNull(drDet("IDDetTransferOri")) = True, 0, drDet("IDDetTransferOri"))
                            .IDIdioma = drDet("IDIdioma")
                            .IDProveedor = drDet("IDProveedor")
                            .IDServicio = drDet("IDServicio")
                            .IDServicio_Det = drDet("IDServicio_Det")
                            .IDTipoProv = drDet("IDTipoProv")
                            .IDUbigeo = drDet("IDUbigeo")
                            .IDUbigeoDes = drDet("IDUbigeoDes")
                            .IDUbigeoOri = drDet("IDUbigeoOri")
                            .Item = drDet("Item")
                            .Lonche = drDet("Lonche")
                            .Margen = drDet("Margen")
                            .MargenAplicado = drDet("MargenAplicado")
                            .MargenAplicadoAnt = If(IsDBNull(drDet("MargenAplicadoAnt")) = True, 0, drDet("MargenAplicadoAnt"))
                            .MargenImpto = drDet("MargenImpto")
                            .MargenLiberado = drDet("MargenLiberado")
                            .MargenLiberadoImpto = drDet("MargenLiberadoImpto")
                            .MotivoEspecial = If(IsDBNull(drDet("MotivoEspecial")) = True, "", drDet("MotivoEspecial"))
                            .NroLiberados = If(IsDBNull(drDet("NroLiberados")) = True, 0, drDet("NroLiberados"))
                            .NroPax = drDet("NroPax")
                            .Recalcular = drDet("Recalcular")
                            .RutaDocSustento = If(IsDBNull(drDet("RutaDocSustento")) = True, "", drDet("RutaDocSustento"))
                            .Servicio = drDet("Servicio")
                            .SSCL = drDet("SSCL")
                            .SSCLImpto = drDet("SSCLImpto")
                            .SSCR = drDet("SSCR")
                            .SSCRImpto = drDet("SSCRImpto")
                            .SSMargen = drDet("SSMargen")
                            .SSMargenImpto = drDet("SSMargenImpto")
                            .SSMargenLiberado = drDet("SSMargenLiberado")
                            .SSMargenLiberadoImpto = drDet("SSMargenLiberadoImpto")
                            .SSTotal = drDet("SSTotal")
                            .SSTotalOrig = drDet("SSTotalOrig")
                            .STCR = drDet("STCR")
                            .STCRImpto = drDet("STCRImpto")
                            .STMargen = drDet("STMargen")
                            .STMargenImpto = drDet("STMargenImpto")
                            .STTotal = drDet("STTotal")
                            .STTotalOrig = drDet("STTotalOrig")
                            .Tipo_Lib = If(IsDBNull(drDet("Tipo_Lib")) = True, "", drDet("Tipo_Lib"))
                            .TipoTransporte = If(IsDBNull(drDet("TipoTransporte")) = True, "", drDet("TipoTransporte"))
                            .Total = drDet("Total")
                            .TotalOrig = drDet("TotalOrig")
                            .TotImpto = drDet("TotImpto")
                            .Transfer = drDet("Transfer")
                            .UserMod = vstrUserMod
                        End With
                        ListaDetalleCotiz.Add(objItem)
                    End While
                    drDet.Close()
                End Using
            End Using

            objBECot.ListaDetalleCotiz = ListaDetalleCotiz

            Return objBECot
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarProveedoresHotelesyAlojamiento(ByVal vintIDCab As Int32) As DataTable

        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("RESERVAS_SelProveeHoteles", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarProveedores(ByVal vintIDCab As Int32) As DataTable

        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("RESERVAS_SelProvee", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveHTMLCorreo(ByVal vintIDReserva As Int32, ByVal vstrIDEmail As String, ByVal vblnGastoTransferencias As Boolean, _
                                          Optional ByVal vblnTipoProvSelHotel As Boolean = True, Optional ByVal vstrIDTipoProv As String = "", Optional ByVal vblnConAlojamiento As Boolean = False) As String
        Try
            Dim objBT As New clsReservaBT.clsDetalleReservaBT
            Dim dtt As DataTable = objBT.ConsultarxIDReserva(vintIDReserva, vstrIDEmail, vblnGastoTransferencias)


            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""

            strLine += strDevuelveCabezeraHTMLReservas(vblnTipoProvSelHotel, vstrIDTipoProv, vblnConAlojamiento)

            For Each dr As DataRow In dtt.Rows
                strLine += "<tr>" & vbCrLf
                If vblnTipoProvSelHotel Then
                    strLine += "<td>" & dr("FechaIn").ToString & "</td>" & vbCrLf
                    strLine += "<td>" & dr("FechaOut").ToString & "</td>" & vbCrLf
                    strLine += "<td align='center'>" & dr("Cantidad").ToString & "</td>" & vbCrLf
                    strLine += "<td>" & dr("Servicio").ToString & "</td>" & vbCrLf

                    ' If Not (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                    strLine += "<td>" & dr("CodTarifa").ToString & "</td>" & vbCrLf

                    If (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                        strLine += "<td>" & dr("Tipo").ToString & "</td>" & vbCrLf
                    End If
                    'strLine += "<td>" & dr("CodTarifa").ToString & "</td>" & vbCrLf

                    strLine += "<td align='center'>" & dr("CantidadPax").ToString & "</td>" & vbCrLf

                    If Not (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                        strLine += "<td align='center'>" & dr("Desayuno").ToString & "</td>" & vbCrLf
                    End If

                    If (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                        strLine += "<td>" & dr("Idio").ToString & "</td>" & vbCrLf
                    End If
                Else
                    strLine += "<td>" & dr("FechaIn").ToString & "</td>" & vbCrLf
                    strLine += "<td>" & dr("Hora").ToString & "</td>" & vbCrLf
                    strLine += "<td>" & dr("Servicio").ToString & "</td>" & vbCrLf
                    strLine += "<td>" & dr("CodTarifa").ToString & "</td>" & vbCrLf
                    strLine += "<td align='center'>" & dr("Tipo").ToString & "</td>" & vbCrLf
                    strLine += "<td align='center'>" & dr("CantidadPax").ToString & "</td>" & vbCrLf
                    strLine += "<td>" & dr("Idio").ToString & "</td>" & vbCrLf
                End If
                strLine += "</tr>" & vbCrLf

                If CBool(dr("ExisteDifPax")) And Not vblnTipoProvSelHotel Then
                    strLine += "<tr>"
                    strLine += "<td colspan='7'> PAX: " & dr("NombrePaxsCompletos").ToString() & "</td>"
                    strLine += "</tr>"
                    'Dim sbDetPax As DataTable = objBT.ConsultarPaxxIDReserva_Det(CInt(dr("IDReserva_Det")))
                    'Dim blnTitulo As Boolean = False
                    'For Each dr2 As DataRow In sbDetPax.Rows
                    '    If Not blnTitulo Then
                    '        strLine += "<tr>" & vbCrLf
                    '        strLine += "<td colspan='8'>Pax:</td>" & vbCrLf
                    '        strLine += "</tr>" & vbCrLf
                    '    End If

                    '    strLine += "<tr>" & vbCrLf
                    '    strLine += "<td colspan='8'>" & dr2("NombreCompleto") & "</td>" & vbCrLf
                    '    strLine += "</tr>" & vbCrLf
                    '    blnTitulo = True
                    'Next


                End If

            Next
            strLine += vbCrLf & "</table>"
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function strDevuelveTextoCorreo(ByVal vintIDReserva As Int32, ByVal vstrIDEmail As String, ByVal vblnGastoTransferencias As Boolean, _
                                           Optional ByVal vblnTipoProvSelHotel As Boolean = True, Optional ByVal vstrIDTipoProv As String = "", Optional ByVal vblnConAlojamiento As Boolean = False) As String
        Try
            Dim objBT As New clsReservaBT.clsDetalleReservaBT
            Dim dtt As DataTable = objBT.ConsultarxIDReserva(vintIDReserva, vstrIDEmail, vblnGastoTransferencias)

            Dim strLine As String = ""
            If vblnTipoProvSelHotel Then
                strLine += strCampoCorreoTexto(11, "Fecha In")
                strLine += strCampoCorreoTexto(11, "Fecha Out")
                strLine += strCampoCorreoTexto(6, "N°Hab")
                strLine += strCampoCorreoTexto(57, "Detalle de Servicio")

                strLine += strCampoCorreoTexto(50, "Cód. Tarifa")
                'jorge
                If (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                    strLine += strCampoCorreoTexto(5, "Tipo")
                End If

                strLine += strCampoCorreoTexto(7, "Pax")

                If Not (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                    strLine += strCampoCorreoTexto(22, "Desayuno")
                Else
                    strLine += strCampoCorreoTexto(3, "Id.")
                End If

            Else
                strLine += strCampoCorreoTexto(11, "Fecha")
                strLine += strCampoCorreoTexto(6, "Hora")
                strLine += strCampoCorreoTexto(79, "Detalle de Servicio")

                strLine += strCampoCorreoTexto(30, "Cód. Tarifa")

                strLine += strCampoCorreoTexto(5, "Tipo")
                strLine += strCampoCorreoTexto(10, "Pax")
                strLine += strCampoCorreoTexto(3, "Id.")
            End If
            strLine += vbCrLf
            'strLine += StrDup(114, "*") & vbCrLf
            strLine += StrDup(150, "*") & vbCrLf

            Dim bytFilasPax As Byte = 0
            For Each dr As DataRow In dtt.Rows
                If vblnTipoProvSelHotel Then
                    strLine += strCampoCorreoTexto(11, dr("FechaIn").ToString)
                    strLine += strCampoCorreoTexto(11, dr("FechaOut").ToString)
                    strLine += strCampoCorreoTexto(6, CInt(dr("Cantidad")).ToString)
                    strLine += strCampoCorreoTexto(57, dr("Servicio").ToString)

                    'jorge

                    strLine += strCampoCorreoTexto(50, dr("CodTarifa").ToString)

                    If (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                        strLine += strCampoCorreoTexto(5, dr("Tipo").ToString)
                    End If


                    strLine += strCampoCorreoTexto(7, dr("CantidadPax").ToString)

                    If Not (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                        strLine += strCampoCorreoTexto(22, dr("Desayuno").ToString)
                    End If

                    If (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                        strLine += strCampoCorreoTexto(5, dr("Idio").ToString)
                    End If

                Else
                    strLine += strCampoCorreoTexto(11, dr("FechaIn").ToString)
                    strLine += strCampoCorreoTexto(6, dr("Hora").ToString)
                    strLine += strCampoCorreoTexto(79, dr("Servicio").ToString)
                    strLine += strCampoCorreoTexto(30, dr("CodTarifa").ToString)
                    strLine += strCampoCorreoTexto(5, dr("Tipo").ToString)
                    strLine += strCampoCorreoTexto(10, dr("CantidadPax").ToString)
                    strLine += strCampoCorreoTexto(3, dr("Idio").ToString)
                End If

                strLine += vbCrLf
                If CBool(dr("ExisteDifPax")) And Not vblnTipoProvSelHotel Then
                    Dim strNombresPax As String = dr("NombrePaxsCompletos").ToString()
                    If strNombresPax.Length < 144 Then
                        strLine += "PAX : " & strNombresPax & vbCrLf
                    Else
                        Dim ListLineasPax As List(Of String) = LstDevuelveLineasMaximoLetras(strNombresPax, 144, 150)
                        Dim blnFirtLine As Boolean = False
                        For Each ItemPax As String In ListLineasPax
                            If Not blnFirtLine Then
                                strLine += "PAX : " & ItemPax & vbCrLf
                                blnFirtLine = True
                            Else
                                strLine += ItemPax & vbCrLf
                            End If
                        Next
                    End If
                End If

            Next

            'strLine += StrDup(114, "*") & vbCrLf
            strLine += StrDup(150, "*") & vbCrLf

            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function LstDevuelveLineasMaximoLetras(ByVal vstrPalabra As String, ByVal vintLetrasporLinea As Int16, _
                                                   Optional ByVal vintLetrasSecundarias As Int16 = 0) As List(Of String)
        Try
            Dim blnCompleto As Boolean = False
            Dim strCadenaLocal As String = "", LstCadenas As New List(Of String)
            Dim intMaxxLinea As Int16 = vintLetrasporLinea
            Dim arrCadena() As Char = vstrPalabra.ToCharArray

ReEval:
            If LstCadenas.Count > 0 Then
                vstrPalabra = Replace(vstrPalabra, strCadenaLocal, String.Empty)
                If vintLetrasSecundarias > 0 Then vintLetrasporLinea = vintLetrasSecundarias
                arrCadena = vstrPalabra.ToCharArray
                strCadenaLocal = String.Empty

                If arrCadena.Length < vintLetrasporLinea Then
                    LstCadenas.Add(vstrPalabra)
                    blnCompleto = True
                End If
            End If

            If Not blnCompleto Then
                For i As Int16 = 0 To arrCadena.Length - 1
                    If i < vintLetrasporLinea Then
                        strCadenaLocal &= arrCadena(i).ToString
                    Else
                        LstCadenas.Add(strCadenaLocal)
                        GoTo ReEval
                    End If
                Next
            End If
            Return LstCadenas
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strCampoCorreoTexto(ByVal vbytTamanioColumna As Byte, ByVal vstrValor As String) As String
        Try
            Dim strValorRet As String = ""

            If vbytTamanioColumna <= vstrValor.Trim.Length Then
                strValorRet = vstrValor.Substring(0, vbytTamanioColumna)
            Else
                strValorRet = vstrValor.Trim & Space(vbytTamanioColumna - vstrValor.Trim.Length)
            End If

            Return strValorRet
        Catch ex As System.Exception
            Throw
        End Try

    End Function
    Public Function strDevuelveHTMLCorreoDetBackup(ByVal vintIDReserva As Int32, ByVal dttBup As DataTable) As String
        Try

            'Dim dtt As DataTable = ConsultarDetalleBackupeado(vintIDReserva)


            Dim bln1erRegistro As Boolean = True
            Dim strLine As String = ""

            For Each dr As DataRow In dttBup.Rows
                'While dr.Read
                If bln1erRegistro Then
                    strLine += "<tr>" & vbCrLf
                End If
                bln1erRegistro = False
                Dim strTD As String = "<td align='left'>"
                strLine += strTD & dr("FechaIn") & "</td>" & vbCrLf
                strTD = "<td align='left'>"
                strLine += strTD & dr("FechaOut") & "</td>" & vbCrLf
                strTD = "<td align='center'>"
                strLine += strTD & dr("Cantidad") & "</td>" & vbCrLf
                strTD = "<td align='left'>"

                strLine += strTD & dr("Servicio") & "</td>" & vbCrLf
                strTD = "<td align='center'>"
                strLine += strTD & dr("Noches") & "</td>" & vbCrLf

                strTD = "<td align='center'>"
                strLine += strTD & dr("NroPax").ToString & "</td>" & vbCrLf


                strTD = "<td align='left'>"
                strLine += strTD & dr("CodReservaProv") & "</td>" & vbCrLf
                strLine += "</tr>" & vbCrLf
                'End While
            Next
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function strDevuelveTextoCorreoDetBackup(ByVal vintIDReserva As Int32, ByVal dttBup As DataTable) As String
        Try

            Dim strLine As String = ""

            For Each dr As DataRow In dttBup.Rows
                'Dim strServicio As String
                strLine += strCampoCorreoTexto(16, dr("FechaIn"))
                strLine += strCampoCorreoTexto(16, dr("FechaOut"))
                strLine += strCampoCorreoTexto(10, dr("Cantidad").ToString)
                'If dr("Servicio").ToString.Length >= 50 Then
                '    strServicio = dr("Servicio").ToString.Substring(0, 50)
                'Else
                '    strServicio = dr("Servicio").ToString & Space(50 - dr("Servicio").ToString.Length)
                'End If
                'strLine += strServicio
                strLine += strCampoCorreoTexto(50, dr("Servicio").ToString)
                strLine += strCampoCorreoTexto(8, dr("Noches").ToString)
                strLine += strCampoCorreoTexto(3, dr("NroPax").ToString)
                strLine += strCampoCorreoTexto(25, dr("CodReservaProv").ToString)

            Next
            Return strLine
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GrabarxCotizacionyProveedor(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                ByVal vintIDReserva As Integer, _
                                                ByVal vblnModificarReserva As Boolean, _
                                                ByVal vstrUserMod As String, _
                                                ByVal vblnHTML As Boolean) As String
        Try


            'Dim dc As New Dictionary(Of String, String)
            'dc.Add("@IDProveedor", vstrIDProveedor)
            'dc.Add("@IDCab", vintIDCab)
            'Dim dr As SqlClient.SqlDataReader = objADO.GetDataReader("COTIDET_LOG_SelxIDProveedor", dc)
            'Dim dtt As DataTable = objADO.GetDataTable("COTIDET_LOG_SelxIDProveedor", dc)

            Dim objBN As New clsReservaBN
            Dim dtt As DataTable = objBN.ConsultarLogVentas(vintIDCab, vstrIDProveedor)

            Dim objDetBT As New clsReservaBT.clsDetalleReservaBT

            'While dr.Read
            'objDetBT.EliminarxIDDet(dr("IDDet"))
            'End While

            If vblnModificarReserva Then BackupearDetalleEliminado(vintIDReserva, vstrUserMod)

            For Each dr As DataRow In dtt.Rows

                objDetBT.EliminarxIDDet(dr("IDDet"))
            Next

            'GrabarxCotizacion(BECot, vintIDReserva)

            Dim strHtml As String = ""

            If vblnModificarReserva Then
                Dim BECot As clsCotizacionBE = LlenarBECot(vintIDCab, vstrIDProveedor, vstrUserMod)
                'GrabarxCotizacion(BECot, vintIDReserva)
                objDetBT.ActualizaEstadoLogCotizacionxProveedor(vintIDCab, True, vstrIDProveedor, vstrUserMod)

                'Cambio de estados de reserva
                If blnExisteDetalle(vintIDReserva) Then
                    ActualizarEstado(vintIDReserva, "NV", vstrUserMod, "", "")
                Else
                    ActualizarEstado(vintIDReserva, "XL", vstrUserMod, "", "")
                End If

            End If

            If vblnHTML Then
                strHtml = strDevuelveHTMLCorreo(vintIDReserva, "", True)
            Else
                strHtml = strDevuelveTextoCorreo(vintIDReserva, "", True)
            End If

            ContextUtil.SetComplete()

            Return strHtml
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Function blnExisteDetalle(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pExiste", 0)

            Return objADO.ExecuteSPOutput("RESERVAS_DET_Sel_ExisteOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub GrabarxCotizacionNuevosProveedores(ByVal BECot As clsCotizacionBE)
        Try
            Dim objBERes As clsReservaBE
            Dim objTareasBE As clsReservaBE.clsTareasReservaBE
            Dim ListaTareasRes As New List(Of clsReservaBE.clsTareasReservaBE)
            Dim ListaProveedTareas As New List(Of String)
            Dim ListaProveed As New List(Of String)

            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                If Item.Reservado Then GoTo Sgte
                If ListaProveed.Contains(Item.IDProveedor) Then GoTo Sgte


                objBERes = New clsReservaBE
                With objBERes
                    .IDProveedor = Item.IDProveedor
                    .IDCab = BECot.IdCab
                    .Estado = "NV"
                    .EstadoSolicitud = "RECR"
                    .IDUsuarioVen = BECot.IDUsuario
                    .IDUsuarioRes = BECot.IDUsuarioRes
                    .IDFile = BECot.IDFile
                    .Observaciones = ""
                    .CodReservaProv = ""


                    If Not ListaProveedTareas.Contains(Item.IDProveedor) Then
                        Dim datMenor As Date = datFechaMenorProveedorDetCotizacion(BECot, Item.IDProveedor)
                        Dim ListaUltimoMinuto As New List(Of Char)
                        Dim dc As Dictionary(Of String, Date) = dicDevuelveFechasDeadLine(Item.IDProveedor, _
                                                                                          datMenor, BECot.NroPax + BECot.NroLiberados, ListaUltimoMinuto)
                        For Each dic As KeyValuePair(Of String, Date) In dc
                            objTareasBE = New clsReservaBE.clsTareasReservaBE
                            Select Case dic.Key
                                Case "FecDeadLRoom1"
                                    objTareasBE.IDTarea = 1
                                Case "FecDeadLRoom2"
                                    objTareasBE.IDTarea = 2
                                Case "FecDeadLRoom3"
                                    objTareasBE.IDTarea = 3
                                Case "FecDeadLPrepag1"
                                    objTareasBE.IDTarea = 4
                                Case "FecDeadLPrepag2"
                                    objTareasBE.IDTarea = 5
                                Case "FecDeadLSaldo"
                                    objTareasBE.IDTarea = 6
                            End Select
                            objTareasBE.FecDeadLine = dic.Value
                            objTareasBE.IDEstado = "PD"
                            objTareasBE.IDCab = BECot.IdCab
                            objTareasBE.Accion = "N"
                            objTareasBE.UltimoMinuto = False
                            If objTareasBE.FecDeadLine <> "01/01/1900" Then
                                ListaTareasRes.Add(objTareasBE)
                            End If

                            objTareasBE.Descripcion = strDevuelveDescripcionTareaPolitica(objTareasBE.IDTarea)
                        Next

                        ListaProveedTareas.Add(Item.IDProveedor)
                        objBERes.ListaUltimoMinuto = ListaUltimoMinuto

                    End If
                    .UserMod = BECot.UserMod
                End With
                objBERes.ListaTareasReservas = ListaTareasRes


                ListaProveed.Add(Item.IDProveedor)

                Dim intIDReserva As Integer = InsertarxCotizacion(objBERes, BECot, 0)

Sgte:
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub GrabarxCotizacionOperadoresConAlojamiento(ByVal BECot As clsCotizacionBE, ByVal vbytCapacMax As Byte, ByVal vintIDReservaCambiosLog As Integer)
        Try

            Dim blnUnTipoHabitacion As Boolean = False
            Dim blnMatrim As Boolean = False
            'Dim blnTwinMatrim As Boolean = False
            Dim intCantDobles As Int16 = 1
            Dim intCantDoblesCont As Int16

            If BECot.Matrimonial <> 0 And BECot.Twin <> 0 Then
                'blnTwinMatrim = True
                intCantDobles = 2
                'Else
                '    If BECot.Matrimonial <> 0 And BECot.Twin = 0 Then
                '        blnMatrim = True
                '    End If

            End If
            intCantDoblesCont = intCantDobles


            Dim objBERes As clsReservaBE
            'Dim objBEResTotal As New clsReservaBE

            Dim objBEDetRes As clsReservaBE.clsDetalleReservaBE

            Dim objBEDetServRes As clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE
            'Dim objBEDetPaxRes As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE
            'Dim objCotiPaxBT As New clsCotizacionBT.clsCotizacionPaxBT
            Dim objTareasBE As clsReservaBE.clsTareasReservaBE

            Dim ListaDetRes As New List(Of clsReservaBE.clsDetalleReservaBE)
            Dim ListaDetResTotal As New List(Of stDetReservas)
            Dim ListaDetResTotal2 As New List(Of stDetReservas)
            Dim ListaDetServRes As New List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE)
            Dim ListaDetPaxRes As New List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
            Dim ListaTareasRes As New List(Of clsReservaBE.clsTareasReservaBE)

            Dim ListaProveed As New List(Of stProveedoresReservas)
            Dim ListaProveedTareas As New List(Of String)
            Dim ListaPax As New List(Of String)
            Dim strDescServicio As String = ""
            Dim bytSaldoPaxHotel As Byte = 0
            Dim blnMismoHotel As Boolean = False
            Dim intIDReserva As Int32 = 0
            Dim bln1ero As Boolean
            Dim bytCont As Byte
            Dim blnExisteServ As Boolean

            'Dim dat1eraFecha As Date '= "01/01/1900"
            Dim datFechaDiaAnt As Date '= "01/01/1900"
            Dim bytContVeces As Byte

            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                If Not (Item.IDTipoProv = gstrTipoProvOper And Item.ConAlojamiento) Then GoTo Sgte

                Dim blnExisteProvee As Boolean = False
                For Each ST As stProveedoresReservas In ListaProveed
                    If ST.IDProveedor = Item.IDProveedor Then
                        blnExisteProvee = True
                        Exit For
                    End If
                Next
                If Not blnExisteProvee Then
                    intIDReserva = 0
                Else
                    For Each ST As stProveedoresReservas In ListaProveed
                        If ST.IDProveedor = Item.IDProveedor Then

                            Exit For
                        End If
                    Next
                End If

                bln1ero = True
BorrarPaxGrabados:
                For intInd = 0 To ListaPax.Count - 1
                    ListaPax.RemoveAt(intInd)
                    GoTo BorrarPaxGrabados
                Next

                bytCont = 0
                Dim intNroPax As Int16
                For Each Item2 As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                    intNroPax = Item2.NroPax
                    bytContVeces = 0
                    'Dim bytSaldoPaxHotel2 As Byte = Item2.NroPax

                    If Item2.IDProveedor = Item.IDProveedor Then
                        objBERes = New clsReservaBE
                        With objBERes
                            .IDProveedor = Item2.IDProveedor
                            .IDCab = BECot.IdCab
                            .Estado = "NV"
                            .EstadoSolicitud = "RECR"
                            .IDUsuarioVen = BECot.IDUsuario
                            .IDUsuarioRes = "0000"
                            .Observaciones = ""
                            .CodReservaProv = ""

                            If Not ListaProveedTareas.Contains(Item.IDProveedor) Then 'And vintIDReservaCambiosLog = 0 Then
                                Dim datMenor As Date = datFechaMenorProveedorDetCotizacion(BECot, Item.IDProveedor)
                                Dim ListaUltimoMinuto As New List(Of Char)
                                Dim dc As Dictionary(Of String, Date) = dicDevuelveFechasDeadLine(Item.IDProveedor, _
                                                                                                  datMenor, BECot.NroPax + BECot.NroLiberados, ListaUltimoMinuto)
                                For Each dic As KeyValuePair(Of String, Date) In dc
                                    objTareasBE = New clsReservaBE.clsTareasReservaBE
                                    Select Case dic.Key
                                        Case "FecDeadLRoom1"
                                            objTareasBE.IDTarea = 1
                                        Case "FecDeadLRoom2"
                                            objTareasBE.IDTarea = 2
                                        Case "FecDeadLRoom3"
                                            objTareasBE.IDTarea = 3
                                        Case "FecDeadLPrepag1"
                                            objTareasBE.IDTarea = 4
                                        Case "FecDeadLPrepag2"
                                            objTareasBE.IDTarea = 5
                                        Case "FecDeadLSaldo"
                                            objTareasBE.IDTarea = 6
                                    End Select
                                    objTareasBE.FecDeadLine = dic.Value
                                    objTareasBE.IDEstado = "PD"
                                    objTareasBE.IDCab = BECot.IdCab
                                    objTareasBE.Accion = "N"
                                    objTareasBE.UltimoMinuto = False
                                    If objTareasBE.FecDeadLine <> "01/01/1900" Then
                                        ListaTareasRes.Add(objTareasBE)
                                    End If
                                    'objTareasBE.Descripcion = dic.Key
                                    objTareasBE.Descripcion = strDevuelveDescripcionTareaPolitica(objTareasBE.IDTarea)
                                Next

                                ListaProveedTareas.Add(Item.IDProveedor)
                                objBERes.ListaUltimoMinuto = ListaUltimoMinuto

                            End If
                            .UserMod = BECot.UserMod
                        End With
                        objBERes.ListaTareasReservas = ListaTareasRes

                        'Reservas_Det
Reservas_Det:

                        objBEDetRes = New clsReservaBE.clsDetalleReservaBE

                        strDescServicio = ""
                        Dim blnHotel As Boolean = False

                        If (Not blnUnTipoHabitacion And Item2.ConAlojamiento And Item2.IDTipoProv = gstrTipoProvOper) Then
                            blnHotel = True
                            If Item2.IncGuia = 0 Then
                                If Not blnMismoHotel Then bytSaldoPaxHotel = intNroPax
                                If bytSaldoPaxHotel > vbytCapacMax Then
                                    If intCantDoblesCont = 2 Then
                                        blnMatrim = True
                                    Else
                                        blnMatrim = False
                                    End If
                                    strDescServicio = Item2.Servicio & " - " & strDescCapacidadHabitacion(vbytCapacMax, blnMatrim)
                                    bytSaldoPaxHotel = bytSaldoPaxHotel - vbytCapacMax

                                    intNroPax = vbytCapacMax
                                Else
                                    If vbytCapacMax = 3 Then
                                        If 2 = bytSaldoPaxHotel Then
                                            If intCantDoblesCont = 2 Then
                                                blnMatrim = True
                                            Else
                                                blnMatrim = False
                                            End If
                                            strDescServicio = Item2.Servicio & " - " & strDescCapacidadHabitacion(2, blnMatrim)

                                            intNroPax = 2
                                        ElseIf 1 = bytSaldoPaxHotel Then
                                            strDescServicio = Item2.Servicio & " - " & strDescCapacidadHabitacion(1, False)
                                            'Item2.NroPax = 1
                                            intNroPax = 1
                                        End If
                                    ElseIf vbytCapacMax = 2 Then
                                        If 1 = bytSaldoPaxHotel Then
                                            strDescServicio = Item2.Servicio & " - " & strDescCapacidadHabitacion(1, False)

                                            intNroPax = 1
                                        ElseIf 2 = bytSaldoPaxHotel Then
                                            If intCantDoblesCont = 2 Then
                                                blnMatrim = True
                                            Else
                                                blnMatrim = False
                                            End If
                                            strDescServicio = Item2.Servicio & " - " & strDescCapacidadHabitacion(2, blnMatrim)

                                            intNroPax = 2
                                        End If
                                    End If

                                    If bytSaldoPaxHotel >= vbytCapacMax Then
                                        bytSaldoPaxHotel = bytSaldoPaxHotel - vbytCapacMax
                                    Else
                                        bytSaldoPaxHotel = 0
                                    End If

                                End If
                            Else
                                strDescServicio = Item2.Servicio & " - " & strDescCapacidadHabitacion(4, False)
                                intNroPax = 1
                                bytSaldoPaxHotel = 0
                            End If
                        ElseIf (blnUnTipoHabitacion And Item2.ConAlojamiento And Item2.IDTipoProv = gstrTipoProvOper) Then
                            blnHotel = True
                            If intCantDoblesCont = 2 Then
                                blnMatrim = True
                            Else
                                blnMatrim = False
                            End If
                            strDescServicio = Item2.Servicio & " - " & strDescCapacidadHabitacion(vbytCapacMax, blnMatrim)


                        End If

                        If Not blnHotel And Item.ConAlojamiento = True Then

                            GoTo SgteItem2
                        End If
                        bytCont += 1
                        Item2.Item = bytCont

                        blnExisteServ = False
                        If Not ListaDetResTotal Is Nothing Then
                            For Each ItemExist As stDetReservas In ListaDetResTotal
                                If ItemExist.IDProveedor = Item2.IDProveedor And ItemExist.Servicio = strDescServicio Then

                                    blnExisteServ = True
                                    Exit For
                                End If
                            Next
                        End If
                        If Not blnExisteServ Then

                            bytContVeces += 1


                            PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, strDescServicio, BECot.ListaDetalleCotiz, BECot.ListaCotizacionVuelo, 0, "N")
                            objBEDetRes.CapacidadHab = vbytCapacMax
                            objBEDetRes.EsMatrimonial = blnMatrim

                            ListaDetRes.Add(objBEDetRes)

                            Dim RegValor As New stDetReservas
                            RegValor.IDCab = Item2.IDCab
                            RegValor.IDDet = Item2.IDDet
                            RegValor.IDProveedor = Item2.IDProveedor
                            RegValor.Servicio = strDescServicio
                            'RegValor.Cantidad = 1
                            'RegValor.NroPax = Item2.NroPax
                            RegValor.NroPax = intNroPax

                            RegValor.UserMod = BECot.UserMod
                            ListaDetResTotal.Add(RegValor)
                        Else
                            'If DateDiff(DateInterval.Day, datFechaDiaAnt, Item2.Dia) > 1 Then
                            'BorrarProveedorLista:
                            'For Each ItemExist As stDetReservas In ListaDetResTotal
                            '    If ItemExist.IDProveedor = Item2.IDProveedor And ItemExist.Servicio = strDescServicio Then
                            '        ListaDetResTotal.Remove(ItemExist)

                            '        GoTo BorrarProveedorLista
                            '    End If
                            'Next
                            'bytContVeces += 1
                            'End If


                            For Each ItemExist As stDetReservas In ListaDetResTotal

                                If ItemExist.IDProveedor = Item2.IDProveedor And ItemExist.Servicio = strDescServicio Then

                                    ListaDetResTotal.Remove(ItemExist)

                                    Dim RegValor As New stDetReservas
                                    RegValor.IDCab = Item2.IDCab
                                    RegValor.IDDet = Item2.IDDet
                                    RegValor.IDProveedor = Item2.IDProveedor
                                    RegValor.Servicio = strDescServicio
                                    'RegValor.Cantidad = bytCantidad
                                    RegValor.NroPax = intNroPax
                                    RegValor.UserMod = BECot.UserMod
                                    ListaDetResTotal.Add(RegValor)

                                    Exit For
                                End If
                            Next
                        End If

                        datFechaDiaAnt = Item2.Dia
                        objBERes.ListaDetReservas = ListaDetRes

                        'Reservas_DetServicios
                        If Not BECot.ListaDetalleCotizacionDetServicio Is Nothing Then
                            For Each ItemDetS As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE In BECot.ListaDetalleCotizacionDetServicio
                                If ItemDetS.IDDet = Item2.IDDet Then
                                    objBEDetServRes = New clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE
                                    PasarDatosPropiedadesaDetServiciosReservasBE(objBEDetServRes, ItemDetS, "N")
                                    ListaDetServRes.Add(objBEDetServRes)
                                    objBERes.ListaDetServiciosDetReservas = ListaDetServRes
                                End If
                            Next
                        End If


                        Dim blnExisteProvee2 As Boolean = False
                        Dim intIDReserva2 As Integer = 0
                        For Each ST As stProveedoresReservas In ListaProveed
                            If ST.IDProveedor = Item.IDProveedor Then
                                intIDReserva2 = ST.IDReserva
                                ListaProveed.Remove(ST)
                                blnExisteProvee2 = True

                                Exit For
                            End If
                        Next

                        If vintIDReservaCambiosLog = 0 Then
                            objBERes.IDReserva = intIDReserva
                            objBERes.IDFile = BECot.IDFile

                            intIDReserva = InsertarxCotizacion(objBERes, BECot, intIDReserva2)
                        Else
                            objBERes.IDReserva = -1
                            objBERes.IDFile = ""
                            objBERes.UserMod = BECot.UserMod

                            intIDReserva = InsertarxCotizacion(objBERes, BECot, vintIDReservaCambiosLog)
                        End If


                        Dim RegProvee As New stProveedoresReservas
                        RegProvee.IDProveedor = Item.IDProveedor
                        If intIDReserva2 <> 0 Then
                            RegProvee.IDReserva = intIDReserva2
                        Else
                            RegProvee.IDReserva = intIDReserva
                        End If

                        ListaProveed.Add(RegProvee)
                        'End If


BorrarListaTareasRes:
                        For intInd = 0 To ListaTareasRes.Count - 1
                            ListaTareasRes.RemoveAt(intInd)
                            GoTo BorrarListaTareasRes
                        Next

BorrarDet:
                        For intInd = 0 To ListaDetRes.Count - 1
                            ListaDetRes.RemoveAt(intInd)
                            GoTo BorrarDet
                        Next
BorrarPax:
                        For intInd = 0 To ListaDetPaxRes.Count - 1
                            ListaDetPaxRes.RemoveAt(intInd)
                            GoTo BorrarPax
                        Next
BorrarDetServRes:
                        For intInd = 0 To ListaDetServRes.Count - 1
                            ListaDetServRes.RemoveAt(intInd)
                            GoTo BorrarDetServRes
                        Next

                        If Item2.IDTipoProv = gstrTipoProvOper Then
                            intCantDoblesCont -= 1
                            If intCantDoblesCont = 0 Then
                                intCantDoblesCont = intCantDobles
                            End If
                        End If


                        If bytSaldoPaxHotel > 0 Then
                            blnMismoHotel = True

                            'If blnTwinMatrim Then blnTwinMatrim = False
                            'intCantDoblesCont -= 1
                            'If intCantDoblesCont = 0 Then
                            '    intCantDoblesCont = intCantDobles

                            'End If

                            GoTo Reservas_Det
                        Else
                            blnMismoHotel = False
                        End If

                    End If
SgteItem2:
                Next

BorrarProveedorLista:
                For Each ItemExist As stDetReservas In ListaDetResTotal
                    If ItemExist.IDProveedor = Item.IDProveedor Then


                        Dim RegValor As New stDetReservas
                        RegValor.IDCab = ItemExist.IDCab
                        RegValor.IDDet = ItemExist.IDDet
                        RegValor.IDProveedor = ItemExist.IDProveedor
                        RegValor.Servicio = ItemExist.Servicio
                        'RegValor.Cantidad = ItemExist.Cantidad
                        RegValor.NroPax = ItemExist.NroPax
                        RegValor.UserMod = BECot.UserMod
                        ListaDetResTotal2.Add(RegValor)


                        ListaDetResTotal.Remove(ItemExist)

                        GoTo BorrarProveedorLista
                    End If
                Next

Sgte:
            Next


            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    '    Public Sub GrabarxCotizacion(ByVal BECot As clsCotizacionBE, Optional ByVal vintIDReservaCambiosLog As Integer = 0)

    '        Try
    '            Dim bytCapacMax As Byte

    '            Dim blnUnTipoHabitacion As Boolean = False


    '            Dim blnMatrim As Boolean = False
    '            'Dim blnTwinMatrim As Boolean = False
    '            Dim intCantDobles As Int16 = 1
    '            Dim intCantDoblesCont As Int16

    '            If BECot.Matrimonial <> 0 And BECot.Twin <> 0 Then
    '                'blnTwinMatrim = True
    '                intCantDobles = 2
    '                'Else
    '                '    If BECot.Matrimonial <> 0 And BECot.Twin = 0 Then
    '                '        blnMatrim = True
    '                '    End If

    '            End If
    '            intCantDoblesCont = intCantDobles

    '            Dim bytUnTipoHabitacionCont As Byte = 0
    '            If BECot.Simple > 0 Then bytUnTipoHabitacionCont += 1
    '            If BECot.Twin > 0 Then bytUnTipoHabitacionCont += 1
    '            If BECot.Matrimonial > 0 Then bytUnTipoHabitacionCont += 1
    '            If BECot.Triple > 0 Then bytUnTipoHabitacionCont += 1

    '            If bytUnTipoHabitacionCont = 1 Then
    '                blnUnTipoHabitacion = True
    '            End If


    '            'If BECot.Matrimonial + BECot.Twin = 0 Or _
    '            '     BECot.Matrimonial + BECot.Triple = 0 Or _
    '            '    BECot.Matrimonial + BECot.Simple = 0 Or _
    '            '     BECot.Twin + BECot.Simple = 0 Or _
    '            '     BECot.Twin + BECot.Matrimonial = 0 Or _
    '            '     BECot.Twin + BECot.Triple = 0 Or _
    '            '     BECot.Simple + BECot.Twin = 0 Or _
    '            '     BECot.Simple + BECot.Matrimonial = 0 Or _
    '            '     BECot.Simple + BECot.Triple = 0 Then

    '            If blnUnTipoHabitacion Then
    '                'blnUnTipoHabitacion = True
    '                If BECot.Simple <> 0 Then
    '                    bytCapacMax = 1

    '                ElseIf BECot.Twin <> 0 Or BECot.Matrimonial <> 0 Then
    '                    bytCapacMax = 2

    '                ElseIf BECot.Triple <> 0 Then
    '                    bytCapacMax = 3

    '                End If

    '            Else

    '                If BECot.Triple <> 0 Then
    '                    bytCapacMax = 3

    '                ElseIf (BECot.Twin <> 0 Or BECot.Matrimonial <> 0) Then
    '                    bytCapacMax = 2

    '                End If
    '            End If



    '            Dim objBERes As clsReservaBE
    '            'Dim objBEResTotal As New clsReservaBE

    '            Dim objBEDetRes As clsReservaBE.clsDetalleReservaBE

    '            Dim objBEDetServRes As clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE
    '            'Dim objBEDetPaxRes As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE
    '            'Dim objCotiPaxBT As New clsCotizacionBT.clsCotizacionPaxBT
    '            Dim objTareasBE As clsReservaBE.clsTareasReservaBE

    '            Dim ListaDetRes As New List(Of clsReservaBE.clsDetalleReservaBE)
    '            Dim ListaDetResTotal As New List(Of stDetReservas)
    '            Dim ListaDetResTotal2 As New List(Of stDetReservas)
    '            Dim ListaDetServRes As New List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE)
    '            Dim ListaDetPaxRes As New List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
    '            Dim ListaTareasRes As New List(Of clsReservaBE.clsTareasReservaBE)

    '            Dim ListaProveed As New List(Of stProveedoresReservas)
    '            Dim ListaProveedTareas As New List(Of String)
    '            Dim ListaPax As New List(Of String)
    '            Dim strDescServicio As String = ""
    '            Dim bytSaldoPaxHotel As Byte = 0
    '            Dim blnMismoHotel As Boolean = False
    '            Dim intIDReserva As Int32 = 0
    '            Dim bln1ero As Boolean
    '            Dim bytCont As Byte
    '            Dim blnExisteServ As Boolean

    '            'Dim dat1eraFecha As Date '= "01/01/1900"
    '            Dim datFechaDiaAnt As Date '= "01/01/1900"
    '            Dim bytContVeces As Byte

    '            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
    '                'If Item.Reservado Then GoTo Sgte
    '                ''If Item.IDTipoProv<>gstrTipoProvHotel and Item.ConAlojamiento

    '                Dim blnExisteProvee As Boolean = False
    '                For Each ST As stProveedoresReservas In ListaProveed
    '                    If ST.IDProveedor = Item.IDProveedor Then
    '                        blnExisteProvee = True
    '                        Exit For
    '                    End If
    '                Next
    '                If Not blnExisteProvee Then
    '                    intIDReserva = 0
    '                Else
    '                    For Each ST As stProveedoresReservas In ListaProveed
    '                        If ST.IDProveedor = Item.IDProveedor Then

    '                            Exit For
    '                        End If
    '                    Next
    '                End If

    '                bln1ero = True
    'BorrarPaxGrabados:
    '                For intInd = 0 To ListaPax.Count - 1
    '                    ListaPax.RemoveAt(intInd)
    '                    GoTo BorrarPaxGrabados
    '                Next

    '                bytCont = 0
    '                Dim intNroPax As Int16
    '                Dim intNroLiberados As Int16
    '                For Each Item2 As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
    '                    intNroPax = Item2.NroPax + Item2.NroLiberados
    '                    intNroLiberados = Item2.NroLiberados
    '                    bytContVeces = 0
    '                    'Dim bytSaldoPaxHotel2 As Byte = Item2.NroPax

    '                    If Item2.IDProveedor = Item.IDProveedor Then
    '                        objBERes = New clsReservaBE
    '                        With objBERes
    '                            .IDProveedor = Item2.IDProveedor
    '                            .IDCab = BECot.IdCab
    '                            .Estado = "NV"
    '                            .EstadoSolicitud = "RECR"
    '                            .IDUsuarioVen = BECot.IDUsuario
    '                            .IDUsuarioRes = "0000"
    '                            .Observaciones = ""
    '                            .CodReservaProv = ""

    '                            If Not ListaProveedTareas.Contains(Item.IDProveedor) Then 'And vintIDReservaCambiosLog = 0 Then
    '                                Dim datMenor As Date = datFechaMenorProveedorDetCotizacion(BECot, Item.IDProveedor)
    '                                Dim ListaUltimoMinuto As New List(Of Char)
    '                                Dim dc As Dictionary(Of String, Date) = dicDevuelveFechasDeadLine(Item.IDProveedor, _
    '                                                                                                  datMenor, BECot.NroPax + BECot.NroLiberados, ListaUltimoMinuto)
    '                                For Each dic As KeyValuePair(Of String, Date) In dc
    '                                    objTareasBE = New clsReservaBE.clsTareasReservaBE
    '                                    Select Case dic.Key
    '                                        Case "FecDeadLRoom1"
    '                                            objTareasBE.IDTarea = 1
    '                                        Case "FecDeadLRoom2"
    '                                            objTareasBE.IDTarea = 2
    '                                        Case "FecDeadLRoom3"
    '                                            objTareasBE.IDTarea = 3
    '                                        Case "FecDeadLPrepag1"
    '                                            objTareasBE.IDTarea = 4
    '                                        Case "FecDeadLPrepag2"
    '                                            objTareasBE.IDTarea = 5
    '                                        Case "FecDeadLSaldo"
    '                                            objTareasBE.IDTarea = 6
    '                                    End Select
    '                                    objTareasBE.FecDeadLine = dic.Value
    '                                    objTareasBE.IDEstado = "PD"
    '                                    objTareasBE.IDCab = BECot.IdCab
    '                                    objTareasBE.Accion = "N"
    '                                    objTareasBE.UltimoMinuto = False
    '                                    If objTareasBE.FecDeadLine <> "01/01/1900" Then
    '                                        ListaTareasRes.Add(objTareasBE)
    '                                    End If
    '                                    'objTareasBE.Descripcion = dic.Key
    '                                    objTareasBE.Descripcion = strDevuelveDescripcionTareaPolitica(objTareasBE.IDTarea)
    '                                Next

    '                                ListaProveedTareas.Add(Item.IDProveedor)
    '                                objBERes.ListaUltimoMinuto = ListaUltimoMinuto

    '                            End If
    '                            .UserMod = BECot.UserMod
    '                        End With
    '                        objBERes.ListaTareasReservas = ListaTareasRes

    '                        'Reservas_Det
    'Reservas_Det:

    '                        objBEDetRes = New clsReservaBE.clsDetalleReservaBE

    '                        strDescServicio = ""

    '                        Dim blnHotel As Boolean = False

    '                        If (Not blnUnTipoHabitacion And Item2.IDTipoProv = gstrTipoProvHotel) Then 'Or (Item2.ConAlojamiento And Item2.IDTipoProv = gstrTipoProvOper) Then
    '                            blnHotel = True
    '                            If Item2.IncGuia = 0 Then
    '                                If Not blnMismoHotel Then
    '                                    bytSaldoPaxHotel = intNroPax

    '                                    'If bytCapacMax = 1 Then
    '                                    '    bytSaldoPaxHotel = intNroPax
    '                                    'ElseIf bytCapacMax = 2 Then
    '                                    '    bytSaldoPaxHotel = BECot.Twin * 2
    '                                    '    bytSaldoPaxHotel += BECot.Matrimonial * 2
    '                                    'ElseIf bytCapacMax = 3 Then
    '                                    '    bytSaldoPaxHotel = BECot.Triple * 3

    '                                    'End If

    '                                End If

    '                                If bytSaldoPaxHotel > bytCapacMax Then
    '                                    If intCantDoblesCont = 2 Then
    '                                        blnMatrim = True
    '                                    Else
    '                                        blnMatrim = False
    '                                    End If
    '                                    strDescServicio = Item2.Servicio & " " & strDescCapacidadHabitacion(bytCapacMax, blnMatrim)
    '                                    bytSaldoPaxHotel = bytSaldoPaxHotel - bytCapacMax

    '                                    intNroPax = bytCapacMax
    '                                Else
    '                                    If bytCapacMax = 3 Then
    '                                        If 2 = bytSaldoPaxHotel Then
    '                                            If intCantDoblesCont = 2 Then
    '                                                blnMatrim = True
    '                                            Else
    '                                                blnMatrim = False
    '                                            End If
    '                                            strDescServicio = Item2.Servicio & " " & strDescCapacidadHabitacion(2, blnMatrim)

    '                                            intNroPax = 2
    '                                        ElseIf 1 = bytSaldoPaxHotel Then
    '                                            strDescServicio = Item2.Servicio & " " & strDescCapacidadHabitacion(1, False)
    '                                            'Item2.NroPax = 1
    '                                            intNroPax = 1
    '                                        End If
    '                                    ElseIf bytCapacMax = 2 Then
    '                                        If 1 = bytSaldoPaxHotel Then
    '                                            strDescServicio = Item2.Servicio & " " & strDescCapacidadHabitacion(1, False)

    '                                            intNroPax = 1
    '                                        ElseIf 2 = bytSaldoPaxHotel Then
    '                                            If intCantDoblesCont = 2 Then
    '                                                blnMatrim = True
    '                                            Else
    '                                                blnMatrim = False
    '                                            End If
    '                                            strDescServicio = Item2.Servicio & " " & strDescCapacidadHabitacion(2, blnMatrim)

    '                                            intNroPax = 2
    '                                        End If
    '                                    End If

    '                                    If bytSaldoPaxHotel >= bytCapacMax Then
    '                                        bytSaldoPaxHotel = bytSaldoPaxHotel - bytCapacMax
    '                                    Else
    '                                        bytSaldoPaxHotel = 0
    '                                    End If

    '                                End If
    '                            Else
    '                                strDescServicio = Item2.Servicio & " " & strDescCapacidadHabitacion(4, False)
    '                                intNroPax = 1
    '                                bytSaldoPaxHotel = 0
    '                            End If
    '                        ElseIf (blnUnTipoHabitacion And Item2.IDTipoProv = gstrTipoProvHotel) Then 'Or (Item2.ConAlojamiento And Item2.IDTipoProv = gstrTipoProvOper) Then
    '                            blnHotel = True
    '                            If intCantDoblesCont = 2 Then
    '                                blnMatrim = True
    '                            Else
    '                                blnMatrim = False
    '                            End If
    '                            strDescServicio = Item2.Servicio & " " & strDescCapacidadHabitacion(bytCapacMax, blnMatrim)


    '                            '''''''

    '                            '''''''

    '                        End If

    '                        If Not blnHotel And Item.ConAlojamiento = True Then

    '                            GoTo SgteItem2
    '                        End If

    '                        bytCont += 1
    '                        Item2.Item = bytCont

    '                        blnExisteServ = False
    '                        If Not ListaDetResTotal Is Nothing Then
    '                            For Each ItemExist As stDetReservas In ListaDetResTotal
    '                                If ItemExist.IDProveedor = Item2.IDProveedor And ItemExist.Servicio = strDescServicio Then

    '                                    blnExisteServ = True
    '                                    Exit For
    '                                End If
    '                            Next
    '                        End If
    '                        If Not blnExisteServ Then

    '                            bytContVeces += 1


    '                            PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, strDescServicio, BECot.ListaDetalleCotiz, BECot.ListaCotizacionVuelo, 0, "N")
    '                            objBEDetRes.CapacidadHab = 0
    '                            objBEDetRes.EsMatrimonial = False
    '                            If blnHotel Then
    '                                objBEDetRes.CapacidadHab = intNroPax
    '                                objBEDetRes.EsMatrimonial = blnMatrim
    '                            End If

    '                            ListaDetRes.Add(objBEDetRes)

    '                            If blnHotel Then
    '                                Dim RegValor As New stDetReservas
    '                                RegValor.IDCab = Item2.IDCab
    '                                RegValor.IDDet = Item2.IDDet
    '                                RegValor.IDProveedor = Item2.IDProveedor
    '                                RegValor.Servicio = strDescServicio
    '                                'RegValor.Cantidad = 1
    '                                'RegValor.NroPax = Item2.NroPax
    '                                RegValor.NroPax = intNroPax

    '                                RegValor.UserMod = BECot.UserMod
    '                                ListaDetResTotal.Add(RegValor)
    '                            End If
    '                        Else
    '                            'If DateDiff(DateInterval.Day, datFechaDiaAnt, Item2.Dia) > 1 Then
    '                            'BorrarProveedorLista:
    '                            'For Each ItemExist As stDetReservas In ListaDetResTotal
    '                            '    If ItemExist.IDProveedor = Item2.IDProveedor And ItemExist.Servicio = strDescServicio Then
    '                            '        ListaDetResTotal.Remove(ItemExist)

    '                            '        GoTo BorrarProveedorLista
    '                            '    End If
    '                            'Next
    '                            'bytContVeces += 1
    '                            'End If

    '                            If blnHotel Then
    '                                For Each ItemExist As stDetReservas In ListaDetResTotal

    '                                    If ItemExist.IDProveedor = Item2.IDProveedor And ItemExist.Servicio = strDescServicio Then

    '                                        ListaDetResTotal.Remove(ItemExist)

    '                                        Dim RegValor As New stDetReservas
    '                                        RegValor.IDCab = Item2.IDCab
    '                                        RegValor.IDDet = Item2.IDDet
    '                                        RegValor.IDProveedor = Item2.IDProveedor
    '                                        RegValor.Servicio = strDescServicio
    '                                        'RegValor.Cantidad = bytCantidad
    '                                        RegValor.NroPax = intNroPax
    '                                        RegValor.UserMod = BECot.UserMod
    '                                        ListaDetResTotal.Add(RegValor)

    '                                        Exit For
    '                                    End If
    '                                Next
    '                            End If
    '                        End If

    '                        datFechaDiaAnt = Item2.Dia
    '                        objBERes.ListaDetReservas = ListaDetRes

    '                        'Reservas_DetServicios
    '                        If Not BECot.ListaDetalleCotizacionDetServicio Is Nothing Then
    '                            For Each ItemDetS As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE In BECot.ListaDetalleCotizacionDetServicio
    '                                If ItemDetS.IDDet = Item2.IDDet Then
    '                                    objBEDetServRes = New clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE
    '                                    PasarDatosPropiedadesaDetServiciosReservasBE(objBEDetServRes, ItemDetS, "N")
    '                                    ListaDetServRes.Add(objBEDetServRes)
    '                                    objBERes.ListaDetServiciosDetReservas = ListaDetServRes
    '                                End If
    '                            Next
    '                        End If


    '                        Dim blnExisteProvee2 As Boolean = False
    '                        Dim intIDReserva2 As Integer = 0
    '                        For Each ST As stProveedoresReservas In ListaProveed
    '                            If ST.IDProveedor = Item.IDProveedor Then
    '                                intIDReserva2 = ST.IDReserva
    '                                ListaProveed.Remove(ST)
    '                                blnExisteProvee2 = True

    '                                Exit For
    '                            End If
    '                        Next

    '                        If vintIDReservaCambiosLog = 0 Then
    '                            objBERes.IDReserva = intIDReserva
    '                            objBERes.IDFile = BECot.IDFile

    '                            intIDReserva = InsertarxCotizacion(objBERes, BECot, intIDReserva2)
    '                        Else
    '                            objBERes.IDReserva = -1
    '                            objBERes.IDFile = ""
    '                            objBERes.UserMod = BECot.UserMod

    '                            intIDReserva = InsertarxCotizacion(objBERes, BECot, vintIDReservaCambiosLog)
    '                        End If


    '                        Dim RegProvee As New stProveedoresReservas
    '                        RegProvee.IDProveedor = Item.IDProveedor
    '                        If intIDReserva2 <> 0 Then
    '                            RegProvee.IDReserva = intIDReserva2
    '                        Else
    '                            RegProvee.IDReserva = intIDReserva
    '                        End If

    '                        ListaProveed.Add(RegProvee)
    '                        'End If


    'BorrarListaTareasRes:
    '                        For intInd = 0 To ListaTareasRes.Count - 1
    '                            ListaTareasRes.RemoveAt(intInd)
    '                            GoTo BorrarListaTareasRes
    '                        Next

    'BorrarDet:
    '                        For intInd = 0 To ListaDetRes.Count - 1
    '                            ListaDetRes.RemoveAt(intInd)
    '                            GoTo BorrarDet
    '                        Next
    'BorrarPax:
    '                        For intInd = 0 To ListaDetPaxRes.Count - 1
    '                            ListaDetPaxRes.RemoveAt(intInd)
    '                            GoTo BorrarPax
    '                        Next
    'BorrarDetServRes:
    '                        For intInd = 0 To ListaDetServRes.Count - 1
    '                            ListaDetServRes.RemoveAt(intInd)
    '                            GoTo BorrarDetServRes
    '                        Next

    '                        If Item2.IDTipoProv = gstrTipoProvHotel Then
    '                            intCantDoblesCont -= 1
    '                            If intCantDoblesCont = 0 Then
    '                                intCantDoblesCont = intCantDobles
    '                            End If
    '                        End If


    '                        If bytSaldoPaxHotel > 0 Then
    '                            blnMismoHotel = True

    '                            'If blnTwinMatrim Then blnTwinMatrim = False
    '                            'intCantDoblesCont -= 1
    '                            'If intCantDoblesCont = 0 Then
    '                            '    intCantDoblesCont = intCantDobles

    '                            'End If

    '                            GoTo Reservas_Det
    '                        Else
    '                            blnMismoHotel = False
    '                        End If

    '                    End If
    'SgteItem2:
    '                Next

    'BorrarProveedorLista:
    '                For Each ItemExist As stDetReservas In ListaDetResTotal
    '                    If ItemExist.IDProveedor = Item.IDProveedor Then


    '                        Dim RegValor As New stDetReservas
    '                        RegValor.IDCab = ItemExist.IDCab
    '                        RegValor.IDDet = ItemExist.IDDet
    '                        RegValor.IDProveedor = ItemExist.IDProveedor
    '                        RegValor.Servicio = ItemExist.Servicio
    '                        'RegValor.Cantidad = ItemExist.Cantidad
    '                        RegValor.NroPax = ItemExist.NroPax

    '                        RegValor.UserMod = BECot.UserMod
    '                        ListaDetResTotal2.Add(RegValor)


    '                        ListaDetResTotal.Remove(ItemExist)

    '                        GoTo BorrarProveedorLista
    '                    End If
    '                Next

    '                'Dim objBTDetPaxRes As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT
    '                'objBTDetPaxRes.GrabarxCotizacion(BECot.IdCab, Item.IDDet)

    '                'MultiplicarxCantidadMontosDetallexIDCab(BECot.IdCab, Item.IDDet, BECot.UserMod)
    'Sgte:
    '            Next


    '            GrabarxCotizacionOperadoresConAlojamiento(BECot, bytCapacMax, vintIDReservaCambiosLog)

    '            'ActualizarNroPaxDetalle(ListaDetResTotal2)

    '            'ActualizarCantidadesDetalle(BECot.IdCab, BECot.Simple, (BECot.Twin + BECot.Matrimonial), BECot.Triple, BECot.UserMod)
    '            ActualizarCantidadesDetalle(BECot.IdCab, BECot.Simple, BECot.Twin, BECot.Matrimonial, BECot.Triple, BECot.UserMod)
    '            ActualizarCantidadesDetalleHabitGuia(BECot.IdCab, BECot.UserMod)

    '            ActualizarNroPaxDetalle(ListaDetResTotal2)

    '            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
    '                'If Item.Reservado Then GoTo Sgte2

    '                Dim objBTDetPaxRes As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT

    '                objBTDetPaxRes.GrabarxCotizacion(BECot.IdCab, Item.IDDet)

    '                MultiplicarxCantidadMontosDetallexIDCab(BECot.IdCab, Item.IDDet, BECot.UserMod)

    'Sgte2:
    '            Next







    '            'Dim objBTDetPaxRes As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT
    '            'objBTDetPaxRes.GrabarxCotizacion(BECot.IdCab, vstrIDProveedor)


    '            'MultiplicarxCantidadMontosDetallexIDCab(BECot.IdCab, BECot.UserMod, vstrIDProveedor)


    '            ActualizarEliminarDetReservasHotelesDiasSeguidos(BECot.IdCab, BECot.UserMod)

    '            'Grabar tareas por file
    '            If vintIDReservaCambiosLog = 0 Then
    '                Dim dcFile As Dictionary(Of String, Date) = dicDevuelveFechasDeadLinexFile(BECot.IdCab)
    '                For Each dic As KeyValuePair(Of String, Date) In dcFile
    '                    objTareasBE = New clsReservaBE.clsTareasReservaBE
    '                    objTareasBE.IDReserva = 0
    '                    objTareasBE.IDTarea = dic.Key.Substring(0, InStr(dic.Key, "|") - 1)
    '                    objTareasBE.FecDeadLine = dic.Value
    '                    objTareasBE.IDEstado = "PD"
    '                    objTareasBE.IDCab = BECot.IdCab
    '                    objTareasBE.Descripcion = Mid(dic.Key, InStr(dic.Key, "|") + 1)
    '                    objTareasBE.UserMod = BECot.UserMod
    '                    objTareasBE.UltimoMinuto = False
    '                    objTareasBE.Accion = "N"
    '                    If objTareasBE.FecDeadLine <> "01/01/1900" Then
    '                        ListaTareasRes.Add(objTareasBE)
    '                    End If
    '                Next
    '                objBERes = New clsReservaBE
    '                objBERes.ListaTareasReservas = ListaTareasRes

    '                Dim objTareasBT As New clsReservaBT.clsTareasReservaBT
    '                objTareasBT.InsertarEliminar(objBERes)
    '                ''''''''''''''
    '            End If

    '            Dim objResDetServ As New clsReservaBT.clsDetalleReservaBT.clsDetalleReservaDetServiciosBT
    '            objResDetServ.GrabarxCotizacion(BECot.IdCab, BECot.UserMod)

    '            ContextUtil.SetComplete()

    '        Catch ex As Exception

    '            ContextUtil.SetAbort()
    '            Throw
    '        End Try

    '    End Sub

    Public Function ActualizarxIDDetLogVentas(ByVal vintIDCab As Integer, _
                                         ByVal vstrIDProveedor As String, _
                                         ByVal vstrIDReserva As Integer, _
                                         ByVal vstrUser As String) As Boolean
        Try

            'Dim objBN As New clsReservaBN
            Dim objCotiDetBN As New clsCotizacionBN.clsDetalleCotizacionBN
            Dim dtt As DataTable = objCotiDetBN.dttConsultarTodosxIDCabProveedorLog(vintIDCab, vstrIDProveedor)
            'Dim dtt As DataTable = objBN.ConsultarLogVentas(vintIDCab, vstrIDProveedor)
            Dim blnOk As Boolean = False
            If dtt.Rows.Count > 0 Then
                Dim objBN As New clsReservaBN
                Dim objBECot As clsCotizacionBE = objBN.objBECotizacion(vintIDCab, vstrIDProveedor, vstrUser)

                'Dim ListaDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE) 
                'objBECot.ListaDetalleCotiz = objBN.objBECotizacion(vintIDCab, vstrIDProveedor)

                Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT

                For Each dr As DataRow In dtt.Rows
                    objDetResBT.EliminarxIDDet(dr("IDDet"))
                Next

                Dim ListaProveed As New List(Of stProveedoresReservas)
                Dim ItemProveedor As New stProveedoresReservas _
                    With {.IDProveedor = vstrIDProveedor, .IDReserva = vstrIDReserva}
                ListaProveed.Add(ItemProveedor)

                GrabarxCotizacion2(objBECot, False, ListaProveed, True)
                GrabarxCotizacion2(objBECot, True, ListaProveed, True)
                CambiosPostGeneracionxFile(objBECot, ListaProveed, True)

                blnOk = True
            Else

            End If

            If blnOk Then
                ActualizarFlagCambiosVtasAceptadas(vstrIDReserva, True, vstrUser)
                ActualizarEstado(vstrIDReserva, "NV", vstrUser, "", "")
            End If

            ContextUtil.SetComplete()

            Return blnOk
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Function bytDevuelveCantidadxCoCapacidad(ByVal vListAcomodo As List(Of stAcomodoEspecial), ByVal vstrCoCapacidad As String) As Int16
        Try
            Dim oST As stAcomodoEspecial = vListAcomodo.Find(Function(x) (x.CoCapacidad = vstrCoCapacidad))
            If Not IsNothing(oST) Then
                Return oST.QtHabitaciones * oST.QtCapacidad
            End If
            Return 0
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub GrabarxCotizacion2(ByVal BECot As clsCotizacionBE, ByVal vblnResidente As Boolean, _
                                  ByVal vListaProveed As List(Of stProveedoresReservas), _
                                  Optional ByVal vblnIDReservaCambiosLog As Boolean = False, _
                                  Optional ByVal vdatFechaOut As Date = #1/1/1900#, _
                                  Optional ByVal vbytNoches As Byte = 0, _
                                  Optional ByVal vchrAccionLog As Char = "", _
                                  Optional ByVal vblnTemp As Boolean = False, _
                                  Optional ByVal vListDetalleCotiz As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = Nothing)
        'Dim ListaProveed As New List(Of stProveedoresReservas)
        Dim ListaDetRes As New List(Of clsReservaBE.clsDetalleReservaBE)
        'Dim ListaDetServRes As New List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE)

        Dim objBERes As clsReservaBE
        Try
            Dim objTabBN As New clsTablasApoyoBN.clsCapacidadHabitacionBN
            Dim dttCapacEspecial As DataTable = objTabBN.ConsultarCbo
            Dim blnFinGenerarAcomodo As Boolean = False ''>>Nuevo
            Dim bytIndexItemGen As Byte = 1 ', bytContAcomodosEsp As Byte = 0
            If vListDetalleCotiz Is Nothing Then vListDetalleCotiz = BECot.ListaDetalleCotiz
            Dim ListaIDReservaDetBD As New List(Of Integer)
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In vListDetalleCotiz 'BECot.ListaDetalleCotiz
                If Item.IDReserva_Det > 0 And Not ListaIDReservaDetBD.Contains(Item.IDReserva_Det) Then
                    ListaIDReservaDetBD.Add(Item.IDReserva_Det)
                End If
            Next

            For Each ItemReserva As stProveedoresReservas In vListaProveed
                Dim ListaIDDet As New List(Of Integer)
                Dim ListaIDReservaDet As New List(Of Integer)
                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                    If Item.IDProveedor = ItemReserva.IDProveedor Then

                        Dim strDescServicio As String = ""
                        Dim bytCantidad As Int16
                        Dim bytCapacidadHab As Int16
                        If (Item.IDTipoProv = gstrTipoProvHotel And Item.PlanAlimenticio = False) Or _
                            (Item.IDTipoProv <> gstrTipoProvHotel And Item.ConAlojamiento = True) Then

                            Dim bytCapacMax As Int16
                            Dim bytSimple As Int16 = If(vblnResidente = True, BECot.SimpleResidente, BECot.Simple)
                            Dim bytTwin As Int16 = If(vblnResidente = True, BECot.TwinResidente, BECot.Twin)
                            Dim bytMatrimonial As Int16 = If(vblnResidente = True, BECot.MatrimonialResidente, BECot.Matrimonial)
                            Dim bytTriple As Int16 = If(vblnResidente = True, BECot.TripleResidente, BECot.Triple)
                            Dim intNroPax As Int16 = Item.NroPax + Item.NroLiberados

                            Dim intCuadruple As Int16 = 0
                            Dim intQuintuple As Int16 = 0
                            Dim intSextuple As Int16 = 0
                            Dim intSeptuple As Int16 = 0
                            Dim intOctuple As Int16 = 0
                            Dim intDoble As Int16 = 0
                            Dim intDobleMat As Int16 = 0
                            Dim intDobleTwin As Int16 = 0
                            Dim intSimple As Int16 = 0
                            Dim intSimpleGuide As Int16 = 0
                            'If intNroPax > bytSimple + (bytTwin * 2) + (bytMatrimonial * 2) + (bytTriple * 3) Then
                            'intNroPax = bytSimple + (bytTwin * 2) + (bytMatrimonial * 2) + (bytTriple * 3)
                            'End If
                            'Dim intNroPax As Int16 = bytSimple + (bytTwin * 2) + (bytMatrimonial * 2) + (bytTriple * 3)
                            If vblnResidente Then
                                intNroPax -= (BECot.Simple + (BECot.Twin * 2) + (BECot.Matrimonial * 2) + (BECot.Triple * 3))
                            End If
                            If bytTriple <> 0 Then
                                bytCapacMax = 3
                            ElseIf bytTwin <> 0 Or bytMatrimonial <> 0 Then
                                bytCapacMax = 2
                            ElseIf bytSimple <> 0 Then
                                bytCapacMax = 1
                            End If

                            Dim blnCapacMaxMayorNroPax As Boolean = False
                            If bytCapacMax > intNroPax Then
                                bytCapacMax = intNroPax
                                blnCapacMaxMayorNroPax = True
                            End If

                            If bytSimple + bytTwin + bytMatrimonial + bytTriple = 0 Then
                                Exit Sub
                            End If

                            Dim strCoCapacidadEspecial As String = ""
                            Dim ListaCoCapacidad As New List(Of String)
                            Dim intSumaHabit As Int16 = 0
                            Dim ListaAcomodosEspec As New List(Of stAcomodoEspecial)

                            If Item.AcomodoEspecial = True Then
                                'bytCapacMax = intCapacMaxAcomodoEspecial(Item.IDDet, strCoCapacidadEspecial, _
                                '                                         bytSimple, bytTwin, _
                                '                                         bytMatrimonial, bytTriple, _
                                '                                         intCuadruple, intQuintuple, _
                                '                                         intSextuple, intSeptuple, _
                                '                                         intOctuple, intSumaHabit)

                                bytCapacMax = intCapacMaxAcomodoEspecial(Item.IDDet, ListaAcomodosEspec, intSumaHabit)
                            End If

                            Dim intContHab As Int16 = intNroPax
                            Dim intContHabMatrimonial As Byte = 0
InicioHabitac:
                            'If bytCapacMax <= 0 Then bytCapacMax = 1
                            Dim blnActualizarPorAtenderListaAcomodosEspec As Boolean = False
                            Dim intCantidadBD As Int16

                            Dim blnMatrim As Boolean = False
                            Dim blnEsGuide As Boolean = False
                            If bytCapacMax = 3 And bytTriple > 0 Then
                                bytCantidad = bytTriple
                                bytCapacidadHab = 3
                            ElseIf bytCapacMax = 2 And bytTwin > 0 Then
                                bytCantidad = bytTwin
                                bytCapacidadHab = 2
                                blnMatrim = False
                            ElseIf bytCapacMax = 2 And bytMatrimonial > 0 Then
                                bytCantidad = bytMatrimonial
                                bytCapacidadHab = 2
                                blnMatrim = True
                            ElseIf bytCapacMax = 1 And bytSimple > 0 Then
                                bytCantidad = bytSimple
                                bytCapacidadHab = 1
                            ElseIf bytCapacMax = 1 Then
                                bytCapacidadHab = 1
                            End If
                            'If bytCapacMax > 3 And Item.AcomodoEspecial = True Then
                            If Item.AcomodoEspecial = True Then

                                intOctuple = intCantTipoHabitEspecialxCapac(8, False, ListaAcomodosEspec, False)
                                intSeptuple = intCantTipoHabitEspecialxCapac(7, False, ListaAcomodosEspec, False)
                                intSextuple = intCantTipoHabitEspecialxCapac(6, False, ListaAcomodosEspec, False)
                                intQuintuple = intCantTipoHabitEspecialxCapac(5, False, ListaAcomodosEspec, False)
                                intCuadruple = intCantTipoHabitEspecialxCapac(4, False, ListaAcomodosEspec, False)
                                bytTriple = intCantTipoHabitEspecialxCapac(3, False, ListaAcomodosEspec, False)
                                intDobleTwin = intCantTipoHabitEspecialxCapac(2, False, ListaAcomodosEspec, False)
                                intDobleMat = intCantTipoHabitEspecialxCapac(2, False, ListaAcomodosEspec, True)
                                intDoble = intDobleTwin + intDobleMat
                                'bytSimple = intCantTipoHabitEspecialxCapac(1, ListaAcomodosEspec, False)
                                intSimple = intCantTipoHabitEspecialxCapac(1, False, ListaAcomodosEspec, False)
                                intSimpleGuide = intCantTipoHabitEspecialxCapac(1, True, ListaAcomodosEspec, False)
                                bytSimple = intSimple + intSimpleGuide

                                bytTwin = intDobleTwin
                                bytMatrimonial = intDobleMat
                                If bytCapacMax = 8 And intOctuple > 0 Then
                                    bytCantidad = intOctuple
                                    bytCapacidadHab = 8
                                ElseIf bytCapacMax = 7 And intSeptuple > 0 Then
                                    bytCantidad = intSeptuple
                                    bytCapacidadHab = 7
                                ElseIf bytCapacMax = 6 And intSextuple > 0 Then
                                    bytCantidad = intSextuple
                                    bytCapacidadHab = 6
                                ElseIf bytCapacMax = 5 And intQuintuple > 0 Then
                                    bytCantidad = intQuintuple
                                    bytCapacidadHab = 5
                                ElseIf bytCapacMax = 4 And intCuadruple > 0 Then
                                    bytCantidad = intCuadruple
                                    bytCapacidadHab = 4
                                ElseIf bytCapacMax = 3 And bytTriple > 0 Then
                                    bytCantidad = bytTriple
                                    bytCapacidadHab = 3
                                    'ElseIf bytCapacMax = 2 And intDoble > 0 Then
                                    '    bytCantidad = intDoble
                                    '    bytCapacidadHab = 2
                                ElseIf bytCapacMax = 2 And intDobleTwin > 0 Then
                                    bytCantidad = intDobleTwin
                                    bytCapacidadHab = 2
                                    blnMatrim = False
                                ElseIf bytCapacMax = 2 And intDobleMat > 0 Then
                                    bytCantidad = intDobleMat
                                    bytCapacidadHab = 2
                                    blnMatrim = True
                                    'ElseIf bytCapacMax = 1 And bytSimple > 0 Then
                                    '    bytCantidad = bytSimple
                                    '    bytCapacidadHab = 1
                                ElseIf bytCapacMax = 1 And intSimple > 0 Then
                                    bytCantidad = intSimple
                                    'bytCantidad = bytSimple
                                    bytCapacidadHab = 1
                                    blnEsGuide = False
                                ElseIf bytCapacMax = 1 And intSimpleGuide > 0 Then
                                    bytCantidad = intSimpleGuide
                                    'bytCantidad = bytSimple
                                    bytCapacidadHab = 1
                                    blnEsGuide = True
                                End If
                            End If
                            If Item.IncGuia = True Then
                                bytCapacidadHab = 1
                                bytCantidad = 1
                            End If
                            intCantidadBD = bytCantidad
                            'If bytCantidad * bytCapacidadHab > intNroPax Then
                            If bytCantidad > 1 Then
                                If bytCapacidadHab < intNroPax Then
                                    If intNroPax Mod bytCapacidadHab = 0 Then
                                        bytCantidad = intNroPax / bytCapacidadHab
                                    Else
                                        bytCantidad = intNroPax Mod bytCapacidadHab
                                    End If
                                Else
                                    bytCantidad = intNroPax / bytCapacidadHab
                                End If
                            End If

                            If bytCapacMax = 1 And bytSimple = 0 And Item.IncGuia = False And blnCapacMaxMayorNroPax = False Then
                                GoTo SgteServ
                            End If
                            If bytCapacMax < 0 Then
                                GoTo SgteServ
                            End If

                            'If bytCapacMax <= 0 Then bytCapacMax = 1
                            'If Item.Dias > 1 Then
                            'strDescServicio = Item.Servicio.Substring(0, InStrRev(Item.Servicio, "-") - 2) & " " & strDescCapacidadHabitacion(bytCapacMax, blnMatrim)
                            'Else

                            If Item.AcomodoEspecial = True Then

                                'strCoCapacidadEspecial = strCoCapacidadporCantidad(Item.IDDet, bytCapacMax, ListaCoCapacidad)
                                If strCoCapacidadEspecial = "" Then
                                    strCoCapacidadEspecial = strCoCapacidadporCantidad(Item.IDDet, blnEsGuide, bytCapacMax, blnMatrim, ListaCoCapacidad)
                                End If
                                strDescServicio = Item.Servicio & " " & strDescCapacidadHabitacionAcomodoEspecial(dttCapacEspecial, strCoCapacidadEspecial)
                            Else
                                strDescServicio = Item.Servicio & " " & strDescCapacidadHabitacion(bytCapacMax, blnMatrim)
                            End If

                            'End If


                            If Item.IncGuia = True Then 'And bytCapacidadHab = 1 Then
                                If vblnResidente Then Exit Sub
                                bytCantidad = 1
                                bytCapacidadHab = 1
                                strDescServicio = Item.Servicio & " " & strDescCapacidadHabitacion(bytCantidad, blnMatrim)
                                intCantidadBD = bytCantidad
                            End If

                            Dim objBEDetRes As New clsReservaBE.clsDetalleReservaBE
                            objBEDetRes.FlAcomodoResidente = vblnResidente
                            If vblnIDReservaCambiosLog Then
                                Dim objDetCotBT As New clsDetalleVentasCotizacionBT
                                Dim dttCotiDet As DataTable = objDetCotBT.ConsultarListxIDCab(BECot.IdCab, True, False, False)
                                Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
                                PasarDatosListaCotiDetRegeneracion(Item.IDProveedor, BECot.IdCab, Item.IDReserva, _
                                                                   BECot.UserMod, dttCotiDet, ListaCotiDet)

                                PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, strDescServicio, ListaCotiDet, BECot.ListaCotizacionVuelo, 0, vchrAccionLog)
                            Else
                                PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, strDescServicio, BECot.ListaDetalleCotiz, _
                                                                    BECot.ListaCotizacionVuelo, 0, "N") ', vblnIDReservaCambiosLog, vdatFechaOut, vbytNoches)
                            End If

                            'If objBEDetRes.FechaOut.ToShortDateString = "14/07/2015" Then
                            '    Dim na As Integer = 0
                            'End If

                            objBEDetRes.CapacidadHab = bytCapacidadHab
                            'objBEDetRes.Cantidad = bytCantidad
                            If Item.AcomodoEspecial = False Then
                                objBEDetRes.Cantidad = intCantidadBD
                            Else
                                objBEDetRes.Cantidad = bytCantTipoHabitEspecialxCoCapacidad(strCoCapacidadEspecial, ListaAcomodosEspec, blnMatrim)
                            End If

                            objBEDetRes.EsMatrimonial = blnMatrim
                            objBEDetRes.IDReserva = ItemReserva.IDReserva
                            objBEDetRes.IDFile = BECot.IDFile
                            'objBEDetRes.NroPax = bytCapacidadHab * bytCantidad
                            objBEDetRes.NroPax = bytCapacidadHab * intCantidadBD
                            objBEDetRes.NroLiberados = 0
                            If Item.Dias > 1 Then
                                If InStrRev(Item.Servicio, " - ") > 0 Then
                                    If Item.AcomodoEspecial = True Then
                                        strCoCapacidadEspecial = strCoCapacidadporCantidad(Item.IDDet, blnEsGuide, bytCapacMax, blnMatrim, ListaCoCapacidad)
                                        objBEDetRes.Servicio = Item.Servicio.Substring(0, InStrRev(Item.Servicio, "-") - 2) & " " & objBEDetRes.DetaTipo & " " & strDescCapacidadHabitacionAcomodoEspecial(dttCapacEspecial, strCoCapacidadEspecial)
                                    Else
                                        objBEDetRes.Servicio = Item.Servicio.Substring(0, InStrRev(Item.Servicio, "-") - 2) & " " & objBEDetRes.DetaTipo & " " & strDescCapacidadHabitacion(bytCapacMax, blnMatrim)
                                    End If

                                Else
                                    If Item.AcomodoEspecial = True Then
                                        strCoCapacidadEspecial = strCoCapacidadporCantidad(Item.IDDet, blnEsGuide, bytCapacMax, blnMatrim, ListaCoCapacidad)
                                        objBEDetRes.Servicio = Item.Servicio & " " & objBEDetRes.DetaTipo & " " & strDescCapacidadHabitacionAcomodoEspecial(dttCapacEspecial, strCoCapacidadEspecial)
                                    Else
                                        objBEDetRes.Servicio = Item.Servicio & " " & objBEDetRes.DetaTipo & " " & strDescCapacidadHabitacion(bytCapacMax, blnMatrim)
                                    End If
                                End If
                            End If
                            'objBEDetRes.IncGuia=Item.IncGuia
                            If bytCantidad > 0 Then
                                If vblnTemp Then
                                    'If ListaIDDet.Contains(objBEDetRes.IDDet) Then
                                    '    'objBEDetRes.IDReserva_Det = objBEDetRes.IDReserva_Det + 1
                                    '    objBEDetRes.IDReserva_Det = intDevuelveSgteIDReserva_Det(BECot.ListaDetalleCotiz, objBEDetRes.IDReserva_Det, _
                                    '                                                             bytCapacidadHab, blnMatrim, objBEDetRes.IDDet)
                                    'End If
                                    objBEDetRes.IDReserva_Det = intDevuelveSgteIDReserva_Det(vListDetalleCotiz, objBEDetRes.IDReserva_Det, _
                                                                                                 bytCapacidadHab, blnMatrim, objBEDetRes.IDDet, bytIndexItemGen)
                                    If Not ListaIDReservaDetBD.Contains(objBEDetRes.IDReserva_Det) Then bytIndexItemGen += 1
                                End If
                                'ListaDetRes.Add(objBEDetRes)
                                If vblnTemp Then
                                    If (Not blnFinGenerarAcomodo Or Not ListaIDReservaDet.Contains(objBEDetRes.IDReserva_Det)) Then
                                        ListaDetRes.Add(objBEDetRes)
                                        ListaIDReservaDet.Add(objBEDetRes.IDReserva_Det)
                                    End If
                                Else
                                    ListaDetRes.Add(objBEDetRes)
                                End If
                                If vblnTemp Then
                                    ListaIDDet.Add(objBEDetRes.IDDet)
                                End If
                            End If


                            If bytSimple < 0 Then bytSimple = 0
                            If bytTwin < 0 Then bytTwin = 0
                            If bytMatrimonial < 0 Then bytMatrimonial = 0
                            If bytTriple < 0 Then bytTriple = 0


                            PasarDatosPropiedadesaCostosDetReservasBE(objBEDetRes, bytSimple, bytTwin, bytMatrimonial, _
                                                                      bytTriple, intCuadruple, intQuintuple, intSextuple, _
                                                                      intSeptuple, intOctuple, _
                                                                      BECot.TipoCambio, BECot.ListaDetalleCotiz, , , , , , blnCapacMaxMayorNroPax, BECot)

                            If bytTwin > 0 And bytMatrimonial > 0 Then
                            Else
                                If bytCantidad > 0 Then
                                    '#2intContHab -= (bytCantidad * bytCapacidadHab)
                                    'intContHab -= bytCantidad

                                    If Item.AcomodoEspecial Then
                                        intContHab -= bytDevuelveCantidadxCoCapacidad(ListaAcomodosEspec, strCoCapacidadEspecial)
                                    Else
                                        intContHab -= bytCantidad
                                        'If bytCapacMax = 3 Then
                                        '    intContHab -= (bytCantidad * bytCapacidadHab)
                                        'Else
                                        '    intContHab -= bytCantidad
                                        'End If
                                    End If
                                Else
                                    intContHab = 0
                                End If
                            End If

                            'Dim intNextCapacidad As Byte = 0
                            If intContHab > 0 Then
RestarMax:
                                If bytCantidad = 0 Then bytCantidad = 1
                                If bytCapacMax >= intContHab Then

                                    If bytCapacMax = 3 Then
                                        'bytTriple -= 1
                                        bytTriple -= bytCantidad
                                        'intNextCapacidad = 3
                                    ElseIf bytCapacMax = 2 And bytTwin > 0 Then
                                        'bytTwin -= 1
                                        bytTwin -= bytCantidad
                                        'intNextCapacidad = 2
                                    ElseIf bytCapacMax = 2 And bytMatrimonial > 0 Then
                                        'bytMatrimonial -= 1
                                        bytMatrimonial -= bytCantidad
                                        'ElseIf bytCapacMax = 1 Then
                                        '    'bytSimple -= 1
                                        '    bytSimple -= bytCantidad
                                        'intNextCapacidad = 2
                                    ElseIf bytCapacMax = 1 And intSimple > 0 Then
                                        bytSimple -= intSimple
                                        'intNextCapacidad = 1
                                    ElseIf bytCapacMax = 1 And intSimpleGuide > 0 Then
                                        bytSimple -= intSimpleGuide
                                        'intNextCapacidad = 1
                                    End If

                                    'If bytCapacMax > 3 And Item.AcomodoEspecial = True Then
                                    If Item.AcomodoEspecial = True Then

                                        If bytCapacMax = 8 Then
                                            intOctuple -= bytCantidad
                                        ElseIf bytCapacMax = 7 And bytTwin > 0 Then
                                            intSeptuple -= bytCantidad
                                        ElseIf bytCapacMax = 6 And bytMatrimonial > 0 Then
                                            intSextuple -= bytCantidad
                                        ElseIf bytCapacMax = 5 Then
                                            intQuintuple -= bytCantidad
                                        ElseIf bytCapacMax = 4 Then
                                            intCuadruple -= bytCantidad
                                        End If
                                        'intSumaHabit -= bytCantidad

                                        ActualizarPorAtenderListaAcomodosEspec(strCoCapacidadEspecial, ListaAcomodosEspec, intSumaHabit)
                                        Dim intCMax As Int16 = 0
                                        For Each ItemST As stAcomodoEspecial In ListaAcomodosEspec
                                            If ItemST.PorAtender Then
                                                If ItemST.QtHabitaciones > intCMax Then intCMax = ItemST.QtCapacidad
                                            End If
                                        Next
                                        bytCapacMax = intCMax
                                        strCoCapacidadEspecial = ""
                                    End If

                                    ''If >>Nuevo [Solo If]
                                    If Not Item.AcomodoEspecial Then
                                        If bytCapacidadHab = 3 Then
                                            If bytTriple > 0 Then
                                                bytCapacMax = 3
                                            ElseIf bytTwin > 0 Then
                                                bytCapacMax = 2
                                                blnMatrim = False
                                            ElseIf bytMatrimonial > 0 Then
                                                bytCapacMax = 2
                                                blnMatrim = True
                                            ElseIf bytSimple > 0 Then
                                                bytCapacMax = 1
                                            End If
                                            'bytCapacMax -= 1
                                            'bytCapacMax = intNextCapacidad
                                            'bytCapacMax = bytsgtCapacidadMax(bytSimple, bytTwin, bytMatrimonial, bytTriple)
                                        Else
                                            If Not (bytMatrimonial > 0 Or bytTwin > 0) Then
                                                bytCapacMax -= 1
                                            End If
                                        End If
                                    End If

                                    If Item.AcomodoEspecial = True Then
                                        If intSumaHabit > 0 Then
                                            GoTo InicioHabitac
                                        Else
                                            blnFinGenerarAcomodo = True
                                        End If
                                    Else
                                        If bytCapacMax = 2 And (bytMatrimonial > 0 Or bytTwin > 0) And bytCantidad > 0 And Item.IncGuia = False Then
                                            GoTo InicioHabitac
                                        Else
                                            'GoTo RestarMax
                                        End If
                                    End If
                                    If Item.AcomodoEspecial = True Then
                                        If ((bytCapacMax = 1 And bytSimple = 0) Or (bytCapacMax = 2 And bytTwin = 0) Or _
                                            (bytCapacMax = 3 And bytTriple = 0) Or (bytCapacMax = 4 And intCuadruple = 0) Or (bytCapacMax = 5 And intQuintuple = 0) Or _
                                            (bytCapacMax = 6 And intSextuple = 0) Or (bytCapacMax = 7 And intSeptuple = 0) Or _
                                            (bytCapacMax = 8 And intOctuple = 0)) _
                                            And Item.IncGuia = False Then
                                            GoTo RestarMax
                                        End If
                                    End If

                                Else
                                    If bytCapacMax = 2 And bytMatrimonial > 0 And bytTwin > 0 Then
                                        bytTwin = 0
                                    Else

                                        'bytCapacMax -= 1

                                        'If bytCapacMax > 3 And Item.AcomodoEspecial = True Then
                                        If Item.AcomodoEspecial = True Then
                                            ActualizarPorAtenderListaAcomodosEspec(strCoCapacidadEspecial, ListaAcomodosEspec, intSumaHabit)
                                            blnActualizarPorAtenderListaAcomodosEspec = True
RestarCapacMax:
                                            If intCantTipoHabitEspecialxCapac(bytCapacMax, blnEsGuide, _
                                                                              ListaAcomodosEspec, blnMatrim, True) = 0 And bytCapacMax >= 0 Then
                                                'si no queda una hab. de la misma capac.
                                                bytCapacMax -= 1
                                                If bytCapacMax = 1 Then blnMatrim = False
                                                If bytCapacMax = 0 And Not blnEsGuide Then
                                                    blnEsGuide = True
                                                    bytCapacMax += 1
                                                End If

                                                strCoCapacidadEspecial = strCoCapacidadporCantidad(Item.IDDet, blnEsGuide, _
                                                                                                   bytCapacMax, blnMatrim, ListaCoCapacidad)
                                                If strCoCapacidadEspecial = "" And intSumaHabit > 0 Then
                                                    If bytCapacMax = 2 Then
                                                        blnMatrim = True
                                                        GoTo RestarCapacMax
                                                    End If
                                                    If bytCapacMax = 1 Then
                                                        blnEsGuide = True
                                                        GoTo RestarCapacMax
                                                    End If
                                                    GoTo RestarCapacMax
                                                End If
                                            Else
                                                strCoCapacidadEspecial = ""
                                            End If

                                            'If bytCapacMax = 7 And intSeptuple = 0 Then
                                            '    bytCapacMax -= 1
                                            'End If
                                            'If bytCapacMax = 6 And intSextuple = 0 Then
                                            '    bytCapacMax -= 1
                                            'End If
                                            'If bytCapacMax = 5 And intQuintuple = 0 Then
                                            '    bytCapacMax -= 1
                                            'End If
                                            'If bytCapacMax = 4 And intCuadruple = 0 Then
                                            '    bytCapacMax -= 1
                                            'End If
                                            'If bytCapacMax = 3 And bytTriple = 0 Then
                                            '    bytCapacMax -= 1
                                            'End If
                                            'If bytCapacMax = 2 And intDoble = 0 Then
                                            '    bytCapacMax -= 1
                                            'End If

                                            'intSumaHabit -= intCantidadBD

                                        Else

                                            If bytCapacMax = 3 Then
                                                bytCapacMax -= 1
                                                If bytTwin = 0 And bytMatrimonial = 0 Then
                                                    bytCapacMax -= 1
                                                End If
                                            Else
                                                bytCapacMax -= 1
                                            End If
                                        End If
                                    End If

                                End If
                                'If (bytCapacMax > 0) Then
                                '((bytSimple + bytTwin+bytMatrimonial+bytTriple) > 0) And Item.IncGuia = False Then
                                If Item.AcomodoEspecial = False Then
                                    If ((bytCapacMax = 1 And bytSimple > 0) Or (bytCapacMax = 2 And bytTwin > 0) Or _
                                    (bytCapacMax = 2 And bytMatrimonial > 0) Or (bytCapacMax = 3 And bytTriple > 0)) _
                                     And Item.IncGuia = False Then
                                        GoTo InicioHabitac
                                    End If

                                Else
                                    'If ((bytCapacMax = 1 And bytSimple > 0) Or (bytCapacMax = 2 And bytTwin > 0) Or _
                                    '    (bytCapacMax = 2 And bytMatrimonial > 0) Or (bytCapacMax = 3 And bytTriple > 0) Or _
                                    '    (bytCapacMax = 4 And intCuadruple > 0) Or (bytCapacMax = 5 And intQuintuple > 0) Or _
                                    '    (bytCapacMax = 6 And intSextuple > 0) Or (bytCapacMax = 7 And intSeptuple > 0) Or _
                                    '    (bytCapacMax = 8 And intOctuple > 0)) _
                                    '    And Item.IncGuia = False Then
                                    '    GoTo InicioHabitac
                                    'End If
                                    If Not blnActualizarPorAtenderListaAcomodosEspec Then
                                        ActualizarPorAtenderListaAcomodosEspec(strCoCapacidadEspecial, ListaAcomodosEspec, intSumaHabit)
                                    End If
                                    If intSumaHabit > 0 Then

                                        If intCantTipoHabitEspecialxCapac(bytCapacMax, blnEsGuide, ListaAcomodosEspec, blnMatrim, True) = 0 And bytCapacMax >= 0 Then
                                            'si no queda una hab. de la misma capac.
                                            bytCapacMax -= 1
                                            If bytCapacMax = 1 Then blnMatrim = False
                                            If bytCapacMax = 0 And Not blnEsGuide Then
                                                blnEsGuide = True
                                                bytCapacMax += 1
                                            End If
                                            strCoCapacidadEspecial = strCoCapacidadporCantidad(Item.IDDet, blnEsGuide, _
                                                                                               bytCapacMax, blnMatrim, ListaCoCapacidad)

                                            If strCoCapacidadEspecial = "" And intSumaHabit > 0 Then
                                                If bytCapacMax = 2 Then
                                                    blnMatrim = True
                                                    GoTo RestarCapacMax
                                                End If
                                                If bytCapacMax = 1 Then
                                                    blnEsGuide = True
                                                    GoTo RestarCapacMax
                                                End If
                                                GoTo RestarCapacMax
                                            End If
                                        Else
                                            strCoCapacidadEspecial = ""
                                        End If
                                        GoTo InicioHabitac
                                    Else
                                        'Cuando Finalizo la generación
                                        blnFinGenerarAcomodo = True
                                        'If Item.CantidadAcomodoEsp = bytContAcomodosEsp Then bytContAcomodosEsp = 0
                                    End If
                                End If

                            Else
                                'Cuando Finalizo la generación
                                blnFinGenerarAcomodo = True
                            End If
                        Else
                            'If Item.IDProveedor = "000387" Then
                            '    Item.IDProveedor = "000387"
                            'End If
                            'Dim objBEDetRes As New clsReservaBE.clsDetalleReservaBE
                            'PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, strDescServicio, BECot.ListaDetalleCotiz, "N")

                            'objBEDetRes.CapacidadHab = 0
                            'objBEDetRes.Cantidad = 0
                            'objBEDetRes.EsMatrimonial = False
                            'objBEDetRes.IDReserva = ItemReserva.IDReserva
                            'objBEDetRes.IDFile = BECot.IDFile

                            'ListaDetRes.Add(objBEDetRes)

                            'PasarDatosPropiedadesaCostosDetReservasBE(objBEDetRes, 0, 0, 0, 0)

                        End If
                    End If



                    ''Reservas_DetServicios
                    'ListaDetServRes = New List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE)
                    'If Not BECot.ListaDetalleCotizacionDetServicio Is Nothing Then
                    '    For Each ItemDetS As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE In BECot.ListaDetalleCotizacionDetServicio
                    '        If ItemDetS.IDDet = Item.IDDet Then
                    '            Dim objBEDetServRes As New clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE
                    '            PasarDatosPropiedadesaDetServiciosReservasBE(objBEDetServRes, ItemDetS, "N")
                    '            ListaDetServRes.Add(objBEDetServRes)

                    '        End If
                    '    Next
                    'End If



SgteServ:
                Next

            Next

            'Limpiar Detalles redundantes.
            If vblnTemp Then QuitarItemsRepetidosDeListaReservasDet(ListaDetRes)

            'Ajustar la cantidad A pagar para los servicios que tengan Liberado por Habitaciones
            'If vblnTemp Then LiberarHabitacionesPorAcomodosIngresados(ListaDetRes)
            LiberarHabitacionesPorAcomodosIngresados(ListaDetRes, False)

            Dim objBTDetRes As New clsReservaBT.clsDetalleReservaBT
            objBERes = New clsReservaBE
            objBERes.ListaDetReservas = ListaDetRes


            ''DETPAX


            ''FIN DETPAX''
            If Not vblnTemp Then
                objBTDetRes.InsertarActualizarEliminar(objBERes, 0)
            Else
                objBERes.Accion = "N"
                objBTDetRes.InsertarActualizarEliminarTMP(objBERes, 0)
            End If

            If vblnResidente = True Then
                If Not vblnTemp Then
                    For Each Item As stProveedoresReservas In vListaProveed
                        If (Item.EsAlojamiento) Then
                            If Not vblnTemp Then
                                EliminarFilasDuplicadasxAcomodoEspecialResidente(Item.IDReserva)
                            Else
                                EliminarFilasDuplicadasxAcomodoEspecialResidenteTMP(Item.IDReserva)
                            End If
                        End If
                    Next
                Else
                    If ListaDetRes IsNot Nothing Then
                        If ListaDetRes.Count > 0 Then
                            Dim intIDReseva As Integer = CInt(ListaDetRes.FirstOrDefault().IDReserva)
                            If Not vblnTemp Then
                                EliminarFilasDuplicadasxAcomodoEspecialResidente(intIDReseva)
                            Else
                                EliminarFilasDuplicadasxAcomodoEspecialResidenteTMP(intIDReseva)
                            End If
                        End If
                    End If
                End If
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function bytsgtCapacidadMax(ByRef vbytSingle As Byte, ByRef vbytDoubleTwin As Byte, ByRef vbytDoubleMat As Byte, ByRef vbytTriple As Byte) As Byte
        Try
            Dim vbytSingle2 As Byte = vbytSingle, vbytDoubleTwin2 As Byte = vbytDoubleTwin, vbytDoubleMat2 As Byte = vbytDoubleMat, vbytTriple2 As Byte = vbytTriple
            If vbytTriple > 0 Then
                vbytTriple = 0
                If vbytDoubleTwin > 0 Then
                    vbytDoubleTwin = 0
                    Return vbytDoubleTwin2
                ElseIf vbytDoubleMat > 0 Then
                    vbytDoubleMat = 0
                    Return vbytDoubleMat2
                ElseIf vbytSingle > 0 Then
                    vbytSingle = 0
                    Return vbytSingle2
                End If
            End If

            If vbytDoubleTwin > 0 Then
                vbytDoubleTwin = 0
                If vbytDoubleMat > 0 Then
                    vbytDoubleMat = 0
                    Return vbytDoubleMat2
                ElseIf vbytSingle > 0 Then
                    vbytSingle = 0
                    Return vbytSingle2
                End If
            End If

            If vbytDoubleMat > 0 Then
                vbytDoubleMat = 0
                If vbytSingle > 0 Then
                    vbytSingle = 0
                    Return vbytSingle2
                End If
            End If

            Return vbytSingle2
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EliminarFilasDuplicadasxAcomodoEspecialResidenteTMP(ByVal vintIDReserva As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)

            objADO.ExecuteSP("RESERVAS_DET_TMP_Del_DetalleIgual", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub EliminarFilasDuplicadasxAcomodoEspecialResidente(ByVal vintIDReserva As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)

            objADO.ExecuteSP("RESERVAS_DET_Del_DetalleIgual", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function blnExisteLiberadosEnAcomodo(ByVal vintIDReservas As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReservas)
            dc.Add("@pExisteLiberado", False)

            Return objADO.GetSPOutputValue("RESERVAS_SelExistLiberados", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub LiberarHabitacionesPorAcomodosIngresados(ByRef rListDetReserva As List(Of clsReservaBE.clsDetalleReservaBE), _
                                                         ByVal vblnPlanAlimenticio As Boolean)
        Try
            Dim intIDDet As Integer = 0
            rListDetReserva = (From Rev In rListDetReserva _
                               Select Rev _
                               Order By Rev.Dia, Rev.IDDet Ascending, Rev.IDServicio_Det Ascending, Rev.CapacidadHab Ascending).ToList()
            Dim bytCantLiberada As Byte = 0
            Dim blnAplicaPolitica As Boolean = True
            For Each Item As clsReservaBE.clsDetalleReservaBE In rListDetReserva
                If Item.NuViaticosTC = 0 Then
                    If vblnPlanAlimenticio And Item.IDTipoProv = gstrTipoProvHotel Then
                        blnAplicaPolitica = blnExisteLiberadosEnAcomodo(Item.IDReserva)
                    End If
                    If intIDDet <> Item.IDDet Then
                        Dim bytTotalHab As Byte = bytCounHabitacionesxIDDet(Item.IDDet, rListDetReserva)
                        If (Item.IDTipoProv = gstrTipoProvOper And Item.ConAlojamiento) And blnExistePorRangoDeCostos(Item.IDServicio_Det) Then
                            bytTotalHab = intDevuelveNroPaxxIDDet(Item.IDDet)
                        End If
                        Dim dtDatoServ As DataTable = dtConsultarDatosServicios_Det(Item.IDServicio_Det)
                        Dim bytCantidadLiberar As Byte = 0
                        For Each drowItem As DataRow In dtDatoServ.Rows
                            Dim blnPoliticaLiberados As Boolean = Convert.ToBoolean(drowItem("PoliticaLiberado"))
                            Dim strTipoLiberado As String = If(IsDBNull(drowItem("TipoLib")) = True, "", drowItem("TipoLib").ToString())
                            If blnPoliticaLiberados And strTipoLiberado = "H" And blnAplicaPolitica Then
                                Dim cantidadLiberado As Byte = If(IsDBNull(drowItem("Liberado")) = True, 0, CByte(drowItem("Liberado")))
                                If bytTotalHab > cantidadLiberado Then
                                    If Convert.ToInt16(If(IsDBNull(drowItem("MaximoLiberado")) = True, 0, drowItem("MaximoLiberado"))) > 0 Then
                                        If bytTotalHab > Convert.ToInt16(If(IsDBNull(drowItem("MaximoLiberado")) = True, 0, drowItem("MaximoLiberado"))) Then
                                            bytCantidadLiberar = drowItem("MaximoLiberado")
                                        End If
                                    Else
                                        If cantidadLiberado > 0 And bytTotalHab > cantidadLiberado Then
                                            bytCantidadLiberar = bytTotalHab \ cantidadLiberado
                                        End If
                                    End If
                                End If
                            ElseIf blnPoliticaLiberados And strTipoLiberado = "P" And (Item.IDTipoProv = gstrTipoProvOper And Item.ConAlojamiento) And blnExistePorRangoDeCostos(Item.IDServicio_Det) Then
                                Item.CantidadAPagar += 1
                                Dim cantidadLiberado As Byte = If(IsDBNull(drowItem("Liberado")) = True, 0, CByte(drowItem("Liberado")))
                                Dim MaxLiberado As Byte = If(IsDBNull(drowItem("MaximoLiberado")) = True, 0, drowItem("MaximoLiberado"))
                                If bytTotalHab > (cantidadLiberado + MaxLiberado) Then
                                    If MaxLiberado > 0 Then
                                        bytCantidadLiberar = MaxLiberado
                                    Else
                                        If cantidadLiberado > 0 And bytTotalHab > cantidadLiberado Then
                                            bytCantidadLiberar = bytTotalHab \ cantidadLiberado
                                        End If
                                    End If
                                End If
                            ElseIf blnPoliticaLiberados And strTipoLiberado = "P" And blnAplicaPolitica Then
                                Dim cantidadLiberado As Byte = If(IsDBNull(drowItem("Liberado")) = True, 0, CByte(drowItem("Liberado")))
                                bytTotalHab = intDevuelveNroPaxxIDDet(Item.IDDet)
                                If bytTotalHab > cantidadLiberado And Item.CantidadAPagar = Item.Cantidad Then
                                    If Convert.ToInt16(If(IsDBNull(drowItem("MaximoLiberado")) = True, 0, drowItem("MaximoLiberado"))) > 0 Then
                                        If bytTotalHab > Convert.ToInt16(If(IsDBNull(drowItem("MaximoLiberado")) = True, 0, drowItem("MaximoLiberado"))) Then
                                            bytCantidadLiberar = drowItem("MaximoLiberado")
                                        End If
                                    Else
                                        If cantidadLiberado > 0 And bytTotalHab > cantidadLiberado Then
                                            bytCantidadLiberar = bytTotalHab \ cantidadLiberado
                                        End If
                                    End If
                                End If
                            Else
                                Exit For
                            End If
                        Next
                        If bytCantidadLiberar > 0 Then
ActualizarLib:
                            If bytCantLiberada > bytCantidadLiberar Then bytCantidadLiberar = 0 Else bytCantidadLiberar -= bytCantLiberada
                            For Each item2 As clsReservaBE.clsDetalleReservaBE In rListDetReserva
                                If ((item2.Dia.ToShortDateString = Item.Dia.ToShortDateString And item2.IDDet = Item.IDDet And _
                                    item2.IDServicio_Det = item2.IDServicio_Det) And bytCantidadLiberar > 0) Then
                                    Dim bytPag As Byte = item2.CantidadAPagar - If(bytCantidadLiberar > item2.CantidadAPagar, 0, bytCantidadLiberar)
                                    If bytCantidadLiberar > item2.CantidadAPagar Then
                                        item2.CantidadAPagar = 0
                                    Else
                                        item2.CantidadAPagar = bytPag
                                    End If
                                    If (item2.IDTipoProv = gstrTipoProvHotel And Not Item.PlanAlimenticio) Or _
                                       (item2.IDTipoProv <> gstrTipoProvHotel And item2.ConAlojamiento) Then
                                        item2.NetoGen = item2.CantidadAPagar * item2.Noches * item2.NetoHab
                                        item2.IgvGen = item2.CantidadAPagar * item2.Noches * item2.IgvHab
                                        item2.TotalGen = item2.CantidadAPagar * item2.Noches * item2.TotalHab
                                    Else
                                        item2.NetoGen = item2.CantidadAPagar * item2.NetoHab
                                        item2.IgvGen = item2.CantidadAPagar * item2.IgvHab
                                        item2.TotalGen = item2.CantidadAPagar * item2.TotalHab
                                    End If
                                    If item2.Cantidad >= item2.CantidadAPagar Then
                                        bytCantLiberada = item2.Cantidad - item2.CantidadAPagar
                                    End If
                                    'blnUpdLiberadoHotel = True
                                    If item2.NroPax + item2.NroLiberados = 1 Then
                                        item2.NroPax = 1
                                        item2.NroLiberados = 0
                                        item2.Tipo_Lib = String.Empty
                                    End If
                                    GoTo ActualizarLib
                                End If
                            Next
                        End If

                        intIDDet = Item.IDDet
                        bytCantLiberada = 0
                    End If

                    If Item.NroPax + Item.NroLiberados = 1 Then
                        Item.NroPax = 1
                        Item.NroLiberados = 0
                        Item.Tipo_Lib = String.Empty
                    End If
                End If
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Private Function blnExisteLiberadoHotel(ByVal vListResDet As List(Of clsReservaBE.clsDetalleReservaBE)) As Boolean
    '    Try
    '        For Each Item As clsReservaBE.clsDetalleReservaBE In vListResDet

    '        Next
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Private Function bytCounHabitacionesxIDDet(ByVal vintIDDet As Integer, _
                                               ByVal vListDetReservas As List(Of clsReservaBE.clsDetalleReservaBE)) As Byte
        Try
            Dim vbytCantiHabit As Byte = 0
            For Each Item As clsReservaBE.clsDetalleReservaBE In vListDetReservas
                If Item.IDDet = vintIDDet Then
                    vbytCantiHabit += Item.Cantidad
                End If
            Next
            Return vbytCantiHabit
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtConsultarDatosServicios_Det(ByVal vintIDServicio_Det As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            Return objADO.GetDataTable("MASERVICIOS_DET_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub QuitarItemsRepetidosDeListaReservasDet(ByRef rListDetReserva As List(Of clsReservaBE.clsDetalleReservaBE))
        Try
RemoverItemIdentico:
            Dim ListaCadena As New List(Of String)
            For Each item As clsReservaBE.clsDetalleReservaBE In rListDetReserva
                If item.NroPax + item.NroLiberados = 1 Then
                    item.NroPax = 1
                    item.NroLiberados = 0
                    item.Tipo_Lib = String.Empty
                End If
                Dim FechaIn As String = Format(item.Dia, "dd/MM/yyyy HH:mm:ss")
                Dim FechaOut As String = Format(item.FechaOut, "dd/MM/yyyy HH:mm:ss")
                Dim IDUbigeo As String = item.IDUbigeo
                Dim NroPax As String = item.NroPax
                Dim Noches As String = item.Noches
                Dim Cantidad As String = item.Cantidad
                Dim Servicios As String = item.Servicio
                Dim IDIdioma As String = item.IDIdioma
                Dim IDServicioDet As String = item.IDServicio_Det
                Dim CantidadPagar As String = item.CantidadAPagar
                Dim blnGuia As Boolean = item.IncGuia

                Dim strCamposValidadoM As String = FechaIn & vbTab & FechaOut & vbTab & IDUbigeo & vbTab & NroPax & vbTab & Noches & vbTab & Cantidad & vbTab & Servicios _
                & vbTab & IDIdioma & vbTab & IDServicioDet & vbTab & CantidadPagar & blnGuia.ToString()

                If ListaCadena.Contains(strCamposValidadoM) Then
                    rListDetReserva.Remove(item)
                    GoTo RemoverItemIdentico
                Else
                    ListaCadena.Add(strCamposValidadoM)
                End If
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function intDevuelveSgteIDReserva_Det(ByVal vListDetCotiz As List(Of clsCotizacionBE.clsDetalleCotizacionBE), _
                                                  ByVal vintIDReservasDetAnt As Integer, ByVal vbytCapacidad As Byte, _
                                                  ByVal blnEsMatrimonial As Boolean, ByVal vintIDDet As Integer, _
                                                  ByVal vbytNroItemGenerado As Byte) As Integer
        Try
            ' ''Dim LstOrdenada = (From Item In vListDetCotiz _
            ' ''                  Where Item.IDReserva_Det > vintIDReservasDetAnt And Item.IDDet = vintIDDet.ToString() _
            ' ''                  (Item.Capacidad = vbytCapacidad And Item.EsMatrimonial = blnEsMatrimonial) _
            ' ''                  Order By Item.IDProveedor, Item.IDReserva_Det Ascending).ToList()
            Dim intIDReservasDet As Integer = 0
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCotiz
                If Item.IDDet = vintIDDet And Item.EsMatrimonial = blnEsMatrimonial And Item.Capacidad = vbytCapacidad Then
                    intIDReservasDet = Item.IDReserva_Det
                    Exit For
                End If
            Next

            If intIDReservasDet = 0 Then
                Dim intMayorIDReserva_Det As Integer = 0
                For Each Item2 As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCotiz
                    If Convert.ToInt32(Item2.IDReserva_Det) > intMayorIDReserva_Det Then
                        intMayorIDReserva_Det = Convert.ToInt32(Item2.IDReserva_Det)
                    End If
                Next
                intIDReservasDet += intMayorIDReserva_Det + vbytNroItemGenerado
            End If

            ''Nuevo
            'If intIDReservasDet = 0 Then
            '    Dim intMayorIDReserva_Det As Integer = 0
            '    For Each Item2 As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCotiz
            '        If Item2.IDDet = vintIDDet Then
            '            If Convert.ToInt32(Item2.IDReserva_Det) > intMayorIDReserva_Det Then
            '                intMayorIDReserva_Det = Convert.ToInt32(Item2.IDReserva_Det)
            '            End If
            '        End If
            '    Next
            '    intIDReservasDet += intMayorIDReserva_Det + 1
            '    Dim blnExisteIDReservaDet As Boolean = False
            '    For Each Item2 As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCotiz
            '        If Item2.IDReserva_Det = intIDReservasDet Then
            '            blnExisteIDReservaDet = True
            '            Exit For
            '        End If
            '    Next

            '    If blnExisteIDReservaDet Then
            '        intMayorIDReserva_Det = 0
            '        For Each Item2 As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCotiz
            '            If Convert.ToInt32(Item2.IDReserva_Det) > intMayorIDReserva_Det Then intMayorIDReserva_Det = Convert.ToInt32(Item2.IDReserva_Det)
            '        Next
            '        intIDReservasDet = intMayorIDReserva_Det + 1
            '    End If
            'End If

            Return intIDReservasDet
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function intCantTipoHabitEspecialxCapac(ByVal vbytCapac As Int16, ByVal vblnIsGuide As Boolean, ByVal ListaAcomodosEspec As List(Of stAcomodoEspecial), _
                                                    ByRef vblnEsMatrimonial As Boolean, Optional ByVal vblnEvalMatrimimonial As Boolean = False) As Int16
        'If vbytCapac < 0 Then Return 0

        Dim intReturn As Int16 = 0
        For Each Item As stAcomodoEspecial In ListaAcomodosEspec
            If Not vblnEvalMatrimimonial Then
                If Item.QtCapacidad = vbytCapac And Item.EsMatrimonial = vblnEsMatrimonial And Item.PorAtender = True _
                    And Item.EsSevicioGuia = vblnIsGuide Then
                    intReturn += Item.QtHabitaciones
                    'vblnEsMatrimonial = Item.EsMatrimonial
                End If
            Else
                If Item.QtCapacidad = vbytCapac And Item.EsSevicioGuia = vblnIsGuide And _
                   Item.PorAtender = True Then
                    intReturn += Item.QtHabitaciones
                    'vblnEsMatrimonial = Item.EsMatrimonial
                End If
            End If
        Next

        Return intReturn
    End Function
    Private Function bytCantTipoHabitEspecialxCoCapacidad(ByVal vstrCoCapacidad As String, ByVal ListaAcomodosEspec As List(Of stAcomodoEspecial), _
                                                          ByRef vblnEsMatrimonial As Boolean) As Byte

        Dim bytReturn As Byte = 0
        For Each Item As stAcomodoEspecial In ListaAcomodosEspec
            If Item.CoCapacidad = vstrCoCapacidad And Item.EsMatrimonial = vblnEsMatrimonial Then
                vblnEsMatrimonial = Item.EsMatrimonial
                Return Item.QtHabitaciones
            End If
        Next

        Return bytReturn
    End Function
    Private Sub ActualizarPorAtenderListaAcomodosEspec(ByVal vstrCoCapacidad As String, _
                                                       ByRef rListaAcomodosEspec As List(Of stAcomodoEspecial), ByRef rintSumaHabit As Int16)

        Dim blnEntro As Boolean = False
        Dim bytQtCapacidad As Byte
        Dim bytQtHabitaciones As Byte
        Dim blnEsMatrimonial As Boolean = False
        Dim blnEsGuide As Boolean = False
        For Each Item As stAcomodoEspecial In rListaAcomodosEspec
            If Item.CoCapacidad = vstrCoCapacidad And Item.PorAtender = True Then
                blnEntro = True
                bytQtCapacidad = Item.QtCapacidad
                bytQtHabitaciones = Item.QtHabitaciones
                blnEsMatrimonial = Item.EsMatrimonial
                blnEsGuide = Item.EsSevicioGuia
                rListaAcomodosEspec.Remove(Item)
                Exit For
            End If
        Next
        If blnEntro Then
            rintSumaHabit -= bytQtHabitaciones
            Dim ItemNue As New stAcomodoEspecial With {.CoCapacidad = vstrCoCapacidad, _
                                                       .QtCapacidad = bytQtCapacidad, _
                                                       .QtHabitaciones = bytQtHabitaciones, _
                                                       .EsMatrimonial = blnEsMatrimonial, _
                                                       .EsSevicioGuia = blnEsGuide, _
                                                       .PorAtender = False}
            rListaAcomodosEspec.Add(ItemNue)
        End If
    End Sub

    'Private Function intCapacMaxAcomodoEspecial(ByVal vintIDDet As Integer, _
    '                                            ByRef rstrCoCapacidad As String, _
    '                                            ByRef rintSimple As Int16, _
    '                                            ByRef rintTwin As Int16, _
    '                                            ByRef rintMat As Int16, _
    '                                            ByRef rintTriple As Int16, _
    '                                            ByRef rintCuadruple As Int16, _
    '                                            ByRef rintQuintuple As Int16, _
    '                                            ByRef rintSextuple As Int16, _
    '                                            ByRef rintSeptuple As Int16, _
    '                                            ByRef rintOctuple As Int16, _
    '                                            ByRef rintSumaHabit As Int16) As Int16
    '    Try
    '        Dim objVtaBT As New clsDetalleVentasCotizacionBT
    '        Dim dttAcom As DataTable = objVtaBT.ConsultarAcomodoEspecial(vintIDDet)

    '        Dim bytCapacMax As Byte = 0
    '        For Each dr As DataRow In dttAcom.Rows

    '            If dr("QtCapacidad") > bytCapacMax Then
    '                bytCapacMax = dr("QtCapacidad")
    '                rstrCoCapacidad = dr("CoCapacidad")
    '            End If

    '        Next

    '        rintSimple = 0
    '        rintTwin = 0
    '        rintMat = 0
    '        rintTriple = 0
    '        For Each dr As DataRow In dttAcom.Rows
    '            If dr("QtCapacidad") = 1 Then rintSimple = dr("QtPax")
    '            If dr("QtCapacidad") = 2 Then
    '                If dr("CoCapacidad") = "03" Then
    '                    rintMat = dr("QtPax")
    '                Else
    '                    rintTwin = dr("QtPax")
    '                End If

    '            End If
    '            If dr("QtCapacidad") = 3 Then rintTriple = dr("QtPax")
    '            If dr("QtCapacidad") = 4 Then rintCuadruple = dr("QtPax")
    '            If dr("QtCapacidad") = 5 Then rintQuintuple = dr("QtPax")
    '            If dr("QtCapacidad") = 6 Then rintSextuple = dr("QtPax")
    '            If dr("QtCapacidad") = 7 Then rintSeptuple = dr("QtPax")
    '            If dr("QtCapacidad") = 8 Then rintOctuple = dr("QtPax")
    '            rintSumaHabit += dr("QtPax")
    '        Next

    '        Return bytCapacMax
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Private Function intCapacMaxAcomodoEspecial(ByVal vintIDDet As Integer, _
                                                ByRef rListaAcomodosEspec As List(Of stAcomodoEspecial), _
                                                ByRef rintSumaHabit As Int16) As Int16
        Try
            Dim objVtaBT As New clsDetalleVentasCotizacionBT
            Dim dttAcom As DataTable = objVtaBT.ConsultarAcomodoEspecial(vintIDDet)

            Dim bytCapacMax As Byte = 0
            For Each dr As DataRow In dttAcom.Rows

                If dr("QtCapacidad") > bytCapacMax Then
                    bytCapacMax = dr("QtCapacidad")
                    'rstrCoCapacidad = dr("CoCapacidad")
                End If

            Next

            For Each dr As DataRow In dttAcom.Rows
                Dim ItemAcomEspe As New stAcomodoEspecial With {.CoCapacidad = dr("CoCapacidad"), _
                                                                .QtCapacidad = dr("QtCapacidad"), _
                                                                .QtHabitaciones = dr("QtPax"), _
                                                                .EsMatrimonial = CBool(dr("FlMatrimonial")), _
                                                                .EsSevicioGuia = CBool(dr("FlGuide")), _
                                                                .PorAtender = True}
                rintSumaHabit += dr("QtPax")
                rListaAcomodosEspec.Add(ItemAcomEspe)
            Next

            Return bytCapacMax
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarAcomodosxIDReserva3(ByVal vintIDReserva As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)

            Return objADO.GetDataTable("ACOMODOPAX_PROVEEDOR_SelxIDReserva3", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub ActualizarDetPaxPostGeneracion(ByVal vintIDReserva As Integer, ByVal vblnEsAlojamiento As Boolean, ByVal vstrUserMod As String)
        Try

            'Dim dc3 As New Dictionary(Of String, String)
            'dc3.Add("@IDReserva", vintIDReserva)

            Dim dtDetRes As DataTable = ConsultarxIDReserva2(vintIDReserva) 'objADO.GetDataTable("RESERVAS_DET_SelxIDReserva3", dc3)
            Dim dtAcomodosNormales As DataTable = ConsultarAcomodosxIDReserva3(vintIDReserva)
            Dim ListaDetRes As New List(Of clsReservaBE.clsDetalleReservaBE)
            For Each dr As DataRow In dtDetRes.Rows
                ListaDetRes.Add(New clsReservaBE.clsDetalleReservaBE With {.IDDet = If(IsDBNull(dr("IDDet")) = True, 0, dr("IDDet")), _
                                                                           .IDReserva_Det = dr("IDReserva_Det"), _
                                                                           .Servicio = dr("Servicio"), _
                                                                           .IDProveedor = dr("IDProveedor"), _
                                                                           .NroPax = dr("NroPax"), _
                                                                           .NroLiberados = dr("NroLiberados"), _
                                                                           .AcomodoEspecial = dr("AcomodoEspecial"), _
                                                                           .CapacidadHab = dr("CapacidadHab"), _
                                                                           .EsMatrimonial = dr("EsMatrimonial")})
            Next


            'If vblnTemp Then ''
            Dim ListaResDetPax As New List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
            'Dim bytContIDReserva_DetTemp As Byte = 1
            For Each ItemDetRes As clsReservaBE.clsDetalleReservaBE In ListaDetRes
                Dim objVtaDetPaxBT As New clsDetallePaxVentasCotizacionBT
                Dim dttCotiDetPax As DataTable = objVtaDetPaxBT.ConsultarDetxIDDet(ItemDetRes.IDDet)
                Dim intContPax As Int16 = ItemDetRes.NroPax + ItemDetRes.NroLiberados
                Dim intCont As Int16 = 0

                If ItemDetRes.CapacidadHab > 0 Then
                    Dim dttTemp As DataTable = ConsultarListxIDDetxCapacidad(ItemDetRes.IDDet, ItemDetRes.CapacidadHab, ItemDetRes.EsMatrimonial)
                    If dtAcomodosNormales.Rows.Count > 0 And dttTemp.Rows.Count = 0 Then
                        Dim strEsMatrimonial As String = If(ItemDetRes.EsMatrimonial, "1", "0")
                        dttCotiDetPax = dtFiltrarDataTable(dtAcomodosNormales, "CapacidadHab=" & ItemDetRes.CapacidadHab.ToString() & " And EsMatrimonial=" & strEsMatrimonial)
                    ElseIf ItemDetRes.AcomodoEspecial Then
                        If dttTemp.Rows.Count > 0 Then dttCotiDetPax = dttTemp
                    End If
                End If

                'Dim ListaResDetPaxTmp As New List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
                For Each drCotiDetPax As DataRow In dttCotiDetPax.Rows
                    Dim intIDPax As Integer = drCotiDetPax("IDPax")
                    Dim intIDReserva_Det As Integer = ItemDetRes.IDReserva_Det
                    Dim blnExisteEnLista As Boolean = False 'blnExisteIDDetyPaxenLista(ListaResDetPax, ItemDetRes.IDDet, intIDPax)
                    If vblnEsAlojamiento Or (ItemDetRes.IDProveedor = gstrIDProveedorSetoursLima Or ItemDetRes.IDProveedor = gstrIDProveedorSetoursCusco Or _
                                              ItemDetRes.IDProveedor = gstrIDProveedorSetoursBuenosAires Or ItemDetRes.IDProveedor = gstrIDProveedorSetoursSantiago) Then
                        blnExisteEnLista = blnExisteIDDetyPaxenLista(ListaResDetPax, ItemDetRes.IDDet, intIDPax)
                    Else
                        blnExisteEnLista = blnExisteIDDetyPaxenLista(ListaResDetPax, ItemDetRes.IDDet, intIDPax, True, ItemDetRes.IDReserva_Det)
                    End If


                    'If Not blnExisteIDDetyPaxenLista(ListaResDetPax, ItemDetRes.IDDet, intIDPax) Then
                    If Not blnExisteEnLista Then
                        Dim objResDetPax As New clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE
                        objResDetPax.IDDet = ItemDetRes.IDDet
                        objResDetPax.IDReserva_Det = intIDReserva_Det
                        objResDetPax.IDPax = intIDPax
                        objResDetPax.Selecc = True
                        objResDetPax.UserMod = vstrUserMod
                        ListaResDetPax.Add(objResDetPax)
                        intCont += 1
                        If intCont = intContPax Then
                            Exit For
                        End If
                    End If
                Next
            Next

            Dim objBERes As New clsReservaBE
            objBERes.ListaPaxDetReservas = ListaResDetPax
            'End If
            Dim objDetResPaxBT As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT


            objDetResPaxBT.InsertarEliminar(objBERes, 0, "N")


        Catch ex As Exception
            Throw
        End Try

    End Sub
    Private Function blnExisteIDDetyPaxenLista(ByVal vListaResDetPax As List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE), _
                                               ByVal vintIDDet As Integer, ByVal vintIDPax As Integer, _
                                               Optional ByVal vblnBusqEnReservas As Boolean = False, _
                                               Optional ByVal vintIDReservas_Det As Integer = 0) As Boolean
        Try
            If Not vblnBusqEnReservas Then
                For Each ItemPax As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In vListaResDetPax
                    If ItemPax.IDDet = vintIDDet And ItemPax.IDPax = vintIDPax Then 'And ItemPax.IDReserva_DetTemp = vbytIDReserva_DetTemp Then
                        Return True
                    End If
                Next
            Else
                For Each ItemPax As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In vListaResDetPax
                    If ItemPax.IDReserva_Det = vintIDReservas_Det And ItemPax.IDPax = vintIDPax Then
                        Return True
                    End If
                Next
            End If
            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function blnExisteAcomodoenLista(ByVal vListaResDetPax As List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE), _
                                             ByVal vintIDServicio_Det As Integer, _
                                              ByVal vdatDia As Date, _
                                              ByVal vbytCapacidadHab As Byte, _
                                              ByVal vblnEsMatrim As Boolean, _
                                              ByVal vintIDPax As Integer) As Boolean
        Try
            For Each Item As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In vListaResDetPax
                'If Item.CapacidadHab = vbytCapacidadHab And _
                'Item.EsMatrimonial = vblnEsMatrim And Format(Item.Dia, "dd/MM/yyyy") = Format(vdatDia, "dd/MM/yyyy") And _
                'Item.IDServicio_Det = vintIDServicio_Det And _
                'Item.IDPax = vintIDPax.ToString Then
                '    Return True
                'End If

                If Item.IDPax = vintIDPax.ToString Then
                    Return True
                End If
            Next
            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function ListIDServicio_DetPerRailLista(ByVal vintIDDet As Integer, ByVal ListaVuelos As List(Of clsCotizacionBE.clsCotizacionVuelosBE)) As List(Of Integer)
        Try
            Dim Lista As New List(Of Integer)
            If ListaVuelos Is Nothing Then Return Lista

            For Each Item As clsCotizacionBE.clsCotizacionVuelosBE In ListaVuelos
                If Item.IDDet = vintIDDet Then
                    Lista.Add(Item.IDServicio_DetPeruRail)
                End If
            Next

            Return Lista
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub ActualizarProveedoresxPeruRail(ByRef BECot As clsCotizacionBE)
        Try
            Dim ItemNuevo As clsCotizacionBE.clsDetalleCotizacionBE = Nothing
InicioFor:

            If Not ItemNuevo Is Nothing Then
                BECot.ListaDetalleCotiz.Add(ItemNuevo)
            End If
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                If Item.IDTipoProv = gstrTipoProvOper And Item.IDProveedor <> gstrIDProveedorPeruRail Then
                    'Dim dttCotidet As DataTable = dttPeruRailRelacionado(Item.IDDet)
                    'If dttCotidet.Rows.Count > 0 Then
                    'Dim intIDServicio_DetPerRail As Integer = ListIDServicio_DetPerRailLista(Item.IDDet, BECot.ListaCotizacionVuelo)
                    Dim ListaIDServicio_DetPerRail As List(Of Integer) = ListIDServicio_DetPerRailLista(Item.IDDet, BECot.ListaCotizacionVuelo)

                    For Each intIDServicio_DetPerRail As Integer In ListaIDServicio_DetPerRail
                        Dim objDetServ As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
                        Dim dtt As DataTable = objDetServ.ConsultarPk(intIDServicio_DetPerRail) 'dttCotidet(0)("IDServicio_Det"))
                        If dtt.Rows.Count > 0 Then


                            ItemNuevo = New clsCotizacionBE.clsDetalleCotizacionBE
                            ItemNuevo = Item

                            BECot.ListaDetalleCotiz.Remove(Item)
                            ItemNuevo.IDProveedor = gstrIDProveedorPeruRail

                            GoTo InicioFor
                        End If
                    Next

                    'End If
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function blnReservaRegeneradoxAcomodo(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@pRegeneradoxAcomodo", 0)

            Return objADO.ExecuteSPOutput("RESERVAS_SelRegeneradoxAcomodoOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnReservaRegeneradoxAcomodo")

        End Try
    End Function

    Private Function blnExisteAlojamiento(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExiste As Boolean = False
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pExisteAlojamiento", False)

            blnExiste = objADO.GetSPOutputValue("COTIDET_SelExisteAlojamiento", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteAlojamientoReservas(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExiste As Boolean = False
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pExisteAlojamiento", False)

            blnExiste = objADO.GetSPOutputValue("RESERVAS_DET_SelExisteAlojamiento", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub CambiosPreGeneracionxFile(ByVal BECot As clsCotizacionBE, _
                                         ByRef rListaProveed As List(Of stProveedoresReservas), _
                                         Optional ByVal vListaProveedCot As List(Of stProveedoresReservas) = Nothing)
        Try
            Dim ListaProveed As New List(Of stProveedoresReservas)
            Dim ListaDetRes As New List(Of clsReservaBE.clsDetalleReservaBE)

            Dim objBERes As clsReservaBE
            ActualizarProveedoresxPeruRail(BECot)

            'For Each iTEM As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
            '    If iTEM.IDDet = 457699 Then
            '        Dim N As Integer = 0
            '    End If
            'Next

            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                Dim blnExisteProvee As Boolean = False
                For Each STReserva As stProveedoresReservas In ListaProveed
                    If STReserva.IDProveedor = Item.IDProveedor Then
                        blnExisteProvee = True
                        Exit For
                    End If
                Next

                ''PPMG20160330
                If Item.IDProveedor = strIDProveedorZicasso Then
                    blnExisteProvee = True
                End If

                If Not blnExisteProvee Then
                    objBERes = New clsReservaBE
                    With objBERes
                        .IDProveedor = Item.IDProveedor
                        .IDCab = BECot.IdCab
                        .Estado = "NV"
                        .EstadoSolicitud = "RECR"
                        .IDUsuarioVen = BECot.IDUsuario
                        .IDUsuarioRes = "0000"
                        .Observaciones = ""
                        .CodReservaProv = ""
                        .IDFile = BECot.IDFile

                        If vListaProveedCot Is Nothing Then
                            .RegeneradoxAcomodo = BECot.RegeneradoxAcomodo
                        Else
                            For Each ItemProvCot As stProveedoresReservas In vListaProveedCot
                                If ItemProvCot.IDProveedor = .IDProveedor Then
                                    .RegeneradoxAcomodo = ItemProvCot.RegeneradoxAcomodo
                                    Exit For
                                End If
                            Next
                        End If
                        .UserMod = BECot.UserMod
                    End With


                    Dim objReserva As New stProveedoresReservas
                    objReserva.IDReserva = Insertar(objBERes) 'Insertar Tabla RESERVAS
                    objReserva.IDProveedor = Item.IDProveedor
                    objReserva.EsAlojamiento = blnExisteAlojamiento(Item.IDProveedor, BECot.IdCab)
                    ListaProveed.Add(objReserva)

                    'Acomodos
                    Dim objBN As New clsReservaBN.clsAcomodoPaxBN
                    Dim dcAco As New Dictionary(Of String, String)
                    dcAco.Add("@IDCAB", BECot.IdCab)
                    Dim dtt As DataTable = objADO.GetDataTable("ACOMODOPAX_FILE_Sel_List", dcAco)
                    Dim ListaAcomodoProveedor As New List(Of clsReservaBE.clsAcomodoPaxProveedorBE)

                    Dim objBEAco As clsReservaBE.clsAcomodoPaxProveedorBE
                    For Each DrowA As DataRow In dtt.Rows
                        objBEAco = New clsReservaBE.clsAcomodoPaxProveedorBE
                        With objBEAco
                            .IDReserva = objReserva.IDReserva
                            .IDPax = DrowA("IDPax")
                            .IdHabit = DrowA("IDHabit")
                            .CapacidadHabit = DrowA("CapacidadHab")
                            .EsMatrimonial = DrowA("EsMatrimonial")
                            .UserMod = BECot.UserMod
                            .Accion = "N"
                        End With
                        ListaAcomodoProveedor.Add(objBEAco)
                    Next

                    'Tareas Dead Line
                    Dim objTareasBE As New clsReservaBE.clsTareasReservaBE
                    Dim datMenor As Date = datFechaMenorProveedorDetCotizacion(BECot, Item.IDProveedor)
                    Dim ListaUltimoMinuto As New List(Of Char)
                    Dim ListaTareasRes As New List(Of clsReservaBE.clsTareasReservaBE)
                    Dim dc As Dictionary(Of String, Date) = dicDevuelveFechasDeadLine(Item.IDProveedor, _
                                datMenor, BECot.NroPax + BECot.NroLiberados, ListaUltimoMinuto)
                    For Each dic As KeyValuePair(Of String, Date) In dc
                        objTareasBE = New clsReservaBE.clsTareasReservaBE
                        Select Case dic.Key
                            Case "FecDeadLRoom1"
                                objTareasBE.IDTarea = 1
                            Case "FecDeadLRoom2"
                                objTareasBE.IDTarea = 2
                            Case "FecDeadLRoom3"
                                objTareasBE.IDTarea = 3
                            Case "FecDeadLPrepag1"
                                objTareasBE.IDTarea = 4
                            Case "FecDeadLPrepag2"
                                objTareasBE.IDTarea = 5
                            Case "FecDeadLSaldo"
                                objTareasBE.IDTarea = 6
                            Case "FecDeadSinPenalidad"
                                objTareasBE.IDTarea = 7
                        End Select
                        objTareasBE.FecDeadLine = dic.Value
                        objTareasBE.IDEstado = "PD"
                        objTareasBE.IDCab = BECot.IdCab
                        objTareasBE.Accion = "N"
                        objTareasBE.UltimoMinuto = False
                        If objTareasBE.FecDeadLine <> "01/01/1900" Then
                            ListaTareasRes.Add(objTareasBE)
                        End If
                        objTareasBE.Descripcion = strDevuelveDescripcionTareaPolitica(objTareasBE.IDTarea)
                    Next

                    objBERes.ListaUltimoMinuto = ListaUltimoMinuto
                    objBERes.ListaTareasReservas = ListaTareasRes
                    objBERes.ListaAcomodPaxReservasProveedor = ListaAcomodoProveedor

                    Dim objTareasBT As New clsReservaBT.clsTareasReservaBT
                    objTareasBT.InsertarEliminar(objBERes)

                    Dim objAcomodo As New clsReservaBT.clsAcomodoPaxProveedorBT
                    objAcomodo.InsertarEliminar(objBERes)

                End If

            Next
            'Dim ListProveeOrd = (From Item In ListaProveed _
            '                     Order By Convert.ToInt64(Item.IDProveedor) Ascending).ToList()

            rListaProveed = ListaProveed
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarCapacidadHabitacionOperConAlojamiento(ByVal BECot As clsCotizacionBE, ByVal vblnResidente As Boolean, _
            ByVal ItemDetRes As clsReservaBE.clsDetalleReservaBE, _
            ByRef rbytCantidad As Byte, ByRef rbytCapacidadHab As Byte, ByRef rstrDescServicio As String)

        Try
            Dim bytCantidad As Byte = 0, bytCapacidadHab As Byte = 0, strDescServicio As String = ""

            Dim bytCapacMax As Int16
            Dim bytSimple As Int16 = If(vblnResidente = True, BECot.SimpleResidente, BECot.Simple)
            Dim bytTwin As Int16 = If(vblnResidente = True, BECot.TwinResidente, BECot.Twin)
            Dim bytMatrimonial As Int16 = If(vblnResidente = True, BECot.MatrimonialResidente, BECot.Matrimonial)
            Dim bytTriple As Int16 = If(vblnResidente = True, BECot.TripleResidente, BECot.Triple)
            Dim intNroPax As Int16 = ItemDetRes.NroPax + ItemDetRes.NroLiberados
            If vblnResidente Then
                intNroPax -= (BECot.Simple + (BECot.Twin * 2) + (BECot.Matrimonial * 2) + (BECot.Triple * 3))
            End If
            If bytTriple <> 0 Then
                bytCapacMax = 3
            ElseIf bytTwin <> 0 Or bytMatrimonial <> 0 Then
                bytCapacMax = 2
            ElseIf bytSimple <> 0 Then
                bytCapacMax = 1
            End If

            If bytSimple + bytTwin + bytMatrimonial + bytTriple = 0 Then

                GoTo Fin
            End If

            Dim intContHab As Int16 = intNroPax

            If bytCapacMax = 0 Then bytCapacMax = 1
            Dim blnMatrim As Boolean = False
            If bytCapacMax = 3 And bytTriple > 0 Then
                bytCantidad = bytTriple
                bytCapacidadHab = 3
            ElseIf bytCapacMax = 2 And bytTwin > 0 Then
                bytCantidad = bytTwin
                bytCapacidadHab = 2
            ElseIf bytCapacMax = 2 And bytMatrimonial > 0 Then
                bytCantidad = bytMatrimonial
                bytCapacidadHab = 2
                blnMatrim = True
            ElseIf bytCapacMax = 1 And bytSimple > 0 Then
                bytCantidad = bytSimple
                bytCapacidadHab = 1
            End If

            If bytCantidad * bytCapacidadHab > intNroPax Then
                bytCantidad = intNroPax / bytCapacidadHab
            End If

            If bytCapacMax <= 0 Then bytCapacMax = 1
            strDescServicio = ItemDetRes.DescServicio & " - " & strDescCapacidadHabitacionOperConAlojamiento(bytCapacMax, blnMatrim, ItemDetRes.DetaTipo)

            If ItemDetRes.IncGuia = True Then
                If vblnResidente Then Exit Sub
                bytCantidad = 1
                bytCapacidadHab = 1
                strDescServicio = ItemDetRes.DescServicio & " - " & strDescCapacidadHabitacionOperConAlojamiento(bytCantidad, blnMatrim, ItemDetRes.DetaTipo)
            End If

Fin:
            rbytCantidad = bytCantidad
            rbytCapacidadHab = bytCapacidadHab
            rstrDescServicio = strDescServicio
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub CambiosPostGeneracionxFile(ByVal BECot As clsCotizacionBE, _
                                          ByVal vListaProveed As List(Of stProveedoresReservas), _
                                          Optional ByVal vintIDDet As Integer = 0, _
                                          Optional ByVal vListaIDDet As List(Of Integer) = Nothing, _
                                          Optional ByVal vblnIDReservaCambiosLog As Boolean = False, _
                                          Optional ByVal vchrAccionLog As Char = "", _
                                          Optional ByVal vblnTemp As Boolean = False)
        Try
            Dim ListaDetRes As New List(Of clsReservaBE.clsDetalleReservaBE)
            For Each ItemReserva As stProveedoresReservas In vListaProveed

                If blnExistenReservasDetIngManuales(ItemReserva.IDReserva) Then
                    PasarDatosPropiedadesCompletosReservasDetporIngManual(ListaDetRes, ItemReserva.IDReserva, BECot.UserMod, BECot.IdCab, vblnTemp)
                End If

                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                    If Item.IDProveedor = ItemReserva.IDProveedor And _
                        Not (Item.IDTipoProv = gstrTipoProvHotel And Item.PlanAlimenticio = False) And _
                        Not (Item.IDTipoProv <> gstrTipoProvHotel And Item.ConAlojamiento = True) And _
                            Not Item.FlServicioParaGuia Then
                        'If Item.IDProveedor = "000637" Then
                        '    Item.IDProveedor = Item.IDProveedor
                        'End If
                        'If Item.IDDet = "623590" Then
                        '    Item.IDDet = "623590"
                        'End If

                        Dim ListaDetServPerRail As List(Of Integer) = ListIDServicio_DetPerRailLista(Item.IDDet, BECot.ListaCotizacionVuelo)
                        ' ''If ListaDetServPerRail.Count > 0 And Item.TipoTransporte.ToString.Trim = String.Empty Then
                        ' ''    For Each intIDServicio_DetPerRail As Integer In ListaDetServPerRail
                        ' ''        Dim objBEDetRes2 As New clsReservaBE.clsDetalleReservaBE
                        ' ''        Item.IDIdioma = "NO APLICA"
                        ' ''        PasarDatosPropiedadesaDetReservasBE(objBEDetRes2, Item, "", BECot.ListaDetalleCotiz, _
                        ' ''                                            BECot.ListaCotizacionVuelo, intIDServicio_DetPerRail, "N")
                        ' ''        'If(vintIDDet = 0, False, True))
                        ' ''        objBEDetRes2.CapacidadHab = 0
                        ' ''        objBEDetRes2.Cantidad = 0
                        ' ''        objBEDetRes2.EsMatrimonial = False
                        ' ''        objBEDetRes2.IDReserva = ItemReserva.IDReserva
                        ' ''        objBEDetRes2.IDFile = BECot.IDFile

                        ' ''        If objBEDetRes2.IncGuia Then
                        ' ''            objBEDetRes2.NroPax = 1
                        ' ''            objBEDetRes2.NroLiberados = 0
                        ' ''            objBEDetRes2.Cantidad = 1
                        ' ''            objBEDetRes2.CantidadAPagar = 1
                        ' ''        End If

                        ' ''        ListaDetRes.Add(objBEDetRes2)

                        ' ''        PasarDatosPropiedadesaCostosDetReservasBE(objBEDetRes2, 0, 0, 0, 0, 0, 0, 0, 0, 0, BECot.TipoCambio, Nothing)
                        ' ''    Next
                        ' ''End If
                        If ListaDetServPerRail.Count = 0 Then
                            Dim objBEDetRes As New clsReservaBE.clsDetalleReservaBE
                            If vblnIDReservaCambiosLog Then
                                Dim objDetCotiBT As New clsDetalleVentasCotizacionBT
                                Dim dttCotiDet As DataTable = objDetCotiBT.ConsultarListxIDCab(BECot.IdCab, True, False, True)
                                Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
                                PasarDatosListaCotiDetRegeneracion("", BECot.IdCab, Item.IDReserva, _
                                                                   BECot.UserMod, dttCotiDet, ListaCotiDet)


                                PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, "", ListaCotiDet, BECot.ListaCotizacionVuelo, 0, vchrAccionLog)
                            Else

                                PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, "", BECot.ListaDetalleCotiz, _
                                                                    BECot.ListaCotizacionVuelo, 0, "N")
                            End If

                            If objBEDetRes.ConAlojamiento = True And objBEDetRes.Dias > 1 Then
                                Dim bytCantidadSum As Byte = 0
                                Dim bytCapacidadSum As Byte = 0
                                Dim strDescServicio As String = ""
                                CargarCapacidadHabitacionOperConAlojamiento(BECot, True, objBEDetRes, objBEDetRes.Cantidad, objBEDetRes.CapacidadHab, strDescServicio)
                                bytCantidadSum = objBEDetRes.Cantidad
                                bytCapacidadSum = objBEDetRes.CapacidadHab
                                CargarCapacidadHabitacionOperConAlojamiento(BECot, False, objBEDetRes, objBEDetRes.Cantidad, objBEDetRes.CapacidadHab, strDescServicio)
                                bytCantidadSum += objBEDetRes.Cantidad
                                bytCapacidadSum += objBEDetRes.CapacidadHab

                                objBEDetRes.CapacidadHab = bytCapacidadSum
                                objBEDetRes.Cantidad = bytCantidadSum
                                objBEDetRes.Servicio = strDescServicio
                            Else
                                objBEDetRes.CapacidadHab = 0
                                objBEDetRes.Cantidad = 0
                                objBEDetRes.EsMatrimonial = False
                            End If

                            If objBEDetRes.IncGuia Then
                                objBEDetRes.NroPax = 1
                                objBEDetRes.NroLiberados = 0
                                objBEDetRes.Cantidad = 1
                                objBEDetRes.CantidadAPagar = 1
                            End If

                            objBEDetRes.IDReserva = ItemReserva.IDReserva
                            objBEDetRes.IDFile = BECot.IDFile

                            ListaDetRes.Add(objBEDetRes)


                            PasarDatosPropiedadesaCostosDetReservasBE(objBEDetRes, _
                                                                      0, 0, 0, 0, 0, 0, 0, 0, 0, BECot.TipoCambio, Nothing)
                        Else
                            For Each intIDServicio_DetPerRail As Integer In ListaDetServPerRail
                                Dim objBEDetRes As New clsReservaBE.clsDetalleReservaBE
                                Item.IDIdioma = "NO APLICA"
                                PasarDatosPropiedadesaDetReservasBE(objBEDetRes, Item, "", BECot.ListaDetalleCotiz, _
                                                                    BECot.ListaCotizacionVuelo, intIDServicio_DetPerRail, "N")
                                'If(vintIDDet = 0, False, True))
                                objBEDetRes.CapacidadHab = 0
                                objBEDetRes.Cantidad = 0
                                objBEDetRes.EsMatrimonial = False
                                objBEDetRes.IDReserva = ItemReserva.IDReserva
                                objBEDetRes.IDFile = BECot.IDFile

                                If objBEDetRes.IncGuia Then
                                    objBEDetRes.NroPax = 1
                                    objBEDetRes.NroLiberados = 0
                                    objBEDetRes.Cantidad = 1
                                    objBEDetRes.CantidadAPagar = 1
                                End If

                                ListaDetRes.Add(objBEDetRes)

                                PasarDatosPropiedadesaCostosDetReservasBE(objBEDetRes, 0, 0, 0, 0, 0, 0, 0, 0, 0, BECot.TipoCambio, Nothing)
                            Next
                        End If
                    End If
                Next
            Next
            LiberarHabitacionesPorAcomodosIngresados(ListaDetRes, True)

            Dim objBTDetRes As New clsReservaBT.clsDetalleReservaBT
            Dim objBERes As New clsReservaBE
            objBERes.ListaDetReservas = ListaDetRes

            If Not vblnTemp Then
                objBTDetRes.InsertarActualizarEliminar(objBERes, 0)
                ActualizarDatosCotizacion(BECot.IdCab, BECot.IDFile, _
                          If(BECot.IDUsuarioRes Is Nothing, "0000", BECot.IDUsuarioRes), BECot.Estado, BECot.UserMod)
            Else
                objBERes.Accion = "N"
                objBTDetRes.InsertarActualizarEliminarTMP(objBERes, 0)
            End If


            'ActualizarEliminarDetReservasHotelesDiasSeguidos(BECot.IdCab, BECot.UserMod)

            'If vintIDDet = 0 Then
            If Not vblnTemp Then
                For Each ItemReserva As stProveedoresReservas In vListaProveed
                    ActualizarEliminarDetReservasHotelesDiasSeguidosxReserva(ItemReserva.IDReserva, ItemReserva.EsAlojamiento, BECot.UserMod)
                Next
            Else
                For Each ItemReserva As stProveedoresReservas In vListaProveed
                    ActualizarEliminarDetReservasHotelesDiasSeguidosxReservaTMP(ItemReserva.IDReserva, BECot.UserMod)
                Next
            End If
            'End If

            If BECot.NroLiberados > 0 Then
                Dim bytCapacidadHab As Byte
                If BECot.Tipo_Lib = "S" Then
                    bytCapacidadHab = 1
                ElseIf BECot.Tipo_Lib = "D" Then
                    bytCapacidadHab = 2
                End If


                'objBTDetRes.ActualizarNroLiberados(BECot.IdCab, bytCapacidadHab, BECot.NroLiberados, BECot.UserMod)
                If Not vblnTemp Then
                    For Each ItemReserva As stProveedoresReservas In vListaProveed
                        objBTDetRes.ActualizarNroLiberadosxReserva(ItemReserva.IDReserva, bytCapacidadHab, BECot.NroLiberados, BECot.UserMod)
                    Next

                Else
                    For Each ItemReserva As stProveedoresReservas In vListaProveed
                        objBTDetRes.ActualizarNroLiberadosxReservaTMP(ItemReserva.IDReserva, bytCapacidadHab, BECot.NroLiberados, BECot.UserMod)
                    Next

                End If

            End If

            If Not vblnTemp Then
                objBTDetRes.ActualizarTipoLiberados(BECot.IdCab, BECot.UserMod)
            Else
                objBTDetRes.ActualizarTipoLiberadosTMP(BECot.IdCab, BECot.UserMod)
            End If

            If Not vblnTemp Then
                Dim objDetCotBT As New clsDetalleVentasCotizacionBT
                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                    Dim objBTDetPaxRes As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT
                    Try
                        MultiplicarxCantidadMontosDetallexIDCab(BECot.IdCab, Item.IDDet, BECot.UserMod)
                    Catch ex As Exception
                        Throw
                    End Try

                    If vintIDDet = 0 Then
                        objDetCotBT.InsertarLog1eraVez(Item.IDDet)
                    End If
                Next

                Dim objResDetServ As New clsReservaBT.clsDetalleReservaBT.clsDetalleReservaDetServiciosBT
                Dim ListaProveedProcesados As New List(Of stProveedoresReservas)

                For Each ItemReserva As stProveedoresReservas In vListaProveed
                    If ItemReserva.EsAlojamiento Or (ItemReserva.IDProveedor = gstrIDProveedorSetoursLima Or ItemReserva.IDProveedor = gstrIDProveedorSetoursCusco Or _
                                                         ItemReserva.IDProveedor = gstrIDProveedorSetoursBuenosAires Or ItemReserva.IDProveedor = gstrIDProveedorSetoursSantiago) Then
                        If vintIDDet = 0 Then
                            objResDetServ.GrabarxReserva(BECot.IdCab, ItemReserva.IDReserva, BECot.UserMod)
                        ElseIf vintIDDet = -1 Then
                            For Each intIDDet As Integer In vListaIDDet
                                objResDetServ.GrabarxIDDet(BECot.IdCab, intIDDet, ItemReserva.IDReserva, BECot.UserMod)
                            Next
                        Else
                            objResDetServ.GrabarxIDDet(BECot.IdCab, vintIDDet, ItemReserva.IDReserva, BECot.UserMod)
                        End If

                        If ItemReserva.IDProveedor = gstrIDProveedorSetoursLima Then
                            If BECot.FlTourConductor Then
                                objResDetServ.GrabarxIDReservaxIDCabViatico(BECot.IdCab, ItemReserva.IDReserva, BECot.UserMod)
                            End If
                        End If
                    Else
                        objResDetServ.GrabarxServicio(BECot.IdCab, ItemReserva.IDReserva, BECot.UserMod)
                    End If
                Next
            End If
            'Acomodo Vehiculos

            For Each ItemReserva As stProveedoresReservas In vListaProveed
                InsertarAcomodosVehiculosInt(ItemReserva.IDReserva, vblnTemp, BECot.UserMod)
                InsertarAcomodosVehiculosExt(ItemReserva.IDReserva, vblnTemp, BECot.UserMod)
            Next

            InsertarEliminarSubServiciosFileAcomVehi(BECot.IdCab, vblnTemp, BECot.UserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub InsertarEliminarSubServiciosFileAcomVehi(ByVal vintIDCab As Integer, ByVal vblnTemp As Boolean, ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            Dim dtt As DataTable = objADO.GetDataTable("COTIDET_SelProvAcomVehiInt", dc)
            For Each dr As DataRow In dtt.Rows

                InsertarEliminarSubServiciosProveedorAcomVehi(vintIDCab, dr("IDProveedor"), vstrUserMod)
                ActualizarCostosServiciosAcomVehi(vintIDCab, dr("IDProveedor"), vblnTemp, vstrUserMod)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub
    Private Sub InsertarEliminarSubServiciosProveedorAcomVehi(ByVal vintIDCab As Integer, ByVal vstrCoProveedor As String, ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("RESERVAS_DETSERVICIOS_Del_CostosGuiasVehiculos", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub ActualizarCostosServiciosAcomVehi(ByVal vintIDCab As Integer, ByVal vstrCoProveedor As String, ByVal vblnTemp As Boolean, ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@UserMod", vstrUserMod)
            If Not vblnTemp Then
                objADO.ExecuteSP("RESERVAS_DET_Upd_CostosAcomVehi", dc)
            Else
                objADO.ExecuteSP("RESERVAS_DET_TMP_Upd_CostosAcomVehi", dc)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub InsertarAcomodosVehiculosInt(ByVal vintIDReserva As Integer, ByVal vblnTemp As Boolean, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@TablaTmp_M", vblnTemp)
            dc.Add("@UserMod", vstrUserMod)

            Dim dtt As DataTable = objADO.GetDataTable("RESERVAS_DET_InsAcomodoVehicInt", dc)

            If Not vblnTemp Then
                For Each dr As DataRow In dtt.Rows
                    InsertarSubServReservasAcomodosVehicInt(dr("IDReserva_Det"), dr("IDReserva_DetNew"), vstrUserMod)
                Next

                InsertarPaxReservasAcomodosVehicInt(vintIDReserva, vstrUserMod)
            End If



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarAcomodosVehiculosInt")
        End Try
    End Sub
    Private Sub InsertarSubServReservasAcomodosVehicInt(ByVal vintIDReserva_Det As Integer, ByVal vintIDReserva_DetNew As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc2 As New Dictionary(Of String, String)
            dc2.Add("@IDReserva_Det", vintIDReserva_Det)
            dc2.Add("@IDReserva_DetNew", vintIDReserva_DetNew)
            dc2.Add("@UserMod", vstrUserMod)

            'objADO.ExecuteSP("RESERVAS_DET_PAX_InsxReserva_DetAcomVehiInt", dc2)
            objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxReserva_DetAcomVehi", dc2)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarDetallesReservasAcomodosVehicInt")
        End Try
    End Sub
    Private Sub InsertarPaxReservasAcomodosVehicInt(ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc2 As New Dictionary(Of String, String)
            dc2.Add("@IDReserva", vintIDReserva)
            dc2.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_DET_PAX_InsxReserva_DetAcomVehiInt", dc2)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarDetallesReservasAcomodosVehicInt")
        End Try
    End Sub
    Private Sub InsertarDetallesReservasAcomodosVehicExt(ByVal vintIDReserva_Det As Integer, ByVal vintIDReserva_DetNew As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc2 As New Dictionary(Of String, String)
            dc2.Add("@IDReserva_Det", vintIDReserva_Det)
            dc2.Add("@IDReserva_DetNew", vintIDReserva_DetNew)
            dc2.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_DET_PAX_InsxReserva_DetAcomVehiExt", dc2)
            objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxReserva_DetAcomVehi", dc2)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarDetallesReservasAcomodosVehicExt")
        End Try
    End Sub

    Private Sub InsertarAcomodosVehiculosExt(ByVal vintIDReserva As Integer, ByVal vblnTemp As Boolean, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@TablaTmp_M", vblnTemp)
            dc.Add("@UserMod", vstrUserMod)

            Dim dtt As DataTable = objADO.GetDataTable("RESERVAS_DET_InsAcomodoVehicExt", dc)

            If Not vblnTemp Then
                For Each dr As DataRow In dtt.Rows
                    InsertarDetallesReservasAcomodosVehicExt(dr("IDReserva_Det"), dr("IDReserva_DetNew"), vstrUserMod)
                Next
            End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarAcomodosVehiculosExt")
        End Try
    End Sub


    Private Function blnExistenReservasDetIngManuales(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pExiste", False)
            Dim blnExiste As Boolean = objADO.GetSPOutputValue("RESERVAS_SelExistsIngresoManual", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub PasarDatosPropiedadesCompletosReservasDetporIngManual(ByRef rListReservaDet As List(Of clsReservaBE.clsDetalleReservaBE), _
                                                                      ByVal vintIDReserva As Integer, ByVal vstrUserMod As String, _
                                                                      ByVal vintIDCab As Integer, ByVal vblnTemp As Boolean)
        Try
            Dim objReservasDet As clsReservaBE.clsDetalleReservaBE = Nothing
            'If Not vblnTemp Then
            '    Dim dtDatResvDetViaticos As DataTable = dtRetornarReservasDetIngresadasViaticos(vintIDCab)
            '    For Each item As DataRow In dtDatResvDetViaticos.Rows
            '        objReservasDet = New clsReservaBE.clsDetalleReservaBE
            '        With objReservasDet
            '            .IDReserva_Det = item("IDReserva_Det")
            '            .IDReserva = item("IDReserva")
            '            .IDFile = item("IDFile")
            '            .IDDet = 0
            '            .IDTipoProv = item("IDTipoProv")
            '            .Item = item("Item")
            '            .Dia = item("Dia")
            '            .FechaOut = If(IsDBNull(item("FechaOut")) = True, #1/1/1900#, Convert.ToDateTime(item("FechaOut")))
            '            .Noches = If(IsDBNull(item("Noches")) = True, 0, Convert.ToByte(item("Noches")))
            '            .IDUbigeo = item("IDubigeo")
            '            .IDServicio = item("IDServicio")
            '            .IDServicio_Det = item("IDServicio_Det")
            '            .IDIdioma = item("IDIdioma")
            '            .Especial = item("Especial")
            '            .MotivoEspecial = If(IsDBNull(item("MotivoEspecial")), "", item("MotivoEspecial"))
            '            .RutaDocSustento = If(IsDBNull(item("RutaDocSustento")), "", item("RutaDocSustento"))
            '            .Desayuno = item("Desayuno")
            '            .Lonche = item("Lonche")
            '            .Almuerzo = item("Almuerzo")
            '            .Cena = item("Cena")
            '            .Transfer = item("Transfer")
            '            .IDDetTransferOri = If(IsDBNull(item("IDDetTransferOri")) = True, 0, item("IDDetTransferOri"))
            '            .IDDetTransferDes = If(IsDBNull(item("IDDetTransferDes")) = True, 0, item("IDDetTransferDes"))
            '            .TipoTransporte = If(IsDBNull(item("TipoTransporte")) = True, "", item("TipoTransporte"))
            '            .IDUbigeoOri = item("IDUbigeoOri")
            '            .IDUbigeoDes = item("IDUbigeoDes")
            '            .NroPax = item("NroPax")
            '            .NroLiberados = If(IsDBNull(item("NroLiberados")) = True, 0, item("NroLiberados"))
            '            .Tipo_Lib = If(IsDBNull(item("Tipo_Lib")) = True, "", item("Tipo_Lib"))
            '            .Cantidad = item("Cantidad")
            '            .CantidadAPagar = item("CantidadAPagar")
            '            .CostoReal = item("CostoReal")
            '            .CostoRealAnt = item("CostoRealAnt")
            '            .CostoLiberado = item("CostoLiberado")
            '            .Margen = item("Margen")
            '            .MargenAplicado = item("MargenAplicado")
            '            .MargenAplicadoAnt = item("MargenAplicadoAnt")
            '            .MargenLiberado = item("MargenLiberado")
            '            .Total = item("Total")
            '            .TotalOrig = item("TotalOrig")
            '            .CostoRealImpto = item("CostoRealImpto")
            '            .CostoLiberadoImpto = item("CostoLiberadoImpto")
            '            .MargenImpto = item("MargenImpto")
            '            .MargenLiberadoImpto = item("MargenLiberadoImpto")
            '            .TotImpto = item("TotImpto")
            '            .NetoHab = .CostoReal
            '            .IgvHab = 0
            '            .TotalHab = .CostoReal
            '            .NetoGen = .CostoReal * .CantidadAPagar
            '            .IgvGen = 0
            '            .TotalGen = .CostoReal * .CantidadAPagar
            '            .IDDetRel = If(IsDBNull(item("IDDetRel")) = True, "0", item("IDDetRel"))
            '            .Servicio = If(IsDBNull(item("Servicio")) = True, "", item("Servicio"))
            '            .IDReserva_DetCopia = If(IsDBNull(item("IDReserva_DetCopia")) = True, 0, item("IDReserva_DetCopia"))
            '            .IDReserva_Det_Rel = If(IsDBNull(item("IDReserva_Det_Rel")) = True, 0, item("IDReserva_Det_Rel"))
            '            .IDEmailRef = "" 'If(IsDBNull(item("IDEmailRef")) = True, "", item("IDEmailRef"))
            '            .IDEmailEdit = "" 'If(IsDBNull(item("IDEmailEdit")) = True, "", item("IDEmailEdit"))
            '            .IDEmailNew = "" 'If(IsDBNull(item("IDEmailNew")) = True, "", item("IDEmailNew"))
            '            .CapacidadHab = If(IsDBNull(item("CapacidadHab")) = True, 0, item("CapacidadHab"))
            '            .EsMatrimonial = If(IsDBNull(item("EsMatrimonial")) = True, False, item("EsMatrimonial"))
            '            .UserMod = vstrUserMod
            '            .IDMoneda = If(IsDBNull(item("IDMoneda")) = True, "", item("IDMoneda"))
            '            .IDGuiaProveedor = If(IsDBNull(item("IDGuiaProveedor")) = True, "", item("IDGuiaProveedor"))
            '            .NuVehiculo = If(IsDBNull(item("NuVehiculo")) = True, 0, item("NuVehiculo"))
            '            .ExecTrigger = item("ExecTrigger")
            '            .FlServicioParaGuia = Convert.ToBoolean(item("FlServicioParaGuia"))
            '            .FlServicioNoShow = Convert.ToBoolean(item("FlServicioNoShow"))
            '            .FlServicioIngManual = Convert.ToBoolean(item("FlServicioIngManual"))
            '            .NuViaticosTC = item("NuViaticoTC")
            '            If .NuViaticosTC > 0 Then .FlServicioIngManual = True
            '            .Accion = "N"
            '        End With
            '        rListReservaDet.Add(objReservasDet)
            '    Next
            'End If

            Dim dtDatResvDet As DataTable = dtRetornarReservasDetIngresadasManualmente(vintIDReserva)
            For Each item As DataRow In dtDatResvDet.Rows
                objReservasDet = New clsReservaBE.clsDetalleReservaBE
                With objReservasDet
                    .IDReserva_Det = item("IDReserva_Det")
                    .IDReserva = item("IDReserva")
                    .IDFile = item("IDFile")
                    .IDDet = 0
                    .IDTipoProv = item("IDTipoProv")
                    .Item = item("Item")
                    .Dia = item("Dia")
                    .FechaOut = If(IsDBNull(item("FechaOut")) = True, #1/1/1900#, Convert.ToDateTime(item("FechaOut")))
                    .Noches = If(IsDBNull(item("Noches")) = True, 0, Convert.ToByte(item("Noches")))
                    .IDUbigeo = item("IDubigeo")
                    .IDServicio = item("IDServicio")
                    .IDServicio_Det = item("IDServicio_Det")
                    .IDIdioma = item("IDIdioma")
                    .Especial = item("Especial")
                    .MotivoEspecial = If(IsDBNull(item("MotivoEspecial")), "", item("MotivoEspecial"))
                    .RutaDocSustento = If(IsDBNull(item("RutaDocSustento")), "", item("RutaDocSustento"))
                    .Desayuno = item("Desayuno")
                    .Lonche = item("Lonche")
                    .Almuerzo = item("Almuerzo")
                    .Cena = item("Cena")
                    .Transfer = item("Transfer")
                    .IDDetTransferOri = If(IsDBNull(item("IDDetTransferOri")) = True, 0, item("IDDetTransferOri"))
                    .IDDetTransferDes = If(IsDBNull(item("IDDetTransferDes")) = True, 0, item("IDDetTransferDes"))
                    .TipoTransporte = If(IsDBNull(item("TipoTransporte")) = True, "", item("TipoTransporte"))
                    .IDUbigeoOri = item("IDUbigeoOri")
                    .IDUbigeoDes = item("IDUbigeoDes")
                    .NroPax = item("NroPax")
                    .NroLiberados = If(IsDBNull(item("NroLiberados")) = True, 0, item("NroLiberados"))
                    .Tipo_Lib = If(IsDBNull(item("Tipo_Lib")) = True, "", item("Tipo_Lib"))
                    .Cantidad = item("Cantidad")
                    .CantidadAPagar = item("CantidadAPagar")
                    .CostoReal = item("CostoReal")
                    .CostoRealAnt = item("CostoRealAnt")
                    .CostoLiberado = item("CostoLiberado")
                    .Margen = item("Margen")
                    .MargenAplicado = item("MargenAplicado")
                    .MargenAplicadoAnt = item("MargenAplicadoAnt")
                    .MargenLiberado = item("MargenLiberado")
                    .Total = item("Total")
                    .TotalOrig = item("TotalOrig")
                    .CostoRealImpto = item("CostoRealImpto")
                    .CostoLiberadoImpto = item("CostoLiberadoImpto")
                    .MargenImpto = item("MargenImpto")
                    .MargenLiberadoImpto = item("MargenLiberadoImpto")
                    .TotImpto = item("TotImpto")
                    .NetoHab = item("NetoHab")
                    .IgvHab = item("IgvHab")
                    .TotalHab = item("TotalHab")
                    .NetoGen = item("NetoGen")
                    .IgvGen = item("IgvGen")
                    .TotalGen = item("TotalGen")
                    .IDDetRel = If(IsDBNull(item("IDDetRel")) = True, "0", item("IDDetRel"))
                    .Servicio = If(IsDBNull(item("Servicio")) = True, "", item("Servicio"))
                    .IDReserva_DetCopia = If(IsDBNull(item("IDReserva_DetCopia")) = True, 0, item("IDReserva_DetCopia"))
                    .IDReserva_Det_Rel = If(IsDBNull(item("IDReserva_Det_Rel")) = True, 0, item("IDReserva_Det_Rel"))
                    .IDEmailRef = If(IsDBNull(item("IDEmailRef")) = True, "", item("IDEmailRef"))
                    .IDEmailEdit = If(IsDBNull(item("IDEmailEdit")) = True, "", item("IDEmailEdit"))
                    .IDEmailNew = If(IsDBNull(item("IDEmailNew")) = True, "", item("IDEmailNew"))
                    .CapacidadHab = If(IsDBNull(item("CapacidadHab")) = True, 0, item("CapacidadHab"))
                    .EsMatrimonial = If(IsDBNull(item("EsMatrimonial")) = True, False, item("EsMatrimonial"))
                    .UserMod = vstrUserMod
                    .IDMoneda = If(IsDBNull(item("IDMoneda")) = True, "", item("IDMoneda"))
                    .IDGuiaProveedor = If(IsDBNull(item("IDGuiaProveedor")) = True, "", item("IDGuiaProveedor"))
                    .NuVehiculo = If(IsDBNull(item("NuVehiculo")) = True, 0, item("NuVehiculo"))
                    .ExecTrigger = item("ExecTrigger")
                    .FlServicioParaGuia = Convert.ToBoolean(item("FlServicioParaGuia"))
                    .FlServicioNoShow = Convert.ToBoolean(item("FlServicioNoShow"))
                    .FlServicioIngManual = Convert.ToBoolean(item("FlServicioIngManual"))
                    .NuViaticosTC = item("NuViaticoTC")
                    .Accion = "N"
                End With
                rListReservaDet.Add(objReservasDet)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function dtRetornarReservasDetIngresadasManualmente(ByVal vintIDReserva As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            Return objADO.GetDataTable("RESERVAS_DET_SelIngresadasManualmente", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtRetornarReservasDetIngresadasViaticos(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable("VIATICOS_TOURCONDUCTOR_SelxIDCabInReservasDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub ActualizarEstadoCotizRegenerada(ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
            End With
            objADO.ExecuteSP("COTICAB_UpdEstadoRegeneracion", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    '    Private Sub ActualizarEliminarDetReservasHotelesDiasSeguidos(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
    '        Try
    '            Dim dc As New Dictionary(Of String, String)
    '            dc.Add("@IDCab", vintIDCab)

    '            Dim dtHot As DataTable = objADO.GetDataTable("RESERVAS_DET_SelHotelesxIDCab", dc)


    '            For Each dr As DataRow In dtHot.Rows

    '                Dim datFechaAnt As Date = "01/01/1900"
    '                'Dim strIDProveedorAnt As String = ""
    '                Dim strServicioAnt As String = ""
    '                Dim intIDDetAnt As Integer = 0
    '                Dim intIDReserva_DetAnt As Integer = 0



    '                Dim LstDetRes As New List(Of stReservasFechaOut)
    '                Dim datFecIn As Date = "01/01/1900"
    '                Dim datFecOut As Date = "01/01/1900"
    '                'Dim bytNoches As Byte = 0

    '                'Dim dc2 As New Dictionary(Of String, String)
    '                'dc2.Add("@IDReserva", dr("IDReserva"))

    '                Dim bln1ero As Boolean = True
    '                Dim intIDDet1ero As Integer = 0
    '                Dim intIDReserva_Det1ero As Integer = 0
    '                Dim intNroPax As Int16 = 0
    '                Dim intNroLiberados As Int16 = 0

    '                'Dim dtHotRes As DataTable = objADO.GetDataTable("RESERVAS_DET_SelxIDReserva2", dc2)
    '                Dim dtHotRes As DataTable = ConsultarxIDReserva(dr("IDReserva"))
    '                For Each dr2 As DataRow In dtHotRes.Rows
    '                    'If dr2("IDProveedor") = "000637" Then
    '                    '    dr2("IDProveedor") = "000637"
    '                    'End If
    'Inicio:
    '                    Dim blnMismoHotel As Boolean = False
    '                    'If datFechaAnt = DateAdd(DateInterval.Day, -1, dr2("Dia")) And _
    '                    '    strServicioAnt = dr2("IDServicio_Det").ToString And _
    '                    '    intNroPax = dr2("NroPax") And _
    '                    '    intNroLiberados = dr2("NroLiberados") Then
    '                    '    blnMismoHotel = True
    '                    'End If
    '                    If datFechaAnt = DateAdd(DateInterval.Day, -1, dr2("Dia")) And _
    '                        strServicioAnt = dr2("IDServicio_Det").ToString Then
    '                        blnMismoHotel = True
    '                    End If

    '                    If blnMismoHotel Then

    '                        If bln1ero Then
    '                            intIDDet1ero = intIDDetAnt
    '                            intIDReserva_Det1ero = intIDReserva_DetAnt
    '                            datFecIn = datFechaAnt
    '                            bln1ero = False
    '                        End If

    '                        datFecOut = dr2("FechaOut")
    '                        If dr2("ConAlojamiento") = True And dr2("Dias") > 1 Then
    '                            datFecOut = DateAdd(DateInterval.Day, -1, datFecOut)
    '                        End If

    '                        Dim objST As New stReservasFechaOut
    '                        objST.IDDet = dr2("IDDet")
    '                        objST.FechaOut = datFecOut
    '                        objST.IDDet1ero = intIDDet1ero

    '                        LstDetRes.Add(objST)

    '                        blnMismoHotel = False
    '                    Else
    '                        If Not bln1ero Then
    '                            bln1ero = True
    '                            GoTo Inicio
    '                        End If
    '                    End If

    '                    datFechaAnt = dr2("Dia")
    '                    strServicioAnt = dr2("IDServicio_Det").ToString
    '                    intIDDetAnt = dr2("IDDet")
    '                    intIDReserva_DetAnt = dr2("IDReserva_Det")

    '                    intNroPax = dr2("NroPax")
    '                    intNroLiberados = dr2("NroLiberados")
    '                Next

    '                If LstDetRes.Count > 0 Then
    '                    Dim objDetRes As New clsReservaBT.clsDetalleReservaBT
    '                    For Each Item As stReservasFechaOut In LstDetRes
    '                        'If Item.IDDet <> intIDDet1ero Then
    '                        objDetRes.EliminarxIDDet(Item.IDDet)
    '                        objDetRes.ActualizarNoches(Item.IDDet1ero, Item.FechaOut, vstrUserMod)
    '                        'End If
    '                    Next

    '                    'objDetRes.ActualizarNoches(intIDReserva_Det1ero, bytNoches, datFecOut, vstrUserMod)
    '                End If

    '            Next

    '            ContextUtil.SetComplete()
    '        Catch ex As Exception
    '            ContextUtil.SetAbort()
    '            Throw
    '        End Try
    '    End Sub

    Private Sub ActualizarEliminarDetReservasHotelesDiasSeguidosxReserva(ByVal vintIDReserva As Integer, ByVal vblnExisteAlojamiento As Boolean, _
                                                                         ByVal vstrUserMod As String)
        Try


            Dim datFechaAnt As Date = "01/01/1900"
            'Dim strIDProveedorAnt As String = ""
            Dim strServicioAnt As String = ""
            Dim intIDDetAnt As Integer = 0
            Dim intIDReserva_DetAnt As Integer = 0
            Dim blnAcomodoEspecialAnt As Boolean = False


            Dim LstDetRes As New List(Of stReservasFechaOut)
            Dim datFecIn As Date = "01/01/1900"
            Dim datFecOut As Date = "01/01/1900"
            'Dim bytNoches As Byte = 0

            'Dim dc2 As New Dictionary(Of String, String)
            'dc2.Add("@IDReserva", vintIDReserva)

            Dim bln1ero As Boolean = True
            Dim intIDDet1ero As Integer = 0
            Dim intIDReserva_Det1ero As Integer = 0
            Dim intNroPax As Int16 = 0
            Dim intNroLiberados As Int16 = 0
            Dim intIDDet1eroTotal As Integer = 0

            'Dim blnAcomodoEspecial As Boolean = False
            'Dim dtHotRes As DataTable = objADO.GetDataTable("RESERVAS_DET_SelxIDReserva2", dc2)
InicioCambiandoAcomodoEspecial:
            Dim dtHotRes As DataTable
            'If Not blnAcomodoEspecial Then
            dtHotRes = ConsultarxIDReserva(vintIDReserva) ', blnAcomodoEspecial)
            'Else
            'Dim objAcoBN As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
            'dtHotRes = objAcoBN.ConsultarxIDReserva(vintIDReserva)
            'End If


            For Each dr2 As DataRow In dtHotRes.Rows

                If (dr2("IDTipoProv") = gstrTipoProvHotel And dr2("PlanAlimenticio") = False) Or _
                    (dr2("IDTipoProv") <> gstrTipoProvHotel And dr2("ConAlojamiento") = True) Then




                    'If dr2("IDProveedor") = "000637" Then
                    '    dr2("IDProveedor") = "000637"
                    'End If
Inicio:
                    Dim blnMismoHotel As Boolean = False
                    'If datFechaAnt = DateAdd(DateInterval.Day, -1, dr2("Dia")) And _
                    '    strServicioAnt = dr2("IDServicio_Det").ToString And _
                    '    intNroPax = dr2("NroPax") And _
                    '    intNroLiberados = dr2("NroLiberados") Then
                    '    blnMismoHotel = True
                    'End If
                    'If blnAcomodoEspecial = False Then
                    'If datFechaAnt = DateAdd(DateInterval.Day, -1, dr2("Dia")) And _
                    '   strServicioAnt = dr2("IDServicio_Det").ToString Then
                    'blnMismoHotel = True
                    'End If
                    'Else
                    If datFechaAnt = DateAdd(DateInterval.Day, -1, dr2("Dia")) And _
                        strServicioAnt = dr2("IDServicio_Det").ToString And _
                        blnAcomodosEspecialesIguales(intIDDetAnt, dr2("IDDet"), blnAcomodoEspecialAnt, dr2("AcomodoEspecial")) = True Then
                        blnMismoHotel = True
                    End If
                    'End If

                    If blnMismoHotel Then

                        If bln1ero Then
                            intIDDet1ero = intIDDetAnt
                            intIDReserva_Det1ero = intIDReserva_DetAnt
                            datFecIn = datFechaAnt
                            bln1ero = False
                            If intIDDet1eroTotal = 0 Then
                                intIDDet1eroTotal = intIDDet1ero
                            End If
                        End If

                        Dim blnConAlojamiento As Boolean = False
                        datFecOut = If(IsDBNull(dr2("FechaOut")), "01/01/1900", dr2("FechaOut"))
                        If dr2("ConAlojamiento") = True And dr2("Dias") > 1 Then
                            datFecOut = DateAdd(DateInterval.Day, -1, datFecOut)
                            intIDDet1ero = intIDDet1eroTotal
                            blnConAlojamiento = True
                        End If

                        Dim objST As New stReservasFechaOut
                        objST.IDDet = dr2("IDDet")
                        objST.IDReserva_Det = dr2("IDReserva_Det")
                        objST.IDReserva_Det1ero = intIDReserva_Det1ero
                        objST.FechaOut = datFecOut
                        objST.IDDet1ero = intIDDet1ero
                        objST.ConAlojamiento = blnConAlojamiento

                        LstDetRes.Add(objST)

                        blnMismoHotel = False
                    Else
                        If Not bln1ero Then
                            bln1ero = True
                            GoTo Inicio
                        End If
                    End If

                    datFechaAnt = dr2("Dia")
                    strServicioAnt = dr2("IDServicio_Det").ToString
                    intIDDetAnt = dr2("IDDet")
                    intIDReserva_DetAnt = dr2("IDReserva_Det")

                    intNroPax = dr2("NroPax")
                    intNroLiberados = dr2("NroLiberados")
                    blnAcomodoEspecialAnt = dr2("AcomodoEspecial")
                End If
            Next




            If LstDetRes.Count > 0 Then
                Dim objDetRes As New clsReservaBT.clsDetalleReservaBT
                Dim blnCruce As Boolean = False
                Dim intIDDetAnteriorCruce As Integer = 0
                Dim intIDReserva_DetAnteriorCruce As Integer = 0
                Dim ListaIDDet As New List(Of Integer)

                For Each Item As stReservasFechaOut In LstDetRes
                    'If Item.IDDet <> intIDDet1ero Then
                    If Item.FechaOut <> "01/01/1900" Then

                        'If blnEliminar Then
                        Dim objOrdenPagoBT As New clsOrdenPagoBT
                        If Not objOrdenPagoBT.blnExistexIDDet(Item.IDDet) Then
                            objDetRes.EliminarxIDDet(Item.IDDet)
                        Else
                            objDetRes.AnularxIDDet(Item.IDDet, True, vstrUserMod)
                        End If


                        'End If


                        If Not blnCruce Then

                            objDetRes.ActualizarNoches(Item.IDDet1ero, CDate(Item.FechaOut.ToShortDateString), vstrUserMod)
                            'objDetRes.ActualizarNochesxIDReserva_Det(Item.IDReserva_Det, Item.FechaOut, vstrUserMod)
                            If Not Item.ConAlojamiento Then
                                ListaIDDet.Add(Item.IDDet1ero)
                            End If

                        Else
                            objDetRes.ActualizarNoches(intIDDetAnteriorCruce, Item.FechaOut, vstrUserMod)
                            'objDetRes.ActualizarNochesxIDReserva_Det(intIDReserva_DetAnteriorCruce, Item.FechaOut, vstrUserMod)
                            If Not Item.ConAlojamiento Then
                                ListaIDDet.Add(intIDDetAnteriorCruce)
                            End If
                        End If

                        'If Not blnCruce Then

                        blnCruce = False
                        For Each Item2 As stReservasFechaOut In LstDetRes
                            If Item2.IDDet1ero = Item.IDDet Then
                                blnCruce = True
                                If intIDDetAnteriorCruce = 0 Then
                                    intIDDetAnteriorCruce = Item.IDDet1ero
                                End If
                                If intIDReserva_DetAnteriorCruce = 0 Then
                                    intIDReserva_DetAnteriorCruce = Item.IDReserva_Det1ero
                                End If
                                Exit For
                            End If

                        Next
                        'Else
                        '   blnCruce = False
                        'End If
                    End If
                    'End If
                Next

                'objDetRes.ActualizarNoches(intIDReserva_Det1ero, bytNoches, datFecOut, vstrUserMod)

                For Each intIDDet As Integer In ListaIDDet
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add("@IDDet", intIDDet)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("RESERVAS_DET_UpdNochesFinal", dc)
                Next
            End If

            'If blnAcomodoEspecial = False Then
            '    blnAcomodoEspecial = True
            '    LstDetRes.Clear()
            '    GoTo InicioCambiandoAcomodoEspecial
            'End If
            ActualizarDetPaxPostGeneracion(vintIDReserva, vblnExisteAlojamiento, vstrUserMod)



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Function blnAcomodosEspecialesIguales(ByVal vintIDDet1 As Integer, ByVal vintIDDet2 As Integer, _
                                                  ByVal vblnAcomodoEspecial1 As Boolean, ByVal vblnAcomodoEspecial2 As Boolean) As Boolean

        If vintIDDet1 = 0 Then Return True
        'If vintIDDet1 = vintIDDet2 Then Return True
        If vblnAcomodoEspecial1 = False And vblnAcomodoEspecial2 = False Then Return True

        If vblnAcomodoEspecial1 = False And vblnAcomodoEspecial2 = True Then Return False
        If vblnAcomodoEspecial1 = True And vblnAcomodoEspecial2 = False Then Return False

        Try
            'Dim objAcomBN As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
            Dim dtt1 As DataTable = ConsultarListxIDDet(vintIDDet1)
            Dim dtt2 As DataTable = ConsultarListxIDDet(vintIDDet2)

            If dtt1.Rows.Count <> dtt2.Rows.Count Then
                Return False
            End If
            Dim bytInd As Byte = 0
            For Each dr1 As DataRow In dtt1.Rows
                If dr1("CoCapacidad") <> dtt2(bytInd)("CoCapacidad") Or dr1("QtPax") <> dtt2(bytInd)("QtPax") Then
                    Return False
                End If
                bytInd += 1
            Next

            Return True
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarListxIDDet(ByVal vintIDDet As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            Return objADO.GetDataTable("COTIDET_ACOMODOESPECIAL_SelxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDDetxCapacidad(ByVal vintIDDet As Int32, ByVal vintNroCapacidad As Byte, ByVal vblnEsMatrimonial As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@Capacidad", vintNroCapacidad)
            dc.Add("@EsMatrimonial", vblnEsMatrimonial)

            Return objADO.GetDataTable("COTIDET_ACOMODOESPECIAL_SelxIDDetxCapacidad", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub ActualizarEliminarDetReservasHotelesDiasSeguidosxReservaTMP(ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
        Try
            Dim datFechaAnt As Date = "01/01/1900"
            'Dim strIDProveedorAnt As String = ""
            Dim strServicioAnt As String = ""
            Dim intIDDetAnt As Integer = 0
            Dim intIDReserva_DetAnt As Integer = 0
            Dim blnAcomodoEspecialAnt As Boolean = False


            Dim LstDetRes As New List(Of stReservasFechaOut)
            Dim datFecIn As Date = "01/01/1900"
            Dim datFecOut As Date = "01/01/1900"
            'Dim bytNoches As Byte = 0

            'Dim dc2 As New Dictionary(Of String, String)
            'dc2.Add("@IDReserva", vintIDReserva)

            Dim bln1ero As Boolean = True
            Dim intIDDet1ero As Integer = 0
            Dim intIDReserva_Det1ero As Integer = 0
            Dim intNroPax As Int16 = 0
            Dim intNroLiberados As Int16 = 0
            Dim intIDDet1eroTotal As Integer = 0

            'Dim dtHotRes As DataTable = objADO.GetDataTable("RESERVAS_DET_SelxIDReserva2", dc2)
            Dim dtHotRes As DataTable = ConsultarxIDReservaTMP(vintIDReserva)
            For Each dr2 As DataRow In dtHotRes.Rows

                If (dr2("IDTipoProv") = gstrTipoProvHotel And dr2("PlanAlimenticio") = False) Or _
                    (dr2("IDTipoProv") <> gstrTipoProvHotel And dr2("ConAlojamiento") = True) Then




                    'If dr2("IDProveedor") = "000637" Then
                    '    dr2("IDProveedor") = "000637"
                    'End If
Inicio:
                    Dim blnMismoHotel As Boolean = False
                    'If datFechaAnt = DateAdd(DateInterval.Day, -1, dr2("Dia")) And _
                    '    strServicioAnt = dr2("IDServicio_Det").ToString And _
                    '    intNroPax = dr2("NroPax") And _
                    '    intNroLiberados = dr2("NroLiberados") Then
                    '    blnMismoHotel = True
                    'End If
                    If datFechaAnt = DateAdd(DateInterval.Day, -1, dr2("Dia")) And _
                        strServicioAnt = dr2("IDServicio_Det").ToString And _
                        blnAcomodosEspecialesIguales(intIDDetAnt, dr2("IDDet"), blnAcomodoEspecialAnt, dr2("AcomodoEspecial")) = True Then
                        blnMismoHotel = True
                    End If

                    If blnMismoHotel Then

                        If bln1ero Then
                            intIDDet1ero = intIDDetAnt
                            intIDReserva_Det1ero = intIDReserva_DetAnt
                            datFecIn = datFechaAnt
                            bln1ero = False
                            If intIDDet1eroTotal = 0 Then
                                intIDDet1eroTotal = intIDDet1ero
                            End If
                        End If

                        Dim blnConAlojamiento As Boolean = False
                        datFecOut = If(IsDBNull(dr2("FechaOut")), "01/01/1900", dr2("FechaOut"))
                        If dr2("ConAlojamiento") = True And dr2("Dias") > 1 Then
                            datFecOut = DateAdd(DateInterval.Day, -1, datFecOut)
                            intIDDet1ero = intIDDet1eroTotal
                            blnConAlojamiento = True
                        End If

                        Dim objST As New stReservasFechaOut
                        objST.IDDet = dr2("IDDet")
                        objST.IDReserva_Det = dr2("IDReserva_Det")
                        objST.IDReserva_Det1ero = intIDReserva_Det1ero
                        objST.FechaOut = datFecOut
                        objST.IDDet1ero = intIDDet1ero
                        objST.ConAlojamiento = blnConAlojamiento
                        LstDetRes.Add(objST)

                        blnMismoHotel = False
                    Else
                        If Not bln1ero Then
                            bln1ero = True
                            GoTo Inicio
                        End If
                    End If

                    datFechaAnt = dr2("Dia")
                    strServicioAnt = dr2("IDServicio_Det").ToString
                    intIDDetAnt = dr2("IDDet")
                    intIDReserva_DetAnt = dr2("IDReserva_Det")

                    intNroPax = dr2("NroPax")
                    intNroLiberados = If(IsDBNull(dr2("NroLiberados")) = True, 0, Convert.ToInt16(dr2("NroLiberados")))
                    blnAcomodoEspecialAnt = dr2("AcomodoEspecial")
                End If
            Next




            If LstDetRes.Count > 0 Then
                Dim objDetRes As New clsReservaBT.clsDetalleReservaBT
                Dim blnCruce As Boolean = False
                Dim intIDDetAnteriorCruce As Integer = 0
                Dim intIDReserva_DetAnteriorCruce As Integer = 0
                Dim ListaIDDet As New List(Of Integer)

                For Each Item As stReservasFechaOut In LstDetRes
                    'If Item.IDDet <> intIDDet1ero Then
                    If Item.FechaOut <> "01/01/1900" Then

                        'If blnEliminar Then


                        objDetRes.EliminarxIDDetTMP(Item.IDDet)


                        'End If

                        If Not blnCruce Then
                            objDetRes.ActualizarNochesTMP(Item.IDDet1ero, CDate(Item.FechaOut.ToShortDateString), vstrUserMod)
                            'objDetRes.ActualizarNochesxIDReserva_Det(Item.IDReserva_Det, Item.FechaOut, vstrUserMod)
                            If Not Item.ConAlojamiento Then
                                ListaIDDet.Add(Item.IDDet1ero)
                            End If
                        Else
                            objDetRes.ActualizarNochesTMP(intIDDetAnteriorCruce, Item.FechaOut, vstrUserMod)
                            'objDetRes.ActualizarNochesxIDReserva_Det(intIDReserva_DetAnteriorCruce, Item.FechaOut, vstrUserMod)
                            If Not Item.ConAlojamiento Then
                                ListaIDDet.Add(intIDDetAnteriorCruce)
                            End If
                        End If

                        'If Not blnCruce Then

                        blnCruce = False
                        For Each Item2 As stReservasFechaOut In LstDetRes
                            If Item2.IDDet1ero = Item.IDDet Then
                                blnCruce = True
                                If intIDDetAnteriorCruce = 0 Then
                                    intIDDetAnteriorCruce = Item.IDDet1ero
                                End If
                                If intIDReserva_DetAnteriorCruce = 0 Then
                                    intIDReserva_DetAnteriorCruce = Item.IDReserva_Det1ero
                                End If
                                Exit For
                            End If

                        Next
                        'Else
                        '   blnCruce = False
                        'End If
                    End If
                    'End If
                Next

                'objDetRes.ActualizarNoches(intIDReserva_Det1ero, bytNoches, datFecOut, vstrUserMod)

                For Each intIDDet As Integer In ListaIDDet
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add("@IDDet", intIDDet)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("RESERVAS_DET_TMP_UpdNochesFinal", dc)
                Next

            End If

            '------


            'Dim ListaPaxDetReservas As New List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
            'Dim BE As New clsReservaBE

            'Dim dc3 As New Dictionary(Of String, String)
            'dc3.Add("@IDReserva", vintIDReserva)

            'Dim dtDetResPax As DataTable = objADO.GetDataTable("RESERVAS_DET_PAX_SelxIDReserva", dc3)
            'For Each dr As DataRow In dtDetResPax.Rows
            '    Dim objPaxBE As New clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE With { _
            '    .IDReserva_Det = dr("IDReserva_DetTMP"), _
            '    .IDReserva_DetAcomodoRealTMP = dr("IDReserva_DetReal"), _
            '    .IDPax = dr("IDPax"), .UserMod = vstrUserMod}
            '    ListaPaxDetReservas.Add(objPaxBE)
            'Next
            'BE.ListaPaxDetReservas = ListaPaxDetReservas

            'Dim objBTDetResPax As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT
            'objBTDetResPax.InsertarEliminarTMP(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub BackupearDetalleEliminado(ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            objADO.ExecuteSP("RESERVAS_DET_BUP_Del_xIDReserva", dc)

            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_DET_BUP_Ins_xIDReserva", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".BackupearDetalleEliminado")
        End Try
    End Sub

    Public Function ConsultarDetalleBackupeado(ByVal vintIDReserva As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            Return objADO.GetDataTable("RESERVAS_DET_BUP_Sel_xIDReserva", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarxIDReserva(ByVal vintIDReserva As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            'dc.Add("@AcomodoEspecial", vblnAcomodoEspecial)

            Return objADO.GetDataTable("RESERVAS_DET_SelxIDReserva2", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarxIDReserva2(ByVal vintIDReserva As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            Return objADO.GetDataTable("RESERVAS_DET_SelxIDReserva3", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarxIDReservaTMP(ByVal vintIDReserva As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            Return objADO.GetDataTable("RESERVAS_DET_TMP_SelxIDReserva", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function ConsultarxIDReservaTMP2(ByVal vintIDReserva As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            Return objADO.GetDataTable("RESERVAS_DET_TMP_SelxIDReserva2", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function strDevuelveDescripcionTareaPolitica(ByVal vbytIDTarea As Byte) As String
        Dim strReturn As String = ""
        Try
            Select Case vbytIDTarea
                Case 1 : strReturn = "ROOMING PRELIMINAR"
                Case 2 : strReturn = "ROOMING ACTUALIZADO"
                Case 3 : strReturn = "ROOMING FINAL"
                Case 4 : strReturn = "PREPAGO"
                Case 5 : strReturn = "PREPAGO 2"
                Case 6 : strReturn = "SALDO"
                Case 7 : strReturn = "PAGO SIN PENALIDAD"
            End Select

            Return strReturn
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function datDevuelveFechaMenorTareasGeneradas(ByVal vintIDCab As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pFecDeadLine", "01/01/1900")
                Return objADO.ExecuteSPOutput("RESERVAS_TAREAS_Sel_FecDeadLineOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dicDevuelveFechasDeadLinexFile(ByVal vintIDCab As Integer) As Dictionary(Of String, Date)
        Dim dcReturn As New Dictionary(Of String, Date)
        Try
            Dim objCotiBT As New clsCotizacionBT
            Dim datFechaReserva As Date = FormatDateTime(objCotiBT.datDevuelveFechaReservaCoti(vintIDCab), DateFormat.ShortDate)
            Dim datFechaMasCercana As Date = FormatDateTime(datDevuelveFechaMenorTareasGeneradas(vintIDCab), DateFormat.ShortDate)
            Dim objRecBN As New clsReservaBN.clsRecordatoriosBN
            Dim drRec As SqlClient.SqlDataReader = objRecBN.ConsultarDiasparaGeneracion

            While drRec.Read
                dcReturn.Add(drRec("IDRecordatorio") & "|" & drRec("Descripcion"), _
                             datSumaRestaDiasFechas(If(drRec("TipoCalculoTarea") = "FR", datFechaReserva, datFechaMasCercana), _
                                                    If(drRec("TipoCalculoTarea") = "FR", drRec("Dias"), drRec("Dias") * (-1)), _
                                                    gstrPeru))

            End While

            Return dcReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnNuevaReserva(ByVal BECot As clsCotizacionBE, ByVal vstrIDTipoProv As String, _
                                     ByVal vstrIDProveedorHotel As String, ByVal vdatFechaDia As Date) As Boolean

        Dim blnReturn As Boolean = False

        If vstrIDTipoProv = gstrTipoProvHotel Then
            Dim bytCont As Byte = 0
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                If Item.IDProveedor = vstrIDProveedorHotel Then
                    bytCont += 1
                End If
            Next
            If bytCont = 1 Then
                blnReturn = True
            Else
                Dim bln1ero As Boolean = True
                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                    If Item.IDProveedor = vstrIDProveedorHotel And Item.Dia < vdatFechaDia Then
                        bln1ero = False
                        Exit For
                    End If
                Next
                If bln1ero Then
                    blnReturn = True
                Else

                    For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                        If Item.IDProveedor = vstrIDProveedorHotel And Item.Dia < vdatFechaDia Then
                            If DateDiff(DateInterval.Day, Item.Dia, vdatFechaDia) > 1 Then
                                blnReturn = True
                                Exit For
                            End If
                        End If
                    Next

                End If

            End If
        Else
            blnReturn = True
        End If


        Return blnReturn
    End Function

    Public Sub ActualizarFechaEnvioAventas(ByVal vintIDCab As Integer, ByVal vdatFechaEnvio As Date, ByVal vstrUserMod As String, _
                                           ByVal vstrRutaImagenes As String)
        'Dim dc As New Dictionary(Of String, String)
        'Try
        '    dc.Add("@IDCab", vintIDCab)
        '    dc.Add("@FechaRetornoReservas", vdatFechaEnvio)
        '    dc.Add("@UserMod", vstrUserMod)

        '    objADO.ExecuteSP("COTICAB_Upd_FechaRetornoReservas", dc)

        '    If vdatFechaEnvio.ToShortDateString <> "01/01/1900" Then
        '        Dim objCorreoBT As New clsCorreoVentasBT
        '        objCorreoBT.gstrRutaImagenes = vstrRutaImagenes
        '        objCorreoBT.NotificacionEnvioFileModulo1aModulo2(vintIDCab, "R", "V")
        '    End If

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EnviaraVentas")
        'End Try
    End Sub

    Public Sub ActualizarFechaEnvioAventas_Campos(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            'dc.Add("@Campo", vbytCampo)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_Upd_FechaRetornoReservas_Campos", dc)

            ''If vdatFechaEnvio.ToShortDateString <> "01/01/1900" Then
            'Dim objCorreoBT As New clsCorreoVentasBT
            'objCorreoBT.gstrRutaImagenes = vstrRutaImagenes
            'objCorreoBT.NotificacionEnvioFileModulo1aModulo2(vintIDCab, "R", "V")
            '' End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EnviaraVentas")
        End Try
    End Sub

    Public Sub AsignarUsuario(ByVal vintIDCab As Integer, ByVal vstrIDUsuarioAsignado As String, ByVal vstrUserMod As String, _
                              ByVal vstrRutaImagenesPlant As String)
        Dim dc As New Dictionary(Of String, String)
        'Try
        '    If vstrIDUsuarioAsignado = "" Then vstrIDUsuarioAsignado = "0000"
        '    dc.Add("@IDCab", vintIDCab)
        '    dc.Add("@IDUsuarioReser", vstrIDUsuarioAsignado)
        '    dc.Add("@UserMod", vstrUserMod)
        '    objADO.ExecuteSP("RESERVAS_Upd_UserReserva", dc)

        '    If vstrIDUsuarioAsignado <> "0000" Then
        '        Dim objCorr As New clsCorreoVentasBT
        '        objCorr.gstrRutaImagenes = vstrRutaImagenesPlant
        '        objCorr.NotificacionAsignacionFile(vintIDCab, vstrIDUsuarioAsignado)
        '    End If

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    'Throw
        '    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AsignarUsuario")
        'End Try
    End Sub


    Public Sub AsignarUsuario_Res_Lista(ByVal vobjLista As List(Of String), ByVal vstrIDUsuarioAsignado As String, ByVal vstrUserMod As String, _
                                        ByVal gstrRutaImagenes As String)
        Dim dc As New Dictionary(Of String, String)
        Try

            If vobjLista Is Nothing Then Exit Sub
            Try
                For Each Item As String In vobjLista
                    AsignarUsuario(Item, vstrIDUsuarioAsignado, vstrUserMod, gstrRutaImagenes)
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AsignarUsuario")
        End Try
    End Sub

    Private Sub ActualizarNroPaxDetalle(ByVal vListaDetResTotal As List(Of stDetReservas))

        Dim dc As Dictionary(Of String, String)
        Dim ListaPax As New List(Of String)
        Try
            For Each Item As stDetReservas In vListaDetResTotal

                dc = New Dictionary(Of String, String)

                With dc
                    .Add("@IDCab", Item.IDCab)
                    .Add("@IDProveedor", Item.IDProveedor)
                    .Add("@Servicio", Item.Servicio)
                    '.Add("@Cantidad", Item.Cantidad)
                    .Add("@NroPax", Item.NroPax)
                    .Add("@IDDet", Item.IDDet)
                    .Add("@UserMod", Item.UserMod)
                End With
                objADO.ExecuteSP("RESERVAS_DET_Upd_NroPax", dc)
Sgte:

            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroPaxDetalle")
        End Try
    End Sub

    Private Sub ActualizarCantidadesDetalle(ByVal vintIDCab As Int32, _
                                            ByVal vbytSimple As Byte, ByVal vbytTwin As Byte, ByVal vbytMatrim As Byte, _
                                            ByVal vbytTriple As Byte, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try

            With dc
                .Add("@IDCab", vintIDCab)

                .Add("@Simple", vbytSimple)

                .Add("@Twin", vbytTwin)
                .Add("@Matrim", vbytMatrim)

                .Add("@Triple", vbytTriple)

                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("RESERVAS_DET_Upd_Cantidades", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCantidadesDetalle")
        End Try
    End Sub
    Private Sub ActualizarCantidadesDetalleHabitGuia(ByVal vintIDCab As Int32, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try

            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("RESERVAS_DET_Upd_CantidadesHabitacGuias", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCantidadesDetalleHabitGuia")
        End Try
    End Sub

    Private Sub MultiplicarxCantidadMontosDetallexIDCab(ByVal vintIDCab As Integer, ByVal vintIDDet As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDDet", vintIDDet)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("RESERVAS_DET_UpdMontosxNroPax", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".MultiplicarxCantidadMontosDetallexIDCab")
        End Try
    End Sub

    Private Function strDescCapacidadHabitacion(ByVal vbytCapacMax As Byte, ByVal vblnMatrim As Boolean) As String
        Dim strReturn As String = ""

        Select Case vbytCapacMax
            Case 1
                strReturn = "SINGLE"
            Case 2
                'strReturn = "DOBLE"
                If vblnMatrim Then
                    'strReturn = "DBL-MAT"
                    strReturn = "DOUBLE (MAT)"
                Else
                    'strReturn = "DBL-TWIN"
                    'strReturn = "DOUBLE (TWIN)"
                    strReturn = "TWIN"
                End If

            Case 3
                strReturn = "TRIPLE"
            Case 4
                strReturn = "SINGLE (GUIDE)"
        End Select

        Return strReturn
    End Function
    Private Function strCoCapacidadporCantidad(ByVal vintIDDet As Integer, _
                                               ByVal vblnEsGuide As Boolean, _
                                               ByVal vintQtCapacidad As Int16, _
                                               ByVal vblnEsMatrimonial As Boolean, _
                                               ByRef rListaCoCapacidad As List(Of String)) As String


        'ByVal vblnMatrimonial As Boolean

        Dim objVtaBT As New clsDetalleVentasCotizacionBT
        Dim dttAcom As DataTable = objVtaBT.ConsultarAcomodoEspecial(vintIDDet)

        'If vintQtCapacidad <> 2 Then
        '    Dim dttFiltrado As DataTable = dtFiltrarDataTable(dttAcom, "QtCapacidad=" & vintQtCapacidad)
        '    If dttFiltrado.Rows.Count > 0 Then
        '        Return dttFiltrado(0)("CoCapacidad")
        '    End If

        'Else
        '    Dim dttFiltrado As DataTable
        '    If vblnMatrimonial Then
        '        dttFiltrado = dtFiltrarDataTable(dttAcom, "QtCapacidad=" & vintQtCapacidad & " And NoCapacidad Like '%MAT%'")
        '    Else
        '        dttFiltrado = dtFiltrarDataTable(dttAcom, "QtCapacidad=" & vintQtCapacidad & " And Not NoCapacidad Like '%MAT%'")
        '    End If

        '    If dttFiltrado.Rows.Count > 0 Then
        '        Return dttFiltrado(0)("CoCapacidad")
        '    End If

        'End If

        Dim dttFiltrado As DataTable = dtFiltrarDataTable(dttAcom, "QtCapacidad=" & vintQtCapacidad & " and FlMatrimonial=" & vblnEsMatrimonial & _
                                                          " and FlGuide = " & vblnEsGuide)
        Dim strCoCapacidad As String = ""
        For Each dr As DataRow In dttFiltrado.Rows
            If Not rListaCoCapacidad.Contains(dr("CoCapacidad")) Then
                rListaCoCapacidad.Add(dr("CoCapacidad"))
                Return dr("CoCapacidad")
            End If
            strCoCapacidad = dr("CoCapacidad")
        Next
        Return strCoCapacidad
    End Function
    Private Function strDescCapacidadHabitacionAcomodoEspecial(ByVal vdttCap As DataTable, ByVal vstrCoCapacidad As String) As String
        'ByVal vbytCapacMax As Byte, ByVal vblnMatrim As Boolean
        Dim strReturn As String = ""

        'Select Case vbytCapacMax
        '    Case 1
        '        strReturn = "SINGLE"
        '    Case 2
        '        If vblnMatrim Then
        '            strReturn = "DOUBLE (MAT)"
        '        Else
        '            strReturn = "TWIN"
        '        End If
        '    Case 3
        '        strReturn = "TRIPLE"
        '    Case 4
        '        strReturn = "CUÁDRUPLE"
        '    Case 5
        '        strReturn = "QUÍNTUPLE"
        '    Case 6
        '        strReturn = "SÉXTUPLE"
        '    Case 7
        '        strReturn = "SÉPTUPLE"
        '    Case 8
        '        strReturn = "ÓCTUPLE"

        'End Select
        '--------------------------

        'Dim objTabBN As New clsTablasApoyoBN.clsCapacidadHabitacionBN
        'Dim dtt As DataTable = objTabBN.ConsultarCbo

        Dim dttFiltrado As DataTable = dtFiltrarDataTable(vdttCap, "CoCapacidad='" & vstrCoCapacidad & "'")
        If dttFiltrado.Rows.Count > 0 Then
            Return dttFiltrado(0)("NoCapacidad")
        End If

        Return strReturn
    End Function

    Private Function strDescCapacidadHabitacionOperConAlojamiento(ByVal vbytCapacMax As Byte, ByVal vblnMatrim As Boolean, ByVal vstrDetaVariante As String) As String
        Dim strReturn As String = ""

        Select Case vbytCapacMax
            Case 1
                strReturn = vstrDetaVariante & " SINGLE"
            Case 2
                'strReturn = "DOBLE"
                If vblnMatrim Then
                    'strReturn = "DBL-MAT"
                    strReturn = vstrDetaVariante & " DOUBLE (MAT)"
                Else
                    'strReturn = "DBL-TWIN"
                    'strReturn = "DOUBLE (TWIN)"
                    strReturn = vstrDetaVariante & " TWIN"
                End If

            Case 3
                strReturn = vstrDetaVariante & " TRIPLE"

        End Select

        Return strReturn
    End Function

    Private Function strDevuelveMonedaxNuviaticoTC(ByVal vintNuViaticoTC As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strIDMoneda As String = String.Empty
            dc.Add("@NuViaticoTC", vintNuViaticoTC)
            dc.Add("@pIDMoneda", strIDMoneda)

            strIDMoneda = objADO.GetSPOutputValue("VIATICOS_TOURCONDUCTOR_SelIDMonedaxNuViaticoTC", dc)
            Return strIDMoneda
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExistenPaxSoloPeruanosReservas(ByVal vintIDReservas_Det As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExistenPeruanos As Boolean = False
            dc.Add("@IDReservas_Det", vintIDReservas_Det)
            dc.Add("@pSoloPeruano", False)
            blnExistenPeruanos = objADO.GetSPOutputValue("RESERVAS_DET_PAX_SoloPeruanos", dc)

            Return blnExistenPeruanos
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Overloads Function blnExistenPaxSoloPeruanosVentas(ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExistenPeruanos As Boolean = False
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pSoloPer", False)
            blnExistenPeruanos = objADO.GetSPOutputValue("COTIDET_PAX_SoloPeruanos", dc)

            Return blnExistenPeruanos
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Overloads Function blnExistenPaxSoloPeruanosVentas(ByVal vintIDDet As Integer, ByVal vintCapacidadHab As Int16, _
                                                               ByVal vblnEsMatrimonial As Boolean) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExistenPeruanos As Boolean = False
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@CapacidadHab", vintCapacidadHab)
            dc.Add("@EsMatrimonial", vblnEsMatrimonial)
            dc.Add("@pSoloPeruano", False)
            blnExistenPeruanos = objADO.GetSPOutputValue("COTIDET_ACOMODOESPECIAL_DISTRIBUC_PAX_SoloPeruanos", dc)

            Return blnExistenPeruanos
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteAcomodoEspecial(ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnAcomodosEspeciales As Boolean = False
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pAcomodoEspecial", False)

            blnAcomodosEspeciales = objADO.GetSPOutputValue("COTIDET_Sel_ExistsAcomodoEspecial", dc)
            Return blnAcomodosEspeciales
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub PasarDatosPropiedadesaCostosDetReservasBE(ByRef robjBEDetRes As clsReservaBE.clsDetalleReservaBE, _
                                                        ByVal vbytSimple As Int16, ByVal vbytTwin As Int16, _
                                                        ByVal vbytMat As Int16, ByVal vbytTriple As Int16, _
                                                        ByVal vintCuadruple As Int16, ByVal vintQuintuple As Int16, _
                                                        ByVal vintSextuple As Int16, ByVal vintSeptuple As Int16, _
                                                        ByVal vintOctuple As Int16, _
                                                        ByVal vsglTCambio As Single, _
                                                        ByVal vListDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE), _
                                                        Optional ByVal vblnRecalculo As Boolean = False, _
                                                        Optional ByVal vsglCantidadAPagar As Single = 0, _
                                                        Optional ByVal vdblNetoHab As Double = 0, _
                                                        Optional ByVal vsglIgvHab As Single = 0, _
                                                        Optional ByVal vListDetRes As List(Of clsReservaBE.clsDetalleReservaBE) = Nothing, _
                                                        Optional ByVal vblnCapacMaxMayorNroPax As Boolean = False, _
                                                        Optional ByVal BECot As clsCotizacionBE = Nothing, _
                                                        Optional ByVal vblnCopiaGuia As Boolean = False)

        'Try
        '    With robjBEDetRes
        '        'Dim objDetServ As New clsServicioProveedorBN.clsDetalleServicioProveedorBN

        '        Dim objDetServBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT


        '        Dim dblMontoSgl As Double = 0
        '        Dim dblMontoDbl As Double = 0
        '        Dim dblMontoTri As Double = 0
        '        Dim blnAfecto As Boolean = False
        '        Dim strTipoLib As String = "" 'Habit, o Pax
        '        Dim bytLiberado As Byte = 0
        '        Dim bytMaxLiberado As Byte = 0
        '        Dim strIDMoneda As String = "USD"
        '        Dim strSimboloMoneda As String = "US$"
        '        Dim sglTCambio As Single = 0
        '        'If .IDDet > 0 And .FlServicioTC Then
        '        '    Dim nint As Integer = 0
        '        'End If
        '        'If .IDServicio_Det = 41566 Then
        '        '    Dim nint As Integer = 0
        '        'End If

        '        Dim dtt As DataTable = objDetServBT.ConsultarPk(.IDServicio_Det)
        '        If dtt.Rows.Count > 0 Then
        '            'dr.Read()
        '            'dblMontoSgl = If(IsDBNull(dr("Monto_sgl")) = True, 0, dr("Monto_sgl"))
        '            'dblMontoDbl = If(IsDBNull(dr("Monto_dbl")) = True, 0, dr("Monto_dbl"))
        '            'dblMontoTri = If(IsDBNull(dr("Monto_tri")) = True, 0, dr("Monto_tri"))
        '            'blnAfecto = dr("Afecto")
        '            'strTipoLib = If(IsDBNull(dr("TipoLib")) = True, "", dr("TipoLib"))
        '            'bytLiberado = If(IsDBNull(dr("Liberado")) = True, 0, dr("Liberado"))

        '            dblMontoSgl = If(IsDBNull(dtt(0)("Monto_sgl")) = True, 0, dtt(0)("Monto_sgl"))
        '            dblMontoDbl = If(IsDBNull(dtt(0)("Monto_dbl")) = True, 0, dtt(0)("Monto_dbl"))
        '            dblMontoTri = If(IsDBNull(dtt(0)("Monto_tri")) = True, 0, dtt(0)("Monto_tri"))

        '            sglTCambio = If(IsDBNull(dtt(0)("TC")) = True, 0, dtt(0)("TC"))
        '            'sglTCambio = vsglTCambio

        '            Dim objProvBN As New clsProveedorBN
        '            'If sglTCambio > 0 And .IDProveedor <> gstrIDProveedorSetoursLima And .IDProveedor <> gstrIDProveedorSetoursCusco Then 'soles
        '            If sglTCambio > 0 And Not objProvBN.blnProveedorInterno(.IDProveedor) Then 'soles
        '                dblMontoSgl = If(IsDBNull(dtt(0)("Monto_sgls")) = True, 0, dtt(0)("Monto_sgls"))
        '                dblMontoDbl = If(IsDBNull(dtt(0)("Monto_dbls")) = True, 0, dtt(0)("Monto_dbls"))
        '                dblMontoTri = If(IsDBNull(dtt(0)("Monto_tris")) = True, 0, dtt(0)("Monto_tris"))
        '                'strIDMoneda = "SOL"
        '                'strSimboloMoneda = "S/."
        '                strIDMoneda = dtt(0)("CoMoneda")
        '                strSimboloMoneda = dtt(0)("SimboloMonedaPais")
        '                If Not .CostoRealEditado Then .CostoReal = dblMontoSgl / sglTCambio
        '            End If
        '            If sglTCambio > 0 Then
        '                strIDMoneda = dtt(0)("CoMoneda")
        '                strSimboloMoneda = dtt(0)("SimboloMonedaPais")
        '            End If

        '            If robjBEDetRes.NuViaticosTC > 0 Then
        '                'strIDMoneda = robjBEDetRes.IDMoneda
        '                strIDMoneda = strDevuelveMonedaxNuviaticoTC(robjBEDetRes.NuViaticosTC)
        '                robjBEDetRes.IDMoneda = strIDMoneda
        '                If robjBEDetRes.IDMoneda = "USD" Then
        '                    strSimboloMoneda = "US$"
        '                ElseIf robjBEDetRes.IDMoneda = "SOL" Then
        '                    strSimboloMoneda = "S/."
        '                End If
        '            End If
        '            ''''''''Jorge'''''''

        '            If dblMontoSgl = 0 And Not .CostoRealEditado Then

        '                Dim objDetCostosServ As New clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsCostosDetalleServicioProveedorBN
        '                Dim dttCostos As DataTable = objDetCostosServ.ConsultarListxIDDetalle(.IDServicio_Det, Nothing)

        '                Dim intPax As Integer = .NroPax 'prueba
        '                Dim dblCostoReal As Double = 0 '.CostoReal
        '                Dim dblCostoAbsoluto As Double = 0
        '                Dim intLiberados As Integer = .NroLiberados

        '                Dim intPaxHastaMax As Int16 = 0
        '                Dim dblCostoRealTemp As Double
        '                Dim dblCostoAbsolutoTemp As Double
        '                For Each drCostos As DataRow In dttCostos.Rows
        '                    If intPax >= drCostos("PaxDesde") And intPax <= drCostos("PaxHasta") Then
        '                        dblCostoReal = drCostos("Monto") / intPax
        '                        If intLiberados > 0 Then
        '                            dblCostoAbsoluto = drCostos("Monto")
        '                        End If

        '                        Exit For
        '                    End If

        '                    If drCostos("PaxHasta") > intPaxHastaMax Then
        '                        intPaxHastaMax = drCostos("PaxHasta")
        '                    End If
        '                    If intPax = 0 Then
        '                        dblCostoRealTemp = drCostos("Monto") '/ vintPax
        '                    Else
        '                        dblCostoRealTemp = drCostos("Monto") / intPax
        '                    End If

        '                    If intLiberados > 0 Then dblCostoAbsolutoTemp = drCostos("Monto")
        '                Next

        '                If dblCostoReal = 0 And intPaxHastaMax >= 40 Then
        '                    dblCostoReal = dblCostoRealTemp
        '                    dblCostoAbsoluto = dblCostoAbsolutoTemp
        '                End If

        '                If sglTCambio > 0 Then
        '                    dblCostoReal = dblCostoReal / sglTCambio
        '                End If
        '                If .NuViaticosTC = 0 Then
        '                    .CostoReal = dblCostoReal
        '                    dblMontoSgl = dblCostoReal
        '                End If
        '            End If
        '            ''''''''Jorge'''''''

        '            blnAfecto = dtt(0)("Afecto")
        '            strTipoLib = If(IsDBNull(dtt(0)("TipoLib")) = True, "", dtt(0)("TipoLib"))
        '            bytLiberado = If(IsDBNull(dtt(0)("Liberado")) = True, 0, dtt(0)("Liberado"))
        '            bytMaxLiberado = If(IsDBNull(dtt(0)("MaximoLiberado")) = True, 0, dtt(0)("MaximoLiberado"))
        '        End If
        '        'dr.Close()
        '        Dim dblMonto As Double = 0

        '        If .IDTipoProv = gstrTipoProvHotel And .PlanAlimenticio = False Then
        '            'Si el Monto es Editado desde venta.. Entonces se debe considerar todos los montos ingresados.'>>Nuevo'
        '            Dim blnCostoRealEdit As Boolean = .CostoRealEditado
        '            If blnCostoRealEdit And .CostoReal >= 0 Then '>>Nuevo'
        '                dblMontoSgl = .CostoReal + .SSCR ''>>Nuevo'
        '                dblMontoDbl = (.CostoReal) * bytDevuelveCapacidadxCadena(.Servicio) '>>Nuevo'
        '                dblMontoTri = (.CostoReal + .STCR) * bytDevuelveCapacidadxCadena(.Servicio) ''>>Nuevo'
        '                If sglTCambio > 0 Then
        '                    dblMontoSgl = (.CostoReal + .SSCR) * sglTCambio ''>>Nuevo'
        '                    dblMontoDbl = .CostoReal * sglTCambio '>>Nuevo'
        '                    dblMontoTri = (.CostoReal + .STCR) * sglTCambio ''>>Nuevo'
        '                End If
        '            End If

        '            If .CapacidadHab = 1 Then
        '                dblMonto = dblMontoSgl
        '            ElseIf .CapacidadHab = 2 Then
        '                dblMonto = dblMontoDbl
        '            ElseIf .CapacidadHab = 3 Then
        '                dblMonto = dblMontoTri
        '            ElseIf .CapacidadHab = 4 Then
        '                dblMonto = dblMontoTri
        '            ElseIf .CapacidadHab = 5 Then
        '                dblMonto = dblMontoTri
        '            ElseIf .CapacidadHab = 6 Then
        '                dblMonto = dblMontoTri
        '            ElseIf .CapacidadHab = 7 Then
        '                dblMonto = dblMontoTri
        '            ElseIf .CapacidadHab = 8 Then
        '                dblMonto = dblMontoTri
        '            End If

        '            Dim intCantAPagar As Single = 0
        '            If Not vblnRecalculo Then
        '                .NetoHab = dblMonto '+ .CostoLiberado
        '                .IgvHab = 0
        '                Dim blnTodosPeruanos As Boolean = False
        '                If .IDDet.ToString() <> "" Then
        '                    'blnTodosPeruanos = blnExistenPaxSoloPeruanosVentas(.IDDet)
        '                    If .IDReserva_Det <> 0 Then
        '                        blnTodosPeruanos = blnExistenPaxSoloPeruanosReservas(.IDReserva_Det)
        '                    Else
        '                        If blnExisteAcomodoEspecial(.IDDet) Then
        '                            blnTodosPeruanos = blnExistenPaxSoloPeruanosVentas(.IDDet, .CapacidadHab, .EsMatrimonial)
        '                        Else
        '                            blnTodosPeruanos = blnExistenPaxSoloPeruanosVentas(.IDDet)
        '                        End If
        '                    End If

        '                    '' ''Evaluación por Pax segmentados por acomodos
        '                    ' ''If Not blnTodosPeruanos And .IDReserva_Det <> 0 Then
        '                    ' ''    blnTodosPeruanos = blnExistenPaxSoloPeruanosReservas(.IDReserva_Det)
        '                    ' ''Else
        '                    ' ''    If .IDReserva_Det Is Nothing Then
        '                    ' ''        blnTodosPeruanos = blnExistenPaxSoloPeruanosVentas(.IDDet, .CapacidadHab, .EsMatrimonial)
        '                    ' ''    End If
        '                    ' ''End If
        '                End If

        '                If blnAfecto Or .IncGuia Or blnTodosPeruanos Then
        '                    .IgvHab = .NetoHab * (gsglIGV / 100)
        '                End If



        '                If .CapacidadHab = 1 Then
        '                    intCantAPagar = vbytSimple
        '                ElseIf .CapacidadHab = 2 Then
        '                    If .EsMatrimonial = True Then
        '                        intCantAPagar = vbytMat
        '                    Else
        '                        intCantAPagar = vbytTwin
        '                    End If
        '                ElseIf .CapacidadHab = 3 Then
        '                    intCantAPagar = vbytTriple

        '                ElseIf .CapacidadHab = 4 Then
        '                    intCantAPagar = vintCuadruple
        '                ElseIf .CapacidadHab = 5 Then
        '                    intCantAPagar = vintQuintuple
        '                ElseIf .CapacidadHab = 6 Then
        '                    intCantAPagar = vintSextuple
        '                ElseIf .CapacidadHab = 7 Then
        '                    intCantAPagar = vintSeptuple
        '                ElseIf .CapacidadHab = 8 Then
        '                    intCantAPagar = vintOctuple

        '                End If
        '                If vblnCapacMaxMayorNroPax And intCantAPagar = 0 Then
        '                    intCantAPagar = .CapacidadHab
        '                End If

        '                .TotalHab = .NetoHab + .IgvHab

        '                'Politica

        '                If strTipoLib = "H" Then
        '                    ' ''Los Liberados de Habitación se consideran por el Grupo de habitaciones dependiendo del acomodo
        '                    ' ''Se considera después de haber generado los acomodos
        '                    ''Dim bytTotalHabit As Byte = vbytSimple + vbytTwin + vbytMat + vbytTriple
        '                    ''If bytTotalHabit > bytLiberado Then
        '                    ''    'Dim bytRestar As Byte = Convert.ToByte(bytTotalHabit / bytLiberado)
        '                    ''    'If bytRestar >= bytMaxLiberado And bytMaxLiberado <> 0 Then
        '                    ''    '    bytRestar = bytMaxLiberado
        '                    ''    'End If
        '                    ''    'intCantAPagar -= bytRestar

        '                    ''    Dim sglRestar As Single = bytTotalHabit / bytLiberado
        '                    ''    If sglRestar >= bytMaxLiberado And bytMaxLiberado <> 0 Then
        '                    ''        sglRestar = bytMaxLiberado
        '                    ''    End If
        '                    ''    intCantAPagar -= sglRestar
        '                    ''End If
        '                ElseIf strTipoLib = "P" Then
        '                    'Dim blnEjecutarPoliticaLiberados As Boolean = False

        '                    'Dim bytCapacidadTriple As Byte = If(vbytTriple > 0, 3, 0)
        '                    'Dim bytCapacidadDoble As Byte = If(vbytTwin > 0 Or vbytMat > 0, 2, 0)
        '                    'Dim bytCapacidadSimple As Byte = If(vbytSimple > 0, 1, 0)
        '                    'Dim bytCapacidadMenor As Byte = 3
        '                    'If bytCapacidadDoble < bytCapacidadMenor Then
        '                    '    bytCapacidadMenor = bytCapacidadDoble
        '                    'End If
        '                    'If bytCapacidadSimple < bytCapacidadMenor Then
        '                    '    bytCapacidadMenor = bytCapacidadSimple
        '                    'End If

        '                    'If .CapacidadHab = 2 And (vbytTwin > 0 And vbytMat > 0) Then
        '                    '    If bytCapacidadMenor = .CapacidadHab And .EsMatrimonial = True Then
        '                    '        blnEjecutarPoliticaLiberados = True
        '                    '    End If
        '                    'Else
        '                    'If bytCapacidadMenor = .CapacidadHab Then
        '                    '    blnEjecutarPoliticaLiberados = True
        '                    'End If
        '                    'End If

        '                    'If .CapacidadHab = 1 Then
        '                    '    blnEjecutarPoliticaLiberados = True
        '                    'End If

        '                    'If blnEjecutarPoliticaLiberados Then

        '                    Dim intNroPaxLib As Int16 = 0
        '                    If Not vListDetCot Is Nothing Then
        '                        For Each ItemDetCot As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCot
        '                            If ItemDetCot.IDDet = robjBEDetRes.IDDet Then
        '                                intNroPaxLib = ItemDetCot.NroPax + ItemDetCot.NroLiberados
        '                                Exit For
        '                            End If
        '                        Next
        '                    Else
        '                        For Each ItemDetCot As clsReservaBE.clsDetalleReservaBE In vListDetRes
        '                            If ItemDetCot.IDDet = robjBEDetRes.IDDet Then
        '                                intNroPaxLib = ItemDetCot.NroPax + ItemDetCot.NroLiberados
        '                                Exit For
        '                            End If
        '                        Next

        '                    End If

        '                    bytLiberado += bytMaxLiberado
        '                    If intNroPaxLib >= bytLiberado Then
        '                        'If (.NroPax) >= bytLiberado Then
        '                        Dim sglRestar As Single = (intNroPaxLib / bytLiberado)
        '                        Dim bytRestar As Byte
        '                        If InStr(sglRestar.ToString, ".") > 0 Then
        '                            bytRestar = Left(sglRestar.ToString, InStr(sglRestar.ToString, ".") - 1)
        '                        Else
        '                            bytRestar = sglRestar
        '                        End If

        '                        If bytRestar >= bytMaxLiberado And bytMaxLiberado <> 0 Then
        '                            bytRestar = bytMaxLiberado
        '                        End If
        '                        If .CapacidadHab = 2 And (vbytTwin > 0 And vbytMat > 0) Then

        '                            If .CapacidadHab = bytRestar And .EsMatrimonial = False Then
        '                                'intCantAPagar -= bytRestar / .CapacidadHab
        '                            End If

        '                        ElseIf .CapacidadHab = 1 Then

        '                            If .CapacidadHab = bytRestar Then

        '                                'intCantAPagar -= bytRestar / .CapacidadHab
        '                            End If

        '                        End If
        '                    End If
        '                    'End If
        '                Else
        '                    If (intCantAPagar > .Cantidad And .Cantidad > 0) Then
        '                        intCantAPagar = .Cantidad
        '                    End If
        '                End If

        '            Else
        '                .NetoHab = vdblNetoHab
        '                .IgvHab = vsglIgvHab
        '                If (.FlServicioNoShow And .IDPais = gstrPeru And vsglIgvHab <> 0) Then
        '                    .IgvHab = .NetoHab * (gsglIGV / 100)
        '                End If
        '                intCantAPagar = vsglCantidadAPagar
        '                .TotalHab = .NetoHab + .IgvHab
        '            End If

        '            If .IncGuia And Not vblnRecalculo Then
        '                intCantAPagar = 1
        '            End If

        '            Dim blnTodosPeruanos2 As Boolean = False
        '            If .IDDet.ToString() <> "" Then
        '                If .IDReserva_Det <> 0 Then
        '                    blnTodosPeruanos2 = blnExistenPaxSoloPeruanosReservas(.IDReserva_Det)
        '                Else
        '                    If blnExisteAcomodoEspecial(.IDDet) Then
        '                        blnTodosPeruanos2 = blnExistenPaxSoloPeruanosVentas(.IDDet, .CapacidadHab, .EsMatrimonial)
        '                    Else
        '                        blnTodosPeruanos2 = blnExistenPaxSoloPeruanosVentas(.IDDet)
        '                    End If
        '                End If

        '                ''blnTodosPeruanos2 = blnExistenPaxSoloPeruanosVentas(.IDDet)
        '                ' ''If Not blnTodosPeruanos2 And .IDReserva_Det <> 0 Then blnTodosPeruanos2 = blnExistenPaxSoloPeruanosReservas(.IDReserva_Det)
        '                ' ''Evaluación por Pax segmentados por acomodos
        '                ''If Not blnTodosPeruanos2 And .IDReserva_Det <> 0 Then
        '                ''    blnTodosPeruanos2 = blnExistenPaxSoloPeruanosReservas(.IDReserva_Det)
        '                ''Else
        '                ''    If .IDReserva_Det Is Nothing Then
        '                ''        blnTodosPeruanos2 = blnExistenPaxSoloPeruanosVentas(.IDDet, .CapacidadHab, .EsMatrimonial)
        '                ''    End If
        '                ''End If
        '            End If

        '            .NetoGen = .NetoHab * intCantAPagar * .Noches
        '            .IgvGen = 0
        '            If (blnAfecto Or .IncGuia = True) Or (.FlServicioNoShow And .IDPais = gstrPeru And vsglIgvHab <> 0) Or blnTodosPeruanos2 Then
        '                .IgvGen = .IgvHab * intCantAPagar * .Noches
        '            End If
        '            .TotalGen = .NetoGen + .IgvGen

        '            .CantidadAPagar = intCantAPagar
        '        Else
        '            'If .IDServicio_Det = 56705 Or .IDServicio_Det = 56705 Then
        '            '    Dim na As Integer = 0
        '            'End If
        '            'Se consideran todos los pax para evaluar si se libera uno de ellos. :Cambio:28/06/2014 ---- 29.12.2014
        '            Dim intCantAPagar As Int16 = .NroPax + .NroLiberados
        '            If Not (.IDTipoProv = gstrTipoProvOper And .ConAlojamiento) Then .Cantidad = intCantAPagar
        '            Dim dcLiberados As New Dictionary(Of Integer, Byte)
        '            If Not vblnRecalculo Then
        '                'Regla de Definicion por Restaurantes con servicio Guía
        '                '.NetoHab = .CostoReal
        '                'If .IDTipoProv = gstrTipoProvRestaurante And .IncGuia = True And Not .CostoRealEditado Then
        '                '    .NetoHab = dblMontoSgl
        '                'ElseIf .IDTipoProv = gstrTipoProvOper And Not .CostoRealEditado Then
        '                '    If .ConAlojamiento = True Then
        '                '        .CostoReal = dblCostoRealSubServiciosParaPropiedadesDetReservasBE(.IDServicio_Det, robjBEDetRes, BECot)
        '                '    Else
        '                '        .NetoHab = dblNetoPaxSumaSubServiciosParaPropiedadesDetReservasBE(.IDDet, .IDServicio_Det, intCantAPagar, _
        '                '                                                                          strIDMoneda, .IgvHab)
        '                '    End If
        '                'else
        '                '   .NetoHab = .CostoReal
        '                'End If

        '                Dim blnRangoCostos As Boolean = False
        '                If .IDTipoProv = gstrTipoProvOper And .ConAlojamiento Then
        '                    blnRangoCostos = blnExistePorRangoDeCostos(.IDServicio_Det)
        '                End If

        '                Dim blnEsHotelPlanAlimenticio As Boolean = False
        '                If .IDTipoProv = gstrTipoProvHotel And .PlanAlimenticio = True Then blnEsHotelPlanAlimenticio = True
        '                'Dim blnAplicarTarifa As Boolean = True
        '                If Not .CostoRealEditado Then
        '                    If vblnCopiaGuia Then
        '                        .NetoHab = dblMontoSgl
        '                    Else
        '                        If .NuViaticosTC > 0 Then
        '                            .NetoHab = .CostoReal
        '                        Else
        '                            .NetoHab = dblNetoPaxSumaSubServiciosParaPropiedadesDetReservasBE(.IDDet, If(.IDTipoOper = gstrIDTipoOperInterno, 0, .IDServicio_Det), intCantAPagar, strIDMoneda, .IgvHab, _
        '                                                                                          dcLiberados, .Servicio, blnEsHotelPlanAlimenticio, blnRangoCostos, .IncGuia)
        '                            If dcLiberados.ContainsKey(.IDServicio_Det) And Not blnRangoCostos Then intCantAPagar -= dcLiberados.Item(.IDServicio_Det)
        '                        End If
        '                    End If
        '                Else
        '                    .NetoHab = .CostoReal
        '                    If .ConAlojamiento = True And .Dias > 1 And Not vblnCopiaGuia Then
        '                        If .CapacidadHab = 1 Then
        '                            .NetoHab = .CostoReal + .SSCR
        '                        ElseIf .CapacidadHab = 2 Then
        '                            .NetoHab = .CostoReal
        '                        Else 'If .CapacidadHab = 3 Then
        '                            .NetoHab = .CostoReal + .STCR
        '                        End If
        '                    End If
        '                End If

        '                'If (.ConAlojamiento = True And .Dias > 1 And (.CostoRealEditado And Not vblnCopiaGuia)) Then
        '                '    If .CapacidadHab = 1 Then
        '                '        .NetoHab = .CostoReal + .SSCR
        '                '    ElseIf .CapacidadHab = 2 Then
        '                '        .NetoHab = .CostoReal * 2
        '                '    ElseIf .CapacidadHab = 3 Then
        '                '        .NetoHab = (.CostoReal + .STCR) * 3
        '                '    ElseIf .CapacidadHab = 4 Then
        '                '        .NetoHab = (.CostoReal + .STCR) * 3
        '                '    ElseIf .CapacidadHab = 5 Then
        '                '        .NetoHab = (.CostoReal + .STCR) * 3
        '                '    ElseIf .CapacidadHab = 6 Then
        '                '        .NetoHab = (.CostoReal + .STCR) * 3
        '                '    ElseIf .CapacidadHab = 7 Then
        '                '        .NetoHab = (.CostoReal + .STCR) * 3
        '                '    ElseIf .CapacidadHab = 8 Then
        '                '        .NetoHab = (.CostoReal + .STCR) * 3
        '                '    End If
        '                'End If

        '                If sglTCambio > 0 And .IDProveedor <> gstrIDProveedorSetoursLima And .IDProveedor <> gstrIDProveedorSetoursCusco _
        '                    And .CostoRealEditado Then 'soles
        '                    .NetoHab = .NetoHab * sglTCambio
        '                End If

        '                If (.NetoHab = 0 And (.IDDetRel.Trim() = "0" Or .IDDetRel.Trim() = "")) And Not .CostoRealEditado And blnEsCostoEditadoxCero(.IDDet) Then
        '                    .NetoHab = dblMontoSgl
        '                    If blnAfecto Or .IncGuia Then
        '                        .IgvHab = .NetoHab * (gsglIGV / 100)
        '                    End If
        '                End If

        '                '.IgvHab = 0
        '                If ((blnAfecto Or .IncGuia = True) And (.CostoRealEditado Or vblnCopiaGuia)) Then
        '                    .IgvHab = .NetoHab * (gsglIGV / 100)
        '                Else
        '                    If (.IDTipoProv = gstrTipoProvHotel And .PlanAlimenticio = True) And (.IncGuia = True Or vblnCopiaGuia) Then
        '                        .IgvHab = .NetoHab * (gsglIGV / 100)
        '                    End If
        '                End If

        '                .TotalHab = .NetoHab + .IgvHab


        '                'Dim intCantAPagar As Int16 = .NroPax - .NroLiberados
        '                'If .PlanAlimenticio = True Or .IDProveedor = gstrIDProveedorPeruRail Then
        '                If .IDProveedor = gstrIDProveedorPeruRail Then
        '                    intCantAPagar = .NroPax + .NroLiberados
        '                ElseIf .ConAlojamiento = True And .Dias > 1 Then
        '                    'intCantAPagar = .Dias - 1
        '                    '.Cantidad = intCantAPagar
        '                    If Not blnRangoCostos And Not .CostoRealEditado Then
        '                        intCantAPagar = .Cantidad
        '                    End If
        '                End If

        '                'Politica
        '                If strTipoLib = "P" And (.CostoRealEditado Or vblnCopiaGuia) Then
        '                    If (.NroPax + .NroLiberados) > bytLiberado Then
        '                        'If (.NroPax) >= bytLiberado Then
        '                        Dim sglRestar As Single = (.NroPax + .NroLiberados) / bytLiberado
        '                        'Dim bytRestar As Byte = Left(sglRestar.ToString, InStr(sglRestar.ToString, ".") - 1)
        '                        Dim bytRestar As Byte
        '                        If InStr(sglRestar.ToString, ".") > 0 Then
        '                            bytRestar = Left(sglRestar.ToString, InStr(sglRestar.ToString, ".") - 1)
        '                        Else
        '                            bytRestar = sglRestar
        '                        End If

        '                        If bytRestar >= bytMaxLiberado And bytMaxLiberado <> 0 Then
        '                            bytRestar = bytMaxLiberado
        '                        End If
        '                        intCantAPagar -= bytRestar
        '                    End If
        '                End If
        '                ''''''


        '                If .ConAlojamiento = True And .Dias > 1 And (.CostoRealEditado Or vblnCopiaGuia) Then
        '                    '.NetoHab = .SSCR * intCantAPagar
        '                    If blnAfecto Or .IncGuia = True Then
        '                        .IgvHab = .NetoHab * (gsglIGV / 100)
        '                    End If
        '                    .TotalHab = .NetoHab + .IgvHab
        '                End If
        '            Else
        '                .NetoHab = vdblNetoHab
        '                .IgvHab = vsglIgvHab
        '                intCantAPagar = vsglCantidadAPagar
        '                .TotalHab = .NetoHab + .IgvHab
        '            End If

        '            If .IncGuia And Not vblnRecalculo Then
        '                intCantAPagar = 1
        '            End If

        '            .NetoGen = .NetoHab * intCantAPagar

        '            .IgvGen = .IgvHab * intCantAPagar
        '            If (blnAfecto Or .IncGuia = True) And (.CostoRealEditado Or vblnCopiaGuia) Then
        '                '.IgvGen = .NetoGen * (gsglIGV / 100)
        '                .IgvGen = .IgvHab * intCantAPagar
        '            End If
        '            If .NuViaticosTC > 0 Then
        '                .TotalGen = Math.Round(.NetoGen + .IgvGen, 0)
        '            Else
        '                .TotalGen = .NetoGen + .IgvGen
        '            End If

        '            .CantidadAPagar = intCantAPagar
        '            If .EsTransporteAutogenerado And Not vblnRecalculo Then
        '                .CantidadAPagar = .NroPax + .NroLiberados
        '            End If

        '        End If

        '        .IDMoneda = strIDMoneda
        '        .SimboloMoneda = strSimboloMoneda

        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Function blnEsCostoEditadoxCero(ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pCostoDiferenteCero", False)

            Return objADO.GetSPOutputValue("COTIDET_SelExisteCostoDifCero", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExistePorRangoDeCostos(ByVal vintIDServicio_Det As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@pExistenRango", False)

            Return objADO.GetSPOutputValue("MASERVICIOS_DET_Sel_ExistsCostos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function bytDevuelveCapacidadxCadena(ByVal vstrCadenaDescrip As String) As Byte
        Try
            Dim bytReturn As Byte = 0
            If InStr(vstrCadenaDescrip, "SINGLE") > 0 Then
                bytReturn = 1
            ElseIf InStr(vstrCadenaDescrip, "DOUBLE (MAT)") > 0 Or InStr(vstrCadenaDescrip, "TWIN") > 0 Then
                bytReturn = 2
                If InStr(vstrCadenaDescrip, "TWIN + (EXCHILD)") > 0 Then
                    bytReturn = 3
                End If
            ElseIf InStr(vstrCadenaDescrip, "TRIPLE") > 0 Then
                bytReturn = 3
            ElseIf InStr(vstrCadenaDescrip, "CUADRUPLE") > 0 Then
                bytReturn = 4
            ElseIf InStr(vstrCadenaDescrip, "QUINTUPLE") > 0 Then
                bytReturn = 5
            ElseIf InStr(vstrCadenaDescrip, "SEXTUPLE") > 0 Then
                bytReturn = 6
            ElseIf InStr(vstrCadenaDescrip, "SEPTUPLE") > 0 Then
                bytReturn = 7
            ElseIf InStr(vstrCadenaDescrip, "OCTUPLE") > 0 Then
                bytReturn = 8
            End If
            Return bytReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function intDevuelveNroPaxxIDDet(ByVal vintIDDet As Integer) As Int16
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pNroPax", 0)

            Return objADO.GetSPOutputValue("COTIDET_SelNroPaxOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblNetoPaxSumaSubServiciosParaPropiedadesDetReservasBE(ByVal vintIDDet As Integer, ByVal vintIDServicios_Det As Integer, ByVal vbytNroCantidadaPagar As Byte, _
                                                                            ByVal vstrMonedaDev As String, ByRef vdblIgvNeto As Double, ByRef dcLiberados As Dictionary(Of Integer, Byte), _
                                                                            ByVal vstrServicio As String, ByVal vblnHotelPlanAlimenticio As Boolean, ByVal vblnEsOperadorConRango As Boolean, _
                                                                            ByVal vblnServicioGuias As Boolean) As Double
        Try
            Dim dblMonto As Double = 0, dblIgv As Double = 0
            Dim tmpCantidadAPagar As Byte = vbytNroCantidadaPagar
            Dim intCantPaxCotiDet As Int16 = intDevuelveNroPaxxIDDet(vintIDDet)
            If vblnEsOperadorConRango Then
                tmpCantidadAPagar = intCantPaxCotiDet
            End If

            Dim dtDatos As DataTable = Nothing
            Try
                If Not vblnHotelPlanAlimenticio Then
                    dtDatos = ConsultarDetServiciosTodosxIDDet(vintIDDet, vintIDServicios_Det, tmpCantidadAPagar, vstrMonedaDev)
                Else
                    dtDatos = ConsultarDetTodosxIDDet(vintIDDet, tmpCantidadAPagar, vstrMonedaDev)
                End If
            Catch ex As Exception
                Throw
            End Try

            For Each dr As DataRow In dtDatos.Rows
                Dim dblMontoSumar As Double = 0
                If Convert.ToBoolean(dr("EsCostoTC")) Then
                    dblMontoSumar = Convert.ToDouble(dr("MontoTourConductor"))
                Else
                    If Convert.ToBoolean(dr("PoliticaLiberado")) Then
                        If tmpCantidadAPagar > Convert.ToByte(dr("CantMinLiberada")) Then
                            If dr("TipoLib").ToString = "P" Then
                                If Convert.ToByte(dr("CantMinLiberada")) > 0 Then
                                    Dim bytCantidadDism As Byte = 0
                                    If Convert.ToByte(dr("CantidadLiberada")) > 0 Then
                                        bytCantidadDism = Convert.ToByte(dr("CantidadLiberada"))
                                    Else
                                        'bytCantidadDism = tmpCantidadAPagar \ Convert.ToByte(dr("CantMinLiberada"))
                                        bytCantidadDism = tmpCantidadAPagar \ (Convert.ToByte(dr("CantMinLiberada")) + 1)
                                    End If
                                    tmpCantidadAPagar -= bytCantidadDism
                                    If Not dcLiberados.ContainsKey(dr("IDServicio_Det")) Then dcLiberados.Add(dr("IDServicio_Det"), bytCantidadDism)
                                ElseIf Convert.ToDouble(dr("MontoLiberado")) > 0 Then
                                    dblMontoSumar -= Convert.ToDouble(dr("MontoLiberado"))
                                End If
                            End If
                        End If
                    End If

                    If Convert.ToDouble(dr("MontoCostoReal")) > 0 Then
                        If tmpCantidadAPagar <> vbytNroCantidadaPagar Then
                            If tmpCantidadAPagar > 0 Then
                                dblMontoSumar = dblConsultarCostoxNuevoPaxLiberado(dr("IDServicio_Det"), tmpCantidadAPagar) / tmpCantidadAPagar
                                If tmpCantidadAPagar = 1 Then
                                    dblMontoSumar = dblConsultarCostoxNuevoPaxLiberado(dr("IDServicio_Det"), tmpCantidadAPagar) / intConsultarPaxHastaxNuevoPaxLiberado(dr("IDServicio_Det"), tmpCantidadAPagar)
                                End If
                            End If
                        Else
                            If tmpCantidadAPagar > 0 Then
                                dblMontoSumar = Convert.ToDouble(dr("MontoCostoReal")) / tmpCantidadAPagar
                                If tmpCantidadAPagar = 1 Then
                                    'dblMontoSumar = Convert.ToDouble(dr("MontoCostoReal")) / Convert.ToInt16(dr("PaxHastaCostoReal"))
                                    If vblnServicioGuias Then
                                        dblMontoSumar = dblConsultarCostoxNuevoPaxLiberado(dr("IDServicio_Det"), Convert.ToInt16(dr("NroPaxCoti"))) '/ Convert.ToInt16(dr("NroPaxCoti"))
                                    End If
                                End If
                            End If
                        End If
                    ElseIf Convert.ToDouble(dr("MontoSegunTC")) > 0 Then
                        dblMontoSumar = Convert.ToDouble(dr("MontoSegunTC"))
                    Else
                        dblMontoSumar = Convert.ToDouble(dr("MontoReal"))
                    End If

                    If (dr("CoMoneda") = "ARS" Or dr("CoMoneda") = "CLP" Or dr("CoMoneda") = "SOL") And vstrMonedaDev = "USD" Then
                        dblMontoSumar = dblMontoSumar / Convert.ToDouble(dr("TC"))
                    ElseIf vstrMonedaDev <> "USD" And dr("CoMoneda") = "USD" Then
                        dblMontoSumar = dblMontoSumar * Convert.ToDouble(dr("TC"))
                    End If
                    If bytDevuelveCapacidadxCadena(vstrServicio) > 0 Then
                        If vblnEsOperadorConRango Then
                            dblMontoSumar = dblMontoSumar
                        Else
                            dblMontoSumar = dblMontoSumar * bytDevuelveCapacidadxCadena(vstrServicio)
                        End If


                        If bytDevuelveCapacidadxCadena(vstrServicio) = 1 Then dblMontoSumar += Convert.ToDouble(dr("DiferSS"))
                        If bytDevuelveCapacidadxCadena(vstrServicio) >= 3 Then dblMontoSumar += (Convert.ToDouble(dr("DiferST")) * 3)
                    End If
                End If


                Dim dblIgvSbItem As Double = 0
                If Convert.ToBoolean(dr("Afecto")) Then
                    dblIgvSbItem = (gsglIGV / 100) * dblMontoSumar
                End If

                dblIgv += dblIgvSbItem
                dblMonto += dblMontoSumar
                tmpCantidadAPagar = vbytNroCantidadaPagar
            Next

            vdblIgvNeto += dblIgv
            Return dblMonto
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblConsultarCostoxNuevoPaxLiberado(ByVal vintIDServicio_Det As Integer, ByVal vbytNroPax As Byte) As Double
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim dblCosto As Double = 0
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@NroPax", vbytNroPax)
            dc.Add("@pMonto", 0)
            dblCosto = objADO.GetSPOutputValue("MASERVICIOS_DET_COSTOS_SelCostoOutput", dc)
            Return dblCosto
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function intConsultarPaxHastaxNuevoPaxLiberado(ByVal vintIDServicio_Det As Integer, ByVal vbytNroPax As Byte) As Int16
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim dblCosto As Double = 0
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@NroPax", vbytNroPax)
            dc.Add("@pPaxHasta", 0)
            dblCosto = objADO.GetSPOutputValue("MASERVICIOS_DET_COSTOS_SelPaxHastaOutput", dc)
            Return dblCosto
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDetTodosxIDDet(ByVal vintIDDet As Integer, ByVal vbytNroPax As Byte, _
                                                     ByVal vstrMonedaDev As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@NroPax", vbytNroPax)
            dc.Add("@IDMoneda", vstrMonedaDev)

            Return objADO.GetDataTable("COTIDET_SelxIDDetxNroPax", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDetServiciosTodosxIDDet(ByVal vintIDDet As Integer, ByVal vintIDServicio_Det As Integer, ByVal vbytNroPax As Byte, _
                                                     ByVal vstrMonedaDev As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@NroPax", vbytNroPax)
            dc.Add("@IDMoneda", vstrMonedaDev)

            Return objADO.GetDataTable("COTIDET_DETSERVICIOS_SelxIDDetxNroPax", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'Private Function dblCostoRealSubServiciosParaPropiedadesDetReservasBE(ByVal vintIDServicioDet As Integer, _
    '                                                                      ByVal vobjBEDetRes As clsReservaBE.clsDetalleReservaBE, _
    '                                                                      ByVal BECot As clsCotizacionBE) As Double
    '    Try
    '        Dim dblCostoNeto As Double = 0.0
    '        Dim strIDServicio As String = "", strAnio As String = "", strTipoVariante As String = "", dblTipoCambioServ As Double = 0.0
    '        Dim dttSubServicios As DataTable = dttCreaDttDetCotiServ()

    '        Using objServ As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
    '            ' ''Using dr As SqlClient.SqlDataReader = objServ.drConsultarPk(vintIDServicioDet)
    '            ' ''    dr.Read()
    '            ' ''    If dr.HasRows Then
    '            ' ''        strIDServicio = dr("IDServicio")
    '            ' ''        strAnio = dr("Anio")
    '            ' ''        strTipoVariante = dr("Tipo")
    '            ' ''        dblTipoCambioServ = If(IsDBNull(dr("TC")) = True, 0, Convert.ToDouble(dr("TC")))
    '            ' ''    End If
    '            ' ''    dr.Close()
    '            ' ''End Using
    '            Dim dtConsulta As DataTable = objServ.ConsultarPk(vintIDServicioDet)
    '            If dtConsulta.Rows.Count > 0 Then
    '                strIDServicio = dtConsulta(0)("IDServicio")
    '                strAnio = dtConsulta(0)("Anio")
    '                strTipoVariante = dtConsulta(0)("Tipo")
    '                dblTipoCambioServ = If(IsDBNull(dtConsulta(0)("TC")) = True, 0, Convert.ToDouble(dtConsulta(0)("TC")))
    '            End If
    '        End Using
    '        Dim intIDCab As Integer = vobjBEDetRes.IDCab
    '        Dim dblMargenCoti As Double = 0.0, strTipoPaxNac As String = ""

    '        If Not BECot Is Nothing Then
    '            dblMargenCoti = BECot.Margen
    '            strTipoPaxNac = BECot.TipoPax
    '        Else
    '            Using objBNCot As New clsVentasCotizacionBT
    '                Dim dtCoti As DataTable = objBNCot.ConsultarPkCoti(intIDCab)
    '                If dtCoti.Rows.Count > 0 Then
    '                    dblMargenCoti = Convert.ToDouble(dtCoti.Rows(0)("MargenCoti"))
    '                    strTipoPaxNac = dtCoti.Rows(0)("TipoPaxNac").ToString
    '                End If
    '            End Using
    '        End If

    '        Using objBT As New clsCotizacionBN
    '            Dim dcDatosCalculo As Dictionary(Of String, Double) = objBT.CalculoCotizacion(strIDServicio, vintIDServicioDet, strAnio, strTipoVariante, gstrTipoProvOper, _
    '                                                                  vobjBEDetRes.NroPax, 0, "", dblMargenCoti, 0.0, -1.0, -1.0, strTipoPaxNac, 1.0, dttSubServicios, gsglIGV)

    '            'If dblTipoCambioServ <> 0 Then
    '            '    dblCostoNeto = dcDatosCalculo("CostoReal") * dblTipoCambioServ
    '            'Else
    '            '    dblCostoNeto = dcDatosCalculo("CostoReal")
    '            'End If
    '            dblCostoNeto = dcDatosCalculo("CostoReal")
    '        End Using
    '        Return dblCostoNeto
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Private Sub PasarDatosPropiedadesaDetReservasBE(ByRef robjBEDetRes As clsReservaBE.clsDetalleReservaBE, _
                                                    ByVal vobjDetCot As clsCotizacionBE.clsDetalleCotizacionBE, _
                                                    ByVal vstrDescServicio As String, _
                                                    ByVal vListDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE), _
                                                    ByVal vListVuelos As List(Of clsCotizacionBE.clsCotizacionVuelosBE), _
                                                    ByVal intIDServicio_DetPerRail As Integer, _
                                                    ByVal vstrAccion As Char)
        Try
            With robjBEDetRes
                .Cantidad = 1
                .Dia = vobjDetCot.Dia
                .FechaOut = "01/01/1900"
                .Noches = 0
                If (vobjDetCot.IDTipoProv = gstrTipoProvHotel And Not vobjDetCot.PlanAlimenticio) Or _
                   (vobjDetCot.IDTipoProv <> gstrTipoProvHotel And vobjDetCot.ConAlojamiento) Then
                    If vobjDetCot.FechaOut_OperAlojamiento.ToShortDateString = "01/01/0001" Then
                        .FechaOut = datDevuelveFechaOut2(vobjDetCot.IDDet, vListDetCot)
                    Else
                        .FechaOut = vobjDetCot.FechaOut_OperAlojamiento
                    End If
                    .Noches = DateDiff(DateInterval.Day, .Dia, .FechaOut)
                End If
                .Especial = vobjDetCot.Especial
                .IDCab = vobjDetCot.IDCab
                .IDDet = vobjDetCot.IDDet
                .IDDetRel = vobjDetCot.IDDetRel
                .IDDetCopia = vobjDetCot.IDDetCopia
                .IDIdioma = vobjDetCot.IDIdioma
                .NroPax = vobjDetCot.NroPax
                .NroLiberados = vobjDetCot.NroLiberados
                .Tipo_Lib = vobjDetCot.Tipo_Lib
                .IDProveedor = vobjDetCot.IDProveedor
                .IDTipoProv = vobjDetCot.IDTipoProv
                .IDServicio = vobjDetCot.IDServicio
                .IDServicio_Det = vobjDetCot.IDServicio_Det
                .CostoRealEditado = vobjDetCot.CostoRealEditado
                If vstrDescServicio = "" Then
                    .Servicio = vobjDetCot.Servicio
                Else
                    .Servicio = vstrDescServicio
                End If
                If vobjDetCot.IncGuia Then
                    .NroPax = 1
                    .NroLiberados = 0
                    .Cantidad = 1
                    .CantidadAPagar = 1
                End If
                .CostoLiberado = vobjDetCot.CostoLiberado
                .CostoLiberadoImpto = vobjDetCot.CostoLiberadoImpto
                .CostoReal = vobjDetCot.CostoReal
                .CostoRealAnt = vobjDetCot.CostoRealAnt
                .CostoRealImpto = vobjDetCot.CostoRealImpto
                .Margen = vobjDetCot.Margen
                .MargenAplicado = vobjDetCot.MargenAplicado
                .MargenAplicadoAnt = vobjDetCot.MargenAplicadoAnt
                .MargenImpto = vobjDetCot.MargenImpto
                .MargenLiberado = vobjDetCot.MargenLiberado
                .MargenLiberadoImpto = vobjDetCot.MargenLiberadoImpto
                .Total = vobjDetCot.Total
                .TotalOrig = vobjDetCot.TotalOrig
                .TotImpto = vobjDetCot.TotImpto
                .IDUbigeoOri = vobjDetCot.IDUbigeoOri
                .IDUbigeoDes = vobjDetCot.IDUbigeoDes
                .IDTipoOper = vobjDetCot.IDTipoOper
                If vobjDetCot.IDTipoProv = gstrTipoProvOper Then
                    If intIDServicio_DetPerRail <> 0 Then
                        Dim objDetServ As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
                        Dim dtt As DataTable = objDetServ.ConsultarPk(intIDServicio_DetPerRail) 'dttCotidet(0)("IDServicio_Det"))
                        If dtt.Rows.Count > 0 Then
                            '.IDProveedor = gstrIDProveedorPeruRail
                            .IDTipoProv = gstrTipoProvTransp
                            .IDServicio = dtt(0)("IDServicio")
                            .IDServicio_Det = dtt(0)("IDServicio_Det")
                            .Servicio = dtt(0)("Descripcion")
                            .NroPax = 1
                            .NroLiberados = 0
                            .Tipo_Lib = ""


                            Dim dttUbig As DataTable = dttUbigeosxDetalleServicio(.IDServicio_Det)
                            If dttUbig.Rows.Count > 0 Then
                                .IDUbigeoOri = dttUbig(0)("IDUbigeoOri")
                                .IDUbigeoDes = dttUbig(0)("IDUbigeoDes")
                            End If
                            .CostoLiberado = vobjDetCot.CostoLiberado '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .CostoLiberadoImpto = vobjDetCot.CostoLiberadoImpto '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            Dim objCostos As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
                            .CostoReal = objCostos.dblConsultarCosto(intIDServicio_DetPerRail, .NroPax)
                            .CostoRealAnt = vobjDetCot.CostoRealAnt '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .CostoRealImpto = vobjDetCot.CostoRealImpto '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .Margen = vobjDetCot.Margen '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .MargenImpto = vobjDetCot.MargenImpto ' * (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .MargenLiberado = vobjDetCot.MargenLiberado '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .MargenLiberadoImpto = vobjDetCot.MargenLiberadoImpto '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .Total = vobjDetCot.Total '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .TotalOrig = vobjDetCot.TotalOrig '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                            .TotImpto = vobjDetCot.TotImpto '* (vobjDetCot.NroPax + vobjDetCot.NroLiberados)
                        End If
                    ElseIf vobjDetCot.ConAlojamiento = True And vobjDetCot.Dias > 1 Then
                        Dim objDetServ As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
                        Dim dtt As DataTable = objDetServ.ConsultarPk(vobjDetCot.IDServicio_Det) 'dttCotidet(0)("IDServicio_Det"))
                        If dtt.Rows.Count > 0 Then
                            .DetaTipo = If(IsDBNull(dtt(0)("DetaTipo")), "", dtt(0)("DetaTipo"))
                            .DescServicio = dtt(0)("DescServicio")
                        End If
                    End If
                End If
                .EsTransporteAutogenerado = blnEsTransporteAutogenerado(.IDDet, vListVuelos)
                .IDUbigeo = vobjDetCot.IDUbigeo
                .IDPais = strDevuelveIDPaisxIDUbigeo(.IDUbigeo)
                .Item = Convert.ToByte(vobjDetCot.Item)
                .MotivoEspecial = vobjDetCot.MotivoEspecial
                .RutaDocSustento = vobjDetCot.RutaDocSustento
                .Desayuno = vobjDetCot.Desayuno
                .Lonche = vobjDetCot.Lonche
                .Almuerzo = vobjDetCot.Almuerzo
                .Cena = vobjDetCot.Cena
                .Transfer = vobjDetCot.Transfer
                .IDDetTransferOri = vobjDetCot.IDDetTransferOri
                .IDDetTransferDes = vobjDetCot.IDDetTransferDes
                .TipoTransporte = vobjDetCot.TipoTransporte
                .IDEmailNew = ""
                .IDEmailEdit = ""
                .IDEmailRef = ""
                .IncGuia = vobjDetCot.IncGuia
                .IDGuiaProveedor = ""
                .NuVehiculo = 0
                .PlanAlimenticio = vobjDetCot.PlanAlimenticio
                .ConAlojamiento = vobjDetCot.ConAlojamiento
                .Dias = vobjDetCot.Dias
                .SSCR = vobjDetCot.SSCR
                .STCR = vobjDetCot.STCR
                .FlServicioTC = vobjDetCot.FlServicioTC
                .UserMod = vobjDetCot.UserMod
                .Accion = vstrAccion
                .NuViaticosTC = vobjDetCot.NuViaticosTC
                If vobjDetCot.NuViaticosTC > 0 Then
                    .FlServicioIngManual = True
                    .FlServicioTC = True
                End If
                .IDReserva_DetAntiguo = intDevuelveIDReserva_DetxIDDet(vobjDetCot.IDDetAntiguo)
                .IDReserva_Det = vobjDetCot.IDReserva_Det
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function intDevuelveIDReserva_DetxIDDet(ByVal intIDDetAntiguo As Integer) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", intIDDetAntiguo)
            dc.Add("@pIDReserva_Det", 0)

            Return objADO.GetSPOutputValue("RESERVAS_DET_SelIDReservaDetxIDDetOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strDevuelveIDPaisxIDUbigeo(ByVal vstrIDUbigeo As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUbigeo", vstrIDUbigeo)
                .Add("@pIDPais", String.Empty)
            End With

            Return objADO.GetSPOutputValue("MAUBIGEO_SelIDPaisxIDUbigeo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnEsTransporteAutogenerado(ByVal vintIDDet As Integer, ByVal vListVuelos As List(Of clsCotizacionBE.clsCotizacionVuelosBE)) As Boolean
        If vListVuelos Is Nothing Then Return False
        Try
            For Each ItemVuelo As clsCotizacionBE.clsCotizacionVuelosBE In vListVuelos
                If ItemVuelo.IDDet = vintIDDet Then
                    Return True
                End If
            Next

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function
    'Private Function dttPeruRailRelacionado(ByVal vintIDDet As Integer) As DataTable

    '    Dim dc As New Dictionary(Of String, String)

    '    Try
    '        dc.Add("@IDDet", vintIDDet)

    '        Return objADO.GetDataTable("COTIDET_SelIDPeruRail", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try


    'End Function

    Private Function dttUbigeosxDetalleServicio(ByVal vintIDServicio_Det As Integer) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDServicio_Det", vintIDServicio_Det)

            Return objADO.GetDataTable("MASERVICIOS_ALIMENTACION_DIA_SelUbigeosxIDServicio_Det", dc)
        Catch ex As Exception
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".dttUbigeosxDetalleServicio")
        End Try


    End Function
    Private Function datDevuelveFechaOut(ByVal vstrIDTipoProv As String, ByVal vintIDServicio_Det As Integer, ByVal vintIDDet As Integer, _
                                         ByRef vListDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE), ByVal vstrDescripAcomodoEspecial As String, _
                                         ByVal vblnIncGuia As Boolean, ByVal vdatItemDay As Date) As Date
        Try
            Dim bytContVeces As Byte = 0
            Dim dat1eraFecha As Date '= "01/01/1900"
            Dim datFechaDiaAnt As Date '= "01/01/1900"
            Dim datFechaOutReturn As Date = "01/01/1900"
            Dim vdatFechaInicialQ As Date = #1/1/2001#
            Dim LstTempDetalles As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = (From item In vListDetCot _
                                                        Where item.IDServicio_Det = vintIDServicio_Det And item.DescAcomodoEspecial = vstrDescripAcomodoEspecial _
                                                               And item.IncGuia = vblnIncGuia And item.Dia >= vdatItemDay _
                                                        Select item).ToList()
            If vstrIDTipoProv = gstrTipoProvHotel Then
                For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In LstTempDetalles
                    If DateDiff(DateInterval.Day, vdatFechaInicialQ, ItemCot.Dia) >= 0 Then
                        bytContVeces += 1
                        If bytContVeces = 1 Then
                            dat1eraFecha = ItemCot.Dia
                            vdatFechaInicialQ = ItemCot.Dia
                        Else
                            If DateDiff(DateInterval.Day, vdatFechaInicialQ, ItemCot.Dia) = 1 Then
                                dat1eraFecha = ItemCot.Dia
                                vdatFechaInicialQ = ItemCot.Dia
                            Else
                                If DateDiff(DateInterval.Day, vdatFechaInicialQ, ItemCot.Dia) > 1 Then
                                    ItemCot.ItemAlojamiento = 1
                                    Exit For
                                End If
                            End If
                        End If
                    End If
                Next
            Else
                For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCot
                    If vstrIDTipoProv = gstrTipoProvHotel Then
                    Else
                        If ItemCot.IDDet = vintIDDet Or ItemCot.IDDetRel = vintIDDet Then
                            'If ItemCot.IDDetRel = vintIDDet Then
                            bytContVeces += 1
                            If bytContVeces = 1 Then
                                dat1eraFecha = ItemCot.Dia
                            Else
                                If DateDiff(DateInterval.Day, CDate(Format(dat1eraFecha, "dd/MM/yyy")), CDate(Format(ItemCot.Dia, "dd/MM/yyyy"))) = 1 Then
                                    datFechaOutReturn = ItemCot.Dia
                                    dat1eraFecha = ItemCot.Dia
                                Else
                                    If Not blnExisteProxServicioHijo(vintIDServicio_Det, vstrDescripAcomodoEspecial, vblnIncGuia, dat1eraFecha, vListDetCot) Then
                                        datFechaOutReturn = DateAdd(DateInterval.Day, 1, datFechaDiaAnt)
                                        Exit For
                                    End If
                                    'datFechaOutReturn = DateAdd(DateInterval.Day, 1, datFechaDiaAnt)
                                    'Exit For
                                End If
                            End If
                            datFechaDiaAnt = ItemCot.Dia
                        End If
                    End If
                Next
            End If

            If bytContVeces = 1 Or vstrIDTipoProv = gstrTipoProvHotel Then
                datFechaOutReturn = DateAdd(DateInterval.Day, 1, dat1eraFecha)
            End If

            Return datFechaOutReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteProxServicioHijo(ByVal vintIDServicio_Det As Integer, ByVal vstrDescripcionAcomodoEspecial As String, _
                                               ByVal vblnIncGuia As Boolean, ByVal vdatItem As Date, ByVal vListDetalle As List(Of clsCotizacionBE.clsDetalleCotizacionBE)) As Boolean
        Try
            Dim blnOk As Boolean = False
            For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In vListDetalle
                If ItemCot.IDServicio_Det = vintIDServicio_Det And ItemCot.DescAcomodoEspecial = vstrDescripcionAcomodoEspecial _
                    And ItemCot.IncGuia = vblnIncGuia Then
                    If DateDiff(DateInterval.Day, vdatItem, ItemCot.Dia) > 0 Then
                        blnOk = True
                        Exit For
                    End If
                End If
            Next
            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteOtroServicioMismoDia(ByRef vListDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE), ByVal vintIDServicio_Det As Integer, _
                                                   ByVal vdatFecha As Date) As Boolean
        Try
            Dim blnExisteOtroServicio As Boolean = False
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCot
                If Item.Dia > vdatFecha Then
                    If Item.IDServicio_Det <> vintIDServicio_Det Then
                        Item.ItemAlojamiento = 1
                        blnExisteOtroServicio = True
                        Exit For
                    End If
                End If
            Next
            Return blnExisteOtroServicio
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function datDevuelveFechaOut2(ByVal vintIDDet As Integer, _
                                         ByVal vListDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE)) As Date
        Try
            Dim bytContVeces As Byte = 0
            Dim dat1eraFecha As Date '= "01/01/1900"
            Dim datFechaDiaAnt As Date '= "01/01/1900"
            Dim datFechaOutReturn As Date = "01/01/1900"
            For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In vListDetCot
                'Caso para Operadores con Alojamiento.
                If ItemCot.IDDet.ToString.Trim = vintIDDet.ToString.Trim Or ItemCot.IDDetRel.ToString.Trim = vintIDDet.ToString.Trim Then
                    bytContVeces += 1
                    If bytContVeces = 1 Then
                        dat1eraFecha = ItemCot.Dia
                    Else
                        If DateDiff(DateInterval.Day, CDate(Format(dat1eraFecha, "dd/MM/yyy")), CDate(Format(ItemCot.Dia, "dd/MM/yyyy"))) = 1 Then
                            datFechaOutReturn = ItemCot.Dia
                            dat1eraFecha = ItemCot.Dia
                        Else
                            'datFechaOutReturn = datFechaDiaAnt
                            datFechaOutReturn = DateAdd(DateInterval.Day, 1, datFechaDiaAnt)
                            Exit For
                        End If
                    End If
                    datFechaDiaAnt = ItemCot.Dia
                End If
            Next

            If bytContVeces = 1 Then
                datFechaOutReturn = DateAdd(DateInterval.Day, 1, dat1eraFecha)
            End If

            Return datFechaOutReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub PasarDatosPropiedadesaDetServiciosReservasBE(ByRef robjBEDetServRes As clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE, _
                                                ByVal vobjDetServCot As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE, _
                                                ByVal vstrAccion As Char)
        Try
            With robjBEDetServRes

                .IDServicio_Det = vobjDetServCot.IDServicio_Det
                .CostoReal = vobjDetServCot.CostoReal
                .CostoRealEditado = vobjDetServCot.CostoRealEditado
                .CostoLiberado = vobjDetServCot.CostoLiberado
                .Margen = vobjDetServCot.Margen
                .MargenAplicado = vobjDetServCot.MargenAplicado
                .MargenLiberado = vobjDetServCot.MargenLiberado
                .Total = vobjDetServCot.Total
                .CostoRealImpto = vobjDetServCot.CostoRealImpto
                .CostoLiberadoImpto = vobjDetServCot.CostoLiberadoImpto
                .MargenImpto = vobjDetServCot.MargenImpto
                .MargenLiberadoImpto = vobjDetServCot.MargenLiberadoImpto
                .TotImpto = vobjDetServCot.TotImpto
                .UserMod = vobjDetServCot.UserMod
                .Accion = vstrAccion
            End With

        Catch ex As Exception
            Throw
        End Try

    End Sub
    Private Function dtFiltrarDataTable(ByVal vDataTable As DataTable, ByVal psFiltro As String, Optional ByVal psOrder As String = "") As DataTable
        Dim loRows As DataRow()
        Dim dtNuevoDataTable As DataTable        ' Copio la estructura del DataTable original        
        dtNuevoDataTable = vDataTable.Clone()        ' Establezco el filtro y el orden        
        If psOrder = "" Then
            loRows = vDataTable.Select(psFiltro)
        Else
            loRows = vDataTable.Select(psFiltro, psOrder)
        End If        ' Cargo el nuevo DataTable con los datos filtrados        
        For Each ldrRow As DataRow In loRows
            dtNuevoDataTable.ImportRow(ldrRow)
        Next
        ' Retorno el nuevo DataTable        
        Return dtNuevoDataTable
    End Function

    Private Function strDiaSemanaenIdioma(ByVal vdatFecha As Date, ByVal vstrCulturaIdioma As String) As String

        Dim strDiaSemana As String = vdatFecha.ToString("dddd", New  _
                    CultureInfo(If(vstrCulturaIdioma.Trim = "", "en-US", vstrCulturaIdioma.Trim))).ToString.ToUpper

        Return strDiaSemana.Substring(0, 3)
    End Function

    Private Function datSumaRestaDiasFechas(ByVal vdatFecha As Date, ByVal vintDias As Int16, ByVal vstrIDPais As String) As Date

        Dim datFecha As Date
        Dim strDiaSemana As String
        Dim objFer As New clsTablasApoyoBN.clsFeriadosBN
        Dim intDias As Int16 = 0

        datFecha = "01/01/1900"
        If vdatFecha <> "01/01/1900" Then
            datFecha = DateAdd(DateInterval.Day, vintDias, vdatFecha)
        End If



Sgte:
        datFecha = DateAdd(DateInterval.Day, intDias, datFecha)
        intDias = 0
        strDiaSemana = strDiaSemanaenIdioma(datFecha, "es-ES")

        If strDiaSemana.Substring(0, 1) = "S" Or strDiaSemana.Substring(0, 1) = "D" Then
            intDias = -1
            GoTo Sgte
        End If

        'Dim strMesDia As String = Format(Month(datFecha), "00") & Format(Day(datFecha), "00")
        If objFer.blnEsFeriado(datFecha, vstrIDPais) Then
            intDias = -1
            GoTo Sgte
        End If


        Return FormatDateTime(datFecha, DateFormat.ShortDate)
    End Function

    Private Function dicDevuelveFechasDeadLine(ByVal vstrIDProveedor As String, ByVal vdatDia As Date, _
                                               ByVal vintNroPax As Int16, ByRef rLstUltMin As List(Of Char)) As Dictionary(Of String, Date)


        'Dim dcReturn As New Dictionary(Of String, Date)
        'Try
        '    Dim objPrv As New clsProveedorBN.clsPoliticasProveedorBN
        '    Dim dttPrv As DataTable = objPrv.ConsultarxIDProveedor(vstrIDProveedor)
        '    Dim dttFil As DataTable = dtFiltrarDataTable(dttPrv, "Anio='" & vdatDia.Year.ToString.Trim & _
        '                                                 "' And DefinDia<=" & vintNroPax.ToString & " And DefinHasta>=" & vintNroPax.ToString & " And DefinTipo='PAX'")
        '    If dttFil.Rows.Count = 0 Then
        '        dtFiltrarDataTable(dttPrv, "Anio='" & (vdatDia.Year - 1).ToString.Trim & "' And DefinDia<=" & vintNroPax.ToString & " And DefinHasta>=" & vintNroPax.ToString & " And DefinTipo='PAX'")
        '    End If
        '    Dim datFechaDeadLine As Date
        '    If dttFil.Rows.Count > 0 Then
        '        If dttFil(0)("RoomPreeliminar") <> 0 Then
        '            datFechaDeadLine = datSumaRestaDiasFechas(vdatDia, dttFil(0)("RoomPreeliminar") * (-1), gstrPeru)

        '            If DateDiff(DateInterval.Day, Today.Date, datFechaDeadLine) < 0 Then
        '                rLstUltMin.Add("1")
        '                dcReturn.Add("FecDeadLRoom1", Today.Date)
        '            Else
        '                dcReturn.Add("FecDeadLRoom1", datFechaDeadLine)
        '            End If

        '        Else
        '            dcReturn.Add("FecDeadLRoom1", "01/01/1900")
        '        End If

        '        If dttFil(0)("RoomActualizado") <> 0 Then
        '            datFechaDeadLine = datSumaRestaDiasFechas(vdatDia, dttFil(0)("RoomActualizado") * (-1), gstrPeru)

        '            If DateDiff(DateInterval.Day, Today.Date, datFechaDeadLine) < 0 Then
        '                rLstUltMin.Add("2")
        '                dcReturn.Add("FecDeadLRoom2", Today.Date)
        '            Else
        '                dcReturn.Add("FecDeadLRoom2", datFechaDeadLine)
        '            End If

        '        Else
        '            dcReturn.Add("FecDeadLRoom2", "01/01/1900")
        '        End If

        '        If dttFil(0)("RoomFinal") <> 0 Then
        '            datFechaDeadLine = datSumaRestaDiasFechas(vdatDia, dttFil(0)("RoomFinal") * (-1), gstrPeru)

        '            If DateDiff(DateInterval.Day, Today.Date, datFechaDeadLine) < 0 Then
        '                rLstUltMin.Add("3")
        '                dcReturn.Add("FecDeadLRoom3", Today.Date)
        '            Else
        '                dcReturn.Add("FecDeadLRoom3", datFechaDeadLine)
        '            End If

        '        Else
        '            dcReturn.Add("FecDeadLRoom3", "01/01/1900")
        '        End If

        '        If dttFil(0)("PrePag1_Dias") <> 0 Then
        '            datFechaDeadLine = datSumaRestaDiasFechas(vdatDia, dttFil(0)("PrePag1_Dias") * (-1), gstrPeru)

        '            If DateDiff(DateInterval.Day, Today.Date, datFechaDeadLine) < 0 Then
        '                rLstUltMin.Add("4")
        '                dcReturn.Add("FecDeadLPrepag1", Today.Date)
        '            Else
        '                dcReturn.Add("FecDeadLPrepag1", datFechaDeadLine)
        '            End If

        '        Else
        '            dcReturn.Add("FecDeadLPrepag1", "01/01/1900")
        '        End If

        '        If dttFil(0)("PrePag2_Dias") <> 0 Then
        '            datFechaDeadLine = datSumaRestaDiasFechas(vdatDia, dttFil(0)("PrePag2_Dias") * (-1), gstrPeru)

        '            If DateDiff(DateInterval.Day, Today.Date, datFechaDeadLine) < 0 Then
        '                rLstUltMin.Add("5")
        '                dcReturn.Add("FecDeadLPrepag2", Today.Date)
        '            Else
        '                dcReturn.Add("FecDeadLPrepag2", datFechaDeadLine)
        '            End If

        '        Else
        '            dcReturn.Add("FecDeadLPrepag2", "01/01/1900")
        '        End If

        '        If dttFil(0)("Saldo_Dias") <> 0 Then
        '            datFechaDeadLine = datSumaRestaDiasFechas(vdatDia, dttFil(0)("Saldo_Dias") * (-1), gstrPeru)

        '            If DateDiff(DateInterval.Day, Today.Date, datFechaDeadLine) < 0 Then
        '                rLstUltMin.Add("6")
        '                dcReturn.Add("FecDeadLSaldo", Today.Date)
        '            Else
        '                dcReturn.Add("FecDeadLSaldo", datFechaDeadLine)
        '            End If

        '        Else
        '            dcReturn.Add("FecDeadLSaldo", "01/01/1900")
        '        End If


        '        If dttFil(0)("DiasSinPenalidad") <> 0 Then
        '            datFechaDeadLine = datSumaRestaDiasFechas(vdatDia, dttFil(0)("DiasSinPenalidad") * (-1), gstrPeru)

        '            If DateDiff(DateInterval.Day, Today.Date, datFechaDeadLine) < 0 Then
        '                rLstUltMin.Add("7")
        '                dcReturn.Add("FecDeadSinPenalidad", Today.Date)
        '            Else
        '                dcReturn.Add("FecDeadSinPenalidad", datFechaDeadLine)
        '            End If

        '        Else
        '            dcReturn.Add("FecDeadSinPenalidad", "01/01/1900")
        '        End If

        '    Else
        '        dcReturn.Add("FecDeadLRoom1", "01/01/1900")
        '        dcReturn.Add("FecDeadLRoom2", "01/01/1900")
        '        dcReturn.Add("FecDeadLRoom3", "01/01/1900")
        '        dcReturn.Add("FecDeadLPrepag1", "01/01/1900")
        '        dcReturn.Add("FecDeadLPrepag2", "01/01/1900")
        '        dcReturn.Add("FecDeadLSaldo", "01/01/1900")
        '        dcReturn.Add("FecDeadSinPenalidad", "01/01/1900")

        '    End If

        '    Return dcReturn
        'Catch ex As Exception
        '    Throw
        'End Try

    End Function


    Private Sub ActualizarDatosCotizacion(ByVal vintIDCab As Int32, ByVal vstrIDFile As String, _
                                    ByVal vstrIDUsuarioRes As String, _
                                    ByVal vchrEstado As Char, _
                                    ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDFile", vstrIDFile)
                '.Add("@FecInicioRes", vdatFecInicioRes)
                '.Add("@FechaOutRes", vdatFechaOutRes)
                .Add("@IDUsuarioRes", vstrIDUsuarioRes)
                .Add("@Estado", vchrEstado)

                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("COTICAB_Upd_IDFile", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarDatosCotizacion")
        End Try

    End Sub

    Public Sub EnvioPrimerCorreo(ByVal BE As clsProveedorBE, ByVal vstrIDReservas As String, _
                                 ByVal vstrEstado As String, ByVal vstrUserMod As String)
        'Try

        '    ActualizarEstado(vstrIDReservas, vstrEstado, vstrUserMod, "", "")

        '    Dim ObjBE As New clsProveedorBE
        '    ObjBE.IDProveedor = BE.IDProveedor
        '    ObjBE.ListaCorreoReservadosProveedor = BE.ListaCorreoReservadosProveedor

        '    Dim objBT As New clsProveedorBT.clsCorreoReservasProveedorBT
        '    objBT.InsertarActualizarEliminar(ObjBE)

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    Throw
        'End Try
    End Sub

    Public Sub ActualizarEstado(ByVal vintIDReserva As Int32, ByVal vstrIDEstado As String, _
                                ByVal vstrUserMod As String, _
                                ByVal vstrIDEmailModEstado As String, _
                                ByVal vstrCodReservaProv As String)

        Dim dc As New Dictionary(Of String, String)
        'Dim strIDEstado As String
        Try
            'dc.Add("@IDReserva", vintIDReserva)
            'dc.Add("@pIDEstado", "")

            'strIDEstado = objADO.ExecuteSPOutput("RESERVAS_DET_Sel_UnicoEstadoOutput", dc)

            'If strIDEstado <> "" Then
            'dc = New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@IDEmailModEstado", vstrIDEmailModEstado)
            dc.Add("@CodReservaProv", vstrCodReservaProv)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_UpdEstado", dc)
            'End If

            If vstrIDEstado = "WL" Then
                dc = New Dictionary(Of String, String)
                dc.Add("@IDReserva", vintIDReserva)
                objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_DelxIDReserva", dc)


                dc = New Dictionary(Of String, String)
                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_InsxIDReserva", dc)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstado")
        End Try
    End Sub

    Private Sub BackupearCotizacion(ByVal vintIDCab As Int32, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("COTIDET_BUP_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".BackupearCotizacion")
        End Try

    End Sub
    Public Function InsertarxCotizacion(ByVal BE As clsReservaBE, ByVal BECot As clsCotizacionBE, ByVal vintIDReservaExistente As Integer) As Int32
        Dim dc As New Dictionary(Of String, String)
        Dim intIDReserva As Int32
        Try

            If BE.IDReserva = 0 Then

                'Dim strIDFile As String = strDevuelveIDFile(BE.IDCab)
                Dim strIDFile As String = BE.IDFile

                With dc
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDCab", BE.IDCab)
                    .Add("@IDFile", strIDFile)
                    .Add("@Estado", BE.Estado)
                    .Add("@EstadoSolicitud", BE.EstadoSolicitud)
                    .Add("@IDUsuarioVen", BE.IDUsuarioVen)
                    .Add("@IDUsuarioRes", BE.IDUsuarioRes)
                    .Add("@Observaciones", BE.Observaciones)
                    .Add("@CodReservaProv", BE.CodReservaProv)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDReserva", 0)
                End With
                intIDReserva = objADO.ExecuteSPOutput("RESERVAS_Ins", dc)

                BE.IDReserva = intIDReserva
                BE.IDFile = strIDFile

                'ActualizarDatosCotizacion(BE.IDCab, strIDFile, "0000", BE.UserMod)
                ActualizarDatosCotizacion(BE.IDCab, strIDFile, BE.IDUsuarioRes, BE.EstadoFile, BE.UserMod)

                'BackupearCotizacion(BE.IDCab, BE.UserMod)

                Dim objTareas As New clsReservaBT.clsTareasReservaBT
                objTareas.InsertarEliminar(BE)
            End If


            If Not BE.ListaDetReservas Is Nothing Then
                Dim objDet As New clsReservaBT.clsDetalleReservaBT
                objDet.InsertarActualizarEliminar(BE, vintIDReservaExistente)
            End If


            ContextUtil.SetComplete()

            If intIDReserva = 0 Then
                If BE.IDReserva <> -1 Then
                    intIDReserva = BE.IDReserva
                Else
                    intIDReserva = vintIDReservaExistente
                End If

            End If

            Return intIDReserva
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Function

    Public Sub InsertarActualizar(ByVal BE As clsReservaBE, Optional ByVal BECot As clsCotizacionBE = Nothing)
        Dim dc As Dictionary(Of String, String)
        Dim intIDReserva As Integer
        Dim strIDReservaAnt As String

        Try
            For Each Item As clsReservaBE In BE.ListaReservas
                dc = New Dictionary(Of String, String)

                If Item.Accion = "M" Then
                    With dc
                        .Add("@IDReserva", Item.IDReserva)
                        .Add("@Estado", Item.Estado)
                        .Add("@EstadoSolicitud", Item.EstadoSolicitud)
                        .Add("@IDUsuarioRes", Item.IDUsuarioRes)
                        .Add("@Observaciones", Item.Observaciones)
                        .Add("@CodReservaProv", Item.CodReservaProv)
                        .Add("@RegeneradoxAcomodo", Item.RegeneradoxAcomodo)

                        .Add("@UserMod", Item.UserMod)
                    End With

                    objADO.ExecuteSP("RESERVAS_Upd", dc)


                ElseIf Item.Accion = "N" Then
                    Dim objTareasBE As clsReservaBE.clsTareasReservaBE
                    Dim ListaTareasRes As New List(Of clsReservaBE.clsTareasReservaBE)
                    Dim ListaUltimoMinuto As New List(Of Char)
                    Dim dcFec As Dictionary(Of String, Date) = dicDevuelveFechasDeadLine(Item.IDProveedor, Item.FecInicio, Item.NroPax + Item.NroLiberados, ListaUltimoMinuto)
                    For Each dic As KeyValuePair(Of String, Date) In dcFec
                        objTareasBE = New clsReservaBE.clsTareasReservaBE
                        Select Case dic.Key
                            Case "FecDeadLRoom1"
                                objTareasBE.IDTarea = 1
                            Case "FecDeadLRoom2"
                                objTareasBE.IDTarea = 2
                            Case "FecDeadLRoom3"
                                objTareasBE.IDTarea = 3
                            Case "FecDeadLPrepag1"
                                objTareasBE.IDTarea = 4
                            Case "FecDeadLPrepag2"
                                objTareasBE.IDTarea = 5
                            Case "FecDeadLSaldo"
                                objTareasBE.IDTarea = 6
                        End Select
                        objTareasBE.IDReserva = Item.IDReserva
                        objTareasBE.FecDeadLine = dic.Value
                        objTareasBE.IDEstado = "PD"
                        objTareasBE.Accion = "N"
                        If objTareasBE.FecDeadLine <> "01/01/1900" Then
                            ListaTareasRes.Add(objTareasBE)
                        End If
                    Next
                    Item.ListaTareasReservas = ListaTareasRes
                    Item.ListaUltimoMinuto = ListaUltimoMinuto

                    strIDReservaAnt = Item.IDReserva
                    intIDReserva = Insertar(Item)
                    For Each ItemDet As clsReservaBE.clsDetalleReservaBE In BE.ListaDetReservas
                        If ItemDet.IDReserva = strIDReservaAnt Then
                            ItemDet.IDReserva = intIDReserva
                        End If
                    Next

                    If Not BE.ListaTareasReservas Is Nothing Then
                        For Each ItemTar As clsReservaBE.clsTareasReservaBE In BE.ListaTareasReservas
                            If ItemTar.IDReserva = strIDReservaAnt Then
                                ItemTar.IDReserva = intIDReserva
                            End If
                        Next
                    End If

                    If Not BE.ListaAcomodPaxReservasProveedor Is Nothing Then
                        For Each ItemAco As clsReservaBE.clsAcomodoPaxProveedorBE In BE.ListaAcomodPaxReservasProveedor
                            If ItemAco.IDReserva = strIDReservaAnt Then
                                ItemAco.IDReserva = intIDReserva
                            End If
                        Next
                    End If



                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.IDReserva) Then
                        Eliminar(Item.IDReserva, False, Item.UserMod)
                    End If
                End If
            Next

            If Not BE.ListaTareasReservas Is Nothing Then
                Dim objTareas As New clsReservaBT.clsTareasReservaBT
                objTareas.InsertarEliminar(BE)
            End If

            If Not BE.ListaPax Is Nothing Then
                Dim objBECot As New clsCotizacionBE
                objBECot.ListaPax = BE.ListaPax

                Dim objBLPax As New clsPaxVentasCotizacionBT
                objBLPax.InsertarActualizarEliminar(objBECot)
            End If

            If Not BE.ListaPaxTransporte Is Nothing Then
                Dim objBECot As New clsCotizacionBE
                objBECot.ListaPaxTransporte = BE.ListaPaxTransporte

                Dim objBLPax As New clsPaxTransporteBT
                objBLPax.InsertarActualizarEliminar(objBECot)
            End If


            If Not BE.ListaCotizacionVuelos Is Nothing Then
                Dim objBECot As New clsCotizacionBE
                objBECot.ListaCotizacionVuelo = BE.ListaCotizacionVuelos

                Dim objVuel As New clsVuelosVentasCotizacionBT
                objVuel.InsertarActualizarEliminar(objBECot)
            End If

            Dim objBEAcoPax As New clsReservaBE
            objBEAcoPax.ListaAcomodoPaxReservas = BE.ListaAcomodoPaxReservas

            Dim objBEAcoPaxPrv As New clsReservaBE
            objBEAcoPaxPrv.ListaAcomodPaxReservasProveedor = BE.ListaAcomodPaxReservasProveedor

            Dim objDet As New clsReservaBT.clsDetalleReservaBT
            objDet.InsertarActualizarEliminar(BE, 0)

            Dim objAco As New clsReservaBT.clsAcomodoPaxBT
            objAco.InsertarEliminar(BE)

            Dim objAco2 As New clsReservaBT.clsAcomodoPaxProveedorBT
            objAco2.InsertarEliminar(BE)

            Dim objDetResHabit As New clsReservaBT.clsDetalleReservaBT.clsReservasDetEstadoHabitBT
            objDetResHabit.InsertarActualizar(BE)

            ActualizarFechasLogOperacionesDet(BE)
            ActualizarDatosCotizacion(BE.IDCab, "", BE.IDUsuarioRes, BE.EstadoFile, BE.UserMod)
            RecalcularVentasxPaxNoShow(BE.IDCab, BE.UserMod, BE.ListaNoShow, BE.ListaShow)

            ''Envio de correo a Finanzas INC (Entradas MP/WP)
            'If BE.EstadoFile = "A" Then

            '#If DEBUG Then

            '#Else
            '            Dim objVtBT As New clsVentasCotizacionBT
            '            objVtBT.EnviarCorreoINC(BE.IDCab)
            '#End If

            'ARCHIVOS
            If Not BECot Is Nothing Then
                Dim objArchivos As New clsCotizacionBT.clsCotizacionArchivosBT
                objArchivos.InsertarActualizarEliminar(BECot)
            End If



            ''PPMG20160311
            If BE.FlEnviarMPWP = True Then
                Dim objVtBT As New clsVentasCotizacionBT
                objVtBT.EnviarCorreoINC(BE.IDCab)
            End If
            'End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Private Sub ActualizarFechasLogOperacionesDet(ByVal BE As clsReservaBE)
        If BE.ListaDetReservas Is Nothing Then Exit Sub
        Try
            For Each Item As clsReservaBE.clsDetalleReservaBE In BE.ListaDetReservas
                If Item.Accion = "M" Then
                    ActualizarFechaOperacionDetxIDReservaDet(Item.IDReserva_Det)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarFechaOperacionDetxIDReservaDet(ByVal vintIDReserva_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva_Det", vintIDReserva_Det)

            objADO.ExecuteSP("OPERACIONES_DET_UpdFecLogxIDReservaDet", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFechaOperacionDetxIDReservaDet")
        End Try
    End Sub

    Private Sub Eliminar(ByVal vintIDReserva As Int32, ByVal vblnAnular As Boolean, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)

            objADO.ExecuteSP("RESERVAS_DETSERVICIOS_DelxIDReserva", dc)
            objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDReserva", dc)

            objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_DelxIDReserva", dc)

            objADO.ExecuteSP("NOTA_VENTA_DET_DelxIDReserva", dc)
            If Not vblnAnular Then
                Dim dc2 As New Dictionary(Of String, String)
                dc2.Add("@IDReserva", vintIDReserva)
                dc2.Add("@UserMod ", vstrUserMod)
                objADO.ExecuteSP("ACOMODO_VEHICULO_DET_PAX_UpdxIDReserva", dc2)

                objADO.ExecuteSP("RESERVAS_DET_DelxIDReserva", dc)
                objADO.ExecuteSP("RESERVAS_TAREAS_DelxIDReserva", dc)
                objADO.ExecuteSP("ACOMODOPAX_PROVEEDOR_DelxIDReserva", dc)

                objADO.ExecuteSP("RESERVAS_Del", dc)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try

    End Sub
    Public Sub RegeneracionReservas(ByVal BE As clsCotizacionBE)
        Try
            Dim ListaProveedCot As New List(Of stProveedoresReservas)
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                Dim blnExisteProvee As Boolean = False
                For Each STReserva As stProveedoresReservas In ListaProveedCot
                    If STReserva.IDProveedor = Item.IDProveedor Then
                        blnExisteProvee = True
                        Exit For
                    End If
                Next

                If Not blnExisteProvee Then
                    Dim ItemProv As New stProveedoresReservas With {.IDProveedor = Item.IDProveedor, _
                                                                    .RegeneradoxAcomodo = blnReservaRegeneradoxAcomodo(BE.IdCab, Item.IDProveedor)}
                    ListaProveedCot.Add(ItemProv)
                End If
            Next


            ''Eliminación del File
            Dim objOpeBT As New clsOperacionBT
            objOpeBT.EliminarxFile(BE.IdCab, BE.UserMod)
            EliminarxFile(BE.IdCab, BE.UserMod)

            ''Nueva Generación
            Dim objResBT As New clsReservaBT
            Dim ListaProveed As Object = objResBT.ListaProvReservas
            objResBT.CambiosPreGeneracionxFile(BE, ListaProveed, ListaProveedCot)
            objResBT.GrabarxCotizacion2(BE, False, ListaProveed)
            objResBT.GrabarxCotizacion2(BE, True, ListaProveed)
            objResBT.CambiosPostGeneracionxFile(BE, ListaProveed)
            objResBT.ActualizarEstadoCotizRegenerada(BE.IdCab)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub EliminarxFile(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDCab", dc)
            objADO.ExecuteSP("RESERVAS_MENSAJES_DelxIDCab", dc)
            objADO.ExecuteSP("RESERVAS_TAREAS_DelxIDCab", dc)
            objADO.ExecuteSP("RESERVAS_DETSERVICIOS_DelxIDCab", dc)
            objADO.ExecuteSP("ACOMODOPAX_PROVEEDOR_DelxIDCab", dc)
            objADO.ExecuteSP("ACOMODOPAX_FILE_DelxIDCab", dc)
            objADO.ExecuteSP("ORDENPAGO_DET_DelxIDCab", dc)
            objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_DelxIDCab", dc)

            objADO.ExecuteSP("NOTA_VENTA_DET_DelxIDCab", dc)
            objADO.ExecuteSP("RESERVAS_DET_DelxIDCab", dc)
            objADO.ExecuteSP("ORDENPAGO_DelxIDCab", dc)

            objADO.ExecuteSP("RESERVAS_DelxIDCab", dc)
            objADO.ExecuteSP("COTIDET_LOG_DelxIDCab", dc)

            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("COTICAB_UpdaPendientexIDCab", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxFile")
        End Try

    End Sub
    Private Sub RegenerarDetalleReserva(ByVal vintIDDet As Integer, _
                                        ByVal vchrAccion As Char, _
                                       ByVal vstrIDProveedor As String, _
                                       ByVal vstrIDTipoProv As String, _
                                       ByVal vblnConAlojamiento As Boolean, _
                                       ByVal vblnPlanAlimenticio As Boolean, _
                                       ByVal vintIDServicio_Det As Integer, _
                                       ByVal vintIDCab As Integer, _
                                       ByVal vintIDReserva As Integer, _
                                       ByVal BECot As clsCotizacionBE, _
                                ByVal vstrUserMod As String)
        Try


            If vchrAccion <> "B" Then
                Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
                Dim objDetCotBT As New clsDetalleVentasCotizacionBT
                Dim dttCotiDet As DataTable
                If (vstrIDTipoProv = gstrTipoProvHotel And vblnPlanAlimenticio = False) Or _
                    (vstrIDTipoProv <> gstrTipoProvHotel And vblnConAlojamiento = True) Then
                    'dttCotiDet = objDetCotBT.ConsultarxProveedorDetServicio(vintIDCab, vstrIDProveedor, vintIDServicio_Det)
                    Dim dcFec As New Dictionary(Of String, String)
                    Dim datFechaIn As Date = #1/1/1900#
                    Dim datFechaFin As Date = #1/1/1900#

                    dcFec.Add("@IDCab", vintIDCab)
                    dcFec.Add("@IDProveedor", vstrIDProveedor)
                    dcFec.Add("@IDServicio_Det", vintIDServicio_Det)
                    Dim dttFechas As DataTable = objADO.GetDataTable("RESERVAS_DET_SelFechasxServicio", dcFec)
                    'If dttFechas.Rows.Count = 1 Then
                    If dttFechas.Rows.Count > 0 Then
                        datFechaIn = dttFechas(0)("Dia")
                        datFechaFin = dttFechas(0)("FechaOut")
                    End If
                    'ElseIf dttFechas.Rows.Count > 1 Then
                    '    Dim bytCont As Byte = 1
                    '    For Each dr As DataRow In dttFechas.Rows
                    '        If bytCont = 1 Then
                    '            datFechaIn = dr("Dia")
                    '        End If
                    '        If bytCont = dttFechas.Rows.Count Then
                    '            datFechaFin = dr("FechaOut")
                    '        End If
                    '        bytCont += 1
                    '    Next
                    'End If


                    RegenerarIngresoHabitacionReserva(vintIDDet, vintIDReserva, vstrIDProveedor, _
                                                      vintIDServicio_Det, datFechaIn, datFechaFin, _
                                                      vintIDCab, vstrUserMod, vchrAccion)

                Else

                    'Dim objDetOpeBT As New clsDetalleOperacionBT

                    'If Not objDetOpeBT.blnExistenOperaciones_DetxIDDet(vintIDDet) Then

                    '    objDetOpeBT.EliminarxIDDet(vintIDDet)
                    '    Dim objDetBT As New clsReservaBT.clsDetalleReservaBT
                    '    objDetBT.EliminarxIDDet(vintIDDet)
                    'Else
                    '    Dim objDetBT As New clsReservaBT.clsDetalleReservaBT
                    '    objDetBT.AnularxIDDet(vintIDDet, True, vstrUserMod)
                    '    objDetBT.InsertarLogxIDDet(vintIDDet, "B")

                    'End If




                    dttCotiDet = objDetCotBT.dttConsultarPK(vintIDDet)



                    PasarDatosListaCotiDetRegeneracion(vstrIDProveedor, vintIDCab, vintIDReserva, _
                                                       vstrUserMod, dttCotiDet, ListaCotiDet)


                    BECot.ListaDetalleCotiz = ListaCotiDet


                    Dim ListaProveed As List(Of stProveedoresReservas) = ListaProvReservas
                    Dim ItemProvee As New stProveedoresReservas With {.IDReserva = vintIDReserva, .IDProveedor = vstrIDProveedor}
                    ListaProveed = New List(Of stProveedoresReservas)
                    ListaProveed.Add(ItemProvee)


                    GrabarxCotizacion2(BECot, False, ListaProveed, True, , , vchrAccion) ', datFechaOut, bytNoches)
                    GrabarxCotizacion2(BECot, True, ListaProveed, True, , , vchrAccion) ', datFechaOut, bytNoches)


                    CambiosPostGeneracionxFile(BECot, ListaProveed, vintIDDet, , True, vchrAccion)

                End If
            Else 'Eliminando

                Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT
                Dim objDetOpeBT As New clsDetalleOperacionBT

                If Not objDetOpeBT.blnExistenOperaciones_DetxIDDet(vintIDDet) Then

                    objDetOpeBT.EliminarxIDDet(vintIDDet)
                    Dim objDetBT As New clsReservaBT.clsDetalleReservaBT
                    objDetBT.EliminarxIDDet(vintIDDet)
                Else
                    Dim objDetBT As New clsReservaBT.clsDetalleReservaBT
                    objDetBT.AnularxIDDet(vintIDDet, True, vstrUserMod)
                    'objDetBT.InsertarLogxIDDet(vintIDDet, "B")

                End If

            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub RegenerarIngresoHabitacionReserva(ByVal vintIDDet_Log As Integer, ByVal vintIDReserva As Integer, _
                                ByVal vstrIDProveedor As String, ByVal vintIDServicio_Det As Integer, _
                                ByVal vdatFechaIn As Date, ByVal vdatFechaFin As Date, _
                                ByVal vintIDCab As Integer, ByVal vstrUserMod As String, _
                                 ByVal vchrAccion As Char)
        Try


            ''Dim objOpeBN As New clsOperacionBN
            ''Dim dr As SqlClient.SqlDataReader = objOpeBN.ConsultarxIDReserva(vintIDReserva)
            ''dr.Read()
            ''If dr.HasRows Then
            ''    Dim intIDOperacion As Integer = dr("IDOperacion")
            ''    Dim objOpeBT As New clsOperacionBT
            ''    objOpeBT.Eliminar(intIDOperacion)
            ''End If


            Dim BECot As New clsCotizacionBE
            Dim objCotBT As New clsVentasCotizacionBT
            Dim dttCB As DataTable = objCotBT.dttConsultarPk(vintIDCab)

            With BECot
                .IdCab = vintIDCab
                .IDFile = dttCB(0)("IDFile")
                .IDUsuario = dttCB(0)("IDUsuario")
                .IDUsuarioRes = dttCB(0)("IDUsuarioRes")
                .Simple = If(IsDBNull(dttCB(0)("Simple")), 0, dttCB(0)("Simple"))
                .SimpleResidente = If(IsDBNull(dttCB(0)("SimpleResidente")), 0, dttCB(0)("SimpleResidente"))
                .Twin = If(IsDBNull(dttCB(0)("Twin")), 0, dttCB(0)("Twin"))
                .TwinResidente = If(IsDBNull(dttCB(0)("TwinResidente")), 0, dttCB(0)("TwinResidente"))
                .Matrimonial = If(IsDBNull(dttCB(0)("Matrimonial")), 0, dttCB(0)("Matrimonial"))
                .MatrimonialResidente = If(IsDBNull(dttCB(0)("MatrimonialResidente")), 0, dttCB(0)("MatrimonialResidente"))
                .Triple = If(IsDBNull(dttCB(0)("Triple")), 0, dttCB(0)("Triple"))
                .TripleResidente = If(IsDBNull(dttCB(0)("TripleResidente")), 0, dttCB(0)("TripleResidente"))
                .Estado = dttCB(0)("Estado")
                .UserMod = vstrUserMod
            End With


            Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Dim objDetCotBT As New clsDetalleVentasCotizacionBT
            'Dim dttCotiDet As DataTable = objDetCotBT.ConsultarxProveedorDetServicio(vintIDCab, vstrIDProveedor, vintIDServicio_Det)


            Dim dttCotiDet As DataTable = objDetCotBT.ConsultarListxRangoFechasConsecutivasProveedor(vintIDCab, _
                                               vstrIDProveedor, vintIDServicio_Det, vdatFechaIn, vdatFechaFin)


            Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT
            Dim objDetOpeBT As New clsDetalleOperacionBT
            Dim ListaIDDet As New List(Of Integer)

            'objDetOpeBT.EliminarxIDDet(vintIDDet_Log)
            'objDetResBT.EliminarxIDDet(vintIDDet_Log)
            For Each dr As DataRow In dttCotiDet.Rows

                'objDetOpeBT.EliminarxIDDet(dr("IDDet"))
                'objDetResBT.EliminarxIDDet(dr("IDDet"))

                'If dr("IDProveedor") = vstrIDProveedor Then
                Dim ItemDetCot As New clsCotizacionBE.clsDetalleCotizacionBE
                With ItemDetCot
                    .NroPax = dr("NroPax")
                    .NroLiberados = If(IsDBNull(dr("NroLiberados")), 0, dr("NroLiberados"))
                    .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib"))
                    .CostoLiberado = dr("CostoLiberado")
                    .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                    .CostoReal = dr("CostoReal")
                    .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")), 0, dr("CostoRealAnt"))
                    .CostoRealImpto = dr("CostoRealImpto")
                    .Cotizacion = dr("Cotizacion")
                    .Dia = dr("Dia")
                    .Especial = dr("Especial")
                    .IDCab = vintIDCab
                    .IDDet = dr("IDDet")
                    .IDIdioma = dr("IDidioma")
                    .IDProveedor = dr("IDProveedor")
                    .IDTipoProv = dr("IDTipoProv")
                    .Dias = 0
                    .IDServicio = dr("IDServicio")
                    .IDServicio_Det = dr("IDServicio_Det")
                    .IDUbigeo = dr("IDubigeo")
                    .Item = dr("Item")
                    .Margen = dr("Margen")
                    .MargenAplicado = dr("MargenAplicado")
                    .MargenAplicadoAnt = If(IsDBNull(dr("MargenAplicadoAnt")), 0, dr("MargenAplicadoAnt"))
                    .MargenImpto = dr("MargenImpto")
                    .MargenLiberado = dr("MargenLiberado")
                    .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                    .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")), "", dr("MotivoEspecial"))
                    .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")), "", dr("RutaDocSustento"))
                    .Desayuno = dr("Desayuno")
                    .Lonche = dr("Lonche")
                    .Almuerzo = dr("Almuerzo")
                    .Cena = dr("Cena")
                    .Transfer = dr("Transfer")
                    .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")), 0, dr("IDDetTransferOri"))
                    .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")), 0, dr("IDDetTransferDes"))
                    .TipoTransporte = If(IsDBNull(dr("TipoTransporte")), "", dr("TipoTransporte"))
                    .IDUbigeoOri = dr("IDUbigeoOri")
                    .IDUbigeoDes = dr("IDUbigeoDes")
                    .Servicio = If(IsDBNull(dr("Servicio")), "", dr("Servicio"))
                    .SSCL = dr("SSCL")
                    .SSCLImpto = dr("SSCLImpto")
                    .SSCR = dr("SSCR")
                    .SSCRImpto = dr("SSCRImpto")
                    .SSMargen = dr("SSMargen")
                    .SSMargenImpto = dr("STMargenImpto")
                    .SSMargenLiberado = dr("SSMargenLiberado")
                    .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                    .SSTotal = dr("SSTotal")
                    .SSTotalOrig = dr("SSTotalOrig")
                    .STCR = dr("STCR")
                    .STCRImpto = dr("STCRImpto")
                    .STMargen = dr("STMargen")
                    .STMargenImpto = dr("STMargenImpto")
                    .STTotal = dr("STTotal")
                    .STTotalOrig = dr("STTotalOrig")
                    .Total = dr("Total")
                    .RedondeoTotal = dr("RedondeoTotal")
                    .TotalOrig = dr("TotalOrig")
                    .TotImpto = dr("TotImpto")
                    .IDDetRel = If(IsDBNull(dr("IDDetRel")), 0, dr("IDDetRel"))
                    .IDDetCopia = If(IsDBNull(dr("IDDetCopia")), 0, dr("IDDetCopia"))
                    '.Recalcular = False
                    .PorReserva = dr("PorReserva")
                    .ExecTrigger = dr("ExecTrigger")
                    .IDReserva = vintIDReserva
                    '.Reservado = dr("Reservado")
                    '.ExecTrigger = dr("ExecTrigger")
                    .IncGuia = dr("IncGuia")
                    .FueradeHorario = dr("FueradeHorario")
                    .ConAlojamiento = dr("ConAlojamiento")
                    .CostoRealEditado = dr("CostoRealEditado")
                    .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                    .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                    .ServicioEditado = dr("ServicioEditado")
                    .PlanAlimenticio = dr("PlanAlimenticio")
                    '.ActualizQuiebre = dr("ActualizQuiebre")
                    .UserMod = vstrUserMod
                    .IDReserva_Det = dr("IDReserva_Det")
                    .FlServicioTC = dr("FlServicioTC")
                    .IDDetAntiguo = dr("IDDetAntiguo")
                    ListaIDDet.Add(.IDDet)
                End With
                ListaCotiDet.Add(ItemDetCot)
                'End If
            Next
            BECot.ListaDetalleCotiz = ListaCotiDet




            'CambiosPreGeneracionxFile(BECot, ListaProveed)
            Dim ListaProveed As List(Of stProveedoresReservas) = ListaProvReservas
            Dim ItemProvee As New stProveedoresReservas With {.IDReserva = vintIDReserva, .IDProveedor = vstrIDProveedor}
            ListaProveed = New List(Of stProveedoresReservas)
            ListaProveed.Add(ItemProvee)

            GrabarxCotizacion2(BECot, False, ListaProveed, True, , , vchrAccion)
            GrabarxCotizacion2(BECot, True, ListaProveed, True, , , vchrAccion)
            CambiosPostGeneracionxFile(BECot, ListaProveed, -1, ListaIDDet, True, vchrAccion)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function blnExistenOperaciones_DetxIDReserva(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_Sel_ExistenxIDReservaOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub Anular(ByVal vintIDReserva As Integer, ByVal vblnAnulado As Boolean, _
                          ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@Anulado", vblnAnulado)


            objADO.ExecuteSP("RESERVAS_UpdAnulado", dc)

            AnularDetalle(vintIDReserva, vblnAnulado, _
                           vstrUserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Anular")
        End Try

    End Sub
    Private Sub AnularDetalle(ByVal vintIDReserva As Integer, ByVal vblnAnulado As Boolean, _
                          ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@Anulado", vblnAnulado)


            objADO.ExecuteSP("RESERVAS_DET_UpdAnuladoxIDReserva", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AnularDetalle")
        End Try

    End Sub

    Public Sub RegenerarReserva(ByVal vintIDReserva As Integer, ByVal vstrIDProveedor As String, _
                                ByVal vintIDCab As Integer, _
                                ByVal vstrUserMod As String, Optional ByVal vblnCambiosAcomodos As Boolean = False)
        Try
            Dim objBTOp As New clsOrdenPagoBT
            Dim blnExisteOrdenPago As Boolean = False
            Dim blnExisteOperacion As Boolean = False
            Dim blnRegeneradoxAcomodo As Boolean = blnReservaRegeneradoxAcomodo(vintIDCab, vstrIDProveedor)
            Dim vblnEsAlojamiento As Boolean = blnExisteAlojamientoReservas(vintIDReserva)
            If vintIDReserva <> 0 Then
                If blnExistenOperaciones_DetxIDReserva(vintIDReserva) Then

                    Anular(vintIDReserva, True, vstrUserMod)
                    'InsertarLogxIDReserva(vintIDReserva, "B")

                    Eliminar(vintIDReserva, True, vstrUserMod)

                    blnExisteOperacion = True
                ElseIf objBTOp.blnExiste(vintIDReserva) Then 'orden pago
                    Anular(vintIDReserva, True, vstrUserMod)
                    Eliminar(vintIDReserva, True, vstrUserMod)

                    blnExisteOrdenPago = True
                Else
                    If Not vblnCambiosAcomodos Then
                        Dim objOpeBT As New clsOperacionBT
                        Dim dtt As DataTable = objOpeBT.ConsultarxIDReserva(vintIDReserva)

                        For Each dr As DataRow In dtt.Rows
                            Dim intIDOperacion As Integer = dr("IDOperacion")

                            objOpeBT.Eliminar(intIDOperacion)
                        Next
                    End If
                    Eliminar(vintIDReserva, False, vstrUserMod)
                End If
            End If


            Dim BECot As New clsCotizacionBE
            Dim objCotBT As New clsVentasCotizacionBT
            Dim dttCB As DataTable = objCotBT.dttConsultarPk(vintIDCab)
            With BECot
                .IdCab = vintIDCab
                .IDFile = dttCB(0)("IDFile")
                .IDUsuario = dttCB(0)("IDUsuario")
                .IDUsuarioRes = dttCB(0)("IDUsuarioRes")
                .Simple = If(IsDBNull(dttCB(0)("Simple")), 0, dttCB(0)("Simple"))
                .SimpleResidente = If(IsDBNull(dttCB(0)("SimpleResidente")), 0, dttCB(0)("SimpleResidente"))
                .Twin = If(IsDBNull(dttCB(0)("Twin")), 0, dttCB(0)("Twin"))
                .TwinResidente = If(IsDBNull(dttCB(0)("TwinResidente")), 0, dttCB(0)("TwinResidente"))
                .Matrimonial = If(IsDBNull(dttCB(0)("Matrimonial")), 0, dttCB(0)("Matrimonial"))
                .MatrimonialResidente = If(IsDBNull(dttCB(0)("MatrimonialResidente")), 0, dttCB(0)("MatrimonialResidente"))
                .Triple = If(IsDBNull(dttCB(0)("Triple")), 0, dttCB(0)("Triple"))
                .TripleResidente = If(IsDBNull(dttCB(0)("TripleResidente")), 0, dttCB(0)("TripleResidente"))
                .Estado = dttCB(0)("Estado")
                .FlTourConductor = dttCB(0)("FlIncluirTC")
                .RegeneradoxAcomodo = blnRegeneradoxAcomodo
                .UserMod = vstrUserMod
            End With

            Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Dim objDetCotBT As New clsDetalleVentasCotizacionBT
            Dim dttCotiDet As DataTable = Nothing
            If (vstrIDProveedor = gstrIDProveedorSetoursLima Or vstrIDProveedor = gstrIDProveedorSetoursCusco Or _
                vstrIDProveedor = gstrIDProveedorSetoursBuenosAires Or vstrIDProveedor = gstrIDProveedorSetoursSantiago) Or vblnEsAlojamiento Then
                dttCotiDet = objDetCotBT.ConsultarListxIDCab(vintIDCab, True, False, False)
            Else
                dttCotiDet = objDetCotBT.ConsultarListSubServiciosxIDCab(vintIDCab, True, False) '>>Nuevo'
                If dtFiltrarDataTable(dttCotiDet, "IDProveedor='" & vstrIDProveedor & "'").Rows.Count = 0 Then
                    dttCotiDet = objDetCotBT.ConsultarListxIDCab(vintIDCab, True, False, False)
                End If
            End If

            For Each dr As DataRow In dttCotiDet.Rows
                If dr("IDProveedor") = vstrIDProveedor Then
                    Dim ItemDetCot As New clsCotizacionBE.clsDetalleCotizacionBE
                    With ItemDetCot
                        .NroPax = dr("NroPax")
                        .NroLiberados = If(IsDBNull(dr("NroLiberados")), 0, dr("NroLiberados"))
                        .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib"))
                        .CostoLiberado = dr("CostoLiberado")
                        .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                        .CostoReal = dr("CostoReal")
                        .CostoRealAnt = dr("CostoRealAnt")
                        .CostoRealImpto = dr("CostoRealImpto")
                        .Cotizacion = dr("Cotizacion")
                        .Dia = dr("Dia")
                        .Especial = dr("Especial")
                        .IDCab = vintIDCab
                        .IDDet = dr("IDDet")
                        .IDIdioma = dr("IDidioma")
                        .IDProveedor = dr("IDProveedor")
                        .IDTipoProv = dr("IDTipoProv")
                        .IDTipoOper = dr("IDTipoOper")
                        .Dias = dr("Dias")
                        .IDServicio = dr("IDServicio")
                        .IDServicio_Det = dr("IDServicio_Det")
                        .IDUbigeo = dr("IDubigeo")
                        .Item = dr("Item")
                        .Margen = dr("Margen")
                        .MargenAplicado = dr("MargenAplicado")
                        .MargenAplicadoAnt = dr("MargenAplicadoAnt")
                        .MargenImpto = dr("MargenImpto")
                        .MargenLiberado = dr("MargenLiberado")
                        .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                        .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")), "", dr("MotivoEspecial"))
                        .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")), "", dr("RutaDocSustento"))
                        .Desayuno = dr("Desayuno")
                        .Lonche = dr("Lonche")
                        .Almuerzo = dr("Almuerzo")
                        .Cena = dr("Cena")
                        .Transfer = dr("Transfer")
                        .IDDetTransferOri = dr("IDDetTransferOri")
                        .IDDetTransferDes = dr("IDDetTransferDes")
                        .TipoTransporte = If(IsDBNull(dr("TipoTransporte")), "", dr("TipoTransporte"))
                        .IDUbigeoOri = dr("IDUbigeoOri")
                        .IDUbigeoDes = dr("IDUbigeoDes")
                        .Servicio = If(IsDBNull(dr("Servicio")), "", dr("Servicio"))
                        .SSCL = dr("SSCL")
                        .SSCLImpto = dr("SSCLImpto")
                        .SSCR = dr("SSCR")
                        .SSCRImpto = dr("SSCRImpto")
                        .SSMargen = dr("SSMargen")
                        .SSMargenImpto = dr("STMargenImpto")
                        .SSMargenLiberado = dr("SSMargenLiberado")
                        .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                        .SSTotal = dr("SSTotal")
                        .SSTotalOrig = dr("SSTotalOrig")
                        .STCR = dr("STCR")
                        .STCRImpto = dr("STCRImpto")
                        .STMargen = dr("STMargen")
                        .STMargenImpto = dr("STMargenImpto")
                        .STTotal = dr("STTotal")
                        .STTotalOrig = dr("STTotalOrig")
                        .Total = dr("Total")
                        .RedondeoTotal = dr("RedondeoTotal")
                        .TotalOrig = dr("TotalOrig")
                        .TotImpto = dr("TotImpto")
                        .IDDetRel = If(IsDBNull(dr("IDDetRel")), 0, dr("IDDetRel"))
                        .IDDetCopia = If(IsDBNull(dr("IDDetCopia")), 0, dr("IDDetCopia"))
                        '.Recalcular = False
                        .PorReserva = dr("PorReserva")
                        .ExecTrigger = dr("ExecTrigger")
                        .IDReserva = dr("IDReserva")
                        '.Reservado = dr("Reservado")
                        '.ExecTrigger = dr("ExecTrigger")
                        .IncGuia = dr("IncGuia")
                        .FueradeHorario = dr("FueradeHorario")
                        .ConAlojamiento = dr("ConAlojamiento")
                        .CostoRealEditado = dr("CostoRealEditado")
                        .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                        .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                        .ServicioEditado = dr("ServicioEditado")
                        .PlanAlimenticio = dr("PlanAlimenticio")
                        '.ActualizQuiebre = dr("ActualizQuiebre")
                        .AcomodoEspecial = dr("AcomodoEspecial")
                        .ItemAlojamiento = Convert.ToByte(dr("ItemAlojamiento"))
                        .DescAcomodoEspecial = dr("DescAcomodosEspeciales").ToString
                        .FlServicioTC = dr("FlServicioTC")
                        .IDDetAntiguo = dr("IDDetAntiguo")
                        .UserMod = vstrUserMod
                    End With
                    ListaCotiDet.Add(ItemDetCot)
                End If
            Next
            Dim ListTempCotiDet As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = ListaCotiDet
            ActualizarFechaOutItemsAlojamiento(ListaCotiDet)
            AgregarViaticosTourConductor(BECot, vstrIDProveedor, ListaCotiDet)
            BECot.ListaDetalleCotiz = ListaCotiDet


            Dim ListaProveed As List(Of stProveedoresReservas) = ListaProvReservas

            If Not ListaProveed Is Nothing Then
                CambiosPreGeneracionxFile(BECot, ListaProveed)
                GrabarxCotizacion2(BECot, False, ListaProveed, vListDetalleCotiz:=ListTempCotiDet)
                GrabarxCotizacion2(BECot, True, ListaProveed, vListDetalleCotiz:=ListTempCotiDet)
                CambiosPostGeneracionxFile(BECot, ListaProveed)

            End If
            

            'Dim dc As New Dictionary(Of String, String)
            'dc.Add("@IDCab", vintIDCab)
            'dc.Add("@IDProveedor", vstrIDProveedor)
            'objADO.ExecuteSP("COTIDET_LOG_DelxIDCabProveedor", dc)
            EliminarLogVentas(vintIDCab, vstrIDProveedor)

            ActualizarFlagCambiosdeVentas(vintIDReserva, False, _
                                                  vstrUserMod)


            If blnExisteOrdenPago Then
                Dim objOrdPagBT As New clsOrdenPagoBT
                objOrdPagBT.ActualizarAnulados(vstrIDProveedor, vintIDCab, vstrUserMod)
            End If

            If blnExisteOperacion Then
                ActualizarIDsPostAnulacion(vstrIDProveedor, vintIDCab, vstrUserMod)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function dtConsultarViaticosTourConductor(ByVal vintIDCab As Integer) As System.Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("VIATICOS_TOURCONDUCTOR_SelxIDCabInReservasDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function oDetViaticosTourConductorList(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As List(Of clsCotizacionBE.clsDetalleCotizacionBE)
        Try
            Dim dtSubServ As DataTable = dtConsultarViaticosTourConductor(vintIDCab)
            Dim oDetCot As clsCotizacionBE.clsDetalleCotizacionBE = Nothing
            Dim ListDetalleCotiz As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            For Each dr As DataRow In dtSubServ.Rows
                If dr("IDProveedor") = vstrIDProveedor Then
                    oDetCot = New clsCotizacionBE.clsDetalleCotizacionBE
                    With oDetCot
                        .IDDet = dr("IDDet")
                        .NroPax = dr("NroPax")
                        .NroLiberados = If(IsDBNull(dr("NroLiberados")), 0, dr("NroLiberados"))
                        .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib"))
                        .CostoLiberado = dr("CostoLiberado")
                        .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                        .CostoReal = dr("CostoReal")
                        .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")), 0, dr("CostoRealAnt"))
                        .CostoRealImpto = dr("CostoRealImpto")
                        .Cotizacion = dr("Cotizacion")
                        .Dia = dr("Dia")
                        .Especial = dr("Especial")
                        .IDCab = vintIDCab
                        .IDIdioma = dr("IDidioma")
                        .IDProveedor = dr("IDProveedor")
                        .IDTipoProv = dr("IDTipoProv")
                        .Dias = dr("Dias")
                        .IDServicio = dr("IDServicio")
                        .IDServicio_Det = dr("IDServicio_Det")
                        .IDUbigeo = dr("IDubigeo")
                        .Item = dr("Item")
                        .Margen = dr("Margen")
                        .MargenAplicado = dr("MargenAplicado")
                        .MargenAplicadoAnt = If(IsDBNull(dr("MargenAplicadoAnt")), 0, dr("MargenAplicadoAnt"))
                        .MargenImpto = dr("MargenImpto")
                        .MargenLiberado = dr("MargenLiberado")
                        .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                        .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")), "", dr("MotivoEspecial"))
                        .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")), "", dr("RutaDocSustento"))
                        .Desayuno = dr("Desayuno")
                        .Lonche = dr("Lonche")
                        .Almuerzo = dr("Almuerzo")
                        .Cena = dr("Cena")
                        .Transfer = dr("Transfer")
                        .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")), 0, dr("IDDetTransferOri"))
                        .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")), 0, dr("IDDetTransferDes"))
                        .TipoTransporte = If(IsDBNull(dr("TipoTransporte")), "", dr("TipoTransporte"))
                        .IDUbigeoOri = dr("IDUbigeoOri")
                        .IDUbigeoDes = dr("IDUbigeoDes")
                        .Servicio = If(IsDBNull(dr("Servicio")), "", dr("Servicio"))
                        .SSCL = dr("SSCL")
                        .SSCLImpto = dr("SSCLImpto")
                        .SSCR = dr("SSCR")
                        .SSCRImpto = dr("SSCRImpto")
                        .SSMargen = dr("SSMargen")
                        .SSMargenImpto = dr("STMargenImpto")
                        .SSMargenLiberado = dr("SSMargenLiberado")
                        .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                        .SSTotal = dr("SSTotal")
                        .SSTotalOrig = dr("SSTotalOrig")
                        .STCR = dr("STCR")
                        .STCRImpto = dr("STCRImpto")
                        .STMargen = dr("STMargen")
                        .STMargenImpto = dr("STMargenImpto")
                        .STTotal = dr("STTotal")
                        .STTotalOrig = dr("STTotalOrig")
                        .Total = dr("Total")
                        .RedondeoTotal = dr("RedondeoTotal")
                        .TotalOrig = dr("TotalOrig")
                        .TotImpto = dr("TotImpto")
                        .IDDetRel = If(IsDBNull(dr("IDDetRel")), 0, dr("IDDetRel"))
                        .IDDetCopia = If(IsDBNull(dr("IDDetCopia")), 0, dr("IDDetCopia"))
                        .PorReserva = dr("PorReserva")
                        .ExecTrigger = dr("ExecTrigger")
                        .IDReserva = dr("IDReserva")
                        .IncGuia = dr("IncGuia")
                        .FueradeHorario = dr("FueradeHorario")
                        .ConAlojamiento = dr("ConAlojamiento")
                        .CostoRealEditado = dr("CostoRealEditado")
                        .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                        .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                        .ServicioEditado = dr("ServicioEditado")
                        .PlanAlimenticio = dr("PlanAlimenticio")
                        .IDReserva_Det = dr("IDReserva_Det")
                        .NuViaticosTC = dr("NuViaticoTC")
                        .IDDetAntiguo = dr("IDDetAntiguo")
                        .FlServicioIngManual = True
                    End With
                    ListDetalleCotiz.Add(oDetCot)
                End If
            Next
            Return ListDetalleCotiz
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub AgregarViaticosTourConductor(ByVal BECot As clsCotizacionBE, ByVal vstrIDProveedor As String, _
                                             ByRef LstDet As List(Of clsCotizacionBE.clsDetalleCotizacionBE))
        Try
            Dim ListNewItems As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)

            'Reemplazar los datos de servicios x los del Viatico
            Dim oBEDetCot As clsCotizacionBE.clsDetalleCotizacionBE = Nothing
            Dim ListServxSubServ As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = oDetViaticosTourConductorList(BECot.IdCab, vstrIDProveedor)
            For Each ItemSub As clsCotizacionBE.clsDetalleCotizacionBE In ListServxSubServ
                oBEDetCot = New clsCotizacionBE.clsDetalleCotizacionBE
                With oBEDetCot
                    .IDDet = ItemSub.IDDet
                    .NroPax = ItemSub.NroPax
                    .NroLiberados = ItemSub.NroLiberados
                    .Tipo_Lib = ItemSub.Tipo_Lib
                    .CostoLiberado = ItemSub.CostoLiberado
                    .CostoLiberadoImpto = ItemSub.CostoLiberadoImpto
                    .CostoReal = ItemSub.CostoReal
                    .CostoRealAnt = ItemSub.CostoRealAnt
                    .CostoRealImpto = ItemSub.CostoRealImpto
                    .Cotizacion = ItemSub.Cotizacion
                    .Dia = ItemSub.Dia
                    .Especial = ItemSub.Especial
                    .IDCab = ItemSub.IDCab
                    .IDIdioma = ItemSub.IDIdioma
                    .IDProveedor = ItemSub.IDProveedor
                    .IDTipoProv = ItemSub.IDProveedor
                    .Dias = ItemSub.Dias
                    .IDServicio = ItemSub.IDServicio
                    .IDServicio_Det = ItemSub.IDServicio_Det
                    .IDUbigeo = ItemSub.IDUbigeo
                    .Item = ItemSub.Item
                    .Margen = ItemSub.Margen
                    .MargenAplicado = ItemSub.MargenAplicado
                    .MargenAplicadoAnt = ItemSub.MargenAplicadoAnt
                    .MargenImpto = ItemSub.MargenImpto
                    .MargenLiberado = ItemSub.MargenLiberado
                    .MargenLiberadoImpto = ItemSub.MargenLiberadoImpto
                    .MotivoEspecial = ItemSub.MotivoEspecial
                    .RutaDocSustento = ItemSub.RutaDocSustento
                    .Desayuno = ItemSub.Desayuno
                    .Lonche = ItemSub.Lonche
                    .Almuerzo = ItemSub.Almuerzo
                    .Cena = ItemSub.Cena
                    .Transfer = ItemSub.Transfer
                    .IDDetTransferOri = ItemSub.IDDetTransferOri
                    .IDDetTransferDes = ItemSub.IDDetTransferDes
                    .TipoTransporte = ItemSub.TipoTransporte
                    .IDUbigeoOri = ItemSub.IDUbigeoOri
                    .IDUbigeoDes = ItemSub.IDUbigeoDes
                    .Servicio = ItemSub.Servicio
                    .SSCL = ItemSub.SSCL
                    .SSCLImpto = ItemSub.SSCLImpto
                    .SSCR = ItemSub.SSCR
                    .SSCRImpto = ItemSub.SSCRImpto
                    .SSMargen = ItemSub.SSMargen
                    .SSMargenImpto = ItemSub.SSMargenImpto
                    .SSMargenLiberado = ItemSub.SSMargenLiberado
                    .SSMargenLiberadoImpto = ItemSub.SSMargenLiberadoImpto
                    .SSTotal = ItemSub.SSTotal
                    .SSTotalOrig = ItemSub.SSTotalOrig
                    .STCR = ItemSub.STCR
                    .STCRImpto = ItemSub.STCRImpto
                    .STMargen = ItemSub.STMargen
                    .STMargenImpto = ItemSub.STMargenImpto
                    .STTotal = ItemSub.STTotal
                    .STTotalOrig = ItemSub.STTotalOrig
                    .Total = ItemSub.Total
                    .RedondeoTotal = ItemSub.RedondeoTotal
                    .TotalOrig = ItemSub.TotalOrig
                    .TotImpto = ItemSub.TotImpto
                    .IDDetRel = ItemSub.IDDetRel
                    .IDDetCopia = ItemSub.IDDetCopia
                    .PorReserva = ItemSub.PorReserva
                    .ExecTrigger = ItemSub.ExecTrigger
                    .IDReserva = ItemSub.IDReserva
                    .IncGuia = ItemSub.IncGuia
                    .FueradeHorario = ItemSub.FueradeHorario
                    .ConAlojamiento = ItemSub.ConAlojamiento
                    .CostoRealEditado = ItemSub.CostoRealEditado
                    .CostoRealImptoEditado = ItemSub.CostoRealImptoEditado
                    .MargenAplicadoEditado = ItemSub.MargenAplicadoEditado
                    .ServicioEditado = ItemSub.ServicioEditado
                    .PlanAlimenticio = ItemSub.PlanAlimenticio
                    .IDReserva_Det = ItemSub.IDReserva_Det
                    .NuViaticosTC = ItemSub.NuViaticosTC
                    .FlServicioIngManual = ItemSub.FlServicioIngManual
                    .UserMod = BECot.UserMod
                End With
                ListNewItems.Add(oBEDetCot)
            Next

            'Agregando los nuevos Sub-Servicios como ServiciosReservas
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In ListNewItems
                LstDet.Add(Item)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub ActualizarIDsPostAnulacion(ByVal vstrIDProveedor As String, _
                                ByVal vintIDCab As Integer, _
                                ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_UpdIDsReservasPostAnulacion", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try

    End Sub

    Private Sub ActualizarIDsVentasPostM(ByVal vstrIDProveedor As String, _
                                ByVal vintIDCab As Integer, _
                                ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_DET_UpdIDsVentasPostM", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try

    End Sub
    Public Sub RegenerarReserva_TMP(ByVal vintIDReserva As Integer, ByVal vstrIDProveedor As String, _
                                    ByVal vblnEsAlojamiento As Boolean, ByVal vintIDCab As Integer, ByVal vintIDReservaProtecc As Integer, _
                                    ByVal vsglIgv As Single, ByVal vstrUserMod As String)
        Try
            EliminarRESERVAS_DET_TMP(vintIDReserva)

            Dim BECot As New clsCotizacionBE
            Dim objCotBT As New clsVentasCotizacionBT
            Dim dttCB As DataTable = objCotBT.dttConsultarPk(vintIDCab)

            With BECot
                .IdCab = vintIDCab
                .IDFile = dttCB(0)("IDFile")
                .IDUsuario = dttCB(0)("IDUsuario")
                .IDUsuarioRes = dttCB(0)("IDUsuarioRes")
                .Simple = If(IsDBNull(dttCB(0)("Simple")), 0, dttCB(0)("Simple"))
                .SimpleResidente = If(IsDBNull(dttCB(0)("SimpleResidente")), 0, dttCB(0)("SimpleResidente"))
                .Twin = If(IsDBNull(dttCB(0)("Twin")), 0, dttCB(0)("Twin"))
                .TwinResidente = If(IsDBNull(dttCB(0)("TwinResidente")), 0, dttCB(0)("TwinResidente"))
                .Matrimonial = If(IsDBNull(dttCB(0)("Matrimonial")), 0, dttCB(0)("Matrimonial"))
                .MatrimonialResidente = If(IsDBNull(dttCB(0)("MatrimonialResidente")), 0, dttCB(0)("MatrimonialResidente"))
                .Triple = If(IsDBNull(dttCB(0)("Triple")), 0, dttCB(0)("Triple"))
                .TripleResidente = If(IsDBNull(dttCB(0)("TripleResidente")), 0, dttCB(0)("TripleResidente"))
                .Estado = dttCB(0)("Estado")
                .UserMod = vstrUserMod
                .NroPax = dttCB(0)("NroPax")
                .NroLiberados = If(IsDBNull(dttCB(0)("NroLiberados")), 0, dttCB(0)("NroLiberados"))
                .Tipo_Lib = If(IsDBNull(dttCB(0)("Tipo_Lib")), "", dttCB(0)("Tipo_Lib"))
                .Redondeo = dttCB(0)("Redondeo")
                .MargenCero = dttCB(0)("MargenCero")
                .FlTourConductor = Convert.ToBoolean(dttCB(0)("FlIncluirTC"))
            End With

            Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Dim objDetCotBT As New clsDetalleVentasCotizacionBT
            'Dim dttCotiDet As DataTable = objDetCotBT.ConsultarListxIDCab(vintIDCab, True, False, True)
            Dim dttCotiDet As DataTable = Nothing 'objDetCotBT.ConsultarListxIDCab(vintIDCab, True, False) '>>Nuevo'
            If (vstrIDProveedor = gstrIDProveedorSetoursLima Or vstrIDProveedor = gstrIDProveedorSetoursCusco Or _
                vstrIDProveedor = gstrIDProveedorSetoursBuenosAires Or vstrIDProveedor = gstrIDProveedorSetoursSantiago) Or vblnEsAlojamiento Then
                dttCotiDet = objDetCotBT.ConsultarListxIDCab(vintIDCab, True, False) '>>Nuevo'
            Else
                dttCotiDet = objDetCotBT.ConsultarListSubServiciosxIDCab(vintIDCab, True, False) '>>Nuevo'
                'Si No encuentra Sub-Servicios >> ConsultarDatosServicios
                If dtFiltrarDataTable(dttCotiDet, "IDProveedor='" & vstrIDProveedor & "'").Rows.Count = 0 Then
                    dttCotiDet = objDetCotBT.ConsultarListxIDCab(vintIDCab, True, False) '>>Nuevo'
                End If
            End If
            'Dim ListIDDet As New List(Of Integer)
            For Each dr As DataRow In dttCotiDet.Rows
                'If Not ListIDDet.Contains(dr("IDDet")) Then
                If dr("IDProveedor") = vstrIDProveedor Then
                    Dim ItemDetCot As New clsCotizacionBE.clsDetalleCotizacionBE
                    With ItemDetCot
                        .NroPax = dr("NroPax")
                        .NroLiberados = If(IsDBNull(dr("NroLiberados")), 0, dr("NroLiberados"))
                        .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib"))
                        .CostoLiberado = dr("CostoLiberado")
                        .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                        .CostoReal = dr("CostoReal")
                        .CostoRealAnt = dr("CostoRealAnt")
                        .CostoRealImpto = dr("CostoRealImpto")
                        .Cotizacion = dr("Cotizacion")
                        .Dia = dr("Dia")
                        .Especial = dr("Especial")
                        .IDCab = vintIDCab
                        .IDDet = dr("IDDet")
                        .IDIdioma = dr("IDidioma")
                        .IDProveedor = dr("IDProveedor")
                        .IDTipoProv = dr("IDTipoProv")
                        .IDTipoOper = dr("IDTipoOper")
                        .Dias = dr("Dias")
                        .IDServicio = dr("IDServicio")
                        .IDServicio_Det = dr("IDServicio_Det")
                        .IDUbigeo = dr("IDubigeo")
                        .Item = dr("Item")
                        .Margen = dr("Margen")
                        .MargenAplicado = dr("MargenAplicado")
                        .MargenAplicadoAnt = dr("MargenAplicadoAnt")
                        .MargenImpto = dr("MargenImpto")
                        .MargenLiberado = dr("MargenLiberado")
                        .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                        .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")), "", dr("MotivoEspecial"))
                        .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")), "", dr("RutaDocSustento"))
                        .Desayuno = dr("Desayuno")
                        .Lonche = dr("Lonche")
                        .Almuerzo = dr("Almuerzo")
                        .Cena = dr("Cena")
                        .Transfer = dr("Transfer")
                        .IDDetTransferOri = dr("IDDetTransferOri")
                        .IDDetTransferDes = dr("IDDetTransferDes")
                        .TipoTransporte = If(IsDBNull(dr("TipoTransporte")), "", dr("TipoTransporte"))
                        .IDUbigeoOri = dr("IDUbigeoOri")
                        .IDUbigeoDes = dr("IDUbigeoDes")
                        .Servicio = If(IsDBNull(dr("Servicio")), "", dr("Servicio"))
                        .SSCL = dr("SSCL")
                        .SSCLImpto = dr("SSCLImpto")
                        .SSCR = dr("SSCR")
                        .SSCRImpto = dr("SSCRImpto")
                        .SSMargen = dr("SSMargen")
                        .SSMargenImpto = dr("STMargenImpto")
                        .SSMargenLiberado = dr("SSMargenLiberado")
                        .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                        .SSTotal = dr("SSTotal")
                        .SSTotalOrig = dr("SSTotalOrig")
                        .STCR = dr("STCR")
                        .STCRImpto = dr("STCRImpto")
                        .STMargen = dr("STMargen")
                        .STMargenImpto = dr("STMargenImpto")
                        .STTotal = dr("STTotal")
                        .STTotalOrig = dr("STTotalOrig")
                        .Total = dr("Total")
                        .RedondeoTotal = dr("RedondeoTotal")
                        .TotalOrig = dr("TotalOrig")
                        .TotImpto = dr("TotImpto")
                        .IDDetRel = If(IsDBNull(dr("IDDetRel")), 0, dr("IDDetRel"))
                        .IDDetCopia = If(IsDBNull(dr("IDDetCopia")), 0, dr("IDDetCopia"))
                        .PorReserva = dr("PorReserva")
                        .ExecTrigger = False
                        .IDReserva = dr("IDReserva")
                        .IncGuia = dr("IncGuia")
                        .FueradeHorario = dr("FueradeHorario")
                        .ConAlojamiento = dr("ConAlojamiento")
                        .CostoRealEditado = dr("CostoRealEditado")
                        .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                        .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                        .ServicioEditado = dr("ServicioEditado")
                        .PlanAlimenticio = dr("PlanAlimenticio")
                        .UserMod = vstrUserMod
                        .IDReserva_Det = If(IsDBNull(dr("IDReserva_Det")), 0, dr("IDReserva_Det"))
                        .AcomodoEspecial = dr("AcomodoEspecial")
                        .Capacidad = If(IsDBNull(dr("CapacidadHab")), 0, dr("CapacidadHab"))
                        .EsMatrimonial = If(IsDBNull(dr("EsMatrimonial")), False, dr("EsMatrimonial"))
                        .CantidadAcomodoEsp = dr("CantidadAcomodos")
                        .FlServicioParaGuia = Convert.ToBoolean(dr("ResServGuia"))
                        .FlServicioNoShow = Convert.ToBoolean(dr("FlServicioNoShow"))
                        .FlServicioIngManual = Convert.ToBoolean(dr("FlServicioIngManual"))
                        .ItemAlojamiento = Convert.ToByte(dr("ItemAlojamiento"))
                        .DescAcomodoEspecial = dr("DescAcomodosEspeciales").ToString
                        .FlServicioTC = Convert.ToBoolean(dr("FlServicioTC"))
                        .IDDetAntiguo = dr("IDDetAntiguo")
                    End With
                    ListaCotiDet.Add(ItemDetCot)
                End If
                '    ListIDDet.Add(dr("IDDet"))
                'End If
            Next
            Dim LocalListCotidet As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = ListaCotiDet
            ActualizarFechaOutItemsAlojamiento(ListaCotiDet)
            AgregarViaticosTourConductor(BECot, vstrIDProveedor, ListaCotiDet)
            BECot.ListaDetalleCotiz = ListaCotiDet

            Dim ListaProveed As New List(Of stProveedoresReservas) '= ListaProvReservas
            ListaProveed.Add(New stProveedoresReservas With {.IDProveedor = vstrIDProveedor, _
                                                             .IDReserva = vintIDReserva})

            GrabarxCotizacion2(BECot, False, ListaProveed, , , , , True, vListDetalleCotiz:=LocalListCotidet)
            GrabarxCotizacion2(BECot, True, ListaProveed, , , , , True, vListDetalleCotiz:=LocalListCotidet)

            CambiosPostGeneracionxFile(BECot, ListaProveed, , , , , True)

            If vintIDReservaProtecc <> 0 Then
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDReservaProtecc", vintIDReserva)
                dc.Add("@UserMod", vstrUserMod)
                objADO.ExecuteSP("RESERVAS_DET_TMPUpdIDServicio_DetVarianteAnioxIDReservaProtecc", dc)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".RegenerarReserva_TMP")

        End Try
    End Sub

    Public Sub ActualizarFechaOutItemsAlojamiento(ByRef rListCotizacionDetalle As List(Of clsCotizacionBE.clsDetalleCotizacionBE))
        Try
            Dim datFechaQ As Date = #1/1/1900#, datFechaOutQ As Date = #1/1/2001#
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In rListCotizacionDetalle
                If Item.IDTipoProv = gstrTipoProvHotel And Not Item.PlanAlimenticio Then
                    If Item.ItemAlojamiento = 1 Then
                        Item.FechaOut_OperAlojamiento = datDevuelveFechaOut(Item.IDTipoProv, Item.IDServicio_Det, Item.IDDet, _
                                                                            rListCotizacionDetalle, Item.DescAcomodoEspecial, Item.IncGuia, Item.Dia)
                        'If Item.FechaOut_OperAlojamiento.Year = 1 Then Item.FechaOut_OperAlojamiento = datDevuelveFechaOut(Item.IDTipoProv, Item.IDServicio_Det, Item.IDDet, rListCotizacionDetalle, datFechaQ)
                        datFechaQ = Item.Dia : datFechaOutQ = Item.FechaOut_OperAlojamiento
                        Item.ItemAlojamiento = 0
                    End If
                ElseIf Item.IDTipoProv <> gstrTipoProvHotel And Item.ConAlojamiento Then
                    If (Item.IDDetRel.Trim = "0" Or Item.IDDetRel.Trim = "") Then
                        Item.FechaOut_OperAlojamiento = datDevuelveFechaOut(Item.IDTipoProv, Item.IDServicio_Det, Item.IDDet, _
                                                                            rListCotizacionDetalle, Item.DescAcomodoEspecial, Item.IncGuia, Item.Dia)
                        Item.ItemAlojamiento = 0
                    End If
                End If
            Next
            rListCotizacionDetalle = (From item2 In rListCotizacionDetalle _
                                      Where item2.ItemAlojamiento = 0 _
                                      Select item2).ToList()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function dttCreaDttDetCotiServ() As DataTable

        Dim dttDetCotiServ As New DataTable("DetCotiServ")
        With dttDetCotiServ
            .Columns.Add("IDDet")
            .Columns.Add("IDServicio")
            .Columns.Add("IDServicio_Det")
            .Columns.Add("CostoReal")
            .Columns.Add("Margen")
            .Columns.Add("CostoLiberado")
            .Columns.Add("MargenLiberado")
            .Columns.Add("MargenAplicado")
            .Columns.Add("Total")
            .Columns.Add("RedondeoTotal")
            .Columns.Add("SSCR")
            .Columns.Add("SSMargen")
            .Columns.Add("SSCL")
            .Columns.Add("SSML")
            .Columns.Add("SSTotal")
            .Columns.Add("STCR")
            .Columns.Add("STMargen")
            .Columns.Add("STTotal")

            .Columns.Add("CostoRealImpto")
            .Columns.Add("CostoLiberadoImpto")
            .Columns.Add("MargenImpto")
            .Columns.Add("MargenLiberadoImpto")
            .Columns.Add("SSCRImpto")
            .Columns.Add("SSCLImpto")
            .Columns.Add("SSMargenImpto")
            .Columns.Add("SSMargenLiberadoImpto")
            .Columns.Add("STCRImpto")
            .Columns.Add("STMargenImpto")
            .Columns.Add("TotImpto")
        End With


        Return dttDetCotiServ
    End Function

    '    Public Sub ReCalculoReservaProtecc(ByVal vintIDCab As Int32, _
    '                       ByVal vintPax As Int16, ByVal vintLiberados As Int16, _
    '                       ByVal vstrTipo_Lib As Char, ByVal vstrTipoPax As String, _
    '                       ByVal vdecRedondeo As Single, _
    '                       ByVal vsglIGV As Single, ByVal vstrAccion As Char, ByVal vstrUser As String, _
    '                       ByVal vblnMargenCero As Boolean, ByVal vintIDReserva As Integer, _
    '                       ByVal vbytSimple As Byte, ByVal vbytTwin As Byte, ByVal vbytMatrimonial As Byte, ByVal vbytTriple As Byte)
    '        Try
    '            'Dim objCotiDet As New clsCotizacionBN.clsDetalleCotizacionBN
    '            Dim objResDet As New clsDetalleReservaBT

    '            Dim objCoti As New clsCotizacionBN
    '            Dim objBE As New clsReservaBE
    '            'Dim drResDetQ As SqlClient.SqlDataReader
    '            'Dim objBLDetQ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT

    '            Dim dttResDet As DataTable = ConsultarxIDReservaTMP2(vintIDReserva)

    '            Dim objBEDet As clsReservaBE.clsDetalleReservaBE
    '            Dim ListaDetRes As List(Of clsReservaBE.clsDetalleReservaBE)
    '            Dim dc As Dictionary(Of String, Double)
    '            Dim dttDetCotiServ As DataTable
    '            Dim dblCostoReal As Double, dblMargenAplicado As Double

    '            'Dim objADODN As New clsDataAccessDN
    '            'Dim cnn As SqlClient.SqlConnection = objADODN.GetConnection
    '            'Dim cnn2 As SqlClient.SqlConnection = objADODN.GetConnection

    '            Dim bytCapacMax As Byte = 0

    '            For Each drResDet As DataRow In dttResDet.Rows

    '                'If Not blnListaTieneCambio(drResDet("IDDet").ToString, vListCotiDet) Then
    '                '    GoTo Sgte
    '                'End If



    '                'If drResDet("IDDetRel") <> 0 Then 'Or drResDet("IDDetCopia") <> 0 Then
    '                '    GoTo Sgte
    '                'End If

    '                'If drResDet("IDDet") = 4618 Then
    '                '    dblCostoReal = -1
    '                'End If

    '                dblCostoReal = -1
    '                dblMargenAplicado = -1
    '                If drResDet("Especial") = True Then
    '                    If drResDet("CostoRealEditado") = True Then
    '                        dblCostoReal = drResDet("CostoReal")
    '                    End If
    '                    If drResDet("MargenAplicadoEditado") = True Then
    '                        dblMargenAplicado = drResDet("MargenAplicado")
    '                    End If

    '                End If

    '                dttDetCotiServ = dttCreaDttDetCotiServ()

    '                Dim strIDTipoProv As String = drResDet("IDTipoProv").ToString


    '                If drResDet("IncGuia") = True Then
    '                    strIDTipoProv = "003" 'cualquier tipo q no sea hotel ni restaurant
    '                Else
    '                    vintPax = drResDet("NroPax").ToString
    '                End If
    '                vintLiberados = If(IsDBNull(drResDet("NroLiberados")), "0", drResDet("NroLiberados").ToString)
    '                vstrTipo_Lib = If(IsDBNull(drResDet("Tipo_Lib")), "", drResDet("Tipo_Lib").ToString)


    '                dc = objCoti.CalculoCotizacion(drResDet("IDServicio").ToString, drResDet("IDServicio_Det").ToString, _
    '                    drResDet("Anio").ToString, drResDet("Tipo").ToString, _
    '                    strIDTipoProv, vintPax, vintLiberados, vstrTipo_Lib, _
    '                    drResDet("MargenCotiCab").ToString, drResDet("TipoCambioCotiCab").ToString, _
    '                    dblCostoReal, dblMargenAplicado, vstrTipoPax, vdecRedondeo, dttDetCotiServ, _
    '                    vsglIGV, , drResDet("IncGuia"), vblnMargenCero, , , drResDet("Dia"))

    '                ListaDetRes = New List(Of clsReservaBE.clsDetalleReservaBE)
    '                objBEDet = New clsReservaBE.clsDetalleReservaBE

    '                For Each dic As KeyValuePair(Of String, Double) In dc
    '                    If dic.Key = "CostoReal" Then objBEDet.CostoReal = dic.Value
    '                    If dic.Key = "Margen" Then objBEDet.Margen = dic.Value
    '                    If dic.Key = "CostoLiberado" Then objBEDet.CostoLiberado = dic.Value
    '                    If dic.Key = "MargenLiberado" Then objBEDet.MargenLiberado = dic.Value
    '                    If dic.Key = "MargenAplicado" Then objBEDet.MargenAplicado = dic.Value
    '                    If dic.Key = "TotImpto" Then objBEDet.TotImpto = dic.Value
    '                    If dic.Key = "Total" Then objBEDet.Total = dic.Value
    '                    If dic.Key = "RedondeoTotal" Then
    '                        objBEDet.RedondeoTotal = dic.Value
    '                    End If

    '                    If dic.Key = "SSCR" Then objBEDet.SSCR = dic.Value
    '                    If dic.Key = "SSMargen" Then objBEDet.SSMargen = dic.Value
    '                    If dic.Key = "SSCL" Then objBEDet.SSCL = dic.Value
    '                    If dic.Key = "SSML" Then objBEDet.SSMargenLiberado = dic.Value
    '                    If dic.Key = "SSTotal" Then objBEDet.SSTotal = dic.Value
    '                    If dic.Key = "STCR" Then objBEDet.STCR = dic.Value
    '                    If dic.Key = "STMargen" Then objBEDet.STMargen = dic.Value
    '                    If dic.Key = "STTotal" Then objBEDet.STTotal = dic.Value

    '                    If dic.Key = "CostoRealImpto" Then objBEDet.CostoRealImpto = dic.Value
    '                    If dic.Key = "CostoLiberadoImpto" Then objBEDet.CostoLiberadoImpto = dic.Value
    '                    If dic.Key = "MargenImpto" Then objBEDet.MargenImpto = dic.Value
    '                    If dic.Key = "MargenLiberadoImpto" Then objBEDet.MargenLiberadoImpto = dic.Value
    '                    If dic.Key = "SSCLImpto" Then objBEDet.SSCLImpto = dic.Value
    '                    If dic.Key = "SSCRImpto" Then objBEDet.SSCRImpto = dic.Value
    '                    If dic.Key = "SSMargenImpto" Then objBEDet.SSMargenImpto = dic.Value
    '                    If dic.Key = "SSMargenLiberadoImpto" Then objBEDet.SSMargenLiberadoImpto = dic.Value
    '                    If dic.Key = "STCRImpto" Then objBEDet.STCRImpto = dic.Value
    '                    If dic.Key = "STMargenImpto" Then objBEDet.STMargenImpto = dic.Value
    '                Next



    '                With objBEDet
    '                    'If vstrAccion = "M" Then
    '                    '    .IDDet_Q = intConsultarIDDet_QxIDCab_Q(vintIDCab_Q)
    '                    'End If
    '                    .IDReserva_Det = drResDet("IDReserva_Det")
    '                    .IDReserva = drResDet("IDReserva")
    '                    .IDFile = drResDet("IDFile")
    '                    .Cotizacion = drResDet("Cotizacion").ToString
    '                    .Item = drResDet("Item").ToString
    '                    .Dia = drResDet("Dia").ToString
    '                    .FechaOut = drResDet("FechaOut").ToString
    '                    .IDUbigeo = drResDet("IDUbigeo").ToString
    '                    .IDProveedor = drResDet("IDProveedor").ToString
    '                    .IDServicio = drResDet("IDServicio").ToString
    '                    .IDServicio_Det = drResDet("IDServicio_Det").ToString
    '                    .IDIdioma = drResDet("IDIdioma").ToString
    '                    .MotivoEspecial = drResDet("MotivoEspecial").ToString
    '                    .RutaDocSustento = drResDet("RutaDocSustento").ToString
    '                    .Desayuno = drResDet("Desayuno")
    '                    .Lonche = drResDet("Lonche")
    '                    .Almuerzo = drResDet("Almuerzo")
    '                    .Cena = drResDet("Cena")
    '                    .Transfer = drResDet("Transfer")
    '                    '.OrigenTransfer = drResDet("OrigenTransfer")
    '                    '.DestinoTransfer = drResDet("DestinoTransfer")
    '                    .IDDetTransferOri = drResDet("IDDetTransferOri")
    '                    .IDDetTransferDes = drResDet("IDDetTransferDes")
    '                    .TipoTransporte = If(IsDBNull(drResDet("TipoTransporte")) = True, "", drResDet("TipoTransporte").ToString)
    '                    .IDUbigeoOri = drResDet("IDUbigeoOri")
    '                    .IDUbigeoDes = drResDet("IDUbigeoDes")
    '                    .Servicio = drResDet("Servicio").ToString
    '                    .IDDetRel = 0
    '                    .IDDet = drResDet("IDDet").ToString
    '                    .NroPax = drResDet("NroPax").ToString
    '                    .NroLiberados = If(IsDBNull(drResDet("NroLiberados")) = True, "0", drResDet("NroLiberados").ToString)
    '                    .Tipo_Lib = If(IsDBNull(drResDet("Tipo_Lib")) = True, "", drResDet("Tipo_Lib"))
    '                    .TotalOrig = drResDet("TotalOrig").ToString
    '                    .SSTotalOrig = drResDet("SSTotalOrig").ToString
    '                    .STTotalOrig = drResDet("STTotalOrig").ToString
    '                    '.IDCab_Q = vintIDCab_Q
    '                    '.CostoLiberadoImpto = .CostoLiberado * (1 + (vsglIGV / 100))
    '                    '.CostoRealImpto = .CostoReal * (1 + (vsglIGV / 100))
    '                    '.MargenImpto = .Margen * (1 + (vsglIGV / 100))
    '                    '.MargenLiberadoImpto = .MargenLiberado * (1 + (vsglIGV / 100))
    '                    '.SSCLImpto = .SSCL * (1 + (vsglIGV / 100))
    '                    '.SSCRImpto = .SSCR * (1 + (vsglIGV / 100))
    '                    '.SSMargenImpto = .SSMargen * (1 + (vsglIGV / 100))
    '                    '.SSMargenLiberadoImpto = .SSMargenLiberado * (1 + (vsglIGV / 100))
    '                    '.STCRImpto = .STCR * (1 + (vsglIGV / 100))
    '                    '.STMargenImpto = .STMargen * (1 + (vsglIGV / 100))
    '                    '.TotImpto = .CostoLiberadoImpto + .CostoRealImpto + .MargenImpto + .MargenLiberadoImpto '+ _
    '                    '.SSCLImpto(+.SSCRImpto + .SSMargenImpto + .SSMargenLiberadoImpto + .STCRImpto + .STMargenImpto)
    '                    .Especial = drResDet("Especial").ToString
    '                    .CostoRealAnt = 0
    '                    .MargenAplicadoAnt = 0
    '                    .UserMod = vstrUser
    '                    .Recalcular = False
    '                    .ExecTrigger = drResDet("ExecTrigger")
    '                    .IncGuia = drResDet("IncGuia")
    '                    .FueradeHorario = drResDet("FueradeHorario")
    '                    .CostoRealEditado = drResDet("CostoRealEditado")
    '                    .CostoRealImptoEditado = drResDet("CostoRealImptoEditado")
    '                    .MargenAplicadoEditado = drResDet("MargenAplicadoEditado")
    '                    .ServicioEditado = drResDet("ServicioEditado")

    '                    .IDGuiaProveedor = If(IsDBNull(drResDet("IDGuiaProveedor")) = True, "", drResDet("IDGuiaProveedor").ToString)
    '                    .IDEmailEdit = If(IsDBNull(drResDet("IDEmailEdit")) = True, "", drResDet("IDEmailEdit").ToString)
    '                    .IDEmailNew = If(IsDBNull(drResDet("IDEmailNew")) = True, "", drResDet("IDEmailNew").ToString)
    '                    .IDEmailRef = If(IsDBNull(drResDet("IDEmailRef")) = True, "", drResDet("IDEmailRef").ToString)
    '                    .IDMoneda = If(IsDBNull(drResDet("IDMoneda")) = True, "", drResDet("IDMoneda").ToString)
    '                    .Accion = vstrAccion


    '                    PasarDatosPropiedadesaCostosDetReservasBE(objBEDet, vbytSimple, vbytTwin, vbytMatrimonial, vbytTriple, 0, 0, 0, 0, 0, _
    '                                                              drResDet("TipoCambioCotiCab"), Nothing, , , , , ListaDetRes)

    '                    If bytCapacMax = 0 Then
    '                        If vbytTriple <> 0 Then
    '                            bytCapacMax = 3
    '                        ElseIf vbytTwin <> 0 Or vbytMatrimonial <> 0 Then
    '                            bytCapacMax = 2
    '                        ElseIf vbytSimple <> 0 Then
    '                            bytCapacMax = 1
    '                        End If
    '                    Else
    '                        bytCapacMax -= 1
    '                    End If
    '                    .Cantidad = (.NroPax + .NroLiberados) / bytCapacMax
    '                    .CapacidadHab = bytCapacMax
    '                    If .IncGuia = True Then
    '                        .Cantidad = 1
    '                    End If
    '                    .Noches = DateDiff(DateInterval.Day, .Dia, .FechaOut)
    '                End With


    '                ListaDetRes.Add(objBEDet)



    '                'Det. Cotiz. Serv*****
    '                Dim objBEDetS As clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE
    '                Dim ListaDetCotS As New List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE)

    '                For Each drDetCotiServ As DataRow In dttDetCotiServ.Rows
    '                    objBEDetS = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE
    '                    With objBEDetS
    '                        .IDServicio_Det = drDetCotiServ("IDServicio_Det")
    '                        .IDDet = drResDet("IDDet")
    '                        .CostoLiberado = drDetCotiServ("CostoLiberado")
    '                        .CostoLiberadoImpto = drDetCotiServ("CostoLiberadoImpto")
    '                        .CostoReal = drDetCotiServ("CostoReal")
    '                        .CostoRealImpto = drDetCotiServ("CostoRealImpto")

    '                        .Margen = drDetCotiServ("Margen")
    '                        .MargenAplicado = drDetCotiServ("MargenAplicado")
    '                        .MargenImpto = drDetCotiServ("MargenImpto")
    '                        .MargenLiberado = drDetCotiServ("MargenLiberado")
    '                        .MargenLiberadoImpto = drDetCotiServ("MargenLiberadoImpto")

    '                        .SSCL = drDetCotiServ("SSCL")
    '                        .SSCLImpto = drDetCotiServ("SSCLImpto")
    '                        .SSCR = drDetCotiServ("SSCR")
    '                        .SSCRImpto = drDetCotiServ("SSCRImpto")
    '                        .SSMargen = drDetCotiServ("SSMargen")
    '                        .SSMargenImpto = drDetCotiServ("STMargenImpto")
    '                        .SSMargenLiberado = drDetCotiServ("SSML")
    '                        .SSMargenLiberadoImpto = drDetCotiServ("SSMargenLiberadoImpto")
    '                        .SSTotal = drDetCotiServ("SSTotal")
    '                        .STCR = drDetCotiServ("STCR")
    '                        .STCRImpto = drDetCotiServ("STCRImpto")
    '                        .STMargen = drDetCotiServ("STMargen")
    '                        .STMargenImpto = drDetCotiServ("STMargenImpto")
    '                        .STTotal = drDetCotiServ("STTotal")
    '                        .Total = drDetCotiServ("Total")
    '                        .RedondeoTotal = drDetCotiServ("RedondeoTotal")
    '                        .TotImpto = drDetCotiServ("TotImpto")
    '                        .UserMod = vstrUser

    '                    End With
    '                    ListaDetCotS.Add(objBEDetS)
    '                Next

    '                'objBE = New clsCotizacionBE
    '                objBE.ListaDetServiciosDetReservas = ListaDetCotS
    '                objBE.ListaDetReservas = ListaDetRes


    '                objBE.UserMod = vstrUser
    '                objBE.IDCab = vintIDCab

    '                'End If



    '                'objResDet.InsertarActualizarEliminar(objBE, 0)
    '                objResDet.InsertarActualizarEliminarTMP(objBE, 0)

    'Sgte:

    '            Next


    '            ContextUtil.SetComplete()

    '        Catch ex As Exception
    '            ContextUtil.SetAbort()
    '            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ReCalculoReservaProtecc")

    '        End Try
    '    End Sub


    Private Sub ActualizarRegeneradoxAcomodo(ByVal vintIDReserva As Integer, ByVal vblnRegeneradoxAcomodo As Boolean, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@RegeneradoxAcomodo", vblnRegeneradoxAcomodo)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("RESERVAS_UpdRegeneradoxAcomodo", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarRegeneradoxAcomodo")
        End Try
    End Sub

    Private Function blnExisteEnReservaDetAntiguo(ByVal vintIDReserva_Det As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            dc.Add("@pExisteEnAntiguos", False)

            Return objADO.GetSPOutputValue("RESERVAS_DET_SelExistsEnAntiguos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub MatchDetReservasRealyTMP(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, _
                                        ByVal vstrIDProveedor As String, ByVal vblnEsAlojamiento As Boolean, ByVal vstrUserMod As String, _
                                        ByVal vintIDReservaProtecc As Integer, ByVal vListaServ As List(Of clsReservaBE.clsServiciosCambiosVentasReservasBE), _
                                        ByVal vblnAnularReserva As Boolean)
        Try
            Dim objDet As New clsDetalleReservaBT
            Dim blnSeAnulo As Boolean = False

            If vblnAnularReserva Then
                Dim objBTReserva As New clsReservaBT
                objBTReserva.ActualizarEstado(vintIDReserva, "XL", vstrUserMod)
                ActualizarCambiosFechasVentasPostFile(vintIDReserva, vstrUserMod)
                GoTo FinGeneracion
            End If

            For Each ItemServ As clsReservaBE.clsServiciosCambiosVentasReservasBE In vListaServ
                If ItemServ.Accion = "Eliminar" Then

                    Dim objOpeDetBT As New clsDetalleOperacionBT
                    Dim objOrdPagBT As New clsOrdenPagoBT
                    If objOpeDetBT.blnExistenOperaciones_DetxIDReserva_Det(ItemServ.IDReserva_Det) Then

                        objDet.Anular(ItemServ.IDReserva_Det, True, vstrUserMod)
                        blnSeAnulo = True
                    ElseIf objOrdPagBT.blnExiste(vintIDReserva) Then
                        objDet.Anular(ItemServ.IDReserva_Det, True, vstrUserMod)
                        blnSeAnulo = True
                    ElseIf blnExisteEnReservaDetAntiguo(ItemServ.IDReserva_Det) Then
                        objDet.Anular(ItemServ.IDReserva_Det, True, vstrUserMod)
                        blnSeAnulo = True
                    Else
                        objDet.Eliminar(ItemServ.IDReserva_Det)
                    End If
                End If
            Next


            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            Dim dttResTmp As DataTable = objADO.GetDataTable("RESERVAS_DET_TMP_Sel", dc)
            If dttResTmp.Rows.Count > 0 Then
                objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDReserva", dc)
                objADO.ExecuteSP("RESERVAS_DETSERVICIOS_DelxIDReserva", dc)
            End If

            'INSERTAR, ACTUALIZAR

            'For Each drDetResTmp As DataRow In dttResTmp.Rows
            '    If blnExisteAcomodoenReal(vintIDReserva, drDetResTmp("IDServicio_Det"), _
            '                        drDetResTmp("CapacidadHab"), drDetResTmp("EsMatrimonial"), drDetResTmp("Dia")) Then

            '        objDet.ActualizarxNuevoAcomodoTMP(vintIDReserva, drDetResTmp("IDReserva_Det"), vstrUserMod)

            '    Else
            '        Dim strIDReserva_Det As String = _
            '        objDet.InsertarxNuevoAcomodoTMP(vintIDReserva, drDetResTmp("IDServicio_Det"), _
            '                        drDetResTmp("CapacidadHab"), drDetResTmp("EsMatrimonial"), drDetResTmp("Dia"), vstrUserMod)
            '    End If
            'Next

            For Each ItemServ As clsReservaBE.clsServiciosCambiosVentasReservasBE In vListaServ
                If ItemServ.Accion = "Actualizar" Then
                    objDet.ActualizarxNuevoAcomodoTMP(vintIDReserva, ItemServ.IDReserva_Det, vstrUserMod)
                ElseIf ItemServ.Accion = "Nuevo" Then
                    Dim strIDReserva_Det As String = _
                    objDet.InsertarxNuevoAcomodoTMP(vintIDReserva, ItemServ.IDServicio_Det, _
                                    ItemServ.CapacidadHab, ItemServ.EsMatrimonial, ItemServ.Dia, _
                                    vstrUserMod, ItemServ.IDReserva_Det)
                    ''If Not (vblnEsAlojamiento Or (vstrIDProveedor = gstrIDProveedorSetoursLima Or vstrIDProveedor = gstrIDProveedorSetoursCusco Or _
                    ''                              vstrIDProveedor = gstrIDProveedorSetoursBuenosAires Or vstrIDProveedor = gstrIDProveedorSetoursSantiago)) Then
                    ''    objResDetServ.GrabarxReserva(vintIDCab, vintIDReserva, vstrUserMod, ItemServ.IDServicio_Det)
                    ''End If
                End If
            Next

            ActualizarDetPaxPostGeneracion(vintIDReserva, vblnEsAlojamiento, vstrUserMod)
            Dim objResDetServ As New clsReservaBT.clsDetalleReservaBT.clsDetalleReservaDetServiciosBT
            If vblnEsAlojamiento Or (vstrIDProveedor = gstrIDProveedorSetoursLima Or vstrIDProveedor = gstrIDProveedorSetoursCusco Or _
                                     vstrIDProveedor = gstrIDProveedorSetoursBuenosAires Or vstrIDProveedor = gstrIDProveedorSetoursSantiago) Then
                objResDetServ.GrabarxReserva(vintIDCab, vintIDReserva, vstrUserMod)
                If Not vblnEsAlojamiento Then
                    If vstrIDProveedor = gstrIDProveedorSetoursLima Then
                        objResDetServ.GrabarxIDReservaxIDCabViatico(vintIDCab, vintIDReserva, vstrUserMod)
                    End If
                End If
            Else
                objResDetServ.GrabarxServicio(vintIDCab, vintIDReserva, vstrUserMod)
            End If

            ActualizarRegeneradoxAcomodo(vintIDReserva, True, vstrUserMod)
            EliminarLogVentas(vintIDCab, vstrIDProveedor)

            If Not blnReservaEsProtegida(vintIDCab, vintIDReserva) Then
                ActualizarEstado(vintIDReserva, "NV", vstrUserMod)
            End If

            EliminarRESERVAS_DET_TMP(vintIDReserva)
            ActualizarCambiosenVentasPostFile(vintIDReserva, False, vstrUserMod)

            'If blnSeAnulo Then
            ActualizarIDsPostAnulacion(vstrIDProveedor, vintIDCab, vstrUserMod)
            'End If
            ActualizarIDsVentasPostM(vstrIDProveedor, vintIDCab, vstrUserMod)
            ActualizarCambiosFechasVentasPostFile(vintIDReserva, vstrUserMod)
            AnularReservaxCantidadDetalle(vintIDReserva, vstrUserMod)

FinGeneracion:
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".MatchDetReservasRealyTMP")
        End Try
    End Sub

    Private Sub AnularReservaxCantidadDetalle(ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDReserva", vintIDReserva)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("RESERVAS_AnuNoExistDetalle", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AnularReservaxCantidadDetalle")
        End Try
    End Sub

    Public Sub EliminarRESERVAS_DET_TMP(ByVal vintIDReserva As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            objADO.ExecuteSP("RESERVAS_DET_TMP_DelxIDReserva", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarRESERVAS_DET_TMP")
        End Try

    End Sub

    Private Sub ActualizarCambiosFechasVentasPostFile(ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_DET_UpdFeUpdRegReservasxIDReserva", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCambiosFechasVentasPostFile")
        End Try
    End Sub

    Public Sub ActualizarCambiosenVentasPostFile(ByVal vintIDReserva As Integer, _
                                       ByVal vblnCambiosenVentasPostFile As Boolean, _
                                       ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@CambiosenVentasPostFile", vblnCambiosenVentasPostFile)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("RESERVAS_UpdCambiosenVentasPostFile", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCambiosenVentasPostFile")
        End Try

    End Sub
    'Private Sub ActualizarDetServicioPostGeneracion(ByVal vintIDReserva As Integer, _
    '                  ByVal vstrUserMod As String)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        'dc.Add("@IDReserva_DetTmp", vintIDReserva_DetTmp)
    '        'dc.Add("@IDReserva_DetNue", vintIDReserva_DetNue)
    '        dc.Add("@IDReserva", vintIDReserva)
    '        dc.Add("@UserMod", vstrUserMod)

    '        objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxAcomodoTMP", dc)
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxNuevoAcomodoTMP")
    '    End Try
    'End Sub

    Private Function blnExisteAcomodoenReal(ByVal vintIDReserva As Integer, ByVal vintIDServicio_Det As Integer, _
                                      ByVal vbytCapacidadHab As Byte, ByVal vblnEsMatrimonial As Boolean, _
                                      ByVal vdatDia As Date) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@CapacidadHab", vbytCapacidadHab)
            dc.Add("@EsMatrimonial", vblnEsMatrimonial)
            dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("RESERVAS_DET_SelExisteAcomodoOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExisteAcomodoenReal")
        End Try
    End Function

    Private Function blnReservaEsProtegida(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pEsProtegido", False)

            Return objADO.ExecuteSPOutput("RESERVAS_SelIDReservaProteccxIDReservaOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnReservaEsProtegida")
        End Try
    End Function

    Private Function blnExisteAcomodoenTMP(ByVal vintIDReserva As Integer, ByVal vintIDServicio_Det As Integer, _
                                      ByVal vbytCapacidadHab As Byte, ByVal vblnEsMatrimonial As Boolean, _
                                      ByVal vdatDia As Date) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@CapacidadHab", vbytCapacidadHab)
            dc.Add("@EsMatrimonial", vblnEsMatrimonial)
            dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@pExiste", 0)

            Return objADO.ExecuteSPOutput("RESERVAS_DET_TMP_SelExisteAcomodoOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExisteAcomodoenTMP")
        End Try
    End Function
    Public Function blnExistenReservasxIDCab(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("RESERVAS_Sel_ExistenxIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub ActualizarCodReservaProvxProveedor(ByVal vintIDCab As Integer, _
                                        ByVal vstrIDProveedor As String, _
                                        ByVal vstrCodReservaProv As String, _
                                        ByVal vstrObservaciones As String, _
                                        ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@CodReservaProv", vstrCodReservaProv)
            dc.Add("@Observaciones", vstrObservaciones)
            dc.Add("@UserMod", vstrUserMod)


            objADO.ExecuteSP("RESERVAS_UpdCodReservaxProveedor", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCodReservaProv")
        End Try
    End Sub
    Public Sub ActualizarEstadoxProveedor(ByVal vintIDCab As Integer, _
                                        ByVal vstrIDProveedor As String, _
                                        ByVal vstrEstado As String, _
                                        ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@Estado", vstrEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_UpdEstadoxProveedor", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCodReservaProv")
        End Try
    End Sub

    Private Sub ActualizarEstado(ByVal vintIDReserva As Integer, _
                                        ByVal vstrEstado As String, _
                                        ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDEstado", vstrEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_UpdEstado2", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCodReservaProv")
        End Try
    End Sub

    Private Sub EliminarLogVentas(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)

            objADO.ExecuteSP("COTIDET_LOG_DelxIDCabProveedor", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarLogVentas")
        End Try
    End Sub
    Private Sub PasarDatosListaCotiDetRegeneracion(ByVal vstrIDProveedor As String, _
            ByVal vintIDCab As Integer, _
            ByVal vintIDReserva As Integer, _
            ByVal vstrUserMod As String, _
            ByVal vdttCotiDet As DataTable, _
            ByRef rListaCotiDet As List(Of clsCotizacionBE.clsDetalleCotizacionBE))

        Try
            For Each dr As DataRow In vdttCotiDet.Rows
                If dr("IDProveedor") = vstrIDProveedor Then
                    Dim ItemDetCot As New clsCotizacionBE.clsDetalleCotizacionBE
                    With ItemDetCot
                        .NroPax = dr("NroPax")
                        .NroLiberados = If(IsDBNull(dr("NroLiberados")), 0, dr("NroLiberados"))
                        .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib"))
                        .CostoLiberado = dr("CostoLiberado")
                        .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                        .CostoReal = dr("CostoReal")
                        .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")), 0, dr("CostoRealAnt"))
                        .CostoRealImpto = dr("CostoRealImpto")
                        .Cotizacion = dr("Cotizacion")
                        .Dia = dr("Dia")
                        .Especial = dr("Especial")
                        .IDCab = vintIDCab
                        .IDDet = dr("IDDet")
                        .IDIdioma = dr("IDidioma")
                        .IDProveedor = dr("IDProveedor")
                        .IDTipoProv = dr("IDTipoProv")
                        .Dias = dr("Dias")
                        .IDServicio = dr("IDServicio")
                        .IDServicio_Det = dr("IDServicio_Det")
                        .IDUbigeo = dr("IDubigeo")
                        .Item = dr("Item")
                        .Margen = dr("Margen")
                        .MargenAplicado = dr("MargenAplicado")
                        .MargenAplicadoAnt = If(IsDBNull(dr("MargenAplicadoAnt")), 0, dr("MargenAplicadoAnt"))
                        .MargenImpto = dr("MargenImpto")
                        .MargenLiberado = dr("MargenLiberado")
                        .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                        .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")), "", dr("MotivoEspecial"))
                        .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")), "", dr("RutaDocSustento"))
                        .Desayuno = dr("Desayuno")
                        .Lonche = dr("Lonche")
                        .Almuerzo = dr("Almuerzo")
                        .Cena = dr("Cena")
                        .Transfer = dr("Transfer")
                        .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")), 0, dr("IDDetTransferOri"))
                        .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")), 0, dr("IDDetTransferDes"))
                        .TipoTransporte = If(IsDBNull(dr("TipoTransporte")), "", dr("TipoTransporte"))
                        .IDUbigeoOri = dr("IDUbigeoOri")
                        .IDUbigeoDes = dr("IDUbigeoDes")
                        .Servicio = If(IsDBNull(dr("Servicio")), "", dr("Servicio"))
                        .SSCL = dr("SSCL")
                        .SSCLImpto = dr("SSCLImpto")
                        .SSCR = dr("SSCR")
                        .SSCRImpto = dr("SSCRImpto")
                        .SSMargen = dr("SSMargen")
                        .SSMargenImpto = dr("STMargenImpto")
                        .SSMargenLiberado = dr("SSMargenLiberado")
                        .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                        .SSTotal = dr("SSTotal")
                        .SSTotalOrig = dr("SSTotalOrig")
                        .STCR = dr("STCR")
                        .STCRImpto = dr("STCRImpto")
                        .STMargen = dr("STMargen")
                        .STMargenImpto = dr("STMargenImpto")
                        .STTotal = dr("STTotal")
                        .STTotalOrig = dr("STTotalOrig")
                        .Total = dr("Total")
                        .RedondeoTotal = dr("RedondeoTotal")
                        .TotalOrig = dr("TotalOrig")
                        .TotImpto = dr("TotImpto")
                        .IDDetRel = If(IsDBNull(dr("IDDetRel")), 0, dr("IDDetRel"))
                        .IDDetCopia = If(IsDBNull(dr("IDDetCopia")), 0, dr("IDDetCopia"))
                        '.Recalcular = False
                        .PorReserva = dr("PorReserva")
                        .ExecTrigger = dr("ExecTrigger")
                        .IDReserva = If(vintIDReserva = 0, dr("IDReserva"), vintIDReserva)
                        '.Reservado = dr("Reservado")
                        '.ExecTrigger = dr("ExecTrigger")
                        .IncGuia = dr("IncGuia")
                        .FueradeHorario = dr("FueradeHorario")
                        .ConAlojamiento = dr("ConAlojamiento")
                        .CostoRealEditado = dr("CostoRealEditado")
                        .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                        .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                        .ServicioEditado = dr("ServicioEditado")
                        .PlanAlimenticio = dr("PlanAlimenticio")
                        '.ActualizQuiebre = dr("ActualizQuiebre")
                        .IDReserva_Det = dr("IDReserva_Det")
                        .IDDetAntiguo = dr("IDDetAntiguo")
                        .UserMod = vstrUserMod
                    End With
                    rListaCotiDet.Add(ItemDetCot)
                End If
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub RecalcularVentasxPaxNoShow(ByVal vintIDCab As Integer, ByVal vstrUserMod As String, _
                                           ByVal vListaNoShowPax As List(Of Integer), _
                                           ByVal vListaShowPax As List(Of Integer))
        Try
            Dim blnEntro As Boolean = False
            If vListaNoShowPax.Count > 0 Then
                Dim objDetPax As New clsDetallePaxVentasCotizacionBT
                For Each intIDPax As Integer In vListaNoShowPax
                    'objDetPax.EliminarxIDPax(intIDPax)
                    objDetPax.ActivarDesactxIDPaxNoShow(vintIDCab, intIDPax, True, vstrUserMod)
                Next

                Dim objCabBT As New clsVentasCotizacionBT
                objCabBT.ActualizarNroPax(vintIDCab, vListaNoShowPax.Count * (-1), vstrUserMod)
                blnEntro = True
            ElseIf vListaShowPax.Count > 0 Then
                Dim objDetPax As New clsDetallePaxVentasCotizacionBT
                For Each intIDPax As Integer In vListaShowPax
                    'objDetPax.InsertarxIDPax(vintIDCab, intIDPax, vstrUserMod)
                    objDetPax.ActivarDesactxIDPaxNoShow(vintIDCab, intIDPax, False, vstrUserMod)
                Next

                Dim objCabBT As New clsVentasCotizacionBT
                objCabBT.ActualizarNroPax(vintIDCab, vListaShowPax.Count, vstrUserMod)

                blnEntro = True
            End If

            If blnEntro Then
                Dim objDet As New clsDetalleVentasCotizacionBT
                objDet.ActualizarNroPaxxDetPax(vintIDCab, vstrUserMod)
                ActualizarFechaLogxIDCab(vintIDCab, vstrUserMod)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".RecalcularVentasxPaxNoShow")
        End Try
    End Sub

    Private Sub ActualizarFechaLogxIDCab(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_UpdFecLogxIDCab", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFechaLogxIDCab")
        End Try
    End Sub

    Public Function Insertar(ByVal BE As clsReservaBE) As Int32
        Dim dc As New Dictionary(Of String, String)
        Dim intIDReserva As Int32
        Try

            With dc
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@IDCab", BE.IDCab)
                .Add("@IDFile", BE.IDFile)
                .Add("@Estado", BE.Estado)
                .Add("@EstadoSolicitud", BE.EstadoSolicitud)
                .Add("@IDUsuarioVen", BE.IDUsuarioVen)
                .Add("@IDUsuarioRes", BE.IDUsuarioRes)
                .Add("@Observaciones", BE.Observaciones)
                .Add("@CodReservaProv", BE.CodReservaProv)
                .Add("@RegeneradoxAcomodo", BE.RegeneradoxAcomodo)
                .Add("@IDReservaProtecc", BE.IDReservaProtecc)
                .Add("@UserMod", BE.UserMod)
                .Add("@pIDReserva", 0)
            End With
            intIDReserva = objADO.ExecuteSPOutput("RESERVAS_Ins", dc)

            BE.IDReserva = intIDReserva

            'Dim objTareas As New clsReservaBT.clsTareasReservaBT
            'objTareas.InsertarEliminar(BE)


            'Dim objDet As New clsReservaBT.clsDetalleReservaBT
            'objDet.InsertarActualizarEliminar(BE)



            ContextUtil.SetComplete()

            Return intIDReserva
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try

    End Function

    Public Sub ActualizarFlagCambiosVtasAceptadas(ByVal vintIDReserva As Integer, _
                                                  ByVal vblnFlag As Boolean, _
                                                  ByVal vstrUserMod As String)
        Try
            'Dim dc As New Dictionary(Of String, String)
            'dc.Add("@IDReserva", vintIDReserva)
            'dc.Add("@CambiosVtasAceptados", vblnFlag)
            'dc.Add("@UserMod", vstrUserMod)
            'objADO.ExecuteSP("RESERVAS_UpdCambiosVtasAceptados", dc)

            ActualizarFlagCambiosdeVentas(vintIDReserva, vblnFlag, _
                                                  vstrUserMod)

            If vblnFlag = False Then
                Dim dc2 As New Dictionary(Of String, String)
                dc2.Add("@IDReserva", vintIDReserva)
                objADO.ExecuteSP("COTIDET_LOG_DelxIDReserva", dc2)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFlagCambiosVtasAceptadas")
        End Try

    End Sub
    Private Sub ActualizarFlagCambiosdeVentas(ByVal vintIDReserva As Integer, _
                                                  ByVal vblnFlag As Boolean, _
                                                  ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@CambiosVtasAceptados", vblnFlag)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("RESERVAS_UpdCambiosVtasAceptados", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFlagCambiosdeVentas")
        End Try
    End Sub
    Public Sub ActualizarCambiosdeVentas(ByVal vintIDReserva As Integer, _
                                ByVal vstrIDProveedor As String, _
                                ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Try

            Dim BECot As New clsCotizacionBE
            Dim objCotBT As New clsVentasCotizacionBT
            Dim dttCB As DataTable = objCotBT.dttConsultarPk(vintIDCab)

            With BECot
                .IdCab = vintIDCab
                .IDFile = dttCB(0)("IDFile")
                .IDUsuario = dttCB(0)("IDUsuario")
                .IDUsuarioRes = dttCB(0)("IDUsuarioRes")
                .Simple = If(IsDBNull(dttCB(0)("Simple")), 0, dttCB(0)("Simple"))
                .SimpleResidente = If(IsDBNull(dttCB(0)("SimpleResidente")), 0, dttCB(0)("SimpleResidente"))
                .Twin = If(IsDBNull(dttCB(0)("Twin")), 0, dttCB(0)("Twin"))
                .TwinResidente = If(IsDBNull(dttCB(0)("TwinResidente")), 0, dttCB(0)("TwinResidente"))
                .Matrimonial = If(IsDBNull(dttCB(0)("Matrimonial")), 0, dttCB(0)("Matrimonial"))
                .MatrimonialResidente = If(IsDBNull(dttCB(0)("MatrimonialResidente")), 0, dttCB(0)("MatrimonialResidente"))
                .Triple = If(IsDBNull(dttCB(0)("Triple")), 0, dttCB(0)("Triple"))
                .TripleResidente = If(IsDBNull(dttCB(0)("TripleResidente")), 0, dttCB(0)("TripleResidente"))
                .Estado = dttCB(0)("Estado")
                .UserMod = vstrUserMod
            End With

            Dim objResBN As New clsReservaBN
            Dim dttLog As DataTable = objResBN.ConsultarLogVentas(vintIDCab, vstrIDProveedor)


            If blnProveedorenLogEsCompleto(vintIDCab, vstrIDProveedor) Then


                Dim ListaProveed As List(Of stProveedoresReservas) = ListaProvReservas
                Dim ItemProvee As New stProveedoresReservas With {.IDReserva = vintIDReserva, .IDProveedor = vstrIDProveedor}
                ListaProveed = New List(Of stProveedoresReservas)
                ListaProveed.Add(ItemProvee)

                Dim objBN As New clsReservaBN
                Dim objBECot As clsCotizacionBE = objBN.objBECotizacion(vintIDCab, vstrIDProveedor, vstrUserMod)


                GrabarxCotizacion2(objBECot, False, ListaProveed, True) ', datFechaOut, bytNoches)
                GrabarxCotizacion2(objBECot, True, ListaProveed, True) ', datFechaOut, bytNoches)


                CambiosPostGeneracionxFile(objBECot, ListaProveed)


                Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT
                Dim objDetOpeBT As New clsDetalleOperacionBT
                For Each drLog As DataRow In dttLog.Rows
                    If drLog("Accion") = "B" Then

                        If Not objDetOpeBT.blnExistenOperaciones_DetxIDDet(drLog("IDDet")) Then
                            objDetOpeBT.EliminarxIDDet(drLog("IDDet"))
                            objDetResBT.EliminarxIDDet(drLog("IDDet"))

                        Else

                            objDetResBT.AnularxIDDet(drLog("IDDet"), True, vstrUserMod)
                            'objDetResBT.InsertarLogxIDDet(drLog("IDDet"), "B")

                        End If

                    End If

                Next

            Else

                For Each drLog As DataRow In dttLog.Rows
                    RegenerarDetalleReserva(drLog("IDDet"), drLog("Accion"), vstrIDProveedor, _
                                            drLog("IDTipoProv"), drLog("ConAlojamiento"), drLog("PlanAlimenticio"), _
                                            drLog("IDServicio_Det"), vintIDCab, vintIDReserva, BECot, vstrUserMod)
                Next

            End If

            ''Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT
            ''Dim objDetOpeBT As New clsDetalleOperacionBT
            ''For Each drLog As DataRow In dttLog.Rows
            ''    If drLog("Accion") = "B" Then

            ''        If Not objDetOpeBT.blnExistenOperaciones_DetxIDDet(drLog("IDDet")) Then
            ''            objDetOpeBT.EliminarxIDDet(drLog("IDDet"))
            ''            objDetResBT.EliminarxIDDet(drLog("IDDet"))

            ''        Else

            ''            objDetResBT.AnularxIDDet(drLog("IDDet"), True, vstrUserMod)
            ''            objDetResBT.InsertarLogxIDDet(drLog("IDDet"), "B")

            ''        End If

            ''    End If

            ''Next
            ''EliminarDetCotiDuplicados(vintIDCab, vstrIDProveedor)




            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            objADO.ExecuteSP("COTIDET_LOG_DelxIDCabProveedor", dc)

            ActualizarFlagCambiosdeVentas(vintIDReserva, False, _
                                                  vstrUserMod)


            'RegenerarReserva(vintIDReserva, _
            '                    vstrIDProveedor, _
            '                    vintIDCab, vstrUserMod)



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub
    Private Sub EliminarDetCotiDuplicados(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@pIDReserva_Det", 0)
            Dim intIDReserva_Det As Integer = objADO.ExecuteSPOutput("RESERVAS_DET_SelIDDetDuplicadoOutput", dc)

            If intIDReserva_Det > 0 Then
                Dim objDetRes As New clsReservaBT.clsDetalleReservaBT
                objDetRes.Eliminar(intIDReserva_Det)
            End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Function blnProveedorenLogEsCompleto(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As Boolean
        Try
            Dim dcFec As New Dictionary(Of String, String)
            dcFec.Add("@IDCab", vintIDCab)
            dcFec.Add("@IDProveedor", vstrIDProveedor)

            Dim datFechaIn As Date = #1/1/1900#
            Dim datFechaFin As Date = #1/1/1900#
            Dim dttFechas As DataTable = objADO.GetDataTable("RESERVAS_DET_SelFechasxProveedor", dcFec)

            If dttFechas.Rows.Count = 1 Then
                datFechaIn = dttFechas(0)("Dia")
                If Not IsDBNull(dttFechas(0)("FechaOut")) Then
                    datFechaFin = dttFechas(0)("FechaOut")
                End If

            ElseIf dttFechas.Rows.Count > 1 Then
                Dim bytCont As Byte = 1
                For Each dr As DataRow In dttFechas.Rows
                    If bytCont = 1 Then
                        datFechaIn = dr("Dia")
                    End If
                    If bytCont = dttFechas.Rows.Count Then
                        If Not IsDBNull(dr("FechaOut")) Then
                            datFechaFin = dr("FechaOut")
                        End If
                    End If
                    bytCont += 1
                Next
            End If

            Dim blnOk As Boolean = False

            If datFechaIn <> #1/1/1900# And datFechaFin <> #1/1/1900# Then
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@FechaIn", Format(datFechaIn, "dd/MM/yyyy"))
                dc.Add("@FechaOut", datFechaFin)
                dc.Add("@pOk", 0)

                blnOk = objADO.ExecuteSPOutput("COTIDET_LOG_SelxSiEsTodoElProveedorOutput", dc)
            End If

            Return blnOk
        Catch ex As Exception
            Throw
        End Try

    End Function


    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsDetalleReservaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Dim strNombreClase As String = "clsDetalleReservaBT"
        Public Sub InsertarActualizarEliminar(ByVal BE As clsReservaBE, ByVal vintIDReservaExistente As Integer)
            Dim dc As Dictionary(Of String, String)
            Dim intIDReservas_Det As Integer
            Dim objBTDetServRes As New clsReservaBT.clsDetalleReservaBT.clsDetalleReservaDetServiciosBT
            Dim objBTDetResPax As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT
            Try
                'Dim bytCont As Byte = 0
                For Each Item As clsReservaBE.clsDetalleReservaBE In BE.ListaDetReservas
                    dc = New Dictionary(Of String, String)

                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        If vintIDReservaExistente <> 0 Then
                            dc.Add("@IDReserva", vintIDReservaExistente)
                            dc.Add("@IDFile", BE.IDFile)
                        Else
                            If Not Item.IDReserva Is Nothing Then
                                dc.Add("@IDReserva", Item.IDReserva)
                                dc.Add("@IDFile", Item.IDFile)
                            Else
                                dc.Add("@IDReserva", BE.IDReserva)
                                dc.Add("@IDFile", BE.IDFile)

                            End If

                        End If
                        dc.Add("@IDDet", Item.IDDet)
                        dc.Add("@Item", Item.Item)
                        dc.Add("@Dia", Format(Item.Dia, "dd/MM/yyyy HH:mm:ss"))
                        dc.Add("@FechaOut", Format(Item.FechaOut, "dd/MM/yyyy HH:mm:ss"))
                        dc.Add("@Noches", Item.Noches)
                        dc.Add("@IDUbigeo", Item.IDUbigeo)
                        dc.Add("@IDServicio", Item.IDServicio)
                        dc.Add("@IDServicio_Det", Item.IDServicio_Det)
                        dc.Add("@Especial", Item.Especial)
                        dc.Add("@MotivoEspecial", Item.MotivoEspecial)
                        dc.Add("@RutaDocSustento", Item.RutaDocSustento)
                        dc.Add("@Desayuno", Item.Desayuno)
                        dc.Add("@Lonche", Item.Lonche)
                        dc.Add("@Almuerzo", Item.Almuerzo)
                        dc.Add("@Cena", Item.Cena)
                        dc.Add("@Transfer", Item.Transfer)
                        If IsNumeric(Item.IDDetTransferOri) Then
                            dc.Add("@IDDetTransferOri", Item.IDDetTransferOri)
                        Else
                            dc.Add("@IDDetTransferOri", 0)
                        End If
                        If IsNumeric(Item.IDDetTransferDes) Then
                            dc.Add("@IDDetTransferDes", Item.IDDetTransferDes)
                        Else
                            dc.Add("@IDDetTransferDes", 0)
                        End If
                        dc.Add("@TipoTransporte", Item.TipoTransporte)
                        dc.Add("@IDUbigeoOri", Item.IDUbigeoOri)
                        dc.Add("@IDUbigeoDes", Item.IDUbigeoDes)
                        dc.Add("@IDIdioma", Item.IDIdioma)
                        dc.Add("@NroPax", Item.NroPax)
                        dc.Add("@NroLiberados", Item.NroLiberados)
                        dc.Add("@Tipo_Lib", Item.Tipo_Lib)
                        dc.Add("@Cantidad", Item.Cantidad)
                        dc.Add("@CantidadAPagar", Item.CantidadAPagar)
                        dc.Add("@CostoReal", Item.CostoReal)
                        dc.Add("@CostoRealAnt", Item.CostoRealAnt)
                        dc.Add("@CostoLiberado", Item.CostoLiberado)
                        dc.Add("@Margen", Item.Margen)
                        dc.Add("@MargenAplicado", Item.MargenAplicado)
                        dc.Add("@MargenAplicadoAnt", Item.MargenAplicadoAnt)
                        dc.Add("@MargenLiberado", Item.MargenLiberado)
                        dc.Add("@Total", Item.Total)
                        dc.Add("@TotalOrig", Item.TotalOrig)
                        dc.Add("@CostoLiberadoImpto", Item.CostoLiberadoImpto)
                        dc.Add("@CostoRealImpto", Item.CostoRealImpto)
                        dc.Add("@MargenImpto", Item.MargenImpto)
                        dc.Add("@MargenLiberadoImpto", Item.MargenLiberadoImpto)
                        dc.Add("@TotImpto", Item.TotImpto)
                        dc.Add("@NetoHab", Item.NetoHab)
                        dc.Add("@IgvHab", Item.IgvHab)
                        dc.Add("@TotalHab", Item.TotalHab)
                        dc.Add("@NetoGen", Item.NetoGen)
                        dc.Add("@IgvGen", Item.IgvGen)
                        dc.Add("@TotalGen", Item.TotalGen)
                        dc.Add("@IDGuiaProveedor", Item.IDGuiaProveedor)
                        dc.Add("@NuVehiculo", Item.NuVehiculo)
                        dc.Add("@IDDetRel", Item.IDDetRel)
                        dc.Add("@Servicio", Item.Servicio)
                        dc.Add("@IDReserva_DetCopia", Item.IDReserva_DetCopia)
                        dc.Add("@IDReserva_Det_Rel", Item.IDReserva_Det_Rel)
                        dc.Add("@IDEmailEdit", Item.IDEmailEdit)
                        dc.Add("@IDEmailNew", Item.IDEmailNew)
                        dc.Add("@IDEmailRef", Item.IDEmailRef)
                        dc.Add("@CapacidadHab", Item.CapacidadHab)
                        dc.Add("@EsMatrimonial", Item.EsMatrimonial)
                        dc.Add("@IDMoneda", Item.IDMoneda)
                        dc.Add("@FlServicioNoShow", Item.FlServNoShow)
                        If BE.UserMod Is Nothing Then
                            dc.Add("@UserMod", Item.UserMod)
                        Else
                            dc.Add("@UserMod", BE.UserMod)
                        End If
                        dc.Add("@CantidadAPagarEditado", Item.CantidadAPagarEditado)
                        dc.Add("@NetoHabEditado", Item.NetoHabEditado)
                        dc.Add("@IgvHabEditado", Item.IgvHabEditado)

                        If Item.Accion = "N" Then
                            dc.Add("@FlServicioParaGuia", Item.FlServicioParaGuia)
                            dc.Add("@FlServicioTC", Item.FlServicioTC)
                            dc.Add("@AcomdoResidente", Item.FlAcomodoResidente)
                            dc.Add("@NuViaticoTC", Item.NuViaticosTC)
                            dc.Add("@pIDReserva_Det", 0)
                            intIDReservas_Det = objADO.ExecuteSPOutput("RESERVAS_DET_Ins", dc)

                            If Not BE.ListaPaxDetReservas Is Nothing Then
                                For Each ItemDetPax As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In BE.ListaPaxDetReservas
                                    If ItemDetPax.IDReserva_Det = Item.IDReserva_Det Then
                                        ItemDetPax.IDReserva_Det = intIDReservas_Det
                                    End If
                                Next
                            End If

                            If Not BE.ListaDetServiciosDetReservas Is Nothing Then
                                For Each ItemDet As clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE In BE.ListaDetServiciosDetReservas
                                    If ItemDet.IDReserva_Det = Item.IDReserva_Det Then
                                        ItemDet.IDReserva_Det = intIDReservas_Det
                                    End If
                                Next
                            End If
                        ElseIf Item.Accion = "M" Then
                            dc.Add("@IDReserva_Det", Item.IDReserva_Det)
                            objADO.ExecuteSP("RESERVAS_DET_Upd", dc)

                            Dim dc2 As New Dictionary(Of String, String)
                            dc2.Add("@Dia", Item.Dia.ToString("dd/MM/yyyy HH:mm"))
                            dc2.Add("@IDDet", Item.IDDet)
                            dc2.Add("@IDServicio_Det", Item.IDServicio_Det)

                            objADO.ExecuteSP("COTIDET_UpdxIDDetxIDServicio_Det", dc2)
                        End If

                        If Not BE.ListaPaxDetReservas Is Nothing Then objBTDetResPax.InsertarEliminar(BE, If(intIDReservas_Det.ToString = "0", Item.IDReserva_Det, intIDReservas_Det.ToString), Item.Accion)
                    ElseIf Item.Accion = "B" Then
                        If IsNumeric(Item.IDReserva_Det) Then
                            Dim objOpeDetBT As New clsDetalleOperacionBT
                            Dim objOrdPagBT As New clsOrdenPagoBT
                            If objOpeDetBT.blnExistenOperaciones_DetxIDReserva_Det(Item.IDReserva_Det) Then
                                Anular(Item.IDReserva_Det, True, BE.UserMod)
                                'InsertarLogxIDReserva_Det(Item.IDReserva_Det, "B")
                            ElseIf objOrdPagBT.blnExistexIDDet(Item.IDDet) Then
                                Anular(Item.IDReserva_Det, True, BE.UserMod)
                            Else
                                Dim dc2 As New Dictionary(Of String, String)
                                dc2.Add("@IDReserva_Det", Item.IDReserva_Det)
                                objADO.ExecuteSP("RESERVAS_DETSERVICIOS_DelxIDReserva_Det", dc2)
                                objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDReserva_Det", dc2)
                                objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_DelxIDReserva_Det", dc2)
                                objOpeDetBT.EliminarxIDReserva_Det(Item.IDReserva_Det)

                                objADO.ExecuteSP("RESERVAS_DET_Del", dc2)
                            End If
                        End If
                    End If
                Next

                If Not BE.ListaDetServiciosDetReservas Is Nothing Then
                    objBTDetServRes.InsertarActualizar(BE)
                End If

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarActualizarEliminar")
            End Try
        End Sub

        Public Sub InsertarActualizarEliminarTMP(ByVal BE As clsReservaBE, ByVal vintIDReservaExistente As Integer)
            Dim dc As Dictionary(Of String, String)
            Dim intIDReservas_Det As Integer
            Dim objBTDetServRes As New clsReservaBT.clsDetalleReservaBT.clsDetalleReservaDetServiciosBT
            Dim objBTDetResPax As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT

            Try
                For Each Item As clsReservaBE.clsDetalleReservaBE In BE.ListaDetReservas
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        'If Item.NuViaticosTC > 0 Then
                        '    Dim na As Integer = 0
                        'End If
                        If vintIDReservaExistente <> 0 Then
                            dc.Add("@IDReserva", vintIDReservaExistente)
                            dc.Add("@IDFile", BE.IDFile)
                        Else
                            If Not Item.IDReserva Is Nothing Then
                                dc.Add("@IDReserva", Item.IDReserva)
                                dc.Add("@IDFile", Item.IDFile)
                            Else
                                dc.Add("@IDReserva", BE.IDReserva)
                                dc.Add("@IDFile", BE.IDFile)

                            End If

                        End If
                        dc.Add("@IDDet", Item.IDDet)
                        dc.Add("@Item", Item.Item)
                        dc.Add("@Dia", Format(Item.Dia, "dd/MM/yyyy HH:mm:ss"))
                        dc.Add("@FechaOut", Format(Item.FechaOut, "dd/MM/yyyy HH:mm:ss"))
                        dc.Add("@Noches", Item.Noches)
                        dc.Add("@IDUbigeo", Item.IDUbigeo)
                        dc.Add("@IDServicio", Item.IDServicio)
                        dc.Add("@IDServicio_Det", Item.IDServicio_Det)
                        dc.Add("@Especial", Item.Especial)
                        dc.Add("@MotivoEspecial", Item.MotivoEspecial)
                        dc.Add("@RutaDocSustento", Item.RutaDocSustento)
                        dc.Add("@Desayuno", Item.Desayuno)
                        dc.Add("@Lonche", Item.Lonche)
                        dc.Add("@Almuerzo", Item.Almuerzo)
                        dc.Add("@Cena", Item.Cena)
                        dc.Add("@Transfer", Item.Transfer)
                        If IsNumeric(Item.IDDetTransferOri) Then
                            dc.Add("@IDDetTransferOri", Item.IDDetTransferOri)
                        Else
                            dc.Add("@IDDetTransferOri", 0)
                        End If
                        If IsNumeric(Item.IDDetTransferDes) Then
                            dc.Add("@IDDetTransferDes", Item.IDDetTransferDes)
                        Else
                            dc.Add("@IDDetTransferDes", 0)
                        End If
                        dc.Add("@TipoTransporte", Item.TipoTransporte)
                        dc.Add("@IDUbigeoOri", Item.IDUbigeoOri)
                        dc.Add("@IDUbigeoDes", Item.IDUbigeoDes)
                        dc.Add("@IDIdioma", Item.IDIdioma)
                        dc.Add("@NroPax", Item.NroPax)
                        dc.Add("@NroLiberados", Item.NroLiberados)
                        dc.Add("@Tipo_Lib", Item.Tipo_Lib)
                        dc.Add("@Cantidad", Item.Cantidad)
                        dc.Add("@CantidadAPagar", Item.CantidadAPagar)
                        dc.Add("@CostoReal", Item.CostoReal)
                        dc.Add("@CostoRealAnt", Item.CostoRealAnt)
                        dc.Add("@CostoLiberado", Item.CostoLiberado)
                        dc.Add("@Margen", Item.Margen)
                        dc.Add("@MargenAplicado", Item.MargenAplicado)
                        dc.Add("@MargenAplicadoAnt", Item.MargenAplicadoAnt)
                        dc.Add("@MargenLiberado", Item.MargenLiberado)
                        dc.Add("@Total", Item.Total)
                        dc.Add("@TotalOrig", Item.TotalOrig)
                        dc.Add("@CostoLiberadoImpto", Item.CostoLiberadoImpto)
                        dc.Add("@CostoRealImpto", Item.CostoRealImpto)
                        dc.Add("@MargenImpto", Item.MargenImpto)
                        dc.Add("@MargenLiberadoImpto", Item.MargenLiberadoImpto)
                        dc.Add("@TotImpto", Item.TotImpto)
                        dc.Add("@NetoHab", Item.NetoHab)
                        dc.Add("@IgvHab", Item.IgvHab)
                        dc.Add("@TotalHab", Item.TotalHab)
                        dc.Add("@NetoGen", Item.NetoGen)
                        dc.Add("@IgvGen", Item.IgvGen)
                        dc.Add("@TotalGen", Item.TotalGen)
                        dc.Add("@IDGuiaProveedor", Item.IDGuiaProveedor)
                        'dc.Add("@DescBus", Item.DescBus)
                        dc.Add("@NuVehiculo", Item.NuVehiculo)
                        dc.Add("@IDDetRel", Item.IDDetRel)
                        dc.Add("@Servicio", Item.Servicio)
                        dc.Add("@IDReserva_DetCopia", Item.IDReserva_DetCopia)
                        dc.Add("@IDReserva_Det_Rel", Item.IDReserva_Det_Rel)
                        dc.Add("@IDEmailEdit", Item.IDEmailEdit)
                        dc.Add("@IDEmailNew", Item.IDEmailNew)
                        dc.Add("@IDEmailRef", Item.IDEmailRef)
                        dc.Add("@CapacidadHab", Item.CapacidadHab)
                        dc.Add("@EsMatrimonial", Item.EsMatrimonial)
                        dc.Add("@IDMoneda", Item.IDMoneda)

                        If BE.UserMod Is Nothing Then
                            dc.Add("@UserMod", Item.UserMod)
                        Else
                            dc.Add("@UserMod", BE.UserMod)
                        End If


                        If Item.Accion = "N" Then

                            'Item.IDReserva_Det = 107330
                            dc.Add("@IDReserva_DetReal", Item.IDReserva_Det)
                            dc.Add("@FlServicioTC", Item.FlServicioTC)
                            dc.Add("@AcomdoResidente", Item.FlAcomodoResidente)
                            dc.Add("@NuViaticoTC", Item.NuViaticosTC)
                            dc.Add("@IDreserva_DetAntiguo", Item.IDReserva_DetAntiguo)
                            dc.Add("@pIDReserva_Det", 0)
                            intIDReservas_Det = objADO.ExecuteSPOutput("RESERVAS_DET_TMP_Ins", dc)

                            'If Not BE.ListaPaxDetReservas Is Nothing Then
                            '    For Each ItemDetPax As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In BE.ListaPaxDetReservas
                            '        If ItemDetPax.IDDet = Item.IDDet Then
                            '            If ItemDetPax.IDReserva_Det = Item.IDReserva_Det Then
                            '                ItemDetPax.IDReserva_Det = intIDReservas_Det
                            '            End If
                            '        End If
                            '    Next
                            'End If

                            'If Not BE.ListaDetServiciosDetReservas Is Nothing Then objBTDetServRes.InsertarActualizarTMP(BE, If(intIDReservas_Det.ToString = "0", Item.IDReserva_Det, intIDReservas_Det.ToString))

                            'If Not BE.ListaPaxDetReservas Is Nothing Then _
                            '    objBTDetResPax.InsertarEliminarTMP(BE, _
                            '        intIDReservas_Det, _
                            '        Item.IDReserva_Det, _
                            '        Item.Accion)

                        ElseIf Item.Accion = "M" Then
                            dc.Add("@IDReserva_Det", Item.IDReserva_Det)
                            objADO.ExecuteSP("RESERVAS_DET_TMP_Upd", dc)

                        End If
                    End If

                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarActualizarEliminarTMP")
            End Try
        End Sub

        Public Function InsertarxNuevoAcomodoTMP(ByVal vintIDReserva As Integer, ByVal vintIDServicio_Det As Integer, _
                                  ByVal vbytCapacidadHab As Byte, ByVal vblnEsMatrimonial As Boolean, _
                                  ByVal vdatDia As Date, ByVal vstrUserMod As String, ByVal vintIDReserva_Det As Integer) As String
            Try
                Dim dc As New Dictionary(Of String, String)
                'dc.Add("@IDReserva", vintIDReserva)

                'dc.Add("@IDServicio_Det", vintIDServicio_Det)
                'dc.Add("@CapacidadHab", vbytCapacidadHab)
                'dc.Add("@EsMatrimonial", vblnEsMatrimonial)

                'dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))

                dc.Add("@IDReserva_DetTMP", vintIDReserva_Det)
                dc.Add("@UserMod", vstrUserMod)
                dc.Add("@pIDReserva_Det", 0)

                Dim strIDReserva_Det As String = objADO.ExecuteSPOutput("RESERVAS_DET_InsxAcomodoTMP", dc)
                'objADO.ExecuteSP("RESERVAS_DET_InsxAcomodoTMP", dc)
                ContextUtil.SetComplete()

                Return strIDReserva_Det
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxNuevoAcomodoTMP")
            End Try
        End Function


        Public Sub ActualizarxNuevoAcomodoTMP(ByVal vintIDReserva As Integer, ByVal vintIDReserva_Det As Integer, ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@IDReserva_Det", vintIDReserva_Det)

                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_UpdxAcomodoTMP", dc)

                ContextUtil.SetComplete()


            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxNuevoAcomodoTMP")
            End Try
        End Sub

        Public Sub ActualizarNoches(ByVal vintIDDet As Integer, _
                                    ByVal vdatFechaOut As Date, _
                                    ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("IDDet", vintIDDet)
                dc.Add("FechaOut", Format(vdatFechaOut, "dd/MM/yyyy"))
                dc.Add("UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_UpdNoches", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNoches")
            End Try
        End Sub
        Public Sub ActualizarNochesTMP(ByVal vintIDDet As Integer, _
                                    ByVal vdatFechaOut As Date, _
                                    ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("IDDet", vintIDDet)
                dc.Add("FechaOut", Format(vdatFechaOut, "dd/MM/yyyy"))
                dc.Add("UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_TMP_UpdNoches", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNochesTMP")
            End Try
        End Sub
        Public Sub ActualizarNochesxIDReserva_Det(ByVal vintIDReserva_Det As Integer, _
                                    ByVal vdatFechaOut As Date, _
                                    ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("IDReserva_Det", vintIDReserva_Det)
                dc.Add("FechaOut", Format(vdatFechaOut, "dd/MM/yyyy"))
                dc.Add("UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_UpdNochesxIDReserva_Det", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNochesxIDReserva_Det")
            End Try
        End Sub

        Public Sub ActualizarCampoDia(ByVal vintIDReservas_Det As Integer, ByVal vdatDia As DateTime, ByVal vstrUser As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva_Det", vintIDReservas_Det)
                dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))
                dc.Add("@UserMod", vstrUser)

                objADO.ExecuteSP("RESERVAS_DET_UpdDiaxIDReserva_Det", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCampoDia")
            End Try
        End Sub

        Public Sub ActualizarNroLiberados(ByVal vintIDCab As Integer, _
                            ByVal vbytCapacidadHab As Byte, _
                            ByVal vintNroLiberados As Int16, _
                            ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCab", vintIDCab)
                dc.Add("@CapacidadHab", vbytCapacidadHab)
                dc.Add("@NroLiberados", vintNroLiberados)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_UpdNroLiberadosxCapacidadHab", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroLiberados")
            End Try
        End Sub


        Public Sub ActualizarNroLiberadosxReserva(ByVal vintIDReserva As Integer, _
                    ByVal vbytCapacidadHab As Byte, _
                    ByVal vintNroLiberados As Int16, _
                    ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@CapacidadHab", vbytCapacidadHab)
                dc.Add("@NroLiberados", vintNroLiberados)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_UpdNroLiberadosxCapacidadHabxReserva", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroLiberadosxReserva")
            End Try
        End Sub


        Public Sub ActualizarNroLiberadosxReservaTMP(ByVal vintIDReserva As Integer, _
                   ByVal vbytCapacidadHab As Byte, _
                   ByVal vintNroLiberados As Int16, _
                   ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@CapacidadHab", vbytCapacidadHab)
                dc.Add("@NroLiberados", vintNroLiberados)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_TMP_UpdNroLiberadosxCapacidadHabxReserva", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroLiberadosxReservaTMP")
            End Try
        End Sub
        Public Sub ActualizarTipoLiberados(ByVal vintIDCab As Integer, _
                    ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCab", vintIDCab)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_UpdTipoLib", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarTipoLiberados")
            End Try
        End Sub

        Public Sub ActualizarTipoLiberadosTMP(ByVal vintIDCab As Integer, _
                    ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCab", vintIDCab)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_TMP_UpdTipoLib", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarTipoLiberadosTMP")
            End Try
        End Sub

        Private Sub ActualizarSTRel(ByRef BE As clsReservaBE, ByVal vstrIDDetRelPadre As String, ByVal vintNuevoID As Integer)
            Try
                For Each Item As clsReservaBE.clsDetalleReservaBE In BE.ListaDetReservas
                    If Item.IDDetRel IsNot Nothing Then
                        If Item.IDDetRel <> "0" And Item.IDDetRel.Length >= 2 Then
                            If Item.IDDetRel.Substring(0, 1) = "H" Then
                                If Mid(Item.IDDetRel, 2) = Mid(vstrIDDetRelPadre, 2) Then
                                    Item.IDDetRel = vintNuevoID
                                End If
                            End If
                        End If
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub
        Private Sub ActualizarSTRelNum(ByRef BE As clsReservaBE, ByVal vstrIDDetPadre As String, ByVal vintNuevoID As Integer)
            Try
                For Each Item As clsReservaBE.clsDetalleReservaBE In BE.ListaDetReservas
                    If Item.IDDetRel = vstrIDDetPadre Then
                        Item.IDDetRel = vintNuevoID
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

        'Public Sub ActualizaEstadoLogCotizacion(ByVal vintIDCab As Integer, ByVal vblnAtendido As Boolean, _
        '                                    ByVal vstrUserModAtendidoLog As String)
        '    Dim dc As New Dictionary(Of String, String)

        '    Try
        '        dc.Add("@IDCab", vintIDCab)
        '        dc.Add("@Atendido", vblnAtendido)
        '        dc.Add("@UserModAtendidoLog", vstrUserModAtendidoLog)

        '        objADO.ExecuteSP("COTIDET_LOG_Upd_Atendido", dc)

        '        ContextUtil.SetComplete()
        '    Catch ex As Exception
        '        ContextUtil.SetAbort()
        '        Throw
        '    End Try
        'End Sub

        Public Sub ActualizaEstadoLogCotizacionxProveedor(ByVal vintIDCab As Integer, ByVal vblnAtendido As Boolean, _
                                                     ByVal vstrIDProveedor As String, _
                                                     ByVal vstrUserModAtendidoLog As String)

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Atendido", vblnAtendido)
                dc.Add("@UserModAtendidoLog", vstrUserModAtendidoLog)

                objADO.ExecuteSP("COTIDET_LOG_Upd_AtendidoxProveedor", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizaEstadoLogCotizacionxProveedor")
            End Try
        End Sub

        'Public Sub InsertarLog1eraVezxIDCab(ByVal vintIDCab As Integer)
        '    Try
        '        Dim dc As New Dictionary(Of String, String)
        '        dc.Add("@IDCab", vintIDCab)

        '        objADO.ExecuteSP("RESERVAS_DET_LOG_SP_Ins", dc)
        '        ContextUtil.SetComplete()
        '    Catch ex As Exception
        '        ContextUtil.SetAbort()
        '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarLog1eraVezxIDCab")
        '    End Try
        'End Sub
        Public Sub Eliminar(ByVal vintIDReserva_Det As Integer)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDReserva_Det", vintIDReserva_Det)

                objADO.ExecuteSP("RESERVAS_DETSERVICIOS_DelxIDReserva_Det", dc)
                objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDReserva_Det", dc)
                objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_DelxIDReserva_Det", dc)

                Dim objOpeDetBT As New clsDetalleOperacionBT
                objOpeDetBT.EliminarxIDReserva_Det(vintIDReserva_Det)

                objADO.ExecuteSP("RESERVAS_DET_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
            End Try

        End Sub
        Public Sub EliminarxIDDet(ByVal vintIDDet As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)

                objADO.ExecuteSP("RESERVAS_DETSERVICIOS_DelxIDDet", dc)
                objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDDet", dc)
                objADO.ExecuteSP("NOTA_VENTA_DET_DelxIDDet", dc)
                'objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_DelxIDDet", dc)

                Dim objResEstHabBT As New clsReservaBT.clsDetalleReservaBT.clsReservasDetEstadoHabitBT
                objResEstHabBT.EliminarxIDDet(vintIDDet)

                objADO.ExecuteSP("RESERVAS_DET_DelxIDDet", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
            End Try

        End Sub

        Public Sub EliminarxIDDetTMP(ByVal vintIDDet As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)

                'objADO.ExecuteSP("RESERVAS_DET_PAX_TMP_DelxIDDet", dc)
                objADO.ExecuteSP("RESERVAS_DET_TMP_DelxIDDet", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
            End Try

        End Sub


        Public Sub AnularxIDDet(ByVal vintIDDet As Integer, ByVal vblnAnulado As Boolean, _
                                ByVal vstrUserMod As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@UserMod", vstrUserMod)
                dc.Add("@Anulado", vblnAnulado)

                'objADO.ExecuteSP("RESERVAS_DETSERVICIOS_UpdAnuladoxIDDet", dc)
                'objADO.ExecuteSP("RESERVAS_DET_PAX_UpdAnuladoxIDDet", dc)
                'objADO.ExecuteSP("NOTA_VENTA_DET_UpdAnuladoxIDDet", dc)
                'objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_UpdAnuladoxIDDet", dc)

                objADO.ExecuteSP("RESERVAS_DET_UpdAnuladoxIDDet", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AnularxIDDet")
            End Try

        End Sub

        Public Sub Anular(ByVal vintIDReserva_Det As Integer, ByVal vblnAnulado As Boolean, _
                          ByVal vstrUserMod As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva_Det", vintIDReserva_Det)
                dc.Add("@UserMod", vstrUserMod)
                dc.Add("@Anulado", vblnAnulado)


                objADO.ExecuteSP("RESERVAS_DET_UpdAnulado", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Anular")
            End Try

        End Sub

        'Public Sub InsertarLogxIDDet(ByVal vintIDDet As Integer, ByVal vchrAccion As Char)
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDDet", vintIDDet)
        '        dc.Add("@Accion", vchrAccion)
        '        objADO.ExecuteSP("RESERVAS_DET_LOG_SP_InsxIDDet", dc)

        '        ContextUtil.SetComplete()
        '    Catch ex As Exception
        '        ContextUtil.SetAbort()
        '        'Throw
        '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarLogxIDDet")
        '    End Try
        'End Sub

        'Public Sub InsertarLogxIDReserva_Det(ByVal vintIDReserva_Det As Integer, ByVal vchrAccion As Char)
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDReserva_Det", vintIDReserva_Det)
        '        dc.Add("@Accion", vchrAccion)
        '        objADO.ExecuteSP("RESERVAS_DET_LOG_SP_InsxIDReserva_Det", dc)

        '        ContextUtil.SetComplete()
        '    Catch ex As Exception
        '        ContextUtil.SetAbort()
        '        'Throw
        '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarLogxIDReserva_Det")
        '    End Try
        'End Sub
        'Public Function dttConsultarPk(ByVal vintIDReserva_Det As Integer) As DataTable
        '    Dim dc As New Dictionary(Of String, String)
        '    dc.Add("@IDReservas", vintIDReserva_Det)

        '    Return objADO.GetDataTable("RESERVAS_DET_Sel_Pk", dc)

        'End Function
        Public Function ConsultarxIDReserva(ByVal vintIDReserva As Integer, ByVal vstrIDEmail As String, _
                                            ByVal vblnGastoTransferencias As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@IDEmail", vstrIDEmail)
                dc.Add("@GastTransfer", vblnGastoTransferencias)

                Return objADO.GetDataTable("RESERVAS_DET_SelxIDReserva", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function ConsultarPaxxIDReserva_Det(ByVal vintIDReserva_Det As Integer) As DataTable
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDReserva_Det", vintIDReserva_Det)

        '        Return objADO.GetDataTable("RESERVAS_DET_SelPaxxIDReserva_Det", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function

        Public Function ConsultarxIDDet(ByVal vintIDDet As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)

                Return objADO.GetDataTable("RESERVAS_DET_SelxIDDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Sub ActualizarFechasOutNochesxIDDet(ByVal vintIDDet As Integer, _
        '                                           ByVal vdatFechaOut As Date, _
        '                                           ByVal vbytNoches As Byte, _
        '                                           ByVal vstrUserMod As String)
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDDet", vintIDDet)
        '        dc.Add("@FechaOut", vdatFechaOut)
        '        dc.Add("@Noches", vbytNoches)
        '        dc.Add("@UserMod", vstrUserMod)

        '        objADO.ExecuteSP("RESERVAS_DET_Upd_NochesFechaOut", dc)

        '        ContextUtil.SetComplete()
        '    Catch ex As Exception
        '        ContextUtil.SetAbort()
        '        Throw
        '    End Try

        'End Sub

        <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsDetalleReservaDetServiciosBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT
            Dim strNombreClase As String = "clsDetalleReservaDetServiciosBT"
            Private Function blnExiste(ByVal vintIDReserva_Det As Int32, ByVal vintIDServicio_Det As Int32) As Boolean
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDReserva_Det", vintIDReserva_Det)
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@pExiste", False)

                    Return objADO.ExecuteSPOutput("RESERVAS_DETSERVICIOS_Sel_ExisteOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function

            Public Sub InsertarActualizar(ByVal BE As clsReservaBE)
                Dim blnExist As Boolean = False
                Try
                    Dim dc As Dictionary(Of String, String)
                    For Each Item As clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE In BE.ListaDetServiciosDetReservas
                        If IsNumeric(Item.IDReserva_Det) Then
                            blnExist = blnExiste(Item.IDReserva_Det, Item.IDServicio_Det)

                            dc = New Dictionary(Of String, String)
                            dc.Add("@IDReserva_Det", Item.IDReserva_Det)
                            dc.Add("@IDServicio_Det", Item.IDServicio_Det)
                            dc.Add("@CostoReal", Item.CostoReal)
                            dc.Add("@CostoLiberado", Item.CostoLiberado)
                            dc.Add("@Margen", Item.Margen)
                            dc.Add("@SSCR", Item.SSCR)
                            dc.Add("@SSCL", Item.SSCL)
                            dc.Add("@SSMargen", Item.SSMargen)
                            dc.Add("@SSMargenLiberado", Item.SSMargenLiberado)
                            dc.Add("@SSTotal", Item.SSTotal)
                            dc.Add("@STCR", Item.STCR)
                            dc.Add("@STMargen", Item.STMargen)
                            dc.Add("@STTotal", Item.STTotal)
                            dc.Add("@SSCRImpto", Item.SSCRImpto)
                            dc.Add("@SSCLImpto", Item.SSCLImpto)
                            dc.Add("@SSMargenImpto", Item.SSMargenImpto)
                            dc.Add("@SSMargenLiberadoImpto", Item.SSMargenLiberadoImpto)
                            dc.Add("@STCRImpto", Item.STCRImpto)
                            dc.Add("@STMargenImpto", Item.STMargenImpto)
                            dc.Add("@MargenAplicado ", Item.MargenAplicado)
                            dc.Add("@MargenLiberado ", Item.MargenLiberado)
                            dc.Add("@Total ", Item.Total)
                            dc.Add("@CostoRealImpto", Item.CostoRealImpto)
                            dc.Add("@CostoLiberadoImpto", Item.CostoLiberadoImpto)
                            dc.Add("@MargenImpto", Item.MargenImpto)
                            dc.Add("@MargenLiberadoImpto", Item.MargenLiberadoImpto)
                            dc.Add("@TotImpto", Item.TotImpto)
                            dc.Add("@UserMod", Item.UserMod)
                            If Not blnExist Then
                                objADO.ExecuteSP("RESERVAS_DETSERVICIOS_Ins", dc)
                            Else
                                objADO.ExecuteSP("RESERVAS_DETSERVICIOS_Upd", dc)
                            End If
                        End If
                    Next
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarActualizar")
                End Try
            End Sub

            'Public Sub InsertarActualizarTMP(ByVal BE As clsReservaBE, ByVal vintIDReserva_Det As Int32)

            '    Try
            '        Dim dc As Dictionary(Of String, String)
            '        For Each Item As clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE In BE.ListaDetServiciosDetReservas



            '            dc = New Dictionary(Of String, String)

            '            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            '            dc.Add("@IDServicio_Det", Item.IDServicio_Det)

            '            dc.Add("@CostoLiberado", Item.CostoLiberado)
            '            dc.Add("@CostoLiberadoImpto", Item.CostoLiberadoImpto)
            '            dc.Add("@CostoReal", Item.CostoReal)
            '            dc.Add("@CostoRealImpto", Item.CostoRealImpto)
            '            dc.Add("@Margen", Item.Margen)
            '            dc.Add("@MargenAplicado", Item.MargenAplicado)
            '            dc.Add("@MargenImpto", Item.MargenImpto)
            '            dc.Add("@MargenLiberado", Item.MargenLiberado)
            '            dc.Add("@MargenLiberadoImpto", Item.MargenLiberadoImpto)
            '            dc.Add("@Total", Item.Total)
            '            dc.Add("@TotImpto", Item.TotImpto)
            '            dc.Add("@UserMod", Item.UserMod)

            '            objADO.ExecuteSP("RESERVAS_DETSERVICIOS_TMP_Ins", dc)


            '        Next
            '        ContextUtil.SetComplete()
            '    Catch ex As Exception
            '        ContextUtil.SetAbort()
            '        'Throw
            '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarActualizarTMP")
            '    End Try
            'End Sub

            Public Sub GrabarxCotizacion(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@UserMod", vstrUserMod)
                    objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxIDCab", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxCotizacion")
                End Try

            End Sub

            'Public Sub GrabarxReserva(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, ByVal vintIDServicio_Det As Integer, ByVal vstrUserMod As String)
            Public Sub GrabarxReserva(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@IDReserva", vintIDReserva)
                    'dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@UserMod", vstrUserMod)
                    objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxIDReserva", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxReserva")
                End Try
            End Sub

            Public Sub GrabarxServicio(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@IDReserva", vintIDReserva)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxIDReservaxServicio", dc)
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxSubServiciosVentas")
                End Try
            End Sub

            'Public Sub GrabarxReservaTMP(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
            '    Dim dc As New Dictionary(Of String, String)
            '    Try
            '        dc.Add("@IDCab", vintIDCab)
            '        dc.Add("@IDReserva", vintIDReserva)
            '        dc.Add("@UserMod", vstrUserMod)
            '        objADO.ExecuteSP("RESERVAS_DETSERVICIOS_TMP_InsxIDReserva", dc)

            '        ContextUtil.SetComplete()
            '    Catch ex As Exception
            '        ContextUtil.SetAbort()
            '        'Throw
            '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxReservaTMP")
            '    End Try

            'End Sub

            Public Sub GrabarxIDReservaxIDCabViatico(ByVal vintIDCab As Integer, _
                                              ByVal vintIDReserva As Integer, _
                                              ByVal vstrUserMode As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@IDReserva", vintIDReserva)
                    dc.Add("@UserMod", vstrUserMode)

                    objADO.ExecuteSP("RESERVAS_DETSERVICIOS_ViaticosInsxIDReserva", dc)
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxIDReservaxIDCabViatico")
                End Try
            End Sub

            Public Sub GrabarxIDDet(ByVal vintIDCab As Integer, _
                                    ByVal vintIDDet As Integer, _
                                    ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@IDDet", vintIDDet)
                    dc.Add("@IDReserva", vintIDReserva)
                    dc.Add("@UserMod", vstrUserMod)
                    objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxIDDet", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxIDDet")
                End Try

            End Sub

            Public Sub InsertarxNuevoAcomodoTMP(ByVal vintIDReserva As Integer, _
                                  ByVal vstrUserMod As String)
                Try
                    Dim dc As New Dictionary(Of String, String)
                    'dc.Add("@IDReserva_DetTmp", vintIDReserva_DetTmp)
                    'dc.Add("@IDReserva_DetNue", vintIDReserva_DetNue)
                    dc.Add("@IDReserva", vintIDReserva)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("RESERVAS_DETSERVICIOS_InsxAcomodoTMP", dc)
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxNuevoAcomodoTMP")
                End Try
            End Sub
        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsReservaDetPaxBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT
            Dim strNombreClase As String = "clsReservaDetPaxBT"
            Private Function blnExiste(ByVal vintIDReserva_Det As Int32, ByVal vintIDPax As Int32) As Boolean
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDReserva_Det", vintIDReserva_Det)
                    dc.Add("@IDPax", vintIDPax)
                    dc.Add("@pExiste", False)

                    Return objADO.ExecuteSPOutput("RESERVAS_DET_PAX_Sel_ExisteOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
            'Private Function blnExisteTMP(ByVal vintIDReserva_Det As Int32, ByVal vintIDPax As Int32) As Boolean
            '    Try
            '        Dim dc As New Dictionary(Of String, String)

            '        dc.Add("@IDReserva_Det", vintIDReserva_Det)
            '        dc.Add("@IDPax", vintIDPax)
            '        dc.Add("@pExiste", False)

            '        Return objADO.ExecuteSPOutput("RESERVAS_DET_PAX_TMP_Sel_ExisteOutput", dc)
            '    Catch ex As Exception
            '        Throw
            '    End Try
            'End Function
            Public Function blnExistePax(ByVal vintIDPax As Int32) As Boolean
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDPax", vintIDPax)
                    dc.Add("@pExiste", False)

                    Return objADO.ExecuteSPOutput("RESERVAS_DET_PAX_Sel_ExistePaxOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function

            Private Sub Insertar(ByVal BE As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE, ByVal vintIDReserva_Det As Integer)
                Dim blnExist As Boolean = False
                Try
                    Dim dc As New Dictionary(Of String, String)
                    With dc
                        dc.Add("@IDReserva_Det", vintIDReserva_Det)
                        dc.Add("@IDPax", BE.IDPax)
                        dc.Add("@UserMod", BE.UserMod)
                    End With

                    If BE.Selecc Then
                        blnExist = blnExiste(vintIDReserva_Det, BE.IDPax)

                        If Not blnExist Then
                            objADO.ExecuteSP("RESERVAS_DET_PAX_Ins", dc)
                        End If
                    End If

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
                End Try
            End Sub

            Private Sub Eliminar(ByVal BE As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
                Try
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add("@IDReserva_Det", BE.IDReserva_Det)
                    dc.Add("@IDPax", BE.IDPax)

                    objADO.ExecuteSP("RESERVAS_DET_PAX_Del", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
                End Try
            End Sub


            Public Sub InsertarEliminar(ByVal BE As clsReservaBE, ByVal vintIDReserva_Det As Int32, ByVal vchrAccion As Char)
                Dim blnExist As Boolean
                Try
                    If BE.ListaPaxDetReservas Is Nothing Then Exit Sub
                    Dim strIDReserva_Det As String
                    Dim dc As Dictionary(Of String, String)
                    For Each Item As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In BE.ListaPaxDetReservas
                        If IsNumeric(Item.IDReserva_Det) Then
                            strIDReserva_Det = Item.IDReserva_Det
                        Else
                            strIDReserva_Det = vintIDReserva_Det
                        End If
                        'If vintIDReserva_Det.ToString = Item.IDReserva_Det Then 'Or vchrAccion = "N" Then
                        If strIDReserva_Det = Item.IDReserva_Det Then 'Or vchrAccion = "N" Then
                            'If Item.Selecc Then
                            '    Insertar(Item, vintIDReserva_Det)
                            'Else
                            '    Eliminar(Item)
                            'End If
                            dc = New Dictionary(Of String, String)

                            'dc.Add("@IDReserva_Det", vintIDReserva_Det)
                            dc.Add("@IDReserva_Det", strIDReserva_Det)
                            dc.Add("@IDPax", Item.IDPax)

                            If Item.Selecc Then
                                blnExist = blnExiste(strIDReserva_Det, Item.IDPax)
                                If Not blnExist Then
                                    dc.Add("@UserMod", Item.UserMod)
                                    objADO.ExecuteSP("RESERVAS_DET_PAX_Ins", dc)
                                End If
                            Else
                                objADO.ExecuteSP("RESERVAS_DET_PAX_Del", dc)
                            End If
                        End If
                    Next
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarEliminar")
                End Try
            End Sub
            Public Sub ActualizarNroPax(ByVal vintIDReserva_Det As Integer, ByVal vstrUserMod As String)
                Try
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add("@IDReserva_Det", vintIDReserva_Det)
                    dc.Add("@UserMod", vstrUserMod)


                    objADO.ExecuteSP("RESERVAS_DET_Upd_NroPax2", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroPax")
                End Try
            End Sub
            'Public Sub InsertarEliminarTMP(ByVal BE As clsReservaBE, ByVal vintIDReserva_Det As Int32, _
            '                               ByVal vintIDReserva_DetReal As Integer, _
            '                               ByVal vchrAccion As Char)
            '    Dim blnExist As Boolean
            '    Try
            '        If BE.ListaPaxDetReservas Is Nothing Then Exit Sub

            '        Dim dc As Dictionary(Of String, String)
            '        For Each Item As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In BE.ListaPaxDetReservas

            '            dc = New Dictionary(Of String, String)

            '            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            '            dc.Add("@IDPax", Item.IDPax)
            '            dc.Add("@IDReserva_DetReal", vintIDReserva_DetReal)

            '            blnExist = blnExisteTMP(vintIDReserva_Det, Item.IDPax)
            '            If Not blnExist Then
            '                dc.Add("@UserMod", Item.UserMod)
            '                objADO.ExecuteSP("RESERVAS_DET_PAX_TMP_Ins", dc)
            '            End If
            '            'End If
            '        Next
            '        ContextUtil.SetComplete()
            '    Catch ex As Exception
            '        ContextUtil.SetAbort()
            '        'Throw
            '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarEliminarTMP")
            '    End Try
            'End Sub
            'Public Sub InsertarEliminarTMP(ByVal BE As clsReservaBE)
            '    'Dim blnExist As Boolean
            '    Try
            '        If BE.ListaPaxDetReservas Is Nothing Then Exit Sub

            '        Dim dc As Dictionary(Of String, String)
            '        For Each Item As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In BE.ListaPaxDetReservas

            '            dc = New Dictionary(Of String, String)

            '            dc.Add("@IDReserva_Det", Item.IDReserva_Det)
            '            dc.Add("@IDPax", Item.IDPax)
            '            dc.Add("@IDReserva_DetReal", Item.IDReserva_DetAcomodoRealTMP)

            '            'blnExist = blnExisteTMP(vintIDReserva_Det, Item.IDPax)
            '            'If Not blnExist Then
            '            dc.Add("@UserMod", Item.UserMod)
            '            objADO.ExecuteSP("RESERVAS_DET_PAX_TMP_Ins", dc)
            '            'End If

            '        Next
            '        ContextUtil.SetComplete()
            '    Catch ex As Exception
            '        ContextUtil.SetAbort()
            '        'Throw
            '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarEliminarTMP")
            '    End Try
            'End Sub

            'Public Sub InsertarxNuevoAcomodoTMP(ByVal vintIDReserva_DetTmp As Integer, _
            '                                    ByVal vintIDReserva_DetNue As Integer, _
            '                      ByVal vstrUserMod As String)
            '    Try
            '        Dim dc As New Dictionary(Of String, String)
            '        dc.Add("@IDReserva_DetTmp", vintIDReserva_DetTmp)
            '        dc.Add("@IDReserva_DetNue", vintIDReserva_DetNue)
            '        dc.Add("@UserMod", vstrUserMod)

            '        objADO.ExecuteSP("RESERVAS_DET_PAX_InsxAcomodoTMP", dc)

            '        ContextUtil.SetComplete()
            '    Catch ex As Exception
            '        ContextUtil.SetAbort()
            '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxNuevoAcomodoTMP")
            '    End Try
            'End Sub

            'Public Sub InsertarxNuevoAcomodoTMPxIDReserva(ByVal vintIDReserva As Integer, _
            '           ByVal vstrUserMod As String)
            '    Try
            '        Dim dc As New Dictionary(Of String, String)
            '        dc.Add("@IDReserva", vintIDReserva)
            '        dc.Add("@UserMod", vstrUserMod)

            '        objADO.ExecuteSP("RESERVAS_DET_PAX_InsxIDReservaAcomodoTMP", dc)

            '        ContextUtil.SetComplete()
            '    Catch ex As Exception
            '        ContextUtil.SetAbort()
            '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxNuevoAcomodoTMP")
            '    End Try
            'End Sub

            Public Sub GrabarxCotizacion(ByVal vintIDCab As Integer, ByVal vintIDDet As Integer)
                Try
                    Dim objCotiPaxBT As New clsCotizacionBT.clsCotizacionPaxBT
                    Dim objBEDetPaxRes As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE
                    Dim ListaDetPaxRes As New List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
                    Dim objBERes As clsReservaBE
                    Dim objBTDetPax As clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT

                    Dim bytContPax As Byte
                    Dim bln1ero As Boolean
                    Dim ListaPax As New List(Of String)

                    Dim dcPaxxIDCab As New Dictionary(Of String, String)
                    dcPaxxIDCab.Add("@IDCab", vintIDCab)
                    dcPaxxIDCab.Add("@IDDet", vintIDDet)
                    Dim dttPaxxIDCab As DataTable = objADO.GetDataTable("RESERVAS_DET_SelNroPaxxIDCab", dcPaxxIDCab)

                    For Each drPaxxIDCab As DataRow In dttPaxxIDCab.Rows

BorrarPaxGrabados:
                        For intInd = 0 To ListaPax.Count - 1
                            ListaPax.RemoveAt(intInd)
                            GoTo BorrarPaxGrabados
                        Next

                        objBERes = New clsReservaBE

                        Dim dttPax As DataTable = objCotiPaxBT.ConsultarDetxIDDetLvw(drPaxxIDCab("IDDet"))
                        bytContPax = 1
                        bln1ero = True
                        For Each drPax As DataRow In dttPax.Rows
                            If bln1ero Then
                                If bytContPax <= (drPaxxIDCab("NroPax") + drPaxxIDCab("NroLiberados")) Then

                                    objBEDetPaxRes = New clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE
                                    With objBEDetPaxRes
                                        .IDPax = drPax("IDPax")
                                        .UserMod = drPaxxIDCab("UserMod")
                                        .Selecc = True
                                        .Accion = "N"
                                    End With
                                    ListaDetPaxRes.Add(objBEDetPaxRes)
                                    objBERes.ListaPaxDetReservas = ListaDetPaxRes

                                    ListaPax.Add(drPax("IDPax"))
                                    bytContPax += 1
                                End If

                            Else
                                If Not ListaPax.Contains(drPax("IDPax")) Then
                                    If bytContPax <= drPaxxIDCab("NroPax") Then

                                        objBEDetPaxRes = New clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE
                                        With objBEDetPaxRes
                                            .IDPax = drPax("IDPax")
                                            .UserMod = drPaxxIDCab("UserMod")
                                            .Selecc = True
                                            .Accion = "N"
                                        End With
                                        ListaDetPaxRes.Add(objBEDetPaxRes)
                                        objBERes.ListaPaxDetReservas = ListaDetPaxRes

                                        ListaPax.Add(drPax("IDPax"))
                                        bytContPax += 1
                                    End If
                                End If

                            End If



                        Next
                        bln1ero = False


                        If Not objBERes.ListaPaxDetReservas Is Nothing Then
                            For Each ItemDetPax As clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE In objBERes.ListaPaxDetReservas

                                ItemDetPax.IDReserva_Det = drPaxxIDCab("IDReserva_Det")

                            Next
                        End If

                        objBTDetPax = New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT
                        objBTDetPax.InsertarEliminar(objBERes, drPaxxIDCab("IDReserva_Det"), "N")


BorrarPax:
                        For intInd = 0 To ListaDetPaxRes.Count - 1
                            ListaDetPaxRes.RemoveAt(intInd)
                            GoTo BorrarPax
                        Next

                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub InsertarxAcomodos(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("RESERVAS_DET_PAX_InsxAcomodos", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxAcomodos")
                End Try
            End Sub

            Public Sub InsertarxAcomodosPax(ByVal vintIDCab As Integer, _
                                            ByVal vintIDPax As Integer, _
                                            ByVal vstrUserMod As String)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@IDPax", vintIDPax)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("RESERVAS_DET_PAX_InsxAcomodosPax", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxAcomodosPax")
                End Try
            End Sub

            Public Sub EliminarxPax(ByVal vintIDPax As Integer)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDPax", vintIDPax)
                    objADO.ExecuteSP("RESERVAS_DET_PAX_DelxPax", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxPax")
                End Try
            End Sub

        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsReservasDetEstadoHabitBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT
            Dim strNombreClase As String = "clsReservasDetEstadoHabitBT"

            Private Sub Insertar(ByVal BE As clsReservaBE.clsDetalleReservaBE.clsReservasDetEstadoHabitBE)

                Dim dc As New Dictionary(Of String, String)
                Try

                    'dc.Add("@IDReserva_Det", BE.IDReserva_Det)
                    dc.Add("@IDDet", BE.IDDet)
                    dc.Add("@NuIngreso", BE.NuIngreso)
                    dc.Add("@Simple", BE.Simple)
                    dc.Add("@Twin", BE.Twin)
                    dc.Add("@Matrimonial", BE.Matrimonial)
                    dc.Add("@Triple", BE.Triple)
                    dc.Add("@UserMod", BE.UserMod)

                    objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_Ins", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
                End Try
            End Sub

            Private Sub Actualizar(ByVal BE As clsReservaBE.clsDetalleReservaBE.clsReservasDetEstadoHabitBE)

                Dim dc As New Dictionary(Of String, String)
                Try

                    'dc.Add("@IDReserva_Det", BE.IDReserva_Det)
                    dc.Add("@IDDet", BE.IDDet)
                    dc.Add("@NuIngreso", BE.NuIngreso)
                    dc.Add("@Simple", BE.Simple)
                    dc.Add("@Twin", BE.Twin)
                    dc.Add("@Matrimonial", BE.Matrimonial)
                    dc.Add("@Triple", BE.Triple)
                    dc.Add("@UserMod", BE.UserMod)

                    objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_Upd", dc)


                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
                End Try
            End Sub

            Private Function blnExiste(ByVal vintIDDet As Integer, ByVal vbytNuIngreso As Byte) As Boolean
                Dim dc As New Dictionary(Of String, String)
                Try
                    'dc.Add("@IDReserva_Det", vintIDReserva_Det)
                    dc.Add("@IDDet", vintIDDet)
                    dc.Add("@NuIngreso", vbytNuIngreso)
                    dc.Add("@pExiste", 0)

                    Return objADO.ExecuteSPOutput("RESERVAS_DET_ESTADOHABITAC_Sel_ExisteOutput", dc)

                Catch ex As Exception
                    Throw
                End Try
            End Function

            Public Sub InsertarActualizar(ByVal BE As clsReservaBE)
                If BE.ListaDetReservasEstadoHabit Is Nothing Then Exit Sub
                Try
                    For Each Item As clsReservaBE.clsDetalleReservaBE.clsReservasDetEstadoHabitBE In BE.ListaDetReservasEstadoHabit
                        If blnExiste(Item.IDDet, Item.NuIngreso) Then
                            Actualizar(Item)
                        Else
                            Insertar(Item)
                        End If
                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub


            Public Sub EliminarxIDDet(ByVal vintIDDet As Integer)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDDet", vintIDDet)

                    objADO.ExecuteSP("RESERVAS_DET_ESTADOHABITAC_DelxIDDet", dc)


                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    'Throw
                    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
                End Try

            End Sub


        End Class

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTareasReservaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Dim strNombreClase As String = "clsTareasReservaBT"
        Public Sub InsertarEliminar(ByVal BE As clsReservaBE)
            Dim dc As Dictionary(Of String, String)

            Try
                For Each Item As clsReservaBE.clsTareasReservaBE In BE.ListaTareasReservas
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then

                        If Not BE.IDReserva Is Nothing Then
                            dc.Add("@IDReserva", BE.IDReserva)
                        Else
                            dc.Add("@IDReserva", Item.IDReserva)
                        End If

                        dc.Add("@IDTarea", Item.IDTarea)
                        dc.Add("@IDEstado", Item.IDEstado)
                        dc.Add("@FecDeadLine", Item.FecDeadLine)
                        dc.Add("@IDCab", Item.IDCab)
                        dc.Add("@Descripcion", Item.Descripcion)
                        'dc.Add("@UltimoMinuto", Item.UltimoMinuto)
                        If Not BE.UserMod Is Nothing Then
                            dc.Add("@UserMod", BE.UserMod)
                        Else
                            dc.Add("@UserMod", Item.UserMod)
                        End If

                        If Item.Accion = "N" Then
                            dc.Add("@UltimoMinuto", Item.UltimoMinuto)
                            objADO.ExecuteSP("RESERVAS_TAREAS_Ins", dc)

                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("RESERVAS_TAREAS_Upd", dc)
                        End If
                    ElseIf Item.Accion = "B" Then
                        If Not BE.IDReserva Is Nothing Then
                            dc.Add("@IDReserva", BE.IDReserva)
                        Else
                            dc.Add("@IDReserva", Item.IDReserva)
                        End If
                        dc.Add("@IDTarea", Item.IDTarea)
                        dc.Add("@IDCab", Item.IDCab)

                        objADO.ExecuteSP("RESERVAS_TAREAS_Del", dc)
                    End If

                Next

                If Not BE.ListaUltimoMinuto Is Nothing Then
                    ActualizarUltimoMinutos(BE)
                End If
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub ActualizarUltimoMinutos(ByVal BE As clsReservaBE)
            Dim dc As Dictionary(Of String, String)
            Try
                For Each strIDTarea As String In BE.ListaUltimoMinuto
                    dc = New Dictionary(Of String, String)
                    dc.Add("@IDReserva", BE.IDReserva)
                    dc.Add("@IDTarea", strIDTarea)
                    dc.Add("@IDCab", BE.IDCab)
                    dc.Add("@UserMod", BE.UserMod)

                    objADO.ExecuteSP("RESERVAS_TAREAS_Upd_UltimoMinuto", dc)
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarUltimoMinutos")
            End Try
        End Sub

        Public Sub ActualizarEstado(ByVal vintIDReserva As Integer, ByVal vintIDTarea As Int16, _
                                    ByVal vintIDCab As Integer, ByVal vstrIDEstado As String, _
                                    ByVal vstrUserMod As String)

            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@IDTarea", vintIDTarea)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDEstado", vstrIDEstado)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_TAREAS_Upd_IDEstado", dc)


                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstado")
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsAcomodoPaxProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Dim strNombreClase As String = "clsAcomodoPaxProveedorBT"
        Private Sub Insertar(ByVal BE As clsReservaBE.clsAcomodoPaxProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", BE.IDReserva)
                dc.Add("@IDPax", BE.IDPax)
                dc.Add("@IDHabit", BE.IdHabit)
                dc.Add("@Capacidad", BE.CapacidadHabit)
                dc.Add("@EsMatrimonial", BE.EsMatrimonial)
                dc.Add("@UserMod", BE.UserMod)


                Dim blnExiste As Boolean = blnExisteAcomodPxPrv(BE.IDReserva, BE.IDPax)

                If Not blnExiste Then
                    objADO.ExecuteSP("ACOMODOPAX_PROVEEDOR_Ins", dc)
                End If

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
            End Try
        End Sub

        Private Sub Eliminar(ByVal BE As clsReservaBE.clsAcomodoPaxProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", BE.IDReserva)
                dc.Add("@IDPax", BE.IDPax)

                objADO.ExecuteSP("ACOMODOPAX_PROVEEDOR_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
            End Try
        End Sub

        Public Sub InsertarEliminar(ByVal BE As clsReservaBE)
            Try
                Dim objDetPaxDet As New clsDetalleReservaBT.clsReservaDetPaxBT
                For Each Item As clsReservaBE.clsAcomodoPaxProveedorBE In BE.ListaAcomodPaxReservasProveedor
                    If Item.Accion = "B" Then
                        If IsNumeric(Item.IDReserva) Then

                            Eliminar(Item)
                        End If
                    End If
                Next
                For Each Item As clsReservaBE.clsAcomodoPaxProveedorBE In BE.ListaAcomodPaxReservasProveedor
                    If Item.Accion = "N" Then
                        If IsNumeric(Item.IDReserva) Then
                            Eliminar(Item)
                            Insertar(Item)
                        End If

                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Function blnExisteAcomodPxPrv(ByVal vintIDReserva As Integer, ByVal vintIDPax As Integer) As Boolean
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@IDPax", vintIDPax)
                dc.Add("@pblnExiste", False)

                Dim blnExiste As Boolean = objADO.GetSPOutputValue("ACOMODOPAX_PROVEEDOR_SelOutput", dc)
                Return blnExiste

            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsAcomodoPaxBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Dim strNombreClase As String = "clsAcomodoPaxBT"

        Private Sub Insertar(ByVal BE As clsReservaBE.clsAcomodoPaxBE)
            Dim Dc As New Dictionary(Of String, String)
            Try
                Dc.Add("@IDPax", BE.IDPax)
                Dc.Add("@IDHabit", BE.IdHabit)
                Dc.Add("@UserMod", BE.UserMod)
                Dc.Add("@IDCAB", BE.IDCAB)
                Dc.Add("@CapacidadHab", BE.CapacidadHabit)
                Dc.Add("@EsMatrimonial", BE.EsMatrimonial)

                Dim blnExiste As Boolean = blnExisteAcomodoReserva(BE.IDPax, BE.CapacidadHabit)

                If Not blnExiste Then
                    objADO.ExecuteSP("ACOMODOPAX_FILE_Ins", Dc)
                End If
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
            End Try
        End Sub

        Private Sub Eliminar(ByVal BE As clsReservaBE.clsAcomodoPaxBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDPax", BE.IDPax)
                dc.Add("@CapacidadHab", BE.CapacidadHabit)
                dc.Add("@IDCab", BE.IDCAB)

                objADO.ExecuteSP("ACOMODOPAX_FILE_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
            End Try
        End Sub

        Public Sub InsertarEliminar(ByVal BE As clsReservaBE)
            Try
                For Each Item As clsReservaBE.clsAcomodoPaxBE In BE.ListaAcomodoPaxReservas
                    If Item.Accion = "B" Then
                        Eliminar(Item)
                    End If
                Next

                For Each Item As clsReservaBE.clsAcomodoPaxBE In BE.ListaAcomodoPaxReservas
                    If Item.Accion = "N" Then
                        Insertar(Item)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function blnExisteAcomodoReserva(ByVal vintIDPax As Integer, ByVal vshtIDCapacidad As Short)
            Try
                Dim dcVal As New Dictionary(Of String, String)
                With dcVal
                    .Add("@IDPax", vintIDPax)
                    .Add("@CapacidadHab", vshtIDCapacidad)
                    .Add("@pblnExiste", False)
                End With

                Dim blnExiste As Boolean = objADO.GetSPOutputValue("ACOMODOPAX_FILE_SelOutput", dcVal)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsMensajesReservasTelefBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Dim strNombreClase As String = "clsMensajesReservasTelefBT"

        Public Sub Insertar(ByVal BE As clsReservaBE.clsMensajesReservasTelefBE)

            Dim dc As New Dictionary(Of String, String)
            Try

                dc.Add("@IDReserva", BE.IDReserva)
                dc.Add("@Contacto", BE.Contacto)
                dc.Add("@IDUsuario", BE.IDUsuario)
                dc.Add("@Fecha", FormatDateTime(BE.Fecha, DateFormat.ShortDate))
                dc.Add("@Asunto", BE.Asunto)
                dc.Add("@Mensaje", BE.Mensaje)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("RESERVAS_MENSAJESTELEF_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsReservaBE.clsMensajesReservasTelefBE)

            Dim dc As New Dictionary(Of String, String)
            Try

                dc.Add("@IDReserva", BE.IDReserva)
                dc.Add("@IDMensaje", BE.IDMensaje)
                dc.Add("@Contacto", BE.Contacto)
                dc.Add("@IDUsuario", BE.IDUsuario)
                dc.Add("@Fecha", BE.Fecha)
                dc.Add("@Asunto", BE.Asunto)
                dc.Add("@Mensaje", BE.Mensaje)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("RESERVAS_MENSAJESTELEF_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
            End Try
        End Sub

        Public Sub Eliminar(ByVal vintIDReserva As Int32, ByVal vbytIDMensaje As Byte)

            Dim dc As New Dictionary(Of String, String)
            Try

                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@IDMensaje", vbytIDMensaje)

                objADO.ExecuteSP("RESERVAS_MENSAJESTELEF_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                'Throw
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
            End Try
        End Sub
    End Class

End Class