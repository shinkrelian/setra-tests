﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public MustInherit Class clsOperacionBaseBT
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDT

    Public Overridable Sub Insertar()

    End Sub

    Public Overridable Sub Actualizar()

    End Sub

    Public Overridable Sub Eliminar()

    End Sub

    Public Overridable Sub InsertarActualizarEliminar()

    End Sub

    Public Overridable Sub ActualizarIDsNuevosenHijosST()

    End Sub

    Public Overridable Sub EliminarIDsHijosST()

    End Sub

    Protected gstrIDProveedorSetoursLima As String = "001554"
    Protected gstrIDProveedorSetoursCusco As String = "000544"
    Protected gstrIDServicioTrasladistaCusco As String = "CUZ00481"
    Protected gstrTipoProveeTransportista As String = "004"
    Protected gstrIDProvTranslivik As String = "000467"
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOperacionBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsOperacionBT"
    Private Overloads Function Insertar(ByVal BE As clsOperacionBE) As Integer
        Dim intIDOperacion As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDReserva", BE.IDReserva)
                dc.Add("@IDCab", BE.IDCab)
                dc.Add("@IDFile", BE.IDFile)
                dc.Add("@IDProveedor", BE.IDProveedor)
                dc.Add("@Observaciones", BE.Observaciones)
                dc.Add("@UserMod", BE.UserMod)
                dc.Add("@pIDOperacion", 0)

                intIDOperacion = objADO.ExecuteSPOutput("OPERACIONES_Ins", dc)


            End With

            ContextUtil.SetComplete()

            Return intIDOperacion
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Private Overloads Sub Actualizar(ByVal BE As clsOperacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDOperacion", BE.IDOperacion)
                dc.Add("@Observaciones", BE.Observaciones)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("OPERACIONES_Upd", dc)

            End With

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Public Overloads Sub Eliminar(ByVal vintIdOperacion As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDoperacion", vintIdOperacion)
            End With

            Dim dtNumero As DataTable = Consultar_NuPreSobxIDOperacion(vintIdOperacion)
            Dim vintNuPreSob As Integer = 0
            If dtNumero.Rows.Count > 0 Then
                vintNuPreSob = dtNumero.Rows(0).Item(0)
            End If

            Dim dc2 As New Dictionary(Of String, String)
            With dc2
                .Add("@IDoperacion", vintIdOperacion)
                .Add("@NuPreSob", vintNuPreSob)
            End With

            objADO.ExecuteSP("OPERACIONES_DET_PAX_DelxIDOperacion", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDOperacion", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_DelxIDOperacion", dc)
            objADO.ExecuteSP("VOUCHER_HISTORICO_DelxIDOperacion", dc)
            objADO.ExecuteSP("SINCERADO_DET_PAX_DelxIDOperacion", dc)
            objADO.ExecuteSP("SINCERADO_DET_DelxIDOperacion", dc)
            objADO.ExecuteSP("SINCERADO_DelxIDOperacion", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_DelxIDOperacion", dc)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_DelxIDOperacion", dc)

            'objADO.ExecuteSP("PRESUPUESTO_SOBRE_DelxIDOperacion", dc)
            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DelxIDOperacion_NuPreSob", dc2)

            'Dim dttVouchers As DataTable = objADO.GetDataTable("OPERACIONES_DET_SelVouchers2", dc)
            'Dim objVou As New clsVoucherOperacionBT
            'Dim ListaVouchers As New List(Of Integer)
            'For Each drVou As DataRow In dttVouchers.Rows
            '    objVou.EliminarDocumentos_Proveedor_Det(drVou("IDVoucher"))
            '    objVou.EliminarDocumentos_Proveedor(drVou("IDVoucher"))
            '    ListaVouchers.Add(drVou("IDVoucher"))
            'Next

            objADO.ExecuteSP("OPERACIONES_DET_DelxIDOperacion", dc)
            ''objADO.ExecuteSP("VOUCHER_OPERACIONES_Del_xIDOperacion", dc)
            'For Each intVoucher As Integer In ListaVouchers
            '    Dim dcVou As New Dictionary(Of String, String)
            '    dcVou.Add("@IDVoucher", intVoucher)
            '    objADO.ExecuteSP("VOUCHER_OPERACIONES_Del", dcVou)
            'Next



            objADO.ExecuteSP("OPERACIONES_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    'jorge
    Public Function Consultar_NuPreSobxIDOperacion(ByVal vintIdOperacion As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDOperacion", vintIdOperacion)

        Return objADO.GetDataTable("PRESUPUESTO_SOBRE_Sel_ID_xIDOperacion", dc)
    End Function

    Public Function Consultar_List_ServiciosxProvInt_ID(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                          ByVal vblnServicioTC As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDProveedor", vstrIDProveedor)
        dc.Add("@IDCab", vintIDCab)
        dc.Add("@FlServicioTC", vblnServicioTC)

        Return objADO.GetDataTable("OPERACIONES_DET_List_ServiciosxProvInt_ID", dc)
    End Function

    Public Sub EliminarxFile(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDCab", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_DelxIDCab", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DelxIDCab", dc)
            'objADO.ExecuteSP("VOUCHER_OPERACIONES_DelxIDCab", dc)
            'objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_SINCERADO_DET_DelxIDCab", dc)
            'objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_SINCERADO_DelxIDCab", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_DelxIDCab", dc)
            objADO.ExecuteSP("OPERACIONES_DET_PAX_DelxIDCab", dc)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_DelxIDCab", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DelxIDCab", dc)
            objADO.ExecuteSP("VOUCHER_OPERACIONES_DelxIDCab", dc)
            objADO.ExecuteSP("OPERACIONES_DelxIDCab", dc)

            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("COTICAB_UpdSinOperacionesxIDCab", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxFile")
        End Try

    End Sub

    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsOperacionBE, ByVal vstrAntiguoID As String, _
                                                              ByVal vintNuevoID As Integer)
        Try
            For Each ItemDet As clsDetalleOperacionBE In BE.ListaDetalleOperaciones
                If ItemDet.IDOperacion = vstrAntiguoID Then
                    ItemDet.IDOperacion = vintNuevoID
                End If
            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarIDsHijosST(ByVal BE As clsOperacionBE, ByVal vstrIDOperacion As String)
        Try
            If BE.ListaVouchers Is Nothing Then GoTo sgte
BorrarVou:
            Dim intIndSTVou As Int16 = 0
            For Each ItemDet As clsVoucherOperacionBE In BE.ListaVouchers
                If ItemDet.IDOperacion = vstrIDOperacion And ItemDet.Accion = "B" Then
                    BE.ListaVouchers.RemoveAt(intIndSTVou)
                    GoTo BorrarVou
                End If
                intIndSTVou += 1
            Next
sgte:


            If BE.ListaDetalleOperaciones Is Nothing Then GoTo sgte2
BorrarDet:
            Dim intIndSTDet As Int16 = 0
            For Each ItemDet As clsDetalleOperacionBE In BE.ListaDetalleOperaciones
                If ItemDet.IDOperacion = vstrIDOperacion And ItemDet.Accion = "B" Then
                    BE.ListaDetalleOperaciones.RemoveAt(intIndSTDet)
                    GoTo BorrarDet
                End If
                intIndSTDet += 1
            Next
sgte2:

            If BE.ListaPaxDetalle Is Nothing Then Exit Sub
BorrarPax:
            Dim intIndSTPax As Int16 = 0
            For Each ItemDet As clsPaxDetalleOperacionBE In BE.ListaPaxDetalle
                If ItemDet.IDOperacion = vstrIDOperacion Then
                    BE.ListaPaxDetalle.RemoveAt(intIndSTPax)
                    GoTo BorrarPax
                End If
                intIndSTPax += 1
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOperacionBE, Optional ByVal BEcot As clsCotizacionBE = Nothing)
        Dim dc As Dictionary(Of String, String)

        Try
            For Each Item As clsOperacionBE In BE.ListaOperaciones
                dc = New Dictionary(Of String, String)
                If Item.Accion = "N" Then
                    Dim strIDOperacionAntiguo As String = Item.IDOperacion
                    Dim intIDOperacionNuevo As Integer = Insertar(Item)
                    ActualizarIDsNuevosenHijosST(BE, strIDOperacionAntiguo, intIDOperacionNuevo)

                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    EliminarIDsHijosST(BE, Item.IDOperacion)
                    If IsNumeric(Item.IDOperacion) Then
                        Eliminar(Item.IDOperacion)
                    End If
                End If
            Next

            Dim objVou As New clsVoucherOperacionBT
            objVou.InsertarActualizarEliminar(BE)

            Dim objVouHst As New clsVoucherHistoricoBT
            objVouHst.Insertar(BE)

            Dim objDet As New clsDetalleOperacionBT
            objDet.InsertarActualizarEliminar(BE)

            Dim objVouBT As New clsVoucherOperacionBT
            objVouBT.ActualizarSaldo(BE.ListaVouchers)

            'Dim objVouBT As New clsVoucherOperacionBT
            objVouBT.Insertar_VoucherDetalle_Bloque(BE.ListaVouchers)

            If Not BE.ListaPax Is Nothing Then
                Dim objBECot As New clsCotizacionBE
                objBECot.ListaPax = BE.ListaPax

                Dim objBLPax As New clsPaxVentasCotizacionBT
                objBLPax.InsertarActualizarEliminar(objBECot)
            End If

            Dim objDetPax As New clsPaxDetalleOperacionBT
            objDetPax.InsertarActualizarEliminar(BE)

            Dim objDetServOpe As New clsDetalleServiciosOperacionBT
            objDetServOpe.InsertarActualizarEliminar(BE)

            ActualizarIDUsuario(BE.IDCab, BE.IDUsuarioOpe, BE.UserMod, BE.IDUsuarioOpeCusco)

            'ARCHIVOS
            If Not BEcot Is Nothing Then
                Dim objArchivos As New clsCotizacionBT.clsCotizacionArchivosBT
                objArchivos.InsertarActualizarEliminar(BEcot)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub GrabarxFile(ByVal vintIDCab As Integer, ByVal vstrUserMod As String, _
                           ByVal vstrRutaImagenes As String)
        'Try
        '    Dim dc As New Dictionary(Of String, String)
        '    dc.Add("@IDCab", vintIDCab)
        '    dc.Add("@UserMod", vstrUserMod)

        '    objADO.ExecuteSP("OPERACIONES_InsxIDCab", dc)
        '    objADO.ExecuteSP("OPERACIONES_DET_InsxIDCab", dc)
        '    objADO.ExecuteSP("OPERACIONES_DET_PAX_InsxIDCab", dc)
        '    objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_InsxIDCab", dc)
        '    objADO.ExecuteSP("COTICAB_Upd_FechaOperaciones", dc)

        '    Dim objDetServ As New clsDetalleServiciosOperacionBT
        '    objDetServ.ActualizarEstadoxFileyOperador(vintIDCab, gstrIDProveedorSetoursLima, _
        '                                              "PA", gstrIDProvTranslivik, gstrTipoProveeTransportista, vstrUserMod)

        '    'Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT
        '    'objDetResBT.InsertarLog1eraVezxIDCab(vintIDCab)

        '    Dim objCorreoBT As New clsCorreoVentasBT
        '    objCorreoBT.gstrRutaImagenes = vstrRutaImagenes
        '    objCorreoBT.NotificacionEnvioFileModulo1aModulo2(vintIDCab, "V", "O")

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    'Throw
        '    Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxFile")
        'End Try
    End Sub

    Public Sub GrabarxFileyProveedor(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_InsxIDProveedor", dc)
            objADO.ExecuteSP("OPERACIONES_DET_InsxIDProveedor", dc)
            objADO.ExecuteSP("OPERACIONES_DET_PAX_InsxIDProveedor", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_InsxIDProveedor", dc)

            If vstrIDProveedor = gstrIDProveedorSetoursLima Then
                Dim objDetServ As New clsDetalleServiciosOperacionBT
                objDetServ.ActualizarEstadoxFileyOperador(vintIDCab, gstrIDProveedorSetoursLima, _
                                          "PA", gstrIDProvTranslivik, gstrTipoProveeTransportista, vstrUserMod)

            ElseIf vstrIDProveedor = gstrIDProveedorSetoursCusco Then
                'GrabarSubServicioTransladista(vintIDCab, vstrIDProveedor, gstrIDServicioTrasladistaCusco, vstrUserMod)
            End If

            'objADO.ExecuteSP("RESERVAS_DET_LOG_SP_InsxIDProveedor", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarxFileyProveedor")
        End Try
    End Sub
    Private Sub GrabarSubServicioTransladista(ByVal vintIDCab As Integer, _
                                              ByVal vstrIDProveedor As String, _
                                              ByVal vstrIDServicioTrasladistaCusco As String, _
                                              ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDServicioTrasladista", vstrIDServicioTrasladistaCusco)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_InsServTrasladistaxIDProveedor", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarSubServicioTransladista")

        End Try
    End Sub
    Public Sub GenerarDetallexDetalleReserva(ByVal vintIDReserva_Det As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_InsxIDReserva_Det", dc)
            objADO.ExecuteSP("OPERACIONES_DET_PAX_InsxIDReserva_Det", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_InsxIDReserva_Det", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GenerarDetallexDetalleReserva")
        End Try
    End Sub

    Public Sub ActualizarDetallexDetalleReserva(ByVal vintIDReserva_Det As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            dc.Add("@UserMod", vstrUserMod)


            objADO.ExecuteSP("OPERACIONES_DET_UpdxIDReserva_Det", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarDetallexDetalleReserva")
        End Try
    End Sub

    'Public Sub ActualizarCambiosdeReservas(ByVal vstrIDProveedor As String, _
    '                                            ByVal vintIDCab As Integer, _
    '                        ByVal vchrAccion As Char, _
    '                        ByVal vstrUserMod As String)
    '    Try

    '        'Dim BECot As New clsCotizacionBE
    '        'Dim objCotBT As New clsVentasCotizacionBT
    '        'Dim dttCB As DataTable = objCotBT.dttConsultarPk(vintIDCab)

    '        'With BECot
    '        '    .IdCab = vintIDCab
    '        '    .IDFile = dttCB(0)("IDFile")
    '        '    .IDUsuario = dttCB(0)("IDUsuario")
    '        '    .IDUsuarioRes = dttCB(0)("IDUsuarioRes")
    '        '    .Simple = If(IsDBNull(dttCB(0)("Simple")), 0, dttCB(0)("Simple"))
    '        '    .SimpleResidente = If(IsDBNull(dttCB(0)("SimpleResidente")), 0, dttCB(0)("SimpleResidente"))
    '        '    .Twin = If(IsDBNull(dttCB(0)("Twin")), 0, dttCB(0)("Twin"))
    '        '    .TwinResidente = If(IsDBNull(dttCB(0)("TwinResidente")), 0, dttCB(0)("TwinResidente"))
    '        '    .Matrimonial = If(IsDBNull(dttCB(0)("Matrimonial")), 0, dttCB(0)("Matrimonial"))
    '        '    .MatrimonialResidente = If(IsDBNull(dttCB(0)("MatrimonialResidente")), 0, dttCB(0)("MatrimonialResidente"))
    '        '    .Triple = If(IsDBNull(dttCB(0)("Triple")), 0, dttCB(0)("Triple"))
    '        '    .TripleResidente = If(IsDBNull(dttCB(0)("TripleResidente")), 0, dttCB(0)("TripleResidente"))

    '        '    .UserMod = vstrUserMod
    '        'End With

    '        Dim objOpeBN As New clsOperacionBN
    '        Dim dttLog As DataTable = objOpeBN.ConsultarLogReservas(vintIDCab, vstrIDProveedor, vchrAccion)

    '        For Each drLog As DataRow In dttLog.Rows
    '            RegenerarDetalleOperaciones(drLog("IDReserva_Det"), drLog("IDDet"), drLog("Accion"), vstrIDProveedor, vstrUserMod)
    '        Next


    '        'Dim dc As New Dictionary(Of String, String)
    '        'dc.Add("@IDCab", vintIDCab)
    '        'dc.Add("@IDProveedor", vstrIDProveedor)
    '        'objADO.ExecuteSP("RESERVAS_DET_LOG_DelxIDCabProveedor", dc)
    '        'EliminarLogReservas(vintIDCab, vstrIDProveedor, vchrAccion)


    '        'ActualizarFlagCambiosdeVentas(vintIDReserva, False, _
    '        '                                      vstrUserMod)

    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDProveedor", vstrIDProveedor)
    '        dc.Add("@pIDReserva", 0)
    '        Dim intIDReserva As Integer = objADO.ExecuteSPOutput("RESERVAS_SelIDReservaOutput", dc)

    '        If intIDReserva <> 0 Then EliminarPostRegeneracion(intIDReserva)

    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try

    'End Sub

    'Public Sub EliminarLogReservas(ByVal vintIDCab As Integer, _
    '                                ByVal vstrIDProveedor As String, _
    '                                ByVal vchrAccion As Char)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDProveedor", vstrIDProveedor)
    '        dc.Add("@Accion", vchrAccion)

    '        objADO.ExecuteSP("RESERVAS_DET_LOG_DelxIDCabProveedor", dc)
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarLogReservas")
    '    End Try
    'End Sub
    Public Sub RegenerarDetalleOperaciones(ByVal vintIDReserva_Det As Integer, _
                                           ByVal vintIDDet As Integer, _
                                            ByVal vchrAccion As Char, _
                                            ByVal vstrIDProveedor As String, _
                                            ByVal vstrUserMod As String)
        Try

            If vchrAccion <> "B" Then
                Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
                Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT

                'Dim objDetOpeBT As New clsDetalleOperacionBT
                'objDetOpeBT.EliminarxIDReserva_Det(vintIDReserva_Det)
                If vchrAccion = "N" Then
                    GenerarDetallexDetalleReserva(vintIDReserva_Det, vstrUserMod)

                    If vstrIDProveedor = gstrIDProveedorSetoursLima Then
                        Dim objDetServ As New clsDetalleServiciosOperacionBT
                        objDetServ.ActualizarEstadoxIDReserva_Det(vintIDReserva_Det, gstrIDProvTranslivik, gstrTipoProveeTransportista, vstrUserMod)

                    End If

                ElseIf vchrAccion = "M" Then
                    ActualizarDetallexDetalleReserva(vintIDReserva_Det, vstrUserMod)
                End If


            Else

                Dim objDetOpeBT As New clsDetalleOperacionBT
                objDetOpeBT.EliminarxIDReserva_Det(vintIDReserva_Det)

                'Dim objDetResBT As New clsReservaBT.clsDetalleReservaBT
                'objDetResBT.EliminarxIDDet(vintIDDet)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub EliminarPostRegeneracion(ByVal vintIDReserva As Int32)
        Try

            Dim dc As New Dictionary(Of String, String)

            'dc.Add("@IDReserva", vintIDReserva)
            'dc.Add("@pAnulado", False)

            'If objADO.ExecuteSPOutput("RESERVAS_Sel_AnuladoOutput", dc) Then
            'dc = New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDReserva", dc)
            objADO.ExecuteSP("RESERVAS_DET_DelxIDReserva", dc)
            objADO.ExecuteSP("RESERVAS_TAREAS_DelxIDReserva", dc)
            objADO.ExecuteSP("ACOMODOPAX_PROVEEDOR_DelxIDReserva", dc)
            objADO.ExecuteSP("OPERACIONES_DelxIDReserva", dc)
            objADO.ExecuteSP("RESERVAS_Del", dc)
            'End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try

    End Sub

    Public Sub ActualizarEstadoxProveedor(ByVal vintIDCab As Integer, _
                                        ByVal vstrIDProveedor As String, _
                                        ByVal vstrEstado As String, _
                                        ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@Estado", vstrEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_UpdEstadoxProveedor", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoxProveedor")
        End Try
    End Sub

    Public Sub ActualizarFechaEnvioKitPax(ByVal vintIDCab As Integer, ByVal vdatFechaKitPax As Date, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@FechaEntregaKitPax", vdatFechaKitPax)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_Upd_FechaEntregaKitPax", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFechaEnvioKitPax")
        End Try
    End Sub

    Public Sub ActualizarIDUsuario(ByVal vintIDCab As Integer, _
                                   ByVal vstrIDUsuarioOpe As String, _
                                   ByVal vstrUserMod As String, _
                                   ByVal vstrIDUsuarioOpe_Cusco As String)
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDUsuarioOpe", vstrIDUsuarioOpe)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@IDUsuarioOpe_Cusco", vstrIDUsuarioOpe_Cusco)
            objADO.ExecuteSP("COTICAB_Upd_IDUsuarioOpe", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarIDUsuario")
        End Try
    End Sub

    Public Sub ActualizarIDUsuario_Lista(ByVal vobjLista As List(Of String), _
                                       ByVal vstrIDUsuarioOpe As String, _
                                       ByVal vstrUserMod As String, _
                                       Optional ByVal vstrIDUsuarioOpe_Cusco As String = "")

        Dim dc As New Dictionary(Of String, String)
        Try

            If vobjLista Is Nothing Then Exit Sub
            Try
                For Each Item As String In vobjLista
                    ActualizarIDUsuario(Item, vstrIDUsuarioOpe, vstrUserMod, vstrIDUsuarioOpe_Cusco)
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarIDUsuario")
        End Try
    End Sub

    Public Sub RegenerarOperacion(ByVal vintIDOperacion As Integer, _
                              ByVal vstrIDProveedor As String, _
                              ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Try
            If vintIDOperacion <> 0 Then Eliminar(vintIDOperacion)

            GrabarxFileyProveedor(vintIDCab, vstrIDProveedor, vstrUserMod)

            'EliminarLogReservas(vintIDCab, vstrIDProveedor, "")

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".RegenerarOperacion")
        End Try
    End Sub

    Public Sub RegenerarOperacion_TMP(ByVal vintIDOperacion As Integer, _
                                      ByVal vstrIDProveedor As String, _
                              ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Try

            EliminarOPERACIONES_DET_TMP(vintIDOperacion)

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_TMP_InsxIDProveedor", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_TMP_InsxIDProveedor", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_TMP_Upd_ProveedoPrg", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_TMP_Upd_NuevaTarifa", dc)
            objADO.ExecuteSP("OPERACIONES_DET_TMP_Upd_NuevaTarifa", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".RegenerarOperacion_TMP")
        End Try
    End Sub

    Public Sub EliminarOPERACIONES_DET_TMP(ByVal vintIDOperacion As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion", vintIDOperacion)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_TMP_DelxIDOperacion", dc)
            objADO.ExecuteSP("OPERACIONES_DET_TMP_DelxIDOperacion", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarOPERACIONES_DET_TMP")
        End Try

    End Sub

    Private Function blnExisteAcomodoenReal(ByVal vintIDOperacion As Integer, ByVal vintIDServicio_Det As Integer, _
                                      ByVal vbytCapacidadHab As Byte, ByVal vblnEsMatrimonial As Boolean, _
                                      ByVal vdatDia As Date) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@CapacidadHab", vbytCapacidadHab)
            dc.Add("@EsMatrimonial", vblnEsMatrimonial)
            dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_SelExisteAcomodoOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExisteAcomodoenReal")
        End Try
    End Function

    Private Sub ActualizarVoucherTMP(ByVal vintIDOperacion As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion", vintIDOperacion)

            objADO.ExecuteSP("OPERACIONES_DET_TMP_UpdVoucherxIDOperacion", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
        End Try
    End Sub

    Public Sub MatchDetOperacionesRealyTMP(ByVal vintIDCab As Integer, ByVal vintIDOperacion As Integer, _
                                           ByVal vstrIDProveedor As String, ByVal vstrUserMod As String, _
                                           ByVal vListaServ As List(Of clsReservaBE.clsServiciosCambiosVentasReservasBE), _
                                           ByVal vblnActualizarVoucherTMP As Boolean)
        Try
            If vblnActualizarVoucherTMP Then
                ActualizarVoucherTMP(vintIDOperacion)
            End If


            If vListaServ.Count > 0 Then
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@IDOperacion", vintIDOperacion)
                End With
                objADO.ExecuteSP("OPERACIONES_DET_PAX_DelxIDOperacion", dc)
            End If

            Dim objDet As New clsDetalleOperacionBT
            For Each ItemServ As clsReservaBE.clsServiciosCambiosVentasReservasBE In vListaServ
                If ItemServ.Accion = "Eliminar" Then
                    objDet.EliminarxIDReserva_Det(ItemServ.IDReserva_Det)
                End If
            Next

            Dim objDetServBT As New clsDetalleServiciosOperacionBT
            For Each ItemServ As clsReservaBE.clsServiciosCambiosVentasReservasBE In vListaServ
                If ItemServ.Accion = "Actualizar" Then
                    objDet.ActualizarxNuevoAcomodoTMP(vintIDOperacion, ItemServ.IDOperacion_Det, vstrUserMod)
                    objDetServBT.ActualizarxNuevoAcomodoTMP(ItemServ.IDOperacion_Det, vstrUserMod)

                    If ItemServ.NuevaTarifa Then
                        ActualizarTarifa(ItemServ.IDOperacion_Det, vstrUserMod)
                    End If
                ElseIf ItemServ.Accion = "Nuevo" Then
                    Dim intIDOperacion_Det As Integer = objDet.InsertarxNuevoAcomodoTMP(vintIDOperacion, ItemServ.IDServicio_Det, _
                                    ItemServ.CapacidadHab, ItemServ.EsMatrimonial, ItemServ.Dia, vstrUserMod, ItemServ.IDOperacion_Det)

                    objDetServBT.InsertarxNuevoAcomodoTMP(vintIDOperacion, intIDOperacion_Det, ItemServ.IDServicio_Det, _
                                    ItemServ.CapacidadHab, ItemServ.EsMatrimonial, ItemServ.Dia, vstrUserMod, ItemServ.IDOperacion_Det)
                End If
            Next


            If vListaServ.Count > 0 Then
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@IDOperacion", vintIDOperacion)
                    .Add("@UserMod", vstrUserMod)
                End With
                objADO.ExecuteSP("OPERACIONES_DET_PAX_InsxIDOperacion", dc)

                EliminarOPERACIONES_DET_TMP(vintIDOperacion)

                'ActualizarCambiosenReservasPostFile(vintIDOperacion, False, vstrUserMod)
            End If

            'If blnSeAnulo Then
            Dim objResBT As New clsReservaBT
            objResBT.ActualizarIDsPostAnulacion(vstrIDProveedor, vintIDCab, vstrUserMod)
            'End If

            'ActualizarFechaCambiosReservasPostFile(vintIDOperacion, vstrUserMod)
            ActualizarCambiosenReservasPostFile(vintIDOperacion, False, vstrUserMod)
            ActualizarFechaCambiosReservasPostFile(vintIDOperacion, vstrUserMod)
            EliminarPresupuestoCab_DetInexistentes(vintIDOperacion)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".MatchDetOperacionesRealyTMP")
        End Try
    End Sub

    Private Sub EliminarPresupuestoCab_DetInexistentes(ByVal vintIDOperacion As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion", vintIDOperacion)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DelxExistenciaDet", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".MatchDetOperacionesRealyTMP")
        End Try
    End Sub

    Private Sub ActualizarTarifa(ByVal vintIDOperacion_Det As Integer, ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@UserMod", vstrUserMod)

            ''objADO.ExecuteSP("OPERACIONES_DET_Upd_NuevaTarifa", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_Upd_NuevaTarifa", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarTarifa")
        End Try
    End Sub

    Private Sub ActualizarFechaCambiosReservasPostFile(ByVal vintIDOperacion As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_UpFecLogdxIDoperacion", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFechaCambiosReservasPostFile")
        End Try
    End Sub

    Private Sub ActualizarCambiosenReservasPostFile(ByVal vintIDOperacion As Integer, _
                                      ByVal vblnCambiosenReservasPostFile As Boolean, _
                                      ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@CambiosenReservasPostFile", vblnCambiosenReservasPostFile)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("OPERACIONES_UpdCambiosenReservasPostFile", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCambiosenReservasPostFile")
        End Try

    End Sub


    Public Function intIDOperacionxIDReserva(ByVal vintIDReserva As Integer) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pIDOperacion", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_SelIDOperacionxIDReservaOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxIDReserva(ByVal vintIDReserva As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDReserva", vintIDReserva)

        Return objADO.GetDataTable("OPERACIONES_SelxIDReserva", dc)
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOrdenServicioBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsOrdenServicioBT"

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsOrdenServicioBE, ByVal vstrAntiguoID As String, _
                                                       ByVal vintNuevoID As Integer)
        Try
            If Not BE.ListaOrdenesServiciosDetalles Is Nothing Then
                For Each ItemOrden As clsOrdenServicioDetalleBE In BE.ListaOrdenesServiciosDetalles
                    If ItemOrden.NuOrden_Servicio = vstrAntiguoID Then
                        ItemOrden.NuOrden_Servicio = vintNuevoID
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Function Insertar(ByVal BE As clsOrdenServicioBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim intNuOrden_Servicio As Integer = 0
            With dc
                .Add("@IDCab", BE.IDCab)
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@IDUsuarioResponsable", BE.IDUsuarioResponsable)
                .Add("@NuVehiculoProg", BE.NuVehiculoProg)
                .Add("@CoOrden_Servicio", BE.CoOrden_Servicio)
                .Add("@IDProveedorSetours", BE.IDProveedorSetours)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuOrden_Servicio", intNuOrden_Servicio)
            End With
            intNuOrden_Servicio = objADO.ExecuteSPOutput("ORDEN_SERVICIO_Ins", dc)

            ContextUtil.SetComplete()
            Return intNuOrden_Servicio

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Private Overloads Sub EliminarDetallexNuOrdenServicio(ByVal vintNuOrdenServicio As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuOrden_Servicio", vintNuOrdenServicio)

            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxNuOrden_Servicio", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_DelxNuOrden_Servicio", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxNuOrden_Servicio")
        End Try
    End Sub

    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsOrdenServicioBE, Optional ByVal strCoMoneda As String = "")
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuOrdenServicio", BE.NuOrden_Servicio)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@CoMoneda_FEgreso", strCoMoneda)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("ORDEN_SERVICIO_UpdxDocumento", dc)

            If Not BE.FlDesdeOtraForEgreso Then
                ActualizarFormasEgreso(BE)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
        End Try
    End Sub

    Private Overloads Sub Insertar_Detalle_Monedas(ByVal vintNuOrdenServicio As Integer, vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuOrden_Servicio", vintNuOrdenServicio)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("ORDEN_SERVICIO_DET_MONEDA_Ins_x_NuOrdenServicio", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxNuOrden_Servicio")
        End Try
    End Sub

    Private Sub ActualizarFormasEgreso(ByVal BE As clsOrdenServicioBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@NuOrden_Servicio", BE.NuOrden_Servicio)
            Dim dttFEgr As DataTable = objADO.GetDataTable("OPERACIONES_DET_SelVoucherxOrdenServicio", dc)

            For Each dr As DataRow In dttFEgr.Rows
                Dim objVoBT As New clsVoucherOperacionBT
                Dim objVoBE As New clsVoucherOperacionBE With {.IDVoucher = dr("IDVoucher"), _
                                                         .CoEstado = BE.CoEstado, _
                                                         .SsSaldo = BE.SsSaldo, _
                                                         .SsDifAceptada = BE.SsDifAceptada, _
                                                         .TxObsDocumento = "", _
                                                         .FlDesdeOtraForEgreso = True, _
                                                         .UserMod = BE.UserMod}

                objVoBT.ActualizarxDocumento(objVoBE)

            Next

            dttFEgr = objADO.GetDataTable("ORDENPAGO_DET_SelxOrdenServicio", dc)

            For Each dr As DataRow In dttFEgr.Rows
                Dim objOPBT As New clsOrdenPagoBT
                Dim objOPBE As New clsOrdenPagoBE With {.IDOrdPag = dr("IDOrdPag"), _
                                                         .CoEstado = BE.CoEstado, _
                                                         .SsSaldo = BE.SsSaldo, _
                                                         .SsDifAceptada = BE.SsDifAceptada, _
                                                         .TxObsDocumento = "", _
                                                         .FlDesdeOtraForEgreso = True, _
                                                         .UserMod = BE.UserMod}

                objOPBT.ActualizarxDocumento(objOPBE)

            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFormasEgreso")

        End Try
    End Sub


    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintNuOrdenServicio As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String, Optional ByVal vstrCoMoneda As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdenServicio", vintNuOrdenServicio)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@CoMoneda_FEgreso", vstrCoMoneda)

            objADO.ExecuteSP("ORDEN_SERVICIO_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Private Function intNuOrden_ServicioOutput(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, ByVal vstrIDProveedorSetours As String) As Integer
        Try
            Dim intNuOrden_Servicio As Integer = 0
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDProveedorSetours", vstrIDProveedorSetours)
                .Add("@pNuOrden_Servicio", 0)
            End With
            intNuOrden_Servicio = objADO.GetSPOutputValue("ORDEN_SERVICIO_SelNuOrden_ServicioOutput", dc)
            Return intNuOrden_Servicio
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub InsertarxEnvioCorreo(ByVal BE As clsOrdenServicioBE)
        If BE Is Nothing Then Exit Sub
        Try
            If BE.IDProveedorSetours Is Nothing Then BE.IDProveedorSetours = String.Empty
            Dim intNuOrden_Servicio As Integer = intNuOrden_ServicioOutput(BE.IDCab, BE.IDProveedor, BE.IDProveedorSetours)
            If intNuOrden_Servicio = 0 Then
                intNuOrden_Servicio = Insertar(BE)
            Else
                EliminarDetallexNuOrdenServicio(intNuOrden_Servicio)
            End If
            ActualizarIDsNuevosenHijosST(BE, BE.NuOrden_Servicio, intNuOrden_Servicio)

            Dim objBTDet As New clsOrdenServicioDetalleBT
            objBTDet.InsertarxEnvioCorreo(BE)

            Dim objBTAsig As New clsOrdenServicioDetalle_AsignadosBT
            objBTAsig.InsertarxEnvioCorreo(BE)

            ActualizarSaldoyMonto(intNuOrden_Servicio)

            'agregar insercion de detalle x moneda
            Insertar_Detalle_Monedas(intNuOrden_Servicio, BE.UserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarSaldoyMonto(ByVal vintNuOrdenServicio As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdenServicio", vintNuOrdenServicio)

            objADO.ExecuteSP("ORDEN_SERVICIO_UpdSaldo_Monto", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldo")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOrdenServicioDetalleBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsOrdenServicioDetalleBT"

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsOrdenServicioBE, ByVal vstrAntiguoID As String, _
                                                      ByVal vintNuevoID As Integer)
        Try
            If Not BE.ListaOrdenesServiciosDetallesAsignados Is Nothing Then
                For Each ItemOrden As clsOrdenServicioDetalle_AsignadosBE In BE.ListaOrdenesServiciosDetallesAsignados
                    If ItemOrden.NuOrden_Servicio_Det = vstrAntiguoID Then
                        ItemOrden.NuOrden_Servicio_Det = vintNuevoID
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Function Insertar(ByVal BE As clsOrdenServicioDetalleBE) As Integer
        Try
            Dim intNuOrden_Servicio_Det As Integer = 0
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuOrden_Servicio", BE.NuOrden_Servicio)
                .Add("@IDOperacion_Det", BE.IDOperacion_Det)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@FlServicioNoShow", BE.FlServicioNoShow)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuOrden_Servicio_Det", intNuOrden_Servicio_Det)
            End With
            intNuOrden_Servicio_Det = objADO.ExecuteSPOutput("ORDEN_SERVICIO_DET_Ins", dc)

            ContextUtil.SetComplete()
            Return intNuOrden_Servicio_Det
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Public Sub InsertarxEnvioCorreo(ByVal BE As clsOrdenServicioBE)
        Try
            For Each Item As clsOrdenServicioDetalleBE In BE.ListaOrdenesServiciosDetalles
                Dim intOrden_Servicio_Det As Integer = intNuOrden_Servicio_Det(Item.NuOrden_Servicio, Item.IDOperacion_Det, _
                                                                                 Item.IDServicio_Det, Item.FlServicioNoShow)

                If intOrden_Servicio_Det = 0 Then intOrden_Servicio_Det = Insertar(Item)
                ActualizarIDsNuevosenHijosST(BE, Item.NuOrden_Servicio_Det, intOrden_Servicio_Det)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function intNuOrden_Servicio_Det(ByVal vintNuOrdenServicio As Integer, ByVal vintIDOperacion_Det As Integer, _
                                            ByVal vintIDServicio_Det As Integer, ByVal vblnFlServicioNoShow As Boolean) As Integer
        Try
            Dim intOrden_Servicio_Det As Integer = 0
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuOrden_Servicio", vintNuOrdenServicio)
                .Add("@IDOperacion_Det", vintIDOperacion_Det)
                .Add("@IDServicio_Det", vintIDServicio_Det)
                .Add("@FlServicioNoShow", vblnFlServicioNoShow)
                .Add("@pNuOrden_Servicio_Det", 0)
            End With
            intOrden_Servicio_Det = objADO.GetSPOutputValue("ORDEN_SERVICIO_DET_SelNuOrden_Servicio_DetOutput", dc)
            Return intOrden_Servicio_Det
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOrdenServicioDetalle_AsignadosBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsOrdenServicioDetalleBT"

    Private Overloads Sub Insertar(ByVal BE As clsOrdenServicioDetalle_AsignadosBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuOrden_Servicio_Det", BE.NuOrden_Servicio_Det)
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@NuVehiculoProg", BE.NuVehiculoProg)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Function blnExisteProveedor(ByVal vintNuOrdenServicioDet As Integer, ByVal vstrIDProveedor As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExiste As Boolean = False
            With dc
                .Add("@NuOrden_Servicio_Det", vintNuOrdenServicioDet)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@pExits", False)
            End With
            blnExiste = objADO.GetSPOutputValue("ORDEN_SERVICIO_DET_ASIGNADOS_SelExists", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub InsertarxEnvioCorreo(ByVal BE As clsOrdenServicioBE)
        Try
            For Each Item As clsOrdenServicioDetalle_AsignadosBE In BE.ListaOrdenesServiciosDetallesAsignados
                If Not blnExisteProveedor(Item.NuOrden_Servicio_Det, Item.IDProveedor) Then
                    Insertar(Item)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsVoucherOperacionBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsVoucherOperacionBT"
    Private Overloads Function Insertar(ByVal BE As clsVoucherOperacionBE) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@ServExportac", BE.ServExportac)
            dc.Add("@ExtrNoIncluid", BE.ExtrNoIncluid)
            dc.Add("@ObservVoucher", BE.ObservVoucher)
            dc.Add("@CoUbigeo_Oficina", BE.CoUbigeo_Oficina)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@pIDVoucher", 0)

            Dim intIDVoucher As Integer = objADO.ExecuteSPOutput("VOUCHER_OPERACIONES_Ins", dc)

            If intIDVoucher < BE.IDVoucher Then
                Actualizar(BE, intIDVoucher)
                intIDVoucher = BE.IDVoucher
            End If

            ContextUtil.SetComplete()
            Return intIDVoucher

        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function
    Public Sub ActualizarSaldo(ByVal ListaVouchers As List(Of clsVoucherOperacionBE))
        Try
            For Each ItemVou As clsVoucherOperacionBE In ListaVouchers
                If ItemVou.Accion <> "" Then
                    Dim dcV As New Dictionary(Of String, String)
                    dcV.Add("@IDVoucher", ItemVou.IDVoucher)
                    dcV.Add("@UserMod", ItemVou.UserMod)
                    objADO.ExecuteSP("VOUCHER_OPERACIONES_UpdSaldo", dcV)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldo")
        End Try
    End Sub

    Public Sub Insertar_VoucherDetalle_Bloque(ByVal ListaVouchers As List(Of clsVoucherOperacionBE))
        Try
            For Each ItemVou As clsVoucherOperacionBE In ListaVouchers
                If ItemVou.Accion <> "" Then
                    Dim dcV As New Dictionary(Of String, String)
                    dcV.Add("@IDVoucher", ItemVou.IDVoucher)
                    dcV.Add("@UserMod", ItemVou.UserMod)
                    objADO.ExecuteSP("VOUCHER_OPERACIONES_Det_Ins_x_Voucher", dcV)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldo")
        End Try
    End Sub

    Public Sub ActualizarSaldo_Estado(ByVal ListaVouchers As List(Of clsVoucherOperacionBE))
        Try
            For Each ItemVou As clsVoucherOperacionBE In ListaVouchers
                If ItemVou.Accion <> "" Then
                    Dim dcV As New Dictionary(Of String, String)
                    dcV.Add("@IDVoucher", ItemVou.IDVoucher)
                    dcV.Add("@UserMod", ItemVou.UserMod)
                    objADO.ExecuteSP("VOUCHER_OPERACIONES_UpdSaldo", dcV)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldo")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsVoucherOperacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDVoucher", BE.IDVoucher)
            dc.Add("@ServExportac", BE.ServExportac)
            dc.Add("@ExtrNoIncluid", BE.ExtrNoIncluid)
            dc.Add("@ObservVoucher", BE.ObservVoucher)
            dc.Add("@CoUbigeo_Oficina", BE.CoUbigeo_Oficina)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("VOUCHER_OPERACIONES_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try

    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsVoucherOperacionBE, ByVal vintIDVoucherAnt As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDVoucherNuevo", BE.IDVoucher)
            dc.Add("@IDVoucherAntiguo", vintIDVoucherAnt)
            dc.Add("@ServExportac", BE.ServExportac)
            dc.Add("@ExtrNoIncluid", BE.ExtrNoIncluid)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("VOUCHER_OPERACIONES_UpdMenor", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar2")
        End Try

    End Sub

    Private Overloads Sub ActualizarEstado(ByVal vintIDVoucher As Integer, ByVal vstrCoEstado As String, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDVoucher", vintIDVoucher)
                .Add("@CoEstado", vstrCoEstado)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("VOUCHER_OPERACIONES_Anu", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Anular")
        End Try
    End Sub

    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsVoucherOperacionBE, Optional ByVal vstrCoMoneda As String = "")
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDVoucher", BE.IDVoucher)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@CoMoneda_FEgreso", vstrCoMoneda)
                .Add("@UserMod", BE.UserMod)

            End With

            objADO.ExecuteSP("VOUCHER_OPERACIONES_UpdxDocumento", dc)

            If Not BE.FlDesdeOtraForEgreso Then
                ActualizarFormasEgreso(BE)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
        End Try
    End Sub

    Private Sub ActualizarFormasEgreso(ByVal BE As clsVoucherOperacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@IDVoucher", BE.IDVoucher)
            Dim dttFEgr As DataTable = objADO.GetDataTable("ORDENPAGO_DET_SelxVoucher", dc)

            For Each dr As DataRow In dttFEgr.Rows
                Dim objOPBT As New clsOrdenPagoBT
                Dim objOPBE As New clsOrdenPagoBE With {.IDOrdPag = dr("IDOrdPag"), _
                                                         .CoEstado = BE.CoEstado, _
                                                         .SsSaldo = BE.SsSaldo, _
                                                         .SsDifAceptada = BE.SsDifAceptada, _
                                                         .TxObsDocumento = "", _
                                                         .FlDesdeOtraForEgreso = True, _
                                                         .UserMod = BE.UserMod}

                objOPBT.ActualizarxDocumento(objOPBE)

            Next

            dttFEgr = objADO.GetDataTable("ORDEN_SERVICIO_DET_SelxVoucher", dc)

            For Each dr As DataRow In dttFEgr.Rows
                Dim objOSBT As New clsOrdenServicioBT
                Dim objOSBE As New clsOrdenServicioBE With {.NuOrden_Servicio = dr("NuOrden_Servicio"), _
                                                         .CoEstado = BE.CoEstado, _
                                                         .SsSaldo = BE.SsSaldo, _
                                                         .SsDifAceptada = BE.SsDifAceptada, _
                                                         .TxObsDocumento = "", _
                                                         .FlDesdeOtraForEgreso = True, _
                                                         .UserMod = BE.UserMod}

                objOSBT.ActualizarxDocumento(objOSBE)

            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFormasEgreso")

        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintNuVoucher As Integer, ByVal vdblTotalOrig As Double, ByVal vstrUserMod As String, Optional ByVal vstrCoMoneda As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVoucher", vintNuVoucher)
            dc.Add("@SsTotalOrig", vdblTotalOrig)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@CoMoneda_FEgreso", vstrCoMoneda)

            objADO.ExecuteSP("VOUCHER_OPERACIONES_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Public Overloads Sub ActualizarEstado_Multiple(ByVal vintNuVoucher As Integer, ByVal vstrCoEstado As String, ByVal vstrUserMod As String, Optional ByVal vstrCoMoneda As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVoucher", vintNuVoucher)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@CoMoneda_FEgreso", vstrCoMoneda)

            objADO.ExecuteSP("VOUCHER_OPERACIONES_DET_Upd_Estado_Multiple", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintIDVoucher As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDVoucher", vintIDVoucher)

            'Dim dcVou As New Dictionary(Of String, String)
            'dcVou.Add("@IDVoucher", vintIDVoucher)
            'dcVou.Add("@pExiste", False)

            'Dim blnEncontro As Boolean = objADO.GetSPOutputValue("OPERACIONES_DET_SelExisteVoucherOutput", dcVou)

            'Eliminar documentos proveedor x voucher
            EliminarDocumentos_Proveedor_Det(vintIDVoucher)
            EliminarDocumentos_Proveedor(vintIDVoucher)
            'If Not blnEncontro Then
            objADO.ExecuteSP("VOUCHER_OPERACIONES_Del", dc)
            'End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try

    End Sub

    Public Sub EliminarDocumentos_Proveedor(ByVal vintNuVoucher As Int32)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuVoucher", vintNuVoucher)

            End With

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Del_xNuVoucher", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub EliminarDocumentos_Proveedor_Det(ByVal vintNuVoucher As Int32)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuVoucher", vintNuVoucher)

            End With

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_DET_DEL_xNuVoucher", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    ' ''Private Overloads Sub InsertarEnHistorial(ByVal BE As clsVoucherOperacionBE)
    ' ''    Try
    ' ''        Dim BEVoucherHst As New clsVoucherHistoricoBE
    ' ''        With BEVoucherHst
    ' ''            .IDVoucher = BE.IDVoucher
    ' ''            .IDOperacion_Det = BE.IdOperacion_Det
    ' ''            .UserMod = BE.UserMod
    ' ''        End With

    ' ''        Dim objBT As New clsVoucherHistoricoBT
    ' ''        objBT.Insertar(BEVoucherHst)

    ' ''        ContextUtil.SetComplete()
    ' ''    Catch ex As Exception
    ' ''        ContextUtil.SetAbort()
    ' ''        Throw
    ' ''    End Try
    ' ''End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOperacionBE)
        Dim dc As Dictionary(Of String, String)
        Try
            For Each Item As clsVoucherOperacionBE In BE.ListaVouchers
                If Item.Accion = "B" Then
                    Dim objDetOpe As New clsDetalleOperacionBT
                    objDetOpe.ActualizarIDVoucherxIDVoucher(Item.IDVoucherAnt, 0, BE.UserMod)
                    'InsertarEnHistorial(Item)
                    ActualizarEstado(Item.IDVoucher, "AN", Item.UserMod)
                End If
            Next
            For Each Item As clsVoucherOperacionBE In BE.ListaVouchers
                dc = New Dictionary(Of String, String)
                If Item.Accion = "N" Then
                    Dim intIDVoucher As Integer = Insertar(Item)
                    Dim objDetOpe As New clsDetalleOperacionBT
                    objDetOpe.ActualizarIDVoucherNuevosenHijosST(BE, Item.IDVoucher, intIDVoucher)

                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                End If
            Next

            'ActualizarSaldo(BE.ListaVouchers)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsVoucherHistoricoBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsVoucherHistoricoBT"

    Public Overloads Sub Insertar(ByVal BE As clsVoucherHistoricoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDVoucher", BE.IDVoucher)
                .Add("@IDOperacion_Det", BE.IDOperacion_Det)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("VOUCHER_HISTORICO_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Function blnExisteVoucher(ByVal vintIDVoucher As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDVoucher", vintIDVoucher)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("VOUCHER_OPERACIONES_Sel_ExistsOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub Insertar(ByVal BE As clsOperacionBE)
        If BE.ListaVoucherHistorico Is Nothing Then Exit Sub
        Try
            For Each Item As clsVoucherHistoricoBE In BE.ListaVoucherHistorico
                If Item.Accion = "N" And blnExisteVoucher(Item.IDVoucher) Then
                    Insertar(Item)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleOperacionBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsDetalleOperacionBT"

    Private Overloads Function Insertar(ByVal BE As clsDetalleOperacionBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion", BE.IDOperacion)
            dc.Add("@IDReserva_Det", BE.IDReserva_Det)

            dc.Add("@Item", BE.Item)
            dc.Add("@Dia", Format(BE.Dia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@FechaOut", BE.FechaOut)
            dc.Add("@Noches", BE.Noches)
            dc.Add("@IDUbigeo", BE.IDUbigeo)
            dc.Add("@IDServicio", BE.IDServicio)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
            dc.Add("@Especial", BE.Especial)
            dc.Add("@MotivoEspecial", BE.MotivoEspecial)
            dc.Add("@RutaDocSustento", BE.RutaDocSustento)
            dc.Add("@Desayuno", BE.Desayuno)
            dc.Add("@Lonche", BE.Lonche)
            dc.Add("@Almuerzo", BE.Almuerzo)
            dc.Add("@Cena", BE.Cena)
            dc.Add("@Transfer", BE.Transfer)
            If IsNumeric(BE.IDDetTransferOri) Then
                dc.Add("@IDDetTransferOri", BE.IDDetTransferOri)
            Else
                dc.Add("@IDDetTransferOri", 0)
            End If
            If IsNumeric(BE.IDDetTransferDes) Then
                dc.Add("@IDDetTransferDes", BE.IDDetTransferDes)
            Else
                dc.Add("@IDDetTransferDes", 0)
            End If
            dc.Add("@TipoTransporte", BE.TipoTransporte)
            dc.Add("@IDUbigeoOri", BE.IDUbigeoOri)
            dc.Add("@IDUbigeoDes", BE.IDUbigeoDes)

            dc.Add("@IDIdioma", BE.IDIdioma)


            dc.Add("@NroPax", BE.NroPax)
            dc.Add("@NroLiberados", BE.NroLiberados)
            dc.Add("@Tipo_Lib", BE.Tipo_Lib)
            dc.Add("@Cantidad", BE.Cantidad)

            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoRealAnt", BE.CostoRealAnt)
            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@Margen", BE.Margen)

            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenAplicadoAnt", BE.MargenAplicadoAnt)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@Total", BE.Total)

            dc.Add("@TotalOrig", BE.TotalOrig)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@MargenImpto", BE.MargenImpto)

            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@TotImpto", BE.TotImpto)


            dc.Add("@Servicio", BE.Servicio)

            dc.Add("@IDVoucher", BE.IDVoucher)
            dc.Add("@ObservBiblia", BE.ObservBiblia)
            dc.Add("@ObservInterno", BE.ObservInterno)
            dc.Add("@CantidadaPagar", BE.CantidadaPagar)
            dc.Add("@ObservVoucher", BE.ObservVoucher)
            'dc.Add("@Recordatorio", BE.Recordatorio)
            'dc.Add("@FechaRecordatorio", BE.FechaRecordatorio)
            dc.Add("@VerObserVoucher", BE.VerObserVoucher)
            dc.Add("@VerObserBiblia", BE.VerObserBiblia)
            dc.Add("@IDTipoOC", BE.IDTipoOC)
            dc.Add("@IDGuiaProveedor", BE.IDGuiaProveedor)
            dc.Add("@FlServicioNoShow", BE.FlServicioNoShow)
            dc.Add("@FlServInManualOper", BE.FlServInManualOper)

            '           ,@NetoHab,
            '@IgvHab,
            '@TotalHab,
            '@NetoGen,
            '@IgvGen,
            '@TotalGen

            dc.Add("@NetoHab", BE.NetoHab)
            dc.Add("@IgvHab", BE.IgvHab)
            dc.Add("@TotalHab", BE.TotalHab)
            dc.Add("@NetoGen", BE.NetoGen)
            dc.Add("@IgvGen", BE.IgvGen)
            dc.Add("@TotalGen", BE.TotalGen)
            dc.Add("@IDMoneda", BE.IDMoneda)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@CantidadAPagarEditado", BE.CantidadAPagarEditado)
            dc.Add("@NetoHabEditado", BE.NetoHabEditado)
            dc.Add("@IgvHabEditado", BE.IgvHabEditado)
            dc.Add("@pIDOperacion_Det", 0)

            Dim intIDOperacion_Det As Integer = objADO.ExecuteSPOutput("OPERACIONES_DET_Ins", dc)


            ContextUtil.SetComplete()

            Return intIDOperacion_Det
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Private Overloads Function Actualizar(ByVal BE As clsDetalleOperacionBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)

            dc.Add("@Dia", Format(BE.Dia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@FechaOut", Format(BE.FechaOut, "dd/MM/yyyy HH:mm:ss"))
            'dc.Add("@FechaOut", BE.FechaOut)
            dc.Add("@Noches", BE.Noches)

            dc.Add("@Especial", BE.Especial)
            dc.Add("@MotivoEspecial", BE.MotivoEspecial)
            dc.Add("@RutaDocSustento", BE.RutaDocSustento)
            dc.Add("@Desayuno", BE.Desayuno)
            dc.Add("@Lonche", BE.Lonche)
            dc.Add("@Almuerzo", BE.Almuerzo)
            dc.Add("@Cena", BE.Cena)

            If IsNumeric(BE.IDDetTransferOri) Then
                dc.Add("@IDDetTransferOri", BE.IDDetTransferOri)
            Else
                dc.Add("@IDDetTransferOri", 0)
            End If
            If IsNumeric(BE.IDDetTransferDes) Then
                dc.Add("@IDDetTransferDes", BE.IDDetTransferDes)
            Else
                dc.Add("@IDDetTransferDes", 0)
            End If
            dc.Add("@TipoTransporte", BE.TipoTransporte)
            dc.Add("@IDUbigeoOri", BE.IDUbigeoOri)
            dc.Add("@IDUbigeoDes", BE.IDUbigeoDes)

            dc.Add("@IDIdioma", BE.IDIdioma)


            dc.Add("@NroPax", BE.NroPax)
            dc.Add("@NroLiberados", BE.NroLiberados)
            dc.Add("@Tipo_Lib", BE.Tipo_Lib)
            dc.Add("@Cantidad", BE.Cantidad)

            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoRealAnt", BE.CostoRealAnt)
            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@Margen", BE.Margen)

            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenAplicadoAnt", BE.MargenAplicadoAnt)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@Total", BE.Total)

            dc.Add("@TotalOrig", BE.TotalOrig)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@MargenImpto", BE.MargenImpto)

            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@TotImpto", BE.TotImpto)


            dc.Add("@Servicio", BE.Servicio)

            dc.Add("@IDVoucher", BE.IDVoucher)
            dc.Add("@ObservBiblia", BE.ObservBiblia)
            dc.Add("@ObservInterno", BE.ObservInterno)
            dc.Add("@ObservVoucher", BE.ObservVoucher)
            'dc.Add("@Recordatorio", BE.Recordatorio)
            'dc.Add("@FechaRecordatorio", BE.FechaRecordatorio)
            dc.Add("@VerObserVoucher", BE.VerObserVoucher)
            dc.Add("@VerObserBiblia", BE.VerObserBiblia)
            dc.Add("@IDGuiaProveedor", BE.IDGuiaProveedor)
            dc.Add("@FlServicioNoShow", BE.FlServicioNoShow)
            dc.Add("@CantidadAPagar", BE.CantidadaPagar)
            dc.Add("@NetoHab", BE.NetoHab)
            dc.Add("@IgvHab", BE.IgvHab)
            dc.Add("@TotalHab", BE.TotalHab)
            dc.Add("@NetoGen", BE.NetoGen)
            dc.Add("@IgvGen", BE.IgvGen)
            dc.Add("@TotalGen", BE.TotalGen)
            dc.Add("@IDMoneda", BE.IDMoneda)
            dc.Add("@FlServicioNoCobrado", BE.FlServicioNoCobrado)
            dc.Add("@SSTotalGenAntNoCobrado", BE.SSTotalGenAntNoCobrado)
            dc.Add("@QtCantidadAPagarAntNoCobrado", BE.QtCantidadAPagarAntNoCobrado)
            dc.Add("@UserMod", BE.UserMod)

            dc.Add("@CantidadAPagarEditado", BE.CantidadAPagarEditado)
            dc.Add("@NetoHabEditado", BE.NetoHabEditado)
            dc.Add("@IgvHabEditado", BE.IgvHabEditado)

            objADO.ExecuteSP("OPERACIONES_DET_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Function

    Private Overloads Sub Eliminar(ByVal vintIDOperaciones_Det As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion_Det", vintIDOperaciones_Det)


            objADO.ExecuteSP("OPERACIONES_DET_PAX_DelxIDOperacion_Det", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDOperacion_Det", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_DelxIDOperacionDet", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_DelxIDOperacion_Det", dc)
            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_DelxIDOperacion_Det", dc)
            objADO.ExecuteSP("VOUCHER_HISTORICO_DelxIDOperacion_Det", dc)
            objADO.ExecuteSP("OPERACIONES_Det_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try

    End Sub

    Private Overloads Sub ActualizarFechaObservaciones(ByVal BE As clsDetalleOperacionBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)
            dc.Add("@Dia", Format(BE.Dia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@ObservacionVoucher", BE.ObservVoucher)
            dc.Add("@ObservacionBiblia", BE.ObservBiblia)
            dc.Add("@ObservacionInterno", BE.ObservInterno)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("OPERACIONES_DET_UpdHorarioObservaciones", dc)

            ActualizarFechaReservasVentas(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarHorarioObservaciones")
        End Try
    End Sub

    Public Sub ActualizarxProgramacion(ByVal BE As clsOperacionBE)
        Try
            For Each Item As clsDetalleOperacionBE In BE.ListaDetalleOperaciones
                Dim objBTDet As New clsDetalleOperacionBT
                objBTDet.ActualizarFechaObservaciones(Item)
            Next

            Dim objBTCoti As New clsCotizacionBT
            objBTCoti.Actualizar_Observacion(BE.CotizacionObservacion.IDCab, BE.CotizacionObservacion.ObservacionVentas)

            Dim objBTServ As New clsDetalleServiciosOperacionBT
            objBTServ.InsertarActualizarEliminar(BE)

            ' ''Dim objBTSin As New clsSinceradoBT
            ' ''objBTSin.InsertarEliminar(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Sub

    Private Sub ActualizarFechaReservasVentas(ByVal BE As clsDetalleOperacionBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)

            Dim intIDReserva_Det As Integer = 0
            Dim intIDDet As Integer = 0
            Dim dtt As DataTable = objADO.GetDataTable("OPERACIONES_DET_SelIDResev_IDDetxIDOperacionDet", dc)

            intIDReserva_Det = dtt(0)("IDReserva_Det")
            intIDDet = dtt(0)("IDDet")

            Dim objBTResv As New clsReservaBT.clsDetalleReservaBT
            objBTResv.ActualizarCampoDia(intIDReserva_Det, BE.Dia, BE.UserMod)

            Dim objBTCoti As New clsDetalleVentasCotizacionBT
            objBTCoti.ActualizarCampoDia(intIDDet, BE.Dia, BE.UserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub EliminarxIDDet(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_DelxIDDet", dc)
            objADO.ExecuteSP("OPERACIONES_DET_PAX_DelxIDDet", dc)
            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_DelxIDDet", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DelxIDDet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
        End Try
    End Sub

    Public Sub EliminarxIDReserva_Det(ByVal vintIDReserva_Det As Integer)
        Try

            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            '''''''''''
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDReservas_Det", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_DelxIDReservas_Det", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_DelxIDReserva_Det", dc)
            objADO.ExecuteSP("OPERACIONES_DET_PAX_DelxIDReserva_Det", dc)
            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_DelxIDReserva_Det", dc)
            objADO.ExecuteSP("VOUCHER_HISTORICO_DelxIDReserva_Det", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DelxIDReserva_Det", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDReserva_Det")
        End Try
    End Sub

    Public Sub ActualizarxNuevoAcomodoTMP(ByVal vintIDOperacion As Integer, ByVal vintIDOperacion_Det As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)

            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_UpdxAcomodoTMP", dc)

            ContextUtil.SetComplete()


        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxNuevoAcomodoTMP")
        End Try
    End Sub
    Public Function InsertarxNuevoAcomodoTMP(ByVal vintIDOperacion As Integer, ByVal vintIDServicio_Det As Integer, _
                                  ByVal vbytCapacidadHab As Byte, ByVal vblnEsMatrimonial As Boolean, _
                                  ByVal vdatDia As Date, ByVal vstrUserMod As String, ByVal vintIDOperacion_Det As Integer) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            'dc.Add("@IDOperacion", vintIDOperacion)
            'dc.Add("@IDServicio_Det", vintIDServicio_Det)
            'dc.Add("@CapacidadHab", vbytCapacidadHab)
            'dc.Add("@EsMatrimonial", vblnEsMatrimonial)

            'dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@IDOperacion_DetTMP", vintIDOperacion_Det)

            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@pIDOperacion_Det", 0)


            Dim intIDOperacion_Det As Integer = objADO.ExecuteSPOutput("OPERACIONES_DET_InsxAcomodoTMP", dc)

            ContextUtil.SetComplete()

            Return intIDOperacion_Det
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxNuevoAcomodoTMP")
        End Try
    End Function
    Public Sub ActualizarIDVoucher(ByVal vintIDOperacion_Det As Integer, _
                                   ByVal vintIDVoucherNue As Integer, ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDVoucherNue", vintIDVoucherNue)
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)

            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_Det_UpdIDVoucher", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarIDVoucher")
        End Try
    End Sub
    Public Sub ActualizarIDVoucherxIDVoucher(ByVal vintIDVoucherAnt As Integer, _
                                   ByVal vintIDVoucherNue As Integer, ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDVoucherNue", vintIDVoucherNue)
            dc.Add("@IDVoucherAnt", vintIDVoucherAnt)

            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_Det_UpdIDVoucherxIDVoucher", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarIDVoucherxIDVoucher")
        End Try
    End Sub

    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsOperacionBE, ByVal vstrAntiguoID As String, _
                                                           ByVal vintNuevoID As Integer)
        Try

            For Each ItemDet As clsPaxDetalleOperacionBE In BE.ListaPaxDetalle
                If ItemDet.IDOperacion_Det = vstrAntiguoID Then
                    ItemDet.IDOperacion_Det = vintNuevoID
                End If
            Next

            For Each ItemDet As clsDetalleServiciosOperacionBE In BE.ListaDetServiciosOperac
                If ItemDet.IDOperacion_Det = vstrAntiguoID Then
                    ItemDet.IDOperacion_Det = vintNuevoID
                End If
            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarIDsHijosST(ByVal BE As clsOperacionBE, ByVal vstrIDOperacion_Det As String)
        Try
            If BE.ListaPaxDetalle Is Nothing Then GoTo sgte
BorrarPax:
            Dim intIndSTPax As Int16 = 0
            For Each ItemDet As clsPaxDetalleOperacionBE In BE.ListaPaxDetalle
                If ItemDet.IDOperacion_Det = vstrIDOperacion_Det Then
                    BE.ListaPaxDetalle.RemoveAt(intIndSTPax)
                    GoTo BorrarPax
                End If
                intIndSTPax += 1
            Next
sgte:
            If BE.ListaDetServiciosOperac Is Nothing Then Exit Sub

BorrarDet:
            Dim intIndSTDtServ As Int16 = 0
            For Each ItemDet As clsDetalleServiciosOperacionBE In BE.ListaDetServiciosOperac
                If ItemDet.IDOperacion_Det = vstrIDOperacion_Det Then
                    BE.ListaDetServiciosOperac.RemoveAt(intIndSTDtServ)
                    GoTo BorrarDet
                End If
                intIndSTDtServ += 1
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Overloads Sub ActualizarIDVoucherNuevosenHijosST(ByRef BE As clsOperacionBE, ByVal vstrAntiguoID As String, _
                                                       ByVal vintNuevoID As Integer)
        Try
            If Not BE.ListaDetalleOperaciones Is Nothing Then
                For Each ItemDgv As clsDetalleOperacionBE In BE.ListaDetalleOperaciones
                    If ItemDgv.IDVoucher = vstrAntiguoID Then
                        ItemDgv.IDVoucher = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaVouchers Is Nothing Then
                For Each item As clsVoucherOperacionBE In BE.ListaVouchers
                    If item.IDVoucher = vstrAntiguoID Then
                        item.IDVoucher = vintNuevoID
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Overloads Sub InsertarActualizarEliminar(ByRef BE As clsOperacionBE)
        Dim dc As Dictionary(Of String, String)
        Try
            For Each Item As clsDetalleOperacionBE In BE.ListaDetalleOperaciones
                dc = New Dictionary(Of String, String)
                If Item.Accion = "N" Then
                    Dim strIDOperacion_DetAntiguo As String = Item.IDOperacion_Det
                    Dim intIDOperacion_DetNuevo As Integer = Insertar(Item)

                    ActualizarIDsNuevosenHijosST(BE, strIDOperacion_DetAntiguo, intIDOperacion_DetNuevo)

                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then

                    EliminarIDsHijosST(BE, Item.IDOperacion_Det)

                    If IsNumeric(Item.IDOperacion_Det) Then
                        Eliminar(Item.IDOperacion_Det)
                    End If

                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Function blnExistenOperaciones_DetxIDReserva_Det(ByVal vintIDReserva_Det As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_Sel_ExistenxIDReserva_DetOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenOperaciones_DetxIDDet(ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_Sel_ExistenxIDDetOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsPaxDetalleOperacionBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsPaxDetalleOperacionBT"
    Private Overloads Sub Insertar(ByVal BE As clsPaxDetalleOperacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)


            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)
            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("OPERACIONES_DET_PAX_Ins", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal BE As clsPaxDetalleOperacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)
            dc.Add("@IDPax", BE.IDPax)

            objADO.ExecuteSP("OPERACIONES_DET_PAX_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOperacionBE)
        Dim dc As Dictionary(Of String, String)
        Try
            For Each Item As clsPaxDetalleOperacionBE In BE.ListaPaxDetalle
                dc = New Dictionary(Of String, String)
                If Item.Selecc Then
                    If IsNumeric(Item.IDOperacion_Det) Then
                        If Not blnExiste(Item.IDOperacion_Det, Item.IDPax) Then Insertar(Item)
                    End If

                Else
                    Eliminar(Item)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function blnExiste(ByVal vintIDOperacion_Det As Int32, ByVal vintIDPax As Int32) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDPax", vintIDPax)
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("OPERACIONES_DET_PAX_Sel_ExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub ActualizarNroPax(ByVal vintIDOperacion_Det As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@UserMod", vstrUserMod)


            objADO.ExecuteSP("OPERACIONES_DET_Upd_NroPax", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroPax")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsControlCalidadProBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsControlCalidadProvBT"

    Public Overloads Function Insertar(ByVal BE As clsControlCalidadProBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", BE.IDCab)
                .Add("@TxComentario", BE.TxComentario)
                .Add("@IDPax", BE.IDPax)
                .Add("@CoProveedor", BE.CoProveedor)
                .Add("@CoCliente", BE.CoCliente)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuControlCalidad", BE.NuControlCalidad)
            End With

            Dim intNuControlCalidad As Integer = Convert.ToInt32(objADO.ExecuteSPOutput("CONTROL_CALIDAD_PRO_Ins", dc))
            ActualizarIDsNuevosenHijosST(BE, BE.NuControlCalidad, intNuControlCalidad)

            Dim objBTCuest As New clsControlCalidadPro_CuestionarioBT
            objBTCuest.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()
            Return intNuControlCalidad
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En " & strNombreClase & ".Insertar")
        End Try
    End Function

    Public Overloads Sub Actualizar(BE As clsControlCalidadProBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@TxComentario", BE.TxComentario)
                .Add("@IDPax", BE.IDPax)
                .Add("@CoProveedor", BE.CoProveedor)
                .Add("@CoCliente", BE.CoCliente)
                .Add("@UserMod", BE.UserMod)
                .Add("@NuControlCalidad", BE.NuControlCalidad)
            End With
            objADO.ExecuteSP("CONTROL_CALIDAD_PRO_Upd", dc)

            Dim objBTCuest As New clsControlCalidadPro_CuestionarioBT
            objBTCuest.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuControlCalidad As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim objBTCuest As New clsControlCalidadPro_CuestionarioBT
            objBTCuest.EliminarxNuControlCalidad(vintNuControlCalidad)

            dc.Add("@NuControlCalidad", vintNuControlCalidad)
            objADO.ExecuteSP("CONTROL_CALIDAD_PRO_DelxNuControlCalidad", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsControlCalidadProBE, ByVal vintIDAntiguo As Integer, ByVal vintIDNuevo As Integer)
        Try
            If BE.ListaControlCalidadPro_Cuestionario IsNot Nothing Then
                For Each Item As clsControlCalidadPro_CuestionarioBE In BE.ListaControlCalidadPro_Cuestionario
                    If Item.NuControlCalidad = vintIDAntiguo Then
                        Item.NuControlCalidad = vintIDNuevo
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsControlCalidadPro_CuestionarioBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsControlCalidadProvBT"

    Private Overloads Sub Insertar(ByVal BE As clsControlCalidadPro_CuestionarioBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuControlCalidad", BE.NuControlCalidad)
                .Add("@NuCuest", BE.NuCuest)
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@FlExcelent", BE.FlExcelent)
                .Add("@FlVeryGood", BE.FlVeryGood)
                .Add("@FlGood", BE.FlGood)
                .Add("@FlAverage", BE.FlAverage)
                .Add("@FlPoor", BE.FlPoor)
                .Add("@TxComentario", BE.TxComentario)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("CONTROL_CALIDAD_PRO_MACUESTIONARIO_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsControlCalidadPro_CuestionarioBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuControlCalidad", BE.NuControlCalidad)
                .Add("@NuCuest", BE.NuCuest)
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@FlExcelent", BE.FlExcelent)
                .Add("@FlVeryGood", BE.FlVeryGood)
                .Add("@FlGood", BE.FlGood)
                .Add("@FlAverage", BE.FlAverage)
                .Add("@FlPoor", BE.FlPoor)
                .Add("@TxComentario", BE.TxComentario)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("CONTROL_CALIDAD_PRO_MACUESTIONARIO_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub EliminarxNuControlCalidad(ByVal vintNuControlCalidad As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuControlCalidad", vintNuControlCalidad)
            objADO.ExecuteSP("CONTROL_CALIDAD_PRO_MACUESTIONARIO_DelxNuControlCalidad", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error : " & ex.Message & " En " & strNombreClase & ".EliminarxNuControlCalidad")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsControlCalidadProBE)
        If BE.ListaControlCalidadPro_Cuestionario Is Nothing Then Exit Sub
        Try
            For Each Item As clsControlCalidadPro_CuestionarioBE In BE.ListaControlCalidadPro_Cuestionario
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsSinceradoBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsDetalleServiciosOperacion_SinceradoBT"

    Private Overloads Function Insertar(ByVal BE As clsSinceradoBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Dim intNuSincerado As Integer = 0
        Try
            With dc
                .Add("@IDOperacion_Det", BE.IDOperacion_Det)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuSincerado", 0)
            End With
            intNuSincerado = CInt(objADO.ExecuteSPOutput("SINCERADO_Ins", dc))

            ContextUtil.SetComplete()
            Return intNuSincerado
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Private Overloads Sub Eliminar(ByVal vintNuSincerado As Integer, ByVal vintIDOperacion_Det As Integer, _
                                   ByVal vintIDServicio_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado", vintNuSincerado)

            Dim objDetServ As New clsDetalleServiciosOperacionBT
            objDetServ.ActualizarFlagSincerado(vintIDOperacion_Det, vintIDServicio_Det)

            Dim objSinDP As New clsSincerado_Det_PaxBT
            objSinDP.EliminarxNuSincerado(vintNuSincerado)

            Dim objSinD As New clsSincerado_DetBT
            objSinD.EliminarxNuSincerado(vintNuSincerado)

            objADO.ExecuteSP("SINCERADO_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Private Overloads Function blnExistexIDOperacionDetxIDServicioDet(ByVal vintIDOperacion_Det As Integer, ByVal vintIDServicio_Det As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Dim blnExiste As Boolean = False
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@pExiste", False)

            blnExiste = objADO.GetSPOutputValue("SINCERADO_SelExistOutPut", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub InsertarEliminar(ByVal BE As clsOperacionBE)
        If BE.ListaSincerado Is Nothing Then Exit Sub
        Try
            For Each Item As clsSinceradoBE In BE.ListaSincerado
                If Item.Accion = "N" Then
                    If Not blnExistexIDOperacionDetxIDServicioDet(Item.IDOperacion_Det, Item.IDServicio_Det) Then
                        Dim intNuSincerado As Integer = Insertar(Item)
                        ActualizarIDsNuevosenHijosST(BE, Item.NuSincerado, intNuSincerado)
                    End If
                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.NuSincerado) Then
                        Eliminar(Item.NuSincerado, Item.IDOperacion_Det, Item.IDServicio_Det)
                    End If
                End If
            Next

            Dim objBTDet As New clsSincerado_DetBT
            objBTDet.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsOperacionBE, ByVal vstrIDAntiguo As String, ByVal vintIDNuevo As Integer)
        Try
            If Not BE.ListaSincerado_Det Is Nothing Then
                For Each ItemDet As clsSincerado_DetBE In BE.ListaSincerado_Det
                    If ItemDet.NuSincerado = vstrIDAntiguo Then
                        ItemDet.NuSincerado = vintIDNuevo
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Sub EliminarIDsHijosST(ByRef BE As clsOperacionBE, ByVal vstrNuSincerado As String)
        Try
            If Not BE.ListaSincerado_Det Is Nothing Then
BorrarSTDet:
                For Each ItemDet As clsSincerado_DetBE In BE.ListaSincerado_Det
                    If ItemDet.NuSincerado = vstrNuSincerado Then
                        BE.ListaSincerado_Det.Remove(ItemDet)
                        GoTo BorrarSTDet
                    End If
                Next
            End If

            If Not BE.ListaSincerado_Det_Pax Is Nothing Then
BorrarSTDetPax:
                For Each ItemDet As clsSincerado_Det_PaxBE In BE.ListaSincerado_Det_Pax
                    If ItemDet.NuSincerado = vstrNuSincerado Then
                        BE.ListaSincerado_Det_Pax.Remove(ItemDet)
                        GoTo BorrarSTDetPax
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsSincerado_DetBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsSincerado_DetBT"
    Private Overloads Function Insertar(ByVal BE As clsSincerado_DetBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Dim intNuSincerado_Det As Integer = 0
        Try
            With dc
                .Add("@NuSincerado", BE.NuSincerado)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@QtPax", BE.QtPax)
                .Add("@QtPaxLiberados", BE.QtPaxLiberado)
                .Add("@SsCostoNeto", BE.SsCostoNeto)
                .Add("@SsCostoIgv", BE.SsCostoIgv)
                .Add("@SsCostoTotal", BE.SsCostoTotal)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuSincerado_Det", 0)
            End With
            intNuSincerado_Det = objADO.ExecuteSPOutput("SINCERADO_DET_Ins", dc)

            ContextUtil.SetComplete()
            Return intNuSincerado_Det
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Private Overloads Sub Actualizar(ByVal BE As clsSincerado_DetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado_Det", BE.NuSincerado_Det)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@QtPaxLiberados", BE.QtPaxLiberado)
            dc.Add("@SsCostoNeto", BE.SsCostoNeto)
            dc.Add("@SsCostoIgv", BE.SsCostoIgv)
            dc.Add("@SsCostoTotal", BE.SsCostoTotal)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("SINCERADO_DET_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintNuSincerado_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim objBTSDPax As New clsSincerado_Det_PaxBT
            objBTSDPax.EliminarxNuSincerado_Det(vintNuSincerado_Det)

            dc.Add("@NuSincerado_Det", vintNuSincerado_Det)

            objADO.ExecuteSP("SINCERADO_DET_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Overloads Sub EliminarxNuSincerado(ByVal vintNuSincerado As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado", vintNuSincerado)

            objADO.ExecuteSP("SINCERADO_DET_DelxNuSincerado", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxNuSincerado")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOperacionBE)
        If BE.ListaSincerado_Det Is Nothing Then Exit Sub
        Try
            For Each ItemDet As clsSincerado_DetBE In BE.ListaSincerado_Det
                If ItemDet.Accion = "N" Then
                    If Not IsNumeric(ItemDet.NuSincerado_Det) Then
                        Dim intNuSincerado_Det As Integer = Insertar(ItemDet)
                        ActualizarIDsNuevosenHijosST(BE, ItemDet.NuSincerado_Det, intNuSincerado_Det)
                    End If
                ElseIf ItemDet.Accion = "M" Then
                    Actualizar(ItemDet)
                ElseIf ItemDet.Accion = "B" Then
                    If IsNumeric(ItemDet.NuSincerado_Det) Then
                        Eliminar(ItemDet.NuSincerado_Det)
                    End If
                End If
            Next

            Dim objBTDetPax As New clsSincerado_Det_PaxBT
            objBTDetPax.InsertarEliminar(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsOperacionBE, ByVal vstrIDAntiguo As String, ByVal vintIDNuevo As Integer)
        Try
            If Not BE.ListaSincerado_Det_Pax Is Nothing Then
                For Each ItemDet As clsSincerado_Det_PaxBE In BE.ListaSincerado_Det_Pax
                    If ItemDet.NuSincerado_Det = vstrIDAntiguo Then
                        ItemDet.NuSincerado_Det = vintIDNuevo
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsSincerado_Det_PaxBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsSincerado_Det_PaxBT"

    Private Overloads Sub Insertar(ByVal BE As clsSincerado_Det_PaxBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado_Det", BE.NuSincerado_Det)
            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("SINCERADO_DET_PAX_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintNuSincerado_Det_Pax As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado_Det_Pax", vintNuSincerado_Det_Pax)

            objADO.ExecuteSP("SINCERADO_DET_PAX_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Overloads Sub EliminarxNuSincerado(ByVal vintNuSincerado As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado", vintNuSincerado)

            objADO.ExecuteSP("SINCERADO_DET_PAX_DelxNuSincerado", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxNuSincerado")
        End Try
    End Sub

    Public Overloads Sub EliminarxNuSincerado_Det(ByVal vintNuSincerado_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuSincerado_Det", vintNuSincerado_Det)

            objADO.ExecuteSP("SINCERADO_DET_PAX_DelxNuSincerado_Det", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxNuSincerado")
        End Try
    End Sub

    Public Overloads Sub InsertarEliminar(ByVal BE As clsOperacionBE)
        Try
            For Each ItemDetPax As clsSincerado_Det_PaxBE In BE.ListaSincerado_Det_Pax
                If ItemDetPax.Accion = "N" Then
                    If Not IsNumeric(ItemDetPax.NuSincerado_Det_Pax) Then
                        Insertar(ItemDetPax)
                    End If
                ElseIf ItemDetPax.Accion = "B" Then
                    If IsNumeric(ItemDetPax.NuSincerado_Det_Pax) Then
                        Eliminar(ItemDetPax.NuSincerado_Det_Pax)
                    End If
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleServiciosOperacionBT
    Inherits clsOperacionBaseBT
    Dim strNombreClase As String = "clsDetalleServiciosOperacionBT"

    Private Overloads Sub Insertar(ByVal BE As clsDetalleServiciosOperacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
            dc.Add("@IDServicio", BE.IDServicio)
            dc.Add("@IDServicio_Det_V_Cot", BE.IDServicio_Det_V)
            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@Total", BE.Total)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargenImpto", BE.STMargenImpto)
            dc.Add("@TotImpto", BE.TotImpto)

            dc.Add("@IDEstadoSolicitud", BE.IDEstadoSolicitud)
            dc.Add("@IDProveedor_Prg", BE.IDProveedor_Prg)
            dc.Add("@Total_PrgEditado", BE.Total_PrgEditado)
            dc.Add("@Activo", BE.Activo)

            dc.Add("@IDMoneda", BE.IDMoneda)
            dc.Add("@TipoCambio", BE.TipoCambio)

            dc.Add("@NetoProgram", BE.NetoProgram)
            dc.Add("@IgvProgram", BE.IgvProgram)
            dc.Add("@TotalProgram", BE.TotalProgram)
            'Cotiz
            dc.Add("@NetoCotiz", BE.NetoCotizado)
            dc.Add("@IgvCotiz", BE.IgvCotizado)
            dc.Add("@TotalCotiz", BE.TotalCotizado)
            dc.Add("@IngManual", BE.IngManual)
            dc.Add("@UserMod", BE.UserMod)

            dc.Add("@FlServicioNoShow", BE.FlServicioNoShow)
            dc.Add("@QtPax", BE.QtPax)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsDetalleServiciosOperacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@Total_Prg", BE.Total_Prg)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargenImpto", BE.STMargenImpto)

            dc.Add("@NetoProgram", BE.NetoProgram)
            dc.Add("@IgvProgram", BE.IgvProgram)
            dc.Add("@TotalProgram", BE.TotalProgram)

            dc.Add("@TotImpto", BE.TotImpto)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@FlServicioNoShow", BE.FlServicioNoShow)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_Upd", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintIDOperacion_Det As Integer, ByVal vintIDServicio_Det As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)

            'objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDOperacionDetIDServicio_Det", dc)
            'objADO.ExecuteSP("ORDEN_SERVICIO_DET_DelxIDOperacionDetIDServicio_Det", dc)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Private Sub ActivarDesactivar(ByVal vintIDOperacion_Det As Integer, _
                                  ByVal vintIDServicio_Det As Integer, _
                                  ByVal blnFlServicioNoShowWhere As Boolean, _
                                  ByVal vblnActivar As Boolean, _
                                  ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@FlServicioNoShowWhere", blnFlServicioNoShowWhere)
            dc.Add("@Activar", vblnActivar)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_ActivarDesactivar", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActivarDesactivar")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOperacionBE)
        Try
            For Each Item As clsDetalleServiciosOperacionBE In BE.ListaDetServiciosOperac
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    ActualizarTotalPrg(Item)
                ElseIf Item.Accion = "Anu" Then
                    ActivarDesactivar(Item.IDOperacion_Det, Item.IDServicio_Det, Item.FlServicioNoShowWhere, False, Item.UserMod)
                ElseIf Item.Accion = "Act" Then
                    ActivarDesactivar(Item.IDOperacion_Det, Item.IDServicio_Det, Item.FlServicioNoShowWhere, True, Item.UserMod)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarTotalPrg(ByVal BE As clsDetalleServiciosOperacionBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With BE
                dc.Add("@IDOperacion_Det", .IDOperacion_Det)
                dc.Add("@IDServicio_Det", .IDServicio_Det)
                dc.Add("@CostoReal", .CostoReal)
                dc.Add("@CostoLiberado", .CostoLiberado)
                dc.Add("@CostoRealImpto", .CostoRealImpto)
                dc.Add("@CostoLiberadoImpto", .CostoLiberadoImpto)
                dc.Add("@Margen", .Margen)
                dc.Add("@MargenAplicado", .MargenAplicado)
                dc.Add("@MargenLiberado", .MargenLiberado)
                dc.Add("@MargenImpto", .MargenImpto)
                dc.Add("@MargenLiberadoImpto", .MargenLiberadoImpto)
                dc.Add("@Total", .Total)
                dc.Add("@Total_PrgEditado", .Total_PrgEditado)
                dc.Add("@TipoCambio", .TipoCambio)
                dc.Add("@IDMoneda", .IDMoneda)
                dc.Add("@Total_Prg", .Total_Prg)
                dc.Add("@IDEstadoSolicitud", .IDEstadoSolicitud)

                dc.Add("@NetoProgram", .NetoProgram)
                dc.Add("@IgvProgram", .IgvProgram)
                dc.Add("@TotalProgram", .TotalProgram)

                dc.Add("@NetoCotizado", BE.NetoCotizado)
                dc.Add("@IgvCotizado", BE.IgvCotizado)
                dc.Add("@TotalCotizado", BE.TotalCotizado)
                dc.Add("@IngManual", BE.IngManual)
                dc.Add("@FlMontosSincerados", BE.FlMontosSincerados)
                dc.Add("@CoEstadoSincerado", BE.CoEstadoSincerado)
                dc.Add("@ObservEdicion", .ObservEdicion)
                dc.Add("@FlServicioNoShow", .FlServicioNoShow)
                dc.Add("@FlServicioNoCobrado", .FlServicioNoCobrado)
                dc.Add("@FlServicioNoShowWhere", .FlServicioNoShowWhere)
                dc.Add("@SSTotalProgramAntNoCobrado", .SSTotalProgramAntNoCobrado)
                dc.Add("@QtPax", .QtPax)
                dc.Add("@UserMod", .UserMod)
            End With

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_UpdTotal", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarTotalPrg")
        End Try
    End Sub

    Public Sub ActualizarFlagSincerado(ByVal vintIDOperacion_Det As Integer, ByVal vintIDServicio_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_UpdFlMontosSincerados", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarTotalPrg")
        End Try
    End Sub

    Public Sub ActualizarEstadoSolicitud(ByVal vListIDOperacion_Det As List(Of Int32), ByVal vintIDServicio_Det As Integer, _
                                          ByVal vstrIDEstadoSolicitud As String, ByVal vintIDServicio_Det_V_Prg As Integer, _
                                          ByVal vstrIDCorreoEstadoAC As String, ByVal vstrIDProveedor_Prg As String, _
                                          ByVal vblnFlServicioNoShow As Boolean, ByVal vstrUserMod As String, _
                                          Optional ByVal strIDTipoProvProgBloque As String = "")
        Try
            Dim dc As Dictionary(Of String, String)
            For Each intIDOperacion_Det As Int32 In vListIDOperacion_Det
                dc = New Dictionary(Of String, String)

                dc.Add("@IDOperacion_Det", intIDOperacion_Det)
                If strIDTipoProvProgBloque = "" Then
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                Else
                    dc.Add("@IDServicio_Det", 0)
                End If

                dc.Add("@IDEstadoSolicitud", vstrIDEstadoSolicitud)
                dc.Add("@IDServicio_Det_V_Prg", vintIDServicio_Det_V_Prg)
                dc.Add("@IDCorreoEstadoAC", vstrIDCorreoEstadoAC)
                dc.Add("@IDProveedor_Prg", vstrIDProveedor_Prg)
                dc.Add("@IDTipoProvProgBloque", strIDTipoProvProgBloque)
                dc.Add("@FlServicioNoShow", vblnFlServicioNoShow)

                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_UpdEstado", dc)
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoSolicitud")
        End Try
    End Sub



    Public Sub ActualizarEstadoProgramacion_Reserva(ByVal vListIDOperacion_Det As List(Of Int32), ByVal vListIDServicio_Det As List(Of Int32), ByVal vstrIDEstadoReserva As String, _
                                         ByVal vstrUserMod As String)
        Try
            Dim dc As Dictionary(Of String, String)

            For i As Integer = 0 To vListIDOperacion_Det.Count - 1
                Dim intIDOperacion_Det As Integer = vListIDOperacion_Det(i)
                Dim intIDServicio_Det As Integer = vListIDServicio_Det(i)
                dc = New Dictionary(Of String, String)

                dc.Add("@IDOperacion_Det", intIDOperacion_Det)
                dc.Add("@IDServicio_Det", intIDServicio_Det)


                dc.Add("@IDEstadoReserva", vstrIDEstadoReserva)

                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("OPERACIONES_DET_Upd_EstadoReserva", dc)
            Next

            'For Each intIDOperacion As Int32 In vListIDOperacion_Det
            '    dc = New Dictionary(Of String, String)

            '    dc.Add("@IDOperacion", intIDOperacion)


            '    dc.Add("@IDEstadoReserva", vstrIDEstadoReserva)

            '    dc.Add("@UserMod", vstrUserMod)

            '    objADO.ExecuteSP("OPERACIONES_Upd_EstadoReserva", dc)
            'Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoSolicitud")
        End Try
    End Sub


    Public Sub ActualizarEstadoProgramacion_Reserva_CorreoBloque(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                          ByVal vblnServicioTC As Boolean, vstrIDEstadoReserva As String, vstrUserMod As String)
        Try
            Dim dc As Dictionary(Of String, String)
            Dim objBN As New clsOperacionBT
            Dim dt As DataTable = objBN.Consultar_List_ServiciosxProvInt_ID(vintIDCab, vstrIDProveedor, vblnServicioTC)

            For i As Integer = 0 To dt.Rows.Count - 1
                Dim intIDOperacion_Det As Integer = dt.Rows(i).Item("IDOperacion_Det")
                Dim intIDServicio_Det As Integer = dt.Rows(i).Item("IDServicio_Det")
                dc = New Dictionary(Of String, String)
                dc.Add("@IDOperacion_Det", intIDOperacion_Det)
                dc.Add("@IDServicio_Det", intIDServicio_Det)
                dc.Add("@IDEstadoReserva", vstrIDEstadoReserva)
                dc.Add("@UserMod", vstrUserMod)
                objADO.ExecuteSP("OPERACIONES_DET_Upd_EstadoReserva", dc)
            Next


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoSolicitud")
        End Try
    End Sub

    Public Sub ActualizarEstadoxFileyOperador(ByVal vintIDCab As Integer, _
                                              ByVal vstrIDProveedorOper As String, _
                                              ByVal vstrIDEstadoSolicitud As String, _
                                              ByVal vstrIDProveedor_Prg As String, _
                                              ByVal vstrIDTipoProvProg As String, _
                                              ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedorOper", vstrIDProveedorOper)
            dc.Add("@IDProveedor_Prg", vstrIDProveedor_Prg)
            dc.Add("@IDTipoProvProg", vstrIDTipoProvProg)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_UpdEstadoPAyProvxIDCabyOperador", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoxFileyOperador")
        End Try
    End Sub

    Public Sub ActualizarEstadoxFileyOperadorTMP(ByVal vintIDCab As Integer, _
                                              ByVal vstrIDProveedorOper As String, _
                                              ByVal vstrIDEstadoSolicitud As String, _
                                              ByVal vstrIDProveedor_Prg As String, _
                                              ByVal vstrIDTipoProvProg As String, _
                                              ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedorOper", vstrIDProveedorOper)
            dc.Add("@IDProveedor_Prg", vstrIDProveedor_Prg)
            dc.Add("@IDTipoProvProg", vstrIDTipoProvProg)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_TMP_UpdEstadoPAyProvxIDCabyOperador", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoxFileyOperadorTMP")
        End Try
    End Sub

    Public Sub ActualizarEstadoxIDReserva_Det(ByVal vintIDReserva_Det As Integer, _
                                          ByVal vstrIDProveedor_Prg As String, _
                                          ByVal vstrIDTipoProvProg As String, _
                                          ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva_Det", vintIDReserva_Det)
            dc.Add("@IDProveedor_Prg", vstrIDProveedor_Prg)
            dc.Add("@IDTipoProvProg", vstrIDTipoProvProg)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_UpdEstadoPAyProvxIDReserva_Det", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoxIDReserva_Det")
        End Try
    End Sub


    Public Sub ActualizarEstadoSolicitudenBloque(ByVal vList As List(Of clsDetalleServiciosOperacionBE), _
                                                 ByVal vstrUserMod As String)
        Try
            For Each Item As clsDetalleServiciosOperacionBE In vList
                Dim ListIDOperacion_Det As New List(Of Int32)
                ListIDOperacion_Det.Add(Item.IDOperacion_Det)
                ActualizarEstadoSolicitud(ListIDOperacion_Det, Item.IDServicio_Det, Item.IDEstadoSolicitud, Item.IDServicio_Det_V_Prg, _
                                          Item.IDCorreoEstadoAC, Item.IDProveedor_Prg, _
                                          Item.FlServicioNoShowWhere, vstrUserMod, "")
                If Item.IDEstadoSolicitud = "NV" Then
                    'EliminarProvAsignado_OrdenServicio(Item.IDOperacion_Det, Item.IDProveedor_Prg_Temp)
                    EliminarDetalleOrdenServicio(Item.IDOperacion_Det, Item.IDServicio_Det, Item.FlServicioNoShow)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub EliminarDetalleOrdenServicio(ByVal vintIDOperancion_Det As Integer, ByVal vintIDServicios_Det As Integer, ByVal vblnNoShow As Boolean)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", vintIDOperancion_Det)
            dc.Add("@IDServicio_Det", vintIDServicios_Det)
            dc.Add("@FlNoShow", vblnNoShow)

            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxPk", dc)
            objADO.ExecuteSP("ORDEN_SERVICIO_DET_DelxPk", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarDetalleOrdenServicio")
        End Try
    End Sub

    Private Sub EliminarProvAsignado_OrdenServicio(ByVal vintIDOperacion_Det As Integer, ByVal vstrIDProveedor As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDOperacion_Det", vintIDOperacion_Det)
            End With

            objADO.ExecuteSP("ORDEN_SERVICIO_DET_ASIGNADOS_DelxIDProveedorxIDOperacionDet", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarProvAsignado_OrdenServicio")
        End Try
    End Sub

    Public Sub ActualizarEstadoSolicitudenBloqueTrasladistas(ByVal vList As List(Of clsDetalleServiciosOperacionBE), _
                                                 ByVal vstrUserMod As String)
        Try
            For Each Item As clsDetalleServiciosOperacionBE In vList
                'Dim ListIDOperacion_Det As New List(Of Int32)
                'ListIDOperacion_Det.Add(Item.IDOperacion_Det)
                'ActualizarEstadoSolicitud(ListIDOperacion_Det, Item.IDServicio_Det, Item.IDEstadoSolicitud, Item.IDServicio_Det_V_Prg, _
                '                          Item.IDCorreoEstadoAC, Item.IDProveedor_Prg, _
                '                          vstrUserMod, "")

                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDOperacion_Det", Item.IDOperacion_Det)
                dc.Add("@IDTrasladista_Prg", Item.IDProveedor_Prg)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("OPERACIONES_DET_Upd_IDTrasladista_Prg", dc)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarEstadoSolicitudenBloqueVehiculos(ByVal vList As List(Of clsDetalleServiciosOperacionBE), _
                                                 ByVal vstrUserMod As String)
        Try
            For Each Item As clsDetalleServiciosOperacionBE In vList
                'Dim ListIDOperacion_Det As New List(Of Int32)
                'ListIDOperacion_Det.Add(Item.IDOperacion_Det)
                'ActualizarEstadoSolicitud(ListIDOperacion_Det, Item.IDServicio_Det, Item.IDEstadoSolicitud, Item.IDServicio_Det_V_Prg, _
                '                          Item.IDCorreoEstadoAC, Item.IDProveedor_Prg, _
                '                          vstrUserMod, "")

                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDOperacion_Det", Item.IDOperacion_Det)
                dc.Add("@IDVehiculo_Prg", Item.IDProveedor_Prg)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("OPERACIONES_DET_Upd_IDVehiculo_Prg", dc)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    'Public Sub ActualizarTotalesPrg(ByVal vList As List(Of clsDetalleServiciosOperacionBE), _
    '                                             ByVal vstrUserMod As String)
    '    Try
    '        For Each Item As clsDetalleServiciosOperacionBE In vList
    '            ActualizarTotalPrg(Item)
    '        Next

    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub

    'Private Function dttCreaDttDetCotiServ() As DataTable
    '    Dim dttDetCotiServ = New DataTable("DetCotiServ")

    '    With dttDetCotiServ
    '        .Columns.Add("IDServicio")
    '        .Columns.Add("IDServicio_Det")
    '        .Columns.Add("CostoReal")
    '        .Columns.Add("Margen")
    '        .Columns.Add("CostoLiberado")
    '        .Columns.Add("MargenLiberado")
    '        .Columns.Add("MargenAplicado")
    '        .Columns.Add("Total")
    '        .Columns.Add("SSCR")
    '        .Columns.Add("SSMargen")
    '        .Columns.Add("SSCL")
    '        .Columns.Add("SSML")
    '        .Columns.Add("SSTotal")
    '        .Columns.Add("STCR")
    '        .Columns.Add("STMargen")
    '        .Columns.Add("STTotal")

    '        .Columns.Add("CostoRealImpto")
    '        .Columns.Add("CostoLiberadoImpto")
    '        .Columns.Add("MargenImpto")
    '        .Columns.Add("MargenLiberadoImpto")
    '        .Columns.Add("SSCRImpto")
    '        .Columns.Add("SSCLImpto")
    '        .Columns.Add("SSMargenImpto")
    '        .Columns.Add("SSMargenLiberadoImpto")
    '        .Columns.Add("STCRImpto")
    '        .Columns.Add("STMargenImpto")
    '        .Columns.Add("TotImpto")

    '    End With

    '    Return dttDetCotiServ
    'End Function

    'Public Sub ActualizarMontos(ByVal vintIDOperacion_Det As Integer, ByVal vstrIDServicio As String, ByVal vintIDServicio_Det As Integer, ByVal vintIDServicio_Det_Prg As Integer, _
    '                            ByVal vintNroPax As Int16, ByVal vintNroLiberados As Int16, ByVal vstrTipo_Lib As String, _
    '                            ByVal vstrVariante As String, ByVal vstrAnio As String, ByVal vstrTipoProv As String, _
    '                            ByVal vstrTipoPax As String, ByVal vstrIDCorreoEstadoAC As String, ByVal vsglIGV As Single, ByVal vstrUserMod As String)
    '    Try


    '        ActualizarEstadoSolicitud(vintIDOperacion_Det, _
    '                            vintIDServicio_Det, _
    '                            "AC", _
    '                            vintIDServicio_Det_Prg, _
    '                            vstrIDCorreoEstadoAC, "", vstrUserMod)


    '        Dim objCotBT As New clsCotizacionBN
    '        Dim dc As Dictionary(Of String, Double) = objCotBT.CalculoCotizacion(vstrIDServicio, vintIDServicio_Det_Prg, _
    '            vstrAnio, vstrVariante, vstrTipoProv, vintNroPax, vintNroLiberados, vstrTipo_Lib, 0, 0, 0, vstrTipoPax, "1.0", Nothing, vsglIGV)

    '        Dim Item As New clsDetalleServiciosOperacionBE

    '        With Item
    '            .IDOperacion_Det = vintIDOperacion_Det
    '            .IDServicio_Det = vintIDServicio_Det

    '            .CostoReal = dblBuscarMontoDictionary(dc, "CostoReal")
    '            .CostoLiberado = dblBuscarMontoDictionary(dc, "CostoLiberado")
    '            .Margen = dblBuscarMontoDictionary(dc, "Margen")
    '            .MargenAplicado = dblBuscarMontoDictionary(dc, "MargenAplicado")
    '            .MargenLiberado = dblBuscarMontoDictionary(dc, "MargenLiberado")
    '            .Total_Prg = dblBuscarMontoDictionary(dc, "Total")
    '            .SSCR = dblBuscarMontoDictionary(dc, "SSCR")
    '            .SSCL = dblBuscarMontoDictionary(dc, "SSCL")
    '            .SSMargen = dblBuscarMontoDictionary(dc, "SSMargen")
    '            .SSMargenLiberado = dblBuscarMontoDictionary(dc, "SSMargenLiberado")
    '            .SSTotal = dblBuscarMontoDictionary(dc, "SSTotal")
    '            .STCR = dblBuscarMontoDictionary(dc, "STCR")
    '            .STMargen = dblBuscarMontoDictionary(dc, "STMargen")
    '            .STTotal = dblBuscarMontoDictionary(dc, "STTotal")
    '            .CostoRealImpto = dblBuscarMontoDictionary(dc, "CostoRealImpto")
    '            .CostoLiberadoImpto = dblBuscarMontoDictionary(dc, "CostoLiberadoImpto")
    '            .MargenImpto = dblBuscarMontoDictionary(dc, "MargenImpto")
    '            .MargenLiberadoImpto = dblBuscarMontoDictionary(dc, "MargenLiberadoImpto")
    '            .SSCRImpto = dblBuscarMontoDictionary(dc, "SSCRImpto")
    '            .SSCLImpto = dblBuscarMontoDictionary(dc, "SSCLImpto")
    '            .SSMargenImpto = dblBuscarMontoDictionary(dc, "SSMargenImpto")
    '            .SSMargenLiberadoImpto = dblBuscarMontoDictionary(dc, "SSMargenLiberadoImpto")
    '            .STCRImpto = dblBuscarMontoDictionary(dc, "STCRImpto")
    '            .STMargenImpto = dblBuscarMontoDictionary(dc, "STMargenImpto")
    '            .TotImpto = dblBuscarMontoDictionary(dc, "TotImpto")

    '            .UserMod = vstrUserMod
    '        End With

    '        Actualizar(Item)


    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub

    Private Function dblBuscarMontoDictionary(ByVal vDc As Dictionary(Of String, Double), ByVal vstrKey As String) As Double
        Try
            For Each dic As KeyValuePair(Of String, Double) In vDc
                If dic.Key = vstrKey Then
                    Return dic.Value
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub ActualizarEstadoSolicitudenBloqueVehiculosDetalle(ByVal vList As List(Of clsDetalleServiciosOperacionBE), _
                                                                 ByVal vListCab As List(Of clsDetalleServiciosOperacionBE), _
                                                ByVal vstrUserMod As String)
        Try
            For Each Item As clsDetalleServiciosOperacionBE In vList
                'Dim ListIDOperacion_Det As New List(Of Int32)
                'ListIDOperacion_Det.Add(Item.IDOperacion_Det)
                'ActualizarEstadoSolicitud(ListIDOperacion_Det, Item.IDServicio_Det, Item.IDEstadoSolicitud, Item.IDServicio_Det_V_Prg, _
                '                          Item.IDCorreoEstadoAC, Item.IDProveedor_Prg, _
                '                          vstrUserMod, "")

                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDOperacion_Det", Item.IDOperacion_Det)
                dc.Add("@IDServicio_Det", Item.IDServicio_Det)
                dc.Add("@IDVehiculo_Prg", Item.IDProveedor_Prg)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_UpdVehiculo", dc)
            Next

            ActualizarEstadoSolicitudenBloqueVehiculos(vListCab, vstrUserMod)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub InsertarxNuevoAcomodoTMP(ByVal vintIDOperacion As Integer, ByVal vintIDOperacion_Det As Integer, ByVal vintIDServicio_Det As Integer, _
                                  ByVal vbytCapacidadHab As Byte, ByVal vblnEsMatrimonial As Boolean, _
                                  ByVal vdatDia As Date, ByVal vstrUserMod As String, ByVal vintIDOperacion_DetTMP As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            'dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)
            dc.Add("@IDOperacion_DetTMP", vintIDOperacion_DetTMP)
            'dc.Add("@IDServicio_Det", vintIDServicio_Det)
            'dc.Add("@CapacidadHab", vbytCapacidadHab)
            'dc.Add("@EsMatrimonial", vblnEsMatrimonial)
            'dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@UserMod", vstrUserMod)


            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_InsxAcomodoTMP", dc)

            ContextUtil.SetComplete()


        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxNuevoAcomodoTMP")
        End Try
    End Sub

    Public Sub ActualizarxNuevoAcomodoTMP(ByVal vintIDOperacion_Det As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOperacion_Det", vintIDOperacion_Det)

            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_UpdxAcomodoTMP", dc)

            ContextUtil.SetComplete()


        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxNuevoAcomodoTMP")
        End Try
    End Sub
End Class