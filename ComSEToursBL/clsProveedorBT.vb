﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
' ''Imports System.Net.Http
' ''Imports System.Net.Http.Headers
' ''Imports System.Text
' ''Imports Newtonsoft.Json
' ''Imports Newtonsoft.Json.Linq

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsProveedorBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Private gstrTipoProvHotel As String = "001"
    Private gstrTipoProveeTransportista As String = "004"
    Private gstrTipoProveeOperadores As String = "003"
    Private gstrTipoProveeRestaurantes As String = "002"

    Public Function Insertar(ByVal BE As clsProveedorBE, ByRef rstrIDProveedor As String) As String
        Dim dc As New Dictionary(Of String, String)
        Dim strIDProveedor As String
        Dim strIDServicio As String = ""

        Try
            With dc
                .Add("@IDTipoProv", BE.IDTipoProv)
                .Add("@IDTipoOper", BE.IDTipoOper)
                .Add("@Persona", BE.Persona)
                .Add("@RazonSocial", BE.RazonSocial)
                .Add("@Nombre", BE.Nombre)
                .Add("@ApPaterno", BE.ApPaterno)
                .Add("@ApMaterno", BE.ApMaterno)
                .Add("@Domiciliado", BE.Domiciliado)
                .Add("@NombreCorto", BE.NombreCorto)
                .Add("@Sigla", BE.Sigla)
                .Add("@EsCadena", BE.EsCadena)
                .Add("@IDProveedor_Cadena", BE.IDProveedor_Cadena)
                .Add("@Direccion", BE.Direccion)
                .Add("@IDCiudad", BE.IDCiudad)
                .Add("@IDIdentidad", BE.IDIdentidad)
                .Add("@NumIdentidad", BE.NumIdentidad)
                .Add("@Telefono1", BE.Telefono1)
                .Add("@Telefono2", BE.Telefono2)
                .Add("@TelefonoReservas1", BE.TelefonoReservas1)
                .Add("@TelefonoReservas2", BE.TelefonoReservas2)
                .Add("@Celular_Claro", BE.Celular_Claro)
                .Add("@Celular_Movistar", BE.Celular_Movistar)
                .Add("@Celular_Nextel", BE.Celular_Nextel)
                .Add("@NomContacto", BE.NomContacto)
                .Add("@Celular", BE.Celular)
                .Add("@Fax", BE.Fax)
                .Add("@FaxReservas1", BE.FaxReservas1)
                .Add("@FaxReservas2", BE.FaxReservas2)
                .Add("@Email1", BE.Email1)
                .Add("@Email2", BE.Email2)
                .Add("@Email3", BE.Email3)
                .Add("@Email4", BE.Email4)
                .Add("@TelefonoRecepcion1", BE.TelefonoRecepcion1)
                .Add("@TelefonoRecepcion2", BE.TelefonoRecepcion2)
                .Add("@FaxRecepcion", BE.FaxRecepcion)
                .Add("@EmailRecepcion1", BE.EmailRecepcion1)
                .Add("@EmailRecepcion2", BE.EmailRecepcion2)
                .Add("@Web", BE.Web)
                .Add("@Adicionales", BE.Adicionales)
                .Add("@PrePago", BE.PrePago)
                .Add("@PlazoDias", BE.PlazoDias)
                .Add("@IDProveedorInternacional", BE.IDProveedorInternacional)
                .Add("@IDFormaPago", BE.IDFormaPago)
                .Add("@TipoMuseo", BE.TipoMuseo)
                .Add("@Guia_Nacionalidad", BE.Guia_Nacionalidad)
                .Add("@Guia_Especialidad", BE.Guia_Especialidad)
                .Add("@Guia_Estado", BE.Guia_Estado)
                .Add("@Guia_Ubica_CV", BE.Guia_Ubica_CV)
                .Add("@Guia_PoliticaMovilidad", BE.Guia_PoliticaMovilidad)
                .Add("@Guia_Asociacion", BE.Guia_Asociacion)
                .Add("@Guia_CarnetApotur", BE.Guia_CarnetApotur)
                .Add("@Guia_CarnetGuiaOficial", BE.Guia_GuiaOficial)
                .Add("@Guia_CertificadoPBIP", BE.Guia_CertificadoPBIP)
                .Add("@Guia_CursoPBIP", BE.Guia_CursoPBIP)
                .Add("@Guia_ExperienciaTC", BE.Guia_ExperienciaTC)
                .Add("@Guia_LugaresTC", BE.Guia_LugaresTC)

                .Add("@DiasCredito", BE.DiasCredito)
                .Add("@EmailContactoAdmin", BE.EmailContactoAdmin)
                .Add("@TelefonoContactoAdmin", BE.TelefonoContactoAdmin)

                .Add("@DocPdfRutaPdf", BE.DocPdfRutaPdf)
                .Add("@Margen", BE.Margen)
                .Add("@IDUsuarioProg", BE.IdUsuarioPrg)

                .Add("@Disponible", BE.Disponible)
                .Add("@DocFoto", BE.DocFoto)
                .Add("@DocCurriculo", BE.DocCurriculo)
                .Add("@RUC", BE.RUC)
                .Add("@FecNacimiento", BE.FecNacimiento)
                .Add("@Movil1", BE.Movil1)
                .Add("@Movil2", BE.Movil2)
                .Add("@OtrosEspecialidad", BE.OtrosEspecialidad)
                .Add("@OtrosNacionalidad", BE.OtrosNacionalidad)
                .Add("@PreferFITS", BE.PreferFITS)
                .Add("@PreferGroups", BE.PreferGroups)
                .Add("@PreferFamilias", BE.PreferFamilias)
                .Add("@Prefer3raEdad", BE.Prefer3raEdad)
                .Add("@PreferOtros", BE.PreferOtros)
                .Add("@EntrBriefing", BE.EntrBriefing)
                .Add("@EntrCultGeneral", BE.EntrCultGeneral)
                .Add("@EntrInfHistorica", BE.EntrInfHistorica)
                .Add("@EntrActitud", BE.EntrActitud)
                .Add("@EntrOtros", BE.EntrOtros)
                .Add("@MeetingPoint", BE.MeetingPoint)
                .Add("@ObservacionPolitica", BE.ObservacionPolitica)
                .Add("@IDUsuarioProgTransp", BE.IDUsuarioProgTransp)
                .Add("@IDUsuarioTrasladista", BE.IDUsuarioTrasladista)
                .Add("@SsMontoCredito", BE.SsMontoCredito)
                .Add("@CoMonedaCredito", BE.CoMonedaCredito)
                .Add("@TxLatitud", BE.TxLatitud)
                .Add("@TxLongitud", BE.TxLongitud)
                .Add("@SSTipoCambio", BE.SSTipoCambio)
                .Add("@CoUbigeo_Oficina", BE.CoUbigeo_Oficina)
                .Add("@FlExcluirSAP", BE.FlExcluirSAP)
                .Add("@CoTarjCredAcept", BE.CoTarjCredAcept)

                .Add("@UserMod", BE.UserMod)
                ''PPMG20151207-->
                .Add("@Usuario", BE.Usuario)
                .Add("@Password", BE.Password)
                .Add("@FlAccesoWeb", BE.FlAccesoWeb)
                ''PPMG20151207<--
                .Add("@pIDProveedor", "")

            End With

            strIDProveedor = objADO.ExecuteSPOutput("MAPROVEEDORES_Ins", dc)
            ActualizarIDNuevosEnListasHijos(BE, BE.IDProveedor, strIDProveedor)

            rstrIDProveedor = strIDProveedor
            BE.IDProveedor = strIDProveedor
            If BE.IDTipoProv = gstrTipoProvHotel Then 'Hoteles: se crea el servicio desde aca
                Dim objBEServ As New clsServicioProveedorBE
                With objBEServ
                    .IDTipoProv = BE.IDTipoProv
                    .IDTipoServ = "NAP"
                    .IDProveedor = BE.IDProveedor
                    .Descripcion = BE.NombreCorto
                    .Descri_tarifario = ""
                    .Capacidad = 0
                    .Categoria = 0
                    .IDCat = "000"
                    .Telefono = BE.Telefono1

                    .IDUbigeo = BE.IDCiudad
                    .AtencionDomingo = True
                    .AtencionLunes = True
                    .AtencionMartes = True
                    .AtencionMiercoles = True
                    .AtencionJueves = True
                    .AtencionViernes = True
                    .AtencionSabado = True

                    .HoraDesde = "01/01/1900"
                    .HoraHasta = "01/01/1900"
                    .HoraCheckOut = "01/01/1900"
                    .HoraCheckIn = "01/01/1900"
                    .DesayunoHoraDesde = "01/01/1900"
                    .DesayunoHoraHasta = "01/01/1900"
                    .PreDesayunoHoraDesde = "01/01/1900"
                    .PreDesayunoHoraHasta = "01/01/1900"
                    .Comision = 0
                    .IDPreDesayuno = "00"
                    '.IDDesayuno = "00"
                    .FecCaducLectura = "01/01/1900"
                    .Celular = ""
                    .Observaciones = ""
                    .Lectura = ""
                    .Activo = True
                    .MotivoDesactivacion = ""
                    '.TelefonoRecepcion1 = ""
                    '.TelefonoRecepcion2 = ""
                    '.FaxRecepcion1 = ""
                    '.FaxRecepcion2 = ""
                    .Tarifario = False
                    .ObservacionesDesayuno = ""
                    .CoTipoPago = ""
                    .APTServicoOpcional = "N"
                    .HoraPredeterminada = "01/01/1900 00:00:00"
                    .UserMod = BE.UserMod
                End With

                Dim objBLServ As New clsServicioProveedorBT
                strIDServicio = objBLServ.Insertar(objBEServ)

            End If

            Dim objBTDet As New clsProveedorBT.clsAdministProveedorBT
            objBTDet.InsertarActualizarEliminar(BE)

            Dim objBTIdio As New clsProveedorBT.clsIdiomaProveedorBT
            objBTIdio.InsertarActualizarEliminar(BE)

            Dim objBTEsp As New clsProveedorBT.clsEspecialidadProveedorBT
            objBTEsp.InsertarEliminar(BE)

            Dim objBTNac As New clsProveedorBT.clsNacionalidadProveedorBT
            objBTNac.InsertarEliminar(BE)

            Dim objBTCom As New clsProveedorBT.clsComentarioProveedorBT
            objBTCom.InsertarActualizarEliminar(BE)

            Dim objBTCor As New clsProveedorBT.clsCorreoReservasProveedorBT
            objBTCor.InsertarActualizarEliminar(BE)

            Dim objBTBib As New clsProveedorBT.clsCorreosBibliaProveedorBT
            objBTBib.InsertarActualizarEliminar(BE)

            Dim objBTPol As New clsProveedorBT.clsPolticasProveedorBT
            objBTPol.InsertarActualizarEliminar(BE)

            Dim objBTAte As New clsProveedorBT.clsAtencionProveedorBT
            objBTAte.InsertarActualizarEliminar(BE)

            Dim objBTDetrac As New clsProveedorBT.clsProveedores_TipoDetraccionBT
            objBTDetrac.Eliminar(strIDProveedor)
            objBTDetrac.InsertarActualizarEliminar(BE)

            Dim objBTEnt As New clsProveedorBT.clsEntrevistaProveedorBT
            objBTEnt.InsertarActualizar(BE)

            Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
            PasarDatosProveedoresToSocioNegocio(strIDProveedor, BE.UserMod, oBESocioNegocio)
            'EnviarDatosRESTFul(oBESocioNegocio, "POST")

            If Not oBESocioNegocio Is Nothing And BE.FlEnviaraSAP Then
                Dim oBTJsonMaster As New clsJsonInterfaceBT()
                'oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                '                                     "http://sap:8899/api/BusinessPartners/", _
                '                                     "POST")
                oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                                                    "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
                                                    "POST")
                Actualizar_CardCodeSAP(BE.IDProveedor, oBESocioNegocio.CardCode)
            End If

            ContextUtil.SetComplete()
            Return strIDServicio
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Function drConsultarDatosProveedorSAP(ByVal vstrIDProveedor As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSocioNegocio", vstrIDProveedor)
            dc.Add("@TipoSocioNegocio", "P")

            Return objADO.GetDataTable("SAP_ListarSociosNegocio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtDatosContactos(ByVal vstrIDSocioNegocio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSocioNegocio", vstrIDSocioNegocio)
            dc.Add("@TipoSocio", "P")

            Return objADO.GetDataTable("SAP_ListarSociosNegocio_ContactEmployees", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtDatosDetracciones(ByVal vstrIDSocioNegocio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSocioNegocio", vstrIDSocioNegocio)

            Return objADO.GetDataTable("SAP_ListarSociosNegocio_BPWithholdingTax", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub PasarDatosProveedoresToSocioNegocio(ByVal vstrIDProveedor As String, ByVal vstrUserMod As String, _
                                                ByRef oBESocioNegocioBE As clsSocioNegocioBE)
        Try
            Dim dtDatos As DataTable = drConsultarDatosProveedorSAP(vstrIDProveedor)
            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBESocioNegocioBE = New clsSocioNegocioBE()
                With oBESocioNegocioBE
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .CardType = dr("CardType")
                    .GroupCode = dr("GroupCode")
                    .FederalTaxID = dr("FederalTaxID")
                    .Currency = dr("Currency")
                    .U_SYP_BPTP = dr("U_SYP_BPTP")
                    .U_SYP_BPTD = dr("U_SYP_BPTD")
                    .U_SYP_BPAP = dr("U_SYP_BPAP")
                    .U_SYP_BPAM = dr("U_SYP_BPAM")
                    .U_SYP_BPN2 = dr("U_SYP_BPN2")
                    .U_SYP_BPNO = dr("U_SYP_BPNO")
                    .EmailAddress = dr("EmailAddress")
                    .Phone1 = dr("Phone1")
                    .Phone2 = dr("Phone2")
                    .SubjectToWithholdingTax = dr("SubjectToWithholdingTax")
                    .Valid = dr("Valid")

                    Dim BPBAddress As New List(Of clsBPAddressesBE)
                    Dim oBPBAddress As New clsBPAddressesBE
                    With oBPBAddress
                        .AddressName = dr("AddressName")
                        .Street = dr("Street")
                        .ZipCode = dr("ZipCode")
                        .Block = dr("Block")
                        .AddressType = dr("AddressType")
                    End With
                    BPBAddress.Add(oBPBAddress)
                    .BPAddresses = BPBAddress

                    Dim ContactEmployees As New List(Of clsContactEmployeesBE)
                    Dim dDatosContact As DataTable = dtDatosContactos(vstrIDProveedor) 'dtDatosContactos(.CardCode)
                    For Each Item As DataRow In dDatosContact.Rows
                        Dim oContactEmployee As New clsContactEmployeesBE
                        With oContactEmployee
                            .Name = Item("Name")
                            .FirstName = Item("FirstName")
                            .LastName = Item("LastName")
                            .Address = Item("Address")
                            .E_Mail = Item("E_mail")
                            .Active = Item("Active")
                        End With
                        ContactEmployees.Add(oContactEmployee)
                    Next
                    .ContactEmployees = ContactEmployees


                    'detracciones
                    Dim BPWithholdingTax As New List(Of clsBPWithholdingTaxBE)
                    Dim dBPWithholdingTax As DataTable = dtDatosDetracciones(vstrIDProveedor)
                    For Each Item As DataRow In dBPWithholdingTax.Rows
                        Dim oBPWithholdingTax As New clsBPWithholdingTaxBE
                        With oBPWithholdingTax
                            .WTCode = Item("WTCode")

                        End With
                        BPWithholdingTax.Add(oBPWithholdingTax)
                    Next
                    .BPWithholdingTax = BPWithholdingTax
                End With
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    '' ''Private Sub EnviarDatosRESTFul(ByVal oBESocioNegocioBE As clsSocioNegocioBE, ByVal vstrVerbo As String) 'As Threading.Tasks.Task
    '' ''    If oBESocioNegocioBE Is Nothing Then Exit Sub
    '' ''    If vstrVerbo.Trim = String.Empty Then Exit Sub
    '' ''    Dim strMessageError As String = String.Empty
    '' ''    Try
    '' ''        Dim strDataValueJSON = JsonConvert.SerializeObject(oBESocioNegocioBE, Formatting.Indented)
    '' ''        'DropPropertyJSON(strDataValueJSON)

    '' ''        Dim strUriHostRestFul As String = "http://sap:8899/"
    '' ''        Dim strUriAPI As String = String.Empty
    '' ''        Select Case vstrVerbo
    '' ''            Case "POST"
    '' ''                strUriAPI &= "api/BusinessPartners"
    '' ''            Case "PUT"
    '' ''                strUriAPI &= "api/BusinessPartners/" & oBESocioNegocioBE.CardCode
    '' ''        End Select

    '' ''        If strUriHostRestFul <> String.Empty And strUriAPI <> String.Empty Then
    '' ''            Using clientConnect As New HttpClient()
    '' ''                With clientConnect
    '' ''                    Try
    '' ''                        .BaseAddress = New Uri(strUriHostRestFul)
    '' ''                        .DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))
    '' ''                    Catch ex As Exception
    '' ''                        strMessageError = "SAP: Error al direccionar el grabado de proveedor."
    '' ''                        Throw
    '' ''                    End Try

    '' ''                    If strMessageError = String.Empty Then
    '' ''                        Dim myContent = New StringContent(content:=strDataValueJSON, encoding:=Encoding.UTF8, mediaType:="application/json")
    '' ''                        Try
    '' ''                            Dim response As New System.Net.Http.HttpResponseMessage
    '' ''                            If vstrVerbo = "POST" Then
    '' ''                                response = .PostAsync(strUriAPI, myContent).Result
    '' ''                            ElseIf vstrVerbo = "PUT" Then
    '' ''                                response = .PutAsync(strUriAPI, myContent).Result
    '' ''                            End If

    '' ''                            If response.IsSuccessStatusCode Then
    '' ''                                Dim strJSONFormat As String = response.Content.ReadAsStringAsync().Result().ToString()
    '' ''                                Dim BEResponseStatus As ResponseStatusSAP = Nothing
    '' ''                                Dim jss = New System.Web.Script.Serialization.JavaScriptSerializer()
    '' ''                                Dim data = jss.Deserialize(Of Object)(strJSONFormat)
    '' ''                                Dim blnErrorInesperado As Boolean = False
    '' ''                                Try
    '' ''                                    Dim dicValorResponse As Dictionary(Of String, Object) = DirectCast(data, Dictionary(Of String, Object))
    '' ''                                    If dicValorResponse.Count > 0 Then
    '' ''                                        Dim dcValores As Object() = dicValorResponse.Item("ResponseStatusSAP")
    '' ''                                        Dim objInterno As Object = dcValores

    '' ''                                        BEResponseStatus = New ResponseStatusSAP()
    '' ''                                        BEResponseStatus.ErrCode = CInt(objInterno(0)("ErrCode"))
    '' ''                                        BEResponseStatus.ErrMsg = objInterno(0)("ErrMsg")
    '' ''                                        BEResponseStatus.Id = String.Empty
    '' ''                                        If IsDBNull(objInterno(0)("Id")) = False Then
    '' ''                                            If Not objInterno(0)("Id") Is Nothing Then
    '' ''                                                BEResponseStatus.Id = objInterno(0)("Id").ToString()
    '' ''                                            End If
    '' ''                                        End If

    '' ''                                    End If

    '' ''                                    If Not BEResponseStatus Is Nothing Then
    '' ''                                        If BEResponseStatus.ErrCode <> 0 Then
    '' ''                                            blnErrorInesperado = True
    '' ''                                        End If
    '' ''                                    End If
    '' ''                                Catch ex As Exception
    '' ''                                    blnErrorInesperado = True
    '' ''                                End Try

    '' ''                                If blnErrorInesperado Then
    '' ''                                    strMessageError &= "SAP: Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''                                    If BEResponseStatus.ErrMsg <> String.Empty Then
    '' ''                                        strMessageError = "SAP: " & BEResponseStatus.ErrMsg
    '' ''                                    End If
    '' ''                                    Throw New Exception
    '' ''                                End If
    '' ''                            Else
    '' ''                                strMessageError &= "SAP: "
    '' ''                                Select Case response.StatusCode
    '' ''                                    Case Net.HttpStatusCode.BadRequest
    '' ''                                        Dim BEResponseStatus As ResponseStatusSAP = Nothing
    '' ''                                        Dim strJSONFormat As String = response.Content.ReadAsStringAsync().Result().ToString()
    '' ''                                        Dim jss = New System.Web.Script.Serialization.JavaScriptSerializer()
    '' ''                                        Dim data = jss.Deserialize(Of Object)(strJSONFormat)
    '' ''                                        Dim blnErrorInesperado As Boolean = False

    '' ''                                        Try
    '' ''                                            Dim dicValorResponse As Dictionary(Of String, Object) = DirectCast(data, Dictionary(Of String, Object))
    '' ''                                            If dicValorResponse.Count > 0 Then
    '' ''                                                Dim dcErrorModel As Dictionary(Of String, Object) = dicValorResponse.Item("ModelState")
    '' ''                                                For Each ItemDic As KeyValuePair(Of String, Object) In dcErrorModel
    '' ''                                                    Dim strKey As String = ItemDic.Key
    '' ''                                                    Dim valObj As Object() = ItemDic.Value

    '' ''                                                    Dim na As Integer = 0
    '' ''                                                    strMessageError &= vbCrLf & "Error: " & Replace(strKey, "oBP.", String.Empty) & vbCrLf & _
    '' ''                                                                        valObj(0).ToString()
    '' ''                                                Next
    '' ''                                            End If

    '' ''                                            If Not BEResponseStatus Is Nothing Then
    '' ''                                                If BEResponseStatus.ErrCode <> 0 Then
    '' ''                                                    blnErrorInesperado = True
    '' ''                                                End If
    '' ''                                            End If
    '' ''                                        Catch ex As Exception
    '' ''                                            blnErrorInesperado = True
    '' ''                                        End Try

    '' ''                                        If blnErrorInesperado Then
    '' ''                                            strMessageError &= "SAP: Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''                                            If BEResponseStatus.ErrMsg <> String.Empty Then
    '' ''                                                strMessageError = "SAP: " & BEResponseStatus.ErrMsg
    '' ''                                            End If
    '' ''                                            Throw New Exception
    '' ''                                        End If
    '' ''                                    Case Else
    '' ''                                        strMessageError &= "Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''                                        Throw New Exception
    '' ''                                End Select
    '' ''                                Throw New Exception
    '' ''                            End If
    '' ''                        Catch ex As Exception
    '' ''                            ContextUtil.SetAbort()
    '' ''                            Throw
    '' ''                        End Try
    '' ''                    End If
    '' ''                End With
    '' ''            End Using
    '' ''        End If

    '' ''        ContextUtil.SetComplete()
    '' ''    Catch ex As Exception
    '' ''        ContextUtil.SetAbort()
    '' ''        If strMessageError = String.Empty Then
    '' ''            strMessageError = "Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''        End If
    '' ''        Throw New ApplicationException(strMessageError)
    '' ''    End Try
    '' ''End Sub

    Private Sub ActualizarIDNuevosEnListasHijos(ByRef BE As clsProveedorBE, ByVal vstrIDAntiguo As String, _
                                                ByVal vstrIDNuevo As String)
        Try
            If Not BE.ListaCorreosBiblia Is Nothing Then
                For Each Item As clsProveedorBE.clsCorreosBibliaBE In BE.ListaCorreosBiblia
                    If Item.IDProveedor = vstrIDAntiguo Then
                        Item.IDProveedor = vstrIDNuevo
                    End If
                Next
            End If

            If Not BE.ListaEntrevistaProveedor Is Nothing Then
                For Each Item As clsProveedorBE.clsEntrevistaProveedorBE In BE.ListaEntrevistaProveedor
                    Item.IDProveedor = vstrIDNuevo
                Next
            End If

            If Not BE.ListaDetracciones Is Nothing Then
                For Each Item As clsProveedorBE.clsProveedores_TipoDetraccionBE In BE.ListaDetracciones
                    Item.IDProveedor = vstrIDNuevo
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub Actualizar(ByVal BE As clsProveedorBE)
        Dim dc As New Dictionary(Of String, String)

        Try
            With dc
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@Persona", BE.Persona)
                .Add("@RazonSocial", BE.RazonSocial)
                .Add("@Nombre", BE.Nombre)
                .Add("@ApPaterno", BE.ApPaterno)
                .Add("@ApMaterno", BE.ApMaterno)
                .Add("@Domiciliado", BE.Domiciliado)
                .Add("@NombreCorto", BE.NombreCorto)
                .Add("@Sigla", BE.Sigla)
                .Add("@EsCadena", BE.EsCadena)
                .Add("@IDProveedor_Cadena", BE.IDProveedor_Cadena)
                .Add("@Direccion", BE.Direccion)
                .Add("@IDCiudad", BE.IDCiudad)
                .Add("@IDIdentidad", BE.IDIdentidad)
                .Add("@NumIdentidad", BE.NumIdentidad)

                .Add("@Telefono1", BE.Telefono1)
                .Add("@Telefono2", BE.Telefono2)
                .Add("@TelefonoReservas1", BE.TelefonoReservas1)
                .Add("@TelefonoReservas2", BE.TelefonoReservas2)

                .Add("@Celular_Claro", BE.Celular_Claro)
                .Add("@Celular_Movistar", BE.Celular_Movistar)
                .Add("@Celular_Nextel", BE.Celular_Nextel)
                .Add("@NomContacto", BE.NomContacto)
                .Add("@Celular", BE.Celular)
                .Add("@Fax", BE.Fax)
                .Add("@FaxReservas1", BE.FaxReservas1)
                .Add("@FaxReservas2", BE.FaxReservas2)

                .Add("@Email1", BE.Email1)
                .Add("@Email2", BE.Email2)
                .Add("@Email3", BE.Email3)
                .Add("@Email4", BE.Email4)

                .Add("@TelefonoRecepcion1", BE.TelefonoRecepcion1)
                .Add("@TelefonoRecepcion2", BE.TelefonoRecepcion2)
                .Add("@FaxRecepcion", BE.FaxRecepcion)
                .Add("@EmailRecepcion1", BE.EmailRecepcion1)
                .Add("@EmailRecepcion2", BE.EmailRecepcion2)

                .Add("@Web", BE.Web)
                .Add("@Adicionales", BE.Adicionales)
                .Add("@PrePago", BE.PrePago)
                .Add("@PlazoDias", BE.PlazoDias)

                .Add("@IDProveedorInternacional", BE.IDProveedorInternacional)
                .Add("@IDFormaPago", BE.IDFormaPago)
                .Add("@TipoMuseo", BE.TipoMuseo)

                .Add("@Guia_Nacionalidad", BE.Guia_Nacionalidad)
                .Add("@Guia_Especialidad", BE.Guia_Especialidad)
                .Add("@Guia_Estado", BE.Guia_Estado)
                .Add("@Guia_Ubica_CV", BE.Guia_Ubica_CV)
                .Add("@Guia_PoliticaMovilidad", BE.Guia_PoliticaMovilidad)
                .Add("@Guia_Asociacion", BE.Guia_Asociacion)
                .Add("@Guia_CarnetApotur", BE.Guia_CarnetApotur)
                .Add("@Guia_CarnetGuiaOficial", BE.Guia_GuiaOficial)
                .Add("@Guia_CertificadoPBIP", BE.Guia_CertificadoPBIP)
                .Add("@Guia_CursoPBIP", BE.Guia_CursoPBIP)
                .Add("@Guia_ExperienciaTC", BE.Guia_ExperienciaTC)
                .Add("@Guia_LugaresTC", BE.Guia_LugaresTC)

                .Add("@DiasCredito", BE.DiasCredito)
                .Add("@EmailContactoAdmin", BE.EmailContactoAdmin)
                .Add("@TelefonoContactoAdmin", BE.TelefonoContactoAdmin)

                .Add("@DocPdfRutaPdf", BE.DocPdfRutaPdf)
                .Add("@Margen", BE.Margen)
                .Add("@IDUsuarioProg", BE.IdUsuarioPrg)

                .Add("@Disponible", BE.Disponible)
                .Add("@DocFoto", BE.DocFoto)
                .Add("@DocCurriculo", BE.DocCurriculo)
                .Add("@RUC", BE.RUC)
                .Add("@FecNacimiento", BE.FecNacimiento)
                .Add("@Movil1", BE.Movil1)
                .Add("@Movil2", BE.Movil2)
                .Add("@OtrosEspecialidad", BE.OtrosEspecialidad)
                .Add("@OtrosNacionalidad", BE.OtrosNacionalidad)
                .Add("@PreferFITS", BE.PreferFITS)
                .Add("@PreferGroups", BE.PreferGroups)
                .Add("@PreferFamilias", BE.PreferFamilias)
                .Add("@Prefer3raEdad", BE.Prefer3raEdad)
                .Add("@PreferOtros", BE.PreferOtros)
                .Add("@EntrBriefing", BE.EntrBriefing)
                .Add("@EntrCultGeneral", BE.EntrCultGeneral)
                .Add("@EntrInfHistorica", BE.EntrInfHistorica)
                .Add("@EntrActitud", BE.EntrActitud)
                .Add("@EntrOtros", BE.EntrOtros)
                .Add("@MeetingPoint", BE.MeetingPoint)
                .Add("@ObservacionPolitica", BE.ObservacionPolitica)
                .Add("@IDUsuarioProgTransp", BE.IDUsuarioProgTransp)
                .Add("@IDUsuarioTrasladista", BE.IDUsuarioTrasladista)
                .Add("@SsMontoCredito", BE.SsMontoCredito)
                .Add("@CoMonedaCredito", BE.CoMonedaCredito)
                .Add("@TxLatitud", BE.TxLatitud)
                .Add("@TxLongitud", BE.TxLongitud)
                .Add("@SSTipoCambio", BE.SSTipoCambio)
                .Add("@CoUbigeo_Oficina", BE.CoUbigeo_Oficina)
                .Add("@FlExcluirSAP", BE.FlExcluirSAP)
                .Add("@CoTarjCredAcept", BE.CoTarjCredAcept)
                .Add("@UserMod", BE.UserMod)
                ''PPMG20151207-->
                .Add("@Usuario", BE.Usuario)
                .Add("@Password", BE.Password)
                .Add("@FlAccesoWeb", BE.FlAccesoWeb)
                ''PPMG20151207<--

            End With

            objADO.ExecuteSP("MAPROVEEDORES_Upd", dc)

            If BE.IDTipoProv = gstrTipoProveeOperadores Or BE.IDTipoProv = gstrTipoProveeTransportista Or BE.IDTipoProv = gstrTipoProveeRestaurantes Then
                Dim objServ As New clsServicioProveedorBT
                objServ.ActualizarMargenxIDProveedor(BE.IDProveedor, BE.Margen, BE.UserMod)
            End If

            Dim objBTDet As New clsProveedorBT.clsAdministProveedorBT
            objBTDet.InsertarActualizarEliminar(BE)

            Dim objBTIdio As New clsProveedorBT.clsIdiomaProveedorBT
            objBTIdio.InsertarActualizarEliminar(BE)

            Dim objBTEsp As New clsProveedorBT.clsEspecialidadProveedorBT
            objBTEsp.InsertarEliminar(BE)

            Dim objBTNac As New clsProveedorBT.clsNacionalidadProveedorBT
            objBTNac.InsertarEliminar(BE)

            Dim objBTCom As New clsProveedorBT.clsComentarioProveedorBT
            objBTCom.InsertarActualizarEliminar(BE)

            Dim objBTCor As New clsProveedorBT.clsCorreoReservasProveedorBT
            objBTCor.InsertarActualizarEliminar(BE)

            Dim objBTBib As New clsProveedorBT.clsCorreosBibliaProveedorBT
            objBTBib.InsertarActualizarEliminar(BE)

            Dim objBTPol As New clsProveedorBT.clsPolticasProveedorBT
            objBTPol.InsertarActualizarEliminar(BE)

            Dim objBTAte As New clsProveedorBT.clsAtencionProveedorBT
            objBTAte.InsertarActualizarEliminar(BE)

            Dim objBTEnt As New clsProveedorBT.clsEntrevistaProveedorBT
            objBTEnt.InsertarActualizar(BE)

            Dim objBTDetrac As New clsProveedorBT.clsProveedores_TipoDetraccionBT
            objBTDetrac.Eliminar(BE.IDProveedor)
            objBTDetrac.InsertarActualizarEliminar(BE)

            Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
            PasarDatosProveedoresToSocioNegocio(BE.IDProveedor, BE.UserMod, oBESocioNegocio)
            'EnviarDatosRESTFul(oBESocioNegocio, "POST")

            If Not oBESocioNegocio Is Nothing And BE.FlEnviaraSAP Then
                Dim oBTJsonMaster As New clsJsonInterfaceBT()
                'oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                '                                     "http://sap:8899/api/BusinessPartners/", _
                '                                     "POST")
                oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                                                   "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
                                                   "POST")

                Actualizar_CardCodeSAP(BE.IDProveedor, oBESocioNegocio.CardCode)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Actualizar_Extranet(ByVal BE As clsProveedorBE)
        'Dim dc As New Dictionary(Of String, String)
        'Try
        '    With dc
        '        .Add("@IDProveedor", BE.IDProveedor)

        '        .Add("@UsuarioLogeo", BE.Usuario)
        '        .Add("@PasswordLogeo", BE.Password)
        '        .Add("@UsuarioLogeo_Hash", BE.UsuarioLogeo_Hash)

        '        .Add("@UserMod", BE.UserMod)

        '    End With

        '    objADO.ExecuteSP("MAPROVEEDORES_Upd_Extranet", dc)

        '    Dim objMenBT As New clsCorreoFileBT
        '    ' ''objMenBT.EnviarCorreoSQL_Contactos("SETRA", BE.Usuario, "", BE.UsuarioLogeo_Hash, "TEXT", "", "G")

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    Throw
        'End Try
    End Sub

    Public Sub Actualizar_Extranet_Pass(ByVal strPassword As String, ByVal strUsuarioLogeo_Hash As String, ByVal strUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@PasswordLogeo", strPassword)
                .Add("@UsuarioLogeo_Hash", strUsuarioLogeo_Hash)
                .Add("@UserMod", strUserMod)
            End With

            objADO.ExecuteSP("MAPROVEEDORES_Upd_Extranet_Pass", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Actualizar_Extranet_Restablecer_Pass(ByVal BE As clsProveedorBE)
        'Dim dc As New Dictionary(Of String, String)
        'Try
        '    With dc
        '        .Add("@IDProveedor", BE.IDProveedor)

        '        .Add("@UsuarioLogeo_Hash", BE.UsuarioLogeo_Hash)
        '        .Add("@UserMod", BE.UserMod)

        '    End With

        '    objADO.ExecuteSP("MAPROVEEDORES_Upd_Extranet_Restablecer_Pass", dc)

        '    Dim objMenBT As New clsCorreoFileBT
        '    ' ''objMenBT.EnviarCorreoSQL_Contactos("SETRA", BE.Usuario, "", BE.UsuarioLogeo_Hash, "TEXT", "", "R")

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    Throw
        'End Try
    End Sub

    Public Sub Actualizar_CardCodeSAP(ByVal strIDProveedor As String, ByVal strCardCodeSAP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDProveedor", strIDProveedor)
                .Add("@CardCodeSAP", strCardCodeSAP)
            End With

            objADO.ExecuteSP("MAPROVEEDORES_Upd_CardCodeSAP", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Anular(ByVal vstrIDProveedor As String, ByVal vstrUserMod As String)
        Try
            AnularSETRA(vstrIDProveedor, vstrUserMod)
            AnularSAP(vstrIDProveedor, vstrUserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub AnularSETRA(ByVal vstrIDProveedor As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("MAPROVEEDORES_Anu", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub AnularSAP(ByVal vstrIDProveedor As String, ByVal vstrUserMod As String)
        Try
            Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
            PasarDatosProveedoresToSocioNegocio(vstrIDProveedor, vstrUserMod, oBESocioNegocio)
            'EnviarDatosRESTFul(oBESocioNegocio, "POST")

            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
            '                                     "http://sap:8899/api/BusinessPartners/", _
            '                                     "POST")
            oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                                                "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
                                                "POST")

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    'RANGOS APT
    '-------------------------------------------------------------------

    <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsRangosAPTProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            Dim dc As Dictionary(Of String, String)
            Try

                For Each Item As clsProveedorBE.clsRangosAPTProveedorBE In BE.ListaRangos

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        If Item.Accion = "M" Then _
                        dc.Add("@Correlativo", Item.Correlativo)
                        dc.Add("@NuAnio", Item.NuAnio)
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@PaxDesde", Item.PaxDesde)
                        dc.Add("@PaxHasta", Item.PaxHasta)
                        dc.Add("@Monto", Item.Monto)
                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MAPROVEEDORES_RANGO_APT_Ins", dc)
                            'InsertarHijosxIDServicio_Det_V(BE.IDProveedor, Item.PaxDesde, _
                            '                               Item.PaxHasta, Item.Monto, Item.UserMod)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MAPROVEEDORES_RANGO_APT_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDServicio_Det", BE.IDProveedor)
                        dc.Add("@Correlativo", Item.Correlativo)
                        dc.Add("@NuAnio", Item.NuAnio)
                        objADO.ExecuteSP("MAPROVEEDORES_RANGO_APT_Del", dc)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub


        Public Function ConsultarListxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                Return objADO.GetDataTable("MAPROVEEDORES_RANGO_APT_Sel_xIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDProveedor_Anio(ByVal vstrIDProveedor As String, ByVal vstrNuAnio As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@NuAnio", vstrNuAnio)
                Return objADO.GetDataTable("MAPROVEEDORES_RANGO_APT_Sel_xIDProveedor_Anio", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub EliminarxIDProveedor(ByVal vstrIDProveedor As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                objADO.ExecuteSP("MAPROVEEDORES_RANGO_APT_DelxIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub EliminarxIDProveedor_Anio(ByVal vstrIDProveedor As String, ByVal vstrNuAnio As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@NuAnio", vstrNuAnio)
                objADO.ExecuteSP("MAPROVEEDORES_RANGO_APT_DelxIDProveedor_Anio", dc)
            Catch ex As Exception
                Throw
            End Try
        End Sub


        Public Function dblConsultarCosto(ByVal vstrIDProveedor As String, ByVal vstrNuAnio As String, ByVal vintNroPax As Int16) As Double
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", vstrIDProveedor)
                    .Add("@NuAnio", vstrNuAnio)
                    .Add("@NroPax", vintNroPax)
                    .Add("@pMonto", 0)
                End With

                Return objADO.ExecuteSPOutput("MAPROVEEDORES_RANGO_APT_SelCostoOutput", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class


    '-------------------------------------------------------------------
    'FIN RANGOS APT


    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsAtencionProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Private Sub Insertar(ByVal BE As clsProveedorBE.clsAtencionProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDAtencion", BE.IDAtencion)
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@Dia1", BE.Dia1)
                    .Add("@Dia2", BE.Dia2)
                    .Add("@Desde1", BE.Desde1)
                    .Add("@Hasta1", BE.Hasta1)
                    .Add("@Desde2", BE.Desde2)
                    .Add("@Hasta2", BE.Hasta2)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MAATENCION_MAPROVEEDOR_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Actualizar(ByVal BE As clsProveedorBE.clsAtencionProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDAtencion", BE.IDAtencion)
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@Dia1", BE.Dia1)
                    .Add("@Dia2", BE.Dia2)
                    .Add("@Desde1", BE.Desde1)
                    .Add("@Hasta1", BE.Hasta1)
                    .Add("@Desde2", BE.Desde2)
                    .Add("@Hasta2", BE.Hasta2)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MAATENCION_MAPROVEEDOR_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal vbytIDAtencion As Byte, ByVal vstrIDProveedor As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDAtencion", vbytIDAtencion)
                    .Add("@IDProveedor", vstrIDProveedor)
                End With
                objADO.ExecuteSP("MAATENCION_MAPROVEEDOR_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            If BE.ListaAtencionProveedor Is Nothing Then Exit Sub
            Try
                For Each Item As clsProveedorBE.clsAtencionProveedorBE In BE.ListaAtencionProveedor
                    If Item.Accion = "N" Then
                        Item.IDProveedor = BE.IDProveedor
                        Insertar(Item)
                    ElseIf Item.Accion = "M" Then
                        Actualizar(Item)
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDAtencion, Item.IDProveedor)
                    End If
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsAdministProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            Try
                Dim dc As Dictionary(Of String, String)

                For Each Item As clsProveedorBE.clsAdministProveedorBE In BE.ListaAdministProveedor

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        dc.Add("@ApellidosTitular", Item.ApellidosTitular)
                        dc.Add("@BancoExtranjero", Item.BancoExtranjero)
                        dc.Add("@Banco", Item.Banco)
                        dc.Add("@BancoInterm", Item.BancoInterm)
                        dc.Add("@CtaCte", Item.CtaCte)
                        dc.Add("@CtaInter", Item.CtaInter)
                        'If Item.Accion = "M" Then dc.Add("@IDAdminist", Item.IDAdminist)
                        dc.Add("@IDAdminist", Item.IDAdminist)
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@IDMoneda", Item.IDMoneda)
                        dc.Add("@IVAN", Item.IVAN)
                        dc.Add("@NombresTitular", Item.NombresTitular)
                        dc.Add("@SWIFT", Item.SWIFT)
                        dc.Add("@ReferenciaAdminist", Item.ReferenciaAdminist)

                        dc.Add("@BancoInterExtranjero", Item.BancoInterExtranjero)
                        dc.Add("@TipoCuenta", Item.TipoCuenta)

                        dc.Add("@TitularBenef", Item.TitularBenef)
                        dc.Add("@TipoCuentBenef", Item.TipoCuentBenef)
                        dc.Add("@NroCuentaBenef", Item.NroCuentaBenef)
                        dc.Add("@DireccionBenef", Item.DireccionBenef)

                        dc.Add("@NombreBancoPag", Item.NombreBancoPag)
                        dc.Add("@PECBancoPag", Item.PECBancoPag)
                        dc.Add("@DireccionBancoPag", Item.DireccionBancoPag)
                        dc.Add("@SWIFTBancoPag", Item.SWIFTBancoPag)

                        dc.Add("@EsOpcBancoInte", Item.EsOpcBancoInte)
                        dc.Add("@NombreBancoInte", Item.NombreBancoInte)
                        dc.Add("@PECBancoInte", Item.PECBancoInte)
                        dc.Add("@SWIFTBancoInte", Item.SWIFTBancoInte)
                        dc.Add("@CuentBncPagBancoInte", Item.CuentBncPagBancoInte)
                        dc.Add("@CoCBU", Item.CoCBU)

                        dc.Add("@UserMod", Item.UserMod)
                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MAADMINISTPROVEEDORES_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MAADMINISTPROVEEDORES_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@IDAdminist", Item.IDAdminist)
                        objADO.ExecuteSP("MAADMINISTPROVEEDORES_Del", dc)
                    End If

                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsEntrevistaProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Overloads Sub Insertar(ByVal BE As clsProveedorBE.clsEntrevistaProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@FlConocimiento", BE.FlConocimiento)
                    .Add("@FeEntrevita", BE.FeEntrevista)
                    .Add("@IDUserEntre", BE.IDUserEntre)
                    .Add("@TxComentario", BE.TxComentario)
                    .Add("@Estado", BE.Estado)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MAENTREVISTAPROVEEDOR_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Overloads Sub Actualizar(ByVal BE As clsProveedorBE.clsEntrevistaProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@FlConocimiento", BE.FlConocimiento)
                    .Add("@FeEntrevita", BE.FeEntrevista)
                    .Add("@IDUserEntre", BE.IDUserEntre)
                    .Add("@TxComentario", BE.TxComentario)
                    .Add("@Estado", BE.Estado)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MAENTREVISTAPROVEEDOR_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Overloads Sub InsertarActualizar(ByVal BE As clsProveedorBE)
            If BE.ListaEntrevistaProveedor Is Nothing Then Exit Sub
            Try
                For Each Item As clsProveedorBE.clsEntrevistaProveedorBE In BE.ListaEntrevistaProveedor
                    If Not blnExisteProveedor(Item.IDProveedor, Item.IDIdioma) Then
                        Insertar(Item)
                    Else
                        Actualizar(Item)
                    End If
                Next
            Catch ex As Exception
                ContextUtil.SetAbort()
            End Try
        End Sub

        Private Function blnExisteProveedor(ByVal vstrIDProveedor As String, ByVal vstrIDIdioma As String) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@IDIdioma", vstrIDIdioma)
                dc.Add("@pExisteIdioma", False)

                Return objADO.GetSPOutputValue("MAENTREVISTAPROVEEDOR_Sel_Exists", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsPolticasProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            Try
                Dim dc As Dictionary(Of String, String)

                For Each Item As clsProveedorBE.clsPoliticaProveedoresBE In BE.ListaPoliticasProveedores
                    dc = New Dictionary(Of String, String)

                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        dc.Add("@IDProveedor", BE.IDProveedor)

                        dc.Add("@Nombre", Item.NombrePolitica)
                        dc.Add("@Anio", Item.Anio)
                        dc.Add("@DefinDia", Item.DefinDia)
                        dc.Add("@DefinHasta", Item.DefinHasta)
                        dc.Add("@DefinTipo", Item.DefinTipo)

                        dc.Add("@RoomPreeliminar", Item.RoomPreeliminar)
                        dc.Add("@RoomActualizado", Item.RoomActualizado)
                        dc.Add("@RoomFinal", Item.RoomFinal)
                        dc.Add("@DiasSinPenalidad", Item.DiasSinPenalidad)
                        dc.Add("@PoliticaFechaReserva", Item.PoliticaFechaReserva)

                        dc.Add("@PrePag1_Dias", Item.PrePag1_Dias)
                        dc.Add("@PrePag1_Porc", Item.PrePag1_Porc)
                        dc.Add("@PrePag1_Mont", Item.PrePag1_Mont)

                        dc.Add("@PrePag2_Dias", Item.PrePag2_Dias)
                        dc.Add("@PrePag2_Porc", Item.PrePag2_Porc)
                        dc.Add("@PrePag2_Mont", Item.PrePag2_Mont)

                        dc.Add("@Saldo_Dias", Item.Saldo_Dias)
                        dc.Add("@Saldo_Porc", Item.Saldo_Porc)
                        dc.Add("@Saldo_Mont", Item.Saldo_Mont)
                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MAPOLITICAPROVEEDORES_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            dc.Add("@IDPolitica", Item.IDPolitica)
                            objADO.ExecuteSP("MAPOLITICAPROVEEDORES_Upd", dc)
                        End If

                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDPolitica", Item.IDPolitica)
                        dc.Add("@IDProveedor", BE.IDProveedor)

                        objADO.ExecuteSP("MAPOLITICAPROVEEDORES_Del", dc)
                    End If
                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsContactoProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub Insertar(ByVal BE As clsProveedorBE.clsContactoProveedorBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDProveedor", BE.IDProveedor)

                    .Add("@IDIdentidad", BE.IDIdentidad)
                    .Add("@NumIdentidad", BE.NumIdentidad)
                    .Add("@Titulo", BE.Titulo)
                    .Add("@Nombres", BE.Nombres)
                    .Add("@Apellidos", BE.Apellidos)
                    .Add("@Cargo", BE.Cargo)
                    .Add("@Utilidad", BE.Utilidad)

                    .Add("@Telefono", BE.Telefono)
                    .Add("@Fax", BE.Fax)
                    .Add("@Celular", BE.Celular)
                    .Add("@Email", BE.Email)
                    .Add("@Anexo", BE.Anexo)
                    .Add("@FechaNacimiento", FormatDateTime(BE.FechaNacimiento, DateFormat.ShortDate))
                    .Add("@UserMod", BE.UserMod)

                    .Add("@Usuario", BE.Usuario)
                    .Add("@Password", BE.Password)
                    .Add("@FlAccesoWeb", BE.FlAccesoWeb)
                End With

                objADO.ExecuteSP("MACONTACTOSPROVEEDOR_Ins", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsProveedorBE.clsContactoProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDContacto", BE.IDContacto)

                    .Add("@IDIdentidad", BE.IDIdentidad)
                    .Add("@NumIdentidad", BE.NumIdentidad)
                    .Add("@Titulo", BE.Titulo)
                    .Add("@Nombres", BE.Nombres)
                    .Add("@Apellidos", BE.Apellidos)
                    .Add("@Cargo", BE.Cargo)
                    .Add("@Utilidad", BE.Utilidad)

                    .Add("@Telefono", BE.Telefono)
                    .Add("@Fax", BE.Fax)
                    .Add("@Celular", BE.Celular)
                    .Add("@Email", BE.Email)
                    .Add("@Anexo", BE.Anexo)
                    .Add("@FechaNacimiento", BE.FechaNacimiento)
                    .Add("@UserMod", BE.UserMod)

                    .Add("@Usuario", BE.Usuario)
                    .Add("@Password", BE.Password)
                    .Add("@FlAccesoWeb", BE.FlAccesoWeb)
                End With

                objADO.ExecuteSP("MACONTACTOSPROVEEDOR_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar_Extranet(ByVal BE As clsProveedorBE.clsContactoProveedorBE)
            'Dim dc As New Dictionary(Of String, String)
            'Try
            '    With dc
            '        .Add("@IDContacto", BE.IDContacto)
            '        .Add("@IDProveedor", BE.IDProveedor)

            '        .Add("@UsuarioLogeo", BE.Usuario)
            '        .Add("@PasswordLogeo", BE.Password)
            '        .Add("@UsuarioLogeo_Hash", BE.UsuarioLogeo_Hash)

            '        .Add("@UserMod", BE.UserMod)

            '    End With

            '    objADO.ExecuteSP("MACONTACTOSPROVEEDOR_Upd_Extranet", dc)

            '    Dim objMenBT As New clsCorreoFileBT
            '    ' ''objMenBT.EnviarCorreoSQL_Contactos("SETRA", BE.Usuario, "", BE.UsuarioLogeo_Hash, "TEXT", "", "G")

            '    ContextUtil.SetComplete()
            'Catch ex As Exception
            '    ContextUtil.SetAbort()
            '    Throw
            'End Try
        End Sub

        Public Sub Actualizar_Extranet_Pass(ByVal strPassword As String, ByVal strUsuarioLogeo_Hash As String, ByVal strUserMod As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc

                    .Add("@PasswordLogeo", strPassword)
                    .Add("@UsuarioLogeo_Hash", strUsuarioLogeo_Hash)
                    .Add("@UserMod", strUserMod)
                End With

                objADO.ExecuteSP("MACONTACTOSPROVEEDOR_Upd_Extranet_Pass", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar_Extranet_Restablecer_Pass(ByVal BE As clsProveedorBE.clsContactoProveedorBE)
            'Dim dc As New Dictionary(Of String, String)
            'Try
            '    With dc
            '        .Add("@IDContacto", BE.IDContacto)
            '        .Add("@IDProveedor", BE.IDProveedor)

            '        .Add("@UsuarioLogeo_Hash", BE.UsuarioLogeo_Hash)
            '        .Add("@UserMod", BE.UserMod)

            '    End With

            '    objADO.ExecuteSP("MACONTACTOSPROVEEDOR_Upd_Extranet_Restablecer_Pass", dc)

            '    Dim objMenBT As New clsCorreoFileBT
            '    ''objMenBT.EnviarCorreoSQL_Contactos("SETRA", BE.Usuario, "", BE.UsuarioLogeo_Hash, "TEXT", "", "R")

            '    ContextUtil.SetComplete()
            'Catch ex As Exception
            '    ContextUtil.SetAbort()
            '    Throw
            'End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDContacto As String, ByVal vstrIDProveedor As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDContacto", vstrIDContacto)
                    .Add("@IDProveedor", vstrIDProveedor)

                End With

                objADO.ExecuteSP("MACONTACTOSPROVEEDOR_Del", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsIdiomaProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            Try
                Dim dc As Dictionary(Of String, String)

                For Each Item As clsProveedorBE.clsIdiomaProveedorBE In BE.ListaIdiomasProveedor

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@IDIdioma", Item.IDIdioma)
                        dc.Add("@Nivel", Item.Nivel)
                        dc.Add("@UserMod", Item.UserMod)
                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MAIDIOMASPROVEEDOR_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MAIDIOMASPROVEEDOR_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@IDIdioma", Item.IDIdioma)
                        objADO.ExecuteSP("MAIDIOMASPROVEEDOR_Del", dc)
                    End If

                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub


        Public Sub Insertar(ByVal BE As clsProveedorBE.clsIdiomaProveedorBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@Nivel", BE.Nivel)

                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MAIDIOMASPROVEEDOR_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsProveedorBE.clsIdiomaProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@Nivel", BE.Nivel)

                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MAIDIOMASPROVEEDOR_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDIdioma As String, ByVal vstrIDProveedor As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIdioma", vstrIDIdioma)
                    .Add("@IDProveedor", vstrIDProveedor)

                End With

                objADO.ExecuteSP("MAIDIOMASPROVEEDOR_Del", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsEspecialidadProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Sub Insertar(ByVal BE As clsProveedorBE.clsEspecialidadProveedorBE)
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@IDEspecialidad", BE.IDEspecialidad)
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@UserMod", BE.UserMod)
                End With

                Dim dcVal As New Dictionary(Of String, String)
                With dcVal
                    .Add("@IDEspecialidad", BE.IDEspecialidad)
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@pblnExiste", False)
                End With
                Dim blnExiste As Boolean = objADO.GetSPOutputValue("MAESPECIALIDADPROVEEDOR_SelOutput", dcVal)

                If Not blnExiste Then
                    objADO.ExecuteSP("MAESPECIALIDADPROVEEDOR_Ins", dc)
                End If

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal BE As clsProveedorBE.clsEspecialidadProveedorBE)
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@IDEspecialidad", BE.IDEspecialidad)
                    .Add("@IDProveedor", BE.IDProveedor)
                End With

                objADO.ExecuteSP("MAESPECIALIDADPROVEEDOR_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub AsignarIDProveedor(ByVal BE As clsProveedorBE, ByVal vstrIDProveedor As String)
            Try
                For Each Item As clsProveedorBE.clsEspecialidadProveedorBE In BE.ListaEspecialidadProveedor
                    If Item.Accion <> "" Then
                        Item.IDProveedor = vstrIDProveedor
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub InsertarEliminar(ByVal BE As clsProveedorBE)
            Try
                AsignarIDProveedor(BE, BE.IDProveedor)

                For Each Item As clsProveedorBE.clsEspecialidadProveedorBE In BE.ListaEspecialidadProveedor
                    If Item.Accion = "B" Then
                        Eliminar(Item)
                    ElseIf Item.Accion = "N" Then
                        Insertar(Item)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsNacionalidadProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Sub Insertar(ByVal BE As clsProveedorBE.clsNacionalidadProveedorBE)
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@IDubigeo", BE.IDUbigeo)
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@UserMod", BE.UserMod)
                End With

                Dim dcVal As New Dictionary(Of String, String)
                With dcVal
                    .Add("@IDubigeo", BE.IDUbigeo)
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@pblnExiste", False)
                End With
                Dim blnExiste As Boolean = objADO.GetSPOutputValue("MANACIONALIDADPROVEEDOR_SelOutput", dcVal)

                If Not blnExiste Then
                    objADO.ExecuteSP("MANACIONALIDADPROVEEDOR_Ins", dc)
                End If

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal BE As clsProveedorBE.clsNacionalidadProveedorBE)
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@IDubigeo", BE.IDUbigeo)
                    .Add("@IDProveedor", BE.IDProveedor)
                End With

                objADO.ExecuteSP("MANACIONALIDADPROVEEDOR_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub AsignarIDProveedor(ByVal BE As clsProveedorBE, ByVal vstrIDProveedor As String)
            Try
                For Each Item As clsProveedorBE.clsNacionalidadProveedorBE In BE.ListaNacionalidadProveedor
                    If Item.Accion <> "" Then
                        Item.IDProveedor = vstrIDProveedor
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub InsertarEliminar(ByVal BE As clsProveedorBE)
            Try
                AsignarIDProveedor(BE, BE.IDProveedor)

                For Each Item As clsProveedorBE.clsNacionalidadProveedorBE In BE.ListaNacionalidadProveedor
                    If Item.Accion = "B" Then
                        Eliminar(Item)
                    ElseIf Item.Accion = "N" Then
                        Insertar(Item)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsComentarioProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            Try
                Dim dc As Dictionary(Of String, String)

                For Each Item As clsProveedorBE.clsComentarioProveedorBE In BE.ListaComentProveedor

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@IDComentario", Item.IDComentario)
                        dc.Add("@IDFile", Item.IDFile)
                        dc.Add("@Comentario", Item.Comentario)
                        dc.Add("@Estado", Item.Estado)
                        dc.Add("@UserMod", Item.UserMod)
                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MACOMENTARIOSPROVEEDOR_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MACOMENTARIOSPROVEEDOR_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@IDComentario", Item.IDComentario)
                        objADO.ExecuteSP("MACOMENTARIOSPROVEEDOR_Del", dc)
                    End If

                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Insertar(ByVal BE As clsProveedorBE.clsComentarioProveedorBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDFile", BE.IDFile)
                    .Add("@Comentario", BE.Comentario)
                    .Add("@Estado", BE.Estado)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MACOMENTARIOSPROVEEDOR_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsProveedorBE.clsComentarioProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDComentario", BE.IDComentario)
                    .Add("@IDProveedor", BE.IDProveedor)
                    .Add("@IDFile", BE.IDFile)
                    .Add("@Comentario", BE.Comentario)
                    .Add("@Estado", BE.Estado)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MACOMENTARIOSPROVEEDOR_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDComentario As String, ByVal vstrIDProveedor As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDComentario", vstrIDComentario)
                    .Add("@IDProveedor", vstrIDProveedor)

                End With

                objADO.ExecuteSP("MACOMENTARIOSPROVEEDOR_Del", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCorreosBibliaProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            Try
                For Each Item As clsProveedorBE.clsCorreosBibliaBE In BE.ListaCorreosBiblia
                    If Item.Accion = "N" Then
                        If Not blnExisteCorreo(Item.IDProveedor, Item.Correo) Then
                            Insertar(Item)
                        End If
                    ElseIf Item.Accion = "M" Then
                        Actualizar(Item)
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDProveedor, Item.Correo)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Private Sub Insertar(ByVal BE As clsProveedorBE.clsCorreosBibliaBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", BE.IDProveedor)
                dc.Add("@Correo", BE.Correo)
                dc.Add("@Descripcion", BE.Descripcion)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MACORREOSBIBLIAPROVEEDOR_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Private Sub Actualizar(ByVal BE As clsProveedorBE.clsCorreosBibliaBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", BE.IDProveedor)
                dc.Add("@Correo", BE.Correo)
                dc.Add("@Descripcion", BE.Descripcion)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MACORREOSBIBLIAPROVEEDOR_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Private Sub Eliminar(ByVal vstrIDProveedor As String, ByVal vstrCorreo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Correo", vstrCorreo)

                objADO.ExecuteSP("MACORREOSBIBLIAPROVEEDOR_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Private Function blnExisteCorreo(ByVal vstrIDProveedor As String, ByVal vstrCorreo As String) As Boolean
            Try
                Dim blnExiste As Boolean = False
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Correo", vstrCorreo)
                dc.Add("@pExists", False)

                blnExiste = objADO.GetSPOutputValue("MACORREOSBIBLIAPROVEEDOR_Sel_ExistsPK", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCorreoReservasProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)

            Try

                Dim dc As New Dictionary(Of String, String)

                For Each Item As clsProveedorBE.clsCorreosReservasBE In BE.ListaCorreoReservadosProveedor

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "N" Then
                        If Not blnExiste(BE.IDProveedor, Item.Correo) Then
                            dc.Add("@IDProveedor", BE.IDProveedor)
                            dc.Add("@Descripcion", Item.Descripcion)
                            dc.Add("@Correo", Item.Correo)
                            dc.Add("@UserMod", Item.UserMod)

                            objADO.ExecuteSP("MACORREOREVPROVEEDOR_Ins", dc)
                        End If
                    ElseIf Item.Accion = "M" Then

                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@Descripcion", Item.Descripcion)
                        dc.Add("@Correo", Item.Correo)
                        dc.Add("@UserMod", Item.UserMod)
                        objADO.ExecuteSP("MACORREOREVPROVEEDOR_Upd", dc)

                    ElseIf Item.Accion = "B" Then
                        '    dc.Add("@IDProveedor", BE.IDProveedor)
                        '    dc.Add("@Correo", Item.Correo)
                        '    dc.Add("@UserMod", Item.UserMod)
                        '    objADO.ExecuteSP("MACORREOREVPROVEEDOR_Del", dc)
                        'Else
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@Correo", Item.Correo)
                        objADO.ExecuteSP("MACORREOREVPROVEEDOR_Del", dc)
                    End If

                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function blnExiste(ByVal vstrIDProveedor As String, ByVal vstrCorreo As String) As Boolean
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Correo", vstrCorreo)
                dc.Add("@pbExists", False)

                Return objADO.ExecuteSPOutput("MACORREOREVPROVEEDOR_SelExistexPK", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class


    <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsProveedores_TipoDetraccionBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsProveedorBE)
            Try
                Dim dc As Dictionary(Of String, String)

                For Each Item As clsProveedorBE.clsProveedores_TipoDetraccionBE In BE.ListaDetracciones

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        dc.Add("@IDProveedor", BE.IDProveedor)
                        dc.Add("@CoTipoDetraccion", Item.CoTipoDetraccion)

                        dc.Add("@UserMod", Item.UserMod)
                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MAPROVEEDORES_MATIPODETRACCION_Ins", dc)
                            'ElseIf Item.Accion = "M" Then
                            '    objADO.ExecuteSP("MAADMINISTPROVEEDORES_Upd", dc)

                        End If
                        'ElseIf Item.Accion = "B" Then
                        '    dc.Add("@IDProveedor", BE.IDProveedor)
                        '    dc.Add("@IDAdminist", Item.IDAdminist)
                        '    objADO.ExecuteSP("MAADMINISTPROVEEDORES_Del", dc)
                    End If

                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDProveedor As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", vstrIDProveedor)
                End With
                objADO.ExecuteSP("MAPROVEEDORES_MATIPODETRACCION_Del_Prov", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
End Class
