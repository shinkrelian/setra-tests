﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports System.Configuration
Imports System.Collections.Specialized
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public MustInherit Class clsAdministracionBaseBT
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDT

    Public Overridable Sub Insertar()

    End Sub

    Public Overridable Sub Actualizar()

    End Sub

    Public Overridable Sub Eliminar()

    End Sub

    Public Overridable Sub InsertarActualizarEliminar()

    End Sub

    Public Overridable Sub ActualizarIDsNuevosenHijosST()

    End Sub

    Public Overridable Sub EliminarIDsHijosST()

    End Sub

    Public Overridable Function ConsultarparaSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String) As DataTable
        Return Nothing
    End Function

    Public Overridable Sub PasarDatosToSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, _
                                             ByRef robjBEInvoicesBE As clsInvoicesBE)

    End Sub
    Public Overridable Sub PasarDatosAnulacionToSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, _
                                             ByRef robjBEInvoicesBE As clsInvoicesBE)

    End Sub
    Public Overridable Sub EnviarSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, ByVal vstrUserMod As String, ByVal vstrVerboController As String)

    End Sub
    Public Overridable Sub ActualizarCoSAP(ByVal vstrCoTipoDoc As String, ByVal vstrNuDocum As String, ByVal vstrCoSAP As String, ByVal vstrUserMod As String)

    End Sub
End Class

''<ComVisible(True)> _
''<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
'Public Class clsAdministracionBTTest
'    Inherits clsAdministracionBaseBT

'    Public Function strCadena() As String
'        Return "Ok"
'    End Function
'End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsAdministracionBT
    Inherits clsAdministracionBaseBT

    Public Sub ActualizarGeneracionEntMPWPPaxINC(ByVal vListaPaxMP As List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE), _
                                                 ByVal vListaPaxWP As List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE), _
                                                 ByVal vstrUserMod As String, _
                                                 Optional ByVal vListaPaxRQ As List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE) = Nothing)
        Try
            If Not vListaPaxMP Is Nothing Then
                For Each ItemMP As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In vListaPaxMP
                    ActualizarGeneracionEntMP(ItemMP, vstrUserMod)
                Next
            End If
            If Not vListaPaxWP Is Nothing Then
                For Each ItemWP As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In vListaPaxWP
                    ActualizarGeneracionEntWP(ItemWP, vstrUserMod)
                Next
            End If
            If Not vListaPaxRQ Is Nothing Then
                For Each ItemRQ As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In vListaPaxRQ
                    ActualizarGeneracionEntRQ(ItemRQ, vstrUserMod)
                Next
            End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarGeneracionEntMP(ByVal BE As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@FlEntMPGenerada", True)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIDET_PAX_UpdFlEntMPGenerada", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarGeneracionEntWP(ByVal BE As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@FlEntWPGenerada", True)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIDET_PAX_UpdFlEntWPGenerada", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarGeneracionEntRQ(ByVal BE As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@FlEntRQGenerada", True)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIDET_PAX_UpdFlEntRQGenerada", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class



<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsIngresoFinanzasBT
    Inherits clsAdministracionBaseBT

    Public Overloads Function Insertar(ByVal BE As clsIngresoFinanzasBE) As Integer
        Dim intIDIngresoFinanza As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@FeAcreditada", BE.FeAcreditada)
                .Add("@FePresentacion", BE.FePresentacion)
                .Add("@IDBanco", BE.IDBanco)
                .Add("@IDFormaPago", BE.IDFormaPago)
                .Add("@CoOperacionBan", BE.CoOperacionBan)
                .Add("@IDCliente", BE.IDCliente)
                .Add("@IDPais", BE.IDPais)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@NoRutaDocumento", BE.NoRutaDocumento)
                .Add("@TxObservacion", BE.TxObservacion)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@SsTc", BE.ssTC)
                .Add("@SsOrdenado", BE.SsOrdenado)
                .Add("@SsGastosTransferencia", BE.SsGastosTransferencia)
                .Add("@SsRecibido", BE.SsRecibido)
                .Add("@SsBanco", BE.SsBanco)
                .Add("@SsNeto", BE.SsNeto)
                .Add("@NoBancoOrdendante", BE.NoBancoOrdendante)
                .Add("@NoBancoCorresponsal1", BE.NoBancoCorresponsal1)
                .Add("@NoBancoCorresponsal2", BE.NoBancoCorresponsal2)
                .Add("@IDUbigeoBancoOrde", BE.IDUbigeoBancoOrde)
                .Add("@IDUbigeoBancoCor1", BE.IDUbigeoBancoCor1)
                .Add("@IDUbigeoBancoCor2", BE.IDUbigeoBancoCor2)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuIngreso", 0)
            End With
            intIDIngresoFinanza = objADO.ExecuteSPOutput("INGRESO_FINANZAS_Ins", dc)

            ContextUtil.SetComplete()
            Return intIDIngresoFinanza
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Overloads Sub Actualizar(ByVal BE As clsIngresoFinanzasBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuIngreso", BE.NuIngreso)
                .Add("@FeAcreditada", BE.FeAcreditada)
                .Add("@FePresentacion", BE.FePresentacion)
                .Add("@IDBanco", BE.IDBanco)
                .Add("@IDFormaPago", BE.IDFormaPago)
                .Add("@CoOperacionBan", BE.CoOperacionBan)
                .Add("@IDCliente", BE.IDCliente)
                .Add("@IDPais", BE.IDPais)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@NoRutaDocumento", BE.NoRutaDocumento)
                .Add("@TxObservacion", BE.TxObservacion)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@SsTc", BE.ssTC)
                .Add("@SsOrdenado", BE.SsOrdenado)
                .Add("@SsGastosTransferencia", BE.SsGastosTransferencia)
                .Add("@SsRecibido", BE.SsRecibido)
                .Add("@SsBanco", BE.SsBanco)
                .Add("@SsNeto", BE.SsNeto)
                .Add("@NoBancoOrdendante", BE.NoBancoOrdendante)
                .Add("@NoBancoCorresponsal1", BE.NoBancoCorresponsal1)
                .Add("@NoBancoCorresponsal2", BE.NoBancoCorresponsal2)
                .Add("@IDUbigeoBancoOrde", BE.IDUbigeoBancoOrde)
                .Add("@IDUbigeoBancoCor1", BE.IDUbigeoBancoCor1)
                .Add("@IDUbigeoBancoCor2", BE.IDUbigeoBancoCor2)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("INGRESO_FINANZAS_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuIngreso As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)

            objADO.ExecuteSP("INGRESO_FINANZAS_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarxAsignacion(ByVal BE As clsIngresoFinanzasBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuIngreso", BE.NuIngreso)
                .Add("@TxObservacion", BE.TxObservacion)
                .Add("@SsOrdenado", BE.SsOrdenado)
                .Add("@SsGastosTransferencia", BE.SsGastosTransferencia)
                .Add("@SsRecibido", BE.SsRecibido)
                .Add("@SsBanco", BE.SsBanco)
                .Add("@SsNeto", BE.SsNeto)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("INGRESO_FINANZAS_UpdxAsignacion", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsAsignacionBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Dim strNombreClase As String = "clsAsignacionBT"

    Public Overloads Function Insertar(ByVal BE As clsAsignacionBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim intNuAsignacion As Integer = 0
            With dc
                .Add("@TxObservacion", BE.TxObservacion)
                .Add("@SsImportePagado", BE.SsImportePagado)
                .Add("@pNuAsignacion", 0)
                .Add("@UserMod", BE.UserMod)
            End With

            intNuAsignacion = objADO.ExecuteSPOutput("ASIGNACION_Ins", dc)
            ActualizarIDsNuevosenHijosST(BE, 0, intNuAsignacion)

            ActualizarEstadoIngreso(BE)

            Dim objBTDebitMemo As New clsDebitMemoBT
            objBTDebitMemo.InsertarActualizarEliminar(BE)

            Dim objBTAsignados As New clsIngreso_DebitMemoBT
            objBTAsignados.InsertarActualizarEliminar(BE)
            ''''''''''


            'InsertarBoletasAnticipoxAsignacion(intNuAsignacion, BE.UserMod)
            ''''''''''''''''
            ActualizarMatchIngresosDocumentos(BE.NuIngreso, intNuAsignacion, BE.UserMod)

            'For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
            '    EnviarDatosCobranzasSAP(BE.NuIngreso, intNuAsignacion, Item.IDDebitMemo)
            'Next



            ContextUtil.SetComplete()
            Return intNuAsignacion
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function
    Public Function InsertarconNCred(ByVal BE As clsAsignacionBE, ByVal vstrNuDebitMemo As String, ByRef rstrError As String, _
                                     ByRef rListaDocsAntic As List(Of clsDocumentoBE), ByVal vchrAccionAsignacion As Char) As Integer
        Dim intNuAsignacion As Integer = 0
        Try

            Dim strError As String = ""
            Dim ListDMemos As New List(Of String)
            Dim ListDMemosAntic As New List(Of String)
            Dim dblTotal As Double = 0
            Dim dblGastosTranfComisionBco As Double = 0
            'Dim ListaDocsAntic As New List(Of String)
            For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
                If Item.Accion = "N" Then
                    Dim strNuDocumAntic As String = "", strCoTipoDocAntic As String = "" ', dttAnticipos As DataTable = Nothing, strCoSAP As String = ""
                    intNuAsignacion = intInsertarxDMemo(BE, intNuAsignacion, Item.IDDebitMemo, strError, vchrAccionAsignacion, Item.Accion, _
                                                               strCoTipoDocAntic, strNuDocumAntic, "", "", 0, "", "", 0, "01/01/1900", "", False)

                    If strError = "" Then
                        If Not ListDMemos.Contains(Item.IDDebitMemo) Then
                            ListDMemos.Add(Item.IDDebitMemo)
                        End If
                        If strNuDocumAntic <> "" Then
                            If Not ListDMemosAntic.Contains(Item.IDDebitMemo) Then
                                ListDMemosAntic.Add(Item.IDDebitMemo)
                            End If
                            'ListaDocsAntic.Add(strCoTipoDocAntic & " " & strNuDocumAntic)
                        End If

                        dblTotal += Item.SsPago
                        dblGastosTranfComisionBco += (Item.SsGastosTransferencia + Item.SsComisionBanco)
                    End If
                End If
            Next

            For Each strDM As String In ListDMemosAntic
                Dim strCoTipoDoc As String = "", strNuDocum As String = "", strCoSAP2 As String = "", _
                    dblSSTotalDoc As Double = 0, strCoMonedaDoc As String = "", strCoTipoVtaDoc As String = "", _
                    strCoClienteDoc As String = "", intIDCab As Integer = 0, datFeDocum As Date = "01/01/1900", strIDDebitMemo As String = strDM
                ConsultarDocumentosxDebitMemoAnticipo(strIDDebitMemo, strCoTipoDoc, strNuDocum, strCoSAP2, _
                                                          strCoMonedaDoc, dblSSTotalDoc, strCoTipoVtaDoc, strCoClienteDoc, intIDCab, datFeDocum)
                Dim NewDoc As New clsDocumentoBE With {.NuDocum = strNuDocum, .IDTipoDoc = strCoTipoDoc, .IDMoneda = strCoMonedaDoc, _
                                                       .SsTotalDocumUSD = dblSSTotalDoc, .CoTipoVenta = strCoTipoVtaDoc, .IDCliente = strCoClienteDoc, _
                                                       .IDCab = intIDCab, .FeDocum = datFeDocum, .CoDebitMemoAnticipo = strIDDebitMemo, .CoSAP = strCoSAP2}
                rListaDocsAntic.Add(NewDoc)
            Next


            Dim strCoSAP As String = EnviarDatosCobranzasSAP(BE.NuIngreso, intNuAsignacion, vstrNuDebitMemo, "N", "", False, ListDMemos, ListDMemosAntic, dblTotal, dblGastosTranfComisionBco)
            ActualizarCoSAP(BE.NuIngreso, vstrNuDebitMemo, strCoSAP, BE.UserMod)

            ContextUtil.SetComplete()
            Return intNuAsignacion
        Catch ex As Exception
            ContextUtil.SetAbort()
            rstrError = ex.Message
            Return intNuAsignacion
        End Try
    End Function
    Public Function intInsertarxDMemo(ByVal BE As clsAsignacionBE, ByVal vintNuAsignacion As Integer, ByVal vstrIDDebitMemo As String, _
                                      ByRef rstrError As String, ByVal vchrAccionAsignacion As Char, ByVal vchrAccionDMemo As Char, _
                                      ByRef rstrCoTipoDocAnticipo As String, ByRef rstrNuDocumAnticipo As String, ByRef rstrCoSAPDocumento As String, _
                                      ByRef rstrCoMoneda As String, ByRef rdblSSTotal As Double, _
                                      ByRef rstrCoTipoVta As String, ByRef rstrCoCliente As String, ByRef rintIDCab As Integer, ByRef rdatFeDocum As Date, _
                                      ByVal vstrCoSAP As String, ByVal vblnSAP As Boolean) As Integer

        Dim dc As New Dictionary(Of String, String)

        Try

            If vchrAccionAsignacion = "N" Then
                If vintNuAsignacion = 0 Then
                    With dc
                        .Add("@TxObservacion", BE.TxObservacion)
                        .Add("@SsImportePagado", 0)
                        .Add("@pNuAsignacion", 0)
                        .Add("@UserMod", BE.UserMod)
                    End With
                    vintNuAsignacion = objADO.ExecuteSPOutput("ASIGNACION_Ins", dc)

                End If
                ActualizarIDsNuevosenHijosST(BE, 0, vintNuAsignacion)
            ElseIf vchrAccionAsignacion = "E" Then 'Or vchrAccionIngreso = "B" Then
                With dc
                    .Add("@NuAsignacion", BE.NuAsignacion)
                    .Add("@TxObservacion", BE.TxObservacion)
                    '.Add("@SsImportePagado", BE.SsImportePagado)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("ASIGNACION_Upd2", dc)
                vintNuAsignacion = BE.NuAsignacion
            End If

            Dim objBTDebitMemo As New clsDebitMemoBT
            For Each ItemDM As clsDebitMemoBE In BE.ListaDebitMemo
                If ItemDM.IDDebitMemo = vstrIDDebitMemo Then
                    objBTDebitMemo.Actualizar(ItemDM)
                End If
            Next

            Dim objBTAsignados As New clsIngreso_DebitMemoBT
            objBTAsignados.InsertarActualizarEliminar(BE, vstrIDDebitMemo)

            If vchrAccionDMemo = "N" Then
                Dim blnSeCreoAnticipos As Boolean = InsertarBoletasAnticipoxAsignacion(vintNuAsignacion, vstrIDDebitMemo, BE.UserMod, True, Nothing)
                InsertarCruceAsignacionesDocumentos(BE.NuIngreso, vintNuAsignacion, vstrIDDebitMemo, BE.UserMod)

                If blnSeCreoAnticipos Then
                    ConsultarDocumentosxDebitMemoAnticipo(vstrIDDebitMemo, rstrCoTipoDocAnticipo, rstrNuDocumAnticipo, rstrCoSAPDocumento, _
                                                          rstrCoMoneda, rdblSSTotal, rstrCoTipoVta, rstrCoCliente, rintIDCab, rdatFeDocum)
                End If

                If vblnSAP Then
                    Dim strCoSAP As String = EnviarDatosCobranzasSAP(BE.NuIngreso, vintNuAsignacion, vstrIDDebitMemo, vchrAccionDMemo, "", blnSeCreoAnticipos, Nothing, Nothing, 0, 0)
                    ActualizarCoSAP(BE.NuIngreso, vstrIDDebitMemo, strCoSAP, BE.UserMod)
                End If

            ElseIf vchrAccionDMemo = "B" Then
                If vblnSAP Then
                    EnviarDatosCobranzasSAP(BE.NuIngreso, vintNuAsignacion, vstrIDDebitMemo, vchrAccionDMemo, vstrCoSAP, False, Nothing, Nothing, 0, 0)
                End If
                Dim strCoTipoDoc As String = "", strNuDocum As String = ""
                Dim dtt As DataTable = ConsultarDocumentosxDebitMemoAnticipo(vstrIDDebitMemo)

                If dtt.Rows.Count > 0 Then
                    rstrCoTipoDocAnticipo = dtt(0)("IDTipoDoc")
                    rstrNuDocumAnticipo = dtt(0)("NuDocum")
                    rstrCoSAPDocumento = dtt(0)("CoSAP")
                    rstrCoMoneda = dtt(0)("IDMoneda")
                    rdblSSTotal = dtt(0)("SsTotalDocumUSD")
                    rstrCoTipoVta = dtt(0)("CoTipoVenta")
                    rstrCoCliente = dtt(0)("CoCliente")
                    rintIDCab = dtt(0)("IDCab")
                    rdatFeDocum = dtt(0)("FeDocum")
                    'If rstrNuDocumAnticipo <> "" Then
                    '    Dim objDocBT As New clsDocumentoBT
                    '    objDocBT.Anular(rstrCoTipoDocAnticipo, rstrNuDocumAnticipo, BE.UserMod, rstrCoSAPDocumento)
                    'End If
                End If
            End If

            ContextUtil.SetComplete()

            rstrError = ""

            Return vintNuAsignacion

        Catch ex As Exception

            ContextUtil.SetAbort()
            rstrError = ex.Message
            Return vintNuAsignacion

        End Try
    End Function

    Private Overloads Sub ActualizarCoSAP(ByVal vintNuIngreso As Integer, ByVal vstrNuDebitMemo As String, ByVal vstrCoSAP As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)
            dc.Add("@NuDebitMemo", vstrNuDebitMemo)
            dc.Add("@CoSAP", vstrCoSAP)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("INGRESO_DEBIT_MEMO_UpdCoSAP", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
        End Try
    End Sub

    Private Function ConsultarDocumentosxDebitMemoAnticipo(ByVal vstrCoDebitMemo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoDebitMemoAnticipo", vstrCoDebitMemo)

            Return objADO.GetDataTable("DOCUMENTO_SelxCoDebitMemoAnticipo", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function
    Private Sub ConsultarDocumentosxDebitMemoAnticipo(ByVal vstrCoDebitMemo As String, ByRef rstrCoTipoDoc As String, ByRef rstrNuDocum As String, _
                                                      ByRef rstrCoSAP As String, ByRef rstrCoMoneda As String, ByRef rdblSSTotal As Double, _
                                                      ByRef rstrCoTipoVta As String, ByRef rstrCoCliente As String, ByRef rintIDCab As Integer, _
                                                      ByRef rdatFeDocum As Date)
        Dim dc As New Dictionary(Of String, String)
        Try
            'rstrCoTipoDoc = ""
            'rstrNuDocum = ""
            'rstrCoSAP = ""

            Dim dtt As DataTable = ConsultarDocumentosxDebitMemoAnticipo(vstrCoDebitMemo)
            If dtt.Rows.Count > 0 Then
                rstrCoTipoDoc = dtt(0)("IDTipoDoc")
                rstrNuDocum = dtt(0)("NuDocum")
                rstrCoSAP = dtt(0)("CoSAP")
                rstrCoMoneda = dtt(0)("IDMoneda")
                rdblSSTotal = dtt(0)("SsTotalDocumUSD")
                rstrCoTipoVta = dtt(0)("CoTipoVenta")
                rstrCoCliente = dtt(0)("CoCliente")
                rintIDCab = dtt(0)("IDCab")
                rdatFeDocum = dtt(0)("FeDocum")
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Sub CrearAnularAnticipos(ByVal vstrCoTipoDoc As String, ByVal vstrNuDocum As String, _
        ByVal vstrIDDebitMemo As String, ByVal vstrCoMoneda As String, ByVal vdblSSTotal As Double, _
        ByVal vstrCoTipoVta As String, ByVal vstrCoCliente As String, ByVal vintIDCab As Integer, ByVal vdatFeDocum As Date, ByVal vstrUserMod As String)


        Try


            'InsertarBoletasAnticipoxAsignacion(intNuAsignacion, vstrIDDebitMemo, vstrUserMod, False, vdttAnticipos)
            Dim BE As New clsDocumentoBE With {.NuDocum = vstrNuDocum, .IDTipoDoc = vstrCoTipoDoc, .CoDebitMemoAnticipo = vstrIDDebitMemo, _
                                               .IDMoneda = vstrCoMoneda, .SsTotalDocumUSD = vdblSSTotal, .CoTipoVenta = vstrCoTipoVta, _
                                               .IDCliente = vstrCoCliente, .IDCab = vintIDCab, .FeDocum = vdatFeDocum, .UserMod = vstrUserMod}
            InsertarBoletasAnticipoxAsignacionparaAnular(BE)

            'If strNuDocum <> "" Then
            Dim objDocBT As New clsDocumentoBT
            objDocBT.Anular(vstrCoTipoDoc, vstrNuDocum, vstrUserMod, "", "")
            objDocBT.EnviarSAP(vstrCoTipoDoc, vstrNuDocum, vstrUserMod, "PUT")

            'End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub EliminarCruceIngresosDocumentos(ByVal vintNuIngreso As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)
            objADO.ExecuteSP("INGRESO_DOCUMENTO_DelxNuIngreso", dc)

            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("INGRESO_FINANZAS_UpdSsSaldoMatchDocumentoxNuIngreso", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarCruceIngresosDocumentos")

        End Try

    End Sub

    Public Sub ActualizarMatchIngresosDocumentos(ByVal vintNuIngreso As Integer, ByVal vintNuAsignacion As Integer, ByVal vstrUserMod As String)
        Try


            EliminarCruceIngresosDocumentos(vintNuIngreso, vstrUserMod)
            InsertarCruceIngresosDocumentos(vintNuIngreso, vintNuAsignacion, vstrUserMod)
            'InsertarCruceAsignacionesDocumentos(vintNuIngreso, vintNuAsignacion, vstrUserMod)


            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub InsertarCruceIngresosDocumentos(ByVal vintNuIngreso As Integer, _
                                                ByVal vintNuAsignacion As Integer, _
                                                ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", 0)
            dc.Add("@NuIngresoPar", vintNuIngreso)
            dc.Add("@NuAsignacion", vintNuAsignacion)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("INGRESO_DOCUMENTO_InsMatch", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarCruceIngresosDocumentos")

        End Try
    End Sub

    Private Sub InsertarCruceAsignacionesDocumentos(ByVal vintNuIngreso As Integer, _
                                            ByVal vintNuAsignacion As Integer, _
                                            ByVal vstrIDDebitMemo As String, _
                                            ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)
            dc.Add("@NuAsignacion", vintNuAsignacion)
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("ASIGNACION_DOCUMENTO_InsMatch", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarCruceAsignacionesDocumentos")

        End Try
    End Sub


    Public Function InsertarBoletasAnticipoxAsignacion(ByVal vintNuAsignacion As Integer, ByVal vstrNuDebitMemo As String, ByVal vstrUserMod As String, _
                                                  ByVal vblnSAP As Boolean, ByVal vdttAnticipos As DataTable) As Boolean
        Try

            Dim blnSeCreoAnticipos As Boolean = False

            'Dim dttAnticipos As DataTable = AnticiposxAsignacion(vintNuAsignacion)
            Dim dttAnticipos As DataTable
            If vdttAnticipos Is Nothing Then
                dttAnticipos = AnticiposxAsignacionyDMemo(vintNuAsignacion, vstrNuDebitMemo)

            Else
                dttAnticipos = vdttAnticipos
            End If


            If dttAnticipos.Rows.Count > 0 Then
                blnSeCreoAnticipos = True
                Dim ListaDoc As New List(Of clsDocumentoBE)

                For Each dr As DataRow In dttAnticipos.Rows
                    Dim strIDTipoDoc As String = "BOL"
                    Dim dblSSPago As Double = dr("SsPago")
                    If dblSSPago < 0 Then
                        strIDTipoDoc = "NCA"
                        dblSSPago = dblSSPago * (-1)
                    End If
                    Dim strCoCtaContable As String = "496121" '"704112"
                    Dim strCoCeCos As String = String.Empty
                    If dr("CoTipoVenta") = "RC" Then
                        strCoCeCos = "900101"
                    End If

                    If dr("CoTipoVenta") <> "RC" Then
                        strCoCtaContable = "000000"
                    Else
                        Dim objBN As New clsDocumentoProveedorBN
                        Dim dt As DataTable = objBN.ConsultarCtaContable_Documento_Cliente(strCoCeCos, dr("CoTipoVenta"), "005", dr("IDCliente"))
                        If dt.Rows.Count > 0 Then
                            strCoCtaContable = dt.Rows(0).Item("CtaContable")
                        End If
                    End If

                    Dim strNumDocumVinc As String = ""
                    Dim strCoTipoDocumVinc As String = ""
                    Dim datFeDocumVinc As Date = "01/01/1900"
                    If strIDTipoDoc = "NCA" Then
                        Dim strIDTipoDocNuDocumFeDocum As String = strConsultarDocAnticipoxDMemo(vstrNuDebitMemo)
                        If strIDTipoDocNuDocumFeDocum <> "" Then
                            Dim ArrCad() As String = strIDTipoDocNuDocumFeDocum.Split("|")
                            strCoTipoDocumVinc = ArrCad(0)
                            strNumDocumVinc = ArrCad(1)
                            datFeDocumVinc = ArrCad(2)
                        End If
                    End If

                    Dim ItemDocum As New clsDocumentoBE With { _
                            .NuDocum = 0, _
                            .IDTipoDoc = strIDTipoDoc, _
                            .CoSerie = "005", _
                            .IDCab = dr("IDCab"), _
                            .FeDocum = dr("FeAcreditada"), _
                            .IDMoneda = "USD", _
                            .SsTotalDocumUSD = dblSSPago, _
                            .SsIGV = 0, _
                            .Generado = False, _
                            .TxTitulo = "", _
                            .NuDocumVinc = strNumDocumVinc, _
                            .IDTipoDocVinc = strCoTipoDocumVinc, _
                            .FlPorAjustePrecio = False, _
                            .CoTipoVenta = dr("CoTipoVenta"), _
                            .IDCliente = dr("IDCliente"), _
                            .CoCtaContable = strCoCtaContable, _
                            .FeEmisionVinc = datFeDocumVinc, _
                            .CoCtroCosto = "", _
                            .NuFileLibre = "", _
                            .CoDebitMemoAnticipo = vstrNuDebitMemo, _
                            .blnSAP = vblnSAP, _
                            .UserMod = vstrUserMod _
                        }
                    ListaDoc.Add(ItemDocum)
                Next

                Dim BEDocs As New clsDocumentoBE With {.ListaDocumentos = ListaDoc}
                Dim objDocBT As New clsDocumentoBT
                objDocBT.InsertarDocumentosGenerados(0, BEDocs, vstrUserMod, True)
            End If
            ContextUtil.SetComplete()

            Return blnSeCreoAnticipos
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Function

    Private Function strConsultarDocAnticipoxDMemo(ByVal vstrNuDebitMemo As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDebitMemo", vstrNuDebitMemo)
            dc.Add("@pIDTipoDocNuDocumFeDocum", "")

            Return objADO.GetSPOutputValue("DOCUMENTO_SelAnticipoxDMemo", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub InsertarBoletasAnticipoxAsignacionparaAnular(ByVal BE As clsDocumentoBE)
        Try

            Dim ListaDoc As New List(Of clsDocumentoBE)

            Dim strIDTipoDoc As String = "BOL"
            Dim dblSSPago As Double = BE.SsTotalDocumUSD
            If dblSSPago < 0 Then
                strIDTipoDoc = "NCA"
                dblSSPago = dblSSPago * (-1)
            End If
            Dim strCoCtaContable As String = "496121" '"704112"
            Dim strCoCeCos As String = String.Empty
            If BE.CoTipoVenta = "RC" Then
                strCoCeCos = "900101"
            End If

            If BE.CoTipoVenta <> "RC" Then
                strCoCtaContable = "000000"
            Else
                Dim objBN As New clsDocumentoProveedorBN
                Dim dt As DataTable = objBN.ConsultarCtaContable_Documento_Cliente(strCoCeCos, BE.CoTipoVenta, "005", BE.IDCliente)
                If dt.Rows.Count > 0 Then
                    strCoCtaContable = dt.Rows(0).Item("CtaContable")
                End If
            End If

            Dim ItemDocum As New clsDocumentoBE With { _
                    .NuDocum = 0, _
                    .IDTipoDoc = strIDTipoDoc, _
                    .CoSerie = "005", _
                    .IDCab = BE.IDCab, _
                    .FeDocum = BE.FeDocum, _
                    .IDMoneda = BE.IDMoneda, _
                    .SsTotalDocumUSD = dblSSPago, _
                    .SsIGV = 0, _
                    .Generado = False, _
                    .TxTitulo = "", _
                    .NuDocumVinc = "", _
                    .IDTipoDocVinc = "", _
                    .FlPorAjustePrecio = False, _
                    .CoTipoVenta = BE.CoTipoVenta, _
                    .IDCliente = BE.IDCliente, _
                    .CoCtaContable = strCoCtaContable, _
                    .FeEmisionVinc = "01/01/1900", _
                    .CoCtroCosto = "", _
                    .NuFileLibre = "", _
                    .CoDebitMemoAnticipo = BE.CoDebitMemoAnticipo, _
                    .blnSAP = False, _
                    .UserMod = BE.UserMod _
                }
            ListaDoc.Add(ItemDocum)


            Dim BEDocs As New clsDocumentoBE With {.ListaDocumentos = ListaDoc}
            Dim objDocBT As New clsDocumentoBT
            objDocBT.InsertarDocumentosGenerados(0, BEDocs, BE.UserMod, True)

            ContextUtil.SetComplete()


        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Public Sub InsertarBoletasAnticipoxAsignacionReproceso(ByVal vdatFecAcredIni As Date, _
                                                            ByVal vdatFecAcredFin As Date, _
                                                            ByVal vstrUserMod As String)
        Try

            'Dim dttAnticipos As DataTable = AnticiposxAsignacion(vintNuAsignacion)
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@FecAcredIni", vdatFecAcredIni)
            dc.Add("@FecAcredFin", vdatFecAcredFin)
            Dim dttAnticipos As DataTable = objADO.GetDataTable("ASIGNACION_SelAnticiposxNuAsignacionReproceso", dc)
            If dttAnticipos.Rows.Count > 0 Then
                Dim ListaDoc As New List(Of clsDocumentoBE)
                '''''''''''''''20140530

                For Each dr As DataRow In dttAnticipos.Rows
                    Dim ItemDocum As New clsDocumentoBE With { _
                            .NuDocum = 0, _
                            .IDTipoDoc = "BOL", _
                            .CoSerie = "005", _
                            .IDCab = dr("IDCab"), _
                            .FeDocum = dr("FeAcreditada"), _
                            .IDMoneda = "USD", _
                            .SsTotalDocumUSD = dr("SsPago"), _
                            .SsIGV = 0, _
                            .Generado = False, _
                            .TxTitulo = "", _
                            .NuDocumVinc = "", _
                            .IDTipoDocVinc = "", _
                            .FlPorAjustePrecio = False, _
                            .CoTipoVenta = "RC", _
                            .NuFileLibre = "", _
                            .UserMod = vstrUserMod _
                        }
                    ListaDoc.Add(ItemDocum)
                Next

                Dim BEDocs As New clsDocumentoBE With {.ListaDocumentos = ListaDoc}
                Dim objDocBT As New clsDocumentoBT
                objDocBT.InsertarDocumentosGenerados(0, BEDocs, vstrUserMod, True)
            End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Private Function AnticiposxAsignacion(ByVal vintNuAsignacion As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuAsignacion", vintNuAsignacion)

            Return objADO.GetDataTable("ASIGNACION_SelAnticiposxNuAsignacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function AnticiposxAsignacionyDMemo(ByVal vintNuAsignacion As Integer, ByVal vstrNuDebitMemo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuAsignacion", vintNuAsignacion)
            dc.Add("@IDDebitMemo", vstrNuDebitMemo)

            Return objADO.GetDataTable("ASIGNACION_SelAnticiposxNuAsignacionyDMemo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Function ConsultarCobranzasSAP(ByVal vintNuIngreso As Integer, ByVal vstrIDDebitMemo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            Return objADO.GetDataTable("INGRESO_DEBIT_MEMO_xNuIngreso", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarInvoicesSAP(ByVal vintNuAsignacion As Integer, _
                                          ByVal vstrIDDebitMemo As String, _
                                          ByVal vsglTipoCambioIng As Single, _
                                          ByVal blnSeCreoAnticipos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            'dc.Add("@IDCab", vintIDCab)
            dc.Add("@NuAsignacion", vintNuAsignacion)
            'dc.Add("@IDMonedaIng", vstrIDMonedaIng)
            dc.Add("@TCambioIng", vsglTipoCambioIng)
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@bSeCreoAnticipos", blnSeCreoAnticipos)

            'Return objADO.GetDataTable("DOCUMENTO_SelxIDCabSAP", dc)
            Return objADO.GetDataTable("ASIGNACION_DOCUMENTO_Sel_SAP", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub ActualizarPago(ByVal vintNuAsignacion As Integer, ByVal vdblSsImportePagado As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuAsignacion", vintNuAsignacion)
                .Add("@SsImportePagado", vdblSsImportePagado)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("ASIGNACION_UpdPago", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Overloads Sub Actualizar(ByVal BE As clsAsignacionBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuAsignacion", BE.NuAsignacion)
                .Add("@TxObservacion", BE.TxObservacion)
                .Add("@SsImportePagado", BE.SsImportePagado)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("ASIGNACION_Upd", dc)

            ActualizarEstadoIngreso(BE)

            Dim objBTDebitMemo As New clsDebitMemoBT
            objBTDebitMemo.InsertarActualizarEliminar(BE)

            Dim objBTAsignados As New clsIngreso_DebitMemoBT
            objBTAsignados.InsertarActualizarEliminar(BE)

            'InsertarBoletasAnticipoxAsignacion(BE.NuAsignacion, BE.UserMod)

            ActualizarMatchIngresosDocumentos(BE.NuIngreso, BE.NuAsignacion, BE.UserMod)

            'For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
            '    EnviarDatosCobranzasSAP(BE.NuIngreso, BE.NuAsignacion, Item.IDDebitMemo)
            'Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function EnviarDatosCobranzasSAP(ByVal vintNuIngreso As Integer, ByVal vintNuAsignacion As Integer, ByVal vstrIDDebitMemo As String, _
                                             ByVal vchrAccion As Char, ByVal vstrCoSAP As String, ByVal vblnSeCreoAnticipos As Boolean, _
                                             ByVal vLstDMemos As List(Of String), ByVal vLstDMemosAntic As List(Of String), _
                                             ByVal vdblTotalPagos As Double, ByVal vdblGastosTranfComisionBco As Double) As String
        Try

            Dim objWebBT As New clsIncomingPaymentsBT
            Dim oIncoPay As clsIncomingPaymentsBE = Nothing
            Dim strCoSAP As String = ""
            If vchrAccion = "N" Then
                Dim dtDatosCobranzas As DataTable = ConsultarCobranzasSAP(vintNuIngreso, vstrIDDebitMemo)
                For Each ItemRow As DataRow In dtDatosCobranzas.Rows

                    oIncoPay = New clsIncomingPaymentsBE
                    With oIncoPay
                        .DocDate = ItemRow("DocDate")
                        .TaxDate = ItemRow("TaxDate")
                        .DueDate = ItemRow("DueDate")
                        .CardCode = ItemRow("CardCodeSAP")
                        .DocType = ItemRow("DocType")
                        .TransferAccount = ItemRow("TransferAccount")
                        '.BankChargeAmount = ItemRow("BankChargeAmount")
                        If vLstDMemos Is Nothing Then
                            .TransferSum = ItemRow("TransferSum")
                            .TransferSumFc = ItemRow("TransferSumFc")
                            .BankChargeAmount = ItemRow("BankChargeAmount")
                        Else
                            .TransferSumFc = vdblTotalPagos
                            .TransferSum = Math.Round(vdblTotalPagos * ItemRow("U_SYP_TC"), 3)
                            ''.TransferSum = vdblTotalPagos * ItemRow("U_SYP_TC")
                            .BankChargeAmount = vdblGastosTranfComisionBco
                        End If

                        .BridgeAccount = ItemRow("BridgeAccount")
                        .DocCurrency = ItemRow("DocCurrency")
                        .DocRate = ItemRow("DocRate")
                        .U_SYP_MPPG = ItemRow("U_SYP_MPPG")
                        .U_SYP_NUMOPER = ItemRow("U_SYP_NUMOPER") '"1508020504"
                        .U_SYP_TC = ItemRow("U_SYP_TC")
                        .U_SYP_NFILE = ItemRow("U_SYP_NFILE")
                        .TransferReference = ItemRow("TransferReference")
                        .ControlAccount = ItemRow("ControlAccount")

                        'Dim ListInvoices As New List(Of clsInvoicesInIncomingPaymentsBE)
                        'Dim DatosInvoices As DataTable = ConsultarInvoicesSAP(vintNuAsignacion, vstrIDDebitMemo, .DocRate, vblnSeCreoAnticipos)
                        'If DatosInvoices.Rows.Count > 0 Then
                        '    .U_SYP_TIPOCOB = ItemRow("U_SYP_TIPOCOB")

                        '    For Each ItemRow2 As DataRow In DatosInvoices.Rows
                        '        Dim oBEInvoice As New clsInvoicesInIncomingPaymentsBE
                        '        With oBEInvoice
                        '            .InvoiceType = ItemRow2("InvoiceType")
                        '            .U_SYP_MDTD = ItemRow2("U_SYP_MDTD")
                        '            .U_SYP_MDSD = ItemRow2("U_SYP_MDSD")
                        '            .U_SYP_MDCD = ItemRow2("U_SYP_MDCD")
                        '            .DocCur = ItemRow2("DocCur")
                        '            .SumApplied = ItemRow2("SumApplied")
                        '            .AppliedFC = ItemRow2("AppliedFC")
                        '        End With
                        '        ListInvoices.Add(oBEInvoice)
                        '    Next
                        '    .Invoices = ListInvoices
                        'End If

                        If vLstDMemos Is Nothing Then
                            .Invoices = lstCargaInvoice(vintNuAsignacion, vstrIDDebitMemo, .DocRate, vblnSeCreoAnticipos, Nothing)
                        Else
                            Dim Invoices As New List(Of clsInvoicesInIncomingPaymentsBE)
                            For Each strDM As String In vLstDMemos
                                Dim blnSeCreoAnticipos As Boolean = False
                                If vLstDMemosAntic.Contains(strDM) Then
                                    blnSeCreoAnticipos = True
                                End If
                                .Invoices = lstCargaInvoice(vintNuAsignacion, strDM, .DocRate, blnSeCreoAnticipos, Invoices)
                            Next

                        End If

                    End With

                    strCoSAP = objWebBT.GrabarSAP(oIncoPay, "PaymentAccounts", "TransferDate", "CounterReference", "Reference2", "Remarks", "JournalRemarks")

                Next
            ElseIf vchrAccion = "B" Then
                If vstrCoSAP <> "" Then
                    oIncoPay = New clsIncomingPaymentsBE With {.U_SYP_NUMOPER = vstrIDDebitMemo}
                    objWebBT.AnularSAP(oIncoPay)
                End If
            End If


            ContextUtil.SetComplete()

            Return strCoSAP
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Function lstCargaInvoice(ByVal vintNuAsignacion As Integer, ByVal vstrIDDebitMemo As String, vsglTCIng As Single, ByVal vblnSeCreoAnticipos As Boolean, _
                                     ByRef rListInvoices As List(Of clsInvoicesInIncomingPaymentsBE)) As List(Of clsInvoicesInIncomingPaymentsBE)
        Try
            Dim ListInvoices As New List(Of clsInvoicesInIncomingPaymentsBE)
            Dim DatosInvoices As DataTable = ConsultarInvoicesSAP(vintNuAsignacion, vstrIDDebitMemo, vsglTCIng, vblnSeCreoAnticipos)
            If DatosInvoices.Rows.Count > 0 Then
                For Each ItemRow2 As DataRow In DatosInvoices.Rows
                    Dim oBEInvoice As New clsInvoicesInIncomingPaymentsBE
                    With oBEInvoice
                        .InvoiceType = ItemRow2("InvoiceType")
                        .U_SYP_MDTD = ItemRow2("U_SYP_MDTD")
                        .U_SYP_MDSD = ItemRow2("U_SYP_MDSD")
                        'If ItemRow2("U_SYP_MDCD").ToString = "0005092" Or ItemRow2("U_SYP_MDCD").ToString = "0005316" Then
                        '.U_SYP_MDCD = Mid(ItemRow2("U_SYP_MDCD"), 2)
                        'Else
                        .U_SYP_MDCD = ItemRow2("U_SYP_MDCD").ToString.Trim
                        'End If
                        .DocCur = ItemRow2("DocCur")
                        .SumApplied = ItemRow2("SumApplied")
                        .AppliedFC = ItemRow2("AppliedFC")
                    End With
                    If Not rListInvoices Is Nothing Then
                        rListInvoices.Add(oBEInvoice)
                    Else
                        ListInvoices.Add(oBEInvoice)
                    End If

                Next
            End If
            If Not rListInvoices Is Nothing Then
                Return rListInvoices
            Else
                Return ListInvoices
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Sub Eliminar(ByVal BE As clsAsignacionBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim objDMemoBT As New clsDebitMemoBT
            For Each Item As clsDebitMemoBE In BE.ListaDebitMemo
                objDMemoBT.ActualizarEstado(Item.IDDebitMemo, Item.IDEstado, Item.UserMod)
                objDMemoBT.ActualizarSaldo(Item.IDDebitMemo, Item.Saldo, Item.UserMod)
            Next

            Dim objIng_DebitBT As New clsIngreso_DebitMemoBT
            objIng_DebitBT.EliminarxAsignacion(BE.NuAsignacion)

            Dim objIngresoBT As New clsIngresoFinanzasBT
            objIngresoBT.Eliminar(BE.NuIngreso)

            dc.Add("@NuAsignacion", BE.NuAsignacion)
            objADO.ExecuteSP("ASIGNACION_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarEstadoIngreso(ByVal BE As clsAsignacionBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", BE.NuIngreso)
            dc.Add("@CoEstado", BE.CoEstado)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("INGRESO_FINANZAS_UpdEstadoxAsignacion", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsAsignacionBE, ByVal vstrAntiguoID As String, _
                                                      ByVal vintNuevoID As Integer)
        If BE.ListaIngresoDebitMemo Is Nothing Then Exit Sub
        Try
            For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
                If Item.NuAsignacion = vstrAntiguoID Then
                    Item.NuAsignacion = vintNuevoID
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsIngreso_DebitMemoBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Private Overloads Sub Insertar(ByVal BE As clsIngreso_DebitMemoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuAsignacion", BE.NuAsignacion)
            dc.Add("@NuIngreso", BE.NuIngreso)
            dc.Add("@IDDebitMemo", BE.IDDebitMemo)
            dc.Add("@SsPago", BE.SsPago)
            dc.Add("@SsGastosTransferencia", BE.SsGastosTransferencia)
            dc.Add("@SsComisionBanco", BE.SsComisionBanco)
            dc.Add("@SsSaldoNuevo", BE.SsSaldoNuevo)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("INGRESO_DEBIT_MEMO_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsIngreso_DebitMemoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuAsignacion", BE.NuAsignacion)
            dc.Add("@NuIngreso", BE.NuIngreso)
            dc.Add("@IDDebitMemo", BE.IDDebitMemo)
            dc.Add("@SsPago", BE.SsPago)
            dc.Add("@SsGastosTransferencia", BE.SsGastosTransferencia)
            dc.Add("@SsComisionBanco", BE.SsComisionBanco)
            dc.Add("@SsSaldoNuevo", BE.SsSaldoNuevo)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("INGRESO_DEBIT_MEMO_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintNuIngreso As Integer, ByVal vstrIDDebitMemo As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            objADO.ExecuteSP("INGRESO_DEBIT_MEMO_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function blnExiste(ByVal vintNuIngreso As Integer, ByVal vstrIDDebitMemo As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@NuIngreso", vintNuIngreso)
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("INGRESO_DEBIT_MEMO_SelExistexNuIngresoxIDDebitMemo", dc)


        Catch ex As Exception

            Throw
        End Try
    End Function

    Public Overloads Sub EliminarxAsignacion(ByVal vintNuAsignacion As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuAsignacion", vintNuAsignacion)

            objADO.ExecuteSP("INGRESO_DEBIT_MEMO_DelxNuAsignacion", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsAsignacionBE, Optional vstrIDDebitMemo As String = "")

        Try
            Dim objDMemoBT As New clsDebitMemoBT
            For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
                If Not vstrIDDebitMemo = "" Then
                    If vstrIDDebitMemo <> Item.IDDebitMemo Then
                        GoTo Sgte
                    End If
                End If
                If Item.Accion = "N" Or Item.Accion = "M" Then
                    If Not blnExiste(Item.NuIngreso, Item.IDDebitMemo) Then
                        Insertar(Item)
                    Else
                        Actualizar(Item)
                    End If
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.NuIngreso, Item.IDDebitMemo)
                End If
                objDMemoBT.ActualizarEstado(Item.IDDebitMemo, Item.IDEstadoDebitMemo, Item.UserMod)
                objDMemoBT.ActualizarSaldo(Item.IDDebitMemo, Item.SsSaldoDebitMemo, Item.UserMod)

                ActualizarSaldoMatchAsignac(Item.NuAsignacion, Item.IDDebitMemo, Item.UserMod)
Sgte:
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarSaldoMatchAsignac(ByVal vintNuAsignacion As Integer, ByVal vstrNuDebitMemo As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@NuAsignacion", vintNuAsignacion)
            dc.Add("@IDDebitMemo", vstrNuDebitMemo)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("DOCUMENTO_UpdSsSaldoMatchAsignac", dc)

            dc.Remove("@UserMod")

            objADO.ExecuteSP("ASIGNACION_DOCUMENTO_DelxAsignacDMemo", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOrdenPago_AdjuntosBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsOrdenPago_AdjuntosBT"
    Private Overloads Sub Insertar(ByVal BE As clsOrdenPago_AdjuntosBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", BE.IDOrdPago)
            dc.Add("@NoArchivo", BE.NoArchivo)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("ORDENPAGO_ADJUNTOS_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuOrdenAdjunto As Integer, _
                                  ByVal vintIDOrdPago As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdenAdjunto", vintNuOrdenAdjunto)
            dc.Add("@IDOrdPag", vintIDOrdPago)

            objADO.ExecuteSP("ORDENPAGO_ADJUNTOS_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Sub InsertarEliminar(ByVal BE As clsOrdenPagoBE)
        If BE.ListaAdjuntosOrdenPago Is Nothing Then Exit Sub
        Try
            For Each Item As clsOrdenPago_AdjuntosBE In BE.ListaAdjuntosOrdenPago
                If Item.Accion = "N" Then
                    If Not blnExisteArchivoIngresado(Item.IDOrdPago, _
                                                     Mid(Item.NoArchivo, InStrRev(Item.NoArchivo, "\") + 1)) Then
                        Insertar(Item)
                    End If
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.NuOrdenAdjunto, Item.IDOrdPago)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarEliminar")
            Throw
        End Try
    End Sub

    Private Function blnExisteArchivoIngresado(ByVal vintIDOrdPag As Integer, ByVal vstrNombreArchivo As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)
            dc.Add("@NombreArchivo", vstrNombreArchivo)
            dc.Add("@pExisteArchivo", 0)

            Return objADO.GetSPOutputValue("ORDENPAGO_ADJUNTOS_Sel_ExistsxIDOrdPagxNoArchivo", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExisteArchivoIngresado")
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOrdenPagoBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsOrdenPagoBT"


    'Dim gstrCorreoAdministPrepagos As String = "pagos3@setours.com"
    'Dim gstrCorreoAdministPrepagos As String = "pagos1@setours.com"
    'Dim gstrCorreoAdministPrepagosOperadInternac As String = "cristhian.arambulo@setours.com"

    'Dim gstrBuenosAires As String = "000113"
    'Dim gstrCorreoPrepagosBuenosAires As String = "eugenia.rosas@setours.com"
    'Dim gstrSantiago As String = "000110"
    'Dim gstrCorreoPrepagosSantiago As String = "marcela.barriga@setours.com"

    Public Overloads Function Insertar(ByVal BE As clsOrdenPagoBE) As Integer
        Dim intIDOrdPag As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDReserva", BE.IDReserva)
                dc.Add("@IDCab", BE.IDCab)
                dc.Add("@IDFormaPago", BE.IDFormaPago)
                dc.Add("@IDBanco", BE.IDBanco)
                dc.Add("@FechaPago", BE.FechaPago)
                dc.Add("@Numeroformapago", BE.Numeroformapago)
                dc.Add("@Porcentaje", BE.Porcentaje)
                dc.Add("@TCambio", BE.TCambio)
                dc.Add("@Observaciones", BE.Observaciones)
                dc.Add("@CtaCte", BE.CtaCte)
                dc.Add("@IDMoneda", BE.IDMoneda)
                dc.Add("@UserMod", BE.UserMod)
                dc.Add("@SsDetraccion", BE.SsDetraccion)
                dc.Add("@pIDOrdPag", 0)

                intIDOrdPag = objADO.ExecuteSPOutput("ORDENPAGO_Ins", dc)
            End With

            'Dim objBTDetOrdPago As New clsDetalleAdministracionBT
            'objBTDetOrdPago.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()

            Return intIDOrdPag
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Overloads Sub Actualizar(ByVal BE As clsOrdenPagoBE, Optional ByVal BECot As clsCotizacionBE = Nothing)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDOrdPag", BE.IDOrdPag)
                .Add("@IDFormaPago", BE.IDFormaPago)
                .Add("@IDBanco", BE.IDBanco)
                .Add("@FechaPago", BE.FechaPago)
                .Add("@FechaEnvio", BE.FechaEnvio)
                .Add("@Porcentaje", BE.Porcentaje)
                .Add("@TCambio", BE.TCambio)
                .Add("@Numeroformapago", BE.Numeroformapago)
                .Add("@Observaciones", BE.Observaciones)
                .Add("@CtaCte", BE.CtaCte)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@IDEstado", BE.IDEstado)
                .Add("@SsDetraccion", BE.SsDetraccion)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_Upd", dc)

            'Actualizar_Total_Saldo(BE.IDOrdPag, BE.UserMod)

            Dim objBTDetOrdPago As New clsDetalleAdministracionBT
            objBTDetOrdPago.InsertarActualizarEliminar(BE)

            Dim objBTAdjuntosOrdPago As New clsOrdenPago_AdjuntosBT
            objBTAdjuntosOrdPago.InsertarEliminar(BE)

            Actualizar_Total_Saldo(BE.IDOrdPag, BE.UserMod)

            'ARCHIVOS
            If Not BECot Is Nothing Then
                Dim objArchivos As New clsCotizacionBT.clsCotizacionArchivosBT
                objArchivos.InsertarActualizarEliminar(BECot)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar_Total_Saldo(ByVal vintIDOrdPag As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDOrdPag", vintIDOrdPag)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_Upd_Saldo_Total", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintIDOrdPago As Integer, Optional vstrUser As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPago)

            'Antes de eliminar, enviar PUT a SAP
            Dim arrDeleteProperties As String() = Nothing

            Dim oBEPurchaseInvoice As clsPurchaseInvoices_CancellationBE = Nothing
            PasarDatosDocumentosToSocioNegocio_PUT(vintIDOrdPago, vstrUser, oBEPurchaseInvoice, arrDeleteProperties)

            'primero verificar que orden de pago tenga codigo sap
            Dim dtt As DataTable = dttConsultar_Pk(vintIDOrdPago)

            If dtt.Rows.Count > 0 Then
                Dim strCoSAP As String = ""
                strCoSAP = IIf(IsDBNull(dtt.Rows(0).Item("CoSap")), "", dtt.Rows(0).Item("CoSap"))
                If strCoSAP <> "" Then
                    Dim oBTJsonMaster As New clsJsonInterfaceBT()
                    'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                    '                                     "http://sap:8899/api/PurchaseInvoices/", _
                    '                                     "PUT", arrDeleteProperties)
                    oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                                       "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                                       "PUT", arrDeleteProperties)
                End If
            End If


            'Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
            '                                     "http://sap:8899/api/PurchaseInvoices/", _
            '                                     "PUT", arrDeleteProperties)


            objADO.ExecuteSP("ORDENPAGO_CORREOFILE_DelxIDOrdPag", dc)
            objADO.ExecuteSP("ORDENPAGO_ADJUNTOS_DelxIDOrdPag", dc)
            objADO.ExecuteSP("ORDENPAGO_DET_DelxIDOrdPag", dc)
            objADO.ExecuteSP("ORDENPAGO_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    'Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOrdenPagoBE)
    '    Try
    '        For Each Item As clsOrdenPagoBE In BE.ListOrdenesPago
    '            If Item.Accion = "N" Then
    '                Dim intIDOrdPag As Integer = Insertar(Item)
    '            ElseIf Item.Accion = "M" Then
    '                Actualizar(Item)
    '            ElseIf Item.Accion = "B" Then
    '                Eliminar(Item.IDOrdPag)
    '            End If
    '        Next

    '        Dim objBTDetOrdPago As New clsDetalleAdministracionBT
    '        objBTDetOrdPago.InsertarActualizarEliminar(BE)

    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub


    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsOrdenPagoBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDOrdPag", BE.IDOrdPag)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_UpdxDocumento", dc)

            If Not BE.FlDesdeOtraForEgreso Then
                ActualizarFormasEgreso(BE)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
            Throw
        End Try
    End Sub

    Private Sub ActualizarFormasEgreso(ByVal BE As clsOrdenPagoBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@IDOrdPag", BE.IDOrdPag)
            Dim dttFEgr As DataTable = objADO.GetDataTable("OPERACIONES_DET_SelVoucherxOrdenPago", dc)

            For Each dr As DataRow In dttFEgr.Rows
                Dim objVoBT As New clsVoucherOperacionBT
                Dim objVoBE As New clsVoucherOperacionBE With {.IDVoucher = dr("IDVoucher"), _
                                                         .CoEstado = BE.CoEstado, _
                                                         .SsSaldo = BE.SsSaldo, _
                                                         .SsDifAceptada = BE.SsDifAceptada, _
                                                         .TxObsDocumento = "", _
                                                         .FlDesdeOtraForEgreso = True, _
                                                         .UserMod = BE.UserMod}

                objVoBT.ActualizarxDocumento(objVoBE)

            Next

            dttFEgr = objADO.GetDataTable("ORDEN_SERVICIO_DET_SelxOrdenPago", dc)

            For Each dr As DataRow In dttFEgr.Rows
                Dim objOSBT As New clsOrdenServicioBT
                Dim objOSBE As New clsOrdenServicioBE With {.NuOrden_Servicio = dr("NuOrden_Servicio"), _
                                                         .CoEstado = BE.CoEstado, _
                                                         .SsSaldo = BE.SsSaldo, _
                                                         .SsDifAceptada = BE.SsDifAceptada, _
                                                         .TxObsDocumento = "", _
                                                         .FlDesdeOtraForEgreso = True, _
                                                         .UserMod = BE.UserMod}

                objOSBT.ActualizarxDocumento(objOSBE)

            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarFormasEgreso")

        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintIDOrdPag As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("ORDENPAGO_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Public Overloads Sub ActualizarEstado_Multiple(ByVal vintIDOrdPag As Integer, ByVal vstrCoEstado As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("ORDENPAGO_Upd_Estado_Multiple", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Public Sub RegenerarxReserva(ByVal BE As clsOrdenPagoBE)
        Try

            Dim dtt As DataTable = dttConsultar(BE.IDReserva, BE.IDCab)
            For Each dr As DataRow In dtt.Rows
                Eliminar(dr("IDOrdPag"))
            Next

            GrabarxReserva(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function dttConsultar(ByVal vintIDReserva As Integer, ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCAB", vintIDCab)

            Return objADO.GetDataTable("ORDENPAGO_SelxIDReserva", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Sub GrabarxReserva(ByVal BE As clsOrdenPagoBE)
        Try


            For Each Item As clsOrdenPagoBE In BE.ListOrdenesPago
                Dim intIDOrdPag As Integer = Insertar(Item)

                Dim objDetBT As New clsDetalleAdministracionBT
                'objDetBT.GrabarxReserva(BE.IDReserva, intIDOrdPag, _
                '                Item.Porcentaje, Item.MontoMaximo, Item.TCambio, BE.UserMod)
                'BE.IDOrdPag = intIDOrdPag

                objDetBT.ActualizarIDsNuevosenHijosST(BE, intIDOrdPag)
                objDetBT.InsertarActualizarEliminar(BE)

                'actualizar total y saldo
                Actualizar_Total_Saldo(intIDOrdPag, BE.UserMod)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub EnviarSAP_Tmp(ByVal vintIDOrdPag As Integer, ByVal vstrUserMod As String)
        Try
            Dim arrDeleteProperties As String() = Nothing

            Dim oBEPurchaseInvoice As clsPurchaseInvoicesBE = Nothing
            PasarDatosDocumentosToSocioNegocio(vintIDOrdPag, vstrUserMod, oBEPurchaseInvoice, arrDeleteProperties)

            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            Dim strPuertoSAP As String = ""
            strPuertoSAP = gstrPuertoSAP 'ConfigurationSettings.AppSettings("PuertoSAP")
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
            '                                     "http://sap:8899/api/PurchaseInvoices/", _
            '                                     "POST", arrDeleteProperties)
            oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                               "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                               "POST", arrDeleteProperties)

            Actualizar_CardCodeSAP(vintIDOrdPag, objResponseSAP.Id)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub
    Private Function strDevuelveUsuarioxCoCargo(ByVal vintCoCargo As Integer) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCargo", vintCoCargo)
            dc.Add("@pCorreos", String.Empty)

            Return objADO.GetSPOutputValue("MAUSUARIOS_SelxCoCargo_CorreosOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub EnviarCorreoaAdministracion(ByVal vintIDOrdPag As Integer, _
                                ByVal vstrArchivoOrdenPagoPdf As String, _
                                ByVal vstrIDFile As String, _
                                ByVal vstrTitulo As String, _
                                ByVal vstrIDProveedor As String, _
                                ByVal vstrDescProveedor As String, _
                                ByVal vstrCorreoUserMod As String, _
                                ByVal vstrUserMod As String, _
                                ByVal vblnEsEnviadoxSupervisor As Boolean)
        'Try


        '    Dim strAsunto As String = vstrIDFile & " - " & vstrTitulo & " - Orden de Pago " & vstrDescProveedor.ToUpper
        '    Dim strMensaje As String = "Buenos días:" & Chr(13) & _
        '        "Se adjunta Orden de Pago del Proveedor: " & vstrDescProveedor & _
        '        ", Nro. File: " & vstrIDFile & ", Ref: " & vstrTitulo & Chr(13) & Chr(13) & _
        '        "Por favor no responder este correo." & Chr(13) & _
        '        "Atentamente " & Chr(13) & _
        '        "Dpto. de Reservas"

        '    Dim strCorreoDestinatario As String = strDevuelveCadenaOcultaDerecha(strRazSocialUbigeoOficina(vstrIDProveedor))
        '    If Not vblnEsEnviadoxSupervisor Then strCorreoDestinatario = strDevuelveUsuarioxCoCargo(gintCoCargoSupReserva) '"annie.valenzuela@setours.com"

        '    Dim objCorreoBT As New clsCorreoFileBT
        '    ' ''objCorreoBT.EnviarCorreoSQL("SETRA", strCorreoDestinatario & ";" & _
        '    ' ''vstrCorreoUserMod, "", "", _
        '    ' ''strMensaje, strAsunto, "TEXT", vstrArchivoOrdenPagoPdf, Nothing, "", 0)

        '    If vblnEsEnviadoxSupervisor Then
        '        ActualizarEstado(vintIDOrdPag, "PD", vstrUserMod)
        '    Else
        '        Actualizar_EnviadoAlSupervisor(vintIDOrdPag, True, vstrUserMod)
        '    End If

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    Throw
        'End Try
    End Sub

    Public Sub Actualizar_EnviadoAlSupervisor(ByVal vintIDOrdPag As Integer, ByVal vblnEnviadoSuper As Boolean, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)
            dc.Add("@FlEnviadoSupResv", vblnEnviadoSuper)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("ORDENPAGO_UpdFlEnviadoSupResv", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Actualizar_CardCodeSAP(ByVal vintIDOrdPag As Integer, ByVal strCardCodeSAP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDOrdPag", vintIDOrdPag)
                .Add("@CoSap", strCardCodeSAP)
            End With

            objADO.ExecuteSP("ORDENPAGO_Upd_CoSap", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub PasarDatosDocumentosToSocioNegocio_PUT(ByVal vintIDOrdPag As Integer, ByVal vstrUserMod As String, _
                                            ByRef oBEPurchaseInvoice As clsPurchaseInvoices_CancellationBE, ByRef arrDelProperty As String())
        Try
            Dim dtDatos As DataTable = drConsultarDatosSAP_PUT(vintIDOrdPag)

            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBEPurchaseInvoice = New clsPurchaseInvoices_CancellationBE()
                With oBEPurchaseInvoice

                    .U_SYP_MDTD = dr("U_SYP_MDTD").ToString.Trim
                    .U_SYP_MDSD = dr("U_SYP_MDSD").ToString.Trim
                    .U_SYP_MDCD = dr("U_SYP_MDCD").ToString.Trim
                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_TCOMPRA = dr("U_SYP_TCOMPRA")

                End With
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub PasarDatosDocumentosToSocioNegocio(ByVal vintIDOrdPag As Integer, ByVal vstrUserMod As String, _
                                                ByRef oBEPurchaseInvoice As clsPurchaseInvoicesBE, ByRef arrDelProperty As String())
        Try
            Dim dtDatos As DataTable = drConsultarDatosSAP(vintIDOrdPag)
            Dim dtDatos_Lines As DataTable = drConsultarDatosSAP_Lines(vintIDOrdPag)
            'Dim dtDatos_Expenses As DataTable = drConsultarDatosSAP_Expenses(vintNuDocumProv)
            Dim dtDatos_WithholdingTaxData As DataTable = drConsultarDatosSAP_WithholdingTaxData(vintIDOrdPag)
            Dim oBEPurchaseInvoices_Lines As clsPurchaseInvoices_LineBE
            Dim objLstLines As List(Of clsPurchaseInvoices_LineBE) = New List(Of clsPurchaseInvoices_LineBE)
            Dim objLstExpenses As List(Of clsPurchaseInvoices_ExpensesBE) = New List(Of clsPurchaseInvoices_ExpensesBE)
            Dim objLstWithholdingTaxData As List(Of clsPurchaseInvoices_WithholdingTaxDataBE) = New List(Of clsPurchaseInvoices_WithholdingTaxDataBE)
            Dim lstCampos_Borrar As List(Of String) = New List(Of String)
            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBEPurchaseInvoice = New clsPurchaseInvoicesBE()
                With oBEPurchaseInvoice

                    .TaxDate = dr("TaxDate")
                    .DocDate = dr("DocDate")
                    .DocDueDate = dr("DocDueDate")
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .DocType = dr("DocType")
                    .FederalTaxID = dr("FederalTaxID")
                    .DocCurrency = dr("DocCurrency")
                    .ControlAccount = dr("ControlAccount")
                    .CashAccount = dr("CashAccount")

                    If .CashAccount.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("CashAccount")
                    End If

                    .Comments = dr("Comments")
                    .PaymentGroupCode = dr("PaymentGroupCode")
                    .DiscountPercent = dr("DiscountPercent")

                    If .DiscountPercent = 0 Then
                        lstCampos_Borrar.Add("DiscountPercent")
                    End If

                    .DocTotal = dr("DocTotal")
                    .DocTotalFc = dr("DocTotalFc")
                    .U_SYP_MDTD = dr("U_SYP_MDTD")
                    .U_SYP_MDSD = dr("U_SYP_MDSD")
                    .U_SYP_MDCD = dr("U_SYP_MDCD")

                    .U_SYP_MDTO = dr("U_SYP_MDTO").ToString.Trim
                    If .U_SYP_MDTO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDTO")
                    End If

                    .U_SYP_MDSO = dr("U_SYP_MDSO").ToString.Trim
                    If .U_SYP_MDSO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDSO")
                    End If

                    .U_SYP_MDCO = dr("U_SYP_MDCO").ToString.Trim
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDCO")
                    End If

                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_FECHAREF = dr("U_SYP_FECHAREF")
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_FECHAREF")
                    End If

                    Dim intcapacidad As Integer = 0
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        intcapacidad = 0
                        ReDim arrDelProperty(intcapacidad)
                        'arrDelProperty.SetValue("U_SYP_MDTO", 0)
                        'arrDelProperty.SetValue("U_SYP_MDSO", 1)
                        'arrDelProperty.SetValue("U_SYP_MDCO", 2)
                        arrDelProperty.SetValue("U_SYP_FECHAREF", 0)
                    End If


                    .U_SYP_NUMOPER = dr("U_SYP_NUMOPER")
                    .U_SYP_TC = dr("U_SYP_TC")
                    .U_SYP_TCOMPRA = dr("U_SYP_TCOMPRA")
                    .U_SYP_NFILE = dr("U_SYP_NFILE").ToString.Trim
                    If .U_SYP_NFILE.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_NFILE")
                    End If

                    .U_SYP_CTAERCC = dr("U_SYP_CTAERCC").ToString.Trim

                    'If .U_SYP_NFILE.Trim.Length = 0 Then
                    '    intcapacidad = intcapacidad + 1
                    '    ReDim arrDelProperty(intcapacidad)
                    '    arrDelProperty.SetValue("U_SYP_MDTO", intcapacidad)
                    'End If

                    If dtDatos_Lines.Rows.Count > 0 Then
                        For Each dr1 As DataRow In dtDatos_Lines.Rows
                            oBEPurchaseInvoices_Lines = New clsPurchaseInvoices_LineBE

                            oBEPurchaseInvoices_Lines.ItemCode = dr1("ItemCode")
                            oBEPurchaseInvoices_Lines.ItemDescription = dr1("ItemDescription")
                            oBEPurchaseInvoices_Lines.WarehouseCode = dr1("WarehouseCode")
                            oBEPurchaseInvoices_Lines.Quantity = dr1("Quantity")
                            oBEPurchaseInvoices_Lines.DiscountPercent = dr1("DiscountPercent")
                            oBEPurchaseInvoices_Lines.AccountCode = dr1("AccountCode")
                            oBEPurchaseInvoices_Lines.UnitPrice = dr1("UnitPrice")
                            oBEPurchaseInvoices_Lines.LineTotal = dr1("LineTotal")
                            oBEPurchaseInvoices_Lines.TaxCode = dr1("TaxCode")
                            oBEPurchaseInvoices_Lines.TaxTotal = dr1("TaxTotal")
                            oBEPurchaseInvoices_Lines.WTLiable = dr1("WTLiable")
                            oBEPurchaseInvoices_Lines.TaxOnly = dr1("TaxOnly")
                            'oBEPurchaseInvoices_Lines.ProjectCode = dr1("ProjectCode")
                            oBEPurchaseInvoices_Lines.CostingCode = dr1("CostingCode").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode2 = dr1("CostingCode2").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode3 = dr1("CostingCode3").ToString.Trim
                            oBEPurchaseInvoices_Lines.U_SYP_CONCEPTO = dr1("U_SYP_CONCEPTO").ToString.Trim
                            objLstLines.Add(oBEPurchaseInvoices_Lines)
                        Next
                        oBEPurchaseInvoice.Lines = objLstLines
                    End If

                    'If dtDatos_Expenses.Rows.Count > 0 Then
                    '    For Each dr1 As DataRow In dtDatos_Expenses.Rows
                    '        oBEPurchaseInvoices_Expenses = New clsPurchaseInvoices_ExpensesBE
                    '        oBEPurchaseInvoices_Expenses.ExpenseCode = dr1("ExpenseCode")
                    '        oBEPurchaseInvoices_Expenses.LineTotal = dr1("LineTotal")
                    '        oBEPurchaseInvoices_Expenses.Remarks = dr1("Remarks")
                    '        oBEPurchaseInvoices_Expenses.DistributionMethod = dr1("DistributionMethod")
                    '        oBEPurchaseInvoices_Expenses.TaxCode = dr1("TaxCode")
                    '        oBEPurchaseInvoices_Expenses.DistributionRule = dr1("DistributionRule")
                    '        oBEPurchaseInvoices_Expenses.DistributionRule2 = dr1("DistributionRule2")
                    '        oBEPurchaseInvoices_Expenses.DistributionRule3 = dr1("DistributionRule3").ToString.Trim
                    '        objLstExpenses.Add(oBEPurchaseInvoices_Expenses)
                    '    Next
                    '    oBEPurchaseInvoice.Expenses = objLstExpenses
                    'End If
                    oBEPurchaseInvoice.Expenses = objLstExpenses
                    If dtDatos_WithholdingTaxData.Rows.Count > 0 Then

                        ''ya no se deben de mandar detracciones de una orden de pago a sap
                        'For Each dr1 As DataRow In dtDatos_WithholdingTaxData.Rows
                        '    oBEPurchaseInvoices_WithholdingTaxData = New clsPurchaseInvoices_WithholdingTaxDataBE
                        '    oBEPurchaseInvoices_WithholdingTaxData.WTCode = dr1("WTCode")
                        '    oBEPurchaseInvoices_WithholdingTaxData.WTAmount = dr1("WTAmount")
                        '    oBEPurchaseInvoices_WithholdingTaxData.WTAmountFC = dr1("WTAmountFC")

                        '    objLstWithholdingTaxData.Add(oBEPurchaseInvoices_WithholdingTaxData)
                        'Next
                        'oBEPurchaseInvoice.WithholdingTaxData = objLstWithholdingTaxData


                    End If

                    If objLstLines.Count = 0 Then
                        lstCampos_Borrar.Add("Lines")
                    End If
                    If objLstExpenses.Count = 0 Then
                        lstCampos_Borrar.Add("Expenses")
                    End If

                    If objLstWithholdingTaxData.Count = 0 Then
                        lstCampos_Borrar.Add("WithholdingTaxData")
                    End If

                    intcapacidad = lstCampos_Borrar.Count - 1
                    ReDim arrDelProperty(intcapacidad)

                    For i As Integer = 0 To intcapacidad
                        arrDelProperty.SetValue(lstCampos_Borrar(i), i)
                    Next

                    ' ''prueba
                    ''intcapacidad = 9
                    ''ReDim arrDelProperty(intcapacidad)
                    ' ''arrDelProperty.SetValue("U_SYP_MDTO", 0)
                    ' ''arrDelProperty.SetValue("U_SYP_MDSO", 1)
                    ' ''arrDelProperty.SetValue("U_SYP_MDCO", 2)


                    ''arrDelProperty.SetValue("CashAccount", 0)
                    ''arrDelProperty.SetValue("DiscountPercent", 1)
                    ' '' arrDelProperty.SetValue("DocTotalFc", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDTO", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDSO", 3)
                    ''arrDelProperty.SetValue("U_SYP_MDCO", 4)
                    ''arrDelProperty.SetValue("U_SYP_FECHAREF", 5)
                    ''arrDelProperty.SetValue("U_SYP_NFILE", 6)
                    ''arrDelProperty.SetValue("Lines", 7)
                    ''arrDelProperty.SetValue("Expenses", 8)
                    ''arrDelProperty.SetValue("WithholdingTaxData", 9)
                End With
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub



    Private Function blnProveedorEsInternacional(ByVal vstrIDProveedor As String) As Boolean
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor ", vstrIDProveedor)
            dc.Add("@pbOk", False)

            Return objADO.ExecuteSPOutput("MAPROVEEDOR_SelSiEsOperadorInternacionalOutput", dc)

        Catch ex As Exception

            Throw
        End Try
    End Function
    Private Function strRazSocialUbigeoOficina(ByVal vstrIDProveedor As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor ", vstrIDProveedor)
            dc.Add("@pvcRazonSocial", "")

            Return objADO.ExecuteSPOutput("MAPROVEEDORES_SelRazSocialCoUbigeo_OficinaOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function



    Public Sub ActualizarEstado(ByVal vintIDOrdPag As Integer, _
                                ByVal vstrIDEstado As String, _
                                ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@IDOrdPag", vintIDOrdPag)
                .Add("@IDEstado", vstrIDEstado)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_UpdEstado", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Sub

    Public Sub ActualizarEnviado(ByVal vintIDOrdPag As Integer, _
                               ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@IDOrdPag", vintIDOrdPag)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_Upd_Enviado", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Sub

    Public Function blnExistexIDDet(ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDet", vintIDDet)
                .Add("@pExiste", False)
            End With

            Return objADO.GetSPOutputValue("ORDENPAGO_DET_SelExistexIDDetOutPut", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function blnExiste(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnResultado As Boolean = False
            With dc
                .Add("@IDReserva", vintIDReserva)
                .Add("@pbExiste", False)
            End With

            blnResultado = objADO.GetSPOutputValue("ORDENPAGO_SelExisteOutPut", dc)
            Return blnResultado
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub ActualizarAnulados(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDCab", vintIDCab)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_UpdIDReservaAnulados", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: clsOrdenPagoBT.ActualizarAnulados")
        End Try
    End Sub

    Private Function dttConsultar_Pk(ByVal vintIDOrdPag As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)

            Return objADO.GetDataTable("ORDENPAGO_SelxIDOrdPag", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP(ByVal vintIDOrdPag As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)

            Return objADO.GetDataTable("ORDENPAGO_Sel_List_SAP", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_Lines(ByVal vintIDOrdPag As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)

            Return objADO.GetDataTable("ORDENPAGO_Sel_List_SAP_Lines", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Function drConsultarDatosSAP_WithholdingTaxData(ByVal vintIDOrdPag As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)

            Return objADO.GetDataTable("ORDENPAGO_Sel_List_SAP_WithholdingTaxData", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_PUT(ByVal vintIDOrdPag As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)

            Return objADO.GetDataTable("ORDENPAGO_List_SAP_PUT", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleAdministracionBT
    Inherits clsAdministracionBaseBT

    Private Overloads Sub Insertar(ByVal BE As clsDetalleOrdenPagoBE)
        Dim dc As New Dictionary(Of String, String)
        Try

            Dim dtt As DataTable = ConsultarDetraccion_Detalle(BE.IDOrdPag, BE.IDServicio_Det, BE.Total)
            Dim dblDetraccion As Double = 0
            Dim dblDetraccion_USD As Double = 0
            If dtt.Rows.Count > 0 Then
                dblDetraccion = CDbl(dtt.Rows(0).Item("SSDetraccion"))
                dblDetraccion_USD = CDbl(dtt.Rows(0).Item("SSDetraccion_USD"))
            End If

            With dc
                .Add("@IDOrdPag", BE.IDOrdPag)
                .Add("@IDReserva_Det", BE.IDReserva_Det)
                .Add("@TxDescripcion", BE.TxDescripcion)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@IDTransporte", BE.IDTransporte)
                .Add("@NroPax", BE.NroPax)
                .Add("@NroLiberados", BE.NroLiberados)
                .Add("@Cantidad", BE.Cantidad)
                .Add("@SSDetraccion", dblDetraccion)
                .Add("@SSDetraccion_USD", dblDetraccion_USD)
                .Add("@Noches", BE.Noches)
                .Add("@Total", BE.Total)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_DET_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub


    Private Overloads Sub Actualizar(ByVal BE As clsDetalleOrdenPagoBE)
        Dim dc As New Dictionary(Of String, String)
        Try

            Dim dtt As DataTable = ConsultarDetraccion_Detalle_Edicion(BE.IDOrdPag, BE.IDOrdPag_Det, BE.SsDetraccion)
            Dim dblDetraccion As Double = 0
            Dim dblDetraccion_USD As Double = 0
            If dtt.Rows.Count > 0 Then
                dblDetraccion = CDbl(dtt.Rows(0).Item("SSDetraccion"))
                dblDetraccion_USD = CDbl(dtt.Rows(0).Item("SSDetraccion_USD"))
            End If


            With dc
                .Add("@IDOrdPag_Det", BE.IDOrdPag_Det)
                .Add("@IDOrdPag", BE.IDOrdPag)
                .Add("@IDReserva_Det", BE.IDReserva_Det)
                .Add("@TxDescripcion", BE.TxDescripcion)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@NroPax", BE.NroPax)
                .Add("@NroLiberados", BE.NroLiberados)
                .Add("@Cantidad", BE.Cantidad)
                .Add("@Noches", BE.Noches)
                .Add("@Total", BE.Total)
                .Add("@SSDetraccion", dblDetraccion)
                .Add("@SSDetraccion_USD", dblDetraccion_USD)
                .Add("@SSOtrosDescuentos", BE.SSOtrosDescuentos)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("ORDENPAGO_DET_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintIDOrdPag_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag_Det", vintIDOrdPag_Det)

            objADO.ExecuteSP("ORDENPAGO_DET_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsOrdenPagoBE, ByVal vstrNuevoIDOrdenPago As String)
        If BE.ListDetalleOrdenPago Is Nothing Then Exit Sub
        Try
            For Each ItemDet As clsDetalleOrdenPagoBE In BE.ListDetalleOrdenPago
                If ItemDet.IDOrdPag = 0 Then
                    ItemDet.IDOrdPag = vstrNuevoIDOrdenPago
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function ConsultarDetraccion_Detalle(ByVal vintIDOrdPag As Integer, vintIDServicio_Det As Integer, Optional ByVal vdblTotal_Det As Double = 0) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOrdPag", vintIDOrdPag)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@Total_Det", vdblTotal_Det)

            Return objADO.GetDataTable("ordenpago_Det_Sel_Detraccion", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarDetraccion_Detalle_Edicion(ByVal vintIDOrdPag As Integer, vintIDOrdPag_Det As Integer, Optional ByVal vdblSSDetraccion As Double = 0) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOrdPag", vintIDOrdPag)
            dc.Add("@IDOrdPag_Det", vintIDOrdPag_Det)
            dc.Add("@SSDetraccion", vdblSSDetraccion)

            Return objADO.GetDataTable("ordenpago_Det_Sel_Detraccion_Edicion", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOrdenPagoBE)
        If BE.ListDetalleOrdenPago Is Nothing Then Exit Sub
        Try
            For Each Item As clsDetalleOrdenPagoBE In BE.ListDetalleOrdenPago
                If Item.Accion = "N" Then
                    Insertar(Item)
                    'ActualizarIDsNuevosenHijosST(BE, strIDDebitMemo)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.IDOrdPag_Det)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub GrabarxReserva(ByVal vintIDReserva As Integer, ByVal vintIDOrdPag As Integer, _
                              ByVal vdecPorcentaje As Single, ByVal vdblMonto As Double, _
                              ByVal vsglTCambio As Single, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDOrdPag", vintIDOrdPag)
                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@Porcentaje", vdecPorcentaje)
                dc.Add("@Monto", vdblMonto)
                dc.Add("@TCambio", vsglTCambio)
                dc.Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("ORDENPAGO_DET_InsxIDReserva", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOrdenCompraBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsOrdenCompraBT"

    Public Overloads Function Insertar(ByVal BE As clsOrdenCompraBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoProveedor", BE.CoProveedor)
                .Add("@FeOrdCom", BE.FeOrdCom)
                .Add("@CoMoneda", BE.CoMoneda)
                .Add("@SsImpuestos", BE.SsImpuestos)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@UserMod", BE.UserMod)


                .Add("@pNuOrdComInt", 0)
            End With

            Dim intNuOrdCom As Integer = objADO.ExecuteSPOutput("ORDENCOMPRA_Ins", dc)



            ContextUtil.SetComplete()

            Return intNuOrdCom

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Public Sub Insertar2(ByVal BE As clsOrdenCompraBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoProveedor", BE.CoProveedor)
                .Add("@FeOrdCom", BE.FeOrdCom)
                .Add("@CoMoneda", BE.CoMoneda)
                .Add("@SsImpuestos", BE.SsImpuestos)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@UserMod", BE.UserMod)

                .Add("@CoEstado_OC", BE.CoEstado_OC)
                .Add("@TxDescripcion ", BE.TxDescripcion)
                .Add("@CoArea ", BE.CoArea)
                .Add("@IDContacto ", BE.IDContacto)
                .Add("@CoRubro", BE.CoRubro)
                .Add("@UserSolicitante ", BE.UserSolicitante)
                .Add("@IDAdminist", BE.IDAdminist)
                .Add("@CoCeCos ", BE.CoCeCos)
                .Add("@SSDetraccion", BE.SSDetraccion)
                .Add("@SSIGV ", BE.SSIGV)
                .Add("@SSSubTotal ", BE.SSSubTotal)
                .Add("@CoTipoDetraccion ", BE.CoTipoDetraccion)

                .Add("@pNuOrdComInt", 0)
            End With

            Dim intNuOrdCom As Integer = objADO.ExecuteSPOutput("ORDENCOMPRA_Ins_2", dc)

            '  objADO.ExecuteSP("ORDENCOMPRA_Ins", dc)
            BE.NuOrdComInt = intNuOrdCom

            Dim objDetBT As New clsOrdenCompra_DetBT
            'If BE.IngresoManual Then
            objDetBT.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()

            'Return intNuOrdCom

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsOrdenCompraBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuOrdComInt", BE.NuOrdComInt)
                .Add("@CoEstado_OC", BE.CoEstado_OC)
                .Add("@NuOrdCom", BE.NuOrdCom)
                .Add("@CoProveedor", BE.CoProveedor)
                .Add("@FeOrdCom", BE.FeOrdCom)

                .Add("@CoMoneda", BE.CoMoneda)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@TxDescripcion", BE.TxDescripcion)
                .Add("@CoArea", BE.CoArea)

                .Add("@IDContacto", BE.IDContacto)

                .Add("@CoRubro", BE.CoRubro)
                .Add("@UserSolicitante", BE.UserSolicitante)
                .Add("@IDAdminist", BE.IDAdminist)
                .Add("@CoCeCos", BE.CoCeCos)
                .Add("@SSDetraccion", BE.SSDetraccion)
                .Add("@SSIGV", BE.SSIGV)
                .Add("@SSSubTotal", BE.SSSubTotal)

                .Add("@CoTipoDetraccion ", BE.CoTipoDetraccion)
                '.Add("@SsSaldo", BE.SsSaldo)
                '.Add("@SsDifAceptada", BE.SsDifAceptada)
                '.Add("@TxObsDocumento", BE.TxObsDocumento)
                '.Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)


            End With

            objADO.ExecuteSP("ORDENCOMPRA_Upd", dc)

            Dim objDetBT As New clsOrdenCompra_DetBT
            objDetBT.InsertarActualizarEliminar(BE)

            ''If Not BE.FlDesdeOtraForEgreso Then
            ''    ActualizarFormasEgreso(BE)
            ''End If

            'Dim objBTIti As New clsTicketItinerarioBT
            'objBTIti.EliminarxCoTicket(vstrCoTicket)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuOrdComInt As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdComInt", vintNuOrdComInt)

            Dim objDetBT As New clsOrdenCompra_DetBT
            objDetBT.Eliminar_Todos(vintNuOrdComInt)

            objADO.ExecuteSP("ORDENCOMPRA_Del", dc)



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsOrdenCompraBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuOrdComInt", BE.NuOrdComInt)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("ORDENCOMPRA_UpdxDocumento", dc)

            ''If Not BE.FlDesdeOtraForEgreso Then
            ''    ActualizarFormasEgreso(BE)
            ''End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintNuOrdComInt As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdComInt", vintNuOrdComInt)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("ORDENCOMPRA_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub
End Class


'JORGE
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsFondoFijoBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsFondoFijoBT"

    Public Overloads Sub Insertar(ByVal BE As clsFondoFijoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@CoPrefijo", BE.CoPrefijo)
                .Add("@CoCeCos", BE.CoCeCos)
                .Add("@CtaContable", BE.CtaContable)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@CoUserResponsable", BE.CoUserResponsable)
                .Add("@CoMoneda", BE.CoMoneda)
                .Add("@SSMonto", BE.SSMonto)
                .Add("@CoTipoFondoFijo", BE.CoTipoFondoFijo)
                .Add("@UserMod", BE.UserMod)

            End With

            'Dim intNuOrdCom As Integer = objADO.ExecuteSPOutput("MAFONDOFIJO_Ins", dc)

            objADO.ExecuteSP("MAFONDOFIJO_Ins", dc)

            ContextUtil.SetComplete()

            ' Return intNuOrdCom

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub
    '    @FeEmision_Reembolso smalldatetime='01/01/1900',
    '@CoTipoDoc_Reembolso char(3)='',
    '@NuDocum_Reembolso varchar(30)='',
    Public Sub Reembolsar(vintNuFondoFijo As Integer, vstrFeEmision_Reembolso As String, vstrCoTipoDoc_Reembolso As String, vstrNuDocum_Reembolso As String, vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@NuFondoFijo_Origen", vintNuFondoFijo)
                .Add("@FeEmision_Reembolso", vstrFeEmision_Reembolso)
                .Add("@CoTipoDoc_Reembolso", vstrCoTipoDoc_Reembolso)
                .Add("@NuDocum_Reembolso", vstrNuDocum_Reembolso)
                .Add("@UserMod", vstrUserMod)

            End With

            'Dim intNuOrdCom As Integer = objADO.ExecuteSPOutput("MAFONDOFIJO_Ins", dc)

            objADO.ExecuteSP("MAFONDOFIJO_Reembolsar", dc)

            ContextUtil.SetComplete()

            ' Return intNuOrdCom

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Reembolsar")
        End Try
    End Sub


    Public Overloads Sub Actualizar(ByVal BE As clsFondoFijoBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc

                .Add("@NuFondoFijo", BE.NuFondoFijo)
                .Add("@NuCodigo", BE.NuCodigo)
                .Add("@CoCeCos", BE.CoCeCos)
                .Add("@CtaContable", BE.CtaContable)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@CoUserResponsable", BE.CoUserResponsable)
                .Add("@CoMoneda", BE.CoMoneda)
                .Add("@SSMonto", BE.SSMonto)
                .Add("@CoPrefijo", BE.CoPrefijo)
                .Add("@CoTipoFondoFijo", BE.CoTipoFondoFijo)
                .Add("@UserMod", BE.UserMod)

            End With

            objADO.ExecuteSP("MAFONDOFIJO_Upd", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar_Saldo_Pres(ByVal vintNuPreSob As Integer, ByVal vstrUser As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc

                .Add("@NuPreSob", vintNuPreSob)
                .Add("@UserMod", vstrUser)

            End With

            objADO.ExecuteSP("MAFONDOFIJO_Upd_Saldo_Presupuesto", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar_Saldo_Pres")
            Throw
        End Try
    End Sub

    Public Sub Actualizar_Activo(vintNuFondoFijo As Integer, vblnFlActivo As Boolean, vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc

                .Add("@NuFondoFijo", vintNuFondoFijo)
                .Add("@FlActivo", vblnFlActivo)
                .Add("@UserMod", vstrUserMod)

            End With

            objADO.ExecuteSP("MAFONDOFIJO_Upd_Activo", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar_Activo")
            Throw
        End Try
    End Sub

    Public Sub Actualizar_Activo2(ByVal BE As clsFondoFijoBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc

                .Add("@NuFondoFijo", BE.NuFondoFijo)
                .Add("@FlActivo", BE.FlActivo)
                .Add("@UserMod", BE.UserMod)

            End With

            objADO.ExecuteSP("MAFONDOFIJO_Upd_Activo", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar_Activo2")
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuFondoFijo As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuFondoFijo", vintNuFondoFijo)


            objADO.ExecuteSP("MAFONDOFIJO_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsFondoFijoBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuFondoFijo", BE.NuFondoFijo)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("MAFONDOFIJO_UpdxDocumento", dc)

            ''If Not BE.FlDesdeOtraForEgreso Then
            ''    ActualizarFormasEgreso(BE)
            ''End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintNuFondoFijo As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuFondoFijo", vintNuFondoFijo)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("MAFONDOFIJO_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsOrdenCompra_DetBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsOrdenCompra_DetBT"

    Public Overloads Sub Insertar(ByVal BE As clsOrdenCompra_DetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc


                '               	@NuOrdComInt int,
                '@TxServicio varchar(max),
                '@SSCantidad numeric(8,2),
                '@SSPrecUnit numeric(8,2),
                '@SSTotal numeric(8,2),
                '@UserMod char(4)

                .Add("@NuOrdComInt", BE.NuOrdComInt)
                .Add("@TxServicio", BE.TxServicio)
                .Add("@SSCantidad", BE.SSCantidad)
                .Add("@SSPrecUnit", BE.SSPrecUnit)
                .Add("@SsTotal", BE.SSTotal)
                .Add("@UserMod", BE.UserMod)

                '----

                '---

                '.Add("@pNuOrdComInt", 0)
            End With

            objADO.ExecuteSP("ORDENCOMPRA_DET_Ins", dc)

            ' Dim intNuOrdCom As Integer = objADO.ExecuteSPOutput("ORDENCOMPRA_DET_Ins", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsOrdenCompra_DetBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc


                '               	@NuOrdComInt_Det int,
                '@NuOrdComInt int,
                '@TxServicio varchar(max),
                '@SSCantidad numeric(8,2),
                '@SSPrecUnit numeric(8,2),
                '@SSTotal numeric(8,2),
                '@UserMod char(4)

                .Add("@NuOrdComInt", BE.NuOrdComInt)
                .Add("@NuOrdComInt_Det", BE.NuOrdComInt_Det)
                .Add("@TxServicio", BE.TxServicio)
                .Add("@SSCantidad", BE.SSCantidad)
                .Add("@SSPrecUnit", BE.SSPrecUnit)
                .Add("@SSTotal", BE.SSTotal)


                .Add("@UserMod", BE.UserMod)


            End With

            objADO.ExecuteSP("ORDENCOMPRA_DET_Upd", dc)

            ''If Not BE.FlDesdeOtraForEgreso Then
            ''    ActualizarFormasEgreso(BE)
            ''End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuOrdComInt_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdComInt_Det", vintNuOrdComInt_Det)

            objADO.ExecuteSP("ORDENCOMPRA_DET_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Public Overloads Sub Eliminar_Todos(ByVal vintNuOrdComInt As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdComInt", vintNuOrdComInt)

            objADO.ExecuteSP("ORDENCOMPRA_DET_Del_Todos", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsOrdenCompraBE)
        If BE.ListaDetalle Is Nothing Then Exit Sub
        Try
            For Each Item As clsOrdenCompra_DetBE In BE.ListaDetalle
                If Item.Accion = "N" Then
                    'If Not BE.IngresoManual Then
                    '    Item.CoPNR_Amadeus = BE.CoPNR_Amadeus
                    '    Item.CoPNR_Aerolinea = BE.CoPNR_Aerolinea
                    'End If
                    Item.NuOrdComInt = BE.NuOrdComInt
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                    ' ActualizarVueloSxTickets(BE, Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.NuOrdComInt_Det)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsVoucherOficinaExternaBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsVoucherOficinaExternaBT"

    Public Overloads Function Insertar(ByVal BE As clsVoucherOficinaExternaBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", BE.IdCab)
                .Add("@CoProveedor", BE.CoProveedor)
                .Add("@FeVoucher", BE.FeVoucher)
                .Add("@CoMoneda", BE.CoMoneda)
                .Add("@SsImpuestos", BE.SsImpuestos)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@CoUbigeo_Oficina", BE.CoUbigeo_Oficina)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuVouOfiExtInt", 0)
            End With

            Dim intNuVouOfiExtInt As Integer = objADO.ExecuteSPOutput("VOUCHER_OFICINA_EXTERNA_Ins", dc)
            ContextUtil.SetComplete()

            Return intNuVouOfiExtInt

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try

    End Function

    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsVoucherOficinaExternaBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuVouOfiExtInt", BE.NuVouOfiExtInt)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)

            End With

            objADO.ExecuteSP("VOUCHER_OFICINA_EXTERNA_UpdxDocumento", dc)

            ''If Not BE.FlDesdeOtraForEgreso Then
            ''    ActualizarFormasEgreso(BE)
            ''End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintNuVouOfiExtInt As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVouOfiExtInt", vintNuVouOfiExtInt)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("VOUCHER_OFICINA_EXTERNA_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsVoucherProveedorTrenBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsVoucherProveedorTrenBT"

    Public Overloads Function Insertar(ByVal BE As clsVoucherProveedorTrenBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", BE.IdCab)
                .Add("@CoProveedor", BE.CoProveedor)
                .Add("@FeVoucher", BE.FeVoucher)
                .Add("@CoMoneda", BE.CoMoneda)
                .Add("@SsImpuestos", BE.SsImpuestos)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@CoUbigeo_Oficina", BE.CoUbigeo_Oficina)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuVouPTrenInt", 0)
            End With

            Dim intNuVouPTrenInt As Integer = objADO.ExecuteSPOutput("VOUCHER_PROV_TREN_Ins", dc)
            ContextUtil.SetComplete()

            Return intNuVouPTrenInt

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try

    End Function

    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsVoucherProveedorTrenBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuVouPTrenInt", BE.NuVouPTrenInt)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)

            End With

            objADO.ExecuteSP("VOUCHER_PROV_TREN_UpdxDocumento", dc)

            ''If Not BE.FlDesdeOtraForEgreso Then
            ''    ActualizarFormasEgreso(BE)
            ''End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintNuVouPTrenInt As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVouPTrenInt", vintNuVouPTrenInt)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("VOUCHER_PROV_TREN_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub
    Public Overloads Sub ActualizarNuVouPTrenIntenOperaciones(ByVal vintIDOperacion As Integer, ByVal vintNuVouPTrenInt As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion", vintIDOperacion)
            dc.Add("@NuVouPTrenInt", vintNuVouPTrenInt)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_UpdNuVouPTrenInt", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNuVouPTrenIntenOperaciones")
        End Try
    End Sub
    Public Overloads Sub ActualizarNuVouPTrenIntenOperacionesxProvee(ByVal vstrCoProveedor As String, _
                                                                     ByVal vintIDCab As Integer, _
                                                                     ByVal vintNuVouPTrenInt As Integer, _
                                                                     ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@NuVouPTrenInt", vintNuVouPTrenInt)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("OPERACIONES_UpdNuVouPTrenIntxProvee", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNuVouPTrenIntenOperacionesxProvee")
        End Try
    End Sub

    Public Overloads Sub ActualizarTotal(ByVal vintNuVouPTrenInt As Integer, ByVal vdblSSTotal As Double, _
                                         ByVal vdblSSImpuestos As Double, _
                                         ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVouPTrenInt", vintNuVouPTrenInt)
            dc.Add("@SSTotal", vdblSSTotal)
            dc.Add("@SSImpuestos", vdblSSImpuestos)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("VOUCHER_PROV_TREN_UpdSsTotal", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarTotal")
        End Try
    End Sub

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDebitMemoBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsDebitMemoBT"
    'Public pstrCoClienteVentasAdicAPT As String
    Public pstrIDClienteAPT As String

    Public Overloads Function Insertar(ByVal BE As clsDebitMemoBE) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                '.Add("@IDSerie", BE.IDSerie)
                .Add("@Fecha", BE.Fecha)
                .Add("@FecVencim", BE.FecVencim)
                .Add("@IDCab", BE.IDCab)
                '.Add("@IDFile", BE.IDFile)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)
                .Add("@IDBanco", BE.IDBanco)
                .Add("@IDEstado", BE.IDEstado)
                .Add("@IDTipo", BE.IDTipo)

                .Add("@FechaIn", BE.FechaIn)
                .Add("@FechaOut", BE.FechaOut)
                .Add("@Cliente", BE.Cliente)
                .Add("@Pax", BE.Pax)
                .Add("@Titulo", BE.Titulo)
                .Add("@Responsable", BE.Responsable)
                .Add("@FlImprimioDebit", BE.FlImprimioDebit)
                .Add("@FlManual", BE.FlManual)
                .Add("@IDCliente", BE.IDCliente)
                .Add("@UserMod", BE.UserMod)

                .Add("@IDMotivo", BE.IDMotivo)
                .Add("@IDResponsable", BE.IDResponsable)
                .Add("@IDSupervisor", BE.IDSupervisor)
                .Add("@TxObservacion", BE.TxObservacion)
                .Add("@pIDDebitMemo", "")
            End With

            Dim strIDDebitMemo As String = objADO.ExecuteSPOutput("DEBIT_MEMO_Ins", dc)
            If BE.IDTipo = "I" Then
                ActualizarIDsNuevosenHijosST(BE, strIDDebitMemo)

                Dim objBTDet As New clsDetalleDebitMemoBT
                objBTDet.InsertarActualizarEliminar(BE)
            ElseIf BE.IDTipo = "R" Then
                ActualizarIDDebitMemoResumen(BE, strIDDebitMemo)
            End If

            'If BE.IDCab <> 0 And BE.Total <> 0 And BE.Cliente <> "VENTAS ADICIONALES APT" Then
            If BE.IDCab <> 0 And BE.Total <> 0 And (BE.IDTipo <> "A" And BE.IDCliente <> pstrIDClienteAPT) Then
                Dim objDocBT As New clsDocumentoBT
                Dim strEstadoFactura As String = objDocBT.strDevuelveEstadoFacturac(BE.IDCab)
                If strEstadoFactura = "F" Or strEstadoFactura = "A" Then 'Facturado
                    InsertarDocumentoxDebitMemo(BE, True)
                End If
            End If

            ContextUtil.SetComplete()
            Return strIDDebitMemo
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function
    Private Function ListConsultarDocsparaVinculo(ByVal vintIDCab As Integer, ByVal vstrIDTipoDocNota As String, ByRef rdblTotalNota As Double, _
                                                  ByRef rdblTotalImpuestos As Double) As List(Of clsDocumentoBE)
        Dim dc As New Dictionary(Of String, String)

        Dim dblTotalVincAdicional As Double = 0
        Dim dblTotalImpuestosAdicional As Double = 0

        Dim strNuDocumVinc As String = ""
        Dim datFechaDocumVinc As Date = "01/01/1900"
        Dim strIDTipoDocVinc As String = ""
        Dim strIDTipoDoc As String = ""


        Dim ListaDocsVinc As New List(Of clsDocumentoBE)
        Try
            dc.Add("@IDCab", vintIDCab)

            Dim dttDocs As DataTable = objADO.GetDataTable("DOCUMENTO_SelxIDCab", dc)
            Dim dttBol As DataTable = dtFiltrarDataTable(dttDocs, "IDTipoDoc='BOL' And SerieDoc='001'")
            If dttBol.Rows.Count > 0 Then
                strNuDocumVinc = dttBol(0)("NuDocum")
                datFechaDocumVinc = dttBol(0)("FeDocum")
                strIDTipoDocVinc = dttBol(0)("IDTipoDoc")
                strIDTipoDoc = vstrIDTipoDocNota


                Dim ItemDoc As New clsDocumentoBE With {.NuDocumVinc = strNuDocumVinc, _
                                                        .FeEmisionVinc = datFechaDocumVinc, _
                                                        .IDTipoDocVinc = strIDTipoDocVinc, _
                                                        .IDTipoDoc = strIDTipoDoc}
                ListaDocsVinc.Add(ItemDoc)

                If rdblTotalNota > Convert.ToDouble(dttBol(0)("SsTotalDocumUSD")) And vstrIDTipoDocNota = "NCR" Then

                    dblTotalVincAdicional = rdblTotalNota - Convert.ToDouble(dttBol(0)("SsTotalDocumUSD"))
                    dblTotalImpuestosAdicional = rdblTotalImpuestos - Convert.ToDouble(dttBol(0)("SsIGV"))
                    rdblTotalNota = Convert.ToDouble(dttBol(0)("SsTotalDocumUSD"))
                    rdblTotalImpuestos = Convert.ToDouble(dttBol(0)("SsIGV"))


                    Dim strIDTipoDocVincAdic As String = ""
                    Dim strSerieDocAdic As String = ""
                    Dim strNuDocumVincAdic As String = ""
                    Dim datFechaDocumVincAdic As Date = "01/01/1900"

                    Dim dttBLE As DataTable = dtFiltrarDataTable(dttDocs, "IDTipoDoc='BLE' And SerieDoc='006'")
                    If dttBLE.Rows.Count > 0 Then
                        strIDTipoDocVincAdic = "BLE"
                        strSerieDocAdic = "006"
                        strNuDocumVincAdic = dttBLE(0)("NuDocum")
                        datFechaDocumVincAdic = dttBLE(0)("FeDocum")
                    End If
                    If strIDTipoDocVincAdic = "" Then

                        Dim dttFac As DataTable = dtFiltrarDataTable(dttDocs, "IDTipoDoc='FAC' And SerieDoc='001'")
                        If dttFac.Rows.Count > 0 Then
                            strIDTipoDocVincAdic = "FAC"
                            strSerieDocAdic = "001"
                            strNuDocumVincAdic = dttFac(0)("NuDocum")
                            datFechaDocumVincAdic = dttFac(0)("FeDocum")
                        End If
                    End If
                    If strIDTipoDocVincAdic <> "" Then
                        Dim ItemDocAdic As New clsDocumentoBE With {.NuDocumVinc = strNuDocumVincAdic, _
                                                .FeEmisionVinc = datFechaDocumVincAdic, _
                                                .IDTipoDocVinc = strIDTipoDocVincAdic, _
                                                .CoSerie = strSerieDocAdic, _
                                                .SsTotalDocumUSD = dblTotalVincAdicional, _
                                                .SsIGV = dblTotalImpuestosAdicional, _
                                                .IDTipoDoc = strIDTipoDoc}
                        ListaDocsVinc.Add(ItemDocAdic)
                    End If
                End If
            Else 'No se encontró Boleta a vincular a la NDB, NCR

                'strNuDocumVinc = ""
                'datFechaDocumVinc = "01/01/1900"
                'strIDTipoDocVinc = ""
                'strIDTipoDoc = "BOL"


                'Dim ItemDoc As New clsDocumentoBE With {.NuDocumVinc = strNuDocumVinc, _
                '                                        .FeEmisionVinc = datFechaDocumVinc, _
                '                                        .IDTipoDocVinc = strIDTipoDocVinc, _
                '                                        .IDTipoDoc = strIDTipoDoc}
                'ListaDocsVinc.Add(ItemDoc)
            End If

            Return ListaDocsVinc
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ConsultarDocsparaVinculo")
        End Try
    End Function
    Private Sub InsertarDocumentoxDebitMemo(ByVal BE As clsDebitMemoBE, ByVal vblnDMManual As Boolean)
        If BE.Total = 0 Then Exit Sub
        If BE.IDCab = 0 Then Exit Sub


        Try
            ActualizarDebitMemoxAjustePrecio(BE.IDDebitMemo, True, BE.UserMod)

            Dim blnDosDocumentos As Boolean = False
            Dim ListaTipoDocs As New List(Of String)
            '.IDTipoDoc = If(BE.Total > 0, "BOL", "NCR"), _
            Dim ListaDoc As New List(Of clsDocumentoBE)
            If vblnDMManual Then
                Dim strNuDocumVinc As String = ""
                Dim datFechaDocumVinc As Date = "01/01/1900"
                Dim strIDTipoDocVinc As String = ""
                Dim strIDTipoDoc As String = If(BE.Total > 0, "NDB", "NCR")
                Dim strSerie As String = "001"

                Dim ListaDocVinc As New List(Of clsDocumentoBE)

                Dim dblTotal As Double
                Dim dblTotalIGV As Double
                If BE.Total < 0 Then
                    dblTotal = BE.Total * (-1)
                    dblTotalIGV = BE.TotalIGV * (-1)
                    ListaDocVinc = ListConsultarDocsparaVinculo(BE.IDCab, "NCR", dblTotal, dblTotalIGV)
                Else
                    dblTotal = BE.Total
                    dblTotalIGV = BE.TotalIGV
                    ListaDocVinc = ListConsultarDocsparaVinculo(BE.IDCab, "NDB", dblTotal, dblTotalIGV)
                End If


                For Each ItemDoc As clsDocumentoBE In ListaDocVinc
                    If ItemDoc.IDTipoDocVinc = "BOL" Then
                        strNuDocumVinc = ItemDoc.NuDocumVinc
                        datFechaDocumVinc = ItemDoc.FeEmisionVinc
                        strIDTipoDocVinc = ItemDoc.IDTipoDocVinc
                        strIDTipoDoc = ItemDoc.IDTipoDoc

                        Exit For
                    End If
                Next
                If strNuDocumVinc = "" Then 'And strIDTipoDoc = "NDB" Then
                    strIDTipoDoc = "BOL"
                End If

                '.IDTipoDoc = If(BE.Total > 0, "NDB", "NCR"), _
                '.CoSerie = "001", _
                '.SsTotalDocumUSD = If(BE.Total > 0, BE.Total, BE.Total * (-1)), _
                '.SsIGV = If(BE.Total > 0, BE.TotalIGV, BE.TotalIGV * (-1)), _

                Dim ItemDocum As New clsDocumentoBE With { _
                        .NuDocum = 0, _
                        .IDTipoDoc = strIDTipoDoc, _
                        .CoSerie = strSerie, _
                        .IDCab = BE.IDCab, _
                        .FeDocum = BE.Fecha, _
                        .IDMoneda = BE.IDMoneda, _
                        .SsTotalDocumUSD = dblTotal, _
                        .SsIGV = dblTotalIGV, _
                        .Generado = False, _
                        .TxTitulo = "", _
                        .NuDocumVinc = strNuDocumVinc, _
                        .FlPorAjustePrecio = True, _
                        .CoTipoVenta = "RC", _
                        .IDCliente = BE.IDCliente, _
                        .IDTipoDocVinc = strIDTipoDocVinc, _
                        .FeEmisionVinc = datFechaDocumVinc, _
                        .CoCtaContable = "704112", _
                        .CoCtroCosto = "", _
                        .NuFileLibre = "", _
                        .blnSAP = False, _
                        .UserMod = BE.UserMod _
                    }
                ListaDoc.Add(ItemDocum)

                If ListaDocVinc.Count = 2 And BE.Total < 0 Then 'Nota Cred adicionales por exceder monto de boleta
                    For Each ItemDoc As clsDocumentoBE In ListaDocVinc
                        If ItemDoc.IDTipoDocVinc <> "BOL" Then
                            '.CoSerie = "001", _
                            Dim ItemDocum2 As New clsDocumentoBE With { _
                               .NuDocum = 0, _
                               .IDTipoDoc = "NCR", _
                               .CoSerie = ItemDoc.CoSerie, _
                               .IDCab = BE.IDCab, _
                               .FeDocum = BE.Fecha, _
                               .IDMoneda = BE.IDMoneda, _
                               .SsTotalDocumUSD = ItemDoc.SsTotalDocumUSD, _
                               .SsIGV = ItemDoc.SsIGV, _
                               .Generado = False, _
                               .TxTitulo = "", _
                               .NuDocumVinc = ItemDoc.NuDocumVinc, _
                               .FlPorAjustePrecio = True, _
                               .CoTipoVenta = "RC", _
                               .IDCliente = BE.IDCliente, _
                               .IDTipoDocVinc = ItemDoc.IDTipoDocVinc, _
                               .FeEmisionVinc = ItemDoc.FeEmisionVinc, _
                               .CoCtaContable = "704112", _
                               .CoCtroCosto = "", _
                               .NuFileLibre = "", _
                               .blnSAP = False, _
                               .UserMod = BE.UserMod _
                           }
                            ListaDoc.Add(ItemDocum2)

                            Exit For
                        End If
                    Next
                End If

            Else
                Dim dtt As DataTable = ConsultarDMPendientes(BE.IDCab)
                If dtt.Rows.Count >= 2 Then '2 ó 3
                    blnDosDocumentos = True
                End If
                Dim ItemDocum As New clsDocumentoBE
                '"NDB", "001"
                For Each dr As DataRow In dtt.Rows
                    ItemDocum = New clsDocumentoBE With { _
                        .NuDocum = 0, _
                        .IDTipoDoc = dr("IDTipoDoc"), _
                        .CoSerie = dr("SerieDoc"), _
                        .IDCab = BE.IDCab, _
                        .FeDocum = BE.Fecha, _
                        .IDMoneda = BE.IDMoneda, _
                        .SsTotalDocumUSD = If(dr("Total") > 0, dr("Total"), dr("Total") * (-1)), _
                        .SsIGV = If(dr("Total") > 0, dr("TotalImpto"), dr("TotalImpto") * (-1)), _
                        .Generado = False, _
                        .TxTitulo = "", _
                        .NuDocumVinc = dr("NuDocumVinc"), _
                        .FlPorAjustePrecio = True, _
                        .CoTipoVenta = "RC", _
                        .IDCliente = BE.IDCliente, _
                        .IDTipoDocVinc = dr("IDTipoDocVinc"), _
                        .FeEmisionVinc = dr("FeDocumVinc"), _
                        .CoCtaContable = "704112", _
                        .CoCtroCosto = "", _
                        .NuFileLibre = "", _
                        .blnSAP = False, _
                        .UserMod = BE.UserMod _
                    }
                    ListaDoc.Add(ItemDocum)

                    If Not ListaTipoDocs.Contains(dr("IDTipoDoc")) Then
                        ListaTipoDocs.Add(dr("IDTipoDoc"))
                    End If
                Next
            End If

            Dim BEDocs As New clsDocumentoBE With {.ListaDocumentos = ListaDoc}
            Dim objDocBT As New clsDocumentoBT
            objDocBT.InsertarDocumentosGenerados(0, BEDocs, BE.UserMod, False)
            objDocBT.ActualizarEstadoFacturacionFile(BE.IDCab, "A", BE.UserMod) 'Ajuste de Precio

            ''''
            If blnDosDocumentos Then
                ActualizarTotalesDocumxAjustePrecio(BE.IDCab, BE.IDDebitMemo, BE.UserMod)
            Else
                If vblnDMManual Then
                    'If(BE.Total > 0, "NDB", "NCR")
                    ActualizarDocumFlagUpdMontosxAjustePrecio(BE.IDCab, BE.IDDebitMemo, If(BE.Total > 0, "NDB", "NCR"), BE.UserMod)
                Else
                    'ActualizarDocumFlagUpdMontosxAjustePrecio(BE.IDCab, BE.IDDebitMemo, "NDB", BE.UserMod)
                    For Each strTipoDoc As String In ListaTipoDocs
                        ActualizarDocumFlagUpdMontosxAjustePrecio(BE.IDCab, BE.IDDebitMemo, strTipoDoc, BE.UserMod)
                    Next

                End If
            End If


            'ActualizarDebitMemoxAjustePrecio(BE.IDDebitMemo, True, BE.UserMod)
            ActualizarDebitMemoxAjustePrecioenDocNota(BE.IDDebitMemo, True, BE.UserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub
    Private Function ConsultarDMPendientes(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("COTIDET_SelDMPendientes", dc)

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ConsultarDMPendientes")
        End Try
    End Function
    Private Sub ActualizarDebitMemoxAjustePrecio(ByVal vstrIDDebitMemo As String, _
                                                 ByVal vblnFlPorAjustePrecio As Boolean, _
                                                 ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDDebitMemo", vstrIDDebitMemo)
                dc.Add("@FlPorAjustePrecio", vblnFlPorAjustePrecio)
                dc.Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("DEBIT_MEMO_UpdPorAjustePrecio", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub ActualizarDebitMemoxAjustePrecioenDocNota(ByVal vstrIDDebitMemo As String, _
                                                 ByVal vblnFlAjustePrecioenDocNota As Boolean, _
                                                 ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDDebitMemo", vstrIDDebitMemo)
                dc.Add("@FlAjustePrecioenDocNota", vblnFlAjustePrecioenDocNota)
                dc.Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("DEBIT_MEMO_UpdFlAjustePrecioenDocNota", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub ActualizarTotalesDocumxAjustePrecio(ByVal vintIDCab As Integer, _
                                                    ByVal vstrIDDebitMemo As String, _
                                             ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDDebitMemo", vstrIDDebitMemo)
                dc.Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_UpdTotalesAjustePrecio", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub ActualizarDocumFlagUpdMontosxAjustePrecio(ByVal vintIDCab As Integer, _
                                                        ByVal vstrIDDebitMemo As String, _
                                                        ByVal vstrIDTipoDoc As String, _
                                                 ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDDebitMemo", vstrIDDebitMemo)
                dc.Add("@IDTipoDoc", vstrIDTipoDoc)
                dc.Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_UpdFlAjustePrecioUpdMontos", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsDebitMemoBE, ByVal vstrNuevoIDDebitMemo As String)
        If BE.ListaDetalleDebitMemo Is Nothing Then Exit Sub
        Try
            For Each ItemDet As clsDetalleDebitMemoBE In BE.ListaDetalleDebitMemo
                If ItemDet.IDDebitMemo = "" Then
                    ItemDet.IDDebitMemo = vstrNuevoIDDebitMemo
                End If
            Next
            BE.IDDebitMemo = vstrNuevoIDDebitMemo
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsAsignacionBE, ByVal vstrNuevoIDDebitMemo As String, _
                                                       ByVal vstrAntiguoIDDebitMemo As String)
        If BE.ListaIngresoDebitMemo Is Nothing Then Exit Sub
        Try
            For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
                If Item.IDDebitMemo = vstrAntiguoIDDebitMemo Then
                    Item.IDDebitMemo = vstrNuevoIDDebitMemo
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsAsignacionBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            For Each Item As clsDebitMemoBE In BE.ListaDebitMemo
                If Item.Accion = "N" Then
                    Dim strIDDebitMemoNuevo As String = Insertar(Item)
                    ActualizarIDsNuevosenHijosST(BE, strIDDebitMemoNuevo, Item.IDDebitMemo)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsDebitMemoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDebitMemo", BE.IDDebitMemo)
                '.Add("@IDSerie", BE.IDSerie)
                .Add("@FecVencim", BE.FecVencim)
                .Add("@IDCab", BE.IDCab)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)
                .Add("@IDBanco", BE.IDBanco)
                .Add("@IDEstado", BE.IDEstado)
                .Add("@IDTipo", BE.IDTipo)
                .Add("@FechaIn", BE.FechaIn)
                .Add("@FechaOut", BE.FechaOut)
                .Add("@Cliente", BE.Cliente)
                .Add("@Pax", BE.Pax)
                .Add("@Titulo", BE.Titulo)
                .Add("@IDCliente", BE.IDCliente)
                .Add("@Responsable", BE.Responsable)
                .Add("@UserMod", BE.UserMod)
                .Add("@IDMotivo", BE.IDMotivo)
                .Add("@IDResponsable", BE.IDResponsable)
                .Add("@IDSupervisor", BE.IDSupervisor)
                .Add("@TxObservacion", BE.TxObservacion)
            End With

            objADO.ExecuteSP("DEBIT_MEMO_Upd", dc)

            If BE.IDTipo = "I" Then
                Dim objBTDet As New clsDetalleDebitMemoBT
                objBTDet.InsertarActualizarEliminar(BE)
            ElseIf BE.IDTipo = "R" Then
                ActualizarIDDebitMemoResumen(BE)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarTotales(ByVal vstrIDDebitMemo As String, _
                                           ByVal vdblSubTotal As Double, _
                                           ByVal vdblTotalIGV As Double, _
                                           ByVal vdblTotal As Double, _
                                           ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDebitMemo", vstrIDDebitMemo)
                .Add("@SubTotal", vdblSubTotal)
                .Add("@TotalIGV", vdblTotalIGV)
                .Add("@Total", vdblTotal)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("DEBIT_MEMO_UpdTotales", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub ActualizarImpresionFlag(ByVal vstrIDDebitMemo As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("DEBIT_MEMO_UpdImprimir", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub ActualizarCotizFlagDebitMemo(ByVal vListIDDet As List(Of Integer), ByVal vstrIDDebitMemo As String, _
                                           ByVal vblnDebitMemoPendiente As Boolean, ByVal vstrUserMod As String)

        Try
            For Each intIDDet As Integer In vListIDDet
                Dim dc As New Dictionary(Of String, String)

                With dc
                    .Add("@IDDet", intIDDet)
                    .Add("@IDDebitMemo", vstrIDDebitMemo)
                    .Add("@DebitMemoPendiente", vblnDebitMemoPendiente)
                    .Add("@UserMod", vstrUserMod)
                End With

                objADO.ExecuteSP("COTIDET_UpdDebitMemoPendiente", dc)

            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarEstado(ByVal vstrIDDebitMemo As String, ByVal vstrIDEstado As String, _
                                          ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("DEBIT_MEMO_UpdEstado", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarSaldo(ByVal vstrIDDebitMemo As String, ByVal vdblSaldo As Double, _
                                          ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@Saldo", vdblSaldo)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("DEBIT_MEMO_UpdSaldo", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarIDDebitMemoResumen(ByVal BE As clsDebitMemoBE, ByVal vstrIDDebitMemoResumen As String)
        Dim dc As Dictionary(Of String, String)
        Try
            For Each Item As clsDebitMemoBE In BE.ListaDebitMemo
                dc = New Dictionary(Of String, String)
                dc.Add("@IDDebitMemoResumen", vstrIDDebitMemoResumen)
                dc.Add("@IDDebitMemo", Item.IDDebitMemo)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("DEBIT_MEMO_UpdDebitMemoResumen", dc)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarIDDebitMemoResumen(ByVal BE As clsDebitMemoBE)
        Dim dc As Dictionary(Of String, String)
        Try
            For Each Item As clsDebitMemoBE In BE.ListaDebitMemo
                dc = New Dictionary(Of String, String)
                dc.Add("@IDDebitMemoResumen", Item.DebitMemoResumen)
                dc.Add("@IDDebitMemo", Item.IDDebitMemo)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("DEBIT_MEMO_UpdDebitMemoResumen", dc)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Anular(ByVal vintIDCab As Integer, ByVal vstrIDDebitMemo As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim ListIddets As List(Of Integer) = LstConsultarIDDetsxIDCab(vintIDCab, vstrIDDebitMemo)
            ActualizarCotizFlagDebitMemo(ListIddets, "", True, vstrUserMod)

            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("DEBIT_MEMO_Anu", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function datFechaVencimiento(ByVal vdatFecInicio As Date, ByVal vstrIDCliente As String, _
                                         ByVal vintNroPax As Int16, ByVal vintNroLiberados As Int16) As Date
        'Try
        '    Dim datFecVencim As Date
        '    Dim objCliBN As New clsClienteBN
        '    Using drCli As SqlClient.SqlDataReader = objCliBN.ConsultarPk(vstrIDCliente)
        '        Dim blnCredito As Boolean = False
        '        drCli.Read()
        '        If drCli.HasRows Then
        '            blnCredito = drCli("Credito")
        '        End If

        '        If blnCredito Then
        '            Dim datMesSgte As Date = DateAdd(DateInterval.Month, 1, vdatFecInicio)
        '            datFecVencim = DateSerial(Year(datMesSgte), Month(datMesSgte), 15)
        '        Else
        '            If (vintNroPax + vintNroLiberados) > 8 Then
        '                datFecVencim = DateAdd(DateInterval.Day, -20, vdatFecInicio)
        '            Else
        '                datFecVencim = DateAdd(DateInterval.Day, -15, vdatFecInicio)
        '            End If
        '        End If
        '        drCli.Close()
        '    End Using
        '    Return datFecVencim
        'Catch ex As Exception
        '    Throw
        'End Try
    End Function
    Private Function LstConsultarIDDetsxIDCab(ByVal vintIDCab As Integer, ByVal vstrIDDebitMemo As String) As List(Of Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim LstIddets As New List(Of Integer)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            Dim dtt As DataTable = objADO.GetDataTable("COTIDET_SelxIDDebitMemo", dc)
            For Each drow As DataRow In dtt.Rows
                LstIddets.Add(CInt(drow("IDDET")))
            Next
            Return LstIddets
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub CargarListaDetDebitMemo(ByVal BECot As clsCotizacionBE, ByVal vstrCapacidad As String, _
                                             ByVal vbytCapacidad As Byte, _
                                             ByVal vsglIgv As Single, _
                                             ByVal vstrIDDebitMemo As String, _
                                             ByRef rListaDetDebMem As List(Of clsDetalleDebitMemoBE))
        If vbytCapacidad = 0 Then Exit Sub
        Try
            Dim dblTotal As Double = 0, dblTotalIGV As Double = 0
            Dim dblTotalItem As Double

            For Each ItemCotiDet As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz

                If vstrCapacidad = "SWB" Then
                    dblTotal += ItemCotiDet.Total + ItemCotiDet.SSTotal
                    dblTotalItem = ItemCotiDet.Total + ItemCotiDet.SSTotal
                ElseIf vstrCapacidad = "DWB" Then
                    dblTotal += ItemCotiDet.Total '* 2
                    dblTotalItem = ItemCotiDet.Total ' * 2
                ElseIf vstrCapacidad = "TPL" Then
                    dblTotal += ItemCotiDet.Total + ItemCotiDet.STTotal '* 3
                    dblTotalItem = ItemCotiDet.Total + ItemCotiDet.STTotal '* 3
                End If

                If ItemCotiDet.CostoRealImpto > 0 Then
                    dblTotalIGV += dblTotalItem * (vsglIgv / 100)
                End If
            Next

            Dim tmpNroPax As Byte = vbytCapacidad
            If BECot.NroLiberados > 0 Then
                If BECot.Tipo_Lib = "S" And vstrCapacidad = "SWB" Then
                    tmpNroPax = tmpNroPax - BECot.NroLiberados
                ElseIf BECot.Tipo_Lib = "D" And vstrCapacidad = "DWB" Then
                    tmpNroPax = tmpNroPax - BECot.NroLiberados
                ElseIf BECot.Tipo_Lib = "T" And vstrCapacidad = "TPL" Then
                    tmpNroPax = tmpNroPax - BECot.NroLiberados
                End If
            End If

            Dim ItemDetDebitMemo As New clsDetalleDebitMemoBE
            With ItemDetDebitMemo
                .IDDebitMemo = vstrIDDebitMemo
                .CapacidadHab = vstrCapacidad
                .NroPax = tmpNroPax
                .SubTotal = dblTotal * tmpNroPax
                .CostoPersona = dblTotal
                .TotalIGV = dblTotalIGV
                .Total = (.SubTotal + .TotalIGV)
                .Descripcion = "PROGRAM IN " & vstrCapacidad & " USD " & dblTotal.ToString & " X " & tmpNroPax.ToString & " PAX"
                .UserMod = BECot.UserMod
                .Accion = "N"
            End With

            rListaDetDebMem.Add(ItemDetDebitMemo)

        Catch ex As Exception
            Throw
        End Try


    End Sub
    Public Sub GenerarxResumen(ByVal vListaFiles As List(Of Integer), ByVal vsglIgv As Single, ByVal vstrUserMod As String)
        Try
            Dim objDetCotBN As New clsCotizacionBN.clsDetalleCotizacionBN
            Dim ListaDetCot As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)

            For Each intIDCab As Integer In vListaFiles

                Using drDetCot As SqlClient.SqlDataReader = objDetCotBN.ConsultarTodosxIDCab(intIDCab)
                    Dim objBEDet As New clsCotizacionBE.clsDetalleCotizacionBE

                    While drDetCot.Read
                        If drDetCot("DebitMemoPendiente") = True Then
                            With objBEDet
                                .IDDet = drDetCot("IDDet")
                                .SSTotal = drDetCot("SSTotal")
                                .Total = drDetCot("Total")
                                .STTotal = drDetCot("STTotal")
                                .CostoRealImpto = drDetCot("CostoRealImpto")
                            End With
                            ListaDetCot.Add(objBEDet)
                        End If
                    End While

                    drDetCot.Close()
                End Using

                Dim objCotBN As New clsCotizacionBN
                Using drCot As SqlClient.SqlDataReader = objCotBN.ConsultarPk(intIDCab)
                    Dim objBE As New clsCotizacionBE
                    With objBE
                        .IdCab = intIDCab
                        .IDFile = drCot("IDFile")
                        .IDCliente = drCot("IDCliente")
                        .FecInicio = drCot("FecInicio")
                        .NroLiberados = If(IsDBNull(drCot("NroLiberados")) = True, "0", drCot("NroLiberados"))
                        .NroPax = drCot("NroPax")
                        .Simple = If(IsDBNull(drCot("Simple")) = True, "0", drCot("Simple"))
                        .Twin = If(IsDBNull(drCot("Twin")) = True, "0", drCot("Twin"))
                        .Matrimonial = If(IsDBNull(drCot("Matrimonial")) = True, "0", drCot("Matrimonial"))
                        .Triple = If(IsDBNull(drCot("Triple")) = True, "0", drCot("Triple"))
                        .IDBanco = drCot("IDBanco")
                        .UserMod = vstrUserMod
                    End With
                    drCot.Close()

                    objBE.ListaDetalleCotiz = ListaDetCot

                    GenerarxFile(objBE, vsglIgv)
                End Using
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Function GenerarxFile(ByVal BECot As clsCotizacionBE, _
                            ByVal vsglIgv As Single) As String
        Try
            Dim datFecVencim As Date = datFechaVencimiento(BECot.FecInicio, BECot.IDCliente, _
                                                           BECot.NroPax, BECot.NroLiberados)

            Dim dblTotal As Double = 0, dblSubTotal As Double = 0, dblTotalIGV As Double = 0
            Dim ListaDetalleDebitMemo As New List(Of clsDetalleDebitMemoBE)

            Dim objBNDe As New clsDebitMemoBN
            Dim datFechaIn = Nothing, datFechaOut As Date = Nothing
            Dim strCliente = "", strResponsable = "", strTitulo As String = ""
            Dim bytPax As Byte = 0
            Using dr As SqlClient.SqlDataReader = objBNDe.ConsultarDatosCotizacion(BECot.IdCab)
                dr.Read()
                If dr.HasRows Then
                    datFechaIn = dr("FechaIn")
                    datFechaOut = dr("FechaOut")
                    strCliente = dr("Cliente")
                    bytPax = CByte(dr("NroPax")) + CByte(dr("NroLiberados"))
                    strTitulo = dr("Titulo")
                    strResponsable = dr("Responsable")
                End If
                dr.Close()
            End Using

            Dim ItemDebitMemo As New clsDebitMemoBE With _
            {.Fecha = Date.Now.ToShortDateString, _
            .FecVencim = datFecVencim, _
            .IDCab = BECot.IdCab, _
            .IDFile = BECot.IDFile, _
            .IDMoneda = "USD", _
            .SubTotal = dblSubTotal, _
            .TotalIGV = dblTotalIGV, _
            .Total = dblTotal, _
            .IDBanco = BECot.IDBanco, _
            .IDEstado = "PD", _
            .IDTipo = "I", _
            .FechaIn = CDate(datFechaIn).ToShortDateString, _
            .FechaOut = CDate(datFechaOut).ToShortDateString, _
            .IDCliente = BECot.IDCliente, _
            .Cliente = strCliente.ToUpper, _
            .Pax = bytPax, _
            .Titulo = strTitulo.ToUpper, _
            .Responsable = strResponsable.ToUpper, _
            .FlImprimioDebit = False, _
            .FlManual = False, _
            .UserMod = BECot.UserMod, _
            .Accion = "N"}

            Dim strIDDebitMemo As String = Insertar(ItemDebitMemo)

            CargarListaDetDebitMemo(BECot, "SWB", BECot.Simple, 0, strIDDebitMemo, ListaDetalleDebitMemo)
            CargarListaDetDebitMemo(BECot, "DWB", (BECot.Twin + BECot.Matrimonial) * 2, 0, strIDDebitMemo, ListaDetalleDebitMemo)
            CargarListaDetDebitMemo(BECot, "TPL", BECot.Triple * 3, 0, strIDDebitMemo, ListaDetalleDebitMemo)

            ItemDebitMemo.ListaDetalleDebitMemo = ListaDetalleDebitMemo
            Dim objDet As New clsDetalleDebitMemoBT
            objDet.InsertarActualizarEliminar(ItemDebitMemo)

            For Each ItemDebitMemoDet As clsDetalleDebitMemoBE In ListaDetalleDebitMemo
                dblTotal += ItemDebitMemoDet.Total
                dblSubTotal += ItemDebitMemoDet.SubTotal
                dblTotalIGV += ItemDebitMemoDet.TotalIGV
            Next

            ActualizarTotales(strIDDebitMemo, dblSubTotal, dblTotalIGV, dblTotal, BECot.UserMod)

            Dim ListIDDet As New List(Of Integer)
            For Each ItemCotiDet As clsCotizacionBE.clsDetalleCotizacionBE In BECot.ListaDetalleCotiz
                ListIDDet.Add(ItemCotiDet.IDDet)
            Next
            ActualizarCotizFlagDebitMemo(ListIDDet, strIDDebitMemo, False, BECot.UserMod)




            ItemDebitMemo.Total = dblTotal
            ItemDebitMemo.IDDebitMemo = strIDDebitMemo
            If ItemDebitMemo.IDCab <> 0 And ItemDebitMemo.Total <> 0 Then
                Dim objDocBT As New clsDocumentoBT
                Dim strEstadoFactura As String = objDocBT.strDevuelveEstadoFacturac(ItemDebitMemo.IDCab)
                If strEstadoFactura = "F" Or strEstadoFactura = "A" Then 'Facturado
                    InsertarDocumentoxDebitMemo(ItemDebitMemo, False)
                End If
            End If



            ContextUtil.SetComplete()

            Return strIDDebitMemo
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Function

    Public Sub ActualizarFlagsFacturado(ByVal BE As clsDocumentoBE)
        Dim dc As Dictionary(Of String, String)
        Try
            For Each Item As clsDebitMemoBE In BE.ListaDebitMemos
                dc = New Dictionary(Of String, String)
                dc.Add("@IDDebitMemo", Item.IDDebitMemo)
                dc.Add("@FlFacturado", Item.FlFacturado)
                dc.Add("@UserMod", Item.UserMod)

                objADO.ExecuteSP("DEBIT_MEMO_UpdFacturado", dc)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDocumentoBT
    Inherits clsAdministracionBaseBT
    Private strNombreClase As String = "clsDocumentoBT"
    Public pstrCoProveedorSetLima As String
    Public pstrCoProveedorSetBuenosAires As String
    Public pstrCoProveedorSetSantiago As String
    Public pstrBuenosAires As String
    Public pstrSantiago As String
    Public pstrCoUbigeo_Oficina As String
    Private Overloads Function Insertar(ByVal BE As clsDocumentoBE) As String
        'Dim dc As New Dictionary(Of String, String)
        'Try
        '    'Verifico que este vacio CoCtroCosto para llamar al procedimiento
        '    Dim strCoCtroCosto As String = BE.CoCtroCosto
        '    Dim strCtaContable As String = BE.CoCtaContable

        '    If strCoCtroCosto Is Nothing Then
        '        strCoCtroCosto = ""
        '    End If
        '    If strCoCtroCosto.Trim() = "" Then
        '        Using objBLCli As New clsClienteBN
        '            Using dr As SqlClient.SqlDataReader = objBLCli.ConsultarxCtaContable(BE.IDCliente, BE.CoCtaContable, BE.IDCab)
        '                dr.Read()
        '                If dr.HasRows Then
        '                    strCoCtroCosto = If(IsDBNull(dr("CoCtroCosto")), "", dr("CoCtroCosto"))
        '                    strCtaContable = If(IsDBNull(dr("CtaContable")), "", dr("CtaContable"))
        '                End If
        '                dr.Close()
        '            End Using
        '        End Using
        '    End If
        '    Dim strCoCliente As String = BE.IDCliente
        '    If pstrCoUbigeo_Oficina = pstrBuenosAires Or pstrCoUbigeo_Oficina = pstrSantiago Then 'B. Aires o Santiago
        '        strCoCliente = pstrCoProveedorSetLima
        '    End If

        '    Dim strNuDocum As String = ""
        '    With dc
        '        '.Add("@NuDocumManual", BE.NuDocum)
        '        .Add("@CoSerie", BE.CoSerie)
        '        .Add("@IDTipoDoc", BE.IDTipoDoc)
        '        .Add("@IDCab", BE.IDCab)
        '        .Add("@FeDocum", Format(BE.FeDocum, "dd/MM/yyyy HH:mm:ss"))
        '        .Add("@IDCliente", strCoCliente)
        '        .Add("@IDMoneda", BE.IDMoneda)
        '        .Add("@SsIGV", BE.SsIGV)
        '        .Add("@SsTotalDocumUSD", BE.SsTotalDocumUSD)
        '        .Add("@FlDocManual", BE.FlDocManual)
        '        .Add("@TxTitulo", BE.TxTitulo)
        '        .Add("@NuDocumVinc", BE.NuDocumVinc)
        '        .Add("@IDTipoDocVinc", BE.IDTipoDocVinc)
        '        .Add("@UserMod", BE.UserMod)
        '        .Add("@FlPorAjustePrecio", BE.FlPorAjustePrecio)
        '        .Add("@CoTipoVenta", BE.CoTipoVenta)

        '        .Add("@CoCtaContable", strCtaContable) '.Add("@CoCtaContable", BE.CoCtaContable)
        '        .Add("@FeEmisionVinc", Format(BE.FeEmisionVinc, "dd/MM/yyyy HH:mm:ss"))

        '        .Add("@CoCtroCosto", strCoCtroCosto) '.Add("@CoCtroCosto", BE.CoCtroCosto)
        '        .Add("@NuFileLibre", BE.NuFileLibre)
        '        .Add("@pNuDocum", "")
        '        .Add("@NuNtaVta", BE.NuNtaVta)
        '        .Add("@NuItem_DM", BE.NuItem_DM)
        '        .Add("@CoDebitMemoAnticipo", BE.CoDebitMemoAnticipo)
        '        .Add("@FlVentaAdicional", BE.FlVentaAdicional)
        '    End With
        '    strNuDocum = objADO.ExecuteSPOutput("DOCUMENTO_Ins", dc)

        '    If BE.FlDocManual Then
        '        If pstrCoUbigeo_Oficina = pstrBuenosAires Or pstrCoUbigeo_Oficina = pstrSantiago Then 'B. Aires o Santiago
        '            InsertarDocumentosProveedorOficinaExterna(BE)
        '        End If
        '    End If


        '    ContextUtil.SetComplete()
        '    Return strNuDocum
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    Throw
        'End Try
    End Function
    Private Sub InsertarDocumentosProveedorOficinaExterna(ByVal BE As clsDocumentoBE)
        Try

            Dim strCoProveedor As String = ""
            If pstrCoUbigeo_Oficina = pstrBuenosAires Then
                strCoProveedor = pstrCoProveedorSetBuenosAires
            ElseIf pstrCoUbigeo_Oficina = pstrSantiago Then
                strCoProveedor = pstrCoProveedorSetSantiago
            End If

            Dim objVouBE As New clsVoucherOficinaExternaBE With {.CoProveedor = strCoProveedor, _
                                                                 .CoUbigeo_Oficina = pstrCoUbigeo_Oficina, _
                                                                 .CoMoneda = BE.IDMoneda, _
                                                                 .FeVoucher = BE.FeDocum, _
                                                                 .IdCab = BE.IDCab, _
                                                                 .SsTotal = BE.SsTotalDocumUSD, _
                                                                 .SsImpuestos = BE.SsIGV, _
                                                                 .UserMod = BE.UserMod}
            Dim objVouBT As New clsVoucherOficinaExternaBT
            Dim intNuVouOfiExtInt As Integer = objVouBT.Insertar(objVouBE)

            Dim objBN As New clsDocumentoProveedorBN
            Dim objDocProvBE As New clsDocumentoProveedorBE
            With objDocProvBE
                .NuDocInterno = objBN.strDevuelveNuDocInterno_Correlativo(BE.FeDocum.ToString("dd/MM/yyyy"))
                .NuVoucher = 0
                .NuOrden_Servicio = 0
                .CoOrdPag = 0
                .NuOrdComInt = 0
                .NuVouOfiExtInt = intNuVouOfiExtInt
                .NuDocum = BE.NuDocum
                If BE.IDTipoDoc.Substring(0, 1) = "F" Then
                    .CoTipoDoc = "FAC"
                ElseIf BE.IDTipoDoc.Substring(0, 1) = "B" Then
                    .CoTipoDoc = "BOL"
                End If
                .FeEmision = BE.FeDocum
                .FeRecepcion = BE.FeDocum
                .CoTipoDetraccion = "00000"
                .CoMoneda = BE.IDMoneda
                .CoMoneda_Pago = BE.IDMoneda
                .CoMoneda_FEgreso = BE.IDMoneda
                .SsIGV = BE.SsIGV
                .SsNeto = BE.SsTotalDocumUSD - BE.SsIGV
                .SsOtrCargos = 0
                .SsDetraccion = 0
                .SSTotalOriginal = BE.SsTotalDocumUSD
                .SSTotal = BE.SsTotalDocumUSD
                .SSTipoCambio = 0

                .CoTipoOC = "011"
                .CoCtaContab = BE.CoCtaContable

                If pstrCoUbigeo_Oficina = pstrBuenosAires Then
                    .CoObligacionPago = "VBA"
                ElseIf pstrCoUbigeo_Oficina = pstrSantiago Then
                    .CoObligacionPago = "VSA"
                End If
                .IngresoManual = False

                .SsTipoCambio_FEgreso = 0
                .SsTotal_FEgreso = 0
                .IDOperacion = 0

                .CoProveedor = ""

                .NuDocumVta = BE.NuDocum
                .CoTipoDocVta = BE.IDTipoDoc

                .UserMod = BE.UserMod
                .UserNuevo = BE.UserMod


            End With

            Dim objDocProvBT As New clsDocumentoProveedorBT
            objDocProvBT.Grabar(objDocProvBE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Overloads Function InsertarManual(ByVal BE As clsDocumentoBE, ByVal vblnSAP As Boolean) As String
        Dim strNuDocum As String = ""
        Try
            strNuDocum = Insertar(BE)
            ActualizarIDsNuevosenHijosST(BE, BE.NuDocum, strNuDocum)

            Dim objBT As New clsDocumentoDet_TextoBT
            objBT.InsertarActualizarEliminar(BE)

            If vblnSAP Then
                EnviarSAP(BE.IDTipoDoc, strNuDocum, BE.UserMod, "POST")
            End If


            ContextUtil.SetComplete()
            Return strNuDocum
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Overloads Sub Actualizar(ByVal BE As clsDocumentoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", BE.NuDocum)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@SsTotalDocumUSD", BE.SsTotalDocumUSD)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_Upd", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarManual(ByVal BE As clsDocumentoBE)
        'Dim dc As New Dictionary(Of String, String)
        'Try
        '    Dim strCoCtroCosto As String = BE.CoCtroCosto
        '    Dim strCtaContable As String = BE.CoCtaContable

        '    If strCoCtroCosto Is Nothing Then
        '        strCoCtroCosto = ""
        '    End If
        '    If strCoCtroCosto.Trim() = "" Then
        '        Using objBLCli As New clsClienteBN
        '            Using dr As SqlClient.SqlDataReader = objBLCli.ConsultarxCtaContable(BE.IDCliente, BE.CoCtaContable, BE.IDCab)
        '                dr.Read()
        '                If dr.HasRows Then
        '                    strCoCtroCosto = If(IsDBNull(dr("CoCtroCosto")), "", dr("CoCtroCosto"))
        '                    strCtaContable = If(IsDBNull(dr("CtaContable")), "", dr("CtaContable"))
        '                End If
        '                dr.Close()
        '            End Using
        '        End Using
        '    End If


        '    With dc
        '        .Add("@NuDocum", BE.NuDocum)
        '        .Add("@IDTipoDoc", BE.IDTipoDoc)
        '        .Add("@FeDocum", Format(BE.FeDocum, "dd/MM/yyyy HH:mm:ss"))
        '        .Add("@IDCab", BE.IDCab)
        '        .Add("@IDCliente", BE.IDCliente)
        '        .Add("@IDMoneda", BE.IDMoneda)
        '        .Add("@SsIGV", BE.SsIGV)
        '        .Add("@SsTotalDocumUSD", BE.SsTotalDocumUSD)
        '        .Add("@CoEstado", BE.CoEstado)
        '        .Add("@TxTitulo", BE.TxTitulo)

        '        .Add("@IDTipoDocVinc", BE.IDTipoDocVinc)
        '        .Add("@NuDocumVinc", BE.NuDocumVinc)
        '        .Add("@CoTipoVenta", BE.CoTipoVenta)
        '        .Add("@UserMod", BE.UserMod)

        '        .Add("@CoCtaContable", strCtaContable)
        '        .Add("@FeEmisionVinc", Format(BE.FeEmisionVinc, "dd/MM/yyyy HH:mm:ss"))
        '        .Add("@CoCtroCosto", strCoCtroCosto)
        '        .Add("@NuFileLibre", BE.NuFileLibre)
        '        .Add("@NuNtaVta", BE.NuNtaVta)
        '    End With
        '    objADO.ExecuteSP("DOCUMENTO_UpdManual", dc)

        '    ' ''Dim objBT As New clsDocumentoDetBT
        '    ' ''objBT.InsertarActualizarEliminarManual(BE)

        '    Dim objBT As New clsDocumentoDet_TextoBT
        '    objBT.InsertarActualizarEliminar(BE)

        '    'EnviarSAP(BE.IDTipoDoc, BE.NuDocum, BE.UserMod, "POST") '''''

        '    ContextUtil.SetComplete()
        'Catch ex As Exception
        '    ContextUtil.SetAbort()
        '    Throw
        'End Try
    End Sub

    Public Overloads Sub Anular(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, _
                                ByVal vstrUserMod As String, ByVal vstrCoSAP As String, ByVal vchrTipoAnul As Char)
        Dim dc As New Dictionary(Of String, String)
        Try

            With dc
                .Add("@NuDocum", vstrNuDocum)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_Anu", dc)

            'If vstrNuDocum.Substring(0, 3) <> "005" Then

            If vstrCoSAP <> "" Then
                EnviarSAP(vstrIDTipoDoc, vstrNuDocum, vstrUserMod, "PUT", vchrTipoAnul)
            End If

            'End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vstrNuDocum As String, ByVal vstrIDTipoDoc As String, _
                                  Optional ByVal vstrCoSerie As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", vstrNuDocum)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
            End With
            'objADO.ExecuteSP("DEBIT_MEMO_DOCUMENTO_DelxFK", dc)
            objADO.ExecuteSP("DOCUMENTO_DET_TEXTO_DelxFK", dc)
            objADO.ExecuteSP("DOCUMENTO_DET_DelxFK", dc)
            objADO.ExecuteSP("INGRESO_DOCUMENTO_DelxNuDocum", dc)
            objADO.ExecuteSP("DOCUMENTO_Del", dc)

            If vstrCoSerie <> "" Then
                Dim objBN As New clsDocumentoBN
                Dim intCorrelativo As Integer = objBN.intDevuelveCorrelativo(vstrIDTipoDoc, vstrCoSerie)
                Dim intNuDocum As Integer = CInt(vstrNuDocum.Substring(3, vstrNuDocum.Length - 3))

                If intCorrelativo = intNuDocum Then
                    Dim dcUpdCorr As New Dictionary(Of String, String)
                    With dcUpdCorr
                        .Add("@IDTipoDoc", vstrIDTipoDoc)
                        .Add("@CoSerie", vstrCoSerie)
                    End With
                    objADO.ExecuteSP("Correlativo_UpdCorrelativoDoc", dcUpdCorr)
                End If
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub ActualizarEstadoFacturacionFile(ByVal vintIDCab As Integer, _
                        ByVal vstrEstado As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@EstadoFacturac", vstrEstado)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("COTICAB_UpdEStadoFacturac", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsDocumentoBE, ByVal vstrAntiguoID As String, _
                                                  ByVal vstrNuevoID As String)
        Try
            If Not BE.ListaDocumentos Is Nothing Then
                For Each Item As clsDocumentoBE In BE.ListaDocumentos
                    If Item.NuDocum = vstrAntiguoID Then 'And (Item.IDTipoDoc = vintNuevoID2 Or vintNuevoID2 = "") Then
                        Item.NuDocum = vstrNuevoID
                        Exit For
                    End If
                Next

                For Each Item As clsDocumentoBE In BE.ListaDocumentos
                    If Item.NuDocumVinc = vstrAntiguoID Then
                        Item.NuDocumVinc = vstrNuevoID
                    End If
                Next
            End If


            If Not BE.ListaDetalleDocumentos Is Nothing Then
                For Each Item As clsDocumentoDetBE In BE.ListaDetalleDocumentos
                    If Item.NuDocum = vstrAntiguoID Then 'And (Item.IDTipoDoc = vintNuevoID2 Or vintNuevoID2 = "") Then
                        Item.NuDocum = vstrNuevoID
                    End If
                Next
            End If

            If Not BE.ListaDetalleDocumentos_Texto Is Nothing Then
                For Each Item As clsDocumentoDet_TextoBE In BE.ListaDetalleDocumentos_Texto
                    If Item.NuDocum = vstrAntiguoID Then
                        Item.NuDocum = vstrNuevoID
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function strInsertarDocumentoGenerado(ByVal BE As clsDocumentoBE) As String
        Try
            If Not BE.blnExistenteenSetra Then
                If BE.Generado = False Then
                    Dim strNumDocum As String = Insertar(BE)
                    ActualizarIDsNuevosenHijosST(BE, BE.NuDocum, strNumDocum)
                Else
                    Actualizar(BE)
                End If

                'Dim dttDocs As DataTable = ConsultarDocumentoxIDCab(BE.IDCab)
                Dim dttDocs As DataTable = dttConsultarDocumentoGenerado(BE.IDCab, BE.IDTipoDoc)

                For Each drDoc As DataRow In dttDocs.Rows
                    Dim blnExisteenLista As Boolean = False
                    For Each Item As clsDocumentoBE In BE.ListaDocumentos
                        If Item.NuDocum = drDoc("NuDocum") And Item.IDTipoDoc = BE.IDTipoDoc Then
                            'and Item.CoSerie=drDoc("CoSerie") Then
                            blnExisteenLista = True
                            Exit For
                        End If
                    Next
                    If Not blnExisteenLista Then
                        Anular(drDoc("IDTipoDoc"), drDoc("NuDocum"), BE.UserMod, "", "")
                    End If
                Next
                Dim objDetBT As New clsDocumentoDetBT
                objDetBT.InsertarActualizarEliminar(BE, BE.IDTipoDoc, BE.NuDocum)

                Dim objDetTextoBT As New clsDocumentoDet_TextoBT
                objDetTextoBT.EliminarFK(BE.NuDocum, BE.IDTipoDoc)

                objDetTextoBT.InsertarActualizarEliminar(ListaDetDocumentoTexto(BE.NuDocum, _
                                            BE.IDTipoDoc, BE.IDCab, BE.IDTipoDocVinc, BE.NuDocumVinc, "N", _
                                            BE.UserMod))

                If Not pstrCoUbigeo_Oficina Is Nothing Then
                    If pstrCoUbigeo_Oficina = pstrBuenosAires Or pstrCoUbigeo_Oficina = pstrSantiago Then 'B. Aires o Santiago
                        InsertarDocumentosProveedorOficinaExterna(BE)
                    End If
                End If
            End If


            If BE.Generado = False And BE.blnSAP Then

                'If BE.NuDocum.Substring(0, 3) = "005" And Not (BE.IDTipoDoc = "NCA" Or BE.IDTipoDoc = "NDB") Then
                'EnviarSAPAnticipo(BE.IDTipoDoc, BE.NuDocum, BE.UserMod, "POST")
                'Else
                EnviarSAP(BE.IDTipoDoc, BE.NuDocum, BE.UserMod, "POST")
                'End If
            End If



            ContextUtil.SetComplete()

            Return ""
        Catch ex As Exception

            ContextUtil.SetAbort()

            Return ex.Message
        End Try
    End Function
    Public Sub InsertarDocumentosGenerados(ByVal vintIDCab As Integer, _
                                 ByVal BE As clsDocumentoBE, _
                                 ByVal vstrUserMod As String, ByVal vblnAnticipo As Boolean)
        Try

            For Each Item As clsDocumentoBE In BE.ListaDocumentos
                If Item.Generado = False Then
                    Dim strNumDocum As String = Insertar(Item)
                    ActualizarIDsNuevosenHijosST(BE, Item.NuDocum, strNumDocum)
                Else
                    Actualizar(Item)
                End If
            Next

            Dim dttDocs As DataTable = ConsultarDocumentoxIDCab(vintIDCab)

            For Each drDoc As DataRow In dttDocs.Rows
                Dim blnExisteenLista As Boolean = False
                For Each Item As clsDocumentoBE In BE.ListaDocumentos
                    If Item.NuDocum = drDoc("NuDocum") And Item.IDTipoDoc = drDoc("IDTipoDoc") Then
                        'and Item.CoSerie=drDoc("CoSerie") Then
                        blnExisteenLista = True
                        Exit For
                    End If
                Next
                If Not blnExisteenLista Then
                    Anular(drDoc("IDTipoDoc"), drDoc("NuDocum"), vstrUserMod, "", "")
                End If
            Next


            Dim objDetBT As New clsDocumentoDetBT
            objDetBT.InsertarActualizarEliminar(BE)

            Dim objDetTextoBT As New clsDocumentoDet_TextoBT
            For Each Item As clsDocumentoBE In BE.ListaDocumentos
                objDetTextoBT.EliminarFK(Item.NuDocum, Item.IDTipoDoc)
            Next

            For Each Item As clsDocumentoBE In BE.ListaDocumentos
                objDetTextoBT.InsertarActualizarEliminar(ListaDetDocumentoTexto(Item.NuDocum, _
                                         Item.IDTipoDoc, Item.IDCab, Item.IDTipoDocVinc, Item.NuDocumVinc, "N", _
                                         Item.UserMod))
            Next


            If Not BE.ListaDebitMemos Is Nothing Then
                If BE.ListaDebitMemos.Count > 0 Then
                    Dim objDebitMemo As New clsDebitMemoBT
                    objDebitMemo.ActualizarFlagsFacturado(BE)
                End If
            End If
            'Dim A As String = pstrCoUbigeo_Oficina
            'Dim B As String = pstrBuenosAires
            If Not pstrCoUbigeo_Oficina Is Nothing Then
                If pstrCoUbigeo_Oficina = pstrBuenosAires Or pstrCoUbigeo_Oficina = pstrSantiago Then 'B. Aires o Santiago
                    For Each Item As clsDocumentoBE In BE.ListaDocumentos
                        InsertarDocumentosProveedorOficinaExterna(Item)
                    Next
                End If
            End If

            If vintIDCab <> 0 Then
                ActualizarEstadoFacturacionFile(vintIDCab, "F", vstrUserMod)
                'Else 'ANTICIPOS 
                '    ActualizarEstadoFacturacionFile(vintIDCab, "M", vstrUserMod) ' FACTURADO PARCIAL

                'EliminarCruceDebitMemoDocumentos(vintIDCab)
                'InsertarCruceDebitMemoDocumentos(vintIDCab, vstrUserMod)
                EliminarCruceIngresosDocumentos(vintIDCab, vstrUserMod)
                InsertarCruceIngresosDocumentos(vintIDCab, vstrUserMod)

            End If

            For Each Item As clsDocumentoBE In BE.ListaDocumentos
                If Item.Generado = False And Item.blnSAP Then
                    'If Not vblnAnticipo Then
                    EnviarSAP(Item.IDTipoDoc, Item.NuDocum, vstrUserMod, "POST")
                    'Else
                    'EnviarSAPAnticipo(Item.IDTipoDoc, Item.NuDocum, vstrUserMod, "POST")
                    'End If
                End If
            Next


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    'Private Sub EliminarCruceDebitMemoDocumentos(ByVal vintIDCab As Integer)
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@IDCab", vintIDCab)


    '        objADO.ExecuteSP("DEBIT_MEMO_DOCUMENTO_DelxIDCab", dc)
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarCruceDebitMemoDocumentos")

    '    End Try

    'End Sub
    'Private Sub InsertarCruceDebitMemoDocumentos(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@UserMod", vstrUserMod)

    '        objADO.ExecuteSP("DEBIT_MEMO_DOCUMENTO_InsxFile", dc)
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarCruceDebitMemoDocumentos")

    '    End Try

    'End Sub

    Public Sub EliminarCruceIngresosDocumentos(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            objADO.ExecuteSP("INGRESO_DOCUMENTO_DelxIDCab", dc)

            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("INGRESO_FINANZAS_UpdSsSaldoMatchDocumento", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarCruceIngresosDocumentos")

        End Try

    End Sub
    Public Sub InsertarCruceIngresosDocumentos(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@NuIngresoPar", 0)
            dc.Add("@NuAsignacion", 0)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("INGRESO_DOCUMENTO_InsMatch", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarCruceIngresosDocumentos")

        End Try

    End Sub

    Private Function strTextoImpresion(ByVal vstrIDTipoDoc As String, _
                                       ByVal vstrSerie As String, ByVal vintIDCab As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@CoSerie", vstrSerie)
            dc.Add("@IDCab", vintIDCab)

            dc.Add("@pTxTextoImpresion", "")
            Return objADO.ExecuteSPOutput("Correlativo_SelTextoImpresion", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function ListaDetDocumentoTexto(ByVal vstrNuDocumento As String, _
                                            ByVal vstrIDTipoDoc As String, _
                                            ByVal vintIDCab As Integer, _
                                            ByVal vstrIDTipoDocVinc As String, _
                                            ByVal vstrNuDocumVinc As String, _
                                            ByVal vstrAccion As String, _
                                            ByVal vstrUserMod As String) As clsDocumentoBE
        Try
            Dim LstDetDocumentoTexto As New List(Of clsDocumentoDet_TextoBE)

            If vstrIDTipoDoc = "BOL" And vstrNuDocumento.Substring(0, 3) = "005" Then
                Dim dttDoc As DataTable = dttConsultarPk(vstrNuDocumento, vstrIDTipoDoc)
                Dim objBEDetText As New clsDocumentoDet_TextoBE With { _
                                            .NuDocum = vstrNuDocumento, _
                                            .IDTipoDoc = vstrIDTipoDoc, _
                                            .NuDetalle = 0, _
                    .NoTexto = "POR EL ANTICIPO EN ATENCIÓN A LOS PAX: " & dttDoc(0)("Titulo").ToString.Trim & " DEL " & dttDoc(0)("FechaIn") & " AL " & dttDoc(0)("FechaOut"), _
                    .SsTotal = dttDoc(0)("SsTotalDocumUSD"), _
                    .Accion = vstrAccion, _
                    .UserMod = vstrUserMod}

                LstDetDocumentoTexto.Add(objBEDetText)
            Else
                Dim dttDoc As DataTable = dttConsultarPk(vstrNuDocumento, vstrIDTipoDoc)
                'Dim objBEDetText As clsDocumentoDet_TextoBE

                If dttDoc.Rows.Count > 0 Then
                    Dim strIDTipoDocTexto As String = vstrIDTipoDoc
                    Dim strSerieDocTexto As String = vstrNuDocumento.Substring(0, 3)
                    If vstrIDTipoDocVinc <> "" And vstrIDTipoDoc = "NDB" Then
                        strIDTipoDocTexto = vstrIDTipoDocVinc
                        strSerieDocTexto = vstrNuDocumVinc.Substring(0, 3)
                    End If
                    Dim strTextoBD As String = strTextoImpresion(strIDTipoDocTexto, _
                                                 strSerieDocTexto, vintIDCab)
                    Dim strNoTexto As String = strTextoBD
                    If strNoTexto = "" Then
                        If Not IsDBNull(dttDoc(0)("FechaIn")) Then
                            strNoTexto = "POR SERVICIOS PRESTADOS EN ATENCION A LOS PAX: " & dttDoc(0)("Titulo").ToString.Trim & " DEL " & dttDoc(0)("FechaIn") & " AL " & dttDoc(0)("FechaOut")
                        Else
                            strNoTexto = "POR SERVICIOS PRESTADOS EN ATENCION A LOS PAX: " & dttDoc(0)("Titulo").ToString.Trim
                        End If
                    End If

                    Dim objBEDetText As New clsDocumentoDet_TextoBE With { _
                                            .NuDocum = vstrNuDocumento, _
                                            .IDTipoDoc = vstrIDTipoDoc, _
                                            .NuDetalle = 0, _
                    .NoTexto = strNoTexto, _
                    .SsTotal = dttDoc(0)("SsTotalDocumUSD"), _
                    .Accion = vstrAccion, _
                    .UserMod = vstrUserMod}

                    'LstDetDocumentoTexto.Add(objBEDetText)
                    If strNoTexto <> "" And dttDoc(0)("SsTotalDocumUSD") <> 0 Then
                        LstDetDocumentoTexto.Add(objBEDetText)
                    End If


                    Dim strTexto As String = ""
                    If strTextoBD <> "" Then
                        If Not IsDBNull(dttDoc(0)("FechaIn")) Then
                            strTexto = "POR SERVICIOS PRESTADOS EN ATENCION A LOS PAX: " & dttDoc(0)("Titulo").ToString.Trim & " DEL " & dttDoc(0)("FechaIn") & " AL " & dttDoc(0)("FechaOut")
                        Else
                            strTexto = "POR SERVICIOS PRESTADOS EN ATENCION A LOS PAX: " & dttDoc(0)("Titulo").ToString.Trim
                        End If
                    End If

                    objBEDetText = New clsDocumentoDet_TextoBE With { _
                                                .NuDocum = vstrNuDocumento, _
                                                .IDTipoDoc = vstrIDTipoDoc, _
                                                .NuDetalle = 0, _
                        .NoTexto = strTexto, _
                        .SsTotal = 0, _
                        .Accion = vstrAccion, _
                        .UserMod = vstrUserMod}
                    If strTexto <> "" Then
                        LstDetDocumentoTexto.Add(objBEDetText)
                    End If
                End If
            End If
            Return New clsDocumentoBE With {.ListaDetalleDocumentos_Texto = LstDetDocumentoTexto}
        Catch ex As Exception
            Throw
        End Try
    End Function
    'Private Function blnExiste(ByVal vstrNuDocumento As String, ByVal vstrIDTipoDoc As String) As Boolean
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@NuDocum", vstrNuDocumento)
    '        dc.Add("@IDTipoDoc", vstrIDTipoDoc)
    '        dc.Add("@pExiste", 0)
    '        Return objADO.ExecuteSPOutput("DOCUMENTO_Sel_ExistsOutput", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Private Function ConsultarDocumentoxIDCab(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("DOCUMENTO_SelxIDCab", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dttConsultarDocumentoGenerado(ByVal vintIDCab As Integer, ByVal vstrIDTipoDoc As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)

            Return objADO.GetDataTable("DOCUMENTO_SelxIDCabTipoDoc", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function dttConsultarPk(ByVal vstrNuDocumento As String, ByVal vstrIDTipoDoc As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDocum", vstrNuDocumento)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            Return objADO.GetDataTable("DOCUMENTO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function strDevuelveEstadoFacturac(ByVal vintIDCab As Int32) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pEstadoFacturac", "")
            Return objADO.ExecuteSPOutput("COTICAB_Sel_EstadoFacturacOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub PasarDatosToSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, _
                                             ByRef robjBEInvoicesBE As clsInvoicesBE)
        Try
            Dim dtt As DataTable = ConsultarparaSAP(vstrIDTipoDoc, vstrNuDocum)
            If dtt.Rows.Count > 0 Then
                Dim dr As DataRow = dtt.Rows(0)
                robjBEInvoicesBE = New clsInvoicesBE
                With robjBEInvoicesBE
                    .TaxDate = dr("TaxDate")
                    .DocDate = dr("DocDate")
                    .DocDueDate = dr("DocDueDate")
                    .CardCode = dr("CardCode")
                    .DocType = 1
                    .JournalMemo = dr("JournalMemo")
                    .FederalTaxID = dr("FederalTaxID")
                    .DocCurrency = dr("DocCurrency")
                    .ControlAccount = dr("ControlAccount")
                    .DiscountPercent = dr("DiscountPercent")
                    .DocTotal = dr("DocTotal")
                    .DocTotalFc = dr("DocTotalFc")
                    .U_SYP_MDTD = dr("U_SYP_MDTD")
                    .U_SYP_MDSD = dr("U_SYP_MDSD")
                    .U_SYP_MDCD = dr("U_SYP_MDCD").ToString.Trim
                    'If dr("U_SYP_MDTO").ToString.Trim <> "" Then
                    .U_SYP_MDTO = dr("U_SYP_MDTO").ToString.Trim
                    .U_SYP_MDSO = dr("U_SYP_MDSO").ToString.Trim
                    .U_SYP_MDCO = dr("U_SYP_MDCO").ToString.Trim
                    'End If
                    If Not IsDBNull(dr("U_SYP_FECHAREF")) Then
                        .U_SYP_FECHAREF = dr("U_SYP_FECHAREF")
                    End If
                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_TC = dr("U_SYP_TC")
                    .U_SYP_NFILE = dr("U_SYP_NFILE").ToString.Trim
                    .U_SYP_DOCEXPORT = dr("U_SYP_DOCEXPORT")

                End With

                Dim objDet As New clsDocumentoDet_TextoBT
                Dim dttDet As DataTable = objDet.ConsultarparaSAP(vstrIDTipoDoc, vstrNuDocum)
                Dim ListaLineas As New List(Of clsInvoicesBE.clsLinesBE)
                For Each dr2 As DataRow In dttDet.Rows
                    Dim strItemDescripcion As String = dr2("ItemDescription").ToString.Trim
                    Dim strItemDescripcionOrig As String = dr2("ItemDescription").ToString.Trim
                    Dim dblTaxTotal As Double = dr2("TaxTotal")
                    Dim dblLineTotal As Double = dr2("LineTotal")
                    Dim strTaxCode As String = dr2("TaxCode").ToString.Trim
                    Dim strCostingCode As String = dr2("CostingCode").ToString.Trim
                    Dim strCostingCode2 As String = dr2("CostingCode2").ToString.Trim
                    Dim strCostingCode3 As String = dr2("CostingCode3").ToString.Trim
                    Dim strAccountCode As String = dr2("AccountCode").ToString.Trim

                    Dim strItemDescripcionSaldo As String = ""
                    Dim intInicioBusqueda As Int16 = 0

                    If strItemDescripcionOrig.Length > 100 Then
                        'strItemDescripcion = strItemDescripcion.Substring(intInicioBusqueda, 100)
InicioBusqueda:
                        strItemDescripcion = Mid(strItemDescripcionOrig, intInicioBusqueda + 1, 100)

                        If strItemDescripcionOrig.Length > intInicioBusqueda + 1 Then
                            intInicioBusqueda += 100
                            strItemDescripcionSaldo = Mid(strItemDescripcionOrig, intInicioBusqueda + 1)
                        Else
                            strItemDescripcionSaldo = ""
                        End If

                    End If
                    Dim objLinea As New clsInvoicesBE.clsLinesBE _
                        With {.ItemDescription = strItemDescripcion, _
                              .LineTotal = dblLineTotal, _
                              .CostingCode = strCostingCode, _
                              .CostingCode2 = strCostingCode2, _
                              .CostingCode3 = strCostingCode3, _
                              .TaxCode = strTaxCode, _
                              .TaxTotal = dblTaxTotal, _
                              .AccountCode = strAccountCode}
                    ListaLineas.Add(objLinea)

                    If strItemDescripcionSaldo.Trim.Length > 0 Then
                        dblTaxTotal = 0
                        dblLineTotal = 0
                        strCostingCode = ""
                        strCostingCode2 = ""
                        strCostingCode3 = ""
                        strTaxCode = ""
                        strAccountCode = ""

                        GoTo InicioBusqueda
                    End If

                Next

                'Detalle de anticipos

                'If vstrNuDocum.Substring(0, 3) = "005" And vstrIDTipoDoc = "NCA" Then
                '    Dim ListaDown As New List(Of clsInvoicesBE.clsDownPaymentsToDrawBE)
                '    Dim dttAnt As DataTable = ConsultarVinculadosparaSAP(vstrIDTipoDoc, vstrNuDocum)
                '    For Each dr3 As DataRow In dttAnt.Rows
                '        Dim objDown As New clsInvoicesBE.clsDownPaymentsToDrawBE _
                '            With {.U_SYP_MDCD = dr3("U_SYP_MDCD"), _
                '                  .U_SYP_MDSD = dr3("U_SYP_MDSD"), _
                '                  .U_SYP_MDTD = dr3("U_SYP_MDTD"), _
                '                  .AmountToDraw = dr3("AmountToDraw"), _
                '                  .AmountToDrawFC = dr3("AmountToDrawFC"), _
                '                  .GrossAmountToDraw = dr3("GrossAmountToDraw"), _
                '                  .GrossAmountToDrawFC = dr3("GrossAmountToDrawFC")}
                '        ListaDown.Add(objDown)
                '    Next
                '    If ListaDown.Count > 0 Then robjBEInvoicesBE.DownPaymentsToDraw = ListaDown
                'End If


                robjBEInvoicesBE.Lines = ListaLineas

            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Overloads Sub PasarDatosToSAPAnticipo(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, _
                                             ByRef robjBEInvoicesBE As clsDownPaymentsBE)
        Try
            Dim dtt As DataTable = ConsultarparaSAP(vstrIDTipoDoc, vstrNuDocum)
            If dtt.Rows.Count > 0 Then
                Dim dr As DataRow = dtt.Rows(0)
                robjBEInvoicesBE = New clsDownPaymentsBE
                With robjBEInvoicesBE
                    .TaxDate = dr("TaxDate")
                    .DocDate = dr("DocDate")
                    .DocDueDate = dr("DocDueDate")
                    .CardCode = dr("CardCode")
                    .DocType = 1
                    .FederalTaxID = dr("FederalTaxID")
                    .DocCurrency = dr("DocCurrency")
                    .ControlAccount = dr("ControlAccount")
                    .DiscountPercent = dr("DiscountPercent")
                    .DocTotal = dr("DocTotal")
                    .DocTotalFc = dr("DocTotalFc")
                    .U_SYP_MDTD = dr("U_SYP_MDTD")
                    .U_SYP_MDSD = dr("U_SYP_MDSD")
                    .U_SYP_MDCD = dr("U_SYP_MDCD")
                    'If dr("U_SYP_MDTO").ToString.Trim <> "" Then
                    .U_SYP_MDTO = dr("U_SYP_MDTO").ToString.Trim
                    .U_SYP_MDSO = dr("U_SYP_MDSO").ToString.Trim
                    .U_SYP_MDCO = dr("U_SYP_MDCO").ToString.Trim
                    'End If
                    If Not IsDBNull(dr("U_SYP_FECHAREF")) Then
                        .U_SYP_FECHAREF = dr("U_SYP_FECHAREF")
                    End If
                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_TC = dr("U_SYP_TC")
                    .U_SYP_NFILE = dr("U_SYP_NFILE").ToString.Trim
                End With

                Dim objDet As New clsDocumentoDet_TextoBT
                Dim dttDet As DataTable = objDet.ConsultarparaSAP(vstrIDTipoDoc, vstrNuDocum)
                Dim ListaLineas As New List(Of clsDownPaymentsBE.clsLinesBE)
                For Each dr2 As DataRow In dttDet.Rows
                    Dim strItemDescripcion As String = dr2("ItemDescription").ToString.Trim
                    Dim strItemDescripcionOrig As String = dr2("ItemDescription").ToString.Trim
                    Dim dblTaxTotal As Double = dr2("TaxTotal")
                    Dim dblLineTotal As Double = dr2("LineTotal")
                    Dim strTaxCode As String = dr2("TaxCode").ToString.Trim
                    Dim strCostingCode As String = dr2("CostingCode").ToString.Trim
                    Dim strCostingCode2 As String = dr2("CostingCode2").ToString.Trim
                    Dim strCostingCode3 As String = dr2("CostingCode3").ToString.Trim
                    'Dim strAccountCode As String = dr2("AccountCode").ToString.Trim

                    Dim strItemDescripcionSaldo As String = ""
                    Dim intInicioBusqueda As Int16 = 0

                    If strItemDescripcionOrig.Length > 100 Then
                        'strItemDescripcion = strItemDescripcion.Substring(intInicioBusqueda, 100)
InicioBusqueda:
                        strItemDescripcion = Mid(strItemDescripcionOrig, intInicioBusqueda + 1, 100)

                        If strItemDescripcionOrig.Length > intInicioBusqueda + 1 Then
                            intInicioBusqueda += 100
                            strItemDescripcionSaldo = Mid(strItemDescripcionOrig, intInicioBusqueda + 1)
                        Else
                            strItemDescripcionSaldo = ""
                        End If

                    End If
                    Dim objLinea As New clsDownPaymentsBE.clsLinesBE _
                        With {.ItemDescription = strItemDescripcion, _
                              .LineTotal = dblLineTotal, _
                              .CostingCode = strCostingCode, _
                              .CostingCode2 = strCostingCode2, _
                              .CostingCode3 = strCostingCode3, _
                              .TaxCode = strTaxCode, _
                              .TaxTotal = dblTaxTotal}
                    ListaLineas.Add(objLinea)

                    If strItemDescripcionSaldo.Trim.Length > 0 Then
                        dblTaxTotal = 0
                        dblLineTotal = 0
                        strCostingCode = ""
                        strCostingCode2 = ""
                        strCostingCode3 = ""
                        strTaxCode = ""
                        'strAccountCode = ""

                        GoTo InicioBusqueda
                    End If

                Next

                robjBEInvoicesBE.Lines = ListaLineas
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Overloads Sub PasarDatosAnulacionToSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, _
                                         ByRef robjBEInvoicesBE As clsInvoicesBE, ByVal vchrTipoAnul As Char)
        Try
            Dim dtt As DataTable = ConsultarparaSAP(vstrIDTipoDoc, vstrNuDocum, vchrTipoAnul)
            If dtt.Rows.Count > 0 Then
                Dim dr As DataRow = dtt.Rows(0)
                robjBEInvoicesBE = New clsInvoicesBE
                With robjBEInvoicesBE
                    .U_SYP_MDTD = dr("U_SYP_MDTD")
                    .U_SYP_MDSD = dr("U_SYP_MDSD")
                    .U_SYP_MDCD = dr("U_SYP_MDCD")
                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                End With
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Function ConsultarparaSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String, Optional ByVal vchrTipoAnul As Char = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@NuDocum", vstrNuDocum)
            dc.Add("@cTipoAnul", vchrTipoAnul)
            Return objADO.GetDataTable("SAP_DOCUMENTO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarVinculadosparaSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@NuDocum", vstrNuDocum)
            Return objADO.GetDataTable("SAP_DOCUMENTO_Sel_DocsVinculados", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub EnviarSAP(ByVal vstrCoTipoDoc As String, ByVal vstrNuDocum As String, _
                                   ByVal vstrUserMod As String, ByVal vstrVerboController As String, Optional ByVal vchrTipoAnul As Char = "")
        Try
            Dim objInvoicesBE As clsInvoicesBE = Nothing
            If vstrVerboController = "POST" Then
                PasarDatosToSAP(vstrCoTipoDoc, vstrNuDocum, objInvoicesBE)
            ElseIf vstrVerboController = "PUT" Then
                PasarDatosAnulacionToSAP(vstrCoTipoDoc, vstrNuDocum, objInvoicesBE, vchrTipoAnul)
            End If

            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(objInvoicesBE, _
            '                                     "http://sap:8899/api/Invoices/", _
            '                                     vstrVerboController)
            oBTJsonMaster.EnviarDatosRESTFulJson(objInvoicesBE, _
                                               "http://sap:" + gstrPuertoSAP + "/api/Invoices/", _
                                               vstrVerboController)

            If vstrVerboController = "POST" Then
                ActualizarCoSAP(vstrCoTipoDoc, vstrNuDocum, objResponseSAP.Id, vstrUserMod)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub EnviarSAPAnticipo(ByVal vstrCoTipoDoc As String, ByVal vstrNuDocum As String, ByVal vstrUserMod As String, ByVal vstrVerboController As String)
        Try
            Dim objDownPaymentsBE As clsDownPaymentsBE = Nothing

            PasarDatosToSAPAnticipo(vstrCoTipoDoc, vstrNuDocum, objDownPaymentsBE)


            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            oBTJsonMaster.EnviarDatosRESTFulJson(objDownPaymentsBE, _
                                               "http://sap:" + gstrPuertoSAP + "/api/DownPayments/", _
                                               vstrVerboController)


            ActualizarCoSAP(vstrCoTipoDoc, vstrNuDocum, objResponseSAP.Id, vstrUserMod)

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Overloads Sub ActualizarCoSAP(ByVal vstrCoTipoDoc As String, ByVal vstrNuDocum As String, ByVal vstrCoSAP As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTipoDoc", vstrCoTipoDoc)
            dc.Add("@NuDocum", vstrNuDocum)
            dc.Add("@CoSAP", vstrCoSAP)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("DOCUMENTO_UpdCoSAP", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
        End Try
    End Sub

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDocumentoDetBT
    Inherits clsAdministracionBaseBT

    Private Overloads Sub InsertarManual(ByVal BE As clsDocumentoDetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", BE.NuDocum)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@IDMonedaCosto", BE.IDMonedaCosto)
                .Add("@NoServicio", BE.NoServicio)
                .Add("@SsCompraNeto", BE.SsCompraNeto)
                .Add("@SsIGVSFE", BE.SsIGVSFE)
                .Add("@SsTotalCosto", BE.SsTotalCosto)
                .Add("@SsMargen", BE.SsMargen)
                .Add("@SsTotalCostoUSD", BE.SsTotalCostoUSD)
                .Add("@SsTotalDocumUSD", BE.SsTotalDocumUSD)
                .Add("@CoTipoDet", BE.CoTipoDet)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_DET_InsManual", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Function Insertar(ByVal BE As clsDocumentoDetBE) As Byte
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", BE.NuDocum)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@IDVoucher", BE.IDVoucher)
                .Add("@NuOrdenServicio", BE.NuOrdenServicio)
                .Add("@IDMonedaCosto", BE.IDMonedaCosto)
                .Add("@SsCompraNeto", BE.SsCompraNeto)
                .Add("@SsIGVCosto", BE.SsIGVCosto)
                .Add("@SsIGVSFE", BE.SsIGVSFE)
                .Add("@SsTotalCosto", BE.SsTotalCosto)
                .Add("@IDTipoOC", BE.IDTipoOC)
                .Add("@SsMargen", BE.SsMargen)
                .Add("@SsTotalCostoUSD", BE.SsTotalCostoUSD)
                .Add("@SsTotalDocumUSD", BE.SsTotalDocumUSD)
                .Add("@CoTipoDet", BE.CoTipoDet)
                .Add("@NoServicio", BE.NoServicio)
                .Add("@UserMod", BE.UserMod)
                .Add("@pNuDetalle", 0)
            End With

            Dim bytNuDetalle As Byte = objADO.ExecuteSPOutput("DOCUMENTO_DET_Ins", dc)

            ContextUtil.SetComplete()

            Return bytNuDetalle
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Overloads Sub ActualizarManual(ByVal BE As clsDocumentoDetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDetalle", BE.NuDetalle)
                .Add("@NuDocum", BE.NuDocum)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@NoServicio", BE.NoServicio)
                .Add("@IDMonedaCosto", BE.IDMonedaCosto)
                .Add("@SsCompraNeto", BE.SsCompraNeto)
                .Add("@SsIGVSFE", BE.SsIGVSFE)
                .Add("@SsTotalCosto", BE.SsTotalCosto)
                .Add("@SsMargen", BE.SsMargen)
                .Add("@SsTotalCostoUSD", BE.SsTotalCostoUSD)
                .Add("@SsTotalDocumUSD", BE.SsTotalDocumUSD)
                .Add("@CoTipoDet", BE.CoTipoDet)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_DET_UpdManual", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsDocumentoDetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", BE.NuDocum)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@NuDetalle", BE.NuDetalle)
                .Add("@IDVoucher", BE.IDVoucher)
                .Add("@NuOrdenServicio", BE.NuOrdenServicio)

                .Add("@SsCompraNeto", BE.SsCompraNeto)
                .Add("@SsIGVCosto", BE.SsIGVCosto)
                .Add("@SsIGVSFE", BE.SsIGVSFE)
                .Add("@SsTotalCosto", BE.SsTotalCosto)
                .Add("@NoServicio", BE.NoServicio)
                .Add("@SsMargen", BE.SsMargen)
                .Add("@SsTotalCostoUSD", BE.SsTotalCostoUSD)
                .Add("@SsTotalDocumUSD", BE.SsTotalDocumUSD)
                .Add("@UserMod", BE.UserMod)

            End With
            objADO.ExecuteSP("DOCUMENTO_DET_Upd", dc)

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal vstrNuDocum As String, ByVal vstrIDTipoDoc As String, ByVal bytNuDetalle As Byte)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocum", vstrNuDocum)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@NuDetalle", bytNuDetalle)

            objADO.ExecuteSP("DOCUMENTO_DET_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Sub
    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsDocumentoBE)
        If BE.ListaDetalleDocumentos Is Nothing Then Exit Sub
        Try
            For Each Item As clsDocumentoBE In BE.ListaDocumentos
                EliminarFK(Item.NuDocum, Item.IDTipoDoc)
            Next


            'Dim ListaNuDetalle As New List(Of Byte)
            For Each Item As clsDocumentoDetBE In BE.ListaDetalleDocumentos

                'Dim strNoServicio As String = ""
                'If Item.CoTipoDet = "BV" Then
                '    strNoServicio = Item.NoServicio
                'End If

                'Dim bytNuDetalle As Byte = bytExisteDetDocumento(Item.IDCab, Item.IDProveedor, Item.IDTipoOC, _
                '                                Item.IDMonedaCosto, Item.CoTipoDet, Item.IDVoucher, strNoServicio)
                'If bytNuDetalle = 0 Then
                '    bytNuDetalle = Insertar(Item)
                'Else
                '    Item.NuDetalle = bytNuDetalle
                '    Actualizar(Item)
                'End If
                'ListaNuDetalle.Add(bytNuDetalle)
                Insertar(Item)
            Next

            'For Each Item As clsDocumentoDetBE In BE.ListaDetalleDocumentos
            '    Dim dttDetDocs As DataTable = ConsultarxFK(Item.NuDocum, Item.IDTipoDoc)
            '    For Each dr As DataRow In dttDetDocs.Rows
            '        If Not ListaNuDetalle.Contains(dr("NuDetalle")) Then
            '            Eliminar(Item.NuDocum, Item.IDTipoDoc, dr("NuDetalle"))
            '        End If
            '    Next
            'Next
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsDocumentoBE, ByVal vstrCoTipoDoc As String, ByVal vstrNuDocum As String)
        If BE.ListaDetalleDocumentos Is Nothing Then Exit Sub
        Try
            For Each Item As clsDocumentoBE In BE.ListaDocumentos
                If Item.NuDocum = vstrNuDocum And Item.IDTipoDoc = vstrCoTipoDoc Then
                    EliminarFK(Item.NuDocum, Item.IDTipoDoc)
                End If
            Next

            For Each Item As clsDocumentoDetBE In BE.ListaDetalleDocumentos
                If Item.NuDocum = vstrNuDocum And Item.IDTipoDoc = vstrCoTipoDoc Then
                    Insertar(Item)
                End If
            Next

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub
    Public Overloads Sub EliminarFK(ByVal vstrNuDocum As String, ByVal vstrIDTipoDoc As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", vstrNuDocum)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
            End With
            objADO.ExecuteSP("DOCUMENTO_DET_DelFk", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminarManual(ByVal BE As clsDocumentoBE)
        Try
            For Each Item As clsDocumentoDetBE In BE.ListaDetalleDocumentos
                If Item.Accion = "N" Then
                    InsertarManual(Item)
                ElseIf Item.Accion = "M" Then
                    ActualizarManual(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.NuDocum, Item.IDTipoDoc, Item.NuDetalle)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Function ConsultarxFK(ByVal vstrNuDocum As String, ByVal vstrIDTipoDoc As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocum", vstrNuDocum)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)

            Return objADO.GetDataTable("DOCUMENTO_DET_Sel_FK", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function bytExisteDetDocumento(ByVal vintIDCab As Integer, _
                                           ByVal vstrIDProveedor As String, _
                                           ByVal vstrIDTipoOC As String, _
                                           ByVal vstrIDMoneda As String, _
                                           ByVal vstrCoTipoDet As String, _
                                           ByVal vstrIDVoucher As String, _
                                           ByVal vstrNoServicio As String) As Byte
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDTipoOC", vstrIDTipoOC)
                .Add("@IDMoneda", vstrIDMoneda)
                .Add("@CoTipoDet", vstrCoTipoDet)
                .Add("@IDVoucher", vstrIDVoucher)
                .Add("@NoServicio", vstrNoServicio)
                .Add("@pNuDetalle", 0)
            End With
            Return objADO.ExecuteSPOutput("DOCUMENTO_DET_SelExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDocumentoDet_TextoBT
    Inherits clsAdministracionBaseBT

    Private Overloads Sub Insertar(ByVal BE As clsDocumentoDet_TextoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", BE.NuDocum)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@NoTexto", BE.NoTexto)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_DET_TEXTO_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsDocumentoDet_TextoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", BE.NuDocum)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@NuDetalle", BE.NuDetalle)
                .Add("@NoTexto", BE.NoTexto)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("DOCUMENTO_DET_TEXTO_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vstrNuDocum As String, ByVal vstrIDTipoDoc As String, ByVal vbytNuDetalle As Byte)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", vstrNuDocum)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
                .Add("@NuDetalle", vbytNuDetalle)
            End With
            objADO.ExecuteSP("DOCUMENTO_DET_TEXTO_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarFK(ByVal vstrNuDocum As String, ByVal vstrIDTipoDoc As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", vstrNuDocum)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
            End With
            objADO.ExecuteSP("DOCUMENTO_DET_TEXTO_DelFk", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsDocumentoBE)
        Try
            For Each Item As clsDocumentoDet_TextoBE In BE.ListaDetalleDocumentos_Texto
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.NuDocum, Item.IDTipoDoc, Item.NuDetalle)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Function ConsultarparaSAP(ByVal vstrIDTipoDoc As String, ByVal vstrNuDocum As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@NuDocum", vstrNuDocum)
            Return objADO.GetDataTable("SAP_DOCUMENTO_DET_TEXTO_Sel_Fk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleDebitMemoBT
    Inherits clsAdministracionBaseBT

    Private Overloads Sub Insertar(ByVal BE As clsDetalleDebitMemoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDebitMemo", BE.IDDebitMemo)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)
                .Add("@CapacidadHab", BE.CapacidadHab)
                .Add("@NroPax", BE.NroPax)
                .Add("@CostoPersona", BE.CostoPersona)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("DEBIT_MEMO_DET_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsDetalleDebitMemoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDebitMemo", BE.IDDebitMemo)
                .Add("@IDDebitMemoDet", BE.IDDebitMemoDet)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)
                .Add("@CapacidadHab", BE.CapacidadHab)
                .Add("@NroPax", BE.NroPax)
                .Add("@CostoPersona", BE.CostoPersona)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("DEBIT_MEMO_DET_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vstrIDDebitMemo As String, ByVal vstrIDDebitMemoDet As Byte)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDebitMemo", vstrIDDebitMemo)
                .Add("@IDDebitMemoDet", vstrIDDebitMemoDet)
            End With

            objADO.ExecuteSP("DEBIT_MEMO_DET_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarxIDDebitMemoDet(ByVal vstrIDDebitMemo As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDebitMemo", vstrIDDebitMemo)
            End With

            objADO.ExecuteSP("DEBIT_MEMO_DET_DelxIDDebitMemo", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsDebitMemoBE)
        If BE.ListaDetalleDebitMemo Is Nothing Then Exit Sub
        Try
            For Each Item As clsDetalleDebitMemoBE In BE.ListaDetalleDebitMemo
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.IDDebitMemo, Item.IDDebitMemoDet)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub


End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsNotaVentaBT
    Inherits clsAdministracionBaseBT

    Public Overloads Function Insertar(ByVal BE As clsNotaVentaBE) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@IDSerie", BE.IDSerie)
                .Add("@IDCab", BE.IDCab)
                .Add("@IDFile", BE.IDFile)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)
                .Add("@IDEstado", BE.IDEstado)
                .Add("@UserMod", BE.UserMod)
                .Add("@pIDNotaVenta", "")
            End With

            Dim strIDNotaVenta As String = objADO.ExecuteSPOutput("NOTA_VENTA_Ins", dc)
            ActualizarIDsNuevosenHijosST(BE, strIDNotaVenta)

            Dim objBTDet As New clsDetalleNotaVentaBT
            objBTDet.InsertarActualizarEliminar(BE)


            ContextUtil.SetComplete()
            Return strIDNotaVenta

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Overloads Sub Actualizar(ByVal BE As clsPreLiquidacionBE, _
                                ByVal vstrIDNotaVenta As String, _
                                ByVal vstrIDEstado As String, _
                                ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            ActualizarEstado(vstrIDNotaVenta, vstrIDEstado, vstrUserMod)

            Dim objBT As New clsDetallePreLiquidacionBT
            objBT.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub
    Public Sub ActualizarEstado(ByVal vstrIDNotaVenta As String, _
                                ByVal vstrIDEstado As String, _
                                ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNotaVenta", vstrIDNotaVenta)
            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("NOTA_VENTA_UpdEstado", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsNotaVentaBE, ByVal vstrNuevoIDNotaVenta As String)
        If BE.ListaDetalleNotaVenta Is Nothing Then Exit Sub
        Try
            For Each ItemDet As clsDetalleNotaVentaBE In BE.ListaDetalleNotaVenta
                If ItemDet.IDNotaVenta = "" Then
                    ItemDet.IDNotaVenta = vstrNuevoIDNotaVenta
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarTotales(ByVal vstrIDNotaVenta As String, _
                                          ByVal vdblSubTotal As Double, _
                                          ByVal vdblTotalIGV As Double, _
                                          ByVal vdblTotal As Double, _
                                          ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDNotaVenta", vstrIDNotaVenta)
                .Add("@SubTotal", vdblSubTotal)
                .Add("@TotalIGV", vdblTotalIGV)
                .Add("@Total", vdblTotal)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("NOTA_VENTA_UpdTotales", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub CargarListaDetNotaVenta(ByVal BERes As clsReservaBE, _
                                             ByVal vstrIDNotaVenta As String, _
                                             ByRef rListaDetNotVta As List(Of clsDetalleNotaVentaBE))

        Try

            For Each ItemDetRes As clsReservaBE.clsDetalleReservaBE In BERes.ListaDetReservas

                Dim ItemDetNotaVenta As New clsDetalleNotaVentaBE
                With ItemDetNotaVenta
                    .IDNotaVenta = vstrIDNotaVenta
                    .IDReserva_Det = ItemDetRes.IDReserva_Det
                    .SubTotal = ItemDetRes.NetoGen

                    .TotalIGV = ItemDetRes.IgvGen
                    .Total = (.SubTotal + .TotalIGV)
                    .Descripcion = ItemDetRes.Servicio
                    .UserMod = BERes.UserMod
                    .Accion = "N"
                End With

                rListaDetNotVta.Add(ItemDetNotaVenta)

            Next

        Catch ex As Exception
            Throw
        End Try


    End Sub

    Public Function GenerarxFile(ByVal BERes As clsReservaBE) As String
        Try

            Dim dblTotal As Double = 0, dblSubTotal As Double = 0, dblTotalIGV As Double = 0
            Dim ListaDetalleNotaVenta As New List(Of clsDetalleNotaVentaBE)


            Dim ItemNotaVenta As New clsNotaVentaBE With _
            {.IDSerie = "001", _
            .IDCab = BERes.IDCab, _
            .IDFile = BERes.IDFile, _
            .IDMoneda = "USD", _
            .SubTotal = dblSubTotal, _
            .TotalIGV = dblTotalIGV, _
            .Total = dblTotal, _
            .IDEstado = "PD", _
            .IDTipo = "I", _
            .UserMod = BERes.UserMod, _
            .Accion = "N"}

            Dim strIDNotaVenta As String = BERes.IDFile
            Dim objBN As New clsNotaVentaBN
            If Not objBN.blnExisteEnFile(BERes.IDCab) Then
                strIDNotaVenta = Insertar(ItemNotaVenta)
            Else
                Dim objBT As New clsNotaVentaBT
                objBT.ActualizarEstado(BERes.IDFile, "PD", BERes.UserMod)

            End If


            CargarListaDetNotaVenta(BERes, strIDNotaVenta, ListaDetalleNotaVenta)

            ItemNotaVenta.ListaDetalleNotaVenta = ListaDetalleNotaVenta

            Dim objDet As New clsDetalleNotaVentaBT
            objDet.InsertarActualizarEliminar(ItemNotaVenta)

            For Each ItemNotaVtaDet As clsDetalleDebitMemoBE In ListaDetalleNotaVenta
                dblTotal += ItemNotaVtaDet.Total
                dblSubTotal += ItemNotaVtaDet.SubTotal
                dblTotalIGV += ItemNotaVtaDet.TotalIGV
            Next

            ActualizarTotales(strIDNotaVenta, dblSubTotal, dblTotalIGV, dblTotal, BERes.UserMod)


            ContextUtil.SetComplete()

            Return strIDNotaVenta
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Function

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleNotaVentaBT
    Inherits clsAdministracionBaseBT

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsNotaVentaBE)
        If BE.ListaDetalleNotaVenta Is Nothing Then Exit Sub
        Try
            For Each Item As clsDetalleDebitMemoBE In BE.ListaDetalleNotaVenta

                Insertar(Item)

            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Insertar(ByVal BE As clsDetalleNotaVentaBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDNotaVenta", BE.IDNotaVenta)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@IDReserva_Det", BE.IDReserva_Det)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)

                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("NOTA_VENTA_DET_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetallePreLiquidacionBT
    Inherits clsAdministracionBaseBT
    Private Overloads Sub Insertar(ByVal BE As clsDetallePreLiquidacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDNotaVenta", BE.IDNotaVenta)
                .Add("@IDOperacion_Det", BE.IDOperacion_Det)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@IDTipoOC", BE.IDTipoOC)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("PRE_LIQUI_DET_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsDetallePreLiquidacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDNotaVenta", BE.IDNotaVenta)
                .Add("@IDPreLiquiDet", BE.IDPreLiquiDet)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@IDTipoOC", BE.IDTipoOC)
                .Add("@SubTotal", BE.SubTotal)
                .Add("@TotalIGV", BE.TotalIGV)
                .Add("@Total", BE.Total)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("PRE_LIQUI_DET_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vstrIDNotaVenta As String, ByVal vbytIDPreLiquidacion As Byte)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDNotaVenta", vstrIDNotaVenta)
                .Add("@IDPreLiquiDet", vbytIDPreLiquidacion)
            End With

            objADO.ExecuteSP("PRE_LIQUI_DET_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsPreLiquidacionBE)
        Try
            For Each Item As clsDetallePreLiquidacionBE In BE.ListaDetallePreLiquidacion
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.IDNotaVenta, Item.IDPreLiquiDet)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Sub CargarListaDetOperaciones(ByVal BEOpe As clsOperacionBE, _
                                             ByVal vstrIDNotaVenta As String, _
                                             ByRef rListaDetPreLiq As List(Of clsDetallePreLiquidacionBE))

        Try

            For Each ItemDetOpe As clsDetalleOperacionBE In BEOpe.ListaDetalleOperaciones

                Dim ItemDetPreLiq As New clsDetallePreLiquidacionBE
                With ItemDetPreLiq
                    .IDNotaVenta = vstrIDNotaVenta
                    .IDOperacion_Det = ItemDetOpe.IDOperacion_Det
                    .SubTotal = ItemDetOpe.NetoGen
                    .IDTipoOC = ItemDetOpe.IDTipoOC
                    .TotalIGV = ItemDetOpe.IgvGen
                    .Total = (.SubTotal + .TotalIGV)
                    .Descripcion = ItemDetOpe.Servicio
                    .UserMod = BEOpe.UserMod
                    .Accion = "N"
                End With

                rListaDetPreLiq.Add(ItemDetPreLiq)

            Next

        Catch ex As Exception
            Throw
        End Try

    End Sub


    Public Sub GenerarxFile(ByVal BEOpe As clsOperacionBE, ByVal vstrIDNotaVenta As String)
        Try

            Dim dblTotal As Double = 0, dblSubTotal As Double = 0, dblTotalIGV As Double = 0
            Dim ListaDetallePreLiquidacion As New List(Of clsDetallePreLiquidacionBE)


            CargarListaDetOperaciones(BEOpe, vstrIDNotaVenta, ListaDetallePreLiquidacion)

            Dim ItemPreLiq As New clsPreLiquidacionBE
            ItemPreLiq.ListaDetallePreLiquidacion = ListaDetallePreLiquidacion

            Dim objDet As New clsDetallePreLiquidacionBT
            objDet.InsertarActualizarEliminar(ItemPreLiq)

            'For Each ItemNotaVtaDet As clsDetalleDebitMemoBE In ListaDetalleNotaVenta
            '    dblTotal += ItemNotaVtaDet.Total
            '    dblSubTotal += ItemNotaVtaDet.SubTotal
            '    dblTotalIGV += ItemNotaVtaDet.TotalIGV
            'Next

            'ActualizarTotales(strIDNotaVenta, dblSubTotal, dblTotalIGV, dblTotal, BEOpe.UserMod)


            ContextUtil.SetComplete()


        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsPresupuesto_SobreBT
    Inherits clsAdministracionBaseBT

    Public Overloads Function Insertar(ByVal BE As clsPresupuesto_SobreBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", BE.IDCab)
                '.Add("@CoPrvInt", BE.CoPrvInt)
                .Add("@CoPrvGui", BE.CoPrvGui)
                .Add("@IDPax", BE.IDPax)
                .Add("@CoEjeOpe", BE.CoEjeOpe)
                .Add("@CoEjeFin", BE.CoEjeFin)
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@UserMod", BE.UserMod)
                .Add("@FePreSob", BE.FePreSob)
                .Add("@CoTipoProv", BE.CoTipoProv)
                .Add("@FlTourConductor", BE.FlTourConductor)
                .Add("@pNuPreSob", 0)
            End With

            Dim intNuPreSob As Integer = objADO.ExecuteSPOutput("PRESUPUESTO_SOBRE_Ins", dc)


            ActualizarIDsNuevosenHijosST(BE, intNuPreSob, BE.NuPreSob)


            Dim objDet As New clsPresupuesto_Sobre_DetBT
            objDet.InsertarActualizarEliminar(BE)

            ActualizarSaldo(intNuPreSob, BE.UserMod)

            Dim ObjFondoFijo As New clsFondoFijoBT
            ObjFondoFijo.Actualizar_Saldo_Pres(intNuPreSob, BE.UserMod)

            ContextUtil.SetComplete()
            Return intNuPreSob
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function
    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsPresupuesto_SobreBE, ByVal vintNuevoNuPreSob As Integer, _
                                                        ByVal vintNuevoNuPreSobAnt As Integer)
        If BE.ListaPresupuestoSobreDet Is Nothing Then Exit Sub
        Try
            For Each ItemDet As clsPresupuesto_Sobre_DetBE In BE.ListaPresupuestoSobreDet
                If ItemDet.NuPreSob = vintNuevoNuPreSobAnt Then
                    ItemDet.NuPreSob = vintNuevoNuPreSob
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarEstado(ByVal vintNuPreSo As Integer, ByVal vstrCoEstado As String, _
                                ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSo)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_Anu", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarSaldo(ByVal vintNuPreSo As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSo)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_UpdSaldoIni", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsPresupuesto_SobreBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuPreSob", BE.NuPreSob)
                '.Add("@CoPrvInt", BE.CoPrvInt)
                .Add("@CoPrvGui", BE.CoPrvGui)
                .Add("@CoEjeOpe", BE.CoEjeOpe)
                .Add("@CoEjeFin", BE.CoEjeFin)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@FePreSob", BE.FePreSob)
                .Add("@NuCodigo_ER", BE.NuCodigo_ER)
                .Add("@CoProveedor", BE.IDProveedor)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_Upd", dc)

            Dim objDet As New clsPresupuesto_Sobre_DetBT
            objDet.InsertarActualizarEliminar(BE)

            ActualizarSaldo(BE.NuPreSob, BE.UserMod)

            Dim ObjFondoFijo As New clsFondoFijoBT
            ObjFondoFijo.Actualizar_Saldo_Pres(BE.NuPreSob, BE.UserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar_SoloDetalle(ByVal BE As clsPresupuesto_SobreBE)
        Dim dc As New Dictionary(Of String, String)
        Try

            Dim objDet As New clsPresupuesto_Sobre_DetBT
            objDet.InsertarActualizarEliminar_RecepDoc(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuPreSob As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_DelxFK", dc)
            objADO.ExecuteSP("PRESUPUESTO_SOBRE_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarxDocumento(ByVal BE As clsPresupuesto_SobreBE, ByVal BEUpdDet As clsPresupuesto_SobreBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuPreSob", BE.NuPreSob)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo)
                .Add("@SsDifAceptada", BE.SsDifAceptada)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_Upd_UpdxDocumento", dc)

            ActualizarxDocumento_USD(BE)

            Actualizar_SoloDetalle(BEUpdDet)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
            Throw
        End Try
    End Sub

    Private Overloads Sub ActualizarxDocumento_USD(ByVal BE As clsPresupuesto_SobreBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuPreSob", BE.NuPreSob)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@SsSaldo", BE.SsSaldo_USD)
                .Add("@SsDifAceptada", BE.SsDifAceptada_USD)
                .Add("@TxObsDocumento", BE.TxObsDocumento)
                .Add("@FlDesdeOtraForEgreso", BE.FlDesdeOtraForEgreso)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_Upd_UpdxDocumento_USD", dc)



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
            Throw
        End Try
    End Sub

    Public Function ActualizarxDocumentoSOLUSD(ByVal BESol As clsPresupuesto_SobreBE, ByVal BEDol As clsPresupuesto_SobreBE, ByVal BEDetPre As clsPresupuesto_SobreBE) As List(Of String)
        Try
            'ActualizarxDocumento(BESol)
            'ActualizarxDocumento_USD(BEDol)

            'Actualizar_SoloDetalle(BEDetPre)

            'Dim objBT As New clsDocumentoProveedorBT
            'objBT.GrabarxStockPresupuesto(BEDetPre)
            Dim objBN As New clsDocumentoProveedorBN
            Dim ListErrores As List(Of String) = objBN.lstGrabarxStockPresupuesto(BEDetPre, BESol, BEDol)



            ContextUtil.SetComplete()

            Return ListErrores
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function


    Public Overloads Sub ActualizarSaldoPendiente_USD(ByVal vintNuPreSob As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_UpdSsPendientexDoc_USD", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente(ByVal vintNuPreSob As Integer, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarEstadoAC_DifAceptada(ByVal vintNuPreSob As Integer, ByVal vdblSsTotal As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@SsTotal", vdblSsTotal)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_UpdEstado_AC_DifAceptada", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Overloads Sub ActualizarSaldoPendiente_Detalle(ByVal vintNuPreSob As Integer, ByVal vbytNuDetPSo As Byte, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String, Optional ByVal vblnSAP As Boolean = False)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@NuDetPSo", vbytNuDetPSo)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_UpdSsPendientexDoc_Detalle", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarSaldoPendiente_Detalle_USD(ByVal vintNuPreSob As Integer, ByVal vbytNuDetPSo As Byte, ByVal vdblSsSaldoPendiente As Double, ByVal vstrUserMod As String, Optional ByVal vblnSAP As Boolean = False)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@NuDetPSo", vbytNuDetPSo)
            dc.Add("@SsTotalOrig", vdblSsSaldoPendiente)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_UpdSsPendientexDoc_Detalle_USD", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsPresupuesto_Sobre_DetBT
    Inherits clsAdministracionBaseBT

    Private Overloads Sub Insertar(ByVal BE As clsPresupuesto_Sobre_DetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuPreSob", BE.NuPreSob)
                .Add("@CoTipPSo", BE.CoTipPSo)
                .Add("@FeDetPSo", Format(BE.FeDetPSo, "dd/MM/yyyy HH:mm")) ' Convert.ToString(BE.FeDetPSo).Substring(0, 16))
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@IDOperacion_Det", BE.IDOperacion_Det)
                .Add("@NuSincerado_Det", BE.NuSincerado_Det)
                .Add("@TxServicio", BE.TxServicio)
                .Add("@QtPax", BE.QtPax)
                .Add("@SsPreUni", BE.SsPreUni)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@SsTotSus", BE.SsTotSus)
                .Add("@SsTotDev", BE.SsTotDev)
                .Add("@CoPago", BE.CoPago)
                .Add("@CoPrvPrg", BE.CoPrvPrg)
                .Add("@IDPax", BE.IDPax)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@IDServicio_Det_V_Cot", BE.IDServicio_Det_V_Cot)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsPresupuesto_Sobre_DetBE, Optional ByVal vblnSAP As Boolean = False)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDetPSo", BE.NuDetPSo)
                .Add("@NuPreSob", BE.NuPreSob)
                .Add("@CoTipPSo", BE.CoTipPSo)
                .Add("@FeDetPSo", Format(BE.FeDetPSo, "dd/MM/yyyy HH:mm")) ' Convert.ToString(BE.FeDetPSo).Substring(0, 16))
                .Add("@TxServicio", BE.TxServicio)
                .Add("@QtPax", BE.QtPax)
                .Add("@SsPreUni", BE.SsPreUni)
                .Add("@SsTotal", BE.SsTotal)
                .Add("@SsTotSus", BE.SsTotSus)
                .Add("@SsTotDev", BE.SsTotDev)
                .Add("@CoPago", BE.CoPago)
                .Add("@CoPrvPrg", BE.CoPrvPrg)
                .Add("@IDMoneda", BE.IDMoneda)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_Upd", dc)

            If vblnSAP Then
                'enviar a sap el json de purchaseinvoice

            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintNuPreSob As Integer, ByVal vbytNuDetPSo As Byte)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDetPSo", vbytNuDetPSo)
            dc.Add("@NuPreSob", vintNuPreSob)
            objADO.ExecuteSP("PRESUPUESTO_SOBRE_DET_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsPresupuesto_SobreBE)
        If BE.ListaPresupuestoSobreDet Is Nothing Then Exit Sub
        Try
            For Each Item As clsPresupuesto_Sobre_DetBE In BE.ListaPresupuestoSobreDet
                If Item.Accion = "N" Then
                    Insertar(Item)
                    AnularLogPresupuestos(Item, "T")
                    AnularLogPresupuestos(Item, "N")
                ElseIf Item.Accion = "M" Then

                    Actualizar(Item)

                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.NuDetPSo) Then
                        Eliminar(Item.NuPreSob, Item.NuDetPSo)
                    End If
                End If
            Next

            'UpdateStockxServicios
            ActualizarStockEntradas(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Sub ActualizarStockEntradas(ByVal BE As clsPresupuesto_SobreBE)
        Try
            For Each Item As clsPresupuesto_Sobre_DetBE In BE.ListaPresupuestoSobreDet
                If BE.CoEstado = "EN" And Item.CoPago = "STK" Then
                    'Dim blnExcede As Boolean = False
                    Dim intQtExcedente As Int16 = 0
                    Dim intCodStockMax As Int16 = 0
                    Dim intQtSaldo As Int16 = 0
                    Dim intQtEnt As Int16 = Item.QtPax
                    Dim strCodSaldo As String = ""
Inicio:
                    If Not (Item.SsTotDev <> 0 Or Item.SsTotSus <> 0) Then 'desde presupuestos

                        'Dim intCodStockMax As Int16 = intDevuelveCodStockMax(Item.IDServicio_Det_V_Cot)
                        'If strCodSaldo = "" Then
                        strCodSaldo = strDevuelveCodStockMax(Item.IDServicio_Det_V_Cot)
                        'End If
                        If strCodSaldo = "0-0" Then GoTo Sgte

                        intQtSaldo = Mid(strCodSaldo, InStr(strCodSaldo, "-") + 1)
                        intCodStockMax = Left(strCodSaldo, InStr(strCodSaldo, "-") - 1)

                        'If intCodStockMax > 0 Then
                        If intQtExcedente <> 0 Then
                            intQtEnt = intQtExcedente
                            intQtExcedente = 0
                        Else
                            If intQtEnt + intQtExcedente > intQtSaldo Then
                                intQtExcedente = intQtEnt - intQtSaldo
                                intQtEnt = intQtSaldo
                            Else
                                'intQtEnt = intQtExcedente
                                intQtExcedente = 0
                            End If
                        End If


                    End If


                    Dim objBTCotiCruce As New clsCotiStockTicketEntradasIngresoBT
                    With objBTCotiCruce

                        If Item.SsTotDev <> 0 Or Item.SsTotSus <> 0 Then
                            intQtEnt = Item.SsTotDev * (-1)
                            If intQtEnt < 0 Then
                                .EliminarxIDCabxIDServicioDet(BE.IDCab, Item.IDServicio_Det_V_Cot)
                                If intCodStockMax = 0 Then
                                    strCodSaldo = strDevuelveCodStockMax(Item.IDServicio_Det_V_Cot)
                                    'End If
                                    'If strCodSaldo = "" Then GoTo Sgte
                                    'If strCodSaldo <> "" Then
                                    intQtSaldo = Mid(strCodSaldo, InStr(strCodSaldo, "-") + 1)
                                    intCodStockMax = Left(strCodSaldo, InStr(strCodSaldo, "-") - 1)
                                    'End If
                                End If
                            End If


                        End If


                        If intQtEnt <> 0 Then
                            .Insertar(New clsCotiStockTicketEntradasIngresoBE With { _
                                                                  .IDCab = BE.IDCab, _
                                                                  .IDServicio_Det = Item.IDServicio_Det_V_Cot, _
                                                                  .NuTckEntIng = intCodStockMax, _
                                                                  .QtUtilizada = intQtEnt, _
                                                                  .UserMod = Item.UserMod})

                            Dim oBTStock As New clsStockTicketEntradasBT
                            oBTStock.ActualizarCantidadTickets(New clsStockTicketEntradasBE With { _
                                                               .IDServicio_Det = Item.IDServicio_Det_V_Cot, _
                                                               .UserMod = Item.UserMod}, BE.IDCab)
                        End If

                    End With

                    If intQtExcedente > 0 Then
                        intQtEnt = 0
                        GoTo Inicio
                    End If


                    'End If
                End If
Sgte:
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Overloads Sub InsertarActualizarEliminar_RecepDoc(ByVal BE As clsPresupuesto_SobreBE)
        If BE.ListaPresupuestoSobreDet Is Nothing Then Exit Sub
        Try
            For Each Item As clsPresupuesto_Sobre_DetBE In BE.ListaPresupuestoSobreDet
                If Item.Accion = "N" Then
                    Insertar(Item)
                    AnularLogPresupuestos(Item, "T")
                    AnularLogPresupuestos(Item, "N")
                ElseIf Item.Accion = "M" Then
                    'actualizar saldo en recepcion de documentos
                    If Item.CoPago <> "STK" Then
                        Dim objBN As New clsPresupuesto_SobreBT
                        If Item.IDMoneda = "SOL" Then
                            objBN.ActualizarSaldoPendiente_Detalle(Item.NuPreSob, Item.NuDetPSo, Item.SsTotSus, Item.UserMod)
                        ElseIf Item.IDMoneda = "USD" Then
                            objBN.ActualizarSaldoPendiente_Detalle_USD(Item.NuPreSob, Item.NuDetPSo, Item.SsTotSus, Item.UserMod)
                        End If

                        Actualizar(Item)
                    End If



                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.NuDetPSo) Then
                        Eliminar(Item.NuPreSob, Item.NuDetPSo)
                    End If
                End If
            Next


            'ActualizarStockEntradas(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub AnularLogPresupuestos(ByVal BE As clsPresupuesto_Sobre_DetBE, ByVal vchrCoAccion As Char)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOperacion_Det", BE.IDOperacion_Det)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
            dc.Add("@FlAnulado", True)
            dc.Add("@CoAccion", vchrCoAccion)
            dc.Add("@UserMod", BE.UserMod)
            objADO.ExecuteSP("OPERACIONES_DET_LOGPRESUPUESTOS_UpdAnulado", dc)
            dc.Add("@IDServicio_Det_V_Cot", BE.IDServicio_Det_V_Cot)
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_LOGPRESUPUESTOS_UpdAnulado", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    'Private Function intDevuelveCodStockMax(ByVal vintIDServicio_Det As Integer) As Int16
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@IDServicio_Det_V_Cot", vintIDServicio_Det)
    '        dc.Add("@pNuTckEntIng", 0)

    '        Return objADO.GetSPOutputValue("STOCK_TICKET_ENTRADAS_INGRESOS_SeNuTckEntlOutput", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function
    Private Function strDevuelveCodStockMax(ByVal vintIDServicio_Det As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio_Det_V_Cot", vintIDServicio_Det)
            dc.Add("@pNuTckEntIng_Saldo", "")

            Return objADO.GetSPOutputValue("STOCK_TICKET_ENTRADAS_INGRESOS_SeNuTckEntlOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsCotiStockTicketEntradasIngresoBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsCotiStockTicketEntradasIngresoBT"

    Public Overloads Sub EliminarxIDCabxIDServicioDet(ByVal vintIDCab As Integer, ByVal vintIDServicio_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDServicio_Det_V_Cot", vintIDServicio_Det)

            objADO.ExecuteSP("COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDCabxIDServicioDet")
        End Try
    End Sub

    Public Overloads Sub Insertar(ByVal BE As clsCotiStockTicketEntradasIngresoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuTckEntIng", BE.NuTckEntIng)
                .Add("@IDServicio_Det_V_Cot", BE.IDServicio_Det)
                .Add("@IDCab", BE.IDCab)
                .Add("@QtUtilizada", BE.QtUtilizada)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsStockTicketEntradasIngresoBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsStockTicketEntradasIngresoBT"

    Private Overloads Sub Insertar(ByVal BE As clsStockTicketEntradasIngresoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicio", BE.IDServicio)
                .Add("@QttkComprado", BE.QtEntCompradas)
                .Add("@FeComprada", BE.FeComprada.ToShortDateString)
                .Add("@FlSustentado", BE.FlSustentado)
                .Add("@IDTipoDocSus", BE.IDTipoDocSus)
                .Add("@NroDocSus", BE.NroDocSus)
                .Add("@NuOperBan", BE.NuOperBan)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("STOCK_TICKET_ENTRADAS_INGRESOS_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsStockTicketEntradasIngresoBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuTckEntIng", BE.NuTckEntIng)
                .Add("@IDServicio", BE.IDServicio)
                .Add("@FlSustentado", BE.FlSustentado)
                .Add("@IDTipoDocSus", BE.IDTipoDocSus)
                .Add("@NroDocSus", BE.NroDocSus)
                .Add("@NuOperBan", BE.NuOperBan)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("STOCK_TICKET_ENTRADAS_INGRESOS_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsStockTicketEntradasBE)
        If BE.ListStockTicketEntIngreso Is Nothing Then Exit Sub
        Try
            For Each Item As clsStockTicketEntradasIngresoBE In BE.ListStockTicketEntIngreso
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsStockTicketEntradasBT
    Inherits clsAdministracionBaseBT

    Dim strNombreClase As String = "clsStockTicketEntradasBT"

    Public Overloads Sub Insertar(ByVal BE As clsStockTicketEntradasBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", BE.IDServicio)
            dc.Add("@QtSkEnt", BE.QtStkEnt)
            dc.Add("@QtEntCompradas", BE.QtEntCompradas)
            dc.Add("@UserMod", BE.UserMod)
            objADO.ExecuteSP("STOCK_TICKET_ENTRADAS_Ins", dc)

            Dim objBTIngresos As New clsStockTicketEntradasIngresoBT
            objBTIngresos.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsStockTicketEntradasBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", BE.IDServicio)
            dc.Add("@QtStkEnt", BE.QtStkEnt)
            dc.Add("@QtEntCompradas", BE.QtEntCompradas)
            dc.Add("@UserMod", BE.UserMod)
            objADO.ExecuteSP("STOCK_TICKET_ENTRADAS_Upd", dc)

            Dim objBTIngresos As New clsStockTicketEntradasIngresoBT
            objBTIngresos.InsertarActualizarEliminar(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub ActualizarCantidadTickets(ByVal BE As clsStockTicketEntradasBE, ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio_Det_V_Cot", BE.IDServicio_Det)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("STOCK_TICKET_ENTRADAS_UpdEntradas", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCantidadTickets")
        End Try
    End Sub
End Class
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsVentaAdicionalBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsVentaAdicionalBT"
    Public pstrIDClienteVtasAdicionalesAPT As String '= "001922"
    Public pstrIDClienteAPT As String '= "000054"
    Public pstrBuenosAires As String
    Public pstrSantiago As String
    Public pstrCoUbigeo_Oficina As String
    Public psglIGV As Single
    Public Overloads Sub Insertar(ByVal BE As clsVentaAdicionalBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@CoUbigeo", BE.CoUbigeo)
            dc.Add("@CoTipo", BE.CoTipo)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@pNuVtaAdi", 0)

            Dim intNuVtaAdi As Integer = objADO.ExecuteSPOutput("VENTA_ADICIONAL_Ins", dc)

            ActualizarIDsNuevosenHijosST(BE, intNuVtaAdi)

            Dim objDet As New clsDetalleVentaAdicionalBT
            objDet.InsertarEliminar(BE)

            If BE.CoTipo = "SE" Then ActualizarClientesxPaxaBE(BE)

            InsertarDebitMemo(BE)
            If BE.CoTipo = "SE" Then
                InsertarBoletasVta(BE)
                Dim objDebitMemo As New clsDebitMemoBT
                Dim ListaDebitMemos As New List(Of clsDebitMemoBE)
                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    Dim BEDM As New clsDebitMemoBE With {.IDDebitMemo = Item.IDDebitMemo, .UserMod = Item.UserMod}
                    ListaDebitMemos.Add(BEDM)
                Next
                Dim BEDoc As New clsDocumentoBE With {.ListaDebitMemos = ListaDebitMemos}
                objDebitMemo.ActualizarFlagsFacturado(BEDoc)
                'ElseIf BE.CoTipo = "FC" Then
                'InsertarPaxenModulos(BE)
            End If

            InsertarPaxenModulos(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Function dtIDsModulosxServicio(ByVal vstrCoServicio As String, ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoServicio", vstrCoServicio)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("COTIDET_SelIDsxServicioFile", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".drIDsModulosxServicio")
        End Try
    End Function

    Private Sub InsertarPaxenModulos(ByVal BE As clsVentaAdicionalBE)
        Try
            Dim ListaCoServicio As New List(Of String)
            For Each ItemDet As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                'If vstrCoTipoAPT = "FC" Then
                '    If Not ListaCoServicio.Contains(ItemDet.CoServicio) Then
                '        ListaCoServicio.Add(ItemDet.CoServicio)
                '    End If
                'ElseIf vstrCoTipoAPT = "SE" Then
                If Not ListaCoServicio.Contains(ItemDet.IDServicio) Then
                    ListaCoServicio.Add(ItemDet.IDServicio)
                End If
                'End If
            Next

            For Each strCoServicio In ListaCoServicio
                Dim ListaIDs As New List(Of String)
                Using dtt As DataTable = dtIDsModulosxServicio(strCoServicio, BE.IDCab)
                    For Each dr As DataRow In dtt.Rows
                        ListaIDs.Add(dr("IDDet") & "|" & dr("IDReserva_Det") & "|" & dr("IDOperacion_Det"))
                    Next

                End Using

                For Each strIDs As String In ListaIDs
                    Dim ArrIDs() As String = strIDs.Split("|")
                    Dim intIDDet As Integer = ArrIDs(0).Trim
                    Dim intIDReserva_Det As Integer = ArrIDs(1).Trim
                    Dim intIDOperaciones_Det As Integer = ArrIDs(2).Trim
                    If intIDDet + intIDReserva_Det + intIDOperaciones_Det > 0 Then
                        If intIDDet > 0 Then
                            Dim ListaDetPax As New List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE)
                            For Each ItemDet As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                                If ItemDet.IDServicio = strCoServicio Then
                                    Dim NewPax As New clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE _
                                        With {.IDDet = intIDDet, .IDPax = ItemDet.NuPax, .UserMod = BE.UserMod, .Accion = "N"}
                                    ListaDetPax.Add(NewPax)
                                End If
                            Next
                            Dim objBEVtas As New clsCotizacionBE With {.ListaDetPax = ListaDetPax}
                            Dim objVtasDetPax As New clsDetallePaxVentasCotizacionBT
                            objVtasDetPax.InsertarActualizarEliminar(objBEVtas)
                            Dim objVtasDet As New clsDetalleVentasCotizacionBT
                            objVtasDet.ActualizarNroPax(intIDDet, BE.UserMod)

                            RecalcularCostosVtas(BE.IDCab, intIDDet, ListaDetPax, BE.UserMod)



                        End If
                        If intIDReserva_Det > 0 Then
                            Dim ListaDetPax As New List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
                            For Each ItemDet As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                                If ItemDet.IDServicio = strCoServicio Then
                                    Dim NewPax As New clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE _
                                        With {.IDReserva_Det = intIDReserva_Det, .IDPax = ItemDet.NuPax, .UserMod = BE.UserMod, _
                                              .Accion = "N", .Selecc = True}
                                    ListaDetPax.Add(NewPax)
                                End If
                            Next
                            Dim objBERvs As New clsReservaBE With {.ListaPaxDetReservas = ListaDetPax}
                            Dim objRvs As New clsReservaBT.clsDetalleReservaBT.clsReservaDetPaxBT
                            objRvs.InsertarEliminar(objBERvs, intIDReserva_Det, "N")
                            objRvs.ActualizarNroPax(intIDReserva_Det, BE.UserMod)
                        End If
                        If intIDOperaciones_Det > 0 Then
                            Dim ListaDetPax As New List(Of clsPaxDetalleOperacionBE)
                            For Each ItemDet As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                                If ItemDet.IDServicio = strCoServicio Then
                                    Dim NewPax As New clsPaxDetalleOperacionBE _
                                        With {.IDOperacion_Det = intIDOperaciones_Det, .IDPax = ItemDet.NuPax, .UserMod = BE.UserMod, _
                                              .Accion = "N", .Selecc = True}
                                    ListaDetPax.Add(NewPax)
                                End If
                            Next
                            Dim objBEOpe As New clsOperacionBE With {.ListaPaxDetalle = ListaDetPax}
                            Dim objOpe As New clsPaxDetalleOperacionBT
                            objOpe.InsertarActualizarEliminar(objBEOpe)
                            objOpe.ActualizarNroPax(intIDOperaciones_Det, BE.UserMod)
                        End If



                    End If

                Next

            Next



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarPaxenModulos")
        End Try
    End Sub
    Private Sub RecalcularCostosVtas(ByVal vintIDCab As Integer, ByVal vintIDDet As Integer, _
                                     ByVal ListaDetPax As List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE), _
                                     ByVal vstrUserMod As String)
        Try
            Dim ListDetVta As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Dim ItemDetVta As New clsCotizacionBE.clsDetalleCotizacionBE With {.IDDet = vintIDDet, .Recalcular = True}
            ListDetVta.Add(ItemDetVta)

            Dim objVtasDet As New clsDetalleVentasCotizacionBT
            Dim strTipoPax As String = objVtasDet.strDevuelveTipoPaxNacionalidad(New clsCotizacionBE With {.IdCab = vintIDCab, _
                            .ListaDetalleCotiz = ListDetVta})
            Dim objVtas As New clsVentasCotizacionBT
            'Dim objVtas.ConsultarPkCoti()
            Dim sglRedondeo As Single = 0
            Dim blnMargenCero As Boolean = False
            'Dim objBN As New clsCotizacionBN
            'Using drCot As SqlClient.SqlDataReader = objBN.ConsultarPk(vintIDCab)
            '    drCot.Read()
            '    If drCot.HasRows Then
            '        sglRedondeo = drCot("Redondeo")
            '        blnMargenCero = drCot("MargenCero")
            '    End If
            '    drCot.Close()
            'End Using
            Dim dttCot As DataTable = objVtas.ConsultarPkCoti(vintIDCab)
            If dttCot.Rows.Count > 0 Then
                sglRedondeo = dttCot(0)("Redondeo")
                blnMargenCero = dttCot(0)("MargenCero")
            End If

            objVtas.ReCalculoCotizacion(vintIDCab, 0, 0, "", strTipoPax, sglRedondeo, psglIGV, "M", _
                                        vstrUserMod, Nothing, blnMargenCero, "", ListDetVta)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".RecalcularCostosVtas")

        End Try
    End Sub
    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsVentaAdicionalBE, ByVal vstrNuevoID As String)
        Try
            For Each Item As clsVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                Item.NuVtaAdi = vstrNuevoID
            Next

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarIDsNuevosenHijosST")
        End Try
    End Sub
    Private Sub InsertarDebitMemo(ByVal BE As clsVentaAdicionalBE)
        Try
            Dim datFechaIn = Nothing, datFechaOut As Date = Nothing
            Dim strResponsable = "", strTitulo As String = ""
            Using objBNDe As New clsDebitMemoBN
                Using dr As SqlClient.SqlDataReader = objBNDe.ConsultarDatosCotizacion(BE.IDCab)
                    dr.Read()
                    If dr.HasRows Then
                        datFechaIn = dr("FechaIn")
                        datFechaOut = dr("FechaOut")
                        strTitulo = dr("Titulo")
                        'strResponsable = dr("Responsable")
                        strResponsable = BE.ResponsableDMemo
                    End If
                    dr.Close()
                End Using
            End Using


            'Dim ListaDM As New List(Of clsDebitMemoBE)
            Dim objDMBT As New clsDebitMemoBT With {.pstrIDClienteAPT = pstrIDClienteAPT}
            Dim objDMBTDet As New clsDetalleDebitMemoBT
            Dim objDet As New clsDetalleVentaAdicionalBT

            'Grupos
            Dim ListaGrupos As New List(Of Byte)
            For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If Item.NuGrupo > 0 Then
                    If Not ListaGrupos.Contains(Item.NuGrupo) Then
                        ListaGrupos.Add(Item.NuGrupo)
                    End If
                End If
            Next

            For Each bytNuGrupo As Byte In ListaGrupos
                Dim dblTotal As Double = 0
                Dim intPax As Int16 = 0
                Dim strNuCliente As String = pstrIDClienteVtasAdicionalesAPT
                Dim strNoCliente As String = "VENTAS ADICIONALES APT"
                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    If Item.NuGrupo = bytNuGrupo Then
                        dblTotal += Item.SsMonto
                        intPax += 1
                        strNuCliente = Item.NuCliente.Substring(0, 6)
                        strNoCliente = Mid(Item.NuCliente, 8)
                    End If
                Next


                Dim objBEDM As New clsDebitMemoBE With {.Fecha = Format(Today, "dd/MM/yyyy"), .FecVencim = Format(Today, "dd/MM/yyyy"), .IDCab = BE.IDCab, _
                                                    .IDMoneda = "USD", .SubTotal = dblTotal, .TotalIGV = 0, .Total = dblTotal, _
                                                    .IDBanco = "000", .IDTipo = "A", .FechaIn = Format(datFechaIn, "dd/MM/yyyy"), .FechaOut = Format(datFechaOut, "dd/MM/yyyy"), _
                                                    .Cliente = strNoCliente, .Pax = intPax, .Titulo = strTitulo, .IDEstado = "PD", _
                                                    .Responsable = strResponsable, .FlImprimioDebit = False, .FlManual = False, _
                                                    .IDCliente = strNuCliente, .UserMod = BE.UserMod}
                Dim strIDDebitMemo As String = objDMBT.Insertar(objBEDM)
                Dim ListaDetDM As New List(Of clsDetalleDebitMemoBE)

                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales

                    If Item.NuGrupo = bytNuGrupo Then
                        Dim objBEDMDet As New clsDetalleDebitMemoBE With {.IDDebitMemo = strIDDebitMemo, _
                                                                          .Descripcion = Item.NoPax, _
                                                                          .SubTotal = Item.SsMonto, _
                                                                          .Total = Item.SsMonto, _
                                                                          .TotalIGV = 0, _
                                                                          .CapacidadHab = "", _
                                                                          .NroPax = 1, _
                                                                          .CostoPersona = Item.SsMonto, _
                                                                          .Accion = "N", _
                                                                          .UserMod = Item.UserMod}
                        ListaDetDM.Add(objBEDMDet)

                        objDet.ActualizarDebitMemo(Item.NuVtaAdi, Item.CoServicio, Item.NuPax, strIDDebitMemo, BE.UserMod)
                        Item.IDDebitMemo = strIDDebitMemo
                    End If
                Next
                objBEDM.ListaDetalleDebitMemo = ListaDetDM
                objDMBTDet.InsertarActualizarEliminar(objBEDM)

            Next

            'Pax Sin Grupo            
            Dim ListaPax As New List(Of Integer)
            For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If Item.NuGrupo = 0 Then
                    If Not ListaPax.Contains(Item.NuPax) Then
                        ListaPax.Add(Item.NuPax)
                    End If
                End If
            Next

            If BE.CoTipo = "SE" Then
                For Each intNuPax As Integer In ListaPax
                    Dim dblTotal As Double = 0
                    Dim intPax As Int16 = 0
                    Dim strNuCliente As String = pstrIDClienteVtasAdicionalesAPT
                    Dim strNoCliente As String = "VENTAS ADICIONALES APT"
                    For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                        If Item.NuPax = intNuPax Then
                            dblTotal += Item.SsMonto
                            intPax += 1
                            'If BE.CoTipo = "SE" Then
                            strNuCliente = Item.NuCliente.Substring(0, 6)
                            strNoCliente = Mid(Item.NuCliente, 8)
                            'End If
                        End If
                    Next

                    Dim objBEDM As New clsDebitMemoBE With {.Fecha = Format(Today, "dd/MM/yyyy"), .FecVencim = Format(Today, "dd/MM/yyyy"), .IDCab = BE.IDCab, _
                                .IDMoneda = "USD", .SubTotal = dblTotal, .TotalIGV = 0, .Total = dblTotal, _
                                .IDBanco = "000", .IDTipo = "A", .FechaIn = Format(datFechaIn, "dd/MM/yyyy"), .FechaOut = Format(datFechaOut, "dd/MM/yyyy"), _
                                .Cliente = strNoCliente, .Pax = intPax, .Titulo = strTitulo, .IDEstado = "PD", _
                                .Responsable = strResponsable, .FlImprimioDebit = False, .FlManual = False, _
                                .IDCliente = strNuCliente, .UserMod = BE.UserMod}
                    Dim strIDDebitMemo As String = objDMBT.Insertar(objBEDM)
                    Dim ListaDetDM As New List(Of clsDetalleDebitMemoBE)

                    For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales

                        If Item.NuPax = intNuPax Then
                            Dim dblSsMonto As Double = Item.SsMonto

                            Dim objBEDMDet As New clsDetalleDebitMemoBE With {.IDDebitMemo = strIDDebitMemo, _
                                                                                  .Descripcion = Item.NoPax, _
                                                                                  .SubTotal = dblSsMonto, _
                                                                                  .Total = dblSsMonto, _
                                                                                  .TotalIGV = 0, _
                                                                                  .CapacidadHab = "", _
                                                                                  .NroPax = 1, _
                                                                                  .CostoPersona = dblSsMonto, _
                                                                                  .Accion = "N", _
                                                                                  .UserMod = Item.UserMod}
                            ListaDetDM.Add(objBEDMDet)

                            objDet.ActualizarDebitMemo(Item.NuVtaAdi, Item.CoServicio, Item.NuPax, strIDDebitMemo, BE.UserMod)

                            Item.IDDebitMemo = strIDDebitMemo
                        End If
                    Next
                    objBEDM.ListaDetalleDebitMemo = ListaDetDM
                    objDMBTDet.InsertarActualizarEliminar(objBEDM)
                Next
            ElseIf BE.CoTipo = "FC" Then
                Dim dblTotal As Double = 0

                Dim strNuCliente As String = pstrIDClienteAPT
                Dim strNoCliente As String = "APT SOUTH AMERICA"
                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    dblTotal += Item.SsMonto
                Next
                Dim objBEDM As New clsDebitMemoBE With {.Fecha = Format(Today, "dd/MM/yyyy"), .FecVencim = BE.FeVencimDMemo, .IDCab = BE.IDCab, _
                            .IDMoneda = "USD", .SubTotal = dblTotal, .TotalIGV = 0, .Total = dblTotal, _
                            .IDBanco = "000", .IDTipo = "I", .FechaIn = Format(datFechaIn, "dd/MM/yyyy"), .FechaOut = Format(datFechaOut, "dd/MM/yyyy"), _
                            .Cliente = strNoCliente, .Pax = ListaPax.Count, .Titulo = strTitulo, .IDEstado = "PD", _
                            .Responsable = strResponsable, .FlImprimioDebit = False, .FlManual = False, _
                            .IDCliente = strNuCliente, .UserMod = BE.UserMod}
                Dim strIDDebitMemo As String = objDMBT.Insertar(objBEDM)

                Dim ListaDetDM As New List(Of clsDetalleDebitMemoBE)
                For Each intNuPax As Integer In ListaPax
                    dblTotal = 0
                    Dim strNoPax As String = ""

                    For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                        If Item.NuPax = intNuPax Then
                            dblTotal += Item.SsMonto
                            strNoPax = Item.NoPax
                            objDet.ActualizarDebitMemo(Item.NuVtaAdi, Item.CoServicio, Item.NuPax, strIDDebitMemo, BE.UserMod)
                        End If
                    Next

                    Dim objBEDMDet As New clsDetalleDebitMemoBE With {.IDDebitMemo = strIDDebitMemo, _
                                                                          .Descripcion = strNoPax, _
                                                                          .SubTotal = dblTotal, _
                                                                          .Total = dblTotal, _
                                                                          .TotalIGV = 0, _
                                                                          .CapacidadHab = "", _
                                                                          .NroPax = 1, _
                                                                          .CostoPersona = dblTotal, _
                                                                          .Accion = "N", _
                                                                          .UserMod = BE.UserMod}
                    ListaDetDM.Add(objBEDMDet)

                    'Item.IDDebitMemo = strIDDebitMemo
                Next
                objBEDM.ListaDetalleDebitMemo = ListaDetDM
                objDMBTDet.InsertarActualizarEliminar(objBEDM)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarDebitMemo")
        End Try
    End Sub
    Private Sub InsertarBoletasVta(ByVal BE As clsVentaAdicionalBE)
        Try
            'Grupos
            Dim ListaGrupos As New List(Of Byte)
            For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If Item.NuGrupo > 0 Then
                    If Not ListaGrupos.Contains(Item.NuGrupo) Then
                        ListaGrupos.Add(Item.NuGrupo)
                    End If
                End If
            Next

            Dim objDet As New clsDetalleVentaAdicionalBT
            Dim strCoCtaContable As String = "70411002" ' "704112"
            For Each bytNuGrupo As Byte In ListaGrupos
                Dim dblTotal As Double = 0
                Dim strNuCliente As String = pstrIDClienteVtasAdicionalesAPT
                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    If Item.NuGrupo = bytNuGrupo Then
                        dblTotal += Item.SsMonto
                        strNuCliente = Item.NuCliente.Substring(0, 6)
                    End If
                Next

                Dim ItemDocum As New clsDocumentoBE With { _
                   .NuDocum = 0, _
                   .IDTipoDoc = "BLE", _
                   .CoSerie = "002", _
                   .IDCab = BE.IDCab, _
                   .FeDocum = "01/01/1900", _
                   .IDMoneda = "USD", _
                   .SsTotalDocumUSD = dblTotal, _
                   .SsIGV = 0, _
                   .Generado = False, _
                   .TxTitulo = "", _
                   .NuDocumVinc = "", _
                   .IDTipoDocVinc = "", _
                   .FlPorAjustePrecio = False, _
                   .CoTipoVenta = "RC", _
                   .IDCliente = strNuCliente, _
                   .CoCtaContable = strCoCtaContable, _
                   .FeEmisionVinc = "01/01/1900", _
                   .CoCtroCosto = "900101", _
                   .NuFileLibre = "", _
                   .FlVentaAdicional = True, _
                   .UserMod = BE.UserMod}


                Dim objDocBT As New clsDocumentoBT With {.pstrSantiago = pstrSantiago, _
                                         .pstrBuenosAires = pstrBuenosAires, _
                                         .pstrCoUbigeo_Oficina = pstrCoUbigeo_Oficina}

                ItemDocum.ListaDetalleDocumentos_Texto = lstListaDocDetTxtVtasAdicionalesAPT(BE, bytNuGrupo, True)
                Dim strNumDocum As String = objDocBT.InsertarManual(ItemDocum, True)

                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    If Item.NuGrupo = bytNuGrupo Then
                        objDet.ActualizarDocumento(Item.NuVtaAdi, Item.CoServicio, Item.NuPax, strNumDocum, "BLE", BE.UserMod)
                    End If
                Next

            Next

            'Pax Sin Grupo
            Dim ListaPax As New List(Of Integer)
            For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If Item.NuGrupo = 0 Then
                    If Not ListaPax.Contains(Item.NuPax) Then
                        ListaPax.Add(Item.NuPax)
                    End If
                End If
            Next

            For Each intNuPax As Integer In ListaPax
                Dim dblTotal As Double = 0
                Dim strNuCliente As String = pstrIDClienteVtasAdicionalesAPT
                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    If Item.NuPax = intNuPax And Item.NuGrupo = 0 Then
                        dblTotal += Item.SsMonto
                        strNuCliente = Item.NuCliente.Substring(0, 6)
                    End If
                Next

                Dim ItemDocum As New clsDocumentoBE With { _
                   .NuDocum = 0, _
                   .IDTipoDoc = "BLE", _
                   .CoSerie = "002", _
                   .IDCab = BE.IDCab, _
                   .FeDocum = "01/01/1900", _
                   .IDMoneda = "USD", _
                   .SsTotalDocumUSD = dblTotal, _
                   .SsIGV = 0, _
                   .Generado = False, _
                   .TxTitulo = "", _
                   .NuDocumVinc = "", _
                   .IDTipoDocVinc = "", _
                   .FlPorAjustePrecio = False, _
                   .CoTipoVenta = "RC", _
                   .IDCliente = strNuCliente, _
                   .CoCtaContable = strCoCtaContable, _
                   .FeEmisionVinc = "01/01/1900", _
                   .CoCtroCosto = "900101", _
                   .NuFileLibre = "", _
                   .FlVentaAdicional = True, _
                   .UserMod = BE.UserMod}


                Dim objDocBT As New clsDocumentoBT With {.pstrSantiago = pstrSantiago, _
                                     .pstrBuenosAires = pstrBuenosAires, _
                                     .pstrCoUbigeo_Oficina = pstrCoUbigeo_Oficina}

                ItemDocum.ListaDetalleDocumentos_Texto = lstListaDocDetTxtVtasAdicionalesAPT(BE, intNuPax, False)
                Dim strNumDocum As String = objDocBT.InsertarManual(ItemDocum, True)

                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    If Item.NuPax = intNuPax Then
                        objDet.ActualizarDocumento(Item.NuVtaAdi, Item.CoServicio, Item.NuPax, strNumDocum, "BLE", BE.UserMod)
                    End If
                Next
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarBoletasVta")
        End Try
    End Sub

    Private Function lstListaDocDetTxtVtasAdicionalesAPT(ByVal BE As clsVentaAdicionalBE, ByVal vintGrupoPax As Integer, ByVal vblnPorGrupo As Boolean) As List(Of clsDocumentoDet_TextoBE)
        Try

            Dim ListaDetTexto As New List(Of clsDocumentoDet_TextoBE)
            Dim ListaCiudades As New List(Of String)
            For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If vblnPorGrupo Then
                    If Item.NuGrupo = vintGrupoPax Then
                        If Not ListaCiudades.Contains(Item.CoUbigeo) Then
                            ListaCiudades.Add(Item.CoUbigeo)
                        End If
                    End If
                Else
                    If Item.NuPax = vintGrupoPax Then
                        If Not ListaCiudades.Contains(Item.CoUbigeo) Then
                            ListaCiudades.Add(Item.CoUbigeo)
                        End If
                    End If
                End If
            Next

            BE.ListaDetalleVtasAdicionales = (From ListaOrd In BE.ListaDetalleVtasAdicionales _
                                         Select ListaOrd Order By ListaOrd.CoUbigeo Ascending, ListaOrd.NuPax Ascending).ToList()



            For Each strCiudad As String In ListaCiudades

                Dim dblTotal As Double = 0
                If vblnPorGrupo Then
                    For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                        If Item.NuGrupo = vintGrupoPax And Item.CoUbigeo = strCiudad Then
                            dblTotal += Item.SsMonto
                        End If
                    Next
                Else
                    For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                        If Item.NuPax = vintGrupoPax And Item.CoUbigeo = strCiudad Then
                            dblTotal += Item.SsMonto
                        End If
                    Next
                End If


                Dim bln1ero As Boolean = True
                Dim ListaPax As New List(Of Integer)
                For Each Item As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                    Dim blnOk As Boolean = False
                    If vblnPorGrupo Then
                        If Item.NuGrupo = vintGrupoPax And Item.CoUbigeo = strCiudad Then
                            blnOk = True
                        End If
                    Else
                        If Item.NuPax = vintGrupoPax And Item.CoUbigeo = strCiudad Then
                            blnOk = True
                        End If

                    End If
                    If blnOk Then
                        If bln1ero Then

                            Dim strTexto As String = "POR SERVICIOS EN " & Item.NoUbigeo & "-" & Item.NoPais
                            Dim objBEDocDet As New clsDocumentoDet_TextoBE With {.IDTipoDoc = "BLE", _
                                                                             .NuDocum = "0", _
                                                                                 .NoTexto = strTexto, _
                                                                                 .SsTotal = dblTotal, _
                                                                                 .Accion = "N", _
                                                                                 .UserMod = Item.UserMod}
                            ListaDetTexto.Add(objBEDocDet)
                            strTexto = "POR SERVICIOS PRESTADOS EN ATENCION A LOS PAX: "

                            objBEDocDet = New clsDocumentoDet_TextoBE With {.IDTipoDoc = "BLE", _
                                                     .NuDocum = "0", _
                                                         .NoTexto = strTexto, _
                                                         .SsTotal = 0, _
                                                         .Accion = "N", _
                                                         .UserMod = Item.UserMod}
                            ListaDetTexto.Add(objBEDocDet)

                            objBEDocDet = New clsDocumentoDet_TextoBE With {.IDTipoDoc = "BLE", _
                                                     .NuDocum = "0", _
                                                         .NoTexto = "  " & Item.NoPax, _
                                                         .SsTotal = 0, _
                                                         .Accion = "N", _
                                                         .UserMod = Item.UserMod}
                            ListaDetTexto.Add(objBEDocDet)
                            bln1ero = False
                            ListaPax.Add(Item.NuPax)
                        Else

                            If Not ListaPax.Contains(Item.NuPax) Then
                                Dim objBEDocDet As New clsDocumentoDet_TextoBE With {.IDTipoDoc = "BLE", _
                                                         .NuDocum = "0", _
                                                             .NoTexto = "  " & Item.NoPax, _
                                                             .SsTotal = 0, _
                                                             .Accion = "N", _
                                                             .UserMod = Item.UserMod}
                                ListaDetTexto.Add(objBEDocDet)

                                ListaPax.Add(Item.NuPax)

                            End If
                        End If


                    End If
                Next
            Next
            Return ListaDetTexto

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".lstListaDocDetTxtVtasAdicionalesAPT")
        End Try
    End Function


    Private Sub ActualizarClientesxPaxaBE(ByRef BE As clsVentaAdicionalBE)

        Try
            Dim ListaGrupos As New List(Of Byte)
            For Each ItemDetVtaAdic As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If ItemDetVtaAdic.NuGrupo > 0 Then
                    If Not ListaGrupos.Contains(ItemDetVtaAdic.NuGrupo) Then
                        ListaGrupos.Add(ItemDetVtaAdic.NuGrupo)
                    End If
                End If
            Next

            For Each bytNuGrupo As Byte In ListaGrupos
                Dim strNuCliente As String = strGeneraClientexPax(strPaxTitularGrupo(BE, bytNuGrupo), BE.UserMod)
                If strNuCliente <> "" Then
                    For Each ItemDetVtaAdic As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                        If bytNuGrupo = ItemDetVtaAdic.NuGrupo Then
                            ItemDetVtaAdic.NuCliente = strNuCliente
                        End If
                    Next
                End If
            Next


            Dim ListaPaxSinGrupos As New List(Of Integer)
            For Each ItemDetVtaAdic As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If ItemDetVtaAdic.NuGrupo = 0 Then
                    If Not ListaPaxSinGrupos.Contains(ItemDetVtaAdic.NuPax) Then
                        ListaPaxSinGrupos.Add(ItemDetVtaAdic.NuPax)
                    End If
                End If
            Next

            For Each intNuPax As Integer In ListaPaxSinGrupos
                Dim strNuCliente As String = strGeneraClientexPax(intNuPax, BE.UserMod)
                If strNuCliente <> "" Then
                    For Each ItemDetVtaAdic As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                        If intNuPax = ItemDetVtaAdic.NuPax Then
                            ItemDetVtaAdic.NuCliente = strNuCliente
                        End If
                    Next
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Function strPaxTitularGrupo(ByVal BE As clsVentaAdicionalBE, ByVal vbytNuGrupo As Byte) As Integer
        Try
            For Each ItemDetVtaAdic As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                If ItemDetVtaAdic.NuGrupo = vbytNuGrupo And ItemDetVtaAdic.FlTitular Then
                    Return ItemDetVtaAdic.NuPax
                End If
            Next
            Return 0
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function strGeneraClientexPax(ByVal vintNuPax As Integer, ByVal vstrUserMod As String) As String
        If vintNuPax = 0 Then Return ""
        Try

            Dim objPaxBN As New clsCotizacionBN.clsCotizacionPaxBN
            Dim objNewClienteBE As New clsClienteBE
            Dim strCoTipDocIdentPax As String = ""
            Dim strNuDocIdentPax As String = ""

            Dim strNoCliente As String = ""
            Using dr As SqlClient.SqlDataReader = objPaxBN.ConsultarPK(vintNuPax)
                dr.Read()
                If dr.HasRows Then
                    With objNewClienteBE
                        .Tipo = "WB"
                        .Nombre = dr("Nombres")
                        .ApPaterno = dr("Apellidos")
                        .ApMaterno = ""
                        .RazonSocial = ""
                        .RazonComercial = dr("Apellidos") & ", " & dr("Nombres")
                        strNoCliente = .RazonComercial
                        .IdIdentidad = dr("IdIdentidad")
                        strCoTipDocIdentPax = .IdIdentidad
                        .NumIdentidad = If(IsDBNull(dr("NumIdentidad")), "", dr("NumIdentidad"))
                        strNuDocIdentPax = .NumIdentidad
                        .CoTipoVenta = "CU"

                        .Persona = "N"
                        .Direccion = "-"
                        .Ciudad = ""
                        .Telefono1 = ""
                        .Telefono2 = ""
                        .Celular = ""
                        .Fax = ""
                        .Notas = ""
                        .IDCiudad = dr("IDNacionalidad")
                        .IDVendedor = "0000"
                        .Web = ""
                        .CodPostal = ""
                        .Complejidad = "N"
                        .Frecuencia = "N"
                        .Solvencia = 0
                        .CoStarSoft = ""
                        .NuCuentaComision = ""
                        .CoTipDocIdentPax = dr("IdIdentidad")
                        .NuDocIdentPax = If(IsDBNull(dr("NumIdentidad")), "", dr("NumIdentidad"))
                        .UserMod = vstrUserMod
                    End With
                End If
            End Using

            Dim strCoClienteExistente As String = ""
            If strNuDocIdentPax <> "" And strCoTipDocIdentPax <> "" Then
                strCoClienteExistente = strCoClientexNuDocIdentPax(strCoTipDocIdentPax, strNuDocIdentPax).Trim
            End If

            Dim strNuCliente As String
            If strCoClienteExistente = "" Then
                Dim objCliBT As New clsClienteBT
                strNuCliente = objCliBT.Insertar(objNewClienteBE)
            Else
                strNuCliente = strCoClienteExistente
            End If

            ContextUtil.SetComplete()
            Return strNuCliente & " " & strNoCliente

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Function strCoClientexNuDocIdentPax(ByVal vstrCoTipDocIdentPax As String, ByVal vstrNuDocIdentPax As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoTipDocIdentPax", vstrCoTipDocIdentPax)
            dc.Add("@NuDocIdentPax", vstrNuDocIdentPax)
            dc.Add("@pCoCliente", "")

            Return objADO.GetSPOutputValue("MACLIENTES_SelxNuDocIdentPaxOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleVentaAdicionalBT
    Inherits clsAdministracionBaseBT
    Dim strNombreClase As String = "clsDetalleVentaAdicionalBT"
    Public Sub InsertarEliminar(ByVal BE As clsVentaAdicionalBE)
        Try
            'For Each ItemDetVtaAdic As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
            '    Eliminar(ItemDetVtaAdic)
            'Next

            For Each ItemDetVtaAdic As clsDetalleVentaAdicionalBE In BE.ListaDetalleVtasAdicionales
                Insertar(ItemDetVtaAdic)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Sub Insertar(ByVal BE As clsDetalleVentaAdicionalBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVtaAdi", BE.NuVtaAdi)
            dc.Add("@CoServicio", BE.CoServicio)
            dc.Add("@NuPax", BE.NuPax)
            dc.Add("@NuGrupo", BE.NuGrupo)
            dc.Add("@FlTitular", BE.FlTitular)
            dc.Add("@SsMonto", BE.SsMonto)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("VENTA_ADICIONAL_DET_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal BE As clsDetalleVentaAdicionalBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVtaAdi", BE.NuVtaAdi)
            dc.Add("@CoServicio", BE.CoServicio)
            dc.Add("@NuPax", BE.NuPax)

            objADO.ExecuteSP("VENTA_ADICIONAL_DET_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Sub ActualizarDebitMemo(ByVal vintNuVtaAdi As Integer, ByVal vintCoServicio As String, _
                                   ByVal vintNuPax As Integer, ByVal vstrIDDebitMemo As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVtaAdi", vintNuVtaAdi)
            dc.Add("@CoServicio", vintCoServicio)
            dc.Add("@NuPax", vintNuPax)
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("VENTA_ADICIONAL_DET_UpdDebitMemo", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarDebitMemo")
        End Try
    End Sub

    Public Sub ActualizarDocumento(ByVal vintNuVtaAdi As Integer, ByVal vintCoServicio As Int16, _
                                   ByVal vintNuPax As Integer, ByVal vstrNuDocum As String, ByVal vstrIDTipoDoc As String, _
                                   ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVtaAdi", vintNuVtaAdi)
            dc.Add("@CoServicio", vintCoServicio)
            dc.Add("@NuPax", vintNuPax)
            dc.Add("@NuDocum", vstrNuDocum)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("VENTA_ADICIONAL_DET_UpdDocumento", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarDocumento")
        End Try
    End Sub
End Class





