﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
' ''Imports System.Net.Http
' ''Imports System.Net.Http.Headers
' ''Imports System.Text
' ''Imports Newtonsoft.Json
' ''Imports Newtonsoft.Json.Linq

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsClienteBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Public Function Insertar(ByVal BE As clsClienteBE) As String
        Dim dc As New Dictionary(Of String, String)
        Dim strIDCliente As String
        Try
            With dc
                .Add("@RazonComercial", BE.RazonComercial)
                .Add("@RazonSocial", BE.RazonSocial)
                .Add("@Persona", BE.Persona)
                .Add("@Nombre", BE.Nombre)
                .Add("@ApPaterno", BE.ApPaterno)
                .Add("@ApMaterno", BE.ApMaterno)
                .Add("@Domiciliado", BE.Domiciliado)
                .Add("@Ciudad", BE.Ciudad)
                .Add("@Celular", BE.Celular)
                .Add("@Comision", BE.Comision)
                .Add("@Credito", BE.Credito)
                .Add("@MostrarEnReserva", BE.MostrarEnReserva)
                .Add("@Direccion", BE.Direccion)
                .Add("@Fax", BE.Fax)
                .Add("@IdIdentidad", BE.IdIdentidad)
                .Add("@NumIdentidad", BE.NumIdentidad)
                .Add("@Tipo", BE.Tipo)
                .Add("@IDCiudad", BE.IDCiudad)
                .Add("@IDVendedor", BE.IDVendedor)
                .Add("@Notas", BE.Notas)
                .Add("@Telefono1", BE.Telefono1)
                .Add("@Telefono2", BE.Telefono2)
                .Add("@Web", BE.Web)
                .Add("@CodPostal", BE.CodPostal)
                .Add("@Complejidad", BE.Complejidad)
                .Add("@Frecuencia", BE.Frecuencia)
                .Add("@Solvencia", BE.Solvencia)
                .Add("@CoTipoVenta", BE.CoTipoVenta)
                .Add("@UserMod", BE.UserMod)

                '
                'MLL-Agregado el dia 29-05-14
                .Add("@CoStarSoft", BE.CoStarSoft)
                .Add("@NuCuentaComision", BE.NuCuentaComision)

                .Add("@FlTop", BE.FlTop)

                '.Add("@FePerfilUpdate", BE.FePerfilUpdate)
                .Add("@CoUserPerfilUpdate", BE.CoUserPerfilUpdate)
                .Add("@txPerfilInformacion", BE.txPerfilInformacion)
                .Add("@txPerfilOperatividad", BE.txPerfilOperatividad)

                .Add("@FlProveedor", BE.FlProveedor)
                .Add("@NuPosicion", BE.NuPosicion)

                .Add("@CoTipDocIdentPax", BE.CoTipDocIdentPax)
                .Add("@NuDocIdentPax", BE.NuDocIdentPax)

                .Add("@pIDCliente", "")
            End With

            strIDCliente = objADO.ExecuteSPOutput("MACLIENTES_Ins", dc)
            BE.IDCliente = strIDCliente
            If Not BE.ListaAdministCliente Is Nothing Then
                BE.IDCliente = strIDCliente
                Dim objBTDet As New clsClienteBT.clsAdministClienteBT
                objBTDet.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaSeriesCliente Is Nothing Then
                ActualizarNuevoID(BE, strIDCliente)
                Dim objBTSeries As New clsClienteBT.clsSeriesClienteBT
                objBTSeries.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaPerfilGuia Is Nothing Then
                Dim objBTGuias As New clsClienteBT.clsClientes_Perfil_GuiasBT
                objBTGuias.InsertarActualizarEliminar(BE)
            End If

            Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
            PasarDatosClientesToSocioNegocio(strIDCliente, BE.UserMod, oBESocioNegocio)

            If Not oBESocioNegocio Is Nothing Then
                Dim oBTJsonMaster As New clsJsonInterfaceBT()
                'oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                '                                     "http://sap:8899/api/BusinessPartners/", _
                '                                     "POST")
                oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                                                    "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
                                                    "POST")
                Actualizar_CardCodeSAP(BE.IDCliente, oBESocioNegocio.CardCode)
            End If



            ContextUtil.SetComplete()
            Return strIDCliente
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Function drConsultarDatosClienteSAP(ByVal vstrIDCliente As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSocioNegocio", vstrIDCliente)
            dc.Add("@TipoSocioNegocio", "C")

            Return objADO.GetDataTable("SAP_ListarSociosNegocio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dtDatosContactos(ByVal vstrIDSocioNegocio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSocioNegocio", vstrIDSocioNegocio)
            dc.Add("@TipoSocio", "C")

            Return objADO.GetDataTable("SAP_ListarSociosNegocio_ContactEmployees", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub PasarDatosClientesToSocioNegocio(ByVal vstrIDCliente As String, ByVal vstrUserMod As String, _
                                                 ByRef oBESocioNegocioBE As clsSocioNegocioBE)
        Try
            Dim dtDatos As DataTable = drConsultarDatosClienteSAP(vstrIDCliente)
            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBESocioNegocioBE = New clsSocioNegocioBE()
                With oBESocioNegocioBE
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .CardType = dr("CardType")
                    .GroupCode = dr("GroupCode")
                    .FederalTaxID = dr("FederalTaxID")
                    .Currency = dr("Currency")
                    .U_SYP_BPTP = dr("U_SYP_BPTP")
                    .U_SYP_BPTD = dr("U_SYP_BPTD")
                    .U_SYP_BPAP = dr("U_SYP_BPAP")
                    .U_SYP_BPAM = dr("U_SYP_BPAM")
                    .U_SYP_BPN2 = dr("U_SYP_BPN2")
                    .U_SYP_BPNO = dr("U_SYP_BPNO")
                    .EmailAddress = dr("EmailAddress")
                    .Phone1 = dr("Phone1")
                    .Phone2 = dr("Phone2")
                    .SubjectToWithholdingTax = dr("SubjectToWithholdingTax")
                    .Valid = dr("Valid")

                    Dim BPBAddress As New List(Of clsBPAddressesBE)
                    Dim oBPBAddress As New clsBPAddressesBE
                    With oBPBAddress
                        .AddressName = dr("AddressName")
                        .Street = dr("Street")
                        .ZipCode = dr("ZipCode")
                        .Block = dr("Block")
                        .AddressType = dr("AddressType")
                    End With
                    BPBAddress.Add(oBPBAddress)
                    .BPAddresses = BPBAddress

                    Dim ContactEmployees As New List(Of clsContactEmployeesBE)
                    Dim dDatosContact As DataTable = dtDatosContactos(.CardCode)
                    For Each Item As DataRow In dDatosContact.Rows
                        Dim oContactEmployee As New clsContactEmployeesBE
                        With oContactEmployee
                            .Name = Item("Name")
                            .FirstName = Item("FirstName")
                            .LastName = Item("LastName")
                            .Address = Item("Address")
                            .E_Mail = Item("E_mail")
                            .Active = Item("Active")
                        End With
                        ContactEmployees.Add(oContactEmployee)
                    Next
                    .ContactEmployees = ContactEmployees
                End With
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    '' ''Private Sub EnviarDatosRESTFul(ByVal oBESocioNegocioBE As clsSocioNegocioBE, ByVal vstrVerbo As String) 'As Threading.Tasks.Task
    '' ''    If oBESocioNegocioBE Is Nothing Then Exit Sub
    '' ''    If vstrVerbo.Trim = String.Empty Then Exit Sub
    '' ''    'If vstrVerbo <> "POST" Or vstrVerbo <> "PUT" Or vstrVerbo <> "DELETE" Then Exit Sub
    '' ''    Dim strMessageError As String = String.Empty
    '' ''    Try
    '' ''        'Dim jser As New System.Web.Script.Serialization.JavaScriptSerializer
    '' ''        'Dim jss As String = jser.Serialize(oBESocioNegocioBE)

    '' ''        Dim strDataValueJSON = JsonConvert.SerializeObject(oBESocioNegocioBE, Formatting.Indented)
    '' ''        DropPropertyJSON(strDataValueJSON)

    '' ''        Dim strUriHostRestFul As String = "http://sap:8899/"
    '' ''        Dim strUriAPI As String = String.Empty
    '' ''        Select Case vstrVerbo
    '' ''            Case "POST"
    '' ''                strUriAPI &= "api/BusinessPartners"
    '' ''            Case "PUT"
    '' ''                strUriAPI &= "api/BusinessPartners/" & oBESocioNegocioBE.CardCode
    '' ''        End Select

    '' ''        If strUriHostRestFul <> String.Empty And strUriAPI <> String.Empty Then
    '' ''            Dim clientConnect As New HttpClient()
    '' ''            With clientConnect
    '' ''                Try
    '' ''                    .BaseAddress = New Uri(strUriHostRestFul)
    '' ''                    .DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))
    '' ''                    '.DefaultRequestHeaders.Add("Body", strDataValueJSON)
    '' ''                Catch ex As Exception
    '' ''                    strMessageError = "SAP: Error al direccionar el grabado de cliente."
    '' ''                    Throw
    '' ''                End Try

    '' ''                If strMessageError = String.Empty Then
    '' ''                    Dim myContent = New StringContent(content:=strDataValueJSON, encoding:=Encoding.UTF8, mediaType:="application/json")
    '' ''                    Try
    '' ''                        Dim response As New System.Net.Http.HttpResponseMessage
    '' ''                        If vstrVerbo = "POST" Then
    '' ''                            response = .PostAsync(strUriAPI, myContent).Result
    '' ''                        ElseIf vstrVerbo = "PUT" Then
    '' ''                            response = .PutAsync(strUriAPI, myContent).Result
    '' ''                        End If

    '' ''                        If response.IsSuccessStatusCode Then
    '' ''                            'Dim BEStatusClass As ResponseStatusSAPBE = JsonConvert.DeserializeObject(Of ResponseStatusSAPBE)(response.Content.ReadAsStringAsync().Result())
    '' ''                            'Dim ResponseStatusSAP = New With {Key .ErrCode = 0, .ErrMsg = String.Empty, .Id = String.Empty}
    '' ''                            Dim strJSONFormat As String = response.Content.ReadAsStringAsync().Result().ToString()
    '' ''                            'Dim BEStatusClass = JsonConvert.DeserializeAnonymousType(strJSONFormat, var)

    '' ''                            'Dim strBody As String = response.Content.ReadAsStringAsync().Result
    '' ''                            'Dim result = JsonConvert.DeserializeAnonymousType(strBody, ResponseStatusSAP)
    '' ''                            Dim BEResponseStatus As ResponseStatusSAP = Nothing
    '' ''                            Dim jss = New System.Web.Script.Serialization.JavaScriptSerializer()
    '' ''                            Dim data = jss.Deserialize(Of Object)(strJSONFormat)
    '' ''                            Dim blnErrorInesperado As Boolean = False
    '' ''                            Try
    '' ''                                Dim dicValorResponse As Dictionary(Of String, Object) = DirectCast(data, Dictionary(Of String, Object))
    '' ''                                If dicValorResponse.Count > 0 Then
    '' ''                                    Dim dcValores As Object() = dicValorResponse.Item("ResponseStatusSAP")
    '' ''                                    Dim objInterno As Object = dcValores

    '' ''                                    BEResponseStatus = New ResponseStatusSAP()
    '' ''                                    BEResponseStatus.ErrCode = CInt(objInterno(0)("ErrCode"))
    '' ''                                    BEResponseStatus.ErrMsg = objInterno(0)("ErrMsg")
    '' ''                                    BEResponseStatus.Id = String.Empty
    '' ''                                    If IsDBNull(objInterno(0)("Id")) = False Then
    '' ''                                        If Not objInterno(0)("Id") Is Nothing Then
    '' ''                                            BEResponseStatus.Id = objInterno(0)("Id").ToString()
    '' ''                                        End If
    '' ''                                    End If

    '' ''                                End If

    '' ''                                If Not BEResponseStatus Is Nothing Then
    '' ''                                    If BEResponseStatus.ErrCode <> 0 Then
    '' ''                                        blnErrorInesperado = True
    '' ''                                    End If
    '' ''                                End If
    '' ''                            Catch ex As Exception
    '' ''                                blnErrorInesperado = True
    '' ''                            End Try

    '' ''                            If blnErrorInesperado Then
    '' ''                                strMessageError &= "SAP: Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''                                If BEResponseStatus.ErrMsg <> String.Empty Then
    '' ''                                    strMessageError = "SAP: " & BEResponseStatus.ErrMsg
    '' ''                                End If
    '' ''                                Throw New Exception
    '' ''                            End If
    '' ''                        Else
    '' ''                            strMessageError &= "SAP: "
    '' ''                            Select Case response.StatusCode
    '' ''                                Case Net.HttpStatusCode.BadRequest
    '' ''                                    'strMessageError &= "Error al enviar la información desde SETRA."
    '' ''                                    Dim BEResponseStatus As ResponseStatusSAP = Nothing
    '' ''                                    Dim strJSONFormat As String = response.Content.ReadAsStringAsync().Result().ToString()
    '' ''                                    Dim jss = New System.Web.Script.Serialization.JavaScriptSerializer()
    '' ''                                    Dim data = jss.Deserialize(Of Object)(strJSONFormat)
    '' ''                                    Dim blnErrorInesperado As Boolean = False

    '' ''                                    Try
    '' ''                                        Dim dicValorResponse As Dictionary(Of String, Object) = DirectCast(data, Dictionary(Of String, Object))
    '' ''                                        If dicValorResponse.Count > 0 Then
    '' ''                                            Dim dcErrorModel As Dictionary(Of String, Object) = dicValorResponse.Item("ModelState")
    '' ''                                            'Dim objInterno As Object = dcValores.Item(0)
    '' ''                                            For Each ItemDic As KeyValuePair(Of String, Object) In dcErrorModel
    '' ''                                                Dim strKey As String = ItemDic.Key
    '' ''                                                Dim valObj As Object() = ItemDic.Value

    '' ''                                                Dim na As Integer = 0
    '' ''                                                strMessageError &= vbCrLf & "Error: " & Replace(strKey, "oBP.", String.Empty) & vbCrLf & _
    '' ''                                                                    valObj(0).ToString()
    '' ''                                            Next

    '' ''                                            'BEResponseStatus = New ResponseStatusSAP()
    '' ''                                            'BEResponseStatus.ErrCode = CInt(objInterno(0)("ErrCode"))
    '' ''                                            'BEResponseStatus.ErrMsg = objInterno(0)("ErrMsg")
    '' ''                                            'BEResponseStatus.Id = String.Empty
    '' ''                                            'If IsDBNull(objInterno(0)("Id")) = False Then
    '' ''                                            '    If Not objInterno(0)("Id") Is Nothing Then
    '' ''                                            '        BEResponseStatus.Id = objInterno(0)("Id").ToString()
    '' ''                                            '    End If
    '' ''                                            'End If
    '' ''                                        End If

    '' ''                                        If Not BEResponseStatus Is Nothing Then
    '' ''                                            If BEResponseStatus.ErrCode <> 0 Then
    '' ''                                                blnErrorInesperado = True
    '' ''                                            End If
    '' ''                                        End If
    '' ''                                    Catch ex As Exception
    '' ''                                        blnErrorInesperado = True
    '' ''                                    End Try

    '' ''                                    If blnErrorInesperado Then
    '' ''                                        strMessageError &= "SAP: Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''                                        If BEResponseStatus.ErrMsg <> String.Empty Then
    '' ''                                            strMessageError = "SAP: " & BEResponseStatus.ErrMsg
    '' ''                                        End If
    '' ''                                        Throw New Exception
    '' ''                                    End If
    '' ''                                Case Else
    '' ''                                    strMessageError &= "Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''                                    Throw New Exception
    '' ''                            End Select
    '' ''                            Throw New Exception
    '' ''                        End If
    '' ''                    Catch ex As Exception
    '' ''                        ContextUtil.SetAbort()
    '' ''                        Throw
    '' ''                    End Try
    '' ''                End If
    '' ''            End With
    '' ''        End If

    '' ''        ContextUtil.SetComplete()
    '' ''    Catch ex As Exception
    '' ''        ContextUtil.SetAbort()
    '' ''        If strMessageError = String.Empty Then
    '' ''            strMessageError = "Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
    '' ''        End If
    '' ''        Throw New ApplicationException(strMessageError)
    '' ''    End Try
    '' ''End Sub

    Private Sub ActualizarNuevoID(ByRef BE As clsClienteBE, ByVal vstrNuevoID As String)
        Try
            If Not BE.ListaSeriesCliente Is Nothing Then
                For Each Item As clsClienteBE.clsSeriesClienteBE In BE.ListaSeriesCliente
                    Item.IDCliente = vstrNuevoID
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub Actualizar(ByVal BE As clsClienteBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", BE.IDCliente)
                .Add("@RazonComercial", BE.RazonComercial)
                .Add("@RazonSocial", BE.RazonSocial)
                .Add("@Persona", BE.Persona)
                .Add("@Nombre", BE.Nombre)
                .Add("@ApPaterno", BE.ApPaterno)
                .Add("@ApMaterno", BE.ApMaterno)
                .Add("@Domiciliado", BE.Domiciliado)
                .Add("@Ciudad", BE.Ciudad)
                .Add("@Celular", BE.Celular)
                .Add("@Comision", BE.Comision)
                .Add("@Credito", BE.Credito)
                .Add("@MostrarEnReserva", BE.MostrarEnReserva)
                .Add("@Direccion", BE.Direccion)
                .Add("@Fax", BE.Fax)
                .Add("@IdIdentidad", BE.IdIdentidad)
                .Add("@NumIdentidad", BE.NumIdentidad)
                .Add("@Tipo", BE.Tipo)
                .Add("@IDCiudad", BE.IDCiudad)
                .Add("@IDVendedor", BE.IDVendedor)
                .Add("@Notas", BE.Notas)
                .Add("@Telefono1", BE.Telefono1)
                .Add("@Telefono2", BE.Telefono2)
                .Add("@Web", BE.Web)
                .Add("@CodPostal", BE.CodPostal)
                .Add("@Complejidad", BE.Complejidad)
                .Add("@Frecuencia", BE.Frecuencia)
                .Add("@Solvencia", BE.Solvencia)
                '.Add("@CoTipoVenta", BE.CoTipoVenta)
                .Add("@UserMod", BE.UserMod)
                'MLL-Agregado el dia 29-05-14
                .Add("@CoStarSoft", BE.CoStarSoft)
                .Add("@NuCuentaComision", BE.NuCuentaComision)

                .Add("@FlTop", BE.FlTop)
                '.Add("@FePerfilUpdate", BE.FePerfilUpdate)
                .Add("@CoUserPerfilUpdate", BE.CoUserPerfilUpdate)
                .Add("@txPerfilInformacion", BE.txPerfilInformacion)
                .Add("@txPerfilOperatividad", BE.txPerfilOperatividad)
                .Add("@NuPosicion", BE.NuPosicion)
                .Add("@FlProveedor", BE.FlProveedor)
            End With
            objADO.ExecuteSP("MACLIENTES_Upd", dc)

            If Not BE.ListaAdministCliente Is Nothing Then
                Dim objBTDet As New clsClienteBT.clsAdministClienteBT
                objBTDet.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaSeriesCliente Is Nothing Then
                Dim objBTSeries As New clsClienteBT.clsSeriesClienteBT
                objBTSeries.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaPerfilGuia Is Nothing Then
                Dim objBTGuias As New clsClienteBT.clsClientes_Perfil_GuiasBT
                objBTGuias.InsertarActualizarEliminar(BE)
            End If

            Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
            PasarDatosClientesToSocioNegocio(BE.IDCliente, BE.UserMod, oBESocioNegocio)
            'EnviarDatosRESTFul(oBESocioNegocio, "POST")

            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
            '                                     "http://sap:8899/api/BusinessPartners/", _
            '                                     "POST")
            oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                                               "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
                                               "POST")

            Actualizar_CardCodeSAP(BE.IDCliente, oBESocioNegocio.CardCode)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Actualizar_CardCodeSAP(ByVal strIDCliente As String, ByVal strCardCodeSAP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", strIDCliente)
                .Add("@CardCodeSAP", strCardCodeSAP)

            End With

            objADO.ExecuteSP("MACLIENTES_Upd_CardCodeSAP", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Anular(ByVal vstrIDCLiente As String, ByVal vstrUserMod As String)
        Try
            AnularSETRA(vstrIDCLiente, vstrUserMod)
            AnularSAP(vstrIDCLiente, vstrUserMod)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub AnularSETRA(ByVal vstrIDCliente As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("MACLIENTES_Anu", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub AnularSAP(ByVal vstrIDCliente As String, ByVal vstrUserMod As String)
        Try
            Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
            PasarDatosClientesToSocioNegocio(vstrIDCliente, vstrUserMod, oBESocioNegocio)
            'EnviarDatosRESTFul(oBESocioNegocio, "POST")

            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
            '                                     "http://sap:8899/api/BusinessPartners/", _
            '                                     "POST")
            oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                                              "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
                                              "POST")

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Actualizar_Tipo(ByVal vstrIDCliente As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@UserMod", vstrUserMod)

            End With

            objADO.ExecuteSP("MACLIENTES_Upd_Tipo", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsAdministClienteBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsClienteBE)
            Try
                Dim dc As Dictionary(Of String, String)

                For Each Item As clsClienteBE.clsAdministClienteBE In BE.ListaAdministCliente

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        dc.Add("@ApellidosTitular", Item.ApellidosTitular)
                        dc.Add("@Banco", Item.Banco)
                        dc.Add("@BancoInterm", Item.BancoInterm)
                        dc.Add("@CtaCte", Item.CtaCte)
                        dc.Add("@CtaInter", Item.CtaInter)
                        dc.Add("@IDAdminist", Item.IDAdminist)
                        dc.Add("@IDCliente", BE.IDCliente)
                        dc.Add("@IDMoneda", Item.IDMoneda)
                        dc.Add("@IVAN", Item.IVAN)
                        dc.Add("@NombresTitular", Item.NombresTitular)
                        dc.Add("@SWIFT", Item.SWIFT)
                        dc.Add("@ReferenciaAdminist", Item.ReferenciaAdminist)
                        dc.Add("@UserMod", Item.UserMod)
                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MAADMINISTCLIENTE_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MAADMINISTCLIENTE_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDCliente", BE.IDCliente)
                        dc.Add("@IDAdminist", Item.IDAdminist)
                        objADO.ExecuteSP("MAADMINISTCLIENTE_Del", dc)
                    End If

                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub


    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsSeriesClienteBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Private Sub Insertar(ByVal BE As clsClienteBE.clsSeriesClienteBE)
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@IDCliente", BE.IDCliente)
                    .Add("@NoSerie", BE.NoSerie)
                    .Add("@PoConcretizacion", BE.PoConcretizacion)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MASERIESCLIENTE_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Actualizar(ByVal BE As clsClienteBE.clsSeriesClienteBE)
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@NuSerie", BE.NuSerie)
                    .Add("@IDCliente", BE.IDCliente)
                    .Add("@NoSerie", BE.NoSerie)
                    .Add("@PoConcretizacion", BE.PoConcretizacion)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MASERIESCLIENTE_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal vstrIDCliente As String, ByVal vintIDSerie As Integer)
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@NuSerie", vintIDSerie)
                    .Add("@IDCliente", vstrIDCliente)
                End With

                objADO.ExecuteSP("MASERIESCLIENTE_Del", dc)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsClienteBE)
            Try
                For Each Item As clsClienteBE.clsSeriesClienteBE In BE.ListaSeriesCliente
                    If Item.Accion = "N" Then
                        Insertar(Item)
                    ElseIf Item.Accion = "M" Then
                        Actualizar(Item)
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDCliente, Item.NuSerie)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsContactoClienteBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub Insertar(ByVal BE As clsClienteBE.clsContactoClienteBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDCliente", BE.IDCliente)
                    .Add("@Titulo", BE.Titulo)
                    .Add("@Nombres", BE.Nombres)
                    .Add("@Apellidos", BE.Apellidos)
                    .Add("@Cargo", BE.Cargo)
                    .Add("@Telefono", BE.Telefono)
                    .Add("@Fax", BE.Fax)
                    .Add("@Celular", BE.Celular)
                    .Add("@Email", BE.Email)
                    .Add("@Email2", BE.Email2)
                    .Add("@Email3", BE.Email3)
                    .Add("@Anexo", BE.Anexo)
                    .Add("@UsuarioLogeo", BE.UsuarioLogeo)
                    .Add("@PasswordLogeo", BE.PasswordLogeo)
                    .Add("@FechaPassword", BE.FechaPassword)
                    .Add("@EsAdminist", BE.EsAdminist)
                    .Add("@Notas", BE.Notas)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@FlAccesoWeb", BE.FlAccesoWeb)
                End With

                objADO.ExecuteSP("MACONTACTOSCLIENTE_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsClienteBE.clsContactoClienteBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDContacto", BE.IDContacto)
                    .Add("@IDCliente", BE.IDCliente)
                    .Add("@Titulo", BE.Titulo)
                    .Add("@Nombres", BE.Nombres)
                    .Add("@Apellidos", BE.Apellidos)
                    .Add("@Cargo", BE.Cargo)
                    .Add("@Telefono", BE.Telefono)
                    .Add("@Fax", BE.Fax)
                    .Add("@Celular", BE.Celular)
                    .Add("@Email", BE.Email)
                    .Add("@Email2", BE.Email2)
                    .Add("@Email3", BE.Email3)
                    .Add("@Anexo", BE.Anexo)
                    .Add("@UsuarioLogeo", BE.UsuarioLogeo)
                    .Add("@PasswordLogeo", BE.PasswordLogeo)
                    .Add("@FechaPassword", BE.FechaPassword)
                    .Add("@EsAdminist", BE.EsAdminist)
                    .Add("@Notas", BE.Notas)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@FlAccesoWeb", BE.FlAccesoWeb)

                End With

                objADO.ExecuteSP("MACONTACTOSCLIENTE_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub


        Public Sub Actualizar_Extranet(ByVal BE As clsClienteBE.clsContactoClienteBE)
            'Dim dc As New Dictionary(Of String, String)
            'Try
            '    With dc
            '        .Add("@IDContacto", BE.IDContacto)
            '        .Add("@IDCliente", BE.IDCliente)

            '        .Add("@UsuarioLogeo", BE.UsuarioLogeo)
            '        .Add("@PasswordLogeo", BE.PasswordLogeo)
            '        .Add("@UsuarioLogeo_Hash", BE.UsuarioLogeo_Hash)

            '        .Add("@UserMod", BE.UserMod)

            '    End With

            '    objADO.ExecuteSP("MACONTACTOSCLIENTE_Upd_Extranet", dc)

            '    Dim objMenBT As New clsCorreoFileBT
            '    ' ''objMenBT.EnviarCorreoSQL_Contactos("SETRA", BE.UsuarioLogeo, "", BE.UsuarioLogeo_Hash, "TEXT", "", "G")

            '    ContextUtil.SetComplete()
            'Catch ex As Exception
            '    ContextUtil.SetAbort()
            '    Throw
            'End Try
        End Sub

        Public Sub Actualizar_Extranet_Pass(ByVal strPassword As String, ByVal strUsuarioLogeo_Hash As String, ByVal strUserMod As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc

                    .Add("@PasswordLogeo", strPassword)
                    .Add("@UsuarioLogeo_Hash", strUsuarioLogeo_Hash)

                    .Add("@UserMod", strUserMod)

                End With

                objADO.ExecuteSP("MACONTACTOSCLIENTE_Upd_Extranet_Pass", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar_Extranet_Restablecer_Pass(ByVal BE As clsClienteBE.clsContactoClienteBE)
            'Dim dc As New Dictionary(Of String, String)
            'Try
            '    With dc
            '        .Add("@IDContacto", BE.IDContacto)
            '        .Add("@IDCliente", BE.IDCliente)

            '        .Add("@UsuarioLogeo_Hash", BE.UsuarioLogeo_Hash)
            '        .Add("@UserMod", BE.UserMod)

            '    End With

            '    objADO.ExecuteSP("MACONTACTOSCLIENTE_Upd_Extranet_Restablecer_Pass", dc)

            '    Dim objMenBT As New clsCorreoFileBT
            '    ''objMenBT.EnviarCorreoSQL_Contactos("SETRA", BE.UsuarioLogeo, "", BE.UsuarioLogeo_Hash, "TEXT", "", "R")

            '    ContextUtil.SetComplete()
            'Catch ex As Exception
            '    ContextUtil.SetAbort()
            '    Throw
            'End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDContacto As String, ByVal vstrIDCliente As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDContacto", vstrIDContacto)
                    .Add("@IDCliente", vstrIDCliente)

                End With

                objADO.ExecuteSP("MACONTACTOSCLIENTE_Del", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsSeguimientoClienteBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsClienteBE.clsSeguimientoClienteBE) As String
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDCliente", BE.IDCliente)
                    .Add("@IDContacto", BE.IDContacto)
                    .Add("@Notas", BE.Notas)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDSeguim", 0)
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MASEGUIMCLIENTE_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Function


        Public Sub Actualizar(ByVal BE As clsClienteBE.clsSeguimientoClienteBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDSeguim", BE.IDSeguimiento)
                    .Add("@IDContacto", BE.IDContacto)
                    .Add("@Notas", BE.Notas)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MASEGUIMCLIENTE_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Sub


        Public Sub Eliminar(ByVal vstrIDSeguim As String)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDSeguim", vstrIDSeguim)
                End With

                objADO.ExecuteSP("MASEGUIMCLIENTE_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Sub
    End Class


    <ComVisible(True)> _
  <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsClientes_Perfil_GuiasBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsClienteBE.clsClientes_Perfil_GuiasBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@CoCliente", BE.CoCliente)
                    .Add("@CoProveedor", BE.CoProveedor)
                    .Add("@UserMod", BE.UserMod)

                End With


                objADO.ExecuteSP("MACLIENTES_PERFIL_GUIAS_Ins", dc)


                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Sub

        Public Sub Eliminar(ByVal BE As clsClienteBE.clsClientes_Perfil_GuiasBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@CoCliente", BE.CoCliente)
                    .Add("@CoProveedor", BE.CoProveedor)

                End With
                objADO.ExecuteSP("MACLIENTES_PERFIL_GUIAS_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsClienteBE)
            Try
                For Each Item As clsClienteBE.clsClientes_Perfil_GuiasBE In BE.ListaPerfilGuia
                    If Item.Accion = "N" Then
                        Item.CoCliente = BE.IDCliente
                        Insertar(Item)

                    ElseIf Item.Accion = "B" Then
                        Item.CoCliente = BE.IDCliente
                        Eliminar(Item)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

End Class
