﻿Imports ComSEToursBE2
Imports ComDataAccessD2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

'<JustInTimeActivation(True), _
'Synchronization(SynchronizationOption.Required), _
'Transaction(TransactionOption.Required)> _
Public Class clsTablasApoyoBT
    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsTipoDocBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsTipoDocBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IdTipoDoc", BE.IDTipoDoc)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Sunat", BE.Sunat)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@NoFiscal", BE.NoFiscal)
                    .Add("@CoSunat", BE.CoSunat)
                    .Add("@CoStarSoft", BE.CoStarSoft)

                End With

                objADO.ExecuteSP("MATIPODOC_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception

                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTipoDocBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDtipodoc", BE.IDTipoDoc)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Sunat", BE.Sunat)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@NoFiscal", BE.NoFiscal)
                    .Add("@CoSunat", BE.CoSunat)
                    .Add("@CoStarSoft", BE.CoStarSoft)
                End With

                objADO.ExecuteSP("MATIPODOC_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDTipoDoc As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDTipoDoc", vstrIDTipoDoc)

                End With

                objADO.ExecuteSP("MATIPODOC_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    Public Class clsENTRADASINC_SAPBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsENTRADASINC_SAPBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc

                    .Add("@IDCab", BE.IDCab)
                    .Add("@IDFile", BE.IDFile)
                    .Add("@Tipo", BE.Tipo)
                    .Add("@Monto", BE.Monto)
                    .Add("@FechaPago", BE.FechaPago)
                    .Add("@NuCodigo_ER", BE.NuCodigo_ER)
                    .Add("@NuDocumProv", BE.NuDocumProv)
                    .Add("@UserMod", BE.UserMod)
                  
                End With

                objADO.ExecuteSP("ENTRADASINC_SAP_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception

                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsDocumentoProveedorBE)
            If BE.ListaDetalle Is Nothing Then Exit Sub
            Try
                For Each Item As clsTablasApoyoBE.clsENTRADASINC_SAPBE In BE.ListaINC
                    If Item.Accion = "N" Then

                        Insertar(Item)
                        'ElseIf Item.Accion = "M" Then
                        '    Actualizar(Item)
                        'ElseIf Item.Accion = "B" Then
                        '    Eliminar(Item.NuDocumProv, Item.NuDocumProvDet)
                    End If
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsSeriesBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsSerieBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDSerie", BE.IDSerie)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MASERIES_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsSerieBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDSerie", BE.IDSerie)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MASERIES_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDSerie As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDSerie", vstrIDSerie)

                End With

                objADO.ExecuteSP("MASERIES_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCategoriaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsCategoriaBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Comision", BE.Comision)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDCat", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MACATEGORIA_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsCategoriaBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCat", BE.IDCat)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Comision", BE.Comision)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MACATEGORIA_Upd", dc)

                Dim objServ As New clsServicioProveedorBT
                objServ.ActualizarMargenxIDCat(BE.IDCat, BE.Comision, BE.UserMod)


                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDCat As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCat", vstrIDCat)

                End With

                objADO.ExecuteSP("MACATEGORIA_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCorrelativoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub ActualizarCorrelativoDocumento(ByVal BE As clsTablasApoyoBE.clsCorrelativoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Tabla", BE.Tabla)
                dc.Add("@IDTipoDoc", BE.IDTipoDoc)
                dc.Add("@CoSerie", BE.CoSerie)
                dc.Add("@Correlativo", BE.Correlativo)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("Correlativo_UpdDocumento", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCapacidadHabitacionBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsCapacidadHabitacionBE) As String
            Dim dc As New Dictionary(Of String, String)
            Dim strCoCapacidad As String = ""
            Try
                With dc
                    .Add("@NoCapacidad", BE.NoCapacidad)
                    .Add("@QtCapacidad", BE.QtCapacidad)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pCoCapacidad", strCoCapacidad)
                End With
                strCoCapacidad = objADO.ExecuteSPOutput("CAPACIDAD_HABITAC_Ins", dc)

                ContextUtil.SetComplete()
                Return strCoCapacidad
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function
        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsCapacidadHabitacionBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CoCapacidad", BE.CoCapacidad)
                    .Add("@NoCapacidad", BE.NoCapacidad)
                    .Add("@QtCapacidad", BE.QtCapacidad)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("CAPACIDAD_HABITAC_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub Eliminar(ByVal vstrCoCapacidad As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoCapacidad", vstrCoCapacidad)
                objADO.ExecuteSP("CAPACIDAD_HABITAC_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsModoServicioBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsModoServicioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CodModoServ", BE.CodModoServicio)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MAMODOSERVICIO_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsModoServicioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CodModoServ", BE.CodModoServicio)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MAMODOSERVICIO_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub Eliminar(ByVal vstrIdModoServicio As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CodModoServ", vstrIdModoServicio)
                End With

                objADO.ExecuteSP("MAMODOSERVICIO_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsDesayunoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsDesayunoBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDDesayuno", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MADESAYUNOS_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsDesayunoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDDesayuno", BE.IDDesayuno)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MADESAYUNOS_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDDesayuno As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDDesayuno", vstrIDDesayuno)
                End With
                objADO.ExecuteSP("MADESAYUNOS_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTarjetaCreditoBT
        Inherits ServicedComponent
        Dim ObjADO As New clsDataAccessDN
        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsTarjetaCreditoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodTar", BE.CodTar)
                dc.Add("@Descripcion", BE.Descripcion)
                dc.Add("@UserMod", BE.UserMod)

                ObjADO.ExecuteSP("MATARJETACREDITO_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrCodTar As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodTar", vstrCodTar)

                ObjADO.ExecuteSP("MATARJETACREDITO_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsIdiomaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsIdiomaBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@Siglas", BE.Siglas)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MAIDIOMAS_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsIdiomaBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@IDIdiomaUpd", BE.IDIdiomaUpd)
                    .Add("@Siglas", BE.Siglas)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MAIDIOMAS_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDIdioma As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIdioma", vstrIDIdioma)

                End With

                objADO.ExecuteSP("MAIDIOMAS_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class


    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTipoServicioBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsTipoServicioBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@CtaCon", BE.CtaCon)
                    .Add("@Ingles", BE.Ingles)
                    .Add("@SubDiario", BE.SubDiario)
                    .Add("@CtaVta", BE.CtaCon)
                    .Add("@SdVta", BE.SdVta)
                    .Add("@IdFactu", BE.IdFactu)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDservicio", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MATIPOSERV_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTipoServicioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDservicio", BE.IDServicio)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@CtaCon", BE.CtaCon)
                    .Add("@Ingles", BE.Ingles)
                    .Add("@SubDiario", BE.SubDiario)
                    .Add("@CtaVta", BE.CtaCon)
                    .Add("@SdVta", BE.SdVta)
                    .Add("@IdFactu", BE.IdFactu)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MATIPOSERV_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDservicio As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDservicio", vstrIDservicio)

                End With

                objADO.ExecuteSP("MATIPOSERV_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTipoUbigeoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsTipoUbigeoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.IDTipoUbigeo)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MATIPOUBIG_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTipoUbigeoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.IDTipoUbigeo)
                    .Add("@DescripcionUpd", BE.IDTipoUbigeoUpd)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MATIPOUBIG_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDTipoUbigeo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", vstrIDTipoUbigeo)

                End With

                objADO.ExecuteSP("MATIPOUBIG_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsPlantillaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsPlantillaBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@IDUsuario", BE.IDUsuario)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDDetalle", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MADETALLE_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsPlantillaBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDDetalle", BE.IDDetalle)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@IDIdioma", BE.IDIdioma)
                    .Add("@IDUsuario", BE.IDUsuario)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MADETALLE_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDDetalle As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDDetalle", vstrIDDetalle)

                End With

                objADO.ExecuteSP("MADETALLE_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTipoDocIdentBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsTipoDocIdentBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@CodPeruRail", BE.CodPeruRail) '22-05-14
                    .Add("@pIDIdentidad", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MATIPOIDENT_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTipoDocIdentBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIdentidad", BE.IDIdentidad)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@CodPeruRail", BE.CodPeruRail) '22-05-14

                End With

                objADO.ExecuteSP("MATIPOIDENT_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDIdentidad As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIdentidad", vstrIDIdentidad)

                End With

                objADO.ExecuteSP("MATIPOIDENT_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTipoOperacContabBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsTipoOperacContabBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@CtaContIGV", BE.CtaContIGV)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@TxDefinicion", BE.TxDefinicion)
                    .Add("@pIDToc", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MATIPOOPERACION_Ins", dc)

                ContextUtil.SetComplete()
                Return strOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTipoOperacContabBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDToc", BE.IDToc)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@CtaContIGV", BE.CtaContIGV)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@TxDefinicion", BE.TxDefinicion)

                End With

                objADO.ExecuteSP("MATIPOOPERACION_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDToc As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDToc", vstrIDToc)

                End With

                objADO.ExecuteSP("MATIPOOPERACION_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTiposDeCambioUSDBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsTiposDeCambioUSDBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CoMoneda", BE.CoMoneda)
                    .Add("@FeTipCam", BE.FeTipCam)
                    .Add("@SsTipCam", BE.SsTipCam)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MATIPOSCAMBIO_USD_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTiposDeCambioUSDBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CoMoneda", BE.CoMoneda)
                    .Add("@FeTipCam", BE.FeTipCam)
                    .Add("@SsTipCam", BE.SsTipCam)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MATIPOSCAMBIO_USD_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrCoMoneda As String, ByVal vdatFeTipCambio As Date)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CoMoneda", vstrCoMoneda)
                    .Add("@FeTipCam", vdatFeTipCambio)
                End With

                objADO.ExecuteSP("MATIPOSCAMBIO_USD_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTiposdeCambioBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsTiposdeCambioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Fecha", BE.Fecha)
                    .Add("@ValCompra", BE.ValCompra)
                    .Add("@ValVenta", BE.ValVenta)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MATIPOCAMBIO_Ins", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTiposdeCambioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Fecha", BE.Fecha)
                    .Add("@ValCompra", BE.ValCompra)
                    .Add("@ValVenta", BE.ValVenta)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MATIPOCAMBIO_Upd", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vdatFecha As Date)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Fecha", vdatFecha)

                End With

                objADO.ExecuteSP("MATIPOCAMBIO_Del", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()

                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
   <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTipoCambio_MonedasBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsTipoCambio_MonedasBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    '                   	@CoMoneda char(3),
                    '@Fecha smalldatetime,
                    '@ValCompra decimal=0,
                    '@ValVenta decimal=0,
                    '@UserMod char(4)
                    .Add("@CoMoneda", BE.CoMoneda)
                    .Add("@Fecha", BE.Fecha)
                    .Add("@ValCompra", BE.ValCompra)
                    .Add("@ValVenta", BE.ValVenta)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MATIPOCAMBIO_MONEDAS_Ins", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTipoCambio_MonedasBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CoMoneda", BE.CoMoneda)
                    .Add("@Fecha", BE.Fecha)
                    .Add("@ValCompra", BE.ValCompra)
                    .Add("@ValVenta", BE.ValVenta)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MATIPOCAMBIO_MONEDAS_Upd", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vdatFecha As Date, vstrIDMoneda As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Fecha", vdatFecha)
                    .Add("@CoMoneda", vstrIDMoneda)
                End With

                objADO.ExecuteSP("MATIPOCAMBIO_MONEDAS_Del", dc)
                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()

                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsIncluidoOperacionesBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsIncluidoOperacionesBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@FlInterno", BE.FlInterno)
                    .Add("@NuOrden", BE.NuOrden)
                    .Add("@NoObserAbrev", BE.NoObserAbrev)
                    .Add("@FlPorDefecto", BE.FlPorDefecto)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pNuIncluido", 0)
                End With
                Dim intIDIncluidoOperaciones = objADO.ExecuteSPOutput("MAINCLUIDOOPERACIONES_Ins", dc)

                Dim dcDet As Dictionary(Of String, String)
                For Each Item As clsTablasApoyoBE.clsIncluidoOperacionesIdiomasBE In BE.ListaIncluidoOperaciones
                    dcDet = New Dictionary(Of String, String)
                    With dcDet
                        .Add("@NuIncluido", intIDIncluidoOperaciones)
                        .Add("@IDIdioma", Item.IDIDioma)
                        .Add("@NoObservacion", Item.NoObservacion)
                        .Add("@UserMod", Item.UserMod)
                    End With
                    objADO.ExecuteSP("MAINCLUIDOOPERACIONES_IDIOMAS_Ins", dcDet)
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsIncluidoOperacionesBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@NuIncluido", BE.NuIncluido)
                    .Add("@FlInterno", BE.FlInterno)
                    .Add("@NuOrden", BE.NuOrden)
                    .Add("@NoObserAbrev", BE.NoObserAbrev)
                    .Add("@FlPorDefecto", BE.FlPorDefecto)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MAINCLUIDOOPERACIONES_Upd", dc)

                Dim dcDet As Dictionary(Of String, String)
                For Each Item As clsTablasApoyoBE.clsIncluidoOperacionesIdiomasBE In BE.ListaIncluidoOperaciones
                    dcDet = New Dictionary(Of String, String)
                    With dcDet
                        .Add("@NuIncluido", Item.NuIncluido)
                        .Add("@IDIdioma", Item.IDIDioma)
                        .Add("@NoObservacion", Item.NoObservacion)
                        .Add("@UserMod", Item.UserMod)
                    End With
                    If Not blnExisteIncluidoDiomas(Item.NuIncluido, Item.IDIDioma) Then
                        objADO.ExecuteSP("MAINCLUIDOOPERACIONES_IDIOMAS_Ins", dcDet)
                    Else
                        objADO.ExecuteSP("MAINCLUIDOOPERACIONES_IDIOMAS_Upd", dcDet)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vintNuIncluido As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuIncluido", vintNuIncluido)

                objADO.ExecuteSP("MAINCLUIDOOPERACIONES_IDIOMAS_DelxNuIncluido", dc)
                objADO.ExecuteSP("MAINCLUIDOOPERACIONES_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Function blnExisteIncluidoDiomas(ByVal vintNuIncluido As Integer, ByVal vstrIDIdioma As String) As Boolean
            Try
                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@NuIncluido", vintNuIncluido)
                    .Add("@IDIdioma", vstrIDIdioma)
                    .Add("@pExiste", False)
                End With
                Dim blnExiste As Boolean = objADO.GetSPOutputValue("MAINCLUIDOOPERACIONES_IDIOMAS_SelOutput", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsIncluidoServicioBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Sub Insertar(ByVal BE As clsTablasApoyoBE.clsIncluidoServicioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIncluido", BE.IDIncluido)
                dc.Add("@IDServicio", BE.IDServicio)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MAINCLUIDO_SERVICIOS_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal BE As clsTablasApoyoBE.clsIncluidoServicioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIncluido", BE.IDIncluido)
                dc.Add("@IDServicio", BE.IDServicio)

                objADO.ExecuteSP("MAINCLUIDO_SERVICIOS_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function blnExiste(ByVal vintIDIncluido As Integer, ByVal vstrIDServicio As String) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim blnExists As Boolean = False
                dc.Add("@IDIncluido", vintIDIncluido)
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@pExist", False)

                blnExists = objADO.GetSPOutputValue("MAINCLUIDO_SERVICIOS_Sel_ExistsxPK", dc)
                Return blnExists
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub InsertarEliminar(BE As clsTablasApoyoBE.clsIncluidoBE)
            If BE.ListaIncluidosServicio Is Nothing Then Exit Sub
            Try
                For Each Item As clsTablasApoyoBE.clsIncluidoServicioBE In BE.ListaIncluidosServicio
                    If Item.Accion = "N" Then
                        If Not blnExiste(Item.IDIncluido, Item.IDServicio) Then
                            Insertar(Item)
                        End If
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsIncluidoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@DescAbrev", BE.DescAbrev)
                    .Add("@Tipo", BE.Tipo)
                    .Add("@PorDefecto", BE.PorDefecto)
                    .Add("@Orden", BE.Orden)
                    .Add("@TxObsStatus", BE.TxObsStatus)
                    .Add("@FlAutomatico", BE.FlAutomatico)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDIncluido", 0)
                End With

                Dim intIDIncluido As Integer = objADO.ExecuteSPOutput("MAINCLUIDO_Ins", dc)

                Dim dcDet As Dictionary(Of String, String)
                For Each Item As clsTablasApoyoBE.clsIncluidoIdiomasBE In BE.ListaIncluidosIdioma
                    dcDet = New Dictionary(Of String, String)
                    With dcDet
                        .Add("@IDIncluido", intIDIncluido)
                        .Add("@Descripcion", Item.Descripcion)
                        .Add("@IDIdioma", Item.IDIdioma)
                        .Add("@UserMod", BE.UserMod)
                    End With
                    objADO.ExecuteSP("MAINCLUIDO_IDIOMAS_Ins", dcDet)
                Next

                ActualizarIDsNuevosenHijosST(BE, 0, intIDIncluido)
                Dim objBT As New clsTablasApoyoBT.clsIncluidoServicioBT
                objBT.InsertarEliminar(BE)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsTablasApoyoBE.clsIncluidoBE, ByVal vstrIDAntiguo As String, _
                                                 ByVal vinIDNuevo As Integer)
            Try
                If BE.ListaIncluidosServicio IsNot Nothing Then
                    For Each Item As clsTablasApoyoBE.clsIncluidoBE In BE.ListaIncluidosServicio
                        If Item.IDIncluido.ToString() = vstrIDAntiguo.ToString() Then
                            Item.IDIncluido = vinIDNuevo
                        End If
                    Next
                End If
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIncluido", BE.IDIncluido)
                    .Add("@DescAbrev", BE.DescAbrev)
                    .Add("@Tipo", BE.Tipo)
                    .Add("@PorDefecto", BE.PorDefecto)
                    .Add("@Orden", BE.Orden)
                    .Add("@FlAutomatico", BE.FlAutomatico)
                    .Add("@TxObsStatus", BE.TxObsStatus)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MAINCLUIDO_Upd", dc)

                Dim dcDet As Dictionary(Of String, String)
                For Each Item As clsTablasApoyoBE.clsIncluidoIdiomasBE In BE.ListaIncluidosIdioma
                    dcDet = New Dictionary(Of String, String)
                    With dcDet
                        .Add("@IDIncluido", BE.IDIncluido)
                        .Add("@Descripcion", Item.Descripcion)
                        .Add("@IDIdioma", Item.IDIdioma)
                        .Add("@UserMod", BE.UserMod)
                    End With
                    objADO.ExecuteSP("MAINCLUIDO_IDIOMAS_Upd", dcDet)
                Next

                Dim objBT As New clsTablasApoyoBT.clsIncluidoServicioBT
                objBT.InsertarEliminar(BE)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDIncluido As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIncluido", vstrIDIncluido)

                End With
                objADO.ExecuteSP("MAINCLUIDO_IDIOMAS_DelxIDIncluido", dc)
                objADO.ExecuteSP("MAINCLUIDO_SERVICIOS_DelxIDIncluido", dc)
                objADO.ExecuteSP("MAINCLUIDO_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsBancoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsBancoBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Moneda", BE.Moneda)
                    .Add("@NroCuenta", BE.NroCuenta)
                    .Add("@Sigla", BE.Sigla)
                    .Add("@CtaInter", BE.CtaInter)
                    .Add("@CtaCon", BE.CtaCon)
                    .Add("@Direccion", BE.Direccion)
                    .Add("@Swift", BE.Swift)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDBanco", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MABANCOS_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsBancoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDBanco", BE.IDBanco)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Moneda", BE.Moneda)
                    .Add("@NroCuenta", BE.NroCuenta)
                    .Add("@Sigla", BE.Sigla)
                    .Add("@CtaInter", BE.CtaInter)
                    .Add("@CtaCon", BE.CtaCon)
                    .Add("@Direccion", BE.Direccion)
                    .Add("@Swift", BE.Swift)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MABANCOS_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDBanco As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDBanco", vstrIDBanco)

                End With

                objADO.ExecuteSP("MABANCOS_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsHabitTripleBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsHabitTripleBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Abreviacion", BE.Abreviacion)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIdHabitTriple", "")
                End With
                Dim strIdTipoTriple As String = objADO.ExecuteSPOutput("MAHABITTRIPLE_Ins", dc)

                ContextUtil.SetComplete()
                Return strIdTipoTriple

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function
        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsHabitTripleBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IdHabitTriple", BE.IdHabitTriple)
                    .Add("@Abreviacion", BE.Abreviacion)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MAHABITTRIPLE_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub Eliminar(ByVal vstrIdTipoTriple As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IdHabitTriple", vstrIdTipoTriple)
                End With
                objADO.ExecuteSP("MAHABITTRIPLE_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class


    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsFormaPagoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsFormaPagoBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@SiglaEsUtil", BE.Tipo)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDFor", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MAFORMASPAGO_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsFormaPagoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDFor", BE.IDFor)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@SiglaEsUtil", BE.Tipo)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MAFORMASPAGO_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDFor As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDFor", vstrIDFor)

                End With

                objADO.ExecuteSP("MAFORMASPAGO_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsUbigeoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsUbigeoBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@TipoUbig", BE.TipoUbigeo)
                    .Add("@Prove", BE.Prove)
                    .Add("@NI", BE.NI)
                    .Add("@DC", BE.DC)
                    .Add("@IDPais", BE.IDPais)
                    .Add("@CodigoINC", BE.CodigoINC)
                    .Add("@CodigoPeruRail", BE.CodigoPeruRail)
                    .Add("@CoInternacPais", BE.CoInternacPais) 'nuevo campo 22/05/14
                    .Add("@EsOficina", BE.EsOficina)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDUbigeo", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MAUBIGEO_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsUbigeoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDUbigeo", BE.IDUbigeo)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@TipoUbig", BE.TipoUbigeo)
                    .Add("@Prove", BE.Prove)
                    .Add("@NI", BE.NI)
                    .Add("@DC", BE.DC)
                    .Add("@IDPais", BE.IDPais)
                    .Add("@CodigoINC", BE.CodigoINC)
                    .Add("@CodigoPeruRail", BE.CodigoPeruRail)
                    .Add("@EsOficina", BE.EsOficina)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@CoInternacPais", BE.CoInternacPais) ' nuevo campo
                End With

                objADO.ExecuteSP("MAUBIGEO_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDUbigeo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDUbigeo", vstrIDUbigeo)
                End With

                objADO.ExecuteSP("MAUBIGEO_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsPlanCuentasBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsPlanCuentasBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CtaContable", BE.CtaContable)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)

                    .Add("@CoCtroCosto", BE.CoCtroCosto)
                    .Add("@FlFacturacion", BE.FlFacturacion)
                    .Add("@FlClientes", BE.FlClientes)

                    .Add("@TxDefinicion", BE.TxDefinicion)


                End With
                objADO.ExecuteSP("MAPLANCUENTAS_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsPlanCuentasBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CtaContable", BE.CtaContable)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)

                    .Add("@CoCtroCosto", BE.CoCtroCosto)
                    .Add("@FlFacturacion", BE.FlFacturacion)
                    .Add("@FlClientes", BE.FlClientes)

                    .Add("@TxDefinicion", BE.TxDefinicion)

                End With

                objADO.ExecuteSP("MAPLANCUENTAS_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrCtaContable As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CtaContable", vstrCtaContable)
                End With

                objADO.ExecuteSP("MAPLANCUENTAS_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTipoProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsTipoProveedorBE) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@PideIdioma", BE.PideIdioma)
                    .Add("@Comision", BE.Comision)
                    .Add("@pIDTipoProv", "")
                End With

                Dim strOut As String = objADO.ExecuteSPOutput("MATIPOPROVEEDOR_Ins", dc)
                ContextUtil.SetComplete()
                Return strOut
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTipoProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDTipoProv", BE.IDTipoProv)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@PideIdioma", BE.PideIdioma)
                    .Add("@Comision", BE.Comision)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MATIPOPROVEEDOR_Upd", dc)

                If BE.IDTipoProv <> "001" Then 'No Hoteles
                    Dim objServ As New clsServicioProveedorBT
                    objServ.ActualizarMargenxIDTipoProv(BE.IDTipoProv, BE.Comision, BE.UserMod)
                End If

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vstrIDTipoProv As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDTipoProv", vstrIDTipoProv)

                End With

                objADO.ExecuteSP("MATIPOPROVEEDOR_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsFechasEspecialesBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsFechasEspecialesBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@IDUbigeo", BE.IDUbigeo)
                    .Add("@Fecha", BE.Fecha)
                    .Add("@NoConsideraAnio", BE.NoConsideraAnio)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MAFECHASNAC_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Insertar_Rango(ByVal BE As clsTablasApoyoBE.clsFechasEspecialesBE, ByVal vdatFechaFin As Date)
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim datFecha As Date = BE.Fecha
                While (datFecha <= vdatFechaFin)
                    dc = New Dictionary(Of String, String)
                    Dim strYearTmp As String = datFecha.Year
                    If BE.NoConsideraAnio = True Then
                        Dim strYear As String = datFecha.Year
                        datFecha = Replace(datFecha.ToShortDateString(), strYear, "1900")
                    End If

                    With dc
                        .Add("@Descripcion", BE.Descripcion)
                        .Add("@IDUbigeo", BE.IDUbigeo)
                        .Add("@Fecha", datFecha)
                        .Add("@NoConsideraAnio", BE.NoConsideraAnio)
                        .Add("@UserMod", BE.UserMod)
                    End With

                    objADO.ExecuteSP("MAFECHASNAC_Ins", dc)
                    If BE.NoConsideraAnio Then
                        datFecha = Replace(datFecha.ToShortDateString(), "1900", strYearTmp)
                    End If
                    datFecha = datFecha.AddDays(1)
                End While

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub


        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsFechasEspecialesBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@IDUbigeo", BE.IDUbigeo)
                    .Add("@Fecha", BE.Fecha)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MAFECHASNAC_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vdatFecha As Date, ByVal vstrIDUbigeo As String, _
                            ByVal vblnNoConsiderarAnio As Boolean)
            Dim dc As New Dictionary(Of String, String)
            Try
                If vblnNoConsiderarAnio Then
                    Dim strYear As String = vdatFecha.Year
                    vdatFecha = Replace(vdatFecha.ToShortDateString(), strYear, "1900")
                End If

                With dc
                    .Add("@IDUbigeo", vstrIDUbigeo)
                    .Add("@Fecha", vdatFecha)
                End With

                objADO.ExecuteSP("MAFECHASNAC_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class



    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsRecordatoriosReservaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsReservaBE.clsRecordatoriosBE) As Short
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Dias", BE.Dias)
                    .Add("@TipoCalculoTarea", BE.TipoCalculoTarea)
                    .Add("@Activo", BE.Activo)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDRecordatorio", "")

                End With

                Dim intOut As Short = objADO.ExecuteSPOutput("MARECORDATORIOSRESERVAS_Ins", dc)
                ContextUtil.SetComplete()
                Return intOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsReservaBE.clsRecordatoriosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDRecordatorio", BE.IDRecordatorio)
                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Dias", BE.Dias)
                    .Add("@TipoCalculoTarea", BE.TipoCalculoTarea)
                    .Add("@Activo", BE.Activo)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MARECORDATORIOSRESERVAS_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vintIDRecordatorio As Short)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDRecordatorio", vintIDRecordatorio)
                End With

                objADO.ExecuteSP("MARECORDATORIOSRESERVAS_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsVehiculosBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsVehiculosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@QtDe", BE.QtDe)
                dc.Add("@QtHasta", BE.QtHasta)
                dc.Add("@NoVehiculoCitado", BE.NoVehiculoCitado)
                dc.Add("@QtCapacidad", BE.QtCapacidad)
                dc.Add("@FlOtraCiudad", BE.FlOtraCiudad)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MAVEHICULOS_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsVehiculosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculo", BE.NuVehiculo)
                dc.Add("@QtDe", BE.QtDe)
                dc.Add("@QtHasta", BE.QtHasta)
                dc.Add("@NoVehiculoCitado", BE.NoVehiculoCitado)
                dc.Add("@QtCapacidad", BE.QtCapacidad)
                dc.Add("@FlOtraCiudad", BE.FlOtraCiudad)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MAVEHICULOS_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vbytNuVehiculo As Byte)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculo", vbytNuVehiculo)

                objADO.ExecuteSP("MAVEHICULOS_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    '--------------------------

    <ComVisible(True)> _
   <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsVehiculos_TransporteBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsVehiculos_TransporteBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@QtDe", BE.QtDe)
                dc.Add("@QtHasta", BE.QtHasta)
                dc.Add("@NoVehiculoCitado", BE.NoVehiculoCitado)
                dc.Add("@QtCapacidad", BE.QtCapacidad)
                dc.Add("@FlOtraCiudad", BE.FlOtraCiudad)

                dc.Add("@CoTipo", BE.CoTipo)
                dc.Add("@CoUbigeo", BE.CoUbigeo)
                dc.Add("@CoMercado", BE.CoMercado)

                dc.Add("@FlReservas", BE.FlReserva)
                dc.Add("@FlProgramacion", BE.FlProgramacion)

                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MAVEHICULOS_TRANSPORTE_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsVehiculos_TransporteBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculo", BE.NuVehiculo)
                dc.Add("@QtDe", BE.QtDe)
                dc.Add("@QtHasta", BE.QtHasta)
                dc.Add("@NoVehiculoCitado", BE.NoVehiculoCitado)
                dc.Add("@QtCapacidad", BE.QtCapacidad)
                dc.Add("@FlOtraCiudad", BE.FlOtraCiudad)

                dc.Add("@CoTipo", BE.CoTipo)
                dc.Add("@CoUbigeo", BE.CoUbigeo)
                dc.Add("@CoMercado", BE.CoMercado)

                dc.Add("@UserMod", BE.UserMod)

                dc.Add("@FlReservas", BE.FlReserva)
                dc.Add("@FlProgramacion", BE.FlProgramacion)

                objADO.ExecuteSP("MAVEHICULOS_TRANSPORTE_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vbytNuVehiculo As Byte)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculo", vbytNuVehiculo)

                objADO.ExecuteSP("MAVEHICULOS_TRANSPORTE_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    '--------------------------

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCuestionarioBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsCuestionarioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDTipoProv", BE.IDTipoProv)
                dc.Add("@NoDescripcion", BE.NoDescripcion)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MACUESTIONARIO_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsCuestionarioBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuCuest", BE.NuCuest)
                dc.Add("@IDTipoProv", BE.IDTipoProv)
                dc.Add("@NoDescripcion", BE.NoDescripcion)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("MACUESTIONARIO_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Anular(ByVal vbytNuCuest As Byte, ByVal vstrUserMod As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuCuest", vbytNuCuest)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("MACUESTIONARIO_Anu", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsVehiculosProgramacionBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsVehiculosProgramacionBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@NoUnidadProg", BE.NoUnidadProg)
                    .Add("@QtCapacidadPax", BE.QtCapacidadPax)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MAVEHICULOSPROGRAMACION_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsVehiculosProgramacionBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@NuVehiculoProg", BE.NuVehiculoProg)
                    .Add("@NoUnidadProg", BE.NoUnidadProg)
                    .Add("@QtCapacidadPax", BE.QtCapacidadPax)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MAVEHICULOSPROGRAMACION_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub Eliminar(ByVal vintNuVehiculoProg As Int16)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculoProg", vintNuVehiculoProg)

                objADO.ExecuteSP("MAVEHICULOSPROGRAMACION_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
   <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
   Public Class clsDiasFeriadosBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsDiasFeriadosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@FechaFeriado", BE.FechaFeriado)
                    .Add("@IDPais", BE.IDPais)
                    .Add("@Descripcion", BE.DescripcionF)
                    .Add("@UserMod", BE.UserMod)
                    '.Add("@pIDFeriado", "")

                End With

                'Dim intOut As Short = objADO.ExecuteSPOutput("MAFERIADOS_Ins", dc)
                objADO.ExecuteSP("MAFERIADOS_Ins", dc)
                ContextUtil.SetComplete()
                'Return intOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsDiasFeriadosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@FechaFeriado", BE.FechaFeriado)
                    .Add("@IDPais", BE.IDPais)
                    .Add("@Descripcion", BE.DescripcionF)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MAFERIADOS_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vFechaFeriado As Date, ByVal vstrIDPais As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@FechaFeriado", vFechaFeriado)
                    .Add("@IDPais", vstrIDPais)
                End With

                objADO.ExecuteSP("MAFERIADOS_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsParametroBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsTablasApoyoBE.clsParametroBE)
            Try

                Dim dc As New Dictionary(Of String, String)
                With dc
                    .Add("@NoNombreEmpresa", BE.NoNombreEmpresa)
                    .Add("@NoRUCEmpresa", BE.NoRUCEmpresa)
                    .Add("@NoRSocialEmpresa", BE.NoRSocialEmpresa)
                    .Add("@NoDireccEmpresa", BE.NoDireccEmpresa)
                    .Add("@NuTelefEmpresa", BE.NuTelefEmpresa)
                    .Add("@NuFaxEmpresa", BE.NuFaxEmpresa)
                    .Add("@NoEmailEmpresa", BE.NoEmailEmpresa)
                    .Add("@NoWebEmpresa", BE.NoWebEmpresa)
                    .Add("@NoRutaCorreoMarketing", BE.NoRutaCorreoMarketing)
                    .Add("@NoRutaCorreoMarketingServer", BE.NoRutaCorreoMarketingServer)
                    .Add("@NoRutaImagenes", BE.NoRutaImagenes)
                    .Add("@NoRutaIniPdfProveedor", BE.NoRutaIniPdfProveedor)
                    .Add("@NoRutaIniFotoWordVtas", BE.NoRutaIniFotoWordVtas)
                    .Add("@NoRutaVouchers", BE.NoRutaVouchers)
                    .Add("@NoRutaPerfilCorreosSQL", BE.NoRutaPerfilCorreosSQL)
                    .Add("@NoServerCorreo", BE.NoServerCorreo)
                    .Add("@NoRutaPerfilCorreosSQLServer", BE.NoRutaPerfilCorreosSQLServer)
                    .Add("@NuIGV", BE.NuIGV)
                    .Add("@PoProrrata", BE.PoProrrata)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("PARAMETRO_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsParametroBE)
            Try
                Dim dc As New Dictionary(Of String, String)

                With dc
                    .Add("@NoNombreEmpresa", BE.NoNombreEmpresa)
                    .Add("@NoRUCEmpresa", BE.NoRUCEmpresa)
                    .Add("@NoRSocialEmpresa", BE.NoRSocialEmpresa)
                    .Add("@NoDireccEmpresa", BE.NoDireccEmpresa)
                    .Add("@NuTelefEmpresa", BE.NuTelefEmpresa)
                    .Add("@NuFaxEmpresa", BE.NuFaxEmpresa)
                    .Add("@NoEmailEmpresa", BE.NoEmailEmpresa)
                    .Add("@NoWebEmpresa", BE.NoWebEmpresa)
                    .Add("@NoRutaCorreoMarketing", BE.NoRutaCorreoMarketing)
                    .Add("@NoRutaCorreoMarketingServer", BE.NoRutaCorreoMarketingServer)
                    .Add("@NoRutaImagenes", BE.NoRutaImagenes)
                    .Add("@NoRutaIniPdfProveedor", BE.NoRutaIniPdfProveedor)
                    .Add("@NoRutaIniFotoWordVtas", BE.NoRutaIniFotoWordVtas)
                    .Add("@NoRutaVouchers", BE.NoRutaVouchers)
                    .Add("@NoRutaPerfilCorreosSQL", BE.NoRutaPerfilCorreosSQL)
                    .Add("@NoServerCorreo", BE.NoServerCorreo)
                    .Add("@NoRutaPerfilCorreosSQLServer", BE.NoRutaPerfilCorreosSQLServer)
                    .Add("@NuIGV", BE.NuIGV)
                    .Add("@PoProrrata", BE.PoProrrata)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("PARAMETRO_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsProrrataBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT

            Private Sub Insertar(ByVal BE As clsTablasApoyoBE.clsParametroBE.clsProrrataBE)
                Dim dc As New Dictionary(Of String, String)
                Try
                    With dc
                        .Add("@FeDesde", BE.FeDesde)
                        .Add("@FeHasta", BE.FeHasta)
                        .Add("@QtProrrata", BE.QtProrrata)
                        .Add("@UserMod", BE.UserMod)
                    End With

                    objADO.ExecuteSP("PRORRATA_Ins", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Private Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsParametroBE.clsProrrataBE)
                Dim dc As New Dictionary(Of String, String)
                Try
                    With dc
                        .Add("@NuCodigoPro", BE.NuCodigoPro)
                        .Add("@FeDesde", BE.FeDesde)
                        .Add("@FeHasta", BE.FeHasta)
                        .Add("@QtProrrata", BE.QtProrrata)
                        .Add("@UserMod", BE.UserMod)
                    End With

                    objADO.ExecuteSP("PRORRATA_Upd", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Private Sub Eliminar(ByVal vintNuCodigoPro As Integer)
                Dim dc As New Dictionary(Of String, String)
                Try
                    With dc
                        .Add("@NuCodigoPro", vintNuCodigoPro)
                    End With

                    objADO.ExecuteSP("PRORRATA_Del", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub InsertarActualizarEliminar(ByVal BE As clsTablasApoyoBE.clsParametroBE.clsProrrataBE)
                Try
                    For Each Item As clsTablasApoyoBE.clsParametroBE.clsProrrataBE In BE.ListaProrrata
                        If Item.Accion = "N" Then
                            Insertar(Item)
                        ElseIf Item.Accion = "M" Then
                            Actualizar(Item)
                        ElseIf Item.Accion = "B" Then
                            Eliminar(Item.NuCodigoPro)
                        End If
                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub
        End Class
    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTemporadaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Function Insertar(ByVal BE As clsTablasApoyoBE.clsTemporadaBE) As Int16
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@NombreTemporada", BE.Descripcion)
                    .Add("@TipoTemporada", BE.TipoTemporada)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDTemporada", 0)
                End With

                Dim intOut As Int16 = objADO.ExecuteSPOutput("MATEMPORADA_Ins", dc)
                ContextUtil.SetComplete()
                Return intOut

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub Actualizar(ByVal BE As clsTablasApoyoBE.clsTemporadaBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDTemporada", BE.IDTemporada)
                    .Add("@NombreTemporada", BE.Descripcion)
                    .Add("@TipoTemporada", BE.TipoTemporada)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MATEMPORADA_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal vintIDIdentidad As Int16)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDTemporada", vintIDIdentidad)

                End With

                objADO.ExecuteSP("MATEMPORADA_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

End Class
