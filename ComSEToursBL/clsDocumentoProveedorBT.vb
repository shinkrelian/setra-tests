﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDocumentoProveedorBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT

    Private strNombreClase As String = "clsDocumentoProveedorBT"
    Public Property intJsonMaster As intJsonInterfaceBT

    Public Sub New()
        intJsonMaster = New clsJsonInterfaceBT()
    End Sub

    Public Overloads Function Grabar(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vblnSAP As Boolean = True,
                                     Optional ByVal vListaDetPres As List(Of clsPresupuesto_Sobre_DetBE) = Nothing,
                                     Optional ByVal BEDocForEgr As clsDocumento_Forma_Egreso = Nothing) As String

        Try

            If BE.CoObligacionPago = "OCP" Then 'Orden Compra Administracion
                'Dim objOrdComBT As New clsOrdenCompraBT
                'If BE.NuOrdComInt = 0 Then
                '    BE.NuOrdComInt = objOrdComBT.Insertar(New clsOrdenCompraBE With {.FeOrdCom = BE.FeEmision, _
                '                                                .CoProveedor = BE.CoProveedor, _
                '                                                .CoMoneda = BE.CoMoneda, _
                '                                                .SsTotal = BE.SSTotal, _
                '                                                .SsImpuestos = BE.SsIGV, _
                '                                                .UserMod = BE.UserMod _
                '                                                })
                'End If

            ElseIf BE.CoObligacionPago = "PRL" Then
                Dim objVouTrenBT As New clsVoucherProveedorTrenBT
                'If BE.NuVouPTrenInt = 0 Then
                Dim intNuVouPTrenInt As Integer = intNuVouPTrenIntxFileProveedor(BE.IDCab, BE.CoProveedor)
                If intNuVouPTrenInt = 0 Then
                    BE.NuVouPTrenInt = objVouTrenBT.Insertar(New clsVoucherProveedorTrenBE With {.FeVoucher = BE.FeEmision,
                                                                    .CoProveedor = BE.CoProveedor,
                                                                    .IdCab = BE.IDCab,
                                                                    .CoMoneda = BE.CoMoneda,
                                                                    .SsTotal = BE.SsTotal_FEgreso,
                                                                    .SsImpuestos = BE.SsIGV,
                                                                    .CoUbigeo_Oficina = BE.CoUbigeo_Oficina,
                                                                    .UserMod = BE.UserMod
                                                                    })
                    If BE.IDOperacion = 0 Then
                        objVouTrenBT.ActualizarNuVouPTrenIntenOperacionesxProvee(BE.CoProveedor, BE.IDCab, BE.NuVouPTrenInt, BE.UserMod)
                    Else
                        objVouTrenBT.ActualizarNuVouPTrenIntenOperaciones(BE.IDOperacion, BE.NuVouPTrenInt, BE.UserMod)
                    End If

                Else
                    BE.NuVouPTrenInt = intNuVouPTrenInt
                    objVouTrenBT.ActualizarTotal(intNuVouPTrenInt, BE.SsTotal_FEgreso, BE.SsIGV, BE.UserMod)

                End If
            ElseIf BE.CoObligacionPago = "ZIC" Then
                If BE.IDDocFormaEgreso = 0 Then
                    Dim objBTDocForEgr As New clsDocumento_Forma_EgresoBT()
                    BE.IDDocFormaEgreso = objBTDocForEgr.intInsertIDDocFormaEgreso(BEDocForEgr)
                End If
            End If

            If Not (BE.CoObligacionPago = "PST" And vblnSAP) Then
                Insertar(BE, vblnSAP)
            End If


            'If BE.CoTipoDoc <> "NCR" Then

            Dim dblRenta4ta As Double = 0
            If BE.CoTipoDoc.Trim = "RH" And BE.SsIGV > 0 Then
                dblRenta4ta = BE.SsIGV
            End If

            If BE.CoObligacionPago = "VOU" Then
                Dim objBTVou As New clsVoucherOperacionBT
                objBTVou.ActualizarSaldoPendiente(BE.NuVoucher, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod, BE.CoMoneda_FEgreso)
            ElseIf BE.CoObligacionPago = "OSV" Then
                If BE.NuOrden_Servicio <> 0 Then
                    Dim objBTOrd As New clsOrdenServicioBT
                    objBTOrd.ActualizarSaldoPendiente(BE.NuOrden_Servicio, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod, BE.CoMoneda_FEgreso)
                End If
            ElseIf BE.CoObligacionPago = "OPA" Then
                Dim objBTOrdPag As New clsOrdenPagoBT
                objBTOrdPag.ActualizarSaldoPendiente(BE.CoOrdPag, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            ElseIf BE.CoObligacionPago = "OCP" Then
                Dim objOCBT As New clsOrdenCompraBT
                objOCBT.ActualizarSaldoPendiente(BE.NuOrdComInt, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            ElseIf BE.CoObligacionPago = "VBA" Or BE.CoObligacionPago = "VSA" Then
                Dim objBT As New clsVoucherOficinaExternaBT
                objBT.ActualizarSaldoPendiente(BE.NuVouOfiExtInt, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            ElseIf BE.CoObligacionPago = "PST" Then
                Dim objBT As New clsPresupuesto_SobreBT
                If BE.CoMoneda = "SOL" Then
                    'objBT.ActualizarSaldoPendiente(BE.NuPreSob, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal), BE.UserMod)
                    objBT.ActualizarSaldoPendiente(BE.NuPreSob, BE.SSTotalOriginal + dblRenta4ta, BE.UserMod)
                Else
                    'objBT.ActualizarSaldoPendiente_USD(BE.NuPreSob, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal), BE.UserMod)
                    objBT.ActualizarSaldoPendiente_USD(BE.NuPreSob, BE.SSTotalOriginal, BE.UserMod)
                End If

                If Not vListaDetPres Is Nothing Then
                    Dim objDetPre As New clsPresupuesto_Sobre_DetBT
                    For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In vListaDetPres
                        objDetPre.Actualizar(ItemDetPre)
                    Next
                    Dim objBEPre As New clsPresupuesto_SobreBE With {.ListaPresupuestoSobreDet = vListaDetPres, .CoEstado = "EN", .IDCab = BE.IDCab}
                    objDetPre.ActualizarStockEntradas(objBEPre)
                End If
                If vblnSAP And BE.SSTotal > 0 Then Insertar(BE, vblnSAP)

            ElseIf BE.CoObligacionPago = "PRL" Then
                Dim objVouTrenBT As New clsVoucherProveedorTrenBT
                objVouTrenBT.ActualizarSaldoPendiente(BE.NuVouPTrenInt, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)

            ElseIf BE.CoObligacionPago = "FFI" Then
                Dim objFondoFijoBT As New clsFondoFijoBT
                objFondoFijoBT.ActualizarSaldoPendiente(BE.NuFondoFijo, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)

            ElseIf BE.CoObligacionPago = "ZIC" Then
                Dim objBTDocForEgr As New clsDocumento_Forma_EgresoBT()
                'objBTDocForEgr.ActualizarSaldoPendiente(BE.IDDocFormaEgreso, BE.CoObligacionPago, BE.CoMoneda, BE.SSTotalOriginal, BE.UserMod)
                objBTDocForEgr.ActualizarSaldoPendiente(BE.IDDocFormaEgreso, BE.CoObligacionPago, BE.CoMoneda_FEgreso, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            End If
            ' End If

            ContextUtil.SetComplete()

            Return ""
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
            'Return ex.Message
        End Try
    End Function

    Public Overloads Sub Grabar_Multiple(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vblnSAP As Boolean = True)

        Try

            'If BE.CoObligacionPago = "OCP" Then 'Orden Compra Administracion
            '    'Dim objOrdComBT As New clsOrdenCompraBT
            '    'If BE.NuOrdComInt = 0 Then
            '    '    BE.NuOrdComInt = objOrdComBT.Insertar(New clsOrdenCompraBE With {.FeOrdCom = BE.FeEmision, _
            '    '                                                .CoProveedor = BE.CoProveedor, _
            '    '                                                .CoMoneda = BE.CoMoneda, _
            '    '                                                .SsTotal = BE.SSTotal, _
            '    '                                                .SsImpuestos = BE.SsIGV, _
            '    '                                                .UserMod = BE.UserMod _
            '    '                                                })
            '    'End If

            'ElseIf BE.CoObligacionPago = "PRL" Then
            '    Dim objVouTrenBT As New clsVoucherProveedorTrenBT
            '    'If BE.NuVouPTrenInt = 0 Then
            '    Dim intNuVouPTrenInt As Integer = intNuVouPTrenIntxFileProveedor(BE.IDCab, BE.CoProveedor)
            '    If intNuVouPTrenInt = 0 Then
            '        BE.NuVouPTrenInt = objVouTrenBT.Insertar(New clsVoucherProveedorTrenBE With {.FeVoucher = BE.FeEmision, _
            '                                                        .CoProveedor = BE.CoProveedor, _
            '                                                        .IdCab = BE.IDCab, _
            '                                                        .CoMoneda = BE.CoMoneda, _
            '                                                        .SsTotal = BE.SsTotal_FEgreso, _
            '                                                        .SsImpuestos = BE.SsIGV, _
            '                                                        .CoUbigeo_Oficina = BE.CoUbigeo_Oficina, _
            '                                                        .UserMod = BE.UserMod _
            '                                                        })
            '        If BE.IDOperacion = 0 Then
            '            objVouTrenBT.ActualizarNuVouPTrenIntenOperacionesxProvee(BE.CoProveedor, BE.IDCab, BE.NuVouPTrenInt, BE.UserMod)
            '        Else
            '            objVouTrenBT.ActualizarNuVouPTrenIntenOperaciones(BE.IDOperacion, BE.NuVouPTrenInt, BE.UserMod)
            '        End If

            '    Else
            '        BE.NuVouPTrenInt = intNuVouPTrenInt
            '        objVouTrenBT.ActualizarTotal(intNuVouPTrenInt, BE.SsTotal_FEgreso, BE.SsIGV, BE.UserMod)

            '    End If
            'End If


            'Pasa como parametro la cabecera y la lista de documentos dentro de BE

            Insertar_Multiple(BE, vblnSAP)
            'Insertar(BE, False)



            ''If BE.CoTipoDoc <> "NCR" Then

            'Dim dblRenta4ta As Double = 0
            'If BE.CoTipoDoc.Trim = "RH" And BE.SsIGV > 0 Then
            '    dblRenta4ta = BE.SsIGV
            'End If

            'If BE.CoObligacionPago = "VOU" Then
            '    Dim objBTVou As New clsVoucherOperacionBT
            '    objBTVou.ActualizarSaldoPendiente(BE.NuVoucher, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod, BE.CoMoneda_FEgreso)
            'ElseIf BE.CoObligacionPago = "OSV" Then
            '    Dim objBTOrd As New clsOrdenServicioBT
            '    objBTOrd.ActualizarSaldoPendiente(BE.NuOrden_Servicio, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            'ElseIf BE.CoObligacionPago = "OPA" Then
            '    Dim objBTOrdPag As New clsOrdenPagoBT
            '    objBTOrdPag.ActualizarSaldoPendiente(BE.CoOrdPag, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            'ElseIf BE.CoObligacionPago = "OCP" Then
            '    Dim objOCBT As New clsOrdenCompraBT
            '    objOCBT.ActualizarSaldoPendiente(BE.NuOrdComInt, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            'ElseIf BE.CoObligacionPago = "VBA" Or BE.CoObligacionPago = "VSA" Then
            '    Dim objBT As New clsVoucherOficinaExternaBT
            '    objBT.ActualizarSaldoPendiente(BE.NuVouOfiExtInt, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            'ElseIf BE.CoObligacionPago = "PST" Then
            '    Dim objBT As New clsPresupuesto_SobreBT
            '    If BE.CoMoneda = "SOL" Then
            '        'objBT.ActualizarSaldoPendiente(BE.NuPreSob, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal), BE.UserMod)
            '        objBT.ActualizarSaldoPendiente(BE.NuPreSob, BE.SSTotalOriginal + dblRenta4ta, BE.UserMod)
            '    Else
            '        'objBT.ActualizarSaldoPendiente_USD(BE.NuPreSob, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal), BE.UserMod)
            '        objBT.ActualizarSaldoPendiente_USD(BE.NuPreSob, BE.SSTotalOriginal, BE.UserMod)
            '    End If

            'ElseIf BE.CoObligacionPago = "PRL" Then
            '    Dim objVouTrenBT As New clsVoucherProveedorTrenBT
            '    objVouTrenBT.ActualizarSaldoPendiente(BE.NuVouPTrenInt, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)

            'ElseIf BE.CoObligacionPago = "FFI" Then
            '    Dim objFondoFijoBT As New clsFondoFijoBT
            '    objFondoFijoBT.ActualizarSaldoPendiente(BE.NuFondoFijo, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            'End If
            '' End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub


    Public Overloads Sub Grabar_INC(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vblnSAP As Boolean = True)
        Try

            Insertar_EntradasINC(BE, True)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function strTipoDocxCoSAP(ByVal vstrCoSAP As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoSAP", vstrCoSAP)
            dc.Add("@pIDtipodoc", "")

            Return objADO.GetSPOutputValue("MATIPODOC_Sel_xCoSAP", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    'Public Sub GrabarxStockPresupuesto(ByVal BEPre As clsPresupuesto_SobreBE)
    '    Try
    '        Dim objBN As New clsDocumentoProveedorBN
    '        Dim objServ As New clsServicioProveedorBN
    '        Dim objStk As New clsStockTicketEntradasIngresosBN

    '        Dim ListMuseos As New List(Of String)
    '        For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In BEPre.ListaPresupuestoSobreDet
    '            If BEPre.CoEstado = "EN" And ItemDetPre.CoPago = "STK" And ItemDetPre.SsTotSus > 0 Then
    '                If Not ListMuseos.Contains(ItemDetPre.IDServicio) Then
    '                    ListMuseos.Add(ItemDetPre.IDServicio)
    '                End If
    '            End If
    '        Next

    '        For Each strIDServicio As String In ListMuseos
    '            Dim dblTotal As Double = 0
    '            Dim intIDServicio_Det_V_Cot As Integer = 0
    '            Dim strDescServicio As String = ""
    '            For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In BEPre.ListaPresupuestoSobreDet
    '                If BEPre.CoEstado = "EN" And ItemDetPre.CoPago = "STK" And ItemDetPre.IDServicio = strIDServicio Then
    '                    dblTotal += (ItemDetPre.SsTotSus * ItemDetPre.SsPreUni)
    '                    intIDServicio_Det_V_Cot = ItemDetPre.IDServicio_Det_V_Cot
    '                    strDescServicio = ItemDetPre.TxServicio
    '                End If
    '            Next

    '            Dim objBE As New clsDocumentoProveedorBE
    '            With objBE
    '                .NuDocInterno = objBN.strDevuelveNuDocInterno_Correlativo(Now.ToString("dd/MM/yyyy"))
    '                .NuVoucher = 0 'vstrVoucher 'intNuVoucher
    '                .NuOrden_Servicio = 0
    '                .CoOrdPag = 0
    '                .NuOrdComInt = 0
    '                .NuPreSob = BEPre.NuPreSob
    '                .NuVouPTrenInt = 0

    '                .IDCab = 0
    '                .IDCab_FF = 0

    '                .NuSerie = ""
    '                .NuDocum = ""

    '                Dim strNroDocNroOper As String = objStk.strNroOperacionDocumentoCompraStock(strIDServicio)
    '                If InStr(strNroDocNroOper, "-") > 0 Then
    '                    .CoTipDocSusEnt = strNroDocNroOper.Substring(0, InStr(strNroDocNroOper, "-") - 1)
    '                    .NuDocumSusEnt = Mid(strNroDocNroOper, InStr(strNroDocNroOper, "-") + 1)
    '                    .NuOperBanSusEnt = ""
    '                Else
    '                    .CoTipDocSusEnt = ""
    '                    .NuDocumSusEnt = ""
    '                    .NuOperBanSusEnt = strNroDocNroOper
    '                End If

    '                .CoTipoDoc = "FAC"
    '                .FeEmision = Now.ToString("dd/MM/yyyy")
    '                .FeRecepcion = Now.ToString("dd/MM/yyyy")

    '                .CoTipoDetraccion = "00000"
    '                .CoMoneda = "SOL"
    '                .CoMoneda_Pago = ""
    '                .SsIGV = 0
    '                .SsNeto = dblTotal
    '                .SsOtrCargos = 0
    '                .SsDetraccion = 0
    '                .SSTotalOriginal = dblTotal
    '                .SSTipoCambio = 0
    '                .SSTotal = dblTotal

    '                .CoCecos = "900101"
    '                .CoTipoOC = "004"
    '                .CoCtaContab = "65931098"
    '                .CoObligacionPago = "OTR"
    '                .IngresoManual = False
    '                .CoMoneda_FEgreso = ""

    '                .SsTipoCambio_FEgreso = 0
    '                .SsTotal_FEgreso = BEPre.SsSaldo

    '                .IDOperacion = 0
    '                .CoProveedor = objServ.strConsultarProveedor(intIDServicio_Det_V_Cot)

    '                .SSPercepcion = 0
    '                '.TxConcepto = "SUSTENTO COMPRA STOCK ENTRADAS: " & strDescServicio
    '                .TxConcepto = strDescServicio


    '                .FeVencimiento = "01/01/1900"
    '                .CoTipoDocSAP = 2
    '                .CoFormaPago = "012" 'al contado


    '                .CoTipoDoc_Ref = ""
    '                .NuSerie_Ref = ""
    '                .NuDocum_Ref = ""
    '                .FeEmision_Ref = "01/01/1900"


    '                .CoCeCon = "000000"
    '                .CoGasto = ""
    '                '.CoUbigeo_Oficina = ""
    '                .UserMod = BEPre.UserMod
    '                .UserNuevo = BEPre.UserMod
    '            End With

    '            Dim LstDetalle As New List(Of clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)
    '            Dim bytNuDocumProvDet As Byte = 1
    '            For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In BEPre.ListaPresupuestoSobreDet

    '                If BEPre.CoEstado = "EN" And ItemDetPre.CoPago = "STK" And ItemDetPre.IDServicio = strIDServicio Then
    '                    Dim objBEIti As New clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
    '                    With objBEIti
    '                        .NuDocumProv = 0 'bytGenerarCoDocumento()
    '                        .NuDocumProvDet = bytNuDocumProvDet
    '                        .IDServicio_Det = ItemDetPre.IDServicio_Det_V_Cot
    '                        .TxServicio = ItemDetPre.TxServicio
    '                        .QtCantidad = ItemDetPre.SsTotSus
    '                        .SsMonto = ItemDetPre.SsTotSus * ItemDetPre.SsPreUni
    '                        .Accion = "N"
    '                        .UserMod = BEPre.UserMod
    '                        .IDCab = 0
    '                        .CoProducto = ""
    '                        .CoAlmacen = ""
    '                        .SsIGV = 0
    '                        .SSPrecUni = ItemDetPre.SsPreUni
    '                        .SsTotal = ItemDetPre.SsTotSus * ItemDetPre.SsPreUni
    '                    End With


    '                    LstDetalle.Add(objBEIti)
    '                    bytNuDocumProvDet += 1
    '                End If
    '            Next
    '            objBE.ListaDetalle = LstDetalle

    '            Grabar(objBE, True)

    '        Next


    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub
    Public Function strGrabar(ByVal BE As clsDocumentoProveedorBE) As String
        Try
            Dim objBN As New clsDocumentoProveedorBN
            If objBN.blnExisteDocumProvee(BE.NuDocum, BE.NuSerie, BE.IDCab, BE.CoProveedor) Then
                Return "Ya existe el documento."
            End If

            Dim objVueBT As New clsVuelosVentasCotizacionBT
            BE.NuDocInterno = objVueBT.strDevuelveNuDocInterno_Correlativo(BE.FeEmision.Date.ToString("dd/MM/yyyy"))
            BE.CoTipoDoc = strTipoDocxCoSAP(BE.CoTipoDoc)

            Grabar(BE, True)

            ContextUtil.SetComplete()
            Return ""
        Catch ex As Exception
            ContextUtil.SetAbort()
            Return ex.Message
        End Try
    End Function

    Private Function intIDDocFormaEgresoxCotiCab(ByVal vintIDCab As Integer, ByVal vstrCoObligacionPago As String) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pNuVouPTrenInt", 0)

            Return objADO.GetSPOutputValue("VOUCHER_PROV_TREN_SelIDxIDCabProveeOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".intIDDocFormaEgresoxCotiCab")
        End Try
    End Function

    Private Function intNuVouPTrenIntxFileProveedor(ByVal vintIDCab As Integer, ByVal vstrCoProveedor As String) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@pNuVouPTrenInt", 0)

            Return objADO.GetSPOutputValue("VOUCHER_PROV_TREN_SelIDxIDCabProveeOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".intNuVouPTrenIntxFileProveedor")
        End Try
    End Function

    Private Overloads Sub Insertar(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vblnSAP As Boolean = True)
        Try
            Dim dc As New Dictionary(Of String, String)

            Dim intNuDocumProv As Integer = 0

            dc.Add("@NuVoucher", BE.NuVoucher)
            dc.Add("@NuOrden_Servicio", BE.NuOrden_Servicio)
            dc.Add("@NuSerie", BE.NuSerie)
            dc.Add("@NuDocum", BE.NuDocum)
            dc.Add("@NuDocInterno", BE.NuDocInterno)
            dc.Add("@CoTipoDoc", BE.CoTipoDoc)

            dc.Add("@FeEmision", BE.FeEmision)
            dc.Add("@FeRecepcion", BE.FeRecepcion)
            dc.Add("@CoTipoDetraccion", BE.CoTipoDetraccion)
            dc.Add("@CoMoneda", BE.CoMoneda)
            dc.Add("@SsIGV", BE.SsIGV)

            dc.Add("@SsNeto", BE.SsNeto)
            dc.Add("@SsOtrCargos", BE.SsOtrCargos)
            dc.Add("@SsDetraccion", BE.SsDetraccion)

            dc.Add("@SSTotalOriginal", BE.SSTotalOriginal)
            dc.Add("@SSTipoCambio", BE.SSTipoCambio)
            dc.Add("@SSTotal", BE.SSTotal)

            dc.Add("@CoTipoOC", BE.CoTipoOC)
            dc.Add("@CoCtaContab", BE.CoCtaContab)
            dc.Add("@CoObligacionPago", BE.CoObligacionPago)
            dc.Add("@CoMoneda_Pago", BE.CoMoneda_Pago)
            dc.Add("@CoMoneda_FEgreso", BE.CoMoneda_FEgreso)
            dc.Add("@SsTipoCambio_FEgreso", BE.SsTipoCambio_FEgreso)
            dc.Add("@SsTotal_FEgreso", BE.SsTotal_FEgreso)

            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@UserNuevo", BE.UserNuevo)
            dc.Add("@CoOrdPag", BE.CoOrdPag)
            dc.Add("@IDOperacion", BE.IDOperacion)
            dc.Add("@NuOrdComInt", BE.NuOrdComInt)

            dc.Add("@NuVouOfiExtInt", BE.NuVouOfiExtInt)
            dc.Add("@NuDocumVta", BE.NuDocumVta)
            dc.Add("@CoTipoDocVta", BE.CoTipoDocVta)
            dc.Add("@NuPreSob", BE.NuPreSob)
            dc.Add("@CoCecos", BE.CoCecos)
            dc.Add("@CoGasto", BE.CoGasto)
            dc.Add("@NuVouPTrenInt", BE.NuVouPTrenInt)

            If Not BE.NuFondoFijo = Nothing Then
                If BE.NuFondoFijo > 0 Then
                    dc.Add("@NuFondoFijo", BE.NuFondoFijo)
                    dc.Add("@CoTipoFondoFijo", BE.CoTipoFondoFijo)
                    'dc.Add("@SSPercepcion", BE.SSPercepcion)
                    'dc.Add("@TxConcepto", BE.TxConcepto)
                    'dc.Add("@CoProveedor", BE.CoProveedor)
                    'dc.Add("@IDCab", BE.IDCab_FF)
                End If
            End If

            dc.Add("@SSPercepcion", BE.SSPercepcion)
            dc.Add("@TxConcepto", BE.TxConcepto)
            dc.Add("@CoProveedor", BE.CoProveedor)
            dc.Add("@IDCab", BE.IDCab_FF)

            '--
            dc.Add("@FeVencimiento", BE.FeVencimiento)
            dc.Add("@CoTipoDocSAP", BE.CoTipoDocSAP)
            dc.Add("@CoFormaPago", BE.CoFormaPago)
            dc.Add("@CoTipoDoc_Ref", BE.CoTipoDoc_Ref)
            dc.Add("@NuSerie_Ref", BE.NuSerie_Ref)
            dc.Add("@NuDocum_Ref", BE.NuDocum_Ref)
            dc.Add("@FeEmision_Ref", BE.FeEmision_Ref)
            dc.Add("@CoCeCon", BE.CoCeCon)

            dc.Add("@FlMultiple", BE.FlMultiple)
            dc.Add("@NuDocum_Multiple", BE.NuDocum_Multiple)

            dc.Add("@NuDocumSusEnt", BE.NuDocumSusEnt)
            dc.Add("@CoTipDocSusEnt", BE.CoTipDocSusEnt)
            dc.Add("@NuOperBanSusEnt", BE.NuOperBanSusEnt)
            dc.Add("@IDDocFormaEgreso", BE.IDDocFormaEgreso)

            dc.Add("@pNuDocumProv", 0)



            'objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Ins", dc)

            intNuDocumProv = objADO.ExecuteSPOutput("DOCUMENTO_PROVEEDOR_Ins", dc)



            Dim objDetBT As New clsDocumentoProveedor_DetBT
            objDetBT.ActualizarIDsNuevosenHijosST(BE, BE.NuDocumProv, intNuDocumProv)

            Dim objBT As New clsDocumentoProveedor_DetBT
            objBT.InsertarActualizarEliminar(BE)

            ''------------SAP


            If vblnSAP Then
                Dim arrDeleteProperties As String() = Nothing


                Dim oBEPurchaseInvoice As clsPurchaseInvoicesBE = Nothing
                PasarDatosDocumentosToSocioNegocio(intNuDocumProv, BE.UserMod, oBEPurchaseInvoice, arrDeleteProperties)




                Dim oBTJsonMaster = Me.intJsonMaster 'As New clsJsonInterfaceBT()

                'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                '                                     "http://sap:8899/api/PurchaseInvoices/", _
                '                                     "POST", arrDeleteProperties)
                oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                                 "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                                 "POST", arrDeleteProperties)


                Actualizar_CardCodeSAP(intNuDocumProv, objResponseSAP.Id)
            End If
            '------------FIN SAP


            'If BE.CoTipoDoc <> "NCR" Then

            '    If BE.NuVoucher > 0 Then
            '        Dim objBTVou As New clsVoucherOperacionBT
            '        objBTVou.ActualizarSaldoPendiente(BE.NuVoucher, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal), BE.UserMod)
            '    Else
            '        If BE.NuOrden_Servicio > 0 Then
            '            Dim objBTOrd As New clsOrdenServicioBT
            '            objBTOrd.ActualizarSaldoPendiente(BE.NuOrden_Servicio, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal), BE.UserMod)
            '        Else
            '            Dim objBTOrdPag As New clsOrdenPagoBT
            '            objBTOrdPag.ActualizarSaldoPendiente(BE.CoOrdPag, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal), BE.UserMod)
            '        End If

            '    End If
            'End If
            BE.NuDocumProv = intNuDocumProv
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Insertar_Multiple(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vblnSAP As Boolean = True)
        Try
            Dim dc As New Dictionary(Of String, String)

            Dim intNuDocumProv As Integer = 0

            dc.Add("@NuVoucher", BE.NuVoucher)
            dc.Add("@NuOrden_Servicio", BE.NuOrden_Servicio)
            dc.Add("@NuSerie", BE.NuSerie)
            dc.Add("@NuDocum", BE.NuDocum)
            dc.Add("@NuDocInterno", BE.NuDocInterno)
            dc.Add("@CoTipoDoc", BE.CoTipoDoc)

            dc.Add("@FeEmision", BE.FeEmision)
            dc.Add("@FeRecepcion", BE.FeRecepcion)
            dc.Add("@CoTipoDetraccion", BE.CoTipoDetraccion)
            dc.Add("@CoMoneda", BE.CoMoneda)
            dc.Add("@SsIGV", BE.SsIGV)

            dc.Add("@SsNeto", BE.SsNeto)
            dc.Add("@SsOtrCargos", BE.SsOtrCargos)
            dc.Add("@SsDetraccion", BE.SsDetraccion)

            dc.Add("@SSTotalOriginal", BE.SSTotalOriginal)
            dc.Add("@SSTipoCambio", BE.SSTipoCambio)
            dc.Add("@SSTotal", BE.SSTotal)

            dc.Add("@CoTipoOC", BE.CoTipoOC)
            dc.Add("@CoCtaContab", BE.CoCtaContab)
            dc.Add("@CoObligacionPago", BE.CoObligacionPago)
            dc.Add("@CoMoneda_Pago", BE.CoMoneda_Pago)
            dc.Add("@CoMoneda_FEgreso", BE.CoMoneda_FEgreso)
            dc.Add("@SsTipoCambio_FEgreso", BE.SsTipoCambio_FEgreso)
            dc.Add("@SsTotal_FEgreso", BE.SsTotal_FEgreso)

            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@UserNuevo", BE.UserNuevo)
            dc.Add("@CoOrdPag", BE.CoOrdPag)
            dc.Add("@IDOperacion", BE.IDOperacion)
            dc.Add("@NuOrdComInt", BE.NuOrdComInt)

            dc.Add("@NuVouOfiExtInt", BE.NuVouOfiExtInt)
            dc.Add("@NuDocumVta", BE.NuDocumVta)
            dc.Add("@CoTipoDocVta", BE.CoTipoDocVta)
            dc.Add("@NuPreSob", BE.NuPreSob)
            dc.Add("@CoCecos", BE.CoCecos)
            dc.Add("@CoGasto", BE.CoGasto)
            dc.Add("@NuVouPTrenInt", BE.NuVouPTrenInt)

            If Not BE.NuFondoFijo = Nothing Then
                If BE.NuFondoFijo > 0 Then
                    dc.Add("@NuFondoFijo", BE.NuFondoFijo)
                    dc.Add("@CoTipoFondoFijo", BE.CoTipoFondoFijo)
                    'dc.Add("@SSPercepcion", BE.SSPercepcion)
                    'dc.Add("@TxConcepto", BE.TxConcepto)
                    'dc.Add("@CoProveedor", BE.CoProveedor)
                    'dc.Add("@IDCab", BE.IDCab_FF)
                End If
            End If

            dc.Add("@SSPercepcion", BE.SSPercepcion)
            dc.Add("@TxConcepto", BE.TxConcepto)
            dc.Add("@CoProveedor", BE.CoProveedor)
            dc.Add("@IDCab", BE.IDCab_FF)

            '--
            dc.Add("@FeVencimiento", BE.FeVencimiento)
            dc.Add("@CoTipoDocSAP", BE.CoTipoDocSAP)
            dc.Add("@CoFormaPago", BE.CoFormaPago)
            dc.Add("@CoTipoDoc_Ref", BE.CoTipoDoc_Ref)
            dc.Add("@NuSerie_Ref", BE.NuSerie_Ref)
            dc.Add("@NuDocum_Ref", BE.NuDocum_Ref)
            dc.Add("@FeEmision_Ref", BE.FeEmision_Ref)
            dc.Add("@CoCeCon", BE.CoCeCon)

            dc.Add("@FlMultiple", BE.FlMultiple)
            dc.Add("@NuDocum_Multiple", BE.NuDocum_Multiple)

            dc.Add("@pNuDocumProv", 0)



            'objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Ins", dc)

            intNuDocumProv = objADO.ExecuteSPOutput("DOCUMENTO_PROVEEDOR_Ins", dc)
            Dim objBTVou As New clsVoucherOperacionBT


            For Each BETmp As clsDocumentoProveedorBE In BE.ListaDocumentos
                If BETmp.CoObligacionPago = "VOU" Then
                    Dim dblRenta4ta As Double = 0
                    If BETmp.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
                        dblRenta4ta = BETmp.SsIGV
                    End If

                    BETmp.NuDocum_Multiple = intNuDocumProv
                    Insertar(BETmp, False)
                    objBTVou.ActualizarSaldoPendiente(BETmp.NuVoucher, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod, BETmp.CoMoneda_FEgreso)
                    objBTVou.ActualizarEstado_Multiple(BETmp.NuVoucher, "AC", BETmp.UserMod, BETmp.CoMoneda_FEgreso)
                ElseIf BETmp.CoObligacionPago = "OPA" Then
                    Dim dblRenta4ta As Double = 0
                    If BETmp.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
                        dblRenta4ta = BETmp.SsIGV
                    End If
                    Dim objBTOrdPag As New clsOrdenPagoBT

                    BETmp.NuDocum_Multiple = intNuDocumProv
                    Insertar(BETmp, False)
                    'objBTVou.ActualizarSaldoPendiente(BETmp.NuVoucher, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod, BETmp.CoMoneda_FEgreso)
                    objBTOrdPag.ActualizarSaldoPendiente(BETmp.CoOrdPag, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod)
                    objBTOrdPag.ActualizarEstado_Multiple(BETmp.CoOrdPag, "AC", BETmp.UserMod)
                ElseIf BETmp.CoObligacionPago = "OSV" Then
                    Dim dblRenta4ta As Double = 0
                    If BETmp.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
                        dblRenta4ta = BETmp.SsIGV
                    End If
                    'Dim objBTOrdPag As New clsOrdenPagoBT

                    Dim objBTOrd As New clsOrdenServicioBT

                    BETmp.NuDocum_Multiple = intNuDocumProv
                    Insertar(BETmp, False)
                    'objBTVou.ActualizarSaldoPendiente(BETmp.NuVoucher, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod, BETmp.CoMoneda_FEgreso)
                    'objBTOrdPag.ActualizarSaldoPendiente(BE.CoOrdPag, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
                    objBTOrd.ActualizarSaldoPendiente(BETmp.NuOrden_Servicio, (If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta), BETmp.UserMod, BETmp.CoMoneda_FEgreso)
                    ActualizarEstado_Multiple_OSV(BETmp.NuOrden_Servicio, "AC", BETmp.UserMod, BETmp.CoMoneda_FEgreso)
                ElseIf BETmp.CoObligacionPago = "PTT" Then
                    BETmp.NuDocum_Multiple = intNuDocumProv
                    Insertar(BETmp, False)

                    Dim objBT As New clsPresupuesto_SobreBT
                    Dim dblRenta4ta As Double = 0
                    If BE.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
                        dblRenta4ta = BETmp.SsIGV
                    End If

                    If BETmp.AceptarPresupuestoTicket Then
                        objBT.ActualizarEstadoAC_DifAceptada(BETmp.NuPreSob, (BETmp.SSTotalOriginal + dblRenta4ta), BE.UserMod)
                    Else
                        objBT.ActualizarSaldoPendiente(BETmp.NuPreSob, (BETmp.SSTotalOriginal + dblRenta4ta), BE.UserMod)
                    End If

                ElseIf BETmp.CoObligacionPago = "ZIC" Then
                    Dim objBTDocForEgr As New clsDocumento_Forma_EgresoBT()
                    Dim objBEDocForEgr As New clsDocumento_Forma_Egreso()
                    Dim dblRenta4ta As Double = 0
                    If BETmp.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
                        dblRenta4ta = BETmp.SsIGV
                    End If

                    If BETmp.IDDocFormaEgreso = 0 Then
                        With objBEDocForEgr
                            .IDCab = BETmp.IDCab
                            .TipoFormaEgreso = BETmp.CoObligacionPago
                            .CoMoneda = BETmp.CoMoneda
                            .SsMontoTotal = BETmp.SSTotalOriginal
                            .SsSaldo = BETmp.SSTotalOriginal
                            .SsDifAceptada = 0
                            .TxObsDocumento = BETmp.TxConcepto
                            .CoEstado = BETmp.CoEstado
                            .UserMod = BETmp.UserMod
                            .CoUbigeo_Oficina = BETmp.CoUbigeo_Oficina
                        End With

                        BETmp.IDDocFormaEgreso = objBTDocForEgr.intInsertIDDocFormaEgreso(objBEDocForEgr)
                    End If

                    BETmp.NuDocum_Multiple = intNuDocumProv

                    Insertar(BETmp, False)

                    objBTDocForEgr.ActualizarSaldoPendiente(BETmp.IDDocFormaEgreso, BETmp.CoObligacionPago, BETmp.CoMoneda_FEgreso, BETmp.SSTotalOriginal, BETmp.UserMod)

                    objBTDocForEgr.ActualizarEstado_Multiple(BETmp.IDDocFormaEgreso, BETmp.CoObligacionPago, "AC", BETmp.UserMod, BETmp.CoMoneda_FEgreso)

                End If


            Next

            'If BE.CoObligacionPago = "VOU" Then
            '    For Each BETmp As clsDocumentoProveedorBE In BE.ListaDocumentos

            '        Dim dblRenta4ta As Double = 0
            '        If BETmp.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
            '            dblRenta4ta = BETmp.SsIGV
            '        End If

            '        BETmp.NuDocum_Multiple = intNuDocumProv
            '        Insertar(BETmp, False)
            '        objBTVou.ActualizarSaldoPendiente(BETmp.NuVoucher, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod, BETmp.CoMoneda_FEgreso)
            '        objBTVou.ActualizarEstado_Multiple(BETmp.NuVoucher, "AC", BETmp.UserMod, BETmp.CoMoneda_FEgreso)
            '    Next
            'ElseIf BE.CoObligacionPago = "OPA" Then
            '    For Each BETmp As clsDocumentoProveedorBE In BE.ListaDocumentos

            '        Dim dblRenta4ta As Double = 0
            '        If BETmp.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
            '            dblRenta4ta = BETmp.SsIGV
            '        End If
            '        Dim objBTOrdPag As New clsOrdenPagoBT

            '        BETmp.NuDocum_Multiple = intNuDocumProv
            '        Insertar(BETmp, False)
            '        'objBTVou.ActualizarSaldoPendiente(BETmp.NuVoucher, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod, BETmp.CoMoneda_FEgreso)
            '        objBTOrdPag.ActualizarSaldoPendiente(BETmp.CoOrdPag, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod)
            '        objBTOrdPag.ActualizarEstado_Multiple(BETmp.CoOrdPag, "AC", BETmp.UserMod)
            '    Next
            'ElseIf BE.CoObligacionPago = "OSV" Then
            '    For Each BETmp As clsDocumentoProveedorBE In BE.ListaDocumentos

            '        Dim dblRenta4ta As Double = 0
            '        If BETmp.CoTipoDoc.Trim = "RH" And BETmp.SsIGV > 0 Then
            '            dblRenta4ta = BETmp.SsIGV
            '        End If
            '        Dim objBTOrdPag As New clsOrdenPagoBT

            '        Dim objBTOrd As New clsOrdenServicioBT

            '        BETmp.NuDocum_Multiple = intNuDocumProv
            '        Insertar(BETmp, False)
            '        'objBTVou.ActualizarSaldoPendiente(BETmp.NuVoucher, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod, BETmp.CoMoneda_FEgreso)
            '        'objBTOrdPag.ActualizarSaldoPendiente(BE.CoOrdPag, If(BE.CoMoneda <> BE.CoMoneda_FEgreso, BE.SsTotal_FEgreso, BE.SSTotalOriginal) + dblRenta4ta, BE.UserMod)
            '        objBTOrd.ActualizarSaldoPendiente(BETmp.NuOrden_Servicio, If(BETmp.CoMoneda <> BETmp.CoMoneda_FEgreso, BETmp.SsTotal_FEgreso, BETmp.SSTotalOriginal) + dblRenta4ta, BETmp.UserMod)
            '        ActualizarEstado_Multiple_OSV(BETmp.NuOrden_Servicio, "AC", BETmp.UserMod)
            '    Next
            'End If


            'For Each BETmp As clsDocumentoProveedorBE In BE.ListaDocumentos
            '    Insertar(BETmp, False)
            'Next


            'Dim objDetBT As New clsDocumentoProveedor_DetBT
            'objDetBT.ActualizarIDsNuevosenHijosST(BE, BE.NuDocumProv, intNuDocumProv)

            'Dim objBT As New clsDocumentoProveedor_DetBT
            'objBT.InsertarActualizarEliminar(BE)

            ''------------SAP

            If vblnSAP Then
                Dim arrDeleteProperties As String() = Nothing


                Dim oBEPurchaseInvoice As clsPurchaseInvoicesBE = Nothing
                PasarDatosDocumentosToSocioNegocio_Multiple(intNuDocumProv, BE.UserMod, oBEPurchaseInvoice, arrDeleteProperties)


                Dim oBTJsonMaster As New clsJsonInterfaceBT()
                'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                '                                     "http://sap:8899/api/PurchaseInvoices/", _
                '                                     "POST", arrDeleteProperties)
                oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                                 "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                                 "POST", arrDeleteProperties)


                Actualizar_CardCodeSAP_Multiple(intNuDocumProv, objResponseSAP.Id)
            End If
            '------------FIN SAP

            BE.NuDocumProv = intNuDocumProv
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Overloads Sub ActualizarEstado_Multiple_OSV(ByVal vintNuOrden_Servicio As Integer, ByVal vstrCoEstado As String, ByVal vstrUserMod As String, Optional ByVal vstrCoMoneda As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrden_Servicio", vintNuOrden_Servicio)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@CoMoneda", vstrCoMoneda)

            objADO.ExecuteSP("ORDEN_SERVICIO_Upd_Estado_Multiple", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub


    Private Overloads Sub Insertar_EntradasINC(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vblnSAP As Boolean = True)
        Try
            Dim dc As New Dictionary(Of String, String)

            Dim intNuDocumProv As Integer = 0

            dc.Add("@NuVoucher", BE.NuVoucher)
            dc.Add("@NuOrden_Servicio", BE.NuOrden_Servicio)
            dc.Add("@NuSerie", BE.NuSerie)
            dc.Add("@NuDocum", BE.NuDocum)
            dc.Add("@NuDocInterno", BE.NuDocInterno)
            dc.Add("@CoTipoDoc", BE.CoTipoDoc)

            dc.Add("@FeEmision", BE.FeEmision)
            dc.Add("@FeRecepcion", BE.FeRecepcion)
            dc.Add("@CoTipoDetraccion", BE.CoTipoDetraccion)
            dc.Add("@CoMoneda", BE.CoMoneda)
            dc.Add("@SsIGV", BE.SsIGV)

            dc.Add("@SsNeto", BE.SsNeto)
            dc.Add("@SsOtrCargos", BE.SsOtrCargos)
            dc.Add("@SsDetraccion", BE.SsDetraccion)

            dc.Add("@SSTotalOriginal", BE.SSTotalOriginal)
            dc.Add("@SSTipoCambio", BE.SSTipoCambio)
            dc.Add("@SSTotal", BE.SSTotal)

            dc.Add("@CoTipoOC", BE.CoTipoOC)
            dc.Add("@CoCtaContab", BE.CoCtaContab)
            dc.Add("@CoObligacionPago", BE.CoObligacionPago)
            dc.Add("@CoMoneda_Pago", BE.CoMoneda_Pago)
            dc.Add("@CoMoneda_FEgreso", BE.CoMoneda_FEgreso)
            dc.Add("@SsTipoCambio_FEgreso", BE.SsTipoCambio_FEgreso)
            dc.Add("@SsTotal_FEgreso", BE.SsTotal_FEgreso)

            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@UserNuevo", BE.UserNuevo)
            dc.Add("@CoOrdPag", BE.CoOrdPag)
            dc.Add("@IDOperacion", BE.IDOperacion)
            dc.Add("@NuOrdComInt", BE.NuOrdComInt)

            dc.Add("@NuVouOfiExtInt", BE.NuVouOfiExtInt)
            dc.Add("@NuDocumVta", BE.NuDocumVta)
            dc.Add("@CoTipoDocVta", BE.CoTipoDocVta)
            dc.Add("@NuPreSob", BE.NuPreSob)
            dc.Add("@CoCecos", BE.CoCecos)
            dc.Add("@CoGasto", BE.CoGasto)
            dc.Add("@NuVouPTrenInt", BE.NuVouPTrenInt)

            If Not BE.NuFondoFijo = Nothing Then
                If BE.NuFondoFijo > 0 Then
                    dc.Add("@NuFondoFijo", BE.NuFondoFijo)
                    dc.Add("@CoTipoFondoFijo", BE.CoTipoFondoFijo)
                    'dc.Add("@SSPercepcion", BE.SSPercepcion)
                    'dc.Add("@TxConcepto", BE.TxConcepto)
                    'dc.Add("@CoProveedor", BE.CoProveedor)
                    'dc.Add("@IDCab", BE.IDCab_FF)
                End If
            End If

            dc.Add("@SSPercepcion", BE.SSPercepcion)
            dc.Add("@TxConcepto", BE.TxConcepto)
            dc.Add("@CoProveedor", BE.CoProveedor)
            dc.Add("@IDCab", BE.IDCab_FF)

            '--
            dc.Add("@FeVencimiento", BE.FeVencimiento)
            dc.Add("@CoTipoDocSAP", BE.CoTipoDocSAP)
            dc.Add("@CoFormaPago", BE.CoFormaPago)
            'dc.Add("@CoTipoDoc_Ref", BE.CoTipoDoc_Ref)
            'dc.Add("@NuSerie_Ref", BE.NuSerie_Ref)
            'dc.Add("@NuDocum_Ref", BE.NuDocum_Ref)
            'dc.Add("@FeEmision_Ref", BE.FeEmision_Ref)
            dc.Add("@CoCeCon", BE.CoCeCon)

            dc.Add("@FlMultiple", BE.FlMultiple)
            dc.Add("@NuDocum_Multiple", BE.NuDocum_Multiple)

            dc.Add("@pNuDocumProv", 0)



            'objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Ins", dc)

            intNuDocumProv = objADO.ExecuteSPOutput("DOCUMENTO_PROVEEDOR_Ins", dc)

            Dim objDetBT As New clsDocumentoProveedor_DetBT
            objDetBT.ActualizarIDsNuevosenHijosST2(BE, BE.NuDocumProv, intNuDocumProv)

            Dim objBT As New clsDocumentoProveedor_DetBT
            objBT.InsertarActualizarEliminar_INC(BE)

            ''------------SAP

            If vblnSAP Then
                Dim arrDeleteProperties As String() = Nothing


                Dim oBEPurchaseInvoice As clsPurchaseInvoicesBE = Nothing
                'PasarDatosDocumentosToSocioNegocio_Multiple(intNuDocumProv, BE.UserMod, oBEPurchaseInvoice, arrDeleteProperties)
                PasarDatosDocumentosToSocioNegocio_INC(intNuDocumProv, BE.UserMod, oBEPurchaseInvoice, arrDeleteProperties)


                Dim oBTJsonMaster As New clsJsonInterfaceBT()
                'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                '                                     "http://sap:8899/api/PurchaseInvoices/", _
                '                                     "POST", arrDeleteProperties)
                oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                                 "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                                 "POST", arrDeleteProperties)


                'Actualizar_CardCodeSAP_Multiple(intNuDocumProv, objResponseSAP.Id)
                Actualizar_CardCodeSAP(intNuDocumProv, objResponseSAP.Id)
            End If
            '------------FIN SAP

            BE.NuDocumProv = intNuDocumProv
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Sub EnviarSAP2(ByVal vintNuDocumProv As Integer, ByVal vstrUser As String)
        Try
            Dim arrDeleteProperties As String() = Nothing


            Dim oBEPurchaseInvoice As clsPurchaseInvoicesBE = Nothing
            PasarDatosDocumentosToSocioNegocio(vintNuDocumProv, vstrUser, oBEPurchaseInvoice, arrDeleteProperties)

            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
            '                                     "http://sap:8899/api/PurchaseInvoices/", _
            '                                     "POST", arrDeleteProperties)
            oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                             "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                             "POST", arrDeleteProperties)


            Actualizar_CardCodeSAP(vintNuDocumProv, objResponseSAP.Id)
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Function strInsertarDocumentoGenerado(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vstrUser As String = "") As String
        Try




            Enviar_SAP(BE.NuDocumProv, vstrUser)


            ContextUtil.SetComplete()

            Return ""
        Catch ex As Exception

            ContextUtil.SetAbort()

            Return ex.Message
        End Try
    End Function

    Public Function Enviar_SAP(ByVal vintNuDocumProv As Integer, Optional vstrUser As String = "") As String
        Try
            Dim arrDeleteProperties As String() = Nothing

            Dim oBEPurchaseInvoice As clsPurchaseInvoicesBE = Nothing
            PasarDatosDocumentosToSocioNegocio(vintNuDocumProv, vstrUser, oBEPurchaseInvoice, arrDeleteProperties)


            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
            '                                     "http://sap:8899/api/PurchaseInvoices/", _
            '                                     "POST", arrDeleteProperties)
            oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                               "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                               "POST", arrDeleteProperties)

            Actualizar_CardCodeSAP(vintNuDocumProv, objResponseSAP.Id)
            'ContextUtil.SetComplete()
            Return ""
        Catch ex As Exception
            ' ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Enviar_SAP")
            Return ex.Message
        End Try
    End Function

    Public Sub Actualizar_CardCodeSAP(ByVal vintNuDocumProv As Integer, ByVal strCardCodeSAP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocumProv", vintNuDocumProv)
                .Add("@CardCodeSAP", strCardCodeSAP)
            End With

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd_CardCodeSAP", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Actualizar_CardCodeSAP_Multiple(ByVal vintNuDocumProv As Integer, ByVal strCardCodeSAP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocumProv", vintNuDocumProv)
                .Add("@CardCodeSAP", strCardCodeSAP)
            End With

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd_CardCodeSAP", dc)
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd_CardCodeSAP_Multiple", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub PasarDatosDocumentosToSocioNegocio(ByVal vintNuDocumProv As Integer, ByVal vstrUserMod As String, _
                                                 ByRef oBEPurchaseInvoice As clsPurchaseInvoicesBE, ByRef arrDelProperty As String())
        Try
            Dim dtDatos As DataTable = drConsultarDatosSAP(vintNuDocumProv)
            Dim dtDatos_Lines As DataTable = drConsultarDatosSAP_Lines(vintNuDocumProv)
            Dim dtDatos_Expenses As DataTable = drConsultarDatosSAP_Expenses(vintNuDocumProv)
            Dim dtDatos_WithholdingTaxData As DataTable = drConsultarDatosSAP_WithholdingTaxData(vintNuDocumProv)
            Dim oBEPurchaseInvoices_Lines As clsPurchaseInvoices_LineBE
            Dim oBEPurchaseInvoices_Expenses As clsPurchaseInvoices_ExpensesBE
            Dim oBEPurchaseInvoices_WithholdingTaxData As clsPurchaseInvoices_WithholdingTaxDataBE
            Dim objLstLines As List(Of clsPurchaseInvoices_LineBE) = New List(Of clsPurchaseInvoices_LineBE)
            Dim objLstExpenses As List(Of clsPurchaseInvoices_ExpensesBE) = New List(Of clsPurchaseInvoices_ExpensesBE)
            Dim objLstWithholdingTaxData As List(Of clsPurchaseInvoices_WithholdingTaxDataBE) = New List(Of clsPurchaseInvoices_WithholdingTaxDataBE)
            Dim lstCampos_Borrar As List(Of String) = New List(Of String)
            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBEPurchaseInvoice = New clsPurchaseInvoicesBE()
                With oBEPurchaseInvoice

                    .TaxDate = dr("TaxDate")
                    .DocDate = dr("DocDate")
                    .DocDueDate = dr("DocDueDate")
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .DocType = dr("DocType")
                    .FederalTaxID = dr("FederalTaxID")
                    .DocCurrency = dr("DocCurrency")
                    .ControlAccount = dr("ControlAccount")
                    .CashAccount = dr("CashAccount")

                    If .CashAccount.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("CashAccount")
                    End If

                    .Comments = dr("Comments").ToString.Trim
                    .JournalMemo = dr("JournalMemo")
                    .PaymentGroupCode = dr("PaymentGroupCode")
                    .DiscountPercent = dr("DiscountPercent")

                    If .DiscountPercent = 0 Then
                        lstCampos_Borrar.Add("DiscountPercent")
                    End If

                    .DocTotal = dr("DocTotal")
                    .DocTotalFc = dr("DocTotalFc")
                    .U_SYP_MDTD = dr("U_SYP_MDTD")
                    .U_SYP_MDSD = dr("U_SYP_MDSD")
                    .U_SYP_MDCD = dr("U_SYP_MDCD")

                    .U_SYP_MDTO = dr("U_SYP_MDTO").ToString.Trim
                    If .U_SYP_MDTO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDTO")
                    End If

                    .U_SYP_MDSO = dr("U_SYP_MDSO").ToString.Trim
                    If .U_SYP_MDSO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDSO")
                    End If

                    .U_SYP_MDCO = dr("U_SYP_MDCO").ToString.Trim
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDCO")
                    End If

                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_FECHAREF = dr("U_SYP_FECHAREF")
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_FECHAREF")
                    End If

                    Dim intcapacidad As Integer = 0
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        intcapacidad = 0
                        ReDim arrDelProperty(intcapacidad)
                        'arrDelProperty.SetValue("U_SYP_MDTO", 0)
                        'arrDelProperty.SetValue("U_SYP_MDSO", 1)
                        'arrDelProperty.SetValue("U_SYP_MDCO", 2)
                        arrDelProperty.SetValue("U_SYP_FECHAREF", 0)
                    End If


                    .U_SYP_NUMOPER = dr("U_SYP_NUMOPER")
                    .U_SYP_TC = dr("U_SYP_TC")
                    .U_SYP_TCOMPRA = dr("U_SYP_TCOMPRA")
                    .U_SYP_NFILE = dr("U_SYP_NFILE").ToString.Trim
                    If .U_SYP_NFILE.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_NFILE")
                    End If

                    .U_SYP_CODERCC = dr("U_SYP_CODERCC").ToString.Trim
                    .U_SYP_CTAERCC = dr("U_SYP_CTAERCC").ToString.Trim

                    .U_SYP_TIPOBOLETO = dr("U_SYP_TIPOBOLETO").ToString.Trim
                    If .U_SYP_TIPOBOLETO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_TIPOBOLETO")
                    End If

                    .U_SYP_TPO_OP = dr("U_SYP_TPO_OP").ToString.Trim
                    If .U_SYP_TPO_OP.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_TPO_OP")
                    End If

                    'If .U_SYP_NFILE.Trim.Length = 0 Then
                    '    intcapacidad = intcapacidad + 1
                    '    ReDim arrDelProperty(intcapacidad)
                    '    arrDelProperty.SetValue("U_SYP_MDTO", intcapacidad)
                    'End If

                    If dtDatos_Lines.Rows.Count > 0 Then
                        For Each dr1 As DataRow In dtDatos_Lines.Rows
                            oBEPurchaseInvoices_Lines = New clsPurchaseInvoices_LineBE

                            oBEPurchaseInvoices_Lines.ItemCode = dr1("ItemCode")
                            oBEPurchaseInvoices_Lines.ItemDescription = dr1("ItemDescription")
                            oBEPurchaseInvoices_Lines.WarehouseCode = dr1("WarehouseCode")
                            oBEPurchaseInvoices_Lines.Quantity = dr1("Quantity")
                            oBEPurchaseInvoices_Lines.DiscountPercent = dr1("DiscountPercent")
                            oBEPurchaseInvoices_Lines.AccountCode = dr1("AccountCode")
                            oBEPurchaseInvoices_Lines.UnitPrice = dr1("UnitPrice")
                            oBEPurchaseInvoices_Lines.LineTotal = dr1("LineTotal")
                            oBEPurchaseInvoices_Lines.TaxCode = dr1("TaxCode")
                            oBEPurchaseInvoices_Lines.TaxTotal = dr1("TaxTotal")
                            oBEPurchaseInvoices_Lines.WTLiable = dr1("WTLiable")
                            oBEPurchaseInvoices_Lines.TaxOnly = dr1("TaxOnly")
                            'oBEPurchaseInvoices_Lines.ProjectCode = dr1("ProjectCode")
                            oBEPurchaseInvoices_Lines.CostingCode = dr1("CostingCode").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode2 = dr1("CostingCode2").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode3 = dr1("CostingCode3").ToString.Trim
                            oBEPurchaseInvoices_Lines.U_SYP_CONCEPTO = dr1("U_SYP_CONCEPTO").ToString.Trim
                            objLstLines.Add(oBEPurchaseInvoices_Lines)
                        Next
                        oBEPurchaseInvoice.Lines = objLstLines
                    End If

                    If dtDatos_Expenses.Rows.Count > 0 Then
                        For Each dr1 As DataRow In dtDatos_Expenses.Rows
                            oBEPurchaseInvoices_Expenses = New clsPurchaseInvoices_ExpensesBE
                            oBEPurchaseInvoices_Expenses.ExpenseCode = dr1("ExpenseCode")
                            oBEPurchaseInvoices_Expenses.LineTotal = dr1("LineTotal")
                            oBEPurchaseInvoices_Expenses.Remarks = dr1("Remarks")
                            oBEPurchaseInvoices_Expenses.DistributionMethod = dr1("DistributionMethod")
                            oBEPurchaseInvoices_Expenses.TaxCode = dr1("TaxCode")
                            oBEPurchaseInvoices_Expenses.DistributionRule = dr1("DistributionRule")
                            oBEPurchaseInvoices_Expenses.DistributionRule2 = dr1("DistributionRule2")
                            oBEPurchaseInvoices_Expenses.DistributionRule3 = dr1("DistributionRule3").ToString.Trim
                            objLstExpenses.Add(oBEPurchaseInvoices_Expenses)
                        Next
                        oBEPurchaseInvoice.Expenses = objLstExpenses
                    End If

                    If dtDatos_WithholdingTaxData.Rows.Count > 0 Then

                        For Each dr1 As DataRow In dtDatos_WithholdingTaxData.Rows
                            oBEPurchaseInvoices_WithholdingTaxData = New clsPurchaseInvoices_WithholdingTaxDataBE
                            oBEPurchaseInvoices_WithholdingTaxData.WTCode = dr1("WTCode")
                            oBEPurchaseInvoices_WithholdingTaxData.WTAmount = dr1("WTAmount")
                            oBEPurchaseInvoices_WithholdingTaxData.WTAmountFC = dr1("WTAmountFC")

                            objLstWithholdingTaxData.Add(oBEPurchaseInvoices_WithholdingTaxData)
                        Next
                        oBEPurchaseInvoice.WithholdingTaxData = objLstWithholdingTaxData


                    End If

                    If objLstLines.Count = 0 Then
                        lstCampos_Borrar.Add("Lines")
                    End If
                    If objLstExpenses.Count = 0 Then
                        lstCampos_Borrar.Add("Expenses")
                    End If

                    If objLstWithholdingTaxData.Count = 0 Then
                        lstCampos_Borrar.Add("WithholdingTaxData")
                    End If

                    intcapacidad = lstCampos_Borrar.Count - 1
                    ReDim arrDelProperty(intcapacidad)

                    For i As Integer = 0 To intcapacidad
                        arrDelProperty.SetValue(lstCampos_Borrar(i), i)
                    Next

                    ' ''prueba
                    ''intcapacidad = 9
                    ''ReDim arrDelProperty(intcapacidad)
                    ' ''arrDelProperty.SetValue("U_SYP_MDTO", 0)
                    ' ''arrDelProperty.SetValue("U_SYP_MDSO", 1)
                    ' ''arrDelProperty.SetValue("U_SYP_MDCO", 2)


                    ''arrDelProperty.SetValue("CashAccount", 0)
                    ''arrDelProperty.SetValue("DiscountPercent", 1)
                    ' '' arrDelProperty.SetValue("DocTotalFc", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDTO", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDSO", 3)
                    ''arrDelProperty.SetValue("U_SYP_MDCO", 4)
                    ''arrDelProperty.SetValue("U_SYP_FECHAREF", 5)
                    ''arrDelProperty.SetValue("U_SYP_NFILE", 6)
                    ''arrDelProperty.SetValue("Lines", 7)
                    ''arrDelProperty.SetValue("Expenses", 8)
                    ''arrDelProperty.SetValue("WithholdingTaxData", 9)
                End With
            End If

            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub PasarDatosDocumentosToSocioNegocio_INC(ByVal vintNuDocumProv As Integer, ByVal vstrUserMod As String, _
                                                ByRef oBEPurchaseInvoice As clsPurchaseInvoicesBE, ByRef arrDelProperty As String())
        Try
            Dim dtDatos As DataTable = drConsultarDatosSAP_INC(vintNuDocumProv)
            Dim dtDatos_Lines As DataTable = drConsultarDatosSAP_Lines_INC(vintNuDocumProv)
            Dim dtDatos_Expenses As DataTable = drConsultarDatosSAP_Expenses(vintNuDocumProv)
            Dim dtDatos_WithholdingTaxData As DataTable = drConsultarDatosSAP_WithholdingTaxData(vintNuDocumProv)
            Dim oBEPurchaseInvoices_Lines As clsPurchaseInvoices_LineBE
            Dim oBEPurchaseInvoices_Expenses As clsPurchaseInvoices_ExpensesBE
            Dim oBEPurchaseInvoices_WithholdingTaxData As clsPurchaseInvoices_WithholdingTaxDataBE
            Dim objLstLines As List(Of clsPurchaseInvoices_LineBE) = New List(Of clsPurchaseInvoices_LineBE)
            Dim objLstExpenses As List(Of clsPurchaseInvoices_ExpensesBE) = New List(Of clsPurchaseInvoices_ExpensesBE)
            Dim objLstWithholdingTaxData As List(Of clsPurchaseInvoices_WithholdingTaxDataBE) = New List(Of clsPurchaseInvoices_WithholdingTaxDataBE)
            Dim lstCampos_Borrar As List(Of String) = New List(Of String)
            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBEPurchaseInvoice = New clsPurchaseInvoicesBE()
                With oBEPurchaseInvoice

                    .TaxDate = dr("TaxDate")
                    .DocDate = dr("DocDate")
                    .DocDueDate = dr("DocDueDate")
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .DocType = dr("DocType")
                    .FederalTaxID = dr("FederalTaxID")
                    .DocCurrency = dr("DocCurrency")
                    .ControlAccount = dr("ControlAccount")
                    .CashAccount = dr("CashAccount")

                    If .CashAccount.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("CashAccount")
                    End If

                    .Comments = dr("Comments").ToString.Trim
                    .JournalMemo = dr("JournalMemo")
                    .PaymentGroupCode = dr("PaymentGroupCode")
                    .DiscountPercent = dr("DiscountPercent")

                    If .DiscountPercent = 0 Then
                        lstCampos_Borrar.Add("DiscountPercent")
                    End If

                    .DocTotal = dr("DocTotal")
                    .DocTotalFc = dr("DocTotalFc")
                    .U_SYP_MDTD = dr("U_SYP_MDTD")
                    .U_SYP_MDSD = dr("U_SYP_MDSD")
                    .U_SYP_MDCD = dr("U_SYP_MDCD")

                    .U_SYP_MDTO = dr("U_SYP_MDTO").ToString.Trim
                    If .U_SYP_MDTO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDTO")
                    End If

                    .U_SYP_MDSO = dr("U_SYP_MDSO").ToString.Trim
                    If .U_SYP_MDSO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDSO")
                    End If

                    .U_SYP_MDCO = dr("U_SYP_MDCO").ToString.Trim
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDCO")
                    End If

                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_FECHAREF = dr("U_SYP_FECHAREF")
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_FECHAREF")
                    End If

                    Dim intcapacidad As Integer = 0
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        intcapacidad = 0
                        ReDim arrDelProperty(intcapacidad)
                        'arrDelProperty.SetValue("U_SYP_MDTO", 0)
                        'arrDelProperty.SetValue("U_SYP_MDSO", 1)
                        'arrDelProperty.SetValue("U_SYP_MDCO", 2)
                        arrDelProperty.SetValue("U_SYP_FECHAREF", 0)
                    End If


                    .U_SYP_NUMOPER = dr("U_SYP_NUMOPER")
                    .U_SYP_TC = dr("U_SYP_TC")
                    .U_SYP_TCOMPRA = dr("U_SYP_TCOMPRA")
                    .U_SYP_NFILE = dr("U_SYP_NFILE").ToString.Trim
                    If .U_SYP_NFILE.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_NFILE")
                    End If

                    .U_SYP_CODERCC = dr("U_SYP_CODERCC").ToString.Trim
                    .U_SYP_CTAERCC = dr("U_SYP_CTAERCC").ToString.Trim

                    .U_SYP_TIPOBOLETO = dr("U_SYP_TIPOBOLETO").ToString.Trim
                    If .U_SYP_TIPOBOLETO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_TIPOBOLETO")
                    End If

                    .U_SYP_TPO_OP = dr("U_SYP_TPO_OP").ToString.Trim
                    If .U_SYP_TPO_OP.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_TPO_OP")
                    End If

                    'If .U_SYP_NFILE.Trim.Length = 0 Then
                    '    intcapacidad = intcapacidad + 1
                    '    ReDim arrDelProperty(intcapacidad)
                    '    arrDelProperty.SetValue("U_SYP_MDTO", intcapacidad)
                    'End If

                    If dtDatos_Lines.Rows.Count > 0 Then
                        For Each dr1 As DataRow In dtDatos_Lines.Rows
                            oBEPurchaseInvoices_Lines = New clsPurchaseInvoices_LineBE

                            oBEPurchaseInvoices_Lines.ItemCode = dr1("ItemCode")
                            oBEPurchaseInvoices_Lines.ItemDescription = dr1("ItemDescription")
                            oBEPurchaseInvoices_Lines.WarehouseCode = dr1("WarehouseCode")
                            oBEPurchaseInvoices_Lines.Quantity = dr1("Quantity")
                            oBEPurchaseInvoices_Lines.DiscountPercent = dr1("DiscountPercent")
                            oBEPurchaseInvoices_Lines.AccountCode = dr1("AccountCode")
                            oBEPurchaseInvoices_Lines.UnitPrice = dr1("UnitPrice")
                            oBEPurchaseInvoices_Lines.LineTotal = dr1("LineTotal")
                            oBEPurchaseInvoices_Lines.TaxCode = dr1("TaxCode")
                            oBEPurchaseInvoices_Lines.TaxTotal = dr1("TaxTotal")
                            oBEPurchaseInvoices_Lines.WTLiable = dr1("WTLiable")
                            oBEPurchaseInvoices_Lines.TaxOnly = dr1("TaxOnly")
                            'oBEPurchaseInvoices_Lines.ProjectCode = dr1("ProjectCode")
                            oBEPurchaseInvoices_Lines.CostingCode = dr1("CostingCode").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode2 = dr1("CostingCode2").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode3 = dr1("CostingCode3").ToString.Trim
                            oBEPurchaseInvoices_Lines.U_SYP_CONCEPTO = dr1("U_SYP_CONCEPTO").ToString.Trim
                            objLstLines.Add(oBEPurchaseInvoices_Lines)
                        Next
                        oBEPurchaseInvoice.Lines = objLstLines
                    End If

                    If dtDatos_Expenses.Rows.Count > 0 Then
                        For Each dr1 As DataRow In dtDatos_Expenses.Rows
                            oBEPurchaseInvoices_Expenses = New clsPurchaseInvoices_ExpensesBE
                            oBEPurchaseInvoices_Expenses.ExpenseCode = dr1("ExpenseCode")
                            oBEPurchaseInvoices_Expenses.LineTotal = dr1("LineTotal")
                            oBEPurchaseInvoices_Expenses.Remarks = dr1("Remarks")
                            oBEPurchaseInvoices_Expenses.DistributionMethod = dr1("DistributionMethod")
                            oBEPurchaseInvoices_Expenses.TaxCode = dr1("TaxCode")
                            oBEPurchaseInvoices_Expenses.DistributionRule = dr1("DistributionRule")
                            oBEPurchaseInvoices_Expenses.DistributionRule2 = dr1("DistributionRule2")
                            oBEPurchaseInvoices_Expenses.DistributionRule3 = dr1("DistributionRule3").ToString.Trim
                            objLstExpenses.Add(oBEPurchaseInvoices_Expenses)
                        Next
                        oBEPurchaseInvoice.Expenses = objLstExpenses
                    End If

                    If dtDatos_WithholdingTaxData.Rows.Count > 0 Then

                        For Each dr1 As DataRow In dtDatos_WithholdingTaxData.Rows
                            oBEPurchaseInvoices_WithholdingTaxData = New clsPurchaseInvoices_WithholdingTaxDataBE
                            oBEPurchaseInvoices_WithholdingTaxData.WTCode = dr1("WTCode")
                            oBEPurchaseInvoices_WithholdingTaxData.WTAmount = dr1("WTAmount")
                            oBEPurchaseInvoices_WithholdingTaxData.WTAmountFC = dr1("WTAmountFC")

                            objLstWithholdingTaxData.Add(oBEPurchaseInvoices_WithholdingTaxData)
                        Next
                        oBEPurchaseInvoice.WithholdingTaxData = objLstWithholdingTaxData


                    End If

                    If objLstLines.Count = 0 Then
                        lstCampos_Borrar.Add("Lines")
                    End If
                    If objLstExpenses.Count = 0 Then
                        lstCampos_Borrar.Add("Expenses")
                    End If

                    If objLstWithholdingTaxData.Count = 0 Then
                        lstCampos_Borrar.Add("WithholdingTaxData")
                    End If

                    intcapacidad = lstCampos_Borrar.Count - 1
                    ReDim arrDelProperty(intcapacidad)

                    For i As Integer = 0 To intcapacidad
                        arrDelProperty.SetValue(lstCampos_Borrar(i), i)
                    Next

                    ' ''prueba
                    ''intcapacidad = 9
                    ''ReDim arrDelProperty(intcapacidad)
                    ' ''arrDelProperty.SetValue("U_SYP_MDTO", 0)
                    ' ''arrDelProperty.SetValue("U_SYP_MDSO", 1)
                    ' ''arrDelProperty.SetValue("U_SYP_MDCO", 2)


                    ''arrDelProperty.SetValue("CashAccount", 0)
                    ''arrDelProperty.SetValue("DiscountPercent", 1)
                    ' '' arrDelProperty.SetValue("DocTotalFc", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDTO", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDSO", 3)
                    ''arrDelProperty.SetValue("U_SYP_MDCO", 4)
                    ''arrDelProperty.SetValue("U_SYP_FECHAREF", 5)
                    ''arrDelProperty.SetValue("U_SYP_NFILE", 6)
                    ''arrDelProperty.SetValue("Lines", 7)
                    ''arrDelProperty.SetValue("Expenses", 8)
                    ''arrDelProperty.SetValue("WithholdingTaxData", 9)
                End With
            End If

            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub PasarDatosDocumentosToSocioNegocio_Multiple(ByVal vintNuDocumProv As Integer, ByVal vstrUserMod As String, _
                                                ByRef oBEPurchaseInvoice As clsPurchaseInvoicesBE, ByRef arrDelProperty As String())
        Try
            Dim dtDatos As DataTable = drConsultarDatosSAP(vintNuDocumProv)
            Dim dtDatos_Lines As DataTable = drConsultarDatosSAP_Lines_Multiple(vintNuDocumProv)
            Dim dtDatos_Expenses As DataTable = drConsultarDatosSAP_Expenses(vintNuDocumProv)
            Dim dtDatos_WithholdingTaxData As DataTable = drConsultarDatosSAP_WithholdingTaxData(vintNuDocumProv)
            Dim oBEPurchaseInvoices_Lines As clsPurchaseInvoices_LineBE
            Dim oBEPurchaseInvoices_Expenses As clsPurchaseInvoices_ExpensesBE
            Dim oBEPurchaseInvoices_WithholdingTaxData As clsPurchaseInvoices_WithholdingTaxDataBE
            Dim objLstLines As List(Of clsPurchaseInvoices_LineBE) = New List(Of clsPurchaseInvoices_LineBE)
            Dim objLstExpenses As List(Of clsPurchaseInvoices_ExpensesBE) = New List(Of clsPurchaseInvoices_ExpensesBE)
            Dim objLstWithholdingTaxData As List(Of clsPurchaseInvoices_WithholdingTaxDataBE) = New List(Of clsPurchaseInvoices_WithholdingTaxDataBE)
            Dim lstCampos_Borrar As List(Of String) = New List(Of String)
            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBEPurchaseInvoice = New clsPurchaseInvoicesBE()
                With oBEPurchaseInvoice

                    .TaxDate = dr("TaxDate")
                    .DocDate = dr("DocDate")
                    .DocDueDate = dr("DocDueDate")
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .DocType = dr("DocType")
                    .FederalTaxID = dr("FederalTaxID")
                    .DocCurrency = dr("DocCurrency")
                    .ControlAccount = dr("ControlAccount")
                    .CashAccount = dr("CashAccount")

                    If .CashAccount.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("CashAccount")
                    End If

                    .Comments = dr("Comments")
                    .JournalMemo = dr("JournalMemo")
                    .PaymentGroupCode = dr("PaymentGroupCode")
                    .DiscountPercent = dr("DiscountPercent")

                    If .DiscountPercent = 0 Then
                        lstCampos_Borrar.Add("DiscountPercent")
                    End If

                    .DocTotal = dr("DocTotal")
                    .DocTotalFc = dr("DocTotalFc")
                    .U_SYP_MDTD = dr("U_SYP_MDTD")
                    .U_SYP_MDSD = dr("U_SYP_MDSD")
                    .U_SYP_MDCD = dr("U_SYP_MDCD")

                    .U_SYP_MDTO = dr("U_SYP_MDTO").ToString.Trim
                    If .U_SYP_MDTO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDTO")
                    End If

                    .U_SYP_MDSO = dr("U_SYP_MDSO").ToString.Trim
                    If .U_SYP_MDSO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDSO")
                    End If

                    .U_SYP_MDCO = dr("U_SYP_MDCO").ToString.Trim
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_MDCO")
                    End If

                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_FECHAREF = dr("U_SYP_FECHAREF")
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_FECHAREF")
                    End If

                    Dim intcapacidad As Integer = 0
                    If .U_SYP_MDCO.Trim.Length = 0 Then
                        intcapacidad = 0
                        ReDim arrDelProperty(intcapacidad)
                        'arrDelProperty.SetValue("U_SYP_MDTO", 0)
                        'arrDelProperty.SetValue("U_SYP_MDSO", 1)
                        'arrDelProperty.SetValue("U_SYP_MDCO", 2)
                        arrDelProperty.SetValue("U_SYP_FECHAREF", 0)
                    End If


                    .U_SYP_NUMOPER = dr("U_SYP_NUMOPER")
                    .U_SYP_TC = dr("U_SYP_TC")
                    .U_SYP_TCOMPRA = dr("U_SYP_TCOMPRA")
                    .U_SYP_NFILE = dr("U_SYP_NFILE").ToString.Trim
                    If .U_SYP_NFILE.Trim.Length = 0 Then
                        lstCampos_Borrar.Add("U_SYP_NFILE")
                    End If

                    .U_SYP_CODERCC = dr("U_SYP_CODERCC").ToString.Trim
                    .U_SYP_CTAERCC = dr("U_SYP_CTAERCC").ToString.Trim

                    'If .U_SYP_NFILE.Trim.Length = 0 Then
                    '    intcapacidad = intcapacidad + 1
                    '    ReDim arrDelProperty(intcapacidad)
                    '    arrDelProperty.SetValue("U_SYP_MDTO", intcapacidad)
                    'End If

                    If dtDatos_Lines.Rows.Count > 0 Then
                        For Each dr1 As DataRow In dtDatos_Lines.Rows
                            oBEPurchaseInvoices_Lines = New clsPurchaseInvoices_LineBE

                            oBEPurchaseInvoices_Lines.ItemCode = dr1("ItemCode")
                            oBEPurchaseInvoices_Lines.ItemDescription = dr1("ItemDescription")
                            oBEPurchaseInvoices_Lines.WarehouseCode = dr1("WarehouseCode")
                            oBEPurchaseInvoices_Lines.Quantity = dr1("Quantity")
                            oBEPurchaseInvoices_Lines.DiscountPercent = dr1("DiscountPercent")
                            oBEPurchaseInvoices_Lines.AccountCode = dr1("AccountCode")
                            oBEPurchaseInvoices_Lines.UnitPrice = dr1("UnitPrice")
                            oBEPurchaseInvoices_Lines.LineTotal = dr1("LineTotal")
                            oBEPurchaseInvoices_Lines.TaxCode = dr1("TaxCode")
                            oBEPurchaseInvoices_Lines.TaxTotal = dr1("TaxTotal")
                            oBEPurchaseInvoices_Lines.WTLiable = dr1("WTLiable")
                            oBEPurchaseInvoices_Lines.TaxOnly = dr1("TaxOnly")
                            'oBEPurchaseInvoices_Lines.ProjectCode = dr1("ProjectCode")
                            oBEPurchaseInvoices_Lines.CostingCode = dr1("CostingCode").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode2 = dr1("CostingCode2").ToString.Trim
                            oBEPurchaseInvoices_Lines.CostingCode3 = dr1("CostingCode3").ToString.Trim
                            oBEPurchaseInvoices_Lines.U_SYP_CONCEPTO = dr1("U_SYP_CONCEPTO").ToString.Trim
                            objLstLines.Add(oBEPurchaseInvoices_Lines)
                        Next
                        oBEPurchaseInvoice.Lines = objLstLines
                    End If

                    If dtDatos_Expenses.Rows.Count > 0 Then
                        For Each dr1 As DataRow In dtDatos_Expenses.Rows
                            oBEPurchaseInvoices_Expenses = New clsPurchaseInvoices_ExpensesBE
                            oBEPurchaseInvoices_Expenses.ExpenseCode = dr1("ExpenseCode")
                            oBEPurchaseInvoices_Expenses.LineTotal = dr1("LineTotal")
                            oBEPurchaseInvoices_Expenses.Remarks = dr1("Remarks")
                            oBEPurchaseInvoices_Expenses.DistributionMethod = dr1("DistributionMethod")
                            oBEPurchaseInvoices_Expenses.TaxCode = dr1("TaxCode")
                            oBEPurchaseInvoices_Expenses.DistributionRule = dr1("DistributionRule")
                            oBEPurchaseInvoices_Expenses.DistributionRule2 = dr1("DistributionRule2")
                            oBEPurchaseInvoices_Expenses.DistributionRule3 = dr1("DistributionRule3").ToString.Trim
                            objLstExpenses.Add(oBEPurchaseInvoices_Expenses)
                        Next
                        oBEPurchaseInvoice.Expenses = objLstExpenses
                    End If

                    If dtDatos_WithholdingTaxData.Rows.Count > 0 Then

                        For Each dr1 As DataRow In dtDatos_WithholdingTaxData.Rows
                            oBEPurchaseInvoices_WithholdingTaxData = New clsPurchaseInvoices_WithholdingTaxDataBE
                            oBEPurchaseInvoices_WithholdingTaxData.WTCode = dr1("WTCode")
                            oBEPurchaseInvoices_WithholdingTaxData.WTAmount = dr1("WTAmount")
                            oBEPurchaseInvoices_WithholdingTaxData.WTAmountFC = dr1("WTAmountFC")

                            objLstWithholdingTaxData.Add(oBEPurchaseInvoices_WithholdingTaxData)
                        Next
                        oBEPurchaseInvoice.WithholdingTaxData = objLstWithholdingTaxData


                    End If

                    If objLstLines.Count = 0 Then
                        lstCampos_Borrar.Add("Lines")
                    End If
                    If objLstExpenses.Count = 0 Then
                        lstCampos_Borrar.Add("Expenses")
                    End If

                    If objLstWithholdingTaxData.Count = 0 Then
                        lstCampos_Borrar.Add("WithholdingTaxData")
                    End If

                    intcapacidad = lstCampos_Borrar.Count - 1
                    ReDim arrDelProperty(intcapacidad)

                    For i As Integer = 0 To intcapacidad
                        arrDelProperty.SetValue(lstCampos_Borrar(i), i)
                    Next

                    ' ''prueba
                    ''intcapacidad = 9
                    ''ReDim arrDelProperty(intcapacidad)
                    ' ''arrDelProperty.SetValue("U_SYP_MDTO", 0)
                    ' ''arrDelProperty.SetValue("U_SYP_MDSO", 1)
                    ' ''arrDelProperty.SetValue("U_SYP_MDCO", 2)


                    ''arrDelProperty.SetValue("CashAccount", 0)
                    ''arrDelProperty.SetValue("DiscountPercent", 1)
                    ' '' arrDelProperty.SetValue("DocTotalFc", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDTO", 2)
                    ''arrDelProperty.SetValue("U_SYP_MDSO", 3)
                    ''arrDelProperty.SetValue("U_SYP_MDCO", 4)
                    ''arrDelProperty.SetValue("U_SYP_FECHAREF", 5)
                    ''arrDelProperty.SetValue("U_SYP_NFILE", 6)
                    ''arrDelProperty.SetValue("Lines", 7)
                    ''arrDelProperty.SetValue("Expenses", 8)
                    ''arrDelProperty.SetValue("WithholdingTaxData", 9)
                End With
            End If

            'ContextUtil.SetComplete()
        Catch ex As Exception
            'ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub PasarDatosDocumentosToSocioNegocio_PUT(ByVal vintNuDocumProv As Integer, ByVal vstrUserMod As String, _
                                             ByRef oBEPurchaseInvoice As clsPurchaseInvoices_CancellationBE, ByRef arrDelProperty As String())
        Try
            Dim dtDatos As DataTable = drConsultarDatosSAP_PUT(vintNuDocumProv)

            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBEPurchaseInvoice = New clsPurchaseInvoices_CancellationBE()
                With oBEPurchaseInvoice

                    .CardCode = dr("CardCode").ToString.Trim
                    .U_SYP_MDTD = dr("U_SYP_MDTD").ToString.Trim
                    .U_SYP_MDSD = dr("U_SYP_MDSD").ToString.Trim
                    .U_SYP_MDCD = dr("U_SYP_MDCD").ToString.Trim
                    .U_SYP_STATUS = dr("U_SYP_STATUS")
                    .U_SYP_TCOMPRA = dr("U_SYP_TCOMPRA")

                End With
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function drConsultarDatos_Pk(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("DOCUMENTO_PROVEEDOR_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_INC(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP_Inc", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_Lines(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP_Lines", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_Lines_Multiple(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP_Lines_Multiple", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_Lines_INC(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP_Lines_INC", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_Expenses(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP_Expenses", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_WithholdingTaxData(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP_WithholdingTaxData", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function drConsultarDatosSAP_PUT(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@nudocumprov", vintNuDocumProv)

            Return objADO.GetDataTable("Documento_Proveedor_List_SAP_PUT", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'Public Overloads Sub Actualizar(ByVal BE As clsDocumentoProveedorBE)
    Public Overloads Sub Actualizar(ByVal BE As clsDocumentoProveedorBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuDocumProv", BE.NuDocumProv)
            dc.Add("@NuVoucher", BE.NuVoucher)
            dc.Add("@NuOrden_Servicio", BE.NuOrden_Servicio)
            dc.Add("@NuSerie", BE.NuSerie)
            dc.Add("@NuDocum", BE.NuDocum)
            dc.Add("@CoTipoDoc", BE.CoTipoDoc)

            dc.Add("@FeEmision", BE.FeEmision)
            dc.Add("@FeRecepcion", BE.FeRecepcion)
            dc.Add("@CoTipoDetraccion", BE.CoTipoDetraccion)
            dc.Add("@CoMoneda", BE.CoMoneda)
            dc.Add("@SsIGV", BE.SsIGV)
            dc.Add("@SSTotalOriginal", BE.SSTotalOriginal)
            dc.Add("@SsNeto", BE.SsNeto)
            dc.Add("@SsOtrCargos", BE.SsOtrCargos)
            dc.Add("@SsDetraccion", BE.SsDetraccion)

            dc.Add("@SSTipoCambio", BE.SSTipoCambio)
            dc.Add("@SSTotal", BE.SSTotal)
            'dc.Add("@CoEstado", BE.CoEstado)
            dc.Add("@CoTipoOC", BE.CoTipoOC)
            dc.Add("@CoCtaContab", BE.CoCtaContab)
            dc.Add("@CoMoneda_Pago", BE.CoMoneda_Pago)
            dc.Add("@SsTipoCambio_FEgreso", BE.SsTipoCambio_FEgreso)
            dc.Add("@SsTotal_FEgreso", BE.SsTotal_FEgreso)
            dc.Add("@CoCecos", BE.CoCecos)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@CoOrdPag", BE.CoOrdPag)
            'dc.Add("@CoProveedor", BE.CoProveedor)
            '--
            dc.Add("@CoTipoFondoFijo", BE.CoTipoFondoFijo)
            dc.Add("@SSPercepcion", BE.SSPercepcion)
            dc.Add("@TxConcepto", BE.TxConcepto)
            dc.Add("@CoProveedor", BE.CoProveedor)
            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@FeVencimiento", BE.FeVencimiento)
            dc.Add("@CoTipoDocSAP", BE.CoTipoDocSAP)
            dc.Add("@CoFormaPago", BE.CoFormaPago)
            dc.Add("@CoTipoDoc_Ref", BE.CoTipoDoc_Ref)
            dc.Add("@NuSerie_Ref", BE.NuSerie_Ref)
            dc.Add("@NuDocum_Ref", BE.NuDocum_Ref)
            dc.Add("@FeEmision_Ref", BE.FeEmision_Ref)
            dc.Add("@CoCeCon", BE.CoCeCon)
            dc.Add("@CoGasto", BE.CoGasto)


            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd", dc)

            'Dim objItem As New clsDocumentoProveedor_DetBT
            'objItem.InsertarActualizarEliminar(BE)

            'Dim objDetBT As New clsDocumentoProveedor_DetBT
            'objDetBT.ActualizarIDsNuevosenHijosST(BE, BE.NuDocumProv, BE.NuDocumProv)

            Dim objBT As New clsDocumentoProveedor_DetBT
            objBT.InsertarActualizarEliminar(BE)

            ActualizarMontos(BE.NuDocumProv)

            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub Anular(ByVal BE As clsDocumentoProveedorBE, Optional ByVal vstrCoEstado As String = "")
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", BE.NuDocumProv)
            dc.Add("@FlActivo", BE.FlActivo)
            dc.Add("@UserMod", BE.UserMod)
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd_Estado", dc)

            dc = New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", BE.NuDocumProv)
            dc.Add("@CoEstado", vstrCoEstado)
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc", dc)
            ActualizarMontos(BE.NuDocumProv, "PD")

            ''SAP
            'Dim arrDeleteProperties As String() = Nothing

            'Dim oBEPurchaseInvoice As clsPurchaseInvoices_CancellationBE = Nothing
            'PasarDatosDocumentosToSocioNegocio_PUT(BE.NuDocumProv, BE.UserMod, oBEPurchaseInvoice, arrDeleteProperties)

            'primero verificar que el documento tenga codigo sap
            Dim dtt As DataTable = drConsultarDatos_Pk(BE.NuDocumProv)

            If dtt.Rows.Count > 0 Then

                Dim blnMultiple As Boolean = False
                Dim strNuDocum_Multiple As String = ""

                blnMultiple = IIf(IsDBNull(dtt.Rows(0).Item("FlMultiple")), False, dtt.Rows(0).Item("FlMultiple"))
                strNuDocum_Multiple = IIf(IsDBNull(dtt.Rows(0).Item("NuDocum_Multiple")), "", dtt.Rows(0).Item("NuDocum_Multiple"))

                Dim strCoSAP As String = ""
                strCoSAP = IIf(IsDBNull(dtt.Rows(0).Item("NuDocInterno")), "", dtt.Rows(0).Item("NuDocInterno"))

                If blnMultiple = True Then


                    If strNuDocum_Multiple <> "" Then
                        'detalle
                        dc = New Dictionary(Of String, String)
                        dc.Add("@NuDocumProv", strNuDocum_Multiple)
                        dc.Add("@UserMod", BE.UserMod)
                        objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc_Upd_Multiple", dc) 'actualiza saldos de vouchers

                        dc.Add("@FlActivo", BE.FlActivo)
                        objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd_Estado_Multiple", dc) ' cambia a inactivo los documentos



                        ' ActualizarMontos(BE.NuDocumProv, "PD")
                    Else
                        'cabecera
                        dc = New Dictionary(Of String, String)
                        dc.Add("@NuDocumProv", BE.NuDocumProv)
                        dc.Add("@UserMod", BE.UserMod)
                        objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc_Upd_Multiple", dc) 'actualiza saldos de vouchers

                        dc.Add("@FlActivo", BE.FlActivo)
                        objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd_Estado_Multiple", dc) ' cambia a inactivo los documentos
                    End If

                End If

                If strCoSAP <> "" Then

                    'SAP
                    Dim arrDeleteProperties As String() = Nothing

                    Dim oBEPurchaseInvoice As clsPurchaseInvoices_CancellationBE = Nothing
                    PasarDatosDocumentosToSocioNegocio_PUT(BE.NuDocumProv, BE.UserMod, oBEPurchaseInvoice, arrDeleteProperties)

                    Dim oBTJsonMaster As New clsJsonInterfaceBT()
                    'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                    '                                     "http://sap:8899/api/PurchaseInvoices/", _
                    '                                     "PUT", arrDeleteProperties)
                    oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                                         "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                                         "PUT", arrDeleteProperties)
                End If

            End If


            'Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
            '                                     "http://sap:8899/api/PurchaseInvoices/", _
            '                                     "PUT", arrDeleteProperties)


            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub Anular_Pres(ByVal BE As clsDocumentoProveedorBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", BE.NuDocumProv)
            dc.Add("@FlActivo", BE.FlActivo)
            dc.Add("@UserMod", BE.UserMod)
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Upd_Estado", dc)

            dc = New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", BE.NuDocumProv)
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc_Pres", dc)
            ' ''ActualizarMontos(BE.NuDocumProv)

            If BE.NuDocInterno <> "" Then
                Dim arrDeleteProperties As String() = Nothing

                Dim oBEPurchaseInvoice As clsPurchaseInvoices_CancellationBE = Nothing
                PasarDatosDocumentosToSocioNegocio_PUT(BE.NuDocumProv, BE.UserMod, oBEPurchaseInvoice, arrDeleteProperties)


                Dim oBTJsonMaster As New clsJsonInterfaceBT()
                'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                '                                     "http://sap:8899/api/PurchaseInvoices/", _
                '                                     "PUT", arrDeleteProperties)
                oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                                   "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                                   "PUT", arrDeleteProperties)
            End If


            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Private Overloads Sub ActualizarMontos(ByVal vintNuDocumProv As Integer, Optional ByVal vstrCoEstado As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDocumProv", vintNuDocumProv)
            dc.Add("@CoEstado", vstrCoEstado)

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_UpdSsPendientexDoc_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarMontos")
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintNuDocumProv As Integer)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", vintNuDocumProv)



            ''detalle

            'Dim objItem As New clsDocumentoProveedor_DetBT

            'objItem.EliminarxDoc(vintNuDocumProv)

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub



End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDocumentoProveedor_DetBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT

    Private strNombreClase As String = "clsDocumentoProveedor_DetBT"


    Public Sub Insertar(ByVal BE As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc


                .Add("@NuDocumProv", BE.NuDocumProv)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@TxServicio", BE.TxServicio)
                .Add("@SsMonto", BE.SsMonto)
                .Add("@QtCantidad", BE.QtCantidad)
                .Add("@UserMod", BE.UserMod)

                .Add("@IDCab", BE.IDCab)
                .Add("@CoProducto", BE.CoProducto)
                .Add("@CoAlmacen", BE.CoAlmacen)
                .Add("@SsIGV", BE.SsIGV)
                .Add("@CoTipoOC", BE.CoTipoOC)
                .Add("@CoCtaContab", BE.CoCtaContab)

                .Add("@CoCeCos", BE.CoCeCos)
                .Add("@SSPrecUni", BE.SSPrecUni)
                .Add("@SSTotal", BE.SsTotal)

            End With
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_DET_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Insertar_INC(ByVal BE As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE, BE_INC As clsTablasApoyoBE.clsENTRADASINC_SAPBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc


                .Add("@NuDocumProv", BE.NuDocumProv)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@TxServicio", BE.TxServicio)
                .Add("@SsMonto", BE.SsMonto)
                .Add("@QtCantidad", BE.QtCantidad)
                .Add("@UserMod", BE.UserMod)

                .Add("@IDCab", BE.IDCab)
                .Add("@CoProducto", BE.CoProducto)
                .Add("@CoAlmacen", BE.CoAlmacen)
                .Add("@SsIGV", BE.SsIGV)
                .Add("@CoTipoOC", BE.CoTipoOC)
                .Add("@CoCtaContab", BE.CoCtaContab)

                .Add("@CoCeCos", BE.CoCeCos)
                .Add("@SSPrecUni", BE.SSPrecUni)
                .Add("@SSTotal", BE.SsTotal)

                .Add("@IDFile", BE_INC.IDFile)
                .Add("@Tipo", BE_INC.Tipo)
                .Add("@Monto", BE_INC.Monto)
                .Add("@FechaPago", BE_INC.FechaPago)
                .Add("@NuCodigo_ER", BE_INC.NuCodigo_ER)

                .Add("@IdDet", BE_INC.IdDet)

                .Add("@NuPreSob", BE_INC.NuPreSob)
                .Add("@NuDetPSo", BE_INC.NuDetPSo)

            End With
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_DET_ENTRADASINC_SAP_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc


                .Add("@NuDocumProv", BE.NuDocumProv)
                .Add("@NuDocumProvDet", BE.NuDocumProvDet)

                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@TxServicio", BE.TxServicio)
                .Add("@SsMonto", BE.SsMonto)
                .Add("@QtCantidad", BE.QtCantidad)
                .Add("@UserMod", BE.UserMod)

                .Add("@IDCab", BE.IDCab)
                .Add("@CoProducto", BE.CoProducto)
                .Add("@CoAlmacen", BE.CoAlmacen)
                .Add("@SsIGV", BE.SsIGV)
                .Add("@CoTipoOC", BE.CoTipoOC)
                .Add("@CoCtaContab", BE.CoCtaContab)

                .Add("@CoCeCos", BE.CoCeCos)
                .Add("@SSPrecUni", BE.SSPrecUni)
                .Add("@SSTotal", BE.SsTotal)

            End With
            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_DET_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub


    Public Overloads Sub Eliminar(ByVal vintNuDocumProv As Integer, ByVal vintNuDocumProvDet As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocumProv", vintNuDocumProv)
                .Add("@NuDocumProvDet", vintNuDocumProvDet)
            End With

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_DET_DEL", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    'Public Overloads Function ConsultarList(ByVal vstrNuNtaVta As String, ByVal vstNuItem As Integer, _
    '                                         ByVal vstCoTipoServicio As String, ByVal vstTxDetalle As String, _
    '                                         ByVal vstCoProveedor As String) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        With dc

    '            .Add("@NuNtaVta", vstrNuNtaVta)
    '            .Add("@NuItem", vstNuItem)
    '            .Add("@CoTipoServicio", vstCoTipoServicio)
    '            .Add("@TxDetalle", vstTxDetalle)
    '            .Add("@CoProveedor", vstCoProveedor)

    '        End With
    '        Return objADO.GetDataTable("NOTAVENTA_COUNTER_ITEM_Sel_xNuNtaVta", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Public Overloads Sub EliminarxDoc(ByVal vintNuDocumProv As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDocumProv", vintNuDocumProv)

            'Dim dt As DataTable = ConsultarList(vstrNuNtaVta, 0, "", "", "")

            ''bucle para eliminar detalle del item
            'For Each drFila As DataRow In dt.Rows
            '    Dim intNuItem As Integer = CInt(drFila("NuItem").ToString)
            '    Dim objDetalleItem As New clsNotaVentaCounter_DetalleItemBT
            '    objDetalleItem.EliminarxNuItem(vstrNuNtaVta, intNuItem)
            'Next

            objADO.ExecuteSP("DOCUMENTO_PROVEEDOR_DET_DEL_xNuDocumProv", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub InsertarActualizarEliminar(ByVal BE As clsDocumentoProveedorBE)
        If BE.ListaDetalle Is Nothing Then Exit Sub
        Try
            For Each Item As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE In BE.ListaDetalle
                If Item.Accion = "N" Then

                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.NuDocumProv, Item.NuDocumProvDet)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub InsertarActualizarEliminar_INC(ByVal BE As clsDocumentoProveedorBE)
        If BE.ListaDetalle Is Nothing Then Exit Sub
        Try
            Dim i As Integer = 0
            For Each Item As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE In BE.ListaDetalle
                Dim itemINC As clsTablasApoyoBE.clsENTRADASINC_SAPBE = BE.ListaINC(i)
                If Item.Accion = "N" Then

                    Insertar_INC(Item, itemINC)
                ElseIf Item.Accion = "M" Then
                    'Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    'Eliminar(Item.NuDocumProv, Item.NuDocumProvDet)
                End If
                i = i + 1
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsDocumentoProveedorBE, ByVal vstrAntiguoID As String, _
                                                ByVal vstrNuevoID As String)
        Try


            If Not BE.ListaDetalle Is Nothing Then
                For Each Item As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE In BE.ListaDetalle
                    If Item.NuDocumProv = vstrAntiguoID Then 'And (Item.IDTipoDoc = vintNuevoID2 Or vintNuevoID2 = "") Then
                        Item.NuDocumProv = vstrNuevoID
                    End If
                Next
            End If


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub ActualizarIDsNuevosenHijosST2(ByRef BE As clsDocumentoProveedorBE, ByVal vstrAntiguoID As String, _
                                              ByVal vstrNuevoID As String)
        Try


            If Not BE.ListaDetalle Is Nothing Then
                For Each Item As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE In BE.ListaDetalle
                    If Item.NuDocumProv = vstrAntiguoID Then 'And (Item.IDTipoDoc = vintNuevoID2 Or vintNuevoID2 = "") Then
                        Item.NuDocumProv = vstrNuevoID
                    End If
                Next
            End If

            If Not BE.ListaINC Is Nothing Then
                For Each Item As clsTablasApoyoBE.clsENTRADASINC_SAPBE In BE.ListaINC
                    If Item.NuDocumProv = vstrAntiguoID Then 'And (Item.IDTipoDoc = vintNuevoID2 Or vintNuevoID2 = "") Then
                        Item.NuDocumProv = vstrNuevoID
                    End If
                Next
            End If


        Catch ex As Exception
            Throw
        End Try
    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDocumento_Forma_EgresoBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Private strNombreClase As String = "clsDocumento_Forma_EgresoBT"

    Public Function intInsertIDDocFormaEgreso(ByVal BE As clsDocumento_Forma_Egreso) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@TipoFormaEgreso", BE.TipoFormaEgreso)
            dc.Add("@CoMoneda", BE.CoMoneda)
            dc.Add("@CoEstado", BE.CoEstado)
            dc.Add("@SsMontoTotal", BE.SsMontoTotal)
            dc.Add("@SsSaldo", BE.SsSaldo)
            dc.Add("@SsDifAceptada", BE.SsDifAceptada)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@TxObsDocumento", BE.TxObsDocumento)
            dc.Add("@CoUbigeo_Oficina", BE.CoUbigeo_Oficina)
            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@pIDDocFormaEgresoInt", 0)

            Return objADO.GetSPOutputValue("DOCUMENTO_FORMA_EGRESO_InsertIDOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".intNuVouPTrenIntxFileProveedor")
        End Try
    End Function

    Public Sub ActualizarSaldoPendiente(ByVal vintIDDocFormaEgreso As Integer, ByVal vstrTipoFormaEgreso As String, ByVal vstrCoMoneda As String,
                                        ByVal vdblTotalOrig As Double, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDocFormaEgreso", vintIDDocFormaEgreso)
            dc.Add("@TipoFormaEgreso", vstrTipoFormaEgreso)
            dc.Add("@CoMoneda_FEgreso", vstrCoMoneda)
            dc.Add("@SsTotalOrig", vdblTotalOrig)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("DOCUMENTO_FORMA_EGRESO_UpdSsPendientexDoc", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

    Public Sub ActualizarxDocumento(ByVal BE As clsDocumento_Forma_Egreso)
        Try
            If BE.IDDocFormaEgreso = 0 Then
                Dim objBTDocForEgr As New clsDocumento_Forma_EgresoBT()
                BE.IDDocFormaEgreso = objBTDocForEgr.intInsertIDDocFormaEgreso(BE)
            End If
            Dim dc As New Dictionary(Of String, String)
            With dc
                dc.Add("@IDDocFormaEgreso", BE.IDDocFormaEgreso)
                dc.Add("@TipoFormaEgreso", BE.TipoFormaEgreso)
                dc.Add("@CoMoneda", BE.CoMoneda)
                dc.Add("@CoEstado", BE.CoEstado)
                dc.Add("@SsSaldo", BE.SsSaldo)
                dc.Add("@SsDifAceptada", BE.SsDifAceptada)
                dc.Add("@UserMod", BE.UserMod)
                dc.Add("@TxObsDocumento", BE.TxObsDocumento)

            End With

            objADO.ExecuteSP("DOCUMENTO_FORMA_EGRESO_UpdxID", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarxDocumento")
        End Try
    End Sub

    Public Sub ActualizarEstado_Multiple(ByVal vintIDDocFormaEgreso As Integer, ByVal vstrTipoFormaEgreso As String, ByVal vstrCoEstado As String, ByVal vstrUserMod As String, Optional ByVal vstrCoMoneda As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDocFormaEgreso", vintIDDocFormaEgreso)
            dc.Add("@TipoFormaEgreso", vstrTipoFormaEgreso)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@CoMoneda_FEgreso", vstrCoMoneda)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("DOCUMENTO_FORMA_EGRESO_Upd_Estado_Multiple", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarSaldoPendiente")
        End Try
    End Sub

End Class
