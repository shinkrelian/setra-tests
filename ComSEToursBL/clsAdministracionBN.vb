﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsAdministracionBaseBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN
    Public Overridable Function ConsultarPk(ByVal vintIDReserva As Integer) As SqlClient.SqlDataReader
        Return Nothing
    End Function

    Public Overridable Function ConsultarList() As DataTable
        Return Nothing
    End Function

    Public Overridable Function ConsultarListxIDCab(ByVal vintIDCab As Integer) As DataTable
        Return Nothing
    End Function

    Public Overridable Function ConsultarxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Return Nothing
    End Function

    Public Overridable Function ConsultarxID() As SqlClient.SqlDataReader
        Return Nothing
    End Function

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAdministracionBN
    Inherits clsAdministracionBaseBN

    Public Function ConsultarReporteFechaINUbigeo(ByVal vstrIDCLiente As String, ByVal vstrIDFIle As String, _
                                                  ByVal vstrTitulo As String, ByVal vstrIDUbigeo As String, _
                                                  ByVal vdatFechaInicioRango1 As Date, ByVal vdatFechaInicioRango2 As Date, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCliente", vstrIDCLiente)
            dc.Add("@IDFile", vstrIDFIle)
            dc.Add("@Titulo", vstrTitulo)
            dc.Add("@IDUbigeo", vstrIDUbigeo)
            dc.Add("@FecInicioRango1", vdatFechaInicioRango1)
            dc.Add("@FecInicioRango2", vdatFechaInicioRango2)
            dc.Add("@CoPais", vstrCoPais)

            Return objADO.GetDataTable(True, "REPORTE_FECHAIN_UBIGEO", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function dttGenerarTextoFacturacionContabilidad(ByVal vDtt As DataTable, ByVal vstrRuta As String, ByVal vstrIDMoneda As String) As DataTable
        Try

            Dim strNuCuenta As String = ""
            Dim strPeriodo As String = ""
            Dim strSubDiario As String = ""
            Dim strNroRegistro As String = ""
            Dim strFecEmision As String = ""
            Dim strTipoCliente As String = ""
            Dim strCodClienteStarSoft As String = ""
            Dim strCodTipoDocStarSoft As String = ""
            Dim strNuDocum As String = ""
            Dim strFecVencim As String = ""
            Dim strIDTipoDocVinc As String = ""
            Dim strNuDocumVinc As String = ""
            Dim strIgv As String = ""
            Dim strImporteTotal As String = ""
            Dim strVTA As String = ""
            Dim strTipoCambio As String = ""
            Dim strTipo_NuDocum As String = ""
            Dim strTexto As String = ""
            Dim chrAnulado As Char = ""
            Dim chrDebeHaber As Char = ""
            Dim strRazonSocial As String = ""
            Dim strCentroCosto As String = ""
            Dim strFecEmisionVinc As String = ""
            Dim chrExportacion As Char = ""
            Dim strIDFile As String = ""
            Dim blnReceptivo As Boolean
            Dim blnCargar As Boolean = False
            Dim dtt As DataTable = dtFiltrarDataTable(vDtt, "IDMoneda='" & vstrIDMoneda & "'")

            Dim dttReturn As New DataTable("STReport")
            Dim objBE As New clsReporteFacturacionContabilidadBE
            For Each Prop As Object In objBE.GetType.GetProperties
                dttReturn.Columns.Add(Prop.name.ToString)
            Next


            For Each dr As DataRow In dtt.Rows

                If dr("CoTipoVenta") = "RC" Then
                    blnReceptivo = True
                    Dim bytCont As Byte = 1
NuevoRC:
                    strNuCuenta = "121221"
                    If bytCont = 1 Then
                        If vstrIDMoneda = "SOL" Then
                            strNuCuenta = "121211"
                        End If
                    Else
                        strNuCuenta = If(IsDBNull(dr("CoCtaContable")), "", dr("CoCtaContable"))
                    End If

                    strPeriodo = dr("Periodo")
                    strSubDiario = dr("SubDiario")
                    strNroRegistro = dr("NroRegistro")
                    strFecEmision = strFechaTextoDDMMYYYY(dr("FecEmision"))
                    strTipoCliente = dr("TipoCliente")
                    strCodClienteStarSoft = If(IsDBNull(dr("CoStarSoft")), "", dr("CoStarSoft"))
                    strCodTipoDocStarSoft = If(IsDBNull(dr("IDTipoDocStarSoft")), "", dr("IDTipoDocStarSoft"))
                    strNuDocum = dr("SerieDoc") & " " & dr("NumeroDoc")
                    strFecVencim = strFechaTextoDDMMYYYY(dr("FecVencim"))

                    ' strFecEmision  = strFechaTextoDDMMYYYY(dr("FecEmision"))
                    strIDTipoDocVinc = If(IsDBNull(dr("IDTipoDocVinc")), "", dr("IDTipoDocVinc"))
                    strNuDocumVinc = ""
                    If Not IsDBNull(dr("SerieDocVinc")) Then
                        strNuDocumVinc = dr("SerieDocVinc") & " " & dr("NumeroDocVinc")
                    End If


                    strIgv = dr("IgvPar")
                    strImporteTotal = dr("ImporteTotal")
                    strVTA = "VTA"
                    strTipoCambio = dr("TipoCambio")
                    strTipo_NuDocum = strCodTipoDocStarSoft & " " & strNuDocum
                    strTexto = If(IsDBNull(dr("Texto")), "", dr("Texto"))
                    chrAnulado = dr("Anulado")

                    chrDebeHaber = "D"
                    If bytCont = 2 Then
                        chrDebeHaber = "H"
                    End If
                    strRazonSocial = dr("RazonSocial").ToString.ToUpper
                    strCentroCosto = If(IsDBNull(dr("CoCtroCosto")), "", dr("CoCtroCosto"))
                    ' strFecVencim  = strFechaTextoDDMMYYYY(dr("FecVencim"))
                    strFecEmisionVinc = "01/01/1900"
                    If Not IsDBNull(dr("FeEmisionVinc")) Then
                        strFecEmisionVinc = strFechaTextoDDMMYYYY(dr("FeEmisionVinc"))
                    End If
                    chrExportacion = dr("Exportacion")
                    strIDFile = If(IsDBNull(dr("IDFile")), "", dr("IDFile"))



                    blnCargar = False
                    If bytCont <= 2 Then
                        blnCargar = True
                        bytCont += 1
                        GoTo CargarST
                    End If

                Else
                    Dim strValorPrevioIGV As String = ""
                    Dim strValorPostIGV As String = ""
                    blnReceptivo = False
                    Dim bytCont As Byte = 1
NuevoNRC:
                    strNuCuenta = "121211"
                    If bytCont = 1 Then
                        If vstrIDMoneda = "USD" Then
                            strNuCuenta = "121221"
                        End If
                    ElseIf bytCont = 2 Then
                        strNuCuenta = "401110"
                    Else
                        strNuCuenta = dr("CoCtaContable")
                    End If

                    strPeriodo = dr("Periodo")
                    strSubDiario = dr("SubDiario")
                    strNroRegistro = dr("NroRegistro")
                    strFecEmision = strFechaTextoDDMMYYYY(dr("FecEmision"))
                    strTipoCliente = dr("TipoCliente")
                    strCodClienteStarSoft = If(IsDBNull(dr("CoStarSoft")), "", dr("CoStarSoft"))
                    strCodTipoDocStarSoft = If(IsDBNull(dr("IDTipoDocStarSoft")), "", dr("IDTipoDocStarSoft"))
                    strNuDocum = dr("SerieDoc") & " " & dr("NumeroDoc")
                    strFecVencim = strFechaTextoDDMMYYYY(dr("FecVencim"))



                    strIDTipoDocVinc = If(IsDBNull(dr("IDTipoDocVinc")), "", dr("IDTipoDocVinc"))
                    'strNuDocumVinc = ""
                    'If Not IsDBNull(dr("SerieDocVinc")) Then
                    '    strNuDocumVinc = dr("SerieDocVinc") & " " & dr("NumeroDocVinc")
                    'End If

                    If bytCont = 1 Then
                        strValorPrevioIGV = dr("Igv")
                        strValorPostIGV = dr("Igv") + dr("ImporteTotal")
                    ElseIf bytCont = 2 Then
                        strValorPrevioIGV = ""
                        strValorPostIGV = dr("Igv")
                    Else
                        strValorPrevioIGV = ""
                        strValorPostIGV = dr("ImporteTotal") - dr("Igv")
                    End If
                    strNuDocumVinc = strValorPrevioIGV

                    strIgv = dr("IgvPar")
                    'strImporteTotal = dr("ImporteTotal")
                    strImporteTotal = strValorPostIGV
                    strVTA = "VTA"
                    strTipoCambio = dr("TipoCambio")
                    strTipo_NuDocum = strCodTipoDocStarSoft & " " & strNuDocum
                    strTexto = If(IsDBNull(dr("Texto")), "", dr("Texto"))
                    chrAnulado = dr("Anulado")
                    chrDebeHaber = "D"
                    If bytCont >= 2 Then
                        chrDebeHaber = "H"
                    End If
                    strRazonSocial = dr("RazonSocial").ToString.ToUpper
                    strCentroCosto = If(IsDBNull(dr("CoCtroCosto")), "", dr("CoCtroCosto"))
                    ' strFecVencim  = strFechaTextoDDMMYYYY(dr("FecVencim"))
                    strFecEmisionVinc = "01/01/1900"
                    If Not IsDBNull(dr("FeEmisionVinc")) Then
                        strFecEmisionVinc = strFechaTextoDDMMYYYY(dr("FeEmisionVinc"))
                    End If
                    chrExportacion = dr("Exportacion")
                    strIDFile = If(IsDBNull(dr("IDFile")), "", dr("IDFile"))


                    blnCargar = False
                    If bytCont <= 3 Then
                        blnCargar = True
                        bytCont += 1
                        GoTo CargarST
                    End If




                End If

CargarST:
                If blnCargar Then
                    Dim NewItem As New clsReporteFacturacionContabilidadBE With { _
                            .NuCuenta = strNuCuenta, _
                            .Periodo = strPeriodo, _
                            .SubDiario = strSubDiario, _
                            .NroRegistro = strNroRegistro, _
                            .FecEmision = strFecEmision, _
                            .TipoCliente = strTipoCliente, _
                            .CodClienteStarSoft = strCodClienteStarSoft, _
                            .CodTipoDocStarSoft = strCodTipoDocStarSoft, _
                            .NuDocum = strNuDocum, _
                            .FecVencim = strFecVencim, _
                            .IDTipoDocVinc = strIDTipoDocVinc, _
                            .NuDocumVinc = strNuDocumVinc, _
                            .IgvPar = strIgv, _
                            .ImporteTotal = strImporteTotal, _
                            .VTA = strVTA, _
                            .TipoCambio = strTipoCambio, _
                            .Tipo_NuDocum = strTipo_NuDocum, _
                            .TextoDoc = strTexto, _
                            .Anulado = chrAnulado, _
                            .DebeHaber = chrDebeHaber, _
                            .RazonSocial = strRazonSocial, _
                            .CentroCosto = strCentroCosto, _
                            .FeEmisionVinc = strFecEmisionVinc, _
                            .Exportacion = chrExportacion, _
                            .IDFile = strIDFile}
                    'dttReturn.Rows.Add(drFilaReporteFacturacionContabilidadTexto(NewItem))
                    dttReturn.ImportRow(drFilaReporteFacturacionContabilidadTexto(NewItem))

                    If blnReceptivo Then
                        GoTo NuevoRC
                    Else
                        GoTo NuevoNRC
                    End If
                End If



            Next

            Return dttReturn

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarFilesPaxINC(ByVal vstrIDFIle As String, _
                                         ByVal vdatFechaInicioRango1 As Date, ByVal vdatFechaInicioRango2 As Date, _
                                       Optional ByVal vblnAutorizados As Boolean = 0, Optional ByVal vblnRaqchi As Boolean = 0) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@FechaIni", vdatFechaInicioRango1)
            dc.Add("@FechaFin", vdatFechaInicioRango2)
            dc.Add("@NuFile", vstrIDFIle)
            dc.Add("@FilesAutorizados", vblnAutorizados)
            dc.Add("@FlRaqchi", vblnRaqchi)
            Return objADO.GetDataTable(True, "COTICAB_Sel_MPWP", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPaxINC(ByVal vintIDCab As Integer, _
                                         ByVal vstrMPWP As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Dim sNameSP = ""
        Try
            If vstrMPWP = "CONS" Then
                dc.Add("@IDCab", vintIDCab)
                sNameSP = "COTIPAX_Sel_CONSETTUR"
            Else
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@vcMPWP", vstrMPWP)
                sNameSP = "COTIPAX_Sel_MPWP"
            End If
            Return objADO.GetDataTable(True, sNameSP, dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPaxINC_Distintos(ByVal vintIDCab As Integer, _
                                       ByVal vstrMPWP As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Dim sNameSP = ""
        Try
            If vstrMPWP = "CONS" Then
                dc.Add("@IDCab", vintIDCab)
                sNameSP = "COTIPAX_Sel_CONSETTUR_Distintos"
            Else
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@vcMPWP", vstrMPWP)
                sNameSP = "COTIPAX_Sel_MPWP_Distintos"
            End If
            Return objADO.GetDataTable(True, sNameSP, dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_Rpt_EntradasINC(ByVal vstrIDFIle As String, _
                                        ByVal vdatFechaInicioRango1 As Date, _
                                        ByVal vdatFechaInicioRango2 As Date, _
                                        ByVal vstrMPWP As String, _
                                        ByVal vstrIDEjecVta As String, _
                                        ByVal vstrIDEjecRes As String, _
                                        ByVal vstrEstado As String, _
                                        ByVal vstrEstadoDatosPax As String, _
                                        ByVal vblnFilesConNinios As Boolean, _
                                        ByVal vblnAutorizados As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@FechaIni", vdatFechaInicioRango1)
            dc.Add("@FechaFin", vdatFechaInicioRango2)
            dc.Add("@NuFile", vstrIDFIle)
            'dc.Add("@vcMPWP", vstrMPWP)
            dc.Add("@vcMPWPRQ", vstrMPWP)
            dc.Add("@IDEjecutivoVta", vstrIDEjecVta)
            dc.Add("@IDEjecutivoRes", vstrIDEjecRes)
            dc.Add("@Estado", vstrEstado)
            dc.Add("@EstadoDatosPax", vstrEstadoDatosPax)
            dc.Add("@FilesConNinios", vblnFilesConNinios)
            dc.Add("@FilesAutorizados", vblnAutorizados)
            Return objADO.GetDataTable(True, "Reporte_INC_MPWP", dc)
            'Return objADO.GetDataTable(True, "Reporte_INC_MPWPRQ", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAsignacionBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarPk(ByVal vintNuAsignacion As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuAsignacion", vintNuAsignacion)

            Return objADO.GetDataReader("ASIGNACION_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Function ConsultarListxClientexAsignacion(ByVal vstrIDCliente As String, ByVal vintNuIngreso As Integer, _
                                                               ByVal vintNuAsignacion As Integer) As Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCliente", vstrIDCliente)
            dc.Add("@NuIngreso", vintNuIngreso)
            dc.Add("@NuAsignacion", vintNuAsignacion)

            Return objADO.GetDataTable(True, "ASIGNACION_SelxClientexNuAsignacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Function ConsultarDebitMemoAsignados(ByVal vintNuAsignacion As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuAsignacion", vintNuAsignacion)
            Return objADO.GetDataReader("ASIGNACION_SelxNuAsignacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function datFechaOutFile(ByVal vstrIDDebitMemo As String) As Date
        Dim dc As New Dictionary(Of String, String)
        Try

            With dc
                .Add("@IDDebitMemo", vstrIDDebitMemo)
                .Add("@pFechaOut", "01/01/1900")
            End With

            Return objADO.GetSPOutputValue("COTICAB_SelFechaOutxDebitMemoOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarListRpt(ByVal vstrIDCliente As String, ByVal vstrIDPais As String, _
                                     ByVal vdatFechaOutRango1 As Date, ByVal vdatFechaOutRango2 As Date, _
                                     ByVal vdatFechaPagRango1 As Date, ByVal vdatFechaPagRango2 As Date) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@IDUbigeo", vstrIDPais)
                .Add("@FechaOutRangInicio", vdatFechaOutRango1)
                .Add("@FechaOutRangFinal", vdatFechaOutRango2)
                .Add("@FechaPagoRangInicio", vdatFechaPagRango1)
                .Add("@FechaPagoRangFinal", vdatFechaPagRango2)
            End With
            Return objADO.GetDataTable(True, "ASIGNACION_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListRptSubTotales(ByVal vstrIDCliente As String, ByVal vstrIDPais As String, _
                                     ByVal vdatFechaOutRango1 As Date, ByVal vdatFechaOutRango2 As Date, _
                                     ByVal vdatFechaPagRango1 As Date, ByVal vdatFechaPagRango2 As Date) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@IDUbigeo", vstrIDPais)
                .Add("@FechaOutRangInicio", vdatFechaOutRango1)
                .Add("@FechaOutRangFinal", vdatFechaOutRango2)
                .Add("@FechaPagoRangInicio", vdatFechaPagRango1)
                .Add("@FechaPagoRangFinal", vdatFechaPagRango2)
            End With
            Return objADO.GetDataTable(True, "ASIGNACION_Sel_RptSubTotales", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    '#Region "Fragmento insertar SAP No Transac."

    '    Public Overloads Function Insertar(ByVal BE As clsAsignacionBE) As Integer
    '        Dim dc As New Dictionary(Of String, String)
    '        Try
    '            Dim intNuAsignacion As Integer = 0
    '            With dc
    '                .Add("@TxObservacion", BE.TxObservacion)
    '                .Add("@SsImportePagado", BE.SsImportePagado)
    '                .Add("@pNuAsignacion", 0)
    '                .Add("@UserMod", BE.UserMod)
    '            End With

    '            intNuAsignacion = objADO.ExecuteSPOutput("ASIGNACION_Ins", dc)
    '            ActualizarIDsNuevosenHijosST(BE, 0, intNuAsignacion)

    '            ActualizarEstadoIngreso(BE)

    '            Dim objBTDebitMemo As New clsDebitMemoBT
    '            objBTDebitMemo.InsertarActualizarEliminar(BE)

    '            Dim objBTAsignados As New clsIngreso_DebitMemoBT
    '            objBTAsignados.InsertarActualizarEliminar(BE)
    '            InsertarBoletasAnticipoxAsignacion(intNuAsignacion, BE.UserMod)

    '            ActualizarMatchIngresosDocumentos(BE.NuIngreso, intNuAsignacion, BE.UserMod)
    '            EnviarDatosCobranzasSAP(BE.NuIngreso)


    '            Return intNuAsignacion
    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Function

    '    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsAsignacionBE, ByVal vstrAntiguoID As String, _
    '                                                         ByVal vintNuevoID As Integer)
    '        If BE.ListaIngresoDebitMemo Is Nothing Then Exit Sub
    '        Try
    '            For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
    '                If Item.NuAsignacion = vstrAntiguoID Then
    '                    Item.NuAsignacion = vintNuevoID
    '                End If
    '            Next
    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Sub

    '    Public Overloads Sub ActualizarEstadoIngreso(ByVal BE As clsAsignacionBE)
    '        Dim dc As New Dictionary(Of String, String)
    '        Try
    '            dc.Add("@NuIngreso", BE.NuIngreso)
    '            dc.Add("@CoEstado", BE.CoEstado)
    '            dc.Add("@UserMod", BE.UserMod)

    '            objADO.ExecuteSP("INGRESO_FINANZAS_UpdEstadoxAsignacion", dc)

    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Sub

    '    Private Sub InsertarBoletasAnticipoxAsignacion(ByVal vintNuAsignacion As Integer, ByVal vstrUserMod As String)
    '        Try

    '            Dim dttAnticipos As DataTable = AnticiposxAsignacion(vintNuAsignacion)
    '            If dttAnticipos.Rows.Count > 0 Then
    '                Dim ListaDoc As New List(Of clsDocumentoBE)

    '                For Each dr As DataRow In dttAnticipos.Rows
    '                    Dim strIDTipoDoc As String = "BOL"
    '                    Dim dblSSPago As Double = dr("SsPago")
    '                    If dblSSPago < 0 Then
    '                        strIDTipoDoc = "NCA"
    '                        dblSSPago = dblSSPago * (-1)
    '                    End If
    '                    Dim strCoCtaContable As String = "496121" '"704112"
    '                    Dim strCoCeCos As String = String.Empty
    '                    If dr("CoTipoVenta") = "RC" Then
    '                        strCoCeCos = "900101"
    '                    End If

    '                    If dr("CoTipoVenta") <> "RC" Then
    '                        strCoCtaContable = "000000"
    '                    Else
    '                        Dim objBN As New clsDocumentoProveedorBN
    '                        Dim dt As DataTable = objBN.ConsultarCtaContable_Documento_Cliente(strCoCeCos, dr("CoTipoVenta"), "005", dr("IDCliente"))
    '                        If dt.Rows.Count > 0 Then
    '                            strCoCtaContable = dt.Rows(0).Item("CtaContable")
    '                        End If
    '                    End If

    '                    Dim ItemDocum As New clsDocumentoBE With { _
    '                            .NuDocum = 0, _
    '                            .IDTipoDoc = strIDTipoDoc, _
    '                            .CoSerie = "005", _
    '                            .IDCab = dr("IDCab"), _
    '                            .FeDocum = dr("FeAcreditada"), _
    '                            .IDMoneda = "USD", _
    '                            .SsTotalDocumUSD = dblSSPago, _
    '                            .SsIGV = 0, _
    '                            .Generado = False, _
    '                            .TxTitulo = "", _
    '                            .NuDocumVinc = "", _
    '                            .IDTipoDocVinc = "", _
    '                            .FlPorAjustePrecio = False, _
    '                            .CoTipoVenta = dr("CoTipoVenta"), _
    '                            .IDCliente = dr("IDCliente"), _
    '                            .CoCtaContable = strCoCtaContable, _
    '                            .FeEmisionVinc = "01/01/1900", _
    '                            .CoCtroCosto = "", _
    '                            .NuFileLibre = "", _
    '                            .UserMod = vstrUserMod _
    '                        }
    '                    ListaDoc.Add(ItemDocum)
    '                Next

    '                Dim BEDocs As New clsDocumentoBE With {.ListaDocumentos = ListaDoc}
    '                Dim objDocBT As New clsDocumentoBT
    '                objDocBT.InsertarDocumentosGenerados(0, BEDocs, vstrUserMod)
    '            End If
    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Sub

    '    Private Function AnticiposxAsignacion(ByVal vintNuAsignacion As Integer) As DataTable
    '        Dim dc As New Dictionary(Of String, String)
    '        Try
    '            dc.Add("@NuAsignacion", vintNuAsignacion)

    '            Return objADO.GetDataTable(True, "ASIGNACION_SelAnticiposxNuAsignacion", dc)
    '        Catch ex As Exception
    '            Throw
    '        End Try
    '    End Function
    '#End Region

    Public Function intInsertar(ByVal BE As clsAsignacionBE, ByRef rListaErrores As List(Of String), _
                                ByRef rListaDMAntic As List(Of String), ByVal vblnNC As Boolean, ByVal vstrCoDebitMemoNeg As String) As Integer

        Dim objBT As New clsAsignacionBT
        Dim strError As String = ""
        Dim blnError As Boolean = False
        'Dim ListaErrores As New List(Of String)
        Dim intNuAsignacion As Integer = 0
        Dim blnEntro As Boolean = False

        If Not vblnNC Then
            For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
                If Item.Accion = "N" Then

                    Dim strCoTipoDoc As String = "", strNuDocum As String = "", strCoSAP As String = "", _
                        dblSSTotalDoc As Double = 0, strCoMonedaDoc As String = "", strCoTipoVtaDoc As String = "", _
                        strCoClienteDoc As String = "", intIDCab As Integer = 0, datFeDocum As Date = "01/01/1900"

                    intNuAsignacion = objBT.intInsertarxDMemo(BE, intNuAsignacion, Item.IDDebitMemo, strError, "N", Item.Accion, _
                                                                strCoTipoDoc, strNuDocum, strCoSAP, strCoMonedaDoc, dblSSTotalDoc, _
                                                                strCoTipoVtaDoc, strCoClienteDoc, intIDCab, datFeDocum, "", True)

                    Dim dblSsImportePagado As Double = 0
                    If strError <> "" Then
                        If strError.Substring(0, 4) = "SAP:" Then
                            Item.ErrorSAP = strError
                        Else
                            Item.ErrorSetra = strError
                        End If
                        blnError = True
                        rListaErrores.Add(Item.IDDebitMemo & " " & strError)
                        'rListaErrores.Add(strError)
                        If strNuDocum <> "" Then
                            'objBT.AnularAnticiposSAP(strCoTipoDoc, strNuDocum, strCoSAP, intNuAsignacion, Item.IDDebitMemo, BE.UserMod, dttAnticipos)
                            objBT.CrearAnularAnticipos(strCoTipoDoc, strNuDocum, Item.IDDebitMemo, _
                                                       strCoMonedaDoc, dblSSTotalDoc, strCoTipoVtaDoc, strCoClienteDoc, intIDCab, datFeDocum, BE.UserMod)
                            'Dim objDocBT As New clsDocumentoBT
                            'objDocBT.Anular(strCoTipoDoc, strNuDocum, BE.UserMod, "")
                            rListaDMAntic.Add("Debit Memo: " & Item.IDDebitMemo & " Cód. SAP/Nro. Docum.: " & strCoSAP & "/" & strNuDocum)
                        End If
                        If Not blnEntro Then intNuAsignacion = 0
                    Else
                        'dblSsImportePagado += BE.SsImportePagado
                        dblSsImportePagado = Item.SsPago 'BE.SsImportePagado
                        If dblSsImportePagado <> 0 Then
                            objBT.ActualizarPago(intNuAsignacion, dblSsImportePagado, BE.UserMod)
                        End If

                        rListaErrores.Add(Item.IDDebitMemo & " se asignó exitosamente.")
                    End If

                    blnEntro = True
                End If
            Next

            'If Not blnError And blnEntro Then
            If blnEntro And intNuAsignacion > 0 Then
                'objBT.InsertarBoletasAnticipoxAsignacion(intNuAsignacion, BE.UserMod)
                objBT.ActualizarMatchIngresosDocumentos(BE.NuIngreso, intNuAsignacion, BE.UserMod)

            End If
        Else
            'Dim strCoTipoDoc As String = "", strNuDocum As String = "", strCoSAP As String = "", _
            '    dblSSTotalDoc As Double = 0, strCoMonedaDoc As String = "", strCoTipoVtaDoc As String = "", _
            '    strCoClienteDoc As String = "", intIDCab As Integer = 0, datFeDocum As Date = "01/01/1900"
            Dim ListaDocsAntic As New List(Of clsDocumentoBE)
            intNuAsignacion = objBT.InsertarconNCred(BE, vstrCoDebitMemoNeg, strError, ListaDocsAntic, "N")

            Dim dblSsImportePagado As Double = 0
            If strError <> "" Then

                blnError = True
                rListaErrores.Add(vstrCoDebitMemoNeg & " " & strError)

                For Each NewDoc As clsDocumentoBE In ListaDocsAntic
                    objBT.CrearAnularAnticipos(NewDoc.IDTipoDoc, NewDoc.NuDocum, NewDoc.CoDebitMemoAnticipo, _
                                               NewDoc.IDMoneda, NewDoc.SsTotalDocumUSD, NewDoc.CoTipoVenta, NewDoc.IDCliente, NewDoc.IDCab, NewDoc.FeDocum, BE.UserMod)
                    rListaDMAntic.Add("Debit Memo: " & vstrCoDebitMemoNeg & " Cód. SAP/Nro. Docum.: " & NewDoc.CoSAP & "/" & NewDoc.NuDocum)
                Next

                intNuAsignacion = 0
            Else
                dblSsImportePagado = BE.SsImportePagado
                If dblSsImportePagado <> 0 Then
                    objBT.ActualizarPago(intNuAsignacion, dblSsImportePagado, BE.UserMod)
                End If

                rListaErrores.Add(vstrCoDebitMemoNeg & " se asignó exitosamente.")
            End If

            If intNuAsignacion > 0 Then
                objBT.ActualizarMatchIngresosDocumentos(BE.NuIngreso, intNuAsignacion, BE.UserMod)
            End If
        End If


        Return intNuAsignacion
    End Function

    Public Sub Actualizar(ByVal BE As clsAsignacionBE, rListaErrores As List(Of String), ByRef rListaDMAntic As List(Of String), _
                          ByVal vblnNC As Boolean, ByVal vstrCoDebitMemoNeg As String)

        Dim objBT As New clsAsignacionBT
        Dim strError As String = ""
        Dim blnError As Boolean = False
        'Dim ListaErrores As New List(Of String)
        Dim intNuAsignacion As Integer = BE.NuAsignacion

        Dim blnEntro As Boolean = False

        If Not vblnNC Then
            For Each Item As clsIngreso_DebitMemoBE In BE.ListaIngresoDebitMemo
                If Item.Accion = "N" Or Item.Accion = "B" Then
                    Dim strCoTipoDoc As String = "", strNuDocum As String = "", strCoSAP As String = "",
                         dblSSTotalDoc As Double = 0, strCoMonedaDoc As String = "", strCoClienteDoc As String = "", strCoTipoVta As String = "", _
                         intIDCab As Integer = 0, datFeDocum As Date = "01/01/1900"
                    intNuAsignacion = objBT.intInsertarxDMemo(BE, intNuAsignacion, Item.IDDebitMemo, strError, "E", Item.Accion, _
                                                              strCoTipoDoc, strNuDocum, strCoSAP, strCoMonedaDoc, dblSSTotalDoc, strCoTipoVta, _
                                                              strCoClienteDoc, intIDCab, datFeDocum, Item.CoSAP, True)

                    Dim dblSsImportePagado As Double = 0
                    If strError <> "" Then
                        If strError.Substring(0, 4) = "SAP:" Then
                            Item.ErrorSAP = strError
                        Else
                            Item.ErrorSetra = strError
                        End If
                        blnError = True
                        rListaErrores.Add(Item.IDDebitMemo & " " & strError)
                        If strNuDocum <> "" Then

                            objBT.CrearAnularAnticipos(strCoTipoDoc, strNuDocum, Item.IDDebitMemo, _
                                          strCoMonedaDoc, dblSSTotalDoc, strCoTipoVta, strCoClienteDoc, intIDCab, datFeDocum, BE.UserMod)

                            rListaDMAntic.Add("Debit Memo: " & Item.IDDebitMemo & " Cód. SAP/Nro. Docum.: " & strCoSAP & "/" & strNuDocum)
                        End If
                    Else
                        dblSsImportePagado = Item.SsPago
                        If Item.Accion = "B" Then
                            dblSsImportePagado = dblSsImportePagado * (-1)
                        End If
                        If dblSsImportePagado <> 0 Then
                            objBT.ActualizarPago(intNuAsignacion, dblSsImportePagado, BE.UserMod)
                        End If
                        rListaErrores.Add(Item.IDDebitMemo & " " & " se asignó exitosamente.")

                        blnEntro = True
                    End If


                End If
            Next

            'If Not blnError And blnEntro Then
            If blnEntro And intNuAsignacion > 0 Then
                'objBT.InsertarBoletasAnticipoxAsignacion(intNuAsignacion, BE.UserMod)
                objBT.ActualizarMatchIngresosDocumentos(BE.NuIngreso, intNuAsignacion, BE.UserMod)

            End If
        Else
            Dim ListaDocsAntic As New List(Of clsDocumentoBE)
            intNuAsignacion = objBT.InsertarconNCred(BE, vstrCoDebitMemoNeg, strError, ListaDocsAntic, "E")

            Dim dblSsImportePagado As Double = 0
            If strError <> "" Then

                blnError = True
                rListaErrores.Add(vstrCoDebitMemoNeg & " " & strError)

                For Each NewDoc As clsDocumentoBE In ListaDocsAntic
                    objBT.CrearAnularAnticipos(NewDoc.IDTipoDoc, NewDoc.NuDocum, NewDoc.CoDebitMemoAnticipo, _
                                               NewDoc.IDMoneda, NewDoc.SsTotalDocumUSD, NewDoc.CoTipoVenta, NewDoc.IDCliente, NewDoc.IDCab, NewDoc.FeDocum, BE.UserMod)
                    rListaDMAntic.Add("Debit Memo: " & vstrCoDebitMemoNeg & " Cód. SAP/Nro. Docum.: " & NewDoc.CoSAP & "/" & NewDoc.NuDocum)
                Next

                intNuAsignacion = 0
            Else
                dblSsImportePagado = BE.SsImportePagado
                If dblSsImportePagado <> 0 Then
                    objBT.ActualizarPago(intNuAsignacion, dblSsImportePagado, BE.UserMod)
                End If

                rListaErrores.Add(vstrCoDebitMemoNeg & " se asignó exitosamente.")
            End If

            If intNuAsignacion > 0 Then
                objBT.ActualizarMatchIngresosDocumentos(BE.NuIngreso, intNuAsignacion, BE.UserMod)
            End If
        End If
    End Sub

    Public Function strCoSAPAnticiposxDMemo(ByVal vstrNuDebitMemo As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDebitMemo", vstrNuDebitMemo)
            dc.Add("@pCoSAP", "")

            Return objADO.ExecuteSPOutput("DOCUMENTO_SelCoSAPAnticipoxDMemoOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsIngresoFinanzasBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrDescCliente As String, ByVal vstrEstado As String, _
                                            ByVal vstrIDBanco As String, ByVal vstrCodigoOperacion As String, _
                                            ByVal vdatRangoFecha1 As Date, ByVal vdatRangoFecha2 As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@DescCliente", vstrDescCliente)
                .Add("@CoEstado", vstrEstado)
                .Add("@Banco", vstrIDBanco)
                .Add("@CoOperacionBan", vstrCodigoOperacion)
                .Add("@RangoFecha1", vdatRangoFecha1)
                .Add("@RangoFecha2", vdatRangoFecha2)
            End With

            Return objADO.GetDataTable(True, "INGRESO_FINANZAS_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vintNuIngreso As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)

            Return objADO.GetDataReader("INGRESO_FINANZAS_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarxID(ByVal vintNuIngreso As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuIngreso", vintNuIngreso)
            Return objADO.GetDataReader("INGRESO_FINANZAS_Sel_ListxNuIngreso", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_Rpt_ParaisosFiscales(ByVal vstrAnio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Anio", vstrAnio)
            Return objADO.GetDataTable(True, "RptTransferenciasParaisosFiscales", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenPago_AdjuntosBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarxIDOrdenPago(ByVal vintIDOrdPag As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)

            Return objADO.GetDataTable(True, "ORDENPAGO_ADJUNTOS_Sel_ListxIDOrdPag", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenCompraBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrNuOrdCom As String, _
                                          ByVal vstrDescripcionProveedor As String, _
                                          ByVal vstrDescripcion As String, _
                                          ByVal vdatFechaIni As Date, _
                                          ByVal vdatFechaFin As Date, _
                                          ByVal vstrCoEstado_OC As String, _
                                           ByVal vstrCoMoneda As String) As DataTable
        'ByVal vintCoArea As Integer, ByVal vstrCoMoneda As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@NuOrdCom", vstrNuOrdCom)
            dc.Add("@DescripcionProveedor", vstrDescripcionProveedor)
            dc.Add("@Descripcion", vstrDescripcion)
            dc.Add("@FechaIni", vdatFechaIni)
            dc.Add("@FechaFin", vdatFechaFin)
            dc.Add("@CoEstado_OC", vstrCoEstado_OC)
            'dc.Add("@CoArea", vintCoArea)
            dc.Add("@CoMoneda", vstrCoMoneda)

            Return objADO.GetDataTable(True, "ORDENCOMPRA_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarList_PDF(ByVal vstrNuOrdComInt As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdComInt", vstrNuOrdComInt)

            Return objADO.GetDataTable(True, "ORDENCOMPRA_Sel_List_PDF", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vstrNuOrdComInt As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuOrdComInt", vstrNuOrdComInt)

            'Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Rpt_Exportar_Starsoft", dc)
            Return objADO.GetDataReader("ORDENCOMPRA_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

'--------------------------------------------------------------------------------
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsFondoFijoBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vintNuFondoFijo As Integer, _
                                          ByVal vstrNuCodigo As String, _
                                          ByVal vstrCoCeCos As String, _
                                          ByVal vstrCtaContable As String, _
                                          ByVal vstrDescripcion As String, _
                                          ByVal vstrCoUserResponsable As String, _
                                          ByVal vstrCoMoneda As String, _
                                          ByVal vblnFlActivo As Boolean,
                                          Optional ByVal vstrCoTipoFondoFijo As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@NuFondoFijo", vintNuFondoFijo)
            dc.Add("@NuCodigo", vstrNuCodigo)
            dc.Add("@CoCeCos", vstrCoCeCos)
            dc.Add("@CtaContable", vstrCtaContable)
            dc.Add("@Descripcion", vstrDescripcion)
            dc.Add("@CoUserResponsable", vstrCoUserResponsable)
            dc.Add("@CoMoneda", vstrCoMoneda)
            dc.Add("@FlActivo", vblnFlActivo)
            dc.Add("@CoTipoFondoFijo", vstrCoTipoFondoFijo)

            Return objADO.GetDataTable(True, "MAFONDOFIJO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function DevolverCorrelativo_Codigo(ByVal vstrCoPrefijo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@CoPrefijo", vstrCoPrefijo)

            Return objADO.GetDataTable(True, "MAFONDOFIJO_GetCorrelativoNumero", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vintNuFondoFijo As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuFondoFijo", vintNuFondoFijo)

            'Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Rpt_Exportar_Starsoft", dc)
            Return objADO.GetDataReader("MAFONDOFIJO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenCompra_DetBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vintNuOrdComInt_Det As Integer, _
                                          ByVal vintNuOrdComInt As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@NuOrdComInt_Det", vintNuOrdComInt_Det)
            dc.Add("@NuOrdComInt", vintNuOrdComInt)

            Return objADO.GetDataTable(True, "ORDENCOMPRA_DET_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarList_PDF(ByVal vintNuOrdComInt As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@NuOrdComInt", vintNuOrdComInt)

            Return objADO.GetDataTable(True, "ORDENCOMPRA_DET_Sel_List_PDF", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsRubroBN
    Inherits clsAdministracionBaseBN

    Public Function ConsultarListCbo(ByVal vintCoRubro As Integer, _
                                          ByVal vstrNoRubro As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@CoRubro", vintCoRubro)
            dc.Add("@NoRubro", vstrNoRubro)

            Return objADO.GetDataTable(True, "MARUBRO_Sel_List_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsCentroCostosBN
    Inherits clsAdministracionBaseBN

    Public Function ConsultarListCbo() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "MACENTROCOSTOS_Sel_List_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListCbo_ServiosVarios() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "MACENTROCOSTOS_Sel_List_Cbo_ServiciosVarios", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListCbo_TipoVenta(vstrCoTipoVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTipoVenta", vstrCoTipoVenta)
            End With
            Return objADO.GetDataTable(True, "MACENTROCOSTOS_TIPOVENTA_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsCentroControlBN
    Inherits clsAdministracionBaseBN

    Public Function ConsultarListCbo() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "MACENTROCONTROL_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

'--------------------------------------------------------------------------------

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenPagoBN
    Inherits clsAdministracionBaseBN

    Public Function blnExistexFile(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@pbExiste", False)
            End With

            Return objADO.GetSPOutputValue("ORDENPAGO_SelExistexFileOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function Consultar(ByVal vintIDReserva As Integer, ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCAB", vintIDCab)

            Return objADO.GetDataTable(True, "ORDENPAGO_SelxIDReserva", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRpt(ByVal vstrIDFile As String, _
                                 ByVal vstrDescripcionProveedor As String, _
                                 ByVal vdatFechaPagoIni As Date, _
                                 ByVal vdatFechaPagoFin As Date, _
                                 ByVal vdatFechaEmisionIni As Date, _
                                 ByVal vdatFechaEmisionFin As Date, _
                                 ByVal vstrIDEstado As String, _
                                 ByVal vintIDReserva As Integer, _
                                 ByVal vintIDCab As Integer, _
                                 ByVal vstrIDBanco As String, _
                                 ByVal vstrIDMoneda As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@DescripcionProveedor", vstrDescripcionProveedor)
            dc.Add("@FechaPagoIni", vdatFechaPagoIni)
            dc.Add("@FechaPagoFin", vdatFechaPagoFin)
            dc.Add("@FechaEmisionIni", vdatFechaEmisionIni)
            dc.Add("@FechaEmisionFin", vdatFechaEmisionFin)
            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDMoneda", vstrIDMoneda)
            dc.Add("@IDBanco", vstrIDBanco)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "ORDENPAGO_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRpt_Reservas(ByVal vstrIDFile As String, _
                                ByVal vstrIDProveedor As String, _
                                ByVal vdatFechaPagoIni As Date, _
                                ByVal vdatFechaPagoFin As Date, _
                                ByVal vdatFechaEmisionIni As Date, _
                                ByVal vdatFechaEmisionFin As Date, _
                                ByVal vdatFechaIngInicial As Date, _
                                ByVal vdatFechaIngFinal As Date, _
                                ByVal vdatFechaOutInicial As Date, _
                                ByVal vdatFechaOutFinal As Date, _
                                ByVal vstrIDEstado As String, _
                                ByVal vintIDCab As Integer, _
                                ByVal vblnFlHistorico As Boolean, _
                                ByVal vstrEstado As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@FechaPagoIni", vdatFechaPagoIni)
            dc.Add("@FechaPagoFin", vdatFechaPagoFin)
            dc.Add("@FechaEmisionIni", vdatFechaEmisionIni)
            dc.Add("@FechaEmisionFin", vdatFechaEmisionFin)

            dc.Add("@FechaIngInicial", vdatFechaIngInicial)
            dc.Add("@FechaIngFinal", vdatFechaIngFinal)
            dc.Add("@FechaOutInicial", vdatFechaOutInicial)
            dc.Add("@FechaOutFinal", vdatFechaOutFinal)

            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@IDCab", vintIDCab)

            dc.Add("@FlHistorico", vblnFlHistorico)
            dc.Add("@Estado", vstrEstado)

            Return objADO.GetDataTable(True, "ORDENPAGO_Sel_RPT_Reservas", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vintIDOrdenPago As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdenPago)

            Return objADO.GetDataReader("ORDENPAGO_SelxIDOrdPag", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarList(ByVal vstrIDFile As String, _
                                            ByVal vstrDescripcionProveedor As String, _
                                            ByVal vdatFechaPagoIni As Date, _
                                            ByVal vdatFechaPagoFin As Date, _
                                            ByVal vdatFechaEmisionIni As Date, _
                                            ByVal vdatFechaEmisionFin As Date, _
                                            ByVal vstrIDEstado As String, _
                                            ByVal vintIDReserva As Integer, _
                                            ByVal vintIDCab As Integer, _
                                            ByVal vstrIDBanco As String, _
                                            ByVal vstrIDMoneda As String, _
                                            ByVal vstrCoUbigeo_Oficina As String, Optional ByVal vblnFlEnviado As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@DescripcionProveedor", vstrDescripcionProveedor)
            dc.Add("@FechaPagoIni", vdatFechaPagoIni)
            dc.Add("@FechaPagoFin", vdatFechaPagoFin)
            dc.Add("@FechaEmisionIni", vdatFechaEmisionIni)
            dc.Add("@FechaEmisionFin", vdatFechaEmisionFin)
            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDMoneda", vstrIDMoneda)
            dc.Add("@IDBanco", vstrIDBanco)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            dc.Add("@FlEnviado", vblnFlEnviado)

            Return objADO.GetDataTable(True, "ORDENPAGO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarList_Finanzas(ByVal vstrIDFile As String, _
                                           ByVal vstrDescripcionProveedor As String, _
                                           ByVal vdatFechaPagoIni As Date, _
                                           ByVal vdatFechaPagoFin As Date, _
                                           ByVal vdatFechaEmisionIni As Date, _
                                           ByVal vdatFechaEmisionFin As Date, _
                                           ByVal vstrIDEstado As String, _
                                           ByVal vintIDReserva As Integer, _
                                           ByVal vintIDCab As Integer, _
                                           ByVal vstrIDBanco As String, _
                                           ByVal vstrIDMoneda As String, _
                                           ByVal vstrCoUbigeo_Oficina As String, _
                                           Optional ByVal vblnFlEnviado As Boolean = False, _
                                           Optional ByVal vblnFlHistorico As Boolean = False) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@DescripcionProveedor", vstrDescripcionProveedor)
            dc.Add("@FechaPagoIni", vdatFechaPagoIni)
            dc.Add("@FechaPagoFin", vdatFechaPagoFin)
            dc.Add("@FechaEmisionIni", vdatFechaEmisionIni)
            dc.Add("@FechaEmisionFin", vdatFechaEmisionFin)
            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDMoneda", vstrIDMoneda)
            dc.Add("@IDBanco", vstrIDBanco)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            dc.Add("@FlEnviado", vblnFlEnviado)
            dc.Add("@FlHistorico", vblnFlHistorico)

            Return objADO.GetDataTable(True, "ORDENPAGO_Sel_List_Finanzas", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_Documento_Multiple_Proveedor(ByVal vstrIDproveedor As String, ByVal vstrCoMoneda As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDproveedor", vstrIDproveedor)
            dc.Add("@CoMoneda", vstrCoMoneda)

            Return objADO.GetDataTable(True, "ORDENPAGO_Sel_List_Proveedor_DocMultiple", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strRazSocialUbigeoOficina(ByVal vstrIDProveedor As String) As String
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor ", vstrIDProveedor)
            dc.Add("@pvcRazonSocial", "")

            Return objADO.ExecuteSPOutput("MAPROVEEDORES_SelRazSocialCoUbigeo_OficinaOutput", dc)

        Catch ex As Exception

            Throw
        End Try

    End Function

    Public Overloads Function ConsultarRptPDF(ByVal vintIDOrdenPago As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPago", vintIDOrdenPago)

            Return objADO.GetDataTable(True, "Consultar_Sel_RptPDF", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function dblSaldoLimiteaPagar(ByVal vintIDReserva As Integer) As Double
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnResultado As Boolean = False
            With dc
                .Add("@IDReserva", vintIDReserva)
                .Add("@pSaldoaPagar", 0)
            End With

            Return objADO.GetSPOutputValue("RESERVA_SelSaldoaPagar", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ListaSaldosxPagarDetalle(ByVal vListaIn As List(Of clsDetalleLiquidacionBE), _
                                             ByVal vstrMonedaRequerida As String) As List(Of clsDetalleLiquidacionBE)
        Try

            Dim ListaOut As New List(Of clsDetalleLiquidacionBE)

            For Each ItemLiq As clsDetalleLiquidacionBE In vListaIn
                Dim ItemNew As clsDetalleLiquidacionBE = ItemLiq
                If ItemLiq.IDTransporte = 0 Then
                    ItemNew.SaldoxPagar = dblSaldoaPagarDetalle(ItemLiq.IDReserva_Det, vstrMonedaRequerida, _
                                                            ItemLiq.TipoCambio) 'ItemLiq.IDReserva_Det, ItemLiq.IDMoneda, _
                    ItemLiq.SaldoPagado = dblSaldoPagadoDetalle(ItemLiq.IDReserva_Det, vstrMonedaRequerida, _
                                                                ItemLiq.TipoCambio)
                Else
                    ItemLiq.SaldoxPagar = dblSadoaPagarDetalleVuelo(ItemLiq.IDTransporte)
                    ItemLiq.SaldoPagado = dblSadoaPagadoDetalleVuelo(ItemLiq.IDTransporte)
                End If
                ItemNew.TotalLiq = ItemNew.SaldoxPagar
                ListaOut.Add(ItemNew)
            Next

            Return vListaIn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblSadoaPagarDetalleVuelo(ByVal vintIDTransporte As Integer) As Double
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTransporte", vintIDTransporte)
            dc.Add("@pSaldoPendiente", 0)
            Return objADO.GetSPOutputValue("COTIVUELOS_Sel_TotalxPagar", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblSadoaPagadoDetalleVuelo(ByVal vintIDTransporte As Integer) As Double
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTransporte", vintIDTransporte)
            dc.Add("@pSaldoPagado", 0)
            Return objADO.GetSPOutputValue("COTIVUELOS_Sel_TotalxPagado", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblSaldoaPagarDetalle(ByVal vintIDReserva_Det As Integer, _
                                           ByVal vstrIDMoneda As String, _
                                           ByVal vsglTCambio As Single) As Double
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDReserva_Det", vintIDReserva_Det)
                .Add("@IDMoneda", vstrIDMoneda)
                .Add("@TCambio", vsglTCambio)
                .Add("@pSaldo", 0)
            End With
            Return objADO.GetSPOutputValue("ORDENPAGO_DET_SelSaldoxPagarOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblSaldoPagadoDetalle(ByVal vintIDReserva_Det As Integer, _
                                       ByVal vstrIDMoneda As String, _
                                       ByVal vsglTCambio As Single) As Double
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDReserva_Det", vintIDReserva_Det)
                .Add("@IDMoneda", vstrIDMoneda)
                .Add("@TCambio", vsglTCambio)
                .Add("@pSaldo", 0)
            End With
            Return objADO.GetSPOutputValue("ORDENPAGO_DET_SelSaldoPagadoOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleAdministracionBN
    Inherits clsAdministracionBaseBN

    Public Function Consultar(ByVal vintIDReserva As Integer, ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDCAB", vintIDCAB)

            Return objADO.GetDataTable(True, "ORDENPAGO_DET_SelxIDReserva", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarxPK(ByVal vintIDDetOrdenPago As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag_Det", vintIDDetOrdenPago)

            Return objADO.GetDataReader("ORDENPAGO_DET_SelxIDOrdPag_Det", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxIDOrdPag(ByVal vintIDOrdPag As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintIDOrdPag)

            Return objADO.GetDataTable(True, "ORDENPAGO_DET_SelxIDOrdPag", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function ConsultarDetraccion_Detalle(ByVal vintIDOrdPag As Integer, vintIDServicio_Det As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDOrdPag", vintIDOrdPag)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)

            Return objADO.GetDataTable(True, "ordenpago_Det_Sel_Detraccion", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveDetallexOrdenPagoTexto(ByVal vintNuOrdenPago As Integer) As String
        Dim strTextReturn As String = ""
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintNuOrdenPago)
            Dim dtDatosDetalle As Data.DataTable = objADO.GetDataTable(True, "ORDENPAGO_DET_Sel_MailConfirmacion", dc)

            Dim strCoMoneda As String = ""
            Dim dblMontoTotal As Double = 0
            Dim dblMontoPorPagar As Double = 0
            Dim dblMontoDetraccion As Double = 0
            'Dim dblMontoDetraccion_Soles As Double = 0
            'Dim dblMontoDetraccion_Soles_Limite As Double = 700
            For Each dItem As Data.DataRow In dtDatosDetalle.Rows
                Dim intNroPax As Byte = CByte(dItem("NroPax"))
                'Dim dblTotalPagado As Double = CDbl(dItem("TotalServicio"))
                Dim dblTotalPagado As Double = CDbl(dItem("Total"))
                Dim dblTotalxPax As Double = dblTotalPagado / intNroPax
                Dim strTotales As String = dItem("CoMoneda").ToString & " " & Format(dblTotalxPax, "###,##0.00") & " X " & intNroPax.ToString() & " PAX ="
                If strCoMoneda = "" Then strCoMoneda = dItem("CoMoneda").ToString.Trim

                strTextReturn += strCampoCorreoTexto(3, intNroPax.ToString("00")) & _
                                 strCampoCorreoTexto(45, dItem("Servicio").ToString) & _
                                 strCampoCorreoTexto(25, strTotales, True) & _
                                 strCampoCorreoTexto(8, Format(dblTotalPagado, "###,##0.00"), True) & " PREPAGO (100%) " & dItem("CoMoneda").ToString & _
                                 strCampoCorreoTexto(10, Format(dblTotalPagado, "###,##0.00"), True) & vbCrLf
                dblMontoTotal += dblTotalPagado
                dblMontoPorPagar += CDbl(dItem("SaldoxPagar"))
                If dblMontoDetraccion = 0 Then dblMontoDetraccion += CDbl(dItem("SSDetraccion"))
            Next
            If strCoMoneda = "SOL" Then

            End If
            If dblMontoDetraccion > 0 Then
                strTextReturn += strCampoCorreoTexto(96, "SUB TOTAL", True) & strCampoCorreoTexto(4, strCoMoneda, True) & _
                             strCampoCorreoTexto(10, Format(dblMontoTotal, "###,##0.00"), True) & vbCrLf
                strTextReturn += strCampoCorreoTexto(96, "DETRACCIÓN", True) & strCampoCorreoTexto(4, strCoMoneda, True) & _
                             strCampoCorreoTexto(10, Format(dblMontoDetraccion, "###,##0.00"), True) & vbCrLf
                dblMontoTotal -= dblMontoDetraccion
            End If
            strTextReturn += strCampoCorreoTexto(96, "TOTAL", True) & strCampoCorreoTexto(4, strCoMoneda, True) & _
                             strCampoCorreoTexto(10, Format(dblMontoTotal, "###,##0.00"), True) & vbCrLf
            strTextReturn += strCampoCorreoTexto(96, "SALDO PENDIENTE", True) & strCampoCorreoTexto(4, strCoMoneda, True) & _
                             strCampoCorreoTexto(10, Format(dblMontoPorPagar, "###,##0.00"), True)
            Return strTextReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveDetallexOrdenPagoTexto_HTML(ByVal vintNuOrdenPago As Integer) As String
        Dim strTextReturn As String = ""
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDOrdPag", vintNuOrdenPago)
            Dim dtDatosDetalle As Data.DataTable = objADO.GetDataTable(True, "ORDENPAGO_DET_Sel_MailConfirmacion", dc)

            Dim strCoMoneda As String = ""
            Dim dblMontoTotal As Double = 0
            Dim dblMontoPorPagar As Double = 0
            Dim dblMontoDetraccion As Double = 0
            strTextReturn += "<table border=""0"" cellspacing=""0"" cellpaiding=""0"" width=""100%"">" & vbCrLf
            For Each dItem As Data.DataRow In dtDatosDetalle.Rows
                strTextReturn += "<tr width=""100%"">" & vbCrLf

                Dim intNroPax As Byte = CByte(dItem("NroPax"))
                'Dim dblTotalPagado As Double = CDbl(dItem("TotalServicio"))
                Dim dblTotalPagado As Double = CDbl(dItem("Total"))
                Dim dblTotalxPax As Double = dblTotalPagado / intNroPax
                Dim strTotales As String = dItem("CoMoneda").ToString & " " & Format(dblTotalxPax, "###,##0.00") & " X " & intNroPax.ToString() & " PAX ="
                If strCoMoneda = "" Then strCoMoneda = dItem("CoMoneda").ToString.Trim

                strTextReturn += "<td width=""10%"">" & strCampoCorreoTexto(3, intNroPax.ToString("00")) & "</td>" & _
                                 "<td align=""right"" width=""25%"">" & strCampoCorreoTexto(45, dItem("Servicio").ToString) & "</td>" & _
                                 "<td align=""right"" width=""25%"">" & strCampoCorreoTexto(25, strTotales, True) & "</td>" & _
                                 "<td align=""right"" width=""30%"">" & strCampoCorreoTexto(8, Format(dblTotalPagado, "###,##0.00"), True) & " PREPAGO (100%) " & dItem("CoMoneda").ToString & "</td>" & _
                                 "<td align=""right"" width=""10%"">" & strCampoCorreoTexto(10, Format(dblTotalPagado, "###,##0.00"), True) & "</td>" & vbCrLf
                dblMontoTotal += dblTotalPagado
                dblMontoPorPagar += CDbl(dItem("SaldoxPagar"))
                If dblMontoDetraccion = 0 Then dblMontoDetraccion += CDbl(dItem("SSDetraccion"))
                strTextReturn += "</tr>" & vbCrLf
            Next
            strTextReturn += vbCrLf & "</table>"
            Dim strFilaDetraccion As String = String.Empty
            If dblMontoDetraccion > 0 Then
                'Cargando el sub-total
                strFilaDetraccion += "<tr width=""100%"" align=""right"">" & vbCrLf
                strFilaDetraccion += "<td align=""right"" width=""85%"">" & strCampoCorreoTexto(96, "SUB TOTAL", True) & "</td>" & _
                                                      "<td align=""right"" width=""5%"">" & strCampoCorreoTexto(4, strCoMoneda, True) & "</td>" & _
                                                       "<td align=""right"" width=""10%"">" & strCampoCorreoTexto(10, Format(dblMontoTotal, "###,##0.00"), True) & "</td>" & vbCrLf
                strFilaDetraccion += "</tr>" & vbCrLf
                dblMontoTotal -= dblMontoDetraccion

                'Cargando la detracción
                strFilaDetraccion += "<tr width=""100%"" align=""right"">" & vbCrLf
                strFilaDetraccion += "<td align=""right"" width=""85%"">" & strCampoCorreoTexto(96, "DETRACCI&Oacute;N", True) & "</td>" & _
                                                      "<td align=""right"" width=""5%"">" & strCampoCorreoTexto(4, strCoMoneda, True) & "</td>" & _
                                                       "<td align=""right"" width=""10%"">" & strCampoCorreoTexto(10, Format(dblMontoDetraccion, "###,##0.00"), True) & "</td>" & vbCrLf
                strFilaDetraccion += "</tr>" & vbCrLf
            End If

            strTextReturn += "<table border=""0"" cellspacing=""0"" cellpaiding=""0"" width=""100%"">" & vbCrLf

            strTextReturn += strFilaDetraccion

            strTextReturn += "<tr width=""100%"" align=""right"">" & vbCrLf
            strTextReturn += "<td align=""right"" width=""85%"">" & strCampoCorreoTexto(96, "TOTAL", True) & "</td>" & _
                                                  "<td align=""right"" width=""5%"">" & strCampoCorreoTexto(4, strCoMoneda, True) & "</td>" & _
                                                   "<td align=""right"" width=""10%"">" & strCampoCorreoTexto(10, Format(dblMontoTotal, "###,##0.00"), True) & "</td>" & vbCrLf

            strTextReturn += "</tr>" & vbCrLf

            strTextReturn += vbCrLf & "</table>"

            strTextReturn += "<table border=""0"" cellspacing=""0"" cellpaiding=""0"" width=""100%"">" & vbCrLf
            strTextReturn += "<tr align=""right"" width=""100%"">" & vbCrLf
            strTextReturn += "<td align=""right""  width=""85%"">" & strCampoCorreoTexto(96, "SALDO PENDIENTE", True) & "</td>" & _
                              "<td align=""right""  width=""5%"">" & strCampoCorreoTexto(4, strCoMoneda, True) & "</td>" & _
                             "<td align=""right""  width=""10%"">" & strCampoCorreoTexto(10, Format(dblMontoPorPagar, "###,##0.00"), True) & "</td>" & vbCrLf
            strTextReturn += "</tr>" & vbCrLf
            strTextReturn += vbCrLf & "</table>"
            strTextReturn += vbCrLf & "<br />"
            Return strTextReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strCampoCorreoTexto(ByVal vbytTamanioColumna As Byte, ByVal vstrValor As String, _
                                        Optional ByVal vblnRightPosition As Boolean = False) As String
        Try
            Dim strValorRet As String = ""
            If vbytTamanioColumna <= vstrValor.Trim.Length Then
                strValorRet = vstrValor.Substring(0, vbytTamanioColumna)
            Else
                If Not vblnRightPosition Then
                    strValorRet = vstrValor.Trim & Space(vbytTamanioColumna - vstrValor.Trim.Length)
                Else
                    strValorRet = Space(vbytTamanioColumna - vstrValor.Trim.Length) & vstrValor
                End If
            End If
            Return strValorRet
        Catch ex As System.Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsIngreso_DebitMemoBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrIDDebitMemo As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            Return objADO.GetDataReader("INGRESO_DEBIT_MEMO_Sel_ListxIDDebitMemo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDebitMemoBN
    Inherits clsAdministracionBaseBN

    Public Function EnvioInvoicePaymentwall(ByVal vstrIDDebitMemo As String, ByVal vstrIDCliente As String, vstrIDContacto As String _
                                            , ByVal vstrAppKey As String, ByVal vstrAppSecret As String, vstrAppUrl As String) As String
        Dim slReturn As String = "OK"
        'Try
        '    Dim slEmail As String = String.Empty ''"pedro.mogollon@setours.com" ''String.Empty
        '    Dim objInvoicePw As New clsInvoicePwBE

        '    Dim objBN As New clsDebitMemoBN
        '    Dim drDM As SqlClient.SqlDataReader = objBN.ConsultarPk_InvoicePaymentwall(vstrIDDebitMemo)
        '    drDM.Read()
        '    If drDM.HasRows Then
        '        objInvoicePw.invoice_number = drDM("invoice_number")
        '        objInvoicePw.currency = drDM("currency")
        '        objInvoicePw.date = drDM("date")
        '        objInvoicePw.due_date = drDM("due_date")
        '    End If
        '    drDM.Close()

        '    Dim objCCBN As New clsClienteBN.clsContactoClienteBN
        '    Dim drCC As SqlClient.SqlDataReader = objCCBN.ConsultarPk_InvoicePaymentwall(vstrIDContacto, vstrIDCliente)
        '    Dim objContactInvoicePwBE As New clsInvoicePwBE.clsContactInvoicePwBE
        '    drCC.Read()
        '    If drCC.HasRows Then
        '        objContactInvoicePwBE.email = slEmail ''drCC("email")
        '        slEmail = objContactInvoicePwBE.email
        '        objContactInvoicePwBE.first_name = drCC("first_name")
        '        objContactInvoicePwBE.last_name = drCC("last_name")
        '        objContactInvoicePwBE.company_name = drCC("company_name")
        '        objContactInvoicePwBE.salutation = drCC("salutation")
        '    End If
        '    drCC.Close()
        '    Dim objContactInvoicePwLst As New List(Of clsInvoicePwBE.clsContactInvoicePwBE)
        '    objContactInvoicePwLst.Add(objContactInvoicePwBE)
        '    objInvoicePw.contacts = objContactInvoicePwLst

        '    Dim objDetDM As New clsDetalleDebitMemoBN
        '    Dim drItem As DataTable = objDetDM.ConsultarListxIDDebitMemo_InvoicePaymentwall(vstrIDDebitMemo)
        '    Dim objItemInvoicePwLst As New List(Of clsInvoicePwBE.clsItemInvoicePwBE)
        '    For Each Item As DataRow In drItem.Rows
        '        Dim objItemInvoicePw As New clsInvoicePwBE.clsItemInvoicePwBE
        '        objItemInvoicePw.quantity = Item("quantity")
        '        objItemInvoicePw.unit_cost = Item("unit_cost")
        '        objItemInvoicePw.currency = Item("currency")
        '        objItemInvoicePw.title = Item("title")
        '        objItemInvoicePw.tax = New clsInvoicePwBE.clsTaxInvoicePwBE With {.type = Item("typeTax"), .value = Single.Parse(Item("valueTax"))}
        '        objItemInvoicePwLst.Add(objItemInvoicePw)
        '    Next
        '    objInvoicePw.items = objItemInvoicePwLst

        '    objInvoicePw.key = vstrAppKey
        '    objInvoicePw.sign_version = 3

        '    Dim objDic As New Dictionary(Of String, String)
        '    With objDic
        '        .Add("key", vstrAppKey)
        '        ''.Add("uid", "")
        '        ''.Add("widget", "")
        '        .Add("email", slEmail)
        '        .Add("sign_version", 3)
        '    End With
        '    Dim slSignature As String = String.Empty
        '    slSignature = modTools.Paymentwall_Widget_calculateSignature(objDic, vstrAppSecret, 3)
        '    ''objDic.Add("sign", slSignature)
        '    objInvoicePw.sign = slSignature
        '    ''
        '    Dim objJ As New clsJsonInterfacePwBN()

        '    objJ.EnviarDatosRESTFulJson(objInvoicePw, vstrAppUrl, "POST")
        '    ''Return slReturn
        'Catch ex As Exception
        '    slReturn = ex.ToString()
        '    ''Throw
        'End Try
        Return slReturn
    End Function

    Public Overloads Function ConsultarList(ByVal vstrDescrCliente As String, ByVal vstrNumeroFileCorr As String, _
                                            ByVal vstrReferencia As String, ByVal vstrDescrEjecutivo As String, _
                                            ByVal vstrIDTipo As String, ByVal vstrIDEstado As String, _
                                            ByVal vstrIDPaisCliente As String, ByVal vdblTotalRango1 As Double, _
                                            ByVal vdblTotalRango2 As Double, _
                                            ByVal vdatFecVencimiento1 As Date, ByVal vdatFecVencimiento2 As Date, _
                                            Optional ByVal vintIDCab As Integer = 0, _
                                            Optional ByVal vblnParaFinanzas As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@DescCliente", vstrDescrCliente)
                .Add("@Numero", vstrNumeroFileCorr)
                .Add("@Referencia", vstrReferencia)
                .Add("@DescEjecutivo", vstrDescrEjecutivo)
                .Add("@Tipo", vstrIDTipo)
                .Add("@IDEstado", vstrIDEstado)
                .Add("@IDCab", vintIDCab)
                .Add("@IDPais", vstrIDPaisCliente)
                .Add("@TotalRango1", vdblTotalRango1)
                .Add("@TotalRango2", vdblTotalRango2)
                .Add("@RangoFecVencim1", vdatFecVencimiento1)
                .Add("@RangoFecVencim2", vdatFecVencimiento2)
            End With

            If Not vblnParaFinanzas Then
                Return objADO.GetDataTable(True, "DEBIT_MEMO_Sel_List", dc)
            Else
                Return objADO.GetDataTable(True, "DEBIT_MEMO_Sel_ListxFinanzas", dc)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarList_Rpt_NC(ByVal vdatFecha1 As Date, ByVal vdatFecha2 As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@Fecha1", vdatFecha1)
                .Add("@Fecha2", vdatFecha2)
            End With

            Return objADO.GetDataTable(True, "DEBIT_MEMO_Sel_Rpt_NC", dc)
          
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function Consultar_Motivos_NC_Cbo() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            

            Return objADO.GetDataTable(True, "MAMOTIVO_NC_Sel_Cbo", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk_InvoicePaymentwall(ByVal vstrIDDebitMemo As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            Return objADO.GetDataReader("DEBIT_MEMO_Sel_Pk_InvoicePaymentwall", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub Actualizar_InvoicePaymentwall(ByVal vstrIDDebitMemo As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            objADO.ExecuteSP("DEBIT_MEMO_Actualizar_InvoicePaymentwall", dc)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Overloads Function ConsultarPk(ByVal vstrIDDebitMemo As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            Return objADO.GetDataReader("DEBIT_MEMO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDatosCotizacion(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Dim strSql As String = "DEBIT_MEMO_Sel_DatosCotizacionxIDCab"
            Return objADO.GetDataReader(strSql, dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarListxDebitMemoResumen(ByVal vstrIDDebitMemoResumen As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemoResumen", vstrIDDebitMemoResumen)

            Return objADO.GetDataTable(True, "DEBIT_MEMO_SelxIDDebitMemoResumen", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDCLiente(ByVal vstrIDCliente As String, _
                                            ByVal vdatFechaRango1 As Date, ByVal vdatFechaRango2 As Date, _
                                            Optional ByVal vblnAsignacion As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCliente", vstrIDCliente)

            If Not vblnAsignacion Then
                dc.Add("@FechaInRangoDe", vdatFechaRango1)
                dc.Add("@FechaInRengoHasta", vdatFechaRango2)

                Return objADO.GetDataTable(True, "DEBIT_MEMO_SelxCliente", dc)
            Else
                Return objADO.GetDataTable(True, "DEBIT_MEMO_SelAsignacionxCLiente", dc)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarxID(ByVal vstrIDDebitMemo As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            Return objADO.GetDataReader("DEBIT_MEMO_SelxIDDebitMemo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GenerarPdfDebitMemoIndividual(ByVal vintIDCab As Integer, ByVal vstrIDDebitMemo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDDetMemo", vstrIDDebitMemo)

            Return objADO.GetDataTable(True, "DEBIT_MEMO_GenPDFIndividual", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GenerarPdfDebitMemoResumen(ByVal vstrIDDebitMemoResumen As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDetMemoResumen", vstrIDDebitMemoResumen)

            Return objADO.GetDataTable(True, "DEBIT_MEMO_GenPDFResumen", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarListxIDCab(ByVal vintIDcab As Integer, _
                                                  Optional ByVal vblnConsultaFacturacion As Boolean = False, _
                                                  Optional ByVal vblnFacturado As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDcab)

            If Not vblnConsultaFacturacion Then
                Return objADO.GetDataTable(True, "DEBIT_MEMO_Sel_ListxIDCab", dc)
            Else
                dc.Add("@FileFacturado", vblnFacturado)
                Return objADO.GetDataTable(True, "DEBIT_MEMO_Sel_ListFacturacionxIDCab", dc)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarSgteCorrelativo(ByVal vintIDCab As Integer, ByVal vstrTipoDebitMemo As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strCorrelativo As String = ""
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@Tipo", vstrTipoDebitMemo)
            dc.Add("@pCorrelativo", strCorrelativo)

            strCorrelativo = objADO.GetSPOutputValue("DEBIT_MEMO_SelSgteCorrelativo", dc)
            Return strCorrelativo.Trim
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistexFile(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pExiste", 0)

            Return objADO.ExecuteSPOutput("DEBIT_MEMO_Sel_ExistexFileOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function blnExistexFilexEstado(ByVal vintIDCab As Integer, ByVal vstrIDEstado As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDEstado", vstrIDEstado)
            dc.Add("@pExiste", 0)

            Return objADO.ExecuteSPOutput("DEBIT_MEMO_Sel_ExistsxEstadoOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRpt(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "DEBIT_MEMO_Sel_RptxIDCab", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptDebitMemosPorCobrar(ByVal vstrIDCliente As String, _
                                                              ByVal vstrIDPais As String, _
                                                              ByVal vdatFecVencIn As Date, _
                                                              ByVal vdatFecVencOut As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@IDPais", vstrIDPais)
                .Add("@FecVencimientoIn", vdatFecVencIn.ToShortDateString)
                .Add("@FecVencimientoOut", vdatFecVencOut.ToShortDateString)
            End With

            Return objADO.GetDataTable(True, "DEBIT_MEMO_RepDebitMemosPorCobrar", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarSbRptDebitMemosPorCobrar(ByVal vstrIDCliente As String, _
                                                                ByVal vstrIDPais As String, _
                                                                ByVal vdatFecVencIn As Date, _
                                                                ByVal vdatFecVencOut As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@IDPais", vstrIDPais)
                .Add("@FecVencimientoIn", vdatFecVencIn.ToShortDateString)
                .Add("@FecVencimientoOut", vdatFecVencOut.ToShortDateString)
            End With

            Return objADO.GetDataTable(True, "DEBIT_MEMO_SbRepDebitMemosPorCobrar", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptDebitMemoPeriodoPromedioCorto(ByVal vstrIDCliente As String, _
                                                     ByVal vstrIDPais As String, _
                                                     ByVal vdatFecVencIn As Date, _
                                                     ByVal vdatFecVencOut As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@IDPais", vstrIDPais)
                .Add("@FecVencimientoIn", vdatFecVencIn.ToShortDateString)
                .Add("@FecVencimientoOut", vdatFecVencOut.ToShortDateString)
            End With

            Return objADO.GetDataTable(True, "DEBIT_MEMO_RepPeriodoPromedioCobro", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptDebitMemoSbPeriodoPromedioCorto(ByVal vstrIDCliente As String, _
                                                                          ByVal vstrIDPais As String, _
                                                                          ByVal vdatFecVencIn As Date, _
                                                                          ByVal vdatFecVencOut As Date)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", vstrIDCliente)
                .Add("@IDPais", vstrIDPais)
                .Add("@FecVencimientoIn", vdatFecVencIn.ToShortDateString)
                .Add("@FecVencimientoOut", vdatFecVencOut.ToShortDateString)
            End With

            Return objADO.GetDataTable(True, "DEBIT_MEMO_SbRepPeriodoPromedioCobro", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnEsFacturado(ByVal vstrNuDebitMemo As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDebitMemo", vstrNuDebitMemo)
            dc.Add("@pFacturado", False)
            Return objADO.ExecuteSPOutput("DEBIT_MEMO_SelFacturadoOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoBN
    Inherits clsAdministracionBaseBN

    Public pstrCoProveedorSetLima As String
    Public pstrCoProveedorSetBuenosAires As String
    Public pstrCoProveedorSetSantiago As String
    Public pstrBuenosAires As String
    Public pstrSantiago As String
    Public pstrCoUbigeo_Oficina As String

    Public Overloads Function ConsultarList(ByVal vstrCliente As String, ByVal vstrReferencia As String, _
                                            ByVal vstrFile As String, ByVal vstrNumero As String, _
                                            ByVal vstrTipoDocumento As String, ByVal vstrSerie As String, _
                                            ByVal vstrStatus As String, ByVal vdatFechaEmisionInicio As Date, _
                                            ByVal vdatFechaEmisionFinal As Date, ByVal vstrCoUbigeo_Oficina As String, _
                                            ByVal vblnFlHistorico As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@DescCliente", vstrCliente)
                .Add("@Referencia", vstrReferencia)
                .Add("@IDFile", vstrFile)
                .Add("@TipoDocumento", vstrTipoDocumento)
                .Add("@Serie", vstrSerie)
                .Add("@Numero", vstrNumero)
                .Add("@CoEstado", vstrStatus)
                .Add("@FecEmisionInicio", vdatFechaEmisionInicio)
                .Add("@FecEmisionFinal", vdatFechaEmisionFinal)
                .Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
                .Add("@FlHistorico", vblnFlHistorico)
            End With
            Return objADO.GetDataTable(True, "DOCUMENTO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarListPDF(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "DOCUMENTO_Sel_ListPDF", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vstrNuDocumento As String, ByVal vstrIDTipoDoc As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDocum", vstrNuDocumento)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            Return objADO.GetDataReader("DOCUMENTO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function strTextoImpresion(ByVal vstrIDTipoDoc As String, ByVal vstrSerie As String, ByVal vintIDCab As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@CoSerie", vstrSerie)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pTxTextoImpresion", "")
            Return objADO.ExecuteSPOutput("Correlativo_SelTextoImpresion", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    'Public Function ConsultarDocumentosaGenerar(ByVal vintIDCab As Integer)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@IDCab", vintIDCab)
    '        Dim dtt As DataTable = objADO.GetDataTable(True, "RESERVAS_DET_SelxIDCabFacturac", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Public Function intDevuelveCorrelativo(ByVal vstrIDTipoDoc As String, ByVal vstrCoSerie As String) As Integer
        Dim dc As New Dictionary(Of String, String)
        Dim intCorrelativo As Integer = 0
        Try
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@CoSerie", vstrCoSerie)
            dc.Add("@pCorrelativo", 0)

            intCorrelativo = CInt(objADO.GetSPOutputValue("Correlativo_Sel_CorrelativoDocumentoOutPut", dc))
            Return intCorrelativo
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function lstInsertarDocumentosGenerados(ByVal vintIDCab As Integer, _
                                 ByVal BE As clsDocumentoBE, _
                                 ByVal vstrUserMod As String) As List(Of String)


        Dim objBT As New clsDocumentoBT With {.pstrSantiago = pstrSantiago, .pstrBuenosAires = pstrBuenosAires, _
                                                  .pstrCoProveedorSetLima = pstrCoProveedorSetLima, _
                                                  .pstrCoUbigeo_Oficina = pstrCoUbigeo_Oficina, _
                                                  .pstrCoProveedorSetBuenosAires = pstrCoProveedorSetBuenosAires, _
                                                  .pstrCoProveedorSetSantiago = pstrCoProveedorSetSantiago}

        Dim blnError As Boolean = False
        Dim ListaErrores As New List(Of String)
        For Each Item As clsDocumentoBE In BE.ListaDocumentos
            Item.ListaDocumentos = BE.ListaDocumentos
            ''PPMG20151202
            Item.ListaDetalleDocumentos = BE.ListaDetalleDocumentos

            Dim strError As String = objBT.strInsertarDocumentoGenerado(Item)

            If strError <> "" Then
                If strError.Substring(0, 4) = "SAP:" Then
                    Item.ErrorSAP = strError
                Else
                    Item.ErrorSetra = strError
                End If
                blnError = True
                ListaErrores.Add(Item.NuDocum & " " & strError)
            Else
                ListaErrores.Add(Item.NuDocum & " " & " se generó exitosamente.")
            End If
        Next

        If Not blnError Then
            If Not BE.ListaDebitMemos Is Nothing Then
                If BE.ListaDebitMemos.Count > 0 Then
                    Dim objDebitMemo As New clsDebitMemoBT
                    objDebitMemo.ActualizarFlagsFacturado(BE)
                End If
            End If

            If vintIDCab <> 0 Then
                objBT.ActualizarEstadoFacturacionFile(vintIDCab, "F", vstrUserMod)
                'Else 'ANTICIPOS 
                '    ActualizarEstadoFacturacionFile(vintIDCab, "M", vstrUserMod) ' FACTURADO PARCIAL

                'EliminarCruceDebitMemoDocumentos(vintIDCab)
                'InsertarCruceDebitMemoDocumentos(vintIDCab, vstrUserMod)
                objBT.EliminarCruceIngresosDocumentos(vintIDCab, vstrUserMod)
                objBT.InsertarCruceIngresosDocumentos(vintIDCab, vstrUserMod)

            End If
        Else

            If vintIDCab <> 0 Then

                Dim blnFullError As Boolean = True
                For Each strMensaje As String In ListaErrores
                    If InStr(strMensaje, "exitosamente") <> 0 Then
                        blnFullError = False
                        Exit For
                    End If
                Next
                If Not blnFullError Then
                    objBT.ActualizarEstadoFacturacionFile(vintIDCab, "M", vstrUserMod) 'Parcial
                End If
            End If

        End If

        Return ListaErrores

    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoDet_TextoBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrNuDocumento As String, ByVal vstrIDTipoDoc As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", vstrNuDocumento)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
            End With

            Return objADO.GetDataTable(True, "DOCUMENTO_DET_TEXTO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoDetBN
    Inherits clsAdministracionBaseBN
    Public Overloads Function ConsultarListPDF(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "DOCUMENTO_DET_Sel_ListPDF", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarList(ByVal vstrNuDocumento As String, ByVal vstrIDTipoDoc As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", vstrNuDocumento)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
            End With
            Return objADO.GetDataTable(True, "DOCUMENTO_DET_Sel_ListxNuDocumxTipoDoc", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRpt(ByVal vstrNuDocumento As String, ByVal vstrIDTipoDoc As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocum", vstrNuDocumento)
                .Add("@IDTipoDoc", vstrIDTipoDoc)
            End With

            Return objADO.GetDataTable(True, "DOCUMENTO_DET_Sel_List_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleDebitMemoBN
    Inherits clsAdministracionBaseBN
    Public Overloads Function ConsultarList(ByVal vstrIDDebitMemo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)

            Return objADO.GetDataTable(True, "DEBIT_MEMO_DET_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Function ConsultarListxIDDebitMemo(ByVal vstrIDDebitMemo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            Return objADO.GetDataTable(True, "DEBIT_MEMO_DET_SelxIDDebitMemo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Function ConsultarListxIDDebitMemo_InvoicePaymentwall(ByVal vstrIDDebitMemo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDebitMemo", vstrIDDebitMemo)
            Return objADO.GetDataTable(True, "DEBIT_MEMO_DET_Sel_List_InvoicePaymentwall", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsNotaVentaBN
    Inherits clsAdministracionBaseBN
    Public Overloads Function ConsultarList(ByVal vstrDescripCliente As String, ByVal vstrIDFile As String, _
                                            ByVal vstrReferencia As String, ByVal vstrDescripEjecutivo As String, _
                                            ByVal vstrIDEstado As String, Optional ByVal vintIDCab As Integer = 0) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@DescCliente", vstrDescripCliente)
                .Add("@Numero", vstrIDFile)
                .Add("@Referencia", vstrReferencia)
                .Add("@DescEjecutivo", vstrDescripEjecutivo)
                .Add("@IDEstado", vstrIDEstado)
                .Add("@IDCab", vintIDCab)
            End With

            Return objADO.GetDataTable(True, "NOTA_VENTA_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteNoAnulEnFile(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@pExiste", False)
            End With

            Return objADO.GetSPOutputValue("NOTA_VENTA_SelNoAnulOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteEnFile(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@pExiste", False)
            End With

            Return objADO.GetSPOutputValue("NOTA_VENTA_SelOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Function ConsultarPK(ByVal vstrIDNotaVenta As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNotaVenta", vstrIDNotaVenta)

            Return objADO.GetDataReader("NOTA_VENTA_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRpt(ByVal vstrIDNotaVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNotaVenta", vstrIDNotaVenta)

            Return objADO.GetDataTable(True, "NOTA_VENTA_RptGeneralDebitMemo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleNotaVentaBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarListxIDNotaVenta(ByVal vstrIDNotaVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNotaVenta", vstrIDNotaVenta)

            Return objADO.GetDataTable(True, "NOTA_VENTA_DET_Sel_ListxIDNotaVenta", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptNotaVenta(ByVal vstrIDNotaVenta As String, Optional ByVal vblnGastosAdministrativos As Boolean = False) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDNotaVenta", vstrIDNotaVenta)

            If Not vblnGastosAdministrativos Then
                Return objADO.GetDataTable(True, "NOTA_VENTA_DET_RptCosto_NoGastoAdm", dc)
            Else
                Return objADO.GetDataTable(True, "NOTA_VENTA_DET_RptCosto_GastoAdm", dc)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPreLiquidacionBN
    Inherits clsAdministracionBaseBN

    Public Function ConsultarFilesaFacturarList(ByVal vstrEstadoFacturac As String, _
                                                ByVal vstrIDFile As String, _
                                                ByVal vstrTitulo As String, _
                                                ByVal vdatFechaOutIni As Date, _
                                                ByVal vdatFechaOutFin As Date, _
                                                ByVal vblnSoloAjustePrecio As Boolean, _
                                                ByVal vstrCoUbigeoOficina As String, _
                                                ByVal vblnFilesHistoricos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@EstadoFacturac", vstrEstadoFacturac)
            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@Titulo", vstrTitulo)
            dc.Add("@FechaOutIni", Format(vdatFechaOutIni, "dd/MM/yyyy"))
            dc.Add("@FechaOutFin", Format(vdatFechaOutFin, "dd/MM/yyyy"))
            dc.Add("@SoloAjustePrecio", vblnSoloAjustePrecio)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeoOficina)
            dc.Add("@FlHistorico", vblnFilesHistoricos)

            Return objADO.GetDataTable(True, "COTICAB_SelFacturac2", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPreLiquidacionVoucher(ByVal vintIDCab As Integer, ByVal vstrCoUbigeo_Oficina As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            Return objADO.GetDataTable(True, "OPERACIONES_SelVouchersPreLiquidacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPreLiquidacionOrdenServicio(ByVal vintIDCab As Integer, ByVal vstrCoUbigeo_Oficina As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            Return objADO.GetDataTable(True, "OPERACIONES_SelOrdenesServicioPreLiquidacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPreLiquidacionBusesVuelos(ByVal vintIDCab As Integer, ByVal vstrCoUbigeo_Oficina As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            Return objADO.GetDataTable(True, "COTIVUELOS_SelBusesVuelosPreLiquidacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPreLiquidacionEntradasPresupuestos(ByVal vintIDCab As Integer, ByVal vstrCoUbigeo_Oficina As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            Return objADO.GetDataTable(True, "OPERACIONES_SelEntradasPresupuestosPreLiquidacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarPreLiquidacionInterno(ByVal vintIDCab As Integer, ByVal vstrCoUbigeo_Oficina As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            Return objADO.GetDataTable(True, "OPERACIONES_SelTraladistasyMovilidadPreLiquidacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarDocumentosPreLiquidacion(ByVal vstrCoUbigeo_Oficina As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            Return objADO.GetDataTable(True, "MATIPODOC_SelPreLiquidacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveCorrelativoDocumentos(ByVal vstrIDTipoDoc As String, ByVal vstrCoSerie As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Tabla", "DOCUMENTO")
            dc.Add("@Upd", 0)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@CoSerie", vstrCoSerie)
            dc.Add("@Tamanio", 10)
            dc.Add("@pCorrelativo", 0)

            Return objADO.ExecuteSPOutput("Correlativo_SelOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveCorrelativoDocumentos_Tmp(ByVal vstrIDTipoDoc As String, ByVal vstrCoSerie As String, ByVal vbytAumento As Byte) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Tabla", "DOCUMENTO")
            dc.Add("@Upd", 0)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)
            dc.Add("@CoSerie", vstrCoSerie)
            dc.Add("@Tamanio", 10)
            dc.Add("@Aumento", vbytAumento)
            dc.Add("@pCorrelativo", 0)

            Return objADO.ExecuteSPOutput("Correlativo_SelOutput_Tmp", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarAnticipos(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataReader("ASIGNACION_SelAnticipos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarAjustePrecio(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataReader("ASIGNACION_SelAjustePrecio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarDocumentoGenerado(ByVal vintIDCab As Integer, ByVal vstrIDTipoDoc As String) As SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDTipoDoc", vstrIDTipoDoc)

            Return objADO.GetDataReader("DOCUMENTO_SelxIDCabTipoDoc", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function ConsultarRpt(ByVal vstrIDCliente As String, ByVal vstrIDFile As String, _
                                 ByVal vdatFechaOutInicial As Date, ByVal vdatFechaOutFinal As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCliente", vstrIDCliente)
            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@FechaOutInicial", vdatFechaOutInicial)
            dc.Add("@FechaOutFinal", vdatFechaOutFinal)

            Return objADO.GetDataTable(True, "PRELIQUIDACION_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptLiquidacion(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "ReporteLiquidacionFile", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarRptLiquidacionDet(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "ReporteLiquidacionFileDetalle", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetallePreLiquidacionBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrIDNotaVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNotaVenta", vstrIDNotaVenta)

            Return objADO.GetDataTable(True, "PRE_LIQUI_DET_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptNotaVenta(ByVal vstrIDNotaVenta As String, Optional ByVal vblnGastosAdministrativos As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNotaVenta", vstrIDNotaVenta)

            If Not vblnGastosAdministrativos Then
                Return objADO.GetDataTable(True, "PRE_LIQUI_DET_RptCostosNoGastoAdm", dc)
            Else
                Return objADO.GetDataTable(True, "PRE_LIQUI_DET_RptCosto_GastoAdm", dc)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteEnFile(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@pExiste", False)
            End With

            Return objADO.GetSPOutputValue("PRE_LIQUI_DET_SelOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPresupuesto_SobreBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vintNuPreSob As Integer, ByVal vstrIDFile As String, ByVal vstrIDProveedor As String, _
                                            ByVal vstrDescPrgGuiaTC As String, ByVal vstrCoEstado As String, _
                                            ByVal vdatFechaIni As Date, ByVal vdatFechaOut As Date, ByVal vstrCategoria As String, _
                                            Optional ByVal vblnModificado As Boolean = False, Optional ByVal vblnFlHistorico As Boolean = False) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDFile", vstrIDFile)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@DescGuiaTC", vstrDescPrgGuiaTC)
                .Add("@CoEstado", vstrCoEstado)
                .Add("@FeDetRango1", vdatFechaIni)
                .Add("@FeDetRango2", vdatFechaOut)
                .Add("@Categoria", vstrCategoria)
                .Add("@Modificado", vblnModificado)
                .Add("@NuPreSob", vintNuPreSob)
                .Add("@FlHistorico", vblnFlHistorico)
            End With

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptGeneral(ByVal vstrIDFile As String, ByVal vstrIDProveedor As String, _
                                            ByVal vstrDescPrgGuiaTC As String, ByVal vstrCoEstado As String, _
                                            ByVal vdatFechaIni As Date, ByVal vdatFechaOut As Date, ByVal vstrCategoria As String) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDFile", vstrIDFile)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@DescGuiaTC", vstrDescPrgGuiaTC)
                .Add("@CoEstado", vstrCoEstado)
                .Add("@FeDetRango1", vdatFechaIni)
                .Add("@FeDetRango2", vdatFechaOut)
                .Add("@Categoria", vstrCategoria)
            End With

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_RptGeneral", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptConsolidado_Cusco(ByVal vintIDCAB As Integer, ByVal vdatFechaIni As String, ByVal vdatFechaOut As String) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCAB", vintIDCAB)
                .Add("@FeDetRango1", vdatFechaIni)
                .Add("@FeDetRango2", vdatFechaOut)
            End With

            Return objADO.GetDataTable(True, "PRESUPUESTO_Rpt_Consolidado_Cusco", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptConsolidado_Cusco_DS(ByVal vintIDCAB As Integer, ByVal vdatFechaIni As String, ByVal vdatFechaOut As String) As DataSet
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            dc.Add("@FeDetRango1", vdatFechaIni)
            dc.Add("@FeDetRango2", vdatFechaOut)
            Return objADO.GetDataSet2("Consolidado_Cusco", True, "PRESUPUESTO_Rpt_Consolidado_Cusco_Detalle", dc)
            '("ReporteExcelVentasReceptivo", "", "", dc) ' colocar nombre de procedure que devuelva la data del año anterior
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vintNuPreSob As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)

            Return objADO.GetDataReader("PRESUPUESTO_SOBRE_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarxIDCabxIDProveedorxIDGuia(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                                 ByVal vstrCoPrvGuia As String, ByVal vintIDPax As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDcab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@CoPrvGuia", vstrCoPrvGuia)
            dc.Add("@IDPax", vintIDPax)

            Return objADO.GetDataReader("PRESUPUESTO_SOBRE_SelxIDCabxIDProveedorxIDProvGuia", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRpt_PlanillaGastosMovilidad(ByVal vintIDCab As Integer, _
                                                                   ByVal vstrIDProveedorGuia As String, _
                                                                   ByVal vstrIDProveedor As String, _
                                                                   ByVal vintIDPax As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProvGuia", vstrIDProveedorGuia)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDPax", vintIDPax)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_Sel_RptPlanGastosMovilidad", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function Consultar_Correlativo_FondoFijo(ByVal vintNuPreSob As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_Sel_Correlativo_FondoFijo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strConsultar_Correlativo_FondoFijo(ByVal vstrCoProveedor As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@pNuCodigo_ER", 0)

            Return objADO.ExecuteSPOutput("PRESUPUESTO_SOBRE_Sel_Correlativo_FondoFijo_Output", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function Consultar_FondoFijo_ER(ByVal vintNuPreSob As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_Sel_FondoFijo_ER", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptIngresoMuseos(ByVal vintNuPreSob As Integer, _
                                                        ByVal vstrIDProveedor As String, _
                                                        ByVal vintIDPax As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDPax", vintIDPax)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_DET_Sel_RptEntradasMuseos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRpt(ByVal vintNuPreSob As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            'dc.Add("@IDCab", vintIDCab)
            'dc.Add("@IDProveedor", vstrIDProveedor)
            'dc.Add("@IDProveedorGuia", vstrIDProveedorGuia)
            'dc.Add("@IDPax", vintIDPax)
            dc.Add("@NuPreSob", vintNuPreSob)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_sel_ListRpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnActivarGeneracion(ByVal vintIDCab As Integer, _
                                           ByVal vstrIDProveedor As String) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@pCoEstado", "")

            Dim strCoEstado As String = objADO.ExecuteSPOutput("PRESUPUESTO_SOBRE_SelxProveedorOutput", dc)
            Dim blnReturn As Boolean = True
            If strCoEstado = "EN" Or strCoEstado = "EJ" Or strCoEstado = "AN" Then
                blnReturn = False
            End If

            Return blnReturn

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDCabxIDProveedor(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                    ByVal vblnEsTourConductor As Boolean) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@EsTourConductor", vblnEsTourConductor)

            Return objADO.GetDataReader("PRESUPUESTO_SOBRE_SelxIDCabxIDProveedor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strUltimoEstado(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, ByVal vblnEsTourConductor As Boolean) As String
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@TourConductor", vblnEsTourConductor)
            dc.Add("@pCoEstado", "")

            Return objADO.ExecuteSPOutput("PRESUPUESTO_SOBRE_SelUltimoEstado", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTickets() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_SelxTicket", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTickets(ByVal vintNuDocumProv As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDocumProv", vintNuDocumProv)
            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_SelTicketxNuDocumProv", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPresupuesto_Sobre_DetBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vintNuPreSob As Integer, ByVal vstrCoPrvPrg As String, _
                                            ByVal vintIDPax As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuPreSob", vintNuPreSob)
                .Add("@CoPrvPrg", vstrCoPrvPrg)
                .Add("@IDPax", vintIDPax)
            End With

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_DET_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarxIDPresupuesto(ByVal vintNuPreSob As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_DET_SelxNuPreSob", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptPresupuestoPropina(ByVal vintNuPreSob As Integer, ByVal vstrCoPrvPrg As String, _
                                                             ByVal vintIDPax As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@CoPrvPrg", vstrCoPrvPrg)
            dc.Add("@IDPax", vintIDPax)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_DET_Sel_ListRpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptPlanillaxDetalle(ByVal vintNuPreSob As Integer, ByVal vintNuPreDetSob As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDetPSo", vintNuPreDetSob)
            dc.Add("@NuPreSob", vintNuPreSob)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_Sel_RptPlanGastosMovilidadxDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptTicketxDetalle(ByVal vintNuPreSob As Integer, ByVal vintNuPreDetSob As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDetPSo", vintNuPreDetSob)
            dc.Add("@NuPreSob", vintNuPreSob)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_DET_Sel_ListRptxDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarRptEntradasMuseoxDetalle(ByVal vintNuPreSob As String, ByVal vintNuPreDetSob As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPreSob", vintNuPreSob)
            dc.Add("@NuDetPreSob", vintNuPreDetSob)

            Return objADO.GetDataTable(True, "PRESUPUESTO_SOBRE_DET_Sel_RptEntradasMuseosxDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsStockTicketEntradasIngresosBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrIDServicio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", vstrIDServicio)
            Return objADO.GetDataTable(True, "STOCK_TICKET_ENTRADAS_INGRESOS_SelxIDServicio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarQtUtilizada(ByVal vintIDCab As Integer, ByVal vstrCoServicio As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoServicio", vstrCoServicio)

            Return objADO.GetDataReader("COTICAB_STOCK_TICKET_ENTRADAS_INGRESOS_SelQtUtilizada", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strNroOperacionDocumentoCompraStock(ByVal strNroDocSus As String, ByVal strIDTipoDocSus As String, ByVal strNuOperBan As String) As String
        'ByVal vstrIDServicio As String

        'Dim dc As New Dictionary(Of String, String)
        Try
            '    Dim strNroDocSus As String = "", strIDTipoDocSus As String = "", strNuOperBan As String = ""
            '    dc.Add("@IDServicio", vstrIDServicio)

            '    Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("STOCK_TICKET_ENTRADAS_INGRESOS_SelNroDocOperSustento", dc)
            '        dr.Read()
            '        If dr.HasRows Then
            '            strNroDocSus = dr("NroDocSus")
            '            strIDTipoDocSus = dr("IDTipoDocSus")
            '            strNuOperBan = dr("NuOperBan")
            '        End If
            '        dr.Close()
            '    End Using

            If strNroDocSus <> "" Then
                Return strIDTipoDocSus & "|" & strNroDocSus
            Else
                Return strNuOperBan
            End If

        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsStockTicketEntradasBN
    Inherits clsAdministracionBaseBN

    Public Overloads Function ConsultarList(ByVal vstrDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Descripcion", vstrDescripcion)
            Return objADO.GetDataTable(True, "STOCK_TICKET_ENTRADAS_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPK(ByVal vstrIDServicio As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio", vstrIDServicio)

            Return objADO.GetDataReader("STOCK_TICKET_ENTRADAS_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function blnExistexPK(ByVal vstrIDServicio As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Dim blnExiste As Boolean = False
        Try
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@pExiste", False)

            blnExiste = objADO.GetSPOutputValue("STOCK_TICKET_ENTRADAS_SelExiste", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strStockEntradas(ByVal vListEntradas As List(Of clsPresupuesto_Sobre_DetBE)) As String
        Try
            For Each Item As clsPresupuesto_Sobre_DetBE In vListEntradas
                If Item.CoPago = "STK" Then
                    If Item.QtPax + Item.QtPaxLib > intStockEntrada(Item.IDServicio_Det_V_Cot) Then
                        Return Item.TxServicio
                    End If
                End If
            Next
            Return ""
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function intStockEntrada(ByVal vintIDServicio_Det As Integer) As Int16
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicio_Det_V_Cot", vintIDServicio_Det)
                .Add("@pQtStkEnt", 0)
            End With

            Return objADO.GetSPOutputValue("STOCK_TICKET_ENTRADAS_SelOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

