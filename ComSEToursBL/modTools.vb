﻿Imports System.Security.Cryptography
Imports System.Text
Imports ComSEToursBE2
Module modTools
    'Public Structure stReporteFacturacionContabilidadTexto
    '    Dim NuCuenta As String
    '    Dim Periodo As String
    '    Dim SubDiario As String
    '    Dim NroRegistro As String
    '    Dim FecRegistro As Date
    '    Dim TipoCliente As String
    '    Dim CodClienteStarSoft As String
    '    Dim CodTipoDocStarSoft As String
    '    Dim NuDocum As String
    '    Dim FecVencim As Date
    '    Dim FecEmision As Date
    '    Dim IDTipoDocVinc As String
    '    Dim NuDocumVinc As String
    '    Dim Igv As Single
    '    Dim ImporteTotal As Double
    '    Dim VTA As String
    '    Dim TipoCambio As Single
    '    Dim Tipo_NuDocum As String
    '    Dim TextoDoc As String
    '    Dim Anulado As Char
    '    Dim DebeHaber As Char
    '    Dim RazonSocial As String
    '    Dim CentroCosto As String
    '    Dim FecEmisionVinc As Date
    '    Dim Exportacion As Char
    '    Dim IDFile As String
    'End Structure
    Public gintCoCargoSupReserva As Integer = 56

    Public Function strDevuelveCadenaOcultaDerecha(ByVal vstrCadena As String) As String
        Dim strCadenaOculta As String = ""
        If InStr(vstrCadena, "|") > 0 Then
            strCadenaOculta = Mid(vstrCadena, InStr(vstrCadena, "|") + 1)
        End If

        Return strCadenaOculta.Trim
    End Function
    Public Function strDevuelveCadenaNoOcultaIzquierda(ByVal vstrCadena As String) As String
        Dim strCadenaNoOculta As String = ""
        If InStr(vstrCadena, "|") > 0 Then
            strCadenaNoOculta = vstrCadena.Substring(0, InStr(vstrCadena, "|") - 1)
        End If

        Return strCadenaNoOculta.Trim
    End Function
    Public Function dtFiltrarDataTable(ByVal vDataTable As DataTable, ByVal psFiltro As String, Optional ByVal psOrder As String = "") As DataTable
        Dim loRows As DataRow()
        Dim dtNuevoDataTable As DataTable
        dtNuevoDataTable = vDataTable.Clone()
        If psOrder = "" Then
            loRows = vDataTable.Select(psFiltro)
        Else
            loRows = vDataTable.Select(psFiltro, psOrder)
        End If
        For Each ldrRow As DataRow In loRows
            dtNuevoDataTable.ImportRow(ldrRow)
        Next
        Return dtNuevoDataTable
    End Function
    Public Function strDevuelveFormatoNombreCorreo(ByVal vstrNombre As String, ByVal vstrCorreo As String)
        Dim strReturn As String = ""
        Dim chrAr() As Char = vstrNombre.ToCharArray
        For id As Byte = 0 To chrAr.Length - 1
            If id = 0 Then
                strReturn += chrAr(id).ToString.ToUpper()
            Else
                If chrAr(id).ToString = " " Then
                    strReturn += chrAr(id) & chrAr(id + 1).ToString.ToUpper
                    id += 1
                Else
                    strReturn += chrAr(id)
                End If
            End If
        Next
        strReturn += " - SETOURS S.A.[" & vstrCorreo.ToLower.Trim & "]"
        Return strReturn
    End Function

    Public Function strDevuelveCabezeraHTMLReservas(ByVal vblnTipoProvSelHotel As Boolean, Optional ByVal vstrIDTipoProv As String = "", Optional ByVal vblnConAlojamiento As Boolean = False) As String
        Dim strReturn As String = ""
        strReturn += "<table class='nuevoEstilo1'>" & vbCrLf
        strReturn += "<tr>" & vbCrLf
        If vblnTipoProvSelHotel Then
            strReturn += "<td class='style2'>IN</td>" & vbCrLf
            strReturn += "<td class='style3'>OUT</td>" & vbCrLf
            strReturn += "<td class='style4' align='center'>N.Hab</td>" & vbCrLf
            strReturn += "<td class='style5'>Detalle</td>" & vbCrLf
            strReturn += "<td class='style5'>Cod. Tarifa</td>" & vbCrLf

            If (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                'strReturn += "<td class='style10' align='center'>Típo</td>" & vbCrLf
                strReturn += "<td class='style5'>Tipo</td>" & vbCrLf
            End If

            strReturn += "<td class='style6' align='center'>Pax</td>" & vbCrLf
            If Not (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                strReturn += "<td class='style10' align='center'>Desayuno</td>" & vbCrLf
            End If

            If (vstrIDTipoProv = "003" And vblnConAlojamiento) Then
                'strReturn += "<td class='style10' align='center'>Típo</td>" & vbCrLf
                strReturn += "<td class='style7'>Id.</td>" & vbCrLf
            End If

        Else
            strReturn += "<td class='style2'>Fecha</td>" & vbCrLf
            strReturn += "<td class='style15'>Hora</td>" & vbCrLf
            strReturn += "<td class='style5'>Detalle de Servicio</td>" & vbCrLf
            strReturn += "<td class='style5'>Cod. Tarifa</td>" & vbCrLf
            strReturn += "<td class='style10' align='center'>Tipo</td>" & vbCrLf
            strReturn += "<td class='style10' align='center'>Pax</td>" & vbCrLf
            strReturn += "<td class='style7'>Id.</td>" & vbCrLf
        End If
        strReturn += "</tr>" & vbCrLf
        Return strReturn
    End Function

    Public Function strFechaTextoDDMMYYYY(ByVal vdatFecha As Date) As String
        'Return Year(vdatFecha).ToString & Format(Month(vdatFecha), "00").ToString & Format(DatePart(DateInterval.Day, vdatFecha), "00")
        If vdatFecha.Year <> 1 And vdatFecha.Year <> 1900 Then
            Return Format(vdatFecha.Day, "00") & "/" & Format(vdatFecha.Month, "00").ToString & "/" & vdatFecha.Year.ToString
        Else
            Return ""
        End If

    End Function

    Public Function drFilaReporteFacturacionContabilidadTexto(ByVal vItemRep As  _
            clsReporteFacturacionContabilidadBE) As DataRow
        Try
            Dim dttST As New DataTable("STReport")

            For Each Prop As Object In vItemRep.GetType.GetProperties
                dttST.Columns.Add(Prop.name.ToString)
            Next

            dttST.Rows.Add()
            For Each Prop As Object In vItemRep.GetType.GetProperties
                For Each dc As DataColumn In dttST.Columns
                    If Prop.name.ToString = dc.ColumnName.ToString Then
                        dttST.Rows(0)(dc) = vItemRep.GetType.GetProperty(Prop.name.ToString).GetValue(vItemRep, Nothing).ToString
                        Exit For
                    End If
                Next
            Next


            'NewRow("") = strn
            'dttReturn.Rows.Add(NewRow)

            Return dttST.Rows(0)
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''/**
    ''  * Build signature for the widget specified
    ''  *
    ''  * @param Dictionary<string, string> parameters
    ''  * @param string secret Paymentwall Secret Key
    ''  * @param int version Paymentwall Signature Version
    ''  * @return string
    ''  */
    Public Function Paymentwall_Widget_calculateSignature(ByVal vdicParameters As Dictionary(Of String, String), ByVal vstrSecret As String, ByVal vintVersion As Integer) As String
        Try
            Dim baseString As String = String.Empty
            If vintVersion = 1 Then ''SIGNATURE_VERSION_1 = 1
                ''TODO: throw exception if no uid parameter is present
                If (Not vdicParameters("uid") Is Nothing) Then
                    baseString += vdicParameters("uid")
                Else
                    baseString += vstrSecret
                    Return Paymentwall_Widget_getHash(baseString, "md5")
                End If
            Else
                vdicParameters = vdicParameters.OrderBy(Function(d) d.Key, StringComparer.Ordinal).ToDictionary(Function(d) d.Key, Function(d) d.Key)

                For Each param As KeyValuePair(Of String, String) In vdicParameters
                    baseString += param.Key + "=" + param.Value
                Next
                baseString += vstrSecret

                If vintVersion = 2 Then ''SIGNATURE_VERSION_2 = 2
                    Return Paymentwall_Widget_getHash(baseString, "md5")
                Else
                    ''SIGNATURE_VERSION_2 = 3
                    Return Paymentwall_Widget_getHash(baseString, "sha256")
                End If

            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    ''/*
    ''     * Generate a hased string
    ''     * 
    ''     * @param string inputString The string to be hased
    ''     * @param string algorithm The hash algorithm, e.g. md5, sha256
    ''     * @return string hashed string
    ''     */
    Private Function Paymentwall_Widget_getHash(ByVal vstrinputString As String, ByVal vstralgorithm As String) As String
        Try
            Dim alg As HashAlgorithm = Nothing
            If vstralgorithm = "md5" Then
                alg = MD5.Create()
            ElseIf vstralgorithm = "sha256" Then
                alg = SHA256.Create()
            End If
            Dim hash As Byte() = alg.ComputeHash(Encoding.UTF8.GetBytes(vstrinputString))
            Dim sb As StringBuilder = New StringBuilder()
            For i As Integer = 0 To hash.Length - 1
                sb.Append(hash(i).ToString("X2"))
            Next

            Return sb.ToString().ToLower()
        Catch ex As Exception
            Throw
        End Try
    End Function
End Module
