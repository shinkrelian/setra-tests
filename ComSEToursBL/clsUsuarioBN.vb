﻿Imports ComSEToursBE2
Imports ComDataAccessD2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsUsuarioBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN
    Public Function blnUsuario_Sel_Id(ByVal vstrIDUsuario As String) As Boolean
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@Usuario", vstrIDUsuario)
            dc.Add("@pExiste", "")

            Return objADO.GetSPOutputValue("MAUSUARIOS_Sel_IdOutput", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function strRetornarPassword(ByVal vstrIDUsuario As String) As String
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@Usuario", vstrIDUsuario)
            'dc.Add("@Password", vstrPassword)
            dc.Add("@pPassword", "")

            Return objADO.GetSPOutputValue("MAUSUARIOS_Sel_IdPasswordOutput", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarListEmpresa(ByVal vblnTodos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Todos", vblnTodos)

            Return objADO.GetDataTable(True, "MAEMPRESA_Sel_Cbo_Lst", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarListEjecOperCusco() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "MAUSUARIOS_SelEjecOperCusco", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarListCargoxEmpresa(ByVal vblnTodos As Boolean, _
                                               ByVal vbytCoEmpresa As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Todos", vblnTodos)
            dc.Add("@CoEmpresa", vbytCoEmpresa)

            Return objADO.GetDataTable(True, "RH_Cargo_Sel_Cbo_Lst", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarList(ByVal vstrNombre As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@Nombre", vstrNombre)

            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function
    Public Function ConsultarPk(ByVal vstrIDUsuario As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDUsuario", vstrIDUsuario)

        Dim strSql As String = "MAUSUARIOS_Sel_Pk"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function Consultar_Detalle_Usuario(ByVal vstrIDUsuario As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDUsuario", vstrIDUsuario)

        Dim strSql As String = "MAUSUARIOS_Sel_ListxIDUsuario"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function Consultar_Coticab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Dim strSql As String = "MAUSUARIOS_Sel_Coticab"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function ConsultarCbo_Supervisores() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_Cbo_Supervisores", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarxUsuario(ByVal vstrUsuario As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@Usuario", vstrUsuario)

        Dim strSql As String = "MAUSUARIOS_Sel_xUsuario"
        Return objADO.GetDataReader(strSql, dc)

    End Function
    Public Function strDevuelveUsuarioxCorreo(ByVal vstrCorreo As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strIDUSuario As String = ""
            dc.Add("@Correo", vstrCorreo)
            dc.Add("@pIDUsuario", "")
            strIDUSuario = objADO.GetSPOutputValue("MAUSUARIOS_SelxCorreo", dc)
            Return strIDUSuario
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarCboPrg(ByVal vblnTodos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@bTodos", vblnTodos)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboPrg", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@bTodos", vblnTodos)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCbo_Ventas(ByVal vblnDescTodos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@bTodos", vblnDescTodos)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_Cbo_UsVentasAsig", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnUsuario_Sel_Profile(ByVal vstrProfile As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExiste As Boolean = False
            dc.Add("@NameProfile", vstrProfile)
            dc.Add("@pProfileExists", blnExiste)

            blnExiste = objADO.GetSPOutputValue("MAUSUARIOS_SelProfileOutput", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnUsuario_Sel_Account(ByVal vstrAccount As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExiste As Boolean = False
            dc.Add("@Account", vstrAccount)
            dc.Add("@pAccountExists", blnExiste)

            blnExiste = objADO.GetSPOutputValue("MAUSUARIOS_SelAccountOutput", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxNivel(ByVal vstrNivel As String, ByVal vblnTodos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Nivel", vstrNivel)
            dc.Add("@bTodos", vblnTodos)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxNivel", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxNivel_Activos(ByVal vstrNivel As String, ByVal vblnTodos As Boolean, Optional ByVal vstrActivo As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Nivel", vstrNivel)
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@Activo", vstrActivo)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxNivel_Activos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarUsuarioxNivel(ByVal vstrNivel As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Nivel", vstrNivel)

            Return objADO.GetDataTable(True, "MAUSUARIOS_SelxNivel", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxNivelAsignacion(ByVal vstrNivel As String, ByVal vblnFilaNula As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Nivel", vstrNivel)
            dc.Add("@bFilaNula", vblnFilaNula)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxNivelAsignacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxNivelAsignacion_Activos(ByVal vstrNivel As String, ByVal vblnFilaNula As Boolean, Optional ByVal vstrActivos As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Nivel", vstrNivel)
            dc.Add("@bFilaNula", vblnFilaNula)
            dc.Add("@Activo", vstrActivos)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxNivelAsignacion_Activos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxNivelAsignacion_Activos_Ubigeo(ByVal vstrNivel As String, ByVal vblnFilaNula As Boolean, Optional ByVal vstrActivos As String = "", Optional ByVal vstrIDUbigeo As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Nivel", vstrNivel)
            dc.Add("@bFilaNula", vblnFilaNula)
            dc.Add("@Activo", vstrActivos)
            dc.Add("@IDUbigeo", vstrIDUbigeo)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxNivelAsignacion_Activos_Ubigeo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxArea(ByVal vstrArea As String, ByVal vblnGerencia As Boolean, _
                                      Optional ByVal vblnTodos As Boolean = False, _
                                      Optional ByVal vblnDato As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@DescArea", vstrArea)
            dc.Add("@bGerencial", vblnGerencia)
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@bDato", vblnDato)

            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxArea", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxArea_Activos(ByVal vstrArea As String, ByVal vblnGerencia As Boolean, _
                                      Optional ByVal vblnTodos As Boolean = False, _
                                      Optional ByVal vblnDato As Boolean = False, _
                                     Optional ByVal vstrActivo As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@DescArea", vstrArea)
            dc.Add("@bGerencial", vblnGerencia)
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@bDato", vblnDato)
            dc.Add("@Activo", vstrActivo)

            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxArea_Activos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function ConsultarCboxOpcion(ByVal vstrNombreOpcion As String, ByVal vblnTodos As Boolean)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@DescOpcion", vstrNombreOpcion)
            dc.Add("@bTodos", vblnTodos)

            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxOpcion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboxOpcion_Activos(ByVal vstrNombreOpcion As String, ByVal vblnTodos As Boolean, Optional ByVal vstrActivo As String = "")
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@DescOpcion", vstrNombreOpcion)
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@Activo", vstrActivo)

            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_CboxOpcion_Activos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDGVxOpcion_Activos(ByVal vstrNombreOpcion As String, ByVal vblnTodos As Boolean, Optional ByVal vblnActivo As Boolean = False)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@DescOpcion", vstrNombreOpcion)
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@FlTrabajador_Activo", vblnActivo)

            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_DGVxOpcion_Activos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarUsAmadeus() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_UsAMAUDEUS", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@ID", vstrID)
            dc.Add("@Descripcion", vstrDescripcion)


            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_Lvw", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function


    Public Function ConsultarNivelCbo() As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "MANIVELUSUARIO_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptUsuarios(ByVal vstrNombre As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Nombre", vstrNombre)
            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRpt(ByVal vstrIDUsuario As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDUsuario", vstrIDUsuario)
            Return objADO.GetDataTable(True, "MAUSUARIOSOPC_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptNiveles(ByVal vstrIDNivel As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNivel", vstrIDNivel)
            Return objADO.GetDataTable(True, "MANIVELOPC_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function ConsultarSbRptNiveles(ByVal vstrIDNivel As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDNivel", vstrIDNivel)
            Return objADO.GetDataTable(True, "MANIVELOPC_Sel_SbRpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCorreosxUsuario(ByVal vstrIDUsuario As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDUsuario", vstrIDUsuario)

            Return objADO.GetDataTable(True, "MACORREOSUSUARIO_SelxUsuario", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function
    Public Function ConsultarxUbigeo(ByVal vstrIDUbigeo As String, _
                                     ByVal vstrIDArea As String, _
                                     ByVal vstrNivel As String, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDUbigeo", vstrIDUbigeo)
            dc.Add("@IDArea", vstrIDArea)
            dc.Add("@Nivel", vstrNivel)
            dc.Add("@CoPais", vstrCoPais)

            Return objADO.GetDataTable(True, "MAUSUARIOS_Sel_xIDUbigeo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

#Region "Métodos No Transaccionales - Extranet"

    'Public Function InsertarxIntranet(ByVal BE As clsUsuarioBE) As String
    '    Dim dc As New Dictionary(Of String, String)
    '    Dim strIDUsuario As String
    '    Try
    '        With dc
    '            .Add("@Usuario", BE.Usuario)
    '            .Add("@Nombre", BE.Nombre)
    '            .Add("@Nivel", BE.Nivel)
    '            .Add("@FechaNac", BE.FechaNac)
    '            .Add("@Directorio", BE.Directorio)
    '            .Add("@Correo", BE.Correo)
    '            .Add("@IdArea", BE.IdArea)
    '            .Add("@EsVendedor", BE.EsVendedor)
    '            .Add("@IDUbigeo", BE.IDUbigeo)
    '            .Add("@TxApellidos", BE.TxApellidos)
    '            .Add("@FecIngreso", BE.FecIngreso)
    '            .Add("@CoSexo", BE.CoSexo)
    '            .Add("@CoTipoDoc", BE.CoTipoDoc)
    '            .Add("@NumIdentidad", BE.NumIdentidad)
    '            .Add("@CoEmpresa", BE.CoEmpresa)
    '            .Add("@CoCargo", BE.CoCargo)
    '            .Add("@CoUserJefe", BE.CoUserJefe)
    '            .Add("@NuAnexo", BE.NuAnexo)
    '            .Add("@TxLema", BE.TxLema)
    '            .Add("@NoArchivoFoto", BE.NoArchivoFoto)
    '            .Add("@UserMod", BE.UserMod)
    '            .Add("@NuOrdenArea", BE.NuOrdenArea)
    '            .Add("@FlTrabajador_Activo", BE.FlTrabajador_Activo)
    '            .Add("@Direccion", BE.Direccion)
    '            .Add("@Telefono", BE.Telefono)
    '            .Add("@Celular", BE.Celular)
    '            .Add("@Password", BE.Password)
    '            .Add("@Siglas", BE.Siglas)
    '            .Add("@FlCompass", BE.FlCompass)
    '            .Add("@FecCese", BE.FecCese)
    '            .Add("@pIdUsuario", "")
    '        End With
    '        strIDUsuario = objADO.ExecuteSPOutput("MAUSUARIOS_Ins", dc)

    '        BE.IDUsuario = strIDUsuario
    '        Dim objBL As New clsSeguridadBT.clsOpcionesBT.clsUsuarioBT
    '        objBL.InsertarxUsuarioyNivel(strIDUsuario, BE.Nivel, BE.UserMod)


    '        If BE.FlCompass Then
    '            If Not BE.ListaReportan Is Nothing Then
    '                For Each strReporta As String In BE.ListaReportan
    '                    Actualizar_Jefe(strReporta, BE.IDUsuario, BE.UserMod)
    '                Next
    '            End If
    '        End If

    '        CrearCarpetaPerfil(BE.RutaAdjuntosxPerfilCorreoSQL, BE.Usuario)
    '        AgregarFotoRutaPublica(BE.NoRutaUserPhoto, BE.NoRutaLocalFoto, BE.Usuario, BE.NoArchivoFoto)

    '        'Procedimiento SAP
    '        Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
    '        PasarDatosUsuariosToSocioNegocio(strIDUsuario, BE.UserMod, oBESocioNegocio)

    '        If Not oBESocioNegocio Is Nothing And BE.FlEnviarASAP Then
    '            Dim oBTJsonMaster As New clsJsonInterfaceBT()
    '            oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
    '                                               "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
    '                                               "POST", _
    '                                               "BPAddresses", "ContactEmployees")
    '            Actualizar_CardCodeSAP(BE.IDUsuario, oBESocioNegocio.CardCode)
    '        End If

    '        Return strIDUsuario
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Public Sub Actualizar_Jefe(ByVal strIDUsuario As String, ByVal vstrCoUserJefe As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUsuario", strIDUsuario)
                .Add("@CoUserJefe", vstrCoUserJefe)
                .Add("@UserMod", vstrUserMod)
            End With
            objADO.ExecuteSP("MAUSUARIOS_Upd_Jefe", dc)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function drConsultarDatosClienteSAP(ByVal vstrIDUsuario As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSocioNegocio", vstrIDUsuario)
            dc.Add("@TipoSocioNegocio", "U")

            Return objADO.GetDataTable(True, "SAP_ListarSociosNegocio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub PasarDatosUsuariosToSocioNegocio(ByVal vstrIDUsuario As String, ByVal vstrUserMod As String, _
                                                 ByRef oBESocioNegocioBE As clsSocioNegocioBE)
        Try
            Dim dtDatosUsuario As DataTable = drConsultarDatosClienteSAP(vstrIDUsuario)
            If dtDatosUsuario.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatosUsuario.Rows(0)
                oBESocioNegocioBE = New clsSocioNegocioBE()
                With oBESocioNegocioBE
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .CardType = dr("CardType")
                    .GroupCode = dr("GroupCode")
                    .FederalTaxID = dr("FederalTaxID")
                    .Currency = dr("Currency")
                    .U_SYP_BPTP = dr("U_SYP_BPTP")
                    .U_SYP_BPTD = dr("U_SYP_BPTD")
                    .U_SYP_BPAP = dr("U_SYP_BPAP")
                    .U_SYP_BPAM = dr("U_SYP_BPAM")
                    .U_SYP_BPN2 = dr("U_SYP_BPN2")
                    .U_SYP_BPNO = dr("U_SYP_BPNO")
                    .EmailAddress = dr("EmailAddress")
                    .Phone1 = dr("Phone1")
                    .Phone2 = dr("Phone2")
                    .SubjectToWithholdingTax = dr("SubjectToWithholdingTax")
                    .Valid = dr("Valid")
                    Dim LstBPAddresses As New List(Of clsBPAddressesBE)
                    .BPAddresses = LstBPAddresses
                    Dim LstContactEmployeeBE As New List(Of clsContactEmployeesBE)
                    .ContactEmployees = LstContactEmployeeBE
                End With
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CrearCarpetaPerfil(ByVal vstrRutaAdjuntosxPerfilCorreoSQL As String, _
                                   ByVal vstrUsuario As String)
        Try
            Dim strCarpeta As String = vstrRutaAdjuntosxPerfilCorreoSQL & vstrUsuario

            If Not System.IO.Directory.Exists(strCarpeta) Then
                System.IO.Directory.CreateDirectory(strCarpeta)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub AgregarFotoRutaPublica(ByVal vstrRutaUserPhoto As String, _
                                       ByVal vstrRutaLocal As String, _
                                       ByVal vstrUsuario As String, _
                                       ByVal vstrImagen As String)
        If vstrRutaLocal.Trim = String.Empty Then Exit Sub
        Try
            Dim strRutaUserPhoto As String = vstrRutaUserPhoto & vstrUsuario & "\"
            If Not System.IO.Directory.Exists(strRutaUserPhoto) Then
                System.IO.Directory.CreateDirectory(strRutaUserPhoto)
            End If

            Dim strNuevoArchivo As String = strRutaUserPhoto & vstrImagen
            If System.IO.File.Exists(strNuevoArchivo) Then
                System.IO.File.Delete(strNuevoArchivo)
            End If
            System.IO.File.Copy(vstrRutaLocal, strNuevoArchivo)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub Actualizar_CardCodeSAP(ByVal strIDUsuario As String, ByVal strCardCodeSAP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUsuario", strIDUsuario)
                .Add("@CardCodeSAP", strCardCodeSAP)
            End With

            objADO.ExecuteSP("MAUSUARIOS_Upd_CardCodeSAP", dc)
        Catch ex As Exception
            Throw
        End Try
    End Sub
#End Region
End Class
