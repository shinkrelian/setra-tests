﻿Imports ComSEToursBE2
Imports ComDataAccessD2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsServicioProveedorBT

    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Private gstrTipoProvHotel As String = "001"

    Public Function Insertar(ByVal BE As clsServicioProveedorBE) As String
        Dim dc As New Dictionary(Of String, String)
        Dim strIDServicio As String

        Try
            With dc
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@IDTipoProv", BE.IDTipoProv)
                .Add("@IDTipoServ", BE.IDTipoServ)
                .Add("@Descripcion", BE.Descripcion)
                .Add("@Tarifario", BE.Tarifario)
                .Add("@Descri_tarifario", BE.Descri_tarifario)
                .Add("@FecCaducLectura", BE.FecCaducLectura)
                .Add("@Activo", BE.Activo)

                .Add("@IDUbigeo", BE.IDUbigeo)
                .Add("@Telefono", BE.Telefono)
                .Add("@Celular", BE.Celular)
                .Add("@Comision", BE.Comision)
                .Add("@UpdComision", BE.UpdComision)
                .Add("@IDCat", BE.IDCat)
                .Add("@Categoria", BE.Categoria)
                .Add("@Capacidad", BE.Capacidad)
                .Add("@Observaciones", BE.Observaciones)
                .Add("@Lectura", BE.Lectura)

                .Add("@Transfer", BE.Transfer)
                .Add("@TipoTransporte", BE.TipoTransporte)

                .Add("@AtencionLunes", BE.AtencionLunes)
                .Add("@AtencionMartes", BE.AtencionMartes)
                .Add("@AtencionMiercoles", BE.AtencionMiercoles)
                .Add("@AtencionJueves", BE.AtencionJueves)
                .Add("@AtencionViernes", BE.AtencionViernes)
                .Add("@AtencionSabado", BE.AtencionSabado)
                .Add("@AtencionDomingo", BE.AtencionDomingo)
                .Add("@HoraDesde", Format(BE.HoraDesde, "dd/MM/yyyy HH:mm:ss"))
                .Add("@HoraHasta", Format(BE.HoraHasta, "dd/MM/yyyy HH:mm:ss"))

                .Add("@DesayunoHoraDesde", Format(BE.DesayunoHoraDesde, "dd/MM/yyyy HH:mm:ss"))
                .Add("@DesayunoHoraHasta", Format(BE.DesayunoHoraHasta, "dd/MM/yyyy HH:mm:ss"))
                .Add("@IDDesayuno", BE.IDDesayuno)

                .Add("@PreDesayunoHoraDesde", Format(BE.PreDesayunoHoraDesde, "dd/MM/yyyy HH:mm:ss"))
                .Add("@PreDesayunoHoraHasta", Format(BE.PreDesayunoHoraHasta, "dd/MM/yyyy HH:mm:ss"))
                .Add("@IDPreDesayuno", BE.IDPreDesayuno)
                .Add("@ObservacionesDesayuno", BE.ObservacionesDesayuno)
                .Add("@HoraCheckIn", Format(BE.HoraCheckIn, "dd/MM/yyyy HH:mm:ss"))
                .Add("@HoraCheckOut", Format(BE.HoraCheckOut, "dd/MM/yyyy HH:mm:ss"))
                '.Add("@TelefonoRecepcion1", BE.TelefonoRecepcion1)
                '.Add("@TelefonoRecepcion2", BE.TelefonoRecepcion2)
                '.Add("@FaxRecepcion1", BE.FaxRecepcion1)
                '.Add("@FaxRecepcion2", BE.FaxRecepcion2)

                '.Add("@IncluyeServiciosDias", BE.IncluyeServiciosDias)
                .Add("@Dias", BE.Dias)

                .Add("@PoliticaLiberado", BE.PoliticaLiberado)
                .Add("@Liberado", BE.Liberado)
                .Add("@TipoLib", BE.TipoLib)

                .Add("@MaximoLiberado", BE.MaximoLiberado)
                .Add("@MontoL", BE.MontoL)

                '.Add("@Desayuno", BE.Desayuno)
                '.Add("@Lonche", BE.Lonche)
                '.Add("@Almuerzo", BE.Almuerzo)
                '.Add("@Cena", BE.Cena)
                .Add("@SinDescripcion", BE.SinDescripcion)
                .Add("@ServicioVarios", BE.ServicioVarios)
                .Add("@IDCabVarios", BE.IDCabVarios)
                .Add("@CoTipoPago", BE.CoTipoPago)
                .Add("@UserMod", BE.UserMod)
                .Add("@APTServicoOpcional", BE.APTServicoOpcional)
                .Add("@HoraPredeterminada", Format(BE.HoraPredeterminada, "dd/MM/yyyy HH:mm:ss"))
                .Add("@pIDServicio", "")

            End With

            strIDServicio = objADO.ExecuteSPOutput("MASERVICIOS_Ins", dc)

            'If Not BE.ListTextos Is Nothing Then
            '    Dim objBTDet As New clsServicioProveedorBT.clsTextoServicioProveedorBT
            '    BE.IDServicio = strIDServicio
            '    objBTDet.InsertarActualizarEliminar(BE)
            'End If

            If Not BE.ListServxDia Is Nothing Then
                Dim objBTServxDia As New clsServicioProveedorBT.clsServiciosporDiaBT
                BE.IDServicio = strIDServicio
                objBTServxDia.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListServAlimxDia Is Nothing Then
                Dim objBTServAlimxDia As New clsServicioProveedorBT.clsServiciosAlimentacionporDiaBT
                BE.IDServicio = strIDServicio
                objBTServAlimxDia.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaServAtencion Is Nothing Then
                Dim objBTServAtencion As New clsServicioProveedorBT.clsServiciosAtencionBT
                BE.IDServicio = strIDServicio
                objBTServAtencion.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaServicios_Hoteles Is Nothing Then
                Dim objBTServHoteles As New clsServicioProveedorBT.clsServicios_HotelBT
                BE.IDServicio = strIDServicio
                objBTServHoteles.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ObjServicios_Operaciones_Hoteles Is Nothing Then
                Dim objBTServOperHoteles As New clsServicioProveedorBT.clsServicios_Operaciones_HotelesBT
                BE.IDServicio = strIDServicio
                objBTServOperHoteles.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaServicios_Hoteles_Hab Is Nothing Then
                Dim objBTServHoteles_Hab As New clsServicioProveedorBT.clsServicios_Hotel_HabitacionesBT
                BE.IDServicio = strIDServicio
                objBTServHoteles_Hab.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaDescRangosAPT Is Nothing Then
                Dim objDescAPT As New clsServicioProveedorBT.clsDescRangosAPTServicioProveedorBT
                objDescAPT.InsertarActualizar(BE)
            End If

            If Not BE.ListaRangosAPT Is Nothing Then
                Dim objAPT As New clsServicioProveedorBT.clsRangosAPTServicioProveedorBT
                objAPT.InsertarActualizarEliminar(BE)
            End If
            ContextUtil.SetComplete()

            Return strIDServicio

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Sub ActualizarMargenxIDCat(ByVal vstrIDCat As String, ByVal vdecComision As Decimal, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCat", vstrIDCat)
            dc.Add("@Comision", vdecComision)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("MASERVICIOS_Upd_MargenesxIDCat", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Function InsertarCopiaCotizxIDServicio(ByVal vstrIDServicio As String, ByVal vintIDCabNuevo As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strIDServicioNuevo As String = ""
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@IDCabVariosNuevo", vintIDCabNuevo)
            dc.Add("@pIDServicioNuevo", "")
            strIDServicioNuevo = objADO.GetSPOutputValue("MASERVICIOS_InsCopiaCotxIDCabVarios", dc)

            Dim objBLAlixDia As New clsServicioProveedorBT.clsServiciosAlimentacionporDiaBT
            objBLAlixDia.InsertarCopiaxIDServicio(vstrIDServicio, strIDServicioNuevo)

            Dim objBLAte As New clsServicioProveedorBT.clsServiciosAtencionBT
            objBLAte.InsertarCopiaxIDServicio(vstrIDServicio, strIDServicioNuevo)

            Dim objBLDia As New clsServicioProveedorBT.clsServiciosporDiaBT
            objBLDia.InsertarCopiaxIDServicio(vstrIDServicio, strIDServicioNuevo)

            Dim objBLTex As New clsServicioProveedorBT.clsTextoServicioProveedorBT
            objBLTex.InsertarCopiaxIDServicio(vstrIDServicio, strIDServicioNuevo)

            ContextUtil.SetComplete()
            Return strIDServicioNuevo
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Function intIDServiciosDetxIDServicioxAnioxVariantexServicio(ByVal vstrIDServicio As String, ByVal vstrAnio As String, _
                                                                        ByVal vstrVariante As String, ByVal vstrServicio As String) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim intIDServicio_Det As Integer = 0
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@Anio", vstrAnio)
            dc.Add("@Variante", vstrVariante)
            dc.Add("@Descripcion", vstrServicio)
            dc.Add("@pIDServicio_Det", 0)

            intIDServicio_Det = objADO.GetSPOutputValue("MASERVICIOS_DET_SelIDxIDServicioxTipoxAnioxDescripcion", dc)
            ContextUtil.SetComplete()
            Return intIDServicio_Det
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Sub ActualizarMargenxIDProveedor(ByVal vstrIDProveedor As String, ByVal vdecComision As Decimal, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@Comision", vdecComision)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("MASERVICIOS_Upd_MargenxIDProveedor", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub ActualizarMargenxIDTipoProv(ByVal vstrIDTipoProv As String, ByVal vdecComision As Decimal, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@Comision", vdecComision)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("MASERVICIOS_Upd_MargenesxIDTipoProv", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Function Actualizar(ByVal BE As clsServicioProveedorBE, ByVal blnNuevoUbigeo As Boolean) As String
        Try

            Dim dc As New Dictionary(Of String, String)

            With dc
                .Add("@IDServicio", BE.IDServicio)
                .Add("@IDProveedor", BE.IDProveedor)
                .Add("@IDTipoProv", BE.IDTipoProv)
                .Add("@IDTipoServ", BE.IDTipoServ)
                .Add("@Activo", BE.Activo)
                .Add("@MotivoDesactivacion", BE.MotivoDesactivacion)

                .Add("@Descripcion", BE.Descripcion)
                .Add("@Tarifario", BE.Tarifario)
                .Add("@Descri_tarifario", BE.Descri_tarifario)
                .Add("@FecCaducLectura", BE.FecCaducLectura)
                If Not blnNuevoUbigeo Then
                    .Add("@IDUbigeo", BE.IDUbigeo)
                Else
                    .Add("@IDUbigeo", "")
                End If
                .Add("@Telefono", BE.Telefono)
                .Add("@Celular", BE.Celular)
                .Add("@IDCat", BE.IDCat)
                .Add("@Comision", BE.Comision)
                .Add("@UpdComision", BE.UpdComision)
                .Add("@Categoria", BE.Categoria)
                .Add("@Capacidad", BE.Capacidad)
                .Add("@Observaciones", BE.Observaciones)
                .Add("@Lectura", BE.Lectura)

                .Add("@Transfer", BE.Transfer)
                .Add("@TipoTransporte", BE.TipoTransporte)

                .Add("@AtencionLunes", BE.AtencionLunes)
                .Add("@AtencionMartes", BE.AtencionMartes)
                .Add("@AtencionMiercoles", BE.AtencionMiercoles)
                .Add("@AtencionJueves", BE.AtencionJueves)
                .Add("@AtencionViernes", BE.AtencionViernes)
                .Add("@AtencionSabado", BE.AtencionSabado)
                .Add("@AtencionDomingo", BE.AtencionDomingo)
                .Add("@HoraDesde", Format(BE.HoraDesde, "dd/MM/yyyy HH:mm:ss"))
                .Add("@HoraHasta", Format(BE.HoraHasta, "dd/MM/yyyy HH:mm:ss"))

                '.Add("@DesayunoHoraDesde", Format(BE.DesayunoHoraDesde, "dd/MM/yyyy hh:mm:ss"))
                .Add("@DesayunoHoraDesde", Format(BE.DesayunoHoraDesde, "dd/MM/yyyy HH:mm:ss"))
                '.Add("@DesayunoHoraHasta", Format(BE.DesayunoHoraHasta, "dd/MM/yyyy hh:mm:ss"))
                .Add("@DesayunoHoraHasta", Format(BE.DesayunoHoraHasta, "dd/MM/yyyy HH:mm:ss"))
                .Add("@IDDesayuno", BE.IDDesayuno)

                .Add("@PreDesayunoHoraDesde", Format(BE.PreDesayunoHoraDesde, "dd/MM/yyyy HH:mm:ss"))
                .Add("@PreDesayunoHoraHasta", Format(BE.PreDesayunoHoraHasta, "dd/MM/yyyy HH:mm:ss"))
                .Add("@IDPreDesayuno", BE.IDPreDesayuno)
                .Add("@ObservacionesDesayuno", BE.ObservacionesDesayuno)
                .Add("@HoraCheckIn", Format(BE.HoraCheckIn, "dd/MM/yyyy HH:mm:ss"))
                .Add("@HoraCheckOut", Format(BE.HoraCheckOut, "dd/MM/yyyy HH:mm:ss"))
                '.Add("@TelefonoRecepcion1", BE.TelefonoRecepcion1)
                '.Add("@TelefonoRecepcion2", BE.TelefonoRecepcion2)
                '.Add("@FaxRecepcion1", BE.FaxRecepcion1)
                '.Add("@FaxRecepcion2", BE.FaxRecepcion2)

                '.Add("@IncluyeServiciosDias", BE.IncluyeServiciosDias)
                .Add("@Dias", BE.Dias)

                .Add("@PoliticaLiberado", BE.PoliticaLiberado)
                .Add("@Liberado", BE.Liberado)
                .Add("@TipoLib", BE.TipoLib)
                .Add("@MaximoLiberado", BE.MaximoLiberado)
                .Add("@MontoL", BE.MontoL)

                '.Add("@Desayuno", BE.Desayuno)
                '.Add("@Lonche", BE.Lonche)
                '.Add("@Almuerzo", BE.Almuerzo)
                '.Add("@Cena", BE.Cena)
                .Add("@SinDescripcion", BE.SinDescripcion)
                .Add("@CoTipoPago", BE.CoTipoPago)
                .Add("@UserMod", BE.UserMod)
                .Add("@APTServicoOpcional", BE.APTServicoOpcional)
                .Add("@HoraPredeterminada", Format(BE.HoraPredeterminada, "dd/MM/yyyy HH:mm:ss"))

            End With

            objADO.ExecuteSP("MASERVICIOS_Upd", dc)

            'Dim objBTDet As New clsServicioProveedorBT.clsTextoServicioProveedorBT
            'objBTDet.InsertarActualizarEliminar(BE)

            If Not BE.ListServxDia Is Nothing Then
                Dim objBTServxDia As New clsServicioProveedorBT.clsServiciosporDiaBT
                objBTServxDia.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListServAlimxDia Is Nothing Then
                Dim objBTServAlimxDia As New clsServicioProveedorBT.clsServiciosAlimentacionporDiaBT
                objBTServAlimxDia.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaServAtencion Is Nothing Then
                Dim objBTServAtencion As New clsServicioProveedorBT.clsServiciosAtencionBT
                objBTServAtencion.InsertarActualizarEliminar(BE)
            End If

            Dim strIDServicio As String = ""

            If blnNuevoUbigeo Then
                strIDServicio = CopiarServiciosNuevoIDServicio(BE.IDServicio, BE.IDUbigeo, BE.UserMod)
            End If

            'If BE.IDTipoProv = gstrTipoProvHotel Then
            Dim objDetServ As New clsDetalleServicioProveedorBT
            objDetServ.ActualizarLiberadosxIDServicio(BE.IDServicio, BE.PoliticaLiberado, BE.Liberado, _
                                                       BE.TipoLib, BE.MaximoLiberado, BE.MontoL, BE.UserMod)

            'End If

            If blnNuevoUbigeo Then
                BE.IDServicio = strIDServicio
            End If
            If Not BE.ListaServicios_Hoteles Is Nothing Then
                Dim objBTServHoteles As New clsServicioProveedorBT.clsServicios_HotelBT
                ' BE.IDServicio = strIDServicio
                objBTServHoteles.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ObjServicios_Operaciones_Hoteles Is Nothing Then
                Dim objBTServOperHoteles As New clsServicioProveedorBT.clsServicios_Operaciones_HotelesBT
                'BE.IDServicio = strIDServicio
                objBTServOperHoteles.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaServicios_Hoteles_Hab Is Nothing Then
                Dim objBTServHoteles_Hab As New clsServicioProveedorBT.clsServicios_Hotel_HabitacionesBT
                objBTServHoteles_Hab.InsertarActualizarEliminar(BE)
            End If

            If Not BE.ListaDescRangosAPT Is Nothing Then
                Dim objDescAPT As New clsServicioProveedorBT.clsDescRangosAPTServicioProveedorBT
                objDescAPT.InsertarActualizar(BE)
            End If

            If Not BE.ListaRangosAPT Is Nothing Then
                Dim objAPT As New clsServicioProveedorBT.clsRangosAPTServicioProveedorBT
                objAPT.InsertarActualizarEliminar(BE)
            End If

            ContextUtil.SetComplete()

            Return strIDServicio
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsServicioProveedorBE, ByVal vstrAntiguoID As String, _
                                                             ByVal vstrNuevoID As String)
        Try
            If Not BE.ListaServicios_Hoteles_Hab Is Nothing Then
                For Each ItemDet As clsServicioProveedorBE.clsServicios_Hotel_HabitacionesBE In BE.ListaServicios_Hoteles_Hab
                    If ItemDet.CoServicio = vstrAntiguoID Then
                        ItemDet.CoServicio = vstrNuevoID
                    End If
                Next

            End If

            If Not BE.ListaServicios_Hoteles Is Nothing Then
                For Each ItemDet As clsServicioProveedorBE.clsServicios_HotelBE In BE.ListaServicios_Hoteles
                    If ItemDet.CoServicio = vstrAntiguoID Then
                        ItemDet.CoServicio = vstrNuevoID
                    End If
                Next

            End If

            If Not BE.ObjServicios_Operaciones_Hoteles Is Nothing Then
                BE.ObjServicios_Operaciones_Hoteles.CoServicio = vstrNuevoID
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub Anular(ByVal vstrIDServicio As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicio", vstrIDServicio)
                .Add("@UserMod", vstrUserMod)

            End With

            objADO.ExecuteSP("MASERVICIOS_Anu", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub InsertarDetallesxIDServicio(ByVal vDtt As DataTable, ByVal vstrUserMod As String, ByVal vstrIDServicioDestino As String)
        Try
            Dim objBE As clsServicioProveedorBE.clsDetalleServicioProveedorBE
            Dim objBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT

            Dim objBEDet As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE
            Dim ListaDet As New List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE)
            Dim objBTCost As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
            Dim dttCos As DataTable

            For Each dr As DataRow In vDtt.Rows

                objBE = New clsServicioProveedorBE.clsDetalleServicioProveedorBE With { _
                .Afecto = dr("Afecto"), _
                .Anio = dr("Anio"), _
                .CtaContable = If(IsDBNull(dr("CtaContable")), "", dr("CtaContable")), _
                .CtaContableC = If(IsDBNull(dr("CtaContableC")), "", dr("CtaContableC")), _
                .Codigo = dr("Codigo"), _
                .Descripcion = dr("Descripcion"), _
                .DetaTipo = If(IsDBNull(dr("DetaTipo")), "", dr("DetaTipo")), _
                .IDServicio = vstrIDServicioDestino, _
                .IDServicio_Det_V = dr("IDServicio_Det"), _
                .IDTipoOC = dr("IDTipoOC"), _
                .Liberado = If(IsDBNull(dr("Liberado")), 0, dr("Liberado")), _
                .Monto_dbl = If(IsDBNull(dr("Monto_dbl")), 0, dr("Monto_dbl")), _
                .Monto_tri = If(IsDBNull(dr("Monto_tri")), 0, dr("Monto_tri")), _
                .Monto_Sgl = If(IsDBNull(dr("Monto_Sgl")), 0, dr("Monto_Sgl")), _
                .Monto_dbls = If(IsDBNull(dr("Monto_dbls")), 0, dr("Monto_dbls")), _
                .Monto_tris = If(IsDBNull(dr("Monto_tris")), 0, dr("Monto_tris")), _
                .Monto_Sgls = If(IsDBNull(dr("Monto_Sgls")), 0, dr("Monto_Sgls")), _
                .TC = If(IsDBNull(dr("TC")), 0, dr("TC")), _
                .TipoGasto = If(IsDBNull(dr("TipoGasto")), "", dr("TipoGasto")), _
                .Cod_x_Triple = If(IsDBNull(dr("Cod_x_Triple")), "", dr("Cod_x_Triple")), _
                .IDServicio_DetxTriple = If(IsDBNull(dr("IDServicio_DetxTriple")), "", dr("IDServicio_DetxTriple")), _
                .MontoL = If(IsDBNull(dr("MontoL")), 0, dr("MontoL")), _
                .PoliticaLiberado = dr("PoliticaLiberado"), _
                .Tarifario = dr("Tarifario"), _
                .Tipo = If(IsDBNull(dr("Tipo")), "", dr("Tipo")), _
                .TipoDesayuno = dr("TipoDesayuno"), _
                .TipoLib = If(IsDBNull(dr("TipoLib")), "", dr("TipoLib")), _
                .TituloGrupo = If(IsDBNull(dr("TituloGrupo")), "", dr("TituloGrupo")), _
                .MaximoLiberado = If(IsDBNull(dr("MaximoLiberado")), 0, dr("MaximoLiberado")), _
                .CoMoneda = dr("CoMoneda"), _
                .UserMod = vstrUserMod}

                dttCos = objBTCost.ConsultarListxIDDetalle(dr("IDServicio_Det").ToString)

                For Each drCos As DataRow In dttCos.Rows
                    objBEDet = New clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE
                    With objBEDet

                        .Correlativo = drCos("Correlativo")
                        .PaxDesde = drCos("PaxDesde")
                        .PaxHasta = drCos("PaxHasta")
                        .Monto = drCos("Monto")
                        .UserMod = vstrUserMod

                        .Accion = "N"
                    End With
                    ListaDet.Add(objBEDet)
                Next
                objBE.ListaCostos = ListaDet


                objBT.Insertar(objBE)

            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub


    Public Function CopiarServicios(ByVal vstrIDServicio As String, _
                               ByVal vstrDescripcion As String, _
                               ByVal vstrIDProveedor As String, _
                               ByVal vstrIDUbigeo As String, _
                               ByVal vstrAnio As String, _
                               ByVal vstrTipo As String, _
                               ByVal vstrUser As String) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicio", vstrIDServicio)
                .Add("@Descripcion", vstrDescripcion)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDUbigeo", vstrIDUbigeo)
                .Add("@UserMod", vstrUser)
                .Add("@pIDServicioNue", "")
            End With

            Dim strIDServicioNue As String = objADO.ExecuteSPOutput("MASERVICIOS_Ins_Copiar", dc)

            dc = New Dictionary(Of String, String)
            With dc
                .Add("@IDServicio", vstrIDServicio)
                .Add("@IDServicioNue", strIDServicioNue)
                .Add("@UserMod", vstrUser)
            End With

            objADO.ExecuteSP("MASERVICIOS_DIA_Ins_Copiar", dc)
            objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIA_Ins_Copiar", dc)


            Dim objTexto As New clsTextoServicioProveedorBT
            objTexto.CopiarxServicio(vstrIDServicio, strIDServicioNue, vstrUser)

            Dim dcDet As New Dictionary(Of String, String)
            With dcDet
                .Add("@IDServicio", vstrIDServicio)
                .Add("@IDServicioNue", strIDServicioNue)
                .Add("@Anio", vstrAnio)
                .Add("@Tipo", vstrTipo)
                .Add("@UserMod", vstrUser)
            End With
            objADO.ExecuteSP("MASERVICIOS_Det_Ins_Copiar", dcDet)



            Dim objCostos As New clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
            objCostos.CopiarxFiltroenDetalleServicio(vstrIDServicio, vstrAnio, vstrTipo, 0, vstrUser)



            Dim dc2 As New Dictionary(Of String, String)
            dc2.Add("@IDServicioCopia", vstrIDServicio)

            Dim dt As DataTable = objADO.GetDataTable("MASERVICIOS_Sel_Copia", dc2)

            ContextUtil.SetComplete()

            Return dt
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Function CopiarServiciosNuevoIDServicio(ByVal vstrIDServicio As String, _
                                                   ByVal vstrIDUbigeoNue As String, _
                                                   ByVal vstrUserMod As String) As String
        Try
            Dim strIDServicioNue As String = ""
            Dim dc As New Dictionary(Of String, String)

            With dc
                .Add("@IDServicio", vstrIDServicio)
                .Add("@IDubigeoNue", vstrIDUbigeoNue)
                .Add("@UserMod", vstrUserMod)
                .Add("@pIDServicioNue", "")
            End With

            strIDServicioNue = objADO.ExecuteSPOutput("MASERVICIOS_Ins_NuevoIDServicio", dc)

            dc = New Dictionary(Of String, String)
            With dc
                .Add("@IDServicio", vstrIDServicio)
                .Add("@UserMod", vstrUserMod)
                .Add("@IDServicioNue", strIDServicioNue)
            End With
            objADO.ExecuteSP("MASERVICIOS_DIA_Upd_NuevoIDServicio", dc)

            objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIA_Upd_NuevoIDServicio", dc)

            objADO.ExecuteSP("MASERVICIOS_Det_Upd_NuevoIDServicio", dc)

            objADO.ExecuteSP("COTIDET_Upd_NuevoIDServicio", dc)

            objADO.ExecuteSP("RESERVAS_DET_Upd_NuevoIDServicio", dc)

            objADO.ExecuteSP("MASERVICIOS_ATENCION_Upd_NuevoIDServicio", dc)

            objADO.ExecuteSP("MASERVICIOS_OPERACIONES_HOTELES_Upd_NuevoIDServicio", dc)
            objADO.ExecuteSP("MASERVICIOS_HOTEL_Upd_NuevoIDServicio", dc)

            'JORGE
            objADO.ExecuteSP("OPERACIONES_DET_DETSERVICIOS_Upd_NuevoIDServicio", dc)


            dc = New Dictionary(Of String, String)
            dc.Add("@IDServicio", vstrIDServicio)

            objADO.ExecuteSP("MASERVICIOS_Del", dc)


            ContextUtil.SetComplete()
            Return strIDServicioNue
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function


    Public Function ConsultarPk(ByVal vstrIDServicio As String, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)

        Return objADO.GetDataTable("MASERVICIOS_Sel_Pk", dc, vCnn)

    End Function

    'DONY TINOCO
    Public Function drConsultarPk(ByVal vstrIDServicio As String, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)

        Return objADO.GetDataReader("MASERVICIOS_Sel_Pk", dc, vCnn)

    End Function

    Public Function ConsultarxIDServicioAnio(ByVal vstrIDServicio As String, ByVal vstrAnio As String, _
                                             ByVal vstrTipo As String, _
                                             ByVal vintIDDet As Integer, _
                                             Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)
        dc.Add("@Anio", vstrAnio)
        dc.Add("@Tipo", vstrTipo)
        dc.Add("@IDDet", vintIDDet)

        Dim strSql As String = "MASERVICIOS_Det_Sel_xIDServicioAnio"
        Return objADO.GetDataReader(strSql, dc, vCnn)

    End Function

    Public Function dttConsultarxIDServicioAnio(ByVal vstrIDServicio As String, ByVal vstrAnio As String, _
                                                ByVal vstrTipo As String, _
                                                ByVal vintIDDet As Integer, _
                                                Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)
        dc.Add("@Anio", vstrAnio)
        dc.Add("@Tipo", vstrTipo)
        dc.Add("@IDDet", vintIDDet)

        Return objADO.GetDataTable("MASERVICIOS_Det_Sel_xIDServicioAnio", dc, vCnn)

    End Function

    <ComVisible(True)> _
 <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsDetalleServicioProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Function drConsultarPk(ByVal vstrIDServicio_Det As Int32) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vstrIDServicio_Det)

            Dim strSql As String = "MASERVICIOS_DET_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)
        End Function

        Public Sub Insertar(ByVal BE As clsServicioProveedorBE.clsDetalleServicioProveedorBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDServicio", BE.IDServicio)
                    .Add("@IDServicio_Det_V", BE.IDServicio_Det_V)
                    .Add("@Anio", BE.Anio)
                    .Add("@Tipo", BE.Tipo)
                    .Add("@DetaTipo", BE.DetaTipo)
                    .Add("@VerDetaTipo", BE.VerDetaTipo)
                    .Add("@Codigo", BE.Codigo)

                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Afecto", BE.Afecto)
                    '.Add("@IDMoneda", BE.IDMoneda)
                    .Add("@Monto_Sgl", BE.Monto_Sgl)
                    .Add("@Monto_dbl", BE.Monto_dbl)
                    .Add("@Monto_tri", BE.Monto_tri)
                    .Add("@Cod_x_Triple", BE.Cod_x_Triple)
                    .Add("@IDServicio_DetxTriple", BE.IDServicio_DetxTriple)
                    .Add("@Monto_Sgls", BE.Monto_Sgls)
                    .Add("@Monto_dbls", BE.Monto_dbls)
                    .Add("@Monto_tris", BE.Monto_tris)

                    .Add("@TC", BE.TC)

                    .Add("@ConAlojamiento", BE.ConAlojamiento)
                    .Add("@PlanAlimenticio", BE.PlanAlimenticio)
                    .Add("@CodTarifa", BE.CodTarifa)

                    .Add("@DiferSS", BE.DiferSS)
                    .Add("@DiferST", BE.DiferST)

                    .Add("@TipoGasto", BE.TipoGasto)

                    .Add("@TipoDesayuno", BE.TipoDesayuno)

                    .Add("@Desayuno", BE.Desayuno)
                    .Add("@Lonche", BE.Lonche)
                    .Add("@Almuerzo", BE.Almuerzo)
                    .Add("@Cena", BE.Cena)

                    .Add("@PoliticaLiberado", BE.PoliticaLiberado)
                    .Add("@Liberado", BE.Liberado)
                    .Add("@TipoLib", BE.TipoLib)

                    .Add("@MaximoLiberado", BE.MaximoLiberado)
                    .Add("@LiberadoM", BE.LiberadoM)
                    .Add("@MontoL", BE.MontoL)
                    .Add("@Tarifario", BE.Tarifario)

                    .Add("@FlDetraccion", BE.FlDetraccion)

                    .Add("@PoliticaLibNivelDetalle", BE.PoliticaLibNivelDetalle)


                    .Add("@TituloGrupo", BE.TituloGrupo)
                    .Add("@IDTipoOC", BE.IDTipoOC)
                    .Add("@CoCeCos", BE.CoCeCos)
                    .Add("@CtaContable", BE.CtaContable)
                    .Add("@CtaContableC", BE.CtaContableC)

                    .Add("@IdHabitTriple", BE.IdHabitTriple)
                    .Add("@CoMoneda", BE.CoMoneda)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@pIDServicio_Det", 0)

                End With
                Dim intIDServicio_Det As Int32 = objADO.ExecuteSPOutput("MASERVICIOS_DET_Ins", dc)
                BE.IDServicio_Det = intIDServicio_Det

                Dim objDet As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
                objDet.InsertarActualizarEliminar(BE)

                Dim objDetDescAPT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsDescRangosAPTDetalleServicioProveedorBT
                objDetDescAPT.InsertarActualizar(BE)

                Dim objDetAPT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsRangosAPTDetalleServicioProveedorBT
                objDetAPT.InsertarActualizarEliminar(BE)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsServicioProveedorBE.clsDetalleServicioProveedorBE)
            Try

                If BE.IDServicio_Det_V <> 0 Then
                    Dim dc2 As New Dictionary(Of String, String)
                    With dc2
                        .Add("@IDServicio_Det", BE.IDServicio_Det)
                        .Add("@IDServicio_Det_V", BE.IDServicio_Det_V)
                        .Add("@pbIguales", 0)
                    End With
                    Dim blnIguales As Boolean = objADO.ExecuteSPOutput("MASERVICIO_DET_Sel_IDServicio_Det_VIgualesOutput", dc2)
                    If Not blnIguales Then
                        Dim objCostosBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
                        objCostosBT.EliminarxIDServicio(BE.IDServicio_Det)

                        '--
                        Dim objRangosAPTBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsRangosAPTDetalleServicioProveedorBT
                        objRangosAPTBT.EliminarxIDServicio(BE.IDServicio_Det)
                    End If
                End If


                Dim dc As New Dictionary(Of String, String)

                With dc
                    .Add("@IDServicio_Det", BE.IDServicio_Det)
                    .Add("@IDServicio_Det_V", BE.IDServicio_Det_V)
                    .Add("@Anio", BE.Anio)
                    .Add("@Tipo", BE.Tipo)
                    .Add("@DetaTipo", BE.DetaTipo)
                    .Add("@VerDetaTipo", BE.VerDetaTipo)
                    .Add("@Codigo", BE.Codigo)

                    .Add("@Descripcion", BE.Descripcion)
                    .Add("@Afecto", BE.Afecto)
                    '.Add("@IDMoneda", BE.IDMoneda)
                    .Add("@Monto_Sgl", BE.Monto_Sgl)
                    .Add("@Monto_dbl", BE.Monto_dbl)
                    .Add("@Monto_tri", BE.Monto_tri)
                    .Add("@Cod_x_Triple", BE.Cod_x_Triple)
                    .Add("@IDServicio_DetxTriple", BE.IDServicio_DetxTriple)
                    .Add("@Monto_Sgls", BE.Monto_Sgls)
                    .Add("@Monto_dbls", BE.Monto_dbls)
                    .Add("@Monto_tris", BE.Monto_tris)

                    .Add("@TC", BE.TC)

                    .Add("@ConAlojamiento", BE.ConAlojamiento)
                    .Add("@PlanAlimenticio", BE.PlanAlimenticio)
                    .Add("@CodTarifa", BE.CodTarifa)

                    .Add("@DiferSS", BE.DiferSS)
                    .Add("@DiferST", BE.DiferST)


                    .Add("@TipoGasto", BE.TipoGasto)
                    .Add("@TipoDesayuno", BE.TipoDesayuno)

                    .Add("@Desayuno", BE.Desayuno)
                    .Add("@Lonche", BE.Lonche)
                    .Add("@Almuerzo", BE.Almuerzo)
                    .Add("@Cena", BE.Cena)


                    .Add("@PoliticaLiberado", BE.PoliticaLiberado)
                    .Add("@Liberado", BE.Liberado)
                    .Add("@TipoLib", BE.TipoLib)

                    .Add("@MaximoLiberado", BE.MaximoLiberado)
                    .Add("@LiberadoM", BE.LiberadoM)
                    .Add("@MontoL", BE.MontoL)
                    .Add("@Tarifario", BE.Tarifario)
                    .Add("@FlDetraccion", BE.FlDetraccion)

                    .Add("@PoliticaLibNivelDetalle", BE.PoliticaLibNivelDetalle)


                    .Add("@TituloGrupo", BE.TituloGrupo)
                    .Add("@IDTipoOC", BE.IDTipoOC)
                    .Add("@CoCeCos", BE.CoCeCos)
                    .Add("@CtaContable", BE.CtaContable)
                    .Add("@CtaContableC", BE.CtaContableC)

                    .Add("@IdHabitTriple", BE.IdHabitTriple)
                    '.Add("@AnioPreciosCopia", BE.AnioPreciosCopia)
                    .Add("@CoMoneda", BE.CoMoneda)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MASERVICIOS_DET_Upd", dc)

                Dim objDet As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
                objDet.InsertarActualizarEliminar(BE)

                If Not BE.ListaDescRangos Is Nothing Then
                    Dim objDetDescAPT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsDescRangosAPTDetalleServicioProveedorBT
                    objDetDescAPT.InsertarActualizar(BE)
                End If

                If Not BE.ListaRangos Is Nothing Then
                    Dim objDetAPT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsRangosAPTDetalleServicioProveedorBT
                    objDetAPT.InsertarActualizarEliminar(BE)
                End If

                If BE.CambioCostos Then
                    Dim objCotiDetBT As New clsDetalleVentasCotizacionBT
                    objCotiDetBT.ActualizarCampoTarifaCambiada(BE.IDServicio_Det, BE.UserMod)
                    objCotiDetBT.ActualizarReservasCampoTarifaCambiada(BE.IDServicio_Det, BE.UserMod)
                End If

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub Eliminar(ByVal vintIDServicio_Det As Int32)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDServicio_Det", vintIDServicio_Det)
                End With


                Dim objCostosBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
                objCostosBT.EliminarxIDServicio(vintIDServicio_Det)

                objADO.ExecuteSP("MASERVICIOS_DET_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Anular(ByVal vintIDServicio_Det As Int32, ByVal vstrActivo As String, ByVal vstrUserMod As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio_Det", vintIDServicio_Det)
                dc.Add("@Estado", vstrActivo)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("MASERVICIOS_DET_Anu", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Function InsertarCopiaxIDServicio_Det(ByVal vintIDServicio_Det As Integer, ByVal vstrIDServicioNuevo As String) As Integer
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim intIDServicio_Det As Integer = 0
                dc.Add("@IDServicio_Det", vintIDServicio_Det)
                dc.Add("@IDServicioNuevo", vstrIDServicioNuevo)
                dc.Add("@pIDServicio_Det", 0)
                intIDServicio_Det = objADO.GetSPOutputValue("MASERVICIOS_DETInsxIDServicio_Det", dc)

                ContextUtil.SetComplete()
                Return intIDServicio_Det
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function

        Public Sub InsertarCopiaxIDServicio(ByVal vstrIDServicio As String, ByVal vstrIDServicioNuevo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim objBLCostos As New clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
                dc.Add("@IDServicio", vstrIDServicio)
                Dim dtServiciosDet As Data.DataTable = objADO.GetDataTable("MASERVICIOS_DET_SelxIDServicio", dc)
                For Each dRowItem As Data.DataRow In dtServiciosDet.Rows
                    Dim intIDServicioDetNuevo As Integer = InsertarCopiaxIDServicio_Det(dRowItem("IDServicio_Det"), vstrIDServicioNuevo)

                    objBLCostos.InsertarCopiaCotizxIDServicios_Det(dRowItem("IDServicio_Det"), intIDServicioDetNuevo)
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub CopiarServiciosNuevoAnio(ByVal vstrIDServicio As String, _
                                                 ByVal vsglPorcentDcto As Single, _
                           ByVal vstrAnioNue As String, _
                           ByVal vstrAnio As String, _
                           ByVal vstrTipo As String, _
                           ByVal vstrUser As String)

            Dim dc As New Dictionary(Of String, String)
            Try

                With dc
                    .Add("@IDServicio", vstrIDServicio)
                    .Add("@PorcentDcto", vsglPorcentDcto)
                    .Add("@AnioNue", vstrAnioNue)
                    .Add("@Anio", vstrAnio)
                    '.Add("@AnioPreciosCopia", vstrAnioPreciosCopia)
                    .Add("@Tipo", vstrTipo)
                    .Add("@UserMod", vstrUser)

                End With

                objADO.ExecuteSP("MASERVICIOS_Det_Ins_CopiarNuevoAnio", dc)


                Dim dc2 As New Dictionary(Of String, String)
                dc2.Add("@IDServicio", vstrIDServicio)
                dc2.Add("@Tipo", vstrTipo)
                dc2.Add("@AnioNue", vstrAnioNue)

                Dim dt As DataTable = objADO.GetDataTable("MASERVICIOS_DET_Sel_Copia2", dc2)

                For Each dr As DataRow In dt.Rows
                    Dim objCostos As New clsCostosDetalleServicioProveedorBT
                    'objCostos.CopiarxFiltroenDetalleServicio(vstrIDServicio, vstrAnio, vstrTipo, 0, vstrUser)
                    'objCostos.EliminarxIDServicio_DetCopia(vstrIDServicio, dr("IDServicio_DetCopia"))
                    If Not IsDBNull(dr("IDServicio_Det_V")) Then
                        'objCostos.CopiarxIDServicio_DetCopiaRel(dr("IDServicio_DetCopia"), vstrIDServicio, vsglPorcentDcto, vstrUser)
                        objCostos.CopiarxIDServicio_DetCopiaRel(dr("IDServicio_Det"), dr("IDServicio_Det_V"), vsglPorcentDcto, vstrUser)
                    Else
                        'objCostos.CopiarxIDServicio_DetCopia(dr("IDServicio_DetCopia"), vstrIDServicio, vsglPorcentDcto, vstrUser)
                        objCostos.CopiarxIDServicio_DetCopia(dr("IDServicio_Det"), dr("IDServicio_DetCopia"), vsglPorcentDcto, vstrUser)
                    End If
                Next


                ContextUtil.SetComplete()


            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Function CopiarServiciosNuevaVariante(ByVal vstrIDServicio As String, _
                   ByVal vstrTipoNue As String, _
                   ByVal vstrDetaTipoNue As String, _
                   ByVal vsglPorcentDcto As Single, _
                   ByVal vstrAnio As String, _
                   ByVal vstrTipo As String, _
                   ByVal vstrUser As String) As DataTable

            Dim dc As New Dictionary(Of String, String)
            Try

                With dc
                    .Add("@IDServicio", vstrIDServicio)
                    .Add("@TipoNue", vstrTipoNue)
                    .Add("@DetaTipoNue", vstrDetaTipoNue)
                    .Add("@PorcentDcto", vsglPorcentDcto)
                    .Add("@Anio", vstrAnio)
                    .Add("@Tipo", vstrTipo)
                    .Add("@UserMod", vstrUser)

                End With

                objADO.ExecuteSP("MASERVICIOS_Det_Ins_CopiarNuevoVariante", dc)

                Dim objCostos As New clsCostosDetalleServicioProveedorBT
                objCostos.CopiarxFiltroenDetalleServicio(vstrIDServicio, vstrAnio, vstrTipo, vsglPorcentDcto, vstrUser)


                Dim dc2 As New Dictionary(Of String, String)
                dc2.Add("@IDServicio", vstrIDServicio)
                dc2.Add("@TipoNue", vstrTipoNue)

                Dim dt As DataTable = objADO.GetDataTable("MASERVICIOS_DET_Sel_CopiaxVariante", dc2)

                ContextUtil.SetComplete()

                Return dt
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Function
        Public Sub ActualizarLiberadosxIDServicio(ByVal vstrIDServicio As String, ByVal vblnPoliticaLiberado As Boolean, _
                                                  ByVal vbytLiberado As Byte, ByVal vchrTipoLib As Char, _
                                                  ByVal vbytMaximoLiberado As Byte, ByVal vdblMontoL As Double, _
                                                  ByVal vstrUserMod As String)

            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDServicio", vstrIDServicio)
                    .Add("@PoliticaLiberado", vblnPoliticaLiberado)
                    .Add("@Liberado", vbytLiberado)
                    .Add("@TipoLib", vchrTipoLib)

                    .Add("@MaximoLiberado", vbytMaximoLiberado)
                    .Add("@MontoL", vdblMontoL)
                    .Add("@UserMod", vstrUserMod)
                End With

                objADO.ExecuteSP("MASERVICIOS_DET_UpdLiberadosxIDServicio", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Function bytActualizarDetServicioRelacionado(ByVal vstrIDServicio As String, _
                                                    ByVal vintIDServicio_Det As Integer, _
                                                    ByVal vstrAnio As String, _
                                                    ByVal vstrTipo As String, _
                                                    ByVal vstrCodigo As String, _
                                                    ByVal vstrUserMod As String) As Byte
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDServicio", vstrIDServicio)
                    .Add("@IDServicio_Det", vintIDServicio_Det)
                    .Add("@Anio", vstrAnio)
                    .Add("@Tipo", vstrTipo)
                    .Add("@Codigo", vstrCodigo)
                    .Add("@UserMod", vstrUserMod)
                    .Add("@pRegistrosAfectados", 0)
                End With

                Dim bytRegAffect As Byte = objADO.ExecuteSPOutput("MASERVICIOS_DET_Upd_IDServicio_Det_V_Output", dc)

                ContextUtil.SetComplete()

                Return bytRegAffect
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDServicio_Det As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vstrIDServicio_Det)

            Dim strSql As String = "MASERVICIOS_DET_Sel_Pk"
            Return objADO.GetDataTable(strSql, dc)

        End Function

        'DONY TINOCO
        Public Function ConsultarPkSinAnulado(ByVal vstrIDServicio_Det As Int32, _
                            Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vstrIDServicio_Det)

            Dim strSql As String = "MASERVICIOS_DET_Sel_PkSinAnul"
            Return objADO.GetDataReader(strSql, dc, vCnn)

        End Function

        <ComVisible(True)> _
     <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsCostosDetalleServicioProveedorBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT

            Public Sub InsertarCopiaCotizxIDServicios_Det(ByVal vintIDServicio_Det As Integer, ByVal vintIDServicio_DetNuevo As Integer)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@IDServicio_DetNuevo", vintIDServicio_DetNuevo)
                    objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_InsxIDServicio_Det", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE.clsDetalleServicioProveedorBE)
                Dim dc As Dictionary(Of String, String)
                Try

                    For Each Item As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE In BE.ListaCostos

                        dc = New Dictionary(Of String, String)
                        If Item.Accion = "M" Or Item.Accion = "N" Then
                            If Item.Accion = "M" Then _
                                dc.Add("@Correlativo", Item.Correlativo)

                            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
                            dc.Add("@PaxDesde", Item.PaxDesde)
                            dc.Add("@PaxHasta", Item.PaxHasta)
                            dc.Add("@Monto", Item.Monto)
                            dc.Add("@UserMod", Item.UserMod)

                            If Item.Accion = "N" Then
                                objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_Ins", dc)
                                InsertarHijosxIDServicio_Det_V(BE.IDServicio_Det, Item.PaxDesde, _
                                                               Item.PaxHasta, Item.Monto, Item.UserMod)
                            ElseIf Item.Accion = "M" Then
                                objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_Upd", dc)

                            End If
                        ElseIf Item.Accion = "B" Then
                            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
                            dc.Add("@Correlativo", Item.Correlativo)
                            objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_Del", dc)
                        End If
                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Private Sub InsertarHijosxIDServicio_Det_V(ByVal vintIDServicio_Det_V As Integer, _
                                                       ByVal vintPaxDesde As Int16, _
                                                       ByVal vintPaxHasta As Int16, _
                                                       ByVal vdblMonto As Double, _
                                                       ByVal vstrUser As String)
                Dim dc As New Dictionary(Of String, String)
                Dim dcIns As Dictionary(Of String, String)

                Try
                    dc.Add("@IDServicio_Det_V", vintIDServicio_Det_V)
                    Dim dtt As DataTable = objADO.GetDataTable("MASERVICIOS_DET_SelIDServicio_Det_xIDServicio_Det_V", dc)

                    For Each dr As DataRow In dtt.Rows
                        dcIns = New Dictionary(Of String, String)
                        dcIns.Add("@IDServicio_Det", dr("IDServicio_Det"))
                        dcIns.Add("@PaxDesde", vintPaxDesde)
                        dcIns.Add("@PaxHasta", vintPaxHasta)
                        dcIns.Add("@Monto", vdblMonto)
                        dcIns.Add("@UserMod", vstrUser)

                        objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_Ins", dcIns)

                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub CopiarxFiltroenDetalleServicio(ByVal vstrIDServicio As String, _
                                                      ByVal vstrAnio As String, _
                                                      ByVal vstrTipo As String, _
                                                      ByVal vdecPorcentDcto As Decimal, _
                                                      ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio", vstrIDServicio)
                    dc.Add("@Anio", vstrAnio)
                    dc.Add("@Tipo", vstrTipo)
                    dc.Add("@PorcentDcto", vdecPorcentDcto)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_Ins_Copiar", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception

                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub CopiarxIDServicio_DetCopiaRel(ByVal vintIDServicio_Det As Integer, _
                                                        ByVal vintIDServicio_Det_V As Integer, _
                                                      ByVal vsglPorcentDcto As Single, _
                                                      ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@IDServicio_Det_V", vintIDServicio_Det_V)
                    dc.Add("@PorcentDcto", vsglPorcentDcto)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_Ins_CopiarxIDServicio_DetCopiaRel", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception

                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub CopiarxIDServicio_DetCopia(ByVal vintIDServicio_Det As Integer, _
                                                  ByVal vintIDServicio_DetCopia As Integer, _
                                          ByVal vdecPorcentDcto As Decimal, _
                                          ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@IDServicio_DetCopia", vintIDServicio_DetCopia)
                    dc.Add("@PorcentDcto", vdecPorcentDcto)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_Ins_CopiarxIDServicio_DetCopia", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception

                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub


            Public Function ConsultarListxIDDetalle(ByVal vstrIDServicio_Det As Int32) As DataTable

                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDServicio_Det", vstrIDServicio_Det)

                    Return objADO.GetDataTable("MASERVICIOS_DET_COSTOS_Sel_xIDServicio_Det", dc)
                Catch ex As Exception
                    Throw
                End Try

            End Function

            Public Sub EliminarxIDServicio(ByVal vstrIDServicio_Det As Int32)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vstrIDServicio_Det)
                    objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_DelxIDServicio_Det", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Public Sub EliminarxIDServicio_DetCopia(ByVal vstrIDServicio As String, ByVal vintIDServicio_DetCopia As Int32)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio", vstrIDServicio)
                    dc.Add("@IDServicio_DetCopia", vintIDServicio_DetCopia)
                    objADO.ExecuteSP("MASERVICIOS_DET_COSTOS_DelxIDServicio_DetCopia", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Public Function dblConsultarCosto(ByVal vintIDServicio_Det As Integer, ByVal vintNroPax As Int16) As Double
                Dim dc As New Dictionary(Of String, String)
                Try
                    With dc
                        .Add("@IDServicio_Det", vintIDServicio_Det)
                        .Add("@NroPax", vintNroPax)
                        .Add("@pMonto", 0)
                    End With

                    Return objADO.ExecuteSPOutput("MASERVICIOS_DET_COSTOS_SelCostoOutput", dc)

                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class

        '------------------------------------------------------------
        <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsDescRangosAPTDetalleServicioProveedorBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT
            Public Sub InsertarActualizar(ByVal BE As clsServicioProveedorBE.clsDetalleServicioProveedorBE)
                Dim dc As Dictionary(Of String, String)
                Try

                    For Each Item As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE In BE.ListaDescRangos

                        dc = New Dictionary(Of String, String)

                        dc.Add("@IDServicio_Det", BE.IDServicio_Det)
                        dc.Add("@IDTemporada", Item.IDTemporada)
                        dc.Add("@DescripcionAlterna", Item.DescripcionAlterna)
                        dc.Add("@SingleSupplement", Item.SingleSupplement)
                        dc.Add("@PoliticaLiberado", Item.PoliticaLiberado)
                        dc.Add("@Liberado", Item.Liberado)
                        dc.Add("@TipoLib", Item.TipoLib)
                        dc.Add("@MaximoLiberado", Item.MaximoLiberado)
                        dc.Add("@MontoL", Item.MontoL)
                        dc.Add("@UserMod", Item.UserMod)
                        objADO.ExecuteSP("MASERVICIOS_DET_DESC_APT_InsUpd", dc)
                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub
        End Class
        '------------------------------------------------------------

        <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsRangosAPTDetalleServicioProveedorBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT

            Public Sub InsertarCopiaCotizxIDServicios_Det(ByVal vintIDServicio_Det As Integer, ByVal vintIDServicio_DetNuevo As Integer)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@IDServicio_DetNuevo", vintIDServicio_DetNuevo)
                    objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_InsxIDServicio_Det", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE.clsDetalleServicioProveedorBE)
                Dim dc As Dictionary(Of String, String)
                Try

                    For Each Item As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE In BE.ListaRangos

                        dc = New Dictionary(Of String, String)
                        If Item.Accion = "M" Or Item.Accion = "N" Then
                            If Item.Accion = "M" Then _
                                dc.Add("@Correlativo", Item.Correlativo)

                            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
                            dc.Add("@PaxDesde", Item.PaxDesde)
                            dc.Add("@PaxHasta", Item.PaxHasta)
                            dc.Add("@Monto", Item.Monto)
                            dc.Add("@UserMod", Item.UserMod)
                            dc.Add("@IDTemporada", Item.IDTemporada)

                            If Item.Accion = "N" Then
                                objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_Ins", dc)
                                InsertarHijosxIDServicio_Det_V(BE.IDServicio_Det, Item.PaxDesde, _
                                                               Item.PaxHasta, Item.Monto, Item.UserMod, Item.IDTemporada)
                            ElseIf Item.Accion = "M" Then
                                objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_Upd", dc)

                            End If
                        ElseIf Item.Accion = "B" Then
                            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
                            dc.Add("@Correlativo", Item.Correlativo)
                            dc.Add("@IDTemporada", Item.IDTemporada)
                            objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_Del", dc)
                        End If
                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Private Sub InsertarHijosxIDServicio_Det_V(ByVal vintIDServicio_Det_V As Integer, _
                                                       ByVal vintPaxDesde As Int16, _
                                                       ByVal vintPaxHasta As Int16, _
                                                       ByVal vdblMonto As Double, _
                                                       ByVal vstrUser As String,
                                                       ByVal vintIDTemporada As Integer)
                Dim dc As New Dictionary(Of String, String)
                Dim dcIns As Dictionary(Of String, String)

                Try
                    dc.Add("@IDServicio_Det_V", vintIDServicio_Det_V)
                    Dim dtt As DataTable = objADO.GetDataTable("MASERVICIOS_DET_SelIDServicio_Det_xIDServicio_Det_V", dc)

                    For Each dr As DataRow In dtt.Rows
                        dcIns = New Dictionary(Of String, String)
                        dcIns.Add("@IDServicio_Det", dr("IDServicio_Det"))
                        dcIns.Add("@PaxDesde", vintPaxDesde)
                        dcIns.Add("@PaxHasta", vintPaxHasta)
                        dcIns.Add("@Monto", vdblMonto)
                        dcIns.Add("@UserMod", vstrUser)
                        dcIns.Add("@IDTemporada", vintIDTemporada)

                        objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_Ins", dcIns)

                    Next

                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub CopiarxFiltroenDetalleServicio(ByVal vstrIDServicio As String, _
                                                      ByVal vstrAnio As String, _
                                                      ByVal vstrTipo As String, _
                                                      ByVal vdecPorcentDcto As Decimal, _
                                                      ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio", vstrIDServicio)
                    dc.Add("@Anio", vstrAnio)
                    dc.Add("@Tipo", vstrTipo)
                    dc.Add("@PorcentDcto", vdecPorcentDcto)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_Ins_Copiar", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception

                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub CopiarxIDServicio_DetCopiaRel(ByVal vintIDServicio_Det As Integer, _
                                                        ByVal vintIDServicio_Det_V As Integer, _
                                                      ByVal vsglPorcentDcto As Single, _
                                                      ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@IDServicio_Det_V", vintIDServicio_Det_V)
                    dc.Add("@PorcentDcto", vsglPorcentDcto)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_Ins_CopiarxIDServicio_DetCopiaRel", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception

                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub CopiarxIDServicio_DetCopia(ByVal vintIDServicio_Det As Integer, _
                                                  ByVal vintIDServicio_DetCopia As Integer, _
                                          ByVal vdecPorcentDcto As Decimal, _
                                          ByVal vstrUserMod As String)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@IDServicio_DetCopia", vintIDServicio_DetCopia)
                    dc.Add("@PorcentDcto", vdecPorcentDcto)
                    dc.Add("@UserMod", vstrUserMod)

                    objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_Ins_CopiarxIDServicio_DetCopia", dc)

                    ContextUtil.SetComplete()
                Catch ex As Exception

                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub


            Public Function ConsultarListxIDDetalle(ByVal vstrIDServicio_Det As Int32) As DataTable

                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDServicio_Det", vstrIDServicio_Det)

                    Return objADO.GetDataTable("MASERVICIOS_DET_RANGO_APT_Sel_xIDServicio_Det", dc)
                Catch ex As Exception
                    Throw
                End Try

            End Function

            Public Sub EliminarxIDServicio(ByVal vstrIDServicio_Det As Int32)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio_Det", vstrIDServicio_Det)
                    objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_DelxIDServicio_Det", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Public Sub EliminarxIDServicio_DetCopia(ByVal vstrIDServicio As String, ByVal vintIDServicio_DetCopia As Int32)
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDServicio", vstrIDServicio)
                    dc.Add("@IDServicio_DetCopia", vintIDServicio_DetCopia)
                    objADO.ExecuteSP("MASERVICIOS_DET_RANGO_APT_DelxIDServicio_DetCopia", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Sub

            Public Function dblConsultarCosto(ByVal vintIDServicio_Det As Integer, ByVal vintNroPax As Int16) As Double
                Dim dc As New Dictionary(Of String, String)
                Try
                    With dc
                        .Add("@IDServicio_Det", vintIDServicio_Det)
                        .Add("@NroPax", vintNroPax)
                        .Add("@pMonto", 0)
                    End With

                    Return objADO.ExecuteSPOutput("MASERVICIOS_DET_RANGO_APT_SelCostoOutput", dc)

                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class

        '------------------------------------------------------------

    End Class

    '------------------------------------------------------------
    <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsDescRangosAPTServicioProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub InsertarActualizar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)
            Try

                For Each Item As clsServicioProveedorBE.clsDescRangosAPTServicioProveedorBE In BE.ListaDescRangosAPT

                    dc = New Dictionary(Of String, String)

                    dc.Add("@IDServicio", BE.IDServicio)
                    dc.Add("@IDTemporada", Item.IDTemporada)
                    dc.Add("@DescripcionAlterna", Item.DescripcionAlterna)
                    dc.Add("@AgruparServicio", Item.AgruparServicio)
                    dc.Add("@PoliticaLiberado", Item.PoliticaLiberado)
                    dc.Add("@Liberado", Item.Liberado)
                    dc.Add("@TipoLib", Item.TipoLib)
                    dc.Add("@MaximoLiberado", Item.MaximoLiberado)
                    dc.Add("@MontoL", Item.MontoL)
                    dc.Add("@UserMod", Item.UserMod)
                    objADO.ExecuteSP("MASERVICIOS_DESC_APT_InsUpd", dc)
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class
    '------------------------------------------------------------

    <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsRangosAPTServicioProveedorBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT
        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)
            Try

                For Each Item As clsServicioProveedorBE.clsRangosAPTServicioProveedorBE In BE.ListaRangosAPT

                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        If Item.Accion = "M" Then _
                            dc.Add("@Correlativo", Item.Correlativo)

                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@PaxDesde", Item.PaxDesde)
                        dc.Add("@PaxHasta", Item.PaxHasta)
                        dc.Add("@Monto", Item.Monto)
                        dc.Add("@UserMod", Item.UserMod)
                        dc.Add("@IDTemporada", Item.IDTemporada)

                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MASERVICIOS_RANGO_APT_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MASERVICIOS_RANGO_APT_Upd", dc)
                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@Correlativo", Item.Correlativo)
                        dc.Add("@IDTemporada", Item.IDTemporada)
                        objADO.ExecuteSP("MASERVICIOS_RANGO_APT_Del", dc)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class
    '------------------------------------------------------------

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTextoServicioProveedorBT

        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarCopiaxIDServicio(ByVal vstrIDServicio As String, ByVal vstrIDServicioNuevo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@IDServicioNuevo", vstrIDServicioNuevo)
                objADO.ExecuteSP("MATEXTOSERVICIOSInsxIDServicio", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)

            Try
                For Each Item As clsServicioProveedorBE.clsTextoServicioProveedorBE In BE.ListTextos
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then

                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@IDIDioma", Item.IDIDioma)
                        dc.Add("@Titulo", Item.Titulo)
                        dc.Add("@Texto", Item.Texto)
                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MATEXTOSERVICIOS_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MATEXTOSERVICIOS_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@IDIDioma", Item.IDIDioma)
                        objADO.ExecuteSP("MATEXTOSERVICIOS_Del", dc)
                    End If
                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub CopiarxServicio(ByVal vstrIDServicio As String, _
                                   ByVal vstrIDServicioNue As String, ByVal vstrUserMod As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@UserMod", vstrUserMod)
                dc.Add("@IDServicioNue", vstrIDServicioNue)

                objADO.ExecuteSP("MATEXTOSERVICIOS_Ins_Copiar", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub



    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsServiciosAtencionBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Sub Insertar(ByVal BE As clsServicioProveedorBE.clsServiciosAtencionBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDAtencion", BE.IDAtencion)
                    .Add("@IDservicio", BE.IDServicio)
                    .Add("@Dia1", BE.Dia1)
                    .Add("@Dia2", BE.Dia2)
                    .Add("@Desde1", BE.Desde1)
                    .Add("@Hasta1", BE.Hasta1)
                    .Add("@Desde2", BE.Desde2)
                    .Add("@Hasta2", BE.Hasta2)
                    .Add("@UserMod", BE.UserMod)
                End With

                objADO.ExecuteSP("MASERVICIOS_ATENCION_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Actualizar(ByVal BE As clsServicioProveedorBE.clsServiciosAtencionBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDAtencion", BE.IDAtencion)
                    .Add("@IDservicio", BE.IDServicio)
                    .Add("@Dia1", BE.Dia1)
                    .Add("@Dia2", BE.Dia2)
                    .Add("@Desde1", BE.Desde1)
                    .Add("@Hasta1", BE.Hasta1)
                    .Add("@Desde2", BE.Desde2)
                    .Add("@Hasta2", BE.Hasta2)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("MASERVICIOS_ATENCION_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal vintIDAtencion As Int16, ByVal vstrIDServicio As String)
            Dim dc As New Dictionary(Of String, String)
            Try

                dc.Add("@IDAtencion", vintIDAtencion)
                dc.Add("@IDServicio", vstrIDServicio)

                objADO.ExecuteSP("MASERVICIOS_ATENCION_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Try
                Dim strIDServicio As String = BE.IDServicio
                For Each Item As clsServicioProveedorBE.clsServiciosAtencionBE In BE.ListaServAtencion
                    Item.IDServicio = strIDServicio
                    If Item.Accion = "N" Then
                        Insertar(Item)
                    ElseIf Item.Accion = "M" Then
                        Actualizar(Item)
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDAtencion, Item.IDServicio)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarCopiaxIDServicio(ByVal vstrIDServicio As String, ByVal vstrIDServicioNuevo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@IDServicioNuevo", vstrIDServicioNuevo)
                objADO.ExecuteSP("MASERVICIOS_ATENCIONInsxIDServicio", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub


    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsServiciosporDiaBT

        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarCopiaxIDServicio(ByVal vstrIDServicio As String, ByVal vstrIDServicioNuevo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@IDServicioNuevo", vstrIDServicioNuevo)
                objADO.ExecuteSP("MASERVICIOS_DIAInsxIDServicio", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)

            Try
                For Each Item As clsServicioProveedorBE.clsServiciosporDiaBE In BE.ListServxDia
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then

                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@IDIDioma", Item.IDIdioma)
                        dc.Add("@Dia", Item.Dia)
                        'dc.Add("@DescripCorta", Item.DescripCorta)
                        dc.Add("@Descripcion", Item.Descripcion)

                        dc.Add("@NoHotel", Item.NoHotel)
                        dc.Add("@TxWebHotel", Item.TxWebHotel)


                        dc.Add("@TxDireccHotel", Item.TxDireccHotel)
                        dc.Add("@TxTelfHotel1", Item.TxTelfHotel1)
                        dc.Add("@TxTelfHotel2", Item.TxTelfHotel2)
                        dc.Add("@FeHoraChkInHotel", Format(Item.FeHoraChkInHotel, "dd/MM/yyyy HH:mm:ss"))
                        dc.Add("@FeHoraChkOutHotel", Format(Item.FeHoraChkOutHotel, "dd/MM/yyyy HH:mm:ss"))
                        dc.Add("@CoTipoDesaHotel", Item.CoTipoDesaHotel)
                        dc.Add("@CoCiudadHotel", Item.CoCiudadHotel)

                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MASERVICIOS_DIA_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MASERVICIOS_DIA_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@IDIDioma", Item.IDIdioma)
                        dc.Add("@Dia", Item.Dia)

                        objADO.ExecuteSP("MASERVICIOS_DIA_Del", dc)

                    End If
                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsServiciosAlimentacionporDiaBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarCopiaxIDServicio(ByVal vstrIDServicio As String, ByVal vstrIDServicioNuevo As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@IDServicioNuevo", vstrIDServicioNuevo)
                objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIAInsxIDServicio", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)
            Try
                For Each Item As clsServicioProveedorBE.clsServiciosAlimentacionporDiaBE In BE.ListServAlimxDia
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then

                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@Dia", Item.Dia)

                        dc.Add("@Desayuno", Item.Desayuno)
                        dc.Add("@Lonche", Item.Lonche)
                        dc.Add("@Almuerzo", Item.Almuerzo)
                        dc.Add("@Cena", Item.Cena)
                        dc.Add("@IDUbigeoOri", Item.IDUbigeoOri)
                        dc.Add("@IDUbigeoDes", Item.IDUbigeoDes)

                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIA_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIA_Upd", dc)

                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDServicio", BE.IDServicio)
                        dc.Add("@Dia", Item.Dia)

                        objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIA_Del", dc)

                    End If
                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class


    <ComVisible(True)> _
  <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsServicios_HotelBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsServicioProveedorBE.clsServicios_HotelBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc


                    .Add("@CoServicio", BE.CoServicio)
                    .Add("@CoConcepto", BE.CoConcepto)
                    .Add("@FlServicio", BE.FlServicio)
                    .Add("@FlCosto", BE.FlCosto)
                    .Add("@CoTipoMenu", BE.CoTipoMenu)
                    .Add("@FlFijoPortable", BE.FlFijoPortable)
                    .Add("@FlPortable", BE.FlPortable)
                    .Add("@CoTipoBanio", BE.CoTipoBanio)
                    .Add("@CoEstadoHab", BE.CoEstadoHab)
                    .Add("@TxPisos", BE.TxPisos)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MASERVICIOS_HOTEL_Ins", dc)

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsServicioProveedorBE.clsServicios_HotelBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc

                    .Add("@CoServicio", BE.CoServicio)
                    .Add("@CoConcepto", BE.CoConcepto)
                    .Add("@FlServicio", BE.FlServicio)
                    .Add("@FlCosto", BE.FlCosto)
                    .Add("@CoTipoMenu", BE.CoTipoMenu)
                    .Add("@FlFijoPortable", BE.FlFijoPortable)
                    .Add("@FlPortable", BE.FlPortable)
                    .Add("@CoTipoBanio", BE.CoTipoBanio)
                    .Add("@CoEstadoHab", BE.CoEstadoHab)
                    .Add("@TxPisos", BE.TxPisos)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MASERVICIOS_HOTEL_Upd", dc)

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)
            Try
                For Each Item As clsServicioProveedorBE.clsServicios_HotelBE In BE.ListaServicios_Hoteles
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then


                        If Item.Accion = "N" Then
                            Item.CoServicio = BE.IDServicio
                            Insertar(Item)
                        ElseIf Item.Accion = "M" Then
                            Item.CoServicio = BE.IDServicio
                            Actualizar(Item)

                        End If
                        'ElseIf Item.Accion = "B" Then
                        '    dc.Add("@IDServicio", BE.IDServicio)
                        '    dc.Add("@Dia", Item.Dia)

                        '    objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIA_Del", dc)

                    End If
                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    '----------------------

    <ComVisible(True)> _
 <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsServicios_Operaciones_HotelesBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsServicioProveedorBE.clsServicios_Operaciones_HotelesBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc

                    .Add("@CoServicio", BE.CoServicio)
                    .Add("@FlRecepcion", BE.FlRecepcion)
                    .Add("@FlEstacionamiento", BE.FlEstacionamiento)
                    .Add("@FlBoxDesayuno", BE.FlBoxDesayuno)
                    .Add("@FlRestaurantes", BE.FlRestaurantes)
                    .Add("@FlRestaurantesSoloDesayuno", BE.FlRestaurantesSoloDesayuno)
                    .Add("@QtRestCantidad", BE.QtRestCantidad)
                    .Add("@QtRestCapacidad", BE.QtRestCapacidad)
                    .Add("@QtTriples", BE.QtTriples)
                    .Add("@QtNumHabitaciones", BE.QtNumHabitaciones)
                    .Add("@CoTipoCama", BE.CoTipoCama)
                    .Add("@TxObservacion", BE.TxObservacion)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MASERVICIOS_OPERACIONES_HOTELES_Ins", dc)

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsServicioProveedorBE.clsServicios_Operaciones_HotelesBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc

                    .Add("@CoServicio", BE.CoServicio)
                    .Add("@FlRecepcion", BE.FlRecepcion)
                    .Add("@FlEstacionamiento", BE.FlEstacionamiento)
                    .Add("@FlBoxDesayuno", BE.FlBoxDesayuno)
                    .Add("@FlRestaurantes", BE.FlRestaurantes)
                    .Add("@FlRestaurantesSoloDesayuno", BE.FlRestaurantesSoloDesayuno)
                    .Add("@QtRestCantidad", BE.QtRestCantidad)
                    .Add("@QtRestCapacidad", BE.QtRestCapacidad)
                    .Add("@QtTriples", BE.QtTriples)
                    .Add("@QtNumHabitaciones", BE.QtNumHabitaciones)
                    .Add("@CoTipoCama", BE.CoTipoCama)
                    .Add("@TxObservacion", BE.TxObservacion)
                    .Add("@UserMod", BE.UserMod)

                End With

                objADO.ExecuteSP("MASERVICIOS_OPERACIONES_HOTELES_Upd", dc)

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)
            Try
                Dim item As clsServicioProveedorBE.clsServicios_Operaciones_HotelesBE = BE.ObjServicios_Operaciones_Hoteles
                ' For Each Item As clsServicioProveedorBE.clsServicios_HotelBE In BE.ListaServicios_Hoteles
                dc = New Dictionary(Of String, String)
                If item.Accion = "M" Or item.Accion = "N" Then


                    If item.Accion = "N" Then
                        item.CoServicio = BE.IDServicio
                        Insertar(item)
                    ElseIf item.Accion = "M" Then
                        item.CoServicio = BE.IDServicio
                        Actualizar(item)

                    End If
                    'ElseIf Item.Accion = "B" Then
                    '    dc.Add("@IDServicio", BE.IDServicio)
                    '    dc.Add("@Dia", Item.Dia)

                    '    objADO.ExecuteSP("MASERVICIOS_ALIMENTACION_DIA_Del", dc)

                End If
                ' Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class


    '----------------------------

    '----------------------

    <ComVisible(True)> _
 <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsServicios_Hotel_HabitacionesBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub Insertar(ByVal BE As clsServicioProveedorBE.clsServicios_Hotel_HabitacionesBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc

                    .Add("@CoServicio", BE.CoServicio)
                    .Add("@CoTipoHab", BE.CoTipoHab)
                    .Add("@QtCantidad", BE.QtCantidad)
                    .Add("@QtCantMatri", BE.QtCantMatri)
                    .Add("@CoTipCama_Matri", BE.CoTipCama_Matri)
                    .Add("@QtCantTwin", BE.QtCantTwin)
                    .Add("@CoTipCama_Twin", BE.CoTipCama_Twin)
                    .Add("@CoTipCama_Adic", BE.CoTipCama_Adic)
                    .Add("@QtCantTriple", BE.QtCantTriple)
                    .Add("@CoTipCama_Triple", BE.CoTipCama_Triple)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@CoTipoHab_Oper", BE.CoTipoHab_Oper)
                End With

                objADO.ExecuteSP("MASERVICIOS_HOTEL_HABITACIONES_Ins", dc)

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsServicioProveedorBE.clsServicios_Hotel_HabitacionesBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc

                    .Add("@CoServicio", BE.CoServicio)
                    .Add("@NuHabitacion", BE.NuHabitacion)
                    .Add("@CoTipoHab", BE.CoTipoHab)
                    .Add("@QtCantidad", BE.QtCantidad)
                    .Add("@QtCantMatri", BE.QtCantMatri)
                    .Add("@CoTipCama_Matri", BE.CoTipCama_Matri)
                    .Add("@QtCantTwin", BE.QtCantTwin)
                    .Add("@CoTipCama_Twin", BE.CoTipCama_Twin)
                    .Add("@CoTipCama_Adic", BE.CoTipCama_Adic)
                    .Add("@QtCantTriple", BE.QtCantTriple)
                    .Add("@CoTipCama_Triple", BE.CoTipCama_Triple)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@CoTipoHab_Oper", BE.CoTipoHab_Oper)

                End With

                objADO.ExecuteSP("MASERVICIOS_HOTEL_HABITACIONES_Upd", dc)

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal BE As clsServicioProveedorBE.clsServicios_Hotel_HabitacionesBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc

                    .Add("@CoServicio", BE.CoServicio)
                    .Add("@NuHabitacion", BE.NuHabitacion)

                End With

                objADO.ExecuteSP("MASERVICIOS_HOTEL_HABITACIONES_Del", dc)

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsServicioProveedorBE)
            Dim dc As Dictionary(Of String, String)
            Try
                'Dim item As clsServicioProveedorBE.clsServicios_Operaciones_HotelesBE = BE.ObjServicios_Operaciones_Hoteles
                For Each Item As clsServicioProveedorBE.clsServicios_Hotel_HabitacionesBE In BE.ListaServicios_Hoteles_Hab
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then


                        If Item.Accion = "N" Then
                            Item.CoServicio = BE.IDServicio
                            Insertar(Item)
                        ElseIf Item.Accion = "M" Then
                            Item.CoServicio = BE.IDServicio
                            Actualizar(Item)

                        End If
                    ElseIf Item.Accion = "B" Then
                        Item.CoServicio = BE.IDServicio
                        Eliminar(Item)
                    End If
                Next

                ContextUtil.SetComplete()

            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class
End Class
