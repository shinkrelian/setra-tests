﻿Imports ComDataAccessD2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsProveedorBN
    Inherits ServicedComponent

    Dim objADO As New clsDataAccessDN
    Public Function ConsultarList(ByVal vstrIDProveedor As String, ByVal vstrNumDocumento As String, ByVal vstrRazonSocial As String, _
                                  ByVal vstrIDTipoProv As String, ByVal vstrRazonComercial As String, ByVal vstrIDCiudad As String, ByVal vstrIDNacionalidad As String _
                                  , Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@NumIdentidad", vstrNumDocumento)
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@RazonComercial", vstrRazonComercial)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@Guia_Nacionalidad", vstrIDNacionalidad)
            dc.Add("@CoPais", vstrCoPais)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarPk(ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDProveedor", vstrIDProveedor)

        Dim strSql As String = "MAPROVEEDORES_Sel_Pk"
        Return objADO.GetDataReader(strSql, dc)
    End Function

    Public Function ConsultarListEmailsBiblia(ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDProveedor", vstrIDProveedor)

        Dim strSql As String = "MACORREOSBIBLIAPROVEEDOR_Sel_ListxIDProveedor"
        Return objADO.GetDataReader(strSql, dc)
    End Function

    Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@ID", vstrID)
            dc.Add("@Descripcion", vstrDescripcion)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_Lvw", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarLvw_CorreosTodos(ByVal vstrDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try

            dc.Add("@Descripcion", vstrDescripcion)

            Return objADO.GetDataTable(True, "ListarCorreos_Todos", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarCbo(ByVal vstrIDTipoProv As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_Cbo", dc)

        Catch ex As Exception
            Throw
        End Try

    End Function
    Public Function ConsultarCboGuias() As DataTable
        Try
            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_CboGuias", New Dictionary(Of String, String))
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strIDProveedorxDominio(ByVal vstrDominio As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@dominio", vstrDominio)
            dc.Add("@pIDProveedor", "")

            Return objADO.ExecuteSPOutput("MAPROVEEDORES_Sel_OutPutxDominio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCbo(ByVal vstrIDTipoProv As String, ByVal vstrIDPais As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@IDPais", vstrIDPais)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_CboxIDPais", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCbo(ByVal vstrIDTipoProv As String, ByVal vblnTodos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@IDTipoProv", vstrIDTipoProv)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_CboTodos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboNoGuiasNoTrans(ByVal vstrIDTipoProv As String, ByVal vblnTodos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@IDTipoProv", vstrIDTipoProv)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_CboTodosNoGuiasNoTrans", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnProveedorInterno(ByVal vstrIDProveedor As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@pbInterno", 0)
            Return objADO.GetSPOutputValue("MAPROVEEDORES_SelxProvInternoOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnProveedorDelExterior(ByVal vstrCoProveedor As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@pDelExterior", 0)
            Return objADO.GetSPOutputValue("MAPROVEEDORES_SelEsDelExterior", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarRazonComercialLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@ID", vstrID)
            dc.Add("@Descripcion", vstrDescripcion)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_LvwxRazonComercial", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarCadenasCbo() As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try

            Return objADO.GetDataTable(True, "MAPROVEEDORES_SelCadenas_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarListAyuda(ByVal vstrIDTipoProv As String, ByVal vstrRazonSocial As String, _
                                        ByVal vstrIDCiudad As String, ByVal vstrIDCat As String, _
                                        ByVal vbytCategoria As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@IDCat", vstrIDCat)
            dc.Add("@Categoria", vbytCategoria)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_ListAyuda", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarListAyuda_RUC(ByVal vstrIDTipoProv As String, ByVal vstrRazonSocial As String, _
                                      ByVal vstrIDCiudad As String, ByVal vstrIDCat As String, _
                                      ByVal vbytCategoria As Byte, ByVal vstrRUC As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@IDCat", vstrIDCat)
            dc.Add("@Categoria", vbytCategoria)
            dc.Add("@NumIdentidad", vstrRUC)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_ListAyuda_RUC", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarListAyuda_CC(ByVal vstrIDTipoProv As String, ByVal vstrRazonSocial As String, _
                                       ByVal vstrIDCiudad As String, ByVal vstrIDCat As String, _
                                       ByVal vbytCategoria As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@IDCat", vstrIDCat)
            dc.Add("@Categoria", vbytCategoria)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_ListAyuda_CentroCostos", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarListAyuda_Counter(ByVal vstrIDTipoProv As String, ByVal vstrRazonSocial As String, _
                                      ByVal vstrIDCiudad As String, ByVal vstrIDCat As String, _
                                      ByVal vbytCategoria As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@IDCat", vstrIDCat)
            dc.Add("@Categoria", vbytCategoria)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_ListAyuda_NotaVenta_Counter", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    'Public Function ConsultarListAyuda_Guias(ByVal vstrRazonSocial As String, _
    '                                   ByVal vstrIDCiudad As String, ByVal vstrIDCat As String, _
    '                                   ByVal vbytCategoria As Byte) As DataTable
    Public Function ConsultarListAyuda_Guias(ByVal vstrRazonSocial As String, _
                                      ByVal vstrIDCiudad As String, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try

            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@CoPais", vstrCoPais)
            'dc.Add("@IDCat", vstrIDCat)
            'dc.Add("@Categoria", vbytCategoria)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_ListAyuda_Guias", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarRpt(ByVal vstrNumIdentidad As String, _
                                 ByVal vstrRazonSocial As String, ByVal vstrIDTipoProv As String, _
                                 ByVal vstrRazonComercial As String, Optional ByVal vstrIDCiudad As String = "", Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NumIdentidad", vstrNumIdentidad)
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@RazonComercial", vstrRazonComercial)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@CoPais", vstrCoPais)
            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptGeneral(ByVal vstrPersona As String, ByVal vstrIDTipoProv As String, _
                                 ByVal vstrIDTipoOper As String, ByVal vstrIDCiudad As String, _
                                 ByVal vbooDomiciliado As Boolean, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Persona", vstrPersona)
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@IDTipoOper", vstrIDTipoOper)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@Domiciliado", vbooDomiciliado)
            dc.Add("@CoPais", vstrCoPais)
            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_RptGeneral", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptHGEN(ByVal vstrIDTipoProv As String, ByVal vstrIDCiudad As String, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@CoPais", vstrCoPais)
            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_Rpt_HGEN", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTipoTransporteCbo(ByVal vchrTipoTransporte As Char, ByVal vbyttodos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@TipoTransporte", vchrTipoTransporte)
            dc.Add("@btodos", vbyttodos)
            Return objADO.GetDataTable(True, "MASERVICIOS_Sel_xTipoTransporte_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTipoOperInterno(Optional ByVal vintIDCab As Integer = 0) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "MAPROVEEDORES_SelxProvInterno", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveIDProveedorxTrasladista(ByVal vstrUsuario As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDUsuario", vstrUsuario)
            dc.Add("@pIDProveedor", String.Empty)

            Return objADO.GetSPOutputValue("MAPROVEEDOR_Sel_xIDUsuarioTrasladista", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarEsCadena(ByVal vstrIDProveedor As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnEsCadena As Boolean = False
            With dc
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@pblnEsCadena", False)
            End With

            blnEsCadena = CBool(objADO.GetSPOutputValue("MAPROVEEDORES_EsCadenaSelOutput", dc))

            Return blnEsCadena
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveIDProveedorxCorreo(ByVal vstrCorreo As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strIDProveedor As String = ""
            dc.Add("@eMail", vstrCorreo)
            dc.Add("@pIDProveedor", "")

            strIDProveedor = objADO.GetSPOutputValue("MAPROVEEDOR_SelIDProveedorxMail", dc)
            Return strIDProveedor
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarGuias(ByVal vstrIDUbigeo As String, ByVal vstrIDIoma As String, ByVal vstrGuiasEstado As String, _
                                   ByVal vstrNombre As String, ByVal vstrCoPais As String, ByVal vstrTipoProv As String, _
                                   ByVal vbytRatingMin As Byte, ByVal vbytRatingMax As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUbigeo", vstrIDUbigeo)
                .Add("@IDIdioma", vstrIDIoma)
                .Add("@Guia_Estado", vstrGuiasEstado)
                .Add("@Nombre", vstrNombre)
                .Add("@CoPais", vstrCoPais)
                .Add("@IDTipoProv", vstrTipoProv)
                .Add("@RatingMin", vbytRatingMin)
                .Add("@RatingMax", vbytRatingMax)
            End With

            Return objADO.GetDataTable(True, "MAPROVEEDOR_Sel_ConsGuias", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarSbRptCommentGuias(ByVal vstrIDProveedor As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@NivelFeedBack", String.Empty)
            End With

            Return objADO.GetDataTable(True, "MAPROVEEDOR_Sel_SbRptConsGuia", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDTipoProv(ByVal vstrDescripcion As String, ByVal vstrIDCiudad As String, _
                                             ByVal vstrIDPais As String, ByVal vstrIDTipoProveedor As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Descripcion", vstrDescripcion)
            dc.Add("@IDUbigeo", vstrIDCiudad)
            dc.Add("@IDPais", vstrIDPais)
            dc.Add("@IDTipoProveedor", vstrIDTipoProveedor)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_Sel_ListxIDTipoProv", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxSigla(ByVal vstrSigla As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Sigla", vstrSigla)

            Return objADO.GetDataTable(True, "MAPROVEEDORES_SelxSigla", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarOficinaFacturacion(ByVal vstrCoUbigeoOficina As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeoOficina)
            Return objADO.GetDataReader("MAPROVEEDORES_SelOficinaFact", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsAdministProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                Return objADO.GetDataTable(True, "MAADMINISTPROVEEDORES_SelxIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarxIDServicio_Detracciones(ByVal vstrIDServicio As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                Return objADO.GetDataTable(True, "MAADMINISTPROVEEDORES_Sel_List_Detrac_PorServicio", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                Return objADO.GetDataTable(True, "MAADMINISTPROVEEDORES_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo2(ByVal vstrIDProveedor As String, Optional ByVal vstrIDMoneda As String = "") As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@IDMoneda", vstrIDMoneda)
                Return objADO.GetDataTable(True, "MAADMINISTPROVEEDORES_Sel_Cbo2", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarxIDProveedorxCta(ByVal vstrIDProveedor As String, ByVal vstrCta As String) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Cta", vstrCta)
                dc.Add("@pNombreBanco", "")

                Return objADO.GetSPOutputValue("MAADMINISTPROVEEDORES_SelxIDProveedorxCta", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsEntrevistaProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MAENTREVISTAPROVEEDOR_SelxIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsPoliticasProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarExistePolitica(ByVal vstrIDProveedor As String) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@pExistePolit", False)

                Return objADO.GetSPOutputValue("MAPROVEEDORES_blnExistePolitica", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MAPOLITICAPROVEEDORES_SelxIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Private Function ConsultarPrepagos(ByVal vstrIDProveedor As String, ByVal vbytCantidad As Byte, _
                                            ByVal vdatFechaInicio As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                'dc.Add("@IDReserva", vintIDReserva)
                'dc.Add("@IDCab", vintIDcab)}
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Cantidad", vbytCantidad)
                dc.Add("@FechaInicio", vdatFechaInicio)

                'Return objADO.GetDataTable(True, "MAPOLITICASPROVEEDORES_Sel_IDReservaOrdPago", dc)
                Return objADO.GetDataTable(True, "MAPOLITICASPROVEEDORES_SelPrepagosxCantidad", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Private Function datFechaHoy() As Date
            Try
                Dim dte_vFecha As Date
                Dim dc As New Dictionary(Of String, String)
                Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("FechaHoy", dc)
                    dr.Read()
                    dte_vFecha = dr(0)
                    dr.Close()
                End Using
                Return dte_vFecha
            Catch ex As Exception
                Throw
            End Try
        End Function
        Private Function dtConsultarPk(ByVal vintIDPolitica As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDPolitica", vintIDPolitica)

            Return objADO.GetDataTable(True, "MAPOLITICAPROVEEDORES_Sel_Pk", dc)

        End Function
        Public Function drwCargarValoresPrepago(ByVal vstrIDProveedor As String, _
                                        ByVal vintCantidad As Int16, _
                                        ByVal vdatFechaInicio As Date, _
                                        ByRef rdblPorcentaje As Double, _
                                        ByRef rdblMonto As Double, _
                                        ByRef rdatFechaPago As Date) As DataRow
            Try
                Dim dt As DataTable = ConsultarPrepagos(vstrIDProveedor, vintCantidad, vdatFechaInicio)

                rdblPorcentaje = 100
                rdblMonto = 0
                Dim datHoy As Date = FormatDateTime(datFechaHoy(), DateFormat.ShortDate)
                rdatFechaPago = datHoy
                Dim dr As DataRow = Nothing
                If dt.Rows.Count = 1 Then
                    rdblPorcentaje = dt(0)("Porcentaje")
                    rdblMonto = dt(0)("Monto")
                    rdatFechaPago = dt(0)("FechaPago")

                    Dim dt2 As DataTable = dtConsultarPk(dt(0)("IDPolitica"))
                    If dt2.Rows.Count > 0 Then
                        If dt2(0)("PoliticaFechaReserva") = True Then
                            rdatFechaPago = datHoy
                        End If

                        dr = dt2(0)

                    End If
                End If


                Return dr
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsAtencionProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MAATENCION_MAPROVEEDOR_SelxIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsRangosAPTProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN


        Public Function ConsultarListxIDDetalle(ByVal vstrIDProveedor As String, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MAPROVEEDORES_RANGO_APT_Sel_xIDProveedor", dc, vCnn)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarListxIDDetalle_Anio(ByVal vstrIDProveedor As String, ByVal vstrNuAnio As String, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@NuAnio", vstrNuAnio)

                Return objADO.GetDataTable(True, "MAPROVEEDORES_RANGO_APT_Sel_xIDProveedor_Anio", dc, vCnn)
            Catch ex As Exception
                Throw
            End Try

        End Function

        'Public Function blnExisteRangos(ByRef vLstRangos As List(Of clsProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax)) As Boolean

        '    Try
        '        Dim objServBT As New clsServicioProveedorBT
        '        Dim blnOkTotal As Boolean = True
        '        For Each ItemServPax As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax In vLstRangos
        '            Dim blnOk As Boolean = True
        '            Dim dttDetServ As DataTable = objServBT.dttConsultarxIDServicioAnio(ItemServPax.IDServicio, ItemServPax.Anio, ItemServPax.Variante)
        '            For Each drDetServ As DataRow In dttDetServ.Rows
        '                If IsDBNull(drDetServ("Monto_sgl")) Then
        '                    If Not blnExisteRango(drDetServ("IDProveedor"), ItemServPax.NroPax, ItemServPax.NroLiberados) Then
        '                        'Return False
        '                        blnOk = False
        '                        blnOkTotal = False
        '                    End If
        '                End If
        '                ItemServPax.Existe = blnOk
        '            Next

        '        Next

        '        Return blnOkTotal

        '    Catch ex As Exception
        '        Throw
        '    End Try

        'End Function

        Private Function blnExisteRango(ByVal vstrIDProveedor As String, ByVal vstrNuAnio As String, _
                                       ByVal vintPax As Int16, ByVal vintLiberados As Int16) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", vstrIDProveedor)
                    .Add("@NuAnio", vstrNuAnio)
                    .Add("@NroPax", vintPax)
                    .Add("@NroLiberados", vintLiberados)
                    .Add("@pExiste", 0)
                End With

                Return objADO.ExecuteSPOutput("MAPROVEEDORES_RANGO_APT_Sel_ExisteRangoOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsContactoProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDProveedor As String, ByVal vstrNombres As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Nombres", vstrNombres)

                Return objADO.GetDataTable(True, "MACONTACTOSPROVEEDOR_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarCbo(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MACONTACTOSPROVEEDOR_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDContacto As String, ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDContacto", vstrIDContacto)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Dim strSql As String = "MACONTACTOSPROVEEDOR_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarLvw(ByVal vstrIDContacto As String, ByVal vstrIDProveedor As String, _
                                     ByVal vstrNombre As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDContacto", vstrIDContacto)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Nombre", vstrNombre)

                Return objADO.GetDataTable(True, "MACONTACTOSPROVEEDOR_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarxProveedorLvw(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try

                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MACONTACTOSPROVEEDOR_Sel_xIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarContactosxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MACONTACTOSPROVEEDOR_SelListxIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarSbRp(ByVal vstrPersona As String, ByVal vstrIDTipoProv As String, _
                                      ByVal vstrIDTipoOper As String, ByVal vstrIDCiudad As String, _
                                      ByVal vbooDomiciliado As Boolean, Optional ByVal vstrCoPais As String = "") As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Persona", vstrPersona)
                dc.Add("@IDTipoProv", vstrIDTipoProv)
                dc.Add("@IDTipoOper", vstrIDTipoOper)
                dc.Add("@IDCiudad", vstrIDCiudad)
                dc.Add("@Domiciliado", vbooDomiciliado)
                dc.Add("@CoPais", vstrCoPais)
                Return objADO.GetDataTable(True, "MACONTACTOSPROVEEDOR_Sel_SbRpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function blnExisteUsuarioExtranet(ByVal vstrIDProveedor As String, ByVal vstrIDContacto As String, ByVal vchrTablaPC As String, ByVal vstrUsuario As String) As Boolean
            Try
                Dim blnExiste As Boolean = False
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDContacto", vstrIDContacto)
                dc.Add("@IDTablaParent", vstrIDProveedor)
                dc.Add("@TipoTablaParent", vchrTablaPC)
                dc.Add("@CodUsuario", vstrUsuario)
                dc.Add("@pExists", False)

                blnExiste = objADO.GetSPOutputValue("MACONTACTOSPROVCLIE_Exists_UsuarioExtranet", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIdiomaProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDIdioma As String, ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDIdioma", vstrIDIdioma)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MAIDIOMASPROVEEDOR_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function


        Public Function ConsultarPk(ByVal vstrIDIdioma As String, ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDIdioma", vstrIDIdioma)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Dim strSql As String = "MAIDIOMASPROVEEDOR_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsEspecialidadProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", vstrIDProveedor)

                    Return objADO.GetDataTable(True, "MAESPECIALIDADPROVEEDOR_Sel_List", dc)
                End With
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsNacionalidadProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", vstrIDProveedor)

                    Return objADO.GetDataTable(True, "MANACIONALIDADPROVEEDOR_Sel_List", dc)
                End With
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsComentarioProveedorBN
        Inherits ServicedComponent

        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDProveedor As String, ByVal vstrIDFile As String, _
                                      ByVal vstrComentario As String, ByVal vdatFechaIni As Date, _
                                      ByVal vdatFechaFin As Date) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@IDFile", vstrIDFile)
                dc.Add("@Comentario", vstrComentario)
                dc.Add("@FechaIni", vdatFechaIni)
                dc.Add("@FechaFin", vdatFechaFin)


                Return objADO.GetDataTable(True, "MACOMENTARIOSPROVEEDOR_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function


        Public Function ConsultarPk(ByVal vstrIDComentario As String, ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDComentario", vstrIDComentario)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Dim strSql As String = "MACOMENTARIOSPROVEEDOR_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function


    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCorreosBibliaProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function blnExisteCorreoxIDProveedor(ByVal vstrIDProveedor As String, ByVal vstrCorreo As String) As Boolean
            Try
                Dim blnExiste As Boolean = False
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Correo", vstrCorreo)
                dc.Add("@pExists", False)

                blnExiste = objADO.GetSPOutputValue("MACORREOSBIBLIAPROVEEDOR_Sel_ExistsPK", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Dim strSql As String = "MACORREOSBIBLIAPROVEEDOR_Sel_ListxIDProveedor"
            Return objADO.GetDataTable(True, strSql, dc)
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCorreoReservasProveedorBN
        Inherits ServicedComponent

        Dim objADO As New clsDataAccessDN
        Public Function ConultarList(ByVal vstrIDProveedor As String, ByVal vstrCorreo As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Correo", vstrCorreo)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MACORREOREVPROVEEDOR_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function ConsultarCbo(ByVal vstrIDProveedor As String) As DataTable
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDProveedor", vstrIDProveedor)

        '        Return objADO.GetDataTable(True, "MACORREOREVPROVEEDOR_Sel_Cbo", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function
        Public Function ConsultarCorreosFiltroBandejas(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MACORREOREVPROVEEDOR_Sel_FiltroBandejas", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class


    <ComVisible(True)> _
  <Transaction(TransactionOption.NotSupported)> _
    Public Class clsProveedores_TipoDetraccionBN
        Inherits ServicedComponent

        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@CoProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "MAPROVEEDORES_MATIPODETRACCION_Sel_List_Prov", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class


End Class
