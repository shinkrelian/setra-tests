﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoProveedorBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN

    Public Function ConsultarVouchers_xFile(ByVal vstrIDFile As String, ByVal vstrProveedor As String, _
                                            ByVal vstrCoEstado As String, _
                                            Optional ByVal vstrNuFormaEgreso As String = "", _
                                            Optional ByVal vstrNuDocum As String = "", _
                                            Optional ByVal vstrTipoVouOrd As String = "", _
                                            Optional ByVal vblnSoloObservados As Boolean = False, _
                                            Optional ByVal vstrCoUbigeo_Oficina As String = "", _
                                            Optional ByVal vstrNumIdentidad As String = "", _
                                            Optional ByVal vblnFilesHistoricos As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@IDFile", vstrIDFile)
                .Add("@Proveedor", vstrProveedor)
                .Add("@NuVoucher", vstrNuFormaEgreso)
                .Add("@NuDocum", If(IsNothing(vstrNuDocum), "", vstrNuDocum))
                .Add("@TipoVouOrd", vstrTipoVouOrd)
                .Add("@CoEstado", vstrCoEstado)
                .Add("@SoloObservados", vblnSoloObservados)
                .Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
                .Add("@NumIdentidad", vstrNumIdentidad)
                .Add("@FlHistorico", vblnFilesHistoricos)
            End With
            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelVouchers_File_Proveedor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListDocEgresoxProveedor(ByVal vstrCoProveedor As String, Optional ByVal vstrCoMoneda As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDProveedor", vstrCoProveedor)
            dc.Add("@CoMoneda", vstrCoMoneda)

            Return objADO.GetDataTable(True, "DOCUMENTO_FORMA_EGRESO_Sel_List_Proveedor", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDocumentos_Rpt(ByVal vdatFecha1 As Date, ByVal vdatFecha2 As Date, ByVal vstrIDFile As String, ByVal vstrProveedor As String, _
                                         ByVal vstrCoEstado As String, _
                                         Optional ByVal vstrTipoVouOrd As String = "", Optional ByVal vintPais As Integer = 0, Optional ByVal vblnSinDocs As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@Fecha1", vdatFecha1)
                .Add("@Fecha2", vdatFecha2)
                .Add("@IDFile", vstrIDFile)
                .Add("@IDProveedor", vstrProveedor)

                .Add("@TipoVouOrd", vstrTipoVouOrd)
                .Add("@CoEstado", vstrCoEstado)
                .Add("@Pais", vintPais)
                .Add("@FlSinDocumentos", vblnSinDocs)

            End With
            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_List_Rpt_Observados", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarServicios_xVoucher(ByVal vintIDVoucher As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@IDVoucher", vintIDVoucher)

            End With
            Return objADO.GetDataTable(True, "OPERACIONES_DET_Sel_Servicios_Vouchers", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function ConsultarCabecera_xVoucher(ByVal vstrIDVoucher_OrdServ As String, _
                                               ByVal vstrIDProvSetoursOS As String, _
                                               Optional ByVal vblnDesdeOrdenServicio As Boolean = False, _
                                               Optional ByVal vblnDesdeOrdenPago As Boolean = False, _
                                               Optional ByVal vstrCoProveedor As String = "", _
                                               Optional ByVal vintIDCab As Integer = 0, _
                                               Optional ByVal vblnDesdeOrdenCompra As Boolean = False, _
                                               Optional ByVal vblnVoucherOficExt As Boolean = False, _
                                               Optional ByVal vblnPresupuesto As Boolean = False, _
                                               Optional ByVal vstrCoMoneda As String = "", _
                                               Optional ByVal vblnFormaEgreso As Boolean = False) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            If vblnDesdeOrdenCompra Then
                dc.Add("@NuOrdComInt", vstrIDVoucher_OrdServ)
                Return objADO.GetDataReader("ORDENCOMPRA_Sel_Pk", dc)
            Else
                dc.Add("@NuVoucher", vstrIDVoucher_OrdServ)
                If Not vblnDesdeOrdenServicio Then
                    'dc.Add("@IDProveedor", vstrIDProveedor)
                    If Not vblnDesdeOrdenPago Then
                        If Not vblnVoucherOficExt Then
                            If Not vblnPresupuesto Then
                                If Not vblnFormaEgreso Then
                                    dc.Add("@CoProveedor", vstrCoProveedor)
                                    dc.Add("@IDCab", vintIDCab)
                                    dc.Add("@CoMoneda", vstrCoMoneda)
                                    Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Cabecera_Voucher", dc)
                                Else
                                    dc.Add("@IDCab", vintIDCab)
                                    Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Cabecera_FormaEgreso", dc)
                                End If
                            Else
                                Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Cabecera_Presupuesto", dc)
                            End If

                            Else
                                Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Cabecera_VoucherOficExt", dc)
                            End If
                        Else
                            Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Cabecera_OrdenPago", dc)
                        End If

                    Else
                        dc.Add("@CoMoneda", vstrCoMoneda)
                        dc.Add("@IDProveedorSetours", vstrIDProvSetoursOS)
                        Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Cabecera_OrdenServicio", dc)
                    End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCabecera_xVoucher_FondoFijo(ByVal vintNuFondoFijo As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@NuFondoFijo", vintNuFondoFijo)
            Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Cabecera_FondoFijo", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDatosProveedor(ByVal vstrIDProveedor As String) 'As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@IDProveedor", vstrIDProveedor)

            End With
            Return objADO.GetDataReader("MAPROVEEDORES_Sel_DocumentoProveedor_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function lstInsertarDocumentosGenerados(ByVal BE As clsDocumentoProveedorBE, _
                                ByVal vstrUserMod As String) As List(Of String)


        'Dim objBT As New clsDocumentoBT With {.pstrSantiago = pstrSantiago, .pstrBuenosAires = pstrBuenosAires, _
        '                                          .pstrCoProveedorSetLima = pstrCoProveedorSetLima, _
        '                                          .pstrCoUbigeo_Oficina = pstrCoUbigeo_Oficina, _
        '                                          .pstrCoProveedorSetBuenosAires = pstrCoProveedorSetBuenosAires, _
        '                                          .pstrCoProveedorSetSantiago = pstrCoProveedorSetSantiago}

        Dim objBT As New clsDocumentoProveedorBT

        Dim blnError As Boolean = False
        Dim ListaErrores As New List(Of String)

        For Each Item As clsDocumentoProveedorBE In BE.ListaDocumentos
            Item.ListaDocumentos = BE.ListaDocumentos
            Dim strError As String = objBT.strInsertarDocumentoGenerado(Item, vstrUserMod)

            If strError <> "" Then
                If strError.Substring(0, 4) = "SAP:" Then
                    Item.ErrorSAP = strError
                Else
                    Item.ErrorSetra = strError
                End If
                blnError = True
                ListaErrores.Add(Item.NuDocum & " " & strError)

            End If
        Next

        Return ListaErrores

    End Function

    Public Function lstGrabarxStockPresupuesto(ByVal BEPre As clsPresupuesto_SobreBE, ByVal BESol As clsPresupuesto_SobreBE, ByVal BEDol As clsPresupuesto_SobreBE) As List(Of String)

        Dim objBT As New clsDocumentoProveedorBT
        Dim objServ As New clsServicioProveedorBN
        Dim objStk As New clsStockTicketEntradasIngresosBN
        'Dim objDetPre As New clsPresupuesto_Sobre_DetBT

        Dim ListMuseos As New List(Of String)
        For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In BEPre.ListaPresupuestoSobreDet
            If BEPre.CoEstado = "EN" And ItemDetPre.CoPago = "STK" Then ' And ItemDetPre.SsTotSus > 0 Then
                If Not ListMuseos.Contains(ItemDetPre.IDServicio) Then
                    ListMuseos.Add(ItemDetPre.IDServicio)
                End If
            End If
        Next

        Dim ListServStk As New List(Of String)
        Dim ListUltCompStk As New List(Of String)
        For Each strIDServicio As String In ListMuseos
            Dim intNuTckEntIngUltComp As Int16 = 0
            Using drQt As SqlClient.SqlDataReader = objStk.ConsultarQtUtilizada(BEPre.IDCab, strIDServicio)
                While drQt.Read
                    ListServStk.Add(strIDServicio & "|" & drQt("NuTckEntIng") & "|" & drQt("QtUtilizada") & "|" & _
                                    drQt("IDTipoDocSus") & "|" & drQt("NroDocSus") & "|" & drQt("NuOperBan"))
                    intNuTckEntIngUltComp = drQt("NuTckEntIng")
                End While
                drQt.Close()
            End Using
            ListUltCompStk.Add(strIDServicio & "|" & intNuTckEntIngUltComp.ToString)
        Next

        Dim blnError As Boolean = False
        Dim ListaErrores As New List(Of String)
        Dim datFecHoy As Date = Now.ToString("dd/MM/yyyy")
        'Dim datFecHoy As Date = DateSerial(2015, 12, 26)

        For Each strIDServicioStk As String In ListServStk
            Dim ArrIDServicioStk() As String = strIDServicioStk.Split("|")
            Dim strIDServicio As String = ArrIDServicioStk(0).Trim ' Left(strIDServicioStk, InStr(strIDServicioStk, "-") - 1)
            Dim intNuTckEntIng As Int16 = ArrIDServicioStk(1).Trim
            Dim intQt As Int16 = ArrIDServicioStk(2).Trim ' Mid(strIDServicioStk, InStr(strIDServicioStk, "-") + 1)

            Dim strIDTipoDocSus As String = ArrIDServicioStk(3).Trim
            Dim strNroDocSus As String = ArrIDServicioStk(4).Trim
            Dim strNuOperBan As String = ArrIDServicioStk(5).Trim

            Dim dblTotal As Double = 0
            Dim intIDServicio_Det_V_Cot As Integer = 0
            Dim strDescServicio As String = ""

            Dim blnEsUltCompra As Boolean = False
            For Each strUltCompra As String In ListUltCompStk
                Dim ArrUltComp() As String = strUltCompra.Split("|")
                If strIDServicio = ArrUltComp(0).Trim And intNuTckEntIng.ToString = ArrUltComp(1).Trim Then
                    blnEsUltCompra = True
                    Exit For
                End If
            Next

            For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In BEPre.ListaPresupuestoSobreDet
                If BEPre.CoEstado = "EN" And ItemDetPre.CoPago = "STK" And ItemDetPre.IDServicio = strIDServicio Then
                    'dblTotal += (ItemDetPre.SsTotSus * ItemDetPre.SsPreUni)
                    If blnEsUltCompra Then
                        intQt -= ItemDetPre.SsTotDev
                    End If
                    dblTotal += (intQt * ItemDetPre.SsPreUni)
                    intIDServicio_Det_V_Cot = ItemDetPre.IDServicio_Det_V_Cot
                    strDescServicio = ItemDetPre.TxServicio
                End If
            Next





            Dim objBE As New clsDocumentoProveedorBE
            With objBE
                .NuDocInterno = strDevuelveNuDocInterno_Correlativo(datFecHoy)
                .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                .NuOrden_Servicio = 0
                .CoOrdPag = 0
                .NuOrdComInt = 0
                .NuPreSob = BEPre.NuPreSob
                .NuVouPTrenInt = 0

                .IDCab = BEPre.IDCab
                .IDCab_FF = BEPre.IDCab

                .NuSerie = ""
                .NuDocum = ""

                'Dim strNroDocNroOper As String = objStk.strNroOperacionDocumentoCompraStock(strIDServicio)
                Dim strNroDocNroOper As String = objStk.strNroOperacionDocumentoCompraStock(strNroDocSus, strIDTipoDocSus, strNuOperBan)

                If InStr(strNroDocNroOper, "|") > 0 Then
                    .CoTipDocSusEnt = strNroDocNroOper.Substring(0, InStr(strNroDocNroOper, "|") - 1)
                    .NuDocumSusEnt = Mid(strNroDocNroOper, InStr(strNroDocNroOper, "|") + 1)
                    .NuOperBanSusEnt = ""
                Else
                    .CoTipDocSusEnt = ""
                    .NuDocumSusEnt = ""
                    .NuOperBanSusEnt = strNroDocNroOper
                End If

                .CoTipoDoc = "OTR"
                .FeEmision = datFecHoy
                .FeRecepcion = datFecHoy

                .CoTipoDetraccion = "00000"
                .CoMoneda = "SOL"
                .CoMoneda_Pago = ""
                .SsIGV = 0
                .SsNeto = dblTotal
                .SsOtrCargos = 0
                .SsDetraccion = 0
                .SSTotalOriginal = dblTotal
                .SSTipoCambio = 0
                .SSTotal = dblTotal

                .CoCecos = "900101"
                .CoTipoOC = "004"
                .CoCtaContab = "65931003" '"65931098"
                .CoObligacionPago = "PST"
                .IngresoManual = False
                .CoMoneda_FEgreso = ""

                .SsTipoCambio_FEgreso = 0
                .SsTotal_FEgreso = BEPre.SsSaldo

                .IDOperacion = 0
                .CoProveedor = objServ.strConsultarProveedor(intIDServicio_Det_V_Cot)

                .SSPercepcion = 0
                '.TxConcepto = "SUSTENTO COMPRA STOCK ENTRADAS: " & strDescServicio
                .TxConcepto = strDescServicio


                .FeVencimiento = "01/01/1900"
                .CoTipoDocSAP = 2
                .CoFormaPago = "012" 'al contado


                .CoTipoDoc_Ref = ""
                .NuSerie_Ref = ""
                .NuDocum_Ref = ""
                .FeEmision_Ref = "01/01/1900"


                .CoCeCon = "000000"
                .CoGasto = ""
                '.CoUbigeo_Oficina = ""
                .UserMod = BEPre.UserMod
                .UserNuevo = BEPre.UserMod
            End With


            Dim LstDetalle As New List(Of clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)
            Dim bytNuDocumProvDet As Byte = 1
            Dim ListaDetPreOk As New List(Of clsPresupuesto_Sobre_DetBE)
            For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In BEPre.ListaPresupuestoSobreDet

                If BEPre.CoEstado = "EN" And ItemDetPre.CoPago = "STK" And ItemDetPre.IDServicio = strIDServicio Then
                    Dim objBEIti As New clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
                    With objBEIti
                        .NuDocumProv = 0 'bytGenerarCoDocumento()
                        .NuDocumProvDet = bytNuDocumProvDet
                        .IDServicio_Det = ItemDetPre.IDServicio_Det_V_Cot
                        .TxServicio = ItemDetPre.TxServicio
                        '.QtCantidad = ItemDetPre.SsTotSus
                        '.SsMonto = ItemDetPre.SsTotSus * ItemDetPre.SsPreUni
                        .QtCantidad = intQt
                        .SsMonto = .QtCantidad * ItemDetPre.SsPreUni
                        .Accion = "N"
                        .UserMod = BEPre.UserMod
                        .IDCab = 0
                        .CoProducto = ""
                        .CoAlmacen = ""
                        .SsIGV = 0
                        .SSPrecUni = ItemDetPre.SsPreUni
                        .SsTotal = ItemDetPre.SsTotSus * ItemDetPre.SsPreUni
                    End With


                    LstDetalle.Add(objBEIti)
                    ListaDetPreOk.Add(ItemDetPre)
                    bytNuDocumProvDet += 1
                End If
            Next
            objBE.ListaDetalle = LstDetalle
            'objBE.CoObligacionPago = "PST"

            Dim strError As String = objBT.Grabar(objBE, True, ListaDetPreOk)

            If strError <> "" Then

                blnError = True
                ListaErrores.Add(objBE.TxConcepto & " " & strError)
            Else

                'For Each ItemDetPre As clsPresupuesto_Sobre_DetBE In ListaDetPreOk
                '    objDetPre.Actualizar(ItemDetPre)
                'Next
                'Dim objBEPre As New clsPresupuesto_SobreBE With {.ListaPresupuestoSobreDet = ListaDetPreOk, .CoEstado = "EN"}
                'objDetPre.ActualizarStockEntradas(objBEPre)

                ListaErrores.Add(objBE.NuDocumProv & " " & " se generó exitosamente.")
            End If

        Next

        Dim objPre As New clsPresupuesto_SobreBT
        If Not blnError Then

            objPre.ActualizarxDocumento(BESol, BEPre)
            'objPre.ActualizarxDocumento_USD(BEDol)
        Else
            objPre.Actualizar_SoloDetalle(BEPre)

        End If

        Return ListaErrores

    End Function

    Public Function ConsultarServiciosCompletoxIDVoucher(ByVal vstrIDVoucher_OrdServ As String, _
                                                         ByVal vintIDCab As Integer, _
                                                         ByVal vstrCoEstado As String, _
                                                         ByVal vstrIDProveedorSetours As String, _
                                                         Optional ByVal vblnOrdenServicio As Boolean = False, _
                                                         Optional ByVal vblnOrdenPago As Boolean = False, _
                                                         Optional ByVal vstrCoProveedor As String = "", _
                                                         Optional ByVal vblnPresupuesto As Boolean = False, _
                                                         Optional ByVal vstrCoMoneda As String = "", _
                                                         Optional ByVal vblnFormaEgreso As Boolean = False) As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDVoucher", vstrIDVoucher_OrdServ)
            dc.Add("@IDCab", vintIDCab)

            If Not vblnOrdenServicio Then
                If Not vblnOrdenPago Then
                    If Not vblnPresupuesto Then
                        If Not vblnFormaEgreso Then
                            If vstrCoEstado = "AN" Then
                                Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxIDVoucherHistorico", dc)
                            Else
                                dc.Add("@CoProveedor", vstrCoProveedor)
                                dc.Add("@CoMoneda", vstrCoMoneda)
                                'Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxIDVoucher", dc)
                                Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxIDVoucher_CoMoneda", dc)
                            End If
                        Else
                            Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxFormaEgreso", dc)
                        End If
                    Else
                        Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxNuPresupuesto", dc)
                    End If
                    Else
                        Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxCoOrdenPago", dc)
                    End If
            Else
                dc.Add("@CoMoneda", vstrCoMoneda)
                dc.Add("@IDProveedorSetours", vstrIDProveedorSetours)
                Return objADO.GetDataTable(True, "OPERACIONES_DET_SelxCoOrdenServicio", dc)
            End If


        Catch ex As Exception
            Throw
        End Try
    End Function

    '---
    Public Function Consultar_Documento_Multiple_Proveedor_OSV(ByVal vstrIDproveedor As String, ByVal vstrCoMoneda As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDproveedor", vstrIDproveedor)
            dc.Add("@CoMoneda", vstrCoMoneda)

            Return objADO.GetDataTable(True, "ORDEN_SERVICIO_Sel_List_Proveedor_DocMultiple", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveNuDocInterno_Correlativo(ByVal vdatFeEmision As Date) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@FeEmision", vdatFeEmision)
            dc.Add("@pNuDocumProv", "")
            Return objADO.ExecuteSPOutput("NuDocInterno_SelCorrelativo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarList(ByVal vintNuDocumProv As Integer, _
                                  ByVal vstrNuVoucher_OServ As String, _
                                  ByVal vstrNuDocum As String, ByVal vstrCoTipoDoc As String, _
                                  ByVal vblnFlActivo As Boolean, _
                                  ByVal vstrCoObligacionPago As String, _
                                  ByVal vstrCoProveedor As String, _
                                  ByVal vintIDOperacion As Integer, _
                                  ByVal vstrIDProveedorSetours As String, _
                                  Optional ByVal vstrCoMoneda As String = "") As DataTable

        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@NuDocumProv", vintNuDocumProv)
                .Add("@NuVoucher", vstrNuVoucher_OServ)
                .Add("@NuDocum", vstrNuDocum)
                .Add("@CoTipoDoc", vstrCoTipoDoc)
                .Add("@FlActivo", vblnFlActivo)
            End With

            If vstrCoObligacionPago = "OSV" Then
                dc.Add("@CoMoneda", vstrCoMoneda)
                dc.Add("@IDProveedorSetours", vstrIDProveedorSetours)
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuOrdenServicio", dc)
            ElseIf vstrCoObligacionPago = "VOU" Or vstrCoObligacionPago = "PRL" Then
                dc.Add("@CoProveedor", vstrCoProveedor)
                dc.Add("@IDOperacion", vintIDOperacion)
                dc.Add("@CoMoneda", vstrCoMoneda)
                'Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_List", dc)
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_List_Voucher", dc)
            ElseIf vstrCoObligacionPago = "OPA" Then
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuOrdenPago", dc)
            ElseIf vstrCoObligacionPago = "OCP" Then
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuOrdenCompra", dc)
            ElseIf vstrCoObligacionPago = "VBA" Or vstrCoObligacionPago = "VSA" Then
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuVouOfiExtInt", dc)
            ElseIf vstrCoObligacionPago = "PST" Or vstrCoObligacionPago = "PTT" Then
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuPresupuesto", dc)
            ElseIf vstrCoObligacionPago = "FFI" Then
                dc.Add("@NuFondoFijo", vstrNuVoucher_OServ)
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuFondoFijo", dc)
            ElseIf vstrCoObligacionPago = "OTR" Then
                Dim dc1 As New Dictionary(Of String, String)
                dc1.Add("@CoProveedor", vstrCoProveedor)
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxOtros", dc1)
            ElseIf vstrCoObligacionPago = "ZIC" Then
                dc.Add("@CoMoneda", vstrCoMoneda)
                Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_List_FormaEgreso", dc)
            End If

            Return Nothing
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function ConsultarPk(ByVal vintNuDocumProv As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", vintNuDocumProv)

            Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPk_FondoFijo(ByVal vintNuDocumProv As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", vintNuDocumProv)

            Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Sel_Pk_FF", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_FondoFijo_Formato(ByVal vintNuFondoFijo As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuFondoFijo", vintNuFondoFijo)

            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuFondoFijo_Formato", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_FondoFijo_Formato_XLS(ByVal vintNuFondoFijo As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuFondoFijo", vintNuFondoFijo)

            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_ListxNuFondoFijo_Formato_XLS", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_NoAnulados_xIDVoucher(ByVal vintNuVoucher As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVoucher", vintNuVoucher)

            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_List_xVoucher_NoAnulados", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPk_Validar(ByVal vintNuDocumProv As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", vintNuDocumProv)

            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_Correlativo() As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Sel_Correlativo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable(ByVal vstrCoTipoOC As String, Optional ByVal vstrProvAdmin As String = "") As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoTipoOC", vstrCoTipoOC)
            dc.Add("@FlProvAdmin", vstrProvAdmin)
            'dc.Add("@CoTipoDoc", vstrCoTipoDoc)
            Return objADO.GetDataTable(True, "CTASCONTAB_OPERCONTAB_TIPODOC_Sel_Cta", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_CentroCostos(ByVal vstrCoCeCos As String, Optional ByVal vstrProvAdmin As String = "") As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCeCos", vstrCoCeCos)
            dc.Add("@FlProvAdmin", vstrProvAdmin)
            'dc.Add("@CoTipoDoc", vstrCoTipoDoc)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_CentroCostos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_Sin_CentroCostos() As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_Sin_CentroCostos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_OrigenOtros(ByVal vblnFlFile As Boolean, ByVal vblnCoCeCos As Boolean, ByVal vblnCoCeCon As Boolean, ByVal vstrIdFile As String, ByVal vstrCoCeCos As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@FlFile", vblnFlFile)
            dc.Add("@FlCoCecos", vblnCoCeCos)
            dc.Add("@FlCoCecon", vblnCoCeCon)
            dc.Add("@IdFile", vstrIdFile)
            dc.Add("@CoCeCos", vstrCoCeCos)

            'dc.Add("@CoTipoDoc", vstrCoTipoDoc)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_DOCUMENTO_PROVEEDOR_ORIGEN_OTROS_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_CentroCostos_Servicios(ByVal vstrCoCeCos As String, Optional ByVal vstrProvAdmin As String = "") As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCeCos", vstrCoCeCos)
            dc.Add("@FlProvAdmin", vstrProvAdmin)
            'dc.Add("@CoTipoDoc", vstrCoTipoDoc)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_CentroCostos_Servicios", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_CentroCostos_Facturacion(ByVal vstrCoCeCos As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCeCos", vstrCoCeCos)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_CentroCostos_Facturacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_CentroCostos_TipoVenta_Facturacion(ByVal vstrCoCeCos As String, ByVal vstrCoTipoVenta As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCeCos", vstrCoCeCos)
            dc.Add("@CoTipoVenta", vstrCoTipoVenta)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_MACENTROCOSTOS_TIPOVENTA_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_Doc_Sel_Cbo_Pk(ByVal vstrCtaContable As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CtaContable", vstrCtaContable)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_Cbo_Doc_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_Doc_Sel_Cbo_Tip_Detail() As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_Tip_Detail_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_Documento_Cliente(ByVal vstrCoCeCos As String, ByVal vstrCoTipoVenta As String, ByVal vstrCoSerie As String, Optional ByVal vstrIDCliente As String = "") As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCeCos", vstrCoCeCos)
            dc.Add("@CoTipoVenta", vstrCoTipoVenta)
            dc.Add("@CoSerie", vstrCoSerie)
            dc.Add("@IDCliente", vstrIDCliente)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_CENTROCOSTOS_DOCUMENTO_Sel_CtaContable", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_FondosFijos() As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            'dc.Add("@CoCeCos", vstrCoCeCos)
            'dc.Add("@FlProvAdmin", vstrProvAdmin)
            ''dc.Add("@CoTipoDoc", vstrCoTipoDoc)
            Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_FondosFijos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCtaContable_MN_ME() As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            Return objADO.GetDataTable(True, "CTASCONTAB_Sel_Cta_MN_ME", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTipoOperacionCont() As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            Return objADO.GetDataTable(True, "MATIPOOPERACION_SelRecDocs", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTipoOperacionCont_CentroCostos(ByVal vstrCoCoCeCos As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCeCos", vstrCoCoCeCos)
            Return objADO.GetDataTable(True, "MATIPOOPERACION_SelRecDocs_CentroCostos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarGastosAdicionales_Cbo(ByVal vstrCoTipoDoc As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTipoDoc", vstrCoTipoDoc)


            Return objADO.GetDataTable(True, "MAGASTOSADICIONALES_MATIPODOC_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function strNuOrden_ServicioOutput(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, ByVal vstrCoProvSetours As String) As String
        Try

            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDProveedorSetours", vstrCoProvSetours)
                .Add("@pNuOrden_Servicio", "")
            End With

            Return objADO.GetSPOutputValue("ORDEN_SERVICIO_SelNuOrden_ServicioOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteDocumProvee(ByVal vstrNuDocum As String, ByVal vstrNuSerie As String, _
                                         ByVal vintIDCab As Integer, ByVal vstrCoProveedor As String) As Boolean
        Try
            Dim intNuOrden_Servicio As Integer = 0
            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@NuDocum", vstrNuDocum)
                .Add("@NuSerie", vstrNuSerie)
                .Add("@CoProveedor", vstrCoProveedor)
                .Add("@IDCab", vintIDCab)

                .Add("@pExists", 0)
            End With

            Return objADO.GetSPOutputValue("DOCUMENTO_PROVEEDOR_Sel_ExisteDocumOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function lstImportarDocumTranslivik(ByVal vListaDocs As List(Of clsDocumentoProveedorBE), ByVal vstrUserMod As String) As List(Of String)


        Dim objBT As New clsDocumentoProveedorBT
        Dim objDetr As New clsTipoDetraccionBN
        Dim ListaErrores As New List(Of String)
        Dim sglTasaDetracc02601 As Single = objDetr.sglTasaDetraccionOutput("02601")
        For Each ItemExcelDet As clsDocumentoProveedorBE In vListaDocs
            'ItemExcelDet.NuOrden_Servicio = intNuOrden_ServicioOutput(ItemExcelDet.IDCab, ItemExcelDet.CoProveedor, ItemExcelDet.CoProvSetours)
            'Dim x As String = strNuOrden_ServicioOutput(ItemExcelDet.IDCab, ItemExcelDet.CoProveedor, ItemExcelDet.CoProvSetours)
            Dim ArrNroyMonto() As String = strNuOrden_ServicioOutput(ItemExcelDet.IDCab, ItemExcelDet.CoProveedor, ItemExcelDet.CoProvSetours).Split("|")
            ItemExcelDet.NuOrden_Servicio = ArrNroyMonto(0)
            ItemExcelDet.SSTotalOrdServ = Convert.ToDouble(ArrNroyMonto(1))

            If ItemExcelDet.CoTipoDetraccion <> "00000" Then
                If ItemExcelDet.CoTipoDetraccion = "02601" Then
                    ItemExcelDet.SsDetraccion = ItemExcelDet.SSTotal_MN * sglTasaDetracc02601
                Else
                    ItemExcelDet.SsDetraccion = ItemExcelDet.SSTotal_MN * objDetr.sglTasaDetraccionOutput(ItemExcelDet.CoTipoDetraccion)
                End If

            End If

            Dim strError As String = ""
            If ItemExcelDet.NuOrden_Servicio = 0 Then
                ListaErrores.Add("No se encontró orden de servicio para el documento " & ItemExcelDet.NuDocum)
                GoTo Sgte
            End If
            If ItemExcelDet.SSTotalOrdServ <> ItemExcelDet.SSTotal Then
                strError = " Observado: Total Doc. " & ItemExcelDet.SSTotal.ToString("##,##0.00") & " / Órden Serv." & _
                    ItemExcelDet.NuOrden_Servicio.ToString & " Total Ord: " & ItemExcelDet.SSTotalOrdServ.ToString("##,##0.00") & " "
                'ListaErrores.Add(strError)
            End If

            Dim strErrorGrabar As String = objBT.strGrabar(ItemExcelDet)

            If strErrorGrabar <> "" Then
                strError = strErrorGrabar
            End If

            If strError <> "" Then
                'If strError.Substring(0, 4) = "SAP:" Then
                If InStr(strError, "SAP:") > 0 Then
                    ItemExcelDet.ErrorSAP = strError
                Else
                    ItemExcelDet.ErrorSetra = strError
                End If

                ListaErrores.Add("Documento " & ItemExcelDet.NuDocum & " " & strError)
            Else

                ListaErrores.Add("Documento " & ItemExcelDet.NuDocum & " se actualizó exitosamente.")
            End If

Sgte:
        Next


        Return ListaErrores
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoProveedor_DetBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN


    '@NuDocumProv int=0,
    '@NuDocumProvDet int=0
    Public Overloads Function ConsultarList(ByVal vintNuDocumProv As Integer, ByVal vintNuDocumProvDet As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@NuDocumProv", vintNuDocumProv)
                .Add("@NuDocumProvDet", vintNuDocumProvDet)

            End With
            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_DET_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Overloads Function ConsultarPk(ByVal vintNuDocumProv As Integer, ByVal vintNuDocumProvDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", vintNuDocumProv)
            dc.Add("@NuDocumProvDet", vintNuDocumProvDet)
            Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_DET_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk_Validar(ByVal vintNuDocumProv As Integer, ByVal vintNuDocumProvDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@NuDocumProv", vintNuDocumProv)
            dc.Add("@NuDocumProvDet", vintNuDocumProvDet)

            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_DET_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTipoDetraccionBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN


    Public Overloads Function ConsultarList(ByVal vstrCoTipoDetraccion As String, ByVal vstrNoTipoBien As String, ByVal vstrTxDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@CoTipoDetraccion", vstrCoTipoDetraccion)
                .Add("@NoTipoBien", vstrNoTipoBien)
                .Add("@TxDescripcion", vstrTxDescripcion)

            End With
            Return objADO.GetDataTable(True, "MATIPODETRACCION_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCboconCodigo(Optional ByVal vstrCoTipoDetraccion As String = "", _
                          Optional ByVal vblnTodos As Boolean = False, _
                          Optional ByVal vblnDescTodos As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTipoDetraccion", vstrCoTipoDetraccion)
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@bDescTodos", vblnDescTodos)

            Return objADO.GetDataTable(True, "MATIPODETRACCION_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function sglTasaDetraccionOutput(ByVal vstrCoTipoDetraccion As String) As Single
        Try

            Dim dc As New Dictionary(Of String, String)
            With dc
                .Add("@CoTipoDetraccion", vstrCoTipoDetraccion)
                .Add("@pSsTasa", 0)
            End With

            Return objADO.GetSPOutputValue("MATIPODETRACCION_Sel_TasaOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_Rpt_Starsoft(ByVal vdatFecha1 As Date, _
                         ByVal vdatFecha2 As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Fecha1", vdatFecha1)
            dc.Add("@Fecha2", vdatFecha2)

            Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Rpt_Exportar_Starsoft", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_Rpt_Starsoft_Dr(ByVal vdatFecha1 As String, _
                        ByVal vdatFecha2 As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Fecha1", vdatFecha1)
            dc.Add("@Fecha2", vdatFecha2)

            'Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_Rpt_Exportar_Starsoft", dc)
            Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_Rpt_Exportar_Starsoft", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    'Public Overloads Function ConsultarPk(ByVal vintNuDocumProv As Integer, ByVal vintNuDocumProvDet As Integer)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@NuDocumProv", vintNuDocumProv)
    '        dc.Add("@NuDocumProvDet", vintNuDocumProvDet)
    '        Return objADO.GetDataReader("DOCUMENTO_PROVEEDOR_DET_Sel_Pk", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    'Public Overloads Function ConsultarPk_Validar(ByVal vintNuDocumProv As Integer, ByVal vintNuDocumProvDet As Integer)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@NuDocumProv", vintNuDocumProv)
    '        dc.Add("@NuDocumProvDet", vintNuDocumProvDet)

    '        Return objADO.GetDataTable(True, "DOCUMENTO_PROVEEDOR_DET_Sel_Pk", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsMAAlmacenBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN


    Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False, _
                          Optional ByVal vblnDescTodos As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@bDescTodos", vblnDescTodos)

            Return objADO.GetDataTable(True, "MAALMACEN_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class



<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsMAGastoAdicionalBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN

    Public Overloads Function ConsultarPk(ByVal vstrCoGasto As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@CoGasto", vstrCoGasto)

        Dim strSql As String = "MAGASTOSADICIONALES_Sel_Pk"
        Return objADO.GetDataReader(strSql, dc)
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsProductosBN
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDN


    Public Overloads Function ConsultarList(ByVal vstrNoProducto As String, ByVal vintCoRubro As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc

                .Add("@NoProducto", vstrNoProducto)
                .Add("@CoRubro", vintCoRubro)

            End With
            Return objADO.GetDataTable(True, "MAPRODUCTOS_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function ConsultarListAyuda(ByVal vstrNoProducto As String, ByVal vintCoRubro As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@NoProducto", vstrNoProducto)
            dc.Add("@CoRubro", vintCoRubro)

            Return objADO.GetDataTable(True, "MAPRODUCTOS_Sel_ListAyuda", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Overloads Function ConsultarPk(ByVal vstrCoProducto As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@CoProducto", vstrCoProducto)

        Dim strSql As String = "MAPRODUCTOS_Sel_Pk"
        Return objADO.GetDataReader(strSql, dc)
    End Function
End Class