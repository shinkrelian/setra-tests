﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public MustInherit Class clsCounterBaseBT
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDT

    Public Overridable Sub Insertar()

    End Sub

    Public Overridable Sub Actualizar()

    End Sub

    Public Overridable Sub Eliminar()

    End Sub

    Public Overridable Sub InsertarActualizarEliminar()

    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsTicketBT
    Inherits clsCounterBaseBT

    Public Overloads Sub Insertar(ByVal BE As clsTicketAMADEUSBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            ' ''With dc
            ' ''    .Add("@CoTicket", BE.CoTicket)
            ' ''    .Add("@IDCab", BE.IDCab)
            ' ''    .Add("@IDCliente", BE.IDCliente)
            ' ''    .Add("@FlArea", BE.FlArea)

            ' ''    .Add("@SsTCambioCont", BE.SsTCambioCont)
            ' ''    .Add("@SsTCambioVenta", BE.SsTCambioVenta)

            ' ''    .Add("@FeEmision", BE.FeEmision)
            ' ''    .Add("@FeViaje", BE.FeViaje)
            ' ''    .Add("@FeRetorno", BE.FeRetorno)
            ' ''    .Add("@IDPax", BE.IDPax)
            ' ''    .Add("@TxPax", BE.TxPax)
            ' ''    .Add("@IDProveedor", BE.IDProveedor)
            ' ''    .Add("@IDUbigeoOri", BE.IDUbigeoOri)
            ' ''    .Add("@IDUbigeoDes", BE.IDUbigeoDes)
            ' ''    .Add("@TxRuta", BE.TxRuta)

            ' ''    .Add("@CoLugarVenta", BE.CoLugarVenta)
            ' ''    .Add("@CoCounter", BE.CoCounter)

            ' ''    .Add("@SsTarifa", BE.SsTarifa)
            ' ''    .Add("@SsPTA", BE.SsPTA)
            ' ''    .Add("@SsPenalidad", BE.SsPenalidad)
            ' ''    .Add("@SsOtros", BE.SsOtros)
            ' ''    .Add("@SsIGV", BE.SsIGV)
            ' ''    .Add("@SsImpExt", BE.SsImpExt)

            ' ''    .Add("@PoDscto", BE.PoDscto)
            ' ''    .Add("@SsDscto", BE.SsDscto)

            ' ''    .Add("@SsTotal", BE.SsTotal)

            ' ''    .Add("@CoTarjCred", BE.CoTarjCred)
            ' ''    .Add("@CoNroTarjCred", BE.CoNroTarjCred)
            ' ''    .Add("@CoNroAprobac", BE.CoNroAprobac)
            ' ''    .Add("@SsTotTarjeta", BE.SsTotTarjeta)
            ' ''    .Add("@SsTotEfectivo", BE.SsTotEfectivo)
            ' ''    .Add("@PoComision", BE.PoComision)
            ' ''    .Add("@SsComision", BE.SsComision)
            ' ''    .Add("@SsIGVComision", BE.SsIGVComision)

            ' ''    .Add("@PoTarifa", BE.PoTarifa)
            ' ''    .Add("@PoOver", BE.PoOver)
            ' ''    .Add("@SsOver", BE.SsOver)
            ' ''    .Add("@SsIGVOver", BE.SsIGVOver)

            ' ''    .Add("@FlMigrado", BE.FlMigrado)
            ' ''    .Add("@CoProvOrigen", BE.CoProvOrigen)

            ' ''    .Add("@TxReferencia", BE.TxReferencia)
            ' ''    .Add("@CoTipo", BE.CoTipo)
            ' ''    .Add("@CoTipoCambio", BE.CoTipoCambio)

            ' ''    .Add("@UserMod", BE.UserMod)
            ' ''End With
            ' ''objADO.ExecuteSP("TICKET_Ins", dc)
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@NoArchivo", BE.NoArchivo)
                .Add("@CoAir", BE.CoAir)
                .Add("@CoProvOrigen", BE.CoProvOrigen)
                .Add("@CoUsrCrea", BE.CoUsrCrea)
                .Add("@CoUsrDuenio", BE.CoUsrDuenio)
                .Add("@CoUsrEmite", BE.CoUsrEmite)
                .Add("@CoIATA", BE.CoIATA)
                .Add("@CoLineaAereaRecord", BE.CoLineaAereaRecord)
                .Add("@NoLineaAerea", BE.NoLineaAerea)
                .Add("@CoLineaAereaEmisora", BE.CoLineaAereaEmisora)
                If BE.IngresoManual Then
                    .Add("@CoLineaAereaSigla", BE.CoLineaAereaSigla)
                Else
                    .Add("@CoLineaAereaSigla", BE.CoLineaAereaRecord.Substring(0, 3).ToUpper)
                End If
                .Add("@CoTransacEmision", BE.CoTransacEmision)
                .Add("@CoLineaAereaServicio", BE.CoLineaAereaServicio)
                .Add("@TxFechaCreacion", BE.TxFechaCreacion)
                .Add("@TxFechaModificacion", BE.TxFechaModificacion)
                .Add("@TxFechaEmision", BE.TxFechaEmision)
                .Add("@TxIndicaVenta", BE.TxIndicaVenta)
                .Add("@CoPtoVentayEmision", BE.CoPtoVentayEmision)
                .Add("@TxTarifaAerea", BE.TxTarifaAerea)
                .Add("@TxTotal", BE.TxTotal)
                .Add("@NoPax", BE.NoPax)
                .Add("@TxRestricciones", BE.TxRestricciones)
                .Add("@CoFormaPago", BE.CoFormaPago)
                .Add("@CoDocumIdent", BE.CoDocumIdent)
                .Add("@NuDocumIdent", BE.NuDocumIdent)
                .Add("@TxComision", BE.TxComision)
                .Add("@TxTasaDY_AE", BE.TxTasaDY_AE)
                .Add("@TxTasaHW_DE", BE.TxTasaHW_DE)
                .Add("@TxTasaXC_DP", BE.TxTasaXC_DP)
                .Add("@TxTasaXB_TI", BE.TxTasaXB_TI)
                .Add("@TxTasaQQ_TO", BE.TxTasaQQ_TO)
                .Add("@TxTasaAH_SE", BE.TxTasaAH_SE)
                .Add("@IDCab", BE.IDCab)
                .Add("@IDCliente", BE.IDCliente)
                .Add("@CoPNR_Amadeus", BE.CoPNR_Amadeus)
                .Add("@CoTicketOrig", BE.CoTicketOrig)
                .Add("@CoEjeCouAmd", BE.CoEjeCouAmd)
                If BE.NoArchivo = "" Then
                    .Add("@CoEjeCou", BE.CoEjeCou)
                Else
                    .Add("@CoEjeCou", strUsuarioxUsrAmadeus(BE.CoEjeCouAmd))
                End If
                .Add("@CoEjeRec", BE.CoEjeRec)
                .Add("@TxObsCou", BE.TxObsCou)
                .Add("@TxTituloPax", BE.TxTituloPax)
                .Add("@CoNacionalidad", BE.CoNacionalidad)
                .Add("@CoPNR_Aerolinea", BE.CoPNR_Aerolinea)
                .Add("@UserMod", BE.UserMod)
                .Add("@NuNtaVta", BE.NuNtaVta)
            End With
            objADO.ExecuteSP("TICKET_AMADEUS_Ins", dc)

            Dim objItiner As New clsTicketItinerarioBT
            'If BE.IngresoManual Then
            objItiner.InsertarActualizarEliminar(BE)

            Dim objTax As New clsTicketAmadeus_TaxBT
            objTax.InsertarActualizarEliminar(BE)
            'Else
            'objItiner.InsertarActualizar(BE)
            'End If

            Dim objTar As New clsTicketAmadeus_TarifaBT
            objTar.InsertarActualizarEliminar(BE)

            If Not BE.IngresoManual Then
                'Docum. BSP
                Dim objDocBSP As New clsDocumentoBSPBT
                objDocBSP.InsertarActualizarEliminar(BE)

                BE.IDCab = intActualizarIDCabTicket(BE.CoTicket, BE.NuDocumIdent, BE.UserMod)
                If BE.CoTicketOrig <> "" Then
                    ActualizarEstadoTicket(BE.CoTicketOrig, "EXCH", BE.UserMod)
                End If
            End If

            InsertarVuelosDesdeTicketItinerario(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub InsertarVuelosDesdeTicketItinerario(ByVal BE As clsTicketAMADEUSBE)
        Try
            Dim strCotizacion As String = ""
            Dim BECot As New clsCotizacionBE
            'Cargar Datos a COTIVUELOS
            Dim oLstCotiVuelos As New List(Of clsCotizacionBE.clsCotizacionVuelosBE)
            Dim oLstPaxTrans As New List(Of clsCotizacionBE.clsPaxTransporteBE)
            Dim oVueloBE As clsCotizacionBE.clsCotizacionVuelosBE
            Dim intIDCab As Integer = BE.IDCab
            If intIDCab = 0 Then
                intIDCab = intActualizarIDCabTicket(BE.CoTicket, BE.NuDocumIdent, BE.UserMod)
            End If
            Dim intVuelo As Byte = 1
            If intIDCab > 0 Then
                Using objCotBL As New clsCotizacionBN
                    Using dr As SqlClient.SqlDataReader = objCotBL.ConsultarPk(intIDCab)
                        dr.Read()
                        If dr.HasRows Then
                            strCotizacion = dr("Cotizacion")
                        End If
                        dr.Close()
                    End Using
                End Using

                Dim ListaItemsNuevos = From Itinerario In BE.ListaItinerarioTicket _
                                 Where Itinerario.Accion = "N"
                Dim strIDVuelo As String = ""
                For Each STIti As clsTicketAMADEUS_ITINERARIOBE In ListaItemsNuevos
                    strIDVuelo = "M" + intVuelo.ToString
                    Dim intIDTransporteV As Integer = intDevuelveIDTransportexIDCabxNuVuelo(intIDCab, STIti.CoLineaAerea, STIti.NuVuelo)

                    If intIDTransporteV = 0 Then
                        oVueloBE = New clsCotizacionBE.clsCotizacionVuelosBE
                        With oVueloBE
                            .ID = strIDVuelo
                            .IDDet = 0
                            .IdCab = intIDCab
                            .Cotizacion = strCotizacion
                            .Servicio = ""
                            .NombrePax = "" 'BE.NoPax
                            .NumBoleto = ""
                            .RutaD = strDevuelveIDUbigeoxCod(STIti.CoCiudadLle)
                            .RutaO = strDevuelveIDUbigeoxCod(STIti.CoCiudadPar)
                            .Ruta = strDevuelveCodxIDUbigeo(.RutaO) & "/" & strDevuelveCodxIDUbigeo(.RutaD)
                            .Vuelo = STIti.CoLineaAerea & STIti.NuVuelo
                            .FecEmision = CDate(BE.TxFechaEmision).ToShortDateString
                            .FecDocumento = #1/1/1900#
                            .FecSalida = CDate(STIti.TxFechaPar).ToShortDateString
                            .FecRetorno = CDate(STIti.TxHoraLle).ToShortDateString
                            '.Salida = CDate(STIti.TxHoraPar).ToShortTimeString.Substring(0, 5)
                            .Salida = CDate(STIti.TxHoraPar).ToString("HH:mm:ss").Substring(0, 5)
                            .HrRecojo = "" 'CDate(STIti.TxHoraLle).ToShortTimeString
                            .PaxC = 0
                            .PCotizado = 0
                            .TCotizado = 0
                            .PaxV = 0
                            .PVolado = 0
                            .TVolado = 0
                            .Status = STIti.CoST
                            .Comentario = BE.TxObsCou
                            '.Llegada = CDate(STIti.TxHoraLle).ToShortTimeString.Substring(0, 5)
                            .Llegada = CDate(STIti.TxHoraLle).ToString("HH:mm:ss").Substring(0, 5)
                            .Emitido = ""
                            .TipoTransporte = "V"
                            .CodReserva = Replace(STIti.CoPNR_Aerolinea, STIti.CoLineaAerea, "").Trim
                            .Tarifa = 0 'Convert.ToDouble(BE.TxTarifaAerea) 'Convert.ToDouble(If(txtTarifa.Text.Trim = "", "0", txtTarifa.Text.Trim))
                            .PTA = 0
                            .ImpuestosExt = 0
                            .Penalidad = 0
                            .IGV = 0
                            .Otros = 0
                            .PorcComm = 0
                            .MontoContado = 0
                            .MontoTarjeta = 0
                            .IDTarjetaCredito = ""
                            .NumeroTarjeta = ""
                            .Aprobacion = ""
                            .PorcOver = 0
                            .PorcOverInCom = 0
                            .MontComm = 0
                            .MontCommOver = 0
                            .IGVComm = 0
                            .IGVCommOver = 0
                            .PorcTarifa = 0
                            .NetoPagar = 0
                            .Descuento = 0

                            .IDLineaA = strDevuelveIDProveedorxSiglaxNombre(STIti.CoLineaAerea)
                            'If BE.IngresoManual Then
                            '.IDLineaA = strDevuelveIDProveedorxIATAxNombre(If(STIti.CoLineaAereaSigla Is Nothing, "", STIti.CoLineaAereaSigla))
                            'Else
                            '.IDLineaA = strDevuelveIDProveedorxIATAxNombre(If(STIti.CoLineaAereaRecord Is Nothing, "", STIti.CoLineaAereaRecord.Substring(0, 3).ToUpper))
                            'End If

                            .MontoFEE = 0
                            .IGVFEE = 0
                            .TotalFEE = 0
                            .EmitidoSetours = True
                            .EmitidoOperador = False
                            .IDProveedorOperador = ""
                            .CostoOperador = Convert.ToDouble(BE.TxTotal)
                            .IdaVuelta = ""
                            .IDServicio_DetPeruRail = 0
                            .CoTicket = STIti.CoTicket
                            .NuSegmento = STIti.NuSegmento
                            .UserMod = STIti.UserMod
                            .Accion = "N"
                        End With
                        oLstCotiVuelos.Add(oVueloBE)
                    Else
                        strIDVuelo = intIDTransporteV
                    End If

                    Dim oPaxTrans As clsCotizacionBE.clsPaxTransporteBE = Nothing
                    If BE.IngresoManual And BE.IDPax > 0 Then
                        oPaxTrans = New clsCotizacionBE.clsPaxTransporteBE
                        With oPaxTrans
                            .IDTransporte = strIDVuelo
                            .IDPax = BE.IDPax
                            .CodReservaTransp = ""
                            .IDProveedorGuia = ""
                            .UserMod = BE.UserMod
                            .Accion = "N"
                        End With
                    Else
                        Dim intIDPax As Integer = intDevuelveIDPaxxNumIdentidadxTipoDoc(intIDCab, BE.NuDocumIdent)
                        If intIDPax > 0 Then
                            oPaxTrans = New clsCotizacionBE.clsPaxTransporteBE
                            With oPaxTrans
                                .IDTransporte = strIDVuelo
                                .IDPax = intIDPax
                                .CodReservaTransp = ""
                                .IDProveedorGuia = ""
                                .UserMod = BE.UserMod
                                .Accion = "N"
                            End With
                        End If
                    End If
                    oLstPaxTrans.Add(oPaxTrans)
                    intVuelo += 1
                Next
                BECot.ListaCotizacionVuelo = oLstCotiVuelos
                BECot.ListaPaxTransporte = oLstPaxTrans

                Dim objBTVuelo As New clsVuelosVentasCotizacionBT
                objBTVuelo.InsertarActualizarEliminar(BECot)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Function strDevuelveIDUbigeoxCod(ByVal vstrCodigoUbig As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strIDUbigeo As String = ""
            dc.Add("@CodUbig", vstrCodigoUbig)
            dc.Add("@pIDUbigeo", strIDUbigeo)

            Return objADO.GetSPOutputValue("MAUBIGEO_SelIDUbigeoxCod", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveCodxIDUbigeo(ByVal vstrIDUbigeo As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strCodigo As String = ""
            dc.Add("@IDUbigeo", vstrIDUbigeo)
            dc.Add("@pCod", strCodigo)

            Return objADO.GetSPOutputValue("MAUBIGEO_SelCodxIDUbigeo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveIDProveedorxIATAxNombre(ByVal vstrSigla As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strIDProveedor As String = ""
            dc.Add("@CoLA", vstrSigla)
            dc.Add("@pIDProveedorLA", strIDProveedor)

            Return objADO.GetSPOutputValue("MAPROVEEDORES_SelxCoxNomLineaAerea", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveIDProveedorxSiglaxNombre(ByVal vstrSigla As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim strIDProveedor As String = ""
            dc.Add("@CoLA", vstrSigla)
            dc.Add("@pIDProveedorLA", strIDProveedor)

            Return objADO.GetSPOutputValue("MAPROVEEDORES_SelxCoxNomLineaAereaSigla", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function intDevuelveIDPaxxNumIdentidadxTipoDoc(ByVal vintIDCab As Integer, ByVal vstrNumIdentidad As String) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim intIDPax As Integer = 0
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@NuDocIdentidad", vstrNumIdentidad)
            dc.Add("@pIDPax", 0)

            Return objADO.GetSPOutputValue("COTIPAX_Sel_IDPaxOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function intDevuelveIDTransportexIDCabxNuVuelo(ByVal vintIDCab As Integer, ByVal vstrCoLineaAerea As String, _
                                                          ByVal vstrNumVuelo As String) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim intIDTransporte As Integer = 0
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoLineaAerea", vstrCoLineaAerea)
            dc.Add("@NumVuelo", vstrNumVuelo)
            dc.Add("@pIDTransp", intIDTransporte)

            intIDTransporte = Convert.ToInt32(objADO.GetSPOutputValue("COTIVUELOS_SelxNroVueloIDOutput", dc))

            Return intIDTransporte
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strUsuarioxUsrAmadeus(ByVal vstrCoUsrAmd As String) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoUsrAmd", vstrCoUsrAmd)
            dc.Add("@pIDUsuario", "")

            Return objADO.GetSPOutputValue("MAUSUARIOS_SelIDUsuarioxAmadeusOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function intActualizarIDCabTicket(ByVal vstrCoTicket As String, _
                                      ByVal vstrNuDocumIdent As String, _
                                      ByVal vstrUserMod As String) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoTicket", vstrCoTicket)
            dc.Add("@NuDocumIdent", vstrNuDocumIdent)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@pIDCab", 0)

            Dim intIDCab As Integer = objADO.ExecuteSPOutput("TICKET_AMADEUS_UpdIDCabxDatosVuelo", dc)
            ContextUtil.SetComplete()

            Return intIDCab
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function
    Private Sub ActualizarEstadoTicket(ByVal vstrCoTicket As String, ByVal vstrCoEstado As String, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoTicket", vstrCoTicket)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("TICKET_AMADEUS_UpdEstado", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    'Public Overloads Sub Actualizar(ByVal BE As clsTicketBE)
    Public Overloads Sub Actualizar(ByVal BE As clsTicketAMADEUSBE)
        Try
            Dim dc As New Dictionary(Of String, String)
            ' ''With dc
            ' ''    .Add("@CoTicket", BE.CoTicket)
            ' ''    .Add("@IDCab", BE.IDCab)
            ' ''    .Add("@IDCliente", BE.IDCliente)
            ' ''    .Add("@FlArea", BE.FlArea)

            ' ''    .Add("@SsTCambioCont", BE.SsTCambioCont)
            ' ''    .Add("@SsTCambioVenta", BE.SsTCambioVenta)

            ' ''    .Add("@FeEmision", BE.FeEmision)
            ' ''    .Add("@FeViaje", BE.FeViaje)
            ' ''    .Add("@FeRetorno", BE.FeRetorno)
            ' ''    .Add("@IDPax", BE.IDPax)
            ' ''    .Add("@TxPax", BE.TxPax)
            ' ''    .Add("@IDProveedor", BE.IDProveedor)
            ' ''    .Add("@IDUbigeoOri", BE.IDUbigeoOri)
            ' ''    .Add("@IDUbigeoDes", BE.IDUbigeoDes)
            ' ''    .Add("@TxRuta", BE.TxRuta)

            ' ''    .Add("@CoLugarVenta", BE.CoLugarVenta)
            ' ''    .Add("@CoCounter", BE.CoCounter)

            ' ''    .Add("@SsTarifa", BE.SsTarifa)
            ' ''    .Add("@SsPTA", BE.SsPTA)
            ' ''    .Add("@SsPenalidad", BE.SsPenalidad)
            ' ''    .Add("@SsOtros", BE.SsOtros)
            ' ''    .Add("@SsIGV", BE.SsIGV)
            ' ''    .Add("@SsImpExt", BE.SsImpExt)

            ' ''    .Add("@PoDscto", BE.PoDscto)
            ' ''    .Add("@SsDscto", BE.SsDscto)

            ' ''    .Add("@SsTotal", BE.SsTotal)

            ' ''    .Add("@CoTarjCred", BE.CoTarjCred)
            ' ''    .Add("@CoNroTarjCred", BE.CoNroTarjCred)
            ' ''    .Add("@CoNroAprobac", BE.CoNroAprobac)
            ' ''    .Add("@SsTotTarjeta", BE.SsTotTarjeta)
            ' ''    .Add("@SsTotEfectivo", BE.SsTotEfectivo)

            ' ''    .Add("@PoComision", BE.PoComision)
            ' ''    .Add("@SsComision", BE.SsComision)
            ' ''    .Add("@SsIGVComision", BE.SsIGVComision)

            ' ''    .Add("@PoTarifa", BE.PoTarifa)
            ' ''    .Add("@PoOver", BE.PoOver)
            ' ''    .Add("@SsOver", BE.SsOver)
            ' ''    .Add("@SsIGVOver", BE.SsIGVOver)

            ' ''    .Add("@FlMigrado", BE.FlMigrado)
            ' ''    .Add("@CoProvOrigen", BE.CoProvOrigen)

            ' ''    .Add("@TxReferencia", BE.TxReferencia)
            ' ''    .Add("@CoTipo", BE.CoTipo)
            ' ''    .Add("@CoTipoCambio", BE.CoTipoCambio)

            ' ''    .Add("@UserMod", BE.UserMod)
            ' ''End With
            ' ''objADO.ExecuteSP("TICKET_Upd", dc)
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@CoAir", BE.CoAir)
                .Add("@CoProvOrigen", BE.CoProvOrigen)
                .Add("@CoUsrCrea", BE.CoUsrCrea)
                .Add("@CoUsrDuenio", BE.CoUsrDuenio)
                .Add("@CoUsrEmite", BE.CoUsrEmite)
                .Add("@CoIATA", BE.CoIATA)
                .Add("@CoLineaAereaRecord", BE.CoLineaAereaRecord)
                .Add("@NoLineaAerea", BE.NoLineaAerea)
                .Add("@CoLineaAereaEmisora", BE.CoLineaAereaEmisora)
                .Add("@CoTransacEmision", BE.CoTransacEmision)
                .Add("@CoLineaAereaServicio", BE.CoLineaAereaServicio)
                If BE.IngresoManual Then
                    .Add("@CoLineaAereaSigla", BE.CoLineaAereaSigla)
                Else
                    .Add("@CoLineaAereaSigla", BE.CoLineaAereaRecord.Substring(0, 3))
                End If
                .Add("@TxFechaCreacion", BE.TxFechaCreacion)
                .Add("@TxFechaModificacion", BE.TxFechaModificacion)
                .Add("@TxFechaEmision", BE.TxFechaEmision)
                .Add("@TxIndicaVenta", BE.TxIndicaVenta)
                .Add("@CoPtoVentayEmision", BE.CoPtoVentayEmision)
                .Add("@TxTarifaAerea", BE.TxTarifaAerea)
                .Add("@TxTotal", BE.TxTotal)
                .Add("@NoPax", BE.NoPax)
                .Add("@TxRestricciones", BE.TxRestricciones)
                .Add("@CoFormaPago", BE.CoFormaPago)
                .Add("@CoDocumIdent", BE.CoDocumIdent)
                .Add("@NuDocumIdent", BE.NuDocumIdent)
                .Add("@TxComision", BE.TxComision)
                .Add("@TxTasaDY_AE", BE.TxTasaDY_AE)
                .Add("@TxTasaHW_DE", BE.TxTasaHW_DE)
                .Add("@TxTasaXC_DP", BE.TxTasaXC_DP)
                .Add("@TxTasaXB_TI", BE.TxTasaXB_TI)
                .Add("@TxTasaQQ_TO", BE.TxTasaQQ_TO)
                .Add("@TxTasaAH_SE", BE.TxTasaAH_SE)
                .Add("@IDCab", BE.IDCab)
                .Add("@IDCliente", BE.IDCliente)
                .Add("@CoPNR_Amadeus", BE.CoPNR_Amadeus)
                .Add("@CoEstado", BE.CoEstado)
                .Add("@TxObsCou", BE.TxObsCou)
                .Add("@CoNacionalidad", BE.CoNacionalidad)
                .Add("@CoEjeRec", BE.CoEjeRec)
                .Add("@CoEjeCou", BE.CoEjeCou)
                .Add("@CoEjeCouAmd", BE.CoEjeCouAmd)
                .Add("@TxTituloPax", BE.TxTituloPax)
                .Add("@CoPNR_Aerolinea", BE.CoPNR_Aerolinea)
                .Add("@UserMod", BE.UserMod)
                .Add("@NuNtaVta", BE.NuNtaVta)
            End With
            objADO.ExecuteSP("TICKET_AMADEUS_Upd", dc)

            If BE.IngresoManual Then
                Dim objItiner As New clsTicketItinerarioBT
                objItiner.InsertarActualizarEliminar(BE)

                Dim objTax As New clsTicketAmadeus_TaxBT
                objTax.InsertarActualizarEliminar(BE)

                Dim objTaf As New clsTicketAmadeus_TarifaBT
                objTaf.InsertarActualizarEliminar(BE)

                Dim objDoc As New clsDocumentoBSPBT
                objDoc.InsertarActualizarEliminar(BE)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vstrCoTicket As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", vstrCoTicket)
            End With

            Dim objBTTar As New clsTicketAmadeus_TarifaBT
            objBTTar.EliminarxCoTicket(vstrCoTicket)

            Dim objBTIti As New clsTicketItinerarioBT
            objBTIti.EliminarxCoTicket(vstrCoTicket)

            Dim objBTTax As New clsTicketAmadeus_TaxBT
            objBTTax.EliminarxCoTicket(vstrCoTicket)

            Dim objBTDoc As New clsDocumentoBSPBT
            objBTDoc.EliminarxCoTicket(vstrCoTicket)

            objADO.ExecuteSP("TICKET_AMADEUS_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function blnExiste(ByVal vstrCoTicket As String) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@CoTicket", vstrCoTicket)
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("TICKET_Sel_ExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub InsertarActualizar(ByVal BE As clsTicketBE)
        Try
            ' ''For Each Item As clsTicketBE In BE.ListaTickets
            ' ''    If Not blnExiste(Item.CoTicket) Then
            ' ''        Insertar(Item)
            ' ''    Else
            ' ''        Actualizar(Item)
            ' ''    End If
            ' ''Next

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsTicketAmadeus_TaxBT
    Inherits clsCounterBaseBT

    Private Overloads Sub Insertar(ByVal BE As clsTicketAmadeus_TaxBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@NuTax", BE.NuTax)
                .Add("@CoTax", BE.CoTax)
                .Add("@SsMonto", BE.SsMonto)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("TICKET_AMADEUS_TAX_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsTicketAmadeus_TaxBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@NuTax", BE.NuTax)
                .Add("@CoTax", BE.CoTax)
                .Add("@SsMonto", BE.SsMonto)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("TICKET_AMADEUS_TAX_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vstrCoTicket As String, ByVal vbytNuTax As Byte)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)
            dc.Add("@NuTax", vbytNuTax)

            objADO.ExecuteSP("TICKET_AMADEUS_TAX_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarxCoTicket(ByVal vstrCoTicket As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)

            objADO.ExecuteSP("TICKET_AMADEUS_TAX_DelxCoTicket", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsTicketAMADEUSBE)
        Try
            For Each Item As clsTicketAmadeus_TaxBE In BE.ListaTicketAmadeus_Tax
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.CoTicket, Item.NuTax)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsTicketItinerarioBT
    Inherits clsCounterBaseBT
    Dim strNombreClase As String = "clsTicketItinerarioBT"
    Public Overloads Sub Insertar(ByVal BE As clsTicketAMADEUS_ITINERARIOBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@NuSegmento", BE.NuSegmento)
                .Add("@CoCiudadPar", BE.CoCiudadPar)
                .Add("@CoLineaAerea", BE.CoLineaAerea)
                .Add("@NuVuelo", BE.NuVuelo)
                .Add("@CoSegmento", BE.CoSegmento)
                .Add("@CoBooking", BE.CoBooking)
                .Add("@TxFechaPar", Format(Convert.ToDateTime(BE.TxFechaPar), "dd/MM/yyyy HH:mm:ss"))
                '.Add("@TxHoraPar", BE.TxHoraPar)
                .Add("@TxBaseTarifa", BE.TxBaseTarifa)
                .Add("@TxBag", BE.TxBag)
                .Add("@CoST", BE.CoST)
                .Add("@CoCiudadLle", BE.CoCiudadLle)
                .Add("@TxHoraLle", Format(Convert.ToDateTime(BE.TxHoraLle), "dd/MM/yyyy HH:mm:ss"))
                .Add("@CoPNR_Amadeus", BE.CoPNR_Amadeus)
                .Add("@CoPNR_Aerolinea", BE.CoPNR_Aerolinea)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("TICKET_AMADEUS_ITINERARIO_Ins", dc)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsTicketAMADEUS_ITINERARIOBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@NuSegmento", BE.NuSegmento)
                .Add("@CoCiudadPar", BE.CoCiudadPar)
                .Add("@CoLineaAerea", BE.CoLineaAerea)
                .Add("@NuVuelo", BE.NuVuelo)
                .Add("@CoSegmento", BE.CoSegmento)
                .Add("@CoBooking", BE.CoBooking)
                .Add("@TxFechaPar", Format(Convert.ToDateTime(BE.TxFechaPar), "dd/MM/yyyy HH:mm:ss"))
                '.Add("@TxHoraPar", BE.TxHoraPar)
                .Add("@TxBaseTarifa", BE.TxBaseTarifa)
                .Add("@TxBag", BE.TxBag)
                .Add("@CoST", BE.CoST)
                .Add("@CoCiudadLle", BE.CoCiudadLle)
                .Add("@TxHoraLle", Format(Convert.ToDateTime(BE.TxHoraLle), "dd/MM/yyyy HH:mm:ss"))
                .Add("@CoPNR_Amadeus", BE.CoPNR_Amadeus)
                .Add("@CoPNR_Aerolinea", BE.CoPNR_Aerolinea)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("TICKET_AMADEUS_ITINERARIO_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub ActualizarTicketingxVuelo(ByVal BE As clsTicketAMADEUSBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@NoLineaAerea", BE.NoLineaAerea)
                .Add("@CoLineaAereaSigla", BE.CoLineaAereaSigla)
                .Add("@CoPNR_AerorLn", BE.CoPNR_Aerolinea)
                .Add("@TxTotal", BE.TxTotal)
                .Add("@UserMod", BE.UserMod)
            End With
            objADO.ExecuteSP("TICKET_AMADEUS_UpdxCOTIVUELO_Upd", dc)

            ActualizarTicketItinerarioxVuelo(BE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub ActualizarTicketItinerarioxVuelo(ByVal BE As clsTicketAMADEUSBE)
        Dim dc As Dictionary(Of String, String)
        Try
            For Each Item As clsTicketAMADEUS_ITINERARIOBE In BE.ListaItinerarioTicket
                dc = New Dictionary(Of String, String)
                With dc
                    .Add("@CoTicket", Item.CoTicket)
                    .Add("@NuSegmento", Item.NuSegmento)
                    .Add("@CoLineaAerea", Item.CoLineaAerea)
                    .Add("@NuVuelo", Item.NuVuelo)
                    .Add("@CodReserva", Item.CoPNR_Aerolinea)
                    .Add("@TxFechaPar", CDate(Item.TxFechaPar).ToString("dd/MM/yyyy hh:mm:ss"))
                    .Add("@TxHoraLle", CDate(Item.TxHoraLle).ToString("dd/MM/yyyy hh:mm:ss"))
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("TICKET_AMADEUS_ITINERARIO_UpdxCOTIVUELO_Upd", dc)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Overloads Sub ActualizarVueloSxTickets(ByVal vObjBE As clsTicketAMADEUSBE, ByVal Item As clsTicketAMADEUS_ITINERARIOBE)
        Try
            If Item.IDTransp > 0 Then
                Dim oBETicket As New clsTicketBT
                Dim oVueloBE As New clsCotizacionBE.clsCotizacionVuelosBE
                With oVueloBE
                    .ID = Item.IDTransp
                    .RutaD = oBETicket.strDevuelveIDUbigeoxCod(Item.CoCiudadLle)
                    .RutaO = oBETicket.strDevuelveIDUbigeoxCod(Item.CoCiudadPar)
                    .Ruta = oBETicket.strDevuelveCodxIDUbigeo(.RutaO) & "/" & oBETicket.strDevuelveCodxIDUbigeo(.RutaD)
                    .FecEmision = CDate(vObjBE.TxFechaEmision).ToShortDateString
                    .IDLineaA = oBETicket.strDevuelveIDProveedorxSiglaxNombre(Item.CoLineaAerea)
                    .Vuelo = Item.CoLineaAerea & Item.NuVuelo
                    .NombrePax = vObjBE.NoPax
                    .Tarifa = Convert.ToDouble(vObjBE.TxTarifaAerea)
                    .FecSalida = CDate(Item.TxFechaPar).ToShortDateString
                    .Salida = CDate(Item.TxFechaPar).ToShortTimeString.Substring(0, 5)
                    .Llegada = CDate(Item.TxHoraLle)
                    .CodReserva = Replace(Item.CoPNR_Aerolinea, Item.CoLineaAerea, "")
                End With

                Dim oBTTrans As New clsVuelosVentasCotizacionBT
                oBTTrans.ActualizarVueloxTickets(oVueloBE)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vstrCoTicket As String, ByVal vbytNuSegmento As Byte)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", vstrCoTicket)
                .Add("@NuSegmento", vbytNuSegmento)
            End With
            objADO.ExecuteSP("TICKET_AMADEUS_ITINERARIO_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarxCoTicket(ByVal vstrCoTicket As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)
            objADO.ExecuteSP("COTITRANSP_PAX_DelxCoTicket", dc)
            objADO.ExecuteSP("COTIVUELOS_DelxCoTicket", dc)
            objADO.ExecuteSP("TICKET_AMADEUS_ITINERARIO_DelxCoTicket", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function datFechaconAnio(ByVal vstrFecha As String, ByVal vstrHora As String, ByVal vdatFechaAnio As Date) As Date
        Try
            'Dim bytDia As Byte = vstrFecha.Substring(0, 2)
            Dim strMes As String = vstrFecha.Substring(2, 3)

            Dim bytMes As Byte = 1

            Select Case strMes
                Case "JAR" : bytMes = 1
                Case "FEB" : bytMes = 2
                Case "MAR" : bytMes = 3
                Case "APR" : bytMes = 4
                Case "MAY" : bytMes = 5
                Case "JUN" : bytMes = 6
                Case "JUL" : bytMes = 7
                Case "AGO" : bytMes = 8
                Case "SET" : bytMes = 9
                Case "OCT" : bytMes = 10
                Case "NOV" : bytMes = 11
                Case "DIC" : bytMes = 12
            End Select


            Return DateSerial(Year(vdatFechaAnio), bytMes, vstrFecha.Substring(0, 2)) & _
                " " & vstrHora.Substring(0, 2) & ":" & vstrHora.Substring(2, 2)



        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function datHora(ByVal vstrHora As String) As Date
        Try
            If vstrHora.Trim = "" Then Return #1/1/1900#

            Return vstrHora.Substring(0, 2) & ":" & vstrHora.Substring(2, 2)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Sub InsertarActualizar(ByVal BE As clsTicketAMADEUSBE)
        If BE.ListaItinerarioTicket Is Nothing Then Exit Sub
        Try
            For Each Item As clsTicketAMADEUS_ITINERARIOBE In BE.ListaItinerarioTicket
                ' ''    If Not blnExiste(Item.CoTicket) Then
                ' ''        Insertar(Item)
                ' ''    Else
                ' ''        Actualizar(Item)
                ' ''    End If
                Item.CoTicket = BE.CoTicket
                Item.TxFechaPar = datFechaconAnio(Item.TxFechaPar, Item.TxHoraPar, BE.TxFechaEmision)
                Item.TxHoraLle = datFechaconAnio(Item.TxFechaPar, Item.TxHoraLle, BE.TxFechaEmision)

                If Item.Accion = "N" Then
                    Insertar(Item)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsTicketAMADEUSBE)
        If BE.ListaItinerarioTicket Is Nothing Then Exit Sub
        Try
            For Each Item As clsTicketAMADEUS_ITINERARIOBE In BE.ListaItinerarioTicket
                If Item.Accion = "N" Then
                    If Not BE.IngresoManual Then
                        Item.CoPNR_Amadeus = BE.CoPNR_Amadeus
                        Item.CoPNR_Aerolinea = BE.CoPNR_Aerolinea
                    End If
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                    ActualizarVueloSxTickets(BE, Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.CoTicket, Item.NuSegmento)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDocumentoBSPBT
    Inherits clsCounterBaseBT

    Private Overloads Sub Insertar(ByVal BE As clsDocumentoBSPBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocumBSP", BE.NuDocumBSP)
                .Add("@CoTipDocBSP", BE.CoTipDocBSP)
                .Add("@CoTicket", BE.CoTicket)
                .Add("@FeEmision", BE.FeEmision)
                .Add("@CoPeriodo", BE.CoPeriodo)
                .Add("@SSTotalDocum", BE.SSTotalDocum)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("DOCUMENTOBSP_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsDocumentoBSPBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NuDocumBSP", BE.NuDocumBSP)
                .Add("@CoTipDocBSP", BE.CoTipDocBSP)
                .Add("@FeEmision", BE.FeEmision)
                .Add("@CoPeriodo", BE.CoPeriodo)
                .Add("@SSTotalDocum", BE.SSTotalDocum)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("DOCUMENTOBSP_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vstrNuDocumBSP As String, ByVal vstrCoTipDocBSP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuDocumBSP", vstrNuDocumBSP)
            dc.Add("@CoTipDocBSP", vstrCoTipDocBSP)

            objADO.ExecuteSP("DOCUMENTOBSP_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarxCoTicket(ByVal vstrCoTicket As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)

            objADO.ExecuteSP("DOCUMENTOSBP_DelxCoTicket", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsTicketAMADEUSBE)
        If BE.ListaDocumentoBSP Is Nothing Then Exit Sub
        Try
            For Each Item As clsDocumentoBSPBE In BE.ListaDocumentoBSP
                If Item.Accion = "N" Then
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.NuDocumBSP, Item.CoTipDocBSP)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsTicketAmadeus_TarifaBT
    Inherits clsCounterBaseBT

    Private Overloads Sub Insertar(ByVal BE As clsTicketAMADEUS_TARIFABE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@CoCiudadPar", BE.CoCiudadPar)
                .Add("@CoCiudadLle", BE.CoCiudadLle)
                .Add("@SsTarifa", BE.SsTarifa)
                .Add("@SsQ", BE.SsQ)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("TICKET_AMADEUS_TARIFA_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsTicketAMADEUS_TARIFABE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoTicket", BE.CoTicket)
                .Add("@NuTarifa", BE.NuTarifa)
                .Add("@CoCiudadPar", BE.CoCiudadPar)
                .Add("@CoCiudadLle", BE.CoCiudadLle)
                .Add("@SsTarifa", BE.SsTarifa)
                .Add("@SsQ", BE.SsQ)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("TICKET_AMADEUS_TARIFA_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vstrCoTicket As String, ByVal vbytNuTarifa As Byte)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)
            dc.Add("@NuTarifa", vbytNuTarifa)

            objADO.ExecuteSP("TICKET_AMADEUS_TARIFA_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarxCoTicket(ByVal vstrCoTicket As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)

            objADO.ExecuteSP("TICKET_AMADEUS_TARIFA_DelxCoTicket", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    'Public Overloads Sub InsertarxCoTicket(ByVal vstrCoTicket As String, ByVal vstrUserMod As String)
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@CoTicket", vstrCoTicket)
    '        dc.Add("@UserMod", vstrUserMod)

    '        objADO.ExecuteSP("TICKET_AMADEUS_TARIFA_InsxCoTicket", dc)
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub
    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsTicketAMADEUSBE)
        Try
            Dim strCoCiudadLleAnt As String = ""
            For Each Item As clsTicketAMADEUS_TARIFABE In BE.ListaTicketTarifas
                If Item.Accion = "N" Then
                    If Not BE.IngresoManual Then
                        If Item.CoCiudadPar = "" Then
                            Item.CoCiudadPar = strCoCiudadLleAnt
                        End If
                        strCoCiudadLleAnt = Item.CoCiudadLle
                    End If
                    Insertar(Item)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    Eliminar(Item.CoTicket, Item.NuTarifa)
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsMAAeropuertoBT
    Inherits clsCounterBaseBT

    'Public Overloads Sub Insertar(ByVal BE As clsTicketBE)
    'Public Overloads Sub Insertar(ByVal BE As clsAeropuertoBE)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        ''''''''''''''''''''''''''''''''''''''
    '        With dc

    '            .Add("@CoAero", BE.CoAero)
    '            .Add("@NoDescripcion", BE.NoDescripcion)
    '            .Add("@CoUbigeo", BE.CoUbigeo)
    '            .Add("@pCoAero", "")
    '            .Add("@UserMod", BE.UserMod)

    '        End With
    '        objADO.ExecuteSP("MAAEROPUERTO_INS", dc)

    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub


    ''Public Overloads Sub Actualizar(ByVal BE As clsTicketBE)
    'Public Overloads Sub Actualizar(ByVal BE As clsAeropuertoBE)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)

    '        '   @CoAero char(3),
    '        '@NoDescripcion varchar(200),
    '        '@CoUbigeo char(6),
    '        '@UserMod char(4)=''

    '        With dc
    '            .Add("@CoAero", BE.CoAero)
    '            .Add("@NoDescripcion", BE.NoDescripcion)
    '            .Add("@CoUbigeo", BE.CoUbigeo)
    '            .Add("@UserMod", BE.UserMod)

    '        End With
    '        objADO.ExecuteSP("MAAEROPUERTO_UPD", dc)

    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub

    'Public Sub Anular(ByVal BE As clsAeropuertoBE)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)

    '        dc.Add("@CoAero", BE.CoAero)
    '        dc.Add("@FlActivo", BE.FlActivo)

    '        dc.Add("@UserMod", BE.UserMod)
    '        objADO.ExecuteSP("MAAEROPUERTO_ACTIVO_UPD", dc)

    '        ContextUtil.SetComplete()
    '    Catch ex As Exception

    '        ContextUtil.SetAbort()
    '        Throw 'New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
    '    End Try
    'End Sub

    Public Overloads Sub Eliminar(ByVal vstrCoAero As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoAero", vstrCoAero)
            End With


            objADO.ExecuteSP("MAAEROPUERTO_DEL", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Overloads Sub InsertarActualizar(ByVal BE As clsTicketBE)
        Try
            ' ''For Each Item As clsTicketBE In BE.ListaTickets
            ' ''    If Not blnExiste(Item.CoTicket) Then
            ' ''        Insertar(Item)
            ' ''    Else
            ' ''        Actualizar(Item)
            ' ''    End If
            ' ''Next

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class