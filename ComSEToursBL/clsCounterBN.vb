﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsCounterBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

End Class

<ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
 Public Class clsTicketBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrIDFile As String, _
                                  ByVal vstrIDTicket As String, ByVal vstrDescCliente As String, _
                                  ByVal vdatFechaViajeInicio As Date, ByVal vdatFechaViajeFin As Date, _
                                  ByVal vstrIDUsuario As String, ByVal vstrEstado As String, ByVal vstrNombrePax As String, _
                                  ByVal vstrCotizacion As String, ByVal vstrCoProvOrigen As String, _
                                  ByVal vdatFechaEmisionInicio As Date, ByVal vdatFechaEmisionFin As Date, _
                                  ByVal vstrCoLineaAerea As String, ByVal vstrDCUbigeoOrigen As String, _
                                  ByVal vstrDCUbigeoDestino As String, ByVal vstrCoEjeCou As String, _
                                  ByVal vstrNuDocumBSP As String, _
                                  Optional ByVal vstrNuNtaVta As String = "", _
                                  Optional ByVal vblnFlHistorico As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDFile", vstrIDFile)
                .Add("@CoTicket", vstrIDTicket)
                .Add("@DescCliente", vstrDescCliente)
                .Add("@TxFechaParIni", vdatFechaViajeInicio)
                .Add("@TxFechaParFin", vdatFechaViajeFin)
                .Add("@TxFechaEmisionIni", vdatFechaEmisionInicio)
                .Add("@TxFechaEmisionFin", vdatFechaEmisionFin)
                .Add("@IDUsuario", vstrIDUsuario)
                .Add("@Estado", vstrEstado)
                '.Add("@Titulo", vstrTitulo)
                .Add("@NombrePax", vstrNombrePax)
                .Add("@Cotizacion", vstrCotizacion)
                .Add("@CoProvOrigen", vstrCoProvOrigen)
                .Add("@CoLineaAerea", vstrCoLineaAerea)
                .Add("@DCUbigeoOrigen", vstrDCUbigeoOrigen)
                .Add("@DCUbigeoDestino", vstrDCUbigeoDestino)
                .Add("@CoEjeCou", vstrCoEjeCou)
                .Add("@NuDocumBSP", vstrNuDocumBSP)
                .Add("@NuNtaVta", vstrNuNtaVta)
                .Add("@FlHistorico", vblnFlHistorico)
            End With

            Return objADO.GetDataTable(True, "TICKET_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPk(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Dim strSql As String = "COTICAB_Sel_Pk"
        Return objADO.GetDataReader(strSql, dc)
    End Function

    Public Overloads Function ConsultarPK(ByVal vstrCoTicket As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@CoTicket", vstrCoTicket)

        Dim strSQL As String = "TICKET_AMADEUS_Sel_Pk"
        Return objADO.GetDataReader(strSQL, dc)
    End Function

    Public Function InicioFinCampoProcesoConsultar(ByVal vstrCoProceso As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@CoProceso", vstrCoProceso)

        Return objADO.GetDataTable(True, "INICIOFINCAMPOPROCESO_Sel_List", dc)
        'Return objADO.GetDataTable(True, "INICIOFINCAMPOPROCESO_Sel_List_Jorge", dc)

    End Function

    Public Function InicioFinCampoProcesoConsultar_2(ByVal vstrCoProceso As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@CoProceso", vstrCoProceso)

        'Return objADO.GetDataTable(True, "INICIOFINCAMPOPROCESO_Sel_List", dc)
        Return objADO.GetDataTable(True, "INICIOFINCAMPOPROCESO_Sel_List_Jorge_2", dc)

    End Function


    Public Function ConsultarCboLineas(Optional ByVal vbytTodos As Boolean = True, _
                                       Optional ByVal vstrDescripcion As String = "", _
                                       Optional ByVal vblnDescTodos As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@bTodos", vbytTodos)
            dc.Add("@CoIATA", vstrDescripcion)
            dc.Add("@bTodosDesc", vblnDescTodos)
            Return objADO.GetDataTable(True, "COTIVUELOS_Sel_CboAereoCOUNTER", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    '----------------------------
    Public Function ConsultarList_Rpt_IATA(ByVal vdatFechaEmisionInicio As String, ByVal vdatFechaEmisionFin As String, _
                                   Optional ByVal vstrCoPeriodo As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@fecha1", vdatFechaEmisionInicio)
                .Add("@fecha2", vdatFechaEmisionFin)
                .Add("@CoPeriodo", vstrCoPeriodo)
            End With
            Return objADO.GetDataTable(True, "TICKET_AMADEUS_Rpt_IATA", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
    ' Return objADO.GetDataReader(strSql, dc)
    Public Function ConsultarList_Rpt_IATA_DR(ByVal vdatFechaEmisionInicio As String, ByVal vdatFechaEmisionFin As String, _
                                 Optional ByVal vstrCoPeriodo As String = "") As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@fecha1", vdatFechaEmisionInicio)
                .Add("@fecha2", vdatFechaEmisionFin)
                .Add("@CoPeriodo", vstrCoPeriodo)
            End With
            'Return objADO.GetDataTable(True, "TICKET_AMADEUS_Rpt_IATA", dc)
            Return objADO.GetDataReader("TICKET_AMADEUS_Rpt_IATA", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarList_Rpt_Comisiones_DR(ByVal vdatFechaEmisionInicio As String, ByVal vdatFechaEmisionFin As String, _
                                Optional ByVal vstrCoPeriodo As String = "") As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@fecha1", vdatFechaEmisionInicio)
                .Add("@fecha2", vdatFechaEmisionFin)
                .Add("@CoPeriodo", vstrCoPeriodo)
            End With
            'Return objADO.GetDataTable(True, "TICKET_AMADEUS_Rpt_IATA", dc)
            Return objADO.GetDataReader("TICKET_AMADEUS_Rpt_Comisiones", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketAmadeus_TaxBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN
    Public Function ConsultarList(ByVal vstrCoTicket As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)
            Return objADO.GetDataTable(True, "TICKET_AMADEUS_TAX_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoBSPBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    Public Function ConsultarListxCoTicket(ByVal vstrCoTicket As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)
            Return objADO.GetDataTable(True, "DOCUMENTOBSP_Sel_ListxCoTicket", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketAmadeus_TarifaBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    Public Function COnsultarListxCoTicket(ByVal vstrCoTicket As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)

            Return objADO.GetDataTable(True, "TICKET_AMADEUS_TARIFA_SelxCoTicket", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCiudadCbo() As DataTable
        Try
            Return objADO.GetDataTable(True, "MAUBIGEO_Sel_CboSelCounter", New Dictionary(Of String, String))
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketAmadeus_ItinerarioBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrCoTicket As String, Optional ByVal vblnEdicion As Boolean = False) As Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoTicket", vstrCoTicket)

            If Not vblnEdicion Then
                Return objADO.GetDataTable(True, "TICKET_AMADEUS_ITINERARIO_Sel_List", dc)
            Else
                Return objADO.GetDataTable(True, "TICKET_AMADEUS_ITINERARIO_Sel_ListxCoTicket", dc)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Function


End Class

<ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
 Public Class clsMAAeropuertoBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrCoAero As String, _
                                  ByVal vstrNoDescripcion As String, ByVal vstrCoUbigeo As String, _
                                  ByVal vstrTxCiudad As String, ByVal vstrTxPais As String, Optional ByVal vblnFlActivo As Boolean = False, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoAero", vstrCoAero)
                .Add("@NoDescripcion", vstrNoDescripcion)
                .Add("@CoUbigeo", vstrCoUbigeo)
                .Add("@TxCiudad", vstrTxCiudad)
                .Add("@TxPais", vstrTxPais)
                .Add("@FlActivo", vblnFlActivo)
                .Add("@CoPais", vstrCoPais)
            End With

            Return objADO.GetDataTable(True, "MAAEROPUERTO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function ConsultarPK(ByVal vstrCoAero As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@CoAero", vstrCoAero)

        Dim strSQL As String = "MAAEROPUERTO_Sel_Pk"
        Return objADO.GetDataReader(strSQL, dc)
    End Function

    Public Overloads Function ConsultarPk_Validar(ByVal vstrCoAero As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoAero", vstrCoAero)

            Return objADO.GetDataTable(True, "MAAEROPUERTO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCbo() As DataTable
        Try
            Return objADO.GetDataTable(True, "MAAEROPUERTO_Sel_Cbo", New Dictionary(Of String, String))
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strCiudadAeropuerto(ByVal vstrCoAero As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try

            dc.Add("@CoAero", vstrCoAero)
            dc.Add("@pCoCiudad", "")

            Return objADO.ExecuteSPOutput("MAAEROPUERTO_SelCiudadOutput", dc)

        Catch ex As Exception

            Throw
        End Try
    End Function
End Class