﻿Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq
Imports ComSEToursBE2
Imports System.Configuration
Imports System.Collections.Specialized

Public Module modJson

    Public objResponseSAP As New ResponseStatusSAP
    Public objResponsePaymentwall As New ResponseStatusPaymentwall
    Public gstrPuertoSAP As String = ConfigurationSettings.AppSettings("PuertoSAP")

    Public Sub DropPropertyJSON(ByRef vstrJsonObject As String, ByVal ParamArray vparPropertysJson() As String)
        Try
            Dim oJson As New JObject
            'Dim deserializedObject As Object = JsonConvert.DeserializeObject(Of Object)(vstrJsonObject)
            'oJson = JsonConvert.DeserializeObject(deserializedObject)

            oJson = getQueryObject(vstrJsonObject)
            If Not vparPropertysJson Is Nothing Then
                For Each Item As String In vparPropertysJson
                    oJson.Remove(Item)
                Next
            End If

            Dim strDataValueJSON = JsonConvert.SerializeObject(oJson, Formatting.Indented)
            vstrJsonObject = strDataValueJSON
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function getQueryObject(ByVal jsonString As String) As JObject
        Dim jsonObject As JObject
        Try
            jsonObject = JObject.Parse(jsonString)

        Catch ex As Exception
            jsonObject = New JObject
        End Try
        Return jsonObject
    End Function

    ''Public Function IsObjectProperty(ByVal vstrJsonObjetc As String, ByVal vstrPropertyName As String) As Boolean
    ''    Try
    ''        If InStr(vstrJsonObjetc, vstrPropertyName & " : {") > 0 Then Return True
    ''        Return False
    ''    Catch ex As Exception
    ''        Throw
    ''    End Try
    ''End Function
End Module
