﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsCotizacionBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN
    Dim gstrTipoOperContExportacion As String = "001"
    Dim gstrTipoOperContGravado As String = "002"
    Dim gstrTipoOperContProrrata As String = "006"

    Public Function ConsultarList(ByVal vstrIDUsuario As String, ByVal vstrCotizacion As String, _
                              ByVal vstrIDFile As String, ByVal vstrDescCliente As String, _
                              ByVal vstrTitulo As String, ByVal vstrEstado As String, _
                              ByVal vstrNombrePax As String, _
                              ByVal vchrModulo As Char, ByVal vblnFilesAntiguos As Boolean, _
                              ByVal vblnUsuariosNoAsignado As Boolean, _
                              ByVal vdatFechaInicioRango1 As Date, ByVal vdatFechaInicioRango2 As Date, _
                              ByVal vdatFecAsigReseRango1 As Date, ByVal vdatFecAsigReseRango2 As Date, _
                              ByVal vdatFecAsigOpeRango1 As Date, ByVal vdatFecAsigOperRango2 As Date, _
                              ByVal vblnFilesHistoricos As Boolean, Optional ByVal vstrCoTipoAPT As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDUsuario", vstrIDUsuario)
            dc.Add("@Cotizacion", vstrCotizacion)
            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@DescCliente", vstrDescCliente)
            dc.Add("@Titulo", vstrTitulo)
            dc.Add("@NombrePax", vstrNombrePax)
            dc.Add("@FecIniciRango1", vdatFechaInicioRango1)
            dc.Add("@FecIniciRango2", vdatFechaInicioRango2)
            dc.Add("@FecAsigReserRango1", vdatFecAsigReseRango1)
            dc.Add("@FecAsigReserRango2", vdatFecAsigReseRango2)

            Select Case vchrModulo
                Case "C"
                    dc.Add("@Estado", vstrEstado)
                    dc.Add("@FlHistorico", vblnFilesHistoricos)
                    dc.Add("@CoTipoAPT", vstrCoTipoAPT)
                    Return objADO.GetDataTable(True, "COTICAB_Sel_List", dc)

                Case "R"
                    dc.Add("@Estado", vstrEstado)
                    dc.Add("@FilesAntiguos", vblnFilesAntiguos)
                    dc.Add("@UsuarioNoAsignado", vblnUsuariosNoAsignado)
                    dc.Add("@FlHistorico", vblnFilesHistoricos)
                    Return objADO.GetDataTable(True, "COTICAB_Sel_ListReservas", dc)

                Case "O"
                    dc.Add("@Estado", vstrEstado)
                    dc.Add("@FilesAntiguos", vblnFilesAntiguos)
                    dc.Add("@UsuarioNoAsignado", vblnUsuariosNoAsignado)
                    dc.Add("@FechaAsigOperacionesRango1", vdatFecAsigOpeRango1)
                    dc.Add("@FechaAsigOperacionesRango2", vdatFecAsigOperRango2)
                    dc.Add("@FlHistorico", vblnFilesHistoricos)
                    Return objADO.GetDataTable(True, "COTICAB_Sel_ListOperaciones", dc)

                Case "T"
                    dc.Add("@Estado", vstrEstado)
                    dc.Add("@FlHistorico", vblnFilesHistoricos)
                    Return objADO.GetDataTable(True, "COTICAB_Sel_ListxTapa", dc)

                Case "D"
                    dc.Add("@Estado", vstrEstado)
                    dc.Add("@FlHistorico", vblnFilesHistoricos)
                    Return objADO.GetDataTable(True, "COTICAB_Sel_ListDebitMemo", dc)
                Case "P" 'Pre Liquidacion
                    dc.Add("@FlHistorico", vblnFilesHistoricos)
                    Return objADO.GetDataTable(True, "COTICAB_Sel_ListPreLiquidacion", dc)
            End Select

            'If Not vblnReserva Then
            '    dc.Add("@Estado", vstrEstado)
            '    Return objADO.GetDataTable(True, "COTICAB_Sel_List", dc)
            'Else
            '    dc.Add("@FilesAntiguos", vblnFilesAntiguos)
            '    dc.Add("@UsuarioNoAsignado", vblnUsuariosNoAsignado)
            '    Return objADO.GetDataTable(True, "COTICAB_Sel_ListReservas", dc)
            'End If
            Return Nothing

        Catch ex As Exception
            Throw

        End Try

    End Function

    Public Function InsertarAddIn(ByVal BE As clsCotizacionBE) As Int32
        Dim dc As New Dictionary(Of String, String)

        Try
            With dc
                .Add("@IDCliente", BE.IDCliente)
                .Add("@Titulo", BE.Titulo)
                .Add("@TipoPax", BE.TipoPax)
                .Add("@IDUsuario", BE.IDUsuario)
                .Add("@NroPax", BE.NroPax)
                .Add("@NroLiberados", BE.NroLiberados)
                .Add("@Tipo_Lib", BE.Tipo_Lib)
                .Add("@NuPedInt", BE.NuPedInt)
                .Add("@UserMod", BE.UserMod)
                .Add("@pIDCab", 0)
            End With

            Dim intIDCab As Int32 = objADO.ExecuteSPOutput("COTICAB_Ins_Outlook", dc)

            BE.IdCab = intIDCab
            Return intIDCab
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRepFBS(ByVal vdatFechaInicioRango1 As Date, ByVal vdatFechaInicioRango2 As Date) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@FechaRango1", vdatFechaInicioRango1)
            dc.Add("@FechaRango2", vdatFechaInicioRango2)

            Return objADO.GetDataTable(True, "COTICAB_Sel_RptFeedBackSheets", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPk(ByVal vintIDCab As Int32, Optional vstrCoUbigeo_Oficina As String = "") As SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)

            Dim strSql As String = "COTICAB_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarObservaciones_Pk(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Dim strSql As String = "COTICAB_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarObservacionVentas_Pk(ByVal vintIDCab As Int32) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pObservacionVentas", "")

            Return objADO.GetSPOutputValue("COTICAB_Sel_ObservacionVentas_Pk", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarAcomodos(ByVal vintIDPax As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDPax)
            Return objADO.GetDataReader("COTICAB_SelAcomodos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'Public Function dttConsultarPk(ByVal vintIDCab As Int32) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    dc.Add("@IDCab", vintIDCab)

    '    Dim strSql As String = "COTICAB_Sel_Pk"
    '    Return objADO.GetDataTable(True, strSql, dc)

    'End Function

    Public Function ConsultarPreciosCotizacion(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Dim strSql As String = "COTICAB_SelTotales_xIDCab"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function intDevuelveIDCabxIDDet(ByVal vintIDDet As Integer) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pIDCab", 0)

            Return objADO.GetSPOutputValue("COTIDET_SelIDCabxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarFilexIDCabOrdenServicio(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "COTICAB_SelCabeceraOrdenServicio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarFechaOutPeru_File(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "COTICAB_DevolverFechaOutPeru", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarFilexIDCab(ByVal vstrIDFile As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDFile", vstrIDFile)

        Dim strSql As String = "COTICAB_SelIDCab_xFile"
        Return objADO.GetDataReader(strSql, dc)
    End Function

    Public Function intConsultarIDCabxFile(ByVal vstrIDFile As String) As Integer
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDFile", vstrIDFile)
            dc.Add("@pIDCab", 0)

            Return objADO.GetSPOutputValue("COTICAB_Sel_IDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListaHotels(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCab)
            Return objADO.GetDataTable(True, "COTICAB_SelSoloInfoHoteles", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListaProveedores(ByVal vintIDCab As Integer, ByVal vintNuControlCalidad As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@NuControlCalidad", vintNuControlCalidad)

            Return objADO.GetDataTable(True, "COTICAB_Sel_ListProvxIDCab", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListaProveedores_SinFile(ByVal vstrCoProveedor As String, ByVal vintNuControlCalidad As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@NuControlCalidad", vintNuControlCalidad)

            Return objADO.GetDataTable(True, "CONTROL_CALIDAD_PRO_Sel_CoProveedor_SinFile", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarAniosVentas() As DataTable
        Try
            Return objADO.GetDataTable(True, "COTICAB_Sel_FechasVentas", New Dictionary(Of String, String))
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTipoCambioxIDCab(ByVal vintIDCab As Integer) As Single
        Dim sglTipoCambio As Single = 0
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)
        dc.Add("@pTipoCambio", sglTipoCambio)

        Dim strSql As String = "COTICAB_SelTipoCambioxIDCab"
        sglTipoCambio = objADO.GetSPOutputValue(strSql, dc)

        Return sglTipoCambio
    End Function

    Public Function ConsultarSbRptTapaFilePNR(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "COTICAB_Sel_SbRpt_CodigosPNR", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveMailxIDcabxArea(ByVal vintIDCab As Integer, ByVal vstrArea As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@Modulo", vstrArea)
            dc.Add("@pCorreo", "")

            Return objADO.GetSPOutputValue("COTICAB_SelCorreoUserVentasOutPut", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblRedondeoInmedSuperior(ByVal strValor As String, _
                                              ByVal strRedondeo As String, _
                                              Optional ByRef sglRedondeado As Single = 0, _
                                              Optional ByVal vblnPermiteNegativo As Boolean = False) As Double
        Dim dblReturn As Double = 0
        Dim strEntero As String
        Dim strDecimales As String

        If InStr(strValor, ".") > 0 Then
            strEntero = strValor.Substring(0, InStr(strValor, ".") - 1)
            strDecimales = Mid(strValor, InStr(strValor, ".") + 1)
        Else
            strEntero = strValor
            strDecimales = "0"
        End If
        If strDecimales.Length > 15 Then
            strDecimales = strDecimales.Substring(0, 15)
            strDecimales = Replace(strDecimales, "E", "0")
        End If

        If Not vblnPermiteNegativo Then
            strEntero = Replace(strEntero, "-", String.Empty)
        End If

        If strRedondeo = "1" Then
            If Convert.ToInt64(strDecimales) = 0 Then
                dblReturn = strEntero
            Else
                If Math.Abs(Convert.ToDouble(strEntero)) >= 0 Then
                    dblReturn = Math.Abs(Convert.ToDouble(strEntero)) + 1
                    If strEntero.Substring(0, 1) = "-" Then
                        dblReturn = dblReturn * (-1)
                    End If
                End If
            End If
        ElseIf strRedondeo = "0.5" Then
            If Convert.ToInt64(strDecimales) = 0 Then
                dblReturn = strEntero
            Else
                If Convert.ToByte(strDecimales.Substring(0, 1)) >= 5 Then
                    dblReturn = Math.Abs(Convert.ToDouble(strEntero)) + 1
                    If strEntero.Substring(0, 1) = "-" Then
                        dblReturn = dblReturn * (-1)
                    End If
                Else
                    dblReturn = strEntero & ".5"
                End If

            End If
        Else
            dblReturn = strValor
        End If

        sglRedondeado = Math.Abs(Convert.ToDouble(strValor) - dblReturn)
        Return dblReturn
    End Function

    Public Structure stTipoCambioMoneda
        Dim CoMoneda As String
        Dim SsTipCam As Single
    End Structure

    Public Function CalculoCotizacion(ByVal vstrIDServicio As String, _
                                      ByVal vintIDServicio_Det As Int32, ByVal vstrAnio As String, ByVal vstrTipo As String, _
                                      ByVal vstrTipoProv As String, ByVal vintPax As Int16, _
                                      ByVal vintLiberados As Int16, ByVal vstrTipo_Lib As Char, _
                                      ByVal vdblMargenCotiCab As Double, ByVal vdblTCambioCotiCab As Single, _
                                      ByVal vdblCostoReal As Double, ByVal vdblMargenAplicado As Double, ByVal vstrTipoPax As String, _
                                      ByVal vsglRedondeo As Single, _
                                      ByRef rdttDetCotiServ As DataTable, _
                                      Optional ByVal vsglIGV As Single = 0, _
                                      Optional ByVal vstrIDDet_DetCotiServ As String = "", _
                                      Optional ByVal vblnIncGuia As Boolean = False, _
                                      Optional ByVal vblnMargenCero As Boolean = False, _
                                      Optional ByVal vCnn As SqlClient.SqlConnection = Nothing, _
                                      Optional ByVal vCnn2 As SqlClient.SqlConnection = Nothing, _
                                      Optional ByVal vdatFechaServicio As Date = #1/1/1900#, _
                                      Optional ByVal vdblSSCR As Double = -1, _
                                      Optional ByVal vdblSTCR As Double = -1, _
                                      Optional ByVal vblnServicioTC As Boolean = False, _
                                      Optional ByVal vstrIDTipoHonario As String = "", _
                                      Optional ByVal vdicValsCostoRealTC As Dictionary(Of String, Double) = Nothing, _
                                      Optional ByVal vblnAcomVehic As Boolean = False) As Dictionary(Of String, Double)
        'ByVal vintIDServicio_Det As Int32, ByVal vstrAnio As String, ByVal vstrTipo As String, _
        Try
            Dim dblCostoReal As Double = 0, dblMargen As Double = 0, dblMargenAplicado As Double = 0
            Dim dblCostoLiberado As Double = 0, dblMargenLiberado As Double = 0, dblTotal As Decimal = 0, dblTotImpto As Double = 0
            Dim dblSSCR As Double = 0, dblSSMargen As Double = 0, dblSSCL As Double = 0, dblSSML As Double = 0
            Dim dblSSTotal As Double = 0
            Dim dblSTCR As Double = 0, dblSTMargen As Double = 0, dblSTTotal As Decimal = 0

            Dim dblCostoRealImpto As Double = 0
            Dim dblCostoLiberadoImpto As Double = 0
            Dim dblMargenImpto As Double = 0
            Dim dblMargenLiberadoImpto As Double = 0
            Dim dblSSCRImpto As Double = 0
            Dim dblSSCLImpto As Double = 0
            Dim dblSSMargenImpto As Double = 0
            Dim dblSSMargenLiberadoImpto As Double = 0
            Dim dblSTCRImpto As Double = 0
            Dim dblSTMargenImpto As Double = 0

            Dim dblAcCostoReal As Double = 0, dblAcMargen As Double = 0, dblAcMargenAplicado As Double = 0
            Dim dblAcCostoLiberado As Double = 0, dblAcMargenLiberado As Double = 0, dblAcTotal As Double = 0, dblAcTotImpto As Double = 0

            Dim dblAcSSCR As Double = 0, dblAcSSMargen As Double = 0, dblAcSSCL As Double = 0, dblAcSSML As Double = 0
            Dim dblAcSSTotal As Double = 0

            Dim dblAcSTCR As Double = 0, dblAcSTMargen As Double = 0, dblAcSTTotal As Double = 0


            Dim dblAcCostoRealImpto As Double = 0
            Dim dblAcCostoLiberadoImpto As Double = 0
            Dim dblAcMargenImpto As Double = 0
            Dim dblAcMargenLiberadoImpto As Double = 0
            Dim dblAcSSCRImpto As Double = 0
            Dim dblAcSSCLImpto As Double = 0
            Dim dblAcSSMargenImpto As Double = 0
            Dim dblAcSSMargenLiberadoImpto As Double = 0
            Dim dblAcSTCRImpto As Double = 0
            Dim dblAcSTMargenImpto As Double = 0
            Dim dblTCambio As Single = 0

            Dim sglRedondeadoTotal As Single = 0
            Dim sglRedondeo As Single = 0
            Dim sglAcRedondeo As Single = 0

            '----------

            Dim dblCostoDoble As Double, dblCostoSimple As Double, dblCostoTriple As Double
            Dim strTipoLib As Char
            Dim intPaxLiberados As Int16, intPaxLiberadosFaltantes As Int16, intPaxLiberadosDetServ As Int16
            Dim dblMontoL As Double = 0
            Dim blnLiberadosPermitidos As Boolean
            Dim blnTieneProveedorIntern As Boolean = False

            Dim dc As New Dictionary(Of String, Double)
            Dim sglIGV As Single = (vsglIGV / 100)

            Dim drServ As SqlClient.SqlDataReader
            If vCnn Is Nothing Then
                Dim objServ As New clsServicioProveedorBN
                drServ = objServ.ConsultarPk(vstrIDServicio, vCnn)
            Else
                Dim objServ As New clsServicioProveedorBT
                drServ = objServ.drConsultarPk(vstrIDServicio, vCnn)
            End If

            drServ.Read()
            If Not vblnMargenCero Then
                If vdblMargenAplicado = -1 Then
                    If vblnServicioTC Then
                        dblMargenAplicado = vdblMargenCotiCab
                    Else
                        blnTieneProveedorIntern = CBool(drServ("TieneProvInternacional"))
                        If blnTieneProveedorIntern Then
                            dblMargenAplicado = If(IsDBNull(drServ("MargenProvInt")) = True, 0, drServ("MargenProvInt"))
                        Else
                            dblMargenAplicado = If(IsDBNull(drServ("Comision")) = True, 0, drServ("Comision"))
                        End If
                        dblMargenAplicado += vdblMargenCotiCab
                    End If
                Else
                    dblMargenAplicado = vdblMargenAplicado
                End If
            End If
            drServ.Close()


            Dim objDetServ As Object
            If vCnn Is Nothing Then
                objDetServ = New clsServicioProveedorBN.clsDetalleServicioProveedorBN
            Else
                objDetServ = New clsServicioProveedorBT.clsDetalleServicioProveedorBT
            End If
            If vstrTipoProv = "001" Then 'Or vstrTipoProv = "002" Then 'Hoteles o Restaurantes
                Using drDetServ As SqlClient.SqlDataReader = objDetServ.ConsultarPkSinAnulado(vintIDServicio_Det, vCnn)
                    drDetServ.Read()

                    If drDetServ.HasRows Then
                        Dim dblCostoAbsoluto As Double = 0
                        Dim dblCostoAbsolutoLib As Double = 0

                        If Not IsDBNull(drDetServ("TC")) Then
                            dblTCambio = drDetServ("TC")
                        End If
                        If dblTCambio <> 0 And vdblTCambioCotiCab <> 0 Then
                            dblTCambio = vdblTCambioCotiCab
                        End If

                        If vintPax = 1 Then
                            dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                            If dblTCambio > 0 Then
                                dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                            End If
                            dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                            If dblTCambio > 0 Then
                                dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                            End If
                            dblCostoTriple = 0

                            If vdblCostoReal = -1 Then
                                dblCostoReal = dblCostoSimple
                            Else
                                dblCostoReal = vdblCostoReal
                            End If
                            If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal

                            'Considerando la definición Triple
                            Dim strIDDefTriple As String = If(IsDBNull(drDetServ("IDServicio_DetxTriple")) = True, "", drDetServ("IDServicio_DetxTriple").ToString)
                            If strIDDefTriple = "" Then
                                dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                                If dblTCambio > 0 Then
                                    dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                                End If
                            Else
                                Dim dblTCambioDefTrip As Double = 0
                                If Not IsDBNull(drDetServ("TipoCambioDefTrip")) Then
                                    dblTCambioDefTrip = drDetServ("TipoCambioDefTrip")
                                End If

                                dblCostoTriple = If(IsDBNull(drDetServ("MontoTrixCodigo")) = True, 0, drDetServ("MontoTrixCodigo"))
                                If dblTCambioDefTrip > 0 Then
                                    dblCostoTriple = If(IsDBNull(drDetServ("MontoTrisxCodigo")) = True, 0, drDetServ("MontoTrisxCodigo"))
                                End If
                            End If

                        ElseIf vintPax = 2 Then
                            dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                            If dblTCambio > 0 Then
                                dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                            End If
                            dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                            If dblTCambio > 0 Then
                                dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                            End If

                            dblCostoTriple = 0

                            If vdblCostoReal = -1 Then
                                If vstrTipoProv = "001" Then
                                    dblCostoReal = dblCostoDoble / 2
                                    If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoDoble
                                Else
                                    dblCostoReal = dblCostoSimple
                                    If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal
                                End If

                            Else
                                dblCostoReal = vdblCostoReal
                            End If

                            'Considerando la definición Triple
                            Dim strIDDefTriple As String = If(IsDBNull(drDetServ("IDServicio_DetxTriple")) = True, "", drDetServ("IDServicio_DetxTriple").ToString)
                            If strIDDefTriple = "" Then
                                dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                                If dblTCambio > 0 Then
                                    dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                                End If
                            Else
                                Dim dblTCambioDefTrip As Double = 0
                                If Not IsDBNull(drDetServ("TipoCambioDefTrip")) Then
                                    dblTCambioDefTrip = drDetServ("TipoCambioDefTrip")
                                End If

                                dblCostoTriple = If(IsDBNull(drDetServ("MontoTrixCodigo")) = True, 0, drDetServ("MontoTrixCodigo"))
                                If dblTCambioDefTrip > 0 Then
                                    dblCostoTriple = If(IsDBNull(drDetServ("MontoTrisxCodigo")) = True, 0, drDetServ("MontoTrisxCodigo"))
                                End If
                            End If

                        Else

                            dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                            If dblTCambio > 0 Then
                                dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                            End If

                            dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                            If dblTCambio > 0 Then
                                dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                            End If

                            'Considerando la definición Triple
                            Dim strIDDefTriple As String = If(IsDBNull(drDetServ("IDServicio_DetxTriple")) = True, "", drDetServ("IDServicio_DetxTriple").ToString)
                            If strIDDefTriple = "" Then
                                dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                                If dblTCambio > 0 Then
                                    dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                                End If
                            Else
                                Dim dblTCambioDefTrip As Double = 0
                                If Not IsDBNull(drDetServ("TipoCambioDefTrip")) Then
                                    dblTCambioDefTrip = drDetServ("TipoCambioDefTrip")
                                End If

                                dblCostoTriple = If(IsDBNull(drDetServ("MontoTrixCodigo")) = True, 0, drDetServ("MontoTrixCodigo"))
                                If dblTCambioDefTrip > 0 Then
                                    dblCostoTriple = If(IsDBNull(drDetServ("MontoTrisxCodigo")) = True, 0, drDetServ("MontoTrisxCodigo"))
                                End If
                            End If
                        End If

                        Dim blnPlanAlimenticio As Boolean = drDetServ("PlanAlimenticio")
                        If vdblCostoReal = -1 Then
                            If vstrTipoProv = "001" Then
                                If blnPlanAlimenticio Then
                                    dblCostoReal = dblCostoSimple
                                    'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal
                                Else
                                    dblCostoReal = dblCostoDoble / 2
                                    'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoDoble
                                End If
                                'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoSimple
                            Else
                                dblCostoReal = dblCostoSimple
                                'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoSimple
                            End If
                        Else
                            dblCostoReal = vdblCostoReal
                        End If

                        'dblMargen = dblCostoReal * (dblMargenAplicado / 100)
                        dblMargen = 0

                        intPaxLiberados = 0
                        If vintLiberados > 0 Then
                            dblCostoAbsolutoLib = dblCostoReal

                            If (vintPax + vintLiberados) = 1 Then
                                If vdblCostoReal = -1 Then

                                    dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                    If dblTCambio > 0 Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                    End If
                                End If

                            ElseIf (vintPax + vintLiberados) = 2 Then

                                If vdblCostoReal = -1 Then
                                    If vstrTipoProv = "001" Then
                                        If vstrTipo_Lib = "S" Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                            If dblTCambio > 0 Then
                                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                            End If

                                        ElseIf vstrTipo_Lib = "D" Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                                            If dblTCambio > 0 Then
                                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                                            End If
                                            dblCostoAbsolutoLib = dblCostoAbsolutoLib / 2
                                        End If

                                    Else
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                        If dblTCambio > 0 Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                        End If

                                    End If
                                End If
                            Else

                            End If

                            'Dim blnPlanAlimenticio As Boolean = drDetServ("PlanAlimenticio")
                            If vdblCostoReal = -1 Then
                                If vstrTipoProv = "001" Then
                                    If blnPlanAlimenticio Then

                                        If vstrTipo_Lib = "S" Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                            If dblTCambio > 0 Then
                                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                            End If

                                        ElseIf vstrTipo_Lib = "D" Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                                            If dblTCambio > 0 Then
                                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                                            End If
                                            dblCostoAbsolutoLib = dblCostoAbsolutoLib / 2
                                        End If

                                    Else

                                        If vstrTipo_Lib = "S" Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                            If dblTCambio > 0 Then
                                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                            End If

                                        ElseIf vstrTipo_Lib = "D" Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                                            If dblTCambio > 0 Then
                                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                                            End If
                                            dblCostoAbsolutoLib = dblCostoAbsolutoLib / 2
                                        End If


                                    End If
                                Else
                                    dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                    If dblTCambio > 0 Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                    End If
                                End If
                            End If

                            If drDetServ("PoliticaLiberado") = True Then
                                strTipoLib = If(IsDBNull(drDetServ("TipoLib")) = True, "", drDetServ("TipoLib"))
                                intPaxLiberadosDetServ = If(IsDBNull(drDetServ("Liberado")) = True, 0, drDetServ("Liberado"))
                                dblMontoL = If(IsDBNull(drDetServ("MontoL")) = True, 0, drDetServ("MontoL"))
                                If strTipoLib = "H" Then
                                    intPaxLiberadosDetServ = intPaxLiberadosDetServ * 2
                                End If

                                If intPaxLiberadosDetServ > 0 Then
                                    intPaxLiberados = 0
                                    If vintPax >= intPaxLiberadosDetServ Then
                                        intPaxLiberados = vintPax / intPaxLiberadosDetServ
                                    End If

                                    If Not IsDBNull(drDetServ("MaximoLiberado")) Then
                                        If drDetServ("MaximoLiberado") < intPaxLiberados Then
                                            intPaxLiberados = drDetServ("MaximoLiberado")
                                        End If
                                    End If

                                    If intPaxLiberados <> vintLiberados Then
                                        If intPaxLiberados < vintLiberados Then
                                            intPaxLiberadosFaltantes = vintLiberados - intPaxLiberados
                                        Else
                                            intPaxLiberados = vintLiberados
                                        End If
                                        If intPaxLiberadosFaltantes = 0 Then blnLiberadosPermitidos = False
                                    Else
                                        'blnLiberadosPermitidos = True
                                        If intPaxLiberadosFaltantes > 0 Then blnLiberadosPermitidos = True
                                    End If
                                End If
                            Else
                                intPaxLiberados = 1
                                intPaxLiberadosFaltantes = vintLiberados
                            End If
                        End If

                        If Not blnLiberadosPermitidos Then
                            'If intPaxLiberados > 0 Then
                            If dblMontoL > 0 Then
                                'dblCostoLiberado = (dblMontoL / vintPax) * intPaxLiberados

                                dblCostoLiberado = (dblCostoAbsolutoLib) / (vintPax + vintLiberados)
                                dblCostoLiberado = (dblCostoLiberado / vintPax) * vintLiberados
                            Else
                                'dblCostoLiberado = (dblCostoSimple / vintPax) * intPaxLiberados
                                'dblCostoLiberado = Math.Abs((dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax)
                                dblCostoLiberado = Math.Abs((dblCostoAbsolutoLib) / vintPax)
                            End If
                            'End If
                        Else
                            'dblCostoLiberado = Math.Abs((dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax)
                            dblCostoLiberado = 0
                        End If

                        'HLF-20151223-I
                        'If intPaxLiberadosFaltantes > 0 Then
                        '    dblCostoLiberado = dblCostoLiberado + ((dblCostoSimple / vintPax) * (intPaxLiberadosFaltantes - 1))
                        'End If
                        If vintLiberados >= 2 Then
                            If vstrTipo_Lib = "S" Then
                                dblCostoLiberado = dblCostoSimple / vintPax
                            Else
                                If dblCostoDoble = 0 Then
                                    dblCostoLiberado = (dblCostoSimple * 2) / vintPax
                                Else
                                    dblCostoLiberado = dblCostoDoble / vintPax
                                End If

                            End If

                        Else
                            If intPaxLiberadosFaltantes > 0 Then
                                dblCostoLiberado = dblCostoLiberado + ((dblCostoSimple / vintPax) * (intPaxLiberadosFaltantes - 1))
                            End If
                        End If
                        'HLF-20151223-F'''''''''''''''''''''

                        If vstrTipo_Lib = "S" And Not blnLiberadosPermitidos And _
                            (vintPax >= intPaxLiberadosDetServ And intPaxLiberadosDetServ > 0) Then
                            Dim dblDiferSS As Double = If(IsDBNull(drDetServ("DiferSS")) = True, 0, drDetServ("DiferSS"))
                            dblCostoLiberado = (dblCostoReal + dblDiferSS) / vintPax

                            'dblCostoLiberado = dblCostoLiberado * vintLiberados
                            'If vstrTipoProv = "001" Then 'hotel
                            'dblCostoLiberado = dblCostoLiberado * vintLiberados
                            'Else 'restaurant
                            dblCostoLiberado = dblCostoLiberado * intPaxLiberados
                            'End If
                        End If
                        If intPaxLiberados > 0 Then
                            dblCostoLiberado += (dblMontoL / vintPax)
                        End If

                        Dim blnIGV As Boolean = False
                        Dim blnProrrata As Boolean = False
                        Dim sglPorcProrrata As Single = 0
                        If drDetServ("idTipoOC") = gstrTipoOperContExportacion Then
                            If vstrTipoPax <> "EXT" Then 'si es peruano
                                If drDetServ("Afecto") = True Then
                                    blnIGV = True
                                End If
                            End If
                        ElseIf drDetServ("idTipoOC") = gstrTipoOperContGravado Then 'OperacContable
                            blnIGV = True
                        ElseIf drDetServ("idTipoOC") = gstrTipoOperContProrrata Then 'OperacContable
                            blnIGV = True
                            blnProrrata = True

                            'Dim objTabBN As New clsTablasApoyoBN
                            'Dim dr As SqlClient.SqlDataReader = objTabBN.ConsultarParametro
                            'If dr.HasRows Then
                            '    dr.Read()
                            '    sglPorcProrrata = 1 - (dr("PoProrrata") / 100)
                            'End If
                            'dr.Close()

                            Dim objProrr As New clsTablasApoyoBN.clsParametroBN.clsProrrataBN
                            sglPorcProrrata = 1 - (objProrr.sglProrrataxFecha(vdatFechaServicio) / 100)
                        End If

                        If vstrTipoPax <> "EXT" Or vblnIncGuia Then 'si es peruano
                            blnIGV = True
                        End If

                        'dblMargenLiberado = dblCostoLiberado * (dblMargenAplicado / 100)
                        dblMargenLiberado = (dblCostoLiberado / (1 - (dblMargenAplicado / 100))) - dblCostoLiberado
                        If blnIGV Then
                            dblMargenLiberado = ((dblCostoLiberado * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblCostoLiberado * (1 + sglIGV))
                        End If

                        'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                        If blnIGV Then
                            dblTotImpto = (dblCostoReal * sglIGV) + _
                                    (dblCostoLiberado * sglIGV) + _
                                    (dblMargen * sglIGV) + _
                                    (dblMargenLiberado * sglIGV)
                        Else
                            dblTotImpto = 0

                        End If

                        If blnProrrata Then
                            dblTotImpto = ((dblCostoReal * sglIGV) * sglPorcProrrata)
                        End If

                        dblTotal = dblCostoReal '+ dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                        'dblMargen = dblTotal * (dblMargenAplicado / 100)


                        dblMargen = (dblTotal / (1 - (dblMargenAplicado / 100))) - dblTotal
                        If blnIGV Then
                            dblMargen = ((dblTotal * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblTotal * (1 + sglIGV))
                        End If
                        'dblTotal = dblTotal + dblMargen

                        dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                        If blnProrrata Then
                            Dim dblTotal2 As Double = (dblCostoReal + dblTotImpto) / (1 - (dblMargenAplicado / 100))
                            dblMargen = dblRedondeoInmedSuperior(dblTotal2, vsglRedondeo) * (dblMargenAplicado / 100)
                            'dblMargen = dblTotal2 * (dblMargenAplicado / 100)
                            dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto
                        End If

                        'Suplementos
                        If vdblSSCR = -1 Then
                            dblSSCR = dblCostoSimple - dblCostoReal
                            If blnTieneProveedorIntern Then
                                dblSSCR = Math.Abs(dblCostoSimple - dblCostoReal)
                            End If
                        Else
                            dblSSCR = vdblSSCR
                        End If
                        'dblSSMargen = dblSSCR * (dblMargenAplicado / 100)
                        dblSSMargen = (dblSSCR / (1 - (dblMargenAplicado / 100))) - dblSSCR

                        If vstrTipo_Lib = "S" Then
                            dblSSCL = 0
                        ElseIf vstrTipo_Lib = "D" Then
                            dblSSCL = (dblCostoDoble / vintPax) - (dblCostoSimple / vintPax)
                        End If
                        'dblSSML = dblSSCL * (dblMargenAplicado / 100)
                        dblSSML = (dblSSCL / (1 - (dblMargenAplicado / 100))) - dblSSCL

                        dblSSTotal = dblSSCR + dblSSMargen + dblSSCL + dblSSML

                        'No aplica el costo triple para los casos cuando sea un Oper. Internacional y su costos en
                        'Simple y Doble sean 0
                        If dblCostoTriple > 0 Then
                            If vdblSTCR = -1 Then
                                dblSTCR = (dblCostoTriple / 3) - dblCostoReal
                                If blnTieneProveedorIntern And dblSSTotal = 0 And dblTotal = 0 Then
                                    dblSTCR = 0
                                End If
                            Else
                                dblSTCR = vdblSTCR
                            End If
                            dblSTMargen = dblSTCR * (dblMargenAplicado / 100)
                            'dblSTMargen = (dblSTCR / (1 - (dblMargenAplicado / 100))) - dblSTCR
                            dblSTTotal = dblSTCR + dblSTMargen
                        Else
                            If vdblSTCR = -1 Then
                            Else
                                dblSTCR = vdblSTCR
                            End If
                            dblSTMargen = dblSTCR * (dblMargenAplicado / 100)
                            'dblSTMargen = (dblSTCR / (1 - (dblMargenAplicado / 100))) - dblSTCR
                            dblSTTotal = dblSTCR + dblSTMargen
                        End If



                        'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                        If blnIGV Then
                            dblCostoRealImpto = dblCostoReal * sglIGV
                            dblCostoLiberadoImpto = dblCostoLiberado * sglIGV
                            dblMargenImpto = dblMargen * sglIGV
                            dblMargenLiberadoImpto = dblMargenLiberado * sglIGV
                            dblSSCRImpto = dblSSCR * sglIGV
                            dblSSCLImpto = dblSSCL * sglIGV
                            dblSSMargenImpto = dblSSMargen * sglIGV
                            dblSSMargenLiberadoImpto = dblSSML * sglIGV
                            dblSTCRImpto = dblSTCR * sglIGV
                            dblSTMargenImpto = dblSTMargen * sglIGV

                        Else
                            dblCostoRealImpto = 0
                            dblCostoLiberadoImpto = 0
                            dblMargenImpto = 0
                            dblMargenLiberadoImpto = 0
                            dblSSCRImpto = 0
                            dblSSCLImpto = 0
                            dblSSMargenImpto = 0
                            dblSSMargenLiberadoImpto = 0
                            dblSTCRImpto = 0
                            dblSTMargenImpto = 0
                        End If

                        'Considerar los Imptuestos :31/07/2014
                        dblSSTotal += dblSSCRImpto
                    End If
                    'fin drLect


                    If dblTCambio = 0 Then
                        dc.Add("CostoReal", dblCostoReal)
                        dc.Add("Margen", dblMargen + dblMargenLiberado)
                        dc.Add("CostoLiberado", dblCostoLiberado)
                        dc.Add("MargenLiberado", dblMargenLiberado)
                        dc.Add("MargenAplicado", dblMargenAplicado)
                        dc.Add("TotImpto", dblTotImpto)
                        dc.Add("TotalOrig", dblTotal)

                        dc.Add("Total", dblRedondeoInmedSuperior(Math.Round(dblTotal, 4), vsglRedondeo, sglRedondeadoTotal))
                        dc.Add("RedondeoTotal", sglRedondeadoTotal)

                        dc.Add("SSCR", dblSSCR)
                        dc.Add("SSMargen", dblSSMargen)
                        dc.Add("SSCL", dblSSCL)
                        dc.Add("SSML", dblSSML)
                        dc.Add("SSTotalOrig", dblSSTotal)
                        dc.Add("SSTotal", dblRedondeoInmedSuperior(dblSSTotal, vsglRedondeo))

                        dc.Add("STCR", dblSTCR)
                        dc.Add("STMargen", dblSTMargen)
                        dc.Add("STTotalOrig", dblSTTotal)
                        dc.Add("STTotal", dblRedondeoInmedSuperior(Math.Round(dblSTTotal, 4), vsglRedondeo, , True))

                        dc.Add("CostoRealImpto", dblCostoRealImpto)
                        dc.Add("CostoLiberadoImpto", dblCostoLiberadoImpto)
                        dc.Add("MargenImpto", dblMargenImpto)
                        dc.Add("MargenLiberadoImpto", dblMargenLiberadoImpto)
                        dc.Add("SSCRImpto", dblSSCRImpto)
                        dc.Add("SSCLImpto", dblSSCLImpto)
                        dc.Add("SSMargenImpto", dblSSMargenImpto)
                        dc.Add("SSMargenLiberadoImpto", dblSSMargenLiberadoImpto)
                        dc.Add("STCRImpto", dblSTCRImpto)
                        dc.Add("STMargenImpto", dblSTMargenImpto)
                    Else
                        dc.Add("CostoReal", If(vdblCostoReal = -1, dblCostoReal / dblTCambio, vdblCostoReal))
                        dc.Add("Margen", If(vdblCostoReal = -1, (dblMargen + dblMargenLiberado) / dblTCambio, dblMargen))
                        dc.Add("CostoLiberado", dblCostoLiberado / dblTCambio)
                        dc.Add("MargenLiberado", dblMargenLiberado / dblTCambio)
                        dc.Add("MargenAplicado", dblMargenAplicado)
                        dc.Add("TotImpto", dblTotImpto / dblTCambio)
                        dc.Add("TotalOrig", dblTotal)
                        dc.Add("Total", dblRedondeoInmedSuperior(Math.Round(If(vdblCostoReal = -1, dblTotal / Convert.ToDecimal(dblTCambio), dblTotal), 4), vsglRedondeo, sglRedondeadoTotal))
                        dc.Add("RedondeoTotal", sglRedondeadoTotal)

                        dc.Add("SSCR", If(vdblSSCR = -1, dblSSCR / dblTCambio, vdblSSCR))
                        dc.Add("SSMargen", dblSSMargen / dblTCambio)
                        dc.Add("SSCL", dblSSCL / dblTCambio)
                        dc.Add("SSML", dblSSML / dblTCambio)
                        dc.Add("SSTotalOrig", dblSSTotal)
                        dc.Add("SSTotal", dblRedondeoInmedSuperior(dblSSTotal / Convert.ToDecimal(dblTCambio), vsglRedondeo))

                        dc.Add("STCR", If(vdblSTCR = -1, dblSTCR / dblTCambio, vdblSTCR))
                        dc.Add("STMargen", dblSTMargen / dblTCambio)
                        dc.Add("STTotalOrig", dblSTTotal)
                        dc.Add("STTotal", dblRedondeoInmedSuperior(dblSTTotal / Convert.ToDecimal(dblTCambio), vsglRedondeo, , True))

                        dc.Add("CostoRealImpto", dblCostoRealImpto / dblTCambio)
                        dc.Add("CostoLiberadoImpto", dblCostoLiberadoImpto / dblTCambio)
                        dc.Add("MargenImpto", dblMargenImpto / dblTCambio)
                        dc.Add("MargenLiberadoImpto", dblMargenLiberadoImpto / dblTCambio)
                        dc.Add("SSCRImpto", dblSSCRImpto / dblTCambio)
                        dc.Add("SSCLImpto", dblSSCLImpto / dblTCambio)
                        dc.Add("SSMargenImpto", dblSSMargenImpto / dblTCambio)
                        dc.Add("SSMargenLiberadoImpto", dblSSMargenLiberadoImpto / dblTCambio)
                        dc.Add("STCRImpto", dblSTCRImpto / dblTCambio)
                        dc.Add("STMargenImpto", dblSTMargenImpto / dblTCambio)
                    End If

                    drDetServ.Close()
                End Using
            Else 'No Hoteles si restaurantes

                Dim drDetServ As SqlClient.SqlDataReader
                If vstrTipoProv = "002" Then
                    drDetServ = objDetServ.ConsultarPkSinAnulado(vintIDServicio_Det, vCnn)
                Else
                    Dim intIDDetAcVe As Integer = 0
                    If vblnAcomVehic Then
                        If IsNumeric(vstrIDDet_DetCotiServ) Then
                            intIDDetAcVe = vstrIDDet_DetCotiServ
                        End If
                    End If

                    If vCnn Is Nothing Then
                        Dim objServBT As New clsServicioProveedorBN
                        drDetServ = objServBT.ConsultarxIDServicioAnio(vstrIDServicio, vstrAnio, vstrTipo, intIDDetAcVe)
                    Else
                        Dim objServBT As New clsServicioProveedorBT
                        drDetServ = objServBT.ConsultarxIDServicioAnio(vstrIDServicio, vstrAnio, vstrTipo, intIDDetAcVe, vCnn)
                    End If

                End If

                While drDetServ.Read()
                    If vblnIncGuia Then
                        If vintIDServicio_Det <> drDetServ("IDServicio_Det") Then
                            GoTo SgteDet
                        End If
                    End If

                    dblTCambio = 0
                    If Not IsDBNull(drDetServ("TC")) Then
                        dblTCambio = drDetServ("TC")
                    End If
                    If dblTCambio <> 0 And vdblTCambioCotiCab <> 0 Then
                        dblTCambio = vdblTCambioCotiCab
                    End If


                    intPaxLiberados = 0
                    If vintLiberados > 0 Then
                        If drDetServ("PoliticaLiberado") = True Then
                            strTipoLib = If(IsDBNull(drDetServ("TipoLib")) = True, "", drDetServ("TipoLib"))
                            intPaxLiberadosDetServ = If(IsDBNull(drDetServ("Liberado")) = True, 0, drDetServ("Liberado"))
                            dblMontoL = If(IsDBNull(drDetServ("MontoL")) = True, 0, drDetServ("MontoL"))
                            If strTipoLib = "H" Then
                                intPaxLiberadosDetServ = intPaxLiberadosDetServ * 2
                            End If

                            If intPaxLiberadosDetServ > 0 Then

                                intPaxLiberados = 0
                                If vintPax >= intPaxLiberadosDetServ Then
                                    intPaxLiberados = vintPax / intPaxLiberadosDetServ
                                End If

                                If Not IsDBNull(drDetServ("MaximoLiberado")) Then
                                    If drDetServ("MaximoLiberado") < intPaxLiberados Then
                                        intPaxLiberados = drDetServ("MaximoLiberado")
                                    End If
                                End If

                                If intPaxLiberados <> vintLiberados Then
                                    If intPaxLiberados < vintLiberados Then
                                        intPaxLiberadosFaltantes = vintLiberados - intPaxLiberados
                                    Else
                                        intPaxLiberados = vintLiberados
                                    End If
                                    'blnLiberadosPermitidos = False
                                    If intPaxLiberadosFaltantes = 0 Then blnLiberadosPermitidos = False
                                Else
                                    ' blnLiberadosPermitidos = True
                                    If intPaxLiberadosFaltantes > 0 Then blnLiberadosPermitidos = True
                                End If
                            End If
                        Else
                            intPaxLiberados = 1
                            intPaxLiberadosFaltantes = vintLiberados
                        End If
                    End If

                    dblCostoReal = 0
                    Dim dblCostoAbsoluto As Double = 0
                    Dim dblCostoAbsolutoLib As Double = 0
                    Dim blnPorRango As Boolean = False
                    If vdblCostoReal = -1 Then
                        If Not IsDBNull(drDetServ("Monto_sgl")) Then
                            If vblnServicioTC Then
                                Dim strCoTipoCostoTC As String = drDetServ("CoTipoCostoTC")
                                If strCoTipoCostoTC = "TAX" Or strCoTipoCostoTC = "TEL" Then
                                    If vstrIDTipoHonario = "002" Then
                                        dblTCambio = 1
                                    End If
                                End If
                                Select Case strCoTipoCostoTC
                                    Case "TAX"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblTaxi")
                                    Case "TEL"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblTarjetaTelefonica")
                                    Case "VID"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblViaticosInternacionales")
                                    Case "VIS"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblViaticosNacionales")
                                    Case "NAD"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblNochesAntesDsp")
                                    Case "VUE"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblVuelos")
                                    Case "TAE"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblTicketAereo")
                                    Case "HON"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblHonorarios")
                                    Case "IGV"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblIGVHoteles")
                                    Case "OTR"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblOtros")
                                    Case "GPD"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblGebPrimerDia")
                                    Case "GUD"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblGebUltimoDia")
                                End Select

                                'If dblTCambio > 0 Then
                                '    dblCostoReal = dblCostoReal / dblTCambio
                                'End If
                                dblCostoReal = dblCostoReal / vintPax '(vintPax + vintLiberados)
                            Else
                                dblCostoReal = drDetServ("Monto_sgl")
                                If dblTCambio > 0 Then
                                    dblCostoReal = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                End If

                                If vblnIncGuia Then
                                    dblCostoReal = dblCostoReal / vintPax
                                End If

                                If vintLiberados > 0 Then dblCostoAbsolutoLib = dblCostoReal
                            End If

                        Else
                            Dim objDetCostosServ As New clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsCostosDetalleServicioProveedorBN
                            'Dim dttCostos As DataTable = objDetCostosServ.ConsultarListxIDDetalle(vintIDServicio_Det)
                            Dim dttCostos As DataTable = objDetCostosServ.ConsultarListxIDDetalle(drDetServ("IDServicio_Det"), vCnn2)
                            Dim intPaxHastaMax As Int16 = 0
                            Dim dblCostoRealTemp As Double
                            Dim dblCostoAbsolutoTemp As Double
                            If vblnServicioTC Then
                                Dim strCoTipoCostoTC As String = drDetServ("CoTipoCostoTC")
                                If strCoTipoCostoTC = "TAX" Or strCoTipoCostoTC = "TEL" Then
                                    If vstrIDTipoHonario = "002" Then
                                        dblTCambio = 1
                                    End If
                                End If
                                Select Case strCoTipoCostoTC
                                    Case "TAX"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblTaxi")
                                    Case "TEL"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblTarjetaTelefonica")
                                    Case "VID"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblViaticosInternacionales")
                                    Case "VIS"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblViaticosNacionales")
                                    Case "NAD"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblNochesAntesDsp")
                                    Case "VUE"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblVuelos")
                                    Case "TAE"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblTicketAereo")
                                    Case "HON"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblHonorarios")
                                    Case "IGV"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblIGVHoteles")
                                    Case "OTR"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblOtros")
                                    Case "GPD"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblGebPrimerDia")
                                    Case "GUD"
                                        dblCostoReal = vdicValsCostoRealTC.Item("dblGebUltimoDia")
                                End Select

                                'If dblTCambio > 0 Then
                                '    dblCostoReal = dblCostoReal / dblTCambio
                                'End If
                                dblCostoReal = dblCostoReal / vintPax '(vintPax + vintLiberados)
                            Else
                                For Each drCostos As DataRow In dttCostos.Rows
                                    If vintPax >= drCostos("PaxDesde") And vintPax <= drCostos("PaxHasta") Then
                                        dblCostoReal = drCostos("Monto") / vintPax
                                        If vintLiberados > 0 Then dblCostoAbsoluto = drCostos("Monto")
                                        Exit For
                                    End If

                                    If drCostos("PaxHasta") > intPaxHastaMax Then
                                        intPaxHastaMax = drCostos("PaxHasta")
                                    End If
                                    If vintPax = 0 Then
                                        dblCostoRealTemp = drCostos("Monto") '/ vintPax
                                    Else
                                        dblCostoRealTemp = drCostos("Monto") / vintPax
                                    End If

                                    If vintLiberados > 0 Then dblCostoAbsolutoTemp = drCostos("Monto")
                                Next
                            End If
                            If dblCostoReal = 0 And (intPaxHastaMax >= 40 And (vintPax + vintLiberados) >= 40) Then
                                dblCostoReal = dblCostoRealTemp
                                dblCostoAbsoluto = dblCostoAbsolutoTemp

                                If vblnAcomVehic Then
                                    ''''
                                    dblCostoReal = dblCostoFueradeRango(dttCostos, intPaxHastaMax, vintPax)
                                    If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal
                                    If vintPax <> 0 Then
                                        dblCostoReal = dblCostoReal / vintPax
                                    End If
                                    ''''
                                End If
                            End If


                            If vintLiberados > 0 Then
                                Dim dblCostoAbsolutoLibTemp As Double
                                For Each drCostos As DataRow In dttCostos.Rows
                                    If (vintPax + vintLiberados) >= drCostos("PaxDesde") And (vintPax + vintLiberados) <= drCostos("PaxHasta") Then

                                        dblCostoAbsolutoLib = drCostos("Monto")
                                        Exit For
                                    End If
                                    dblCostoAbsolutoLibTemp = drCostos("Monto")
                                Next
                                If dblCostoAbsolutoLib = 0 And intPaxHastaMax >= 40 Then
                                    dblCostoAbsolutoLib = dblCostoAbsolutoLibTemp
                                    If vblnAcomVehic Then
                                        ''''
                                        dblCostoAbsolutoLib = dblCostoFueradeRango(dttCostos, intPaxHastaMax, (vintPax + vintLiberados))
                                        ''''
                                    End If
                                End If
                            End If

                            Dim dblCostoSimpleTemp As Double
                            For Each drCostos As DataRow In dttCostos.Rows
                                If (vintPax + intPaxLiberadosFaltantes) >= drCostos("PaxDesde") And (vintPax + intPaxLiberadosFaltantes) <= drCostos("PaxHasta") Then
                                    dblCostoSimple = drCostos("Monto")
                                    Exit For
                                End If
                                dblCostoSimpleTemp = drCostos("Monto")
                            Next
                            If dblCostoSimple = 0 And intPaxHastaMax >= 40 Then
                                dblCostoSimple = dblCostoSimpleTemp
                            End If

                            blnPorRango = True
                        End If
                    Else
                        dblCostoReal = vdblCostoReal
                    End If

                    'dblMargen = dblCostoReal * (dblMargenAplicado / 100)
                    dblMargen = 0



                    If Not blnLiberadosPermitidos Then
                        'If intPaxLiberados > 0 Then
                        If dblMontoL > 0 Then
                            'dblCostoLiberado = (dblMontoL / vintPax) * intPaxLiberados
                            'dblCostoLiberado = (dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax
                            dblCostoLiberado = (dblCostoAbsolutoLib) / (vintPax + vintLiberados)
                            dblCostoLiberado = (dblCostoLiberado / vintPax) * vintLiberados

                        Else
                            ''dblCostoLiberado = (dblCostoSimple / vintPax) * intPaxLiberados
                            ''If dblCostoLiberado > dblCostoReal Then
                            ''    dblCostoLiberado = dblCostoLiberado - dblCostoReal
                            ''End If
                            dblCostoLiberado = (dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax
                            If dblCostoAbsoluto = 0 Then
                                dblCostoLiberado = ((dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax) * vintLiberados
                            End If
                        End If
                        'End If
                    Else
                        dblCostoLiberado = 0
                    End If

                    'If vstrTipo_Lib = "S" And Not blnLiberadosPermitidos And vintPax >= intPaxLiberadosDetServ Then
                    'If vstrTipo_Lib = "D" And Not blnLiberadosPermitidos And vintPax >= intPaxLiberadosDetServ Then
                    If vstrTipo_Lib = "D" And Not blnLiberadosPermitidos And vintPax >= intPaxLiberadosDetServ And blnPorRango = False Then
                        Dim dblDiferSS As Double = If(IsDBNull(drDetServ("DiferSS")) = True, 0, drDetServ("DiferSS"))
                        dblCostoLiberado = (dblCostoReal + dblDiferSS) / vintPax
                        dblCostoLiberado = dblCostoLiberado * intPaxLiberados ' vintLiberados
                    End If
                    'If intPaxLiberadosFaltantes > 0 Then
                    '    dblCostoLiberado = dblCostoLiberado + ((dblCostoSimple / vintPax) * (intPaxLiberadosFaltantes - 1))
                    'End If
                    If intPaxLiberados > 0 Then
                        dblCostoLiberado += (dblMontoL / vintPax)
                    End If


                    Dim blnIGV As Boolean = False
                    Dim blnProrrata As Boolean = False
                    Dim sglPorcProrrata As Single = 0
                    'If vstrTipoProv <> "002" Then
                    If drDetServ("OperacContable") = gstrTipoOperContExportacion Then
                        If vstrTipoPax <> "EXT" Then 'si es peruano
                            If drDetServ("Afecto") = True Then
                                blnIGV = True
                            End If
                        End If
                    ElseIf drDetServ("OperacContable") = gstrTipoOperContGravado Then
                        blnIGV = True
                    ElseIf drDetServ("OperacContable") = gstrTipoOperContProrrata Then 'OperacContable
                        blnIGV = True
                        blnProrrata = True
                        Dim objProrr As New clsTablasApoyoBN.clsParametroBN.clsProrrataBN
                        sglPorcProrrata = 1 - (objProrr.sglProrrataxFecha(vdatFechaServicio) / 100)
                    End If
                    'End If


                    If vstrTipoPax <> "EXT" Or vblnIncGuia Then 'si es peruano
                        blnIGV = True
                    End If
                    'dblMargenLiberado = dblCostoLiberado * (dblMargenAplicado / 100)
                    dblMargenLiberado = (dblCostoLiberado / (1 - (dblMargenAplicado / 100))) - dblCostoLiberado
                    If blnIGV Then
                        dblMargenLiberado = ((dblCostoLiberado * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblCostoLiberado * (1 + sglIGV))
                    End If


                    'If drDetServ("Afecto") = True Then Or vstrTipoPax <> "EXT" Then
                    If blnIGV Then
                        dblCostoRealImpto = dblCostoReal * sglIGV
                        dblCostoLiberadoImpto = dblCostoLiberado * sglIGV
                        dblMargenImpto = dblMargen * sglIGV
                        dblMargenLiberadoImpto = dblMargenLiberado * sglIGV
                        dblSSCRImpto = dblSSCR * sglIGV
                        dblSSCLImpto = dblSSCL * sglIGV
                        dblSSMargenImpto = dblSSMargen * sglIGV
                        dblSSMargenLiberadoImpto = dblSSML * sglIGV
                        dblSTCRImpto = dblSTCR * sglIGV
                        dblSTMargenImpto = dblSTMargen * sglIGV

                    Else
                        dblCostoRealImpto = 0
                        dblCostoLiberadoImpto = 0
                        dblMargenImpto = 0
                        dblMargenLiberadoImpto = 0
                        dblSSCRImpto = 0
                        dblSSCLImpto = 0
                        dblSSMargenImpto = 0
                        dblSSMargenLiberadoImpto = 0
                        dblSTCRImpto = 0
                        dblSTMargenImpto = 0
                    End If


                    If vsglIGV > 0 Or vblnServicioTC Then
                        'If drDetServ("Afecto") = True Then Or vstrTipoPax <> "EXT" Then
                        If blnIGV Then
                            dblTotImpto = (dblCostoReal * sglIGV) + _
                                                    (dblCostoLiberado * sglIGV) + _
                                                    (dblMargen * sglIGV) + _
                                                    (dblMargenLiberado * sglIGV) ' + _
                            '(dblSSCR * sglIGV) + _
                            '(dblSSCL * sglIGV) + _
                            '(dblSSMargen * sglIGV) + _
                            '(dblSSML * sglIGV) + _
                            '(dblSTCR * sglIGV) + _
                            '(dblSTMargen * sglIGV)
                        Else
                            dblTotImpto = 0
                        End If

                        If blnProrrata Then
                            dblTotImpto = ((dblCostoReal * sglIGV) * sglPorcProrrata)
                        End If

                        dblTotal = dblCostoReal '+ dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto
                        'dblMargen = dblTotal * (dblMargenAplicado / 100)
                        dblMargen = (dblTotal / (1 - (dblMargenAplicado / 100))) - dblTotal
                        If blnIGV Then
                            dblMargen = ((dblTotal * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblTotal * (1 + sglIGV))
                        End If

                        'dblTotal = dblTotal + dblMargen
                        dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                        If blnProrrata Then
                            Dim dblTotal2 As Decimal = (dblCostoReal + dblTotImpto) / (1 - (dblMargenAplicado / 100))
                            dblMargen = dblRedondeoInmedSuperior(dblTotal2, vsglRedondeo) * (dblMargenAplicado / 100)
                            'dblMargen = dblTotal2 * (dblMargenAplicado / 100)
                            dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto
                        End If

                        If dblTCambio = 0 Then
                            dblTotal = dblRedondeoInmedSuperior(dblTotal, vsglRedondeo, sglRedondeadoTotal)
                            sglRedondeo = sglRedondeadoTotal
                        End If


                        'Servicio con Alojamiento
                        dblSSCR = 0
                        dblSTCR = 0
                        dblSSMargen = 0
                        dblSTMargen = 0
                        If drDetServ("ConAlojamiento") = True Then


                            Dim dblDiferSS As Double = If(IsDBNull(drDetServ("DiferSS")) = True, 0, drDetServ("DiferSS"))
                            Dim dblDiferST As Double = If(IsDBNull(drDetServ("DiferST")) = True, 0, drDetServ("DiferST"))

                            'dblSSCR = dblTotal + dblDiferSS
                            If vdblSSCR = -1 Then
                                dblSSCR = dblDiferSS
                            Else
                                dblSSCR = vdblSSCR
                            End If

                            'dblSTCR = dblTotal - dblDiferST
                            If vdblSTCR = -1 Then
                                dblSTCR = dblDiferST
                            Else
                                dblSTCR = vdblSTCR
                            End If

                            'HLF-20151119-I
                            If blnIGV Then
                                If Not blnProrrata Then
                                    dblSSCRImpto = dblSSCR * sglIGV
                                    dblSTCRImpto = dblSTCR * sglIGV
                                Else
                                    dblSSCRImpto = (dblSSCR * sglIGV) * sglPorcProrrata
                                    dblSTCRImpto = (dblSTCR * sglIGV) * sglPorcProrrata
                                End If
                            End If
                            'HLF-20151119-F

                            'dblSSMargen = dblDiferSS / (1 - (dblMargenAplicado / 100))
                            'dblSTMargen = dblDiferST / (1 - (dblMargenAplicado / 100))

                            'dblSSMargen = dblDiferSS * (dblMargenAplicado / 100)
                            ''PPMG-20151022-If vdblSSCR = 0 Then
                            If vdblSSCR = 0 Then
                                dblSSMargen = 0
                            Else
                                dblSSMargen = (dblDiferSS / (1 - (dblMargenAplicado / 100))) - dblDiferSS
                            End If
                            'dblSTMargen = dblDiferST * (dblMargenAplicado / 100)
                            ''PPMG-20151022-If vdblSTCR = 0 Then
                            If vdblSTCR = 0 Then
                                dblSTMargen = 0
                            Else
                                dblSTMargen = (dblDiferST / (1 - (dblMargenAplicado / 100))) - dblDiferST
                            End If
                            'HL-20150731-I
                            'dblCostoLiberado = ((dblCostoReal + dblDiferSS) / vintPax) * intPaxLiberadosFaltantes
                            If blnLiberadosPermitidos Then
                                If intPaxLiberadosFaltantes > 0 Then
                                    dblCostoLiberado = ((dblCostoReal + dblDiferSS) / vintPax) * intPaxLiberadosFaltantes
                                Else
                                    'dblCostoLiberado = ((dblCostoReal - dblMontoL) / vintPax)
                                    If (intPaxLiberados <> vintLiberados And dblMontoL = 0) Or dblMontoL > 0 Then
                                        dblCostoLiberado = ((dblCostoReal - dblMontoL) / vintPax)
                                    End If
                                End If
                            End If
                            'dblCostoLiberado = 0 'Cambio Temporal

                            ''If intPaxLiberadosFaltantes > 0 Then
                            ''    dblCostoLiberado = ((dblCostoReal + dblDiferSS) / vintPax) * intPaxLiberadosFaltantes
                            ''End If
                            'HL-20150731-F



                            'Recalculando Total - I

                            dblMargenLiberado = (dblCostoLiberado / (1 - (dblMargenAplicado / 100))) - dblCostoLiberado
                            If blnIGV Then
                                dblMargenLiberado = ((dblCostoLiberado * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblCostoLiberado * (1 + sglIGV))
                            End If

                            If blnIGV Then
                                dblTotImpto = (dblCostoReal * sglIGV) + _
                                        (dblCostoLiberado * sglIGV) + _
                                        (dblMargen * sglIGV) + _
                                        (dblMargenLiberado * sglIGV)
                            Else
                                dblTotImpto = 0

                            End If

                            If blnProrrata Then
                                dblTotImpto = ((dblCostoReal * sglIGV) * sglPorcProrrata)
                            End If

                            dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                            If blnProrrata Then
                                Dim dblTotal2 As Decimal = (dblCostoReal + dblTotImpto) / (1 - (dblMargenAplicado / 100))
                                dblMargen = dblRedondeoInmedSuperior(dblTotal2, vsglRedondeo) * (dblMargenAplicado / 100)
                                'dblMargen = dblTotal2 * (dblMargenAplicado / 100)
                                dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto
                            End If

                            If dblTCambio = 0 Then
                                dblTotal = dblRedondeoInmedSuperior(dblTotal, vsglRedondeo, sglRedondeadoTotal)
                                sglRedondeo = sglRedondeadoTotal
                            End If
                            'Recalculando Total - F

                        End If

                        dblSSTotal = dblSSCR + dblSSMargen + dblSSCL + dblSSML
                        dblSTTotal = dblSTCR + dblSTMargen

                        If dblTCambio <> 0 Then


                            If vdblCostoReal = -1 Then dblCostoReal = dblCostoReal / dblTCambio
                            If vdblCostoReal = -1 Then
                                dblTotal = dblRedondeoInmedSuperior(dblTotal / Convert.ToDecimal(dblTCambio), vsglRedondeo, sglRedondeadoTotal)
                            Else
                                dblTotal = dblRedondeoInmedSuperior(dblTotal, vsglRedondeo, sglRedondeadoTotal)
                            End If

                            If vdblCostoReal = -1 Then sglRedondeo = sglRedondeadoTotal

                            If vdblCostoReal = -1 Then dblMargen = dblMargen / dblTCambio
                            dblTotImpto = dblTotImpto / dblTCambio

                            dblCostoLiberado = dblCostoLiberado / dblTCambio
                            dblMargenLiberado = dblMargenLiberado / dblTCambio

                            dblSSCR = dblSSCR / dblTCambio
                            dblSSMargen = dblSSMargen / dblTCambio
                            dblSSCL = dblSSCL / dblTCambio
                            dblSSML = dblSSML / dblTCambio
                            dblSSTotal = dblSSTotal / dblTCambio

                            dblSTCR = dblSTCR / dblTCambio
                            dblSTMargen = dblSTMargen / dblTCambio
                            dblSTTotal = dblSTTotal / dblTCambio

                            dblCostoRealImpto = dblCostoRealImpto / dblTCambio
                            dblCostoLiberadoImpto = dblCostoLiberadoImpto / dblTCambio
                            dblMargenImpto = dblMargenImpto / dblTCambio
                            dblMargenLiberadoImpto = dblMargenLiberadoImpto / dblTCambio
                            dblSSCRImpto = dblSSCRImpto / dblTCambio
                            dblSSCLImpto = dblSSCLImpto / dblTCambio
                            dblSSMargenImpto = dblSSMargenImpto / dblTCambio
                            dblSSMargenLiberadoImpto = dblSSMargenLiberadoImpto / dblTCambio
                            dblSTCRImpto = dblSTCRImpto / dblTCambio
                            dblSTMargenImpto = dblSTMargenImpto / dblTCambio

                        End If


                        If Not rdttDetCotiServ Is Nothing Then

                            'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                            If blnIGV Then
                                rdttDetCotiServ.Rows.Add(vstrIDDet_DetCotiServ, vstrIDServicio, drDetServ("IDServicio_Det"), dblCostoReal, _
                                                         dblMargen + dblMargenLiberado, dblCostoLiberado, _
                                                        dblMargenLiberado, dblMargenAplicado, _
                                                        dblTotal, sglRedondeo, dblSSCR, dblSSMargen, dblSSCL, dblSSML, _
                                                        dblSSTotal, dblSTCR, dblSTMargen, dblSTTotal, _
                                                        (dblCostoReal * sglIGV), _
                                                        (dblCostoLiberado * sglIGV), _
                                                        (dblMargen * sglIGV), _
                                                        (dblMargenLiberado * sglIGV), _
                                                        (dblSSCR * sglIGV), _
                                                        (dblSSCL * sglIGV), _
                                                        (dblSSMargen * sglIGV), _
                                                        (dblSSML * sglIGV), _
                                                        (dblSTCR * sglIGV), _
                                                        (dblSTMargen * sglIGV), _
                                                        dblTotImpto)
                            Else
                                rdttDetCotiServ.Rows.Add(vstrIDDet_DetCotiServ, vstrIDServicio, drDetServ("IDServicio_Det"), dblCostoReal, _
                                                         dblMargen + dblMargenLiberado, dblCostoLiberado, _
                                                        dblMargenLiberado, dblMargenAplicado, _
                                                        dblTotal, sglRedondeo, dblSSCR, dblSSMargen, dblSSCL, dblSSML, _
                                                        dblSSTotal, dblSTCR, dblSTMargen, dblSTTotal, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        dblTotImpto)

                            End If
                        End If
                    End If





                    '''''


                    'Acumula
                    If vdblCostoReal = -1 Then
                        dblAcCostoReal += dblCostoReal
                        dblAcTotal += dblTotal
                        sglAcRedondeo += sglRedondeo
                        dblAcMargen += (dblMargen + dblMargenLiberado)
                    Else

                        dblAcCostoReal = dblCostoReal
                        dblAcTotal = dblTotal
                        sglAcRedondeo = sglRedondeo
                        dblAcMargen = dblMargen

                    End If

                    dblAcTotImpto += dblTotImpto

                    dblAcCostoLiberado += dblCostoLiberado
                    dblAcMargenLiberado += dblMargenLiberado
                    dblAcMargenAplicado = dblMargenAplicado


                    dblAcSSCR += dblSSCR
                    dblAcSSMargen += dblSSMargen
                    dblAcSSCL += dblSSCL
                    dblAcSSML += dblSSML
                    dblAcSSTotal += dblSSTotal

                    dblAcSTCR += dblSTCR
                    dblAcSTMargen += dblSTMargen
                    dblAcSTTotal += dblSTTotal

                    dblAcCostoRealImpto += dblCostoRealImpto
                    dblAcCostoLiberadoImpto += dblCostoLiberadoImpto
                    dblAcMargenImpto += dblMargenImpto
                    dblAcMargenLiberadoImpto += dblMargenLiberadoImpto
                    dblAcSSCRImpto += dblSSCRImpto
                    dblAcSSCLImpto += dblSSCLImpto
                    dblAcSSMargenImpto += dblSSMargenImpto
                    dblAcSSMargenLiberadoImpto += dblSSMargenLiberadoImpto
                    dblAcSTCRImpto += dblSTCRImpto
                    dblAcSTMargenImpto += dblSTMargenImpto

SgteDet:
                End While
                drDetServ.Close()

                'If dblTCambio = 0 Then
                dc.Add("CostoReal", dblAcCostoReal)
                dc.Add("Margen", dblAcMargen) ' + dblAcMargenLiberado)
                dc.Add("CostoLiberado", dblAcCostoLiberado)
                dc.Add("MargenLiberado", dblAcMargenLiberado)
                dc.Add("MargenAplicado", dblAcMargenAplicado)
                dc.Add("TotImpto", dblAcTotImpto)
                dc.Add("TotalOrig", dblAcTotal)
                dc.Add("Total", dblAcTotal) 'dblRedondeoInmedSuperior(Math.Round(dblAcTotal, 2), vsglRedondeo, sglRedondeadoTotal))
                dc.Add("RedondeoTotal", sglAcRedondeo) ' sglAcRedondeo  + sglRedondeadoTotal)

                dc.Add("SSCR", dblAcSSCR)
                dc.Add("SSMargen", dblRedondeoInmedSuperior(dblAcSSMargen, vsglRedondeo))
                dc.Add("SSCL", dblAcSSCL)
                dc.Add("SSML", dblAcSSML)
                dc.Add("SSTotalOrig", dblAcSSTotal)
                dc.Add("SSTotal", dblRedondeoInmedSuperior(dblAcSSTotal, vsglRedondeo))

                dc.Add("STCR", dblAcSTCR)
                dc.Add("STMargen", dblRedondeoInmedSuperior(dblAcSTMargen, vsglRedondeo))

                dc.Add("STTotalOrig", dblAcSTTotal)
                dc.Add("STTotal", dblRedondeoInmedSuperior(dblAcSTTotal, vsglRedondeo, , True))

                dc.Add("CostoRealImpto", dblAcCostoRealImpto)
                dc.Add("CostoLiberadoImpto", dblAcCostoLiberadoImpto)
                dc.Add("MargenImpto", dblAcMargen * sglIGV)

                dc.Add("MargenLiberadoImpto", dblAcMargenLiberadoImpto)
                dc.Add("SSCRImpto", dblAcSSCRImpto)
                dc.Add("SSCLImpto", dblAcSSCLImpto)
                dc.Add("SSMargenImpto", dblAcSSMargenImpto)
                dc.Add("SSMargenLiberadoImpto", dblAcSSMargenLiberadoImpto)
                dc.Add("STCRImpto", dblAcSTCRImpto)
                dc.Add("STMargenImpto", dblAcSTMargenImpto)
                '    Else

                '    dc.Add("CostoReal", If(vdblCostoReal = 0, dblAcCostoReal / dblTCambio, vdblCostoReal))
                '    dc.Add("Margen", dblAcMargen / dblTCambio)
                '    dc.Add("CostoLiberado", dblAcCostoLiberado / dblTCambio)
                '    dc.Add("MargenLiberado", dblAcMargenLiberado / dblTCambio)
                '    dc.Add("MargenAplicado", dblAcMargenAplicado)
                '    dc.Add("TotImpto", dblAcTotImpto / dblTCambio)
                '    dc.Add("TotalOrig", dblAcTotal)
                '    dc.Add("Total", dblRedondeoInmedSuperior(Math.Round(dblAcTotal / dblTCambio, 2), vsglRedondeo, sglRedondeadoTotal))
                '    dc.Add("RedondeoTotal", (sglAcRedondeo / dblTCambio) + sglRedondeadoTotal)

                '    dc.Add("SSCR", dblAcSSCR / dblTCambio)
                '    dc.Add("SSMargen", dblAcSSMargen / dblTCambio)
                '    dc.Add("SSCL", dblAcSSCL / dblTCambio)
                '    dc.Add("SSML", dblAcSSML / dblTCambio)
                '    dc.Add("SSTotalOrig", dblAcSSTotal)
                '    dc.Add("SSTotal", dblRedondeoInmedSuperior(dblAcSSTotal / dblTCambio, vsglRedondeo))

                '    dc.Add("STCR", dblAcSTCR / dblTCambio)
                '    dc.Add("STMargen", dblAcSTMargen / dblTCambio)
                '    dc.Add("STTotalOrig", dblAcSTTotal)
                '    dc.Add("STTotal", dblRedondeoInmedSuperior(dblAcSTTotal / dblTCambio, vsglRedondeo))

                '    dc.Add("CostoRealImpto", dblAcCostoRealImpto / dblTCambio)
                '    dc.Add("CostoLiberadoImpto", dblAcCostoLiberadoImpto / dblTCambio)
                '    dc.Add("MargenImpto", (dblAcMargen * sglIGV) / dblTCambio)

                '    dc.Add("MargenLiberadoImpto", dblAcMargenLiberadoImpto / dblTCambio)
                '    dc.Add("SSCRImpto", dblAcSSCRImpto / dblTCambio)
                '    dc.Add("SSCLImpto", dblAcSSCLImpto / dblTCambio)
                '    dc.Add("SSMargenImpto", dblAcSSMargenImpto / dblTCambio)
                '    dc.Add("SSMargenLiberadoImpto", dblAcSSMargenLiberadoImpto / dblTCambio)
                '    dc.Add("STCRImpto", dblAcSTCRImpto / dblTCambio)
                '    dc.Add("STMargenImpto", dblAcSTMargenImpto / dblTCambio)


                'End If
            End If




            Return dc

        Catch ex As Exception
            Throw
        End Try

    End Function
    Public Function dblCostoFueradeRango(ByVal vdttCostos As DataTable, ByVal vintPaxHastaMax As Int16, ByVal vintPax As Int16) As Double
        Try
            Dim dblMontoAnt As Double = 0
            Dim dblDifCostos As Double = 0
            Dim intTamanio As Int16 = 0
            Dim dblMontoMax As Double = 0
            For Each drCostos As DataRow In vdttCostos.Rows
                intTamanio = drCostos("PaxHasta") - drCostos("PaxDesde")
                If drCostos("PaxHasta") = vintPaxHastaMax Then
                    dblDifCostos = drCostos("Monto") - dblMontoAnt
                    dblMontoMax = drCostos("Monto")
                    Exit For
                End If
                dblMontoAnt = drCostos("Monto")
            Next
            If intTamanio > 0 Or dblMontoMax > 0 Then
                intTamanio += 1
                If dblDifCostos <> dblMontoMax Then
                    For intInd As Int16 = vintPaxHastaMax + 1 To vintPax - intTamanio Step intTamanio
                        dblMontoMax += dblDifCostos
                    Next
                Else 'sólo hay un costo en tabla

                    dblMontoMax += dblDifCostos

                End If
            End If

            Return dblMontoMax
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveCotizacion(ByVal vintIDCab As Int32) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pCotizacion", "")
            Return objADO.ExecuteSPOutput("COTICAB_Sel_CotizacionOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCotizacionCliente(ByVal vintIDCAB As Integer, _
                                                  ByVal vintPaxInicial As Int16, _
                                                  ByVal vintPaxFinal As Int16, _
                                                  Optional ByVal vblnPasoRango As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            dc.Add("@PaxInic", vintPaxInicial)
            dc.Add("@PaxFin", vintPaxFinal)
            dc.Add("@blnSiguRango", vblnPasoRango)

            Return objADO.GetDataTable(True, "COTIDET_COTIZACION_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptWeeklyNetSales(ByVal vdatFecIni As Date, ByVal vdatFecFin As Date, _
                                               ByVal vchrSemanaMes As Char, ByVal vchrNroQuery As Char) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@FecIni", vdatFecIni.ToString("dd/MM/yyyy"))
            dc.Add("@FecFin", vdatFecFin.ToString("dd/MM/yyyy"))
            dc.Add("@SemanaMes", vchrSemanaMes)
            dc.Add("@NroQuery", vchrNroQuery)

            Return objADO.GetDataReader("ReporteWeeklyNetSales", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCotizacionCliente_RangoAPT(ByVal vintIDCAB As Integer, _
                                                ByVal vintIDTemporada As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            dc.Add("@IDTemporada", vintIDTemporada)

            Return objADO.GetDataTable(True, "COTIDET_COTIZACION_Montos_APT_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCotizacionCliente_RangoFOC(ByVal vintIDCAB As Integer, _
                                            ByVal vintIDTemporada As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            dc.Add("@IDTemporada", vintIDTemporada)

            Return objADO.GetDataTable(True, "COTIDET_COTIZACION_Montos_FOC_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCotizacionCliente_Rangos(ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCAB)
            'dc.Add("@Div", vintIDCAB)

            Return objADO.GetDataTable(True, "COTICAB_QUIEBRES_SelRangos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptTiposCambio(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "COTICAB_Sel_RptTCambios", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCostosCotizacion(ByVal vintIDCAB As Integer, _
                                                 ByVal vintPaxInicial As Int16, _
                                                 ByVal vintPaxFinal As Int16, _
                                                 Optional ByVal vblnPasoRango As Boolean = False) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            dc.Add("@PaxInic", vintPaxInicial)
            dc.Add("@PaxFin", vintPaxFinal)
            dc.Add("@blnSiguRango", vblnPasoRango)

            Return objADO.GetDataTable(True, "COTIDET_RptCostosCotizacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCostoCotizacionSGL_TPL(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCab)
            Return objADO.GetDataTable(True, "COTIDET_RptCostos_SGL_TPL_Cotizacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptDetalleCotizacion(ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            Return objADO.GetDataTable(True, "COTIDET_DETALLE_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCostosDetallexPax(ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            Return objADO.GetDataTable(True, "COTIDET_RptCostosDetalladoxPax", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCostosDetallexPaxRangos(ByVal vintIDCAB As Integer, ByVal vintPaxIni As Int16, ByVal vintPaxFin As Int16) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            dc.Add("@PaxIni", vintPaxIni)
            dc.Add("@PaxFin", vintPaxFin)
            Return objADO.GetDataTable(True, "COTIDET_RptCostosDetalladoxPaxRangos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function consultarRptPax(ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCAB)
            Return objADO.GetDataTable(True, "COTIPAX_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptPeruRAIL(ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IdCab", vintIDCAB)
            Return objADO.GetDataTable(True, "COTIPAX_Sel_RptPeruRAIL", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptINC(ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCAB)
            Return objADO.GetDataTable(True, "COTIPAX_Sel_RptINC", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptPeruRAILExcel(ByVal vintIDCab As Integer) As Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "COTIPAX_Sel_RptINC_Excel", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDatosDocWord(ByVal vintIDCAB As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCAB)
            Return objADO.GetDataReader("COTICAB_SelDatosDocWord", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptPax_Serv_Detallado(ByVal vintIDCAB As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCAB)
            Return objADO.GetDataTable(True, "COTIPAX_Sel_RptDetalleServicio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptTapa(ByVal vintIDCab As Integer, ByVal vstrIDUbigeo As String, _
                                     ByVal vdatRangoFecIni As Date, ByVal vdatRangoFecFin As Date, _
                                     ByVal vblnConGastosTransferencia As Boolean, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDUbigeo", vstrIDUbigeo)
                .Add("@RangoFecIni", vdatRangoFecIni)
                .Add("@RangoFecFin", vdatRangoFecFin)
                .Add("@ConGastosTransferencias", vblnConGastosTransferencia)
                .Add("@CoPais", vstrCoPais)
            End With

            Return objADO.GetDataTable(True, "ReporteTapa", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptTapaVuelos(ByVal vintIDCAB As Integer, ByVal vstrIDUbigeo As String, _
                                           ByVal vdatRangoFecIni As Date, ByVal vdatRangoFecFin As Date, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCAB)
                .Add("@IDUbigeo", vstrIDUbigeo)
                .Add("@RangoFecIni", vdatRangoFecIni)
                .Add("@RangoFecFin", vdatRangoFecFin)
                .Add("@CoPais", vstrCoPais)
            End With

            Return objADO.GetDataTable(True, "ReporteTapa_Vuelos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptBiblia(ByVal vstrCotizacion As String, ByVal vintIDCAB As Integer, ByVal vstrIDGuiaProg As String, _
                                       ByVal vstrIDProveedor As String, _
                                       ByVal vstrIDTransProg As String, ByVal vstrIDUsuarioOpe As String, ByVal vstrIDIdioma As String, _
                                       ByVal vstrIDUbigeo As String, ByVal vdatFecInicial As Date, ByVal vdatFecFinal As Date, Optional ByVal vstrCoPais As String = "") As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@Cotizacion", vstrCotizacion)
                .Add("@IDCab", vintIDCAB)
                .Add("@IDGuiaProg", vstrIDGuiaProg)
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDTransProg", vstrIDTransProg)
                .Add("@IDUsuarioOpe", vstrIDUsuarioOpe)
                .Add("@IDIdioma", vstrIDIdioma)
                .Add("@IDUbigeo", vstrIDUbigeo)
                .Add("@FechaIni", vdatFecInicial)
                .Add("@FechaFin", vdatFecFinal)
                .Add("@CoPais", vstrCoPais)
            End With

            Return objADO.GetDataTable(True, "ReporteBiblia", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCotizacionGeneral(ByVal vstrIDUsuariosResp As String, ByVal vstrIDUsuarioResv As String, _
                                                  ByVal vstrIDUsuariosOper As String, ByVal vstrIDCliente As String, _
                                                  ByVal vstrIDFile As String, ByVal vstrEstado As String, _
                                                  ByVal vdatFechaGenInicial As Date, ByVal vdatFechaGenFinal As Date, _
                                                  ByVal vdatFechaInInicial As Date, ByVal vdatFechaInFinal As Date, _
                                                  ByVal vdatFechaOutInicial As Date, ByVal vdatFechaOutFinal As Date, _
                                                  ByVal vdatFechaAsgReservaInicial As Date, ByVal vdatFechaAsgReservaFinal As Date, _
                                                  ByVal vdatFechaGenerFileInicial As Date, ByVal vdatFechaGenerFileFinal As Date, _
                                                  ByVal vblnNoExisteInfoOperacion As Boolean, ByVal vstrOrd As String, _
                                                  ByVal vblnFilesHistoricos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUsuarioResp", vstrIDUsuariosResp)
                .Add("@IDUsuarioResv", vstrIDUsuarioResv)
                .Add("@IDUsuarioOper", vstrIDUsuariosOper)
                .Add("@IDCliente", vstrIDCliente)
                .Add("@NroFile", vstrIDFile)
                .Add("@Estado", vstrEstado)
                .Add("@FechaGenInicial", vdatFechaGenInicial)
                .Add("@FechaGenFinal", vdatFechaGenFinal)
                .Add("@FechaIngInicial", vdatFechaInInicial)
                .Add("@FechaIngFinal", vdatFechaInFinal)
                .Add("@FechaOutInicial", vdatFechaOutInicial)
                .Add("@FechaOutFinal", vdatFechaOutFinal)
                .Add("@FechaAsgReservaInicial", vdatFechaAsgReservaInicial)
                .Add("@FechaAsgReservaFinal", vdatFechaAsgReservaFinal)
                .Add("@FechaGenReservasInicial", vdatFechaGenerFileInicial)
                .Add("@FechaGenReservasFinal", vdatFechaGenerFileFinal)
                .Add("@NoExisteInfoOperacion", vblnNoExisteInfoOperacion)
                .Add("@Orderx", vstrOrd)
                .Add("@FlHistorico", vblnFilesHistoricos)

            End With
            Return objADO.GetDataTable(True, "COTICAB_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptCotizacionGeneral_Excel(ByVal vstrIDUsuariosResp As String, ByVal vstrIDUsuarioResv As String, _
                                                  ByVal vstrIDUsuariosOper As String, ByVal vstrIDCliente As String, _
                                                  ByVal vstrIDFile As String, ByVal vstrEstado As String, _
                                                  ByVal vdatFechaGenInicial As Date, ByVal vdatFechaGenFinal As Date, _
                                                  ByVal vdatFechaInInicial As Date, ByVal vdatFechaInFinal As Date, _
                                                  ByVal vdatFechaOutInicial As Date, ByVal vdatFechaOutFinal As Date, _
                                                  ByVal vdatFechaAsgReservaInicial As Date, ByVal vdatFechaAsgReservaFinal As Date, _
                                                  ByVal vdatFechaGenerFileInicial As Date, ByVal vdatFechaGenerFileFinal As Date, _
                                                  ByVal vblnNoExisteInfoOperacion As Boolean, ByVal vstrOrd As String, _
                                                  ByVal vblnFilesHistoricos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUsuarioResp", vstrIDUsuariosResp)
                .Add("@IDUsuarioResv", vstrIDUsuarioResv)
                .Add("@IDUsuarioOper", vstrIDUsuariosOper)
                .Add("@IDCliente", vstrIDCliente)
                .Add("@NroFile", vstrIDFile)
                .Add("@Estado", vstrEstado)
                .Add("@FechaGenInicial", vdatFechaGenInicial)
                .Add("@FechaGenFinal", vdatFechaGenFinal)
                .Add("@FechaIngInicial", vdatFechaInInicial)
                .Add("@FechaIngFinal", vdatFechaInFinal)
                .Add("@FechaOutInicial", vdatFechaOutInicial)
                .Add("@FechaOutFinal", vdatFechaOutFinal)
                .Add("@FechaAsgReservaInicial", vdatFechaAsgReservaInicial)
                .Add("@FechaAsgReservaFinal", vdatFechaAsgReservaFinal)
                .Add("@FechaGenReservasInicial", vdatFechaGenerFileInicial)
                .Add("@FechaGenReservasFinal", vdatFechaGenerFileFinal)
                .Add("@NoExisteInfoOperacion", vblnNoExisteInfoOperacion)
                .Add("@Orderx", vstrOrd)
                .Add("@FlHistorico", vblnFilesHistoricos)
            End With
            Return objADO.GetDataTable(True, "COTICAB_Sel_Rpt_Excel", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptPagos_Finanzas(ByVal vstrIDProveedor As String, ByVal vstrIDUsuarioVentas As String, _
                                               ByVal vstrIDUsuarioReserva As String, ByVal vstrIDUsuarioOperaciones As String, _
                                               ByVal vstrIDFile As String, ByVal vstrIDFormaPago As String, _
                                               ByVal vdatPagoDesde As Date, ByVal vdatPagoHasta As Date, _
                                               ByVal vdatAsigReservaDesde As Date, ByVal vdatAsigReservaHasta As Date, _
                                               ByVal vdatAceptadoDesde As Date, ByVal vdatAceptadoHasta As Date, _
                                               ByVal vdatIniServDesde As Date, ByVal vdatIniServHasta As Date, _
                                               ByVal vdatOutServDesde As Date, ByVal vdatOutServHasta As Date, _
                                               ByVal vdatCotiDesde As Date, ByVal vdatCotiHasta As Date, _
                                               ByVal vstrCoUbigeoOficina As String, ByVal vblnEntradasMuseos As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDProveedor", vstrIDProveedor)
                .Add("@IDUsuarioResp", vstrIDUsuarioVentas)
                .Add("@IDUsuarioResv", vstrIDUsuarioReserva)
                .Add("@IDUsuarioOper", vstrIDUsuarioOperaciones)
                .Add("@IDFile", vstrIDFile)
                .Add("@IDFormaPago", vstrIDFormaPago)
                .Add("@FePagoDesde", vdatPagoDesde)
                .Add("@FePagoHasta", vdatPagoHasta)
                .Add("@FeAsigResDesde", vdatAsigReservaDesde)
                .Add("@FeAsigResHasta", vdatAsigReservaHasta)
                .Add("@FeAceptDesde", vdatAceptadoDesde)
                .Add("@FeAceptHasta", vdatAceptadoHasta)
                .Add("@FeInServDesde", vdatIniServDesde)
                .Add("@FeInServHasta", vdatIniServHasta)
                .Add("@FeOutDesde", vdatOutServDesde)
                .Add("@FeOutHasta", vdatOutServHasta)
                .Add("@FeCotizDesde", vdatCotiDesde)
                .Add("@FeCotizHasta", vdatCotiHasta)
                .Add("@CoOficinaFact", vstrCoUbigeoOficina)
                .Add("@FlEntradasMuseos", vblnEntradasMuseos)
            End With
            Return objADO.GetDataTable(True, "RESERVAS_Sel_Rpt_PresupuestosGastos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptPagos_Reservas(ByVal vstrIDUsuariosResp As String, ByVal vstrIDUsuarioResv As String, _
                                               ByVal vstrIDUsuariosOper As String, _
                                               ByVal vstrIDFile As String, ByVal vstrEstado As String, _
                                               ByVal vdatFechaGenInicial As Date, ByVal vdatFechaGenFinal As Date, _
                                               ByVal vdatFechaInInicial As Date, ByVal vdatFechaInFinal As Date, _
                                               ByVal vdatFechaOutInicial As Date, ByVal vdatFechaOutFinal As Date, _
                                               ByVal vdatFechaAsgReservaInicial As Date, ByVal vdatFechaAsgReservaFinal As Date, _
                                               ByVal vdatFechaGenerFileInicial As Date, ByVal vdatFechaGenerFileFinal As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUsuarioResp", vstrIDUsuariosResp)
                .Add("@IDUsuarioResv", vstrIDUsuarioResv)
                .Add("@IDUsuarioOper", vstrIDUsuariosOper)
                '.Add("@IDCliente", vstrIDCliente)
                .Add("@NroFile", vstrIDFile)
                .Add("@Estado", vstrEstado)
                .Add("@FechaGenInicial", vdatFechaGenInicial)
                .Add("@FechaGenFinal", vdatFechaGenFinal)
                .Add("@FechaIngInicial", vdatFechaInInicial)
                .Add("@FechaIngFinal", vdatFechaInFinal)
                .Add("@FechaOutInicial", vdatFechaOutInicial)
                .Add("@FechaOutFinal", vdatFechaOutFinal)
                .Add("@FechaAsgReservaInicial", vdatFechaAsgReservaInicial)
                .Add("@FechaAsgReservaFinal", vdatFechaAsgReservaFinal)
                .Add("@FechaGenReservasInicial", vdatFechaGenerFileInicial)
                .Add("@FechaGenReservasFinal", vdatFechaGenerFileFinal)
                '.Add("@NoExisteInfoOperacion", vblnNoExisteInfoOperacion)
                '.Add("@Orderx", vstrOrd)
            End With
            Return objADO.GetDataTable(True, "REPORTEPAGOS_RESERVAS", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnCompararFechaOut(ByVal vintIDCab As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pFecha", False)

            Return objADO.ExecuteSPOutput("COTICAB_CompararFechaOut", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveRutaCotizacion(ByVal vintIDCab As Int32) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pRuta", "")
            Return objADO.ExecuteSPOutput("COTIDET_Sel_RutasOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function strDevuelveRutaLargaCotizacion(ByVal vintIDCab As Int32) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pRuta", "")
            Return objADO.ExecuteSPOutput("COTIDET_Sel_RutasLargasOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarHotelesCotizacion(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "COTIDET_Sel_Hoteles", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function objBECotizacion(ByVal vintIDCab As Integer, ByVal vstrUserMod As String) As clsCotizacionBE
        Try
            Dim objBE As New clsCotizacionBE
            Dim drCoti As SqlClient.SqlDataReader = ConsultarPk(vintIDCab)
            drCoti.Read()
            If drCoti.HasRows Then
                With objBE
                    .IdCab = drCoti("IDCAB")
                    .Cotizacion = drCoti("Cotizacion")
                    .Estado = "A"
                    .IDCliente = drCoti("IDCliente")
                    .Titulo = drCoti("Titulo")
                    .IDFile = drCoti("IDFile")
                    .IDUsuario = drCoti("IDUsuario")
                    .IDUsuarioRes = drCoti("IDUsuarioRes")
                    .Simple = If(IsDBNull(drCoti("Simple")) = True, 0, drCoti("Simple"))
                    .Twin = If(IsDBNull(drCoti("Twin")) = True, 0, drCoti("Twin"))
                    .Matrimonial = If(IsDBNull(drCoti("Matrimonial")) = True, 0, drCoti("Matrimonial"))
                    .Triple = If(IsDBNull(drCoti("Triple")) = True, 0, drCoti("Triple"))
                    .SimpleResidente = If(IsDBNull(drCoti("SimpleResidente")) = True, 0, drCoti("SimpleResidente"))
                    .TwinResidente = If(IsDBNull(drCoti("TwinResidente")) = True, 0, drCoti("TwinResidente"))
                    .MatrimonialResidente = If(IsDBNull(drCoti("MatrimonialResidente")) = True, 0, drCoti("MatrimonialResidente"))
                    .TripleResidente = If(IsDBNull(drCoti("TripleResidente")) = True, 0, drCoti("TripleResidente"))
                    .UserMod = vstrUserMod
                End With
            End If
            drCoti.Close()

            Dim ListaDetCot As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Using objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarTodosxIDCab(vintIDCab)
                    Dim objBEDet As clsCotizacionBE.clsDetalleCotizacionBE
                    While dr.Read
                        objBEDet = New clsCotizacionBE.clsDetalleCotizacionBE
                        With objBEDet
                            .IDDet = dr("IDDet")
                            .IDCab = dr("IDCAB")
                            .Cotizacion = dr("Cotizacion")
                            .Item = dr("Item")
                            .Dia = dr("Dia")
                            .IDUbigeo = dr("IDUbigeo")
                            .IDProveedor = dr("IDProveedor")
                            .IDServicio = dr("IDServicio")
                            .IDServicio_Det = dr("IDServicio_Det")
                            .IDIdioma = dr("IDIdioma")
                            .Especial = dr("Especial")
                            .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")) = True, "", dr("MotivoEspecial"))
                            .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")) = True, "", dr("RutaDocSustento"))
                            .Desayuno = dr("Desayuno")
                            .Lonche = dr("Lonche")
                            .Almuerzo = dr("Almuerzo")
                            .Cena = dr("Cena")
                            .Transfer = dr("Transfer")
                            .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")) = True, "", dr("IDDetTransferOri"))
                            .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")) = True, "", dr("IDDetTransferDes"))
                            .TipoTransporte = If(IsDBNull(dr("TipoTransporte")) = True, "", dr("TipoTransporte"))
                            .IDUbigeoOri = dr("IDUbigeoOri")
                            .IDUbigeoDes = dr("IDUbigeoDes")
                            .NroPax = dr("NroPax")
                            .NroLiberados = If(IsDBNull(dr("NroLiberados")) = True, 0, dr("NroLiberados"))
                            .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")) = True, "", dr("Tipo_Lib"))
                            .CostoReal = If(IsDBNull(dr("CostoReal")) = True, 0, dr("CostoReal"))
                            .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")) = True, 0, dr("CostoRealAnt"))
                            .CostoLiberado = If(IsDBNull(dr("CostoLiberado")) = True, 0, dr("CostoLiberado"))
                            .Margen = If(IsDBNull(dr("Margen")) = True, 0, dr("Margen"))
                            .MargenAplicado = If(IsDBNull(dr("MargenAplicado")) = True, 0, dr("MargenAplicado"))
                            .MargenAplicadoAnt = 0
                            .MargenLiberado = If(IsDBNull(dr("MargenLiberado")) = True, 0, dr("MargenLiberado"))
                            .Total = If(IsDBNull(dr("Total")) = True, 0, dr("Total"))
                            .TotalOrig = If(IsDBNull(dr("TotalOrig")) = True, 0, dr("TotalOrig"))
                            .SSCR = dr("SSCR")
                            .SSCL = dr("SSCL")
                            .SSMargen = dr("SSMargen")
                            .SSMargenLiberado = dr("SSMargenLiberado")
                            .SSTotal = dr("SSTotal")
                            .SSTotalOrig = dr("SSTotalOrig")
                            .CostoRealImpto = dr("CostoRealImpto")
                            .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                            .MargenImpto = dr("MargenImpto")
                            .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                            .SSCRImpto = dr("SSCRImpto")
                            .SSCLImpto = dr("SSCLImpto")
                            .SSMargenImpto = dr("SSMargenImpto")
                            .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                            .STCRImpto = dr("STCRImpto")
                            .STCRImpto = dr("STCRImpto")
                            .STMargenImpto = dr("STMargenImpto")
                            .TotImpto = dr("TotImpto")
                            .IDDetRel = If(IsDBNull(dr("IDDetRel")) = True, 0, dr("IDDetRel"))
                            .Servicio = If(IsDBNull(dr("Servicio")) = True, "", dr("Servicio"))
                            .IDDetCopia = dr("IDDet")
                            .Recalcular = False
                            .PorReserva = False
                            .ExecTrigger = dr("ExecTrigger")
                            .IncGuia = dr("IncGuia")
                            .FueradeHorario = dr("FueradeHorario")
                            .CostoRealEditado = dr("CostoRealEditado")
                            .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                            .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                            .ServicioEditado = dr("ServicioEditado")
                            .IDTipoProv = dr("IDTipoProv")
                            .PlanAlimenticio = dr("PlanAlimenticio")
                            .ConAlojamiento = dr("ConAlojamiento")
                            .AcomodoEspecial = dr("AcomodoEspecial")
                            .UserMod = vstrUserMod
                        End With
                        ListaDetCot.Add(objBEDet)
                    End While
                    dr.Close()
                End Using
            End Using

            'Vuelos
            Dim ListaVuelos As New List(Of clsCotizacionBE.clsCotizacionVuelosBE)
            Dim objVuelosBN As New clsCotizacionBN.clsCotizacionVuelosBN
            Using drVuelo As SqlClient.SqlDataReader = objVuelosBN.ConsultarxIDCab(vintIDCab)
                'drVuelo.Read()
                Dim objVuelosBE As clsCotizacionBE.clsCotizacionVuelosBE
                While drVuelo.Read
                    objVuelosBE = New clsCotizacionBE.clsCotizacionVuelosBE
                    With objVuelosBE
                        .IdCab = drVuelo("IDCab")
                        .IDDet = drVuelo("Iddet")
                        .Cotizacion = If(IsDBNull(drVuelo("Cotizacion")) = True, "", drVuelo("Cotizacion")).ToString.Trim
                        .NombrePax = drVuelo("NombrePax")
                        .NumBoleto = If(IsDBNull(drVuelo("Num_Boleto")) = True, "", drVuelo("Num_Boleto")).ToString.Trim
                        .Ruta = drVuelo("Ruta")
                        .RutaD = drVuelo("RutaD")
                        .RutaO = drVuelo("RutaO")
                        .Vuelo = drVuelo("Vuelo")
                        .Servicio = If(IsDBNull(drVuelo("Vuelo")) = True, "", drVuelo("Vuelo"))
                        .FecEmision = If(IsDBNull(drVuelo("Fec_Emision")) = True, "01/01/1900", CDate(drVuelo("Fec_Emision")).ToShortDateString)
                        .FecDocumento = If(IsDBNull(drVuelo("Fec_Documento")) = True, "01/01/1900", CDate(drVuelo("Fec_Documento")).ToShortDateString)
                        .FecSalida = If(IsDBNull(drVuelo("Fec_Salida")) = True, "01/01/1900", CDate(drVuelo("Fec_Salida")).ToShortDateString)
                        .FecRetorno = If(IsDBNull(drVuelo("Fec_Retorno")) = True, "01/01/1900", CDate(drVuelo("Fec_Retorno")).ToShortDateString)
                        .Salida = drVuelo("Salida")
                        .HrRecojo = If(IsDBNull(drVuelo("HrRecojo")) = True, "01/01/1900", drVuelo("HrRecojo"))
                        .PaxC = drVuelo("PaxC")
                        .PCotizado = drVuelo("PCotizado")
                        .TCotizado = drVuelo("TCotizado")
                        .PaxV = drVuelo("PaxV")
                        .PVolado = drVuelo("PVolado")
                        .TVolado = drVuelo("TVolado")
                        .Status = drVuelo("Status")
                        .Comentario = drVuelo("Comentario")
                        .Llegada = drVuelo("Llegada")
                        .Emitido = drVuelo("Emitido")
                        .TipoTransporte = drVuelo("TipoTransporte")
                        .CodReserva = If(IsDBNull(drVuelo("CodReserva")) = True, "", drVuelo("CodReserva"))
                        .Tarifa = drVuelo("Tarifa")
                        .PTA = drVuelo("PTA")
                        .ImpuestosExt = drVuelo("ImpuestosExt")
                        .Penalidad = drVuelo("Penalidad")
                        .IGV = drVuelo("IGV")
                        .Otros = drVuelo("Otros")
                        .PorcComm = drVuelo("PorcComm")
                        .MontoContado = drVuelo("MontoContado")
                        .MontoTarjeta = drVuelo("MontoTarjeta")
                        .IDTarjetaCredito = drVuelo("IdTarjCred")
                        .NumeroTarjeta = If(IsDBNull(drVuelo("NumeroTarjeta")) = True, "", drVuelo("NumeroTarjeta"))
                        .Aprobacion = drVuelo("Aprobacion")
                        .PorcOver = drVuelo("PorcOver")
                        .PorcOverInCom = drVuelo("PorcOverInCom")
                        .MontComm = drVuelo("MontComm")
                        .MontCommOver = drVuelo("MontCommOver")
                        .IGVComm = drVuelo("IGVComm")
                        .IGVCommOver = drVuelo("IGVCommOver")
                        .PorcTarifa = drVuelo("PorcTarifa")
                        .NetoPagar = drVuelo("NetoPagar")
                        .Descuento = drVuelo("Descuento")
                        .IDLineaA = drVuelo("IdLineaA")
                        .MontoFEE = drVuelo("MontoFEE")
                        .IGVFEE = drVuelo("IGVFEE")
                        .TotalFEE = drVuelo("TotalFEE")
                        .EmitidoSetours = drVuelo("EmitidoSetours")
                        .EmitidoOperador = drVuelo("EmitidoOperador")
                        .IDProveedorOperador = If(IsDBNull(drVuelo("IDProveedorOperador")) = True, "", drVuelo("IDProveedorOperador"))
                        .CostoOperador = If(IsDBNull(drVuelo("CostoOperador")) = True, 0, drVuelo("CostoOperador"))
                        .IdaVuelta = If(IsDBNull(drVuelo("IdaVuelta")) = True, "", drVuelo("IdaVuelta"))
                        .IDServicio_DetPeruRail = If(IsDBNull(drVuelo("IDServicio_DetPeruRail")) = True, 0, drVuelo("IDServicio_DetPeruRail"))
                        .UserMod = vstrUserMod
                    End With
                    ListaVuelos.Add(objVuelosBE)
                End While
                drVuelo.Close()
            End Using

            objBE.ListaCotizacionVuelo = ListaVuelos
            objBE.ListaDetalleCotiz = ListaDetCot

            Return objBE
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strExistenCambiosPendientes(ByVal vintIDCab As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pAreaPendiente", String.Empty)

            Return objADO.GetSPOutputValue("COTICAB_SelExistenCambiosPendientes", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarGastosTransferencia(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable(True, "COTIDET_SelGastosTransferenciaProvExternos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionQuiebresBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "COTICAB_QUIEBRES_Sel_xIDCab", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarListCopia(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataReader("COTICA_QUIEBRES_Sel_Copia", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
  <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionArchivosBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "COTICAB_ARCHIVOS_Sel_List_IDCAB", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionTiposCambioBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function blnExisteTiposCambio(ByVal vintIDCab As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim blnExiste As Boolean = False
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pExiste", False)

                blnExiste = objADO.GetSPOutputValue("COTICAB_TIPOSCAMBIO_SelExistsOutPut", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListTipoCambio() As DataTable
            Try
                Return objADO.GetDataTable(True, "COTICAB_TIPOSCAMBIO_SelTiposCambio", New Dictionary(Of String, String))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "COTICAB_TIPOSCAMBIO_SelxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function drConsultarList(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataReader("COTICAB_TIPOSCAMBIO_SelxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionViaticosTCBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarxIDCab(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "VIATICOS_TOURCONDUCTOR_SelxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionIncluidoBN
        Inherits ServicedComponent
        Dim ObjADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vintIDCAB As Integer) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCAB", vintIDCAB)

                Return ObjADO.GetDataReader("COTI_INCLUIDO_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32, ByVal vstrIDIdioma As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDIdioma", vstrIDIdioma)

                Return ObjADO.GetDataTable(True, "COTI_INCLUIDO_Sel_xIDCab", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCostoTCCotizacionBN
        Inherits ServicedComponent
        Dim objADO As clsDataAccessDN

        Public Overloads Function ConsultarList(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataTable(True, "COTICAB_COSTOSTC_Sel_ListxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Overloads Function blnExistexIDCab(ByVal vintIDCab As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Dim blnExiste As Boolean = False
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pExiste", False)
                blnExiste = objADO.GetSPOutputValue("COTICAB_COSTOSTC_Sel_ExistsxIDCab", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDetalleCotizacionBN
        Inherits ServicedComponent
        Dim gstrTipoProveeHoteles As String = "001"
        Dim gstrTipoProveeRestaurantes As String = "002"
        Dim gstrTipoProveeOperadores As String = "003"
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32, ByVal vblnConCopias As Boolean, _
                                            ByVal vblnSoloparaRecalcular As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@ConCopias", vblnConCopias)
                dc.Add("@SoloparaRecalcular", vblnSoloparaRecalcular)
                dc.Add("@ConIDReserva_Det", False)

                Return objADO.GetDataTable(True, "COTIDET_Sel_xIDCab", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function
        Public Function ConsultarTodosxIDCab(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataReader("COTIDET_Sel_TodosxIDCab", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPaxAcomodoEspecial(ByVal vintIDDet As Integer) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDDet", vintIDDet)
                Return objADO.GetDataTable(True, "COTIDET_SelPaxAcomodoEspecialxIDDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPaLiberadosxIDCab(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataReader("COTIDET_SelPaxLiberadosxIDCab", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarExistsPresupuestoxIDDet(ByVal vintIDDet As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@pExistePresupuesto", False)

                Return objADO.GetSPOutputValue("COTIDET_Sel_ExistPresupuesto", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        ''->PPMG20160330
        Public Function blnDetEsZicasso(ByVal vstrIDProveedor As String,
                                               ByVal vstrIDServicio As String, ByVal vintIDServicio_Det As Int32) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@IDServicio_Det", vintIDServicio_Det)
                dc.Add("@pExisteZ", False)

                Return objADO.GetSPOutputValue("COTIDET_Sel_Es_Zicasso", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function dblDetValorZicasso(ByVal vintIDCab As Int32) As Double
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pTotalParaZ", 0)

                Return objADO.GetSPOutputValue("COTIDET_Sel_Valor_Zicasso", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Sub dblDetUpdateZicasso(ByVal vintIDCab As Int32)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                objADO.ExecuteSP("COTIDET_Update_Valor_Zicasso", dc)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        ''<-PPMG20160330

        Public Function ConsultarTodosxIDCabProveedorLog(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataReader("COTIDET_Sel_TodosxIDCabProveedorLog", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarUltimoLogxIDProveedor(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataReader("COTIDET_LOG_SelUltimoxIDProveedor", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function dttConsultarTodosxIDCabProveedorLog(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "COTIDET_Sel_TodosxIDCabProveedorLog", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function blnVerificaHorarioServicio(ByVal vstrIDServicio As String, ByVal vdatFechaDia As Date, Optional ByVal vblnSoloDia As Boolean = False) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@FechaDia", Format(vdatFechaDia, "dd/MM/yyyy HH:mm:ss"))
                dc.Add("@pOk", 0)
                If vblnSoloDia Then
                    Return objADO.ExecuteSPOutput("MASERVICIOS_Sel_VerificaDiaOutPut", dc)
                Else
                    Return objADO.ExecuteSPOutput("MASERVICIOS_Sel_VerificaHoraOutput", dc)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarDestinos(ByVal vintIDCab As Int32) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Dim strSql As String = "COTIDET_Sel_Destinos"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarListLog(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "COTIDET_LOG_Sel_List", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarPK(ByVal vintIDDet As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                'dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDDet", vintIDDet)

                Return objADO.GetDataReader("COTIDET_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarListaxAcomodosEspeciales(ByVal vintIDCab As Integer) As Data.DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "COTIDET_SelServiciosxAcomodoEspecialGroup", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListaxAcomodosEspeciales(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String, _
                                                          ByVal vdatFechaIn As Date, ByVal vdatFechaOut As Date) As Data.DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@FecDiaIng", vdatFechaIn)
                dc.Add("@FecDiaOut", vdatFechaOut)

                Return objADO.GetDataTable(True, "COTIDET_SelServiciosxAcomodoEspecial", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function strConsultarDetaVariante(ByVal vintIDServicio_Det As Integer, ByVal vstrTipoProv As String, _
                                                 ByVal vstrIDServicio As String, ByVal vstrVariante As String, _
                                                 ByVal vstrAnio As String, ByVal vblnConAlojamiento As Boolean) As String
            Dim dc As Dictionary(Of String, String)
            Try
                Dim strDetaVariante As String = ""
                If vstrTipoProv = gstrTipoProveeHoteles Or vstrTipoProv = gstrTipoProveeRestaurantes Then
                    dc = New Dictionary(Of String, String)
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    Using dr As SqlClient.SqlDataReader = objADO.GetDataReader("MASERVICIOS_DET_Sel_VerDetaVariante", dc)
                        If dr.HasRows Then
                            dr.Read()
                            If Convert.ToBoolean(dr("VerDetaTipo")) Then
                                strDetaVariante = If(IsDBNull(dr("DetaTipo")) = True, "", dr("DetaTipo"))
                            End If
                        End If
                        dr.Close()
                    End Using

                ElseIf vstrTipoProv = gstrTipoProveeOperadores And vblnConAlojamiento = True Then
                    Dim intIDServicio_Det As Integer = 0

                    Using objServBN As New clsServicioProveedorBN
                        Using dr As SqlClient.SqlDataReader = objServBN.ConsultarxIDServicioAnio(vstrIDServicio, vstrAnio, vstrVariante, 0)
                            If dr.HasRows Then
                                While dr.Read
                                    If Convert.ToBoolean(dr("VerDetaTipo")) Then
                                        intIDServicio_Det = dr("IDServicio_Det")
                                        Exit While
                                    End If
                                End While
                            End If
                            dr.Close()
                        End Using
                    End Using

                    If intIDServicio_Det <> 0 Then

                        dc = New Dictionary(Of String, String)
                        dc.Add("@IDServicio_Det", intIDServicio_Det)
                        Using drCodTarifa As SqlClient.SqlDataReader = objADO.GetDataReader("MASERVICIOS_DET_Sel_CodTarifa", dc)
                            If drCodTarifa.HasRows Then
                                drCodTarifa.Read()
                                strDetaVariante = If(IsDBNull(drCodTarifa("CodTarifa")) = True, "", drCodTarifa("CodTarifa"))
                            End If
                            drCodTarifa.Close()
                        End Using
                    End If

                End If

                Return strDetaVariante

            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function objReemplazoDetalleCotizacionxHotel(ByVal vObjCotiDetBEDel As clsCotizacionBE.clsCotiDetGridBE, _
                                                            ByVal vObjCotiDetBENuevo As clsCotizacionBE.clsCotiDetGridBE, ByRef rDtDatosNuevos As DataTable, _
                                                            ByVal vLstCotiDetBE As List(Of clsCotizacionBE.clsCotiDetGridBE)) As Object
            Try

                Dim dcFechaItemOrden As New Dictionary(Of Date, Single)
                Dim LstIDDetsEliminados As New List(Of String)
                For Each Item As clsCotizacionBE.clsCotiDetGridBE In vLstCotiDetBE
                    If Item.IDServicio = vObjCotiDetBEDel.IDServicio And Item.IDServicio_Det = vObjCotiDetBEDel.IDServicio_Det _
                        And Item.IDDet <> vObjCotiDetBEDel.IDDet And Item.IncGuia = vObjCotiDetBEDel.IncGuia And Item.PlanAlimenticio = vObjCotiDetBEDel.PlanAlimenticio Then
                        LstIDDetsEliminados.Add(Item.IDDet)
                        dcFechaItemOrden.Add(CDate(Item.Dia), CSng(Item.Item))
                    End If
                Next

                For Each dicItems As KeyValuePair(Of Date, Single) In dcFechaItemOrden
                    rDtDatosNuevos.Rows.Add(0, Nothing, dicItems.Value, dicItems.Key, dicItems.Key.ToString("ddd dd/MM/yyyy").ToUpper, _
                                            "", vObjCotiDetBENuevo.IDPais, vObjCotiDetBENuevo.IDCiudad, _
                                            vObjCotiDetBENuevo.DescCiudad)
                Next

                Return LstIDDetsEliminados
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function strDevuelveValorDataGridView(ByVal vDgv As System.Windows.Forms.DataGridView, ByVal vstrIDTransporte As String, _
                                                     ByVal vstrColumn As String) As String
            Try
                Dim strIDColumn As String = String.Empty
                Dim strValorRetorno As String = String.Empty
                Select Case vDgv.Name
                    Case "dgvVuelos" : strIDColumn = "ID"
                    Case "dgvBuss" : strIDColumn = "IDB"
                    Case "dgvTren" : strIDColumn = "IDT"
                End Select
                Dim blnExisteColumn As Boolean = False
                For Each col As System.Windows.Forms.DataGridViewColumn In vDgv.Columns
                    If col.Name = vstrColumn Then
                        blnExisteColumn = True
                        Exit For
                    End If
                Next

                If blnExisteColumn Then
                    For Each row As System.Windows.Forms.DataGridViewRow In vDgv.Rows
                        If Not row.Cells(strIDColumn).Value Is Nothing Then
                            If row.Cells(strIDColumn).Value.ToString = vstrIDTransporte Then    ''PPMG20151214
                                strValorRetorno = row.Cells(vstrColumn).Value
                                Exit For
                            End If
                        End If
                    Next
                End If

                If (InStr(vstrColumn.ToLower, "hrrecojo") > 0 Or InStr(vstrColumn.ToLower, "salida") > 0 Or InStr(vstrColumn.ToLower, "llegada") > 0) _
                    And strValorRetorno = "" Then
                    strValorRetorno = "00:00"
                End If

                Return strValorRetorno
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function objTransportexIDDet(ByVal vobjCotiDetBE As clsCotizacionBE.clsCotiDetGridBE, _
                                            ByVal vbytCantFilasTransp As Byte, _
                                            ByVal vdrIDServicio_DetPeruRail As DataRow, _
                                            ByVal vstrIDTransporte As String, _
                                            ByVal vDgvVuelos As System.Windows.Forms.DataGridView, _
                                            ByVal vDgvBuss As System.Windows.Forms.DataGridView, _
                                            ByVal vDgvTren As System.Windows.Forms.DataGridView) As Object
            'ByVal vblnPeruRail As Boolean
            Try

                Dim strTipoTransporte As Char = vobjCotiDetBE.TipoTransporte
                Dim blnPeruRail As Boolean = False
                If Not vdrIDServicio_DetPeruRail Is Nothing Then
                    blnPeruRail = True
                End If

                If strTipoTransporte = "V" Or strTipoTransporte = "B" Or strTipoTransporte = "T" Or blnPeruRail = True Then

                    If strTipoTransporte = "V" Then
                        Dim objGridVuelosBE As New clsCotizacionBE.clsVuelosGridBE

                        objGridVuelosBE.ID = "V" & vbytCantFilasTransp
                        'objGridVuelosBE.IDDetV = "V" & vobjCotiDetBE.IDDet
                        If Not IsNumeric(vobjCotiDetBE.IDDet) Then
                            objGridVuelosBE.IDDetV = "V" & vobjCotiDetBE.IDDet
                        Else
                            objGridVuelosBE.IDDetV = vobjCotiDetBE.IDDet
                        End If
                        If vstrIDTransporte <> "" Then
                            objGridVuelosBE.ID = vstrIDTransporte
                            'objGridVuelosBE.IDDetV = vobjCotiDetBE.IDDet
                        End If

                        objGridVuelosBE.DiaV = Format(CDate(vobjCotiDetBE.Dia), "dd/MM/yyyy") ' vobjCotiDetBE.DiaFormat.ToString
                        objGridVuelosBE.DescripcionV = vobjCotiDetBE.DescServicioDet.ToString
                        objGridVuelosBE.IdLineaA = vobjCotiDetBE.IDProveedor.ToString
                        objGridVuelosBE.DescProveedorV = vobjCotiDetBE.DescProveedor.ToString
                        objGridVuelosBE.CantPaxV = vobjCotiDetBE.NroPax + vobjCotiDetBE.NroLiberados
                        objGridVuelosBE.RutaO = vobjCotiDetBE.IDUbigeoOri.ToString
                        objGridVuelosBE.OrigenVuelo = vobjCotiDetBE.DescUbigeoOri.ToString
                        objGridVuelosBE.RutaD = vobjCotiDetBE.IDUbigeoDes.ToString
                        objGridVuelosBE.Destino = vobjCotiDetBE.DescUbigeoDes.ToString

                        objGridVuelosBE.FechaSalida = vobjCotiDetBE.Dia.ToString
                        objGridVuelosBE.FechaRetorno = vobjCotiDetBE.Dia.ToString

                        objGridVuelosBE.RutaVuelo = strDevuelveCadenaOcultaDerecha(vobjCotiDetBE.DescUbigeoOri) & " / " & _
                            strDevuelveCadenaOcultaDerecha(vobjCotiDetBE.DescUbigeoDes)

                        objGridVuelosBE.TipoTransporteV = "V"

                        Return objGridVuelosBE
                    ElseIf strTipoTransporte = "B" Then
                        Dim objGridBusBE As New clsCotizacionBE.clsBusGridBE


                        objGridBusBE.DiaB = Format(CDate(vobjCotiDetBE.Dia), "dd/MM/yyyy") 'vobjCotiDetBE.DiaFormat.ToString
                        objGridBusBE.ServicioB = vobjCotiDetBE.DescServicioDet.ToString
                        objGridBusBE.IdLineaAB = vobjCotiDetBE.IDProveedor.ToString
                        objGridBusBE.DescProveedorB = vobjCotiDetBE.DescProveedor.ToString

                        objGridBusBE.CantPaxB = vobjCotiDetBE.NroPax + vobjCotiDetBE.NroLiberados
                        objGridBusBE.RutaOB = vobjCotiDetBE.IDUbigeoOri.ToString
                        objGridBusBE.OrigenB = vobjCotiDetBE.DescUbigeoOri.ToString
                        objGridBusBE.RutaDB = vobjCotiDetBE.IDUbigeoDes.ToString
                        objGridBusBE.DestinoB = vobjCotiDetBE.DescUbigeoDes.ToString

                        objGridBusBE.Fec_SalidaB = Format(CDate(vobjCotiDetBE.Dia), "dd/MM/yyyy")
                        objGridBusBE.Fec_RetornoB = Format(CDate(vobjCotiDetBE.Dia), "dd/MM/yyyy")
                        objGridBusBE.SalidaB = strDevuelveValorDataGridView(vDgvBuss, vstrIDTransporte, "SalidaB") '"00:00"
                        objGridBusBE.LlegadaB = strDevuelveValorDataGridView(vDgvBuss, vstrIDTransporte, "LlegadaB") '"00:00"
                        objGridBusBE.HrRecojoB = strDevuelveValorDataGridView(vDgvBuss, vstrIDTransporte, "HrRecojoB") '"00:00"
                        objGridBusBE.Num_BoletoB = strDevuelveValorDataGridView(vDgvBuss, vstrIDTransporte, "Num_BoletoB")
                        objGridBusBE.CodReservaB = strDevuelveValorDataGridView(vDgvBuss, vstrIDTransporte, "CodReservaB")


                        objGridBusBE.TipoTransporteB = "B"
                        objGridBusBE.IDCABB = vobjCotiDetBE.IDCab
                        objGridBusBE.IDB = "B" & vbytCantFilasTransp
                        'objGridBusBE.IDDetB = "B" & vobjCotiDetBE.IDDet
                        If Not IsNumeric(vobjCotiDetBE.IDDet) Then
                            objGridBusBE.IDDetB = "B" & vobjCotiDetBE.IDDet
                        Else
                            objGridBusBE.IDDetB = vobjCotiDetBE.IDDet
                        End If
                        If vstrIDTransporte <> "" Then
                            objGridBusBE.IDB = vstrIDTransporte
                            'objGridBusBE.IDDetB = vobjCotiDetBE.IDDet
                        End If

                        objGridBusBE.RutaB = strDevuelveCadenaOcultaDerecha(vobjCotiDetBE.DescUbigeoOri) & " / " & _
                        strDevuelveCadenaOcultaDerecha(vobjCotiDetBE.DescUbigeoDes)
                        objGridBusBE.EmitidoSetoursB = True


                        Return objGridBusBE
                    ElseIf strTipoTransporte = "T" Or blnPeruRail = True Then
                        Dim objGridTrenBE As New clsCotizacionBE.clsTrenGridBE


                        objGridTrenBE.DiaT = Format(CDate(vobjCotiDetBE.Dia), "dd/MM/yyyy") 'vobjCotiDetBE.DiaFormat.ToString
                        objGridTrenBE.Fec_SalidaT = Format(CDate(vobjCotiDetBE.Dia), "dd/MM/yyyy")
                        objGridTrenBE.Fec_RetornoT = Format(CDate(vobjCotiDetBE.Dia), "dd/MM/yyyy")
                        objGridTrenBE.SalidaT = strDevuelveValorDataGridView(vDgvTren, vstrIDTransporte, "SalidaT") '"00:00"
                        objGridTrenBE.LlegadaT = strDevuelveValorDataGridView(vDgvTren, vstrIDTransporte, "LlegadaT") '"00:00"
                        objGridTrenBE.HrRecojoT = strDevuelveValorDataGridView(vDgvTren, vstrIDTransporte, "HrRecojoT") '"00:00"
                        objGridTrenBE.VueloT = strDevuelveValorDataGridView(vDgvTren, vstrIDTransporte, "VueloT")
                        objGridTrenBE.CodReservaT = strDevuelveValorDataGridView(vDgvTren, vstrIDTransporte, "CodReservaT")

                        objGridTrenBE.TipoTransporteT = "T"
                        objGridTrenBE.IDCABT = vobjCotiDetBE.IDCab
                        objGridTrenBE.IDT = "T" & vbytCantFilasTransp
                        'objGridTrenBE.IDDetT = "T" & vobjCotiDetBE.IDDet
                        If Not IsNumeric(vobjCotiDetBE.IDDet) Then
                            objGridTrenBE.IDDetT = "T" & vobjCotiDetBE.IDDet
                        Else
                            objGridTrenBE.IDDetT = vobjCotiDetBE.IDDet
                        End If
                        If vstrIDTransporte <> "" Then
                            objGridTrenBE.IDT = vstrIDTransporte
                            'objGridTrenBE.IDDetT = vobjCotiDetBE.IDDet
                        End If
                        objGridTrenBE.EmitidoSetoursT = True
                        objGridTrenBE.IDServicio_DetPeruRailT = 0
                        If blnPeruRail = True Then
                            'Dim objDetServ As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                            'Dim dr As SqlClient.SqlDataReader = objDetServ.ConsultarPk(vintIDServicio_DetPeruRail)
                            'dr.Read()
                            objGridTrenBE.ServicioTren = vdrIDServicio_DetPeruRail("Descripcion")
                            objGridTrenBE.IdLineaAT = vdrIDServicio_DetPeruRail("IDProveedor")
                            objGridTrenBE.DescProveedorT = vdrIDServicio_DetPeruRail("DescProveedor")
                            objGridTrenBE.CantPaxT = 0
                            objGridTrenBE.RutaOT = vdrIDServicio_DetPeruRail("IDUbigeoOri")
                            objGridTrenBE.OrigenT = ""
                            objGridTrenBE.RutaDT = vdrIDServicio_DetPeruRail("IDUbigeoDes")
                            objGridTrenBE.DestinoT = ""

                            objGridTrenBE.RutaT = vdrIDServicio_DetPeruRail("AbrevUbigeoOri") & " / " & vdrIDServicio_DetPeruRail("AbrevUbigeoDes")

                            objGridTrenBE.IDServicio_DetPeruRailT = vdrIDServicio_DetPeruRail("IDServicio_Det")
                        Else
                            objGridTrenBE.ServicioTren = vobjCotiDetBE.DescServicioDet
                            objGridTrenBE.IdLineaAT = vobjCotiDetBE.IDProveedor
                            objGridTrenBE.DescProveedorT = vobjCotiDetBE.DescProveedor
                            objGridTrenBE.CantPaxT = vobjCotiDetBE.NroPax + vobjCotiDetBE.NroLiberados
                            objGridTrenBE.RutaOT = vobjCotiDetBE.IDUbigeoOri
                            objGridTrenBE.OrigenT = vobjCotiDetBE.DescUbigeoOri
                            objGridTrenBE.RutaDT = vobjCotiDetBE.IDUbigeoDes
                            objGridTrenBE.DestinoT = vobjCotiDetBE.DescUbigeoDes

                            objGridTrenBE.RutaT = strDevuelveCadenaOcultaDerecha(vobjCotiDetBE.DescUbigeoOri) & " / " & _
                            strDevuelveCadenaOcultaDerecha(vobjCotiDetBE.DescUbigeoDes)
                        End If

                        Return objGridTrenBE
                    End If

                End If

                Return Nothing
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function strConsultarEstadoReserva(ByVal vintIDDet As Integer) As String
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@pCoEstado", "")

                Return objADO.GetSPOutputValue("RESERVAS_SelEstadoxIDDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function strConsultarEstadoReserva(ByVal vintIDCab As Integer, ByVal vstrCoProveedor As String) As String
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@CoProveedor", vstrCoProveedor)
                dc.Add("@pCoEstado", "")

                Return objADO.GetSPOutputValue("RESERVAS_SelEstadoxIDCabProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleCotizacionDescripServicioBN
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN

            Public Function ConsultarListxIDDet(ByVal vintIDDet As Int32, _
                                                ByVal vstrIDServicio As String, ByVal vbytDia As Byte) As DataTable
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDDet", vintIDDet)
                    dc.Add("@IDServicio", vstrIDServicio)
                    dc.Add("@Dia", vbytDia)

                    Return objADO.GetDataTable(True, "COTIDET_DESCRIPSERV_Sel_xIDDet", dc)
                Catch ex As Exception
                    Throw

                End Try

            End Function

            Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32, ByVal vstrIdioma As String) As DataTable
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@IDIdioma", vstrIdioma)

                    Return objADO.GetDataTable(True, "COTIDET_DESCRIPSERV_Sel_xIDCab", dc)
                Catch ex As Exception
                    Throw

                End Try

            End Function

            Public Function ConsultarListxIDDetCopy(ByVal vintIDDet As Int32) As DataTable
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDDet", vintIDDet)

                    Return objADO.GetDataTable(True, "COTIDET_DESCRIPSERV_Sel_xIDDetCopy", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function

            Public Function ConsultarTodosxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)

                    Return objADO.GetDataReader("COTIDET_DESCRIPSERV_Sel_TodosxIDCab", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleCotizacionQuiebresBN
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN


            Public Function ConsultarListxIDDet(ByVal vintIDDet As Int32) As DataTable
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDDet", vintIDDet)

                    Return objADO.GetDataTable(True, "COTIDET_QUIEBRES_SelxIDDet", dc)
                Catch ex As Exception
                    Throw

                End Try

            End Function

            Public Function ConsultarxIDCab_Q(ByVal vintIDCab_Q As Int32) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDCab_Q", vintIDCab_Q)

                    Return objADO.GetDataReader("COTIDET_QUIEBRES_SelxIDCab_Q", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function

            Public Function ConsultarTodosxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)

                    Return objADO.GetDataReader("COTIDET_QUIEBRES_Sel_TodosxIDCab", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleCotizacionDetServiciosBN
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN

            Public Function ConsultarTodosxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDCab", vintIDCab)

                    Return objADO.GetDataReader("COTIDET_DETSERVICIOS_Sel_TodosxIDCab", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function

            Public Function ConsultarTodosxIDDet(ByVal vintIDDet As Integer) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)
                Try
                    dc.Add("@IDDet", vintIDDet)

                    Return objADO.GetDataReader("COTIDET_DETSERVICIOS_SelxIDDet", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleCotizacionAcomodoEspecialBN
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN

            Public Function ConsultarListxIDDet(ByVal vintIDDet As Int32) As DataTable
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDDet", vintIDDet)

                    Return objADO.GetDataTable(True, "COTIDET_ACOMODOESPECIAL_SelxIDDet", dc)
                Catch ex As Exception
                    Throw

                End Try

            End Function

            Public Function blnExistexIDCab(ByVal vintIDCab As Int32) As Boolean
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDCab", vintIDCab)
                    dc.Add("@pExiste", 0)

                    Return objADO.GetSPOutputValue("COTIDET_ACOMODOESPECIAL_Sel_ExistexFileOutput", dc)

                Catch ex As Exception
                    Throw

                End Try

            End Function

            Public Function blnExistexIDReserva(ByVal vintIDReserva As Int32) As Boolean
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDReserva", vintIDReserva)
                    dc.Add("@pExiste", 0)
                    Return objADO.GetSPOutputValue("COTIDET_ACOMODOESPECIAL_Sel_ExistexReservaOutput", dc)

                Catch ex As Exception
                    Throw

                End Try

            End Function

            Public Class clsDistribucAcomodoEspecialBN
                Dim objADO As New clsDataAccessDN
                Public Function ConsultarListxIDDet(ByVal vintIDDet As Int32) As DataTable
                    Dim dc As New Dictionary(Of String, String)

                    Try
                        dc.Add("@IDDet", vintIDDet)

                        Return objADO.GetDataTable(True, "COTIDET_ACOMODOESPECIAL_DISTRIBUC_SelxIDDet", dc)
                    Catch ex As Exception
                        Throw

                    End Try

                End Function

                Public Function ConsultarPaxxIDDet(ByVal vintIDDet As Integer) As DataTable
                    Try
                        Dim dc As New Dictionary(Of String, String)
                        dc.Add("@IDDet", vintIDDet)

                        Return objADO.GetDataTable(True, "COTIDET_PAX_Sel_xIDDet", dc)
                    Catch ex As Exception
                        Throw
                    End Try
                End Function
            End Class
        End Class
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTourConductorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarPK(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataReader("COTICAB_TOURCONDUCTOR_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionPaxBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDCabLvw(ByVal vintIDCab As Int32, _
                                               ByVal vblnNoShow As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@NoShow", vblnNoShow)

                Return objADO.GetDataTable(True, "COTIPAX_Sel_xIDCab_Lvw", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function drConsultarListxIDCab(ByVal vintIDCab As Int32, _
                                              ByVal vblnNoShow As Boolean) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@NoShow", vblnNoShow)

                Return objADO.GetDataReader("COTIPAX_Sel_xIDCab_Lvw", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarDetxIDDetLvw(ByVal vintIDDet As Int32, ByVal vblnFlDesactNoShow As Boolean) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@FlDesactNoShow", vblnFlDesactNoShow)
                Return objADO.GetDataReader("COTIDET_PAX_Sel_xIDDet_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function blnConsultarPaxEntradasMPCompradasxIDDet(ByVal vintIDDet As Int32) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@pbCompradas ", False)
                Return objADO.ExecuteSPOutput("COTIDET_PAX_SelEntradasMPCompradasxIDDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function blnConsultarPaxEntradasWPCompradasxIDDet(ByVal vintIDDet As Int32) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@pbCompradas ", False)
                Return objADO.ExecuteSPOutput("COTIDET_PAX_SelEntradasWPCompradasxIDDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarDetxIDDetLvw2(ByVal vintIDDet As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                Return objADO.GetDataTable(True, "COTIDET_PAX_Sel_xIDDet_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "COTIPAX_Sel_ListxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListPaxMasNinguno(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "COTIPAX_SelPaxxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDCabxNombre(ByVal vintIDCab As Int32, ByVal vstrNombrePax As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@Nombre", vstrNombrePax)

                Return objADO.GetDataTable(True, "COTIPAX_Sel_ListxIDCabxNombre", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarExisteTourConductor(ByVal vintIDCab As Integer) As Integer
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim intIDPaxTC As Integer = 0
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pIDPaxTC", 0)

                intIDPaxTC = objADO.GetSPOutputValue("COTIPAX_SelExistTourConductor", dc)
                Return intIDPaxTC
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarTourConductor(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataReader("COTIPAX_Sel_TCxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarMontosxIDCabLvw(ByVal vintIDCab As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataTable(True, "COTIPAX_Sel_MontosLvw", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function
        Public Function intDevuelvePaxMayor(ByVal vintIDDet As Int32) As Integer
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@pIDPax", 0)

                Return objADO.ExecuteSPOutput("COTIDET_PAX_Sel_MayorPaxOutput", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function
        'COTIPAX_Sel_ListxIDCab_Mail
        Public Function ConsultarListPaxxIDCabxIDReserva(ByVal vintIDCab As Integer, ByVal vintIDReserva As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDReserva", vintIDReserva)

                Return objADO.GetDataTable(True, "COTIPAX_Sel_ListxIDCab_Mail", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function blnDevuelveSiExisteIncompletos(ByVal vintIDCab As Int32) As Boolean
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pExiste", 0)

                Return objADO.ExecuteSPOutput("COTIPAX_Sel_ExisteIncompletosOutput", dc)
            Catch ex As Exception
                Throw

            End Try
        End Function

        Public Function ConsultarPaxAcomodoEspecial(ByVal vintIDDet As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                Return objADO.GetDataTable(True, "COTIDET_SelPaxAcomodoEspecialxIDDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRptxFechaIngresoPais(ByVal vdatFecIngPaisIni As Date, ByVal vdatFecIngPaisFin As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@RangoFechaInicio", vdatFecIngPaisIni)
                    .Add("@RangoFechaFin", vdatFecIngPaisFin)
                End With
                Return objADO.GetDataTable(True, "COTIPAX_Sel_RptxPDB", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRptPDBPaxContabilidad(ByVal vdatFecIngPaisIni As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@FechaMes", vdatFecIngPaisIni)
                    '.Add("@RangoFechaFin", vdatFecIngPaisFin)
                End With
                Return objADO.GetDataTable(True, "ReportePDBPaxContabilidad", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRptPaxConDocIdentNoConfirmado(ByVal vstrIDFile As String, _
                                                               ByVal vdatFechaIngPaisRango1 As Date, ByVal vdatFechaIngPaisRango2 As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDFile", vstrIDFile)
                    .Add("@FecIngresoIn", vdatFechaIngPaisRango1)
                    .Add("@FecIngresoOut", vdatFechaIngPaisRango2)
                End With

                Return objADO.GetDataTable(True, "COTIPAX_Sel_RptNoConfirmados", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPK(ByVal vintIDPax As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDPax", vintIDPax)
                Return objADO.GetDataReader("COTIPAX_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function blnExisteDetPax(ByVal vintIDDet As Integer, ByVal vintIDPax As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@IDPax", vintIDPax)
                dc.Add("@pExiste", False)

                Dim blnExistePaxxDet As Boolean = objADO.GetSPOutputValue("COTIDET_Sel_ExistsxIDDetxIDPax", dc)
                Return blnExistePaxxDet
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCategoriasHotelesBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)


                Return objADO.GetDataTable(True, "COTI_CATEGORIAHOTELES_Sel_xIDCab", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionVuelosBN

        Dim ObjADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vintID As Integer, ByVal vintIDCAB As Integer, ByVal vstrCotizacion As String, ByVal vstrRuta As String, ByVal vstrOrigen As String, _
                                      ByVal vstrDestino As String, ByVal vchrTipoTransporte As Char, ByVal vstrEmitido As String, ByVal vstrStatus As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@ID", vintID)
                dc.Add("@IDCAB", vintIDCAB)
                dc.Add("@Cotizacion", vstrCotizacion)
                dc.Add("@Ruta", vstrRuta)
                dc.Add("@Origen", vstrOrigen)
                dc.Add("@Destino", vstrDestino)
                dc.Add("@TipoTransporte", vchrTipoTransporte)
                dc.Add("@Emitido", vstrEmitido)
                dc.Add("@Status", vstrStatus)

                Return ObjADO.GetDataTable(True, "COTIVUELOS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vintID As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@ID", vintID)

            Dim strSql As String = "COTIVUELOS_Sel_Pk"
            Return ObjADO.GetDataReader(strSql, dc)
        End Function

        Public Function ConsultarMontoOriginal_Pk(ByVal vintID As Integer) As Double
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@ID", vintID)
                dc.Add("@pCostoOperadorOriginal", 0)

                Dim strSql As String = "COTIVUELOS_SelMontoOrginal_Pk"
                Return ObjADO.ExecuteSPOutput(strSql, dc)

            Catch ex As Exception
                Throw

            End Try
        End Function

        Public Function ConsultarCboLineas(Optional ByVal vbytTodos As Boolean = True, _
                                           Optional ByVal vstrIdTipProv As String = "", _
                                           Optional ByVal vstrDescripcion As String = "", _
                                           Optional ByVal vblnDescTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", vbytTodos)
                dc.Add("@IdTipProv", vstrIdTipProv)
                dc.Add("@Sigla", vstrDescripcion)
                dc.Add("@bTodosDesc", vblnDescTodos)
                Return ObjADO.GetDataTable(True, "COTIVUELOS_Sel_CboAereo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarRutasxIDCab(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Dim strSql As String = "COTIVUELOS_SelRutasxIDCab"
            Return ObjADO.GetDataTable(True, strSql, dc)
        End Function

        Public Function ConsultarRutasVoucherSummary(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                Return ObjADO.GetDataTable(True, "COTIVUELOS_SelRutasVoucherSummary", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRptListadoBolCorrelativo(ByVal FecInicial As Date, ByVal FecFinal As Date, ByVal IdLineaA As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FecInicial", FecInicial)
                dc.Add("@FecFinal", FecFinal)
                dc.Add("@IDProveedor", IdLineaA)
                Return ObjADO.GetDataTable(True, "COTIVUELOS_Sel_RptxBoletosCorrelativos", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarVuelosxOperador(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return ObjADO.GetDataTable(True, "COTIVUELOS_Sel_ListxIDProvOperxIDcab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function ConsultarRptxQuiebreLineaArea()
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        Return ObjADO.GetDataTable(True, "", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function

        Public Function blnDevuelveSiExisteIncompletos(ByVal vintIDCab As Int32) As Boolean
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pExiste", 0)

                Return ObjADO.ExecuteSPOutput("COTIVUELOS_Sel_ExisteIncompletosOutput", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarxIDCabIDDet(ByVal vintIDCab As Integer, ByVal vintIDDet As Integer) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDDet", vintIDDet)

                Return ObjADO.GetDataReader("COTIVUELOS_Sel_xIDCabIDDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarxIDCab(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)

                Return ObjADO.GetDataReader("COTIVUELOS_Sel_xIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function blnExisteVuelosManuales(ByVal vintIDCab As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim blnExiste As Boolean = False
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@pExiste", False)
                blnExiste = ObjADO.GetSPOutputValue("COTIVUELOS_SelExisteManual", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRptNotaVentas(ByVal vstrIDNotaVenta As String) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDNotaVenta", vstrIDNotaVenta)

                Return ObjADO.GetDataTable(True, "COTIVUELOS_RptNotaVenta", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function lstActualizarCostosTrenesPeruRail(ByVal vListaTickets As List(Of clsCotizacionBE.clsTicketsPaxPeruRailBE), ByVal vstrUserMod As String) As List(Of String)
            'Dim ListaExcelDet As New List(Of clsCotizacionBE.clsTicketsPaxPeruRailBE)

            'For Each ItemExcel As clsCotizacionBE.clsTicketsPaxPeruRailBE In vListaTickets
            '    Dim NewItem As New clsCotizacionBE.clsTicketsPaxPeruRailBE With {.NuFile = ItemExcel.NuFile, _
            '                                                                           .CoReserva = ItemExcel.CoReserva, _
            '                                                                           .CoCiudadOri = ItemExcel.CoCiudadOri, _
            '                                                                           .CoCiudadDes = ItemExcel.CoCiudadDes, _
            '                                                                           .SsValorVentaUSD = ItemExcel.SsValorVentaUSD, _
            '                                                                           .SsIgvUSD = ItemExcel.SsIgvUSD, _
            '                                                                           .SsTotalUSD = ItemExcel.SsTotalUSD, _
            '                                                                          .FeEmision = ItemExcel.FeEmision, _
            '                                                                          .NuDocum = ItemExcel.NuDocum}
            '    ListaExcelDet.Add(NewItem)
            'Next

            Dim objBT As New clsVuelosVentasCotizacionBT
            Dim ListaErrores As New List(Of String)
            Dim ListaFiles As New List(Of String)

            For Each ItemExcelDet As clsCotizacionBE.clsTicketsPaxPeruRailBE In vListaTickets
                Dim strError As String = objBT.strGrabarDocumentoPeruRail(ItemExcelDet, vstrUserMod)

                If strError <> "" Then
                    If strError.Substring(0, 4) = "SAP:" Then
                        ItemExcelDet.ErrorSAP = strError
                    Else
                        ItemExcelDet.ErrorSetra = strError
                    End If

                    ListaErrores.Add(ItemExcelDet.NuDocum & " " & strError)
                Else
                    If Not ListaFiles.Contains(ItemExcelDet.NuFile) Then
                        ListaFiles.Add(ItemExcelDet.NuFile)
                    End If
                    ListaErrores.Add(ItemExcelDet.NuDocum & " se actualizó exitosamente.")
                End If
            Next

            For Each strFile As String In ListaFiles
                objBT.ActualizarEstadoVoucherTren(intIDCabxFile(strFile), "001836", vstrUserMod)
            Next

            Return ListaErrores
        End Function

        Private Function intIDCabxFile(ByVal vstrNuFile As String) As Integer
            Try
                Dim intIDCab As Integer = 0
                Using objBN As New clsCotizacionBN
                    Using dr As SqlClient.SqlDataReader = objBN.ConsultarFilexIDCab(vstrNuFile)
                        If dr.HasRows Then
                            dr.Read()
                            intIDCab = dr("IDCab")
                        End If
                        dr.Close()
                    End Using
                End Using
                Return intIDCab

            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class


End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPaxTransporteBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    Public Function ConsultarxTransporte(ByVal vintIDTransporte As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDTransporte", vintIDTransporte)

        Return objADO.GetDataTable(True, "COTITRANSP_PAX_SelxIDTransporte", dc)
    End Function

    Public Function ConsultarxTransport(ByVal vintIDTransporte As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDTransporte", vintIDTransporte)

        Return objADO.GetDataReader("COTITRANSP_PAX_SelxID", dc)
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAcomodoVehiculoBN
    Inherits ServicedComponent
    Dim ObjADO As New clsDataAccessDN
    Public Function ConsultarxIDDet(ByVal vintIDDet As Integer, ByVal vstrCoUbigeo As String, _
                                    ByVal vstrCoCliente As String, ByVal vstrCoVariante As String, ByVal vintCoTipo As Int16) As SqlClient.SqlDataReader

        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@CoUbigeo", vstrCoUbigeo)
            dc.Add("@CoCliente", vstrCoCliente)
            dc.Add("@CoVariante", vstrCoVariante)
            dc.Add("@CoTipo", vintCoTipo)

            Return ObjADO.GetDataReader("ACOMODO_VEHICULO_SelxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    'Public Function drConsultarxIDDet(ByVal vintIDDet As Integer) As SqlClient.SqlDataReader

    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@IDDet", vintIDDet)

    '        Return ObjADO.GetDataReader("ACOMODO_VEHICULO_SelxIDDet2", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function
    Public Function ConsultarDetVtasAcomVehiCopiaPrInterno(ByVal vintIDCab As Integer, ByVal vstrCoUbigeo As String, _
                                                           ByVal vstrCoVariante As String, ByVal vintIDDetBase As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoUbigeo", vstrCoUbigeo)
            dc.Add("@CoVariante", vstrCoVariante)
            dc.Add("@IDDetBase", vintIDDetBase)

            Return ObjADO.GetDataTable(True, "COTIDET_SelAcomoVehicCopia", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnTieneSubServiciosGuia(ByVal vintIDServicio_Det As Integer) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@pSubservGuias", False)

            Return ObjADO.ExecuteSPOutput("MASERVICIOS_DET_SelTieneSubServiciosGuiasOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAcomodoVehiculoExtBN
    Inherits ServicedComponent
    Dim ObjADO As New clsDataAccessDN
    Public Function ConsultarPk(ByVal vintIDDet As Integer) As SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)

            Return ObjADO.GetDataReader("ACOMODO_VEHICULO_EXT_SelPk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarDetVtasAcomVehiCopiaPrExterno(ByVal vintIDCab As Integer,
                                                           ByVal vintIDDetBase As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            'dc.Add("@CoUbigeo", vstrCoUbigeo)
            dc.Add("@IDDetBase", vintIDDetBase)

            Return ObjADO.GetDataTable(True, "COTIDET_SelAcomoVehicExtCopia", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAcomodoVehiculoDetPaxIntBN
    Inherits ServicedComponent
    Dim ObjADO As New clsDataAccessDN
    Public Function ConsultarxIDDet(ByVal vintIDDet As Integer) As SqlClient.SqlDataReader

        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)

            Return ObjADO.GetDataReader("ACOMODO_VEHICULO_DET_PAX_SelxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAcomodoVehiculoDetPaxExtBN
    Inherits ServicedComponent
    Dim ObjADO As New clsDataAccessDN
    Public Function ConsultarxIDDet(ByVal vintIDDet As Integer) As SqlClient.SqlDataReader

        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)

            Return ObjADO.GetDataReader("ACOMODO_VEHICULO_EXT_DET_PAX_SelxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPedidoBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrCoEjeVta As String, ByVal vstrNuPedido As String, _
                                  ByVal vstrNuFile As String, ByVal vstrNuCotizacion As String, _
                                  ByVal vstrTxTitulo As String, ByVal vstrRazonComercial As String, _
                                  ByVal vblnFlHistorico As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoEjeVta", vstrCoEjeVta)
            dc.Add("@NuPedido", vstrNuPedido)
            dc.Add("@NuFile", vstrNuFile)
            dc.Add("@NuCotizacion", vstrNuCotizacion)
            dc.Add("@TxTitulo", vstrTxTitulo)
            dc.Add("@RazonComercial", vstrRazonComercial)
            dc.Add("@FlHistorico", vblnFlHistorico)
            Return objADO.GetDataTable(True, "PEDIDO_Sel_List", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListAddIn(ByVal vstrNroPedido As String, ByVal vstrFile As String, ByVal vstrCotizacion As String, _
                                       ByVal vstrDescCliente As String, ByVal vstrTitulo As String, _
                                       ByVal vstrUsuarioVentas As String, ByVal vstrEstado As String, ByVal vblnHistorico As Boolean) As Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@NroPedido", vstrNroPedido)
                .Add("@IDFile", vstrFile)
                .Add("@Cotizacion", vstrCotizacion)
                .Add("@DescCliente", vstrDescCliente)
                .Add("@Titulo", vstrTitulo)
                .Add("@IDUsVentas", vstrUsuarioVentas)
                .Add("@Estado", vstrEstado)
                .Add("@FlHistorico", vblnHistorico)
            End With

            Return objADO.GetDataTable(True, "PEDIDO_Sel_ListAddIn", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Function InsertarxAddIn(ByVal BE As clsCotizacionBE.clsPedidoBE) As String
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@CoCliente", BE.CoCliente)
            dc.Add("@FePedido", BE.FePedido.ToShortDateString())
            dc.Add("@CoEjeVta", BE.CoEjeVta)
            dc.Add("@TxTitulo", BE.TxTitulo)
            dc.Add("@CoEstado", BE.CoEstado)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@pNuPedido", "")
            Dim strRet As String = objADO.ExecuteSPOutput("PEDIDO_InsxAddIn", dc)

            Return strRet
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: clsPedidoBN.Insertar")
        End Try
    End Function

    Public Overloads Sub ActualizarEstadoxAddIn(ByVal vintNuPedInt As Integer, ByVal vstrCoEstado As String, _
                                                ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vintNuPedInt)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PEDIDO_UpdEstado", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: clsPedidoBN.ActualizarEstadoxAddIn")
        End Try
    End Sub

    Public Function ConsultarListAddInxCoOutlook(ByVal vstrCoOutlook As String) As Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@CoOutlook", vstrCoOutlook)
            End With
            Return objADO.GetDataTable(True, "PEDIDO_Sel_ListxCoOutlookAddIn", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCotizaciones(ByVal vstrNuPedInt As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vstrNuPedInt)
            Return objADO.GetDataTable(True, "COTICAB_SelxPedido", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCotizacionesAddIn(ByVal vstrNuPedInt As Integer, _
                                               ByVal vstrCoCorreoOutlook As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vstrNuPedInt)
            dc.Add("@CoCorreoOutlook", vstrCoCorreoOutlook)

            Return objADO.GetDataTable(True, "COTICAB_SelxPedidoAddIn", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPK(ByVal vintNuPedInt As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vintNuPedInt)
            Return objADO.GetDataReader("PEDIDO_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnYaTieneFile(ByVal vintNuPedInt As Integer, ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vintNuPedInt)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pYaTieneFile", False)
            Return objADO.ExecuteSPOutput("PEDIDO_SelTieneFileOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function bytVersionCotizacion(ByVal vintNuPedInt As Integer) As Byte
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vintNuPedInt)
            dc.Add("@pVersion", 0)
            Return objADO.ExecuteSPOutput("PEDIDO_SelVersionCotizacion", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarCotizacionesFileCorreo(ByVal vstrNuPedInt As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vstrNuPedInt)
            Return objADO.GetDataTable(True, "PEDIDO_SelCotizacionesFiles", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarProveedoresCorreo(ByVal vstrNuPedInt As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuPedInt", vstrNuPedInt)
            Return objADO.GetDataTable(True, "RESERVAS_SelProveedoresCorreoxPedido", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class