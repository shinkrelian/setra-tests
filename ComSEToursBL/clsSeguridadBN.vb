﻿Imports ComDataAccessD2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices


Public Class clsSeguridadBN
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsOpcionesBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAOPCIONES_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDOpc As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDOpc", vstrIDOpc)

            Dim strSql As String = "MAOPCIONES_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function


        Public Function ConsultarLvwDescripcion(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAOPCIONES_Sel_LvwxDescripcion", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvwNombre(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Nombre", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAOPCIONES_Sel_LvwxNombre", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function ConsultarTvw() As DataSet
            Try
                Dim ds As New DataSet
                Dim dt1 As DataTable
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@NivelOpc", 1)

                dt1 = objADO.GetDataTable(True, "MAOPCIONES_Sel_Tvw", dc)
                dt1.TableName = "dtParent"
                ds.Tables.Add(dt1)
                '----
                Dim dt2 As DataTable
                Dim dc2 As New Dictionary(Of String, String)
                dc2.Add("@NivelOpc", 2)

                dt2 = objADO.GetDataTable(True, "MAOPCIONES_Sel_Tvw", dc2)
                dt2.TableName = "dtChild"
                ds.Tables.Add(dt2)

                Dim dt3 As DataTable
                Dim dc3 As New Dictionary(Of String, String)
                dc3.Add("@NivelOpc", 3)

                dt3 = objADO.GetDataTable(True, "MAOPCIONES_Sel_Tvw", dc3)
                dt3.TableName = "dtChild2"
                ds.Tables.Add(dt3)


                '------
                If dt2.Rows.Count > 0 Then
                    ds.Relations.Add("ParentChild", ds.Tables("dtParent").Columns("NivelOpcUnion"), ds.Tables("dtChild").Columns("NivelOpcUnion"))
                    If dt3.Rows.Count > 0 Then
                        ds.Relations.Add("ParentChild2", ds.Tables("dtChild").Columns("NivelOpcUnion2"), ds.Tables("dtChild2").Columns("NivelOpcUnion2"))
                    End If
                End If

                Return ds
            Catch ex As Exception
                Throw
            End Try

        End Function

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsUsuarioBN
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN

            Public Function ConsultarOpcionesMenu(ByVal vstrIDUsuario As String) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDUsuario", vstrIDUsuario)

                    Return objADO.GetDataReader("MAOPCIONES_SelAccionesxUsuario_Menu", dc)
                Catch ex As Exception
                    Throw
                End Try

            End Function

            Public Function ConsultarOpcionesForm(ByVal vstrIDUsuario As String, ByVal vstrForm As String) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDUsuario", vstrIDUsuario)
                    dc.Add("@Form", vstrForm)

                    Return objADO.GetDataReader("MAOPCIONES_SelAccionesxUsuario_Form", dc)
                Catch ex As Exception
                    Throw
                End Try

            End Function

            Public Function ConsultarTvw(ByVal vstrIDUsuario As String) As DataSet
                Try
                    Dim ds As New DataSet
                    Dim dt1 As DataTable
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add("@IDUsuario", vstrIDUsuario)
                    dc.Add("@NivelOpc", 1)

                    dt1 = objADO.GetDataTable(True, "MAUSUARIOSOPC_SelxUsuario_Tvw", dc)
                    dt1.TableName = "dtParent"
                    ds.Tables.Add(dt1)
                    '----
                    Dim dt2 As DataTable
                    Dim dc2 As New Dictionary(Of String, String)
                    dc2.Add("@IDUsuario", vstrIDUsuario)
                    dc2.Add("@NivelOpc", 2)

                    dt2 = objADO.GetDataTable(True, "MAUSUARIOSOPC_SelxUsuario_Tvw", dc2)
                    dt2.TableName = "dtChild"
                    ds.Tables.Add(dt2)

                    Dim dtOpc2 As DataTable
                    Dim dcOpc2 As New Dictionary(Of String, String)
                    dcOpc2.Add("@IDUsuario", vstrIDUsuario)
                    dcOpc2.Add("@NivelOpc", 2)

                    dtOpc2 = objADO.GetDataTable(True, "MAOPCIONES_SelAccionesxUsuario_Tvw", dcOpc2)
                    dtOpc2.TableName = "dtChildOpc"
                    ds.Tables.Add(dtOpc2)
                    '------

                    Dim dt3 As DataTable
                    Dim dc3 As New Dictionary(Of String, String)
                    dc3.Add("@IDUsuario", vstrIDUsuario)
                    dc3.Add("@NivelOpc", 3)

                    dt3 = objADO.GetDataTable(True, "MAUSUARIOSOPC_SelxUsuario_Tvw", dc3)
                    dt3.TableName = "dtChild2"
                    ds.Tables.Add(dt3)

                    Dim dtOpc3 As DataTable
                    Dim dcOpc3 As New Dictionary(Of String, String)
                    dcOpc3.Add("@IDUsuario", vstrIDUsuario)
                    dcOpc3.Add("@NivelOpc", 3)

                    dtOpc3 = objADO.GetDataTable(True, "MAOPCIONES_SelAccionesxUsuario_Tvw", dcOpc3)
                    dtOpc3.TableName = "dtChildOpc2"
                    ds.Tables.Add(dtOpc3)



                    '------
                    If dt2.Rows.Count > 0 Then
                        ds.Relations.Add("ParentChild", ds.Tables("dtParent").Columns("NivelOpcUnion"), ds.Tables("dtChild").Columns("NivelOpcUnion"))
                        If dtOpc2.Rows.Count > 0 Then
                            ds.Relations.Add("ParentChildOpc", ds.Tables("dtChild").Columns("IDOpc"), ds.Tables("dtChildOpc").Columns("IDOpc"))




                            If dt3.Rows.Count > 0 Then
                                ds.Relations.Add("ParentChild2", ds.Tables("dtChild").Columns("NivelOpcUnion2"), ds.Tables("dtChild2").Columns("NivelOpcUnion2"))
                                If dtOpc3.Rows.Count > 0 Then
                                    ds.Relations.Add("ParentChildOpc2", ds.Tables("dtChild2").Columns("IDOpc"), ds.Tables("dtChildOpc2").Columns("IDOpc"))
                                End If
                            End If
                        End If
                    End If

                    Return ds
                Catch ex As Exception
                    Throw
                End Try

            End Function
        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsNivelBN 'o Perfil
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN
            Public Function ConsultarTvw(ByVal vstrIDNivel As String) As DataSet
                Try
                    Dim ds As New DataSet
                    Dim dt1 As DataTable
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add("@IDNivel", vstrIDNivel)
                    dc.Add("@NivelOpc", 1)

                    dt1 = objADO.GetDataTable(True, "MANIVELOPC_SelxNivel_Tvw", dc)
                    dt1.TableName = "dtParent"
                    ds.Tables.Add(dt1)
                    '----
                    Dim dt2 As DataTable
                    Dim dc2 As New Dictionary(Of String, String)
                    dc2.Add("@IDNivel", vstrIDNivel)
                    dc2.Add("@NivelOpc", 2)

                    dt2 = objADO.GetDataTable(True, "MANIVELOPC_SelxNivel_Tvw", dc2)
                    dt2.TableName = "dtChild"
                    ds.Tables.Add(dt2)

                    Dim dtOpc2 As DataTable
                    Dim dcOpc2 As New Dictionary(Of String, String)
                    dcOpc2.Add("@IDNivel", vstrIDNivel)
                    dcOpc2.Add("@NivelOpc", 2)

                    dtOpc2 = objADO.GetDataTable(True, "MAOPCIONES_SelAccionesxNivel_Tvw", dcOpc2)
                    dtOpc2.TableName = "dtChildOpc"
                    ds.Tables.Add(dtOpc2)
                    '------

                    Dim dt3 As DataTable
                    Dim dc3 As New Dictionary(Of String, String)
                    dc3.Add("@IDNivel", vstrIDNivel)
                    dc3.Add("@NivelOpc", 3)

                    dt3 = objADO.GetDataTable(True, "MANIVELOPC_SelxNivel_Tvw", dc3)
                    dt3.TableName = "dtChild2"
                    ds.Tables.Add(dt3)

                    Dim dtOpc3 As DataTable
                    Dim dcOpc3 As New Dictionary(Of String, String)
                    dcOpc3.Add("@IDNivel", vstrIDNivel)
                    dcOpc3.Add("@NivelOpc", 3)

                    dtOpc3 = objADO.GetDataTable(True, "MAOPCIONES_SelAccionesxNivel_Tvw", dcOpc3)
                    dtOpc3.TableName = "dtChildOpc2"
                    ds.Tables.Add(dtOpc3)



                    '------
                    If dt2.Rows.Count > 0 Then
                        ds.Relations.Add("ParentChild", ds.Tables("dtParent").Columns("NivelOpcUnion"), ds.Tables("dtChild").Columns("NivelOpcUnion"))
                        If dtOpc2.Rows.Count > 0 Then
                            ds.Relations.Add("ParentChildOpc", ds.Tables("dtChild").Columns("IDOpc"), ds.Tables("dtChildOpc").Columns("IDOpc"))




                            If dt3.Rows.Count > 0 Then
                                ds.Relations.Add("ParentChild2", ds.Tables("dtChild").Columns("NivelOpcUnion2"), ds.Tables("dtChild2").Columns("NivelOpcUnion2"))
                                If dtOpc3.Rows.Count > 0 Then
                                    ds.Relations.Add("ParentChildOpc2", ds.Tables("dtChild2").Columns("IDOpc"), ds.Tables("dtChildOpc2").Columns("IDOpc"))
                                End If
                            End If
                        End If
                    End If

                    Return ds
                Catch ex As Exception
                    Throw
                End Try

            End Function

            Public Function ConsultarList(ByVal vstrDescripcion As String) As DataTable
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@Descripcion", vstrDescripcion)

                    Return objADO.GetDataTable(True, "MANIVELUSUARIO_Sel_List", dc)
                Catch ex As Exception
                    Throw
                End Try

            End Function
            Public Function ConsultarPk(ByVal vstrIDNivel As String) As SqlClient.SqlDataReader
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDNivel", vstrIDNivel)

                Dim strSql As String = "MANIVELUSUARIO_Sel_Pk"
                Return objADO.GetDataReader(strSql, dc)

            End Function
            Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@ID", vstrID)
                    dc.Add("@Descripcion", vstrDescripcion)

                    Return objADO.GetDataTable(True, "MANIVELUSUARIO_Sel_Lvw", dc)
                Catch ex As Exception
                    Throw
                End Try

            End Function



        End Class



    End Class





End Class


