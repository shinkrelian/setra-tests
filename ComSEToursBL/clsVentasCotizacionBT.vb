﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public MustInherit Class clsVentasCotizacionBaseBT
    Inherits ServicedComponent
    Public objADO As New clsDataAccessDT

    Public Overridable Sub Insertar()

    End Sub

    Public Overridable Sub Actualizar()

    End Sub

    Public Overridable Sub Eliminar()

    End Sub

    Public Overridable Sub InsertarActualizarEliminar()

    End Sub

    Public Overridable Sub ActualizarIDsNuevosenHijosST()

    End Sub

    Public Overridable Sub EliminarIDsHijosST()

    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Dim gstrTipoOperContExportacion As String = "001"
    Dim gstrTipoOperContGravado As String = "002"
    Dim gstrTipoOperContProrrata As String = "006"
    Private gstrIDProveedorSetoursLima As String = "001554"
    Private gstrIDProveedorSetoursCusco As String = "000544"
    Private gstrIDProveedorSetoursBuenosAires As String = "002315"
    Private gstrIDProveedorSetoursSantiago As String = "002317"
    Public gstrRutaImagenes As String
    Private strNombreClase As String = "clsVentasCotizacionBT"

    Public Structure stProveedoresReservas
        Dim IDProveedor As String
        Dim CodReserva As String
        Dim Observaciones As String
    End Structure

    Public Overloads Function Insertar(ByVal BE As clsCotizacionBE) As Int32
        Dim dc As New Dictionary(Of String, String)

        Try
            With dc
                .Add("@IDCliente", BE.IDCliente)
                .Add("@Titulo", BE.Titulo)
                .Add("@TipoPax", BE.TipoPax)
                .Add("@CotiRelacion", BE.CotiRelacion)
                .Add("@IDUsuario", BE.IDUsuario)
                .Add("@NroPax", BE.NroPax)
                .Add("@NroLiberados", BE.NroLiberados)
                .Add("@Tipo_Lib", BE.Tipo_Lib)
                .Add("@Margen", BE.Margen)
                .Add("@IDBanco", BE.IDBanco)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@Observaciones", BE.Observaciones)
                .Add("@FecInicio", BE.FecInicio)
                .Add("@FechaOut", BE.FechaOut)
                .Add("@Cantidad_Dias", BE.Cantidad_Dias)
                .Add("@FechaSeg", BE.FechaSeg)
                .Add("@DatoSeg", BE.DatoSeg)
                .Add("@TipoCambio", BE.TipoCambio)
                .Add("@DocVta", BE.DocVta)
                .Add("@TipoServicio", BE.TipoServicio)
                .Add("@IDIdioma", BE.IDIdioma)
                .Add("@Simple", BE.Simple)
                .Add("@Twin", BE.Twin)
                .Add("@Matrimonial", BE.Matrimonial)
                .Add("@Triple", BE.Triple)
                .Add("@IDUbigeo", BE.IDUbigeo)
                .Add("@Notas", BE.Notas)
                .Add("@Programa_Desc", BE.Programa_Desc)
                .Add("@PaxAdulE", BE.PaxAdulE)
                .Add("@PaxAdulN", BE.PaxAdulN)
                .Add("@PaxNinoE", BE.PaxNinoE)
                .Add("@PaxNinoN", BE.PaxNinoN)
                .Add("@Aporte", BE.Aporte)
                .Add("@AporteMonto", BE.AporteMonto)
                .Add("@AporteTotal", BE.AporteTotal)
                .Add("@Redondeo", BE.Redondeo)
                .Add("@IDCabCopia", BE.IDCabCopia)
                .Add("@Categoria", BE.Categoria)
                .Add("@MargenCero", BE.MargenCero)
                .Add("@UserMod", BE.UserMod)
                .Add("@SimpleResidente", BE.SimpleResidente)
                .Add("@TwinResidente", BE.TwinResidente)
                .Add("@MatrimonialResidente", BE.MatrimonialResidente)
                .Add("@TripleResidente", BE.TripleResidente)
                .Add("@DiasReconfirmacion", BE.DiasReconfirmacion)
                .Add("@ObservacionGeneral", BE.ObservacionGeneral)
                .Add("@ObservacionVentas", BE.ObservacionVentas)
                .Add("@CoTipoVenta", BE.CoTipoVenta)
                .Add("@DocWordVersion", BE.DocWordVersion)
                .Add("@DocWordFecha", BE.DocWordFecha)
                .Add("@NuSerieCliente", BE.NuSerieCliente)
                .Add("@CoSerieWeb", BE.CoSerieWeb)
                .Add("@NuPedInt", BE.NuPedInt)
                .Add("@IDContactoCliente", BE.IDContactoCliente)
                .Add("@pIDCab", 0)

            End With

            Dim intIDCab As Int32 = objADO.ExecuteSPOutput("COTICAB_Ins", dc)
            ActualizarIDsNuevosenHijosST(BE, BE.IdCab, intIDCab)

            ActualizarPorcentaje_Concretacion(intIDCab, BE.UserMod)

            Dim objQui As New clsQuiebresVentasCotizacionBT
            objQui.InsertarActualizarEliminar(BE)

            Dim objTCam As New clsTiposCambioVentasCotizacionBT
            objTCam.InsertarActualizarEliminar(BE)

            Dim objPax As New clsPaxVentasCotizacionBT
            objPax.InsertarActualizarEliminar(BE)

            Dim objBTTC As New clsTourConductorBT
            objBTTC.InsertarEliminar(BE)

            Dim objBTCostosTC As New clsCostosTourConductorBT
            objBTCostosTC.InsertarActualizarEliminar(BE)

            ''->PPMG20160330
            Dim objCotDetBT As New clsCotizacionBT.clsDetalleCotizacionBT
            objCotDetBT.dblDetUpdateZicasso(intIDCab)
            ''<-PPMG20160330


            ContextUtil.SetComplete()

            Return intIDCab
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Public Sub ActualizarListaaFileHistorico(ByVal vListaFiles As List(Of Integer), _
                                             ByVal vblnOk As Boolean, ByVal vstrUserMod As String)
        Try

            For Each intIDCab As Integer In vListaFiles
                ActualizaraFileHistorico(intIDCab, vblnOk, vstrUserMod)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarListaaFileHistorico")

        End Try
    End Sub
    Private Sub ActualizaraFileHistorico(ByVal vintIDCab As Integer, ByVal vblnOk As Boolean, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@FlHistorico", vblnOk)
                .Add("@UserMod", vstrUserMod)

            End With

            objADO.ExecuteSP("COTICAB_UpdFlHistorico", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    'actualizar porcentaje de concretacion
    Private Sub ActualizarPorcentaje_Concretacion(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@UserMod", vstrUserMod)

            End With

            objADO.ExecuteSP("COTICAB_UpdPoConcretVta", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarPorcentaje_Concretacion_Manual(ByVal vintIDCab As Integer, ByVal vstrUserMod As String, ByVal vdblPoConcretVta As Double)
        Try
            Dim dc As New Dictionary(Of String, String)

            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@UserMod", vstrUserMod)
                .Add("@PoConcretVta", vdblPoConcretVta)

            End With

            objADO.ExecuteSP("COTICAB_Actualizar_PoConcretVta", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function strDevuelveIDFileCoti(ByVal vintIDCab As Int32) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pIDFile", "")
            Return objADO.ExecuteSPOutput("COTICAB_Sel_IDFileOutput", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".strDevuelveIDFileCoti")
        End Try
    End Function

    Private Sub ActualizarRuta(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_Upd_Ruta", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarRuta")
        End Try
    End Sub

    Public Sub ActualizarNroPax(ByVal vintIDCab As Integer, ByVal vintNroPax As Int16, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@NroPax", vintNroPax)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_UpdSumaRestaNroPax", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroPax")
        End Try
    End Sub

    Public Overloads Sub Actualizar(ByVal BE As clsCotizacionBE)
        Dim dc As New Dictionary(Of String, String)

        Try
            With dc
                .Add("@IdCab", BE.IdCab)
                .Add("@Estado", BE.Estado)
                .Add("@IDCliente", BE.IDCliente)
                .Add("@Titulo", BE.Titulo)
                .Add("@TipoPax", BE.TipoPax)
                .Add("@CotiRelacion", BE.CotiRelacion)
                .Add("@IDUsuario", BE.IDUsuario)
                .Add("@NroPax", BE.NroPax)
                .Add("@NroLiberados", BE.NroLiberados)
                .Add("@Tipo_Lib", BE.Tipo_Lib)

                .Add("@Margen", BE.Margen)
                .Add("@IDBanco", BE.IDBanco)
                .Add("@IDFile", BE.IDFile)

                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@Observaciones", BE.Observaciones)
                .Add("@FecInicio", BE.FecInicio)
                .Add("@FechaOut", Format(BE.FechaOut, "dd/MM/yyyy"))
                .Add("@Cantidad_Dias", BE.Cantidad_Dias)
                .Add("@FechaSeg", Format(BE.FechaSeg, "dd/MM/yyyy hh:mm:ss"))
                .Add("@DatoSeg", BE.DatoSeg)
                .Add("@TipoCambio", BE.TipoCambio)
                .Add("@DocVta", BE.DocVta)
                .Add("@TipoServicio", BE.TipoServicio)
                .Add("@IDIdioma", BE.IDIdioma)
                .Add("@Simple", BE.Simple)
                .Add("@Twin", BE.Twin)
                .Add("@Matrimonial", BE.Matrimonial)
                .Add("@Triple", BE.Triple)
                .Add("@IDUbigeo", BE.IDUbigeo)
                .Add("@Notas", BE.Notas)
                .Add("@Programa_Desc", BE.Programa_Desc)
                .Add("@PaxAdulE", BE.PaxAdulE)
                .Add("@PaxAdulN", BE.PaxAdulN)
                .Add("@PaxNinoE", BE.PaxNinoE)
                .Add("@PaxNinoN", BE.PaxNinoN)
                .Add("@Aporte", BE.Aporte)
                .Add("@AporteMonto", BE.AporteMonto)
                .Add("@AporteTotal", BE.AporteTotal)
                .Add("@Redondeo", BE.Redondeo)
                .Add("@Categoria", BE.Categoria)
                .Add("@MargenCero", BE.MargenCero)
                .Add("@UserMod", BE.UserMod)

                .Add("@SimpleResidente", BE.SimpleResidente)
                .Add("@TwinResidente", BE.TwinResidente)
                .Add("@MatrimonialResidente", BE.MatrimonialResidente)
                .Add("@TripleResidente", BE.TripleResidente)
                .Add("@DiasReconfirmacion", BE.DiasReconfirmacion)
                .Add("@ObservacionGeneral", BE.ObservacionGeneral)
                .Add("@ObservacionVentas", BE.ObservacionVentas)
                .Add("@DocWordVersion", BE.DocWordVersion)
                .Add("@DocWordFecha", BE.DocWordFecha)
                .Add("@CoTipoVenta", BE.CoTipoVenta)
                .Add("@NuSerieCliente", BE.NuSerieCliente)
                .Add("@CoSerieWeb", BE.CoSerieWeb)
                .Add("@IDContactoCliente", BE.IDContactoCliente)
            End With

            objADO.ExecuteSP("COTICAB_Upd", dc)

            Dim objQui As New clsQuiebresVentasCotizacionBT
            objQui.InsertarActualizarEliminar(BE)

            Dim objTCam As New clsTiposCambioVentasCotizacionBT
            objTCam.InsertarActualizarEliminar(BE)

            Dim objPax As New clsPaxVentasCotizacionBT
            objPax.InsertarActualizarEliminar(BE)

            'CopiarServiciosVarios(BE)
            Dim objBTDet As New clsDetalleVentasCotizacionBT
            objBTDet.InsertarActualizarEliminar(BE)

            Dim objBTTC As New clsTourConductorBT
            objBTTC.InsertarEliminar(BE)

            Dim objBTCostosTC As New clsCostosTourConductorBT
            objBTCostosTC.InsertarActualizarEliminar(BE)

            Dim objViaticosTC As New clsViaticosTCCotizacionBT
            objViaticosTC.Insertar(BE)

            Dim objArchivos As New clsCotizacionBT.clsCotizacionArchivosBT
            objArchivos.InsertarActualizarEliminar(BE)

            ActualizarIDDetTransfer(BE)

            'DESCOMENTAR 20131210
            Dim objBTAcomodoEspecial As New clsDetalleVentasCotizacionAcomodoEspecialBT
            objBTAcomodoEspecial.InsertarActualizarEliminar(BE)

            Dim objBTAcomodoEspecialDistribucion As New clsDetalleVentasCotizacionAcomodoEspecialBT.clsDistribucAcomodoEspecialBT
            objBTAcomodoEspecialDistribucion.InsertarEliminar(BE)

            'Dim objBTVuelos As New clsVuelosVentasCotizacionBT
            'objBTVuelos.InsertarActualizarEliminar(BE)

            'actualizar montos quiebres en base a su detalle
            'objQui.InsertarActualizarEliminar(BE)
            Dim strIDTipoHonorario As String = String.Empty
            If BE.FlTourConductor Then
                If Not BE.ListaTourConductor Is Nothing Then
                    strIDTipoHonorario = BE.ListaTourConductor.FirstOrDefault().CoTipoHon
                End If
            End If

            Dim strTipoPax As String = objBTDet.strDevuelveTipoPaxNacionalidad(BE)
            ReCalculoCotizacion(BE.IdCab, BE.NroPax, BE.NroLiberados, BE.Tipo_Lib, _
                                strTipoPax, BE.Redondeo, BE.IGV, "M", BE.UserMod, _
                                BE.ListaDetPax, BE.MargenCero, strIDTipoHonorario, BE.ListaDetalleCotiz)


            BE.TipoPax = strTipoPax
            If BE.BlPoConcretVta_CheckManual Then
                ActualizarPorcentaje_Concretacion_Manual(BE.IdCab, BE.UserMod, BE.PoConcretVta)
            Else
                ActualizarPorcentaje_Concretacion(BE.IdCab, BE.UserMod)
            End If

            Dim objBTAcomVehi As New clsAcomodoVehiculoBT
            objBTAcomVehi.InsertarActualizarEliminar(BE)
            Dim objBTAcomVehiPax As New clsAcomodoVehiculoDetPaxIntBT
            objBTAcomVehiPax.InsertarActualizarEliminar(BE)
            objBTAcomVehi.InsertarCopias(BE)

            objBTAcomVehi.InsertarEliminarSubServiciosCotizacion(BE)

            Dim objBTAcomVehiExt As New clsAcomodoVehiculo_Ext_BT
            objBTAcomVehiExt.InsertarActualizarEliminar(BE)
            Dim objBTAcomVehiPaxExt As New clsAcomodoVehiculoDetPaxExtBT
            objBTAcomVehiPaxExt.InsertarActualizarEliminar(BE)
            objBTAcomVehiExt.InsertarCopias(BE)

            ActualizarRuta(BE.IdCab, BE.UserMod)
            'Reservas
            If BE.Estado = "A" Then
                Dim objCliBT As New clsClienteBT
                objCliBT.Actualizar_Tipo(BE.IDCliente, BE.UserMod)

                Dim blnCambioPEaAC As Boolean = blnCambioDePendienteaAceptado(BE.IdCab)
                If blnCambioPEaAC Then
                    'Dim objCorreoBT As New clsCorreoVentasBT
                    'objCorreoBT.gstrRutaImagenes = gstrRutaImagenes
                    'objCorreoBT.NotificacionNuevoFile(BE)
                    'ActualizarEnvioCorreoCotizacionAceptada(BE.IdCab, BE.UserMod)
                End If

                ''If strDevuelveIDFileCoti(BE.IdCab).Trim = "" Then
                Dim objResBN As New clsReservaBN
                Dim objResBT As New clsReservaBT
                If Not objResBT.blnExistenReservasxIDCab(BE.IdCab) Then
                    ActualizarListSubServiciosaServiciosReservasDet(BE)
                    AgregarViaticosTourConductor(BE)
                    Dim ListaProveed As Object = objResBT.ListaProvReservas
                    objResBT.CambiosPreGeneracionxFile(BE, ListaProveed)
                    objResBT.GrabarxCotizacion2(BE, False, ListaProveed)
                    objResBT.GrabarxCotizacion2(BE, True, ListaProveed)
                    objResBT.CambiosPostGeneracionxFile(BE, ListaProveed)

                    If BE.NuPedInt <> 0 Then
                        Dim objPed As New clsPedidoBT
                        objPed.ActualizarEstado(BE.NuPedInt, "AC", BE.UserMod)
                    End If
                Else
                    Dim dttProv As DataTable = ConsultarProveedoresNoReservados(BE.IdCab)
                    Dim objOpeBT As New clsOperacionBT
                    Dim blnOpe As Boolean = blnExistenOperacionesProvExternosxIDCab(BE.IdCab)
                    For Each dr As DataRow In dttProv.Rows
                        objResBT.RegenerarReserva(dr("IDReserva"), dr("IDProveedor"), BE.IdCab, BE.UserMod)
                        If blnOpe Then
                            objOpeBT.RegenerarOperacion(0, dr("IDProveedor"), BE.IdCab, BE.UserMod)
                        End If
                    Next

                    'Dim objCorrBT As New clsCorreoVentasBT
                    'objCorrBT.gstrRutaImagenes = gstrRutaImagenes
                    'objCorrBT.NotificacionCambiosModulo(BE)
                End If

                If BE.CambioMatTwin Then
                    Dim dttHot As DataTable = objResBT.ConsultarProveedoresHotelesyAlojamiento(BE.IdCab)
                    For Each dr As DataRow In dttHot.Rows
                        objResBT.ActualizarCambiosenVentasPostFile(dr("IDReserva"), True, BE.UserMod)
                    Next
                End If

            ElseIf BE.Estado = "X" And Not BE.FlCorreoAnuEnviado Then 'Anulado
                'Dim objCorrBT As New clsCorreoVentasBT
                'objCorrBT.gstrRutaImagenes = gstrRutaImagenes
                'objCorrBT.NotificacionEnvioSupervisorFile(BE.IdCab, "X")
                'objCorrBT.gstrRutaImagenes = gstrRutaImagenes
                'objCorrBT.NotificacionFileAnulado(BE)

                'ActualizarEnvioCorreoAnulado(BE.IdCab, BE.UserMod)
            ElseIf BE.Estado = "E" And Not BE.FlCorreoEliEnviado Then
                'Dim objCorrBT As New clsCorreoVentasBT
                'objCorrBT.gstrRutaImagenes = gstrRutaImagenes
                'objCorrBT.NotificacionEnvioSupervisorFile(BE.IdCab, "E")

                'ActualizarEnvioCorreoEliminado(BE.IdCab, BE.UserMod)
            End If

            'Envio de correo a Finanzas INC (Entradas MP/WP)
            ''PPMG20160311
            If BE.Estado = "A" And BE.FlEnviarMPWP = True Then
                EnviarCorreoINC(BE.IdCab)
            End If

            If BE.Estado = "A" Or BE.Estado = "X" Then
                Dim arrDeleteProperties As String() = Nothing
                Dim BEProfit As clsProfitCenterBE = Nothing
                PasarDatosCotiToProfitCenter(BEProfit, BE.IdCab, arrDeleteProperties)

                If BEProfit IsNot Nothing Then
                    Dim objProf As New clsProfitCenterBT
                    objProf.GrabarActualizarSAP(BEProfit, arrDeleteProperties)

                    If BE.Estado = "A" Then
                        ActualizarFlFileMigradoA_SAP(BE.IdCab, True, BE.UserMod)
                    ElseIf BE.Estado = "X" Then
                        ActualizarFlFileAnuladoEn_SAP(BE.IdCab, True, BE.UserMod)
                    End If
                End If
            End If

            ''->PPMG20160330
            Dim objCotDetBT As New clsCotizacionBT.clsDetalleCotizacionBT
            objCotDetBT.dblDetUpdateZicasso(BE.IdCab)
            ''<-PPMG20160330

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function intDevuelveIDCabxIDDet(ByVal vintIDDet As Integer) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pIDCab", 0)

            Return objADO.GetSPOutputValue("COTIDET_SelIDCabxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnCambioDePendienteaAceptado(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExisteCambio As Boolean = False
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pCambioEstado", False)
            blnExisteCambio = objADO.GetSPOutputValue("COTICAB_HISTORIAL_UPD_ExistsCambioPtoA", dc)

            Return blnExisteCambio
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub InsertarInvoiceRpt(ByVal vintIDCab As Integer, ByVal vintIDTemporada As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDTemporada", vintIDTemporada)

            objADO.ExecuteSP("RptInvoiceRangoAPT_InsxTMP", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarEnvioCorreoCotizacionAceptada(ByVal vintIDcab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDcab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_UpdFlCorreoCotizacionAceptada", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarEnvioCorreoAnulado(ByVal vintIDcab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDcab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_UpdFlCorreoAnuEnviadoxIDCab", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarEnvioCorreoEliminado(ByVal vintIDcab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDcab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_UpdFlCorreoEliEnviadoxIDCab", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub PasarDatosCotiToProfitCenter(ByRef BE As clsProfitCenterBE, ByVal vintIDCab As Integer, _
                                             ByRef arrDelProperty As String())
        Try
            Dim dtDato As DataTable = ConsultarPkCoti(vintIDCab)
            If dtDato.Rows.Count > 0 Then
                Dim dr As DataRow = dtDato.Rows(0)
                Dim strIDFile As String = If(IsDBNull(dr("IDFile")) = True, String.Empty, dr("IDFile"))
                Dim blnMigradoSAP As Boolean = If(IsDBNull(dr("FlFileMigA_SAP")) = True, False, dr("FlFileMigA_SAP"))
                Dim blnAnuladoSAP As Boolean = If(IsDBNull(dr("FlFileAnulaEn_SAP")) = True, False, dr("FlFileAnulaEn_SAP"))
                Dim strEstado As String = If(IsDBNull(dr("Estado")), "", dr("Estado"))
                Dim blnActualizarEnSAP As Boolean = False
                If strEstado = "A" Then
                    If Not blnMigradoSAP Then blnActualizarEnSAP = True
                ElseIf strEstado = "X" Then
                    If Not blnAnuladoSAP Then blnActualizarEnSAP = True
                End If

                If blnActualizarEnSAP Then
                    BE = New clsProfitCenterBE()
                    With BE
                        .CenterCode = dr("IDFile")
                        .CenterName = dr("IDFile")
                        .InWhichDimension = 3
                        .Effectivefrom = CDate(dr("FechaReserva"))
                        .CardCode = dr("CardCodeSAP")
                        .CardName = dr("CardName")
                        .Active = 1
                        If dr("Estado").ToString = "X" Then
                            If IsDBNull(dr("FeAnulacion")) Then
                                .EffectiveTo = Date.Now.ToShortDateString
                            Else
                                .EffectiveTo = CDate(dr("FeAnulacion"))
                            End If
                            .Active = 0
                        Else
                            ReDim arrDelProperty(0)
                            arrDelProperty.SetValue("EffectiveTo", 0)
                        End If
                    End With
                End If
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarFlFileAnuladoEn_SAP(ByVal vintIDCab As Integer, ByVal vblnFileMigrado As Boolean, _
                                              ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@FlFileAnulaEn_SAP", vblnFileMigrado)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("COTICAB_UpdAnuSAP", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarFlFileMigradoA_SAP(ByVal vintIDCab As Integer, ByVal vblnFileMigrado As Boolean, _
                                             ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@FlFileMigA_SAP", vblnFileMigrado)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("COTICAB_UpdMigSAP", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub AgregarViaticosTourConductor(ByRef BE As clsCotizacionBE)
        Try
            Dim ListNewItems As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)

            'Reemplazar los datos de servicios x los del Viatico
            Dim oBEDetCot As clsCotizacionBE.clsDetalleCotizacionBE = Nothing
            Dim ListServxSubServ As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = oDetViaticosTourConductorList(BE.IdCab)
            For Each ItemSub As clsCotizacionBE.clsDetalleCotizacionBE In ListServxSubServ
                oBEDetCot = New clsCotizacionBE.clsDetalleCotizacionBE
                With oBEDetCot
                    .IDDet = ItemSub.IDDet
                    .NroPax = ItemSub.NroPax
                    .NroLiberados = ItemSub.NroLiberados
                    .Tipo_Lib = ItemSub.Tipo_Lib
                    .CostoLiberado = ItemSub.CostoLiberado
                    .CostoLiberadoImpto = ItemSub.CostoLiberadoImpto
                    .CostoReal = ItemSub.CostoReal
                    .CostoRealAnt = ItemSub.CostoRealAnt
                    .CostoRealImpto = ItemSub.CostoRealImpto
                    .Cotizacion = ItemSub.Cotizacion
                    .Dia = ItemSub.Dia
                    .Especial = ItemSub.Especial
                    .IDCab = ItemSub.IDCab
                    .IDIdioma = ItemSub.IDIdioma
                    .IDProveedor = ItemSub.IDProveedor
                    .IDTipoProv = ItemSub.IDProveedor
                    .Dias = ItemSub.Dias
                    .IDServicio = ItemSub.IDServicio
                    .IDServicio_Det = ItemSub.IDServicio_Det
                    .IDUbigeo = ItemSub.IDUbigeo
                    .Item = ItemSub.Item
                    .Margen = ItemSub.Margen
                    .MargenAplicado = ItemSub.MargenAplicado
                    .MargenAplicadoAnt = ItemSub.MargenAplicadoAnt
                    .MargenImpto = ItemSub.MargenImpto
                    .MargenLiberado = ItemSub.MargenLiberado
                    .MargenLiberadoImpto = ItemSub.MargenLiberadoImpto
                    .MotivoEspecial = ItemSub.MotivoEspecial
                    .RutaDocSustento = ItemSub.RutaDocSustento
                    .Desayuno = ItemSub.Desayuno
                    .Lonche = ItemSub.Lonche
                    .Almuerzo = ItemSub.Almuerzo
                    .Cena = ItemSub.Cena
                    .Transfer = ItemSub.Transfer
                    .IDDetTransferOri = ItemSub.IDDetTransferOri
                    .IDDetTransferDes = ItemSub.IDDetTransferDes
                    .TipoTransporte = ItemSub.TipoTransporte
                    .IDUbigeoOri = ItemSub.IDUbigeoOri
                    .IDUbigeoDes = ItemSub.IDUbigeoDes
                    .Servicio = ItemSub.Servicio
                    .SSCL = ItemSub.SSCL
                    .SSCLImpto = ItemSub.SSCLImpto
                    .SSCR = ItemSub.SSCR
                    .SSCRImpto = ItemSub.SSCRImpto
                    .SSMargen = ItemSub.SSMargen
                    .SSMargenImpto = ItemSub.SSMargenImpto
                    .SSMargenLiberado = ItemSub.SSMargenLiberado
                    .SSMargenLiberadoImpto = ItemSub.SSMargenLiberadoImpto
                    .SSTotal = ItemSub.SSTotal
                    .SSTotalOrig = ItemSub.SSTotalOrig
                    .STCR = ItemSub.STCR
                    .STCRImpto = ItemSub.STCRImpto
                    .STMargen = ItemSub.STMargen
                    .STMargenImpto = ItemSub.STMargenImpto
                    .STTotal = ItemSub.STTotal
                    .STTotalOrig = ItemSub.STTotalOrig
                    .Total = ItemSub.Total
                    .RedondeoTotal = ItemSub.RedondeoTotal
                    .TotalOrig = ItemSub.TotalOrig
                    .TotImpto = ItemSub.TotImpto
                    .IDDetRel = ItemSub.IDDetRel
                    .IDDetCopia = ItemSub.IDDetCopia
                    .PorReserva = ItemSub.PorReserva
                    .ExecTrigger = ItemSub.ExecTrigger
                    .IDReserva = ItemSub.IDReserva
                    .IncGuia = ItemSub.IncGuia
                    .FueradeHorario = ItemSub.FueradeHorario
                    .ConAlojamiento = ItemSub.ConAlojamiento
                    .CostoRealEditado = ItemSub.CostoRealEditado
                    .CostoRealImptoEditado = ItemSub.CostoRealImptoEditado
                    .MargenAplicadoEditado = ItemSub.MargenAplicadoEditado
                    .ServicioEditado = ItemSub.ServicioEditado
                    .PlanAlimenticio = ItemSub.PlanAlimenticio
                    .IDReserva_Det = ItemSub.IDReserva_Det
                    .NuViaticosTC = ItemSub.NuViaticosTC
                    .FlServicioIngManual = ItemSub.FlServicioIngManual
                    .UserMod = BE.UserMod
                End With
                ListNewItems.Add(oBEDetCot)
            Next

            'Agregando los nuevos Sub-Servicios como ServiciosReservas
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In ListNewItems
                BE.ListaDetalleCotiz.Add(Item)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarListSubServiciosaServiciosReservasDet(ByRef BE As clsCotizacionBE)
        If BE.ListaDetalleCotiz Is Nothing Then Exit Sub
        Try

            Dim ListNewItems As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Dim ListTempNewItems As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
DeleteItem:
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                If Not (blnExisteAlojamiento(Item.IDProveedor, BE.IdCab) Or _
                       (Item.IDProveedor = gstrIDProveedorSetoursLima Or Item.IDProveedor = gstrIDProveedorSetoursCusco Or _
                        Item.IDProveedor = gstrIDProveedorSetoursBuenosAires Or Item.IDProveedor = gstrIDProveedorSetoursSantiago)) And _
                        (Item.IDDetRel.Trim() = "0" Or Item.IDDetRel.Trim() = "") Then
                    ListTempNewItems.Add(Item)
                    BE.ListaDetalleCotiz.Remove(Item)
                    GoTo DeleteItem
                End If
            Next

            'Reemplazar los datos de servicios x los del Sub-Servicio
            Dim oBEDetCot As clsCotizacionBE.clsDetalleCotizacionBE = Nothing
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In ListTempNewItems
                Dim ListServxSubServ As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = oDetSbDetConsultarxList(Item.IDCab, Item.IDDet, True, False, False)
                For Each ItemSub As clsCotizacionBE.clsDetalleCotizacionBE In ListServxSubServ
                    oBEDetCot = New clsCotizacionBE.clsDetalleCotizacionBE
                    With oBEDetCot
                        .IDDet = ItemSub.IDDet
                        .NroPax = ItemSub.NroPax
                        .NroLiberados = ItemSub.NroLiberados
                        .Tipo_Lib = ItemSub.Tipo_Lib
                        .CostoLiberado = ItemSub.CostoLiberado
                        .CostoLiberadoImpto = ItemSub.CostoLiberadoImpto
                        .CostoReal = ItemSub.CostoReal
                        .CostoRealAnt = ItemSub.CostoRealAnt
                        .CostoRealImpto = ItemSub.CostoRealImpto
                        .Cotizacion = ItemSub.Cotizacion
                        .Dia = ItemSub.Dia
                        .Especial = ItemSub.Especial
                        .IDCab = ItemSub.IDCab
                        .IDIdioma = ItemSub.IDIdioma
                        .IDProveedor = ItemSub.IDProveedor
                        .IDTipoProv = ItemSub.IDProveedor
                        .Dias = ItemSub.Dias
                        .IDServicio = ItemSub.IDServicio
                        .IDServicio_Det = ItemSub.IDServicio_Det
                        .IDUbigeo = ItemSub.IDUbigeo
                        .Item = ItemSub.Item
                        .Margen = ItemSub.Margen
                        .MargenAplicado = ItemSub.MargenAplicado
                        .MargenAplicadoAnt = ItemSub.MargenAplicadoAnt
                        .MargenImpto = ItemSub.MargenImpto
                        .MargenLiberado = ItemSub.MargenLiberado
                        .MargenLiberadoImpto = ItemSub.MargenLiberadoImpto
                        .MotivoEspecial = ItemSub.MotivoEspecial
                        .RutaDocSustento = ItemSub.RutaDocSustento
                        .Desayuno = ItemSub.Desayuno
                        .Lonche = ItemSub.Lonche
                        .Almuerzo = ItemSub.Almuerzo
                        .Cena = ItemSub.Cena
                        .Transfer = ItemSub.Transfer
                        .IDDetTransferOri = ItemSub.IDDetTransferOri
                        .IDDetTransferDes = ItemSub.IDDetTransferDes
                        .TipoTransporte = ItemSub.TipoTransporte
                        .IDUbigeoOri = ItemSub.IDUbigeoOri
                        .IDUbigeoDes = ItemSub.IDUbigeoDes
                        .Servicio = ItemSub.Servicio
                        .SSCL = ItemSub.SSCL
                        .SSCLImpto = ItemSub.SSCLImpto
                        .SSCR = ItemSub.SSCR
                        .SSCRImpto = ItemSub.SSCRImpto
                        .SSMargen = ItemSub.SSMargen
                        .SSMargenImpto = ItemSub.SSMargenImpto
                        .SSMargenLiberado = ItemSub.SSMargenLiberado
                        .SSMargenLiberadoImpto = ItemSub.SSMargenLiberadoImpto
                        .SSTotal = ItemSub.SSTotal
                        .SSTotalOrig = ItemSub.SSTotalOrig
                        .STCR = ItemSub.STCR
                        .STCRImpto = ItemSub.STCRImpto
                        .STMargen = ItemSub.STMargen
                        .STMargenImpto = ItemSub.STMargenImpto
                        .STTotal = ItemSub.STTotal
                        .STTotalOrig = ItemSub.STTotalOrig
                        .Total = ItemSub.Total
                        .RedondeoTotal = ItemSub.RedondeoTotal
                        .TotalOrig = ItemSub.TotalOrig
                        .TotImpto = ItemSub.TotImpto
                        .IDDetRel = ItemSub.IDDetRel
                        .IDDetCopia = ItemSub.IDDetCopia
                        .PorReserva = ItemSub.PorReserva
                        .ExecTrigger = ItemSub.ExecTrigger
                        .IDReserva = ItemSub.IDReserva
                        .IncGuia = ItemSub.IncGuia
                        .FueradeHorario = ItemSub.FueradeHorario
                        .ConAlojamiento = ItemSub.ConAlojamiento
                        .CostoRealEditado = ItemSub.CostoRealEditado
                        .CostoRealImptoEditado = ItemSub.CostoRealImptoEditado
                        .MargenAplicadoEditado = ItemSub.MargenAplicadoEditado
                        .ServicioEditado = ItemSub.ServicioEditado
                        .PlanAlimenticio = ItemSub.PlanAlimenticio
                        .IDReserva_Det = ItemSub.IDReserva_Det
                        .UserMod = BE.UserMod
                    End With
                    ListNewItems.Add(oBEDetCot)
                Next
            Next

            'For Each iTEM As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
            '    If iTEM.IDDet = 457699 Then
            '        Dim N As Integer = 0
            '    End If
            'Next

            'Agregando los nuevos Sub-Servicios como ServiciosReservas
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In ListNewItems
                BE.ListaDetalleCotiz.Add(Item)
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function dtConsultarSubServiciosxIDDet(ByVal vintIDCab As Integer, ByVal vintIDDet As Integer, ByVal vblnConCopias As Boolean, _
                                                   ByVal vblnRecalculo As Boolean, ByVal vblnConReservaDet As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCab)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@ConCopias", vblnConCopias)
            dc.Add("@SoloparaRecalcular", vblnRecalculo)
            dc.Add("@ConIDReserva_Det", vblnConReservaDet)

            Return objADO.GetDataTable("COTIDET_DETSERVICIOS_Sel_xIDCab", dc)
        Catch ex As Exception
            Throw 'New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".dtConsultarSubServiciosxIDDet")
        End Try
    End Function

    Private Function dtConsultarViaticosTourConductor(ByVal vintIDCab As Integer) As System.Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("VIATICOS_TOURCONDUCTOR_SelxIDCabInReservasDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function oDetViaticosTourConductorList(ByVal vintIDCab As Integer) As List(Of clsCotizacionBE.clsDetalleCotizacionBE)
        Try
            Dim dtSubServ As DataTable = dtConsultarViaticosTourConductor(vintIDCab)
            Dim oDetCot As clsCotizacionBE.clsDetalleCotizacionBE = Nothing
            Dim ListDetalleCotiz As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            For Each dr As DataRow In dtSubServ.Rows
                oDetCot = New clsCotizacionBE.clsDetalleCotizacionBE
                With oDetCot
                    .IDDet = dr("IDDet")
                    .NroPax = dr("NroPax")
                    .NroLiberados = If(IsDBNull(dr("NroLiberados")), 0, dr("NroLiberados"))
                    .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib"))
                    .CostoLiberado = dr("CostoLiberado")
                    .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                    .CostoReal = dr("CostoReal")
                    .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")), 0, dr("CostoRealAnt"))
                    .CostoRealImpto = dr("CostoRealImpto")
                    .Cotizacion = dr("Cotizacion")
                    .Dia = dr("Dia")
                    .Especial = dr("Especial")
                    .IDCab = vintIDCab
                    .IDIdioma = dr("IDidioma")
                    .IDProveedor = dr("IDProveedor")
                    .IDTipoProv = dr("IDTipoProv")
                    .Dias = dr("Dias")
                    .IDServicio = dr("IDServicio")
                    .IDServicio_Det = dr("IDServicio_Det")
                    .IDUbigeo = dr("IDubigeo")
                    .Item = dr("Item")
                    .Margen = dr("Margen")
                    .MargenAplicado = dr("MargenAplicado")
                    .MargenAplicadoAnt = If(IsDBNull(dr("MargenAplicadoAnt")), 0, dr("MargenAplicadoAnt"))
                    .MargenImpto = dr("MargenImpto")
                    .MargenLiberado = dr("MargenLiberado")
                    .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                    .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")), "", dr("MotivoEspecial"))
                    .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")), "", dr("RutaDocSustento"))
                    .Desayuno = dr("Desayuno")
                    .Lonche = dr("Lonche")
                    .Almuerzo = dr("Almuerzo")
                    .Cena = dr("Cena")
                    .Transfer = dr("Transfer")
                    .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")), 0, dr("IDDetTransferOri"))
                    .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")), 0, dr("IDDetTransferDes"))
                    .TipoTransporte = If(IsDBNull(dr("TipoTransporte")), "", dr("TipoTransporte"))
                    .IDUbigeoOri = dr("IDUbigeoOri")
                    .IDUbigeoDes = dr("IDUbigeoDes")
                    .Servicio = If(IsDBNull(dr("Servicio")), "", dr("Servicio"))
                    .SSCL = dr("SSCL")
                    .SSCLImpto = dr("SSCLImpto")
                    .SSCR = dr("SSCR")
                    .SSCRImpto = dr("SSCRImpto")
                    .SSMargen = dr("SSMargen")
                    .SSMargenImpto = dr("STMargenImpto")
                    .SSMargenLiberado = dr("SSMargenLiberado")
                    .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                    .SSTotal = dr("SSTotal")
                    .SSTotalOrig = dr("SSTotalOrig")
                    .STCR = dr("STCR")
                    .STCRImpto = dr("STCRImpto")
                    .STMargen = dr("STMargen")
                    .STMargenImpto = dr("STMargenImpto")
                    .STTotal = dr("STTotal")
                    .STTotalOrig = dr("STTotalOrig")
                    .Total = dr("Total")
                    .RedondeoTotal = dr("RedondeoTotal")
                    .TotalOrig = dr("TotalOrig")
                    .TotImpto = dr("TotImpto")
                    .IDDetRel = If(IsDBNull(dr("IDDetRel")), 0, dr("IDDetRel"))
                    .IDDetCopia = If(IsDBNull(dr("IDDetCopia")), 0, dr("IDDetCopia"))
                    .PorReserva = dr("PorReserva")
                    .ExecTrigger = dr("ExecTrigger")
                    .IDReserva = dr("IDReserva")
                    .IncGuia = dr("IncGuia")
                    .FueradeHorario = dr("FueradeHorario")
                    .ConAlojamiento = dr("ConAlojamiento")
                    .CostoRealEditado = dr("CostoRealEditado")
                    .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                    .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                    .ServicioEditado = dr("ServicioEditado")
                    .PlanAlimenticio = dr("PlanAlimenticio")
                    .IDReserva_Det = dr("IDReserva_Det")
                    .NuViaticosTC = dr("NuViaticoTC")
                    .FlServicioIngManual = True
                End With
                ListDetalleCotiz.Add(oDetCot)
            Next
            Return ListDetalleCotiz
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Function oDetSbDetConsultarxList(ByVal vintIDCab As Integer, ByVal vintIDDet As Integer, ByVal vblnConCopias As Boolean, _
                                      ByVal vblnRecalculo As Boolean, ByVal vblnConReservadDet As Boolean) _
        As List(Of clsCotizacionBE.clsDetalleCotizacionBE)
        Try
            Dim dtSubServ As DataTable = dtConsultarSubServiciosxIDDet(vintIDCab, vintIDDet, vblnConCopias, vblnRecalculo, vblnConReservadDet)
            Dim oDetCot As clsCotizacionBE.clsDetalleCotizacionBE = Nothing
            Dim ListDetalleCotiz As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            For Each dr As DataRow In dtSubServ.Rows
                oDetCot = New clsCotizacionBE.clsDetalleCotizacionBE
                With oDetCot
                    .IDDet = dr("IDDet")
                    .NroPax = dr("NroPax")
                    .NroLiberados = If(IsDBNull(dr("NroLiberados")), 0, dr("NroLiberados"))
                    .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib"))
                    .CostoLiberado = dr("CostoLiberado")
                    .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                    .CostoReal = dr("CostoReal")
                    .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")), 0, dr("CostoRealAnt"))
                    .CostoRealImpto = dr("CostoRealImpto")
                    .Cotizacion = dr("Cotizacion")
                    .Dia = dr("Dia")
                    .Especial = dr("Especial")
                    .IDCab = vintIDCab
                    .IDIdioma = dr("IDidioma")
                    .IDProveedor = dr("IDProveedor")
                    .IDTipoProv = dr("IDTipoProv")
                    .Dias = dr("Dias")
                    .IDServicio = dr("IDServicio")
                    .IDServicio_Det = dr("IDServicio_Det")
                    .IDUbigeo = dr("IDubigeo")
                    .Item = dr("Item")
                    .Margen = dr("Margen")
                    .MargenAplicado = dr("MargenAplicado")
                    .MargenAplicadoAnt = If(IsDBNull(dr("MargenAplicadoAnt")), 0, dr("MargenAplicadoAnt"))
                    .MargenImpto = dr("MargenImpto")
                    .MargenLiberado = dr("MargenLiberado")
                    .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                    .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")), "", dr("MotivoEspecial"))
                    .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")), "", dr("RutaDocSustento"))
                    .Desayuno = dr("Desayuno")
                    .Lonche = dr("Lonche")
                    .Almuerzo = dr("Almuerzo")
                    .Cena = dr("Cena")
                    .Transfer = dr("Transfer")
                    .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")), 0, dr("IDDetTransferOri"))
                    .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")), 0, dr("IDDetTransferDes"))
                    .TipoTransporte = If(IsDBNull(dr("TipoTransporte")), "", dr("TipoTransporte"))
                    .IDUbigeoOri = dr("IDUbigeoOri")
                    .IDUbigeoDes = dr("IDUbigeoDes")
                    .Servicio = If(IsDBNull(dr("Servicio")), "", dr("Servicio"))
                    .SSCL = dr("SSCL")
                    .SSCLImpto = dr("SSCLImpto")
                    .SSCR = dr("SSCR")
                    .SSCRImpto = dr("SSCRImpto")
                    .SSMargen = dr("SSMargen")
                    .SSMargenImpto = dr("STMargenImpto")
                    .SSMargenLiberado = dr("SSMargenLiberado")
                    .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                    .SSTotal = dr("SSTotal")
                    .SSTotalOrig = dr("SSTotalOrig")
                    .STCR = dr("STCR")
                    .STCRImpto = dr("STCRImpto")
                    .STMargen = dr("STMargen")
                    .STMargenImpto = dr("STMargenImpto")
                    .STTotal = dr("STTotal")
                    .STTotalOrig = dr("STTotalOrig")
                    .Total = dr("Total")
                    .RedondeoTotal = dr("RedondeoTotal")
                    .TotalOrig = dr("TotalOrig")
                    .TotImpto = dr("TotImpto")
                    .IDDetRel = If(IsDBNull(dr("IDDetRel")), 0, dr("IDDetRel"))
                    .IDDetCopia = If(IsDBNull(dr("IDDetCopia")), 0, dr("IDDetCopia"))
                    '.Recalcular = False
                    .PorReserva = dr("PorReserva")
                    .ExecTrigger = dr("ExecTrigger")
                    .IDReserva = dr("IDReserva") 'If(vintIDReserva = 0, dr("IDReserva"), vintIDReserva)
                    '.Reservado = dr("Reservado")
                    '.ExecTrigger = dr("ExecTrigger")
                    .IncGuia = dr("IncGuia")
                    .FueradeHorario = dr("FueradeHorario")
                    .ConAlojamiento = dr("ConAlojamiento")
                    .CostoRealEditado = dr("CostoRealEditado")
                    .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                    .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                    .ServicioEditado = dr("ServicioEditado")
                    .PlanAlimenticio = dr("PlanAlimenticio")
                    '.ActualizQuiebre = dr("ActualizQuiebre")
                    .IDReserva_Det = dr("IDReserva_Det")
                End With
                ListDetalleCotiz.Add(oDetCot)
            Next

            Return ListDetalleCotiz
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteAlojamiento(ByVal vstrIDProveedor As String, ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExiste As Boolean = False
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pExisteAlojamiento", False)

            blnExiste = objADO.GetSPOutputValue("COTIDET_SelExisteAlojamiento", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub EnviarCorreoINC(ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            objADO.ExecuteSP("EnvioCorreoEntradasMPWPAprobadas", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EnviarCorreoINC")
        End Try
    End Sub
    Private Sub AnularReservasxEliminacionTotaldeVentas(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("RESERVAS_AnuxElimDeVentas", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AnularReservasxEliminacionTotaldeVentas")
        End Try
    End Sub

    Public Function blnExistenOperacionesProvExternosxIDCab(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("OPERACIONES_Sel_ExistenProvExternosxIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPkCoti(ByVal vintIDCab As Int32) As DataTable 'SqlClient.SqlDataReader
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)

            Dim strSql As String = "COTICAB_Sel_Pk"
            Return objADO.GetDataTable(strSql, dc)
        Catch ex As Exception
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ConsultarPkCoti")
        End Try
    End Function

    Private Sub RegenerarCambiosAcomodos(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
        Try
            Dim objResBT As New clsReservaBT
            Dim objOpeBT As New clsOperacionBT
            'Dim dttProv As DataTable = objResBT.ConsultarProveedoresHotelesyAlojamiento(vintIDCab)
            Dim dttProv As DataTable = objResBT.ConsultarProveedores(vintIDCab)

            Dim objOpeBN As New clsOperacionBN
            Dim blnOpe As Boolean = objOpeBN.blnExistenOperacionesxIDCab(vintIDCab)

            Dim ListProveedoresReservas As New List(Of stProveedoresReservas)

            For Each dr As DataRow In dttProv.Rows
                Dim objSTRes As New stProveedoresReservas With {.IDProveedor = dr("IDProveedor"), _
                                                                .CodReserva = dr("CodReservaProv"), _
                                                                .Observaciones = dr("Observaciones")}
                ListProveedoresReservas.Add(objSTRes)
                objResBT.RegenerarReserva(dr("IDReserva"), dr("IDProveedor"), vintIDCab, vstrUserMod, True)
                If blnOpe Then
                    Dim intIDOperacion As Integer = objOpeBT.intIDOperacionxIDReserva(dr("IDReserva"))
                    If intIDOperacion <> 0 Then
                        objOpeBT.RegenerarOperacion(intIDOperacion, _
                                                    dr("IDProveedor"), vintIDCab, vstrUserMod)
                    End If
                End If

                'objOpeBT.EliminarLogReservas(vintIDCab, dr("IDProveedor"), "B")
            Next





            For Each ST As stProveedoresReservas In ListProveedoresReservas
                objResBT.ActualizarCodReservaProvxProveedor(vintIDCab, ST.IDProveedor, _
                                                            ST.CodReserva, _
                                                            ST.Observaciones, _
                                                            vstrUserMod)
                objResBT.ActualizarEstadoxProveedor(vintIDCab, ST.IDProveedor, "RQ", vstrUserMod)

                objOpeBT.ActualizarEstadoxProveedor(vintIDCab, ST.IDProveedor, "SM", vstrUserMod)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    ' ''Private Sub RegenerarCambiosAcomodos2(ByVal vintIDCab As Integer, ByVal vstrUserMod As String)
    ' ''    Try
    ' ''        Dim objResBT As New clsReservaBT
    ' ''        Dim objOpeBT As New clsOperacionBT
    ' ''        'Dim dttProv As DataTable = objResBT.ConsultarProveedoresHotelesyAlojamiento(vintIDCab)
    ' ''        Dim dttProv As DataTable = objResBT.ConsultarProveedores(vintIDCab)

    ' ''        ''Dim objOpeBN As New clsOperacionBN
    ' ''        ''Dim blnOpe As Boolean = objOpeBN.blnExistenOperacionesxIDCab(vintIDCab)

    ' ''        ''Dim ListProveedoresReservas As New List(Of stProveedoresReservas)

    ' ''        For Each dr As DataRow In dttProv.Rows

    ' ''            If dr("IDReservaProtecc") = 0 Then
    ' ''                ''objResBT.RegenerarReserva_TMP(dr("IDReserva"), dr("IDProveedor"), vintIDCab, vstrUserMod)

    ' ''            End If


    ' ''        Next




    ' ''        ' Proveedores de Proteccion
    ' ''        For Each dr As DataRow In dttProv.Rows

    ' ''            If dr("IDReservaProtecc") <> 0 Then
    ' ''                ''objResBT.RegenerarReserva_TMP(dr("IDReservaProtecc"), dr("IDProveedorProtecc"), vintIDCab, vstrUserMod)

    ' ''            End If
    ' ''        Next

    ' ''        ContextUtil.SetComplete()
    ' ''    Catch ex As Exception
    ' ''        ContextUtil.SetAbort()
    ' ''        Throw
    ' ''    End Try
    ' ''End Sub

    Public Function blnExistexProveedor(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("COTIDET_SelxIDProveedorExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub Actualizar(ByVal vintIDCab As Integer, ByVal vstrObservacion As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCAB", vintIDCab)
                .Add("@Observaciones", vstrObservacion)
            End With

            objADO.ExecuteSP("COTICAB_UpdObserv", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")

        End Try
    End Sub

    Public Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsCotizacionBE, ByVal vstrAntiguoID As String, _
                                                           ByVal vintNuevoID As Integer)
        Try

            If Not BE.ListaPax Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                    If ItemDet.IDCab = vstrAntiguoID Then
                        ItemDet.IDCab = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaCotizQuiebre Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsCotizacionQuiebreBE In BE.ListaCotizQuiebre
                    If ItemDet.IDCab = vstrAntiguoID Then
                        ItemDet.IDCab = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaCotizacionVuelo Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                    If ItemDet.IdCab = vstrAntiguoID Then
                        ItemDet.IdCab = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaTipoCambios Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsCotizacionTiposCambioBE In BE.ListaTipoCambios
                    If ItemDet.IDCab = vstrAntiguoID Then
                        ItemDet.IDCab = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaTourConductor Is Nothing Then
                For Each Item As clsCotizacionBE.clsTourConductorBE In BE.ListaTourConductor
                    If Item.IdCab = vstrAntiguoID Then
                        Item.IdCab = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaCostosTourConductor Is Nothing Then
                For Each Item As clsCotizacionBE.clsCostosTourConductorBE In BE.ListaCostosTourConductor
                    If Item.IdCab = vstrAntiguoID Then
                        Item.IdCab = vintNuevoID
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarIDDetTransfer(ByVal BE As clsCotizacionBE)
        Try
            Dim dc As Dictionary(Of String, String)

            For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz

                If ItemCot.IDDetTransferOri <> "0" Then
                    If Not IsNumeric(ItemCot.IDDetTransferOri) Then
                        For Each ItemCot2 As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                            'If ItemCot2.IDDetTransferOri <> "0" Then
                            If Not ItemCot2.IDDetMem Is Nothing Then
                                If Mid(ItemCot.IDDetTransferOri, 2) = Mid(ItemCot2.IDDetMem, 2) Then
                                    ItemCot.IDDetTransferOri = ItemCot2.IDDet

                                    dc = New Dictionary(Of String, String)
                                    dc.Add("@IDDet", ItemCot.IDDet)
                                    dc.Add("@IDDetTransferOri", ItemCot.IDDetTransferOri)
                                    objADO.ExecuteSP("COTIDET_Upd_TransferOri", dc)

                                    Exit For
                                End If
                                'End If
                            End If
                        Next
                    End If
                End If
            Next



            For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz

                If ItemCot.IDDetTransferDes <> "0" Then
                    If Not IsNumeric(ItemCot.IDDetTransferDes) Then
                        For Each ItemCot2 As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                            'If ItemCot2.IDDetTransferOri <> "0" Then
                            If Not ItemCot2.IDDetMem Is Nothing Then
                                If Mid(ItemCot.IDDetTransferDes, 2) = Mid(ItemCot2.IDDetMem, 2) Then
                                    ItemCot.IDDetTransferDes = ItemCot2.IDDet

                                    dc = New Dictionary(Of String, String)
                                    dc.Add("@IDDet", ItemCot.IDDet)
                                    dc.Add("@IDDetTransferDes", ItemCot.IDDetTransferDes)
                                    objADO.ExecuteSP("COTIDET_Upd_TransferDes", dc)

                                    Exit For
                                End If
                                'End If
                            End If
                        Next
                    End If
                End If
            Next


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarIDDetTransfer")

        End Try
    End Sub
    Private Function dttCreaDttDetCotiServ() As DataTable

        Dim dttDetCotiServ As New DataTable("DetCotiServ")
        With dttDetCotiServ
            .Columns.Add("IDDet")
            .Columns.Add("IDServicio")
            .Columns.Add("IDServicio_Det")
            .Columns.Add("CostoReal")
            .Columns.Add("Margen")
            .Columns.Add("CostoLiberado")
            .Columns.Add("MargenLiberado")
            .Columns.Add("MargenAplicado")
            .Columns.Add("Total")
            .Columns.Add("RedondeoTotal")
            .Columns.Add("SSCR")
            .Columns.Add("SSMargen")
            .Columns.Add("SSCL")
            .Columns.Add("SSML")
            .Columns.Add("SSTotal")
            .Columns.Add("STCR")
            .Columns.Add("STMargen")
            .Columns.Add("STTotal")

            .Columns.Add("CostoRealImpto")
            .Columns.Add("CostoLiberadoImpto")
            .Columns.Add("MargenImpto")
            .Columns.Add("MargenLiberadoImpto")
            .Columns.Add("SSCRImpto")
            .Columns.Add("SSCLImpto")
            .Columns.Add("SSMargenImpto")
            .Columns.Add("SSMargenLiberadoImpto")
            .Columns.Add("STCRImpto")
            .Columns.Add("STMargenImpto")
            .Columns.Add("TotImpto")
        End With


        Return dttDetCotiServ
    End Function
    Private Function blnListaTieneCambio(ByVal vstrIDDet As String, ByVal vListCotiDet As List(Of clsCotizacionBE.clsDetalleCotizacionBE)) As Boolean
        Try
            For Each ItemCotiDet As clsCotizacionBE.clsDetalleCotizacionBE In vListCotiDet
                ' ''If Not ItemCotiDet.FlServicioCopiadoVarios Then
                If vstrIDDet = ItemCotiDet.IDDet And ItemCotiDet.Recalcular = True Then
                    Return True
                End If
                If vstrIDDet = ItemCotiDet.IDDet Then
                    If ItemCotiDet.Accion = "M" Then 'And ItemCotiDet.DifPax = False Then
                        Return True
                    End If
                End If
                ' ''End If
            Next

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function intIDDetListaRecalculoAcVe(ByVal vintIDDet As Integer, ByVal vListCotiDet As List(Of clsCotizacionBE.clsDetalleCotizacionBE)) As Integer
        Try
            For Each ItemCotiDet As clsCotizacionBE.clsDetalleCotizacionBE In vListCotiDet
                If vintIDDet = ItemCotiDet.IDDet Then 'And ItemCotiDet.Recalcular = True Then
                    Return vintIDDet
                End If
            Next

            Return False
        Catch ex As Exception
            Throw
        End Try
    End Function

    'Public Sub RecalculoPuntual() 'metodo para proceso puntual
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        Dim dtt As DataTable = objADO.GetDataTable("COTIDET_SelInsertadosTI", dc)
    '        Dim objBTDet As New clsDetalleVentasCotizacionBT
    '        For Each dr As DataRow In dtt.Rows
    '            Dim BE As New clsCotizacionBE
    '            Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
    '            Dim ItemCotiDet As New clsCotizacionBE.clsDetalleCotizacionBE

    '            Dim dcDet As New Dictionary(Of String, String)
    '            dcDet.Add("@IDCab", dr("IDCab"))
    '            ''Dim dttDet As DataTable = objADO.GetDataTable("COTIDET_SelInsertadosTIxIDCab", dcDet)
    '            Dim dttDet As DataTable = objADO.GetDataTable("COTIDET_SelCOLCAxIDCabInsertadosTI", dcDet)
    '            For Each drDet As DataRow In dttDet.Rows


    '                ItemCotiDet.IDDet = drDet("IDDet")
    '                ListaCotiDet.Add(ItemCotiDet)
    '                BE.ListaDetalleCotiz = ListaCotiDet
    '                BE.IdCab = dr("IDCab")
    '                Dim strTipoPax As String = objBTDet.strDevuelveTipoPaxNacionalidad(BE)


    '                ReCalculoCotizacion(dr("IDCab"), dr("NroPax"), _
    '                                    If(IsDBNull(dr("NroLiberados")), "0", dr("NroLiberados")), _
    '                                    If(IsDBNull(dr("Tipo_Lib")), "", dr("Tipo_Lib")), _
    '                                    strTipoPax, dr("Redondeo"), 18, "M", "0001", New List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE), _
    '                                    dr("MargenCero"), New List(Of clsCotizacionBE.clsDetalleCotizacionBE))

    '            Next
    '        Next
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub

    'Public Sub RecalculoPuntual2() 'metodo para proceso puntual
    '    Try
    '        Dim dc As New Dictionary(Of String, String)
    '        Dim dtt As DataTable = objADO.GetDataTable("COTIDET_SelInsertadosTI", dc)

    '        Dim obj As New clsPaxVentasCotizacionBT
    '        For Each dr As DataRow In dtt.Rows
    '            obj.ActualizarTotales(0, dr("IDCab"), 0, True, "003", "0001")
    '        Next


    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw


    '    End Try
    'End Sub

    Private Function dcDevuelveValoresTCxIDDet(ByVal vstrIDDet As Integer, ByVal vListCotiDet As List(Of clsCotizacionBE.clsDetalleCotizacionBE)) As Dictionary(Of String, Double)
        Try
            Dim dicValoresTC As Dictionary(Of String, Double) = Nothing
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In vListCotiDet
                If Item.IDDet = vstrIDDet Then
                    dicValoresTC = Item.dicValoresTC
                    Exit For
                End If
            Next
            Return dicValoresTC
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub ReCalculoCotizacion(ByVal vintIDCab As Int32, _
                           ByVal vintPax As Int16, ByVal vintLiberados As Int16, _
                           ByVal vstrTipo_Lib As Char, ByVal vstrTipoPax As String, _
                           ByVal vdecRedondeo As Single, _
                           ByVal vsglIGV As Single, ByVal vstrAccion As Char, ByVal vstrUser As String, _
                           ByVal vListaDetPax As List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE), ByVal vblnMargenCero As Boolean, _
                           ByVal vstrIDTipoHonorario As String, _
                           ByVal vListCotiDet As List(Of clsCotizacionBE.clsDetalleCotizacionBE), Optional ByVal vblnAcomVehi As Boolean = False)
        Try
            'Dim objCotiDet As New clsCotizacionBN.clsDetalleCotizacionBN
            Dim objCotiDet As New clsDetalleVentasCotizacionBT

            Dim objCoti As New clsCotizacionBN
            Dim objBE As New clsCotizacionBE
            'Dim drCotiDetQ As SqlClient.SqlDataReader
            'Dim objBLDetQ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
            Dim dttCotiDet As DataTable = objCotiDet.ConsultarListxIDCab(vintIDCab, True, False, False)
            Dim objBEDet As clsCotizacionBE.clsDetalleCotizacionBE
            Dim ListaDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Dim dc As Dictionary(Of String, Double)
            Dim dttDetCotiServ As DataTable
            Dim dblCostoReal As Double, dblMargenAplicado As Double
            Dim dblSSCR As Double, dblSTCR As Double
            'Dim objADODN As New clsDataAccessDN
            'Dim cnn As SqlClient.SqlConnection = objADODN.GetConnection
            'Dim cnn2 As SqlClient.SqlConnection = objADODN.GetConnection
            'Dim cnn As SqlClient.SqlConnection = objADO.GetConnection
            'Dim cnn2 As SqlClient.SqlConnection = objADO.GetConnection
            Dim cnn As SqlClient.SqlConnection = Nothing
            Dim cnn2 As SqlClient.SqlConnection = Nothing

            For Each drCotiDet As DataRow In dttCotiDet.Rows
                Dim intIDDetAcVe As Integer = 0
                If Not vblnAcomVehi Then
                    Dim blnUpdCalculoxAnio As Boolean = Convert.ToBoolean(drCotiDet("FlServicioUpdxAnio"))
                    If Not blnListaTieneCambio(drCotiDet("IDDet").ToString, vListCotiDet) And Not blnUpdCalculoxAnio Then
                        GoTo Sgte
                    End If

                    If drCotiDet("IDDetRel") <> 0 Then 'Or drCotiDet("IDDetCopia") <> 0 Then
                        GoTo Sgte
                    End If
                Else
                    intIDDetAcVe = intIDDetListaRecalculoAcVe(drCotiDet("IDDet").ToString, vListCotiDet)
                    If intIDDetAcVe = 0 Then GoTo Sgte
                End If
                'If drCotiDet("IDDet") = 4618 Then
                '    dblCostoReal = -1
                'End If

                dblCostoReal = -1
                dblMargenAplicado = -1
                dblSSCR = -1
                dblSTCR = -1
                If drCotiDet("Especial") = True Then
                    If drCotiDet("CostoRealEditado") = True Then
                        dblCostoReal = drCotiDet("CostoReal")
                    End If
                    If drCotiDet("MargenAplicadoEditado") = True Then
                        dblMargenAplicado = drCotiDet("MargenAplicado")
                    End If
                    If drCotiDet("FlSSCREditado") = True Then
                        dblSSCR = drCotiDet("SSCR")
                    End If
                    If drCotiDet("FlSTCREditado") = True Then
                        dblSTCR = drCotiDet("STCR")
                    End If
                End If

                dttDetCotiServ = dttCreaDttDetCotiServ()

                Dim strIDTipoProv As String = drCotiDet("IDTipoProv").ToString


                If drCotiDet("IncGuia") = True Then
                    strIDTipoProv = "003" 'cualquier tipo q no sea hotel ni restaurant
                Else
                    vintPax = drCotiDet("NroPax").ToString
                End If
                vintLiberados = If(IsDBNull(drCotiDet("NroLiberados")), "0", drCotiDet("NroLiberados").ToString)
                vstrTipo_Lib = If(IsDBNull(drCotiDet("Tipo_Lib")), "", drCotiDet("Tipo_Lib").ToString)

                Dim dcValoresTC As Dictionary(Of String, Double) = Nothing
                If Convert.ToBoolean(drCotiDet("FlServicioTC")) Then
                    dcValoresTC = dcDevuelveValoresTCxIDDet(drCotiDet("IDDet"), vListCotiDet)
                End If
                If Not vblnAcomVehi Then
                    dc = objCoti.CalculoCotizacion(drCotiDet("IDServicio").ToString, drCotiDet("IDServicio_Det").ToString, _
                        drCotiDet("Anio").ToString, drCotiDet("Tipo").ToString, _
                        strIDTipoProv, vintPax, vintLiberados, vstrTipo_Lib, _
                        drCotiDet("MargenCotiCab").ToString, drCotiDet("TipoCambioCotiCab").ToString, _
                        dblCostoReal, dblMargenAplicado, vstrTipoPax, vdecRedondeo, dttDetCotiServ, _
                        vsglIGV, , drCotiDet("IncGuia"), vblnMargenCero, cnn, cnn2, drCotiDet("Dia"), _
                        dblSSCR, dblSTCR, drCotiDet("FlServicioTC"), vstrIDTipoHonorario, dcValoresTC, drCotiDet("FlAcomodosVehic"))
                Else
                    dc = CalculoCotizacion(drCotiDet("IDServicio").ToString, drCotiDet("IDServicio_Det").ToString, _
                        drCotiDet("Anio").ToString, drCotiDet("Tipo").ToString, _
                        strIDTipoProv, vintPax, vintLiberados, vstrTipo_Lib, _
                        drCotiDet("MargenCotiCab").ToString, drCotiDet("TipoCambioCotiCab").ToString, _
                        dblCostoReal, dblMargenAplicado, vstrTipoPax, vdecRedondeo, dttDetCotiServ, _
                        vsglIGV, intIDDetAcVe, drCotiDet("IncGuia"), vblnMargenCero, cnn, drCotiDet("Dia"), _
                        dblSSCR, dblSTCR, drCotiDet("FlServicioTC"), vstrIDTipoHonorario, dcValoresTC, drCotiDet("FlAcomodosVehic"))
                End If
                ListaDetCot = New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
                objBEDet = New clsCotizacionBE.clsDetalleCotizacionBE

                For Each dic As KeyValuePair(Of String, Double) In dc
                    If dic.Key = "CostoReal" Then objBEDet.CostoReal = dic.Value
                    If dic.Key = "Margen" Then objBEDet.Margen = dic.Value
                    If dic.Key = "CostoLiberado" Then objBEDet.CostoLiberado = dic.Value
                    If dic.Key = "MargenLiberado" Then objBEDet.MargenLiberado = dic.Value
                    If dic.Key = "MargenAplicado" Then objBEDet.MargenAplicado = dic.Value
                    If dic.Key = "TotImpto" Then objBEDet.TotImpto = dic.Value
                    If dic.Key = "Total" Then objBEDet.Total = dic.Value
                    If dic.Key = "RedondeoTotal" Then
                        objBEDet.RedondeoTotal = dic.Value
                    End If

                    If dic.Key = "SSCR" Then
                        objBEDet.SSCR = dic.Value
                    End If

                    If dic.Key = "SSMargen" Then objBEDet.SSMargen = dic.Value
                    If dic.Key = "SSCL" Then objBEDet.SSCL = dic.Value
                    If dic.Key = "SSML" Then objBEDet.SSMargenLiberado = dic.Value
                    If dic.Key = "SSTotal" Then objBEDet.SSTotal = dic.Value
                    If dic.Key = "STCR" Then
                        objBEDet.STCR = dic.Value
                    End If

                    If dic.Key = "STMargen" Then objBEDet.STMargen = dic.Value
                    If dic.Key = "STTotal" Then objBEDet.STTotal = dic.Value

                    If dic.Key = "CostoRealImpto" Then objBEDet.CostoRealImpto = dic.Value
                    If dic.Key = "CostoLiberadoImpto" Then objBEDet.CostoLiberadoImpto = dic.Value
                    If dic.Key = "MargenImpto" Then objBEDet.MargenImpto = dic.Value
                    If dic.Key = "MargenLiberadoImpto" Then objBEDet.MargenLiberadoImpto = dic.Value
                    If dic.Key = "SSCLImpto" Then objBEDet.SSCLImpto = dic.Value
                    If dic.Key = "SSCRImpto" Then objBEDet.SSCRImpto = dic.Value
                    If dic.Key = "SSMargenImpto" Then objBEDet.SSMargenImpto = dic.Value
                    If dic.Key = "SSMargenLiberadoImpto" Then objBEDet.SSMargenLiberadoImpto = dic.Value
                    If dic.Key = "STCRImpto" Then objBEDet.STCRImpto = dic.Value
                    If dic.Key = "STMargenImpto" Then objBEDet.STMargenImpto = dic.Value
                Next



                With objBEDet
                    'If vstrAccion = "M" Then
                    '    .IDDet_Q = intConsultarIDDet_QxIDCab_Q(vintIDCab_Q)
                    'End If

                    .Cotizacion = drCotiDet("Cotizacion").ToString
                    .Item = drCotiDet("Item").ToString
                    .Dia = drCotiDet("Dia").ToString
                    .IDUbigeo = drCotiDet("IDUbigeo").ToString
                    .IDProveedor = drCotiDet("IDProveedor").ToString
                    .IDServicio = drCotiDet("IDServicio").ToString
                    .IDServicio_Det = drCotiDet("IDServicio_Det").ToString
                    .IDIdioma = drCotiDet("IDIdioma").ToString
                    .MotivoEspecial = drCotiDet("MotivoEspecial").ToString
                    .RutaDocSustento = drCotiDet("RutaDocSustento").ToString
                    .Desayuno = drCotiDet("Desayuno")
                    .Lonche = drCotiDet("Lonche")
                    .Almuerzo = drCotiDet("Almuerzo")
                    .Cena = drCotiDet("Cena")
                    .Transfer = drCotiDet("Transfer")
                    '.OrigenTransfer = drCotiDet("OrigenTransfer")
                    '.DestinoTransfer = drCotiDet("DestinoTransfer")
                    .IDDetTransferOri = drCotiDet("IDDetTransferOri")
                    .IDDetTransferDes = drCotiDet("IDDetTransferDes")
                    .TipoTransporte = drCotiDet("TipoTransporte")
                    .IDUbigeoOri = drCotiDet("IDUbigeoOri")
                    .IDUbigeoDes = drCotiDet("IDUbigeoDes")
                    .Servicio = drCotiDet("Servicio").ToString
                    .IDDetRel = 0
                    .IDDet = drCotiDet("IDDet").ToString
                    .NroPax = drCotiDet("NroPax").ToString
                    .NroLiberados = If(IsDBNull(drCotiDet("NroLiberados")) = True, "0", drCotiDet("NroLiberados").ToString)
                    .Tipo_Lib = If(IsDBNull(drCotiDet("Tipo_Lib")) = True, "", drCotiDet("Tipo_Lib"))
                    .TotalOrig = drCotiDet("TotalOrig").ToString
                    .SSTotalOrig = drCotiDet("SSTotalOrig").ToString
                    .STTotalOrig = drCotiDet("STTotalOrig").ToString
                    '.IDCab_Q = vintIDCab_Q
                    '.CostoLiberadoImpto = .CostoLiberado * (1 + (vsglIGV / 100))
                    '.CostoRealImpto = .CostoReal * (1 + (vsglIGV / 100))
                    '.MargenImpto = .Margen * (1 + (vsglIGV / 100))
                    '.MargenLiberadoImpto = .MargenLiberado * (1 + (vsglIGV / 100))
                    '.SSCLImpto = .SSCL * (1 + (vsglIGV / 100))
                    '.SSCRImpto = .SSCR * (1 + (vsglIGV / 100))
                    '.SSMargenImpto = .SSMargen * (1 + (vsglIGV / 100))
                    '.SSMargenLiberadoImpto = .SSMargenLiberado * (1 + (vsglIGV / 100))
                    '.STCRImpto = .STCR * (1 + (vsglIGV / 100))
                    '.STMargenImpto = .STMargen * (1 + (vsglIGV / 100))
                    '.TotImpto = .CostoLiberadoImpto + .CostoRealImpto + .MargenImpto + .MargenLiberadoImpto '+ _
                    '.SSCLImpto(+.SSCRImpto + .SSMargenImpto + .SSMargenLiberadoImpto + .STCRImpto + .STMargenImpto)
                    .Especial = drCotiDet("Especial").ToString
                    .CostoRealAnt = 0
                    .MargenAplicadoAnt = 0
                    .UserMod = vstrUser
                    .Recalcular = False
                    .ExecTrigger = drCotiDet("ExecTrigger")
                    .IncGuia = drCotiDet("IncGuia")
                    .FueradeHorario = drCotiDet("FueradeHorario")
                    .CostoRealEditado = drCotiDet("CostoRealEditado")
                    .CostoRealImptoEditado = drCotiDet("CostoRealImptoEditado")
                    .MargenAplicadoEditado = drCotiDet("MargenAplicadoEditado")
                    .ServicioEditado = drCotiDet("ServicioEditado")
                    .AcomodoEspecial = drCotiDet("AcomodoEspecial")
                    .FlSSCREditado = drCotiDet("FlSSCREditado")
                    .FlSTCREditado = drCotiDet("FlSTCREditado")
                    .Accion = vstrAccion
                End With
                ListaDetCot.Add(objBEDet)



                'Det. Cotiz. Serv*****
                Dim objBEDetS As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE
                Dim ListaDetCotS As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE)

                For Each drDetCotiServ As DataRow In dttDetCotiServ.Rows
                    objBEDetS = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE
                    With objBEDetS
                        .IDServicio_Det = drDetCotiServ("IDServicio_Det")
                        .IDDet = drCotiDet("IDDet")
                        .CostoLiberado = drDetCotiServ("CostoLiberado")
                        .CostoLiberadoImpto = drDetCotiServ("CostoLiberadoImpto")
                        .CostoReal = drDetCotiServ("CostoReal")
                        .CostoRealImpto = drDetCotiServ("CostoRealImpto")

                        .Margen = drDetCotiServ("Margen")
                        .MargenAplicado = drDetCotiServ("MargenAplicado")
                        .MargenImpto = drDetCotiServ("MargenImpto")
                        .MargenLiberado = drDetCotiServ("MargenLiberado")
                        .MargenLiberadoImpto = drDetCotiServ("MargenLiberadoImpto")

                        .SSCL = drDetCotiServ("SSCL")
                        .SSCLImpto = drDetCotiServ("SSCLImpto")
                        .SSCR = drDetCotiServ("SSCR")
                        .SSCRImpto = drDetCotiServ("SSCRImpto")
                        .SSMargen = drDetCotiServ("SSMargen")
                        .SSMargenImpto = drDetCotiServ("STMargenImpto")
                        .SSMargenLiberado = drDetCotiServ("SSML")
                        .SSMargenLiberadoImpto = drDetCotiServ("SSMargenLiberadoImpto")
                        .SSTotal = drDetCotiServ("SSTotal")
                        .STCR = drDetCotiServ("STCR")
                        .STCRImpto = drDetCotiServ("STCRImpto")
                        .STMargen = drDetCotiServ("STMargen")
                        .STMargenImpto = drDetCotiServ("STMargenImpto")
                        .STTotal = drDetCotiServ("STTotal")
                        .Total = drDetCotiServ("Total")
                        .RedondeoTotal = drDetCotiServ("RedondeoTotal")
                        .TotImpto = drDetCotiServ("TotImpto")
                        .UserMod = vstrUser

                    End With
                    ListaDetCotS.Add(objBEDetS)
                Next

                'objBE = New clsCotizacionBE
                objBE.ListaDetalleCotizacionDetServicio = ListaDetCotS
                objBE.ListaDetalleCotiz = ListaDetCot

                'If Not vListaDetPax Is Nothing Then
                Dim intIDDet As Integer = Convert.ToInt64(drCotiDet("IDDet"))
                'Dim TmpListaPaxXDet = (From Pax In vListaDetPax _
                '                        Where Pax.IDDet = intIDDet.ToString()).ToList()
                Dim dttListPax As DataTable = ConsultarPaxXIDDetRecalculo(intIDDet)
                Dim TmpListaPaxXDet As New List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE)
                Dim oBEDetPax As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE
                For Each ItemRow As DataRow In dttListPax.Rows
                    oBEDetPax = New clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE
                    With oBEDetPax
                        .IDDet = ItemRow("IDDet")
                        .IDPax = ItemRow("IDPax")
                        .NoShow = ItemRow("FlDesactNoShow")
                        .SSTotal = ItemRow("SSTotal")
                        .STTotal = ItemRow("STTotal")
                        .UserMod = vstrUser
                    End With
                    TmpListaPaxXDet.Add(oBEDetPax)
                Next

                objBE.ListaDetPax = TmpListaPaxXDet
                objBE.UserMod = vstrUser
                objBE.IdCab = vintIDCab

                'End If
                objBE.ListaDetalleCotizacionQuiebres = Nothing
                objCotiDet.InsertarActualizarEliminar(objBE)
Sgte:
            Next
            'If cnn.State = ConnectionState.Open Then cnn.Close()
            'If cnn2.State = ConnectionState.Open Then cnn2.Close()

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ReCalculoCotizacion")

        End Try
    End Sub

    Private Function ConsultarPaxXIDDetRecalculo(ByVal vintIDDet As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)

            Return objADO.GetDataTable("COTIDET_PAX_SelxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function CalculoCotizacion(ByVal vstrIDServicio As String, _
                                  ByVal vintIDServicio_Det As Int32, ByVal vstrAnio As String, ByVal vstrTipo As String, _
                                  ByVal vstrTipoProv As String, ByVal vintPax As Int16, _
                                  ByVal vintLiberados As Int16, ByVal vstrTipo_Lib As Char, _
                                  ByVal vdblMargenCotiCab As Double, ByVal vdblTCambioCotiCab As Single, _
                                  ByVal vdblCostoReal As Double, ByVal vdblMargenAplicado As Double, ByVal vstrTipoPax As String, _
                                  ByVal vsglRedondeo As Single, _
                                  ByRef rdttDetCotiServ As DataTable, _
                                  Optional ByVal vsglIGV As Single = 0, _
                                  Optional ByVal vstrIDDet_DetCotiServ As String = "", _
                                  Optional ByVal vblnIncGuia As Boolean = False, _
                                  Optional ByVal vblnMargenCero As Boolean = False, _
                                  Optional ByVal vCnn As SqlClient.SqlConnection = Nothing, _
                                  Optional ByVal vdatFechaServicio As Date = #1/1/1900#, _
                                  Optional ByVal vdblSSCR As Double = -1, _
                                  Optional ByVal vdblSTCR As Double = -1, _
                                  Optional ByVal vblnServicioTC As Boolean = False, _
                                  Optional ByVal vstrIDTipoHonorario As String = "", _
                                  Optional ByVal vdicValoresTC As Dictionary(Of String, Double) = Nothing, _
                                  Optional ByVal vblnAcomVehic As Boolean = False) As Dictionary(Of String, Double)

        'ByVal vintIDServicio_Det As Int32, ByVal vstrAnio As String, ByVal vstrTipo As String, _
        Try
            Dim dblCostoReal As Double = 0, dblMargen As Double = 0, dblMargenAplicado As Double = 0
            Dim dblCostoLiberado As Double = 0, dblMargenLiberado As Double = 0, dblTotal As Decimal = 0, dblTotImpto As Double = 0

            Dim dblSSCR As Double = 0, dblSSMargen As Double = 0, dblSSCL As Double = 0, dblSSML As Double = 0
            Dim dblSSTotal As Double = 0

            Dim dblSTCR As Double = 0, dblSTMargen As Double = 0, dblSTTotal As Decimal = 0


            Dim dblCostoRealImpto As Double = 0
            Dim dblCostoLiberadoImpto As Double = 0
            Dim dblMargenImpto As Double = 0
            Dim dblMargenLiberadoImpto As Double = 0
            Dim dblSSCRImpto As Double = 0
            Dim dblSSCLImpto As Double = 0
            Dim dblSSMargenImpto As Double = 0
            Dim dblSSMargenLiberadoImpto As Double = 0
            Dim dblSTCRImpto As Double = 0
            Dim dblSTMargenImpto As Double = 0



            Dim dblAcCostoReal As Double = 0, dblAcMargen As Double = 0, dblAcMargenAplicado As Double = 0
            Dim dblAcCostoLiberado As Double = 0, dblAcMargenLiberado As Double = 0, dblAcTotal As Double = 0, dblAcTotImpto As Double = 0

            Dim dblAcSSCR As Double = 0, dblAcSSMargen As Double = 0, dblAcSSCL As Double = 0, dblAcSSML As Double = 0
            Dim dblAcSSTotal As Double = 0

            Dim dblAcSTCR As Double = 0, dblAcSTMargen As Double = 0, dblAcSTTotal As Double = 0


            Dim dblAcCostoRealImpto As Double = 0
            Dim dblAcCostoLiberadoImpto As Double = 0
            Dim dblAcMargenImpto As Double = 0
            Dim dblAcMargenLiberadoImpto As Double = 0
            Dim dblAcSSCRImpto As Double = 0
            Dim dblAcSSCLImpto As Double = 0
            Dim dblAcSSMargenImpto As Double = 0
            Dim dblAcSSMargenLiberadoImpto As Double = 0
            Dim dblAcSTCRImpto As Double = 0
            Dim dblAcSTMargenImpto As Double = 0
            Dim dblTCambio As Single = 0

            Dim sglRedondeadoTotal As Single = 0
            Dim sglRedondeo As Single = 0
            Dim sglAcRedondeo As Single = 0

            '----------

            Dim dblCostoDoble As Double, dblCostoSimple As Double, dblCostoTriple As Double
            Dim strTipoLib As Char
            Dim intPaxLiberados As Int16, intPaxLiberadosFaltantes As Int16, intPaxLiberadosDetServ As Int16
            Dim dblMontoL As Double = 0
            Dim blnLiberadosPermitidos As Boolean
            Dim blnTieneProveedorIntern As Boolean = False

            Dim objDetServ As New clsServicioProveedorBN.clsDetalleServicioProveedorBN


            'Dim objServ As New clsServicioProveedorBN
            Dim objServBT As New clsServicioProveedorBT
            'Dim drServ As SqlClient.SqlDataReader = objServBT.ConsultarPk(vstrIDServicio)
            Dim dttServ As DataTable = objServBT.ConsultarPk(vstrIDServicio, vCnn)

            Dim dc As New Dictionary(Of String, Double)
            Dim sglIGV As Single = (vsglIGV / 100)

            'drServ.Read()


            If Not vblnMargenCero Then
                If vblnServicioTC Then
                    dblMargenAplicado = vdblMargenCotiCab
                Else
                    If vdblMargenAplicado = -1 Then
                        blnTieneProveedorIntern = CBool(dttServ(0)("TieneProvInternacional"))
                        If blnTieneProveedorIntern Then
                            dblMargenAplicado = If(IsDBNull(dttServ(0)("MargenProvInt")) = True, 0, dttServ(0)("MargenProvInt"))
                        Else
                            dblMargenAplicado = If(IsDBNull(dttServ(0)("Comision")) = True, 0, dttServ(0)("Comision"))
                        End If
                        dblMargenAplicado += vdblMargenCotiCab
                    Else
                        dblMargenAplicado = vdblMargenAplicado
                    End If
                End If
            End If

            If vstrTipoProv = "001" Then 'Or vstrTipoProv = "002" Then 'Hoteles o Restaurantes
                Dim drDetServ As SqlClient.SqlDataReader = objDetServ.ConsultarPkSinAnulado(vintIDServicio_Det, vCnn)
                drDetServ.Read()
                If drDetServ.HasRows Then
                    Dim dblCostoAbsoluto As Double = 0
                    Dim dblCostoAbsolutoLib As Double = 0

                    If Not IsDBNull(drDetServ("TC")) Then
                        dblTCambio = drDetServ("TC")
                    End If
                    If dblTCambio <> 0 And vdblTCambioCotiCab <> 0 Then
                        dblTCambio = vdblTCambioCotiCab
                    End If

                    If vintPax = 1 Then
                        dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                        If dblTCambio > 0 Then
                            dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                        End If
                        dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                        If dblTCambio > 0 Then
                            dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                        End If
                        dblCostoTriple = 0

                        If vdblCostoReal = -1 Then
                            dblCostoReal = dblCostoSimple
                        Else
                            dblCostoReal = vdblCostoReal
                        End If
                        If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal

                        'dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                        'If dblTCambio > 0 Then
                        '    dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                        'End If
                        'Considerando la definición Triple
                        Dim strIDDefTriple As String = If(IsDBNull(drDetServ("IDServicio_DetxTriple")) = True, "", drDetServ("IDServicio_DetxTriple").ToString)
                        If strIDDefTriple = "" Then
                            dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                            If dblTCambio > 0 Then
                                dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                            End If
                        Else
                            Dim dblTCambioDefTrip As Double = 0
                            If Not IsDBNull(drDetServ("TipoCambioDefTrip")) Then
                                dblTCambioDefTrip = drDetServ("TipoCambioDefTrip")
                            End If

                            dblCostoTriple = If(IsDBNull(drDetServ("MontoTrixCodigo")) = True, 0, drDetServ("MontoTrixCodigo"))
                            If dblTCambioDefTrip > 0 Then
                                dblCostoTriple = If(IsDBNull(drDetServ("MontoTrisxCodigo")) = True, 0, drDetServ("MontoTrisxCodigo"))
                            End If
                        End If

                    ElseIf vintPax = 2 Then
                        dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                        If dblTCambio > 0 Then
                            dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                        End If
                        dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                        If dblTCambio > 0 Then
                            dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                        End If

                        dblCostoTriple = 0

                        If vdblCostoReal = -1 Then
                            If vstrTipoProv = "001" Then
                                dblCostoReal = dblCostoDoble / 2
                                If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoDoble
                            Else
                                dblCostoReal = dblCostoSimple
                                If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal
                            End If

                        Else
                            dblCostoReal = vdblCostoReal
                        End If

                        'dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                        'If dblTCambio > 0 Then
                        '    dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                        'End If
                        'Considerando la definición Triple
                        Dim strIDDefTriple As String = If(IsDBNull(drDetServ("IDServicio_DetxTriple")) = True, "", drDetServ("IDServicio_DetxTriple").ToString)
                        If strIDDefTriple = "" Then
                            dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                            If dblTCambio > 0 Then
                                dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                            End If
                        Else
                            Dim dblTCambioDefTrip As Double = 0
                            If Not IsDBNull(drDetServ("TipoCambioDefTrip")) Then
                                dblTCambioDefTrip = drDetServ("TipoCambioDefTrip")
                            End If

                            dblCostoTriple = If(IsDBNull(drDetServ("MontoTrixCodigo")) = True, 0, drDetServ("MontoTrixCodigo"))
                            If dblTCambioDefTrip > 0 Then
                                dblCostoTriple = If(IsDBNull(drDetServ("MontoTrisxCodigo")) = True, 0, drDetServ("MontoTrisxCodigo"))
                            End If
                        End If

                    Else

                        dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                        If dblTCambio > 0 Then
                            dblCostoDoble = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                        End If

                        dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                        If dblTCambio > 0 Then
                            dblCostoSimple = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                        End If

                        'dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                        'If dblTCambio > 0 Then
                        '    dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                        'End If
                        'Considerando la definición Triple
                        Dim strIDDefTriple As String = If(IsDBNull(drDetServ("IDServicio_DetxTriple")) = True, "", drDetServ("IDServicio_DetxTriple").ToString)
                        If strIDDefTriple = "" Then
                            dblCostoTriple = If(IsDBNull(drDetServ("Monto_tri")) = True, 0, drDetServ("Monto_tri"))
                            If dblTCambio > 0 Then
                                dblCostoTriple = If(IsDBNull(drDetServ("Monto_tris")) = True, 0, drDetServ("Monto_tris"))
                            End If
                        Else
                            Dim dblTCambioDefTrip As Double = 0
                            If Not IsDBNull(drDetServ("TipoCambioDefTrip")) Then
                                dblTCambioDefTrip = drDetServ("TipoCambioDefTrip")
                            End If

                            dblCostoTriple = If(IsDBNull(drDetServ("MontoTrixCodigo")) = True, 0, drDetServ("MontoTrixCodigo"))
                            If dblTCambioDefTrip > 0 Then
                                dblCostoTriple = If(IsDBNull(drDetServ("MontoTrisxCodigo")) = True, 0, drDetServ("MontoTrisxCodigo"))
                            End If
                        End If
                    End If

                    Dim blnPlanAlimenticio As Boolean = drDetServ("PlanAlimenticio")
                    If vdblCostoReal = -1 Then
                        If vstrTipoProv = "001" Then
                            If blnPlanAlimenticio Then
                                dblCostoReal = dblCostoSimple
                                'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal
                            Else
                                dblCostoReal = dblCostoDoble / 2
                                'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoDoble
                            End If
                            'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoSimple
                        Else
                            dblCostoReal = dblCostoSimple
                            'If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoSimple
                        End If
                    Else
                        dblCostoReal = vdblCostoReal
                    End If

                    'dblMargen = dblCostoReal * (dblMargenAplicado / 100)
                    dblMargen = 0

                    intPaxLiberados = 0
                    If vintLiberados > 0 Then
                        dblCostoAbsolutoLib = dblCostoReal

                        If (vintPax + vintLiberados) = 1 Then
                            If vdblCostoReal = -1 Then

                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                If dblTCambio > 0 Then
                                    dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                End If
                            End If

                        ElseIf (vintPax + vintLiberados) = 2 Then

                            If vdblCostoReal = -1 Then
                                If vstrTipoProv = "001" Then
                                    If vstrTipo_Lib = "S" Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                        If dblTCambio > 0 Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                        End If

                                    ElseIf vstrTipo_Lib = "D" Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                                        If dblTCambio > 0 Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                                        End If
                                        dblCostoAbsolutoLib = dblCostoAbsolutoLib / 2
                                    End If

                                Else
                                    dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                    If dblTCambio > 0 Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                    End If

                                End If
                            End If
                        Else

                        End If

                        'Dim blnPlanAlimenticio As Boolean = drDetServ("PlanAlimenticio")
                        If vdblCostoReal = -1 Then
                            If vstrTipoProv = "001" Then
                                If blnPlanAlimenticio Then

                                    If vstrTipo_Lib = "S" Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                        If dblTCambio > 0 Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                        End If

                                    ElseIf vstrTipo_Lib = "D" Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                                        If dblTCambio > 0 Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                                        End If
                                        dblCostoAbsolutoLib = dblCostoAbsolutoLib / 2
                                    End If

                                Else

                                    If vstrTipo_Lib = "S" Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                        If dblTCambio > 0 Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                        End If

                                    ElseIf vstrTipo_Lib = "D" Then
                                        dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbl")) = True, 0, drDetServ("Monto_dbl"))
                                        If dblTCambio > 0 Then
                                            dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_dbls")) = True, 0, drDetServ("Monto_dbls"))
                                        End If
                                        dblCostoAbsolutoLib = dblCostoAbsolutoLib / 2
                                    End If


                                End If
                            Else
                                dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgl")) = True, 0, drDetServ("Monto_sgl"))
                                If dblTCambio > 0 Then
                                    dblCostoAbsolutoLib = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                End If
                            End If
                        End If

                        If drDetServ("PoliticaLiberado") = True Then
                            strTipoLib = If(IsDBNull(drDetServ("TipoLib")) = True, "", drDetServ("TipoLib"))
                            intPaxLiberadosDetServ = If(IsDBNull(drDetServ("Liberado")) = True, 0, drDetServ("Liberado"))
                            dblMontoL = If(IsDBNull(drDetServ("MontoL")) = True, 0, drDetServ("MontoL"))
                            If strTipoLib = "H" Then
                                intPaxLiberadosDetServ = intPaxLiberadosDetServ * 2
                            End If

                            If intPaxLiberadosDetServ > 0 Then
                                intPaxLiberados = 0
                                If vintPax >= intPaxLiberadosDetServ Then
                                    intPaxLiberados = vintPax / intPaxLiberadosDetServ
                                End If

                                If Not IsDBNull(drDetServ("MaximoLiberado")) Then
                                    If drDetServ("MaximoLiberado") < intPaxLiberados Then
                                        intPaxLiberados = drDetServ("MaximoLiberado")
                                    End If
                                End If

                                If intPaxLiberados <> vintLiberados Then
                                    If intPaxLiberados < vintLiberados Then
                                        intPaxLiberadosFaltantes = vintLiberados - intPaxLiberados
                                    Else
                                        intPaxLiberados = vintLiberados
                                    End If
                                    'blnLiberadosPermitidos = False
                                    If intPaxLiberadosFaltantes = 0 Then blnLiberadosPermitidos = False
                                Else
                                    'blnLiberadosPermitidos = True
                                    If intPaxLiberadosFaltantes > 0 Then blnLiberadosPermitidos = True
                                End If
                            End If
                        Else
                            intPaxLiberados = 1
                            intPaxLiberadosFaltantes = vintLiberados
                        End If
                    End If

                    If Not blnLiberadosPermitidos Then
                        'If intPaxLiberados > 0 Then
                        If dblMontoL > 0 Then
                            'dblCostoLiberado = (dblMontoL / vintPax) * intPaxLiberados
                            'dblCostoLiberado = Math.Abs((dblCostoAbsolutoLib) / vintPax)
                            dblCostoLiberado = (dblCostoAbsolutoLib) / (vintPax + vintLiberados)
                            dblCostoLiberado = (dblCostoLiberado / vintPax) * vintLiberados

                        Else
                            ''dblCostoLiberado = (dblCostoSimple / vintPax) * intPaxLiberados
                            'dblCostoLiberado = Math.Abs((dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax)
                            dblCostoLiberado = Math.Abs((dblCostoAbsolutoLib) / vintPax)
                        End If
                        'End If
                    Else
                        'dblCostoLiberado = Math.Abs((dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax)
                        dblCostoLiberado = 0

                    End If
                    'HLF-20151223-I
                    'If intPaxLiberadosFaltantes > 0 Then
                    '    dblCostoLiberado = dblCostoLiberado + ((dblCostoSimple / vintPax) * (intPaxLiberadosFaltantes - 1))
                    'End If
                    If vintLiberados >= 2 Then
                        If vstrTipo_Lib = "S" Then
                            dblCostoLiberado = dblCostoSimple / vintPax
                        Else
                            If dblCostoDoble = 0 Then
                                dblCostoLiberado = (dblCostoSimple * 2) / vintPax
                            Else
                                dblCostoLiberado = dblCostoDoble / vintPax
                            End If

                        End If

                    Else
                        If intPaxLiberadosFaltantes > 0 Then
                            dblCostoLiberado = dblCostoLiberado + ((dblCostoSimple / vintPax) * (intPaxLiberadosFaltantes - 1))
                        End If
                    End If
                    'HLF-20151223-F'''''''''''''''''''''
                    If vstrTipo_Lib = "S" And Not blnLiberadosPermitidos And _
                        (vintPax >= intPaxLiberadosDetServ And intPaxLiberadosDetServ > 0) Then
                        Dim dblDiferSS As Double = If(IsDBNull(drDetServ("DiferSS")) = True, 0, drDetServ("DiferSS"))
                        dblCostoLiberado = (dblCostoReal + dblDiferSS) / vintPax
                        'If vstrTipoProv = "001" Then 'hotel
                        'dblCostoLiberado = dblCostoLiberado * vintLiberados
                        'Else 'restaurant
                        dblCostoLiberado = dblCostoLiberado * intPaxLiberados
                        'End If

                    End If

                    If intPaxLiberados > 0 Then
                        dblCostoLiberado += (dblMontoL / vintPax)
                    End If

                    Dim blnIGV As Boolean = False
                    Dim blnProrrata As Boolean = False
                    Dim sglPorcProrrata As Single = 0
                    If drDetServ("idTipoOC") = gstrTipoOperContExportacion Then
                        If vstrTipoPax <> "EXT" Then 'si es peruano
                            If drDetServ("Afecto") = True Then
                                blnIGV = True
                            End If
                        End If
                    ElseIf drDetServ("idTipoOC") = gstrTipoOperContGravado Then 'OperacContable
                        blnIGV = True
                    ElseIf drDetServ("idTipoOC") = gstrTipoOperContProrrata Then 'OperacContable
                        blnIGV = True
                        blnProrrata = True

                        'Dim objTabBN As New clsTablasApoyoBN
                        'Dim dr As SqlClient.SqlDataReader = objTabBN.ConsultarParametro
                        'If dr.HasRows Then
                        '    dr.Read()
                        '    sglPorcProrrata = 1 - (dr("PoProrrata") / 100)
                        'End If
                        'dr.Close()
                        Dim objProrr As New clsTablasApoyoBN.clsParametroBN.clsProrrataBN
                        sglPorcProrrata = 1 - (objProrr.sglProrrataxFecha(vdatFechaServicio) / 100)
                    End If

                    If vstrTipoPax <> "EXT" Or vblnIncGuia Then 'si es peruano
                        blnIGV = True
                    End If

                    'dblMargenLiberado = dblCostoLiberado * (dblMargenAplicado / 100)
                    dblMargenLiberado = (dblCostoLiberado / (1 - (dblMargenAplicado / 100))) - dblCostoLiberado
                    If blnIGV Then
                        dblMargenLiberado = ((dblCostoLiberado * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblCostoLiberado * (1 + sglIGV))
                    End If


                    'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                    If blnIGV = True Then
                        dblTotImpto = (dblCostoReal * sglIGV) + _
                                (dblCostoLiberado * sglIGV) + _
                                (dblMargen * sglIGV) + _
                                (dblMargenLiberado * sglIGV)
                    Else
                        dblTotImpto = 0

                    End If

                    If blnProrrata Then
                        dblTotImpto = ((dblCostoReal * sglIGV) * sglPorcProrrata)
                    End If

                    dblTotal = dblCostoReal '+ dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                    'dblMargen = dblTotal * (dblMargenAplicado / 100)
                    dblMargen = (dblTotal / (1 - (dblMargenAplicado / 100))) - dblTotal
                    If blnIGV Then
                        dblMargen = ((dblTotal * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblTotal * (1 + sglIGV))
                    End If


                    'dblTotal = dblTotal + dblMargen

                    dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                    If blnProrrata Then
                        Dim dblTotal2 As Decimal = (dblCostoReal + dblTotImpto) / (1 - (dblMargenAplicado / 100))
                        dblMargen = dblRedondeoInmedSuperior(dblTotal2, vsglRedondeo) * (dblMargenAplicado / 100)
                        dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto
                    End If

                    'Suplementos
                    If vdblSSCR = -1 Then
                        dblSSCR = dblCostoSimple - dblCostoReal
                        If blnTieneProveedorIntern Then
                            dblSSCR = Math.Abs(dblCostoSimple - dblCostoReal)
                        End If
                    Else
                        dblSSCR = vdblSSCR
                    End If

                    'dblSSMargen = dblSSCR * (dblMargenAplicado / 100)
                    dblSSMargen = (dblSSCR / (1 - (dblMargenAplicado / 100))) - dblSSCR

                    If vstrTipo_Lib = "S" Then
                        dblSSCL = 0
                    ElseIf vstrTipo_Lib = "D" Then
                        dblSSCL = (dblCostoDoble / vintPax) - (dblCostoSimple / vintPax)
                    End If
                    'dblSSML = dblSSCL * (dblMargenAplicado / 100)
                    dblSSML = (dblSSCL / (1 - (dblMargenAplicado / 100))) - dblSSCL
                    dblSSTotal = dblSSCR + dblSSMargen + dblSSCL + dblSSML

                    'No aplica el costo triple para los casos cuando sea un Oper. Internacional y su costos en
                    'Simple y Doble sean 0
                    If dblCostoTriple > 0 Then
                        If vdblSTCR = -1 Then
                            dblSTCR = (dblCostoTriple / 3) - dblCostoReal
                            If blnTieneProveedorIntern And dblSSTotal = 0 And dblTotal = 0 Then
                                dblSTCR = 0
                            End If
                        Else
                            dblSTCR = vdblSTCR
                        End If
                        dblSTMargen = dblSTCR * (dblMargenAplicado / 100)
                        'dblSTMargen = (dblSTCR / (1 - (dblMargenAplicado / 100))) - dblSTCR
                        dblSTTotal = dblSTCR + dblSTMargen
                    Else
                        If vdblSTCR = -1 Then
                        Else
                            dblSTCR = vdblSTCR
                        End If
                        dblSTMargen = dblSTCR * (dblMargenAplicado / 100)
                        'dblSTMargen = (dblSTCR / (1 - (dblMargenAplicado / 100))) - dblSTCR
                        dblSTTotal = dblSTCR + dblSTMargen
                    End If



                    'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                    If blnIGV Then
                        dblCostoRealImpto = dblCostoReal * sglIGV
                        dblCostoLiberadoImpto = dblCostoLiberado * sglIGV
                        dblMargenImpto = dblMargen * sglIGV
                        dblMargenLiberadoImpto = dblMargenLiberado * sglIGV
                        dblSSCRImpto = dblSSCR * sglIGV
                        dblSSCLImpto = dblSSCL * sglIGV
                        dblSSMargenImpto = dblSSMargen * sglIGV
                        dblSSMargenLiberadoImpto = dblSSML * sglIGV
                        dblSTCRImpto = dblSTCR * sglIGV
                        dblSTMargenImpto = dblSTMargen * sglIGV

                    Else
                        dblCostoRealImpto = 0
                        dblCostoLiberadoImpto = 0
                        dblMargenImpto = 0
                        dblMargenLiberadoImpto = 0
                        dblSSCRImpto = 0
                        dblSSCLImpto = 0
                        dblSSMargenImpto = 0
                        dblSSMargenLiberadoImpto = 0
                        dblSTCRImpto = 0
                        dblSTMargenImpto = 0
                    End If

                    dblSSTotal += dblSSCRImpto
                End If

                If dblTCambio = 0 Then
                    dc.Add("CostoReal", dblCostoReal)
                    dc.Add("Margen", dblMargen + dblMargenLiberado)
                    dc.Add("CostoLiberado", dblCostoLiberado)
                    dc.Add("MargenLiberado", dblMargenLiberado)
                    dc.Add("MargenAplicado", dblMargenAplicado)
                    dc.Add("TotImpto", dblTotImpto)
                    dc.Add("TotalOrig", dblTotal)

                    dc.Add("Total", dblRedondeoInmedSuperior(Math.Round(dblTotal, 2), vsglRedondeo, sglRedondeadoTotal))
                    dc.Add("RedondeoTotal", sglRedondeadoTotal)

                    dc.Add("SSCR", dblSSCR)
                    dc.Add("SSMargen", dblSSMargen)
                    dc.Add("SSCL", dblSSCL)
                    dc.Add("SSML", dblSSML)
                    dc.Add("SSTotalOrig", dblSSTotal)
                    dc.Add("SSTotal", dblRedondeoInmedSuperior(dblSSTotal, vsglRedondeo))

                    dc.Add("STCR", dblSTCR)
                    dc.Add("STMargen", dblSTMargen)
                    dc.Add("STTotalOrig", dblSTTotal)
                    dc.Add("STTotal", dblRedondeoInmedSuperior(Math.Round(dblSTTotal, 4), vsglRedondeo, , True))

                    dc.Add("CostoRealImpto", dblCostoRealImpto)
                    dc.Add("CostoLiberadoImpto", dblCostoLiberadoImpto)
                    dc.Add("MargenImpto", dblMargenImpto)
                    dc.Add("MargenLiberadoImpto", dblMargenLiberadoImpto)
                    dc.Add("SSCRImpto", dblSSCRImpto)
                    dc.Add("SSCLImpto", dblSSCLImpto)
                    dc.Add("SSMargenImpto", dblSSMargenImpto)
                    dc.Add("SSMargenLiberadoImpto", dblSSMargenLiberadoImpto)
                    dc.Add("STCRImpto", dblSTCRImpto)
                    dc.Add("STMargenImpto", dblSTMargenImpto)
                Else
                    dc.Add("CostoReal", If(vdblCostoReal = -1, dblCostoReal / dblTCambio, vdblCostoReal))
                    dc.Add("Margen", If(vdblCostoReal = -1, (dblMargen + dblMargenLiberado) / dblTCambio, dblMargen))
                    dc.Add("CostoLiberado", dblCostoLiberado / dblTCambio)
                    dc.Add("MargenLiberado", dblMargenLiberado / dblTCambio)
                    dc.Add("MargenAplicado", dblMargenAplicado)
                    dc.Add("TotImpto", dblTotImpto / dblTCambio)
                    dc.Add("TotalOrig", dblTotal)
                    dc.Add("Total", dblRedondeoInmedSuperior(Math.Round(If(vdblCostoReal = -1, dblTotal / Convert.ToDecimal(dblTCambio), dblTotal), 2), vsglRedondeo, sglRedondeadoTotal))
                    dc.Add("RedondeoTotal", sglRedondeadoTotal)

                    dc.Add("SSCR", If(vdblSSCR = -1, dblSSCR / dblTCambio, vdblSSCR)) 'dblSSCR / dblTCambio)
                    dc.Add("SSMargen", dblSSMargen / dblTCambio)
                    dc.Add("SSCL", dblSSCL / dblTCambio)
                    dc.Add("SSML", dblSSML / dblTCambio)
                    dc.Add("SSTotalOrig", dblSSTotal)
                    dc.Add("SSTotal", dblRedondeoInmedSuperior(dblSSTotal / Convert.ToDecimal(dblTCambio), vsglRedondeo))

                    dc.Add("STCR", If(vdblSTCR = -1, dblSTCR / dblTCambio, vdblSTCR)) 'dblSTCR / dblTCambio)
                    dc.Add("STMargen", dblSTMargen / dblTCambio)
                    dc.Add("STTotalOrig", dblSTTotal)
                    dc.Add("STTotal", dblRedondeoInmedSuperior(dblSTTotal / Convert.ToDecimal(dblTCambio), vsglRedondeo, , True))

                    dc.Add("CostoRealImpto", dblCostoRealImpto / dblTCambio)
                    dc.Add("CostoLiberadoImpto", dblCostoLiberadoImpto / dblTCambio)
                    dc.Add("MargenImpto", dblMargenImpto / dblTCambio)
                    dc.Add("MargenLiberadoImpto", dblMargenLiberadoImpto / dblTCambio)
                    dc.Add("SSCRImpto", dblSSCRImpto / dblTCambio)
                    dc.Add("SSCLImpto", dblSSCLImpto / dblTCambio)
                    dc.Add("SSMargenImpto", dblSSMargenImpto / dblTCambio)
                    dc.Add("SSMargenLiberadoImpto", dblSSMargenLiberadoImpto / dblTCambio)
                    dc.Add("STCRImpto", dblSTCRImpto / dblTCambio)
                    dc.Add("STMargenImpto", dblSTMargenImpto / dblTCambio)
                End If
            Else 'No Hoteles ni restaurantes


                'Dim drDetServ As SqlClient.SqlDataReader = objServBT.ConsultarxIDServicioAnio(vstrIDServicio, vstrAnio, vstrTipo)
                Dim dttDetServ As New DataTable '= objServBT.dttConsultarxIDServicioAnio(vstrIDServicio, vstrAnio, vstrTipo, vCnn)
                If vstrTipoProv = "002" Then
                    dttDetServ = objDetServ.dttConsultarPkSinAnulado(vintIDServicio_Det, vCnn)
                Else
                    Dim intIDDetAcVe As Integer = 0
                    If vblnAcomVehic Then
                        If IsNumeric(vstrIDDet_DetCotiServ) Then
                            intIDDetAcVe = vstrIDDet_DetCotiServ
                        End If
                    End If
                    dttDetServ = objServBT.dttConsultarxIDServicioAnio(vstrIDServicio, vstrAnio, vstrTipo, intIDDetAcVe, vCnn)
                End If

                For Each drDetServ As DataRow In dttDetServ.Rows


                    'While drDetServ.Read()
                    If vblnIncGuia Then
                        If vintIDServicio_Det <> drDetServ("IDServicio_Det") Then
                            GoTo SgteDet
                        End If
                    End If

                    If Not IsDBNull(drDetServ("TC")) Then
                        dblTCambio = drDetServ("TC")
                    Else
                        dblTCambio = 0
                    End If
                    If dblTCambio <> 0 And vdblTCambioCotiCab <> 0 Then
                        dblTCambio = vdblTCambioCotiCab
                    End If


                    intPaxLiberados = 0
                    If vintLiberados > 0 Then
                        If drDetServ("PoliticaLiberado") = True Then
                            strTipoLib = If(IsDBNull(drDetServ("TipoLib")) = True, "", drDetServ("TipoLib"))
                            intPaxLiberadosDetServ = If(IsDBNull(drDetServ("Liberado")) = True, 0, drDetServ("Liberado"))
                            dblMontoL = If(IsDBNull(drDetServ("MontoL")) = True, 0, drDetServ("MontoL"))
                            If strTipoLib = "H" Then
                                intPaxLiberadosDetServ = intPaxLiberadosDetServ * 2
                            End If

                            If intPaxLiberadosDetServ > 0 Then
                                intPaxLiberados = 0
                                If vintPax >= intPaxLiberadosDetServ Then
                                    intPaxLiberados = vintPax / intPaxLiberadosDetServ
                                End If

                                If Not IsDBNull(drDetServ("MaximoLiberado")) Then
                                    If drDetServ("MaximoLiberado") < intPaxLiberados Then
                                        intPaxLiberados = drDetServ("MaximoLiberado")
                                    End If
                                End If

                                If intPaxLiberados <> vintLiberados Then
                                    If intPaxLiberados < vintLiberados Then
                                        intPaxLiberadosFaltantes = vintLiberados - intPaxLiberados
                                    Else
                                        intPaxLiberados = vintLiberados
                                    End If
                                    'blnLiberadosPermitidos = False
                                    If intPaxLiberadosFaltantes = 0 Then blnLiberadosPermitidos = False
                                Else
                                    'blnLiberadosPermitidos = True
                                    If intPaxLiberadosFaltantes > 0 Then blnLiberadosPermitidos = True
                                End If
                            End If
                        Else
                            intPaxLiberados = 1
                            intPaxLiberadosFaltantes = vintLiberados
                        End If
                    End If

                    dblCostoReal = 0
                    Dim dblCostoAbsoluto As Double = 0
                    Dim dblCostoAbsolutoLib As Double = 0
                    Dim blnPorRango As Boolean = False
                    If vdblCostoReal = -1 Then

                        If Not IsDBNull(drDetServ("Monto_sgl")) Then
                            If vblnServicioTC Then
                                Dim strCoTipoCostoTC As String = drDetServ("CoTipoCostoTC")
                                If strCoTipoCostoTC = "TAX" Or strCoTipoCostoTC = "TEL" Then
                                    If vstrIDTipoHonorario = "002" Then
                                        dblTCambio = 1
                                    End If
                                End If
                                Select Case strCoTipoCostoTC
                                    Case "TAX"
                                        dblCostoReal = vdicValoresTC.Item("dblTaxi")
                                    Case "TEL"
                                        dblCostoReal = vdicValoresTC.Item("dblTarjetaTelefonica")
                                    Case "VID"
                                        dblCostoReal = vdicValoresTC.Item("dblViaticosInternacionales")
                                    Case "VIS"
                                        dblCostoReal = vdicValoresTC.Item("dblViaticosNacionales")
                                    Case "NAD"
                                        dblCostoReal = vdicValoresTC.Item("dblNochesAntesDsp")
                                    Case "VUE"
                                        dblCostoReal = vdicValoresTC.Item("dblVuelos")
                                    Case "TAE"
                                        dblCostoReal = vdicValoresTC.Item("dblTicketAereo")
                                    Case "HON"
                                        dblCostoReal = vdicValoresTC.Item("dblHonorarios")
                                    Case "IGV"
                                        dblCostoReal = vdicValoresTC.Item("dblIGVHoteles")
                                    Case "OTR"
                                        dblCostoReal = vdicValoresTC.Item("dblOtros")
                                    Case "GPD"
                                        dblCostoReal = vdicValoresTC.Item("dblGebPrimerDia")
                                    Case "GUD"
                                        dblCostoReal = vdicValoresTC.Item("dblGebUltimoDia")
                                End Select

                                'If dblTCambio > 0 Then
                                '    dblCostoReal = dblCostoReal / dblTCambio
                                'End If
                                dblCostoReal = dblCostoReal / vintPax '(vintPax + vintLiberados)
                            Else
                                dblCostoReal = drDetServ("Monto_sgl")
                                If dblTCambio > 0 Then
                                    dblCostoReal = If(IsDBNull(drDetServ("Monto_sgls")) = True, 0, drDetServ("Monto_sgls"))
                                End If

                                If vblnIncGuia Then
                                    dblCostoReal = dblCostoReal / vintPax
                                End If

                                If vintLiberados > 0 Then dblCostoAbsolutoLib = dblCostoReal
                            End If
                        Else
                            Dim objDetCostosServ As New clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsCostosDetalleServicioProveedorBN
                            'Dim dttCostos As DataTable = objDetCostosServ.ConsultarListxIDDetalle(vintIDServicio_Det)
                            Dim dttCostos As DataTable = objDetCostosServ.ConsultarListxIDDetalle(drDetServ("IDServicio_Det"), vCnn)
                            Dim intPaxHastaMax As Int16 = 0
                            Dim dblCostoRealTemp As Double
                            Dim dblCostoAbsolutoTemp As Double

                            If vblnServicioTC Then
                                Dim strCoTipoCostoTC As String = drDetServ("CoTipoCostoTC")
                                If strCoTipoCostoTC = "TAX" Or strCoTipoCostoTC = "TEL" Then
                                    If vstrIDTipoHonorario = "002" Then
                                        dblTCambio = 1
                                    End If
                                End If
                                Select Case strCoTipoCostoTC
                                    Case "TAX"
                                        dblCostoReal = vdicValoresTC.Item("dblTaxi")
                                    Case "TEL"
                                        dblCostoReal = vdicValoresTC.Item("dblTarjetaTelefonica")
                                    Case "VID"
                                        dblCostoReal = vdicValoresTC.Item("dblViaticosInternacionales")
                                    Case "VIS"
                                        dblCostoReal = vdicValoresTC.Item("dblViaticosNacionales")
                                    Case "NAD"
                                        dblCostoReal = vdicValoresTC.Item("dblNochesAntesDsp")
                                    Case "VUE"
                                        dblCostoReal = vdicValoresTC.Item("dblVuelos")
                                    Case "TAE"
                                        dblCostoReal = vdicValoresTC.Item("dblTicketAereo")
                                    Case "HON"
                                        dblCostoReal = vdicValoresTC.Item("dblHonorarios")
                                    Case "IGV"
                                        dblCostoReal = vdicValoresTC.Item("dblIGVHoteles")
                                    Case "OTR"
                                        dblCostoReal = vdicValoresTC.Item("dblOtros")
                                    Case "GPD"
                                        dblCostoReal = vdicValoresTC.Item("dblGebPrimerDia")
                                    Case "GUD"
                                        dblCostoReal = vdicValoresTC.Item("dblGebUltimoDia")
                                End Select

                                'If dblTCambio > 0 Then
                                '    dblCostoReal = dblCostoReal / dblTCambio
                                'End If
                                dblCostoReal = dblCostoReal / vintPax '(vintPax + vintLiberados)
                            Else
                                For Each drCostos As DataRow In dttCostos.Rows
                                    If vintPax >= drCostos("PaxDesde") And vintPax <= drCostos("PaxHasta") Then
                                        dblCostoReal = drCostos("Monto") / vintPax
                                        If vintLiberados > 0 Then dblCostoAbsoluto = drCostos("Monto")
                                        Exit For
                                    End If

                                    If drCostos("PaxHasta") > intPaxHastaMax Then
                                        intPaxHastaMax = drCostos("PaxHasta")
                                    End If
                                    'dblCostoRealTemp = drCostos("Monto") / vintPax
                                    If vintPax = 0 Then
                                        dblCostoRealTemp = drCostos("Monto") '/ vintPax
                                    Else
                                        dblCostoRealTemp = drCostos("Monto") / vintPax
                                    End If
                                    If vintLiberados > 0 Then dblCostoAbsolutoTemp = drCostos("Monto")

                                Next
                            End If

                            If dblCostoReal = 0 And (intPaxHastaMax >= 40 And (vintPax + vintLiberados) >= 40) Then
                                dblCostoReal = dblCostoRealTemp
                                dblCostoAbsoluto = dblCostoAbsolutoTemp

                                If vblnAcomVehic Then
                                    ''''
                                    dblCostoReal = dblCostoFueradeRango(dttCostos, intPaxHastaMax, vintPax)
                                    If vintLiberados > 0 Then dblCostoAbsoluto = dblCostoReal
                                    If vintPax <> 0 Then
                                        dblCostoReal = dblCostoReal / vintPax
                                    End If
                                    ''''
                                End If
                            End If

                            If vintLiberados > 0 Then
                                Dim dblCostoAbsolutoLibTemp As Double
                                For Each drCostos As DataRow In dttCostos.Rows
                                    If (vintPax + vintLiberados) >= drCostos("PaxDesde") And (vintPax + vintLiberados) <= drCostos("PaxHasta") Then

                                        dblCostoAbsolutoLib = drCostos("Monto")
                                        Exit For
                                    End If
                                    dblCostoAbsolutoLibTemp = drCostos("Monto")
                                Next
                                If dblCostoAbsolutoLib = 0 And intPaxHastaMax >= 40 Then
                                    dblCostoAbsolutoLib = dblCostoAbsolutoLibTemp
                                    If vblnAcomVehic Then
                                        ''''
                                        dblCostoAbsolutoLib = dblCostoFueradeRango(dttCostos, intPaxHastaMax, (vintPax + vintLiberados))
                                        ''''
                                    End If

                                End If

                            End If

                            Dim dblCostoSimpleTemp As Double
                            For Each drCostos As DataRow In dttCostos.Rows
                                If (vintPax + intPaxLiberadosFaltantes) >= drCostos("PaxDesde") And (vintPax + intPaxLiberadosFaltantes) <= drCostos("PaxHasta") Then
                                    dblCostoSimple = drCostos("Monto")
                                    Exit For
                                End If
                                dblCostoSimpleTemp = drCostos("Monto")
                            Next
                            If dblCostoSimple = 0 And intPaxHastaMax >= 40 Then
                                dblCostoSimple = dblCostoSimpleTemp
                            End If

                            blnPorRango = True
                        End If
                    Else
                        dblCostoReal = vdblCostoReal
                    End If

                    'dblMargen = dblCostoReal * (dblMargenAplicado / 100)
                    dblMargen = 0



                    If Not blnLiberadosPermitidos Then
                        'If intPaxLiberados > 0 Then
                        If dblMontoL > 0 Then
                            'dblCostoLiberado = (dblMontoL / vintPax) * intPaxLiberados
                            'dblCostoLiberado = (dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax
                            dblCostoLiberado = (dblCostoAbsolutoLib) / (vintPax + vintLiberados)
                            dblCostoLiberado = (dblCostoLiberado / vintPax) * vintLiberados

                        Else
                            ''dblCostoLiberado = (dblCostoSimple / vintPax) * intPaxLiberados
                            ''If dblCostoLiberado > dblCostoReal Then
                            ''    dblCostoLiberado = dblCostoLiberado - dblCostoReal
                            ''End If
                            dblCostoLiberado = (dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax
                            If dblCostoAbsoluto = 0 Then
                                dblCostoLiberado = ((dblCostoAbsolutoLib - dblCostoAbsoluto) / vintPax) * vintLiberados
                            End If
                        End If
                        'End If
                    Else
                        dblCostoLiberado = 0
                    End If

                    'If vstrTipo_Lib = "D" And Not blnLiberadosPermitidos And vintPax >= intPaxLiberadosDetServ Then
                    If vstrTipo_Lib = "D" And Not blnLiberadosPermitidos And vintPax >= intPaxLiberadosDetServ And blnPorRango = False Then
                        Dim dblDiferSS As Double = If(IsDBNull(drDetServ("DiferSS")) = True, 0, drDetServ("DiferSS"))
                        dblCostoLiberado = (dblCostoReal + dblDiferSS) / vintPax
                        dblCostoLiberado = dblCostoLiberado * intPaxLiberados ' vintLiberados
                    End If

                    'If intPaxLiberadosFaltantes > 0 Then
                    '    dblCostoLiberado = dblCostoLiberado + ((dblCostoSimple / vintPax) * (intPaxLiberadosFaltantes - 1))
                    'End If
                    If intPaxLiberados > 0 Then
                        dblCostoLiberado += (dblMontoL / vintPax)
                    End If


                    Dim blnIGV As Boolean = False
                    Dim blnProrrata As Boolean = False
                    Dim sglPorcProrrata As Single = 0
                    If drDetServ("OperacContable") = gstrTipoOperContExportacion Then
                        If vstrTipoPax <> "EXT" Then 'si es peruano
                            If drDetServ("Afecto") = True Then
                                blnIGV = True
                            End If
                        End If
                    ElseIf drDetServ("OperacContable") = gstrTipoOperContGravado Then
                        blnIGV = True
                    ElseIf drDetServ("OperacContable") = gstrTipoOperContProrrata Then 'OperacContable
                        blnIGV = True
                        blnProrrata = True

                        'Dim objTabBN As New clsTablasApoyoBN
                        'Dim dr As SqlClient.SqlDataReader = objTabBN.ConsultarParametro
                        'If dr.HasRows Then
                        '    dr.Read()
                        '    sglPorcProrrata = 1 - (dr("PoProrrata") / 100)
                        'End If
                        'dr.Close()
                        Dim objProrr As New clsTablasApoyoBN.clsParametroBN.clsProrrataBN
                        sglPorcProrrata = 1 - (objProrr.sglProrrataxFecha(vdatFechaServicio) / 100)
                    End If
                    If vstrTipoPax <> "EXT" Or vblnIncGuia Then 'si es peruano
                        blnIGV = True
                    End If

                    'dblMargenLiberado = dblCostoLiberado * (dblMargenAplicado / 100)
                    dblMargenLiberado = (dblCostoLiberado / (1 - (dblMargenAplicado / 100))) - dblCostoLiberado
                    If blnIGV Then
                        dblMargenLiberado = ((dblCostoLiberado * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblCostoLiberado * (1 + sglIGV))
                    End If



                    'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                    If blnIGV Then
                        dblCostoRealImpto = dblCostoReal * sglIGV
                        dblCostoLiberadoImpto = dblCostoLiberado * sglIGV
                        dblMargenImpto = dblMargen * sglIGV
                        dblMargenLiberadoImpto = dblMargenLiberado * sglIGV
                        dblSSCRImpto = dblSSCR * sglIGV
                        dblSSCLImpto = dblSSCL * sglIGV
                        dblSSMargenImpto = dblSSMargen * sglIGV
                        dblSSMargenLiberadoImpto = dblSSML * sglIGV
                        dblSTCRImpto = dblSTCR * sglIGV
                        dblSTMargenImpto = dblSTMargen * sglIGV

                    Else
                        dblCostoRealImpto = 0
                        dblCostoLiberadoImpto = 0
                        dblMargenImpto = 0
                        dblMargenLiberadoImpto = 0
                        dblSSCRImpto = 0
                        dblSSCLImpto = 0
                        dblSSMargenImpto = 0
                        dblSSMargenLiberadoImpto = 0
                        dblSTCRImpto = 0
                        dblSTMargenImpto = 0
                    End If


                    If vsglIGV > 0 Or vblnServicioTC Then
                        'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                        If blnIGV Then
                            dblTotImpto = (dblCostoReal * sglIGV) + _
                                                    (dblCostoLiberado * sglIGV) + _
                                                    (dblMargen * sglIGV) + _
                                                    (dblMargenLiberado * sglIGV) ' + _
                            '(dblSSCR * sglIGV) + _
                            '(dblSSCL * sglIGV) + _
                            '(dblSSMargen * sglIGV) + _
                            '(dblSSML * sglIGV) + _
                            '(dblSTCR * sglIGV) + _
                            '(dblSTMargen * sglIGV)
                        Else
                            dblTotImpto = 0
                        End If

                        If blnProrrata Then
                            dblTotImpto = ((dblCostoReal * sglIGV) * sglPorcProrrata)
                        End If

                        dblTotal = dblCostoReal '+ dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto
                        'dblMargen = dblTotal * (dblMargenAplicado / 100)
                        dblMargen = (dblTotal / (1 - (dblMargenAplicado / 100))) - dblTotal
                        If blnIGV Then
                            dblMargen = ((dblTotal * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblTotal * (1 + sglIGV))
                        End If

                        'dblTotal = dblTotal + dblMargen
                        dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                        If blnProrrata Then
                            Dim dblTotal2 As Double = (dblCostoReal + dblTotImpto) / (1 - (dblMargenAplicado / 100))
                            dblMargen = dblRedondeoInmedSuperior(dblTotal2, vsglRedondeo) * (dblMargenAplicado / 100)
                            dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                        End If

                        If dblTCambio = 0 Then
                            dblTotal = dblRedondeoInmedSuperior(dblTotal, vsglRedondeo, sglRedondeadoTotal)
                            sglRedondeo = sglRedondeadoTotal
                        End If


                        'Servicio con Alojamiento
                        dblSSCR = 0
                        dblSTCR = 0
                        dblSSMargen = 0
                        dblSTMargen = 0
                        If drDetServ("ConAlojamiento") = True Then

                            Dim dblDiferSS As Double = If(IsDBNull(drDetServ("DiferSS")) = True, 0, drDetServ("DiferSS"))
                            Dim dblDiferST As Double = If(IsDBNull(drDetServ("DiferST")) = True, 0, drDetServ("DiferST"))

                            'dblSSCR = dblTotal + dblDiferSS
                            If vdblSSCR = -1 Then
                                dblSSCR = dblDiferSS
                            Else
                                dblSSCR = vdblSSCR
                            End If

                            'dblSTCR = dblTotal - dblDiferST
                            If vdblSTCR = -1 Then
                                dblSTCR = dblDiferST
                            Else
                                dblSTCR = vdblSTCR
                            End If

                            'HLF-20151119-I
                            If blnIGV Then
                                If Not blnProrrata Then
                                    dblSSCRImpto = dblSSCR * sglIGV
                                    dblSTCRImpto = dblSTCR * sglIGV
                                Else
                                    dblSSCRImpto = (dblSSCR * sglIGV) * sglPorcProrrata
                                    dblSTCRImpto = (dblSTCR * sglIGV) * sglPorcProrrata
                                End If
                            End If
                            'HLF-20151119-F

                            'dblSSMargen = dblDiferSS / (1 - (dblMargenAplicado / 100))
                            'dblSTMargen = dblDiferST / (1 - (dblMargenAplicado / 100))

                            'dblSSMargen = dblDiferSS * (dblMargenAplicado / 100)
                            ''PPMG-20151022-If vdblSSCR = 0 Then
                            If vdblSSCR = 0 Then
                                dblSSMargen = 0
                            Else
                                dblSSMargen = (dblDiferSS / (1 - (dblMargenAplicado / 100))) - dblDiferSS
                            End If
                            'dblSTMargen = dblDiferST * (dblMargenAplicado / 100)
                            ''PPMG-20151022-If vdblSTCR = 0 Then
                            If vdblSTCR = 0 Then
                                dblSTMargen = 0
                            Else
                                dblSTMargen = (dblDiferST / (1 - (dblMargenAplicado / 100))) - dblDiferST
                            End If

                            'HL-20150731-I
                            'dblCostoLiberado = ((dblCostoReal + dblDiferSS) / vintPax) * intPaxLiberadosFaltantes
                            If blnLiberadosPermitidos Then
                                If intPaxLiberadosFaltantes > 0 Then
                                    dblCostoLiberado = ((dblCostoReal + dblDiferSS) / vintPax) * intPaxLiberadosFaltantes
                                Else
                                    'dblCostoLiberado = ((dblCostoReal - dblMontoL) / vintPax)
                                    If (intPaxLiberados <> vintLiberados And dblMontoL = 0) Or dblMontoL > 0 Then
                                        dblCostoLiberado = ((dblCostoReal - dblMontoL) / vintPax)
                                    End If
                                End If
                            End If
                            ''If intPaxLiberadosFaltantes > 0 Then
                            ''    dblCostoLiberado = ((dblCostoReal + dblDiferSS) / vintPax) * intPaxLiberadosFaltantes
                            ''End If
                            'HL-20150731-F

                            'Recalculando Total - I

                            dblMargenLiberado = (dblCostoLiberado / (1 - (dblMargenAplicado / 100))) - dblCostoLiberado
                            If blnIGV Then
                                dblMargenLiberado = ((dblCostoLiberado * (1 + sglIGV)) / (1 - (dblMargenAplicado / 100))) - (dblCostoLiberado * (1 + sglIGV))
                            End If

                            If blnIGV Then
                                dblTotImpto = (dblCostoReal * sglIGV) + _
                                        (dblCostoLiberado * sglIGV) + _
                                        (dblMargen * sglIGV) + _
                                        (dblMargenLiberado * sglIGV)
                            Else
                                dblTotImpto = 0

                            End If
                            If blnProrrata Then
                                dblTotImpto = ((dblCostoReal * sglIGV) * sglPorcProrrata)
                            End If

                            dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto

                            If blnProrrata Then
                                Dim dblTotal2 As Double = (dblCostoReal + dblTotImpto) / (1 - (dblMargenAplicado / 100))
                                dblMargen = dblRedondeoInmedSuperior(dblTotal2, vsglRedondeo) * (dblMargenAplicado / 100)
                                'dblMargen = dblTotal2 * (dblMargenAplicado / 100)
                                dblTotal = dblCostoReal + dblMargen + dblMargenLiberado + dblCostoLiberado + dblTotImpto
                            End If

                            If dblTCambio = 0 Then
                                dblTotal = dblRedondeoInmedSuperior(dblTotal, vsglRedondeo, sglRedondeadoTotal)
                                sglRedondeo = sglRedondeadoTotal
                            End If
                            'Recalculando Total - F

                        End If

                        dblSSTotal = dblSSCR + dblSSMargen + dblSSCL + dblSSML
                        dblSTTotal = dblSTCR + dblSTMargen

                        If dblTCambio <> 0 Then


                            If vdblCostoReal = -1 Then dblCostoReal = dblCostoReal / dblTCambio
                            If vdblCostoReal = -1 Then
                                dblTotal = dblRedondeoInmedSuperior(dblTotal / Convert.ToDecimal(dblTCambio), vsglRedondeo, sglRedondeadoTotal)
                            Else
                                dblTotal = dblRedondeoInmedSuperior(dblTotal, vsglRedondeo, sglRedondeadoTotal)
                            End If
                            If vdblCostoReal = -1 Then sglRedondeo = sglRedondeadoTotal

                            If vdblCostoReal = -1 Then dblMargen = dblMargen / dblTCambio
                            dblTotImpto = dblTotImpto / dblTCambio

                            dblCostoLiberado = dblCostoLiberado / dblTCambio
                            dblMargenLiberado = dblMargenLiberado / dblTCambio

                            dblSSCR = dblSSCR / dblTCambio
                            dblSSMargen = dblSSMargen / dblTCambio
                            dblSSCL = dblSSCL / dblTCambio
                            dblSSML = dblSSML / dblTCambio
                            dblSSTotal = dblSSTotal / dblTCambio

                            dblSTCR = dblSTCR / dblTCambio
                            dblSTMargen = dblSTMargen / dblTCambio
                            dblSTTotal = dblSTTotal / dblTCambio

                            dblCostoRealImpto = dblCostoRealImpto / dblTCambio
                            dblCostoLiberadoImpto = dblCostoLiberadoImpto / dblTCambio
                            dblMargenImpto = dblMargenImpto / dblTCambio
                            dblMargenLiberadoImpto = dblMargenLiberadoImpto / dblTCambio
                            dblSSCRImpto = dblSSCRImpto / dblTCambio
                            dblSSCLImpto = dblSSCLImpto / dblTCambio
                            dblSSMargenImpto = dblSSMargenImpto / dblTCambio
                            dblSSMargenLiberadoImpto = dblSSMargenLiberadoImpto / dblTCambio
                            dblSTCRImpto = dblSTCRImpto / dblTCambio
                            dblSTMargenImpto = dblSTMargenImpto / dblTCambio

                        End If


                        If Not rdttDetCotiServ Is Nothing Then

                            'If drDetServ("Afecto") = True Or vstrTipoPax <> "EXT" Then
                            If blnIGV Then
                                rdttDetCotiServ.Rows.Add(vstrIDDet_DetCotiServ, vstrIDServicio, drDetServ("IDServicio_Det"), dblCostoReal, _
                                                         dblMargen + dblMargenLiberado, dblCostoLiberado, _
                                                        dblMargenLiberado, dblMargenAplicado, _
                                                        dblTotal, sglRedondeo, dblSSCR, dblSSMargen, dblSSCL, dblSSML, _
                                                        dblSSTotal, dblSTCR, dblSTMargen, dblSTTotal, _
                                                        (dblCostoReal * sglIGV), _
                                                        (dblCostoLiberado * sglIGV), _
                                                        (dblMargen * sglIGV), _
                                                        (dblMargenLiberado * sglIGV), _
                                                        (dblSSCR * sglIGV), _
                                                        (dblSSCL * sglIGV), _
                                                        (dblSSMargen * sglIGV), _
                                                        (dblSSML * sglIGV), _
                                                        (dblSTCR * sglIGV), _
                                                        (dblSTMargen * sglIGV), _
                                                        dblTotImpto)
                            Else
                                rdttDetCotiServ.Rows.Add(vstrIDDet_DetCotiServ, vstrIDServicio, drDetServ("IDServicio_Det"), dblCostoReal, _
                                                         dblMargen + dblMargenLiberado, dblCostoLiberado, _
                                                        dblMargenLiberado, dblMargenAplicado, _
                                                        dblTotal, sglRedondeo, dblSSCR, dblSSMargen, dblSSCL, dblSSML, _
                                                        dblSSTotal, dblSTCR, dblSTMargen, dblSTTotal, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        0, _
                                                        dblTotImpto)

                            End If
                        End If
                    End If




                    '''''

                    'Acumula
                    If vdblCostoReal = -1 Then
                        dblAcCostoReal += dblCostoReal
                        dblAcTotal += dblTotal
                        sglAcRedondeo += sglRedondeo
                        dblAcMargen += (dblMargen + dblMargenLiberado)
                    Else
                        dblAcCostoReal = dblCostoReal
                        dblAcTotal = dblTotal
                        sglAcRedondeo = sglRedondeo
                        dblAcMargen = dblMargen
                    End If
                    dblAcTotImpto += dblTotImpto

                    dblAcCostoLiberado += dblCostoLiberado
                    dblAcMargenLiberado += dblMargenLiberado
                    dblAcMargenAplicado = dblMargenAplicado


                    dblAcSSCR += dblSSCR
                    dblAcSSMargen += dblSSMargen
                    dblAcSSCL += dblSSCL
                    dblAcSSML += dblSSML
                    dblAcSSTotal += dblSSTotal

                    dblAcSTCR += dblSTCR
                    dblAcSTMargen += dblSTMargen
                    dblAcSTTotal += dblSTTotal

                    dblAcCostoRealImpto += dblCostoRealImpto
                    dblAcCostoLiberadoImpto += dblCostoLiberadoImpto
                    dblAcMargenImpto += dblMargenImpto
                    dblAcMargenLiberadoImpto += dblMargenLiberadoImpto
                    dblAcSSCRImpto += dblSSCRImpto
                    dblAcSSCLImpto += dblSSCLImpto
                    dblAcSSMargenImpto += dblSSMargenImpto
                    dblAcSSMargenLiberadoImpto += dblSSMargenLiberadoImpto
                    dblAcSTCRImpto += dblSTCRImpto
                    dblAcSTMargenImpto += dblSTMargenImpto

SgteDet:
                    'End While
                Next

                'If dblTCambio = 0 Then
                dc.Add("CostoReal", dblAcCostoReal)
                dc.Add("Margen", dblAcMargen) ' + dblAcMargenLiberado)
                dc.Add("CostoLiberado", dblAcCostoLiberado)
                dc.Add("MargenLiberado", dblAcMargenLiberado)
                dc.Add("MargenAplicado", dblAcMargenAplicado)
                dc.Add("TotImpto", dblAcTotImpto)
                dc.Add("TotalOrig", dblAcTotal)
                dc.Add("Total", dblAcTotal) ' dblRedondeoInmedSuperior(Math.Round(dblAcTotal, 2), vsglRedondeo, sglRedondeadoTotal))
                dc.Add("RedondeoTotal", sglAcRedondeo) ' + sglRedondeadoTotal)

                dc.Add("SSCR", dblAcSSCR)
                dc.Add("SSMargen", dblRedondeoInmedSuperior(dblAcSSMargen, vsglRedondeo))
                dc.Add("SSCL", dblAcSSCL)
                dc.Add("SSML", dblAcSSML)
                dc.Add("SSTotalOrig", dblAcSSTotal)
                dc.Add("SSTotal", dblRedondeoInmedSuperior(dblAcSSTotal, vsglRedondeo))

                dc.Add("STCR", dblAcSTCR)
                dc.Add("STMargen", dblRedondeoInmedSuperior(dblAcSTMargen, vsglRedondeo))

                dc.Add("STTotalOrig", dblAcSTTotal)
                dc.Add("STTotal", dblRedondeoInmedSuperior(dblAcSTTotal, vsglRedondeo, , True))

                dc.Add("CostoRealImpto", dblAcCostoRealImpto)
                dc.Add("CostoLiberadoImpto", dblAcCostoLiberadoImpto)
                dc.Add("MargenImpto", dblAcMargen * sglIGV)

                dc.Add("MargenLiberadoImpto", dblAcMargenLiberadoImpto)
                dc.Add("SSCRImpto", dblAcSSCRImpto)
                dc.Add("SSCLImpto", dblAcSSCLImpto)
                dc.Add("SSMargenImpto", dblAcSSMargenImpto)
                dc.Add("SSMargenLiberadoImpto", dblAcSSMargenLiberadoImpto)
                dc.Add("STCRImpto", dblAcSTCRImpto)
                dc.Add("STMargenImpto", dblAcSTMargenImpto)
                'Else

                '    dc.Add("CostoReal", If(vdblCostoReal = 0, dblAcCostoReal / dblTCambio, vdblCostoReal))
                '    dc.Add("Margen", dblAcMargen / dblTCambio)
                '    dc.Add("CostoLiberado", dblAcCostoLiberado / dblTCambio)
                '    dc.Add("MargenLiberado", dblAcMargenLiberado / dblTCambio)
                '    dc.Add("MargenAplicado", dblAcMargenAplicado)
                '    dc.Add("TotImpto", dblAcTotImpto / dblTCambio)
                '    dc.Add("TotalOrig", dblAcTotal)
                '    dc.Add("Total", dblRedondeoInmedSuperior(Math.Round(dblAcTotal / dblTCambio, 2), vsglRedondeo, sglRedondeadoTotal))
                '    dc.Add("RedondeoTotal", (sglAcRedondeo / dblTCambio) + sglRedondeadoTotal)

                '    dc.Add("SSCR", dblAcSSCR / dblTCambio)
                '    dc.Add("SSMargen", dblAcSSMargen / dblTCambio)
                '    dc.Add("SSCL", dblAcSSCL / dblTCambio)
                '    dc.Add("SSML", dblAcSSML / dblTCambio)
                '    dc.Add("SSTotalOrig", dblAcSSTotal)
                '    dc.Add("SSTotal", dblRedondeoInmedSuperior(dblAcSSTotal / dblTCambio, vsglRedondeo))

                '    dc.Add("STCR", dblAcSTCR / dblTCambio)
                '    dc.Add("STMargen", dblAcSTMargen / dblTCambio)
                '    dc.Add("STTotalOrig", dblAcSTTotal)
                '    dc.Add("STTotal", dblRedondeoInmedSuperior(dblAcSTTotal / dblTCambio, vsglRedondeo))

                '    dc.Add("CostoRealImpto", dblAcCostoRealImpto / dblTCambio)
                '    dc.Add("CostoLiberadoImpto", dblAcCostoLiberadoImpto / dblTCambio)

                '    dc.Add("MargenImpto", (dblAcMargen * sglIGV) / dblTCambio)
                '    dc.Add("MargenLiberadoImpto", dblAcMargenLiberadoImpto / dblTCambio)
                '    dc.Add("SSCRImpto", dblAcSSCRImpto / dblTCambio)
                '    dc.Add("SSCLImpto", dblAcSSCLImpto / dblTCambio)
                '    dc.Add("SSMargenImpto", dblAcSSMargenImpto / dblTCambio)
                '    dc.Add("SSMargenLiberadoImpto", dblAcSSMargenLiberadoImpto / dblTCambio)
                '    dc.Add("STCRImpto", dblAcSTCRImpto / dblTCambio)
                '    dc.Add("STMargenImpto", dblAcSTMargenImpto / dblTCambio)


                'End If
            End If




            Return dc

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".CalculoCotizacion")
        End Try

    End Function
    Public Function dblCostoFueradeRango(ByVal vdttCostos As DataTable, ByVal vintPaxHastaMax As Int16, ByVal vintPax As Int16) As Double
        Try
            Dim dblMontoAnt As Double = 0
            Dim dblDifCostos As Double = 0
            Dim intTamanio As Int16 = 0
            Dim dblMontoMax As Double = 0
            For Each drCostos As DataRow In vdttCostos.Rows
                intTamanio = drCostos("PaxHasta") - drCostos("PaxDesde")
                If drCostos("PaxHasta") = vintPaxHastaMax Then
                    dblDifCostos = drCostos("Monto") - dblMontoAnt
                    dblMontoMax = drCostos("Monto")
                    Exit For
                End If
                dblMontoAnt = drCostos("Monto")
            Next
            If intTamanio > 0 Or dblMontoMax > 0 Then
                intTamanio += 1
                If dblDifCostos <> dblMontoMax Then
                    For intInd As Int16 = vintPaxHastaMax + 1 To vintPax - intTamanio Step intTamanio
                        dblMontoMax += dblDifCostos
                    Next
                Else 'sólo hay un costo en tabla

                    dblMontoMax += dblDifCostos

                End If
            End If


            Return dblMontoMax
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function dblRedondeoInmedSuperior(ByVal strValor As String, _
                                          ByVal strRedondeo As String, _
                                          Optional ByRef sglRedondeado As Single = 0, _
                                          Optional ByVal vblnPermiteNegativo As Boolean = False) As Double
        Dim dblReturn As Double = 0
        Dim strEntero As String
        Dim strDecimales As String

        If InStr(strValor, ".") > 0 Then
            strEntero = strValor.Substring(0, InStr(strValor, ".") - 1)
            strDecimales = Mid(strValor, InStr(strValor, ".") + 1)
        Else
            strEntero = strValor
            strDecimales = "0"
        End If

        If strDecimales.Length > 15 Then
            strDecimales = strDecimales.Substring(0, 15)
        End If
        If Not vblnPermiteNegativo Then
            strEntero = Replace(strEntero, "-", String.Empty)
        End If

        If strRedondeo = "1" Then
            If Convert.ToInt64(strDecimales) = 0 Then
                dblReturn = strEntero
            Else
                If Math.Abs(Convert.ToDouble(strEntero)) >= 0 Then
                    dblReturn = Math.Abs(Convert.ToDouble(strEntero)) + 1
                    If strEntero.Substring(0, 1) = "-" Then
                        dblReturn = dblReturn * (-1)
                    End If
                End If
            End If
        ElseIf strRedondeo = "0.5" Then
            If Convert.ToInt64(strDecimales) = 0 Then
                dblReturn = strEntero
            Else
                If Convert.ToByte(strDecimales.Substring(0, 1)) >= 5 Then
                    'dblReturn = Convert.ToDouble(strEntero) + 1
                    dblReturn = Math.Abs(Convert.ToDouble(strEntero)) + 1
                    If strEntero.Substring(0, 1) = "-" Then
                        dblReturn = dblReturn * (-1)
                    End If

                Else
                    dblReturn = strEntero & ".5"
                End If

            End If
        Else
            dblReturn = strValor
        End If

        sglRedondeado = Math.Abs(Convert.ToDouble(strValor) - dblReturn)
        Return dblReturn
    End Function


    Public Sub Copiar(ByVal vintIDCab As Integer, ByVal vstrIDUsuarioNue As String, _
                      ByVal vdatDia As Date, _
                      ByVal vstrUserMod As String, _
                      ByVal vblnCopiarTodosVuelos As Boolean, _
                      ByVal vintNuPedInt As Integer)

        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDUsuarioNue", vstrIDUsuarioNue)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@NuPedInt", vintNuPedInt)
            dc.Add("@pIDCab", 0)

            Dim intIDCabNue As Int32 = objADO.ExecuteSPOutput("COTICAB_Ins_CopiarNuevoUsuario", dc)
            Dim blnExisteServicioVarios As Boolean = False

            Dim objBE As New clsCotizacionBE
            objBE.CotizCopiada = True
            Dim ListaDetCot As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Using objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarTodosxIDCab(vintIDCab)
                    Dim objBEDet As clsCotizacionBE.clsDetalleCotizacionBE
                    While dr.Read
                        If Not CBool(dr("FlServicioTC")) Then
                            objBEDet = New clsCotizacionBE.clsDetalleCotizacionBE
                            With objBEDet
                                .Accion = "N"
                                .IDDet = dr("IDDet")
                                .IDCab = intIDCabNue
                                .Cotizacion = dr("Cotizacion")
                                .Item = dr("Item")
                                '.Dia = vdatDia
                                .Dia = dr("Dia")
                                'DiaNombre
                                .IDUbigeo = dr("IDUbigeo")
                                .IDProveedor = dr("IDProveedor")
                                .IDIdioma = dr("IDIdioma")
                                .Especial = dr("Especial")
                                .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")) = True, "", dr("MotivoEspecial"))
                                .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")) = True, "", dr("RutaDocSustento"))
                                .Desayuno = dr("Desayuno")
                                .Lonche = dr("Lonche")
                                .Almuerzo = dr("Almuerzo")
                                .Cena = dr("Cena")
                                .Transfer = dr("Transfer")
                                .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")) = True, "", dr("IDDetTransferOri"))
                                .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")) = True, "", dr("IDDetTransferDes"))
                                'CamaMat
                                .TipoTransporte = If(IsDBNull(dr("TipoTransporte")) = True, "", dr("TipoTransporte"))
                                .IDUbigeoOri = dr("IDUbigeoOri")
                                .IDUbigeoDes = dr("IDUbigeoDes")
                                .NroPax = dr("NroPax")
                                .NroLiberados = If(IsDBNull(dr("NroLiberados")) = True, 0, dr("NroLiberados"))
                                .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")) = True, "", dr("Tipo_Lib"))
                                .CostoReal = If(IsDBNull(dr("CostoReal")) = True, 0, dr("CostoReal"))
                                .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")) = True, 0, dr("CostoRealAnt"))
                                .CostoLiberado = If(IsDBNull(dr("CostoLiberado")) = True, 0, dr("CostoLiberado"))
                                .Margen = If(IsDBNull(dr("Margen")) = True, 0, dr("Margen"))
                                .MargenAplicado = If(IsDBNull(dr("MargenAplicado")) = True, 0, dr("MargenAplicado"))
                                .MargenAplicadoAnt = 0
                                .MargenLiberado = If(IsDBNull(dr("MargenLiberado")) = True, 0, dr("MargenLiberado"))
                                .RedondeoTotal = If(IsDBNull(dr("RedondeoTotal")) = True, 0, dr("RedondeoTotal"))
                                .Total = If(IsDBNull(dr("Total")) = True, 0, dr("Total"))
                                .TotalOrig = If(IsDBNull(dr("TotalOrig")) = True, 0, dr("TotalOrig"))
                                .SSCR = dr("SSCR")
                                .FlSSCREditado = dr("FlSSCREditado")
                                .SSCL = dr("SSCL")
                                .SSMargen = dr("SSMargen")
                                .SSMargenLiberado = dr("SSMargenLiberado")
                                .SSTotal = dr("SSTotal")
                                .SSTotalOrig = dr("SSTotalOrig")
                                .CostoRealImpto = dr("CostoRealImpto")
                                .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                                .MargenImpto = dr("MargenImpto")
                                .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                                .SSCRImpto = dr("SSCRImpto")
                                .SSCLImpto = dr("SSCLImpto")
                                .SSMargenImpto = dr("SSMargenImpto")
                                .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                                .STCR = dr("STCR")
                                .FlSTCREditado = dr("FlSTCREditado")
                                .STMargen = dr("STMargen")
                                .STTotal = dr("STTotal")
                                .STTotalOrig = dr("STTotalOrig")
                                .STCRImpto = dr("STCRImpto")
                                .STCRImpto = dr("STCRImpto")
                                .STMargenImpto = dr("STMargenImpto")
                                .TotImpto = dr("TotImpto")
                                .IDDetRel = If(IsDBNull(dr("IDDetRel")) = True, 0, dr("IDDetRel"))
                                .Servicio = If(IsDBNull(dr("Servicio")) = True, "", dr("Servicio"))
                                .IDDetCopia = dr("IDDet")
                                .Recalcular = False
                                .PorReserva = False
                                .ExecTrigger = dr("ExecTrigger")
                                .IncGuia = dr("IncGuia")
                                .FueradeHorario = dr("FueradeHorario")
                                .CostoRealEditado = dr("CostoRealEditado")
                                .CostoRealImptoEditado = dr("CostoRealImptoEditado")
                                .MargenAplicadoEditado = dr("MargenAplicadoEditado")
                                .ServicioEditado = dr("ServicioEditado")
                                .IDServicio = dr("IDServicio")
                                .IDServicio_Det = dr("IDServicioReal") 'dr("IDServicio_Det")
                                If CBool(dr("ServicioVarios")) Then blnExisteServicioVarios = True
                                .CalculoTarifaPendiente = CBool(dr("CalculoTarifaPendiente"))
                                .FlServicioTC = False
                                .UserMod = vstrUserMod
                            End With
                            ListaDetCot.Add(objBEDet)
                        End If
                    End While
                    dr.Close()

                End Using
            End Using
            objBE.ListaDetalleCotiz = ListaDetCot
            If blnExisteServicioVarios Then CopiarServiciosVariosxIDCab(objBE, vintIDCab, intIDCabNue)

            'Descripciones
            Dim ListaDescripciones As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE)
            Using objBNDesc As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionDescripServicioBN
                Using drDescr As SqlClient.SqlDataReader = objBNDesc.ConsultarTodosxIDCab(vintIDCab)
                    Dim objBEDesc As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE
                    While drDescr.Read
                        objBEDesc = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE
                        With objBEDesc
                            .Accion = "N"
                            .IDDet = drDescr("IDDet")
                            .IDIdioma = drDescr("IDIdioma")
                            .DescLarga = drDescr("DescLarga")
                            .NoHotel = drDescr("NoHotel")
                            .TxWebHotel = drDescr("TxWebHotel")

                            .TxDireccHotel = drDescr("TxDireccHotel")
                            .TxTelfHotel1 = drDescr("TxTelfHotel1")
                            .TxTelfHotel2 = drDescr("TxTelfHotel2")
                            .FeHoraChkInHotel = If(IsDBNull(drDescr("FeHoraChkInHotel")), "01/01/1900", "01/01/1900 " & drDescr("FeHoraChkInHotel"))
                            .FeHoraChkOutHotel = If(IsDBNull(drDescr("FeHoraChkOutHotel")), "01/01/1900", "01/01/1900 " & drDescr("FeHoraChkOutHotel"))
                            .CoTipoDesaHotel = drDescr("CoTipoDesaHotel")
                            .CoCiudadHotel = drDescr("CoCiudadHotel")
                            .UserMod = vstrUserMod
                        End With
                        ListaDescripciones.Add(objBEDesc)
                    End While
                    drDescr.Close()
                End Using
            End Using
            objBE.ListaDetalleCotizacionDescripServicio = ListaDescripciones

            'Vuelos
            Dim dtCotiCabtmp As DataTable = dttConsultarPk(intIDCabNue)

            Dim ListaVuelos As New List(Of clsCotizacionBE.clsCotizacionVuelosBE)
            Dim objVuelosBE As clsCotizacionBE.clsCotizacionVuelosBE
            Dim objVuelosBN As New clsCotizacionBN.clsCotizacionVuelosBN
            If Not vblnCopiarTodosVuelos Then
                For Each ItemIDDet As clsCotizacionBE.clsDetalleCotizacionBE In ListaDetCot
                    Using drVuelo As SqlClient.SqlDataReader = objVuelosBN.ConsultarxIDCabIDDet(vintIDCab, ItemIDDet.IDDet)
                        While drVuelo.Read
                            objVuelosBE = New clsCotizacionBE.clsCotizacionVuelosBE
                            With objVuelosBE
                                .ID = drVuelo("ID")
                                .IdCab = intIDCabNue
                                .IDDet = drVuelo("Iddet")
                                .Cotizacion = If(IsDBNull(drVuelo("Cotizacion")) = True, "", drVuelo("Cotizacion")).ToString.Trim
                                .NombrePax = drVuelo("NombrePax")
                                .Ruta = drVuelo("Ruta")
                                .RutaD = drVuelo("RutaD")
                                .RutaO = drVuelo("RutaO")
                                If drVuelo("TipoTransporte").ToString <> "V" Then
                                    .Vuelo = ""
                                    .NumBoleto = ""
                                    If drVuelo("TipoTransporte").ToString = "T" Then
                                        .FecEmision = CDate(dtCotiCabtmp(0)("Fecha")).ToShortDateString
                                    Else
                                        .FecEmision = "01/01/1900"
                                    End If
                                    .FecDocumento = "01/01/1900"
                                    .FecRetorno = "01/01/1900"
                                    .Salida = "00:00"
                                    .HrRecojo = "00:00"
                                    .Llegada = "00:00"
                                    .Status = ""
                                    .Emitido = ""
                                    .CodReserva = ""
                                    .Comentario = ""
                                    .FecSalida = "01/01/1900"
                                Else
                                    .Vuelo = drVuelo("Vuelo")
                                    .NumBoleto = If(IsDBNull(drVuelo("Num_Boleto")) = True, "", drVuelo("Num_Boleto")).ToString.Trim
                                    .FecEmision = If(IsDBNull(drVuelo("Fec_Emision")) = True, "01/01/1900", CDate(drVuelo("Fec_Emision")).ToShortDateString)
                                    .FecDocumento = If(IsDBNull(drVuelo("Fec_Documento")) = True, "01/01/1900", CDate(drVuelo("Fec_Documento")).ToShortDateString)
                                    .FecRetorno = If(IsDBNull(drVuelo("Fec_Retorno")) = True, "01/01/1900", CDate(drVuelo("Fec_Retorno")).ToShortDateString)
                                    .Salida = drVuelo("Salida")
                                    .HrRecojo = If(IsDBNull(drVuelo("HrRecojo")) = True, "01/01/1900", drVuelo("HrRecojo"))
                                    .Llegada = drVuelo("Llegada")
                                    .Status = drVuelo("Status")
                                    .Emitido = drVuelo("Emitido")
                                    .CodReserva = If(IsDBNull(drVuelo("CodReserva")) = True, "", drVuelo("CodReserva"))
                                    .Comentario = drVuelo("Comentario")
                                    .FecSalida = If(IsDBNull(drVuelo("Fec_Salida")) = True, "01/01/1900", CDate(drVuelo("Fec_Salida")).ToShortDateString)
                                End If
                                .Servicio = If(IsDBNull(drVuelo("Servicio")) = True, "", drVuelo("Servicio"))
                                .PaxC = drVuelo("PaxC")
                                .PCotizado = drVuelo("PCotizado")
                                .TCotizado = drVuelo("TCotizado")
                                .PaxV = drVuelo("PaxV")
                                .PVolado = drVuelo("PVolado")
                                .TVolado = drVuelo("TVolado")
                                .TipoTransporte = drVuelo("TipoTransporte")
                                .Tarifa = drVuelo("Tarifa")
                                .PTA = drVuelo("PTA")
                                .ImpuestosExt = drVuelo("ImpuestosExt")
                                .Penalidad = drVuelo("Penalidad")
                                .IGV = drVuelo("IGV")
                                .Otros = drVuelo("Otros")
                                .PorcComm = drVuelo("PorcComm")
                                .MontoContado = drVuelo("MontoContado")
                                .MontoTarjeta = drVuelo("MontoTarjeta")
                                .IDTarjetaCredito = drVuelo("IdTarjCred")
                                .NumeroTarjeta = If(IsDBNull(drVuelo("NumeroTarjeta")) = True, "", drVuelo("NumeroTarjeta"))
                                .Aprobacion = drVuelo("Aprobacion")
                                .PorcOver = drVuelo("PorcOver")
                                .PorcOverInCom = drVuelo("PorcOverInCom")
                                .MontComm = drVuelo("MontComm")
                                .MontCommOver = drVuelo("MontCommOver")
                                .IGVComm = drVuelo("IGVComm")
                                .IGVCommOver = drVuelo("IGVCommOver")
                                .PorcTarifa = drVuelo("PorcTarifa")
                                .NetoPagar = drVuelo("NetoPagar")
                                .Descuento = drVuelo("Descuento")
                                .IDLineaA = drVuelo("IdLineaA")
                                .MontoFEE = drVuelo("MontoFEE")
                                .IGVFEE = drVuelo("IGVFEE")
                                .TotalFEE = drVuelo("TotalFEE")
                                .EmitidoSetours = drVuelo("EmitidoSetours")
                                .EmitidoOperador = drVuelo("EmitidoOperador")
                                .IDProveedorOperador = If(IsDBNull(drVuelo("IDProveedorOperador")) = True, "", drVuelo("IDProveedorOperador"))
                                .CostoOperador = If(IsDBNull(drVuelo("CostoOperador")) = True, 0, drVuelo("CostoOperador"))
                                .IdaVuelta = If(IsDBNull(drVuelo("IdaVuelta")) = True, "", drVuelo("IdaVuelta"))
                                .IDServicio_DetPeruRail = If(IsDBNull(drVuelo("IDServicio_DetPeruRail")) = True, 0, drVuelo("IDServicio_DetPeruRail"))
                                .CoTicket = ""
                                .NuSegmento = 0
                                .UserMod = vstrUserMod
                                .Accion = "N"
                            End With
                            ListaVuelos.Add(objVuelosBE)
                        End While
                        drVuelo.Close()
                    End Using
                Next
            Else
                Using drVuelo As SqlClient.SqlDataReader = objVuelosBN.ConsultarxIDCab(vintIDCab)
                    While drVuelo.Read
                        objVuelosBE = New clsCotizacionBE.clsCotizacionVuelosBE
                        With objVuelosBE
                            .ID = drVuelo("ID")
                            .IdCab = intIDCabNue
                            .IDDet = If(IsDBNull(drVuelo("Iddet")) = True, 0, drVuelo("Iddet"))
                            .Cotizacion = If(IsDBNull(drVuelo("Cotizacion")) = True, "", drVuelo("Cotizacion")).ToString.Trim
                            .NombrePax = drVuelo("NombrePax")
                            .Ruta = drVuelo("Ruta")
                            .RutaD = drVuelo("RutaD")
                            .RutaO = drVuelo("RutaO")
                            If drVuelo("TipoTransporte").ToString <> "V" Then
                                .Vuelo = ""
                                .NumBoleto = ""
                                If drVuelo("TipoTransporte").ToString = "T" Then
                                    .FecEmision = CDate(dtCotiCabtmp(0)("Fecha")).ToShortDateString
                                Else
                                    .FecEmision = "01/01/1900"
                                End If
                                .FecDocumento = "01/01/1900"
                                .FecRetorno = "01/01/1900"
                                .Salida = "00:00"
                                .HrRecojo = "00:00"
                                .Llegada = "00:00"
                                .Status = ""
                                .Emitido = ""
                                .CodReserva = ""
                                .Comentario = ""
                                .FecSalida = "01/01/1900"
                            Else
                                .Vuelo = drVuelo("Vuelo")
                                .NumBoleto = If(IsDBNull(drVuelo("Num_Boleto")) = True, "", drVuelo("Num_Boleto")).ToString.Trim
                                .FecEmision = If(IsDBNull(drVuelo("Fec_Emision")) = True, "01/01/1900", CDate(drVuelo("Fec_Emision")).ToShortDateString)
                                .FecDocumento = If(IsDBNull(drVuelo("Fec_Documento")) = True, "01/01/1900", CDate(drVuelo("Fec_Documento")).ToShortDateString)
                                .FecRetorno = If(IsDBNull(drVuelo("Fec_Retorno")) = True, "01/01/1900", CDate(drVuelo("Fec_Retorno")).ToShortDateString)
                                .Salida = drVuelo("Salida")
                                .HrRecojo = If(IsDBNull(drVuelo("HrRecojo")) = True, "01/01/1900", drVuelo("HrRecojo"))
                                .Llegada = drVuelo("Llegada")
                                .Status = drVuelo("Status")
                                .Emitido = drVuelo("Emitido")
                                .CodReserva = If(IsDBNull(drVuelo("CodReserva")) = True, "", drVuelo("CodReserva"))
                                .Comentario = drVuelo("Comentario")
                                .FecSalida = If(IsDBNull(drVuelo("Fec_Salida")) = True, "01/01/1900", CDate(drVuelo("Fec_Salida")).ToShortDateString)
                            End If
                            .Servicio = If(IsDBNull(drVuelo("Servicio")) = True, "", drVuelo("Servicio"))
                            .PaxC = drVuelo("PaxC")
                            .PCotizado = drVuelo("PCotizado")
                            .TCotizado = drVuelo("TCotizado")
                            .PaxV = drVuelo("PaxV")
                            .PVolado = drVuelo("PVolado")
                            .TVolado = drVuelo("TVolado")
                            .TipoTransporte = drVuelo("TipoTransporte")
                            .Tarifa = drVuelo("Tarifa")
                            .PTA = drVuelo("PTA")
                            .ImpuestosExt = drVuelo("ImpuestosExt")
                            .Penalidad = drVuelo("Penalidad")
                            .IGV = drVuelo("IGV")
                            .Otros = drVuelo("Otros")
                            .PorcComm = drVuelo("PorcComm")
                            .MontoContado = drVuelo("MontoContado")
                            .MontoTarjeta = drVuelo("MontoTarjeta")
                            .IDTarjetaCredito = drVuelo("IdTarjCred")
                            .NumeroTarjeta = If(IsDBNull(drVuelo("NumeroTarjeta")) = True, "", drVuelo("NumeroTarjeta"))
                            .Aprobacion = drVuelo("Aprobacion")
                            .PorcOver = drVuelo("PorcOver")
                            .PorcOverInCom = drVuelo("PorcOverInCom")
                            .MontComm = drVuelo("MontComm")
                            .MontCommOver = drVuelo("MontCommOver")
                            .IGVComm = drVuelo("IGVComm")
                            .IGVCommOver = drVuelo("IGVCommOver")
                            .PorcTarifa = drVuelo("PorcTarifa")
                            .NetoPagar = drVuelo("NetoPagar")
                            .Descuento = drVuelo("Descuento")
                            .IDLineaA = drVuelo("IdLineaA")
                            .MontoFEE = drVuelo("MontoFEE")
                            .IGVFEE = drVuelo("IGVFEE")
                            .TotalFEE = drVuelo("TotalFEE")
                            .EmitidoSetours = drVuelo("EmitidoSetours")
                            .EmitidoOperador = drVuelo("EmitidoOperador")
                            .IDProveedorOperador = If(IsDBNull(drVuelo("IDProveedorOperador")) = True, "", drVuelo("IDProveedorOperador"))
                            .CostoOperador = If(IsDBNull(drVuelo("CostoOperador")) = True, 0, drVuelo("CostoOperador"))
                            .IdaVuelta = If(IsDBNull(drVuelo("IdaVuelta")) = True, "", drVuelo("IdaVuelta"))
                            .IDServicio_DetPeruRail = If(IsDBNull(drVuelo("IDServicio_DetPeruRail")) = True, 0, drVuelo("IDServicio_DetPeruRail"))
                            .CoTicket = ""
                            .NuSegmento = 0
                            .UserMod = vstrUserMod
                            .Accion = "N"
                        End With
                        ListaVuelos.Add(objVuelosBE)
                    End While
                    drVuelo.Close()
                End Using
            End If

            objBE.ListaCotizacionVuelo = ListaVuelos


            Dim ListaVuelosPax As New List(Of clsCotizacionBE.clsCotizacionVuelosBE.clsPaxTransporteBE)
            Dim objVuelosPaxBE As clsCotizacionBE.clsCotizacionVuelosBE.clsPaxTransporteBE
            Dim objVuelosPaxBN As New clsPaxTransporteBN
            For Each ItemVuelo As clsCotizacionBE.clsCotizacionVuelosBE In ListaVuelos
                Using drVuelosPax As SqlClient.SqlDataReader = objVuelosPaxBN.ConsultarxTransport(ItemVuelo.ID)
                    While drVuelosPax.Read
                        objVuelosPaxBE = New clsCotizacionBE.clsCotizacionVuelosBE.clsPaxTransporteBE
                        With objVuelosPaxBE
                            .IDTransporte = drVuelosPax("IDTransporte")
                            .IDPaxTransp = drVuelosPax("IDPaxTransp")
                            .IDPax = If(IsDBNull(drVuelosPax("IDPax")) = True, 0, drVuelosPax("IDPax"))
                            .CodReservaTransp = If(IsDBNull(drVuelosPax("CodReservaTransp")) = True, "", drVuelosPax("CodReservaTransp"))
                            .IDProveedorGuia = If(IsDBNull(drVuelosPax("IDProveedorGuia")) = True, "", drVuelosPax("IDProveedorGuia"))
                            .UserMod = vstrUserMod
                            .Accion = ""
                        End With
                        ListaVuelosPax.Add(objVuelosPaxBE)
                    End While
                    drVuelosPax.Close()
                End Using
            Next
            objBE.ListaPaxTransporte = ListaVuelosPax


            'DetallesQuiebres
            Dim ListQuiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
            Using objBNQuiDet As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionQuiebresBN
                Using drQuiDet As SqlClient.SqlDataReader = objBNQuiDet.ConsultarTodosxIDCab(vintIDCab)
                    Dim objBEQuiDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE
                    While drQuiDet.Read
                        objBEQuiDet = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE
                        With objBEQuiDet
                            .Accion = "N"
                            .IDCab_Q = drQuiDet("IDCAB_Q")
                            .IDDet = drQuiDet("IDDet")
                            .CostoReal = drQuiDet("CostoReal")
                            .CostoLiberado = drQuiDet("CostoLiberado")
                            .Margen = drQuiDet("Margen")
                            .MargenAplicado = drQuiDet("MargenAplicado")
                            .MargenLiberado = drQuiDet("MargenLiberado")
                            .Total = drQuiDet("Total")
                            .RedondeoTotal = drQuiDet("RedondeoTotal")
                            .SSCR = drQuiDet("SSCR")
                            .SSCL = drQuiDet("SSCL")
                            .SSMargen = drQuiDet("SSMargen")
                            .SSMargenLiberado = drQuiDet("SSMargenLiberado")
                            .SSTotal = drQuiDet("SSTotal")
                            .STCR = drQuiDet("STCR")
                            .STMargen = drQuiDet("STMargen")
                            .STTotal = drQuiDet("STTotal")
                            .CostoRealImpto = drQuiDet("CostoRealImpto")
                            .CostoLiberadoImpto = drQuiDet("CostoLiberadoImpto")
                            .MargenImpto = drQuiDet("MargenImpto")
                            .MargenLiberadoImpto = drQuiDet("MargenLiberadoImpto")
                            .SSCRImpto = drQuiDet("SSCRImpto")
                            .SSCLImpto = drQuiDet("SSCLImpto")
                            .SSMargenImpto = drQuiDet("SSMargenImpto")
                            .SSMargenLiberadoImpto = drQuiDet("SSMargenLiberadoImpto")
                            .STCRImpto = drQuiDet("STCRImpto")
                            .STMargenImpto = drQuiDet("STMargenImpto")
                            .TotImpto = drQuiDet("TotImpto")
                            .UserMod = vstrUserMod
                        End With
                        ListQuiDet.Add(objBEQuiDet)
                    End While
                    drQuiDet.Close()
                End Using
            End Using
            objBE.ListaDetalleCotizacionQuiebres = ListQuiDet

            'Cabecera Quiebres
            Dim ListQuiebresCab As New List(Of clsCotizacionBE.clsCotizacionQuiebreBE)
            Using ObjBNQuiCab As New clsCotizacionBN.clsCotizacionQuiebresBN
                Using drQui As SqlClient.SqlDataReader = ObjBNQuiCab.ConsultarListCopia(vintIDCab)
                    Dim objBEQuiCab As clsCotizacionBE.clsCotizacionQuiebreBE
                    While drQui.Read
                        objBEQuiCab = New clsCotizacionBE.clsCotizacionQuiebreBE
                        With objBEQuiCab
                            .Accion = "N"
                            .IDCab_Q = drQui("IDCAB_Q")
                            .IDCab = intIDCabNue
                            .Pax = drQui("Pax")
                            .Liberados = If(IsDBNull(drQui("Liberados")) = True, 0, drQui("Liberados"))
                            .Tipo_Lib = If(IsDBNull(drQui("Tipo_Lib")) = True, "", drQui("Tipo_Lib"))
                            .UserMod = vstrUserMod
                        End With
                        ListQuiebresCab.Add(objBEQuiCab)
                    End While
                    drQui.Close()
                End Using
            End Using
            objBE.ListaCotizQuiebre = ListQuiebresCab


            Dim objQui As New clsQuiebresVentasCotizacionBT
            objQui.InsertarActualizarEliminar(objBE)

            'DetServicios
            Dim ListCotDetServ As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE)
            Using objBNCotDetServ As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionDetServiciosBN
                Using drCotDetServ As SqlClient.SqlDataReader = objBNCotDetServ.ConsultarTodosxIDCab(vintIDCab)
                    Dim objBECotDetServ As New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE
                    While drCotDetServ.Read
                        objBECotDetServ = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE
                        With objBECotDetServ
                            .Accion = "N"
                            .IDDet = drCotDetServ("IDDET")
                            .IDServicio_Det = drCotDetServ("IDServicio_Det")
                            .CostoReal = drCotDetServ("CostoReal")
                            .CostoLiberado = drCotDetServ("CostoLiberado")
                            .Margen = drCotDetServ("Margen")
                            .MargenAplicado = drCotDetServ("MargenAplicado")
                            .MargenLiberado = drCotDetServ("MargenLiberado")
                            .Total = drCotDetServ("Total")
                            .SSCR = drCotDetServ("SSCR")
                            .SSCL = drCotDetServ("SSCL")
                            .SSMargen = drCotDetServ("SSMargen")
                            .SSMargenLiberado = drCotDetServ("SSMargenLiberado")
                            .SSTotal = drCotDetServ("SSTotal")
                            .STCR = drCotDetServ("STCR")
                            .STMargen = drCotDetServ("STMargen")
                            .STTotal = drCotDetServ("STTotal")
                            .CostoRealImpto = drCotDetServ("CostoRealImpto")
                            .CostoLiberadoImpto = drCotDetServ("CostoLiberadoImpto")
                            .MargenImpto = drCotDetServ("MargenImpto")
                            .MargenLiberadoImpto = drCotDetServ("MargenLiberadoImpto")
                            .SSCRImpto = drCotDetServ("SSCRImpto")
                            .SSCLImpto = drCotDetServ("SSCLImpto")
                            .SSMargenImpto = drCotDetServ("SSMargenImpto")
                            .SSMargenLiberadoImpto = drCotDetServ("SSMargenLiberadoImpto")
                            .STCRImpto = drCotDetServ("STCRImpto")
                            .STMargenImpto = drCotDetServ("STMargenImpto")
                            .TotImpto = drCotDetServ("TotImpto")
                            .RedondeoTotal = drCotDetServ("RedondeoTotal")
                            .UserMod = vstrUserMod
                        End With
                        ListCotDetServ.Add(objBECotDetServ)
                    End While
                    drCotDetServ.Close()
                End Using
            End Using
            objBE.ListaDetalleCotizacionDetServicio = ListCotDetServ

            Dim dtCotiCab As DataTable = dttConsultarPk(vintIDCab)

            Dim objBTDet As New clsDetalleVentasCotizacionBT
            objBTDet.InsertarActualizarEliminar(objBE)

            'PAX()
            ''Dim dtCotiCab As DataTable = dttConsultarPk(vintIDCab)
            'drCotiCab.Read()
            Dim intNroPax As Int16 = dtCotiCab(0)("NroPax")
            Dim bytNroLiberados As Int16 = If(IsDBNull(dtCotiCab(0)("NroLiberados")) = True, 0, dtCotiCab(0)("NroLiberados"))
            Dim strIDPais As String = dtCotiCab(0)("IDPais")
            Dim intTotalPax As Int16 = intNroPax + bytNroLiberados
            'drCotiCab.Close()
            Dim strTipoDocPasaporte As String = "004"
            Dim objBEPax As clsCotizacionBE.clsCotizacionPaxBE
            Dim ListaPax As New List(Of clsCotizacionBE.clsCotizacionPaxBE)
            Dim dttCotiPaxAnt As Data.DataTable = ConsultarPaxxIDCab(vintIDCab)
            Dim intFila As Byte = 1
            For Each dRowPaxNuevos As DataRow In dttCotiPaxAnt.Rows
                objBEPax = New clsCotizacionBE.clsCotizacionPaxBE
                With objBEPax
                    .Accion = "N"
                    .Apellidos = "Pax " & intFila '+ 1
                    .Nombres = "Pax " & intFila '+ 1
                    .FecNacimiento = "01/01/1900"
                    .IDCab = intIDCabNue
                    .IDIdentidad = strTipoDocPasaporte
                    .IDNacionalidad = strIDPais
                    .NumIdentidad = ""
                    .Titulo = "Mr."
                    .ObservEspecial = ""
                    .IDPaisResidencia = "0000001"
                    .FecIngresoPais = CDate("01/01/1900")
                    .Orden = intFila
                    .UserMod = vstrUserMod
                    .IDPax = dRowPaxNuevos("IDPax")
                    .TC = False
                    .TituloAcademico = "---"
                    .NoShow = False
                    intFila += 1
                End With
                ListaPax.Add(objBEPax)
            Next

            objBE = New clsCotizacionBE
            objBE.ListaPax = ListaPax
            objBE.ListaPaxTransporte = ListaVuelosPax
            Dim objBT As New clsPaxVentasCotizacionBT
            objBT.InsertarActualizarEliminar(objBE)

            LstPaxTransporte(ListaVuelosPax)
            objBE.ListaPaxTransporte = ListaVuelosPax
            Dim objBTPaxTrans As New clsPaxTransporteBT
            objBTPaxTrans.InsertarActualizarEliminar(objBE)

            'DETPAX
            Dim dttIDDET As DataTable = objBTDet.ConsultarIDDetxIDCab(intIDCabNue)
            'Dim objPaxDet As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
            Dim objPaxDet As New clsDetallePaxVentasCotizacionBT
            For Each drIDDet As DataRow In dttIDDET.Rows
                objPaxDet.InsertarxIDDet(intIDCabNue, drIDDet("IDDet"), drIDDet("IDTipoProv"), vstrUserMod)
                objBTDet.ActualizarNroPax(drIDDet("IDDet"), vstrUserMod)
            Next

            'TIPOS DE CAMBIO
            Dim ListTipoCambio As New List(Of clsCotizacionBE.clsCotizacionTiposCambioBE)
            Using objTCambioBN As New clsCotizacionBN.clsCotizacionTiposCambioBN
                Using drTCam As SqlClient.SqlDataReader = objTCambioBN.drConsultarList(vintIDCab)
                    While drTCam.Read
                        Dim ItemTCambio As New clsCotizacionBE.clsCotizacionTiposCambioBE With { _
                        .IDCab = intIDCabNue, _
                        .CoMoneda = drTCam("CoMoneda"), _
                        .SsTipCam = drTCam("SsTipCam"), _
                        .Accion = "N", _
                        .UserMod = vstrUserMod _
                        }
                        ListTipoCambio.Add(ItemTCambio)
                    End While
                    drTCam.Close()
                End Using
            End Using
            objBE.ListaTipoCambios = ListTipoCambio

            Dim objTCamBT As New clsTiposCambioVentasCotizacionBT
            objTCamBT.InsertarActualizarEliminar(objBE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Copiar")
        End Try
    End Sub

    ' ''Private Sub CopiarServiciosVarios(ByRef BE As clsCotizacionBE)
    ' ''    If BE.ListaDetalleCotiz Is Nothing Then Exit Sub
    ' ''    Try
    ' ''        'Dim blnExisteDetalleCopiadoSV As Boolean = False
    ' ''        For Each ItemST As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
    ' ''            If Not IsNumeric(ItemST.IDDet) And ItemST.Accion = "N" Then
    ' ''                If Not ItemST.IDDetCopia Is Nothing Then
    ' ''                    Dim strIDDetCopia As String = ItemST.IDDetCopia.ToString()
    ' ''                    Dim intIDCabCopia As Integer = 0

    ' ''                    If Not (strIDDetCopia = String.Empty Or strIDDetCopia = "0") Then
    ' ''                        intIDCabCopia = intDevuelveIDCabxIDDet(CInt(strIDDetCopia))

    ' ''                        If intIDCabCopia <> BE.IdCab Then
    ' ''                            CopiarServiciosVariosxIDCab(BE, intIDCabCopia, BE.IdCab)
    ' ''                            ItemST.FlServicioCopiadoVarios = True
    ' ''                        End If
    ' ''                    End If
    ' ''                End If
    ' ''            End If
    ' ''        Next

    ' ''        'If blnExisteDetalleCopiadoSV Then
    ' ''        '    CopiarServiciosVariosxIDCab(BE, )
    ' ''        'End If
    ' ''        ContextUtil.SetComplete()
    ' ''    Catch ex As Exception
    ' ''        ContextUtil.SetAbort()
    ' ''        Throw
    ' ''    End Try
    ' ''End Sub

    Private Sub InsertarDetallesxCopiarxIDServicioxIDCab(ByVal vstrIDServicioAntiguo As String, _
                                                         ByVal vstrIDServicioNuevo As String, _
                                                         ByVal vintIDCab As Integer, _
                                                         ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicioAnt", vstrIDServicioAntiguo)
                .Add("@IDServicioNuev", vstrIDServicioNuevo)
                .Add("@IDCab", vintIDCab)
                .Add("@UserMod", vstrUserMod)
            End With

            objADO.ExecuteSP("MASERVICIOS_COPIA_COTICAB_InsxIDServicio_Det", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Sub ActualizarNuDetallexIDServicio_DetxIDCab(ByVal vintIDCab As Integer, _
                                                         ByVal vintIDServicio_Det As Integer, _
                                                         ByVal vintIDServicio_DetNuevo As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@IDServicio_Det", vintIDServicio_Det)
                .Add("@IDServicio_DetNuevo", vintIDServicio_DetNuevo)
            End With

            objADO.ExecuteSP("MASERVICIOS_COPIA_COTICAB_UpdIDServicio_DetNuevo", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function blnExisteServicioYaRegistrado(ByVal vstrIDServicio As String, ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicio", vstrIDServicio)
                .Add("@IDCab", vintIDCab)
                .Add("@pExist", 0)
            End With

            Return objADO.GetSPOutputValue("MASERVICIOS_ExistsxIDServicio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExisteServicioDetYaRegistrado(ByVal vintIDServicio_Det As Integer, ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicio_Det", vintIDServicio_Det)
                .Add("@IDCab", vintIDCab)
                .Add("@pExists", 0)
            End With

            Return objADO.GetSPOutputValue("MASERVICIO_Det_ExistsxIDServicio_Det", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function objCopiaServiciosVariosxDetalles(ByVal BE As clsCotizacionBE) As clsCotizacionBE.clsCopiaDetalleBE
        If BE.CopiaDetalleVarios Is Nothing Then Return Nothing
        Try
            Dim BTDetServ As New clsServicioProveedorBT
            Dim objCopia As clsCotizacionBE.clsCopiaDetalleBE = BE.CopiaDetalleVarios
            For Each Item As clsCotizacionBE.clsCopiaDetalle_ServicioBE In objCopia.ListServicios
                If Not blnExisteServicioYaRegistrado(Item.IDServicioAntiguo, BE.IdCab) Then
                    Dim strIDServEval As String = strDevuelveIDServicioRel(Item.IDServicioAntiguo, BE.IdCab)
                    If strIDServEval = String.Empty Then
                        Item.IDServicioNuevo = BTDetServ.InsertarCopiaCotizxIDServicio(Item.IDServicioAntiguo, BE.IdCab)
                    Else
                        Item.IDServicioNuevo = strIDServEval
                    End If
                    InsertarDetallesxCopiarxIDServicioxIDCab(Item.IDServicioAntiguo, Item.IDServicioNuevo, BE.IdCab, BE.UserMod)

                    For Each Item2 As clsCotizacionBE.clsCopiaDetalle_DetServicioBE In objCopia.ListDetServicios
                        If Item2.IDServicio = Item.IDServicioAntiguo Then
                            Item2.IDServicio = Item.IDServicioNuevo
                        End If
                    Next
                Else
                    Item.IDServicioNuevo = Item.IDServicioAntiguo
                End If
            Next

            Dim objBLCostos As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
            For Each Item2 As clsCotizacionBE.clsCopiaDetalle_DetServicioBE In objCopia.ListDetServicios
                If Not blnExisteServicioDetYaRegistrado(Item2.IDServicio_DetAntiguo, BE.IdCab) Then
                    Dim intIDServicio_DetRel As Integer = intDevuelveIDServicio_DetRel(Item2.IDServicio_DetAntiguo, BE.IdCab)
                    If intIDServicio_DetRel = 0 Then
                        Item2.IDServicio_DetNuevo = InsertarCopiaxIDServicio_Det(Item2.IDServicio_DetAntiguo, Item2.IDServicio)
                        ActualizarNuDetallexIDServicio_DetxIDCab(BE.IdCab, Item2.IDServicio_DetAntiguo, Item2.IDServicio_DetNuevo)

                        objBLCostos.InsertarCopiaCotizxIDServicios_Det(Item2.IDServicio_DetAntiguo, Item2.IDServicio_DetNuevo)
                    Else
                        Item2.IDServicio_DetNuevo = intIDServicio_DetRel
                    End If
                Else
                    Item2.IDServicio_DetNuevo = Item2.IDServicio_DetAntiguo
                End If
            Next

            ContextUtil.SetComplete()
            Return objCopia
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Function strDevuelveIDServicioRel(ByVal vstrIDServicioAntiguo As String, ByVal vintIDCab As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicioAntiguo", vstrIDServicioAntiguo)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pIDServicio", "")

            Return objADO.GetSPOutputValue("MASERVICIOS_COPIA_COTICAB_SelIDServicio", dc).Trim()
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function intDevuelveIDServicio_DetRel(ByVal vintIDServicio_DetAntiguo As Integer, ByVal vintIDCab As Integer) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDServicio_DetAntiguo", vintIDServicio_DetAntiguo)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pIDServicio_Det", 0)

            Return objADO.GetSPOutputValue("MASERVICIOS_COPIA_COTICAB_SelxIDServicio_Det", dc).Trim()
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub CopiarServiciosVariosxIDCab(ByRef BE As clsCotizacionBE, ByVal vintIDCabOrigen As Integer, ByVal vintIDCabDestino As Integer)
        Try
            Dim objBTServicios As New clsServicioProveedorBT
            Dim objBLCostos As New clsServicioProveedorBT.clsDetalleServicioProveedorBT.clsCostosDetalleServicioProveedorBT
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCabVarios", vintIDCabOrigen)
            Dim dtServiciosVars As Data.DataTable = objADO.GetDataTable("COTIDET_SelxIDCabVarios", dc)
            Dim LstIDServicios As New List(Of String)

            For Each DRowItem As Data.DataRow In dtServiciosVars.Rows
                If Not LstIDServicios.Contains(DRowItem("IDServicio").ToString) Then
                    Dim strIDServicioNuevo As String = objBTServicios.InsertarCopiaCotizxIDServicio(DRowItem("IDServicio"), vintIDCabDestino)
                    'objBLDet.InsertarCopiaxIDServicio(DRowItem("IDServicio"), strIDServicioNuevo)
                    'Dim intIDServicio_Det As Integer = objBTServicios.intIDServiciosDetxIDServicioxAnioxVariantexServicio(strIDServicioNuevo, _
                    '                                                                                                      DRowItem("Anio"), _
                    '                                                                                                      DRowItem("Variante"), _
                    '                                                                                                      DRowItem("Servicio"))
                    'Obteniendo los MASERVICIOS_DET
                    dc = New Dictionary(Of String, String)
                    dc.Add("@IDServicio", DRowItem("IDServicio"))
                    Dim dtServiciosDet As Data.DataTable = objADO.GetDataTable("MASERVICIOS_DET_SelxIDServicio", dc)

                    For Each dRowItem2 As Data.DataRow In dtServiciosDet.Rows
                        Dim intIDServicioDetNuevo As Integer = InsertarCopiaxIDServicio_Det(dRowItem2("IDServicio_Det"), strIDServicioNuevo)
                        If Convert.ToInt32(dRowItem2("IDServicio_Det_V")) <> 0 Then
                            objBLCostos.InsertarCopiaCotizxIDServicios_Det(dRowItem2("IDServicio_Det_V"), intIDServicioDetNuevo)
                        Else
                            objBLCostos.InsertarCopiaCotizxIDServicios_Det(dRowItem2("IDServicio_Det"), intIDServicioDetNuevo)
                        End If


                        For Each itemDet As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                            If itemDet.IDServicio_Det = CInt(dRowItem2("IDServicio_Det")) And itemDet.IDServicio = DRowItem("IDServicio").ToString Then
                                itemDet.IDServicio_Det = intIDServicioDetNuevo
                                itemDet.IDServicio = strIDServicioNuevo
                            End If
                        Next
                    Next
                    LstIDServicios.Add(DRowItem("IDServicio"))
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function InsertarCopiaxIDServicio_Det(ByVal vintIDServicio_Det As Integer, ByVal vstrIDServicioNuevo As String) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim intIDServicio_Det As Integer = 0
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@IDServicioNuevo", vstrIDServicioNuevo)
            dc.Add("@pIDServicio_Det", 0)
            intIDServicio_Det = objADO.GetSPOutputValue("MASERVICIOS_DETInsxIDServicio_Det", dc)

            ContextUtil.SetComplete()
            Return intIDServicio_Det
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Private Sub LstPaxTransporte(ByRef vLstPaxTransporte As List(Of clsCotizacionBE.clsCotizacionVuelosBE.clsPaxTransporteBE))
        Try
            For Each Item As clsCotizacionBE.clsCotizacionVuelosBE.clsPaxTransporteBE In vLstPaxTransporte
                Item.Accion = "N"
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function ConsultarProveedoresNoReservados(ByVal vintIDCab As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Return objADO.GetDataTable("COTIDET_SelProveeNoReservados", dc)

    End Function

    Public Function dttConsultarPk(ByVal vintIDCab As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Dim strSql As String = "COTICAB_Sel_Pk"
        Return objADO.GetDataTable(strSql, dc)

    End Function

    Public Function ConsultarPaxxIDCab(ByVal vintIDCab As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable("COTIPAX_Sel_ListxIDCab", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'jorge
    Public Sub ActualizarPaxReservas_Excel(ByVal BE As clsCotizacionBE, Optional ByVal vstrNoRutaArchivoExcelPax As String = "", Optional ByVal vstrCoUserExcelPax As String = "", Optional ByVal vstrNoPCExcelPax As String = "")
        Dim dc As Dictionary(Of String, String)

        Try

            For Each Item As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax

                dc = New Dictionary(Of String, String)

                '                    @IDPax int,
                '@Nombres varchar(50),
                '@Apellidos varchar(50),
                '@IDIdentidad char(3),
                '@NumIdentidad varchar(15),
                '@IDNacionalidad char(3),
                '@FecNacimiento smalldatetime,
                '@UserMod char(4)

                dc.Add("@IDPax", Item.IDPax)
                dc.Add("@Nombres", Item.Nombres)
                dc.Add("@Apellidos", Item.Apellidos)
                dc.Add("@IDIdentidad", Item.IDIdentidad)
                dc.Add("@NumIdentidad", Item.NumIdentidad)
                dc.Add("@IDNacionalidad", Item.IDNacionalidad)
                dc.Add("@FecNacimiento", Item.FecNacimiento)

                dc.Add("@NoRutaArchivoExcelPax", vstrNoRutaArchivoExcelPax)
                dc.Add("@CoUserExcelPax", vstrCoUserExcelPax)
                dc.Add("@NoPCExcelPax", vstrNoPCExcelPax)

                dc.Add("@UserMod", Item.UserMod)

                objADO.ExecuteSP("COTIPAX_UPD_EXCEL", dc)
            Next


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Public Sub ActualizarPaxReservas_Excel_APT(ByVal BE As clsCotizacionBE, Optional ByVal vstrNoRutaArchivoExcelPax As String = "", Optional ByVal vstrCoUserExcelPax As String = "", Optional ByVal vstrNoPCExcelPax As String = "")
        Dim dc As Dictionary(Of String, String)

        Try

            For Each Item As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax

                dc = New Dictionary(Of String, String)

                dc.Add("@IDPax", Item.IDPax)
                dc.Add("@NumberAPT", Item.NumberAPT)
                dc.Add("@Apellidos", Item.Apellidos)
                dc.Add("@Nombres", Item.Nombres)
                dc.Add("@Titulo", Item.Titulo)
                dc.Add("@ObservEspecial", Item.ObservEspecial)
                dc.Add("@FecNacimiento", Item.FecNacimiento)
                dc.Add("@IDNacionalidad", Item.IDNacionalidad)
                dc.Add("@NumIdentidad", Item.NumIdentidad)
                dc.Add("@UserMod", Item.UserMod)
                dc.Add("@NoRutaArchivoExcelPax", vstrNoRutaArchivoExcelPax)
                dc.Add("@CoUserExcelPax", vstrCoUserExcelPax)
                dc.Add("@NoPCExcelPax", vstrNoPCExcelPax)

                objADO.ExecuteSP("COTIPAX_UPD_EXCEL_APT", dc)
            Next


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub
End Class



'Private Sub CopiarVuelos(ByVal BE As clsCotizacionBE, _
'                         ByVal vintIDCab As Integer, _
'                         ByVal vintIDDet As Integer, _
'                         ByVal vstrUserMod As String)
'    Try
'        Dim dc3 As Dictionary(Of String, String)
'        For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
'            dc3 = New Dictionary(Of String, String)

'            dc3.Add("@IDCab", vintIDCab)
'            dc3.Add("@IDCabCopia", Item.IDCab)
'            dc3.Add("@IDDet", vintIDDet)
'            dc3.Add("@IDDetCopia", Item.IDDet)
'            dc3.Add("@UserMod", vstrUserMod)
'            objADO.ExecuteSP("COTIVUELOS_InsCopiaIDCAB", dc3)

'        Next

'    Catch ex As Exception
'        Throw
'    End Try

'End Sub
'End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Private strNombreClase As String = "clsDetalleVentasCotizacionBT"
    Private Overloads Function Insertar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE) As Int32
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NroPax", BE.NroPax)
            dc.Add("@NroLiberados", BE.NroLiberados)
            dc.Add("@Tipo_Lib", BE.Tipo_Lib)

            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoReal", BE.CostoReal)

            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@Cotizacion", BE.Cotizacion)
            dc.Add("@Dia", Format(BE.Dia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@Especial", BE.Especial)
            If BE.IDCab <> 0 Then
                dc.Add("@IDCab", BE.IDCab)
            Else
                dc.Add("@IDCab", BE.IDCab)
            End If
            dc.Add("@IDIdioma", BE.IDIdioma)
            dc.Add("@IDProveedor", BE.IDProveedor)
            dc.Add("@IDServicio", BE.IDServicio)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
            dc.Add("@IDUbigeo", BE.IDUbigeo)
            dc.Add("@Item", BE.Item)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)

            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@MotivoEspecial", BE.MotivoEspecial)
            dc.Add("@RutaDocSustento", BE.RutaDocSustento)

            dc.Add("@Desayuno", BE.Desayuno)
            dc.Add("@Lonche", BE.Lonche)
            dc.Add("@Almuerzo", BE.Almuerzo)
            dc.Add("@Cena", BE.Cena)

            dc.Add("@Transfer", BE.Transfer)

            If IsNumeric(BE.IDDetTransferOri) Then
                dc.Add("@IDDetTransferOri", BE.IDDetTransferOri)
            Else
                dc.Add("@IDDetTransferOri", 0)
            End If
            If IsNumeric(BE.IDDetTransferDes) Then
                dc.Add("@IDDetTransferDes", BE.IDDetTransferDes)
            Else
                dc.Add("@IDDetTransferDes", 0)
            End If

            dc.Add("@TipoTransporte", BE.TipoTransporte)

            dc.Add("@IDUbigeoOri", BE.IDUbigeoOri)
            dc.Add("@IDUbigeoDes", BE.IDUbigeoDes)

            dc.Add("@Servicio", BE.Servicio)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@SSTotalOrig", BE.SSTotalOrig)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STMargenImpto", BE.STMargenImpto)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@STTotalOrig", BE.STTotalOrig)
            dc.Add("@Total", BE.Total)
            dc.Add("@RedondeoTotal", BE.RedondeoTotal)
            dc.Add("@TotalOrig", BE.TotalOrig)
            dc.Add("@TotImpto", BE.TotImpto)
            dc.Add("@Recalcular", BE.Recalcular)
            dc.Add("@IncGuia", BE.IncGuia)
            dc.Add("@UserMod", BE.UserMod)

            If IsNumeric(BE.IDDetRel) Then
                dc.Add("@IDDetRel", BE.IDDetRel)
            Else
                dc.Add("@IDDetRel", 0)
            End If

            dc.Add("@IDDetCopia", BE.IDDetCopia)
            dc.Add("@PorReserva", BE.PorReserva)
            dc.Add("@FueradeHorario", BE.FueradeHorario)

            dc.Add("@CostoRealEditado", BE.CostoRealEditado)
            dc.Add("@MargenAplicadoEditado", BE.MargenAplicadoEditado)
            dc.Add("@AcomodoEspecial", BE.AcomodoEspecial)
            dc.Add("@ServicioEditado", BE.ServicioEditado)
            dc.Add("@CalculoTarifaCotizacion", BE.CalculoTarifaPendiente)
            dc.Add("@FlSSCREditado", BE.FlSSCREditado)
            dc.Add("@FlSTCREditado", BE.FlSTCREditado)
            dc.Add("@FlServicioTC", BE.FlServicioTC)
            dc.Add("@IDDetAntiguo", BE.IDDetAntiguo)

            dc.Add("@pIDDet", 0)

            Dim intIDDet As Integer = objADO.ExecuteSPOutput("COTIDET_Ins", dc)

            ContextUtil.SetComplete()
            Return intIDDet
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function
    Private Sub ActualizarCampoItem(ByVal vintIDDet As Integer, ByVal vsglItem As Single)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)
            dc.Add("@Item", vsglItem)

            objADO.ExecuteSP("COTIDET_UpdItem", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCampoItem")
        End Try
    End Sub

    Public Sub ActualizarCampoTarifaCambiada(ByVal vintIDServicios_Det As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vintIDServicios_Det)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("COTIDET_UpdxTarifaCambiada", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub ActualizarReservasCampoTarifaCambiada(ByVal vintIDServicios_Det As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vintIDServicios_Det)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("RESERVAS_DET_UpdxTarifaCambiada", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub ActualizarCampoDia(ByVal vintIDDet As Integer, ByVal vdatDia As DateTime, ByVal vstrUser As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDdet", vintIDDet)
            dc.Add("@Dia", Format(vdatDia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@UserMod", vstrUser)

            objADO.ExecuteSP("COTIDET_UpdDiaxIDDet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCampoDia")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@CostoRealAnt", BE.CostoRealAnt)
            dc.Add("@MargenAplicadoAnt", BE.MargenAplicadoAnt)

            dc.Add("@NroPax", BE.NroPax)
            dc.Add("@NroLiberados", BE.NroLiberados)
            dc.Add("@Tipo_Lib", BE.Tipo_Lib)

            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoReal", BE.CostoReal)

            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@Cotizacion", BE.Cotizacion)
            dc.Add("@Dia", Format(BE.Dia, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@Especial", BE.Especial)
            If BE.IDCab <> 0 Then
                dc.Add("@IDCab", BE.IDCab)
            Else
                dc.Add("@IDCab", BE.IDCab)
            End If
            dc.Add("@IDIdioma", BE.IDIdioma)
            dc.Add("@IDProveedor", BE.IDProveedor)
            dc.Add("@IDServicio", BE.IDServicio)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)
            dc.Add("@IDUbigeo", BE.IDUbigeo)
            dc.Add("@Item", BE.Item)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)

            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@MotivoEspecial", BE.MotivoEspecial)
            dc.Add("@RutaDocSustento", BE.RutaDocSustento)

            dc.Add("@Desayuno", BE.Desayuno)
            dc.Add("@Lonche", BE.Lonche)
            dc.Add("@Almuerzo", BE.Almuerzo)
            dc.Add("@Cena", BE.Cena)

            dc.Add("@Transfer", BE.Transfer)

            If IsNumeric(BE.IDDetTransferOri) Then
                dc.Add("@IDDetTransferOri", BE.IDDetTransferOri)
            Else
                dc.Add("@IDDetTransferOri", 0)
            End If
            If IsNumeric(BE.IDDetTransferDes) Then
                dc.Add("@IDDetTransferDes", BE.IDDetTransferDes)
            Else
                dc.Add("@IDDetTransferDes", 0)
            End If

            dc.Add("@TipoTransporte", BE.TipoTransporte)

            dc.Add("@IDUbigeoOri", BE.IDUbigeoOri)
            dc.Add("@IDUbigeoDes", BE.IDUbigeoDes)

            dc.Add("@Servicio", BE.Servicio)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@SSTotalOrig", BE.SSTotalOrig)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STMargenImpto", BE.STMargenImpto)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@STTotalOrig", BE.STTotalOrig)
            dc.Add("@Total", BE.Total)
            dc.Add("@RedondeoTotal", BE.RedondeoTotal)
            dc.Add("@TotalOrig", BE.TotalOrig)
            dc.Add("@TotImpto", BE.TotImpto)

            dc.Add("@CostoRealEditado", BE.CostoRealEditado)
            dc.Add("@CostoRealImptoEditado", BE.CostoRealImptoEditado)
            dc.Add("@MargenAplicadoEditado", BE.MargenAplicadoEditado)
            dc.Add("@ServicioEditado", BE.ServicioEditado)

            dc.Add("@Recalcular", BE.Recalcular)
            dc.Add("@IncGuia", BE.IncGuia)
            dc.Add("@FueradeHorario", BE.FueradeHorario)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@CalculoTarifaPendiente", BE.CalculoTarifaPendiente)

            dc.Add("@ExecTrigger", BE.ExecTrigger)
            dc.Add("@AcomodoEspecial", BE.AcomodoEspecial)

            dc.Add("@FlSSCREditado", BE.FlSSCREditado)
            dc.Add("@FlSTCREditado", BE.FlSTCREditado)
            objADO.ExecuteSP("COTIDET_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception

            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")


        End Try
    End Sub

    Public Overloads Sub Eliminar(ByVal vintIDDet As Integer)
        Try

            Dim objBTDetServ As New clsDetalleVentasCotizacionDetServiciosBT
            objBTDetServ.EliminarxIDDet(vintIDDet)

            Dim objBTDescServ As New clsDetalleDescripServicioVentasCotizacionBT
            objBTDescServ.EliminarxIDDet(vintIDDet)

            Dim objBTDetPax As New clsDetallePaxVentasCotizacionBT
            objBTDetPax.EliminarxIDDet(vintIDDet)

            Dim objBTDetQuieb As New clsDetalleQuiebresVentasCotizacionBT
            objBTDetQuieb.EliminarxIDDet(vintIDDet)

            Dim objResEstHabBT As New clsReservaBT.clsDetalleReservaBT.clsReservasDetEstadoHabitBT
            objResEstHabBT.EliminarxIDDet(vintIDDet)

            Dim objBTAcomodoEspDistribuc As New clsDetalleVentasCotizacionAcomodoEspecialBT.clsDistribucAcomodoEspecialBT
            objBTAcomodoEspDistribuc.EliminarxIDDet(vintIDDet)

            Dim objBTAcomodoEspecial As New clsDetalleVentasCotizacionAcomodoEspecialBT
            objBTAcomodoEspecial.EliminarxIDDet(vintIDDet)

            Dim objBTAcomodoVehicIntDetPaxBT As New clsAcomodoVehiculoDetPaxIntBT
            objBTAcomodoVehicIntDetPaxBT.EliminarxIDDet(vintIDDet)

            Dim objBTAcomodoVehicIntBT As New clsAcomodoVehiculoBT
            objBTAcomodoVehicIntBT.EliminarxIDDet(vintIDDet)

            Dim objBTAcomodoVehicExtDetPaxBT As New clsAcomodoVehiculoDetPaxExtBT
            objBTAcomodoVehicExtDetPaxBT.EliminarxIDDet(vintIDDet)

            Dim objBTAcomodoVehicExtBT As New clsAcomodoVehiculo_Ext_BT
            objBTAcomodoVehicExtBT.EliminarxIDDet(vintIDDet)

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)

            '' ''
            objADO.ExecuteSP("COTIDET_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub InsertarLog1eraVez(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("COTIDET_LOG_SP_Ins", dc)
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarLog1eraVez")
        End Try
    End Sub

    Private Sub ActualizarSTRel(ByRef BE As clsCotizacionBE, ByVal vstrIDDetRelPadre As String, ByVal vintNuevoID As Integer)
        Try
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                If Item.IDDetRel IsNot Nothing Then
                    If Item.IDDetRel <> "0" And Item.IDDetRel.Length >= 2 Then
                        If Item.IDDetRel.Substring(0, 1) = "H" Then
                            If Mid(Item.IDDetRel, 2) = Mid(vstrIDDetRelPadre, 2) Then
                                Item.IDDetRel = vintNuevoID
                            End If
                        Else

                            If Mid(Item.IDDetRel, 2) = Mid(vstrIDDetRelPadre, 2) Then
                                Item.IDDetRel = vintNuevoID
                            End If

                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ActualizarSTRelNum(ByRef BE As clsCotizacionBE, ByVal vstrIDDetPadre As String, ByVal vintNuevoID As Integer)
        Try
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                If Item.IDDetRel = vstrIDDetPadre Then
                    Item.IDDetRel = vintNuevoID
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Sub EliminarIDsHijosST(ByVal BE As clsCotizacionBE, ByVal vstrIDDet As String)
        Try
            If BE.ListaDetPax Is Nothing Then GoTo Sgte
DelLstPax:

            For Each ItemDet As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                If ItemDet.IDDet = vstrIDDet Then
                    BE.ListaDetPax.Remove(ItemDet)
                    GoTo DelLstPax
                End If
            Next
Sgte:

            If BE.ListaDetalleCotizacionQuiebres Is Nothing Then GoTo Sgte2
DellStQui:
            For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE In BE.ListaDetalleCotizacionQuiebres
                If ItemDet.IDDet = vstrIDDet Then
                    BE.ListaDetalleCotizacionQuiebres.Remove(ItemDet)
                    GoTo DellStQui
                End If
            Next
Sgte2:

            If BE.ListaDetalleCotizacionDescripServicio Is Nothing Then GoTo Sgte3
DelLstDeSe:
            For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE.clsDetalleCotizacionDescripServicioBE In BE.ListaDetalleCotizacionDescripServicio
                If ItemDet.IDDet = vstrIDDet Then
                    BE.ListaDetalleCotizacionDescripServicio.Remove(ItemDet)
                    GoTo DelLstDeSe
                End If
            Next
Sgte3:
            If BE.ListaDetalleCotizacionDetServicio Is Nothing Then GoTo Sgte5
DelListDetSrvDet:
            For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE In BE.ListaDetalleCotizacionDetServicio
                If ItemDet.IDDet = vstrIDDet Then
                    BE.ListaDetalleCotizacionDetServicio.Remove(ItemDet)
                    GoTo DelListDetSrvDet
                End If
            Next
Sgte5:
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsCotizacionBE, ByVal vstrAntiguoID As String, _
                                                       ByVal vintNuevoID As Integer, Optional ByVal vstrEstado As String = "", _
                                                       Optional ByVal vblnCopiaCotiz As Boolean = False)
        Try

            If Not BE.ListaDetPax Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                    If ItemDet.IDDet = vstrAntiguoID Then
                        ItemDet.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaDetalleCotizacionQuiebres Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE In BE.ListaDetalleCotizacionQuiebres
                    If ItemDet.IDDet = vstrAntiguoID Then
                        ItemDet.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaDetalleCotizacionDescripServicio Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE.clsDetalleCotizacionDescripServicioBE In BE.ListaDetalleCotizacionDescripServicio
                    If ItemDet.IDDet = vstrAntiguoID Then
                        ItemDet.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaCotizacionVuelo Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                    If Not IsNumeric(ItemDet.IDDet) Then
                        If ItemDet.IDDet = ItemDet.TipoTransporte & vstrAntiguoID Then
                            ItemDet.IDDet = vintNuevoID
                        End If
                    Else
                        If ItemDet.IDDet = vstrAntiguoID Then
                            ItemDet.IDDet = vintNuevoID
                        End If
                    End If
                Next
            End If

            If Not BE.ListaDetalleCotizacionDetServicio Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE In BE.ListaDetalleCotizacionDetServicio
                    If ItemDet.IDDet = vstrAntiguoID Then
                        ItemDet.IDDet = vintNuevoID
                    End If
                Next
            End If
            '' ''

            'Actualizando posibles copias
            If Not BE.ListaDetalleCotiz Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                    If ItemDet.IDDetCopia IsNot Nothing Then
                        If ItemDet.IDDetCopia = vstrAntiguoID Then
                            ItemDet.IDDetCopia = vintNuevoID
                        End If
                    End If
                Next
            End If

            If vblnCopiaCotiz Then
                If Not BE.ListaDetalleCotiz Is Nothing Then
                    ActualizarSTRelNum(BE, vstrAntiguoID, vintNuevoID)
                End If
            End If

            If Not BE.ListaDetalleCotiz Is Nothing Then
                For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                    If ItemDet.IDDet = vstrAntiguoID Then 'And vstrEstado = "A" Then
                        ItemDet.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaDetalleCotizacionAcomodoEspecial Is Nothing Then
                For Each itemAcomodo As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionAcomodoEspecialBE In BE.ListaDetalleCotizacionAcomodoEspecial
                    If itemAcomodo.IDDet = vstrAntiguoID Then
                        itemAcomodo.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaDistribucAcomodoEspecial Is Nothing Then
                For Each itemAcomodPaxDet As clsCotizacionBE.clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE In BE.ListaDistribucAcomodoEspecial
                    If itemAcomodPaxDet.IDDet = vstrAntiguoID Then
                        itemAcomodPaxDet.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaAcomodoVehiculo Is Nothing Then
                For Each itemAcomodVeh As clsCotizacionBE.clsAcomodoVehiculoBE In BE.ListaAcomodoVehiculo
                    If itemAcomodVeh.IDDet = vstrAntiguoID Then
                        itemAcomodVeh.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaAcomodoVehiculoExt Is Nothing Then
                For Each itemAcomodVeh As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE In BE.ListaAcomodoVehiculoExt
                    If itemAcomodVeh.IDDet = vstrAntiguoID Then
                        itemAcomodVeh.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaIDDetCopiarAcomVehi Is Nothing Then
                For Each itemCopiaAcomodVeh As clsCotizacionBE.clsAcomodoVehiculoCopiaBE In BE.ListaIDDetCopiarAcomVehi
                    If itemCopiaAcomodVeh.IDDet = vstrAntiguoID Then
                        itemCopiaAcomodVeh.IDDet = vintNuevoID
                    End If
                    If itemCopiaAcomodVeh.IDDetBase = vstrAntiguoID Then
                        itemCopiaAcomodVeh.IDDetBase = vintNuevoID
                    End If
                Next
            End If


            If Not BE.ListaAcomodoVehiculoIntDetPax Is Nothing Then
                For Each itemAcomodVeh As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In BE.ListaAcomodoVehiculoIntDetPax
                    If itemAcomodVeh.IDDet = vstrAntiguoID Then
                        itemAcomodVeh.IDDet = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaAcomodoVehiculoExtDetPax Is Nothing Then
                For Each itemAcomodVeh As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In BE.ListaAcomodoVehiculoExtDetPax
                    If itemAcomodVeh.IDDet = vstrAntiguoID Then
                        itemAcomodVeh.IDDet = vintNuevoID
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function ConsultarAcomodoEspecial(ByVal vintIDDet As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)

            Return objADO.GetDataTable("COTIDET_ACOMODOESPECIAL_SelxIDDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub ActualizarFechaLog(ByVal vintIDDet As Integer, ByVal vintIDServicio_Det As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)

            objADO.ExecuteSP("RESERVAS_DET_UpdFeRowxIDDet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        Try
            ActualizarReservasDeDetallesEditados(BE)
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                If Item.Accion = "N" Then
                    Dim intIDDet As Integer = Insertar(Item)
                    ActualizarIDsNuevosenHijosST(BE, Item.IDDet, intIDDet, BE.Estado, BE.CotizCopiada)

                    If Item.IDDetRel <> "" Then
                        If Item.IDDetRel.Substring(0, 1) = "P" Then
                            ActualizarSTRel(BE, Item.IDDetRel, intIDDet)
                        ElseIf Item.IDDetRel = "0" Then
                            ActualizarSTRelNum(BE, Item.IDDet, intIDDet)
                        End If
                    End If

                ElseIf Item.Accion = "M" Then
                    'If IsNumeric(Item.IDDet) Then ActualizarFechaLog(Item.IDDet)
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then

                    If IsNumeric(Item.IDDet) Then
                        Eliminar(Item.IDDet)
                    End If

                    EliminarIDsHijosST(BE, Item.IDDet)
                ElseIf Item.Accion = "UpdItem" Then
                    If IsNumeric(Item.IDDet) Then
                        'ActualizarFechaLog(Item.IDDet)
                        ActualizarCampoItem(Item.IDDet, Item.Item)
                    End If
                End If

                If Item.FlServicioUpdxAnio Then
                    If IsNumeric(Item.IDDet) Then
                        Dim objBTDetServDel As New clsDetalleVentasCotizacionDetServiciosBT
                        objBTDetServDel.EliminarxIDDet(Item.IDDet)
                    End If
                End If
            Next

            Dim objBTDetQui As New clsDetalleQuiebresVentasCotizacionBT
            objBTDetQui.InsertarActualizarEliminar(BE)

            Dim objBTDetPax As New clsDetallePaxVentasCotizacionBT
            objBTDetPax.InsertarActualizarEliminar(BE)

            Dim objBTDetDescripServ As New clsDetalleDescripServicioVentasCotizacionBT
            objBTDetDescripServ.InsertarActualizarEliminar(BE)

            Dim objBTDetServ As New clsDetalleVentasCotizacionDetServiciosBT
            objBTDetServ.InsertarActualizarEliminar(BE)

            Dim objBTVuelos As New clsVuelosVentasCotizacionBT
            objBTVuelos.InsertarActualizarEliminar(BE)

            If Not BE.ListaCotizQuiebre Is Nothing Then
                For Each Item As clsCotizacionBE.clsCotizacionQuiebreBE In BE.ListaCotizQuiebre
                    Dim intIDCab_Q As Integer = Item.IDCab_Q

                    Dim strTipoPax As String = strDevuelveTipoPaxNacionalidad(BE)
                    objBTDetQui.ReCalculoDetQuiebre(BE.IdCab, intIDCab_Q, Item.Pax, Item.Liberados, _
                                                Item.Tipo_Lib, strTipoPax, BE.Redondeo, BE.IGV, "M", _
                                                Item.UserMod, BE.ListaDetalleCotiz)

                Next
            End If

            'Dim strNro As String = "300"
            'Dim bytNro As Byte = Convert.ToByte(strNro)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw


        End Try
    End Sub

    Private Sub ActualizarReservasDeDetallesEditados(ByVal BE As clsCotizacionBE)
        If BE.ListaDetalleCotiz Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                If IsNumeric(Item.IDDet) Then
                    If Item.Accion = "M" Then
                        ActualizarFechaLog(Item.IDDet, Item.IDServicio_Det)
                    End If
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Function dttConsultarPK(ByVal vintIDDet As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            'dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDDet", vintIDDet)

            Return objADO.GetDataTable("COTIDET_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    'Public Function ConsultarxProveedorDetServicio(ByVal vintIDCab As Integer, _
    '                                               ByVal vstrIDProveedor As String, _
    '                                               ByVal vintIDServicio_Det As Integer) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDProveedor", vstrIDProveedor)
    '        dc.Add("@IDServicio_Det", vintIDServicio_Det)

    '        Return objADO.GetDataTable("COTIDET_SelxIDProveedorIDServicio_Det", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function
    Public Function strDevuelveTipoPaxNacionalidad(ByVal BE As clsCotizacionBE) As String
        Try

            'Dim objBTPax As clsCotizacionBT.clsCotizacionPaxBT
            Dim objBTPax As clsPaxVentasCotizacionBT
            Dim dttPax As DataTable = Nothing
            Dim dttDetPax As DataTable

            'Dim strTipoPax As String = If(BE.TipoPax Is Nothing, "EXT", BE.TipoPax)
            Dim strTipoPax As String = "EXT"
            Dim blnPeruano As Boolean
            Dim strIDDet As String = ""
            If Not BE.ListaDetalleCotiz Is Nothing Then
                For Each ItemDetCot As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                    strIDDet = ItemDetCot.IDDet
                    Exit For
                Next
            End If
            Dim blnIDDetTexto As Boolean = False
            If Not IsNumeric(strIDDet) Then
                blnIDDetTexto = True

                'Return strTipoPax
                'Exit Function
            End If
            Dim intCountPax As Int16 = 0
            If Not BE.ListaDetPax Is Nothing Then
                intCountPax = BE.ListaDetPax.Count
            End If

            If intCountPax > 0 Then

                For Each ItemDetPax As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                    If ItemDetPax.Selecc Then
                        If BE.ListaPax.Count > 0 Then
                            blnPeruano = False
                            For Each ItemPax As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                                If IsNumeric(ItemPax.IDPax) Then
                                    'If ItemDetPax.IDPax = ItemPax.IDPax And ItemPax.IDNacionalidad = "000323" Then 'Perú
                                    If ItemDetPax.IDPax = ItemPax.IDPax And ItemPax.Residente = True Then 'Perú
                                        blnPeruano = True
                                        Exit For
                                    End If
                                End If
                            Next
                            If blnPeruano Then
                                strTipoPax = "NCR"
                            End If
                        Else
                            'bd
                            objBTPax = New clsPaxVentasCotizacionBT 'New clsCotizacionBT.clsCotizacionPaxBT
                            dttPax = objBTPax.ConsultarListxIDCab(BE.IdCab)
                            blnPeruano = False
                            For Each drPax As DataRow In dttPax.Rows
                                If IsNumeric(ItemDetPax.IDPax) Then
                                    'If ItemDetPax.IDPax = drPax("IDPax") And drPax("IDNacionalidad") = "000323" Then 'Perú
                                    If ItemDetPax.IDPax = drPax("IDPax") And Convert.ToBoolean(drPax("Residente")) = True Then 'Perú
                                        blnPeruano = True
                                        Exit For
                                    End If
                                End If
                            Next
                            If blnPeruano Then
                                strTipoPax = "NCR"
                                Exit For
                            End If

                        End If

                    End If
                Next
            Else 'BD

                objBTPax = New clsPaxVentasCotizacionBT ' New clsCotizacionBT.clsCotizacionPaxBT
                If Not blnIDDetTexto Then
                    dttDetPax = objBTPax.ConsultarDetxIDDetLvw(strIDDet)
                Else
                    dttDetPax = objBTPax.ConsultarListxIDCab(BE.IdCab)
                End If
                For Each drDetPax As DataRow In dttDetPax.Rows
                    Dim intCountPax2 As Int16 = 0
                    If Not BE.ListaPax Is Nothing Then
                        intCountPax2 = BE.ListaPax.Count
                    End If
                    If intCountPax2 > 0 Then
                        blnPeruano = False
                        For Each ItemPax As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                            If IsNumeric(ItemPax.IDPax) Then
                                'If drDetPax("IDPax") = ItemPax.IDPax And ItemPax.IDNacionalidad = "000323" Then 'Perú
                                If drDetPax("IDPax") = ItemPax.IDPax And ItemPax.Residente = True Then 'Perú
                                    blnPeruano = True
                                    Exit For
                                End If
                            End If
                        Next
                        If blnPeruano Then
                            strTipoPax = "NCR"
                        End If
                    Else
                        'bd
                        If dttPax Is Nothing Then
                            dttPax = objBTPax.ConsultarListxIDCab(BE.IdCab)
                        End If

                        blnPeruano = False
                        For Each drPax As DataRow In dttPax.Rows
                            'If drDetPax("IDPax") = drPax("IDPax") And drPax("IDNacionalidad") = "000323" Then 'Perú
                            If drDetPax("IDPax") = drPax("IDPax") And Convert.ToBoolean(drPax("Residente")) = True Then 'Perú
                                blnPeruano = True
                                Exit For
                            End If
                        Next
                        If blnPeruano Then
                            strTipoPax = "NCR"
                            Exit For
                        End If

                    End If


                Next



            End If



            Return strTipoPax
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32, ByVal vblnConCopias As Boolean, _
                                            ByVal vblnSoloparaRecalcular As Boolean, _
                                            ByVal vblnConIDReserva_Det As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@ConCopias", vblnConCopias)
            dc.Add("@SoloparaRecalcular", vblnSoloparaRecalcular)
            dc.Add("@ConIDReserva_Det", vblnConIDReserva_Det)
            Return objADO.GetDataTable("COTIDET_Sel_xIDCab", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32, ByVal vblnConCopias As Boolean, _
                                        ByVal vblnSoloparaRecalcular As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@ConCopias", vblnConCopias)
            dc.Add("@SoloparaRecalcular", vblnSoloparaRecalcular)
            'dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataTable("COTIDET_Sel_xIDCabxUpdReserva", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'COTIDET_DETSERVICIOS_Sel_xIDCabxUpdReserva
    Public Function ConsultarListSubServiciosxIDCab(ByVal vintIDCab As Int32, ByVal vblnConCopias As Boolean, _
                                                    ByVal vblnSoloparaRecalcular As Boolean) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@ConCopias", vblnConCopias)
            dc.Add("@SoloparaRecalcular", vblnSoloparaRecalcular)

            Return objADO.GetDataTable("COTIDET_DETSERVICIOS_Sel_xIDCabxUpdReserva", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxRangoFechasConsecutivasProveedor(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String, _
                                            ByVal vintIDServicio_Det As Integer, ByVal vdatFechaIni As Date, ByVal vdatFechaFin As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@FechaIni", vdatFechaIni)
            dc.Add("@FechaFin", vdatFechaFin)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)

            Dim dtt As DataTable = objADO.GetDataTable("COTIDET_SelxRangoFechasProvee", dc)

            Dim dttRet As DataTable = dtt.Clone

            Dim bln1ero As Boolean = True

            For Each dr As DataRow In dtt.Rows
                Dim datFechaDiaAnt As Date
                If Not bln1ero Then
                    Dim datFechaDia As Date = FormatDateTime(dr("Dia"), DateFormat.ShortDate)
                    If (datFechaDia = DateAdd(DateInterval.Day, 1, datFechaDiaAnt)) Or _
                        (datFechaDia = datFechaDiaAnt) Then
                        dttRet.ImportRow(dr)
                    End If
                Else
                    dttRet.ImportRow(dr)
                End If
                bln1ero = False
                datFechaDiaAnt = FormatDateTime(dr("Dia"), DateFormat.ShortDate)

            Next

            Return dttRet


        Catch ex As Exception
            Throw

        End Try

    End Function

    Public Function ConsultarListxIDCab_Q(ByVal vintIDCab_Q As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDCab_Q", vintIDCab_Q)

            Return objADO.GetDataTable("COTIDET_Sel_xIDCab_Q", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function


    Public Function ConsultarIDDetxIDCab(ByVal vintIDCab As Integer) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            Return objADO.GetDataTable("COTIDET_Sel_IDDetxIDCab", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub ActualizarNroPax(ByVal vintIDDet As Integer, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@UserMod", vstrUserMod)


            objADO.ExecuteSP("COTIDET_Upd_NroPax", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroPax")
        End Try
    End Sub

    Public Sub ActualizarNroPaxxDetPax(ByVal vintIDCab As Integer, ByVal vstrUserMod As Single)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIDET_UpdxDetPax", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarNroPaxxDetPax")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsCostosTourConductorBT
    Inherits clsVentasCotizacionBaseBT
    Private strNombreClase As String = "clsCostosTourConductorBT"

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsCostosTourConductorBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", BE.IdCab)
            dc.Add("@CoTipoHon", BE.CotipoHon)
            dc.Add("@QtDesde", BE.QtDesde)
            dc.Add("@QtHasta", BE.QtHasta)
            dc.Add("@SsPrecio", BE.SsPrecio)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTICAB_COSTOSTC_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vbytNuCosto As Byte, ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuCosto", vbytNuCosto)
            dc.Add("@IDCab", vintIDCab)

            objADO.ExecuteSP("COTICAB_COSTOSTC_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Sub EliminarCostosxIDCab(ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            objADO.ExecuteSP("COTICAB_COSTOSTC_DelxIDCab", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarCostosxIDCab")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsCostosTourConductorBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuCosto", BE.NuCosto)
            dc.Add("@IDCab", BE.IdCab)
            dc.Add("@QtDesde", BE.QtDesde)
            dc.Add("@QtHasta", BE.QtHasta)
            dc.Add("@SsPrecio", BE.SsPrecio)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTICAB_COSTOSTC_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaCostosTourConductor Is Nothing Then Exit Sub
        Try
            If BE.FlTourConductor Then
                EliminarCostosxIDCab(BE.IdCab)
                For Each Item As clsCotizacionBE.clsCostosTourConductorBE In BE.ListaCostosTourConductor
                    Insertar(Item)
                Next
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarElimninar")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsTourConductorBT
    Inherits clsVentasCotizacionBaseBT
    Private strNombreClase As String = "clsTourConductorBT"

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsTourConductorBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", BE.IdCab)
                .Add("@FeDesde", BE.FeDesde.ToShortDateString)
                .Add("@FeHasta", BE.FeHasta.ToShortDateString)
                .Add("@FlIncluiNochesAntDesp", BE.FlIncluiNochesAntDesp)
                .Add("@SsMontoRealHotel", BE.SsMontoRealHotel)
                .Add("@SsMontoTotalHotel", BE.SsMontoTotalHotel)
                .Add("@FlIncluirVuelo", BE.FlIncluirVuelo)
                .Add("@SsVuelo", BE.SsVuelo)
                .Add("@CoTipoHon", BE.CoTipoHon)
                .Add("@SsHonorario", BE.SsHonorario)
                .Add("@PoMargen", BE.PoMargen)
                .Add("@SsViaticosSOL", BE.SsViaticosSOL)
                .Add("@SsViaticosDOL", BE.SsViaticosDOL)
                .Add("@SsTicketAereos", BE.SsTicketAereos)
                .Add("@SsIGVHoteles", BE.SsIGVHoteles)
                .Add("@SsTarjetaTelef", BE.SsTarjetaTelef)
                .Add("@SsTaxi", BE.SsTaxi)
                .Add("@SsOtros", BE.SsOtros)
                .Add("@SsGEBPrimerDia", BE.SsGEBPrimerDia)
                .Add("@SsGEBUltimoDia", BE.SsGEBUltimoDia)
                .Add("@Total", BE.Total)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("COTICAB_TOURCONDUCTOR_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsTourConductorBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", BE.IdCab)
                .Add("@FeDesde", BE.FeDesde.ToShortDateString)
                .Add("@FeHasta", BE.FeHasta.ToShortDateString)
                .Add("@FlIncluiNochesAntDesp", BE.FlIncluiNochesAntDesp)
                .Add("@SsMontoRealHotel", BE.SsMontoRealHotel)
                .Add("@SsMontoTotalHotel", BE.SsMontoTotalHotel)
                .Add("@FlIncluirVuelo", BE.FlIncluirVuelo)
                .Add("@SsVuelo", BE.SsVuelo)
                .Add("@CoTipoHon", BE.CoTipoHon)
                .Add("@SsHonorario", BE.SsHonorario)
                .Add("@PoMargen", BE.PoMargen)
                .Add("@SsViaticosSOL", BE.SsViaticosSOL)
                .Add("@SsViaticosDOL", BE.SsViaticosDOL)
                .Add("@SsTicketAereos", BE.SsTicketAereos)
                .Add("@SsIGVHoteles", BE.SsIGVHoteles)
                .Add("@SsTarjetaTelef", BE.SsTarjetaTelef)
                .Add("@SsTaxi", BE.SsTaxi)
                .Add("@SsOtros", BE.SsOtros)
                .Add("@SsGEBPrimerDia", BE.SsGEBPrimerDia)
                .Add("@SsGEBUltimoDia", BE.SsGEBUltimoDia)
                .Add("@Total", BE.Total)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("COTICAB_TOURCONDUCTOR_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim objBTCostos As New clsCostosTourConductorBT
            objBTCostos.EliminarCostosxIDCab(vintIDCab)

            dc.Add("@IDCab", vintIDCab)
            objADO.ExecuteSP("COTICAB_TOURCONDUCTOR_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Overloads Sub InsertarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaTourConductor Is Nothing Then Exit Sub
        Try
            If BE.FlTourConductor Then
                For Each Item As clsCotizacionBE.clsTourConductorBE In BE.ListaTourConductor
                    If Not blnExisteTourConductor(Item.IdCab) Then
                        Insertar(Item)
                    Else
                        Actualizar(Item)
                    End If
                Next
            Else
                Eliminar(BE.IdCab)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarElimninar")
        End Try
    End Sub

    Private Function blnExisteTourConductor(ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim blnExiste As Boolean = False
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pExiste", False)
            blnExiste = objADO.GetSPOutputValue("COTICAB_TOURCONDUCTOR_Sel_ExistxIDCab", dc)
            Return blnExiste
        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExisteTourConductor")
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsPaxVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Private strNombreClase As String = "clsPaxVentasCotizacionBT"
    Private Overloads Function Insertar(ByVal BE As clsCotizacionBE.clsCotizacionPaxBE) As Int32
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@Orden", BE.Orden)
            dc.Add("@IDIdentidad", BE.IDIdentidad)
            dc.Add("@NumIdentidad", BE.NumIdentidad)
            dc.Add("@Nombres", BE.Nombres)
            dc.Add("@Apellidos", BE.Apellidos)
            dc.Add("@Titulo", BE.Titulo)
            dc.Add("@FecNacimiento", BE.FecNacimiento)
            dc.Add("@IDNacionalidad", BE.IDNacionalidad)
            dc.Add("@Peso", BE.Peso)
            dc.Add("@Residente", BE.Residente)
            dc.Add("@ObservEspecial", BE.ObservEspecial)
            dc.Add("@IDPaisResidencia", BE.IDPaisResidencia)
            dc.Add("@FecIngresoPais", BE.FecIngresoPais)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@TxTituloAcademico", BE.TituloAcademico)
            dc.Add("@FlTC", BE.TC)
            dc.Add("@FlNoShow", BE.NoShow)

            dc.Add("@FlEntMP", BE.FlEntMP)
            dc.Add("@FlEntWP", BE.FlEntWP)
            dc.Add("@FlAutorizarMPWP", BE.FlAutorizMPWP)

            dc.Add("@NumberAPT", BE.NumberAPT)

            dc.Add("@pIDPax", 0)

            Dim intIDPax As Integer = objADO.ExecuteSPOutput("COTIPAX_Ins", dc)

            ContextUtil.SetComplete()
            Return intIDPax

        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsCotizacionBE, ByVal vstrAntiguoID As String, _
                                                           ByVal vintNuevoID As Integer)
        Try
            If Not BE.ListaDetPax Is Nothing Then
                For Each ItemDgv As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                    If ItemDgv.IDPax = vstrAntiguoID Then
                        ItemDgv.IDPax = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaPaxTransporte Is Nothing Then
                For Each ItemDgv As clsCotizacionBE.clsPaxTransporteBE In BE.ListaPaxTransporte
                    If ItemDgv.IDPax = vstrAntiguoID Then
                        ItemDgv.IDPax = vintNuevoID
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionPaxBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@Orden", BE.Orden)
            dc.Add("@IDIdentidad", BE.IDIdentidad)
            dc.Add("@NumIdentidad", BE.NumIdentidad)
            dc.Add("@Nombres", BE.Nombres)
            dc.Add("@Apellidos", BE.Apellidos)
            dc.Add("@Titulo", BE.Titulo)
            dc.Add("@FecNacimiento", BE.FecNacimiento)
            dc.Add("@IDNacionalidad", BE.IDNacionalidad)
            dc.Add("@Peso", BE.Peso)
            dc.Add("@Residente", BE.Residente)
            dc.Add("@ObservEspecial", BE.ObservEspecial)
            dc.Add("@IDPaisResidencia", BE.IDPaisResidencia)
            dc.Add("@FecIngresoPais", BE.FecIngresoPais)
            dc.Add("@PassValidado", BE.PassValidado)
            dc.Add("@UserMod", BE.UserMod)

            dc.Add("@TxTituloAcademico", BE.TituloAcademico)
            dc.Add("@FlTC", BE.TC)
            dc.Add("@FlNoShow", BE.NoShow)

            dc.Add("@FlEntMP", BE.FlEntMP)
            dc.Add("@FlEntWP", BE.FlEntWP)
            dc.Add("@FlAutorizarMPWP", BE.FlAutorizMPWP)

            dc.Add("@FlAdultoINC", BE.FlAdultoINC)

            dc.Add("@NumberAPT", BE.NumberAPT)

            objADO.ExecuteSP("COTIPAX_Upd", dc)

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintIDPax As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDPax", vintIDPax)

            '' ''
            objADO.ExecuteSP("OPERACIONES_DET_PAX_DelxIDPax", dc)
            objADO.ExecuteSP("RESERVAS_DET_PAX_DelxIDPax", dc)
            objADO.ExecuteSP("COTIDET_DelxIDPax", dc)
            objADO.ExecuteSP("COTITRANSP_PAX_DelxIDPax", dc)

            objADO.ExecuteSP("ACOMODOPAX_PROVEEDOR_DelxIDPax", dc)
            objADO.ExecuteSP("ACOMODOPAX_FILE_DelxIDPax", dc)
            objADO.ExecuteSP("COTIPAX_Del", dc)

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    Public Sub ActualizarTotales(ByVal vintIDPax As Int32, ByVal vintIDCab As Int32, _
                                     ByVal vstrUserMod As String)

        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDPax", vintIDPax)
            dc.Add("@IDCab", vintIDCab)
            'dc.Add("@IDDet", vintIDDet)
            'dc.Add("@bSuma", vblnSuma)
            'dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIPAX_Upd_Totales", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarTotales")
        End Try
    End Sub
    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        Try
            For Each Item As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                If Item.Accion = "N" Then
                    Dim intPax As Int32 = Insertar(Item)

                    ActualizarIDsNuevosenHijosST(BE, Item.IDPax, intPax)

                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.IDPax) Then
                        Eliminar(Item.IDPax)
                    End If
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Function ConsultarListxIDCabLvw(ByVal vintIDCab As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable("COTIPAX_Sel_xIDCab_Lvw", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function
    Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDCab", vintIDCab)


            Return objADO.GetDataTable("COTIPAX_Sel_ListxIDCab", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function
    Public Function ConsultarDetxIDDetLvw(ByVal vintIDDet As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDDet", vintIDDet)

            Return objADO.GetDataTable("COTIDET_PAX_Sel_xIDDet_Lvw", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsQuiebresVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsQuiebresVentasCotizacionBT"
    Private Overloads Function Insertar(ByVal BE As clsCotizacionBE.clsCotizacionQuiebreBE) As Int32
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IdCab", BE.IDCab)
            dc.Add("@Pax", BE.Pax)
            dc.Add("@Liberados", BE.Liberados)
            dc.Add("@Tipo_Lib", BE.Tipo_Lib)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@pIDCab_Q", 0)
            Dim intIDCab_Q As Integer = objADO.ExecuteSPOutput("COTICAB_QUIEBRES_Ins", dc)

            ContextUtil.SetComplete()
            Return intIDCab_Q

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function
    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsCotizacionBE, ByVal vstrAntiguoID As String, _
                                                       ByVal vintNuevoID As Integer)
        Try
            If Not BE.ListaDetalleCotizacionQuiebres Is Nothing Then
                For Each ItemDgv As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE In BE.ListaDetalleCotizacionQuiebres
                    If ItemDgv.IDCab_Q = vstrAntiguoID Then
                        ItemDgv.IDCab_Q = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaCotizQuiebre Is Nothing Then
                For Each ItemDgv As clsCotizacionBE.clsCotizacionQuiebreBE In BE.ListaCotizQuiebre
                    If ItemDgv.IDCab_Q = vstrAntiguoID Then
                        ItemDgv.IDCab_Q = vintNuevoID
                    End If
                Next
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionQuiebreBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab_Q", BE.IDCab_Q)
            dc.Add("@Pax", BE.Pax)
            dc.Add("@Liberados", BE.Liberados)
            dc.Add("@Tipo_Lib", BE.Tipo_Lib)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTICAB_QUIEBRES_Upd", dc)

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal vintIDCab_Q As Integer)

        Dim dc As New Dictionary(Of String, String)
        Try
            Dim objDet As New clsDetalleQuiebresVentasCotizacionBT
            objDet.EliminarxIDCab_Q(vintIDCab_Q)

            dc.Add("@IDCab_Q", vintIDCab_Q)
            objADO.ExecuteSP("COTICAB_QUIEBRES_Del", dc)

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    'Private Function ConsultarListxIDCab(ByVal vintIDCab As Int32) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@IDCab", vintIDCab)
    '        Return objADO.GetDataTable("COTICAB_QUIEBRES_Sel_xIDCab", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try

    'End Function

    'Private Sub InsertarQuiebresST(ByRef rBE As clsCotizacionBE)
    '    Try
    '        Dim dttQ As DataTable = ConsultarListxIDCab(rBE.IdCab)
    '        Dim objBE As clsCotizacionBE.clsCotizacionQuiebreBE
    '        For Each dr As DataRow In dttQ.Rows
    '            objBE = New clsCotizacionBE.clsCotizacionQuiebreBE
    '            With objBE
    '                .IDCab_Q = dr("IDCab_Q")
    '                .Accion = "M" 'para q jale el Item.IDCab_Q
    '            End With
    '            rBE.ListaCotizQuiebre.Add(objBE)

    '        Next
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub
    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        Try
            Dim objBTDetCot As New clsDetalleVentasCotizacionBT
            Dim objBTDetQui As New clsDetalleQuiebresVentasCotizacionBT
            Dim intIDCab_Q As Integer

            'If BE.ListaCotizQuiebre.Count = 0 Then
            '    InsertarQuiebresST(BE)
            'End If

            For Each Item As clsCotizacionBE.clsCotizacionQuiebreBE In BE.ListaCotizQuiebre
                If Item.Accion = "N" Or Item.Accion = "M" Then
                    If Item.Accion = "N" Then
                        intIDCab_Q = Insertar(Item)

                        ActualizarIDsNuevosenHijosST(BE, Item.IDCab_Q, intIDCab_Q)
                    ElseIf Item.Accion = "M" Then
                        Actualizar(Item)
                        intIDCab_Q = Item.IDCab_Q
                    End If

                    Dim strTipoPax As String = objBTDetCot.strDevuelveTipoPaxNacionalidad(BE)
                    objBTDetQui.ReCalculoDetQuiebre(BE.IdCab, intIDCab_Q, Item.Pax, Item.Liberados, _
                                                Item.Tipo_Lib, strTipoPax, BE.Redondeo, BE.IGV, Item.Accion, Item.UserMod)


                ElseIf Item.Accion = "B" Then

                    Eliminar(Item.IDCab_Q)
                End If
            Next

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleQuiebresVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsDetalleQuiebresVentasCotizacionBT"
    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab_Q", BE.IDCab_Q)
            dc.Add("@IDDet", BE.IDDet)

            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STMargenImpto", BE.STMargenImpto)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@Total", BE.Total)
            dc.Add("@RedondeoTotal", BE.RedondeoTotal)
            dc.Add("@TotImpto", BE.TotImpto)
            dc.Add("@UserMod", BE.UserMod)
            'dc.Add("@pIDDet_Q", 0)

            'Dim intIDDet_Q As Integer = objADO.ExecuteSPOutput("COTIDET_QUIEBRES_Ins", dc)

            'BE.IDDet_Q = intIDDet_Q

            objADO.ExecuteSP("COTIDET_QUIEBRES_Ins", dc)

            ContextUtil.SetComplete()

            'Return intIDDet_Q


        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet_Q", BE.IDDet_Q)
            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STMargenImpto", BE.STMargenImpto)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@Total", BE.Total)
            dc.Add("@RedondeoTotal", BE.RedondeoTotal)
            dc.Add("@TotImpto", BE.TotImpto)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_QUIEBRES_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal vintIDDet_Q As Integer)
        Try

            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet_Q", vintIDDet_Q)
            objADO.ExecuteSPOutput("COTIDET_QUIEBRES_Del", dc)

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("COTIDET_QUIEBRES_DelxIDDet", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
        End Try
    End Sub
    Public Sub EliminarxIDCab_Q(ByVal vintIDCab_Q As Int32)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab_Q", vintIDCab_Q)

            objADO.ExecuteSP("COTIDET_QUIEBRES_DelxIDCab_Q", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDCab_Q")
        End Try
    End Sub

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        'Dim ListBorrar As New List(Of Integer)
        Try
            If Not BE.ListaDetalleCotizacionQuiebres Is Nothing Then
                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE In BE.ListaDetalleCotizacionQuiebres
                    If Item.Accion = "N" Then
                        Insertar(Item)
                        'ListBorrar.Add(Insertar(Item))

                    ElseIf Item.Accion = "M" Then
                        If Item.IDDet_Q <> 0 Then
                            Actualizar(Item)
                        End If
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDCab_Q)
                    End If
                Next


            End If



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub ReCalculoDetQuiebre(ByVal vintIDCab As Int32, ByVal vintIDCab_Q As Int32, _
                               ByVal vintPax As Int16, ByVal vintLiberados As Int16, _
                               ByVal vstrTipo_Lib As Char, ByVal vstrTipoPax As String, _
                               ByVal vdecRedondeo As Single, _
                               ByVal vsglIGV As Single, ByVal vstrAccion As Char, ByVal vstrUser As String, _
                               Optional ByVal vLstDetCotiz As List(Of clsCotizacionBE.clsDetalleCotizacionBE) = Nothing)
        Try

            Dim objCotiDet As New clsDetalleVentasCotizacionBT

            'Dim objCoti As New clsCotizacionBN
            Dim objCoti As New clsVentasCotizacionBT
            Dim objBE As clsCotizacionBE

            Dim dttCotiDet As DataTable = objCotiDet.ConsultarListxIDCab_Q(vintIDCab_Q)
            Dim objBEDetQ As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE

            Dim dc As Dictionary(Of String, Double)
            Dim dblCostoReal As Double, dblMargenAplicado As Double
            Dim dblSSCR As Double, dblSTCR As Double

            Dim ListaDetCotQ As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)

            For Each drCotiDet As DataRow In dttCotiDet.Rows
                Dim blnActualiz As Boolean = True
                If Not vLstDetCotiz Is Nothing Then
                    blnActualiz = False
                    For Each ItemDetCot As clsCotizacionBE.clsDetalleCotizacionBE In vLstDetCotiz
                        If IsNumeric(ItemDetCot.IDDet) Then
                            If ItemDetCot.IDDet = drCotiDet("IDDet") Then
                                If ItemDetCot.ActualizQuiebre Then
                                    blnActualiz = True
                                End If
                                Exit For
                            End If
                        Else
                            blnActualiz = True
                            Exit For
                        End If
                    Next
                End If
                If Not blnActualiz Then GoTo Sgte
                If drCotiDet("IDDetRel") <> 0 Then
                    GoTo Sgte
                End If


                Dim strIDTipoProv As String = drCotiDet("IDTipoProv").ToString
                If drCotiDet("IncGuia") = True Then
                    strIDTipoProv = "003" 'cualquier tipo q no sea hotel ni restaurant


                End If



                'Dim ListaDetCotQ As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
                'Dim dttDetQui As DataTable = ConsultarxIDCab_Q(vintIDCab_Q)

                'For Each drDetQui As DataRow In dttDetQui.Rows
                'dblCostoReal = drDetQui("CostoReal")
                'dblMargenAplicado = drDetQui("MargenAplicado")
                dblCostoReal = -1
                dblMargenAplicado = -1
                dblSSCR = -1
                dblSTCR = -1

                'If drCotiDet("CostoRealAnt") <> 0 Then
                '    If drCotiDet("CostoRealEditado") = True Then
                '        dblCostoReal = drCotiDet("CostoReal")
                '    End If
                'End If
                'If drCotiDet("MargenAplicadoAnt") <> 0 Then
                '    If drCotiDet("MargenAplicadoEditado") = True Then
                '        dblMargenAplicado = drCotiDet("MargenAplicado")
                '    End If
                'End If
                If drCotiDet("Especial") = True Then
                    If drCotiDet("CostoRealEditado") = True Then
                        dblCostoReal = drCotiDet("CostoReal")
                    End If
                    If drCotiDet("MargenAplicadoEditado") = True Then
                        dblMargenAplicado = drCotiDet("MargenAplicado")
                    End If
                    If drCotiDet("FlSSCREditado") = True Then
                        dblSSCR = drCotiDet("SSCR")
                    End If
                    If drCotiDet("FlSTCREditado") = True Then
                        dblSTCR = drCotiDet("STCR")
                    End If
                End If

                Dim cnn As SqlClient.SqlConnection = objADO.GetConnection

                dc = objCoti.CalculoCotizacion(drCotiDet("IDServicio").ToString, drCotiDet("IDServicio_Det").ToString, _
                drCotiDet("Anio").ToString, drCotiDet("Tipo").ToString, _
                strIDTipoProv, vintPax, vintLiberados, vstrTipo_Lib, _
                drCotiDet("MargenCotiCab").ToString, drCotiDet("TipoCambioCotiCab").ToString, _
                dblCostoReal, dblMargenAplicado, vstrTipoPax, vdecRedondeo, dttCreaDttDetCotiServ, _
                vsglIGV, , drCotiDet("IncGuia"), , cnn, drCotiDet("Dia"), dblSSCR, dblSTCR, , , , drCotiDet("FlAcomodosVehic"))

                cnn.Close()
                'ListaDetCotQ = New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
                objBEDetQ = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE

                For Each dic As KeyValuePair(Of String, Double) In dc
                    If dic.Key = "CostoReal" Then objBEDetQ.CostoReal = dic.Value
                    If dic.Key = "Margen" Then objBEDetQ.Margen = dic.Value
                    If dic.Key = "CostoLiberado" Then objBEDetQ.CostoLiberado = dic.Value
                    If dic.Key = "MargenLiberado" Then objBEDetQ.MargenLiberado = dic.Value
                    If dic.Key = "MargenAplicado" Then objBEDetQ.MargenAplicado = dic.Value
                    If dic.Key = "TotImpto" Then objBEDetQ.TotImpto = dic.Value
                    If dic.Key = "Total" Then objBEDetQ.Total = dic.Value
                    If dic.Key = "RedondeoTotal" Then objBEDetQ.RedondeoTotal = dic.Value
                    If dic.Key = "SSCR" Then objBEDetQ.SSCR = dic.Value
                    If dic.Key = "SSMargen" Then objBEDetQ.SSMargen = dic.Value
                    If dic.Key = "SSCL" Then objBEDetQ.SSCL = dic.Value
                    If dic.Key = "SSML" Then objBEDetQ.SSMargenLiberado = dic.Value
                    If dic.Key = "SSTotal" Then objBEDetQ.SSTotal = dic.Value
                    If dic.Key = "STCR" Then objBEDetQ.STCR = dic.Value
                    If dic.Key = "STMargen" Then objBEDetQ.STMargen = dic.Value
                    If dic.Key = "STTotal" Then objBEDetQ.STTotal = dic.Value

                    If dic.Key = "CostoRealImpto" Then objBEDetQ.CostoRealImpto = dic.Value
                    If dic.Key = "CostoLiberadoImpto" Then objBEDetQ.CostoLiberadoImpto = dic.Value
                    If dic.Key = "MargenImpto" Then objBEDetQ.MargenImpto = dic.Value
                    If dic.Key = "MargenLiberadoImpto" Then objBEDetQ.MargenLiberadoImpto = dic.Value
                    If dic.Key = "SSCLImpto" Then objBEDetQ.SSCLImpto = dic.Value
                    If dic.Key = "SSCRImpto" Then objBEDetQ.SSCRImpto = dic.Value
                    If dic.Key = "SSMargenImpto" Then objBEDetQ.SSMargenImpto = dic.Value
                    If dic.Key = "SSMargenLiberadoImpto" Then objBEDetQ.SSMargenLiberadoImpto = dic.Value
                    If dic.Key = "STCRImpto" Then objBEDetQ.STCRImpto = dic.Value
                    If dic.Key = "STMargenImpto" Then objBEDetQ.STMargenImpto = dic.Value
                Next

                With objBEDetQ

                    If vstrAccion = "M" Then

                        'If blnCotiDetHasRows Then
                        '.IDDet_Q = intConsultarIDDet_QxIDCab_Q(vintIDCab_Q)
                        'Else
                        .IDDet_Q = drCotiDet("IDDet_Q")
                        'End If

                    End If

                    '.IDDet = drCotiDet("IDDet").ToString
                    .IDDet = drCotiDet("IDDet").ToString
                    .IDCab_Q = vintIDCab_Q
                    '.CostoLiberadoImpto = .CostoLiberado * (1 + (vsglIGV / 100))
                    '.CostoRealImpto = .CostoReal * (1 + (vsglIGV / 100))
                    '.MargenImpto = .Margen * (1 + (vsglIGV / 100))
                    '.MargenLiberadoImpto = .MargenLiberado * (1 + (vsglIGV / 100))
                    '.SSCLImpto = .SSCL * (1 + (vsglIGV / 100))
                    '.SSCRImpto = .SSCR * (1 + (vsglIGV / 100))
                    '.SSMargenImpto = .SSMargen * (1 + (vsglIGV / 100))
                    '.SSMargenLiberadoImpto = .SSMargenLiberado * (1 + (vsglIGV / 100))
                    '.STCRImpto = .STCR * (1 + (vsglIGV / 100))
                    '.STMargenImpto = .STMargen * (1 + (vsglIGV / 100))
                    '.TotImpto = .CostoLiberadoImpto + .CostoRealImpto + .MargenImpto + .MargenLiberadoImpto '+ _
                    '.SSCLImpto(+.SSCRImpto + .SSMargenImpto + .SSMargenLiberadoImpto + .STCRImpto + .STMargenImpto)
                    .UserMod = vstrUser
                    .Accion = vstrAccion

                End With
                ListaDetCotQ.Add(objBEDetQ)


                'Next

                'objBE = New clsCotizacionBE
                'objBE.ListaDetalleCotizacionQuiebres = ListaDetCotQ

                'InsertarActualizarEliminar(objBE)
Sgte:
            Next

            objBE = New clsCotizacionBE
            objBE.ListaDetalleCotizacionQuiebres = ListaDetCotQ

            InsertarActualizarEliminar(objBE)

            'cnn.Close()
            'cnn2.Close()

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Function ConsultarxIDCab_Q(ByVal vintIDCab_Q As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDCab_Q", vintIDCab_Q)

            Return objADO.GetDataTable("COTIDET_QUIEBRES_SelxIDCab_Q", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function
    Private Function intConsultarIDDet_QxIDCab_Q(ByVal vintIDCab_Q As Int32) As Int32
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDCab_Q", vintIDCab_Q)
            dc.Add("@pIDDet_Q", 0)

            Return objADO.ExecuteSPOutput("COTIDET_QUIEBRES_SelxIDCab_Q_IDDet_QOutput", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function
    Private Function dttCreaDttDetCotiServ() As DataTable

        Dim dttDetCotiServ As New DataTable("DetCotiServ")
        With dttDetCotiServ
            .Columns.Add("IDDet")
            .Columns.Add("IDServicio")
            .Columns.Add("IDServicio_Det")
            .Columns.Add("dblCostoReal")
            .Columns.Add("dblMargen")
            .Columns.Add("dblCostoLiberado")
            .Columns.Add("dblMargenLiberado")
            .Columns.Add("dblMargenAplicado")
            .Columns.Add("dblTotal")
            .Columns.Add("RedondeoTotal")
            .Columns.Add("dblSSCR")
            .Columns.Add("dblSSMargen")
            .Columns.Add("dblSSCL")
            .Columns.Add("dblSSML")
            .Columns.Add("dblSSTotal")
            .Columns.Add("dblSTCR")
            .Columns.Add("dblSTMargen")
            .Columns.Add("dblSTTotal")

            .Columns.Add("CostoRealImpto")
            .Columns.Add("CostoLiberadoImpto")
            .Columns.Add("MargenImpto")
            .Columns.Add("MargenLiberadoImpto")
            .Columns.Add("SSCRImpto")
            .Columns.Add("SSCLImpto")
            .Columns.Add("SSMargenImpto")
            .Columns.Add("SSMargenLiberadoImpto")
            .Columns.Add("STCRImpto")
            .Columns.Add("STMargenImpto")
            .Columns.Add("TotImpto")

        End With


        Return dttDetCotiServ
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetallePaxVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsDetallePaxVentasCotizacionBT"
    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_PAX_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal BE As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_PAX_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaDetPax Is Nothing Then Exit Sub
        Try
            Dim objPaxBT As New clsPaxVentasCotizacionBT
            For Each Item As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                If Item.Accion = "N" Then
                    If IsNumeric(Item.IDDet) And IsNumeric(Item.IDPax) Then
                        If Not blnExiste(Item.IDDet, Item.IDPax) And blnExistePax(Item.IDPax) Then
                            Insertar(Item)
                            'objPaxBT.ActualizarTotales(Item.IDPax, BE.IdCab, Item.IDDet, True, Item.IDTipoProv, Item.UserMod)
                        End If
                    End If
                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.IDDet) And IsNumeric(Item.IDPax) Then
                        Eliminar(Item)
                        'objPaxBT.ActualizarTotales(Item.IDPax, BE.IdCab, Item.IDDet, False, Item.IDTipoProv, Item.UserMod)
                    End If


                End If
                'objPaxBT.ActualizarTotales(Item.IDPax, BE.IdCab, Item.IDDet, True, Item.IDTipoProv, Item.UserMod)
                'ERROR : 17/10/2014
                If IsNumeric(Item.IDPax) Then
                    objPaxBT.ActualizarTotales(Item.IDPax, BE.IdCab, Item.UserMod)
                End If

            Next
            'BE.ListaPax
            If BE.ListaDetPax.Count = 0 Then
                If Not BE.ListaPax Is Nothing Then
                    For Each Item As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                        If IsNumeric(Item.IDPax) Then
                            objPaxBT.ActualizarTotales(Item.IDPax, BE.IdCab, Item.UserMod)
                        End If
                    Next

                End If

            End If

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("COTIDET_PAX_DelxIDDet", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
        End Try
    End Sub

    Public Sub EliminarxIDPax(ByVal vintIDPax As Int32)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDPax", vintIDPax)

            objADO.ExecuteSP("COTIDET_PAX_DelxIDPax", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDPax")
        End Try
    End Sub
    'Public Sub InsertarxIDPax(ByVal vintIDCab As Integer, ByVal vintIDPax As Int32, ByVal vstrUserMod As String)
    '    Try
    '        Dim dc As New Dictionary(Of String, String)

    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@IDPax", vintIDPax)
    '        dc.Add("@UserMod", vstrUserMod)

    '        objADO.ExecuteSP("COTIDET_PAX_InsxIDPax", dc)
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxIDPax")
    '    End Try
    'End Sub
    Public Sub ActivarDesactxIDPaxNoShow(ByVal vintIDCab As Integer, ByVal vintIDPax As Int32, ByVal vblnFlDesactNoShow As Boolean, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDPax", vintIDPax)
            dc.Add("@FlDesactNoShow", vblnFlDesactNoShow)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIDET_PAX_UpdxIDPaxNoShow", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".InsertarxIDPax")
        End Try
    End Sub

    Public Function ConsultarDetxIDDet(ByVal vintIDDet As Int32) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDDet", vintIDDet)

            Return objADO.GetDataTable("COTIDET_PAX_Sel_xIDDet_Lvw", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function

    Public Sub InsertarxIDDet(ByVal vintIDCab As Int32, ByVal vintIDDet As Int32, _
                              ByVal vstrIDTipoProv As String, ByVal vstrUserMod As String)
        Try

            'Dim objBLPax As New clsCotizacionPaxBT
            Dim objBLPax As New clsPaxVentasCotizacionBT
            Dim dttPax As DataTable = objBLPax.ConsultarListxIDCabLvw(vintIDCab)
            Dim objBLDetPax As New clsDetallePaxVentasCotizacionBT
            'Dim objBLDetPax As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
            Dim objBEDetPax As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE
            Dim ListaDetPax As New List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE)
            Dim BE As New clsCotizacionBE

            For Each dr As DataRow In dttPax.Rows
                objBEDetPax = New clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE
                With objBEDetPax
                    .IDDet = vintIDDet
                    .IDPax = dr("IDPax")
                    .Selecc = True
                    .Accion = "N"
                    .IDTipoProv = vstrIDTipoProv
                    .UserMod = vstrUserMod
                End With
                ListaDetPax.Add(objBEDetPax)
            Next
            BE.ListaDetPax = ListaDetPax

            'objBLDetPax.InsertarEliminar(BE, 0, True)
            objBLDetPax.InsertarActualizarEliminar(BE)


            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Public Function blnExiste(ByVal vintIDDet As Int32, ByVal vintIDPax As Int32) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)
            dc.Add("@IDPax", vintIDPax)
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("COTIDET_PAX_Sel_ExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnExistePax(ByVal vintIDPax As Int32) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDPax", vintIDPax)
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("COTIPAX_Sel_ExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleDescripServicioVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsDetalleDescripServicioVentasCotizacionBT"
    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@IDIDioma", BE.IDIdioma)
            dc.Add("@DescLarga", BE.DescLarga)

            dc.Add("@NoHotel", BE.NoHotel)
            dc.Add("@TxWebHotel", BE.TxWebHotel)

            dc.Add("@TxDireccHotel", BE.TxDireccHotel)
            dc.Add("@TxTelfHotel1", BE.TxTelfHotel1)
            dc.Add("@TxTelfHotel2", BE.TxTelfHotel2)
            dc.Add("@FeHoraChkInHotel", Format(BE.FeHoraChkInHotel, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@FeHoraChkOutHotel", Format(BE.FeHoraChkOutHotel, "dd/MM/yyyy HH:mm:ss"))
            dc.Add("@CoTipoDesaHotel", BE.CoTipoDesaHotel)
            dc.Add("@CoCiudadHotel", BE.CoCiudadHotel)

            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_DESCRIPSERV_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw

        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@IDIDioma", BE.IDIdioma)
            dc.Add("@DescLarga", BE.DescLarga)

            dc.Add("@NoHotel", BE.NoHotel)
            dc.Add("@TxWebHotel", BE.TxWebHotel)

            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_DESCRIPSERV_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal vintIDDet As Integer, ByVal vstrIDIdioma As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)
            dc.Add("@IDIDioma", vstrIDIdioma)

            objADO.ExecuteSP("COTIDET_DESCRIPSERV_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("COTIDET_DESCRIPSERV_DelxIDDet", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
        End Try
    End Sub
    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        Try
            If Not BE.ListaDetalleCotizacionDescripServicio Is Nothing Then
                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE In BE.ListaDetalleCotizacionDescripServicio
                    If Not Item.IDDet Is Nothing Then
                        If Item.IDDet <> "0" Then
                            If Item.Accion = "N" Then
                                If IsNumeric(Item.IDDet) Then
                                    If Not blnExiste(Item.IDDet, Item.IDIdioma) Then
                                        Insertar(Item)
                                    End If
                                End If
                            ElseIf Item.Accion = "M" Then
                                Actualizar(Item)
                            ElseIf Item.Accion = "B" Then
                                If IsNumeric(Item.IDDet) Then
                                    Eliminar(Item.IDDet, Item.IDIdioma)
                                End If
                            End If
                        End If
                    End If
                Next

                ContextUtil.SetComplete()
            End If

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function blnExiste(ByVal vintIDDet As Integer, ByVal vstrIDIoma As String) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", CInt(vintIDDet))
            dc.Add("@IDIdioma", vstrIDIoma)
            dc.Add("@pExiste", False)

            Return objADO.GetSPOutputValue("COTIDET_DESCRIPSERV_SelExiste", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleVentasCotizacionDetServiciosBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsDetalleVentasCotizacionDetServiciosBT"
    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)

            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STMargenImpto", BE.STMargenImpto)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@Total", BE.Total)
            dc.Add("@RedondeoTotal", BE.RedondeoTotal)
            dc.Add("@TotImpto", BE.TotImpto)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_DETSERVICIOS_Ins", dc)



            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@IDServicio_Det", BE.IDServicio_Det)

            dc.Add("@CostoLiberado", BE.CostoLiberado)
            dc.Add("@CostoLiberadoImpto", BE.CostoLiberadoImpto)
            dc.Add("@CostoReal", BE.CostoReal)
            dc.Add("@CostoRealImpto", BE.CostoRealImpto)
            dc.Add("@Margen", BE.Margen)
            dc.Add("@MargenAplicado", BE.MargenAplicado)
            dc.Add("@MargenImpto", BE.MargenImpto)
            dc.Add("@MargenLiberado", BE.MargenLiberado)
            dc.Add("@MargenLiberadoImpto", BE.MargenLiberadoImpto)
            dc.Add("@SSCL", BE.SSCL)
            dc.Add("@SSCLImpto", BE.SSCLImpto)
            dc.Add("@SSCR", BE.SSCR)
            dc.Add("@SSCRImpto", BE.SSCRImpto)
            dc.Add("@SSMargen", BE.SSMargen)
            dc.Add("@SSMargenImpto", BE.SSMargenImpto)
            dc.Add("@SSMargenLiberado", BE.SSMargenLiberado)
            dc.Add("@SSMargenLiberadoImpto", BE.SSMargenLiberadoImpto)
            dc.Add("@SSTotal", BE.SSTotal)
            dc.Add("@STCR", BE.STCR)
            dc.Add("@STCRImpto", BE.STCRImpto)
            dc.Add("@STMargen", BE.STMargen)
            dc.Add("@STMargenImpto", BE.STMargenImpto)
            dc.Add("@STTotal", BE.STTotal)
            dc.Add("@Total", BE.Total)
            dc.Add("@RedondeoTotal", BE.RedondeoTotal)
            dc.Add("@TotImpto", BE.TotImpto)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_DETSERVICIOS_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Private Function blnExiste(ByVal vintIDDet As Int32, ByVal vintIDServicio_Det As Int32) As Boolean
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@pExiste", False)

            Return objADO.ExecuteSPOutput("COTIDET_DETSERVICIOS_Sel_ExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaDetalleCotizacionDetServicio Is Nothing Then Exit Sub
        Try

            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE In BE.ListaDetalleCotizacionDetServicio
                If IsNumeric(Item.IDDet) Then
                    If Not blnExiste(Item.IDDet, Item.IDServicio_Det) Then
                        Insertar(Item)
                    Else
                        Actualizar(Item)
                    End If
                    'Else
                    '    Insertar(Item)
                End If
            Next

            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("COTIDET_DETSERVICIOS_DelxIDDet", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDDet")
        End Try
    End Sub

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsTiposCambioVentasCotizacionBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Dim strNombreClase As String = "clsCotizacionTiposCambioBT"

    Private Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionTiposCambioBE, _
                        Optional ByVal vintIDCab As Integer = 0)
        Dim dc As New Dictionary(Of String, String)
        Try
            If vintIDCab <> 0 Then
                dc.Add("@IDCab", vintIDCab)
            Else
                dc.Add("@IDCab", BE.IDCab)
            End If
            dc.Add("@CoMoneda", BE.CoMoneda)
            dc.Add("@SsTipCam", BE.SsTipCam)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTICAB_TIPOSCAMBIO_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionTiposCambioBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", BE.IDCab)
            dc.Add("@CoMoneda", BE.CoMoneda)
            dc.Add("@SsTipCam", BE.SsTipCam)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTICAB_TIPOSCAMBIO_Upd", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Private Sub Eliminar(ByVal vintIDCab As Integer, ByVal vstrCoMoneda As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoMoneda", vstrCoMoneda)

            objADO.ExecuteSP("COTICAB_TIPOSCAMBIO_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Erro: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaTipoCambios Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsCotizacionTiposCambioBE In BE.ListaTipoCambios
                If Item.Accion = "B" Then
                    Eliminar(Item.IDCab, Item.CoMoneda)
                End If
            Next

            For Each Item As clsCotizacionBE.clsCotizacionTiposCambioBE In BE.ListaTipoCambios
                If Item.Accion = "N" Then
                    Insertar(Item, BE.IdCab)
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub


End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsViaticosTCCotizacionBT
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT
    Dim strNombreClase As String = "clsViaticosTCCotizacionBT"

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionViaticosTCBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@Dia", BE.Dia)
                .Add("@IDCab", BE.IDCab)
                .Add("@IDServicio_Det", BE.IDServicio_Det)
                .Add("@Item", BE.Item)
                .Add("@CostoUSD", BE.CostoUSD)
                .Add("@CostoSOL", BE.CostoSOL)
                .Add("@FlActivo", BE.FlActivo)
                .Add("@UserMod", BE.UserMod)
            End With

            objADO.ExecuteSP("VIATICOS_TOURCONDUCTOR_Ins", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Public Overloads Sub Insertar(ByVal BE As clsCotizacionBE)
        If BE.ListaViaticosTourConductor Is Nothing Then Exit Sub
        Dim dc As New Dictionary(Of String, String)
        Try
            EliminarxIDCab(BE.IdCab)
            For Each Item As clsCotizacionBE.clsCotizacionViaticosTCBE In BE.ListaViaticosTourConductor
                Insertar(Item)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub EliminarxIDCab(ByVal vintIDCab As Integer)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            objADO.ExecuteSP("VIATICOS_TOURCONDUCTOR_DelxIDCab", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".EliminarxIDCab")
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsVuelosVentasCotizacionBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsVuelosVentasCotizacionBT"

    Private Overloads Function Insertar(ByVal BE As clsCotizacionBE.clsCotizacionVuelosBE) As Integer
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", BE.IdCab)
            dc.Add("@Cotizacion", BE.Cotizacion)
            dc.Add("@Servicio", BE.Servicio)
            dc.Add("@Iddet", BE.IDDet)
            dc.Add("@NombrePax", BE.NombrePax)
            dc.Add("@Ruta", BE.Ruta)
            dc.Add("@NumBoleto", BE.NumBoleto)
            dc.Add("@RutaD", BE.RutaD)
            dc.Add("@RutaO", BE.RutaO)
            dc.Add("@Vuelo", BE.Vuelo)
            dc.Add("@FecEmision", BE.FecEmision)
            dc.Add("@FecDocumento", BE.FecDocumento)
            dc.Add("@FecSalida", BE.FecSalida)
            dc.Add("@FecRetorno", BE.FecRetorno)
            dc.Add("@Salida", BE.Salida)
            dc.Add("@HrRecojo", BE.HrRecojo)
            dc.Add("@PaxC", BE.PaxC)
            dc.Add("@PCotizado", BE.PCotizado)
            dc.Add("@TCotizado", BE.TCotizado)
            dc.Add("@PaxV", BE.PaxV)
            dc.Add("@PVolado", BE.PVolado)
            dc.Add("@TVolado", BE.TVolado)
            dc.Add("@Status", BE.Status)
            dc.Add("@Comentario", BE.Comentario)
            dc.Add("@Llegada", BE.Llegada)
            dc.Add("@Emitido", BE.Emitido)
            dc.Add("@Identificador", BE.TipoTransporte)
            dc.Add("@CodReserva", BE.CodReserva)
            dc.Add("@Tarifa", BE.Tarifa)
            dc.Add("@PTA", BE.PTA)
            dc.Add("@ImpuestosExt", BE.ImpuestosExt)
            dc.Add("@Penalidad", BE.Penalidad)
            dc.Add("@IGV", BE.IGV)
            dc.Add("@Otros", BE.Otros)
            dc.Add("@PorcComm", BE.PorcComm)
            dc.Add("@MontoContado", BE.MontoContado)
            dc.Add("@MontoTarjeta", BE.MontoTarjeta)
            dc.Add("@IdTarjCred", BE.IDTarjetaCredito)
            dc.Add("@NumeroTarjeta", BE.NumeroTarjeta)
            dc.Add("@Aprobacion", BE.Aprobacion)
            dc.Add("@PorcOver", BE.PorcOver)
            dc.Add("@PorcOverInCom", BE.PorcOverInCom)
            dc.Add("@MontComm", BE.MontComm)
            dc.Add("@MontCommOver", BE.MontCommOver)
            dc.Add("@IGVComm", BE.IGVComm)
            dc.Add("@IGVCommOver", BE.IGVCommOver)
            dc.Add("@PorcTarifa", BE.PorcTarifa)
            dc.Add("@NetoPagar", BE.NetoPagar)
            dc.Add("@Descuento", BE.Descuento)
            dc.Add("@IdLineaA", BE.IDLineaA)
            dc.Add("@MontoFEE", BE.MontoFEE)
            dc.Add("@IGVFEE", BE.IGVFEE)
            dc.Add("@TotalFEE", BE.TotalFEE)
            dc.Add("@EmitidoSetours", BE.EmitidoSetours)
            dc.Add("@EmitidoOperador", BE.EmitidoOperador)
            dc.Add("@IDProveedorOperador", BE.IDProveedorOperador)
            dc.Add("@CostoOperador", BE.CostoOperador)
            dc.Add("@IdaVuelta", BE.IdaVuelta)
            dc.Add("@IDServicio_DetPeruRail", BE.IDServicio_DetPeruRail)
            dc.Add("@CoTicket", BE.CoTicket)
            dc.Add("@NuSegmento", BE.NuSegmento)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@pIDTransporte", 0)

            Dim intIDTransporte As Integer = objADO.ExecuteSPOutput("COTIVUELOS_Ins", dc)

            ContextUtil.SetComplete()

            Return intIDTransporte
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function
    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionVuelosBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", BE.IdCab)
            dc.Add("@Cotizacion", BE.Cotizacion)
            dc.Add("@Servicio", BE.Servicio)
            dc.Add("@ID", BE.ID)
            dc.Add("@NombrePax", BE.NombrePax)
            dc.Add("@Ruta", BE.Ruta)
            dc.Add("@NumBoleto", BE.NumBoleto)
            dc.Add("@RutaD", BE.RutaD)
            dc.Add("@RutaO", BE.RutaO)
            dc.Add("@Vuelo", BE.Vuelo)
            dc.Add("@FecEmision", BE.FecEmision)
            dc.Add("@FecDocumento", BE.FecDocumento)
            dc.Add("@FecSalida", BE.FecSalida)
            dc.Add("@FecRetorno", BE.FecRetorno)
            dc.Add("@Salida", BE.Salida)
            dc.Add("@HrRecojo", BE.HrRecojo)
            dc.Add("@PaxC", BE.PaxC)
            dc.Add("@PCotizado", BE.PCotizado)
            dc.Add("@TCotizado", BE.TCotizado)
            dc.Add("@PaxV", BE.PaxV)
            dc.Add("@PVolado", BE.PVolado)
            dc.Add("@TVolado", BE.TVolado)
            dc.Add("@Status", BE.Status)
            dc.Add("@Comentario", BE.Comentario)
            dc.Add("@Llegada", BE.Llegada)
            dc.Add("@Emitido", BE.Emitido)
            dc.Add("@Identificador", BE.TipoTransporte)
            dc.Add("@CodReserva", BE.CodReserva)
            dc.Add("@Tarifa", BE.Tarifa)
            dc.Add("@PTA", BE.PTA)
            dc.Add("@ImpuestosExt", BE.ImpuestosExt)
            dc.Add("@Penalidad", BE.Penalidad)
            dc.Add("@IGV", BE.IGV)
            dc.Add("@Otros", BE.Otros)
            dc.Add("@PorcComm", BE.PorcComm)
            dc.Add("@MontoContado", BE.MontoContado)
            dc.Add("@MontoTarjeta", BE.MontoTarjeta)
            dc.Add("@IdTarjCred", BE.IDTarjetaCredito)
            dc.Add("@NumeroTarjeta", BE.NumeroTarjeta)
            dc.Add("@Aprobacion", BE.Aprobacion)
            dc.Add("@PorcOver", BE.PorcOver)
            dc.Add("@PorcOverInCom", BE.PorcOverInCom)
            dc.Add("@MontComm", BE.MontComm)
            dc.Add("@MontCommOver", BE.MontCommOver)
            dc.Add("@IGVComm", BE.IGVComm)
            dc.Add("@IGVCommOver", BE.IGVCommOver)
            dc.Add("@PorcTarifa", BE.PorcTarifa)
            dc.Add("@NetoPagar", BE.NetoPagar)
            dc.Add("@Descuento", BE.Descuento)
            dc.Add("@IdLineaA", BE.IDLineaA)
            dc.Add("@MontoFEE", BE.MontoFEE)
            dc.Add("@IGVFEE", BE.IGVFEE)
            dc.Add("@TotalFEE", BE.TotalFEE)
            dc.Add("@EmitidoSetours", BE.EmitidoSetours)
            dc.Add("@EmitidoOperador", BE.EmitidoOperador)
            dc.Add("@IDProveedorOperador", BE.IDProveedorOperador)
            dc.Add("@CostoOperador", BE.CostoOperador)
            dc.Add("@IdaVuelta", BE.IdaVuelta)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@FlMotivo", BE.FlMotivo)
            dc.Add("@DescMotivo", BE.DescMotivo)
            objADO.ExecuteSP("COTIVUELOS_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub ActualizarVueloxTickets(ByVal BE As clsCotizacionBE.clsCotizacionVuelosBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDTrans", BE.ID)
                .Add("@RutaD", BE.RutaD)
                .Add("@RutaO", BE.RutaO)
                .Add("@Ruta", BE.Ruta)
                .Add("@FecEmision", BE.FecEmision.ToShortDateString)
                .Add("@IDLineaAerea", BE.IDLineaA)
                .Add("@NumVuelo", BE.Vuelo)
                .Add("@NombrePax", BE.NombrePax)
                .Add("@TarifaBase", BE.Tarifa)
                .Add("@FechaSalida", BE.FecSalida.ToString("dd/MM/yyyy hh:mm:ss"))
                .Add("@Salida", BE.Salida)
                .Add("@Llegada", CDate(BE.Llegada).ToString("hh:mm"))
                .Add("@CodReserva", BE.CodReserva)
            End With
            objADO.ExecuteSP("COTIVUELO_UpdxTicketItinerario_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarVueloxTickets")
        End Try
    End Sub

    Private Overloads Sub ActualizarTicketxTransporte(ByVal Item As clsCotizacionBE.clsCotizacionVuelosBE)
        Dim dc As New Dictionary(Of String, String)
        Try
            If Item.CoTicket <> "" And Item.NuSegmento <> 0 Then
                Dim strCoLineaAerea As String = "", strNoLineaAerea As String = ""
                Using objBL As New clsProveedorBN
                    Using dr As SqlClient.SqlDataReader = objBL.ConsultarPk(Item.IDLineaA)
                        dr.Read()
                        If dr.HasRows Then
                            strCoLineaAerea = If(IsDBNull(dr("CoIATA")) = True, "", dr("CoIATA").ToString)
                            strNoLineaAerea = If(IsDBNull(dr("NombreCorto")) = True, "", dr("NombreCorto").ToString)
                        End If
                        dr.Close()
                    End Using
                End Using

                Dim oTicketBE As New clsTicketAMADEUSBE
                With oTicketBE
                    .CoTicket = Item.CoTicket
                    .NoLineaAerea = strNoLineaAerea
                    .CoLineaAereaSigla = strCoLineaAerea
                    .CoPNR_Aerolinea = strCoLineaAerea & " " & Item.CodReserva
                    .TxTotal = Item.CostoOperador
                    .UserMod = Item.UserMod
                End With

                Dim oLstTicketItinerarioBE As New List(Of clsTicketAMADEUS_ITINERARIOBE)
                Dim oTicketItinerariBE As New clsTicketAMADEUS_ITINERARIOBE
                With oTicketItinerariBE
                    .CoTicket = Item.CoTicket
                    .NuSegmento = Item.NuSegmento
                    .CoLineaAerea = strCoLineaAerea
                    .NuVuelo = Replace(Item.Vuelo, strCoLineaAerea, "").Trim
                    .CoPNR_Aerolinea = strCoLineaAerea & " " & Item.CodReserva
                    .TxFechaPar = CDate(Item.FecSalida.ToShortDateString & " " & Item.Salida)
                    .TxHoraLle = CDate("01/01/1900 " & Item.Llegada)
                    .UserMod = Item.UserMod
                End With
                oLstTicketItinerarioBE.Add(oTicketItinerariBE)
                oTicketBE.ListaItinerarioTicket = oLstTicketItinerarioBE

                Dim objBTIt As New clsTicketItinerarioBT
                objBTIt.ActualizarTicketingxVuelo(oTicketBE)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintID As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDTransporte", vintID)
            objADO.ExecuteSP("COTITRANSP_PAX_DelxIDTransporte", dc)

            dc = New Dictionary(Of String, String)
            dc.Add("@ID", vintID)
            objADO.ExecuteSP("COTIVUELOS_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Private Overloads Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsCotizacionBE, ByVal vstrAntiguoID As String, _
                                                       ByVal vintNuevoID As Integer)
        Try
            If Not BE.ListaCotizacionVuelo Is Nothing Then
                For Each ItemDgv As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                    If ItemDgv.ID = vstrAntiguoID Then
                        ItemDgv.ID = vintNuevoID
                    End If
                Next
            End If

            If Not BE.ListaPaxTransporte Is Nothing Then
                For Each ItemDgv As clsCotizacionBE.clsCotizacionVuelosBE.clsPaxTransporteBE In BE.ListaPaxTransporte
                    If ItemDgv.IDTransporte = vstrAntiguoID Then
                        ItemDgv.IDTransporte = vintNuevoID
                    End If
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaCotizacionVuelo Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                If Item.Accion = "N" Then
                    Dim intIDTransp As Integer = Insertar(Item)
                    ActualizarIDsNuevosenHijosST(BE, Item.ID, intIDTransp)

                ElseIf Item.Accion = "M" Then
                    If IsNumeric(Item.ID) Then
                        Actualizar(Item)
                        ActualizarTicketxTransporte(Item)
                    End If
                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.ID) Then
                        Eliminar(Item.ID)
                    End If
                End If
            Next

            'Pax Transporte
            Dim objTranspPax As New clsPaxTransporteBT
            objTranspPax.InsertarActualizarEliminar(BE)

            If Not BE.ListaPaxTransporte Is Nothing Then
                BE.ListaPaxTransporte = ListPaxTransporte(BE)
                objTranspPax.InsertarActualizarEliminar(BE)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Private Function ListPaxTransporte(ByVal BE As clsCotizacionBE) As List(Of clsCotizacionBE.clsPaxTransporteBE)
        If BE.ListaDetPax Is Nothing Then Return New List(Of clsCotizacionBE.clsPaxTransporteBE)
        Try
            Dim ListaPaxTransportes As New List(Of clsCotizacionBE.clsPaxTransporteBE)

            If BE.ListaDetPax.Count > 0 Then
                For Each ItemVuelo As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                    If ItemVuelo.IDServicio_DetPeruRail = 0 Then
                        For Each ItemDetPax As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                            If ItemDetPax.IDDet = ItemVuelo.IDDet Then
                                Dim ItemPaxTransporte As New clsCotizacionBE.clsPaxTransporteBE
                                If ItemVuelo.Accion = "N" Then
                                    ItemPaxTransporte.Accion = "N"
                                ElseIf ItemVuelo.Accion = "M" Then
                                    Dim objDetPax As New clsDetallePaxVentasCotizacionBT

                                    If IsNumeric(ItemDetPax.IDPax) Then



                                        If Not objDetPax.blnExiste(ItemVuelo.IDDet, ItemDetPax.IDPax) Then
                                            ItemPaxTransporte.Accion = "B"
                                            ItemPaxTransporte.IDPaxTransp = 0
                                        End If

                                    End If
                                ElseIf ItemVuelo.Accion = "B" Then
                                    ItemPaxTransporte.Accion = "B"
                                End If

                                ItemPaxTransporte.IDTransporte = ItemVuelo.ID
                                ItemPaxTransporte.IDPax = ItemDetPax.IDPax
                                ItemPaxTransporte.IDProveedorGuia = ""
                                ItemPaxTransporte.CodReservaTransp = ""
                                ItemPaxTransporte.UserMod = ItemVuelo.UserMod


                                ListaPaxTransportes.Add(ItemPaxTransporte)
                            End If
                        Next


                    End If
                Next
            Else 'Vuelos sin IDDet
                For Each ItemVuelo As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                    Dim objPax As New clsPaxVentasCotizacionBT
                    If ItemVuelo.Accion = "N" Then
                        Dim dttPax As DataTable = objPax.ConsultarListxIDCab(BE.IdCab)
                        For Each dr As DataRow In dttPax.Rows
                            Dim ItemPaxTransporte As New clsCotizacionBE.clsPaxTransporteBE
                            ItemPaxTransporte.IDTransporte = ItemVuelo.ID
                            ItemPaxTransporte.IDPax = dr("IDPax")
                            ItemPaxTransporte.IDProveedorGuia = ""
                            ItemPaxTransporte.CodReservaTransp = ""
                            ItemPaxTransporte.UserMod = ItemVuelo.UserMod
                            ItemPaxTransporte.Accion = "N"

                            ListaPaxTransportes.Add(ItemPaxTransporte)

                        Next
                    End If


                Next

            End If

            Return ListaPaxTransportes
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub ActualizarCostosTrenesPeruRail(ByVal vListaTickets As List(Of clsCotizacionBE.clsTicketsPaxPeruRailBE), ByVal vstrUserMod As String)

        Try

            Dim ListaExcelDet As New List(Of clsCotizacionBE.clsTicketsPaxPeruRailBE)

            For Each ItemExcel As clsCotizacionBE.clsTicketsPaxPeruRailBE In vListaTickets
                Dim NewItem As New clsCotizacionBE.clsTicketsPaxPeruRailBE With {.NuFile = ItemExcel.NuFile, _
                                                                                       .CoReserva = ItemExcel.CoReserva, _
                                                                                       .CoCiudadOri = ItemExcel.CoCiudadOri, _
                                                                                       .CoCiudadDes = ItemExcel.CoCiudadDes, _
                                                                                       .SsValorVentaUSD = ItemExcel.SsValorVentaUSD, _
                                                                                       .SsIgvUSD = ItemExcel.SsIgvUSD, _
                                                                                       .SsTotalUSD = ItemExcel.SsTotalUSD, _
                                                                                      .FeEmision = ItemExcel.FeEmision, _
                                                                                      .NuDocum = ItemExcel.NuDocum}

                ListaExcelDet.Add(NewItem)
            Next

            Dim ListaExcelAgrup As New List(Of clsCotizacionBE.clsTicketsPaxPeruRailBE)

            For Each ItemExcel As clsCotizacionBE.clsTicketsPaxPeruRailBE In vListaTickets
                If ListaExcelAgrup.Count = 0 Then
                    Dim NewItem As New clsCotizacionBE.clsTicketsPaxPeruRailBE With {.NuFile = ItemExcel.NuFile, _
                                                                                     .CoReserva = ItemExcel.CoReserva, _
                                                                                     .CoCiudadOri = ItemExcel.CoCiudadOri, _
                                                                                     .CoCiudadDes = ItemExcel.CoCiudadDes, _
                                                                                     .SsValorVentaUSD = ItemExcel.SsValorVentaUSD, _
                                                                                     .SsIgvUSD = ItemExcel.SsIgvUSD, _
                                                                                     .SsTotalUSD = ItemExcel.SsTotalUSD, _
                                                                                     .FeEmision = ItemExcel.FeEmision, _
                                                                                     .NuDocum = ItemExcel.NuDocum}

                    ListaExcelAgrup.Add(NewItem)
                Else

                    'Dim blnFind As Boolean = False
                    Dim dblSSValorVentaUSD As Double = ItemExcel.SsValorVentaUSD
                    Dim dblSsIgvUSD As Double = ItemExcel.SsIgvUSD
                    Dim dblSsTotalUSD As Double = ItemExcel.SsTotalUSD
                    'Dim ItemExcelAgrupBorr As clsCotizacionBE.clsTicketsPaxPeruRailBE = Nothing
                    For Each ItemExcelAgrup As clsCotizacionBE.clsTicketsPaxPeruRailBE In ListaExcelAgrup
                        If ItemExcelAgrup.CoCiudadOri = ItemExcel.CoCiudadOri And _
                            ItemExcelAgrup.CoCiudadDes = ItemExcel.CoCiudadDes And _
                            ItemExcelAgrup.CoReserva = ItemExcel.CoReserva And _
                            ItemExcelAgrup.NuFile = ItemExcel.NuFile Then

                            dblSSValorVentaUSD += ItemExcelAgrup.SsValorVentaUSD
                            dblSsIgvUSD += ItemExcelAgrup.SsIgvUSD
                            dblSsTotalUSD += ItemExcelAgrup.SsTotalUSD

                            ListaExcelAgrup.Remove(ItemExcelAgrup)
                            'blnFind = True
                            Exit For
                        End If
                    Next


                    Dim NewItem As New clsCotizacionBE.clsTicketsPaxPeruRailBE With {.NuFile = ItemExcel.NuFile, _
                                                                                 .CoReserva = ItemExcel.CoReserva, _
                                                                                 .CoCiudadOri = ItemExcel.CoCiudadOri, _
                                                                                 .CoCiudadDes = ItemExcel.CoCiudadDes, _
                                                                                 .SsValorVentaUSD = dblSSValorVentaUSD, _
                                                                                 .SsIgvUSD = dblSsIgvUSD, _
                                                                                 .SsTotalUSD = dblSsTotalUSD, _
                                                                                 .FeEmision = ItemExcel.FeEmision, _
                                                                                 .NuDocum = ItemExcel.NuDocum}


                    ListaExcelAgrup.Add(NewItem)


                End If
            Next


            For Each ItemExcelDet As clsCotizacionBE.clsTicketsPaxPeruRailBE In ListaExcelDet
                GrabarDocumentoPeruRail(ItemExcelDet, vstrUserMod)
            Next
            For Each ItemExcelAgrup As clsCotizacionBE.clsTicketsPaxPeruRailBE In ListaExcelAgrup
                Dim intIDCab As Integer = 0
                If ItemExcelAgrup.NuFile <> "" Then
                    intIDCab = intIDCabxFile(ItemExcelAgrup.NuFile)
                End If
                If intIDCab = 0 Then
                    intIDCab = intIDCabxCodReserva(ItemExcelAgrup.CoReserva, "T")
                End If

                ActualizarCostoTrenPeruRail(intIDCab, ItemExcelAgrup.CoCiudadOri, _
                                            ItemExcelAgrup.CoCiudadDes, ItemExcelAgrup.CoReserva, ItemExcelAgrup.SsTotalUSD, vstrUserMod)
                ActualizarEstadoVoucherTren(intIDCab, "001836", vstrUserMod)
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCostosTrenesPeruRail")
        End Try

    End Sub
    Public Function strDevuelveNuDocInterno_Correlativo(ByVal vdatFeEmision As Date) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@FeEmision", vdatFeEmision)
            dc.Add("@pNuDocumProv", "")
            Return objADO.ExecuteSPOutput("NuDocInterno_SelCorrelativo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub GrabarDocumentoPeruRail(ByVal vItemExcel As clsCotizacionBE.clsTicketsPaxPeruRailBE, ByVal vstrUserMod As String)
        Try



            Dim objBE As New clsDocumentoProveedorBE
            With objBE


                .NuDocInterno = strDevuelveNuDocInterno_Correlativo(vItemExcel.FeEmision.Date.ToString("dd/MM/yyyy"))


                .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                .NuOrden_Servicio = 0
                .CoOrdPag = 0
                .NuOrdComInt = 0
                .NuPreSob = 0
                .NuVouPTrenInt = 0


                'If intNuOrden_Servicio > 0 Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = intNuOrden_Servicio
                'Else
                '    .NuVoucher = vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                'End If
                If vItemExcel.NuFile <> "" Then
                    .IDCab = intIDCabxFile(vItemExcel.NuFile)
                End If
                If .IDCab = 0 Then
                    .IDCab = intIDCabxCodReserva(vItemExcel.CoReserva, "T")
                End If
                .NuDocum = vItemExcel.NuDocum
                .CoTipoDoc = "BOL"
                .FeEmision = vItemExcel.FeEmision.Date.ToString("dd/MM/yyyy")
                .FeRecepcion = Today.Date.ToString("dd/MM/yyyy")
                .CoTipoDetraccion = "00000"
                .CoMoneda = "USD"
                .CoMoneda_Pago = "USD"
                .SsIGV = vItemExcel.SsIgvUSD
                .SsNeto = vItemExcel.SsValorVentaUSD
                .SsOtrCargos = 0
                .SsDetraccion = 0
                .SSTotalOriginal = vItemExcel.SsTotalUSD
                '.NuMaxHorasSemana = IIf(txtNuMaxHorasSemana.Text.Trim.Length > 0, CInt(txtNuMaxHorasSemana.Text.Trim), 0)

                .SSTipoCambio = 0
                .SSTotal = vItemExcel.SsTotalUSD

                .CoTipoOC = "002"
                .CoCtaContab = "903114"
                .CoObligacionPago = "PRL"
                .IngresoManual = False
                .CoMoneda_FEgreso = "USD"
                .SsTipoCambio_FEgreso = 0
                .SsTotal_FEgreso = vItemExcel.SsTotalUSD
                .IDOperacion = 0

                .CoProveedor = "001836"
                .CoUbigeo_Oficina = "000065"
                .FeVencimiento = "01/01/1900"
                .FeEmision_Ref = "01/01/1900"
                .UserMod = vstrUserMod
                .UserNuevo = vstrUserMod
            End With


            Dim objBT As New clsDocumentoProveedorBT
            objBT.Grabar(objBE)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".GrabarDocumentoPeruRail")

        End Try

    End Sub

    Public Function strGrabarDocumentoPeruRail(ByVal vItemExcel As clsCotizacionBE.clsTicketsPaxPeruRailBE, ByVal vstrUserMod As String) As String
        Try

            Dim objBE As New clsDocumentoProveedorBE
            With objBE

                .NuDocInterno = strDevuelveNuDocInterno_Correlativo(vItemExcel.FeEmision.Date.ToString("dd/MM/yyyy"))
                .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                .NuOrden_Servicio = 0
                .CoOrdPag = 0
                .NuOrdComInt = 0
                .NuPreSob = 0
                .NuVouPTrenInt = 0

                If vItemExcel.NuFile <> "" Then
                    .IDCab_FF = intIDCabxFile(vItemExcel.NuFile)
                End If
                If .IDCab_FF = 0 Then
                    .IDCab_FF = intIDCabxCodReserva(vItemExcel.CoReserva, "T")
                End If
                .IDCab = .IDCab_FF
                .NuDocum = vItemExcel.NuDocum
                .CoTipoDoc = "TDT" '"BOL"
                .FeEmision = vItemExcel.FeEmision.Date.ToString("dd/MM/yyyy")
                .FeRecepcion = Today.Date.ToString("dd/MM/yyyy")
                .CoTipoDetraccion = "00000"
                .CoMoneda = "USD"
                .CoMoneda_Pago = "USD"
                .SsIGV = vItemExcel.SsIgvUSD
                .SsNeto = vItemExcel.SsValorVentaUSD
                .SsOtrCargos = 0
                .SsDetraccion = 0
                .SSTotalOriginal = vItemExcel.SsTotalUSD

                .SSTipoCambio = 0
                .SSTotal = vItemExcel.SsTotalUSD

                .CoTipoOC = "002"
                .CoCtaContab = "63111004"
                .CoObligacionPago = "PRL"
                .IngresoManual = False
                .CoMoneda_FEgreso = "USD"
                .SsTipoCambio_FEgreso = 0
                .SsTotal_FEgreso = vItemExcel.SsTotalUSD
                .IDOperacion = 0

                .CoProveedor = "001836"
                .CoUbigeo_Oficina = "000065"
                .FeVencimiento = "01/01/1900"
                .FeEmision_Ref = "01/01/1900"
                .CoTipoDocSAP = 1
                '.NuSerie = "000"
                .NuSerie = "2"
                .CoCecos = "900101"
                '.CoCeCon = "999999"
                .CoCeCon = "000000"
                .UserMod = vstrUserMod
                .UserNuevo = vstrUserMod
                .TxConcepto = "PASAJE DE TREN"
            End With


            Dim objBT As New clsDocumentoProveedorBT
            objBT.Grabar(objBE, False) 'quitar SAP


            Dim intIDCab As Integer = 0
            If vItemExcel.NuFile <> "" Then
                intIDCab = intIDCabxFile(vItemExcel.NuFile)
            End If
            If intIDCab = 0 Then
                intIDCab = intIDCabxCodReserva(vItemExcel.CoReserva, "T")
            End If

            ActualizarIncrementaCostoTrenPeruRail(intIDCab, vItemExcel.CoCiudadOri, _
                                        vItemExcel.CoCiudadDes, vItemExcel.CoReserva, vItemExcel.SsTotalUSD, vstrUserMod)


            'SAP

            Dim arrDeleteProperties As String() = Nothing

            Dim oBEPurchaseInvoice As clsPurchaseInvoicesBE = Nothing
            objBT.PasarDatosDocumentosToSocioNegocio(objBE.NuDocumProv, vstrUserMod, oBEPurchaseInvoice, arrDeleteProperties)


            Dim oBTJsonMaster As New clsJsonInterfaceBT()
            'oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
            '                                     "http://sap:8899/api/PurchaseInvoices/", _
            '                                     "POST", arrDeleteProperties)

            oBTJsonMaster.EnviarDatosRESTFulJson(oBEPurchaseInvoice, _
                                                "http://sap:" + gstrPuertoSAP + "/api/PurchaseInvoices/", _
                                                "POST", arrDeleteProperties)

            objBT.Actualizar_CardCodeSAP(objBE.NuDocumProv, objResponseSAP.Id)

            ContextUtil.SetComplete()

            Return ""
        Catch ex As Exception
            ContextUtil.SetAbort()
            Return ex.Message

        End Try

    End Function

    Private Function intIDCabxCodReserva(ByVal vstrCodReserva As String, ByVal vchrTipoTransporte As Char) As Integer
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CodReserva", vstrCodReserva)
            dc.Add("@TipoTransporte", vchrTipoTransporte)
            dc.Add("@pIDCab", 0)

            Return objADO.GetSPOutputValue("COTIVUELOS_SelIDCabxCodReservaOutput", dc)

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".intIDCabxCodReserva")
        End Try
    End Function
    Private Function intIDCabxFile(ByVal vstrNuFile As String) As Integer
        Try
            Dim intIDCab As Integer = 0
            Using objBN As New clsCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarFilexIDCab(vstrNuFile)
                    If dr.HasRows Then
                        dr.Read()
                        intIDCab = dr("IDCab")
                    End If
                    dr.Close()
                End Using
            End Using
            Return intIDCab

        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub ActualizarCostoTrenPeruRail(ByVal vintIDCab As Integer, ByVal vstrCoRutaOri As String, _
                                            ByVal vstrCoRutaDes As String, ByVal vstrCoReserva As String, _
                                            ByVal vdblCosto As Double, _
                                            ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            'dc.Add("@NuFile", vstrNuFile)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoRutaOri", vstrCoRutaOri)
            dc.Add("@CoRutaDes", vstrCoRutaDes)
            dc.Add("@CoReserva", vstrCoReserva)
            dc.Add("@CostoOperador", vdblCosto)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIVUELOS_UpdCosto", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarCostoTrenPeruRail")

        End Try
    End Sub
    Public Sub ActualizarIncrementaCostoTrenPeruRail(ByVal vintIDCab As Integer, ByVal vstrCoRutaOri As String, _
                                        ByVal vstrCoRutaDes As String, ByVal vstrCoReserva As String, _
                                        ByVal vdblCosto As Double, _
                                        ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            'dc.Add("@NuFile", vstrNuFile)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoRutaOri", vstrCoRutaOri)
            dc.Add("@CoRutaDes", vstrCoRutaDes)
            dc.Add("@CoReserva", vstrCoReserva)
            dc.Add("@CostoOperador", vdblCosto)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTIVUELOS_UpdIncrementaCosto", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarIncrementaCostoTrenPeruRail")

        End Try
    End Sub
    Public Sub ActualizarEstadoVoucherTren(ByVal vintIDCab As Integer, ByVal vstrCoProveedorTren As String, ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            'dc.Add("@NuFile", vstrNuFile)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoProveedor", vstrCoProveedorTren)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("VOUCHER_PROV_TREN_UpdEstadoAC", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoVoucherTren")

        End Try
    End Sub

    'Public Sub ActualizarEstadoVoucherTren2(ByVal vintIDCab As Integer, ByVal vstrCoProveedorTren As String, ByVal vstrCoEstado As String, ByVal vstrUserMod As String)
    '    Try

    '        Dim dc As New Dictionary(Of String, String)
    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@CoProveedor", vstrCoProveedorTren)
    '        dc.Add("@CoEstado", vstrCoEstado)
    '        dc.Add("@UserMod", vstrUserMod)

    '        objADO.ExecuteSP("VOUCHER_PROV_TREN_UpdEstado", dc)
    '        ContextUtil.SetComplete()
    '    Catch ex As Exception
    '        ContextUtil.SetAbort()
    '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstadoVoucherTren")

    '    End Try
    'End Sub

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsPaxTransporteBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsPaxTransporteBT"

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaPaxTransporte Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsPaxTransporteBE In BE.ListaPaxTransporte
                If Item.Accion = "N" Then
                    If IsNumeric(Item.IDPaxTransp) Then
                        If Not blnExiste(Item.IDTransporte, Item.IDPaxTransp) Then
                            Insertar(Item)
                        Else
                            Actualizar(Item)
                        End If
                    Else
                        Insertar(Item)
                    End If
                ElseIf Item.Accion = "M" Then
                    If IsNumeric(Item.IDPaxTransp) Then
                        Actualizar(Item)
                    End If
                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.IDPaxTransp) Then
                        Eliminar(Item.IDTransporte, Item.IDPaxTransp)
                    End If
                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsPaxTransporteBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDTransporte", BE.IDTransporte)
            dc.Add("@IDPax", BE.IDPax)
            dc.Add("@CodReservaTransp", BE.CodReservaTransp)
            dc.Add("@IDProveedorGuia", BE.IDProveedorGuia)
            dc.Add("@SsReembolso", BE.Reembolso)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTITRANSP_PAX_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub
    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsPaxTransporteBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDTransporte", BE.IDTransporte)
            dc.Add("@IDPaxTransp", BE.IDPaxTransp)
            dc.Add("@CodReservaTransp", BE.CodReservaTransp)
            dc.Add("@IDProveedorGuia", BE.IDProveedorGuia)
            dc.Add("@SsReembolso", BE.Reembolso)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTITRANSP_PAX_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal vintIDTransporte As Integer, ByVal vintIDPaxTransp As Int16)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDTransporte", vintIDTransporte)
            dc.Add("@IDPaxTransp", vintIDPaxTransp)

            objADO.ExecuteSP("COTITRANSP_PAX_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    Public Function blnExiste(ByVal vintIDTransporte As Integer, ByVal vintIDPaxTransp As Int16) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDTransporte", vintIDTransporte)
            dc.Add("@IDPaxTransp", vintIDPaxTransp)
            dc.Add("@pExiste", 0)

            Return objADO.ExecuteSPOutput("COTITRANSP_PAX_Sel_ExisteOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function




End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsDetalleVentasCotizacionAcomodoEspecialBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsDetalleVentasCotizacionAcomodoEspecialBT"

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionAcomodoEspecialBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@CoCapacidad", BE.CoCapacidad)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionAcomodoEspecialBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@CoCapacidad", BE.CoCapacidad)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Private Overloads Sub Eliminar(ByVal vintIDDet As Integer, ByVal vstrCoCapacidad As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)
            dc.Add("@CoCapacidad", vstrCoCapacidad)

            objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_DISTRIBUC_Del_IDDetCoCapacidad", dc)
            objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_Del", dc)
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Overloads Sub EliminarxIDDet(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)
            objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_DelxIddet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Function blnExiste(ByVal vintIDDet As Integer, ByVal vstrCoCapacidad As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@CoCapacidad", vstrCoCapacidad)
            dc.Add("@pExiste", 0)

            Return objADO.ExecuteSPOutput("COTIDET_ACOMODOESPECIAL_Sel_ExisteOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaDetalleCotizacionAcomodoEspecial Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionAcomodoEspecialBE In BE.ListaDetalleCotizacionAcomodoEspecial
                If Item.Accion = "N" Then
                    If Not blnExiste(Item.IDDet, Item.CoCapacidad) Then
                        Insertar(Item)
                    Else
                        Actualizar(Item)
                    End If
                ElseIf Item.Accion = "M" Then
                    Actualizar(Item)
                ElseIf Item.Accion = "B" Then
                    If IsNumeric(Item.IDDet) Then
                        Eliminar(Item.IDDet, Item.CoCapacidad)
                    End If
                End If
            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Class clsDistribucAcomodoEspecialBT
        Inherits clsVentasCotizacionBaseBT
        Dim strNombreClase As String = "clsDistribucAcomodoEspecialBT"

        Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE)
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDDet", BE.IDDet)
                dc.Add("@CoCapacidad", BE.CoCapacidad)
                dc.Add("@IDPax", BE.IDPax)
                dc.Add("@IDHabit", BE.QtPax)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_DISTRIBUC_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
            End Try
        End Sub
        Private Overloads Sub Eliminar(ByVal BE As clsCotizacionBE.clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE)
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDDet", BE.IDDet)
                dc.Add("@CoCapacidad", BE.CoCapacidad)
                dc.Add("@IDPax", BE.IDPax)

                objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_DISTRIBUC_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
            End Try
        End Sub

        Public Overloads Sub EliminarxIDDet(ByVal vintIDDet As Integer)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDDet", vintIDDet)

                objADO.ExecuteSP("COTIDET_ACOMODOESPECIAL_DISTRIBUC_DelxIDDet", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
            End Try
        End Sub

        Public Overloads Sub InsertarEliminar(ByVal BE As clsCotizacionBE)
            If BE.ListaDistribucAcomodoEspecial Is Nothing Then Exit Sub
            Try
                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE In BE.ListaDistribucAcomodoEspecial
                    If Item.Accion = "B" Then
                        Eliminar(Item)
                    End If
                Next

                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE In BE.ListaDistribucAcomodoEspecial
                    If Item.Accion = "N" Then
                        Insertar(Item)
                    End If
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        'Public Function ConsultarxIDReserva(ByVal vintIDReserva As Integer) As DataTable
        '    Try
        '        Dim dc As New Dictionary(Of String, String)
        '        dc.Add("@IDReserva", vintIDReserva)

        '        Return objADO.GetDataTable("RESERVAS_DET_SelxIDReservaAcomodoEspecial", dc)

        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function

    End Class
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsAcomodoVehiculoBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsAcomodoVehiculoBT"

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsAcomodoVehiculoBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuVehiculo", BE.NuVehiculo)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@QtVehiculos", BE.QtVehiculos)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@FlGuia", BE.FlGuia)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("ACOMODO_VEHICULO_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsAcomodoVehiculoBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuVehiculo", BE.NuVehiculo)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@QtVehiculos", BE.QtVehiculos)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@FlGuia", BE.FlGuia)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("ACOMODO_VEHICULO_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal vintNuVehiculo As Int16, ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuVehiculo", vintNuVehiculo)
            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("ACOMODO_VEHICULO_DET_PAX_DelxFk", dc)
            objADO.ExecuteSP("ACOMODO_VEHICULO_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    Private Function blnExiste(ByVal vintNuVehiculo As Int16, ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVehiculo", vintNuVehiculo)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pExists", 0)

            Return objADO.ExecuteSPOutput("ACOMODO_VEHICULO_SelExistsOutput", dc)

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExiste")
        End Try
    End Function
    'Private Function ConsultarDetVtasAcomVehiCopiaPrInterno(ByVal vintIDCab As Integer, ByVal vstrCoUbigeo As String, _
    '                                                        ByVal vstrCoVariante As String) As DataTable
    '    Try
    '        Dim dc As New Dictionary(Of String, String)

    '        dc.Add("@IDCab", vintIDCab)
    '        dc.Add("@CoUbigeo", vstrCoUbigeo)
    '        dc.Add("@CoVariante", vstrCoVariante)

    '        Return objADO.GetDataTable("COTIDET_SelAcomoVehicCopia", dc)
    '    Catch ex As Exception
    '        Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ConsultarDetVtasAcomVehiCopiaPrInterno")
    '    End Try
    'End Function
    Public Sub InsertarCopias(ByVal BE As clsCotizacionBE)
        If BE.ListaAcomodoVehiculo Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsAcomodoVehiculoBE In BE.ListaAcomodoVehiculo
                If Item.FlCopiar Then

                    'Dim dtt As DataTable = ConsultarDetVtasAcomVehiCopiaPrInterno(BE.IdCab, Item.CoUbigeo, Item.CoVariante)
                    'For Each dr As DataRow In dtt.Rows
                    For Each ItemIDDetsCopiar As clsCotizacionBE.clsAcomodoVehiculoCopiaBE In BE.ListaIDDetCopiarAcomVehi
                        If ItemIDDetsCopiar.IDDetBase = Item.IDDet Then
                            'Dim intIDDet As Integer = dr("IDDet")
                            Dim intIDDet As Integer = ItemIDDetsCopiar.IDDet
                            Dim NuevaLista As List(Of clsCotizacionBE.clsAcomodoVehiculoBE) = _
                                lstListaAcomodoVehiculo(BE.ListaAcomodoVehiculo, Item.IDDet, intIDDet)
                            Dim NuevoBE As New clsCotizacionBE With {.ListaAcomodoVehiculo = NuevaLista}
                            InsertarActualizarEliminar(NuevoBE)

                            Dim NuevaListaDetPax As List(Of clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE) = _
                                lstListaAcomodoVehiculoDetPax(BE.ListaAcomodoVehiculoIntDetPax, Item.IDDet, intIDDet)
                            Dim NuevoBEDetPax As New clsCotizacionBE With {.ListaAcomodoVehiculoIntDetPax = NuevaListaDetPax}
                            Dim objDetPaxBT As New clsAcomodoVehiculoDetPaxIntBT
                            objDetPaxBT.InsertarActualizarEliminar(NuevoBEDetPax)
                        End If
                    Next


                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Function lstListaAcomodoVehiculo(ByVal vListaAcomodoVehiculo As List(Of clsCotizacionBE.clsAcomodoVehiculoBE), _
                                             ByVal vintIDDetBase As Integer, ByVal vintIDDetNue As Integer) As List(Of clsCotizacionBE.clsAcomodoVehiculoBE)
        Try
            Dim ListaRet As New List(Of clsCotizacionBE.clsAcomodoVehiculoBE)
            For Each Item As clsCotizacionBE.clsAcomodoVehiculoBE In vListaAcomodoVehiculo
                If Item.IDDet = vintIDDetBase Then
                    Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoBE With {.IDDet = vintIDDetNue, _
                                                                                  .NuVehiculo = Item.NuVehiculo, _
                                                                                  .QtVehiculos = Item.QtVehiculos, _
                                                                                  .QtPax = Item.QtPax, _
                                                                                  .FlGuia = Item.FlGuia, _
                                                                                  .UserMod = Item.UserMod}
                    ListaRet.Add(NewItem)
                End If
            Next
            Return ListaRet
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function lstListaAcomodoVehiculoDetPax(ByVal vListaAcomodoVehiculoDetPax As List(Of clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE), _
                                             ByVal vintIDDetBase As Integer, ByVal vintIDDetNue As Integer) As List(Of clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE)
        Try
            Dim ListaRet As New List(Of clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE)
            For Each Item As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In vListaAcomodoVehiculoDetPax
                If Item.IDDet = vintIDDetBase Then
                    Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE With {.IDDet = vintIDDetNue, _
                                                                                  .NuVehiculo = Item.NuVehiculo, _
                                                                                           .NuNroVehiculo = Item.NuNroVehiculo, _
                                                                                  .NuPax = Item.NuPax, _
                                                                                  .Selecc = Item.Selecc, _
                                                                                           .FlGuia = Item.FlGuia, _
                                                                                  .UserMod = Item.UserMod}
                    ListaRet.Add(NewItem)
                End If
            Next
            Return ListaRet
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaAcomodoVehiculo Is Nothing Then Exit Sub
        Try

            For Each Item As clsCotizacionBE.clsAcomodoVehiculoBE In BE.ListaAcomodoVehiculo

                If Item.QtPax = 0 Then
                    Eliminar(Item.NuVehiculo, Item.IDDet)
InicioPax:
                    For Each ItemPax As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In BE.ListaAcomodoVehiculoIntDetPax
                        If ItemPax.NuVehiculo = Item.NuVehiculo And ItemPax.IDDet = Item.IDDet Then
                            BE.ListaAcomodoVehiculoIntDetPax.Remove(ItemPax)
                            GoTo InicioPax
                        End If
                    Next

                Else
                    If blnExiste(Item.NuVehiculo, Item.IDDet) Then
                        Actualizar(Item)
                    Else
                        Insertar(Item)
                    End If
                End If

            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Overloads Sub EliminarxIDDet(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)
            objADO.ExecuteSP("ACOMODO_VEHICULO_DelxIddet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub InsertarEliminarSubServiciosCotizacion(ByVal BE As clsCotizacionBE)
        Try

            Dim ListaProvs As New List(Of String)
            Dim ListaCotiDet As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            For Each itemAcomodVeh As clsCotizacionBE.clsAcomodoVehiculoBE In BE.ListaAcomodoVehiculo
                'If itemAcomodVeh.QtPax > 0 Then
                For Each ItemDet As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                    If ItemDet.IDDet = itemAcomodVeh.IDDet Then
                        Dim blnYaGrabado As Boolean = False
                        For Each ItemDetBusca As clsCotizacionBE.clsDetalleCotizacionBE In ListaCotiDet
                            If ItemDetBusca.IDDet = ItemDet.IDDet Then
                                blnYaGrabado = True
                                Exit For
                            End If
                        Next
                        If Not blnYaGrabado Then
                            'ItemDet.Recalcular = True
                            ListaCotiDet.Add(ItemDet)
                            If Not ListaProvs.Contains(ItemDet.IDProveedor) Then
                                ListaProvs.Add(ItemDet.IDProveedor)
                            End If
                        End If


                    End If
                Next
                'End If
            Next

            Dim strIDTipoHonorario As String = String.Empty
            If BE.FlTourConductor Then
                If Not BE.ListaTourConductor Is Nothing Then
                    strIDTipoHonorario = BE.ListaTourConductor.FirstOrDefault().CoTipoHon
                End If
            End If

            If ListaProvs.Count > 0 Then

                Dim objDetServCot As New clsDetalleVentasCotizacionDetServiciosBT
                For Each ItemDetBusca As clsCotizacionBE.clsDetalleCotizacionBE In ListaCotiDet
                    objDetServCot.EliminarxIDDet(ItemDetBusca.IDDet)
                Next
                Dim objBTDet As New clsDetalleVentasCotizacionBT
                Dim strTipoPax As String = objBTDet.strDevuelveTipoPaxNacionalidad(BE)
                Dim objBT As New clsVentasCotizacionBT
                objBT.ReCalculoCotizacion(BE.IdCab, BE.NroPax, BE.NroLiberados, BE.Tipo_Lib, _
                                    strTipoPax, BE.Redondeo, BE.IGV, "M", BE.UserMod, _
                                    BE.ListaDetPax, BE.MargenCero, strIDTipoHonorario, ListaCotiDet, False)

                'Dim objProv As New clsProveedorBN
                For Each strCoProveedor As String In ListaProvs
                    'If objProv.blnProveedorInterno(strCoProveedor) Then
                    InsertarEliminarSubServiciosProveedor(BE.IdCab, strCoProveedor, BE.UserMod)
                    'End If
                Next

                objBT.ReCalculoCotizacion(BE.IdCab, BE.NroPax, BE.NroLiberados, BE.Tipo_Lib, _
                                    strTipoPax, BE.Redondeo, BE.IGV, "M", BE.UserMod, _
                                    BE.ListaDetPax, BE.MargenCero, strIDTipoHonorario, ListaCotiDet, True)

            End If
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try

    End Sub

    Private Sub InsertarEliminarSubServiciosProveedor(ByVal vintIDCab As Integer, ByVal vstrCoProveedor As String, ByVal vstrUserMod As String)
        Try

            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@CoProveedor", vstrCoProveedor)
            dc.Add("@UserMod", vstrUserMod)
            objADO.ExecuteSP("COTIDET_DETSERVICIOS_InsDel_CostosGuiasVehiculos", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsAcomodoVehiculo_Ext_BT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsAcomodoVehiculo_Ext_BT"

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@QtGrupo", BE.QtGrupo)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("ACOMODO_VEHICULO_EXT_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@QtPax", BE.QtPax)
            dc.Add("@QtGrupo", BE.QtGrupo)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("ACOMODO_VEHICULO_EXT_Upd", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub
    Private Overloads Sub Eliminar(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDDet", vintIDDet)

            objADO.ExecuteSP("ACOMODO_VEHICULO_EXT_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub
    Private Function blnExiste(ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@pExists", 0)

            Return objADO.ExecuteSPOutput("ACOMODO_VEHICULO_EXT_SelExistsOutput", dc)

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExiste")
        End Try
    End Function

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaAcomodoVehiculoExt Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE In BE.ListaAcomodoVehiculoExt

                If Item.QtPax = 0 Then
                    Eliminar(Item.IDDet)
                Else
                    If blnExiste(Item.IDDet) Then
                        Actualizar(Item)
                    Else
                        Insertar(Item)
                    End If
                End If

            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub InsertarCopias(ByVal BE As clsCotizacionBE)
        If BE.ListaAcomodoVehiculoExt Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE In BE.ListaAcomodoVehiculoExt
                If Item.FlCopiar Then

                    'Dim dtt As DataTable = ConsultarDetVtasAcomVehiCopiaPrInterno(BE.IdCab, Item.CoUbigeo, Item.CoVariante)
                    'For Each dr As DataRow In dtt.Rows
                    For Each ItemIDDetsCopiar As clsCotizacionBE.clsAcomodoVehiculoCopiaBE In BE.ListaIDDetCopiarAcomVehi
                        If ItemIDDetsCopiar.IDDetBase = Item.IDDet Then
                            'Dim intIDDet As Integer = dr("IDDet")
                            Dim intIDDet As Integer = ItemIDDetsCopiar.IDDet
                            Dim NuevaLista As List(Of clsCotizacionBE.clsAcomodoVehiculo_Ext_BE) = _
                                lstListaAcomodoVehiculo(BE.ListaAcomodoVehiculoExt, Item.IDDet, intIDDet)
                            Dim NuevoBE As New clsCotizacionBE With {.ListaAcomodoVehiculoExt = NuevaLista}
                            InsertarActualizarEliminar(NuevoBE)


                            Dim NuevaListaDetPax As List(Of clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE) = _
                               lstListaAcomodoVehiculoDetPax(BE.ListaAcomodoVehiculoExtDetPax, Item.IDDet, intIDDet)
                            Dim NuevoBEDetPax As New clsCotizacionBE With {.ListaAcomodoVehiculoExtDetPax = NuevaListaDetPax}
                            Dim objDetPaxBT As New clsAcomodoVehiculoDetPaxExtBT
                            objDetPaxBT.InsertarActualizarEliminar(NuevoBEDetPax)
                        End If
                    Next

                End If
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Function lstListaAcomodoVehiculo(ByVal vListaAcomodoVehiculo As List(Of clsCotizacionBE.clsAcomodoVehiculo_Ext_BE), _
                                         ByVal vintIDDetBase As Integer, ByVal vintIDDetNue As Integer) As List(Of clsCotizacionBE.clsAcomodoVehiculo_Ext_BE)
        Try
            Dim ListaRet As New List(Of clsCotizacionBE.clsAcomodoVehiculo_Ext_BE)
            For Each Item As clsCotizacionBE.clsAcomodoVehiculo_Ext_BE In vListaAcomodoVehiculo
                If Item.IDDet = vintIDDetBase Then
                    Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculo_Ext_BE With {.IDDet = vintIDDetNue, _
                                                                                  .QtPax = Item.QtPax, _
                                                                                  .QtGrupo = Item.QtGrupo, _
                                                                                  .UserMod = Item.UserMod}
                    ListaRet.Add(NewItem)
                End If


            Next
            Return ListaRet
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function lstListaAcomodoVehiculoDetPax(ByVal vListaAcomodoVehiculoDetPax As List(Of clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE), _
                                            ByVal vintIDDetBase As Integer, ByVal vintIDDetNue As Integer) As List(Of clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE)
        Try
            Dim ListaRet As New List(Of clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE)
            For Each Item As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In vListaAcomodoVehiculoDetPax
                If Item.IDDet = vintIDDetBase Then
                    Dim NewItem As New clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE With {.IDDet = vintIDDetNue, _
                                                                                           .NuGrupo = Item.NuGrupo, _
                                                                                  .NuPax = Item.NuPax, _
                                                                                  .Selecc = Item.Selecc, _
                                                                                  .UserMod = Item.UserMod}
                    ListaRet.Add(NewItem)
                End If
            Next
            Return ListaRet
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Overloads Sub EliminarxIDDet(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)
            objADO.ExecuteSP("ACOMODO_VEHICULO_EXT_DelxIddet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsAcomodoVehiculoDetPaxIntBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsAcomodoVehiculoDetPaxIntBT"

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaAcomodoVehiculoIntDetPax Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE In BE.ListaAcomodoVehiculoIntDetPax
                If Not Item.Selecc Then
                    Eliminar(Item.NuVehiculo, Item.NuNroVehiculo, Item.IDDet, Item.NuPax)
                Else
                    If Not blnExiste(Item.NuVehiculo, Item.NuNroVehiculo, Item.IDDet, Item.NuPax) Then
                        Insertar(Item)
                    End If
                End If

            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsAcomodoVehiculoIntDetPaxBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuVehiculo", BE.NuVehiculo)
            dc.Add("@NuNroVehiculo", BE.NuNroVehiculo)
            dc.Add("@NuPax", BE.NuPax)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@FlGuia", BE.FlGuia)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("ACOMODO_VEHICULO_DET_PAX_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Function blnExiste(ByVal vintNuVehiculo As Int16, ByVal vbytNuNroVehiculo As Byte, ByVal vintIDDet As Integer, ByVal vintNuPax As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuVehiculo", vintNuVehiculo)
            dc.Add("@NuNroVehiculo", vbytNuNroVehiculo)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@NuPax", vintNuPax)
            dc.Add("@pExists", 0)

            Return objADO.ExecuteSPOutput("ACOMODO_VEHICULO_DET_PAX_SelExistsOutput", dc)

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExiste")
        End Try
    End Function
    Private Overloads Sub Eliminar(ByVal vintNuVehiculo As Int16, ByVal vbytNuNroVehiculo As Byte, ByVal vintIDDet As Integer, ByVal vintNuPax As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuVehiculo", vintNuVehiculo)
            dc.Add("@NuNroVehiculo", vbytNuNroVehiculo)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@NuPax", vintNuPax)

            objADO.ExecuteSP("ACOMODO_VEHICULO_DET_PAX_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub

    Public Overloads Sub EliminarxIDDet(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)
            objADO.ExecuteSP("ACOMODO_VEHICULO_DET_PAX_DelxIDDet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsAcomodoVehiculoDetPaxExtBT
    Inherits clsVentasCotizacionBaseBT
    Dim strNombreClase As String = "clsAcomodoVehiculoDetPaxExtBT"

    Public Overloads Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
        If BE.ListaAcomodoVehiculoExtDetPax Is Nothing Then Exit Sub
        Try
            For Each Item As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE In BE.ListaAcomodoVehiculoExtDetPax
                If Not Item.Selecc Then
                    Eliminar(Item.NuGrupo, Item.IDDet, Item.NuPax)
                Else
                    If Not blnExiste(Item.NuGrupo, Item.IDDet, Item.NuPax) Then
                        Insertar(Item)
                    End If
                End If

            Next
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Private Overloads Sub Insertar(ByVal BE As clsCotizacionBE.clsAcomodoVehiculoExtDetPaxBE)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuGrupo", BE.NuGrupo)
            dc.Add("@NuPax", BE.NuPax)
            dc.Add("@IDDet", BE.IDDet)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("ACOMODO_VEHICULO_EXT_DET_PAX_Ins", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Sub

    Private Function blnExiste(ByVal vbytNuGrupo As Byte, ByVal vintIDDet As Integer, ByVal vintNuPax As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@NuGrupo", vbytNuGrupo)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@NuPax", vintNuPax)
            dc.Add("@pExists", 0)

            Return objADO.ExecuteSPOutput("ACOMODO_VEHICULO_EXT_DET_PAX_SelExistsOutput", dc)

        Catch ex As Exception
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".blnExiste")
        End Try
    End Function
    Private Overloads Sub Eliminar(ByVal vbytNuGrupo As Byte, ByVal vintIDDet As Integer, ByVal vintNuPax As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuGrupo", vbytNuGrupo)
            dc.Add("@IDDet", vintIDDet)
            dc.Add("@NuPax", vintNuPax)

            objADO.ExecuteSP("ACOMODO_VEHICULO_EXT_DET_PAX_Del", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Eliminar")
        End Try
    End Sub


    Public Overloads Sub EliminarxIDDet(ByVal vintIDDet As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDet", vintIDDet)
            objADO.ExecuteSP("ACOMODO_VEHICULO_EXT_DET_PAX_DelxIDDet", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsPedidoBT
    Inherits clsVentasCotizacionBaseBT
    Private strNombreClase As String = "clsPedidoBT"

    Public Overloads Function Insertar(ByVal BE As clsCotizacionBE.clsPedidoBE) As String
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@CoCliente", BE.CoCliente)
            dc.Add("@FePedido", BE.FePedido)
            dc.Add("@CoEjeVta", BE.CoEjeVta)
            dc.Add("@TxTitulo", BE.TxTitulo)
            dc.Add("@CoEstado", BE.CoEstado)
            dc.Add("@UserMod", BE.UserMod)
            dc.Add("@pNuPedido", "")

            Dim strRet As String = objADO.ExecuteSPOutput("PEDIDO_Ins", dc)

            ContextUtil.SetComplete()

            Return strRet
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
        End Try
    End Function

    Public Overloads Sub Actualizar(ByVal BE As clsCotizacionBE.clsPedidoBE, Optional ByVal BECot As clsCotizacionBE = Nothing)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuPedInt", BE.NuPedInt)
            dc.Add("@CoCliente", BE.CoCliente)
            dc.Add("@FePedido", BE.FePedido)
            dc.Add("@CoEjeVta", BE.CoEjeVta)
            dc.Add("@TxTitulo", BE.TxTitulo)
            dc.Add("@CoEstado", BE.CoEstado)
            dc.Add("@UserMod", BE.UserMod)

            objADO.ExecuteSP("PEDIDO_Upd", dc)

            If Not BECot Is Nothing Then
                Dim objArchivos As New clsCotizacionBT.clsCotizacionArchivosBT
                objArchivos.InsertarActualizarEliminar(BECot)
            End If

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
        End Try
    End Sub

    Public Overloads Sub ActualizarEstado(ByVal vintNuPedInt As Integer, ByVal vstrCoEstado As String, ByVal vstrUserMod As String)
        Try
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@NuPedInt", vintNuPedInt)
            dc.Add("@CoEstado", vstrCoEstado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("PEDIDO_UpdEstado", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".ActualizarEstado")
        End Try
    End Sub
End Class