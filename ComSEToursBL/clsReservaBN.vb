﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsReservaBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN

    'Public Function ConsultarList(ByVal vstrProveedor As String, ByVal vstrCotizacion As String, ByVal vstrIDFile As String, _
    '                              ByVal vstrEstado As String, ByVal vstrIDUsuarioVen As String, ByVal vstrIDUsuarioRes As String, _
    '                              ByVal vdatFechaOut As Date) As DataTable
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        dc.Add("@Proveedor", vstrProveedor)
    '        dc.Add("@Cotizacion", vstrCotizacion)
    '        dc.Add("@IDFile", vstrIDFile)
    '        dc.Add("@Estado", vstrEstado)
    '        dc.Add("@IDUsuarioVen", vstrIDUsuarioVen)
    '        dc.Add("@IDUsuarioRes", vstrIDUsuarioRes)
    '        dc.Add("@FechaOut", vdatFechaOut)

    '        Return objADO.GetDataTable(True, "RESERVAS_Sel_List", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Public Function ConsultarPk(ByVal vintIDReserva As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDReservas", vintIDReserva)

        Dim strSql As String = "RESERVAS_Sel_Pk"
        Return objADO.GetDataReader(strSql, dc)
    End Function

    Public Function ConsultarListxIDCab_HotelyEspc(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_Sel_ListxIDCabHotelyEspc", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListGrabacionCorreos() As Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            Return objADO.GetDataTable(True, "RESERVAS_Sel_PGrabarCorreos", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_FechasRetorno(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCab", vintIDCab)

        Dim strSql As String = "COTICAB_Get_FechaRetornoReservas"
        Return objADO.GetDataReader(strSql, dc)
    End Function

    Public Function ConsultarReporteWordHoteles(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDTipoProv", "001")

            Return objADO.GetDataTable(True, "RESERVAS_DET_SelxIDCab_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteWordServicios(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_SelxIDCab_RptServicios", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteWordRestaurantes(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_DET_SelxIDCab_RptRestaurantes", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarSubReportexIDCab(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_DET_SelxIDCab_SubRpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListxIDCab_NoHoteles(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_Sel_ListxIDCabNoHoteles", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function strDevuelveIDFile(ByVal vintIDCab As Int32, ByVal vdatFechaInicio As Date) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@FecInicioCoti", vdatFechaInicio)
            dc.Add("@pIDFile", "")
            Return objADO.ExecuteSPOutput("NroFile_SelOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarListEstadosxIDCab(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_Sel_ListEstadosxIDCab", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnTodossonOKRRxIDCab(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("RESERVAS_Sel_SiTodossonOKRRxIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteCambioPaxNivelDetalle(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pExisteCambio", False)

            Return objADO.GetSPOutputValue("RESERVAS_ExisteDifDetallePax", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenReservasxIDCab(ByVal vintIDCab As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("RESERVAS_Sel_ExistenxIDCabOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExistenCorreoEnviadoxReserva(ByVal vintIDReserva As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@pbOk", 0)

            Return objADO.ExecuteSPOutput("RESERVAS_Sel_ExisteCorreoEnviado", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteWord(ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataReader("RESERVAS_Sel_RptCabecera", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteWordTren(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "COTIVUELOS_SelTrenxIDCab", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteWordObservaciones(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_Sel_RptObservaciones", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarReporteWordObservacionesStatus(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "COTI_INCLUIDO_SelObsStatus", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarOperadoresXRangoDiaDetalle(ByVal vintIdCab As Integer, ByVal vdatFecha1 As Date, _
                                                        ByVal vdatFecha2 As Date) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIdCab)
            dc.Add("@Fecha1", vdatFecha1.ToShortDateString)
            dc.Add("@Fecha2", vdatFecha2.ToShortDateString)

            Return objADO.GetDataTable(True, "RESERVAS_SelOperxRangoFechaDet", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function objBECotizacion(ByVal vintIDCab As Integer, _
                                    ByVal vstrIDProveedor As String, _
                                    ByVal vstrUser As String) As clsCotizacionBE
        Try
            Dim objBECot As New clsCotizacionBE
            Dim ListaDetCot As New List(Of clsCotizacionBE.clsDetalleCotizacionBE) ''''

            Dim objBNCot As New clsCotizacionBN
            Dim dr As Object = objBNCot.ConsultarPk(vintIDCab)

            If dr.hasrows Then
                dr.read()
                objBECot.IdCab = vintIDCab
                objBECot.Simple = If(IsDBNull(dr("Simple")) = True, "0", dr("Simple"))
                objBECot.Twin = If(IsDBNull(dr("Twin")) = True, "0", dr("Twin"))
                objBECot.Matrimonial = If(IsDBNull(dr("Matrimonial")) = True, "0", dr("Matrimonial"))
                objBECot.Triple = If(IsDBNull(dr("Triple")) = True, "0", dr("Triple"))


                objBECot.SimpleResidente = If(IsDBNull(dr("SimpleResidente")) = True, "0", dr("SimpleResidente"))
                objBECot.TwinResidente = If(IsDBNull(dr("TwinResidente")) = True, "0", dr("TwinResidente"))
                objBECot.MatrimonialResidente = If(IsDBNull(dr("MatrimonialResidente")) = True, "0", dr("MatrimonialResidente"))
                objBECot.TripleResidente = If(IsDBNull(dr("TripleResidente")) = True, "0", dr("TripleResidente"))

                objBECot.IDFile = If(IsDBNull(dr("IDFile")) = True, "", dr("IDFile"))
                objBECot.IDUsuarioRes = dr("IDUsuarioRes")
                objBECot.UserMod = vstrUser

                Dim objBNDetCot As New clsCotizacionBN.clsDetalleCotizacionBN
                Dim dtt As DataTable = objBNDetCot.ConsultarListxIDCab(vintIDCab, True, False)
                For Each drCotiDet As DataRow In dtt.Rows
                    ListaDetCot.Add(objItemPasarDatosDataRowaPropiedadesBE(vintIDCab, drCotiDet, vstrUser))
                Next
            End If
            objBECot.ListaDetalleCotiz = ListaDetCot
            Return objBECot
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function objItemPasarDatosDataRowaPropiedadesBE(ByVal vintIDCab As Integer, ByVal vdrCotidet As DataRow, ByVal vstrUser As String) As clsCotizacionBE.clsDetalleCotizacionBE
        Try
            Dim objBEDetCot As New clsCotizacionBE.clsDetalleCotizacionBE

            With objBEDetCot

                .NroPax = vdrCotidet("NroPax")
                .NroLiberados = If(IsDBNull(vdrCotidet("NroLiberados")) = True, 0, vdrCotidet("NroLiberados"))
                .Tipo_Lib = If(IsDBNull(vdrCotidet("Tipo_Lib")) = True, "", vdrCotidet("Tipo_Lib"))

                .CostoLiberado = vdrCotidet("CostoLiberado")
                .CostoLiberadoImpto = vdrCotidet("CostoLiberadoImpto")
                .CostoReal = vdrCotidet("CostoReal")
                .CostoRealAnt = vdrCotidet("CostoRealAnt")
                .CostoRealImpto = vdrCotidet("CostoRealImpto")

                .Dia = vdrCotidet("Dia")


                .Especial = vdrCotidet("Especial")
                '.IDCab = vdrCotidet("IDCab")
                .IDDet = vdrCotidet("IDDet")
                '.IDDet = vintIDDet
                .IDDetRel = vdrCotidet("IDDetRel")
                .IDDetCopia = vdrCotidet("IDDetCopia")
                .IDIdioma = vdrCotidet("IDIdioma")

                .IDProveedor = vdrCotidet("IDProveedor")
                .IDTipoProv = vdrCotidet("IDTipoProv")
                .PlanAlimenticio = vdrCotidet("PlanAlimenticio")
                .IDServicio = vdrCotidet("IDServicio")
                .IDServicio_Det = vdrCotidet("IDServicio_Det")


                .IDUbigeo = vdrCotidet("IDUbigeo")
                .Item = vdrCotidet("Item")
                .Margen = vdrCotidet("Margen")
                .MargenAplicado = vdrCotidet("MargenAplicado")
                .MargenAplicadoAnt = vdrCotidet("MargenAplicadoAnt")
                .MargenImpto = vdrCotidet("MargenImpto")
                .MargenLiberado = vdrCotidet("MargenLiberado")
                .MargenLiberadoImpto = vdrCotidet("MargenLiberadoImpto")
                .MotivoEspecial = If(IsDBNull(vdrCotidet("MotivoEspecial")) = True, "", vdrCotidet("MotivoEspecial"))
                .RutaDocSustento = If(IsDBNull(vdrCotidet("RutaDocSustento")) = True, "", vdrCotidet("RutaDocSustento"))
                .Desayuno = vdrCotidet("Desayuno")
                .Lonche = vdrCotidet("Lonche")
                .Almuerzo = vdrCotidet("Almuerzo")
                .Cena = vdrCotidet("Cena")

                .Transfer = vdrCotidet("Transfer")
                .IDDetTransferOri = vdrCotidet("IDDetTransferOri")
                .IDDetTransferDes = vdrCotidet("IDDetTransferDes")

                .TipoTransporte = vdrCotidet("TipoTransporte")

                .IDUbigeoOri = vdrCotidet("IDUbigeoOri")
                .IDUbigeoDes = vdrCotidet("IDUbigeoDes")
                .Total = vdrCotidet("Total")
                .TotalOrig = vdrCotidet("TotalOrig")
                .TotImpto = vdrCotidet("TotImpto")

                .Servicio = vdrCotidet("Servicio")
                Dim objLogBN As New clsCotizacionBN.clsDetalleCotizacionBN
                Dim drLog As SqlClient.SqlDataReader = objLogBN.ConsultarUltimoLogxIDProveedor(vintIDCab, vdrCotidet("IDProveedor"))
                If drLog.HasRows Then
                    drLog.Read()
                    .Servicio = drLog("Servicio")
                End If
                drLog.Close()

                .IncGuia = vdrCotidet("IncGuia")

                .UserMod = vstrUser
                '.Accion = vstrAccion
            End With
            Return objBEDetCot
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function Consultar_Rpt_GastosxOficina(ByVal vstrCoUbigeo_Oficina As String, ByVal vdatFecha1 As String, _
                                                      ByVal vdatFecha2 As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@CoUbigeo_Oficina", vstrCoUbigeo_Oficina)
            'dc.Add("@Fecha1", vdatFecha1.ToShortDateString)
            'dc.Add("@Fecha2", vdatFecha2.ToShortDateString)
            dc.Add("@Fecha1", vdatFecha1)
            dc.Add("@Fecha2", vdatFecha2)

            Return objADO.GetDataTable(True, "RESERVAS_DET_Sel_Rpt_GastosxOficina", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function Consultar_Rpt_NoShow(ByVal vintIDCab As Integer, ByVal vdatFecha1 As String, _
                                                    ByVal vdatFecha2 As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCAB", vintIDCab)
            'dc.Add("@Fecha1", vdatFecha1.ToShortDateString)
            'dc.Add("@Fecha2", vdatFecha2.ToShortDateString)

            dc.Add("@Fecha1", vdatFecha1)
            dc.Add("@Fecha2", vdatFecha2)

            Return objADO.GetDataTable(True, "RESERVAS_DET_Rpt_NoShow", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarLogVentas(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataTable(True, "COTIDET_LOG_SelxIDProveedor", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strEstadoReserva(ByVal intIDReserva As Integer) As String

        Try
            Dim str_vReturn As String = ""

            Using dr As SqlClient.SqlDataReader = ConsultarPk(intIDReserva)
                dr.Read()
                If dr.HasRows Then
                    str_vReturn = dr("Estado")
                End If
                dr.Close()
            End Using

            Return str_vReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarProveedoresCorreo(ByVal vintIDCab As Integer) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)

            Return objADO.GetDataTable(True, "RESERVAS_SelProveedoresCorreo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Class clsDetalleReservaBN
        Dim objADO As New clsDataAccessDN
        'Public Function ConsultarxIDReserva(ByVal vintIDReserva As Integer) As SqlClient.SqlDataReader
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDReserva", vintIDReserva)

        '        Return objADO.GetDataReader("RESERVAS_DET_SelxIDReserva", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function

        Public Function blnExistePresupuesto(ByVal vintIDReserva_Det As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva_Det", vintIDReserva_Det)
                dc.Add("@pExistePresupuesto", False)

                Return objADO.GetSPOutputValue("RESERVAS_DET_Sel_ExistPresupuesto", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function ConsultarListxIDFile(ByVal vstrIDFile As String) As DataTable
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDFile", vstrIDFile)

        '        Return objADO.GetDataTable(True, "RESERVAS_DET_SelxIDFile", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function
        Public Function ConsultarListxIDCabIDProveedorMail(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)
                Return objADO.GetDataTable(True, "RESERVAS_DET_SelServiciosxAcomodoEspecial", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDCabIDProveedorGroup(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "RESERVAS_DET_SelServiciosxAcomodoEspecialGroup", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListSubServiciosxIDReservaDet(ByVal vintIDReserva_Det As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva_Det", vintIDReserva_Det)
                Return objADO.GetDataTable(True, "RESERVAS_DETSERVICIOS_Sel_ListxIDReservaDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListSubServiciosTodosxIDReservaDet(ByVal vintIDReserva_Det As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva_Det", vintIDReserva_Det)
                Return objADO.GetDataTable(True, "RESERVAS_DETSERVICIOS_Sel_ListxIDReservaDetTodos", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        ' ''Public Function ConsultarListxIDCabIDProveedorPdf(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
        ' ''    Dim dc As New Dictionary(Of String, String)
        ' ''    Try
        ' ''        dc.Add("@IDCab", vintIDCab)
        ' ''        dc.Add("@IDProveedor", vstrIDProveedor)
        ' ''        Return objADO.GetDataTable(True, "RESERVAS_DET_SelServiciosxAcomodoEspecialPdf", dc)
        ' ''    Catch ex As Exception
        ' ''        Throw
        ' ''    End Try
        ' ''End Function

        'COTIPAX_Sel_ListxIDCab_Mail

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataTable(True, "RESERVAS_DET_SelxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRptServicios(ByVal vstrDescServicio As String, ByVal vstrIDServicio As String, _
                                              ByVal vstrIDCliente As String, ByVal vstrIDProveedor As String, ByVal vstrIDUbigeo As String, _
                                              ByVal vstrEstado As String, ByVal vdatFechaRango1 As Date, _
                                              ByVal vstrIDUsuarioVentas As String, ByVal vstrIDUsuarioReservas As String, _
                                              ByVal vdatFechaRango2 As Date, ByVal vstrCoPais As String, ByVal vstrEstadoReserva As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@DescServicio", vstrDescServicio)
                    .Add("@IDServicio", vstrIDServicio)
                    .Add("@IDCliente", vstrIDCliente)
                    .Add("@IDProveedor", vstrIDProveedor)
                    .Add("@IDUbigeo", vstrIDUbigeo)
                    .Add("@Estado", vstrEstado)
                    .Add("@IDUsuarioVentas", vstrIDUsuarioVentas)
                    .Add("@IDUsuarioReservas", vstrIDUsuarioReservas)
                    .Add("@FechaRango1", vdatFechaRango1)
                    .Add("@FechaRango2", vdatFechaRango2)
                    .Add("@CoPais", vstrCoPais)
                    .Add("@EstadoReserva", vstrEstadoReserva)
                End With
                Return objADO.GetDataTable(True, "RESERVAS_DET_Sel_RptServicios", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRptHotelesRestaurantes(ByVal vstrIDProveedor As String, ByVal vstrIDCliente As String, _
                                                        ByVal vblnPlanAlimenticio As Boolean, ByVal vstrIDCiudad As String, _
                                                        ByVal vstrEstado As String, ByVal vstrEstadoReserva As String, _
                                                        ByVal vstrIDUsuarioVentas As String, ByVal vstrIDUsuarioReservas As String, _
                                                        ByVal vdatFechaRango1 As Date, ByVal vdatFechaRango2 As Date, Optional ByVal vstrCoPais As String = "", _
                                                        Optional ByVal vblnHoteles As Boolean = False, Optional ByVal vblnRestaurantes As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDProveedor", vstrIDProveedor)
                    .Add("@IDCliente", vstrIDCliente)
                    .Add("@PlanAlimenticio", vblnPlanAlimenticio)
                    .Add("@IDCiudad", vstrIDCiudad)
                    .Add("@Estado", vstrEstado)
                    .Add("@EstadoReserva", vstrEstadoReserva)
                    .Add("@IDUsuarioVentas", vstrIDUsuarioVentas)
                    .Add("@IDUsuarioReservas", vstrIDUsuarioReservas)
                    .Add("@FechaRango1", vdatFechaRango1)
                    .Add("@FechaRango2", vdatFechaRango2)
                    .Add("@CoPais", vstrCoPais)
                    .Add("@VerHoteles", vblnHoteles)
                    .Add("@VerRestaurantes", vblnRestaurantes)
                End With
                Return objADO.GetDataTable(True, "RESERVAS_DET_Sel_RptHotelesRestaurantes", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDCabIDProveedor(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "RESERVAS_DET_SelxIDCabIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDCabIDProveedorTMP(ByVal vintIDCab As Integer, ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataTable(True, "RESERVAS_DET_TMP_SelxIDCabIDProveedor", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarxIDCabEmail(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataTable(True, "RESERVAS_DET_SelxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarDetPaxxIDReservaDetLvw(ByVal vintIDReserva_Det As Int32, _
                                                        ByVal vblnNoShow As Boolean) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDReserva_Det", vintIDReserva_Det)
                dc.Add("@NoShow", vblnNoShow)

                Return objADO.GetDataReader("RESERVAS_DET_PAX_Sel_xIDReservaDet_Lvw", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarPendientesxIDCabyIDProvee(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDProveedor", vstrIDProveedor)

                Return objADO.GetDataReader("RESERVAS_DET_SelPendientesxIDCabyIDProvee", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarReporteWordDet(ByVal vintIDreserva As Integer, ByVal vdatFechaIn As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDReserva", vintIDreserva)
                dc.Add("@Dia", vdatFechaIn)

                Return objADO.GetDataTable(True, "RESERVAS_DET_SelxDiaWord", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarHabitEstado(ByVal vintIDReserva As Int32) As DataTable
            ', ByVal vbytNuIngreso As Byte
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDReserva", vintIDReserva)
                'dc.Add("@NuIngreso", vbytNuIngreso)
                'dc.Add("@Simple", vbytSimple)
                'dc.Add("@Twin", vbytTwin)
                'dc.Add("@Matrimonial", vbytMatrimonial)
                'dc.Add("@Triple", vbytTriple)

                Return objADO.GetDataTable(True, "RESERVAS_DET_ESTADOHABITAC_SelxIDReserva", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function blnServicioHotelEsAlimenticioPuro(ByVal vintIDReserva As Integer, _
        '                                                  ByVal vdatFecha As Date) As Boolean
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@IDReserva", vintIDReserva)
        '        dc.Add("@Fecha", vdatFecha)
        '        dc.Add("@pEs", 0)

        '        Return objADO.ExecuteSPOutput("RESERVAS_DET_EsServicioHotelEsAlimenticioPuro_SelOutput", dc)

        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function

        Private Function ConsultarPk(ByVal vintIDReserva_Det As Int32) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva_Det", vintIDReserva_Det)

            Return objADO.GetDataReader("RESERVAS_DET_Sel_Pk", dc)

        End Function

        Public Function ConsultarListaPaxxIDReservaDet(ByVal vintIDReserva_Det As Integer) As Data.DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva_Det", vintIDReserva_Det)
                Return objADO.GetDataTable(True, "RESERVAS_DET_Sel_PaxxIDReservaDet", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarDetServiciosTodosxIDDet(ByVal vintIDDet As Integer, ByVal vbytNroPax As Byte, _
                                                 ByVal vstrMonedaDev As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@NroPax", vbytNroPax)
                dc.Add("@IDMoneda", vstrMonedaDev)

                Return objADO.GetDataTable(True, "COTIDET_DETSERVICIOS_SelxIDDetxNroPax", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function objDetResBEActualizarCtrlsMontosCostos(ByVal vintIDCab As Integer, _
            ByVal vstrIDProveedor As String, _
            ByVal vintIDServicio_Det As Integer, _
            ByVal vdblCostoReal As Double, ByVal vdblCostoLiberado As Double, ByVal vdblSSCR As Double, _
            ByVal vintIDReserva_Det As Integer) As clsReservaBE.clsDetalleReservaBE

            Try
                Dim objCotBN As New clsCotizacionBN
                Dim drCot As SqlClient.SqlDataReader = objCotBN.ConsultarPk(vintIDCab)
                drCot.Read()

                Dim bytSimple As Byte = If(IsDBNull(drCot("Simple")) = True, 0, drCot("Simple"))
                Dim bytTwin As Byte = If(IsDBNull(drCot("Twin")) = True, 0, drCot("Twin"))
                Dim bytMat As Byte = If(IsDBNull(drCot("Matrimonial")) = True, 0, drCot("Matrimonial"))
                Dim bytTriple As Byte = If(IsDBNull(drCot("Triple")) = True, 0, drCot("Triple"))

                Dim bytSimpleRes As Byte = If(IsDBNull(drCot("SimpleResidente")) = True, 0, drCot("SimpleResidente"))
                Dim bytTwinRes As Byte = If(IsDBNull(drCot("TwinResidente")) = True, 0, drCot("TwinResidente"))
                Dim bytMatRes As Byte = If(IsDBNull(drCot("MatrimonialResidente")) = True, 0, drCot("MatrimonialResidente"))
                Dim bytTripleRes As Byte = If(IsDBNull(drCot("TripleResidente")) = True, 0, drCot("TripleResidente"))
                Dim sglTCambio As Single = If(IsDBNull(drCot("TipoCambio")) = True, 0, drCot("TipoCambio"))

                bytSimple += bytSimpleRes
                bytTwin += bytTwinRes
                bytMat += bytMatRes
                bytTriple += bytTripleRes



                Dim drDetRes As SqlClient.SqlDataReader = ConsultarPk(vintIDReserva_Det)
                drDetRes.Read()

                Dim bytCapacidadHab As Byte = If(IsDBNull(drDetRes("CapacidadHab")) = True, 0, drDetRes("CapacidadHab"))
                Dim bytNoches As Byte = If(IsDBNull(drDetRes("Noches")) = True, 0, drDetRes("Noches"))
                Dim blnEsMatrimonial As Boolean = drDetRes("EsMatrimonial")
                Dim dblSSCR As Double = If(IsDBNull(drDetRes("SSCR")) = True, 0, drDetRes("SSCR"))


                Dim objDetCotBN As New clsCotizacionBN.clsDetalleCotizacionBN
                Dim drCotiDet As Object = objDetCotBN.ConsultarPaLiberadosxIDCab(vintIDCab)
                Dim ListDetCot As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)

                While drCotiDet.Read
                    Dim objDetCot As New clsCotizacionBE.clsDetalleCotizacionBE
                    objDetCot.IDDet = drCotiDet("IDDet")
                    objDetCot.NroPax = drCotiDet("NroPax")
                    objDetCot.NroLiberados = drCotiDet("NroLiberados")
                    ListDetCot.Add(objDetCot)
                End While


                Dim objDetResBE As New clsReservaBE.clsDetalleReservaBE
                With objDetResBE
                    .IDDet = drDetRes("IDDet")
                    .IDServicio_Det = vintIDServicio_Det
                    .IDTipoProv = drDetRes("IDTipoProv")


                    Dim objServDetBN As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                    Dim drDetServ As SqlClient.SqlDataReader = objServDetBN.ConsultarPk(vintIDServicio_Det)
                    drDetServ.Read()
                    .CostoReal = vdblCostoReal
                    .CostoLiberado = vdblCostoLiberado
                    .PlanAlimenticio = drDetServ("PlanAlimenticio")
                    .Dias = drDetServ("Dias")
                    .ConAlojamiento = drDetServ("ConAlojamiento")
                    .SSCR = vdblSSCR

                    .IDProveedor = vstrIDProveedor
                    .NroPax = drDetRes("NroPax")
                    .NroLiberados = If(IsDBNull(drDetRes("NroLiberados")), "0", drDetRes("NroLiberados"))
                    .IncGuia = If(IsDBNull(drDetRes("IncGuia")), False, drDetRes("IncGuia"))
                    .CapacidadHab = bytCapacidadHab
                    .Noches = bytNoches
                    .EsMatrimonial = blnEsMatrimonial

                End With



                Dim objResBT As New clsReservaBT
                objResBT.PasarDatosPropiedadesaCostosDetReservasBE(objDetResBE, _
                                                                   bytSimple, bytTwin, _
                                                                   bytMat, bytTriple, _
                                                                   0, 0, 0, 0, 0, _
                                                                   sglTCambio, _
                                                                   ListDetCot)

                Return objDetResBE

            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function ConsultarListLog(ByVal vintIDCab As Int32, ByVal vstrIDProveedor As String, _
        '                                 ByVal vintIDServicio_Det As Integer, _
        '                                 ByVal vchrAccion As Char) As DataTable

        '    Dim dc As New Dictionary(Of String, String)

        '    Try
        '        dc.Add("@IDCab", vintIDCab)
        '        dc.Add("@IDProveedor", vstrIDProveedor)
        '        dc.Add("@IDServicio_Det", vintIDServicio_Det)
        '        dc.Add("@Accion", vchrAccion)

        '        Return objADO.GetDataTable(True, "RESERVAS_DET_LOG_Sel_List", dc)
        '    Catch ex As Exception
        '        Throw

        '    End Try

        'End Function

    End Class

    Public Class clsDetalleReservaPax
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDcab(ByVal vintIDcab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Idcab", vintIDcab)

                Return objADO.GetDataTable(True, "RESERVAS_DET_PAX_SelxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    Public Class clsTareasReservaBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vstrIDUsuarioRes As String, _
                                      ByVal vdatFechaDeadLine As Date, _
                                      ByVal vstrEstado As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDUsuarioRes", vstrIDUsuarioRes)
                dc.Add("@FechaDeadLine", vdatFechaDeadLine)
                dc.Add("@Estado", vstrEstado)

                Return objADO.GetDataTable(True, "RESERVAS_TAREAS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDReserva(ByVal vintIDReserva As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", vintIDReserva)

                Return objADO.GetDataTable(True, "RESERVAS_TAREAS_SelxIDReserva", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
    Public Class clsRecordatoriosBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarDiasparaGeneracion() As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataReader("MARECORDATORIOSRESERVAS_Sel_DiasparaGeneracion", dc)
            Catch ex As Exception
                Throw

            End Try
        End Function

    End Class
    <ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
    Public Class clsMensajesReservasTelefBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarPk(ByVal vintIDReserva As Integer, ByVal vbytIDMensaje As Byte) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)
            dc.Add("@IDMensaje", vbytIDMensaje)

            Return objADO.GetDataReader("RESERVAS_MENSAJESTELEF_Sel_Pk", dc)
        End Function

        Public Function ConsultarList(ByVal vintIDReserva As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDReserva", vintIDReserva)

            Return objADO.GetDataTable(True, "RESERVAS_MENSAJESTELEF_Sel_List", dc)
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsAcomodoPaxProveedorBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarTvw(ByVal vintIDReserva As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", vintIDReserva)

                Return objADO.GetDataTable(True, "ACOMODOPAX_PROVEEDOR_Sel_TvwNd", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vintIDReserva As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", vintIDReserva)

                Return objADO.GetDataTable(True, "ACOMODOPAX_PROVEEDOR_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPdf(ByVal vintIDReserva As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", vintIDReserva)

                Return objADO.GetDataTable(True, "ACOMODOPAX_PROVEEDOR_ListAcomodo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarListxIDReserva(ByVal vintIDReserva As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDReserva", vintIDReserva)

                Return objADO.GetDataTable(True, "ACOMODOPAX_PROVEEDOR_Sel_ListxIDReserva", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarListxIDCab(ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDcab", vintIDCab)
                Return objADO.GetDataTable(True, "ACOMODOPAX_PROVEEDOR_SelxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function blnExisteAcomodoxReserva(ByVal vintIDReserva As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim blnOk As Boolean = False
                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@pExiste", False)
                blnOk = objADO.GetSPOutputValue("ACOMODOPAX_PROVEEDOR_SelExistexIDReserva", dc)

                Return blnOk
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsAcomodoPaxBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarTvw(ByVal vstrIDCAB As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IdCab", vstrIDCAB)

                Return objADO.GetDataTable(True, "ACOMODOPAX_FILE_Sel_TvwNd", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarTvwNdPax(ByVal vintIDCAB As Integer) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCAB", vintIDCAB)

                Return objADO.GetDataTable(True, "ACOMODOPAX_FILE_TvwNdPax", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vintIDCAB As Integer) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCAB", vintIDCAB)

                Return objADO.GetDataTable(True, "ACOMODOPAX_FILE_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPax(ByVal vintIDCab As Integer, ByVal vBytIDHabit As Byte, ByVal vBytCapacidadHab As Byte, ByVal vblnEsMatrimonial As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCab", vintIDCab)
                    .Add("@IDHabit", vBytIDHabit)
                    .Add("@CapacidadHab", vBytCapacidadHab)
                    .Add("@EsMatrimonial", vblnEsMatrimonial)
                End With
                Return objADO.GetDataTable(True, "COTIPAX_SelxAcomodoFile", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function



    End Class

End Class

