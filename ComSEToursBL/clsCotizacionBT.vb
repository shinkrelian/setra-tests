﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsCotizacionBT

    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDT

    Public Function Insertar(ByVal BE As clsCotizacionBE) As Int32
        Dim dc As New Dictionary(Of String, String)

        Try
            With dc
                .Add("@IDCliente", BE.IDCliente)
                .Add("@Titulo", BE.Titulo)
                .Add("@TipoPax", BE.TipoPax)
                .Add("@CotiRelacion", BE.CotiRelacion)
                .Add("@IDUsuario", BE.IDUsuario)
                .Add("@NroPax", BE.NroPax)
                .Add("@NroLiberados", BE.NroLiberados)
                .Add("@Tipo_Lib", BE.Tipo_Lib)
                .Add("@Margen", BE.Margen)
                .Add("@IDBanco", BE.IDBanco)
                '.Add("@IDFile", BE.IDFile)
                .Add("@IDTipoDoc", BE.IDTipoDoc)
                .Add("@Observaciones", BE.Observaciones)
                .Add("@FecInicio", BE.FecInicio)
                .Add("@FechaOut", BE.FechaOut)
                .Add("@Cantidad_Dias", BE.Cantidad_Dias)
                .Add("@FechaSeg", BE.FechaSeg)
                .Add("@DatoSeg", BE.DatoSeg)
                .Add("@TipoCambio", BE.TipoCambio)
                .Add("@DocVta", BE.DocVta)
                .Add("@TipoServicio", BE.TipoServicio)
                .Add("@IDIdioma", BE.IDIdioma)
                .Add("@Simple", BE.Simple)
                .Add("@Twin", BE.Twin)
                .Add("@Matrimonial", BE.Matrimonial)
                .Add("@Triple", BE.Triple)
                .Add("@IDUbigeo", BE.IDUbigeo)
                .Add("@Notas", BE.Notas)
                .Add("@Programa_Desc", BE.Programa_Desc)
                .Add("@PaxAdulE", BE.PaxAdulE)
                .Add("@PaxAdulN", BE.PaxAdulN)
                .Add("@PaxNinoE", BE.PaxNinoE)
                .Add("@PaxNinoN", BE.PaxNinoN)
                .Add("@Aporte", BE.Aporte)
                .Add("@AporteMonto", BE.AporteMonto)
                .Add("@AporteTotal", BE.AporteTotal)
                .Add("@Redondeo", BE.Redondeo)
                .Add("@IDCabCopia", BE.IDCabCopia)
                .Add("@Categoria", BE.Categoria)
                .Add("@UserMod", BE.UserMod)
                .Add("@pIDCab", 0)

            End With

            Dim intIDCab As Int32 = objADO.ExecuteSPOutput("COTICAB_Ins", dc)

            BE.IdCab = intIDCab

            Dim objBTQui As New clsCotizacionBT.clsCotizacionQuiebresBT
            objBTQui.InsertarActualizarEliminar(BE)

            Dim objBTDet As New clsCotizacionBT.clsDetalleCotizacionBT
            objBTDet.InsertarActualizarEliminar(BE)

            ActualizarIDDetTransfer(BE)

            'Dim objBTDetQ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
            'objBTDetQ.InsertarActualizarEliminar(BE)

            Dim objBLPax As New clsCotizacionBT.clsCotizacionPaxBT
            objBLPax.InsertarActualizarEliminar(BE)


            'Dim dcIDDet As New Dictionary(Of String, String)
            'dcIDDet.Add("@IDCab", intIDCab)
            'Dim dttIDDet As DataTable = objADO.GetDataTable("COTIDET_Sel_IDDetxIDCab", dcIDDet)
            Dim objPaxDet As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
            'For Each dr As DataRow In dttIDDet.Rows
            '    objPaxDet.InsertarxIDDet(BE.IdCab, dr("IDDet"), BE.UserMod)
            'Next

            Dim dttIDDet As DataTable = objBTDet.ConsultarIDDetxIDCab(intIDCab)
            For Each dr As DataRow In dttIDDet.Rows
                objPaxDet.InsertarxIDDet(BE.IdCab, dr("IDDet"), BE.UserMod)
            Next

            Dim objHotCat As New clsCategoriasHotelesBT
            objHotCat.InsertarActualizarEliminar(BE)

            ActualizarIDDetenBETransportes(BE)

            Dim objCotiVuelo As New clsCotizacionVuelosBT
            objCotiVuelo.InsertarEliminar(BE)


            'borrar metodo y sp
            'Dim objHotCat As New clsCategoriasHotelesBT
            'objHotCat.InsertarxIDCab(intIDCab)

            ContextUtil.SetComplete()

            Return intIDCab
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Sub Actualizar_Observacion(ByVal vintIDCab As Integer, ByVal vstrObservacionVentas As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@ObservacionVentas", vstrObservacionVentas)

            objADO.ExecuteSP("COTICAB_UpdObservaciones", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            'Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AsignarUsuario")
        End Try
    End Sub

    Private Sub ActualizarIDDetenBETransportes(ByVal BE As clsCotizacionBE)
        Try
            For Each ItemVuelo As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                    If Not IsNumeric(ItemVuelo.IDDet) Then
                        If Mid(ItemVuelo.IDDet, 3) = Mid(ItemCot.IDDetMem, 2) Then
                            ItemVuelo.IDDet = ItemCot.IDDet
                            Exit For
                        End If
                    End If
                Next

            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActualizarIDDetTransfer(ByVal BE As clsCotizacionBE)
        Try
            Dim dc As Dictionary(Of String, String)

            For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz

                If ItemCot.IDDetTransferOri <> "0" Then
                    If Not IsNumeric(ItemCot.IDDetTransferOri) Then
                        For Each ItemCot2 As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                            'If ItemCot2.IDDetTransferOri <> "0" Then
                            If Not ItemCot2.IDDetMem Is Nothing Then
                                If Mid(ItemCot.IDDetTransferOri, 2) = Mid(ItemCot2.IDDetMem, 2) Then
                                    ItemCot.IDDetTransferOri = ItemCot2.IDDet

                                    dc = New Dictionary(Of String, String)
                                    dc.Add("@IDDet", ItemCot.IDDet)
                                    dc.Add("@IDDetTransferOri", ItemCot.IDDetTransferOri)
                                    objADO.ExecuteSP("COTIDET_Upd_TransferOri", dc)

                                    Exit For
                                End If
                                'End If
                            End If
                        Next
                    End If
                End If
            Next



            For Each ItemCot As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz

                If ItemCot.IDDetTransferDes <> "0" Then
                    If Not IsNumeric(ItemCot.IDDetTransferDes) Then
                        For Each ItemCot2 As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                            'If ItemCot2.IDDetTransferOri <> "0" Then
                            If Not ItemCot2.IDDetMem Is Nothing Then
                                If Mid(ItemCot.IDDetTransferDes, 2) = Mid(ItemCot2.IDDetMem, 2) Then
                                    ItemCot.IDDetTransferDes = ItemCot2.IDDet

                                    dc = New Dictionary(Of String, String)
                                    dc.Add("@IDDet", ItemCot.IDDet)
                                    dc.Add("@IDDetTransferDes", ItemCot.IDDetTransferDes)
                                    objADO.ExecuteSP("COTIDET_Upd_TransferDes", dc)

                                    Exit For
                                End If
                                'End If
                            End If
                        Next
                    End If
                End If
            Next


            ContextUtil.SetComplete()
        Catch ex As Exception
            Throw
            ContextUtil.SetAbort()
        End Try
    End Sub


    Public Sub AsignarUsuario_Ventas(ByVal vintIDCab As Integer, ByVal vstrIDUsuarioAsignado As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDUsuario", vstrIDUsuarioAsignado)
            dc.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_Upd_UserVentas", dc)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            'Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AsignarUsuario")
        End Try
    End Sub


    Public Sub AsignarUsuario_Ventas_Lista(ByVal vobjLista As List(Of String), ByVal vstrIDUsuarioAsignado As String, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try

            If vobjLista Is Nothing Then Exit Sub
            Try
                For Each Item As String In vobjLista
                    AsignarUsuario_Ventas(Item, vstrIDUsuarioAsignado, vstrUserMod)
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        Catch ex As Exception
            ContextUtil.SetAbort()
            'Throw
            ' Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".AsignarUsuario")
        End Try
    End Sub


    'Public Sub Actualizar(ByVal BE As clsCotizacionBE)
    '    Dim dc As New Dictionary(Of String, String)

    '    Try


    '        With dc
    '            .Add("@IdCab", BE.IdCab)
    '            .Add("@Estado", BE.Estado)
    '            .Add("@IDCliente", BE.IDCliente)
    '            .Add("@Titulo", BE.Titulo)
    '            .Add("@TipoPax", BE.TipoPax)
    '            .Add("@CotiRelacion", BE.CotiRelacion)
    '            .Add("@IDUsuario", BE.IDUsuario)
    '            .Add("@NroPax", BE.NroPax)
    '            .Add("@NroLiberados", BE.NroLiberados)
    '            .Add("@Tipo_Lib", BE.Tipo_Lib)

    '            .Add("@Margen", BE.Margen)
    '            .Add("@IDBanco", BE.IDBanco)
    '            '.Add("@IDFile", BE.IDFile)
    '            .Add("@IDTipoDoc", BE.IDTipoDoc)
    '            .Add("@Observaciones", BE.Observaciones)
    '            .Add("@FecInicio", BE.FecInicio)
    '            .Add("@FechaOut", Format(BE.FechaOut, "dd/MM/yyyy"))
    '            .Add("@Cantidad_Dias", BE.Cantidad_Dias)
    '            .Add("@FechaSeg", BE.FechaSeg)
    '            .Add("@DatoSeg", BE.DatoSeg)
    '            .Add("@TipoCambio", BE.TipoCambio)
    '            .Add("@DocVta", BE.DocVta)
    '            .Add("@TipoServicio", BE.TipoServicio)
    '            .Add("@IDIdioma", BE.IDIdioma)
    '            .Add("@Simple", BE.Simple)
    '            .Add("@Twin", BE.Twin)
    '            .Add("@Matrimonial", BE.Matrimonial)
    '            .Add("@Triple", BE.Triple)
    '            .Add("@IDUbigeo", BE.IDUbigeo)
    '            .Add("@Notas", BE.Notas)
    '            .Add("@Programa_Desc", BE.Programa_Desc)
    '            .Add("@PaxAdulE", BE.PaxAdulE)
    '            .Add("@PaxAdulN", BE.PaxAdulN)
    '            .Add("@PaxNinoE", BE.PaxNinoE)
    '            .Add("@PaxNinoN", BE.PaxNinoN)
    '            .Add("@Aporte", BE.Aporte)
    '            .Add("@AporteMonto", BE.AporteMonto)
    '            .Add("@AporteTotal", BE.AporteTotal)
    '            .Add("@Redondeo", BE.Redondeo)
    '            .Add("@Categoria", BE.Categoria)
    '            .Add("@UserMod", BE.UserMod)

    '        End With

    '        objADO.ExecuteSP("COTICAB_Upd", dc)


    '        Dim objBTQui As New clsCotizacionBT.clsCotizacionQuiebresBT
    '        objBTQui.InsertarActualizarEliminar(BE)

    '        Dim objBLPax As New clsCotizacionBT.clsCotizacionPaxBT
    '        objBLPax.InsertarActualizarEliminar(BE)

    '        Dim objBTDet As New clsCotizacionBT.clsDetalleCotizacionBT

    '        objBTDet.InsertarActualizarEliminar(BE)

    '        ActualizarIDDetTransfer(BE)

    '        objBTQui.InsertarActualizarEliminar(BE, "M") ' recalculo quiebres siempre

    '        Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN
    '        Dim strTipoPax As String = objBTDet.strDevuelveTipoPaxNacionalidad(BE)

    '        ReCalculoCotizacion(BE.IdCab, BE.NroPax, BE.NroLiberados, BE.Tipo_Lib, strTipoPax, BE.Redondeo, BE.IGV, "M", BE.UserMod, BE.ListaDetPax)


    '        'Dim objBLPax As New clsCotizacionBT.clsCotizacionPaxBT
    '        'objBLPax.InsertarActualizarEliminar(BE)

    '        'objBLPax.ActualizarTotales(0, BE.IdCab, 0, False, BE.UserMod)

    '        'Dim objBLDetPax As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
    '        'objBLDetPax.InsertarEliminar(BE)


    '        'Dim objHotCat As New clsCategoriasHotelesBT

    '        'borrar metodo y sp
    '        'objHotCat.InsertarxIDCab(BE.IdCab)


    '        ''objHotCat.InsertarActualizarEliminar(BE)

    '        Dim objHotCat As New clsCategoriasHotelesBT
    '        objHotCat.InsertarActualizarEliminar(BE)

    '        ActualizarIDDetenBETransportes(BE)

    '        Dim objCotVuelo As New clsCotizacionVuelosBT
    '        objCotVuelo.InsertarEliminar(BE)

    '        'Reservas
    '        If BE.Estado = "A" Then

    '            If strDevuelveIDFileCoti(BE.IdCab).Trim = "" Then
    '                Dim objResBT As New clsReservaBT
    '                'objResBT.GrabarxCotizacion(BE)

    '            Else

    '                'Proveedores nuevos
    '                Dim blnPorReservar As Boolean = False
    '                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
    '                    If Not Item.Reservado Then
    '                        blnPorReservar = True
    '                        Exit For
    '                    End If
    '                Next
    '                If blnPorReservar Then
    '                    Dim objResBT As New clsReservaBT
    '                    'objResBT.GrabarxCotizacion(BE)
    '                    objResBT.GrabarxCotizacionNuevosProveedores(BE)

    '                End If
    '                '''''


    '            End If

    '        End If



    '        ContextUtil.SetComplete()
    '    Catch ex As Exception

    '        ContextUtil.SetAbort()
    '        Throw
    '    End Try
    'End Sub
    Private Function strDevuelveIDFileCoti(ByVal vintIDCab As Int32) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pIDFile", "")
            Return objADO.ExecuteSPOutput("COTICAB_Sel_IDFileOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function datDevuelveFechaReservaCoti(ByVal vintIDCab As Int32) As Date
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@pFechaReserva", "")
            Return objADO.ExecuteSPOutput("COTICAB_Sel_FechaReservaOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Sub Anular(ByVal vintIDCab As Int32, ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@UserMod", vstrUserMod)

            End With

            objADO.ExecuteSP("COTICAB_Anu", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Public Sub ActualizarDatosDocWord(ByVal vintIDCab As Int32, _
                                      ByVal vstrDocWordIDIdioma As String, _
                                      ByVal vstrDocWordRutaImagen As String, _
                                      ByVal vstrDocWordSubTitulo As String, _
                                      ByVal vstrDocWordTexto As String, _
                                      ByVal vstrUserMod As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)
                .Add("@DocWordIDIdioma", vstrDocWordIDIdioma)
                .Add("@DocWordRutaImagen", vstrDocWordRutaImagen)
                .Add("@DocWordSubTitulo", vstrDocWordSubTitulo)
                .Add("@DocWordTexto", vstrDocWordTexto)
                .Add("@UserMod", vstrUserMod)

            End With

            objADO.ExecuteSP("COTICAB_UpdDatosDocWord", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub Eliminar(ByVal vintIDCab As Int32)
        Dim dc As New Dictionary(Of String, String)
        Try
            Using objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarTodosxIDCab(vintIDCab)
                    If Not dr.HasRows Then
                        EliminarPaxs(vintIDCab)

                        With dc
                            .Add("@IDCab", vintIDCab)
                        End With

                        objADO.ExecuteSP("COTICAB_Del", dc)
                        ContextUtil.SetComplete()
                    End If
                    dr.Close()
                End Using
            End Using

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    Public Sub EliminarPaxs(ByVal vintIDCab As Int32)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCab", vintIDCab)

            End With
            'objADO.ExecuteSP("COTIDET_PAX_DelxIDCab", dc)
            objADO.ExecuteSP("COTIPAX_DelxIDCab", dc)
            ContextUtil.SetComplete()

        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
    Private Function dttCreaDttDetCotiServ() As DataTable

        Dim dttDetCotiServ As New DataTable("DetCotiServ")
        With dttDetCotiServ
            .Columns.Add("IDServicio")
            .Columns.Add("IDServicio_Det")
            .Columns.Add("CostoReal")
            .Columns.Add("Margen")
            .Columns.Add("CostoLiberado")
            .Columns.Add("MargenLiberado")
            .Columns.Add("MargenAplicado")
            .Columns.Add("Total")
            .Columns.Add("RedondeoTotal")
            .Columns.Add("SSCR")
            .Columns.Add("SSMargen")
            .Columns.Add("SSCL")
            .Columns.Add("SSML")
            .Columns.Add("SSTotal")
            .Columns.Add("STCR")
            .Columns.Add("STMargen")
            .Columns.Add("STTotal")

            .Columns.Add("CostoRealImpto")
            .Columns.Add("CostoLiberadoImpto")
            .Columns.Add("MargenImpto")
            .Columns.Add("MargenLiberadoImpto")
            .Columns.Add("SSCRImpto")
            .Columns.Add("SSCLImpto")
            .Columns.Add("SSMargenImpto")
            .Columns.Add("SSMargenLiberadoImpto")
            .Columns.Add("STCRImpto")
            .Columns.Add("STMargenImpto")
            .Columns.Add("TotImpto")

        End With


        Return dttDetCotiServ
    End Function

    '    Public Sub ReCalculo_Cotizacion(ByVal vintIDCab As Int32, _
    '                               ByVal vintPax As Int16, ByVal vintLiberados As Int16, _
    '                               ByVal vstrTipo_Lib As Char, ByVal vstrTipoPax As String, _
    '                               ByVal vdecRedondeo As Decimal, _
    '                               ByVal vsglIGV As Single, ByVal vstrAccion As Char, ByVal vstrUser As String, _
    '                               ByVal vListaDetPax As List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE))
    '        Try
    '            'Dim objCotiDet As New clsCotizacionBN.clsDetalleCotizacionBN
    '            Dim objCotiDet As New clsCotizacionBT.clsDetalleCotizacionBT

    '            Dim objCoti As New clsCotizacionBN
    '            Dim objBE As New clsCotizacionBE
    '            'Dim drCotiDetQ As SqlClient.SqlDataReader
    '            'Dim objBLDetQ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
    '            Dim dttCotiDet As DataTable = objCotiDet.ConsultarListxIDCab(vintIDCab, False, False)
    '            Dim objBEDet As clsCotizacionBE.clsDetalleCotizacionBE
    '            Dim ListaDetCot As List(Of clsCotizacionBE.clsDetalleCotizacionBE)
    '            Dim dc As Dictionary(Of String, Double)
    '            Dim dttDetCotiServ As DataTable
    '            Dim dblCostoReal As Double, dblMargenAplicado As Double

    '            For Each drCotiDet As DataRow In dttCotiDet.Rows
    '                If drCotiDet("IDDetRel") <> 0 Then GoTo Sgte

    '                dblCostoReal = 0
    '                dblMargenAplicado = 0
    '                If drCotiDet("Especial") = True Then
    '                    dblCostoReal = drCotiDet("CostoReal")
    '                    dblMargenAplicado = drCotiDet("MargenAplicado")
    '                End If

    '                dttDetCotiServ = dttCreaDttDetCotiServ()
    '                dc = objCoti.CalculoCotizacion(drCotiDet("IDServicio").ToString, drCotiDet("IDServicio_Det").ToString, _
    '                    drCotiDet("Anio").ToString, drCotiDet("Tipo").ToString, _
    '                    drCotiDet("IDTipoProv").ToString, vintPax, vintLiberados, vstrTipo_Lib, _
    '                    drCotiDet("MargenCotiCab").ToString, drCotiDet("TipoCambioCotiCab").ToString, _
    '                    dblCostoReal, dblMargenAplicado, vstrTipoPax, vdecRedondeo, dttDetCotiServ, vsglIGV)

    '                ListaDetCot = New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
    '                objBEDet = New clsCotizacionBE.clsDetalleCotizacionBE

    '                For Each dic As KeyValuePair(Of String, Double) In dc
    '                    If dic.Key = "CostoReal" Then objBEDet.CostoReal = dic.Value
    '                    If dic.Key = "Margen" Then objBEDet.Margen = dic.Value
    '                    If dic.Key = "CostoLiberado" Then objBEDet.CostoLiberado = dic.Value
    '                    If dic.Key = "MargenLiberado" Then objBEDet.MargenLiberado = dic.Value
    '                    If dic.Key = "MargenAplicado" Then objBEDet.MargenAplicado = dic.Value
    '                    If dic.Key = "TotImpto" Then objBEDet.TotImpto = dic.Value
    '                    If dic.Key = "Total" Then objBEDet.Total = dic.Value
    '                    If dic.Key = "SSCR" Then objBEDet.SSCR = dic.Value
    '                    If dic.Key = "SSMargen" Then objBEDet.SSMargen = dic.Value
    '                    If dic.Key = "SSCL" Then objBEDet.SSCL = dic.Value
    '                    If dic.Key = "SSML" Then objBEDet.SSMargenLiberado = dic.Value
    '                    If dic.Key = "SSTotal" Then objBEDet.SSTotal = dic.Value
    '                    If dic.Key = "STCR" Then objBEDet.STCR = dic.Value
    '                    If dic.Key = "STMargen" Then objBEDet.STMargen = dic.Value
    '                    If dic.Key = "STTotal" Then objBEDet.STTotal = dic.Value

    '                    If dic.Key = "CostoRealImpto" Then objBEDet.CostoRealImpto = dic.Value
    '                    If dic.Key = "CostoLiberadoImpto" Then objBEDet.CostoLiberadoImpto = dic.Value
    '                    If dic.Key = "MargenImpto" Then objBEDet.MargenImpto = dic.Value
    '                    If dic.Key = "MargenLiberadoImpto" Then objBEDet.MargenLiberadoImpto = dic.Value
    '                    If dic.Key = "SSCLImpto" Then objBEDet.SSCLImpto = dic.Value
    '                    If dic.Key = "SSCRImpto" Then objBEDet.SSCRImpto = dic.Value
    '                    If dic.Key = "SSMargenImpto" Then objBEDet.SSMargenImpto = dic.Value
    '                    If dic.Key = "SSMargenLiberadoImpto" Then objBEDet.SSMargenLiberadoImpto = dic.Value
    '                    If dic.Key = "STCRImpto" Then objBEDet.STCRImpto = dic.Value
    '                    If dic.Key = "STMargenImpto" Then objBEDet.STMargenImpto = dic.Value
    '                Next

    '                With objBEDet
    '                    'If vstrAccion = "M" Then
    '                    '    .IDDet_Q = intConsultarIDDet_QxIDCab_Q(vintIDCab_Q)
    '                    'End If

    '                    .Cotizacion = drCotiDet("Cotizacion").ToString
    '                    .Item = drCotiDet("Item").ToString
    '                    .Dia = drCotiDet("Dia").ToString
    '                    .IDUbigeo = drCotiDet("IDUbigeo").ToString
    '                    .IDProveedor = drCotiDet("IDProveedor").ToString
    '                    .IDServicio = drCotiDet("IDServicio").ToString
    '                    .IDServicio_Det = drCotiDet("IDServicio_Det").ToString
    '                    .IDIdioma = drCotiDet("IDIdioma").ToString
    '                    .MotivoEspecial = drCotiDet("MotivoEspecial").ToString
    '                    .RutaDocSustento = drCotiDet("RutaDocSustento").ToString
    '                    .Desayuno = drCotiDet("Desayuno")
    '                    .Lonche = drCotiDet("Lonche")
    '                    .Almuerzo = drCotiDet("Almuerzo")
    '                    .Cena = drCotiDet("Cena")
    '                    .Transfer = drCotiDet("Transfer")
    '                    '.OrigenTransfer = drCotiDet("OrigenTransfer")
    '                    '.DestinoTransfer = drCotiDet("DestinoTransfer")
    '                    .IDDetTransferOri = drCotiDet("IDDetTransferOri")
    '                    .IDDetTransferDes = drCotiDet("IDDetTransferDes")
    '                    .TipoTransporte = drCotiDet("TipoTransporte")
    '                    .IDUbigeoOri = drCotiDet("IDUbigeoOri")
    '                    .IDUbigeoDes = drCotiDet("IDUbigeoDes")
    '                    .Servicio = drCotiDet("Servicio").ToString
    '                    .IDDetRel = 0
    '                    .IDDet = drCotiDet("IDDet").ToString
    '                    .NroPax = drCotiDet("NroPax").ToString
    '                    .NroLiberados = If(IsDBNull(drCotiDet("NroLiberados")) = True, "0", drCotiDet("NroLiberados").ToString)
    '                    .Tipo_Lib = If(IsDBNull(drCotiDet("Tipo_Lib")) = True, "", drCotiDet("Tipo_Lib"))
    '                    .TotalOrig = drCotiDet("TotalOrig").ToString
    '                    .SSTotalOrig = drCotiDet("SSTotalOrig").ToString
    '                    .STTotalOrig = drCotiDet("STTotalOrig").ToString
    '                    '.IDCab_Q = vintIDCab_Q
    '                    '.CostoLiberadoImpto = .CostoLiberado * (1 + (vsglIGV / 100))
    '                    '.CostoRealImpto = .CostoReal * (1 + (vsglIGV / 100))
    '                    '.MargenImpto = .Margen * (1 + (vsglIGV / 100))
    '                    '.MargenLiberadoImpto = .MargenLiberado * (1 + (vsglIGV / 100))
    '                    '.SSCLImpto = .SSCL * (1 + (vsglIGV / 100))
    '                    '.SSCRImpto = .SSCR * (1 + (vsglIGV / 100))
    '                    '.SSMargenImpto = .SSMargen * (1 + (vsglIGV / 100))
    '                    '.SSMargenLiberadoImpto = .SSMargenLiberado * (1 + (vsglIGV / 100))
    '                    '.STCRImpto = .STCR * (1 + (vsglIGV / 100))
    '                    '.STMargenImpto = .STMargen * (1 + (vsglIGV / 100))
    '                    '.TotImpto = .CostoLiberadoImpto + .CostoRealImpto + .MargenImpto + .MargenLiberadoImpto '+ _
    '                    '.SSCLImpto(+.SSCRImpto + .SSMargenImpto + .SSMargenLiberadoImpto + .STCRImpto + .STMargenImpto)
    '                    .Especial = drCotiDet("Especial").ToString
    '                    .CostoRealAnt = 0
    '                    .MargenAplicadoAnt = 0
    '                    .UserMod = vstrUser
    '                    .Recalcular = False
    '                    .ExecTrigger = drCotiDet("ExecTrigger")
    '                    .Accion = vstrAccion
    '                End With
    '                ListaDetCot.Add(objBEDet)




    '                'Det. Cotiz. Serv*****
    '                Dim objBEDetS As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE
    '                Dim ListaDetCotS As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE)

    '                For Each drDetCotiServ As DataRow In dttDetCotiServ.Rows
    '                    objBEDetS = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE
    '                    With objBEDetS
    '                        .IDServicio_Det = drDetCotiServ("IDServicio_Det")
    '                        '.IDDet = drCotiDet("IDDet")
    '                        .CostoLiberado = drDetCotiServ("CostoLiberado")
    '                        .CostoLiberadoImpto = drDetCotiServ("CostoLiberadoImpto")
    '                        .CostoReal = drDetCotiServ("CostoReal")
    '                        .CostoRealImpto = drDetCotiServ("CostoRealImpto")

    '                        .Margen = drDetCotiServ("Margen")
    '                        .MargenAplicado = drDetCotiServ("MargenAplicado")
    '                        .MargenImpto = drDetCotiServ("MargenImpto")
    '                        .MargenLiberado = drDetCotiServ("MargenLiberado")
    '                        .MargenLiberadoImpto = drDetCotiServ("MargenLiberadoImpto")

    '                        .SSCL = drDetCotiServ("SSCL")
    '                        .SSCLImpto = drDetCotiServ("SSCLImpto")
    '                        .SSCR = drDetCotiServ("SSCR")
    '                        .SSCRImpto = drDetCotiServ("SSCRImpto")
    '                        .SSMargen = drDetCotiServ("SSMargen")
    '                        .SSMargenImpto = drDetCotiServ("STMargenImpto")
    '                        .SSMargenLiberado = drDetCotiServ("SSML")
    '                        .SSMargenLiberadoImpto = drDetCotiServ("SSMargenLiberadoImpto")
    '                        .SSTotal = drDetCotiServ("SSTotal")
    '                        .STCR = drDetCotiServ("STCR")
    '                        .STCRImpto = drDetCotiServ("STCRImpto")
    '                        .STMargen = drDetCotiServ("STMargen")
    '                        .STMargenImpto = drDetCotiServ("STMargenImpto")
    '                        .STTotal = drDetCotiServ("STTotal")
    '                        .Total = drDetCotiServ("Total")
    '                        .TotImpto = drDetCotiServ("TotImpto")
    '                        .UserMod = vstrUser

    '                    End With
    '                    ListaDetCotS.Add(objBEDetS)
    '                Next

    '                'objBE = New clsCotizacionBE
    '                objBE.ListaDetalleCotizacionDetServicio = ListaDetCotS
    '                objBE.ListaDetalleCotiz = ListaDetCot

    '                'If Not vListaDetPax Is Nothing Then
    '                objBE.ListaDetPax = vListaDetPax
    '                objBE.UserMod = vstrUser
    '                objBE.IdCab = vintIDCab

    '                'End If


    '                objCotiDet.InsertarActualizarEliminar(objBE, True)

    'Sgte:

    '            Next


    '            ContextUtil.SetComplete()

    '        Catch ex As Exception
    '            ContextUtil.SetAbort()
    '            Throw
    '        End Try
    '    End Sub

    Public Sub Copiar(ByVal vintIDCab As Integer, ByVal vstrIDUsuarioNue As String, _
                      ByVal vdatDia As Date, _
                      ByVal vstrUserMod As String)

        Try

            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCab", vintIDCab)
            dc.Add("@IDUsuarioNue", vstrIDUsuarioNue)
            dc.Add("@UserMod", vstrUserMod)
            dc.Add("@pIDCab", 0)

            Dim intIDCabNue As Int32 = objADO.ExecuteSPOutput("COTICAB_Ins_CopiarNuevoUsuario", dc)


            Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN
            Dim objBE As New clsCotizacionBE

            Dim ListaDetCot As New List(Of clsCotizacionBE.clsDetalleCotizacionBE)
            Using dr As SqlClient.SqlDataReader = objBN.ConsultarTodosxIDCab(vintIDCab)
                Dim objBEDet As clsCotizacionBE.clsDetalleCotizacionBE
                While dr.Read
                    objBEDet = New clsCotizacionBE.clsDetalleCotizacionBE
                    With objBEDet
                        .Accion = "N"
                        .IDDet = dr("IDDet")
                        .IDCab = intIDCabNue
                        .Cotizacion = dr("Cotizacion")
                        .Item = dr("Item")
                        .Dia = vdatDia
                        'DiaNombre
                        .IDUbigeo = dr("IDUbigeo")
                        .IDProveedor = dr("IDProveedor")
                        .IDServicio = dr("IDServicio")
                        .IDServicio_Det = dr("IDServicio_Det")
                        .IDIdioma = dr("IDIdioma")
                        .Especial = dr("Especial")
                        .MotivoEspecial = If(IsDBNull(dr("MotivoEspecial")) = True, "", dr("MotivoEspecial"))
                        .RutaDocSustento = If(IsDBNull(dr("RutaDocSustento")) = True, "", dr("RutaDocSustento"))
                        .Desayuno = dr("Desayuno")
                        .Lonche = dr("Lonche")
                        .Almuerzo = dr("Almuerzo")
                        .Cena = dr("Cena")
                        .Transfer = dr("Transfer")
                        .IDDetTransferOri = If(IsDBNull(dr("IDDetTransferOri")) = True, "", dr("IDDetTransferOri"))
                        .IDDetTransferDes = If(IsDBNull(dr("IDDetTransferDes")) = True, "", dr("IDDetTransferDes"))
                        'CamaMat
                        .TipoTransporte = If(IsDBNull(dr("TipoTransporte")) = True, "", dr("TipoTransporte"))
                        .IDUbigeoOri = dr("IDUbigeoOri")
                        .IDUbigeoDes = dr("IDUbigeoDes")
                        .NroPax = dr("NroPax")
                        .NroLiberados = If(IsDBNull(dr("NroLiberados")) = True, 0, dr("NroLiberados"))
                        .Tipo_Lib = If(IsDBNull(dr("Tipo_Lib")) = True, "", dr("Tipo_Lib"))
                        .CostoReal = If(IsDBNull(dr("CostoReal")) = True, 0, dr("CostoReal"))
                        .CostoRealAnt = If(IsDBNull(dr("CostoRealAnt")) = True, 0, dr("CostoRealAnt"))
                        .CostoLiberado = If(IsDBNull(dr("CostoLiberado")) = True, 0, dr("CostoLiberado"))
                        .Margen = If(IsDBNull(dr("Margen")) = True, 0, dr("Margen"))
                        .MargenAplicado = If(IsDBNull(dr("MargenAplicado")) = True, 0, dr("MargenAplicado"))
                        .MargenAplicadoAnt = 0
                        .MargenLiberado = If(IsDBNull(dr("MargenLiberado")) = True, 0, dr("MargenLiberado"))
                        .Total = If(IsDBNull(dr("Total")) = True, 0, dr("Total"))
                        .TotalOrig = If(IsDBNull(dr("TotalOrig")) = True, 0, dr("TotalOrig"))
                        .SSCR = dr("SSCR")
                        .SSCL = dr("SSCL")
                        .SSMargen = dr("SSMargen")
                        .SSMargenLiberado = dr("SSMargenLiberado")
                        .SSTotal = dr("SSTotal")
                        .SSTotalOrig = dr("SSTotalOrig")
                        .CostoRealImpto = dr("CostoRealImpto")
                        .CostoLiberadoImpto = dr("CostoLiberadoImpto")
                        .MargenImpto = dr("MargenImpto")
                        .MargenLiberadoImpto = dr("MargenLiberadoImpto")
                        .SSCRImpto = dr("SSCRImpto")
                        .SSCLImpto = dr("SSCLImpto")
                        .SSMargenImpto = dr("SSMargenImpto")
                        .SSMargenLiberadoImpto = dr("SSMargenLiberadoImpto")
                        .STCRImpto = dr("STCRImpto")
                        .STCRImpto = dr("STCRImpto")
                        .STMargenImpto = dr("STMargenImpto")
                        .TotImpto = dr("TotImpto")
                        .IDDetRel = If(IsDBNull(dr("IDDetRel")) = True, 0, dr("IDDetRel"))
                        .Servicio = If(IsDBNull(dr("Servicio")) = True, "", dr("Servicio"))
                        .IDDetCopia = dr("IDDet")
                        .Recalcular = False
                        .PorReserva = False
                        '.ExecTrigger = False
                        .UserMod = vstrUserMod

                    End With
                    ListaDetCot.Add(objBEDet)
                End While
                dr.Close()
            End Using
            objBE.ListaDetalleCotiz = ListaDetCot

            Dim objBTDet As New clsCotizacionBT.clsDetalleCotizacionBT
            objBTDet.InsertarActualizarEliminar(objBE, vintIDCab)

            Dim dc2 As New Dictionary(Of String, String)

            dc2.Add("@IDCab", intIDCabNue)
            dc2.Add("@IDCabCopia", vintIDCab)
            dc2.Add("@UserMod", vstrUserMod)

            objADO.ExecuteSP("COTICAB_QUIEBRES_Ins_CopiaIDCab", dc2)

            'Dim dc3 As Dictionary(Of String, String)
            'For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In objBE.ListaDetalleCotiz
            '    dc3 = New Dictionary(Of String, String)
            '    If Item.TipoTransporte = "T" Then
            '        dc3.Add("@IDCab", Item.IDCab)
            '        dc3.Add("@IDCabCopia", vintIDCab)
            '        dc3.Add("@IDDet", Item.IDDet)
            '        dc3.Add("@UserMod", vstrUserMod)
            '        objADO.ExecuteSP("COTIVUELOS_InsCopiaIDCAB", dc3)
            '    End If
            'Next


            'PAX()
            Dim intNroPax As Int16 = 0
            Dim strIDPais As String = ""
            Using objBNCab As New clsCotizacionBN
                Using drCotiCab As SqlClient.SqlDataReader = objBNCab.ConsultarPk(vintIDCab)
                    drCotiCab.Read()
                    intNroPax = drCotiCab("NroPax")
                    strIDPais = drCotiCab("IDPais")
                    drCotiCab.Close()
                End Using
            End Using

            Dim strTipoDocPasaporte As String = "004"
            Dim objBEPax As clsCotizacionBE.clsCotizacionPaxBE
            Dim ListaPax As New List(Of clsCotizacionBE.clsCotizacionPaxBE)
            For intFila As Int16 = 0 To intNroPax - 1

                objBEPax = New clsCotizacionBE.clsCotizacionPaxBE
                With objBEPax
                    .Accion = "N"
                    .Apellidos = "Pax " & intFila + 1
                    .Nombres = "Pax " & intFila + 1
                    .FecNacimiento = "01/01/1900"
                    .IDCab = intIDCabNue
                    .IDIdentidad = strTipoDocPasaporte
                    .IDNacionalidad = strIDPais
                    .NumIdentidad = ""
                    .Titulo = "SR."
                    .UserMod = vstrUserMod
                End With
                ListaPax.Add(objBEPax)
            Next

            objBE = New clsCotizacionBE
            objBE.ListaPax = ListaPax
            Dim objBT As New clsCotizacionBT.clsCotizacionPaxBT
            objBT.InsertarActualizarEliminar(objBE)

            'DETPAX

            Dim dttIDDET As DataTable = objBTDet.ConsultarIDDetxIDCab(intIDCabNue)
            Dim objPaxDet As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
            For Each drIDDet As DataRow In dttIDDET.Rows
                objPaxDet.InsertarxIDDet(intIDCabNue, drIDDet("IDDet"), vstrUserMod)

                objBTDet.ActualizarNroPax(drIDDet("IDDet"), vstrUserMod)
            Next

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub

    ' ''<ComVisible(True)> _
    ' ''<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    ' ''Public Class clsCotizacionTiposCambioBT
    ' ''    Inherits ServicedComponent
    ' ''    Dim objADO As New clsDataAccessDT
    ' ''    Dim strNombreClase As String = "clsCotizacionTiposCambioBT"

    ' ''    Private Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionTiposCambioBE, _
    ' ''                        Optional ByVal vintIDCab As Integer = 0)
    ' ''        Dim dc As New Dictionary(Of String, String)
    ' ''        Try
    ' ''            dc.Add("@IDCab", vintIDCab)
    ' ''            dc.Add("@CoMoneda", BE.CoMoneda)
    ' ''            dc.Add("@SsTipCam", BE.SsTipCam)
    ' ''            dc.Add("@UserMod", BE.UserMod)

    ' ''            objADO.ExecuteSP("COTICAB_TIPOSCAMBIO_Ins", dc)
    ' ''            ContextUtil.SetComplete()
    ' ''        Catch ex As Exception
    ' ''            ContextUtil.SetAbort()
    ' ''            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Insertar")
    ' ''        End Try
    ' ''    End Sub

    ' ''    Private Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionTiposCambioBE)
    ' ''        Dim dc As New Dictionary(Of String, String)
    ' ''        Try
    ' ''            dc.Add("@IDCab", BE.IDCab)
    ' ''            dc.Add("@CoMoneda", BE.CoMoneda)
    ' ''            dc.Add("@SsTipCam", BE.SsTipCam)
    ' ''            dc.Add("@UserMod", BE.UserMod)

    ' ''            objADO.ExecuteSP("COTICAB_TIPOSCAMBIO_Upd", dc)
    ' ''            ContextUtil.SetComplete()
    ' ''        Catch ex As Exception
    ' ''            ContextUtil.SetAbort()
    ' ''            Throw New ApplicationException("Error: " & ex.Message & " En: " & strNombreClase & ".Actualizar")
    ' ''        End Try
    ' ''    End Sub

    ' ''    Public Sub InsertarActualizar(ByVal BE As clsCotizacionBE)
    ' ''        Try
    ' ''            For Each Item As clsCotizacionBE.clsCotizacionTiposCambioBE In BE.ListaTipoCambios
    ' ''                If Item.Accion = "N" Then
    ' ''                    Insertar(Item, BE.IdCab)
    ' ''                ElseIf Item.Accion = "M" Then
    ' ''                    Actualizar(Item)
    ' ''                End If
    ' ''            Next
    ' ''        Catch ex As Exception
    ' ''            Throw
    ' ''        End Try
    ' ''    End Sub
    ' ''End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCotizacionQuiebresBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE, Optional ByVal vstrAccion As String = "")
            Try
                Dim dc As Dictionary(Of String, String)
                Dim objDetQ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
                Dim intIDCab_Q As Int32
                Dim objBN As New clsCotizacionBN.clsDetalleCotizacionBN
                Dim objBT As New clsCotizacionBT.clsDetalleCotizacionBT


                For Each Item As clsCotizacionBE.clsCotizacionQuiebreBE In BE.ListaCotizQuiebre
                    If vstrAccion <> "" Then Item.Accion = "M"
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        If Item.Accion = "N" Then dc.Add("@IdCab", BE.IdCab)
                        If Item.Accion = "M" Then dc.Add("@IDCab_Q", Item.IDCab_Q)
                        dc.Add("@Pax", Item.Pax)
                        dc.Add("@Liberados", Item.Liberados)
                        dc.Add("@Tipo_Lib", Item.Tipo_Lib)
                        dc.Add("@UserMod", Item.UserMod)
                        If Item.Accion = "N" Then
                            dc.Add("@pIDCab_Q", 0)
                            intIDCab_Q = objADO.ExecuteSPOutput("COTICAB_QUIEBRES_Ins", dc)
                        ElseIf Item.Accion = "M" Then
                            objADO.ExecuteSP("COTICAB_QUIEBRES_Upd", dc)
                            intIDCab_Q = Item.IDCab_Q
                        End If


                        Dim strTipoPax As String = objBT.strDevuelveTipoPaxNacionalidad(BE)
                        objDetQ.ReCalculoDetQuiebre(BE.IdCab, intIDCab_Q, Item.Pax, Item.Liberados, _
                                                    Item.Tipo_Lib, strTipoPax, BE.Redondeo, BE.IGV, Item.Accion, Item.UserMod)
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDCab_Q", Item.IDCab_Q)
                        objADO.ExecuteSP("COTICAB_QUIEBRES_Del", dc)
                    End If
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
   <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCotizacionArchivosBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT


        Public Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionArchivosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    '.Add("@IDArchivo", BE.IDArchivo)
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@Nombre", BE.Nombre)
                    .Add("@RutaOrigen", BE.RutaOrigen)
                    .Add("@RutaDestino", BE.RutaDestino)
                    .Add("@CoTipo", BE.CoTipo)
                    .Add("@FlCopiado", BE.FlCopiado)
                    .Add("@CoUsuarioCreacion", BE.CoUsuarioCreacion)
                    .Add("@UserMod", BE.UserMod)

                End With
                objADO.ExecuteSP("COTICAB_ARCHIVOS_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionArchivosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc


                    .Add("@IDArchivo", BE.IDArchivo)
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@Nombre", BE.Nombre)
                    .Add("@RutaOrigen", BE.RutaOrigen)
                    .Add("@RutaDestino", BE.RutaDestino)
                    .Add("@CoTipo", BE.CoTipo)
                    .Add("@FlCopiado", BE.FlCopiado)
                    .Add("@CoUsuarioCreacion", BE.CoUsuarioCreacion)
                    .Add("@UserMod", BE.UserMod)

                End With
                objADO.ExecuteSP("COTICAB_ARCHIVOS_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal BE As clsCotizacionBE.clsCotizacionArchivosBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc


                    .Add("@IDArchivo", BE.IDArchivo)

                End With
                objADO.ExecuteSP("COTICAB_ARCHIVOS_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
            If BE.ListaCotizArchivos Is Nothing Then Exit Sub
            Try
                For Each Item As clsCotizacionBE.clsCotizacionArchivosBE In BE.ListaCotizArchivos
                    If Item.Accion = "N" Then
                        CopiarArchivosCotizacion(Item)
                        Insertar(Item)
                    ElseIf Item.Accion = "M" Then
                        CopiarArchivosCotizacion(Item)
                        Actualizar(Item)
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item)
                    End If
                Next
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub CopiarArchivosCotizacion(ByRef BE As clsCotizacionBE.clsCotizacionArchivosBE)
            Try


                'Dim strRutaFinal1 As String = strRuta + Year(datFechaFile).ToString + "\" + strAnioMes + "\" + strRutaFile1
                Dim strTipoTmp As String = ""
                If BE.FlCopiado = False Then

                    'copiar el archivo
                    My.Computer.FileSystem.CopyFile(
    BE.RutaOrigen,
                    BE.RutaDestino,
    Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
    Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)

                    BE.FlCopiado = True
                    'Dim blnExisteRuta As Boolean
                    'blnExisteRuta = System.IO.Directory.Exists("c:\ExistingFolderName")
                End If

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsDetalleCotizacionBT

        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE, Optional ByVal vintIDCabAnt As Integer = 0, Optional ByVal vblnRecalculo As Boolean = False)

            Try
                Dim dc As Dictionary(Of String, String)
                Dim intIDDet As Integer, intIDDetRel As Integer = 0
                Dim intIDDetCopia As Integer
                Dim strIDDet As String
                Dim objBLPax As New clsCotizacionBT.clsCotizacionPaxBT
                Dim intIDDetInicial As Integer = 0

                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                    dc = New Dictionary(Of String, String)

                    If Item.Accion = "M" Or Item.Accion = "N" Then

                        If BE.Estado = "A" And Item.Accion = "M" And Item.ExecTrigger = True Then
                            InsertarLog1eraVez(Item.IDDet)
                        End If

                        If Item.Accion = "M" Then
                            dc.Add("@IDDet", Item.IDDet)
                            dc.Add("@CostoRealAnt", Item.CostoRealAnt)
                            dc.Add("@MargenAplicadoAnt", Item.MargenAplicadoAnt)
                        End If

                        dc.Add("@NroPax", Item.NroPax)
                        dc.Add("@NroLiberados", Item.NroLiberados)
                        dc.Add("@Tipo_Lib", Item.Tipo_Lib)

                        dc.Add("@CostoLiberado", Item.CostoLiberado)
                        dc.Add("@CostoLiberadoImpto", Item.CostoLiberadoImpto)
                        dc.Add("@CostoReal", Item.CostoReal)

                        dc.Add("@CostoRealImpto", Item.CostoRealImpto)
                        dc.Add("@Cotizacion", Item.Cotizacion)
                        dc.Add("@Dia", Format(Item.Dia, "dd/MM/yyyy HH:mm:ss"))
                        dc.Add("@Especial", Item.Especial)
                        If BE.IdCab <> 0 Then
                            dc.Add("@IDCab", BE.IdCab)
                        Else
                            dc.Add("@IDCab", Item.IDCab)
                        End If
                        dc.Add("@IDIdioma", Item.IDIdioma)
                        dc.Add("@IDProveedor", Item.IDProveedor)
                        dc.Add("@IDServicio", Item.IDServicio)
                        dc.Add("@IDServicio_Det", Item.IDServicio_Det)
                        dc.Add("@IDUbigeo", Item.IDUbigeo)
                        dc.Add("@Item", Item.Item)
                        dc.Add("@Margen", Item.Margen)
                        dc.Add("@MargenAplicado", Item.MargenAplicado)

                        dc.Add("@MargenImpto", Item.MargenImpto)
                        dc.Add("@MargenLiberado", Item.MargenLiberado)
                        dc.Add("@MargenLiberadoImpto", Item.MargenLiberadoImpto)
                        dc.Add("@MotivoEspecial", Item.MotivoEspecial)
                        dc.Add("@RutaDocSustento", Item.RutaDocSustento)

                        dc.Add("@Desayuno", Item.Desayuno)
                        dc.Add("@Lonche", Item.Lonche)
                        dc.Add("@Almuerzo", Item.Almuerzo)
                        dc.Add("@Cena", Item.Cena)

                        dc.Add("@Transfer", Item.Transfer)

                        If IsNumeric(Item.IDDetTransferOri) Then
                            dc.Add("@IDDetTransferOri", Item.IDDetTransferOri)
                        Else
                            dc.Add("@IDDetTransferOri", 0)
                        End If
                        If IsNumeric(Item.IDDetTransferDes) Then
                            dc.Add("@IDDetTransferDes", Item.IDDetTransferDes)
                        Else
                            dc.Add("@IDDetTransferDes", 0)
                        End If

                        dc.Add("@TipoTransporte", Item.TipoTransporte)

                        dc.Add("@IDUbigeoOri", Item.IDUbigeoOri)
                        dc.Add("@IDUbigeoDes", Item.IDUbigeoDes)

                        dc.Add("@Servicio", Item.Servicio)
                        dc.Add("@SSCL", Item.SSCL)
                        dc.Add("@SSCLImpto", Item.SSCLImpto)
                        dc.Add("@SSCR", Item.SSCR)
                        dc.Add("@SSCRImpto", Item.SSCRImpto)
                        dc.Add("@SSMargen", Item.SSMargen)
                        dc.Add("@SSMargenImpto", Item.SSMargenImpto)
                        dc.Add("@SSMargenLiberado", Item.SSMargenLiberado)
                        dc.Add("@SSMargenLiberadoImpto", Item.SSMargenLiberadoImpto)
                        dc.Add("@SSTotal", Item.SSTotal)
                        dc.Add("@SSTotalOrig", Item.SSTotalOrig)
                        dc.Add("@STCR", Item.STCR)
                        dc.Add("@STCRImpto", Item.STCRImpto)
                        dc.Add("@STMargen", Item.STMargen)
                        dc.Add("@STMargenImpto", Item.STMargenImpto)
                        dc.Add("@STTotal", Item.STTotal)
                        dc.Add("@STTotalOrig", Item.STTotalOrig)
                        dc.Add("@Total", Item.Total)
                        dc.Add("@TotalOrig", Item.TotalOrig)
                        dc.Add("@TotImpto", Item.TotImpto)
                        dc.Add("@Recalcular", Item.Recalcular)
                        dc.Add("@UserMod", Item.UserMod)
                        If Item.Accion = "N" Then

                            If Item.IDDetRel <> 0 Then
                                dc.Add("@IDDetRel", Item.IDDetRel)
                            Else
                                dc.Add("@IDDetRel", 0)
                            End If

                            'Copia
                            intIDDetCopia = 0
                            If Not IsNumeric(Item.IDDetCopia) Then
                                For Each Item2 As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                                    If Item.IDDetCopia = Item2.IDDetMem Then
                                        intIDDetCopia = Item2.IDDet
                                        Exit For
                                    End If
                                Next
                            Else
                                intIDDetCopia = Item.IDDetCopia
                            End If
                            dc.Add("@IDDetCopia", intIDDetCopia)

                            dc.Add("@PorReserva", Item.PorReserva)
                            dc.Add("@FueradeHorario", Item.FueradeHorario)

                            dc.Add("@pIDDet", 0)
                            intIDDet = objADO.ExecuteSPOutput("COTIDET_Ins", dc)

                            If Item.IDDetRel = 0 Then
                                ActualizarIDsNuevosenHijosST(BE, Item.IDDet, intIDDet)
                            End If


                            If BE.Estado = "A" And Item.Accion = "N" And Item.ExecTrigger = True Then
                                InsertarLog1eraVez(intIDDet)
                            End If

                            If Item.PorReserva = True And Item.IDReserva <> "" Then
                                ActualizarPorCambioReserva(intIDDet, Item.IDReserva, BE.UserMod)
                            End If

                            Item.IDDetMem = Item.IDDet

                            If blnExisteIDDetVuelo(Item.IDDet, vintIDCabAnt) Then
                                Dim dc3 As New Dictionary(Of String, String)
                                dc3.Add("@IDCab", Item.IDCab)
                                dc3.Add("@IDCabCopia", vintIDCabAnt)
                                dc3.Add("@IDDet", intIDDet)
                                dc3.Add("@IDDetCopia", Item.IDDet)
                                dc3.Add("@UserMod", Item.UserMod)
                                objADO.ExecuteSP("COTIVUELOS_InsCopiaIDCAB", dc3)
                            End If

                            Item.IDDet = intIDDet

                            If Item.IDDetRel <> "" Then
                                If Item.IDDetRel.Substring(0, 1) = "P" Then 'R0
                                    intIDDetRel = intIDDet
                                End If
                            End If


                            If Not BE.ListaDetPax Is Nothing Then

                                ''''''''
                                If Item.IDDetRel = "" Then
                                    strIDDet = ""
                                    For Each Item2 As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax

                                        If strIDDet = "" Then strIDDet = Item2.IDDet
                                        If Item2.IDDet.ToString = strIDDet Then
                                            Item2.IDDet = intIDDet
                                        End If
                                    Next
                                End If
                                ''''''''
                                Dim objBLDetPax As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
                                objBLDetPax.InsertarEliminar(BE, intIDDet, If(Item.IDDetRel = "" Or Item.IDDetRel = "0", False, True))

                                ''''
                                If Item.IDDetRel = "" Then
Borrar:
                                    For Each Item2 As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                                        If Item2.IDDet = intIDDet.ToString Then
                                            BE.ListaDetPax.Remove(Item2)
                                            GoTo Borrar
                                        End If
                                    Next
                                    ''''
                                End If
                            End If

                            If Item.IDTipoProv IsNot Nothing Then
                                objBLPax.ActualizarTotales(0, BE.IdCab, 0, False, Item.IDTipoProv, BE.UserMod)
                            End If

                        ElseIf Item.Accion = "M" Then
                            If IsNumeric(Item.IDDet) Then
                                intIDDet = Item.IDDet
                            End If
                            dc.Add("@FueradeHorario", Item.FueradeHorario)
                            dc.Add("@ExecTrigger", Item.ExecTrigger)

                            objADO.ExecuteSP("COTIDET_Upd", dc)

                            If Not BE.ListaDetPax Is Nothing Then
                                Dim objBLDetPax As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
                                'objBLDetPax.InsertarEliminar(BE, intIDDet, True, vblnRecalculo)
                                objBLDetPax.InsertarEliminar(BE, intIDDet, If(Item.IDDetRel = "" Or Item.IDDetRel = "0", False, True), vblnRecalculo)
                            End If

                        End If

                    ElseIf Item.Accion = "B" Then
                        If IsNumeric(Item.IDDet) Then
                            Dim objBTDetServ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionDetServiciosBT
                            objBTDetServ.EliminarxIDDet(Item.IDDet)

                            Dim objBTDescServ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionDescripServicioBT
                            objBTDescServ.EliminarxIDDet(Item.IDDet)

                            Dim objBTDetPax As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
                            objBTDetPax.EliminarxIDDet(Item.IDDet)

                            Dim objBTDetQuieb As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
                            objBTDetQuieb.EliminarxIDDet(Item.IDDet)

                            dc.Add("@IDDet", Item.IDDet)
                            objADO.ExecuteSP("COTIDET_Del", dc)
                        End If
                    End If


                    If intIDDetCopia <> 0 And Item.Accion = "N" Then
                        Copiar(intIDDet, intIDDetCopia, Item.IDCab, Item.UserMod)
                    End If

                    If Not BE.ListaDetalleCotizacionQuiebres Is Nothing Then
                        Dim objBTDetQ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
                        objBTDetQ.InsertarActualizarEliminar(BE, intIDDet)
                    End If

                    'If Item.IDDetCopia = 0 Then
                    If intIDDetCopia = 0 Then
                        If Not BE.ListaDetalleCotizacionDetServicio Is Nothing Then
                            If intIDDet > 0 Then
                                Dim objBTDetServ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionDetServiciosBT
                                objBTDetServ.InsertarActualizar(BE, intIDDet)
                            End If
                        End If
                    End If


                    If Not vblnRecalculo Then
                        'Textos Servicios x IDDet
                        If Item.Accion = "M" Then
                            If Not BE.ListaDetalleCotizacionDescripServicio Is Nothing Then
                                Dim objBTDesc As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionDescripServicioBT
                                objBTDesc.InsertarActualizarEliminar(BE, Item.IDDet)

                            End If
                        ElseIf Item.Accion = "N" Then
                            'Dim objServBN As New clsServicioProveedorBN.clsTextoServicioProveedorBN
                            Dim objServBN As New clsServicioProveedorBN.clsServiciosporDiaBN
                            'Dim dr As SqlClient.SqlDataReader = objServBN.ConsultarxIDServicio(Item.IDServicio)

                            Dim strIDDetRel As String, strIDDetRelLet As Char
                            If Item.IDDetRel = "" Then
                                strIDDetRel = ""
                            Else
                                strIDDetRel = Mid(Item.IDDetRel, 2)
                                strIDDetRelLet = Item.IDDetRel.Substring(0, 1)
                            End If


                            Dim dr As SqlClient.SqlDataReader
                            If strIDDetRel = "" Or strIDDetRelLet = "P" Then
                                dr = objServBN.ConsultarxIDServicioDia(Item.IDServicio, 1)
                            Else
                                dr = objServBN.ConsultarxIDServicioDia(Item.IDServicio, bytDiaDetCotiz(BE, Mid(Item.IDDetRel, 2)))
                            End If

                            Dim objBETExtoIdioma As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE
                            Dim ListaDetTextoIdioma As New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE)
                            While dr.Read
                                objBETExtoIdioma = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE
                                With objBETExtoIdioma
                                    .IDDet = intIDDet
                                    .IDIdioma = dr("IDIdioma")
                                    .DescLarga = dr("Descripcion")
                                    .UserMod = dr("UserMod")
                                    .Accion = "N"
                                End With
                                ListaDetTextoIdioma.Add(objBETExtoIdioma)
                            End While
                            dr.Close()


                            BE.ListaDetalleCotizacionDescripServicio = ListaDetTextoIdioma
                            Dim objBTDesc As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionDescripServicioBT
                            objBTDesc.InsertarActualizarEliminar(BE, intIDDet)

                        End If
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        ''->PPMG20160330
        Public Sub dblDetUpdateZicasso(ByVal vintIDCab As Int32)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                objADO.ExecuteSP("COTIDET_Update_Valor_Zicasso", dc)
            Catch ex As Exception
                Throw
            End Try
        End Sub
        ''<-PPMG20160330

        Private Function blnExisteIDDetVuelo(ByVal vintIDDet As Integer, ByVal vintIDCab As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim blnExiste As Boolean = False
                With dc
                    .Add("@IDCAB", vintIDCab)
                    .Add("@IDDet", vintIDDet)
                    .Add("@pOk", blnExiste)
                End With

                blnExiste = CBool(objADO.GetSPOutputValue("COTIVUELOS_ExisteSelOutPut", dc))
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub ActualizarIDsNuevosenHijosST(ByRef BE As clsCotizacionBE, ByVal vintAntiguoID As Integer, ByVal vintNuevoID As Integer)
            Try
                For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                    If Item.IDDetRel = vintAntiguoID Then
                        Item.IDDetRel = vintNuevoID
                    End If
                Next
            Catch ex As Exception
                Throw
            End Try
        End Sub

        Private Sub ActualizarPorCambioReserva(ByVal vintIDDet As Integer, _
                                               ByVal vintIDReserva As Integer, ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDReserva", vintIDReserva)
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("RESERVAS_DET_Upd_IDDet", dc)

                dc = New Dictionary(Of String, String)
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@PorReserva", 0)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("COTIDET_Upd_PorReserva", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function bytDiaDetCotiz(ByVal BE As clsCotizacionBE, ByVal vstrIDDetRel As String)

            Dim bytContFila As Byte = 1
            Dim bytDia As Byte = 1
            For Each Item As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz

                If Not IsNumeric(Item.IDDet) Then
                    If Mid(Item.IDDetRel, 2) = vstrIDDetRel Then

                        While bytDia <= bytContFila
                            bytDia += 1
                        End While

                        Exit For
                    End If
                    bytContFila += 1
                End If
            Next

            Return bytDia
        End Function

        Public Sub ActualizarNroPax(ByVal vintIDDet As Integer, ByVal vstrUserMod As String)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("COTIDET_Upd_NroPax", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub Copiar(ByVal vintIDDet As Integer, ByVal vintIDDetCopia As Integer, ByVal vintIDCab As Int32, ByVal vstrUserMod As String)
            Try
                Dim dcDet As New Dictionary(Of String, String)

                dcDet.Add("@IDDet", vintIDDet)
                dcDet.Add("@IDDetCopia", vintIDDetCopia)
                dcDet.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("COTIDET_DETSERVICIOS_Ins_CopiaIDDet", dcDet)
                'objADO.ExecuteSP("COTIDET_PAX_Ins_CopiaIDDet", dcDet)
                objADO.ExecuteSP("COTIDET_QUIEBRES_Ins_CopiaIDDet", dcDet)

                Dim objBTDet As New clsCotizacionBT.clsDetalleCotizacionBT
                Dim dttIDDET As DataTable = objBTDet.ConsultarIDDetxIDCab(vintIDCab)
                Dim objPaxDet As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
                For Each drIDDet As DataRow In dttIDDET.Rows
                    objPaxDet.InsertarxIDDet(vintIDCab, drIDDet("IDDet"), vstrUserMod)
                Next


                ActualizarNroPax(vintIDDet, vstrUserMod)


                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32, ByVal vblnConCopias As Boolean, _
                                            ByVal vblnSoloparaRecalcular As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@ConCopias", vblnConCopias)
                dc.Add("@SoloparaRecalcular", vblnSoloparaRecalcular)

                Return objADO.GetDataTable("COTIDET_Sel_xIDCab", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarIDDetxIDCab(ByVal vintIDCab As Integer) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable("COTIDET_Sel_IDDetxIDCab", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function strDevuelveTipoPaxNacionalidad(ByVal BE As clsCotizacionBE) As String
            Try

                Dim objBTPax As clsCotizacionBT.clsCotizacionPaxBT
                Dim dttPax As DataTable
                Dim dttDetPax As DataTable

                'Dim strTipoPax As String = If(BE.TipoPax Is Nothing, "EXT", BE.TipoPax)
                Dim strTipoPax As String = "EXT"
                Dim blnPeruano As Boolean
                Dim strIDDet As String = ""
                If Not BE.ListaDetalleCotiz Is Nothing Then
                    For Each ItemDetCot As clsCotizacionBE.clsDetalleCotizacionBE In BE.ListaDetalleCotiz
                        strIDDet = ItemDetCot.IDDet
                        Exit For
                    Next
                End If
                Dim blnIDDetTexto As Boolean = False
                If Not IsNumeric(strIDDet) Then
                    blnIDDetTexto = True

                    'Return strTipoPax
                    'Exit Function
                End If
                Dim intCountPax As Int16 = 0
                If Not BE.ListaDetPax Is Nothing Then
                    intCountPax = BE.ListaDetPax.Count
                End If

                If intCountPax > 0 Then

                    For Each ItemDetPax As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                        If ItemDetPax.Selecc Then
                            If BE.ListaPax.Count > 0 Then
                                blnPeruano = False
                                For Each ItemPax As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                                    If IsNumeric(ItemPax.IDPax) Then
                                        If ItemDetPax.IDPax = ItemPax.IDPax And ItemPax.IDNacionalidad = "000323" Then 'Perú
                                            blnPeruano = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If blnPeruano Then
                                    strTipoPax = "NCR"
                                End If
                            Else
                                'bd
                                objBTPax = New clsCotizacionBT.clsCotizacionPaxBT
                                dttPax = objBTPax.ConsultarListxIDCab(BE.IdCab)
                                blnPeruano = False
                                For Each drPax As DataRow In dttPax.Rows
                                    If IsNumeric(ItemDetPax.IDPax) Then
                                        If ItemDetPax.IDPax = drPax("IDPax") And drPax("IDNacionalidad") = "000323" Then 'Perú
                                            blnPeruano = True
                                            Exit For
                                        End If
                                    End If
                                Next
                                If blnPeruano Then
                                    strTipoPax = "NCR"
                                    Exit For
                                End If

                            End If

                        End If
                    Next
                Else 'BD

                    objBTPax = New clsCotizacionBT.clsCotizacionPaxBT
                    If Not blnIDDetTexto Then
                        dttDetPax = objBTPax.ConsultarDetxIDDetLvw(strIDDet)
                    Else
                        dttDetPax = objBTPax.ConsultarListxIDCab(BE.IdCab)
                    End If
                    For Each drDetPax As DataRow In dttDetPax.Rows
                        Dim intCountPax2 As Int16 = 0
                        If Not BE.ListaPax Is Nothing Then
                            intCountPax2 = BE.ListaPax.Count
                        End If
                        If intCountPax2 > 0 Then
                            blnPeruano = False
                            For Each ItemPax As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                                If IsNumeric(ItemPax.IDPax) Then
                                    If drDetPax("IDPax") = ItemPax.IDPax And ItemPax.IDNacionalidad = "000323" Then 'Perú
                                        blnPeruano = True
                                        Exit For
                                    End If
                                End If
                            Next
                            If blnPeruano Then
                                strTipoPax = "NCR"
                            End If
                        Else
                            'bd

                            dttPax = objBTPax.ConsultarListxIDCab(BE.IdCab)
                            blnPeruano = False
                            For Each drPax As DataRow In dttPax.Rows
                                If drDetPax("IDPax") = drPax("IDPax") And drPax("IDNacionalidad") = "000323" Then 'Perú
                                    blnPeruano = True
                                    Exit For
                                End If
                            Next
                            If blnPeruano Then
                                strTipoPax = "NCR"
                                Exit For
                            End If

                        End If


                    Next



                End If



                Return strTipoPax
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Sub InsertarLog1eraVez(ByVal vintIDDet As Integer)
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDDet", vintIDDet)

                objADO.ExecuteSP("COTIDET_LOG_SP_Ins", dc)
            Catch ex As Exception
                Throw
            End Try
        End Sub

        <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsDetalleCotizacionQuiebresBT

            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT

            Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE, ByVal vintIDDet As Int32)
                Try
                    Dim dc As Dictionary(Of String, String)
                    For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE In BE.ListaDetalleCotizacionQuiebres

                        dc = New Dictionary(Of String, String)
                        If Item.Accion = "M" Or Item.Accion = "N" Then
                            If Item.Accion = "M" Then
                                dc.Add("@IDDet_Q", Item.IDDet_Q)
                            Else
                                dc.Add("@IDCab_Q", Item.IDCab_Q)
                                dc.Add("@IDDet", vintIDDet)
                            End If
                            dc.Add("@CostoLiberado", Item.CostoLiberado)
                            dc.Add("@CostoLiberadoImpto", Item.CostoLiberadoImpto)
                            dc.Add("@CostoReal", Item.CostoReal)
                            dc.Add("@CostoRealImpto", Item.CostoRealImpto)
                            dc.Add("@Margen", Item.Margen) ''
                            dc.Add("@MargenAplicado", Item.MargenAplicado)
                            dc.Add("@MargenImpto", Item.MargenImpto)
                            dc.Add("@MargenLiberado", Item.MargenLiberado)
                            dc.Add("@MargenLiberadoImpto", Item.MargenLiberadoImpto)
                            dc.Add("@SSCL", Item.SSCL)
                            dc.Add("@SSCLImpto", Item.SSCLImpto)
                            dc.Add("@SSCR", Item.SSCR)
                            dc.Add("@SSCRImpto", Item.SSCRImpto)
                            dc.Add("@SSMargen", Item.SSMargen) ''
                            dc.Add("@SSMargenImpto", Item.SSMargenImpto)
                            dc.Add("@SSMargenLiberado", Item.SSMargenLiberado)
                            dc.Add("@SSMargenLiberadoImpto", Item.SSMargenLiberadoImpto)
                            dc.Add("@SSTotal", Item.SSTotal) ''
                            dc.Add("@STCR", Item.STCR) ''
                            dc.Add("@STCRImpto", Item.STCRImpto)
                            dc.Add("@STMargen", Item.STMargen) ''
                            dc.Add("@STMargenImpto", Item.STMargenImpto)
                            dc.Add("@STTotal", Item.STTotal) ''
                            dc.Add("@Total", Item.Total) ''
                            dc.Add("@TotImpto", Item.TotImpto)
                            dc.Add("@UserMod", Item.UserMod)
                            If Item.Accion = "N" Then
                                objADO.ExecuteSP("COTIDET_QUIEBRES_Ins", dc)
                            ElseIf Item.Accion = "M" Then
                                objADO.ExecuteSP("COTIDET_QUIEBRES_Upd", dc)
                            End If

                        ElseIf Item.Accion = "B" Then
                            dc.Add("@IDDet_Q", Item.IDDet_Q)
                            objADO.ExecuteSP("COTIDET_QUIEBRES_Del", dc)
                        End If

                    Next
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Private Function dttCreaDttDetCotiServ() As DataTable

                Dim dttDetCotiServ As New DataTable("DetCotiServ")
                With dttDetCotiServ
                    .Columns.Add("IDServicio")
                    .Columns.Add("IDServicio_Det")
                    .Columns.Add("dblCostoReal")
                    .Columns.Add("dblMargen")
                    .Columns.Add("dblCostoLiberado")
                    .Columns.Add("dblMargenLiberado")
                    .Columns.Add("dblMargenAplicado")
                    .Columns.Add("dblTotal")
                    .Columns.Add("RedondeoTotal")
                    .Columns.Add("dblSSCR")
                    .Columns.Add("dblSSMargen")
                    .Columns.Add("dblSSCL")
                    .Columns.Add("dblSSML")
                    .Columns.Add("dblSSTotal")
                    .Columns.Add("dblSTCR")
                    .Columns.Add("dblSTMargen")
                    .Columns.Add("dblSTTotal")

                    .Columns.Add("CostoRealImpto")
                    .Columns.Add("CostoLiberadoImpto")
                    .Columns.Add("MargenImpto")
                    .Columns.Add("MargenLiberadoImpto")
                    .Columns.Add("SSCRImpto")
                    .Columns.Add("SSCLImpto")
                    .Columns.Add("SSMargenImpto")
                    .Columns.Add("SSMargenLiberadoImpto")
                    .Columns.Add("STCRImpto")
                    .Columns.Add("STMargenImpto")
                    .Columns.Add("TotImpto")

                End With


                Return dttDetCotiServ
            End Function

            Public Sub ReCalculoDetQuiebre(ByVal vintIDCab As Int32, ByVal vintIDCab_Q As Int32, _
                                           ByVal vintPax As Int16, ByVal vintLiberados As Int16, _
                                           ByVal vstrTipo_Lib As Char, ByVal vstrTipoPax As String, _
                                           ByVal vdecRedondeo As Decimal, _
                                           ByVal vsglIGV As Single, ByVal vstrAccion As Char, ByVal vstrUser As String)
                Try
                    'Dim objCotiDet As New clsCotizacionBN.clsDetalleCotizacionBN
                    Dim objCotiDet As New clsCotizacionBT.clsDetalleCotizacionBT

                    Dim objCoti As New clsCotizacionBN
                    Dim objBE As clsCotizacionBE
                    'Dim drCotiDetQ As SqlClient.SqlDataReader
                    'Dim objBLDetQ As New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
                    Dim dttCotiDet As DataTable = objCotiDet.ConsultarListxIDCab(vintIDCab, False, True)
                    Dim objBEDetQ As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE
                    Dim ListaDetCotQ As List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
                    Dim dc As Dictionary(Of String, Double)
                    Dim dblCostoReal As Double, dblMargenAplicado As Double

                    For Each drCotiDet As DataRow In dttCotiDet.Rows
                        dblCostoReal = 0
                        dblMargenAplicado = 0
                        If drCotiDet("Especial") = True Then
                            dblCostoReal = drCotiDet("CostoReal")
                            dblMargenAplicado = drCotiDet("MargenAplicado")
                        End If


                        dc = objCoti.CalculoCotizacion(drCotiDet("IDServicio").ToString, drCotiDet("IDServicio_Det").ToString, _
                            drCotiDet("Anio").ToString, drCotiDet("Tipo").ToString, _
                            drCotiDet("IDTipoProv").ToString, vintPax, vintLiberados, vstrTipo_Lib, _
                            drCotiDet("MargenCotiCab").ToString, drCotiDet("TipoCambioCotiCab").ToString, _
                            dblCostoReal, dblMargenAplicado, vstrTipoPax, vdecRedondeo, dttCreaDttDetCotiServ)

                        ListaDetCotQ = New List(Of clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
                        objBEDetQ = New clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE

                        For Each dic As KeyValuePair(Of String, Double) In dc
                            If dic.Key = "CostoReal" Then objBEDetQ.CostoReal = dic.Value
                            If dic.Key = "Margen" Then objBEDetQ.Margen = dic.Value
                            If dic.Key = "CostoLiberado" Then objBEDetQ.CostoLiberado = dic.Value
                            If dic.Key = "MargenLiberado" Then objBEDetQ.MargenLiberado = dic.Value
                            If dic.Key = "MargenAplicado" Then objBEDetQ.MargenAplicado = dic.Value
                            If dic.Key = "TotImpto" Then objBEDetQ.TotImpto = dic.Value
                            If dic.Key = "Total" Then objBEDetQ.Total = dic.Value
                            If dic.Key = "SSCR" Then objBEDetQ.SSCR = dic.Value
                            If dic.Key = "SSMargen" Then objBEDetQ.SSMargen = dic.Value
                            If dic.Key = "SSCL" Then objBEDetQ.SSCL = dic.Value
                            If dic.Key = "SSML" Then objBEDetQ.SSMargenLiberado = dic.Value
                            If dic.Key = "SSTotal" Then objBEDetQ.SSTotal = dic.Value
                            If dic.Key = "STCR" Then objBEDetQ.STCR = dic.Value
                            If dic.Key = "STMargen" Then objBEDetQ.STMargen = dic.Value
                            If dic.Key = "STTotal" Then objBEDetQ.STTotal = dic.Value

                            If dic.Key = "CostoRealImpto" Then objBEDetQ.CostoRealImpto = dic.Value
                            If dic.Key = "CostoLiberadoImpto" Then objBEDetQ.CostoLiberadoImpto = dic.Value
                            If dic.Key = "MargenImpto" Then objBEDetQ.MargenImpto = dic.Value
                            If dic.Key = "MargenLiberadoImpto" Then objBEDetQ.MargenLiberadoImpto = dic.Value
                            If dic.Key = "SSCLImpto" Then objBEDetQ.SSCLImpto = dic.Value
                            If dic.Key = "SSCRImpto" Then objBEDetQ.SSCRImpto = dic.Value
                            If dic.Key = "SSMargenImpto" Then objBEDetQ.SSMargenImpto = dic.Value
                            If dic.Key = "SSMargenLiberadoImpto" Then objBEDetQ.SSMargenLiberadoImpto = dic.Value
                            If dic.Key = "STCRImpto" Then objBEDetQ.STCRImpto = dic.Value
                            If dic.Key = "STMargenImpto" Then objBEDetQ.STMargenImpto = dic.Value
                        Next

                        With objBEDetQ
                            If vstrAccion = "M" Then
                                'objBLDetQ = New clsCotizacionBT.clsDetalleCotizacionBT.clsDetalleCotizacionQuiebresBT
                                .IDDet_Q = intConsultarIDDet_QxIDCab_Q(vintIDCab_Q)

                                'drCotiDetQ.Read()
                                'If drCotiDetQ.HasRows Then
                                '    .IDDet_Q = drCotiDetQ("IDDet_Q")
                                'End If
                            End If

                            .IDDet = drCotiDet("IDDet").ToString
                            .IDCab_Q = vintIDCab_Q
                            '.CostoLiberadoImpto = .CostoLiberado * (1 + (vsglIGV / 100))
                            '.CostoRealImpto = .CostoReal * (1 + (vsglIGV / 100))
                            '.MargenImpto = .Margen * (1 + (vsglIGV / 100))
                            '.MargenLiberadoImpto = .MargenLiberado * (1 + (vsglIGV / 100))
                            '.SSCLImpto = .SSCL * (1 + (vsglIGV / 100))
                            '.SSCRImpto = .SSCR * (1 + (vsglIGV / 100))
                            '.SSMargenImpto = .SSMargen * (1 + (vsglIGV / 100))
                            '.SSMargenLiberadoImpto = .SSMargenLiberado * (1 + (vsglIGV / 100))
                            '.STCRImpto = .STCR * (1 + (vsglIGV / 100))
                            '.STMargenImpto = .STMargen * (1 + (vsglIGV / 100))
                            '.TotImpto = .CostoLiberadoImpto + .CostoRealImpto + .MargenImpto + .MargenLiberadoImpto '+ _
                            '.SSCLImpto(+.SSCRImpto + .SSMargenImpto + .SSMargenLiberadoImpto + .STCRImpto + .STMargenImpto)
                            .UserMod = vstrUser
                            .Accion = vstrAccion
                        End With
                        ListaDetCotQ.Add(objBEDetQ)

                        objBE = New clsCotizacionBE
                        objBE.ListaDetalleCotizacionQuiebres = ListaDetCotQ

                        InsertarActualizarEliminar(objBE, drCotiDet("IDDet").ToString)

                    Next

                    ContextUtil.SetComplete()

                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Function intConsultarIDDet_QxIDCab_Q(ByVal vintIDCab_Q As Int32) As Int32
                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDCab_Q", vintIDCab_Q)
                    dc.Add("@pIDDet_Q", 0)

                    Return objADO.ExecuteSPOutput("COTIDET_QUIEBRES_SelxIDCab_Q_IDDet_QOutput", dc)
                Catch ex As Exception
                    Throw

                End Try

            End Function
            Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDDet", vintIDDet)

                    objADO.ExecuteSP("COTIDET_QUIEBRES_DelxIDDet", dc)
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

        End Class



        <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsDetalleCotizacionDetServiciosBT

            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT

            Private Function blnExiste(ByVal vintIDDet As Int32, ByVal vintIDServicio_Det As Int32) As Boolean
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDDet", vintIDDet)
                    dc.Add("@IDServicio_Det", vintIDServicio_Det)
                    dc.Add("@pExiste", False)

                    Return objADO.ExecuteSPOutput("COTIDET_DETSERVICIOS_Sel_ExisteOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
            Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDDet", vintIDDet)

                    objADO.ExecuteSP("COTIDET_DETSERVICIOS_DelxIDDet", dc)
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub InsertarActualizar(ByVal BE As clsCotizacionBE, ByVal vintIDDet As Int32)
                Dim blnExist As Boolean
                Try
                    Dim dc As Dictionary(Of String, String)
                    For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE In BE.ListaDetalleCotizacionDetServicio

                        blnExist = blnExiste(vintIDDet, Item.IDServicio_Det)

                        dc = New Dictionary(Of String, String)

                        dc.Add("@IDDet", vintIDDet)
                        dc.Add("@IDServicio_Det", Item.IDServicio_Det)

                        dc.Add("@CostoLiberado", Item.CostoLiberado)
                        dc.Add("@CostoLiberadoImpto", Item.CostoLiberadoImpto)
                        dc.Add("@CostoReal", Item.CostoReal)
                        dc.Add("@CostoRealImpto", Item.CostoRealImpto)
                        dc.Add("@Margen", Item.Margen)
                        dc.Add("@MargenAplicado", Item.MargenAplicado)
                        dc.Add("@MargenImpto", Item.MargenImpto)
                        dc.Add("@MargenLiberado", Item.MargenLiberado)
                        dc.Add("@MargenLiberadoImpto", Item.MargenLiberadoImpto)
                        dc.Add("@SSCL", Item.SSCL)
                        dc.Add("@SSCLImpto", Item.SSCLImpto)
                        dc.Add("@SSCR", Item.SSCR)
                        dc.Add("@SSCRImpto", Item.SSCRImpto)
                        dc.Add("@SSMargen", Item.SSMargen)
                        dc.Add("@SSMargenImpto", Item.SSMargenImpto)
                        dc.Add("@SSMargenLiberado", Item.SSMargenLiberado)
                        dc.Add("@SSMargenLiberadoImpto", Item.SSMargenLiberadoImpto)
                        dc.Add("@SSTotal", Item.SSTotal)
                        dc.Add("@STCR", Item.STCR)
                        dc.Add("@STCRImpto", Item.STCRImpto)
                        dc.Add("@STMargen", Item.STMargen)
                        dc.Add("@STMargenImpto", Item.STMargenImpto)
                        dc.Add("@STTotal", Item.STTotal)
                        dc.Add("@Total", Item.Total)
                        dc.Add("@TotImpto", Item.TotImpto)
                        dc.Add("@UserMod", Item.UserMod)
                        If Not blnExist Then
                            objADO.ExecuteSP("COTIDET_DETSERVICIOS_Ins", dc)
                        Else
                            objADO.ExecuteSP("COTIDET_DETSERVICIOS_Upd", dc)
                        End If


                    Next
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

        End Class

        <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsDetalleCotizacionDescripServicioBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT


            Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE, ByVal vintIDDet As Integer)
                Dim dc As Dictionary(Of String, String)

                Try
                    For Each Item As clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE In BE.ListaDetalleCotizacionDescripServicio
                        If vintIDDet = Item.IDDet Then
                            dc = New Dictionary(Of String, String)
                            If Item.Accion = "M" Or Item.Accion = "N" Then

                                dc.Add("@IDDet", Item.IDDet)
                                dc.Add("@IDIDioma", Item.IDIdioma)
                                'dc.Add("@DescCorta", Item.DescCorta)
                                dc.Add("@DescLarga", Item.DescLarga)
                                dc.Add("@UserMod", Item.UserMod)

                                If Item.Accion = "N" Then
                                    objADO.ExecuteSP("COTIDET_DESCRIPSERV_Ins", dc)
                                ElseIf Item.Accion = "M" Then
                                    objADO.ExecuteSP("COTIDET_DESCRIPSERV_Upd", dc)

                                End If
                            ElseIf Item.Accion = "B" Then
                                dc.Add("@IDDet", Item.IDDet)
                                dc.Add("@IDIDioma", Item.IDIdioma)
                                objADO.ExecuteSP("COTIDET_DESCRIPSERV_Del", dc)
                            End If
                        End If
                    Next

                    ContextUtil.SetComplete()

                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDDet", vintIDDet)

                    objADO.ExecuteSP("COTIDET_DESCRIPSERV_DelxIDDet", dc)
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

        End Class
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCotizacionPaxBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)

            Dim dc As Dictionary(Of String, String)
            Dim objDetPax As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
            Try
                For Each Item As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        If Item.Accion = "M" Then dc.Add("@IDPax", Item.IDPax)
                        If Item.Accion = "N" Then
                            If BE.IdCab <> 0 Then
                                dc.Add("@IDCab", BE.IdCab)
                            Else
                                dc.Add("@IDCab", Item.IDCab)
                            End If
                        End If


                        dc.Add("@IDIdentidad", Item.IDIdentidad)
                        dc.Add("@NumIdentidad", Item.NumIdentidad)
                        dc.Add("@Nombres", Item.Nombres)
                        dc.Add("@Apellidos", Item.Apellidos)
                        dc.Add("@Titulo", Item.Titulo)
                        dc.Add("@FecNacimiento", Item.FecNacimiento)
                        dc.Add("@IDNacionalidad", Item.IDNacionalidad)
                        dc.Add("@Peso", Item.Peso)
                        dc.Add("@ObservEspecial", If(Item.ObservEspecial Is Nothing, "", Item.ObservEspecial))
                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            dc.Add("@pIDPax", 0)
                            Dim intIDPax As Integer = objADO.ExecuteSPOutput("COTIPAX_Ins", dc)
                            If Not BE.ListaDetPax Is Nothing Then
                                objDetPax.InsertarxIDPax(Item.IDPax, intIDPax, BE)
                            End If
                        ElseIf Item.Accion = "M" Then
                            'dc.Add("@Total", Item.Total)
                            'dc.Add("@SSTotal", Item.SSTotal)
                            'dc.Add("@STTotal", Item.STTotal)

                            objADO.ExecuteSP("COTIPAX_Upd", dc)
                        End If
                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDPax", Item.IDPax)


                        objADO.ExecuteSP("COTIDET_PAX_DelxIDPax", dc)
                        objADO.ExecuteSP("COTIPAX_Del", dc)
                    End If
                Next


                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub ActualizarTotales(ByVal vintIDPax As Int32, ByVal vintIDCab As Int32, _
                                     ByVal vintIDDet As Int32, ByVal vblnSuma As Boolean, _
                                     ByVal vstrIDTipoProv As String, ByVal vstrUserMod As String)

            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDPax", vintIDPax)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDDet", vintIDDet)
                dc.Add("@bSuma", vblnSuma)
                dc.Add("@IDTipoProv", vstrIDTipoProv)
                dc.Add("@UserMod", vstrUserMod)

                objADO.ExecuteSP("COTIPAX_Upd_Totales", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Function ConsultarListxIDCabLvw(ByVal vintIDCab As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)

                Return objADO.GetDataTable("COTIPAX_Sel_xIDCab_Lvw", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        Public Function ConsultarListxIDCab(ByVal vintIDCab As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCab", vintIDCab)


                Return objADO.GetDataTable("COTIPAX_Sel_ListxIDCab", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function
        Public Function ConsultarDetxIDDetLvw(ByVal vintIDDet As Int32) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDDet", vintIDDet)

                Return objADO.GetDataTable("COTIDET_PAX_Sel_xIDDet_Lvw", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

        <ComVisible(True)> _
        <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
        Public Class clsCotizacionDetPaxBT
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDT
            Private Function blnExiste(ByVal vintIDDet As Int32, ByVal vintIDPax As Int32) As Boolean
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDDet", vintIDDet)
                    dc.Add("@IDPax", vintIDPax)
                    dc.Add("@pExiste", False)

                    Return objADO.ExecuteSPOutput("COTIDET_PAX_Sel_ExisteOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
            Public Sub InsertarEliminar(ByVal BE As clsCotizacionBE, ByVal vintIDDet As Integer, ByVal vblnTodos As Boolean, Optional ByVal vblnRecalculo As Boolean = False)

                Dim intIDDet As Integer
                Dim dc As Dictionary(Of String, String)
                Dim objPax As New clsCotizacionBT.clsCotizacionPaxBT
                Try
                    For Each Item As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                        If IsNumeric(Item.IDDet) Then
                            If Item.IDDet = vintIDDet.ToString Or vblnTodos = True Then
                                dc = New Dictionary(Of String, String)
                                If vintIDDet <> 0 Then
                                    If Item.Accion = "M" Then
                                        intIDDet = Item.IDDet
                                    Else
                                        intIDDet = vintIDDet
                                    End If
                                Else
                                    intIDDet = Item.IDDet
                                End If
                                If Not vblnRecalculo Then
                                    If Item.Selecc Then
                                        Dim blnConfirmar As Boolean = True
                                        If Not IsNumeric(Item.IDPax) Then
                                            blnConfirmar = False
                                        End If
                                        If blnConfirmar Then
                                            If Not blnExiste(intIDDet, Item.IDPax) Then
                                                'objPax.ActualizarTotales(Item.IDPax, BE.IdCab, intIDDet, True, BE.UserMod)

                                                dc.Add("@IDPax", Item.IDPax)
                                                dc.Add("@IDDet", intIDDet)
                                                dc.Add("@UserMod", Item.UserMod)

                                                objADO.ExecuteSP("COTIDET_PAX_Ins", dc)

                                            End If
                                        Else
                                            'dc.Add("@IDPax", Item.IDPax)
                                            'dc.Add("@IDDet", intIDDet)
                                            'dc.Add("@UserMod", Item.UserMod)

                                            'objADO.ExecuteSP("COTIDET_PAX_Ins", dc)

                                        End If
                                    Else
                                        'objPax.ActualizarTotales(Item.IDPax, BE.IdCab, intIDDet, False, BE.UserMod)

                                        dc.Add("@IDPax", Item.IDPax)
                                        dc.Add("@IDDet", intIDDet)
                                        dc.Add("@UserMod", Item.UserMod)
                                        objADO.ExecuteSP("COTIDET_PAX_Del", dc)

                                    End If
                                Else
                                    If IsNumeric(Item.IDPax) Then
                                        If Item.Selecc Then
                                            objPax.ActualizarTotales(Item.IDPax, BE.IdCab, intIDDet, True, Item.IDTipoProv, BE.UserMod)
                                        Else
                                            objPax.ActualizarTotales(Item.IDPax, BE.IdCab, intIDDet, False, Item.IDTipoProv, BE.UserMod)
                                        End If
                                    End If
                                End If
                            End If
                        End If
                    Next


                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub InsertarxIDPax(ByVal vstrIDPax As String, ByVal vintIDPax As Integer, ByVal BE As clsCotizacionBE)
                Dim dc As Dictionary(Of String, String)

                Try
                    For Each Item As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE In BE.ListaDetPax
                        If Item.IDPax = vstrIDPax And Item.Accion = "N" Then
                            dc = New Dictionary(Of String, String)
                            dc.Add("@IDPax", vintIDPax)
                            dc.Add("@IDDet", Item.IDDet)
                            dc.Add("@UserMod", Item.UserMod)

                            objADO.ExecuteSP("COTIDET_PAX_Ins", dc)
                        End If
                    Next
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub InsertarxIDDet(ByVal vintIDCab As Int32, ByVal vintIDDet As Int32, ByVal vstrUserMod As String)
                Try

                    Dim objBLPax As New clsCotizacionPaxBT
                    Dim dttPax As DataTable = objBLPax.ConsultarListxIDCabLvw(vintIDCab)
                    Dim objBLDetPax As New clsCotizacionBT.clsCotizacionPaxBT.clsCotizacionDetPaxBT
                    Dim objBEDetPax As clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE
                    Dim ListaDetPax As New List(Of clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE)
                    Dim BE As New clsCotizacionBE

                    For Each dr As DataRow In dttPax.Rows
                        objBEDetPax = New clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE
                        With objBEDetPax
                            .IDDet = vintIDDet
                            .IDPax = dr("IDPax")
                            .Selecc = True
                            .UserMod = vstrUserMod
                        End With
                        ListaDetPax.Add(objBEDetPax)
                    Next
                    BE.ListaDetPax = ListaDetPax
                    BE.UserMod = vstrUserMod

                    objBLDetPax.InsertarEliminar(BE, 0, True)


                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try

            End Sub

            Public Sub EliminarxIDDet(ByVal vintIDDet As Int32)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDDet", vintIDDet)

                    objADO.ExecuteSP("COTIDET_PAX_DelxIDDet", dc)
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub

            Public Sub EliminarxIDPax(ByVal vintIDPax As Int32)
                Try
                    Dim dc As New Dictionary(Of String, String)

                    dc.Add("@IDPax", vintIDPax)

                    objADO.ExecuteSP("COTIDET_PAX_DelxIDPax", dc)
                    ContextUtil.SetComplete()
                Catch ex As Exception
                    ContextUtil.SetAbort()
                    Throw
                End Try
            End Sub
        End Class

        'jorge
        Public Sub ActualizarPaxReservas_Excel(ByVal BE As clsCotizacionBE)
            Dim dc As Dictionary(Of String, String)

            Try

                For Each Item As clsCotizacionBE.clsCotizacionPaxBE In BE.ListaPax

                    dc = New Dictionary(Of String, String)

                    '                    @IDPax int,
                    '@Nombres varchar(50),
                    '@Apellidos varchar(50),
                    '@IDIdentidad char(3),
                    '@NumIdentidad varchar(15),
                    '@IDNacionalidad char(3),
                    '@FecNacimiento smalldatetime,
                    '@UserMod char(4)

                    dc.Add("@IDPax", Item.IDPax)
                    dc.Add("@Nombres", Item.Nombres)
                    dc.Add("@Apellidos", Item.Apellidos)
                    dc.Add("@IDIdentidad", Item.IDIdentidad)
                    dc.Add("@NumIdentidad", Item.NumIdentidad)
                    dc.Add("@IDNacionalidad", Item.IDNacionalidad)
                    dc.Add("@FecNacimiento", Item.FecNacimiento)
                    dc.Add("@UserMod", Item.UserMod)

                    objADO.ExecuteSP("COTIPAX_UPD_EXCEL", dc)
                Next


                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Sub
    End Class


    <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsCategoriasHotelesBT

        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
            Dim dc As Dictionary(Of String, String)
            Try
                For Each Item As clsCotizacionBE.clsCategoriasHotelesBE In BE.ListaCategHoteles
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        dc.Add("@IDCab", BE.IdCab)
                        dc.Add("@IDUbigeo", Item.IDUbigeo)
                        dc.Add("@Categoria", Item.Categoria)
                        dc.Add("@IDProveedor", Item.IDProveedor)
                        dc.Add("@IDServicio_Det", Item.IDServicio_Det)
                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            dc.Add("@NroOrd", Item.NroOrd)

                            ''Poner verificacion de llave antes de grabar
                            If Not blnExiste(BE.IdCab, Item.IDUbigeo, Item.Categoria, Item.IDProveedor) Then
                                objADO.ExecuteSP("COTI_CATEGORIAHOTELES_Ins", dc)
                            End If
                        ElseIf Item.Accion = "M" Then
                            dc.Add("@IDProveedorUpd", Item.IDProveedorUpd)
                            objADO.ExecuteSP("COTI_CATEGORIAHOTELES_Upd", dc)
                        End If

                    ElseIf Item.Accion = "B" Then
                        dc.Add("@IDCab", BE.IdCab)
                        dc.Add("@Categoria", Item.Categoria)

                        objADO.ExecuteSP("COTI_CATEGORIAHOTELES_Del", dc)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function blnExiste(ByVal vintIDCab As Integer, ByVal vstrIDUbigeo As String, _
                                   ByVal vstrCategoria As String, ByVal vstrIDProveedor As String) As Boolean
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDUbigeo", vstrIDUbigeo)
                dc.Add("@Categoria", vstrCategoria)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@pExiste", False)

                Return objADO.ExecuteSPOutput("COTI_CATEGORIAHOTELES_Sel_ExisteOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Sub InsertarxIDCab(ByVal vintIDCab As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)

                objADO.ExecuteSP("COTI_CATEGORIAHOTELES_Ins_xIDCab", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try

        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCotizacionIncluidoBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@IDIncluido", BE.IDIncluido)
                    .Add("@Orden", BE.Orden)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("COTI_INCLUIDO_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@IDIncluido", BE.IDIncluido)
                    .Add("@Orden", BE.Orden)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("COTI_INCLUIDO_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal vintIDCab As Integer, ByVal vintIDIncluido As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCAB", vintIDCab)
                dc.Add("@IDIncluido", vintIDIncluido)

                objADO.ExecuteSP("COTI_INCLUIDO_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
            Try
                For Each Item As clsCotizacionBE.clsCotizacionIncluidoBE In BE.ListadeIDIncluido
                    If Item.Accion = "N" Then
                        If Not blnExiste(Item.IDCab, Item.IDIncluido) Then
                            Insertar(Item)
                        Else
                            Actualizar(Item)
                        End If
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDCab, Item.IDIncluido)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function blnExiste(ByVal vintIDCab As Integer, ByVal vintIDIncluido As Int32) As Boolean
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDIncluido", vintIDIncluido)
                dc.Add("@pExiste", False)

                Return objADO.ExecuteSPOutput("COTI_INCLUIDO_Sel_ExisteOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCotizacionVuelosBT
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionVuelosBE, ByVal intIDCAB As Integer, ByVal strCotizacion As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCAB", BE.IdCab)
                dc.Add("@Cotizacion", BE.Cotizacion)
                dc.Add("@Servicio", BE.Servicio)
                dc.Add("@Iddet", BE.IDDet)
                dc.Add("@NombrePax", BE.NombrePax)
                dc.Add("@Ruta", BE.Ruta)
                dc.Add("@NumBoleto", BE.NumBoleto)
                dc.Add("@RutaD", BE.RutaD)
                dc.Add("@RutaO", BE.RutaO)
                dc.Add("@Vuelo", BE.Vuelo)
                dc.Add("@FecEmision", BE.FecEmision)
                dc.Add("@FecDocumento", BE.FecDocumento)
                dc.Add("@FecSalida", BE.FecSalida)
                dc.Add("@FecRetorno", BE.FecRetorno)
                dc.Add("@Salida", BE.Salida)
                dc.Add("@HrRecojo", BE.HrRecojo)
                dc.Add("@PaxC", BE.PaxC)
                dc.Add("@PCotizado", BE.PCotizado)
                dc.Add("@TCotizado", BE.TCotizado)
                dc.Add("@PaxV", BE.PaxV)
                dc.Add("@PVolado", BE.PVolado)
                dc.Add("@TVolado", BE.TVolado)
                dc.Add("@Status", BE.Status)
                dc.Add("@Comentario", BE.Comentario)
                dc.Add("@Llegada", BE.Llegada)
                dc.Add("@Emitido", BE.Emitido)
                dc.Add("@Identificador", BE.TipoTransporte)
                dc.Add("@CodReserva", BE.CodReserva)
                dc.Add("@Tarifa", BE.Tarifa)
                dc.Add("@PTA", BE.PTA)
                dc.Add("@ImpuestosExt", BE.ImpuestosExt)
                dc.Add("@Penalidad", BE.Penalidad)
                dc.Add("@IGV", BE.IGV)
                dc.Add("@Otros", BE.Otros)
                dc.Add("@PorcComm", BE.PorcComm)
                dc.Add("@MontoContado", BE.MontoContado)
                dc.Add("@MontoTarjeta", BE.MontoTarjeta)
                dc.Add("@IdTarjCred", BE.IDTarjetaCredito)
                dc.Add("@NumeroTarjeta", BE.NumeroTarjeta)
                dc.Add("@Aprobacion", BE.Aprobacion)
                dc.Add("@PorcOver", BE.PorcOver)
                dc.Add("@PorcOverInCom", BE.PorcOverInCom)
                dc.Add("@MontComm", BE.MontComm)
                dc.Add("@MontCommOver", BE.MontCommOver)
                dc.Add("@IGVComm", BE.IGVComm)
                dc.Add("@IGVCommOver", BE.IGVCommOver)
                dc.Add("@PorcTarifa", BE.PorcTarifa)
                dc.Add("@NetoPagar", BE.NetoPagar)
                dc.Add("@Descuento", BE.Descuento)
                dc.Add("@IdLineaA", BE.IDLineaA)
                dc.Add("@MontoFEE", BE.MontoFEE)
                dc.Add("@IGVFEE", BE.IGVFEE)
                dc.Add("@TotalFEE", BE.TotalFEE)
                dc.Add("@EmitidoSetours", BE.EmitidoSetours)
                dc.Add("@EmitidoOperador", BE.EmitidoOperador)
                dc.Add("@IDProveedorOperador", BE.IDProveedorOperador)
                dc.Add("@CostoOperador", BE.CostoOperador)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("COTIVUELOS_Ins", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionVuelosBE, ByVal intIDCAB As Integer, ByVal strCotizacion As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCAB", BE.IdCab)
                dc.Add("@Cotizacion", BE.Cotizacion)
                dc.Add("@Servicio", BE.Servicio)
                dc.Add("@ID", BE.ID)
                dc.Add("@NombrePax", BE.NombrePax)
                dc.Add("@Ruta", BE.Ruta)
                dc.Add("@NumBoleto", BE.NumBoleto)
                dc.Add("@RutaD", BE.RutaD)
                dc.Add("@RutaO", BE.RutaO)
                dc.Add("@Vuelo", BE.Vuelo)
                dc.Add("@FecEmision", BE.FecEmision)
                dc.Add("@FecDocumento", BE.FecDocumento)
                dc.Add("@FecSalida", BE.FecSalida)
                dc.Add("@FecRetorno", BE.FecRetorno)
                dc.Add("@Salida", BE.Salida)
                dc.Add("@HrRecojo", BE.HrRecojo)
                dc.Add("@PaxC", BE.PaxC)
                dc.Add("@PCotizado", BE.PCotizado)
                dc.Add("@TCotizado", BE.TCotizado)
                dc.Add("@PaxV", BE.PaxV)
                dc.Add("@PVolado", BE.PVolado)
                dc.Add("@TVolado", BE.TVolado)
                dc.Add("@Status", BE.Status)
                dc.Add("@Comentario", BE.Comentario)
                dc.Add("@Llegada", BE.Llegada)
                dc.Add("@Emitido", BE.Emitido)
                dc.Add("@Identificador", BE.TipoTransporte)
                dc.Add("@CodReserva", BE.CodReserva)
                dc.Add("@Tarifa", BE.Tarifa)
                dc.Add("@PTA", BE.PTA)
                dc.Add("@ImpuestosExt", BE.ImpuestosExt)
                dc.Add("@Penalidad", BE.Penalidad)
                dc.Add("@IGV", BE.IGV)
                dc.Add("@Otros", BE.Otros)
                dc.Add("@PorcComm", BE.PorcComm)
                dc.Add("@MontoContado", BE.MontoContado)
                dc.Add("@MontoTarjeta", BE.MontoTarjeta)
                dc.Add("@IdTarjCred", BE.IDTarjetaCredito)
                dc.Add("@NumeroTarjeta", BE.NumeroTarjeta)
                dc.Add("@Aprobacion", BE.Aprobacion)
                dc.Add("@PorcOver", BE.PorcOver)
                dc.Add("@PorcOverInCom", BE.PorcOverInCom)
                dc.Add("@MontComm", BE.MontComm)
                dc.Add("@MontCommOver", BE.MontCommOver)
                dc.Add("@IGVComm", BE.IGVComm)
                dc.Add("@IGVCommOver", BE.IGVCommOver)
                dc.Add("@PorcTarifa", BE.PorcTarifa)
                dc.Add("@NetoPagar", BE.NetoPagar)
                dc.Add("@Descuento", BE.Descuento)
                dc.Add("@IdLineaA", BE.IDLineaA)
                dc.Add("@MontoFEE", BE.MontoFEE)
                dc.Add("@IGVFEE", BE.IGVFEE)
                dc.Add("@TotalFEE", BE.TotalFEE)
                dc.Add("@EmitidoSetours", BE.EmitidoSetours)
                dc.Add("@EmitidoOperador", BE.EmitidoOperador)
                dc.Add("@IDProveedorOperador", BE.IDProveedorOperador)
                dc.Add("@CostoOperador", BE.CostoOperador)
                dc.Add("@UserMod", BE.UserMod)

                objADO.ExecuteSP("COTIVUELOS_Upd", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub Eliminar(ByVal intID As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@ID", intID)
                objADO.ExecuteSP("COTIVUELOS_Del", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub EliminarxIDDet(ByVal intIDDet As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDet", intIDDet)
                objADO.ExecuteSP("COTIVUELOS_DelxIDDet", dc)

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
        Public Sub InsertarEliminar(ByVal BE As clsCotizacionBE)

            Dim dc As Dictionary(Of String, String)
            Try

                For Each Item As clsCotizacionBE.clsCotizacionVuelosBE In BE.ListaCotizacionVuelo
                    dc = New Dictionary(Of String, String)
                    If Item.Accion = "M" Or Item.Accion = "N" Then
                        'Dim n As Integer = Item.ID
                        dc.Add("@IDCAB", BE.IdCab)
                        dc.Add("@Cotizacion", BE.Cotizacion)
                        dc.Add("@NombrePax", Item.NombrePax)
                        dc.Add("@Ruta", Item.Ruta)
                        dc.Add("@NumBoleto", Item.NumBoleto)
                        dc.Add("@RutaD", Item.RutaD)
                        dc.Add("@RutaO", Item.RutaO)
                        dc.Add("@Vuelo", Item.Vuelo)
                        dc.Add("@FecEmision", Item.FecEmision)
                        dc.Add("@FecDocumento", Item.FecDocumento)
                        dc.Add("@FecSalida", Item.FecSalida)
                        dc.Add("@FecRetorno", Item.FecRetorno)
                        dc.Add("@Salida", Item.Salida)
                        dc.Add("@HrRecojo", Item.HrRecojo)
                        dc.Add("@PaxC", Item.PaxC)
                        dc.Add("@PCotizado", Item.PCotizado)
                        dc.Add("@TCotizado", Item.TCotizado)
                        dc.Add("@PaxV", Item.PaxV)
                        dc.Add("@PVolado", Item.PVolado)
                        dc.Add("@TVolado", Item.TVolado)
                        dc.Add("@Status", Item.Status)
                        dc.Add("@Comentario", Item.Comentario)
                        dc.Add("@Llegada", Item.Llegada)
                        dc.Add("@Emitido", Item.Emitido)
                        dc.Add("@Identificador", Item.TipoTransporte)
                        dc.Add("@CodReserva", Item.CodReserva)
                        dc.Add("@Tarifa", Item.Tarifa)
                        dc.Add("@PTA", Item.PTA)
                        dc.Add("@ImpuestosExt", Item.ImpuestosExt)
                        dc.Add("@Penalidad", Item.Penalidad)
                        dc.Add("@IGV", Item.IGV)
                        dc.Add("@Otros", Item.Otros)
                        dc.Add("@PorcComm", Item.PorcComm)
                        dc.Add("@MontoContado", Item.MontoContado)
                        dc.Add("@MontoTarjeta", Item.MontoTarjeta)
                        dc.Add("@IdTarjCred", Item.IDTarjetaCredito)
                        dc.Add("@NumeroTarjeta", Item.NumeroTarjeta)
                        dc.Add("@Aprobacion", Item.Aprobacion)
                        dc.Add("@PorcOver", Item.PorcOver)
                        dc.Add("@PorcOverInCom", Item.PorcOverInCom)
                        dc.Add("@MontComm", Item.MontComm)
                        dc.Add("@MontCommOver", Item.MontCommOver)
                        dc.Add("@IGVComm", Item.IGVComm)
                        dc.Add("@IGVCommOver", Item.IGVCommOver)
                        dc.Add("@PorcTarifa", Item.PorcTarifa)
                        dc.Add("@NetoPagar", Item.NetoPagar)
                        dc.Add("@Descuento", Item.Descuento)
                        dc.Add("@IdLineaA", Item.IDLineaA)
                        dc.Add("@MontoFEE", Item.MontoFEE)
                        dc.Add("@IGVFEE", Item.IGVFEE)
                        dc.Add("@TotalFEE", Item.TotalFEE)
                        dc.Add("@UserMod", Item.UserMod)

                        If Item.Accion = "N" Then
                            If IsNumeric(Item.IDDet) Then
                                dc.Add("@Iddet", Item.IDDet)
                                objADO.ExecuteSP("COTIVUELOS_Ins", dc)
                            End If
                        ElseIf Item.Accion = "M" Then
                            If IsNumeric(Item.ID) Then
                                dc.Add("@ID", Item.ID)
                                objADO.ExecuteSP("COTIVUELOS_Upd", dc)
                            End If
                        End If
                    ElseIf Item.Accion = "B" Then
                        If IsNumeric(Item.ID) Then
                            dc.Add("@ID", Item.ID)
                            objADO.ExecuteSP("COTIVUELOS_Del", dc)
                        End If
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCotizacionIncluidoObservBT

        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@IDIncluido", BE.IDIncluido)
                    .Add("@Orden", BE.Orden)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("COTI_INCLUIDO_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@IDIncluido", BE.IDIncluido)
                    .Add("@Orden", BE.Orden)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("COTI_INCLUIDO_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal vintIDCab As Integer, ByVal vintIDIncluido As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCAB", vintIDCab)
                dc.Add("@IDIncluido", vintIDIncluido)

                objADO.ExecuteSP("COTI_INCLUIDO_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
            Try
                For Each Item As clsCotizacionBE.clsCotizacionIncluidoObservBE In BE.ListaIncluidosObserv
                    If Item.Accion = "N" Then
                        If Not blnExiste(Item.IDCab, Item.IDIncluido) Then
                            Insertar(Item)
                        Else
                            Actualizar(Item)
                        End If
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDCab, Item.IDIncluido)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function blnExiste(ByVal vintIDCab As Integer, ByVal vintIDIncluido As Int32) As Boolean
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDIncluido", vintIDIncluido)
                dc.Add("@pExiste", False)

                Return objADO.ExecuteSPOutput("COTI_INCLUIDO_Sel_ExisteOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
    Public Class clsCotizacionNoIncluidoBT

        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDT

        Private Sub Insertar(ByVal BE As clsCotizacionBE.clsCotizacionIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@IDIncluido", BE.IDIncluido)
                    .Add("@Orden", BE.Orden)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("COTI_INCLUIDO_Ins", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Actualizar(ByVal BE As clsCotizacionBE.clsCotizacionIncluidoBE)
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDCAB", BE.IDCab)
                    .Add("@IDIncluido", BE.IDIncluido)
                    .Add("@Orden", BE.Orden)
                    .Add("@UserMod", BE.UserMod)
                End With
                objADO.ExecuteSP("COTI_INCLUIDO_Upd", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Sub Eliminar(ByVal vintIDCab As Integer, ByVal vintIDIncluido As Integer)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCAB", vintIDCab)
                dc.Add("@IDIncluido", vintIDIncluido)

                objADO.ExecuteSP("COTI_INCLUIDO_Del", dc)
                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Public Sub InsertarActualizarEliminar(ByVal BE As clsCotizacionBE)
            Try
                For Each Item As clsCotizacionBE.clsCotizacionNoIncluidoBE In BE.ListadeIDNoIncluidos
                    If Item.Accion = "N" Then
                        If Not blnExiste(Item.IDCab, Item.IDIncluido) Then
                            Insertar(Item)
                        Else
                            Actualizar(Item)
                        End If
                    ElseIf Item.Accion = "B" Then
                        Eliminar(Item.IDCab, Item.IDIncluido)
                    End If
                Next

                ContextUtil.SetComplete()
            Catch ex As Exception
                ContextUtil.SetAbort()
                Throw
            End Try
        End Sub

        Private Function blnExiste(ByVal vintIDCab As Integer, ByVal vintIDIncluido As Int32) As Boolean
            Try
                Dim dc As New Dictionary(Of String, String)

                dc.Add("@IDCab", vintIDCab)
                dc.Add("@IDIncluido", vintIDIncluido)
                dc.Add("@pExiste", False)

                Return objADO.ExecuteSPOutput("COTI_INCLUIDO_Sel_ExisteOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
End Class