﻿Imports ComDataAccessD2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Imports ComSEToursBE2

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsClienteBN
    Inherits ServicedComponent
    Dim objADO As New clsDataAccessDN
    Public Function ConsultarList(ByVal vstrIDCliente As String, _
                                  ByVal vstrRazonComercial As String, _
                                  ByVal vstrIDPais As String, _
                                  ByVal vstrContacto As String, _
                                  ByVal vbytTop As Byte, _
                                  ByVal vstrTipo As String, _
                                  ByVal vstrCoTipoVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            'dc.Add("@NumIdentidad", vstrNumDocumento)
            dc.Add("@IDCliente", vstrIDCliente)
            dc.Add("@RazonComercial", vstrRazonComercial)
            dc.Add("@IDPais", vstrIDPais)
            dc.Add("@Contacto", vstrContacto)
            dc.Add("@Top", vbytTop)
            dc.Add("@Tipo", vstrTipo)
            dc.Add("@CoTipoVenta", vstrCoTipoVenta)

            Return objADO.GetDataTable(True, "MACLIENTES_Sel_List", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function

    Public Function ConsultarListAddIn(ByVal vstrRazonComercial As String, _
                                  ByVal vstrIDPais As String, _
                                  ByVal vstrTipo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@RazonComercial", vstrRazonComercial)
            dc.Add("@IDPais", vstrIDPais)
            dc.Add("@Tipo", vstrTipo)
            'dc.Add("@CoTipoVenta", vstrCoTipoVenta)

            Return objADO.GetDataTable(True, "MACLIENTES_Sel_List_AddIn", dc)
        Catch ex As Exception
            Throw

        End Try

    End Function

    Public Function GeneracionExcel(ByVal vstrRazonComercial As String, _
                                    ByVal vstrIDPais As String, _
                                    ByVal vstrContacto As String, _
                                    ByVal vbytTop As Byte, _
                                    ByVal vstrTipo As String, _
                                    ByVal vstrCoTipoVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@RazonComercial", vstrRazonComercial)
            dc.Add("@IDPais", vstrIDPais)
            dc.Add("@Contacto", vstrContacto)
            dc.Add("@Top", vbytTop)
            dc.Add("@Tipo", vstrTipo)
            dc.Add("@CoTipoVenta", vstrCoTipoVenta)

            Return objADO.GetDataTable(True, "MACLIENTES_Sel_GeneracionExcel", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarListAyuda(ByVal vstrRazonSocial As String, _
                                       ByVal vstrCoTipoVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@CoTipoVenta", vstrCoTipoVenta)

            Return objADO.GetDataTable(True, "MACLIENTES_Sel_ListAyuda", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strIDClientexDominio(ByVal vstrDominio As String) As String
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@dominio", vstrDominio)
            dc.Add("@pIDCliente", "")

            Return objADO.ExecuteSPOutput("MACLIENTES_Sel_OutPutxDominio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarPk(ByVal vstrIDCliente As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCliente", vstrIDCliente)

        Dim strSql As String = "MACLIENTES_Sel_Pk"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function Consultar_x_Correo(ByVal vstrEmail As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@Email", vstrEmail)

        Dim strSql As String = "MACLIENTES_Sel_List_Correo"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function ConsultarxCtaContable(ByVal vstrIDCliente As String, ByVal vstrCtaContable As String, ByVal vintIDCAB As Int32) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCliente", vstrIDCliente)
        dc.Add("@CtaContable", vstrCtaContable)
        dc.Add("@IDCAB", vintIDCAB)

        Dim strSql As String = "MACLIENTES_SelxCtaContable"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function ConsultarxCtaContable_CeCos(ByVal vstrIDCliente As String, ByVal vstrCtaContable As String, ByVal vintIDCAB As Int32) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDCliente", vstrIDCliente)
        dc.Add("@CtaContable", vstrCtaContable)
        dc.Add("@IDCAB", vintIDCAB)

        Dim strSql As String = "MACLIENTES_SelxCtaContable_CentroCostos"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@ID", vstrID)
            dc.Add("@Descripcion", vstrDescripcion)

            Return objADO.GetDataTable(True, "MACLIENTES_Sel_Lvw", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    'Public Function strConsultarxCorreo(ByVal vstrCorreo As String) As String
    '    Dim dc As New Dictionary(Of String, String)
    '    Try
    '        With dc
    '            .Add("@Correo", vstrCorreo)
    '            .Add("@pIDCliente", "")
    '        End With

    '        Return objADO.GetSPOutputValue("MACLIENTES_SelIDxCorreo_Output", dc)
    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Function

    Public Function ConsultarRpt(ByVal vstrRazonSocial As String, _
                                  ByVal vstrIDPais As String, _
                                  ByVal vstrContacto As String, _
                                  ByVal vbytTop As Byte, _
                                  ByVal vstrCoTipoVenta As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@IDPais", vstrIDPais)
            dc.Add("@Contacto", vstrContacto)
            dc.Add("@Top", vbytTop)
            dc.Add("@CoTipoVenta", vstrCoTipoVenta)

            Return objADO.GetDataTable(True, "MACLIENTES_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarRptDistribucion(ByVal vstrIDVendedor As String) As Data.DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDvendedor", vstrIDVendedor)
            Return objADO.GetDataTable(True, "MACLIENTES_Sel_RptDistribClientes", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptGeneral(ByVal vstrPersona As String, ByVal vstrTipo As String, ByVal vstrIDVendedor _
                                        As String, ByVal vstrIDPais As String, ByVal vstrIDCiudad As String, _
                                        ByVal vbytDomiciliado As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@Persona", vstrPersona)
            dc.Add("@Tipo", vstrTipo)
            dc.Add("@IDVendedor", vstrIDVendedor)
            dc.Add("@IDPais", vstrIDPais)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@Domiciliado", vbytDomiciliado)
            Return objADO.GetDataTable(True, "MACLIENTES_sel_RptGeneral", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarRptFicha(ByVal vstrIDCliente As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCliente", vstrIDCliente)
            Return objADO.GetDataTable(True, "MACLIENTES_Sel_Ficha", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    'jorge
    Public Function ConsultarRptComparaVentas(ByVal vstrIDCliente As String, _
                                              ByVal vBytTop As Byte, _
                                              ByVal vstrAnio As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCliente", vstrIDCliente)
            dc.Add("@Top", vBytTop)
            dc.Add("@Anio", vstrAnio)

            Dim strSql As String = "ReporteComparaVentasCliente"
            Return objADO.GetDataTable(True, strSql, dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarSbRptComparaVentas(ByVal vstrIDCliente As String, _
                                                ByVal vBytTop As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDCliente", vstrIDCliente)
            dc.Add("@Top", vBytTop)

            Dim strSql As String = "ReporteComparaVentasCliente_SubInfxClientexMes"
            Return objADO.GetDataTable(True, strSql, dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptComparaVentasEjecutiva(ByVal vstrIDUsuario As String, ByVal vstrAnio As String, _
                                                       ByVal vstrEstado As String, vBytTopClientes As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUsuarioVenta", vstrIDUsuario)
                .Add("@Anio", vstrAnio)
                .Add("@Estado", vstrEstado)
                .Add("@TopCliente", vBytTopClientes)
            End With

            Return objADO.GetDataTable(True, "ReporteComparaVentasEjecutivo", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarRptComparaVentasEjecutivaAnioAnterior(ByVal vstrIDUsuario As String, ByVal vstrAnio As String, _
                                                                   ByVal vstrEstado As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDUsuarioVenta", vstrIDUsuario)
                .Add("@Anio", vstrAnio)
                .Add("@Estado", vstrEstado)
            End With

            Return objADO.GetDataTable(True, "ReporteComparaVentasEjecutivoSubDetalleAnioAnt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function InsertarxAddIn(ByVal BE As clsClienteBE) As String
        Dim dc As New Dictionary(Of String, String)
        Dim strIDCliente As String = String.Empty
        Try
            'Dim objBTCli As New clsClienteBT
            'Return objBTCli.Insertar(BE)

            With dc
                .Add("@RazonComercial", BE.RazonComercial)
                .Add("@RazonSocial", BE.RazonSocial)
                .Add("@Persona", BE.Persona)
                .Add("@Nombre", BE.Nombre)
                .Add("@ApPaterno", BE.ApPaterno)
                .Add("@ApMaterno", BE.ApMaterno)
                .Add("@Domiciliado", BE.Domiciliado)
                .Add("@Ciudad", BE.Ciudad)
                .Add("@Celular", BE.Celular)
                .Add("@Comision", BE.Comision)
                .Add("@Credito", BE.Credito)
                .Add("@MostrarEnReserva", BE.MostrarEnReserva)
                .Add("@Direccion", BE.Direccion)
                .Add("@Fax", BE.Fax)
                .Add("@IdIdentidad", BE.IdIdentidad)
                .Add("@NumIdentidad", BE.NumIdentidad)
                .Add("@Tipo", BE.Tipo)
                .Add("@IDCiudad", BE.IDCiudad)
                .Add("@IDVendedor", BE.IDVendedor)
                .Add("@Notas", BE.Notas)
                .Add("@Telefono1", BE.Telefono1)
                .Add("@Telefono2", BE.Telefono2)
                .Add("@Web", BE.Web)
                .Add("@CodPostal", BE.CodPostal)
                .Add("@Complejidad", BE.Complejidad)
                .Add("@Frecuencia", BE.Frecuencia)
                .Add("@Solvencia", BE.Solvencia)
                .Add("@CoTipoVenta", BE.CoTipoVenta)
                .Add("@UserMod", BE.UserMod)

                '
                'MLL-Agregado el dia 29-05-14
                .Add("@CoStarSoft", BE.CoStarSoft)
                .Add("@NuCuentaComision", BE.NuCuentaComision)

                .Add("@FlTop", BE.FlTop)
                .Add("@CoUserPerfilUpdate", BE.CoUserPerfilUpdate)
                .Add("@txPerfilInformacion", BE.txPerfilInformacion)
                .Add("@txPerfilOperatividad", BE.txPerfilOperatividad)

                .Add("@FlProveedor", BE.FlProveedor)
                .Add("@NuPosicion", BE.NuPosicion)

                .Add("@CoTipDocIdentPax", BE.CoTipDocIdentPax)
                .Add("@NuDocIdentPax", BE.NuDocIdentPax)

                .Add("@pIDCliente", "")
            End With

            strIDCliente = objADO.ExecuteSPOutput("MACLIENTES_Ins", dc)
            BE.IDCliente = strIDCliente

            Dim oBESocioNegocio As clsSocioNegocioBE = Nothing
            PasarDatosClientesToSocioNegocio(strIDCliente, BE.UserMod, oBESocioNegocio)

            If Not oBESocioNegocio Is Nothing Then
                Dim oBTJsonMaster As New clsJsonInterfaceBN()
                oBTJsonMaster.EnviarDatosRESTFulJson(oBESocioNegocio, _
                                                    "http://sap:" + gstrPuertoSAP + "/api/BusinessPartners/", _
                                                    "POST")
                Actualizar_CardCodeSAP(BE.IDCliente, oBESocioNegocio.CardCode)
            End If
            Return strIDCliente
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub Actualizar_CardCodeSAP(ByVal strIDCliente As String, ByVal strCardCodeSAP As String)
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDCliente", strIDCliente)
                .Add("@CardCodeSAP", strCardCodeSAP)
            End With

            objADO.ExecuteSP("MACLIENTES_Upd_CardCodeSAP", dc)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub PasarDatosClientesToSocioNegocio(ByVal vstrIDCliente As String, ByVal vstrUserMod As String, _
                                                 ByRef oBESocioNegocioBE As clsSocioNegocioBE)
        Try
            Dim dtDatos As DataTable = drConsultarDatosClienteSAP(vstrIDCliente)
            If dtDatos.Rows.Count > 0 Then
                Dim dr As DataRow = dtDatos.Rows(0)
                oBESocioNegocioBE = New clsSocioNegocioBE()
                With oBESocioNegocioBE
                    .CardCode = dr("CardCode")
                    .CardName = dr("CardName")
                    .CardType = dr("CardType")
                    .GroupCode = dr("GroupCode")
                    .FederalTaxID = dr("FederalTaxID")
                    .Currency = dr("Currency")
                    .U_SYP_BPTP = dr("U_SYP_BPTP")
                    .U_SYP_BPTD = dr("U_SYP_BPTD")
                    .U_SYP_BPAP = dr("U_SYP_BPAP")
                    .U_SYP_BPAM = dr("U_SYP_BPAM")
                    .U_SYP_BPN2 = dr("U_SYP_BPN2")
                    .U_SYP_BPNO = dr("U_SYP_BPNO")
                    .EmailAddress = dr("EmailAddress")
                    .Phone1 = dr("Phone1")
                    .Phone2 = dr("Phone2")
                    .SubjectToWithholdingTax = dr("SubjectToWithholdingTax")
                    .Valid = dr("Valid")

                    Dim BPBAddress As New List(Of clsBPAddressesBE)
                    Dim oBPBAddress As New clsBPAddressesBE
                    With oBPBAddress
                        .AddressName = dr("AddressName")
                        .Street = dr("Street")
                        .ZipCode = dr("ZipCode")
                        .Block = dr("Block")
                        .AddressType = dr("AddressType")
                    End With
                    BPBAddress.Add(oBPBAddress)
                    .BPAddresses = BPBAddress

                    Dim ContactEmployees As New List(Of clsContactEmployeesBE)
                    .ContactEmployees = ContactEmployees
                End With
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function drConsultarDatosClienteSAP(ByVal vstrIDCliente As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@IDSocioNegocio", vstrIDCliente)
            dc.Add("@TipoSocioNegocio", "C")

            Return objADO.GetDataTable(True, "SAP_ListarSociosNegocio", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsAdministClienteBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarxIDCliente(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCliente", vstrIDCliente)

                Return objADO.GetDataTable(True, "MAADMINISTCLIENTE_SelxIDCliente", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsSeriesClienteBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDCliente(ByVal vstrIDCLiente As String) As Data.DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCliente", vstrIDCLiente)

                Return objADO.GetDataTable(True, "MASERIESCLIENTE_Del_SelxIDCliente", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo(ByVal vstrIDCliente As String) As Data.DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCliente", vstrIDCliente)

                Return objADO.GetDataTable(True, "MASERIESCLIENTE_Sel_CboxIDCliente", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsContactoClienteBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Sub InsertarxAddIn(ByVal BE As clsClienteBE.clsContactoClienteBE)
            Dim dc As New Dictionary(Of String, String)

            Try
                With dc
                    .Add("@IDCliente", BE.IDCliente)
                    .Add("@Titulo", BE.Titulo)
                    .Add("@Nombres", BE.Nombres)
                    .Add("@Apellidos", BE.Apellidos)
                    .Add("@Cargo", BE.Cargo)
                    .Add("@Telefono", BE.Telefono)
                    .Add("@Fax", BE.Fax)
                    .Add("@Celular", BE.Celular)
                    .Add("@Email", BE.Email)
                    .Add("@Email2", BE.Email2)
                    .Add("@Email3", BE.Email3)
                    .Add("@Anexo", BE.Anexo)
                    .Add("@UsuarioLogeo", BE.UsuarioLogeo)
                    .Add("@PasswordLogeo", BE.PasswordLogeo)
                    .Add("@FechaPassword", BE.FechaPassword)
                    .Add("@EsAdminist", BE.EsAdminist)
                    .Add("@Notas", BE.Notas)
                    .Add("@UserMod", BE.UserMod)
                    .Add("@FlAccesoWeb", BE.FlAccesoWeb)
                End With

                objADO.ExecuteSP("MACONTACTOSCLIENTE_Ins", dc)

            Catch ex As Exception
                Throw
            End Try
        End Sub

        Public Function ConsultarList(ByVal vstrIDCliente As String, ByVal vstrNombres As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCliente", vstrIDCliente)
                dc.Add("@Nombres", vstrNombres)

                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vstrIDContacto As String, ByVal vstrIDCliente As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDContacto", vstrIDContacto)
            dc.Add("@IDCliente", vstrIDCliente)

            Dim strSql As String = "MACONTACTOSCLIENTE_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarPk_InvoicePaymentwall(ByVal vstrIDContacto As String, ByVal vstrIDCliente As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDCliente", vstrIDCliente)
            dc.Add("@IDContacto", vstrIDContacto)

            Dim strSql As String = "MACONTACTOSCLIENTE_Sel_Pk_InvoicePaymentwall"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarxContAdminist(ByVal vstrIDCliente As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)

            dc.Add("@IDCliente", vstrIDCliente)

            Dim strSql As String = "MACONTACTOSCLIENTE_Sel_xContAdminist"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function blnExisteContactoAdminist(ByVal vstrIDCliente As String, ByVal vstrIDContacto As String) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCliente", vstrIDCliente)
                dc.Add("@IDContacto", vstrIDContacto)
                dc.Add("@pbOk", "1")

                Dim strSql As String = "MACONTACTOSCLIENTE_SelOut_SiExisteContactoAdminist"
                Return objADO.GetSPOutputValue(strSql, dc)


            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarLvw(ByVal vstrIDContacto As String, ByVal vstrIDCliente As String, _
                                     ByVal vstrNombre As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDContacto", vstrIDContacto)
                dc.Add("@IDCliente", vstrIDCliente)
                dc.Add("@Nombre", vstrNombre)

                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function


        Public Function ConsultarxClienteLvw(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try

                dc.Add("@IDCliente", vstrIDCliente)

                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_Sel_xIDCliente", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarxCliente_ContactosconMail(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCliente", vstrIDCliente)

                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_Sel_Mail_xIDCliente", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarCbo(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCliente", vstrIDCliente)

                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarSbRpt(ByVal vstrPersona As String, ByVal vstrTipo As String, ByVal vstrIDVendedor _
                                        As String, ByVal vstrIDPais As String, ByVal vstrIDCiudad As String, _
                                        ByVal vbytDomiciliado As Byte) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Persona", vstrPersona)
                dc.Add("@Tipo", vstrTipo)
                dc.Add("@IDVendedor", vstrIDVendedor)
                dc.Add("@IDPais", vstrIDPais)
                dc.Add("@IDCiudad", vstrIDCiudad)
                dc.Add("@Domiciliado", vbytDomiciliado)
                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_Sel_SbRpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarSbFicCliente(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCliente", vstrIDCliente)
                Return objADO.GetDataTable(True, "MACLIENTES_Sel_SbFicha", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCorreos(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCLiente", vstrIDCliente)
                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_SelCorreos", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarContactosxIDCliente(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCliente", vstrIDCliente)
                Return objADO.GetDataTable(True, "MACONTACTOSCLIENTE_SelListxIDCliente", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function blnExisteUsuarioExtranet(ByVal vstrIDCliente As String, ByVal vstrIDContacto As String, ByVal vchrTablaPC As String, ByVal vstrUsuario As String) As Boolean
            Try
                Dim blnExiste As Boolean = False
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDContacto", vstrIDContacto)
                dc.Add("@IDTablaParent", vstrIDCliente)
                dc.Add("@TipoTablaParent", vchrTablaPC)
                dc.Add("@CodUsuario", vstrUsuario)
                dc.Add("@pExists", False)

                blnExiste = objADO.GetSPOutputValue("MACONTACTOSPROVCLIE_Exists_UsuarioExtranet", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsSeguimientoClienteBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCliente", vstrIDCliente)

                Return objADO.GetDataTable(True, "MASEGUIMCLIENTE_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vintIDSeguim As Int32) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDSeguim", vintIDSeguim)

            Dim strSql As String = "MASEGUIMCLIENTE_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
    End Class

    <ComVisible(True)> _
  <Transaction(TransactionOption.NotSupported)> _
    Public Class clsClientes_Perfil_GuiasBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrCoCliente As String, ByVal vstrCoProveedor As String, ByVal vstrIDCiudad As String, ByVal vstrRazonSocial As String) As DataTable
            Dim dc As New Dictionary(Of String, String)


            Try
                dc.Add("@CoCliente", vstrCoCliente)
                dc.Add("@CoProveedor", vstrCoProveedor)
                dc.Add("@IDCiudad", vstrIDCiudad)
                dc.Add("@RazonSocial", vstrRazonSocial)

                Return objADO.GetDataTable(True, "MACLIENTES_PERFIL_GUIAS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vstrCoCliente As String, ByVal vstrCoProveedor As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoCliente", vstrCoCliente)

            Dim strSql As String = "MACLIENTES_PERFIL_GUIAS_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarList_Ciudades(ByVal vstrCoCliente As String) As DataTable
            Dim dc As New Dictionary(Of String, String)


            Try
                dc.Add("@CoCliente", vstrCoCliente)

                Return objADO.GetDataTable(True, "MACLIENTES_PERFIL_GUIAS_Sel_Ciudades_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

    End Class

End Class


