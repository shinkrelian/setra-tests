﻿Imports ComDataAccessD2
Imports ComSEToursBE2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsServicioProveedorBN
    Inherits ServicedComponent

    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrIDServicio As String, ByVal vstrIDProveedor As String, _
                                  ByVal vstrRazonSocial As String, ByVal vstrDescripcion As String, _
                                ByVal vblnSoloActivos As Boolean, ByVal vstrIDTipoProv As String, _
                                ByVal vstrAnio As String, _
                                ByVal vstrIDCiudad As String, ByVal vbytCategoria As Byte, _
                                ByVal vstrIDCat As String, ByVal vintIDCab As Integer, ByVal vstrTipoGasto As String, _
                                ByVal vstrCoPago As String, _
                                Optional ByVal vblnFiltraTodosServiciosyVarios As Boolean = False, Optional ByVal vstrCoPais As String = "") As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@IDProveedor", vstrIDProveedor)
            dc.Add("@RazonSocial", vstrRazonSocial)
            dc.Add("@Descripcion", vstrDescripcion)
            dc.Add("@SoloActivos", vblnSoloActivos)
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@Anio", vstrAnio)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@Categoria", vbytCategoria)
            dc.Add("@IDCat", vstrIDCat)
            dc.Add("@IDCab", vintIDCab)
            dc.Add("@TipoGasto", vstrTipoGasto)
            dc.Add("@CoPago", vstrCoPago)
            dc.Add("@CoPais", vstrCoPais)
            If Not vblnFiltraTodosServiciosyVarios Then
                Return objADO.GetDataTable(True, "MASERVICIOS_Sel_List", dc)
            Else
                Return objADO.GetDataTable(True, "MASERVICIOS_Sel_ListVariosRel", dc)
            End If
        Catch ex As Exception
            Throw
        End Try

    End Function
    Public Function ConsultarRpt(ByVal vstrAño As String, ByVal vstrIDCiudad As String, _
                                 ByVal vstrIDTipoProv As String, ByVal vintActivo As Boolean, _
                                 ByVal vstrDescripcion As String, ByVal vstrProveedor As String, _
                                 ByVal vstrIDCat As String, ByVal vbytCategoria As Byte) As DataTable
        Dim dc As New Dictionary(Of String, String)
        Try
            dc.Add("@año", vstrAño)
            dc.Add("@IDCiudad", vstrIDCiudad)
            dc.Add("@IDTipoProv", vstrIDTipoProv)
            dc.Add("@Activo", vintActivo)
            dc.Add("@Descripcion", vstrDescripcion)
            dc.Add("@Proveedor", vstrProveedor)
            dc.Add("@IDCat", vstrIDCat)
            dc.Add("@Categoria", vbytCategoria)
            Return objADO.GetDataTable(True, "MASERVICIOS_Sel_Rpt", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function
    Public Function ConsultarDetallexIDServicio(ByVal vstrIDServicio As String, ByVal vstrAnio As String, _
                                                ByVal vstrTipoGasto As String, ByVal vstrdActivo As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDServicio", vstrIDServicio)
            dc.Add("@Anio", vstrAnio)
            dc.Add("@TipoGasto", vstrTipoGasto)
            dc.Add("@Estado", vstrdActivo)

            Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_ListxIDServicio", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarDetallexIDProveedor(ByVal vstrIDProveedor As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDProveedor", vstrIDProveedor)


            Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_IDProveedor_Alojamiento", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@ID", vstrID)
            dc.Add("@Descripcion", vstrDescripcion)

            Return objADO.GetDataTable(True, "MASERVICIOS_Sel_Lvw", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarCboxIDProveedor(ByVal vstrIDProveedor As String) As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDProveedor", vstrIDProveedor)

            Return objADO.GetDataTable(True, "MASERVICIOS_Sel_xIDProveedor_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarTipoCbo() As DataTable
        Dim dc As New Dictionary(Of String, String)

        Try

            Return objADO.GetDataTable(True, "MATIPOSERVICIOS_Sel_Cbo", dc)
        Catch ex As Exception
            Throw
        End Try

    End Function


    Public Function ConsultarPk(ByVal vstrIDServicio As String, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)

        Return objADO.GetDataReader("MASERVICIOS_Sel_Pk", dc, vCnn)

    End Function

    Public Function ConsultarxIDServicioAnio(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrTipo As String, ByVal vintIDDet As Integer) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)
        dc.Add("@Anio", vstrAnio)
        dc.Add("@Tipo", vstrTipo)
        dc.Add("@IDDet", vintIDDet)

        Dim strSql As String = "MASERVICIOS_Det_Sel_xIDServicioAnio"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function ConsultarxIDServicioAnioServicio(ByVal vstrIDServicio As String, ByVal vstrAnio As String, _
                                                     ByVal vstrTipo As String, ByVal vstrServicio As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)
        dc.Add("@Anio", vstrAnio)
        dc.Add("@Tipo", vstrTipo)
        dc.Add("@Servicio", vstrServicio)

        Dim strSql As String = "MASERVICIOS_Det_Sel_xIDServicioAnioServicio"
        Return objADO.GetDataReader(strSql, dc)

    End Function

    Public Function strConsultarProveedor(ByVal vintIDServicio_Det As Integer) As String
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vintIDServicio_Det)
            dc.Add("@pCoProveedor", "")

            Return objADO.GetSPOutputValue("MASERVICIOS_DET_SelCoProveedorxIDServicio_DetOutput", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarTodosxIDServicioAnioTipo(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrTipo As String) As DataTable
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)
        dc.Add("@Anio", vstrAnio)
        dc.Add("@Tipo", vstrTipo)

        Dim strSql As String = "MASERVICIOS_DET_Sel_TodosxIDServicioAnioTipo"
        Return objADO.GetDataTable(True, strSql, dc)

    End Function

    Public Function ConsultarxIDServicioAnioTipo(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrTipo As String) As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        dc.Add("@IDServicio", vstrIDServicio)
        dc.Add("@Anio", vstrAnio)
        dc.Add("@Tipo", vstrTipo)

        Dim strSql As String = "MASERVICIOS_DET_Sel_TodosxIDServicioAnioTipo"
        Return objADO.GetDataReader(strSql, dc)
    End Function

    Public Function ConsultarCopias(ByVal vstrIDServicio As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicioCopia", vstrIDServicio)

            Return objADO.GetDataTable(True, "MASERVICIOS_Sel_Copia", dc)

        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function ConsultarExisteDC(ByVal vstrIdUbigeo As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IdUbigeo", vstrIdUbigeo)
                .Add("@pblnExiste", "")
            End With

            Dim blnExiste As Boolean = objADO.ExecuteSPOutput("MASERVICIOS_Sel_ExisteDCOutput", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteEnReservasPeroNoEnCoti(ByVal vintIDDet As Integer) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDDet", vintIDDet)
                .Add("@pExiste", False)
            End With
            Dim blnExiste As Boolean = objADO.ExecuteSPOutput("RESERVAS_DET_ExistsxIDDet", dc)
            Return blnExiste
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnExisteenCotiz(ByVal vstrIDServicio As String) As Boolean
        Dim dc As New Dictionary(Of String, String)
        Try
            With dc
                .Add("@IDServicio", vstrIDServicio)
                .Add("@pbExists", 0)
            End With

            Return objADO.ExecuteSPOutput("COTIDET_SelxIDServicio_ExisteOutput", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDetalleServicioProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrDescripcion As String) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@Anio", vstrAnio)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vstrIDServicio_Det As Int32, _
                                    Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vstrIDServicio_Det)

            Dim strSql As String = "MASERVICIOS_DET_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc, vCnn)

        End Function

        Public Function ConsultarPkSinAnulado(ByVal vstrIDServicio_Det As Int32, _
                            Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vstrIDServicio_Det)

            Dim strSql As String = "MASERVICIOS_DET_Sel_PkSinAnul"
            Return objADO.GetDataReader(strSql, dc, vCnn)

        End Function

        Public Function dttConsultarPkSinAnulado(ByVal vstrIDServicio_Det As Int32, _
                    Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio_Det", vstrIDServicio_Det)

            Dim strSql As String = "MASERVICIOS_DET_Sel_PkSinAnul"
            Return objADO.GetDataTable(True, strSql, dc)

        End Function

        Public Function ConsultarTodosxIDServicioAnioTipoDescripcion(ByVal vstrIDServicio As String, _
                                                                 ByVal vstrAnio As String, _
                                                                 ByVal vstrTipo As String, _
                                                                 ByVal vstrDescripcion As String) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@Anio", vstrAnio)
                dc.Add("@Tipo", vstrTipo)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataReader("MASERVICIOS_DET_SelxIDServicioAnioTipoDescripcion", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function dblDevuelveCostoRealxServicioxCantPax(ByVal vintIDServicio_Det As Integer, ByVal vintPax As Integer) As Double
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio_Det", vintIDServicio_Det)
                dc.Add("@NroPax", vintPax)
                dc.Add("@pUnitPrice", 0)

                Return objADO.GetSPOutputValue("MASERVICIOS_DET_SelCostosxIDServicio_DetxPax", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvw(ByVal vstrIDServicio_Det As Int32, _
                                     ByVal vstrIDServicio As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio_Det", vstrIDServicio_Det)
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarAplicaIgvxIDSservicio_Det(ByVal vintIDServicio_Det As Integer) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim blnAplicaIGV As Boolean = False
                dc.Add("@IDServicio_Det", vintIDServicio_Det)
                dc.Add("@pAplicaIgv", False)

                blnAplicaIGV = objADO.GetSPOutputValue("MASERVICIOS_DET_SelCampoAplicaIgvxIDServicioDet", dc)
                Return blnAplicaIGV
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarServiciosTC(ByVal vstrAnio As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Anio", vstrAnio)
                Return objADO.GetDataReader("MASERVICIOS_Sel_ServiciosTCxAnio", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCodxTripleCbo(ByVal vstrIDServicio As String, ByVal vstrAnio As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@Anio", vstrAnio)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_CodxTriple_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarListAyuda(ByVal vstrIDTipoProv As String, ByVal vstrIDProveedor As String, _
                                           ByVal vstrIDServicio As String, _
                                           ByVal vbytCategoria As Byte, _
                                           ByVal vstrIDCat As String) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@IDTipoProv", vstrIDTipoProv)
                dc.Add("@IDProveedor", vstrIDProveedor)
                dc.Add("@Categoria", vbytCategoria)
                dc.Add("@IDCat", vstrIDCat)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_ListAyuda", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function bytCorrelativo(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrTipo As String) As Byte
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@Anio", vstrAnio)
                dc.Add("@Tipo", vstrTipo)
                dc.Add("@pCorrelativo", 0)

                Return objADO.ExecuteSPOutput("MASERVICIOS_DET_SelOut_Correlativo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function strCoMonedaDetServicio(ByVal vintIDServicio_Det As Integer) As String
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio_Det", vintIDServicio_Det)
                dc.Add("@pCoMoneda", "")

                Return objADO.ExecuteSPOutput("MASERVICIOS_DET_SelCoMonedaOutput", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function


        Public Function ConsultarCopiasxAnioNue(ByVal vstrIDServicio As String, ByVal vstrAnioNue As String) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@AnioNue", vstrAnioNue)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_Copia", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarCopiasxVarianteNue(ByVal vstrIDServicio As String, ByVal vstrTipoNue As String) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@TipoNue", vstrTipoNue)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_CopiaxVariante", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function blnExisteServPeruRail(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrVariante As String) As Boolean
        '    Dim dc As New Dictionary(Of String, String)
        '    Try

        '        dc.Add("@IDservicio", vstrIDServicio)
        '        dc.Add("@Anio", vstrAnio)
        '        dc.Add("@Variante", vstrVariante)
        '        dc.Add("@pExiste", False)

        '        Return objADO.GetSPOutputValue("MASERVICIOS_DET_SelxExistePeruRail", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function

        Public Function dttDetServPeruRail(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrVariante As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try

                dc.Add("@IDservicio", vstrIDServicio)
                dc.Add("@Anio", vstrAnio)
                dc.Add("@Variante", vstrVariante)
                'dc.Add("@pIDServicio_Det", 0)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_SelIDServicio_DetPeruRail", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function intIDservicio_DetxAnioVariante(ByVal vstrIDServicio As String, ByVal vstrAnio As String, ByVal vstrTipo As String) As Integer
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        With dc
        '            .Add("@IDServicio", vstrIDServicio)
        '            .Add("@Anio", vstrAnio)
        '            .Add("@Tipo", vstrTipo)
        '            .Add("@pIDservicio_Det", 0)
        '        End With

        '        Return objADO.ExecuteSPOutput("MASERVICIOS_Det_SelxIDServicioAnioTipo_IDServicio_DetOutput", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function

        Public Function ConsultarServiciosVtasAdicionalesAPT(ByVal vstrCoCiudad As String, ByVal vintIDCab As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoCiudad", vstrCoCiudad)
                If vintIDCab > 0 Then
                    'dc.Add("@QtPax", vintQtPax)
                    dc.Add("@IDCab", vintIDCab)
                    Return objADO.GetDataReader("MASERVICIOS_SelFOCxCiudad", dc)
                Else
                    Return objADO.GetDataReader("MASERVICIOS_VTA_ADIC_APT_SelTourAdicAPT", dc)
                End If
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCostosServiciosPaxFOC(ByVal vstrCoCiudad As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoCiudad", vstrCoCiudad)
                Return objADO.GetDataReader("MASERVICIOS_RANGO_APT_SelxCiudadFOC", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarDetalleServiciosxIDServicio(ByVal vstrIDServicio As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_xIDServicio", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsCostosDetalleServicioProveedorBN
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN


            Public Function ConsultarListxIDDetalle(ByVal vstrIDServicio_Det As Int32, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable

                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDServicio_Det", vstrIDServicio_Det)

                    Return objADO.GetDataTable(True, "MASERVICIOS_DET_COSTOS_Sel_xIDServicio_Det", dc, vCnn)
                Catch ex As Exception
                    Throw
                End Try

            End Function

            Public Function blnExisteRangos(ByRef vLstRangos As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax)) As Boolean

                Try
                    Dim objServBT As New clsServicioProveedorBT
                    Dim blnOkTotal As Boolean = True
                    For Each ItemServPax As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax In vLstRangos
                        Dim blnOk As Boolean = True
                        Dim dttDetServ As DataTable = objServBT.dttConsultarxIDServicioAnio(ItemServPax.IDServicio, ItemServPax.Anio, ItemServPax.Variante, 0)
                        For Each drDetServ As DataRow In dttDetServ.Rows
                            If IsDBNull(drDetServ("Monto_sgl")) Then
                                If Not blnExisteRango(drDetServ("IDServicio_Det"), ItemServPax.NroPax, ItemServPax.NroLiberados) Then
                                    'Return False
                                    blnOk = False
                                    blnOkTotal = False
                                End If
                            End If
                            ItemServPax.Existe = blnOk
                        Next

                    Next

                    Return blnOkTotal

                Catch ex As Exception
                    Throw
                End Try

            End Function

            Private Function blnExisteRango(ByVal vintIDServicio_Det As Int32, _
                                           ByVal vintPax As Int16, ByVal vintLiberados As Int16) As Boolean
                Dim dc As New Dictionary(Of String, String)
                Try
                    With dc
                        .Add("@IDServicio_Det", vintIDServicio_Det)
                        .Add("@NroPax", vintPax)
                        .Add("@NroLiberados", vintLiberados)
                        .Add("@pExiste", 0)
                    End With

                    Return objADO.ExecuteSPOutput("MASERVICIOS_DET_COSTOS_Sel_ExisteRangoOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class

        '-------------------------------------------------------

        <ComVisible(True)> _
       <Transaction(TransactionOption.NotSupported)> _
        Public Class clsRangosAPTDetalleServicioProveedorBN
            Inherits ServicedComponent
            Dim objADO As New clsDataAccessDN


            Public Function ConsultarListxIDDetalle(ByVal vstrIDServicio_Det As Int32, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable

                Dim dc As New Dictionary(Of String, String)

                Try
                    dc.Add("@IDServicio_Det", vstrIDServicio_Det)

                    Return objADO.GetDataTable(True, "MASERVICIOS_DET_RANGO_APT_Sel_xIDServicio_Det", dc, vCnn)
                Catch ex As Exception
                    Throw
                End Try

            End Function

            Public Function blnExisteRangos(ByRef vLstRangos As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax)) As Boolean

                Try
                    Dim objServBT As New clsServicioProveedorBT
                    Dim blnOkTotal As Boolean = True
                    For Each ItemServPax As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax In vLstRangos
                        Dim blnOk As Boolean = True
                        Dim dttDetServ As DataTable = objServBT.dttConsultarxIDServicioAnio(ItemServPax.IDServicio, ItemServPax.Anio, ItemServPax.Variante, 0)
                        For Each drDetServ As DataRow In dttDetServ.Rows
                            If IsDBNull(drDetServ("Monto_sgl")) Then
                                If Not blnExisteRango(drDetServ("IDServicio_Det"), ItemServPax.NroPax, ItemServPax.NroLiberados) Then
                                    'Return False
                                    blnOk = False
                                    blnOkTotal = False
                                End If
                            End If
                            ItemServPax.Existe = blnOk
                        Next

                    Next

                    Return blnOkTotal

                Catch ex As Exception
                    Throw
                End Try

            End Function

            Private Function blnExisteRango(ByVal vintIDServicio_Det As Int32, _
                                           ByVal vintPax As Int16, ByVal vintLiberados As Int16) As Boolean
                Dim dc As New Dictionary(Of String, String)
                Try
                    With dc
                        .Add("@IDServicio_Det", vintIDServicio_Det)
                        .Add("@NroPax", vintPax)
                        .Add("@NroLiberados", vintLiberados)
                        .Add("@pExiste", 0)
                    End With

                    Return objADO.ExecuteSPOutput("MASERVICIOS_DET_RANGO_APT_Sel_ExisteRangoOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class

        '-------------------------------------------------------

    End Class

    '-------------------------------------------------------

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsRangosAPTServicioProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN


        Public Function ConsultarListxIDServicio(ByVal vstrIDServicio As String, Optional ByVal vCnn As SqlClient.SqlConnection = Nothing) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataTable(True, "MASERVICIOS_RANGO_APT_Sel_xIDServicio", dc, vCnn)
            Catch ex As Exception
                Throw
            End Try

        End Function

    End Class

    '-------------------------------------------------------

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTextoServicioProveedorBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDServicio(ByVal vstrIDServicio As String) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataTable(True, "MATEXTOSERVICIOS_Sel_xIDServicio", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarxIDServicio(ByVal vstrIDServicio As String) As SqlClient.SqlDataReader

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataReader("MATEXTOSERVICIOS_Sel_xIDServicio", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsServiciosAtencionBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDServicio(ByVal vstrIDServicio As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataTable(True, "MASERVICIOS_ATENCION_Sel_xIDServicio", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsServiciosporDiaBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDServicio(ByVal vstrIDServicio As String) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataTable(True, "MASERVICIOS_DIA_Sel_xIDServicio", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarListxIDServicioIdioma(ByVal vstrIDServicio As String, ByVal vstrIDIdioma As String) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@IDIdioma", vstrIDIdioma)

                Return objADO.GetDataTable(True, "MASERVICIOS_DIA_Sel_xIDServicioIdioma", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarxIDServicioDia(ByVal vstrIDServicio As String, ByVal vbytDia As Byte) As SqlClient.SqlDataReader

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)
                dc.Add("@Dia", vbytDia)

                Return objADO.GetDataReader("MASERVICIOS_DIA_Sel_xIDServicioDia", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarxIDServicioIdiomaparaCotizar(ByVal vstrIDServicio As String) As SqlClient.SqlDataReader

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataReader("MASERVICIOS_DIA_Sel_xIDServicioparaCotizar", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

    End Class

    <ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
    Public Class clsServiciosAlimentacionporDiaBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDServicio(ByVal vstrIDServicio As String) As DataTable

            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDServicio", vstrIDServicio)

                Return objADO.GetDataTable(True, "MASERVICIOS_ALIMENTACION_DIA_Sel_xIDServicio", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
    End Class
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsConcepto_HotelBN
    Inherits ServicedComponent

    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrCoConcepto As String, ByVal vstrCoTipo As String, _
                                  ByVal vstrTxDescripcion As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@CoConcepto", vstrCoConcepto)
            dc.Add("@CoTipo", vstrCoTipo)
            dc.Add("@TxDescripcion", vstrTxDescripcion)

            Return objADO.GetDataTable(True, "MACONCEPTO_HOTEL_Sel_List", dc)

        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarList_TiposHab_Operadores(ByVal vstrIDServicio As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@IDServicio", vstrIDServicio)

            Return objADO.GetDataTable(True, "MASERVICIOS_DET_Sel_Cbo_Tipo_IDServicio", dc)

        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarList_Servicios(ByVal vstrCoConcepto As String, ByVal vstrCoTipo As String, _
                                ByVal vstrTxDescripcion As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@CoConcepto", vstrCoConcepto)
            dc.Add("@CoTipo", vstrCoTipo)
            dc.Add("@TxDescripcion", vstrTxDescripcion)

            Return objADO.GetDataTable(True, "MACONCEPTO_HOTEL_Sel_List_Servicios", dc)

        Catch ex As Exception
            Throw
        End Try

    End Function


End Class


'-----------------------------------------------------
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsServicios_Operaciones_HotelesBN
    Inherits ServicedComponent

    Dim objADO As New clsDataAccessDN


    Public Function ConsultarPk(ByVal vstrCoServicio As String)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoServicio", vstrCoServicio)


            Return objADO.GetDataReader("MASERVICIOS_OPERACIONES_HOTELES_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function


    Public Function ConsultarPk_DT(ByVal vstrCoServicio As String) As DataTable
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoServicio", vstrCoServicio)


            Return objADO.GetDataTable(True, "MASERVICIOS_OPERACIONES_HOTELES_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsServicios_HotelBN
    Inherits ServicedComponent

    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrCoServicio As String, ByVal vstrCoConcepto As Integer, ByVal vstrCoTipo As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@CoServicio", vstrCoServicio)
            dc.Add("@CoConcepto", vstrCoConcepto)
            dc.Add("@CoTipo", vstrCoTipo)


            Return objADO.GetDataTable(True, "MASERVICIOS_HOTEL_Sel_List", dc)

        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarList_Conceptos(ByVal vstrCoServicio As String, ByVal vstrCoConcepto As Integer, ByVal vstrCoTipo As String) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@CoServicio", vstrCoServicio)
            dc.Add("@CoConcepto", vstrCoConcepto)
            dc.Add("@CoTipo", vstrCoTipo)


            Return objADO.GetDataTable(True, "MASERVICIOS_HOTEL_Sel_List_Conceptos", dc)

        Catch ex As Exception
            Throw
        End Try

    End Function

    Public Function ConsultarPk(ByVal vstrCoServicio As String, ByVal vstrCoConcepto As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoServicio", vstrCoServicio)
            dc.Add("@CoConcepto", vstrCoConcepto)

            Return objADO.GetDataReader("MASERVICIOS_HOTEL_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsServicios_Hotel_HabitacionesBN
    Inherits ServicedComponent

    Dim objADO As New clsDataAccessDN

    Public Function ConsultarList(ByVal vstrCoServicio As String, ByVal vintNuHabitacion As Integer) As DataTable

        Dim dc As New Dictionary(Of String, String)

        Try
            dc.Add("@CoServicio", vstrCoServicio)
            dc.Add("@NuHabitacion", vintNuHabitacion)


            Return objADO.GetDataTable(True, "MASERVICIOS_HOTEL_HABITACIONES_Sel_List", dc)

        Catch ex As Exception
            Throw
        End Try

    End Function



    Public Function ConsultarPk(ByVal vstrCoServicio As String, ByVal vintNuHabitacion As Integer)
        Try
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoServicio", vstrCoServicio)
            dc.Add("@NuHabitacion", vintNuHabitacion)

            Return objADO.GetDataReader("MASERVICIOS_HOTEL_HABITACIONES_Sel_Pk", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

End Class