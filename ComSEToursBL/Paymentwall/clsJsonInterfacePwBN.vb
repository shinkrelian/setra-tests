﻿Imports ComSEToursBE2
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Imports System.IO
Imports System.Net.Http
Imports System.Net.Http.Headers
Imports System.Text
Imports System.Runtime.InteropServices
Imports System.EnterpriseServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsJsonInterfacePwBN
    Public Sub EnviarDatosRESTFulJson(ByVal oClass As Object, _
                                  ByVal vstrRouteController As String, _
                                  ByVal vstrVerboController As String, _
                                  ByVal ParamArray vparDeletePropertysJson() As String)


        If oClass Is Nothing Then Exit Sub
        If vstrVerboController.Trim = String.Empty Then Exit Sub
        Dim strMessageError As String = String.Empty
        Try

            Dim strDataValueJSON As String = ""
            If Not (vstrVerboController = "DELETE") Then
                strDataValueJSON = JsonConvert.SerializeObject(oClass, Formatting.Indented)
            End If

            DropPropertyJSON(strDataValueJSON, vparDeletePropertysJson)

            Dim strUriHostRestFul As String = vstrRouteController
            Dim strUriAPI As String = vstrRouteController

            If strUriHostRestFul <> String.Empty Then
                Dim clientConnect As New HttpClient()
                With clientConnect
                    Try
                        .BaseAddress = New Uri(strUriHostRestFul)
                        .DefaultRequestHeaders.Accept.Add(New MediaTypeWithQualityHeaderValue("application/json"))
                    Catch ex As Exception
                        strMessageError = "Paymentwall: Error al direccionar el grabado del usuario."
                        Throw
                    End Try

                    If strMessageError = String.Empty Then
                        Dim myContent = New StringContent(content:=strDataValueJSON, encoding:=Encoding.UTF8, mediaType:="application/json")
                        Try
                            Dim response As New System.Net.Http.HttpResponseMessage
                            If vstrVerboController = "POST" Then
                                response = .PostAsync(strUriAPI, myContent).Result
                            ElseIf vstrVerboController = "PUT" Then
                                response = .PutAsync(strUriAPI, myContent).Result
                            ElseIf vstrVerboController = "DELETE" Then
                                response = .DeleteAsync(strUriAPI).Result
                            End If

                            If response.IsSuccessStatusCode Then
                                Dim strJSONFormat As String = response.Content.ReadAsStringAsync().Result().ToString()

                                Dim BEResponseStatus As ResponseStatusPaymentwall = Nothing
                                Dim jss = New System.Web.Script.Serialization.JavaScriptSerializer()
                                Dim data = jss.Deserialize(Of Object)(strJSONFormat)
                                Dim blnErrorInesperado As Boolean = False
                                Try
                                    Dim dicValorResponse As Dictionary(Of String, Object) = DirectCast(data, Dictionary(Of String, Object))
                                    If dicValorResponse.Count > 0 Then
                                        Dim dcValores As Object() = dicValorResponse.Item("errors")
                                        Dim objInterno As Object = dcValores

                                        BEResponseStatus = New ResponseStatusPaymentwall()

                                        BEResponseStatus.ErrCode = dicValorResponse.Item("result").ToString()
                                        BEResponseStatus.ErrMsg = objInterno(0).ToString
                                        If BEResponseStatus.ErrMsg <> "" Then
                                            BEResponseStatus.ErrCode = -1
                                        End If
                                        BEResponseStatus.Id = String.Empty

                                        'BEResponseStatus.ErrCode = CInt(objInterno(0)("ErrCode"))
                                        'BEResponseStatus.ErrMsg = objInterno(0)("ErrMsg")
                                        'BEResponseStatus.Id = String.Empty
                                        'If IsDBNull(objInterno(0)("Id")) = False Then
                                        '    If Not objInterno(0)("Id") Is Nothing Then
                                        '        BEResponseStatus.Id = objInterno(0)("Id").ToString()
                                        '    End If
                                        'End If

                                    End If

                                    objResponsePaymentwall = BEResponseStatus

                                    If Not BEResponseStatus Is Nothing Then
                                        If BEResponseStatus.ErrCode <> 0 Then
                                            blnErrorInesperado = True
                                        End If
                                    End If
                                Catch ex As Exception
                                    blnErrorInesperado = True
                                End Try

                                If blnErrorInesperado Then
                                    strMessageError &= "Paymentwall: Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
                                    If BEResponseStatus.ErrMsg <> String.Empty Then
                                        strMessageError = "Paymentwall: " & BEResponseStatus.ErrMsg
                                    End If
                                    Throw New Exception
                                End If
                            Else
                                strMessageError &= "Paymentwall: "
                                Select Case response.StatusCode
                                    Case Net.HttpStatusCode.BadRequest
                                        Dim BEResponseStatus As ResponseStatusPaymentwall = Nothing
                                        Dim strJSONFormat As String = response.Content.ReadAsStringAsync().Result().ToString()
                                        Dim jss = New System.Web.Script.Serialization.JavaScriptSerializer()
                                        Dim data = jss.Deserialize(Of Object)(strJSONFormat)
                                        Dim blnErrorInesperado As Boolean = False

                                        Try
                                            Dim dicValorResponse As Dictionary(Of String, Object) = DirectCast(data, Dictionary(Of String, Object))
                                            If dicValorResponse.Count > 0 Then
                                                Dim dcErrorModel As Dictionary(Of String, Object) = dicValorResponse.Item("ModelState")
                                                For Each ItemDic As KeyValuePair(Of String, Object) In dcErrorModel
                                                    Dim strKey As String = ItemDic.Key
                                                    Dim valObj As Object() = ItemDic.Value

                                                    Dim na As Integer = 0
                                                    strMessageError &= vbCrLf & "Error: " & Replace(strKey, "oBP.", String.Empty) & vbCrLf & _
                                                                        valObj(0).ToString()
                                                Next
                                            End If

                                            If Not BEResponseStatus Is Nothing Then
                                                If BEResponseStatus.ErrCode <> 0 Then
                                                    blnErrorInesperado = True
                                                End If
                                            End If
                                        Catch ex As Exception
                                            blnErrorInesperado = True
                                        End Try

                                        If blnErrorInesperado Then
                                            strMessageError &= "Paymentwall: Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
                                            If BEResponseStatus.ErrMsg <> String.Empty Then
                                                strMessageError = "Paymentwall: " & BEResponseStatus.ErrMsg
                                            End If
                                            Throw New Exception
                                        End If
                                    Case Else
                                        strMessageError &= "Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
                                        Throw New Exception
                                End Select
                                Throw New Exception
                            End If
                        Catch ex As Exception
                            Throw
                        End Try
                    End If
                End With
            End If

        Catch ex As Exception
            If strMessageError = String.Empty Then
                strMessageError = "Paymentwall: Ocurrió un problema inesperado en el componente." & vbCrLf & "Pónganse en contacto con el departamento de desarrollo."
            End If
            Throw New ApplicationException(strMessageError)
        End Try
    End Sub
End Class
