﻿'Imports ComSeToursBE
Imports ComDataAccessD2
Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

Public Class clsTablasApoyoBN

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoDocBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDtipodoc As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDtipodoc", vstrIDtipodoc)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPODOC_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vstrIDtipodoc As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDtipodoc", vstrIDtipodoc)

            Dim strSql As String = "MATIPODOC_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPODOC_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATIPODOC_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarCbo(Optional ByVal blnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", blnTodos)
                Return objADO.GetDataTable(True, "MATIPODOC_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarCboProv(Optional ByVal blnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", blnTodos)
                Return objADO.GetDataTable(True, "MATIPODOC_Sel_CboProvee", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarCboFondoFijo(Optional ByVal blnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", blnTodos)
                Return objADO.GetDataTable(True, "MATIPODOC_Sel_CboFondoFijo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'Public Function ConsultarCboxNotaCredito(Optional ByVal blnTodos As Boolean = False) As DataTable
        '    Dim dc As New Dictionary(Of String, String)
        '    Try
        '        dc.Add("@bTodos", blnTodos)
        '        Return objADO.GetDataTable(True, "MATIPODOC_Sel_CboxNotaCredito", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Function
        Public Function ConsultarCboDocumentos(ByVal vblnTodos As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MATIPODOC_Sel_CboxDocumentos", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoPresupuestoSobreBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MATIPOPRESUPUESTO_SOBRE_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsSeriesBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarCbo(ByVal vblnDescripcion As Boolean, ByVal vblnDescrTodos As Boolean, Optional ByVal vblnTodos As Boolean = False)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@ConDescrip", vblnDescripcion)
                dc.Add("@DescrTodo", vblnDescrTodos)
                dc.Add("@Todos", vblnTodos)

                Return objADO.GetDataTable(True, "MASERIES_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vstrIDSerie As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDSerie", vstrIDSerie)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MASERIES_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDSerie As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDSerie", vstrIDSerie)

            Dim strSql As String = "MASERIES_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MASERIES_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MASERIES_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCorrelativoBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxTabla(ByVal vstrTabla As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Tabla", vstrTabla)
                Return objADO.GetDataTable(True, "Correlativo_Sel_ListxTabla", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCalendario_IataBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "CALENDARIO_IATA_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCuestionarioBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxTipoProv(ByVal vstrIDTipoProv As String, ByVal vintIDCab As Integer, _
                                               ByVal vintNuControlCalidad As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDTipoProv", vstrIDTipoProv)
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@NuControlCalidad", vintNuControlCalidad)

                Return objADO.GetDataTable(True, "MACUESTIONARIO_SelxIDTipoProvxIDCab", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCategoriaBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDCat As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDCat", vstrIDCat)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MACATEGORIA_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDCat As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IdCat", vstrIDCat)

            Dim strSql As String = "MACATEGORIA_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function


        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MACATEGORIA_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MACATEGORIA_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarCboMargen(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MACATEGORIA_Sel_CboMargen", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        'jorge
        Public Function ConsultarIDCat_Proveedor(ByVal vstrIDProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDProveedor", vstrIDProveedor)
                Return objADO.GetDataTable(True, "MASERVICIOS_CAT_SEL_IDPROV", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try

                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MACATEGORIA_Sel_Rpt", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoHonorarioBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MATIPOHONORARIOS_Sel_List_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCostosTipoHonarioBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Private Function blnExistenCostos(ByVal vintIDCab As Integer, ByVal vstrTipoHon As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim blnExiste As Boolean = False
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@CoTipoHon", vstrTipoHon)

                dc.Add("@pExiste", False)
                blnExiste = objADO.GetSPOutputValue("COTICAB_COSTOSTC_Sel_Existe", dc)
                Return blnExiste
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxTipoHonorario(ByVal vstrIDTipoHonario As String, Optional ByVal vintIDCab As Integer = 0) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDTipoHon", vstrIDTipoHonario)
                If Not blnExistenCostos(vintIDCab, vstrIDTipoHonario) Then
                    Return objADO.GetDataTable(True, "MACOSTOS_TIPOHONORARIO_Sel_ListxTipoHonorario", dc)
                Else
                    dc = New Dictionary(Of String, String)
                    dc.Add("@IDCab", vintIDCab)
                    Return objADO.GetDataTable(True, "COTICAB_COSTOSTC_Sel_ListxIDCab", dc)
                End If
                'Return Nothing
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsModoServicioBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAMODOSERVICIO_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarList(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAMODOSERVICIO_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvw(ByVal vstrCodModoServicio As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodModoServ", vstrCodModoServicio)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAMODOSERVICIO_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vstrCodModoServicio As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodModoServ", vstrCodModoServicio)
                Dim strSQl As String = "MAMODOSERVICIO_Sel_Pk"
                Return objADO.GetDataReader(strSQl, dc)

            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIdiomaBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDIdioma As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDIdioma", vstrIDIdioma)

                Return objADO.GetDataTable(True, "MAIDIOMAS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDIdioma As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDIdioma", vstrIDIdioma)

            Dim strSql As String = "MAIDIOMAS_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
        Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MAIDIOMAS_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboConSiglas() As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                Return objADO.GetDataTable(True, "MAIDIOMAS_Sel_CboConSiglas", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCulturasCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MAIDIOMAS_SelCulturas_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAIDIOMAS_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarRpt(ByVal vstrIDIdioma As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIdioma", vstrIDIdioma)
                Return objADO.GetDataTable(True, "MAIDIOMAS_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoServicioBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDservicio As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDservicio", vstrIDservicio)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOSERV_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDServicio As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDServicio", vstrIDServicio)

            Dim strSql As String = "MATIPOSERV_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOSERV_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATIPOSERV_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoUbigeoBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOUBIG_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrDescripcion As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@Descripcion", vstrDescripcion)

            Dim strSql As String = "MATIPOUBIG_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try

                Return objADO.GetDataTable(True, "MATIPOUBIG_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOUBIG_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATIPOUBIG_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsPlantillaBN

        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDDetalle As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDDetalle", vstrIDDetalle)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MADETALLE_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDDetalle As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDetalle", vstrIDDetalle)

            Dim strSql As String = "MADETALLE_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MADETALLE_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoDocIdentBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDIdentidad As String, _
                                      ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDIdentidad", vstrIDIdentidad)
                dc.Add("@Descripcion", vstrDescripcion)


                Return objADO.GetDataTable(True, "MATIPOIDENT_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDIdentidad As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDIdentidad", vstrIDIdentidad)

            Dim strSql As String = "MATIPOIDENT_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function


        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOIDENT_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarCbo(Optional ByVal vblnConCodigoSAP As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@ConCodigoSAP", vblnConCodigoSAP)

                Return objADO.GetDataTable(True, "MATIPOIDENT_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATIPOIDENT_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'JORGE
        Public Function Consultar_CodRail(ByVal vstrCodPeruRail As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodPeruRail", vstrCodPeruRail)
                Return objADO.GetDataTable(True, "MATIPOIDENT_Sel_PeruRail", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoOperacContabBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIdToc As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IdToc", vstrIdToc)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOOPERACION_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vstrIDToc As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDToc", vstrIDToc)

            Dim strSql As String = "MATIPOOPERACION_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarPk_DestOper(ByVal vstrCoDestOper As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoDestOper", vstrCoDestOper)

            Dim strSql As String = "MATIPOOPERACION_Sel_Pk_CoDestOper"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOOPERACION_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MATIPOOPERACION_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATIPOOPERACION_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTiposDeCambioUSDBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vdatFechaInicial As String, ByVal vdatFechaFin As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FecRango1", vdatFechaInicial)
                dc.Add("@FecRango2", vdatFechaFin)

                Return objADO.GetDataTable(True, "MATIPOSCAMBIO_USD_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vdatFeTipCam As Date, ByVal vstrCoMoneda As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FeTipCam", vdatFeTipCam)
                dc.Add("@CoMoneda", vstrCoMoneda)

                Return objADO.GetDataReader("MATIPOSCAMBIO_USD_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class


    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTiposdeCambioBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vdatFechaInicial As String, ByVal vdatFechaFin As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@FechaInicio", vdatFechaInicial)
                dc.Add("@FechaFin", vdatFechaFin)

                Return objADO.GetDataTable(True, "MATIPOCAMBIO_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vdatFecha As Date) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@Fecha", vdatFecha)

            Dim strSql As String = "MATIPOCAMBIO_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
        Public Function ConsultarRpt(ByVal vdatFecha As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Fecha", vdatFecha)
                Return objADO.GetDataTable(True, "MATIPOCAMBIO_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoCambio_MonedasBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vdatFechaInicial As String, ByVal vdatFechaFin As String, ByVal vstrCodMoneda As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@FechaInicio", vdatFechaInicial)
                dc.Add("@FechaFin", vdatFechaFin)
                dc.Add("@CoMoneda", vstrCodMoneda)

                Return objADO.GetDataTable(True, "MATIPOCAMBIO_MONEDAS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vdatFecha As Date, ByVal vstrCodMoneda As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@Fecha", vdatFecha)
            dc.Add("@CoMoneda", vstrCodMoneda)

            Dim strSql As String = "MATIPOCAMBIO_MONEDAS_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
        Public Function ConsultarRpt(ByVal vdatFechaInicial As String, ByVal vdatFechaFin As String, ByVal vstrCodMoneda As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FechaInicio", vdatFechaInicial)
                dc.Add("@FechaFin", vdatFechaFin)
                dc.Add("@CoMoneda", vstrCodMoneda)
                Return objADO.GetDataTable(True, "MATIPOCAMBIO_MONEDAS_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsBancoBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDBanco As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDBanco", vstrIDBanco)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MABANCOS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MABANCOS_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vstrIDBanco As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDBanco", vstrIDBanco)

            Dim strSql As String = "MABANCOS_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MABANCOS_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarCboTodos(ByVal vblnTodos As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Todos", vblnTodos)

                Return objADO.GetDataTable(True, "MABANCOS_Sel_CboTodos", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboList(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Todos", vblnTodos)

                Return objADO.GetDataTable(True, "MABANCOS_Sel_CboList", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboConTarjetaCredito(ByVal vblnTodos As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Todos", vblnTodos)

                Return objADO.GetDataTable(True, "MABANCOS_Sel_ListUnidoTarjetaCredito", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboOrdenPago() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MABANCOS_Sel_Cbo_OrdPag", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MABANCOS_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsHabitTripleBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIdHabitTriple As String, ByVal vstrAbreviacion As String, ByVal vstrDescripcion As String) As DataTable
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@IdHabitTriple", vstrIdHabitTriple)
                dc.Add("@Abreviacion", vstrAbreviacion)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAHABITTRIPLE_Sel_List", dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vstrIdTipoTriple As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IdHabitTriple", vstrIdTipoTriple)

            Dim strSql As String = "MAHABITTRIPLE_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)
        End Function

        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try

                Return objADO.GetDataTable(True, "MAHABITTRIPLE_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRpt(ByVal vstrAbreviacion As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@Abreviacion", vstrAbreviacion)
                    .Add("@Descripcion", vstrDescripcion)
                End With

                Return objADO.GetDataTable(True, "MAHABITTRIPLE_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvw(ByVal vstrIDHabitTriple As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDHabitTriple", vstrIDHabitTriple)
                    .Add("@Descripcion", vstrDescripcion)

                    Return objADO.GetDataTable(True, "MAHABITTRIPLE_Sel_Lvw", dc)
                End With
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsFormaPagoBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIdFor As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IdFor", vstrIdFor)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAFORMASPAGO_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vstrIDFor As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDFor", vstrIDFor)

            Dim strSql As String = "MAFORMASPAGO_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAFORMASPAGO_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                Return objADO.GetDataTable(True, "MAFORMASPAGO_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarCboxTipoxTodos(ByVal vstrTipo As String, ByVal vblnTodos As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Tipo", vstrTipo)
                dc.Add("@Todos", vblnTodos)
                Return objADO.GetDataTable(True, "MAFORMASPAGO_SelCbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboxTodos(ByVal vblnTodos As Boolean, Optional ByVal vblnNinguno As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Todos", vblnTodos)
                dc.Add("@Ninguno", vblnNinguno)
                Return objADO.GetDataTable(True, "MAFORMASPAGO_SelCbo_Todos", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo(ByVal vstrEsUtil As String, _
                                     ByVal vblnPorDefinir As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@EsUtil", vstrEsUtil)
                dc.Add("@PorDefinir", vblnPorDefinir)

                Return objADO.GetDataTable(True, "MAFORMASPAGO_Sel_CboxUtil", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarTarjetasCredito() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try

                Return objADO.GetDataTable(True, "MAFORMASPAGO_Sel_TarjCred", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MAFORMASPAGO_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsUbigeoBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDUbigeo As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDUbigeo", vstrIDUbigeo)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAUBIGEO_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDUbigeo As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDUbigeo", vstrIDUbigeo)

            Dim strSql As String = "MAUBIGEO_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAUBIGEO_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarCbo(Optional ByVal vstrTipo As String = "", _
                                     Optional ByVal vstrIDPais As String = "", _
                                     Optional ByVal vblnTodos As Boolean = False, _
                                     Optional ByVal vblnConDC As Boolean = True) As DataTable
            Dim dc As New Dictionary(Of String, String)

            If InStr(vstrTipo, Chr(34)) = 0 Then
                vstrTipo = Chr(34) & vstrTipo & Chr(34)
            End If


            Try
                dc.Add("@TipoUbig", vstrTipo)
                dc.Add("@IDPais", vstrIDPais)
                dc.Add("@bTodos", vblnTodos)
                If vblnConDC Then
                    Return objADO.GetDataTable(True, "MAUBIGEO_Sel_Cbo", dc)
                Else
                    Return objADO.GetDataTable(True, "MAUBIGEO_Sel_CboSinDC", dc)
                End If

            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Overloads Function ConsultarCboDC() As DataTable
            Try
                Return objADO.GetDataTable(True, "MAUBIGEO_Sel_CboSelCodigo", New Dictionary(Of String, String))
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboparaProvee(Optional ByVal vstrTipo As String = "", _
                                     Optional ByVal vstrIDPais As String = "", _
                                     Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@TipoUbig", vstrTipo)
                dc.Add("@IDPais", vstrIDPais)
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MAUBIGEO_Sel_CboProvee", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Overloads Function ConsultarNacionalidadCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAUBIGEO_SelNacionalidad_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function strDevuelveIDPaisxCiudad(ByVal vstrIDCiudad As String) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCiudad", vstrIDCiudad)
                dc.Add("@pIDPais", String.Empty)

                Return objADO.GetSPOutputValue("MAUBIGEO_Sel_IDPaisOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Overloads Function ConsultarNacionalidadCbo(ByVal blnTodos As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", blnTodos)

                Return objADO.GetDataTable(True, "MAUBIGEO_SelNacionalidad_Cbo2", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MAUBIGEO_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboCodigo(Optional ByVal vstrTipo As String = "", _
                                     Optional ByVal vstrIDPais As String = "", _
                                     Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@TipoUbig", vstrTipo)
                dc.Add("@IDPais", vstrIDPais)
                dc.Add("@bTodos", vblnTodos)

                Return objADO.GetDataTable(True, "MAUBIGEO_Sel_CboCodigo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarCboconCodigo(Optional ByVal vstrTipo As String = "", _
                             Optional ByVal vstrIDPais As String = "", _
                             Optional ByVal vblnTodos As Boolean = False, _
                             Optional ByVal vblnDescTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@TipoUbig", vstrTipo)
                dc.Add("@IDPais", vstrIDPais)
                dc.Add("@bTodos", vblnTodos)
                dc.Add("@bDescTodos", vblnDescTodos)

                Return objADO.GetDataTable(True, "MAUbigeo_Sel_CboconCodigo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarExisteDC_TipoUbigeo(ByVal vstrDC As String, ByVal vstrTipoUbigeo As String) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@DC", vstrDC)
                    .Add("@TipoUbig", vstrTipoUbigeo)
                    .Add("@pblnExiste", "")

                    Dim blnExiste As Boolean = objADO.ExecuteSPOutput("MAUBIGEO_Sel_ExisteDCTipoOutput", dc)
                    Return blnExiste
                End With
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarExisteDC_TipoUbigeoUpd(ByVal vstrIdUbigeo As String, ByVal vstrDC As String, ByVal vstrTipoUbigeo As String) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IdUbigeo", vstrIdUbigeo)
                    .Add("@DC", vstrDC)
                    .Add("@TipoUbig", vstrTipoUbigeo)
                    .Add("@pblnExiste", "")

                    Dim blnExiste As Boolean = objADO.ExecuteSPOutput("MAUBIGEO_Sel_ExisteDCTipoUpd_Output", dc)
                    Return blnExiste
                End With
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarOficinas() As DataTable
            Dim dc As New Dictionary(Of String, String)

            Dim strSql As String = "MAUBIGEO_Sel_Oficinas"
            Return objADO.GetDataTable(True, strSql, dc)

        End Function
        Public Function ConsultarListxDC(ByVal vstrDC As String)
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@DC", vstrDC)

                Return objADO.GetDataTable(True, "MAUBIGEO_SelxDC", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        'JORGE
        Public Function Consultar_CodRail(ByVal vstrCodigoPeruRail As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodigoPeruRail", vstrCodigoPeruRail)
                Return objADO.GetDataTable(True, "MAUBIGEO_Sel_PeruRail", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function strDevuelveIDNacionalidad(ByVal vstrNacionalidad As String) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Nacionalidad", vstrNacionalidad)
                dc.Add("@pIDNacionalidad", String.Empty)

                Return objADO.GetSPOutputValue("MAUBIGEO_Sel_IDNacionalidadOutput", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function Consultar_Oficinas() As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                'dc.Add("@IDUbigeo", vstrIDUbigeo)
                'dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAUBIGEO_Get_Oficinas", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function Consultar_Oficinas_Extranjeras() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAUBIGEO_Get_Oficinas_Extranjeras", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function Consultar_Oficinas_Proveedor(ByVal vstrIdProveedor As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IdProveedor", vstrIdProveedor)

                Return objADO.GetDataTable(True, "MAUBIGEO_Get_Oficinas_Proveedor", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function Consultar_Oficinas_Proveedor_Voucher(ByVal vstrIdProveedor As String, ByVal vintIdVoucher As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IdProveedor", vstrIdProveedor)
                dc.Add("@IDVoucher", vintIdVoucher)

                Return objADO.GetDataTable(True, "MAUBIGEO_Get_Oficinas_Voucher", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function Consultar_Voucher_Oficina(ByVal vintIdVoucher As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try

                dc.Add("@IDVoucher", vintIdVoucher)

                Return objADO.GetDataTable(True, "MAUBIGEO_Devolver_Voucher_Oficina", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarCiudadesTourAdicAPT(ByVal vstrCoTipo As String, ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDCab", vintIDCab)
                dc.Add("@CoTipoAPT", vstrCoTipo)
                Return objADO.GetDataTable(True, "MASERVICIOS_SelCiudadesFOC", dc)
                'If vstrCoTipo = "SE" Then
                '    Return objADO.GetDataTable(True, "MAUBIGEO_SelCiudadesTourAdicAPT", dc)
                'ElseIf vstrCoTipo = "FC" Then
                '    Return objADO.GetDataTable(True, "MASERVICIOS_SelCiudadesFOC", dc)
                'End If
                Return Nothing
            Catch ex As Exception
                Throw
            End Try

        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoOperacionesBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrObservacion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@DescAbrev", vstrObservacion)

                Return objADO.GetDataTable(True, "MAINCLUIDOOPERACIONES_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vintIDIncluido As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuIncluido", vintIDIncluido)

                Return objADO.GetDataReader("MAINCLUIDOOPERACIONES_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvw(ByVal vintNuIncluido As Integer, ByVal vstrDescAbrev As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuIncluido", vintNuIncluido)
                dc.Add("@DescAbrev", vstrDescAbrev)

                Return objADO.GetDataTable(True, "MAINCLUIDOOPERACIONES_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoOperacionesIdiomaBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarListxNuIncluido(ByVal vintNuIncluido As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@NuIncluido", vintNuIncluido)
                End With
                Return objADO.GetDataReader("MAINCLUIDOOPERACIONES_IDIOMAS_SelxNuIncluido", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIDIdioma(ByVal vstrIDioma As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIdioma", vstrIDioma)

                Return objADO.GetDataTable(True, "MAINCLUIDOOPERACIONES_IDIOMAS_SelxIDIdioma", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboIdiomas() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAINCLUIDOOPERACIONES_IDIOMAS_SelIDIdiomas", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarTraduccionesxIDIdioma(ByVal vstrIDIoma As String, ByVal vblnInterno As Boolean) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@IDIdioma", vstrIDIoma)
                    .Add("@FlInterno", vblnInterno)
                End With
                Return objADO.GetDataReader("MAINCLUIDOOPERACIONES_IDIOMAS_Sel_ObservxIdioma", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarTraduccionesxNuIncluidoxIDIdioma(ByVal vintNuIncluido As Int16, ByVal vstrIDIoma As String, ByVal vblnInterno As Boolean) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@NuIncluido", vintNuIncluido)
                    .Add("@IDIdioma", vstrIDIoma)
                    .Add("@FlInterno", vblnInterno)
                End With
                Return objADO.GetDataReader("MAINCLUIDOOPERACIONES_IDIOMAS_Sel_ObservxNuIncluidoxIdioma", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vintIDIncluido As Integer, ByVal vstrDescripcion As String, _
                                      ByVal vchrTipo As Char) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIncluido", vintIDIncluido)
                dc.Add("@DescAbrev", vstrDescripcion)
                dc.Add("@Tipo", vchrTipo)

                Return objADO.GetDataTable(True, "MAINCLUIDO_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListxIdioma(ByVal vstrIDIdioma As String, ByVal vintIDCab As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIdioma", vstrIDIdioma)
                dc.Add("@IDCab", vintIDCab)
                Return objADO.GetDataTable(True, "MAINCLUIDO_IDIOMAS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarxIDIncluido(ByVal vintIDIncluido As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIncluido", vintIDIncluido)
                Return objADO.GetDataReader("MAINCLUIDO_IDIOMAS_SelxIDIncluido", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvw(ByVal vintIDIncluido As Integer, ByVal vstrDescAbrev As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIncluido", vintIDIncluido)
                dc.Add("@DescAbrev", vstrDescAbrev)
                Return objADO.GetDataTable(True, "MAINCLUIDO_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vintIDIncluido As Integer) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDIncluido", vintIDIncluido)

            Dim strSql As String = "MAINCLUIDO_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)
        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String, _
                                      ByVal vchrTipo As Char) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                dc.Add("@Tipo", vchrTipo)
                Return objADO.GetDataTable(True, "MAINCLUIDO_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoServicioBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarListxIDIncluido(ByVal vintIDIncluido As Integer) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDIncluido", vintIDIncluido)

                Return objADO.GetDataTable(True, "MAINCLUIDO_SERVICIOS_SelxIDIncluido", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoProveedorBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDTipoProv As String, _
                                      ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDTipoProv", vstrIDTipoProv)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOPROVEEDOR_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vstrIDTipoProvee As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDTipoProv", vstrIDTipoProvee)

            Dim strSql As String = "MATIPOPROVEEDOR_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function
        Public Function ConsultarLvw(ByVal vstrID As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@ID", vstrID)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOPROVEEDOR_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False, _
                                     Optional ByVal vstrUserAccess As String = "") As DataTable
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@bTodos", vblnTodos)
            dc.Add("@UserAccess", vstrUserAccess)
            Try
                Return objADO.GetDataTable(True, "MATIPOPROVEEDOR_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarTiposProvConControlCalidad()
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MATIPOPROVEEDOR_SelxControlCalidad", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarTiposProvConControlCalidad_Prov(ByVal vstrCoProveedor As String)
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CoProveedor", vstrCoProveedor)
            Try
                Return objADO.GetDataTable(True, "MATIPOPROVEEDOR_SelxControlCalidad_IDProv", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function blnPideIdioma(ByVal vstrIDTipoProvee As String) As Boolean
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDTipoProv", vstrIDTipoProvee)
                dc.Add("@pPideIdioma", "1")

                Dim strSql As String = "MATIPOPROVEEDOR_Sel_PideIdiomaOutput"
                Return objADO.GetSPOutputValue(strSql, dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try

                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATIPOPROVEEDOR_Sel_Rpt", dc)
            Catch ex As Exception
                Throw

            End Try

        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsMonedaBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarPk(ByVal vstrCoMoneda As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDMoneda", vstrCoMoneda)
                Return objADO.GetDataReader("MAMONEDAS_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarCbo(Optional ByVal vstrMonedadNoConsiderada As String = "") As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDMonedaNoCo", vstrMonedadNoConsiderada)
                Return objADO.GetDataTable(True, "MAMONEDAS_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo_Multiple(Optional ByVal vstrMonedadNoConsiderada As String = "", Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDMonedaNoCo", vstrMonedadNoConsiderada)
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MAMONEDAS_Sel_Cbo_Multiple", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarconSimboloCbo(Optional ByVal vstrTipo As String = "") As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAMONEDAS_Sel_conSimboloCbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarconSimboloCboSAP() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAMONEDAS_Sel_conSimboloCboSAP", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarListCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Todos", vblnTodos)
                Return objADO.GetDataTable(True, "MAMONEDAS_Sel_CboList", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarxPais(ByVal vstrCoPais As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoPais", vstrCoPais)
                Return objADO.GetDataTable(True, "MAMONEDAS_SelxPais", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function drConsultar() As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)

            Return objADO.GetDataReader("MAMONEDAS_Sel_conSimboloCbo", dc)
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTarjetaCreditoBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@bTodos", vblnTodos)
            Try
                Return objADO.GetDataTable(True, "MATARJETACREDITO_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarList(ByVal vstrCodTar As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodTar", vstrCodTar)
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATARJETACREDITO_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvw(ByVal vstrCodTar As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CodTar", vstrCodTar)
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATARJETACREDITO_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vstrCodTar As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@CodTar", vstrCodTar)

            Dim Strsql As String = "MATARJETACREDITO_Sel_Pk"
            Return objADO.GetDataReader(Strsql, dc)
        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATARJETACREDITO_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
     <Transaction(TransactionOption.NotSupported)> _
    Public Class clsFechasEspecialesBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vdatFecha As Date, ByVal vstrIDUbigeo As String, Optional ByVal vstrCoPais As String = "") As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@Fecha", vdatFecha)
                dc.Add("@IDUbigeo", vstrIDUbigeo)
                dc.Add("@CoPais", vstrCoPais)

                Return objADO.GetDataTable(True, "MAFECHASNAC_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function
        Public Function ConsultarPk(ByVal vdatFecha As Date, ByVal vstrIDUbigeo As String) As SqlClient.SqlDataReader
            Try
                Dim dc As New Dictionary(Of String, String)
                dc.Add("@Fecha", vdatFecha)
                dc.Add("@IDUbigeo", vstrIDUbigeo)

                Dim strSql As String = "MAFECHASNAC_Sel_Pk"
                Return objADO.GetDataReader(strSql, dc)

            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarRpt(ByVal vdatFecha As Date, ByVal vstrIDUbigeo As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@Fecha", vdatFecha)
                dc.Add("@IDUbigeo", vstrIDUbigeo)

                Return objADO.GetDataTable(True, "MAFECHASNAC_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function strDevuelveDescripcionxFechaxUbigeo(ByVal vdatFecha As Date, ByVal vstrIDUbigeo As String) As String
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Fecha", vdatFecha)
                dc.Add("@IDUbigeo", vstrIDUbigeo)
                dc.Add("@pDescripcion", "")

                Return objADO.GetSPOutputValue("MAFECHASNAC_Sel_DescripcionFechaEspecialOutPut", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDesayunosBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarLvw(ByVal vstrIDDesayuno As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDesayuno", vstrIDDesayuno)
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MADESAYUNOS_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = True, Optional ByVal vblnDescTodos As Boolean = True) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", vblnTodos)
                dc.Add("@bDescTodos", vblnDescTodos)
                Return objADO.GetDataTable(True, "MADESAYUNOS_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vstrIDDesayuno As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDDesayuno", vstrIDDesayuno)
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MADESAYUNOS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vstrIDDesayuno As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDDesayuno", vstrIDDesayuno)

            Dim strSql As String = "MADESAYUNOS_Pk"
            Return objADO.GetDataReader(strSql, dc)
        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MADESAYUNOS_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class
    <ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
    Public Class clsFeriadosBN
        Dim objADO As New clsDataAccessDN

        Public Function blnEsFeriado(ByVal vdatFecha As Date, ByVal vstrIDPais As String) As Boolean
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@FechaFeriado", Format(vdatFecha, "dd/MM/yyyy"))
                dc.Add("@IDPais", vstrIDPais)
                dc.Add("@pbExists", 0)

                Return objADO.GetSPOutputValue("MAFERIADOS_Sel_ExisteOutput", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsEstadoObligacionesPagoBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarCbo(Optional ByVal vblnTodos As Boolean = False) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "ESTADO_OBLIGACIONESPAGO_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsEspecialidadBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try

                Return objADO.GetDataTable(True, "MAESPECIALIDAD_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboNacionalidad() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAESPECIALIDAD_SelNacionalidad_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsPlanCuentasBN
        Dim objADO As New clsDataAccessDN
        Public Function COnsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        'Agregado el dia 29-05-14
        'Marco Llapapasca
        Public Function ConsultarList_FLCLIENTES() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_List_FLCLIENTES", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        'Agregado el dia 29-05-14
        'Marco Llapapasca
        Public Function ConsultarList_FlFacturacion() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_List_FlFacturacion", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList_FlFacturacion_CentroCostos(ByVal vstrCoCeCos As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoCeCos", vstrCoCeCos)
                Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_List_FlFacturacion_CentroCostos", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList_FacturacionContabilidad(ByVal vstrFechaMes As String, ByVal vstrIDMoneda As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FechaMes", vstrFechaMes)
                dc.Add("@IDMoneda", vstrIDMoneda)
                Return objADO.GetDataTable(True, "ReporteFacturacionContabilidad", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarList_CuentaCorrientexCliente(ByVal vstrIDCliente As String, ByVal vstrNuDocum As String, _
                            ByVal vintDiaVencidos As Int16, ByVal vstrIDDebitMemo As String, ByVal vstrIDBanco As String, _
                            ByVal vdatFecEmisionRango1 As Date, ByVal vdatFecEmisionRango2 As Date, _
                            ByVal vdatFecIngresoRango1 As Date, ByVal vdatFecIngresoRango2 As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                'dc.Add("@FechaMes", vstrFechaMes)
                'dc.Add("@IDMoneda", vstrIDMoneda)
                dc.Add("@IDCliente", vstrIDCliente)
                dc.Add("@NuDocum", vstrNuDocum)
                dc.Add("@DiaVencidos", vintDiaVencidos)
                dc.Add("@IDDebitMemo", vstrIDDebitMemo)
                dc.Add("@IDBanco", vstrIDBanco)
                dc.Add("@FecEmisionRango1", vdatFecEmisionRango1)
                dc.Add("@FecEmisionRango2", vdatFecEmisionRango2)
                dc.Add("@FecIngresoRango1", vdatFecIngresoRango1)
                dc.Add("@FecIngresoRango2", vdatFecIngresoRango2)

                'Return objADO.GetDataTable(True, "Reporte_CuentaCorrientexCliente", dc)
                Return objADO.GetDataTable(True, "Rep_CuentaCorrientexCliente", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        'Agregado el dia 30-05-14
        'Marco Llapapasca
        Public Function ConsultarPk(ByVal vstrCtaContable As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CtaContable", vstrCtaContable)
                Return objADO.GetDataReader("MAPLANCUENTAS_Sel_PK", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarRpt() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarLvw(ByVal vstrCtaContable As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                With dc
                    .Add("@CtaContable", vstrCtaContable)
                    .Add("@Descripcion", vstrDescripcion)
                End With

                Return objADO.GetDataTable(True, "MAPLANCUENTAS_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    '*********************************************************************
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsRecordatorioBN
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vintIDRecordatorio As Short, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDRecordatorio", vintIDRecordatorio)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MARECORDATORIOSRESERVAS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarLvw(ByVal vintIDRecordatorio As Short, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDRecordatorio", vintIDRecordatorio)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MARECORDATORIOSRESERVAS_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarPk(ByVal vintIDRecordatorio As Short) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            dc.Add("@IDRecordatorio", vintIDRecordatorio)

            Dim strSql As String = "MARECORDATORIOSRESERVAS_Sel_Pk"
            Return objADO.GetDataReader(strSql, dc)

        End Function

        'Public Function ConsultarCbo() As DataTable
        '    Dim dc As New Dictionary(Of String, String)

        '    Try

        '        Return objADO.GetDataTable(True, "MABANCOS_Sel_Cbo", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try

        'End Function
        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MARECORDATORIOSRESERVAS_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class


    '*********************************************************************

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsParametroBN
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "PARAMETRO_Sel", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsProrrataBN
            Dim objADO As New clsDataAccessDN

            Public Function ConsultarList() As DataTable
                Try
                    Return objADO.GetDataTable(True, "PRORRATA_Sel_List", New Dictionary(Of String, String))
                Catch ex As Exception
                    Throw
                End Try
            End Function

            Public Function sglProrrataxFecha(ByVal vdatFecha As Date) As Single
                Try
                    Dim dc As New Dictionary(Of String, String)
                    dc.Add("@Fecha", Format(vdatFecha, "dd/MM/yyyy HH:mm:ss"))
                    'dc.Add("@Fecha", vdatFecha)
                    dc.Add("@pQtProrrata", 0)
                    Return objADO.ExecuteSPOutput("PRORRATA_SelxFechasOutput", dc)
                Catch ex As Exception
                    Throw
                End Try
            End Function
        End Class
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsVehiculosBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vblnEsOtraCiudad As Boolean) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FlOtraCiudad", vblnEsOtraCiudad)

                Return objADO.GetDataTable(True, "MAVEHICULOS_Sel_ListxFlOtraCiudad", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function


        Public Function ConsultarList2(ByVal vstrCoUbigeo As String, ByVal vstrIDTipoServ As String, vbytCoTipo As Byte, Optional ByVal vstrCoProveedor As String = "", Optional ByVal vstrTipo As String = "", Optional ByVal vstrIDUbigeoOri As String = "", Optional ByVal vstrIDUbigeoDes As String = "", Optional ByVal vstrIDCliente As String = "") As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoUbigeo", vstrCoUbigeo)
                dc.Add("@IDTipoServ", vstrIDTipoServ)
                dc.Add("@CoTipo", vbytCoTipo)
                dc.Add("@CoProveedor", vstrCoProveedor)
                dc.Add("@Tipo", vstrTipo)

                dc.Add("@IDUbigeoOri", vstrIDUbigeoOri)
                dc.Add("@IDUbigeoDes", vstrIDUbigeoDes)
                dc.Add("@IDCliente", vstrIDCliente)

                Return objADO.GetDataTable(True, "MAVEHICULOS_TRANSPORTE_Sel_ListxFlOtraCiudad", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAVEHICULOS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vbytNuVehiculo As Byte) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculo", vbytNuVehiculo)

                Return objADO.GetDataReader("MAVEHICULOS_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarIDxRango(ByVal vblnFlEsOtraCiudad As Boolean, ByVal vbytCantidad As Byte) As Byte
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim bytNuOpcion As Byte = 0
                dc.Add("@flOtraCiudad", vblnFlEsOtraCiudad)
                dc.Add("@Cantidad", vbytCantidad)
                dc.Add("@pNuVehiculo", bytNuOpcion)

                bytNuOpcion = objADO.ExecuteSPOutput("MAVEHICULOS_SelNuVehiculoxRango_Output", dc)
                Return bytNuOpcion
            Catch ex As Exception
                Throw
            End Try
        End Function

        'jorge
        Public Function ConsultarIDxRango2(ByVal vstrCoUbigeo As String, ByVal vbytCantidad As Byte, vbytCoTipo As Byte, ByVal vstrIDTipoServ As String, Optional ByVal vstrCoProveedor As String = "", Optional ByVal vstrTipo As String = "", Optional ByVal vstrIDUbigeoOri As String = "", Optional ByVal vstrIDUbigeoDes As String = "", Optional ByVal vstrIDCliente As String = "") As Byte
            Dim dc As New Dictionary(Of String, String)
            Try
                Dim bytNuOpcion As Byte = 0
                dc.Add("@CoUbigeo", vstrCoUbigeo)
                dc.Add("@Cantidad", vbytCantidad)
                dc.Add("@CoTipo", vbytCoTipo)
                dc.Add("@IDTipoServ", vstrIDTipoServ)

                dc.Add("@CoProveedor", vstrCoProveedor)
                dc.Add("@Tipo", vstrTipo)

                dc.Add("@IDUbigeoOri", vstrIDUbigeoOri)
                dc.Add("@IDUbigeoDes", vstrIDUbigeoDes)
                dc.Add("@IDCliente", vstrIDCliente)

                dc.Add("@pNuVehiculo", bytNuOpcion)

                bytNuOpcion = objADO.ExecuteSPOutput("MAVEHICULOS_SelNuVehiculoxRango_Output2", dc)
                Return bytNuOpcion
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsVehiculos_TransporteBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vbytCoTipo As Byte, vstrNoVehiculoCitado As String, ByVal vstrCoUbigeo As String, ByVal vbytCoMercado As Byte, Optional ByVal vstrCoPais As String = "") As DataTable

            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoTipo", vbytCoTipo)
                dc.Add("@NoVehiculoCitado", vstrNoVehiculoCitado)
                dc.Add("@CoUbigeo", vstrCoUbigeo)
                dc.Add("@CoMercado", vbytCoMercado)

                dc.Add("@CoPais", vstrCoPais)

                Return objADO.GetDataTable(True, "MAVEHICULOS_TRANSPORTE_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList_Prog(vstrNoVehiculoCitado As String, ByVal vstrCoUbigeo As String, Optional ByVal vstrTipo As String = "", Optional ByVal vstrIDCliente As String = "", Optional ByVal vstrCoPais As String = "") As DataTable

            Dim dc As New Dictionary(Of String, String)
            Try

                dc.Add("@NoVehiculoCitado", vstrNoVehiculoCitado)
                dc.Add("@CoUbigeo", vstrCoUbigeo)
                dc.Add("@Tipo", vstrTipo)
                dc.Add("@IDCliente", vstrIDCliente)
                dc.Add("@CoPais", vstrCoPais)


                Return objADO.GetDataTable(True, "MAVEHICULOS_TRANSPORTE_Sel_List_Prog", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vbytNuVehiculo As Byte) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculo", vbytNuVehiculo)

                Return objADO.GetDataReader("MAVEHICULOS_TRANSPORTE_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
 <Transaction(TransactionOption.NotSupported)> _
    Public Class clsMercadoBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarCbo(ByVal vblnTodos As Boolean) As DataTable

            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MAMERCADO_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo2(ByVal vblnTodos As Boolean) As DataTable

            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@bTodos", vblnTodos)
                Return objADO.GetDataTable(True, "MAMERCADO_Sel_Cbo2", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsVehiculosProgramacionBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarList(ByVal vstrNoUnidadProg As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NoUnidadProg", vstrNoUnidadProg)

                Return objADO.GetDataTable(True, "MAVEHICULOSPROGRAMACION_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vintNuVehiculoProg As Int16) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NuVehiculoProg", vintNuVehiculoProg)

                Return objADO.GetDataReader("MAVEHICULOSPROGRAMACION_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
        Public Function ConsultarCbo() As Data.DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MAVEHICULOSPROGRAMACION_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    '*********************************************************************
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDiasFeriadosBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN
        Public Function ConsultarList(ByVal vstrIDPais As String, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDPais", vstrIDPais)
                dc.Add("@Descripcion", vstrDescripcion)

                Return objADO.GetDataTable(True, "MAFERIADOS_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        'Public Function ConsultarLvw(ByVal vintIDRecordatorio As Short, ByVal vstrDescripcion As String) As DataTable
        '    Dim dc As New Dictionary(Of String, String)

        '    Try
        '        dc.Add("@IDRecordatorio", vintIDRecordatorio)
        '        dc.Add("@Descripcion", vstrDescripcion)

        '        Return objADO.GetDataTable(True, "MARECORDATORIOSRESERVAS_Sel_Lvw", dc)
        '    Catch ex As Exception
        '        Throw
        '    End Try

        'End Function

        'Public Function ConsultarPk(ByVal vintIDRecordatorio As Short) As SqlClient.SqlDataReader
        '    Dim dc As New Dictionary(Of String, String)
        '    dc.Add("@IDRecordatorio", vintIDRecordatorio)

        '    Dim strSql As String = "MARECORDATORIOSRESERVAS_Sel_Pk"
        '    Return objADO.GetDataReader(strSql, dc)

        'End Function

        ''Public Function ConsultarCbo() As DataTable
        ''    Dim dc As New Dictionary(Of String, String)

        ''    Try

        ''        Return objADO.GetDataTable(True, "MABANCOS_Sel_Cbo", dc)
        ''    Catch ex As Exception
        ''        Throw
        ''    End Try

        ''End Function

        Public Function ConsultarCbo(ByVal tipo As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@tipo", tipo)
                Return objADO.GetDataTable(True, "LISTAR_DIA_MES", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function



        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MAFERIADOSD_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class


    '*********************************************************************

    Public Function ConsultarParametro() As SqlClient.SqlDataReader
        Dim dc As New Dictionary(Of String, String)
        Try
            Dim objADO As New clsDataAccessDN

            Return objADO.GetDataReader("PARAMETRO_Sel", dc)
        Catch ex As Exception
            Throw
        End Try
    End Function

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCapacidadHabitacionBN
        Inherits ServicedComponent
        '## Autor          : Dony Tinoco
        '## Fecha creación : 28.11.2013 
        '## Descripcion    : Clase parcial utilizada para mostrar la capacidad de las habitaciones
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "CAPACIDAD_HABITAC_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vstrNoCapacidad As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@NoCapacidad", vstrNoCapacidad)
                Return objADO.GetDataTable(True, "CAPACIDAD_HABITAC_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vstrCoCapacidad As String) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@CoCapacidad", vstrCoCapacidad)
                Return objADO.GetDataReader("CAPACIDAD_HABITAC_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function
    End Class

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTemporadaBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function ConsultarCboXTipoServ(ByVal vstrIDServicio As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_CboXTipoServ", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboXTipo(ByVal vstrTipoTemporada As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@TipoTemporada", vstrTipoTemporada)
                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_CboXTipo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCbo() As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_Cbo", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboServicioAPT(ByVal vstrIDServicio As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio", vstrIDServicio)
                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_Cbo_ServicioAPT", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCboDetServicioAPT(ByVal vintIDServicio_Det As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDServicio_Det", vintIDServicio_Det)
                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_Cbo_DetServicioAPT", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarPk(ByVal vintIDTemporada As Int16) As SqlClient.SqlDataReader
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@IDTemporada", vintIDTemporada)

                Return objADO.GetDataReader("MATEMPORADA_Sel_Pk", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarList(ByVal vintIDIdentidad As Int16, _
                                      ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDTemporada", vintIDIdentidad)
                dc.Add("@NombreTemporada", vstrDescripcion)


                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_List", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarLvw(ByVal vintID As Int16, ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)

            Try
                dc.Add("@IDTemporada", vintID)
                dc.Add("@NombreTemporada", vstrDescripcion)

                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_Lvw", dc)
            Catch ex As Exception
                Throw
            End Try

        End Function

        Public Function ConsultarRpt(ByVal vstrDescripcion As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@Descripcion", vstrDescripcion)
                Return objADO.GetDataTable(True, "MATEMPORADA_Sel_Rpt", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class


    <ComVisible(True)> _
  <Transaction(TransactionOption.NotSupported)> _
    Public Class clsENTRADASINC_SAPBN
        Inherits ServicedComponent
        Dim objADO As New clsDataAccessDN

        Public Function Consultar(ByVal vdatFePago1 As Date, _
                                      ByVal vdatFePago2 As Date, ByVal vdatFechaINCusco1 As Date, ByVal vdatFechaINCusco2 As Date, strTipo As String) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FePago1", vdatFePago1)
                dc.Add("@FePago2", vdatFePago2)
                dc.Add("@FechaINCusco1", vdatFechaINCusco1)
                dc.Add("@FechaINCusco2", vdatFechaINCusco2)

                Dim strSP As String = ""

                If strTipo = "I" Then
                    strSP = "ENTRADASINC_SAP_List"
                ElseIf strTipo = "C" Then
                    strSP = "ENTRADASINC_SAP_List_CONSSETUR"
                ElseIf strTipo = "B" Then
                    strSP = "ENTRADASINC_SAP_List_BTG"
                End If

                Return objADO.GetDataTable(True, strSP, dc)

            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarINC(ByVal vdatFePago1 As Date, _
                                        ByVal vdatFePago2 As Date, ByVal vdatFechaINCusco1 As Date, ByVal vdatFechaINCusco2 As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FePago1", vdatFePago1)
                dc.Add("@FePago2", vdatFePago2)
                dc.Add("@FechaINCusco1", vdatFechaINCusco1)
                dc.Add("@FechaINCusco2", vdatFechaINCusco2)
                Return objADO.GetDataTable(True, "ENTRADASINC_SAP_List", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarCONSETTUR(ByVal vdatFePago1 As Date, _
                                      ByVal vdatFePago2 As Date, ByVal vdatFechaINCusco1 As Date, ByVal vdatFechaINCusco2 As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FePago1", vdatFePago1)
                dc.Add("@FePago2", vdatFePago2)
                dc.Add("@FechaINCusco1", vdatFechaINCusco1)
                dc.Add("@FechaINCusco2", vdatFechaINCusco2)
                Return objADO.GetDataTable(True, "ENTRADASINC_SAP_List_CONSSETUR", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Function ConsultarBTG(ByVal vdatFePago1 As Date, _
                                    ByVal vdatFePago2 As Date, ByVal vdatFechaINCusco1 As Date, ByVal vdatFechaINCusco2 As Date) As DataTable
            Dim dc As New Dictionary(Of String, String)
            Try
                dc.Add("@FePago1", vdatFePago1)
                dc.Add("@FePago2", vdatFePago2)
                dc.Add("@FechaINCusco1", vdatFechaINCusco1)
                dc.Add("@FechaINCusco2", vdatFechaINCusco2)
                Return objADO.GetDataTable(True, "ENTRADASINC_SAP_List_BTG", dc)
            Catch ex As Exception
                Throw
            End Try
        End Function

    End Class
End Class
