﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports ComSEToursBE2

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsProfitCenterBT
    Inherits ServicedComponent
    Public objWebBT As New clsJsonInterfaceBT

    Public Sub GrabarActualizarSAP(ByVal BE As clsProfitCenterBE, ByVal ParamArray varPropertiesDelete As String())
        'If BE Is Nothing Then Exit Sub
        Try
            'objWebBT.EnviarDatosRESTFulJson(BE, "http://sap:8899/api/ProfitCenter/", "POST", varPropertiesDelete)
            objWebBT.EnviarDatosRESTFulJson(BE, "http://sap:" + gstrPuertoSAP + "/api/ProfitCenter/", "POST", varPropertiesDelete)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub
End Class