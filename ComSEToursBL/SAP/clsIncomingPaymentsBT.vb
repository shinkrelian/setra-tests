﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports ComSEToursBE2

<ComVisible(True)> _
<Transaction(TransactionOption.Required, Isolation:=TransactionIsolationLevel.ReadCommitted)> _
Public Class clsIncomingPaymentsBT
    Inherits ServicedComponent

    Public objWebBT As New clsJsonInterfaceBT
    Public Function GrabarSAP(ByVal BE As clsIncomingPaymentsBE, ByVal ParamArray varPropertiesDelete As String()) As String
        Try
            'objWebBT.EnviarDatosRESTFulJson(BE, "http://sap:8899/api/IncomingPayments/", "POST", varPropertiesDelete)
            objWebBT.EnviarDatosRESTFulJson(BE, "http://sap:" + gstrPuertoSAP + "/api/IncomingPayments/", "POST", varPropertiesDelete)

            Return objResponseSAP.Id
            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Function

    Public Sub AnularSAP(ByVal BE As clsIncomingPaymentsBE, ByVal ParamArray varPropertiesDelete As String())
        Try
            'objWebBT.EnviarDatosRESTFulJson(BE, "http://sap:8899/api/IncomingPayments/" & BE.U_SYP_NUMOPER, "DELETE", varPropertiesDelete)

            objWebBT.EnviarDatosRESTFulJson(BE, "http://sap:" + gstrPuertoSAP + "/api/IncomingPayments/" & BE.U_SYP_NUMOPER, "DELETE", varPropertiesDelete)

            ContextUtil.SetComplete()
        Catch ex As Exception
            ContextUtil.SetAbort()
            Throw
        End Try
    End Sub


End Class
