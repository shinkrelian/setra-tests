﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsOperacionBaseBE
    'Inherits ServicedComponent

    Private _IDOperacion As String
    Public Property IDOperacion() As String
        Get
            Return _IDOperacion
        End Get
        Set(ByVal value As String)
            _IDOperacion = value
        End Set
    End Property

    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

    Private _Accion As Char
    Public Property Accion() As Char
        Get
            Return _Accion
        End Get
        Set(ByVal value As Char)
            _Accion = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOperacionBE
    Inherits clsOperacionBaseBE

    Private _IDReserva As Integer
    Public Property IDReserva() As Integer
        Get
            Return _IDReserva
        End Get
        Set(ByVal value As Integer)
            _IDReserva = value
        End Set
    End Property

    Private _Fecha As Date
    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _IDFile As String
    Public Property IDFile() As String
        Get
            Return _IDFile
        End Get
        Set(ByVal value As String)
            _IDFile = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    Private _Observaciones As String
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

    Private _IDUsuarioOpe As String
    Public Property IDUsuarioOpe() As String
        Get
            Return _IDUsuarioOpe
        End Get
        Set(ByVal value As String)
            _IDUsuarioOpe = value
        End Set
    End Property

    Private _IDUsuarioOpeCusco As String
    Public Property IDUsuarioOpeCusco() As String
        Get
            Return _IDUsuarioOpeCusco
        End Get
        Set(ByVal value As String)
            _IDUsuarioOpeCusco = value
        End Set
    End Property

    Private _ListaDetalleOperaciones As List(Of clsDetalleOperacionBE)
    Public Property ListaDetalleOperaciones() As List(Of clsDetalleOperacionBE)
        Get
            Return _ListaDetalleOperaciones
        End Get
        Set(ByVal value As List(Of clsDetalleOperacionBE))
            _ListaDetalleOperaciones = value
        End Set
    End Property

    Private _ListaOperaciones As List(Of clsOperacionBE)
    Public Property ListaOperaciones() As List(Of clsOperacionBE)
        Get
            Return _ListaOperaciones
        End Get
        Set(ByVal value As List(Of clsOperacionBE))
            _ListaOperaciones = value
        End Set
    End Property

    Private _ListaPax As List(Of clsCotizacionBE.clsCotizacionPaxBE)
    Public Property ListaPax() As List(Of clsCotizacionBE.clsCotizacionPaxBE)
        Get
            Return _ListaPax
        End Get
        Set(ByVal value As List(Of clsCotizacionBE.clsCotizacionPaxBE))
            _ListaPax = value
        End Set
    End Property

    Private _ListaVouchers As List(Of clsVoucherOperacionBE)
    Public Property ListaVouchers() As List(Of clsVoucherOperacionBE)
        Get
            Return _ListaVouchers
        End Get
        Set(ByVal value As List(Of clsVoucherOperacionBE))
            _ListaVouchers = value
        End Set
    End Property

    Private _ListaPaxDetalle As List(Of clsPaxDetalleOperacionBE)
    Public Property ListaPaxDetalle() As List(Of clsPaxDetalleOperacionBE)
        Get
            Return _ListaPaxDetalle
        End Get
        Set(ByVal value As List(Of clsPaxDetalleOperacionBE))
            _ListaPaxDetalle = value
        End Set
    End Property

    Private _ListaDetServiciosOperac As List(Of clsDetalleServiciosOperacionBE)
    Public Property ListaDetServiciosOperac() As List(Of clsDetalleServiciosOperacionBE)
        Get
            Return _ListaDetServiciosOperac
        End Get
        Set(ByVal value As List(Of clsDetalleServiciosOperacionBE))
            _ListaDetServiciosOperac = value
        End Set
    End Property

    Private _ListaSincerado As List(Of clsSinceradoBE)
    Public Property ListaSincerado() As List(Of clsSinceradoBE)
        Get
            Return _ListaSincerado
        End Get
        Set(ByVal value As List(Of clsSinceradoBE))
            _ListaSincerado = value
        End Set
    End Property

    Private _ListaSincerado_Det As List(Of clsSincerado_DetBE)
    Public Property ListaSincerado_Det() As List(Of clsSincerado_DetBE)
        Get
            Return _ListaSincerado_Det
        End Get
        Set(ByVal value As List(Of clsSincerado_DetBE))
            _ListaSincerado_Det = value
        End Set
    End Property

    Private _ListaSincerado_Det_Pax As List(Of clsSincerado_Det_PaxBE)
    Public Property ListaSincerado_Det_Pax() As List(Of clsSincerado_Det_PaxBE)
        Get
            Return _ListaSincerado_Det_Pax
        End Get
        Set(ByVal value As List(Of clsSincerado_Det_PaxBE))
            _ListaSincerado_Det_Pax = value
        End Set
    End Property

    Private _ListaVoucherHistorico As List(Of clsVoucherHistoricoBE)
    Public Property ListaVoucherHistorico() As List(Of clsVoucherHistoricoBE)
        Get
            Return _ListaVoucherHistorico
        End Get
        Set(ByVal value As List(Of clsVoucherHistoricoBE))
            _ListaVoucherHistorico = value
        End Set
    End Property

    Private _CotizacionObservacion As CotizacionObservacion
    Public Property CotizacionObservacion() As CotizacionObservacion
        Get
            Return _CotizacionObservacion
        End Get
        Set(ByVal value As CotizacionObservacion)
            _CotizacionObservacion = value
        End Set
    End Property

End Class

Public Class CotizacionObservacion
    Private _IDCab As Integer
    Private _ObservacionVentas As String

    Public Sub New(pIDCab As Integer, pObservacionVentas As String)
        _IDCab = pIDCab
        _ObservacionVentas = pObservacionVentas
    End Sub

    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Public Property ObservacionVentas() As String
        Get
            Return _ObservacionVentas
        End Get
        Set(ByVal value As String)
            _ObservacionVentas = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenServicioBE
    Inherits clsOperacionBaseBE
    Private _NuOrden_Servicio As String
    Public Property NuOrden_Servicio() As String
        Get
            Return _NuOrden_Servicio
        End Get
        Set(ByVal value As String)
            _NuOrden_Servicio = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    Private _IDUsuarioResponsable As String
    Public Property IDUsuarioResponsable() As String
        Get
            Return _IDUsuarioResponsable
        End Get
        Set(ByVal value As String)
            _IDUsuarioResponsable = value
        End Set
    End Property

    Private _NuVehiculoProg As Byte
    Public Property NuVehiculoProg() As Byte
        Get
            Return _NuVehiculoProg
        End Get
        Set(ByVal value As Byte)
            _NuVehiculoProg = value
        End Set
    End Property

    Private _CoOrden_Servicio As String
    Public Property CoOrden_Servicio() As String
        Get
            Return _CoOrden_Servicio
        End Get
        Set(ByVal value As String)
            _CoOrden_Servicio = value
        End Set
    End Property

    Private _IDProveedorSetours As String
    Public Property IDProveedorSetours() As String
        Get
            Return _IDProveedorSetours
        End Get
        Set(ByVal value As String)
            _IDProveedorSetours = value
        End Set
    End Property

    Private _SsSaldo As Double
    Public Property SsSaldo() As Double
        Get
            Return _SsSaldo
        End Get
        Set(ByVal value As Double)
            _SsSaldo = value
        End Set
    End Property

    Private _SsDifAceptada As Double
    Public Property SsDifAceptada() As Double
        Get
            Return _SsDifAceptada
        End Get
        Set(ByVal value As Double)
            _SsDifAceptada = value
        End Set
    End Property

    Private _TxObsDocumento As String
    Public Property TxObsDocumento() As String
        Get
            Return _TxObsDocumento
        End Get
        Set(ByVal value As String)
            _TxObsDocumento = value
        End Set
    End Property

    Private _FlDesdeOtraForEgreso As Boolean
    Public Property FlDesdeOtraForEgreso() As Boolean
        Get
            Return _FlDesdeOtraForEgreso
        End Get
        Set(ByVal value As Boolean)
            _FlDesdeOtraForEgreso = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _ListaOrdenesServiciosDetalles As List(Of clsOrdenServicioDetalleBE)
    Public Property ListaOrdenesServiciosDetalles() As List(Of clsOrdenServicioDetalleBE)
        Get
            Return _ListaOrdenesServiciosDetalles
        End Get
        Set(ByVal value As List(Of clsOrdenServicioDetalleBE))
            _ListaOrdenesServiciosDetalles = value
        End Set
    End Property

    Private _ListaOrdenesServiciosDetallesAsignados As List(Of clsOrdenServicioDetalle_AsignadosBE)
    Public Property ListaOrdenesServiciosDetallesAsignados() As List(Of clsOrdenServicioDetalle_AsignadosBE)
        Get
            Return _ListaOrdenesServiciosDetallesAsignados
        End Get
        Set(ByVal value As List(Of clsOrdenServicioDetalle_AsignadosBE))
            _ListaOrdenesServiciosDetallesAsignados = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenServicioDetalleBE
    Inherits clsOrdenServicioBE

    Private _NuOrden_Servicio_Det As String
    Public Property NuOrden_Servicio_Det() As String
        Get
            Return _NuOrden_Servicio_Det
        End Get
        Set(ByVal value As String)
            _NuOrden_Servicio_Det = value
        End Set
    End Property

    Private _IDOperacion_Det As Integer
    Public Property IDOperacion_Det() As Integer
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As Integer)
            _IDOperacion_Det = value
        End Set
    End Property

    Private _IDServicio_Det As String
    Public Property IDServicio_Det() As String
        Get
            Return _IDServicio_Det
        End Get
        Set(ByVal value As String)
            _IDServicio_Det = value
        End Set
    End Property

    ' ''Private _CoPNR As String
    ' ''Public Property CoPNR() As String
    ' ''    Get
    ' ''        Return _CoPNR
    ' ''    End Get
    ' ''    Set(ByVal value As String)
    ' ''        _CoPNR = value
    ' ''    End Set
    ' ''End Property
    Private _FlServicioNoShow As Boolean
    Public Property FlServicioNoShow() As Boolean
        Get
            Return _FlServicioNoShow
        End Get
        Set(ByVal value As Boolean)
            _FlServicioNoShow = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenServicioDetalle_AsignadosBE
    Inherits clsOrdenServicioDetalleBE
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsVoucherOperacionBE
    Inherits clsOperacionBE
    Private _IdOperacion_Det As String
    Public Property IdOperacion_Det() As String
        Get
            Return _IdOperacion_Det
        End Get
        Set(ByVal value As String)
            _IdOperacion_Det = value
        End Set
    End Property

    Private _IDVoucher As Integer
    Public Property IDVoucher() As Integer
        Get
            Return _IDVoucher
        End Get
        Set(ByVal value As Integer)
            _IDVoucher = value
        End Set
    End Property

    Private _IDVoucherAnt As Integer
    Public Property IDVoucherAnt() As Integer
        Get
            Return _IDVoucherAnt
        End Get
        Set(ByVal value As Integer)
            _IDVoucherAnt = value
        End Set
    End Property

    Private _ServExportac As Boolean
    Public Property ServExportac() As Boolean
        Get
            Return _ServExportac
        End Get
        Set(ByVal value As Boolean)
            _ServExportac = value
        End Set
    End Property

    Private _ExtrNoIncluid As Boolean
    Public Property ExtrNoIncluid() As Boolean
        Get
            Return _ExtrNoIncluid
        End Get
        Set(ByVal value As Boolean)
            _ExtrNoIncluid = value
        End Set
    End Property

    Private _ObservVoucher As String
    Public Property ObservVoucher() As String
        Get
            Return _ObservVoucher
        End Get
        Set(ByVal value As String)
            _ObservVoucher = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _SsSaldo As Double
    Public Property SsSaldo() As Double
        Get
            Return _SsSaldo
        End Get
        Set(ByVal value As Double)
            _SsSaldo = value
        End Set
    End Property

    Private _SsDifAceptada As Double
    Public Property SsDifAceptada() As Double
        Get
            Return _SsDifAceptada
        End Get
        Set(ByVal value As Double)
            _SsDifAceptada = value
        End Set
    End Property

    Private _TxObsDocumento As String
    Public Property TxObsDocumento() As String
        Get
            Return _TxObsDocumento
        End Get
        Set(ByVal value As String)
            _TxObsDocumento = value
        End Set
    End Property

    Private _FlDesdeOtraForEgreso As Boolean
    Public Property FlDesdeOtraForEgreso() As Boolean
        Get
            Return _FlDesdeOtraForEgreso
        End Get
        Set(ByVal value As Boolean)
            _FlDesdeOtraForEgreso = value
        End Set
    End Property

    Private _CoUbigeo_Oficina As String
    Public Property CoUbigeo_Oficina() As String
        Get
            Return _CoUbigeo_Oficina
        End Get
        Set(ByVal value As String)
            _CoUbigeo_Oficina = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsVoucherHistoricoBE
    Inherits clsOperacionBaseBE
    Private _IDVoucher As Integer
    Public Property IDVoucher() As Integer
        Get
            Return _IDVoucher
        End Get
        Set(ByVal value As Integer)
            _IDVoucher = value
        End Set
    End Property

    Private _IDOperacion_Det As Integer
    Public Property IDOperacion_Det() As Integer
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As Integer)
            _IDOperacion_Det = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleOperacionBE
    'Inherits clsVoucherOperacionBE
    Inherits clsCotizacionBE.clsDetalleCotizacionBE


    Private _IDOperacion As String
    Public Property IDOperacion() As String
        Get
            Return _IDOperacion
        End Get
        Set(ByVal value As String)
            _IDOperacion = value
        End Set
    End Property

    Private _IDOperacion_Det As String
    Public Property IDOperacion_Det() As String
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As String)
            _IDOperacion_Det = value
        End Set
    End Property

    'Private _IDReserva_Det As String
    'Public Property IDReserva_Det() As String
    '    Get
    '        Return _IDReserva_Det
    '    End Get
    '    Set(ByVal value As String)
    '        _IDReserva_Det = value
    '    End Set
    'End Property

    Private _Cantidad As Byte
    Public Property Cantidad() As Byte
        Get
            Return _Cantidad
        End Get
        Set(ByVal value As Byte)
            _Cantidad = value
        End Set
    End Property

    Private _CantidadaPagar As Byte
    Public Property CantidadaPagar() As Byte
        Get
            Return _CantidadaPagar
        End Get
        Set(ByVal value As Byte)
            _CantidadaPagar = value
        End Set
    End Property

    Private _FechaOut As Date
    Public Property FechaOut() As Date
        Get
            Return _FechaOut
        End Get
        Set(ByVal value As Date)
            _FechaOut = value
        End Set
    End Property

    Private _Noches As Byte
    Public Property Noches() As Byte
        Get
            Return _Noches
        End Get
        Set(ByVal value As Byte)
            _Noches = value
        End Set
    End Property

    Private _IDVoucher As Integer
    Public Property IDVoucher() As Integer
        Get
            Return _IDVoucher
        End Get
        Set(ByVal value As Integer)
            _IDVoucher = value
        End Set
    End Property

    'Private _FechaRecordatorio As Date
    'Public Property FechaRecordatorio() As Date
    '    Get
    '        Return _FechaRecordatorio
    '    End Get
    '    Set(ByVal value As Date)
    '        _FechaRecordatorio = value
    '    End Set
    'End Property

    'Private _Recordatorio As String
    'Public Property Recordatorio() As String
    '    Get
    '        Return _Recordatorio
    '    End Get
    '    Set(ByVal value As String)
    '        _Recordatorio = value
    '    End Set
    'End Property

    Private _ObservVoucher As String
    Public Property ObservVoucher() As String
        Get
            Return _ObservVoucher
        End Get
        Set(ByVal value As String)
            _ObservVoucher = value
        End Set
    End Property

    Private _ObservBiblia As String
    Public Property ObservBiblia() As String
        Get
            Return _ObservBiblia
        End Get
        Set(ByVal value As String)
            _ObservBiblia = value
        End Set
    End Property

    Private _ObservInterno As String
    Public Property ObservInterno() As String
        Get
            Return _ObservInterno
        End Get
        Set(ByVal value As String)
            _ObservInterno = value
        End Set
    End Property

    Private _VerObserVoucher As Boolean
    Public Property VerObserVoucher() As Boolean
        Get
            Return _VerObserVoucher
        End Get
        Set(ByVal value As Boolean)
            _VerObserVoucher = value
        End Set
    End Property

    Private _VerObserBiblia As Boolean
    Public Property VerObserBiblia() As Boolean
        Get
            Return _VerObserBiblia
        End Get
        Set(ByVal value As Boolean)
            _VerObserBiblia = value
        End Set
    End Property

    Private _NetoGen As Double
    Public Property NetoGen() As Double
        Get
            Return _NetoGen
        End Get
        Set(ByVal value As Double)
            _NetoGen = value
        End Set
    End Property

    Private _IgvGen As Single
    Public Property IgvGen() As Single
        Get
            Return _IgvGen
        End Get
        Set(ByVal value As Single)
            _IgvGen = value
        End Set
    End Property

    Dim _NetoHab As Double
    Public Property NetoHab() As Double
        Get
            Return _NetoHab
        End Get
        Set(ByVal value As Double)
            _NetoHab = value
        End Set
    End Property

    Dim _IgvHab As Double
    Public Property IgvHab() As Double
        Get
            Return _IgvHab
        End Get
        Set(ByVal value As Double)
            _IgvHab = value
        End Set
    End Property

    Dim _TotalHab As Double
    Public Property TotalHab() As Double
        Get
            Return _TotalHab
        End Get
        Set(ByVal value As Double)
            _TotalHab = value
        End Set
    End Property

    Dim _TotalGen As Double
    Public Property TotalGen() As Double
        Get
            Return _TotalGen
        End Get
        Set(ByVal value As Double)
            _TotalGen = value
        End Set
    End Property

    Private _IDTipoOC As String
    Public Property IDTipoOC() As String
        Get
            Return _IDTipoOC
        End Get
        Set(ByVal value As String)
            _IDTipoOC = value
        End Set
    End Property

    Private _IDGuiaProveedor As String
    Public Property IDGuiaProveedor() As String
        Get
            Return _IDGuiaProveedor
        End Get
        Set(ByVal value As String)
            _IDGuiaProveedor = value
        End Set
    End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    Private _FlServicioNoCobrado As Boolean
    Public Property FlServicioNoCobrado() As Boolean
        Get
            Return _FlServicioNoCobrado
        End Get
        Set(ByVal value As Boolean)
            _FlServicioNoCobrado = value
        End Set
    End Property

    Private _SSTotalGenAntNoCobrado As Double
    Public Property SSTotalGenAntNoCobrado() As Double
        Get
            Return _SSTotalGenAntNoCobrado
        End Get
        Set(ByVal value As Double)
            _SSTotalGenAntNoCobrado = value
        End Set
    End Property
    Private _QtCantidadAPagarAntNoCobrado As Byte
    Public Property QtCantidadAPagarAntNoCobrado() As Byte
        Get
            Return _QtCantidadAPagarAntNoCobrado
        End Get
        Set(ByVal value As Byte)
            _QtCantidadAPagarAntNoCobrado = value
        End Set
    End Property

    Private _CantidadAPagarEditado As Boolean
    Public Property CantidadAPagarEditado() As Boolean
        Get
            Return _CantidadAPagarEditado
        End Get
        Set(ByVal value As Boolean)
            _CantidadAPagarEditado = value
        End Set
    End Property

    Private _NetoHabEditado As Boolean
    Public Property NetoHabEditado() As Boolean
        Get
            Return _NetoHabEditado
        End Get
        Set(ByVal value As Boolean)
            _NetoHabEditado = value
        End Set
    End Property

    Private _IgvHabEditado As Boolean
    Public Property IgvHabEditado() As Boolean
        Get
            Return _IgvHabEditado
        End Get
        Set(ByVal value As Boolean)
            _IgvHabEditado = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPaxDetalleOperacionBE
    Inherits clsOperacionBaseBE

    Private _IDOperacion_Det As String
    Public Property IDOperacion_Det() As String
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As String)
            _IDOperacion_Det = value
        End Set
    End Property

    Private _IDPax As Integer
    Public Property IDPax() As Integer
        Get
            Return _IDPax
        End Get
        Set(ByVal value As Integer)
            _IDPax = value
        End Set
    End Property

    Private _Selecc As Boolean
    Public Property Selecc() As Boolean
        Get
            Return _Selecc
        End Get
        Set(ByVal value As Boolean)
            _Selecc = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsControlCalidadProBE
    Inherits clsOperacionBaseBE

    Private _NuControlCalidad As Integer
    Public Property NuControlCalidad() As Integer
        Get
            Return _NuControlCalidad
        End Get
        Set(ByVal value As Integer)
            _NuControlCalidad = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _TxComentario As String
    Public Property TxComentario() As String
        Get
            Return _TxComentario
        End Get
        Set(ByVal value As String)
            _TxComentario = value
        End Set
    End Property

    Private _IDPax As Integer
    Public Property IDPax() As Integer
        Get
            Return _IDPax
        End Get
        Set(ByVal value As Integer)
            _IDPax = value
        End Set
    End Property

    Private _CoProveedor As String
    Public Property CoProveedor() As String
        Get
            Return _CoProveedor
        End Get
        Set(ByVal value As String)
            _CoProveedor = value
        End Set
    End Property

    Private _CoCliente As String
    Public Property CoCliente() As String
        Get
            Return _CoCliente
        End Get
        Set(ByVal value As String)
            _CoCliente = value
        End Set
    End Property

    Private _ListaControlCalidadPro_Cuestionario As List(Of clsControlCalidadPro_CuestionarioBE)
    Public Property ListaControlCalidadPro_Cuestionario() As List(Of clsControlCalidadPro_CuestionarioBE)
        Get
            Return _ListaControlCalidadPro_Cuestionario
        End Get
        Set(ByVal value As List(Of clsControlCalidadPro_CuestionarioBE))
            _ListaControlCalidadPro_Cuestionario = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsControlCalidadPro_CuestionarioBE
    Inherits clsOperacionBaseBE

    Private _NuControlCalidad As Integer
    Public Property NuControlCalidad() As Integer
        Get
            Return _NuControlCalidad
        End Get
        Set(ByVal value As Integer)
            _NuControlCalidad = value
        End Set
    End Property

    Private _NuCuest As Byte
    Public Property NuCuest() As Byte
        Get
            Return _NuCuest
        End Get
        Set(ByVal value As Byte)
            _NuCuest = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    Private _FlExcelent As Boolean
    Public Property FlExcelent() As Boolean
        Get
            Return _FlExcelent
        End Get
        Set(ByVal value As Boolean)
            _FlExcelent = value
        End Set
    End Property

    Private _FlVeryGood As Boolean
    Public Property FlVeryGood() As Boolean
        Get
            Return _FlVeryGood
        End Get
        Set(ByVal value As Boolean)
            _FlVeryGood = value
        End Set
    End Property

    Private _FlGood As Boolean
    Public Property FlGood() As Boolean
        Get
            Return _FlGood
        End Get
        Set(ByVal value As Boolean)
            _FlGood = value
        End Set
    End Property

    Private _FlAverage As Boolean
    Public Property FlAverage() As Boolean
        Get
            Return _FlAverage
        End Get
        Set(ByVal value As Boolean)
            _FlAverage = value
        End Set
    End Property

    Private _FlPoor As Boolean
    Public Property FlPoor() As Boolean
        Get
            Return _FlPoor
        End Get
        Set(ByVal value As Boolean)
            _FlPoor = value
        End Set
    End Property

    Private _TxComentario As String
    Public Property TxComentario() As String
        Get
            Return _TxComentario
        End Get
        Set(ByVal value As String)
            _TxComentario = value
        End Set
    End Property

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsSinceradoBE
    Inherits clsOperacionBaseBE
    Private _NuSincerado As String
    Public Property NuSincerado() As String
        Get
            Return _NuSincerado
        End Get
        Set(ByVal value As String)
            _NuSincerado = value
        End Set
    End Property

    Private _IDOperacion_Det As Integer
    Public Property IDOperacion_Det() As Integer
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As Integer)
            _IDOperacion_Det = value
        End Set
    End Property

    Private _IDServicio_Det As Integer
    Public Property IDServicio_Det() As Integer
        Get
            Return _IDServicio_Det
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsStockTicketEntradasBE
    Inherits clsOperacionBaseBE

    Private _IDServicio_Det As Integer
    Public Property IDServicio_Det() As Integer
        Get
            Return _IDServicio_Det
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det = value
        End Set
    End Property

    Private _IDServicio As String
    Public Property IDServicio() As String
        Get
            Return _IDServicio
        End Get
        Set(ByVal value As String)
            _IDServicio = value
        End Set
    End Property

    Private _QtStkEnt As Int16
    Public Property QtStkEnt() As Int16
        Get
            Return _QtStkEnt
        End Get
        Set(ByVal value As Int16)
            _QtStkEnt = value
        End Set
    End Property

    Private _QtEntCompradas As Int16
    Public Property QtEntCompradas() As Int16
        Get
            Return _QtEntCompradas
        End Get
        Set(ByVal value As Int16)
            _QtEntCompradas = value
        End Set
    End Property

    Private _ListStockTicketEntIngreso As List(Of clsStockTicketEntradasIngresoBE)
    Public Property ListStockTicketEntIngreso() As List(Of clsStockTicketEntradasIngresoBE)
        Get
            Return _ListStockTicketEntIngreso
        End Get
        Set(ByVal value As List(Of clsStockTicketEntradasIngresoBE))
            _ListStockTicketEntIngreso = value
        End Set
    End Property

    Private _ListCotiStockEntIngreso As List(Of clsCotiStockTicketEntradasIngresoBE)
    Public Property ListCotiStockEntIngreso() As List(Of clsCotiStockTicketEntradasIngresoBE)
        Get
            Return _ListCotiStockEntIngreso
        End Get
        Set(ByVal value As List(Of clsCotiStockTicketEntradasIngresoBE))
            _ListCotiStockEntIngreso = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsCotiStockTicketEntradasIngresoBE

    Private _NuStockFile As Integer
    Public Property NuStockFile() As Integer
        Get
            Return _NuStockFile
        End Get
        Set(ByVal value As Integer)
            _NuStockFile = value
        End Set
    End Property

    Private _NuTckEntIng As Integer
    Public Property NuTckEntIng() As Integer
        Get
            Return _NuTckEntIng
        End Get
        Set(ByVal value As Integer)
            _NuTckEntIng = value
        End Set
    End Property

    Private _IDServicio_Det As Integer
    Public Property IDServicio_Det() As Integer
        Get
            Return _IDServicio_Det
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _QtUtilizada As Int16
    Public Property QtUtilizada() As Int16
        Get
            Return _QtUtilizada
        End Get
        Set(ByVal value As Int16)
            _QtUtilizada = value
        End Set
    End Property

    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsStockTicketEntradasIngresoBE
    Inherits clsStockTicketEntradasBE
    Private _NuTckEntIng As Byte
    Public Property NuTckEntIng() As Byte
        Get
            Return _NuTckEntIng
        End Get
        Set(ByVal value As Byte)
            _NuTckEntIng = value
        End Set
    End Property

    Private _FeComprada As Date
    Public Property FeComprada() As Date
        Get
            Return _FeComprada
        End Get
        Set(ByVal value As Date)
            _FeComprada = value
        End Set
    End Property

    Private _FlSustentado As Boolean
    Public Property FlSustentado() As Boolean
        Get
            Return _FlSustentado
        End Get
        Set(ByVal value As Boolean)
            _FlSustentado = value
        End Set
    End Property

    Private _IDTipoDocSus As String
    Public Property IDTipoDocSus() As String
        Get
            Return _IDTipoDocSus
        End Get
        Set(ByVal value As String)
            _IDTipoDocSus = value
        End Set
    End Property

    Private _NroDocSus As String
    Public Property NroDocSus() As String
        Get
            Return _NroDocSus
        End Get
        Set(ByVal value As String)
            _NroDocSus = value
        End Set
    End Property

    Private _NuOperBan As String
    Public Property NuOperBan() As String
        Get
            Return _NuOperBan
        End Get
        Set(ByVal value As String)
            _NuOperBan = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsSincerado_Det_PaxBE
    Inherits clsSincerado_DetBE

    Private _NuSincerado_Det_Pax As String
    Public Property NuSincerado_Det_Pax() As String
        Get
            Return _NuSincerado_Det_Pax
        End Get
        Set(ByVal value As String)
            _NuSincerado_Det_Pax = value
        End Set
    End Property

    Private _IDPax As Integer
    Public Property IDPax() As Integer
        Get
            Return _IDPax
        End Get
        Set(ByVal value As Integer)
            _IDPax = value
        End Set
    End Property
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsSincerado_DetBE
    Inherits clsSinceradoBE
    Private _NuSinceradoDet As String
    Public Property NuSincerado_Det() As String
        Get
            Return _NuSinceradoDet
        End Get
        Set(ByVal value As String)
            _NuSinceradoDet = value
        End Set
    End Property

    ''Private _IDPax As Integer
    ''Public Property IDPax() As Integer
    ''    Get
    ''        Return _IDPax
    ''    End Get
    ''    Set(ByVal value As Integer)
    ''        _IDPax = value
    ''    End Set
    ''End Property

    Private _QtPax As Byte
    Public Property QtPax() As Byte
        Get
            Return _QtPax
        End Get
        Set(ByVal value As Byte)
            _QtPax = value
        End Set
    End Property

    Private _QtPaxLiberado As String
    Public Property QtPaxLiberado() As String
        Get
            Return _QtPaxLiberado
        End Get
        Set(ByVal value As String)
            _QtPaxLiberado = value
        End Set
    End Property

    Private _SsCostoNeto As Double
    Public Property SsCostoNeto() As Double
        Get
            Return _SsCostoNeto
        End Get
        Set(ByVal value As Double)
            _SsCostoNeto = value
        End Set
    End Property

    Private _SsCostoIgv As Double
    Public Property SsCostoIgv() As Double
        Get
            Return _SsCostoIgv
        End Get
        Set(ByVal value As Double)
            _SsCostoIgv = value
        End Set
    End Property

    Private _SsCostoTotal As Double
    Public Property SsCostoTotal() As Double
        Get
            Return _SsCostoTotal
        End Get
        Set(ByVal value As Double)
            _SsCostoTotal = value
        End Set
    End Property
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleServiciosOperacionBE
    Inherits clsCotizacionBE.clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE


    Private _IDOperacion_Det As String
    Public Property IDOperacion_Det() As String
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As String)
            _IDOperacion_Det = value
        End Set
    End Property

    Private _Total_Prg As Double
    Public Property Total_Prg() As Double
        Get
            Return _Total_Prg
        End Get
        Set(ByVal value As Double)
            _Total_Prg = value
        End Set
    End Property

    Private _ObservEdicion As String
    Public Property ObservEdicion() As String
        Get
            Return _ObservEdicion
        End Get
        Set(ByVal value As String)
            _ObservEdicion = value
        End Set
    End Property

    Private _IDEstadoSolicitud As String
    Public Property IDEstadoSolicitud() As String
        Get
            Return _IDEstadoSolicitud
        End Get
        Set(ByVal value As String)
            _IDEstadoSolicitud = value
        End Set
    End Property

    Private _IDCorreoEstadoAC As String
    Public Property IDCorreoEstadoAC() As String
        Get
            Return _IDCorreoEstadoAC
        End Get
        Set(ByVal value As String)
            _IDCorreoEstadoAC = value
        End Set
    End Property

    Private _IDProveedor_Prg_Temp As String
    Public Property IDProveedor_Prg_Temp() As String
        Get
            Return _IDProveedor_Prg_Temp
        End Get
        Set(ByVal value As String)
            _IDProveedor_Prg_Temp = value
        End Set
    End Property

    Private _IDProveedor_Prg As String
    Public Property IDProveedor_Prg() As String
        Get
            Return _IDProveedor_Prg
        End Get
        Set(ByVal value As String)
            _IDProveedor_Prg = value
        End Set
    End Property

    Private _IDServicio_Det_V_Prg As Integer
    Public Property IDServicio_Det_V_Prg() As Integer
        Get
            Return _IDServicio_Det_V_Prg
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det_V_Prg = value
        End Set
    End Property

    Private _Total_PrgEditado As Boolean
    Public Property Total_PrgEditado() As Boolean
        Get
            Return _Total_PrgEditado
        End Get
        Set(ByVal value As Boolean)
            _Total_PrgEditado = value
        End Set
    End Property

    Private _TipoCambio As Single
    Public Property TipoCambio() As Single
        Get
            Return _TipoCambio
        End Get
        Set(ByVal value As Single)
            _TipoCambio = value
        End Set
    End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    Private _NetoProgram As Double
    Public Property NetoProgram() As Double
        Get
            Return _NetoProgram
        End Get
        Set(ByVal value As Double)
            _NetoProgram = value
        End Set
    End Property

    Private _IgvProgram As Single
    Public Property IgvProgram() As Single
        Get
            Return _IgvProgram
        End Get
        Set(ByVal value As Single)
            _IgvProgram = value
        End Set
    End Property

    Private _TotalProgram As Double
    Public Property TotalProgram() As Double
        Get
            Return _TotalProgram
        End Get
        Set(ByVal value As Double)
            _TotalProgram = value
        End Set
    End Property


    Private _NetoCotizado As Double
    Public Property NetoCotizado() As Double
        Get
            Return _NetoCotizado
        End Get
        Set(ByVal value As Double)
            _NetoCotizado = value
        End Set
    End Property

    Private _IgvCotizado As Single
    Public Property IgvCotizado() As Single
        Get
            Return _IgvCotizado
        End Get
        Set(ByVal value As Single)
            _IgvCotizado = value
        End Set
    End Property

    Private _TotalCotizado As Double
    Public Property TotalCotizado() As Double
        Get
            Return _TotalCotizado
        End Get
        Set(ByVal value As Double)
            _TotalCotizado = value
        End Set
    End Property

    Private _IngManual As Boolean
    Public Property IngManual() As Boolean
        Get
            Return _IngManual
        End Get
        Set(ByVal value As Boolean)
            _IngManual = value
        End Set
    End Property

    Private _Activo As Boolean
    Public Property Activo() As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property

    Private _FlMontosSincerados As Boolean
    Public Property FlMontosSincerados() As Boolean
        Get
            Return _FlMontosSincerados
        End Get
        Set(ByVal value As Boolean)
            _FlMontosSincerados = value
        End Set
    End Property

    Private _CoEstadoSincerado As String
    Public Property CoEstadoSincerado() As String
        Get
            Return _CoEstadoSincerado
        End Get
        Set(ByVal value As String)
            _CoEstadoSincerado = value
        End Set
    End Property

    Private _FlServicioNoCobrado As Boolean
    Public Property FlServicioNoCobrado() As Boolean
        Get
            Return _FlServicioNoCobrado
        End Get
        Set(ByVal value As Boolean)
            _FlServicioNoCobrado = value
        End Set
    End Property

    Private _SSTotalProgramAntNoCobrado As Double
    Public Property SSTotalProgramAntNoCobrado() As Double
        Get
            Return _SSTotalProgramAntNoCobrado
        End Get
        Set(ByVal value As Double)
            _SSTotalProgramAntNoCobrado = value
        End Set
    End Property

    'Pax + Liberados
    Private _QtPax As Int16
    Public Property QtPax() As Int16
        Get
            Return _QtPax
        End Get
        Set(ByVal value As Int16)
            _QtPax = value
        End Set
    End Property

End Class
