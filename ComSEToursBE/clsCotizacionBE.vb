﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsCotizacionBE
    ''Inherits ServicedComponent
    'Inherits ServicedComponent

    'prueba VSS
    Private _IdCab As Int32
    Public Property IdCab() As Int32
        Get
            Return _IdCab
        End Get
        Set(ByVal value As Int32)
            _IdCab = value
        End Set
    End Property

    Private _IDCabCopia As Integer
    Public Property IDCabCopia() As Integer
        Get
            Return _IDCabCopia
        End Get
        Set(ByVal value As Integer)
            _IDCabCopia = value
        End Set
    End Property

    Private _Cotizacion As String
    Public Property Cotizacion() As String
        Get
            Return _Cotizacion
        End Get
        Set(ByVal value As String)
            _Cotizacion = value
        End Set
    End Property

    Private _Estado As Char
    Public Property Estado() As Char
        Get
            Return _Estado
        End Get
        Set(ByVal value As Char)
            _Estado = value
        End Set
    End Property

    Private _IDCliente As String
    Public Property IDCliente() As String
        Get
            Return _IDCliente
        End Get
        Set(ByVal value As String)
            _IDCliente = value
        End Set
    End Property

    ''PPMG20160420
    Private _IDContactoCliente As String
    Public Property IDContactoCliente() As String
        Get
            Return _IDContactoCliente
        End Get
        Set(ByVal value As String)
            _IDContactoCliente = value
        End Set
    End Property

    Private _Titulo As String
    Public Property Titulo() As String
        Get
            Return _Titulo
        End Get
        Set(ByVal value As String)
            _Titulo = value
        End Set
    End Property

    Private _TipoPax As String
    Public Property TipoPax() As String
        Get
            Return _TipoPax
        End Get
        Set(ByVal value As String)
            _TipoPax = value
        End Set
    End Property

    Private _CotiRelacion As String
    Public Property CotiRelacion() As String
        Get
            Return _CotiRelacion
        End Get
        Set(ByVal value As String)
            _CotiRelacion = value
        End Set
    End Property

    Private _IGV As Single
    Public Property IGV() As Single
        Get
            Return _IGV
        End Get
        Set(ByVal value As Single)
            _IGV = value
        End Set
    End Property

    Private _IDUsuario As String
    Public Property IDUsuario() As String
        Get
            Return _IDUsuario
        End Get
        Set(ByVal value As String)
            _IDUsuario = value
        End Set
    End Property

    Private _NroPax As Int16
    Public Property NroPax() As Int16
        Get
            Return _NroPax
        End Get
        Set(ByVal value As Int16)
            _NroPax = value
        End Set
    End Property

    Private _NroLiberados As Int16
    Public Property NroLiberados() As Int16
        Get
            Return _NroLiberados
        End Get
        Set(ByVal value As Int16)
            _NroLiberados = value
        End Set
    End Property

    Private _Tipo_Lib As Char
    Public Property Tipo_Lib() As Char
        Get
            Return _Tipo_Lib
        End Get
        Set(ByVal value As Char)
            _Tipo_Lib = value
        End Set
    End Property

    Private _Margen As Double
    Public Property Margen() As Double
        Get
            Return _Margen
        End Get
        Set(ByVal value As Double)
            _Margen = value
        End Set
    End Property

    Private _IDBanco As String
    Public Property IDBanco() As String
        Get
            Return _IDBanco
        End Get
        Set(ByVal value As String)
            _IDBanco = value
        End Set
    End Property

    Private _IDFile As String
    Public Property IDFile() As String
        Get
            Return _IDFile
        End Get
        Set(ByVal value As String)
            _IDFile = value
        End Set
    End Property

    Private _IDTipoDoc As String
    Public Property IDTipoDoc() As String
        Get
            Return _IDTipoDoc
        End Get
        Set(ByVal value As String)
            _IDTipoDoc = value
        End Set
    End Property


    Private _Observaciones As String
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

    Private _FecInicio As Date
    Public Property FecInicio() As Date
        Get
            Return _FecInicio
        End Get
        Set(ByVal value As Date)
            _FecInicio = value
        End Set
    End Property

    Private _FechaOut As Date
    Public Property FechaOut() As Date
        Get
            Return _FechaOut
        End Get
        Set(ByVal value As Date)
            _FechaOut = value
        End Set
    End Property

    Private _Cantidad_Dias As Byte
    Public Property Cantidad_Dias() As Byte
        Get
            Return _Cantidad_Dias
        End Get
        Set(ByVal value As Byte)
            _Cantidad_Dias = value
        End Set
    End Property

    Private _FechaSeg As Date
    Public Property FechaSeg() As Date
        Get
            Return _FechaSeg
        End Get
        Set(ByVal value As Date)
            _FechaSeg = value
        End Set
    End Property

    Private _DatoSeg As String
    Public Property DatoSeg() As String
        Get
            Return _DatoSeg
        End Get
        Set(ByVal value As String)
            _DatoSeg = value
        End Set
    End Property

    Private _TipoCambio As Double
    Public Property TipoCambio() As Double
        Get
            Return _TipoCambio
        End Get
        Set(ByVal value As Double)
            _TipoCambio = value
        End Set
    End Property

    Private _DocVta As Int32
    Public Property DocVta() As Int32
        Get
            Return _DocVta
        End Get
        Set(ByVal value As Int32)
            _DocVta = value
        End Set
    End Property

    Private _TipoServicio As String
    Public Property TipoServicio() As String
        Get
            Return _TipoServicio
        End Get
        Set(ByVal value As String)
            _TipoServicio = value
        End Set
    End Property

    Private _IDIdioma As String
    Public Property IDIdioma() As String
        Get
            Return _IDIdioma
        End Get
        Set(ByVal value As String)
            _IDIdioma = value
        End Set
    End Property

    Private _Simple As Byte
    Public Property Simple() As Byte
        Get
            Return _Simple
        End Get
        Set(ByVal value As Byte)
            _Simple = value
        End Set
    End Property

    Private _Twin As Byte
    Public Property Twin() As Byte
        Get
            Return _Twin
        End Get
        Set(ByVal value As Byte)
            _Twin = value
        End Set
    End Property

    Private _Matrimonial As Byte
    Public Property Matrimonial() As Byte
        Get
            Return _Matrimonial
        End Get
        Set(ByVal value As Byte)
            _Matrimonial = value
        End Set
    End Property

    Private _Triple As Byte
    Public Property Triple() As Byte
        Get
            Return _Triple
        End Get
        Set(ByVal value As Byte)
            _Triple = value
        End Set
    End Property

    Private _SimpleResidente As Byte
    Public Property SimpleResidente() As Byte
        Get
            Return _SimpleResidente
        End Get
        Set(ByVal value As Byte)
            _SimpleResidente = value
        End Set
    End Property

    Private _TwinResidente As Byte
    Public Property TwinResidente() As Byte
        Get
            Return _TwinResidente
        End Get
        Set(ByVal value As Byte)
            _TwinResidente = value
        End Set
    End Property

    Private _MatrimonialResidente As Byte
    Public Property MatrimonialResidente() As Byte
        Get
            Return _MatrimonialResidente
        End Get
        Set(ByVal value As Byte)
            _MatrimonialResidente = value
        End Set
    End Property

    Private _TripleResidente As Byte
    Public Property TripleResidente() As Byte
        Get
            Return _TripleResidente
        End Get
        Set(ByVal value As Byte)
            _TripleResidente = value
        End Set
    End Property

    Private _IDUbigeo As String
    Public Property IDUbigeo() As String
        Get
            Return _IDUbigeo
        End Get
        Set(ByVal value As String)
            _IDUbigeo = value
        End Set
    End Property

    Private _Notas As String
    Public Property Notas() As String
        Get
            Return _Notas
        End Get
        Set(ByVal value As String)
            _Notas = value
        End Set
    End Property

    Private _Programa_Desc As String
    Public Property Programa_Desc() As String
        Get
            Return _Programa_Desc
        End Get
        Set(ByVal value As String)
            _Programa_Desc = value
        End Set
    End Property

    Private _PaxAdulE As Byte
    Public Property PaxAdulE() As Byte
        Get
            Return _PaxAdulE
        End Get
        Set(ByVal value As Byte)
            _PaxAdulE = value
        End Set
    End Property

    Private _PaxAdulN As Byte
    Public Property PaxAdulN() As Byte
        Get
            Return _PaxAdulN
        End Get
        Set(ByVal value As Byte)
            _PaxAdulN = value
        End Set
    End Property

    Private _PaxNinoE As Byte
    Public Property PaxNinoE() As Byte
        Get
            Return _PaxNinoE
        End Get
        Set(ByVal value As Byte)
            _PaxNinoE = value
        End Set
    End Property

    Private _PaxNinoN As Byte
    Public Property PaxNinoN() As Byte
        Get
            Return _PaxNinoN
        End Get
        Set(ByVal value As Byte)
            _PaxNinoN = value
        End Set
    End Property

    Private _Aporte As Boolean
    Public Property Aporte() As Boolean
        Get
            Return _Aporte
        End Get
        Set(ByVal value As Boolean)
            _Aporte = value
        End Set
    End Property

    Private _AporteMonto As Double
    Public Property AporteMonto() As Double
        Get
            Return _AporteMonto
        End Get
        Set(ByVal value As Double)
            _AporteMonto = value
        End Set
    End Property

    Private _AporteTotal As Double
    Public Property AporteTotal() As Double
        Get
            Return _AporteTotal
        End Get
        Set(ByVal value As Double)
            _AporteTotal = value
        End Set
    End Property

    Private _Redondeo As Single
    Public Property Redondeo() As Single
        Get
            Return _Redondeo
        End Get
        Set(ByVal value As Single)
            _Redondeo = value
        End Set
    End Property

    Private _Categoria As String
    Public Property Categoria() As String
        Get
            Return _Categoria
        End Get
        Set(ByVal value As String)
            _Categoria = value
        End Set
    End Property

    Private _IDUsuarioRes As String
    Public Property IDUsuarioRes() As String
        Get
            Return _IDUsuarioRes
        End Get
        Set(ByVal value As String)
            _IDUsuarioRes = value
        End Set
    End Property

    Private _MargenCero As Boolean
    Public Property MargenCero() As Boolean
        Get
            Return _MargenCero
        End Get
        Set(ByVal value As Boolean)
            _MargenCero = value
        End Set
    End Property

    Private _DiasReconfirmacion As Byte
    Public Property DiasReconfirmacion() As Byte
        Get
            Return _DiasReconfirmacion
        End Get
        Set(ByVal value As Byte)
            _DiasReconfirmacion = value
        End Set
    End Property

    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

    Private _CambiosAcomodos As Boolean
    Public Property CambiosAcomodos() As Boolean
        Get
            Return _CambiosAcomodos
        End Get
        Set(ByVal value As Boolean)
            _CambiosAcomodos = value
        End Set
    End Property

    Private _RegeneradoxAcomodo As Boolean
    Public Property RegeneradoxAcomodo() As Boolean
        Get
            Return _RegeneradoxAcomodo
        End Get
        Set(ByVal value As Boolean)
            _RegeneradoxAcomodo = value
        End Set
    End Property

    Private _CambioMatTwin As Boolean
    Public Property CambioMatTwin() As Boolean
        Get
            Return _CambioMatTwin
        End Get
        Set(ByVal value As Boolean)
            _CambioMatTwin = value
        End Set
    End Property

    Private _ObservacionGeneral As String
    Public Property ObservacionGeneral() As String
        Get
            Return _ObservacionGeneral
        End Get
        Set(ByVal value As String)
            _ObservacionGeneral = value
        End Set
    End Property

    Private _ObservacionVentas As String
    Public Property ObservacionVentas() As String
        Get
            Return _ObservacionVentas
        End Get
        Set(ByVal value As String)
            _ObservacionVentas = value
        End Set
    End Property

    Private _CoTipoVenta As String
    Public Property CoTipoVenta() As String
        Get
            Return _CoTipoVenta
        End Get
        Set(ByVal value As String)
            _CoTipoVenta = value
        End Set
    End Property

    Private _DocWordVersion As Byte
    Public Property DocWordVersion() As Byte
        Get
            Return _DocWordVersion
        End Get
        Set(ByVal value As Byte)
            _DocWordVersion = value
        End Set
    End Property

    Private _DocWordFecha As Date
    Public Property DocWordFecha() As Date
        Get
            Return _DocWordFecha
        End Get
        Set(ByVal value As Date)
            _DocWordFecha = value
        End Set
    End Property

    Private _NuSerieCliente As Integer
    Public Property NuSerieCliente() As Integer
        Get
            Return _NuSerieCliente
        End Get
        Set(ByVal value As Integer)
            _NuSerieCliente = value
        End Set
    End Property

    Private _CotizCopiada As Boolean
    Public Property CotizCopiada() As Boolean
        Get
            Return _CotizCopiada
        End Get
        Set(ByVal value As Boolean)
            _CotizCopiada = value
        End Set
    End Property

    Private _PoConcretVta As Decimal
    Public Property PoConcretVta() As Decimal
        Get
            Return _PoConcretVta
        End Get
        Set(ByVal value As Decimal)
            _PoConcretVta = value
        End Set
    End Property

    Private _BlPoConcretVta_CheckManual As Boolean
    Public Property BlPoConcretVta_CheckManual() As Boolean
        Get
            Return _BlPoConcretVta_CheckManual
        End Get
        Set(ByVal value As Boolean)
            _BlPoConcretVta_CheckManual = value
        End Set
    End Property

    Private _FlPoConcretManualUpd As Boolean
    Public Property FlPoConcretManualUpd() As Boolean
        Get
            Return _FlPoConcretManualUpd
        End Get
        Set(ByVal value As Boolean)
            _FlPoConcretManualUpd = value
        End Set
    End Property

    Private _FlTourConductor As Boolean
    Public Property FlTourConductor() As Boolean
        Get
            Return _FlTourConductor
        End Get
        Set(ByVal value As Boolean)
            _FlTourConductor = value
        End Set
    End Property

    Private _CoSerieWeb As String
    Public Property CoSerieWeb() As String
        Get
            Return _CoSerieWeb
        End Get
        Set(ByVal value As String)
            _CoSerieWeb = value
        End Set
    End Property

    Private _FlFileMigA_SAP As Boolean
    Public Property FlFileMigA_SAP() As Boolean
        Get
            Return _FlFileMigA_SAP
        End Get
        Set(ByVal value As Boolean)
            _FlFileMigA_SAP = value
        End Set
    End Property

    Private _FlCorreoAnuEnviado As Boolean
    Public Property FlCorreoAnuEnviado() As Boolean
        Get
            Return _FlCorreoAnuEnviado
        End Get
        Set(ByVal value As Boolean)
            _FlCorreoAnuEnviado = value
        End Set
    End Property

    Private _FlCorreoEliEnviado As Boolean
    Public Property FlCorreoEliEnviado() As Boolean
        Get
            Return _FlCorreoEliEnviado
        End Get
        Set(ByVal value As Boolean)
            _FlCorreoEliEnviado = value
        End Set
    End Property

    Private _NuPedInt As Integer
    Public Property NuPedInt() As Integer
        Get
            Return _NuPedInt
        End Get
        Set(ByVal value As Integer)
            _NuPedInt = value
        End Set
    End Property

    ''PPMG20160311
    Private _FlEnviarMPWP As Boolean
    Public Property FlEnviarMPWP() As Boolean
        Get
            Return _FlEnviarMPWP
        End Get
        Set(ByVal value As Boolean)
            _FlEnviarMPWP = value
        End Set
    End Property

    'Private _FlCorreoEnviadoPendAcp As Boolean
    'Public Property FlCorreoEnviadoPendAcp() As Boolean
    '    Get
    '        Return _FlCorreoEnviadoPendAcp
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _FlCorreoEnviadoPendAcp = value
    '    End Set
    'End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionQuiebreBE
        'Inherits ServicedComponent
        Private _IDCab_Q As Int32
        Public Property IDCab_Q() As Int32
            Get
                Return _IDCab_Q
            End Get
            Set(ByVal value As Int32)
                _IDCab_Q = value
            End Set
        End Property


        Private _IDCab As Int32
        Public Property IDCab() As Int32
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Int32)
                _IDCab = value
            End Set
        End Property

        Private _Pax As Int16
        Public Property Pax() As Int16
            Get
                Return _Pax
            End Get
            Set(ByVal value As Int16)
                _Pax = value
            End Set
        End Property

        Private _Liberados As Int16
        Public Property Liberados() As Int16
            Get
                Return _Liberados
            End Get
            Set(ByVal value As Int16)
                _Liberados = value
            End Set
        End Property

        Private _Tipo_Lib As Char
        Public Property Tipo_Lib() As Char
            Get
                Return _Tipo_Lib
            End Get
            Set(ByVal value As Char)
                _Tipo_Lib = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsCotizacionArchivosBE
        'Inherits ServicedComponent

        Private _IDArchivo As Int32
        Public Property IDArchivo() As Int32
            Get
                Return _IDArchivo
            End Get
            Set(ByVal value As Int32)
                _IDArchivo = value
            End Set
        End Property

        Private _IDCab As Int32
        Public Property IDCab() As Int32
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Int32)
                _IDCab = value
            End Set
        End Property

        Private _Nombre As String
        Public Property Nombre() As String
            Get
                Return _Nombre
            End Get
            Set(ByVal value As String)
                _Nombre = value
            End Set
        End Property

        Private _RutaOrigen As String
        Public Property RutaOrigen() As String
            Get
                Return _RutaOrigen
            End Get
            Set(ByVal value As String)
                _RutaOrigen = value
            End Set
        End Property

        Private _RutaDestino As String
        Public Property RutaDestino() As String
            Get
                Return _RutaDestino
            End Get
            Set(ByVal value As String)
                _RutaDestino = value
            End Set
        End Property

        Private _CoTipo As String
        Public Property CoTipo() As String
            Get
                Return _CoTipo
            End Get
            Set(ByVal value As String)
                _CoTipo = value
            End Set
        End Property

        Private _FlCopiado As Boolean
        Public Property FlCopiado() As Boolean
            Get
                Return _FlCopiado
            End Get
            Set(ByVal value As Boolean)
                _FlCopiado = value
            End Set
        End Property

        Private _CoUsuarioCreacion As String
        Public Property CoUsuarioCreacion() As String
            Get
                Return _CoUsuarioCreacion
            End Get
            Set(ByVal value As String)
                _CoUsuarioCreacion = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Public Class clsCotizacionVuelosBE
        Inherits clsCotizacionBE

        Private _ID As String
        Public Property ID() As String
            Get
                Return _ID
            End Get
            Set(ByVal value As String)
                _ID = value
            End Set
        End Property

        Private _IDDet As String
        Public Property IDDet() As String
            Get
                Return _IDDet
            End Get
            Set(ByVal value As String)
                _IDDet = value
            End Set
        End Property


        Private _NombrePax As String
        Public Property NombrePax() As String
            Get
                Return _NombrePax
            End Get
            Set(ByVal value As String)
                _NombrePax = value
            End Set
        End Property

        Private _Servicio As String
        Public Property Servicio() As String
            Get
                Return _Servicio
            End Get
            Set(ByVal value As String)
                _Servicio = value
            End Set
        End Property


        Private _Ruta As String
        Public Property Ruta() As String
            Get
                Return _Ruta
            End Get
            Set(ByVal value As String)
                _Ruta = value
            End Set
        End Property

        Private _RutaD As String
        Public Property RutaD() As String
            Get
                Return _RutaD
            End Get
            Set(ByVal value As String)
                _RutaD = value
            End Set
        End Property

        Private _RutaO As String
        Public Property RutaO() As String
            Get
                Return _RutaO
            End Get
            Set(ByVal value As String)
                _RutaO = value
            End Set
        End Property

        Private _Vuelo As String
        Public Property Vuelo() As String
            Get
                Return _Vuelo
            End Get
            Set(ByVal value As String)
                _Vuelo = value
            End Set
        End Property


        Private _FecEmision As Date
        Public Property FecEmision() As Date
            Get
                Return _FecEmision
            End Get
            Set(ByVal value As Date)
                _FecEmision = value
            End Set
        End Property


        Private _FecDocumento As Date
        Public Property FecDocumento() As Date
            Get
                Return _FecDocumento
            End Get
            Set(ByVal value As Date)
                _FecDocumento = value
            End Set
        End Property


        Private _FecSalida As Date
        Public Property FecSalida() As Date
            Get
                Return _FecSalida
            End Get
            Set(ByVal value As Date)
                _FecSalida = value
            End Set
        End Property


        Private _FecRetorno As Date
        Public Property FecRetorno() As Date
            Get
                Return _FecRetorno
            End Get
            Set(ByVal value As Date)
                _FecRetorno = value
            End Set
        End Property


        Private _Salida As String
        Public Property Salida() As String
            Get
                Return _Salida
            End Get
            Set(ByVal value As String)
                _Salida = value
            End Set
        End Property


        Private _HrRecojo As String
        Public Property HrRecojo() As String
            Get
                Return _HrRecojo
            End Get
            Set(ByVal value As String)
                _HrRecojo = value
            End Set
        End Property

        Private _NumBoleto As String
        Public Property NumBoleto() As String
            Get
                Return _NumBoleto
            End Get
            Set(ByVal value As String)
                _NumBoleto = value
            End Set
        End Property


        Private _PaxC As Int16
        Public Property PaxC() As Int16
            Get
                Return _PaxC
            End Get
            Set(ByVal value As Int16)
                _PaxC = value
            End Set
        End Property

        Private _PCotizado As Double
        Public Property PCotizado() As Double
            Get
                Return _PCotizado
            End Get
            Set(ByVal value As Double)
                _PCotizado = value
            End Set
        End Property

        Private _TCotizado As Double
        Public Property TCotizado() As Double
            Get
                Return _TCotizado
            End Get
            Set(ByVal value As Double)
                _TCotizado = value
            End Set
        End Property

        Private _PaxV As Int16
        Public Property PaxV() As Int16
            Get
                Return _PaxV
            End Get
            Set(ByVal value As Int16)
                _PaxV = value
            End Set
        End Property

        Private _PVolado As Double
        Public Property PVolado() As Double
            Get
                Return _PVolado
            End Get
            Set(ByVal value As Double)
                _PVolado = value
            End Set
        End Property

        Private _TVolado As Double
        Public Property TVolado() As Double
            Get
                Return _TVolado
            End Get
            Set(ByVal value As Double)
                _TVolado = value
            End Set
        End Property

        Private _Status As String
        Public Property Status() As String
            Get
                Return _Status
            End Get
            Set(ByVal value As String)
                _Status = value
            End Set
        End Property

        Private _Comentario As String
        Public Property Comentario() As String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As String)
                _Comentario = value
            End Set
        End Property


        Private _Llegada As String
        Public Property Llegada() As String
            Get
                Return _Llegada
            End Get
            Set(ByVal value As String)
                _Llegada = value
            End Set
        End Property

        Private _Emitido As String
        Public Property Emitido() As String
            Get
                Return _Emitido
            End Get
            Set(ByVal value As String)
                _Emitido = value
            End Set
        End Property

        Private _TipoTransporte As Char
        Public Property TipoTransporte() As Char
            Get
                Return _TipoTransporte
            End Get
            Set(ByVal value As Char)
                _TipoTransporte = value
            End Set
        End Property

        Private _CodReserva As String
        Public Property CodReserva() As String
            Get
                Return _CodReserva
            End Get
            Set(ByVal value As String)
                _CodReserva = value
            End Set
        End Property

        Private _Tarifa As Double
        Public Property Tarifa() As Double
            Get
                Return _Tarifa
            End Get
            Set(ByVal value As Double)
                _Tarifa = value
            End Set
        End Property

        Private _PTA As Double
        Public Property PTA() As Double
            Get
                Return _PTA
            End Get
            Set(ByVal value As Double)
                _PTA = value
            End Set
        End Property

        Private _ImpuestosExt As Double
        Public Property ImpuestosExt() As Double
            Get
                Return _ImpuestosExt
            End Get
            Set(ByVal value As Double)
                _ImpuestosExt = value
            End Set
        End Property

        Private _Penalidad As Double
        Public Property Penalidad() As Double
            Get
                Return _Penalidad
            End Get
            Set(ByVal value As Double)
                _Penalidad = value
            End Set
        End Property

        'Private _IGV As Single
        'Public Property Igv() As Single
        '    Get
        '        Return _IGV
        '    End Get
        '    Set(ByVal value As Single)
        '        _IGV = value
        '    End Set
        'End Property

        Private _Otros As Double
        Public Property Otros() As Double
            Get
                Return _Otros
            End Get
            Set(ByVal value As Double)
                _Otros = value
            End Set
        End Property

        Private _MontoContado As Double
        Public Property MontoContado() As Double
            Get
                Return _MontoContado
            End Get
            Set(ByVal value As Double)
                _MontoContado = value
            End Set
        End Property

        Private _MontoTarjeta As Double
        Public Property MontoTarjeta() As Double
            Get
                Return _MontoTarjeta
            End Get
            Set(ByVal value As Double)
                _MontoTarjeta = value
            End Set
        End Property


        Private _IDTarjCredito As String
        Public Property IDTarjetaCredito() As String
            Get
                Return _IDTarjCredito
            End Get
            Set(ByVal value As String)
                _IDTarjCredito = value
            End Set
        End Property


        Private _NumeroTarjeta As String
        Public Property NumeroTarjeta() As String
            Get
                Return _NumeroTarjeta
            End Get
            Set(ByVal value As String)
                _NumeroTarjeta = value
            End Set
        End Property


        Private _Aprobacion As String
        Public Property Aprobacion() As String
            Get
                Return _Aprobacion
            End Get
            Set(ByVal value As String)
                _Aprobacion = value
            End Set
        End Property


        Private _PorcOver As Double
        Public Property PorcOver() As Double
            Get
                Return _PorcOver
            End Get
            Set(ByVal value As Double)
                _PorcOver = value
            End Set
        End Property

        Private _PorcComm As Single
        Public Property PorcComm() As Single
            Get
                Return _PorcComm
            End Get
            Set(ByVal value As Single)
                _PorcComm = value
            End Set
        End Property



        Private _PorcOverInCom As Single
        Public Property PorcOverInCom() As Single
            Get
                Return _PorcOverInCom
            End Get
            Set(ByVal value As Single)
                _PorcOverInCom = value
            End Set
        End Property


        Private _MontComm As Double
        Public Property MontComm() As Double
            Get
                Return _MontComm
            End Get
            Set(ByVal value As Double)
                _MontComm = value
            End Set
        End Property


        Private _MontCommOver As Double
        Public Property MontCommOver() As Double
            Get
                Return _MontCommOver
            End Get
            Set(ByVal value As Double)
                _MontCommOver = value
            End Set
        End Property


        Private _IGVComm As Double
        Public Property IGVComm() As Double
            Get
                Return _IGVComm
            End Get
            Set(ByVal value As Double)
                _IGVComm = value
            End Set
        End Property


        Private _IGVCommOver As Double
        Public Property IGVCommOver() As Double
            Get
                Return _IGVCommOver
            End Get
            Set(ByVal value As Double)
                _IGVCommOver = value
            End Set
        End Property


        Private _NetoPagar As Double
        Public Property NetoPagar() As Double
            Get
                Return _NetoPagar
            End Get
            Set(ByVal value As Double)
                _NetoPagar = value
            End Set
        End Property


        Private _Descuento As Double
        Public Property Descuento() As Double
            Get
                Return _Descuento
            End Get
            Set(ByVal value As Double)
                _Descuento = value
            End Set
        End Property


        Private _IDLineaA As String
        Public Property IDLineaA() As String
            Get
                Return _IDLineaA
            End Get
            Set(ByVal value As String)
                _IDLineaA = value
            End Set
        End Property

        Private _CoLineaAerea As String
        Public Property CoLineaAerea() As String
            Get
                Return _CoLineaAerea
            End Get
            Set(ByVal value As String)
                _CoLineaAerea = value
            End Set
        End Property

        Private _MontoFEE As Double
        Public Property MontoFEE() As Double
            Get
                Return _MontoFEE
            End Get
            Set(ByVal value As Double)
                _MontoFEE = value
            End Set
        End Property


        Private _IGVFEE As Double
        Public Property IGVFEE() As Double
            Get
                Return _IGVFEE
            End Get
            Set(ByVal value As Double)
                _IGVFEE = value
            End Set
        End Property


        Private _TotalFEE As Double
        Public Property TotalFEE() As Double
            Get
                Return _TotalFEE
            End Get
            Set(ByVal value As Double)
                _TotalFEE = value
            End Set
        End Property

        Private _PorcTarifa As Double
        Public Property PorcTarifa() As Double
            Get
                Return _PorcTarifa
            End Get
            Set(ByVal value As Double)
                _PorcTarifa = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

        Private _EmitidoSetours As Boolean
        Public Property EmitidoSetours() As Boolean
            Get
                Return _EmitidoSetours
            End Get
            Set(ByVal value As Boolean)
                _EmitidoSetours = value
            End Set
        End Property

        Private _EmitidoOperador As Boolean
        Public Property EmitidoOperador() As Boolean
            Get
                Return _EmitidoOperador
            End Get
            Set(ByVal value As Boolean)
                _EmitidoOperador = value
            End Set
        End Property

        Private _IDProveedorOperador As String
        Public Property IDProveedorOperador() As String
            Get
                Return _IDProveedorOperador
            End Get
            Set(ByVal value As String)
                _IDProveedorOperador = value
            End Set
        End Property

        Private _CostoOperador As Double
        Public Property CostoOperador() As Double
            Get
                Return _CostoOperador
            End Get
            Set(ByVal value As Double)
                _CostoOperador = value
            End Set
        End Property

        Private _CoTicket As String
        Public Property CoTicket() As String
            Get
                Return _CoTicket
            End Get
            Set(ByVal value As String)
                _CoTicket = value
            End Set
        End Property

        Private _NuSegmento As Byte
        Public Property NuSegmento() As Byte
            Get
                Return _NuSegmento
            End Get
            Set(ByVal value As Byte)
                _NuSegmento = value
            End Set
        End Property

        Private _FlMotivo As Boolean
        Public Property FlMotivo() As Boolean
            Get
                Return _FlMotivo
            End Get
            Set(value As Boolean)
                _FlMotivo = value
            End Set
        End Property

        Private _DescMotivo As String
        Public Property DescMotivo() As String
            Get
                Return _DescMotivo
            End Get
            Set(value As String)
                _DescMotivo = value
            End Set
        End Property

    End Class

    Private _IdaVuelta As String
    Public Property IdaVuelta() As String
        Get
            Return _IdaVuelta
        End Get
        Set(ByVal value As String)
            _IdaVuelta = value
        End Set
    End Property

    Private _IDServicio_DetPeruRail As Integer
    Public Property IDServicio_DetPeruRail() As Integer
        Get
            Return _IDServicio_DetPeruRail
        End Get
        Set(ByVal value As Integer)
            _IDServicio_DetPeruRail = value
        End Set
    End Property


    Private _ListaCotizQuiebre As List(Of clsCotizacionQuiebreBE)
    Public Property ListaCotizQuiebre() As List(Of clsCotizacionQuiebreBE)
        Get
            Return _ListaCotizQuiebre
        End Get
        Set(ByVal value As List(Of clsCotizacionQuiebreBE))
            _ListaCotizQuiebre = value
        End Set
    End Property

    Private _ListaCotizArchivos As List(Of clsCotizacionArchivosBE)
    Public Property ListaCotizArchivos() As List(Of clsCotizacionArchivosBE)
        Get
            Return _ListaCotizArchivos
        End Get
        Set(ByVal value As List(Of clsCotizacionArchivosBE))
            _ListaCotizArchivos = value
        End Set
    End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCostosTourConductorBE
        Inherits clsCotizacionBE
        Private _NuCosto As Byte
        Public Property NuCosto() As Byte
            Get
                Return _NuCosto
            End Get
            Set(ByVal value As Byte)
                _NuCosto = value
            End Set
        End Property

        Private _CotipoHon As String
        Public Property CotipoHon() As String
            Get
                Return _CotipoHon
            End Get
            Set(ByVal value As String)
                _CotipoHon = value
            End Set
        End Property

        Private _QtDesde As Byte
        Public Property QtDesde() As Byte
            Get
                Return _QtDesde
            End Get
            Set(ByVal value As Byte)
                _QtDesde = value
            End Set
        End Property

        Private _QtHasta As Byte
        Public Property QtHasta() As Byte
            Get
                Return _QtHasta
            End Get
            Set(ByVal value As Byte)
                _QtHasta = value
            End Set
        End Property

        Private _SsPrecio As Double
        Public Property SsPrecio() As Double
            Get
                Return _SsPrecio
            End Get
            Set(ByVal value As Double)
                _SsPrecio = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTourConductorBE
        Inherits clsCotizacionBE

        Private _FeDesde As Date
        Public Property FeDesde() As Date
            Get
                Return _FeDesde
            End Get
            Set(ByVal value As Date)
                _FeDesde = value
            End Set
        End Property

        Private _FeHasta As Date
        Public Property FeHasta() As Date
            Get
                Return _FeHasta
            End Get
            Set(ByVal value As Date)
                _FeHasta = value
            End Set
        End Property

        Private _FlIncluiNochesAntDesp As Boolean
        Public Property FlIncluiNochesAntDesp() As Boolean
            Get
                Return _FlIncluiNochesAntDesp
            End Get
            Set(ByVal value As Boolean)
                _FlIncluiNochesAntDesp = value
            End Set
        End Property

        Private _SsMontoRealHotel As Double
        Public Property SsMontoRealHotel() As Double
            Get
                Return _SsMontoRealHotel
            End Get
            Set(ByVal value As Double)
                _SsMontoRealHotel = value
            End Set
        End Property

        Private _SsMontoTotalHotel As Double
        Public Property SsMontoTotalHotel() As Double
            Get
                Return _SsMontoTotalHotel
            End Get
            Set(ByVal value As Double)
                _SsMontoTotalHotel = value
            End Set
        End Property

        Private _FlIncluirVuelo As Boolean
        Public Property FlIncluirVuelo() As Boolean
            Get
                Return _FlIncluirVuelo
            End Get
            Set(ByVal value As Boolean)
                _FlIncluirVuelo = value
            End Set
        End Property

        Private _SsVuelo As Double
        Public Property SsVuelo() As Double
            Get
                Return _SsVuelo
            End Get
            Set(ByVal value As Double)
                _SsVuelo = value
            End Set
        End Property

        Private _CoTipoHon As String
        Public Property CoTipoHon() As String
            Get
                Return _CoTipoHon
            End Get
            Set(ByVal value As String)
                _CoTipoHon = value
            End Set
        End Property

        Private _SsHonorario As Double
        Public Property SsHonorario() As Double
            Get
                Return _SsHonorario
            End Get
            Set(ByVal value As Double)
                _SsHonorario = value
            End Set
        End Property

        Private _PoMargen As Double
        Public Property PoMargen() As Double
            Get
                Return _PoMargen
            End Get
            Set(ByVal value As Double)
                _PoMargen = value
            End Set
        End Property

        Private _SsViaticosSOL As Double
        Public Property SsViaticosSOL() As Double
            Get
                Return _SsViaticosSOL
            End Get
            Set(ByVal value As Double)
                _SsViaticosSOL = value
            End Set
        End Property

        Private _SsViaticosDOL As Double
        Public Property SsViaticosDOL() As Double
            Get
                Return _SsViaticosDOL
            End Get
            Set(ByVal value As Double)
                _SsViaticosDOL = value
            End Set
        End Property

        Private _SsTicketAereos As Double
        Public Property SsTicketAereos() As Double
            Get
                Return _SsTicketAereos
            End Get
            Set(ByVal value As Double)
                _SsTicketAereos = value
            End Set
        End Property

        Private _SsIGVHoteles As Double
        Public Property SsIGVHoteles() As Double
            Get
                Return _SsIGVHoteles
            End Get
            Set(ByVal value As Double)
                _SsIGVHoteles = value
            End Set
        End Property

        Private _SsTarjetaTelef As Double
        Public Property SsTarjetaTelef() As Double
            Get
                Return _SsTarjetaTelef
            End Get
            Set(ByVal value As Double)
                _SsTarjetaTelef = value
            End Set
        End Property

        Private _SsTaxi As Double
        Public Property SsTaxi() As Double
            Get
                Return _SsTaxi
            End Get
            Set(ByVal value As Double)
                _SsTaxi = value
            End Set
        End Property

        Private _SsOtros As Double
        Public Property SsOtros() As Double
            Get
                Return _SsOtros
            End Get
            Set(ByVal value As Double)
                _SsOtros = value
            End Set
        End Property

        Private _SsGEBPrimerDia As Double
        Public Property SsGEBPrimerDia() As Double
            Get
                Return _SsGEBPrimerDia
            End Get
            Set(ByVal value As Double)
                _SsGEBPrimerDia = value
            End Set
        End Property

        Private _SsGEBUltimoDia As Double
        Public Property SsGEBUltimoDia() As Double
            Get
                Return _SsGEBUltimoDia
            End Get
            Set(ByVal value As Double)
                _SsGEBUltimoDia = value
            End Set
        End Property

        Private _Total As Double
        Public Property Total() As Double
            Get
                Return _Total
            End Get
            Set(ByVal value As Double)
                _Total = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCopiaDetalle_ServicioBE
        Private _IDServicioAntiguo As String
        Public Property IDServicioAntiguo() As String
            Get
                Return _IDServicioAntiguo
            End Get
            Set(ByVal value As String)
                _IDServicioAntiguo = value
            End Set
        End Property

        Private _IDServicioNuevo As String
        Public Property IDServicioNuevo() As String
            Get
                Return _IDServicioNuevo
            End Get
            Set(ByVal value As String)
                _IDServicioNuevo = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCopiaDetalle_DetServicioBE
        Private _IDServicio As String
        Public Property IDServicio() As String
            Get
                Return _IDServicio
            End Get
            Set(ByVal value As String)
                _IDServicio = value
            End Set
        End Property

        Private _IDServicio_DetAntiguo As Integer
        Public Property IDServicio_DetAntiguo() As Integer
            Get
                Return _IDServicio_DetAntiguo
            End Get
            Set(ByVal value As Integer)
                _IDServicio_DetAntiguo = value
            End Set
        End Property

        Private _IDServicio_DetNuevo As Integer
        Public Property IDServicio_DetNuevo() As Integer
            Get
                Return _IDServicio_DetNuevo
            End Get
            Set(ByVal value As Integer)
                _IDServicio_DetNuevo = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCopiaDetalleBE
        Inherits clsDetalleCotizacionBE
        Private _ListServicios As List(Of clsCopiaDetalle_ServicioBE)
        Public Property ListServicios() As List(Of clsCopiaDetalle_ServicioBE)
            Get
                Return _ListServicios
            End Get
            Set(ByVal value As List(Of clsCopiaDetalle_ServicioBE))
                _ListServicios = value
            End Set
        End Property

        Private _ListDetServicios As List(Of clsCopiaDetalle_DetServicioBE)
        Public Property ListDetServicios() As List(Of clsCopiaDetalle_DetServicioBE)
            Get
                Return _ListDetServicios
            End Get
            Set(ByVal value As List(Of clsCopiaDetalle_DetServicioBE))
                _ListDetServicios = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDetalleCotizacionBE
        'Inherits ServicedComponent
        Private _IDDet As String
        Public Property IDDet() As String
            Get
                Return _IDDet
            End Get
            Set(ByVal value As String)
                _IDDet = value
            End Set
        End Property

        Private _IDDetMem As String
        Public Property IDDetMem() As String
            Get
                Return _IDDetMem
            End Get
            Set(ByVal value As String)
                _IDDetMem = value
            End Set
        End Property

        Private _IDCab As Int32
        Public Property IDCab() As Int32
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Int32)
                _IDCab = value
            End Set
        End Property

        Private _Cotizacion As String
        Public Property Cotizacion() As String
            Get
                Return _Cotizacion
            End Get
            Set(ByVal value As String)
                _Cotizacion = value
            End Set
        End Property

        Private _Item As Single
        Public Property Item() As Single
            Get
                Return _Item
            End Get
            Set(ByVal value As Single)
                _Item = value
            End Set
        End Property

        Private _Dia As DateTime
        Public Property Dia() As DateTime
            Get
                Return _Dia
            End Get
            Set(ByVal value As DateTime)
                _Dia = value
            End Set
        End Property

        Private _IDUbigeo As String
        Public Property IDUbigeo() As String
            Get
                Return _IDUbigeo
            End Get
            Set(ByVal value As String)
                _IDUbigeo = value
            End Set
        End Property

        Private _IDProveedor As String
        Public Property IDProveedor() As String
            Get
                Return _IDProveedor
            End Get
            Set(ByVal value As String)
                _IDProveedor = value
            End Set
        End Property

        Private _IDTipoProv As String
        Public Property IDTipoProv() As String
            Get
                Return _IDTipoProv
            End Get
            Set(ByVal value As String)
                _IDTipoProv = value
            End Set
        End Property

        Private _IDTipoOper As String
        Public Property IDTipoOper() As String
            Get
                Return _IDTipoOper
            End Get
            Set(ByVal value As String)
                _IDTipoOper = value
            End Set
        End Property

        Private _Dias As Byte
        Public Property Dias() As Byte
            Get
                Return _Dias
            End Get
            Set(ByVal value As Byte)
                _Dias = value
            End Set
        End Property

        Private _IDServicio As String
        Public Property IDServicio() As String
            Get
                Return _IDServicio
            End Get
            Set(ByVal value As String)
                _IDServicio = value
            End Set
        End Property

        Private _IDServicio_Det As Int32
        Public Property IDServicio_Det() As Int32
            Get
                Return _IDServicio_Det
            End Get
            Set(ByVal value As Int32)
                _IDServicio_Det = value
            End Set
        End Property

        Private _IDServicio_Det_V As Integer
        Public Property IDServicio_Det_V() As Integer
            Get
                Return _IDServicio_Det_V
            End Get
            Set(ByVal value As Integer)
                _IDServicio_Det_V = value
            End Set
        End Property

        Private _IDIdioma As String
        Public Property IDIdioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property

        Private _Especial As Boolean
        Public Property Especial() As Boolean
            Get
                Return _Especial
            End Get
            Set(ByVal value As Boolean)
                _Especial = value
            End Set
        End Property

        Private _MotivoEspecial As String
        Public Property MotivoEspecial() As String
            Get
                Return _MotivoEspecial
            End Get
            Set(ByVal value As String)
                _MotivoEspecial = value
            End Set
        End Property

        Private _RutaDocSustento As String
        Public Property RutaDocSustento() As String
            Get
                Return _RutaDocSustento
            End Get
            Set(ByVal value As String)
                _RutaDocSustento = value
            End Set
        End Property

        Private _Desayuno As Boolean
        Public Property Desayuno() As Boolean
            Get
                Return _Desayuno
            End Get
            Set(ByVal value As Boolean)
                _Desayuno = value
            End Set
        End Property
        Private _Lonche As Boolean
        Public Property Lonche() As Boolean
            Get
                Return _Lonche
            End Get
            Set(ByVal value As Boolean)
                _Lonche = value
            End Set
        End Property

        Private _Almuerzo As Boolean
        Public Property Almuerzo() As Boolean
            Get
                Return _Almuerzo
            End Get
            Set(ByVal value As Boolean)
                _Almuerzo = value
            End Set
        End Property


        Private _Cena As Boolean
        Public Property Cena() As Boolean
            Get
                Return _Cena
            End Get
            Set(ByVal value As Boolean)
                _Cena = value
            End Set
        End Property

        Private _Transfer As Boolean
        Public Property Transfer() As Boolean
            Get
                Return _Transfer
            End Get
            Set(ByVal value As Boolean)
                _Transfer = value
            End Set
        End Property 

        Private _IDDetTransferOri As String
        Public Property IDDetTransferOri() As String
            Get
                Return _IDDetTransferOri
            End Get
            Set(ByVal value As String)
                _IDDetTransferOri = value
            End Set
        End Property

        Private _IDDetTransferDes As String
        Public Property IDDetTransferDes() As String
            Get
                Return _IDDetTransferDes
            End Get
            Set(ByVal value As String)
                _IDDetTransferDes = value
            End Set
        End Property


        Private _TipoTransporte As Char
        Public Property TipoTransporte() As Char
            Get
                Return _TipoTransporte
            End Get
            Set(ByVal value As Char)
                _TipoTransporte = value
            End Set
        End Property

        Private _IDUbigeoOri As String
        Public Property IDUbigeoOri() As String
            Get
                Return _IDUbigeoOri
            End Get
            Set(ByVal value As String)
                _IDUbigeoOri = value
            End Set
        End Property

        Private _IDUbigeoDes As String
        Public Property IDUbigeoDes() As String
            Get
                Return _IDUbigeoDes
            End Get
            Set(ByVal value As String)
                _IDUbigeoDes = value
            End Set
        End Property
        Private _NroPax As Int16
        Public Property NroPax() As Int16
            Get
                Return _NroPax
            End Get
            Set(ByVal value As Int16)
                _NroPax = value
            End Set
        End Property

        Private _NroLiberados As Int16
        Public Property NroLiberados() As Int16
            Get
                Return _NroLiberados
            End Get
            Set(ByVal value As Int16)
                _NroLiberados = value
            End Set
        End Property

        Private _Tipo_Lib As Char
        Public Property Tipo_Lib() As Char
            Get
                Return _Tipo_Lib
            End Get
            Set(ByVal value As Char)
                _Tipo_Lib = value
            End Set
        End Property

        Private _CostoReal As Double
        Public Property CostoReal() As Double
            Get
                If _CostoReal.ToString = "NeuN" Or _CostoReal.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _CostoReal
                Return Math.Round(_CostoReal, 4)
            End Get
            Set(ByVal value As Double)
                _CostoReal = value
            End Set
        End Property

        Private _CostoRealEditado As Boolean
        Public Property CostoRealEditado() As Boolean
            Get
                Return _CostoRealEditado
            End Get
            Set(ByVal value As Boolean)
                _CostoRealEditado = value
            End Set
        End Property


        Private _CostoRealAnt As Double
        Public Property CostoRealAnt() As Double
            Get
                If _CostoRealAnt.ToString = "NeuN" Or _CostoRealAnt.ToString = "Infinito" Then
                    Return 0
                End If
                Return _CostoRealAnt
            End Get
            Set(ByVal value As Double)
                _CostoRealAnt = value
            End Set
        End Property

        Private _CostoLiberado As Double
        Public Property CostoLiberado() As Double
            Get
                If _CostoLiberado.ToString = "NeuN" Or _CostoLiberado.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _CostoLiberado
                Return Math.Round(_CostoLiberado, 4)
            End Get
            Set(ByVal value As Double)
                _CostoLiberado = value
            End Set
        End Property

        Private _Margen As Double
        Public Property Margen() As Double
            Get
                If _Margen.ToString = "NeuN" Or _Margen.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_Margen, 4)
            End Get
            Set(ByVal value As Double)
                _Margen = value
            End Set
        End Property

        Private _MargenAplicado As Double
        Public Property MargenAplicado() As Double
            Get
                If _MargenAplicado.ToString = "NeuN" Or _MargenAplicado.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _MargenAplicado
                Return Math.Round(_MargenAplicado, 2)
            End Get
            Set(ByVal value As Double)
                _MargenAplicado = value
            End Set
        End Property


        Private _MargenAplicadoEditado As Boolean
        Public Property MargenAplicadoEditado() As Boolean
            Get
                Return _MargenAplicadoEditado
            End Get
            Set(ByVal value As Boolean)
                _MargenAplicadoEditado = value
            End Set
        End Property

        Private _MargenAplicadoAnt As Double
        Public Property MargenAplicadoAnt() As Double
            Get
                If _MargenAplicadoAnt.ToString = "NeuN" Or _MargenAplicadoAnt.ToString = "Infinito" Then
                    Return 0
                End If
                Return _MargenAplicadoAnt
            End Get
            Set(ByVal value As Double)
                _MargenAplicadoAnt = value
            End Set
        End Property


        Private _FlSSCREditado As Boolean
        Public Property FlSSCREditado() As Boolean
            Get
                Return _FlSSCREditado
            End Get
            Set(ByVal value As Boolean)
                _FlSSCREditado = value
            End Set
        End Property

        Private _FlSTCREditado As Boolean
        Public Property FlSTCREditado() As Boolean
            Get
                Return _FlSTCREditado
            End Get
            Set(ByVal value As Boolean)
                _FlSTCREditado = value
            End Set
        End Property


        Private _MargenLiberado As Double
        Public Property MargenLiberado() As Double
            Get
                If _MargenLiberado.ToString = "NeuN" Or _MargenLiberado.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _MargenLiberado
                Return Math.Round(_MargenLiberado, 2)
            End Get
            Set(ByVal value As Double)
                _MargenLiberado = value
            End Set
        End Property

        Private _Total As Double
        Public Property Total() As Double
            Get
                If _Total.ToString = "NeuN" Or _Total.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_Total, 2)
            End Get
            Set(ByVal value As Double)
                _Total = value
            End Set
        End Property

        Private _RedondeoTotal As Single
        Public Property RedondeoTotal() As Single
            Get
                If _RedondeoTotal.ToString = "NeuN" Or _RedondeoTotal.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_RedondeoTotal, 4)
            End Get
            Set(ByVal value As Single)
                _RedondeoTotal = value
            End Set
        End Property


        Private _TotalOrig As Double
        Public Property TotalOrig() As Double
            Get
                If _TotalOrig.ToString = "NeuN" Or _TotalOrig.ToString = "Infinito" Then
                    Return 0
                End If
                Return _TotalOrig
            End Get
            Set(ByVal value As Double)
                _TotalOrig = value
            End Set
        End Property

        Private _SSCR As Double
        Public Property SSCR() As Double
            Get
                If _SSCR.ToString = "NeuN" Or _SSCR.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _SSCR
                Return Math.Round(_SSCR, 4)
            End Get
            Set(ByVal value As Double)
                _SSCR = value
            End Set
        End Property

        Private _SSCL As Double
        Public Property SSCL() As Double
            Get
                If _SSCL.ToString = "NeuN" Or _SSCL.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _SSCL
                Return Math.Round(_SSCL, 4)
            End Get
            Set(ByVal value As Double)
                _SSCL = value
            End Set
        End Property

        Private _SSMargen As Double
        Public Property SSMargen() As Double
            Get
                If _SSMargen.ToString = "NeuN" Or _SSMargen.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_SSMargen, 4)
            End Get
            Set(ByVal value As Double)
                _SSMargen = value
            End Set
        End Property

        Private _SSMargenLiberado As Double
        Public Property SSMargenLiberado() As Double
            Get
                If _SSMargenLiberado.ToString = "NeuN" Or _SSMargenLiberado.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _SSMargenLiberado
                Return Math.Round(_SSMargenLiberado, 4)
            End Get
            Set(ByVal value As Double)
                _SSMargenLiberado = value
            End Set
        End Property

        Private _SSTotal As Double
        Public Property SSTotal() As Double
            Get
                If _SSTotal.ToString = "NeuN" Or _SSTotal.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_SSTotal, 4)
            End Get
            Set(ByVal value As Double)
                _SSTotal = value
            End Set
        End Property

        Private _SSTotalOrig As Double
        Public Property SSTotalOrig() As Double
            Get
                If _SSTotalOrig.ToString = "NeuN" Or _SSTotalOrig.ToString = "Infinito" Then
                    Return 0
                End If
                Return _SSTotalOrig
            End Get
            Set(ByVal value As Double)
                _SSTotalOrig = value
            End Set
        End Property

        Private _STCR As Double
        Public Property STCR() As Double
            Get
                If _STCR.ToString = "NeuN" Or _STCR.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_STCR, 4)
            End Get
            Set(ByVal value As Double)
                _STCR = value
            End Set
        End Property

        Private _STMargen As Double
        Public Property STMargen() As Double
            Get
                If _STMargen.ToString = "NeuN" Or _STMargen.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_STMargen, 4)
            End Get
            Set(ByVal value As Double)
                _STMargen = value
            End Set
        End Property

        Private _STTotal As Double
        Public Property STTotal() As Double
            Get
                If _STTotal.ToString = "NeuN" Or _STTotal.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_STTotal, 4)
            End Get
            Set(ByVal value As Double)
                _STTotal = value
            End Set
        End Property

        Private _STTotalOrig As Double
        Public Property STTotalOrig() As Double
            Get
                If _STTotalOrig.ToString = "NeuN" Or _STTotalOrig.ToString = "Infinito" Then
                    Return 0
                End If
                Return _STTotalOrig
            End Get
            Set(ByVal value As Double)
                _STTotalOrig = value
            End Set
        End Property

        Private _CostoRealImpto As Double
        Public Property CostoRealImpto() As Double
            Get
                If _CostoRealImpto.ToString = "NeuN" Or _CostoRealImpto.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _CostoRealImpto
                Return Math.Round(_CostoRealImpto, 4)
            End Get
            Set(ByVal value As Double)
                _CostoRealImpto = value
            End Set
        End Property

        Private _CostoRealImptoEditado As Boolean
        Public Property CostoRealImptoEditado() As Boolean
            Get
                Return _CostoRealImptoEditado
            End Get
            Set(ByVal value As Boolean)
                _CostoRealImptoEditado = value
            End Set
        End Property

        Private _CostoLiberadoImpto As Double
        Public Property CostoLiberadoImpto() As Double
            Get
                If _CostoLiberadoImpto.ToString = "NeuN" Or _CostoLiberadoImpto.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _CostoLiberadoImpto
                Return Math.Round(_CostoLiberadoImpto, 4)
            End Get
            Set(ByVal value As Double)
                _CostoLiberadoImpto = value
            End Set
        End Property

        Private _MargenImpto As Double
        Public Property MargenImpto() As Double
            Get
                If _MargenImpto.ToString = "NeuN" Or _MargenImpto.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _MargenImpto
                Return Math.Round(_MargenImpto, 4)
            End Get
            Set(ByVal value As Double)
                _MargenImpto = value
            End Set
        End Property

        Private _MargenLiberadoImpto As Double
        Public Property MargenLiberadoImpto() As Double
            Get
                If _MargenLiberadoImpto.ToString = "NeuN" Or _MargenLiberadoImpto.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_MargenLiberadoImpto, 4)
            End Get
            Set(ByVal value As Double)
                _MargenLiberadoImpto = value
            End Set
        End Property

        Private _SSCRImpto As Double
        Public Property SSCRImpto() As Double
            Get
                If _SSCRImpto.ToString = "NeuN" Or _SSCRImpto.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _SSCRImpto
                Return Math.Round(_SSCRImpto, 4)
            End Get
            Set(ByVal value As Double)
                _SSCRImpto = value
            End Set
        End Property

        Private _SSCLImpto As Double
        Public Property SSCLImpto() As Double
            Get
                If _SSCLImpto.ToString = "NeuN" Or _SSCLImpto.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _SSCLImpto
                Return Math.Round(_SSCLImpto, 4)
            End Get
            Set(ByVal value As Double)
                _SSCLImpto = value
            End Set
        End Property

        Private _SSMargenImpto As Double
        Public Property SSMargenImpto() As Double
            Get
                If _SSMargenImpto.ToString = "NeuN" Or _SSMargenImpto.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_SSMargenImpto, 4)
            End Get
            Set(ByVal value As Double)
                _SSMargenImpto = value
            End Set
        End Property

        Private _SSMargenLiberadoImpto As Double
        Public Property SSMargenLiberadoImpto() As Double
            Get
                If _SSMargenLiberadoImpto.ToString = "NeuN" Or _SSMargenLiberadoImpto.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_SSMargenLiberadoImpto, 4)
            End Get
            Set(ByVal value As Double)
                _SSMargenLiberadoImpto = value
            End Set
        End Property

        Private _STCRImpto As Double
        Public Property STCRImpto() As Double
            Get
                If _STCRImpto.ToString = "NeuN" Or _STCRImpto.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _STCRImpto
                Return Math.Round(_STCRImpto, 4)
            End Get
            Set(ByVal value As Double)
                _STCRImpto = value
            End Set
        End Property

        Private _STMargenImpto As Double
        Public Property STMargenImpto() As Double
            Get
                If _STMargenImpto.ToString = "NeuN" Or _STMargenImpto.ToString = "Infinito" Then
                    Return 0
                End If
                Return Math.Round(_STMargenImpto, 4)
            End Get
            Set(ByVal value As Double)
                _STMargenImpto = value
            End Set
        End Property

        Private _TotImpto As Double
        Public Property TotImpto() As Double
            Get
                If _TotImpto.ToString = "NeuN" Or _TotImpto.ToString = "Infinito" Then
                    Return 0
                End If
                'Return _TotImpto
                Return Math.Round(_TotImpto, 4)
            End Get
            Set(ByVal value As Double)
                _TotImpto = value
            End Set
        End Property

        Private _IDPais As String
        Public Property IDPais() As String
            Get
                Return _IDPais
            End Get
            Set(ByVal value As String)
                _IDPais = value
            End Set
        End Property

        Private _IDDetRel As String
        Public Property IDDetRel() As String
            Get
                If IsNothing(_IDDetRel) Then
                    Return "0"
                End If
                Return _IDDetRel
            End Get
            Set(ByVal value As String)
                If IsNothing(value) Then
                    _IDDetRel = "0"
                Else
                    _IDDetRel = value
                End If
            End Set
        End Property

        Private _Servicio As String
        Public Property Servicio() As String
            Get
                Return _Servicio
            End Get
            Set(ByVal value As String)
                _Servicio = value
            End Set
        End Property

        Private _ServicioEditado As Boolean
        Public Property ServicioEditado() As Boolean
            Get
                Return _ServicioEditado
            End Get
            Set(ByVal value As Boolean)
                _ServicioEditado = value
            End Set
        End Property

        Private _FlServicioParaGuia As Boolean
        Public Property FlServicioParaGuia() As Boolean
            Get
                Return _FlServicioParaGuia
            End Get
            Set(ByVal value As Boolean)
                _FlServicioParaGuia = value
            End Set
        End Property

        Private _FlServicioNoShow As Boolean
        Public Property FlServicioNoShow() As Boolean
            Get
                Return _FlServicioNoShow
            End Get
            Set(ByVal value As Boolean)
                _FlServicioNoShow = value
            End Set
        End Property

        Private _FlServicioNoShowWhere As Boolean
        Public Property FlServicioNoShowWhere() As Boolean
            Get
                Return _FlServicioNoShowWhere
            End Get
            Set(ByVal value As Boolean)
                _FlServicioNoShowWhere = value
            End Set
        End Property

        Private _FlServicioIngManual As Boolean
        Public Property FlServicioIngManual() As Boolean
            Get
                Return _FlServicioIngManual
            End Get
            Set(ByVal value As Boolean)
                _FlServicioIngManual = value
            End Set
        End Property

        Private _FlServInManualOper As Boolean
        Public Property FlServInManualOper() As Boolean
            Get
                Return _FlServInManualOper
            End Get
            Set(ByVal value As Boolean)
                _FlServInManualOper = value
            End Set
        End Property

        Private _IDDetCopia As String
        Public Property IDDetCopia() As String
            Get
                Return _IDDetCopia
            End Get
            Set(ByVal value As String)
                _IDDetCopia = value
            End Set
        End Property

        Private _Recalcular As Boolean
        Public Property Recalcular() As Boolean
            Get
                Return _Recalcular
            End Get
            Set(ByVal value As Boolean)
                _Recalcular = value
            End Set
        End Property


        Private _CalculoTarifaPendiente As Boolean
        Public Property CalculoTarifaPendiente() As Boolean
            Get
                Return _CalculoTarifaPendiente
            End Get
            Set(ByVal value As Boolean)
                _CalculoTarifaPendiente = value
            End Set
        End Property


        Private _PorReserva As Boolean
        Public Property PorReserva() As Boolean
            Get
                Return _PorReserva
            End Get
            Set(ByVal value As Boolean)
                _PorReserva = value
            End Set
        End Property


        Private _ExecTrigger As Boolean
        Public Property ExecTrigger() As Boolean
            Get
                Return _ExecTrigger
            End Get
            Set(ByVal value As Boolean)
                _ExecTrigger = value
            End Set
        End Property


        Private _IDReserva As String
        Public Property IDReserva() As String
            Get
                Return _IDReserva
            End Get
            Set(ByVal value As String)
                _IDReserva = value
            End Set
        End Property

        Private _Reservado As Boolean
        Public Property Reservado() As Boolean
            Get
                Return _Reservado
            End Get
            Set(ByVal value As Boolean)
                _Reservado = value
            End Set
        End Property

        Private _IncGuia As Boolean
        Public Property IncGuia() As Boolean
            Get
                Return _IncGuia
            End Get
            Set(ByVal value As Boolean)
                _IncGuia = value
            End Set
        End Property

        Private _FueradeHorario As Boolean
        Public Property FueradeHorario() As Boolean
            Get
                Return _FueradeHorario
            End Get
            Set(ByVal value As Boolean)
                _FueradeHorario = value
            End Set
        End Property

        Private _IDReserva_Det As String
        Public Property IDReserva_Det() As String
            Get
                Return _IDReserva_Det
            End Get
            Set(ByVal value As String)
                _IDReserva_Det = value
            End Set
        End Property

        Private _NuViaticosTC As Integer
        Public Property NuViaticosTC() As Integer
            Get
                Return _NuViaticosTC
            End Get
            Set(ByVal value As Integer)
                _NuViaticosTC = value
            End Set
        End Property

        Private _IDReserva_DetAntiguo As Integer
        Public Property IDReserva_DetAntiguo() As Integer
            Get
                Return _IDReserva_DetAntiguo
            End Get
            Set(ByVal value As Integer)
                _IDReserva_DetAntiguo = value
            End Set
        End Property

        Private _ConAlojamiento As Boolean
        Public Property ConAlojamiento() As Boolean
            Get
                Return _ConAlojamiento
            End Get
            Set(ByVal value As Boolean)
                _ConAlojamiento = value
            End Set
        End Property

        Private _PlanAlimenticio As Boolean
        Public Property PlanAlimenticio() As Boolean
            Get
                Return _PlanAlimenticio
            End Get
            Set(ByVal value As Boolean)
                _PlanAlimenticio = value
            End Set
        End Property

        Private _ActualizQuiebre As Boolean
        Public Property ActualizQuiebre() As Boolean
            Get
                Return _ActualizQuiebre
            End Get
            Set(ByVal value As Boolean)
                _ActualizQuiebre = value
            End Set
        End Property

        Private _DetaTipo As String
        Public Property DetaTipo() As String
            Get
                Return _DetaTipo
            End Get
            Set(ByVal value As String)
                _DetaTipo = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _DifPax As Boolean
        Public Property DifPax() As Boolean
            Get
                Return _DifPax
            End Get
            Set(ByVal value As Boolean)
                _DifPax = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

        Private _AcomodoEspecial As Boolean
        Public Property AcomodoEspecial() As Boolean
            Get
                Return _AcomodoEspecial
            End Get
            Set(ByVal value As Boolean)
                _AcomodoEspecial = value
            End Set
        End Property

        Private _Capacidad As Byte = 0
        Public Property Capacidad() As Byte
            Get
                Return _Capacidad
            End Get
            Set(ByVal value As Byte)
                _Capacidad = value
            End Set
        End Property

        Private _EsMatrimonial As Boolean
        Public Property EsMatrimonial() As Boolean
            Get
                Return _EsMatrimonial
            End Get
            Set(ByVal value As Boolean)
                _EsMatrimonial = value
            End Set
        End Property

        Private _CantidadAcomodoEsp As Byte
        Public Property CantidadAcomodoEsp() As Byte
            Get
                Return _CantidadAcomodoEsp
            End Get
            Set(ByVal value As Byte)
                _CantidadAcomodoEsp = value
            End Set
        End Property

        Private _FechaOut_OperAlojamiento As DateTime
        Public Property FechaOut_OperAlojamiento() As DateTime
            Get
                Return _FechaOut_OperAlojamiento
            End Get
            Set(ByVal value As DateTime)
                _FechaOut_OperAlojamiento = value
            End Set
        End Property

        Private _ItemAlojamiento As Byte
        Public Property ItemAlojamiento() As Byte
            Get
                Return _ItemAlojamiento
            End Get
            Set(ByVal value As Byte)
                _ItemAlojamiento = value
            End Set
        End Property

        Private _DescAcomodoEspecial As String
        Public Property DescAcomodoEspecial() As String
            Get
                Return _DescAcomodoEspecial
            End Get
            Set(ByVal value As String)
                _DescAcomodoEspecial = value
            End Set
        End Property

        Private _FlServicioTC As Boolean
        Public Property FlServicioTC() As Boolean
            Get
                Return _FlServicioTC
            End Get
            Set(ByVal value As Boolean)
                _FlServicioTC = value
            End Set
        End Property

        Private _IDDetAntiguo As Integer
        Public Property IDDetAntiguo() As Integer
            Get
                Return _IDDetAntiguo
            End Get
            Set(ByVal value As Integer)
                _IDDetAntiguo = value
            End Set
        End Property

        Private _dicValoresTC As Dictionary(Of String, Double)
        Public Property dicValoresTC() As Dictionary(Of String, Double)
            Get
                Return _dicValoresTC
            End Get
            Set(ByVal value As Dictionary(Of String, Double))
                _dicValoresTC = value
            End Set
        End Property

        Private _FlServicioUpdxAnio As Boolean
        Public Property FlServicioUpdxAnio() As Boolean
            Get
                Return _FlServicioUpdxAnio
            End Get
            Set(ByVal value As Boolean)
                _FlServicioUpdxAnio = value
            End Set
        End Property

        Private _SoloAlojamientoGen As Boolean
        Public Property SoloAlojamientoGen() As Boolean
            Get
                Return _SoloAlojamientoGen
            End Get
            Set(ByVal value As Boolean)
                _SoloAlojamientoGen = value
            End Set
        End Property


        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleCotizacionQuiebresBE
            Inherits clsDetalleCotizacionBE

            Private _IDDet_Q As String
            Public Property IDDet_Q() As String
                Get
                    Return _IDDet_Q
                End Get
                Set(ByVal value As String)
                    _IDDet_Q = value
                End Set
            End Property

            Private _IDCab_Q As Int32
            Public Property IDCab_Q() As Int32
                Get
                    Return _IDCab_Q
                End Get
                Set(ByVal value As Int32)
                    _IDCab_Q = value
                End Set
            End Property


        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleCotizacionDetServiciosBE
            Inherits clsDetalleCotizacionBE

        End Class

        Public Class clsDetalleCotizacionDescripServicioBE
            'Inherits ServicedComponent
            Private _IDDet As String
            Public Property IDDet() As String
                Get
                    Return _IDDet
                End Get
                Set(ByVal value As String)
                    _IDDet = value
                End Set
            End Property

            Private _IDIdioma As String
            Public Property IDIdioma() As String
                Get
                    Return _IDIdioma
                End Get
                Set(ByVal value As String)
                    _IDIdioma = value
                End Set
            End Property

            'Private _DescCorta As String
            'Public Property DescCorta() As String
            '    Get
            '        Return _DescCorta
            '    End Get
            '    Set(ByVal value As String)
            '        _DescCorta = value
            '    End Set
            'End Property

            Private _DescLarga As String
            Public Property DescLarga() As String
                Get
                    Return _DescLarga
                End Get
                Set(ByVal value As String)
                    _DescLarga = value
                End Set
            End Property

            Private _NoHotel As String
            Public Property NoHotel() As String
                Get
                    Return _NoHotel
                End Get
                Set(ByVal value As String)
                    _NoHotel = value
                End Set
            End Property

            Private _TxWebHotel As String
            Public Property TxWebHotel() As String
                Get
                    Return _TxWebHotel
                End Get
                Set(ByVal value As String)
                    _TxWebHotel = value
                End Set
            End Property

            Private _TxDireccHotel As String
            Public Property TxDireccHotel() As String
                Get
                    Return _TxDireccHotel
                End Get
                Set(ByVal value As String)
                    _TxDireccHotel = value
                End Set
            End Property

            Private _TxTelfHotel1 As String
            Public Property TxTelfHotel1() As String
                Get
                    Return _TxTelfHotel1
                End Get
                Set(ByVal value As String)
                    _TxTelfHotel1 = value
                End Set
            End Property

            Private _TxTelfHotel2 As String
            Public Property TxTelfHotel2() As String
                Get
                    Return _TxTelfHotel2
                End Get
                Set(ByVal value As String)
                    _TxTelfHotel2 = value
                End Set
            End Property

            Private _FeHoraChkInHotel As DateTime
            Public Property FeHoraChkInHotel() As DateTime
                Get
                    Return _FeHoraChkInHotel
                End Get
                Set(ByVal value As DateTime)
                    _FeHoraChkInHotel = value
                End Set
            End Property

            Private _FeHoraChkOutHotel As DateTime
            Public Property FeHoraChkOutHotel() As DateTime
                Get
                    Return _FeHoraChkOutHotel
                End Get
                Set(ByVal value As DateTime)
                    _FeHoraChkOutHotel = value
                End Set
            End Property

            Private _CoTipoDesaHotel As String
            Public Property CoTipoDesaHotel() As String
                Get
                    Return _CoTipoDesaHotel
                End Get
                Set(ByVal value As String)
                    _CoTipoDesaHotel = value
                End Set
            End Property

            Private _CoCiudadHotel As String
            Public Property CoCiudadHotel() As String
                Get
                    Return _CoCiudadHotel
                End Get
                Set(ByVal value As String)
                    _CoCiudadHotel = value
                End Set
            End Property

            Private _UserMod As String
            Public Property UserMod() As String
                Get
                    Return _UserMod
                End Get
                Set(ByVal value As String)
                    _UserMod = value
                End Set
            End Property
            Private _Accion As Char
            Public Property Accion() As Char
                Get
                    Return _Accion
                End Get
                Set(ByVal value As Char)
                    _Accion = value
                End Set
            End Property


        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleCotizacionAcomodoEspecialBE

            Private _IDDet As String
            Public Property IDDet() As String
                Get
                    Return _IDDet
                End Get
                Set(ByVal value As String)
                    _IDDet = value
                End Set
            End Property

            Private _CoCapacidad As String
            Public Property CoCapacidad() As String
                Get
                    Return _CoCapacidad
                End Get
                Set(ByVal value As String)
                    _CoCapacidad = value
                End Set
            End Property

            Private _QtPax As Int16
            Public Property QtPax() As Int16
                Get
                    Return _QtPax
                End Get
                Set(ByVal value As Int16)
                    _QtPax = value
                End Set
            End Property

            Private _UserMod As String
            Public Property UserMod() As String
                Get
                    Return _UserMod
                End Get
                Set(ByVal value As String)
                    _UserMod = value
                End Set
            End Property

            Private _Accion As Char
            Public Property Accion() As Char
                Get
                    Return _Accion
                End Get
                Set(ByVal value As Char)
                    _Accion = value
                End Set
            End Property

        End Class

        Public Class clsDistribucAcomodoEspecialBE
            Inherits clsDetalleCotizacionAcomodoEspecialBE

            Private _IDPax As Integer
            Public Property IDPax() As Integer
                Get
                    Return _IDPax
                End Get
                Set(ByVal value As Integer)
                    _IDPax = value
                End Set
            End Property

        End Class
    End Class


    Private _ListaDetalleCotizacionDetServicio As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE)
    Public Property ListaDetalleCotizacionDetServicio() As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE)
        Get
            Return _ListaDetalleCotizacionDetServicio
        End Get
        Set(ByVal value As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionDetServiciosBE))
            _ListaDetalleCotizacionDetServicio = value
        End Set
    End Property


    Private _ListaDetalleCotiz As List(Of clsDetalleCotizacionBE)
    Public Property ListaDetalleCotiz() As List(Of clsDetalleCotizacionBE)
        Get
            Return _ListaDetalleCotiz
        End Get
        Set(ByVal value As List(Of clsDetalleCotizacionBE))
            _ListaDetalleCotiz = value
        End Set
    End Property

    Private _ListaDetalleCotizacionQuiebres As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
    Public Property ListaDetalleCotizacionQuiebres() As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE)
        Get
            Return _ListaDetalleCotizacionQuiebres
        End Get
        Set(ByVal value As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionQuiebresBE))
            _ListaDetalleCotizacionQuiebres = value
        End Set
    End Property


    Private _ListaDetalleCotizacionDescripServicio As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE)
    Public Property ListaDetalleCotizacionDescripServicio() As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE)
        Get
            Return _ListaDetalleCotizacionDescripServicio
        End Get
        Set(ByVal value As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionDescripServicioBE))
            _ListaDetalleCotizacionDescripServicio = value
        End Set
    End Property


    Public Class clsCotizacionPaxBE
        'Inherits ServicedComponent
        Private _IDPax As String
        Public Property IDPax() As String
            Get
                Return _IDPax
            End Get
            Set(ByVal value As String)
                _IDPax = value
            End Set
        End Property

        Private _IDCab As Int32
        Public Property IDCab() As Int32
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Int32)
                _IDCab = value
            End Set
        End Property

        Private _IDIdentidad As String
        Public Property IDIdentidad() As String
            Get
                Return _IDIdentidad
            End Get
            Set(ByVal value As String)
                _IDIdentidad = value
            End Set
        End Property

        Private _NumIdentidad As String
        Public Property NumIdentidad() As String
            Get
                Return _NumIdentidad
            End Get
            Set(ByVal value As String)
                _NumIdentidad = value
            End Set
        End Property

        Private _Nombres As String
        Public Property Nombres() As String
            Get
                Return _Nombres
            End Get
            Set(ByVal value As String)
                _Nombres = value
            End Set
        End Property

        Private _Apellidos As String
        Public Property Apellidos() As String
            Get
                Return _Apellidos
            End Get
            Set(ByVal value As String)
                _Apellidos = value
            End Set
        End Property

        Private _Titulo As String
        Public Property Titulo() As String
            Get
                Return _Titulo
            End Get
            Set(ByVal value As String)
                _Titulo = value
            End Set
        End Property
        Private _TituloAcademico As String
        Public Property TituloAcademico() As String
            Get
                Return _TituloAcademico
            End Get
            Set(ByVal value As String)
                _TituloAcademico = value
            End Set
        End Property
        Private _TC As Boolean
        Public Property TC() As Boolean
            Get
                Return _TC
            End Get
            Set(ByVal value As Boolean)
                _TC = value
            End Set
        End Property

        Private _FecNacimiento As Date
        Public Property FecNacimiento() As Date
            Get
                Return _FecNacimiento
            End Get
            Set(ByVal value As Date)
                _FecNacimiento = value
            End Set
        End Property

        Private _IDNacionalidad As String
        Public Property IDNacionalidad() As String
            Get
                Return _IDNacionalidad
            End Get
            Set(ByVal value As String)
                _IDNacionalidad = value
            End Set
        End Property

        Private _Peso As Decimal
        Public Property Peso() As Decimal
            Get
                Return _Peso
            End Get
            Set(ByVal value As Decimal)
                _Peso = value
            End Set
        End Property

        Private _ObservEspecial As String
        Public Property ObservEspecial() As String
            Get
                Return _ObservEspecial
            End Get
            Set(ByVal value As String)
                _ObservEspecial = value
            End Set
        End Property

        Private _Orden As Int16
        Public Property Orden() As Int16
            Get
                Return _Orden
            End Get
            Set(ByVal value As Int16)
                _Orden = value
            End Set
        End Property

        Private _Residente As Boolean
        Public Property Residente() As Boolean
            Get
                Return _Residente
            End Get
            Set(ByVal value As Boolean)
                _Residente = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

        Private _IDPaisResidencia As String
        Public Property IDPaisResidencia() As String
            Get
                Return _IDPaisResidencia
            End Get
            Set(ByVal value As String)
                _IDPaisResidencia = value
            End Set
        End Property

        Private _FecIngresoPais As Date
        Public Property FecIngresoPais() As Date
            Get
                Return _FecIngresoPais
            End Get
            Set(ByVal value As Date)
                _FecIngresoPais = value
            End Set
        End Property

        Private _PassValidado As Boolean
        Public Property PassValidado() As Boolean
            Get
                Return _PassValidado
            End Get
            Set(ByVal value As Boolean)
                _PassValidado = value
            End Set
        End Property


        Private _NoShow As Boolean
        Public Property NoShow() As Boolean
            Get
                Return _NoShow
            End Get
            Set(ByVal value As Boolean)
                _NoShow = value
            End Set
        End Property
        Private _FlEntWP As Boolean
        Public Property FlEntWP() As Boolean
            Get
                Return _FlEntWP
            End Get
            Set(ByVal value As Boolean)
                _FlEntWP = value
            End Set
        End Property
        Private _FlEntMP As Boolean
        Public Property FlEntMP() As Boolean
            Get
                Return _FlEntMP
            End Get
            Set(ByVal value As Boolean)
                _FlEntMP = value
            End Set
        End Property
        Private _FlAutorizMPWP As Boolean
        Public Property FlAutorizMPWP() As Boolean
            Get
                Return _FlAutorizMPWP
            End Get
            Set(ByVal value As Boolean)
                _FlAutorizMPWP = value
            End Set
        End Property

        Private _FlAdultoINC As Boolean
        Public Property FlAdultoINC() As Boolean
            Get
                Return _FlAdultoINC
            End Get
            Set(ByVal value As Boolean)
                _FlAdultoINC = value
            End Set
        End Property

        Private _NumberAPT As String
        Public Property NumberAPT() As String
            Get
                Return _NumberAPT
            End Get
            Set(ByVal value As String)
                _NumberAPT = value
            End Set
        End Property

        Public Class clsCotizacionDetPaxBE

            Private _IDDet As String
            Public Property IDDet() As String
                Get
                    Return _IDDet
                End Get
                Set(ByVal value As String)
                    _IDDet = value
                End Set
            End Property

            Private _IDPax As String
            Public Property IDPax() As String
                Get
                    Return _IDPax
                End Get
                Set(ByVal value As String)
                    _IDPax = value
                End Set
            End Property

            Private _Selecc As Boolean
            Public Property Selecc() As Boolean
                Get
                    Return _Selecc
                End Get
                Set(ByVal value As Boolean)
                    _Selecc = value
                End Set
            End Property

            Private _UserMod As String
            Public Property UserMod() As String
                Get
                    Return _UserMod
                End Get
                Set(ByVal value As String)
                    _UserMod = value
                End Set
            End Property


            Private _Total As Double
            Public Property Total() As Double
                Get
                    Return _Total
                End Get
                Set(ByVal value As Double)
                    _Total = value
                End Set
            End Property

            Private _SSTotal As Double
            Public Property SSTotal() As Double
                Get
                    Return _SSTotal
                End Get
                Set(ByVal value As Double)
                    _SSTotal = value
                End Set
            End Property

            Private _STTotal As Double
            Public Property STTotal() As Double
                Get
                    Return _STTotal
                End Get
                Set(ByVal value As Double)
                    _STTotal = value
                End Set
            End Property


            Private _IDTipoProv As String
            Public Property IDTipoProv() As String
                Get
                    Return _IDTipoProv
                End Get
                Set(ByVal value As String)
                    _IDTipoProv = value
                End Set
            End Property

            Private _Accion As Char
            Public Property Accion() As Char
                Get
                    Return _Accion
                End Get
                Set(ByVal value As Char)
                    _Accion = value
                End Set
            End Property

            Private _NoShow As Boolean
            Public Property NoShow() As Boolean
                Get
                    Return _NoShow
                End Get
                Set(ByVal value As Boolean)
                    _NoShow = value
                End Set
            End Property



            'Private _IDReserva_DetTemp As Byte
            'Public Property IDReserva_DetTemp() As byte
            '    Get
            '        Return _IDReserva_DetTemp
            '    End Get
            '    Set(ByVal value As Byte)
            '        _IDReserva_DetTemp = value
            '    End Set
            'End Property


        End Class

    End Class

    Public Class clsCategoriasHotelesBE
        'Inherits ServicedComponent
        Private _IdCab As Int32

        Private _IDUbigeo As String
        Public Property IDUbigeo() As String
            Get
                Return _IDUbigeo
            End Get
            Set(ByVal value As String)
                _IDUbigeo = value
            End Set
        End Property

        Private _IDProveedor As String
        Public Property IDProveedor() As String
            Get
                Return _IDProveedor
            End Get
            Set(ByVal value As String)
                _IDProveedor = value
            End Set
        End Property

        Private _IDProveedorUpd As String
        Public Property IDProveedorUpd() As String
            Get
                Return _IDProveedorUpd
            End Get
            Set(ByVal value As String)
                _IDProveedorUpd = value
            End Set
        End Property

        Private _IDServicio_Det As Integer
        Public Property IDServicio_Det() As Integer
            Get
                Return _IDServicio_Det
            End Get
            Set(ByVal value As Integer)
                _IDServicio_Det = value
            End Set
        End Property

        Private _NroOrd As Byte
        Public Property NroOrd() As Byte
            Get
                Return _NroOrd
            End Get
            Set(ByVal value As Byte)
                _NroOrd = value
            End Set
        End Property

        Private _Categoria As String
        Public Property Categoria() As String
            Get
                Return _Categoria
            End Get
            Set(ByVal value As String)
                _Categoria = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property
    End Class

    Public Class clsCotizacionTiposCambioBE

        Private _IDCab As Integer
        Public Property IDCab() As Integer
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Integer)
                _IDCab = value
            End Set
        End Property

        Private _CoMoneda As String
        Public Property CoMoneda() As String
            Get
                Return _CoMoneda
            End Get
            Set(ByVal value As String)
                _CoMoneda = value
            End Set
        End Property

        Private _SsTipCam As Double
        Public Property SsTipCam() As Double
            Get
                Return _SsTipCam
            End Get
            Set(ByVal value As Double)
                _SsTipCam = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

        'Private _AplicaTCam As Boolean
        'Public Property AplicaTCam() As Boolean
        '    Get
        '        Return _AplicaTCam
        '    End Get
        '    Set(ByVal value As Boolean)
        '        _AplicaTCam = value
        '    End Set
        'End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

    End Class

    Public Class clsCotizacionViaticosTCBE

        Private _NuViaticoTC As Integer
        Public Property NuViaticoTC() As Integer
            Get
                Return _NuViaticoTC
            End Get
            Set(ByVal value As Integer)
                _NuViaticoTC = value
            End Set
        End Property

        Private _Dia As Date
        Public Property Dia() As Date
            Get
                Return _Dia
            End Get
            Set(ByVal value As Date)
                _Dia = value
            End Set
        End Property

        Private _IDCab As Integer
        Public Property IDCab() As Integer
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Integer)
                _IDCab = value
            End Set
        End Property

        Private _IDServicio_Det As Integer
        Public Property IDServicio_Det() As Integer
            Get
                Return _IDServicio_Det
            End Get
            Set(ByVal value As Integer)
                _IDServicio_Det = value
            End Set
        End Property

        Private _Item As Byte
        Public Property Item() As Byte
            Get
                Return _Item
            End Get
            Set(ByVal value As Byte)
                _Item = value
            End Set
        End Property

        Private _CostoUSD As Double
        Public Property CostoUSD() As Double
            Get
                Return _CostoUSD
            End Get
            Set(ByVal value As Double)
                _CostoUSD = value
            End Set
        End Property

        Private _CostoSOL As String
        Public Property CostoSOL() As String
            Get
                Return _CostoSOL
            End Get
            Set(ByVal value As String)
                _CostoSOL = value
            End Set
        End Property

        Private _FlActivo As Boolean
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal value As Boolean)
                _FlActivo = value
            End Set
        End Property


        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

    End Class

    Public Class clsCotizacionIncluidoBE
        'Inherits ServicedComponent

        Private _IDCab As Integer
        Public Property IDCab() As Integer
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Integer)
                _IDCab = value
            End Set
        End Property

        Private _IDIncluido As Integer
        Public Property IDIncluido() As Integer
            Get
                Return _IDIncluido
            End Get
            Set(ByVal value As Integer)
                _IDIncluido = value
            End Set
        End Property

        Private _Orden As Byte
        Public Property Orden() As Byte
            Get
                Return _Orden
            End Get
            Set(ByVal value As Byte)
                _Orden = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property
    End Class

    Public Class clsCotizacionNoIncluidoBE
        Inherits clsCotizacionIncluidoBE

    End Class

    Public Class clsCotizacionIncluidoObservBE
        Inherits clsCotizacionIncluidoBE

    End Class

    Public Class clsPaxTransporteBE
        'Inherits ServicedComponent
        Private _IDTransporte As String
        Public Property IDTransporte() As String
            Get
                Return _IDTransporte
            End Get
            Set(ByVal value As String)
                _IDTransporte = value
            End Set
        End Property

        Private _IDPaxTransp As String
        Public Property IDPaxTransp() As String
            Get
                Return _IDPaxTransp
            End Get
            Set(ByVal value As String)
                _IDPaxTransp = value
            End Set
        End Property

        Private _IDPax As String
        Public Property IDPax() As String
            Get
                Return _IDPax
            End Get
            Set(ByVal value As String)
                _IDPax = value
            End Set
        End Property

        Private _CodReservaTransp As String
        Public Property CodReservaTransp() As String
            Get
                Return _CodReservaTransp
            End Get
            Set(ByVal value As String)
                _CodReservaTransp = value
            End Set
        End Property

        Private _IDProveedorGuia As String
        Public Property IDProveedorGuia() As String
            Get
                Return _IDProveedorGuia
            End Get
            Set(ByVal value As String)
                _IDProveedorGuia = value
            End Set
        End Property

        Private _SsReembolso As Double
        Public Property Reembolso() As Double
            Get
                Return _SsReembolso
            End Get
            Set(ByVal value As Double)
                _SsReembolso = value
            End Set
        End Property


        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class
    Public Class clsVuelosGridBE
        'Inherits ServicedComponent
        Private _ID As String
        Public Property ID() As String
            Get
                Return _ID
            End Get
            Set(ByVal value As String)
                _ID = value
            End Set
        End Property

        Private _IDDetV As String
        Public Property IDDetV() As String
            Get
                Return _IDDetV
            End Get
            Set(ByVal value As String)
                _IDDetV = value
            End Set
        End Property

        Private _DiaV As Date
        Public Property DiaV() As Date
            Get
                Return _DiaV
            End Get
            Set(ByVal value As Date)
                _DiaV = value
            End Set
        End Property

        Private _DescripcionV As String
        Public Property DescripcionV() As String
            Get
                Return _DescripcionV
            End Get
            Set(ByVal value As String)
                _DescripcionV = value
            End Set
        End Property

        Private _CantPaxV As Int16
        Public Property CantPaxV() As Int16
            Get
                Return _CantPaxV
            End Get
            Set(ByVal value As Int16)
                _CantPaxV = value
            End Set
        End Property

        Private _IdLineaA As String
        Public Property IdLineaA() As String
            Get
                Return _IdLineaA
            End Get
            Set(ByVal value As String)
                _IdLineaA = value
            End Set
        End Property

        Private _DescProveedorV As String
        Public Property DescProveedorV() As String
            Get
                Return _DescProveedorV
            End Get
            Set(ByVal value As String)
                _DescProveedorV = value
            End Set
        End Property

        Private _RutaO As String
        Public Property RutaO() As String
            Get
                Return _RutaO
            End Get
            Set(ByVal value As String)
                _RutaO = value
            End Set
        End Property

        Private _OrigenVuelo As String
        Public Property OrigenVuelo() As String
            Get
                Return _OrigenVuelo
            End Get
            Set(ByVal value As String)
                _OrigenVuelo = value
            End Set
        End Property

        Private _RutaD As String
        Public Property RutaD() As String
            Get
                Return _RutaD
            End Get
            Set(ByVal value As String)
                _RutaD = value
            End Set
        End Property

        Private _Destino As String
        Public Property Destino() As String
            Get
                Return _Destino
            End Get
            Set(ByVal value As String)
                _Destino = value
            End Set
        End Property

        Private _FechaSalida As Date
        Public Property FechaSalida() As Date
            Get
                Return _FechaSalida
            End Get
            Set(ByVal value As Date)
                _FechaSalida = value
            End Set
        End Property

        Private _FechaRetorno As Date
        Public Property FechaRetorno() As Date
            Get
                Return _FechaRetorno
            End Get
            Set(ByVal value As Date)
                _FechaRetorno = value
            End Set
        End Property

        Private _TipoTransporteV As Char
        Public Property TipoTransporteV() As Char
            Get
                Return _TipoTransporteV
            End Get
            Set(ByVal value As Char)
                _TipoTransporteV = value
            End Set
        End Property

        Private _RutaVuelo As String
        Public Property RutaVuelo() As String
            Get
                Return _RutaVuelo
            End Get
            Set(ByVal value As String)
                _RutaVuelo = value
            End Set
        End Property


    End Class

    Public Class clsBusGridBE
        'Inherits ServicedComponent
        Private _IDDetB As String
        Public Property IDDetB() As String
            Get
                Return _IDDetB
            End Get
            Set(ByVal value As String)
                _IDDetB = value
            End Set
        End Property

        Private _DiaB As Date
        Public Property DiaB() As Date
            Get
                Return _DiaB
            End Get
            Set(ByVal value As Date)
                _DiaB = value
            End Set
        End Property

        Private _ServicioB As String
        Public Property ServicioB() As String
            Get
                Return _ServicioB
            End Get
            Set(ByVal value As String)
                _ServicioB = value
            End Set
        End Property

        Private _CantPaxB As Int16
        Public Property CantPaxB() As Int16
            Get
                Return _CantPaxB
            End Get
            Set(ByVal value As Int16)
                _CantPaxB = value
            End Set
        End Property

        Private _IdLineaAB As String
        Public Property IdLineaAB() As String
            Get
                Return _IdLineaAB
            End Get
            Set(ByVal value As String)
                _IdLineaAB = value
            End Set
        End Property

        Private _DescProveedorB As String
        Public Property DescProveedorB() As String
            Get
                Return _DescProveedorB
            End Get
            Set(ByVal value As String)
                _DescProveedorB = value
            End Set
        End Property

        Private _RutaOB As String
        Public Property RutaOB() As String
            Get
                Return _RutaOB
            End Get
            Set(ByVal value As String)
                _RutaOB = value
            End Set
        End Property

        Private _OrigenB As String
        Public Property OrigenB() As String
            Get
                Return _OrigenB
            End Get
            Set(ByVal value As String)
                _OrigenB = value
            End Set
        End Property

        Private _RutaDB As String
        Public Property RutaDB() As String
            Get
                Return _RutaDB
            End Get
            Set(ByVal value As String)
                _RutaDB = value
            End Set
        End Property

        Private _DestinoB As String
        Public Property DestinoB() As String
            Get
                Return _DestinoB
            End Get
            Set(ByVal value As String)
                _DestinoB = value
            End Set
        End Property

        Private _Fec_SalidaB As Date
        Public Property Fec_SalidaB() As Date
            Get
                Return _Fec_SalidaB
            End Get
            Set(ByVal value As Date)
                _Fec_SalidaB = value
            End Set
        End Property

        Private _Fec_RetornoB As Date
        Public Property Fec_RetornoB() As Date
            Get
                Return _Fec_RetornoB
            End Get
            Set(ByVal value As Date)
                _Fec_RetornoB = value
            End Set
        End Property

        Private _SalidaB As String
        Public Property SalidaB() As String
            Get
                Return _SalidaB
            End Get
            Set(ByVal value As String)
                _SalidaB = value
            End Set
        End Property

        Private _LlegadaB As String
        Public Property LlegadaB() As String
            Get
                Return _LlegadaB
            End Get
            Set(ByVal value As String)
                _LlegadaB = value
            End Set
        End Property

        Private _HrRecojoB As String
        Public Property HrRecojoB() As String
            Get
                Return _HrRecojoB
            End Get
            Set(ByVal value As String)
                _HrRecojoB = value
            End Set
        End Property

        Private _TipoTransporteB As Char
        Public Property TipoTransporteB() As Char
            Get
                Return _TipoTransporteB
            End Get
            Set(ByVal value As Char)
                _TipoTransporteB = value
            End Set
        End Property

        Private _IDCABB As Integer
        Public Property IDCABB() As Integer
            Get
                Return _IDCABB
            End Get
            Set(ByVal value As Integer)
                _IDCABB = value
            End Set
        End Property

        Private _IDB As String
        Public Property IDB() As String
            Get
                Return _IDB
            End Get
            Set(ByVal value As String)
                _IDB = value
            End Set
        End Property

        Private _EmitidoSetoursB As Boolean
        Public Property EmitidoSetoursB() As Boolean
            Get
                Return _EmitidoSetoursB
            End Get
            Set(ByVal value As Boolean)
                _EmitidoSetoursB = value
            End Set
        End Property

        Private _RutaB As String
        Public Property RutaB() As String
            Get
                Return _RutaB
            End Get
            Set(ByVal value As String)
                _RutaB = value
            End Set
        End Property

        Private _Num_BoletoB As String
        Public Property Num_BoletoB() As String
            Get
                Return _Num_BoletoB
            End Get
            Set(ByVal value As String)
                _Num_BoletoB = value
            End Set
        End Property

        Private _CodReservaB As String
        Public Property CodReservaB() As String
            Get
                Return _CodReservaB
            End Get
            Set(ByVal value As String)
                _CodReservaB = value
            End Set
        End Property

    End Class
    Public Class clsTrenGridBE
        'Inherits ServicedComponent
        Private _IDDetT As String
        Public Property IDDetT() As String
            Get
                Return _IDDetT
            End Get
            Set(ByVal value As String)
                _IDDetT = value
            End Set
        End Property

        Private _DiaT As Date
        Public Property DiaT() As Date
            Get
                Return _DiaT
            End Get
            Set(ByVal value As Date)
                _DiaT = value
            End Set
        End Property

        Private _ServicioTren As String
        Public Property ServicioTren() As String
            Get
                Return _ServicioTren
            End Get
            Set(ByVal value As String)
                _ServicioTren = value
            End Set
        End Property

        Private _CantPaxT As Int16
        Public Property CantPaxT() As Int16
            Get
                Return _CantPaxT
            End Get
            Set(ByVal value As Int16)
                _CantPaxT = value
            End Set
        End Property

        Private _IdLineaAT As String
        Public Property IdLineaAT() As String
            Get
                Return _IdLineaAT
            End Get
            Set(ByVal value As String)
                _IdLineaAT = value
            End Set
        End Property

        Private _DescProveedorT As String
        Public Property DescProveedorT() As String
            Get
                Return _DescProveedorT
            End Get
            Set(ByVal value As String)
                _DescProveedorT = value
            End Set
        End Property

        Private _RutaOT As String
        Public Property RutaOT() As String
            Get
                Return _RutaOT
            End Get
            Set(ByVal value As String)
                _RutaOT = value
            End Set
        End Property

        Private _OrigenT As String
        Public Property OrigenT() As String
            Get
                Return _OrigenT
            End Get
            Set(ByVal value As String)
                _OrigenT = value
            End Set
        End Property

        Private _RutaDT As String
        Public Property RutaDT() As String
            Get
                Return _RutaDT
            End Get
            Set(ByVal value As String)
                _RutaDT = value
            End Set
        End Property

        Private _DestinoT As String
        Public Property DestinoT() As String
            Get
                Return _DestinoT
            End Get
            Set(ByVal value As String)
                _DestinoT = value
            End Set
        End Property

        Private _RutaT As String
        Public Property RutaT() As String
            Get
                Return _RutaT
            End Get
            Set(ByVal value As String)
                _RutaT = value
            End Set
        End Property

        Private _Fec_SalidaT As Date
        Public Property Fec_SalidaT() As Date
            Get
                Return _Fec_SalidaT
            End Get
            Set(ByVal value As Date)
                _Fec_SalidaT = value
            End Set
        End Property

        Private _Fec_RetornoT As Date
        Public Property Fec_RetornoT() As Date
            Get
                Return _Fec_RetornoT
            End Get
            Set(ByVal value As Date)
                _Fec_RetornoT = value
            End Set
        End Property

        Private _SalidaT As String
        Public Property SalidaT() As String
            Get
                Return _SalidaT
            End Get
            Set(ByVal value As String)
                _SalidaT = value
            End Set
        End Property

        Private _LlegadaT As String
        Public Property LlegadaT() As String
            Get
                Return _LlegadaT
            End Get
            Set(ByVal value As String)
                _LlegadaT = value
            End Set
        End Property

        Private _HrRecojoT As String
        Public Property HrRecojoT() As String
            Get
                Return _HrRecojoT
            End Get
            Set(ByVal value As String)
                _HrRecojoT = value
            End Set
        End Property

        Private _TipoTransporteT As Char
        Public Property TipoTransporteT() As Char
            Get
                Return _TipoTransporteT
            End Get
            Set(ByVal value As Char)
                _TipoTransporteT = value
            End Set
        End Property

        Private _IDCABT As Integer
        Public Property IDCABT() As Integer
            Get
                Return _IDCABT
            End Get
            Set(ByVal value As Integer)
                _IDCABT = value
            End Set
        End Property

        Private _IDT As String
        Public Property IDT() As String
            Get
                Return _IDT
            End Get
            Set(ByVal value As String)
                _IDT = value
            End Set
        End Property

        Private _EmitidoSetoursT As Boolean
        Public Property EmitidoSetoursT() As Boolean
            Get
                Return _EmitidoSetoursT
            End Get
            Set(ByVal value As Boolean)
                _EmitidoSetoursT = value
            End Set
        End Property

        Private _IDServicio_DetPeruRailT As Integer
        Public Property IDServicio_DetPeruRailT() As Integer
            Get
                Return _IDServicio_DetPeruRailT
            End Get
            Set(ByVal value As Integer)
                _IDServicio_DetPeruRailT = value
            End Set
        End Property

        Private _VueloT As String
        Public Property VueloT() As String
            Get
                Return _VueloT
            End Get
            Set(ByVal value As String)
                _VueloT = value
            End Set
        End Property

        Private _CodReservaT As String
        Public Property CodReservaT() As String
            Get
                Return _CodReservaT
            End Get
            Set(ByVal value As String)
                _CodReservaT = value
            End Set
        End Property

        'Private _ListaTrenGrid As List(Of clsTrenGridBE)
        'Public Property ListaTrenGrid() As List(Of clsTrenGridBE)
        '    Get
        '        Return _ListaTrenGrid
        '    End Get
        '    Set(ByVal value As List(Of clsTrenGridBE))
        '        _ListaTrenGrid = value
        '    End Set
        'End Property

    End Class

    Public Class clsCotiDetGridBE
        'Inherits ServicedComponent

        Private _IDCab As Integer
        Public Property IDCab() As Integer
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Integer)
                _IDCab = value
            End Set
        End Property

        Private _IDDet As String
        Public Property IDDet() As String
            Get
                Return _IDDet
            End Get
            Set(ByVal value As String)
                _IDDet = value
            End Set
        End Property

        Private _IDDetRel As String
        Public Property IDDetRel() As String
            Get
                Return _IDDetRel
            End Get
            Set(ByVal value As String)
                _IDDetRel = value
            End Set
        End Property

        Private _Dia As Date
        Public Property Dia() As Date
            Get
                Return _Dia
            End Get
            Set(ByVal value As Date)
                _Dia = value
            End Set
        End Property

        Private _Item As Single
        Public Property Item() As Single
            Get
                Return _Item
            End Get
            Set(ByVal value As Single)
                _Item = value
            End Set
        End Property

        Private _DescServicioDet As String
        Public Property DescServicioDet() As String
            Get
                Return _DescServicioDet
            End Get
            Set(ByVal value As String)
                _DescServicioDet = value
            End Set
        End Property

        Private _IDTipoProv As String
        Public Property IDTipoProv() As String
            Get
                Return _IDTipoProv
            End Get
            Set(ByVal value As String)
                _IDTipoProv = value
            End Set
        End Property

        Private _IDPais As String
        Public Property IDPais() As String
            Get
                Return _IDPais
            End Get
            Set(ByVal value As String)
                _IDPais = value
            End Set
        End Property

        Private _IDCiudad As String
        Public Property IDCiudad() As String
            Get
                Return _IDCiudad
            End Get
            Set(ByVal value As String)
                _IDCiudad = value
            End Set
        End Property

        Private _DescCiudad As String
        Public Property DescCiudad() As String
            Get
                Return _DescCiudad
            End Get
            Set(ByVal value As String)
                _DescCiudad = value
            End Set
        End Property

        Private _IDProveedor As String
        Public Property IDProveedor() As String
            Get
                Return _IDProveedor
            End Get
            Set(ByVal value As String)
                _IDProveedor = value
            End Set
        End Property

        Private _DescProveedor As String
        Public Property DescProveedor() As String
            Get
                Return _DescProveedor
            End Get
            Set(ByVal value As String)
                _DescProveedor = value
            End Set
        End Property

        Private _IDServicio As String
        Public Property IDServicio() As String
            Get
                Return _IDServicio
            End Get
            Set(ByVal value As String)
                _IDServicio = value
            End Set
        End Property

        Private _IDServicio_Det As Integer
        Public Property IDServicio_Det() As Integer
            Get
                Return _IDServicio_Det
            End Get
            Set(ByVal value As Integer)
                _IDServicio_Det = value
            End Set
        End Property

        Private _IDUbigeoOri As String
        Public Property IDUbigeoOri() As String
            Get
                Return _IDUbigeoOri
            End Get
            Set(ByVal value As String)
                _IDUbigeoOri = value
            End Set
        End Property

        Private _DescUbigeoOri As String
        Public Property DescUbigeoOri() As String
            Get
                Return _DescUbigeoOri
            End Get
            Set(ByVal value As String)
                _DescUbigeoOri = value
            End Set
        End Property

        Private _IDUbigeoDes As String
        Public Property IDUbigeoDes() As String
            Get
                Return _IDUbigeoDes
            End Get
            Set(ByVal value As String)
                _IDUbigeoDes = value
            End Set
        End Property

        Private _PlanAlimenticio As Boolean
        Public Property PlanAlimenticio() As Boolean
            Get
                Return _PlanAlimenticio
            End Get
            Set(ByVal value As Boolean)
                _PlanAlimenticio = value
            End Set
        End Property

        Private _IncGuia As Boolean
        Public Property IncGuia() As Boolean
            Get
                Return _IncGuia
            End Get
            Set(ByVal value As Boolean)
                _IncGuia = value
            End Set
        End Property

        Private _DescUbigeoDes As String
        Public Property DescUbigeoDes() As String
            Get
                Return _DescUbigeoDes
            End Get
            Set(ByVal value As String)
                _DescUbigeoDes = value
            End Set
        End Property

        Private _TipoTransporte As Char
        Public Property TipoTransporte() As Char
            Get
                Return _TipoTransporte
            End Get
            Set(ByVal value As Char)
                _TipoTransporte = value
            End Set
        End Property

        Private _NroPax As Int16
        Public Property NroPax() As Int16
            Get
                Return _NroPax
            End Get
            Set(ByVal value As Int16)
                _NroPax = value
            End Set
        End Property

        Private _NroLiberados As Int16
        Public Property NroLiberados() As Int16
            Get
                Return _NroLiberados
            End Get
            Set(ByVal value As Int16)
                _NroLiberados = value
            End Set
        End Property


        'Private _IDServicio As String
        'Public Property IDServicio() As String
        '    Get
        '        Return _IDServicio
        '    End Get
        '    Set(ByVal value As String)
        '        _IDServicio = value
        '    End Set
        'End Property

        'Private _Anio As String
        'Public Property Anio() As String
        '    Get
        '        Return _Anio
        '    End Get
        '    Set(ByVal value As String)
        '        _Anio = value
        '    End Set
        'End Property

        'Private _Tipo As String
        'Public Property Tipo() As String
        '    Get
        '        Return _Tipo
        '    End Get
        '    Set(ByVal value As String)
        '        _Tipo = value
        '    End Set
        'End Property


    End Class

    Public Class clsTicketsPaxPeruRailBE
        Private _NuFile As String
        Public Property NuFile() As String
            Get
                Return _NuFile
            End Get
            Set(ByVal value As String)
                _NuFile = value
            End Set
        End Property
        Private _CoReserva As String
        Public Property CoReserva() As String
            Get
                Return _CoReserva
            End Get
            Set(ByVal value As String)
                _CoReserva = value
            End Set
        End Property
        Private _CoCiudadOri As String
        Public Property CoCiudadOri() As String
            Get
                Return _CoCiudadOri
            End Get
            Set(ByVal value As String)
                _CoCiudadOri = value
            End Set
        End Property
        Private _CoCiudadDes As String
        Public Property CoCiudadDes() As String
            Get
                Return _CoCiudadDes
            End Get
            Set(ByVal value As String)
                _CoCiudadDes = value
            End Set
        End Property
        Private _SsValorVtaUSD As Double
        Public Property SsValorVentaUSD() As Double
            Get
                Return _SsValorVtaUSD
            End Get
            Set(ByVal value As Double)
                _SsValorVtaUSD = value
            End Set
        End Property
        Private _SsIgvUSD As Double
        Public Property SsIgvUSD() As Double
            Get
                Return _SsIgvUSD
            End Get
            Set(ByVal value As Double)
                _SsIgvUSD = value
            End Set
        End Property
        Private _SsTotalUSD As Double
        Public Property SsTotalUSD() As Double
            Get
                Return _SsTotalUSD
            End Get
            Set(ByVal value As Double)
                _SsTotalUSD = value
            End Set
        End Property
        Private _NuDocum As String
        Public Property NuDocum() As String
            Get
                Return _NuDocum
            End Get
            Set(ByVal value As String)
                _NuDocum = value
            End Set
        End Property
        Private _FeEmision As Date
        Public Property FeEmision() As Date
            Get
                Return _FeEmision
            End Get
            Set(ByVal value As Date)
                _FeEmision = value
            End Set
        End Property

        Private _ErrorSAP As String
        Public Property ErrorSAP() As String
            Get
                Return _ErrorSAP
            End Get
            Set(ByVal value As String)
                _ErrorSAP = value
            End Set
        End Property

        Private _ErrorSetra As String
        Public Property ErrorSetra() As String
            Get
                Return _ErrorSetra
            End Get
            Set(ByVal value As String)
                _ErrorSetra = value
            End Set
        End Property

        'Public Class clsTicketsPaxPeruRailAgrupBE
        '    Inherits clsTicketsPaxPeruRailBE
        'End Class

        'Private _ListaTicketsPaxPeruRailAgrup As List(Of clsTicketsPaxPeruRailAgrupBE)
        'Public Property ListaTicketsPaxPeruRailAgrup() As List(Of clsTicketsPaxPeruRailAgrupBE)
        '    Get
        '        Return _ListaTicketsPaxPeruRailAgrup
        '    End Get
        '    Set(ByVal value As List(Of clsTicketsPaxPeruRailAgrupBE))
        '        _ListaTicketsPaxPeruRailAgrup = value
        '    End Set
        'End Property
    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsAcomodoVehiculoBE
        Private _NuVehiculo As Int16
        Public Property NuVehiculo() As Int16
            Get
                Return _NuVehiculo
            End Get
            Set(ByVal value As Int16)
                _NuVehiculo = value
            End Set
        End Property
        Private _IDDet As String
        Public Property IDDet() As String
            Get
                Return _IDDet
            End Get
            Set(ByVal value As String)
                _IDDet = value
            End Set
        End Property
        Private _QtVehiculos As Byte
        Public Property QtVehiculos() As Byte
            Get
                Return _QtVehiculos
            End Get
            Set(ByVal value As Byte)
                _QtVehiculos = value
            End Set
        End Property
        Private _QtPax As Byte
        Public Property QtPax() As Byte
            Get
                Return _QtPax
            End Get
            Set(ByVal value As Byte)
                _QtPax = value
            End Set
        End Property
        Private _FlGuia As Boolean
        Public Property FlGuia() As Boolean
            Get
                Return _FlGuia
            End Get
            Set(ByVal value As Boolean)
                _FlGuia = value
            End Set
        End Property
        Private _NoVehiculo As String
        Public Property NoVehiculo() As String
            Get
                Return _NoVehiculo
            End Get
            Set(ByVal value As String)
                _NoVehiculo = value
            End Set
        End Property
        Private _QtCapacidad As Byte
        Public Property QtCapacidad() As Byte
            Get
                Return _QtCapacidad
            End Get
            Set(ByVal value As Byte)
                _QtCapacidad = value
            End Set
        End Property
        Private _FlCopiar As Boolean
        Public Property FlCopiar() As Boolean
            Get
                Return _FlCopiar
            End Get
            Set(ByVal value As Boolean)
                _FlCopiar = value
            End Set
        End Property
        'Private _CoUbigeo As String
        'Public Property CoUbigeo() As String
        '    Get
        '        Return _CoUbigeo
        '    End Get
        '    Set(ByVal value As String)
        '        _CoUbigeo = value
        '    End Set
        'End Property
        'Private _CoVariante As String
        'Public Property CoVariante() As String
        '    Get
        '        Return _CoVariante
        '    End Get
        '    Set(ByVal value As String)
        '        _CoVariante = value
        '    End Set
        'End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsAcomodoVehiculo_Ext_BE
        Private _IDDet As String
        Public Property IDDet() As String
            Get
                Return _IDDet
            End Get
            Set(ByVal value As String)
                _IDDet = value
            End Set
        End Property
        Private _QtPax As Int16
        Public Property QtPax() As Int16
            Get
                Return _QtPax
            End Get
            Set(ByVal value As Int16)
                _QtPax = value
            End Set
        End Property
        Private _QtGrupo As Byte
        Public Property QtGrupo() As Byte
            Get
                Return _QtGrupo
            End Get
            Set(ByVal value As Byte)
                _QtGrupo = value
            End Set
        End Property
        Private _FlCopiar As Boolean
        Public Property FlCopiar() As Boolean
            Get
                Return _FlCopiar
            End Get
            Set(ByVal value As Boolean)
                _FlCopiar = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsAcomodoVehiculoCopiaBE
        Private _IDDetBase As String
        Public Property IDDetBase() As String
            Get
                Return _IDDetBase
            End Get
            Set(ByVal value As String)
                _IDDetBase = value
            End Set
        End Property

        Private _IDDet As String
        Public Property IDDet() As String
            Get
                Return _IDDet
            End Get
            Set(ByVal value As String)
                _IDDet = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsAcomodoVehiculoIntDetPaxBE
        Inherits clsAcomodoVehiculoBE
        Private _NuPax As Integer
        Public Property NuPax() As Integer
            Get
                Return _NuPax
            End Get
            Set(ByVal value As Integer)
                _NuPax = value
            End Set
        End Property

        Private _NuNroVehiculo As Byte
        Public Property NuNroVehiculo() As Byte
            Get
                Return _NuNroVehiculo
            End Get
            Set(ByVal value As Byte)
                _NuNroVehiculo = value
            End Set
        End Property

        Private _Selecc As Boolean
        Public Property Selecc() As Boolean
            Get
                Return _Selecc
            End Get
            Set(ByVal value As Boolean)
                _Selecc = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsAcomodoVehiculoExtDetPaxBE
        Inherits clsAcomodoVehiculo_Ext_BE
        Private _NuPax As Integer
        Public Property NuPax() As Integer
            Get
                Return _NuPax
            End Get
            Set(ByVal value As Integer)
                _NuPax = value
            End Set
        End Property

        Private _Selecc As Boolean
        Public Property Selecc() As Boolean
            Get
                Return _Selecc
            End Get
            Set(ByVal value As Boolean)
                _Selecc = value
            End Set
        End Property

        Private _NuGrupo As Byte
        Public Property NuGrupo() As Byte
            Get
                Return _NuGrupo
            End Get
            Set(ByVal value As Byte)
                _NuGrupo = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsPedidoBE
        Private _NuPedInt As Integer
        Public Property NuPedInt() As Integer
            Get
                Return _NuPedInt
            End Get
            Set(ByVal value As Integer)
                _NuPedInt = value
            End Set
        End Property
        Private _NuPedido As String
        Public Property NuPedido() As String
            Get
                Return _NuPedido
            End Get
            Set(ByVal value As String)
                _NuPedido = value
            End Set
        End Property
        Private _CoCliente As String
        Public Property CoCliente() As String
            Get
                Return _CoCliente
            End Get
            Set(ByVal value As String)
                _CoCliente = value
            End Set
        End Property
        Private _FePedido As Date
        Public Property FePedido() As Date
            Get
                Return _FePedido
            End Get
            Set(ByVal value As Date)
                _FePedido = value
            End Set
        End Property
        Private _CoEjeVta As String
        Public Property CoEjeVta() As String
            Get
                Return _CoEjeVta
            End Get
            Set(ByVal value As String)
                _CoEjeVta = value
            End Set
        End Property
        Private _TxTitulo As String
        Public Property TxTitulo() As String
            Get
                Return _TxTitulo
            End Get
            Set(ByVal value As String)
                _TxTitulo = value
            End Set
        End Property
        Private _CoEstado As String
        Public Property CoEstado() As String
            Get
                Return _CoEstado
            End Get
            Set(ByVal value As String)
                _CoEstado = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        'Private _FlCreadoDsdAddIn As Boolean
        'Public Property FlCreadoDsdAddIn() As Boolean
        '    Get
        '        Return _FlCreadoDsdAddIn
        '    End Get
        '    Set(ByVal value As Boolean)
        '        _FlCreadoDsdAddIn = value
        '    End Set
        'End Property

    End Class

    Private _ListaTicketsPaxPeruRail As List(Of clsTicketsPaxPeruRailBE)
    Public Property ListaTicketsPaxPeruRail() As List(Of clsTicketsPaxPeruRailBE)
        Get
            Return _ListaTicketsPaxPeruRail
        End Get
        Set(ByVal value As List(Of clsTicketsPaxPeruRailBE))
            _ListaTicketsPaxPeruRail = value
        End Set
    End Property


    Private _ListaCategHoteles As List(Of clsCategoriasHotelesBE)
    Public Property ListaCategHoteles() As List(Of clsCategoriasHotelesBE)
        Get
            Return _ListaCategHoteles
        End Get
        Set(ByVal value As List(Of clsCategoriasHotelesBE))
            _ListaCategHoteles = value
        End Set
    End Property

    Private _ListaPax As List(Of clsCotizacionPaxBE)
    Public Property ListaPax() As List(Of clsCotizacionPaxBE)
        Get
            Return _ListaPax
        End Get
        Set(ByVal value As List(Of clsCotizacionPaxBE))
            _ListaPax = value
        End Set
    End Property

    Private _ListaDetPax As List(Of clsCotizacionPaxBE.clsCotizacionDetPaxBE)
    Public Property ListaDetPax() As List(Of clsCotizacionPaxBE.clsCotizacionDetPaxBE)
        Get
            Return _ListaDetPax
        End Get
        Set(ByVal value As List(Of clsCotizacionPaxBE.clsCotizacionDetPaxBE))
            _ListaDetPax = value
        End Set
    End Property

    Private _ListaIDIncluidos As List(Of clsCotizacionIncluidoBE)
    Public Property ListadeIDIncluido() As List(Of clsCotizacionIncluidoBE)
        Get
            Return _ListaIDIncluidos
        End Get
        Set(ByVal value As List(Of clsCotizacionIncluidoBE))
            _ListaIDIncluidos = value
        End Set
    End Property

    Private _ListaTipoCambios As List(Of clsCotizacionTiposCambioBE)
    Public Property ListaTipoCambios() As List(Of clsCotizacionTiposCambioBE)
        Get
            Return _ListaTipoCambios
        End Get
        Set(ByVal value As List(Of clsCotizacionTiposCambioBE))
            _ListaTipoCambios = value
        End Set
    End Property

    Private _ListaIDNoIncluidos As List(Of clsCotizacionNoIncluidoBE)
    Public Property ListadeIDNoIncluidos() As List(Of clsCotizacionNoIncluidoBE)
        Get
            Return _ListaIDNoIncluidos
        End Get
        Set(ByVal value As List(Of clsCotizacionNoIncluidoBE))
            _ListaIDNoIncluidos = value
        End Set
    End Property


    Private _ListaIncluidosObserv As List(Of clsCotizacionIncluidoObservBE)
    Public Property ListaIncluidosObserv() As List(Of clsCotizacionIncluidoObservBE)
        Get
            Return _ListaIncluidosObserv
        End Get
        Set(ByVal value As List(Of clsCotizacionIncluidoObservBE))
            _ListaIncluidosObserv = value
        End Set
    End Property


    Private _ListaCotizacionVuelos As List(Of clsCotizacionVuelosBE)
    Public Property ListaCotizacionVuelo() As List(Of clsCotizacionVuelosBE)
        Get
            Return _ListaCotizacionVuelos
        End Get
        Set(ByVal value As List(Of clsCotizacionVuelosBE))
            _ListaCotizacionVuelos = value
        End Set
    End Property

    Private _ListaPaxTransporte As List(Of clsPaxTransporteBE)
    Public Property ListaPaxTransporte() As List(Of clsPaxTransporteBE)
        Get
            Return _ListaPaxTransporte
        End Get
        Set(ByVal value As List(Of clsPaxTransporteBE))
            _ListaPaxTransporte = value
        End Set
    End Property

    Private _ListaDetalleCotizacionAcomodoEspecial As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionAcomodoEspecialBE)
    Public Property ListaDetalleCotizacionAcomodoEspecial() As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionAcomodoEspecialBE)
        Get
            Return _ListaDetalleCotizacionAcomodoEspecial
        End Get
        Set(ByVal value As List(Of clsDetalleCotizacionBE.clsDetalleCotizacionAcomodoEspecialBE))
            _ListaDetalleCotizacionAcomodoEspecial = value
        End Set
    End Property

    Private _ListaDistribucAcomodoEspecial As List(Of clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE)
    Public Property ListaDistribucAcomodoEspecial() As List(Of clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE)
        Get
            Return _ListaDistribucAcomodoEspecial
        End Get
        Set(ByVal value As List(Of clsDetalleCotizacionBE.clsDistribucAcomodoEspecialBE))
            _ListaDistribucAcomodoEspecial = value
        End Set
    End Property

    Private _ListaTourConductor As List(Of clsTourConductorBE)
    Public Property ListaTourConductor() As List(Of clsTourConductorBE)
        Get
            Return _ListaTourConductor
        End Get
        Set(ByVal value As List(Of clsTourConductorBE))
            _ListaTourConductor = value
        End Set
    End Property

    Private _ListaCostosTourConductor As List(Of clsCostosTourConductorBE)
    Public Property ListaCostosTourConductor() As List(Of clsCostosTourConductorBE)
        Get
            Return _ListaCostosTourConductor
        End Get
        Set(ByVal value As List(Of clsCostosTourConductorBE))
            _ListaCostosTourConductor = value
        End Set
    End Property

    Private _ListaAcomodoVehiculo As List(Of clsAcomodoVehiculoBE)
    Public Property ListaAcomodoVehiculo() As List(Of clsAcomodoVehiculoBE)
        Get
            Return _ListaAcomodoVehiculo
        End Get
        Set(ByVal value As List(Of clsAcomodoVehiculoBE))
            _ListaAcomodoVehiculo = value
        End Set
    End Property

    Private _ListaAcomodoVehiculoExt As List(Of clsAcomodoVehiculo_Ext_BE)
    Public Property ListaAcomodoVehiculoExt() As List(Of clsAcomodoVehiculo_Ext_BE)
        Get
            Return _ListaAcomodoVehiculoExt
        End Get
        Set(ByVal value As List(Of clsAcomodoVehiculo_Ext_BE))
            _ListaAcomodoVehiculoExt = value
        End Set
    End Property

    Private _ListaIDDetCopiarAcomVehi As List(Of clsAcomodoVehiculoCopiaBE)
    Public Property ListaIDDetCopiarAcomVehi() As List(Of clsAcomodoVehiculoCopiaBE)
        Get
            Return _ListaIDDetCopiarAcomVehi
        End Get
        Set(ByVal value As List(Of clsAcomodoVehiculoCopiaBE))
            _ListaIDDetCopiarAcomVehi = value
        End Set
    End Property

    Private _ListaAcomodoVehiculoIntDetPax As List(Of clsAcomodoVehiculoIntDetPaxBE)
    Public Property ListaAcomodoVehiculoIntDetPax() As List(Of clsAcomodoVehiculoIntDetPaxBE)
        Get
            Return _ListaAcomodoVehiculoIntDetPax
        End Get
        Set(ByVal value As List(Of clsAcomodoVehiculoIntDetPaxBE))
            _ListaAcomodoVehiculoIntDetPax = value
        End Set
    End Property

    Private _ListaAcomodoVehiculoExtDetPax As List(Of clsAcomodoVehiculoExtDetPaxBE)
    Public Property ListaAcomodoVehiculoExtDetPax() As List(Of clsAcomodoVehiculoExtDetPaxBE)
        Get
            Return _ListaAcomodoVehiculoExtDetPax
        End Get
        Set(ByVal value As List(Of clsAcomodoVehiculoExtDetPaxBE))
            _ListaAcomodoVehiculoExtDetPax = value
        End Set
    End Property

    Private _ListaViaticosTourConductor As List(Of clsCotizacionViaticosTCBE)
    Public Property ListaViaticosTourConductor() As List(Of clsCotizacionViaticosTCBE)
        Get
            Return _ListaViaticosTourConductor
        End Get
        Set(ByVal value As List(Of clsCotizacionViaticosTCBE))
            _ListaViaticosTourConductor = value
        End Set
    End Property

    Private _CopiaDetalleVarios As clsCopiaDetalleBE
    Public Property CopiaDetalleVarios() As clsCopiaDetalleBE
        Get
            Return _CopiaDetalleVarios
        End Get
        Set(ByVal value As clsCopiaDetalleBE)
            _CopiaDetalleVarios = value
        End Set
    End Property
End Class


