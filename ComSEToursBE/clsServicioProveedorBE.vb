﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsServicioProveedorBaseBE

    ''Inherits ServicedComponent
    'Inherits ServicedComponent

    Private _IDServicio As String
    Public Property IDServicio() As String
        Get
            Return _IDServicio
        End Get
        Set(ByVal value As String)
            _IDServicio = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property
    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsServicioProveedorBE
    'Inherits clsServicioProveedorBaseBE
    'Inherits ServicedComponent

    Private _IDServicio As String
    Public Property IDServicio() As String
        Get
            Return _IDServicio
        End Get
        Set(ByVal value As String)
            _IDServicio = value
        End Set
    End Property

    Private _IDTipoProv As String
    Public Property IDTipoProv() As String
        Get
            Return _IDTipoProv
        End Get
        Set(ByVal value As String)
            _IDTipoProv = value
        End Set
    End Property

    Private _IDTipoServ As String
    Public Property IDTipoServ() As String
        Get
            Return _IDTipoServ
        End Get
        Set(ByVal value As String)
            _IDTipoServ = value
        End Set
    End Property

    Private _Activo As Boolean
    Public Property Activo() As Boolean
        Get
            Return _Activo
        End Get
        Set(ByVal value As Boolean)
            _Activo = value
        End Set
    End Property

    Private _MotivoDesactivacion As String
    Public Property MotivoDesactivacion() As String
        Get
            Return _MotivoDesactivacion
        End Get
        Set(ByVal value As String)
            _MotivoDesactivacion = value
        End Set
    End Property

    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Private _Tarifario As Boolean
    Public Property Tarifario() As Boolean
        Get
            Return _Tarifario
        End Get
        Set(ByVal value As Boolean)
            _Tarifario = value
        End Set
    End Property

    Private _Descri_tarifario As String
    Public Property Descri_tarifario() As String
        Get
            Return _Descri_tarifario
        End Get
        Set(ByVal value As String)
            _Descri_tarifario = value
        End Set
    End Property

    Private _IDUbigeo As String
    Public Property IDUbigeo() As String
        Get
            Return _IDUbigeo
        End Get
        Set(ByVal value As String)
            _IDUbigeo = value
        End Set
    End Property

    Private _Telefono As String
    Public Property Telefono() As String
        Get
            Return _Telefono
        End Get
        Set(ByVal value As String)
            _Telefono = value
        End Set
    End Property

    Private _Celular As String
    Public Property Celular() As String
        Get
            Return _Celular
        End Get
        Set(ByVal value As String)
            _Celular = value
        End Set
    End Property

    Private _Comision As Decimal
    Public Property Comision() As Decimal
        Get
            Return _Comision
        End Get
        Set(ByVal value As Decimal)
            _Comision = value
        End Set
    End Property

    Private _UpdComision As Boolean
    Public Property UpdComision() As Boolean
        Get
            Return _UpdComision
        End Get
        Set(ByVal value As Boolean)
            _UpdComision = value
        End Set
    End Property

    Private _IDCat As String
    Public Property IDCat() As String
        Get
            Return _IDCat
        End Get
        Set(ByVal value As String)
            _IDCat = value
        End Set
    End Property

    Private _Categoria As Byte
    Public Property Categoria() As Byte
        Get
            Return _Categoria
        End Get
        Set(ByVal value As Byte)
            _Categoria = value
        End Set
    End Property

    Private _Capacidad As Int16
    Public Property Capacidad() As Int16
        Get
            Return _Capacidad
        End Get
        Set(ByVal value As Int16)
            _Capacidad = value
        End Set
    End Property

    Private _Observaciones As String
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

    Private _Lectura As String
    Public Property Lectura() As String
        Get
            Return _Lectura
        End Get
        Set(ByVal value As String)
            _Lectura = value
        End Set
    End Property

    Private _Transfer As Boolean
    Public Property Transfer() As Boolean
        Get
            Return _Transfer
        End Get
        Set(ByVal value As Boolean)
            _Transfer = value
        End Set
    End Property

    Private _TipoTransporte As Char
    Public Property TipoTransporte() As Char
        Get
            Return _TipoTransporte
        End Get
        Set(ByVal value As Char)
            _TipoTransporte = value
        End Set
    End Property

    Private _FecCaducLectura As Date
    Public Property FecCaducLectura() As Date
        Get
            Return _FecCaducLectura
        End Get
        Set(ByVal value As Date)
            _FecCaducLectura = value
        End Set
    End Property

    Private _AtencionLunes As Boolean
    Public Property AtencionLunes() As Boolean
        Get
            Return _AtencionLunes
        End Get
        Set(ByVal value As Boolean)
            _AtencionLunes = value
        End Set
    End Property

    Private _AtencionMartes As Boolean
    Public Property AtencionMartes() As Boolean
        Get
            Return _AtencionMartes
        End Get
        Set(ByVal value As Boolean)
            _AtencionMartes = value
        End Set
    End Property

    Private _AtencionMiercoles As Boolean
    Public Property AtencionMiercoles() As Boolean
        Get
            Return _AtencionMiercoles
        End Get
        Set(ByVal value As Boolean)
            _AtencionMiercoles = value
        End Set
    End Property

    Private _AtencionJueves As Boolean
    Public Property AtencionJueves() As Boolean
        Get
            Return _AtencionJueves
        End Get
        Set(ByVal value As Boolean)
            _AtencionJueves = value
        End Set
    End Property

    Private _AtencionViernes As Boolean
    Public Property AtencionViernes() As Boolean
        Get
            Return _AtencionViernes
        End Get
        Set(ByVal value As Boolean)
            _AtencionViernes = value
        End Set
    End Property

    Private _AtencionSabado As Boolean
    Public Property AtencionSabado() As Boolean
        Get
            Return _AtencionSabado
        End Get
        Set(ByVal value As Boolean)
            _AtencionSabado = value
        End Set
    End Property

    Private _AtencionDomingo As Boolean
    Public Property AtencionDomingo() As Boolean
        Get
            Return _AtencionDomingo
        End Get
        Set(ByVal value As Boolean)
            _AtencionDomingo = value
        End Set
    End Property

    Private _HoraDesde As DateTime
    Public Property HoraDesde() As DateTime
        Get
            Return _HoraDesde
        End Get
        Set(ByVal value As DateTime)
            _HoraDesde = value
        End Set
    End Property

    Private _HoraHasta As DateTime
    Public Property HoraHasta() As DateTime
        Get
            Return _HoraHasta
        End Get
        Set(ByVal value As DateTime)
            _HoraHasta = value
        End Set
    End Property


    Private _DesayunoHoraDesde As DateTime
    Public Property DesayunoHoraDesde() As DateTime
        Get
            Return _DesayunoHoraDesde
        End Get
        Set(ByVal value As DateTime)
            _DesayunoHoraDesde = value
        End Set
    End Property

    Private _DesayunoHoraHasta As DateTime
    Public Property DesayunoHoraHasta() As DateTime
        Get
            Return _DesayunoHoraHasta
        End Get
        Set(ByVal value As DateTime)
            _DesayunoHoraHasta = value
        End Set
    End Property

    Private _IDDesayuno As String
    Public Property IDDesayuno() As String
        Get
            Return _IDDesayuno
        End Get
        Set(ByVal value As String)
            _IDDesayuno = value
        End Set
    End Property

    Private _ObservacionesDesayuno As String
    Public Property ObservacionesDesayuno() As String
        Get
            Return _ObservacionesDesayuno
        End Get
        Set(ByVal value As String)
            _ObservacionesDesayuno = value
        End Set
    End Property

    Private _PreDesayunoHoraDesde As DateTime
    Public Property PreDesayunoHoraDesde() As DateTime
        Get
            Return _PreDesayunoHoraDesde
        End Get
        Set(ByVal value As DateTime)
            _PreDesayunoHoraDesde = value
        End Set
    End Property

    Private _PreDesayunoHoraHasta As DateTime
    Public Property PreDesayunoHoraHasta() As DateTime
        Get
            Return _PreDesayunoHoraHasta
        End Get
        Set(ByVal value As DateTime)
            _PreDesayunoHoraHasta = value
        End Set
    End Property

    Private _IDPreDesayuno As String
    Public Property IDPreDesayuno() As String
        Get
            Return _IDPreDesayuno
        End Get
        Set(ByVal value As String)
            _IDPreDesayuno = value
        End Set
    End Property

    Private _HoraCheckIn As DateTime
    Public Property HoraCheckIn() As DateTime
        Get
            Return _HoraCheckIn
        End Get
        Set(ByVal value As DateTime)
            _HoraCheckIn = value
        End Set
    End Property

    Private _HoraCheckOut As DateTime
    Public Property HoraCheckOut() As DateTime
        Get
            Return _HoraCheckOut
        End Get
        Set(ByVal value As DateTime)
            _HoraCheckOut = value
        End Set
    End Property

    'Private _TelefonoRecepcion1 As String
    'Public Property TelefonoRecepcion1() As String
    '    Get
    '        Return _TelefonoRecepcion1
    '    End Get
    '    Set(ByVal value As String)
    '        _TelefonoRecepcion1 = value
    '    End Set
    'End Property

    'Private _TelefonoRecepcion2 As String
    'Public Property TelefonoRecepcion2() As String
    '    Get
    '        Return _TelefonoRecepcion2
    '    End Get
    '    Set(ByVal value As String)
    '        _TelefonoRecepcion2 = value
    '    End Set
    'End Property

    'Private _FaxRecepcion1 As String
    'Public Property FaxRecepcion1() As String
    '    Get
    '        Return _FaxRecepcion1
    '    End Get
    '    Set(ByVal value As String)
    '        _FaxRecepcion1 = value
    '    End Set
    'End Property

    'Private _FaxRecepcion2 As String
    'Public Property FaxRecepcion2() As String
    '    Get
    '        Return _FaxRecepcion2
    '    End Get
    '    Set(ByVal value As String)
    '        _FaxRecepcion2 = value
    '    End Set
    'End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    'Private _IncluyeServiciosDias As Boolean
    'Public Property IncluyeServiciosDias() As Boolean
    '    Get
    '        Return _IncluyeServiciosDias
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _IncluyeServiciosDias = value
    '    End Set
    'End Property

    Private _Dias As Byte
    Public Property Dias() As Byte
        Get
            Return _Dias
        End Get
        Set(ByVal value As Byte)
            _Dias = value
        End Set
    End Property
    Private _PoliticaLiberado As Boolean
    Public Property PoliticaLiberado() As Boolean
        Get
            Return _PoliticaLiberado
        End Get
        Set(ByVal value As Boolean)
            _PoliticaLiberado = value
        End Set
    End Property

    Private _Liberado As Byte
    Public Property Liberado() As Byte
        Get
            Return _Liberado
        End Get
        Set(ByVal value As Byte)
            _Liberado = value
        End Set
    End Property

    Private _TipoLib As Char
    Public Property TipoLib() As Char
        Get
            Return _TipoLib
        End Get
        Set(ByVal value As Char)
            _TipoLib = value
        End Set
    End Property


    Private _MaximoLiberado As Byte
    Public Property MaximoLiberado() As Byte
        Get
            Return _MaximoLiberado
        End Get
        Set(ByVal value As Byte)
            _MaximoLiberado = value
        End Set
    End Property

    Private _MontoL As Double
    Public Property MontoL() As Double
        Get
            Return _MontoL
        End Get
        Set(ByVal value As Double)
            _MontoL = value
        End Set
    End Property

    'Private _Desayuno As Boolean
    'Public Property Desayuno() As Boolean
    '    Get
    '        Return _Desayuno
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Desayuno = value
    '    End Set
    'End Property

    'Private _Lonche As Boolean
    'Public Property Lonche() As Boolean
    '    Get
    '        Return _Lonche
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Lonche = value
    '    End Set
    'End Property


    'Private _Almuerzo As Boolean
    'Public Property Almuerzo() As Boolean
    '    Get
    '        Return _Almuerzo
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Almuerzo = value
    '    End Set
    'End Property

    'Private _Cena As Boolean
    'Public Property Cena() As Boolean
    '    Get
    '        Return _Cena
    '    End Get
    '    Set(ByVal value As Boolean)
    '        _Cena = value
    '    End Set
    'End Property

    Private _SinDescripcion As Boolean
    Public Property SinDescripcion() As Boolean
        Get
            Return _SinDescripcion
        End Get
        Set(ByVal value As Boolean)
            _SinDescripcion = value
        End Set
    End Property

    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

    Private _ServicioVarios As Boolean
    Public Property ServicioVarios() As Boolean
        Get
            Return _ServicioVarios
        End Get
        Set(ByVal value As Boolean)
            _ServicioVarios = value
        End Set
    End Property

    Private _IDCabVarios As Integer
    Public Property IDCabVarios() As Integer
        Get
            Return _IDCabVarios
        End Get
        Set(ByVal value As Integer)
            _IDCabVarios = value
        End Set
    End Property


    Private _CoTipoPago As String
    Public Property CoTipoPago() As String
        Get
            Return _CoTipoPago
        End Get
        Set(ByVal value As String)
            _CoTipoPago = value
        End Set
    End Property

    Private _APTServicoOpcional As Char
    Public Property APTServicoOpcional() As Char
        Get
            Return _APTServicoOpcional
        End Get
        Set(ByVal value As Char)
            _APTServicoOpcional = value
        End Set
    End Property

    Private _HoraPredeterminada As DateTime
    Public Property HoraPredeterminada() As DateTime
        Get
            Return _HoraPredeterminada
        End Get
        Set(ByVal value As DateTime)
            _HoraPredeterminada = value
        End Set
    End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDetalleServicioProveedorBE
        Inherits clsServicioProveedorBaseBE


        Private _IDServicio_Det As Int32
        Public Property IDServicio_Det() As Int32
            Get
                Return _IDServicio_Det
            End Get
            Set(ByVal value As Int32)
                _IDServicio_Det = value
            End Set
        End Property

        Private _IDServicio_Det_V As Int32
        Public Property IDServicio_Det_V() As Int32
            Get
                Return _IDServicio_Det_V
            End Get
            Set(ByVal value As Int32)
                _IDServicio_Det_V = value
            End Set
        End Property

        Private _Anio As String
        Public Property Anio() As String
            Get
                Return _Anio
            End Get
            Set(ByVal value As String)
                _Anio = value
            End Set
        End Property

        Private _Tipo As String
        Public Property Tipo() As String
            Get
                Return _Tipo
            End Get
            Set(ByVal value As String)
                _Tipo = value
            End Set
        End Property

        Private _DetaTipo As String
        Public Property DetaTipo() As String
            Get
                Return _DetaTipo
            End Get
            Set(ByVal value As String)
                _DetaTipo = value
            End Set
        End Property

        Private _VerDetaTipo As Boolean
        Public Property VerDetaTipo() As Boolean
            Get
                Return _VerDetaTipo
            End Get
            Set(ByVal value As Boolean)
                _VerDetaTipo = value
            End Set
        End Property

        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property

        Private _Descripcion As String
        Public Property Descripcion() As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property

        Private _Afecto As Boolean
        Public Property Afecto() As Boolean
            Get
                Return _Afecto
            End Get
            Set(ByVal value As Boolean)
                _Afecto = value
            End Set
        End Property

        'Private _IDMoneda As String
        'Public Property IDMoneda() As String
        '    Get
        '        Return _IDMoneda
        '    End Get
        '    Set(ByVal value As String)
        '        _IDMoneda = value
        '    End Set
        'End Property

        Private _Monto_sgl As Double
        Public Property Monto_Sgl() As Double
            Get
                Return _Monto_sgl
            End Get
            Set(ByVal value As Double)
                _Monto_sgl = value
            End Set
        End Property

        Private _Monto_dbl As Double
        Public Property Monto_dbl() As Double
            Get
                Return _Monto_dbl
            End Get
            Set(ByVal value As Double)
                _Monto_dbl = value
            End Set
        End Property

        Private _Monto_tri As Double
        Public Property Monto_tri() As Double
            Get
                Return _Monto_tri
            End Get
            Set(ByVal value As Double)
                _Monto_tri = value
            End Set
        End Property

        Private _Cod_x_Triple As String
        Public Property Cod_x_Triple() As String
            Get
                Return _Cod_x_Triple
            End Get
            Set(ByVal value As String)
                _Cod_x_Triple = value
            End Set
        End Property

        Private _IDServicio_DetxTriple As Integer
        Public Property IDServicio_DetxTriple() As Integer
            Get
                Return _IDServicio_DetxTriple
            End Get
            Set(ByVal value As Integer)
                _IDServicio_DetxTriple = value
            End Set
        End Property

        Private _Monto_sgls As Double
        Public Property Monto_Sgls() As Double
            Get
                Return _Monto_sgls
            End Get
            Set(ByVal value As Double)
                _Monto_sgls = value
            End Set
        End Property

        Private _Monto_dbls As Double
        Public Property Monto_dbls() As Double
            Get
                Return _Monto_dbls
            End Get
            Set(ByVal value As Double)
                _Monto_dbls = value
            End Set
        End Property

        Private _Monto_tris As Double
        Public Property Monto_tris() As Double
            Get
                Return _Monto_tris
            End Get
            Set(ByVal value As Double)
                _Monto_tris = value
            End Set
        End Property

        Private _TC As Single
        Public Property TC() As Single
            Get
                Return _TC
            End Get
            Set(ByVal value As Single)
                _TC = value
            End Set
        End Property

        Private _FlDetraccion As Boolean
        Public Property FlDetraccion() As Boolean
            Get
                Return _FlDetraccion
            End Get
            Set(ByVal value As Boolean)
                _FlDetraccion = value
            End Set
        End Property

        Private _ConAlojamiento As Boolean
        Public Property ConAlojamiento() As Boolean
            Get
                Return _ConAlojamiento
            End Get
            Set(ByVal value As Boolean)
                _ConAlojamiento = value
            End Set
        End Property

        Private _PlanAlimenticio As Boolean
        Public Property PlanAlimenticio() As Boolean
            Get
                Return _PlanAlimenticio
            End Get
            Set(ByVal value As Boolean)
                _PlanAlimenticio = value
            End Set
        End Property


        Private _CodTarifa As String
        Public Property CodTarifa() As String
            Get
                Return _CodTarifa
            End Get
            Set(ByVal value As String)
                _CodTarifa = value
            End Set
        End Property


        Private _DiferSS As Double
        Public Property DiferSS() As Double
            Get
                Return _DiferSS
            End Get
            Set(ByVal value As Double)
                _DiferSS = value
            End Set
        End Property

        Private _DiferST As Double
        Public Property DiferST() As Double
            Get
                Return _DiferST
            End Get
            Set(ByVal value As Double)
                _DiferST = value
            End Set
        End Property


        Private _TipoGasto As Char
        Public Property TipoGasto() As Char
            Get
                Return _TipoGasto
            End Get
            Set(ByVal value As Char)
                _TipoGasto = value
            End Set
        End Property

        Private _TipoDesayuno As String
        Public Property TipoDesayuno() As String
            Get
                Return _TipoDesayuno
            End Get
            Set(ByVal value As String)
                _TipoDesayuno = value
            End Set
        End Property

        Private _Desayuno As Boolean
        Public Property Desayuno() As Boolean
            Get
                Return _Desayuno
            End Get
            Set(ByVal value As Boolean)
                _Desayuno = value
            End Set
        End Property
        Private _Lonche As Boolean
        Public Property Lonche() As Boolean
            Get
                Return _Lonche
            End Get
            Set(ByVal value As Boolean)
                _Lonche = value
            End Set
        End Property

        Private _Almuerzo As Boolean
        Public Property Almuerzo() As Boolean
            Get
                Return _Almuerzo
            End Get
            Set(ByVal value As Boolean)
                _Almuerzo = value
            End Set
        End Property

        Private _Cena As Boolean
        Public Property Cena() As Boolean
            Get
                Return _Cena
            End Get
            Set(ByVal value As Boolean)
                _Cena = value
            End Set
        End Property


        Private _PoliticaLiberado As Boolean
        Public Property PoliticaLiberado() As Boolean
            Get
                Return _PoliticaLiberado
            End Get
            Set(ByVal value As Boolean)
                _PoliticaLiberado = value
            End Set
        End Property

        Private _Liberado As Byte
        Public Property Liberado() As Byte
            Get
                Return _Liberado
            End Get
            Set(ByVal value As Byte)
                _Liberado = value
            End Set
        End Property

        Private _TipoLib As Char
        Public Property TipoLib() As Char
            Get
                Return _TipoLib
            End Get
            Set(ByVal value As Char)
                _TipoLib = value
            End Set
        End Property


        Private _MaximoLiberado As Byte
        Public Property MaximoLiberado() As Byte
            Get
                Return _MaximoLiberado
            End Get
            Set(ByVal value As Byte)
                _MaximoLiberado = value
            End Set
        End Property

        Private _LiberadoM As Byte
        Public Property LiberadoM() As Byte
            Get
                Return _LiberadoM
            End Get
            Set(ByVal value As Byte)
                _LiberadoM = value
            End Set
        End Property

        Private _MontoL As Double
        Public Property MontoL() As Double
            Get
                Return _MontoL
            End Get
            Set(ByVal value As Double)
                _MontoL = value
            End Set
        End Property

        Private _PoliticaLibNivelDetalle As Boolean
        Public Property PoliticaLibNivelDetalle() As Boolean
            Get
                Return _PoliticaLibNivelDetalle
            End Get
            Set(ByVal value As Boolean)
                _PoliticaLibNivelDetalle = value
            End Set
        End Property

        Private _Tarifario As Boolean
        Public Property Tarifario() As Boolean
            Get
                Return _Tarifario
            End Get
            Set(ByVal value As Boolean)
                _Tarifario = value
            End Set
        End Property

        Private _TituloGrupo As String
        Public Property TituloGrupo() As String
            Get
                Return _TituloGrupo
            End Get
            Set(ByVal value As String)
                _TituloGrupo = value
            End Set
        End Property

        Private _IDTipoOC As String
        Public Property IDTipoOC() As String
            Get
                Return _IDTipoOC
            End Get
            Set(ByVal value As String)
                _IDTipoOC = value
            End Set
        End Property

        Private _CoCeCos As String
        Public Property CoCeCos() As String
            Get
                Return _CoCeCos
            End Get
            Set(ByVal value As String)
                _CoCeCos = value
            End Set
        End Property

        Private _CtaContable As String
        Public Property CtaContable() As String
            Get
                Return _CtaContable
            End Get
            Set(ByVal value As String)
                _CtaContable = value
            End Set
        End Property

        Private _CtaContableC As String
        Public Property CtaContableC() As String
            Get
                Return _CtaContableC
            End Get
            Set(ByVal value As String)
                _CtaContableC = value
            End Set
        End Property

        Private _IdHabitTriple As String
        Public Property IdHabitTriple() As String
            Get
                Return _IdHabitTriple
            End Get
            Set(ByVal value As String)
                _IdHabitTriple = value
            End Set
        End Property

        Private _dActivo As String
        Public Property dActivo() As String
            Get
                Return _dActivo
            End Get
            Set(ByVal value As String)
                _dActivo = value
            End Set
        End Property

        Private _blnCambioCostos As Boolean
        Public Property CambioCostos() As Boolean
            Get
                Return _blnCambioCostos
            End Get
            Set(ByVal value As Boolean)
                _blnCambioCostos = value
            End Set
        End Property

        Private _CoMoneda As String
        Public Property CoMoneda() As String
            Get
                Return _CoMoneda
            End Get
            Set(ByVal value As String)
                _CoMoneda = value
            End Set
        End Property

        'Private _AnioPreciosCopia As String
        'Public Property AnioPreciosCopia() As String
        '    Get
        '        Return _AnioPreciosCopia
        '    End Get
        '    Set(ByVal value As String)
        '        _AnioPreciosCopia = value
        '    End Set
        'End Property
        <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
        Public Class clsCostosDetalleRangoPax
            'Inherits ServicedComponent

            Private _IDServicio_Det As Integer
            Public Property IDServicio_Det() As Integer
                Get
                    Return _IDServicio_Det
                End Get
                Set(ByVal value As Integer)
                    _IDServicio_Det = value
                End Set
            End Property

            Private _NroPax As Int16
            Public Property NroPax() As Int16
                Get
                    Return _NroPax
                End Get
                Set(ByVal value As Int16)
                    _NroPax = value
                End Set
            End Property

            Private _NroLiberados As Int16
            Public Property NroLiberados() As Int16
                Get
                    Return _NroLiberados
                End Get
                Set(ByVal value As Int16)
                    _NroLiberados = value
                End Set
            End Property

            Private _IDServicio As String
            Public Property IDServicio() As String
                Get
                    Return _IDServicio
                End Get
                Set(ByVal value As String)
                    _IDServicio = value
                End Set
            End Property

            Private _Anio As String
            Public Property Anio() As String
                Get
                    Return _Anio
                End Get
                Set(ByVal value As String)
                    _Anio = value
                End Set
            End Property

            Private _Variante As String
            Public Property Variante() As String
                Get
                    Return _Variante
                End Get
                Set(ByVal value As String)
                    _Variante = value
                End Set
            End Property

            Private _DescServicio As String
            Public Property DescServicio() As String
                Get
                    Return _DescServicio
                End Get
                Set(ByVal value As String)
                    _DescServicio = value
                End Set
            End Property

            Private _Existe As Boolean
            Public Property Existe() As Boolean
                Get
                    Return _Existe
                End Get
                Set(ByVal value As Boolean)
                    _Existe = value
                End Set
            End Property

        End Class

        Private _ListaRangoCostosPax As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax)
        Public Property ListaRangoCostosPax() As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax)
            Get
                Return _ListaRangoCostosPax
            End Get
            Set(ByVal value As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleRangoPax))
                _ListaRangoCostosPax = value
            End Set
        End Property

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsCostosDetalleServicioProveedorBE
            'Inherits ServicedComponent
            'Private _IDServicio_Det As Int32
            'Public Property IDServicio_Det() As Int32
            '    Get
            '        Return _IDServicio_Det
            '    End Get
            '    Set(ByVal value As Int32)
            '        _IDServicio_Det = value
            '    End Set
            'End Property

            Private _Correlativo As Int32
            Public Property Correlativo() As Int32
                Get
                    Return _Correlativo
                End Get
                Set(ByVal value As Int32)
                    _Correlativo = value
                End Set
            End Property

            Private _PaxDesde As Int16
            Public Property PaxDesde() As Int16
                Get
                    Return _PaxDesde
                End Get
                Set(ByVal value As Int16)
                    _PaxDesde = value
                End Set
            End Property

            Private _PaxHasta As Int16
            Public Property PaxHasta() As Int16
                Get
                    Return _PaxHasta
                End Get
                Set(ByVal value As Int16)
                    _PaxHasta = value
                End Set
            End Property

            Private _Monto As Double
            Public Property Monto() As Double
                Get
                    Return _Monto
                End Get
                Set(ByVal value As Double)
                    _Monto = value
                End Set
            End Property

            Private _UserMod As String
            Public Property UserMod() As String
                Get
                    Return _UserMod
                End Get
                Set(ByVal value As String)
                    _UserMod = value
                End Set
            End Property

            Private _Accion As Char
            Public Property Accion() As Char
                Get
                    Return _Accion
                End Get
                Set(ByVal value As Char)
                    _Accion = value
                End Set
            End Property

        End Class

        Private _ListaCostos As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE)
        Public Property ListaCostos() As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE)
            Get
                Return _ListaCostos
            End Get
            Set(ByVal value As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE))
                _ListaCostos = value
            End Set
        End Property

        '----------------------

        <ComVisible(True)> _
       <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDescRangosAPTDetalleServicioProveedorBE
            'Inherits ServicedComponent
            'Private _IDServicio_Det As Int32
            'Public Property IDServicio_Det() As Int32
            '    Get
            '        Return _IDServicio_Det
            '    End Get
            '    Set(ByVal value As Int32)
            '        _IDServicio_Det = value
            '    End Set
            'End Property

            Private _IDTemporada As Integer
            Public Property IDTemporada() As Integer
                Get
                    Return _IDTemporada
                End Get
                Set(ByVal value As Integer)
                    _IDTemporada = value
                End Set
            End Property

            Private _DescripcionAlterna As String
            Public Property DescripcionAlterna() As String
                Get
                    Return _DescripcionAlterna
                End Get
                Set(ByVal value As String)
                    _DescripcionAlterna = value
                End Set
            End Property

            Private _SingleSupplement As Double
            Public Property SingleSupplement() As Double
                Get
                    Return _SingleSupplement
                End Get
                Set(ByVal value As Double)
                    _SingleSupplement = value
                End Set
            End Property

            Private _PoliticaLiberado As Boolean
            Public Property PoliticaLiberado() As Boolean
                Get
                    Return _PoliticaLiberado
                End Get
                Set(ByVal value As Boolean)
                    _PoliticaLiberado = value
                End Set
            End Property

            Private _Liberado As Byte
            Public Property Liberado() As Byte
                Get
                    Return _Liberado
                End Get
                Set(ByVal value As Byte)
                    _Liberado = value
                End Set
            End Property

            Private _TipoLib As String
            Public Property TipoLib() As String
                Get
                    Return _TipoLib
                End Get
                Set(ByVal value As String)
                    _TipoLib = value
                End Set
            End Property

            Private _MaximoLiberado As Byte
            Public Property MaximoLiberado() As Byte
                Get
                    Return _MaximoLiberado
                End Get
                Set(ByVal value As Byte)
                    _MaximoLiberado = value
                End Set
            End Property

            Private _MontoL As Double
            Public Property MontoL() As Double
                Get
                    Return _MontoL
                End Get
                Set(ByVal value As Double)
                    _MontoL = value
                End Set
            End Property

            Private _UserMod As String
            Public Property UserMod() As String
                Get
                    Return _UserMod
                End Get
                Set(ByVal value As String)
                    _UserMod = value
                End Set
            End Property
        End Class

        Private _ListaDescRangos As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE)
        Public Property ListaDescRangos() As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE)
            Get
                Return _ListaDescRangos
            End Get
            Set(ByVal value As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE))
                _ListaDescRangos = value
            End Set
        End Property

        '----------------------

        <ComVisible(True)> _
       <Transaction(TransactionOption.NotSupported)> _
        Public Class clsRangosAPTDetalleServicioProveedorBE
            'Inherits ServicedComponent
            'Private _IDServicio_Det As Int32
            'Public Property IDServicio_Det() As Int32
            '    Get
            '        Return _IDServicio_Det
            '    End Get
            '    Set(ByVal value As Int32)
            '        _IDServicio_Det = value
            '    End Set
            'End Property

            Private _IDTemporada As Integer
            Public Property IDTemporada() As Integer
                Get
                    Return _IDTemporada
                End Get
                Set(ByVal value As Integer)
                    _IDTemporada = value
                End Set
            End Property

            Private _Correlativo As Int32
            Public Property Correlativo() As Int32
                Get
                    Return _Correlativo
                End Get
                Set(ByVal value As Int32)
                    _Correlativo = value
                End Set
            End Property

            Private _PaxDesde As Int16
            Public Property PaxDesde() As Int16
                Get
                    Return _PaxDesde
                End Get
                Set(ByVal value As Int16)
                    _PaxDesde = value
                End Set
            End Property

            Private _PaxHasta As Int16
            Public Property PaxHasta() As Int16
                Get
                    Return _PaxHasta
                End Get
                Set(ByVal value As Int16)
                    _PaxHasta = value
                End Set
            End Property

            Private _Monto As Double
            Public Property Monto() As Double
                Get
                    Return _Monto
                End Get
                Set(ByVal value As Double)
                    _Monto = value
                End Set
            End Property

            Private _UserMod As String
            Public Property UserMod() As String
                Get
                    Return _UserMod
                End Get
                Set(ByVal value As String)
                    _UserMod = value
                End Set
            End Property

            Private _Accion As Char
            Public Property Accion() As Char
                Get
                    Return _Accion
                End Get
                Set(ByVal value As Char)
                    _Accion = value
                End Set
            End Property

        End Class

        Private _ListaRangos As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE)
        Public Property ListaRangos() As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE)
            Get
                Return _ListaRangos
            End Get
            Set(ByVal value As List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE))
                _ListaRangos = value
            End Set
        End Property


        '-----------------------

    End Class

    '----------------------

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDescRangosAPTServicioProveedorBE
        Private _IDTemporada As Integer
        Public Property IDTemporada() As Integer
            Get
                Return _IDTemporada
            End Get
            Set(ByVal value As Integer)
                _IDTemporada = value
            End Set
        End Property

        Private _DescripcionAlterna As String
        Public Property DescripcionAlterna() As String
            Get
                Return _DescripcionAlterna
            End Get
            Set(ByVal value As String)
                _DescripcionAlterna = value
            End Set
        End Property

        Private _AgruparServicio As Boolean
        Public Property AgruparServicio() As Boolean
            Get
                Return _AgruparServicio
            End Get
            Set(ByVal value As Boolean)
                _AgruparServicio = value
            End Set
        End Property

        Private _PoliticaLiberado As Boolean
        Public Property PoliticaLiberado() As Boolean
            Get
                Return _PoliticaLiberado
            End Get
            Set(ByVal value As Boolean)
                _PoliticaLiberado = value
            End Set
        End Property

        Private _Liberado As Byte
        Public Property Liberado() As Byte
            Get
                Return _Liberado
            End Get
            Set(ByVal value As Byte)
                _Liberado = value
            End Set
        End Property

        Private _TipoLib As String
        Public Property TipoLib() As String
            Get
                Return _TipoLib
            End Get
            Set(ByVal value As String)
                _TipoLib = value
            End Set
        End Property

        Private _MaximoLiberado As Byte
        Public Property MaximoLiberado() As Byte
            Get
                Return _MaximoLiberado
            End Get
            Set(ByVal value As Byte)
                _MaximoLiberado = value
            End Set
        End Property

        Private _MontoL As Double
        Public Property MontoL() As Double
            Get
                Return _MontoL
            End Get
            Set(ByVal value As Double)
                _MontoL = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property
    End Class

    Private _ListaDescRangosAPT As List(Of clsServicioProveedorBE.clsDescRangosAPTServicioProveedorBE)
    Public Property ListaDescRangosAPT() As List(Of clsServicioProveedorBE.clsDescRangosAPTServicioProveedorBE)
        Get
            Return _ListaDescRangosAPT
        End Get
        Set(ByVal value As List(Of clsServicioProveedorBE.clsDescRangosAPTServicioProveedorBE))
            _ListaDescRangosAPT = value
        End Set
    End Property

    '----------------------

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsRangosAPTServicioProveedorBE
        Private _IDTemporada As Integer
        Public Property IDTemporada() As Integer
            Get
                Return _IDTemporada
            End Get
            Set(ByVal value As Integer)
                _IDTemporada = value
            End Set
        End Property

        Private _Correlativo As Int32
        Public Property Correlativo() As Int32
            Get
                Return _Correlativo
            End Get
            Set(ByVal value As Int32)
                _Correlativo = value
            End Set
        End Property

        Private _PaxDesde As Int16
        Public Property PaxDesde() As Int16
            Get
                Return _PaxDesde
            End Get
            Set(ByVal value As Int16)
                _PaxDesde = value
            End Set
        End Property

        Private _PaxHasta As Int16
        Public Property PaxHasta() As Int16
            Get
                Return _PaxHasta
            End Get
            Set(ByVal value As Int16)
                _PaxHasta = value
            End Set
        End Property

        Private _Monto As Double
        Public Property Monto() As Double
            Get
                Return _Monto
            End Get
            Set(ByVal value As Double)
                _Monto = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaRangosAPT As List(Of clsServicioProveedorBE.clsRangosAPTServicioProveedorBE)
    Public Property ListaRangosAPT() As List(Of clsServicioProveedorBE.clsRangosAPTServicioProveedorBE)
        Get
            Return _ListaRangosAPT
        End Get
        Set(ByVal value As List(Of clsServicioProveedorBE.clsRangosAPTServicioProveedorBE))
            _ListaRangosAPT = value
        End Set
    End Property


    '-----------------------

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTextoServicioProveedorBE
        'Inherits clsServicioProveedorBaseBE
        'Inherits ServicedComponent
        Private _IDIdioma As String
        Public Property IDIDioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property

        Private _Titulo As String
        Public Property Titulo() As String
            Get
                Return _Titulo
            End Get
            Set(ByVal value As String)
                _Titulo = value
            End Set
        End Property

        Private _Texto As String
        Public Property Texto() As String
            Get
                Return _Texto
            End Get
            Set(ByVal value As String)
                _Texto = value
            End Set
        End Property



        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property


    End Class

    Private _ListTextos As List(Of clsServicioProveedorBE.clsTextoServicioProveedorBE)
    Public Property ListTextos() As List(Of clsServicioProveedorBE.clsTextoServicioProveedorBE)
        Get
            Return _ListTextos
        End Get
        Set(ByVal value As List(Of clsServicioProveedorBE.clsTextoServicioProveedorBE))
            _ListTextos = value
        End Set
    End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsServiciosAtencionBE
        'Inherits ServicedComponent
        Private _IDAtencion As Integer
        Public Property IDAtencion() As Integer
            Get
                Return _IDAtencion
            End Get
            Set(ByVal value As Integer)
                _IDAtencion = value
            End Set
        End Property

        Private _IDServicio As String
        Public Property IDServicio() As String
            Get
                Return _IDServicio
            End Get
            Set(ByVal value As String)
                _IDServicio = value
            End Set
        End Property

        Private _Dia1 As Char
        Public Property Dia1() As Char
            Get
                Return _Dia1
            End Get
            Set(ByVal value As Char)
                _Dia1 = value
            End Set
        End Property

        Private _Dia2 As Char
        Public Property Dia2() As Char
            Get
                Return _Dia2
            End Get
            Set(ByVal value As Char)
                _Dia2 = value
            End Set
        End Property

        Private _Desde1 As String
        Public Property Desde1() As String
            Get
                Return _Desde1
            End Get
            Set(ByVal value As String)
                _Desde1 = value
            End Set
        End Property

        Private _Hasta1 As String
        Public Property Hasta1() As String
            Get
                Return _Hasta1
            End Get
            Set(ByVal value As String)
                _Hasta1 = value
            End Set
        End Property

        Private _Desde2 As String
        Public Property Desde2() As String
            Get
                Return _Desde2
            End Get
            Set(ByVal value As String)
                _Desde2 = value
            End Set
        End Property

        Private _Hasta2 As String
        Public Property Hasta2() As String
            Get
                Return _Hasta2
            End Get
            Set(ByVal value As String)
                _Hasta2 = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaServAtencion As List(Of clsServiciosAtencionBE)
    Public Property ListaServAtencion() As List(Of clsServiciosAtencionBE)
        Get
            Return _ListaServAtencion
        End Get
        Set(ByVal value As List(Of clsServiciosAtencionBE))
            _ListaServAtencion = value
        End Set
    End Property


    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsServiciosporDiaBE
        'Inherits ServicedComponent
        Private _Dia As Byte
        Public Property Dia() As Byte
            Get
                Return _Dia
            End Get
            Set(ByVal value As Byte)
                _Dia = value
            End Set
        End Property

        Private _IDIdioma As String
        Public Property IDIdioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property

        Private _Descripcion As String
        Public Property Descripcion() As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _NoHotel As String
        Public Property NoHotel() As String
            Get
                Return _NoHotel
            End Get
            Set(ByVal value As String)
                _NoHotel = value
            End Set
        End Property

        Private _TxWebHotel As String
        Public Property TxWebHotel() As String
            Get
                Return _TxWebHotel
            End Get
            Set(ByVal value As String)
                _TxWebHotel = value
            End Set
        End Property

        Private _TxDireccHotel As String
        Public Property TxDireccHotel() As String
            Get
                Return _TxDireccHotel
            End Get
            Set(ByVal value As String)
                _TxDireccHotel = value
            End Set
        End Property

        Private _TxTelfHotel1 As String
        Public Property TxTelfHotel1() As String
            Get
                Return _TxTelfHotel1
            End Get
            Set(ByVal value As String)
                _TxTelfHotel1 = value
            End Set
        End Property

        Private _TxTelfHotel2 As String
        Public Property TxTelfHotel2() As String
            Get
                Return _TxTelfHotel2
            End Get
            Set(ByVal value As String)
                _TxTelfHotel2 = value
            End Set
        End Property

        Private _FeHoraChkInHotel As DateTime
        Public Property FeHoraChkInHotel() As DateTime
            Get
                Return _FeHoraChkInHotel
            End Get
            Set(ByVal value As DateTime)
                _FeHoraChkInHotel = value
            End Set
        End Property

        Private _FeHoraChkOutHotel As DateTime
        Public Property FeHoraChkOutHotel() As DateTime
            Get
                Return _FeHoraChkOutHotel
            End Get
            Set(ByVal value As DateTime)
                _FeHoraChkOutHotel = value
            End Set
        End Property

        Private _CoTipoDesaHotel As String
        Public Property CoTipoDesaHotel() As String
            Get
                Return _CoTipoDesaHotel
            End Get
            Set(ByVal value As String)
                _CoTipoDesaHotel = value
            End Set
        End Property

        Private _CoCiudadHotel As String
        Public Property CoCiudadHotel() As String
            Get
                Return _CoCiudadHotel
            End Get
            Set(ByVal value As String)
                _CoCiudadHotel = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class


    Private _ListServxDia As List(Of clsServiciosporDiaBE)
    Public Property ListServxDia() As List(Of clsServiciosporDiaBE)
        Get
            Return _ListServxDia
        End Get
        Set(ByVal value As List(Of clsServiciosporDiaBE))
            _ListServxDia = value
        End Set
    End Property

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsServiciosAlimentacionporDiaBE
        'Inherits ServicedComponent
        Private _Dia As Byte
        Public Property Dia() As Byte
            Get
                Return _Dia
            End Get
            Set(ByVal value As Byte)
                _Dia = value
            End Set
        End Property

        Private _Desayuno As Boolean
        Public Property Desayuno() As Boolean
            Get
                Return _Desayuno
            End Get
            Set(ByVal value As Boolean)
                _Desayuno = value
            End Set
        End Property
        Private _Lonche As Boolean
        Public Property Lonche() As Boolean
            Get
                Return _Lonche
            End Get
            Set(ByVal value As Boolean)
                _Lonche = value
            End Set
        End Property

        Private _Almuerzo As Boolean
        Public Property Almuerzo() As Boolean
            Get
                Return _Almuerzo
            End Get
            Set(ByVal value As Boolean)
                _Almuerzo = value
            End Set
        End Property

        Private _Cena As Boolean
        Public Property Cena() As Boolean
            Get
                Return _Cena
            End Get
            Set(ByVal value As Boolean)
                _Cena = value
            End Set
        End Property

        Private _IDUbigeoOri As String
        Public Property IDUbigeoOri() As String
            Get
                Return _IDUbigeoOri
            End Get
            Set(ByVal value As String)
                _IDUbigeoOri = value
            End Set
        End Property

        Private _IDUbigeoDes As String
        Public Property IDUbigeoDes() As String
            Get
                Return _IDUbigeoDes
            End Get
            Set(ByVal value As String)
                _IDUbigeoDes = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property
        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListServAlimxDia As List(Of clsServiciosAlimentacionporDiaBE)
    Public Property ListServAlimxDia() As List(Of clsServiciosAlimentacionporDiaBE)
        Get
            Return _ListServAlimxDia
        End Get
        Set(ByVal value As List(Of clsServiciosAlimentacionporDiaBE))
            _ListServAlimxDia = value
        End Set
    End Property


    Public Class clsServicios_HotelBE
        Dim _CoServicio As String
        Dim _CoConcepto As Byte
        Dim _FlServicio As Boolean
        Dim _FlCosto As Boolean
        Dim _CoTipoMenu As String
        Dim _FlFijoPortable As Boolean
        Dim _FlPortable As Boolean
        Dim _CoTipoBanio As String
        Dim _CoEstadoHab As String
        Dim _TxPisos As String
        Dim _FlActivo As Boolean
        Dim _UserMod As String



        Public Property CoServicio() As String
            Get
                Return _CoServicio
            End Get
            Set(ByVal Value As String)
                _CoServicio = Value
            End Set
        End Property
        Public Property CoConcepto() As Byte
            Get
                Return _CoConcepto
            End Get
            Set(ByVal Value As Byte)
                _CoConcepto = Value
            End Set
        End Property
        Public Property FlServicio() As Boolean
            Get
                Return _FlServicio
            End Get
            Set(ByVal Value As Boolean)
                _FlServicio = Value
            End Set
        End Property
        Public Property FlCosto() As Boolean
            Get
                Return _FlCosto
            End Get
            Set(ByVal Value As Boolean)
                _FlCosto = Value
            End Set
        End Property
        Public Property CoTipoMenu() As String
            Get
                Return _CoTipoMenu
            End Get
            Set(ByVal Value As String)
                _CoTipoMenu = Value
            End Set
        End Property
        Public Property FlFijoPortable() As Boolean
            Get
                Return _FlFijoPortable
            End Get
            Set(ByVal Value As Boolean)
                _FlFijoPortable = Value
            End Set
        End Property
        Public Property FlPortable() As Boolean
            Get
                Return _FlPortable
            End Get
            Set(ByVal Value As Boolean)
                _FlPortable = Value
            End Set
        End Property
        Public Property CoTipoBanio() As String
            Get
                Return _CoTipoBanio
            End Get
            Set(ByVal Value As String)
                _CoTipoBanio = Value
            End Set
        End Property
        Public Property CoEstadoHab() As String
            Get
                Return _CoEstadoHab
            End Get
            Set(ByVal Value As String)
                _CoEstadoHab = Value
            End Set
        End Property
        Public Property TxPisos() As String
            Get
                Return _TxPisos
            End Get
            Set(ByVal Value As String)
                _TxPisos = Value
            End Set
        End Property
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal Value As String)
                _UserMod = Value
            End Set
        End Property

        Private _IngresoManual As Boolean
        Public Property IngresoManual() As Boolean
            Get
                Return _IngresoManual
            End Get
            Set(ByVal value As Boolean)
                _IngresoManual = value
            End Set
        End Property


        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property
    End Class

    Private _ListaServicios_Hoteles As List(Of clsServicios_HotelBE)
    Public Property ListaServicios_Hoteles() As List(Of clsServicios_HotelBE)
        Get
            Return _ListaServicios_Hoteles
        End Get
        Set(ByVal value As List(Of clsServicios_HotelBE))
            _ListaServicios_Hoteles = value
        End Set
    End Property


    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsServicios_Operaciones_HotelesBE
        Dim _CoServicio As String
        Dim _FlRecepcion As Boolean
        Dim _FlEstacionamiento As Boolean
        Dim _FlBoxDesayuno As Boolean
        Dim _FlRestaurantes As Boolean
        Dim _FlRestaurantesSoloDesayuno As Boolean
        Dim _QtRestCantidad As Byte
        Dim _QtRestCapacidad As Byte
        Dim _QtTriples As Byte
        Dim _QtNumHabitaciones As Byte
        Dim _CoTipoCama As String
        Dim _TxObservacion As String
        Dim _FlActivo As Boolean
        Dim _UserMod As String

        Public Property CoServicio() As String
            Get
                Return _CoServicio
            End Get
            Set(ByVal Value As String)
                _CoServicio = Value
            End Set
        End Property
        Public Property FlRecepcion() As Boolean
            Get
                Return _FlRecepcion
            End Get
            Set(ByVal Value As Boolean)
                _FlRecepcion = Value
            End Set
        End Property
        Public Property FlEstacionamiento() As Boolean
            Get
                Return _FlEstacionamiento
            End Get
            Set(ByVal Value As Boolean)
                _FlEstacionamiento = Value
            End Set
        End Property
        Public Property FlBoxDesayuno() As Boolean
            Get
                Return _FlBoxDesayuno
            End Get
            Set(ByVal Value As Boolean)
                _FlBoxDesayuno = Value
            End Set
        End Property
        Public Property FlRestaurantes() As Boolean
            Get
                Return _FlRestaurantes
            End Get
            Set(ByVal Value As Boolean)
                _FlRestaurantes = Value
            End Set
        End Property
        Public Property FlRestaurantesSoloDesayuno() As Boolean
            Get
                Return _FlRestaurantesSoloDesayuno
            End Get
            Set(ByVal Value As Boolean)
                _FlRestaurantesSoloDesayuno = Value
            End Set
        End Property
        Public Property QtRestCantidad() As Byte
            Get
                Return _QtRestCantidad
            End Get
            Set(ByVal Value As Byte)
                _QtRestCantidad = Value
            End Set
        End Property
        Public Property QtRestCapacidad() As Byte
            Get
                Return _QtRestCapacidad
            End Get
            Set(ByVal Value As Byte)
                _QtRestCapacidad = Value
            End Set
        End Property
        Public Property QtTriples() As Byte
            Get
                Return _QtTriples
            End Get
            Set(ByVal Value As Byte)
                _QtTriples = Value
            End Set
        End Property
        Public Property QtNumHabitaciones() As Byte
            Get
                Return _QtNumHabitaciones
            End Get
            Set(ByVal Value As Byte)
                _QtNumHabitaciones = Value
            End Set
        End Property
        Public Property CoTipoCama() As String
            Get
                Return _CoTipoCama
            End Get
            Set(ByVal Value As String)
                _CoTipoCama = Value
            End Set
        End Property
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property
        Public Property TxObservacion() As String
            Get
                Return _TxObservacion
            End Get
            Set(ByVal Value As String)
                _TxObservacion = Value
            End Set
        End Property
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal Value As String)
                _UserMod = Value
            End Set
        End Property

        Private _IngresoManual As Boolean
        Public Property IngresoManual() As Boolean
            Get
                Return _IngresoManual
            End Get
            Set(ByVal value As Boolean)
                _IngresoManual = value
            End Set
        End Property


        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ObjServicios_Operaciones_Hoteles As clsServicios_Operaciones_HotelesBE
    Public Property ObjServicios_Operaciones_Hoteles() As clsServicios_Operaciones_HotelesBE
        Get
            Return _ObjServicios_Operaciones_Hoteles
        End Get
        Set(ByVal value As clsServicios_Operaciones_HotelesBE)
            _ObjServicios_Operaciones_Hoteles = value
        End Set
    End Property

    '------------------------------------------

    Public Class clsServicios_Hotel_HabitacionesBE
        Dim _CoServicio As String
        Dim _NuHabitacion As Byte
        Dim _CoTipoHab As Byte
        Dim _QtCantidad As Byte
        Dim _QtCantMatri As Byte
        Dim _CoTipCama_Matri As Byte
        Dim _QtCantTwin As Byte
        Dim _CoTipCama_Twin As Byte
        Dim _CoTipCama_Adic As Byte
        Dim _QtCantTriple As Byte
        Dim _CoTipCama_Triple As Byte
        Dim _FlActivo As Boolean
        Dim _UserMod As String
        Dim _CoTipoHab_Oper As String


        Public Property CoServicio() As String
            Get
                Return _CoServicio
            End Get
            Set(ByVal Value As String)
                _CoServicio = Value
            End Set
        End Property
        Public Property NuHabitacion() As Byte
            Get
                Return _NuHabitacion
            End Get
            Set(ByVal Value As Byte)
                _NuHabitacion = Value
            End Set
        End Property
        Public Property CoTipoHab() As Byte
            Get
                Return _CoTipoHab
            End Get
            Set(ByVal Value As Byte)
                _CoTipoHab = Value
            End Set
        End Property
        Public Property QtCantidad() As Byte
            Get
                Return _QtCantidad
            End Get
            Set(ByVal Value As Byte)
                _QtCantidad = Value
            End Set
        End Property
        Public Property QtCantMatri() As Byte
            Get
                Return _QtCantMatri
            End Get
            Set(ByVal Value As Byte)
                _QtCantMatri = Value
            End Set
        End Property
        Public Property CoTipCama_Matri() As Byte
            Get
                Return _CoTipCama_Matri
            End Get
            Set(ByVal Value As Byte)
                _CoTipCama_Matri = Value
            End Set
        End Property
        Public Property QtCantTwin() As Byte
            Get
                Return _QtCantTwin
            End Get
            Set(ByVal Value As Byte)
                _QtCantTwin = Value
            End Set
        End Property
        Public Property CoTipCama_Twin() As Byte
            Get
                Return _CoTipCama_Twin
            End Get
            Set(ByVal Value As Byte)
                _CoTipCama_Twin = Value
            End Set
        End Property
        Public Property CoTipCama_Adic() As Byte
            Get
                Return _CoTipCama_Adic
            End Get
            Set(ByVal Value As Byte)
                _CoTipCama_Adic = Value
            End Set
        End Property

        Public Property QtCantTriple() As Byte
            Get
                Return _QtCantTriple
            End Get
            Set(ByVal Value As Byte)
                _QtCantTriple = Value
            End Set
        End Property
        Public Property CoTipCama_Triple() As Byte
            Get
                Return _CoTipCama_Triple
            End Get
            Set(ByVal Value As Byte)
                _CoTipCama_Triple = Value
            End Set
        End Property

        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal Value As String)
                _UserMod = Value
            End Set
        End Property
        Public Property CoTipoHab_Oper() As String
            Get
                Return _CoTipoHab_Oper
            End Get
            Set(ByVal Value As String)
                _CoTipoHab_Oper = Value
            End Set
        End Property
        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaServicios_Hoteles_Hab As List(Of clsServicios_Hotel_HabitacionesBE)
    Public Property ListaServicios_Hoteles_Hab() As List(Of clsServicios_Hotel_HabitacionesBE)
        Get
            Return _ListaServicios_Hoteles_Hab
        End Get
        Set(ByVal value As List(Of clsServicios_Hotel_HabitacionesBE))
            _ListaServicios_Hoteles_Hab = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsConcepto_HotelBE
    Dim _CoConcepto As Integer
    Dim _TxDescripcion As String
    Dim _CoTipo As String
    Dim _FlActivo As Boolean
    Dim _UserMod As String

    Public Property CoConcepto() As Integer
        Get
            Return _CoConcepto
        End Get
        Set(ByVal Value As Integer)
            _CoConcepto = Value
        End Set
    End Property
    Public Property TxDescripcion() As String
        Get
            Return _TxDescripcion
        End Get
        Set(ByVal Value As String)
            _TxDescripcion = Value
        End Set
    End Property
    Public Property CoTipo() As String
        Get
            Return _CoTipo
        End Get
        Set(ByVal Value As String)
            _CoTipo = Value
        End Set
    End Property
    Public Property FlActivo() As Boolean
        Get
            Return _FlActivo
        End Get
        Set(ByVal Value As Boolean)
            _FlActivo = Value
        End Set
    End Property
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal Value As String)
            _UserMod = Value
        End Set
    End Property
End Class

