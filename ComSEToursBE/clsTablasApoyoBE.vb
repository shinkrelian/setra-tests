﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices
Public Class clsTablasApoyoBE
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsBaseBE

        Private _Descripcion As String
        Public Property Descripcion() As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _CodPeruRail As String
        Public Property CodPeruRail() As String
            Get
                Return _CodPeruRail
            End Get
            Set(ByVal value As String)
                _CodPeruRail = value
            End Set
        End Property

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoDocBE
        Inherits clsBaseBE

        Private _IDTipoDoc As String
        Public Property IDTipoDoc() As String
            Get
                Return _IDTipoDoc
            End Get
            Set(ByVal value As String)
                _IDTipoDoc = value
            End Set
        End Property

        Private _Sunat As String
        Public Property Sunat() As String
            Get
                Return _Sunat
            End Get
            Set(ByVal value As String)
                _Sunat = value
            End Set
        End Property

        Private _CoStarSoft As String
        Public Property CoStarSoft() As String
            Get
                Return _CoStarSoft
            End Get
            Set(ByVal value As String)
                _CoStarSoft = value
            End Set
        End Property

        Private _CoSunat As String
        Public Property CoSunat() As String
            Get
                Return _CoSunat
            End Get
            Set(ByVal value As String)
                _CoSunat = value
            End Set
        End Property

        Private _NoFiscal As String
        Public Property NoFiscal() As String
            Get
                Return _NoFiscal
            End Get
            Set(ByVal value As String)
                _NoFiscal = value
            End Set
        End Property


    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsSerieBE
        Inherits clsBaseBE
        Private _IDSerie As String
        Public Property IDSerie() As String
            Get
                Return _IDSerie
            End Get
            Set(ByVal value As String)
                _IDSerie = value
            End Set
        End Property
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCategoriaBE
        Inherits clsBaseBE
        Private _IDCat As String
        Public Property IDCat() As String
            Get
                Return _IDCat
            End Get
            Set(ByVal value As String)
                _IDCat = value
            End Set
        End Property

        Private _Comision As Decimal
        Public Property Comision() As Decimal
            Get
                Return _Comision
            End Get
            Set(ByVal value As Decimal)
                _Comision = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCorrelativoBE
        Inherits clsBaseBE

        Private _Tabla As String
        Public Property Tabla() As String
            Get
                Return _Tabla
            End Get
            Set(ByVal value As String)
                _Tabla = value
            End Set
        End Property

        Private _Correlativo As Integer
        Public Property Correlativo() As Integer
            Get
                Return _Correlativo
            End Get
            Set(ByVal value As Integer)
                _Correlativo = value
            End Set
        End Property

        Private _IDTipoDoc As String
        Public Property IDTipoDoc() As String
            Get
                Return _IDTipoDoc
            End Get
            Set(ByVal value As String)
                _IDTipoDoc = value
            End Set
        End Property

        Private _CoSerie As String
        Public Property CoSerie() As String
            Get
                Return _CoSerie
            End Get
            Set(ByVal value As String)
                _CoSerie = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCapacidadHabitacionBE
        Inherits clsBaseBE

        Private _CoCapacidad As String
        Public Property CoCapacidad() As String
            Get
                Return _CoCapacidad
            End Get
            Set(ByVal value As String)
                _CoCapacidad = value
            End Set
        End Property

        Private _NoCapacidad As String
        Public Property NoCapacidad() As String
            Get
                Return _NoCapacidad
            End Get
            Set(ByVal value As String)
                _NoCapacidad = value
            End Set
        End Property

        Private _QtCapacidad As Byte
        Public Property QtCapacidad() As Byte
            Get
                Return _QtCapacidad
            End Get
            Set(ByVal value As Byte)
                _QtCapacidad = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsModoServicioBE
        Inherits clsBaseBE

        Private _CodModoServicio As String
        Public Property CodModoServicio() As String
            Get
                Return _CodModoServicio
            End Get
            Set(ByVal value As String)
                _CodModoServicio = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDesayunoBE
        Inherits clsBaseBE
        Private _IDDesayuno As String
        Public Property IDDesayuno() As String
            Get
                Return _IDDesayuno
            End Get
            Set(ByVal value As String)
                _IDDesayuno = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTarjetaCreditoBE
        Inherits clsBaseBE

        Private _CodTar As String
        Public Property CodTar() As String
            Get
                Return _CodTar
            End Get
            Set(ByVal value As String)
                _CodTar = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIdiomaBE
        Inherits clsBaseBE
        Private _IDIdioma As String
        Public Property IDIdioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property


        Private _IDIdiomaUpd As String
        Public Property IDIdiomaUpd() As String
            Get
                Return _IDIdiomaUpd
            End Get
            Set(ByVal value As String)
                _IDIdiomaUpd = value
            End Set
        End Property

        Private _Siglas As String
        Public Property Siglas() As String
            Get
                Return _Siglas
            End Get
            Set(ByVal value As String)
                _Siglas = value
            End Set
        End Property


    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsEspecialidadBE
        Inherits clsBaseBE

        Private _IDEspecialidad As String
        Public Property IDEspecialidad() As String
            Get
                Return _IDEspecialidad
            End Get
            Set(ByVal value As String)
                _IDEspecialidad = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoServicioBE
        Inherits clsBaseBE


        Private _IDServicio As String
        Public Property IDServicio() As String
            Get
                Return _IDServicio
            End Get
            Set(ByVal value As String)
                _IDServicio = value
            End Set
        End Property

        Private _CtaCon As String
        Public Property CtaCon() As String
            Get
                Return _CtaCon
            End Get
            Set(ByVal value As String)
                _CtaCon = value
            End Set
        End Property

        Private _Ingles As String
        Public Property Ingles() As String
            Get
                Return _Ingles
            End Get
            Set(ByVal value As String)
                _Ingles = value
            End Set
        End Property

        Private _SubDiario As String
        Public Property SubDiario() As String
            Get
                Return _SubDiario
            End Get
            Set(ByVal value As String)
                _SubDiario = value
            End Set
        End Property

        Private _CtaVta As String
        Public Property CtaVta() As String
            Get
                Return _CtaVta
            End Get
            Set(ByVal value As String)
                _CtaVta = value
            End Set
        End Property

        Private _SdVta As String
        Public Property SdVta() As String
            Get
                Return _SdVta
            End Get
            Set(ByVal value As String)
                _SdVta = value
            End Set
        End Property

        Private _IdFactu As String
        Public Property IdFactu() As String
            Get
                Return _IdFactu
            End Get
            Set(ByVal value As String)
                _IdFactu = value
            End Set
        End Property

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoUbigeoBE
        Inherits clsBaseBE
        Private _IDTipoUbigeo As String
        Public Property IDTipoUbigeo() As String
            Get
                Return _IDTipoUbigeo
            End Get
            Set(ByVal value As String)
                _IDTipoUbigeo = value
            End Set
        End Property


        Private _IDTipoUbigeoUpd As String
        Public Property IDTipoUbigeoUpd() As String
            Get
                Return _IDTipoUbigeoUpd
            End Get
            Set(ByVal value As String)
                _IDTipoUbigeoUpd = value
            End Set
        End Property

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsPlantillaBE
        Inherits clsBaseBE


        Private _IDDetalle As String
        Public Property IDDetalle() As String
            Get
                Return _IDDetalle
            End Get
            Set(ByVal value As String)
                _IDDetalle = value
            End Set
        End Property

        Private _IDIdioma As String
        Public Property IDIdioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property

        Private _IDUsuario As String
        Public Property IDUsuario() As String
            Get
                Return _IDUsuario
            End Get
            Set(ByVal value As String)
                _IDUsuario = value
            End Set
        End Property

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoDocIdentBE
        Inherits clsBaseBE
        Private _IDIdentidad As String
        Public Property IDIdentidad() As String
            Get
                Return _IDIdentidad
            End Get
            Set(ByVal value As String)
                _IDIdentidad = value
            End Set
        End Property
    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoOperacContabBE
        Inherits clsBaseBE
        Private _IDToc As String
        Public Property IDToc() As String
            Get
                Return _IDToc
            End Get
            Set(ByVal value As String)
                _IDToc = value
            End Set
        End Property

        Private _CtaContIGV As String
        Public Property CtaContIGV() As String
            Get
                Return _CtaContIGV
            End Get
            Set(ByVal value As String)
                _CtaContIGV = value
            End Set
        End Property

        Private _TxDefinicion As String
        Public Property TxDefinicion() As String
            Get
                Return _TxDefinicion
            End Get
            Set(ByVal value As String)
                _TxDefinicion = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTiposDeCambioUSDBE
        Inherits clsBaseBE

        Private _CoMoneda As String
        Public Property CoMoneda() As String
            Get
                Return _CoMoneda
            End Get
            Set(ByVal value As String)
                _CoMoneda = value
            End Set
        End Property

        Private _FeTipCam As Date
        Public Property FeTipCam() As Date
            Get
                Return _FeTipCam
            End Get
            Set(ByVal value As Date)
                _FeTipCam = value
            End Set
        End Property

        Private _SsTipCam As Decimal
        Public Property SsTipCam() As Decimal
            Get
                Return _SsTipCam
            End Get
            Set(ByVal value As Decimal)
                _SsTipCam = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTiposdeCambioBE
        Inherits clsBaseBE
        Private _Fecha As Date
        Public Property Fecha() As Date
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Date)
                _Fecha = value
            End Set
        End Property


        Private _ValCompra As Decimal
        Public Property ValCompra() As Decimal
            Get
                Return _ValCompra
            End Get
            Set(ByVal value As Decimal)
                _ValCompra = value
            End Set
        End Property

        Private _ValVenta As Decimal
        Public Property ValVenta() As Decimal
            Get
                Return _ValVenta
            End Get
            Set(ByVal value As Decimal)
                _ValVenta = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoCambio_MonedasBE
        Inherits clsBaseBE
        Dim _CoMoneda As String
        Dim _Fecha As Date
        Dim _ValCompra As Decimal
        Dim _ValVenta As Decimal
     
        Public Property CoMoneda() As String
            Get
                Return _CoMoneda
            End Get
            Set(ByVal Value As String)
                _CoMoneda = Value
            End Set
        End Property
        Public Property Fecha() As Date
            Get
                Return _Fecha
            End Get
            Set(ByVal Value As Date)
                _Fecha = Value
            End Set
        End Property
        Public Property ValCompra() As Decimal
            Get
                Return _ValCompra
            End Get
            Set(ByVal Value As Decimal)
                _ValCompra = Value
            End Set
        End Property
        Public Property ValVenta() As Decimal
            Get
                Return _ValVenta
            End Get
            Set(ByVal Value As Decimal)
                _ValVenta = Value
            End Set
        End Property
     
    End Class


    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsBancoBE
        Inherits clsBaseBE


        Private _IDBanco As String
        Public Property IDBanco() As String
            Get
                Return _IDBanco
            End Get
            Set(ByVal value As String)
                _IDBanco = value
            End Set
        End Property


        Private _Moneda As Char
        Public Property Moneda() As Char
            Get
                Return _Moneda
            End Get
            Set(ByVal value As Char)
                _Moneda = value
            End Set
        End Property

        Private _NroCuenta As String
        Public Property NroCuenta() As String
            Get
                Return _NroCuenta
            End Get
            Set(ByVal value As String)
                _NroCuenta = value
            End Set
        End Property

        Private _Sigla As String
        Public Property Sigla() As String
            Get
                Return _Sigla
            End Get
            Set(ByVal value As String)
                _Sigla = value
            End Set
        End Property

        Private _CtaInter As String
        Public Property CtaInter() As String
            Get
                Return _CtaInter
            End Get
            Set(ByVal value As String)
                _CtaInter = value
            End Set
        End Property

        Private _CtaCon As String
        Public Property CtaCon() As String
            Get
                Return _CtaCon
            End Get
            Set(ByVal value As String)
                _CtaCon = value
            End Set
        End Property

        Private _Direccion As String
        Public Property Direccion() As String
            Get
                Return _Direccion
            End Get
            Set(ByVal value As String)
                _Direccion = value
            End Set
        End Property

        Private _Swift As String
        Public Property Swift() As String
            Get
                Return _Swift
            End Get
            Set(ByVal value As String)
                _Swift = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsHabitTripleBE
        Inherits clsBaseBE

        Private _IdHabitTriple As String
        Public Property IdHabitTriple() As String
            Get
                Return _IdHabitTriple
            End Get
            Set(ByVal value As String)
                _IdHabitTriple = value
            End Set
        End Property

        Private _Abreviacion As String
        Public Property Abreviacion() As String
            Get
                Return _Abreviacion
            End Get
            Set(ByVal value As String)
                _Abreviacion = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsFormaPagoBE
        Inherits clsBaseBE
        Private _IDFor As String
        Public Property IDFor() As String
            Get
                Return _IDFor
            End Get
            Set(ByVal value As String)
                _IDFor = value
            End Set
        End Property

        Private _Tipo As String
        Public Property Tipo() As String
            Get
                Return _Tipo
            End Get
            Set(ByVal value As String)
                _Tipo = value
            End Set
        End Property

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsUbigeoBE
        Inherits clsBaseBE

        Private _IDUbigeo As String
        Public Property IDUbigeo() As String
            Get
                Return _IDUbigeo
            End Get
            Set(ByVal value As String)
                _IDUbigeo = value
            End Set
        End Property

        Private _TipoUbigeo As String
        Public Property TipoUbigeo() As String
            Get
                Return _TipoUbigeo
            End Get
            Set(ByVal value As String)
                _TipoUbigeo = value
            End Set
        End Property

        Private _Codigo As String
        Public Property Codigo() As String
            Get
                Return _Codigo
            End Get
            Set(ByVal value As String)
                _Codigo = value
            End Set
        End Property

        Private _Prove As Char
        Public Property Prove() As Char
            Get
                Return _Prove
            End Get
            Set(ByVal value As Char)
                _Prove = value
            End Set
        End Property


        Private _NI As Char
        Public Property NI() As Char
            Get
                Return _NI
            End Get
            Set(ByVal value As Char)
                _NI = value
            End Set
        End Property

        Private _DC As String
        Public Property DC() As String
            Get
                Return _DC
            End Get
            Set(ByVal value As String)
                _DC = value
            End Set
        End Property

        Private _Llamar As String
        Public Property Llamar() As String
            Get
                Return _Llamar
            End Get
            Set(ByVal value As String)
                _Llamar = value
            End Set
        End Property

        Private _IDPais As String
        Public Property IDPais() As String
            Get
                Return _IDPais
            End Get
            Set(ByVal value As String)
                _IDPais = value
            End Set
        End Property

        Private _Gentilicio As String
        Public Property Gentilicio() As String
            Get
                Return _Gentilicio
            End Get
            Set(ByVal value As String)
                _Gentilicio = value
            End Set
        End Property

        Private _EsOficina As Boolean
        Public Property EsOficina() As Boolean
            Get
                Return _EsOficina
            End Get
            Set(ByVal value As Boolean)
                _EsOficina = value
            End Set
        End Property

        Private _CodigoINC As String
        Public Property CodigoINC() As String
            Get
                Return _CodigoINC
            End Get
            Set(ByVal value As String)
                _CodigoINC = value
            End Set
        End Property

        Private _CodigoPeruRail As String
        Public Property CodigoPeruRail() As String
            Get
                Return _CodigoPeruRail
            End Get
            Set(ByVal value As String)
                _CodigoPeruRail = value
            End Set
        End Property

        Private _CoInternacPais As String
        Public Property CoInternacPais() As String
            Get
                Return _CoInternacPais
            End Get
            Set(ByVal value As String)
                _CoInternacPais = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsPlanCuentasBE
        Inherits clsBaseBE

        Private _CtaContable As String
        Public Property CtaContable() As String
            Get
                Return _CtaContable
            End Get
            Set(ByVal value As String)
                _CtaContable = value
            End Set
        End Property

        Private _CoCtroCosto As String
        Public Property CoCtroCosto() As String
            Get
                Return _CoCtroCosto
            End Get
            Set(ByVal value As String)
                _CoCtroCosto = value
            End Set
        End Property

        Private _FlFacturacion As Boolean
        Public Property FlFacturacion() As Boolean
            Get
                Return _FlFacturacion
            End Get
            Set(ByVal value As Boolean)
                _FlFacturacion = value
            End Set
        End Property
        Private _FlClientes As Boolean
        Public Property FlClientes() As Boolean
            Get
                Return _FlClientes
            End Get
            Set(ByVal value As Boolean)
                _FlClientes = value
            End Set
        End Property

        Private _TxDefinicion As String
        Public Property TxDefinicion() As String
            Get
                Return _TxDefinicion
            End Get
            Set(ByVal value As String)
                _TxDefinicion = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoBE
        Inherits clsBaseBE

        Private _IDIncluido As Integer
        Public Property IDIncluido() As Integer
            Get
                Return _IDIncluido
            End Get
            Set(ByVal value As Integer)
                _IDIncluido = value
            End Set
        End Property

        Private _Tipo As Char
        Public Property Tipo() As Char
            Get
                Return _Tipo
            End Get
            Set(ByVal value As Char)
                _Tipo = value
            End Set
        End Property

        Private _DescAbrev As String
        Public Property DescAbrev() As String
            Get
                Return _DescAbrev
            End Get
            Set(ByVal value As String)
                _DescAbrev = value
            End Set
        End Property

        Private _PorDefecto As Boolean
        Public Property PorDefecto() As Boolean
            Get
                Return _PorDefecto
            End Get
            Set(ByVal value As Boolean)
                _PorDefecto = value
            End Set
        End Property

        Private _FlAutomatico As Boolean
        Public Property FlAutomatico() As Boolean
            Get
                Return _FlAutomatico
            End Get
            Set(ByVal value As Boolean)
                _FlAutomatico = value
            End Set
        End Property

        Private _Orden As Byte
        Public Property Orden() As Byte
            Get
                Return _Orden
            End Get
            Set(ByVal value As Byte)
                _Orden = value
            End Set
        End Property

        Private _TxObsStatus As String
        Public Property TxObsStatus() As String
            Get
                Return _TxObsStatus
            End Get
            Set(ByVal value As String)
                _TxObsStatus = value
            End Set
        End Property

        Private _ListaIncluidosIdioma As List(Of clsIncluidoIdiomasBE)
        Public Property ListaIncluidosIdioma() As List(Of clsIncluidoIdiomasBE)
            Get
                Return _ListaIncluidosIdioma
            End Get
            Set(ByVal value As List(Of clsIncluidoIdiomasBE))
                _ListaIncluidosIdioma = value
            End Set
        End Property

        Private _ListIncluidosServicio As List(Of clsIncluidoServicioBE)
        Public Property ListaIncluidosServicio() As List(Of clsIncluidoServicioBE)
            Get
                Return _ListIncluidosServicio
            End Get
            Set(ByVal value As List(Of clsIncluidoServicioBE))
                _ListIncluidosServicio = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoIdiomasBE
        Inherits clsIncluidoBE

        Private _IDIdioma As String
        Public Property IDIdioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoServicioBE
        Inherits clsIncluidoBE

        Private _IDServicio As String
        Public Property IDServicio() As String
            Get
                Return _IDServicio
            End Get
            Set(ByVal value As String)
                _IDServicio = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsIncluidoOperacionesBE
        Inherits clsBaseBE

        Private _NuIncluido As Integer
        Public Property NuIncluido() As Integer
            Get
                Return _NuIncluido
            End Get
            Set(ByVal value As Integer)
                _NuIncluido = value
            End Set
        End Property

        Private _NoObserAbrev As String
        Public Property NoObserAbrev() As String
            Get
                Return _NoObserAbrev
            End Get
            Set(ByVal value As String)
                _NoObserAbrev = value
            End Set
        End Property

        Private _FlPorDefecto As Boolean
        Public Property FlPorDefecto() As Boolean
            Get
                Return _FlPorDefecto
            End Get
            Set(ByVal value As Boolean)
                _FlPorDefecto = value
            End Set
        End Property

        Private _FlInterno As Boolean
        Public Property FlInterno() As Boolean
            Get
                Return _FlInterno
            End Get
            Set(ByVal value As Boolean)
                _FlInterno = value
            End Set
        End Property


        Private _NuOrden As Byte
        Public Property NuOrden() As Byte
            Get
                Return _NuOrden
            End Get
            Set(ByVal value As Byte)
                _NuOrden = value
            End Set
        End Property

        Private _ListaIncluidoOperaciones As List(Of clsIncluidoOperacionesIdiomasBE)
        Public Property ListaIncluidoOperaciones() As List(Of clsIncluidoOperacionesIdiomasBE)
            Get
                Return _ListaIncluidoOperaciones
            End Get
            Set(ByVal value As List(Of clsIncluidoOperacionesIdiomasBE))
                _ListaIncluidoOperaciones = value
            End Set
        End Property

    End Class

    Public Class clsIncluidoOperacionesIdiomasBE
        Inherits clsIncluidoOperacionesBE

        Private _NoObservacion As String
        Public Property NoObservacion() As String
            Get
                Return _NoObservacion
            End Get
            Set(ByVal value As String)
                _NoObservacion = value
            End Set
        End Property

        Private _IDIDioma As String
        Public Property IDIDioma() As String
            Get
                Return _IDIDioma
            End Get
            Set(ByVal value As String)
                _IDIDioma = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTipoProveedorBE
        Inherits clsBaseBE
        Private _IDTipoProv As String
        Public Property IDTipoProv() As String
            Get
                Return _IDTipoProv
            End Get
            Set(ByVal value As String)
                _IDTipoProv = value
            End Set
        End Property

        Private _PideIdioma As Boolean
        Public Property PideIdioma() As Boolean
            Get
                Return _PideIdioma
            End Get
            Set(ByVal value As Boolean)
                _PideIdioma = value
            End Set
        End Property

        Private _Comision As Decimal
        Public Property Comision() As Decimal
            Get
                Return _Comision
            End Get
            Set(ByVal value As Decimal)
                _Comision = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
     <Transaction(TransactionOption.NotSupported)> _
    Public Class clsFechasEspecialesBE
        Inherits clsBaseBE

        Private _Fecha As Date
        Public Property Fecha() As Date
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Date)
                _Fecha = value
            End Set
        End Property

        Private _IDUbigeo As String
        Public Property IDUbigeo() As String
            Get
                Return _IDUbigeo
            End Get
            Set(ByVal value As String)
                _IDUbigeo = value
            End Set
        End Property

        Private _NoConsideraAnio As Boolean
        Public Property NoConsideraAnio() As Boolean
            Get
                Return _NoConsideraAnio
            End Get
            Set(ByVal value As Boolean)
                _NoConsideraAnio = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsVehiculosBE
        Inherits clsBaseBE
        Private _NuVehiculo As Byte
        Public Property NuVehiculo() As Byte
            Get
                Return _NuVehiculo
            End Get
            Set(ByVal value As Byte)
                _NuVehiculo = value
            End Set
        End Property

        Private _QtDe As Byte
        Public Property QtDe() As Byte
            Get
                Return _QtDe
            End Get
            Set(ByVal value As Byte)
                _QtDe = value
            End Set
        End Property

        Private _QtHasta As Byte
        Public Property QtHasta() As Byte
            Get
                Return _QtHasta
            End Get
            Set(ByVal value As Byte)
                _QtHasta = value
            End Set
        End Property

        Private _NoVehiculoCitado As String
        Public Property NoVehiculoCitado() As String
            Get
                Return _NoVehiculoCitado
            End Get
            Set(ByVal value As String)
                _NoVehiculoCitado = value
            End Set
        End Property

        Private _QtCapacidad As Byte
        Public Property QtCapacidad() As Byte
            Get
                Return _QtCapacidad
            End Get
            Set(ByVal value As Byte)
                _QtCapacidad = value
            End Set
        End Property

        Private _FlOtraCiudad As Boolean
        Public Property FlOtraCiudad() As Boolean
            Get
                Return _FlOtraCiudad
            End Get
            Set(ByVal value As Boolean)
                _FlOtraCiudad = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
   <Transaction(TransactionOption.NotSupported)> _
    Public Class clsVehiculos_TransporteBE
        Inherits clsBaseBE
        Dim _NuVehiculo As Byte
        Dim _NuVehiculo_Anterior As Byte
        Dim _CoTipo As Byte
        Dim _QtDe As Byte
        Dim _QtHasta As Byte
        Dim _NoVehiculoCitado As String
        Dim _QtCapacidad As Byte
        Dim _FlOtraCiudad As Boolean
        Dim _CoUbigeo As String
        Dim _CoMercado As Byte
        Dim _FlActivo As Boolean
        Dim _FlReserva As Boolean
        Dim _FlProgramacion As Boolean

        Public Property NuVehiculo() As Byte
            Get
                Return _NuVehiculo
            End Get
            Set(ByVal Value As Byte)
                _NuVehiculo = Value
            End Set
        End Property
        Public Property NuVehiculo_Anterior() As Byte
            Get
                Return _NuVehiculo_Anterior
            End Get
            Set(ByVal Value As Byte)
                _NuVehiculo_Anterior = Value
            End Set
        End Property
        Public Property CoTipo() As Byte
            Get
                Return _CoTipo
            End Get
            Set(ByVal Value As Byte)
                _CoTipo = Value
            End Set
        End Property
        Public Property QtDe() As Byte
            Get
                Return _QtDe
            End Get
            Set(ByVal Value As Byte)
                _QtDe = Value
            End Set
        End Property
        Public Property QtHasta() As Byte
            Get
                Return _QtHasta
            End Get
            Set(ByVal Value As Byte)
                _QtHasta = Value
            End Set
        End Property
        Public Property NoVehiculoCitado() As String
            Get
                Return _NoVehiculoCitado
            End Get
            Set(ByVal Value As String)
                _NoVehiculoCitado = Value
            End Set
        End Property
        Public Property QtCapacidad() As Byte
            Get
                Return _QtCapacidad
            End Get
            Set(ByVal Value As Byte)
                _QtCapacidad = Value
            End Set
        End Property
        Public Property FlOtraCiudad() As Boolean
            Get
                Return _FlOtraCiudad
            End Get
            Set(ByVal Value As Boolean)
                _FlOtraCiudad = Value
            End Set
        End Property
        Public Property CoUbigeo() As String
            Get
                Return _CoUbigeo
            End Get
            Set(ByVal Value As String)
                _CoUbigeo = Value
            End Set
        End Property
        Public Property CoMercado() As Byte
            Get
                Return _CoMercado
            End Get
            Set(ByVal Value As Byte)
                _CoMercado = Value
            End Set
        End Property
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property

        Public Property FlReserva As Boolean
            Get
                Return _FlReserva
            End Get
            Set(ByVal Value As Boolean)
                _FlReserva = Value
            End Set
        End Property

        Public Property FlProgramacion As Boolean
            Get
                Return _FlProgramacion
            End Get
            Set(ByVal Value As Boolean)
                _FlProgramacion = Value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsMercadoBE
        Inherits clsBaseBE
        Dim _CoMercado As Byte
      


        Public Property CoMercado() As Byte
            Get
                Return _CoMercado
            End Get
            Set(ByVal Value As Byte)
                _CoMercado = Value
            End Set
        End Property
     
    End Class


    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsCuestionarioBE
        Inherits clsBaseBE

        Private _NuCuest As Byte
        Public Property NuCuest() As Byte
            Get
                Return _NuCuest
            End Get
            Set(ByVal value As Byte)
                _NuCuest = value
            End Set
        End Property

        Private _IDTipoProv As String
        Public Property IDTipoProv() As String
            Get
                Return _IDTipoProv
            End Get
            Set(ByVal value As String)
                _IDTipoProv = value
            End Set
        End Property

        Private _NoDescripcion As String
        Public Property NoDescripcion() As String
            Get
                Return _NoDescripcion
            End Get
            Set(ByVal value As String)
                _NoDescripcion = value
            End Set
        End Property

        Private _Activo As Boolean
        Public Property Activo() As Boolean
            Get
                Return _Activo
            End Get
            Set(ByVal value As Boolean)
                _Activo = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsVehiculosProgramacionBE
        Inherits clsBaseBE
        Private _NuVehiculoProg As Int16
        Public Property NuVehiculoProg() As Int16
            Get
                Return _NuVehiculoProg
            End Get
            Set(ByVal value As Int16)
                _NuVehiculoProg = value
            End Set
        End Property

        Private _NoUnidadProg As String
        Public Property NoUnidadProg() As String
            Get
                Return _NoUnidadProg
            End Get
            Set(ByVal value As String)
                _NoUnidadProg = value
            End Set
        End Property

        Private _QtCapacidadPax As Byte
        Public Property QtCapacidadPax() As Byte
            Get
                Return _QtCapacidadPax
            End Get
            Set(ByVal value As Byte)
                _QtCapacidadPax = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDiasFeriadosBE
        Inherits clsBaseBE

        Private _IDFeriado As String
        Public Property IDFeriado() As String
            Get
                Return _IDFeriado
            End Get
            Set(ByVal value As String)
                _IDFeriado = value
            End Set
        End Property


        Private _FechaFeriado As Date
        Public Property FechaFeriado() As Date
            Get
                Return _FechaFeriado
            End Get
            Set(ByVal value As Date)
                _FechaFeriado = value
            End Set
        End Property

        Private _IDPais As String
        Public Property IDPais() As String
            Get
                Return _IDPais
            End Get
            Set(ByVal value As String)
                _IDPais = value
            End Set
        End Property

        Private _DescripcionF As String
        Public Property DescripcionF() As String
            Get
                Return _DescripcionF
            End Get
            Set(ByVal value As String)
                _DescripcionF = value
            End Set
        End Property



        Private _UserModF As String
        Public Property UserModF() As String
            Get
                Return _UserModF
            End Get
            Set(ByVal value As String)
                _UserModF = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsParametroBE
        Inherits clsBaseBE

        Private _NoNombreEmpresa As String
        Public Property NoNombreEmpresa() As String
            Get
                Return _NoNombreEmpresa
            End Get
            Set(ByVal value As String)
                _NoNombreEmpresa = value
            End Set
        End Property


        Private _NoRUCEmpresa As String
        Public Property NoRUCEmpresa() As String
            Get
                Return _NoRUCEmpresa
            End Get
            Set(ByVal value As String)
                _NoRUCEmpresa = value
            End Set
        End Property

        Private _NoRSocialEmpresa As String
        Public Property NoRSocialEmpresa() As String
            Get
                Return _NoRSocialEmpresa
            End Get
            Set(ByVal value As String)
                _NoRSocialEmpresa = value
            End Set
        End Property

        Private _NoDireccEmpresa As String
        Public Property NoDireccEmpresa() As String
            Get
                Return _NoDireccEmpresa
            End Get
            Set(ByVal value As String)
                _NoDireccEmpresa = value
            End Set
        End Property

        Private _NuTelefEmpresa As String
        Public Property NuTelefEmpresa() As String
            Get
                Return _NuTelefEmpresa
            End Get
            Set(ByVal value As String)
                _NuTelefEmpresa = value
            End Set
        End Property

        Private _NuFaxEmpresa As String
        Public Property NuFaxEmpresa() As String
            Get
                Return _NuFaxEmpresa
            End Get
            Set(ByVal value As String)
                _NuFaxEmpresa = value
            End Set
        End Property

        Private _NoEmailEmpresa As String
        Public Property NoEmailEmpresa() As String
            Get
                Return _NoEmailEmpresa
            End Get
            Set(ByVal value As String)
                _NoEmailEmpresa = value
            End Set
        End Property

        Private _NoWebEmpresa As String
        Public Property NoWebEmpresa() As String
            Get
                Return _NoWebEmpresa
            End Get
            Set(ByVal value As String)
                _NoWebEmpresa = value
            End Set
        End Property

        Private _NoRutaCorreoMarketing As String
        Public Property NoRutaCorreoMarketing() As String
            Get
                Return _NoRutaCorreoMarketing
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If

                _NoRutaCorreoMarketing = strRuta
            End Set
        End Property

        Private _NoRutaCorreoMarketingServer As String
        Public Property NoRutaCorreoMarketingServer() As String
            Get
                Return _NoRutaCorreoMarketingServer
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If

                _NoRutaCorreoMarketingServer = strRuta
            End Set
        End Property

        Private _NoRutaImagenes As String
        Public Property NoRutaImagenes() As String
            Get
                Return _NoRutaImagenes
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If

                _NoRutaImagenes = strRuta
            End Set
        End Property

        Private _NoRutaIniPdfProveedor As String
        Public Property NoRutaIniPdfProveedor() As String
            Get
                Return _NoRutaIniPdfProveedor
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If

                _NoRutaIniPdfProveedor = strRuta
            End Set
        End Property

        Private _NoRutaIniFotoWordVtas As String
        Public Property NoRutaIniFotoWordVtas() As String
            Get
                Return _NoRutaIniFotoWordVtas
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If


                _NoRutaIniFotoWordVtas = strRuta
            End Set
        End Property

        Private _NoRutaVouchers As String
        Public Property NoRutaVouchers() As String
            Get
                Return _NoRutaVouchers
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If

                _NoRutaVouchers = strRuta
            End Set
        End Property

        Private _NoRutaPerfilCorreosSQL As String
        Public Property NoRutaPerfilCorreosSQL() As String
            Get
                Return _NoRutaPerfilCorreosSQL
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If

                _NoRutaPerfilCorreosSQL = strRuta

            End Set
        End Property

        Private _NoServerCorreo As String
        Public Property NoServerCorreo() As String
            Get
                Return _NoServerCorreo
            End Get
            Set(ByVal value As String)
                _NoServerCorreo = value
            End Set
        End Property

        Private _NoRutaPerfilCorreosSQLServer As String
        Public Property NoRutaPerfilCorreosSQLServer() As String
            Get
                Return _NoRutaPerfilCorreosSQLServer
            End Get
            Set(ByVal value As String)
                Dim strRuta As String = value
                If strRuta.Trim.Length > 0 Then
                    If strRuta.Trim.Substring(strRuta.Trim.Length - 1, 1) <> "\" Then
                        strRuta = strRuta.Trim & "\"
                    End If
                End If

                _NoRutaPerfilCorreosSQLServer = strRuta
            End Set
        End Property


        Private _NuIGV As Single
        Public Property NuIGV() As Single
            Get
                Return _NuIGV
            End Get
            Set(ByVal value As Single)
                _NuIGV = value
            End Set
        End Property

        Private _PoProrrata As Single
        Public Property PoProrrata() As Single
            Get
                Return _PoProrrata
            End Get
            Set(ByVal value As Single)
                _PoProrrata = value
            End Set
        End Property

        Public Class clsProrrataBE
            Inherits clsBaseBE

            Private _NuCodigoPro As Byte
            Public Property NuCodigoPro() As Byte
                Get
                    Return _NuCodigoPro
                End Get
                Set(ByVal value As Byte)
                    _NuCodigoPro = value
                End Set
            End Property

            Private _FeDesde As Date
            Public Property FeDesde() As Date
                Get
                    Return _FeDesde
                End Get
                Set(ByVal value As Date)
                    _FeDesde = value
                End Set
            End Property

            Private _FeHasta As Date
            Public Property FeHasta() As Date
                Get
                    Return _FeHasta
                End Get
                Set(ByVal value As Date)
                    _FeHasta = value
                End Set
            End Property

            Private _QtProrrata As Single
            Public Property QtProrrata() As Single
                Get
                    Return _QtProrrata
                End Get
                Set(ByVal value As Single)
                    _QtProrrata = value
                End Set
            End Property

            Private _Accion As String
            Public Property Accion() As String
                Get
                    Return _Accion
                End Get
                Set(ByVal value As String)
                    _Accion = value
                End Set
            End Property



            Private _ListaProrrata As List(Of clsProrrataBE)
            Public Property ListaProrrata() As List(Of clsProrrataBE)
                Get
                    Return _ListaProrrata
                End Get
                Set(ByVal value As List(Of clsProrrataBE))
                    _ListaProrrata = value
                End Set
            End Property

        End Class

    End Class

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsTemporadaBE
        Inherits clsBaseBE
        Private _IDTemporada As String
        Public Property IDTemporada() As Int16
            Get
                Return _IDTemporada
            End Get
            Set(ByVal value As Int16)
                _IDTemporada = value
            End Set
        End Property

        Private _TipoTemporada As String
        Public Property TipoTemporada() As String
            Get
                Return _TipoTemporada
            End Get
            Set(ByVal Value As String)
                _TipoTemporada = Value
            End Set
        End Property

        Private _FlActivo As String
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
  <Transaction(TransactionOption.NotSupported)> _
    Public Class clsENTRADASINC_SAPBE
        Inherits clsBaseBE
        Dim _IDCab As Integer
        Dim _IDFile As String
        Dim _Tipo As String
        Dim _Monto As Decimal
        Dim _FechaPago As Date
        Dim _NuCodigo_ER As String
        Dim _NuDocumProv As Integer
        Dim _FlActivo As Boolean
        Dim _UserMod As String
        Dim _FecMod As Date
        Dim _Accion As String
        Dim _IDDet As Integer

        Dim _NuPreSob As Integer
        Dim _NuDetPSo As Integer

        'NuPreSob
        'NuDetPSo

        Public Property IDCab() As Integer
            Get
                Return _IDCab
            End Get
            Set(ByVal Value As Integer)
                _IDCab = Value
            End Set
        End Property
        Public Property IDFile() As String
            Get
                Return _IDFile
            End Get
            Set(ByVal Value As String)
                _IDFile = Value
            End Set
        End Property
        Public Property Tipo() As String
            Get
                Return _Tipo
            End Get
            Set(ByVal Value As String)
                _Tipo = Value
            End Set
        End Property
        Public Property Monto() As Decimal
            Get
                Return _Monto
            End Get
            Set(ByVal Value As Decimal)
                _Monto = Value
            End Set
        End Property
        Public Property FechaPago() As Date
            Get
                Return _FechaPago
            End Get
            Set(ByVal Value As Date)
                _FechaPago = Value
            End Set
        End Property
        Public Property NuCodigo_ER() As String
            Get
                Return _NuCodigo_ER
            End Get
            Set(ByVal Value As String)
                _NuCodigo_ER = Value
            End Set
        End Property
        Public Property NuDocumProv() As Integer
            Get
                Return _NuDocumProv
            End Get
            Set(ByVal Value As Integer)
                _NuDocumProv = Value
            End Set
        End Property
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property
   
        Public Property FecMod() As Date
            Get
                Return _FecMod
            End Get
            Set(ByVal Value As Date)
                _FecMod = Value
            End Set
        End Property

        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal Value As String)
                _Accion = Value
            End Set
        End Property

        Public Property IdDet() As Integer
            Get
                Return _IDDet
            End Get
            Set(ByVal Value As Integer)
                _IDDet = Value
            End Set
        End Property

        Public Property NuPreSob() As Integer
            Get
                Return _NuPreSob
            End Get
            Set(ByVal Value As Integer)
                _NuPreSob = Value
            End Set
        End Property
        Public Property NuDetPSo() As Integer
            Get
                Return _NuDetPSo
            End Get
            Set(ByVal Value As Integer)
                _NuDetPSo = Value
            End Set
        End Property

    End Class

End Class