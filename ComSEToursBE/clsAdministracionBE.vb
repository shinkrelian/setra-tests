﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsAdministracionBaseBE
    'Inherits ServicedComponent

    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

    Private _Accion As String
    Public Property Accion() As String
        Get
            Return _Accion
        End Get
        Set(ByVal value As String)
            _Accion = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAdministracionBE
    Inherits clsAdministracionBaseBE

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsIngresoFinanzasBE
    Inherits clsAdministracionBaseBE

    Private _NuIngreso As Integer
    Public Property NuIngreso() As Integer
        Get
            Return _NuIngreso
        End Get
        Set(ByVal value As Integer)
            _NuIngreso = value
        End Set
    End Property

    Private _FeAcreditada As Date
    Public Property FeAcreditada() As Date
        Get
            Return _FeAcreditada
        End Get
        Set(ByVal value As Date)
            _FeAcreditada = value
        End Set
    End Property

    Private _FePresentacion As Date
    Public Property FePresentacion() As Date
        Get
            Return _FePresentacion
        End Get
        Set(ByVal value As Date)
            _FePresentacion = value
        End Set
    End Property

    Private _IDBanco As String
    Public Property IDBanco() As String
        Get
            Return _IDBanco
        End Get
        Set(ByVal value As String)
            _IDBanco = value
        End Set
    End Property

    Private _IDFormaPago As String
    Public Property IDFormaPago() As String
        Get
            Return _IDFormaPago
        End Get
        Set(ByVal value As String)
            _IDFormaPago = value
        End Set
    End Property

    Private _CoOperacionBan As String
    Public Property CoOperacionBan() As String
        Get
            Return _CoOperacionBan
        End Get
        Set(ByVal value As String)
            _CoOperacionBan = value
        End Set
    End Property

    Private _IDCliente As String
    Public Property IDCliente() As String
        Get
            Return _IDCliente
        End Get
        Set(ByVal value As String)
            _IDCliente = value
        End Set
    End Property

    Private _IDPais As String
    Public Property IDPais() As String
        Get
            Return _IDPais
        End Get
        Set(ByVal value As String)
            _IDPais = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _NoRutaDocumento As String
    Public Property NoRutaDocumento() As String
        Get
            Return _NoRutaDocumento
        End Get
        Set(ByVal value As String)
            _NoRutaDocumento = value
        End Set
    End Property

    Private _TxObservacion As String
    Public Property TxObservacion() As String
        Get
            Return _TxObservacion
        End Get
        Set(ByVal value As String)
            _TxObservacion = value
        End Set
    End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    Private _ssTC As Single
    Public Property ssTC() As Single
        Get
            Return _ssTC
        End Get
        Set(ByVal value As Single)
            _ssTC = value
        End Set
    End Property

    Private _SsOrdenado As Double
    Public Property SsOrdenado() As Double
        Get
            Return _SsOrdenado
        End Get
        Set(ByVal value As Double)
            _SsOrdenado = value
        End Set
    End Property

    Private _SsGastosTransferencia As Double
    Public Property SsGastosTransferencia() As Double
        Get
            Return _SsGastosTransferencia
        End Get
        Set(ByVal value As Double)
            _SsGastosTransferencia = value
        End Set
    End Property

    Private _SsRecibido As Double
    Public Property SsRecibido() As Double
        Get
            Return _SsRecibido
        End Get
        Set(ByVal value As Double)
            _SsRecibido = value
        End Set
    End Property

    Private _SsBanco As Double
    Public Property SsBanco() As Double
        Get
            Return _SsBanco
        End Get
        Set(ByVal value As Double)
            _SsBanco = value
        End Set
    End Property

    Private _SsNeto As Double
    Public Property SsNeto() As Double
        Get
            Return _SsNeto
        End Get
        Set(ByVal value As Double)
            _SsNeto = value
        End Set
    End Property

    Private _NoBancoOrdendante As String
    Public Property NoBancoOrdendante() As String
        Get
            Return _NoBancoOrdendante
        End Get
        Set(ByVal value As String)
            _NoBancoOrdendante = value
        End Set
    End Property

    Private _NoBancoCorresponsal1 As String
    Public Property NoBancoCorresponsal1() As String
        Get
            Return _NoBancoCorresponsal1
        End Get
        Set(ByVal value As String)
            _NoBancoCorresponsal1 = value
        End Set
    End Property

    Private _NoBancoCorresponsal2 As String
    Public Property NoBancoCorresponsal2() As String
        Get
            Return _NoBancoCorresponsal2
        End Get
        Set(ByVal value As String)
            _NoBancoCorresponsal2 = value
        End Set
    End Property

    Private _IDUbigeoBancoOrde As String
    Public Property IDUbigeoBancoOrde() As String
        Get
            Return _IDUbigeoBancoOrde
        End Get
        Set(ByVal value As String)
            _IDUbigeoBancoOrde = value
        End Set
    End Property

    Private _IDUbigeoBancoCor1 As String
    Public Property IDUbigeoBancoCor1() As String
        Get
            Return _IDUbigeoBancoCor1
        End Get
        Set(ByVal value As String)
            _IDUbigeoBancoCor1 = value
        End Set
    End Property

    Private _IDUbigeoBancoCor2 As String
    Public Property IDUbigeoBancoCor2() As String
        Get
            Return _IDUbigeoBancoCor2
        End Get
        Set(ByVal value As String)
            _IDUbigeoBancoCor2 = value
        End Set
    End Property

    'Private _ListaIngresoDebitMemo As List(Of clsIngreso_DebitMemoBE)
    'Public Property ListaIngresoDebitMemo() As List(Of clsIngreso_DebitMemoBE)
    '    Get
    '        Return _ListaIngresoDebitMemo
    '    End Get
    '    Set(ByVal value As List(Of clsIngreso_DebitMemoBE))
    '        _ListaIngresoDebitMemo = value
    '    End Set
    'End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsAsignacionBE
    Inherits clsAdministracionBaseBE

    Private _NuAsignacion As Integer
    Public Property NuAsignacion() As Integer
        Get
            Return _NuAsignacion
        End Get
        Set(ByVal value As Integer)
            _NuAsignacion = value
        End Set
    End Property

    Private _TxObservacion As String
    Public Property TxObservacion() As String
        Get
            Return _TxObservacion
        End Get
        Set(ByVal value As String)
            _TxObservacion = value
        End Set
    End Property

    Private _SsImportePagado As Double
    Public Property SsImportePagado() As Double
        Get
            Return _SsImportePagado
        End Get
        Set(ByVal value As Double)
            _SsImportePagado = value
        End Set
    End Property

    '
    Private _NuIngreso As Integer
    Public Property NuIngreso() As Integer
        Get
            Return _NuIngreso
        End Get
        Set(ByVal value As Integer)
            _NuIngreso = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _ListaDebitMemo As List(Of clsDebitMemoBE)
    Public Property ListaDebitMemo() As List(Of clsDebitMemoBE)
        Get
            Return _ListaDebitMemo
        End Get
        Set(ByVal value As List(Of clsDebitMemoBE))
            _ListaDebitMemo = value
        End Set
    End Property

    Private _ListaIngresoDebitMemo As List(Of clsIngreso_DebitMemoBE)
    Public Property ListaIngresoDebitMemo() As List(Of clsIngreso_DebitMemoBE)
        Get
            Return _ListaIngresoDebitMemo
        End Get
        Set(ByVal value As List(Of clsIngreso_DebitMemoBE))
            _ListaIngresoDebitMemo = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsIngreso_DebitMemoBE
    Inherits clsAdministracionBaseBE

    Private _NuIngreso As Integer
    Public Property NuIngreso() As Integer
        Get
            Return _NuIngreso
        End Get
        Set(ByVal value As Integer)
            _NuIngreso = value
        End Set
    End Property

    Private _IDDebitMemo As String
    Public Property IDDebitMemo() As String
        Get
            Return _IDDebitMemo
        End Get
        Set(ByVal value As String)
            _IDDebitMemo = value
        End Set
    End Property

    Private __NuAsignacion As Integer
    Public Property NuAsignacion() As Integer
        Get
            Return __NuAsignacion
        End Get
        Set(ByVal value As Integer)
            __NuAsignacion = value
        End Set
    End Property

    Private _SsPago As Double
    Public Property SsPago() As Double
        Get
            Return _SsPago
        End Get
        Set(ByVal value As Double)
            _SsPago = value
        End Set
    End Property

    Private _SsGastosTransferencia As Double
    Public Property SsGastosTransferencia() As Double
        Get
            Return _SsGastosTransferencia
        End Get
        Set(ByVal value As Double)
            _SsGastosTransferencia = value
        End Set
    End Property

    Private _SsComisionBanco As Double
    Public Property SsComisionBanco() As Double
        Get
            Return _SsComisionBanco
        End Get
        Set(ByVal value As Double)
            _SsComisionBanco = value
        End Set
    End Property


    Private _SsSaldoNuevo As Double
    Public Property SsSaldoNuevo() As Double
        Get
            Return _SsSaldoNuevo
        End Get
        Set(ByVal value As Double)
            _SsSaldoNuevo = value
        End Set
    End Property

    Private _IDEstadoDebitMemo As String
    Public Property IDEstadoDebitMemo() As String
        Get
            Return _IDEstadoDebitMemo
        End Get
        Set(ByVal value As String)
            _IDEstadoDebitMemo = value
        End Set
    End Property

    Private _SsSaldoDebitMemo As Double
    Public Property SsSaldoDebitMemo() As Double
        Get
            Return _SsSaldoDebitMemo
        End Get
        Set(ByVal value As Double)
            _SsSaldoDebitMemo = value
        End Set
    End Property

    Private _ErrorSAP As String
    Public Property ErrorSAP() As String
        Get
            Return _ErrorSAP
        End Get
        Set(ByVal value As String)
            _ErrorSAP = value
        End Set
    End Property

    Private _ErrorSetra As String
    Public Property ErrorSetra() As String
        Get
            Return _ErrorSetra
        End Get
        Set(ByVal value As String)
            _ErrorSetra = value
        End Set
    End Property

    Private _CoSAP As String
    Public Property CoSAP() As String
        Get
            Return _CoSAP
        End Get
        Set(ByVal value As String)
            _CoSAP = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenPagoBE
    Inherits clsAdministracionBaseBE

    Private _IDOrdPag As Integer
    Public Property IDOrdPag() As Integer
        Get
            Return _IDOrdPag
        End Get
        Set(ByVal value As Integer)
            _IDOrdPag = value
        End Set
    End Property

    Private _IDReserva As Integer
    Public Property IDReserva() As Integer
        Get
            Return _IDReserva
        End Get
        Set(ByVal value As Integer)
            _IDReserva = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _IDFormaPago As String
    Public Property IDFormaPago() As String
        Get
            Return _IDFormaPago
        End Get
        Set(ByVal value As String)
            _IDFormaPago = value
        End Set
    End Property

    Private _IDBanco As String
    Public Property IDBanco() As String
        Get
            Return _IDBanco
        End Get
        Set(ByVal value As String)
            _IDBanco = value
        End Set
    End Property

    Private _FechaPago As Date
    Public Property FechaPago() As Date
        Get
            Return _FechaPago
        End Get
        Set(ByVal value As Date)
            _FechaPago = value
        End Set
    End Property

    Private _FechaEnvio As Date
    Public Property FechaEnvio() As Date
        Get
            Return _FechaEnvio
        End Get
        Set(ByVal value As Date)
            _FechaEnvio = value
        End Set
    End Property

    Private _Porcentaje As Single
    Public Property Porcentaje() As Single
        Get
            Return _Porcentaje
        End Get
        Set(ByVal value As Single)
            _Porcentaje = value
        End Set
    End Property

    Private _MontoMaximo As Double
    Public Property MontoMaximo() As Double
        Get
            Return _MontoMaximo
        End Get
        Set(ByVal value As Double)
            _MontoMaximo = value
        End Set
    End Property

    Private _TCambio As Single
    Public Property TCambio() As Single
        Get
            Return _TCambio
        End Get
        Set(ByVal value As Single)
            _TCambio = value
        End Set
    End Property

    Private _Observaciones As String
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

    Private _CtaCte As String
    Public Property CtaCte() As String
        Get
            Return _CtaCte
        End Get
        Set(ByVal value As String)
            _CtaCte = value
        End Set
    End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    Private _IDEstado As String
    Public Property IDEstado() As String
        Get
            Return _IDEstado
        End Get
        Set(ByVal value As String)
            _IDEstado = value
        End Set
    End Property

    Private _Numeroformapago As String
    Public Property Numeroformapago() As String
        Get
            Return _Numeroformapago
        End Get
        Set(ByVal value As String)
            _Numeroformapago = value
        End Set
    End Property

    Private _SsSaldo As Double
    Public Property SsSaldo() As Double
        Get
            Return _SsSaldo
        End Get
        Set(ByVal value As Double)
            _SsSaldo = value
        End Set
    End Property

    Private _SsDifAceptada As Double
    Public Property SsDifAceptada() As Double
        Get
            Return _SsDifAceptada
        End Get
        Set(ByVal value As Double)
            _SsDifAceptada = value
        End Set
    End Property

    Private _TxObsDocumento As String
    Public Property TxObsDocumento() As String
        Get
            Return _TxObsDocumento
        End Get
        Set(ByVal value As String)
            _TxObsDocumento = value
        End Set
    End Property

    Private _FlDesdeOtraForEgreso As Boolean
    Public Property FlDesdeOtraForEgreso() As Boolean
        Get
            Return _FlDesdeOtraForEgreso
        End Get
        Set(ByVal value As Boolean)
            _FlDesdeOtraForEgreso = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _SsDetraccion As Double
    Public Property SsDetraccion() As Double
        Get
            Return _SsDetraccion
        End Get
        Set(ByVal value As Double)
            _SsDetraccion = value
        End Set
    End Property

    Private _ListOrdenesPago As List(Of clsOrdenPagoBE)
    Public Property ListOrdenesPago() As List(Of clsOrdenPagoBE)
        Get
            Return _ListOrdenesPago
        End Get
        Set(ByVal value As List(Of clsOrdenPagoBE))
            _ListOrdenesPago = value
        End Set
    End Property

    Private _ListDetalleOrdenPago As List(Of clsDetalleOrdenPagoBE)
    Public Property ListDetalleOrdenPago() As List(Of clsDetalleOrdenPagoBE)
        Get
            Return _ListDetalleOrdenPago
        End Get
        Set(ByVal value As List(Of clsDetalleOrdenPagoBE))
            _ListDetalleOrdenPago = value
        End Set
    End Property

    Private _ListaDetalleLiquidacion As List(Of clsDetalleLiquidacionBE)
    Public Property ListaDetalleLiquidacion() As List(Of clsDetalleLiquidacionBE)
        Get
            Return _ListaDetalleLiquidacion
        End Get
        Set(ByVal value As List(Of clsDetalleLiquidacionBE))
            _ListaDetalleLiquidacion = value
        End Set
    End Property

    Private _ListaAdjuntosOrdenPago As List(Of clsOrdenPago_AdjuntosBE)
    Public Property ListaAdjuntosOrdenPago() As List(Of clsOrdenPago_AdjuntosBE)
        Get
            Return _ListaAdjuntosOrdenPago
        End Get
        Set(ByVal value As List(Of clsOrdenPago_AdjuntosBE))
            _ListaAdjuntosOrdenPago = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleOrdenPagoBE
    Inherits clsOrdenPagoBE

    Private _IDOrdPag_Det As Integer
    Public Property IDOrdPag_Det() As Integer
        Get
            Return _IDOrdPag_Det
        End Get
        Set(ByVal value As Integer)
            _IDOrdPag_Det = value
        End Set
    End Property

    Private _IDReserva_Det As Integer
    Public Property IDReserva_Det() As Integer
        Get
            Return _IDReserva_Det
        End Get
        Set(ByVal value As Integer)
            _IDReserva_Det = value
        End Set
    End Property

    Private _TxDescripcion As String
    Public Property TxDescripcion() As String
        Get
            Return _TxDescripcion
        End Get
        Set(ByVal value As String)
            _TxDescripcion = value
        End Set
    End Property

    Private _IDServicio_Det As Integer
    Public Property IDServicio_Det() As Integer
        Get
            Return _IDServicio_Det
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det = value
        End Set
    End Property

    Private _NroPax As Int16
    Public Property NroPax() As Int16
        Get
            Return _NroPax
        End Get
        Set(ByVal value As Int16)
            _NroPax = value
        End Set
    End Property

    Private _NroLiberados As Int16
    Public Property NroLiberados() As Int16
        Get
            Return _NroLiberados
        End Get
        Set(ByVal value As Int16)
            _NroLiberados = value
        End Set
    End Property

    Private _Cantidad As Byte
    Public Property Cantidad() As Byte
        Get
            Return _Cantidad
        End Get
        Set(ByVal value As Byte)
            _Cantidad = value
        End Set
    End Property

    Private _Noches As Byte
    Public Property Noches() As Byte
        Get
            Return _Noches
        End Get
        Set(ByVal value As Byte)
            _Noches = value
        End Set
    End Property

    Private _Total As Double
    Public Property Total() As Double
        Get
            Return _Total
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property

    Private _SSDetraccion As Double
    Public Property SSDetraccion_Det() As Double
        Get
            Return _SSDetraccion
        End Get
        Set(ByVal value As Double)
            _SSDetraccion = value
        End Set
    End Property

    Private _SSOtrosDescuentos As Double
    Public Property SSOtrosDescuentos() As Double
        Get
            Return _SSOtrosDescuentos
        End Get
        Set(ByVal value As Double)
            _SSOtrosDescuentos = value
        End Set
    End Property

    Private _IDTransporte As Integer
    Public Property IDTransporte() As Integer
        Get
            Return _IDTransporte
        End Get
        Set(ByVal value As Integer)
            _IDTransporte = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenPago_AdjuntosBE
    Inherits clsAdministracionBaseBE

    Private _NuOrdenAdjunto As Integer
    Public Property NuOrdenAdjunto() As Integer
        Get
            Return _NuOrdenAdjunto
        End Get
        Set(ByVal value As Integer)
            _NuOrdenAdjunto = value
        End Set
    End Property

    Private _IDOrdPago As Integer
    Public Property IDOrdPago() As Integer
        Get
            Return _IDOrdPago
        End Get
        Set(ByVal value As Integer)
            _IDOrdPago = value
        End Set
    End Property

    Private _NoArchivo As String
    Public Property NoArchivo() As String
        Get
            Return _NoArchivo
        End Get
        Set(ByVal value As String)
            _NoArchivo = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenCompraBE
    Inherits clsAdministracionBaseBE
    Private _NuOrdComInt As Integer
    Public Property NuOrdComInt() As Integer
        Get
            Return _NuOrdComInt
        End Get
        Set(ByVal value As Integer)
            _NuOrdComInt = value
        End Set
    End Property

    Private _NuOrdCom As String
    Public Property NuOrdCom() As String
        Get
            Return _NuOrdCom
        End Get
        Set(ByVal value As String)
            _NuOrdCom = value
        End Set
    End Property

    Private _CoProveedor As String
    Public Property CoProveedor() As String
        Get
            Return _CoProveedor
        End Get
        Set(ByVal value As String)
            _CoProveedor = value
        End Set
    End Property
    Private _FeOrdCom As Date
    Public Property FeOrdCom() As Date
        Get
            Return _FeOrdCom
        End Get
        Set(ByVal value As Date)
            _FeOrdCom = value
        End Set
    End Property
    Private _CoMoneda As String
    Public Property CoMoneda() As String
        Get
            Return _CoMoneda
        End Get
        Set(ByVal value As String)
            _CoMoneda = value
        End Set
    End Property

    Private _SsImpuestos As Double
    Public Property SsImpuestos() As Double
        Get
            Return _SsImpuestos
        End Get
        Set(ByVal value As Double)
            _SsImpuestos = value
        End Set
    End Property
    Private _SsTotal As Double
    Public Property SsTotal() As Double
        Get
            Return _SsTotal
        End Get
        Set(ByVal value As Double)
            _SsTotal = value
        End Set
    End Property
    Private _SsSaldo As Double
    Public Property SsSaldo() As Double
        Get
            Return _SsSaldo
        End Get
        Set(ByVal value As Double)
            _SsSaldo = value
        End Set
    End Property
    Private _SsDifAceptada As Double
    Public Property SsDifAceptada() As Double
        Get
            Return _SsDifAceptada
        End Get
        Set(ByVal value As Double)
            _SsDifAceptada = value
        End Set
    End Property
    Private _TxObsDocumento As String
    Public Property TxObsDocumento() As String
        Get
            Return _TxObsDocumento
        End Get
        Set(ByVal value As String)
            _TxObsDocumento = value
        End Set
    End Property
    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _CoEstado_OC As String
    Public Property CoEstado_OC() As String
        Get
            Return _CoEstado_OC
        End Get
        Set(ByVal value As String)
            _CoEstado_OC = value
        End Set
    End Property

    Private _FlDesdeOtraForEgreso As Boolean
    Public Property FlDesdeOtraForEgreso() As Boolean
        Get
            Return _FlDesdeOtraForEgreso
        End Get
        Set(ByVal value As Boolean)
            _FlDesdeOtraForEgreso = value
        End Set
    End Property

    '------------------------

    Private _TxDescripcion As String
    Public Property TxDescripcion() As String
        Get
            Return _TxDescripcion
        End Get
        Set(ByVal Value As String)
            _TxDescripcion = Value
        End Set
    End Property


    Private _CoArea As Byte
    Public Property CoArea() As Byte
        Get
            Return _CoArea
        End Get
        Set(ByVal Value As Byte)
            _CoArea = Value
        End Set
    End Property

    Private _IDContacto As String
    Public Property IDContacto() As String
        Get
            Return _IDContacto
        End Get
        Set(ByVal Value As String)
            _IDContacto = Value
        End Set
    End Property

    Private _CoRubro As Byte
    Public Property CoRubro() As Byte
        Get
            Return _CoRubro
        End Get
        Set(ByVal Value As Byte)
            _CoRubro = Value
        End Set
    End Property

    Private _UserSolicitante As String
    Public Property UserSolicitante() As String
        Get
            Return _UserSolicitante
        End Get
        Set(ByVal Value As String)
            _UserSolicitante = Value
        End Set
    End Property

    Private _IDAdminist As String
    Public Property IDAdminist() As String
        Get
            Return _IDAdminist
        End Get
        Set(ByVal Value As String)
            _IDAdminist = Value
        End Set
    End Property

    Private _CoCeCos As String
    Public Property CoCeCos() As String
        Get
            Return _CoCeCos
        End Get
        Set(ByVal Value As String)
            _CoCeCos = Value
        End Set
    End Property

    Private _SSDetraccion As Decimal
    Public Property SSDetraccion() As Decimal
        Get
            Return _SSDetraccion
        End Get
        Set(ByVal Value As Decimal)
            _SSDetraccion = Value
        End Set
    End Property

    Private _SSIGV As Decimal
    Public Property SSIGV() As Decimal
        Get
            Return _SSIGV
        End Get
        Set(ByVal Value As Decimal)
            _SSIGV = Value
        End Set
    End Property

    Private _SSSubTotal As Decimal
    Public Property SSSubTotal() As Decimal
        Get
            Return _SSSubTotal
        End Get
        Set(ByVal Value As Decimal)
            _SSSubTotal = Value
        End Set
    End Property

    Private _CoTipoDetraccion As String
    Public Property CoTipoDetraccion() As String
        Get
            Return _CoTipoDetraccion
        End Get
        Set(ByVal value As String)
            _CoTipoDetraccion = value
        End Set
    End Property

    Private _ListaDetalle As List(Of clsOrdenCompra_DetBE)
    Public Property ListaDetalle() As List(Of clsOrdenCompra_DetBE)
        Get
            Return _ListaDetalle
        End Get
        Set(ByVal value As List(Of clsOrdenCompra_DetBE))
            _ListaDetalle = value
        End Set
    End Property

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsOrdenCompra_DetBE
    Inherits clsAdministracionBaseBE
    Private _NuOrdComInt_Det As Int32
    Private _NuOrdComInt As Int32
    Private _TxServicio As String
    Private _SSCantidad As Decimal
    Private _SSPrecUnit As Decimal
    Private _SSTotal As Decimal
    Private _UserMod As String
    Private _FecMod As DateTime


    Public Property NuOrdComInt_Det() As Integer
        Get
            Return _NuOrdComInt_Det
        End Get
        Set(ByVal Value As Integer)
            _NuOrdComInt_Det = Value
        End Set
    End Property
    Public Property NuOrdComInt() As Int32
        Get
            Return _NuOrdComInt
        End Get
        Set(ByVal Value As Int32)
            _NuOrdComInt = Value
        End Set
    End Property
    Public Property TxServicio() As String
        Get
            Return _TxServicio
        End Get
        Set(ByVal Value As String)
            _TxServicio = Value
        End Set
    End Property
    Public Property SSCantidad() As Decimal
        Get
            Return _SSCantidad
        End Get
        Set(ByVal Value As Decimal)
            _SSCantidad = Value
        End Set
    End Property
    Public Property SSPrecUnit() As Decimal
        Get
            Return _SSPrecUnit
        End Get
        Set(ByVal Value As Decimal)
            _SSPrecUnit = Value
        End Set
    End Property
    Public Property SSTotal() As Decimal
        Get
            Return _SSTotal
        End Get
        Set(ByVal Value As Decimal)
            _SSTotal = Value
        End Set
    End Property
  
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsFondoFijoBE
    Inherits clsAdministracionBaseBE
    Private _NuFondoFijo As Integer
    Private _NuCodigo As String
    Private _CoCeCos As String
    Private _CtaContable As String
    Private _Descripcion As String
    Private _CoUserResponsable As String
    Private _CoMoneda As String
    Private _SSMonto As Decimal
    Private _CoEstado As String
    Private _SsSaldo As Decimal
    Private _SsDifAceptada As Decimal
    Private _TxObsDocumento As String
    Private _CoPrefijo As String
    Private _NuFondoFijo_Reembolso As Integer
    Private _CoAnio As String
    Private _UserNuevo As String
    Private _FlActivo As Boolean
    Private _FlDesdeOtraForEgreso As Boolean
    Private _CoTipoFondoFijo As String


    Public Property NuFondoFijo() As Integer
        Get
            Return _NuFondoFijo
        End Get
        Set(ByVal Value As Integer)
            _NuFondoFijo = Value
        End Set
    End Property
    Public Property NuCodigo() As String
        Get
            Return _NuCodigo
        End Get
        Set(ByVal Value As String)
            _NuCodigo = Value
        End Set
    End Property
    Public Property CoPrefijo() As String
        Get
            Return _CoPrefijo
        End Get
        Set(ByVal Value As String)
            _CoPrefijo = Value
        End Set
    End Property
    Public Property CoTipoFondoFijo() As String
        Get
            Return _CoTipoFondoFijo
        End Get
        Set(ByVal Value As String)
            _CoTipoFondoFijo = Value
        End Set
    End Property
    Public Property CoAnio() As String
        Get
            Return _CoAnio
        End Get
        Set(ByVal Value As String)
            _CoAnio = Value
        End Set
    End Property
    Public Property NuFondoFijo_Reembolso() As Integer
        Get
            Return _NuFondoFijo_Reembolso
        End Get
        Set(ByVal Value As Integer)
            _NuFondoFijo_Reembolso = Value
        End Set
    End Property
    Public Property CoCeCos() As String
        Get
            Return _CoCeCos
        End Get
        Set(ByVal Value As String)
            _CoCeCos = Value
        End Set
    End Property
    Public Property CtaContable() As String
        Get
            Return _CtaContable
        End Get
        Set(ByVal Value As String)
            _CtaContable = Value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal Value As String)
            _Descripcion = Value
        End Set
    End Property
    Public Property CoUserResponsable() As String
        Get
            Return _CoUserResponsable
        End Get
        Set(ByVal Value As String)
            _CoUserResponsable = Value
        End Set
    End Property
    Public Property CoMoneda() As String
        Get
            Return _CoMoneda
        End Get
        Set(ByVal Value As String)
            _CoMoneda = Value
        End Set
    End Property
    Public Property SSMonto() As Decimal
        Get
            Return _SSMonto
        End Get
        Set(ByVal Value As Decimal)
            _SSMonto = Value
        End Set
    End Property
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal Value As String)
            _CoEstado = Value
        End Set
    End Property
    Public Property SsSaldo() As Decimal
        Get
            Return _SsSaldo
        End Get
        Set(ByVal Value As Decimal)
            _SsSaldo = Value
        End Set
    End Property
    Public Property SsDifAceptada() As Decimal
        Get
            Return _SsDifAceptada
        End Get
        Set(ByVal Value As Decimal)
            _SsDifAceptada = Value
        End Set
    End Property
    Public Property TxObsDocumento() As String
        Get
            Return _TxObsDocumento
        End Get
        Set(ByVal Value As String)
            _TxObsDocumento = Value
        End Set
    End Property
    Public Property UserNuevo() As String
        Get
            Return _UserNuevo
        End Get
        Set(ByVal Value As String)
            _UserNuevo = Value
        End Set
    End Property
    Public Property FlActivo() As Boolean
        Get
            Return _FlActivo
        End Get
        Set(ByVal Value As Boolean)
            _FlActivo = Value
        End Set
    End Property

    Public Property FlDesdeOtraForEgreso() As Boolean
        Get
            Return _FlDesdeOtraForEgreso
        End Get
        Set(ByVal value As Boolean)
            _FlDesdeOtraForEgreso = value
        End Set
    End Property

End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsRubroBE
    Inherits clsAdministracionBaseBE

    Private _CoRubro As Byte
    Private _NoRubro As String
    Private _FlActivo As Boolean
    Private _UserMod As String
    Private _FecMod As DateTime
    Private _FlSAP As Boolean
    Private _CoSAP As Byte


    Public Property CoRubro() As Byte
        Get
            Return _CoRubro
        End Get
        Set(ByVal Value As Byte)
            _CoRubro = Value
        End Set
    End Property
    Public Property NoRubro() As String
        Get
            Return _NoRubro
        End Get
        Set(ByVal Value As String)
            _NoRubro = Value
        End Set
    End Property
    Public Property FlActivo() As Boolean
        Get
            Return _FlActivo
        End Get
        Set(ByVal Value As Boolean)
            _FlActivo = Value
        End Set
    End Property

    Public Property FlSAP() As Boolean
        Get
            Return _FlSAP
        End Get
        Set(ByVal Value As Boolean)
            _FlSAP = Value
        End Set
    End Property
    Public Property CoSAP() As Byte
        Get
            Return _CoSAP
        End Get
        Set(ByVal Value As Byte)
            _CoSAP = Value
        End Set
    End Property


End Class




<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsCentroCostosBE
    Inherits clsAdministracionBaseBE

    Private _CoCeCos As String
    Private _Descripcion As String
    Private _CoCtaGasto As String
    Private _CoCtaIngreso As String
    Private _FlActivo As Boolean
    Private _UserMod As String
    Private _FecMod As DateTime


    Public Property CoCeCos() As String
        Get
            Return _CoCeCos
        End Get
        Set(ByVal Value As String)
            _CoCeCos = Value
        End Set
    End Property
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal Value As String)
            _Descripcion = Value
        End Set
    End Property
    Public Property CoCtaGasto() As String
        Get
            Return _CoCtaGasto
        End Get
        Set(ByVal Value As String)
            _CoCtaGasto = Value
        End Set
    End Property
    Public Property CoCtaIngreso() As String
        Get
            Return _CoCtaIngreso
        End Get
        Set(ByVal Value As String)
            _CoCtaIngreso = Value
        End Set
    End Property
    Public Property FlActivo() As Boolean
        Get
            Return _FlActivo
        End Get
        Set(ByVal Value As Boolean)
            _FlActivo = Value
        End Set
    End Property
   
End Class



<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsVoucherOficinaExternaBE
    Inherits clsOrdenCompraBE
    Private _NuVouOfiExtInt As Integer
    Public Property NuVouOfiExtInt() As Integer
        Get
            Return _NuVouOfiExtInt
        End Get
        Set(ByVal value As Integer)
            _NuVouOfiExtInt = value
        End Set
    End Property

    Private _IdCab As Integer
    Public Property IdCab() As Integer
        Get
            Return _IdCab
        End Get
        Set(ByVal value As Integer)
            _IdCab = value
        End Set
    End Property

    Private _FeVoucher As Date
    Public Property FeVoucher() As Date
        Get
            Return _FeVoucher
        End Get
        Set(ByVal value As Date)
            _FeVoucher = value
        End Set
    End Property
    
    Private _CoUbigeo_Oficina As String
    Public Property CoUbigeo_Oficina() As String
        Get
            Return _CoUbigeo_Oficina
        End Get
        Set(ByVal value As String)
            _CoUbigeo_Oficina = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsVoucherProveedorTrenBE
    Inherits clsVoucherOficinaExternaBE

    Private _NuVouPTrenInt As Integer
    Public Property NuVouPTrenInt() As Integer
        Get
            Return _NuVouPTrenInt
        End Get
        Set(ByVal value As Integer)
            _NuVouPTrenInt = value
        End Set
    End Property
End Class



<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDebitMemoBE
    Inherits clsAdministracionBaseBE

    Private _IDDebitMemo As String
    Public Property IDDebitMemo() As String
        Get
            Return _IDDebitMemo
        End Get
        Set(ByVal value As String)
            _IDDebitMemo = value
        End Set
    End Property

    Private _IDSerie As String
    Public Property IDSerie() As String
        Get
            Return _IDSerie
        End Get
        Set(ByVal value As String)
            _IDSerie = value
        End Set
    End Property

    Private _Fecha As Date
    Public Property Fecha() As Date
        Get
            Return _Fecha
        End Get
        Set(ByVal value As Date)
            _Fecha = value
        End Set
    End Property

    Private _FecVencim As Date
    Public Property FecVencim() As Date
        Get
            Return _FecVencim
        End Get
        Set(ByVal value As Date)
            _FecVencim = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _IDCliente As String
    Public Property IDCliente() As String
        Get
            Return _IDCliente
        End Get
        Set(ByVal value As String)
            _IDCliente = value
        End Set
    End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    Private _IDFile As String
    Public Property IDFile() As String
        Get
            Return _IDFile
        End Get
        Set(ByVal value As String)
            _IDFile = value
        End Set
    End Property


    Private _SubTotal As Double
    Public Property SubTotal() As Double
        Get
            Return _SubTotal
        End Get
        Set(ByVal value As Double)
            _SubTotal = value
        End Set
    End Property

    Private _TotalIGV As Double
    Public Property TotalIGV() As Double
        Get
            Return _TotalIGV
        End Get
        Set(ByVal value As Double)
            _TotalIGV = value
        End Set
    End Property

    Private _Total As Double
    Public Property Total() As Double
        Get
            Return _Total
        End Get
        Set(ByVal value As Double)
            _Total = value
        End Set
    End Property
    Private _IDBanco As String
    Public Property IDBanco() As String
        Get
            Return _IDBanco
        End Get
        Set(ByVal value As String)
            _IDBanco = value
        End Set
    End Property

    Private _IDEstado As String
    Public Property IDEstado() As String
        Get
            Return _IDEstado
        End Get
        Set(ByVal value As String)
            _IDEstado = value
        End Set
    End Property

    Private _IDTipo As Char
    Public Property IDTipo() As Char
        Get
            Return _IDTipo
        End Get
        Set(ByVal value As Char)
            _IDTipo = value
        End Set
    End Property


    Private _FechaIn As Date
    Public Property FechaIn() As Date
        Get
            Return _FechaIn
        End Get
        Set(ByVal value As Date)
            _FechaIn = value
        End Set
    End Property

    Private _FechaOut As Date
    Public Property FechaOut() As Date
        Get
            Return _FechaOut
        End Get
        Set(ByVal value As Date)
            _FechaOut = value
        End Set
    End Property

    Private _Cliente As String
    Public Property Cliente() As String
        Get
            Return _Cliente
        End Get
        Set(ByVal value As String)
            _Cliente = value
        End Set
    End Property

    Private _Pax As Byte
    Public Property Pax() As Byte
        Get
            Return _Pax
        End Get
        Set(ByVal value As Byte)
            _Pax = value
        End Set
    End Property

    Private _Titulo As String
    Public Property Titulo() As String
        Get
            Return _Titulo
        End Get
        Set(ByVal value As String)
            _Titulo = value
        End Set
    End Property

    Private _Responsable As String
    Public Property Responsable() As String
        Get
            Return _Responsable
        End Get
        Set(ByVal value As String)
            _Responsable = value
        End Set
    End Property

    Private _FlImprimioDebit As Boolean
    Public Property FlImprimioDebit() As Boolean
        Get
            Return _FlImprimioDebit
        End Get
        Set(ByVal value As Boolean)
            _FlImprimioDebit = value
        End Set
    End Property

    Private _FlManual As Boolean
    Public Property FlManual() As Boolean
        Get
            Return _FlManual
        End Get
        Set(ByVal value As Boolean)
            _FlManual = value
        End Set
    End Property

    Private _DebitMemoResumen As String
    Public Property DebitMemoResumen() As String
        Get
            Return _DebitMemoResumen
        End Get
        Set(ByVal value As String)
            _DebitMemoResumen = value
        End Set
    End Property

    Private _Saldo As Double
    Public Property Saldo() As Double
        Get
            Return _Saldo
        End Get
        Set(ByVal value As Double)
            _Saldo = value
        End Set
    End Property

    Private _FlFacturado As Boolean
    Public Property FlFacturado() As Boolean
        Get
            Return _FlFacturado
        End Get
        Set(ByVal value As Boolean)
            _FlFacturado = value
        End Set
    End Property

    Private _IDMotivo As Integer
    Public Property IDMotivo() As Double
        Get
            Return _IDMotivo
        End Get
        Set(ByVal value As Double)
            _IDMotivo = value
        End Set
    End Property

    Private _IDResponsable As String
    Public Property IDResponsable() As String
        Get
            Return _IDResponsable
        End Get
        Set(ByVal value As String)
            _IDResponsable = value
        End Set
    End Property

    Private _IDSupervisor As String
    Public Property IDSupervisor() As String
        Get
            Return _IDSupervisor
        End Get
        Set(ByVal value As String)
            _IDSupervisor = value
        End Set
    End Property

    Private _TxObservacion As String
    Public Property TxObservacion() As String
        Get
            Return _TxObservacion
        End Get
        Set(ByVal value As String)
            _TxObservacion = value
        End Set
    End Property

    Private _ListaDebitMemo As List(Of clsDebitMemoBE)
    Public Property ListaDebitMemo() As List(Of clsDebitMemoBE)
        Get
            Return _ListaDebitMemo
        End Get
        Set(ByVal value As List(Of clsDebitMemoBE))
            _ListaDebitMemo = value
        End Set
    End Property

    Private _ListaDetalleDebitMemo As List(Of clsDetalleDebitMemoBE)
    Public Property ListaDetalleDebitMemo() As List(Of clsDetalleDebitMemoBE)
        Get
            Return _ListaDetalleDebitMemo
        End Get
        Set(ByVal value As List(Of clsDetalleDebitMemoBE))
            _ListaDetalleDebitMemo = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoBE
    Inherits clsAdministracionBaseBE

    Private _NuFileLibre As String
    Public Property NuFileLibre() As String
        Get
            Return _NuFileLibre
        End Get
        Set(ByVal value As String)
            _NuFileLibre = value
        End Set
    End Property

    Private _CoCtroCosto As String
    Public Property CoCtroCosto() As String
        Get
            Return _CoCtroCosto
        End Get
        Set(ByVal value As String)
            _CoCtroCosto = value
        End Set
    End Property

    Private _CoCtaContable As String
    Public Property CoCtaContable() As String
        Get
            Return _CoCtaContable
        End Get
        Set(ByVal value As String)
            _CoCtaContable = value
        End Set
    End Property

    Private _FeEmisionVinc As Date
    Public Property FeEmisionVinc() As Date
        Get
            Return _FeEmisionVinc
        End Get
        Set(ByVal value As Date)
            _FeEmisionVinc = value
        End Set
    End Property

    Private _NuDocum As String
    Public Property NuDocum() As String
        Get
            Return _NuDocum
        End Get
        Set(ByVal value As String)
            _NuDocum = value
        End Set
    End Property

    Private _CoSerie As String
    Public Property CoSerie() As String
        Get
            Return _CoSerie
        End Get
        Set(ByVal value As String)
            _CoSerie = value
        End Set
    End Property

    Private _IDTipoDoc As String
    Public Property IDTipoDoc() As String
        Get
            Return _IDTipoDoc
        End Get
        Set(ByVal value As String)
            _IDTipoDoc = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _FeDocum As Date
    Public Property FeDocum() As Date
        Get
            Return _FeDocum
        End Get
        Set(ByVal value As Date)
            _FeDocum = value
        End Set
    End Property

    'Private _IDProveedor As String
    'Public Property IDProveedor() As String
    '    Get
    '        Return _IDProveedor
    '    End Get
    '    Set(ByVal value As String)
    '        _IDProveedor = value
    '    End Set
    'End Property

    'Private _IDVoucher As Integer
    'Public Property IDVoucher() As Integer
    '    Get
    '        Return _IDVoucher
    '    End Get
    '    Set(ByVal value As Integer)
    '        _IDVoucher = value
    '    End Set
    'End Property

    'Private _NuOrdenServicio As String
    'Public Property NuOrdenServicio() As String
    '    Get
    '        Return _NuOrdenServicio
    '    End Get
    '    Set(ByVal value As String)
    '        _NuOrdenServicio = value
    '    End Set
    'End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    'Private _SsCompraNeto As Double
    'Public Property SsCompraNeto() As Double
    '    Get
    '        Return _SsCompraNeto
    '    End Get
    '    Set(ByVal value As Double)
    '        _SsCompraNeto = value
    '    End Set
    'End Property

    'Private _SsIGVCosto As Double
    'Public Property SsIGVCosto() As Double
    '    Get
    '        Return _SsIGVCosto
    '    End Get
    '    Set(ByVal value As Double)
    '        _SsIGVCosto = value
    '    End Set
    'End Property

    'Private _SsIGVSFE As Double
    'Public Property SsIGVSFE() As Double
    '    Get
    '        Return _SsIGVSFE
    '    End Get
    '    Set(ByVal value As Double)
    '        _SsIGVSFE = value
    '    End Set
    'End Property

    'Private _SsTotalCosto As Double
    'Public Property SsTotalCosto() As Double
    '    Get
    '        Return _SsTotalCosto
    '    End Get
    '    Set(ByVal value As Double)
    '        _SsTotalCosto = value
    '    End Set
    'End Property

    'Private _IDTipoOC As String
    'Public Property IDTipoOC() As String
    '    Get
    '        Return _IDTipoOC
    '    End Get
    '    Set(ByVal value As String)
    '        _IDTipoOC = value
    '    End Set
    'End Property

    'Private _SsMargen As Double
    'Public Property SsMargen() As Double
    '    Get
    '        Return _SsMargen
    '    End Get
    '    Set(ByVal value As Double)
    '        _SsMargen = value
    '    End Set
    'End Property

    'Private _SsTotalCostoUSD As String
    'Public Property SsTotalCostoUSD() As String
    '    Get
    '        Return _SsTotalCostoUSD
    '    End Get
    '    Set(ByVal value As String)
    '        _SsTotalCostoUSD = value
    '    End Set
    'End Property

    Private _SsTotalDocumUSD As Double
    Public Property SsTotalDocumUSD() As Double
        Get
            Return _SsTotalDocumUSD
        End Get
        Set(ByVal value As Double)
            _SsTotalDocumUSD = value
        End Set
    End Property

    Private _SsIGV As Double
    Public Property SsIGV() As Double
        Get
            Return _SsIGV
        End Get
        Set(ByVal value As Double)
            _SsIGV = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _Generado As Boolean
    Public Property Generado() As Boolean
        Get
            Return _Generado
        End Get
        Set(ByVal value As Boolean)
            _Generado = value
        End Set
    End Property

    Private _IDCliente As String
    Public Property IDCliente() As String
        Get
            Return _IDCliente
        End Get
        Set(ByVal value As String)
            _IDCliente = value
        End Set
    End Property

    Private _CoTipoVenta As String
    Public Property CoTipoVenta() As String
        Get
            Return _CoTipoVenta
        End Get
        Set(ByVal value As String)
            _CoTipoVenta = value
        End Set
    End Property

    Private _FlDocManual As Boolean
    Public Property FlDocManual() As Boolean
        Get
            Return _FlDocManual
        End Get
        Set(ByVal value As Boolean)
            _FlDocManual = value
        End Set
    End Property

    Private _TxTitulo As String
    Public Property TxTitulo() As String
        Get
            Return _TxTitulo
        End Get
        Set(ByVal value As String)
            _TxTitulo = value
        End Set
    End Property

    Private _NuDocumVinc As String
    Public Property NuDocumVinc() As String
        Get
            Return _NuDocumVinc
        End Get
        Set(ByVal value As String)
            _NuDocumVinc = value
        End Set
    End Property

    Private _FlPorAjustePrecio As Boolean
    Public Property FlPorAjustePrecio() As Boolean
        Get
            Return _FlPorAjustePrecio
        End Get
        Set(ByVal value As Boolean)
            _FlPorAjustePrecio = value
        End Set
    End Property

    Private _IDTipoDocVinc As String
    Public Property IDTipoDocVinc() As String
        Get
            Return _IDTipoDocVinc
        End Get
        Set(ByVal value As String)
            _IDTipoDocVinc = value
        End Set
    End Property

    Private _ErrorSetra As String
    Public Property ErrorSetra() As String
        Get
            Return _ErrorSetra
        End Get
        Set(ByVal value As String)
            _ErrorSetra = value
        End Set
    End Property

    Private _ErrorSAP As String
    Public Property ErrorSAP() As String
        Get
            Return _ErrorSAP
        End Get
        Set(ByVal value As String)
            _ErrorSAP = value
        End Set
    End Property

    Private _CoDebitMemoAnticipo As String
    Public Property CoDebitMemoAnticipo() As String
        Get
            If _CoDebitMemoAnticipo Is Nothing Then
                Return ""
            Else
                Return _CoDebitMemoAnticipo
            End If

        End Get
        Set(ByVal value As String)
            _CoDebitMemoAnticipo = value
        End Set
    End Property

    Private _FlVentaAdicional As Boolean
    Public Property FlVentaAdicional() As Boolean
        Get
            Return _FlVentaAdicional
        End Get
        Set(ByVal value As Boolean)
            _FlVentaAdicional = value
        End Set
    End Property


    Private _blnSAP As Boolean
    Public Property blnSAP() As Boolean
        Get
            Return _blnSAP
        End Get
        Set(ByVal value As Boolean)
            _blnSAP = value
        End Set
    End Property

    Private _CoSAP As String
    Public Property CoSAP() As String
        Get
            Return _CoSAP
        End Get
        Set(ByVal value As String)
            _CoSAP = value
        End Set
    End Property


    Private _blnExistenteenSetra As Boolean
    Public Property blnExistenteenSetra() As Boolean
        Get
            Return _blnExistenteenSetra
        End Get
        Set(ByVal value As Boolean)
            _blnExistenteenSetra = value
        End Set
    End Property


    Private _ListaDocumentos As List(Of clsDocumentoBE)
    Public Property ListaDocumentos() As List(Of clsDocumentoBE)
        Get
            Return _ListaDocumentos
        End Get
        Set(ByVal value As List(Of clsDocumentoBE))
            _ListaDocumentos = value
        End Set
    End Property

    Private _ListaDetalleDocumentos As List(Of clsDocumentoDetBE)
    Public Property ListaDetalleDocumentos() As List(Of clsDocumentoDetBE)
        Get
            Return _ListaDetalleDocumentos
        End Get
        Set(ByVal value As List(Of clsDocumentoDetBE))
            _ListaDetalleDocumentos = value
        End Set
    End Property

    Private _ListaDetalleDocumentos_Texto As List(Of clsDocumentoDet_TextoBE)
    Public Property ListaDetalleDocumentos_Texto() As List(Of clsDocumentoDet_TextoBE)
        Get
            Return _ListaDetalleDocumentos_Texto
        End Get
        Set(ByVal value As List(Of clsDocumentoDet_TextoBE))
            _ListaDetalleDocumentos_Texto = value
        End Set
    End Property

    Private _ListaDebitMemos As List(Of clsDebitMemoBE)
    Public Property ListaDebitMemos() As List(Of clsDebitMemoBE)
        Get
            Return _ListaDebitMemos
        End Get
        Set(ByVal value As List(Of clsDebitMemoBE))
            _ListaDebitMemos = value
        End Set
    End Property

    'jorge
    Private _NuNtaVta As String
    Public Property NuNtaVta() As String
        Get
            Return _NuNtaVta
        End Get
        Set(ByVal value As String)
            _NuNtaVta = value
        End Set
    End Property

    'jorge
    Private _NuItem_DM As Integer
    Public Property NuItem_DM() As Integer
        Get
            Return _NuItem_DM
        End Get
        Set(ByVal value As Integer)
            _NuItem_DM = value
        End Set
    End Property
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoDetBE
    Inherits clsAdministracionBaseBE

    Private _NuDocum As String
    Public Property NuDocum() As String
        Get
            Return _NuDocum
        End Get
        Set(ByVal value As String)
            _NuDocum = value
        End Set
    End Property

    Private _CoSerie As String
    Public Property CoSerie() As String
        Get
            Return _CoSerie
        End Get
        Set(ByVal value As String)
            _CoSerie = value
        End Set
    End Property

    Private _IDTipoDoc As String
    Public Property IDTipoDoc() As String
        Get
            Return _IDTipoDoc
        End Get
        Set(ByVal value As String)
            _IDTipoDoc = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _NuDetalle As Byte
    Public Property NuDetalle() As Byte
        Get
            Return _NuDetalle
        End Get
        Set(ByVal value As Byte)
            _NuDetalle = value
        End Set
    End Property

    Private _FeDocum As Date
    Public Property FeDocum() As Date
        Get
            Return _FeDocum
        End Get
        Set(ByVal value As Date)
            _FeDocum = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    Private _IDVoucher As String
    Public Property IDVoucher() As String
        Get
            Return _IDVoucher
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then
                _IDVoucher = value
            Else
                _IDVoucher = ""
            End If

        End Set
    End Property

    Private _NuOrdenServicio As String
    Public Property NuOrdenServicio() As String
        Get
            Return _NuOrdenServicio
        End Get
        Set(ByVal value As String)
            If Not value Is Nothing Then
                _NuOrdenServicio = value
            Else
                _NuOrdenServicio = ""
            End If
        End Set
    End Property

    Private _IDMonedaCosto As String
    Public Property IDMonedaCosto() As String
        Get
            Return _IDMonedaCosto
        End Get
        Set(ByVal value As String)
            _IDMonedaCosto = value
        End Set
    End Property

    Private _SsCompraNeto As Double
    Public Property SsCompraNeto() As Double
        Get
            Return _SsCompraNeto
        End Get
        Set(ByVal value As Double)
            _SsCompraNeto = value
        End Set
    End Property

    Private _SsIGVCosto As Double
    Public Property SsIGVCosto() As Double
        Get
            Return _SsIGVCosto
        End Get
        Set(ByVal value As Double)
            _SsIGVCosto = value
        End Set
    End Property

    Private _SsIGVSFE As Double
    Public Property SsIGVSFE() As Double
        Get
            Return _SsIGVSFE
        End Get
        Set(ByVal value As Double)
            _SsIGVSFE = value
        End Set
    End Property

    Private _SsTotalCosto As Double
    Public Property SsTotalCosto() As Double
        Get
            Return _SsTotalCosto
        End Get
        Set(ByVal value As Double)
            _SsTotalCosto = value
        End Set
    End Property

    Private _IDTipoOC As String
    Public Property IDTipoOC() As String
        Get
            Return _IDTipoOC
        End Get
        Set(ByVal value As String)
            _IDTipoOC = value
        End Set
    End Property

    Private _SsMargen As Double
    Public Property SsMargen() As Double
        Get
            Return _SsMargen
        End Get
        Set(ByVal value As Double)
            _SsMargen = value
        End Set
    End Property

    Private _SsTotalCostoUSD As Double
    Public Property SsTotalCostoUSD() As Double
        Get
            Return _SsTotalCostoUSD
        End Get
        Set(ByVal value As Double)
            _SsTotalCostoUSD = value
        End Set
    End Property

    Private _SsTotalDocumUSD As Double
    Public Property SsTotalDocumUSD() As Double
        Get
            Return _SsTotalDocumUSD
        End Get
        Set(ByVal value As Double)
            _SsTotalDocumUSD = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _CoTipoDet As String
    Public Property CoTipoDet() As String
        Get
            Return _CoTipoDet
        End Get
        Set(ByVal value As String)
            _CoTipoDet = value
        End Set
    End Property

    Private _NoServicio As String
    Public Property NoServicio() As String
        Get
            Return _NoServicio
        End Get
        Set(ByVal value As String)
            _NoServicio = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoDet_TextoBE
    Inherits clsAdministracionBaseBE

    Private _NuDocum As String
    Public Property NuDocum() As String
        Get
            Return _NuDocum
        End Get
        Set(ByVal value As String)
            _NuDocum = value
        End Set
    End Property

    Private _IDTipoDoc As String
    Public Property IDTipoDoc() As String
        Get
            Return _IDTipoDoc
        End Get
        Set(ByVal value As String)
            _IDTipoDoc = value
        End Set
    End Property

    Private _NuDetalle As Byte
    Public Property NuDetalle() As Byte
        Get
            Return _NuDetalle
        End Get
        Set(ByVal value As Byte)
            _NuDetalle = value
        End Set
    End Property

    Private _NoTexto As String
    Public Property NoTexto() As String
        Get
            Return _NoTexto
        End Get
        Set(ByVal value As String)
            _NoTexto = value
        End Set
    End Property

    Private _SsTotal As Double
    Public Property SsTotal() As Double
        Get
            Return _SsTotal
        End Get
        Set(ByVal value As Double)
            _SsTotal = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleDebitMemoBE
    Inherits clsDebitMemoBE

    Private _IDDebitMemoDet As Byte
    Public Property IDDebitMemoDet() As Byte
        Get
            Return _IDDebitMemoDet
        End Get
        Set(ByVal value As Byte)
            _IDDebitMemoDet = value
        End Set
    End Property

    Private _Descripcion As String
    Public Property Descripcion() As String
        Get
            Return _Descripcion
        End Get
        Set(ByVal value As String)
            _Descripcion = value
        End Set
    End Property

    Private _CapacidadHab As String
    Public Property CapacidadHab() As String
        Get
            Return _CapacidadHab
        End Get
        Set(ByVal value As String)
            _CapacidadHab = value
        End Set
    End Property

    Private _CostoPersona As Double
    Public Property CostoPersona() As Double
        Get
            Return _CostoPersona
        End Get
        Set(ByVal value As Double)
            _CostoPersona = value
        End Set
    End Property

    Private _NroPax As Byte
    Public Property NroPax() As Byte
        Get
            Return _NroPax
        End Get
        Set(ByVal value As Byte)
            _NroPax = value
        End Set
    End Property

    Private _IDTipoOC As String
    Public Property IDTipoOC() As String
        Get
            Return _IDTipoOC
        End Get
        Set(ByVal value As String)
            _IDTipoOC = value
        End Set
    End Property


End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsNotaVentaBE
    Inherits clsDebitMemoBE

    Private _IDNotaVenta As String
    Public Property IDNotaVenta() As String
        Get
            Return _IDNotaVenta
        End Get
        Set(ByVal value As String)
            _IDNotaVenta = value
        End Set
    End Property

    Private _ListaDetalleNotaVenta As List(Of clsDetalleNotaVentaBE)
    Public Property ListaDetalleNotaVenta() As List(Of clsDetalleNotaVentaBE)
        Get
            Return _ListaDetalleNotaVenta
        End Get
        Set(ByVal value As List(Of clsDetalleNotaVentaBE))
            _ListaDetalleNotaVenta = value
        End Set
    End Property



End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleNotaVentaBE
    Inherits clsDetalleDebitMemoBE

    Private _IDNotaVenta As String
    Public Property IDNotaVenta() As String
        Get
            Return _IDNotaVenta
        End Get
        Set(ByVal value As String)
            _IDNotaVenta = value
        End Set
    End Property

    Private _IDNotaVentaDet As Byte
    Public Property IDNotaVentaDet() As Byte
        Get
            Return _IDNotaVentaDet
        End Get
        Set(ByVal value As Byte)
            _IDNotaVentaDet = value
        End Set
    End Property

    Private _IDReserva_Det As Integer
    Public Property IDReserva_Det() As Integer
        Get
            Return _IDReserva_Det
        End Get
        Set(ByVal value As Integer)
            _IDReserva_Det = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPreLiquidacionBE
    Inherits clsNotaVentaBE

    Private _ListaDetallePreLiquidacion As List(Of clsDetallePreLiquidacionBE)
    Public Property ListaDetallePreLiquidacion() As List(Of clsDetallePreLiquidacionBE)
        Get
            Return _ListaDetallePreLiquidacion
        End Get
        Set(ByVal value As List(Of clsDetallePreLiquidacionBE))
            _ListaDetallePreLiquidacion = value
        End Set
    End Property


End Class



<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetallePreLiquidacionBE
    Inherits clsDetalleNotaVentaBE

    Private _IDPreLiquiDet As Byte
    Public Property IDPreLiquiDet() As Byte
        Get
            Return _IDPreLiquiDet
        End Get
        Set(ByVal value As Byte)
            _IDPreLiquiDet = value
        End Set
    End Property

    Private _IDOperacion_Det As String
    Public Property IDOperacion_Det() As String
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As String)
            _IDOperacion_Det = value
        End Set
    End Property
End Class


<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleLiquidacionBE

    Private _IDReserva_Det As Integer
    Public Property IDReserva_Det() As Integer
        Get
            Return _IDReserva_Det
        End Get
        Set(ByVal value As Integer)
            _IDReserva_Det = value
        End Set
    End Property

    Private _IDServicio_Det As Integer
    Public Property IDServicio_Det() As Integer
        Get
            Return _IDServicio_Det
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det = value
        End Set
    End Property

    Private _IDOrdPag_Det As Integer
    Public Property IDOrdPag_Det() As Integer
        Get
            Return _IDOrdPag_Det
        End Get
        Set(ByVal value As Integer)
            _IDOrdPag_Det = value
        End Set
    End Property

    Private _Cantidad As Byte
    Public Property Cantidad() As Byte
        Get
            Return _Cantidad
        End Get
        Set(ByVal value As Byte)
            _Cantidad = value
        End Set
    End Property

    Private _DescServicioDet As String
    Public Property DescServicioDet() As String
        Get
            Return _DescServicioDet
        End Get
        Set(ByVal value As String)
            _DescServicioDet = value
        End Set
    End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    Private _SimboloMoneda As String
    Public Property SimboloMoneda() As String
        Get
            Return _SimboloMoneda
        End Get
        Set(ByVal value As String)
            _SimboloMoneda = value
        End Set
    End Property

    Private _TipoCambio As Single
    Public Property TipoCambio() As Single
        Get
            Return _TipoCambio
        End Get
        Set(ByVal value As Single)
            _TipoCambio = value
        End Set
    End Property

    Private _TotalHab As Double
    Public Property TotalHab() As Double
        Get
            Return _TotalHab
        End Get
        Set(ByVal value As Double)
            _TotalHab = value
        End Set
    End Property

    Private _Noches As Byte
    Public Property Noches() As Byte
        Get
            Return _Noches
        End Get
        Set(ByVal value As Byte)
            _Noches = value
        End Set
    End Property

    Private _NroPax As Int16
    Public Property NroPax() As Int16
        Get
            Return _NroPax
        End Get
        Set(ByVal value As Int16)
            _NroPax = value
        End Set
    End Property

    Private _NroLiberados As Int16
    Public Property NroLiberados() As Int16
        Get
            Return _NroLiberados
        End Get
        Set(ByVal value As Int16)
            _NroLiberados = value
        End Set
    End Property


    Private _TotalGen As Double
    Public Property TotalGen() As Double
        Get
            Return _TotalGen
        End Get
        Set(ByVal value As Double)
            _TotalGen = value
        End Set
    End Property

    Private _SaldoxPagar As Double
    Public Property SaldoxPagar() As Double
        Get
            Return _SaldoxPagar
        End Get
        Set(ByVal value As Double)
            _SaldoxPagar = value
        End Set
    End Property


    Private _TotalLiq As Double
    Public Property TotalLiq() As Double
        Get
            Return _TotalLiq
        End Get
        Set(ByVal value As Double)
            _TotalLiq = value
        End Set
    End Property

    Private _IDTransporte As Integer
    Public Property IDTransporte() As Integer
        Get
            Return _IDTransporte
        End Get
        Set(ByVal value As Integer)
            _IDTransporte = value
        End Set
    End Property

    Private _SaldoPagado As Double
    Public Property SaldoPagado() As Double
        Get
            Return _SaldoPagado
        End Get
        Set(ByVal value As Double)
            _SaldoPagado = value
        End Set
    End Property

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsReporteFacturacionContabilidadBE

    Private _NuCuenta As String
    Public Property NuCuenta() As String
        Get
            Return _NuCuenta
        End Get
        Set(ByVal value As String)
            _NuCuenta = value
        End Set
    End Property

    Private _Periodo As String
    Public Property Periodo() As String
        Get
            Return _Periodo
        End Get
        Set(ByVal value As String)
            _Periodo = value
        End Set
    End Property

    Private _SubDiario As String
    Public Property SubDiario() As String
        Get
            Return _SubDiario
        End Get
        Set(ByVal value As String)
            _SubDiario = value
        End Set
    End Property

    Private _NroRegistro As String
    Public Property NroRegistro() As String
        Get
            Return _NroRegistro
        End Get
        Set(ByVal value As String)
            _NroRegistro = value
        End Set
    End Property

    Private _FecRegistro As Date
    Public Property FecRegistro() As Date
        Get
            Return _FecRegistro
        End Get
        Set(ByVal value As Date)
            _FecRegistro = value
        End Set
    End Property

    Private _TipoCliente As String
    Public Property TipoCliente() As String
        Get
            Return _TipoCliente
        End Get
        Set(ByVal value As String)
            _TipoCliente = value
        End Set
    End Property

    Private _CodClienteStarSoft As String
    Public Property CodClienteStarSoft() As String
        Get
            Return _CodClienteStarSoft
        End Get
        Set(ByVal value As String)
            _CodClienteStarSoft = value
        End Set
    End Property

    Private _CodTipoDocStarSoft As String
    Public Property CodTipoDocStarSoft() As String
        Get
            Return _CodTipoDocStarSoft
        End Get
        Set(ByVal value As String)
            _CodTipoDocStarSoft = value
        End Set
    End Property

    Private _NuDocum As String
    Public Property NuDocum() As String
        Get
            Return _NuDocum
        End Get
        Set(ByVal value As String)
            _NuDocum = value
        End Set
    End Property

    Private _FecVencim As Date
    Public Property FecVencim() As Date
        Get
            Return _FecVencim
        End Get
        Set(ByVal value As Date)
            _FecVencim = value
        End Set
    End Property

    Private _FecEmision As Date
    Public Property FecEmision() As Date
        Get
            Return _FecEmision
        End Get
        Set(ByVal value As Date)
            _FecEmision = value
        End Set
    End Property

    Private _IDTipoDocVinc As String
    Public Property IDTipoDocVinc() As String
        Get
            Return _IDTipoDocVinc
        End Get
        Set(ByVal value As String)
            _IDTipoDocVinc = value
        End Set
    End Property

    Private _NuDocumVinc As String
    Public Property NuDocumVinc() As String
        Get
            Return _NuDocumVinc
        End Get
        Set(ByVal value As String)
            _NuDocumVinc = value
        End Set
    End Property

    Private _IgvPar As Single
    Public Property IgvPar() As Single
        Get
            Return _IgvPar
        End Get
        Set(ByVal value As Single)
            _IgvPar = value
        End Set
    End Property

    Private _ImporteTotal As Double
    Public Property ImporteTotal() As Double
        Get
            Return _ImporteTotal
        End Get
        Set(ByVal value As Double)
            _ImporteTotal = value
        End Set
    End Property

    Private _VTA As String
    Public Property VTA() As String
        Get
            Return _VTA
        End Get
        Set(ByVal value As String)
            _VTA = value
        End Set
    End Property

    Private _TipoCambio As Single
    Public Property TipoCambio() As Single
        Get
            Return _TipoCambio
        End Get
        Set(ByVal value As Single)
            _TipoCambio = value
        End Set
    End Property

    Private _Tipo_NuDocum As String
    Public Property Tipo_NuDocum() As String
        Get
            Return _Tipo_NuDocum
        End Get
        Set(ByVal value As String)
            _Tipo_NuDocum = value
        End Set
    End Property

    Private _TextoDoc As String
    Public Property TextoDoc() As String
        Get
            Return _TextoDoc
        End Get
        Set(ByVal value As String)
            _TextoDoc = value
        End Set
    End Property

    Private _Anulado As Char
    Public Property Anulado() As Char
        Get
            Return _Anulado
        End Get
        Set(ByVal value As Char)
            _Anulado = value
        End Set
    End Property

    Private _DebeHaber As Char
    Public Property DebeHaber() As Char
        Get
            Return _DebeHaber
        End Get
        Set(ByVal value As Char)
            _DebeHaber = value
        End Set
    End Property

    Private _RazonSocial As String
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    Private _CentroCosto As String
    Public Property CentroCosto() As String
        Get
            Return _CentroCosto
        End Get
        Set(ByVal value As String)
            _CentroCosto = value
        End Set
    End Property

    Private _FeEmisionVinc As Date
    Public Property FeEmisionVinc() As Date
        Get
            Return _FeEmisionVinc
        End Get
        Set(ByVal value As Date)
            _FeEmisionVinc = value
        End Set
    End Property

    Private _Exportacion As Char
    Public Property Exportacion() As Char
        Get
            Return _Exportacion
        End Get
        Set(ByVal value As Char)
            _Exportacion = value
        End Set
    End Property

    Private _IDFile As String
    Public Property IDFile() As String
        Get
            Return _IDFile
        End Get
        Set(ByVal value As String)
            _IDFile = value
        End Set
    End Property


End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPresupuesto_SobreBE
    Inherits clsAdministracionBaseBE

    Private _NuPreSob As Integer
    Public Property NuPreSob() As Integer
        Get
            Return _NuPreSob
        End Get
        Set(ByVal value As Integer)
            _NuPreSob = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _FePreSob As Date
    Public Property FePreSob() As Date
        Get
            Return _FePreSob
        End Get
        Set(ByVal value As Date)
            _FePreSob = value
        End Set
    End Property

    Private _CoPrvInt As String
    Public Property CoPrvInt() As String
        Get
            Return _CoPrvInt
        End Get
        Set(ByVal value As String)
            _CoPrvInt = value
        End Set
    End Property

    Private _CoPrvGui As String
    Public Property CoPrvGui() As String
        Get
            Return _CoPrvGui
        End Get
        Set(ByVal value As String)
            _CoPrvGui = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    Private _CoEjeOpe As String
    Public Property CoEjeOpe() As String
        Get
            Return _CoEjeOpe
        End Get
        Set(ByVal value As String)
            _CoEjeOpe = value
        End Set
    End Property

    Private _CoEjeFin As String
    Public Property CoEjeFin() As String
        Get
            Return _CoEjeFin
        End Get
        Set(ByVal value As String)
            _CoEjeFin = value
        End Set
    End Property

    Private _CoTipoProv As String
    Public Property CoTipoProv() As String
        Get
            Return _CoTipoProv
        End Get
        Set(ByVal value As String)
            _CoTipoProv = value
        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _IDPax As Integer
    Public Property IDPax() As Integer
        Get
            Return _IDPax
        End Get
        Set(ByVal value As Integer)
            _IDPax = value
        End Set
    End Property

    Private _FlTourConductor As Boolean
    Public Property FlTourConductor() As Boolean
        Get
            Return _FlTourConductor
        End Get
        Set(ByVal value As Boolean)
            _FlTourConductor = value
        End Set
    End Property

    Private _SsSaldo As Decimal
    Public Property SsSaldo() As Decimal
        Get
            Return _SsSaldo
        End Get
        Set(ByVal Value As Decimal)
            _SsSaldo = Value
        End Set
    End Property

    Private _SsDifAceptada As Decimal
    Public Property SsDifAceptada() As Decimal
        Get
            Return _SsDifAceptada
        End Get
        Set(ByVal Value As Decimal)
            _SsDifAceptada = Value
        End Set
    End Property

    Private _SsSaldo_USD As Decimal
    Public Property SsSaldo_USD() As Decimal
        Get
            Return _SsSaldo_USD
        End Get
        Set(ByVal Value As Decimal)
            _SsSaldo_USD = Value
        End Set
    End Property

    Private _SsDifAceptada_USD As Decimal
    Public Property SsDifAceptada_USD() As Decimal
        Get
            Return _SsDifAceptada_USD
        End Get
        Set(ByVal Value As Decimal)
            _SsDifAceptada_USD = Value
        End Set
    End Property

    Private _TxObsDocumento As String
    Public Property TxObsDocumento() As String
        Get
            Return _TxObsDocumento
        End Get
        Set(ByVal Value As String)
            _TxObsDocumento = Value
        End Set
    End Property

    Private _FlDesdeOtraForEgreso As Boolean
    Public Property FlDesdeOtraForEgreso() As Boolean
        Get
            Return _FlDesdeOtraForEgreso
        End Get
        Set(ByVal Value As Boolean)
            _FlDesdeOtraForEgreso = Value
        End Set
    End Property
   
    Private _NuCodigo_ER As String
    Public Property NuCodigo_ER() As String
        Get
            Return _NuCodigo_ER
        End Get
        Set(ByVal Value As String)
            _NuCodigo_ER = Value
        End Set
    End Property


    Private _ListaPresupuestoSobreDet As List(Of clsPresupuesto_Sobre_DetBE)
    Public Property ListaPresupuestoSobreDet() As List(Of clsPresupuesto_Sobre_DetBE)
        Get
            Return _ListaPresupuestoSobreDet
        End Get
        Set(ByVal value As List(Of clsPresupuesto_Sobre_DetBE))
            _ListaPresupuestoSobreDet = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPresupuesto_Sobre_DetBE
    Inherits clsAdministracionBaseBE

    Private _NuDetPSo As String
    Public Property NuDetPSo() As String
        Get
            Return _NuDetPSo
        End Get
        Set(ByVal value As String)
            _NuDetPSo = value
        End Set
    End Property

    Private _NuPreSob As Integer
    Public Property NuPreSob() As Integer
        Get
            Return _NuPreSob
        End Get
        Set(ByVal value As Integer)
            _NuPreSob = value
        End Set
    End Property

    Private _CoTipPSo As String
    Public Property CoTipPSo() As String
        Get
            Return _CoTipPSo
        End Get
        Set(ByVal value As String)
            _CoTipPSo = value
        End Set
    End Property

    Private _FeDetPSo As Date
    Public Property FeDetPSo() As Date
        Get
            Return _FeDetPSo
        End Get
        Set(ByVal value As Date)
            _FeDetPSo = value
        End Set
    End Property

    Private _IDOperacion_Det As Integer
    Public Property IDOperacion_Det() As Integer
        Get
            Return _IDOperacion_Det
        End Get
        Set(ByVal value As Integer)
            _IDOperacion_Det = value
        End Set
    End Property

    Private _IDServicio_Det As Integer
    Public Property IDServicio_Det() As Integer
        Get
            Return _IDServicio_Det
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det = value
        End Set
    End Property
    Private _IDServicio_Det_V_Cot As Integer
    Public Property IDServicio_Det_V_Cot() As Integer
        Get
            Return _IDServicio_Det_V_Cot
        End Get
        Set(ByVal value As Integer)
            _IDServicio_Det_V_Cot = value
        End Set
    End Property

    Private _NuSincerado_Det As Integer
    Public Property NuSincerado_Det() As Integer
        Get
            Return _NuSincerado_Det
        End Get
        Set(ByVal value As Integer)
            _NuSincerado_Det = value
        End Set
    End Property

    Private _TxServicio As String
    Public Property TxServicio() As String
        Get
            Return _TxServicio
        End Get
        Set(ByVal value As String)
            _TxServicio = value
        End Set
    End Property

    Private _QtPax As Int16
    Public Property QtPax() As Int16
        Get
            Return _QtPax
        End Get
        Set(ByVal value As Int16)
            _QtPax = value
        End Set
    End Property

    Private _QtPaxLib As Int16
    Public Property QtPaxLib() As Int16
        Get
            Return _QtPaxLib
        End Get
        Set(ByVal value As Int16)
            _QtPaxLib = value
        End Set
    End Property

    Private _SsPreUni As Double
    Public Property SsPreUni() As Double
        Get
            Return _SsPreUni
        End Get
        Set(ByVal value As Double)
            _SsPreUni = value
        End Set
    End Property

    Private _SsTotal As Double
    Public Property SsTotal() As Double
        Get
            Return _SsTotal
        End Get
        Set(ByVal value As Double)
            _SsTotal = value
        End Set
    End Property

    Private _SsTotSus As Double
    Public Property SsTotSus() As Double
        Get
            Return _SsTotSus
        End Get
        Set(ByVal value As Double)
            _SsTotSus = value
        End Set
    End Property

    Private _SsTotDev As Double
    Public Property SsTotDev() As Double
        Get
            Return _SsTotDev
        End Get
        Set(ByVal value As Double)
            _SsTotDev = value
        End Set
    End Property

    Private _CoPago As String
    Public Property CoPago() As String
        Get
            Return _CoPago
        End Get
        Set(ByVal value As String)
            _CoPago = value
        End Set
    End Property

    Private _CoPrvPrg As String
    Public Property CoPrvPrg() As String
        Get
            Return _CoPrvPrg
        End Get
        Set(ByVal value As String)
            _CoPrvPrg = value
        End Set
    End Property

    Private _IDMoneda As String
    Public Property IDMoneda() As String
        Get
            Return _IDMoneda
        End Get
        Set(ByVal value As String)
            _IDMoneda = value
        End Set
    End Property

    Private _IDServicio As String
    Public Property IDServicio() As String
        Get
            Return _IDServicio
        End Get
        Set(ByVal value As String)
            _IDServicio = value
        End Set
    End Property


    Private _IDPax As Integer
    Public Property IDPax() As Integer
        Get
            Return _IDPax
        End Get
        Set(ByVal value As Integer)
            _IDPax = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsVentaAdicionalBE
    Inherits clsAdministracionBaseBE

    Private _NuVtaAdi As Integer
    Public Property NuVtaAdi() As Integer
        Get
            Return _NuVtaAdi
        End Get
        Set(ByVal value As Integer)
            _NuVtaAdi = value
        End Set
    End Property
    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property
    Private _FeVtaAdi As Date
    Public Property FeVtaAdi() As Date
        Get
            Return _FeVtaAdi
        End Get
        Set(ByVal value As Date)
            _FeVtaAdi = value
        End Set
    End Property
    Private _QtPax As Int16
    Public Property QtPax() As Int16
        Get
            Return _QtPax
        End Get
        Set(ByVal value As Int16)
            _QtPax = value
        End Set
    End Property
    Private _CoUbigeo As String
    Public Property CoUbigeo() As String
        Get
            Return _CoUbigeo
        End Get
        Set(ByVal value As String)
            _CoUbigeo = value
        End Set
    End Property
    Private _ResponsableDMemo As String
    Public Property ResponsableDMemo() As String
        Get
            Return _ResponsableDMemo
        End Get
        Set(ByVal value As String)
            _ResponsableDMemo = value
        End Set
    End Property
    Private _CoTipo As String
    Public Property CoTipo() As String
        Get
            Return _CoTipo
        End Get
        Set(ByVal value As String)
            _CoTipo = value
        End Set
    End Property

    'Private _CoEstado As String
    'Public Property CoEstado() As String
    '    Get
    '        Return _CoEstado
    '    End Get
    '    Set(ByVal value As String)
    '        _CoEstado = value
    '    End Set
    'End Property

    Private _FeVencimDMemo As Date
    Public Property FeVencimDMemo() As Date
        Get
            Return _FeVencimDMemo
        End Get
        Set(ByVal value As Date)
            _FeVencimDMemo = value
        End Set
    End Property


    Private _ListaDetalleVtasAdicionales As List(Of clsDetalleVentaAdicionalBE)
    Public Property ListaDetalleVtasAdicionales() As List(Of clsDetalleVentaAdicionalBE)
        Get
            Return _ListaDetalleVtasAdicionales
        End Get
        Set(ByVal value As List(Of clsDetalleVentaAdicionalBE))
            _ListaDetalleVtasAdicionales = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDetalleVentaAdicionalBE
    Inherits clsVentaAdicionalBE
    Private _NuPax As Integer
    Public Property NuPax() As Integer
        Get
            Return _NuPax
        End Get
        Set(ByVal value As Integer)
            _NuPax = value
        End Set
    End Property
    Private _CoServicio As String
    Public Property CoServicio() As String
        Get
            Return _CoServicio
        End Get
        Set(ByVal value As String)
            _CoServicio = value
        End Set
    End Property
    Private _IDServicio As String
    Public Property IDServicio() As String
        Get
            Return _IDServicio
        End Get
        Set(ByVal value As String)
            _IDServicio = value
        End Set
    End Property

    Private _NuGrupo As Byte
    Public Property NuGrupo() As Byte
        Get
            Return _NuGrupo
        End Get
        Set(ByVal value As Byte)
            _NuGrupo = value
        End Set
    End Property
    Private _FlTitular As Boolean
    Public Property FlTitular() As Boolean
        Get
            Return _FlTitular
        End Get
        Set(ByVal value As Boolean)
            _FlTitular = value
        End Set
    End Property
    Private _SsMonto As Double
    Public Property SsMonto() As Double
        Get
            Return _SsMonto
        End Get
        Set(ByVal value As Double)
            _SsMonto = value
        End Set
    End Property
    Private _NoPax As String
    Public Property NoPax() As String
        Get
            Return _NoPax
        End Get
        Set(ByVal value As String)
            _NoPax = value
        End Set
    End Property
    Private _IDDebitMemo As String
    Public Property IDDebitMemo() As String
        Get
            Return _IDDebitMemo
        End Get
        Set(ByVal value As String)
            _IDDebitMemo = value
        End Set
    End Property

    Private _NoUbigeo As String
    Public Property NoUbigeo() As String
        Get
            Return _NoUbigeo
        End Get
        Set(ByVal value As String)
            _NoUbigeo = value
        End Set
    End Property
    Private _NoPais As String
    Public Property NoPais() As String
        Get
            Return _NoPais
        End Get
        Set(ByVal value As String)
            _NoPais = value
        End Set
    End Property
    Private _NuDocum As String
    Public Property NuDocum() As String
        Get
            Return _NuDocum
        End Get
        Set(ByVal value As String)
            _NuDocum = value
        End Set
    End Property
    Private _NuCliente As String
    Public Property NuCliente() As String
        Get
            Return _NuCliente
        End Get
        Set(ByVal value As String)
            _NuCliente = value
        End Set
    End Property

End Class

