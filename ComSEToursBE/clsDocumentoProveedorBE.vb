﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoProveedorBE

    Dim _NuDocumProv As Integer
    Dim _NuVoucher As Integer
    Dim _NuOrden_Servicio As Integer
    Dim _CoOrdPag As Integer
    Dim _NuSerie As String
    Dim _NuDocum As String
    Dim _NuDocInterno As String
    Dim _CoTipoDoc As String
    Dim _FeEmision As Date
    Dim _FeRecepcion As Date
    Dim _CoTipoDetraccion As String
    Dim _CoMoneda As String
    Dim _SsIGV As Decimal
    'Dim _SsMonto As Decimal
    Dim _SsNeto As Decimal
    Dim _SsOtrCargos As Decimal
    Dim _SsDetraccion As Decimal
    'Dim _QtCantidad As Integer
    Dim _CoEstado As String
    Dim _UserMod As String
    Dim _FecMod As Date
    Dim _SSTipoCambio As Double
    Dim _SSTotal As Double
    Dim _SSTotalOriginal As Double
    Dim _CoTipoOC As String
    Dim _CoCtaContab As String
    Dim _CoObligacionPago As String
    Dim _CoCecos As String
    Dim _CoMoneda_Pago As String
    Dim _NuFondoFijo As Integer
    Dim _CoTipoFondoFijo As String
    Dim _SSPercepcion As Decimal
    Dim _TxConcepto As String
    Dim _CoProveedor As String
    Dim _IDCab_FF As Integer
    Dim _UserNuevo As String

    Dim _FeVencimiento As Date
    Dim _CoTipoDocSAP As Byte
    Dim _CoFormaPago As String
    Dim _CoTipoDoc_Ref As String
    Dim _NuSerie_Ref As String
    Dim _NuDocum_Ref As String
    Dim _FeEmision_Ref As Date
    Dim _CardCodeSAP As String

    Dim _CoCecon As String
    Dim _CoGasto As String

    Dim _FlMultiple As Boolean
    Dim _NuDocum_Multiple As Integer
    Dim _IDDocFormaEgreso As Integer

    Public Property NuDocumProv() As Integer
        Get
            Return _NuDocumProv
        End Get
        Set(ByVal Value As Integer)
            _NuDocumProv = Value
        End Set
    End Property
    Public Property NuVoucher() As Integer
        Get
            Return _NuVoucher
        End Get
        Set(ByVal Value As Integer)
            _NuVoucher = Value
        End Set
    End Property
    Public Property NuOrden_Servicio() As Integer
        Get
            Return _NuOrden_Servicio
        End Get
        Set(ByVal Value As Integer)
            _NuOrden_Servicio = Value
        End Set
    End Property

    Public Property CoOrdPag() As Integer
        Get
            Return _CoOrdPag
        End Get
        Set(ByVal Value As Integer)
            _CoOrdPag = Value
        End Set
    End Property
    Private _NuOrdComInt As Integer
    Public Property NuOrdComInt() As Integer
        Get
            Return _NuOrdComInt
        End Get
        Set(ByVal value As Integer)
            _NuOrdComInt = value
        End Set
    End Property

    Public Property NuDocInterno() As String
        Get
            Return _NuDocInterno
        End Get
        Set(ByVal Value As String)
            _NuDocInterno = Value
        End Set
    End Property
    Public Property NuSerie() As String
        Get
            Return _NuSerie
        End Get
        Set(ByVal Value As String)
            _NuSerie = Value
        End Set
    End Property
    Public Property NuDocum() As String
        Get
            Return _NuDocum
        End Get
        Set(ByVal Value As String)
            _NuDocum = Value
        End Set
    End Property

    Public Property CoTipoDoc() As String
        Get
            Return _CoTipoDoc
        End Get
        Set(ByVal Value As String)
            _CoTipoDoc = Value
        End Set
    End Property
    Public Property FeEmision() As Date
        Get
            Return _FeEmision
        End Get
        Set(ByVal Value As Date)
            _FeEmision = Value
        End Set
    End Property
    Public Property FeRecepcion() As Date
        Get
            Return _FeRecepcion
        End Get
        Set(ByVal Value As Date)
            _FeRecepcion = Value
        End Set
    End Property
    Public Property CoTipoDetraccion() As String
        Get
            Return _CoTipoDetraccion
        End Get
        Set(ByVal Value As String)
            _CoTipoDetraccion = Value
        End Set
    End Property
    Public Property CoMoneda() As String
        Get
            Return _CoMoneda
        End Get
        Set(ByVal Value As String)
            _CoMoneda = Value
        End Set
    End Property
    Public Property SsIGV() As Double
        Get
            Return _SsIGV
        End Get
        Set(ByVal Value As Double)
            _SsIGV = Value
        End Set
    End Property
    Public Property SSTotalOriginal() As Double
        Get
            Return _SSTotalOriginal
        End Get
        Set(ByVal Value As Double)
            _SSTotalOriginal = Value
        End Set
    End Property

    Private _SSTotal_MN As Double
    Public Property SSTotal_MN() As Double
        Get
            Return _SSTotal_MN
        End Get
        Set(ByVal value As Double)
            _SSTotal_MN = value
        End Set
    End Property

    Public Property SsNeto() As Double
        Get
            Return _SsNeto
        End Get
        Set(ByVal Value As Double)
            _SsNeto = Value
        End Set
    End Property

    Public Property SsOtrCargos() As Double
        Get
            Return _SsOtrCargos
        End Get
        Set(ByVal Value As Double)
            _SsOtrCargos = Value
        End Set
    End Property

    Public Property SsDetraccion() As Double
        Get
            Return _SsDetraccion
        End Get
        Set(ByVal Value As Double)
            _SsDetraccion = Value
        End Set
    End Property

    'Public Property QtCantidad() As Decimal
    '    Get
    '        Return _QtCantidad
    '    End Get
    '    Set(ByVal Value As Decimal)
    '        _QtCantidad = Value
    '    End Set
    'End Property

    Public Property CoCecos() As String
        Get
            Return _CoCecos
        End Get
        Set(ByVal Value As String)
            _CoCecos = Value
        End Set
    End Property

    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal Value As String)
            _CoEstado = Value
        End Set
    End Property
    Private _FlActivo As Boolean
    Public Property FlActivo() As Boolean
        Get
            Return _FlActivo
        End Get
        Set(ByVal value As Boolean)
            _FlActivo = value
        End Set
    End Property
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal Value As String)
            _UserMod = Value
        End Set
    End Property
    Public Property UserNuevo() As String
        Get
            Return _UserNuevo
        End Get
        Set(ByVal Value As String)
            _UserNuevo = Value
        End Set
    End Property
    Public Property FecMod() As Date
        Get
            Return _FecMod
        End Get
        Set(ByVal Value As Date)
            _FecMod = Value
        End Set
    End Property

    Public Property SSTipoCambio() As Double
        Get
            Return _SSTipoCambio
        End Get
        Set(ByVal value As Double)
            _SSTipoCambio = value
        End Set
    End Property

    Public Property SSTotal() As Double
        Get
            Return _SSTotal
        End Get
        Set(ByVal value As Double)
            _SSTotal = value
        End Set
    End Property

    Private _SSTotalOrdServ As Double
    Public Property SSTotalOrdServ() As Double
        Get
            Return _SSTotalOrdServ
        End Get
        Set(ByVal value As Double)
            _SSTotalOrdServ = value
        End Set
    End Property


    Public Property CoObligacionPago() As String
        Get
            Return _CoObligacionPago
        End Get
        Set(ByVal Value As String)
            _CoObligacionPago = Value
        End Set
    End Property
    Public Property CoTipoOC() As String
        Get
            Return _CoTipoOC
        End Get
        Set(ByVal Value As String)
            _CoTipoOC = Value
        End Set
    End Property
    Public Property CoCtaContab() As String
        Get
            Return _CoCtaContab
        End Get
        Set(ByVal Value As String)
            _CoCtaContab = Value
        End Set
    End Property
    Public Property CoMoneda_Pago() As String
        Get
            Return _CoMoneda_Pago
        End Get
        Set(ByVal Value As String)
            _CoMoneda_Pago = Value
        End Set
    End Property

    Private _CoMoneda_FEgreso As String
    Public Property CoMoneda_FEgreso() As String
        Get
            Return _CoMoneda_FEgreso
        End Get
        Set(ByVal value As String)
            _CoMoneda_FEgreso = value
        End Set
    End Property

    Private _SsTipoCambio_FEgreso As Double
    Public Property SsTipoCambio_FEgreso() As Double
        Get
            Return _SsTipoCambio_FEgreso
        End Get
        Set(ByVal value As Double)
            _SsTipoCambio_FEgreso = value
        End Set
    End Property

    Private _SsTotal_FEgreso As Decimal
    Public Property SsTotal_FEgreso() As Decimal
        Get
            Return _SsTotal_FEgreso
        End Get
        Set(ByVal value As Decimal)
            _SsTotal_FEgreso = value
        End Set
    End Property

    Private _IDOperacion As Integer
    Public Property IDOperacion() As Integer
        Get
            Return _IDOperacion
        End Get
        Set(ByVal value As Integer)
            _IDOperacion = value
        End Set
    End Property

    Private _NuVouOfiExtInt As Integer
    Public Property NuVouOfiExtInt() As Integer
        Get
            Return _NuVouOfiExtInt
        End Get
        Set(ByVal value As Integer)
            _NuVouOfiExtInt = value
        End Set
    End Property
    Private _NuVouPTrenInt As Integer
    Public Property NuVouPTrenInt() As Integer
        Get
            Return _NuVouPTrenInt
        End Get
        Set(ByVal value As Integer)
            _NuVouPTrenInt = value
        End Set
    End Property

    Private _NuDocumVta As String
    Public Property NuDocumVta() As String
        Get
            Return _NuDocumVta
        End Get
        Set(ByVal value As String)
            _NuDocumVta = value
        End Set
    End Property
    Private _CoTipoDocVta As String
    Public Property CoTipoDocVta() As String
        Get
            Return _CoTipoDocVta
        End Get
        Set(ByVal value As String)
            _CoTipoDocVta = value
        End Set
    End Property
    Private _NuPreSob As Integer
    Public Property NuPreSob() As Integer
        Get
            Return _NuPreSob
        End Get
        Set(ByVal value As Integer)
            _NuPreSob = value
        End Set
    End Property
    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property
    Private _CoUbigeo_Oficina As String
    Public Property CoUbigeo_Oficina() As String
        Get
            Return _CoUbigeo_Oficina
        End Get
        Set(ByVal value As String)
            _CoUbigeo_Oficina = value
        End Set
    End Property

    Public Property NuFondoFijo() As Integer
        Get
            Return _NuFondoFijo
        End Get
        Set(ByVal Value As Integer)
            _NuFondoFijo = Value
        End Set
    End Property
    Public Property CoTipoFondoFijo() As String
        Get
            Return _CoTipoFondoFijo
        End Get
        Set(ByVal Value As String)
            _CoTipoFondoFijo = Value
        End Set
    End Property
    Public Property SSPercepcion() As Decimal
        Get
            Return _SSPercepcion
        End Get
        Set(ByVal Value As Decimal)
            _SSPercepcion = Value
        End Set
    End Property
    Public Property TxConcepto() As String
        Get
            Return _TxConcepto
        End Get
        Set(ByVal Value As String)
            _TxConcepto = Value
        End Set
    End Property
    Public Property CoProveedor() As String
        Get
            Return _CoProveedor
        End Get
        Set(ByVal Value As String)
            _CoProveedor = Value
        End Set
    End Property

    Public Property IDCab_FF() As Integer
        Get
            Return _IDCab_FF
        End Get
        Set(ByVal Value As Integer)
            _IDCab_FF = Value
        End Set
    End Property


    Public Property FeVencimiento() As Date
        Get
            Return _FeVencimiento
        End Get
        Set(ByVal Value As Date)
            _FeVencimiento = Value
        End Set
    End Property
    Public Property CoTipoDocSAP() As Byte
        Get
            Return _CoTipoDocSAP
        End Get
        Set(ByVal Value As Byte)
            _CoTipoDocSAP = Value
        End Set
    End Property
    Public Property CoFormaPago() As String
        Get
            Return _CoFormaPago
        End Get
        Set(ByVal Value As String)
            _CoFormaPago = Value
        End Set
    End Property
    Public Property CoTipoDoc_Ref() As String
        Get
            Return _CoTipoDoc_Ref
        End Get
        Set(ByVal Value As String)
            _CoTipoDoc_Ref = Value
        End Set
    End Property
    Public Property NuSerie_Ref() As String
        Get
            Return _NuSerie_Ref
        End Get
        Set(ByVal Value As String)
            _NuSerie_Ref = Value
        End Set
    End Property
    Public Property NuDocum_Ref() As String
        Get
            Return _NuDocum_Ref
        End Get
        Set(ByVal Value As String)
            _NuDocum_Ref = Value
        End Set
    End Property
    Public Property FeEmision_Ref() As Date
        Get
            Return _FeEmision_Ref
        End Get
        Set(ByVal Value As Date)
            _FeEmision_Ref = Value
        End Set
    End Property

    Public Property CoCeCon() As String
        Get
            Return _CoCecon
        End Get
        Set(ByVal Value As String)
            _CoCecon = Value
        End Set
    End Property

    Public Property CardCodeSAP() As String
        Get
            Return _CardCodeSAP
        End Get
        Set(ByVal Value As String)
            _CardCodeSAP = Value
        End Set
    End Property

    Public Property CoGasto() As String
        Get
            Return _CoGasto
        End Get
        Set(ByVal Value As String)
            _CoGasto = Value
        End Set
    End Property

    Public Property FlMultiple() As Boolean
        Get
            Return _FlMultiple
        End Get
        Set(ByVal Value As Boolean)
            _FlMultiple = Value
        End Set
    End Property

    Public Property NuDocum_Multiple() As Integer
        Get
            Return _NuDocum_Multiple
        End Get
        Set(ByVal Value As Integer)
            _NuDocum_Multiple = Value
        End Set
    End Property

    Public Property IDDocFormaEgreso() As Integer
        Get
            Return _IDDocFormaEgreso
        End Get
        Set(ByVal Value As Integer)
            _IDDocFormaEgreso = Value
        End Set
    End Property

    Private _AceptarPresupuestoTicket As Boolean
    Public Property AceptarPresupuestoTicket() As Boolean
        Get
            Return _AceptarPresupuestoTicket
        End Get
        Set(ByVal value As Boolean)
            _AceptarPresupuestoTicket = value
        End Set
    End Property

    Private _CoTipDocSusEnt As String
    Public Property CoTipDocSusEnt() As String
        Get
            Return _CoTipDocSusEnt
        End Get
        Set(ByVal value As String)
            _CoTipDocSusEnt = value
        End Set
    End Property

    Private _NuDocumSusEnt As String
    Public Property NuDocumSusEnt() As String
        Get
            Return _NuDocumSusEnt
        End Get
        Set(ByVal value As String)
            _NuDocumSusEnt = value
        End Set
    End Property

    Private _NuOperBanSusEnt As String
    Public Property NuOperBanSusEnt() As String
        Get
            Return _NuOperBanSusEnt
        End Get
        Set(ByVal value As String)
            _NuOperBanSusEnt = value
        End Set
    End Property

    Private _CoProvSetours As String
    Public Property CoProvSetours() As String
        Get
            Return _CoProvSetours
        End Get
        Set(ByVal value As String)
            _CoProvSetours = value
        End Set
    End Property


    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDocumentoProveedor_DetBE
        Dim _NuDocumProv As Integer
        Dim _NuDocumProvDet As Byte
        Dim _IDServicio_Det As Integer
        Dim _TxServicio As String
        Dim _SsMonto As Decimal
        Dim _FlActivo As Boolean
        Dim _UserMod As String
        Dim _FecMod As Date
        Dim _QtCantidad As Integer
        Dim _IDCab As Integer
        Dim _CoProducto As String
        Dim _CoAlmacen As String
        Dim _SsIGV As Decimal
        Dim _CoTipoOC As String
        Dim _CoCtaContab As String
        Dim _CoCeCos As String
        Dim _CoCeCon As String
        Dim _SSPrecUni As Decimal
        Dim _SSTotal As Decimal
        Dim _Accion As String

        Public Property NuDocumProv() As Integer
            Get
                Return _NuDocumProv
            End Get
            Set(ByVal Value As Integer)
                _NuDocumProv = Value
            End Set
        End Property
        Public Property NuDocumProvDet() As Byte
            Get
                Return _NuDocumProvDet
            End Get
            Set(ByVal Value As Byte)
                _NuDocumProvDet = Value
            End Set
        End Property
        Public Property IDServicio_Det() As Integer
            Get
                Return _IDServicio_Det
            End Get
            Set(ByVal Value As Integer)
                _IDServicio_Det = Value
            End Set
        End Property
        Public Property TxServicio() As String
            Get
                Return _TxServicio
            End Get
            Set(ByVal Value As String)
                _TxServicio = Value
            End Set
        End Property
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal Value As String)
                _Accion = Value
            End Set
        End Property
        Public Property SsMonto() As Decimal
            Get
                Return _SsMonto
            End Get
            Set(ByVal Value As Decimal)
                _SsMonto = Value
            End Set
        End Property
        Public Property SsTotal() As Decimal
            Get
                Return _SSTotal
            End Get
            Set(ByVal Value As Decimal)
                _SSTotal = Value
            End Set
        End Property
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal Value As String)
                _UserMod = Value
            End Set
        End Property
        Public Property FecMod() As Date
            Get
                Return _FecMod
            End Get
            Set(ByVal Value As Date)
                _FecMod = Value
            End Set
        End Property
        Public Property QtCantidad() As Integer
            Get
                Return _QtCantidad
            End Get
            Set(ByVal Value As Integer)
                _QtCantidad = Value
            End Set
        End Property
        Public Property IDCab() As Integer
            Get
                Return _IDCab
            End Get
            Set(ByVal Value As Integer)
                _IDCab = Value
            End Set
        End Property
        Public Property CoProducto() As String
            Get
                Return _CoProducto
            End Get
            Set(ByVal Value As String)
                _CoProducto = Value
            End Set
        End Property
        Public Property CoAlmacen() As String
            Get
                Return _CoAlmacen
            End Get
            Set(ByVal Value As String)
                _CoAlmacen = Value
            End Set
        End Property
        Public Property SsIGV() As Decimal
            Get
                Return _SsIGV
            End Get
            Set(ByVal Value As Decimal)
                _SsIGV = Value
            End Set
        End Property
        Public Property CoTipoOC() As String
            Get
                Return _CoTipoOC
            End Get
            Set(ByVal Value As String)
                _CoTipoOC = Value
            End Set
        End Property
        Public Property CoCtaContab() As String
            Get
                Return _CoCtaContab
            End Get
            Set(ByVal Value As String)
                _CoCtaContab = Value
            End Set
        End Property
        Public Property CoCeCos() As String
            Get
                Return _CoCeCos
            End Get
            Set(ByVal Value As String)
                _CoCeCos = Value
            End Set
        End Property
        Public Property CoCeCon() As String
            Get
                Return _CoCeCon
            End Get
            Set(ByVal Value As String)
                _CoCeCon = Value
            End Set
        End Property
        Public Property SSPrecUni() As Decimal
            Get
                Return _SSPrecUni
            End Get
            Set(ByVal Value As Decimal)
                _SSPrecUni = Value
            End Set
        End Property
    End Class



    Private _ListaDetalle As List(Of clsDocumentoProveedor_DetBE)
    Public Property ListaDetalle() As List(Of clsDocumentoProveedor_DetBE)
        Get
            Return _ListaDetalle
        End Get
        Set(ByVal value As List(Of clsDocumentoProveedor_DetBE))
            _ListaDetalle = value
        End Set
    End Property


    Private _IngresoManual As Boolean
    Public Property IngresoManual() As Boolean
        Get
            Return _IngresoManual
        End Get
        Set(ByVal value As Boolean)
            _IngresoManual = value
        End Set
    End Property

    Private _ListaDocumentos As List(Of clsDocumentoProveedorBE)
    Public Property ListaDocumentos() As List(Of clsDocumentoProveedorBE)
        Get
            Return _ListaDocumentos
        End Get
        Set(ByVal value As List(Of clsDocumentoProveedorBE))
            _ListaDocumentos = value
        End Set
    End Property

    Private _ErrorSetra As String
    Public Property ErrorSetra() As String
        Get
            Return _ErrorSetra
        End Get
        Set(ByVal value As String)
            _ErrorSetra = value
        End Set
    End Property

    Private _ErrorSAP As String
    Public Property ErrorSAP() As String
        Get
            Return _ErrorSAP
        End Get
        Set(ByVal value As String)
            _ErrorSAP = value
        End Set
    End Property

    Private _ListaINC As List(Of clsTablasApoyoBE.clsENTRADASINC_SAPBE)
    Public Property ListaINC() As List(Of clsTablasApoyoBE.clsENTRADASINC_SAPBE)
        Get
            Return _ListaINC
        End Get
        Set(ByVal value As List(Of clsTablasApoyoBE.clsENTRADASINC_SAPBE))
            _ListaINC = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumento_Forma_Egreso
    Dim _IDDocFormaEgreso As Integer
    Dim _TipoFormaEgreso As String
    Dim _CoMoneda As String
    Dim _CoEstado As String
    Dim _SsMontoTotal As Decimal
    Dim _SsSaldo As Decimal
    Dim _SsDifAceptada As Decimal
    Dim _UserMod As String
    Dim _FecMod As Date
    Dim _TxObsDocumento As String
    Dim _FeCreacion As Date
    Dim _CoUbigeo_Oficina As String
    Dim _IDCab As Integer

    Public Property IDDocFormaEgreso() As Integer
        Get
            Return _IDDocFormaEgreso
        End Get
        Set(value As Integer)
            _IDDocFormaEgreso = value
        End Set
    End Property

    Public Property TipoFormaEgreso() As String
        Get
            Return _TipoFormaEgreso
        End Get
        Set(value As String)
            _TipoFormaEgreso = value
        End Set
    End Property

    Public Property CoMoneda() As String
        Get
            Return _CoMoneda
        End Get
        Set(value As String)
            _CoMoneda = value
        End Set
    End Property

    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(value As String)
            _CoEstado = value
        End Set
    End Property

    Public Property SsMontoTotal() As Decimal
        Get
            Return _SsMontoTotal
        End Get
        Set(value As Decimal)
            _SsMontoTotal = value
        End Set
    End Property

    Public Property SsSaldo() As Decimal
        Get
            Return _SsSaldo
        End Get
        Set(value As Decimal)
            _SsSaldo = value
        End Set
    End Property

    Public Property SsDifAceptada() As Decimal
        Get
            Return _SsDifAceptada
        End Get
        Set(value As Decimal)
            _SsDifAceptada = value
        End Set
    End Property

    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(value As String)
            _UserMod = value
        End Set
    End Property

    Public Property FecMod() As Date
        Get
            Return _FecMod
        End Get
        Set(value As Date)
            _FecMod = value
        End Set
    End Property

    Public Property TxObsDocumento() As String
        Get
            Return _TxObsDocumento
        End Get
        Set(value As String)
            _TxObsDocumento = value
        End Set
    End Property

    Public Property FeCreacion() As Date
        Get
            Return _FeCreacion
        End Get
        Set(value As Date)
            _FeCreacion = value
        End Set
    End Property

    Public Property CoUbigeo_Oficina() As String
        Get
            Return _CoUbigeo_Oficina
        End Get
        Set(value As String)
            _CoUbigeo_Oficina = value
        End Set
    End Property

    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(value As Integer)
            _IDCab = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsMAAlmacenBE
    Dim _CoAlmacen As String
    Dim _NoAlmacen As String
    Dim _CoSAP As String
    Dim _FlActivo As Boolean
    Dim _UserMod As String
    Dim _FecMod As Date


    Public Property CoAlmacen() As String
        Get
            Return _CoAlmacen
        End Get
        Set(ByVal Value As String)
            _CoAlmacen = Value
        End Set
    End Property
    Public Property NoAlmacen() As String
        Get
            Return _NoAlmacen
        End Get
        Set(ByVal Value As String)
            _NoAlmacen = Value
        End Set
    End Property
    Public Property CoSAP() As String
        Get
            Return _CoSAP
        End Get
        Set(ByVal Value As String)
            _CoSAP = Value
        End Set
    End Property
    Public Property FlActivo() As Boolean
        Get
            Return _FlActivo
        End Get
        Set(ByVal Value As Boolean)
            _FlActivo = Value
        End Set
    End Property
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal Value As String)
            _UserMod = Value
        End Set
    End Property
    Public Property FecMod() As Date
        Get
            Return _FecMod
        End Get
        Set(ByVal Value As Date)
            _FecMod = Value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsProductosBE
    Dim _CoProducto As String
    Dim _NoProducto As String
    Dim _CoRubro As Byte
    Dim _CoSAP As String
    Dim _FlActivo As Boolean
    Dim _UserMod As String
    Dim _FecMod As Date


    Public Property CoProducto() As String
        Get
            Return _CoProducto
        End Get
        Set(ByVal Value As String)
            _CoProducto = Value
        End Set
    End Property
    Public Property NoProducto() As String
        Get
            Return _NoProducto
        End Get
        Set(ByVal Value As String)
            _NoProducto = Value
        End Set
    End Property
    Public Property CoRubro() As Byte
        Get
            Return _CoRubro
        End Get
        Set(ByVal Value As Byte)
            _CoRubro = Value
        End Set
    End Property
    Public Property CoSAP() As String
        Get
            Return _CoSAP
        End Get
        Set(ByVal Value As String)
            _CoSAP = Value
        End Set
    End Property
    Public Property FlActivo() As Boolean
        Get
            Return _FlActivo
        End Get
        Set(ByVal Value As Boolean)
            _FlActivo = Value
        End Set
    End Property
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal Value As String)
            _UserMod = Value
        End Set
    End Property
    Public Property FecMod() As Date
        Get
            Return _FecMod
        End Get
        Set(ByVal Value As Date)
            _FecMod = Value
        End Set
    End Property

End Class
