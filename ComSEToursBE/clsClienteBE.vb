﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

'<ComVisible(True)> _
'<Transaction(TransactionOption.NotSupported)> _
'Public Class clsClienteBaseBE
'    'Inherits ServicedComponent
'    Private _IDCliente As String
'    Public Property IDCliente() As String
'        Get
'            Return _IDCliente
'        End Get
'        Set(ByVal value As String)
'            _IDCliente = value
'        End Set
'    End Property
'    Private _UserMod As String
'    Public Property UserMod() As String
'        Get
'            Return _UserMod
'        End Get
'        Set(ByVal value As String)
'            _UserMod = value
'        End Set
'    End Property
'End Class
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsClienteBE
    'Inherits clsClienteBaseBE
    ''Inherits ServicedComponent
    'Inherits ServicedComponent


    Private _CoStarSoft As String
    Public Property CoStarSoft() As String
        Get
            Return _CoStarSoft
        End Get
        Set(ByVal value As String)
            _CoStarSoft = value
        End Set
    End Property
    Private _NuCuentaComision As String
    Public Property NuCuentaComision() As String
        Get
            Return _NuCuentaComision
        End Get
        Set(ByVal value As String)
            _NuCuentaComision = value
        End Set
    End Property


    Private _IDCliente As String
    Public Property IDCliente() As String
        Get
            Return _IDCliente
        End Get
        Set(ByVal value As String)
            _IDCliente = value
        End Set
    End Property
    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

    Private _RazonSocial As String
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    Private _RazonComercial As String
    Public Property RazonComercial() As String
        Get
            Return _RazonComercial
        End Get
        Set(ByVal value As String)
            _RazonComercial = value
        End Set
    End Property

    Private _Persona As String
    Public Property Persona() As String
        Get
            Return _Persona
        End Get
        Set(ByVal value As String)
            _Persona = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Private _ApPaterno As String
    Public Property ApPaterno() As String
        Get
            Return _ApPaterno
        End Get
        Set(ByVal value As String)
            _ApPaterno = value
        End Set
    End Property

    Private _ApMaterno As String
    Public Property ApMaterno() As String
        Get
            Return _ApMaterno
        End Get
        Set(ByVal value As String)
            _ApMaterno = value
        End Set
    End Property

    Private _Domiciliado As Boolean
    Public Property Domiciliado() As Boolean
        Get
            Return _Domiciliado
        End Get
        Set(ByVal value As Boolean)
            _Domiciliado = value
        End Set
    End Property
    Private _NombreCorto As String
    Public Property NombreCorto() As String
        Get
            Return _NombreCorto
        End Get
        Set(ByVal value As String)
            _NombreCorto = value
        End Set
    End Property

    Private _Tipo As String
    Public Property Tipo() As String
        Get
            Return _Tipo
        End Get
        Set(ByVal value As String)
            _Tipo = value
        End Set
    End Property


    Private _Direccion As String
    Public Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property

    'Private _IDIdentidad As String
    'Public Property IDIdentidad() As String
    '    Get
    '        Return _IDIdentidad
    '    End Get
    '    Set(ByVal value As String)
    '        _IDIdentidad = value
    '    End Set
    'End Property

    'Private _NumIdentidad As String
    'Public Property NumIDentidad() As String
    '    Get
    '        Return _NumIdentidad
    '    End Get
    '    Set(ByVal value As String)
    '        _NumIdentidad = value
    '    End Set
    'End Property



    Private _NumIdentidad As String
    Public Property NumIdentidad() As String
        Get
            Return _NumIdentidad
        End Get
        Set(ByVal value As String)
            _NumIdentidad = value
        End Set
    End Property

    Private _IdIdentidad As String
    Public Property IdIdentidad() As String
        Get
            Return _IdIdentidad
        End Get
        Set(ByVal value As String)
            _IdIdentidad = value
        End Set
    End Property


    Private _Telefono1 As String
    Public Property Telefono1() As String
        Get
            Return _Telefono1
        End Get
        Set(ByVal value As String)
            _Telefono1 = value
        End Set
    End Property

    Private _Telefono2 As String
    Public Property Telefono2() As String
        Get
            Return _Telefono2
        End Get
        Set(ByVal value As String)
            _Telefono2 = value
        End Set
    End Property

    Private _Celular As String
    Public Property Celular() As String
        Get
            Return _Celular
        End Get
        Set(ByVal value As String)
            _Celular = value
        End Set
    End Property

    Private _Fax As String
    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal value As String)
            _Fax = value
        End Set
    End Property

    Private _Comision As Decimal
    Public Property Comision() As Decimal
        Get
            Return _Comision
        End Get
        Set(ByVal value As Decimal)
            _Comision = value
        End Set
    End Property

    Private _Notas As String
    Public Property Notas() As String
        Get
            Return _Notas
        End Get
        Set(ByVal value As String)
            _Notas = value
        End Set
    End Property


    Private _Ciudad As String
    Public Property Ciudad() As String
        Get
            Return _Ciudad
        End Get
        Set(ByVal value As String)
            _Ciudad = value
        End Set
    End Property

    Private _IDCiudad As String
    Public Property IDCiudad() As String
        Get
            Return _IDCiudad
        End Get
        Set(ByVal value As String)
            _IDCiudad = value
        End Set
    End Property

    'Private _IDUbigeo As String
    'Public Property IDUbigeo() As String
    '    Get
    '        Return _IDUbigeo
    '    End Get
    '    Set(ByVal value As String)
    '        _IDUbigeo = value
    '    End Set
    'End Property

    'Private _Contacto As String
    'Public Property Contacto() As String
    '    Get
    '        Return _Contacto
    '    End Get
    '    Set(ByVal value As String)
    '        _Contacto = value
    '    End Set
    'End Property

    'Private _Cargo As String
    'Public Property Cargo() As String
    '    Get
    '        Return _Cargo
    '    End Get
    '    Set(ByVal value As String)
    '        _Cargo = value
    '    End Set
    'End Property

    Private _IDVendedor As String
    Public Property IDVendedor() As String
        Get
            Return _IDVendedor
        End Get
        Set(ByVal value As String)
            _IDVendedor = value
        End Set
    End Property

    Private _Web As String
    Public Property Web() As String
        Get
            Return _Web
        End Get
        Set(ByVal value As String)
            _Web = value
        End Set
    End Property

    Private _CodPostal As String
    Public Property CodPostal() As String
        Get
            Return _CodPostal
        End Get
        Set(ByVal value As String)
            _CodPostal = value
        End Set
    End Property

    Private _Credito As Boolean
    Public Property Credito() As Boolean
        Get
            Return _Credito
        End Get
        Set(ByVal value As Boolean)
            _Credito = value
        End Set
    End Property

    Private _MostrarEnReserva As Boolean
    Public Property MostrarEnReserva() As Boolean
        Get
            Return _MostrarEnReserva
        End Get
        Set(ByVal value As Boolean)
            _MostrarEnReserva = value
        End Set
    End Property


    Private _Complejidad As String
    Public Property Complejidad() As String
        Get
            Return _Complejidad
        End Get
        Set(ByVal value As String)
            _Complejidad = value
        End Set
    End Property

    Private _Frecuencia As String
    Public Property Frecuencia() As String
        Get
            Return _Frecuencia
        End Get
        Set(ByVal value As String)
            _Frecuencia = value
        End Set
    End Property

    Private _Solvencia As Byte
    Public Property Solvencia() As Byte
        Get
            Return _Solvencia
        End Get
        Set(ByVal value As Byte)
            _Solvencia = value
        End Set
    End Property

    Private _CoTipoVenta As String
    Public Property CoTipoVenta() As String
        Get
            Return _CoTipoVenta
        End Get
        Set(ByVal value As String)
            _CoTipoVenta = value
        End Set

    End Property

    Private _FlTop As Boolean
    Public Property FlTop() As Boolean
        Get
            Return _FlTop
        End Get
        Set(ByVal value As Boolean)
            _FlTop = value
        End Set
    End Property

    Dim _FePerfilUpdate As Date
    Public Property FePerfilUpdate() As Date
        Get
            Return _FePerfilUpdate
        End Get
        Set(ByVal Value As Date)
            _FePerfilUpdate = Value
        End Set
    End Property

    Dim _CoUserPerfilUpdate As String
    Public Property CoUserPerfilUpdate() As String
        Get
            Return _CoUserPerfilUpdate
        End Get
        Set(ByVal Value As String)
            _CoUserPerfilUpdate = Value
        End Set
    End Property

    Dim _txPerfilInformacion As String
    Public Property txPerfilInformacion() As String
        Get
            Return _txPerfilInformacion
        End Get
        Set(ByVal Value As String)
            _txPerfilInformacion = Value
        End Set
    End Property

    Dim _txPerfilOperatividad As String
    Public Property txPerfilOperatividad() As String
        Get
            Return _txPerfilOperatividad
        End Get
        Set(ByVal Value As String)
            _txPerfilOperatividad = Value
        End Set
    End Property

    Dim _FlProveedor As Boolean
    Public Property FlProveedor() As Boolean
        Get
            Return _FlProveedor
        End Get
        Set(ByVal Value As Boolean)
            _FlProveedor = Value
        End Set
    End Property

    Dim _NuPosicion As Byte
    Public Property NuPosicion() As Byte
        Get
            Return _NuPosicion
        End Get
        Set(ByVal Value As Byte)
            _NuPosicion = Value
        End Set
    End Property

    Private _CardCodeSAP As String
    Public Property CardCodeSAP() As String
        Get
            Return _CardCodeSAP
        End Get
        Set(ByVal value As String)
            _CardCodeSAP = value
        End Set
    End Property

    Private _CoTipDocIdentPax As String
    Public Property CoTipDocIdentPax() As String
        Get
            Return _CoTipDocIdentPax
        End Get
        Set(ByVal value As String)
            _CoTipDocIdentPax = value
        End Set
    End Property

    Private _NuDocIdentPax As String
    Public Property NuDocIdentPax() As String
        Get
            Return _NuDocIdentPax
        End Get
        Set(ByVal value As String)
            _NuDocIdentPax = value
        End Set
    End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsSeriesClienteBE
        Inherits clsClienteBE

        Private _NuSerie As Byte
        Public Property NuSerie() As Byte
            Get
                Return _NuSerie
            End Get
            Set(ByVal value As Byte)
                _NuSerie = value
            End Set
        End Property

        Private _NoSerie As String
        Public Property NoSerie() As String
            Get
                Return _NoSerie
            End Get
            Set(ByVal value As String)
                _NoSerie = value
            End Set
        End Property

        Private _PoConcretizacion As Double
        Public Property PoConcretizacion() As Double
            Get
                Return _PoConcretizacion
            End Get
            Set(ByVal value As Double)
                _PoConcretizacion = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
        Public Class clsContactoClienteBE
        'Inherits clsClienteBaseBE
        ''Inherits ServicedComponent
        'Inherits ServicedComponent

        Private _IDCliente As String
        Public Property IDCliente() As String
            Get
                Return _IDCliente
            End Get
            Set(ByVal value As String)
                _IDCliente = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _IDContacto As String
        Public Property IDContacto() As String
            Get
                Return _IDContacto
            End Get
            Set(ByVal value As String)
                _IDContacto = value
            End Set
        End Property

        Private _IDIdentidad As String
        Public Property IDIdentidad() As String
            Get
                Return _IDIdentidad
            End Get
            Set(ByVal value As String)
                _IDIdentidad = value
            End Set
        End Property

        Private _NumIdentidad As String
        Public Property NumIdentidad() As String
            Get
                Return _NumIdentidad
            End Get
            Set(ByVal value As String)
                _NumIdentidad = value
            End Set
        End Property

        Private _Titulo As String
        Public Property Titulo() As String
            Get
                Return _Titulo
            End Get
            Set(ByVal value As String)
                _Titulo = value
            End Set
        End Property

        Private _Nombres As String
        Public Property Nombres() As String
            Get
                Return _Nombres
            End Get
            Set(ByVal value As String)
                _Nombres = value
            End Set
        End Property

        Private _Apellidos As String
        Public Property Apellidos() As String
            Get
                Return _Apellidos
            End Get
            Set(ByVal value As String)
                _Apellidos = value
            End Set
        End Property

        Private _Cargo As String
        Public Property Cargo() As String
            Get
                Return _Cargo
            End Get
            Set(ByVal value As String)
                _Cargo = value
            End Set
        End Property

        Private _Telefono As String
        Public Property Telefono() As String
            Get
                Return _Telefono
            End Get
            Set(ByVal value As String)
                _Telefono = value
            End Set
        End Property

        Private _Fax As String
        Public Property Fax() As String
            Get
                Return _Fax
            End Get
            Set(ByVal value As String)
                _Fax = value
            End Set
        End Property

        Private _Celular As String
        Public Property Celular() As String
            Get
                Return _Celular
            End Get
            Set(ByVal value As String)
                _Celular = value
            End Set
        End Property

        Private _Email As String
        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal value As String)
                _Email = value
            End Set
        End Property

        Private _Email2 As String
        Public Property Email2() As String
            Get
                Return _Email2
            End Get
            Set(ByVal value As String)
                _Email2 = value
            End Set
        End Property

        Private _Email3 As String
        Public Property Email3() As String
            Get
                Return _Email3
            End Get
            Set(ByVal value As String)
                _Email3 = value
            End Set
        End Property

        Private _Anexo As String
        Public Property Anexo() As String
            Get
                Return _Anexo
            End Get
            Set(ByVal value As String)
                _Anexo = value
            End Set
        End Property

        Private _UsuarioLogeo As String
        Public Property UsuarioLogeo() As String
            Get
                Return _UsuarioLogeo
            End Get
            Set(ByVal value As String)
                _UsuarioLogeo = value
            End Set
        End Property

        Private _PasswordLogeo As String
        Public Property PasswordLogeo() As String
            Get
                Return _PasswordLogeo
            End Get
            Set(ByVal value As String)
                _PasswordLogeo = value
            End Set
        End Property

        Private _FechaPassword As Date
        Public Property FechaPassword() As Date
            Get
                Return _FechaPassword
            End Get
            Set(ByVal value As Date)
                _FechaPassword = value
            End Set
        End Property

        Private _EsAdminist As Boolean
        Public Property EsAdminist() As Boolean
            Get
                Return _EsAdminist
            End Get
            Set(ByVal value As Boolean)
                _EsAdminist = value
            End Set
        End Property

        Private _FechaNacimiento As Date
        Public Property FechaNacimiento() As Date
            Get
                Return _FechaNacimiento
            End Get
            Set(ByVal value As Date)
                _FechaNacimiento = value
            End Set
        End Property

        Private _Notas As String
        Public Property Notas() As String
            Get
                Return _Notas
            End Get
            Set(ByVal value As String)
                _Notas = value
            End Set
        End Property

        Private _FlAccesoWeb As Boolean
        Public Property FlAccesoWeb() As Boolean
            Get
                Return _FlAccesoWeb
            End Get
            Set(ByVal value As Boolean)
                _FlAccesoWeb = value
            End Set
        End Property

        Private _UsuarioLogeo_Hash As String
        Public Property UsuarioLogeo_Hash() As String
            Get
                Return _UsuarioLogeo_Hash
            End Get
            Set(ByVal value As String)
                _UsuarioLogeo_Hash = value
            End Set
        End Property

        Private _FechaHash As Date
        Public Property FechaHash() As Date
            Get
                Return _FechaHash
            End Get
            Set(ByVal value As Date)
                _FechaHash = value
            End Set
        End Property

    End Class
    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
        Public Class clsAdministClienteBE
        'Inherits clsClienteBaseBE
        ''Inherits ServicedComponent
        'Inherits ServicedComponent

        Private _IDCliente As String
        Public Property IDCliente() As String
            Get
                Return _IDCliente
            End Get
            Set(ByVal value As String)
                _IDCliente = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _IDAdminist As String
        Public Property IDAdminist() As String
            Get
                Return _IDAdminist
            End Get
            Set(ByVal value As String)
                _IDAdminist = value
            End Set
        End Property


        Private _Banco As String
        Public Property Banco() As String
            Get
                Return _Banco
            End Get
            Set(ByVal value As String)
                _Banco = value
            End Set
        End Property

        Private _CtaCte As String
        Public Property CtaCte() As String
            Get
                Return _CtaCte
            End Get
            Set(ByVal value As String)
                _CtaCte = value
            End Set
        End Property

        Private _IDMoneda As String
        Public Property IDMoneda() As String
            Get
                Return _IDMoneda
            End Get
            Set(ByVal value As String)
                _IDMoneda = value
            End Set
        End Property

        Private _SWIFT As String
        Public Property SWIFT() As String
            Get
                Return _SWIFT
            End Get
            Set(ByVal value As String)
                _SWIFT = value
            End Set
        End Property


        Private _IVAN As String
        Public Property IVAN() As String
            Get
                Return _IVAN
            End Get
            Set(ByVal value As String)
                _IVAN = value
            End Set
        End Property

        Private _NombresTitular As String
        Public Property NombresTitular() As String
            Get
                Return _NombresTitular
            End Get
            Set(ByVal value As String)
                _NombresTitular = value
            End Set
        End Property

        Private _ApellidosTitular As String
        Public Property ApellidosTitular() As String
            Get
                Return _ApellidosTitular
            End Get
            Set(ByVal value As String)
                _ApellidosTitular = value
            End Set
        End Property

        Private _ReferenciaAdminist As String
        Public Property ReferenciaAdminist() As String
            Get
                Return _ReferenciaAdminist
            End Get
            Set(ByVal value As String)
                _ReferenciaAdminist = value
            End Set
        End Property

        Private _CtaInter As String
        Public Property CtaInter() As String
            Get
                Return _CtaInter
            End Get
            Set(ByVal value As String)
                _CtaInter = value
            End Set
        End Property

        Private _BancoInterm As String
        Public Property BancoInterm() As String
            Get
                Return _BancoInterm
            End Get
            Set(ByVal value As String)
                _BancoInterm = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaAdministCliente As List(Of clsAdministClienteBE)
    Public Property ListaAdministCliente() As List(Of clsAdministClienteBE)
        Get
            Return _ListaAdministCliente
        End Get
        Set(ByVal value As List(Of clsAdministClienteBE))
            _ListaAdministCliente = value
        End Set
    End Property

    Private _ListaSeriesCliente As List(Of clsSeriesClienteBE)
    Public Property ListaSeriesCliente() As List(Of clsSeriesClienteBE)
        Get
            Return _ListaSeriesCliente
        End Get
        Set(ByVal value As List(Of clsSeriesClienteBE))
            _ListaSeriesCliente = value
        End Set
    End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
        Public Class clsSeguimientoClienteBE
        'Inherits clsClienteBaseBE
        ''Inherits ServicedComponent
        'Inherits ServicedComponent

        Private _IDCliente As String
        Public Property IDCliente() As String
            Get
                Return _IDCliente
            End Get
            Set(ByVal value As String)
                _IDCliente = value
            End Set
        End Property
        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _IDSeguimiento As Int32
        Public Property IDSeguimiento() As Int32
            Get
                Return _IDSeguimiento
            End Get
            Set(ByVal value As Int32)
                _IDSeguimiento = value
            End Set
        End Property


        Private _IDContacto As String
        Public Property IDContacto() As String
            Get
                Return _IDContacto
            End Get
            Set(ByVal value As String)
                _IDContacto = value
            End Set
        End Property

        Private _Notas As String
        Public Property Notas() As String
            Get
                Return _Notas
            End Get
            Set(ByVal value As String)
                _Notas = value
            End Set
        End Property

    End Class


    Public Class clsClientes_Perfil_GuiasBE
        Dim _CoCliente As String
        Dim _CoProveedor As String
        Dim _FlActivo As Boolean
        Dim _UserMod As String


        Public Property CoCliente() As String
            Get
                Return _CoCliente
            End Get
            Set(ByVal Value As String)
                _CoCliente = Value
            End Set
        End Property
        Public Property CoProveedor() As String
            Get
                Return _CoProveedor
            End Get
            Set(ByVal Value As String)
                _CoProveedor = Value
            End Set
        End Property
        Public Property FlActivo() As Boolean
            Get
                Return _FlActivo
            End Get
            Set(ByVal Value As Boolean)
                _FlActivo = Value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal Value As String)
                _UserMod = Value
            End Set
        End Property
        
    End Class

    Private _ListaPerfilGuia As List(Of clsClientes_Perfil_GuiasBE)
    Public Property ListaPerfilGuia() As List(Of clsClientes_Perfil_GuiasBE)
        Get
            Return _ListaPerfilGuia
        End Get
        Set(ByVal value As List(Of clsClientes_Perfil_GuiasBE))
            _ListaPerfilGuia = value
        End Set
    End Property
End Class
