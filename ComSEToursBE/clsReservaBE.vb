﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsReservaBE
    'Inherits ServicedComponent
    Private _IDReserva As String
    Public Property IDReserva() As String
        Get
            Return _IDReserva
        End Get
        Set(ByVal value As String)
            _IDReserva = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _IDFile As String
    Public Property IDFile() As String
        Get
            Return _IDFile
        End Get
        Set(ByVal value As String)
            _IDFile = value
        End Set
    End Property

    Private _Estado As String
    Public Property Estado() As String
        Get
            Return _Estado
        End Get
        Set(ByVal value As String)
            _Estado = value
        End Set
    End Property

    Private _EstadoSolicitud As String
    Public Property EstadoSolicitud() As String
        Get
            Return _EstadoSolicitud
        End Get
        Set(ByVal value As String)
            _EstadoSolicitud = value
        End Set
    End Property

    Private _IDUsuarioVen As String
    Public Property IDUsuarioVen() As String
        Get
            Return _IDUsuarioVen
        End Get
        Set(ByVal value As String)
            _IDUsuarioVen = value
        End Set
    End Property

    Private _IDUsuarioRes As String
    Public Property IDUsuarioRes() As String
        Get
            Return _IDUsuarioRes
        End Get
        Set(ByVal value As String)
            _IDUsuarioRes = value
        End Set
    End Property

    Private _Observaciones As String
    Public Property Observaciones() As String
        Get
            Return _Observaciones
        End Get
        Set(ByVal value As String)
            _Observaciones = value
        End Set
    End Property

    Private _CodReservaProv As String
    Public Property CodReservaProv() As String
        Get
            Return _CodReservaProv
        End Get
        Set(ByVal value As String)
            _CodReservaProv = value
        End Set
    End Property

    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

    Private _FecInicio As Date
    Public Property FecInicio() As Date
        Get
            Return _FecInicio
        End Get
        Set(ByVal value As Date)
            _FecInicio = value
        End Set
    End Property

    Private _NroPax As Int16
    Public Property NroPax() As Int16
        Get
            Return _NroPax
        End Get
        Set(ByVal value As Int16)
            _NroPax = value
        End Set
    End Property

    Private _NroLiberados As Int16
    Public Property NroLiberados() As String
        Get
            Return _NroLiberados
        End Get
        Set(ByVal value As String)
            _NroLiberados = value
        End Set
    End Property

    Private _Accion As Char
    Public Property Accion() As Char
        Get
            Return _Accion
        End Get
        Set(ByVal value As Char)
            _Accion = value
        End Set
    End Property

    Private _EstadoFile As Char
    Public Property EstadoFile() As Char
        Get
            Return _EstadoFile
        End Get
        Set(ByVal value As Char)
            _EstadoFile = value
        End Set
    End Property

    Private _RegeneradoxAcomodo As Boolean
    Public Property RegeneradoxAcomodo() As Boolean
        Get
            Return _RegeneradoxAcomodo
        End Get
        Set(ByVal value As Boolean)
            _RegeneradoxAcomodo = value
        End Set
    End Property

    Private _IDReservaProtecc As Integer
    Public Property IDReservaProtecc() As Integer
        Get
            Return _IDReservaProtecc
        End Get
        Set(ByVal value As Integer)
            _IDReservaProtecc = value
        End Set
    End Property

    ''PPMG20160311
    Private _FlEnviarMPWP As Boolean
    Public Property FlEnviarMPWP() As Boolean
        Get
            Return _FlEnviarMPWP
        End Get
        Set(ByVal value As Boolean)
            _FlEnviarMPWP = value
        End Set
    End Property

    Private _FlSoloAlojamientos As Boolean
    Public Property FlSoloAlojamientos() As Boolean
        Get
            Return _FlSoloAlojamientos
        End Get
        Set(ByVal value As Boolean)
            _FlSoloAlojamientos = value
        End Set
    End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsDetalleReservaBE
        Inherits clsCotizacionBE.clsDetalleCotizacionBE

        Private _IDReserva As String
        Public Overloads Property IDReserva() As String
            Get
                Return _IDReserva
            End Get
            Set(ByVal value As String)
                _IDReserva = value
            End Set
        End Property
        'Private _IDReserva_Det As String
        'Public Property IDReserva_Det() As String
        '    Get
        '        Return _IDReserva_Det
        '    End Get
        '    Set(ByVal value As String)
        '        _IDReserva_Det = value
        '    End Set
        'End Property
        Private _IDFile As String
        Public Property IDFile() As String
            Get
                Return _IDFile
            End Get
            Set(ByVal value As String)
                _IDFile = value
            End Set
        End Property

        Private _IDReserva_Det_Rel As Integer
        Public Property IDReserva_Det_Rel() As Integer
            Get
                Return _IDReserva_Det_Rel
            End Get
            Set(ByVal value As Integer)
                _IDReserva_Det_Rel = value
            End Set
        End Property

        Private _IDReserva_DetCopia As Int32
        Public Property IDReserva_DetCopia() As Int32
            Get
                Return _IDReserva_DetCopia
            End Get
            Set(ByVal value As Int32)
                _IDReserva_DetCopia = value
            End Set
        End Property

        Private _Cantidad As Byte
        Public Property Cantidad() As Byte
            Get
                Return _Cantidad
            End Get
            Set(ByVal value As Byte)
                _Cantidad = value
            End Set
        End Property

        Private _CantidadAPagar As Single
        Public Property CantidadAPagar() As Single
            Get
                Return _CantidadAPagar
            End Get
            Set(ByVal value As Single)
                _CantidadAPagar = value
            End Set
        End Property

        Private _FechaOut As Date
        Public Property FechaOut() As Date
            Get
                Return _FechaOut
            End Get
            Set(ByVal value As Date)
                _FechaOut = value
            End Set
        End Property

        Private _Noches As Byte
        Public Property Noches() As Byte
            Get
                Return _Noches
            End Get
            Set(ByVal value As Byte)
                _Noches = value
            End Set
        End Property


        Private _IDEmailEdit As String
        Public Property IDEmailEdit() As String
            Get
                Return _IDEmailEdit
            End Get
            Set(ByVal value As String)
                _IDEmailEdit = value
            End Set
        End Property

        Private _IDEmailNew As String
        Public Property IDEmailNew() As String
            Get
                Return _IDEmailNew
            End Get
            Set(ByVal value As String)
                _IDEmailNew = value
            End Set
        End Property

        Private _IDEmailRef As String
        Public Property IDEmailRef() As String
            Get
                Return _IDEmailRef
            End Get
            Set(ByVal value As String)
                _IDEmailRef = value
            End Set
        End Property

        Private _CapacidadHab As Byte
        Public Property CapacidadHab() As Byte
            Get
                Return _CapacidadHab
            End Get
            Set(ByVal value As Byte)
                _CapacidadHab = value
            End Set
        End Property

        Private _EsMatrimonial As Boolean
        Public Overloads Property EsMatrimonial() As Boolean
            Get
                Return _EsMatrimonial
            End Get
            Set(ByVal value As Boolean)
                _EsMatrimonial = value
            End Set
        End Property

        Private _NetoHab As Double
        Public Property NetoHab() As Double
            Get
                Return _NetoHab
            End Get
            Set(ByVal value As Double)
                _NetoHab = value
            End Set
        End Property

        Private _IgvHab As Single
        Public Property IgvHab() As Single
            Get
                Return _IgvHab
            End Get
            Set(ByVal value As Single)
                _IgvHab = value
            End Set
        End Property

        Private _TotalHab As Double
        Public Property TotalHab() As Double
            Get
                Return _TotalHab
            End Get
            Set(ByVal value As Double)
                _TotalHab = value
            End Set
        End Property

        Private _NetoGen As Double
        Public Property NetoGen() As Double
            Get
                Return _NetoGen
            End Get
            Set(ByVal value As Double)
                _NetoGen = value
            End Set
        End Property

        Private _IgvGen As Single
        Public Property IgvGen() As Single
            Get
                Return _IgvGen
            End Get
            Set(ByVal value As Single)
                _IgvGen = value
            End Set
        End Property

        Private _TotalGen As Double
        Public Property TotalGen() As Double
            Get
                Return _TotalGen
            End Get
            Set(ByVal value As Double)
                _TotalGen = value
            End Set
        End Property

        'Private _DescGuia As String
        'Public Property DescGuia() As String
        '    Get
        '        Return _DescGuia
        '    End Get
        '    Set(ByVal value As String)
        '        _DescGuia = value
        '    End Set
        'End Property

        Private _IDGuiaProveedor As String
        Public Property IDGuiaProveedor() As String
            Get
                Return _IDGuiaProveedor
            End Get
            Set(ByVal value As String)
                _IDGuiaProveedor = value
            End Set
        End Property

        Private _IDPais As String
        Public Overloads Property IDPais() As String
            Get
                Return _IDPais
            End Get
            Set(ByVal value As String)
                _IDPais = value
            End Set
        End Property

        Private _FlLiberoHotel As Boolean
        Public Property FlLiberoHotel() As Boolean
            Get
                Return _FlLiberoHotel
            End Get
            Set(ByVal value As Boolean)
                _FlLiberoHotel = value
            End Set
        End Property

        'Private _DescBus As String
        'Public Property DescBus() As String
        '    Get
        '        Return _DescBus
        '    End Get
        '    Set(ByVal value As String)
        '        _DescBus = value
        '    End Set
        'End Property

        Private _NuVehiculo As Byte
        Public Property NuVehiculo() As Byte
            Get
                Return _NuVehiculo
            End Get
            Set(ByVal value As Byte)
                _NuVehiculo = value
            End Set
        End Property


        Private _IDMoneda As String
        Public Property IDMoneda() As String
            Get
                Return _IDMoneda
            End Get
            Set(ByVal value As String)
                _IDMoneda = value
            End Set
        End Property

        Private _SimboloMoneda As String
        Public Property SimboloMoneda() As String
            Get
                Return _SimboloMoneda
            End Get
            Set(ByVal value As String)
                _SimboloMoneda = value
            End Set
        End Property

        Private _EsTransporteAutogenerado As Boolean
        Public Property EsTransporteAutogenerado() As Boolean
            Get
                Return _EsTransporteAutogenerado
            End Get
            Set(ByVal value As Boolean)
                _EsTransporteAutogenerado = value
            End Set
        End Property

        Private _DescServicio As String
        Public Property DescServicio() As String
            Get
                Return _DescServicio
            End Get
            Set(ByVal value As String)
                _DescServicio = value
            End Set
        End Property

        Private _FlServicioParaGuia As Boolean
        Public Overloads Property FlServicioParaGuia() As Boolean
            Get
                Return _FlServicioParaGuia
            End Get
            Set(ByVal value As Boolean)
                _FlServicioParaGuia = value
            End Set
        End Property

        Private _FlServNoShow As Boolean
        Public Property FlServNoShow() As Boolean
            Get
                Return _FlServNoShow
            End Get
            Set(ByVal value As Boolean)
                _FlServNoShow = value
            End Set
        End Property

        Private _CantidadAPagarEditado As Boolean
        Public Property CantidadAPagarEditado() As Boolean
            Get
                Return _CantidadAPagarEditado
            End Get
            Set(ByVal value As Boolean)
                _CantidadAPagarEditado = value
            End Set
        End Property

        Private _NetoHabEditado As Boolean
        Public Property NetoHabEditado() As Boolean
            Get
                Return _NetoHabEditado
            End Get
            Set(ByVal value As Boolean)
                _NetoHabEditado = value
            End Set
        End Property

        Private _IgvHabEditado As Boolean
        Public Property IgvHabEditado() As Boolean
            Get
                Return _IgvHabEditado
            End Get
            Set(ByVal value As Boolean)
                _IgvHabEditado = value
            End Set
        End Property

        Private _FlAcomodoResidente As Boolean
        Public Property FlAcomodoResidente() As Boolean
            Get
                Return _FlAcomodoResidente
            End Get
            Set(ByVal value As Boolean)
                _FlAcomodoResidente = value
            End Set
        End Property

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
                Public Class clsPaxDetalleReservaBE
            Inherits clsCotizacionBE.clsCotizacionPaxBE.clsCotizacionDetPaxBE

            Private _IDReserva_Det As String
            Public Property IDReserva_Det() As String
                Get
                    Return _IDReserva_Det
                End Get
                Set(ByVal value As String)
                    _IDReserva_Det = value
                End Set
            End Property

            Private _IDReserva_DetAcomodoRealTMP As Integer
            Public Property IDReserva_DetAcomodoRealTMP() As Integer
                Get
                    Return _IDReserva_DetAcomodoRealTMP
                End Get
                Set(ByVal value As Integer)
                    _IDReserva_DetAcomodoRealTMP = value
                End Set
            End Property


            'Private _IDServicio_Det As Integer
            'Public Property IDServicio_Det() As Integer
            '    Get
            '        Return _IDServicio_Det
            '    End Get
            '    Set(ByVal value As Integer)
            '        _IDServicio_Det = value
            '    End Set
            'End Property

            'Private _Dia As Date
            'Public Property Dia() As Date
            '    Get
            '        Return _Dia
            '    End Get
            '    Set(ByVal value As Date)
            '        _Dia = value
            '    End Set
            'End Property

            'Private _CapacidadHab As Byte
            'Public Property CapacidadHab() As Byte
            '    Get
            '        Return _CapacidadHab
            '    End Get
            '    Set(ByVal value As Byte)
            '        _CapacidadHab = value
            '    End Set
            'End Property

            'Private _EsMatrimonial As Boolean
            'Public Property EsMatrimonial() As Boolean
            '    Get
            '        Return _EsMatrimonial
            '    End Get
            '    Set(ByVal value As Boolean)
            '        _EsMatrimonial = value
            '    End Set
            'End Property


        End Class

        <ComVisible(True)> _
        <Transaction(TransactionOption.NotSupported)> _
        Public Class clsDetalleReservaDetServiciosBE
            Inherits clsDetalleCotizacionDetServiciosBE

            'Private _IDReserva_Det As Integer
            'Public Property IDReserva_Det() As Integer
            '    Get
            '        Return _IDReserva_Det
            '    End Get
            '    Set(ByVal value As Integer)
            '        _IDReserva_Det = value
            '    End Set
            'End Property

        End Class

        Public Class clsReservasDetEstadoHabitBE
            Inherits clsDetalleReservaBE

            Private _NuIngreso As Byte
            Public Property NuIngreso() As Byte
                Get
                    Return _NuIngreso
                End Get
                Set(ByVal value As Byte)
                    _NuIngreso = value
                End Set
            End Property

            Private _Simple As Byte
            Public Property Simple() As Byte
                Get
                    Return _Simple
                End Get
                Set(ByVal value As Byte)
                    _Simple = value
                End Set
            End Property

            Private _Twin As Byte
            Public Property Twin() As Byte
                Get
                    Return _Twin
                End Get
                Set(ByVal value As Byte)
                    _Twin = value
                End Set
            End Property

            Private _Matrimonial As Byte
            Public Property Matrimonial() As Byte
                Get
                    Return _Matrimonial
                End Get
                Set(ByVal value As Byte)
                    _Matrimonial = value
                End Set
            End Property

            Private _Triple As Byte
            Public Property Triple() As Byte
                Get
                    Return _Triple
                End Get
                Set(ByVal value As Byte)
                    _Triple = value
                End Set
            End Property

        End Class
    End Class

    Public Class clsTareasReservaBE
        'Inherits ServicedComponent
        Private _IDReserva As String
        Public Property IDReserva() As String
            Get
                Return _IDReserva
            End Get
            Set(ByVal value As String)
                _IDReserva = value
            End Set
        End Property
        Private _IDTarea As Int16
        Public Property IDTarea() As Int16
            Get
                Return _IDTarea
            End Get
            Set(ByVal value As Int16)
                _IDTarea = value
            End Set
        End Property

        Private _FecDeadLine As Date
        Public Property FecDeadLine() As Date
            Get
                Return _FecDeadLine
            End Get
            Set(ByVal value As Date)
                _FecDeadLine = value
            End Set
        End Property

        Private _IDEstado As String
        Public Property IDEstado() As String
            Get
                Return _IDEstado
            End Get
            Set(ByVal value As String)
                _IDEstado = value
            End Set
        End Property

        Private _IDCab As Integer
        Public Property IDCab() As Integer
            Get
                Return _IDCab
            End Get
            Set(ByVal value As Integer)
                _IDCab = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Descripcion As String
        Public Property Descripcion() As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property

        Private _UltimoMinuto As Boolean
        Public Property UltimoMinuto() As Boolean
            Get
                Return _UltimoMinuto
            End Get
            Set(ByVal value As Boolean)
                _UltimoMinuto = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property
    End Class
    Public Class clsAcomodoPaxProveedorBE
        'Inherits ServicedComponent
        Private _IDReserva As String
        Public Property IDReserva() As String
            Get
                Return _IDReserva
            End Get
            Set(ByVal value As String)
                _IDReserva = value
            End Set
        End Property

        Private _IDPax As Integer
        Public Property IDPax() As Integer
            Get
                Return _IDPax
            End Get
            Set(ByVal value As Integer)
                _IDPax = value
            End Set
        End Property

        Private _IdHabit As Int16
        Public Property IdHabit() As Int16
            Get
                Return _IdHabit
            End Get
            Set(ByVal value As Int16)
                _IdHabit = value
            End Set
        End Property

        Private _CapacidadHabit As Int16
        Public Property CapacidadHabit() As Int16
            Get
                Return _CapacidadHabit
            End Get
            Set(ByVal value As Int16)
                _CapacidadHabit = value
            End Set
        End Property

        Private _EsMatrimonial As Boolean
        Public Property EsMatrimonial() As Boolean
            Get
                Return _EsMatrimonial
            End Get
            Set(ByVal value As Boolean)
                _EsMatrimonial = value
            End Set
        End Property


        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property
    End Class

    Public Class clsAcomodoPaxBE
        'Inherits clsReservaBE
        'Inherits ServicedComponent
        Private _IDCAB As Integer
        Public Property IDCAB() As Integer
            Get
                Return _IDCAB
            End Get
            Set(ByVal value As Integer)
                _IDCAB = value
            End Set
        End Property

        Private _IDPax As Integer
        Public Property IDPax() As Integer
            Get
                Return _IDPax
            End Get
            Set(ByVal value As Integer)
                _IDPax = value
            End Set
        End Property

        Private _CapacidadHabit As Int16
        Public Property CapacidadHabit() As Int16
            Get
                Return _CapacidadHabit
            End Get
            Set(ByVal value As Int16)
                _CapacidadHabit = value
            End Set
        End Property

        Private _IdHabit As Int16
        Public Property IdHabit() As Int16
            Get
                Return _IdHabit
            End Get
            Set(ByVal value As Int16)
                _IdHabit = value
            End Set
        End Property

        Private _EsMatrimonial As Boolean
        Public Property EsMatrimonial() As Boolean
            Get
                Return _EsMatrimonial
            End Get
            Set(ByVal value As Boolean)
                _EsMatrimonial = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property


        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class


    Public Class clsServiciosCambiosVentasReservasBE

        Private _IDReserva_Det As Integer
        Public Property IDReserva_Det() As Integer
            Get
                Return _IDReserva_Det
            End Get
            Set(ByVal value As Integer)
                _IDReserva_Det = value
            End Set
        End Property

        Private _IDOperacion_Det As Integer
        Public Property IDOperacion_Det() As Integer
            Get
                Return _IDOperacion_Det
            End Get
            Set(ByVal value As Integer)
                _IDOperacion_Det = value
            End Set
        End Property

        Private _IDServicio_Det As Integer
        Public Property IDServicio_Det() As Integer
            Get
                Return _IDServicio_Det
            End Get
            Set(ByVal value As Integer)
                _IDServicio_Det = value
            End Set
        End Property

        Private _Dia As Date
        Public Property Dia() As Date
            Get
                Return _Dia
            End Get
            Set(ByVal value As Date)
                _Dia = value
            End Set
        End Property

        Private _CapacidadHab As Byte
        Public Property CapacidadHab() As Byte
            Get
                Return _CapacidadHab
            End Get
            Set(ByVal value As Byte)
                _CapacidadHab = value
            End Set
        End Property

        Private _EsMatrimonial As Boolean
        Public Property EsMatrimonial() As Boolean
            Get
                Return _EsMatrimonial
            End Get
            Set(ByVal value As Boolean)
                _EsMatrimonial = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

        Private _NuevaTarifa As Boolean
        Public Property NuevaTarifa() As Boolean
            Get
                Return _NuevaTarifa
            End Get
            Set(ByVal value As Boolean)
                _NuevaTarifa = value
            End Set
        End Property

    End Class

    Private _ListaServiciosCambiosVentasReservas As List(Of clsServiciosCambiosVentasReservasBE)
    Public Property ListaServiciosCamb() As List(Of clsServiciosCambiosVentasReservasBE)
        Get
            Return _ListaServiciosCambiosVentasReservas
        End Get
        Set(ByVal value As List(Of clsServiciosCambiosVentasReservasBE))
            _ListaServiciosCambiosVentasReservas = value
        End Set
    End Property




    Private _ListaAcomodPaxReservasProveedor As List(Of clsAcomodoPaxProveedorBE)
    Public Property ListaAcomodPaxReservasProveedor() As List(Of clsAcomodoPaxProveedorBE)
        Get
            Return _ListaAcomodPaxReservasProveedor
        End Get
        Set(ByVal value As List(Of clsAcomodoPaxProveedorBE))
            _ListaAcomodPaxReservasProveedor = value
        End Set
    End Property


    Private _ListaAcomodPaxReservas As List(Of clsAcomodoPaxBE)
    Public Property ListaAcomodoPaxReservas() As List(Of clsAcomodoPaxBE)
        Get
            Return _ListaAcomodPaxReservas
        End Get
        Set(ByVal value As List(Of clsAcomodoPaxBE))
            _ListaAcomodPaxReservas = value
        End Set
    End Property


    Private _ListaReservas As List(Of clsReservaBE)
    Public Property ListaReservas() As List(Of clsReservaBE)
        Get
            Return _ListaReservas
        End Get
        Set(ByVal value As List(Of clsReservaBE))
            _ListaReservas = value
        End Set
    End Property


    Private _ListaDetReservas As List(Of clsReservaBE.clsDetalleReservaBE)
    Public Property ListaDetReservas() As List(Of clsReservaBE.clsDetalleReservaBE)
        Get
            Return _ListaDetReservas
        End Get
        Set(ByVal value As List(Of clsReservaBE.clsDetalleReservaBE))
            _ListaDetReservas = value
        End Set
    End Property


    Private _ListaPaxDetReservas As List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
    Public Property ListaPaxDetReservas() As List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE)
        Get
            Return _ListaPaxDetReservas
        End Get
        Set(ByVal value As List(Of clsReservaBE.clsDetalleReservaBE.clsPaxDetalleReservaBE))
            _ListaPaxDetReservas = value
        End Set
    End Property

    Private _ListaDetServiciosDetReservas As List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE)
    Public Property ListaDetServiciosDetReservas() As List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE)
        Get
            Return _ListaDetServiciosDetReservas
        End Get
        Set(ByVal value As List(Of clsReservaBE.clsDetalleReservaBE.clsDetalleReservaDetServiciosBE))
            _ListaDetServiciosDetReservas = value
        End Set
    End Property

    Private _ListaTareasReservas As List(Of clsTareasReservaBE)
    Public Property ListaTareasReservas() As List(Of clsTareasReservaBE)
        Get
            Return _ListaTareasReservas
        End Get
        Set(ByVal value As List(Of clsTareasReservaBE))
            _ListaTareasReservas = value
        End Set
    End Property

    Private _ListaUltimoMinuto As List(Of Char)
    Public Property ListaUltimoMinuto() As List(Of Char)
        Get
            Return _ListaUltimoMinuto
        End Get
        Set(ByVal value As List(Of Char))
            _ListaUltimoMinuto = value
        End Set
    End Property


    Private _ListaPax As List(Of clsCotizacionBE.clsCotizacionPaxBE)
    Public Property ListaPax() As List(Of clsCotizacionBE.clsCotizacionPaxBE)
        Get
            Return _ListaPax
        End Get
        Set(ByVal value As List(Of clsCotizacionBE.clsCotizacionPaxBE))
            _ListaPax = value
        End Set
    End Property

    Private _ListaCotizacionVuelos As List(Of clsCotizacionBE.clsCotizacionVuelosBE)
    Public Property ListaCotizacionVuelos() As List(Of clsCotizacionBE.clsCotizacionVuelosBE)
        Get
            Return _ListaCotizacionVuelos
        End Get
        Set(ByVal value As List(Of clsCotizacionBE.clsCotizacionVuelosBE))
            _ListaCotizacionVuelos = value
        End Set
    End Property


    Private _ListaPaxTransporte As List(Of clsCotizacionBE.clsPaxTransporteBE)
    Public Property ListaPaxTransporte() As List(Of clsCotizacionBE.clsPaxTransporteBE)
        Get
            Return _ListaPaxTransporte
        End Get
        Set(ByVal value As List(Of clsCotizacionBE.clsPaxTransporteBE))
            _ListaPaxTransporte = value
        End Set
    End Property


    Private _ListaDetReservasEstadoHabit As List(Of clsReservaBE.clsDetalleReservaBE.clsReservasDetEstadoHabitBE)
    Public Property ListaDetReservasEstadoHabit() As List(Of clsReservaBE.clsDetalleReservaBE.clsReservasDetEstadoHabitBE)
        Get
            Return _ListaDetReservasEstadoHabit
        End Get
        Set(ByVal value As List(Of clsReservaBE.clsDetalleReservaBE.clsReservasDetEstadoHabitBE))
            _ListaDetReservasEstadoHabit = value
        End Set
    End Property


    '*****************************************************
    'Acción: Agregar campos de tabla y lista de MARECORDATORIOSRESERVAS
    'Responsable: Anthony Huamani C.
    'Fecha: 01/02/2012

    Public Class clsRecordatoriosBE
        'Inherits ServicedComponent
        Private _IDRecordatorio As Short
        Public Property IDRecordatorio() As Short
            Get
                Return _IDRecordatorio
            End Get
            Set(ByVal value As Short)
                _IDRecordatorio = value
            End Set
        End Property

        Private _Descripcion As String
        Public Property Descripcion() As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property

        Private _TipoCalculoTarea As String
        Public Property TipoCalculoTarea() As String
            Get
                Return _TipoCalculoTarea
            End Get
            Set(ByVal value As String)
                _TipoCalculoTarea = value
            End Set
        End Property

        Private _Dias As Byte
        Public Property Dias() As Byte
            Get
                Return _Dias
            End Get
            Set(ByVal value As Byte)
                _Dias = value
            End Set
        End Property

        Private _Activo As Boolean
        Public Property Activo() As Boolean
            Get
                Return _Activo
            End Get
            Set(ByVal value As Boolean)
                _Activo = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

    End Class

    Public Class clsMensajesReservasTelefBE
        Inherits clsReservaBE

        Private _IDMensaje As String
        Public Property IDMensaje() As String
            Get
                Return _IDMensaje
            End Get
            Set(ByVal value As String)
                _IDMensaje = value
            End Set
        End Property

        Private _Contacto As String
        Public Property Contacto() As String
            Get
                Return _Contacto
            End Get
            Set(ByVal value As String)
                _Contacto = value
            End Set
        End Property

        Private _IDUsuario As String
        Public Property IDUsuario() As String
            Get
                Return _IDUsuario
            End Get
            Set(ByVal value As String)
                _IDUsuario = value
            End Set
        End Property

        Private _Fecha As Date
        Public Property Fecha() As Date
            Get
                Return _Fecha
            End Get
            Set(ByVal value As Date)
                _Fecha = value
            End Set
        End Property

        Private _Asunto As String
        Public Property Asunto() As String
            Get
                Return _Asunto
            End Get
            Set(ByVal value As String)
                _Asunto = value
            End Set
        End Property

        Private _Mensaje As String
        Public Property Mensaje() As String
            Get
                Return _Mensaje
            End Get
            Set(ByVal value As String)
                _Mensaje = value
            End Set
        End Property
    End Class


    Private _ListaRecordatorios As List(Of clsRecordatoriosBE)
    Public Property ListaRecordatorios() As List(Of clsRecordatoriosBE)
        Get
            Return _ListaRecordatorios
        End Get
        Set(ByVal value As List(Of clsRecordatoriosBE))
            _ListaRecordatorios = value
        End Set
    End Property

    '******************************************************
    Private _ListaNoShow As List(Of Integer)
    Public Property ListaNoShow() As List(Of Integer)
        Get
            Return _ListaNoShow
        End Get
        Set(ByVal value As List(Of Integer))
            _ListaNoShow = value
        End Set
    End Property

    Private _ListaShow As List(Of Integer)
    Public Property ListaShow() As List(Of Integer)
        Get
            Return _ListaShow
        End Get
        Set(ByVal value As List(Of Integer))
            _ListaShow = value
        End Set
    End Property

End Class
