﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports Newtonsoft.Json

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsTablaMaestroBasePwBE
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsTablaMaestroPwBE

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class ResponseStatusPaymentwall
    Private _ErrCode As String
    Public Property ErrCode() As String
        Get
            Return _ErrCode
        End Get
        Set(ByVal value As String)
            _ErrCode = value
        End Set
    End Property

    Private _ErrMsg As String
    Public Property ErrMsg() As String
        Get
            Return _ErrMsg
        End Get
        Set(ByVal value As String)
            _ErrMsg = value
        End Set
    End Property

    Private _Id As String
    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal value As String)
            _Id = value
        End Set
    End Property

End Class