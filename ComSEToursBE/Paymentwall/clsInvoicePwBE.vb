﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsInvoicePwBE

    Private _invoice_number As String
    Private _currency As String
    Private _date As String
    Private _due_date As String
    Private _contacts As List(Of clsContactInvoicePwBE)
    Private _items As List(Of clsItemInvoicePwBE)
    Private _key As String
    Private _sign As String
    Private _sign_version As String

    Public Property invoice_number() As String
        Get
            Return _invoice_number
        End Get
        Set(ByVal value As String)
            _invoice_number = value
        End Set
    End Property

    Public Property currency() As String
        Get
            Return _currency
        End Get
        Set(ByVal value As String)
            _currency = value
        End Set
    End Property

    Public Property [date]() As String
        Get
            Return _date
        End Get
        Set(ByVal value As String)
            _date = value
        End Set
    End Property

    Public Property due_date() As String
        Get
            Return _due_date
        End Get
        Set(ByVal value As String)
            _due_date = value
        End Set
    End Property

    Public Overloads Property contacts() As List(Of clsContactInvoicePwBE)
        Get
            Return _contacts
        End Get
        Set(ByVal value As List(Of clsContactInvoicePwBE))
            _contacts = value
        End Set
    End Property

    Public Overloads Property items() As List(Of clsItemInvoicePwBE)
        Get
            Return _items
        End Get
        Set(ByVal value As List(Of clsItemInvoicePwBE))
            _items = value
        End Set
    End Property

    Public Property key() As String
        Get
            Return _key
        End Get
        Set(ByVal value As String)
            _key = value
        End Set
    End Property

    Public Property sign() As String
        Get
            Return _sign
        End Get
        Set(ByVal value As String)
            _sign = value
        End Set
    End Property

    Public Property sign_version() As String
        Get
            Return _sign_version
        End Get
        Set(ByVal value As String)
            _sign_version = value
        End Set
    End Property

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsContactInvoicePwBE
        Private _email As String
        Private _first_name As String
        Private _last_name As String
        Private _company_name As String
        Private _salutation As String

        Public Property email() As String
            Get
                Return _email
            End Get
            Set(ByVal value As String)
                _email = value
            End Set
        End Property

        Public Property first_name() As String
            Get
                Return _first_name
            End Get
            Set(ByVal value As String)
                _first_name = value
            End Set
        End Property

        Public Property company_name() As String
            Get
                Return _company_name
            End Get
            Set(ByVal value As String)
                _company_name = value
            End Set
        End Property

        Public Property last_name() As String
            Get
                Return _last_name
            End Get
            Set(ByVal value As String)
                _last_name = value
            End Set
        End Property

        Public Property salutation() As String
            Get
                Return _salutation
            End Get
            Set(ByVal value As String)
                _salutation = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsItemInvoicePwBE
        Private _quantity As Integer
        Private _unit_cost As Single
        Private _currency As String
        Private _title As String
        Private _tax As New clsTaxInvoicePwBE()

        Public Property quantity() As Integer
            Get
                Return _quantity
            End Get
            Set(ByVal value As Integer)
                _quantity = value
            End Set
        End Property

        Public Property unit_cost() As Single
            Get
                Return _unit_cost
            End Get
            Set(ByVal value As Single)
                _unit_cost = value
            End Set
        End Property

        Public Property currency() As String
            Get
                Return _currency
            End Get
            Set(ByVal value As String)
                _currency = value
            End Set
        End Property

        Public Property title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property

        Public Overloads Property tax() As clsTaxInvoicePwBE
            Get
                Return _tax
            End Get
            Set(ByVal value As clsTaxInvoicePwBE)
                _tax = value
            End Set
        End Property
    End Class

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsTaxInvoicePwBE
        Private _type As String
        Private _value As Single

        Public Property type() As String
            Get
                Return _type
            End Get
            Set(ByVal value As String)
                _type = value
            End Set
        End Property

        Public Property value() As String
            Get
                Return _value
            End Get
            Set(ByVal value As String)
                _value = value
            End Set
        End Property

        'Public Sub New(ByVal ptype As String, ByVal pvalue As Single)
        '    _type = ptype
        '    _value = pvalue
        'End Sub

        'Public Sub New()
        'End Sub
    End Class
End Class
