﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports Newtonsoft.Json

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsTablasMaestroBaseBE
    '<NonSerialized(), JsonIgnore()> _
    'Private _UserMod As String
    'Public Property UserMod() As String
    '    Get
    '        Return _UserMod
    '    End Get
    '    Set(ByVal value As String)
    '        _UserMod = value
    '    End Set
    'End Property

    '<NonSerialized(), JsonIgnore()> _
    'Private _FecMod As DateTime
    'Public Property FecMod() As DateTime
    '    Get
    '        Return _FecMod
    '    End Get
    '    Set(ByVal value As DateTime)
    '        _FecMod = value
    '    End Set
    'End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTablasMaestroBE
    Inherits clsTablasMaestroBaseBE

End Class

'<ComVisible(True)> _
'<Transaction(TransactionOption.NotSupported)> _
'Public Class ResponseStatusSAPBE

'    Private _ResponseStatusSAPBE As ResponseStatusSAP
'    Public Property ResponseStatusSAPBE() As ResponseStatusSAP
'        Get
'            Return _ResponseStatusSAPBE
'        End Get
'        Set(ByVal value As ResponseStatusSAP)
'            _ResponseStatusSAPBE = value
'        End Set
'    End Property

'End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class ResponseStatusSAP

    ''' <summary>
    ''' Define el número de error en la transación del API.
    ''' </summary>
    ''' <remarks></remarks>
    Private _ErrCode As String
    Public Property ErrCode() As String
        Get
            Return _ErrCode
        End Get
        Set(ByVal value As String)
            _ErrCode = value
        End Set
    End Property

    ''' <summary>
    ''' Define el mensaje de error en la transacción del API.
    ''' </summary>
    ''' <remarks></remarks>
    Private _ErrMsg As String
    Public Property ErrMsg() As String
        Get
            Return _ErrMsg
        End Get
        Set(ByVal value As String)
            _ErrMsg = value
        End Set
    End Property

    ''' <summary>
    ''' Define el código ingresado a la interfaz SAP
    ''' </summary>
    ''' <remarks></remarks>
    Private _Id As String
    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal value As String)
            _Id = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsSocioNegocioBE
    Inherits clsTablasMaestroBE

    ''' <summary>
    ''' Campo que se utiliza como llave primaria en la base de datos para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardCode As String
    Public Property CardCode() As String
        Get
            Return _CardCode
        End Get
        Set(ByVal value As String)
            _CardCode = value
        End Set
    End Property

    ''' <summary>
    ''' Campo que se utiliza como nombre en la base de datos para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardName As String
    Public Property CardName() As String
        Get
            Return _CardName
        End Get
        Set(ByVal value As String)
            _CardName = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardType As Byte
    Public Property CardType() As Byte
        Get
            Return _CardType
        End Get
        Set(ByVal value As Byte)
            _CardType = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private _GroupCode As Int16
    Public Property GroupCode() As Int16
        Get
            Return _GroupCode
        End Get
        Set(ByVal value As Int16)
            _GroupCode = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private _FederalTaxID As String
    Public Property FederalTaxID() As String
        Get
            Return _FederalTaxID
        End Get
        Set(ByVal value As String)
            _FederalTaxID = value
        End Set
    End Property

    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private _Currency As String
    Public Property Currency() As String
        Get
            Return _Currency
        End Get
        Set(ByVal value As String)
            _Currency = value
        End Set
    End Property

    ''' <summary>
    ''' Se define el tipo de persona
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_BPTP As String
    Public Property U_SYP_BPTP() As String
        Get
            Return _U_SYP_BPTP
        End Get
        Set(ByVal value As String)
            _U_SYP_BPTP = value
        End Set
    End Property

    ''' <summary>
    ''' Es el tipo de documento.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_BPTD As String
    Public Property U_SYP_BPTD() As String
        Get
            Return _U_SYP_BPTD
        End Get
        Set(ByVal value As String)
            _U_SYP_BPTD = value
        End Set
    End Property

    ''' <summary>
    ''' Es el apellido paterno.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_BPAP As String
    Public Property U_SYP_BPAP() As String
        Get
            Return _U_SYP_BPAP
        End Get
        Set(ByVal value As String)
            _U_SYP_BPAP = value
        End Set
    End Property

    ''' <summary>
    ''' Es el apellido materno
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_BPAM As String
    Public Property U_SYP_BPAM() As String
        Get
            Return _U_SYP_BPAM
        End Get
        Set(ByVal value As String)
            _U_SYP_BPAM = value
        End Set
    End Property

    ''' <summary>
    ''' Es el segundo nombre del socio de negocio
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_BPN2 As String
    Public Property U_SYP_BPN2() As String
        Get
            Return _U_SYP_BPN2
        End Get
        Set(ByVal value As String)
            _U_SYP_BPN2 = value
        End Set
    End Property

    ''' <summary>
    ''' Primer nombre del socio de negocio
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_BPNO As String
    Public Property U_SYP_BPNO() As String
        Get
            Return _U_SYP_BPNO
        End Get
        Set(ByVal value As String)
            _U_SYP_BPNO = value
        End Set
    End Property

    ''' <summary>
    ''' Campo email
    ''' </summary>
    ''' <remarks></remarks>
    Private _EmailAddress As String
    Public Property EmailAddress() As String
        Get
            Return _EmailAddress
        End Get
        Set(ByVal value As String)
            _EmailAddress = value
        End Set
    End Property

    ''' <summary>
    ''' Campo telefono 1
    ''' </summary>
    ''' <remarks></remarks>
    Private _Phone1 As String
    Public Property Phone1() As String
        Get
            Return _Phone1
        End Get
        Set(ByVal value As String)
            _Phone1 = value
        End Set
    End Property

    ''' <summary>
    ''' Campo telefono 2
    ''' </summary>
    ''' <remarks></remarks>
    Private _Phone2 As String
    Public Property Phone2() As String
        Get
            Return _Phone2
        End Get
        Set(ByVal value As String)
            _Phone2 = value
        End Set
    End Property

    ''' <summary>
    ''' Campo que identifica si el socio esta sujeto a retención
    ''' </summary>
    ''' <remarks></remarks>
    Private _SubjectToWithholdingTax As Byte
    Public Property SubjectToWithholdingTax() As Byte
        Get
            Return _SubjectToWithholdingTax
        End Get
        Set(ByVal value As Byte)
            _SubjectToWithholdingTax = value
        End Set
    End Property

    ''' <summary>
    ''' Campo que identifica si es valido
    ''' </summary>
    ''' <remarks></remarks>
    Private _Valid As Byte
    Public Property Valid() As Byte
        Get
            Return _Valid
        End Get
        Set(ByVal value As Byte)
            _Valid = value
        End Set
    End Property

    ''' <summary>
    ''' Direcciones
    ''' </summary>
    ''' <remarks></remarks>
    Private _BPAddresses As List(Of clsBPAddressesBE)
    Public Property BPAddresses() As List(Of clsBPAddressesBE)
        Get
            Return _BPAddresses
        End Get
        Set(ByVal value As List(Of clsBPAddressesBE))
            _BPAddresses = value
        End Set
    End Property

    ''' <summary>
    ''' Información de contactos
    ''' </summary>
    ''' <remarks></remarks>
    Private _ContactEmployees As List(Of clsContactEmployeesBE)
    Public Property ContactEmployees() As List(Of clsContactEmployeesBE)
        Get
            Return _ContactEmployees
        End Get
        Set(ByVal value As List(Of clsContactEmployeesBE))
            _ContactEmployees = value
        End Set
    End Property


    ''' <summary>
    ''' Información de retenciones
    ''' </summary>
    ''' <remarks></remarks>
    Private _BPWithholdingTax As List(Of clsBPWithholdingTaxBE)
    Public Property BPWithholdingTax() As List(Of clsBPWithholdingTaxBE)
        Get
            Return _BPWithholdingTax
        End Get
        Set(ByVal value As List(Of clsBPWithholdingTaxBE))
            _BPWithholdingTax = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsBPAddressesBE

    ''' <summary>
    ''' Nombre de la direccion
    ''' </summary>
    ''' <remarks></remarks>
    Private _AddressName As String
    Public Property AddressName() As String
        Get
            Return _AddressName
        End Get
        Set(ByVal value As String)
            _AddressName = value
        End Set
    End Property

    ''' <summary>
    ''' Nombre de la Calle
    ''' </summary>
    ''' <remarks></remarks>
    Private _Street As String
    Public Property Street() As String
        Get
            Return _Street
        End Get
        Set(ByVal value As String)
            _Street = value
        End Set
    End Property

    ''' <summary>
    ''' Codigo de la ciudad
    ''' </summary>
    ''' <remarks></remarks>
    Private _ZipCode As String
    Public Property ZipCode() As String
        Get
            Return _ZipCode
        End Get
        Set(ByVal value As String)
            _ZipCode = value
        End Set
    End Property

    Private _Block As String
    Public Property Block() As String
        Get
            Return _Block
        End Get
        Set(ByVal value As String)
            _Block = value
        End Set
    End Property

    ''' <summary>
    ''' bo_BillTo [Direccion Fiscal] / bo_ShipTo [Direccion de entrega]
    ''' </summary>
    ''' <remarks></remarks>
    Private _AddressType As Byte
    Public Property AddressType() As Byte
        Get
            Return _AddressType
        End Get
        Set(ByVal value As Byte)
            _AddressType = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsContactEmployeesBE
    Private _Name As String
    Public Property Name() As String
        Get
            Return _Name
        End Get
        Set(ByVal value As String)
            _Name = value
        End Set
    End Property

    Private _FirstName As String
    Public Property FirstName() As String
        Get
            Return _FirstName
        End Get
        Set(ByVal value As String)
            _FirstName = value
        End Set
    End Property

    Private _LastName As String
    Public Property LastName() As String
        Get
            Return _LastName
        End Get
        Set(ByVal value As String)
            _LastName = value
        End Set
    End Property

    Private _Address As String
    Public Property Address() As String
        Get
            Return _Address
        End Get
        Set(ByVal value As String)
            _Address = value
        End Set
    End Property

    Private _E_Mail As String
    Public Property E_Mail() As String
        Get
            Return _E_Mail
        End Get
        Set(ByVal value As String)
            _E_Mail = value
        End Set
    End Property

    Private _Active As Byte
    Public Property Active() As Byte
        Get
            Return _Active
        End Get
        Set(ByVal value As Byte)
            _Active = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsBPWithholdingTaxBE
    Private _WTCode As String
    Public Property WTCode() As String
        Get
            Return _WTCode
        End Get
        Set(ByVal value As String)
            _WTCode = value
        End Set
    End Property
End Class
