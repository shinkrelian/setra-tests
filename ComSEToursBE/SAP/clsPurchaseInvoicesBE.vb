﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPurchaseInvoicesBE

    ''' <summary>
    ''' Campo que se utiliza como fecha de documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _TaxDate As Date
    Public Property TaxDate() As Date
        Get
            Return _TaxDate
        End Get
        Set(ByVal value As Date)
            _TaxDate = value
        End Set
    End Property

    'DocDate
    ''' <summary>
    ''' Campo que se utiliza como fecha de facturación para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DocDate As Date
    Public Property DocDate() As Date
        Get
            Return _DocDate
        End Get
        Set(ByVal value As Date)
            _DocDate = value
        End Set
    End Property

    'DocDueDate
    ''' <summary>
    ''' Campo que se utiliza como fecha de vencimiento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DocDueDate As Date
    Public Property DocDueDate() As Date
        Get
            Return _DocDueDate
        End Get
        Set(ByVal value As Date)
            _DocDueDate = value
        End Set
    End Property

    'CardCode
    ''' <summary>
    ''' Campo que se utiliza como codigo del proveedor en la base de datos para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardCode As String
    Public Property CardCode() As String
        Get
            Return _CardCode
        End Get
        Set(ByVal value As String)
            _CardCode = value
        End Set
    End Property

    'CardName
    ''' <summary>
    ''' Campo que se utiliza como codigo del proveedor en la base de datos para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardName As String
    Public Property CardName() As String
        Get
            Return _CardName
        End Get
        Set(ByVal value As String)
            _CardName = value
        End Set
    End Property

    'DocType
    ''' <summary>
    ''' Campo que se utiliza como tipo de documento (Productos/Servicios) para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DocType As Byte
    Public Property DocType() As Byte
        Get
            Return _DocType
        End Get
        Set(ByVal value As Byte)
            _DocType = value
        End Set
    End Property

    'FederalTaxID
    ''' <summary>
    ''' Campo que se utiliza como RUC del proveedor para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _FederalTaxID As String
    Public Property FederalTaxID() As String
        Get
            Return _FederalTaxID
        End Get
        Set(ByVal value As String)
            _FederalTaxID = value
        End Set
    End Property

    'DocCurrency
    ''' <summary>
    ''' Campo que se utiliza como moneda del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DocCurrency As String
    Public Property DocCurrency() As String
        Get
            Return _DocCurrency
        End Get
        Set(ByVal value As String)
            _DocCurrency = value
        End Set
    End Property

    'ControlAccount
    ''' <summary>
    ''' Campo que se utiliza cuenta contable del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _ControlAccount As String
    Public Property ControlAccount() As String
        Get
            Return _ControlAccount
        End Get
        Set(ByVal value As String)
            _ControlAccount = value
        End Set
    End Property

    'CashAccount
    ''' <summary>
    ''' Campo que se utiliza cuenta contable del fondo fijo para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CashAccount As String
    Public Property CashAccount() As String
        Get
            Return _CashAccount
        End Get
        Set(ByVal value As String)
            _CashAccount = value
        End Set
    End Property

    'Comments
    ''' <summary>
    ''' Campo que se utiliza para glosario para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _Comments As String
    Public Property Comments() As String
        Get
            Return _Comments
        End Get
        Set(ByVal value As String)
            _Comments = value
        End Set
    End Property

    'JournalMemo
    ''' <summary>
    ''' Campo que cumple las mismas funciones que comments.(Muestra la glosa)
    ''' </summary>
    ''' <remarks></remarks>
    Private _JournalMemo As String
    Public Property JournalMemo() As String
        Get
            Return _JournalMemo
        End Get
        Set(ByVal value As String)
            _JournalMemo = value
        End Set
    End Property


    'PaymentGroupCode
    ''' <summary>
    ''' Campo que se utiliza para forma de pago para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _PaymentGroupCode As Integer
    Public Property PaymentGroupCode() As Integer
        Get
            Return _PaymentGroupCode
        End Get
        Set(ByVal value As Integer)
            _PaymentGroupCode = value
        End Set
    End Property

    'DiscountPercent
    ''' <summary>
    ''' Campo que se utiliza para el porcentaje de descuento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DiscountPercent As Decimal
    Public Property DiscountPercent() As Decimal
        Get
            Return _DiscountPercent
        End Get
        Set(ByVal value As Decimal)
            _DiscountPercent = value
        End Set
    End Property

    'DocTotal
    ''' <summary>
    ''' Campo que se utiliza para el total del documento en soles para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DocTotal As Decimal
    Public Property DocTotal() As Decimal
        Get
            Return _DocTotal
        End Get
        Set(ByVal value As Decimal)
            _DocTotal = value
        End Set
    End Property

    'DocTotalFc
    ''' <summary>
    ''' Campo que se utiliza para el total del documento en dolares para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DocTotalFc As Decimal
    Public Property DocTotalFc() As Decimal
        Get
            Return _DocTotalFc
        End Get
        Set(ByVal value As Decimal)
            _DocTotalFc = value
        End Set
    End Property

    'U_SYP_MDTD
    ''' <summary>
    ''' Campo que se utiliza para el tipo de documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDTD As String
    Public Property U_SYP_MDTD() As String
        Get
            Return _U_SYP_MDTD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDTD = value
        End Set
    End Property

    'U_SYP_MDSD
    ''' <summary>
    ''' Campo que se utiliza para la serie del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDSD As String
    Public Property U_SYP_MDSD() As String
        Get
            Return _U_SYP_MDSD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDSD = value
        End Set
    End Property

    'U_SYP_MDCD
    ''' <summary>
    ''' Campo que se utiliza para el correlativo del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDCD As String
    Public Property U_SYP_MDCD() As String
        Get
            Return _U_SYP_MDCD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDCD = value
        End Set
    End Property

    'U_SYP_MDTO
    ''' <summary>
    ''' Campo que se utiliza para el tipo de documento de referencia para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDTO As String
    Public Property U_SYP_MDTO() As String
        Get
            Return _U_SYP_MDTO
        End Get
        Set(ByVal value As String)
            _U_SYP_MDTO = value
        End Set
    End Property

    'U_SYP_MDSO
    ''' <summary>
    ''' Campo que se utiliza para la serie del documento de referencia para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDSO As String
    Public Property U_SYP_MDSO() As String
        Get
            Return _U_SYP_MDSO
        End Get
        Set(ByVal value As String)
            _U_SYP_MDSO = value
        End Set
    End Property

    'U_SYP_MDCO
    ''' <summary>
    ''' Campo que se utiliza para el correlativo del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDCO As String
    Public Property U_SYP_MDCO() As String
        Get
            Return _U_SYP_MDCO
        End Get
        Set(ByVal value As String)
            _U_SYP_MDCO = value
        End Set
    End Property

    'U_SYP_STATUS
    ''' <summary>
    ''' Campo que se utiliza para el estado (V=Vigente,A=Anulado) del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_STATUS As String
    Public Property U_SYP_STATUS() As String
        Get
            Return _U_SYP_STATUS
        End Get
        Set(ByVal value As String)
            _U_SYP_STATUS = value
        End Set
    End Property

    'U_SYP_FECHAREF
    ''' <summary>
    ''' Campo que se utiliza para la fecha de referencia del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_FECHAREF As Date
    Public Property U_SYP_FECHAREF() As Date
        Get
            Return _U_SYP_FECHAREF
        End Get
        Set(ByVal value As Date)
            _U_SYP_FECHAREF = value
        End Set
    End Property

    'U_SYP_NUMOPER
    ''' <summary>
    ''' Campo que se utiliza para la clave primaria del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_NUMOPER As String
    Public Property U_SYP_NUMOPER() As String
        Get
            Return _U_SYP_NUMOPER
        End Get
        Set(ByVal value As String)
            _U_SYP_NUMOPER = value
        End Set
    End Property

    'U_SYP_TC
    ''' <summary>
    ''' Campo que se utiliza para el tipo de cambio del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_TC As String
    Public Property U_SYP_TC() As String
        Get
            Return _U_SYP_TC
        End Get
        Set(ByVal value As String)
            _U_SYP_TC = value
        End Set
    End Property

    'U_SYP_TCOMPRA
    ''' <summary>
    ''' Campo que se utiliza para el tipo de compra (ER: Estados a rendir, y CC: Fondo Fijo) del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_TCOMPRA As String
    Public Property U_SYP_TCOMPRA() As String
        Get
            Return _U_SYP_TCOMPRA
        End Get
        Set(ByVal value As String)
            _U_SYP_TCOMPRA = value
        End Set
    End Property

    'U_SYP_NFILE
    ''' <summary>
    ''' Campo que se utiliza para el N° de File del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_NFILE As String
    Public Property U_SYP_NFILE() As String
        Get
            Return _U_SYP_NFILE
        End Get
        Set(ByVal value As String)
            _U_SYP_NFILE = value
        End Set
    End Property

    'U_SYP_CODERCC
    ''' <summary>
    ''' Campo que se utiliza para el codigo de la caja chica para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_CODERCC As String
    Public Property U_SYP_CODERCC() As String
        Get
            Return _U_SYP_CODERCC
        End Get
        Set(ByVal value As String)
            _U_SYP_CODERCC = value
        End Set
    End Property

    'U_SYP_CTAERCC
    ''' <summary>
    ''' Campo que se utiliza para la cuenta contable de la caja chica para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_CTAERCC As String
    Public Property U_SYP_CTAERCC() As String
        Get
            Return _U_SYP_CTAERCC
        End Get
        Set(ByVal value As String)
            _U_SYP_CTAERCC = value
        End Set
    End Property

    'U_SYP_TIPOBOLETO
    ''' <summary>
    ''' Campo que se utiliza para el tipo de boleto de Peru Rail para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_TIPOBOLETO As String
    Public Property U_SYP_TIPOBOLETO() As String
        Get
            Return _U_SYP_TIPOBOLETO
        End Get
        Set(ByVal value As String)
            _U_SYP_TIPOBOLETO = value
        End Set
    End Property

    'U_SYP_TPO_OP
    ''' <summary>
    ''' Campo que se utiliza para el tipo de operacion DET para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_TPO_OP As String
    Public Property U_SYP_TPO_OP() As String
        Get
            Return _U_SYP_TPO_OP
        End Get
        Set(ByVal value As String)
            _U_SYP_TPO_OP = value
        End Set
    End Property

    ''' <summary>
    '''  Campo que se utiliza para la lista de detalles del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _Lines As List(Of clsPurchaseInvoices_LineBE)
    Public Property Lines() As List(Of clsPurchaseInvoices_LineBE)
        Get
            Return _Lines
        End Get
        Set(ByVal value As List(Of clsPurchaseInvoices_LineBE))
            _Lines = value
        End Set
    End Property

    ''' <summary>
    '''  Campo que se utiliza para la lista de detalles del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _Expenses As List(Of clsPurchaseInvoices_ExpensesBE)
    Public Property Expenses() As List(Of clsPurchaseInvoices_ExpensesBE)
        Get
            Return _Expenses
        End Get
        Set(ByVal value As List(Of clsPurchaseInvoices_ExpensesBE))
            _Expenses = value
        End Set
    End Property

    ''' <summary>
    '''  Campo que se utiliza para la lista de retenciones por honorarios para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WithholdingTaxData As List(Of clsPurchaseInvoices_WithholdingTaxDataBE)
    Public Property WithholdingTaxData() As List(Of clsPurchaseInvoices_WithholdingTaxDataBE)
        Get
            Return _WithholdingTaxData
        End Get
        Set(ByVal value As List(Of clsPurchaseInvoices_WithholdingTaxDataBE))
            _WithholdingTaxData = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPurchaseInvoices_LineBE

    'ItemCode		
    ''' <summary>
    ''' Campo que se utiliza para el codigo del producto del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _ItemCode As String
    Public Property ItemCode() As String
        Get
            Return _ItemCode
        End Get
        Set(ByVal value As String)
            _ItemCode = value
        End Set
    End Property

    'ItemDescription		
    ''' <summary>
    ''' Campo que se utiliza para la descripcion del producto del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _ItemDescription As String
    Public Property ItemDescription() As String
        Get
            Return _ItemDescription
        End Get
        Set(ByVal value As String)
            _ItemDescription = value
        End Set
    End Property

    'WarehouseCode		
    ''' <summary>
    ''' Campo que se utiliza para el codigo de almacen del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WarehouseCode As String
    Public Property WarehouseCode() As String
        Get
            Return _WarehouseCode
        End Get
        Set(ByVal value As String)
            _WarehouseCode = value
        End Set
    End Property

    'Quantity	
    ''' <summary>
    ''' Campo que se utiliza para la cantidad del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _Quantity As Decimal
    Public Property Quantity() As Decimal
        Get
            Return _Quantity
        End Get
        Set(ByVal value As Decimal)
            _Quantity = value
        End Set
    End Property

    'DiscountPercent		
    ''' <summary>
    ''' Campo que se utiliza para el porcentaje de descuento del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DiscountPercent As Decimal
    Public Property DiscountPercent() As Decimal
        Get
            Return _DiscountPercent
        End Get
        Set(ByVal value As Decimal)
            _DiscountPercent = value
        End Set
    End Property

    'AccountCode		
    ''' <summary>
    ''' Campo que se utiliza para la cuenta contable del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _AccountCode As String
    Public Property AccountCode() As String
        Get
            Return _AccountCode
        End Get
        Set(ByVal value As String)
            _AccountCode = value
        End Set
    End Property

    'UnitPrice		
    ''' <summary>
    ''' Campo que se utiliza para el precio unitario del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _UnitPrice As Decimal
    Public Property UnitPrice() As Decimal
        Get
            Return _UnitPrice
        End Get
        Set(ByVal value As Decimal)
            _UnitPrice = value
        End Set
    End Property

    'LineTotal
    ''' <summary>
    ''' Campo que se utiliza para el total sin igv del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _LineTotal As Decimal
    Public Property LineTotal() As Decimal
        Get
            Return _LineTotal
        End Get
        Set(ByVal value As Decimal)
            _LineTotal = value
        End Set
    End Property

    'TaxCode		
    ''' <summary>
    ''' Campo que se utiliza para el codigo de impuesto del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _TaxCode As String
    Public Property TaxCode() As String
        Get
            Return _TaxCode
        End Get
        Set(ByVal value As String)
            _TaxCode = value
        End Set
    End Property

    'TaxTotal		
    ''' <summary>
    ''' Campo que se utiliza para el total de impuesto del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _TaxTotal As Decimal
    Public Property TaxTotal() As Decimal
        Get
            Return _TaxTotal
        End Get
        Set(ByVal value As Decimal)
            _TaxTotal = value
        End Set
    End Property

    'WTLiable		
    ''' <summary>
    ''' Campo que se utiliza para determinar si se trata de un recibo por honorario del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WTLiable As Boolean
    Public Property WTLiable() As Boolean
        Get
            Return _WTLiable
        End Get
        Set(ByVal value As Boolean)
            _WTLiable = value
        End Set
    End Property

    'TaxOnly		
    ''' <summary>
    ''' Campo que se utiliza para determinar si se trata de sólo impuesto del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _TaxOnly As Boolean
    Public Property TaxOnly() As Boolean
        Get
            Return _TaxOnly
        End Get
        Set(ByVal value As Boolean)
            _TaxOnly = value
        End Set
    End Property


    'CostingCode
    ''' <summary>
    ''' Campo que se utiliza para la dimension 1 del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CostingCode As String
    Public Property CostingCode() As String
        Get
            Return _CostingCode
        End Get
        Set(ByVal value As String)
            _CostingCode = value
        End Set
    End Property

    'CostingCode2		
    ''' <summary>
    ''' Campo que se utiliza para la dimension 2 del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CostingCode2 As String
    Public Property CostingCode2() As String
        Get
            Return _CostingCode2
        End Get
        Set(ByVal value As String)
            _CostingCode2 = value
        End Set
    End Property

    'CostingCode3		
    ''' <summary>
    ''' Campo que se utiliza para la dimension 3 del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CostingCode3 As String
    Public Property CostingCode3() As String
        Get
            Return _CostingCode3
        End Get
        Set(ByVal value As String)
            _CostingCode3 = value
        End Set
    End Property

    'CostingCode4		
    ''' <summary>
    ''' Campo que se utiliza para la dimension 4 del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CostingCode4 As String
    Public Property CostingCode4() As String
        Get
            Return _CostingCode4
        End Get
        Set(ByVal value As String)
            _CostingCode4 = value
        End Set
    End Property

    'CostingCode5		
    ''' <summary>
    ''' Campo que se utiliza para la dimension 5 del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CostingCode5 As String
    Public Property CostingCode5() As String
        Get
            Return _CostingCode5
        End Get
        Set(ByVal value As String)
            _CostingCode5 = value
        End Set
    End Property

    'U_SYP_CONCEPTO		
    ''' <summary>
    ''' Campo que se utiliza para un concepto informativo del detalle para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_CONCEPTO As String
    Public Property U_SYP_CONCEPTO() As String
        Get
            Return _U_SYP_CONCEPTO
        End Get
        Set(ByVal value As String)
            _U_SYP_CONCEPTO = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPurchaseInvoices_ExpensesBE

    'ExpenseCode		integer
    ''' <summary>
    ''' Campo que se utiliza para el codigo del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _ExpenseCode As Integer
    Public Property ExpenseCode() As Integer
        Get
            Return _ExpenseCode
        End Get
        Set(ByVal value As Integer)
            _ExpenseCode = value
        End Set
    End Property


    'LineTotal		decimal number
    ''' <summary>
    ''' Campo que se utiliza para el total del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _LineTotal As Decimal
    Public Property LineTotal() As Decimal
        Get
            Return _LineTotal
        End Get
        Set(ByVal value As Decimal)
            _LineTotal = value
        End Set
    End Property


    'Remarks		
    ''' <summary>
    ''' Campo que se utiliza para una descripcion del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _Remarks As String
    Public Property Remarks() As String
        Get
            Return _Remarks
        End Get
        Set(ByVal value As String)
            _Remarks = value
        End Set
    End Property

    'DistributionMethod		BoAdEpnsDistribMethods
    ''' <summary>
    ''' Campo que se utiliza para el metodo de distribución del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DistributionMethod As Byte
    Public Property DistributionMethod() As Byte
        Get
            Return _DistributionMethod
        End Get
        Set(ByVal value As Byte)
            _DistributionMethod = value
        End Set
    End Property

    'TaxCode		string
    ''' <summary>
    ''' Campo que se utiliza para el codigo del indicador de impuesto del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _TaxCode As String
    Public Property TaxCode() As String
        Get
            Return _TaxCode
        End Get
        Set(ByVal value As String)
            _TaxCode = value
        End Set
    End Property

    'DistributionRule		string
    ''' <summary>
    ''' Campo que se utiliza para la dimension 1 del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DistributionRule As String
    Public Property DistributionRule() As String
        Get
            Return _DistributionRule
        End Get
        Set(ByVal value As String)
            _DistributionRule = value
        End Set
    End Property

    'DistributionRule2		string
    ''' <summary>
    ''' Campo que se utiliza para la dimension 2 del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DistributionRule2 As String
    Public Property DistributionRule2() As String
        Get
            Return _DistributionRule2
        End Get
        Set(ByVal value As String)
            _DistributionRule2 = value
        End Set
    End Property

    'DistributionRule3		string
    ''' <summary>
    ''' Campo que se utiliza para la dimension 2 del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DistributionRule3 As String
    Public Property DistributionRule3() As String
        Get
            Return _DistributionRule3
        End Get
        Set(ByVal value As String)
            _DistributionRule3 = value
        End Set
    End Property

    'DistributionRule4		string
    ''' <summary>
    ''' Campo que se utiliza para la dimension 4 del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DistributionRule4 As String
    Public Property DistributionRule4() As String
        Get
            Return _DistributionRule4
        End Get
        Set(ByVal value As String)
            _DistributionRule4 = value
        End Set
    End Property

    'DistributionRule5		string
    ''' <summary>
    ''' Campo que se utiliza para la dimension 5 del gasto adicional para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _DistributionRule5 As String
    Public Property DistributionRule5() As String
        Get
            Return _DistributionRule5
        End Get
        Set(ByVal value As String)
            _DistributionRule5 = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPurchaseInvoices_WithholdingTaxDataBE

    '    WTCode	
    ''' <summary>
    ''' Campo que se utiliza para el codigo de retencion por honorarios para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WTCode As String
    Public Property WTCode() As String
        Get
            Return _WTCode
        End Get
        Set(ByVal value As String)
            _WTCode = value
        End Set
    End Property

    'WTAmount		decimal number
    ''' <summary>
    ''' Campo que se utiliza para el monto de retencion por honorarios en soles para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WTAmount As String
    Public Property WTAmount() As String
        Get
            Return _WTAmount
        End Get
        Set(ByVal value As String)
            _WTAmount = value
        End Set
    End Property

    'WTAmountFC		decimal number
    ''' <summary>
    ''' Campo que se utiliza para el monto de retencion por honorarios en dolares para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _WTAmountFC As String
    Public Property WTAmountFC() As String
        Get
            Return _WTAmountFC
        End Get
        Set(ByVal value As String)
            _WTAmountFC = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsPurchaseInvoices_CancellationBE

    'U_SYP_MDTO
    ''' <summary>
    ''' Campo que se utiliza para el CardCode del Socio de Negocio para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardCode As String
    Public Property CardCode() As String
        Get
            Return _CardCode
        End Get
        Set(ByVal value As String)
            _CardCode = value
        End Set
    End Property

    'U_SYP_MDTO
    ''' <summary>
    ''' Campo que se utiliza para el tipo de documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDTD As String
    Public Property U_SYP_MDTD() As String
        Get
            Return _U_SYP_MDTD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDTD = value
        End Set
    End Property

    'U_SYP_MDSO
    ''' <summary>
    ''' Campo que se utiliza para la serie del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDSD As String
    Public Property U_SYP_MDSD() As String
        Get
            Return _U_SYP_MDSD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDSD = value
        End Set
    End Property

    'U_SYP_MDCO
    ''' <summary>
    ''' Campo que se utiliza para el correlativo del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_MDCD As String
    Public Property U_SYP_MDCD() As String
        Get
            Return _U_SYP_MDCD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDCD = value
        End Set
    End Property

    'U_SYP_STATUS
    ''' <summary>
    ''' Campo que se utiliza para el estado (V=Vigente,A=Anulado) del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_STATUS As String
    Public Property U_SYP_STATUS() As String
        Get
            Return _U_SYP_STATUS
        End Get
        Set(ByVal value As String)
            _U_SYP_STATUS = value
        End Set
    End Property


    'U_SYP_TCOMPRA
    ''' <summary>
    ''' Campo que se utiliza para el tipo de compra (ER: Estados a rendir, y CC: Fondo Fijo) del documento para SAP Bussiness One.
    ''' </summary>
    ''' <remarks></remarks>
    Private _U_SYP_TCOMPRA As String
    Public Property U_SYP_TCOMPRA() As String
        Get
            Return _U_SYP_TCOMPRA
        End Get
        Set(ByVal value As String)
            _U_SYP_TCOMPRA = value
        End Set
    End Property

End Class