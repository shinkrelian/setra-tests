﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsProfitCenterBE
    ''' <summary>
    ''' Este es el file desde SAP
    ''' </summary>
    ''' <remarks></remarks>
    Private _CenterCode As String
    Public Property CenterCode() As String
        Get
            Return _CenterCode
        End Get
        Set(ByVal value As String)
            _CenterCode = value
        End Set
    End Property

    ''' <summary>
    ''' Misma descripción del file SETRA
    ''' </summary>
    ''' <remarks></remarks>
    Private _CenterName As String
    Public Property CenterName() As String
        Get
            Return _CenterName
        End Get
        Set(ByVal value As String)
            _CenterName = value
        End Set
    End Property

    ''' <summary>
    ''' Dimensiones creadas en SAP - Por default '3' (Files)
    ''' </summary>
    ''' <remarks></remarks>
    Private _InWhichDimension As Byte
    Public Property InWhichDimension() As Byte
        Get
            Return _InWhichDimension
        End Get
        Set(ByVal value As Byte)
            _InWhichDimension = value
        End Set
    End Property

    ''' <summary>
    ''' Fecha inicio de vigencia en SAP
    ''' </summary>
    ''' <remarks></remarks>
    Private _Effectivefrom As Date
    Public Property Effectivefrom() As Date
        Get
            Return _Effectivefrom
        End Get
        Set(ByVal value As Date)
            _Effectivefrom = value
        End Set
    End Property

    ''' <summary>
    ''' Fecha fin de vigencia en SAP
    ''' </summary>
    ''' <remarks></remarks>
    Private _EffectiveTo As Date
    Public Property EffectiveTo() As Date
        Get
            Return _EffectiveTo
        End Get
        Set(ByVal value As Date)
            _EffectiveTo = value
        End Set
    End Property

    ''' <summary>
    ''' Se define cuando esta activo '1' o se anula '0'.
    ''' </summary>
    ''' <remarks></remarks>
    Private _Active As Byte
    Public Property Active() As Byte
        Get
            Return _Active
        End Get
        Set(ByVal value As Byte)
            _Active = value
        End Set
    End Property

    ''' <summary>
    ''' Código del socio de negocio
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardCode As String
    Public Property CardCode() As String
        Get
            Return _CardCode
        End Get
        Set(ByVal value As String)
            _CardCode = value
        End Set
    End Property

    ''' <summary>
    ''' Nombre del socio de negocio
    ''' </summary>
    ''' <remarks></remarks>
    Private _CardName As String
    Public Property CardName() As String
        Get
            Return _CardName
        End Get
        Set(ByVal value As String)
            _CardName = value
        End Set
    End Property

End Class
