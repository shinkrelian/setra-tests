﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsInvoicesBE
    Inherits clsPurchaseInvoicesBE

    Private _U_SYP_DOCEXPORT As String
    Public Property U_SYP_DOCEXPORT() As String
        Get
            Return _U_SYP_DOCEXPORT
        End Get
        Set(ByVal value As String)
            _U_SYP_DOCEXPORT = value
        End Set
    End Property


    Public Class clsLinesBE
        Private _ItemDescription As String
        Public Property ItemDescription() As String
            Get
                Return _ItemDescription
            End Get
            Set(ByVal value As String)
                _ItemDescription = value
            End Set
        End Property

        Private _LineTotal As Double
        Public Property LineTotal() As Double
            Get
                Return _LineTotal
            End Get
            Set(ByVal value As Double)
                _LineTotal = value
            End Set
        End Property

        Private _TaxCode As String
        Public Property TaxCode() As String
            Get
                Return _TaxCode
            End Get
            Set(ByVal value As String)
                _TaxCode = value
            End Set
        End Property

        Private _TaxTotal As Double
        Public Property TaxTotal() As Double
            Get
                Return _TaxTotal
            End Get
            Set(ByVal value As Double)
                _TaxTotal = value
            End Set
        End Property


        Private _CostingCode As String
        Public Property CostingCode() As String
            Get
                Return _CostingCode
            End Get
            Set(ByVal value As String)
                _CostingCode = value
            End Set
        End Property

        Private _CostingCode2 As String
        Public Property CostingCode2() As String
            Get
                Return _CostingCode2
            End Get
            Set(ByVal value As String)
                _CostingCode2 = value
            End Set
        End Property

        Private _CostingCode3 As String
        Public Property CostingCode3() As String
            Get
                Return _CostingCode3
            End Get
            Set(ByVal value As String)
                _CostingCode3 = value
            End Set
        End Property

        Private _AccountCode As String
        Public Property AccountCode() As String
            Get
                Return _AccountCode
            End Get
            Set(ByVal value As String)
                _AccountCode = value
            End Set
        End Property

    End Class

    Public Class clsDownPaymentsToDrawBE
        Private _U_SYP_MDCD As String
        Public Property U_SYP_MDCD() As String
            Get
                Return _U_SYP_MDCD
            End Get
            Set(ByVal value As String)
                _U_SYP_MDCD = value
            End Set
        End Property

        Private _U_SYP_MDTD As String
        Public Property U_SYP_MDTD() As String
            Get
                Return _U_SYP_MDTD
            End Get
            Set(ByVal value As String)
                _U_SYP_MDTD = value
            End Set
        End Property

        Private _U_SYP_MDSD As String
        Public Property U_SYP_MDSD() As String
            Get
                Return _U_SYP_MDSD
            End Get
            Set(ByVal value As String)
                _U_SYP_MDSD = value
            End Set
        End Property

        Private _GrossAmountToDraw As Double
        Public Property GrossAmountToDraw() As Double
            Get
                Return _GrossAmountToDraw
            End Get
            Set(ByVal value As Double)
                _GrossAmountToDraw = value
            End Set
        End Property

        Private _GrossAmountToDrawFC As Double
        Public Property GrossAmountToDrawFC() As Double
            Get
                Return _GrossAmountToDrawFC
            End Get
            Set(ByVal value As Double)
                _GrossAmountToDrawFC = value
            End Set
        End Property

        Private _AmountToDraw As Double
        Public Property AmountToDraw() As Double
            Get
                Return _AmountToDraw
            End Get
            Set(ByVal value As Double)
                _AmountToDraw = value
            End Set
        End Property

        Private _AmountToDrawFC As Double
        Public Property AmountToDrawFC() As Double
            Get
                Return _AmountToDrawFC
            End Get
            Set(ByVal value As Double)
                _AmountToDrawFC = value
            End Set
        End Property

    End Class

    Private _Lines As List(Of clsLinesBE)
    Public Overloads Property Lines() As List(Of clsLinesBE)
        Get
            Return _Lines
        End Get
        Set(ByVal value As List(Of clsLinesBE))
            _Lines = value
        End Set
    End Property

    Private _DownPaymentsToDraw As List(Of clsDownPaymentsToDrawBE)
    Public Property DownPaymentsToDraw() As List(Of clsDownPaymentsToDrawBE)
        Get
            Return _DownPaymentsToDraw
        End Get
        Set(ByVal value As List(Of clsDownPaymentsToDrawBE))
            _DownPaymentsToDraw = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDownPaymentsBE
    Inherits clsInvoicesBE

End Class
