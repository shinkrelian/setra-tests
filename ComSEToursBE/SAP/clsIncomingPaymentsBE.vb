﻿Imports System.Runtime.InteropServices
Imports System.EnterpriseServices
Imports Newtonsoft.Json
Imports Newtonsoft.Json.Linq

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsIncomingPaymentsBE
    ''' <summary>
    ''' 
    ''' </summary>
    ''' <remarks></remarks>
    Private _DocDate As Date
    Public Property DocDate() As Date
        Get
            Return _DocDate
        End Get
        Set(ByVal value As Date)
            _DocDate = value
        End Set
    End Property

    Private _TaxDate As Date
    Public Property TaxDate() As Date
        Get
            Return _TaxDate
        End Get
        Set(ByVal value As Date)
            _TaxDate = value
        End Set
    End Property

    Private _DueDate As Date
    Public Property DueDate() As Date
        Get
            Return _DueDate
        End Get
        Set(ByVal value As Date)
            _DueDate = value
        End Set
    End Property

    Private _CardCode As String
    Public Property CardCode() As String
        Get
            Return _CardCode
        End Get
        Set(ByVal value As String)
            _CardCode = value
        End Set
    End Property

    Private _DocType As Byte
    Public Property DocType() As Byte
        Get
            Return _DocType
        End Get
        Set(ByVal value As Byte)
            _DocType = value
        End Set
    End Property

    Private _TransferAccount As String
    Public Property TransferAccount() As String
        Get
            Return _TransferAccount
        End Get
        Set(ByVal value As String)
            _TransferAccount = value
        End Set
    End Property

    Private _TransferDate As Date
    Public Property TransferDate() As Date
        Get
            Return _TransferDate
        End Get
        Set(ByVal value As Date)
            _TransferDate = value
        End Set
    End Property

    Private _TransferReference As String
    Public Property TransferReference() As String
        Get
            Return _TransferReference
        End Get
        Set(ByVal value As String)
            _TransferReference = value
        End Set
    End Property

    Private _TransferSum As Double
    Public Property TransferSum() As Double
        Get
            Return _TransferSum
        End Get
        Set(ByVal value As Double)
            _TransferSum = value
        End Set
    End Property

    Private _TransferSumFc As Double
    Public Property TransferSumFc() As Double
        Get
            Return _TransferSumFc
        End Get
        Set(ByVal value As Double)
            _TransferSumFc = value
        End Set
    End Property

    Private _BridgeAccount As String
    Public Property BridgeAccount() As String
        Get
            Return _BridgeAccount
        End Get
        Set(ByVal value As String)
            _BridgeAccount = value
        End Set
    End Property

    Private _CounterReference As String
    Public Property CounterReference() As String
        Get
            Return _CounterReference
        End Get
        Set(ByVal value As String)
            _CounterReference = value
        End Set
    End Property

    Private _Reference2 As String
    Public Property Reference2() As String
        Get
            Return _Reference2
        End Get
        Set(ByVal value As String)
            _Reference2 = value
        End Set
    End Property

    Private _DocCurrency As String
    Public Property DocCurrency() As String
        Get
            Return _DocCurrency
        End Get
        Set(ByVal value As String)
            _DocCurrency = value
        End Set
    End Property

    Private _DocRate As Double
    Public Property DocRate() As Double
        Get
            Return _DocRate
        End Get
        Set(ByVal value As Double)
            _DocRate = value
        End Set
    End Property

    Private _Remarks As String
    Public Property Remarks() As String
        Get
            Return _Remarks
        End Get
        Set(ByVal value As String)
            _Remarks = value
        End Set
    End Property

    Private _JournalRemarks As String
    Public Property JournalRemarks() As String
        Get
            Return _JournalRemarks
        End Get
        Set(ByVal value As String)
            _JournalRemarks = value
        End Set
    End Property

    Private _U_SYP_MPPG As String
    Public Property U_SYP_MPPG() As String
        Get
            Return _U_SYP_MPPG
        End Get
        Set(ByVal value As String)
            _U_SYP_MPPG = value
        End Set
    End Property

    Private _U_SYP_NUMOPER As String
    Public Property U_SYP_NUMOPER() As String
        Get
            Return _U_SYP_NUMOPER
        End Get
        Set(ByVal value As String)
            _U_SYP_NUMOPER = value
        End Set
    End Property

    Private _U_SYP_TC As String
    Public Property U_SYP_TC() As String
        Get
            Return _U_SYP_TC
        End Get
        Set(ByVal value As String)
            _U_SYP_TC = value
        End Set
    End Property

    Private _U_SYP_NFILE As String
    Public Property U_SYP_NFILE() As String
        Get
            Return _U_SYP_NFILE
        End Get
        Set(ByVal value As String)
            _U_SYP_NFILE = value
        End Set
    End Property

    Private _U_SYP_TIPOCOB As Byte
    Public Property U_SYP_TIPOCOB() As Byte
        Get
            Return _U_SYP_TIPOCOB
        End Get
        Set(ByVal value As Byte)
            _U_SYP_TIPOCOB = value
        End Set
    End Property

    Private _Id As String
    Public Property Id() As String
        Get
            Return _Id
        End Get
        Set(ByVal value As String)
            _Id = value
        End Set
    End Property

    Private _ControlAccount As String
    Public Property ControlAccount() As String
        Get
            Return _ControlAccount
        End Get
        Set(ByVal value As String)
            _ControlAccount = value
        End Set
    End Property

    Private _BankChargeAmount As Double
    Public Property BankChargeAmount() As Double
        Get
            Return _BankChargeAmount
        End Get
        Set(ByVal value As Double)
            _BankChargeAmount = value
        End Set
    End Property


    Private _Invoices As List(Of clsInvoicesInIncomingPaymentsBE)
    Public Property Invoices() As List(Of clsInvoicesInIncomingPaymentsBE)
        Get
            Return _Invoices
        End Get
        Set(ByVal value As List(Of clsInvoicesInIncomingPaymentsBE))
            _Invoices = value
        End Set
    End Property

    ' ''Private _PaymentAccounts As List(Of clsPaymentAccountsInIncomingPaymentsBE)
    ' ''Public Property PaymentAccounts() As List(Of clsPaymentAccountsInIncomingPaymentsBE)
    ' ''    Get
    ' ''        Return _PaymentAccounts
    ' ''    End Get
    ' ''    Set(ByVal value As List(Of clsPaymentAccountsInIncomingPaymentsBE))
    ' ''        _PaymentAccounts = value
    ' ''    End Set
    ' ''End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsInvoicesInIncomingPaymentsBE
    Private _InvoiceType As Int16
    Public Property InvoiceType() As Int16
        Get
            Return _InvoiceType
        End Get
        Set(ByVal value As Int16)
            _InvoiceType = value
        End Set
    End Property

    Private _U_SYP_MDTD As String
    Public Property U_SYP_MDTD() As String
        Get
            Return _U_SYP_MDTD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDTD = value
        End Set
    End Property

    Private _U_SYP_MDSD As String
    Public Property U_SYP_MDSD() As String
        Get
            Return _U_SYP_MDSD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDSD = value
        End Set
    End Property

    Private _U_SYP_MDCD As String
    Public Property U_SYP_MDCD() As String
        Get
            Return _U_SYP_MDCD
        End Get
        Set(ByVal value As String)
            _U_SYP_MDCD = value
        End Set
    End Property

    Private _DocCur As String
    Public Property DocCur() As String
        Get
            Return _DocCur
        End Get
        Set(ByVal value As String)
            _DocCur = value
        End Set
    End Property

    Private _SumApplied As Double
    Public Property SumApplied() As Double
        Get
            Return _SumApplied
        End Get
        Set(ByVal value As Double)
            _SumApplied = value
        End Set
    End Property

    Private _AppliedFC As Double
    Public Property AppliedFC() As Double
        Get
            Return _AppliedFC
        End Get
        Set(ByVal value As Double)
            _AppliedFC = value
        End Set
    End Property

End Class

'' ''Clase no necesaria en la API (Ingreso por Cuenta Contable)
' ''<ComVisible(True)> _
' ''<Transaction(TransactionOption.NotSupported)> _
' ''Public Class clsPaymentAccountsInIncomingPaymentsBE

' ''    Private _AccountCode As String
' ''    Public Property AccountCode() As String
' ''        Get
' ''            Return _AccountCode
' ''        End Get
' ''        Set(ByVal value As String)
' ''            _AccountCode = value
' ''        End Set
' ''    End Property

' ''    Private _AccountName As String
' ''    Public Property AccountName() As String
' ''        Get
' ''            Return _AccountName
' ''        End Get
' ''        Set(ByVal value As String)
' ''            _AccountName = value
' ''        End Set
' ''    End Property

' ''    Private _Decription As String
' ''    Public Property Decription() As String
' ''        Get
' ''            Return _Decription
' ''        End Get
' ''        Set(ByVal value As String)
' ''            _Decription = value
' ''        End Set
' ''    End Property

' ''    Private _SumPaid As Double
' ''    Public Property SumPaid() As Double
' ''        Get
' ''            Return _SumPaid
' ''        End Get
' ''        Set(ByVal value As Double)
' ''            _SumPaid = value
' ''        End Set
' ''    End Property

' ''End Class