﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public MustInherit Class clsCounterBaseBE

    Private _CoTicket As String
    Public Property CoTicket() As String
        Get
            Return _CoTicket
        End Get
        Set(ByVal value As String)
            _CoTicket = Replace(value, "-", "")
        End Set
    End Property

    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property

    Private _Accion As Char
    Public Property Accion() As Char
        Get
            Return _Accion
        End Get
        Set(ByVal value As Char)
            _Accion = value
        End Set
    End Property

End Class
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketBE
    Inherits clsCounterBaseBE

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _IDCliente As String
    Public Property IDCliente() As String
        Get
            Return _IDCliente
        End Get
        Set(ByVal value As String)
            _IDCliente = value
        End Set
    End Property

    Private _FlArea As Char
    Public Property FlArea() As Char
        Get
            Return _FlArea
        End Get
        Set(ByVal value As Char)
            _FlArea = value
        End Set
    End Property

    Private _SsTCambioCont As Single
    Public Property SsTCambioCont() As Single
        Get
            Return _SsTCambioCont
        End Get
        Set(ByVal value As Single)
            _SsTCambioCont = value
        End Set
    End Property

    Private _SsTCambioVenta As Single
    Public Property SsTCambioVenta() As Single
        Get
            Return _SsTCambioVenta
        End Get
        Set(ByVal value As Single)
            _SsTCambioVenta = value
        End Set
    End Property

    Private _FeEmision As Date
    Public Property FeEmision() As Date
        Get
            Return _FeEmision
        End Get
        Set(ByVal value As Date)
            _FeEmision = value
        End Set
    End Property

    Private _FeViaje As Date
    Public Property FeViaje() As Date
        Get
            Return _FeViaje
        End Get
        Set(ByVal value As Date)
            _FeViaje = value
        End Set
    End Property

    Private _FeRetorno As Date
    Public Property FeRetorno() As Date
        Get
            Return _FeRetorno
        End Get
        Set(ByVal value As Date)
            _FeRetorno = value
        End Set
    End Property

    Private _IDPax As Integer
    Public Property IDPax() As Integer
        Get
            Return _IDPax
        End Get
        Set(ByVal value As Integer)
            _IDPax = value
        End Set
    End Property

    Private _TxPax As String
    Public Property TxPax() As String
        Get
            Return _TxPax
        End Get
        Set(ByVal value As String)
            _TxPax = value
        End Set
    End Property

    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property

    Private _IDUbigeoOri As String
    Public Property IDUbigeoOri() As String
        Get
            Return _IDUbigeoOri
        End Get
        Set(ByVal value As String)
            _IDUbigeoOri = value
        End Set
    End Property

    Private _IDUbigeoDes As String
    Public Property IDUbigeoDes() As String
        Get
            Return _IDUbigeoDes
        End Get
        Set(ByVal value As String)
            _IDUbigeoDes = value
        End Set
    End Property

    Private _TxRuta As String
    Public Property TxRuta() As String
        Get
            Return _TxRuta
        End Get
        Set(ByVal value As String)
            _TxRuta = value
        End Set
    End Property

    Private _CoLugarVenta As String
    Public Property CoLugarVenta() As String
        Get
            Return _CoLugarVenta
        End Get
        Set(ByVal value As String)
            _CoLugarVenta = value
        End Set
    End Property

    Private _CoCounter As String
    Public Property CoCounter() As String
        Get
            Return _CoCounter
        End Get
        Set(ByVal value As String)
            _CoCounter = value
        End Set
    End Property

    Private _SsTarifa As Double
    Public Property SsTarifa() As Double
        Get
            Return _SsTarifa
        End Get
        Set(ByVal value As Double)
            _SsTarifa = value
        End Set
    End Property

    Private _SsPTA As Double
    Public Property SsPTA() As Double
        Get
            Return _SsPTA
        End Get
        Set(ByVal value As Double)
            _SsPTA = value
        End Set
    End Property

    Private _SsPenalidad As Double
    Public Property SsPenalidad() As Double
        Get
            Return _SsPenalidad
        End Get
        Set(ByVal value As Double)
            _SsPenalidad = value
        End Set
    End Property

    Private _SsOtros As Double
    Public Property SsOtros() As Double
        Get
            Return _SsOtros
        End Get
        Set(ByVal value As Double)
            _SsOtros = value
        End Set
    End Property

    Private _SsIGV As Double
    Public Property SsIGV() As Double
        Get
            Return _SsIGV
        End Get
        Set(ByVal value As Double)
            _SsIGV = value
        End Set
    End Property

    Private _SsImpExt As Double
    Public Property SsImpExt() As Double
        Get
            Return _SsImpExt
        End Get
        Set(ByVal value As Double)
            _SsImpExt = value
        End Set
    End Property

    Private _PoDscto As Single
    Public Property PoDscto() As Single
        Get
            Return _PoDscto
        End Get
        Set(ByVal value As Single)
            _PoDscto = value
        End Set
    End Property

    Private _SsDscto As Double
    Public Property SsDscto() As Double
        Get
            Return _SsDscto
        End Get
        Set(ByVal value As Double)
            _SsDscto = value
        End Set
    End Property

    Private _SsTotal As Double
    Public Property SsTotal() As Double
        Get
            Return _SsTotal
        End Get
        Set(ByVal value As Double)
            _SsTotal = value
        End Set
    End Property

    Private _CoTarjCred As String
    Public Property CoTarjCred() As String
        Get
            Return _CoTarjCred
        End Get
        Set(ByVal value As String)
            _CoTarjCred = value
        End Set
    End Property

    Private _CoNroTarjCred As String
    Public Property CoNroTarjCred() As String
        Get
            Return _CoNroTarjCred
        End Get
        Set(ByVal value As String)
            _CoNroTarjCred = value
        End Set
    End Property

    Private _CoNroAprobac As String
    Public Property CoNroAprobac() As String
        Get
            Return _CoNroAprobac
        End Get
        Set(ByVal value As String)
            _CoNroAprobac = value
        End Set
    End Property

    Private _SsTotTarjeta As Double
    Public Property SsTotTarjeta() As Double
        Get
            Return _SsTotTarjeta
        End Get
        Set(ByVal value As Double)
            _SsTotTarjeta = value
        End Set
    End Property

    Private _SsTotEfectivo As Double
    Public Property SsTotEfectivo() As Double
        Get
            Return _SsTotEfectivo
        End Get
        Set(ByVal value As Double)
            _SsTotEfectivo = value
        End Set
    End Property

    Private _PoComision As Single
    Public Property PoComision() As Single
        Get
            Return _PoComision
        End Get
        Set(ByVal value As Single)
            _PoComision = value
        End Set
    End Property

    Private _SsComision As Double
    Public Property SsComision() As Double
        Get
            Return _SsComision
        End Get
        Set(ByVal value As Double)
            _SsComision = value
        End Set
    End Property

    Private _SsIGVComision As Double
    Public Property SsIGVComision() As Double
        Get
            Return _SsIGVComision
        End Get
        Set(ByVal value As Double)
            _SsIGVComision = value
        End Set
    End Property

    Private _PoTarifa As Single
    Public Property PoTarifa() As Single
        Get
            Return _PoTarifa
        End Get
        Set(ByVal value As Single)
            _PoTarifa = value
        End Set
    End Property

    Private _PoOver As Single
    Public Property PoOver() As Single
        Get
            Return _PoOver
        End Get
        Set(ByVal value As Single)
            _PoOver = value
        End Set
    End Property

    Private _SsOver As Double
    Public Property SsOver() As Double
        Get
            Return _SsOver
        End Get
        Set(ByVal value As Double)
            _SsOver = value
        End Set
    End Property

    Private _SsIGVOver As Double
    Public Property SsIGVOver() As Double
        Get
            Return _SsIGVOver
        End Get
        Set(ByVal value As Double)
            _SsIGVOver = value
        End Set
    End Property

    Private _FlMigrado As Boolean
    Public Property FlMigrado() As Boolean
        Get
            Return _FlMigrado
        End Get
        Set(ByVal value As Boolean)
            _FlMigrado = value
        End Set
    End Property

    Private _CoProvOrigen As String
    Public Property CoProvOrigen() As String
        Get
            Return _CoProvOrigen
        End Get
        Set(ByVal value As String)
            _CoProvOrigen = value
        End Set
    End Property

    Private _TxReferencia As String
    Public Property TxReferencia() As String
        Get
            Return _TxReferencia
        End Get
        Set(ByVal value As String)
            _TxReferencia = value
        End Set
    End Property

    Private _CoTipo As String
    Public Property CoTipo() As String
        Get
            Return _CoTipo
        End Get
        Set(ByVal value As String)
            _CoTipo = value
        End Set
    End Property

    Private _CoTipoCambio As String
    Public Property CoTipoCambio() As String
        Get
            Return _CoTipoCambio
        End Get
        Set(ByVal value As String)
            _CoTipoCambio = value
        End Set
    End Property

    Private _ListaTickets As List(Of clsTicketBE)
    Public Property ListaTickets() As List(Of clsTicketBE)
        Get
            Return _ListaTickets
        End Get
        Set(ByVal value As List(Of clsTicketBE))
            _ListaTickets = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketAMADEUSBE
    Inherits clsCounterBaseBE

    Private _NoArchivo As String
    Public Property NoArchivo() As String
        Get
            Return _NoArchivo
        End Get
        Set(ByVal value As String)
            _NoArchivo = value
        End Set
    End Property

    Private _CoAir As String
    Public Property CoAir() As String
        Get
            Return _CoAir
        End Get
        Set(ByVal value As String)
            _CoAir = value
        End Set
    End Property

    Private _CoPNR_Amadeus As String
    Public Property CoPNR_Amadeus() As String
        Get
            Return _CoPNR_Amadeus
        End Get
        Set(ByVal value As String)
            _CoPNR_Amadeus = value
        End Set
    End Property

    Private _CoProvOrigen As String
    Public Property CoProvOrigen() As String
        Get
            Return _CoProvOrigen
        End Get
        Set(ByVal value As String)
            _CoProvOrigen = value
        End Set
    End Property

    Private _CoUsrCrea As String
    Public Property CoUsrCrea() As String
        Get
            Return _CoUsrCrea
        End Get
        Set(ByVal value As String)
            _CoUsrCrea = value
        End Set
    End Property

    Private _CoUsrDuenio As String
    Public Property CoUsrDuenio() As String
        Get
            Return _CoUsrDuenio
        End Get
        Set(ByVal value As String)
            _CoUsrDuenio = value
        End Set
    End Property

    Private _CoUsrEmite As String
    Public Property CoUsrEmite() As String
        Get
            Return _CoUsrEmite
        End Get
        Set(ByVal value As String)
            _CoUsrEmite = value
        End Set
    End Property

    Private _CoIATA As String
    Public Property CoIATA() As String
        Get
            Return _CoIATA
        End Get
        Set(ByVal value As String)
            _CoIATA = value
        End Set
    End Property

    Private _CoLineaAereaRecord As String
    Public Property CoLineaAereaRecord() As String
        Get
            Return _CoLineaAereaRecord
        End Get
        Set(ByVal value As String)
            _CoLineaAereaRecord = value
        End Set
    End Property

    Private _NoLineaAerea As String
    Public Property NoLineaAerea() As String
        Get
            Return _NoLineaAerea
        End Get
        Set(ByVal value As String)
            _NoLineaAerea = value
        End Set
    End Property

    Private _CoLineaAereaEmisora As String
    Public Property CoLineaAereaEmisora() As String
        Get
            Return _CoLineaAereaEmisora
        End Get
        Set(ByVal value As String)
            _CoLineaAereaEmisora = value
        End Set
    End Property

    Private _CoTransacEmision As String
    Public Property CoTransacEmision() As String
        Get
            Return _CoTransacEmision
        End Get
        Set(ByVal value As String)
            _CoTransacEmision = value
        End Set
    End Property

    Private _CoLineaAereaServicio As String
    Public Property CoLineaAereaServicio() As String
        Get
            Return _CoLineaAereaServicio
        End Get
        Set(ByVal value As String)
            _CoLineaAereaServicio = value
        End Set
    End Property

    Private _CoLineaAereaSigla As String
    Public Property CoLineaAereaSigla() As String
        Get
            Return _CoLineaAereaSigla
        End Get
        Set(ByVal value As String)
            _CoLineaAereaSigla = value
        End Set
    End Property

    Private _TxFechaCreacion As Date
    Public Property TxFechaCreacion() As String
        Get
            Return _TxFechaCreacion
        End Get
        Set(ByVal value As String)
            If IsDate(value) Then
                _TxFechaCreacion = value
            Else
                '  _TxFechaCreacion = datFechaDMY(value)
            End If

        End Set
    End Property

    Private _TxFechaModificacion As Date
    Public Property TxFechaModificacion() As String
        Get
            Return _TxFechaModificacion
        End Get
        Set(ByVal value As String)
            If IsDate(value) Then
                _TxFechaModificacion = value
            Else
                '  _TxFechaModificacion = datFechaDMY(value)
            End If

        End Set
    End Property

    Private _TxFechaEmision As Date
    Public Property TxFechaEmision() As String
        Get
            Return _TxFechaEmision
        End Get
        Set(ByVal value As String)
            If IsDate(value) Then
                _TxFechaEmision = value
            Else
                _TxFechaEmision = value 'datFechaDMY(value)
            End If

        End Set
    End Property

    Private _TxIndicaVenta As String
    Public Property TxIndicaVenta() As String
        Get
            Return _TxIndicaVenta
        End Get
        Set(ByVal value As String)
            _TxIndicaVenta = value
        End Set
    End Property

    Private _CoPtoVentayEmision As String
    Public Property CoPtoVentayEmision() As String
        Get
            Return _CoPtoVentayEmision
        End Get
        Set(ByVal value As String)
            _CoPtoVentayEmision = value
        End Set
    End Property

    Private _TxTarifaAerea As String
    Public Property TxTarifaAerea() As String
        Get
            Return _TxTarifaAerea
        End Get
        Set(ByVal value As String)
            _TxTarifaAerea = Replace(If(value = "", "0", value), ",", "")
        End Set
    End Property

    Private _TxTotal As String
    Public Property TxTotal() As String
        Get
            Return _TxTotal
        End Get
        Set(ByVal value As String)
            _TxTotal = Replace(If(value = "", "0", value), ",", "")
        End Set
    End Property

    Private _NoPax As String
    Public Property NoPax() As String
        Get
            Return _NoPax
        End Get
        Set(ByVal value As String)
            _NoPax = value
        End Set
    End Property

    Private _TxRestricciones As String
    Public Property TxRestricciones() As String
        Get
            Return _TxRestricciones
        End Get
        Set(ByVal value As String)
            _TxRestricciones = value
        End Set
    End Property

    Private _CoFormaPago As String
    Public Property CoFormaPago() As String
        Get
            Return _CoFormaPago
        End Get
        Set(ByVal value As String)
            _CoFormaPago = value
        End Set
    End Property

    Private _CoDocumIdent As String
    Public Property CoDocumIdent() As String
        Get
            Return _CoDocumIdent
        End Get
        Set(ByVal value As String)
            _CoDocumIdent = value
        End Set
    End Property

    Private _NuDocumIdent As String
    Public Property NuDocumIdent() As String
        Get
            Return _NuDocumIdent
        End Get
        Set(ByVal value As String)
            _NuDocumIdent = value
        End Set
    End Property

    Private _TxComision As String
    Public Property TxComision() As String
        Get
            Return _TxComision
        End Get
        Set(ByVal value As String)
            _TxComision = Replace(If(value = "", "0", value), ",", "")
        End Set
    End Property

    Private _TxTasaDY_AE As String
    Public Property TxTasaDY_AE() As String
        Get
            Return _TxTasaDY_AE
        End Get
        Set(ByVal value As String)
            _TxTasaDY_AE = If(value = "", "0", value)
        End Set
    End Property

    Private _TxTasaHW_DE As String
    Public Property TxTasaHW_DE() As String
        Get
            Return _TxTasaHW_DE
        End Get
        Set(ByVal value As String)
            _TxTasaHW_DE = If(value = "", "0", value)
        End Set
    End Property

    Private _TxTasaXC_DP As String
    Public Property TxTasaXC_DP() As String
        Get
            Return _TxTasaXC_DP
        End Get
        Set(ByVal value As String)
            _TxTasaXC_DP = If(value = "", "0", value)
        End Set
    End Property

    Private _TxTasaXB_TI As String
    Public Property TxTasaXB_TI() As String
        Get
            Return _TxTasaXB_TI
        End Get
        Set(ByVal value As String)
            _TxTasaXB_TI = If(value = "", "0", value)
        End Set
    End Property

    Private _TxTasaQQ_TO As String
    Public Property TxTasaQQ_TO() As String
        Get
            Return _TxTasaQQ_TO
        End Get
        Set(ByVal value As String)
            _TxTasaQQ_TO = If(value = "", "0", value)
        End Set
    End Property

    Private _TxTasaAH_SE As String
    Public Property TxTasaAH_SE() As String
        Get
            Return _TxTasaAH_SE
        End Get
        Set(ByVal value As String)
            _TxTasaAH_SE = If(value = "", "0", value)
        End Set
    End Property

    Private _IDCab As Integer
    Public Property IDCab() As Integer
        Get
            Return _IDCab
        End Get
        Set(ByVal value As Integer)
            _IDCab = value
        End Set
    End Property

    Private _IDCliente As String
    Public Property IDCliente() As String
        Get
            Return _IDCliente
        End Get
        Set(ByVal value As String)
            _IDCliente = value
        End Set
    End Property

    Private _IngresoManual As Boolean
    Public Property IngresoManual() As Boolean
        Get
            Return _IngresoManual
        End Get
        Set(ByVal value As Boolean)
            _IngresoManual = value
        End Set
    End Property

    Private _CoTicketOrig As String
    Public Property CoTicketOrig() As String
        Get
            Return _CoTicketOrig
        End Get
        Set(ByVal value As String)
            If value <> "" Then
                _CoTicketOrig = "K" & Replace(value, "-", "")
            Else
                _CoTicketOrig = ""
            End If

        End Set
    End Property

    Private _CoEstado As String
    Public Property CoEstado() As String
        Get
            Return _CoEstado
        End Get
        Set(ByVal value As String)
            _CoEstado = value
        End Set
    End Property

    Private _CoEjeCouAmd As String
    Public Property CoEjeCouAmd() As String
        Get
            Return _CoEjeCouAmd
        End Get
        Set(ByVal value As String)
            _CoEjeCouAmd = value
        End Set
    End Property

    Private _CoEjeCou As String
    Public Property CoEjeCou() As String
        Get
            Return _CoEjeCou
        End Get
        Set(ByVal value As String)
            _CoEjeCou = value
        End Set
    End Property

    Private _CoEjeRec As String
    Public Property CoEjeRec() As String
        Get
            Return _CoEjeRec
        End Get
        Set(ByVal value As String)
            _CoEjeRec = value
        End Set
    End Property

    Private _TxObsCou As String
    Public Property TxObsCou() As String
        Get
            Return _TxObsCou
        End Get
        Set(ByVal value As String)
            _TxObsCou = value
        End Set
    End Property

    Private _TxTituloPax As String
    Public Property TxTituloPax() As String
        Get
            Return _TxTituloPax
        End Get
        Set(ByVal value As String)
            _TxTituloPax = value
        End Set
    End Property

    Private _CoNacionalidad As String
    Public Property CoNacionalidad() As String
        Get
            Return _CoNacionalidad
        End Get
        Set(ByVal value As String)
            _CoNacionalidad = value
        End Set
    End Property

    Private _CoPNR_Aerolinea As String
    Public Property CoPNR_Aerolinea() As String
        Get
            Return _CoPNR_Aerolinea
        End Get
        Set(ByVal value As String)
            _CoPNR_Aerolinea = value
        End Set
    End Property

    Private _IDPax As Integer
    Public Property IDPax() As Integer
        Get
            Return _IDPax
        End Get
        Set(ByVal value As Integer)
            _IDPax = value
        End Set
    End Property

    Private _ListaItinerarioTicket As List(Of clsTicketAMADEUS_ITINERARIOBE)
    Public Property ListaItinerarioTicket() As List(Of clsTicketAMADEUS_ITINERARIOBE)
        Get
            Return _ListaItinerarioTicket
        End Get
        Set(ByVal value As List(Of clsTicketAMADEUS_ITINERARIOBE))
            _ListaItinerarioTicket = value
        End Set
    End Property

    Private _ListaTicketAmadeus_Tax As List(Of clsTicketAmadeus_TaxBE)
    Public Property ListaTicketAmadeus_Tax() As List(Of clsTicketAmadeus_TaxBE)
        Get
            Return _ListaTicketAmadeus_Tax
        End Get
        Set(ByVal value As List(Of clsTicketAmadeus_TaxBE))
            _ListaTicketAmadeus_Tax = value
        End Set
    End Property

    Private _ListaDocumentoBSP As List(Of clsDocumentoBSPBE)
    Public Property ListaDocumentoBSP() As List(Of clsDocumentoBSPBE)
        Get
            Return _ListaDocumentoBSP
        End Get
        Set(ByVal value As List(Of clsDocumentoBSPBE))
            _ListaDocumentoBSP = value
        End Set
    End Property

    Private _ListaTicketTarifas As List(Of clsTicketAMADEUS_TARIFABE)
    Public Property ListaTicketTarifas() As List(Of clsTicketAMADEUS_TARIFABE)
        Get
            Return _ListaTicketTarifas
        End Get
        Set(ByVal value As List(Of clsTicketAMADEUS_TARIFABE))
            _ListaTicketTarifas = value
        End Set
    End Property

    'jorge
    Private _NuNtaVta As String
    Public Property NuNtaVta() As String
        Get
            Return _NuNtaVta
        End Get
        Set(ByVal value As String)
            _NuNtaVta = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketAmadeus_TaxBE
    Inherits clsCounterBaseBE

    Private _NuTax As Byte
    Public Property NuTax() As Byte
        Get
            Return _NuTax
        End Get
        Set(ByVal value As Byte)
            _NuTax = value
        End Set
    End Property

    Private _CoTax As String
    Public Property CoTax() As String
        Get
            Return _CoTax
        End Get
        Set(ByVal value As String)
            _CoTax = value
        End Set
    End Property

    Private _SsMonto As String
    Public Property SsMonto() As String
        Get
            Return _SsMonto
        End Get
        Set(ByVal value As String)
            _SsMonto = If(value = "", "0", value)
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketAMADEUS_ITINERARIOBE
    Inherits clsCounterBaseBE

    Private _NuSegmento As Byte
    Public Property NuSegmento() As Byte
        Get
            Return _NuSegmento
        End Get
        Set(ByVal value As Byte)
            _NuSegmento = value
        End Set
    End Property

    Private _CoCiudadPar As String
    Public Property CoCiudadPar() As String
        Get
            Return _CoCiudadPar
        End Get
        Set(ByVal value As String)
            _CoCiudadPar = value
        End Set
    End Property

    Private _CoLineaAerea As String
    Public Property CoLineaAerea() As String
        Get
            Return _CoLineaAerea
        End Get
        Set(ByVal value As String)
            _CoLineaAerea = value
        End Set
    End Property

    Private _NuVuelo As String
    Public Property NuVuelo() As String
        Get
            Return _NuVuelo
        End Get
        Set(ByVal value As String)
            _NuVuelo = value
        End Set
    End Property

    Private _CoSegmento As String
    Public Property CoSegmento() As String
        Get
            Return _CoSegmento
        End Get
        Set(ByVal value As String)
            _CoSegmento = value
        End Set
    End Property

    Private _CoBooking As String
    Public Property CoBooking() As String
        Get
            Return _CoBooking
        End Get
        Set(ByVal value As String)
            _CoBooking = value
        End Set
    End Property

    Private _TxFechaPar As Date
    Public Property TxFechaPar() As String
        Get
            Return _TxFechaPar
        End Get
        Set(ByVal value As String)
            If value <> "VOID" Then
                If InStr(value, "MAR") = 0 Then
                    _TxFechaPar = value
                Else
                    '     _TxFechaPar = datFechaDMYMesLetras(value)
                End If

            Else
                _TxFechaPar = "01/01/1900"
            End If

        End Set
    End Property

    Private _TxHoraPar As Date
    Public Property TxHoraPar() As String
        Get
            Return _TxHoraPar
        End Get
        Set(ByVal value As String)
            If value <> "VOID" Then
                If InStr(value, ":") = 0 And value.Length = 4 Then
                    _TxHoraPar = value.Substring(0, 2) & ":" & value.Substring(2, 2)
                Else
                    _TxHoraPar = value
                End If

            Else
                _TxHoraPar = "01/01/1900"
            End If
        End Set
    End Property

    Private _TxBaseTarifa As String
    Public Property TxBaseTarifa() As String
        Get
            Return _TxBaseTarifa
        End Get
        Set(ByVal value As String)
            _TxBaseTarifa = value
        End Set
    End Property

    Private _TxBag As String
    Public Property TxBag() As String
        Get
            Return _TxBag
        End Get
        Set(ByVal value As String)
            _TxBag = value
        End Set
    End Property

    Private _CoST As String
    Public Property CoST() As String
        Get
            Return _CoST
        End Get
        Set(ByVal value As String)
            _CoST = value
        End Set
    End Property

    Private _CoCiudadLle As String
    Public Property CoCiudadLle() As String
        Get
            Return _CoCiudadLle
        End Get
        Set(ByVal value As String)
            _CoCiudadLle = value
        End Set
    End Property

    Private _TxHoraLle As Date
    Public Property TxHoraLle() As String
        Get
            Return _TxHoraLle
        End Get
        Set(ByVal value As String)
            If value <> "VOID" Then
                If InStr(value, ":") = 0 And value.Length = 4 Then
                    _TxHoraLle = value.Substring(0, 2) & ":" & value.Substring(2, 2)
                Else
                    _TxHoraLle = value
                End If
            Else
                _TxHoraLle = "01/01/1900"
            End If
        End Set
    End Property

    Private _CoPNR_Amadeus As String
    Public Property CoPNR_Amadeus() As String
        Get
            Return _CoPNR_Amadeus
        End Get
        Set(ByVal value As String)
            _CoPNR_Amadeus = value
        End Set
    End Property

    Private _CoPNR_Aerolinea As String
    Public Property CoPNR_Aerolinea() As String
        Get
            Return _CoPNR_Aerolinea
        End Get
        Set(ByVal value As String)
            _CoPNR_Aerolinea = value
        End Set
    End Property

    Private _IDTransp As Integer
    Public Property IDTransp() As Integer
        Get
            Return _IDTransp
        End Get
        Set(ByVal value As Integer)
            _IDTransp = value
        End Set
    End Property

End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsDocumentoBSPBE
    Inherits clsCounterBaseBE

    Private _NuDocumBSP As String
    Public Property NuDocumBSP() As String
        Get
            Return _NuDocumBSP
        End Get
        Set(ByVal value As String)
            _NuDocumBSP = value
        End Set
    End Property

    Private _CoTipDocBSP As String
    Public Property CoTipDocBSP() As String
        Get
            Return _CoTipDocBSP
        End Get
        Set(ByVal value As String)
            _CoTipDocBSP = value
        End Set
    End Property

    Private _FeEmision As Date
    Public Property FeEmision() As Date
        Get
            Return _FeEmision
        End Get
        Set(ByVal value As Date)
            _FeEmision = value
        End Set
    End Property

    Private _CoPeriodo As String
    Public Property CoPeriodo() As String
        Get
            Return _CoPeriodo
        End Get
        Set(ByVal value As String)
            _CoPeriodo = value
        End Set
    End Property

    Private _SSTotalDocum As Double
    Public Property SSTotalDocum() As Double
        Get
            Return _SSTotalDocum
        End Get
        Set(ByVal value As Double)
            _SSTotalDocum = value
        End Set
    End Property
End Class

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsTicketAMADEUS_TARIFABE
    Inherits clsCounterBaseBE

    Private _NuTarifa As Byte
    Public Property NuTarifa() As Byte
        Get
            Return _NuTarifa
        End Get
        Set(ByVal value As Byte)
            _NuTarifa = value
        End Set
    End Property

    Private _CoCiudadPar As String
    Public Property CoCiudadPar() As String
        Get
            Return _CoCiudadPar
        End Get
        Set(ByVal value As String)
            _CoCiudadPar = value
        End Set
    End Property

    Private _CoCiudadLle As String
    Public Property CoCiudadLle() As String
        Get
            Return _CoCiudadLle
        End Get
        Set(ByVal value As String)
            _CoCiudadLle = value
        End Set
    End Property

    Private _SsTarifa As String
    Public Property SsTarifa() As String
        Get
            Return _SsTarifa
        End Get
        Set(ByVal value As String)
            _SsTarifa = If(value = "", "0", value)
        End Set
    End Property

    Private _SsQ As String
    Public Property SsQ() As String
        Get
            Return _SsQ
        End Get
        Set(ByVal value As String)
            _SsQ = If(value = "", "0", value)
        End Set
    End Property

End Class

