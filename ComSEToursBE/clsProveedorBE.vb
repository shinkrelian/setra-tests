﻿Imports System.EnterpriseServices
Imports System.Runtime.InteropServices

<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsProveedorBaseBE
    ''Inherits ServicedComponent
    'Inherits ServicedComponent
    Private _IDProveedor As String
    Public Property IDProveedor() As String
        Get
            Return _IDProveedor
        End Get
        Set(ByVal value As String)
            _IDProveedor = value
        End Set
    End Property
    Private _UserMod As String
    Public Property UserMod() As String
        Get
            Return _UserMod
        End Get
        Set(ByVal value As String)
            _UserMod = value
        End Set
    End Property
End Class
<ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
Public Class clsProveedorBE
    Inherits clsProveedorBaseBE

    Private _IDTipoProv As String
    Public Property IDTipoProv() As String
        Get
            Return _IDTipoProv
        End Get
        Set(ByVal value As String)
            _IDTipoProv = value
        End Set
    End Property

    Private _IDTipoOper As Char
    Public Property IDTipoOper() As Char
        Get
            Return _IDTipoOper
        End Get
        Set(ByVal value As Char)
            _IDTipoOper = value
        End Set
    End Property

    Private _Persona As String
    Public Property Persona() As String
        Get
            Return _Persona
        End Get
        Set(ByVal value As String)
            _Persona = value
        End Set
    End Property

    Private _RazonSocial As String
    Public Property RazonSocial() As String
        Get
            Return _RazonSocial
        End Get
        Set(ByVal value As String)
            _RazonSocial = value
        End Set
    End Property

    Private _Nombre As String
    Public Property Nombre() As String
        Get
            Return _Nombre
        End Get
        Set(ByVal value As String)
            _Nombre = value
        End Set
    End Property

    Private _ApPaterno As String
    Public Property ApPaterno()
        Get
            Return _ApPaterno
        End Get
        Set(ByVal value)
            _ApPaterno = value
        End Set
    End Property

    Private _ApMaterno As String
    Public Property ApMaterno() As String
        Get
            Return _ApMaterno
        End Get
        Set(ByVal value As String)
            _ApMaterno = value
        End Set
    End Property

    Private _Domiciliado As Boolean
    Public Property Domiciliado() As Boolean
        Get
            Return _Domiciliado
        End Get
        Set(ByVal value As Boolean)
            _Domiciliado = value
        End Set
    End Property
    Private _NombreCorto As String
    Public Property NombreCorto() As String
        Get
            Return _NombreCorto
        End Get
        Set(ByVal value As String)
            _NombreCorto = value
        End Set
    End Property

    Private _Sigla As String
    Public Property Sigla() As String
        Get
            Return _Sigla
        End Get
        Set(ByVal value As String)
            _Sigla = value
        End Set
    End Property

    Private _EsCadena As Boolean
    Public Property EsCadena() As Boolean
        Get
            Return _EsCadena
        End Get
        Set(ByVal value As Boolean)
            _EsCadena = value
        End Set
    End Property

    Private _IDProveedor_Cadena As String
    Public Property IDProveedor_Cadena() As String
        Get
            Return _IDProveedor_Cadena
        End Get
        Set(ByVal value As String)
            _IDProveedor_Cadena = value
        End Set
    End Property

    Private _Direccion As String
    Public Property Direccion() As String
        Get
            Return _Direccion
        End Get
        Set(ByVal value As String)
            _Direccion = value
        End Set
    End Property

    Private _IDCiudad As String
    Public Property IDCiudad() As String
        Get
            Return _IDCiudad
        End Get
        Set(ByVal value As String)
            _IDCiudad = value
        End Set
    End Property

    Private _IDIdentidad As String
    Public Property IDIdentidad() As String
        Get
            Return _IDIdentidad
        End Get
        Set(ByVal value As String)
            _IDIdentidad = value
        End Set
    End Property

    Private _NumIdentidad As String
    Public Property NumIdentidad() As String
        Get
            Return _NumIdentidad
        End Get
        Set(ByVal value As String)
            _NumIdentidad = value
        End Set
    End Property

    Private _Telefono1 As String
    Public Property Telefono1() As String
        Get
            Return _Telefono1
        End Get
        Set(ByVal value As String)
            _Telefono1 = value
        End Set
    End Property

    Private _Telefono2 As String
    Public Property Telefono2() As String
        Get
            Return _Telefono2
        End Get
        Set(ByVal value As String)
            _Telefono2 = value
        End Set
    End Property

    Private _TelefonoReservas1 As String
    Public Property TelefonoReservas1() As String
        Get
            Return _TelefonoReservas1
        End Get
        Set(ByVal value As String)
            _TelefonoReservas1 = value
        End Set
    End Property

    Private _TelefonoReservas2 As String
    Public Property TelefonoReservas2() As String
        Get
            Return _TelefonoReservas2
        End Get
        Set(ByVal value As String)
            _TelefonoReservas2 = value
        End Set
    End Property

    Private _TelefonoRecepcion1 As String
    Public Property TelefonoRecepcion1() As String
        Get
            Return _TelefonoRecepcion1
        End Get
        Set(ByVal value As String)
            _TelefonoRecepcion1 = value
        End Set
    End Property

    Private _TelefonoRecepcion2 As String
    Public Property TelefonoRecepcion2() As String
        Get
            Return _TelefonoRecepcion2
        End Get
        Set(ByVal value As String)
            _TelefonoRecepcion2 = value
        End Set
    End Property

    Private _EmailRecepcion1 As String
    Public Property EmailRecepcion1() As String
        Get
            Return _EmailRecepcion1
        End Get
        Set(ByVal value As String)
            _EmailRecepcion1 = value
        End Set
    End Property

    Private _EmailRecepcion2 As String
    Public Property EmailRecepcion2() As String
        Get
            Return _EmailRecepcion2
        End Get
        Set(ByVal value As String)
            _EmailRecepcion2 = value
        End Set
    End Property

    Private _Celular_Claro As String
    Public Property Celular_Claro() As String
        Get
            Return _Celular_Claro
        End Get
        Set(ByVal value As String)
            _Celular_Claro = value
        End Set
    End Property

    Private _Celular_Movistar As String
    Public Property Celular_Movistar() As String
        Get
            Return _Celular_Movistar
        End Get
        Set(ByVal value As String)
            _Celular_Movistar = value
        End Set
    End Property

    Private _Celular_Nextel As String
    Public Property Celular_Nextel() As String
        Get
            Return _Celular_Nextel
        End Get
        Set(ByVal value As String)
            _Celular_Nextel = value
        End Set
    End Property

    Private _Celular As String
    Public Property Celular() As String
        Get
            Return _Celular
        End Get
        Set(ByVal value As String)
            _Celular = value
        End Set
    End Property

    Private _NomContacto As String
    Public Property NomContacto() As String
        Get
            Return _NomContacto
        End Get
        Set(ByVal value As String)
            _NomContacto = value
        End Set
    End Property

    Private _Fax As String
    Public Property Fax() As String
        Get
            Return _Fax
        End Get
        Set(ByVal value As String)
            _Fax = value
        End Set
    End Property

    Private _FaxReservas1 As String
    Public Property FaxReservas1() As String
        Get
            Return _FaxReservas1
        End Get
        Set(ByVal value As String)
            _FaxReservas1 = value
        End Set
    End Property

    Private _FaxReservas2 As String
    Public Property FaxReservas2() As String
        Get
            Return _FaxReservas2
        End Get
        Set(ByVal value As String)
            _FaxReservas2 = value
        End Set
    End Property

    Private _Email3 As String
    Public Property Email3() As String
        Get
            Return _Email3
        End Get
        Set(ByVal value As String)
            _Email3 = value
        End Set
    End Property

    Private _Email4 As String
    Public Property Email4() As String
        Get
            Return _Email4
        End Get
        Set(ByVal value As String)
            _Email4 = value
        End Set
    End Property

    Private _FaxRecepcion As String
    Public Property FaxRecepcion() As String
        Get
            Return _FaxRecepcion
        End Get
        Set(ByVal value As String)
            _FaxRecepcion = value
        End Set
    End Property

    Private _Email1 As String
    Public Property Email1() As String
        Get
            Return _Email1
        End Get
        Set(ByVal value As String)
            _Email1 = value
        End Set
    End Property

    Private _Email2 As String
    Public Property Email2() As String
        Get
            Return _Email2
        End Get
        Set(ByVal value As String)
            _Email2 = value
        End Set
    End Property

    Private _Web As String
    Public Property Web() As String
        Get
            Return _Web
        End Get
        Set(ByVal value As String)
            _Web = value
        End Set
    End Property

    Private _Adicionales As String
    Public Property Adicionales() As String
        Get
            Return _Adicionales
        End Get
        Set(ByVal value As String)
            _Adicionales = value
        End Set
    End Property

    Private _PrePago As Double
    Public Property PrePago() As Double
        Get
            Return _PrePago
        End Get
        Set(ByVal value As Double)
            _PrePago = value
        End Set
    End Property

    Private _PlazoDias As Byte
    Public Property PlazoDias() As Byte
        Get
            Return _PlazoDias
        End Get
        Set(ByVal value As Byte)
            _PlazoDias = value
        End Set
    End Property

    Private _IDProveedorInternacional As String
    Public Property IDProveedorInternacional() As String
        Get
            Return _IDProveedorInternacional
        End Get
        Set(ByVal value As String)
            _IDProveedorInternacional = value
        End Set
    End Property

    Private _IDFormaPago As String
    Public Property IDFormaPago() As String
        Get
            Return _IDFormaPago
        End Get
        Set(ByVal value As String)
            _IDFormaPago = value
        End Set
    End Property

    Private _TipoMuseo As String
    Public Property TipoMuseo() As String
        Get
            Return _TipoMuseo
        End Get
        Set(ByVal value As String)
            _TipoMuseo = value
        End Set
    End Property

    Private _Guia_Nacionalidad As String
    Public Property Guia_Nacionalidad() As String
        Get
            Return _Guia_Nacionalidad
        End Get
        Set(ByVal value As String)
            _Guia_Nacionalidad = value
        End Set
    End Property

    Private _Guia_Especialidad As String
    Public Property Guia_Especialidad() As String
        Get
            Return _Guia_Especialidad
        End Get
        Set(ByVal value As String)
            _Guia_Especialidad = value
        End Set
    End Property

    Private _Guia_Estado As Char
    Public Property Guia_Estado() As Char
        Get
            Return _Guia_Estado
        End Get
        Set(ByVal value As Char)
            _Guia_Estado = value
        End Set
    End Property

    Private _Guia_Ubica_CV As String
    Public Property Guia_Ubica_CV() As String
        Get
            Return _Guia_Ubica_CV
        End Get
        Set(ByVal value As String)
            _Guia_Ubica_CV = value
        End Set
    End Property

    Private _Guia_PoliticaMovilidad As Boolean
    Public Property Guia_PoliticaMovilidad() As Boolean
        Get
            Return _Guia_PoliticaMovilidad
        End Get
        Set(ByVal value As Boolean)
            _Guia_PoliticaMovilidad = value
        End Set
    End Property

    Private _Guia_Asociacion As String
    Public Property Guia_Asociacion() As String
        Get
            Return _Guia_Asociacion
        End Get
        Set(ByVal value As String)
            _Guia_Asociacion = value
        End Set
    End Property

    Private _Guia_CarnetApotur As Boolean
    Public Property Guia_CarnetApotur() As Boolean
        Get
            Return _Guia_CarnetApotur
        End Get
        Set(ByVal value As Boolean)
            _Guia_CarnetApotur = value
        End Set
    End Property

    Private _Guia_GuiaOficial As Boolean
    Public Property Guia_GuiaOficial() As Boolean
        Get
            Return _Guia_GuiaOficial
        End Get
        Set(ByVal value As Boolean)
            _Guia_GuiaOficial = value
        End Set
    End Property

    Private _Guia_CertificadoPBIP As Boolean
    Public Property Guia_CertificadoPBIP() As Boolean
        Get
            Return _Guia_CertificadoPBIP
        End Get
        Set(ByVal value As Boolean)
            _Guia_CertificadoPBIP = value
        End Set
    End Property

    Private _Guia_CursoPBIP As Boolean
    Public Property Guia_CursoPBIP() As Boolean
        Get
            Return _Guia_CursoPBIP
        End Get
        Set(ByVal value As Boolean)
            _Guia_CursoPBIP = value
        End Set
    End Property

    Private _Guia_ExperienciaTC As Boolean
    Public Property Guia_ExperienciaTC() As Boolean
        Get
            Return _Guia_ExperienciaTC
        End Get
        Set(ByVal value As Boolean)
            _Guia_ExperienciaTC = value
        End Set
    End Property

    Private _Guia_LugaresTC As String
    Public Property Guia_LugaresTC() As String
        Get
            Return _Guia_LugaresTC
        End Get
        Set(ByVal value As String)
            _Guia_LugaresTC = value
        End Set
    End Property

    'Nuevos campos
    Private _DiasCredito As Int32
    Public Property DiasCredito() As Int32
        Get
            Return _DiasCredito
        End Get
        Set(ByVal value As Int32)
            _DiasCredito = value
        End Set
    End Property

    Private _EmailContactoAdmin As String
    Public Property EmailContactoAdmin() As String
        Get
            Return _EmailContactoAdmin
        End Get
        Set(ByVal value As String)
            _EmailContactoAdmin = value
        End Set
    End Property

    Private _TelefonoContactoAdmin As String
    Public Property TelefonoContactoAdmin() As String
        Get
            Return _TelefonoContactoAdmin
        End Get
        Set(ByVal value As String)
            _TelefonoContactoAdmin = value
        End Set
    End Property

    Private _DocPdfRutaPdf As String
    Public Property DocPdfRutaPdf() As String
        Get
            Return _DocPdfRutaPdf
        End Get
        Set(ByVal value As String)
            _DocPdfRutaPdf = value
        End Set
    End Property

    Private _Margen As Single
    Public Property Margen() As Single
        Get
            Return _Margen
        End Get
        Set(ByVal value As Single)
            _Margen = value
        End Set
    End Property

    Private _IdUsuarioPrg As String
    Public Property IdUsuarioPrg() As String
        Get
            Return _IdUsuarioPrg
        End Get
        Set(ByVal value As String)
            _IdUsuarioPrg = value
        End Set
    End Property

    ''Nuevos Campos para Guía
    Private _Disponible As String
    Public Property Disponible() As String
        Get
            Return _Disponible
        End Get
        Set(ByVal value As String)
            _Disponible = value
        End Set
    End Property

    Private _DocFoto As String
    Public Property DocFoto() As String
        Get
            Return _DocFoto
        End Get
        Set(ByVal value As String)
            _DocFoto = value
        End Set
    End Property

    Private _DocCurriculo As String
    Public Property DocCurriculo() As String
        Get
            Return _DocCurriculo
        End Get
        Set(ByVal value As String)
            _DocCurriculo = value
        End Set
    End Property

    Private _RUC As String
    Public Property RUC() As String
        Get
            Return _RUC
        End Get
        Set(ByVal value As String)
            _RUC = value
        End Set
    End Property

    Private _FecNacimiento As Date
    Public Property FecNacimiento() As Date
        Get
            Return _FecNacimiento
        End Get
        Set(ByVal value As Date)
            _FecNacimiento = value
        End Set
    End Property

    Private _Movil1 As String
    Public Property Movil1() As String
        Get
            Return _Movil1
        End Get
        Set(ByVal value As String)
            _Movil1 = value
        End Set
    End Property

    Private _Movil2 As String
    Public Property Movil2() As String
        Get
            Return _Movil2
        End Get
        Set(ByVal value As String)
            _Movil2 = value
        End Set
    End Property

    Private _OtrosEspecialidad As String
    Public Property OtrosEspecialidad() As String
        Get
            Return _OtrosEspecialidad
        End Get
        Set(ByVal value As String)
            _OtrosEspecialidad = value
        End Set
    End Property

    Private _OtrosNacionalidad As String
    Public Property OtrosNacionalidad() As String
        Get
            Return _OtrosNacionalidad
        End Get
        Set(ByVal value As String)
            _OtrosNacionalidad = value
        End Set
    End Property

    Private _PreferFITS As Boolean
    Public Property PreferFITS() As Boolean
        Get
            Return _PreferFITS
        End Get
        Set(ByVal value As Boolean)
            _PreferFITS = value
        End Set
    End Property

    Private _PreferGroups As Boolean
    Public Property PreferGroups() As Boolean
        Get
            Return _PreferGroups
        End Get
        Set(ByVal value As Boolean)
            _PreferGroups = value
        End Set
    End Property

    Private _PreferFamilias As Boolean
    Public Property PreferFamilias() As Boolean
        Get
            Return _PreferFamilias
        End Get
        Set(ByVal value As Boolean)
            _PreferFamilias = value
        End Set
    End Property

    Private _Prefer3raEdad As Boolean
    Public Property Prefer3raEdad() As Boolean
        Get
            Return _Prefer3raEdad
        End Get
        Set(ByVal value As Boolean)
            _Prefer3raEdad = value
        End Set
    End Property

    Private _PreferOtros As String
    Public Property PreferOtros() As String
        Get
            Return _PreferOtros
        End Get
        Set(ByVal value As String)
            _PreferOtros = value
        End Set
    End Property

    Private _EntrBriefing As String
    Public Property EntrBriefing() As String
        Get
            Return _EntrBriefing
        End Get
        Set(ByVal value As String)
            _EntrBriefing = value
        End Set
    End Property

    Private _EntrCultGeneral As String
    Public Property EntrCultGeneral() As String
        Get
            Return _EntrCultGeneral
        End Get
        Set(ByVal value As String)
            _EntrCultGeneral = value
        End Set
    End Property

    Private _EntrInfHistorica As String
    Public Property EntrInfHistorica() As String
        Get
            Return _EntrInfHistorica
        End Get
        Set(ByVal value As String)
            _EntrInfHistorica = value
        End Set
    End Property

    Private _EntrActitud As String
    Public Property EntrActitud() As String
        Get
            Return _EntrActitud
        End Get
        Set(ByVal value As String)
            _EntrActitud = value
        End Set
    End Property

    Private _EntrOtros As String
    Public Property EntrOtros() As String
        Get
            Return _EntrOtros
        End Get
        Set(ByVal value As String)
            _EntrOtros = value
        End Set
    End Property

    Private _MeetingPoint As String
    Public Property MeetingPoint() As String
        Get
            Return _MeetingPoint
        End Get
        Set(ByVal value As String)
            _MeetingPoint = value
        End Set
    End Property

    Private _ObservacionPolitica As String
    Public Property ObservacionPolitica() As String
        Get
            Return _ObservacionPolitica
        End Get
        Set(ByVal value As String)
            _ObservacionPolitica = value
        End Set
    End Property

    Private _IDUsuarioProgTransp As String
    Public Property IDUsuarioProgTransp() As String
        Get
            Return _IDUsuarioProgTransp
        End Get
        Set(ByVal value As String)
            _IDUsuarioProgTransp = value
        End Set
    End Property

    Private _IDUsuarioTrasladista As String
    Public Property IDUsuarioTrasladista() As String
        Get
            Return _IDUsuarioTrasladista
        End Get
        Set(ByVal value As String)
            _IDUsuarioTrasladista = value
        End Set
    End Property

    'Nuevos campos (08/09/2014>>>): SsMontoCredito
    Private _SsMontoCredito As Double
    Public Property SsMontoCredito() As Double
        Get
            Return _SsMontoCredito
        End Get
        Set(ByVal value As Double)
            _SsMontoCredito = value
        End Set
    End Property

    'Jorge
    'Nuevos campos (10/09/2014>>>): CoMonedaCredito
    Private _CoMonedaCredito As String
    Public Property CoMonedaCredito() As String
        Get
            Return _CoMonedaCredito
        End Get
        Set(ByVal value As String)
            _CoMonedaCredito = value
        End Set
    End Property

    Private _TxLatitud As String
    Public Property TxLatitud() As String
        Get
            Return _TxLatitud
        End Get
        Set(ByVal value As String)
            _TxLatitud = value
        End Set
    End Property

    Private _TxLongitud As String
    Public Property TxLongitud() As String
        Get
            Return _TxLongitud
        End Get
        Set(ByVal value As String)
            _TxLongitud = value
        End Set
    End Property

    Private _SSTipoCambio As Double
    Public Property SSTipoCambio() As Double
        Get
            Return _SSTipoCambio
        End Get
        Set(ByVal value As Double)
            _SSTipoCambio = value
        End Set
    End Property

    Private _CoUbigeo_Oficina As String
    Public Property CoUbigeo_Oficina() As String
        Get
            Return _CoUbigeo_Oficina
        End Get
        Set(ByVal value As String)
            _CoUbigeo_Oficina = value
        End Set
    End Property

    Private _CardCodeSAP As String
    Public Property CardCodeSAP() As String
        Get
            Return _CardCodeSAP
        End Get
        Set(ByVal value As String)
            _CardCodeSAP = value
        End Set
    End Property

    Private _FlExcluirSAP As Boolean
    Public Property FlExcluirSAP() As Boolean
        Get
            Return _FlExcluirSAP
        End Get
        Set(ByVal value As Boolean)
            _FlExcluirSAP = value
        End Set
    End Property

    Private _FlEnviaraSAP As Boolean
    Public Property FlEnviaraSAP() As Boolean
        Get
            Return _FlEnviaraSAP
        End Get
        Set(ByVal value As Boolean)
            _FlEnviaraSAP = value
        End Set
    End Property

    Private _CoTarjCredAcept As String
    Public Property CoTarjCredAcept() As String
        Get
            Return _CoTarjCredAcept
        End Get
        Set(ByVal value As String)
            _CoTarjCredAcept = value
        End Set
    End Property

    Private _Usuario As String
    Public Property Usuario() As String
        Get
            Return _Usuario
        End Get
        Set(ByVal value As String)
            _Usuario = value
        End Set
    End Property

    Private _Password As String
    Public Property Password() As String
        Get
            Return _Password
        End Get
        Set(ByVal value As String)
            _Password = value
        End Set
    End Property

    Private _FlAccesoWeb As Boolean
    Public Property FlAccesoWeb() As Boolean
        Get
            Return _FlAccesoWeb
        End Get
        Set(ByVal value As Boolean)
            _FlAccesoWeb = value
        End Set
    End Property

    Private _UsuarioLogeo_Hash As String
    Public Property UsuarioLogeo_Hash() As String
        Get
            Return _UsuarioLogeo_Hash
        End Get
        Set(ByVal value As String)
            _UsuarioLogeo_Hash = value
        End Set
    End Property

    Private _FechaHash As Date
    Public Property FechaHash() As Date
        Get
            Return _FechaHash
        End Get
        Set(ByVal value As Date)
            _FechaHash = value
        End Set
    End Property

    Public Class clsAtencionProveedorBE
        Inherits clsProveedorBaseBE

        Private _IDAtencion As Byte
        Public Property IDAtencion() As Byte
            Get
                Return _IDAtencion
            End Get
            Set(ByVal value As Byte)
                _IDAtencion = value
            End Set
        End Property

        Private _Dia1 As Char
        Public Property Dia1() As Char
            Get
                Return _Dia1
            End Get
            Set(ByVal value As Char)
                _Dia1 = value
            End Set
        End Property

        Private _Dia2 As Char
        Public Property Dia2() As Char
            Get
                Return _Dia2
            End Get
            Set(ByVal value As Char)
                _Dia2 = value
            End Set
        End Property

        Private _Desde1 As String
        Public Property Desde1() As String
            Get
                Return _Desde1
            End Get
            Set(ByVal value As String)
                _Desde1 = value
            End Set
        End Property

        Private _Hasta1 As String
        Public Property Hasta1() As String
            Get
                Return _Hasta1
            End Get
            Set(ByVal value As String)
                _Hasta1 = value
            End Set
        End Property

        Private _Desde2 As String
        Public Property Desde2() As String
            Get
                Return _Desde2
            End Get
            Set(ByVal value As String)
                _Desde2 = value
            End Set
        End Property

        Private _Hasta2 As String
        Public Property Hasta2() As String
            Get
                Return _Hasta2
            End Get
            Set(ByVal value As String)
                _Hasta2 = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _EmailBiblia1 As String
    Public Property EmailBiblia1() As String
        Get
            Return _EmailBiblia1
        End Get
        Set(ByVal value As String)
            _EmailBiblia1 = value
        End Set
    End Property

    Private _EmailBiblia2 As String
    Public Property EmailBiblia2() As String
        Get
            Return _EmailBiblia2
        End Get
        Set(ByVal value As String)
            _EmailBiblia2 = value
        End Set
    End Property

    Private _ListaAtencionProveedor As List(Of clsAtencionProveedorBE)
    Public Property ListaAtencionProveedor() As List(Of clsAtencionProveedorBE)
        Get
            Return _ListaAtencionProveedor
        End Get
        Set(ByVal value As List(Of clsAtencionProveedorBE))
            _ListaAtencionProveedor = value
        End Set
    End Property


    'RANGOS APT

    <ComVisible(True)> _
    <Transaction(TransactionOption.NotSupported)> _
    Public Class clsRangosAPTProveedorBE
     

        Private _Correlativo As Int32
        Public Property Correlativo() As Int32
            Get
                Return _Correlativo
            End Get
            Set(ByVal value As Int32)
                _Correlativo = value
            End Set
        End Property

        Private _NuAnio As String
        Public Property NuAnio() As String
            Get
                Return _NuAnio
            End Get
            Set(ByVal value As String)
                _NuAnio = value
            End Set
        End Property

        Private _PaxDesde As Int16
        Public Property PaxDesde() As Int16
            Get
                Return _PaxDesde
            End Get
            Set(ByVal value As Int16)
                _PaxDesde = value
            End Set
        End Property

        Private _PaxHasta As Int16
        Public Property PaxHasta() As Int16
            Get
                Return _PaxHasta
            End Get
            Set(ByVal value As Int16)
                _PaxHasta = value
            End Set
        End Property

        Private _Monto As Double
        Public Property Monto() As Double
            Get
                Return _Monto
            End Get
            Set(ByVal value As Double)
                _Monto = value
            End Set
        End Property

        Private _UserMod As String
        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal value As String)
                _UserMod = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaRangos As List(Of clsProveedorBE.clsRangosAPTProveedorBE)
    Public Property ListaRangos() As List(Of clsProveedorBE.clsRangosAPTProveedorBE)
        Get
            Return _ListaRangos
        End Get
        Set(ByVal value As List(Of clsProveedorBE.clsRangosAPTProveedorBE))
            _ListaRangos = value
        End Set
    End Property

    'FIN RANGOS APT
    ''
    Public Class clsContactoProveedorBE
        Inherits clsProveedorBaseBE

        Private _IDContacto As String
        Public Property IDContacto() As String
            Get
                Return _IDContacto
            End Get
            Set(ByVal value As String)
                _IDContacto = value
            End Set
        End Property


        Private _IDIdentidad As String
        Public Property IDIdentidad() As String
            Get
                Return _IDIdentidad
            End Get
            Set(ByVal value As String)
                _IDIdentidad = value
            End Set
        End Property

        Private _NumIdentidad As String
        Public Property NumIdentidad() As String
            Get
                Return _NumIdentidad
            End Get
            Set(ByVal value As String)
                _NumIdentidad = value
            End Set
        End Property

        Private _Titulo As String
        Public Property Titulo() As String
            Get
                Return _Titulo
            End Get
            Set(ByVal value As String)
                _Titulo = value
            End Set
        End Property

        Private _Nombres As String
        Public Property Nombres() As String
            Get
                Return _Nombres
            End Get
            Set(ByVal value As String)
                _Nombres = value
            End Set
        End Property

        Private _Apellidos As String
        Public Property Apellidos() As String
            Get
                Return _Apellidos
            End Get
            Set(ByVal value As String)
                _Apellidos = value
            End Set
        End Property

        Private _Cargo As String
        Public Property Cargo() As String
            Get
                Return _Cargo
            End Get
            Set(ByVal value As String)
                _Cargo = value
            End Set
        End Property

        Private _Utilidad As String
        Public Property Utilidad() As String
            Get
                Return _Utilidad
            End Get
            Set(ByVal value As String)
                _Utilidad = value
            End Set
        End Property

        Private _Telefono As String
        Public Property Telefono() As String
            Get
                Return _Telefono
            End Get
            Set(ByVal value As String)
                _Telefono = value
            End Set
        End Property

        Private _Fax As String
        Public Property Fax() As String
            Get
                Return _Fax
            End Get
            Set(ByVal value As String)
                _Fax = value
            End Set
        End Property

        Private _Celular As String
        Public Property Celular() As String
            Get
                Return _Celular
            End Get
            Set(ByVal value As String)
                _Celular = value
            End Set
        End Property

        Private _Email As String
        Public Property Email() As String
            Get
                Return _Email
            End Get
            Set(ByVal value As String)
                _Email = value
            End Set
        End Property

        Private _Anexo As String
        Public Property Anexo() As String
            Get
                Return _Anexo
            End Get
            Set(ByVal value As String)
                _Anexo = value
            End Set
        End Property

        Private _FechaNacimiento As Date
        Public Property FechaNacimiento() As Date
            Get
                Return _FechaNacimiento
            End Get
            Set(ByVal value As Date)
                _FechaNacimiento = value
            End Set
        End Property

        Private _Usuario As String
        Public Property Usuario() As String
            Get
                Return _Usuario
            End Get
            Set(ByVal value As String)
                _Usuario = value
            End Set
        End Property

        Private _Password As String
        Public Property Password() As String
            Get
                Return _Password
            End Get
            Set(ByVal value As String)
                _Password = value
            End Set
        End Property

        Private _FlAccesoWeb As Boolean
        Public Property FlAccesoWeb() As Boolean
            Get
                Return _FlAccesoWeb
            End Get
            Set(ByVal value As Boolean)
                _FlAccesoWeb = value
            End Set
        End Property

        Private _UsuarioLogeo_Hash As String
        Public Property UsuarioLogeo_Hash() As String
            Get
                Return _UsuarioLogeo_Hash
            End Get
            Set(ByVal value As String)
                _UsuarioLogeo_Hash = value
            End Set
        End Property

        Private _FechaHash As Date
        Public Property FechaHash() As Date
            Get
                Return _FechaHash
            End Get
            Set(ByVal value As Date)
                _FechaHash = value
            End Set
        End Property

    End Class

    Public Class clsAdministProveedorBE
        Inherits clsProveedorBaseBE

        Private _IDAdminist As String
        Public Property IDAdminist() As String
            Get
                Return _IDAdminist
            End Get
            Set(ByVal value As String)
                _IDAdminist = value
            End Set
        End Property

        Private _Banco As String
        Public Property Banco() As String
            Get
                Return _Banco
            End Get
            Set(ByVal value As String)
                _Banco = value
            End Set
        End Property

        Private _BancoExtranjero As String
        Public Property BancoExtranjero() As String
            Get
                Return _BancoExtranjero
            End Get
            Set(ByVal value As String)
                _BancoExtranjero = value
            End Set
        End Property

        Private _CtaCte As String
        Public Property CtaCte() As String
            Get
                Return _CtaCte
            End Get
            Set(ByVal value As String)
                _CtaCte = value
            End Set
        End Property

        Private _IDMoneda As String
        Public Property IDMoneda() As String
            Get
                Return _IDMoneda
            End Get
            Set(ByVal value As String)
                _IDMoneda = value
            End Set
        End Property

        Private _SWIFT As String
        Public Property SWIFT() As String
            Get
                Return _SWIFT
            End Get
            Set(ByVal value As String)
                _SWIFT = value
            End Set
        End Property

        Private _IVAN As String
        Public Property IVAN() As String
            Get
                Return _IVAN
            End Get
            Set(ByVal value As String)
                _IVAN = value
            End Set
        End Property

        Private _NombresTitular As String
        Public Property NombresTitular() As String
            Get
                Return _NombresTitular
            End Get
            Set(ByVal value As String)
                _NombresTitular = value
            End Set
        End Property

        Private _ApellidosTitular As String
        Public Property ApellidosTitular() As String
            Get
                Return _ApellidosTitular
            End Get
            Set(ByVal value As String)
                _ApellidosTitular = value
            End Set
        End Property

        Private _ReferenciaAdminist As String
        Public Property ReferenciaAdminist() As String
            Get
                Return _ReferenciaAdminist
            End Get
            Set(ByVal value As String)
                _ReferenciaAdminist = value
            End Set
        End Property

        Private _CtaInter As String
        Public Property CtaInter() As String
            Get
                Return _CtaInter
            End Get
            Set(ByVal value As String)
                _CtaInter = value
            End Set
        End Property

        Private _BancoInterm As String
        Public Property BancoInterm() As String
            Get
                Return _BancoInterm
            End Get
            Set(ByVal value As String)
                _BancoInterm = value
            End Set
        End Property

        Private _BancoInterExtranjero As String
        Public Property BancoInterExtranjero() As String
            Get
                Return _BancoInterExtranjero
            End Get
            Set(ByVal value As String)
                _BancoInterExtranjero = value
            End Set
        End Property


        Private _TipoCuenta As String
        Public Property TipoCuenta() As String
            Get
                Return _TipoCuenta
            End Get
            Set(ByVal value As String)
                _TipoCuenta = value
            End Set
        End Property


        Private _TitularBenef As String
        Public Property TitularBenef() As String
            Get
                Return _TitularBenef
            End Get
            Set(ByVal value As String)
                _TitularBenef = value
            End Set
        End Property

        Private _TipoCuentBenef As String
        Public Property TipoCuentBenef() As String
            Get
                Return _TipoCuentBenef
            End Get
            Set(ByVal value As String)
                _TipoCuentBenef = value
            End Set
        End Property

        Private _NroCuentaBenef As String
        Public Property NroCuentaBenef() As String
            Get
                Return _NroCuentaBenef
            End Get
            Set(ByVal value As String)
                _NroCuentaBenef = value
            End Set
        End Property

        Private _DireccionBenef As String
        Public Property DireccionBenef() As String
            Get
                Return _DireccionBenef
            End Get
            Set(ByVal value As String)
                _DireccionBenef = value
            End Set
        End Property

        Private _NombreBancoPag As String
        Public Property NombreBancoPag() As String
            Get
                Return _NombreBancoPag
            End Get
            Set(ByVal value As String)
                _NombreBancoPag = value
            End Set
        End Property

        Private _PECBancoPag As String
        Public Property PECBancoPag() As String
            Get
                Return _PECBancoPag
            End Get
            Set(ByVal value As String)
                _PECBancoPag = value
            End Set
        End Property

        Private _DireccionBancoPag As String
        Public Property DireccionBancoPag() As String
            Get
                Return _DireccionBancoPag
            End Get
            Set(ByVal value As String)
                _DireccionBancoPag = value
            End Set
        End Property

        Private _SWIFTBancoPag As String
        Public Property SWIFTBancoPag() As String
            Get
                Return _SWIFTBancoPag
            End Get
            Set(ByVal value As String)
                _SWIFTBancoPag = value
            End Set
        End Property

        Private _EsOpcBancoInte As Boolean
        Public Property EsOpcBancoInte() As Boolean
            Get
                Return _EsOpcBancoInte
            End Get
            Set(ByVal value As Boolean)
                _EsOpcBancoInte = value
            End Set
        End Property


        Private _NombreBancoInte As String
        Public Property NombreBancoInte() As String
            Get
                Return _NombreBancoInte
            End Get
            Set(ByVal value As String)
                _NombreBancoInte = value
            End Set
        End Property

        Private _PECBancoInte As String
        Public Property PECBancoInte() As String
            Get
                Return _PECBancoInte
            End Get
            Set(ByVal value As String)
                _PECBancoInte = value
            End Set
        End Property

        Private _SWIFTBancoInte As String
        Public Property SWIFTBancoInte() As String
            Get
                Return _SWIFTBancoInte
            End Get
            Set(ByVal value As String)
                _SWIFTBancoInte = value
            End Set
        End Property

        Private _CuentBncPagBancoInte As String
        Public Property CuentBncPagBancoInte() As String
            Get
                Return _CuentBncPagBancoInte
            End Get
            Set(ByVal value As String)
                _CuentBncPagBancoInte = value
            End Set
        End Property


        Private _CoCBU As String
        Public Property CoCBU() As String
            Get
                Return _CoCBU
            End Get
            Set(ByVal value As String)
                _CoCBU = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaAdministProveedor As List(Of clsAdministProveedorBE)
    Public Property ListaAdministProveedor() As List(Of clsAdministProveedorBE)
        Get
            Return _ListaAdministProveedor
        End Get
        Set(ByVal value As List(Of clsAdministProveedorBE))
            _ListaAdministProveedor = value
        End Set
    End Property

    Public Class clsEntrevistaProveedorBE
        Inherits clsProveedorBaseBE
        Private _IDIdioma As String
        Public Property IDIdioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property

        Private _FlConocimiento As Boolean
        Public Property FlConocimiento() As Boolean
            Get
                Return _FlConocimiento
            End Get
            Set(ByVal value As Boolean)
                _FlConocimiento = value
            End Set
        End Property


        Private _FeEntrevista As Date
        Public Property FeEntrevista() As Date
            Get
                Return _FeEntrevista
            End Get
            Set(ByVal value As Date)
                _FeEntrevista = value
            End Set
        End Property

        Private _IDUserEntre As String
        Public Property IDUserEntre() As String
            Get
                Return _IDUserEntre
            End Get
            Set(ByVal value As String)
                _IDUserEntre = value
            End Set
        End Property

        Private _TxComentario As String
        Public Property TxComentario() As String
            Get
                Return _TxComentario
            End Get
            Set(ByVal value As String)
                _TxComentario = value
            End Set
        End Property

        Private _Estado As String
        Public Property Estado() As String
            Get
                Return _Estado
            End Get
            Set(ByVal value As String)
                _Estado = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaEntrevistaProveedor As List(Of clsEntrevistaProveedorBE)
    Public Property ListaEntrevistaProveedor() As List(Of clsEntrevistaProveedorBE)
        Get
            Return _ListaEntrevistaProveedor
        End Get
        Set(ByVal value As List(Of clsEntrevistaProveedorBE))
            _ListaEntrevistaProveedor = value
        End Set
    End Property

    Public Class clsPoliticaProveedoresBE
        Inherits clsProveedorBaseBE

        Private _IDPolitica As String
        Public Property IDPolitica() As String
            Get
                Return _IDPolitica
            End Get
            Set(ByVal value As String)
                _IDPolitica = value
            End Set
        End Property

        Private _NombrePolitica As String
        Public Property NombrePolitica() As String
            Get
                Return _NombrePolitica
            End Get
            Set(ByVal value As String)
                _NombrePolitica = value
            End Set
        End Property

        Private _Anio As String
        Public Property Anio() As String
            Get
                Return _Anio
            End Get
            Set(ByVal value As String)
                _Anio = value
            End Set
        End Property


        Private _DefinDia As Int16
        Public Property DefinDia() As Int16
            Get
                Return _DefinDia
            End Get
            Set(ByVal value As Int16)
                _DefinDia = value
            End Set
        End Property


        Private _DefinHasta As Int16
        Public Property DefinHasta() As Int16
            Get
                Return _DefinHasta
            End Get
            Set(ByVal value As Int16)
                _DefinHasta = value
            End Set
        End Property

        Private _DefinTipo As String
        Public Property DefinTipo() As String
            Get
                Return _DefinTipo
            End Get
            Set(ByVal value As String)
                _DefinTipo = value
            End Set
        End Property

        Private _RoomPreeliminar As Int16
        Public Property RoomPreeliminar() As Int16
            Get
                Return _RoomPreeliminar
            End Get
            Set(ByVal value As Int16)
                _RoomPreeliminar = value
            End Set
        End Property

        Private _RoomActualizado As Int16
        Public Property RoomActualizado() As Int16
            Get
                Return _RoomActualizado
            End Get
            Set(ByVal value As Int16)
                _RoomActualizado = value
            End Set
        End Property

        Private _RoomFinal As Int16
        Public Property RoomFinal() As Int16
            Get
                Return _RoomFinal
            End Get
            Set(ByVal value As Int16)
                _RoomFinal = value
            End Set
        End Property

        Private _DiasSinPenalidad As Int16
        Public Property DiasSinPenalidad() As Int16
            Get
                Return _DiasSinPenalidad
            End Get
            Set(ByVal value As Int16)
                _DiasSinPenalidad = value
            End Set
        End Property

        Private _PrePag1_Dias As Int16
        Public Property PrePag1_Dias() As Int16
            Get
                Return _PrePag1_Dias
            End Get
            Set(ByVal value As Int16)
                _PrePag1_Dias = value
            End Set
        End Property

        Private _PrePag1_Porc As Single
        Public Property PrePag1_Porc() As Single
            Get
                Return _PrePag1_Porc
            End Get
            Set(ByVal value As Single)
                _PrePag1_Porc = value
            End Set
        End Property

        Private _PrePag1_Mont As Double
        Public Property PrePag1_Mont() As Double
            Get
                Return _PrePag1_Mont
            End Get
            Set(ByVal value As Double)
                _PrePag1_Mont = value
            End Set
        End Property

        Private _PrePag2_Dias As Int16
        Public Property PrePag2_Dias() As Int16
            Get
                Return _PrePag2_Dias
            End Get
            Set(ByVal value As Int16)
                _PrePag2_Dias = value
            End Set
        End Property

        Private _PrePag2_Porc As Single
        Public Property PrePag2_Porc() As Single
            Get
                Return _PrePag2_Porc
            End Get
            Set(ByVal value As Single)
                _PrePag2_Porc = value
            End Set
        End Property

        Private _PrePag2_Mont As Double
        Public Property PrePag2_Mont() As Double
            Get
                Return _PrePag2_Mont
            End Get
            Set(ByVal value As Double)
                _PrePag2_Mont = value
            End Set
        End Property

        Private _Saldo_Dias As Int16
        Public Property Saldo_Dias() As Int16
            Get
                Return _Saldo_Dias
            End Get
            Set(ByVal value As Int16)
                _Saldo_Dias = value
            End Set
        End Property

        Private _Saldo_Porc As Single
        Public Property Saldo_Porc() As Single
            Get
                Return _Saldo_Porc
            End Get
            Set(ByVal value As Single)
                _Saldo_Porc = value
            End Set
        End Property

        Private _Saldo_Mont As Double
        Public Property Saldo_Mont() As Double
            Get
                Return _Saldo_Mont
            End Get
            Set(ByVal value As Double)
                _Saldo_Mont = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

        Private _PoliticaFechaReserva As Boolean
        Public Property PoliticaFechaReserva() As Boolean
            Get
                Return _PoliticaFechaReserva
            End Get
            Set(ByVal value As Boolean)
                _PoliticaFechaReserva = value
            End Set
        End Property

    End Class

    Private _ListaPoliticaProveedores As List(Of clsPoliticaProveedoresBE)
    Public Property ListaPoliticasProveedores() As List(Of clsPoliticaProveedoresBE)
        Get
            Return _ListaPoliticaProveedores
        End Get
        Set(ByVal value As List(Of clsPoliticaProveedoresBE))
            _ListaPoliticaProveedores = value
        End Set
    End Property

    Public Class clsEspecialidadProveedorBE
        Inherits clsProveedorBE

        Private _IDEspecialidad As String
        Public Property IDEspecialidad() As String
            Get
                Return _IDEspecialidad
            End Get
            Set(ByVal value As String)
                _IDEspecialidad = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaEspecialidadProveedor As List(Of clsProveedorBE.clsEspecialidadProveedorBE)
    Public Property ListaEspecialidadProveedor() As List(Of clsProveedorBE.clsEspecialidadProveedorBE)
        Get
            Return _ListaEspecialidadProveedor
        End Get
        Set(ByVal value As List(Of clsProveedorBE.clsEspecialidadProveedorBE))
            _ListaEspecialidadProveedor = value
        End Set
    End Property


    Public Class clsNacionalidadProveedorBE
        Inherits clsProveedorBE

        Private _IDUbigeo As String
        Public Property IDUbigeo() As String
            Get
                Return _IDUbigeo
            End Get
            Set(ByVal value As String)
                _IDUbigeo = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaNacionalidadProveedor As List(Of clsProveedorBE.clsNacionalidadProveedorBE)
    Public Property ListaNacionalidadProveedor() As List(Of clsProveedorBE.clsNacionalidadProveedorBE)
        Get
            Return _ListaNacionalidadProveedor
        End Get
        Set(ByVal value As List(Of clsProveedorBE.clsNacionalidadProveedorBE))
            _ListaNacionalidadProveedor = value
        End Set
    End Property


    Public Class clsIdiomaProveedorBE
        Inherits clsProveedorBaseBE


        Private _IDIdioma As String
        Public Property IDIdioma() As String
            Get
                Return _IDIdioma
            End Get
            Set(ByVal value As String)
                _IDIdioma = value
            End Set
        End Property

        Private _Nivel As String
        Public Property Nivel() As String
            Get
                Return _Nivel
            End Get
            Set(ByVal value As String)
                _Nivel = value
            End Set
        End Property
        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property
    End Class


    Private _ListaIdiomasProveedor As List(Of clsIdiomaProveedorBE)
    Public Property ListaIdiomasProveedor() As List(Of clsIdiomaProveedorBE)
        Get
            Return _ListaIdiomasProveedor
        End Get
        Set(ByVal value As List(Of clsIdiomaProveedorBE))
            _ListaIdiomasProveedor = value
        End Set
    End Property


    Public Class clsComentarioProveedorBE
        Inherits clsProveedorBaseBE

        Private _IDComentario As String
        Public Property IDComentario() As String
            Get
                Return _IDComentario
            End Get
            Set(ByVal value As String)
                _IDComentario = value
            End Set
        End Property

        Private _IDFile As String
        Public Property IDFile() As String
            Get
                Return _IDFile
            End Get
            Set(ByVal value As String)
                _IDFile = value
            End Set
        End Property

        Private _Comentario As String
        Public Property Comentario() As String
            Get
                Return _Comentario
            End Get
            Set(ByVal value As String)
                _Comentario = value
            End Set
        End Property

        Private _Estado As Char
        Public Property Estado() As Char
            Get
                Return _Estado
            End Get
            Set(ByVal value As Char)
                _Estado = value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

    End Class

    Private _ListaComentProveedor As List(Of clsComentarioProveedorBE)
    Public Property ListaComentProveedor() As List(Of clsComentarioProveedorBE)
        Get
            Return _ListaComentProveedor
        End Get
        Set(ByVal value As List(Of clsComentarioProveedorBE))
            _ListaComentProveedor = value
        End Set
    End Property

    Public Class clsCorreosReservasBE
        Inherits clsProveedorBaseBE

        Private _Correo As String
        Public Property Correo() As String
            Get
                Return _Correo
            End Get
            Set(ByVal value As String)
                _Correo = value
            End Set
        End Property

        Private _Descripcion As String
        Public Property Descripcion() As String
            Get
                Return _Descripcion
            End Get
            Set(ByVal value As String)
                _Descripcion = value
            End Set
        End Property

        Private _Estado As String
        Public Property Estado() As String
            Get
                Return _Estado
            End Get
            Set(ByVal value As String)
                _Estado = value
            End Set
        End Property

        Private _Accion As String
        Public Property Accion() As String
            Get
                Return _Accion
            End Get
            Set(ByVal value As String)
                _Accion = value
            End Set
        End Property

    End Class
    Public Class clsCorreosInternosBE
        Inherits clsCorreosReservasBE

        Private _IDUsuario As String
        Public Property IDUsuario() As String
            Get
                Return _IDUsuario
            End Get
            Set(ByVal value As String)
                _IDUsuario = value
            End Set
        End Property

    End Class

    Public Class clsCorreosClientesBE
        Inherits clsCorreosReservasBE

    End Class


    Public Class clsCorreosBibliaBE
        Inherits clsCorreosReservasBE
    End Class

    Private _ListaCorreoReservadosProveedor As List(Of clsCorreosReservasBE)
    Public Property ListaCorreoReservadosProveedor() As List(Of clsCorreosReservasBE)
        Get
            Return _ListaCorreoReservadosProveedor
        End Get
        Set(ByVal value As List(Of clsCorreosReservasBE))
            _ListaCorreoReservadosProveedor = value
        End Set
    End Property

    Private _ListaCorreoReservadosClientes As List(Of clsCorreosClientesBE)
    Public Property ListaCorreoReservadosClientes() As List(Of clsCorreosClientesBE)
        Get
            Return _ListaCorreoReservadosClientes
        End Get
        Set(ByVal value As List(Of clsCorreosClientesBE))
            _ListaCorreoReservadosClientes = value
        End Set
    End Property

    Private _ListaCorreoReservadosInternos As List(Of clsCorreosInternosBE)
    Public Property ListaCorreoReservadosInternos() As List(Of clsCorreosInternosBE)
        Get
            Return _ListaCorreoReservadosInternos
        End Get
        Set(ByVal value As List(Of clsCorreosInternosBE))
            _ListaCorreoReservadosInternos = value
        End Set
    End Property

    Private _ListaCorreosBiblia As List(Of clsCorreosBibliaBE)
    Public Property ListaCorreosBiblia() As List(Of clsCorreosBibliaBE)
        Get
            Return _ListaCorreosBiblia
        End Get
        Set(ByVal value As List(Of clsCorreosBibliaBE))
            _ListaCorreosBiblia = value
        End Set
    End Property

    <ComVisible(True)> _
<Transaction(TransactionOption.NotSupported)> _
    Public Class clsProveedores_TipoDetraccionBE
        Dim _IDProveedor As String
        Dim _CoTipoDetraccion As String
        Dim _UserMod As String

        Public Property IDProveedor() As String
            Get
                Return _IDProveedor
            End Get
            Set(ByVal Value As String)
                _IDProveedor = Value
            End Set
        End Property
        Public Property CoTipoDetraccion() As String
            Get
                Return _CoTipoDetraccion
            End Get
            Set(ByVal Value As String)
                _CoTipoDetraccion = Value
            End Set
        End Property

        Private _Accion As Char
        Public Property Accion() As Char
            Get
                Return _Accion
            End Get
            Set(ByVal value As Char)
                _Accion = value
            End Set
        End Property

        Public Property UserMod() As String
            Get
                Return _UserMod
            End Get
            Set(ByVal Value As String)
                _UserMod = Value
            End Set
        End Property

    End Class

    Private _ListaDetracciones As List(Of clsProveedores_TipoDetraccionBE)
    Public Property ListaDetracciones() As List(Of clsProveedores_TipoDetraccionBE)
        Get
            Return _ListaDetracciones
        End Get
        Set(ByVal value As List(Of clsProveedores_TipoDetraccionBE))
            _ListaDetracciones = value
        End Set
    End Property
End Class

