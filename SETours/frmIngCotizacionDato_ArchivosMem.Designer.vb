﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngCotizacionDato_ArchivosMem
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cboTipo = New System.Windows.Forms.ComboBox()
        Me.lblDocumento = New System.Windows.Forms.Label()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnSelArchivoExcel = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.lblTitulo = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.lblArchivoExcel = New System.Windows.Forms.Label()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'cboTipo
        '
        Me.cboTipo.BackColor = System.Drawing.Color.Moccasin
        Me.cboTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipo.FormattingEnabled = True
        Me.cboTipo.Location = New System.Drawing.Point(10, 108)
        Me.cboTipo.Name = "cboTipo"
        Me.cboTipo.Size = New System.Drawing.Size(179, 21)
        Me.cboTipo.TabIndex = 535
        Me.cboTipo.Tag = "Borrar"
        '
        'lblDocumento
        '
        Me.lblDocumento.AutoSize = True
        Me.lblDocumento.Location = New System.Drawing.Point(7, 47)
        Me.lblDocumento.Name = "lblDocumento"
        Me.lblDocumento.Size = New System.Drawing.Size(53, 13)
        Me.lblDocumento.TabIndex = 534
        Me.lblDocumento.Text = "Archivo:"
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.accept
        Me.btnAceptar.Location = New System.Drawing.Point(2, 6)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 274
        Me.ToolTip1.SetToolTip(Me.btnAceptar, "Grabar (F6)")
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(2, 37)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 275
        Me.ToolTip1.SetToolTip(Me.btnSalir, "Salir (ESC)")
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnSelArchivoExcel
        '
        Me.btnSelArchivoExcel.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnSelArchivoExcel.Image = Global.SETours.My.Resources.Resources.zoom
        Me.btnSelArchivoExcel.Location = New System.Drawing.Point(496, 61)
        Me.btnSelArchivoExcel.Name = "btnSelArchivoExcel"
        Me.btnSelArchivoExcel.Size = New System.Drawing.Size(30, 25)
        Me.btnSelArchivoExcel.TabIndex = 538
        Me.ToolTip1.SetToolTip(Me.btnSelArchivoExcel, "Seleccionar archivo")
        Me.btnSelArchivoExcel.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(7, 92)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(31, 13)
        Me.Label5.TabIndex = 12
        Me.Label5.Text = "Tipo"
        '
        'lblTitulo
        '
        Me.lblTitulo.BackColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblTitulo.Dock = System.Windows.Forms.DockStyle.Top
        Me.lblTitulo.Font = New System.Drawing.Font("Tahoma", 11.5!, System.Drawing.FontStyle.Bold)
        Me.lblTitulo.ForeColor = System.Drawing.Color.White
        Me.lblTitulo.Location = New System.Drawing.Point(3, 17)
        Me.lblTitulo.Name = "lblTitulo"
        Me.lblTitulo.Size = New System.Drawing.Size(548, 23)
        Me.lblTitulo.TabIndex = 0
        Me.lblTitulo.Text = "Nuevo"
        '
        'GroupBox1
        '
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.btnSelArchivoExcel)
        Me.GroupBox1.Controls.Add(Me.lblArchivoExcel)
        Me.GroupBox1.Controls.Add(Me.cboTipo)
        Me.GroupBox1.Controls.Add(Me.lblDocumento)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.lblTitulo)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Right
        Me.GroupBox1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.GroupBox1.Location = New System.Drawing.Point(36, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(554, 147)
        Me.GroupBox1.TabIndex = 276
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Tag = "Borrar"
        '
        'lblArchivoExcel
        '
        Me.lblArchivoExcel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblArchivoExcel.BackColor = System.Drawing.Color.Moccasin
        Me.lblArchivoExcel.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblArchivoExcel.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblArchivoExcel.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblArchivoExcel.Location = New System.Drawing.Point(10, 64)
        Me.lblArchivoExcel.Name = "lblArchivoExcel"
        Me.lblArchivoExcel.Size = New System.Drawing.Size(480, 22)
        Me.lblArchivoExcel.TabIndex = 537
        '
        'frmIngCotizacionDato_ArchivosMem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(590, 147)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.GroupBox1)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmIngCotizacionDato_ArchivosMem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Adjuntar Archivo"
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboTipo As System.Windows.Forms.ComboBox
    Friend WithEvents lblDocumento As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents lblTitulo As System.Windows.Forms.Label
    Friend WithEvents lblArchivoExcel As System.Windows.Forms.Label
    Friend WithEvents btnSelArchivoExcel As System.Windows.Forms.Button
End Class
