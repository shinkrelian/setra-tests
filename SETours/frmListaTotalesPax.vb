﻿Imports ComSEToursBL2
Public Class frmListaTotalesPax

    Public intIDCab As Integer
    Public strCotizacion As String

    Private Sub CrearListView()

        With lvw
            .View = View.Details
            .FullRowSelect = True
            '.GridLines = True
            .LabelEdit = False
            .HideSelection = False

            .Columns.Clear()

            .Columns.Add("Nombre", 300, HorizontalAlignment.Left)
            .Columns.Add("Simple", 100, HorizontalAlignment.Left)
            .Columns.Add("Doble", 100, HorizontalAlignment.Left)
            .Columns.Add("Triple", 100, HorizontalAlignment.Left)

        End With

    End Sub
    Private Sub CargarLvw()
        Try
            Dim objBL As New clsCotizacionBN.clsCotizacionPaxBN
            Dim dt As DataTable = objBL.ConsultarMontosxIDCabLvw(intIDCab)

            If dt.Rows.Count > 0 Then

                pCargaListView(lvw, dt, dt.Columns.Count - 1, 0)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub frmListaTotalesPax_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Me.Text = "Totales por Pax de la Cotización: " & strCotizacion
            CrearListView()
            CargarLvw()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub

    Private Sub frmListaTotalesPax_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.Escape
                Me.Close()
        End Select
    End Sub

End Class