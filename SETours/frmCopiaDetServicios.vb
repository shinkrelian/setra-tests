﻿Imports ComSEToursBL2
Public Class frmCopiaDetServicios
    Public strIDServicio As String
    Public strDescripcion As String
    Dim blnLoad As Boolean = False
    Dim blnCopio As Boolean = False
    'Public strAnio As String
    Dim strIDServicio_DetCopiado As String = ""
    Public frmRefMantServiciosProveedor As frmMantServiciosProveedor

    Private Sub frmCopiaDetServicios_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed

    End Sub

    Private Sub frmCopiaDetServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = "Copia del Detalle del Servicio: " & strDescripcion

        txtNuevoAnio.BackColor = gColorTextBoxObligat
        txtNuevoAnio.Text = Today.Year
        txtNuevoTipo.BackColor = gColorTextBoxObligat

        Try

            CargarCopiasxAnio()
            blnLoad = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)


        End Try
    End Sub
    Private Sub CargarCopiasxAnio()
        
        Try
            Dim objBN As New clsServicioProveedorBN.clsDetalleServicioProveedorBN

            dgv.DataSource = objBN.ConsultarCopiasxAnioNue(strIDServicio, txtNuevoAnio.Text)
            pDgvAnchoColumnas(dgv)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        'Me.Close()
        Me.Dispose()
    End Sub
    Private Sub frmCopiarServicios_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                btnSalir_Click(Nothing, Nothing)
        End Select
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        Try
            If Not blnValidarIngresos() Then Exit Sub

            If MessageBox.Show("¿Esta Ud. seguro de ejecutar la copia?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objBL As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
            If optAnio.Checked Then
                'Dim intAnioPreciosCopia As Int16 = Today.Year
                'If MessageBox.Show("¿Desea mantener los precios del año anterior?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                '    If MessageBox.Show("¿El proveedor le ha informado que los precios serán los mismos del año anterior?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                '        intAnioPreciosCopia -= 1
                '    End If
                'End If

                'Dim objBN As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                'Dim intIDServicio_Det As Integer = objBN.intIDservicio_DetxAnioVariante(strIDServicio, txtNuevoAnio.Text, txtTipo.Text)

                objBL.CopiarServiciosNuevoAnio(strIDServicio, _
                                                       If(txtPorcentDcto.Text.Trim = "", 0, CSng(txtPorcentDcto.Text.Trim)), _
                                                       txtNuevoAnio.Text, _
                                                       txtAnio.Text, _
                                                       If(txtTipo.Text.Trim = "", "*SF*", txtTipo.Text), _
                                                       gstrUser)
                CargarCopiasxAnio()
            Else
                dgv.DataSource = objBL.CopiarServiciosNuevaVariante(strIDServicio, _
                                                       txtNuevoTipo.Text, _
                                                       txtDetaTipo.Text, _
                                                       If(txtPorcentDcto.Text.Trim = "", 0, CSng(txtPorcentDcto.Text.Trim)), _
                                                       txtAnio.Text, _
                                                       If(txtTipo.Text.Trim = "", "*SF*", txtTipo.Text), _
                                                       gstrUser)

            End If
            pDgvAnchoColumnas(dgv)

            'btnAceptar.Enabled = False



            If dgv.RowCount > 0 Then
                blnCopio = True
                MessageBox.Show("Copia realizada exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                strIDServicio_DetCopiado = dgv("IDServicio_Det", 0).Value.ToString
            Else
                MessageBox.Show("El proceso no ha copiado nada, debe revisar el filtro", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Function blnValidarIngresos() As Boolean
        Dim blnOk As Boolean = True

        If optAnio.Checked Then
            If txtNuevoAnio.Text.Trim = "" Then
                ErrPrv.SetError(txtNuevoAnio, "Debe ingresar Nuevo Año")

                blnOk = False
            Else
                If txtNuevoAnio.Text.Length < 4 Then
                    ErrPrv.SetError(txtNuevoAnio, "Debe ingresar un Nuevo Año correcto")
                    blnOk = False
                End If

            End If

            If txtAnio.Text.Trim = "" Then
                ErrPrv.SetError(txtAnio, "Debe ingresar Año Filtro, al seleccionar Nuevo Año")
                blnOk = False
            Else
                If txtAnio.Text.Length < 4 Then
                    ErrPrv.SetError(txtAnio, "Debe ingresar un Año Filtro correcto")
                    blnOk = False
                End If
            End If
        End If
        If Not blnOk Then
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub txtAnio_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNuevoAnio.KeyPress, txtAnio.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Not IsNumeric(e.KeyChar) Then
                e.KeyChar = ""
            End If
        End If
    End Sub
    Private Sub txtPorcentDcto_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPorcentDcto.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If InStr("0123456789.-", e.KeyChar) = 0 Then
                e.KeyChar = ""
            End If
        End If

    End Sub
    Private Sub txtNuevoAnio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNuevoAnio.TextChanged, txtAnio.TextChanged
        ErrPrv.SetError(txtNuevoAnio, "")
        ErrPrv.SetError(txtAnio, "")

    End Sub

    Private Sub optAnio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optAnio.CheckedChanged
        If Not blnLoad Then Exit Sub
        Try

            If optAnio.Checked Then
                txtNuevoAnio.Enabled = True
                txtNuevoAnio.Text = Today.Year
                txtNuevoAnio.Focus()
                txtNuevoAnio.SelectAll()
                txtNuevoTipo.Enabled = False
                txtNuevoTipo.Text = ""
                txtDetaTipo.Enabled = False
                txtDetaTipo.Text = ""
                'txtPorcentDcto.Text = "0.00"
                If txtNuevoAnio.Text.Trim <> "" Then
                    Dim objBN As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                    dgv.DataSource = objBN.ConsultarCopiasxAnioNue(strIDServicio, txtNuevoAnio.Text)
                    pDgvAnchoColumnas(dgv)
                End If
            Else
                txtNuevoAnio.Enabled = False
                txtNuevoAnio.Text = ""
                txtNuevoTipo.Enabled = True
                txtDetaTipo.Enabled = True
                txtNuevoTipo.Focus()
                txtNuevoTipo.SelectAll()
                If txtNuevoTipo.Text.Trim <> "" Then
                    Dim objBN As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                    dgv.DataSource = objBN.ConsultarCopiasxVarianteNue(strIDServicio, txtNuevoTipo.Text)
                    pDgvAnchoColumnas(dgv)
                End If
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub frmCopiaDetServicios_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        frmRefMantServiciosProveedor.blnCopio = blnCopio
        frmRefMantServiciosProveedor.strIDServicio_DetCopiado = strIDServicio_DetCopiado
    End Sub

    Private Sub optTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles optTipo.CheckedChanged

    End Sub

    Private Sub txtNuevoAnio_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtNuevoAnio.Validated

        Try
            CargarCopiasxAnio()
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
End Class