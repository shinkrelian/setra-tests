﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngDetAcomodoEspecial
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmIngDetAcomodoEspecial))
        Me.btnSalir = New System.Windows.Forms.Button
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TabControlAcomodoEspecial = New System.Windows.Forms.TabControl
        Me.TabPageAcomodo = New System.Windows.Forms.TabPage
        Me.GroupBox1 = New System.Windows.Forms.GroupBox
        Me.dgvAcomodoEspecial = New System.Windows.Forms.DataGridView
        Me.ID = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.EnBD = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.PaxActual = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Pax = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Capacidad = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.HabitacionActual = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Habitacion = New System.Windows.Forms.DataGridViewComboBoxColumn
        Me.btnDelDet = New System.Windows.Forms.DataGridViewButtonColumn
        Me.TabPageDistribucion = New System.Windows.Forms.TabPage
        Me.ImageList1 = New System.Windows.Forms.ImageList(Me.components)
        Me.TabControlDistribucion = New System.Windows.Forms.TabControl
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControlAcomodoEspecial.SuspendLayout()
        Me.TabPageAcomodo.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvAcomodoEspecial, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPageDistribucion.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(4, 38)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 6
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Enabled = False
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.accept
        Me.btnAceptar.Location = New System.Drawing.Point(4, 7)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 5
        Me.btnAceptar.Tag = "Borrar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'TabControlAcomodoEspecial
        '
        Me.TabControlAcomodoEspecial.Controls.Add(Me.TabPageAcomodo)
        Me.TabControlAcomodoEspecial.Controls.Add(Me.TabPageDistribucion)
        Me.TabControlAcomodoEspecial.Location = New System.Drawing.Point(39, 7)
        Me.TabControlAcomodoEspecial.Name = "TabControlAcomodoEspecial"
        Me.TabControlAcomodoEspecial.SelectedIndex = 0
        Me.TabControlAcomodoEspecial.Size = New System.Drawing.Size(498, 378)
        Me.TabControlAcomodoEspecial.TabIndex = 11
        '
        'TabPageAcomodo
        '
        Me.TabPageAcomodo.Controls.Add(Me.GroupBox1)
        Me.TabPageAcomodo.Location = New System.Drawing.Point(4, 22)
        Me.TabPageAcomodo.Name = "TabPageAcomodo"
        Me.TabPageAcomodo.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageAcomodo.Size = New System.Drawing.Size(490, 352)
        Me.TabPageAcomodo.TabIndex = 0
        Me.TabPageAcomodo.Text = "Acomodo"
        Me.TabPageAcomodo.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.dgvAcomodoEspecial)
        Me.GroupBox1.Location = New System.Drawing.Point(6, 6)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(478, 338)
        Me.GroupBox1.TabIndex = 11
        Me.GroupBox1.TabStop = False
        '
        'dgvAcomodoEspecial
        '
        Me.dgvAcomodoEspecial.AllowUserToAddRows = False
        Me.dgvAcomodoEspecial.AllowUserToOrderColumns = True
        Me.dgvAcomodoEspecial.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAcomodoEspecial.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvAcomodoEspecial.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvAcomodoEspecial.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.ID, Me.EnBD, Me.PaxActual, Me.Pax, Me.Capacidad, Me.HabitacionActual, Me.Habitacion, Me.btnDelDet})
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvAcomodoEspecial.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgvAcomodoEspecial.Location = New System.Drawing.Point(25, 14)
        Me.dgvAcomodoEspecial.Name = "dgvAcomodoEspecial"
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvAcomodoEspecial.RowHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvAcomodoEspecial.Size = New System.Drawing.Size(424, 313)
        Me.dgvAcomodoEspecial.TabIndex = 1
        '
        'ID
        '
        Me.ID.HeaderText = "ID"
        Me.ID.Name = "ID"
        Me.ID.ReadOnly = True
        Me.ID.Visible = False
        Me.ID.Width = 50
        '
        'EnBD
        '
        Me.EnBD.HeaderText = "EnBD"
        Me.EnBD.Name = "EnBD"
        Me.EnBD.ReadOnly = True
        Me.EnBD.Visible = False
        Me.EnBD.Width = 50
        '
        'PaxActual
        '
        Me.PaxActual.HeaderText = "PaxActual"
        Me.PaxActual.MaxInputLength = 2
        Me.PaxActual.Name = "PaxActual"
        Me.PaxActual.ReadOnly = True
        Me.PaxActual.Visible = False
        '
        'Pax
        '
        Me.Pax.HeaderText = "Cant. Habit."
        Me.Pax.Name = "Pax"
        Me.Pax.Width = 70
        '
        'Capacidad
        '
        Me.Capacidad.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.Capacidad.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Capacidad.HeaderText = "Capacidad"
        Me.Capacidad.Name = "Capacidad"
        Me.Capacidad.ReadOnly = True
        Me.Capacidad.Visible = False
        '
        'HabitacionActual
        '
        Me.HabitacionActual.HeaderText = "HabitaciónActual"
        Me.HabitacionActual.Name = "HabitacionActual"
        Me.HabitacionActual.ReadOnly = True
        Me.HabitacionActual.Visible = False
        '
        'Habitacion
        '
        Me.Habitacion.DisplayStyle = System.Windows.Forms.DataGridViewComboBoxDisplayStyle.ComboBox
        Me.Habitacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Habitacion.HeaderText = "Habitación"
        Me.Habitacion.Name = "Habitacion"
        Me.Habitacion.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Habitacion.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.Habitacion.Width = 250
        '
        'btnDelDet
        '
        Me.btnDelDet.HeaderText = ""
        Me.btnDelDet.Name = "btnDelDet"
        Me.btnDelDet.Width = 40
        '
        'TabPageDistribucion
        '
        Me.TabPageDistribucion.Controls.Add(Me.TabControlDistribucion)
        Me.TabPageDistribucion.Location = New System.Drawing.Point(4, 22)
        Me.TabPageDistribucion.Name = "TabPageDistribucion"
        Me.TabPageDistribucion.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPageDistribucion.Size = New System.Drawing.Size(490, 352)
        Me.TabPageDistribucion.TabIndex = 1
        Me.TabPageDistribucion.Text = "Distribución"
        Me.TabPageDistribucion.UseVisualStyleBackColor = True
        '
        'ImageList1
        '
        Me.ImageList1.ImageStream = CType(resources.GetObject("ImageList1.ImageStream"), System.Windows.Forms.ImageListStreamer)
        Me.ImageList1.TransparentColor = System.Drawing.Color.Transparent
        Me.ImageList1.Images.SetKeyName(0, "group.png")
        Me.ImageList1.Images.SetKeyName(1, "contacto_icono.png")
        '
        'TabControlDistribucion
        '
        Me.TabControlDistribucion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TabControlDistribucion.Location = New System.Drawing.Point(6, 5)
        Me.TabControlDistribucion.Name = "TabControlDistribucion"
        Me.TabControlDistribucion.SelectedIndex = 0
        Me.TabControlDistribucion.Size = New System.Drawing.Size(478, 343)
        Me.TabControlDistribucion.TabIndex = 13
        '
        'frmIngDetAcomodoEspecial
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(547, 396)
        Me.Controls.Add(Me.TabControlAcomodoEspecial)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmIngDetAcomodoEspecial"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso de acomodo especial"
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControlAcomodoEspecial.ResumeLayout(False)
        Me.TabPageAcomodo.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.dgvAcomodoEspecial, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPageDistribucion.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents TabControlAcomodoEspecial As System.Windows.Forms.TabControl
    Friend WithEvents TabPageAcomodo As System.Windows.Forms.TabPage
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvAcomodoEspecial As System.Windows.Forms.DataGridView
    Friend WithEvents ID As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents EnBD As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PaxActual As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Pax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Capacidad As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents HabitacionActual As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Habitacion As System.Windows.Forms.DataGridViewComboBoxColumn
    Friend WithEvents btnDelDet As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents TabPageDistribucion As System.Windows.Forms.TabPage
    Friend WithEvents ImageList1 As System.Windows.Forms.ImageList
    Friend WithEvents TabControlDistribucion As System.Windows.Forms.TabControl
End Class
