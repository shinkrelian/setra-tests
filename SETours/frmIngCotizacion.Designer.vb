﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngCotizacion
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkFilesArchivados = New System.Windows.Forms.CheckBox()
        Me.chkActivos = New System.Windows.Forms.CheckBox()
        Me.dtpRango1FecAsigOper = New System.Windows.Forms.DateTimePicker()
        Me.chkRangFecAsigOperaciones = New System.Windows.Forms.CheckBox()
        Me.dtpRango2FecAsigOper = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaAsigReservIni = New System.Windows.Forms.DateTimePicker()
        Me.chkRangoFechaAsigReserva = New System.Windows.Forms.CheckBox()
        Me.dtpFechaAsigReservFin = New System.Windows.Forms.DateTimePicker()
        Me.chkRangoFechaInicio = New System.Windows.Forms.CheckBox()
        Me.dtpFechaInicioFin = New System.Windows.Forms.DateTimePicker()
        Me.dtpFechaInicioIni = New System.Windows.Forms.DateTimePicker()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtNombrePax = New System.Windows.Forms.TextBox()
        Me.chkUsuarioNoAsignado = New System.Windows.Forms.CheckBox()
        Me.chkFilesAntiguos = New System.Windows.Forms.CheckBox()
        Me.cboEstado = New System.Windows.Forms.ComboBox()
        Me.lblEstadoEtiq = New System.Windows.Forms.Label()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.txtTitulo = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtCliente = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtIDFile = New System.Windows.Forms.TextBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtCotizacion = New System.Windows.Forms.TextBox()
        Me.cboUsuario = New System.Windows.Forms.ComboBox()
        Me.lblEjecutivoEtiq = New System.Windows.Forms.Label()
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.cmsCopiarCotiz = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuCopiarCotizacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuAgregarDebitMemo = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuEjecutivoResOpe = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuArchivarFiles = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCopiarCotizacionAPedido = New System.Windows.Forms.ToolStripMenuItem()
        Me.ErrPrv2 = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cmsEjecutivosResOpe = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.mnuEjecutivoVentas = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuArchivarFiles2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.UsuarioVen = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioRes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDUsuario = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CorreoEjecReservas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CorreoEjecVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDUsuarioOper = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioOpe = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDUsuarioOper_Cusco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.UsuarioOpe_Cusco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fecha = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaAsigReserva = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cotizacion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDFile = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuPedido = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NuPedInt = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Titulo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocWordVersion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Ruta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IdCab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NroPax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Observaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDBanco = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaIn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaOut = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDNotaVenta = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FecEntregaVentas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.FechaAsigOperaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCliente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnAsigEjec = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnEntrVentas = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnDel = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnEliminarReservasuOperaciones = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnCorreos = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsCopiarCotiz.SuspendLayout()
        CType(Me.ErrPrv2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsEjecutivosResOpe.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.chkFilesArchivados)
        Me.GroupBox1.Controls.Add(Me.chkActivos)
        Me.GroupBox1.Controls.Add(Me.dtpRango1FecAsigOper)
        Me.GroupBox1.Controls.Add(Me.chkRangFecAsigOperaciones)
        Me.GroupBox1.Controls.Add(Me.dtpRango2FecAsigOper)
        Me.GroupBox1.Controls.Add(Me.dtpFechaAsigReservIni)
        Me.GroupBox1.Controls.Add(Me.chkRangoFechaAsigReserva)
        Me.GroupBox1.Controls.Add(Me.dtpFechaAsigReservFin)
        Me.GroupBox1.Controls.Add(Me.chkRangoFechaInicio)
        Me.GroupBox1.Controls.Add(Me.dtpFechaInicioFin)
        Me.GroupBox1.Controls.Add(Me.dtpFechaInicioIni)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txtNombrePax)
        Me.GroupBox1.Controls.Add(Me.chkUsuarioNoAsignado)
        Me.GroupBox1.Controls.Add(Me.chkFilesAntiguos)
        Me.GroupBox1.Controls.Add(Me.cboEstado)
        Me.GroupBox1.Controls.Add(Me.lblEstadoEtiq)
        Me.GroupBox1.Controls.Add(Me.dgv)
        Me.GroupBox1.Controls.Add(Me.GroupBox2)
        Me.GroupBox1.Controls.Add(Me.btnBuscar)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.txtTitulo)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txtCliente)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtIDFile)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.txtCotizacion)
        Me.GroupBox1.Controls.Add(Me.cboUsuario)
        Me.GroupBox1.Controls.Add(Me.lblEjecutivoEtiq)
        Me.GroupBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.GroupBox1.Location = New System.Drawing.Point(0, 0)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(764, 548)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkFilesArchivados
        '
        Me.chkFilesArchivados.AutoSize = True
        Me.chkFilesArchivados.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFilesArchivados.Location = New System.Drawing.Point(611, 35)
        Me.chkFilesArchivados.Name = "chkFilesArchivados"
        Me.chkFilesArchivados.Size = New System.Drawing.Size(140, 17)
        Me.chkFilesArchivados.TabIndex = 309
        Me.chkFilesArchivados.Text = "Mostrar files archivados"
        Me.chkFilesArchivados.UseVisualStyleBackColor = True
        '
        'chkActivos
        '
        Me.chkActivos.AutoSize = True
        Me.chkActivos.Location = New System.Drawing.Point(15, 13)
        Me.chkActivos.Name = "chkActivos"
        Me.chkActivos.Size = New System.Drawing.Size(15, 14)
        Me.chkActivos.TabIndex = 308
        Me.ToolTip1.SetToolTip(Me.chkActivos, "Ver sólo activos")
        Me.chkActivos.UseVisualStyleBackColor = True
        '
        'dtpRango1FecAsigOper
        '
        Me.dtpRango1FecAsigOper.Enabled = False
        Me.dtpRango1FecAsigOper.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRango1FecAsigOper.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRango1FecAsigOper.Location = New System.Drawing.Point(507, 116)
        Me.dtpRango1FecAsigOper.Name = "dtpRango1FecAsigOper"
        Me.dtpRango1FecAsigOper.Size = New System.Drawing.Size(98, 21)
        Me.dtpRango1FecAsigOper.TabIndex = 305
        Me.dtpRango1FecAsigOper.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpRango1FecAsigOper.Visible = False
        '
        'chkRangFecAsigOperaciones
        '
        Me.chkRangFecAsigOperaciones.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRangFecAsigOperaciones.Location = New System.Drawing.Point(368, 117)
        Me.chkRangFecAsigOperaciones.Name = "chkRangFecAsigOperaciones"
        Me.chkRangFecAsigOperaciones.Size = New System.Drawing.Size(137, 31)
        Me.chkRangFecAsigOperaciones.TabIndex = 307
        Me.chkRangFecAsigOperaciones.Text = "Rango Fec. Asignación Operaciones a Hotline"
        Me.chkRangFecAsigOperaciones.UseVisualStyleBackColor = True
        Me.chkRangFecAsigOperaciones.Visible = False
        '
        'dtpRango2FecAsigOper
        '
        Me.dtpRango2FecAsigOper.Enabled = False
        Me.dtpRango2FecAsigOper.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpRango2FecAsigOper.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpRango2FecAsigOper.Location = New System.Drawing.Point(611, 117)
        Me.dtpRango2FecAsigOper.Name = "dtpRango2FecAsigOper"
        Me.dtpRango2FecAsigOper.Size = New System.Drawing.Size(98, 21)
        Me.dtpRango2FecAsigOper.TabIndex = 306
        Me.dtpRango2FecAsigOper.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        Me.dtpRango2FecAsigOper.Visible = False
        '
        'dtpFechaAsigReservIni
        '
        Me.dtpFechaAsigReservIni.Enabled = False
        Me.dtpFechaAsigReservIni.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaAsigReservIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaAsigReservIni.Location = New System.Drawing.Point(132, 116)
        Me.dtpFechaAsigReservIni.Name = "dtpFechaAsigReservIni"
        Me.dtpFechaAsigReservIni.Size = New System.Drawing.Size(98, 21)
        Me.dtpFechaAsigReservIni.TabIndex = 302
        Me.dtpFechaAsigReservIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'chkRangoFechaAsigReserva
        '
        Me.chkRangoFechaAsigReserva.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRangoFechaAsigReserva.Location = New System.Drawing.Point(12, 114)
        Me.chkRangoFechaAsigReserva.Name = "chkRangoFechaAsigReserva"
        Me.chkRangoFechaAsigReserva.Size = New System.Drawing.Size(122, 31)
        Me.chkRangoFechaAsigReserva.TabIndex = 304
        Me.chkRangoFechaAsigReserva.Text = "Rango Fec. Asignación Reserva"
        Me.chkRangoFechaAsigReserva.UseVisualStyleBackColor = True
        '
        'dtpFechaAsigReservFin
        '
        Me.dtpFechaAsigReservFin.Enabled = False
        Me.dtpFechaAsigReservFin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaAsigReservFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaAsigReservFin.Location = New System.Drawing.Point(261, 116)
        Me.dtpFechaAsigReservFin.Name = "dtpFechaAsigReservFin"
        Me.dtpFechaAsigReservFin.Size = New System.Drawing.Size(98, 21)
        Me.dtpFechaAsigReservFin.TabIndex = 303
        Me.dtpFechaAsigReservFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'chkRangoFechaInicio
        '
        Me.chkRangoFechaInicio.AutoSize = True
        Me.chkRangoFechaInicio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkRangoFechaInicio.Location = New System.Drawing.Point(368, 95)
        Me.chkRangoFechaInicio.Name = "chkRangoFechaInicio"
        Me.chkRangoFechaInicio.Size = New System.Drawing.Size(109, 17)
        Me.chkRangoFechaInicio.TabIndex = 301
        Me.chkRangoFechaInicio.Text = "Rango Fec. Inicio"
        Me.chkRangoFechaInicio.UseVisualStyleBackColor = True
        '
        'dtpFechaInicioFin
        '
        Me.dtpFechaInicioFin.Enabled = False
        Me.dtpFechaInicioFin.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaInicioFin.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicioFin.Location = New System.Drawing.Point(611, 91)
        Me.dtpFechaInicioFin.Name = "dtpFechaInicioFin"
        Me.dtpFechaInicioFin.Size = New System.Drawing.Size(98, 21)
        Me.dtpFechaInicioFin.TabIndex = 300
        Me.dtpFechaInicioFin.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'dtpFechaInicioIni
        '
        Me.dtpFechaInicioIni.Enabled = False
        Me.dtpFechaInicioIni.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.dtpFechaInicioIni.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInicioIni.Location = New System.Drawing.Point(507, 91)
        Me.dtpFechaInicioIni.Name = "dtpFechaInicioIni"
        Me.dtpFechaInicioIni.Size = New System.Drawing.Size(98, 21)
        Me.dtpFechaInicioIni.TabIndex = 299
        Me.dtpFechaInicioIni.Value = New Date(1900, 1, 1, 0, 0, 0, 0)
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 92)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(69, 13)
        Me.Label1.TabIndex = 183
        Me.Label1.Text = "Nombre Pax:"
        '
        'txtNombrePax
        '
        Me.txtNombrePax.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtNombrePax.Location = New System.Drawing.Point(132, 89)
        Me.txtNombrePax.MaxLength = 60
        Me.txtNombrePax.Name = "txtNombrePax"
        Me.txtNombrePax.Size = New System.Drawing.Size(227, 21)
        Me.txtNombrePax.TabIndex = 169
        Me.txtNombrePax.Tag = "Borrar"
        '
        'chkUsuarioNoAsignado
        '
        Me.chkUsuarioNoAsignado.AutoSize = True
        Me.chkUsuarioNoAsignado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkUsuarioNoAsignado.Location = New System.Drawing.Point(611, 13)
        Me.chkUsuarioNoAsignado.Name = "chkUsuarioNoAsignado"
        Me.chkUsuarioNoAsignado.Size = New System.Drawing.Size(196, 17)
        Me.chkUsuarioNoAsignado.TabIndex = 181
        Me.chkUsuarioNoAsignado.Text = "Mostrar sólo reservas no asignadas"
        Me.chkUsuarioNoAsignado.UseVisualStyleBackColor = True
        Me.chkUsuarioNoAsignado.Visible = False
        '
        'chkFilesAntiguos
        '
        Me.chkFilesAntiguos.AutoSize = True
        Me.chkFilesAntiguos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkFilesAntiguos.Location = New System.Drawing.Point(824, 15)
        Me.chkFilesAntiguos.Name = "chkFilesAntiguos"
        Me.chkFilesAntiguos.Size = New System.Drawing.Size(151, 17)
        Me.chkFilesAntiguos.TabIndex = 180
        Me.chkFilesAntiguos.Text = "Mostrar sólo files antiguos"
        Me.chkFilesAntiguos.UseVisualStyleBackColor = True
        Me.chkFilesAntiguos.Visible = False
        '
        'cboEstado
        '
        Me.cboEstado.BackColor = System.Drawing.SystemColors.Window
        Me.cboEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboEstado.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboEstado.FormattingEnabled = True
        Me.cboEstado.Location = New System.Drawing.Point(429, 9)
        Me.cboEstado.Name = "cboEstado"
        Me.cboEstado.Size = New System.Drawing.Size(165, 21)
        Me.cboEstado.TabIndex = 164
        '
        'lblEstadoEtiq
        '
        Me.lblEstadoEtiq.AutoSize = True
        Me.lblEstadoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoEtiq.Location = New System.Drawing.Point(365, 11)
        Me.lblEstadoEtiq.Name = "lblEstadoEtiq"
        Me.lblEstadoEtiq.Size = New System.Drawing.Size(44, 13)
        Me.lblEstadoEtiq.TabIndex = 179
        Me.lblEstadoEtiq.Text = "Estado:"
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.UsuarioVen, Me.UsuarioRes, Me.IDUsuario, Me.CorreoEjecReservas, Me.CorreoEjecVenta, Me.IDUsuarioOper, Me.UsuarioOpe, Me.IDUsuarioOper_Cusco, Me.UsuarioOpe_Cusco, Me.Fecha, Me.FechaAsigReserva, Me.Cotizacion, Me.IDFile, Me.NuPedido, Me.NuPedInt, Me.DescCliente, Me.Titulo, Me.DocWordVersion, Me.Ruta, Me.Estado, Me.IdCab, Me.NroPax, Me.Observaciones, Me.IDBanco, Me.FechaIn, Me.FechaOut, Me.IDNotaVenta, Me.FecEntregaVentas, Me.FechaAsigOperaciones, Me.IDCliente, Me.btnAsigEjec, Me.btnEdit, Me.btnEntrVentas, Me.btnDel, Me.btnEliminarReservasuOperaciones, Me.btnCorreos})
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgv.Location = New System.Drawing.Point(15, 158)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgv.Size = New System.Drawing.Size(737, 373)
        Me.dgv.TabIndex = 177
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Location = New System.Drawing.Point(12, 144)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(737, 8)
        Me.GroupBox2.TabIndex = 176
        Me.GroupBox2.TabStop = False
        '
        'btnBuscar
        '
        Me.btnBuscar.Image = Global.SETours.My.Resources.Resources.find
        Me.btnBuscar.Location = New System.Drawing.Point(679, 58)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(32, 26)
        Me.btnBuscar.TabIndex = 175
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.Location = New System.Drawing.Point(12, 65)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(37, 13)
        Me.Label5.TabIndex = 174
        Me.Label5.Text = "Título:"
        '
        'txtTitulo
        '
        Me.txtTitulo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtTitulo.Location = New System.Drawing.Point(132, 62)
        Me.txtTitulo.MaxLength = 100
        Me.txtTitulo.Name = "txtTitulo"
        Me.txtTitulo.Size = New System.Drawing.Size(227, 21)
        Me.txtTitulo.TabIndex = 167
        Me.txtTitulo.Tag = "Borrar"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(365, 65)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(44, 13)
        Me.Label4.TabIndex = 172
        Me.Label4.Text = "Cliente:"
        '
        'txtCliente
        '
        Me.txtCliente.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCliente.Location = New System.Drawing.Point(429, 62)
        Me.txtCliente.MaxLength = 60
        Me.txtCliente.Name = "txtCliente"
        Me.txtCliente.Size = New System.Drawing.Size(244, 21)
        Me.txtCliente.TabIndex = 168
        Me.txtCliente.Tag = "Borrar"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(12, 39)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(51, 13)
        Me.Label3.TabIndex = 170
        Me.Label3.Text = "Nro. File:"
        '
        'txtIDFile
        '
        Me.txtIDFile.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtIDFile.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDFile.Location = New System.Drawing.Point(132, 36)
        Me.txtIDFile.MaxLength = 8
        Me.txtIDFile.Name = "txtIDFile"
        Me.txtIDFile.Size = New System.Drawing.Size(97, 21)
        Me.txtIDFile.TabIndex = 165
        Me.txtIDFile.Tag = "Borrar"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(365, 39)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(60, 13)
        Me.Label2.TabIndex = 168
        Me.Label2.Text = "Cotización:"
        '
        'txtCotizacion
        '
        Me.txtCotizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.txtCotizacion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtCotizacion.Location = New System.Drawing.Point(429, 36)
        Me.txtCotizacion.MaxLength = 8
        Me.txtCotizacion.Name = "txtCotizacion"
        Me.txtCotizacion.Size = New System.Drawing.Size(98, 21)
        Me.txtCotizacion.TabIndex = 166
        Me.txtCotizacion.Tag = "Borrar"
        '
        'cboUsuario
        '
        Me.cboUsuario.BackColor = System.Drawing.SystemColors.Window
        Me.cboUsuario.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboUsuario.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboUsuario.FormattingEnabled = True
        Me.cboUsuario.Location = New System.Drawing.Point(132, 9)
        Me.cboUsuario.Name = "cboUsuario"
        Me.cboUsuario.Size = New System.Drawing.Size(186, 21)
        Me.cboUsuario.TabIndex = 163
        '
        'lblEjecutivoEtiq
        '
        Me.lblEjecutivoEtiq.AutoSize = True
        Me.lblEjecutivoEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEjecutivoEtiq.Location = New System.Drawing.Point(31, 13)
        Me.lblEjecutivoEtiq.Name = "lblEjecutivoEtiq"
        Me.lblEjecutivoEtiq.Size = New System.Drawing.Size(91, 13)
        Me.lblEjecutivoEtiq.TabIndex = 164
        Me.lblEjecutivoEtiq.Text = "Ejecutivo Ventas:"
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'cmsCopiarCotiz
        '
        Me.cmsCopiarCotiz.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCopiarCotizacion, Me.mnuAgregarDebitMemo, Me.mnuEjecutivoResOpe, Me.mnuArchivarFiles, Me.mnuCopiarCotizacionAPedido})
        Me.cmsCopiarCotiz.Name = "cmsCopiarCotiz"
        Me.cmsCopiarCotiz.Size = New System.Drawing.Size(238, 114)
        '
        'mnuCopiarCotizacion
        '
        Me.mnuCopiarCotizacion.Image = Global.SETours.My.Resources.Resources.page_copy
        Me.mnuCopiarCotizacion.Name = "mnuCopiarCotizacion"
        Me.mnuCopiarCotizacion.Size = New System.Drawing.Size(237, 22)
        Me.mnuCopiarCotizacion.Text = "Copiar Cotización a mi Usuario"
        Me.mnuCopiarCotizacion.Visible = False
        '
        'mnuAgregarDebitMemo
        '
        Me.mnuAgregarDebitMemo.Image = Global.SETours.My.Resources.Resources.script_add
        Me.mnuAgregarDebitMemo.Name = "mnuAgregarDebitMemo"
        Me.mnuAgregarDebitMemo.Size = New System.Drawing.Size(237, 22)
        Me.mnuAgregarDebitMemo.Text = "Agregar a Debit Memo"
        '
        'mnuEjecutivoResOpe
        '
        Me.mnuEjecutivoResOpe.Name = "mnuEjecutivoResOpe"
        Me.mnuEjecutivoResOpe.Size = New System.Drawing.Size(237, 22)
        Me.mnuEjecutivoResOpe.Text = "Asignar Ejecutivo"
        '
        'mnuArchivarFiles
        '
        Me.mnuArchivarFiles.Name = "mnuArchivarFiles"
        Me.mnuArchivarFiles.Size = New System.Drawing.Size(237, 22)
        Me.mnuArchivarFiles.Text = "Archivar File(s)"
        '
        'mnuCopiarCotizacionAPedido
        '
        Me.mnuCopiarCotizacionAPedido.Image = Global.SETours.My.Resources.Resources.add_source_copy
        Me.mnuCopiarCotizacionAPedido.Name = "mnuCopiarCotizacionAPedido"
        Me.mnuCopiarCotizacionAPedido.Size = New System.Drawing.Size(237, 22)
        Me.mnuCopiarCotizacionAPedido.Text = "Copiar Cotización a Pedido"
        '
        'ErrPrv2
        '
        Me.ErrPrv2.ContainerControl = Me
        '
        'cmsEjecutivosResOpe
        '
        Me.cmsEjecutivosResOpe.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuEjecutivoVentas, Me.mnuArchivarFiles2})
        Me.cmsEjecutivosResOpe.Name = "cmsCopiarCotiz"
        Me.cmsEjecutivosResOpe.Size = New System.Drawing.Size(166, 48)
        '
        'mnuEjecutivoVentas
        '
        Me.mnuEjecutivoVentas.Name = "mnuEjecutivoVentas"
        Me.mnuEjecutivoVentas.Size = New System.Drawing.Size(165, 22)
        Me.mnuEjecutivoVentas.Text = "Asignar Ejecutivo"
        '
        'mnuArchivarFiles2
        '
        Me.mnuArchivarFiles2.Name = "mnuArchivarFiles2"
        Me.mnuArchivarFiles2.Size = New System.Drawing.Size(165, 22)
        Me.mnuArchivarFiles2.Text = "Archivar File(s)"
        '
        'UsuarioVen
        '
        Me.UsuarioVen.DataPropertyName = "UsuarioVen"
        Me.UsuarioVen.HeaderText = "Ejecutivo de Ventas"
        Me.UsuarioVen.Name = "UsuarioVen"
        Me.UsuarioVen.ReadOnly = True
        '
        'UsuarioRes
        '
        Me.UsuarioRes.DataPropertyName = "UsuarioRes"
        Me.UsuarioRes.HeaderText = "Ejecutivo de Reservas"
        Me.UsuarioRes.Name = "UsuarioRes"
        Me.UsuarioRes.ReadOnly = True
        '
        'IDUsuario
        '
        Me.IDUsuario.DataPropertyName = "IDUsuario"
        Me.IDUsuario.HeaderText = "IDUsuarioRes"
        Me.IDUsuario.Name = "IDUsuario"
        Me.IDUsuario.ReadOnly = True
        Me.IDUsuario.Visible = False
        '
        'CorreoEjecReservas
        '
        Me.CorreoEjecReservas.DataPropertyName = "CorreoEjecReservas"
        Me.CorreoEjecReservas.HeaderText = "CorreoEjecReservas"
        Me.CorreoEjecReservas.Name = "CorreoEjecReservas"
        Me.CorreoEjecReservas.ReadOnly = True
        Me.CorreoEjecReservas.Visible = False
        '
        'CorreoEjecVenta
        '
        Me.CorreoEjecVenta.DataPropertyName = "CorreoEjecVenta"
        Me.CorreoEjecVenta.HeaderText = "CorreoEjecVenta"
        Me.CorreoEjecVenta.Name = "CorreoEjecVenta"
        Me.CorreoEjecVenta.ReadOnly = True
        Me.CorreoEjecVenta.Visible = False
        '
        'IDUsuarioOper
        '
        Me.IDUsuarioOper.DataPropertyName = "IDUsuarioOper"
        Me.IDUsuarioOper.HeaderText = "IDUsuarioOper"
        Me.IDUsuarioOper.Name = "IDUsuarioOper"
        Me.IDUsuarioOper.ReadOnly = True
        Me.IDUsuarioOper.Visible = False
        '
        'UsuarioOpe
        '
        Me.UsuarioOpe.DataPropertyName = "UsuarioOpe"
        Me.UsuarioOpe.HeaderText = "Ejecutivo de Operaciones"
        Me.UsuarioOpe.Name = "UsuarioOpe"
        Me.UsuarioOpe.ReadOnly = True
        '
        'IDUsuarioOper_Cusco
        '
        Me.IDUsuarioOper_Cusco.DataPropertyName = "IDUsuarioOper_Cusco"
        Me.IDUsuarioOper_Cusco.HeaderText = "IDUsuarioOper_Cusco"
        Me.IDUsuarioOper_Cusco.Name = "IDUsuarioOper_Cusco"
        Me.IDUsuarioOper_Cusco.ReadOnly = True
        Me.IDUsuarioOper_Cusco.Visible = False
        '
        'UsuarioOpe_Cusco
        '
        Me.UsuarioOpe_Cusco.DataPropertyName = "UsuarioOpe_Cusco"
        Me.UsuarioOpe_Cusco.HeaderText = "Ejecutivo de Operaciones Cusco"
        Me.UsuarioOpe_Cusco.Name = "UsuarioOpe_Cusco"
        Me.UsuarioOpe_Cusco.ReadOnly = True
        '
        'Fecha
        '
        Me.Fecha.DataPropertyName = "Fecha"
        DataGridViewCellStyle2.Format = "d"
        DataGridViewCellStyle2.NullValue = " "
        Me.Fecha.DefaultCellStyle = DataGridViewCellStyle2
        Me.Fecha.HeaderText = "Fecha"
        Me.Fecha.Name = "Fecha"
        Me.Fecha.ReadOnly = True
        '
        'FechaAsigReserva
        '
        Me.FechaAsigReserva.DataPropertyName = "FechaAsigReserva"
        DataGridViewCellStyle3.Format = "d"
        DataGridViewCellStyle3.NullValue = Nothing
        Me.FechaAsigReserva.DefaultCellStyle = DataGridViewCellStyle3
        Me.FechaAsigReserva.HeaderText = "Fecha Asig. Reserva"
        Me.FechaAsigReserva.Name = "FechaAsigReserva"
        Me.FechaAsigReserva.ReadOnly = True
        Me.FechaAsigReserva.Visible = False
        '
        'Cotizacion
        '
        Me.Cotizacion.DataPropertyName = "Cotizacion"
        Me.Cotizacion.HeaderText = "Cotización"
        Me.Cotizacion.Name = "Cotizacion"
        Me.Cotizacion.ReadOnly = True
        Me.Cotizacion.Width = 70
        '
        'IDFile
        '
        Me.IDFile.DataPropertyName = "IDFile"
        Me.IDFile.HeaderText = "File"
        Me.IDFile.Name = "IDFile"
        Me.IDFile.ReadOnly = True
        '
        'NuPedido
        '
        Me.NuPedido.DataPropertyName = "NuPedido"
        Me.NuPedido.HeaderText = "Pedido"
        Me.NuPedido.Name = "NuPedido"
        Me.NuPedido.ReadOnly = True
        '
        'NuPedInt
        '
        Me.NuPedInt.DataPropertyName = "NuPedInt"
        Me.NuPedInt.HeaderText = "NuPedInt"
        Me.NuPedInt.Name = "NuPedInt"
        Me.NuPedInt.ReadOnly = True
        Me.NuPedInt.Visible = False
        '
        'DescCliente
        '
        Me.DescCliente.DataPropertyName = "DescCliente"
        Me.DescCliente.HeaderText = "Cliente"
        Me.DescCliente.Name = "DescCliente"
        Me.DescCliente.ReadOnly = True
        '
        'Titulo
        '
        Me.Titulo.DataPropertyName = "Titulo"
        Me.Titulo.HeaderText = "Título"
        Me.Titulo.Name = "Titulo"
        Me.Titulo.ReadOnly = True
        '
        'DocWordVersion
        '
        Me.DocWordVersion.DataPropertyName = "DocWordVersion"
        Me.DocWordVersion.HeaderText = "Ver"
        Me.DocWordVersion.Name = "DocWordVersion"
        Me.DocWordVersion.ReadOnly = True
        Me.DocWordVersion.Width = 35
        '
        'Ruta
        '
        Me.Ruta.DataPropertyName = "Ruta"
        Me.Ruta.HeaderText = "Ruta"
        Me.Ruta.Name = "Ruta"
        Me.Ruta.ReadOnly = True
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        Me.Estado.Visible = False
        '
        'IdCab
        '
        Me.IdCab.DataPropertyName = "IdCab"
        Me.IdCab.HeaderText = "Id. Cotización"
        Me.IdCab.Name = "IdCab"
        Me.IdCab.ReadOnly = True
        Me.IdCab.Visible = False
        '
        'NroPax
        '
        Me.NroPax.DataPropertyName = "NroPax"
        Me.NroPax.HeaderText = "NroPax"
        Me.NroPax.Name = "NroPax"
        Me.NroPax.ReadOnly = True
        Me.NroPax.Visible = False
        '
        'Observaciones
        '
        Me.Observaciones.DataPropertyName = "Observaciones"
        Me.Observaciones.HeaderText = "Observaciones"
        Me.Observaciones.Name = "Observaciones"
        Me.Observaciones.ReadOnly = True
        Me.Observaciones.Visible = False
        '
        'IDBanco
        '
        Me.IDBanco.DataPropertyName = "IDBanco"
        Me.IDBanco.HeaderText = "IDBanco"
        Me.IDBanco.Name = "IDBanco"
        Me.IDBanco.ReadOnly = True
        Me.IDBanco.Visible = False
        '
        'FechaIn
        '
        Me.FechaIn.DataPropertyName = "FechaIn"
        DataGridViewCellStyle4.Format = "d"
        DataGridViewCellStyle4.NullValue = " "
        Me.FechaIn.DefaultCellStyle = DataGridViewCellStyle4
        Me.FechaIn.HeaderText = "FechaIn"
        Me.FechaIn.Name = "FechaIn"
        Me.FechaIn.ReadOnly = True
        Me.FechaIn.Visible = False
        '
        'FechaOut
        '
        Me.FechaOut.DataPropertyName = "FechaOut"
        Me.FechaOut.HeaderText = "FechaOut"
        Me.FechaOut.Name = "FechaOut"
        Me.FechaOut.ReadOnly = True
        Me.FechaOut.Visible = False
        '
        'IDNotaVenta
        '
        Me.IDNotaVenta.DataPropertyName = "IDNotaVenta"
        Me.IDNotaVenta.HeaderText = "IDNotaVenta"
        Me.IDNotaVenta.Name = "IDNotaVenta"
        Me.IDNotaVenta.ReadOnly = True
        Me.IDNotaVenta.Visible = False
        '
        'FecEntregaVentas
        '
        Me.FecEntregaVentas.DataPropertyName = "FecEntregaVentas"
        DataGridViewCellStyle5.Format = "d"
        DataGridViewCellStyle5.NullValue = " "
        Me.FecEntregaVentas.DefaultCellStyle = DataGridViewCellStyle5
        Me.FecEntregaVentas.HeaderText = "Fecha Entrega Ventas"
        Me.FecEntregaVentas.Name = "FecEntregaVentas"
        Me.FecEntregaVentas.ReadOnly = True
        Me.FecEntregaVentas.Visible = False
        '
        'FechaAsigOperaciones
        '
        Me.FechaAsigOperaciones.DataPropertyName = "FechaAsigOperaciones"
        Me.FechaAsigOperaciones.HeaderText = "FechaAsigOperaciones"
        Me.FechaAsigOperaciones.Name = "FechaAsigOperaciones"
        Me.FechaAsigOperaciones.ReadOnly = True
        Me.FechaAsigOperaciones.Visible = False
        '
        'IDCliente
        '
        Me.IDCliente.DataPropertyName = "IDCliente"
        Me.IDCliente.HeaderText = "IDCliente"
        Me.IDCliente.Name = "IDCliente"
        Me.IDCliente.ReadOnly = True
        Me.IDCliente.Visible = False
        '
        'btnAsigEjec
        '
        Me.btnAsigEjec.HeaderText = ""
        Me.btnAsigEjec.Name = "btnAsigEjec"
        Me.btnAsigEjec.ReadOnly = True
        Me.btnAsigEjec.Visible = False
        '
        'btnEdit
        '
        Me.btnEdit.HeaderText = ""
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.ReadOnly = True
        '
        'btnEntrVentas
        '
        Me.btnEntrVentas.HeaderText = ""
        Me.btnEntrVentas.Name = "btnEntrVentas"
        Me.btnEntrVentas.ReadOnly = True
        Me.btnEntrVentas.Visible = False
        '
        'btnDel
        '
        Me.btnDel.HeaderText = ""
        Me.btnDel.Name = "btnDel"
        Me.btnDel.ReadOnly = True
        '
        'btnEliminarReservasuOperaciones
        '
        Me.btnEliminarReservasuOperaciones.HeaderText = ""
        Me.btnEliminarReservasuOperaciones.Name = "btnEliminarReservasuOperaciones"
        Me.btnEliminarReservasuOperaciones.ReadOnly = True
        Me.btnEliminarReservasuOperaciones.Visible = False
        '
        'btnCorreos
        '
        Me.btnCorreos.HeaderText = ""
        Me.btnCorreos.Name = "btnCorreos"
        Me.btnCorreos.ReadOnly = True
        Me.btnCorreos.Visible = False
        '
        'frmIngCotizacion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(764, 548)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.Name = "frmIngCotizacion"
        Me.Tag = "Cotización"
        Me.Text = "Cotizaciones"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsCopiarCotiz.ResumeLayout(False)
        CType(Me.ErrPrv2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsEjecutivosResOpe.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents cboUsuario As System.Windows.Forms.ComboBox
    Friend WithEvents lblEjecutivoEtiq As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents txtCotizacion As System.Windows.Forms.TextBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtIDFile As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtCliente As System.Windows.Forms.TextBox
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents txtTitulo As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents cboEstado As System.Windows.Forms.ComboBox
    Friend WithEvents lblEstadoEtiq As System.Windows.Forms.Label
    Friend WithEvents cmsCopiarCotiz As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents chkFilesAntiguos As System.Windows.Forms.CheckBox
    Friend WithEvents chkUsuarioNoAsignado As System.Windows.Forms.CheckBox
    Friend WithEvents mnuCopiarCotizacion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtNombrePax As System.Windows.Forms.TextBox
    Friend WithEvents mnuAgregarDebitMemo As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ErrPrv2 As System.Windows.Forms.ErrorProvider
    Friend WithEvents chkRangoFechaAsigReserva As System.Windows.Forms.CheckBox
    Friend WithEvents dtpFechaAsigReservFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaAsigReservIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkRangoFechaInicio As System.Windows.Forms.CheckBox
    Friend WithEvents dtpFechaInicioFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpFechaInicioIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpRango1FecAsigOper As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkRangFecAsigOperaciones As System.Windows.Forms.CheckBox
    Friend WithEvents dtpRango2FecAsigOper As System.Windows.Forms.DateTimePicker
    Friend WithEvents chkActivos As System.Windows.Forms.CheckBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents mnuEjecutivoResOpe As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents cmsEjecutivosResOpe As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents mnuEjecutivoVentas As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuArchivarFiles As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents chkFilesArchivados As System.Windows.Forms.CheckBox
    Friend WithEvents mnuArchivarFiles2 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCopiarCotizacionAPedido As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents UsuarioVen As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioRes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDUsuario As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CorreoEjecReservas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CorreoEjecVenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDUsuarioOper As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioOpe As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDUsuarioOper_Cusco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents UsuarioOpe_Cusco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fecha As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaAsigReserva As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cotizacion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDFile As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuPedido As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NuPedInt As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Titulo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocWordVersion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Ruta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IdCab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NroPax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Observaciones As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDBanco As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaIn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaOut As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDNotaVenta As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FecEntregaVentas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents FechaAsigOperaciones As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDCliente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnAsigEjec As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnEdit As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnEntrVentas As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnDel As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnEliminarReservasuOperaciones As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnCorreos As System.Windows.Forms.DataGridViewButtonColumn
End Class
