﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMantServiciosProveedor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle14 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle15 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle16 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle17 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle18 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.grb = New System.Windows.Forms.GroupBox()
        Me.btnVerPolitica = New System.Windows.Forms.Button()
        Me.grbControlCalidad = New System.Windows.Forms.GroupBox()
        Me.lblPoor = New System.Windows.Forms.Label()
        Me.lblAverage = New System.Windows.Forms.Label()
        Me.lblGood = New System.Windows.Forms.Label()
        Me.lblVeryGood = New System.Windows.Forms.Label()
        Me.lblExcellent = New System.Windows.Forms.Label()
        Me.lblFiles = New System.Windows.Forms.Label()
        Me.lblRoomnights = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lblRoomRes = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.btnRepControl = New System.Windows.Forms.Button()
        Me.lblPercent = New System.Windows.Forms.Label()
        Me.colorSlider5 = New MB.Controls.ColorSlider()
        Me.lblCategoriaSEToursEtiq = New System.Windows.Forms.Label()
        Me.cboCategSeTours = New System.Windows.Forms.ComboBox()
        Me.lblCategoriaEtiq = New System.Windows.Forms.Label()
        Me.nudCategoria = New System.Windows.Forms.NumericUpDown()
        Me.cboCiudad = New System.Windows.Forms.ComboBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.cboPais = New System.Windows.Forms.ComboBox()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.cboTipo = New System.Windows.Forms.ComboBox()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.lblAnioEtiq = New System.Windows.Forms.Label()
        Me.cboAnio = New System.Windows.Forms.ComboBox()
        Me.lblLectura = New System.Windows.Forms.Label()
        Me.lblDet = New System.Windows.Forms.Label()
        Me.dgvDet = New System.Windows.Forms.DataGridView()
        Me.Anio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Tipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DetaTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VerDetaTipo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ConAlojamiento = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto_sgl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto_dbl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Monto_tri = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VieneAnio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VieneIDServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VieneServicio = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.VieneIDProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.VieneProveedor = New System.Windows.Forms.DataGridViewLinkColumn()
        Me.VieneVariante = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDServicio_Det = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DesayunoDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LoncheDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AlmuerzoDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CenaDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDServicio_Det_V = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Codigo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PlanAlimenticio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoTriple = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.AfectoIgv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoMonedaDet = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idTipoOC = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDServicio_V = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnEditDet = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnDelDet = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnAnular = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnUpdRelacRetro = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.dgv = New System.Windows.Forms.DataGridView()
        Me.Descripcion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDTipoServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RazonSocial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescProvInternac = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDServicio = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCiudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescCiudad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lectura = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDTipoProv = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescTipoServ = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDProveedor = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CorreoReservas = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CorreoReservas2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDPais = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDTipoOper = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Desayuno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Lonche = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Almuerzo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Cena = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Transfer = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TipoTransporte = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDUbigeoOri = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDUbigeoDes = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDFormaPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDCabVarios = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SsTipCam = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoMonedaPais = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SimboloMonedaPais = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CoPago = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DocPdfRutaPdf = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HoraPredeterminada = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Web = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Direccion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefono1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Telefono2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HoraCheckIn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.HoraCheckOut = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDDesayuno = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.btnDetalle = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnCopiar = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnCopiarDetalles = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnEdit = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.btnDel = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.chkServCoticen = New System.Windows.Forms.CheckBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtDescServicio = New System.Windows.Forms.TextBox()
        Me.btnBuscar = New System.Windows.Forms.Button()
        Me.txtRazonSocial = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.toolBar = New System.Windows.Forms.ToolStrip()
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton()
        Me.btnEditar = New System.Windows.Forms.ToolStripButton()
        Me.btnDeshacer = New System.Windows.Forms.ToolStripButton()
        Me.btnGrabar = New System.Windows.Forms.ToolStripButton()
        Me.btnEliminar = New System.Windows.Forms.ToolStripButton()
        Me.btnImprimir = New System.Windows.Forms.ToolStripButton()
        Me.btnSalir = New System.Windows.Forms.ToolStripButton()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.dtpFechaInsertarCoti = New System.Windows.Forms.DateTimePicker()
        Me.lblEtiqDiaServicio = New System.Windows.Forms.Label()
        Me.grb.SuspendLayout()
        Me.grbControlCalidad.SuspendLayout()
        CType(Me.nudCategoria, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.toolBar.SuspendLayout()
        Me.SuspendLayout()
        '
        'grb
        '
        Me.grb.Controls.Add(Me.lblEtiqDiaServicio)
        Me.grb.Controls.Add(Me.dtpFechaInsertarCoti)
        Me.grb.Controls.Add(Me.btnVerPolitica)
        Me.grb.Controls.Add(Me.grbControlCalidad)
        Me.grb.Controls.Add(Me.lblCategoriaSEToursEtiq)
        Me.grb.Controls.Add(Me.cboCategSeTours)
        Me.grb.Controls.Add(Me.lblCategoriaEtiq)
        Me.grb.Controls.Add(Me.nudCategoria)
        Me.grb.Controls.Add(Me.cboCiudad)
        Me.grb.Controls.Add(Me.Label12)
        Me.grb.Controls.Add(Me.cboPais)
        Me.grb.Controls.Add(Me.Label13)
        Me.grb.Controls.Add(Me.cboTipo)
        Me.grb.Controls.Add(Me.Label22)
        Me.grb.Controls.Add(Me.lblAnioEtiq)
        Me.grb.Controls.Add(Me.cboAnio)
        Me.grb.Controls.Add(Me.lblLectura)
        Me.grb.Controls.Add(Me.lblDet)
        Me.grb.Controls.Add(Me.dgvDet)
        Me.grb.Controls.Add(Me.dgv)
        Me.grb.Controls.Add(Me.GroupBox2)
        Me.grb.Controls.Add(Me.chkServCoticen)
        Me.grb.Controls.Add(Me.Label3)
        Me.grb.Controls.Add(Me.txtDescServicio)
        Me.grb.Controls.Add(Me.btnBuscar)
        Me.grb.Controls.Add(Me.txtRazonSocial)
        Me.grb.Controls.Add(Me.Label1)
        Me.grb.Dock = System.Windows.Forms.DockStyle.Fill
        Me.grb.Location = New System.Drawing.Point(0, 0)
        Me.grb.Name = "grb"
        Me.grb.Size = New System.Drawing.Size(1271, 575)
        Me.grb.TabIndex = 0
        Me.grb.TabStop = False
        '
        'btnVerPolitica
        '
        Me.btnVerPolitica.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnVerPolitica.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnVerPolitica.Image = Global.SETours.My.Resources.Resources.page_white_acrobat2
        Me.btnVerPolitica.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVerPolitica.Location = New System.Drawing.Point(1142, 288)
        Me.btnVerPolitica.Name = "btnVerPolitica"
        Me.btnVerPolitica.Size = New System.Drawing.Size(117, 24)
        Me.btnVerPolitica.TabIndex = 215
        Me.btnVerPolitica.Text = "Ver Política"
        Me.btnVerPolitica.UseVisualStyleBackColor = True
        '
        'grbControlCalidad
        '
        Me.grbControlCalidad.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.grbControlCalidad.BackColor = System.Drawing.Color.White
        Me.grbControlCalidad.Controls.Add(Me.lblPoor)
        Me.grbControlCalidad.Controls.Add(Me.lblAverage)
        Me.grbControlCalidad.Controls.Add(Me.lblGood)
        Me.grbControlCalidad.Controls.Add(Me.lblVeryGood)
        Me.grbControlCalidad.Controls.Add(Me.lblExcellent)
        Me.grbControlCalidad.Controls.Add(Me.lblFiles)
        Me.grbControlCalidad.Controls.Add(Me.lblRoomnights)
        Me.grbControlCalidad.Controls.Add(Me.Label9)
        Me.grbControlCalidad.Controls.Add(Me.lblRoomRes)
        Me.grbControlCalidad.Controls.Add(Me.Label7)
        Me.grbControlCalidad.Controls.Add(Me.Label6)
        Me.grbControlCalidad.Controls.Add(Me.Label5)
        Me.grbControlCalidad.Controls.Add(Me.Label4)
        Me.grbControlCalidad.Controls.Add(Me.Label2)
        Me.grbControlCalidad.Controls.Add(Me.btnRepControl)
        Me.grbControlCalidad.Controls.Add(Me.lblPercent)
        Me.grbControlCalidad.Controls.Add(Me.colorSlider5)
        Me.grbControlCalidad.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.grbControlCalidad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.grbControlCalidad.Location = New System.Drawing.Point(677, 83)
        Me.grbControlCalidad.Name = "grbControlCalidad"
        Me.grbControlCalidad.Size = New System.Drawing.Size(316, 229)
        Me.grbControlCalidad.TabIndex = 213
        Me.grbControlCalidad.TabStop = False
        '
        'lblPoor
        '
        Me.lblPoor.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblPoor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPoor.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblPoor.Location = New System.Drawing.Point(5, 35)
        Me.lblPoor.Name = "lblPoor"
        Me.lblPoor.Size = New System.Drawing.Size(55, 19)
        Me.lblPoor.TabIndex = 228
        Me.lblPoor.Text = "0"
        Me.lblPoor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblAverage
        '
        Me.lblAverage.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblAverage.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAverage.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblAverage.Location = New System.Drawing.Point(66, 35)
        Me.lblAverage.Name = "lblAverage"
        Me.lblAverage.Size = New System.Drawing.Size(55, 19)
        Me.lblAverage.TabIndex = 227
        Me.lblAverage.Text = "0"
        Me.lblAverage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGood
        '
        Me.lblGood.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblGood.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGood.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblGood.Location = New System.Drawing.Point(127, 35)
        Me.lblGood.Name = "lblGood"
        Me.lblGood.Size = New System.Drawing.Size(55, 19)
        Me.lblGood.TabIndex = 226
        Me.lblGood.Text = "0"
        Me.lblGood.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblVeryGood
        '
        Me.lblVeryGood.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblVeryGood.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblVeryGood.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblVeryGood.Location = New System.Drawing.Point(188, 35)
        Me.lblVeryGood.Name = "lblVeryGood"
        Me.lblVeryGood.Size = New System.Drawing.Size(55, 19)
        Me.lblVeryGood.TabIndex = 225
        Me.lblVeryGood.Text = "0"
        Me.lblVeryGood.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblExcellent
        '
        Me.lblExcellent.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblExcellent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblExcellent.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblExcellent.Location = New System.Drawing.Point(249, 35)
        Me.lblExcellent.Name = "lblExcellent"
        Me.lblExcellent.Size = New System.Drawing.Size(55, 19)
        Me.lblExcellent.TabIndex = 224
        Me.lblExcellent.Text = "0"
        Me.lblExcellent.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblFiles
        '
        Me.lblFiles.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblFiles.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFiles.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblFiles.Location = New System.Drawing.Point(90, 173)
        Me.lblFiles.Name = "lblFiles"
        Me.lblFiles.Size = New System.Drawing.Size(58, 19)
        Me.lblFiles.TabIndex = 223
        Me.lblFiles.Text = "0"
        Me.lblFiles.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblRoomnights
        '
        Me.lblRoomnights.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblRoomnights.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoomnights.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblRoomnights.Location = New System.Drawing.Point(90, 143)
        Me.lblRoomnights.Name = "lblRoomnights"
        Me.lblRoomnights.Size = New System.Drawing.Size(58, 19)
        Me.lblRoomnights.TabIndex = 222
        Me.lblRoomnights.Text = "0"
        Me.lblRoomnights.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label9.Location = New System.Drawing.Point(6, 176)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(38, 13)
        Me.Label9.TabIndex = 221
        Me.Label9.Text = "Files :"
        '
        'lblRoomRes
        '
        Me.lblRoomRes.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRoomRes.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblRoomRes.Location = New System.Drawing.Point(6, 143)
        Me.lblRoomRes.Name = "lblRoomRes"
        Me.lblRoomRes.Size = New System.Drawing.Size(87, 26)
        Me.lblRoomRes.TabIndex = 220
        Me.lblRoomRes.Text = "Roomnights :"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(15, 16)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(33, 13)
        Me.Label7.TabIndex = 219
        Me.Label7.Text = "Poor"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(67, 16)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 218
        Me.Label6.Text = "Average"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(138, 16)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(36, 13)
        Me.Label5.TabIndex = 217
        Me.Label5.Text = "Good"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label4.Location = New System.Drawing.Point(184, 16)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(65, 13)
        Me.Label4.TabIndex = 216
        Me.Label4.Text = "Very Good"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label2.Location = New System.Drawing.Point(248, 16)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(58, 13)
        Me.Label2.TabIndex = 215
        Me.Label2.Text = "Excellent"
        '
        'btnRepControl
        '
        Me.btnRepControl.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.btnRepControl.Image = Global.SETours.My.Resources.Resources.printer
        Me.btnRepControl.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepControl.Location = New System.Drawing.Point(175, 194)
        Me.btnRepControl.Name = "btnRepControl"
        Me.btnRepControl.Size = New System.Drawing.Size(127, 26)
        Me.btnRepControl.TabIndex = 214
        Me.btnRepControl.Text = "Ver Reporte"
        Me.btnRepControl.UseVisualStyleBackColor = True
        '
        'lblPercent
        '
        Me.lblPercent.AutoSize = True
        Me.lblPercent.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPercent.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblPercent.Location = New System.Drawing.Point(10, 98)
        Me.lblPercent.Name = "lblPercent"
        Me.lblPercent.Size = New System.Drawing.Size(27, 13)
        Me.lblPercent.TabIndex = 231
        Me.lblPercent.Text = "0%"
        '
        'colorSlider5
        '
        Me.colorSlider5.BackColor = System.Drawing.Color.Transparent
        Me.colorSlider5.BackgroundImage = Global.SETours.My.Resources.Resources.range_feedback
        Me.colorSlider5.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.colorSlider5.BarInnerColor = System.Drawing.Color.Transparent
        Me.colorSlider5.BarOuterColor = System.Drawing.Color.Transparent
        Me.colorSlider5.BarPenColor = System.Drawing.Color.Transparent
        Me.colorSlider5.BorderRoundRectSize = New System.Drawing.Size(1, 1)
        Me.colorSlider5.ElapsedInnerColor = System.Drawing.Color.Transparent
        Me.colorSlider5.ElapsedOuterColor = System.Drawing.Color.Transparent
        Me.colorSlider5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.colorSlider5.ImeMode = System.Windows.Forms.ImeMode.Off
        Me.colorSlider5.LargeChange = CType(0UI, UInteger)
        Me.colorSlider5.Location = New System.Drawing.Point(18, 73)
        Me.colorSlider5.MouseEffects = False
        Me.colorSlider5.MouseWheelBarPartitions = 20
        Me.colorSlider5.Name = "colorSlider5"
        Me.colorSlider5.Size = New System.Drawing.Size(276, 23)
        Me.colorSlider5.SmallChange = CType(0UI, UInteger)
        Me.colorSlider5.TabIndex = 230
        Me.colorSlider5.ThumbRoundRectSize = New System.Drawing.Size(10, 15)
        Me.colorSlider5.ThumbSize = 10
        Me.colorSlider5.Value = 0
        '
        'lblCategoriaSEToursEtiq
        '
        Me.lblCategoriaSEToursEtiq.AutoSize = True
        Me.lblCategoriaSEToursEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategoriaSEToursEtiq.Location = New System.Drawing.Point(693, 44)
        Me.lblCategoriaSEToursEtiq.Name = "lblCategoriaSEToursEtiq"
        Me.lblCategoriaSEToursEtiq.Size = New System.Drawing.Size(100, 13)
        Me.lblCategoriaSEToursEtiq.TabIndex = 212
        Me.lblCategoriaSEToursEtiq.Text = "Categoría SETours:"
        Me.lblCategoriaSEToursEtiq.Visible = False
        '
        'cboCategSeTours
        '
        Me.cboCategSeTours.BackColor = System.Drawing.Color.White
        Me.cboCategSeTours.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCategSeTours.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCategSeTours.FormattingEnabled = True
        Me.cboCategSeTours.Location = New System.Drawing.Point(799, 42)
        Me.cboCategSeTours.Name = "cboCategSeTours"
        Me.cboCategSeTours.Size = New System.Drawing.Size(113, 21)
        Me.cboCategSeTours.TabIndex = 8
        Me.cboCategSeTours.Visible = False
        '
        'lblCategoriaEtiq
        '
        Me.lblCategoriaEtiq.AutoSize = True
        Me.lblCategoriaEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCategoriaEtiq.Location = New System.Drawing.Point(579, 45)
        Me.lblCategoriaEtiq.Name = "lblCategoriaEtiq"
        Me.lblCategoriaEtiq.Size = New System.Drawing.Size(58, 13)
        Me.lblCategoriaEtiq.TabIndex = 210
        Me.lblCategoriaEtiq.Text = "Categoría:"
        Me.lblCategoriaEtiq.Visible = False
        '
        'nudCategoria
        '
        Me.nudCategoria.BackColor = System.Drawing.SystemColors.Window
        Me.nudCategoria.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.nudCategoria.Location = New System.Drawing.Point(643, 42)
        Me.nudCategoria.Maximum = New Decimal(New Integer() {5, 0, 0, 0})
        Me.nudCategoria.Name = "nudCategoria"
        Me.nudCategoria.Size = New System.Drawing.Size(44, 21)
        Me.nudCategoria.TabIndex = 7
        Me.nudCategoria.Visible = False
        '
        'cboCiudad
        '
        Me.cboCiudad.BackColor = System.Drawing.SystemColors.Window
        Me.cboCiudad.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboCiudad.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboCiudad.FormattingEnabled = True
        Me.cboCiudad.Location = New System.Drawing.Point(361, 17)
        Me.cboCiudad.Name = "cboCiudad"
        Me.cboCiudad.Size = New System.Drawing.Size(206, 21)
        Me.cboCiudad.TabIndex = 2
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label12.Location = New System.Drawing.Point(302, 23)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(44, 13)
        Me.Label12.TabIndex = 208
        Me.Label12.Text = "Ciudad:"
        '
        'cboPais
        '
        Me.cboPais.BackColor = System.Drawing.SystemColors.Window
        Me.cboPais.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboPais.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboPais.FormattingEnabled = True
        Me.cboPais.Location = New System.Drawing.Point(79, 17)
        Me.cboPais.Name = "cboPais"
        Me.cboPais.Size = New System.Drawing.Size(206, 21)
        Me.cboPais.TabIndex = 1
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Label13.Location = New System.Drawing.Point(12, 20)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(30, 13)
        Me.Label13.TabIndex = 207
        Me.Label13.Text = "País:"
        '
        'cboTipo
        '
        Me.cboTipo.BackColor = System.Drawing.SystemColors.Window
        Me.cboTipo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboTipo.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboTipo.FormattingEnabled = True
        Me.cboTipo.Location = New System.Drawing.Point(615, 17)
        Me.cboTipo.Name = "cboTipo"
        Me.cboTipo.Size = New System.Drawing.Size(187, 21)
        Me.cboTipo.TabIndex = 3
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label22.Location = New System.Drawing.Point(578, 20)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(31, 13)
        Me.Label22.TabIndex = 113
        Me.Label22.Text = "Tipo:"
        '
        'lblAnioEtiq
        '
        Me.lblAnioEtiq.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblAnioEtiq.AutoSize = True
        Me.lblAnioEtiq.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAnioEtiq.Location = New System.Drawing.Point(996, 294)
        Me.lblAnioEtiq.Name = "lblAnioEtiq"
        Me.lblAnioEtiq.Size = New System.Drawing.Size(30, 13)
        Me.lblAnioEtiq.TabIndex = 110
        Me.lblAnioEtiq.Text = "Año:"
        Me.lblAnioEtiq.Visible = False
        '
        'cboAnio
        '
        Me.cboAnio.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cboAnio.BackColor = System.Drawing.Color.White
        Me.cboAnio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cboAnio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cboAnio.FormattingEnabled = True
        Me.cboAnio.Location = New System.Drawing.Point(1032, 291)
        Me.cboAnio.Name = "cboAnio"
        Me.cboAnio.Size = New System.Drawing.Size(104, 21)
        Me.cboAnio.TabIndex = 109
        Me.cboAnio.Visible = False
        '
        'lblLectura
        '
        Me.lblLectura.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lblLectura.BackColor = System.Drawing.Color.White
        Me.lblLectura.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.lblLectura.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLectura.Location = New System.Drawing.Point(999, 88)
        Me.lblLectura.Name = "lblLectura"
        Me.lblLectura.Size = New System.Drawing.Size(260, 190)
        Me.lblLectura.TabIndex = 108
        '
        'lblDet
        '
        Me.lblDet.AutoSize = True
        Me.lblDet.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDet.Location = New System.Drawing.Point(6, 319)
        Me.lblDet.Name = "lblDet"
        Me.lblDet.Size = New System.Drawing.Size(47, 13)
        Me.lblDet.TabIndex = 103
        Me.lblDet.Tag = ""
        Me.lblDet.Text = "Detalle"
        '
        'dgvDet
        '
        Me.dgvDet.AllowUserToAddRows = False
        Me.dgvDet.AllowUserToDeleteRows = False
        Me.dgvDet.AllowUserToOrderColumns = True
        Me.dgvDet.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDet.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvDet.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvDet.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Anio, Me.DescripcionDet, Me.Tipo, Me.DetaTipo, Me.VerDetaTipo, Me.ConAlojamiento, Me.Monto_sgl, Me.Monto_dbl, Me.Monto_tri, Me.VieneAnio, Me.VieneIDServicio, Me.VieneServicio, Me.VieneIDProveedor, Me.VieneProveedor, Me.VieneVariante, Me.IDServicio_Det, Me.DesayunoDet, Me.LoncheDet, Me.AlmuerzoDet, Me.CenaDet, Me.IDServicio_Det_V, Me.Codigo, Me.TC, Me.PlanAlimenticio, Me.Estado, Me.TipoTriple, Me.AfectoIgv, Me.CoMonedaDet, Me.idTipoOC, Me.IDServicio_V, Me.btnEditDet, Me.btnDelDet, Me.btnAnular, Me.btnUpdRelacRetro})
        DataGridViewCellStyle14.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle14.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle14.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle14.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle14.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle14.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle14.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvDet.DefaultCellStyle = DataGridViewCellStyle14
        Me.dgvDet.Location = New System.Drawing.Point(9, 338)
        Me.dgvDet.Name = "dgvDet"
        Me.dgvDet.ReadOnly = True
        DataGridViewCellStyle15.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle15.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle15.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle15.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle15.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle15.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvDet.RowHeadersDefaultCellStyle = DataGridViewCellStyle15
        Me.dgvDet.Size = New System.Drawing.Size(1250, 221)
        Me.dgvDet.TabIndex = 102
        '
        'Anio
        '
        Me.Anio.DataPropertyName = "Anio"
        Me.Anio.HeaderText = "Año"
        Me.Anio.Name = "Anio"
        Me.Anio.ReadOnly = True
        '
        'DescripcionDet
        '
        Me.DescripcionDet.DataPropertyName = "Descripcion"
        Me.DescripcionDet.HeaderText = "Descripción"
        Me.DescripcionDet.Name = "DescripcionDet"
        Me.DescripcionDet.ReadOnly = True
        '
        'Tipo
        '
        Me.Tipo.DataPropertyName = "Tipo"
        Me.Tipo.HeaderText = "Variante"
        Me.Tipo.Name = "Tipo"
        Me.Tipo.ReadOnly = True
        '
        'DetaTipo
        '
        Me.DetaTipo.DataPropertyName = "DetaTipo"
        Me.DetaTipo.HeaderText = "Detalle Variante"
        Me.DetaTipo.Name = "DetaTipo"
        Me.DetaTipo.ReadOnly = True
        '
        'VerDetaTipo
        '
        Me.VerDetaTipo.DataPropertyName = "VerDetaTipo"
        Me.VerDetaTipo.HeaderText = "VerDetaTipo"
        Me.VerDetaTipo.Name = "VerDetaTipo"
        Me.VerDetaTipo.ReadOnly = True
        Me.VerDetaTipo.Visible = False
        '
        'ConAlojamiento
        '
        Me.ConAlojamiento.DataPropertyName = "ConAlojamiento"
        Me.ConAlojamiento.HeaderText = "ConAlojamiento"
        Me.ConAlojamiento.Name = "ConAlojamiento"
        Me.ConAlojamiento.ReadOnly = True
        Me.ConAlojamiento.Visible = False
        '
        'Monto_sgl
        '
        Me.Monto_sgl.DataPropertyName = "Monto_sgl"
        Me.Monto_sgl.HeaderText = "Monto Simple"
        Me.Monto_sgl.Name = "Monto_sgl"
        Me.Monto_sgl.ReadOnly = True
        '
        'Monto_dbl
        '
        Me.Monto_dbl.DataPropertyName = "Monto_dbl"
        Me.Monto_dbl.HeaderText = "Monto Doble"
        Me.Monto_dbl.Name = "Monto_dbl"
        Me.Monto_dbl.ReadOnly = True
        '
        'Monto_tri
        '
        Me.Monto_tri.DataPropertyName = "Monto_tri"
        Me.Monto_tri.HeaderText = "Monto Triple"
        Me.Monto_tri.Name = "Monto_tri"
        Me.Monto_tri.ReadOnly = True
        '
        'VieneAnio
        '
        Me.VieneAnio.DataPropertyName = "VieneAnio"
        Me.VieneAnio.HeaderText = "Año que proviene"
        Me.VieneAnio.Name = "VieneAnio"
        Me.VieneAnio.ReadOnly = True
        '
        'VieneIDServicio
        '
        Me.VieneIDServicio.DataPropertyName = "VieneIDServicio"
        Me.VieneIDServicio.HeaderText = "VieneIDServicio"
        Me.VieneIDServicio.Name = "VieneIDServicio"
        Me.VieneIDServicio.ReadOnly = True
        Me.VieneIDServicio.Visible = False
        '
        'VieneServicio
        '
        Me.VieneServicio.DataPropertyName = "VieneServicio"
        Me.VieneServicio.HeaderText = "Servicio que proviene"
        Me.VieneServicio.Name = "VieneServicio"
        Me.VieneServicio.ReadOnly = True
        '
        'VieneIDProveedor
        '
        Me.VieneIDProveedor.DataPropertyName = "VieneIDProveedor"
        Me.VieneIDProveedor.HeaderText = "VieneIDProveedor"
        Me.VieneIDProveedor.Name = "VieneIDProveedor"
        Me.VieneIDProveedor.ReadOnly = True
        Me.VieneIDProveedor.Visible = False
        '
        'VieneProveedor
        '
        Me.VieneProveedor.DataPropertyName = "VieneProveedor"
        Me.VieneProveedor.HeaderText = "Proveedor que proviene"
        Me.VieneProveedor.Name = "VieneProveedor"
        Me.VieneProveedor.ReadOnly = True
        '
        'VieneVariante
        '
        Me.VieneVariante.DataPropertyName = "VieneVariante"
        Me.VieneVariante.HeaderText = "Variante que proviene"
        Me.VieneVariante.Name = "VieneVariante"
        Me.VieneVariante.ReadOnly = True
        '
        'IDServicio_Det
        '
        Me.IDServicio_Det.DataPropertyName = "IDServicio_Det"
        Me.IDServicio_Det.HeaderText = "Id. Det. Servicio"
        Me.IDServicio_Det.Name = "IDServicio_Det"
        Me.IDServicio_Det.ReadOnly = True
        Me.IDServicio_Det.Visible = False
        '
        'DesayunoDet
        '
        Me.DesayunoDet.DataPropertyName = "Desayuno"
        Me.DesayunoDet.HeaderText = "DesayunoDet"
        Me.DesayunoDet.Name = "DesayunoDet"
        Me.DesayunoDet.ReadOnly = True
        Me.DesayunoDet.Visible = False
        '
        'LoncheDet
        '
        Me.LoncheDet.DataPropertyName = "Lonche"
        Me.LoncheDet.HeaderText = "LoncheDet"
        Me.LoncheDet.Name = "LoncheDet"
        Me.LoncheDet.ReadOnly = True
        Me.LoncheDet.Visible = False
        '
        'AlmuerzoDet
        '
        Me.AlmuerzoDet.DataPropertyName = "Almuerzo"
        Me.AlmuerzoDet.HeaderText = "AlmuerzoDet"
        Me.AlmuerzoDet.Name = "AlmuerzoDet"
        Me.AlmuerzoDet.ReadOnly = True
        Me.AlmuerzoDet.Visible = False
        '
        'CenaDet
        '
        Me.CenaDet.DataPropertyName = "Cena"
        Me.CenaDet.HeaderText = "CenaDet"
        Me.CenaDet.Name = "CenaDet"
        Me.CenaDet.ReadOnly = True
        Me.CenaDet.Visible = False
        '
        'IDServicio_Det_V
        '
        Me.IDServicio_Det_V.DataPropertyName = "IDServicio_Det_V"
        Me.IDServicio_Det_V.HeaderText = "IDServicio_Det_V"
        Me.IDServicio_Det_V.Name = "IDServicio_Det_V"
        Me.IDServicio_Det_V.ReadOnly = True
        Me.IDServicio_Det_V.Visible = False
        '
        'Codigo
        '
        Me.Codigo.DataPropertyName = "Codigo"
        Me.Codigo.HeaderText = "Codigo"
        Me.Codigo.Name = "Codigo"
        Me.Codigo.ReadOnly = True
        Me.Codigo.Visible = False
        '
        'TC
        '
        Me.TC.DataPropertyName = "TC"
        Me.TC.HeaderText = "Tipo de Cambio"
        Me.TC.Name = "TC"
        Me.TC.ReadOnly = True
        Me.TC.Visible = False
        '
        'PlanAlimenticio
        '
        Me.PlanAlimenticio.DataPropertyName = "PlanAlimenticio"
        Me.PlanAlimenticio.HeaderText = "PlanAlimenticio"
        Me.PlanAlimenticio.Name = "PlanAlimenticio"
        Me.PlanAlimenticio.ReadOnly = True
        Me.PlanAlimenticio.Visible = False
        '
        'Estado
        '
        Me.Estado.DataPropertyName = "Estado"
        Me.Estado.HeaderText = "Estado"
        Me.Estado.Name = "Estado"
        Me.Estado.ReadOnly = True
        Me.Estado.Visible = False
        '
        'TipoTriple
        '
        Me.TipoTriple.DataPropertyName = "TipoTriple"
        Me.TipoTriple.HeaderText = "TipoTriple"
        Me.TipoTriple.Name = "TipoTriple"
        Me.TipoTriple.ReadOnly = True
        Me.TipoTriple.Visible = False
        '
        'AfectoIgv
        '
        Me.AfectoIgv.DataPropertyName = "AfectoIgv"
        Me.AfectoIgv.HeaderText = "AfectoIgv"
        Me.AfectoIgv.Name = "AfectoIgv"
        Me.AfectoIgv.ReadOnly = True
        Me.AfectoIgv.Visible = False
        '
        'CoMonedaDet
        '
        Me.CoMonedaDet.DataPropertyName = "CoMoneda"
        Me.CoMonedaDet.HeaderText = "CoMonedaDet"
        Me.CoMonedaDet.Name = "CoMonedaDet"
        Me.CoMonedaDet.ReadOnly = True
        Me.CoMonedaDet.Visible = False
        '
        'idTipoOC
        '
        Me.idTipoOC.DataPropertyName = "idTipoOC"
        Me.idTipoOC.HeaderText = "idTipoOC"
        Me.idTipoOC.Name = "idTipoOC"
        Me.idTipoOC.ReadOnly = True
        Me.idTipoOC.Visible = False
        '
        'IDServicio_V
        '
        Me.IDServicio_V.DataPropertyName = "IDServicio_V"
        Me.IDServicio_V.HeaderText = "IDServicio_V"
        Me.IDServicio_V.Name = "IDServicio_V"
        Me.IDServicio_V.ReadOnly = True
        Me.IDServicio_V.Visible = False
        '
        'btnEditDet
        '
        Me.btnEditDet.HeaderText = ""
        Me.btnEditDet.Name = "btnEditDet"
        Me.btnEditDet.ReadOnly = True
        Me.btnEditDet.ToolTipText = "Editar"
        Me.btnEditDet.Width = 21
        '
        'btnDelDet
        '
        Me.btnDelDet.HeaderText = ""
        Me.btnDelDet.Name = "btnDelDet"
        Me.btnDelDet.ReadOnly = True
        Me.btnDelDet.ToolTipText = "Eliminar"
        Me.btnDelDet.Width = 21
        '
        'btnAnular
        '
        Me.btnAnular.HeaderText = ""
        Me.btnAnular.Name = "btnAnular"
        Me.btnAnular.ReadOnly = True
        Me.btnAnular.Width = 21
        '
        'btnUpdRelacRetro
        '
        Me.btnUpdRelacRetro.HeaderText = ""
        Me.btnUpdRelacRetro.Name = "btnUpdRelacRetro"
        Me.btnUpdRelacRetro.ReadOnly = True
        Me.btnUpdRelacRetro.ToolTipText = "Actualizar relación en base a este detalle de servicio, para el año anterior"
        Me.btnUpdRelacRetro.Width = 21
        '
        'dgv
        '
        Me.dgv.AllowUserToAddRows = False
        Me.dgv.AllowUserToDeleteRows = False
        Me.dgv.AllowUserToOrderColumns = True
        Me.dgv.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle16.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle16.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle16.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle16.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle16.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle16.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle16.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle16
        Me.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgv.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Descripcion, Me.IDTipoServ, Me.RazonSocial, Me.DescProvInternac, Me.IDServicio, Me.IDCiudad, Me.DescCiudad, Me.Lectura, Me.IDTipoProv, Me.DescTipoServ, Me.IDProveedor, Me.CorreoReservas, Me.CorreoReservas2, Me.IDPais, Me.IDTipoOper, Me.Desayuno, Me.Lonche, Me.Almuerzo, Me.Cena, Me.Transfer, Me.TipoTransporte, Me.IDUbigeoOri, Me.IDUbigeoDes, Me.IDFormaPago, Me.IDCabVarios, Me.SsTipCam, Me.CoMonedaPais, Me.SimboloMonedaPais, Me.CoPago, Me.DocPdfRutaPdf, Me.HoraPredeterminada, Me.Web, Me.Direccion, Me.Telefono1, Me.Telefono2, Me.HoraCheckIn, Me.HoraCheckOut, Me.IDDesayuno, Me.btnDetalle, Me.btnCopiar, Me.btnCopiarDetalles, Me.btnEdit, Me.btnDel})
        DataGridViewCellStyle17.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle17.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle17.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle17.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle17.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle17.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle17.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgv.DefaultCellStyle = DataGridViewCellStyle17
        Me.dgv.Location = New System.Drawing.Point(9, 88)
        Me.dgv.Name = "dgv"
        Me.dgv.ReadOnly = True
        DataGridViewCellStyle18.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle18.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle18.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle18.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle18.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle18.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle18.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgv.RowHeadersDefaultCellStyle = DataGridViewCellStyle18
        Me.dgv.Size = New System.Drawing.Size(662, 224)
        Me.dgv.TabIndex = 101
        '
        'Descripcion
        '
        Me.Descripcion.DataPropertyName = "Descripcion"
        Me.Descripcion.HeaderText = "Descripción"
        Me.Descripcion.Name = "Descripcion"
        Me.Descripcion.ReadOnly = True
        '
        'IDTipoServ
        '
        Me.IDTipoServ.DataPropertyName = "IDTipoServ"
        Me.IDTipoServ.HeaderText = "Tipo"
        Me.IDTipoServ.Name = "IDTipoServ"
        Me.IDTipoServ.ReadOnly = True
        '
        'RazonSocial
        '
        Me.RazonSocial.DataPropertyName = "RazonSocial"
        Me.RazonSocial.HeaderText = "Proveedor"
        Me.RazonSocial.Name = "RazonSocial"
        Me.RazonSocial.ReadOnly = True
        '
        'DescProvInternac
        '
        Me.DescProvInternac.DataPropertyName = "DescProvInternac"
        Me.DescProvInternac.HeaderText = "Op. Int."
        Me.DescProvInternac.Name = "DescProvInternac"
        Me.DescProvInternac.ReadOnly = True
        '
        'IDServicio
        '
        Me.IDServicio.DataPropertyName = "IDServicio"
        Me.IDServicio.HeaderText = "Id. Servicio"
        Me.IDServicio.Name = "IDServicio"
        Me.IDServicio.ReadOnly = True
        '
        'IDCiudad
        '
        Me.IDCiudad.DataPropertyName = "IDCiudad"
        Me.IDCiudad.HeaderText = "IDCiudad"
        Me.IDCiudad.Name = "IDCiudad"
        Me.IDCiudad.ReadOnly = True
        Me.IDCiudad.Visible = False
        '
        'DescCiudad
        '
        Me.DescCiudad.DataPropertyName = "DescCiudad"
        Me.DescCiudad.HeaderText = "DescCiudad"
        Me.DescCiudad.Name = "DescCiudad"
        Me.DescCiudad.ReadOnly = True
        Me.DescCiudad.Visible = False
        '
        'Lectura
        '
        Me.Lectura.DataPropertyName = "Lectura"
        Me.Lectura.HeaderText = "Lectura"
        Me.Lectura.Name = "Lectura"
        Me.Lectura.ReadOnly = True
        Me.Lectura.Visible = False
        '
        'IDTipoProv
        '
        Me.IDTipoProv.DataPropertyName = "IDTipoProv"
        Me.IDTipoProv.HeaderText = "IDTipoProv"
        Me.IDTipoProv.Name = "IDTipoProv"
        Me.IDTipoProv.ReadOnly = True
        Me.IDTipoProv.Visible = False
        '
        'DescTipoServ
        '
        Me.DescTipoServ.DataPropertyName = "DescTipoServ"
        Me.DescTipoServ.HeaderText = "IDTipoServ"
        Me.DescTipoServ.Name = "DescTipoServ"
        Me.DescTipoServ.ReadOnly = True
        Me.DescTipoServ.Visible = False
        '
        'IDProveedor
        '
        Me.IDProveedor.DataPropertyName = "IDProveedor"
        Me.IDProveedor.HeaderText = "IDProveedor"
        Me.IDProveedor.Name = "IDProveedor"
        Me.IDProveedor.ReadOnly = True
        Me.IDProveedor.Visible = False
        '
        'CorreoReservas
        '
        Me.CorreoReservas.DataPropertyName = "CorreoReservas"
        Me.CorreoReservas.HeaderText = "CorreoReservas"
        Me.CorreoReservas.Name = "CorreoReservas"
        Me.CorreoReservas.ReadOnly = True
        Me.CorreoReservas.Visible = False
        '
        'CorreoReservas2
        '
        Me.CorreoReservas2.DataPropertyName = "CorreoReservas2"
        Me.CorreoReservas2.HeaderText = "CorreoReservas2"
        Me.CorreoReservas2.Name = "CorreoReservas2"
        Me.CorreoReservas2.ReadOnly = True
        Me.CorreoReservas2.Visible = False
        '
        'IDPais
        '
        Me.IDPais.DataPropertyName = "IDPais"
        Me.IDPais.HeaderText = "IDPais"
        Me.IDPais.Name = "IDPais"
        Me.IDPais.ReadOnly = True
        Me.IDPais.Visible = False
        '
        'IDTipoOper
        '
        Me.IDTipoOper.DataPropertyName = "IDTipoOper"
        Me.IDTipoOper.HeaderText = "IDTipoOper"
        Me.IDTipoOper.Name = "IDTipoOper"
        Me.IDTipoOper.ReadOnly = True
        Me.IDTipoOper.Visible = False
        '
        'Desayuno
        '
        Me.Desayuno.DataPropertyName = "Desayuno"
        Me.Desayuno.HeaderText = "Desayuno"
        Me.Desayuno.Name = "Desayuno"
        Me.Desayuno.ReadOnly = True
        Me.Desayuno.Visible = False
        '
        'Lonche
        '
        Me.Lonche.DataPropertyName = "Lonche"
        Me.Lonche.HeaderText = "Lonche"
        Me.Lonche.Name = "Lonche"
        Me.Lonche.ReadOnly = True
        Me.Lonche.Visible = False
        '
        'Almuerzo
        '
        Me.Almuerzo.DataPropertyName = "Almuerzo"
        Me.Almuerzo.HeaderText = "Almuerzo"
        Me.Almuerzo.Name = "Almuerzo"
        Me.Almuerzo.ReadOnly = True
        Me.Almuerzo.Visible = False
        '
        'Cena
        '
        Me.Cena.DataPropertyName = "Cena"
        Me.Cena.HeaderText = "Cena"
        Me.Cena.Name = "Cena"
        Me.Cena.ReadOnly = True
        Me.Cena.Visible = False
        '
        'Transfer
        '
        Me.Transfer.DataPropertyName = "Transfer"
        Me.Transfer.HeaderText = "Transfer"
        Me.Transfer.Name = "Transfer"
        Me.Transfer.ReadOnly = True
        Me.Transfer.Visible = False
        '
        'TipoTransporte
        '
        Me.TipoTransporte.DataPropertyName = "TipoTransporte"
        Me.TipoTransporte.HeaderText = "TipoTransporte"
        Me.TipoTransporte.Name = "TipoTransporte"
        Me.TipoTransporte.ReadOnly = True
        Me.TipoTransporte.Visible = False
        '
        'IDUbigeoOri
        '
        Me.IDUbigeoOri.DataPropertyName = "IDUbigeoOri"
        Me.IDUbigeoOri.HeaderText = "IDUbigeoOri"
        Me.IDUbigeoOri.Name = "IDUbigeoOri"
        Me.IDUbigeoOri.ReadOnly = True
        Me.IDUbigeoOri.Visible = False
        '
        'IDUbigeoDes
        '
        Me.IDUbigeoDes.DataPropertyName = "IDUbigeoDes"
        Me.IDUbigeoDes.HeaderText = "IDUbigeoDes"
        Me.IDUbigeoDes.Name = "IDUbigeoDes"
        Me.IDUbigeoDes.ReadOnly = True
        Me.IDUbigeoDes.Visible = False
        '
        'IDFormaPago
        '
        Me.IDFormaPago.DataPropertyName = "IDFormaPago"
        Me.IDFormaPago.HeaderText = "IDFormaPago"
        Me.IDFormaPago.Name = "IDFormaPago"
        Me.IDFormaPago.ReadOnly = True
        Me.IDFormaPago.Visible = False
        '
        'IDCabVarios
        '
        Me.IDCabVarios.DataPropertyName = "IDCabVarios"
        Me.IDCabVarios.HeaderText = "IDCabVarios"
        Me.IDCabVarios.Name = "IDCabVarios"
        Me.IDCabVarios.ReadOnly = True
        Me.IDCabVarios.Visible = False
        '
        'SsTipCam
        '
        Me.SsTipCam.DataPropertyName = "SsTipCam"
        Me.SsTipCam.HeaderText = "SsTipCam"
        Me.SsTipCam.Name = "SsTipCam"
        Me.SsTipCam.ReadOnly = True
        Me.SsTipCam.Visible = False
        '
        'CoMonedaPais
        '
        Me.CoMonedaPais.DataPropertyName = "CoMonedaPais"
        Me.CoMonedaPais.HeaderText = "CoMonedaPais"
        Me.CoMonedaPais.Name = "CoMonedaPais"
        Me.CoMonedaPais.ReadOnly = True
        Me.CoMonedaPais.Visible = False
        '
        'SimboloMonedaPais
        '
        Me.SimboloMonedaPais.DataPropertyName = "SimboloMonedaPais"
        Me.SimboloMonedaPais.HeaderText = "SimboloMonedaPais"
        Me.SimboloMonedaPais.Name = "SimboloMonedaPais"
        Me.SimboloMonedaPais.ReadOnly = True
        Me.SimboloMonedaPais.Visible = False
        '
        'CoPago
        '
        Me.CoPago.DataPropertyName = "CoPago"
        Me.CoPago.HeaderText = "CoPago"
        Me.CoPago.Name = "CoPago"
        Me.CoPago.ReadOnly = True
        Me.CoPago.Visible = False
        '
        'DocPdfRutaPdf
        '
        Me.DocPdfRutaPdf.DataPropertyName = "DocPdfRutaPdf"
        Me.DocPdfRutaPdf.HeaderText = "DocPdfRutaPdf"
        Me.DocPdfRutaPdf.Name = "DocPdfRutaPdf"
        Me.DocPdfRutaPdf.ReadOnly = True
        Me.DocPdfRutaPdf.Visible = False
        '
        'HoraPredeterminada
        '
        Me.HoraPredeterminada.DataPropertyName = "HoraPredeterminada"
        Me.HoraPredeterminada.HeaderText = "HoraPredeterminada"
        Me.HoraPredeterminada.Name = "HoraPredeterminada"
        Me.HoraPredeterminada.ReadOnly = True
        Me.HoraPredeterminada.Visible = False
        '
        'Web
        '
        Me.Web.DataPropertyName = "Web"
        Me.Web.HeaderText = "Web"
        Me.Web.Name = "Web"
        Me.Web.ReadOnly = True
        Me.Web.Visible = False
        '
        'Direccion
        '
        Me.Direccion.DataPropertyName = "Direccion"
        Me.Direccion.HeaderText = "Dirección"
        Me.Direccion.Name = "Direccion"
        Me.Direccion.ReadOnly = True
        Me.Direccion.Visible = False
        '
        'Telefono1
        '
        Me.Telefono1.DataPropertyName = "Telefono1"
        Me.Telefono1.HeaderText = "Telefono1"
        Me.Telefono1.Name = "Telefono1"
        Me.Telefono1.ReadOnly = True
        Me.Telefono1.Visible = False
        '
        'Telefono2
        '
        Me.Telefono2.DataPropertyName = "Telefono2"
        Me.Telefono2.HeaderText = "Telefono2"
        Me.Telefono2.Name = "Telefono2"
        Me.Telefono2.ReadOnly = True
        Me.Telefono2.Visible = False
        '
        'HoraCheckIn
        '
        Me.HoraCheckIn.DataPropertyName = "HoraCheckIn"
        Me.HoraCheckIn.HeaderText = "HoraCheckIn"
        Me.HoraCheckIn.Name = "HoraCheckIn"
        Me.HoraCheckIn.ReadOnly = True
        Me.HoraCheckIn.Visible = False
        '
        'HoraCheckOut
        '
        Me.HoraCheckOut.DataPropertyName = "HoraCheckOut"
        Me.HoraCheckOut.HeaderText = "HoraCheckOut"
        Me.HoraCheckOut.Name = "HoraCheckOut"
        Me.HoraCheckOut.ReadOnly = True
        Me.HoraCheckOut.Visible = False
        '
        'IDDesayuno
        '
        Me.IDDesayuno.DataPropertyName = "IDDesayuno"
        Me.IDDesayuno.HeaderText = "IDDesayuno"
        Me.IDDesayuno.Name = "IDDesayuno"
        Me.IDDesayuno.ReadOnly = True
        Me.IDDesayuno.Visible = False
        '
        'btnDetalle
        '
        Me.btnDetalle.HeaderText = ""
        Me.btnDetalle.Name = "btnDetalle"
        Me.btnDetalle.ReadOnly = True
        Me.btnDetalle.ToolTipText = "Nuevo Detalle"
        Me.btnDetalle.Width = 21
        '
        'btnCopiar
        '
        Me.btnCopiar.HeaderText = ""
        Me.btnCopiar.Name = "btnCopiar"
        Me.btnCopiar.ReadOnly = True
        Me.btnCopiar.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.btnCopiar.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.btnCopiar.ToolTipText = "Copiar"
        Me.btnCopiar.Width = 21
        '
        'btnCopiarDetalles
        '
        Me.btnCopiarDetalles.HeaderText = ""
        Me.btnCopiarDetalles.Name = "btnCopiarDetalles"
        Me.btnCopiarDetalles.ReadOnly = True
        Me.btnCopiarDetalles.ToolTipText = "Copiar Detalles"
        Me.btnCopiarDetalles.Width = 21
        '
        'btnEdit
        '
        Me.btnEdit.HeaderText = ""
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.ReadOnly = True
        Me.btnEdit.ToolTipText = "Editar"
        Me.btnEdit.Width = 21
        '
        'btnDel
        '
        Me.btnDel.HeaderText = ""
        Me.btnDel.Name = "btnDel"
        Me.btnDel.ReadOnly = True
        Me.btnDel.ToolTipText = "Eliminar"
        Me.btnDel.Width = 21
        '
        'GroupBox2
        '
        Me.GroupBox2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox2.Location = New System.Drawing.Point(9, 69)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(1250, 8)
        Me.GroupBox2.TabIndex = 100
        Me.GroupBox2.TabStop = False
        '
        'chkServCoticen
        '
        Me.chkServCoticen.AutoSize = True
        Me.chkServCoticen.Checked = True
        Me.chkServCoticen.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkServCoticen.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkServCoticen.Location = New System.Drawing.Point(824, 19)
        Me.chkServCoticen.Name = "chkServCoticen"
        Me.chkServCoticen.Size = New System.Drawing.Size(140, 17)
        Me.chkServCoticen.TabIndex = 4
        Me.chkServCoticen.Text = "Servicios que se coticen"
        Me.chkServCoticen.UseVisualStyleBackColor = True
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(302, 46)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(48, 13)
        Me.Label3.TabIndex = 98
        Me.Label3.Text = "Servicio:"
        '
        'txtDescServicio
        '
        Me.txtDescServicio.Location = New System.Drawing.Point(361, 42)
        Me.txtDescServicio.MaxLength = 100
        Me.txtDescServicio.Name = "txtDescServicio"
        Me.txtDescServicio.Size = New System.Drawing.Size(206, 20)
        Me.txtDescServicio.TabIndex = 6
        '
        'btnBuscar
        '
        Me.btnBuscar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnBuscar.Image = Global.SETours.My.Resources.Resources.find
        Me.btnBuscar.Location = New System.Drawing.Point(1227, 39)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(32, 26)
        Me.btnBuscar.TabIndex = 9
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'txtRazonSocial
        '
        Me.txtRazonSocial.Location = New System.Drawing.Point(79, 42)
        Me.txtRazonSocial.MaxLength = 60
        Me.txtRazonSocial.Name = "txtRazonSocial"
        Me.txtRazonSocial.Size = New System.Drawing.Size(206, 20)
        Me.txtRazonSocial.TabIndex = 5
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(12, 45)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(61, 13)
        Me.Label1.TabIndex = 94
        Me.Label1.Text = "Proveedor:"
        '
        'toolBar
        '
        Me.toolBar.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnEditar, Me.btnDeshacer, Me.btnGrabar, Me.btnEliminar, Me.btnImprimir, Me.btnSalir})
        Me.toolBar.Location = New System.Drawing.Point(0, 0)
        Me.toolBar.Name = "toolBar"
        Me.toolBar.Size = New System.Drawing.Size(1258, 25)
        Me.toolBar.TabIndex = 6
        Me.toolBar.Text = "ToolStrip1"
        Me.toolBar.Visible = False
        '
        'btnNuevo
        '
        Me.btnNuevo.Enabled = False
        Me.btnNuevo.Image = Global.SETours.My.Resources.Resources.page
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(58, 22)
        Me.btnNuevo.Text = "Nuevo"
        Me.btnNuevo.ToolTipText = "Nuevo (F2)"
        '
        'btnEditar
        '
        Me.btnEditar.Enabled = False
        Me.btnEditar.Image = Global.SETours.My.Resources.Resources.editarpng
        Me.btnEditar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEditar.Name = "btnEditar"
        Me.btnEditar.Size = New System.Drawing.Size(55, 22)
        Me.btnEditar.Text = "Editar"
        Me.btnEditar.ToolTipText = "Editar (F3)"
        '
        'btnDeshacer
        '
        Me.btnDeshacer.Enabled = False
        Me.btnDeshacer.Image = Global.SETours.My.Resources.Resources.arrow_undo
        Me.btnDeshacer.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnDeshacer.Name = "btnDeshacer"
        Me.btnDeshacer.Size = New System.Drawing.Size(72, 22)
        Me.btnDeshacer.Text = "Deshacer"
        Me.btnDeshacer.ToolTipText = "Deshacer (ESC)"
        '
        'btnGrabar
        '
        Me.btnGrabar.Enabled = False
        Me.btnGrabar.Image = Global.SETours.My.Resources.Resources.disk
        Me.btnGrabar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGrabar.Name = "btnGrabar"
        Me.btnGrabar.Size = New System.Drawing.Size(60, 22)
        Me.btnGrabar.Text = "Grabar"
        Me.btnGrabar.ToolTipText = "Grabar (F6)"
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Image = Global.SETours.My.Resources.Resources.eliminarpng
        Me.btnEliminar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(63, 22)
        Me.btnEliminar.Text = "Eliminar"
        Me.btnEliminar.ToolTipText = "Eliminar (F4)"
        '
        'btnImprimir
        '
        Me.btnImprimir.Enabled = False
        Me.btnImprimir.Image = Global.SETours.My.Resources.Resources.printer
        Me.btnImprimir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(65, 22)
        Me.btnImprimir.Text = "Imprimir"
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(47, 22)
        Me.btnSalir.Text = "Salir"
        '
        'dtpFechaInsertarCoti
        '
        Me.dtpFechaInsertarCoti.Format = System.Windows.Forms.DateTimePickerFormat.[Short]
        Me.dtpFechaInsertarCoti.Location = New System.Drawing.Point(973, 36)
        Me.dtpFechaInsertarCoti.Name = "dtpFechaInsertarCoti"
        Me.dtpFechaInsertarCoti.Size = New System.Drawing.Size(91, 20)
        Me.dtpFechaInsertarCoti.TabIndex = 216
        Me.dtpFechaInsertarCoti.Visible = False
        '
        'lblEtiqDiaServicio
        '
        Me.lblEtiqDiaServicio.AutoSize = True
        Me.lblEtiqDiaServicio.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEtiqDiaServicio.Location = New System.Drawing.Point(970, 20)
        Me.lblEtiqDiaServicio.Name = "lblEtiqDiaServicio"
        Me.lblEtiqDiaServicio.Size = New System.Drawing.Size(66, 13)
        Me.lblEtiqDiaServicio.TabIndex = 217
        Me.lblEtiqDiaServicio.Text = "Día Servicio:"
        Me.lblEtiqDiaServicio.Visible = False
        '
        'frmMantServiciosProveedor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1271, 575)
        Me.Controls.Add(Me.toolBar)
        Me.Controls.Add(Me.grb)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmMantServiciosProveedor"
        Me.Tag = "Servicio"
        Me.Text = "Mantenimiento de Servicios de Proveedor"
        Me.grb.ResumeLayout(False)
        Me.grb.PerformLayout()
        Me.grbControlCalidad.ResumeLayout(False)
        Me.grbControlCalidad.PerformLayout()
        CType(Me.nudCategoria, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvDet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.toolBar.ResumeLayout(False)
        Me.toolBar.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents grb As System.Windows.Forms.GroupBox
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtDescServicio As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscar As System.Windows.Forms.Button
    Friend WithEvents txtRazonSocial As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents chkServCoticen As System.Windows.Forms.CheckBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents dgv As System.Windows.Forms.DataGridView
    Friend WithEvents dgvDet As System.Windows.Forms.DataGridView
    Friend WithEvents lblDet As System.Windows.Forms.Label
    Friend WithEvents lblLectura As System.Windows.Forms.Label
    Friend WithEvents lblAnioEtiq As System.Windows.Forms.Label
    Friend WithEvents cboAnio As System.Windows.Forms.ComboBox
    Friend WithEvents cboTipo As System.Windows.Forms.ComboBox
    Friend WithEvents Label22 As System.Windows.Forms.Label
    Friend WithEvents cboCiudad As System.Windows.Forms.ComboBox
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents cboPais As System.Windows.Forms.ComboBox
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents lblCategoriaSEToursEtiq As System.Windows.Forms.Label
    Friend WithEvents cboCategSeTours As System.Windows.Forms.ComboBox
    Friend WithEvents lblCategoriaEtiq As System.Windows.Forms.Label
    Friend WithEvents nudCategoria As System.Windows.Forms.NumericUpDown
    Friend WithEvents toolBar As System.Windows.Forms.ToolStrip
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEditar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnDeshacer As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGrabar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnEliminar As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnImprimir As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnSalir As System.Windows.Forms.ToolStripButton
    Friend WithEvents grbControlCalidad As System.Windows.Forms.GroupBox
    Friend WithEvents btnRepControl As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblFiles As System.Windows.Forms.Label
    Friend WithEvents lblRoomnights As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents lblRoomRes As System.Windows.Forms.Label
    Friend WithEvents lblPoor As System.Windows.Forms.Label
    Friend WithEvents lblAverage As System.Windows.Forms.Label
    Friend WithEvents lblGood As System.Windows.Forms.Label
    Friend WithEvents lblVeryGood As System.Windows.Forms.Label
    Friend WithEvents lblExcellent As System.Windows.Forms.Label
    Private WithEvents colorSlider5 As MB.Controls.ColorSlider
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents lblPercent As System.Windows.Forms.Label
    Friend WithEvents Anio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescripcionDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Tipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DetaTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VerDetaTipo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ConAlojamiento As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto_sgl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto_dbl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Monto_tri As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VieneAnio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VieneIDServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VieneServicio As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents VieneIDProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents VieneProveedor As System.Windows.Forms.DataGridViewLinkColumn
    Friend WithEvents VieneVariante As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio_Det As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DesayunoDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LoncheDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AlmuerzoDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CenaDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio_Det_V As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Codigo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PlanAlimenticio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoTriple As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents AfectoIgv As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoMonedaDet As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idTipoOC As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio_V As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnEditDet As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnDelDet As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnAnular As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnUpdRelacRetro As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnVerPolitica As System.Windows.Forms.Button
    Friend WithEvents Descripcion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDTipoServ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RazonSocial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescProvInternac As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDServicio As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDCiudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescCiudad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lectura As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDTipoProv As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DescTipoServ As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDProveedor As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CorreoReservas As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CorreoReservas2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPais As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDTipoOper As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Desayuno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Lonche As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Almuerzo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Cena As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Transfer As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TipoTransporte As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDUbigeoOri As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDUbigeoDes As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDFormaPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDCabVarios As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SsTipCam As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoMonedaPais As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents SimboloMonedaPais As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CoPago As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DocPdfRutaPdf As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HoraPredeterminada As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Web As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Direccion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Telefono1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Telefono2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HoraCheckIn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents HoraCheckOut As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDDesayuno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnDetalle As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnCopiar As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnCopiarDetalles As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnEdit As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents btnDel As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents lblEtiqDiaServicio As System.Windows.Forms.Label
    Friend WithEvents dtpFechaInsertarCoti As System.Windows.Forms.DateTimePicker
End Class
