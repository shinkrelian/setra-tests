﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngCotizacionObs
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.gboxGeneral = New System.Windows.Forms.GroupBox()
        Me.txtObservaciones = New System.Windows.Forms.TextBox()
        Me.lblObservaciones = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtIDCotizacion = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.gboxGeneral.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(5, 36)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 264
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.accept
        Me.btnAceptar.Location = New System.Drawing.Point(5, 5)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 263
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'gboxGeneral
        '
        Me.gboxGeneral.BackColor = System.Drawing.Color.White
        Me.gboxGeneral.Controls.Add(Me.txtObservaciones)
        Me.gboxGeneral.Controls.Add(Me.lblObservaciones)
        Me.gboxGeneral.Controls.Add(Me.GroupBox2)
        Me.gboxGeneral.Controls.Add(Me.txtIDCotizacion)
        Me.gboxGeneral.Controls.Add(Me.Label1)
        Me.gboxGeneral.Dock = System.Windows.Forms.DockStyle.Right
        Me.gboxGeneral.Location = New System.Drawing.Point(38, 0)
        Me.gboxGeneral.Name = "gboxGeneral"
        Me.gboxGeneral.Size = New System.Drawing.Size(375, 287)
        Me.gboxGeneral.TabIndex = 265
        Me.gboxGeneral.TabStop = False
        '
        'txtObservaciones
        '
        Me.txtObservaciones.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.txtObservaciones.BackColor = System.Drawing.SystemColors.Window
        Me.txtObservaciones.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservaciones.Location = New System.Drawing.Point(18, 79)
        Me.txtObservaciones.Multiline = True
        Me.txtObservaciones.Name = "txtObservaciones"
        Me.txtObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.txtObservaciones.Size = New System.Drawing.Size(345, 196)
        Me.txtObservaciones.TabIndex = 350
        Me.txtObservaciones.Tag = "Borrar"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObservaciones.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.lblObservaciones.Location = New System.Drawing.Point(15, 63)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(150, 13)
        Me.lblObservaciones.TabIndex = 349
        Me.lblObservaciones.Text = "Observaciones Generales"
        '
        'GroupBox2
        '
        Me.GroupBox2.Location = New System.Drawing.Point(18, 42)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(346, 8)
        Me.GroupBox2.TabIndex = 347
        Me.GroupBox2.TabStop = False
        '
        'txtIDCotizacion
        '
        Me.txtIDCotizacion.BackColor = System.Drawing.Color.LightGray
        Me.txtIDCotizacion.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtIDCotizacion.Location = New System.Drawing.Point(114, 19)
        Me.txtIDCotizacion.MaxLength = 6
        Me.txtIDCotizacion.Name = "txtIDCotizacion"
        Me.txtIDCotizacion.ReadOnly = True
        Me.txtIDCotizacion.Size = New System.Drawing.Size(88, 21)
        Me.txtIDCotizacion.TabIndex = 344
        Me.txtIDCotizacion.TextAlign = System.Windows.Forms.HorizontalAlignment.Right
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label1.Location = New System.Drawing.Point(15, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(93, 13)
        Me.Label1.TabIndex = 343
        Me.Label1.Text = "Nro. Cotización:"
        '
        'frmIngCotizacionObs
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(413, 287)
        Me.Controls.Add(Me.gboxGeneral)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmIngCotizacionObs"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Observaciones"
        Me.gboxGeneral.ResumeLayout(False)
        Me.gboxGeneral.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents gboxGeneral As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtIDCotizacion As System.Windows.Forms.TextBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents txtObservaciones As System.Windows.Forms.TextBox
End Class
