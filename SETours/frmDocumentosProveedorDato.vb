﻿Imports ComSEToursBL2
Imports ComSEToursBE2

Public Class frmDocumentosProveedorDato
    Public frmRef As Object 'frmDocumentosProveedor
    Public frmRefE As Object ' frmDocumentosProveedorEdicion
    Public intNuDocumProv As Integer = 0
    Public strCoTipoServicio As String = ""
    Public intNuItem As Integer = 0
    Public intNuDocumProvIndex As Integer = 0
    Public intIDCab As Integer = 0
    Public intIDCab_Inicial As Integer = 0
    Public intIDOperacion As Integer = 0
    Public intIDPax As Integer = 0
    Public strIDCliente As String = ""
    Public strIDProv_LA As String = ""
    Public blnActualizoDocumentoBSP As Boolean = False
    Public blnActualizoItinerario As Boolean = False
    Public blnActualizoTax As Boolean = False
    Public blnActualizoTarifa As Boolean = False
    Dim bln_mMigrado As Boolean = False
    Dim blnCargaIniStruct As Boolean
    Dim datFechaCreacion As Date

    Dim blnNuevoEnabled As Boolean
    Dim blnGrabarEnabled As Boolean
    Dim blnDeshacerEnabled As Boolean
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnImprimirEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean = False

    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False

    Dim strModoEdicion As String
    Dim blnCambios As Boolean = False
    Dim blnLoad As Boolean = False
    Public chrModulo As String = ""

    Public intNuVoucher As Integer = 0

    Public vstrNuDocum As String = ""
    Public intNuOrden_Servicio As Integer = 0
    Public vstrVoucher As String = ""
    Public intNuOrdenCompra As Integer = 0
    Public intNuPreSob As Integer = 0
    Public intNuVouPTrenInt As Integer = 0

    Public strTipOpe As String = ""
    Dim strTipoDoc As String = ""
    Public strTipoVoucheruOrServ As String = ""
    Public strMonedaFEgreso As String = ""

    Public strIDTipoOC As String = ""
    Public strCoCeCos As String = ""
    Public strCtaContable As String = ""

    Public vstrIDProveedor As String = ""
    Dim dblTipoCambioProveedor As Double = 0
    Public strCoProveedorAdminist As String = ""
    'Public strCoProveedorTren As String = ""

    Dim blnEsProvAdmin As Boolean = False
    Public strCoUbigeo_Oficina As String = ""
    Public blnDocActivo As Boolean = True
    Dim blnDetraccionError As Boolean = False
    Dim blnDetraccionErrorMoneda As Boolean = False

    Public strCoCeCos_Defecto As String = "900101"

    Dim strFormaPago As String = ""
    Dim dtFeVencimiento As Date = Now

    Public strCoSAP As String = ""
    Public intIDCab_FondoFijo As Integer = 0

    Dim dblIGV_Multiple As Double = 0
    Dim dblTipoCambio_Multiple As Double = 0
    Dim blnTipoCambio_TXT As Boolean = False

    Dim dblIVA_Arg As Double = 21
    Dim dblRHLimiteSinRetencion_Arg As Double = 5000

    Dim dblIVA_Chile As Double = 19
    Dim dblRHLimiteSinRetencion_Chile As Double = 0

    Public blnSoloLectura As Boolean = False

    Structure stDocumento_Detalle
        Dim NuDocumProvDet As String
        Dim IDServicio_Det As String
        Dim TxServicio As String
        Dim QtCantidad As String
        Dim SsTotalOriginal As String
        Dim strAccion As String
        Dim IDCab As Integer

        Dim CoProducto As String
        Dim CoAlmacen As String
        Dim SsIGV As Decimal
        Dim CoTipoOC As String
        Dim CoCtaContab As String
        Dim CoCeCos As String
        Dim CoCeCon As String
        Dim SSPrecUni As Decimal
        Dim SsMonto As Decimal
        Dim SSTotal As Decimal

    End Structure
    Dim objLstDocumento_Detalle As New List(Of stDocumento_Detalle)

    Dim blnMNME As Boolean = False
    'Dim sglIgv As Single = 0

    'Dim objLstNotaVentaCounter_DetalleItem As New List(Of stNotaVentaCounter_DetalleItem)

#Region "Metodos del formulario"

    Private Sub frmIngTicketDato_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'frmMain.btnEditar.Enabled = blnEditarEnabled
        'AddHandler frmMain.btnDeshacer.Click, AddressOf pDeshacer
        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        'AddHandler frmMain.btnGrabar.Click, AddressOf pGrabar
        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'AddHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        'frmMain.btnSalir.Enabled = blnSalirEnabled
        'pCambiarTitulosToolBar(Me)
        blnLoad = True
    End Sub

    Private Sub frmIngTicketDato_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnDeshacer.Click, AddressOf pDeshacer
        'RemoveHandler frmMain.btnGrabar.Click, AddressOf pGrabar
        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir
        'RemoveHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        'pCambiarTitulosToolBar(Me, True)
    End Sub

    Private Sub frmIngTicketDato_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        ' frmMain.DesabledBtns()
    End Sub

    Private Sub frmIngTicketDato_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin grabar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False
                'If Not frmRefE Is Nothing Then
                '    frmRefE.ListarDetalleVoucher()
                'End If
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub frmIngTicketDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If blnGrabarEnabled Then pGrabar(Nothing, Nothing)
            Case Keys.F9
                If blnImprimirEnabled Then pImprimir(Nothing, Nothing)
            Case Keys.F2
                'btnNuevo_Click(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmIngTicketDato_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            strTipoVoucheruOrServ = "OTR"
            strCoUbigeo_Oficina = gstrLima
            CargarSeguridad()
            ActivarDesactivarBotones()
            'CargarIgv()

            intIDCab_Inicial = intIDCab
            ''dgvDetalle.ColumnHeadersDefaultCellStyle.Font = New Font(dgvDetalle.Font, FontStyle.Bold)
            ''dgvDetalle.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            ''dgvDetalle.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
            'If Not frmRefE Is Nothing Then
            '    RemoveHandler frmMain.btnDeshacer.Click, AddressOf frmRefE.pDeshacer
            'End If

            If strTipoVoucheruOrServ = "OCP" Then

                blnEsProvAdmin = True

            End If

            'If strTipoVoucheruOrServ = "OTR" Then
            If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP" Then
                lblEtiqFile.Visible = True
                lblIDFile.Visible = True
                btnConsultarFile.Visible = True
                btnLimpiarFile.Visible = True
            Else
                lblEtiqFile.Visible = False
                lblIDFile.Visible = False
                btnConsultarFile.Visible = False
                btnLimpiarFile.Visible = False
            End If

            CargarCombos()
            ConfigurarxUbigeo()
            If intNuDocumProv = 0 Then
                ModoNuevo()
                Configurar_Origen_Otros()
            Else
                strModoEdicion = "E"
                CargarControles()
                ModoEditar()
            End If
            ColorearControlesObligatorios()
            CargarMonedaFEgreso()

            If strTipoVoucheruOrServ = "VOU" Then
                lblNroObligPago.Text = "Nro. Voucher:"
            ElseIf strTipoVoucheruOrServ = "OSV" Then
                lblNroObligPago.Text = "Nro. Orden Serv.:"
            ElseIf strTipoVoucheruOrServ = "OPA" Then
                lblNroObligPago.Text = "Nro. Orden Pago:"
            ElseIf strTipoVoucheruOrServ = "PST" Or strTipoVoucheruOrServ = "PTT" Then
                lblNroObligPago.Text = "Nro. Presupuesto:"
                btnSelProveedor.Enabled = True
            ElseIf strTipoVoucheruOrServ = "ZIC" Then
                lblNroObligPago.Text = "Nro. Doc. Egreso:"
            ElseIf strTipoVoucheruOrServ = "OCP" Then
                lblNroObligPago.Text = "Nro. Orden Compra:"
                If Not frmRefE Is Nothing Then
                    lblProveedor.Visible = False
                    lblEtiqProveedor.Visible = False
                    btnSelProveedor.Visible = False
                Else
                    lblProveedor.Visible = True
                    lblEtiqProveedor.Visible = True
                    btnSelProveedor.Visible = True
                End If

                blnEsProvAdmin = True
                If strModoEdicion = "N" Then
                    btnSelProveedor.Enabled = True
                End If
            ElseIf strTipoVoucheruOrServ = "OTR" Then
                lblProveedor.Visible = True
                lblEtiqProveedor.Visible = True
                btnSelProveedor.Visible = True
                lblProveedor.Enabled = True
                lblEtiqProveedor.Enabled = True
                btnSelProveedor.Enabled = True
            End If

            If intNuDocumProv <> 0 And (strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "PTT") Then
                BloquearControles_Edicion()
            End If
            If strCoSAP <> "" Then
                BloquearControles_Edicion()
            End If

            If blnSoloLectura Then
                BloquearControles_Edicion()
            End If

            If strCtaContable <> "" Then
                If cboCuentaContable.Text = "" Then
                    cboCuentaContable.SelectedValue = strCtaContable
                End If
            End If




        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarDetalleDocumento_Texto()
        Try
            Dim objBLDet As New clsDocumentoProveedor_DetBN
            Dim dtDetalles As DataTable = objBLDet.ConsultarList(intNuDocumProv, 0)
            dgvDetalle.Rows.Clear()
            For Each drDet As DataRow In dtDetalles.Rows
                dgvDetalle.Rows.Add()
                For i As Byte = 0 To dtDetalles.Columns.Count - 1
                    If dgvDetalle.Columns(i).Name = "SsTotal" Then
                        dgvDetalle.Item(i, dgvDetalle.Rows.Count - 1).Value = Format(CDbl(drDet(i)), "###,##0.00")
                    Else
                        dgvDetalle.Item(i, dgvDetalle.Rows.Count - 1).Value = drDet(i)
                    End If
                Next
            Next

            If dtDetalles.Rows.Count = 0 Then
                'CargarDetallePorDefecto()
            Else
                blnCargaIniStruct = True
                For Each dgvItem As DataGridViewRow In dgvDetalle.Rows
                    AgregarEditarEliminarItinerario(dgvItem.Index, "")
                Next
                blnCargaIniStruct = False
            End If

            'PintarTotales()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub BloquearControles_Edicion()
        Try
            For Each c As Control In Me.Controls
                c.Enabled = False
            Next

            blnGrabarEnabled = False
            ActivarDesactivarBotones()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarTipoCambio_Proveedor()
        Try
            If vstrIDProveedor.Trim.Length > 0 Then
                Using objBL As New clsDocumentoProveedorBN
                    Using dr As SqlClient.SqlDataReader = objBL.ConsultarDatosProveedor(vstrIDProveedor)
                        dr.Read()
                        If dr.HasRows Then
                            dblTipoCambioProveedor = IIf(IsDBNull(dr("SSTipoCambio")), "0", dr("SSTipoCambio").ToString.Trim)
                        End If
                        dr.Close()
                    End Using
                End Using

                If dblTipoCambioProveedor > 0 Then 'si tiene tipo de cambio, se le asigna al textbox
                    If strModoEdicion = "N" Then
                        'txtTipoCambio.Text = Format(dblTipoCambioProveedor, "###,##0.00")
                        Text = Format(dblTipoCambioProveedor, "###,##0.00")

                    End If
                    'txtTipoCambio.Enabled = False
                    txtTipoCambioFEgreso.Enabled = False
                End If

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarMonedaFEgreso()
        If strMonedaFEgreso.Trim = "" Then Exit Sub
        Try
            If strMonedaFEgreso <> cboMoneda.SelectedValue.ToString Then
                grbTipoCambioFormaEgreso.Visible = True
            Else
                grbTipoCambioFormaEgreso.Visible = False
            End If
            If Not blnEsMultiple() Then
                pCalcularTipoCambio()
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ColorearControlesObligatorios()
        Try

            txtCoIATA.BackColor = gColorTextBoxObligat

            txtCoLAServicio.BackColor = gColorTextBoxObligat
            txtCodLARecord.BackColor = gColorTextBoxObligat
            txtCodLAEmision.BackColor = gColorTextBoxObligat
            txtCodTransEmision.BackColor = gColorTextBoxObligat
            txtCoPtoVentaEmision.BackColor = gColorTextBoxObligat

            txtBoletoNro.BackColor = gColorTextBoxObligat

            txtMonto.BackColor = gColorTextBoxObligat

            dtpFechaEmision.BackColor = gColorTextBoxObligat
            cboTipoDocumento.BackColor = gColorTextBoxObligat
            txtNuDocumento.BackColor = gColorTextBoxObligat
            txtSerie.BackColor = gColorTextBoxObligat

            dtpFechaEmision.BackColor = gColorTextBoxObligat
            dtpFechaRecepcion.BackColor = gColorTextBoxObligat
            cboMoneda.BackColor = gColorTextBoxObligat
            'cboMoneda_Pago.BackColor = gColorTextBoxObligat
            txtSsNeto.BackColor = gColorTextBoxObligat
            txtMonto.BackColor = gColorTextBoxObligat
            'cboEstado.BackColor = gColorTextBoxObligat

            cboTipoOperacion.BackColor = gColorTextBoxObligat
            cboCuentaContable.BackColor = gColorTextBoxObligat
            cboMoneda.BackColor = gColorTextBoxObligat

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub CargarSeguridad()
        Try
            'Dim strNombreForm As String
            'strNombreForm = Replace(Me.Name, "frm", "mnu")
            'strNombreForm = Replace(strNombreForm, "Dato", "")
            Dim strNombreForm As String = "" '= "mnuGenPreLiquidacion2"
            If strCoUbigeo_Oficina = gstrLima Then
                strNombreForm = "mnuDocumentosProveedorSetLima"
            ElseIf strCoUbigeo_Oficina = gstrCusco Then
                strNombreForm = "mnuDocumentosProveedorSetCusco"
            ElseIf strCoUbigeo_Oficina = gstrBuenosAires Then
                strNombreForm = "mnuDocumentosProveedorSetBuenosAires"
            ElseIf strCoUbigeo_Oficina = gstrSantiago Then
                strNombreForm = "mnuDocumentosProveedorSetSantiago"
            End If
            'Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
            '    Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
            '        If dr.HasRows Then
            '            dr.Read()
            '            blnGrabarEnabledBD = dr("Grabar")
            '            blnEditarEnabledBD = dr("Actualizar")
            '            blnEliminarEnabledBD = dr("Eliminar")
            '        End If
            '        dr.Close()
            '    End Using
            'End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Sub ActivarDesactivarBotones()

        ''frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'If blnNuevoEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnNuevo.Enabled = blnNuevoEnabled
        '    Else
        '        frmMain.btnNuevo.Enabled = False
        '        blnNuevoEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'End If

        ''frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'If blnGrabarEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnGrabar.Enabled = blnGrabarEnabled
        '    Else
        '        frmMain.btnGrabar.Enabled = False
        '        blnGrabarEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'End If

        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled

        ''frmMain.btnEditar.Enabled = blnEditarEnabled
        'If blnEditarEnabled = True Then
        '    If blnEditarEnabledBD Then
        '        frmMain.btnEditar.Enabled = blnEditarEnabled
        '    Else
        '        frmMain.btnEditar.Enabled = False
        '        blnEditarEnabled = blnEditarEnabledBD
        '    End If
        'Else
        '    frmMain.btnEditar.Enabled = blnEditarEnabled
        'End If

        ''frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'If blnEliminarEnabled = True Then
        '    If blnEliminarEnabledBD Then
        '        frmMain.btnEliminar.Enabled = blnEliminarEnabled
        '    Else
        '        frmMain.btnEliminar.Enabled = False
        '        blnEliminarEnabled = blnEliminarEnabledBD
        '    End If
        'Else
        '    frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'End If

        'frmMain.btnSalir.Enabled = blnSalirEnabled

    End Sub

#End Region


    Private Sub pGrabar(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Not blnValidarIngresos() Then Exit Sub
        Try
            For Each dgvItem As DataGridViewRow In dgvDetalle.Rows
                AgregarEditarEliminarItinerario(dgvItem.Index, "")
            Next

            If strModoEdicion = "N" Then

                If cboTipoSAP.SelectedValue = "2" Then 'multiple
                    Grabar_Multiple()
                Else
                    Grabar()

                End If


                If txtNuDocumento.Text.Trim = "" Then
                    MessageBox.Show("Se creó el documento exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    MessageBox.Show("Se creó el Documento N° " & txtNuDocumento.Text.Trim & " exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                End If

            ElseIf strModoEdicion = "E" Then
                Actualizar()
                MessageBox.Show("Se actualizó el Documento exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            End If
            'If frmRef IsNot Nothing Then
            '    frmRef.Buscar()
            'End If

            If frmRefE IsNot Nothing Then
                'frmRefE.ListarDetalleVoucher()
                frmRefE.blnCambiosDato = True
                If strTipoVoucheruOrServ = "PST" Or strTipoVoucheruOrServ = "PTT" Then
                    frmRefE.CargarControles_Pres(True)
                Else
                    frmRefE.CargarControles()
                    If frmRefE.txtSaldoPendiente.Text = "0.00" Then
                        frmRefE.cboEstado.SelectedValue = "AC"
                        frmRefE.cboEstado2.SelectedValue = "AC"
                        frmRefE.cboEstado_Pres.SelectedValue = "AC"
                    End If
                End If

            End If

            If frmRef IsNot Nothing Then
                'frmRefE.ListarDetalleVoucher()
                If strTipoVoucheruOrServ = "OTR" Then
                    'frmRef.strCoProveedor_Busqueda = vstrIDProveedor
                    frmRef.cboTipoFormaEgreso.SelectedValue = "OTR"
                    frmRef.Buscar()

                End If

            End If

            blnCambios = False
            'frmRefE.blnCambiosDato = True
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pDeshacer(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'frmRefE.blnCambiosDato = False
        Me.Close()
    End Sub

    Private Sub pImprimir(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Sub CargarCombos()
        Try


            Dim strNombreForm As String = ""
            If chrModulo = "C" Or chrModulo = "D" Or chrModulo = "TI" Or chrModulo = "DO" Then
                strNombreForm = "mnuIngCotizacion"
            ElseIf chrModulo = "R" Then
                strNombreForm = "mnuIngReservasControlporFile"
            ElseIf chrModulo = "O" Then
                strNombreForm = "mnuIngOperacionesControlporFile"
            End If


            'Dim dttEstados As New DataTable
            'With dttEstados
            '    .Columns.Add("Id")
            '    .Columns.Add("Descripcion")
            '    .Rows.Add("RE", "RECEPCIONADO")
            '    .Rows.Add("PG", "PAGADO")
            '    .Rows.Add("AN", "ANULADO")
            'End With
            'pCargaCombosBox(cboEstado, dttEstados, True)

            Dim dttTipos As New DataTable
            With dttTipos
                .Columns.Add("Id")
                .Columns.Add("Descripcion")
                'If strCoUbigeo_Oficina <> gstrBuenosAires Then
                '    .Rows.Add("0", "PRODUCTOS")
                'End If

                If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                    .Rows.Add("0", "PRODUCTOS")
                End If

                .Rows.Add("1", "SERVICIOS")

                If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "PTT" Then
                    .Rows.Add("2", "MULTIPLE")
                End If


            End With
            pCargaCombosBox(cboTipoSAP, dttTipos, True)


            Dim objBL As New clsTablasApoyoBN.clsTipoDocBN
            Dim dttTipoDocs As DataTable = objBL.ConsultarCboProv()


            If strCoUbigeo_Oficina = gstrSantiago Then
                pCargaCombosBox(cboTipoDocumento, dtFiltrarDataTable(dttTipoDocs, "IDtipodoc in ('FAC','BOL','RH ','IFE','NCR','NDB')"), True)
            Else
                pCargaCombosBox(cboTipoDocumento, dttTipoDocs)
            End If

            Dim objBL1 As New clsTablasApoyoBN.clsTipoDocBN
            Dim dttTipoDocs1 As DataTable = objBL1.ConsultarCboProv()
            pCargaCombosBox(cboTipoDocumento_DocumVinc, dttTipoDocs1)

            Dim objDetBL As New clsTipoDetraccionBN
            Dim dttDetracciones As DataTable = objDetBL.ConsultarCboconCodigo("", True, False)
            'pCargaCombosBox(cboTipoDetraccion, dttDetracciones, False)
            pCargaCombosBox(cboTipoDetraccion, dttDetracciones, True)


            Dim objMon As New clsTablasApoyoBN.clsMonedaBN
            'Dim dtMonedas As DataTable = objMon.ConsultarCbo()
            'dtMonedas = dtFiltrarDataTable(dtMonedas, "IDMoneda in ('ARS','USD')")
            If strCoUbigeo_Oficina = gstrBuenosAires Then
                pCargaCombosBox(cboMoneda, dtFiltrarDataTable(objMon.ConsultarCbo(), "IDMoneda in ('ARS','USD')"), True)
            ElseIf strCoUbigeo_Oficina = gstrSantiago Then
                pCargaCombosBox(cboMoneda, dtFiltrarDataTable(objMon.ConsultarCbo(), "IDMoneda in ('CLP','USD')"), True)
            Else
                pCargaCombosBox(cboMoneda, objMon.ConsultarCbo(), True)
            End If


            'Dim objMon2 As New clsTablasApoyoBN.clsMonedaBN
            'pCargaCombosBox(cboMoneda_Pago, objMon2.ConsultarCbo(), True)

            Dim objDocBN As New clsDocumentoProveedorBN
            pCargaCombosBox(cboTipoOperacion, objDocBN.ConsultarTipoOperacionCont)

            Dim objCenCosBN As New clsCentroCostosBN
            pCargaCombosBox(cboCentroCostos, objCenCosBN.ConsultarListCbo)

            Dim objCenConBN As New clsCentroControlBN
            pCargaCombosBox(cboCentroControl, objCenConBN.ConsultarListCbo)

            If strIDTipoOC <> "" Then
                cboTipoOperacion.SelectedValue = strIDTipoOC
            End If


            Dim dttFormaEgreso As New DataTable("FormaEgreso")
            With dttFormaEgreso
                .Columns.Add("Id")
                .Columns.Add("Descripcion")
                .Rows.Add("VOU", "VOUCHER")
                .Rows.Add("OPA", "ORDEN DE PAGO")
                .Rows.Add("OSV", "ORDEN DE SERVICIO")

                If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                    .Rows.Add("PTT", "PRESUPUESTO TKT")
                    .Rows.Add("ZIC", "ZICASSO")
                End If

            End With

            pCargaCombosBox(cboFormaEgreso, dttFormaEgreso, True)

            Dim objMon2 As New clsTablasApoyoBN.clsMonedaBN
            'pCargaCombosBox(cboMoneda_Multiple, objMon2.ConsultarCbo(), True)
            'pCargaCombosBox(cboMoneda_Multiple, objMon2.ConsultarCbo_Multiple(, True), True)
            If strCoUbigeo_Oficina = gstrBuenosAires Then
                pCargaCombosBox(cboMoneda_Multiple, dtFiltrarDataTable(objMon2.ConsultarCbo_Multiple(, True), "IDMoneda in ('ARS','USD')"), True)
            ElseIf strCoUbigeo_Oficina = gstrSantiago Then
                pCargaCombosBox(cboMoneda_Multiple, dtFiltrarDataTable(objMon2.ConsultarCbo_Multiple(, True), "IDMoneda in ('CLP','USD')"), True)
            Else
                pCargaCombosBox(cboMoneda_Multiple, objMon2.ConsultarCbo_Multiple(, True), True)
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    'Private Sub CargarCombo_TipoOperacion()
    '    Try
    '        Dim strTipoDoc As String = ""

    '        If cboTipoDocumento.Items.Count > 0 Then
    '            'capturar valor de tipo de operacion
    '            If Not cboTipoDocumento.SelectedValue Is Nothing Then
    '                strTipoDoc = cboTipoDocumento.SelectedValue.ToString

    '            End If

    '        End If
    '        'cargar combo
    '        Dim objBN As New clsTablasApoyoBN.clsTipoOperacContabBN
    '        pCargaCombosBox(cboTipoOperacion, objBN.ConsultarCbo_xTipoDoc(strTipoDoc), False)
    '        If cboTipoOperacion.Items.Count = 1 Then
    '            cboTipoOperacion.SelectedIndex = 0
    '        End If

    '    Catch ex As Exception
    '        Throw
    '    End Try
    'End Sub

    Private Sub Cuentas_FechaOut()
        Try
            If intIDCab <> 0 Then
                Dim dtFechaRecepcion As Date = dtpFechaRecepcion.Value
                'nueva condicion, solo para files
                Dim dttFechaOutPeru As DataTable
                Dim objCotiBN As New clsCotizacionBN
                dttFechaOutPeru = objCotiBN.ConsultarFechaOutPeru_File(intIDCab)
                Dim dtFecha As DateTime = Now
                If dttFechaOutPeru.Rows.Count > 0 Then
                    dtFecha = CDate(dttFechaOutPeru.Rows(0).Item("Dia").ToString)
                    'pregunto si año es igual
                    If dtFecha.Year = dtFechaRecepcion.Year Then
                        'preguntar por el mes
                        If dtFecha.Month <= dtFechaRecepcion.Month Then
                            'lista normal
                            Configurar_Cuentas_General()
                        Else
                            'moneda nacional o extranjera
                            cargarCuentas_MN_ME()
                        End If
                    ElseIf dtFecha.Year < dtFechaRecepcion.Year Then
                        'lista normal
                        Configurar_Cuentas_General()
                    Else
                        'moneda nacional o extranjera
                        cargarCuentas_MN_ME()
                    End If
                Else
                    Exit Sub
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarCombo_CuentasContables()
        Try

            Dim strTipoDoc As String = ""
            Dim strTipoOpe As String = ""

            If cboTipoOperacion.Items.Count > 0 Then
                If Not cboTipoOperacion.SelectedValue Is Nothing Then
                    strTipoOpe = cboTipoOperacion.SelectedValue.ToString
                End If

            End If

            Dim strEsProvAdmin As String = ""

            If blnEsProvAdmin = True Then
                strEsProvAdmin = "S"
            Else
                strEsProvAdmin = "N"
            End If

            If strTipoVoucheruOrServ = "OCP" Then
                Dim objBN As New clsDocumentoProveedorBN
                pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable(strTipoOpe, strEsProvAdmin), False)
                If cboCuentaContable.Items.Count = 1 Then
                    cboCuentaContable.SelectedIndex = 0
                End If
            Else
                'nueva condicion, solo para files
                Dim dttFechaOutPeru As DataTable
                Dim objCotiBN As New clsCotizacionBN
                dttFechaOutPeru = objCotiBN.ConsultarFechaOutPeru_File(intIDCab)
                Dim dtFecha As DateTime = Now
                If dttFechaOutPeru.Rows.Count > 0 Then
                    dtFecha = CDate(dttFechaOutPeru.Rows(0).Item("Dia").ToString)
                    'pregunto si año es igual
                    If dtFecha.Year = Now.Year Then
                        'preguntar por el mes
                        If dtFecha.Month <= Now.Month Then
                            'lista normal
                            Dim objBN As New clsDocumentoProveedorBN
                            pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable(strTipoOpe, strEsProvAdmin), False)
                            If cboCuentaContable.Items.Count = 1 Then
                                cboCuentaContable.SelectedIndex = 0
                            End If
                        Else
                            'moneda nacional o extranjera
                            cargarCuentas_MN_ME()
                        End If
                    ElseIf dtFecha.Year < Now.Year Then
                        'lista normal
                        Dim objBN As New clsDocumentoProveedorBN
                        pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable(strTipoOpe, strEsProvAdmin), False)
                        If cboCuentaContable.Items.Count = 1 Then
                            cboCuentaContable.SelectedIndex = 0
                        End If
                    Else
                        'moneda nacional o extranjera
                        cargarCuentas_MN_ME()
                    End If
                Else
                    Exit Sub
                End If

            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarCombo_CuentasContables_CeCos()
        Try

            Dim strTipoDoc As String = ""
            Dim strTipoOpe As String = ""
            Dim strCecos As String = ""

            'If cboTipoOperacion.Items.Count > 0 Then
            '    If Not cboTipoOperacion.SelectedValue Is Nothing Then
            '        strTipoOpe = cboTipoOperacion.SelectedValue.ToString
            '    End If

            'End If

            If cboCentroCostos.Items.Count > 0 Then
                If Not cboCentroCostos.SelectedValue Is Nothing Then
                    strCecos = cboCentroCostos.SelectedValue.ToString
                End If

            End If

            Dim strEsProvAdmin As String = ""

            If blnEsProvAdmin = True Then
                strEsProvAdmin = "S"
            Else
                strEsProvAdmin = "N"
            End If

            If strTipoVoucheruOrServ = "OCP" Or strTipoVoucheruOrServ = "OTR" Then
                Dim objBN As New clsDocumentoProveedorBN
                pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable_CentroCostos(strCecos, strEsProvAdmin), False)
                If cboCuentaContable.Items.Count = 1 Then
                    cboCuentaContable.SelectedIndex = 0
                End If
            Else
                'nueva condicion, solo para files
                Dim dttFechaOutPeru As DataTable
                Dim objCotiBN As New clsCotizacionBN
                dttFechaOutPeru = objCotiBN.ConsultarFechaOutPeru_File(intIDCab)
                Dim dtFecha As DateTime = Now
                If dttFechaOutPeru.Rows.Count > 0 Then
                    If dttFechaOutPeru.Rows(0).Item("Dia").ToString <> "" Then
                        dtFecha = CDate(dttFechaOutPeru.Rows(0).Item("Dia").ToString)
                    End If

                    'pregunto si año es igual
                    If dtFecha.Year = Now.Year Then
                        'preguntar por el mes
                        If dtFecha.Month <= Now.Month Then
                            'lista normal
                            Dim objBN As New clsDocumentoProveedorBN
                            pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable_CentroCostos(strCecos, strEsProvAdmin), False)
                            If cboCuentaContable.Items.Count = 1 Then
                                cboCuentaContable.SelectedIndex = 0
                            End If
                        Else
                            'moneda nacional o extranjera
                            cargarCuentas_MN_ME()
                        End If
                    ElseIf dtFecha.Year < Now.Year Then
                        'lista normal
                        Dim objBN As New clsDocumentoProveedorBN
                        pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable_CentroCostos(strCecos, strEsProvAdmin), False)
                        If cboCuentaContable.Items.Count = 1 Then
                            cboCuentaContable.SelectedIndex = 0
                        End If
                    Else
                        'moneda nacional o extranjera
                        cargarCuentas_MN_ME()
                    End If
                Else
                    Exit Sub
                End If

            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarCombo_TiposdeOperacion_CeCos()
        Try


            Dim strCecos As String = ""

            If cboCentroCostos.Items.Count > 0 Then
                If Not cboCentroCostos.SelectedValue Is Nothing Then
                    strCecos = cboCentroCostos.SelectedValue.ToString
                End If

            End If

            Dim objBN As New clsDocumentoProveedorBN
            pCargaCombosBox(cboTipoOperacion, objBN.ConsultarTipoOperacionCont_CentroCostos(strCecos), False)


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cargarCuentas_MN_ME()
        Try
            'moneda nacional o extranjera
            blnMNME = True
            Dim intIndice As Integer = 0
            If cboCuentaContable.Items.Count > 0 Then
                If cboCuentaContable.SelectedIndex > -1 Then
                    intIndice = cboCuentaContable.SelectedIndex
                End If
            End If
            Dim objBN As New clsDocumentoProveedorBN
            pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable_MN_ME(), False)
            If cboCuentaContable.Items.Count = 1 Then
                cboCuentaContable.SelectedIndex = 0
            End If

            If intIndice > 0 Then
                cboCuentaContable.SelectedIndex = intIndice
            End If


            Dim strIDMoneda As String = cboMoneda.SelectedValue.ToString
            If strIDMoneda = gstrTipoMonedaSol Then
                cboCuentaContable.SelectedIndex = 0
            Else
                cboCuentaContable.SelectedIndex = 1
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cambiarCuenta_MNME_Moneda()
        Try
            If cboCuentaContable.Items.Count > 0 Then
                Dim strIDMoneda As String = cboMoneda.SelectedValue.ToString
                If strIDMoneda = gstrTipoMonedaSol Then
                    cboCuentaContable.SelectedIndex = 0
                Else
                    cboCuentaContable.SelectedIndex = 1
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarCombo_CuentasContables_Anterior()
        Try

            Dim strTipoDoc As String = ""
            Dim strTipoOpe As String = ""
            'capturar valor de tipo documento
            'If cboTipoDocumento.Items.Count > 0 Then
            'capturar valor de tipo de operacion
            'If Not cboTipoDocumento.SelectedValue Is Nothing Then
            'strTipoDoc = cboTipoDocumento.SelectedValue.ToString
            If cboTipoOperacion.Items.Count > 0 Then
                If Not cboTipoOperacion.SelectedValue Is Nothing Then
                    strTipoOpe = cboTipoOperacion.SelectedValue.ToString
                End If

            End If
            'End If

            'End If
            'cargar combo

            Dim strEsProvAdmin As String = ""

            If blnEsProvAdmin = True Then
                strEsProvAdmin = "S"
            Else
                strEsProvAdmin = "N"
            End If

            Dim objBN As New clsDocumentoProveedorBN
            pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable(strTipoOpe, strEsProvAdmin), False)
            If cboCuentaContable.Items.Count = 1 Then
                cboCuentaContable.SelectedIndex = 0
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ModoNuevo()
        strModoEdicion = "N"

        lblTitulo.Text = "Nuevo Documento de Proveedor"
        Me.Text = lblTitulo.Text
        lblNuVoucher.Text = vstrVoucher 'intNuVoucher
        If Not frmRefE Is Nothing Then
            lblNuVoucher.Text = frmRefE.lblNuVoucher.Text
        End If

        If strTipoVoucheruOrServ = "PST" Or strTipoVoucheruOrServ = "PTT" Then
            lblNuVoucher.Text = vstrVoucher
        End If

        blnNuevoEnabled = False
        blnDeshacerEnabled = True
        blnGrabarEnabled = True
        blnEditarEnabled = False
        blnEliminarEnabled = False
        blnSalirEnabled = False
        ActivarDesactivarBotones()

        pClearControls(Me)
        pEnableControls(Me, True)

        'chkTipoCambio.Checked = False
        'grbTipoCambio.Enabled = False
        chkIGV.Checked = False
        txtIGV.Enabled = False
        txtIGV.Text = "0.00"
        txtSsNeto.Text = "0.00"
        txtSsOtrCargos.Text = "0.00"
        txtSsDetraccion.Text = "0.00"

        chkFechaEmision.Checked = False
        dtpFechaEmision.Enabled = False
        dtpFechaEmision.Value = "01/01/1900" 'Now.Date
        dtpFechaRecepcion.Value = Now.Date
        BloquearDesbloquear_Detalle()

        If strMonedaFEgreso <> String.Empty Then cboMoneda.SelectedValue = strMonedaFEgreso
        'cboMoneda_Pago.SelectedValue = strMonedaFEgreso

        CambioMoneda()

        'If strIDTipoOC <> "" Then
        '    cboTipoOperacion.SelectedValue = strIDTipoOC
        '    CargarCombo_CuentasContables()
        '    If Not strCtaContable Is Nothing Then
        '        If blnMNME = False Then
        '            cboCuentaContable.SelectedValue = strCtaContable
        '        End If
        '    End If

        'End If

        If strIDTipoOC <> "" Then
            cboTipoOperacion.SelectedValue = strIDTipoOC
            CambiarTipoOpe()
        End If

        'validar si el file es de tipo de venta counter
        Dim strTipoVenta_File As String = ""
        If intIDCab > 0 Then
            Using objBN As New clsCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(intIDCab)
                    dr.Read()
                    If dr.HasRows Then
                        strTipoVenta_File = dr("CoTipoVenta")
                    End If
                End Using
            End Using
        End If

        If strTipoVenta_File = "CU" Then
            strCoCeCos_Defecto = "050102"
            cboTipoOperacion.SelectedValue = "012"
            'cboCentroCostos.SelectedValue = "050102"
            'Else
            '    cboCentroCostos.SelectedValue = strCoCeCos_Defecto
        End If

        If strTipoVenta_File = "VC" Then
            strCoCeCos_Defecto = "050301 "
            cboCentroControl.SelectedValue = "300203"
        End If

        cboCentroCostos.SelectedValue = strCoCeCos_Defecto

        CambiarValor_CentroCostos()

        If strCoCeCos <> "" Then
            cboCentroCostos.SelectedValue = strCoCeCos
            CargarCombo_CuentasContables_CeCos()
            If Not strCtaContable Is Nothing Then
                If blnMNME = False Then
                    cboCuentaContable.SelectedValue = strCtaContable
                End If
            End If

        End If

        If vstrIDProveedor.Trim.Length > 0 Then
            DevolverDatosProveedor_SAP()
        End If

        CargarTipoCambio_Proveedor()

        'seleccionar x defecto servicios y bloquear
        If cboTipoSAP.Items.Count > 0 Then
            cboTipoSAP.SelectedValue = "1"
            'cboTipoSAP.Enabled = False
        End If

        If strTipoVoucheruOrServ = "OTR" Then
            If FlTodasMonedas() Then
                grbTipoCambioFormaEgreso.Visible = True
            End If
        End If

        If strTipoVenta_File = "VC" Then

            cboCentroControl.SelectedValue = "300203"
        End If

    End Sub


    Private Sub ModoEditar()
        strModoEdicion = "E"

        'lblTitulo.Text = " Edición de Documento N° " & vstrNuDocum 'strCoTicket '& strCoTicket
        lblTitulo.Text = " Documento N° " & vstrNuDocum 'strCoTicket '& strCoTicket
        Me.Text = lblTitulo.Text

        blnNuevoEnabled = False
        blnDeshacerEnabled = True
        blnGrabarEnabled = True
        blnSalirEnabled = False
        ActivarDesactivarBotones()
        'chkFechaEmision.Checked = True
        If bln_mMigrado Then

        Else
            pEnableControls(Me, True)
        End If

        If blnDocActivo = False Then
            'si esta anulado bloquear todo
            For Each ctrl As Control In Me.Controls
                ctrl.Enabled = False
            Next
            blnGrabarEnabled = False
            ActivarDesactivarBotones()
        End If

        If vstrIDProveedor <> strCoProveedorAdminist And strCoProveedorAdminist <> "" Then
            vstrIDProveedor = strCoProveedorAdminist
            lblProveedor.Visible = True
            lblEtiqProveedor.Visible = True
            btnSelProveedor.Visible = True

        End If

        'lblFile.Focus()
        CargarTipoCambio_Proveedor()
    End Sub

    Private Function blnValidarIngresos() As Boolean
        Try
            Dim blnOK As Boolean = True

            If txtNuDocumento.Text = "" Then
                'ErrPrv.SetError(txtNuDocumento, "Debe  de ingresar N° de Documento")
                'blnOK = False
                ErrPrv.SetError(txtNuDocumento, "")
            Else
                'validar solo 10 numeros sin guion en num. doc.
                Dim strNuDoc As String = txtNuDocumento.Text.Trim.Replace("-", "")
                'If strNuDoc.Length <> 10 Then

                'If strNuDoc.Length > 15 Then
                If strNuDoc.Length > 30 Then
                    'ErrPrv.SetError(txtNuDocumento, "N° de Documento debe de estar compuesto máximo por 15 números")
                    ErrPrv.SetError(txtNuDocumento, "N° de Documento debe de estar compuesto máximo por 30 números")
                    blnOK = False
                    txtNuDocumento.Focus()
                Else
                    ErrPrv.SetError(txtNuDocumento, "")
                End If

                'strNuDoc=strNuDoc.

            End If

            ''If cboTipoDocumento.SelectedValue <> "OTR" Then
            'If Not (cboTipoDocumento.SelectedValue = "OTR" Or cboTipoDocumento.SelectedValue = "TFC" Or cboTipoDocumento.SelectedValue = "CBR" Or cboTipoDocumento.SelectedValue = "RSP" Or cboTipoDocumento.SelectedValue = "IFE") Then
            '    If txtSerie.Text = "" Then
            '        ErrPrv.SetError(txtSerie, "Debe  de ingresar Serie de Documento")
            '        blnOK = False
            '    Else
            '        ErrPrv.SetError(txtSerie, "")
            '    End If
            'Else
            '    ErrPrv.SetError(txtSerie, "")
            'End If



            Dim dblNeto As Double = If(txtSsNeto.Text.Trim = "", "0", txtSsNeto.Text.Trim)
            If dblNeto = 0 Then
                ErrPrv.SetError(txtSsNeto, "Debe ingresar Neto")
                blnOK = False
            Else
                ErrPrv.SetError(txtSsNeto, "")
            End If

            Dim dblImporte As Double = If(txtMonto.Text.Trim = "", "0", txtMonto.Text.Trim)
            If dblImporte = 0 Then
                ErrPrv.SetError(txtMonto, "Debe ingresar Monto")
                blnOK = False
            Else
                ErrPrv.SetError(txtMonto, "")
            End If

            'Dim dblImporte2 As Double = If(txtIGV.Text.Trim = "", "0", txtIGV.Text.Trim)
            'If dblImporte2 = 0 Then
            '    ErrPrv.SetError(txtIGV, "Debe ingresar IGV")
            '    blnOK = False
            'Else
            '    ErrPrv.SetError(txtIGV, "")
            'End If

            ' If strCoUbigeo_Oficina <> gstrBuenosAires Then
            If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                Dim dblGastos As Double = If(txtSsOtrCargos.Text.Trim = "", "0", txtSsOtrCargos.Text.Trim)
                If dblGastos <> 0 Then
                    Dim strGastos As String = cboGastos.Text.Trim
                    If strGastos = "" Then
                        ErrPrv.SetError(cboGastos, "Debe seleccionar un gasto adicional")
                        blnOK = False
                    Else
                        ErrPrv.SetError(cboGastos, "")
                    End If
                Else
                    ErrPrv.SetError(cboGastos, "")
                End If

                If Not blnGastoAdicional_CuentaContable() Then
                    ErrPrv.SetError(cboGastos, "El gasto adicional no está asociado a la cuenta contable seleccionada")
                    blnOK = False
                Else
                    ErrPrv.SetError(cboGastos, "")
                End If
            End If


            If cboTipoDocumento.SelectedValue Is Nothing Then
                ErrPrv.SetError(cboTipoDocumento, "Debe seleccionar un tipo de documento")
                blnOK = False
                cboTipoDocumento.Focus()
            Else
                ErrPrv.SetError(cboTipoDocumento, "")
            End If

            'If strCoUbigeo_Oficina <> gstrBuenosAires Then
            If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                If cboTipoOperacion.SelectedValue Is Nothing Then
                    ErrPrv.SetError(cboTipoOperacion, "Debe seleccionar un tipo de operación")
                    blnOK = False
                    cboTipoOperacion.Focus()
                Else
                    If cboTipoOperacion.SelectedValue.ToString.Trim = "" Then
                        ErrPrv.SetError(cboTipoOperacion, "Debe seleccionar un tipo de operación")
                        blnOK = False
                        cboTipoOperacion.Focus()
                    End If

                End If
            End If


            'If strCoUbigeo_Oficina <> gstrBuenosAires Then
            If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                'If strTipoVoucheruOrServ <> "OTR" Then
                If Not (strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP") Then
                    If cboCentroCostos.SelectedValue Is Nothing Then
                        ErrPrv.SetError(cboCentroCostos, "Debe seleccionar un centro de costos")
                        blnOK = False
                        cboCentroCostos.Focus()
                    Else
                        If cboCentroCostos.SelectedValue.ToString.Trim = "" Then
                            ErrPrv.SetError(cboCentroCostos, "Debe seleccionar un centro de costos")
                            blnOK = False
                            cboCentroCostos.Focus()
                        End If

                    End If


                End If


                If cboTipoSAP.SelectedValue = "1" Then
                    If cboCuentaContable.SelectedValue Is Nothing Then
                        ErrPrv.SetError(cboCuentaContable, "Debe seleccionar una cuenta contable")
                        blnOK = False
                        cboCuentaContable.Focus()
                    End If
                End If


                If cboTipoDetraccion.SelectedValue Is Nothing Then
                    ErrPrv.SetError(cboTipoDetraccion, "Debe seleccionar un tipo de detracción")
                    blnOK = False
                    cboTipoDetraccion.Focus()
                End If


                If blnDetraccionError = True And chkFechaEmision.Checked = True Then
                    ErrPrv.SetError(dtpFechaEmision, "No existe un tipo de cambio para la fecha indicada.")
                    blnOK = False
                    dtpFechaEmision.Focus()
                End If

                If blnDetraccionErrorMoneda = True Then
                    ErrPrv.SetError(cboMoneda, "No existe un tipo de cambio para la moneda indicada.")
                    blnOK = False
                    cboMoneda.Focus()
                End If

            End If





            If dtpFechaEmision.Value.Date = "01/01/1900" Then
                ErrPrv.SetError(dtpFechaEmision, "Debe ingresar una fecha de Emisión")
                blnOK = False
                dtpFechaEmision.Focus()
            Else
                If dtpFechaEmision.Value.Date > dtpFechaRecepcion.Value.Date Then
                    ErrPrv.SetError(dtpFechaEmision, "Fecha de Emisión debe de ser menor o igual a la Fecha de Recepción")
                    blnOK = False
                    dtpFechaEmision.Focus()
                End If
            End If



            If cboMoneda.SelectedValue Is Nothing Then
                ErrPrv.SetError(cboMoneda, "Debe seleccionar una moneda")
                blnOK = False
                cboMoneda.Focus()
            End If


            'If cboMoneda_Pago.SelectedValue Is Nothing Then
            '    ErrPrv.SetError(cboMoneda_Pago, "Debe seleccionar una moneda de pago")
            '    blnOK = False
            '    cboMoneda_Pago.Focus()
            'End If

            'If chkTipoCambio.Checked = True Then
            '    If txtTipoCambio.Text.Trim.Length = 0 Then
            '        ErrPrv.SetError(txtTipoCambio, "Debe ingresar un tipo de cambio")
            '        blnOK = False
            '        txtTipoCambio.Focus()
            '    Else
            '        If CDbl(txtTipoCambio.Text.Trim) = 0 Then
            '            ErrPrv.SetError(txtTipoCambio, "Debe ingresar un tipo de cambio")
            '            blnOK = False
            '            txtTipoCambio.Focus()
            '        End If
            '    End If
            'End If

            If strTipoVoucheruOrServ <> "PST" Or strTipoVoucheruOrServ = "PTT" Then
                If cboMoneda.SelectedValue IsNot Nothing Then
                    If strMonedaFEgreso <> "" Then
                        If strMonedaFEgreso <> cboMoneda.SelectedValue.ToString Then
                            If txtTipoCambioFEgreso.Text.Trim.Length = 0 Then
                                ErrPrv.SetError(txtTipoCambioFEgreso, "Debe ingresar un tipo de cambio")
                                blnOK = False
                                txtTipoCambioFEgreso.Focus()
                            Else
                                If CDbl(txtTipoCambioFEgreso.Text.Trim) = 0 Then
                                    ErrPrv.SetError(txtTipoCambioFEgreso, "Debe ingresar un tipo de cambio")
                                    blnOK = False
                                    txtTipoCambioFEgreso.Focus()
                                End If
                            End If
                        End If
                    End If
                End If
            End If

            If strTipoVoucheruOrServ = "OCP" Then
                If strCoProveedorAdminist = "" Then
                    ErrPrv.SetError(btnSelProveedor, "Debe seleccionar un Proveedor")
                    blnOK = False
                End If
            End If

            Return blnOK
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnGastoAdicional_CuentaContable() As Boolean
        Try
            Dim bln As Boolean = True
            Dim strCoGasto As String = ""
            Dim strCtaContable As String = ""
            Dim strCtaContable_Gasto As String = ""

            If cboGastos.Items.Count > 0 Then
                If cboGastos.Text <> "" Then
                    strCoGasto = cboGastos.SelectedValue.ToString
                End If
            End If



            If cboCuentaContable.Items.Count > 0 Then
                If cboCuentaContable.Text.Trim.Length > 0 Then
                    strCtaContable = cboCuentaContable.SelectedValue.ToString
                Else
                    strCtaContable = ""
                End If

            End If

            'obtener cuenta contable del gasto adicional

            Using objBN As New clsMAGastoAdicionalBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(strCoGasto)
                    dr.Read()
                    If dr.HasRows Then
                        strCtaContable_Gasto = dr("CtaContable").ToString.Trim
                    Else

                    End If
                    dr.Close()
                End Using
            End Using

            'validar
            If strCoGasto = "" Then
                bln = True
            Else
                If strCoGasto = "PERCXP" Then
                    bln = True
                Else

                    If strCtaContable = "" Then
                        bln = True
                    Else
                        If strCtaContable = strCtaContable_Gasto Then
                            bln = True
                        Else
                            bln = False
                        End If
                    End If


                End If
            End If

            Return bln

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub Grabar()
        Try
            Dim objBN As New clsDocumentoProveedorBN
            Dim objBE As New clsDocumentoProveedorBE
            Dim objBEDocForEgr As New clsDocumento_Forma_Egreso
            With objBE

                'numero interno
                .NuDocInterno = objBN.strDevuelveNuDocInterno_Correlativo(dtpFechaEmision.Value.Date.ToString("dd/MM/yyyy"))

                If strTipoVoucheruOrServ = "VOU" Then
                    .NuVoucher = vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = 0
                    .NuOrdComInt = 0
                    .NuPreSob = 0
                    .NuVouPTrenInt = 0
                ElseIf strTipoVoucheruOrServ = "OSV" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = intNuPreSob 'intNuOrden_Servicio
                    .CoOrdPag = 0
                    .NuOrdComInt = 0
                    .NuPreSob = 0
                    .NuVouPTrenInt = 0
                ElseIf strTipoVoucheruOrServ = "OPA" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = intNuPreSob 'intNuOrden_Servicio
                    .NuOrdComInt = 0
                    .NuPreSob = 0
                    .NuVouPTrenInt = 0
                ElseIf strTipoVoucheruOrServ = "OCP" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = 0
                    .NuOrdComInt = intNuOrdenCompra
                    .NuPreSob = 0
                    .NuVouPTrenInt = 0
                ElseIf strTipoVoucheruOrServ = "PST" Or strTipoVoucheruOrServ = "PTT" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = 0
                    .NuOrdComInt = 0
                    .NuPreSob = intNuPreSob
                    .NuVouPTrenInt = 0
                ElseIf strTipoVoucheruOrServ = "PRL" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = 0
                    .NuOrdComInt = 0
                    .NuPreSob = 0
                    .NuVouPTrenInt = 0
                ElseIf strTipoVoucheruOrServ = "ZIC" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = 0
                    .NuOrdComInt = 0
                    .NuPreSob = 0
                    .NuVouPTrenInt = 0
                    If IsNumeric(vstrVoucher) = True Then
                        .IDDocFormaEgreso = CInt(vstrVoucher)
                    End If
                    .CoMoneda_FEgreso = strMonedaFEgreso
                    objBEDocForEgr = frmRefE.ObtenerDocumentoFormaEgreso()
                End If

                'If intNuOrden_Servicio > 0 Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = intNuOrden_Servicio
                'Else
                '    .NuVoucher = vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                'End If

                .IDCab = intIDCab
                .IDCab_FF = intIDCab
                .NuSerie = txtSerie.Text.Trim.Replace("-", "")
                .NuDocum = txtNuDocumento.Text.Trim.Replace("-", "")
                .CoTipoDoc = cboTipoDocumento.SelectedValue
                .FeEmision = dtpFechaEmision.Value.Date.ToString("dd/MM/yyyy")
                .FeRecepcion = dtpFechaRecepcion.Value.Date.ToString("dd/MM/yyyy")


                .CoTipoDetraccion = cboTipoDetraccion.SelectedValue
                .SsOtrCargos = IIf(txtSsOtrCargos.Text.Trim.Length > 0, txtSsOtrCargos.Text.Trim, "0")
                .SsDetraccion = IIf(txtSsDetraccion.Text.Trim.Length > 0, txtSsDetraccion.Text.Trim, "0")


                .CoMoneda = cboMoneda.SelectedValue
                .CoMoneda_Pago = cboMoneda.SelectedValue 'cboMoneda_Pago.SelectedValue
                .SsIGV = IIf(txtIGV.Text.Trim.Length > 0, txtIGV.Text.Trim, "0")
                .SsNeto = IIf(txtSsNeto.Text.Trim.Length > 0, txtSsNeto.Text.Trim, "0")


                .SSTotalOriginal = IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")

                'If cboTipoDocumento.SelectedValue = "NCR" Then
                If cboTipoDocumento.SelectedValue = "NCR" Or cboTipoDocumento.SelectedValue = "NCE" Then
                    .SSTotalOriginal = .SSTotalOriginal * -1
                End If

                '.NuMaxHorasSemana = IIf(txtNuMaxHorasSemana.Text.Trim.Length > 0, CInt(txtNuMaxHorasSemana.Text.Trim), 0)

                'If chkTipoCambio.Checked = True Then
                '    .SSTipoCambio = IIf(txtTipoCambio.Text.Trim.Length > 0, txtTipoCambio.Text.Trim, "0")
                '    .SSTotal = IIf(txtTotalTC.Text.Trim.Length > 0, txtTotalTC.Text.Trim, "0")

                '    If cboTipoDocumento.SelectedValue = "NCR" Then
                '        .SSTotal = .SSTotal * -1
                '    End If

                'Else
                .SSTipoCambio = 0
                .SSTotal = IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")

                If cboTipoDocumento.SelectedValue = "NCR" Or cboTipoDocumento.SelectedValue = "NCE" Then
                    .SSTotal = .SSTotal * -1
                End If
                'End If

                .CoCecos = cboCentroCostos.SelectedValue
                .CoTipoOC = cboTipoOperacion.SelectedValue
                .CoCtaContab = cboCuentaContable.SelectedValue


                .CoObligacionPago = strTipoVoucheruOrServ
                .IngresoManual = True
                If strTipoVoucheruOrServ <> "PST" Or strTipoVoucheruOrServ = "PTT" Then
                    .CoMoneda_FEgreso = strMonedaFEgreso
                Else
                    .CoMoneda_FEgreso = cboMoneda.SelectedValue
                End If

                If strTipoVoucheruOrServ = "OTR" Then
                    .CoMoneda_FEgreso = cboMoneda.SelectedValue
                End If

                .SsTipoCambio_FEgreso = Convert.ToDouble(If(txtTipoCambioFEgreso.Text.Trim = "", "0", txtTipoCambioFEgreso.Text.Trim))
                .SsTotal_FEgreso = Convert.ToDouble(txtTotalFEgreso.Text.Trim)

                .IDOperacion = intIDOperacion



                If strTipoVoucheruOrServ = "PRL" Then
                    .CoProveedor = vstrIDProveedor 'Peru Rail
                    .SsTotal_FEgreso = frmRefE.lblTotal.Text
                Else
                    If strCoProveedorAdminist <> "" Then
                        .CoProveedor = strCoProveedorAdminist
                    Else
                        .CoProveedor = vstrIDProveedor
                    End If
                End If

                If cboTipoDocumento.SelectedValue = "NCR" Or cboTipoDocumento.SelectedValue = "NCE" Then
                    .SsTotal_FEgreso = .SsTotal_FEgreso * -1
                End If

                .SSPercepcion = txtPercepcion.Text.Trim
                .TxConcepto = txtConcepto.Text.Trim


                .FeVencimiento = dtpFechaVencimiento.Value.Date.ToString("dd/MM/yyyy")
                .CoTipoDocSAP = cboTipoSAP.SelectedValue
                .CoFormaPago = strFormaPago

                If gbxDocumentoVinculado.Visible = True Then
                    .CoTipoDoc_Ref = cboTipoDocumento_DocumVinc.SelectedValue
                    .NuSerie_Ref = txtSerie_Ref.Text.Trim
                    .NuDocum_Ref = txtNroDocumento_DocumVinc.Text.Trim
                    .FeEmision_Ref = dtpFechaEmision_Ref.Value.Date.ToString("dd/MM/yyyy")
                Else
                    .FeEmision_Ref = CDate("01/01/1900").ToString("dd/MM/yyyy")
                End If


                .CoCeCon = cboCentroControl.SelectedValue
                .CoGasto = cboGastos.SelectedValue
                .CoUbigeo_Oficina = strCoUbigeo_Oficina
                .UserMod = gstrUser
                .UserNuevo = gstrUser


                'Si es Setours bs aires, mandar parametros en blanco
                ' If strCoUbigeo_Oficina = gstrBuenosAires Then
                If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                    .CoTipoDetraccion = "00000"
                    .SsOtrCargos = 0
                    .SsDetraccion = 0
                    .CoCecos = ""
                    .CoTipoOC = ""
                    .CoCtaContab = ""
                    .SSPercepcion = 0
                    .CoCeCon = ""
                    .CoGasto = ""
                End If



            End With

            Dim objBEIti As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
            Dim LstDetalle As New List(Of clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)

            If cboTipoSAP.SelectedValue = "0" Then
                For Each ST As stDocumento_Detalle In objLstDocumento_Detalle
                    objBEIti = New clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
                    With objBEIti

                        .NuDocumProv = 0 'bytGenerarCoDocumento()
                        .NuDocumProvDet = ST.NuDocumProvDet
                        .IDServicio_Det = 0 'ST.IDServicio_Det
                        .TxServicio = ""
                        .QtCantidad = ST.QtCantidad
                        .SsMonto = ST.SsMonto
                        .Accion = ST.strAccion
                        .UserMod = gstrUser
                        .IDCab = intIDCab
                        .CoProducto = ST.CoProducto
                        .CoAlmacen = ST.CoAlmacen
                        .SsIGV = ST.SsIGV
                        .SSPrecUni = ST.SSPrecUni
                        .SsTotal = ST.SSTotal


                    End With
                    LstDetalle.Add(objBEIti)
                Next
            End If

            objBE.ListaDetalle = LstDetalle


            Dim objBT As New clsDocumentoProveedorBT
            'objBT.Insertar(objBE)
            'If strCoUbigeo_Oficina = gstrBuenosAires Then
            If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                objBT.Grabar(objBE, False)
            Else
                If strTipoVoucheruOrServ = "OTR" Then
                    objBT.Grabar(objBE, True)
                Else
                    If strTipoVoucheruOrServ = "ZIC" Then
                        objBT.Grabar(objBE, False, , objBEDocForEgr)
                    Else
                        objBT.Grabar(objBE, False)
                    End If
                End If
            End If

            'objBT.Grabar(objBE, False)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Grabar_Multiple()
        Try
            Dim objBN As New clsDocumentoProveedorBN
            Dim objBE As New clsDocumentoProveedorBE
            With objBE

                'numero interno
                .NuDocInterno = objBN.strDevuelveNuDocInterno_Correlativo(dtpFechaEmision.Value.Date.ToString("dd/MM/yyyy"))

                'If strTipoVoucheruOrServ = "VOU" Then
                '    .NuVoucher = vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                '    .CoOrdPag = 0
                '    .NuOrdComInt = 0
                '    .NuPreSob = 0
                '    .NuVouPTrenInt = 0
                'ElseIf strTipoVoucheruOrServ = "OSV" Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = intNuPreSob 'intNuOrden_Servicio
                '    .CoOrdPag = 0
                '    .NuOrdComInt = 0
                '    .NuPreSob = 0
                '    .NuVouPTrenInt = 0
                'ElseIf strTipoVoucheruOrServ = "OPA" Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                '    .CoOrdPag = intNuPreSob 'intNuOrden_Servicio
                '    .NuOrdComInt = 0
                '    .NuPreSob = 0
                '    .NuVouPTrenInt = 0
                'ElseIf strTipoVoucheruOrServ = "OCP" Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                '    .CoOrdPag = 0
                '    .NuOrdComInt = intNuOrdenCompra
                '    .NuPreSob = 0
                '    .NuVouPTrenInt = 0
                'ElseIf strTipoVoucheruOrServ = "PST" Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                '    .CoOrdPag = 0
                '    .NuOrdComInt = 0
                '    .NuPreSob = intNuPreSob
                '    .NuVouPTrenInt = 0
                'ElseIf strTipoVoucheruOrServ = "PRL" Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                '    .CoOrdPag = 0
                '    .NuOrdComInt = 0
                '    .NuPreSob = 0
                '    .NuVouPTrenInt = 0
                'End If

                'If intNuOrden_Servicio > 0 Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = intNuOrden_Servicio
                'Else
                '    .NuVoucher = vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                'End If

                .IDCab = intIDCab
                .IDCab_FF = intIDCab
                .NuSerie = txtSerie.Text.Trim.Replace("-", "")
                .NuDocum = txtNuDocumento.Text.Trim.Replace("-", "")
                .CoTipoDoc = cboTipoDocumento.SelectedValue
                .FeEmision = dtpFechaEmision.Value.Date.ToString("dd/MM/yyyy")
                .FeRecepcion = dtpFechaRecepcion.Value.Date.ToString("dd/MM/yyyy")
                .CoTipoDetraccion = cboTipoDetraccion.SelectedValue
                .CoMoneda = cboMoneda.SelectedValue
                .CoMoneda_Pago = cboMoneda.SelectedValue 'cboMoneda_Pago.SelectedValue
                .SsIGV = IIf(txtIGV.Text.Trim.Length > 0, txtIGV.Text.Trim, "0")
                .SsNeto = IIf(txtSsNeto.Text.Trim.Length > 0, txtSsNeto.Text.Trim, "0")
                .SsOtrCargos = IIf(txtSsOtrCargos.Text.Trim.Length > 0, txtSsOtrCargos.Text.Trim, "0")
                .SsDetraccion = IIf(txtSsDetraccion.Text.Trim.Length > 0, txtSsDetraccion.Text.Trim, "0")

                .SSTotalOriginal = IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")

                If cboTipoDocumento.SelectedValue = "NCR" Then
                    .SSTotalOriginal = .SSTotalOriginal * -1
                End If

                '.NuMaxHorasSemana = IIf(txtNuMaxHorasSemana.Text.Trim.Length > 0, CInt(txtNuMaxHorasSemana.Text.Trim), 0)

                'If chkTipoCambio.Checked = True Then
                '    .SSTipoCambio = IIf(txtTipoCambio.Text.Trim.Length > 0, txtTipoCambio.Text.Trim, "0")
                '    .SSTotal = IIf(txtTotalTC.Text.Trim.Length > 0, txtTotalTC.Text.Trim, "0")

                '    If cboTipoDocumento.SelectedValue = "NCR" Then
                '        .SSTotal = .SSTotal * -1
                '    End If

                'Else
                .SSTipoCambio = 0
                .SSTotal = IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")

                If cboTipoDocumento.SelectedValue = "NCR" Then
                    .SSTotal = .SSTotal * -1
                End If
                'End If

                .CoCecos = cboCentroCostos.SelectedValue
                .CoTipoOC = cboTipoOperacion.SelectedValue
                .CoCtaContab = cboCuentaContable.SelectedValue
                .CoObligacionPago = strTipoVoucheruOrServ
                .IngresoManual = True
                If strTipoVoucheruOrServ <> "PST" Or strTipoVoucheruOrServ = "PTT" Then
                    .CoMoneda_FEgreso = strMonedaFEgreso
                Else
                    .CoMoneda_FEgreso = cboMoneda.SelectedValue
                End If

                If strTipoVoucheruOrServ = "OTR" Then
                    .CoMoneda_FEgreso = cboMoneda.SelectedValue
                End If

                .CoMoneda_FEgreso = cboMoneda_Multiple.SelectedValue

                .SsTipoCambio_FEgreso = Convert.ToDouble(If(txtTipoCambioFEgreso.Text.Trim = "", "0", txtTipoCambioFEgreso.Text.Trim))
                .SsTotal_FEgreso = Convert.ToDouble(txtTotalFEgreso.Text.Trim)

                .IDOperacion = intIDOperacion



                If strTipoVoucheruOrServ = "PRL" Then
                    .CoProveedor = vstrIDProveedor 'Peru Rail
                    .SsTotal_FEgreso = frmRefE.lblTotal.Text
                Else
                    If strCoProveedorAdminist <> "" Then
                        .CoProveedor = strCoProveedorAdminist
                    Else
                        .CoProveedor = vstrIDProveedor
                    End If
                End If

                If cboTipoDocumento.SelectedValue = "NCR" Then
                    .SsTotal_FEgreso = .SsTotal_FEgreso * -1
                End If

                .SSPercepcion = txtPercepcion.Text.Trim
                .TxConcepto = txtConcepto.Text.Trim


                .FeVencimiento = dtpFechaVencimiento.Value.Date.ToString("dd/MM/yyyy")
                .CoTipoDocSAP = cboTipoSAP.SelectedValue
                .CoFormaPago = strFormaPago

                If gbxDocumentoVinculado.Visible = True Then
                    .CoTipoDoc_Ref = cboTipoDocumento_DocumVinc.SelectedValue
                    .NuSerie_Ref = txtSerie_Ref.Text.Trim
                    .NuDocum_Ref = txtNroDocumento_DocumVinc.Text.Trim
                    .FeEmision_Ref = dtpFechaEmision_Ref.Value.Date.ToString("dd/MM/yyyy")
                Else
                    .FeEmision_Ref = CDate("01/01/1900").ToString("dd/MM/yyyy")
                End If


                .CoCeCon = cboCentroControl.SelectedValue
                .CoGasto = cboGastos.SelectedValue
                .CoUbigeo_Oficina = strCoUbigeo_Oficina
                .UserMod = gstrUser
                .UserNuevo = gstrUser
                .FlMultiple = True
            End With

            'Dim objBEIti As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
            Dim LstDetalle As New List(Of clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)
            'For Each ST As stDocumento_Detalle In objLstDocumento_Detalle
            '    objBEIti = New clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
            '    With objBEIti

            '        .NuDocumProv = 0 'bytGenerarCoDocumento()
            '        .NuDocumProvDet = ST.NuDocumProvDet
            '        .IDServicio_Det = 0 'ST.IDServicio_Det
            '        .TxServicio = ""
            '        .QtCantidad = ST.QtCantidad
            '        .SsMonto = ST.SsMonto
            '        .Accion = ST.strAccion
            '        .UserMod = gstrUser
            '        .IDCab = intIDCab
            '        .CoProducto = ST.CoProducto
            '        .CoAlmacen = ST.CoAlmacen
            '        .SsIGV = ST.SsIGV
            '        .SSPrecUni = ST.SSPrecUni
            '        .SsTotal = ST.SSTotal


            '    End With
            '    LstDetalle.Add(objBEIti)
            'Next
            objBE.ListaDetalle = LstDetalle

            Dim strFEgreso_Tmp As String = ""

            If cboFormaEgreso.Items.Count > 0 Then
                If Not cboFormaEgreso.SelectedValue Is Nothing Then
                    strFEgreso_Tmp = cboFormaEgreso.SelectedValue
                End If
            End If



            'lista de vouchers para documentos
            Dim objBEDoc As clsDocumentoProveedorBE
            Dim LstDocs As New List(Of clsDocumentoProveedorBE)

            For Each dgvRow As DataGridViewRow In dgvVouchers.Rows
                Dim blnAsignar As Boolean = dgvRow.Cells("chkAsignar").Value
                If blnAsignar Then
                    'insertar documento detalle
                    objBEDoc = New clsDocumentoProveedorBE
                    With objBEDoc

                        'numero interno
                        .NuDocInterno = objBN.strDevuelveNuDocInterno_Correlativo(dtpFechaEmision.Value.Date.ToString("dd/MM/yyyy"))

                        'If strTipoVoucheruOrServ = "VOU" Then
                        If strFEgreso_Tmp = "VOU" Then
                            .NuVoucher = dgvRow.Cells("Codigo").Value 'intNuVoucher
                            .NuOrden_Servicio = 0
                            .CoOrdPag = 0
                            .NuOrdComInt = 0
                            .NuPreSob = 0
                            .NuVouPTrenInt = 0

                        ElseIf strFEgreso_Tmp = "OPA" Then
                            .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                            .NuOrden_Servicio = 0
                            .CoOrdPag = dgvRow.Cells("Codigo").Value 'intNuOrden_Servicio
                            .NuOrdComInt = 0
                            .NuPreSob = 0
                            .NuVouPTrenInt = 0

                        ElseIf strFEgreso_Tmp = "OSV" Then
                            .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                            .NuOrden_Servicio = dgvRow.Cells("Codigo").Value
                            .CoOrdPag = 0 'intNuOrden_Servicio
                            .NuOrdComInt = 0
                            .NuPreSob = 0
                            .NuVouPTrenInt = 0

                        ElseIf strFEgreso_Tmp = "PTT" Then
                            .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                            .NuOrden_Servicio = 0
                            .NuPreSob = dgvRow.Cells("Codigo").Value
                            .CoOrdPag = 0 'intNuOrden_Servicio
                            .NuOrdComInt = 0
                            .NuVouPTrenInt = 0

                        ElseIf strFEgreso_Tmp = "ZIC" Then
                            .NuVoucher = 0
                            .NuOrden_Servicio = 0
                            .NuPreSob = 0
                            .CoOrdPag = 0
                            .NuOrdComInt = 0
                            .NuVouPTrenInt = 0

                        End If

                        'ElseIf strTipoVoucheruOrServ = "OCP" Then
                        '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                        '    .NuOrden_Servicio = 0
                        '    .CoOrdPag = 0
                        '    .NuOrdComInt = intNuOrdenCompra
                        '    .NuPreSob = 0
                        '    .NuVouPTrenInt = 0
                        'ElseIf strTipoVoucheruOrServ = "PST" Then
                        '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                        '    .NuOrden_Servicio = 0
                        '    .CoOrdPag = 0
                        '    .NuOrdComInt = 0
                        '    .NuPreSob = intNuPreSob
                        '    .NuVouPTrenInt = 0
                        'ElseIf strTipoVoucheruOrServ = "PRL" Then
                        '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                        '    .NuOrden_Servicio = 0
                        '    .CoOrdPag = 0
                        '    .NuOrdComInt = 0
                        '    .NuPreSob = 0
                        '    .NuVouPTrenInt = 0
                        'End If

                        'If intNuOrden_Servicio > 0 Then
                        '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                        '    .NuOrden_Servicio = intNuOrden_Servicio
                        'Else
                        '    .NuVoucher = vstrVoucher 'intNuVoucher
                        '    .NuOrden_Servicio = 0
                        'End If

                        .IDCab = dgvRow.Cells("IDCab").Value
                        .IDCab_FF = dgvRow.Cells("IDCab").Value
                        .NuSerie = txtSerie.Text.Trim.Replace("-", "")
                        .NuDocum = txtNuDocumento.Text.Trim.Replace("-", "")
                        .CoTipoDoc = cboTipoDocumento.SelectedValue
                        .FeEmision = dtpFechaEmision.Value.Date.ToString("dd/MM/yyyy")
                        .FeRecepcion = dtpFechaRecepcion.Value.Date.ToString("dd/MM/yyyy")
                        .CoTipoDetraccion = cboTipoDetraccion.SelectedValue
                        .CoMoneda = cboMoneda.SelectedValue
                        .CoMoneda_Pago = cboMoneda.SelectedValue 'cboMoneda_Pago.SelectedValue
                        .SsIGV = 0 'IIf(txtIGV.Text.Trim.Length > 0, txtIGV.Text.Trim, "0")
                        '.SsIGV = IIf(txtIGV.Text.Trim.Length > 0, txtIGV.Text.Trim, "0")
                        If cboMoneda.SelectedValue <> cboMoneda_Multiple.SelectedValue Then
                            .SsNeto = dgvRow.Cells("Total_TC").Value 'IIf(txtSsNeto.Text.Trim.Length > 0, txtSsNeto.Text.Trim, "0")
                        Else
                            .SsNeto = dgvRow.Cells("MontoFinal").Value 'IIf(txtSsNeto.Text.Trim.Length > 0, txtSsNeto.Text.Trim, "0")
                        End If

                        '.SsOtrCargos = 0 'IIf(txtSsOtrCargos.Text.Trim.Length > 0, txtSsOtrCargos.Text.Trim, "0")
                        .SsOtrCargos = IIf(dgvRow.Cells("Gastos").Value.ToString.Trim.Length > 0, dgvRow.Cells("Gastos").Value, "0")
                        .SsDetraccion = 0 'IIf(txtSsDetraccion.Text.Trim.Length > 0, txtSsDetraccion.Text.Trim, "0")

                        .SSTotalOriginal = dgvRow.Cells("Monto").Value 'IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")

                        If cboTipoDocumento.SelectedValue = "NCR" Then
                            .SSTotalOriginal = .SSTotalOriginal * -1
                        End If

                        '.NuMaxHorasSemana = IIf(txtNuMaxHorasSemana.Text.Trim.Length > 0, CInt(txtNuMaxHorasSemana.Text.Trim), 0)

                        'If chkTipoCambio.Checked = True Then
                        '    .SSTipoCambio = IIf(txtTipoCambio.Text.Trim.Length > 0, txtTipoCambio.Text.Trim, "0")
                        '    .SSTotal = IIf(txtTotalTC.Text.Trim.Length > 0, txtTotalTC.Text.Trim, "0")

                        '    If cboTipoDocumento.SelectedValue = "NCR" Then
                        '        .SSTotal = .SSTotal * -1
                        '    End If

                        'Else
                        .SSTipoCambio = 0


                        If cboMoneda.SelectedValue <> cboMoneda_Multiple.SelectedValue Then
                            .SSTotal = dgvRow.Cells("Total_TC").Value 'IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")
                        Else
                            .SSTotal = dgvRow.Cells("Monto").Value 'IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")
                        End If



                        If cboTipoDocumento.SelectedValue = "NCR" Then
                            .SSTotal = .SSTotal * -1
                        End If
                        'End If

                        .CoCecos = cboCentroCostos.SelectedValue
                        .CoTipoOC = cboTipoOperacion.SelectedValue
                        .CoCtaContab = cboCuentaContable.SelectedValue
                        .CoObligacionPago = strFEgreso_Tmp '"VOU" 'strTipoVoucheruOrServ
                        .IngresoManual = True
                        'If strTipoVoucheruOrServ <> "PST" Then
                        '    .CoMoneda_FEgreso = strMonedaFEgreso
                        'Else
                        '    .CoMoneda_FEgreso = cboMoneda.SelectedValue
                        'End If

                        'If strTipoVoucheruOrServ = "OTR" Then
                        '    .CoMoneda_FEgreso = cboMoneda.SelectedValue
                        'End If

                        If FlTodasMonedas() Then
                            .CoMoneda_FEgreso = dgvRow.Cells("IDMoneda").Value  'cboMoneda_Multiple.SelectedValue
                        Else
                            .CoMoneda_FEgreso = cboMoneda_Multiple.SelectedValue
                        End If


                        .SsTipoCambio_FEgreso = Convert.ToDouble(If(txtTipoCambioFEgreso.Text.Trim = "", "0", txtTipoCambioFEgreso.Text.Trim))


                        .SsTotal_FEgreso = dgvRow.Cells("MontoFinal").Value 'Convert.ToDouble(txtTotalFEgreso.Text.Trim)

                        .IDOperacion = intIDOperacion



                        If strTipoVoucheruOrServ = "PRL" Then
                            .CoProveedor = vstrIDProveedor 'Peru Rail
                            '.SsTotal_FEgreso = frmRefE.lblTotal.Text
                        Else
                            If strCoProveedorAdminist <> "" Then
                                .CoProveedor = strCoProveedorAdminist
                            Else
                                .CoProveedor = vstrIDProveedor
                            End If
                        End If

                        'If cboTipoDocumento.SelectedValue = "NCR" Then
                        '    .SsTotal_FEgreso = .SsTotal_FEgreso * -1
                        'End If

                        .SSPercepcion = 0 'txtPercepcion.Text.Trim
                        .TxConcepto = txtConcepto.Text.Trim


                        .FeVencimiento = dtpFechaVencimiento.Value.Date.ToString("dd/MM/yyyy")
                        .CoTipoDocSAP = cboTipoSAP.SelectedValue
                        .CoFormaPago = strFormaPago

                        If gbxDocumentoVinculado.Visible = True Then
                            .CoTipoDoc_Ref = cboTipoDocumento_DocumVinc.SelectedValue
                            .NuSerie_Ref = txtSerie_Ref.Text.Trim
                            .NuDocum_Ref = txtNroDocumento_DocumVinc.Text.Trim
                            .FeEmision_Ref = dtpFechaEmision_Ref.Value.Date.ToString("dd/MM/yyyy")
                        Else
                            .FeEmision_Ref = CDate("01/01/1900").ToString("dd/MM/yyyy")
                        End If


                        .CoCeCon = cboCentroControl.SelectedValue
                        .CoGasto = cboGastos.SelectedValue
                        .CoUbigeo_Oficina = strCoUbigeo_Oficina
                        .UserMod = gstrUser
                        .UserNuevo = gstrUser
                        .AceptarPresupuestoTicket = dgvRow.Cells("chkAceptar").Value
                        .FlMultiple = True

                        .CoEstado = "AC"
                    End With
                    LstDocs.Add(objBEDoc)
                End If
            Next
            objBE.ListaDocumentos = LstDocs
            Dim objBT As New clsDocumentoProveedorBT
            'objBT.Insertar(objBE)
            'If strTipoVoucheruOrServ = "OTR" Then
            If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                objBT.Grabar_Multiple(objBE, False)
            Else
                objBT.Grabar_Multiple(objBE, True)
            End If

            'Else
            'objBT.Grabar(objBE, False)
            'End If
            'objBT.Grabar(objBE, False)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function bytGenerarCoDocumento() As Byte

        Try
            Dim bytCorrelativo As Byte = 0
            Dim objBN As New clsDocumentoProveedorBN
            Dim dtdoc As DataTable
            dtdoc = objBN.Consultar_Correlativo

            If dtdoc.Rows.Count > 0 Then
                bytCorrelativo = CInt(dtdoc.Rows(0).Item(0).ToString)
            End If

            Return bytCorrelativo
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Sub Actualizar()
        Try

            Dim objBE As New clsDocumentoProveedorBE
            With objBE

                .NuDocumProv = intNuDocumProv

                'If intNuOrden_Servicio > 0 Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = intNuOrden_Servicio
                'Else
                '    .NuVoucher = vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                'End If

                'If strTipoVoucheruOrServ = "VOU" Then
                '    .NuVoucher = vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = 0
                'ElseIf strTipoVoucheruOrServ = "OSV" Then
                '    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                '    .NuOrden_Servicio = intNuOrden_Servicio
                'End If

                If strTipoVoucheruOrServ = "VOU" Then
                    .NuVoucher = vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = 0
                ElseIf strTipoVoucheruOrServ = "OSV" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = intNuOrden_Servicio
                    .CoOrdPag = 0
                ElseIf strTipoVoucheruOrServ = "OPA" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = intNuOrden_Servicio
                ElseIf strTipoVoucheruOrServ = "ZIC" Then
                    .NuVoucher = 0 'vstrVoucher 'intNuVoucher
                    .NuOrden_Servicio = 0
                    .CoOrdPag = 0
                    .NuOrdComInt = 0
                    .NuPreSob = 0
                    .NuVouPTrenInt = 0
                    If IsNumeric(vstrVoucher) = True Then
                        .IDDocFormaEgreso = CInt(vstrVoucher)
                    End If
                    .CoMoneda_FEgreso = strMonedaFEgreso
                    '  objBEDocForEgr = frmRefE.ObtenerDocumentoFormaEgreso()
                End If

                .IDCab = intIDCab
                '.NuVoucher = vstrVoucher 'intNuVoucher
                '.NuOrden_Servicio = 0
                .NuSerie = txtSerie.Text.Trim.Replace("-", "")
                .NuDocum = txtNuDocumento.Text.Trim.Replace("-", "")
                .CoTipoDoc = cboTipoDocumento.SelectedValue
                .FeEmision = dtpFechaEmision.Value.Date.ToString("dd/MM/yyyy")
                .FeRecepcion = dtpFechaRecepcion.Value.Date.ToString("dd/MM/yyyy")
                .CoTipoDetraccion = cboTipoDetraccion.SelectedValue
                .CoMoneda = cboMoneda.SelectedValue
                .CoMoneda_Pago = cboMoneda.SelectedValue 'cboMoneda_Pago.SelectedValue
                .SsIGV = IIf(txtIGV.Text.Trim.Length > 0, txtIGV.Text.Trim, "0")
                .SsNeto = IIf(txtSsNeto.Text.Trim.Length > 0, txtSsNeto.Text.Trim, "0")
                .SsOtrCargos = IIf(txtSsOtrCargos.Text.Trim.Length > 0, txtSsOtrCargos.Text.Trim, "0")
                .SsDetraccion = IIf(txtSsDetraccion.Text.Trim.Length > 0, txtSsDetraccion.Text.Trim, "0")
                .SSTotalOriginal = IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")

                'If chkTipoCambio.Checked = True Then
                '    .SSTipoCambio = IIf(txtTipoCambio.Text.Trim.Length > 0, txtTipoCambio.Text.Trim, "0")
                '    .SSTotal = IIf(txtTotalTC.Text.Trim.Length > 0, txtTotalTC.Text.Trim, "0")
                'Else
                .SSTipoCambio = 0
                .SSTotal = IIf(txtMonto.Text.Trim.Length > 0, txtMonto.Text.Trim, "0")
                'End If

                .CoCecos = cboCentroCostos.SelectedValue
                .CoTipoOC = cboTipoOperacion.SelectedValue
                .CoCtaContab = cboCuentaContable.SelectedValue
                .CoObligacionPago = strTipoVoucheruOrServ
                ' .NuMaxHorasSemana = IIf(txtNuMaxHorasSemana.Text.Trim.Length > 0, CInt(txtNuMaxHorasSemana.Text.Trim), 0)
                '.CoEstado = cboEstado.SelectedValue
                '.IngresoManual = True
                .SsTipoCambio_FEgreso = Convert.ToDouble(If(txtTipoCambioFEgreso.Text.Trim = "", "0", txtTipoCambioFEgreso.Text.Trim))
                .SsTotal_FEgreso = Convert.ToDouble(txtTotalFEgreso.Text.Trim)

                .CoProveedor = vstrIDProveedor 'strCoProveedorAdminist
                If strTipoVoucheruOrServ = "PRL" Then
                    .CoProveedor = vstrIDProveedor 'Peru Rail
                End If
                .CoUbigeo_Oficina = strCoUbigeo_Oficina
                .UserMod = gstrUser

                '-----
                '@CoTipoFondoFijo=Case When Ltrim(Rtrim(@CoTipoFondoFijo))='' Then Null Else @CoTipoFondoFijo End    ,
                '@SSPercepcion =Case When @SSPercepcion=0 Then Null Else @SSPercepcion End,
                '@TxConcepto=Case When Ltrim(Rtrim(@TxConcepto))='' Then Null Else @TxConcepto End    ,
                '@CoProveedor= Case When Ltrim(Rtrim(@CoProveedor))='' Then Null Else @CoProveedor End    ,
                '@IDCab = Case When @IDCab=0 Then Null Else @IDCab End,
                '@FeVencimiento = Case When @FeVencimiento='01/01/1900' Then getdate() Else @FeVencimiento End,
                '@CoTipoDocSAP = Case When @CoTipoDocSAP=0 Then Null Else @CoTipoDocSAP End,
                '@CoFormaPago = Case When Ltrim(Rtrim(@CoFormaPago))='' Then Null Else @CoFormaPago End    ,
                '@CoTipoDoc_Ref = Case When Ltrim(Rtrim(@CoTipoDoc_Ref))='' Then Null Else @CoTipoDoc_Ref End    ,
                '@NuSerie_Ref = Case When Ltrim(Rtrim(@NuSerie_Ref))='' Then Null Else @NuSerie_Ref End    ,
                '@NuDocum_Ref = Case When Ltrim(Rtrim(@NuDocum_Ref))='' Then Null Else @NuDocum_Ref End    ,
                '@FeEmision_Ref = Case When @FeEmision_Ref='01/01/1900' Then getdate() Else @FeEmision_Ref End,
                '@CoCeCon = Case When Ltrim(Rtrim(@CoCeCon))='' Then Null Else @CoCeCon End    ,
                '@CoGasto  = Case When Ltrim(Rtrim(@CoGasto))='' Then Null Else @CoGasto End 

                .SSPercepcion = txtPercepcion.Text.Trim
                .TxConcepto = txtConcepto.Text.Trim
                '.CoProveedor = strCoProveedorAdminist
                .IDCab = intIDCab
                .IDCab_FF = intIDCab
                .FeVencimiento = dtpFechaVencimiento.Value.Date.ToString("dd/MM/yyyy")
                .CoTipoDocSAP = cboTipoSAP.SelectedValue
                .CoFormaPago = strFormaPago
                If gbxDocumentoVinculado.Visible = True Then
                    .CoTipoDoc_Ref = cboTipoDocumento_DocumVinc.SelectedValue
                    .NuSerie_Ref = txtSerie_Ref.Text.Trim
                    .NuDocum_Ref = txtNroDocumento_DocumVinc.Text.Trim
                    .FeEmision_Ref = dtpFechaEmision_Ref.Value.Date.ToString("dd/MM/yyyy")
                Else
                    .FeEmision_Ref = CDate("01/01/1900").ToString("dd/MM/yyyy")
                End If
                .CoCeCon = cboCentroControl.SelectedValue
                .CoGasto = cboGastos.SelectedValue

                'Si es Setours bs aires, mandar parametros en blanco
                'If strCoUbigeo_Oficina = gstrBuenosAires Then
                If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                    .CoTipoDetraccion = "00000"
                    .SsOtrCargos = 0
                    .SsDetraccion = 0
                    .CoCecos = ""
                    .CoTipoOC = ""
                    .CoCtaContab = ""
                    .SSPercepcion = 0
                    .CoCeCon = ""
                    .CoGasto = ""
                End If

            End With



            Dim objBEIti As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
            Dim LstDetalle As New List(Of clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)
            For Each ST As stDocumento_Detalle In objLstDocumento_Detalle
                objBEIti = New clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
                With objBEIti

                    .NuDocumProv = intNuDocumProv 'bytGenerarCoDocumento()
                    .NuDocumProvDet = ST.NuDocumProvDet
                    .IDServicio_Det = 0 'ST.IDServicio_Det
                    .TxServicio = ""
                    .QtCantidad = ST.QtCantidad
                    .SsMonto = ST.SsMonto
                    .Accion = ST.strAccion
                    .UserMod = gstrUser
                    .IDCab = intIDCab
                    .CoProducto = ST.CoProducto
                    .CoAlmacen = ST.CoAlmacen
                    .SsIGV = ST.SsIGV
                    .SSPrecUni = ST.SSPrecUni
                    .SsTotal = ST.SSTotal

                End With
                LstDetalle.Add(objBEIti)
            Next
            objBE.ListaDetalle = LstDetalle



            'Dim objBEIti As clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
            'Dim LstDetalle As New List(Of clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE)
            'For Each ST As stDocumento_Detalle In objLstDocumento_Detalle
            '    objBEIti = New clsDocumentoProveedorBE.clsDocumentoProveedor_DetBE
            '    With objBEIti

            '        '.Add("@NuDocumProv", BE.NuDocumProv)
            '        '.Add("@IDServicio_Det", BE.IDServicio_Det)
            '        '.Add("@TxServicio", BE.TxServicio)
            '        '.Add("@SsTotalOriginal", BE.SsTotalOriginal)
            '        '.Add("@UserMod", BE.UserMod)

            '        .NuDocumProv = objBE.NuDocumProv
            '        .NuDocumProvDet = ST.NuDocumProvDet
            '        .IDServicio_Det = 0 'ST.IDServicio_Det
            '        .TxServicio = ST.TxServicio
            '        .QtCantidad = ST.QtCantidad
            '        .SsTotalOriginal = ST.SsTotalOriginal
            '        .Accion = ST.strAccion
            '        .UserMod = gstrUser
            '    End With
            '    LstDetalle.Add(objBEIti)
            'Next
            'objBE.ListaDetalle = LstDetalle


            Dim objBT As New clsDocumentoProveedorBT
            objBT.Actualizar(objBE)
            frmRefE.blnCambiosDato = True
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarControles()
        Try

            'CargarDocumentos()

            Using objBL As New clsDocumentoProveedorBN
                'Dim dt As DataTable = objBL.ConsultarPk_Validar(strCoTicket)
                Using dr As SqlClient.SqlDataReader = objBL.ConsultarPk(intNuDocumProv)
                    dr.Read()
                    If dr.HasRows Then
                        lblNroInterno.Text = dr("NuDocInterno").ToString.Trim
                        'If strTipoVoucheruOrServ = "VOU" Then
                        '    lblNuVoucher.Text = dr("NuVoucher").ToString.Trim
                        'ElseIf strTipoVoucheruOrServ = "OSV" Then
                        '    lblNuVoucher.Text = dr("CoOrden_Servicio").ToString.Trim
                        'End If
                        lblNuVoucher.Text = vstrVoucher 'intNuVoucher

                        'Dim strNudoc As String = dr("NuDocum").ToString.Trim
                        'If strNudoc.Trim.Length > 3 Then
                        '    If strNudoc.Trim.Length <= 10 Then
                        '        txtNuDocumento.Text = strNudoc.Insert(3, "-")
                        '    Else

                        '        txtNuDocumento.Text = strNudoc.Insert(4, "-")
                        '    End If
                        'Else
                        '    txtNuDocumento.Text = dr("NuDocum").ToString.Trim
                        'End If
                        'txtNuDocumento.Text = strFormatearDocumProvee(strNudoc)
                        'txtNuDocumento.Text = dr("NuDocumFormat").ToString.Trim
                        txtNuDocumento.Text = dr("NuDocum").ToString.Trim
                        txtSerie.Text = dr("NuSerie").ToString.Trim
                        cboTipoDocumento.SelectedValue = If(IsDBNull(dr("CoTipoDoc")) = True, "", dr("CoTipoDoc"))
                        CargarGastos()
                        chkFechaEmision.Checked = True
                        dtpFechaEmision.Text = dr("FeEmision").ToString.Trim
                        dtpFechaRecepcion.Text = dr("FeRecepcion").ToString.Trim
                        'cboTipoDetraccion.SelectedValue = If(IsDBNull(dr("CoTipoDetraccion")) = True, "", dr("CoTipoDetraccion"))

                        'If strCoUbigeo_Oficina <> gstrBuenosAires Then
                        If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                            cboTipoDetraccion.SelectedValue = dr("CoTipoDetraccion").ToString.Trim
                            txtSsDetraccion.Text = Format(Math.Round(CDbl(IIf(IsDBNull(dr("SsDetraccion")) = True, "0", dr("SsDetraccion"))), 2, MidpointRounding.AwayFromZero), "###,##0.00")
                            txtSsOtrCargos.Text = Format(CDbl(IIf(IsDBNull(dr("SsOtrCargos")) = True, "0", dr("SsOtrCargos"))), "###,##0.00") 'Format(CDbl(dr("SsOtrCargos")), "###,##0.00")

                            If IsDBNull(dr("CoCeCos")) = True Then
                                cboCentroCostos.Text = ""
                            Else
                                cboCentroCostos.SelectedValue = If(IsDBNull(dr("CoCeCos")) = True, "", dr("CoCeCos"))
                            End If

                            If IsDBNull(dr("CoGasto")) = True Then
                                cboGastos.Text = ""
                            Else
                                cboGastos.SelectedValue = If(IsDBNull(dr("CoGasto")) = True, "", dr("CoGasto"))
                            End If

                            'CargarCombo_TiposdeOperacion_CeCos()

                            If IsDBNull(dr("CoTipoOC")) = True Then
                                cboTipoOperacion.Text = ""
                            Else
                                cboTipoOperacion.SelectedValue = If(IsDBNull(dr("CoTipoOC")) = True, "", dr("CoTipoOC"))
                            End If


                        End If


                        cboMoneda.SelectedValue = dr("CoMoneda")
                        'cboMoneda_Pago.SelectedValue = dr("CoMoneda_Pago")
                        strMonedaFEgreso = If(IsDBNull(dr("CoMoneda_FEgreso")) = True, "", dr("CoMoneda_FEgreso").ToString)
                        txtTipoCambioFEgreso.Text = Format(If(IsDBNull(dr("SsTipoCambio_FEgreso")) = True, 0, CDbl(dr("SsTipoCambio_FEgreso"))), "###,##0.000")
                        txtTotalFEgreso.Text = Format(If(IsDBNull(dr("SsTotal_FEgreso")) = True, 0, CDbl(dr("SsTotal_FEgreso"))), "###,##0.00")

                        txtSsNeto.Text = Format(CDbl(dr("SsNeto")), "###,##0.00")
                        'txtSsDetraccion.Text = Format(CDbl(IIf(IsDBNull(dr("SsDetraccion")) = True, "0", dr("SsDetraccion"))), "###,##0.00")

                        txtIGV.Text = Format(CDbl(dr("SsIGV")), "###,##0.00")
                        Dim dblIGV As Double = CDbl(dr("SsIGV"))
                        If dblIGV > 0 Then
                            chkIGV.Checked = True
                        Else
                            chkIGV.Checked = False
                        End If

                        'If Not (strCoUbigeo_Oficina = gstrBuenosAires) Then
                        If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                            CambiarRetencion()
                        End If


                        CambioMoneda()
                        txtIGV.Text = Format(CDbl(dr("SsIGV")), "###,##0.00")
                        txtMonto.Text = Format(CDbl(dr("SsTotalOriginal")), "###,##0.00")
                        'txtTipoCambio.Text = Format(CDbl(IIf(IsDBNull(dr("SSTipoCambio")) = True, "0", dr("SSTipoCambio"))), "###,##0.000")
                        'txtTotalTC.Text = Format(CDbl(IIf(IsDBNull(dr("SsTotal")) = True, "0", dr("SsTotal"))), "###,##0.00")
                        'Dim dblTipoCambio As Double = CDbl(txtTipoCambio.Text)
                        'If dblTipoCambio > 0 Then
                        '    chkTipoCambio.Checked = True
                        '    grbTipoCambio.Enabled = True
                        'Else
                        '    chkTipoCambio.Checked = False
                        '    grbTipoCambio.Enabled = False
                        'End If

                        ''CargarCombo_TipoOperacion()




                        If strTipoVoucheruOrServ = "OCP" Then
                            strCoProveedorAdminist = dr("CoProveedorOC")
                            lblProveedor.Text = dr("NoProveedorOC")
                            blnEsProvAdmin = True
                        End If

                        If strTipoVoucheruOrServ = "PST" Or strTipoVoucheruOrServ = "PTT" Then
                            strCoProveedorAdminist = dr("CoProveedorOC")
                            lblProveedor.Text = dr("NoProveedorOC")
                            'blnEsProvAdmin = True
                        End If

                        'CargarCombo_CuentasContables()
                        CargarCombo_CuentasContables_CeCos()
                        If cboCuentaContable.Items.Count > 0 Then
                            cboCuentaContable.SelectedValue = dr("CoCtaContab")
                        End If

                        'If strTipoVoucheruOrServ = "OCP" Then
                        '    strCoProveedorAdminist = dr("CoProveedorOC")
                        '    lblProveedor.Text = dr("NoProveedorOC")
                        '    blnEsProvAdmin = True
                        'End If

                        ValidarCheckRetencion()

                        'Dim dblIGV As Double = CDbl(dr("SsIGV"))

                        pCalcularTotal()

                        If dblIGV > 0 Then
                            chkIGV.Checked = True
                        Else
                            chkIGV.Checked = False
                        End If
                        txtIGV.Text = Format(CDbl(dr("SsIGV")), "###,##0.00")
                        txtMonto.Text = Format(CDbl(dr("SSTotal")), "###,##0.00")

                        'cboEstado.SelectedValue = If(IsDBNull(dr("CoEstado")) = True, "", dr("CoEstado"))

                        'If strCoUbigeo_Oficina <> gstrBuenosAires Then
                        If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                            txtSsDetraccion.Text = Format(Math.Round(CDbl(IIf(IsDBNull(dr("SsDetraccion")) = True, "0", dr("SsDetraccion"))), 2, MidpointRounding.AwayFromZero), "###,##0.00")
                            'CargarDetalleDocumentos()

                            txtPercepcion.Text = dr("SSPercepcion")
                        End If

                        txtConcepto.Text = dr("TxConcepto")

                        dtpFechaVencimiento.Text = If(IsDBNull(dr("FeVencimiento")) = True, "01/01/1900", dr("FeVencimiento"))

                        Dim strTipoSAP As String = ""
                        strTipoSAP = If(IsDBNull(dr("CoTipoDocSAP")) = True, "", dr("CoTipoDocSAP"))

                        If strTipoSAP = "2" Then
                            If strTipoVoucheruOrServ = "VOU" Then
                                strTipoSAP = "1"
                            End If
                        End If

                        cboTipoSAP.SelectedValue = strTipoSAP 'If(IsDBNull(dr("CoTipoDocSAP")) = True, "", dr("CoTipoDocSAP"))

                        '@CoFormaPago char(3)='',
                        cboTipoDocumento_DocumVinc.SelectedValue = If(IsDBNull(dr("CoTipoDoc_Ref")) = True, "", dr("CoTipoDoc_Ref"))
                        '@NuSerie_Ref varchar(10)='',
                        txtSerie_Ref.Text = If(IsDBNull(dr("NuSerie_Ref")) = True, "", dr("NuSerie_Ref"))
                        '@NuDocum_Ref varchar(30)='',
                        txtNroDocumento_DocumVinc.Text = If(IsDBNull(dr("NuDocum_Ref")) = True, "", dr("NuDocum_Ref"))
                        '@FeEmision_Ref datetime='01/01/1900',
                        dtpFechaEmision_Ref.Text = If(IsDBNull(dr("FeEmision_Ref")) = True, "01/01/1900", dr("FeEmision_Ref"))
                        '@CoCeCon char(6)=''
                        'If strCoUbigeo_Oficina <> gstrBuenosAires Then
                        If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                            cboCentroControl.SelectedValue = If(IsDBNull(dr("CoCeCon")) = True, "", dr("CoCeCon"))
                        End If


                        Dim intIDCab_tmp As Integer = If(IsDBNull(dr("IDCab")) = True, 0, dr("IDCab"))
                        If intIDCab_tmp <> 0 Then
                            intIDCab = If(IsDBNull(dr("IDCab")) = True, 0, dr("IDCab"))
                        End If

                        'intIDCab = If(IsDBNull(dr("IDCab")) = True, 0, dr("IDCab"))

                        'If strCoUbigeo_Oficina <> gstrBuenosAires Then
                        If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                            If strCoSAP <> "" Then
                                If cboCuentaContable.Text = "" Then
                                    CargarCombo_CuentasContables_Doc_Pk(dr("CoCtaContab").ToString)
                                End If
                            End If
                        End If


                        If strTipoVoucheruOrServ = "PTT" Then
                            cboFormaEgreso.SelectedValue = strTipoVoucheruOrServ
                            cboTipoSAP_SelectedValueChanged(cboTipoSAP, Nothing)
                            CargarVouchersxProveedor()
                        End If


                    Else
                        MessageBox.Show("El registro se ha eliminado.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                    dr.Close()
                End Using
            End Using

            CargarDetalleDocumento_Texto()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarCombo_CuentasContables_Doc_Pk(vstrCtaContable As String)
        Try

            Dim objBN As New clsDocumentoProveedorBN
            Dim dttCuentasContables As DataTable = New DataTable
            'dttCuentasContables = objBN.ConsultarCtaContable_CentroCostos_Servicios(strCecos)
            'dttCuentasContables = objBN.ConsultarCtaContable_CentroCostos_Facturacion(strCecos)
            dttCuentasContables = objBN.ConsultarCtaContable_Doc_Sel_Cbo_Pk(vstrCtaContable)
            'pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable(strTipoOpe), False)
            pCargaCombosBox(cboCuentaContable, dttCuentasContables, False)
            If cboCuentaContable.Items.Count = 1 Then
                cboCuentaContable.SelectedIndex = 0
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub pCalcularTotal(Optional ByVal vblnHTD As Boolean = False, Optional ByVal vblnMultiple As Boolean = False)
        Try
            Dim dblTotal As Double = 0
            Dim dblTotal_Tmp As Double = 0
            Dim dblNeto_Tmp As Double = 0
            Dim dblNeto As Double = 0
            Dim dblIGV As Double = 0
            Dim dblIGV_Tmp As Double = 0
            Dim dblOtrosCargos As Double = 0
            Dim dblPorcentajeRenta As Double = 10
            Dim dblPorcentajeRenta2 As Double = 0
            Dim dblPercepcion As Double = 0

            If txtSsNeto.Text.Trim.Length > 0 Then
                dblNeto = CDbl(txtSsNeto.Text)
            End If

            'If strCoUbigeo_Oficina <> gstrBuenosAires Then
            If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                If txtSsOtrCargos.Text.Trim.Length > 0 Then
                    dblOtrosCargos = CDbl(txtSsOtrCargos.Text)
                End If
            End If


            If txtIGV.Text.Trim.Length > 0 Then
                'dblIGV = CDbl(txtIGV.Text)
                dblIGV_Tmp = CDbl(txtIGV.Text)
            End If

            'If vblnMultiple Then
            '    dblNeto_Tmp = dblNeto - dblOtrosCargos
            '    txtSsNeto.Text = Format(dblNeto, "###,##0.00")
            'End If

            'If strCoUbigeo_Oficina <> gstrBuenosAires Then
            If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                If txtPercepcion.Text.Trim.Length > 0 Then
                    dblPercepcion = CDbl(txtPercepcion.Text)
                End If
            End If


            If chkIGV.Text = "IGV:" Then
                If strTipOpe = "001" Or strTipOpe = "012" Or strTipOpe = "002" Then
                    chkIGV.Checked = True
                Else

                    chkIGV.Checked = False
                End If
            End If

            If chkIGV.Text = "IVA (21%):" Then
                'If strTipOpe = "001" Or strTipOpe = "012" Or strTipOpe = "002" Then
                If dblIGV_Tmp > 0 Then
                    chkIGV.Checked = True
                End If

            End If



            If cboTipoSAP.Items.Count > 0 Then
                If cboTipoSAP.SelectedValue = "0" Then
                    ''igv ahora se calcula x detalle si es producto
                    If txtIGV.Text.Trim.Length > 0 Then
                        dblIGV = CDbl(txtIGV.Text)
                    End If
                Else
                    'Dim objBN As New clsTablasApoyoBN.clsParametroBN
                    'Dim dtParametros As New DataTable
                    'dtParametros = objBN.ConsultarList()
                    'If dtParametros.Rows.Count > 0 Then
                    '    If chkIGV.Checked = True Then
                    '        dblIGV = CDbl(dtParametros.Rows(0).Item("NuIGV").ToString)
                    '        dblIGV = dblNeto * (dblIGV / 100)
                    '    End If
                    'End If
                    If chkIGV.Checked = True Then
                        If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                            dblIGV = dblNeto * (gsglIGV / 100)
                        Else
                            If strCoUbigeo_Oficina = gstrBuenosAires Then
                                dblIGV = dblNeto * (dblIVA_Arg / 100)
                            ElseIf strCoUbigeo_Oficina = gstrSantiago Then
                                dblIGV = dblNeto * (dblIVA_Chile / 100)
                            Else
                                dblIGV = CDbl(txtIGV.Text)
                            End If
                        End If
                    End If
                End If
            End If


            dblTotal = dblNeto + dblIGV + dblOtrosCargos + dblPercepcion

            'validar el tipo de documento y tipo de operacion
            'If strTipoDoc.Trim = "RH" Then '0And strTipOpe.Trim = "007" Then
            If (strTipoDoc.Trim = "RH" Or strTipoDoc = "HTD") Then '0And strTipOpe.Trim = "007" Then
                'If strCoUbigeo_Oficina <> gstrBuenosAires Then
                If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                    If cboTipoOperacion.SelectedIndex > -1 Then
                        Dim strDestOpe As String = cboTipoOperacion.SelectedValue.ToString.Trim
                        'obtener porcentaje de retencion segun el destino de operación
                        Dim objDO_BN As New clsTablasApoyoBN.clsTipoOperacContabBN
                        'Using dr As SqlClient.SqlDataReader = objDO_BN.ConsultarPk_DestOper(strDestOpe)
                        Using dr As SqlClient.SqlDataReader = objDO_BN.ConsultarPk(strDestOpe)
                            dr.Read()
                            If dr.HasRows Then
                                dblPorcentajeRenta = CDbl(dr("SsTasa").ToString.Trim)
                                dblPorcentajeRenta2 = CDbl(dr("SsTasa").ToString.Trim)
                            Else

                            End If
                            dr.Close()
                        End Using

                        If dblPorcentajeRenta = 0 Then
                            dblPorcentajeRenta = 10
                        End If

                        'retencion

                        If cboMoneda.SelectedValue = gstrTipoMonedaSol Then
                            If dblNeto >= gdblRHLimiteSinRetencion Then
                                'le corresponde impuesto a la renta
                                If chkIGV.Checked = True Then
                                    dblIGV = dblNeto * (dblPorcentajeRenta / 100)
                                Else
                                    dblIGV = 0
                                End If

                            Else
                                If dblPorcentajeRenta2 > 0 Then
                                    dblIGV = dblNeto * (dblPorcentajeRenta / 100)
                                Else
                                    dblIGV = 0
                                End If

                            End If
                        Else
                            If chkIGV.Checked = True Then
                                dblIGV = dblNeto * (dblPorcentajeRenta / 100)
                            Else
                                dblIGV = 0
                            End If
                        End If

                        'If dblNeto >= gdblRHLimiteSinRetencion Then
                        '    'le corresponde impuesto a la renta
                        '    If chkIGV.Checked = True Then
                        '        dblIGV = dblNeto * (dblPorcentajeRenta / 100)
                        '    Else
                        '        dblIGV = 0
                        '    End If

                        'Else
                        '    dblIGV = 0
                        'End If

                        If vblnHTD Then
                            If txtIGV.Text.Trim.Length > 0 Then
                                dblIGV = CDbl(txtIGV.Text.Trim)
                            Else
                                dblIGV = 0
                            End If

                        End If

                        dblTotal = dblNeto - dblIGV + dblOtrosCargos
                    End If
                Else
                    'argentina
                    If cboMoneda.SelectedValue = gstrTipoMonedaArg Then
                        If dblNeto >= dblRHLimiteSinRetencion_Arg Then
                            'le corresponde impuesto a la renta
                            If chkIGV.Checked = True Then
                                dblIGV = dblNeto * (2 / 100)
                            Else
                                dblIGV = 0
                            End If

                        Else
                            If dblPorcentajeRenta2 > 0 Then
                                dblIGV = dblNeto * (2 / 100)
                            Else
                                dblIGV = 0
                            End If

                        End If
                        'chile
                    ElseIf cboMoneda.SelectedValue = gstrTipoMonedaChi Then
                        If dblNeto >= dblRHLimiteSinRetencion_Chile Then
                            'le corresponde impuesto a la renta
                            If chkIGV.Checked = True Then
                                dblIGV = dblNeto * (10 / 100)
                            Else
                                dblIGV = 0
                            End If

                        Else
                            If dblPorcentajeRenta2 > 0 Then
                                dblIGV = dblNeto * (2 / 100)
                            Else
                                dblIGV = 0
                            End If

                        End If
                    Else
                        If chkIGV.Checked = True Then
                            dblIGV = dblNeto * (2 / 100)
                        Else
                            dblIGV = 0
                        End If
                    End If
                    ' dblTotal = dblNeto + dblIGV + dblOtrosCargos + dblPercepcion
                    dblTotal = dblNeto - dblIGV + dblOtrosCargos
                End If



            End If



            'dblTotal = dblNeto + dblIGV + dblOtrosCargos

            'txtSSTotal.Text = dblTotal
            'If Not (vblnHTD Or vblnMultiple Or blnEsMultiple()) Then
            If Not (vblnHTD) Then
                txtIGV.Text = Format(dblIGV, "###,##0.00")
            End If

            'If Not vblnMultiple Then
            '    txtIGV.Text = Format(dblIGV, "###,##0.00")
            'End If

            If vblnMultiple Then
                'If vblnMultiple Then
                calculalTotalMarcados()

                If txtSsNeto.Text.Trim.Length > 0 Then
                    dblNeto = CDbl(txtSsNeto.Text)
                End If

                If dblIGV_Multiple <> dblIGV_Tmp Then
                    dblIGV = dblIGV_Tmp
                Else
                    dblIGV = dblIGV_Multiple
                End If

                dblNeto_Tmp = dblNeto - dblOtrosCargos - dblIGV
                dblTotal = dblNeto
                txtSsNeto.Text = Format(dblNeto_Tmp, "###,##0.00")


                If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                    txtIGV.Text = Format(dblIGV_Multiple, "###,##0.00")
                End If


            End If

            txtMonto.Text = Format(dblTotal, "###,##0.00")


            pCalcularTipoCambio()

            If cboTipoDetraccion.Items.Count > 0 Then
                cboTipoDetraccion_SelectedValueChanged(Nothing, Nothing)
            End If
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub CargarDetalleDocumentos()
        'Try
        '    dgvDetalle.Rows.Clear()

        '    Dim objBL As New clsDocumentoProveedor_DetBN
        '    Dim dtDet As Data.DataTable = objBL.ConsultarList(strCoTicket, 0)

        '    For Each drDet As DataRow In dtDet.Rows
        '        dgvDetalle.Rows.Add()
        '        For i As Byte = 0 To dtDet.Columns.Count - 1
        '            dgvDetalle.Item(i, dgvDetalle.Rows.Count - 1).Value = drDet(i)
        '        Next
        '    Next

        '    blnCargaIniStruct = True
        '    For Each dgvItem As DataGridViewRow In dgvDetalle.Rows
        '        AgregarEditarEliminarItinerario(dgvItem.Index, "")
        '    Next
        '    blnCargaIniStruct = False

        '    'CargarTamanioDgvDet()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub CargarDocumentos()
        Try
            'dgvDocumentos.Rows.Clear()

            'Dim objBL As New clsDocumentoProveedorBN
            'Dim dtDet As Data.DataTable = objBL.ConsultarList(0, intNuVoucher, "", "", "")

            'For Each drDet As DataRow In dtDet.Rows
            '    dgvDocumentos.Rows.Add()
            '    For i As Byte = 0 To dtDet.Columns.Count - 1
            '        dgvDocumentos.Item(i, dgvDocumentos.Rows.Count - 1).Value = drDet(i)
            '    Next
            'Next

            'blnCargaIniStruct = True
            'For Each dgvItem As DataGridViewRow In dgvDocumentos.Rows
            '    AgregarEditarEliminarItinerario_Data(dgvItem.Index, "")
            'Next
            'blnCargaIniStruct = False
            'pDgvAnchoColumnas(dgvDocumentos)
            ''CargarTamanioDgvDet()
            'If dgvDocumentos.Rows.Count > 0 Then
            '    dgvDocumentos.Rows(0).Selected = True
            '    'dgvItems_CellEnter(Nothing, Nothing)
            '    pItems_VerDetalle()
            'End If

            'BloquearDesbloquear_Detalle()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarDetalleItemsNota(ByVal intIndexItem As Integer)
        Try
            ''pasar de la lista a la grilla
            'dgvDetalle.Rows.Clear()
            'Dim index As Integer = 0
            'Dim intNuDocumProv As Integer = CInt(dgvDocumentos.Rows(intIndexItem).Cells("NuDocumProv").Value.ToString)

            'For i As Integer = 0 To objLstNotaVentaCounter_Item.Count - 1
            '    If intNuItem = objLstNotaVentaCounter_Item(i).NuItem Then
            '        index = i
            '    End If
            'Next

            'Dim objItem As stNotaVentaCounter_Item = objLstNotaVentaCounter_Item(index)

            'For i As Integer = 0 To objItem.LstLista_DetalleItems.Count - 1
            '    If objItem.LstLista_DetalleItems(i).strAccion <> "B" Then

            '        dgvDetalle.Rows.Add()

            '        Dim vintIndex As Integer = dgvDetalle.Rows.Count - 1
            '        With dgvDetalle
            '            .Item("NuDetalle", vintIndex).Value = objItem.LstLista_DetalleItems(i).NuDetalle

            '            .Item("NuDocumento", vintIndex).Value = objItem.LstLista_DetalleItems(i).NuDocumento

            '            '.Item("CoMoneda1", vintIndex).Value = objItem.LstLista_DetalleItems(i).CoMoneda
            '            '.Item("Descripcion1", vintIndex).Value = objItem.LstLista_DetalleItems(i).Descripcion

            '            '.Item("SSTipoCambio1", vintIndex).Value = objItem.LstLista_DetalleItems(i).SSTipoCambio
            '            .Item("CoSegmento", vintIndex).Value = objItem.LstLista_DetalleItems(i).CoSegmento
            '            .Item("SSCosto", vintIndex).Value = objItem.LstLista_DetalleItems(i).SSCosto
            '            .Item("SSComision", vintIndex).Value = objItem.LstLista_DetalleItems(i).SSComision
            '            .Item("SSValor", vintIndex).Value = objItem.LstLista_DetalleItems(i).SSValor
            '            .Item("SSFEE_Emision", vintIndex).Value = objItem.LstLista_DetalleItems(i).SSFEE_Emision
            '            .Item("SSVenta", vintIndex).Value = objItem.LstLista_DetalleItems(i).SSVenta
            '            .Item("SSUtilidad", vintIndex).Value = objItem.LstLista_DetalleItems(i).SSUtilidad

            '        End With
            '    End If



            'Next


            'pDgvAnchoColumnas(dgvDetalle)
            ''CargarTamanioDgvDet()
        Catch ex As Exception
            Throw
        End Try
    End Sub



    Public Sub AgregarEditarEliminarItinerario(ByVal vbytIndex As Byte, ByVal vstrAccion As String)
        Dim strAccion As String = "" 'N,B,M
        Dim bytCoDetalle As Byte = 0
        Try
            dgvDetalle.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
            bytCoDetalle = dgvDetalle.Item("NuDocumProvDet", vbytIndex).Value

            Dim stReg As stDocumento_Detalle
            Dim intInd As Int16
            For Each ST As stDocumento_Detalle In objLstDocumento_Detalle
                If ST.NuDocumProvDet = bytCoDetalle Then
                    If ST.strAccion <> "N" Then
                        strAccion = "M"
                    Else
                        strAccion = "N"
                    End If
                    objLstDocumento_Detalle.RemoveAt(intInd)
                    Exit For
                End If
                intInd += 1
            Next

            If vstrAccion <> "B" Then
            Else
                strAccion = vstrAccion
            End If

            stReg = New stDocumento_Detalle
            With stReg

                '                dd.NuDocumProvDet,
                'dd.CoProducto,
                'Producto=isnull((select NoProducto from MAPRODUCTOS where CoProducto=dd.coproducto),''),
                'dd.coalmacen,
                'Almacen=isnull((select NoAlmacen from MAALMACEN where coalmacen=dd.coalmacen),''),
                'dd.CoCtaContab,
                'CuentaContable = isnull((select CtaContable+' - '+ Descripcion from MAPLANCUENTAS where CtaContable=dd.CoCtaContab),''),
                'QtCantidad=isnull(dd.QtCantidad,0),
                'SSPrecUni=isnull(dd.SSPrecUni,0),
                'SsIGV=isnull(dd.SsIGV,0),
                'SsMonto=isnull(dd.SsMonto,0),
                '                SsTotal = isnull(dd.SsTotal, 0)



                'Dim _QtCantidad As Byte
                'Dim _IDCab As Integer
                'Dim _CoProducto As String
                'Dim _CoAlmacen As String
                'Dim _SsIGV As Decimal
                'Dim _CoTipoOC As String
                'Dim _CoCtaContab As String
                'Dim _CoCeCos As String
                'Dim _CoCeCon As String
                'Dim _SSPrecUni As Decimal
                'Dim _SSTotal As Decimal
                'Dim _Accion As String

                .NuDocumProvDet = bytCoDetalle
                If Not dgvDetalle.Item("CoProducto", vbytIndex).Value Is Nothing Then
                    .CoProducto = dgvDetalle.Item("CoProducto", vbytIndex).Value.ToString
                End If

                If Not dgvDetalle.Item("CoAlmacen", vbytIndex).Value Is Nothing Then
                    .CoAlmacen = dgvDetalle.Item("CoAlmacen", vbytIndex).Value.ToString
                End If

                If Not dgvDetalle.Item("SsMonto", vbytIndex).Value Is Nothing Then
                    .SsMonto = dgvDetalle.Item("SsMonto", vbytIndex).Value.ToString
                End If

                If Not dgvDetalle.Item("QtCantidad", vbytIndex).Value Is Nothing Then
                    .QtCantidad = dgvDetalle.Item("QtCantidad", vbytIndex).Value.ToString
                End If

                If Not dgvDetalle.Item("SsIGV", vbytIndex).Value Is Nothing Then
                    .SsIGV = dgvDetalle.Item("SsIGV", vbytIndex).Value.ToString
                End If

                If Not dgvDetalle.Item("SSPrecUni", vbytIndex).Value Is Nothing Then
                    .SSPrecUni = dgvDetalle.Item("SSPrecUni", vbytIndex).Value.ToString
                End If

                If Not dgvDetalle.Item("SSTotal", vbytIndex).Value Is Nothing Then
                    .SSTotal = dgvDetalle.Item("SSTotal", vbytIndex).Value.ToString
                End If

                If strAccion = "" Then
                    If Not blnCargaIniStruct Then
                        .strAccion = "N"
                    Else
                        .strAccion = strAccion
                    End If
                Else
                    .strAccion = strAccion
                End If
            End With
            objLstDocumento_Detalle.Add(stReg)
        Catch ex As Exception
            Throw
        End Try
    End Sub




    Private Sub BloquearSelectFile()
        'Try
        '    btnConsultarCliente.Enabled = False
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub


    'Private Sub Validaciones_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lblDescCliente.TextChanged, _
    '     txtIndVenta.TextChanged, txtImporte.TextChanged, txtCoIATA.TextChanged, _
    '    txtCoLAServicio.TextChanged, txtCodLARecord.TextChanged, txtCodLAEmision.TextChanged, dtpFecha.ValueChanged, txtCodTransEmision.TextChanged, txtNroPax.TextChanged, txtUtilidad1.TextChanged, _
    '     txtNotaVenta2.TextChanged, txtBoletoNro.TextChanged, txtCoPtoVentaEmision.TextChanged, cboFormaPago.SelectionChangeCommitted, _
    '     txtDetalle.TextChanged, txtSegmentos.TextChanged, txtPago.TextChanged, txtObservacion.TextChanged, cboEjecCounter.SelectionChangeCommitted, txtUtilidad2.TextChanged, txtSsCosto.TextChanged, txtNroNotaVenta.TextChanged
    Private Sub Validaciones_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
       txtIndVenta.TextChanged, txtMonto.TextChanged, txtCoIATA.TextChanged, _
      txtCoLAServicio.TextChanged, txtCodLARecord.TextChanged, txtCodLAEmision.TextChanged, dtpFechaEmision.ValueChanged, txtCodTransEmision.TextChanged, _
        txtBoletoNro.TextChanged, txtCoPtoVentaEmision.TextChanged, _
        cboCuentaContable.SelectionChangeCommitted, cboMoneda.SelectionChangeCommitted, _
        cboTipoDetraccion.SelectionChangeCommitted, cboTipoDocumento.SelectionChangeCommitted, txtPercepcion.TextChanged, _
        txtConcepto.TextChanged, cboTipoOperacion.SelectionChangeCommitted

        If Not blnLoad Then Exit Sub
        Try
            If sender IsNot Nothing Then ErrPrv.SetError(CType(sender, Control), "")
            blnCambios = True

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub dtpFechaEmision_ValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _
     dtpFechaEmision.ValueChanged

        Try
            If Not blnEsMultiple() Then
                pCalcularTipoCambio()
                pCalcularTotal()
                If cboTipoDetraccion.Items.Count > 0 Then
                    cboTipoDetraccion_SelectedValueChanged(Nothing, Nothing)
                End If
            Else
                If Not FlTodasMonedas() Then
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_Arg()
                    Else
                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle()
                    End If

                Else

                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas_Arg()
                    Else
                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas()
                    End If


                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Importes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
 txtMonto.KeyPress, txtIGV.KeyPress, txtSsNeto.KeyPress, txtSsOtrCargos.KeyPress, txtSsDetraccion.KeyPress, txtPercepcion.KeyPress

        If sender Is Nothing Then Exit Sub
        Try
            If Not IsNumeric(e.KeyChar) And Asc(e.KeyChar) <> 8 Then
                Dim Text As TextBox = CType(sender, TextBox)
                If Asc(e.KeyChar) = 46 Then
                    If Text.Text.Trim = "" Or InStr(Text.Text.Trim, ".") > 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CambioMoneda()
        Try
            'Dim comoneda As String = cboMoneda.SelectedValue.ToString.Trim
            'Dim comoneda_pago As String = cboMoneda_Pago.SelectedValue.ToString.Trim

            'If comoneda <> "System.Data.DataRowView" Then
            '    If comoneda_pago <> "System.Data.DataRowView" Then

            '        If comoneda = comoneda_pago Then 'si son iguales ocultar tipo de cambio
            '            chkTipoCambio.Visible = False
            '            chkTipoCambio.Checked = False
            '            grbTipoCambio.Visible = False
            '        Else
            '            chkTipoCambio.Visible = True
            '            chkTipoCambio.Checked = True
            '            grbTipoCambio.Visible = True
            '            chkTipoCambio.Text = "Convertir a " + cboMoneda_Pago.Text.Trim
            '        End If
            '    End If

            '    'If comoneda = gstrTipoMonedaDol Then
            '    '    chkTipoCambio.Text = "Convertir a Soles"
            '    'Else
            '    '    chkTipoCambio.Text = "Convertir a Dolares"
            '    'End If

            '    If chkTipoCambio.Checked = True Then
            '        pCalcularTipoCambio()
            '    Else
            '        If validarTipoCambio_Proveedor() = False Then
            '            pCalcularTipoCambio()
            '        End If
            '    End If
            'End If

            'cboTipoDetraccion_SelectedValueChanged(Nothing, Nothing)

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cboMoneda_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboMoneda.SelectionChangeCommitted ', cboMoneda_Pago.SelectionChangeCommitted 'Handles cboMoneda.SelectedValueChanged,cboMoneda_Pago.SelectionChangeCommitted
        Try

            CambioMoneda()
            If strTipoVoucheruOrServ <> "OTR" Then
                CargarMonedaFEgreso()
            Else
                strMonedaFEgreso = cboMoneda.SelectedValue
            End If
            If Not blnEsMultiple() Then
                pCalcularTipoCambio()
            Else
                strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
                MostrarOcultar_ColumnaTCambio()
                CargarMonedaFEgreso()
                If Not FlTodasMonedas() Then
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_Arg()
                    Else
                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle()
                    End If

                Else

                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas_Arg()
                    Else

                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas()
                    End If


                End If

                'pCalcularTipoCambio()
            End If
            'pCalcularTipoCambio()
            If cboTipoDetraccion.Items.Count > 0 Then
                cboTipoDetraccion_SelectedValueChanged(Nothing, Nothing)
            End If
            'Dim comoneda As String = cboMoneda.SelectedValue.ToString.Trim
            'If comoneda <> "System.Data.DataRowView" Then
            '    'If comoneda = gstrTipoMonedaDol Then
            '    '    chkTipoCambio.Text = "Convertir a Soles"
            '    'Else
            '    '    chkTipoCambio.Text = "Convertir a Dolares"
            '    'End If

            '    'If chkTipoCambio.Checked = True Then
            '    '    pCalcularTipoCambio()
            '    'End If
            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvDetalle_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellClick
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            If e.ColumnIndex = dgvDetalle.Columns("btnEdit").Index Then
                dgvDetItem_DoubleClick(Nothing, Nothing)
            ElseIf e.ColumnIndex = dgvDetalle.Columns("btnDel").Index Then
                If MessageBox.Show("¿Está Ud. seguro de eliminar el registro de la cuadrícula?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    AgregarEditarEliminarItinerario(e.RowIndex, "B")
                    dgvDetalle.Rows.RemoveAt(e.RowIndex)
                    blnCambios = True
                    CalcularTotales_Detalle()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub




    Public Function bytGenerarNuDetalle() As Byte
        'If objLstNotaVentaCounter_Detalle.Count = 0 Then Return 1
        'Try
        '    Dim bytNuDetalle As Byte = 0
        '    For Each ST As stNotaVentaCounter_Detalle In objLstNotaVentaCounter_Detalle
        '        If ST.NuDetalle > bytNuDetalle Then bytNuDetalle = ST.NuDetalle
        '    Next

        '    Return (bytNuDetalle + 1)
        'Catch ex As Exception
        '    Throw
        'End Try
    End Function

    Public Function bytGenerarNuDetalleItem() As Byte

        'Dim intIndexItem As Integer = dgvDocumentos.CurrentRow.Index
        'If intIndexItem > -1 Then

        '    If objLstNotaVentaCounter_Item(intIndexItem).LstLista_DetalleItems.Count = 0 Then Return 1
        '    Try
        '        Dim bytNuDetalle As Byte = 0
        '        For Each ST As stNotaVentaCounter_DetalleItem In objLstNotaVentaCounter_Item(intIndexItem).LstLista_DetalleItems
        '            If ST.NuDetalle > bytNuDetalle Then bytNuDetalle = ST.NuDetalle
        '        Next

        '        Return (bytNuDetalle + 1)
        '    Catch ex As Exception
        '        Throw
        '    End Try

        'End If


    End Function

    Public Function bytGenerarNuItem() As Byte
        If objLstDocumento_Detalle.Count = 0 Then Return 1
        Try
            Dim bytNuDocumProvDet As Byte = 0
            For Each ST As stDocumento_Detalle In objLstDocumento_Detalle
                If ST.NuDocumProvDet > bytNuDocumProvDet Then bytNuDocumProvDet = ST.NuDocumProvDet
            Next

            Return (bytNuDocumProvDet + 1)
        Catch ex As Exception
            Throw
        End Try
    End Function



    Private Sub btnNuevoDetalleItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    Dim frm As New frmDocumentosProveedorDatoMem
        '    frm.frmRef = Me
        '    frm.ShowDialog()
        '    If blnActualizoItinerario Then
        '        AgregarEditarEliminarItinerario(dgvDetalle.Rows.Count - 1, "N")
        '        blnActualizoItinerario = False
        '        blnCambios = True
        '    End If
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    'Private Sub dgvDetItem_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDetalle.CellClick

    'End Sub

    Private Sub dgvDetItem_CellPainting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvDetalle.CellPainting
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            If dgvDetalle.Columns(e.ColumnIndex).Name = "btnEdit" Then
                e.Paint(e.CellBounds, DataGridViewPaintParts.All)
                Dim celBoton As DataGridViewButtonCell = CType(dgvDetalle.Rows(e.RowIndex).Cells("btnEdit"), DataGridViewButtonCell)
                Dim icoAtomico As Image
                'If Not bln_mMigrado Then
                '    icoAtomico = My.Resources.Resources.editarpng
                'Else
                '    icoAtomico = My.Resources.Resources.zoom
                'End If
                icoAtomico = My.Resources.Resources.editarpng
                e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
                dgvDetalle.Rows(e.RowIndex).Height = 21
                dgvDetalle.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
                e.Handled = True
            End If

            If dgvDetalle.Columns(e.ColumnIndex).Name = "btnDel" Then
                e.Paint(e.CellBounds, DataGridViewPaintParts.All)
                Dim celBoton As DataGridViewButtonCell = CType(dgvDetalle.Rows(e.RowIndex).Cells("btnDel"), DataGridViewButtonCell)
                Dim icoAtomico As Image = My.Resources.Resources.eliminarpng
                e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
                dgvDetalle.Rows(e.RowIndex).Height = 21
                dgvDetalle.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
                e.Handled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvDetItem_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvDetalle.DoubleClick
        If dgvDetalle.CurrentRow Is Nothing Then Exit Sub
        Try
            Dim frm As New frmDocumentosProveedorDatoMem
            frm.bytNuSegmento = dgvDetalle.CurrentRow.Cells("NuDocumProvDet").Value
            frm.strDescripcion = dgvDetalle.CurrentRow.Cells("NuDocumProvDet").Value
            frm.frmRef = Me
            frm.ShowDialog()
            If blnActualizoItinerario Then
                AgregarEditarEliminarItinerario(dgvDetalle.CurrentRow.Index, "")
                blnActualizoItinerario = False
                blnCambios = True
                CalcularTotales_Detalle()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

        'If dgvItems.CurrentRow Is Nothing Then Exit Sub
        'Try
        '    Dim frm As New frmIngNotaVentaCounterDatoItemMem
        '    frm.bytNuSegmento = dgvItems.CurrentRow.Cells("NuItem").Value
        '    frm.strDescripcion = dgvItems.CurrentRow.Cells("NuItem").Value
        '    frm.frmRef = Me
        '    frm.ShowDialog()
        '    If blnActualizoItinerario Then
        '        AgregarEditarEliminarItinerario(dgvItems.CurrentRow.Index, "")
        '        blnActualizoItinerario = False
        '        blnCambios = True
        '    End If
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try

    End Sub



    Private Sub btnConsultarTipoCambio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub BloquearDesbloquear_Detalle()
        'Try
        '    If dgvDocumentos.Rows.Count = 0 Then
        '        btnNuevoDetalleItem.Enabled = False
        '        ToolTip1.SetToolTip(btnNuevoDetalleItem, "No existen items en la Nota de Venta")
        '        'ToolTip1.Show("No existen items en la Nota de Venta", btnNuevoDetalleItem)
        '    Else
        '        btnNuevoDetalleItem.Enabled = True
        '        ToolTip1.SetToolTip(btnNuevoDetalleItem, "")
        '    End If
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub dgvDetalle_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)

    End Sub

    Private Sub txtNuDocumento_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNuDocumento.KeyPress


        'Dim strCaracteres As String
        'strCaracteres = "0123456789-/A"

        'If Asc(e.KeyChar) <> 8 Then
        '    If InStr(strCaracteres, e.KeyChar) = 0 Then
        '        e.KeyChar = ""
        '    End If
        'End If


    End Sub

    Private Sub chkFechaEmision_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkFechaEmision.CheckedChanged
        Try
            If chkFechaEmision.Checked = True Then
                dtpFechaEmision.Enabled = True
                dtpFechaEmision.Value = Now
            Else
                dtpFechaEmision.Enabled = False
            End If
            If Not blnEsMultiple() Then
                pCalcularTipoCambio()
            Else
                If Not FlTodasMonedas() Then
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_Arg()
                    Else
                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle()
                    End If

                Else

                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas_Arg()
                    Else
                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas()
                    End If


                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub chkIGV_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkIGV.CheckedChanged
        Try
            If strModoEdicion = "N" Then

                If cboTipoSAP.Items.Count > 0 Then

                    If Not cboTipoSAP.SelectedValue Is Nothing Then
                        Dim strCoTipoSAP As String = cboTipoSAP.SelectedValue.ToString
                        If strCoTipoSAP <> "System.Data.DataRowView" Then
                            If strCoTipoSAP.Trim = "2" Then 'si es documento multiple
                                'pCalcularTotal(False, True)



                                txtIGV.Enabled = True

                                'multiples
                                If blnEsMultiple() And FlTodasMonedas() Then
                                    If chkIGV.Checked = False Then
                                        txtIGV.Text = "0.00"
                                    End If
                                End If

                                CalculosIGV()

                                Exit Sub
                            End If
                        End If
                    End If

                End If

                txtIGV.Enabled = chkIGV.Checked



                ValidarCheckRetencion()
                pCalcularTotal()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    'Private Sub CargarIgv()
    '    Try
    '        Dim objBN As New clsTablasApoyoBN.clsParametroBN
    '        Dim dtParametros As New DataTable
    '        dtParametros = objBN.ConsultarList()
    '        If dtParametros.Rows.Count > 0 Then
    '            sglIgv = dtParametros.Rows(0).Item("NuIGV")
    '        End If

    '    Catch ex As Exception
    '        Throw
    '    End Try

    'End Sub

    Private Sub CalculosIGV()
        Try
            If chkIGV.Checked Then
                Dim dblTotal As Double = txtMonto.Text
                'Dim dblTotIgv As Double = dblTotal * (gsglIGV / 100)
                Dim dblTotIgv As Double = dblTotal - (dblTotal / (1 + (gsglIGV / 100)))

                If (strCoUbigeo_Oficina = gstrBuenosAires) Then
                    'dblTotIgv = 0
                    dblTotIgv = dblTotal - (dblTotal / (1 + (dblIVA_Arg / 100)))
                End If

                If (strCoUbigeo_Oficina = gstrSantiago) Then
                    'dblTotIgv = 0
                    ' dblTotIgv = 0
                    dblTotIgv = dblTotal - (dblTotal / (1 + (dblIVA_Chile / 100)))
                End If

                txtIGV.Text = Format(dblTotIgv, "###,##0.00")

            Else
                txtIGV.Text = "0.00"
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function blnEsMultiple() As Boolean
        Dim blnResultado As Boolean = False

        If cboTipoSAP.Items.Count > 0 Then

            If Not cboTipoSAP.SelectedValue Is Nothing Then
                Dim strCoTipoSAP As String = cboTipoSAP.SelectedValue.ToString
                If strCoTipoSAP <> "System.Data.DataRowView" Then
                    If strCoTipoSAP.Trim = "2" Then 'si es documento multiple
                        blnResultado = True
                    End If
                End If
            End If

        End If

        Return blnResultado
    End Function

    Private Sub RestarIgv_Multiples()
        Try
            Dim dblTotal As Double = 0
            Dim dblTotal_Tmp As Double = 0
            Dim dblNeto_Tmp As Double = 0
            Dim dblNeto As Double = 0
            Dim dblIGV As Double = 0
            Dim dblIGV_Tmp As Double = 0
            Dim dblOtrosCargos As Double = 0
            Dim dblPorcentajeRenta As Double = 10
            Dim dblPorcentajeRenta2 As Double = 0
            Dim dblPercepcion As Double = 0

            'If txtSsNeto.Text.Trim.Length > 0 Then
            '    dblNeto = CDbl(txtSsNeto.Text)
            'End If

            If txtSsOtrCargos.Text.Trim.Length > 0 Then
                dblOtrosCargos = CDbl(txtSsOtrCargos.Text)
            End If

            If txtMonto.Text.Trim.Length > 0 Then
                dblTotal = CDbl(txtMonto.Text)
            End If

            If txtIGV.Text.Trim.Length > 0 Then
                'dblIGV = CDbl(txtIGV.Text)
                dblIGV_Tmp = CDbl(txtIGV.Text)
            End If

            dblNeto = dblTotal - dblOtrosCargos - dblIGV_Tmp

            txtSsNeto.Text = Format(dblNeto, "###,##0.00")
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtSsNeto_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtSsNeto.TextChanged, txtIGV.TextChanged, txtSsOtrCargos.TextChanged, txtMonto.TextChanged, txtPercepcion.TextChanged
        If sender Is Nothing Then Exit Sub
        ErrPrv.SetError(DirectCast(sender, Control), "")
        ' If strModoEdicion <> "E" Then
        If sender.Name = "txtSsNeto" Or sender.Name = "txtSsOtrCargos" Or sender.Name = "txtIGV" Or sender.Name = "txtPercepcion" Then

            'If sender.name = "txtIGV" And blnEsMultiple() Then
            If blnEsMultiple() Then
                'pCalcularTotal(, True)
                RestarIgv_Multiples()
                Exit Sub
            End If

            If cboTipoDocumento.Items.Count > 0 Then

                If Not cboTipoDocumento.SelectedValue Is Nothing Then
                    Dim strCoTipoDoc As String = cboTipoDocumento.SelectedValue.ToString
                    If strCoTipoDoc <> "System.Data.DataRowView" Then
                        If sender.name = "txtIGV" And strCoTipoDoc.Trim = "HTD" Then
                            pCalcularTotal(True)
                            ValidarCheckRetencion()
                            Exit Sub
                        End If

                        If sender.name = "txtIGV" And blnEsMultiple() Then
                            pCalcularTotal(, True)

                            Exit Sub
                        End If
                    End If
                End If

            End If


            If sender.Name = "txtSsOtrCargos" Then

                If cboTipoSAP.Items.Count > 0 Then

                    If Not cboTipoSAP.SelectedValue Is Nothing Then
                        Dim strCoTipoSAP As String = cboTipoSAP.SelectedValue.ToString
                        If strCoTipoSAP <> "System.Data.DataRowView" Then
                            If strCoTipoSAP.Trim = "2" Then 'si es documento multiple
                                'pCalcularTotal(False, True)
                                ValidarCheckRetencion()
                                Exit Sub
                            End If
                        End If
                    End If

                End If

            End If

            pCalcularTotal()
            ValidarCheckRetencion()
        End If
        'End If
    End Sub


    Private Sub cboTipoDetraccion_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoDetraccion.SelectedValueChanged
        Try
            Dim strValor As String = "" 'cboTipoDetraccion.SelectedValue.ToString
            Dim dblDetraccion_TipoCambio As Double = 0
            If Not cboTipoDetraccion.SelectedValue Is Nothing Then
                strValor = cboTipoDetraccion.SelectedValue.ToString
            End If

            Dim strIDMoneda As String = ""
            If Not cboMoneda.SelectedValue Is Nothing Then
                strIDMoneda = cboMoneda.SelectedValue.ToString
            End If



            If strValor <> "System.Data.DataRowView" Then
                Dim dblTasa As Double = 0
                Dim dblNeto As Double = 0
                Dim objBN As New clsTipoDetraccionBN
                Dim dtData As DataTable = objBN.ConsultarList(cboTipoDetraccion.SelectedValue, "", "")
                If dtData.Rows.Count > 0 Then
                    dblTasa = dtData.Rows(0).Item("SsTasa")
                End If
                'If txtSsNeto.Text.Trim.Length > 0 Then
                '    dblNeto = CDbl(txtSsNeto.Text)
                'End If
                If txtMonto.Text.Trim.Length > 0 Then
                    dblNeto = CDbl(txtMonto.Text)
                End If
                dblTasa = dblNeto * dblTasa
                If strIDMoneda = gstrTipoMonedaSol Then
                    'dblTasa = Math.Round(dblTasa, 0, MidpointRounding.AwayFromZero)
                    dblTasa = Math.Round(dblTasa, 2, MidpointRounding.AwayFromZero)
                End If

                'dblTasa = Math.Round(dblTasa, 0, MidpointRounding.AwayFromZero)
                'txtSsDetraccion.Text = Format(dblTasa, "###,##0.00")
                txtSsDetraccion.Text = Format(dblTasa, "###,##0.00")

                ErrPrvSS.SetError(dtpFechaEmision, "")
                ErrPrvSS.SetError(cboMoneda, "")
                blnDetraccionError = False
                blnDetraccionErrorMoneda = False

                If dblTasa > 0 Then
                    dblDetraccion_TipoCambio = pCalcularTipoCambio_Detracción(dblTasa)
                    txtSsDetraccion.Text = Format(dblDetraccion_TipoCambio, "###,##0.00")
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CalcularDetraccion_Soles()
        Try
            Dim strValor As String = "" 'cboTipoDetraccion.SelectedValue.ToString
            If Not cboTipoDetraccion.SelectedValue Is Nothing Then
                strValor = cboTipoDetraccion.SelectedValue.ToString
            End If
            If strValor <> "System.Data.DataRowView" Then
                Dim dblTasa As Double = 0
                Dim dblNeto As Double = 0
                Dim objBN As New clsTipoDetraccionBN
                Dim dtData As DataTable = objBN.ConsultarList(cboTipoDetraccion.SelectedValue, "", "")
                If dtData.Rows.Count > 0 Then
                    dblTasa = dtData.Rows(0).Item("SsTasa")
                End If
                'If txtSsNeto.Text.Trim.Length > 0 Then
                '    dblNeto = CDbl(txtSsNeto.Text)
                'End If

                If cboMoneda.SelectedValue = gstrTipoMonedaSol Then
                    If txtMonto.Text.Trim.Length > 0 Then
                        dblNeto = CDbl(txtMonto.Text)
                    End If
                Else

                End If



                dblTasa = dblNeto * dblTasa
                dblTasa = Math.Round(dblTasa, 2, MidpointRounding.AwayFromZero)
                'txtSsDetraccion.Text = Format(dblTasa, "###,##0.00")
                txtSsDetraccion.Text = Format(dblTasa, "###,##0.00")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function validarTipoCambio_Proveedor() As Boolean
        Try
            Dim bln As Boolean = True

            'If strModoEdicion = "N" Then
            If dblTipoCambioProveedor > 0 Then
                bln = False
            End If
            'End If
            Return bln
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Sub chkTipoCambio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Try
        '    If chkTipoCambio.Checked = True Then
        '        grbTipoCambio.Enabled = True
        '        pCalcularTipoCambio()
        '    Else
        '        grbTipoCambio.Enabled = False
        '        'si el proveedor tiene tipo de cambio, no se cambia el textbox

        '        If validarTipoCambio_Proveedor() Then
        '            txtTipoCambio.Text = "0.00"
        '        End If

        '        txtTotalTC.Text = "0.00"
        '    End If
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub pCalcularTipoCambio()
        If blnLoad = False Then Exit Sub
        Try
            If (strCoUbigeo_Oficina <> gstrLima And strCoUbigeo_Oficina <> gstrCusco) Then
                pCalcularTipoCambio_Moneda()
                Exit Sub
            End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                'objBN.ConsultarPk
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
                    dr.Read()
                    If dr.HasRows Then

                        Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)


                        Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                        Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                        'txtTipoCambio.Text = dr("ValVenta").ToString.Trim
                        txtTipoCambioFEgreso.Text = dr("ValVenta").ToString.Trim



                        If txtMonto.Text.Trim.Length > 0 Then
                            dblTotalOrigen = CDbl(txtMonto.Text)
                            dblTotalOrigen2 = CDbl(txtMonto.Text)
                        End If

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        strMoneda = cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = strMonedaFEgreso
                        If strMoneda = gstrTipoMonedaDol Then
                            If strMoneda_Pago = gstrTipoMonedaSol Then
                                dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                            Else
                                dblTipoCambio = 0

                                txtTipoCambioFEgreso.Text = "0.00"

                                dblTotalDestino2 = 0 'dblTotalOrigen
                            End If
                        ElseIf strMoneda = gstrTipoMonedaSol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblTotalDestino2 = 0
                                Else
                                    dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                                End If

                            Else
                                dblTipoCambio = 0

                                txtTipoCambioFEgreso.Text = "0.00"

                                dblTotalDestino2 = 0 'dblTotalOrigen
                            End If
                        Else
                            dblTipoCambio = 0

                            'txtTipoCambio.Text = "0.00"

                            dblTotalDestino2 = 0 'dblTotalOrigen
                        End If

                        txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")
                    Else

                        txtTipoCambioFEgreso.Text = "0.00"
                        txtTotalFEgreso.Text = "0.00"

                        'txtTipoCambio.Text = "0.00"
                        'txtTotalTC.Text = "0.00"
                    End If
                    dr.Close()
                End Using
                'End If

            Else

                'cuando el proveedor tiene tipo de cambio
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                'txtTipoCambio.Text = dblTipoCambioProveedor.ToString.Trim
                txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString.Trim



                If txtMonto.Text.Trim.Length > 0 Then
                    dblTotalOrigen = CDbl(txtMonto.Text)
                    dblTotalOrigen2 = CDbl(txtMonto.Text)
                End If

                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString
                ''If chkTipoCambio.Checked = True Then
                'If strMoneda = gstrTipoMonedaDol Then

                '    If strMoneda_Pago = "SOL" Then
                '        dblTotalDestino = dblTotalOrigen * dblTipoCambio
                '    Else
                '        dblTipoCambio = 0
                '        'txtTipoCambio.Text = "0.00"
                '        'txtTipoCambio.Text = dblTipoCambioProveedor.ToString

                '        dblTotalDestino = 0 'dblTotalOrigen
                '    End If


                'ElseIf strMoneda = "SOL" Then
                '    'convertir a dolares
                '    If strMoneda_Pago = gstrTipoMonedaDol Then
                '        dblTotalDestino = dblTotalOrigen / dblTipoCambio
                '    Else
                '        dblTipoCambio = 0

                '        'txtTipoCambio.Text = "0.00"
                '        'txtTipoCambio.Text = dblTipoCambioProveedor.ToString '"0.00"

                '        dblTotalDestino = 0 'dblTotalOrigen
                '    End If

                'Else
                '    dblTipoCambio = 0

                '    'txtTipoCambio.Text = dblTipoCambioProveedor.ToString '"0.00"

                '    'txtTipoCambio.Text = "0.00"
                '    dblTotalDestino = 0 'dblTotalOrigen
                'End If

                ' ''dblTotalDestino = dblTotalOrigen / dblTipoCambio
                ''txtTotalTC.Text = Format(dblTotalDestino, "###,##0.00")
                ' ''End If

                strMoneda = cboMoneda.SelectedValue.ToString
                'strMoneda_Pago = strMonedaFEgreso
                If strMoneda = gstrTipoMonedaDol Then
                    'If strMoneda_Pago = "SOL" Then
                    If strMonedaFEgreso = gstrTipoMonedaSol Then
                        dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                    Else
                        dblTipoCambio = 0

                        'txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString '"0.00"
                        txtTipoCambioFEgreso.Text = "0.000"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                ElseIf strMoneda = gstrTipoMonedaSol Then
                    'convertir a dolares
                    'If strMoneda_Pago = gstrTipoMonedaDol Then
                    If strMonedaFEgreso = gstrTipoMonedaDol Then

                        Dim dblTotalOrigen3 As Double = 0

                        If dblTipoCambio = 0 Then
                            'dblTotalOrigen3 = 0
                            dblTotalDestino2 = 0
                        Else
                            'dblTotalOrigen3 = dblTotalOrigen2
                            dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                        End If

                        'dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                        'dblTotalDestino2 = dblTotalOrigen3 / dblTipoCambio
                    Else
                        dblTipoCambio = 0

                        txtTipoCambioFEgreso.Text = "0.000" 'dblTipoCambioProveedor.ToString '"0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                Else
                    dblTipoCambio = 0

                    'txtTipoCambio.Text = "0.00" 'dblTipoCambioProveedor.ToString '"0.00"

                    dblTotalDestino2 = 0 'dblTotalOrigen
                End If

                txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")


            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub pCalcularTipoCambio_Moneda()
        If blnLoad = False Then Exit Sub
        Try

            Dim strMonedaCambio As String = strMonedaFEgreso
            If strMonedaFEgreso = "USD" Then
                If cboMoneda.SelectedValue <> "USD" Then
                    strMonedaCambio = cboMoneda.SelectedValue
                End If
            End If

            'If chkTipoCambio.Checked = True Then
            Dim objBN As New clsTablasApoyoBN.clsTipoCambio_MonedasBN
            'objBN.ConsultarPk
            Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date, strMonedaCambio)
                dr.Read()
                If dr.HasRows Then

                    Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                    dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)


                    Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                    Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                    'txtTipoCambio.Text = dr("ValVenta").ToString.Trim
                    txtTipoCambioFEgreso.Text = dr("ValVenta").ToString.Trim



                    If txtMonto.Text.Trim.Length > 0 Then
                        dblTotalOrigen = CDbl(txtMonto.Text)
                        dblTotalOrigen2 = CDbl(txtMonto.Text)
                    End If

                    Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                    Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                    strMoneda = cboMoneda.SelectedValue.ToString
                    strMoneda_Pago = strMonedaFEgreso
                    If strMoneda = gstrTipoMonedaDol Then
                        If strMoneda_Pago <> gstrTipoMonedaDol Then
                            dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                        Else
                            dblTipoCambio = 0

                            txtTipoCambioFEgreso.Text = "0.00"

                            dblTotalDestino2 = 0 'dblTotalOrigen
                        End If
                    ElseIf strMoneda <> gstrTipoMonedaDol Then
                        'convertir a dolares
                        If strMoneda_Pago = gstrTipoMonedaDol Then
                            If dblTipoCambio = 0 Then
                                dblTotalDestino2 = 0
                            Else
                                dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                            End If

                        Else
                            dblTipoCambio = 0

                            txtTipoCambioFEgreso.Text = "0.00"

                            dblTotalDestino2 = 0 'dblTotalOrigen
                        End If
                    Else
                        dblTipoCambio = 0

                        'txtTipoCambio.Text = "0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If

                    txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")
                Else

                    txtTipoCambioFEgreso.Text = "0.00"
                    txtTotalFEgreso.Text = "0.00"

                    'txtTipoCambio.Text = "0.00"
                    'txtTotalTC.Text = "0.00"
                End If
                dr.Close()
            End Using
            'End If




        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pCalcularTipoCambio_Multiple(Optional ByVal vdblTipoCambio As Decimal = 0)
        If blnLoad = False Then Exit Sub
        Try
            strMonedaFEgreso = cboMoneda_Multiple.SelectedValue

            Dim strMonedaCambio As String = strMonedaFEgreso
            If strMonedaFEgreso = "USD" Then
                If cboMoneda.SelectedValue <> "USD" Then
                    strMonedaCambio = cboMoneda.SelectedValue
                End If
            End If

            calculalTotalMarcados(True)
            'If strMonedaFEgreso = cboMoneda.SelectedValue.ToString Then
            '    calculalTotalMarcados(True)
            '    Exit Sub
            'End If


            Dim dblNeto1 As Double = 0
            Dim dblNeto2 As Double = 0
            Dim dblIGV1 As Double = 0
            Dim dblIGV2 As Double = 0
            Dim dblOtrosCargos1 As Double = 0
            Dim dblOtrosCargos2 As Double = 0
            Dim dblTotal1 As Double = 0
            Dim dblTotal2 As Double = 0

            If vdblTipoCambio > 0 Then
                'tipo de cambio personalizado
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)
                dblTipoCambio = vdblTipoCambio

                If dblTipoCambio_Multiple > 0 Then
                    If dblTipoCambio_Multiple <> dblTipoCambio Then
                        dblTipoCambio_Multiple = dblTipoCambio
                    End If
                Else
                    dblTipoCambio_Multiple = dblTipoCambio
                End If


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0



                'txtTipoCambio.Text = dr("ValVenta").ToString.Trim
                'txtTipoCambioFEgreso.Text = vdblTipoCambio

                If txtMonto.Text.Trim.Length > 0 Then
                    dblTotalOrigen = CDbl(txtMonto.Text)
                    dblTotalOrigen2 = CDbl(txtMonto.Text)
                    dblTotal1 = CDbl(txtMonto.Text)
                    dblTotal2 = CDbl(txtMonto.Text)
                End If

                If txtSsNeto.Text.Trim.Length > 0 Then
                    dblNeto1 = CDbl(txtSsNeto.Text)
                    dblNeto2 = CDbl(txtSsNeto.Text)
                End If

                If txtIGV.Text.Trim.Length > 0 Then
                    dblIGV1 = CDbl(txtIGV.Text)
                    dblIGV1 = CDbl(txtIGV.Text)
                End If

                If txtSsOtrCargos.Text.Trim.Length > 0 Then
                    dblOtrosCargos1 = CDbl(txtSsOtrCargos.Text)
                    dblOtrosCargos2 = CDbl(txtSsOtrCargos.Text)
                End If

                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                strMoneda_Pago = cboMoneda.SelectedValue.ToString
                If strMoneda = gstrTipoMonedaDol Then
                    If strMoneda_Pago = gstrTipoMonedaSol Then
                        dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                        dblNeto2 = dblNeto1 * dblTipoCambio
                        dblIGV2 = dblIGV2 * dblTipoCambio
                        dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                        dblTotal2 = dblTotal1 * dblTipoCambio

                    Else
                        dblTipoCambio = 0

                        txtTipoCambioFEgreso.Text = "0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                ElseIf strMoneda = gstrTipoMonedaSol Then
                    'convertir a dolares
                    If strMoneda_Pago = gstrTipoMonedaDol Then
                        If dblTipoCambio = 0 Then
                            dblTotalDestino2 = 0
                        Else
                            dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                            dblNeto2 = dblNeto1 / dblTipoCambio
                            dblIGV2 = dblIGV2 / dblTipoCambio
                            dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                            dblTotal2 = dblTotal1 / dblTipoCambio
                        End If

                    Else
                        'dblTipoCambio = 0

                        'txtTipoCambioFEgreso.Text = "0.00"

                        'dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                Else
                    dblTipoCambio = 0

                    'txtTipoCambio.Text = "0.00"

                    dblTotalDestino2 = 0 'dblTotalOrigen
                End If

                txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")



                txtSsOtrCargos.Text = Format(dblOtrosCargos2, "###,##0.00")
                txtMonto.Text = Format(dblTotal2, "###,##0.00")
                txtIGV.Text = Format(dblIGV2, "###,##0.00")
                txtSsNeto.Text = Format(dblNeto2, "###,##0.00")


                Exit Sub
            End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                'objBN.ConsultarPk
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
                    dr.Read()
                    If dr.HasRows Then

                        Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)

                        If dblTipoCambio_Multiple > 0 Then
                            If dblTipoCambio_Multiple <> dblTipoCambio Then
                                dblTipoCambio_Multiple = dblTipoCambio
                            End If
                        Else
                            dblTipoCambio_Multiple = dblTipoCambio
                        End If


                        Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                        Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0



                        'txtTipoCambio.Text = dr("ValVenta").ToString.Trim
                        txtTipoCambioFEgreso.Text = dr("ValVenta").ToString.Trim

                        If txtMonto.Text.Trim.Length > 0 Then
                            dblTotalOrigen = CDbl(txtMonto.Text)
                            dblTotalOrigen2 = CDbl(txtMonto.Text)
                            dblTotal1 = CDbl(txtMonto.Text)
                            dblTotal2 = CDbl(txtMonto.Text)
                        End If

                        If txtSsNeto.Text.Trim.Length > 0 Then
                            dblNeto1 = CDbl(txtSsNeto.Text)
                            dblNeto2 = CDbl(txtSsNeto.Text)
                        End If

                        If txtIGV.Text.Trim.Length > 0 Then
                            dblIGV1 = CDbl(txtIGV.Text)
                            dblIGV1 = CDbl(txtIGV.Text)
                        End If

                        If txtSsOtrCargos.Text.Trim.Length > 0 Then
                            dblOtrosCargos1 = CDbl(txtSsOtrCargos.Text)
                            dblOtrosCargos2 = CDbl(txtSsOtrCargos.Text)
                        End If

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString
                        ''If chkTipoCambio.Checked = True Then
                        'If strMoneda = gstrTipoMonedaDol Then

                        '    If strMoneda_Pago = "SOL" Then
                        '        dblTotalDestino = dblTotalOrigen * dblTipoCambio
                        '    Else
                        '        dblTipoCambio = 0
                        '        'txtTipoCambio.Text = "0.00"

                        '        dblTotalDestino = 0 'dblTotalOrigen
                        '    End If


                        'ElseIf strMoneda = "SOL" Then
                        '    'convertir a dolares
                        '    If strMoneda_Pago = gstrTipoMonedaDol Then
                        '        dblTotalDestino = dblTotalOrigen / dblTipoCambio
                        '    Else
                        '        dblTipoCambio = 0

                        '        'txtTipoCambio.Text = "0.00"

                        '        dblTotalDestino = 0 'dblTotalOrigen
                        '    End If

                        'Else
                        '    dblTipoCambio = 0

                        '    'txtTipoCambio.Text = "0.00"

                        '    'txtTipoCambio.Text = "0.00"
                        '    dblTotalDestino = 0 'dblTotalOrigen
                        'End If

                        ''dblTotalDestino = dblTotalOrigen / dblTipoCambio
                        ''txtTotalTC.Text = Format(dblTotalDestino, "###,##0.00")
                        ''End If

                        'strMoneda = cboMoneda.SelectedValue.ToString
                        'strMoneda_Pago = strMonedaFEgreso
                        strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            If strMoneda_Pago = gstrTipoMonedaSol Then
                                dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                                dblNeto2 = dblNeto1 * dblTipoCambio
                                dblIGV2 = dblIGV2 * dblTipoCambio
                                dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                dblTotal2 = dblTotal1 * dblTipoCambio

                            Else
                                dblTipoCambio = 0

                                txtTipoCambioFEgreso.Text = "0.00"

                                dblTotalDestino2 = 0 'dblTotalOrigen
                            End If
                        ElseIf strMoneda = gstrTipoMonedaSol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblTotalDestino2 = 0
                                Else
                                    dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV2 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio
                                End If

                            Else
                                dblTipoCambio = 0

                                txtTipoCambioFEgreso.Text = "0.00"

                                dblTotalDestino2 = 0 'dblTotalOrigen
                            End If
                        Else
                            dblTipoCambio = 0

                            'txtTipoCambio.Text = "0.00"

                            dblTotalDestino2 = 0 'dblTotalOrigen
                        End If

                        txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")



                        txtSsOtrCargos.Text = Format(dblOtrosCargos2, "###,##0.00")
                        txtMonto.Text = Format(dblTotal2, "###,##0.00")
                        txtIGV.Text = Format(dblIGV2, "###,##0.00")
                        txtSsNeto.Text = Format(dblNeto2, "###,##0.00")
                    Else

                        txtTipoCambioFEgreso.Text = "0.00"
                        txtTotalFEgreso.Text = "0.00"

                        'txtTipoCambio.Text = "0.00"
                        'txtTotalTC.Text = "0.00"
                    End If
                    dr.Close()
                End Using
                'End If

            Else

                'cuando el proveedor tiene tipo de cambio
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                'txtTipoCambio.Text = dblTipoCambioProveedor.ToString.Trim
                txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString.Trim



                'If txtMonto.Text.Trim.Length > 0 Then
                '    dblTotalOrigen = CDbl(txtMonto.Text)
                '    dblTotalOrigen2 = CDbl(txtMonto.Text)
                'End If

                If txtMonto.Text.Trim.Length > 0 Then
                    dblTotalOrigen = CDbl(txtMonto.Text)
                    dblTotalOrigen2 = CDbl(txtMonto.Text)
                    dblTotal1 = CDbl(txtMonto.Text)
                    dblTotal2 = CDbl(txtMonto.Text)
                End If

                If txtSsNeto.Text.Trim.Length > 0 Then
                    dblNeto1 = CDbl(txtSsNeto.Text)
                    dblNeto2 = CDbl(txtSsNeto.Text)
                End If

                If txtIGV.Text.Trim.Length > 0 Then
                    dblIGV1 = CDbl(txtIGV.Text)
                    dblIGV1 = CDbl(txtIGV.Text)
                End If

                If txtSsOtrCargos.Text.Trim.Length > 0 Then
                    dblOtrosCargos1 = CDbl(txtSsOtrCargos.Text)
                    dblOtrosCargos2 = CDbl(txtSsOtrCargos.Text)
                End If

                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString
                ''If chkTipoCambio.Checked = True Then
                'If strMoneda = gstrTipoMonedaDol Then

                '    If strMoneda_Pago = "SOL" Then
                '        dblTotalDestino = dblTotalOrigen * dblTipoCambio
                '    Else
                '        dblTipoCambio = 0
                '        'txtTipoCambio.Text = "0.00"
                '        'txtTipoCambio.Text = dblTipoCambioProveedor.ToString

                '        dblTotalDestino = 0 'dblTotalOrigen
                '    End If


                'ElseIf strMoneda = "SOL" Then
                '    'convertir a dolares
                '    If strMoneda_Pago = gstrTipoMonedaDol Then
                '        dblTotalDestino = dblTotalOrigen / dblTipoCambio
                '    Else
                '        dblTipoCambio = 0

                '        'txtTipoCambio.Text = "0.00"
                '        'txtTipoCambio.Text = dblTipoCambioProveedor.ToString '"0.00"

                '        dblTotalDestino = 0 'dblTotalOrigen
                '    End If

                'Else
                '    dblTipoCambio = 0

                '    'txtTipoCambio.Text = dblTipoCambioProveedor.ToString '"0.00"

                '    'txtTipoCambio.Text = "0.00"
                '    dblTotalDestino = 0 'dblTotalOrigen
                'End If

                ' ''dblTotalDestino = dblTotalOrigen / dblTipoCambio
                ''txtTotalTC.Text = Format(dblTotalDestino, "###,##0.00")
                ' ''End If

                strMoneda = cboMoneda.SelectedValue.ToString
                'strMoneda_Pago = strMonedaFEgreso
                If strMoneda = gstrTipoMonedaDol Then
                    'If strMoneda_Pago = "SOL" Then
                    If strMonedaFEgreso = gstrTipoMonedaSol Then
                        'dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                        dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                        dblNeto2 = dblNeto1 / dblTipoCambio
                        dblIGV2 = dblIGV2 / dblTipoCambio
                        dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                        dblTotal2 = dblTotal1 / dblTipoCambio
                    Else
                        dblTipoCambio = 0

                        'txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString '"0.00"
                        txtTipoCambioFEgreso.Text = "0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                ElseIf strMoneda = gstrTipoMonedaSol Then
                    'convertir a dolares
                    'If strMoneda_Pago = gstrTipoMonedaDol Then
                    If strMonedaFEgreso = gstrTipoMonedaDol Then

                        Dim dblTotalOrigen3 As Double = 0

                        If dblTipoCambio = 0 Then
                            'dblTotalOrigen3 = 0
                            dblTotalDestino2 = 0
                        Else
                            'dblTotalOrigen3 = dblTotalOrigen2
                            'dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                            dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                            dblNeto2 = dblNeto1 * dblTipoCambio
                            dblIGV2 = dblIGV2 * dblTipoCambio
                            dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                            dblTotal2 = dblTotal1 * dblTipoCambio
                        End If

                        'dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                        'dblTotalDestino2 = dblTotalOrigen3 / dblTipoCambio
                    Else
                        dblTipoCambio = 0

                        txtTipoCambioFEgreso.Text = "0.00" 'dblTipoCambioProveedor.ToString '"0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                Else
                    dblTipoCambio = 0

                    'txtTipoCambio.Text = "0.00" 'dblTipoCambioProveedor.ToString '"0.00"

                    dblTotalDestino2 = 0 'dblTotalOrigen
                End If

                txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")

                txtSsOtrCargos.Text = Format(dblOtrosCargos2, "###,##0.00")
                txtMonto.Text = Format(dblTotal2, "###,##0.00")
                txtIGV.Text = Format(dblIGV2, "###,##0.00")
                txtSsNeto.Text = Format(dblNeto2, "###,##0.00")
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pCalcularTipoCambio_Multiple_Arg(Optional ByVal vdblTipoCambio As Decimal = 0)
        If blnLoad = False Then Exit Sub
        Try
            strMonedaFEgreso = cboMoneda_Multiple.SelectedValue

            Dim strMonedaCambio As String = strMonedaFEgreso
            If strMonedaFEgreso = "USD" Then
                If cboMoneda.SelectedValue <> "USD" Then
                    strMonedaCambio = cboMoneda.SelectedValue
                End If
            End If


            calculalTotalMarcados(True)
            'If strMonedaFEgreso = cboMoneda.SelectedValue.ToString Then
            '    calculalTotalMarcados(True)
            '    Exit Sub
            'End If


            Dim dblNeto1 As Double = 0
            Dim dblNeto2 As Double = 0
            Dim dblIGV1 As Double = 0
            Dim dblIGV2 As Double = 0
            Dim dblOtrosCargos1 As Double = 0
            Dim dblOtrosCargos2 As Double = 0
            Dim dblTotal1 As Double = 0
            Dim dblTotal2 As Double = 0

            If vdblTipoCambio > 0 Then
                'tipo de cambio personalizado
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)
                dblTipoCambio = vdblTipoCambio

                If dblTipoCambio_Multiple > 0 Then
                    If dblTipoCambio_Multiple <> dblTipoCambio Then
                        dblTipoCambio_Multiple = dblTipoCambio
                    End If
                Else
                    dblTipoCambio_Multiple = dblTipoCambio
                End If


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0



                'txtTipoCambio.Text = dr("ValVenta").ToString.Trim
                'txtTipoCambioFEgreso.Text = vdblTipoCambio

                If txtMonto.Text.Trim.Length > 0 Then
                    dblTotalOrigen = CDbl(txtMonto.Text)
                    dblTotalOrigen2 = CDbl(txtMonto.Text)
                    dblTotal1 = CDbl(txtMonto.Text)
                    dblTotal2 = CDbl(txtMonto.Text)
                End If

                If txtSsNeto.Text.Trim.Length > 0 Then
                    dblNeto1 = CDbl(txtSsNeto.Text)
                    dblNeto2 = CDbl(txtSsNeto.Text)
                End If

                If txtIGV.Text.Trim.Length > 0 Then
                    dblIGV1 = CDbl(txtIGV.Text)
                    dblIGV1 = CDbl(txtIGV.Text)
                End If

                If txtSsOtrCargos.Text.Trim.Length > 0 Then
                    dblOtrosCargos1 = CDbl(txtSsOtrCargos.Text)
                    dblOtrosCargos2 = CDbl(txtSsOtrCargos.Text)
                End If

                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                strMoneda_Pago = cboMoneda.SelectedValue.ToString
                If strMoneda = gstrTipoMonedaDol Then
                    ' If strMoneda_Pago = gstrTipoMonedaSol Then
                    If strMoneda_Pago <> gstrTipoMonedaDol Then
                        dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                        dblNeto2 = dblNeto1 * dblTipoCambio
                        dblIGV2 = dblIGV2 * dblTipoCambio
                        dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                        dblTotal2 = dblTotal1 * dblTipoCambio

                    Else
                        dblTipoCambio = 0

                        txtTipoCambioFEgreso.Text = "0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                    'ElseIf strMoneda = gstrTipoMonedaSol Then
                ElseIf strMoneda <> gstrTipoMonedaDol Then
                    'convertir a dolares
                    If strMoneda_Pago = gstrTipoMonedaDol Then
                        If dblTipoCambio = 0 Then
                            dblTotalDestino2 = 0
                        Else
                            dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                            dblNeto2 = dblNeto1 / dblTipoCambio
                            dblIGV2 = dblIGV2 / dblTipoCambio
                            dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                            dblTotal2 = dblTotal1 / dblTipoCambio
                        End If

                    Else
                        'dblTipoCambio = 0

                        'txtTipoCambioFEgreso.Text = "0.00"

                        'dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                Else
                    dblTipoCambio = 0

                    'txtTipoCambio.Text = "0.00"

                    dblTotalDestino2 = 0 'dblTotalOrigen
                End If

                txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")



                txtSsOtrCargos.Text = Format(dblOtrosCargos2, "###,##0.00")
                txtMonto.Text = Format(dblTotal2, "###,##0.00")
                txtIGV.Text = Format(dblIGV2, "###,##0.00")
                txtSsNeto.Text = Format(dblNeto2, "###,##0.00")


                Exit Sub
            End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                ' Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                Dim objBN As New clsTablasApoyoBN.clsTipoCambio_MonedasBN
                'objBN.ConsultarPk
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date, strMonedaCambio) 'objBN.ConsultarPk(dtpFechaEmision.Value.Date)
                    dr.Read()
                    If dr.HasRows Then

                        Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)

                        If dblTipoCambio_Multiple > 0 Then
                            If dblTipoCambio_Multiple <> dblTipoCambio Then
                                dblTipoCambio_Multiple = dblTipoCambio
                            End If
                        Else
                            dblTipoCambio_Multiple = dblTipoCambio
                        End If


                        Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                        Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0



                        'txtTipoCambio.Text = dr("ValVenta").ToString.Trim
                        txtTipoCambioFEgreso.Text = dr("ValVenta").ToString.Trim

                        If txtMonto.Text.Trim.Length > 0 Then
                            dblTotalOrigen = CDbl(txtMonto.Text)
                            dblTotalOrigen2 = CDbl(txtMonto.Text)
                            dblTotal1 = CDbl(txtMonto.Text)
                            dblTotal2 = CDbl(txtMonto.Text)
                        End If

                        If txtSsNeto.Text.Trim.Length > 0 Then
                            dblNeto1 = CDbl(txtSsNeto.Text)
                            dblNeto2 = CDbl(txtSsNeto.Text)
                        End If

                        If txtIGV.Text.Trim.Length > 0 Then
                            dblIGV1 = CDbl(txtIGV.Text)
                            dblIGV1 = CDbl(txtIGV.Text)
                        End If

                        If txtSsOtrCargos.Text.Trim.Length > 0 Then
                            dblOtrosCargos1 = CDbl(txtSsOtrCargos.Text)
                            dblOtrosCargos2 = CDbl(txtSsOtrCargos.Text)
                        End If

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString
                        ''If chkTipoCambio.Checked = True Then
                        'If strMoneda = gstrTipoMonedaDol Then

                        '    If strMoneda_Pago = "SOL" Then
                        '        dblTotalDestino = dblTotalOrigen * dblTipoCambio
                        '    Else
                        '        dblTipoCambio = 0
                        '        'txtTipoCambio.Text = "0.00"

                        '        dblTotalDestino = 0 'dblTotalOrigen
                        '    End If


                        'ElseIf strMoneda = "SOL" Then
                        '    'convertir a dolares
                        '    If strMoneda_Pago = gstrTipoMonedaDol Then
                        '        dblTotalDestino = dblTotalOrigen / dblTipoCambio
                        '    Else
                        '        dblTipoCambio = 0

                        '        'txtTipoCambio.Text = "0.00"

                        '        dblTotalDestino = 0 'dblTotalOrigen
                        '    End If

                        'Else
                        '    dblTipoCambio = 0

                        '    'txtTipoCambio.Text = "0.00"

                        '    'txtTipoCambio.Text = "0.00"
                        '    dblTotalDestino = 0 'dblTotalOrigen
                        'End If

                        ''dblTotalDestino = dblTotalOrigen / dblTipoCambio
                        ''txtTotalTC.Text = Format(dblTotalDestino, "###,##0.00")
                        ''End If

                        'strMoneda = cboMoneda.SelectedValue.ToString
                        'strMoneda_Pago = strMonedaFEgreso
                        strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            'If strMoneda_Pago = gstrTipoMonedaSol Then
                            If strMoneda_Pago <> gstrTipoMonedaDol Then
                                dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                                dblNeto2 = dblNeto1 * dblTipoCambio
                                dblIGV2 = dblIGV2 * dblTipoCambio
                                dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                dblTotal2 = dblTotal1 * dblTipoCambio

                            Else
                                dblTipoCambio = 0

                                txtTipoCambioFEgreso.Text = "0.00"

                                dblTotalDestino2 = 0 'dblTotalOrigen
                            End If
                            'ElseIf strMoneda = gstrTipoMonedaSol Then
                        ElseIf strMoneda <> gstrTipoMonedaDol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblTotalDestino2 = 0
                                Else
                                    dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV2 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio
                                End If

                            Else
                                dblTipoCambio = 0

                                txtTipoCambioFEgreso.Text = "0.00"

                                dblTotalDestino2 = 0 'dblTotalOrigen
                            End If
                        Else
                            dblTipoCambio = 0

                            'txtTipoCambio.Text = "0.00"

                            dblTotalDestino2 = 0 'dblTotalOrigen
                        End If

                        txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")



                        txtSsOtrCargos.Text = Format(dblOtrosCargos2, "###,##0.00")
                        txtMonto.Text = Format(dblTotal2, "###,##0.00")
                        txtIGV.Text = Format(dblIGV2, "###,##0.00")
                        txtSsNeto.Text = Format(dblNeto2, "###,##0.00")
                    Else

                        txtTipoCambioFEgreso.Text = "0.00"
                        txtTotalFEgreso.Text = "0.00"

                        'txtTipoCambio.Text = "0.00"
                        'txtTotalTC.Text = "0.00"
                    End If
                    dr.Close()
                End Using
                'End If

            Else

                'cuando el proveedor tiene tipo de cambio
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                'txtTipoCambio.Text = dblTipoCambioProveedor.ToString.Trim
                txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString.Trim



                'If txtMonto.Text.Trim.Length > 0 Then
                '    dblTotalOrigen = CDbl(txtMonto.Text)
                '    dblTotalOrigen2 = CDbl(txtMonto.Text)
                'End If

                If txtMonto.Text.Trim.Length > 0 Then
                    dblTotalOrigen = CDbl(txtMonto.Text)
                    dblTotalOrigen2 = CDbl(txtMonto.Text)
                    dblTotal1 = CDbl(txtMonto.Text)
                    dblTotal2 = CDbl(txtMonto.Text)
                End If

                If txtSsNeto.Text.Trim.Length > 0 Then
                    dblNeto1 = CDbl(txtSsNeto.Text)
                    dblNeto2 = CDbl(txtSsNeto.Text)
                End If

                If txtIGV.Text.Trim.Length > 0 Then
                    dblIGV1 = CDbl(txtIGV.Text)
                    dblIGV1 = CDbl(txtIGV.Text)
                End If

                If txtSsOtrCargos.Text.Trim.Length > 0 Then
                    dblOtrosCargos1 = CDbl(txtSsOtrCargos.Text)
                    dblOtrosCargos2 = CDbl(txtSsOtrCargos.Text)
                End If

                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString
                ''If chkTipoCambio.Checked = True Then
                'If strMoneda = gstrTipoMonedaDol Then

                '    If strMoneda_Pago = "SOL" Then
                '        dblTotalDestino = dblTotalOrigen * dblTipoCambio
                '    Else
                '        dblTipoCambio = 0
                '        'txtTipoCambio.Text = "0.00"
                '        'txtTipoCambio.Text = dblTipoCambioProveedor.ToString

                '        dblTotalDestino = 0 'dblTotalOrigen
                '    End If


                'ElseIf strMoneda = "SOL" Then
                '    'convertir a dolares
                '    If strMoneda_Pago = gstrTipoMonedaDol Then
                '        dblTotalDestino = dblTotalOrigen / dblTipoCambio
                '    Else
                '        dblTipoCambio = 0

                '        'txtTipoCambio.Text = "0.00"
                '        'txtTipoCambio.Text = dblTipoCambioProveedor.ToString '"0.00"

                '        dblTotalDestino = 0 'dblTotalOrigen
                '    End If

                'Else
                '    dblTipoCambio = 0

                '    'txtTipoCambio.Text = dblTipoCambioProveedor.ToString '"0.00"

                '    'txtTipoCambio.Text = "0.00"
                '    dblTotalDestino = 0 'dblTotalOrigen
                'End If

                ' ''dblTotalDestino = dblTotalOrigen / dblTipoCambio
                ''txtTotalTC.Text = Format(dblTotalDestino, "###,##0.00")
                ' ''End If

                strMoneda = cboMoneda.SelectedValue.ToString
                'strMoneda_Pago = strMonedaFEgreso
                If strMoneda = gstrTipoMonedaDol Then
                    'If strMoneda_Pago = "SOL" Then
                    ' If strMonedaFEgreso = gstrTipoMonedaSol Then
                    If strMonedaFEgreso <> gstrTipoMonedaDol Then
                        'dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                        dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                        dblNeto2 = dblNeto1 / dblTipoCambio
                        dblIGV2 = dblIGV2 / dblTipoCambio
                        dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                        dblTotal2 = dblTotal1 / dblTipoCambio
                    Else
                        dblTipoCambio = 0

                        'txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString '"0.00"
                        txtTipoCambioFEgreso.Text = "0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                    ' ElseIf strMoneda = gstrTipoMonedaSol Then
                ElseIf strMoneda <> gstrTipoMonedaDol Then
                    'convertir a dolares
                    'If strMoneda_Pago = gstrTipoMonedaDol Then
                    If strMonedaFEgreso = gstrTipoMonedaDol Then

                        Dim dblTotalOrigen3 As Double = 0

                        If dblTipoCambio = 0 Then
                            'dblTotalOrigen3 = 0
                            dblTotalDestino2 = 0
                        Else
                            'dblTotalOrigen3 = dblTotalOrigen2
                            'dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                            dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
                            dblNeto2 = dblNeto1 * dblTipoCambio
                            dblIGV2 = dblIGV2 * dblTipoCambio
                            dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                            dblTotal2 = dblTotal1 * dblTipoCambio
                        End If

                        'dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
                        'dblTotalDestino2 = dblTotalOrigen3 / dblTipoCambio
                    Else
                        dblTipoCambio = 0

                        txtTipoCambioFEgreso.Text = "0.00" 'dblTipoCambioProveedor.ToString '"0.00"

                        dblTotalDestino2 = 0 'dblTotalOrigen
                    End If
                Else
                    dblTipoCambio = 0

                    'txtTipoCambio.Text = "0.00" 'dblTipoCambioProveedor.ToString '"0.00"

                    dblTotalDestino2 = 0 'dblTotalOrigen
                End If

                txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")

                txtSsOtrCargos.Text = Format(dblOtrosCargos2, "###,##0.00")
                txtMonto.Text = Format(dblTotal2, "###,##0.00")
                txtIGV.Text = Format(dblIGV2, "###,##0.00")
                txtSsNeto.Text = Format(dblNeto2, "###,##0.00")
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub pCalcularTipoCambio_Multiple_Detalle_TodasMonedas(Optional ByVal vdblTipoCambio As Decimal = 0)
        If blnLoad = False Then Exit Sub
        Try
            If Not FlTodasMonedas() Then
                strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
            End If
            'strMonedaFEgreso = cboMoneda_Multiple.SelectedValue

            ''calculalTotalMarcados(True)
            ''If strMonedaFEgreso = cboMoneda.SelectedValue.ToString Then
            ''    calculalTotalMarcados(True)
            ''    Exit Sub
            ''End If


            Dim dblNeto1 As Double = 0
            Dim dblNeto2 As Double = 0
            Dim dblIGV1 As Double = 0
            Dim dblIGV2 As Double = 0
            Dim dblOtrosCargos1 As Double = 0
            Dim dblOtrosCargos2 As Double = 0
            Dim dblTotal1 As Double = 0
            Dim dblTotal2 As Double = 0

            Dim dblNeto2_Suma As Double = 0
            Dim dblIGV2_Suma As Double = 0
            Dim dblOtrosCargos2_Suma As Double = 0
            Dim dblTotal2_Suma As Double = 0

            If vdblTipoCambio > 0 Then
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                dblTipoCambio = vdblTipoCambio

                If dblTipoCambio_Multiple > 0 Then
                    If dblTipoCambio_Multiple <> dblTipoCambio Then
                        dblTipoCambio_Multiple = dblTipoCambio
                    End If
                Else
                    dblTipoCambio_Multiple = dblTipoCambio
                End If

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblIGV1 = CDbl(drow.Cells("IGV_Multiple").Value) 'IGV_Multiple
                        dblOtrosCargos1 = CDbl(drow.Cells("Gastos").Value) 'Gastos
                        dblTotal1 = CDbl(drow.Cells("Monto").Value)


                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value 'cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            If strMoneda_Pago = gstrTipoMonedaSol Then

                                dblNeto2 = dblNeto1 * dblTipoCambio

                                dblIGV2 = dblIGV1 * dblTipoCambio
                                dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                dblTotal2 = dblTotal1 * dblTipoCambio


                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If
                        ElseIf strMoneda = gstrTipoMonedaSol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio

                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If
                        Else
                            'dblTipoCambio = 0

                            ''txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0

                            'otra moneda

                            dblTipoCambio = CDbl(txtTipoCambioFEgreso.Text.Trim)

                            'txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0

                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0

                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If

                        End If

                        If dblNeto2 <> 0 Then
                            drow.Cells("Total_TC").Value = dblNeto2

                            dblNeto2_Suma = dblNeto2_Suma + dblNeto2
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV2
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos2
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal2


                        Else
                            drow.Cells("Total_TC").Value = dblNeto1

                            dblNeto2_Suma = dblNeto2_Suma + dblNeto1
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV1
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos1
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal1
                        End If


                    End If
                Next

                txtSsOtrCargos.Text = Format(dblOtrosCargos2_Suma, "###,##0.00")
                txtMonto.Text = Format(dblTotal2_Suma, "###,##0.00")
                txtIGV.Text = Format(dblIGV2_Suma, "###,##0.00")
                txtSsNeto.Text = Format(dblNeto2_Suma, "###,##0.00")

                Exit Sub
            End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                'objBN.ConsultarPk
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
                    dr.Read()
                    If dr.HasRows Then

                        Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)

                        If dblTipoCambio_Multiple > 0 Then
                            If dblTipoCambio_Multiple <> dblTipoCambio Then
                                dblTipoCambio_Multiple = dblTipoCambio
                            End If
                        Else
                            dblTipoCambio_Multiple = dblTipoCambio
                        End If

                        For Each drow As DataGridViewRow In dgvVouchers.Rows
                            If drow.Cells("chkAsignar").Value = True Then
                                dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                                dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                                dblIGV1 = CDbl(drow.Cells("IGV_Multiple").Value) 'IGV_Multiple
                                dblOtrosCargos1 = CDbl(drow.Cells("Gastos").Value) 'Gastos
                                dblTotal1 = CDbl(drow.Cells("Monto").Value)


                                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString



                                'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                                strMoneda = drow.Cells("IDMoneda").Value
                                strMoneda_Pago = cboMoneda.SelectedValue.ToString
                                If strMoneda = gstrTipoMonedaDol Then
                                    If strMoneda_Pago = gstrTipoMonedaSol Then

                                        dblNeto2 = dblNeto1 * dblTipoCambio
                                        dblIGV2 = dblIGV1 * dblTipoCambio
                                        dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                        dblTotal2 = dblTotal1 * dblTipoCambio

                                    Else

                                        dblNeto2 = 0 'dblTotalOrigen
                                        dblIGV2 = 0
                                        dblOtrosCargos2 = 0
                                        dblTotal2 = 0

                                    End If
                                ElseIf strMoneda = gstrTipoMonedaSol Then
                                    'convertir a dolares
                                    If strMoneda_Pago = gstrTipoMonedaDol Then
                                        If dblTipoCambio = 0 Then
                                            dblNeto2 = 0
                                            dblIGV2 = 0
                                            dblOtrosCargos2 = 0
                                            dblTotal2 = 0

                                        Else

                                            dblNeto2 = dblNeto1 / dblTipoCambio
                                            dblIGV2 = dblIGV1 / dblTipoCambio
                                            dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                            dblTotal2 = dblTotal1 / dblTipoCambio

                                        End If

                                    Else
                                        dblTipoCambio = 0

                                        dblNeto2 = 0 'dblTotalOrigen
                                        dblIGV2 = 0
                                        dblOtrosCargos2 = 0
                                        dblTotal2 = 0
                                    End If
                                Else
                                    'otra moneda

                                    dblTipoCambio = CDbl(txtTipoCambioFEgreso.Text.Trim)

                                    'txtTipoCambio.Text = "0.00"

                                    'dblNeto2 = 0 'dblTotalOrigen
                                    'dblIGV2 = 0
                                    'dblOtrosCargos2 = 0
                                    'dblTotal2 = 0

                                    If strMoneda_Pago = gstrTipoMonedaDol Then
                                        If dblTipoCambio = 0 Then
                                            dblNeto2 = 0
                                            dblIGV2 = 0
                                            dblOtrosCargos2 = 0
                                            dblTotal2 = 0

                                        Else

                                            dblNeto2 = dblNeto1 / dblTipoCambio
                                            dblIGV2 = dblIGV1 / dblTipoCambio
                                            dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                            dblTotal2 = dblTotal1 / dblTipoCambio

                                        End If

                                    Else
                                        dblTipoCambio = 0

                                        dblNeto2 = 0 'dblTotalOrigen
                                        dblIGV2 = 0
                                        dblOtrosCargos2 = 0
                                        dblTotal2 = 0
                                    End If

                                End If

                                'drow.Cells("Total_TC").Value = dblNeto2
                                If dblNeto2 <> 0 Then
                                    drow.Cells("Total_TC").Value = dblNeto2
                                    dblNeto2_Suma = dblNeto2_Suma + dblNeto2
                                    dblIGV2_Suma = dblIGV2_Suma + dblIGV2
                                    dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos2
                                    dblTotal2_Suma = dblTotal2_Suma + dblTotal2
                                Else
                                    drow.Cells("Total_TC").Value = dblNeto1
                                    dblNeto2_Suma = dblNeto2_Suma + dblNeto1
                                    dblIGV2_Suma = dblIGV2_Suma + dblIGV1
                                    dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos1
                                    dblTotal2_Suma = dblTotal2_Suma + dblTotal1
                                End If
                            End If
                        Next




                    Else

                        'txtTipoCambioFEgreso.Text = "0.00"
                        'txtTotalFEgreso.Text = "0.00"

                        dr.Close()
                    End If
                End Using
                'End If

            Else

                'cuando el proveedor tiene tipo de cambio
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                'txtTipoCambio.Text = dblTipoCambioProveedor.ToString.Trim
                ' txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString.Trim
                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        dblIGV1 = CDbl(drow.Cells("IGV_Multiple").Value) 'IGV_Multiple
                        dblOtrosCargos1 = CDbl(drow.Cells("Gastos").Value) 'Gastos
                        dblTotal1 = CDbl(drow.Cells("Monto").Value)
                        'Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        'Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        strMoneda = cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            If strMoneda_Pago = gstrTipoMonedaSol Then

                                dblNeto2 = dblNeto1 * dblTipoCambio
                                dblIGV2 = dblIGV1 * dblTipoCambio
                                dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                dblTotal2 = dblTotal1 * dblTipoCambio

                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0

                            End If
                        ElseIf strMoneda = gstrTipoMonedaSol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0

                            End If
                        Else
                            'dblTipoCambio = 0

                            ''txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0


                            'otra moneda

                            dblTipoCambio = CDbl(txtTipoCambioFEgreso.Text.Trim)

                            'txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0

                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0

                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If

                        End If

                        'drow.Cells("Total_TC").Value = dblNeto2
                        If dblNeto2 <> 0 Then
                            drow.Cells("Total_TC").Value = dblNeto2
                            dblNeto2_Suma = dblNeto2_Suma + dblNeto2
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV2
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos2
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal2
                        Else
                            drow.Cells("Total_TC").Value = dblNeto1
                            dblNeto2_Suma = dblNeto2_Suma + dblNeto1
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV1
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos1
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal1
                        End If
                    End If
                Next


            End If

            txtSsOtrCargos.Text = Format(dblOtrosCargos2_Suma, "###,##0.00")
            txtMonto.Text = Format(dblTotal2_Suma, "###,##0.00")
            txtIGV.Text = Format(dblIGV2_Suma, "###,##0.00")
            txtSsNeto.Text = Format(dblNeto2_Suma, "###,##0.00")

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pCalcularTipoCambio_Multiple_Detalle_TodasMonedas_Arg(Optional ByVal vdblTipoCambio As Decimal = 0)
        If blnLoad = False Then Exit Sub
        Try
            If Not FlTodasMonedas() Then
                strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
            End If

            Dim strMonedaCambio As String = strMonedaFEgreso
            If strMonedaFEgreso = "USD" Then
                If cboMoneda.SelectedValue <> "USD" Then
                    strMonedaCambio = cboMoneda.SelectedValue
                End If
            End If

            'strMonedaFEgreso = cboMoneda_Multiple.SelectedValue

            ''calculalTotalMarcados(True)
            ''If strMonedaFEgreso = cboMoneda.SelectedValue.ToString Then
            ''    calculalTotalMarcados(True)
            ''    Exit Sub
            ''End If


            Dim dblNeto1 As Double = 0
            Dim dblNeto2 As Double = 0
            Dim dblIGV1 As Double = 0
            Dim dblIGV2 As Double = 0
            Dim dblOtrosCargos1 As Double = 0
            Dim dblOtrosCargos2 As Double = 0
            Dim dblTotal1 As Double = 0
            Dim dblTotal2 As Double = 0

            Dim dblNeto2_Suma As Double = 0
            Dim dblIGV2_Suma As Double = 0
            Dim dblOtrosCargos2_Suma As Double = 0
            Dim dblTotal2_Suma As Double = 0

            If vdblTipoCambio > 0 Then
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                dblTipoCambio = vdblTipoCambio

                If dblTipoCambio_Multiple > 0 Then
                    If dblTipoCambio_Multiple <> dblTipoCambio Then
                        dblTipoCambio_Multiple = dblTipoCambio
                    End If
                Else
                    dblTipoCambio_Multiple = dblTipoCambio
                End If

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblIGV1 = CDbl(drow.Cells("IGV_Multiple").Value) 'IGV_Multiple
                        dblOtrosCargos1 = CDbl(drow.Cells("Gastos").Value) 'Gastos
                        dblTotal1 = CDbl(drow.Cells("Monto").Value)


                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value 'cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            'If strMoneda_Pago = gstrTipoMonedaSol Then
                            If strMoneda_Pago <> gstrTipoMonedaDol Then

                                dblNeto2 = dblNeto1 * dblTipoCambio

                                dblIGV2 = dblIGV1 * dblTipoCambio
                                dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                dblTotal2 = dblTotal1 * dblTipoCambio


                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If
                            'ElseIf strMoneda = gstrTipoMonedaSol Then
                        ElseIf strMoneda <> gstrTipoMonedaDol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio

                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If
                        Else
                            'dblTipoCambio = 0

                            ''txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0

                            'otra moneda

                            dblTipoCambio = CDbl(txtTipoCambioFEgreso.Text.Trim)

                            'txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0

                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0

                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If

                        End If

                        If dblNeto2 <> 0 Then
                            drow.Cells("Total_TC").Value = dblNeto2

                            dblNeto2_Suma = dblNeto2_Suma + dblNeto2
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV2
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos2
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal2


                        Else
                            drow.Cells("Total_TC").Value = dblNeto1

                            dblNeto2_Suma = dblNeto2_Suma + dblNeto1
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV1
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos1
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal1
                        End If


                    End If
                Next

                txtSsOtrCargos.Text = Format(dblOtrosCargos2_Suma, "###,##0.00")
                txtMonto.Text = Format(dblTotal2_Suma, "###,##0.00")
                txtIGV.Text = Format(dblIGV2_Suma, "###,##0.00")
                txtSsNeto.Text = Format(dblNeto2_Suma, "###,##0.00")

                Exit Sub
            End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                ' Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                Dim objBN As New clsTablasApoyoBN.clsTipoCambio_MonedasBN
                'objBN.ConsultarPk
                'Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date, strMonedaCambio)
                    dr.Read()
                    If dr.HasRows Then

                        Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)

                        If dblTipoCambio_Multiple > 0 Then
                            If dblTipoCambio_Multiple <> dblTipoCambio Then
                                dblTipoCambio_Multiple = dblTipoCambio
                            End If
                        Else
                            dblTipoCambio_Multiple = dblTipoCambio
                        End If

                        For Each drow As DataGridViewRow In dgvVouchers.Rows
                            If drow.Cells("chkAsignar").Value = True Then
                                dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                                dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                                dblIGV1 = CDbl(drow.Cells("IGV_Multiple").Value) 'IGV_Multiple
                                dblOtrosCargos1 = CDbl(drow.Cells("Gastos").Value) 'Gastos
                                dblTotal1 = CDbl(drow.Cells("Monto").Value)


                                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString



                                'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                                strMoneda = drow.Cells("IDMoneda").Value
                                strMoneda_Pago = cboMoneda.SelectedValue.ToString
                                If strMoneda = gstrTipoMonedaDol Then
                                    'If strMoneda_Pago = gstrTipoMonedaSol Then
                                    If strMoneda_Pago <> gstrTipoMonedaDol Then

                                        dblNeto2 = dblNeto1 * dblTipoCambio
                                        dblIGV2 = dblIGV1 * dblTipoCambio
                                        dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                        dblTotal2 = dblTotal1 * dblTipoCambio

                                    Else

                                        dblNeto2 = 0 'dblTotalOrigen
                                        dblIGV2 = 0
                                        dblOtrosCargos2 = 0
                                        dblTotal2 = 0

                                    End If
                                    'ElseIf strMoneda = gstrTipoMonedaSol Then
                                ElseIf strMoneda <> gstrTipoMonedaDol Then
                                    'convertir a dolares
                                    If strMoneda_Pago = gstrTipoMonedaDol Then
                                        If dblTipoCambio = 0 Then
                                            dblNeto2 = 0
                                            dblIGV2 = 0
                                            dblOtrosCargos2 = 0
                                            dblTotal2 = 0

                                        Else

                                            dblNeto2 = dblNeto1 / dblTipoCambio
                                            dblIGV2 = dblIGV1 / dblTipoCambio
                                            dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                            dblTotal2 = dblTotal1 / dblTipoCambio

                                        End If

                                    Else
                                        dblTipoCambio = 0

                                        dblNeto2 = 0 'dblTotalOrigen
                                        dblIGV2 = 0
                                        dblOtrosCargos2 = 0
                                        dblTotal2 = 0
                                    End If
                                Else
                                    'otra moneda

                                    dblTipoCambio = CDbl(txtTipoCambioFEgreso.Text.Trim)

                                    'txtTipoCambio.Text = "0.00"

                                    'dblNeto2 = 0 'dblTotalOrigen
                                    'dblIGV2 = 0
                                    'dblOtrosCargos2 = 0
                                    'dblTotal2 = 0

                                    If strMoneda_Pago = gstrTipoMonedaDol Then
                                        If dblTipoCambio = 0 Then
                                            dblNeto2 = 0
                                            dblIGV2 = 0
                                            dblOtrosCargos2 = 0
                                            dblTotal2 = 0

                                        Else

                                            dblNeto2 = dblNeto1 / dblTipoCambio
                                            dblIGV2 = dblIGV1 / dblTipoCambio
                                            dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                            dblTotal2 = dblTotal1 / dblTipoCambio

                                        End If

                                    Else
                                        dblTipoCambio = 0

                                        dblNeto2 = 0 'dblTotalOrigen
                                        dblIGV2 = 0
                                        dblOtrosCargos2 = 0
                                        dblTotal2 = 0
                                    End If

                                End If

                                'drow.Cells("Total_TC").Value = dblNeto2
                                If dblNeto2 <> 0 Then
                                    drow.Cells("Total_TC").Value = dblNeto2
                                    dblNeto2_Suma = dblNeto2_Suma + dblNeto2
                                    dblIGV2_Suma = dblIGV2_Suma + dblIGV2
                                    dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos2
                                    dblTotal2_Suma = dblTotal2_Suma + dblTotal2
                                Else
                                    drow.Cells("Total_TC").Value = dblNeto1
                                    dblNeto2_Suma = dblNeto2_Suma + dblNeto1
                                    dblIGV2_Suma = dblIGV2_Suma + dblIGV1
                                    dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos1
                                    dblTotal2_Suma = dblTotal2_Suma + dblTotal1
                                End If
                            End If
                        Next




                    Else

                        'txtTipoCambioFEgreso.Text = "0.00"
                        'txtTotalFEgreso.Text = "0.00"

                        dr.Close()
                    End If
                End Using
                'End If

            Else

                'cuando el proveedor tiene tipo de cambio
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                'txtTipoCambio.Text = dblTipoCambioProveedor.ToString.Trim
                ' txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString.Trim
                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        dblIGV1 = CDbl(drow.Cells("IGV_Multiple").Value) 'IGV_Multiple
                        dblOtrosCargos1 = CDbl(drow.Cells("Gastos").Value) 'Gastos
                        dblTotal1 = CDbl(drow.Cells("Monto").Value)
                        'Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        'Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        strMoneda = cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            'If strMoneda_Pago = gstrTipoMonedaSol Then
                            If strMoneda_Pago <> gstrTipoMonedaDol Then

                                dblNeto2 = dblNeto1 * dblTipoCambio
                                dblIGV2 = dblIGV1 * dblTipoCambio
                                dblOtrosCargos2 = dblOtrosCargos1 * dblTipoCambio
                                dblTotal2 = dblTotal1 * dblTipoCambio

                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0

                            End If
                            'ElseIf strMoneda = gstrTipoMonedaSol Then
                        ElseIf strMoneda <> gstrTipoMonedaDol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0

                            End If
                        Else
                            'dblTipoCambio = 0

                            ''txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0


                            'otra moneda

                            dblTipoCambio = CDbl(txtTipoCambioFEgreso.Text.Trim)

                            'txtTipoCambio.Text = "0.00"

                            'dblNeto2 = 0 'dblTotalOrigen
                            'dblIGV2 = 0
                            'dblOtrosCargos2 = 0
                            'dblTotal2 = 0

                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                    dblIGV2 = 0
                                    dblOtrosCargos2 = 0
                                    dblTotal2 = 0

                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio
                                    dblIGV2 = dblIGV1 / dblTipoCambio
                                    dblOtrosCargos2 = dblOtrosCargos1 / dblTipoCambio
                                    dblTotal2 = dblTotal1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                                dblIGV2 = 0
                                dblOtrosCargos2 = 0
                                dblTotal2 = 0
                            End If

                        End If

                        'drow.Cells("Total_TC").Value = dblNeto2
                        If dblNeto2 <> 0 Then
                            drow.Cells("Total_TC").Value = dblNeto2
                            dblNeto2_Suma = dblNeto2_Suma + dblNeto2
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV2
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos2
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal2
                        Else
                            drow.Cells("Total_TC").Value = dblNeto1
                            dblNeto2_Suma = dblNeto2_Suma + dblNeto1
                            dblIGV2_Suma = dblIGV2_Suma + dblIGV1
                            dblOtrosCargos2_Suma = dblOtrosCargos2_Suma + dblOtrosCargos1
                            dblTotal2_Suma = dblTotal2_Suma + dblTotal1
                        End If
                    End If
                Next


            End If

            txtSsOtrCargos.Text = Format(dblOtrosCargos2_Suma, "###,##0.00")
            txtMonto.Text = Format(dblTotal2_Suma, "###,##0.00")
            txtIGV.Text = Format(dblIGV2_Suma, "###,##0.00")
            txtSsNeto.Text = Format(dblNeto2_Suma, "###,##0.00")

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pCalcularTipoCambio_Multiple_Detalle(Optional ByVal vdblTipoCambio As Decimal = 0)
        If blnLoad = False Then Exit Sub
        Try
            If Not FlTodasMonedas() Then
                strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
            End If
            'strMonedaFEgreso = cboMoneda_Multiple.SelectedValue

            ''calculalTotalMarcados(True)
            ''If strMonedaFEgreso = cboMoneda.SelectedValue.ToString Then
            ''    calculalTotalMarcados(True)
            ''    Exit Sub
            ''End If


            Dim dblNeto1 As Double = 0
            Dim dblNeto2 As Double = 0
            Dim dblIGV1 As Double = 0
            Dim dblIGV2 As Double = 0
            Dim dblOtrosCargos1 As Double = 0
            Dim dblOtrosCargos2 As Double = 0
            Dim dblTotal1 As Double = 0
            Dim dblTotal2 As Double = 0

            If vdblTipoCambio > 0 Then
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                dblTipoCambio = vdblTipoCambio

                If dblTipoCambio_Multiple > 0 Then
                    If dblTipoCambio_Multiple <> dblTipoCambio Then
                        dblTipoCambio_Multiple = dblTipoCambio
                    End If
                Else
                    dblTipoCambio_Multiple = dblTipoCambio
                End If

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value 'cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            If strMoneda_Pago = gstrTipoMonedaSol Then

                                dblNeto2 = dblNeto1 * dblTipoCambio

                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                        ElseIf strMoneda = gstrTipoMonedaSol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                        Else
                            dblTipoCambio = 0

                            'txtTipoCambio.Text = "0.00"

                            dblNeto2 = 0 'dblTotalOrigen
                        End If

                        drow.Cells("Total_TC").Value = dblNeto2

                    End If
                Next
                Exit Sub
            End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                'objBN.ConsultarPk
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
                    dr.Read()
                    If dr.HasRows Then

                        Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)

                        If dblTipoCambio_Multiple > 0 Then
                            If dblTipoCambio_Multiple <> dblTipoCambio Then
                                dblTipoCambio_Multiple = dblTipoCambio
                            End If
                        Else
                            dblTipoCambio_Multiple = dblTipoCambio
                        End If

                        For Each drow As DataGridViewRow In dgvVouchers.Rows
                            If drow.Cells("chkAsignar").Value = True Then
                                dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                                dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                                'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                                strMoneda = drow.Cells("IDMoneda").Value
                                strMoneda_Pago = cboMoneda.SelectedValue.ToString
                                If strMoneda = gstrTipoMonedaDol Then
                                    If strMoneda_Pago = gstrTipoMonedaSol Then

                                        dblNeto2 = dblNeto1 * dblTipoCambio

                                    Else

                                        dblNeto2 = 0 'dblTotalOrigen
                                    End If
                                ElseIf strMoneda = gstrTipoMonedaSol Then
                                    'convertir a dolares
                                    If strMoneda_Pago = gstrTipoMonedaDol Then
                                        If dblTipoCambio = 0 Then
                                            dblNeto2 = 0
                                        Else

                                            dblNeto2 = dblNeto1 / dblTipoCambio

                                        End If

                                    Else
                                        dblTipoCambio = 0

                                        dblNeto2 = 0 'dblTotalOrigen
                                    End If
                                Else
                                    dblTipoCambio = 0

                                    'txtTipoCambio.Text = "0.00"

                                    dblNeto2 = 0 'dblTotalOrigen
                                End If

                                drow.Cells("Total_TC").Value = dblNeto2

                            End If
                        Next




                    Else

                        'txtTipoCambioFEgreso.Text = "0.00"
                        'txtTotalFEgreso.Text = "0.00"

                        dr.Close()
                    End If
                End Using
                'End If

            Else

                'cuando el proveedor tiene tipo de cambio
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                'txtTipoCambio.Text = dblTipoCambioProveedor.ToString.Trim
                ' txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString.Trim
                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        'Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        'Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        strMoneda = cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            If strMoneda_Pago = gstrTipoMonedaSol Then

                                dblNeto2 = dblNeto1 * dblTipoCambio

                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                        ElseIf strMoneda = gstrTipoMonedaSol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                        Else
                            dblTipoCambio = 0

                            'txtTipoCambio.Text = "0.00"

                            dblNeto2 = 0 'dblTotalOrigen
                        End If

                        drow.Cells("Total_TC").Value = dblNeto2

                    End If
                Next


            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pCalcularTipoCambio_Multiple_Detalle_Arg(Optional ByVal vdblTipoCambio As Decimal = 0)
        If blnLoad = False Then Exit Sub
        Try
            Dim strMonedaCambio As String = strMonedaFEgreso


            If Not FlTodasMonedas() Then
                strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
            End If

            If strMonedaFEgreso = "USD" Then
                If cboMoneda.SelectedValue <> "USD" Then
                    strMonedaCambio = cboMoneda.SelectedValue
                End If
            End If

            'strMonedaFEgreso = cboMoneda_Multiple.SelectedValue

            ''calculalTotalMarcados(True)
            ''If strMonedaFEgreso = cboMoneda.SelectedValue.ToString Then
            ''    calculalTotalMarcados(True)
            ''    Exit Sub
            ''End If


            Dim dblNeto1 As Double = 0
            Dim dblNeto2 As Double = 0
            Dim dblIGV1 As Double = 0
            Dim dblIGV2 As Double = 0
            Dim dblOtrosCargos1 As Double = 0
            Dim dblOtrosCargos2 As Double = 0
            Dim dblTotal1 As Double = 0
            Dim dblTotal2 As Double = 0

            If vdblTipoCambio > 0 Then
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                dblTipoCambio = vdblTipoCambio

                If dblTipoCambio_Multiple > 0 Then
                    If dblTipoCambio_Multiple <> dblTipoCambio Then
                        dblTipoCambio_Multiple = dblTipoCambio
                    End If
                Else
                    dblTipoCambio_Multiple = dblTipoCambio
                End If

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value 'cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            'If strMoneda_Pago = gstrTipoMonedaSol Then
                            If strMoneda_Pago <> gstrTipoMonedaDol Then
                                dblNeto2 = dblNeto1 * dblTipoCambio

                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                            'ElseIf strMoneda = gstrTipoMonedaSol Then
                        ElseIf strMoneda <> gstrTipoMonedaDol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                        Else
                            dblTipoCambio = 0

                            'txtTipoCambio.Text = "0.00"

                            dblNeto2 = 0 'dblTotalOrigen
                        End If

                        drow.Cells("Total_TC").Value = dblNeto2

                    End If
                Next
                Exit Sub
            End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                'Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                Dim objBN As New clsTablasApoyoBN.clsTipoCambio_MonedasBN()
                'objBN.ConsultarPk
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date, strMonedaCambio)
                    dr.Read()
                    If dr.HasRows Then

                        Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)

                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)

                        If dblTipoCambio_Multiple > 0 Then
                            If dblTipoCambio_Multiple <> dblTipoCambio Then
                                dblTipoCambio_Multiple = dblTipoCambio
                            End If
                        Else
                            dblTipoCambio_Multiple = dblTipoCambio
                        End If

                        For Each drow As DataGridViewRow In dgvVouchers.Rows
                            If drow.Cells("chkAsignar").Value = True Then
                                dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                                dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                                'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                                strMoneda = drow.Cells("IDMoneda").Value
                                strMoneda_Pago = cboMoneda.SelectedValue.ToString
                                If strMoneda = gstrTipoMonedaDol Then
                                    'If strMoneda_Pago = gstrTipoMonedaSol Then
                                    If strMoneda_Pago <> gstrTipoMonedaDol Then
                                        dblNeto2 = Math.Round(dblNeto1 * dblTipoCambio, 2)

                                    Else

                                        dblNeto2 = 0 'dblTotalOrigen
                                    End If
                                    'ElseIf strMoneda = gstrTipoMonedaSol Then
                                ElseIf strMoneda <> gstrTipoMonedaDol Then
                                    'convertir a dolares
                                    If strMoneda_Pago = gstrTipoMonedaDol Then
                                        If dblTipoCambio = 0 Then
                                            dblNeto2 = 0
                                        Else

                                            dblNeto2 = Math.Round(dblNeto1 / dblTipoCambio, 2)

                                        End If

                                    Else
                                        dblTipoCambio = 0

                                        dblNeto2 = 0 'dblTotalOrigen
                                    End If
                                Else
                                    dblTipoCambio = 0

                                    'txtTipoCambio.Text = "0.00"

                                    dblNeto2 = 0 'dblTotalOrigen
                                End If

                                drow.Cells("Total_TC").Value = dblNeto2

                            End If
                        Next




                    Else

                        'txtTipoCambioFEgreso.Text = "0.00"
                        'txtTotalFEgreso.Text = "0.00"

                        dr.Close()
                    End If
                End Using
                'End If

            Else

                'cuando el proveedor tiene tipo de cambio
                Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor


                Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                'txtTipoCambio.Text = dblTipoCambioProveedor.ToString.Trim
                ' txtTipoCambioFEgreso.Text = dblTipoCambioProveedor.ToString.Trim
                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                For Each drow As DataGridViewRow In dgvVouchers.Rows
                    If drow.Cells("chkAsignar").Value = True Then
                        dblNeto1 = CDbl(drow.Cells("MontoFinal").Value)
                        dblNeto2 = CDbl(drow.Cells("MontoFinal").Value)

                        'Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        'Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        strMoneda = cboMoneda.SelectedValue.ToString
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        'strMoneda = strMonedaFEgreso 'cboMoneda.SelectedValue.ToString
                        strMoneda = drow.Cells("IDMoneda").Value
                        strMoneda_Pago = cboMoneda.SelectedValue.ToString
                        If strMoneda = gstrTipoMonedaDol Then
                            'If strMoneda_Pago = gstrTipoMonedaSol Then
                            If strMoneda_Pago <> gstrTipoMonedaDol Then

                                dblNeto2 = dblNeto1 * dblTipoCambio

                            Else

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                            'ElseIf strMoneda = gstrTipoMonedaSol Then
                        ElseIf strMoneda <> gstrTipoMonedaDol Then
                            'convertir a dolares
                            If strMoneda_Pago = gstrTipoMonedaDol Then
                                If dblTipoCambio = 0 Then
                                    dblNeto2 = 0
                                Else

                                    dblNeto2 = dblNeto1 / dblTipoCambio

                                End If

                            Else
                                dblTipoCambio = 0

                                dblNeto2 = 0 'dblTotalOrigen
                            End If
                        Else
                            dblTipoCambio = 0

                            'txtTipoCambio.Text = "0.00"

                            dblNeto2 = 0 'dblTotalOrigen
                        End If

                        drow.Cells("Total_TC").Value = dblNeto2

                    End If
                Next


            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Function FlTodasMonedas() As Boolean
        Try
            Dim bln As Boolean = False
            If cboMoneda_Multiple.Items.Count > 0 Then
                If cboMoneda_Multiple.SelectedIndex > -1 Then
                    If cboMoneda_Multiple.SelectedValue.ToString.Trim = "" Then
                        bln = True
                    End If
                End If
            End If
            Return bln
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Function pCalcularTipoCambio_Detracción(Optional ByVal dblDetraccion As Double = 0) As Double
        If blnLoad = False Then Exit Function
        Try
            ErrPrvSS.SetError(dtpFechaEmision, "")
            ErrPrvSS.SetError(cboMoneda, "")
            blnDetraccionError = False
            blnDetraccionErrorMoneda = False

            Dim dblTipoCambio As Double = 0 '= CDbl(dr("ValVenta").ToString.Trim)
            ' Dim dblDetracción As Double = 0

            'If txtSsDetraccion.Text.Trim.Length > 0 Then
            '    dblDetracción = CDbl(txtSsDetraccion.Text)
            'End If

            If validarTipoCambio_Proveedor() Then

                'If chkTipoCambio.Checked = True Then
                Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
                'objBN.ConsultarPk
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
                    dr.Read()
                    If dr.HasRows Then

                        dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)

                        Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
                        Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

                        'txtTipoCambio.Text = dr("ValVenta").ToString.Trim
                        'txtTipoCambioFEgreso.Text = dr("ValVenta").ToString.Trim

                        If txtMonto.Text.Trim.Length > 0 Then
                            dblTotalOrigen = CDbl(txtMonto.Text)
                            dblTotalOrigen2 = CDbl(txtMonto.Text)
                        End If

                        Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                        Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                        If strMoneda = gstrTipoMonedaDol Then

                            dblDetraccion = dblDetraccion * dblTipoCambio

                        ElseIf strMoneda = gstrTipoMonedaSol Then

                        Else
                            dblTipoCambio = 0
                            ErrPrvSS.SetError(cboMoneda, "Detracción: No existe un tipo de cambio para la moneda indicada.")
                            blnDetraccionErrorMoneda = True
                            Return 0
                        End If

                    End If
                    dr.Close()
                End Using
                'End If

                If dblTipoCambio = 0 Then
                    'alerta
                    ErrPrvSS.SetError(dtpFechaEmision, "Detracción: No existe un tipo de cambio para la fecha indicada.")
                    blnDetraccionError = True
                    Return 0
                Else
                    ErrPrvSS.SetError(dtpFechaEmision, "")
                    ErrPrvSS.SetError(cboMoneda, "")
                    blnDetraccionError = False
                    blnDetraccionErrorMoneda = False
                    Return Math.Round(dblDetraccion, 2)

                End If
            Else

                'cuando el proveedor tiene tipo de cambio
                'Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


                dblTipoCambio = dblTipoCambioProveedor



                Dim strMoneda As String = cboMoneda.SelectedValue.ToString
                Dim strMoneda_Pago As String = cboMoneda.SelectedValue.ToString 'cboMoneda_Pago.SelectedValue.ToString

                If strMoneda = gstrTipoMonedaDol Then

                    dblDetraccion = dblDetraccion * dblTipoCambio

                ElseIf strMoneda = gstrTipoMonedaSol Then

                Else
                    dblTipoCambio = 0
                    blnDetraccionErrorMoneda = True
                    ErrPrvSS.SetError(cboMoneda, "No existe un tipo de cambio para la moneda indicada.")
                    Return 0
                End If


            End If

            ErrPrvSS.SetError(dtpFechaEmision, "")
            ErrPrvSS.SetError(cboMoneda, "")
            blnDetraccionError = False
            blnDetraccionErrorMoneda = False
            Return Math.Round(dblDetraccion, 2)

            'If dblTipoCambio = 0 Then
            '    'alerta
            '    ErrPrvSS.SetError(dtpFechaEmision, "No existe un tipo de cambio para la fecha indicada.")
            '    Exit Function
            'Else
            '    ErrPrvSS.SetError(dtpFechaEmision, "")
            '    Return dblDetracción
            'End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Sub pCalcularTipoCambio_Anterior()
        'If blnLoad = False Then Exit Sub
        'Try
        '    'If chkTipoCambio.Checked = True Then
        '    Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
        '    'objBN.ConsultarPk
        '    Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
        '        dr.Read()
        '        If dr.HasRows Then

        '            Dim dblTipoCambio As Double '= CDbl(dr("ValVenta").ToString.Trim)


        '            dblTipoCambio = CDbl(dr("ValVenta").ToString.Trim)


        '            Dim dblTotalOrigen As Double = 0, dblTotalOrigen2 As Double = 0
        '            Dim dblTotalDestino As Double = 0, dblTotalDestino2 As Double = 0

        '            txtTipoCambio.Text = dr("ValVenta").ToString.Trim
        '            txtTipoCambioFEgreso.Text = dr("ValVenta").ToString.Trim



        '            If txtMonto.Text.Trim.Length > 0 Then
        '                dblTotalOrigen = CDbl(txtMonto.Text)
        '                dblTotalOrigen2 = CDbl(txtMonto.Text)
        '            End If

        '            Dim strMoneda As String = cboMoneda.SelectedValue.ToString
        '            Dim strMoneda_Pago As String = cboMoneda_Pago.SelectedValue.ToString
        '            If chkTipoCambio.Checked = True Then
        '                If strMoneda = gstrTipoMonedaDol Then

        '                    If strMoneda_Pago = "SOL" Then
        '                        dblTotalDestino = dblTotalOrigen * dblTipoCambio
        '                    Else
        '                        dblTipoCambio = 0
        '                        txtTipoCambio.Text = "0.00"

        '                        dblTotalDestino = 0 'dblTotalOrigen
        '                    End If


        '                ElseIf strMoneda = "SOL" Then
        '                    'convertir a dolares
        '                    If strMoneda_Pago = gstrTipoMonedaDol Then
        '                        dblTotalDestino = dblTotalOrigen / dblTipoCambio
        '                    Else
        '                        dblTipoCambio = 0

        '                        txtTipoCambio.Text = "0.00"

        '                        dblTotalDestino = 0 'dblTotalOrigen
        '                    End If

        '                Else
        '                    dblTipoCambio = 0

        '                    txtTipoCambio.Text = "0.00"

        '                    'txtTipoCambio.Text = "0.00"
        '                    dblTotalDestino = 0 'dblTotalOrigen
        '                End If

        '                'dblTotalDestino = dblTotalOrigen / dblTipoCambio
        '                txtTotalTC.Text = Format(dblTotalDestino, "###,##0.00")
        '            End If

        '            strMoneda = cboMoneda.SelectedValue.ToString
        '            strMoneda_Pago = strMonedaFEgreso
        '            If strMoneda = gstrTipoMonedaDol Then
        '                If strMoneda_Pago = "SOL" Then
        '                    dblTotalDestino2 = dblTotalOrigen2 * dblTipoCambio
        '                Else
        '                    dblTipoCambio = 0

        '                    txtTipoCambioFEgreso.Text = "0.00"

        '                    dblTotalDestino2 = 0 'dblTotalOrigen
        '                End If
        '            ElseIf strMoneda = "SOL" Then
        '                'convertir a dolares
        '                If strMoneda_Pago = gstrTipoMonedaDol Then
        '                    dblTotalDestino2 = dblTotalOrigen2 / dblTipoCambio
        '                Else
        '                    dblTipoCambio = 0

        '                    txtTipoCambioFEgreso.Text = "0.00"

        '                    dblTotalDestino2 = 0 'dblTotalOrigen
        '                End If
        '            Else
        '                dblTipoCambio = 0

        '                txtTipoCambio.Text = "0.00"

        '                dblTotalDestino2 = 0 'dblTotalOrigen
        '            End If

        '            txtTotalFEgreso.Text = Format(dblTotalDestino2, "###,##0.00")
        '        Else

        '            txtTipoCambioFEgreso.Text = "0.00"
        '            txtTotalFEgreso.Text = "0.00"
        '        End If
        '        dr.Close()
        '    End Using
        '    'End If

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    'Private Sub pCalcularTipoCambio0()
    '    Try
    '        If chkTipoCambio.Checked = True Then
    '            Dim objBN As New clsTablasApoyoBN.clsTiposdeCambioBN
    '            'objBN.ConsultarPk

    '            Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(dtpFechaEmision.Value.Date)
    '                dr.Read()
    '                If dr.HasRows Then
    '                    Dim dblTipoCambio As Double = CDbl(dr("ValVenta").ToString.Trim)
    '                    Dim dblTotalOrigen As Double = 0
    '                    Dim dblTotalDestino As Double = 0
    '                    txtTipoCambio.Text = dr("ValVenta").ToString.Trim

    '                    If txtMonto.Text.Trim.Length > 0 Then
    '                        dblTotalOrigen = CDbl(txtMonto.Text)
    '                    End If

    '                    Dim strMoneda As String = cboMoneda.SelectedValue.ToString

    '                    If strMoneda = gstrTipoMonedaDol Then
    '                        'convertir a soles
    '                        dblTotalDestino = dblTotalOrigen * dblTipoCambio
    '                        'Else
    '                        '    convertir a dolares
    '                        '    dblTotalDestino = dblTotalOrigen / dblTipoCambio

    '                        'End If

    '                    ElseIf strMoneda = "SOL" Then
    '                        'convertir a dolares
    '                        dblTotalDestino = dblTotalOrigen / dblTipoCambio
    '                    Else
    '                        dblTipoCambio = 0
    '                        txtTipoCambio.Text = "0.00"
    '                        dblTotalDestino = 0 'dblTotalOrigen
    '                    End If

    '                    'dblTotalDestino = dblTotalOrigen / dblTipoCambio

    '                    txtTotalFEgreso.Text = Format(dblTotalDestino, "###,##0.00")
    '                Else
    '                    txtTipoCambioFEgreso.Text = "0.00"
    '                    txtTotalFEgreso.Text = "0.00"
    '                End If
    '                dr.Close()
    '            End Using
    '        End If


    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    Private Sub txtTipoCambioFEgreso_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTipoCambioFEgreso.TextChanged
        If sender Is Nothing Then Exit Sub
        If Not blnLoad Then Exit Sub
        If blnTipoCambio_TXT Then Exit Sub
        ErrPrv.SetError(DirectCast(sender, Control), "")

        If sender.Name = "txtTipoCambioFEgreso" Then

            'pCalcularTipoCambio()
            If txtTipoCambioFEgreso.Text.Trim.Length > 0 Then

                If Not blnEsMultiple() Then
                    Dim dblTipoCambio As Double = CDbl(txtTipoCambioFEgreso.Text)
                    Dim dblTotalOrigen As Double = 0
                    Dim dblTotalDestino As Double = 0


                    If txtMonto.Text.Trim.Length > 0 Then
                        dblTotalOrigen = CDbl(txtMonto.Text)
                    End If

                    Dim strMoneda As String = cboMoneda.SelectedValue.ToString

                    If strMoneda = gstrTipoMonedaDol Then
                        'convertir a soles
                        dblTotalDestino = dblTotalOrigen * dblTipoCambio
                    Else
                        'convertir a dolares
                        If dblTipoCambio = 0 Then
                            dblTotalDestino = 0
                        Else
                            dblTotalDestino = dblTotalOrigen / dblTipoCambio
                        End If
                    End If
                    txtTotalFEgreso.Text = Format(dblTotalDestino, "###,##0.00")
                    'If blnEsMultiple() Then
                    '    pCalcularTipoCambio_Multiple(dblTipoCambio)
                    '    pCalcularTipoCambio_Multiple_Detalle(dblTipoCambio)
                    'End If
                Else
                    'Dim dblTipoCambio As Double = CDbl(txtTipoCambioFEgreso.Text)
                    'pCalcularTipoCambio_Multiple(dblTipoCambio)
                    'pCalcularTipoCambio_Multiple_Detalle(dblTipoCambio)

                    'Dim dblTipoCambio As Double = CDbl(txtTipoCambioFEgreso.Text)
                    'Dim dblTotalOrigen As Double = 0
                    'Dim dblTotalDestino As Double = 0


                    'If txtMonto.Text.Trim.Length > 0 Then
                    '    dblTotalOrigen = CDbl(txtMonto.Text)
                    'End If

                    'Dim strMoneda As String = cboMoneda.SelectedValue.ToString

                    'If strMoneda = gstrTipoMonedaDol Then
                    '    'convertir a soles
                    '    dblTotalDestino = dblTotalOrigen / dblTipoCambio
                    'Else
                    '    'convertir a dolares
                    '    If dblTipoCambio = 0 Then
                    '        dblTotalDestino = 0
                    '    Else
                    '        dblTotalDestino = dblTotalOrigen * dblTipoCambio
                    '    End If
                    'End If
                    'txtTotalFEgreso.Text = Format(dblTotalDestino, "###,##0.00")
                    ''If blnEsMultiple() Then
                    ''    pCalcularTipoCambio_Multiple(dblTipoCambio)
                    ''    pCalcularTipoCambio_Multiple_Detalle(dblTipoCambio)
                    ''End If
                End If

            Else
                txtTotalFEgreso.Text = "0.00"
            End If


        End If
    End Sub

    'Private Sub txtTipoCambio_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTipoCambio.TextChanged
    '    If sender Is Nothing Then Exit Sub
    '    ErrPrv.SetError(DirectCast(sender, Control), "")

    '    If chkTipoCambio.Checked = True Then
    '        If sender.Name = "txtTipoCambio" Then
    '            'pCalcularTipoCambio()
    '            If txtTipoCambio.Text.Trim.Length > 0 Then
    '                Dim dblTipoCambio As Double = CDbl(txtTipoCambio.Text)
    '                Dim dblTotalOrigen As Double = 0
    '                Dim dblTotalDestino As Double = 0


    '                If txtMonto.Text.Trim.Length > 0 Then
    '                    dblTotalOrigen = CDbl(txtMonto.Text)
    '                End If

    '                Dim strMoneda As String = cboMoneda.SelectedValue.ToString

    '                If strMoneda = gstrTipoMonedaDol Then
    '                    'convertir a soles
    '                    dblTotalDestino = dblTotalOrigen * dblTipoCambio
    '                Else
    '                    'convertir a dolares
    '                    If dblTipoCambio = 0 Then
    '                        dblTotalDestino = 0
    '                    Else
    '                        dblTotalDestino = dblTotalOrigen / dblTipoCambio
    '                    End If
    '                End If
    '                txtTotalTC.Text = Format(dblTotalDestino, "###,##0.00")
    '            Else
    '                txtTotalTC.Text = "0.00"
    '            End If


    '        End If
    '    End If
    'End Sub

    Private Sub cboTipoDocumento_SelectedValueChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoOperacion.SelectionChangeCommitted
        Try

            'If sender.name = "cboTipoDocumento" Then
            '    ErrPrv.SetError(cboTipoDocumento, "")
            '    If cboTipoDocumento.Items.Count > 0 Then
            '        Dim strValor As String = sender.SelectedValue.ToString
            '        If strValor <> "System.Data.DataRowView" Then
            '            strTipoDoc = strValor
            '            CargarCombo_TipoOperacion()

            '            CambiarRetencion()
            '            CargarCombo_CuentasContables()
            '            ValidarCheckRetencion()
            '            pCalcularTotal()
            '        End If
            '    End If
            'ElseIf sender.name = "cboTipoOperacion" Then
            If Not blnEsMultiple() Then
                Dim strValor1 As String = sender.SelectedValue.ToString
                If strValor1 <> "System.Data.DataRowView" Then
                    ErrPrv.SetError(cboTipoOperacion, "")
                    strTipOpe = strValor1
                    CambiarRetencion()
                    'End If
                    'CargarCombo_CuentasContables()
                    ValidarCheckRetencion()
                    pCalcularTotal()
                End If
                'End If
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub CambiarTipoOpe()
        Try
            Dim objValor As Object = cboTipoOperacion.SelectedValue
            If Not objValor Is Nothing Then
                Dim strValor1 As String = cboTipoOperacion.SelectedValue.ToString
                If strValor1 <> "System.Data.DataRowView" Then
                    ErrPrv.SetError(cboTipoOperacion, "")
                    strTipOpe = strValor1
                    CambiarRetencion()
                    'End If
                    'CargarCombo_CuentasContables()
                    ValidarCheckRetencion()
                    pCalcularTotal()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CambiarRetencion1()
        Try
            If cboTipoDocumento.Items.Count > 0 Then

                If cboTipoDocumento.SelectedValue Is Nothing = False Then

                    Dim strTipoDoc1 As String = cboTipoDocumento.SelectedValue.ToString

                    strTipoDoc = strTipoDoc1
                    'strTipOpe = strTipOpe1
                    If strTipoDoc1.Trim = "RH" Then
                        'If strTipOpe1.Trim = "007" Then
                        chkIGV.Text = "Retención (10%):"
                        'Else
                        '    chkIGV.Text = "IGV:"
                        'End If
                    Else
                        chkIGV.Text = "IGV:"
                    End If

                End If


            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CambiarRetencion()
        Try
            Dim dblPorcentajeRenta As Double = 10
            Dim dblPorcentajeRenta2 As Double = 0

            If cboTipoDocumento.Items.Count > 0 Then

                If cboTipoDocumento.SelectedValue Is Nothing = False Then
                    Dim strTipoDoc1 As String = cboTipoDocumento.SelectedValue.ToString
                    strTipoDoc = strTipoDoc1

                    'If strTipoDoc1.Trim = "RH" Then
                    If (strTipoDoc1.Trim = "RH" Or strTipoDoc1.Trim = "HTD") Then
                        chkIGV.Text = "Retención (10%):"

                    Else
                        chkIGV.Text = "IGV:"
                        chkIGV.Enabled = True
                    End If

                    If cboTipoOperacion.Items.Count > 0 Then
                        If cboTipoOperacion.SelectedValue Is Nothing = False Then

                            Dim strTipOpe1 As String = cboTipoOperacion.SelectedValue.ToString


                            'obtener porcentaje de retencion segun el destino de operación
                            Dim objDO_BN As New clsTablasApoyoBN.clsTipoOperacContabBN
                            'Using dr As SqlClient.SqlDataReader = objDO_BN.ConsultarPk_DestOper(strTipOpe1)
                            Using dr As SqlClient.SqlDataReader = objDO_BN.ConsultarPk(strTipOpe1)
                                dr.Read()
                                If dr.HasRows Then
                                    dblPorcentajeRenta = CDbl(dr("SsTasa").ToString.Trim)
                                    dblPorcentajeRenta2 = CDbl(dr("SsTasa").ToString.Trim)
                                Else

                                End If
                                dr.Close()
                            End Using


                            strTipOpe = strTipOpe1
                            'If strTipoDoc1.Trim = "RH" Then
                            If (strTipoDoc1.Trim = "RH" Or strTipoDoc1.Trim = "HTD") Then
                                'chkIGV.Text = "Retención (10%):"
                                If dblPorcentajeRenta > 0 Then
                                    chkIGV.Text = "Retención (" + CInt(dblPorcentajeRenta).ToString + "%)"
                                Else
                                    chkIGV.Text = "Retención (10%):"
                                End If
                                If strTipOpe1.Trim = "007" Then
                                    chkIGV.Enabled = True
                                ElseIf strTipOpe1.Trim = "010" Then
                                    chkIGV.Checked = False
                                    chkIGV.Enabled = False
                                    txtIGV.Text = "0.00"
                                End If
                                If dblPorcentajeRenta2 > 0 Then
                                    chkIGV.Enabled = True
                                    chkIGV.Checked = True
                                End If
                            Else
                                chkIGV.Text = "IGV:"
                                chkIGV.Enabled = True

                                'If strTipOpe = "001" Or strTipOpe = "002" Or strTipOpe = "003" Then
                                '    chkIGV.Checked = True
                                'Else
                                '    chkIGV.Checked = False
                                'End If
                            End If
                        End If
                    End If

                End If

                If chkIGV.Text = "IGV:" Then
                    If strTipOpe = "001" Or strTipOpe = "012" Or strTipOpe = "002" Then
                        chkIGV.Checked = True
                    Else
                        chkIGV.Checked = False
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ValidarCheckRetencion()
        Try
            If chkIGV.Text.Contains("Retención") And chkIGV.Checked = False Then

                Dim dblNeto As Double = 0

                If txtSsNeto.Text.Trim.Length > 0 Then
                    dblNeto = CDbl(txtSsNeto.Text)
                End If

                Dim dblLimite As Double = gdblRHLimiteSinRetencion

                If (strCoUbigeo_Oficina = gstrBuenosAires) Then
                    dblLimite = dblRHLimiteSinRetencion_Arg
                End If

                If (strCoUbigeo_Oficina = gstrSantiago) Then
                    dblLimite = dblRHLimiteSinRetencion_Chile
                End If

                'If dblNeto >= gdblRHLimiteSinRetencion Then
                If dblNeto >= dblLimite Then

                    ErrPrvSS.SetError(chkIGV, "Advertencia: No está aplicando Retención pese a que el monto sobrepasa o es igual al monto límite (" & dblLimite.ToString & ").")

                Else
                    ErrPrvSS.SetError(chkIGV, "")
                End If

            Else
                ErrPrvSS.SetError(chkIGV, "")
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtNuDocumento_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtNuDocumento.TextChanged
        ErrPrv.SetError(txtNuDocumento, "")
    End Sub

    Private Sub cboTipoDocumento_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cboTipoDocumento.SelectedIndexChanged

    End Sub

    Private Sub btnSelProveedor_Click(sender As Object, e As EventArgs) Handles btnSelProveedor.Click
        Dim frmAyuda As New frmAyudas
        With frmAyuda
            .strCaller = Me.Name
            .strTipoBusqueda = "Proveedor"
            .frmRefDocumentosProveedorDato = Me
            .strPar = "" 'gstrTipoProveeAdminist

            .Text = "Consulta de Proveedores"
            .ShowDialog()
            If strCoProveedorAdminist <> "" Then
                'If strCoProveedorAdminist = "000545" Or strCoProveedorAdminist = "001836" Then ' peru rail
                '    strTipoVoucheruOrServ = "PRL"
                'End If
                ErrPrv.SetError(btnSelProveedor, "")
                vstrIDProveedor = strCoProveedorAdminist
                DevolverDatosProveedor_SAP()
            End If

        End With
    End Sub

    Private Sub cboTipoDocumento_SelectedValueChanged_1(sender As Object, e As EventArgs) Handles cboTipoDocumento.SelectedValueChanged
        If cboTipoDocumento.SelectedIndex > -1 Then
            Dim strValor1 As String = sender.SelectedValue.ToString
            If strValor1 <> "System.Data.DataRowView" Then
                ErrPrv.SetError(cboTipoDocumento, "")

                If strValor1.Trim = "RH" Then
                    lblTipoDetraccion.Visible = False
                    lblDetracciones.Visible = False
                    txtSsDetraccion.Visible = False
                    cboTipoDetraccion.Visible = False
                    'If strCoUbigeo_Oficina = gstrBuenosAires Then
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        chkIGV.Text = "Retención (2%):"
                        If strCoUbigeo_Oficina = gstrSantiago Then
                            chkIGV.Text = "Retención (10%):"
                        End If
                        If chkIGV.Checked Then
                            pCalcularTotal()
                        End If
                    End If
                Else
                    ' If strCoUbigeo_Oficina <> gstrBuenosAires Then
                    If (strCoUbigeo_Oficina = gstrLima Or strCoUbigeo_Oficina = gstrCusco) Then
                        lblTipoDetraccion.Visible = True
                        lblDetracciones.Visible = True
                        txtSsDetraccion.Visible = True
                        cboTipoDetraccion.Visible = True
                    End If
                    'If strCoUbigeo_Oficina = gstrBuenosAires Then
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        chkIGV.Text = "IVA (21%):"
                        If strCoUbigeo_Oficina = gstrSantiago Then
                            chkIGV.Text = "IVA (19%):"
                        End If
                        If chkIGV.Checked Then
                            pCalcularTotal()
                        End If
                    End If
                End If

                If strValor1.Trim = "NCR" Or strValor1.Trim = "NDB" Or strValor1.Trim = "NCE" Then
                    gbxDocumentoVinculado.Visible = True
                Else
                    gbxDocumentoVinculado.Visible = False
                End If

                If strTipoVoucheruOrServ = "PST" Or strTipoVoucheruOrServ = "PTT" Then
                    If strValor1 = "OTR" Then
                        If cboTipoOperacion.Items.Count > 0 Then
                            cboTipoOperacion.SelectedValue = "004"
                        Else
                            Dim objDocBN As New clsDocumentoProveedorBN
                            pCargaCombosBox(cboTipoOperacion, objDocBN.ConsultarTipoOperacionCont)
                            cboTipoOperacion.SelectedValue = "004"
                        End If
                    End If

                End If

                If (strValor1 = "HTD") Then
                    txtMonto.Enabled = True
                Else
                    txtMonto.Enabled = False
                End If

                CargarGastos()

            End If
        End If
    End Sub

    Private Sub CargarGastos()
        Try

            ' Dim strTipoDoc As String = ""
            Dim strTipoOpe As String = ""
            Dim strCecos As String = ""

            If cboTipoDocumento.Items.Count > 0 Then
                If Not cboTipoDocumento.SelectedValue Is Nothing Then
                    strTipoDoc = cboTipoDocumento.SelectedValue.ToString

                    Dim objBN As New clsDocumentoProveedorBN
                    pCargaCombosBox(cboGastos, objBN.ConsultarGastosAdicionales_Cbo(strTipoDoc), False)
                    If cboGastos.Items.Count = 1 Then
                        cboGastos.SelectedIndex = 0
                    End If

                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cboMoneda_SelectedValueChanged_1(sender As Object, e As EventArgs) Handles cboMoneda.SelectedValueChanged
        Dim strValor1 As String = sender.SelectedValue.ToString
        If strValor1 <> "System.Data.DataRowView" Then
            If blnMNME Then
                cambiarCuenta_MNME_Moneda()
            End If

            Dim strTipoSAP As String = ""
            If cboTipoSAP.Items.Count > 0 Then
                If Not cboTipoSAP.SelectedValue Is Nothing Then
                    strTipoSAP = cboTipoSAP.SelectedValue.ToString.Trim
                    If strTipoSAP = "2" Then
                        'CargarVouchersxProveedor()

                    End If
                End If
            End If

        End If
    End Sub

    'Private Sub cboCentroCostos_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboCentroCostos.SelectedValueChanged
    '    Try
    '        Dim strValor1 As String = sender.SelectedValue.ToString
    '        If strValor1 <> "System.Data.DataRowView" Then
    '            ErrPrv.SetError(cboCentroCostos, "")
    '            strCoCeCos = strValor1
    '            CargarCombo_TiposdeOperacion_CeCos()
    '            CargarCombo_CuentasContables_CeCos()
    '            'strTipOpe = strValor1
    '            'CambiarRetencion()
    '            ''End If
    '            'CargarCombo_CuentasContables()
    '            'ValidarCheckRetencion()
    '            'pCalcularTotal()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub

    Private Sub CambiarValor_CentroCostos()
        Try
            Dim strValor1 As String = cboCentroCostos.SelectedValue.ToString
            'If strValor1 <> "System.Data.DataRowView" Then
            ErrPrv.SetError(cboCentroCostos, "")
            strCoCeCos = strValor1
            CambiarRetencion()
            'CargarCombo_TiposdeOperacion_CeCos()
            CargarCombo_CuentasContables_CeCos()
            ValidarCheckRetencion()
            pCalcularTotal()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cboCentroCostos_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboCentroCostos.SelectionChangeCommitted
        Try
            Dim strValor1 As String = sender.SelectedValue.ToString
            If strValor1 <> "System.Data.DataRowView" Then
                ErrPrv.SetError(cboCentroCostos, "")
                strCoCeCos = strValor1
                CambiarRetencion()
                'CargarCombo_TiposdeOperacion_CeCos()

                If Not (strTipoVoucheruOrServ = "OTR" And lblIDFile.Text.Trim.Length = 0) Then

                    'Else
                    CargarCombo_CuentasContables_CeCos()
                End If

                If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP" Then
                    CargarCuentas_Origen_Otros()
                End If
                Cuentas_FechaOut()
                If Not blnEsMultiple() Then
                    ValidarCheckRetencion()
                    pCalcularTotal()
                End If

                'strTipOpe = strValor1
                'CambiarRetencion()
                ''End If
                'CargarCombo_CuentasContables()
                'ValidarCheckRetencion()
                'pCalcularTotal()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub DevolverDatosProveedor_SAP(Optional ByVal vblnExisteProv As Boolean = False)
        Try
            Dim intDias As Integer = 0
            Using objBN As New clsProveedorBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(vstrIDProveedor)
                    dr.Read()
                    If dr.HasRows Then
                        strFormaPago = dr("IDFormaPago")
                        'intDias = IIf(IsDBNull(dr("DiasCredito")) = True, 0, dr("DiasCredito"))
                    End If
                    dr.Close()
                End Using
            End Using

            Dim objForBN As New clsTablasApoyoBN.clsFormaPagoBN
            Using dr As SqlClient.SqlDataReader = objForBN.ConsultarPk(strFormaPago)
                dr.Read()
                If dr.HasRows Then
                    intDias = IIf(IsDBNull(dr("NuDiasCredito")) = True, 0, dr("NuDiasCredito"))
                End If

                dr.Close()
            End Using

            If intDias > 0 Then
                Dim feRecep As Date = dtpFechaRecepcion.Value
                feRecep = feRecep.AddDays(intDias)
                dtpFechaVencimiento.Value = feRecep
            End If

            If Not vblnExisteProv And strTipoVoucheruOrServ <> "PTT" Then
                Dim strTipoSAP As String = ""
                If cboTipoSAP.Items.Count > 0 Then
                    If Not cboTipoSAP.SelectedValue Is Nothing Then
                        strTipoSAP = cboTipoSAP.SelectedValue.ToString.Trim
                        If strTipoSAP = "2" Then
                            CargarVouchersxProveedor()
                        End If
                    End If
                End If
            End If


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarVouchersxProveedor()
        If cboFormaEgreso.SelectedValue <> "PTT" Then
            If vstrIDProveedor = "" Then
                dgvVouchers.Rows.Clear()
                Exit Sub
            End If
        Else
            cboMoneda_Multiple.SelectedValue = gstrTipoMonedaSol
            cboMoneda.SelectedValue = gstrTipoMonedaSol
            'If intIDCab = 0 Then
            '    dgvVouchers.Rows.Clear()
            '    Exit Sub
            'End If
        End If

        Try

            'If strIDCliente = "" Then GoTo LoadDesgin

            Dim dtt As New Data.DataTable
            Dim strIDMoneda As String = ""
            Dim strIDMoneda_Cambio As String = ""

            If cboMoneda_Multiple.Items.Count > 0 Then
                strIDMoneda = cboMoneda_Multiple.SelectedValue.ToString
            End If

            'dtt = objVoucherBL.ConsultarListVouchersxProveedor(vstrIDProveedor, strIDMoneda)

            If cboFormaEgreso.Items.Count > 0 Then
                If Not cboFormaEgreso.SelectedValue Is Nothing Then
                    'strTipoVoucheruOrServ = "OTR"
                    dgvVouchers.Columns("chkAceptar").Visible = False
                    dgvVouchers.Columns("SaldoOrig").Visible = False
                    dgvVouchers.Columns("SaldoIni").Visible = False
                    If cboFormaEgreso.SelectedValue = "VOU" Then
                        Dim objVoucherBL As New clsVoucherOperacionBN
                        dtt = objVoucherBL.ConsultarListVouchersxProveedor(vstrIDProveedor, strIDMoneda)
                        dgvVouchers.Columns("IDVoucher").HeaderText = "Voucher"
                    ElseIf cboFormaEgreso.SelectedValue = "ZIC" Then
                        Dim objOSVBL As New clsDocumentoProveedorBN
                        dtt = objOSVBL.ConsultarListDocEgresoxProveedor(vstrIDProveedor, strIDMoneda)
                        dgvVouchers.Columns("IDVoucher").HeaderText = "N° Doc. Egreso"
                    ElseIf cboFormaEgreso.SelectedValue = "OPA" Then
                        Dim objOrdenPagoBL As New clsOrdenPagoBN
                        dtt = objOrdenPagoBL.Consultar_Documento_Multiple_Proveedor(vstrIDProveedor, strIDMoneda)
                        dgvVouchers.Columns("IDVoucher").HeaderText = "N° Ord. Pago"
                    ElseIf cboFormaEgreso.SelectedValue = "OSV" Then
                        Dim objOSVBL As New clsDocumentoProveedorBN
                        dtt = objOSVBL.Consultar_Documento_Multiple_Proveedor_OSV(vstrIDProveedor, strIDMoneda)
                        dgvVouchers.Columns("IDVoucher").HeaderText = "N° Ord. Serv."
                    ElseIf cboFormaEgreso.SelectedValue = "PTT" Then
                        Dim objPst As New clsPresupuesto_SobreBN
                        'dtt = objPst.ConsultarTicketsxFile(intIDCab)
                        If intNuDocumProv = 0 Then
                            dtt = objPst.ConsultarTickets
                        Else
                            dtt = objPst.ConsultarTickets(intNuDocumProv)
                        End If

                        dgvVouchers.Columns("IDVoucher").HeaderText = "N° Presup."
                        strTipoVoucheruOrServ = "PTT"
                        dgvVouchers.Columns("chkAceptar").Visible = True
                        dgvVouchers.Columns("SaldoOrig").Visible = True
                        dgvVouchers.Columns("SaldoIni").Visible = True
                    End If
                End If
            End If

            dgvVouchers.Rows.Clear()
            If dtt.Rows.Count > 0 Then

                For Each dRowItem As DataRow In dtt.Rows
                    Dim blnAceptado As Boolean = False
                    Dim blnAsignado As Boolean = False
                    If intNuDocumProv <> 0 And cboFormaEgreso.SelectedValue = "PTT" Then
                        If dRowItem("CoEstadoFormaEgreso") = "AC" Then
                            blnAceptado = True
                        End If
                        blnAsignado = True

                    End If
                    Dim dblSaldoIni As Double = 0
                    If cboFormaEgreso.SelectedValue = "PTT" Then
                        dblSaldoIni = dRowItem("SaldoIni")
                    End If
                    dgvVouchers.Rows.Add(dRowItem("Codigo"), dRowItem("IDVoucher"), _
                                         dRowItem("Titulo"), dRowItem("IDCab"), _
                                         dRowItem("IDFile"), dRowItem("IDMoneda"), _
                                         dRowItem("Simbolo"), dblSaldoIni, dRowItem("Total"), dRowItem("Total"), _
                                         "0.00", dRowItem("IGV"), _
                                         dRowItem("Total"), "0.00", blnAsignado, blnAceptado)
                Next
            End If
            dgvVouchers.Columns("IDVoucher").Width = 80
            dgvVouchers.Columns("Titulo").Width = 250
            dgvVouchers.Columns("IDFile").Width = 80
            dgvVouchers.Columns("Simbolo").Width = 30
            dgvVouchers.Columns("SaldoOrig").Width = 60
            dgvVouchers.Columns("SaldoIni").Width = 60
            dgvVouchers.Columns("Monto").Width = 60
            dgvVouchers.Columns("Gastos").Width = 60
            dgvVouchers.Columns("MontoFinal").Width = 60
            dgvVouchers.Columns("Total_TC").Width = 120
            dgvVouchers.Columns("chkAsignar").Width = 20
            dgvVouchers.Columns("chkAceptar").Width = 20
            'LoadDesgin:
            'CargarTamaniosColumnasDebitMemo()
            'PintarTotales()

            MostrarOcultar_ColumnaTCambio()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MostrarOcultar_ColumnaTCambio()
        Try
            Dim strIDMoneda As String = ""
            Dim strIDMoneda_Cambio As String = ""

            If cboMoneda_Multiple.Items.Count > 0 Then
                strIDMoneda = cboMoneda_Multiple.SelectedValue.ToString
            End If

            If cboMoneda.Items.Count > 0 Then
                strIDMoneda_Cambio = cboMoneda.SelectedValue.ToString
            End If

            If strIDMoneda_Cambio <> strIDMoneda Then
                dgvVouchers.Columns("Total_TC").Visible = True
                Dim strSimbolo As String = ""
                Dim objMonBN As New clsTablasApoyoBN.clsMonedaBN
                Using dr As SqlClient.SqlDataReader = objMonBN.ConsultarPk(strIDMoneda_Cambio)
                    dr.Read()
                    If dr.HasRows Then
                        strSimbolo = dr("Simbolo").ToString.Trim
                        dgvVouchers.Columns("Total_TC").Visible = True
                        dgvVouchers.Columns("Total_TC").HeaderText = "Total (" + strSimbolo + ")"
                    Else
                        'MessageBox.Show("El registro ha sido eliminado recientemente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvVouchers.Columns("Total_TC").Visible = False
                    End If
                    dr.Close()
                End Using
                'End Using
            Else
                dgvVouchers.Columns("Total_TC").Visible = False
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvVouchers_CellEndEdit(sender As Object, e As DataGridViewCellEventArgs) Handles dgvVouchers.CellEndEdit

    End Sub

    Private Sub dgvDetracciones_CurrentCellDirtyStateChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgvVouchers.CurrentCellDirtyStateChanged
        Try
            If dgvVouchers.IsCurrentCellDirty Then
                dgvVouchers.CommitEdit(DataGridViewDataErrorContexts.Commit)
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Function CommitEdit(ByVal context As DataGridViewDataErrorContexts) As Boolean
        'total_aGirar_Marcados()
    End Function

    Private Sub dgvDetracciones_CellValueChanged(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvVouchers.CellValueChanged

        If e.RowIndex < 0 Then Exit Sub
        ErrPrv.SetError(dgvVouchers, "")
        Try
            If dgvVouchers.Columns(e.ColumnIndex).Name <> "chkAsignar" And dgvVouchers.Columns(e.ColumnIndex).Name <> "chkAceptar" Then
                dgvVouchers.Rows(e.RowIndex).Cells("chkAsignar").Value = True
            End If

            Dim dblTotal As Double = 0
            If dgvVouchers.Columns(e.ColumnIndex).Name = "Gastos" Then
                Dim strGasto As String = dgvVouchers.Rows(e.RowIndex).Cells(e.ColumnIndex).Value
                If IsNumeric(strGasto) Then
                    Dim dblNeto As Double = 0
                    If IsNumeric(dgvVouchers.Rows(e.RowIndex).Cells("Monto").Value.ToString) Then
                        dblNeto = dgvVouchers.Rows(e.RowIndex).Cells("Monto").Value.ToString
                    End If
                    dgvVouchers.Rows(e.RowIndex).Cells("MontoFinal").Value = dblNeto - CDbl(strGasto)
                End If
            End If
            If dgvVouchers.Columns(e.ColumnIndex).Name = "MontoFinal" Then
                Dim strGasto As String = dgvVouchers.Rows(e.RowIndex).Cells("Gastos").Value
                If IsNumeric(strGasto) Then

                    If IsNumeric(dgvVouchers.Rows(e.RowIndex).Cells("MontoFinal").Value.ToString) Then
                        dblTotal = dgvVouchers.Rows(e.RowIndex).Cells("MontoFinal").Value.ToString
                    End If

                    dgvVouchers.Rows(e.RowIndex).Cells("Monto").Value = dblTotal + CDbl(strGasto)
                End If

                If strTipoVoucheruOrServ = "PTT" Then
                    Dim dblSaldoOrig As Double = 0
                    If IsNumeric(dgvVouchers.Rows(e.RowIndex).Cells("SaldoOrig").Value.ToString) Then
                        dblSaldoOrig = dgvVouchers.Rows(e.RowIndex).Cells("SaldoOrig").Value.ToString
                    End If
                    If dblTotal = dblSaldoOrig Then
                        dgvVouchers.Rows(e.RowIndex).Cells("chkAceptar").Value = True
                    Else
                        dgvVouchers.Rows(e.RowIndex).Cells("chkAceptar").Value = False
                    End If
                End If
            End If

            If dgvVouchers.Columns(e.ColumnIndex).Name = "chkAsignar" Then
                If dgvVouchers.Rows(e.RowIndex).Cells("chkAsignar").Value = True Then
                    If IsNumeric(dgvVouchers.Rows(e.RowIndex).Cells("MontoFinal").Value.ToString) Then
                        dblTotal = dgvVouchers.Rows(e.RowIndex).Cells("MontoFinal").Value.ToString
                    End If
                    Dim dblSaldoOrig As Double = 0
                    If IsNumeric(dgvVouchers.Rows(e.RowIndex).Cells("SaldoOrig").Value.ToString) Then
                        dblSaldoOrig = dgvVouchers.Rows(e.RowIndex).Cells("SaldoOrig").Value.ToString
                    End If
                    If dblTotal = dblSaldoOrig Then
                        dgvVouchers.Rows(e.RowIndex).Cells("chkAceptar").Value = True
                    Else
                        dgvVouchers.Rows(e.RowIndex).Cells("chkAceptar").Value = False
                    End If
                Else
                    dgvVouchers.Rows(e.RowIndex).Cells("chkAceptar").Value = False
                End If
            End If
            If Not blnTipoCambio_TXT Then
                calculalTotalMarcados()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub calculalTotalMarcados(Optional ByVal vblnTipoCambio As Boolean = False)
        Try
            dblIGV_Multiple = 0
            Dim dblTotal As Double = 0
            Dim dblTotalFinal As Double = 0
            Dim dblGastos As Double = 0
            For Each drow As DataGridViewRow In dgvVouchers.Rows
                If drow.Cells("chkAsignar").Value = True Then
                    If IsNumeric(drow.Cells("Monto").Value) Then
                        dblTotal = dblTotal + CDbl(drow.Cells("Monto").Value)
                    End If
                    If IsNumeric(drow.Cells("MontoFinal").Value) Then
                        dblTotalFinal = dblTotalFinal + CDbl(drow.Cells("MontoFinal").Value)
                    End If
                    If IsNumeric(drow.Cells("Gastos").Value) Then
                        dblGastos = dblGastos + CDbl(drow.Cells("Gastos").Value)
                    End If
                    If IsNumeric(drow.Cells("IGV_Multiple").Value) Then
                        dblIGV_Multiple = dblIGV_Multiple + CDbl(drow.Cells("IGV_Multiple").Value)
                    End If
                End If
            Next
            'lblTotal.Text = Format(dblTotal, "###,###0.00")
            'txtSsNeto.Text = Format(dblTotal, "###,###0.00")
            txtSsOtrCargos.Text = Format(dblGastos, "###,###0.00")
            'txtMonto.Text = Format(dblTotal, "###,###0.00")
            txtMonto.Text = Format(dblTotal, "###,###0.00")
            'txtSsNeto.Text = Format(dblTotalFinal, "###,###0.00")
            txtSsNeto.Text = Format(dblTotalFinal - dblIGV_Multiple, "###,###0.00")

            If cboFormaEgreso.SelectedValue <> "PTT" Then
                chkIGV.Enabled = True
                chkIGV.Checked = True

                If dblIGV_Multiple > 0 Then ' si tiene igv
                    'chkIGV.Enabled = True
                    'chkIGV.Checked = True
                    chkIGV.Text = "IGV:"
                Else
                    'chkIGV.Enabled = True
                    'chkIGV.Checked = True
                    txtIGV.Text = "0.00"
                End If

                If chkIGV.Checked Then
                    txtIGV.Text = Format(dblIGV_Multiple, "###,###0.00")
                End If
            Else
                CalculosIGV()
            End If

            If Not vblnTipoCambio Then
                If Not FlTodasMonedas() Then
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_Arg()
                    Else
                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle()
                    End If

                Else
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas_Arg()
                    Else
                        pCalcularTipoCambio_Multiple()
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas()
                    End If

                End If

            End If

        Catch ex As Exception
            'MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            Throw
        End Try
    End Sub

    Private Sub dtpFechaRecepcion_ValueChanged(sender As Object, e As EventArgs) Handles dtpFechaRecepcion.ValueChanged
        Try
            If dtpFechaRecepcion.Value.Date > Now Then
                dtpFechaRecepcion.Value = Now
            End If
            If vstrIDProveedor.Trim.Length > 0 Then
                DevolverDatosProveedor_SAP()
            End If
            Cuentas_FechaOut()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnNuevoDetalleItem_Click_1(sender As Object, e As EventArgs) Handles btnNuevoDetalleItem.Click
        Try
            Dim frm As New frmDocumentosProveedorDatoMem
            frm.frmRef = Me
            frm.ShowDialog()
            If blnActualizoItinerario Then
                AgregarEditarEliminarItinerario(dgvDetalle.Rows.Count - 1, "N")
                blnActualizoItinerario = False
                blnCambios = True
                CalcularTotales_Detalle()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CambiarTamanio_Detalle()
        Try
            Dim strTipoDoc As String = cboTipoSAP.SelectedValue
            cboMoneda_Multiple.Visible = False
            lblEtiqMonedaMultiple.Visible = False
            txtSsOtrCargos.Enabled = True
            If strTipoDoc = "0" Then
                grbDetalle.Visible = True
                Me.Size = New Size(1262, 633)

                Me.dgvDetalle.Visible = True
                Me.btnNuevoDetalleItem.Visible = True
                Me.dgvVouchers.Visible = False
                Me.btnNuevoDetalleItem.BringToFront()
                lblFormaEgreso.Visible = False
                cboFormaEgreso.Visible = False
                Me.dgvDetalle.Dock = DockStyle.Top
                If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP" Then
                    lblEtiqFile.Visible = True
                    lblIDFile.Visible = True
                    btnConsultarFile.Visible = True
                    btnLimpiarFile.Visible = True
                Else
                    lblEtiqFile.Visible = False
                    lblIDFile.Visible = False
                    btnConsultarFile.Visible = False
                    btnLimpiarFile.Visible = False
                End If

            ElseIf strTipoDoc = "1" Then
                grbDetalle.Visible = False
                Me.Size = New Size(1262, 421)

                'If strTipoVoucheruOrServ = "PST" Then
                If cboCentroControl.Items.Count > 0 Then
                    cboCentroControl.SelectedValue = "000000"
                Else
                    Dim objCenConBN As New clsCentroControlBN
                    pCargaCombosBox(cboCentroControl, objCenConBN.ConsultarListCbo)
                    cboCentroControl.SelectedValue = "000000"
                End If

                'Me.dgvDetalle.Visible = True
                'Me.btnNuevoDetalleItem.Visible = True
                'Me.dgvVouchers.Visible = False

                'End If

                If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP" Then
                    lblEtiqFile.Visible = True
                    lblIDFile.Visible = True
                    btnConsultarFile.Visible = True
                    btnLimpiarFile.Visible = True
                Else
                    lblEtiqFile.Visible = False
                    lblIDFile.Visible = False
                    btnConsultarFile.Visible = False
                    btnLimpiarFile.Visible = False
                End If

            ElseIf strTipoDoc = "2" Then 'multiple
                grbDetalle.Visible = True
                Me.Size = New Size(1262, 633)
                Me.dgvDetalle.Dock = DockStyle.None
                Me.dgvDetalle.Visible = False
                Me.btnNuevoDetalleItem.Visible = False
                Me.dgvVouchers.Visible = True

                lblFormaEgreso.Visible = True
                cboFormaEgreso.Visible = True
                cboMoneda_Multiple.Visible = True
                lblEtiqMonedaMultiple.Visible = True
                If cboCentroCostos.Items.Count > 0 Then
                    If cboCentroCostos.SelectedValue Is Nothing Then
                        cboCentroCostos.SelectedValue = "940101"
                    End If
                Else
                    Dim objCenCosBN As New clsCentroCostosBN
                    pCargaCombosBox(cboCentroCostos, objCenCosBN.ConsultarListCbo)
                    If cboCentroCostos.SelectedValue Is Nothing Then
                        cboCentroCostos.SelectedValue = "940101"
                    End If
                End If



                'If strTipoVoucheruOrServ = "PST" Then
                If cboCentroControl.Items.Count > 0 Then
                    If cboCentroCostos.SelectedValue Is Nothing Then
                        cboCentroControl.SelectedValue = "000000"
                    End If
                Else
                    Dim objCenConBN As New clsCentroControlBN
                    pCargaCombosBox(cboCentroControl, objCenConBN.ConsultarListCbo)
                    If cboCentroCostos.SelectedValue Is Nothing Then
                        cboCentroControl.SelectedValue = "000000"
                    End If
                End If
                'End If
                If lblProveedor.Text.Trim.Length > 0 Then
                    DevolverDatosProveedor_SAP()
                End If

                txtSsNeto.Enabled = False
                txtSsOtrCargos.Enabled = False

                ActivarOcultarFileProveedor()


                'cargar cuentas origen otros con file
                CargarCuentas_Origen_Otros()

                If strTipoVoucheruOrServ = "OTR" Then
                    If FlTodasMonedas() Then
                        grbTipoCambioFormaEgreso.Visible = True
                    End If
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cboTipoSAP_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboTipoSAP.SelectedValueChanged
        If cboTipoSAP.SelectedIndex > -1 Then
            Dim strValor1 As String = sender.SelectedValue.ToString
            If strValor1 <> "System.Data.DataRowView" Then
                ErrPrv.SetError(cboTipoSAP, "")
                CambiarTamanio_Detalle()

            End If
        End If
    End Sub
    Private Sub ActivarOcultarFileProveedor()
        Try
            'If cboFormaEgreso.SelectedValue <> "PTT" Then
            lblEtiqFile.Visible = False
            lblIDFile.Visible = False
            btnConsultarFile.Visible = False
            btnLimpiarFile.Visible = False

            If cboFormaEgreso.SelectedValue <> "PTT" Then
                lblProveedor.Visible = True
                lblEtiqProveedor.Visible = True
                btnSelProveedor.Visible = True
            Else
                'lblProveedor.Visible = False
                'lblEtiqProveedor.Visible = False
                'btnSelProveedor.Visible = False
            End If
            'Else
            'lblEtiqFile.Visible = True
            'lblIDFile.Visible = True
            'btnConsultarFile.Visible = True
            'btnLimpiarFile.Visible = True

            'lblProveedor.Visible = False
            'lblEtiqProveedor.Visible = False
            'btnSelProveedor.Visible = False

            'End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CalcularTotales_Detalle()
        Try
            Dim dblTotalNeto As Decimal = 0
            Dim dblTotalIGV As Decimal = 0
            Dim dblTotalGeneral As Decimal = 0

            For Each drow As DataGridViewRow In dgvDetalle.Rows
                dblTotalNeto = dblTotalNeto + CDbl(drow.Cells("SsMonto").Value)
                dblTotalIGV = dblTotalIGV + CDbl(drow.Cells("SSIgv").Value)
                dblTotalGeneral = dblTotalGeneral + CDbl(drow.Cells("SSTotal").Value)
            Next

            txtSsNeto.Text = dblTotalNeto.ToString
            txtIGV.Text = dblTotalIGV.ToString
            txtMonto.Text = dblTotalGeneral.ToString

            pCalcularTotal()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnConsultarFile_Click(sender As Object, e As EventArgs) Handles btnConsultarFile.Click
        Try
            Dim frm As New frmIngCotizacion
            frm.chrModulo = "DP"
            blnLoad = False
            frm.frmRefDocumentosProveedorDato = Me
            frm.ShowDialog()
            blnLoad = True
            ' frmMain.btnImprimir.Enabled = blnImprimirEnabled
            'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
            'frmMain.btnGrabar.Enabled = blnGrabarEnabled
            'frmMain.btnNuevo.Enabled = blnNuevoEnabled
            'frmMain.btnEditar.Enabled = blnEditarEnabled
            'pCambiarTitulosToolBar(Me)

            If cboFormaEgreso.SelectedValue = "PTT" Then
                intIDCab = intIDCab_FondoFijo
                CargarVouchersxProveedor()
            Else
                If intIDCab_FondoFijo <> 0 Then 'ErrPrv.SetError(btnConsultarFile, "")
                    intIDCab = intIDCab_FondoFijo
                    Configurar_Origen_Otros()
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Configurar_Cuentas_General()
        If Not (strTipoVoucheruOrServ = "OTR" And lblIDFile.Text.Trim.Length = 0) Then

            'Else
            CargarCombo_CuentasContables_CeCos()
        End If

        If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP" Then

            CargarCuentas_Origen_Otros()
        End If
    End Sub

    Private Sub Configurar_Origen_Otros()
        Try
            'If strTipoVoucheruOrServ = "OTR" Then
            If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP" Then
                If lblIDFile.Text.Trim.Length > 0 Then
                    cboCentroCostos.Enabled = True
                    cboCentroControl.Enabled = True
                    If cboCentroCostos.Items.Count > 0 Then
                        cboCentroCostos.SelectedValue = "900101"
                        CargarCombo_CuentasContables_CeCos()
                    End If
                    If cboCentroControl.Items.Count > 0 Then
                        cboCentroControl.SelectedValue = "000000"

                    End If
                    CargarCuentas_Origen_Otros()
                Else
                    'cboCentroCostos.Enabled = False
                    'cboCentroControl.Enabled = False
                    cboCentroCostos.Enabled = True
                    cboCentroControl.Enabled = True
                    cboCentroCostos.SelectedIndex = -1
                    cboCentroControl.SelectedIndex = -1
                    If cboCentroControl.Items.Count > 0 Then
                        cboCentroControl.SelectedValue = "000000"
                    End If
                    'CargarCuentas_Sin_CC()
                    CargarCuentas_Origen_Otros()
                End If
            End If
            Cuentas_FechaOut()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarCuentas_Origen_Otros()
        Try
            'If strTipoVoucheruOrServ = "OTR" Then
            If strTipoVoucheruOrServ = "OTR" Or strTipoVoucheruOrServ = "OCP" Then
                '@FlFile bit, --=0,
                '@FlCoCecos bit, --=1,
                '@FlCoCecon bit, --=1,
                '@IdFile char(8) ='',
                '@CoCecos char(6) =''

                Dim blnFlFile As Boolean = 0
                Dim blnFlCoCecos As Boolean = 0
                Dim blnFlCoCecon As Boolean = 0
                Dim strIDFile_Tmp As String = ""
                Dim strCoCecos_Tmp As String = ""

                If lblIDFile.Text.Trim.Length > 0 Then
                    blnFlFile = 1
                End If

                Dim strTipoSAP As String = ""
                If cboTipoSAP.Items.Count > 0 Then
                    If Not cboTipoSAP.SelectedValue Is Nothing Then
                        strTipoSAP = cboTipoSAP.SelectedValue.ToString.Trim
                        If strTipoSAP = "2" Then
                            blnFlFile = 1
                        End If
                    End If
                End If


                If cboCentroCostos.Items.Count > 0 Then
                    If cboCentroCostos.SelectedIndex > -1 Then
                        blnFlCoCecos = True
                        blnFlCoCecon = True
                        strCoCecos_Tmp = cboCentroCostos.SelectedValue.ToString
                        '50102	900102	900103	900201	900202

                        'If Not (strCoCecos_Tmp = "900101" Or strCoCecos_Tmp = "940101" Or strCoCecos_Tmp = "050102" Or strCoCecos_Tmp = "900102" _
                        '        Or strCoCecos_Tmp = "900103" Or strCoCecos_Tmp = "900201" Or strCoCecos_Tmp = "900202") Then
                        '    'If Not (strCoCecos_Tmp = "940101") Then
                        '    strCoCecos_Tmp = ""
                        'End If

                        ''If strCoCecos_Tmp <> "900101" Then
                        ''    strCoCecos_Tmp = ""
                        ''End If
                        ''If strCoCecos_Tmp <> "940101" Then
                        ''    strCoCecos_Tmp = ""
                        ''End If
                    End If
                End If

                'If cboCentroControl.Items.Count > 0 And cboCentroCostos.Items.Count > 0 Then
                '    If cboCentroControl.SelectedIndex > -1 Then

                '        blnFlCoCecon = True
                '    End If
                'End If


                Dim objBN As New clsDocumentoProveedorBN
                pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable_OrigenOtros(blnFlFile, blnFlCoCecos, blnFlCoCecon, strIDFile_Tmp, strCoCecos_Tmp), False)
                If cboCuentaContable.Items.Count = 1 Then
                    cboCuentaContable.SelectedIndex = 0
                End If
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarCuentas_Sin_CC()
        Try
            Dim objBN As New clsDocumentoProveedorBN
            pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable_Sin_CentroCostos(), False)
            If cboCuentaContable.Items.Count = 1 Then
                cboCuentaContable.SelectedIndex = 0
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtIGV_Leave(sender As Object, e As EventArgs) 'Handles txtIGV.Leave
        Try
            If sender Is Nothing Then Exit Sub
            ErrPrv.SetError(DirectCast(sender, Control), "")
            ' If strModoEdicion <> "E" Then
            If sender.Name = "txtSsNeto" Or sender.Name = "txtSsOtrCargos" Or sender.Name = "txtIGV" Or sender.Name = "txtPercepcion" Then

                If cboTipoDocumento.Items.Count > 0 Then

                    If Not cboTipoDocumento.SelectedValue Is Nothing Then
                        Dim strCoTipoDoc As String = cboTipoDocumento.SelectedValue.ToString
                        If strCoTipoDoc <> "System.Data.DataRowView" Then
                            If sender.name = "txtIGV" And strCoTipoDoc.Trim = "HTD" Then
                                pCalcularTotal(True)
                                ValidarCheckRetencion()
                                Exit Sub
                            End If
                        End If
                    End If

                End If

                pCalcularTotal()
                ValidarCheckRetencion()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub btnLimpiarFile_Click(sender As Object, e As EventArgs) Handles btnLimpiarFile.Click
        Try
            intIDCab_FondoFijo = intIDCab_Inicial
            intIDCab = intIDCab_Inicial
            lblIDFile.Text = String.Empty
            Configurar_Origen_Otros()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub cboFormaEgreso_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboFormaEgreso.SelectedValueChanged
        Try
            Dim strTipoSAP As String = ""
            If cboTipoSAP.Items.Count > 0 Then
                If Not cboTipoSAP.SelectedValue Is Nothing Then
                    strTipoSAP = cboTipoSAP.SelectedValue.ToString.Trim
                    If strTipoSAP = "2" Then
                        CargarVouchersxProveedor()
                        ActivarOcultarFileProveedor()
                    End If
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub ValidarNros_DgvKeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim intColumna As Int32 = dgvVouchers.CurrentCell.ColumnIndex
        If intColumna = dgvVouchers.Columns("Gastos").Index Then
            Dim Caracter As Char = e.KeyChar
            Dim Text As TextBox = CType(sender, TextBox)

            If Not IsNumeric(Caracter) And (Caracter = ChrW(Keys.Back)) = False And Caracter <> "." Then
                e.KeyChar = Chr(0)
            End If

        End If
    End Sub

    Private Sub ValidarNros_DgvKeyPress2(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)
        Dim intColumna As Int32 = dgvVouchers.CurrentCell.ColumnIndex
        If intColumna = dgvVouchers.Columns("Gastos").Index Then

            If Not IsNumeric(e.KeyChar) And Asc(e.KeyChar) <> 8 Then
                Dim Text As TextBox = CType(sender, TextBox)
                If Asc(e.KeyChar) = 46 Then
                    If Text.Text.Trim = "" Or InStr(Text.Text.Trim, ".") > 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If
            End If

            'Dim Caracter As Char = e.KeyChar
            'Dim Text As TextBox = CType(sender, TextBox)

            'If Not IsNumeric(Caracter) And (Caracter = ChrW(Keys.Back)) = False And Caracter <> "." Then
            '    e.KeyChar = Chr(0)
            'End If


        End If

        If intColumna = dgvVouchers.Columns("MontoFinal").Index Then

            If Not IsNumeric(e.KeyChar) And Asc(e.KeyChar) <> 8 Then
                Dim Text As TextBox = CType(sender, TextBox)
                If Asc(e.KeyChar) = 46 Then
                    If Text.Text.Trim = "" Or InStr(Text.Text.Trim, ".") > 0 Then
                        e.Handled = True
                    End If
                Else
                    e.Handled = True
                End If
            End If

        End If
    End Sub

    Private Sub DgvDetalle_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvVouchers.EditingControlShowing
        If InStr(e.Control.ToString, "System.Windows.Forms.DataGridViewTextBoxEditingControl") = 0 Then Exit Sub

        Dim txtEntero As TextBox = CType(e.Control, TextBox)

        AddHandler txtEntero.KeyPress, AddressOf ValidarNros_DgvKeyPress2
    End Sub

    Private Sub cboMoneda_Multiple_SelectedValueChanged(sender As Object, e As EventArgs) Handles cboMoneda_Multiple.SelectedValueChanged
        Try
            If blnEsMultiple() Then
                If cboMoneda_Multiple.SelectedValue.ToString.Trim <> "" Then
                    strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
                Else
                    grbTipoCambioFormaEgreso.Visible = True
                End If
                'Else
                '    strMonedaFEgreso = cboMoneda.SelectedValue
                'End If
                CargarVouchersxProveedor()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cboMoneda_Multiple_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboMoneda_Multiple.SelectionChangeCommitted
        Try
            If cboMoneda_Multiple.SelectedValue.ToString.Trim <> "" Then
                strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
            End If
            'If cboMoneda_Multiple.SelectedValue.ToString.Trim <> "" Then
            '    strMonedaFEgreso = cboMoneda_Multiple.SelectedValue
            'Else
            '    strMonedaFEgreso = cboMoneda.SelectedValue
            'End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtTipoCambioFEgreso_KeyDown(sender As Object, e As KeyEventArgs) Handles txtTipoCambioFEgreso.KeyDown
        'Try
        '    If blnEsMultiple() Then
        '        RemoveHandler txtTipoCambioFEgreso.TextChanged, AddressOf txtTipoCambioFEgreso_TextChanged
        '        Dim dblTipoCambio As Double = CDbl(txtTipoCambioFEgreso.Text)
        '        pCalcularTipoCambio_Multiple(dblTipoCambio)
        '        pCalcularTipoCambio_Multiple_Detalle(dblTipoCambio)
        '        AddHandler txtTipoCambioFEgreso.TextChanged, AddressOf txtTipoCambioFEgreso_TextChanged
        '    End If

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub txtTipoCambioFEgreso_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtTipoCambioFEgreso.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            'If InStr("0123456789.,", e.KeyChar) = 0 Then
            If InStr("0123456789.", e.KeyChar) = 0 Then
                e.KeyChar = ""
            End If
            Dim oControl As Control = sender
            If oControl.Name = "txtTipoCambioFEgreso" Then
                If e.KeyChar = "." And Not txtTipoCambioFEgreso.Text.IndexOf(".") Then
                    e.Handled = True
                End If
           
            End If
        End If
    End Sub

    Private Sub txtTipoCambioFEgreso_KeyUp(sender As Object, e As KeyEventArgs) Handles txtTipoCambioFEgreso.KeyUp
        Try
            blnTipoCambio_TXT = True
            If blnEsMultiple() Then
                RemoveHandler txtTipoCambioFEgreso.TextChanged, AddressOf txtTipoCambioFEgreso_TextChanged
                Dim dblTipoCambio As Double = CDbl(txtTipoCambioFEgreso.Text)
                If Not FlTodasMonedas() Then

                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg(dblTipoCambio)
                        pCalcularTipoCambio_Multiple_Detalle_Arg(dblTipoCambio)
                    Else
                        pCalcularTipoCambio_Multiple(dblTipoCambio)
                        pCalcularTipoCambio_Multiple_Detalle(dblTipoCambio)
                    End If

                Else
                    If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
                        pCalcularTipoCambio_Multiple_Arg(dblTipoCambio)
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas_Arg(dblTipoCambio)
                    Else
                        pCalcularTipoCambio_Multiple(dblTipoCambio)
                        pCalcularTipoCambio_Multiple_Detalle_TodasMonedas(dblTipoCambio)
                    End If

                End If

                AddHandler txtTipoCambioFEgreso.TextChanged, AddressOf txtTipoCambioFEgreso_TextChanged
            End If
            blnTipoCambio_TXT = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub dgvVouchers_CellValidating(sender As Object, e As DataGridViewCellValidatingEventArgs) Handles dgvVouchers.CellValidating

        If strTipoVoucheruOrServ <> "PTT" Then Exit Sub

        If e.ColumnIndex = dgvVouchers.Columns("MontoFinal").Index Then
            Dim dblSaldoOrig As Double = 0
            If IsNumeric(dgvVouchers.CurrentRow.Cells("SaldoOrig").Value.ToString) Then
                dblSaldoOrig = dgvVouchers.CurrentRow.Cells("SaldoOrig").Value.ToString
            End If
            Dim dblTotal As Double = 0
            If IsNumeric(dgvVouchers.Rows(e.RowIndex).Cells("MontoFinal").Value.ToString) Then
                dblTotal = dgvVouchers.Rows(e.RowIndex).Cells("MontoFinal").Value.ToString
            End If
            If dblTotal > dblSaldoOrig Then
                e.Cancel = True
                ErrPrv.SetError(dgvVouchers, "Debe ingresar un valor menor o igual al Saldo Original.")
            End If

        End If


    End Sub

    Private Sub cboFormaEgreso_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboFormaEgreso.SelectedIndexChanged

    End Sub

    Private Sub MostrarOcultarControles_Conta(ByVal vbln As Boolean)
        Try
            lblCentroCostos.Visible = vbln
            cboCentroCostos.Visible = vbln
            lblCentroControl.Visible = vbln
            cboCentroControl.Visible = vbln
            lblDestino.Visible = vbln
            cboTipoOperacion.Visible = vbln
            lblCuenta.Visible = vbln
            cboCuentaContable.Visible = vbln
            lblTipoDetraccion.Visible = vbln
            cboTipoDetraccion.Visible = vbln
            lblDetracciones.Visible = vbln
            txtSsDetraccion.Visible = vbln
            lblOtrosCargos.Visible = vbln
            cboGastos.Visible = vbln
            txtSsOtrCargos.Visible = vbln
            lblPercepcion.Visible = vbln
            txtPercepcion.Visible = vbln

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ConfigurarxUbigeo()
        If (strCoUbigeo_Oficina = gstrBuenosAires Or strCoUbigeo_Oficina = gstrSantiago) Then
            MostrarOcultarControles_Conta(False)
            lblGlosa.Location = New Drawing.Point(534, 194)
            txtConcepto.Location = New Drawing.Point(581, 191)

            lblImporte.Location = New Drawing.Point(5, 157)
            txtMonto.Location = New Drawing.Point(136, 154)
            If strCoUbigeo_Oficina = gstrBuenosAires Then
                chkIGV.Text = "IVA (21%): "
            ElseIf strCoUbigeo_Oficina = gstrSantiago Then
                chkIGV.Text = "IVA (19%): "
            Else
                chkIGV.Text = "IVA: "
            End If

            dgvVouchers.Columns("Gastos").Visible = False
        Else
            MostrarOcultarControles_Conta(True)
        End If
    End Sub

    Private Sub btnGrabar_Click(sender As Object, e As EventArgs) Handles btnGrabar.Click
        pGrabar(Nothing, Nothing)
    End Sub
End Class