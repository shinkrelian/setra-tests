﻿Imports ComSEToursBL2
Imports ComSEToursBE2
Public Class frmIngCotizacionDato_ArchivosMem
    Public frmRef As frmIngCotizacionDato
    Public frmRefP As frmIngPedidoDato
    'Public frmRefR As frmIngReservasControlporFileDato
    'Public frmRefO As frmIngReservasOrdendePago
    'Public frmRefRD As frmDocumentosProveedorEdicion
    'Public frmRefCo As frmConsIngresoAsignacion

    Public strModulo As String = ""

    ''Public frmRef_FF As frmDocumentosProveedorDato_FondoFijo
    'Public frmRefC As frmIngNotaVentaCounter
    Public bytNuSegmento As Byte = 0
    Public strDescripcion As String = ""
    Dim strModoEdicion As String = ""
    Public strIDProv_LA As String = ""
    Dim blnCambios As Boolean = False
    Dim blnLoad As Boolean = False
    Public strCoProducto As String = ""
    Dim strCoAlmacen As String = ""

    Dim strRuta As String = ""
    Dim strRutaDestino As String = ""
    Dim strArchivo As String = ""
    Dim FlCopiado As Boolean = False
    Dim strUsuarioCreacion As String = ""
    Public intIDCAB As Integer = 0
    Public strIDFile As String = ""
    Public strTitulo As String = ""
    Public datFechaFile As Date = Now
    Public strTipoFormArchivos As String = ""
    Private Sub frmIngTicketDatoMem_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If btnAceptar.Enabled Then btnAceptar_Click(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmIngTicketDato_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios Then
            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin grabar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False
            Else
                e.Cancel = True
            End If
        End If
    End Sub

    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()
    End Sub

    Private Sub frmIngTicketDatoMem_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'btnTicket.Visible = False

            ColorearControlesObligatorios()
            CargarCombos()
            'txtIGV.Text = "0.00"
            If bytNuSegmento = 0 Then
                ModoNuevo()
            Else
                ModoEditar()
                CargarControles()
            End If
            blnLoad = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub CargarCombos()
        Try
            'Dim objAlm As New clsMAAlmacenBN
            'pCargaCombosBox(cboTipo, objAlm.ConsultarCbo(), True)

            Dim dttTipos As New DataTable("Tipos")
            With dttTipos
                .Columns.Add("Codigo")
                .Columns.Add("Descripcion")
                .Rows.Add("VEN", "VENTAS")
                .Rows.Add("RES", "RESERVAS")
                .Rows.Add("OPE", "OPERACIONES")
                .Rows.Add("FIN", "FINANZAS")
                
            End With
            pCargaCombosBox(cboTipo, dttTipos)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarControles()
        Try

            If Not frmRef Is Nothing Then

                With frmRef
                    Dim vintIndex As Integer = frmRef.dgvArchivos.CurrentRow.Index
                    With frmRef.dgvArchivos


                        strArchivo = .Item("Nombre", vintIndex).Value.ToString.Trim
                        strRuta = .Item("RutaOrigen", vintIndex).Value.ToString.Trim
                        lblArchivoExcel.Text = strRuta
                        strRutaDestino = .Item("RutaDestino", vintIndex).Value.ToString.Trim
                        cboTipo.SelectedValue = .Item("CoTipo", vintIndex).Value.ToString.Trim
                        FlCopiado = .Item("FlCopiado", vintIndex).Value
                        strUsuarioCreacion = .Item("CoUsuarioCreacion", vintIndex).Value.ToString.Trim


                    End With


                End With
                Exit Sub
            End If

         

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ModoEditar()
        strModoEdicion = "E"
        ' If frmRefC Is Nothing Then
        lblTitulo.Text = "Edición del Item: " & strDescripcion
        Me.Text = "Edición del Item: " & strDescripcion

    End Sub

    Private Sub ModoNuevo()
        strModoEdicion = "N"
        cboTipo.SelectedValue = strTipoFormArchivos
    End Sub

    Private Sub Grabar()
        Try
            If Not frmRef Is Nothing Then
                With frmRef

                    .dgvArchivos.Rows.Add()
                    Dim vintIndex As Integer = .dgvArchivos.Rows.Count - 1
                    With .dgvArchivos

                     

                        .Item("IDArchivo", vintIndex).Value = frmRef.bytGenerarNuArchivo
                        .Item("IDCAB_Archivo", vintIndex).Value = intIDCAB
                        .Item("Nombre", vintIndex).Value = strArchivo
                        .Item("RutaOrigen", vintIndex).Value = strRuta 'cboAlmacen.SelectedValue
                        .Item("RutaDestino", vintIndex).Value = GenerarRutaDestino()
                        .Item("CoTipo", vintIndex).Value = cboTipo.SelectedValue
                        .Item("Tipo_Archivo", vintIndex).Value = cboTipo.Text
                        .Item("FlCopiado", vintIndex).Value = False
                        .Item("FechaModificacion", vintIndex).Value = Now
                        .Item("CoUsuarioCreacion", vintIndex).Value = gstrUser
                        .Item("Usuario", vintIndex).Value = gstrUserNombre

                    End With
                End With

                Exit Sub
            End If

       
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Actualizar()
        Try
            If Not frmRef Is Nothing Then
                With frmRef
                    Dim vintIndex As Integer = .dgvArchivos.CurrentRow.Index
                    With .dgvArchivos

                        .Item("IDCAB_Archivo", vintIndex).Value = intIDCAB
                        .Item("Nombre", vintIndex).Value = strArchivo
                        .Item("RutaOrigen", vintIndex).Value = strRuta 'cboAlmacen.SelectedValue
                        .Item("RutaDestino", vintIndex).Value = GenerarRutaDestino()
                        .Item("CoTipo", vintIndex).Value = cboTipo.SelectedValue
                        .Item("Tipo_Archivo", vintIndex).Value = cboTipo.Text
                        .Item("FlCopiado", vintIndex).Value = False
                        .Item("FechaModificacion", vintIndex).Value = Now
                        .Item("CoUsuarioCreacion", vintIndex).Value = gstrUser
                        .Item("Usuario", vintIndex).Value = gstrUserNombre

                    End With
                End With
                Exit Sub
            End If

          
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnAceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnAceptar.Click
        If Not blnValidarIngresos() Then Exit Sub
        Try
            If strModoEdicion = "N" Then
                Grabar()
            ElseIf strModoEdicion = "E" Then
                Actualizar()
            End If
            If Not frmRef Is Nothing Then
                frmRef.blnActualizoArchivo = True
            End If

            blnCambios = False
            Me.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function blnValidarIngresos() As Boolean
        Try
            Dim blnOk As Boolean = True


            If lblArchivoExcel.Text.Trim = "" Then
                btnSelArchivoExcel.Focus()
                ErrPrv.SetError(btnSelArchivoExcel, "Debe adjuntar un archivo")
                blnOk = False
            End If

            If cboTipo.SelectedIndex < 0 Then
                cboTipo.Focus()
                ErrPrv.SetError(cboTipo, "Debe seleccionar un tipo")
                blnOk = False
            End If


            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Sub ColorearControlesObligatorios()
        Try
            'cboCoCiudadLle.BackColor = gColorTextBoxObligat
            lblArchivoExcel.BackColor = gColorTextBoxObligat
            'txtNuDocumento.BackColor = gColorTextBoxObligat
            'lblCodProveedor.BackColor = gColorTextBoxObligat
            cboTipo.BackColor = gColorTextBoxObligat
         

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        pSalir(Nothing, Nothing)
    End Sub

 

    Private Sub btnTicket_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub Importes_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs)



        Dim strCaracteres As String
        strCaracteres = "0123456789."

        'If sender.name = "txtIDServicio_Det" Then
        '    strCaracteres = "0123456789"
        'End If
        If sender.name = "txtQtCantidad" Then
            strCaracteres = "0123456789"
        End If
        If Asc(e.KeyChar) <> 8 Then
            If InStr(strCaracteres, e.KeyChar) = 0 Then
                e.KeyChar = ""
            End If
        End If


    End Sub

    Private Sub Validaciones_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If Not blnLoad Then Exit Sub
        Try
            If sender IsNot Nothing Then ErrPrv.SetError(CType(sender, Control), "")
            blnCambios = True



        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    

    Private Sub btnSelArchivoExcel_Click(sender As Object, e As EventArgs) Handles btnSelArchivoExcel.Click
        Try
            ErrPrv.SetError(lblArchivoExcel, "")
            Dim strFile As String = ""
            Dim out_FileSelect As New OpenFileDialog
            'out_FileSelect.Filter = "Hoja de Excel Office 201x (*.xlsx)|*.xlsx|Hoja de Excel (*.xls)|*.xls"
            'out_FileSelect.FilterIndex = 1
            out_FileSelect.RestoreDirectory = True

            If out_FileSelect.ShowDialog() = DialogResult.OK Then
                'txtMensajeError.Visible = False
                'txtMensajeOK.Visible = False
                'lblTicketOkEtiq.Visible = False
                'lblTicketErrorEtiq.Visible = False
                'Me.Height = 150

                strRuta = out_FileSelect.FileName
                strFile = out_FileSelect.SafeFileName
                strArchivo = strFile
                If strFile.Trim <> String.Empty Then
                    lblArchivoExcel.Text = strRuta

                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function GenerarRutaDestino() As String
        Try
            Dim strRutaDestinoTmp As String = ""

            Dim strRuta As String = gstrRutaArchivos_File
            ' Dim datFechaFile As Date = datFechaFile 'CDate(lblFecha.Text)
            Dim strAnioMes As String = Month(datFechaFile).ToString.PadLeft(2, "0") + Year(datFechaFile).ToString
            Dim strRutaFile1 As String = strIDFile + " " + strReemplazarNombreCarpeta(strTitulo)
            Dim strTipoTmp As String = ""
            Dim strRutaFinal1 As String = strRuta + Year(datFechaFile).ToString + "\" + strAnioMes + "\" + strRutaFile1

            Dim strRutaFinal2 As String = ""

            Dim blnExisteRuta As Boolean
            blnExisteRuta = System.IO.Directory.Exists(strRutaFinal1)

            If System.IO.Directory.Exists(strRutaFinal1) = False Then
                'si no existe la ruta crearla
                System.IO.Directory.CreateDirectory(strRutaFinal1)
            End If

            If System.IO.Directory.Exists(strRutaFinal1 + "\" + cboTipo.Text) = False Then
                'si no existe la ruta crearla
                System.IO.Directory.CreateDirectory(strRutaFinal1 + "\" + cboTipo.Text)
            End If

            strRutaDestinoTmp = strRutaFinal1 + "\" + cboTipo.Text + "\" + strArchivo

            Return strRutaDestinoTmp
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

End Class