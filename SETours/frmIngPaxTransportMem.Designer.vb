﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmIngPaxTransportMem
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.chkMarcarTodos = New System.Windows.Forms.CheckBox()
        Me.btnNuevo = New System.Windows.Forms.Button()
        Me.dgvPax = New System.Windows.Forms.DataGridView()
        Me.btnSalir = New System.Windows.Forms.Button()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.ErrPrv = New System.Windows.Forms.ErrorProvider(Me.components)
        Me.Sel = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.IDPaxTransp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDPax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombrePax = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CodReservaTransp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.IDProveedorGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NombreGuia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.NoShow = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.Reembolso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.dgvPax, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.BackColor = System.Drawing.Color.White
        Me.GroupBox1.Controls.Add(Me.chkMarcarTodos)
        Me.GroupBox1.Controls.Add(Me.btnNuevo)
        Me.GroupBox1.Controls.Add(Me.dgvPax)
        Me.GroupBox1.Location = New System.Drawing.Point(35, -1)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(596, 344)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'chkMarcarTodos
        '
        Me.chkMarcarTodos.AutoSize = True
        Me.chkMarcarTodos.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.chkMarcarTodos.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.chkMarcarTodos.Location = New System.Drawing.Point(6, 16)
        Me.chkMarcarTodos.Name = "chkMarcarTodos"
        Me.chkMarcarTodos.Size = New System.Drawing.Size(101, 17)
        Me.chkMarcarTodos.TabIndex = 286
        Me.chkMarcarTodos.Text = "Marcar todos"
        Me.chkMarcarTodos.UseVisualStyleBackColor = True
        '
        'btnNuevo
        '
        Me.btnNuevo.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnNuevo.Image = Global.SETours.My.Resources.Resources.page_add
        Me.btnNuevo.Location = New System.Drawing.Point(563, 37)
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(27, 27)
        Me.btnNuevo.TabIndex = 285
        Me.btnNuevo.UseVisualStyleBackColor = True
        '
        'dgvPax
        '
        Me.dgvPax.AllowUserToAddRows = False
        Me.dgvPax.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.White
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPax.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvPax.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPax.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Sel, Me.IDPaxTransp, Me.IDPax, Me.NombrePax, Me.CodReservaTransp, Me.IDProveedorGuia, Me.NombreGuia, Me.NoShow, Me.Reembolso})
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvPax.DefaultCellStyle = DataGridViewCellStyle4
        Me.dgvPax.Location = New System.Drawing.Point(6, 37)
        Me.dgvPax.Name = "dgvPax"
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.ControlDarkDark
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvPax.RowHeadersDefaultCellStyle = DataGridViewCellStyle5
        Me.dgvPax.Size = New System.Drawing.Size(552, 300)
        Me.dgvPax.TabIndex = 0
        '
        'btnSalir
        '
        Me.btnSalir.Image = Global.SETours.My.Resources.Resources.door_out
        Me.btnSalir.Location = New System.Drawing.Point(2, 36)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(27, 27)
        Me.btnSalir.TabIndex = 4
        Me.ToolTip1.SetToolTip(Me.btnSalir, "Salir (ESC)")
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'btnAceptar
        '
        Me.btnAceptar.Image = Global.SETours.My.Resources.Resources.accept
        Me.btnAceptar.Location = New System.Drawing.Point(2, 5)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(27, 27)
        Me.btnAceptar.TabIndex = 3
        Me.btnAceptar.Tag = "Borrar"
        Me.ToolTip1.SetToolTip(Me.btnAceptar, "Aceptar (F6)")
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'ErrPrv
        '
        Me.ErrPrv.ContainerControl = Me
        '
        'Sel
        '
        Me.Sel.HeaderText = ""
        Me.Sel.Name = "Sel"
        Me.Sel.Width = 20
        '
        'IDPaxTransp
        '
        Me.IDPaxTransp.HeaderText = "IDPaxTransp"
        Me.IDPaxTransp.Name = "IDPaxTransp"
        Me.IDPaxTransp.ReadOnly = True
        Me.IDPaxTransp.Visible = False
        '
        'IDPax
        '
        Me.IDPax.HeaderText = "IDPax"
        Me.IDPax.Name = "IDPax"
        Me.IDPax.ReadOnly = True
        Me.IDPax.Visible = False
        '
        'NombrePax
        '
        Me.NombrePax.HeaderText = "Nombre"
        Me.NombrePax.Name = "NombrePax"
        Me.NombrePax.ReadOnly = True
        '
        'CodReservaTransp
        '
        Me.CodReservaTransp.HeaderText = "Cod. Reserva"
        Me.CodReservaTransp.Name = "CodReservaTransp"
        '
        'IDProveedorGuia
        '
        Me.IDProveedorGuia.HeaderText = "IDProveedorGuia"
        Me.IDProveedorGuia.Name = "IDProveedorGuia"
        Me.IDProveedorGuia.ReadOnly = True
        Me.IDProveedorGuia.Visible = False
        '
        'NombreGuia
        '
        Me.NombreGuia.HeaderText = "Guía"
        Me.NombreGuia.Name = "NombreGuia"
        Me.NombreGuia.ReadOnly = True
        '
        'NoShow
        '
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.BackColor = System.Drawing.Color.Silver
        DataGridViewCellStyle2.NullValue = False
        Me.NoShow.DefaultCellStyle = DataGridViewCellStyle2
        Me.NoShow.FalseValue = "0"
        Me.NoShow.HeaderText = "No Show"
        Me.NoShow.Name = "NoShow"
        Me.NoShow.ReadOnly = True
        Me.NoShow.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.NoShow.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic
        Me.NoShow.TrueValue = "1"
        Me.NoShow.Width = 50
        '
        'Reembolso
        '
        DataGridViewCellStyle3.Format = "N2"
        DataGridViewCellStyle3.NullValue = "0.00"
        Me.Reembolso.DefaultCellStyle = DataGridViewCellStyle3
        Me.Reembolso.HeaderText = "Reembolso"
        Me.Reembolso.Name = "Reembolso"
        '
        'frmIngPaxTransportMem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(630, 344)
        Me.Controls.Add(Me.btnSalir)
        Me.Controls.Add(Me.btnAceptar)
        Me.Controls.Add(Me.GroupBox1)
        Me.KeyPreview = True
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "frmIngPaxTransportMem"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ingreso de Pax por Transporte"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.dgvPax, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ErrPrv, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents dgvPax As System.Windows.Forms.DataGridView
    Friend WithEvents btnSalir As System.Windows.Forms.Button
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnNuevo As System.Windows.Forms.Button
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents ErrPrv As System.Windows.Forms.ErrorProvider
    Friend WithEvents chkMarcarTodos As System.Windows.Forms.CheckBox
    Friend WithEvents Sel As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents IDPaxTransp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDPax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombrePax As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CodReservaTransp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents IDProveedorGuia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NombreGuia As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents NoShow As System.Windows.Forms.DataGridViewCheckBoxColumn
    Friend WithEvents Reembolso As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
