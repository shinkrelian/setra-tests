﻿Imports ComSEToursBL2
Imports System.Drawing
Imports System.Drawing.Drawing2D
Imports System.Windows.Forms.VisualStyles

Public Class frmMantServiciosProveedor
    Dim blnNuevoEnabled As Boolean = True
    Dim blnGrabarEnabled As Boolean = False
    Dim blnDeshacerEnabled As Boolean = False
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean = True
    Dim blnImprimirEnabled As Boolean = False

    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False
    Dim blnEliminarEnabledDetalleBD As Boolean = True

    Public strProveedor As String = ""
    Public strIDProveedor As String = ""
    Public strCoPago As String = ""
    Public dblMargen As Double = 0
    Public strTipoProveedor As String = ""
    Public strAnio As String = ""
    Public strAnioDiaSgte As String = ""
    Public strIDPais As String = ""
    Public strIDCiudad As String = ""

    Public strIDTipoOper As Char = ""

    Public blnDgvTipoProvSelHotel As Boolean
    Public strIDProveedorAntReservas As String
    Public intIDReservaAnt As Integer
    Public intNroPax As Int16
    Public intNroLiberados As Int16
    Public strTipoLib As String
    Public strTipoPaxLoc As String
    Public blnReemplazandoServicio As Boolean = False
    Public datFechaInsert As Date = #1/1/1900#
    Public blnNuevoServicioCot As Boolean = False

    Public frmRefIngDetCotiDatoMem As frmIngDetCotizacionDatoMem = Nothing
    Public frmRefIngDetServicioDato As Object = Nothing 'frmMantDetServiciosProveedorDato = Nothing
    Public frmRefIngCotizacionDato As frmIngCotizacionDato = Nothing
    Public frmRefIngReservasCtrlxFileDato As Object = Nothing 'As frmIngReservasControlporFileDato = Nothing
    Public frmRefIngOperacionProgramacion As Object = Nothing 'As frmIngOperacionProgramacion = Nothing
    Public strTipoConsProgramacion As String = ""
    Public bytNroDetDgvProgram As Byte

    Public frmRefIngPresupuestoDatoMem As Object = Nothing 'As frmIngPresupuestoDatoMem = Nothing
    Public frmRefServOperadores As Object = Nothing 'As frmRepServOperadores = Nothing
    Public frmRefIncluidos As Object = Nothing 'As frmMantIncluidoDato
    Public frmRefMantStockEntradasDato As Object = Nothing 'As frmMantStockEntradasDato
    Public frmRefMantServiciosDias As Object = Nothing 'As frmMantServiciosProveedorDato

    Public strTipoGasto As String = String.Empty

    Public blnBuscar As Boolean = True
    Public blnCotizacion As Boolean = False
    Dim blnLoad As Boolean = False
    Public blnCopio As Boolean = False
    Public strIDServicioCopiado As String = String.Empty
    Public strIDServicio_DetCopiado As String = String.Empty

    Public blnServicioVarios As Boolean = False
    Public intIDCab As Integer = 0
    Public strTituloFile As String = String.Empty
    Public strDescIncluido As String = String.Empty

    Public blnReemplazaServiciosHotel As Boolean = False
    Public strIDServicioHotel As String = ""
    Public intIDServicio_DetHotel As Integer = 0

    Dim objBN As New clsServicioProveedorBN
    'Public strIDServicioGuide As String = ""
    'Public intIDServicio_DetGuide As Integer
    Public blnFiltrarTodosServicioyVarios As Boolean = False

    'Public blnProveedor_Oper_Manual As Boolean = False
    Public strModulo As String = ""
    Public blnSeleccionHotelTC As Boolean = False
    Public blnServicioxTarifas As Boolean = False

    Dim frmMain As Object = Nothing

    'Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click, _
    '    txtRazonSocial.TextChanged, txtDescServicio.TextChanged, chkServCoticen.CheckedChanged, cboTipo.SelectionChangeCommitted, nudCategoria.ValueChanged, _
    '    cboCategSeTours.SelectionChangeCommitted, cboCiudad.SelectionChangeCommitted, cboPais.SelectionChangeCommitted

    'Jorge: se quitaron los eventos: txtRazonSocial.TextChanged, txtDescServicio.TextChanged 
    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click, _
        chkServCoticen.CheckedChanged, cboTipo.SelectionChangeCommitted, nudCategoria.ValueChanged, _
       cboCategSeTours.SelectionChangeCommitted, cboCiudad.SelectionChangeCommitted, cboPais.SelectionChangeCommitted
        Try
            'dgvDet.Columns("IDServicio_Det").Visible = True

            If Not blnLoad And strIDCiudad = "" And Not blnReemplazaServiciosHotel Then Exit Sub
            If Not sender Is Nothing Then
                If sender.name = "cboTipo" Then
                    If cboTipo.SelectedValue.ToString = gstrTipoProveeHoteles Then
                        lblCategoriaEtiq.Visible = True
                        lblCategoriaSEToursEtiq.Visible = True
                        cboCategSeTours.Visible = True
                        nudCategoria.Visible = True

                        Dim objBLCat As New clsTablasApoyoBN.clsCategoriaBN
                        pCargaCombosBox(cboCategSeTours, objBLCat.ConsultarCbo(True), True)
                    Else

                        lblCategoriaEtiq.Visible = False
                        lblCategoriaSEToursEtiq.Visible = False
                        cboCategSeTours.Visible = False
                        nudCategoria.Visible = False
                        nudCategoria.Value = 0
                        cboCategSeTours.SelectedValue = ""

                    End If
                End If


                'If sender.name = "txtRazonSocial" Then
                '    If txtRazonSocial.Text.Length <= 5 Then
                '        Exit Sub
                '    End If
                'End If

                'If sender.name = "txtDescServicio" Then
                '    If txtDescServicio.Text.Length <= 5 Then
                '        Exit Sub
                '    End If
                'End If
            End If
            'If Not frmRefIngDetCotiDatoMem Is Nothing Or Not frmRefIngDetServicioDato Is Nothing Or _
            '   Not frmRefIngCotizacionDato Is Nothing Or Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
            '   Not frmRefIngOperacionProgramacion Is Nothing Then
            If blnReemplazaServiciosHotel Then
                Buscar(strIDServicioHotel)
            Else
                Buscar()
            End If

            'Buscar() 'strIDServicioGuide)

            'End If


            If blnLoad Then
                dgv_Click(Nothing, Nothing)
            End If
            'If dgv.Rows.Count = 0 Then
            dgv_CellEnter(Nothing, Nothing)
            'End If
            'dgvDet.Columns("IDServicio_Det").Visible = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, "Error en " & Err.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub CargarSeguridad()
        Try
            Dim strNombreForm As String
            strNombreForm = Replace(Me.Name, "frm", "mnu")
            strNombreForm = Replace(strNombreForm, "Dato", "")

            If blnServicioVarios Then
                strNombreForm = "mnuMantServiciosVariosCotizacion"
            End If

            Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
                    If dr.HasRows Then
                        dr.Read()
                        blnGrabarEnabledBD = dr("Grabar")
                        blnEditarEnabledBD = dr("Actualizar")
                        blnEliminarEnabledBD = dr("Eliminar")
                    End If
                    dr.Close()
                End Using
            End Using

            strNombreForm = "mnuMantDetServiciosProveedor"
            Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
                    If dr.HasRows Then
                        dr.Read()
                        blnEliminarEnabledDetalleBD = dr("Eliminar")
                    End If
                    dr.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActivarDesactivarBotones()
        If Not frmRefIngPresupuestoDatoMem Is Nothing Then Exit Sub
        If Not frmRefIngDetCotiDatoMem Is Nothing Then Exit Sub
        If Not frmRefIngDetServicioDato Is Nothing Then Exit Sub
        If Not frmRefIngCotizacionDato Is Nothing Then Exit Sub
        If Not frmRefIngReservasCtrlxFileDato Is Nothing Then Exit Sub
        If Not frmRefServOperadores Is Nothing Then Exit Sub
        'If strProveedor <> "" Then Exit Sub

        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        If blnNuevoEnabled = True Then
            If blnGrabarEnabledBD Then
                frmMain.btnNuevo.Enabled = blnNuevoEnabled
                btnNuevo.Enabled = blnNuevoEnabled
            Else
                frmMain.btnNuevo.Enabled = False
                btnNuevo.Enabled = False
                blnNuevoEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnNuevo.Enabled = blnNuevoEnabled
            btnNuevo.Enabled = blnNuevoEnabled
        End If

        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        If blnGrabarEnabled = True Then
            If blnGrabarEnabledBD Then
                frmMain.btnGrabar.Enabled = blnGrabarEnabled
                btnGrabar.Enabled = blnGrabarEnabled
            Else
                frmMain.btnGrabar.Enabled = False
                btnGrabar.Enabled = False
                blnGrabarEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnGrabar.Enabled = blnGrabarEnabled
            btnGrabar.Enabled = blnGrabarEnabled
        End If

        frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        btnDeshacer.Enabled = blnDeshacerEnabled

        'frmMain.btnEditar.Enabled = blnEditarEnabled
        If blnEditarEnabled = True Then
            If blnEditarEnabledBD Then
                frmMain.btnEditar.Enabled = blnEditarEnabled
                btnEditar.Enabled = blnEditarEnabled
            Else
                frmMain.btnEditar.Enabled = False
                btnEditar.Enabled = False
                blnEditarEnabled = blnEditarEnabledBD
            End If
        Else
            frmMain.btnEditar.Enabled = blnEditarEnabled
            btnEditar.Enabled = blnEditarEnabled
        End If

        'frmMain.btnEliminar.Enabled = blnEliminarEnabled

        If blnEliminarEnabled = True Then
            If blnEliminarEnabledBD Then
                frmMain.btnEliminar.Enabled = blnEliminarEnabled
                btnEliminar.Enabled = blnEliminarEnabled
            Else
                frmMain.btnEliminar.Enabled = False
                btnEliminar.Enabled = False
                blnEliminarEnabled = blnEliminarEnabledBD
            End If
        Else
            frmMain.btnEliminar.Enabled = blnEliminarEnabled
            btnEliminar.Enabled = blnEliminarEnabled
        End If

        Dim blnSoloConsulta As Boolean = False
        If Not frmRefIngDetCotiDatoMem Is Nothing Or Not frmRefIngDetServicioDato Is Nothing Or _
                Not frmRefIngCotizacionDato Is Nothing Or Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
                Not frmRefIngOperacionProgramacion Is Nothing Or Not frmRefIngPresupuestoDatoMem Is Nothing Or _
                Not frmRefServOperadores Is Nothing Or Not frmRefIncluidos Is Nothing Or _
                Not frmRefMantStockEntradasDato Is Nothing Or Not frmRefMantServiciosDias Is Nothing Then
            blnSoloConsulta = True
        End If

        If Not dgv.Columns("btnDel") Is Nothing Then
            dgv.Columns("btnDel").Visible = blnEliminarEnabled
            If blnSoloConsulta Then dgv.Columns("btnDel").Visible = False
        End If
        If Not dgvDet.Columns("btnDelDet") Is Nothing Then
            dgvDet.Columns("btnDelDet").Visible = blnEliminarEnabledDetalleBD
            If blnSoloConsulta Then dgvDet.Columns("btnDelDet").Visible = False
        End If

        If Not dgvDet.Columns("btnAnular") Is Nothing Then
            dgvDet.Columns("btnAnular").Visible = blnEliminarEnabledDetalleBD
            If blnSoloConsulta Then dgvDet.Columns("btnAnular").Visible = False
        End If


        'dgv.Columns("btnDel").Visible = blnEliminarEnabled
        'dgvDet.Columns("btnDelDet").Visible = blnEliminarEnabled


        frmMain.btnImprimir.Enabled = blnImprimirEnabled
        btnImprimir.Enabled = blnImprimirEnabled

        frmMain.btnSalir.Enabled = blnSalirEnabled
        btnSalir.Enabled = blnSalirEnabled

    End Sub


    Private Sub frmMantUsuarios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        'frmMain.btnSalir.Enabled = blnSalirEnabled
        btnSalir.Enabled = blnSalirEnabled
        ' AddHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        btnNuevo.Enabled = blnNuevoEnabled
        ' AddHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'btnEliminar.Enabled = blnEliminarEnabled
        'AddHandler frmMain.btnEditar.Click, AddressOf pEditar
        'frmMain.btnEditar.Enabled = blnEditarEnabled
        'btnEditar.Enabled = blnEditarEnabled
        ' AddHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        btnImprimir.Enabled = blnImprimirEnabled

        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        btnDeshacer.Enabled = blnDeshacerEnabled

        blnLoad = True

        pCambiarTitulosToolBar(Me)

        If blnServicioVarios = True Then
            'Buscar()
            If dgv.Rows.Count = 0 Then
                pNuevo(Nothing, Nothing)
            End If
            'blnServicioVarios = False
        End If

        If btnGrabar.Visible Then
            blnServicioVarios = True
        End If

    End Sub

    Private Sub frmMantUsuarios_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir
        'RemoveHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'RemoveHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'RemoveHandler frmMain.btnEditar.Click, AddressOf pEditar
        'RemoveHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        pCambiarTitulosToolBar(Me, True)
        If blnCotizacion Then blnServicioVarios = False
    End Sub
    Private Sub pImprimir(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnImprimir.Click

        'Try
        '    Dim frmRep As New frmCReportViewer
        '    With frmRep
        '        Dim obj As New clsServicioProveedorBN
        '        .dt = obj.ConsultarRpt("", _
        '                         If(cboCiudad.Text = "", "", cboCiudad.SelectedValue.ToString) _
        '                        , If(cboTipo.Text = "", "", cboTipo.SelectedValue.ToString), _
        '                                chkServCoticen.Checked, txtDescServicio.Text, _
        '                                txtRazonSocial.Text, _
        '                         If(cboCategSeTours.Text = "", "", cboCategSeTours.SelectedValue.ToString), _
        '                          nudCategoria.Value)
        '        ' If Not .dt Is Nothing Then
        '        'If .dt.Rows.Count = 0 Then
        '        '    MessageBox.Show("El Reporte de Servicios de Proveedores no contiene datos", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '        'Else
        '        .strRepNom = "ServicioProveedores"
        '        Dim strFiltro As String = ""

        '        If cboPais.Text <> "" Then
        '            strFiltro = strFiltro & " País seleccionado '" & strDevuelveCadenaNoOcultaIzquierda(cboPais.Text.Trim) & "',"
        '        End If
        '        If cboCiudad.Text <> "" Then
        '            strFiltro = strFiltro & " Ciudad seleccionada '" & cboCiudad.Text & "',"
        '        End If
        '        If txtRazonSocial.Text.Trim <> "" Then
        '            strFiltro = strFiltro & " Proveedor incluye '" & txtRazonSocial.Text.Trim & "',"
        '        End If

        '        If txtDescServicio.Text.Trim <> "" Then
        '            strFiltro = strFiltro & " Servicio incluye '" & txtDescServicio.Text.Trim & "',"
        '        End If
        '        Dim vrcpHotel As Boolean = False
        '        If cboTipo.Text.Trim <> "<TODOS>" Then
        '            strFiltro = strFiltro & " Tipo de Servicio seleccionado '" & cboTipo.Text & "',"
        '            If cboTipo.Text = "HOTELES" Then vrcpHotel = True
        '        End If
        '        If nudCategoria.Value <> 0 Then
        '            strFiltro = strFiltro & " Numero de categoria perteneciente '" & nudCategoria.Value.ToString & "',"
        '        End If
        '        If vrcpHotel Then
        '            If cboCategSeTours.Text <> "" Then
        '                strFiltro = strFiltro & " Categoria Setours seleccionada '" & cboCategSeTours.Text.Trim & "',"
        '            End If
        '        End If

        '        .strPar1 = ""
        '        If strFiltro <> "" Then 'Mid(strFiltro, 1, Len(strFiltro) - 1)
        '            .strPar1 = "Filtrado : " & Mid(strFiltro, 1, Len(strFiltro) - 1)
        '        End If
        '        .ShowDialog()
        '        'End If
        '        'ElseIf .dt.Rows.Count = 0 Then
        '        'MessageBox.Show("El Reporte requerido no contiene datos", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '        'End If
        '    End With
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub frmMantUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Me.Top = 0 : Me.Left = 0
        dgv.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
        dgv.DefaultCellStyle.SelectionForeColor = Color.Black
        dgvDet.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
        dgvDet.DefaultCellStyle.SelectionForeColor = Color.Black
        dgv.Columns("Descripcion").Tag = 1
        dgvDet.Columns("DescripcionDet").Tag = 1
        Try
            dgvDet.Columns("IDServicio_Det").Visible = True
            dgvDet.Columns("IDServicio_Det_V").Visible = True
            'dgv.Columns("CoMonedaPais").Visible = True
            CargarCombos()
            'If strProveedor <> "" Then
            If Not frmRefIngDetCotiDatoMem Is Nothing Or Not frmRefIngDetServicioDato Is Nothing Or _
                Not frmRefIngCotizacionDato Is Nothing Or Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
                Not frmRefIngOperacionProgramacion Is Nothing Or Not frmRefIngPresupuestoDatoMem Is Nothing Or _
                Not frmRefServOperadores Is Nothing Or Not frmRefIncluidos Is Nothing Or _
                Not frmRefMantStockEntradasDato Is Nothing Or Not frmRefMantServiciosDias Is Nothing Then
                'Not frmRefIngOperacionProgramacion Is Nothing Then
                Me.CenterToScreen()

                If blnServicioVarios = True Then
                    GoTo Mant
                End If
                txtRazonSocial.Text = strProveedor

                blnNuevoEnabled = False
                blnEliminarEnabled = False
                blnEditarEnabled = False

                dgv.Columns("btnDetalle").Visible = False
                dgv.Columns("btnEdit").Visible = False
                dgv.Columns("btnDel").Visible = False
                dgv.Columns("btnCopiar").Visible = False
                dgv.Columns("btnCopiarDetalles").Visible = False

                If strModulo = "IV" Or strModulo = "SK" Then
                    dgvDet.Columns("btnEditDet").Visible = False
                    'If strModulo = "SK" Then chkServCoticen.Checked = False
                    'chkServCoticen.Enabled = False
                End If
                'dgvDet.Columns("btnEditDet").Visible = False
                dgvDet.Columns("btnDelDet").Visible = False
                dgvDet.Columns("btnAnular").Visible = False
                dgvDet.Columns("btnUpdRelacRetro").Visible = False

                If Not frmRefIngDetServicioDato Is Nothing Or Not frmRefIngDetCotiDatoMem Is Nothing Then
                    dgvDet.Columns("VieneServicio").Visible = True
                    dgvDet.Columns("VieneProveedor").Visible = True
                Else
                    dgvDet.Columns("VieneServicio").Visible = False
                    dgvDet.Columns("VieneProveedor").Visible = False
                End If

                If frmRefIngOperacionProgramacion Is Nothing Then
                    Me.Text = "Consulta de Detalle de Servicio de Proveedor"
                Else
                    If strTipoConsProgramacion = "P" Then
                        Me.Text = "Seleccionar Proveedor..."
                    ElseIf strTipoConsProgramacion = "S" Then
                        Me.Text = "Seleccionar Detalle de Servicio..."
                    End If
                    chkServCoticen.Checked = False
                    chkServCoticen.Enabled = False
                End If



                lblAnioEtiq.Visible = True
                cboAnio.Visible = True

                If strAnioDiaSgte <> "" Then cboAnio.SelectedValue = strAnioDiaSgte
                If strTipoProveedor <> "" Then
                    If Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
                        Not frmRefIngOperacionProgramacion Is Nothing Then 'Or _
                        'Not frmRefServOperadores Is Nothing Then
                        If strModulo <> "O" Then
                            cboTipo.Enabled = False
                        End If

                        If Not frmRefIngReservasCtrlxFileDato Is Nothing Then
                            If Not frmRefIngReservasCtrlxFileDato.blnNuevoProveedor And strModulo = "O" Then
                                cboTipo.Enabled = False
                            End If
                        End If


                        If frmRefIngOperacionProgramacion Is Nothing Then
                            chkServCoticen.Enabled = False
                        End If
                    End If
                    If InStr(strTipoProveedor, ",") > 0 Then
                        cboTipo.Enabled = True
                    Else
                        cboTipo.SelectedValue = strTipoProveedor
                        If blnSeleccionHotelTC Then cboTipo.Enabled = False
                    End If
                End If

                'cboTipo.Enabled = False
                'txtDescServicio.Enabled = False
                'txtRazonSocial.Enabled = False
                'chkActivo.Enabled = False
                If Not frmRefIngCotizacionDato Is Nothing Or _
                    Not frmRefIngDetCotiDatoMem Is Nothing Or _
                    Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
                    Not frmRefIngOperacionProgramacion Is Nothing Or _
                    Not frmRefServOperadores Is Nothing Or _
                    Not frmRefIncluidos Is Nothing Or _
                    Not frmRefMantStockEntradasDato Is Nothing Or _
                    Not frmRefMantServiciosDias Is Nothing Then
                    'Not frmRefIngOperacionProgramacion Is Nothing Then

                    If strIDPais = "" Then
                        strIDPais = gstrPeru
                    End If
                    cboPais.SelectedValue = strIDPais
                    cboPais_SelectionChangeCommitted(Nothing, Nothing)
                    cboCiudad.SelectedValue = strIDCiudad

                    If strIDCiudad <> "" Then
                        btnBuscar_Click(Nothing, Nothing)
                    End If

                    If frmRefServOperadores IsNot Nothing Then
                        blnLoad = True
                        btnBuscar_Click(Nothing, Nothing)
                        blnLoad = False
                    End If

                    If blnReemplazaServiciosHotel Then
                        btnBuscar_Click(Nothing, Nothing)
                        'Localizando al Servicio
                        For Each dgvItem As DataGridViewRow In dgv.Rows
                            If dgvItem.Cells("IDServicio").Value.ToString = strIDServicioHotel Then
                                dgv.CurrentCell = dgv.Item("Descripcion", dgvItem.Index)
                                Exit For
                            End If
                        Next

                        'Localizando al Detalle
                        For Each dgvItem As DataGridViewRow In dgvDet.Rows
                            If CInt(dgvItem.Cells("IDServicio_Det").Value) = intIDServicio_DetHotel Then
                                dgvDet.CurrentCell = dgvDet.Item("Anio", dgvItem.Index)
                                Exit For
                            End If
                        Next
                        With frmRefIngDetCotiDatoMem
                            .intIDServicio_Det = dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString
                            If dgv.CurrentRow.Cells("IDTipoProv").Value.ToString = gstrTipoProveeHoteles Or dgv.CurrentRow.Cells("IDTipoProv").Value.ToString = gstrTipoProveeRestaurantes Then
                                .strDescServicio_Det = dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString
                            Else
                                .strDescServicio_Det = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                            End If
                            .strDescServicio = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                            .strTipoServicio_Det = dgvDet.CurrentRow.Cells("Tipo").Value.ToString
                            .blnConAlojamiento = dgvDet.CurrentRow.Cells("ConAlojamiento").Value
                            .strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value.ToString
                            .strAnio = dgvDet.CurrentRow.Cells("Anio").Value.ToString
                            .cboTipoProv.SelectedValue = dgv.CurrentRow.Cells("IDTipoProv").Value.ToString
                            .cboPais.SelectedValue = dgv.CurrentRow.Cells("IDPais").Value.ToString
                            .cboPais_SelectionChangeCommitted(Nothing, Nothing)
                            .cboCiudad.SelectedValue = dgv.CurrentRow.Cells("IDCiudad").Value.ToString
                            .lblProveedor.Text = dgv.CurrentRow.Cells("RazonSocial").Value.ToString
                            .strIDProveedor = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
                            .strIDTipoServ = dgv.CurrentRow.Cells("IDTipoServ").Value.ToString
                            .strDescTipoServ = dgv.CurrentRow.Cells("DescTipoServ").Value.ToString
                            .lblTipoServicio.Text = dgv.CurrentRow.Cells("DescTipoServ").Value.ToString
                            If dgv.CurrentRow.Cells("IDTipoProv").Value = gstrTipoProveeHoteles Or _
                                dgv.CurrentRow.Cells("IDTipoProv").Value = gstrTipoProveeRestaurantes Then
                                .chkDesayuno.Checked = dgvDet.CurrentRow.Cells("DesayunoDet").Value
                                .chkLonche.Checked = dgvDet.CurrentRow.Cells("LoncheDet").Value
                                .chkAlmuerzo.Checked = dgvDet.CurrentRow.Cells("AlmuerzoDet").Value
                                .chkCena.Checked = dgvDet.CurrentRow.Cells("CenaDet").Value
                            Else
                                .chkDesayuno.Checked = dgv.CurrentRow.Cells("Desayuno").Value
                                .chkLonche.Checked = dgv.CurrentRow.Cells("Lonche").Value
                                .chkAlmuerzo.Checked = dgv.CurrentRow.Cells("Almuerzo").Value
                                .chkCena.Checked = dgv.CurrentRow.Cells("Cena").Value
                            End If
                            .chkTransfer.Checked = dgv.CurrentRow.Cells("Transfer").Value
                            .cboTipoTransporte.SelectedValue = dgv.CurrentRow.Cells("TipoTransporte").Value
                            .cboIDUbigeoOri.SelectedValue = dgv.CurrentRow.Cells("IDUbigeoOri").Value.ToString
                            .cboIDUbigeoDes.SelectedValue = dgv.CurrentRow.Cells("IDUbigeoDes").Value.ToString
                            .blnPlanAlimenticio = dgvDet.CurrentRow.Cells("PlanAlimenticio").Value
                        End With
                        Me.Close()


                    End If
                End If

                If Not frmRefIngPresupuestoDatoMem Is Nothing Then
                    Me.Text += " - Gastos Operativos"
                End If
                If frmRefMantServiciosDias IsNot Nothing Then
                    blnLoad = True
                    btnBuscar_Click(Nothing, Nothing)
                    blnLoad = False
                End If
                txtRazonSocial.Select()
            Else
Mant:
                If blnServicioVarios = False Then
                    Me.Top = 0 : Me.Left = 0
                    Me.Text = "Mantenimiento de Servicios de Proveedor"

                    'lblAnioEtiq.Visible = False
                    'lblCategoriaEtiq.Visible = False
                    'lblCategoriaSEToursEtiq.Visible = False
                    'cboAnio.Visible = False
                    'nudCategoria.Visible = False
                    'cboCategSeTours.Visible = False
                    If strTipoProveedor <> "" Then
                        cboTipo.SelectedValue = strTipoProveedor
                    End If
                    txtRazonSocial.Text = strProveedor
                    txtRazonSocial.Select()

                    If blnServicioxTarifas Then
                        Buscar()
                        dgv_CellEnter(Nothing, Nothing)
                    End If
                Else
                    'Me.CenterToScreen()
                    ''Dim _bLoad As Boolean = blnLoad
                    ''If blnLoad Then
                    ''    blnLoad = False
                    ''End If
                    ''chkServCoticen.Checked = False
                    ''blnLoad = _bLoad
                    Buscar()
                    dgv_CellEnter(Nothing, Nothing)
                    Me.WindowState = FormWindowState.Maximized
                    Me.MaximizeBox = True
                    Me.MinimizeBox = True
                    grb.Dock = DockStyle.None
                    grb.Location = New System.Drawing.Point(0, 80)

                    grb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom _
                        Or System.Windows.Forms.AnchorStyles.Left) _
                        Or System.Windows.Forms.AnchorStyles.Right),  _
                        System.Windows.Forms.AnchorStyles)

                    toolBar.Visible = True

                    Me.Text = "Mantenimiento de Servicios de Proveedor del File: " & strTituloFile

                    'If dgv.Rows.Count = 0 Then
                    '    pNuevo(Nothing, Nothing)
                    'End If
                    If blnCotizacion Then blnServicioVarios = True
                End If
            End If

            If Not frmRefIngDetCotiDatoMem Is Nothing Then
                If blnNuevoServicioCot Then
                    lblEtiqDiaServicio.Visible = True
                    dtpFechaInsertarCoti.Visible = True
                    dtpFechaInsertarCoti.Value = datFechaInsert
                End If
            End If

            CargarSeguridad()
            ActivarDesactivarBotones()
            dgv.ColumnHeadersDefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Bold)
            dgvDet.ColumnHeadersDefaultCellStyle.Font = New Font(dgvDet.Font, FontStyle.Bold)

            pDgvAnchoColumnas(dgv)
            pDgvAnchoColumnas(dgvDet)

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ColorearAnioProviniente()
        Try
            For Each Item As DataGridViewRow In dgvDet.Rows
                If Item.Cells("VieneAnio").Value.ToString.Trim <> "" Then
                    If Item.Cells("Anio").Value.ToString <> Item.Cells("VieneAnio").Value.ToString Then
                        Item.Cells("VieneAnio").Style.ForeColor = Color.Red
                        Item.Cells("VieneAnio").Style.SelectionForeColor = Color.Red
                        'Item.Cells("VieneAnio").Style.Font = New Font("Tahoma", 8, FontStyle.Bold, GraphicsUnit.Point)
                    End If
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub ColorearDetalleServicioAnulado()
        Try
            For Each Item As DataGridViewRow In dgvDet.Rows
                If Item.Cells("Estado").Value.ToString = "X" Then
                    Item.DefaultCellStyle.ForeColor = Color.Gray
                    Item.DefaultCellStyle.SelectionForeColor = Color.Gray
                ElseIf Item.Cells("Estado").Value.ToString = "A" Then
                    Item.DefaultCellStyle.SelectionForeColor = System.Drawing.SystemColors.ControlText
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Me.Close()
        'frmMain.DesabledBtns()
    End Sub
    Private Sub frmMantBancos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        ' frmMain.DesabledBtns()
    End Sub

    Private Sub cboPais_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboPais.SelectionChangeCommitted
        If cboPais.SelectedValue Is Nothing Then Exit Sub
        Try
            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboCiudad, objBLUbig.ConsultarCboparaProvee(, cboPais.SelectedValue.ToString, True), True)

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarCombos()
        Try
            'Dim objBLCat As New clsTablasApoyoBN.clsCategoriaBN
            'pCargaCombosBox(cboCategSeTours, objBLCat.ConsultarCbo(True), True)


            If strAnio = "" Then
                'strAnio = Today.Year
                If strModulo = "IV" Then
                    Dim dttAnios As New DataTable("Anios")
                    With dttAnios
                        .Columns.Add("Anio")
                        .Columns.Add("AnioValor")
                        .Rows.Add("", "<TODOS>")

                        'For intAnio As Int16 = Convert.ToInt16(strAnio) - 10 To Convert.ToInt16(strAnio) + 10
                        '    .Rows.Add(intAnio.ToString, intAnio.ToString)
                        'Next
                        If strAnioDiaSgte <> "" Then .Rows.Add(strAnioDiaSgte, strAnioDiaSgte)
                        pCargaCombosBox(cboAnio, dttAnios, True)
                    End With
                End If
            Else
                Dim dttAnios As New DataTable("Anios")
                With dttAnios
                    .Columns.Add("Anio")
                    .Columns.Add("AnioValor")
                    .Rows.Add("", "<TODOS>")

                    'For intAnio As Int16 = Convert.ToInt16(strAnio) - 10 To Convert.ToInt16(strAnio) + 10
                    '    .Rows.Add(intAnio.ToString, intAnio.ToString)
                    'Next
                    If strAnioDiaSgte <> "" Then .Rows.Add(strAnioDiaSgte, strAnioDiaSgte)
                    pCargaCombosBox(cboAnio, dttAnios, True)
                End With
            End If

            Dim objBLCat As New clsTablasApoyoBN.clsCategoriaBN
            pCargaCombosBox(cboCategSeTours, objBLCat.ConsultarCbo(True), True)

            Dim objBLUbig As New clsTablasApoyoBN.clsUbigeoBN
            pCargaCombosBox(cboPais, objBLUbig.ConsultarCbo("PAIS", , True), True)

            cboPais.SelectedValue = gstrPeru
            cboPais_SelectionChangeCommitted(Nothing, Nothing)




            Dim objBLTPrv As New clsTablasApoyoBN.clsTipoProveedorBN
            Dim dttTipoProv As DataTable = objBLTPrv.ConsultarCbo(True)

            pCargaCombosBox(cboTipo, dttTipoProv, True)

            'If InStr(strTipoProveedor, ",") > 0 Then
            '    pCargaCombosBox(cboTipo, dttTipoProvFiltrado(dttTipoProv), True)
            '    If cboTipo.Items.Count > 0 Then cboTipo.SelectedIndex = 0
            'Else
            '    pCargaCombosBox(cboTipo, dttTipoProv, True)
            'End If



        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Function dttTipoProvFiltrado(ByVal vdttTipoProv As DataTable) As DataTable
        Dim ListTipoProv As New List(Of String)
        Dim strTipoProveedorLoc As String = "" ' = strTipoProveedor
        For i As Byte = 0 To strTipoProveedor.Length - 1

            Dim strCar As Char = strTipoProveedor.Substring(i, 1)
            If strCar <> "," Then
                strTipoProveedorLoc &= strCar
            Else
                ListTipoProv.Add(strTipoProveedorLoc)
                strTipoProveedorLoc = ""
            End If

        Next
        ListTipoProv.Add(strTipoProveedorLoc)

        Dim dttTipoProvHCore As DataTable = vdttTipoProv.Clone
        For Each dr As DataRow In vdttTipoProv.Rows
            For Each strTipoProv As String In ListTipoProv
                If strTipoProv = dr("IDTipoProv") Then
                    dttTipoProvHCore.ImportRow(dr)
                End If
            Next
        Next

        Return dttTipoProvHCore
    End Function

    Public Sub dgv_CellEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEnter
        If Not blnLoad And strIDCiudad = "" And Not blnServicioVarios And Not blnReemplazaServiciosHotel Then Exit Sub
        Try
            Dim strAnio As String = ""
            Dim strActivo As String = ""
            Dim strIDServicio As String = ""

            If Not frmRefIngDetCotiDatoMem Is Nothing Or Not frmRefIngDetServicioDato Is Nothing Or Not frmRefIngReservasCtrlxFileDato Is Nothing Then
                If cboAnio.Items.Count > 0 Then
                    strAnio = If(cboAnio.Text = "", "", cboAnio.SelectedValue.ToString)
                Else
                    strAnio = strAnioDiaSgte
                End If
                strActivo = "A"
            Else
                strAnio = ""
            End If
            If Not dgv.CurrentRow Is Nothing Then
                If blnReemplazaServiciosHotel Then
                    strIDServicio = strIDServicioHotel
                    strAnio = ""
                Else
                    strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value.ToString()
                End If
            End If

            dgvDet.DataSource = objBN.ConsultarDetallexIDServicio(strIDServicio, strAnio, strTipoGasto, strActivo)
            pDgvAnchoColumnas(dgvDet)
            ColorearAnioProviniente()
            ColorearDetalleServicioAnulado()
            lblLectura.Text = ""
            If Not dgv.CurrentRow Is Nothing Then
                If dgv.CurrentRow.Cells("IDTipoProv").Value.ToString = gstrTipoProveeHoteles Then
                    dgvDet.Columns("Monto_dbl").Visible = True
                    dgvDet.Columns("Monto_tri").Visible = True
                    dgvDet.Columns("Monto_sgl").HeaderText = "Monto Simple"
                Else
                    dgvDet.Columns("Monto_dbl").Visible = False
                    dgvDet.Columns("Monto_tri").Visible = False
                    dgvDet.Columns("Monto_sgl").HeaderText = "Monto (en DBL)"
                End If
                dgvDet.Columns("Monto_sgl").Width = 80
                dgvDet.Columns("VieneAnio").Width = 60
                lblLectura.Text = Replace(dgv.CurrentRow.Cells("Lectura").Value.ToString, "&", "&&")
            End If
            MostrarDatosFeedBackxProveedor()

            'Dim vsr As New VisualStyleRenderer(VisualStyleElement.TrackBar.Thumb.Normal)
            'vsr.DrawBackground(frmMantServiciosProveedorDato.CreateGraphics(), New Rectangle(10, 10, 90, 20))

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub MostrarDatosFeedBackxProveedor() Handles lblRoomnights.TextChanged
        Try
            Dim intExcellent = 0, intPoor = 0, intAverage = 0, intGood = 0, intVeryGood As Int16 = 0
            Dim intRoomNight = 0, intCantServ = 0, intTotalFiles As Int16 = 0
            Dim strIDProveedorSelect As String = String.Empty
            Dim strIDTipoProvSel As String = String.Empty
            If dgv.CurrentRow Is Nothing Then
                GoTo DefaultBack
                Exit Sub
            End If

            strIDProveedorSelect = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
            strIDTipoProvSel = dgv.CurrentRow.Cells("IDTipoProv").Value.ToString

            If strIDTipoProvSel <> gstrTipoProveeHoteles Then
                lblRoomRes.Text = Replace(lblRoomRes.Text, "Roomnights", "Reservas")
            Else
                lblRoomRes.Text = Replace(lblRoomRes.Text, "Reservas", "Roomnights")
            End If

            Using objC As clsControlCalidadProBN = New clsControlCalidadProBN
                Using dr As SqlClient.SqlDataReader = objC.ConsultarResumen(strIDProveedorSelect)
                    dr.Read()
                    If dr.HasRows Then
                        intExcellent = If(IsDBNull(dr("SumaExcelente")) = True, 0, dr("SumaExcelente"))
                        intVeryGood = If(IsDBNull(dr("SumaVeryGood")) = True, 0, dr("SumaVeryGood"))
                        intGood = If(IsDBNull(dr("SumaGood")) = True, 0, dr("SumaGood"))
                        intAverage = If(IsDBNull(dr("SumaAverage")) = True, 0, dr("SumaAverage"))
                        intPoor = If(IsDBNull(dr("SumaPoor")) = True, 0, dr("SumaPoor"))
                        intTotalFiles = If(IsDBNull(dr("CantFiles")) = True, 0, dr("CantFiles"))
                        intRoomNight = If(IsDBNull(dr("RoomNights")) = True, 0, dr("RoomNights"))
                        intCantServ = If(IsDBNull(dr("CantidadServicios")) = True, 0, dr("CantidadServicios"))
                    End If
                    dr.Close()
                End Using
            End Using

            ' ''If dttDatos.Rows.Count > 0 Then
            ' ''    'intExcellent = If(IsDBNull(dttDatos.Rows(0)("CantidadExcellent")) = True, 0, dttDatos.Rows(0)("CantidadExcellent"))
            ' ''    'intVeryGood = If(IsDBNull(dttDatos.Rows(0)("CantidadVeryGood")) = True, 0, dttDatos.Rows(0)("CantidadVeryGood"))
            ' ''    'intGood = If(IsDBNull(dttDatos.Rows(0)("CantidadGood")) = True, 0, dttDatos.Rows(0)("CantidadGood"))
            ' ''    'intAverage = If(IsDBNull(dttDatos.Rows(0)("CantidadAverage")) = True, 0, dttDatos.Rows(0)("CantidadAverage"))
            ' ''    'intPoor = If(IsDBNull(dttDatos.Rows(0)("CantidadPoor")) = True, 0, dttDatos.Rows(0)("CantidadPoor"))
            ' ''    'intTotalFiles = If(IsDBNull(dttDatos.Rows(0)("CantidadFile")) = True, 0, dttDatos.Rows(0)("CantidadFile"))
            ' ''    'intRoomNight = If(IsDBNull(dttDatos.Rows(0)("CantidadRoomNight")) = True, 0, dttDatos.Rows(0)("CantidadRoomNight"))

            ' ''    For Each dRowItem As DataRow In dttDatos.Rows
            ' ''        intExcellent += Convert.ToInt16(dRowItem("CantExcelent"))
            ' ''        intVeryGood += Convert.ToInt16(dRowItem("CantVeryGood"))
            ' ''        intGood += Convert.ToInt16(dRowItem("CantGood"))
            ' ''        intAverage += Convert.ToInt16(dRowItem("CantAverage"))
            ' ''        intPoor += Convert.ToInt16(dRowItem("CantPoor"))

            ' ''        If Convert.ToInt16(dRowItem("Item")) = 1 Then
            ' ''            intRoomNight += Convert.ToInt16(dRowItem("RoomNights"))
            ' ''            intCantServ += Convert.ToInt16(dRowItem("CantServiciosResv"))
            ' ''            intTotalFiles += Convert.ToInt16(dRowItem("Item"))
            ' ''        End If
            ' ''    Next
            ' ''End If
DefaultBack:
            Dim intValueSlider As Int16 = 0
            Dim intExcellentVal As Int16 = intExcellent * 20
            Dim intVeryGoodVal As Int16 = intVeryGood * 15
            Dim intGoodVal As Int16 = intGood * 10
            Dim intAverageVal As Int16 = intAverage * 5
            Dim intPoorVal As Int16 = intPoor * 1
            If strIDTipoProvSel = gstrTipoProveeHoteles Then
                lblRoomnights.Text = intRoomNight.ToString
            Else
                lblRoomnights.Text = intCantServ.ToString
            End If

            Dim intValTotal As Int16 = intExcellentVal + intVeryGoodVal + intGoodVal + intAverageVal + intPoorVal
            Dim intMaxValPorc As Int16 = (intExcellent + intVeryGood + intGood + intAverage + intPoor) * 20 'intTotalFiles * 20

            If intMaxValPorc > 0 Then
                intValueSlider = (intValTotal / intMaxValPorc) * 100
                If CInt(intValueSlider) > 100 Then intValueSlider = 100
            End If
            colorSlider5.Value = CInt(intValueSlider)
            ToolTip1.SetToolTip(colorSlider5, CInt(intValueSlider).ToString & "%")
            colorSlider5.Text = CInt(intValueSlider).ToString & "%"

            '------------------------------------------------------------------------------------------------------
            ''Dim myBrush As New System.Drawing.SolidBrush(System.Drawing.Color.Red)
            ''Dim formGraphics As System.Drawing.Graphics
            ''formGraphics = Me.CreateGraphics()
            ''formGraphics.FillRectangle(myBrush, New Rectangle(0, 0, 200, 300))
            ''myBrush.Dispose()
            ''formGraphics.Dispose()
            '------------------------------------------------------------------------------------------------------
            'Me.tcbRango.DisplayRectangle.SolidBrush(Color.FromArgb(87, 147, 17))
            'Dim vTrackBar1 As TrackBar
            'vTrackBar1.Us()
            'Dim blendex As Blend()

            'Dim linGrBrush As New LinearGradientBrush(New Point(0, 10), New Point(200, 10), Color.FromArgb(255, 255, 0, 0), Color.FromArgb(255, 0, 0, 255))
            'Dim formGraphics As System.Drawing.Graphics
            'formGraphics = frmMantServiciosProveedorDato.CreateGraphics() 'Me.CreateGraphics()
            'formGraphics.FillRectangle(linGrBrush, New Rectangle(0, 0, 200, 300))
            '-------------------------------------------------------------------------------------------------------
            ''If TrackBarRenderer.IsSupported Then
            ''    Dim trackBarRect As New Rectangle(120, 150, 100, 5)
            ''    Dim trackBarTrackRect As New Rectangle( _
            ''        trackBarRect.X + 8, trackBarRect.Y + 10, trackBarRect.Width - 16, 4)

            ''    Dim trackBarTicksRect As New Rectangle( _
            ''        trackBarRect.X + 13, trackBarRect.Y + 25, trackBarRect.Width - 26, 4)
            ''    Dim trackBarThumbRect As New Rectangle( _
            ''        trackBarRect.X + 8, trackBarRect.Y + 2, 11, 21)

            ''    TrackBarRenderer.DrawHorizontalTrack(g, trackBarTrackRect)
            ''    TrackBarRenderer.DrawHorizontalTicks(g, trackBarTicksRect, 11, _
            ''                                         System.Windows.Forms.VisualStyles.EdgeStyle.Raised)
            ''    TrackBarRenderer.DrawBottomPointingThumb(g, trackBarThumbRect, _
            ''                                             System.Windows.Forms.VisualStyles.TrackBarThumbState.Normal)
            ''End If
            '-------------------------------------------------------------------------------------------------------
            'Me.tcbRango.UseThemeBackground = False
            '-------------------------------------------------------------------------------------------------------
            ''Dim oTrack As New clsTrackBarTest
            ''With oTrack
            ''    .Name = "TrackBar2"
            ''    .Visible = True
            ''    .BeginInit()
            ''End With
            ''Me.Controls.Add(oTrack)
            ''oTrack.EndInit()


            lblExcellent.Text = intExcellent.ToString
            lblVeryGood.Text = intVeryGood.ToString
            lblGood.Text = intGood.ToString
            lblAverage.Text = intAverage.ToString
            lblPoor.Text = intPoor.ToString
            lblFiles.Text = intTotalFiles.ToString
            lblPercent.Text = CInt(intValueSlider).ToString & "%"

            'My.Application.DoEvents()
            Dim PosX As Int16 = 10
            PosX += CInt((CInt(intValueSlider) / 100) * 265)

            lblPercent.Location = New Point(PosX, 98)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub pEditar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEditar.Click
        Try
            Editar()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub


    Private Sub Editar()
        'If dgv.CurrentCell Is Nothing Then Exit Sub

        'If Not frmRefIngDetCotiDatoMem Is Nothing And Not blnServicioVarios Then Exit Sub
        'If Not frmRefIngDetServicioDato Is Nothing Then Exit Sub
        'If Not frmRefIngCotizacionDato Is Nothing Then Exit Sub
        'If Not frmRefIngReservasCtrlxFileDato Is Nothing Then Exit Sub

        'Try
        '    Dim frm As New frmMantServiciosProveedorDato
        '    With frm
        '        .strIDServicio = dgv.Item("IDServicio", dgv.CurrentCell.RowIndex).Value.ToString
        '        .strDescripcion = dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString

        '        If dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString = gstrTipoProveeHoteles Or _
        '            dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString = gstrTipoProveeRestaurantes Then
        '            .strIDUbigeo = dgv.Item("IDCiudad", dgv.CurrentCell.RowIndex).Value.ToString
        '        End If
        '        .blnCotizacion = blnCotizacion
        '        .blnServicioVarios = blnServicioVarios
        '        If Not dgv.Item("IDCabVarios", dgv.CurrentCell.RowIndex).Value Is Nothing Then
        '            .intIDCabVarios = CInt(dgv.Item("IDCabVarios", dgv.CurrentCell.RowIndex).Value)
        '        End If
        '        .FormRef = Me
        '        If Not blnServicioVarios Then
        '            .MdiParent = frmMain
        '            .Show()
        '        Else
        '            .ShowDialog()
        '            Buscar()
        '        End If
        '        'blnServicioVarios = False
        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub Detalle()
        'Try
        '    'Dim frm As New frmMantDetServiciosProveedor
        '    'With frm
        '    '    .strIDServicio = dgv.Item("IDServicio", dgv.CurrentCell.RowIndex).Value.ToString
        '    '    .strIDTipoProv = dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString
        '    '    .strDescServicio = dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString

        '    '    .MdiParent = frmMain
        '    '    .FormRef = Me
        '    '    .Show()
        '    'End With

        '    Dim frm As New frmMantDetServiciosProveedorDato
        '    With frm
        '        .strIDServicio = dgv.Item("IDServicio", dgv.CurrentCell.RowIndex).Value.ToString
        '        .strIDTipoProv = dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString
        '        .strDescServicio = dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString
        '        .strIDTipoOper = dgv.Item("IDTipoOper", dgv.CurrentCell.RowIndex).Value.ToString
        '        .blnServicioVarios = blnServicioVarios
        '        .FormRefMantServiciosProveedor = Me
        '        .blnEsServicioInternacional = If(dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString = gstrTipoProveeHoteles And dgv.Item("IDPais", dgv.CurrentCell.RowIndex).Value.ToString <> gstrPeru, True, False)
        '        .sglTipoCambioPais = dgv.Item("SsTipCam", dgv.CurrentCell.RowIndex).Value.ToString
        '        .strCoMonedaPais = dgv.Item("CoMonedaPais", dgv.CurrentCell.RowIndex).Value.ToString
        '        .strCoSimboloMonedaPais = dgv.Item("SimboloMonedaPais", dgv.CurrentCell.RowIndex).Value.ToString
        '        If Not blnServicioVarios Then
        '            .MdiParent = frmMain

        '            .Show()
        '        Else
        '            .ShowDialog()
        '        End If
        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub pNuevo(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        'Try
        '    If strIDProveedor = "" Then

        '        Dim frm As New frmMantServiciosProveedorDato
        '        'Dim bytCountReg As Int16 = dgv.Rows.Count

        '        With frm


        '            .blnServicioVarios = blnServicioVarios

        '            If Not blnServicioVarios Then
        '                .MdiParent = frmMain
        '                .FormRef = Me
        '                .Show()
        '            Else
        '                blnServicioVarios = False
        '                .intIDCabVarios = intIDCab
        '                .ShowDialog()
        '                Buscar()
        '            End If
        '            '.BringToFront()
        '            '.Focus()
        '            'Buscar()
        '        End With
        '    Else
        '        NuevoxProveedor()
        '    End If
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub
    Public Sub NuevoxProveedor()
        'Try
        '    Dim frm As New frmMantServiciosProveedorDato
        '    With frm
        '        .MdiParent = frmMain
        '        .FormRef = Me
        '        .strIDProveedor = strIDProveedor
        '        .strIDTipoProv = strTipoProveedor
        '        .strDescProveedor = strProveedor
        '        .dblMargen = dblMargen
        '        .strIDTipoOper = strIDTipoOper
        '        .strIDPais = strIDPais
        '        .strIDCiudad = strIDCiudad
        '        .Show()
        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Public Sub Buscar(Optional ByVal vstrIDServicio As String = "")

        If Not blnBuscar Then Exit Sub

        Me.Cursor = Cursors.WaitCursor

        Try
            If vstrIDServicio = "" Then
                dgv.DataSource = objBN.ConsultarList(vstrIDServicio, strIDProveedor, txtRazonSocial.Text, _
                                                     txtDescServicio.Text, chkServCoticen.Checked, If(cboTipo.Text = "", "", cboTipo.SelectedValue.ToString), _
                                                     "", If(cboCiudad.Text = "", "", cboCiudad.SelectedValue.ToString), nudCategoria.Value, _
                                                     If(cboCategSeTours.Text = "", "", cboCategSeTours.SelectedValue.ToString), intIDCab, strTipoGasto, strCoPago, blnFiltrarTodosServicioyVarios, If(cboPais.Text = "", "", cboPais.SelectedValue.ToString))
            Else
                dgv.DataSource = objBN.ConsultarList(vstrIDServicio, "", "", _
                                                     "", False, "", _
                                                     "", "", 0, _
                                                     "", intIDCab, "", strCoPago)

            End If
            pDgvAnchoColumnas(dgv)
            If blnFiltrarTodosServicioyVarios Then ColorearServiciosVarios()

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Private Sub ColorearServiciosVarios()
        Try
            For Each dgvItem As DataGridViewRow In dgv.Rows
                If CInt(dgvItem.Cells("IDCabVarios").Value) <> 0 Then
                    dgvItem.DefaultCellStyle.SelectionBackColor = Color.LightYellow
                    dgvItem.DefaultCellStyle.BackColor = Color.LightYellow
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub pEliminar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try

            If MessageBox.Show("¿Está Ud. seguro de anular el Servicio " & dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString & "?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objBT As New clsServicioProveedorBT
            objBT.Anular(dgv.Item("IdServicio", dgv.CurrentCell.RowIndex).Value.ToString, gstrUser)

            MessageBox.Show("Se anuló el Servicio " & dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString & " exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Buscar()
            dgv_Click(Nothing, Nothing)
            dgv_CellEnter(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub frmMantBancos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
Handles Me.KeyDown

        Select Case e.KeyCode
            Case Keys.F2
                If frmMain.btnNuevo.Enabled Then pNuevo(Nothing, Nothing)
            Case Keys.F3
                If frmMain.btnEditar.Enabled Then pEditar(Nothing, Nothing)
            Case Keys.F4
                If frmMain.btnEliminar.Enabled Then pEliminar(Nothing, Nothing)
            Case Keys.F5
                btnBuscar_Click(Nothing, Nothing)
            Case Keys.F9
                If dgv.Rows.Count = 0 Then
                    'MessageBox.Show("No hay registros que imprimir", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Else
                    pImprimir(Nothing, Nothing)
                End If
            Case Keys.Enter
                btnBuscar_Click(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub
    Private Sub dgv_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        If dgv.Columns(e.ColumnIndex).Name = "btnEdit" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnEdit"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.editarpng
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If
        If dgv.Columns(e.ColumnIndex).Name = "btnDel" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnDel"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.eliminarpng
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If
        If dgv.Columns(e.ColumnIndex).Name = "btnDetalle" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnDetalle"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.application_double
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgv.Columns(e.ColumnIndex).Name = "btnCopiar" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnCopiar"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.page_copy
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgv.Columns(e.ColumnIndex).Name = "btnCopiarDetalles" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnCopiarDetalles"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.application_cascade
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

    End Sub

    Public Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Click
        If dgv.RowCount > 0 Then
            blnEditarEnabled = True
            blnEliminarEnabled = True
            blnImprimirEnabled = True
            ActivarDesactivarBotones()

        Else
            blnEditarEnabled = False
            blnEliminarEnabled = False
            blnImprimirEnabled = False
            ActivarDesactivarBotones()
        End If

    End Sub


    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick

        'If dgv.CurrentCell.ColumnIndex = dgv.Columns("btnEdit").Index Then
        '    Editar()
        'ElseIf dgv.CurrentCell.ColumnIndex = dgv.Columns("btnDel").Index Then
        '    pEliminar(Nothing, Nothing)

        'End If


        Select Case dgv.CurrentCell.ColumnIndex
            Case dgv.Columns("btnEdit").Index
                Try
                    Editar()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try
            Case dgv.Columns("btnDel").Index
                pEliminar(Nothing, Nothing)
            Case dgv.Columns("btnDetalle").Index
                Detalle()
            Case dgv.Columns("btnCopiar").Index
                Try
                    LlamarFormCopia()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try

            Case dgv.Columns("btnCopiarDetalles").Index
                Try
                    LlamarFormCopiaDet()
                Catch ex As Exception
                    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
                End Try

        End Select

    End Sub

    Private Sub LlamarFormCopia()
        If Not frmRefIngDetCotiDatoMem Is Nothing And Not blnServicioVarios Then Exit Sub
        If Not frmRefIngDetServicioDato Is Nothing Then Exit Sub
        If Not frmRefIngReservasCtrlxFileDato Is Nothing Then Exit Sub

        blnCopio = False
        Try
            With frmCopiarServicios
                .strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value.ToString
                .strDescripcion = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                .strIDProveedor = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
                .strDescProveedor = dgv.CurrentRow.Cells("RazonSocial").Value.ToString
                .strIDPais = dgv.CurrentRow.Cells("IDPais").Value.ToString
                .strIDUbigeo = dgv.CurrentRow.Cells("IDCiudad").Value.ToString
                .strIDTipoProv = dgv.CurrentRow.Cells("IDTipoProv").Value.ToString
                .frmRefMantServiciosProveedor = Me
                .ShowDialog()
            End With

            If blnCopio Then
                'Buscar(dgv.CurrentRow.Cells("IDServicio").Value.ToString)


                btnBuscar_Click(Nothing, Nothing)

                PosicicionarenFilaCab()
                strIDServicioCopiado = ""


                If blnLoad Then
                    dgv_Click(Nothing, Nothing)
                End If
                If dgv.Rows.Count = 0 Then
                    dgv_CellEnter(Nothing, Nothing)
                End If
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub PosicicionarenFilaCab()

        For Each dgvRow As DataGridViewRow In dgv.Rows
            If dgvRow.Cells("IDServicio").Value = strIDServicioCopiado Then
                dgv.CurrentCell = dgv.Item("Descripcion", dgvRow.Index)
                Exit For
            End If
        Next

    End Sub

    Private Sub PosicicionarenFilaCabDet()

        'For Each dgvRow As DataGridViewRow In dgv.Rows
        '    If dgvRow.Cells("IDServicio").Value = strIDServicioCopiado Then
        '        dgv.CurrentCell = dgv.Item(0, dgvRow.Index)
        '        Exit For
        '    End If
        'Next

        For Each dgvRow As DataGridViewRow In dgvDet.Rows
            If dgvRow.Cells("IDServicio_Det").Value = strIDServicio_DetCopiado Then
                dgvDet.CurrentCell = dgvDet.Item("Anio", dgvRow.Index)
                Exit For
            End If
        Next

    End Sub

    Private Sub LlamarFormCopiaDet()
        If Not frmRefIngDetCotiDatoMem Is Nothing And Not blnServicioVarios Then Exit Sub
        If Not frmRefIngDetServicioDato Is Nothing Then Exit Sub
        If Not frmRefIngReservasCtrlxFileDato Is Nothing Then Exit Sub

        blnCopio = False
        Try
            With frmCopiaDetServicios
                .strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value.ToString
                .strDescripcion = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                .frmRefMantServiciosProveedor = Me
                .ShowDialog()
            End With


            If blnCopio Then
                'Buscar(dgv.CurrentRow.Cells("IDServicio").Value.ToString)

                strIDServicioCopiado = dgv.CurrentRow.Cells("IDServicio").Value.ToString
                PosicicionarenFilaCab()
                strIDServicioCopiado = ""

                If blnLoad Then
                    dgv_Click(Nothing, Nothing)
                End If
                If dgv.Rows.Count = 0 Then
                    dgv_CellEnter(Nothing, Nothing)
                End If

                PosicicionarenFilaCabDet()
                strIDServicio_DetCopiado = ""
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgvDet_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvDet.CellContentClick
        If dgvDet.CurrentCell Is Nothing Then Exit Sub
        Try
            'If dgvDet.CurrentCell.ColumnIndex = dgvDet.Columns("btnEditDet").Index Then
            '    EditarDet()
            'ElseIf dgvDet.CurrentCell.ColumnIndex = dgvDet.Columns("btnDelDet").Index Then
            '    EliminarDet()
            'End If

            Select Case dgvDet.CurrentCell.ColumnIndex
                Case dgvDet.Columns("btnEditDet").Index
                    EditarDet()
                Case dgvDet.Columns("btnDelDet").Index
                    EliminarDet()
                Case dgvDet.Columns("btnAnular").Index
                    AnularDet()
                Case dgvDet.Columns("VieneProveedor").Index
                    ConsultarProveedor()
                Case dgvDet.Columns("VieneServicio").Index
                    ConsultarServicio()
                    ConsultarDetServicio()
                Case dgvDet.Columns("btnUpdRelacRetro").Index
                    If MessageBox.Show("¿Está Ud. seguro de actualizar las relaciones anteriores en base a este detalle de servicio?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                        Dim bytRegsAffect As Byte = bytActualizarRelacionRetro()

                        If bytRegsAffect > 0 Then
                            MessageBox.Show("Actualización realizada exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                        Else
                            MessageBox.Show("No se encontró ningún detalle de servicio afectado", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If

                    End If


            End Select


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub ConsultarProveedor()
        'If Not frmRefIngDetCotiDatoMem Is Nothing Then Exit Sub
        'If Not frmRefIngDetServicioDato Is Nothing Then Exit Sub
        'If Not frmRefIngReservasCtrlxFileDato Is Nothing Then Exit Sub

        'Try
        '    Dim frm As New frmMantProveedoresDato
        '    With frm
        '        .strIDProveedor = dgvDet.Item("VieneIDProveedor", dgvDet.CurrentCell.RowIndex).Value.ToString
        '        .strDescripcion = dgvDet.Item("VieneProveedor", dgvDet.CurrentCell.RowIndex).Value.ToString
        '        .MdiParent = frmMain
        '        .blnBloquearEdit = True
        '        .FormRefConsServ = Me
        '        .Show()
        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub ConsultarServicio()
        'If dgv.CurrentCell Is Nothing Then Exit Sub

        'If Not frmRefIngDetCotiDatoMem Is Nothing Then Exit Sub
        'If Not frmRefIngDetServicioDato Is Nothing Then Exit Sub
        'If Not frmRefIngReservasCtrlxFileDato Is Nothing Then Exit Sub

        'Try
        '    Dim frm As New frmMantServiciosProveedorDato
        '    With frm
        '        '.strIDServicio = dgv.Item("IDServicio", dgv.CurrentCell.RowIndex).Value.ToString
        '        '.strDescripcion = dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString
        '        .strIDServicio = dgvDet.Item("VieneIDServicio", dgvDet.CurrentCell.RowIndex).Value.ToString
        '        .strDescripcion = dgvDet.Item("VieneServicio", dgvDet.CurrentCell.RowIndex).Value.ToString

        '        If dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString = gstrTipoProveeHoteles Or _
        '            dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString = gstrTipoProveeRestaurantes Then
        '            .strIDUbigeo = dgv.Item("IDCiudad", dgv.CurrentCell.RowIndex).Value.ToString
        '        End If
        '        .blnCotizacion = blnCotizacion

        '        .blnBloquearEdit = True
        '        .FormRef = Me

        '        .MdiParent = frmMain
        '        .Show()

        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub ConsultarDetServicio()
        If dgv.CurrentCell Is Nothing Then Exit Sub

        If Not frmRefIngDetCotiDatoMem Is Nothing Then Exit Sub
        If Not frmRefIngDetServicioDato Is Nothing Then Exit Sub
        If Not frmRefIngReservasCtrlxFileDato Is Nothing Then Exit Sub

        Try
            Dim frm As New frmMantDetServiciosProveedorDato
            With frm
                .strIDServicio = dgvDet.Item("VieneIDServicio", dgvDet.CurrentCell.RowIndex).Value.ToString
                .strIDServicio_Det = dgvDet.Item("IDServicio_Det_V", dgvDet.CurrentCell.RowIndex).Value.ToString

                .strCoMonedaPais = dgv.Item("CoMonedaPais", dgv.CurrentCell.RowIndex).Value.ToString
                .strCoSimboloMonedaPais = dgv.Item("SimboloMonedaPais", dgv.CurrentCell.RowIndex).Value.ToString

                .blnCotizacion = blnCotizacion
                .blnServicioVarios = blnServicioVarios
                .blnBloquearEdit = True
                .FormRefMantServiciosProveedor = Me

                .MdiParent = frmMain
                .Show()

            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub EliminarDet()
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try

            If MessageBox.Show("¿Está Ud. seguro de eliminar el Detalle de Servicio " & dgvDet.Item("DescripcionDet", dgvDet.CurrentCell.RowIndex).Value.ToString & "?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
            objBT.Eliminar(dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString)

            'Buscar()
            'dgv_Click(Nothing, Nothing)
            dgv_CellEnter(Nothing, Nothing)
        Catch ex As Exception
            If InStr(ex.Message, "FK_") > 0 Then
                MessageBox.Show("No se puede eliminar, tiene datos que dependen de él", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If

        End Try
    End Sub

    Private Sub AnularDet()
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try
            Dim strEstado As String = dgvDet.CurrentRow.Cells("Estado").Value.ToString
            If strEstado = "X" Then
                strEstado = "A"
            ElseIf strEstado = "A" Then
                strEstado = "X"
            End If

            If MessageBox.Show("¿Está Ud. seguro de " & If(strEstado = "X", "anular", "activar") & " el Detalle de Servicio " & dgvDet.Item("DescripcionDet", dgvDet.CurrentCell.RowIndex).Value.ToString & "?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT


            objBT.Anular(dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString, strEstado, gstrUser)

            'Buscar()
            'dgv_Click(Nothing, Nothing)
            dgv_CellEnter(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Function bytActualizarRelacionRetro() As Byte
        Try
            Dim objBT As New clsServicioProveedorBT.clsDetalleServicioProveedorBT
            Return objBT.bytActualizarDetServicioRelacionado(dgv.CurrentRow.Cells("IDServicio").Value.ToString, _
                                                   dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString, _
                                                   dgvDet.CurrentRow.Cells("Anio").Value.ToString, _
                                                   dgvDet.CurrentRow.Cells("Tipo").Value.ToString, _
                                                   dgvDet.CurrentRow.Cells("Codigo").Value.ToString, _
                                                   gstrUser)

        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Sub EditarDet()
        Try
            Dim frm As New frmMantDetServiciosProveedorDato
            With frm
                .strIDServicio_Det = dgvDet.Item("IDServicio_Det", dgvDet.CurrentCell.RowIndex).Value.ToString
                .strDescripcion = dgvDet.Item("DescripcionDet", dgvDet.CurrentCell.RowIndex).Value.ToString
                .strIDServicio = dgv.Item("IDServicio", dgv.CurrentCell.RowIndex).Value.ToString
                .strDescServicio = dgv.Item("Descripcion", dgv.CurrentCell.RowIndex).Value.ToString
                .strIDTipoProv = dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString
                .blnCotizacion = blnCotizacion
                .blnServicioVarios = blnServicioVarios
                .blnEsServicioInternacional = If(dgv.Item("IDTipoProv", dgv.CurrentCell.RowIndex).Value.ToString = gstrTipoProveeHoteles And dgv.Item("IDPais", dgv.CurrentCell.RowIndex).Value.ToString <> gstrPeru, True, False)
                .sglTipoCambioPais = dgv.Item("SsTipCam", dgv.CurrentCell.RowIndex).Value.ToString
                .strCoMonedaPais = dgv.Item("CoMonedaPais", dgv.CurrentCell.RowIndex).Value.ToString
                .strCoSimboloMonedaPais = dgv.Item("SimboloMonedaPais", dgv.CurrentCell.RowIndex).Value.ToString

                .FormRefMantServiciosProveedor = Me
                If Not frmRefIngDetCotiDatoMem Is Nothing Or Not frmRefIngDetServicioDato Is Nothing Or _
                   Not frmRefIngCotizacionDato Is Nothing Or Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
                   Not frmRefIngOperacionProgramacion Is Nothing Or Not frmRefIngPresupuestoDatoMem Is Nothing Or _
                   Not frmRefServOperadores Is Nothing Then

                    If Not blnServicioVarios Then .blnBloquearEdit = True
                    .ShowDialog()
                Else
                    .MdiParent = frmMain
                    .Show()


                End If
                'If Not blnCotizacion Then
                '    .MdiParent = frmMain
                '    .Show()
                'Else
                '    .blnBloquearEdit = True
                '    .ShowDialog()
                'End If

            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgvDet_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvDet.CellFormatting
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub

        'If Not frmRefIngDetCotiDatoMem Is Nothing Or Not frmRefIngDetServicioDato Is Nothing Or _
        '        Not frmRefIngCotizacionDato Is Nothing Or Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
        '        Not frmRefIngOperacionProgramacion Is Nothing Or Not frmRefIngPresupuestoDatoMem Is Nothing Then
        '    If e.ColumnIndex = dgvDet.Columns("btnEditDet").Index Then
        '        If blnServicioVarios Then
        '            dgvDet.Item("btnEditDet", e.RowIndex).ToolTipText = "Editar"
        '        Else
        '            dgvDet.Item("btnEditDet", e.RowIndex).ToolTipText = "Ver Servicio"
        '        End If

        '    End If
        'Else
        If e.ColumnIndex = dgvDet.Columns("btnEditDet").Index Then
            'dgvDet("btnEditDet", e.RowIndex).ToolTipText = "Editar"
            If blnServicioVarios Then
                dgvDet.Item("btnEditDet", e.RowIndex).ToolTipText = "Editar"
            Else
                dgvDet.Item("btnEditDet", e.RowIndex).ToolTipText = "Ver Servicio"
            End If
        ElseIf e.ColumnIndex = dgvDet.Columns("btnDelDet").Index Then
            dgvDet("btnDelDet", e.RowIndex).ToolTipText = "Eliminar"
        ElseIf e.ColumnIndex = dgvDet.Columns("btnAnular").Index Then
            If dgvDet.Item("Estado", e.RowIndex).Value = "A" Then
                dgvDet("btnAnular", e.RowIndex).ToolTipText = "Anular"
            ElseIf dgvDet.Item("Estado", e.RowIndex).Value = "X" Then
                dgvDet("btnAnular", e.RowIndex).ToolTipText = "Activar"
            End If
        End If

        If dgvDet.Columns("btnUpdRelacRetro") Is Nothing Then Exit Sub
        If e.ColumnIndex = dgvDet.Columns("btnUpdRelacRetro").Index Then
            dgvDet("btnUpdRelacRetro", e.RowIndex).ToolTipText = "Actualizar relación en base a este detalle de servicio, para el año anterior"
        End If
        'End If
    End Sub

    Private Sub dgvDet_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgvDet.CellPainting
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        If dgvDet.Columns(e.ColumnIndex).Name = "btnEditDet" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgvDet.Rows(e.RowIndex).Cells("btnEditDet"), DataGridViewButtonCell)
            Dim icoAtomico As Image = Nothing
            If Not frmRefIngDetCotiDatoMem Is Nothing Or Not frmRefIngDetServicioDato Is Nothing Or _
               Not frmRefIngCotizacionDato Is Nothing Or Not frmRefIngReservasCtrlxFileDato Is Nothing Or _
               Not frmRefIngOperacionProgramacion Is Nothing Or Not frmRefIngPresupuestoDatoMem Is Nothing Or _
               Not frmRefServOperadores Is Nothing Then
                If blnServicioVarios Then
                    icoAtomico = My.Resources.Resources.editarpng
                Else
                    icoAtomico = My.Resources.Resources.zoom
                End If

            Else
                icoAtomico = My.Resources.Resources.editarpng
            End If
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgvDet.Rows(e.RowIndex).Height = 21
            dgvDet.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If
        If dgvDet.Columns(e.ColumnIndex).Name = "btnDelDet" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgvDet.Rows(e.RowIndex).Cells("btnDelDet"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.eliminarpng
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgvDet.Rows(e.RowIndex).Height = 21
            dgvDet.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgvDet.Columns(e.ColumnIndex).Name = "btnAnular" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgvDet.Rows(e.RowIndex).Cells("btnAnular"), DataGridViewButtonCell)
            Dim icoAtomico As Image = Nothing
            If dgvDet.Item("Estado", e.RowIndex).Value = "A" Then
                icoAtomico = My.Resources.Resources.thumb_down
            ElseIf dgvDet.Item("Estado", e.RowIndex).Value = "X" Then
                icoAtomico = My.Resources.Resources.thumb_up
            End If
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgvDet.Rows(e.RowIndex).Height = 21
            dgvDet.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgvDet.Columns(e.ColumnIndex).Name = "btnUpdRelacRetro" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgvDet.Rows(e.RowIndex).Cells("btnUpdRelacRetro"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.book_previous
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgvDet.Rows(e.RowIndex).Height = 21
            dgvDet.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If
    End Sub

    Private Sub dgvDet_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvDet.DoubleClick
        If Not frmRefIngOperacionProgramacion Is Nothing And strTipoConsProgramacion = "P" Then Exit Sub
        If Not frmRefIncluidos Is Nothing Then Exit Sub
        If dgvDet.CurrentCell Is Nothing Then Exit Sub

        Try
            If frmRefIngDetCotiDatoMem Is Nothing And frmRefIngDetServicioDato Is Nothing And _
                frmRefIngCotizacionDato Is Nothing And frmRefIngReservasCtrlxFileDato Is Nothing And _
                 frmRefIngPresupuestoDatoMem Is Nothing _
                And frmRefIngOperacionProgramacion Is Nothing And frmRefServOperadores Is Nothing And _
                frmRefMantStockEntradasDato Is Nothing And frmRefMantServiciosDias Is Nothing Then
                'If strProveedor = "" Then
                EditarDet()
            Else
                If Not frmRefIngDetCotiDatoMem Is Nothing Then
                    ''->PPMG20160330
                    Dim objCotDetBN As New clsCotizacionBN.clsDetalleCotizacionBN
                    Dim lIntIDServDet As Integer, lDblValorZicasso As Double = 0, lBlnEsZicasso As Boolean = False
                    lIntIDServDet = Integer.Parse(dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString)
                    lBlnEsZicasso = objCotDetBN.blnDetEsZicasso(dgv.CurrentRow.Cells("IDProveedor").Value.ToString, _
                                                       dgv.CurrentRow.Cells("IDServicio").Value.ToString, lIntIDServDet)
                    If lBlnEsZicasso Then
                        If frmRefIngDetCotiDatoMem.frmRef.blnExisteDetZicasso(lIntIDServDet) Then
                            MessageBox.Show("El Servicio " & dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString & " ya existe." _
                                            & vbCrLf & "Este servicio se ingresa una sola vez.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        Else
                            lDblValorZicasso = objCotDetBN.dblDetValorZicasso(frmRefIngDetCotiDatoMem.intIDCab)
                        End If
                    End If
                    ''<-PPMG20160330
                    If dgv.CurrentRow.Cells("IDServicio").Value.ToString = gstrIDServicioCostoTC And _
                        frmRefIngDetCotiDatoMem.frmRef.blnExisteCostosTC() Then

                        MessageBox.Show("El servicio Costos TC ya se existe." & vbCrLf & "Este servicio se ingresa desde la sección Tour Conductor.", gstrTituloMsg, _
                                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If

                    With frmRefIngDetCotiDatoMem
                        .intIDServicio_Det = dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString
                        If dgv.CurrentRow.Cells("IDTipoProv").Value.ToString = gstrTipoProveeHoteles Or dgv.CurrentRow.Cells("IDTipoProv").Value.ToString = gstrTipoProveeRestaurantes Then
                            .strDescServicio_Det = dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString
                        Else
                            .strDescServicio_Det = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                        End If
                        .strDescServicio = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                        .strTipoServicio_Det = dgvDet.CurrentRow.Cells("Tipo").Value.ToString
                        .blnConAlojamiento = dgvDet.CurrentRow.Cells("ConAlojamiento").Value
                        .strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value.ToString
                        .strAnio = dgvDet.CurrentRow.Cells("Anio").Value.ToString
                        .strDescripcionTipoTriple = dgvDet.CurrentRow.Cells("TipoTriple").Value.ToString
                        .cboTipoProv.SelectedValue = dgv.CurrentRow.Cells("IDTipoProv").Value.ToString
                        .strIDTipoOper = dgv.CurrentRow.Cells("IDTipoOper").Value.ToString
                        .cboPais.SelectedValue = dgv.CurrentRow.Cells("IDPais").Value.ToString
                        .cboPais_SelectionChangeCommitted(Nothing, Nothing)
                        .cboCiudad.SelectedValue = dgv.CurrentRow.Cells("IDCiudad").Value.ToString
                        .lblProveedor.Text = dgv.CurrentRow.Cells("RazonSocial").Value.ToString
                        .strIDProveedor = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
                        .strIDTipoServ = dgv.CurrentRow.Cells("IDTipoServ").Value.ToString
                        .strDescTipoServ = dgv.CurrentRow.Cells("DescTipoServ").Value.ToString
                        .lblTipoServicio.Text = dgv.CurrentRow.Cells("DescTipoServ").Value.ToString
                        .strCoMoneda = dgvDet.CurrentRow.Cells("CoMonedaDet").Value.ToString
                        If dgv.CurrentRow.Cells("IDTipoProv").Value = gstrTipoProveeHoteles Or _
                            dgv.CurrentRow.Cells("IDTipoProv").Value = gstrTipoProveeRestaurantes Then
                            .chkDesayuno.Checked = dgvDet.CurrentRow.Cells("DesayunoDet").Value
                            .chkLonche.Checked = dgvDet.CurrentRow.Cells("LoncheDet").Value
                            .chkAlmuerzo.Checked = dgvDet.CurrentRow.Cells("AlmuerzoDet").Value
                            .chkCena.Checked = dgvDet.CurrentRow.Cells("CenaDet").Value
                        Else
                            .chkDesayuno.Checked = dgv.CurrentRow.Cells("Desayuno").Value
                            .chkLonche.Checked = dgv.CurrentRow.Cells("Lonche").Value
                            .chkAlmuerzo.Checked = dgv.CurrentRow.Cells("Almuerzo").Value
                            .chkCena.Checked = dgv.CurrentRow.Cells("Cena").Value
                        End If
                        .chkTransfer.Checked = dgv.CurrentRow.Cells("Transfer").Value
                        .cboTipoTransporte.SelectedValue = dgv.CurrentRow.Cells("TipoTransporte").Value
                        .cboIDUbigeoOri.SelectedValue = dgv.CurrentRow.Cells("IDUbigeoOri").Value.ToString
                        .cboIDUbigeoDes.SelectedValue = dgv.CurrentRow.Cells("IDUbigeoDes").Value.ToString
                        .blnPlanAlimenticio = dgvDet.CurrentRow.Cells("PlanAlimenticio").Value
                        .strIDServicio_V = dgvDet.CurrentRow.Cells("IDServicio_V").Value
                        If Not dgv.CurrentRow.Cells("HoraPredeterminada").Value Is Nothing Then
                            .dtpDiaHora.Text = FormatDateTime(CDate(dgv.CurrentRow.Cells("HoraPredeterminada").Value.ToString), DateFormat.ShortTime)
                        End If

                        If blnReemplazandoServicio Then
                            Dim intIDDetAntiguo As String = frmRefIngDetCotiDatoMem.frmRef.dgvDet.CurrentRow.Cells("IDDetAntiguo").Value.ToString
                            Dim strLocalIDDet As String = frmRefIngDetCotiDatoMem.frmRef.dgvDet.CurrentRow.Cells("IDDet").Value.ToString
                            .intIDDetAntiguo = CInt(intIDDetAntiguo)
                            If intIDDetAntiguo <> strLocalIDDet And IsNumeric(strLocalIDDet) Then
                                Dim objResBN As New clsReservaBN
                                If Not objBN.blnExisteEnReservasPeroNoEnCoti(.intIDDetAntiguo) Then
                                    .intIDDetAntiguo = CInt(strLocalIDDet)
                                End If
                            End If

                        End If

                        If Not blnReemplazandoServicio And Not blnReemplazaServiciosHotel And Not blnServicioVarios Then
                            .blnSelecServxServ = False
                            .dtpDia.Text = dtpFechaInsertarCoti.Text
                            .CalcularMontosNuevoServicio(True)
                            ''->PPMG20160330
                            If lBlnEsZicasso Then
                                frmRefIngDetCotiDatoMem.txtCostoReal.Text = Format(lDblValorZicasso, "###,##0.00") ''lDblValorZicasso.ToString
                                frmRefIngDetCotiDatoMem.lblTotal.Text = Format(lDblValorZicasso, "###,##0.00") ''lDblValorZicasso.ToString
                                If frmRefIngDetCotiDatoMem.frmRef.dtpFechaOut.Text > .dtpDia.Text Then
                                    .dtpDia.Text = frmRefIngDetCotiDatoMem.frmRef.dtpFechaOut.Text
                                End If
                            End If
                            ''<-PPMG20160330
                            .btnAceptar_Click(Nothing, Nothing)
                            If .strMensajeValidacNuevoServ = "" Then
                                For Each ItemDgv As DataGridViewRow In .frmRef.dgvDet.Rows
                                    If Not ItemDgv.Cells("IDDet").Value Is Nothing Then
                                        If Not IsNumeric(ItemDgv.Cells("IDDet").Value.ToString) Then
                                            .frmRef.AgregarEditarValorStructDetCoti(ItemDgv.Index, "N")
                                        End If
                                    End If
                                Next

                                If .frmRef.chkIncluirTC.Checked Then
                                    If dtpFechaInsertarCoti.Value <= .frmRef.dtpFeTCHasta.Value And dtpFechaInsertarCoti.Value >= .frmRef.dtpFeTCDesde.Value Then
                                        If .chkLonche.Checked Or .chkAlmuerzo.Checked Or .chkCena.Checked Then
                                            Dim strDiaSelect As String = dtpFechaInsertarCoti.Value.ToShortDateString
                                            For Each ItemDgv As DataGridViewRow In .frmRef.dgvDet.Rows
                                                If Not ItemDgv.Cells("IDDet").Value Is Nothing Then
                                                    If CBool(ItemDgv.Cells("FlServicioTC").Value) Then
                                                        .frmRef.AgregarEditarDetallesPorItemTourConductor(True, True, strDiaSelect, ItemDgv.Index)
                                                    End If
                                                End If
                                            Next
                                        End If
                                    End If
                                End If

                                MessageBox.Show("Se agregó el servicio " & .txtServicio.Text.Trim & " el día " & .dtpDia.Value.ToShortDateString & " exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                            Else
                                MessageBox.Show(.strMensajeValidacNuevoServ, gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                .lblProveedor.Text = String.Empty
                                .strMensajeValidacNuevoServ = ""
                            End If
                        Else
                            Dim blnTodoValido As Boolean = .blnValidarIngresos()
                            If .strMensajeValidacNuevoServ <> String.Empty Then
                                MessageBox.Show(.strMensajeValidacNuevoServ, gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                .strMensajeValidacNuevoServ = String.Empty
                                .lblProveedor.Text = String.Empty
                                Exit Sub
                            End If
                            Me.Close()
                        End If
                    End With

                    'Me.Close()
                ElseIf Not frmRefIngDetServicioDato Is Nothing Then

                    With frmRefIngDetServicioDato
                        .strIDServicioNue = dgv.CurrentRow.Cells("IDServicio").Value.ToString
                        .strTipoNue = dgvDet.CurrentRow.Cells("Tipo").Value.ToString
                        .strAnioNue = dgvDet.CurrentRow.Cells("Anio").Value.ToString
                        If frmRefIngDetServicioDato.intIDServicio_Det_V = dgvDet.CurrentRow.Cells("IDServicio_Det").Value Then
                            .blnIDServicio_DetRelIgual = True
                        End If
                    End With
                    Me.Close()
                ElseIf Not frmRefIngCotizacionDato Is Nothing Then 'And blnServicioVarios = False Then
                    If Not blnSeleccionHotelTC Then
                        With frmRefIngCotizacionDato.dgvCat
                            If .CurrentRow.Cells(.CurrentCell.ColumnIndex).Value Is Nothing Then
                                .CurrentRow.Cells(.CurrentCell.ColumnIndex - 4).Value = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
                            End If
                            .CurrentCell.Value = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                            .CurrentRow.Cells(.CurrentCell.ColumnIndex - 5).Value = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
                            .CurrentRow.Cells(.CurrentCell.ColumnIndex - 3).Value = dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString
                            If dgv.CurrentRow.Cells("IDTipoProv").Value.ToString = gstrTipoProveeHoteles Then
                                .CurrentRow.Cells(.CurrentCell.ColumnIndex - 2).Value = dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString
                            Else
                                .CurrentRow.Cells(.CurrentCell.ColumnIndex - 2).Value = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                            End If
                            .CurrentRow.Cells(.CurrentCell.ColumnIndex - 1).Value = dgv.CurrentRow.Cells("IDServicio").Value.ToString
                        End With
                    Else
                        Dim objBL As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                        Using dr As SqlClient.SqlDataReader = objBL.ConsultarPk(dgvDet.CurrentRow.Cells("IDServicio_Det").Value)
                            dr.Read()
                            If dr.HasRows Then
                                With frmRefIngCotizacionDato
                                    Dim dblMontoCostoReal As Double = If(IsDBNull(dr("Monto_sgl")) = True, 0, CDbl(dr("Monto_sgl")))
                                    If dgv.CurrentRow.Cells("IDPais").Value = gstrPeru Then
                                        dblMontoCostoReal = dblMontoCostoReal + ((gsglIGV / 100) * dblMontoCostoReal)
                                    End If

                                    'Dim datDiffFechas As Int16 = DateDiff(DateInterval.Day, .dtpFeTCDesde.Value, .dtpFeTCHasta.Value)

                                    .lblMontoRealHotel.Text = Format(dblMontoCostoReal, "###,##0.00")
                                    If .chkIncluirNocheAntesDesp.Checked Then dblMontoCostoReal = dblMontoCostoReal * 2
                                    .lblMontoTotalxNoches.Text = Format(dblMontoCostoReal, "###,##0.00")
                                End With
                            End If
                            dr.Close()
                        End Using
                    End If
                    Me.Close()

                ElseIf Not frmRefIngReservasCtrlxFileDato Is Nothing Then
                    With frmRefIngReservasCtrlxFileDato
                        'validar proveedor que no se repita en reservas y Operaciones
                        Dim strProveedor As String = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
                        'Dim stridtipoprov As String = dgv.CurrentRow.Cells("IDTipoProv").Value.ToString
                        Dim strAreaSelect As String = String.Empty
                        If strModulo = "R" Then
                            strAreaSelect = "Reservas"
                        ElseIf strModulo = "O" Then
                            strAreaSelect = "Operaciones"
                        End If

                        Dim blnEncontroProv As Boolean = False
                        If (.blnNuevoProveedor And .blnNuevoServicioxProveedor) Or strModulo = "R" Then
                            'Busqueda en Hoteles
                            For Each Item As DataGridViewRow In .DgvHotelesyEspeciales.Rows
                                If Item.Cells("IDProveedorHotel").Value.ToString = strProveedor Then
                                    blnEncontroProv = True
                                    Exit For
                                End If
                            Next
                            'Busqueda en Servicios
                            If Not blnEncontroProv Then
                                For Each item2 As DataGridViewRow In .DgvNoHoteles.Rows
                                    If item2.Cells("IDProveedorNoHotel").Value.ToString = strProveedor Then
                                        blnEncontroProv = True
                                        Exit For
                                    End If
                                Next
                            End If
                        End If

                        If blnEncontroProv Then
                            MessageBox.Show("Ya existe el proveedor seleccionado en " & strAreaSelect, gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Exit Sub
                        End If


                        Dim obj As New clsCotizacionBN
                        'If strIDServicioGuide = "" Then
                        Dim dcMontos As Dictionary(Of String, Double) = obj.CalculoCotizacion( _
                                                    dgv.CurrentRow.Cells("IDServicio").Value.ToString, _
                                                    dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString, strAnio, _
                                                    dgvDet.CurrentRow.Cells("Tipo").Value.ToString, _
                                                    dgv.CurrentRow.Cells("IDTipoProv").Value, intNroPax, _
                                                    intNroLiberados, _
                                                    strTipoLib, _
                                                    0, 0, -1, -1, strTipoPaxLoc, "1.0", .dttDetCotiServ, gsglIGV)

                        'Dim strDescServicio As String = If(dgvDet.CurrentRow.Cells("VerDetaTipo").Value = True, dgvDet.CurrentRow.Cells("DetaTipo").Value.ToString, dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString)
                        Dim strDescServicio As String = dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString

                        Dim strDescServicioAnt As String = frmRefIngReservasCtrlxFileDato.DgvDetalle.Rows(0).Cells("DescServicioDet").Value.ToString
                        Dim strDescServicioAntPuro As String = frmRefIngReservasCtrlxFileDato.DgvDetalle.Rows(0).Cells("DescServicioDetBup").Value.ToString
                        'strDescServicio = Replace(strDescServicioAnt, strDescServicioAntPuro, strDescServicio)
                        If strModulo = "R" Then
                            If dgv.CurrentRow.Cells("IDTipoProv").Value.ToString() = gstrTipoProveeOperadores And _
                            CBool(dgvDet.CurrentRow.Cells("ConAlojamiento").Value) Then
                                strDescServicio += " " & dgvDet.CurrentRow.Cells("DetaTipo").Value.ToString
                            End If
                        End If

                        .GrabarArreglosNuevoProvee(strIDProveedorAntReservas, dgv.CurrentRow.Cells("IDProveedor").Value.ToString, _
                                                   dgv.CurrentRow.Cells("RazonSocial").Value.ToString, dgv.CurrentRow.Cells("DescProvInternac").Value.ToString, _
                                                   dgv.CurrentRow.Cells("IDTipoProv").Value.ToString, dgv.CurrentRow.Cells("CorreoReservas").Value.ToString, _
                                                   dgv.CurrentRow.Cells("CorreoReservas2").Value.ToString, strIDPais, dgv.CurrentRow.Cells("IDTipoServ").Value.ToString, _
                                                   strIDCiudad, dgv.CurrentRow.Cells("DescCiudad").Value.ToString, dgv.CurrentRow.Cells("IDServicio").Value.ToString, _
                                                   strDescServicio, dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString, dgvDet.CurrentRow.Cells("Tipo").Value.ToString, _
                                                   dgv.CurrentRow.Cells("IDFormaPago").Value.ToString, dgvDet.CurrentRow.Cells("ConAlojamiento").Value, dgv.CurrentRow.Cells("IDTipoOper").Value.ToString, _
                                                   If(dgvDet.CurrentRow.Cells("idTipoOC").Value Is Nothing, "NAP", dgvDet.CurrentRow.Cells("idTipoOC").Value.ToString), dcMontos, intIDReservaAnt, .blnNuevoProveedor, .blnNuevoServicioxProveedor, _
                                                   dgv.CurrentRow.Cells("IDCiudad").Value.ToString, dgvDet.CurrentRow.Cells("DesayunoDet").Value, dgvDet.CurrentRow.Cells("LoncheDet").Value, _
                                                   dgvDet.CurrentRow.Cells("AlmuerzoDet").Value, dgvDet.CurrentRow.Cells("CenaDet").Value, dgv.CurrentRow.Cells("Transfer").Value, _
                                                   dgvDet.CurrentRow.Cells("Anio").Value.ToString, dgvDet.CurrentRow.Cells("CoMonedaDet").Value, _
                                                   dgvDet.CurrentRow.Cells("PlanAlimenticio").Value, dgvDet.CurrentRow.Cells("ConAlojamiento").Value)
                        .blnAgregoReserva = True
                    End With

                    Me.Close()
                ElseIf Not frmRefIngPresupuestoDatoMem Is Nothing Then
                    Dim strCoPago As String = dgv.CurrentRow.Cells("CoPago").Value.ToString.Trim
                    If strCoPago = String.Empty Then
                        MessageBox.Show("Debe seleccionar un servicio que tenga una forma de pago (EFECTIVO,TICKET O PRECOMPRA)", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        Exit Sub
                    End If
                    With frmRefIngPresupuestoDatoMem
                        .intIDServicio_Det = dgvDet.CurrentRow.Cells("IDServicio_Det").Value
                        .txtServicio.Text = dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString
                        .cboMoneda.SelectedValue = dgvDet.CurrentRow.Cells("CoMonedaDet").Value.ToString
                        .strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value
                        Dim dblMontoPorPers As Double = 0
                        Using oBLServ As clsServicioProveedorBN.clsDetalleServicioProveedorBN = New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                            Using dr As SqlClient.SqlDataReader = oBLServ.ConsultarPk(.intIDServicio_Det)
                                dr.Read()
                                If dr.HasRows Then
                                    If Not IsDBNull(dr("Monto_sgls")) Then
                                        dblMontoPorPers = dr("Monto_sgls")
                                    Else
                                        dblMontoPorPers = If(IsDBNull(dr("Monto_sgl")) = True, 0, dr("Monto_sgl"))
                                    End If
                                End If
                                dr.Close()
                            End Using
                        End Using
                        .txtPreUnit.Text = dblMontoPorPers.ToString("###,##0.00")
                    End With
                    Me.Close()

                    'ElseIf Not frmRefMantStockEntradasDato Is Nothing Then
                    '    With frmRefMantStockEntradasDato
                    '        '.intIDServicioDet = dgvDet.CurrentRow.Cells("IDServicio_Det").Value
                    '        .strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value
                    '        .lblAnio.Text = dgvDet.CurrentRow.Cells("Anio").Value
                    '        .lblServicio.Text = dgvDet.CurrentRow.Cells("DescripcionDet").Value
                    '        .lblVariante.Text = dgvDet.CurrentRow.Cells("Tipo").Value
                    '        .blnCambios = True
                    '    End With
                    '    Me.Close()
                ElseIf Not frmRefIngOperacionProgramacion Is Nothing Then

                    NuevoDetServicioenProgramacion()
                    Me.Close()

                    'ElseIf Not frmRefServOperadores Is Nothing Then
                    '    frmRefServOperadores.lblDescripcionServicio.Text = dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString
                    '    frmRefServOperadores.intIDServicio_Det = dgvDet.CurrentRow.Cells("IDServicio_Det").Value

                    '    Me.Close()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NuevoDetServicioenProgramacion()
        Try
            With frmRefIngOperacionProgramacion.ArrDgvDetServiciosDinam(bytNroDetDgvProgram)
                Dim bytNueFila As Byte = .RowCount

                .Rows.Add()
                Dim blnAfectoIgv As Boolean = CBool(dgvDet.CurrentRow.Cells("AfectoIgv").Value)
                Dim dblTipoCambio As Double = CDbl(dgvDet.CurrentRow.Cells("TC").Value)
                Dim dblNetoProgramado As Double = 0, dblIGVProgramado As Double = 0, dblTotalProgramado As Double = 0
                Dim obj As New clsCotizacionBN
                Dim dcMontos As Dictionary(Of String, Double) = obj.CalculoCotizacion( _
                    dgv.CurrentRow.Cells("IDServicio").Value.ToString, _
                    dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString, _
                    dgvDet.CurrentRow.Cells("Anio").Value.ToString, _
                    dgvDet.CurrentRow.Cells("Tipo").Value.ToString, _
                    dgv.CurrentRow.Cells("IDTipoProv").Value, intNroPax, _
                    intNroLiberados, _
                    strTipoLib, _
                    0, 0, -1, -1, strTipoPaxLoc, "1.0", frmRefIngOperacionProgramacion.dttDetCotiServ, gsglIGV)
                Dim bytNroPaxLib As Byte = frmRefIngOperacionProgramacion.intNroPax '+ frmRefIngOperacionProgramacion.intNroLiberados
                For Each dic As KeyValuePair(Of String, Double) In dcMontos
                    For Each dgvCol As DataGridViewColumn In .Columns
                        If dic.Key = dgvCol.Name Then
                            .Item(dic.Key, bytNueFila).Value = dic.Value
                            Exit For
                        End If
                    Next
                    If dic.Key = "Total" Then
                        .Item("TotalCotizadoDet", bytNueFila).Value = dic.Value.ToString("##,##0.00")
                    End If
                    If dic.Key = "CostoReal" Then
                        If dblTipoCambio <> 0 Then
                            dblNetoProgramado = CDbl(dic.Value * bytNroPaxLib) * dblTipoCambio
                        Else
                            dblNetoProgramado = CDbl(dic.Value * bytNroPaxLib)
                        End If
                    End If
                Next

                .Item("PaxLiberados", bytNueFila).Value = bytNroPaxLib.ToString
                .Item("DescProveedor_Cot", bytNueFila).Value = dgv.CurrentRow.Cells("RazonSocial").Value.ToString
                .Item("DescServicio_Cot", bytNueFila).Value = dgvDet.CurrentRow.Cells("DescripcionDet").Value.ToString
                .Item("IDEstadoSolicitud", bytNueFila).Value = "NV"
                .Item("IDServicio_detalle", bytNueFila).Value = dgv.CurrentRow.Cells("IDServicio").Value.ToString

                .Item("IDServicio_Det", bytNueFila).Value = dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString
                .Item("IDTipoProvDet", bytNueFila).Value = dgv.CurrentRow.Cells("IDTipoProv").Value.ToString
                .Item("IDServicio_Det_V_Cot", bytNueFila).Value = dgvDet.CurrentRow.Cells("IDServicio_Det").Value.ToString

                .Item("TotalProgramadoDet", bytNueFila).Value = "0.00"
                .Item("ObservEdicion", bytNueFila).Value = ""
                .Item("IDMoneda", bytNueFila).Value = gstrTipoMonedaDol
                .Item("TipoCambio", bytNueFila).Value = dgvDet.CurrentRow.Cells("TC").Value.ToString
                If Convert.ToDouble(dgvDet.CurrentRow.Cells("TC").Value.ToString) > 0 Then
                    .Item("IDMoneda", bytNueFila).Value = dgv.CurrentRow.Cells("CoMonedaPais").Value.ToString
                End If


                If blnAfectoIgv Then dblIGVProgramado = dblNetoProgramado * (gsglIGV / 100)
                dblTotalProgramado = dblIGVProgramado + dblNetoProgramado

                .Item("NetoGen", bytNueFila).Value = Format(dblNetoProgramado, "###,###0.00") '"0.00"
                .Item("IgvGen", bytNueFila).Value = Format(dblIGVProgramado, "###,###0.00") '"0.00"
                .Item("TotalGen", bytNueFila).Value = Format(dblTotalProgramado, "###,###0.00") '"0.00"

                .Item("NetoProgram", bytNueFila).Value = Format(dblNetoProgramado, "###,###0.00")
                .Item("IgvProgram", bytNueFila).Value = Format(dblIGVProgramado, "###,###0.00")
                .Item("TotalProgram", bytNueFila).Value = Format(dblTotalProgramado, "###,###0.00")

                .Item("ValorNeto", bytNueFila).Value = Format(dblNetoProgramado, "###,###0.00")
                .Item("ValorIgv", bytNueFila).Value = Format(dblIGVProgramado, "###,###0.00")
                .Item("ValorTotal", bytNueFila).Value = Format(dblTotalProgramado, "###,###0.00")

                If .Item("IDMoneda", bytNueFila).Value = gstrTipoMonedaDol Then
                    .Item("SimboloMoneda", bytNueFila).Value = "US$"
                    .Item("ValorMoneda", bytNueFila).Value = "US$"
                    .Item("IDMonedaGen", bytNueFila).Value = gstrTipoMonedaDol
                Else
                    .Item("SimboloMoneda", bytNueFila).Value = dgv.CurrentRow.Cells("SimboloMonedaPais").Value.ToString
                    .Item("ValorMoneda", bytNueFila).Value = dgv.CurrentRow.Cells("SimboloMonedaPais").Value.ToString
                    .Item("IDMonedaGen", bytNueFila).Value = dgv.CurrentRow.Cells("CoMonedaPais").Value.ToString
                End If

                .Item("Upd", bytNueFila).Value = "1"
                .Item("Activo", bytNueFila).Value = True
                .Item("IngManual", bytNueFila).Value = True
                frmRefIngOperacionProgramacion.blnSeleccServ = True
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub cboAnio_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboAnio.SelectionChangeCommitted
        dgv_CellEnter(Nothing, Nothing)
    End Sub

    Private Sub dgv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.DoubleClick
        If Not frmRefIngPresupuestoDatoMem Is Nothing Then Exit Sub
        'If Not frmRefMantStockEntradasDato Is Nothing Then Exit Sub
        If dgv.CurrentRow Is Nothing Then Exit Sub

        Try
            If frmRefIngOperacionProgramacion Is Nothing Then
                If Not frmRefServOperadores Is Nothing Then
                    frmRefServOperadores.lblDescripcionServicio.Text = dgv.CurrentRow.Cells("Descripcion").Value.ToString
                    frmRefServOperadores.strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value.ToString

                    Me.Close()
                ElseIf Not frmRefIncluidos Is Nothing Then
                    AgregarServicioaIncluido()
                ElseIf Not frmRefMantStockEntradasDato Is Nothing Then
                    With frmRefMantStockEntradasDato
                        '.intIDServicioDet = dgvDet.CurrentRow.Cells("IDServicio_Det").Value
                        .strIDServicio = dgv.CurrentRow.Cells("IDServicio").Value
                        '.lblAnio.Text = dgvDet.CurrentRow.Cells("Anio").Value
                        .lblServicio.Text = dgvDet.CurrentRow.Cells("DescripcionDet").Value
                        '.lblVariante.Text = dgvDet.CurrentRow.Cells("Tipo").Value
                        .blnCambios = True
                    End With
                    Me.Close()
                ElseIf Not frmRefMantServiciosDias Is Nothing Then
                    With frmRefMantServiciosDias
                        '.dgvServDia.CurrentRow.Cells("TxWebHotel").Value
                        If Not (.dgvServDia.CurrentRow.Cells("TxWebHotel").Value Is Nothing And _
                            .dgvServDia.CurrentRow.Cells("TxDireccHotel").Value Is Nothing And _
                            .dgvServDia.CurrentRow.Cells("TxTelfHotel1").Value Is Nothing And _
                            .dgvServDia.CurrentRow.Cells("TxTelfHotel2").Value Is Nothing And _
                            .dgvServDia.CurrentRow.Cells("FeHoraChkInHotel").Value Is Nothing And _
                            .dgvServDia.CurrentRow.Cells("FeHoraChkOutHotel").Value Is Nothing And _
                            .dgvServDia.CurrentRow.Cells("CoCiudadHotel").Value Is Nothing) Then
                            If MessageBox.Show("Se han ingresado datos del hotel anteriormente. ¿Desea Ud. reemplazarlos?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.No Then
                                Exit Sub
                            End If

                        End If
                        .dgvServDia.CurrentRow.Cells("NoHotel").Value = dgv.CurrentRow.Cells("RazonSocial").Value
                        .dgvServDia.CurrentRow.Cells("TxWebHotel").Value = dgv.CurrentRow.Cells("Web").Value
                        .dgvServDia.CurrentRow.Cells("TxDireccHotel").Value = dgv.CurrentRow.Cells("Direccion").Value
                        .dgvServDia.CurrentRow.Cells("TxTelfHotel1").Value = dgv.CurrentRow.Cells("Telefono1").Value
                        .dgvServDia.CurrentRow.Cells("TxTelfHotel2").Value = dgv.CurrentRow.Cells("Telefono2").Value
                        .dgvServDia.CurrentRow.Cells("FeHoraChkInHotel").Value = dgv.CurrentRow.Cells("HoraCheckIn").Value
                        .dgvServDia.CurrentRow.Cells("FeHoraChkOutHotel").Value = dgv.CurrentRow.Cells("HoraCheckOut").Value
                        .dgvServDia.CurrentRow.Cells("CoCiudadHotel").Value = dgv.CurrentRow.Cells("IDCiudad").Value
                    End With
                    Me.Close()
                Else
                    Editar()
                End If

            Else
                If strTipoConsProgramacion = "P" Then 'Cons Proveedor
                    If MessageBox.Show("¿Está Ud. seguro de asignar el proveedor seleccionado?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub
                    frmRefIngOperacionProgramacion.strIDProveedorPrgFiltro = dgv.CurrentRow.Cells("IDProveedor").Value.ToString
                    Me.Close()
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub AgregarServicioaIncluido()
        If dgv.CurrentRow Is Nothing Then Exit Sub
        Try
            Dim strIDServicio As String = dgv.CurrentRow.Cells("IDServicio").Value.ToString
            Dim blnEncontroServicio As Boolean = False
            With frmRefIncluidos
                For Each item As DataGridViewRow In .dgvServIncluidos.Rows
                    If item.Cells("IDServicio").Value.ToString = strIDServicio Then
                        blnEncontroServicio = True
                        Exit For
                    End If
                Next

                If blnEncontroServicio Then
                    MessageBox.Show("El servicio seleccionado ya se encuentra en los servicios incluidos de " & strDescIncluido, gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                Else
                    .dgvServIncluidos.Rows.Add(.intIDIncluido, dgv.CurrentRow.Cells("IDServicio").Value.ToString, _
                                               dgv.CurrentRow.Cells("Descripcion").Value.ToString, _
                                               dgv.CurrentRow.Cells("IDTipoServ").Value.ToString, dgv.CurrentRow.Cells("RazonSocial").Value.ToString)
                    .blnCambios = True
                    .blnAgregoServicio = True
                End If
            End With
            Me.Close()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub dgv_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgv.CellFormatting
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            If e.ColumnIndex = dgv.Columns("btnDetalle").Index Then
                dgv.Item("btnDetalle", e.RowIndex).ToolTipText = "Detalle"
            ElseIf e.ColumnIndex = dgv.Columns("btnCopiar").Index Then
                dgv.Item("btnCopiar", e.RowIndex).ToolTipText = "Copia"
            ElseIf e.ColumnIndex = dgv.Columns("btnCopiarDetalles").Index Then
                dgv.Item("btnCopiarDetalles", e.RowIndex).ToolTipText = "Copiar Detalles"
            ElseIf e.ColumnIndex = dgv.Columns("btnEdit").Index Then
                dgv.Item("btnEdit", e.RowIndex).ToolTipText = "Editar"
            ElseIf e.ColumnIndex = dgv.Columns("btnDel").Index Then
                dgv.Item("btnDel", e.RowIndex).ToolTipText = "Eliminar"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnRepControl_Click(sender As Object, e As EventArgs) Handles btnRepControl.Click
        'If dgv.CurrentRow Is Nothing Then Exit Sub
        'Try
        '    Dim frm As New frmCReportViewer
        '    Dim oContCal As New clsControlCalidadProBN
        '    With frm
        '        .dt = oContCal.ConsultarRpt(String.Empty, dgv.CurrentRow.Cells("IDProveedor").Value.ToString, #1/1/1900#, #1/1/1900#, 0, 0, 0)
        '        .strRepNom = "ControlCalidad"

        '        Dim strGroup As String = "P"
        '        .strPar1 = "P"
        '        .strPar2 = True
        '        .strPar4 = False
        '        .WindowState = FormWindowState.Maximized
        '        .ShowDialog()
        '    End With
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub colorSlider5_KeyDown(sender As Object, e As KeyEventArgs) Handles colorSlider5.KeyDown
        Try
            Select Case e.KeyCode
                Case Keys.Escape
                    pSalir(Nothing, Nothing)
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub colorSlider5_Click(sender As Object, e As EventArgs) Handles colorSlider5.Click
        Try
            MostrarDatosFeedBackxProveedor()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub btnVerPolitica_Click(sender As Object, e As EventArgs) Handles btnVerPolitica.Click
        If dgv.CurrentRow Is Nothing Then Exit Sub
        Try
            Dim sRutaFilePDF As String
            Dim sRutaFilePDFAlter As String
            sRutaFilePDF = dgv.CurrentRow.Cells("DocPdfRutaPdf").Value.ToString
            If sRutaFilePDF <> "" Then
                If System.IO.File.Exists(sRutaFilePDF) Then
                    Process.Start(sRutaFilePDF)
                Else
                    sRutaFilePDFAlter = gstrRutaPDF
                    If (sRutaFilePDFAlter <> "") Then
                        If (sRutaFilePDFAlter.Substring(sRutaFilePDFAlter.Length - 1) <> "\") Then
                            sRutaFilePDFAlter += "\"
                        End If
                        Dim ioFl As New IO.FileInfo(sRutaFilePDF)
                        sRutaFilePDFAlter += ioFl.Name
                        If System.IO.File.Exists(sRutaFilePDFAlter) Then
                            Process.Start(sRutaFilePDFAlter)
                        End If
                    End If
                    MessageBox.Show("El archivo seleccionado ha sido eliminado o removido de su sitio recientemente." & vbCr & _
                                    "( " & sRutaFilePDF & " )", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            Else
                MessageBox.Show("El Proveedor no tiene Política de Reserva.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    'Jorge: Se agrego este evento para buscar con la tecla Enter los servicios y proveedores
    Private Sub txtDescServicio_KeyDown(sender As Object, e As KeyEventArgs) Handles txtDescServicio.KeyDown, txtRazonSocial.KeyDown
        Select Case e.KeyCode

            Case Keys.Enter
                btnBuscar_Click(Nothing, Nothing)

        End Select
    End Sub
End Class
