﻿Imports ComSEToursBL2

Imports ComSEToursBE2
Imports System.Reflection
Public Class frmIngCotizacion
    Dim blnNuevoEnabled As Boolean = False
    Dim blnGrabarEnabled As Boolean = False
    Dim blnDeshacerEnabled As Boolean = False
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean = True
    Dim blnImprimirEnabled As Boolean = False
    Dim blnConsultarEnabledBD As Boolean = False
    Dim blnConsultarAsignacion As Boolean = False
    Dim blnEditarAsignacion As Boolean = False
    Dim blnEliminarReservasuOperacionesxFile As Boolean = False


    Dim objBN As New clsCotizacionBN

    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False
    'Public blnReserva As Boolean = False
    Public chrModulo As String = "" 'C (Cotizaciones), R(Reservas), O(Operaciones),...
    Public blnOpeProgram As Boolean = False
    'Public frmRefTapas As frmRepTapaFile
    'Public frmRefAntic As frmIngAnticiposDato
    'Public frmRefBiblias As frmRepBibliaFile
    'Public frmRefVoucherMaster As frmRepVoucherMaster
    'Public frmRefDebitMemo As frmIngDebitMemoDato
    'Public frmRefGenPreLiq As frmGenPreLiquidacion
    'Public frmRefTicketDato As frmIngTicketDato
    'Public frmRefDocumentosDato As frmDocumentosDato
    'Public frmRefIngAdministracionPresupuestoDato As frmIngAdministracionPresupuestoDato
    'Public frmRefControlCalidadDato As frmControlCalidadDato
    'Public frmRefRepPaxConIDentNoConfirmada As frmRepPaxConDocIdentNoConfirmado
    'Public frmRefRepNoShow As frmRepNoShow
    'Public frmRefRepControlDeCalidad As frmRepControlDeCalidad
    'Public frmRefIngVentasAdicionales As frmIngVentasAdicionalesDato
    'Public frmRefRepPresConsolidado_Cusco As frmRepPresConsolidado_Cusco
    'Public frmRefDocumentosProveedorDato_FondoFijo As frmDocumentosProveedorDato_FondoFijo
    Public frmRefDocumentosProveedorDato As frmDocumentosProveedorDato
    'Public frmRefLiquidacionFile As frmRepLiquidacionFile
    Dim dtUsuarios_Combo As DataTable
    'Dim blnAccesoCotizacionessinPedido As Boolean = False
    'Structure stEjecutivos
    '    Dim IDUsuarioAsig As String
    '    Dim Usuario As String
    '    Dim IDUsuarioAsig_Cusco As String
    '    Dim UsuarioOperac_Cusco As String

    'End Structure
    'Dim objLstEjecutivos_Sel As New List(Of stEjecutivos)

    Public objLstCab As List(Of String)

    Private Sub btnBuscar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscar.Click, _
        txtCliente.TextChanged, txtTitulo.TextChanged, txtCotizacion.TextChanged, _
        cboUsuario.SelectionChangeCommitted, cboEstado.SelectionChangeCommitted, chkFilesAntiguos.CheckedChanged, _
        txtIDFile.TextChanged, chkUsuarioNoAsignado.CheckedChanged, txtNombrePax.TextChanged, chkRangoFechaAsigReserva.CheckedChanged, _
        chkRangoFechaInicio.CheckedChanged, dtpFechaAsigReservFin.ValueChanged, dtpFechaAsigReservIni.ValueChanged, dtpFechaInicioFin.ValueChanged, dtpFechaInicioIni.ValueChanged, _
        chkRangFecAsigOperaciones.CheckedChanged, dtpRango1FecAsigOper.ValueChanged, dtpRango2FecAsigOper.ValueChanged, chkFilesArchivados.CheckedChanged
        Try

            'Dim obj As New clsAdministracionBTTest
            'Dim str As String = obj.strCadena


            'If Not blnReserva Then
            '    Buscar()
            'Else
            '    If chkUsuarioNoAsignado.Checked Then
            '        cboUsuario.SelectedValue = ""
            '    End If

            '    BuscarReserva()
            'End If
            ErrPrv2.SetError(cboUsuario, "")
            ErrPrv2.SetError(txtTitulo, "")
            ErrPrv2.SetError(txtCotizacion, "")
            ErrPrv2.SetError(txtNombrePax, "")
            ErrPrv2.SetError(txtIDFile, "")
            If sender Is Nothing Then Exit Sub
            If sender.name = "txtTitulo" Then
                If txtTitulo.Text.Length < 6 Then
                    ErrPrv2.SetError(txtTitulo, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
                    Exit Sub
                End If
            End If
            If sender.name = "txtCotizacion" Then
                If txtCotizacion.Text.Length < 6 Then
                    ErrPrv2.SetError(txtCotizacion, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
                    Exit Sub
                End If
            End If
            If sender.name = "txtNombrePax" Then
                If txtNombrePax.Text.Length < 3 Then
                    ErrPrv2.SetError(txtNombrePax, "Debe ingresar mínimo 3 caracteres para ejecutar consulta")
                    Exit Sub
                End If
            End If
            If sender.name = "txtIDFile" Then
                If txtIDFile.Text.ToString.Trim.Length < 6 Then
                    ErrPrv2.SetError(txtIDFile, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
                    Exit Sub
                End If
            End If
            If sender.name = "txtCliente" Then
                If txtCliente.Text.Length < 6 Then
                    ErrPrv2.SetError(txtCliente, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
                    Exit Sub
                End If
            End If

            If sender.name = "chkRangoFechaAsigReserva" Or sender.name = "chkRangoFechaInicio" Or sender.name = "dtpFechaAsigReservFin" Or _
               sender.name = "dtpFechaAsigReservIni" Or sender.name = "dtpFechaInicioFin" Or sender.name = "dtpFechaInicioIni" Or _
               sender.name = "chkRangFecAsigOperaciones" Or sender.name = "dtpRango1FecAsigOper" Or sender.name = "dtpRango2FecAsigOper" Then
                If Not blnValidarIngresos() Then
                    Exit Sub
                End If
                ''Else
                ''    If sender.name = "btnBuscar" Then
                ''        If txtIDFile.Text.ToString.Trim.Length < 6 Then
                ''            ErrPrv2.SetError(txtIDFile, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
                ''            Exit Sub
                ''        End If
                ''    End If
            End If
            Select Case chrModulo
                Case "C"
                    'PruebaAmadeusTemp() ''''

                    'Dim obj As New clsAsignacionBT
                    'obj.InsertarBoletasAnticipoxAsignacionReproceso("01/05/2014", "31/05/2014", "0001")

                    'PruebaGeneraTextoTemp()

                    Buscar()
                Case "A"
                    Buscar()

                Case "R"
                    'If chkUsuarioNoAsignado.Checked Then
                    '    cboUsuario.SelectedValue = ""
                    'End If

                    BuscarReserva()

                Case "O"
                    'If chkUsuarioNoAsignado.Checked Then
                    '    cboUsuario.SelectedValue = ""
                    'End If
                    BuscarOperaciones()
                Case "T", "V", "NS", "PPTO", "FF", "DP", "LF"
                    BuscarTapa()
                Case "B"
                    Buscar()
                Case "D", "TI"
                    BuscarDebitMemo()
                Case "P"
                    BuscarPreLiquidacion()
                Case "DO"
                    Buscar()
                Case "PS"
                    Buscar()
                Case "CC"
                    Buscar()
                Case "RPCDIC"
                    Buscar()
                Case "RCC"
                    Buscar()
                Case "VA"
                    Buscar()
            End Select


            dgv_Click(Nothing, Nothing)

            ''Dim obj As Object
            ''Dim dr As SqlClient.SqlDataReader = obj.ConsultarPk("000035")
            ''obj = CType(CreateObject("ComSEToursBL.clsClienteBN", "192.168.30.50"), Object)

            ColorearFilaxEstado()


        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub
    'Private Sub PruebaGeneraTextoTemp()
    '    'Dim cd As CommonDialog
    '    Dim datFecha As Date = "01/05/2014"
    '    fbd.ShowDialog()
    '    Dim strRuta As String = fbd.SelectedPath.ToString & If(fbd.SelectedPath.ToString.Substring(fbd.SelectedPath.ToString.Length - 1, 1) = "\", "", "\") & _
    '                        "PDBPax_" & strFechaTextoAAAAMMDD(datFecha) & ".txt"

    '    Dim objPaxBN As New clsCotizacionBN.clsCotizacionPaxBN

    '    Dim ListaColFechas As New List(Of String)
    '    With ListaColFechas
    '        .Add("FecIngresoPais")
    '        .Add("FechaIn")
    '        .Add("FechaOut")
    '        '.Add("FeFacturac")
    '    End With

    '    Dim ListaColNoTomar As New List(Of String)
    '    With ListaColNoTomar
    '        .Add("IDFile")
    '        .Add("FeFacturac")
    '        .Add("SsTotalDocumUSD")
    '        .Add("DescFormaPago")
    '        .Add("DescBanco")
    '        .Add("CoOperacionBan")
    '        .Add("Mes")
    '        .Add("Anio")
    '    End With

    '    Dim ListaColumnasNumero As New List(Of stColumnaNumeroFormato)
    '    Dim ItemColNumero As New stColumnaNumeroFormato With {.strColumna = "TotalxPax", _
    '                                                          .strFormato = "0.00"}
    '    ListaColumnasNumero.Add(ItemColNumero)
    '    GenerarArchivoTexto(objPaxBN.ConsultarRptPDBPaxContabilidad(datFecha), _
    '                        "|", strRuta, _
    '                        ListaColFechas, ListaColNoTomar, ListaColumnasNumero)

    '    Process.Start(strRuta)
    'End Sub
    Private Sub PruebaAmadeusTemp()
        Try
            Dim oBN As clsTicketBN = New clsTicketBN
            Dim dtCampos As DataTable = oBN.InicioFinCampoProcesoConsultar("AMA")
            'Dim strRutaArchivo As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\Amadeus\archivos\AIR00033.Txt"
            'Dim strRutaArchivo As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\Amadeus\archivos\AIR00034.Txt"
            'Dim strRutaArchivo As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\Amadeus\archivos\AIR00035.Txt"
            'Dim strRutaArchivo As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\Amadeus\archivos\AIR00030.Txt"
            'Dim strRutaArchivo As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\Amadeus\ArchivosHairo\robert watt.txt"
            Dim strRutaArchivo As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\Amadeus\ArchivosHairo\constance theresa.txt"
            'Dim strRutaArchivo As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\Amadeus\ArchivosHairo\HOCHHAUSER ERICH.txt"

            Dim strNoArchivo As String = Mid(strRutaArchivo, InStrRev(strRutaArchivo, "\") + 1)

            'Dim strValor As String = strValorCampo("CoTicket", strRutaArchivo, dtCampos)
            'strValor = strValorCampo("CoIATA", strRutaArchivo, dtCampos)

            Dim dtTicket As DataTable = dtFiltrarDataTable(dtCampos, "FlMultiLineas=0")

            Dim objBETicket As New clsTicketAMADEUSBE With {.UserMod = gstrUser, .Accion = "N", _
                                                            .NoArchivo = strNoArchivo, .IDCab = 0, _
                                                            .IDCliente = "", .CoProvOrigen = "IATA"}
            Dim objBETicket2 As Object = objBETicket
            For Each drCampo As DataRow In dtTicket.Rows
                Dim strCampo As String = drCampo("NoCampo")
                'If strCampo = "NoPax" Then
                '    strCampo = "NoPax"
                'End If
                'If strCampo = "TxTarifaAerea" Then
                '    strCampo = "TxTarifaAerea"
                'End If
                'If strCampo = "TxTasaXB_TI" Then
                '    strCampo = "TxTasaXB_TI"
                'End If

                Dim strValor As String = strValorCampoArchTexto(strCampo, strRutaArchivo, dtCampos, 1)

                For Each Prop As Object In objBETicket.GetType.GetProperties
                    If Prop.name.ToString.ToUpper = strCampo.ToUpper Then
                        Dim newValueProp As PropertyInfo = objBETicket2.GetType.GetProperty(Prop.name.ToString)
                        newValueProp.SetValue(objBETicket2, strValor, Nothing)

                        Exit For
                    End If
                Next
            Next

            'INICIO ITINERARIO'''''
            Dim dtItiner As DataTable = dtFiltrarDataTable(dtCampos, "NoTabla='TICKET_AMADEUS_ITINERARIO' And FlMultiLineas=1")
            Dim bytCantLineas As Byte = bytCantLineasArchTexto(dtItiner(1)("NoCampo"), strRutaArchivo, dtItiner(1)("TxClave"))

            Dim ListaTicketItiner As New List(Of clsTicketAMADEUS_ITINERARIOBE)

            For bytContCantLineas As Byte = 1 To bytCantLineas
                Dim objBETicketItiner As New clsTicketAMADEUS_ITINERARIOBE With {.UserMod = gstrUser, .Accion = "N"}
                Dim objBETicketItiner2 As Object = objBETicketItiner
                For Each drCampo As DataRow In dtItiner.Rows
                    Dim strCampo As String = drCampo("NoCampo")
                    Dim strValor As String = strValorCampoArchTexto(strCampo, strRutaArchivo, dtCampos, bytContCantLineas)

                    For Each Prop As Object In objBETicketItiner.GetType.GetProperties
                        If Prop.name.ToString.ToUpper = strCampo.ToUpper Then
                            Dim newValueProp As PropertyInfo = objBETicketItiner2.GetType.GetProperty(Prop.name.ToString)
                            newValueProp.SetValue(objBETicketItiner2, strValor, Nothing)
                            Exit For
                        End If
                    Next
                Next
                objBETicketItiner2.NuSegmento = bytContCantLineas
                objBETicketItiner2.TxFechaPar = objBETicketItiner2.TxFechaPar & " " & objBETicketItiner2.TxHoraPar

                ListaTicketItiner.Add(objBETicketItiner2)
            Next

            objBETicket2.ListaItinerarioTicket = ListaTicketItiner

            'FIN ITINERARIO'''''

            'INICIO TAX
            Dim dtTax As DataTable = dtFiltrarDataTable(dtCampos, "NoTabla='TICKET_AMADEUS_TAX' And FlMultiLineas=1")
            bytCantLineas = bytCantLineasArchTexto(dtTax(1)("NoCampo"), strRutaArchivo, dtTax(1)("TxClave"))

            Dim ListaTicketTaxes As New List(Of clsTicketAmadeus_TaxBE)
            Dim bytContInd As Byte = 1
            For bytContCantLineas As Byte = 1 To bytCantLineas
                Dim objBETicketTax As New clsTicketAmadeus_TaxBE With {.UserMod = gstrUser, .Accion = "N"}
                Dim objBETicketTax2 As Object = objBETicketTax

                Dim bytContCampo As Byte = 1
                Dim bytContCampo2 As Byte = 1
                For Each drCampo As DataRow In dtTax.Rows
                    Dim strCampo As String = drCampo("NoCampo")
                    Dim strValor As String = strValorCampoArchTexto(strCampo, strRutaArchivo, _
                                                                    dtCampos, bytContCantLineas)
                    If strValor = "" Then GoTo Sgte

                    For Each Prop As Object In objBETicketTax.GetType.GetProperties
                        If Prop.name.ToString.ToUpper = strCampo.ToUpper Then
                            Dim newValueProp As PropertyInfo = objBETicketTax2.GetType.GetProperty(Prop.name.ToString)
                            newValueProp.SetValue(objBETicketTax2, strValor, Nothing)
                            Exit For
                        End If
                    Next

                    For Each Prop As Object In objBETicketTax.GetType.GetProperties

                        If Prop.name.ToString.ToUpper & bytContCampo.ToString = strCampo.ToUpper Then
                            Dim newValueProp As PropertyInfo = objBETicketTax2.GetType.GetProperty(Prop.name.ToString)
                            newValueProp.SetValue(objBETicketTax2, strValor, Nothing)

                            If bytContCampo2 Mod 2 = 0 Then
                                bytContCampo += 1
                            End If
                            bytContCampo2 += 1

                            Exit For
                        End If
                    Next
                    If bytContCampo2 = 3 Then
                        objBETicketTax2.NuTax = bytContInd
                        ListaTicketTaxes.Add(objBETicketTax2)
                        bytContCampo2 = 1
                        bytContInd += 1

                        objBETicketTax = New clsTicketAmadeus_TaxBE With {.UserMod = gstrUser, .Accion = "N"}
                        objBETicketTax2 = objBETicketTax

                        For Each drCampo2 As DataRow In dtTax.Rows
                            Dim strCampo2 As String = drCampo2("NoCampo")
                            Dim strValor2 As String = strValorCampoArchTexto(strCampo2, strRutaArchivo, _
                                                                            dtCampos, bytContCantLineas)
                            If strValor2 = "" Then GoTo Sgte

                            For Each Prop As Object In objBETicketTax.GetType.GetProperties
                                If Prop.name.ToString.ToUpper = strCampo2.ToUpper Then
                                    Dim newValueProp As PropertyInfo = objBETicketTax2.GetType.GetProperty(Prop.name.ToString)
                                    newValueProp.SetValue(objBETicketTax2, strValor2, Nothing)
                                    Exit For
                                End If
                            Next

                        Next
                    End If
Sgte:
                Next

                'objBETicketTax2.NuTax = bytContCantLineas

                'ListaTicketTaxes.Add(objBETicketTax2)
            Next

            objBETicket2.ListaTicketAmadeus_Tax = ListaTicketTaxes

            'FIN TAX


            Dim objTicketBT As New clsTicketBT
            objTicketBT.Insertar(objBETicket2)

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ColorearFilaxEstado()
        Try
            For Each ItemDgv As DataGridViewRow In dgv.Rows
                If ItemDgv.Cells("Estado").Value.ToString = "X" Or ItemDgv.Cells("Estado").Value.ToString = "E" Then
                    ItemDgv.DefaultCellStyle.ForeColor = gColorSelectCotiAnulada
                    ItemDgv.DefaultCellStyle.SelectionForeColor = gColorSelectCotiAnulada
                Else
                    If chrModulo = "R" Then
                        If ItemDgv.Cells("Estado").Value.ToString = "P" Then
                            ItemDgv.DefaultCellStyle.ForeColor = gColorSelectCotiNoAsignada
                            ItemDgv.DefaultCellStyle.SelectionForeColor = gColorSelectCotiNoAsignada
                        End If
                    ElseIf chrModulo = "O" Then
                        If ItemDgv.Cells("Fecha").Value.ToString = "" Then
                            ItemDgv.DefaultCellStyle.ForeColor = gColorSelectCotiNoAsignada
                            ItemDgv.DefaultCellStyle.SelectionForeColor = gColorSelectCotiNoAsignada
                        End If
                    End If
                End If
            Next
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnValidarIngresos() As Boolean
        Dim blnOk As Boolean = True
        If chrModulo <> "" Then
            If chrModulo = "C" Then
                If cboUsuario.SelectedValue Is Nothing Then
                    ErrPrv2.SetError(cboUsuario, "Debe seleccionar un usuario para la consulta.")
                    Exit Function
                    'Else
                    '    If txtCotizacion.Text.Length < 4 Then
                    '        ErrPrv2.SetError(txtCotizacion, "Debe ingresar mínimo 4 caracteres para ejecutar consulta")
                    '        Exit Function
                    '    End If
                End If
            Else
                If cboUsuario.SelectedValue Is Nothing Then
                    ErrPrv2.SetError(cboUsuario, "Debe seleccionar un usuario para la consulta.")
                    Exit Function
                    'Else
                    '    If txtCotizacion.Text.Length < 4 Then
                    '        ErrPrv2.SetError(txtCotizacion, "Debe ingresar mínimo 4 caracteres para ejecutar consulta")
                    '        Exit Function
                    '    End If
                End If
            End If
        End If

        'If txtTitulo.Text.Length < 6 Then
        '    ErrPrv2.SetError(txtTitulo, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
        '    Exit Function
        'End If
        'If txtNombrePax.Text.Length < 6 Then
        '    ErrPrv2.SetError(txtNombrePax, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
        '    Exit Function
        'End If
        'If txtCliente.Text.Length < 6 Then
        '    ErrPrv2.SetError(txtCliente, "Debe ingresar mínimo 6 caracteres para ejecutar consulta")
        '    Exit Function
        'End If

        If Not blnOk Then
            Return False
        Else
            Return True
        End If
    End Function


    Private Sub frmMantBancos_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) _
Handles Me.KeyDown

        'Select Case e.KeyCode
        '    Case Keys.F2
        '        If frmMain.btnNuevo.Enabled Then pNuevo(Nothing, Nothing)
        '    Case Keys.F3
        '        If frmMain.btnEditar.Enabled Then pEditar(Nothing, Nothing)
        '    Case Keys.F4
        '        If frmMain.btnEliminar.Enabled Then pEliminar(Nothing, Nothing)
        '    Case Keys.F5
        '        btnBuscar_Click(btnBuscar, Nothing)
        '    Case Keys.F9
        '        'If frmMain.btnImprimir.Enabled Then pImprimir(Nothing, Nothing)
        '    Case Keys.Escape
        '        pSalir(Nothing, Nothing)
        'End Select
    End Sub
    Private Sub CargarSeguridad()
        'Try
        '    Dim strNombreForm As String = Me.Name

        '    If chrModulo = "R" Then
        '        strNombreForm = "mnuIngReservasControlporFile"
        '    ElseIf chrModulo = "O" Then
        '        strNombreForm = "mnuIngOperacionesControlporFile"
        '    End If
        '    strNombreForm = Replace(strNombreForm, "frm", "mnu")
        '    strNombreForm = Replace(strNombreForm, "Dato", "")

        '    Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
        '        Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
        '            If dr.HasRows Then
        '                dr.Read()
        '                blnGrabarEnabledBD = dr("Grabar")
        '                blnEditarEnabledBD = dr("Actualizar")
        '                blnEliminarEnabledBD = dr("Eliminar")
        '                blnConsultarEnabledBD = dr("Consultar")
        '            End If
        '            dr.Close()
        '        End Using


        '        Dim strAsigOpcion As String = ""
        '        Dim strEliminarReservasuOperacionesxFile As String = ""
        '        If chrModulo = "R" Then
        '            strAsigOpcion = "AsignacionEjecutivoReservas"
        '            strEliminarReservasuOperacionesxFile = "EliminacionReservasxFile"
        '        ElseIf chrModulo = "O" Then
        '            strAsigOpcion = "AsignacionEjecutivoOperaciones"
        '            strEliminarReservasuOperacionesxFile = "EliminacionOperacionesxFile"
        '        End If

        '        'If Not blnOpeProgram Then
        '        If strAsigOpcion <> "" Then
        '            Using drAsig As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strAsigOpcion)
        '                If drAsig.HasRows Then
        '                    drAsig.Read()
        '                    blnEditarAsignacion = drAsig("Grabar") Or drAsig("Actualizar")
        '                    blnConsultarAsignacion = drAsig("Consultar")
        '                End If
        '                drAsig.Close()
        '            End Using
        '        End If
        '        'End If

        '        If strEliminarReservasuOperacionesxFile <> "" Then
        '            Using drElim As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strEliminarReservasuOperacionesxFile)
        '                If drElim.HasRows Then
        '                    drElim.Read()
        '                    blnEliminarReservasuOperacionesxFile = drElim("Grabar") Or drElim("Actualizar")
        '                End If
        '                drElim.Close()
        '            End Using
        '        End If

        '        mnuArchivarFiles.Enabled = False
        '        Using drArchFiles As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, "AccesoArchivarFiles")
        '            If drArchFiles.HasRows Then
        '                drArchFiles.Read()
        '                mnuArchivarFiles.Enabled = drArchFiles("Grabar") Or drArchFiles("Actualizar")
        '            End If
        '            drArchFiles.Close()
        '        End Using

        '        If chrModulo = "C" Then

        '            Using drCotSinPermiso As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, "AccesoCotizacionessinPedido")
        '                If drCotSinPermiso.HasRows Then
        '                    drCotSinPermiso.Read()
        '                    blnNuevoEnabled = drCotSinPermiso("Grabar") Or drCotSinPermiso("Actualizar")
        '                    If blnNuevoEnabled Then
        '                        mnuCopiarCotizacion.Visible = True
        '                    End If
        '                End If
        '                drCotSinPermiso.Close()
        '            End Using
        '        End If

        '    End Using

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub ActivarDesactivarBotones()

        'If chrModulo = "T" Or chrModulo = "B" Or chrModulo = "V" Or chrModulo = "CC" Or chrModulo = "RPCDIC" Then
        '    blnNuevoEnabled = False
        '    blnImprimirEnabled = True
        'End If


        ''frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'If blnNuevoEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnNuevo.Enabled = blnNuevoEnabled
        '    Else
        '        frmMain.btnNuevo.Enabled = False
        '        blnNuevoEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'End If

        ''frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'If blnGrabarEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnGrabar.Enabled = blnGrabarEnabled
        '    Else
        '        frmMain.btnGrabar.Enabled = False
        '        blnGrabarEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'End If

        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled

        ''frmMain.btnEditar.Enabled = blnEditarEnabled
        'If blnEditarEnabled = True Then
        '    If blnEditarEnabledBD Then
        '        frmMain.btnEditar.Enabled = blnEditarEnabled
        '    Else
        '        frmMain.btnEditar.Enabled = False
        '        blnEditarEnabled = blnEditarEnabledBD
        '    End If
        'Else
        '    frmMain.btnEditar.Enabled = blnEditarEnabled
        'End If

        ''frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''If blnEliminarEnabled = True Then
        ''    If blnEliminarEnabledBD Then
        ''        frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''    Else
        ''        frmMain.btnEliminar.Enabled = False
        ''        blnEliminarEnabled = blnEliminarEnabledBD
        ''    End If
        ''Else
        ''    frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''End If

        ''If chrModulo = "C" Then
        '' If Not dgv.Columns("btnDel") Is Nothing Then dgv.Columns("btnDel").Visible = blnEliminarEnabled
        ''End If

        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        'frmMain.btnSalir.Enabled = blnSalirEnabled

    End Sub


    Private Sub frmMantUsuarios_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        'frmMain.btnSalir.Enabled = blnSalirEnabled
        'AddHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'AddHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'AddHandler frmMain.btnEditar.Click, AddressOf pEditar
        'frmMain.btnEditar.Enabled = blnEditarEnabled

        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        'frmMain.btnGrabar.Enabled = blnGrabarEnabled

        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        ''AddHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        'pCambiarTitulosToolBarLocal(Me)
    End Sub

    Private Sub frmMantUsuarios_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir
        'RemoveHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'RemoveHandler frmMain.btnEliminar.Click, AddressOf pEliminar
        'RemoveHandler frmMain.btnEditar.Click, AddressOf pEditar
        ''RemoveHandler frmMain.btnImprimir.Click, AddressOf pImprimir
        'pCambiarTitulosToolBarLocal(Me, True)
    End Sub

    Public Sub pCambiarTitulosToolBarLocal(ByVal vFrm As Form, Optional ByVal vblnDesactivar As Boolean = False)
        'Try
        '    If vFrm.Tag <> "" Then
        '        Dim strPalabra As String
        '        Dim strForm As String = ""
        '        'If vFrm.Tag = "Origen" Or vFrm.Tag = "Destino" Then
        '        '    strForm = "Cotización"
        '        'Else
        '        '    strForm = vFrm.Tag
        '        'End If

        '        Select Case chrModulo
        '            Case "C"
        '                strForm = "Cotización"
        '            Case "R"
        '                strForm = "Reserva"
        '            Case "O"
        '                strForm = "Operación"
        '        End Select

        '        If vblnDesactivar Then
        '            strForm = ""
        '        End If

        '        If InStr(strForm, " ") > 0 Then
        '            strPalabra = strForm.ToString.Substring(0, InStr(strForm, " "))
        '        Else
        '            strPalabra = strForm.ToString
        '        End If

        '        frmMain.btnNuevo.Text = "Nueva " & strForm & " (F2)"
        '        frmMain.btnEditar.Text = "Editar " & strForm & " (F3)"
        '        frmMain.btnGrabar.Text = "Grabar " & strForm & " (F6)"
        '        frmMain.btnDeshacer.Text = "Deshacer " & strForm & " (ESC)"
        '        frmMain.btnEliminar.Text = "Eliminar " & strForm & " (F4)"
        '        frmMain.btnImprimir.Text = "Imprimir " & strForm
        '        frmMain.btnSalir.Text = "Salir " & strForm
        '    Else
        '        frmMain.btnNuevo.Text = "Nuevo (F2)"
        '        frmMain.btnEditar.Text = "Editar (F3)"
        '        frmMain.btnGrabar.Text = "Grabar (F6)"
        '        frmMain.btnDeshacer.Text = "Deshacer (ESC)"
        '        frmMain.btnEliminar.Text = "Eliminar (F4)"
        '        frmMain.btnImprimir.Text = "Imprimir"
        '        frmMain.btnSalir.Text = "Salir"
        '    End If
        'Catch ex As System.Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try

    End Sub
    Private Sub CargarCombos()
        Try
            'Dim blnGenercia As Boolean = If(gstrNivelUser = gstrNivelGerencia, True, False)
            'Dim objBLUser As New clsUsuarioBN
            'Select Case chrModulo
            '    Case "C"
            '        pCargaCombosBox(cboUsuario, objBLUser.ConsultarCboxArea(gstrAreaVentas, blnGenercia, True), True)
            '    Case "R"
            '        pCargaCombosBox(cboUsuario, objBLUser.ConsultarCboxArea(gstrAreaReservas, blnGenercia, True), True)
            '    Case "O", "P"
            '        pCargaCombosBox(cboUsuario, objBLUser.ConsultarCboxArea(gstrAreaOperaciones, blnGenercia, True), True)
            '    Case Else
            '        pCargaCombosBox(cboUsuario, objBLUser.ConsultarCboxArea(gstrAreaVentas, blnGenercia, True), True)
            'End Select

            Dim strNombreForm As String = ""
            If chrModulo = "C" Or chrModulo = "D" Or chrModulo = "TI" Or chrModulo = "DO" Or chrModulo = "PS" Or chrModulo = "CC" Then
                strNombreForm = "mnuIngCotizacion"
            ElseIf chrModulo = "R" Then
                strNombreForm = "mnuIngReservasControlporFile"
            ElseIf chrModulo = "O" Then
                strNombreForm = "mnuIngOperacionesControlporFile"
            End If

            'Using objBLUser As New clsUsuarioBN
            '    pCargaCombosBox(cboUsuario, objBLUser.ConsultarCboxOpcion(strNombreForm, True), True)
            'End Using
            CargarCombo_Usuario_Activos()

            Dim dttEstados As New DataTable("Estados")
            With dttEstados
                .Columns.Add("Codigo")
                .Columns.Add("Descripcion")

                Dim blnAccesoPendientes As Boolean = True
                Dim strAsigOpcion As String = ""
                If chrModulo = "R" Then
                    strAsigOpcion = "AccesoReservasPendientes"
                ElseIf chrModulo = "O" Then
                    strAsigOpcion = "AccesoOperacionesPendientes"
                End If

                If strAsigOpcion <> "" Then
                    'Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                    '    Using drAsig As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strAsigOpcion)
                    '        If drAsig.HasRows Then
                    '            drAsig.Read()
                    '            blnAccesoPendientes = drAsig("Consultar")
                    '        End If
                    '        drAsig.Close()
                    '    End Using
                    'End Using
                End If

                If blnAccesoPendientes Then .Rows.Add("", "<TODOS>")
                If blnAccesoPendientes Then .Rows.Add("P", "PENDIENTE")
                .Rows.Add("A", "ACEPTADO")
                .Rows.Add("N", "NO ACEPTADO")
                .Rows.Add("X", "ANULADO")
                '.Rows.Add("F", "FINALIZADO")
                .Rows.Add("E", "ELIMINADO")
                .Rows.Add("C", "CERRADO")
            End With
            pCargaCombosBox(cboEstado, dttEstados, True)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    'jorge
    Private Sub CargarCombo_Usuario_Activos()
        Try
            Dim strActivos As String = ""
            If chkActivos.Checked Then
                strActivos = "A"
            End If

            'Dim strNombreForm As String = Me.Name

            Dim strNombreForm As String = ""
            If chrModulo = "C" Or chrModulo = "D" Or chrModulo = "TI" Or chrModulo = "DO" Or chrModulo = "PS" Or chrModulo = "CC" Then
                strNombreForm = "mnuIngCotizacion"
            ElseIf chrModulo = "R" Then
                strNombreForm = "mnuIngReservasControlporFile"
            ElseIf chrModulo = "O" Then
                strNombreForm = "mnuIngOperacionesControlporFile"
            End If

            'strNombreForm = Replace(strNombreForm, "frm", "mnu")
            'strNombreForm = Replace(strNombreForm, "Dato", "")

            Dim objBLUser As New clsUsuarioBN
            Dim dtCombo As DataTable = objBLUser.ConsultarCboxOpcion_Activos(strNombreForm, True, strActivos)
            'dtUsuarios_Combo = dtFiltrarDataTable(dtCombo, "") 'objBLUser.ConsultarCboxOpcion_Activos(strNombreForm, True, strActivos)

            dtUsuarios_Combo = New DataTable
            dtUsuarios_Combo = dtCombo.Clone
            For Each row As DataRow In dtCombo.Rows
                dtUsuarios_Combo.ImportRow(row)
            Next

            pCargaCombosBox(cboUsuario, dtCombo, True)

            'With cboUsuario
            '    .DataSource = dtCombo
            '    .DisplayMember = dtCombo.Columns(1).ToString
            '    .ValueMember = dtCombo.Columns(0).ToString
            '    If .Items.Count > 0 Then

            '        .SelectedIndex = 0

            '    End If
            'End With

        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub frmMantUsuarios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            'dgv.Columns("IdCab").Visible = True
            ' objLstEjecutivos_Sel = New List(Of stEjecutivos)
            cboUsuario.DrawMode = DrawMode.OwnerDrawFixed
            CargarCombos()
            chkActivos.Checked = True

            If frmRefDocumentosProveedorDato Is Nothing Then
                chrModulo = "C"
            End If

            Select Case chrModulo
                Case "C"
                    'dgv.Columns("Usuario").HeaderText = "Ejecutivo Ventas"
                    Me.WindowState = FormWindowState.Maximized
                    chkUsuarioNoAsignado.Visible = False
                    'lblEstadoEtiq.Visible = True
                    'cboEstado.Visible = True
                    dgv.Columns("btnDel").Visible = False
                Case "D"
                    chkUsuarioNoAsignado.Visible = False
                    'lblEstadoEtiq.Visible = True
                    'cboEstado.Enabled = False
                    cboEstado.SelectedValue = "A"

                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "A"
                    chkUsuarioNoAsignado.Visible = False
                    'lblEstadoEtiq.Visible = True
                    'cboEstado.Visible = True

                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False

                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "R"
                    Me.WindowState = FormWindowState.Maximized
                    blnNuevoEnabled = False
                    chkFilesAntiguos.Visible = True
                    'cboEstado.Enabled = False
                    dgv.Columns("Ruta").Visible = False
                    dgv.Columns("Estado").Visible = False
                    'dgv.Columns("UsuarioVen").Visible = True
                    dgv.Columns("btnDel").Visible = False
                    dgv.Columns("DocWordVersion").Visible = False
                    dgv.Columns("FechaAsigReserva").Visible = True
                    dgv.Columns("Fecha").HeaderText = "Fecha Inicio"
                    dgv.Columns("FecEntregaVentas").Visible = True
                    dgv.Columns("btnEntrVentas").Visible = True
                    'Me.Text = "Cotizaciones con Reservas Aceptadas"
                    Me.Text = "Reservas: Control por File"
                    lblEjecutivoEtiq.Text = "Ejecutivo Reservas:"
                    'lblEstadoEtiq.Visible = False
                    'cboEstado.Visible = False
                    chkUsuarioNoAsignado.Visible = True
                    dgv.Columns("btnEliminarReservasuOperaciones").Visible = True

                    dgv.Columns("UsuarioOpe_Cusco").Visible = False
                    'dgv.Columns("UsuarioOpe").HeaderText = "Ejecutivo de Operaciones Lima"

                Case "O"
                    Me.WindowState = FormWindowState.Maximized
                    blnNuevoEnabled = False
                    chkFilesAntiguos.Visible = True
                    chkRangFecAsigOperaciones.Visible = True
                    dtpRango1FecAsigOper.Visible = True
                    dtpRango2FecAsigOper.Visible = True

                    'cboEstado.Enabled = False
                    dgv.Columns("Ruta").Visible = False
                    dgv.Columns("DocWordVersion").Visible = False
                    dgv.Columns("Estado").Visible = False
                    dgv.Columns("FechaAsigReserva").Visible = True
                    dgv.Columns("FechaAsigReserva").HeaderText = "Fecha Asig. Operaciones"
                    dgv.Columns("FecEntregaVentas").Visible = True
                    dgv.Columns("FecEntregaVentas").HeaderText = "Fecha Entrega Kit Pax"
                    dgv.Columns("btnDel").Visible = False
                    dgv.Columns("btnEntrVentas").Visible = True
                    'dgv.Columns("Usuario").HeaderText = "Ejecutivo Operaciones"
                    dgv.Columns("Fecha").HeaderText = "Fecha Inicio"

                    If Not blnOpeProgram Then
                        Me.Text = "Operaciones: Vouchers por File"
                    Else
                        Me.Text = "Programaciones: Control por File"
                        Me.Tag = "Programación"
                    End If

                    dgv.Columns("UsuarioOpe_Cusco").Visible = True
                    dgv.Columns("UsuarioOpe").HeaderText = "Ejecutivo de Operaciones Lima"

                    lblEjecutivoEtiq.Text = "Ejec. Operaciones:"
                    'lblEstadoEtiq.Visible = False
                    'cboEstado.Visible = False
                    chkUsuarioNoAsignado.Visible = True
                    chkUsuarioNoAsignado.Text = "Mostrar sólo operaciones no asignadas"
                    dgv.Columns("btnEliminarReservasuOperaciones").Visible = True
                Case "T"
                    chkUsuarioNoAsignado.Visible = False
                    'lblEstadoEtiq.Visible = True
                    'cboEstado.Visible = True

                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "B", "V"
                    chkUsuarioNoAsignado.Visible = False
                    'lblEstadoEtiq.Visible = True
                    'cboEstado.Visible = True

                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False

                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "P" 'Pre Liquidacion
                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False
                    'cboEstado.Visible = True
                    lblEjecutivoEtiq.Text = "Ejecutivo Operaciones"
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "TI" 'Ticket
                    chkUsuarioNoAsignado.Visible = False
                    'lblEstadoEtiq.Visible = True
                    'cboEstado.Enabled = False
                    cboEstado.SelectedValue = "A"

                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "DO"
                    Me.WindowState = FormWindowState.Maximized
                    chkUsuarioNoAsignado.Visible = False
                    dgv.Columns("btnDel").Visible = False
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "PS"
                    dgv.Columns("btnDel").Visible = False
                    cboEstado.SelectedValue = "A"
                    cboEstado.Enabled = False
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "CC", "VA"
                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False
                    cboEstado.SelectedValue = "A"
                    cboEstado.Enabled = False
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
                Case "RPCDIC", "RCC"
                    dgv.Columns("btnEdit").Visible = False
                    dgv.Columns("btnDel").Visible = False
                    cboEstado.SelectedValue = "A"
                    cboEstado.Enabled = False
                    Me.WindowState = FormWindowState.Normal
                    Me.CenterToScreen()
            End Select

            CargarSeguridad()
            ActivarDesactivarBotones()

            dgv.ColumnHeadersDefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Bold)
            dgv.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgv.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black

            If chrModulo <> "P" Then cboUsuario.SelectedValue = gstrUser

            'If Not blnReserva Then
            '    Buscar()
            'Else
            '    BuscarReserva()
            '    cboEstado.SelectedValue = "A"
            'End If

            Select Case chrModulo
                Case "C" 'Cotizacion
                    'Buscar()
                Case "A"
                    'Buscar()
                Case "R" 'Reserva
                    'BuscarReserva()
                    cboEstado.SelectedValue = "A"
                Case "O" 'Operaciones
                    'BuscarOperaciones()
                    cboEstado.SelectedValue = "A"
                Case "T", "V"
                    'BuscarTapa()
                Case "B" 'Biblia
                    'Buscar()
                Case "D", "TI" 'Debit Memo, Ticket
                    'BuscarDebitMemo()
                Case "P"
                    'BuscarPreLiquidacion()
                    cboEstado.SelectedValue = "A"
                    cboEstado.Enabled = False

            End Select
            dgv_Click(Nothing, Nothing)
            'PintarBotonAsignacion()

            If chrModulo = "C" Then
                'cboUsuario.Enabled = False
                'If (gstrNivelUser = gstrNivelSupervisorVentas Or gstrNivelUser = gstrNivelGerencia) Then
                '    cboUsuario.Enabled = True
                'End If
                BloquearAccesoVentas()
            End If
            PintarBotonAsignacion()
            VisibleBotonEliminarReservasOperacionesxFile()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try
    End Sub
    Private Sub BloquearAccesoVentas()
        Try
            cboUsuario.Enabled = (blnConsultarEnabledBD And blnEditarEnabledBD) Or (blnConsultarEnabledBD And blnGrabarEnabledBD)

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub PintarBotonAsignacion()
        'If chrModulo = "R" And (gstrNivelUser = gstrNivelSupervisorReserva Or gstrNivelUser = gstrNivelGerencia) Then
        '    dgv.Columns("btnAsigRes").Visible = True
        'Else
        '    dgv.Columns("btnAsigRes").Visible = False
        'End If


        dgv.Columns("btnAsigEjec").Visible = (blnEditarAsignacion Or blnConsultarAsignacion)



    End Sub
    Private Sub VisibleBotonEliminarReservasOperacionesxFile()
        dgv.Columns("btnEliminarReservasuOperaciones").Visible = blnEliminarReservasuOperacionesxFile
    End Sub
    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()

    End Sub
    Private Sub frmMantBancos_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        ' frmMain.DesabledBtns()
    End Sub

    Private Sub dgv_CellFormatting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgv.CellFormatting
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            If e.ColumnIndex = dgv.Columns("btnAsigEjec").Index Then
                dgv.Item("btnAsigEjec", e.RowIndex).ToolTipText = "Asignar Ejecutivo"
            ElseIf e.ColumnIndex = dgv.Columns("btnEdit").Index Then
                dgv.Item("btnEdit", e.RowIndex).ToolTipText = "Editar"
            ElseIf e.ColumnIndex = dgv.Columns("btnDel").Index Then
                dgv.Item("btnDel", e.RowIndex).ToolTipText = "Anular"
            ElseIf e.ColumnIndex = dgv.Columns("btnEliminarReservasuOperaciones").Index Then
                If chrModulo = "R" Then
                    dgv.Item("btnEliminarReservasuOperaciones", e.RowIndex).ToolTipText = "Regenerar Info. Reservas"
                ElseIf chrModulo = "O" Then
                    dgv.Item("btnEliminarReservasuOperaciones", e.RowIndex).ToolTipText = "Eliminar Info. Operaciones"
                End If
            ElseIf e.ColumnIndex = dgv.Columns("btnEntrVentas").Index Then
                If chrModulo = "R" Then
                    dgv.Item("btnEntrVentas", e.RowIndex).ToolTipText = "Fecha de Envio a ventas"
                ElseIf chrModulo = "O" Then
                    dgv.Item("btnEntrVentas", e.RowIndex).ToolTipText = "Fecha entrega Kit de Pax"
                End If
            ElseIf e.ColumnIndex = dgv.Columns("btnCorreos").Index Then
                dgv.Item("btnCorreos", e.RowIndex).ToolTipText = "Bandeja de Correos Áreas"
            End If
            ColorearFilaxEstado()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub pEditar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgv.DoubleClick
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try
            Select Case chrModulo
                Case "C"
                    Editar()
                Case "A"
                    EditarAnticipos()
                Case "R"
                    EditarReservas()
                Case "O"
                    If Not blnOpeProgram Then
                        EditarOperaciones()
                    Else
                        EditarProgramacionOperaciones()
                    End If
                Case "T"
                    EditarTapa()
                Case "B"
                    EditarBiblia()
                Case "V"
                    EditarVoucherMaster()
                Case "D"
                    EditarDebitMemo()
                Case "P"
                    SelFilePreLiquidacion()
                Case "TI"
                    EditarTicket()
                Case "DO"
                    EditarDocumento()
                Case "PS"
                    EditarPresupuestoSobre()
                Case "CC"
                    EditarControlCalidad()
                Case "RPCDIC"
                    EditarReportePaxConDocIdentNoConfirmada()
                Case "NS"
                    EditarRptNoShow()
                Case "RCC"
                    EditarReporteControlCalidad()
                Case "VA" 'Ventas Adicionales APT
                    SelFileVtasAdicionales()
                Case "PPTO"
                    EditarPPTOConsCusco()
                Case "FF"
                    EditarFondoFijo()
                Case "DP"
                    EditarDocumProv()
                Case "LF"
                    SelFileRepLiquidacionFile()
            End Select

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub EditarReporteControlCalidad()
        'If frmRefRepControlDeCalidad Is Nothing Then Exit Sub
        'Try
        '    With frmRefRepControlDeCalidad
        '        .intIDCab = dgv.CurrentRow.Cells("IdCab").Value
        '        .lblIDFile.Text = If(dgv.CurrentRow.Cells("IDFile").Value Is Nothing, "", dgv.CurrentRow.Cells("IDFile").Value.ToString)
        '    End With
        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarReportePaxConDocIdentNoConfirmada()
        'If frmRefRepPaxConIDentNoConfirmada Is Nothing Then Exit Sub
        'Try
        '    With frmRefRepPaxConIDentNoConfirmada
        '        .txtFile.Text = If(dgv.CurrentRow.Cells("IDFile").Value Is Nothing, "", dgv.CurrentRow.Cells("IDFile").Value.ToString)
        '    End With
        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub SelFileVtasAdicionales()
        'If frmRefIngVentasAdicionales Is Nothing Then Exit Sub
        'Try
        '    With frmRefIngVentasAdicionales
        '        If .intIDCab <> Convert.ToInt32(dgv.CurrentRow.Cells("IDCab").Value) Then
        '            .intIDCab = Convert.ToInt32(dgv.CurrentRow.Cells("IDCab").Value)
        '            .lblFile.Text = dgv.CurrentRow.Cells("IDFile").Value
        '            .blnSelectFile = True
        '        End If
        '    End With
        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub EditarControlCalidad()
        'If frmRefControlCalidadDato Is Nothing Then Exit Sub
        'Try
        '    With frmRefControlCalidadDato
        '        If .intIDCab <> Convert.ToInt32(dgv.CurrentRow.Cells("IDCab").Value) Then
        '            .intIDCab = Convert.ToInt32(dgv.CurrentRow.Cells("IDCab").Value)
        '            .strNroFile = dgv.CurrentRow.Cells("IDFile").Value
        '            .lblIDFile.Text = .strNroFile
        '            .txtIDFile.Text = .strNroFile
        '            .blnSelectFile = True
        '        End If
        '    End With
        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub EditarPresupuestoSobre()
        'If frmRefIngAdministracionPresupuestoDato Is Nothing Then Exit Sub
        'Try
        '    With frmRefIngAdministracionPresupuestoDato
        '        .intIDCab = dgv.CurrentRow.Cells("IDCab").Value.ToString
        '        .lblIDFile.Text = dgv.CurrentRow.Cells("IDFile").Value.ToString
        '        .strIDFile = dgv.CurrentRow.Cells("IDFile").Value.ToString
        '    End With
        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub Editar()
        Try
            Dim frm As frmIngCotizacionDato
            For Each dgvFila As DataGridViewRow In dgv.Rows
                If dgvFila.Selected = True Then
                    frm = New frmIngCotizacionDato
                    With frm
                        .intIDCab = dgvFila.Cells("IDCab").Value.ToString
                        .strCotizacion = dgvFila.Cells("Cotizacion").Value.ToString
                        .strCorreoReservas = dgvFila.Cells("CorreoEjecReservas").Value.ToString
                        .MdiParent = frmMenuTesting 'Me 'frmMain
                        .FormRef = Me
                        .FormRef.WindowState = FormWindowState.Maximized
                        .Show()

                        If blnPantallaFileAbierto(.Name, .intIDCab) Then
                            .Close()
                        End If

                    End With

                End If
            Next

            'pPosicionarFormsCascada("frmIngCotizacionDato")
            'End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub EditarDocumento()
        'If frmRefDocumentosDato Is Nothing Then Exit Sub
        'Try
        '    frmRefDocumentosDato.intIDCab = CInt(dgv.CurrentRow.Cells("IDCab").Value)
        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarAnticipos()
        'Try
        '    With frmRefAntic
        '        .txtNroFile.Text = If(IsDBNull(dgv.Item("IDFile", dgv.CurrentRow.Index).Value) = True, "", dgv.Item("IDFile", dgv.CurrentRow.Index).Value) 'dgv.CurrentRow.Cells("IDFile").Value
        '        .strIDCliente = dgv.Item("IDCliente", dgv.CurrentRow.Index).Value
        '        .blnCambioFile = True
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarDebitMemo()
        'Try
        '    With frmRefDebitMemo
        '        .lblNroFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value.ToString
        '        .intIDCab = dgv.Item("IDCab", dgv.CurrentRow.Index).Value.ToString
        '        .txtResponsable.Text = dgv.Item("UsuarioVen", dgv.CurrentRow.Index).Value.ToString
        '        .txtTitulo.Text = dgv.Item("Titulo", dgv.CurrentRow.Index).Value.ToString
        '        .txtPax.Text = dgv.Item("NroPax", dgv.CurrentRow.Index).Value.ToString
        '        .txtCLiente.Text = dgv.Item("DescCliente", dgv.CurrentRow.Index).Value.ToString
        '        .strIDCLiente = dgv.Item("IDCliente", dgv.CurrentRow.Index).Value.ToString
        '        If dgv.Item("IDBanco", dgv.CurrentRow.Index).Value.ToString <> "000" Then .cboBanco.SelectedValue = dgv.Item("IDBanco", dgv.CurrentRow.Index).Value.ToString
        '        .DtpFechaIn.Text = dgv.Item("FechaIn", dgv.CurrentRow.Index).Value.ToString
        '        .dtpFechaOUT.Text = dgv.Item("FechaOut", dgv.CurrentRow.Index).Value.ToString
        '        .blnConsultoFiltro = True

        '        'devolver idusuario de ventas

        '        Using objUsuBN As New clsUsuarioBN
        '            Using dr As SqlClient.SqlDataReader = objUsuBN.Consultar_Coticab(.intIDCab)

        '                dr.Read()
        '                If dr.HasRows Then
        '                    .strIDResponsable = dr("IDUsuario")
        '                    ' txtSupervisor.Text = dr("NombreJefe") + " " + dr("TxApellidosJefe")

        '                End If
        '                dr.Close()
        '            End Using
        '        End Using
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub AsignarUsuarioReservasOperaciones()
        'If Not blnValidarAnulado() Then Exit Sub
        'If Not blnValidarAsignacion() Then Exit Sub
        'Try

        '    Dim frm As New frmIngReservasUsuario
        '    With frm
        '        .frmRef = Me
        '        If chrModulo = "R" Then
        '            .UsuarioReserv = dgv.Item("UsuarioRes", dgv.CurrentRow.Index).Value.ToString
        '            .IDUsuarioAsig = dgv.Item("IDUsuario", dgv.CurrentRow.Index).Value.ToString
        '        ElseIf chrModulo = "O" Then
        '            .IDUsuarioAsig = dgv.Item("IDUsuarioOper", dgv.CurrentRow.Index).Value.ToString
        '            .UsuarioOperac = dgv.Item("UsuarioOpe", dgv.CurrentRow.Index).Value.ToString

        '            .IDUsuarioAsig_Cusco = dgv.Item("IDUsuarioOper_Cusco", dgv.CurrentRow.Index).Value.ToString
        '            .UsuarioOperac_Cusco = dgv.Item("UsuarioOpe_Cusco", dgv.CurrentRow.Index).Value.ToString

        '        End If
        '        .intIDCab = dgv.Item("IDCab", dgv.CurrentRow.Index).Value
        '        .blnSoloConsulta = Not blnEditarAsignacion
        '        .chrModulo = chrModulo
        '        .ShowDialog()
        '    End With

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub AsignarUsuarioReservasOperaciones_Lista()
        'If Not blnValidarAnulado() Then Exit Sub
        'If Not blnValidarAsignacion() Then Exit Sub
        'Try

        '    If dgv.SelectedRows.Count > 0 Then

        '        'objLstEjecutivos_Sel = New List(Of stEjecutivos)

        '        objLstCab = New List(Of String)

        '        For i As Integer = 0 To dgv.SelectedRows.Count - 1


        '            objLstCab.Add(dgv.SelectedRows(i).Cells("IDCab").Value.ToString)

        '        Next
        '    End If


        '    Dim frm As New frmIngReservasUsuario
        '    With frm
        '        .frmRef = Me
        '        .blnMultiple = True

        '        .objLstCab = objLstCab

        '        'If chrModulo = "R" Then
        '        '    .UsuarioReserv = dgv.Item("UsuarioRes", dgv.CurrentRow.Index).Value.ToString
        '        '    .IDUsuarioAsig = dgv.Item("IDUsuario", dgv.CurrentRow.Index).Value.ToString
        '        'ElseIf chrModulo = "O" Then
        '        '    .IDUsuarioAsig = dgv.Item("IDUsuarioOper", dgv.CurrentRow.Index).Value.ToString
        '        '    .UsuarioOperac = dgv.Item("UsuarioOpe", dgv.CurrentRow.Index).Value.ToString

        '        '    .IDUsuarioAsig_Cusco = dgv.Item("IDUsuarioOper_Cusco", dgv.CurrentRow.Index).Value.ToString
        '        '    .UsuarioOperac_Cusco = dgv.Item("UsuarioOpe_Cusco", dgv.CurrentRow.Index).Value.ToString

        '        'End If
        '        '.intIDCab = dgv.Item("IDCab", dgv.CurrentRow.Index).Value
        '        If chrModulo = "C" Then
        '            .blnSoloConsulta = False
        '        Else
        '            .blnSoloConsulta = Not blnEditarAsignacion
        '        End If

        '        'jorge
        '        If chrModulo = "O" Then
        '            .IDUsuarioAsig = dgv.Item("IDUsuarioOper", dgv.CurrentRow.Index).Value.ToString
        '            .UsuarioOperac = dgv.Item("UsuarioOpe", dgv.CurrentRow.Index).Value.ToString

        '            .IDUsuarioAsig_Cusco = dgv.Item("IDUsuarioOper_Cusco", dgv.CurrentRow.Index).Value.ToString
        '            .UsuarioOperac_Cusco = dgv.Item("UsuarioOpe_Cusco", dgv.CurrentRow.Index).Value.ToString

        '        End If

        '        .chrModulo = chrModulo
        '        .ShowDialog()
        '    End With

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Function blnValidarAsignacion()
        Try

            Dim blnOk As Boolean = True
            Dim strTitulo As String = ""
            If chrModulo = "R" Then
                Dim strEstado As String = dgv.Item("Estado", dgv.CurrentRow.Index).Value.ToString

                If strEstado = "P" Then
                    blnOk = False
                End If
                strTitulo = "Reservas"
                'ElseIf chrModulo = "O" Then
                '    Dim strFecha As String = dgv.Item("Fecha", dgv.CurrentRow.Index).Value.ToString

                '    If strFecha = "" Then
                '        blnOk = False
                '    End If
                '    strTitulo = "Operaciones"
            End If

            If Not blnOk Then
                MessageBox.Show("El registro seleccionado, no contiene datos para " & strTitulo & "." & vbCrLf & _
                                "Por ende no se podrá asignar un ejecutivo", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

            End If

            Return blnOk
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function blnValidarAnulado() As Boolean
        Try

            Dim strEstado As String = dgv.Item("Estado", dgv.CurrentRow.Index).Value.ToString

            If strEstado = "X" Then
                MessageBox.Show("La cotización o file seleccionado se encuentra anulado", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Return False
            End If

            Return True
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub EditarReservas()
        'Try
        '    Dim lintIDCab As Integer
        '    lintIDCab = dgv.CurrentRow.Cells("IDCab").Value.ToString
        '    Dim frm As New frmIngReservasControlporFileDato
        '    With frm
        '        .intIDCab = lintIDCab
        '        .strIDFile = dgv.CurrentRow.Cells("IDFile").Value.ToString
        '        .strNombreEjecutivosVentas = dgv.CurrentRow.Cells("UsuarioVen").Value.ToString
        '        .MdiParent = frmMain
        '        .strModulo = "R"
        '        .FormRef = Me
        '        Debug.Print("Llamada al Show: " & DateTime.Now.ToLongTimeString)
        '        .Show()
        '        If blnPantallaFileAbierto(.Name, .intIDCab) Then
        '            .Close()
        '        End If
        '    End With
        '    'Dim frm As frmIngReservasControlporFileDato
        '    'For Each DgvFila As DataGridViewRow In dgv.Rows
        '    '    If DgvFila.Selected = True Then
        '    '        frm = New frmIngReservasControlporFileDato
        '    '        With frm
        '    '            .intIDCab = DgvFila.Cells("IDCab").Value.ToString 'dgv.Item("IDCab", dgv.CurrentCell.RowIndex).Value
        '    '            .strIDFile = DgvFila.Cells("IDFile").Value.ToString 'dgv.Item("IDFile", dgv.CurrentCell.RowIndex).Value
        '    '            .strNombreEjecutivosVentas = DgvFila.Cells("UsuarioVen").Value.ToString
        '    '            .MdiParent = frmMain
        '    '            .strModulo = "R"
        '    '            .FormRef = Me
        '    '            .Show()

        '    '            If blnPantallaFileAbierto(.Name, .intIDCab) Then
        '    '                .Close()
        '    '            End If
        '    '        End With
        '    '    End If
        '    'Next
        '    '''''pPosicionarFormsCascada("frmIngReservasControlporFileDato")

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub
    Private Sub EditarOperaciones()
        'Try
        '    Dim frm As frmIngReservasControlporFileDato
        '    For Each DgvFila As DataGridViewRow In dgv.Rows
        '        If DgvFila.Selected = True Then
        '            frm = New frmIngReservasControlporFileDato
        '            With frm
        '                .intIDCab = DgvFila.Cells("IDCab").Value.ToString 'dgv.Item("IDCab", dgv.CurrentCell.RowIndex).Value
        '                .strIDFile = DgvFila.Cells("IDFile").Value.ToString 'dgv.Item("IDFile", dgv.CurrentCell.RowIndex).Value

        '                '.strIDProveedor = dgv.Item("IDProveedor", dgv.CurrentCell.RowIndex).Value
        '                '.strProveedor = dgv.Item("DescProveedor", dgv.CurrentCell.RowIndex).Value

        '                .MdiParent = frmMain
        '                .strModulo = "O"
        '                .FormRef = Me
        '                .Show()

        '                If blnPantallaFileAbierto(.Name, .intIDCab) Then
        '                    .Close()
        '                End If
        '            End With
        '        End If
        '    Next

        '    'pPosicionarFormsCascada("frmIngReservasControlporFileDato")
        'Catch ex As Exception
        '    Throw
        'End Try

    End Sub

    Private Sub EditarProgramacionOperaciones()
        'Try
        '    Dim frm As frmIngOperacionProgramacion
        '    For Each DgvFila As DataGridViewRow In dgv.Rows
        '        If DgvFila.Selected = True Then
        '            frm = New frmIngOperacionProgramacion
        '            With frm
        '                .intIDCab = dgv.Item("IDCab", dgv.CurrentCell.RowIndex).Value
        '                .strIDFile = dgv.Item("IDFile", dgv.CurrentCell.RowIndex).Value
        '                .strTitulo = dgv.Item("Titulo", dgv.CurrentCell.RowIndex).Value
        '                .strCorreoEjecutivoReservas = dgv.CurrentRow.Cells("CorreoEjecReservas").Value.ToString
        '                .strNombreEjecutivoReservas = dgv.CurrentRow.Cells("UsuarioRes").Value.ToString
        '                .strNombreEjecutivoReservas = dgv.CurrentRow.Cells("UsuarioRes").Value.ToString
        '                .intNroPax = dgv.CurrentRow.Cells("NroPax").Value.ToString
        '                .strDesCliente = dgv.CurrentRow.Cells("DescCliente").Value.ToString
        '                .strIDCliente = dgv.CurrentRow.Cells("IDCliente").Value.ToString
        '                .strIDUsuarioOper_Cusco = dgv.CurrentRow.Cells("IDUsuarioOper_Cusco").Value.ToString
        '                .MdiParent = frmMain
        '                .Show()

        '                If blnPantallaFileAbierto(.Name, .intIDCab) Then
        '                    .Close()
        '                End If
        '            End With
        '        End If
        '    Next

        '    pPosicionarFormsCascada("frmIngOperacionProgramacion")
        'Catch ex As Exception
        '    Throw
        'End Try

    End Sub

    Private Sub EditarTapa()
        'If frmRefTapas Is Nothing Then Exit Sub
        'Try
        '    With frmRefTapas
        '        .intIDCAB = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
        '        .txtObservaciones.Text = If(IsDBNull(dgv.Item("Observaciones", dgv.CurrentRow.Index).Value) = True, "", dgv.Item("Observaciones", dgv.CurrentRow.Index).Value)
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarPPTOConsCusco()
        'If frmRefRepPresConsolidado_Cusco Is Nothing Then Exit Sub
        'Try
        '    With frmRefRepPresConsolidado_Cusco
        '        .intIDCAB = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
        '        '.txtObservaciones.Text = If(IsDBNull(dgv.Item("Observaciones", dgv.CurrentRow.Index).Value) = True, "", dgv.Item("Observaciones", dgv.CurrentRow.Index).Value)
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarFondoFijo()
        'If frmRefDocumentosProveedorDato_FondoFijo Is Nothing Then Exit Sub
        'Try
        '    With frmRefDocumentosProveedorDato_FondoFijo
        '        .intIDCab_FondoFijo = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
        '        '.txtObservaciones.Text = If(IsDBNull(dgv.Item("Observaciones", dgv.CurrentRow.Index).Value) = True, "", dgv.Item("Observaciones", dgv.CurrentRow.Index).Value)
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarDocumProv()
        If frmRefDocumentosProveedorDato Is Nothing Then Exit Sub
        Try
            With frmRefDocumentosProveedorDato
                .intIDCab_FondoFijo = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
                .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
                '.txtObservaciones.Text = If(IsDBNull(dgv.Item("Observaciones", dgv.CurrentRow.Index).Value) = True, "", dgv.Item("Observaciones", dgv.CurrentRow.Index).Value)
            End With

            Me.Close()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub EditarRptNoShow()
        'If frmRefRepNoShow Is Nothing Then Exit Sub
        'Try
        '    With frmRefRepNoShow
        '        .intIDCAB = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
        '        '.txtObservaciones.Text = If(IsDBNull(dgv.Item("Observaciones", dgv.CurrentRow.Index).Value) = True, "", dgv.Item("Observaciones", dgv.CurrentRow.Index).Value)
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub SelFilePreLiquidacion()
        ''If frmRefGenPreLiq Is Nothing Then Exit Sub
        'Try
        '    With frmRefGenPreLiq
        '        .intIDCab = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
        '        .lblTituloFile.Text = dgv.Item("Titulo", dgv.CurrentRow.Index).Value
        '        .strIDNotaVenta = dgv.Item("IDNotaVenta", dgv.CurrentRow.Index).Value
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub SelFileRepLiquidacionFile()

        'Try
        '    With frmRefLiquidacionFile
        '        .intIDCAB = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarBiblia()
        'If frmRefBiblias Is Nothing Then Exit Sub
        'Try
        '    With frmRefBiblias
        '        .intIDCab = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .txtCotizacion.Text = dgv.Item("Cotizacion", dgv.CurrentRow.Index).Value
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub EditarVoucherMaster()
        'If frmRefVoucherMaster Is Nothing Then Exit Sub
        'Try
        '    With frmRefVoucherMaster
        '        .intIDCAB = dgv.Item("IdCab", dgv.CurrentRow.Index).Value
        '        .lblIDFile.Text = dgv.Item("IDFile", dgv.CurrentRow.Index).Value
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EditarTicket()
        'Try

        '    With frmRefTicketDato
        '        .intIDCab = dgv.Item("IDCab", dgv.CurrentRow.Index).Value
        '        .lblFile.Text = If(IsDBNull(dgv.Item("IDFile", dgv.CurrentRow.Index).Value) = True, "", dgv.Item("IDFile", dgv.CurrentRow.Index).Value)
        '        .strIDCliente = dgv.Item("IDCliente", dgv.CurrentRow.Index).Value
        '        .lblDescCliente.Text = dgv.Item("DescCliente", dgv.CurrentRow.Index).Value
        '    End With

        '    Me.Close()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub pNuevo(ByVal sender As System.Object, ByVal e As System.EventArgs)
        'Dim frm As New frmIngCotizacionDato
        'With frm
        '    .MdiParent = frmMain
        '    .FormRef = Me
        '    .Show()
        '    'Buscar()
        'End With
    End Sub

    Public Sub Buscar()
        Dim strIDUsuario As String = If(cboUsuario.Text = "", "", cboUsuario.SelectedValue.ToString)
        Dim strIDEstado As String = If(cboEstado.Text = "", "", cboEstado.SelectedValue.ToString)

        Dim strIDCab As String = ""
        If Not dgv.CurrentRow Is Nothing Then
            strIDCab = dgv.CurrentRow.Cells("IDCab").Value
        End If

        Try
            Me.Cursor = Cursors.WaitCursor
            dgv.DataSource = objBN.ConsultarList(strIDUsuario, txtCotizacion.Text, txtIDFile.Text, txtCliente.Text, _
                                                 txtTitulo.Text, strIDEstado, txtNombrePax.Text.Trim, "C", False, False, _
                                                 dtpFechaInicioIni.Value.ToShortDateString, dtpFechaInicioFin.Value.ToShortDateString, _
                                                 dtpFechaAsigReservIni.Value.ToShortDateString, dtpFechaAsigReservFin.Value.ToShortDateString, _
                                                 dtpRango1FecAsigOper.Value.ToShortDateString, dtpRango2FecAsigOper.Value.ToShortDateString, _
                                                 chkFilesArchivados.Checked, If(chrModulo = "VA", "", ""))

            AlinearFilaGrid(dgv, "IDCab", strIDCab, dgv.Columns("UsuarioVen").Index)
            pDgvAnchoColumnas(dgv)

            If chrModulo = "O" Then
                dgv.Columns("UsuarioOpe_Cusco").Visible = True
                dgv.Columns("UsuarioOpe").HeaderText = "Ejecutivo de Operaciones Lima"
            Else
                dgv.Columns("UsuarioOpe_Cusco").Visible = False
            End If

            AnchoColumnasDuro()
            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Public Sub BuscarDebitMemo()
        Dim strIDUsuario As String = If(cboUsuario.Text = "", "", cboUsuario.SelectedValue.ToString)
        Dim strIDEstado As String = If(cboEstado.Text = "", "", cboEstado.SelectedValue.ToString)
        Dim strIDCab As String = ""
        If Not dgv.CurrentRow Is Nothing Then
            strIDCab = dgv.CurrentRow.Cells("IDCab").Value
        End If
        Try
            Me.Cursor = Cursors.WaitCursor
            dgv.DataSource = objBN.ConsultarList(strIDUsuario, txtCotizacion.Text, txtIDFile.Text, txtCliente.Text, _
                                                 txtTitulo.Text, strIDEstado, txtNombrePax.Text.Trim, "D", False, False, _
                                                 dtpFechaInicioIni.Value.ToShortDateString, dtpFechaInicioFin.Value.ToShortDateString, _
                                                 dtpFechaAsigReservIni.Value.ToShortDateString, dtpFechaAsigReservFin.Value.ToShortDateString, _
                                                 dtpRango1FecAsigOper.Value.ToShortDateString, dtpRango2FecAsigOper.Value.ToShortDateString, chkFilesArchivados.Checked)
            AlinearFilaGrid(dgv, "IDCab", strIDCab, dgv.Columns("UsuarioVen").Index)
            pDgvAnchoColumnas(dgv)

            AnchoColumnasDuro()

            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub
    Private Sub AnchoColumnasDuro()
        dgv.Columns("Cotizacion").Width = 70
        dgv.Columns("UsuarioOpe").Width = 110
        dgv.Columns("UsuarioOpe_Cusco").Width = 110
        dgv.Columns("UsuarioVen").Width = 110
        dgv.Columns("UsuarioRes").Width = 110
        dgv.Columns("Fecha").Width = 70
        dgv.Columns("FechaAsigReserva").Width = 80
        dgv.Columns("IDFile").Width = 60
        dgv.Columns("DescCliente").Width = 100
        dgv.Columns("Titulo").Width = 300
        dgv.Columns("Ruta").Width = 130
    End Sub
    Public Sub BuscarReserva()
        'Dim objRes As New clsReservaBT
        'objRes.RegenerarReserva(6710, "000072", 3231, "0001")
        'objRes.RegenerarReserva(6709, "000222", 3231, "0001")
        'objRes.RegenerarReserva(21288, "000090", 4733, "0001")
        'objRes.RegenerarReserva(0, "002311", 2988, "0001")
        'objRes.RegenerarReserva(0, "002311", 6478, "0001")
        'objRes.RegenerarReserva(0, "002311", 7025, "0001")
        'objRes.RegenerarReserva(0, "002311", 7193, "0001")
        'objRes.RegenerarReserva(0, "002311", 7324, "0001")
        'objRes.RegenerarReserva(0, "001624", 5092, "0001")
        'objRes.RegenerarReserva(0, "002506", 10978, "0001")
        'objRes.RegenerarReserva(0, "000185", 11088, "0001")
        'objRes.RegenerarReserva(0, "000092", 11088, "0001")
        'objRes.RegenerarReserva(0, "000074", 11088, "0001")
        'objRes.RegenerarReserva(0, "001839", 8506, "0001")
        'objRes.RegenerarReserva(36250, "001915", 8359, "0001")
        'objRes.RegenerarReserva(0, "001924", 3427, "0001")
        'objRes.RegenerarReserva(0, "000071", 8045, "0001")
        'objRes.RegenerarReserva(0, "000150", 8040, "0001")
        'objRes.RegenerarReserva(0, "000387", 8040, "0001")
        'objRes.RegenerarReserva(0, "000185", 12611, "0001")
        'objRes.RegenerarReserva(0, "001836", 8503, "0001")
        'objRes.RegenerarReserva(0, "001872", 13550, "0001")
        'objRes.RegenerarReserva(39718, "002195", 13550, "0001")
        'objRes.RegenerarReserva(0, "000562", 7989, "0001")
        'objRes.RegenerarReserva(0, "000416", 10702, "0001")
        'objRes.RegenerarReserva(0, "000418", 8850, "0001")
        'objRes.RegenerarReserva(0, "002265", 11061, "0001")
        'objRes.RegenerarReserva(0, "000182", 11063, "0001")

        'Dim obj As New clsOperacionBT
        'obj.GrabarxFileyProveedor(4221, "002120", "0001")
        'obj.GrabarxFileyProveedor(5158, "002231", "0001")
        'obj.GrabarxFileyProveedor(5158, "000177", "0001")
        'obj.GrabarxFileyProveedor(5158, "000562", "0001")
        'obj.GrabarxFileyProveedor(5158, "001858", "0001")
        'obj.GrabarxFileyProveedor(5895, "002120", "0001")
        'obj.GrabarxFileyProveedor(5787, "002155", "0001")
        'obj.GrabarxFileyProveedor(2371, "002101", "0001")
        'obj.GrabarxFileyProveedor(6366, "002101", "0001")
        'obj.GrabarxFileyProveedor(6366, "002120", "0001")
        'obj.GrabarxFileyProveedor(5730, "002120", "0001")
        'obj.GrabarxFileyProveedor(6040, "002120", "0001")
        'obj.GrabarxFileyProveedor(5288, "001587", "0001")
        'obj.GrabarxFileyProveedor(4255, "002148", "0001")
        'obj.GrabarxFileyProveedor(2293, "002101", "0001")
        'obj.GrabarxFileyProveedor(2424, "002187", "0001")
        'obj.GrabarxFileyProveedor(4640, "002120", "0001")
        'obj.GrabarxFileyProveedor(4061, "002173", "0001")
        'obj.GrabarxFileyProveedor(4061, "002120", "0001")
        'obj.GrabarxFileyProveedor(2470, "002190", "0001")
        'obj.GrabarxFileyProveedor(2470, "002270", "0001")
        'obj.GrabarxFileyProveedor(6027, "002179", "0001")
        'obj.GrabarxFileyProveedor(2806, "000636", "0001")
        'obj.GrabarxFileyProveedor(6786, "000468", "0001")
        'obj.GrabarxFileyProveedor(6635, "002148", "0001")
        'obj.GrabarxFileyProveedor(6635, "000180", "0001")
        'obj.GrabarxFileyProveedor(2988, "002311", "0001")
        'obj.GrabarxFileyProveedor(6478, "002311", "0001")
        'obj.GrabarxFileyProveedor(7025, "002311", "0001")
        'obj.GrabarxFileyProveedor(4194, "001712", "0001")
        'obj.GrabarxFileyProveedor(4194, "000035", "0001")
        'obj.GrabarxFileyProveedor(4194, "001686", "0001")
        'obj.GrabarxFileyProveedor(4860, "000417", "0001")
        'obj.GrabarxFileyProveedor(7324, "002311", "0001")
        'obj.GrabarxFileyProveedor(7543, "001915", "0001")
        'obj.GrabarxFileyProveedor(8592, "001587", "0001")
        'obj.GrabarxFileyProveedor(10657, "000160", "0001")
        'obj.GrabarxFileyProveedor(8588, "000159", "0001")
        'obj.GrabarxFileyProveedor(8040, "000150", "0001")
        'obj.GrabarxFileyProveedor(8040, "000387", "0001")
        'obj.GrabarxFileyProveedor(9043, "001836", "0001")
        'obj.GrabarxFileyProveedor(12152, "001872", "0001")
        'obj.GrabarxFileyProveedor(7143, "000579", "0001")
        'obj.GrabarxFileyProveedor(11455, "000067", "0001")
        'obj.GrabarxFileyProveedor(12103, "001587", "0001")
        'obj.GrabarxFileyProveedor(8072, "000382", "0001")
        'obj.GrabarxFileyProveedor(8503, "001836", "0001")
        'obj.GrabarxFileyProveedor(12959, "000336", "0001")
        'obj.GrabarxFileyProveedor(12747, "000355", "0001")
        'obj.GrabarxFileyProveedor(12747, "000529", "0001")
        'obj.GrabarxFileyProveedor(11793, "000182", "0001")
        'obj.GrabarxFileyProveedor(7989, "000562", "0001")
        'obj.GrabarxFileyProveedor(11793, "000295", "0001")
        'obj.GrabarxFileyProveedor(8771, "000612", "0001")
        'obj.GrabarxFileyProveedor(12163, "000101", "0001")
        'obj.GrabarxFileyProveedor(12163, "002224", "0001")
        'obj.GrabarxFileyProveedor(12163, "000387", "0001")
        'obj.GrabarxFileyProveedor(12163, "000146", "0001")
        'obj.GrabarxFileyProveedor(12163, "000162", "0001") 
        'obj.GrabarxFileyProveedor(10953, "002876", "0001")
        'obj.GrabarxFileyProveedor(8850, "000418", "0001")
        'obj.GrabarxFileyProveedor(11175, "000313", "0001")
        'obj.GrabarxFileyProveedor(11063, "000182", "0001")
        'obj.GrabarxFileyProveedor(14461, "001726", "0001")
        'obj.GrabarxFileyProveedor(17706, "002834", "0001")
        'obj.GrabarxFileyProveedor(17706, "002833", "0001")
        'obj.GrabarxFileyProveedor(10873, "002549", "0001")
        'obj.GrabarxFileyProveedor(14227, "001632", "0001")

        If chkUsuarioNoAsignado.Checked Then
            cboUsuario.SelectedValue = ""
        End If
        Dim strIDUsuario As String = If(cboUsuario.Text = "", "", cboUsuario.SelectedValue.ToString)
        'Dim strIDEstado As Char = If(cboEstado.Text = "", "", cboEstado.SelectedValue.ToString)
        Dim strIDCab As String = ""
        If Not dgv.CurrentRow Is Nothing Then
            strIDCab = dgv.CurrentRow.Cells("IDCab").Value
        End If

        Try
            Me.Cursor = Cursors.WaitCursor
            dgv.DataSource = objBN.ConsultarList(strIDUsuario, txtCotizacion.Text, txtIDFile.Text, txtCliente.Text, _
                                                 txtTitulo.Text, cboEstado.SelectedValue.ToString, txtNombrePax.Text.Trim, "R", chkFilesAntiguos.Checked, chkUsuarioNoAsignado.Checked, _
                                                 dtpFechaInicioIni.Value.ToShortDateString, dtpFechaInicioFin.Value.ToShortDateString, _
                                                 dtpFechaAsigReservIni.Value.ToShortDateString, dtpFechaAsigReservFin.Value.ToShortDateString, _
                                                 dtpRango1FecAsigOper.Value.ToShortDateString, dtpRango2FecAsigOper.Value.ToShortDateString, chkFilesArchivados.Checked)
            AlinearFilaGrid(dgv, "IDCab", strIDCab, dgv.Columns("UsuarioVen").Index)
            pDgvAnchoColumnas(dgv)
            dgv.Columns("UsuarioOpe_Cusco").Visible = False
            AnchoColumnasDuro()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Public Sub BuscarOperaciones()
        If chkUsuarioNoAsignado.Checked Then
            cboUsuario.SelectedValue = ""
        End If

        Dim strIDUsuario As String = If(cboUsuario.Text = "", "", cboUsuario.SelectedValue.ToString)
        'Dim strIDEstado As Char = If(cboEstado.Text = "", "", cboEstado.SelectedValue.ToString)
        Dim strIDCab As String = ""
        If Not dgv.CurrentRow Is Nothing Then
            strIDCab = dgv.CurrentRow.Cells("IDCab").Value
        End If
        Try
            Me.Cursor = Cursors.WaitCursor
            dgv.DataSource = objBN.ConsultarList(strIDUsuario, txtCotizacion.Text, txtIDFile.Text, txtCliente.Text, _
                                                 txtTitulo.Text, cboEstado.SelectedValue.ToString, txtNombrePax.Text.Trim, "O", chkFilesAntiguos.Checked, chkUsuarioNoAsignado.Checked, _
                                                 dtpFechaInicioIni.Value.ToShortDateString, dtpFechaInicioFin.Value.ToShortDateString, _
                                                 dtpFechaAsigReservIni.Value.ToShortDateString, dtpFechaAsigReservFin.Value.ToShortDateString, _
                                                 dtpRango1FecAsigOper.Value.ToShortDateString, dtpRango2FecAsigOper.Value.ToShortDateString, chkFilesArchivados.Checked)

            AlinearFilaGrid(dgv, "IDCab", strIDCab, dgv.Columns("UsuarioVen").Index)


            dgv.Columns("UsuarioOpe_Cusco").Visible = True
            dgv.Columns("UsuarioOpe").HeaderText = "Ejecutivo de Operaciones Lima"


            pDgvAnchoColumnas(dgv)
            AnchoColumnasDuro()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Public Sub BuscarPreLiquidacion()

        Dim strIDUsuario As String = If(cboUsuario.Text = "", "", cboUsuario.SelectedValue.ToString)
        'Dim strIDEstado As Char = If(cboEstado.Text = "", "", cboEstado.SelectedValue.ToString)
        Dim strIDCab As String = ""
        If Not dgv.CurrentRow Is Nothing Then
            strIDCab = dgv.CurrentRow.Cells("IDCab").Value
        End If
        Try
            Me.Cursor = Cursors.WaitCursor
            dgv.DataSource = objBN.ConsultarList(strIDUsuario, txtCotizacion.Text, txtIDFile.Text, txtCliente.Text, _
                                                 txtTitulo.Text, "", txtNombrePax.Text.Trim, "P", False, False, _
                                                 dtpFechaInicioIni.Value.ToShortDateString, dtpFechaInicioFin.Value.ToShortDateString, _
                                                 dtpFechaAsigReservIni.Value.ToShortDateString, dtpFechaAsigReservFin.Value.ToShortDateString, _
                                                 dtpRango1FecAsigOper.Value.ToShortDateString, dtpRango2FecAsigOper.Value.ToShortDateString, chkFilesArchivados.Checked)
            AlinearFilaGrid(dgv, "IDCab", strIDCab, dgv.Columns("UsuarioVen").Index)
            pDgvAnchoColumnas(dgv)
            AnchoColumnasDuro()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub



    Private Sub BuscarTapa()
        Dim strIDUsuario As String = If(cboUsuario.Text = "", "", cboUsuario.SelectedValue.ToString)
        Dim strIDEstado As Char = If(cboEstado.Text = "", "", cboEstado.SelectedValue.ToString)
        Dim strIDCab As String = ""
        If Not dgv.CurrentRow Is Nothing Then
            strIDCab = dgv.CurrentRow.Cells("IDCab").Value
        End If
        Try
            Me.Cursor = Cursors.WaitCursor
            dgv.DataSource = objBN.ConsultarList(strIDUsuario, txtCotizacion.Text, txtIDFile.Text, txtCliente.Text, _
                                                 txtTitulo.Text, strIDEstado, txtNombrePax.Text.Trim, "T", False, False, _
                                                 dtpFechaInicioIni.Value.ToShortDateString, dtpFechaInicioFin.Value.ToShortDateString, _
                                                 dtpFechaAsigReservIni.Value.ToShortDateString, dtpFechaAsigReservFin.Value.ToShortDateString, _
                                                 dtpRango1FecAsigOper.Value.ToShortDateString, dtpRango2FecAsigOper.Value.ToShortDateString, chkFilesArchivados.Checked)
            AlinearFilaGrid(dgv, "IDCab", strIDCab, dgv.Columns("UsuarioVen").Index)
            pDgvAnchoColumnas(dgv)
            AnchoColumnasDuro()
            Me.Cursor = Cursors.Default
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Private Sub pEliminar(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try

            If MessageBox.Show("¿Está Ud. seguro de anular la Cotización " & dgv.Item("Cotizacion", dgv.CurrentCell.RowIndex).Value.ToString & "?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then Exit Sub

            Dim objBT As New clsCotizacionBT
            objBT.Anular(dgv.Item("IDCab", dgv.CurrentCell.RowIndex).Value.ToString, gstrUser)

            MessageBox.Show("Se anuló el Cotización " & dgv.Item("Cotizacion", dgv.CurrentCell.RowIndex).Value.ToString & " exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

            Buscar()
            dgv_Click(Nothing, Nothing)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    'Private Sub pImprimir(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    Try
    '        Dim frm As New frmRepCotizacionGeneral
    '        With frm
    '            .ShowDialog()
    '        End With
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
    '    End Try
    'End Sub


    Private Sub dgv_CellPainting(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        If dgv.Columns(e.ColumnIndex).Name = "btnAsigEjec" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnAsigEjec"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.folder_user

            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgv.Columns(e.ColumnIndex).Name = "btnEdit" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnEdit"), DataGridViewButtonCell)
            Dim icoAtomico As Image = Nothing
            icoAtomico = My.Resources.Resources.zoom
            If (blnEditarEnabledBD Or blnGrabarEnabledBD) Then
                icoAtomico = My.Resources.Resources.editarpng
            End If
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If
        If dgv.Columns(e.ColumnIndex).Name = "btnDel" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnDel"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.eliminarpng
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgv.Columns(e.ColumnIndex).Name = "btnEliminarReservasuOperaciones" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnEliminarReservasuOperaciones"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.error_delete
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        'btnEntrVentas
        If dgv.Columns(e.ColumnIndex).Name = "btnEntrVentas" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnEntrVentas"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.date_go
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

        If dgv.Columns(e.ColumnIndex).Name = "btnCorreos" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnCorreos"), DataGridViewButtonCell)
            Dim icoAtomico As Image = My.Resources.Resources.outlook_folder_inbox
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If
    End Sub

    Public Sub dgv_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.Click
        If chrModulo = "T" Or chrModulo = "B" Or chrModulo = "V" Or chrModulo = "CC" Or chrModulo = "RPCDIC" Or chrModulo = "" Then Exit Sub
        If dgv.RowCount > 0 Then
            blnEditarEnabled = True
            'blnEliminarEnabled = True
            ActivarDesactivarBotones()

        Else
            blnEditarEnabled = False
            blnEliminarEnabled = False
            ActivarDesactivarBotones()
        End If

    End Sub

    'Public Sub dgvSedes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvSedes.Click
    '    If dgvSedes.RowCount > 0 Then
    '        blnEditarEnabled = True
    '        blnEliminarEnabled = True
    '        ActivarDesactivarBotones()

    '    Else
    '        blnEditarEnabled = False
    '        blnEliminarEnabled = False
    '        ActivarDesactivarBotones()
    '    End If

    'End Sub
    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick


        Try
            If dgv.CurrentCell Is Nothing Then Exit Sub
            'If dgv.CurrentCell.ColumnIndex = -1 Then Exit Sub

            Select Case dgv.CurrentCell.ColumnIndex
                Case dgv.Columns("btnEdit").Index
                    Select Case chrModulo
                        Case "C"
                            Editar()
                        Case "R"
                            EditarReservas()
                        Case "O"
                            If Not blnOpeProgram Then
                                EditarOperaciones()
                            Else
                                EditarProgramacionOperaciones()
                            End If
                    End Select

                Case dgv.Columns("btnDel").Index
                    pEliminar(Nothing, Nothing)

                Case dgv.Columns("btnEntrVentas").Index
                    EnviaraVentas()

                Case dgv.Columns("btnAsigEjec").Index
                    AsignarUsuarioReservasOperaciones()
                    'ReportearLiquidacionFile(dgv.CurrentRow.Cells("IDCab").Value, dgv.CurrentRow.Cells("IDFile").Value)

                Case dgv.Columns("btnEliminarReservasuOperaciones").Index
                    Dim intIDCab As Integer = dgv.CurrentRow.Cells("IDCab").Value
                    If chrModulo = "R" Then
                        EliminarReservasFile(intIDCab)
                    ElseIf chrModulo = "O" Then
                        EliminarOperacionesFile(intIDCab)
                    End If

                Case dgv.Columns("btnCorreos").Index
                    'Dim frm As New frmBandejaCorreosAreas
                    'frm.MdiParent = frmMain
                    'frm.intIDCab = dgv.CurrentRow.Cells("IDCab").Value.ToString
                    'frm.strIDFile = dgv.CurrentRow.Cells("IDFile").Value.ToString
                    'frm.strTitulo = dgv.CurrentRow.Cells("Titulo").Value

                    'frm.Show()

            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub EnviaraVentas()
        'If dgv.CurrentRow Is Nothing Then Exit Sub
        'Try
        '    Dim frm As New frmIngResOpeFechaEnvioVentas
        '    frm.strModulo = chrModulo
        '    frm.frmRef = Me
        '    frm.intIDCab = dgv.CurrentRow.Cells("IDCab").Value
        '    frm.ShowDialog()
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub EliminarReservasFile(ByVal vintIDCab As Integer)
        Try
            Dim objResBn As New clsReservaBN

            If Not objResBn.blnExistenReservasxIDCab(vintIDCab) Then
                MessageBox.Show("No se ha encontrado información generada en Reservas para este File", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            Dim objBNOp As New clsOrdenPagoBN
            If objBNOp.blnExistexFile(vintIDCab) Then
                MessageBox.Show("No se puede eliminar. Se han encontrado Órdenes de Pago para este File", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            Dim objOpeBN As New clsOperacionBN
            If objOpeBN.blnExistenOperacionesxIDCab(vintIDCab) Then
                MessageBox.Show("No se puede eliminar. Se ha encontrado información generada en Operaciones para este File, antes debe hacerlo desde dicho módulo", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            If MessageBox.Show("¿Está Ud. seguro de regenerar toda la información de este File?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                Dim objResBT As New clsReservaBT
                objResBT.RegeneracionReservas(objBN.objBECotizacion(vintIDCab, gstrUser))

                MessageBox.Show("La información generada a Reservas para este File ha sido regenerada exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                BuscarReserva()
                Me.Cursor = Cursors.Default
            End If


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Private Sub EliminarOperacionesFile(ByVal vintIDCab As Integer)
        Try

            'Dim objOpeBN As New clsOperacionBN
            'If Not objOpeBN.blnExistenOperacionesxIDCab(vintIDCab) Then
            '    MessageBox.Show("No se ha encontrado información generada en Operaciones para este File", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            '    Exit Sub
            'End If

            Dim objOpeBN As New clsOperacionBN
            If objOpeBN.blnExistenVouchersuOrdenesServicioxIDCab(vintIDCab) Then
                MessageBox.Show("No se puede eliminar. Se han registrado vouchers u órdenes de servicio.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Exit Sub
            End If

            If MessageBox.Show("¿Está Ud. seguro de eliminar toda la información generada a Operaciones para este File?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation) = Windows.Forms.DialogResult.Yes Then
                Me.Cursor = Cursors.WaitCursor
                Dim objOpeBT As New clsOperacionBT
                objOpeBT.EliminarxFile(vintIDCab, gstrUser)

                MessageBox.Show("La información generada a Operaciones para este File ha sido eliminada exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                BuscarOperaciones()
                Me.Cursor = Cursors.Default
            End If


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Private Sub txtCotizacion_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCotizacion.TextChanged, _
        txtCliente.TextChanged, txtIDFile.TextChanged, txtTitulo.TextChanged, cboUsuario.TextChanged
        ErrPrv.SetError(btnBuscar, "")
    End Sub

    Private Sub dgv_MouseDown(ByVal sender As Object, ByVal e As System.Windows.Forms.MouseEventArgs) Handles dgv.MouseDown
        'If cboUsuario.SelectedValue.ToString = gstrUser Then Exit Sub
        If dgv.CurrentRow Is Nothing Then Exit Sub
        'If chrModulo <> "C" Then Exit Sub


        If chrModulo = "C" Then
            'ventas
            If IsDBNull(dgv.CurrentRow.Cells("IDFile").Value) Then
                mnuAgregarDebitMemo.Enabled = False
            Else
                mnuAgregarDebitMemo.Enabled = True
            End If

            If e.Button = Windows.Forms.MouseButtons.Right Then

                Dim intSelRows As Int16 = 0
                For Each dgvFila As DataGridViewRow In dgv.Rows
                    If dgvFila.Selected = True Then
                        intSelRows += 1
                    End If
                Next

                If intSelRows = 1 Then
                    Dim hti As DataGridView.HitTestInfo = dgv.HitTest(e.X, e.Y)

                    If hti.Type = DataGridViewHitTestType.Cell Then
                        dgv.CurrentCell = _
                        dgv.Rows(hti.RowIndex).Cells(hti.ColumnIndex)
                    End If
                End If


                dgv.ContextMenuStrip = cmsCopiarCotiz
                cmsCopiarCotiz.Show()
                Me.Activate()
            End If
        Else
            If e.Button = Windows.Forms.MouseButtons.Right Then
                dgv.ContextMenuStrip = cmsEjecutivosResOpe
                cmsEjecutivosResOpe.Show()
            End If

        End If

        'If IsDBNull(dgv.CurrentRow.Cells("IDFile").Value) Then
        '    mnuAgregarDebitMemo.Enabled = False
        'Else
        '    mnuAgregarDebitMemo.Enabled = True
        'End If

        'If e.Button = Windows.Forms.MouseButtons.Right Then

        '    Dim intSelRows As Int16 = 0
        '    For Each dgvFila As DataGridViewRow In dgv.Rows
        '        If dgvFila.Selected = True Then
        '            intSelRows += 1
        '        End If
        '    Next

        '    If intSelRows = 1 Then
        '        Dim hti As DataGridView.HitTestInfo = dgv.HitTest(e.X, e.Y)

        '        If hti.Type = DataGridViewHitTestType.Cell Then
        '            dgv.CurrentCell = _
        '            dgv.Rows(hti.RowIndex).Cells(hti.ColumnIndex)
        '        End If
        '    End If


        '    dgv.ContextMenuStrip = cmsCopiarCotiz
        '    cmsCopiarCotiz.Show()
        '    Me.Activate()
        'End If
    End Sub

    Private Sub mnuCopiar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles mnuCopiarCotizacion.Click
        Dim blnCopiarTodosVuelos As Boolean = False
        Try
            If MessageBox.Show("¿Está Ud. seguro de realizar una copia de la cotización seleccionada?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                Dim objVuelosBN As New clsCotizacionBN.clsCotizacionVuelosBN
                Dim blnExistenVuelos As Boolean = objVuelosBN.blnExisteVuelosManuales(CInt(dgv.CurrentRow.Cells("IDCab").Value))
                If blnExistenVuelos Then
                    If MessageBox.Show("¿Desea Ud. Copiar todos los vuelos relacionados con esta cotización?", _
                                       gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                        blnCopiarTodosVuelos = True
                    End If
                End If

                Dim objBL As New clsVentasCotizacionBT
                objBL.Copiar(dgv.CurrentRow.Cells("IDCab").Value.ToString, dgv.CurrentRow.Cells("IDUsuario").Value.ToString, _
                             dgv.CurrentRow.Cells("Fecha").Value.ToString, gstrUser, blnCopiarTodosVuelos, 0)

                MessageBox.Show("La copia de Cotización se realizó exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                Buscar()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub mnuAgregarDebitMemo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles mnuAgregarDebitMemo.Click

        'Try
        '    With frmSelDebitMemoFilesResumen
        '        .MdiParent = frmMain
        '        .strIDCab = dgv.CurrentRow.Cells("IDCab").Value.ToString
        '        .strIDFile = dgv.CurrentRow.Cells("IDFile").Value
        '        .strTitulo = dgv.CurrentRow.Cells("Titulo").Value

        '        .Show()
        '        'My.Application.DoEvents()
        '        .Activate()
        '        'frmSelDebitMemoFilesResumen.CrearListView()

        '    End With
        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try
    End Sub

    Private Sub chkRangoFechasPago_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRangoFechaInicio.CheckedChanged
        Try
            If chkRangoFechaInicio.Checked Then
                dtpFechaInicioIni.Enabled = True : dtpFechaInicioFin.Enabled = True
                dtpFechaInicioIni.Text = Date.Now.ToShortDateString : dtpFechaInicioFin.Text = Date.Now.ToShortDateString
            Else
                dtpFechaInicioIni.Enabled = False : dtpFechaInicioFin.Enabled = False
                dtpFechaInicioIni.Text = "01/01/1900" : dtpFechaInicioFin.Text = "01/01/1900"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub chkRangoFechasEmision_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRangoFechaAsigReserva.CheckedChanged
        Try
            If chkRangoFechaAsigReserva.Checked Then
                dtpFechaAsigReservIni.Enabled = True : dtpFechaAsigReservFin.Enabled = True
                dtpFechaAsigReservIni.Text = Date.Now.ToShortDateString : dtpFechaAsigReservFin.Text = Date.Now.ToShortDateString
            Else
                dtpFechaAsigReservIni.Enabled = False : dtpFechaAsigReservFin.Enabled = False
                dtpFechaAsigReservIni.Text = "01/01/1900" : dtpFechaAsigReservFin.Text = "01/01/1900"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub chkRangFecAsigOperaciones_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRangFecAsigOperaciones.CheckedChanged
        Try
            If chkRangFecAsigOperaciones.Checked Then
                dtpRango1FecAsigOper.Enabled = True : dtpRango2FecAsigOper.Enabled = True
                dtpRango1FecAsigOper.Text = Date.Now.ToShortDateString : dtpRango2FecAsigOper.Text = Date.Now.ToShortDateString
            Else
                dtpRango1FecAsigOper.Enabled = False : dtpRango2FecAsigOper.Enabled = False
                dtpRango1FecAsigOper.Text = "01/01/1900" : dtpRango2FecAsigOper.Text = "01/01/1900"
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgv_DockChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgv.DockChanged

    End Sub

    Private Sub cboUsuario_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles cboUsuario.DrawItem
        Try
            pColorear_ComboBox_Usuarios(cboUsuario, dtUsuarios_Combo, sender, e)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub chkActivos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkActivos.CheckedChanged

        Try
            CargarCombo_Usuario_Activos()
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub mnuEjecutivoVentas_Click(sender As Object, e As EventArgs) Handles mnuEjecutivoVentas.Click

        Try
            AsignarUsuarioReservasOperaciones_Lista()
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub mnuEjecutivoResOpe_Click(sender As Object, e As EventArgs) Handles mnuEjecutivoResOpe.Click
        Try
            AsignarUsuarioReservasOperaciones_Lista()
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Function lstListaFilesSelecc() As List(Of Integer)

        Try
            Dim ListaRet As New List(Of Integer)
            For Each DgvFila As DataGridViewRow In dgv.Rows
                If DgvFila.Selected Then
                    ListaRet.Add(DgvFila.Cells("IDCab").Value)
                End If
            Next
            Return ListaRet
        Catch ex As Exception
            Throw
        End Try

    End Function

    Private Sub mnuArchivarFiles_Click(sender As Object, e As EventArgs) Handles mnuArchivarFiles.Click, mnuArchivarFiles2.Click
        Try

            If MessageBox.Show("¿Está Ud. seguro de archivar los files seleccionados?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                Cursor = Cursors.WaitCursor
                Dim objBT As New clsVentasCotizacionBT
                objBT.ActualizarListaaFileHistorico(lstListaFilesSelecc(), True, gstrUser)

                btnBuscar_Click(btnBuscar, Nothing)
                Cursor = Cursors.Default
                MessageBox.Show("Los files seleccionados fueron archivados exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

        Catch ex As Exception
            Cursor = Cursors.Default
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub mnuCopiarCotizacionAPedido_Click(sender As Object, e As EventArgs) Handles mnuCopiarCotizacionAPedido.Click
        Try
            CopiarPegarCotizPedido(dgv.CurrentRow.Cells("IDCab").Value, dgv.CurrentRow.Cells("Fecha").Value)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CopiarPegarCotizPedido(ByVal vintIDCabCopia As Integer, ByVal vdatFechaCopia As Date)

        Try
            If MessageBox.Show("¿Está Ud. seguro de copiar la Cotización seleccionada a un Pedido?", _
                               gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                gintIDCabCopiaraPedido = vintIDCabCopia
                gdatFechaCopiaraPedido = vdatFechaCopia

                Dim objVuelosBN As New clsCotizacionBN.clsCotizacionVuelosBN
                Dim blnExistenVuelos As Boolean = objVuelosBN.blnExisteVuelosManuales(vintIDCabCopia)
                If blnExistenVuelos Then
                    gblnTodosVuelosCopiaraPedido = MessageBox.Show("¿Desea Ud. Copiar todos los vuelos relacionados con esta cotización?", _
                                       gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes
                End If

                MessageBox.Show("Debe ir al Pedido destino y darle click derecho Pegar para completar la copia.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                gintIDCabCopiaraPedido = 0
                gdatFechaCopiaraPedido = "01/01/1900"
                gblnTodosVuelosCopiaraPedido = False
            End If


        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub



End Class