﻿Imports System.Globalization
'Imports Microsoft.Office.Interop.Outlook
'Imports Outlook = Microsoft.Office.Interop.Outlook
'Imports Word = Microsoft.Office.Interop.Word
'Imports Excel = Microsoft.Office.Interop.Excel
Imports iTextSharp
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.Image
Imports iTextSharp.text.html
'Imports System.Drawing.Color
Imports System.IO
Imports System.Reflection
Imports ComSEToursBL2
Imports ComSEToursBE2
Imports System.Security.Cryptography
Imports System.Text

'Imports System.Configuration
'Imports System.Collections.Specialized

Module General
    Public gstrTituloMsg As String = "SETRA"
    Public gstrUser As String = "0001"
    Public gstrNivelUser As String
    Public gstrUserNombre As String
    Public gstrUsuario As String
    Public gstrUserCorreo As String
    Public gstrUserSigla As String
    Public gstrUserTelefono As String
    Public gstrUserCultureInfo As String
    Public gintUserProfile_id As Integer
    Public gintUserAccount_id As Integer
    Public gstrUserUbigeo As String
    Public gstrUserNivel As String

    Public gstrTipoProveeHoteles As String = "001"
    Public gstrTipoProveeRestaurantes As String = "002"
    Public gstrTipoProveeMuseos As String = "006"
    Public gstrTipoProveeGuias As String = "005"
    Public gstrTipoProveeAsocGuias As String = "008"
    Public gstrTipoProveeTransportista As String = "004"
    Public gstrTipoProveeOperadores As String = "003"
    Public gstrTipoProveeLineaAerea As String = "015"
    Public gstrTipoProveeTrasladista As String = "017" '"TRASL CUSCO"
    Public gstrTipoProveeTrasladistaLima As String = "020" '"TRASL LIMA"
    Public gstrTipoProveeTC As String = "021" '"Tour Conductor
    Public gstrTipoProveeAdminist As String = "019"
    Public gstrTipoProveeVehiculo As String = "VEHIC"
    Public gstrIDProvTranslivik As String = "000467"
    Public gstrDescProvTranslivik As String = "TRANSLIVIK LIMA"
    Public gstrIDProvSetoursLima As String = "001554"
    Public gstrIDProvSetoursCusco As String = "000544"
    Public gstrIDProvSetoursChile As String = "002317"
    Public gstrIDProvSetoursArgentina As String = "002315"
    Public gColorTextBoxObligat As System.Drawing.Color = System.Drawing.Color.FromArgb(194, 213, 242)
    Public gColorTextBoxModificado As System.Drawing.Color = System.Drawing.Color.SteelBlue
    Public gColorCotizEspecial As System.Drawing.Color = System.Drawing.Color.FromArgb(242, 232, 177)
    Public gColorDia1 As System.Drawing.Color = System.Drawing.Color.White
    Public gColorDia2 As System.Drawing.Color = System.Drawing.Color.LightYellow
    Public gColorSelectGrid As System.Drawing.Color = System.Drawing.Color.LightBlue
    Public gColorSelectCotiAnulada As System.Drawing.Color = System.Drawing.Color.Silver
    Public gColorSelectCotiNoAsignada As System.Drawing.Color = System.Drawing.Color.CornflowerBlue
    '"Server=192.168.30.50;DataBase=BDSETRA;Integrated Security=SSPI;"
    Public gstrPeru As String = "000323" '"000049"
    Public gstrBolivia As String = "000007"
    Public gstrChile As String = "000011"
    Public gstrArgentina As String = "000003"
    Public gstrLima As String = "000065"
    Public gstrNasca As String = "000070"
    Public gstrParacas As String = "000071"
    Public gstrCusco As String = "000066"
    Public gstrBuenosAires As String = "000113"
    Public gstrSantiago As String = "000110"

    Public gstrIngles As String = "ENGLISH"
    Public gstrEspaniol As String = "SPANISH"
    Public gstrFormaPagoCredito As String = "001"
    Public gstrFormaPagoPrepago As String = "002"
    Public gstrFormaPagoTransferencia As String = "005"
    Public gstrFormaPorDefinir As String = "003"
    Public gstrFormaPagoLiquidMensual As String = "010"
    Public gstrTipoMonedaSol As String = "SOL"
    Public gstrTipoMonedaDol As String = "USD"
    Public gstrTipoMonedaEur As String = "EUR"
    Public gstrTipoMonedaChi As String = "CLP"
    Public gstrTipoMonedaArg As String = "ARS"


    Public gstrTipoOperacContabExportac As String = "001"
    Public gstrTipoOperacContabGravado As String = "002"
    Public gstrTipoOperacContabNoGravado As String = "003"
    Public gstrTipoOperacContabDocCobranza As String = "004"
    Public gstrTipoOperacContabGastosTransferencia As String = "005"

    Public gstrTipoDocIdentPasaporte As String = "004"
    Public gstrTipoDocFactura As String = "FAC"
    Public gstrBancoBCP As String = "001"

    Public gsglIGV As Single = 18

    Public gstrEmpresa As String '= "SETOURS S.A."
    Public gstrRuc As String '= "20100939887"
    Public gstrDireccion As String '= "Av. Comandante Espinar 229 - Miraflores - LIMA 18 - PERU"
    Public gstrRazonSocial As String '= "SERVICIOS TURISTICOS GENERALES S.A."
    Public gstrTelefonoEmpresa As String '= "(511)202-4620"
    Public gstrFaxEmpresa As String '= "(511) 202-4630"
    Public gstrEmailEmpresa As String '= "setours@setours.com"
    Public gstrWebEmpresa As String '= "www.setours.com"
    Public gstrRutaImagenes As String '= "\\192.168.30.50\publica\ProyectoSETours\ImagenesyPlantillas\"
    Public gstrRutaWord As String '= "\\192.168.30.10\documentos\VENTAS\Fotos\baja resolución\Peru\"
    Public gstrRutaPDF As String '= "\\192.168.30.50\publica\ProyectoSETours\Archivos Pdf\"
    'Public gstrRutaExcels As String = "\\192.168.30.50\publica\ProyectoSETours\Archivos Excel\"
    'Public gstrRutaVouchers As String = "\\192.168.30.50\publica\ProyectoSETours\Vouchers\"

    'Public gstrRutaImagenesCorreoMarketing As String = "\\192.168.30.50\publica\ProyectoSETours\ImagenesyPlantillas\CorreoMarketing\"
    'Public gstrRutaImagenesCorreoMarketingenServer As String = "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\ImagenesyPlantillas\CorreoMarketing\"
    Public gstrRutaImagenesCorreoMarketing As String '= "\\192.168.30.6\setra\CorreoMarketing\"
    Public gstrRutaImagenesCorreoMarketingenServer As String '= "D:\SETRA\CorreoMarketing\"
    Public gstrRutaVouchers As String
    Public gstrRutaPstCorreosFiles As String
    Public gstrRutaDocsStatusReservas As String
    Public gstrRutaDocumentosPreLiquidacion As String
    Public gstrRutaAdjuntosxPerfilCorreoSQL As String '= "\\192.168.30.50\publica\ProyectoSETours\CorreoSQL\"
    Public gstrRutaAdjuntosxPerfilCorreoSQLServer As String '= "D:\Dpto Desarrollo\PUBLICA\ProyectoSETours\CorreoSQL\"
    Public gstrServerCorreo As String '= "192.168.2.2"
    Public gstrRutaAdjuntosOrdenesPago As String '= "\\10.10.10.206\publica\ProyectoSETours\ImagenesyPlantillas\Ordenes de Pago\"
    Public gdblRHLimiteSinRetencion As Double
    Public gstrIDClienteVtasAdicionalesAPT As String
    Public gstrIDClienteAPT As String = "000054"
    Public gstrNoRutaUserPhoto As String
    'Public gstrRutaImagenesCorreoMarketingenServerWeb As String = "http://setours.com/images/"

    'Public gstrCorreoAdministPrepagos As String = "pagos3@setours.com"
    'Public gstrCorreoAdministPrepagosOperadInternac As String = "cristhian.arambulo@setours.com"

    'Niveles
    Public gstrNivelEjecutivoVenta As String = "VTA"
    Public gstrNivelEjecutivoReserva As String = "RES"
    Public gstrNivelEjecutivoOperacion As String = "OPE"
    Public gstrNivelSupervisorReserva As String = "SRE"
    Public gstrNivelSupervisorVentas As String = "SVE"
    Public gstrNivelSupervisorOperacion As String = "SOP"
    Public gstrNivelSupervisorCounter As String = "SCO"
    Public gstrNivelSupervisorFinanzas As String = "SFI"
    Public gstrNivelEjecutivoCounter As String = "COU"
    Public gstrNivelEjecutivoFinanzas As String = "EFI"
    Public gstrNivelGerencia As String = "GER"

    'Areas
    Public gstrAreaAdministración As String = "AD"
    Public gstrAreaCounter As String = "CO"
    Public gstrAreaFinanzas As String = "FI"
    Public gstrAreaGerencia As String = "GR"
    Public gstrAreaMarketing As String = "MK"
    Public gstrAreaOperaciones As String = "OP"
    Public gstrAreaReservas As String = "RS"
    Public gstrAreaSistemas As String = "SI"
    Public gstrAreaTarifas As String = "TR"
    Public gstrAreaVentas As String = "VS"


    Public DgvCopia As DataGridView = Nothing
    Public ListFilasCopia As New List(Of Int16)

    Public DgvCopia_V As DataGridView = Nothing
    Public ListFilasCopia_V As New List(Of Int16)

    Public DgvVueloCopia As DataGridView = Nothing
    Public DgvBusCopia As DataGridView = Nothing
    Public DgvTrenCopia As DataGridView = Nothing

    Private strTipoLetraVoucher As String = "Century Gothic"
    Public gsglTipoCambio As Single = 2.7
    'Public gstrIDServicioTC As String = "LIM00906"
    Public gstrIDServicioBTGCompleto As String = "CUZ00179"
    'jorge
    Public dtComboUsuarios_Cotizacion As DataTable
    Public gstrtipoCuentaExterior As String = "EX"

    Public gstrIDTipoOperInterno As String = "I"
    Public gstrIDTipoOperExterno As String = "E"
    Public gstrIDTipoHonorarioAPT As String = "002"

    Public gstrIDClienteGEBECO As String = "000160"
    Public gintSerieAufdenSpuren As Integer = 14
    Public gstrIDServicioNTransfer As String = "LIM00102"
    Public gstrIDServicioCostoTC As String = "LIM00918"
    Public gstrIDProveedorZicasso As String = "003557"
    Public gstrIDServicioProveedorZicasso As String = "LAX00002"

    'Public gstrPuertoSAP As String = ConfigurationSettings.AppSettings("PuertoSAP")

    Private des As New TripleDESCryptoServiceProvider 'Algorithmo TripleDES
    Private hashmd5 As New MD5CryptoServiceProvider 'objeto md5
    Private myKey As String = "MyKey2012" 'Clave secreta(puede alterarse)

    Public gstrRutaArchivos_File As String = "" '"\\dc1\Publica\GENERAL\FILES\"
    Public gstrCaracteresEspeciales As String = "\/:*?""<>|"
    Public Structure stColumnaNumeroFormato
        Dim strColumna As String
        Dim strFormato As String
    End Structure

    Structure stTipoCambioMonedas
        Dim IDCab As Integer
        Dim CoMoneda As String
        Dim SsTipCam As Double
        Dim AplicaTCam As Boolean
        Dim Accion As String
    End Structure

    Public gstrIDServicioMP As String = "CUZ00132"
    Public gstrIDServicioMPWP As String = "CUZ00133"
    Public gstrIDServicioWP As String = "CUZ00134"
    Public gstrIDServicioRQ As String = "CUZ00145"
    Public gstrIDServicioMPMonta As String = "CUZ00905"
    Public gstrIDServicioMPMuseo As String = "CUZ00912"
    Public gstrIDServicioMPMSupp As String = "CUZ00946"

    Public gstrIDServicioTipsHotelLima As String = "LIM00905"
    Public gstrIDServicioTipsAirportLima As String = "LIM00392"
    Public gstrIDServicioTipsHotelCusco As String = "CUZ00946"
    Public gstrIDServicioTipsAirportCusco As String = "CUZ00416"

    Public bytContNroPantPresupPvw As Byte = 1

    Public Sub pClearControls(ByVal nForm As Form)
        With nForm
            For Each ctrl As Object In .Controls
                'If ctrl.Tag = "Borrar" And TypeOf ctrl Is System.Windows.Forms.TextBox Then
                'If ctrl.Tag = "Borrar" And ctrl.Name.Substring(0, 3) = "txt" Then
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.TextBox) Then
                    ctrl.Text = String.Empty
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.Label) Then
                    ctrl.Text = String.Empty
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.ComboBox) Then
                    ctrl.SelectedIndex = -1
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                    ctrl.Checked = False
                End If


                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                    pClearGroups(ctrl)
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                    pClearTabs(ctrl)
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                    pClearSplitCont(ctrl)
                End If


            Next
        End With
    End Sub

    Public Function strDevuelveSumaSsTotalDocumUSD(ByVal vdtt As DataTable) As String
        Dim strReturn As String = ""
        Try
            Dim dblTotal As Double = 0.0
            For Each dRowItem As DataRow In vdtt.Rows
                dblTotal += Convert.ToDouble(dRowItem("SsTotal"))
            Next
            Dim strMoneda As String = vdtt(0)("DescMoneda")

            If dblTotal < 0 Then dblTotal *= -1
            If dblTotal = 0.0 Then
                'strReturn = "0 Y 00/100 DÓLARES AMERICANOS"
                strReturn = "0 Y 00/100 " & strMoneda
            Else
                'strReturn = strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 DÓLARES AMERICANOS"
                strReturn = strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 " & strMoneda
            End If
            Return strReturn
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function pDTStringToDateFormat(ByVal vstrDate As String, ByVal vstrFormat As String, ByVal vstrFormatCulturure As String) As DateTime
        Dim dtfecha As DateTime = DateTime.MinValue
        Try
            dtfecha = DateTime.ParseExact(vstrDate, vstrFormat, CultureInfo.CreateSpecificCulture(vstrFormatCulturure))
            Return dtfecha
        Catch ex As Exception
            Return dtfecha
        End Try
    End Function

    Public Function pValidarStringToDateFormat(ByVal vstrDate As String, ByVal vstrFormat As String, ByVal vstrFormatCulturure As String) As Boolean
        Try
            Dim dtfecha As DateTime = DateTime.ParseExact(vstrDate, vstrFormat, CultureInfo.CreateSpecificCulture(vstrFormatCulturure))
            Return True
        Catch ex As Exception
            Return False
        End Try
    End Function

    'jorge: funcion para colorear combos
    Public Sub pColorear_ComboBox_Usuarios(ByVal combo As ComboBox, ByVal dt As DataTable, ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs)
        Try
            If e.Index = -1 Then
                Exit Sub
            End If
            'Dim combo As ComboBox = TryCast(sender, ComboBox)
            combo.DropDownStyle = ComboBoxStyle.DropDownList
            If combo.Items.Count > 0 Then
                'If intContadorCombos <= cboUsuario.Items.Count Then
                Dim myFont As System.Drawing.Font = combo.Font


                Dim rectangle As Rectangle = New Rectangle(0, 0, _
                    e.Bounds.Width, e.Bounds.Height - 4)

                Dim indice As Integer = e.Index

                Dim estado As String = dt.Rows(indice).Item("Activo").ToString
                Dim nombre As String = dt.Rows(indice).Item(1).ToString
                e.DrawBackground()




                If estado = "I" Then
                    e.Graphics.DrawString(dt.Rows(indice).Item(1).ToString, myFont, System.Drawing.Brushes.Gray, _
                                            New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))

                Else
                    If (e.State And DrawItemState.Selected) = DrawItemState.Selected Then
                        e.Graphics.DrawString(dt.Rows(indice).Item(1).ToString, myFont, System.Drawing.Brushes.White, _
                                           New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
                    Else
                        e.Graphics.DrawString(dt.Rows(indice).Item(1).ToString, myFont, System.Drawing.Brushes.Black, _
                                           New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
                    End If


                End If


                e.DrawFocusRectangle()


                'If (e.State And DrawItemState.HotLight) = DrawItemState.HotLight Then
                '    e.Graphics.DrawString(dt.Rows(indice).Item(1).ToString, myFont, System.Drawing.Brushes.White, _
                '                     New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
                'Else
                '    e.Graphics.DrawString(dt.Rows(indice).Item(1).ToString, myFont, System.Drawing.Brushes.Black, _
                '                    New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
                'End If

            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub cboUsuario_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs)
        'Try
        '    If e.Index = -1 Then
        '        Exit Sub
        '    End If
        '    Dim combo As ComboBox = TryCast(sender, ComboBox)

        '    If combo.Items.Count > 0 Then
        '        'If intContadorCombos <= cboUsuario.Items.Count Then
        '        Dim myFont As System.Drawing.Font = combo.Font


        '        Dim rectangle As Rectangle = New Rectangle(0, 0, _
        '            e.Bounds.Width, e.Bounds.Height - 4)

        '        Dim indice As Integer = e.Index

        '        Dim estado As String = dtUsuarios_Combo.Rows(indice).Item("Activo").ToString
        '        Dim nombre As String = dtUsuarios_Combo.Rows(indice).Item(1).ToString
        '        e.DrawBackground()
        '        If estado = "I" Then
        '            e.Graphics.DrawString(dtUsuarios_Combo.Rows(indice).Item(1).ToString, myFont, System.Drawing.Brushes.Gray, _
        '                                    New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
        '        Else
        '            e.Graphics.DrawString(dtUsuarios_Combo.Rows(indice).Item(1).ToString, myFont, System.Drawing.Brushes.Black, _
        '                                    New RectangleF(e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height))
        '        End If

        '        'If e.State = DrawItemState.Focus Then
        '        ' If e.State = DrawItemState.HotLight Then
        '        If (e.State And DrawItemState.HotLight) = DrawItemState.HotLight Then
        '            e.Graphics.FillRectangle(Brushes.Blue, e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height)
        '        End If


        '        'e.Graphics.DrawString(ComboBox1.Items(index).ToString(), e.Font, TextBrush, e.Bounds, StringFormat.GenericDefault)
        '        e.DrawFocusRectangle()



        '    End If

        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Public Sub pClearGroups(ByVal cGroup As GroupBox)
        With cGroup
            For Each ctrl As Object In .Controls
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.TextBox) Then
                    ctrl.Text = String.Empty

                    'ElseIf ctrl.Tag = "Borrar" And TypeOf ctrl Is ComboBox Then
                    '    ctrl.listindex = -1
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.Label) Then
                    ctrl.Text = String.Empty
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.ComboBox) Then
                    ctrl.SelectedIndex = -1
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                    ctrl.Checked = False
                End If

            Next
        End With
    End Sub
    Public Sub pClearTabs(ByVal cTab As TabControl)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                    pClearTabPages(ctrl)
                End If
            Next
        End With
    End Sub
    Public Sub pClearTabPages(ByVal cTab As TabPage)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                    pClearGroups(ctrl)
                End If
            Next
        End With
    End Sub


    Public Sub pClearSplitCont(ByVal cTab As SplitContainer)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.GetType Is GetType(System.Windows.Forms.SplitterPanel) Then
                    pClearSplitContPanel(ctrl)
                End If
            Next
        End With
    End Sub
    Public Sub pClearSplitContPanel(ByVal cTab As SplitterPanel)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.TextBox) Then
                    ctrl.Text = String.Empty

                    'ElseIf ctrl.Tag = "Borrar" And TypeOf ctrl Is ComboBox Then
                    '    ctrl.listindex = -1
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.Label) Then
                    ctrl.Text = String.Empty
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.ComboBox) Then
                    ctrl.SelectedIndex = -1
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                    ctrl.Checked = False
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                    pClearGroups(ctrl)
                End If
            Next
        End With
    End Sub
    Public Sub pEnableSplitContPanel(ByVal cTab As SplitterPanel, ByVal vblnEnabled As Boolean)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.TextBox) Then
                    ctrl.Enabled = vblnEnabled
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.Label) Then
                    ctrl.Enabled = vblnEnabled
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.ComboBox) Then
                    ctrl.Enabled = vblnEnabled
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                    ctrl.Enabled = vblnEnabled
                End If
            Next
        End With
    End Sub

    Public Sub pEnableTabPages(ByVal cTab As TabPage, ByVal vblnEnabled As Boolean)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.TextBox) Then
                    ctrl.Enabled = vblnEnabled
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.Label) Then
                    ctrl.Enabled = vblnEnabled
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.ComboBox) Then
                    ctrl.Enabled = vblnEnabled
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                    ctrl.Enabled = vblnEnabled
                End If
                If ctrl.Tag = "Borrar" And ctrl.GetType Is GetType(System.Windows.Forms.DateTimePicker) Then
                    ctrl.Enabled = vblnEnabled
                End If

            Next
        End With
    End Sub
    Public Sub pEnableControls(ByVal nForm As Form, ByVal vEnable As Boolean)
        With nForm
            For Each ctrl As Object In .Controls
                If ctrl.Tag = "Borrar" And ctrl.Name.Substring(0, 3) <> "tab" Then
                    ctrl.Enabled = vEnable
                End If
            Next
        End With
    End Sub
    Public Sub pEnableControlsGroupBox(ByVal cGroup As GroupBox, ByVal vEnable As Boolean)
        With cGroup
            For Each ctrl As Object In .Controls
                If ctrl.Tag = "Borrar" And ctrl.Name.Substring(0, 3) <> "tab" Then
                    ctrl.Enabled = vEnable
                End If
            Next
        End With
    End Sub

    Public Sub pVisibleControls(ByVal cGroup As GroupBox, ByVal vblnVisible As Boolean)
        With cGroup
            For Each cntrl As Control In .Controls
                If cntrl.Tag = "Borrar" And cntrl.Name.Substring(0, 3) <> "tab" Then
                    cntrl.Visible = vblnVisible
                End If
            Next
        End With
    End Sub

    Public Sub pCargaCombosBox(ByVal vCbo As ComboBox, ByVal vDt As DataTable, Optional ByVal vblnTodos As Boolean = False)
        Try
            With vCbo
                .DataSource = vDt
                .DisplayMember = vDt.Columns(1).ToString
                .ValueMember = vDt.Columns(0).ToString
                If .Items.Count > 0 Then
                    If Not vblnTodos Then
                        .SelectedIndex = -1
                    Else
                        .SelectedIndex = 0
                    End If
                End If
            End With

        Catch ex As System.Exception
            Throw

        End Try
    End Sub

    Public Function pEncriptarLogIn(ByVal texto As String) As String

        If Trim(texto) = "" Then
            pEncriptarLogIn = ""
        Else
            des.Key = hashmd5.ComputeHash((New UnicodeEncoding).GetBytes(myKey))
            des.Mode = CipherMode.ECB
            Dim encrypt As ICryptoTransform = des.CreateEncryptor()
            Dim buff() As Byte = UnicodeEncoding.ASCII.GetBytes(texto)
            pEncriptarLogIn = Convert.ToBase64String(encrypt.TransformFinalBlock(buff, 0, buff.Length))
        End If
        Return pEncriptarLogIn
    End Function

    Public Function pDesencriptarLogIn(ByVal texto As String) As String
        If Trim(texto) = "" Then
            pDesencriptarLogIn = ""
        Else
            des.Key = hashmd5.ComputeHash((New UnicodeEncoding).GetBytes(myKey))
            des.Mode = CipherMode.ECB
            Dim desencrypta As ICryptoTransform = des.CreateDecryptor()
            Dim buff() As Byte = Convert.FromBase64String(texto)
            pDesencriptarLogIn = UnicodeEncoding.ASCII.GetString(desencrypta.TransformFinalBlock(buff, 0, buff.Length))
        End If
        Return pDesencriptarLogIn
    End Function


    Public Sub pCargaListView(ByVal vLvw As ListView, ByVal vDt As DataTable, ByVal vbytCols As Byte, ByVal vbytImg As Byte, Optional ByVal blnClear As Boolean = True)

        Dim lvItem As New ListViewItem
        If blnClear Then
            vLvw.Items.Clear()
        End If


        For Each dr As DataRow In vDt.Rows
            lvItem = vLvw.Items.Add(dr(0).ToString, vbytImg)
            For i As Byte = 1 To vbytCols
                lvItem.SubItems.Add(If(IsDBNull(dr(i)) = True, "", dr(i)))

            Next
        Next
    End Sub

    Public Sub pCargaListView2(ByVal vLvw As ListView, ByVal vDt As DataTable, ByVal vbytImg As Byte)

        Dim lvItem As ListViewItem
        vLvw.Items.Clear()

        For Each dr As DataRow In vDt.Rows
            lvItem = vLvw.Items.Add(dr(0), dr(1), vbytImg)

        Next
    End Sub

    Public Sub pDgvAnchoColumnas(ByVal vDgv As DataGridView)
        'Dim intLength As Int32, intLengthMax As Int32

        'If vDgv.RowCount = 0 Then Exit Sub

        'For Each dgvCol As DataGridViewColumn In vDgv.Columns
        '    If dgvCol.CellType.Name = "DataGridViewTextBoxCell" Then
        '        intLengthMax = 0
        '        For Each dgvRow As DataGridViewRow In vDgv.Rows
        '            If Not dgvRow.Cells(dgvCol.Name.ToString).Value Is Nothing Then
        '                intLength = dgvRow.Cells(dgvCol.Name.ToString).Value.ToString.Trim.Length
        '            Else
        '                intLength = 0
        '            End If
        '            If intLength > intLengthMax Then
        '                intLengthMax = intLength
        '            End If
        '        Next

        '        If dgvCol.HeaderText.Length > intLengthMax Then
        '            intLengthMax = dgvCol.HeaderText.Length + 1
        '        End If

        '        If dgvCol.HeaderText = "Año" Then
        '            dgvCol.Width = 40
        '        ElseIf InStr(dgvCol.HeaderText, "Fec") > 0 Then
        '            dgvCol.Width = 70
        '        Else
        '            If intLengthMax * 7.7 <= 65536 Then
        '                dgvCol.Width = intLengthMax * 7.7
        '            Else
        '                dgvCol.Width = 65536
        '            End If

        '        End If

        '    End If
        'Next
        For Each dgvCol As DataGridViewColumn In vDgv.Columns
            'MessageBox.Show(dgvCol.CellType.ToString)
            'If dgvCol.CellType.ToString = "System.Windows.Forms.DataGridViewCheckBoxCell" Then
            '    GoTo Sgte
            'End If

            If dgvCol.Tag = "1" Then 'Contenido de las celdas mandatorio
                If vDgv.Rows.Count > 0 Then
                    'Calculando el Máximo
                    Dim intLength As Int32, intLengthMax As Int32
                    intLengthMax = 0
                    For Each dgvRow As DataGridViewRow In vDgv.Rows
                        If Not dgvRow.Cells(dgvCol.Name.ToString).Value Is Nothing Then
                            intLength = dgvRow.Cells(dgvCol.Name.ToString).Value.ToString.Trim.Length
                        Else
                            intLength = 0
                        End If
                        If intLength > intLengthMax Then
                            intLengthMax = intLength
                        End If
                    Next
                    'dgvCol.Width = vDgv.Item(dgvCol.Index, 0).Value.ToString.Trim.Length * 8.5
                    If intLengthMax > 50 Then
                        dgvCol.Width = intLengthMax * 6.2
                    Else
                        If intLengthMax < 30 Then
                            If intLengthMax < 15 Then
                                dgvCol.Width = intLengthMax * 7.5
                            Else
                                dgvCol.Width = intLengthMax * 8.5
                            End If

                        Else
                            dgvCol.Width = intLengthMax * 6.5
                        End If

                    End If
                    'dgvCol.Width = intLengthMax * 7.5
                End If

            Else 'Titulo mandatorio
                'dgvCol.Width = dgvCol.HeaderText.ToString.Trim.Length * 8

                Dim strTitulo As String = dgvCol.HeaderText.ToString.Trim
                Dim strPalabra As String = ""
                Dim lstTit As New List(Of String)
                For Each chrChar As Char In strTitulo
                    strPalabra += chrChar
                    If chrChar = " " Then
                        lstTit.Add(strPalabra.Trim)
                        strPalabra = ""
                    End If
                Next
                If strPalabra.Trim <> "" Then
                    lstTit.Add(strPalabra.Trim)
                End If

                Dim strPalabraMayor As String = ""
                If lstTit.Count > 0 Then
                    For Each strPalabraLista As String In lstTit
                        If strPalabraLista.Length > strPalabraMayor.Length Then
                            strPalabraMayor = strPalabraLista
                        End If
                    Next
                Else
                    strPalabraMayor = dgvCol.HeaderText.ToString.Trim
                End If
                dgvCol.Width = strPalabraMayor.Length * 12
            End If
            'Sgte:
        Next
    End Sub

    Public Function blnDGVFilaVacia(ByVal vDgv As DataGridView, Optional ByVal vintFila As Int16 = -1, _
                                    Optional ByVal vbytColIni As Byte = 0) As Boolean
        Dim blnVacia As Boolean = True
        If vbytColIni = 0 Then
            If vintFila = -1 Then
                For Each DgvCol As DataGridViewColumn In vDgv.Columns
                    If vDgv.CurrentCell Is Nothing Then GoTo Sgte
                    If vDgv.Item(DgvCol.Index, vDgv.CurrentCell.RowIndex).Value Is Nothing Then GoTo Sgte
                    If vDgv.Item(DgvCol.Index, vDgv.CurrentCell.RowIndex).Value.ToString.Trim <> "" Then
                        blnVacia = False
                        Exit For
                    End If
Sgte:
                Next
            Else
                For Each DgvCol As DataGridViewColumn In vDgv.Columns
                    ''If vDgv.CurrentCell Is Nothing Then GoTo Sgte2
                    If vDgv.Item(DgvCol.Index, vintFila).Value Is Nothing Then GoTo Sgte2
                    If vDgv.Item(DgvCol.Index, vintFila).Value.ToString.Trim <> "" Then
                        blnVacia = False
                        Exit For
                    End If

Sgte2:
                Next

            End If
        Else

            For bytDgvCol As Byte = vbytColIni To vDgv.Columns.Count - 1

                If vDgv.CurrentCell Is Nothing Then GoTo Sgte3
                If vDgv.Item(bytDgvCol, vDgv.CurrentCell.RowIndex).Value Is Nothing Then GoTo Sgte3
                If vDgv.Item(bytDgvCol, vDgv.CurrentCell.RowIndex).Value.ToString.Trim <> "" Then
                    blnVacia = False
                    Exit For
                End If
Sgte3:
            Next

        End If
        Return blnVacia
    End Function


    Public Structure stDetCotiQuiebres
        Dim IDCAB As Integer
        Dim IDDet As String
        Dim IDCab_Q As Int32
        Dim IDDet_Q As String
        Dim CostoReal As Double
        Dim CostoLiberado As Double
        Dim Margen As Double
        Dim MargenAplicado As Double
        Dim MargenLiberado As Double
        Dim Total As Double
        Dim RedondeoTotal As Double
        Dim SSCR As Double
        Dim SSCL As Double
        Dim SSMargen As Double
        Dim SSMargenLiberado As Double
        Dim SSTotal As Double
        Dim STCR As Double
        Dim STMargen As Double
        Dim STTotal As Double
        Dim CostoRealImpto As Double
        Dim CostoLiberadoImpto As Double
        Dim MargenImpto As Double
        Dim MargenLiberadoImpto As Double
        Dim SSCRImpto As Double
        Dim SSCLImpto As Double
        Dim SSMargenImpto As Double
        Dim SSMargenLiberadoImpto As Double
        Dim STCRImpto As Double
        Dim STMargenImpto As Double
        Dim TotImpto As Double
        Dim Accion As String
    End Structure
    Public objLstDetCotiQuiebres As New List(Of stDetCotiQuiebres)

    Public Structure stDetCotiPax
        Dim IDCAB As Integer
        Dim IDDet As String
        Dim IDPax As String
        Dim IDTipoProv As String
        Dim Selecc As Boolean
        Dim Accion As Char
    End Structure
    Public objLstDetCotiPax As New List(Of stDetCotiPax)

    Public Structure stDetReservPax
        Dim IDCab As Integer
        Dim IDReserva_Det As String
        Dim IDPax As String
        Dim IDTipoProv As String
        Dim Selecc As Boolean
        Dim Accion As Char
    End Structure
    Public objLstDetReservPax As New List(Of stDetReservPax)

    Public Structure stAcomodoPax
        Dim IDReserva As String
        Dim IDPax As Integer
        Dim CapacidadHab As Short
        Dim IDCAB As Integer
        Dim IDHabit As Int16
        Dim EsMatrimonial As Boolean
        Dim UserMod As String
        Dim Accion As String
    End Structure
    Public objLstAcomodoPax As New List(Of stAcomodoPax)
    Public objLstAcomodoPaxProveedor As New List(Of stAcomodoPax)

    Public Structure stTareasReservas
        Dim IDReserva As String
        Dim IDTarea As String
        Dim FecDeadLine As Date
        Dim IDEstado As String
        Dim DescEstado As String
        Dim Descripcion As String
        Dim DescripcionTipo As String
        Dim IDCab As Integer
        Dim Accion As String
    End Structure
    Public objLstTareasReservas As New List(Of stTareasReservas)

    Public Structure stDetOperacPax
        Dim IDCab As Integer
        Dim IdOperacion As String
        Dim IDOperacion_Det As String
        Dim IDPax As String
        Dim Selecc As Boolean
        Dim Accion As Char
    End Structure
    Public objLstDetOperacPax As New List(Of stDetOperacPax)

    Public Structure stCorreosProveedorTmp
        Dim IDProveedor As String
        Dim Correo As String
        Dim Descripcion As String
        Dim Estado As String
        Dim Accion As String
    End Structure
    Public objLstCorreosProveedorTmp As New List(Of stCorreosProveedorTmp)

    Public Structure stAcodomodoEspecial
        Dim IDCab As Integer
        Dim IDDet As String
        Dim CoCapacidad As String
        Dim QtPax As Int16
        Dim Almacenado As Char
        Dim Accion As Char
    End Structure
    Public objLstAcodomodoEspecial As New List(Of stAcodomodoEspecial)

    Public Structure stAcodomodoEspecialDistribucion
        Dim IDCab As Integer
        Dim IDDet As String
        Dim CoCapacidad As String
        Dim IDPax As Integer
        Dim IDHabit As Int16
        Dim UserMod As String
        Dim Accion As String
    End Structure
    Public objLstAcodomodoEspecialDistribucion As New List(Of stAcodomodoEspecialDistribucion)

    Public Structure stSincerado
        Dim NuSincerado As String
        Dim IDProveedor As String
        Dim DescProveedor As String
        Dim IDServicio As String
        Dim IDServicio_Det As Integer
        Dim SsCostoNeto As Double
        Dim SsCostoIgv As Double
        Dim SsCostoTotal As Double
        Dim SsMargenCoti As Double
        Dim CoEstadoSincerado As String
        Dim IDOperacion_Det As Integer
        Dim IDCab As Integer
        Dim strAccion As String
    End Structure
    Public objLstSincerado As New List(Of stSincerado)
    Public Structure stSincerado_Det
        Dim NuSincerado As String
        Dim NuSincerado_Det As String
        Dim IDServicio_Det As Integer
        Dim QtPax As Byte
        Dim QtPaxLiberados As Byte
        Dim SsCostoNeto As Double
        Dim SsCostoIgv As Double
        Dim SsCostoTotal As Double
        Dim IDCab As Integer
        Dim blnSelect As Boolean
        Dim strAccion As String
    End Structure
    Public objLstSincerado_Det As New List(Of stSincerado_Det)
    Public Structure stSincerado_Det_Pax
        Dim NuSincerado_Det_Pax As String
        Dim NuSincerado_Det As String
        Dim NuSincerado As String
        Dim IDPax As Integer
        Dim IDCab As Integer
        Dim blnSelect As Boolean
        Dim strAccion As String
    End Structure
    Public objLstSincerado_Det_Pax As New List(Of stSincerado_Det_Pax)

    Public Sub CargarobjLstAcodomodoEspecial(ByVal strIDDet As String)

        Try
            If Not IsNumeric(strIDDet) Then Exit Sub

            Dim objBNDet As New clsCotizacionBN.clsDetalleCotizacionBN.clsDetalleCotizacionAcomodoEspecialBN
            Dim dt As DataTable = objBNDet.ConsultarListxIDDet(CInt(strIDDet))

            Dim oAcomodoEspecial As stAcodomodoEspecial
            If dt.Rows.Count > 0 Then
                objLstAcodomodoEspecial.Clear()

                For Each Dr As DataRow In dt.Rows
                    oAcomodoEspecial = New stAcodomodoEspecial
                    oAcomodoEspecial.IDCab = Dr("IDCAB")
                    oAcomodoEspecial.IDDet = CInt(strIDDet)
                    oAcomodoEspecial.QtPax = If(IsDBNull(Dr("QtPax")), 0, Dr("QtPax"))
                    oAcomodoEspecial.CoCapacidad = If(IsDBNull(Dr("CoCapacidad")), "", Dr("CoCapacidad"))
                    oAcomodoEspecial.Almacenado = "S"

                    objLstAcodomodoEspecial.Add(oAcomodoEspecial)
                Next
            Else
                For int_vContador As Int16 = 0 To objLstAcodomodoEspecial.Count - 1
                    With objLstAcodomodoEspecial(int_vContador)
                        If CInt(strIDDet) <> .IDDet Then
                            objLstAcodomodoEspecial.Clear()
                            Exit For
                        End If
                    End With
                Next
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function blnActualizarCorreo(ByRef vstrCorreo As String) As Boolean
        Try
            Dim blnExiste As Boolean = False
            For Each ST As stCorreosProveedorTmp In objLstCorreosProveedorTmp
                If bytCantidadCorreosCadena(ST.Correo) = 1 Then
                    If ST.Correo.ToLower.Trim = vstrCorreo.ToLower.Trim Then
                        blnExiste = True
                        Exit For
                    ElseIf InStr(vstrCorreo.ToLower.Trim, ST.Correo.ToLower.Trim) > 0 Then
                        vstrCorreo = Replace(vstrCorreo, ST.Correo, "").Trim
                        vstrCorreo = Replace(vstrCorreo, ";", "").Trim
                        Exit For
                    End If
                End If
            Next
FinFor:
            Return blnExiste
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function blnIgualdadInformacionDataTable(ByVal vDataTable1 As Data.DataTable, ByVal vDataTable2 As Data.DataTable)
        If vDataTable1.Rows.Count <> vDataTable2.Rows.Count Then Return False
        If vDataTable1.Columns.Count <> vDataTable2.Columns.Count Then Return False
        Try
            Dim blnIgualdad As Boolean = True
            Dim intNroColumnas As Int16 = vDataTable1.Columns.Count
            Dim intIndex As Int16 = 0
            For Each DTRow As Data.DataRow In vDataTable1.Rows
                For iCol As Int16 = 0 To intNroColumnas - 1
                    If DTRow(iCol).ToString <> vDataTable2(intIndex)(iCol).ToString Then
                        blnIgualdad = False
                        Exit For
                    End If
                Next
                intIndex += 1
            Next

            Return blnIgualdad
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function bytCantidadCorreosCadena(ByVal vstrCorreosCopias As String) As Byte
        Try

            Dim bytCantidad As Byte = 0
            If vstrCorreosCopias.Trim = String.Empty Then Return 0

            Dim strCorreo As String = ""
            If vstrCorreosCopias.Substring(vstrCorreosCopias.Length - 1) <> ";" Then vstrCorreosCopias += ";"
            Dim chrArr() As Char = vstrCorreosCopias.ToCharArray
            For i = 0 To chrArr.Length - 1
                If chrArr(i) <> ";" Then
                    strCorreo += chrArr(i)
                Else
                    If blnValidarFormatoMail(strCorreo) Then
                        bytCantidad += 1
                    End If
                End If
            Next

            Return bytCantidad

        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Sub BorrarSTTareasReservasxIDReserva(ByVal vstrIDReserva As String)
        If objLstTareasReservas Is Nothing Then Exit Sub
Borrar:
        For Each ST As stTareasReservas In objLstTareasReservas
            If ST.IDReserva = vstrIDReserva Then
                objLstTareasReservas.Remove(ST)
                GoTo Borrar
            End If
        Next

    End Sub

    Public Sub BorrarSTTareasReservasxIDCab(ByVal vintIDCab As Integer)
        If objLstTareasReservas Is Nothing Then Exit Sub
Borrar:
        For Each ST As stTareasReservas In objLstTareasReservas
            If ST.IDCab = vintIDCab Then
                objLstTareasReservas.Remove(ST)
                GoTo Borrar
            End If
        Next

    End Sub

    Public Sub BorrarSTDetCotiQuiebresxIDDet(ByVal vstrIDDet As String)
        If objLstDetCotiQuiebres Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stDetCotiQuiebres In objLstDetCotiQuiebres
            If ST.IDDet = vstrIDDet Then
                objLstDetCotiQuiebres.RemoveAt(intInd)
                GoTo Borrar
            End If
            intInd += 1
        Next
    End Sub

    Public Sub BorrarSTDetPaxxIDCab(ByVal vstrIDCab As String, Optional ByVal vstrIddet As String = "")
        If vstrIddet <> "" Then
            If objLstDetCotiPax Is Nothing Then Exit Sub
Borrar2:
            Dim intInd As Int16 = 0
            For Each ST As stDetCotiPax In objLstDetCotiPax
                If ST.IDCAB = vstrIDCab And ST.IDDet = vstrIddet Then
                    objLstDetCotiPax.RemoveAt(intInd)
                    GoTo Borrar2
                End If
                intInd += 1
            Next
        Else
            If objLstDetCotiPax Is Nothing Then Exit Sub
Borrar1:
            Dim intInd As Int16 = 0
            For Each ST As stDetCotiPax In objLstDetCotiPax
                If ST.IDCAB = vstrIDCab Then
                    objLstDetCotiPax.RemoveAt(intInd)
                    GoTo Borrar1
                End If
                intInd += 1
            Next
        End If
    End Sub

    Public Sub BorrarSTAcomodoPaxIDCAB(ByVal vintIDCAB As Integer)
        If objLstAcomodoPax Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stAcomodoPax In objLstAcomodoPax
            If ST.IDCAB = vintIDCAB Then
                objLstAcomodoPax.RemoveAt(intInd)
                GoTo Borrar
            End If
            intInd += 1
        Next
    End Sub

    Public Sub BorrarSTAcomodoEspecialxIDDet(ByVal vstrIDDet As String, ByVal vintIDCab As Integer)
        If objLstAcodomodoEspecial Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stAcodomodoEspecial In objLstAcodomodoEspecial
            If ST.IDDet = vstrIDDet And ST.IDCab = vintIDCab Then
                objLstAcodomodoEspecial.RemoveAt(intInd)
                GoTo Borrar
            End If
            intInd += 1
        Next
    End Sub

    Public Sub BorrarSTAcomodoPaxProveedorIDCab(ByVal vintIDCab As Integer)
        If objLstAcomodoPaxProveedor Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stAcomodoPax In objLstAcomodoPaxProveedor
            If ST.IDCAB = vintIDCab Then
                objLstAcomodoPaxProveedor.RemoveAt(intInd)
                GoTo Borrar
            End If
            intInd += 1
        Next
    End Sub

    Public Sub BorrarSTReservPaxxIDCab(ByVal vintIDCab As Integer)
        If objLstDetReservPax Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stDetReservPax In objLstDetReservPax
            If ST.IDCab = vintIDCab Then
                objLstDetReservPax.RemoveAt(intInd)
                GoTo Borrar
            End If
            intInd += 1
        Next
    End Sub

    Public Sub BorrarSTOperacPaxxIDCab(ByVal vintIDCab As Integer)
        If objLstDetReservPax Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stDetOperacPax In objLstDetOperacPax
            If ST.IDCab = vintIDCab Then
                objLstDetOperacPax.RemoveAt(intInd)
                GoTo Borrar
            End If
            intInd += 1
        Next
    End Sub


    Public Class stCotizTextoIdioma
        Public Property IDCab As Integer
        Public Property IDDet As String
        Public Property IDIdioma As String

        Public Property DescCorta As String
        Public Property DescLarga As String

        Public Property NoHotel As String
        Public Property TxWebHotel As String

        Public Property TxDireccHotel As String
        Public Property TxTelfHotel1 As String
        Public Property TxTelfHotel2 As String
        Public Property FeHoraChkInHotel As DateTime
        Public Property FeHoraChkOutHotel As DateTime
        Public Property CoTipoDesaHotel As String
        Public Property CoCiudadHotel As String
        Public Property strAccion As String
    End Class
    Public objLstCotizTextoIdioma As New List(Of stCotizTextoIdioma)
    Public objLstCotizTextoIdiomaTmp As New List(Of stCotizTextoIdioma)


    Public Sub BorrarSTCotizTextoIdiomaxIDCab(ByVal vintIDCab As Integer)
        If objLstCotizTextoIdioma Is Nothing Then Exit Sub
Borrar:
        Dim intInd As Int16 = 0
        For Each ST As stCotizTextoIdioma In objLstCotizTextoIdioma
            If ST.IDCab = vintIDCab Then
                objLstCotizTextoIdioma.RemoveAt(intInd)
                GoTo Borrar
            End If
            intInd += 1
        Next
    End Sub

    'Public objLstCorreos As New List(Of String)

    Public Function strBuscarenTextoIdioma(ByVal vstrIDDet As String, _
                                           ByVal vstrIDIdioma As String, _
                                           ByVal vblnDescLarga As Boolean, _
                                           ByVal strDescBD As String) As String

        Dim strDesc As String = ""
        Dim blnBorrado As Boolean

        For Each ST As stCotizTextoIdioma In objLstCotizTextoIdioma
            blnBorrado = False
            If ST.IDDet = vstrIDDet And ST.IDIdioma = vstrIDIdioma Then
                If vblnDescLarga Then
                    strDesc = ST.DescLarga
                    'Else
                    '    strDesc = ST.DescCorta
                End If


                If ST.strAccion = "B" Then blnBorrado = True

                Exit For
            End If

        Next
        If Not blnBorrado Then
            If strDesc = "" Then
                Return strDescBD
            Else
                Return strDesc
            End If
        Else
            Return ""
        End If

    End Function

    Public Function strMesenIdioma(ByVal vdatFecha As Date, ByVal vstrCulturaIdioma As String, _
                                   Optional ByVal vblnTodoMes As Boolean = False) As String
        Dim strMes As String = ""

        'Select Case vstrIdioma
        '    Case "ENGLISH"
        '        Select Case vbytMes
        '            Case 1
        '                strMes = "JANUARY"
        '            Case 2
        '                strMes = "FEBRUARY"
        '            Case 3
        '                strMes = "MARCH"
        '            Case 4
        '                strMes = "APRIL"
        '            Case 5
        '                strMes = "MAY"
        '            Case 6
        '                strMes = "JUNE"
        '            Case 7
        '                strMes = "JULY"
        '            Case 8
        '                strMes = "AUGUST"
        '            Case 9
        '                strMes = "SEPTEMBER"
        '            Case 10
        '                strMes = "OCTOBER"
        '            Case 11
        '                strMes = "NOVEMBER"
        '            Case 12
        '                strMes = "DECEMBER"
        '        End Select


        '    Case "SPANISH"
        '        Select Case vbytMes
        '            Case 1
        '                strMes = "ENERO"
        '            Case 2
        '                strMes = "FEBRERO"
        '            Case 3
        '                strMes = "MARZO"
        '            Case 4
        '                strMes = "ABRIL"
        '            Case 5
        '                strMes = "MAYO"
        '            Case 6
        '                strMes = "JUNIO"
        '            Case 7
        '                strMes = "JULIO"
        '            Case 8
        '                strMes = "AGOSTO"
        '            Case 9
        '                strMes = "SETIEMBRE"
        '            Case 10
        '                strMes = "OCTUBRE"
        '            Case 11
        '                strMes = "NOVIEMBRE"
        '            Case 12
        '                strMes = "DICIEMBRE"
        '        End Select


        '    Case "FRENCH"
        '        Select Case vbytMes
        '            Case 1
        '                strMes = "JANVIER"
        '            Case 2
        '                strMes = "FÉVRIER"
        '            Case 3
        '                strMes = "MARS"
        '            Case 4
        '                strMes = "AVRIL"
        '            Case 5
        '                strMes = "MAI"
        '            Case 6
        '                strMes = "JUIN"
        '            Case 7
        '                strMes = "JUILLET"
        '            Case 8
        '                strMes = "AOÛT"
        '            Case 9
        '                strMes = "SEPTEMBRE"
        '            Case 10
        '                strMes = "OCTOBRE"
        '            Case 11
        '                strMes = "NOVEMBRE"
        '            Case 12
        '                strMes = "DÊCEMBRE"
        '        End Select


        '    Case "GERMAN"
        '        Select Case vbytMes
        '            Case 1
        '                strMes = "JANUAR"
        '            Case 2
        '                strMes = "FEBRUAR"
        '            Case 3
        '                strMes = "MÄRZ"
        '            Case 4
        '                strMes = "APRIL"
        '            Case 5
        '                strMes = "MAY"
        '            Case 6
        '                strMes = "JUNI"
        '            Case 7
        '                strMes = "JULI"
        '            Case 8
        '                strMes = "AUGUST"
        '            Case 9
        '                strMes = "SEPTEMBER"
        '            Case 10
        '                strMes = "OKTOBER"
        '            Case 11
        '                strMes = "NOVEMBER"
        '            Case 12
        '                strMes = "DEZEMBER"
        '        End Select
        '    Case Else
        '        Select Case vbytMes
        '            Case 1
        '                strMes = "JANUARY"
        '            Case 2
        '                strMes = "FEBRUARY"
        '            Case 3
        '                strMes = "MARCH"
        '            Case 4
        '                strMes = "APRIL"
        '            Case 5
        '                strMes = "MAY"
        '            Case 6
        '                strMes = "JUNE"
        '            Case 7
        '                strMes = "JULY"
        '            Case 8
        '                strMes = "AUGUST"
        '            Case 9
        '                strMes = "SEPTEMBER"
        '            Case 10
        '                strMes = "OCTOBER"
        '            Case 11
        '                strMes = "NOVEMBER"
        '            Case 12
        '                strMes = "DECEMBER"
        '        End Select
        'End Select

        strMes = vdatFecha.ToString("m", New  _
            CultureInfo(If(vstrCulturaIdioma.Trim = "", "en-US", vstrCulturaIdioma.Trim))).ToString.ToUpper
        If Not vblnTodoMes Then
            If IsNumeric(strMes.Substring(0, 2)) Then
                Return Mid(strMes, 3)
            Else
                Return strMes.Substring(0, InStr(strMes, " ") - 1)
            End If
        Else
            Dim arrPal() As String = strMes.Split(" ")
            For i As Byte = 0 To arrPal.Length - 1
                If Not IsNumeric(arrPal(i)) Then
                    strMes = arrPal(i).Substring(0, 1) & arrPal(i).Substring(1, arrPal(i).Length - 1).ToLower
                End If
            Next
            Return strMes
        End If
    End Function


    Public Function strAbrevAlimentacionenIdioma(ByVal vblnDesayuno As Boolean, _
                                                 ByVal vblnLonche As Boolean, _
                                                 ByVal vblnAlmuerzo As Boolean, _
                                                 ByVal vblnCena As Boolean, _
                                                 ByVal vstrIdioma As String, _
                                                 ByVal vblnParentesis As Boolean) As String
        Dim strAbrev As String = ""
        Dim strDesayuno As String = ""
        Dim strLonche As String = ""
        Dim strAlmuerzo As String = ""
        Dim strCena As String = ""

        Select Case vstrIdioma
            Case "ENGLISH"
                strDesayuno = If(vblnDesayuno = True, "B,", "")
                strLonche = If(vblnLonche = True, "BoxL,", "")
                strAlmuerzo = If(vblnAlmuerzo = True, "L,", "")
                strCena = If(vblnCena = True, "D,", "")

            Case "SPANISH"

                strDesayuno = If(vblnDesayuno = True, "D,", "")
                strLonche = If(vblnLonche = True, "BoxL,", "")
                strAlmuerzo = If(vblnAlmuerzo = True, "A,", "")
                strCena = If(vblnCena = True, "C,", "")

            Case "FRENCH"

                strDesayuno = If(vblnDesayuno = True, "PD,", "")
                strLonche = If(vblnLonche = True, "BoxL,", "")
                strAlmuerzo = If(vblnAlmuerzo = True, "D,", "")
                strCena = If(vblnCena = True, "Di,", "")

            Case "GERMAN"

                strDesayuno = If(vblnDesayuno = True, "F,", "")
                strLonche = If(vblnLonche = True, "BoxL,", "")
                strAlmuerzo = If(vblnAlmuerzo = True, "M,", "")
                strCena = If(vblnCena = True, "A,", "")

            Case Else
                strDesayuno = If(vblnDesayuno = True, "B,", "")
                strLonche = If(vblnLonche = True, "BoxL,", "")
                strAlmuerzo = If(vblnAlmuerzo = True, "L,", "")
                strCena = If(vblnCena = True, "D,", "")

        End Select

        strAbrev = strDesayuno & strLonche & strAlmuerzo & strCena
        If strAbrev.Length > 0 Then
            strAbrev = strAbrev.Substring(0, strAbrev.Length - 1)
        Else
            strAbrev = "-"
        End If
        If vblnParentesis And strAbrev.Trim <> "" Then strAbrev = "(" & strAbrev & ")"

        Return strAbrev

    End Function


    Public Function strDiaSemanaenIdioma(ByVal vdatFecha As Date, ByVal vstrCulturaIdioma As String, ByVal vstrIdioma As String) As String

        Dim strDiaSemana As String = vdatFecha.ToString("dddd", New  _
                    CultureInfo(If(vstrCulturaIdioma.Trim = "", "en-US", vstrCulturaIdioma.Trim))).ToString.ToUpper

        If vstrIdioma = "GERMAN" Then
            Return strDiaSemana.Trim.Substring(0, 2)
        Else
            Return strDiaSemana.Trim.Substring(0, 3)
        End If

    End Function

    Public Function strDiaSemanaenIdioma(ByVal vdatFecha As Date, ByVal vstrCulturaIdioma As String) As String

        Dim strDiaSemana As String = vdatFecha.ToString("dddddddddddddddddddddddddd", New  _
                    CultureInfo(If(vstrCulturaIdioma.Trim = "", "en-US", vstrCulturaIdioma.Trim))).ToString.ToUpper

        Return strDiaSemana.Trim
    End Function

    Public Function strDiaSemanaenIngles(ByVal vstrDiaEspaniol As String) As String

        Dim strDiaIngles As String = ""
        If vstrDiaEspaniol = "LUNES" Then
            strDiaIngles = "MONDAY"
        ElseIf vstrDiaEspaniol = "MARTES" Then
            strDiaIngles = "TUESDAY"
        ElseIf vstrDiaEspaniol = "MIÉRCOLES" Then
            strDiaIngles = "WEDNESDAY"
        ElseIf vstrDiaEspaniol = "JUEVES" Then
            strDiaIngles = "THURSDAY"
        ElseIf vstrDiaEspaniol = "VIERNES" Then
            strDiaIngles = "FRIDAY"
        ElseIf vstrDiaEspaniol = "SÁBADO" Then
            strDiaIngles = "SATURDAY"
        ElseIf vstrDiaEspaniol = "DOMINGO" Then
            strDiaIngles = "SUNDAY"
        End If
        Return strDiaIngles
    End Function

    Public Function strDiaenIdioma(ByVal vbytDia As Byte, ByVal vstrIdioma As String) As String
        Dim strDia As String
        Select Case vstrIdioma
            Case "ENGLISH"
                strDia = "DAY " & vbytDia.ToString
            Case "SPANISH"
                strDia = "DÍA " & vbytDia.ToString
            Case "FRENCH"
                strDia = "JOUR " & vbytDia.ToString
            Case "GERMAN"
                strDia = vbytDia.ToString & ". TAG"
            Case Else
                strDia = "DAY " & vbytDia.ToString

        End Select
        Return strDia
    End Function
    Public Function strHotelesConsideradosenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma
            Case "ENGLISH"
                strReturn = "HOTELS CONSIDERED"
            Case "SPANISH"
                strReturn = "HOTELES CONSIDERADOS"
            Case "FRENCH"
                strReturn = "HôTELS EXAMINÉ"
            Case "GERMAN"
                'strReturn = "HOTELS ALS"
                strReturn = "HOTELS"
            Case Else
                strReturn = "HOTELS CONSIDERED"
        End Select
        Return strReturn
    End Function

    Public Function strServiciosIncluidosenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma
            Case "ENGLISH"
                strReturn = "SERVICES INCLUDED"
            Case "SPANISH"
                strReturn = "SERVICIOS INCLUÍDOS"
            Case "FRENCH"
                strReturn = "SERVICES INCLUS"
            Case "GERMAN"
                strReturn = "LEISTUNGEN"
            Case Else
                strReturn = "SERVICES INCLUDED"

        End Select
        Return strReturn
    End Function

    Public Function strTouristenIdioma(ByVal vstrPalabra As String, ByVal vstrIdioma As String) As String
        Dim strReturn As String = String.Empty
        If vstrPalabra.ToLower = "tourist" Then
            Select Case vstrIdioma
                Case "ENGLISH"
                    strReturn = "Tourist"
                Case "SPANISH"
                    strReturn = "Turista"
                Case "FRENCH"
                    strReturn = "Tourisme"
                Case "GERMAN"
                    strReturn = "Mittelklasse"
                Case Else
                    strReturn = "Tourist"
            End Select
        ElseIf vstrPalabra.ToLower = "first" Then
            Select Case vstrIdioma
                Case "ENGLISH"
                    strReturn = "First"
                Case "SPANISH"
                    strReturn = "Primera"
                Case "FRENCH"
                    strReturn = "Premier"
                Case "GERMAN"
                    strReturn = "Erste"
                Case Else
                    strReturn = "First"
            End Select
        ElseIf vstrPalabra.ToLower = "deluxe" Then
            Select Case vstrIdioma
                Case "ENGLISH"
                    strReturn = "Deluxe"
                Case "SPANISH"
                    strReturn = "Lujo"
                Case "FRENCH"
                    strReturn = "Luxe"
                Case "GERMAN"
                    strReturn = "Luxus"
                Case Else
                    strReturn = "Deluxe"
            End Select
        End If
        Return strReturn
    End Function

    Public Function strServiciosNoIncluidosenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma
            Case "ENGLISH"
                strReturn = "SERVICES NOT INCLUDED"
            Case "SPANISH"
                strReturn = "SERVICIOS NO INCLUÍDOS"
            Case "FRENCH"
                strReturn = "SERVICES NON INCLUS"
            Case "GERMAN"
                strReturn = "NICHT ENTHALTENE LEISTUNGEN"
            Case Else
                strReturn = "SERVICES NOT INCLUDED"

        End Select
        Return strReturn
    End Function
    Public Function strAlimentacionenIdioma(ByVal vstrIdioma As String, _
                                            ByVal vblnDesayuno As Boolean, _
                                            ByVal vblnLonche As Boolean, _
                                            ByVal vblnAlmuerzo As Boolean, _
                                            ByVal vblnCena As Boolean) As String
        Dim strReturn As String = ""

        If vblnDesayuno Then
            strReturn = strDesayunoenIdioma(vstrIdioma)
        ElseIf vblnLonche Then
            strReturn = strLoncheenIdioma(vstrIdioma)
        ElseIf vblnAlmuerzo Then
            strReturn = strAlmuerzoenIdioma(vstrIdioma)
        ElseIf vblnCena Then
            strReturn = strCenaenIdioma(vstrIdioma)
        End If

        Return strReturn

    End Function
    Public Function strDesayunoenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma

            Case "ENGLISH"
                strReturn = "Breakfast in "
            Case "SPANISH"
                strReturn = "Desayuno en "
            Case "FRENCH"
                strReturn = "Petit-déjeuner en "
            Case "GERMAN"
                strReturn = "Frühstück im "
            Case Else
                strReturn = "Breakfast in "

        End Select
        Return strReturn

    End Function
    Public Function strAlmuerzoenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma

            Case "ENGLISH"
                strReturn = "Lunch at "
            Case "SPANISH"
                strReturn = "Almuerzo en "
            Case "FRENCH"
                strReturn = "Déjeuner au "
            Case "GERMAN"
                strReturn = "Mittagessen im "
            Case Else
                strReturn = "Lunch at "

        End Select
        Return strReturn

    End Function
    Public Function strCenaenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma

            Case "ENGLISH"
                strReturn = "Dinner at "
            Case "SPANISH"
                strReturn = "Cena en "
            Case "FRENCH"
                strReturn = "Dîner au "
            Case "GERMAN"
                strReturn = "Abendessen im "
            Case Else
                strReturn = "Dinner at "

        End Select
        Return strReturn

    End Function
    Public Function strLoncheenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma

            Case "ENGLISH"
                strReturn = "Box Lunch at "
            Case "SPANISH"
                strReturn = "Lonche en "
            Case "FRENCH"
                strReturn = "Box Lunch au "
            Case "GERMAN"
                strReturn = "Box Lunch im "
            Case Else
                strReturn = "Box Lunch at "

        End Select
        Return strReturn

    End Function
    Public Function strCiudadenIdioma(ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma
            Case "ENGLISH"
                strReturn = "City"
            Case "SPANISH"
                strReturn = "Ciudad"
            Case "FRENCH"
                strReturn = "Ville"
            Case "GERMAN"
                strReturn = "Stadt"
            Case Else
                strReturn = "City"
        End Select
        Return strReturn
    End Function

    ''Public Function strProgramaCategoriaenIdiomas(ByVal vbytDia As Byte, ByVal vstrIdioma As String) As String
    Public Function strProgramaCategoriaenIdiomas(ByVal vbytDia As Int16, ByVal vstrIdioma As String) As String
        Dim strReturn As String
        Select Case vstrIdioma
            Case "ENGLISH"
                If vbytDia = 1 Then
                    strReturn = "Program: " & vbytDia.ToString & " day"
                Else
                    strReturn = "Program: " & vbytDia.ToString & " days"
                End If

            Case "SPANISH"
                If vbytDia = 1 Then
                    strReturn = "Programa: " & vbytDia.ToString & " día"
                Else
                    strReturn = "Programa: " & vbytDia.ToString & " días"
                End If
            Case "FRENCH"
                If vbytDia = 1 Then
                    strReturn = "Programme: " & vbytDia.ToString & " journée"
                Else
                    strReturn = "Programme: " & vbytDia.ToString & " jours"
                End If
            Case "GERMAN"
                If vbytDia = 1 Then
                    strReturn = "Programm: " & vbytDia.ToString & " Tag"
                Else
                    strReturn = "Programm: " & vbytDia.ToString & " Tage"
                End If
            Case Else
                If vbytDia = 1 Then
                    strReturn = "Program: " & vbytDia.ToString & " day"
                Else
                    strReturn = "Program: " & vbytDia.ToString & " days"
                End If
        End Select
        Return strReturn
    End Function

    ''Public Function strDiasNochesenIdioma(ByVal vbytDia As Byte, ByVal vstrIdioma As String) As String
    Public Function strDiasNochesenIdioma(ByVal vbytDia As Int16, ByVal vstrIdioma As String) As String
        Dim strDiaNoche As String
        Select Case vstrIdioma
            Case "ENGLISH"
                If vbytDia = 1 Then
                    strDiaNoche = vbytDia.ToString & " Day"
                ElseIf vbytDia = 2 Then
                    strDiaNoche = vbytDia.ToString & " Days / " & (vbytDia - 1).ToString & " Night"
                Else
                    strDiaNoche = vbytDia.ToString & " Days / " & (vbytDia - 1).ToString & " Nights"
                End If

            Case "SPANISH"
                If vbytDia = 1 Then
                    strDiaNoche = vbytDia.ToString & " Día"
                ElseIf vbytDia = 2 Then
                    strDiaNoche = vbytDia.ToString & " Días / " & (vbytDia - 1).ToString & " Noche"
                Else
                    strDiaNoche = vbytDia.ToString & " Días / " & (vbytDia - 1).ToString & " Noches"
                End If
            Case "FRENCH"
                If vbytDia = 1 Then
                    strDiaNoche = vbytDia.ToString & " jour"
                ElseIf vbytDia = 2 Then
                    strDiaNoche = vbytDia.ToString & " jours / " & (vbytDia - 1).ToString & " nuit"
                Else
                    strDiaNoche = vbytDia.ToString & " jours / " & (vbytDia - 1).ToString & " nuits"
                End If
            Case "GERMAN"
                If vbytDia = 1 Then
                    strDiaNoche = vbytDia.ToString & " Tag"
                ElseIf vbytDia = 2 Then
                    strDiaNoche = vbytDia.ToString & " Tage / " & (vbytDia - 1).ToString & " Nacht"
                Else
                    strDiaNoche = vbytDia.ToString & " Tage / " & (vbytDia - 1).ToString & " Nächte"
                End If
            Case Else
                If vbytDia = 1 Then
                    strDiaNoche = vbytDia.ToString & " Day"
                ElseIf vbytDia = 2 Then
                    strDiaNoche = vbytDia.ToString & " Days / " & (vbytDia - 1).ToString & " Night"
                Else
                    strDiaNoche = vbytDia.ToString & " Days / " & (vbytDia - 1).ToString & " Nights"
                End If
        End Select
        Return "(" & strDiaNoche & ")"
    End Function

    Public Function strTituloTablaCategoriaHotelesenIdiomas(ByVal vstrIdioma As String) As String
        Dim strReturn As String

        Select Case vstrIdioma
            Case "ENGLISH"
                strReturn = "Price per person in USD sharing TWIN"
            Case "SPANISH"
                strReturn = "Precio por persona en USD en base TWIN"
            Case "FRENCH"
                strReturn = "Prix net par personne en USD basé sur une occupation double"
            Case "GERMAN"
                strReturn = "Preise pro Person im Doppelzimmer in USD"
            Case Else
                strReturn = "Price per person in USD sharing TWIN"
        End Select

        Return strReturn
    End Function

    Public Function strTituloTablaPrivateServicesenIdiomas(ByVal vstrIdioma As String) As String
        Dim strReturn As String

        Select Case vstrIdioma
            Case "ENGLISH"
                strReturn = "Private Services"
            Case "SPANISH"
                strReturn = "Servicios Privados"
            Case "FRENCH"
                strReturn = "Services Privés"
            Case "GERMAN"
                strReturn = "Private Leistungen"
            Case Else
                strReturn = "Private Services"
        End Select

        Return strReturn
    End Function

    '    Public Sub CambiarCorreoaLeido(ByVal vstrIDFile As String, ByVal vlstCorreosProveedor As List(Of String), ByVal vstrConversationIndex As String, ByVal vstrModulo As String)

    '        Try

    '            Dim strArchivoPST As String = ""
    '            If vstrModulo = "R" Or vstrModulo = "RV" Then
    '                strArchivoPST = vstrIDFile & "res.pst"
    '            ElseIf vstrModulo = "O" Then
    '                strArchivoPST = vstrIDFile & "ope.pst"
    '            ElseIf vstrModulo = "C" Then
    '                strArchivoPST = vstrIDFile & "Ventas.pst"
    '            End If
    '            Dim objOutlook As Object
    '            objOutlook = CreateObject("Outlook.Application")
    '            objOutlook.Session.SendAndReceive(False)

    '            Dim objNS As Object = objOutlook.Session

    '            Dim objCorreos As Object = Nothing
    '            objCorreos = objNS.GetDefaultFolder(Outlook.OlDefaultFolders.olFolderInbox)

    '            Dim IndxFolder As Int16 = 1
    '            'For Each MiFolder As object In objOutlook.Session.Folders
    '            For Each MiFolder As Object In objOutlook.Session.Folders
    '                Dim strArchivo As String = Mid(MiFolder.Store.FilePath, InStrRev(MiFolder.Store.FilePath, "\") + 1)
    '                If strArchivo.ToLower = strArchivoPST Then
    '                    objCorreos = objNS.Folders(IndxFolder)
    '                End If
    '                strArchivo = "" : IndxFolder += 1
    '            Next
    '            'Dim objItems as object = Nothing
    '            Dim objItems As Object = Nothing

    '            'vstrConversationIndex = "'" & vstrConversationIndex & "'"
    '            'objItems = objCorreos.Items.Restrict("[ConversationIndex]=""" & vstrConversationIndex & """")
    '            'objItems = objCorreos.Items.Find("[ConversationIndex]=""" & vstrConversationIndex & """")



    '            Dim strCorreoProveedor As String = ""
    'RetornarDT:
    '            For Each strCorreoProveedor In vlstCorreosProveedor
    '                objItems = objCorreos.Items.Restrict("[SenderEmailAddress]=""" & strCorreoProveedor & """")
    '                Exit For
    '            Next

    '            'If objItems.Count > 0 Then
    '            '    CType(objItems.Item(1), Outlook.MailItem).UnRead = False
    '            'End If

    '            Dim iCount As Int32 = 0
    '            If Not objItems Is Nothing Then
    '                iCount = objItems.Count
    '            End If

    '            For i As Int16 = 1 To iCount
    '                If TypeOf (objItems.Item(i)) Is Outlook.MailItem Then
    '                    Dim strConversationIndex As String = CType(objItems.Item(i), Outlook.MailItem).ConversationIndex

    '                    If strConversationIndex = vstrConversationIndex Then
    '                        CType(objItems.Item(i), Outlook.MailItem).UnRead = False
    '                        vlstCorreosProveedor.Remove(strCorreoProveedor)
    '                        Exit For
    '                    Else
    '                        vlstCorreosProveedor.Remove(strCorreoProveedor)
    '                    End If
    '                End If

    '            Next
    '            If iCount = 0 Then vlstCorreosProveedor.Remove(strCorreoProveedor)
    '            If vlstCorreosProveedor.Count <> 0 Then
    '                GoTo RetornarDT
    '            End If

    '        Catch ex As System.Exception
    '            Throw
    '        End Try

    '    End Sub


    'Public Function strCrearArchivoPST(ByVal vstrNombreArchivo As String) As String
    '    Try
    '        Dim strRuta As String = gstrRutaImagenes & vstrNombreArchivo.Trim & ".pst"
    '        Dim blnCreoArchivoPST As Boolean = False
    '        Dim objOutlook As object
    '        objOutlook = createobject("Outlook.Application")


    '        If Not IO.File.Exists(strRuta) Then
    '            With objOutlook
    '                Dim myNameSpace As Outlook.NameSpace
    '                myNameSpace = objOutlook.GetNamespace("MAPI")
    '                myNameSpace.AddStore(strRuta)

    '                blnCreoArchivoPST = True
    '            End With
    '        End If

    '        If blnCreoArchivoPST Then
    '            Return strRuta
    '        Else
    '            Return String.Empty
    '        End If

    '    Catch ex As System.Exception
    '        Return String.Empty
    '        Throw
    '    End Try
    'End Function



    'Public Sub EnviarCorreoOutlook(ByVal vstrDestinatarios As String, ByVal vstrDestinatariosCopia As String, _
    '                                       ByVal vstrAsunto As String, ByVal vListArchivos As List(Of String), ByVal vstrDetalle As String, _
    '                                       ByVal vstrCopiaOculta As String, ByVal vchrFormato As Char)

    '    Try
    '        '- Crear una aplicación Outlook.
    '        Dim oApp As Object
    '        oApp = CreateObject("Outlook.Application")
    '        '- Crear un nuevo elemento de correo.

    '        Dim objNS As Object = oApp.Session
    '        Dim oMsg As Outlook._MailItem
    '        oMsg = oApp.CreateItem(Outlook.OlItemType.olMailItem)

    '        With oMsg
    '            .Subject = vstrAsunto
    '            .CC = vstrDestinatariosCopia
    '            If vchrFormato = "H" Then
    '                .HTMLBody = vstrDetalle
    '                .BodyFormat = OlBodyFormat.olFormatHTML
    '            ElseIf vchrFormato = "T" Then
    '                .Body = vstrDetalle
    '                .HTMLBody = strDevuelveFormatorHTMLTmp(.HTMLBody)
    '                .InternetCodepage = 65001
    '                .BodyFormat = OlBodyFormat.olFormatRichText
    '            End If

    '            .To = vstrDestinatarios
    '            .BCC = vstrCopiaOculta

    '            For Each lstArchivos As String In vListArchivos
    '                .Attachments.Add(lstArchivos)
    '            Next

    '        End With


    '        '------------------------
    '        Dim sBodyLen As String = oMsg.Body.Length
    '        Dim oAttachs As Outlook.Attachments = oMsg.Attachments
    '        'Dim oAttach As Outlook.Attachment
    '        '------------------------
    '        oMsg.Send()

    '    Catch ex As System.Exception
    '        Throw
    '    End Try
    'End Sub

    Public Function strDevuelveFormatorHTMLTmp(ByVal vstrBodyHtml As String) As String
        Try
            Dim strReturn As String = vstrBodyHtml
            strReturn = Replace(strReturn, "Calibri", "Lucida Console")
            strReturn = Replace(strReturn, "Arial", "Lucida Console")
            strReturn = Replace(strReturn, "<FONT ", "<FONT size='-1' ")
            strReturn = Replace(strReturn, "*", "&nbsp;")
            strReturn = strDevueleCodigoHTMLxLetra(strReturn)
            Return strReturn
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveCuentasCorreoPuros(ByVal vstrCuentaCorreo As String, Optional ByVal vstrCorreo2 As String = "") As String
        Dim strReturn As String = ""
        Dim chrCar As Char = ""
        Dim strCorreo As String = ""
        Dim blnContinuar As Boolean = False
        Dim lstCorreosDupl As New List(Of String)

        For i As Byte = 1 To vstrCuentaCorreo.Length
            chrCar = vstrCuentaCorreo.Substring(i - 1, 1)

            If blnContinuar And chrCar <> "'" And chrCar <> "<" And chrCar <> ">" Then
                'strReturn += chrCar
                strCorreo += chrCar
            End If

            If chrCar = "<" Then
                blnContinuar = True

            ElseIf chrCar = ">" Then
                blnContinuar = False
                lstCorreosDupl.Add(strCorreo)
                strCorreo = ""
                'strReturn += "; "

            End If
        Next

        If vstrCorreo2 <> "" Then
            lstCorreosDupl.Add(vstrCorreo2)
        End If

        Dim lstCorreos2 As New List(Of String)
        For Each strCor As String In lstCorreosDupl
            If Not lstCorreos2.Contains(strCor) Then
                lstCorreos2.Add(strCor)
            End If
        Next

        For Each strCor As String In lstCorreos2
            strReturn += strCor & "; "
        Next

        Return strReturn.Trim
    End Function

    Public Function strDevuelveCuentasCorreoPuros2(ByVal vstrCuentaCorreo As String, Optional ByVal vstrCorreo2 As String = "") As String
        Dim strReturn As String = ""
        Dim chrCar As Char = ""
        Dim strCorreo As String = ""
        Dim lstCorreosDupl As New List(Of String)

        If InStr(vstrCuentaCorreo, ";") = 0 Then
            vstrCuentaCorreo = vstrCuentaCorreo.Trim & ";"
        End If
        If InStr(vstrCorreo2, ";") = 0 Then
            vstrCorreo2 = vstrCorreo2.Trim & ";"
        End If

        For i As Byte = 1 To vstrCuentaCorreo.Length
            chrCar = vstrCuentaCorreo.Substring(i - 1, 1)

            strCorreo += chrCar
            If chrCar = ";" Then
                lstCorreosDupl.Add(strCorreo.Replace(";", "").Trim)
                strCorreo = ""
            End If
        Next

        If vstrCorreo2 <> "" Then
            lstCorreosDupl.Add(vstrCorreo2.Replace(";", "").Trim)
        End If

        Dim lstCorreos2 As New List(Of String)
        For Each strCor As String In lstCorreosDupl
            If Not lstCorreos2.Contains(strCor) Then
                lstCorreos2.Add(strCor)
            End If
        Next

        For Each strCor As String In lstCorreos2
            strReturn += strCor & "; "
        Next

        Return strReturn.Trim
    End Function

    Public Function strNombreUsuarioOutlook(Optional ByVal blnTitulo As Boolean = False) As String
        Dim strAddress As String = ""
        Dim objOutlook As Object
        objOutlook = CreateObject("Outlook.Application")
        Dim objNS As Object = objOutlook.Session
        If Not blnTitulo Then strAddress = objNS.CurrentUser.Address Else strAddress = "'" & objNS.CurrentUser.Address & " - SETOURS S.A.'"
        Return strAddress
    End Function

    Public Sub pOutlookEnviarRecibir()
        Dim objOutlook As Object
        objOutlook = CreateObject("Outlook.Application")
        objOutlook.Session.SendAndReceive(False)
    End Sub

    Public Function strDevuelveCadenaOcultaDerecha(ByVal vstrCadena As String) As String
        Dim strCadenaOculta As String = ""
        If InStr(vstrCadena, "|") > 0 Then
            strCadenaOculta = Mid(vstrCadena, InStr(vstrCadena, "|") + 1)
        End If

        Return strCadenaOculta.Trim
    End Function

    Public Function strDevuelveCadenaNoOcultaIzquierda(ByVal vstrCadena As String) As String
        Dim strCadenaNoOculta As String = ""
        If InStr(vstrCadena, "|") > 0 Then
            strCadenaNoOculta = vstrCadena.Substring(0, InStr(vstrCadena, "|") - 1)
        End If

        Return strCadenaNoOculta.Trim
    End Function
    Public Function dblRedondeoInmedSuperior(ByVal strValor As String, _
                                             ByVal strRedondeo As String, _
                                             Optional ByRef sglRedondeado As Single = 0) As Double
        Dim dblReturn As Double = 0
        Dim strEntero As String
        Dim strDecimales As String

        If InStr(strValor, ".") > 0 Then
            strEntero = strValor.Substring(0, InStr(strValor, ".") - 1)
            strDecimales = Mid(strValor, InStr(strValor, ".") + 1)
        Else
            strEntero = strValor
            strDecimales = "0"
        End If

        If strRedondeo = "1.0" Then
            If strDecimales = "0" Then
                dblReturn = strEntero
            Else
                dblReturn = Convert.ToDouble(strEntero) + 1
            End If
        ElseIf strRedondeo = "0.5" Then
            If strDecimales = "0" Then
                dblReturn = strEntero
            Else
                If Convert.ToByte(strDecimales.Substring(0, 1)) >= 5 Then
                    dblReturn = Convert.ToDouble(strEntero) + 1
                Else
                    dblReturn = strEntero & ".5"
                End If

            End If
        Else
            dblReturn = strValor
        End If

        sglRedondeado = Math.Abs(Convert.ToDouble(strValor) - dblReturn)
        Return dblReturn
    End Function

    Public Function sRTF_To_HTML(ByVal sRTF As String) As String
        'Declare a Word Application Object and a Word WdSaveOptions object
        'Dim MyWord As New Word.Application 'Microsoft.Office.Interop.Word.Application
        Dim MyWord As New Object 'Microsoft.Office.Interop.Word.Application

        'Dim oDoNotSaveChanges As Object = Word.WdSaveOptions.wdDoNotSaveChanges  '_
        Dim oDoNotSaveChanges As Object = CreateObject("Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges")

        'Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges()
        'Declare two strings to handle the data
        Dim sReturnString As String = ""
        Dim sConvertedString As String = ""
        Try
            'Instantiate the Word application,
            'set visible to false and create a document
            'MyWord = CreateObject("Word.application")

            'MyWord = New Word.Application
            MyWord = CreateObject("Word.Application")
            MyWord.Visible = False
            MyWord.Documents.Add()
            'Create a DataObject to hold the Rich Text
            'and copy it to the clipboard
            Dim doRTF As New System.Windows.Forms.DataObject
            doRTF.SetData("Rich Text Format", sRTF)
            Clipboard.SetDataObject(doRTF)
            'Paste the contents of the clipboard to the empty,
            'hidden Word Document
            MyWord.Windows(1).Selection.Paste()
            'â€¦then, select the entire contents of the document
            'and copy back to the clipboard
            MyWord.Windows(1).Selection.WholeStory()
            MyWord.Windows(1).Selection.Copy()
            'Now retrieve the HTML property of the DataObject
            'stored on the clipboard
            sConvertedString = _
                 Clipboard.GetData(System.Windows.Forms.DataFormats.Html)
            'Remove some leading text that shows up in some instances
            '(like when you insert it into an email in Outlook
            sConvertedString = _
                 sConvertedString.Substring(sConvertedString.IndexOf("<html"))
            'Also remove multiple Ã‚ characters that somehow end up in there
            sConvertedString = sConvertedString.Replace("Ã‚", "")
            'â€¦and you're done.
            sReturnString = sConvertedString
            If Not MyWord Is Nothing Then
                MyWord.Quit(oDoNotSaveChanges)
                MyWord = Nothing
            End If
        Catch ex As System.Exception
            If Not MyWord Is Nothing Then
                MyWord.Quit(oDoNotSaveChanges)
                MyWord = Nothing
            End If
            MsgBox("Error converting Rich Text to HTML")
        End Try
        Return sReturnString
    End Function


    'Public Function sHTML_To_RTF(ByVal sHTML As String) As String
    '    'Declare a Word Application Object and a Word WdSaveOptions object
    '    Dim MyWord As New Microsoft.Office.Interop.Word.Application

    '    Dim oDoNotSaveChanges As Object = _
    '         Microsoft.Office.Interop.Word.WdSaveOptions.wdDoNotSaveChanges
    '    'Declare two strings to handle the data
    '    Dim sReturnString As String = ""
    '    Dim sConvertedString As String = ""
    '    Try
    '        'Instantiate the Word application,
    '        'set visible to false and create a document
    '        'MyWord = CreateObject("Word.application")

    '        MyWord = New Word.Application
    '        MyWord.Visible = False
    '        MyWord.Documents.Add()
    '        'Create a DataObject to hold the Rich Text
    '        'and copy it to the clipboard

    '        Dim doHTML As New System.Windows.Forms.DataObject

    '        doHTML.SetData(sHTML)
    '        Clipboard.SetDataObject(doHTML)
    '        'Paste the contents of the clipboard to the empty,
    '        'hidden Word Document
    '        MyWord.Windows(1).Selection.Paste()
    '        'â€¦then, select the entire contents of the document
    '        'and copy back to the clipboard
    '        MyWord.Windows(1).Selection.WholeStory()
    '        MyWord.Windows(1).Selection.Copy()
    '        'Now retrieve the HTML property of the DataObject
    '        'stored on the clipboard
    '        sConvertedString = _
    '             Clipboard.GetData(System.Windows.Forms.DataFormats.Rtf)
    '        'Remove some leading text that shows up in some instances
    '        '(like when you insert it into an email in Outlook
    '        'sConvertedString = _
    '        'sConvertedString.Substring(sConvertedString.IndexOf("<html"))
    '        'Also remove multiple Ã‚ characters that somehow end up in there
    '        'sConvertedString = sConvertedString.Replace("Ã‚", "")
    '        'â€¦and you're done.
    '        sReturnString = sConvertedString
    '        If Not MyWord Is Nothing Then
    '            MyWord.Quit(oDoNotSaveChanges)
    '            MyWord = Nothing
    '        End If
    '    Catch ex As System.Exception
    '        If Not MyWord Is Nothing Then
    '            MyWord.Quit(oDoNotSaveChanges)
    '            MyWord = Nothing
    '        End If
    '        'MsgBox("Error converting HTML To Rich Text")
    '        Throw
    '    End Try
    '    Return sReturnString
    'End Function


    Public Sub pCambiarTitulosToolBar(ByVal vFrm As Form, Optional ByVal vblnDesactivar As Boolean = False)
        'Try
        '    If vFrm.Tag <> "" Then
        '        Dim strPalabra As String
        '        Dim strForm As String = ""
        '        If vFrm.Tag = "Origen" Or vFrm.Tag = "Destino" Then
        '            strForm = "Cotización"
        '        Else
        '            strForm = vFrm.Tag
        '        End If

        '        If vblnDesactivar Then
        '            strForm = ""
        '        End If

        '        If InStr(strForm, " ") > 0 Then
        '            strPalabra = strForm.ToString.Substring(0, InStr(strForm, " ")).Trim
        '        Else
        '            strPalabra = strForm.ToString.Trim
        '        End If

        '        If strPalabra.ToUpper <> "IDIOMA" Then
        '            If strPalabra <> "" Then
        '                If strPalabra.Substring(strPalabra.Length - 1, 1).ToUpper = "A" Then
        '                    frmMain.btnNuevo.Text = "Nueva " & strForm & " (F2)"
        '                Else
        '                    frmMain.btnNuevo.Text = "Nuevo " & strForm & " (F2)"
        '                End If
        '            Else
        '                frmMain.btnNuevo.Text = "Nuevo " & strForm & " (F2)"
        '            End If
        '        Else
        '            frmMain.btnNuevo.Text = "Nuevo " & strForm & " (F2)"
        '        End If

        '        frmMain.btnEditar.Text = "Editar " & strForm & " (F3)"
        '        frmMain.btnGrabar.Text = "Grabar " & strForm & " (F6)"
        '        frmMain.btnDeshacer.Text = "Deshacer " & strForm & " (ESC)"
        '        frmMain.btnEliminar.Text = "Eliminar " & strForm & " (F4)"
        '        frmMain.btnImprimir.Text = "Imprimir " & strForm
        '        frmMain.btnSalir.Text = "Salir " & strForm
        '    Else
        '        frmMain.btnNuevo.Text = "Nuevo (F2)"
        '        frmMain.btnEditar.Text = "Editar (F3)"
        '        frmMain.btnGrabar.Text = "Grabar (F6)"
        '        frmMain.btnDeshacer.Text = "Deshacer (ESC)"
        '        frmMain.btnEliminar.Text = "Eliminar (F4)"
        '        frmMain.btnImprimir.Text = "Imprimir"
        '        frmMain.btnSalir.Text = "Salir"
        '    End If
        'Catch ex As System.Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        'End Try

    End Sub

    Public Sub pCambiarSelecionColorGridsenGroups(ByVal cGroup As GroupBox)
        With cGroup
            For Each ctrl As Object In .Controls

                If ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    ctrl.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
                    ctrl.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
                    If ctrl.tag <> "CellSelect" Then ctrl.selectionmode = 1
                End If

                If ctrl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                    pCambiarSelecionColorGridsenSplitContainer(ctrl)
                End If

            Next
        End With
    End Sub

    Public Sub pCambiarSelecionColorGridsenSplitContainer(ByVal cTab As SplitContainer)
        With cTab
            For Each ctrl As Object In .Controls
                'MessageBox.Show(ctrl.Name.ToString)
                If ctrl.GetType Is GetType(System.Windows.Forms.SplitterPanel) Then
                    pCambiarSelecionColorGridsenSplitPanel(ctrl)
                End If
                If ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    ctrl.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
                    ctrl.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
                    If ctrl.tag <> "CellSelect" Then ctrl.selectionmode = 1
                End If
                If ctrl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                    pCambiarSelecionColorGridsenSplitContainer(ctrl)
                End If
                If ctrl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                    pCambiarSelecionColorGridsenTabPages(ctrl)
                End If
                If ctrl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                    pCambiarSelecionColorGridsenTabs(ctrl)
                End If
            Next
        End With
    End Sub
    Public Sub pCambiarSelecionColorGridsenSplitPanel(ByVal cTab As SplitterPanel)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    'MessageBox.Show(ctrl.name.ToString)
                    ctrl.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
                    ctrl.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
                    If ctrl.tag <> "CellSelect" Then ctrl.selectionmode = 1
                    'MessageBox.Show(ctrl.name.ToString)
                End If
                If ctrl.GetType Is GetType(System.Windows.Forms.SplitterPanel) Then
                    pCambiarSelecionColorGridsenSplitPanel(ctrl)
                End If
            Next
        End With
    End Sub

    Public Sub pCambiarSelecionColorGridsenTabs(ByVal cTab As TabControl)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.GetType Is GetType(System.Windows.Forms.TabPage) Then
                    pCambiarSelecionColorGridsenTabPages(ctrl)
                End If
                If ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    ctrl.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
                    ctrl.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
                    If ctrl.tag <> "CellSelect" Then ctrl.selectionmode = 1
                End If
            Next
        End With
    End Sub
    Public Sub pCambiarSelecionColorGridsenTabPages(ByVal cTab As TabPage)
        With cTab
            For Each ctrl As Object In .Controls
                If ctrl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                    pCambiarSelecionColorGridsenGroups(ctrl)
                End If
                If ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                    ctrl.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
                    ctrl.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
                    If ctrl.tag <> "CellSelect" Then ctrl.selectionmode = 1
                End If
            Next

        End With
    End Sub

    Public Sub pCambiarSelecionColorGrids()

        For Each frm As Form In My.Application.OpenForms
            For Each ctrl As Object In frm.Controls
                If frm.IsMdiContainer = False And frm.Visible = True Then
                    'MessageBox.Show(frm.Name)
                    'MessageBox.Show(ctrl.Name)ctrl
                    If ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                        ctrl.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
                        ctrl.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black
                        If ctrl.tag <> "CellSelect" Then ctrl.selectionmode = 1
                    End If
                    If ctrl.GetType Is GetType(System.Windows.Forms.GroupBox) Then
                        pCambiarSelecionColorGridsenGroups(ctrl)
                    End If
                    If ctrl.GetType Is GetType(System.Windows.Forms.SplitContainer) Then
                        pCambiarSelecionColorGridsenSplitContainer(ctrl)
                    End If
                    If ctrl.GetType Is GetType(System.Windows.Forms.TabControl) Then
                        pCambiarSelecionColorGridsenTabs(ctrl)
                    End If
                End If
            Next
        Next

    End Sub

    Public Function sglDevuelveItemCorrecto(ByVal vDgvDet As DataGridView, ByVal datDia As Date, _
                                            ByVal vstrTipoProv As String, ByVal vstrModoEdicion As String, _
                                            Optional ByVal vstrIDDetPadre As String = "") As Single


        Dim sglRet As Single = 0
        Dim sglItem As Single = 0.0
        Dim sglSumRes As Single = 0.01
        Dim blnBuscar As Boolean = True
        Try
            With vDgvDet
                If vstrModoEdicion = "N" And .Rows.Count = 1 Then
                    Return 1.0
                    Exit Function
                End If

                If vstrTipoProv = gstrTipoProveeHoteles Then
                    For bytFila As Byte = 0 To .Rows.Count - 1
                        blnBuscar = True
                        If Not .Item("Item", bytFila).Value Is Nothing Then
                            If vstrIDDetPadre <> "" And .Item("IDDetRel", bytFila).Value.ToString <> "0" And .Item("IDDetRel", bytFila).Value.ToString <> "" Then
                                If Not IsNumeric(vstrIDDetPadre) Then
                                    If Mid(.Item("IDDetRel", bytFila).Value.ToString, 2) = Mid(vstrIDDetPadre, 2) Then
                                        blnBuscar = False
                                    End If
                                Else
                                    If .Item("IDDet", bytFila).Value.ToString = vstrIDDetPadre Then
                                        blnBuscar = False
                                    End If
                                End If
                            End If
                        End If
                        If blnBuscar Then
                            If Convert.ToDateTime(FormatDateTime(datDia, DateFormat.ShortDate)) = Convert.ToDateTime(FormatDateTime(.Item("Dia", bytFila).Value, DateFormat.ShortDate)) Then
                                sglItem = .Item("Item", bytFila).Value
                            End If
                        End If
                    Next

                    If sglItem <> 0 Then
                        If InStr(sglItem.ToString, ".") > 0 Then
                            If Mid(sglItem.ToString, InStr(sglItem.ToString, ".")).Length - 1 = 3 Then
                                sglSumRes = 0.001
                            Else
                                sglSumRes = 0.01
                            End If

                        Else
                            sglSumRes = 0.01
                        End If
                        sglRet = sglItem + sglSumRes
                    End If

                Else

                    Dim blnExisteHotelenDia As Boolean = False
                    'Dim datDiaActual As Date = .CurrentRow.Cells("Dia").Value
                    For bytFila As Byte = 0 To .Rows.Count - 1
                        If Not .Item("Dia", bytFila).Value Is Nothing And Not .Item("IDTipoProv", bytFila).Value Is Nothing Then
                            If datDia = .Item("Dia", bytFila).Value And .Item("IDTipoProv", bytFila).Value.ToString = gstrTipoProveeHoteles Then
                                blnExisteHotelenDia = True
                                Exit For
                            End If
                        End If
                    Next
                    If Not blnExisteHotelenDia Then
                        If .Rows.Count = 1 Then
                            Return 1.0
                        Else
                            Return .Item("Item", .Rows.Count - 2).Value + 0.01
                        End If

                    End If


                    For bytFila As Byte = 0 To .Rows.Count - 1
                        blnBuscar = True
                        If Not .Item("Item", bytFila).Value Is Nothing Then
                            'MessageBox.Show(.Item("IDDetRel", bytFila).Value.ToString())

                            If vstrIDDetPadre <> "" And .Item("IDDetRel", bytFila).Value.ToString <> "0" And .Item("IDDetRel", bytFila).Value.ToString <> "" Then
                                If Not IsNumeric(vstrIDDetPadre) Then
                                    If Mid(.Item("IDDetRel", bytFila).Value.ToString, 2) = Mid(vstrIDDetPadre, 2) Then
                                        blnBuscar = False
                                    End If
                                Else
                                    If .Item("IDDetRel", bytFila).Value.ToString = vstrIDDetPadre Then
                                        blnBuscar = False
                                    End If
                                End If
                            End If
                        End If
                        If blnBuscar Then

                            If Convert.ToDateTime(FormatDateTime(datDia, DateFormat.ShortDate)) = Convert.ToDateTime(FormatDateTime(.Item("Dia", bytFila).Value, DateFormat.ShortDate)) Then
                                'MessageBox.Show(.Item("Item", bytFila).Value.ToString())
                                If InStr(.Item("Item", bytFila).Value.ToString, ".") > 0 Then
                                    If Mid(.Item("Item", bytFila).Value.ToString, InStr(.Item("Item", bytFila).Value.ToString, ".")).Length - 1 = 3 Then
                                        sglSumRes = 0.001
                                    Else
                                        sglSumRes = 0.01
                                    End If
                                Else
                                    sglSumRes = 0.01
                                End If
                                'If vstrIDDetPadre = "" Then
                                'MessageBox.Show(Convert.ToSingle(.Item("Item", bytFila).Value.ToString))
                                'sglRet = Convert.ToSingle(.Item("Item", bytFila).Value.ToString) - sglSumRes
                                'Else

                                sglSumRes = 0.001
                                sglRet = Convert.ToSingle(.Item("Item", bytFila).Value.ToString) + sglSumRes
                                'End If
                            End If

                        End If
                        If sglRet <> 0 Then Exit For
                    Next
                End If

                If sglRet = 0 Then

                    blnBuscar = True
                    If Not .Item("Item", 0).Value Is Nothing Then
                        If vstrIDDetPadre <> "" And .Item("IDDetRel", 0).Value.ToString <> "0" And .Item("IDDetRel", 0).Value.ToString <> "" Then
                            If Not IsNumeric(vstrIDDetPadre) Then
                                If Mid(.Item("IDDetRel", 0).Value.ToString, 2) = Mid(vstrIDDetPadre, 2) Then
                                    blnBuscar = False
                                End If
                            Else
                                If .Item("IDDetRel", 0).Value.ToString = vstrIDDetPadre Then
                                    blnBuscar = False
                                End If
                            End If
                        End If
                    End If
                    If Convert.ToDateTime(FormatDateTime(datDia, DateFormat.ShortDate)) < Convert.ToDateTime(FormatDateTime(.Item("Dia", 0).Value, DateFormat.ShortDate)) Then
                        If blnBuscar Then
                            If InStr(.Item("Item", 0).Value.ToString, ".") > 0 Then
                                If Mid(.Item("Item", 0).Value.ToString, InStr(.Item("Item", 0).Value.ToString, ".")).Length - 1 = 3 Then
                                    sglSumRes = 0.001
                                Else
                                    sglSumRes = 0.01
                                End If
                            Else
                                sglSumRes = 0.01
                            End If

                            If vstrIDDetPadre = "" Then
                                sglRet = Convert.ToSingle(.Item("Item", 0).Value.ToString) - sglSumRes
                            Else
                                sglRet = Convert.ToSingle(.Item("Item", 0).Value.ToString) + sglSumRes
                            End If

                        End If
                    Else
                        If vstrModoEdicion = "N" Then
                            blnBuscar = True
                            If Not .Item("Item", .Rows.Count - 2).Value Is Nothing Then
                                If vstrIDDetPadre <> "" And .Item("IDDetRel", .Rows.Count - 2).Value.ToString <> "0" And .Item("IDDetRel", .Rows.Count - 2).Value.ToString <> "" Then
                                    If Not IsNumeric(vstrIDDetPadre) Then
                                        If Mid(.Item("IDDetRel", .Rows.Count - 2).Value.ToString, 2) = Mid(vstrIDDetPadre, 2) Then
                                            blnBuscar = False
                                        End If
                                    Else
                                        If .Item("IDDetRel", .Rows.Count - 2).Value.ToString = vstrIDDetPadre Then
                                            blnBuscar = False
                                        End If
                                    End If
                                End If
                            End If
                            If Convert.ToDateTime(FormatDateTime(datDia, DateFormat.ShortDate)) > Convert.ToDateTime(FormatDateTime(.Item("Dia", .Rows.Count - 2).Value, DateFormat.ShortDate)) Then
                                If blnBuscar Then
                                    If InStr(.Item("Item", .Rows.Count - 2).Value.ToString, ".") > 0 Then
                                        If Mid(.Item("Item", .Rows.Count - 2).Value.ToString, InStr(.Item("Item", .Rows.Count - 2).Value.ToString, ".")).Length - 1 = 3 Then
                                            sglSumRes = 0.001
                                        Else
                                            sglSumRes = 0.01
                                        End If
                                    Else
                                        sglSumRes = 0.01
                                    End If
                                    sglRet = Convert.ToSingle(.Item("Item", .Rows.Count - 2).Value.ToString) + sglSumRes
                                End If
                            End If
                        Else
                            blnBuscar = True
                            If Not .Item("Item", .Rows.Count - 1).Value Is Nothing Then
                                If vstrIDDetPadre <> "" And .Item("IDDetRel", .Rows.Count - 1).Value.ToString <> "0" And .Item("IDDetRel", .Rows.Count - 1).Value.ToString <> "" Then
                                    If Not IsNumeric(vstrIDDetPadre) Then
                                        If Mid(.Item("IDDetRel", .Rows.Count - 1).Value.ToString, 2) = Mid(vstrIDDetPadre, 2) Then
                                            blnBuscar = False
                                        End If
                                    Else
                                        If .Item("IDDetRel", .Rows.Count - 1).Value.ToString = vstrIDDetPadre Then
                                            blnBuscar = False
                                        End If
                                    End If
                                End If
                            End If
                            If blnBuscar Then
                                If Convert.ToDateTime(FormatDateTime(datDia, DateFormat.ShortDate)) > Convert.ToDateTime(FormatDateTime(.Item("Dia", .Rows.Count - 1).Value, DateFormat.ShortDate)) Then
                                    If InStr(.Item("Item", .Rows.Count - 1).Value.ToString, ".") > 0 Then
                                        If Mid(.Item("Item", .Rows.Count - 1).Value.ToString, InStr(.Item("Item", .Rows.Count - 1).Value.ToString, ".")).Length - 1 = 3 Then
                                            sglSumRes = 0.001
                                        Else
                                            sglSumRes = 0.01
                                        End If
                                    Else
                                        sglSumRes = 0.01
                                    End If
                                    sglRet = Convert.ToSingle(.Item("Item", .Rows.Count - 1).Value.ToString) + sglSumRes
                                End If
                            End If
                        End If
                    End If
                End If

                'Ver cruces
                sglSumRes = 0.001
Cruces:
                For Each dgvRow As DataGridViewRow In .Rows
                    If Not dgvRow.Cells("Item").Value Is Nothing Then
                        If Convert.ToSingle(dgvRow.Cells("Item").Value.ToString) = sglRet Then
                            sglRet += sglSumRes
                            GoTo Cruces
                            Exit For
                        End If
                    End If
                Next

                If sglRet = 0 And vstrIDDetPadre <> "" Then
                    Dim sglMayor As Single = 0

                    For bytFila As Byte = 0 To .Rows.Count - 1
                        blnBuscar = True
                        If Not .Item("IDDetRel", bytFila).Value Is Nothing Then
                            If .Item("IDDetRel", bytFila).Value.ToString <> "0" Then
                                If Not IsNumeric(vstrIDDetPadre) Then
                                    If Mid(.Item("IDDetRel", bytFila).Value.ToString, 2) = Mid(vstrIDDetPadre, 2) Then
                                        blnBuscar = False
                                    End If
                                Else
                                    If .Item("IDDetRel", bytFila).Value.ToString = vstrIDDetPadre Then
                                        blnBuscar = False
                                    End If
                                End If
                            End If
                        End If
                        If blnBuscar Then
                            If Not .Item("Item", bytFila).Value Is Nothing Then

                                If Convert.ToSingle(.Item("Item", bytFila).Value) > sglMayor Then
                                    sglMayor = Convert.ToSingle(.Item("Item", bytFila).Value)
                                End If
                            End If
                        End If
                    Next
                    If InStr(sglMayor.ToString, ".") > 0 Then
                        If Mid(sglMayor.ToString, InStr(sglMayor.ToString, ".")).Length - 1 = 3 Then
                            sglSumRes = 0.001
                        Else
                            sglSumRes = 0.01
                        End If
                    Else
                        sglSumRes = 0.01
                    End If

                    sglRet = sglMayor + sglSumRes



                    'Ver cruces
                    sglSumRes = 0.001
Cruces2:
                    For Each dgvRow As DataGridViewRow In .Rows
                        If Not dgvRow.Cells("Item").Value Is Nothing Then
                            If Convert.ToSingle(dgvRow.Cells("Item").Value.ToString) = sglRet Then
                                sglRet += sglSumRes
                                GoTo Cruces
                                Exit For
                            End If
                        End If
                    Next

                End If
            End With

            Return sglRet
        Catch ex As System.Exception
            Throw
        End Try

    End Function

    Public Function blnValidarFormatoMail(ByVal vstrCorreo As String) As Boolean

        Try
            '"^*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.(com|edu|info|gov|int|mil|net|org|biz|name|museum|coop|aero|pro|tv|[a-zA-Z]{2})$"

            Dim strpattern As String = "^*@[-.a-zA-Z0-9]+(\.[-.a-zA-Z0-9]+)*\.[^*]"

            Dim emailRegex As New System.Text.RegularExpressions.Regex(strpattern)
            Dim emailMatch As System.Text.RegularExpressions.Match = emailRegex.Match(vstrCorreo)
            Return emailMatch.Success

        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function blnValidarFormatoURL(ByVal vstrWeb As String) As Boolean
        Try
            Dim strpattern As String = "^(https?|ftp|file)://[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]"
            'Dim strpattern As String = "^[-A-Z0-9+&@#/%?=~_|!:,.;]*[-A-Z0-9+&@#/%=~_|]"

            Dim emailRegex As New System.Text.RegularExpressions.Regex(strpattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
            Dim m As System.Text.RegularExpressions.Match = emailRegex.Match(vstrWeb)

            If m.Captures.Count = 0 Then
                Return False
            Else
                Return True
            End If

            'Dim emailMatch As System.Text.RegularExpressions.Match = emailRegex.Match(vstrWeb)
            'Return emailMatch.Success

        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function blnValidarFormatoMail2(ByVal vstrCorreosCopias As String) As Boolean
        Try

            Dim blnValido As Boolean = True

            Dim strCorreo As String = ""
            If vstrCorreosCopias.Substring(vstrCorreosCopias.Length - 1) <> ";" Then vstrCorreosCopias += ";"
            Dim chrArr() As Char = vstrCorreosCopias.ToCharArray
            For i = 0 To chrArr.Length - 1
                If chrArr(i) <> ";" Then
                    strCorreo += chrArr(i)
                Else
                    If blnValidarFormatoMail(strCorreo) = False Then
                        blnValido = False
                        Exit For
                    End If
                    strCorreo = ""
                End If
            Next


            Return blnValido

        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function lstFiltraBloqueCorreosCliente(ByVal vstrCorreosCompletos As String) As List(Of String)
        Try
            'Dim blnPasandoBloque As Boolean = False
            Dim objLstCorreosBloque As New List(Of String)
            Dim cntBloque As Int32 = 0
            Dim strCorreoBloque As String = ""
            Dim strCorreoBloquePntCm As String = ""
            Dim chrArr() As Char = vstrCorreosCompletos.ToCharArray

            For i = 0 To chrArr.Length - 1
                If chrArr(i) <> ";" Then
                    strCorreoBloque += chrArr(i)
                ElseIf chrArr(i) = ";" Then

                    If i + 1 <> chrArr.Length - 1 Then
                        If chrArr(i + 1) <> " " Then strCorreoBloque += " "
                    End If
                    If Mid(strCorreoBloque, Len(strCorreoBloque) - 1, Len(strCorreoBloque) - 1).Trim = "-" Then strCorreoBloque = strCorreoBloque.Substring(0, strCorreoBloque.Trim.Length - 1).Trim

                    If blnValidarFormatoMail(strCorreoBloque.Trim) = True Then
                        'strCorreoBloque += strCorreoBloque & ";"
                        cntBloque += 1
                    End If
                End If
                'Modificar esta seccion en caso aumenten y/o disminuyan el limite de correos.
                If cntBloque = 450 Then
                    strCorreoBloque = strMinimizarSpace(strCorreoBloque)
                    strCorreoBloquePntCm = strCorreoBloque.Replace(" ", ";")

                    If strCorreoBloquePntCm.Substring(0, 1) = ";" Then strCorreoBloquePntCm = strCorreoBloquePntCm.Remove(0, 1)
                    objLstCorreosBloque.Add(strCorreoBloquePntCm)
                    cntBloque = 0
                    strCorreoBloque = ""
                End If
            Next
            If cntBloque <> 0 Then

                strCorreoBloque = strMinimizarSpace(strCorreoBloque)

                strCorreoBloquePntCm = strCorreoBloque.Replace(" ", ";")
                If strCorreoBloquePntCm.Substring(0, 1) = ";" Then strCorreoBloquePntCm = strCorreoBloquePntCm.Remove(0, 1)
                objLstCorreosBloque.Add(strCorreoBloquePntCm)
            End If

            Return objLstCorreosBloque
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function strMinimizarSpace(ByVal vstrCorreo As String) As String
        Try
            Dim strReturn As String = vstrCorreo.Trim
            Dim strVal As String = ""
            Dim chrArr() As Char = strReturn.ToCharArray

            Dim blnAddVacio As Boolean = False
            For i = 0 To chrArr.Length - 1
                If chrArr(i) = " " Then
                    If chrArr(i + 1) = " " Then
                        blnAddVacio = True
                    End If
                    If Not blnAddVacio Then strVal += " "
                    blnAddVacio = False
                Else
                    strVal += chrArr(i)
                End If
            Next

            Return strVal
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function dtFiltrarDataTable(ByVal vDataTable As DataTable, ByVal psFiltro As String, Optional ByVal psOrder As String = "") As DataTable
        Dim loRows As DataRow()
        Dim dtNuevoDataTable As DataTable        ' Copio la estructura del DataTable original        
        dtNuevoDataTable = vDataTable.Clone()        ' Establezco el filtro y el orden        
        If psOrder = "" Then
            loRows = vDataTable.Select(psFiltro)
        Else
            loRows = vDataTable.Select(psFiltro, psOrder)
        End If        ' Cargo el nuevo DataTable con los datos filtrados        
        For Each ldrRow As DataRow In loRows
            dtNuevoDataTable.ImportRow(ldrRow)
        Next
        ' Retorno el nuevo DataTable        
        Return dtNuevoDataTable
    End Function

    '------------------------------------------------------------------
    'OC
    Public Function GenerarPdfOrdenCompra(ByVal vintNuOrdComInt As Integer) As String
        ' If vdttVoucher.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 36.0F, 100.0F) 'Declaración del documento
        Dim parrafo As New Paragraph ' Declaración de un parrafo
        Dim imgLogo As iTextSharp.text.Image

        Dim dttCabecera As DataTable
        Dim dttDetalle As DataTable

        Dim objOCBN As New clsOrdenCompraBN
        Dim objOCDet_BN As New clsOrdenCompra_DetBN
        dttCabecera = objOCBN.ConsultarList_PDF(vintNuOrdComInt)
        dttDetalle = objOCDet_BN.ConsultarList_PDF(vintNuOrdComInt)

        Dim strNuOrden As String = ""

        If dttCabecera.Rows.Count > 0 Then
            strNuOrden = dttCabecera.Rows(0).Item("NuOrdCom").ToString
        End If

        '--borrar

        Dim strNombreArchivo As String = gstrRutaVouchers & "OrdenCompraNro" & strNuOrden & ".pdf"

        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing

        'devolver oficina para establecer logo
        Dim objBN As New clsTablasApoyoBN.clsUbigeoBN
        'Dim dtoficina As DataTable '= objBN.Consultar_Voucher_Oficina(CInt(strIDVoucher))

        Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"

        Dim strUbigeo As String = ""


        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
        Catch ex As System.Exception
            ' rbytControlError = Err.Number
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Return ""
        End Try
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)

            Documento.Open() 'Abre documento para su escritura
            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & strLogo) 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(38, 730) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(530) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            'For i As Byte = 1 To 12
            '    Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            'Next

            For i As Byte = 1 To 6 '7
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            ''strPaxCoti = 6
            Documento.Add(tblOrdenTitulo(strNuOrden))
            For i As Byte = 1 To 2
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next
            Documento.Add(tblOrdenCompra_DatosProv1(dttCabecera))

            'For i As Byte = 1 To 3
            For i As Byte = 1 To 2
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            Documento.Add(tblOrdenCompra_Detalle(dttDetalle))

            'For i As Byte = 1 To 3
            For i As Byte = 1 To 2
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            Documento.Add(tblOrdenCompra_DatosTotal(dttCabecera))

            'For i As Byte = 1 To 4
            For i As Byte = 1 To 2 '3
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            Documento.Add(tblOrdenCompra_DatosNotas(dttCabecera))
            'For i As Byte = 1 To 5
            For i As Byte = 1 To 2 '3
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next
            Documento.Add(tblOrdenCompra_Firmas())


            'Documento.Add(New Paragraph(" ", NewFont))
            'Documento.Add(tblVoucherPara(strDescProveedor, strDireccion))
            'Documento.Add(New Paragraph(" ", NewFont))
            'Documento.Add(tblMasDatosVoucher(strTitulo, strCodReservaProv, strPaxCoti, strNacionalidad, _
            '                                 strResponsable))
            'Documento.Add(New Paragraph(" ", NewFont))

            'Dim NewFont2 As New Font(bsDefault, 12, Font.COURIER)
            'Documento.Add(New Paragraph("SERVICIOS", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            'Documento.Add(New Paragraph(" ", NewFont))
            'Documento.Add(tblBorderSeparacionParrafo(True))
            'Documento.Add(tblServiciosVoucherHotel(vdttVoucher))
            'Documento.Add(tblBorderSeparacionParrafo(False))

            'For Each dRowItem As DataRow In vdttVoucher.Rows
            '    Dim strDet As String = dRowItem("DetalleTipoDesc").ToString().Trim
            '    If strDet <> "" Then
            '        If InStr(strDescripcionVariante.Trim, strDet) = 0 Then
            '            strDescripcionVariante += strDet & ", "
            '        End If
            '    End If
            'Next

            'If strDescripcionVariante.Trim <> "" Then strDescripcionVariante = strDescripcionVariante.Substring(0, strDescripcionVariante.Length - 2).Trim
            'Documento.Add(New Paragraph(" ", NewFont))

            'Documento.Add(tblExtrasNoIncluidoHotel(strDescripcionVariante, strObservVoucher, blnExtraNoIncluido, vstrIDProveedor))

            pdfw.Flush()
            Documento.Close()

            System.Diagnostics.Process.Start(strNombreArchivo)

            Return strNombreArchivo
        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try

    End Function



    Public Function GenerarPdfVoucherHotel(ByVal vdttVoucher As Data.DataTable, ByRef rbytControlError As Byte, ByVal vstrIDProveedor As String) As String
        If vdttVoucher.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 36.0F, 100.0F) 'Declaración del documento
        Dim parrafo As New Paragraph ' Declaración de un parrafo
        Dim imgLogo As iTextSharp.text.Image



        Dim strIDVoucher As String = vdttVoucher(0)("IDVoucher")
        Dim strIDFile As String = vdttVoucher(0)("IDFile")
        Dim strNombreArchivo As String = gstrRutaVouchers & "VoucherNro" & strIDVoucher & ".pdf"
        Dim strDescProveedor As String = vdttVoucher(0)("DescProveedor")
        Dim strDireccion As String = vdttVoucher(0)("Direccion")

        Dim strDescProveedor_Int As String = vdttVoucher(0)("DescProveedor_Int")
        Dim strDireccion_Int As String = vdttVoucher(0)("Direccion_Int")

        Dim strPax As String = vdttVoucher(0)("Pax")
        Dim strPaxCoti As String = vdttVoucher(0)("NroPaxCoti")
        Dim strResponsable As String = vdttVoucher(0)("Responsable")
        Dim strTitulo As String = vdttVoucher(0)("Titulo")
        Dim strCodReservaProv As String = vdttVoucher(0)("CodReservaProv")
        Dim strIDIdioma As String = vdttVoucher(0)("IDIdioma")

        Dim strNacionalidad As String = vdttVoucher(0)("Nacionalidad")
        Dim strFechaOut As String = vdttVoucher(0)("FechaOut")
        Dim strObservVoucher As String = If(IsDBNull(vdttVoucher(0)("ObservVoucher")) = True, "", vdttVoucher(0)("ObservVoucher"))
        Dim blnExtraNoIncluido As Boolean = If(IsDBNull(vdttVoucher(0)("ServExportac")) = True, False, vdttVoucher(0)("ServExportac"))
        Dim strDescripcionVariante As String = ""
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing

        'devolver oficina para establecer logo
        Dim objBN As New clsTablasApoyoBN.clsUbigeoBN
        Dim dtoficina As DataTable = objBN.Consultar_Voucher_Oficina(CInt(strIDVoucher))

        Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"
        Dim strUbigeo As String = ""
        If dtoficina.Rows.Count > 0 Then
            strUbigeo = dtoficina.Rows(0).Item(0).ToString
            Select Case strUbigeo
                Case gstrSantiago
                    strLogo = "SETOURS LOGO-FINAL-CHILE.jpg"
                Case gstrBuenosAires
                    strLogo = "SETOURS LOGO-FINAL-ARGENTINA.jpg"
                Case Else
                    strLogo = "SETOURS LOGO-FINAL.jpg"
            End Select

        End If

        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
        Catch ex As System.Exception
            rbytControlError = Err.Number
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Return ""
        End Try
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)

            Documento.Open() 'Abre documento para su escritura
            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & strLogo) 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(38, 730) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(530) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            For i As Byte = 1 To 10
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next
            'strPaxCoti = 6
            Documento.Add(tblVoucherTitulo(strIDVoucher, strIDFile))
            Documento.Add(New Paragraph(" ", NewFont))
            If strDescProveedor_Int.Trim.Length > 0 Then
                Documento.Add(tblVoucherPara(strDescProveedor_Int, strDireccion_Int))
            Else
                Documento.Add(tblVoucherPara(strDescProveedor, strDireccion))
            End If
            'Documento.Add(tblVoucherPara(strDescProveedor, strDireccion))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblMasDatosVoucher(strTitulo, strCodReservaProv, strPaxCoti, strNacionalidad, _
                                             strResponsable))
            Documento.Add(New Paragraph(" ", NewFont))

            Dim NewFont2 As New Font(bsDefault, 12, Font.COURIER)
            Documento.Add(New Paragraph("SERVICIOS", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblBorderSeparacionParrafo(True))
            Documento.Add(tblServiciosVoucherHotel(vdttVoucher))
            Documento.Add(tblBorderSeparacionParrafo(False))

            For Each dRowItem As DataRow In vdttVoucher.Rows
                Dim strDet As String = dRowItem("DetalleTipoDesc").ToString().Trim
                If strDet <> "" Then
                    If InStr(strDescripcionVariante.Trim, strDet) = 0 Then
                        strDescripcionVariante += strDet & ", "
                    End If
                End If
            Next

            If strDescripcionVariante.Trim <> "" Then strDescripcionVariante = strDescripcionVariante.Substring(0, strDescripcionVariante.Length - 2).Trim
            Documento.Add(New Paragraph(" ", NewFont))

            Documento.Add(tblExtrasNoIncluidoHotel(strDescripcionVariante, strObservVoucher, blnExtraNoIncluido, vstrIDProveedor))

            pdfw.Flush()
            Documento.Close()
            Return strNombreArchivo
        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
     

    End Function


    Public Function GenerarPdfVoucherRestaurant(ByVal vdttVoucher As Data.DataTable, ByRef rbytControlError As Byte, ByVal vstrIDProveedor As String) As String
        If vdttVoucher.Rows.Count = 0 Then Return ""
        Dim Documento As New Document 'Declaración del documento
        Dim parrafo As New Paragraph ' Declaración de un parrafo
        Dim imgLogo As iTextSharp.text.Image

        Dim strIDVoucher As String = vdttVoucher(0)("IDVoucher")
        Dim strIDFile As String = vdttVoucher(0)("IDFile")
        Dim strNombreArchivo As String = gstrRutaVouchers & "VoucherNro" & strIDVoucher & ".pdf"
        Dim strDescProveedor As String = vdttVoucher(0)("DescProveedor")
        Dim strDireccion As String = vdttVoucher(0)("Direccion")
        Dim strPax As String = vdttVoucher(0)("Pax")
        Dim strPaxCoti As String = vdttVoucher(0)("NroPaxCoti")
        Dim strResponsable As String = vdttVoucher(0)("Responsable")
        Dim strTitulo As String = vdttVoucher(0)("Titulo")
        Dim strCodReservaProv As String = vdttVoucher(0)("CodReservaProv")
        Dim strIDIdioma As String = vdttVoucher(0)("IDIdioma")
        Dim strNacionalidad As String = vdttVoucher(0)("Nacionalidad")
        Dim strFechaOut As String = vdttVoucher(0)("FechaOut")
        Dim strObservVoucher As String = If(IsDBNull(vdttVoucher(0)("ObservVoucher")) = True, "", vdttVoucher(0)("ObservVoucher"))
        Dim blnExtraNoIncluido As Boolean = If(IsDBNull(vdttVoucher(0)("ExtraNoIncluido")) = True, False, vdttVoucher(0)("ExtraNoIncluido"))
        Dim strDescripcionVariante As String = ""

        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing

        'devolver oficina para establecer logo
        Dim objBN As New clsTablasApoyoBN.clsUbigeoBN
        Dim dtoficina As DataTable = objBN.Consultar_Voucher_Oficina(CInt(strIDVoucher))

        Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"
        Dim strUbigeo As String = ""
        If dtoficina.Rows.Count > 0 Then
            strUbigeo = dtoficina.Rows(0).Item(0).ToString
            Select Case strUbigeo
                Case "000110"
                    strLogo = "SETOURS LOGO-FINAL-CHILE.jpg"
                Case "000113"
                    strLogo = "SETOURS LOGO-FINAL-ARGENTINA.jpg"
                Case Else
                    strLogo = "SETOURS LOGO-FINAL.jpg"
            End Select

        End If



        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
        Catch ex As System.Exception
            rbytControlError = Err.Number
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Return ""
        End Try
        'pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create))
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)

            Documento.Open() 'Abre documento para su escritura
            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & strLogo) 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(38, 730) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(530) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            For i As Byte = 1 To 10
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            Documento.Add(tblVoucherTitulo(strIDVoucher, strIDFile))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblVoucherPara(strDescProveedor, strDireccion))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblMasDatosVoucherRestaurantes(strTitulo, strPaxCoti, strNacionalidad, _
                                             strResponsable))
            Documento.Add(New Paragraph(" ", NewFont))
            Dim NewFont2 As New Font(bsDefault, 12, Font.COURIER)
            Documento.Add(New Paragraph("SERVICIOS", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblBorderSeparacionParrafo(True))
            Documento.Add(tblServiciosVoucherRestaurantes(vdttVoucher))
            Documento.Add(tblBorderSeparacionParrafo(False))

            For Each dRowItem As DataRow In vdttVoucher.Rows
                Dim strDet As String = dRowItem("DetalleTipoDesc").ToString().Trim
                If strDet <> "" Then
                    If InStr(strDescripcionVariante.Trim, strDet) = 0 Then
                        strDescripcionVariante += strDet & ", "
                    End If
                End If
            Next

            If strDescripcionVariante.Trim <> "" Then strDescripcionVariante = strDescripcionVariante.Substring(0, strDescripcionVariante.Length - 2).Trim
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblExtrasNoIncluidoRestaurantes(strDescripcionVariante, strObservVoucher, blnExtraNoIncluido, vstrIDProveedor))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Documento.Open() 'Abre documento para su escritura

            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            'imgLogo.SetAbsolutePosition(30, 745) 'Posición en el eje cartesiano
            'imgLogo.ScaleAbsoluteWidth(200) 'Ancho de la imagen
            'imgLogo.ScaleAbsoluteHeight(70) 'Altura de la imagen
            'Documento.Add(imgLogo) ' Agrega la imagen al documento

            'Documento.Add(New Paragraph(" ")) 'Salto de linea
            'Documento.Add(New Paragraph(" ")) 'Salto de linea
            'Documento.Add(New Paragraph(" ")) 'Salto de linea
            ''Texto Cabecera Voucher(Logo)
            'Documento.Add(New Paragraph("Av. Comandante Espinar 229, Miraflores - Lima", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'Documento.Add(New Paragraph("Tel: +51(1)202-4620  -  Fax: +51(1)202-4630", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'Documento.Add(New Paragraph("RUC: " & gstrRuc, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            'Documento.Add(New Paragraph(" ")) 'Salto de linea


            'Documento.Add(tblVoucherTitulo(strIDVoucher, strIDFile))
            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblVoucherPara(strDescProveedor, strDireccion))
            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblMasDatosVoucherRestaurantes(strTitulo, strPax, strNacionalidad, _
            '                                 strResponsable))
            'Documento.Add(New Paragraph(" "))
            'Documento.Add(New Paragraph("SERVICIOS", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblServiciosVoucherRestaurantes(vdttVoucher))

            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblExtrasNoIncluidoRestaurantes(strObservVoucher, blnExtraNoIncluido))

            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo

        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Public Function GenerarPdfVoucherServicios(ByVal vdttVoucher As Data.DataTable, ByRef rbytControlError As Byte, ByVal vstrIDProveedor As String) As String
        If vdttVoucher.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 36.0F, 100.0F) 'Declaración del documento
        Dim parrafo As New Paragraph ' Declaración de un parrafo
        Dim imgLogo As iTextSharp.text.Image

        Dim strIDVoucher As String = vdttVoucher(0)("IDVoucher")
        Dim strIDFile As String = vdttVoucher(0)("IDFile")
        Dim strNombreArchivo As String = gstrRutaVouchers & "VoucherNro" & strIDVoucher & ".pdf"
        Dim strDescProveedor As String = vdttVoucher(0)("DescProveedor")
        Dim strDireccion As String = vdttVoucher(0)("Direccion")
        Dim strPax As String = vdttVoucher(0)("Pax")
        Dim strPaxCoti As String = vdttVoucher(0)("NroPaxCoti")
        Dim strResponsable As String = vdttVoucher(0)("Responsable")
        Dim strTitulo As String = vdttVoucher(0)("Titulo")
        Dim strIDIdioma As String = vdttVoucher(0)("IDIdioma")
        Dim strNacionalidad As String = vdttVoucher(0)("Nacionalidad")
        Dim strFechaOut As String = vdttVoucher(0)("FechaOut")
        Dim bytCantidadServicios As Byte = If(IsDBNull(vdttVoucher(0)("CantServicios")) = True, 0, vdttVoucher(0)("CantServicios"))
        Dim strDescTarifa As String = If(IsDBNull(vdttVoucher(0)("DescripcionTarifa")) = True, "", vdttVoucher(0)("DescripcionTarifa"))
        Dim strObservVoucher As String = If(IsDBNull(vdttVoucher(0)("ObservVoucher")) = True, "", vdttVoucher(0)("ObservVoucher"))
        Dim blnExtraNoIncluido As Boolean = If(IsDBNull(vdttVoucher(0)("ExtraNoIncluido")) = True, False, vdttVoucher(0)("ExtraNoIncluido"))
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        'Dim rbytControlError As Byte = 0

        'devolver oficina para establecer logo
        Dim objBN As New clsTablasApoyoBN.clsUbigeoBN
        Dim dtoficina As DataTable = objBN.Consultar_Voucher_Oficina(CInt(strIDVoucher))

        Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"
        Dim strUbigeo As String = ""
        If dtoficina.Rows.Count > 0 Then
            strUbigeo = dtoficina.Rows(0).Item(0).ToString
            Select Case strUbigeo
                Case "000110"
                    strLogo = "SETOURS LOGO-FINAL-CHILE.jpg"
                Case "000113"
                    strLogo = "SETOURS LOGO-FINAL-ARGENTINA.jpg"
                Case Else
                    strLogo = "SETOURS LOGO-FINAL.jpg"
            End Select
        End If


        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
        Catch ex As System.Exception
            rbytControlError = Err.Number
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Return ""
        End Try
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)

            Documento.Open() 'Abre documento para su escritura
            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & strLogo) 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(38, 730) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(530) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            For i As Byte = 1 To 10
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            If bytCantidadServicios <> 1 Then strDescTarifa = ""

            Documento.Add(tblVoucherTitulo(strIDVoucher, strIDFile))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblVoucherPara(strDescProveedor, strDireccion))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblMasDatosVoucherServicios(strTitulo, strPaxCoti, strNacionalidad, _
                                             strResponsable))
            Documento.Add(New Paragraph(" ", NewFont))
            Dim NewFont2 As New Font(bsDefault, 12, Font.COURIER)
            Documento.Add(New Paragraph("SERVICIOS", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblBorderSeparacionParrafo(True))
            Documento.Add(tblServiciosVoucherServicios(vdttVoucher))
            Documento.Add(tblBorderSeparacionParrafo(False))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblExtrasNoIncluido(strObservVoucher, strDescTarifa, vstrIDProveedor))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Documento.Open() 'Abre documento para su escritura

            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            'imgLogo.SetAbsolutePosition(30, 745) 'Posición en el eje cartesiano
            'imgLogo.ScaleAbsoluteWidth(200) 'Ancho de la imagen
            'imgLogo.ScaleAbsoluteHeight(70) 'Altura de la imagen
            'Documento.Add(imgLogo) ' Agrega la imagen al documento

            'Documento.Add(New Paragraph(" ")) 'Salto de linea
            'Documento.Add(New Paragraph(" ")) 'Salto de linea
            'Documento.Add(New Paragraph(" ")) 'Salto de linea

            ''Texto Cabecera Voucher(Logo)
            'Documento.Add(New Paragraph("Av. Comandante Espinar 229, Miraflores - Lima", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'Documento.Add(New Paragraph("Tel: +51(1)202-4620  -  Fax: +51(1)202-4630", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'Documento.Add(New Paragraph("RUC: " & gstrRuc, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            'Documento.Add(New Paragraph(" ")) 'Salto de linea

            'Documento.Add(tblVoucherTitulo(strIDVoucher, strIDFile))
            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblVoucherPara(strDescProveedor, strDireccion))
            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblMasDatosVoucherServicios(strTitulo, strPax, strNacionalidad, _
            '                                 strResponsable))
            'Documento.Add(New Paragraph(" "))
            'Documento.Add(New Paragraph("SERVICIOS", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblServiciosVoucherServicios(vdttVoucher))
            'Documento.Add(New Paragraph(" "))
            'Documento.Add(tblExtrasNoIncluido(strObservVoucher))

            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo

        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Public Function GenerarPdfBibliaServicios(ByVal vdttBiblia As Data.DataTable, _
                                              ByRef rintNumErrorGeneracion As Int16) As String
        If vdttBiblia.Rows.Count = 0 Then Return ""
        Dim strFechaIn As String = vdttBiblia(0)("FechaIn").ToString
        Dim strFechaOut As String = vdttBiblia(0)("FechaOut").ToString

        Dim strFechaInicialLet As String = CDate(strFechaIn).Day.ToString("00") & " " & strMesenIdioma(CDate(strFechaIn), "es-ES", True)
        Dim strFechaOutLet As String = CDate(strFechaOut).Day.ToString("00") & " " & strMesenIdioma(CDate(strFechaOut), "es-ES", True)
        Dim strArchivoGenerado As String = gstrRutaImagenes & "Biblia " & strFechaInicialLet & " - " & strFechaOutLet & ".pdf"
        Dim Documento As Document = New Document(PageSize.A4, 25.0F, 25.0F, 15.0F, 100.0F) 'Declaración del documento
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strArchivoGenerado, FileMode.Create))
        Catch ex As Exception
            rintNumErrorGeneracion = Err.Number
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Return ""
        End Try
        Try
            Dim ObjEvent As New PDFFooter()
            ObjEvent.ActionFooterSelect = "BibliaHeader"
            ObjEvent.FechaIn_BibliaReport = strFechaIn
            ObjEvent.FechaOut_BibliaReport = strFechaOut
            pdfw.PageEvent = ObjEvent
            Documento.Open()

            Dim LstDiasFormato As New List(Of String)
            For Each dRowItem As DataRow In vdttBiblia.Rows
                If Not LstDiasFormato.Contains(dRowItem("DiaFormat").ToString.Trim) Then
                    LstDiasFormato.Add(dRowItem("DiaFormat").ToString.Trim)
                End If
            Next

            'Documento.Add(tblDetallesBibliaServicio(LstDiasFormato, vdttBiblia, Documento))
            For Each strItemDia As String In LstDiasFormato
                Dim dtFiltradoBibliaxDia As Data.DataTable = dtFiltrarDataTable(vdttBiblia, "DiaFormat='" & strItemDia & "'")
                Documento.Add(tblDiaBibliaServicio(strItemDia))
                Documento.Add(tblSubDetallesBibliaServicio(dtFiltradoBibliaxDia))
                Documento.Add(tblCantidadServiciosxDiaBibliaServicio(dtFiltradoBibliaxDia.Rows.Count.ToString))
            Next

            pdfw.Flush()
            Documento.Close()
            Return strArchivoGenerado
        Catch ex As Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Private Function tblDiaBibliaServicio(ByVal vstrDiaFormat As String) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
            Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
            Dim NewFontBoldtLt As Font = New Font(bsDefault, 7, Font.BOLD)
            Dim objPDFTabla As New PdfPTable(1)
            With objPDFTabla
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.BorderWidth = 0
                .DefaultCell.Padding = 4.0F
                .DefaultCell.PaddingLeft = 0.0F
                .DefaultCell.PaddingRight = 0.0F
                .WidthPercentage = 100
                .DefaultCell.BorderWidthBottom = 1.0F
                .AddCell(New Paragraph("Día : " & vstrDiaFormat, NewFontBoldtLt))

                .DefaultCell.Padding = 4.0F
            End With
            Return objPDFTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblCantidadServiciosxDiaBibliaServicio(ByVal vstrCantidadServicio As String) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
            Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
            Dim NewFontBoldtLt As Font = New Font(bsDefault, 7, Font.BOLD)
            Dim objPDFTabla As New PdfPTable(1)
            With objPDFTabla
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.BorderWidth = 0
                .DefaultCell.Padding = 4.0F
                .DefaultCell.PaddingLeft = 0.0F
                .DefaultCell.PaddingRight = 0.0F
                .WidthPercentage = 100

                .AddCell(New Paragraph("Cantidad de servicios : " & vstrCantidadServicio, NewFontBoldtLt))

                .DefaultCell.Padding = 4.0F
            End With
            Return objPDFTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    ' ''Private Function tblDetallesBibliaServicio(ByVal vLstDiaFormatos As List(Of String), _
    ' ''                                           ByVal vdtDatosGeneralBiblia As DataTable, _
    ' ''                                           ByRef rDocPDF As Document) As PdfPTable
    ' ''    Try
    ' ''        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
    ' ''        Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
    ' ''        Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
    ' ''        Dim NewFontBoldtLt As Font = New Font(bsDefault, 7, Font.BOLD)
    ' ''        Dim objPDFTabla As New PdfPTable(1)
    ' ''        With objPDFTabla
    ' ''            .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
    ' ''            .DefaultCell.BorderWidth = 0
    ' ''            .DefaultCell.Padding = 4.0F
    ' ''            .WidthPercentage = 100

    ' ''            '.AddCell(New Paragraph(" ", NewFont))

    ' ''            For Each strItemDia As String In vLstDiaFormatos
    ' ''                Dim dtFiltradoBibliaxDia As Data.DataTable = dtFiltrarDataTable(vdtDatosGeneralBiblia, "DiaFormat='" & strItemDia & "'")
    ' ''                Dim rblnFirstPage As Boolean = True
    ' ''                .DefaultCell.PaddingLeft = 0.0F
    ' ''                .DefaultCell.PaddingRight = 0.0F
    ' ''                .AddCell(New Paragraph("Día : " & strItemDia, NewFontBoldtLt))
    ' ''                .AddCell(tblSubDetallesBibliaServicio(dtFiltradoBibliaxDia))
    ' ''                .AddCell(New Paragraph("Cantidad de Servicios : " & dtFiltradoBibliaxDia.Rows.Count.ToString(), NewFontBoldtLt))
    ' ''            Next
    ' ''            .DefaultCell.Padding = 4.0F
    ' ''        End With
    ' ''        '.SetTotalWidth(New Single() {3.0F, 6.0F, 1.8F, 3.5F, 2.0F, 18.0F, 3.0F, 4.0F, 5.0F, 1.0F})
    ' ''        Return objPDFTabla
    ' ''    Catch ex As Exception
    ' ''        Throw
    ' ''    End Try
    ' ''End Function

    Private Function tblSubDetallesBibliaServicio(ByVal vddtDatosFiltradosxDia As Data.DataTable) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
            Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
            Dim NewFontLt As Font = New Font(bsDefault, 7, Font.COURIER)
            Dim tblPdfCot As New PdfPTable(1)
            With tblPdfCot
                '.DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.BorderWidth = 0
                '.SetTotalWidth(New Single() {3.0F, 6.0F, 1.8F, 3.5F, 2.0F, 18.0F, 3.0F, 4.0F, 5.0F, 1.0F})
                .WidthPercentage = 100
                .DefaultCell.Padding = 2.0F

                For Each dRowItem As DataRow In vddtDatosFiltradosxDia.Rows
                    .AddCell(tblInsertarDatosFileBibliaServicios(dRowItem))
                    .AddCell(tblInsertarTituloFileBibliaServicios(dRowItem))
                    .AddCell(tblInsertarDescripcionBibliaServicios("Obs. Biblia", If(IsDBNull(dRowItem("ObservBiblia")) = True, "", dRowItem("ObservBiblia"))))
                    .AddCell(tblInsertarDescripcionBibliaServicios("Obs. Int.", If(IsDBNull(dRowItem("ObservInterno")) = True, "", dRowItem("ObservInterno"))))
                    .AddCell(tblInsertarDescripcionBibliaServicios("Cód. PNR", If(IsDBNull(dRowItem("CodigoPNR")) = True, "", dRowItem("CodigoPNR"))))
                    .AddCell(New Paragraph(StrDup(284, "-"), NewFontLt))
                Next
            End With
            Return tblPdfCot
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblInsertarDatosFileBibliaServicios(ByVal vdrDatoFiltrados As Data.DataRow) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
            Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
            Dim NewFontLt As Font = New Font(bsDefault, 7, Font.COURIER)
            Dim tblPdfCot As New PdfPTable(10)
            With tblPdfCot
                '.DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.PaddingLeft = 0.0F
                .DefaultCell.PaddingRight = 0.0F
                .DefaultCell.PaddingBottom = 2.0F
                .DefaultCell.PaddingTop = 2.0F
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {3.0F, 6.0F, 1.8F, 3.5F, 2.0F, 18.0F, 3.0F, 4.0F, 5.0F, 1.0F})
                .WidthPercentage = 100

                .AddCell(New Paragraph(vdrDatoFiltrados("IDFile"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("DescCliente"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("Hora"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("DescUbigeo"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("PaxL"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("Servicio"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("IDIdioma"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("GuiaProgramado"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("DescProvee") & " " & _
                                       vdrDatoFiltrados("TransportistaProgramado"), NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("ResponsableOpe"), NewFontLt))
            End With
            Return tblPdfCot
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblInsertarDescripcionBibliaServicios(ByVal vstrDescripcion As String, _
                                                           ByVal vstrValorDescripcion As String) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
            Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
            Dim NewFontLt As Font = New Font(bsDefault, 7, Font.COURIER)
            Dim tblPdfCot As New PdfPTable(3)
            With tblPdfCot
                '.DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.PaddingLeft = 0.0F
                .DefaultCell.PaddingRight = 0.0F
                .DefaultCell.PaddingBottom = 2.0F
                .DefaultCell.PaddingTop = 2.0F
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {2.8F, 0.2F, 44.3F})
                .WidthPercentage = 100

                .AddCell(New Paragraph(vstrDescripcion, NewFontLt))
                .AddCell(New Paragraph(":", NewFontLt))
                .AddCell(New Paragraph(vstrValorDescripcion, NewFontLt))
            End With
            Return tblPdfCot
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblInsertarTituloFileBibliaServicios(ByVal vdrDatoFiltrados As Data.DataRow) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
            Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
            Dim NewFontLt As Font = New Font(bsDefault, 7, Font.COURIER)
            Dim tblPdfCot As New PdfPTable(2)
            With tblPdfCot
                '.DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.PaddingLeft = 0.0F
                .DefaultCell.PaddingRight = 0.0F
                .DefaultCell.PaddingBottom = 2.0F
                .DefaultCell.PaddingTop = 2.0F
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {3.0F, 44.3F})
                .WidthPercentage = 100

                .AddCell(New Paragraph(" ", NewFontLt))
                .AddCell(New Paragraph(vdrDatoFiltrados("Titulo"), NewFontLt))
            End With
            Return tblPdfCot
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GenerarPdfOrdenPago(ByVal vdttOrdenPago As Data.DataTable) As String
        If vdttOrdenPago.Rows.Count = 0 Then Return ""
        Dim Documento As New Document(PageSize.A4, 36.0F, 36.0F, 120.0F, 36.0F)
        'Dim Documento As New Document
        Dim parrafo As New Paragraph
        Dim intIDOrdenPago As Integer = vdttOrdenPago(0)("IDOrdPag")
        Dim strFechaPago As String = vdttOrdenPago(0)("FechaPago")
        Dim strObservacion As String = vdttOrdenPago(0)("Observacion")
        Dim strUsuarioReservas As String = vdttOrdenPago(0)("ResponsableReservas")
        Dim dblDetraccion As Double = vdttOrdenPago(0)("SSDetraccion")
        Dim strNoFormaPagoTarjCred As String = vdttOrdenPago(0)("NoFormaPagoTarjCred")
        'Dim strFechaHoy As String = Date.Now.ToShortDateString
        Dim strFecEmision As String = Format(vdttOrdenPago(0)("FechaEmision"), "dd/MM/yyyy")
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
        Dim strNombreArchivo As String = gstrRutaImagenes & "ORDEN PAGO Nro." & intIDOrdenPago.ToString & ".pdf"
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim strOficinaFacturacion As String = vdttOrdenPago(0)("Oficina_Facturacion")
        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            Dim ObjEvent As New PDFFooter()
            ObjEvent.ActionFooterSelect = "DefaultImageHeader"
            ObjEvent.EsOrdenPago = True
            pdfw.PageEvent = ObjEvent
            Documento.Open()
            'Documento.Add(tblCabeceraIzquierdaOrdenPago(strFechaHoy, strFechaPago))
            Documento.Add(tblCabeceraIzquierdaOrdenPago(strFecEmision, strFechaPago))
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Saltar Linea
            Documento.Add(tblTituloOrdenPago(intIDOrdenPago.ToString))
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 2, Font.NORMAL))) 'Saltar Linea
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 2, Font.NORMAL))) 'Saltar Linea
            ObjEvent.EsOrdenPago = False
            Documento.Add(tblOrdenPago(vdttOrdenPago))
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Saltar Linea
            Documento.Add(tblFooterOrdenPago(strObservacion, strNoFormaPagoTarjCred))
            Documento.Add(tblFooterResponsableOrdenPago(strUsuarioReservas)) 'Saltar Linea

            ''Lineas Verticales
            'Dim Lin1 As PdfContentByte = pdfw.DirectContent
            'Lin1.SetLineWidth(0.03) 'configurando el ancho de linea
            'Lin1.MoveTo(202.0F, 608.0F) 'MoveTo indica el punto de inicio
            'Lin1.LineTo(202.0F, 591.0F) 'LineTo indica hacia donde se dibuja la linea
            'Lin1.Stroke() 'traza la linea actual y se puede iniciar una nueva

            ''Lineas Verticales
            'Dim Lin2 As PdfContentByte = pdfw.DirectContent
            'Lin2.SetLineWidth(0.03) 'configurando el ancho de linea
            'Lin2.MoveTo(202.0F, 540.0F) 'MoveTo indica el punto de inicio
            'Lin2.LineTo(202.0F, 505.0F) 'LineTo indica hacia donde se dibuja la linea
            'Lin2.Stroke() 'traza la linea actual y se puede iniciar una nueva

            ''Lineas Verticales
            'Dim Lin3 As PdfContentByte = pdfw.DirectContent
            'Lin3.SetLineWidth(0.03) 'configurando el ancho de linea
            'Lin3.MoveTo(376.0F, 608.0F) 'MoveTo indica el punto de inicio
            'Lin3.LineTo(376.0F, 591.0F) 'LineTo indica hacia donde se dibuja la linea
            'Lin3.Stroke() 'traza la linea actual y se puede iniciar una nueva

            ''Lineas Verticales
            'Dim Lin4 As PdfContentByte = pdfw.DirectContent
            'Lin4.SetLineWidth(0.03) 'configurando el ancho de linea
            'Lin4.MoveTo(376.0F, 557.0F) 'MoveTo indica el punto de inicio
            'Lin4.LineTo(376.0F, 540.0F) 'LineTo indica hacia donde se dibuja la linea
            'Lin4.Stroke() 'traza la linea actual y se puede iniciar una nueva

            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo
        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Private Function tblCargarGuias(ByVal vdttOrdGuias As Data.DataTable) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            Dim objTabla As New PdfPTable(2)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {4.3F, 19.3F})
                .WidthPercentage = 100
                Dim blnPintoDescReg As Boolean = True

                If vdttOrdGuias.Rows.Count = 0 Then
                    .AddCell(New Paragraph("Guía Asignado", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                Else
                    For Each dRow As Data.DataRow In vdttOrdGuias.Rows
                        .AddCell(New Paragraph(If(blnPintoDescReg, "Guía Asignado", " "), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("NombreCorto"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        blnPintoDescReg = False
                    Next
                End If

            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {4.0F, 19.3F})
            '    .WidthPercentage = 100
            '    Dim blnPintoDescReg As Boolean = True

            '    If vdttOrdGuias.Rows.Count = 0 Then
            '        .AddCell(New Paragraph("Guía Asignado", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    Else
            '        For Each dRow As Data.DataRow In vdttOrdGuias.Rows
            '            .AddCell(New Paragraph(If(blnPintoDescReg, "Guía Asignado", " "), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '            .AddCell(New Paragraph(dRow("NombreCorto"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '            blnPintoDescReg = False
            '        Next
            '    End If

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCargarTransportistas(ByVal vdttOrdTransportista As Data.DataTable) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {4.3F, 19.3F})
                .WidthPercentage = 100
                Dim blnPintoDescReg As Boolean = True

                If vdttOrdTransportista.Rows.Count = 0 Then
                    .AddCell(New Paragraph("Transportista Asignado", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                Else
                    For Each dRow As Data.DataRow In vdttOrdTransportista.Rows
                        .AddCell(New Paragraph(If(blnPintoDescReg, "Transportista Asignado", " "), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("NombreCorto"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        blnPintoDescReg = False
                    Next
                End If

            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {4.0F, 19.3F})
            '    .WidthPercentage = 100
            '    Dim blnPintoDescReg As Boolean = True

            '    If vdttOrdTransportista.Rows.Count = 0 Then
            '        .AddCell(New Paragraph("Transportista Asignado", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    Else
            '        For Each dRow As Data.DataRow In vdttOrdTransportista.Rows
            '            .AddCell(New Paragraph(If(blnPintoDescReg, "Transportista Asignado", " "), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '            .AddCell(New Paragraph(dRow("NombreCorto"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '            blnPintoDescReg = False
            '        Next
            '    End If

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCargarTrasladista(ByVal vdttOrdTrasladista As Data.DataTable) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {4.3F, 19.3F})
                .WidthPercentage = 100
                Dim blnPintoDescReg As Boolean = True

                If vdttOrdTrasladista.Rows.Count = 0 Then
                    .AddCell(New Paragraph("Trasladista Asignado", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                Else
                    For Each dRow As Data.DataRow In vdttOrdTrasladista.Rows
                        .AddCell(New Paragraph(If(blnPintoDescReg, "Trasladista Asignado", " "), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("NombreCorto"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        blnPintoDescReg = False
                    Next
                End If

            End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    'Private Function tblCargarTrasladistaOrden(ByVal vdttOrdTrasladista As Data.DataTable) As PdfPTable
    '    Try
    '        Dim objTabla As New PdfPTable(2)
    '        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
    '        Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
    '        With objTabla
    '            .DefaultCell.BorderWidth = 0
    '            .SetTotalWidth(New Single() {4.3F, 19.3F})
    '            .WidthPercentage = 100
    '            Dim blnPintoDescReg As Boolean = True

    '            If vdttOrdTrasladista.Rows.Count = 0 Then
    '                .AddCell(New Paragraph("Trasladista Asignado", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
    '                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
    '            Else
    '                For Each dRow As Data.DataRow In vdttOrdTrasladista.Rows
    '                    .AddCell(New Paragraph(If(blnPintoDescReg, "Trasladista Asignado", " "), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
    '                    .AddCell(New Paragraph(dRow("TrasladistaProgramado"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
    '                    blnPintoDescReg = False
    '                Next
    '            End If

    '        End With

    '        Return objTabla
    '    Catch ex As System.Exception
    '        Throw
    '    End Try
    'End Function

    Private Function tblCargarSubServicioGuias(ByVal vdttOrdSubServiciosGuia As Data.DataTable, _
                                               ByVal vblnEsTransportista As Boolean) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(11)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                '.SetTotalWidth(New Single() {2.6F, 1.2F, 8.8F, 0.8F, 1.0F, 0.7F, 0.6F, 1.5F, 1.5F, 0.9F, 1.5F})
                .SetTotalWidth(New Single() {2.6F, 1.2F, 8.8F, 0.8F, 1.0F, 0.7F, 0.6F, 1.8F, 1.5F, 0.9F, 1.8F})
                .WidthPercentage = 100

                For Each dRow As DataRow In vdttOrdSubServiciosGuia.Rows
                    If Convert.ToDouble(dRow("TotalProgram2").ToString) <> 0 Then
                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                        If vblnEsTransportista Then
                            .AddCell(New Paragraph(dRow("TipoOperContable2").ToString, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        Else
                            .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        End If
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("DescServicio_Cot2"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                        .AddCell(New Paragraph(If(IsDBNull(dRow("IDTipoServ2")) = True, "", dRow("IDTipoServ2")), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("IDioma2"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("NroPax2"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("nroLiberados2"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(Format(CDbl(dRow("NetoProgram2")), "###,###0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(Format(CDbl(dRow("IgvProgram2")), "###,###0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(dRow("SimboloMoneda2"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                        .AddCell(New Paragraph(Format(CDbl(dRow("TotalProgram2")), "###,###0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    End If
                Next
            End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCargarCabecerServiciosGuia(ByVal vdttOrdGuias As Data.DataRow, ByVal vblnTitulo As Boolean, Optional ByVal vstrIDPais_Prov As String = "") As PdfPTable
        Try
            Dim objTabla As New PdfPTable(11)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                '.DefaultCell.BorderWidthTop = 1
                '.SetTotalWidth(New Single() {2.6F, 1.2F, 8.8F, 0.8F, 1.0F, 0.7F, 0.6F, 1.5F, 1.5F, 0.9F, 1.5F})
                .SetTotalWidth(New Single() {2.6F, 1.2F, 8.8F, 0.8F, 1.0F, 0.7F, 0.6F, 1.8F, 1.5F, 0.9F, 1.8F})
                .WidthPercentage = 100

                Dim strIGV_IVA As String = "Igv"

                If vstrIDPais_Prov <> "000323" Then
                    strIGV_IVA = "IVA"
                End If

                If vblnTitulo Then
                    'Titulos
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph("Fecha - Hora", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph("Ubigeo", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph("Servicio", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph("Tipo", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Id.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Pax", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Lib", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Neto", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    '.AddCell(New Paragraph("Igv", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(strIGV_IVA, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Mon.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Total", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                End If
                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0.03
                .DefaultCell.BorderWidthBottom = 0.03
                .DefaultCell.PaddingBottom = 5

                Dim NewFont2 As New Font(bsDefault, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vdttOrdGuias("Dia").ToString.Substring(3, Len(vdttOrdGuias("Dia")) - 3).Trim & " " & vdttOrdGuias("Hora"), NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vdttOrdGuias("IDServicio").ToString.Substring(0, 3), NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vdttOrdGuias("DescServicio"), NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            End With
            ''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(11)
            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    '.DefaultCell.BorderWidthTop = 1
            '    .SetTotalWidth(New Single() {2.4F, 1.2F, 8.8F, 0.8F, 1.0F, 0.7F, 0.6F, 1.5F, 1.5F, 0.9F, 1.5F})
            '    .WidthPercentage = 100

            '    If vblnTitulo Then
            '        'Titulos
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph("Fecha - Hora", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '        .AddCell(New Paragraph("Ubigeo", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph("Servicio", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '        .AddCell(New Paragraph("Tipo", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Id.", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Pax", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Lib", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Neto", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Igv", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Mon.", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Total", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '    End If
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    .DefaultCell.BorderWidthTop = 1
            '    .DefaultCell.BorderWidthBottom = 1
            '    .DefaultCell.PaddingBottom = 5

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vdttOrdGuias("Dia").ToString.Substring(3, Len(vdttOrdGuias("Dia")) - 3).Trim & " " & vdttOrdGuias("Hora"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(vdttOrdGuias("IDServicio").ToString.Substring(0, 3), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vdttOrdGuias("DescServicio"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCargarCabecerServiciosGuia_Anterior(ByVal vdttOrdGuias As Data.DataRow, ByVal vblnTitulo As Boolean) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(11)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                '.DefaultCell.BorderWidthTop = 1
                .SetTotalWidth(New Single() {2.6F, 1.2F, 8.8F, 0.8F, 1.0F, 0.7F, 0.6F, 1.5F, 1.5F, 0.9F, 1.5F})
                .WidthPercentage = 100

                If vblnTitulo Then
                    'Titulos
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph("Fecha - Hora", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph("Ubigeo", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph("Servicio", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph("Tipo", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Id.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Pax", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Lib", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Neto", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Igv", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Mon.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph("Total", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                End If
                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0.03
                .DefaultCell.BorderWidthBottom = 0.03
                .DefaultCell.PaddingBottom = 5

                Dim NewFont2 As New Font(bsDefault, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vdttOrdGuias("Dia").ToString.Substring(3, Len(vdttOrdGuias("Dia")) - 3).Trim & " " & vdttOrdGuias("Hora"), NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vdttOrdGuias("IDServicio").ToString.Substring(0, 3), NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vdttOrdGuias("DescServicio"), NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            End With
            ''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(11)
            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    '.DefaultCell.BorderWidthTop = 1
            '    .SetTotalWidth(New Single() {2.4F, 1.2F, 8.8F, 0.8F, 1.0F, 0.7F, 0.6F, 1.5F, 1.5F, 0.9F, 1.5F})
            '    .WidthPercentage = 100

            '    If vblnTitulo Then
            '        'Titulos
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph("Fecha - Hora", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '        .AddCell(New Paragraph("Ubigeo", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph("Servicio", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '        .AddCell(New Paragraph("Tipo", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Id.", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Pax", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Lib", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Neto", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Igv", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Mon.", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        .AddCell(New Paragraph("Total", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '    End If
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    .DefaultCell.BorderWidthTop = 1
            '    .DefaultCell.BorderWidthBottom = 1
            '    .DefaultCell.PaddingBottom = 5

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vdttOrdGuias("Dia").ToString.Substring(3, Len(vdttOrdGuias("Dia")) - 3).Trim & " " & vdttOrdGuias("Hora"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(vdttOrdGuias("IDServicio").ToString.Substring(0, 3), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vdttOrdGuias("DescServicio"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCargarServicioTransporte(ByVal vdttOrdTransporte As Data.DataTable, ByVal vdttSubServ As Data.DataTable, _
                                                 ByVal vdttProveedoresProgramados As Data.DataTable, ByVal vBytVehiculoCab As Byte, Optional ByVal vstrIDPais_Prov As String = "") As PdfPTable
        Try
            Dim blnPintarTitulo As Boolean = True
            Dim objTabla As New PdfPTable(1)
            With objTabla
                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.BorderWidthBottom = 0
                .SetTotalWidth(New Single() {100})
                .WidthPercentage = 100

                For Each dRow As Data.DataRow In vdttOrdTransporte.Rows
                    Dim dtSubServicio As Data.DataTable = dtFiltrarDataTable(vdttSubServ, "IDOperacion_Det2='" & dRow("IDOperacion_Det").ToString & "'")
                    If dtSubServicio.Rows.Count > 0 Then
                        .AddCell(tblCargarCabecerServiciosGuia(dRow, blnPintarTitulo, vstrIDPais_Prov))
                        .AddCell(tblCargarSubServicioGuias(dtSubServicio, True))
                        Dim dtGuias As Data.DataTable = dtFiltrarDataTable(vdttProveedoresProgramados, "IDOperacion_Det='" & dRow("IDOperacion_Det").ToString & "' and IDTipoProv='" & gstrTipoProveeGuias.ToString & "'")
                        .AddCell(tblCargarGuias(dtGuias))

                        Dim dtVehiculosDif As Data.DataTable = dtFiltrarDataTable(vdttProveedoresProgramados, "IDOperacion_Det='" & dRow("IDOperacion_Det").ToString & "' and IDTipoProv='" & _
                                                                                  gstrTipoProveeTransportista.ToString & "' and IDVehiculoProgramado <> '" & vBytVehiculoCab & "'")

                        Dim dtVehiculosIgual As Data.DataTable = dtFiltrarDataTable(vdttProveedoresProgramados, "IDOperacion_Det='" & dRow("IDOperacion_Det").ToString & "' and IDTipoProv='" & _
                                                                                  gstrTipoProveeTransportista.ToString & "' and IDVehiculoProgramado = '" & vBytVehiculoCab & "'")

                        'If dtVehiculosDif.Rows.Count > 0 Then .AddCell(tblCargarTransportistas(dtVehiculosDif))
                        'jorge
                        If dtVehiculosDif.Rows.Count > 0 Then
                            .AddCell(tblCargarTransportistas(dtVehiculosDif))
                        Else
                            .AddCell(tblCargarTransportistas(dtVehiculosIgual))
                        End If
                        '.AddCell(tblCargarTransportistas(dtVehiculosDif))

                        Dim dtTrasl As Data.DataTable = dtFiltrarDataTable(vdttProveedoresProgramados, "IDOperacion_Det='" & dRow("IDOperacion_Det").ToString & "' and IDTipoProv='" & gstrTipoProveeTrasladistaLima.ToString & "'")
                        .AddCell(tblCargarTrasladista(dtTrasl))

                        .AddCell(dtAgregarFilaObservaciones(dRow("ObservBiblia"), dRow("CodigoPnr")))
                        blnPintarTitulo = False
                    End If
                Next
            End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    'Private Function blnNoExistenDatosValidos(ByVal vdtSubServicio As Data.DataTable) As Boolean
    '    Dim blnOk As Boolean = False
    '    Try
    '        For Each dRow As Data.DataRow In vdtSubServicio.Rows
    '            If dRow("TotalProgram2").ToString <> "0.00" Then
    '                blnOk = True
    '                Exit For
    '            End If
    '        Next

    '        Return blnOk
    '    Catch ex As System.Exception
    '        Throw
    '    End Try
    'End Function

    Private Function tblCargarServiciosTourConductor(ByVal vdttOrdGuias As Data.DataTable, ByVal vdttSubServ As Data.DataTable, _
                                                     Optional ByVal vstrIDPais_Prov As String = "") As PdfPTable
        Try
            Dim blnPintarTitulo As Boolean = True
            Dim objTabla As New PdfPTable(1)
            With objTabla
                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.BorderWidthBottom = 0
                .SetTotalWidth(New Single() {100})
                .WidthPercentage = 100

                For Each dRow As Data.DataRow In vdttOrdGuias.Rows
                    Dim dtSubServicio As Data.DataTable = dtFiltrarDataTable(vdttSubServ, "IDOperacion_Det2='" & dRow("IDOperacion_Det").ToString & "'")
                    If dtSubServicio.Rows.Count > 0 Then
                        .AddCell(tblCargarCabecerServiciosGuia(dRow, blnPintarTitulo, vstrIDPais_Prov))
                        .AddCell(tblCargarSubServicioGuias(dtSubServicio, False))
                        .AddCell(dtAgregarFilaObservaciones(dRow("ObservBiblia"), dRow("CodigoPnr")))
                        blnPintarTitulo = False
                    End If
                Next
            End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCargarServiciosGuia(ByVal vdttOrdGuias As Data.DataTable, ByVal vdttSubServ As Data.DataTable, _
                                            ByVal vdttOrdTransportistas As Data.DataTable, Optional ByVal vstrIDPais_Prov As String = "") As PdfPTable
        Try
            Dim blnPintarTitulo As Boolean = True
            Dim objTabla As New PdfPTable(1)
            With objTabla
                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.BorderWidthBottom = 0
                .SetTotalWidth(New Single() {100})
                .WidthPercentage = 100

                For Each dRow As Data.DataRow In vdttOrdGuias.Rows
                    Dim dtSubServicio As Data.DataTable = dtFiltrarDataTable(vdttSubServ, "IDOperacion_Det2='" & dRow("IDOperacion_Det").ToString & "'")
                    If dtSubServicio.Rows.Count > 0 Then
                        .AddCell(tblCargarCabecerServiciosGuia(dRow, blnPintarTitulo, vstrIDPais_Prov))
                        .AddCell(tblCargarSubServicioGuias(dtSubServicio, False))
                        Dim dtTransServ As Data.DataTable = dtFiltrarDataTable(vdttOrdTransportistas, "IDOperacion_Det='" & dRow("IDOperacion_Det").ToString & "' and IDTipoProv='" & gstrTipoProveeTransportista & "'")
                        .AddCell(tblCargarTransportistas(dtTransServ))
                        .AddCell(dtAgregarFilaObservaciones(dRow("ObservBiblia"), dRow("CodigoPnr")))
                        blnPintarTitulo = False
                    End If
                Next
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim blnPintarTitulo As Boolean = True
            'Dim objTabla As New PdfPTable(1)
            'With objTabla
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    .DefaultCell.BorderWidthTop = 0
            '    .DefaultCell.BorderWidthBottom = 0
            '    .SetTotalWidth(New Single() {100})
            '    .WidthPercentage = 100

            '    For Each dRow As Data.DataRow In vdttOrdGuias.Rows
            '        Dim dtSubServicio As Data.DataTable = dtFiltrarDataTable(vdttSubServ, "IDOperacion_Det2='" & dRow("IDOperacion_Det").ToString & "'")
            '        If dtSubServicio.Rows.Count > 0 Then
            '            .AddCell(tblCargarCabecerServiciosGuia(dRow, blnPintarTitulo))
            '            .AddCell(tblCargarSubServicioGuias(dtSubServicio))
            '            Dim dtTransServ As Data.DataTable = dtFiltrarDataTable(vdttOrdTransportistas, "IDOperacion_Det='" & dRow("IDOperacion_Det").ToString & "' and IDTipoProv='" & gstrTipoProveeTransportista & "'")
            '            .AddCell(tblCargarTransportistas(dtTransServ))
            '            .AddCell(dtAgregarFilaObservaciones(dRow("ObservBiblia"), dRow("CodigoPnr")))
            '            blnPintarTitulo = False
            '        End If
            '    Next
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function GenerarPdfOrdenServicioTourConductor(ByVal vdttDatosCotizacion As Data.DataTable, ByVal vdttOrdenServiciosTC As Data.DataTable, _
                                                         ByVal vdttOrdenSubServiciosTC As Data.DataTable) As String
        If vdttOrdenServiciosTC.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 120.0F, 36.0F)
        Dim parrafo As New Paragraph
        Dim strFechaHoy As String = Date.Now.ToShortDateString
        Dim strIDFile As String = vdttDatosCotizacion(0)("IDFile")
        Dim strFormatoCadenaIDOperacion_Det As String = vdttOrdenServiciosTC(0)("IDOperacion_Det")
        Dim strTourConductor As String = vdttOrdenSubServiciosTC(0)("DescProveedor_Prg2")
        Dim strIDTourConductor As String = vdttOrdenSubServiciosTC(0)("IDProveedor_Prg2")
        Dim strNombreArchivo As String = gstrRutaImagenes & "ORDEN DE SERVICIO Nro " & strIDFile & "-" & strIDTourConductor & ".pdf"
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
        Dim imgLogo As iTextSharp.text.Image
        Dim strUbigeo_Prov As String = ""
        Dim strPais_Prov As String = ""
        Dim strIDProv_Interno As String = ""
        Dim strTextoFinal As String = ""

        Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"
        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create))

            Dim strCotizacion As String = vdttDatosCotizacion(0)("Cotizacion")
            Dim strDescCliente As String = vdttDatosCotizacion(0)("DescCliente")
            Dim strTitulo As String = vdttDatosCotizacion(0)("Titulo")
            Dim strFechaIN As String = vdttDatosCotizacion(0)("FechaIN")
            Dim strFechaOUT As String = vdttDatosCotizacion(0)("FechaOUT")
            Dim strNroPax_Cot As String = vdttDatosCotizacion(0)("NroPax")
            Dim strNombrePax As String = If(IsDBNull(vdttOrdenSubServiciosTC(0)("DescProveedor_Prg2")) = True, "", vdttOrdenSubServiciosTC(0)("DescProveedor_Prg2"))
            Dim strMoneda As String = vdttOrdenSubServiciosTC(0)("IDMoneda2")
            Dim strTituloPdf As String = strIDFile & "-" & strTourConductor
            Documento.Open()

            'logo
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & strLogo) 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(38, 730) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(530) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento


            Dim dblNetoUSD As Double = 0
            Dim dblIgvUSD As Double = 0
            Dim dblTotalUSD As Double = 0
            Dim dblNetoNoUSD As Double = 0
            Dim dblIgvNoUSD As Double = 0
            Dim dblTotalNoUSD As Double = 0

            ''Obtener los Totales de los SubServicios
            ''Dim dtDatosDolares As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTC, "='US$'")
            'For Each dRow As Data.DataRow In vdttOrdenSubServiciosTC.Rows
            '    dblNeto += Convert.ToDouble(dRow("NetoProgram2"))
            '    dblIgv += Convert.ToDouble(dRow("IgvProgram2"))
            '    dblTotal += Convert.ToDouble(dRow("TotalProgram2"))
            'Next

            Dim dtDatosTotales As New DataTable()
            With dtDatosTotales
                .Columns.Add("SimbMoneda")
                .Columns.Add("Neto")
                .Columns.Add("Igv")
                .Columns.Add("Total")
            End With
            'Obtener los Totales de los SubServicios
            Dim dtDatosDolares As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTC, "SimboloMoneda2='US$'")
            For Each dRow As Data.DataRow In dtDatosDolares.Rows
                dblNetoUSD += Convert.ToDouble(dRow("NetoProgram2"))
                dblIgvUSD += Convert.ToDouble(dRow("IgvProgram2"))
                dblTotalUSD += Convert.ToDouble(dRow("TotalProgram2"))
            Next

            If dblTotalUSD > 0 Then dtDatosTotales.Rows.Add("US$", dblNetoUSD, dblIgvUSD, dblTotalUSD)
            Dim dtDatosOtrasMon As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTC, "SimboloMoneda2 <> 'US$'")
            Dim xList As List(Of String) = New List(Of String)
            For Each ItemRow As DataRow In dtDatosOtrasMon.Rows
                If Not xList.Contains(ItemRow("SimboloMoneda2")) Then
                    xList.Add(ItemRow("SimboloMoneda2"))
                End If
            Next

            For Each strItem As String In xList
                dblNetoNoUSD = 0 : dblIgvNoUSD = 0 : dblTotalNoUSD = 0
                Dim dtFiltroServ As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTC, "SimboloMoneda2='" & strItem & "'")
                For Each dRow As Data.DataRow In dtFiltroServ.Rows
                    dblNetoNoUSD += Convert.ToDouble(dRow("NetoProgram2"))
                    dblIgvNoUSD += Convert.ToDouble(dRow("IgvProgram2"))
                    dblTotalNoUSD += Convert.ToDouble(dRow("TotalProgram2"))
                Next

                If dblTotalNoUSD > 0 Then dtDatosTotales.Rows.Add(strItem, dblNetoNoUSD, dblIgvNoUSD, dblTotalNoUSD)
            Next

            Documento.Add(dtCabeceraGuiaOrdenServicio(strTituloPdf, True))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(dtTablaDatosCotizacionOrdenServicios(strIDFile, strCotizacion, strDescCliente, strTitulo, _
                                                               strFechaIN, strFechaOUT, strNroPax_Cot, gstrUsuario, strNombrePax, True))

            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 5, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            Documento.Add(tblCargarServiciosTourConductor(vdttOrdenServiciosTC, vdttOrdenSubServiciosTC, strPais_Prov))

            Documento.Add(New Paragraph(" ", NewFont))

            'Documento.Add(dtCargarTotalesOrdenServicios(dblNeto, dblIgv, strMoneda, dblTotal))
            'Documento.Add(New Paragraph(" ", NewFont))
            For Each Item As DataRow In dtDatosTotales.Rows
                Documento.Add(dtCargarTotalesOrdenServicios(Item("Neto"), Item("Igv"), Item("SimbMoneda"), Item("Total")))
                Documento.Add(New Paragraph(" ", NewFont))
            Next

            'Dim NewFont2 As New Font(bsDefault, 12, Font.BOLD, New iTextSharp.text.Color(145, 145, 145))
            'Documento.Add(New Paragraph("Lista de Pax", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'Documento.Add(New Paragraph(" ", NewFont))
            'Documento.Add(tblCargarListadoPaxOrdenServicio(vdttPax))

            'TEXTO FINAL
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(New Paragraph(strTextoFinal, NewFont))

            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo
        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Public Function GenerarPdfOrdenServiciosGuias(ByVal vdttDatosCotizacion As Data.DataTable, ByVal vdttOrdenServiciosGuias As Data.DataTable, ByVal vdttOrdenSubServiciosGuias As Data.DataTable, _
                                                   ByVal vdttOrdenProveedoresProgramados As Data.DataTable, ByVal vdttPax As Data.DataTable, ByVal vblnTrasladista As Boolean, _
                                                   ByVal vstrDescOficina As String) As String
        If vdttOrdenServiciosGuias.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 120.0F, 36.0F)
        'Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 130.0F, 36.0F)

        Dim parrafo As New Paragraph
        Dim strFechaHoy As String = Date.Now.ToShortDateString
        Dim strIDFile As String = vdttDatosCotizacion(0)("IDFile")
        Dim strFormatoCadenaIDOperacion_Det As String = vdttOrdenServiciosGuias(0)("IDOperacion_Det")
        Dim strProveedorProgramado As String = vdttOrdenServiciosGuias(0)("IDGuiaProgramado")
        If vblnTrasladista Then
            strProveedorProgramado = vdttOrdenServiciosGuias(0)("IDTrasladistaProgramado")
        End If
        Dim strOficina As String = If(vstrDescOficina <> "", " (" & vstrDescOficina & ") -", "")
        Dim strNombreArchivo As String = gstrRutaImagenes & "ORDEN DE SERVICIO" & strOficina & " Nro " & strIDFile & "-" & strProveedorProgramado & ".pdf"
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
        Dim imgLogo As iTextSharp.text.Image

        ''devolver oficina para establecer logo

        Dim strUbigeo_Prov As String = ""
        Dim strPais_Prov As String = ""

        Dim strIDProv_Interno As String = ""

        Dim strTextoFinal As String = ""

        Using objProvBN As New clsProveedorBN
            Using dr As SqlClient.SqlDataReader = objProvBN.ConsultarPk(strProveedorProgramado)
                dr.Read()
                If dr.HasRows Then
                    strUbigeo_Prov = If(IsDBNull(dr("CoUbigeo_Oficina")), "", dr("CoUbigeo_Oficina"))
                    strPais_Prov = dr("IDPais")
                End If
            End Using
        End Using

        Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"
        If strPais_Prov = gstrPeru Then
            strIDProv_Interno = gstrIDProvSetoursLima
        ElseIf strPais_Prov = gstrChile Then
            strIDProv_Interno = gstrIDProvSetoursChile
        ElseIf strPais_Prov = gstrArgentina Then
            strIDProv_Interno = gstrIDProvSetoursArgentina
        End If

        'devolver datos proveedor interno para obtener texto final:
        If strIDProv_Interno.Trim.Length > 0 Then
            Using objProvBN As New clsProveedorBN
                Using dr As SqlClient.SqlDataReader = objProvBN.ConsultarPk(strIDProv_Interno)
                    dr.Read()
                    If dr.HasRows Then
                        strTextoFinal = If(IsDBNull(dr("TxTexto_OS")), "", dr("TxTexto_OS"))
                    End If
                End Using
            End Using
        End If


        Dim strubigeo As String = strUbigeo_Prov
        Select Case strubigeo
            Case "000110"
                strLogo = "SETOURS LOGO-FINAL-CHILE.jpg"
            Case "000113"
                strLogo = "SETOURS LOGO-FINAL-ARGENTINA.jpg"
            Case Else
                strLogo = "SETOURS LOGO-FINAL.jpg"
        End Select

        '        Dim strTextoFinal As String = _
        '"Se procederá con el pago del comprobante de pago al término del servicio para lo cual se necesita el envío del comprobante físico o electrónico (según Resolución " + vbCrLf + _
        '"de Superintendencia N°287-2014-SUNAT) en el mes que culmina el servicio (según numeral 5 artículo 5° del Reglamento de Comprobantes de Pago)." + vbCrLf + _
        '"Si los proveedores sujetos a Renta de 4ta categoría están dentro de los siguientes supuestos:" + vbCrLf + _
        '"   1.       Proyección Anual de Renta no supera los S/. 33,688" + vbCrLf + _
        '"   2.       Total de Rentas en el mes no superen los S/.  2,807" + vbCrLf + _
        '"   3.       Emisión de un Recibo de Honorario por más de S/. 1,500" + vbCrLf + _
        '"Deben entregar obligatoriamente la respectiva ""Constancia de Suspensión de 4ta Categoría – Formulario 1609""" + _
        '""
        '        If strPais_Prov <> "000323" Then
        '            strTextoFinal = ""
        '        End If

        'Dim objProvBN As New clsProveedorBN
        'Dim dtoficina As DataTable = objProvBN.ConsultarPk(strProveedorProgramado)

        'Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"

        'If dtoficina.Rows.Count > 0 Then
        '    Dim strubigeo As String = dtoficina.Rows(0).Item(0).ToString
        '    Select Case strubigeo
        '        Case "000110"
        '            strLogo = "SETOURS LOGO-FINAL-CHILE.jpg"
        '        Case "000113"
        '            strLogo = "SETOURS LOGO-FINAL-ARGENTINA.jpg"
        '        Case Else
        '            strLogo = "SETOURS LOGO-FINAL.jpg"
        '    End Select

        'End If

        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create))
            Dim strCotizacion As String = vdttDatosCotizacion(0)("Cotizacion")
            Dim strDescCliente As String = vdttDatosCotizacion(0)("DescCliente")
            Dim strTitulo As String = vdttDatosCotizacion(0)("Titulo")
            Dim strFechaIN As String = vdttDatosCotizacion(0)("FechaIN")
            Dim strFechaOUT As String = vdttDatosCotizacion(0)("FechaOUT")
            Dim strNroPax_Cot As String = vdttDatosCotizacion(0)("NroPax")
            Dim strNombrePax As String = vdttOrdenSubServiciosGuias(0)("DescProveedor_Prg2")
            Dim strMoneda As String = vdttOrdenSubServiciosGuias(0)("IDMoneda2")
            Dim strTituloPdf As String = strIDFile & "-" & strProveedorProgramado
            'Dim ObjEvent As New PDFFooter()
            'ObjEvent.ActionFooterSelect = "DefaultImageHeader"
            'pdfw.PageEvent = ObjEvent
            Documento.Open()

            'logo
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & strLogo) 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(38, 730) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(530) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento


            ' ''Dim dblNeto As Double = 0
            ' ''Dim dblIgv As Double = 0
            ' ''Dim dblTotal As Double = 0

            '' ''Obtener los Totales de los SubServicios
            ' ''For Each dRow As Data.DataRow In vdttOrdenSubServiciosGuias.Rows
            ' ''    dblNeto += Convert.ToDouble(dRow("NetoProgram2"))
            ' ''    dblIgv += Convert.ToDouble(dRow("IgvProgram2"))
            ' ''    dblTotal += Convert.ToDouble(dRow("TotalProgram2"))
            ' ''Next

            Dim dblNetoUSD As Double = 0
            Dim dblIgvUSD As Double = 0
            Dim dblTotalUSD As Double = 0
            Dim dblNetoNoUSD As Double = 0
            Dim dblIgvNoUSD As Double = 0
            Dim dblTotalNoUSD As Double = 0

            ''Obtener los Totales de los SubServicios
            ''Dim dtDatosDolares As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTC, "='US$'")
            'For Each dRow As Data.DataRow In vdttOrdenSubServiciosTC.Rows
            '    dblNeto += Convert.ToDouble(dRow("NetoProgram2"))
            '    dblIgv += Convert.ToDouble(dRow("IgvProgram2"))
            '    dblTotal += Convert.ToDouble(dRow("TotalProgram2"))
            'Next

            Dim dtDatosTotales As New DataTable()
            With dtDatosTotales
                .Columns.Add("SimbMoneda")
                .Columns.Add("Neto")
                .Columns.Add("Igv")
                .Columns.Add("Total")
            End With
            'Obtener los Totales de los SubServicios
            Dim dtDatosDolares As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosGuias, "SimboloMoneda2='US$'")
            For Each dRow As Data.DataRow In dtDatosDolares.Rows
                dblNetoUSD += Convert.ToDouble(dRow("NetoProgram2"))
                dblIgvUSD += Convert.ToDouble(dRow("IgvProgram2"))
                dblTotalUSD += Convert.ToDouble(dRow("TotalProgram2"))
            Next

            If dblTotalUSD > 0 Then dtDatosTotales.Rows.Add("US$", dblNetoUSD, dblIgvUSD, dblTotalUSD)
            Dim dtDatosOtrasMon As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosGuias, "SimboloMoneda2 <> 'US$'")
            Dim xList As List(Of String) = New List(Of String)
            For Each ItemRow As DataRow In dtDatosOtrasMon.Rows
                If Not xList.Contains(ItemRow("SimboloMoneda2")) Then
                    xList.Add(ItemRow("SimboloMoneda2"))
                End If
            Next

            For Each strItem As String In xList
                dblNetoNoUSD = 0 : dblIgvNoUSD = 0 : dblTotalNoUSD = 0
                Dim dtFiltroServ As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosGuias, "SimboloMoneda2='" & strItem & "'")
                For Each dRow As Data.DataRow In dtFiltroServ.Rows
                    dblNetoNoUSD += Convert.ToDouble(dRow("NetoProgram2"))
                    dblIgvNoUSD += Convert.ToDouble(dRow("IgvProgram2"))
                    dblTotalNoUSD += Convert.ToDouble(dRow("TotalProgram2"))
                Next

                If dblTotalNoUSD > 0 Then dtDatosTotales.Rows.Add(strItem, dblNetoNoUSD, dblIgvNoUSD, dblTotalNoUSD)
            Next

            Documento.Add(dtCabeceraGuiaOrdenServicio(strTituloPdf))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(dtTablaDatosCotizacionOrdenServicios(strIDFile, strCotizacion, strDescCliente, strTitulo, _
                                                               strFechaIN, strFechaOUT, strNroPax_Cot, gstrUsuario, strNombrePax, True))

            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 5, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            Documento.Add(tblCargarServiciosGuia(vdttOrdenServiciosGuias, vdttOrdenSubServiciosGuias, vdttOrdenProveedoresProgramados, strPais_Prov))


            'Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 5, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            'Documento.Add(tblCargarTrasladistaOrden(vdttOrdenServiciosGuias))


            Documento.Add(New Paragraph(" ", NewFont))
            'Documento.Add(dtCargarTotalesOrdenServicios(dblNeto, dblIgv, strMoneda, dblTotal))
            'Documento.Add(New Paragraph(" ", NewFont))
            For Each Item As DataRow In dtDatosTotales.Rows
                Documento.Add(dtCargarTotalesOrdenServicios(Item("Neto"), Item("Igv"), Item("SimbMoneda"), Item("Total")))
                Documento.Add(New Paragraph(" ", NewFont))
            Next

            Dim NewFont2 As New Font(bsDefault, 12, Font.BOLD, New iTextSharp.text.Color(145, 145, 145))
            Documento.Add(New Paragraph("Lista de Pax", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblCargarListadoPaxOrdenServicio(vdttPax))

            'TEXTO FINAL
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(New Paragraph(strTextoFinal, NewFont))

            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo
        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create))
        'Documento.Open()
        'Try
        '    Dim strCotizacion As String = vdttDatosCotizacion(0)("Cotizacion")
        '    Dim strDescCliente As String = vdttDatosCotizacion(0)("DescCliente")
        '    Dim strTitulo As String = vdttDatosCotizacion(0)("Titulo")
        '    Dim strFechaIN As String = vdttDatosCotizacion(0)("FechaIN")
        '    Dim strFechaOUT As String = vdttDatosCotizacion(0)("FechaOUT")
        '    Dim strNroPax_Cot As String = vdttDatosCotizacion(0)("NroPax")
        '    Dim strNombrePax As String = vdttOrdenSubServiciosGuias(0)("DescProveedor_Prg2")
        '    Dim strMoneda As String = vdttOrdenSubServiciosGuias(0)("IDMoneda2")

        '    Dim strTituloPdf As String = strIDFile & "-" & strProveedorProgramado

        '    'Codigo x Rectangulo
        '    Dim Rect As PdfContentByte = pdfw.DirectContent
        '    Rect.RoundRectangle(55.0F, 675.0F, 490.0F, 55.0F, 5.0F) '(Pos X, Pos Y, Tam width, Tam Height, Redon )
        '    Rect.Stroke()

        '    Dim dblNeto As Double = 0
        '    Dim dblIgv As Double = 0
        '    Dim dblTotal As Double = 0

        '    'Obtener los Totales de los SubServicios
        '    For Each dRow As Data.DataRow In vdttOrdenSubServiciosGuias.Rows
        '        dblNeto += Convert.ToDouble(dRow("NetoProgram2"))
        '        dblIgv += Convert.ToDouble(dRow("IgvProgram2"))
        '        dblTotal += Convert.ToDouble(dRow("TotalProgram2"))
        '    Next

        '    Documento.Add(dtCabeceraGuiaOrdenServicio(strTituloPdf))
        '    Documento.Add(New Paragraph(" "))
        '    Documento.Add(dtTablaDatosCotizacionOrdenServicios(strIDFile, strCotizacion, strDescCliente, strTitulo, _
        '                                                       strFechaIN, strFechaOUT, strNroPax_Cot, gstrUsuario, strNombrePax, True))

        '    Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 5, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '    Documento.Add(tblCargarServiciosGuia(vdttOrdenServiciosGuias, vdttOrdenSubServiciosGuias, vdttOrdenProveedoresProgramados))

        '    Documento.Add(New Paragraph(" "))
        '    Documento.Add(dtCargarTotalesOrdenServicios(dblNeto, dblIgv, strMoneda, dblTotal))
        '    Documento.Add(New Paragraph(" "))
        '    Documento.Add(New Paragraph("Lista de Pax", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
        '    Documento.Add(New Paragraph(" "))
        '    Documento.Add(tblCargarListadoPaxOrdenServicio(vdttPax))

        '    pdfw.Flush()
        '    Documento.Close()

        '    Return strNombreArchivo
        'Catch ex As System.Exception
        '    pdfw.Flush()
        '    Documento.Close()
        '    Throw
        'End Try
    End Function

    Public Function GenerarPdfOrdenServiciosTransportistas(ByVal vdttDatosCotizacion As Data.DataTable, ByVal vdttOrdenServicios As Data.DataTable, _
                                                           ByVal vdttOrdenSubServiciosTransportes As Data.DataTable, _
                                                           ByVal vdttProveedoresProgramados As Data.DataTable, _
                                                           ByVal vdttPax As Data.DataTable, _
                                                           ByVal vstrOficina As String) As String
        If vdttOrdenServicios.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 130.0F, 16.0F)
        'Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 36.0F, 36.0F)
        Dim parrafo As New Paragraph
        Dim strFechaHoy As String = Date.Now.ToShortDateString
        Dim strIDFile As String = vdttDatosCotizacion(0)("IDFile").ToString
        Dim strFormatoCadenaIDOperacion_Det As String = vdttOrdenServicios(0)("IDOperacion_Det").ToString
        Dim strIDProveedorPrg As String = vdttOrdenServicios(0)("IDTransportistaProgramado").ToString
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
        Dim strOficina As String = If(vstrOficina <> "", " (" & vstrOficina & ") -", "")
        Dim strNombreArchivo As String = gstrRutaImagenes & "ORDEN DE SERVICIO" & strOficina & " Nro " & strIDFile & "-" & strIDProveedorPrg & ".pdf"
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim imgLogo As iTextSharp.text.Image

        ''devolver oficina para establecer logo

        Dim strUbigeo_Prov As String = ""
        Dim strPais_Prov As String = ""
        Dim strTextoFinal As String = ""
        Dim strIDProv_Interno As String = ""

        Using objProvBN As New clsProveedorBN
            Using dr As SqlClient.SqlDataReader = objProvBN.ConsultarPk(strIDProveedorPrg)
                dr.Read()
                If dr.HasRows Then
                    strUbigeo_Prov = If(IsDBNull(dr("CoUbigeo_Oficina")), "", dr("CoUbigeo_Oficina"))
                    strPais_Prov = dr("IDPais")
                End If
            End Using
        End Using

        Dim strLogo As String = "SETOURS LOGO-FINAL.jpg"


        If strPais_Prov = gstrPeru Then
            strIDProv_Interno = gstrIDProvSetoursLima
        ElseIf strPais_Prov = gstrChile Then
            strIDProv_Interno = gstrIDProvSetoursChile
        ElseIf strPais_Prov = gstrArgentina Then
            strIDProv_Interno = gstrIDProvSetoursArgentina
        End If

        Dim strubigeo As String = strUbigeo_Prov
        Select Case strubigeo
            Case gstrSantiago
                strLogo = "SETOURS LOGO-FINAL-CHILE.jpg"
            Case gstrBuenosAires
                strLogo = "SETOURS LOGO-FINAL-ARGENTINA.jpg"
            Case Else
                strLogo = "SETOURS LOGO-FINAL.jpg"
        End Select

        'devolver datos proveedor interno para obtener texto final:
        If strIDProv_Interno.Trim.Length > 0 Then
            Using objProvBN As New clsProveedorBN
                Using dr As SqlClient.SqlDataReader = objProvBN.ConsultarPk(strIDProv_Interno)
                    dr.Read()
                    If dr.HasRows Then
                        strTextoFinal = If(IsDBNull(dr("TxTexto_OS")), "", dr("TxTexto_OS"))
                    End If
                End Using
            End Using
        End If

        'devolver distintos vehiculos

        Dim dttVehiculos_Dist As DataTable = vdttOrdenSubServiciosTransportes.DefaultView.ToTable(True, "VehiculoProgramado2")

        'texto al final del pdf

        '        Dim strTextoFinal As String = _
        '"Se procederá con el pago del comprobante de pago al término del servicio para lo cual se necesita el envío del comprobante físico o electrónico (según Resolución " + vbCrLf + _
        '"de Superintendencia N°287-2014-SUNAT) en el mes que culmina el servicio (según numeral 5 artículo 5° del Reglamento de Comprobantes de Pago)." + vbCrLf + _
        '"Si los proveedores sujetos a Renta de 4ta categoría están dentro de los siguientes supuestos:" + vbCrLf + _
        '"   1.       Proyección Anual de Renta no supera los S/. 33,688" + vbCrLf + _
        '"   2.       Total de Rentas en el mes no superen los S/.  2,807" + vbCrLf + _
        '"   3.       Emisión de un Recibo de Honorario por más de S/. 1,500" + vbCrLf + _
        '"Deben entregar obligatoriamente la respectiva ""Constancia de Suspensión de 4ta Categoría – Formulario 1609""" + _
        '""
        '        If strPais_Prov <> "000323" Then
        '            strTextoFinal = ""
        '        End If

        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            'Dim ObjEvent As New PDFFooter()
            'ObjEvent.ActionFooterSelect = "DefaultImageHeader"
            'pdfw.PageEvent = ObjEvent
            Documento.Open()

            'logo
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & strLogo) 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(38, 730) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(530) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            Dim strCotizacion As String = vdttDatosCotizacion(0)("Cotizacion")
            Dim strDescCliente As String = vdttDatosCotizacion(0)("DescCliente")
            Dim strTitulo As String = vdttDatosCotizacion(0)("Titulo")
            Dim strFechaIN As String = vdttDatosCotizacion(0)("FechaIN")
            Dim strFechaOUT As String = vdttDatosCotizacion(0)("FechaOUT")
            Dim strNroPax_Cot As String = vdttDatosCotizacion(0)("NroPax")
            Dim bytIDVehiculoPrg As Byte = CByte(vdttOrdenSubServiciosTransportes(0)("IDVehiculoProgramado2").ToString.Trim)
            Dim strNombrePax As String = ""


            'If vdttOrdenSubServiciosTransportes(0)("VehiculoProgramado2").ToString.Trim <> "" Then
            'validar si tiene solo un vehiculo en toda la orden, que salga en la cabecera
            If vdttOrdenSubServiciosTransportes(0)("VehiculoProgramado2").ToString.Trim <> "" And dttVehiculos_Dist.Rows.Count = 1 Then
                strNombrePax = vdttOrdenSubServiciosTransportes(0)("DescProveedor_Prg2") & " (" & vdttOrdenSubServiciosTransportes(0)("VehiculoProgramado2") & ")"
            Else
                strNombrePax = vdttOrdenSubServiciosTransportes(0)("DescProveedor_Prg2")
            End If

            Dim strMoneda As String = vdttOrdenSubServiciosTransportes(0)("IDMoneda2")
            Dim strTituloPdf As String = strIDFile & "-" & strIDProveedorPrg


            Dim dblNetoUSD As Double = 0
            Dim dblIgvUSD As Double = 0
            Dim dblTotalUSD As Double = 0
            Dim dblNetoNoUSD As Double = 0
            Dim dblIgvNoUSD As Double = 0
            Dim dblTotalNoUSD As Double = 0

            Dim dtDatosTotales As New DataTable()
            With dtDatosTotales
                .Columns.Add("SimbMoneda")
                .Columns.Add("Neto")
                .Columns.Add("Igv")
                .Columns.Add("Total")
            End With
            'Obtener los Totales de los SubServicios
            Dim dtDatosDolares As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTransportes, "SimboloMoneda2='US$'")
            For Each dRow As Data.DataRow In dtDatosDolares.Rows
                dblNetoUSD += Convert.ToDouble(dRow("NetoProgram2"))
                dblIgvUSD += Convert.ToDouble(dRow("IgvProgram2"))
                dblTotalUSD += Convert.ToDouble(dRow("TotalProgram2"))
            Next

            If dblTotalUSD > 0 Then dtDatosTotales.Rows.Add("US$", dblNetoUSD, dblIgvUSD, dblTotalUSD)
            Dim dtDatosOtrasMon As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTransportes, "SimboloMoneda2 <> 'US$'")
            Dim xList As List(Of String) = New List(Of String)
            For Each ItemRow As DataRow In dtDatosOtrasMon.Rows
                If Not xList.Contains(ItemRow("SimboloMoneda2")) Then
                    xList.Add(ItemRow("SimboloMoneda2"))
                End If
            Next

            For Each strItem As String In xList
                dblNetoNoUSD = 0 : dblIgvNoUSD = 0 : dblTotalNoUSD = 0
                Dim dtFiltroServ As DataTable = dtFiltrarDataTable(vdttOrdenSubServiciosTransportes, "SimboloMoneda2='" & strItem & "'")
                For Each dRow As Data.DataRow In dtFiltroServ.Rows
                    dblNetoNoUSD += Convert.ToDouble(dRow("NetoProgram2"))
                    dblIgvNoUSD += Convert.ToDouble(dRow("IgvProgram2"))
                    dblTotalNoUSD += Convert.ToDouble(dRow("TotalProgram2"))
                Next

                If dblTotalNoUSD > 0 Then dtDatosTotales.Rows.Add(strItem, dblNetoNoUSD, dblIgvNoUSD, dblTotalNoUSD)
            Next


            'Dim Lists

            ''Codigo x Rectangulo
            'Dim Rect As PdfContentByte = pdfw.DirectContent
            'Rect.RoundRectangle(55.0F, 675.0F, 490.0F, 55.0F, 5.0F) '(Pos X, Pos Y, Tam width, Tam Height, Redon )
            'Rect.Stroke()

            Documento.Add(dtCabeceraTransportistasOrdenServicio(strTituloPdf))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(dtTablaDatosCotizacionOrdenServicios(strIDFile, strCotizacion, strDescCliente, strTitulo, _
                                                               strFechaIN, strFechaOUT, strNroPax_Cot, gstrUsuario, strNombrePax, False))

            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 5, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            Documento.Add(tblCargarServicioTransporte(vdttOrdenServicios, vdttOrdenSubServiciosTransportes, vdttProveedoresProgramados, bytIDVehiculoPrg, strPais_Prov))
            Documento.Add(New Paragraph(" ", NewFont))
            For Each Item As DataRow In dtDatosTotales.Rows
                Documento.Add(dtCargarTotalesOrdenServicios(Item("Neto"), Item("Igv"), Item("SimbMoneda"), Item("Total")))
                Documento.Add(New Paragraph(" ", NewFont))
            Next
            'Documento.Add(dtCargarTotalesOrdenServicios(dblNeto, dblIgv, strMoneda, dblTotal))
            'Documento.Add(New Paragraph(" ", NewFont))
            Dim NewFont2 As New Font(bsDefault, 12, Font.BOLD, New iTextSharp.text.Color(145, 145, 145))
            Documento.Add(New Paragraph("Lista de Pax", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblCargarListadoPaxOrdenServicio(vdttPax))
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

            'texto final
            ' If strPais_Prov = "000323" Then
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(New Paragraph(strTextoFinal, NewFont))
            'End If

            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo
        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Public Function dtCargarTotalesOrdenServicios(ByVal vdblTotNeto As Double, ByVal vdblIgv As Double, ByVal vstrMoneda As String, ByVal vdblTotalTot As Double) As PdfPTable
        Dim objTable As New PdfPTable(5)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 9, Font.BOLD)
        Try
            With objTable
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {14.3F, 1.5F, 1.5F, 0.9F, 1.5F})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER


                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Neto", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Igv", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Mon", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Total", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

                .DefaultCell.PaddingBottom = 6
                .DefaultCell.PaddingTop = 6

                .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                .AddCell(New Paragraph("TOTAL", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.BorderWidthLeft = 1
                .DefaultCell.BorderWidthTop = 1
                .DefaultCell.BorderWidthBottom = 1
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(Format(vdblTotNeto, "######0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.BorderWidthLeft = 0
                .AddCell(New Paragraph(Format(vdblIgv, "######0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrMoneda, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                .DefaultCell.BorderWidthRight = 1
                .AddCell(New Paragraph(Format(vdblTotalTot, "######0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            End With
            Return objTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCargarListadoPaxOrdenServicio(ByVal vdtt As Data.DataTable) As PdfPTable
        Dim objTable As New PdfPTable(6)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
        Try
            With objTable
                '.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {0.5F, 2.0F, 1.2F, 1.2F, 1.2F, 1.2F})
                .WidthPercentage = 100
                .DefaultCell.PaddingBottom = 5.0F

                Dim NewFont2 As New Font(bsDefault, 9, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))

                DefinirBordesTablas(objTable, 0.03, 0.03, 0.03, 0.03)
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph("Ord.", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                DefinirBordesTablas(objTable, 0, 0.03, 0.03, 0.03)
                .AddCell(New Paragraph("Nombre", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Tipo Doc. Identidad", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Nro. Doc. Identidad", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Nacionalidad", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Fec. Nacimiento", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(objTable, 0, 0, 0, 0)

                'NroOrd
                For Each dRow As Data.DataRow In vdtt.Rows
                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph(dRow("NroOrd").ToString, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(dRow("Titulo").ToString & " " & dRow("Apellidos") & " " & dRow("Nombres"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(dRow("DescIdentidad"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(Convert.ToString(dRow("NumIdentidad")), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(dRow("DescNacionalidad"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                    .AddCell(New Paragraph(If(IsDBNull(dRow("FecNacimiento")), "--------", dRow("FecNacimiento")), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                Next
            End With
            Return objTable
        Catch ex As System.Exception
            Throw
        End Try
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim objTable As New PdfPTable(6)
        'Try
        '    With objTable
        '        '.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
        '        .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
        '        .DefaultCell.BorderWidth = 1
        '        .SetTotalWidth(New Single() {0.5F, 2.0F, 1.2F, 1.2F, 1.2F, 1.2F})
        '        .WidthPercentage = 100
        '        .DefaultCell.PaddingBottom = 5.0F

        '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
        '        .AddCell(New Paragraph("Ord.", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
        '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '        .AddCell(New Paragraph("Nombre", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
        '        .AddCell(New Paragraph("Tipo Doc. Identidad", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
        '        .AddCell(New Paragraph("Nro. Doc. Identidad", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
        '        .AddCell(New Paragraph("Nacionalidad", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
        '        .AddCell(New Paragraph("Fec. Nacimiento", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

        '        'NroOrd
        '        For Each dRow As Data.DataRow In vdtt.Rows
        '            .DefaultCell.HorizontalAlignment = ALIGN_CENTER
        '            .AddCell(New Paragraph(dRow("NroOrd").ToString, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '            .AddCell(New Paragraph(dRow("Titulo").ToString & " " & dRow("Apellidos") & " " & dRow("Nombres"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '            .AddCell(New Paragraph(dRow("DescIdentidad"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '            .AddCell(New Paragraph(Convert.ToString(dRow("NumIdentidad")), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '            .AddCell(New Paragraph(dRow("DescNacionalidad"), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '            .AddCell(New Paragraph(If(IsDBNull(dRow("FecNacimiento")), "--------", dRow("FecNacimiento")), FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '        Next
        '    End With
        '    Return objTable
        'Catch ex As System.Exception
        '    Throw
        'End Try
    End Function

    Public Function dtCargarDetalleOrdenServicio(ByVal vdtt As Data.DataTable) As PdfPTable
        Dim objTable As New PdfPTable(11)
        Try
            Dim strTipoProveedor As String = vdtt(0)("DescripTipoProv")
            With objTable
                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.BorderWidthBottom = 1
                .SetTotalWidth(New Single() {2.8F, 1.2F, 8, 0.8F, 1.0F, 0.7F, 0.6F, 1.5F, 1.5F, 0.9F, 1.5F})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER

                'Titulos
                .AddCell(New Paragraph("Fecha - Hora", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Ubigeo", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Servicio", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph("Tipo", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Id.", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Pax", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Lib", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Neto", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Igv", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Mon.", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Total", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                'Union de Celdas
                .DefaultCell.Colspan = 11

                'Insertar Fila A Fila
                For Each DRow As DataRow In vdtt.Rows
                    .AddCell(dtAgregarFilaDetalle1OrdenServicio(DRow("FechaDetalle"), DRow("HoraDetalle"), DRow("Ciudad"), DRow("Servicio"), DRow("IDTipoServ"), _
                                                               DRow("Siglas"), DRow("NroPax"), DRow("Liberados"), DRow("NetoProgram"), DRow("IgvProgram"), _
                                                               DRow("SimboloMoneda"), DRow("TotalProgram")))
                    '.AddCell(dtAgregarFilaDetalle2OrdenServicio(DRow("DescServicio_Cot"), DRow("DescProveedor_Prg"), "", "", DRow("ObservacionBiblia"), DRow("ObservacionOpe"), DRow("PNR"), _
                    '                                            strTipoProveedor, DRow("GuiaProg"), DRow("TrasProg")))
                Next
            End With
            Return objTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function dtAgregarFilaDetalle1OrdenServicio(ByVal vstrFechaDetalle As String, ByVal vstrHoraDetalle As String, ByVal vstrCiudad As String, _
                                                      ByVal vstrServicio As String, ByVal vstrIDTipoServ As String, ByVal vstrSiglas As String, ByVal vstrNropax As String, ByVal vstrNroLiberados As String, _
                                                      ByVal vstrNetoProgram As String, ByVal vstrIgvProgram As String, ByVal vstrSimboloMoneda As String, _
                                                      ByVal vstrTotalProgram As String) As PdfPTable
        Dim objTable As New PdfPTable(11)
        Try
            With objTable
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {2.8F, 1.2F, 8, 0.8F, 1.0F, 0.7F, 0.6F, 1.5F, 1.5F, 0.9F, 1.5F})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER

                'Fila Cabecera
                .AddCell(New Paragraph(vstrFechaDetalle & " " & vstrHoraDetalle, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrCiudad, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrServicio, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrIDTipoServ, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrSiglas, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrNropax, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrNroLiberados, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrNetoProgram, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrIgvProgram, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrSimboloMoneda, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrTotalProgram, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            End With
            Return objTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function dtAgregarFilaObservaciones(ByVal vstrObservacionBiblia As String, ByVal vstrPNR As String) As PdfPTable

        Dim ObjTable As New PdfPTable(2)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
        Try
            With ObjTable
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {2.5F, 11.5F})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.VerticalAlignment = ALIGN_TOP

                Dim NewFont2 As New Font(bsDefault, 9, Font.BOLD)

                .AddCell(New Paragraph("Obs. Biblia", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrObservacionBiblia, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                Dim tmpPNR As String = vstrPNR
                If InStr(tmpPNR, "PNR: ") > 0 Then
                    tmpPNR = Replace(vstrPNR, "PNR: ", "")
                End If
                .AddCell(New Paragraph("PNR (Cód. Reserva)", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(tmpPNR, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'With ObjTable
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {2.4F, 11.5F})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.VerticalAlignment = ALIGN_TOP

            '    .AddCell(New Paragraph("Obs. Biblia", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrObservacionBiblia, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            '    Dim tmpPNR As String = vstrPNR
            '    If InStr(tmpPNR, "PNR: ") > 0 Then
            '        tmpPNR = Replace(vstrPNR, "PNR: ", "")
            '    End If
            '    .AddCell(New Paragraph("PNR (Cód. Reserva)", FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(tmpPNR, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            'End With
            Return ObjTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function dtCabeceraGuiaOrdenServicio(ByVal vstrIDFile As String, _
                                                Optional ByVal vblnTourConductor As Boolean = False) As PdfPTable
        Dim ObjTable As New PdfPTable(2)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 10, Font.BOLD)
        Try
            With ObjTable
                .DefaultCell.Border = 0
                .SetTotalWidth(New Single() {5, 5})
                .WidthPercentage = 100
                .DefaultCell.Colspan = 2
                .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                .AddCell(New Paragraph("Fecha de Impresión " & Date.Now.ToShortDateString, _
                         NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                Dim NewFont2 As New Font(bsDefault, 14, Font.BOLD)
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                If Not vblnTourConductor Then
                    .AddCell(New Paragraph(vbCrLf & "ORDEN DE SERVICIOS POR GUÍA Nº" & vstrIDFile, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                Else
                    .AddCell(New Paragraph(vbCrLf & "ORDEN DE SERVICIOS PARA TC Nº" & vstrIDFile, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                End If
            End With
            ''''''''''''''''''''''''''''''''''''''''''''
            'With ObjTable
            '    .DefaultCell.Border = 0
            '    .SetTotalWidth(New Single() {5, 5})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(gstrRazonSocial & vbCrLf & gstrDireccion & vbCrLf & gstrRuc, _
            '             FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '    .AddCell(New Paragraph("Fecha de Impresión " & Date.Now.ToShortDateString, _
            '             FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            '    .DefaultCell.Colspan = 2
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(vbCrLf & "ORDEN DE SERVICIOS POR GUÍA Nº" & vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'End With
            Return ObjTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function dtCabeceraTransportistasOrdenServicio(ByVal vstrIDFile As String) As PdfPTable
        Dim ObjTable As New PdfPTable(2)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 10, Font.BOLD)
        Try
            With ObjTable
                .DefaultCell.Border = 0
                .SetTotalWidth(New Single() {5, 5})
                .WidthPercentage = 100
                .DefaultCell.Colspan = 2

                .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                .AddCell(New Paragraph("Fecha de Impresión " & Date.Now.ToShortDateString, _
                         NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                Dim NewFont2 As New Font(bsDefault, 14, Font.BOLD)
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vbCrLf & "ORDEN DE SERVICIOS POR TRANSPORTISTA Nº" & vstrIDFile, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            End With
            Return ObjTable
        Catch ex As System.Exception
            Throw
        End Try
        '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'Dim ObjTable As New PdfPTable(2)
        'Try
        '    With ObjTable
        '        .DefaultCell.Border = 0
        '        .SetTotalWidth(New Single() {5, 5})
        '        .WidthPercentage = 100
        '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '        .AddCell(New Paragraph(gstrRazonSocial & vbCrLf & gstrDireccion & vbCrLf & gstrRuc, _
        '                 FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
        '        .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
        '        .AddCell(New Paragraph("Fecha de Impresión " & Date.Now.ToShortDateString, _
        '                 FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

        '        .DefaultCell.Colspan = 2
        '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
        '        .AddCell(New Paragraph(vbCrLf & "ORDEN DE SERVICIOS POR TRANSPORTISTA Nº" & vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
        '    End With
        '    Return ObjTable
        'Catch ex As System.Exception
        '    Throw
        'End Try
    End Function

    Public Function dtTablaDatosCotizacionOrdenServicios(ByVal vstrIDFile As String, ByVal vstrCotizacion As String, ByVal vstrCliente As String, ByVal vstrTitutlo As String, _
                                                         ByVal vstrFechaIN As String, ByVal vstrFechaOUT As String, ByVal vstrNroPax As String, ByVal vstrResponsable As String, _
                                                         ByVal vstrNombreGuia As String, ByVal vblnEsGuia As Boolean) As PdfPTable
        Dim ObjTable As New PdfPTable(8)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
        Try
            With ObjTable
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {0.5F, 2.5F, 0.5F, 8, 2.5F, 0.5F, 4, 1})
                .WidthPercentage = 90
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT

                DefinirBordesTablas(ObjTable, 0.03, 0, 0.03, 0)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0, 0.03, 0)
                .AddCell(New Paragraph("File", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrIDFile, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Fecha IN", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrFechaIN, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0.03, 0.03, 0)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                DefinirBordesTablas(ObjTable, 0.03, 0, 0, 0)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0, 0, 0)
                .AddCell(New Paragraph("Cotización", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrCotizacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Fecha OUT", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrFechaOUT, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0.03, 0, 0)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                DefinirBordesTablas(ObjTable, 0.03, 0, 0, 0)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0, 0, 0)
                .AddCell(New Paragraph("Cliente", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrCliente, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Nº Pasajero", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrNroPax, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0.03, 0, 0)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

                .DefaultCell.PaddingBottom = 7
                DefinirBordesTablas(ObjTable, 0.03, 0, 0, 0.03)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0, 0, 0.03)
                .AddCell(New Paragraph("Título", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrTitutlo, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph("Responsable", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(vstrResponsable, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0.03, 0, 0.03)
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                DefinirBordesTablas(ObjTable, 0, 0, 0, 0)

                If vstrNombreGuia.Trim <> "" Then
                    .DefaultCell.Colspan = 8
                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph(" ", NewFont))
                    Dim NewFont2 As New Font(bsDefault, 12, Font.BOLD)
                    If vblnEsGuia Then
                        .AddCell(New Paragraph("Guía : ".ToUpper & vstrNombreGuia, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    Else
                        .AddCell(New Paragraph("Transportista : ".ToUpper & vstrNombreGuia, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
                    End If
                End If
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'With ObjTable
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {1, 2.5F, 0.5F, 8, 2.5F, 0.5F, 4, 1})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("File", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("Fecha IN", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrFechaIN, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("Cotización", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrCotizacion, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("Fecha OUT", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrFechaOUT, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("Cliente", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrCliente, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("Nº Pasajero", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrNroPax, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("Título", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrTitutlo, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph("Responsable", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(vstrResponsable, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

            '    If vstrNombreGuia.Trim <> "" Then
            '        .DefaultCell.Colspan = 8
            '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '        .AddCell(New Paragraph(" "))
            '        If vblnEsGuia Then
            '            .AddCell(New Paragraph("Guía : ".ToUpper & vstrNombreGuia, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        Else
            '            .AddCell(New Paragraph("Transportista : ".ToUpper & vstrNombreGuia, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            '        End If
            '    End If
            'End With
            Return ObjTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveCadenaConPrimerMayuscula(ByVal vstrCadena As String, ByVal vblnPrimeraMayuscula As Boolean) As String
        If vstrCadena.ToString = String.Empty Then Return ""
        Try
            Dim strReturnValue As String = ""
            Dim strCadenas() As String = vstrCadena.Split(" ")

            For Each strItem As String In strCadenas
                Dim strItem2 As String = strItem.Substring(0, 1)
                Dim strItem3 As String = strItem.Substring(1, strItem.Length - 1).ToLower

                If vblnPrimeraMayuscula Then
                    strItem2 = strItem2.ToUpper
                End If
                strReturnValue += strItem2 & strItem3 & " "
            Next

            Return strReturnValue.Trim()
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strCorrelativoDocumentoPDF(ByVal vstrIDFile As String) As String
        Dim strCorrDocumento As String = ""
        Dim bytNumeroCorrelativo As Byte = 0
        Try
            Dim strFiles As String()
            'Leyendo los archivos de la carpeta ‘C:\Musica’
            strFiles = IO.Directory.GetFiles(gstrRutaDocumentosPreLiquidacion, "*.pdf")
            For Each strFile In strFiles
                'Dim ioFil As IO.FileInfo = CType(File, IO.FileInfo)
                Dim ioFile As New IO.FileInfo(strFile)
                If InStr(ioFile.Name, vstrIDFile) > 0 Then
                    Dim strArchivo As String = ioFile.Name
                    strArchivo = Replace(ioFile.Name, ".pdf", "")
                    strArchivo = Replace(strArchivo, "Pre-Liquidación " & vstrIDFile & " -", "").ToString.Trim
                    'Dim bytVersion As Byte = CByte(ioFile.Name.Substring(
                    If IsNumeric(strArchivo) Then
                        Dim bytCorrelativo As Byte = CByte(strArchivo)
                        If bytCorrelativo > bytNumeroCorrelativo Then bytNumeroCorrelativo = bytCorrelativo
                    End If
                End If
            Next
            Return (bytNumeroCorrelativo + 1).ToString("00")
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GenerarPdfDocumento(ByVal vdatDatosCabecera As Data.DataTable, ByVal vdatDatosDetalle As Data.DataTable, _
                                        ByVal vdatDatosDebitMemo As Data.DataTable) As String
        If vdatDatosCabecera.Rows.Count = 0 Then Return ""
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5, Font.COURIER)
        Dim NewFont2 As Font = New Font(bsDefault, 10, Font.BOLD)
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 120.0F, 170.0F)
        Dim strIDFile As String = vdatDatosCabecera(0)("IDFile").ToString
        Dim strCotizacion As String = vdatDatosCabecera(0)("Cotizacion").ToString
        Dim strTipoVenta As String = vdatDatosCabecera(0)("TipoVenta").ToString
        Dim strUsVentas As String = vdatDatosCabecera(0)("UsVentas").ToString
        Dim strUsReservas As String = vdatDatosCabecera(0)("UsReservas").ToString
        Dim strUsOperaciones As String = vdatDatosCabecera(0)("UsOperaciones").ToString
        Dim strTitulo As String = vdatDatosCabecera(0)("Titulo").ToString
        Dim strCliente As String = vdatDatosCabecera(0)("DescCliente").ToString
        Dim strDireccion As String = vdatDatosCabecera(0)("Direccion").ToString
        Dim strPais As String = vdatDatosCabecera(0)("Pais").ToString
        Dim strFechaIn As String = vdatDatosCabecera(0)("FechaIn").ToString
        Dim strFechaOut As String = vdatDatosCabecera(0)("FechaOut").ToString
        Dim strNroPax As String = vdatDatosCabecera(0)("NroPax").ToString
        Dim dblSumaTotalVentaUSB As Double = 0.0
        Dim dblSumaFacturacionExp As Double = 0.0
        Dim dblSumaBoletaNoExp As Double = 0.0
        'Dim strRutaDocumentos As String = "D:\Documentos Pre-Liquidacion\"
        Dim strNombreArchivo As String = gstrRutaDocumentosPreLiquidacion & "Pre-Liquidación " & strIDFile & " - 01.pdf"
        Try
            If IO.File.Exists(strNombreArchivo) Then
                'strNombreArchivo = Replace(strNombreArchivo, "01", strCorrelativoDocumentoPDF(strRutaDocumentos, strIDFile))
                strNombreArchivo = gstrRutaDocumentosPreLiquidacion & "Pre-Liquidación " & strIDFile & " - " & strCorrelativoDocumentoPDF(strIDFile) & ".pdf"
            End If

            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            Dim ObjEvent As New PDFFooter()
            ObjEvent.ActionFooterSelect = "DefaultImageHeader"
            pdfw.PageEvent = ObjEvent
            Documento.Open()

            Documento.Add(tblTituloDocumento("Pre - Liquidación " & strIDFile))
            Documento.Add(New Paragraph("Datos Generales", NewFont2))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblEncabezadoDocumento(strIDFile, strCotizacion, strTipoVenta, strUsVentas, strUsReservas, _
                                                 strUsOperaciones, strTitulo, strCliente, strDireccion, strPais, _
                                                 strFechaIn, strFechaOut, strNroPax))

            Dim dblTotalDebitMemo As Double = 0
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(New Paragraph("Debit Memo", NewFont2))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblDebitMemo_Documento(vdatDatosDebitMemo, dblTotalDebitMemo))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblDebitMemoTotal_Documento(dblTotalDebitMemo))

            Documento.Add(New Paragraph("Detalles Costos", NewFont2))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblDetalleAgruparDetalle_Documento(vdatDatosDetalle, dblSumaTotalVentaUSB, _
                                                             dblSumaFacturacionExp, dblSumaBoletaNoExp))
            Documento.Add(New Paragraph(" ", NewFont))

            Documento.Add(New Paragraph("Documentos a Generar", NewFont2))
            Documento.Add(New Paragraph(" ", NewFont))
            Documento.Add(tblDocumentosGenerar_Segment(vdatDatosCabecera, dblTotalDebitMemo, dblSumaTotalVentaUSB, _
                                                       dblSumaFacturacionExp, dblSumaBoletaNoExp))
            Documento.Add(New Paragraph(" ", NewFont))

            pdfw.Flush()
            Documento.Close()
            Return strNombreArchivo
        Catch ex As Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Private Function tblDebitMemoTotal_Documento(ByVal vdblTotalDebitMemo As Double) As PdfPTable
        Dim objTabla As New PdfPTable(10)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5F, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5F, Font.BOLD)
        Try
            With objTabla
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {3.0F, 5.0F, 2.0F, 6.0F, 6.0F, _
                                             15.0F, 6.0F, 6.0F, 6.0F, 6.0F})
                .DefaultCell.BorderWidth = 0
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.HorizontalAlignment = ALIGN_RIGHT

                'Títulos
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)
                .AddCell(New Paragraph("Total", NewFontBold))
                .AddCell(New Paragraph(Format(vdblTotalDebitMemo, "###,###0.00"), NewFontBold))
                DefinirBordesTablas(objTabla, 0.0, 0.0, 0.0, 0.0)
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))

            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDebitMemo_Documento(ByVal vdatDatosDebitMemo As Data.DataTable, _
                                            ByRef rdblTotalDebitMemo As Double) As PdfPTable
        Dim objTabla As New PdfPTable(10)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5F, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5F, Font.BOLD)
        Try
            With objTabla
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {3.0F, 5.0F, 2.0F, 6.0F, 6.0F, _
                                             15.0F, 6.0F, 6.0F, 6.0F, 6.0F})
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE

                'Títulos
                .AddCell(New Paragraph("Item", NewFontBold))
                .AddCell(New Paragraph("Número", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph("Fecha", NewFontBold))
                .AddCell(New Paragraph("Total", NewFontBold))
                .AddCell(New Paragraph("Cobrado a", NewFontBold))
                .AddCell(New Paragraph("Fec. Vencimiento", NewFontBold))
                .AddCell(New Paragraph("Monto", NewFontBold))
                .AddCell(New Paragraph("Comision", NewFontBold))
                .AddCell(New Paragraph("Gastos Transferencia", NewFontBold))

                For Each drItem As DataRow In vdatDatosDebitMemo.Rows
                    .AddCell(New Paragraph(drItem("Item"), NewFont))
                    .AddCell(New Paragraph(drItem("NroFile"), NewFont))
                    .AddCell(New Paragraph(drItem("Correlativo"), NewFont))
                    .AddCell(New Paragraph(drItem("Fecha"), NewFont))
                    .AddCell(New Paragraph(drItem("Total"), NewFont))
                    .AddCell(New Paragraph(drItem("DescCliente"), NewFont))
                    .AddCell(New Paragraph(drItem("FecVencim"), NewFont))
                    .AddCell(New Paragraph(drItem("Monto"), NewFont))
                    .AddCell(New Paragraph(drItem("ComisioBanco"), NewFont))
                    .AddCell(New Paragraph(drItem("GastoTransfer"), NewFont))
                    rdblTotalDebitMemo += Convert.ToDouble(drItem("Total"))
                Next
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDetalleAgruparDetalle_Documento(ByVal vdatDetalleDocumento As Data.DataTable, _
                                                        ByRef rdblTotalVentaUSD As Double, _
                                                        ByRef rdblTotalFactExportacion As Double, _
                                                        ByRef rdblTotalFactNoExportacion As Double) As PdfPTable
        Dim objTabla As New PdfPTable(1)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5F, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5F, Font.BOLD)
        Try
            With objTabla
                .DefaultCell.BorderWidth = 0.0
                .WidthPercentage = 100
                .DefaultCell.Padding = 0
                .AddCell(tblDetalle_Documento(vdatDetalleDocumento, rdblTotalVentaUSD, _
                                              rdblTotalFactExportacion, rdblTotalFactNoExportacion))

                'Pintando los Voucher's
                'DefinirBordesTablas(objTabla, 0.03, 0.03, 0, 0)
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" Voucher", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))

                '.DefaultCell.BorderWidth = 0.03
                'Dim dtFiltrado As Data.DataTable = dtFiltrarDataTable(vdatDetalleDocumento, "IDVoucher <> '0' Or IDProveedor ='001836'")
                Dim dtFiltrado As Data.DataTable = dtFiltrarDataTable(vdatDetalleDocumento, "CoTipoDet = 'VO'")
                For Each dRowItem As Data.DataRow In dtFiltrado.Rows
                    .AddCell(tblDetallexTipo_Documento(dRowItem))
                Next

                'Pintando las Ordenes de Servicio
                'DefinirBordesTablas(objTabla, 0.03, 0.03, 0, 0)
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" Órdenes de Servicio", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))

                '.DefaultCell.BorderWidth = 0.03
                'dtFiltrado = dtFiltrarDataTable(vdatDetalleDocumento, "NuOrdenServicio <> ''")
                dtFiltrado = dtFiltrarDataTable(vdatDetalleDocumento, "CoTipoDet = 'OS'")
                For Each dRowItem As Data.DataRow In dtFiltrado.Rows
                    .AddCell(tblDetallexTipo_Documento(dRowItem))
                Next

                'Pintando Entradas y Presupuestos
                'DefinirBordesTablas(objTabla, 0.03, 0.03, 0, 0)
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" Entradas y Presupuestos", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))

                '.DefaultCell.BorderWidth = 0.03
                'dtFiltrado = dtFiltrarDataTable(vdatDetalleDocumento, "NuOrdenServicio = '' and IDVoucher = '0' and Interno = '' and VueloBus = '' and IDProveedor <> '001836'")
                dtFiltrado = dtFiltrarDataTable(vdatDetalleDocumento, "CoTipoDet = 'EP'")
                For Each dRowItem As Data.DataRow In dtFiltrado.Rows
                    .AddCell(tblDetallexTipo_Documento(dRowItem))
                Next

                'Pintando Vuelos y Bus
                'DefinirBordesTablas(objTabla, 0.03, 0.03, 0, 0)
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" Buses y Vuelos", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))

                '.DefaultCell.BorderWidth = 0.03
                'dtFiltrado = dtFiltrarDataTable(vdatDetalleDocumento, "NuOrdenServicio = '' and IDVoucher = '0' and Interno = '' and VueloBus <> ''")
                dtFiltrado = dtFiltrarDataTable(vdatDetalleDocumento, "CoTipoDet = 'BV'")
                For Each dRowItem As Data.DataRow In dtFiltrado.Rows
                    .AddCell(tblDetallexTipo_Documento(dRowItem))
                Next

                'Pintando Interno
                'DefinirBordesTablas(objTabla, 0.03, 0.03, 0, 0)
                .AddCell(New Paragraph(" ", NewFontBold))
                .AddCell(New Paragraph(" Interno", NewFontBold))
                .AddCell(New Paragraph(" ", NewFontBold))

                '.DefaultCell.BorderWidth = 0.03
                dtFiltrado = dtFiltrarDataTable(vdatDetalleDocumento, "Interno = 'I' and CoTipoDet = 'IN'")
                For Each dRowItem As Data.DataRow In dtFiltrado.Rows
                    .AddCell(tblDetallexTipo_Documento(dRowItem))
                Next
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDetallexTipo_Documento(ByVal vdatRowDocumento As Data.DataRow) As PdfPTable
        'Dim objTable As New PdfPTable(13)
        'Dim objTable As New PdfPTable(14)
        Dim objTable As New PdfPTable(15)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5F, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5F, Font.BOLD)
        Try
            With objTable
                .DefaultCell.BorderWidth = 0.03
                .WidthPercentage = 100
                '.SetTotalWidth(New Single() {15.0F, 6.0F, 6.0F, 5.0F, 5.0F, 5.3F, 8.0F, 10.0F, _
                '                             7.0F, 6.0F, 5.0F, 11.0F, 13.0F})

                ''AGREGAR PAIS EN LA 2DA COLUMNA
                '.SetTotalWidth(New Single() {15.0F, 7.0F, 6.0F, 6.0F, 5.0F, 5.0F, 5.3F, 8.0F, 10.0F, _
                '                           7.0F, 6.0F, 5.0F, 10.0F, 10.0F})

                'AGREGAR “Boleta Servicios Exterior” EN LA ULTIMA COLUMNA
                .SetTotalWidth(New Single() {15.0F, 7.0F, 6.0F, 6.0F, 5.0F, 5.0F, 5.3F, 8.0F, 10.0F, _
                                           7.0F, 6.0F, 5.0F, 9.0F, 9.0F, 9.0F})

                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                'Agregando Títulos
                .AddCell(New Paragraph(vdatRowDocumento("Proveedor"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("Pais"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("NroVoucher_OS"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("SsCompraNeto"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("SsIGVCosto"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("SsIGVSFE"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("IDMonedaCosto"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("SsTotalCostoUSD"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("TipoOperacionContable"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("SsTotalCostoUSD"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("SsMargen"), NewFont))
                .AddCell(New Paragraph(vdatRowDocumento("SsTotalDocumUSD"), NewFont))
                .AddCell(New Paragraph(Format(CDbl(vdatRowDocumento("FacturaExportacion")), "###,##0.00"), NewFont))
                .AddCell(New Paragraph(Format(CDbl(vdatRowDocumento("BoletaNoExportacion")), "###,##0.00"), NewFont))
                .AddCell(New Paragraph(Format(CDbl(vdatRowDocumento("BoletaServiciosExterior")), "###,##0.00"), NewFont))
            End With
            Return objTable
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDetalle_Documento(ByVal vdatDetalleDocumento As Data.DataTable, _
                                          ByRef rdblTotalVentaUSD As Double, _
                                          ByRef rdblTotalFactExportacion As Double, _
                                          ByRef rdblTotalFactNoExportacion As Double) As PdfPTable
        'Dim objTable As New PdfPTable(13)
        'Dim objTable As New PdfPTable(14)
        Dim objTable As New PdfPTable(15)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5F, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5F, Font.BOLD)
        Try
            With objTable
                .DefaultCell.BorderWidth = 0.03
                .WidthPercentage = 100
                '.SetTotalWidth(New Single() {15.0F, 6.0F, 6.0F, 5.0F, 5.0F, 5.3F, 8.0F, 10.0F, _
                '                             7.0F, 6.0F, 5.0F, 11.0F, 13.0F})

                'AGREGAR PAIS EN LA 2DA COLUMNA
                .SetTotalWidth(New Single() {15.0F, 7.0F, 6.0F, 6.0F, 5.0F, 5.0F, 5.3F, 8.0F, 10.0F, _
                                             7.0F, 6.0F, 5.0F, 9.0F, 9.0F, 9.0F})

                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE

                'Agregando Títulos
                .AddCell(New Paragraph("Proveedor", NewFontBold))
                .AddCell(New Paragraph("País Proveedor", NewFontBold))
                .AddCell(New Paragraph("Nro. Voucher/ O.S.", NewFontBold))
                .AddCell(New Paragraph("Compra Neto", NewFontBold))
                .AddCell(New Paragraph("Igv Costo", NewFontBold))
                .AddCell(New Paragraph("IGV SFE", NewFontBold))
                '.AddCell(New Paragraph("Moneda", NewFontBold))
                .AddCell(New Paragraph("Mon.", NewFontBold))
                .AddCell(New Paragraph("Costo Total USD", NewFontBold))
                .AddCell(New Paragraph("Oper. Contable", NewFontBold))
                .AddCell(New Paragraph("Costo Base USD", NewFontBold))
                .AddCell(New Paragraph("Margen", NewFontBold))
                .AddCell(New Paragraph("Venta USD", NewFontBold))
                .AddCell(New Paragraph("Facturación Exportación USD", NewFontBold))
                .AddCell(New Paragraph("Boleta No Exportación USD", NewFontBold))
                .AddCell(New Paragraph("Boleta Servicios Exterior USD", NewFontBold))

                For Each dRowItem As DataRow In vdatDetalleDocumento.Rows
                    rdblTotalVentaUSD += CDbl(dRowItem("SsTotalDocumUSD"))
                    rdblTotalFactExportacion += CDbl(dRowItem("FacturaExportacion"))
                    rdblTotalFactNoExportacion += CDbl(dRowItem("BoletaNoExportacion"))
                Next
            End With
            Return objTable
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDocumentosTotalesGenerar_Documento(ByVal vbytFilasVacias As Byte, _
                                                           ByVal vdblTotalDebitMemo As Double, _
                                                           ByVal vdblTotalVentaUSD As Double, _
                                                           ByVal vdblTotalFactExportacion As Double, _
                                                           ByVal vdblTotalBoletaNoExportacion As Double) As PdfPTable
        Dim objTabla As New PdfPTable(5)
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 7.5, Font.COURIER)
            Dim NewFontBold As Font = New Font(bsDefault, 7.5, Font.BOLD)
            With objTabla
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {3.0F, 8.0F, 11.2F, 11.2F, 11.2F})
                .DefaultCell.BorderWidth = 0
                '.DefaultCell.Padding = 0
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE

                Dim dblRedondeoMargen As Double = vdblTotalDebitMemo - vdblTotalVentaUSD

                'Calculo
                .AddCell(New Paragraph(" ", NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)
                .AddCell(New Paragraph("Cálculo", NewFontBold))
                .AddCell(New Paragraph(Format(vdblTotalVentaUSD, "###,##0.00"), NewFont))
                .AddCell(New Paragraph(Format(vdblTotalFactExportacion, "###,##0.00"), NewFont))
                .AddCell(New Paragraph(Format(vdblTotalBoletaNoExportacion, "###,##0.00"), NewFont))


                'Redondeo/Margen
                DefinirBordesTablas(objTabla, 0.0, 0.0, 0.0, 0.0)
                .AddCell(New Paragraph(" ", NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)
                .AddCell(New Paragraph("Redondeo/" & vbCrLf & "Margen", NewFontBold))
                .AddCell(New Paragraph(Format(dblRedondeoMargen, "###,##0.00"), NewFont))
                .AddCell(New Paragraph(" ", NewFont))
                .AddCell(New Paragraph(Format(dblRedondeoMargen, "###,##0.00"), NewFont))


                'Calculo
                DefinirBordesTablas(objTabla, 0.0, 0.0, 0.0, 0.0)
                .AddCell(New Paragraph(" ", NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)
                .AddCell(New Paragraph("Total", NewFontBold))
                .AddCell(New Paragraph(Format(vdblTotalDebitMemo, "###,##0.00"), NewFont))
                .AddCell(New Paragraph(Format(vdblTotalFactExportacion, "###,##0.00"), NewFont))
                .AddCell(New Paragraph(Format(vdblTotalBoletaNoExportacion + dblRedondeoMargen, "###,##0.00"), NewFont))

                DefinirBordesTablas(objTabla, 0.0, 0.0, 0.0, 0.0)
                For i = 1 To vbytFilasVacias
                    For Fil As Byte = 1 To 5
                        .AddCell(New Paragraph(" ", NewFont))
                    Next
                Next
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Function tblDocumentosGenerar_Segment(ByVal vdatDocumentos As Data.DataTable, _
                                                  ByVal vdblTotalDebitMemo As Double, _
                                                  ByVal vdblTotalVentaUSD As Double, _
                                                  ByVal vdblTotalFactExportacion As Double, _
                                                  ByVal vdblTotalBoletaNoExportacion As Double) As PdfPTable
        Dim objTabla As New PdfPTable(2)
        Try
            With objTabla
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {50.0F, 50.0F})
                .DefaultCell.BorderWidth = 0
                .DefaultCell.Padding = 0
                Dim intFilas As Integer = vdatDocumentos.Rows.Count - 3
                If intFilas < 0 Then intFilas = 0
                .AddCell(tblDocumentosGenerar_Documento(vdatDocumentos))
                .AddCell(tblDocumentosTotalesGenerar_Documento(intFilas, vdblTotalDebitMemo, _
                                                               vdblTotalVentaUSD, vdblTotalFactExportacion, vdblTotalBoletaNoExportacion))
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDocumentosGenerar_Documento(ByVal vdatDocumentos As Data.DataTable) As PdfPTable
        Dim objTabla As New PdfPTable(4)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5, Font.BOLD)
        Try
            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0.03
                .SetTotalWidth(New Single() {12.0F, 5.0F, 7.0F, 5.0F})
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE

                'DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)
                'Agregamos Titulos 
                .AddCell(New Paragraph("Tipo Documento", NewFontBold))
                .AddCell(New Paragraph("Nro. Documento", NewFontBold))
                .AddCell(New Paragraph("Total Documento USD", NewFontBold))
                .AddCell(New Paragraph("Estado Cobro", NewFontBold))

                For Each dRowItem As DataRow In vdatDocumentos.Rows
                    'DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)
                    .AddCell(New Paragraph(dRowItem("TipoDocumento"), NewFont))
                    .AddCell(New Paragraph(dRowItem("NuDocum"), NewFont))
                    .AddCell(New Paragraph(dRowItem("SsTotalDocumUSD"), NewFont))
                    .AddCell(New Paragraph(dRowItem("EstadoDoc"), NewFont))

                Next
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblTituloDocumento(ByVal vstrTitulo As String) As PdfPTable
        Dim objTabla As New PdfPTable(1)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 16, Font.BOLD)
        Try
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {100})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrTitulo, NewFont))
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblEncabezadoDocumento(ByVal vstrIDFile As String, ByVal vstrCotizacion As String, ByVal vstrTipoVenta As String, _
                                            ByVal vstrUsVentas As String, ByVal vstrUsReservas As String, ByVal vstrUsOperaciones As String, _
                                            ByVal vstrTituloFile As String, ByVal vstrCliente As String, ByVal vstrDireccion As String, _
                                            ByVal vstrPais As String, ByVal vstrFechaIn As String, ByVal vstrFechaOut As String, ByVal vstrNroPax As String) As PdfPTable
        Dim objTabla As New PdfPTable(1)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5, Font.BOLD)
        Try
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {100.0F})
                .WidthPercentage = 100
                .DefaultCell.Padding = 0

                .AddCell(tblDatosEncabezadoxFila4_1("File", "Ejec. Ventas", "Título", vstrIDFile, vstrUsVentas, vstrTituloFile))
                .AddCell(tblDatosEncabezadoxFila4("Cotización", "Ejec. Reservas", "Cliente", "País", vstrCotizacion, vstrUsReservas, vstrCliente, vstrPais))
                .AddCell(tblDatosEncabezadoxFila4_1("Tipo Venta", "Ejec. Operaciones", "Dirección", vstrTipoVenta, vstrUsOperaciones, vstrDireccion))
                .AddCell(tblDatosEncabezadoxFila4_1("Fecha In", "Fecha Out", "Pax", vstrFechaIn, vstrFechaOut, vstrNroPax))
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDatosEncabezadoxFila4_1(ByVal vstrColumnName1 As String, ByVal vstrColumnName2 As String, ByVal vstrColumnName3 As String, _
                                              ByVal vstrDatoCol1 As String, ByVal vstrDatoCol2 As String, ByVal vstrDatoCol3 As String) As PdfPTable
        Dim objTabla As New PdfPTable(9)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5, Font.BOLD)
        Try
            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0.03
                .SetTotalWidth(New Single() {8.0F, 1.0F, 14.0F, 10.0F, 1.0F, 8.0F, 5.5F, 1.0F, 39.5F})
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE

                DefinirBordesTablas(objTabla, 0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthLeft = 0.03
                .AddCell(New Paragraph(vstrColumnName1, NewFontBold))
                .DefaultCell.BorderWidthLeft = 0.0
                .AddCell(New Paragraph(":", NewFont))
                .AddCell(New Paragraph(vstrDatoCol1, NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)

                DefinirBordesTablas(objTabla, 0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthLeft = 0.03
                .AddCell(New Paragraph(vstrColumnName2, NewFontBold))
                .DefaultCell.BorderWidthLeft = 0.0
                .AddCell(New Paragraph(":", NewFont))
                .AddCell(New Paragraph(vstrDatoCol2, NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)

                DefinirBordesTablas(objTabla, 0.0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthLeft = 0.03
                .AddCell(New Paragraph(vstrColumnName3, NewFontBold))
                .DefaultCell.BorderWidthLeft = 0.0
                .AddCell(New Paragraph(":", NewFont))
                .DefaultCell.Colspan = 4
                .DefaultCell.BorderWidthRight = 0.03
                .AddCell(New Paragraph(vstrDatoCol3, NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)

                '.AddCell(New Paragraph(" ", NewFontBold))
                '.AddCell(New Paragraph(" ", NewFont))
                '.AddCell(New Paragraph(" ", NewFont))
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDatosEncabezadoxFila4(ByVal vstrColumnName1 As String, ByVal vstrColumnName2 As String, ByVal vstrColumnName3 As String, ByVal vstrColumnName4 As String, _
                                              ByVal vstrDatoCol1 As String, ByVal vstrDatoCol2 As String, ByVal vstrDatoCol3 As String, ByVal vstrDatoCol4 As String) As PdfPTable
        Dim objTabla As New PdfPTable(12)
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 7.5, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 7.5, Font.BOLD)
        Try
            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0.03
                .SetTotalWidth(New Single() {8.0F, 1.0F, 14.0F, 10.0F, 1.0F, 8.0F, 5.5F, 1.0F, 15.5F, 5.0F, 1.0F, 18.0F})
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE

                DefinirBordesTablas(objTabla, 0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthLeft = 0.03
                .AddCell(New Paragraph(vstrColumnName1, NewFontBold))
                .DefaultCell.BorderWidthLeft = 0.0
                .AddCell(New Paragraph(":", NewFont))
                .AddCell(New Paragraph(vstrDatoCol1, NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)

                DefinirBordesTablas(objTabla, 0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthLeft = 0.03
                .AddCell(New Paragraph(vstrColumnName2, NewFontBold))
                .DefaultCell.BorderWidthLeft = 0.0
                .AddCell(New Paragraph(":", NewFont))
                .AddCell(New Paragraph(vstrDatoCol2, NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)

                DefinirBordesTablas(objTabla, 0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthLeft = 0.03
                .AddCell(New Paragraph(vstrColumnName3, NewFontBold))
                .DefaultCell.BorderWidthLeft = 0.0
                .AddCell(New Paragraph(":", NewFont))
                .AddCell(New Paragraph(vstrDatoCol3, NewFont))
                DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)

                DefinirBordesTablas(objTabla, 0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthLeft = 0.03
                .AddCell(New Paragraph(vstrColumnName4, NewFontBold))
                .DefaultCell.BorderWidthLeft = 0.0
                .AddCell(New Paragraph(":", NewFont))
                'DefinirBordesTablas(objTabla, 0.0, 0.0, 0.03, 0.03)
                .DefaultCell.BorderWidthRight = 0.3
                .AddCell(New Paragraph(vstrDatoCol4, NewFont))

                'DefinirBordesTablas(objTabla, 0.03, 0.03, 0.03, 0.03)
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GenerarPdfPropinasPresupuestos(ByVal vdatDatosPropinas As Data.DataTable) As String
        'Dim Documento As Document = New Document(PageSize.A6.Rotate())
        Dim Documento As Document = New Document(New Rectangle(416.7F, 294.8F), 10.0F, 10.0F, 80.0F, 0.0F)
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim pdfDest As PdfDestination = Nothing
        Dim imgLogo As iTextSharp.text.Image = Nothing
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 12, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 12, Font.BOLD)
        Try
            Dim strNombreArchivo As String = gstrRutaImagenes & "Test.pdf"
            Dim intNumError As Int16 = -1
            Dim strFecha As String = "01/02/2014"
            Dim strIDFile As String = "14070192"
            Dim strNombrePax As String = "Pax 1 Pax 1"
            Dim strVuelo As String = "98840"
            Dim strHora As String = "15:10 pm"
            Dim strServicio As String = "Descripcion del Servicio - Colocando Texto Largo Texto Largo"
            Dim strNumPax As String = "1"
            Dim strNumMaletas As String = "3"
            Dim dblTotal As Double = 30.0
            Try
                pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
                'Definir 100% 
                pdfDest = New PdfDestination(PdfDestination.XYZ, 0, Documento.PageSize.Height, 1.0F)
            Catch ex As Exception
                intNumError = Err.Number
                Return ""
            End Try
            Dim objEvent As New PDFFooter
            objEvent.ActionFooterSelect = "TKTPropinaHeader"
            objEvent.NewPage = False
            pdfw.PageEvent = objEvent
            Documento.Open()

            'Documento.Add(New Paragraph("TKT PROPINA DE MALETAS", NewFontBold))
            Documento.Add(tblTituloTKTPropMalestas("TKT PROPINA DE MALETAS"))
            Documento.Add(tblDetalleTKTPropMalestas(strFecha, strIDFile, strNombrePax, strVuelo, _
                                                    strHora, strServicio, strNumPax, strNumMaletas, dblTotal))

            'Aplicar Zoom del 100%
            Dim Action As PdfAction = PdfAction.GotoLocalPage(1, pdfDest, pdfw)
            pdfw.SetOpenAction(Action)

            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo
        Catch ex As Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Public Function tblTituloTKTPropMalestas(ByVal vstrTitulo As String) As PdfPTable
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 12, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 12, Font.BOLD)
        Try
            Dim objTabla As New PdfPTable(1)
            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrTitulo, NewFont))
            End With

            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function tblDetalleTKTPropMalestas(ByVal vstrFecha As String, ByVal vstrIDFile As String, _
                                              ByVal vstrNombrePax As String, ByVal vstrVuelo As String, _
                                              ByVal vstrHora As String, ByVal vstrServicio As String, _
                                              ByVal vstrNumPax As String, ByVal vstrNumMaletas As String, _
                                              ByVal vdblTotal As Double) As PdfPTable
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 12, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 12, Font.BOLD)
        Try
            Dim objTabla As New PdfPTable(1)
            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0

                'Primera Fila
                .AddCell(tblFecha_y_File_TKTPropMaletas(vstrFecha, vstrIDFile))
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblFecha_y_File_TKTPropMaletas(ByVal vstrFecha As String, ByVal vstrIDFile As String) As PdfPTable
        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 12, Font.COURIER)
        Dim NewFontBold As Font = New Font(bsDefault, 12, Font.BOLD)
        Try
            Dim objTable As New PdfPTable(4)
            With objTable
                .DefaultCell.BorderWidth = 1
                .DefaultCell.PaddingBottom = 4

                .SetTotalWidth(New Single() {2.0F, 4.0F, 2.0F, 4.0F})
                .WidthPercentage = 100

                ' ''First Rows
                ''.DefaultCell.HorizontalAlignment = ALIGN_LEFT
                ''.AddCell(New Paragraph("FECHA:"
            End With
            Return objTable
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function GenerarPdfDebitMemo(ByVal vdttDebitMemo As Data.DataTable, ByVal vblnEsDebitMemoResumen As Boolean) As String
        If vdttDebitMemo.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 36.0F, 36.0F, 36.0F, 170.0F)
        Dim parrafo As New Paragraph
        'Dim imgLogo As iTextSharp.text.Image

        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

        Dim strIDFile As String = If(IsDBNull(vdttDebitMemo(0)("IDFile")) = True, "", vdttDebitMemo(0)("IDFile"))
        Dim strCorrelativo As String = vdttDebitMemo(0)("Correlativo")
        Dim strDescrClient As String = vdttDebitMemo(0)("Client")
        Dim strRuc As String = vdttDebitMemo(0)("RucDI")
        Dim strRef As String = vdttDebitMemo(0)("Ref")
        Dim strDireccion As String = vdttDebitMemo(0)("Direcc")
        Dim strFechaIn As String = vdttDebitMemo(0)("FechaIn")
        Dim strFechaOut As String = vdttDebitMemo(0)("FechaOut")
        Dim strNroPax As String = vdttDebitMemo(0)("NroPax")
        Dim dtFecVenci As Date = CDate(vdttDebitMemo(0)("FecVencim"))
        Dim dblTotalDebitMemo As Double = CDbl(vdttDebitMemo(0)("TotalDebitMemo"))
        Dim strDia As String = dtFecVenci.Day.ToString
        Dim strMes As String = dtFecVenci.Month.ToString
        Dim strAnio As String = dtFecVenci.Year.ToString

        'Datos Banco
        Dim strFechaVenc As String = CDate(vdttDebitMemo(0)("FecVencim")).ToShortDateString
        Dim strAccount As String = vdttDebitMemo(0)("NroCuenta")
        Dim strswift As String = vdttDebitMemo(0)("Swift")
        Dim strBanco As String = vdttDebitMemo(0)("NomBanc")
        Dim strDireccionBanco As String = vdttDebitMemo(0)("Direccion")


        strDia = If(strDia.Length = 1, "0" & strDia, strDia)
        strMes = If(strMes.Length = 1, "0" & strMes, strMes)

        Dim strNombreArchivo As String = gstrRutaImagenes & "Debit Memo " & strIDFile & If(strCorrelativo.Trim = "", "", "-" & strCorrelativo) & ".pdf"
        If vblnEsDebitMemoResumen Then
            strNombreArchivo = gstrRutaImagenes & "Debit Memo " & vdttDebitMemo(0)("IDDebitMemoResumen").ToString.Trim & ".pdf"
        End If
        Dim pdfw As iTextSharp.text.pdf.PdfWriter = Nothing
        Dim blnNewPage As Boolean = False
        Dim bytLimitePage As Byte = If(vblnEsDebitMemoResumen, 12, 0)
        Try
            pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            Dim ObjEvent As New PDFFooter()
            ObjEvent.ActionFooterSelect = "FooterDebitMemo"
            ObjEvent.FechaPago = CDate(strFechaVenc)
            ObjEvent.Account = strAccount
            ObjEvent.Swift = strswift
            ObjEvent.Bank = strBanco
            ObjEvent.Address = strDireccionBanco
            ObjEvent.Comment = "We kindly remind you that bank transfer fees must be covered by the agency/client."
            ObjEvent.Total = dblTotalDebitMemo
            ObjEvent.NewPage = False
            pdfw.PageEvent = ObjEvent
            Documento.Open()

            Dim dblTotal As Double = 0
            Dim bytLineas As Byte = 0
            If vblnEsDebitMemoResumen Then
                strIDFile = vdttDebitMemo(0)("IDDebitMemoResumen").ToString
                strCorrelativo = ""
            End If

            For index As Integer = 1 To 5
                Documento.Add(New Paragraph(" ", NewFont))
            Next

            Documento.Add(tblCabeceraIzquierdaDebitMemo(strIDFile, strCorrelativo))
            Documento.Add(New Paragraph(" ", NewFont))
            NewFont.Size = 4
            Documento.Add(tblDatos_ClienteRucDebitMemo(strDescrClient, strRuc, bytLineas)) 'Salto de linea
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL)))
            Documento.Add(tblDatos_ReferenciaDebitMemo(strRef, bytLineas))
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL)))
            Documento.Add(tblDatos_AddressDebitMemo(strDireccion, bytLineas))
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Salto de linea
            If vblnEsDebitMemoResumen Then
                Documento.Add(tblDatos_FileFecInOUTPaxDebitMemo(" ", " ", " ", " "))
            Else
                Documento.Add(tblDatos_FileFecInOUTPaxDebitMemo(strIDFile, strFechaIn, strFechaOut, strNroPax))
            End If
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Salto de linea
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Salto de linea
            NewFont.Size = 10

ImprimirNewPage:
            If Not blnNewPage Then
                Documento.Add(tblDetalleDebitMemo(vdttDebitMemo, pdfw, bytLineas, bytLimitePage)) 'Salto de linea
            Else
                ObjEvent.NewPage = blnNewPage
                ObjEvent.DatosNewPage = vdttDebitMemo
                Documento.NewPage()
                Documento.Add(tblDetalleDebitMemo(vdttDebitMemo, pdfw, bytLineas, bytLimitePage)) 'Salto de linea
            End If
            If vdttDebitMemo.Rows.Count > 0 Then
                blnNewPage = True
                bytLimitePage += 16
                GoTo ImprimirNewPage
            End If


            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'pdfw = pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            'Dim ObjEvent As New PDFFooter()
            'ObjEvent.ActionFooterSelect = "FooterDebitMemo"
            'ObjEvent.FechaPago = CDate(strFechaVenc)
            'ObjEvent.Account = strAccount
            'ObjEvent.Swift = strswift
            'ObjEvent.Bank = strBanco
            'ObjEvent.Address = strDireccionBanco
            'ObjEvent.Comment = "We kindly remind you that bank transfer fees must be covered by the agency/client."
            'ObjEvent.Total = dblTotalDebitMemo
            'pdfw.PageEvent = ObjEvent
            ''Dim hf As New HeaderFooter(New Paragraph(strInformacionBanco, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)), False)
            ''hf.BorderWidth = 0

            ''Documento.Footer = hf

            'Documento.Open()
            ''Documento.Add(hd)
            ''Documento.Add(hf)

            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            'imgLogo.SetAbsolutePosition(35, 750) 'Posición en el eje cartesiano
            'imgLogo.ScaleAbsoluteWidth(210) 'Ancho de la imagen
            'imgLogo.ScaleAbsoluteHeight(70) 'Altura de la imagen
            'Documento.Add(imgLogo) ' Agrega la imagen al documento

            ''Dibujando los marcos de todos los cuadros.
            ''TABLA CABECERA
            ''Codigo x Rectangulo
            'Dim Rect As PdfContentByte = pdfw.DirectContent
            'Rect.RoundRectangle(433.0F, 760.0F, 122.5F, 55.0F, 5.0F) '(Pos X, Pos Y, Tam width, Tam Height, Redon )
            'Rect.Stroke()

            'Dim Rect2 As PdfContentByte = pdfw.DirectContent
            'Rect2.RoundRectangle(433.0F, 700.0F, 122.5F, 50.0F, 5.0F)
            'Rect2.Stroke()

            ''Dibujando las Lineas
            'Dim Lin1 As PdfContentByte = pdfw.DirectContent
            'Lin1.SetLineWidth(0.85) 'configurando el ancho de linea
            'Lin1.MoveTo(433.0F, 725.5F) 'MoveTo indica el punto de inicio
            'Lin1.LineTo(555.5F, 725.5F) 'LineTo indica hacia donde se dibuja la linea
            'Lin1.Stroke() 'traza la linea actual y se puede iniciar una nueva

            'Dim Lin2 As PdfContentByte = pdfw.DirectContent
            'Lin2.SetLineWidth(0.85) 'configurando el ancho de linea
            'Lin2.MoveTo(472.0F, 725.5F) 'MoveTo indica el punto de inicio
            'Lin2.LineTo(472.0F, 700.0F) 'LineTo indica hacia donde se dibuja la linea
            'Lin2.Stroke() 'traza la linea actual y se puede iniciar una nueva

            'Dim Lin3 As PdfContentByte = pdfw.DirectContent
            'Lin3.SetLineWidth(0.85) 'configurando el ancho de linea
            'Lin3.MoveTo(512.0F, 725.5F) 'MoveTo indica el punto de inicio
            'Lin3.LineTo(512.0F, 700.0F) 'LineTo indica hacia donde se dibuja la linea
            'Lin3.Stroke() 'traza la linea actual y se puede iniciar una nueva

            'Dim dtFecHoy As Date = Date.Now
            'Dim strDia2 As String = dtFecHoy.Day.ToString
            'Dim strMes2 As String = dtFecHoy.Month.ToString
            'Dim strAnio2 As String = dtFecHoy.Year.ToString

            'strDia2 = If(strDia2.Length = 1, "0" & strDia2, strDia2)
            'strMes2 = If(strMes2.Length = 1, "0" & strMes2, strMes2)

            'Dim dblTotal As Double = 0
            'Dim bytLineas As Byte = 0
            'If vblnEsDebitMemoResumen Then
            '    strIDFile = vdttDebitMemo(0)("IDDebitMemoResumen").ToString
            '    strCorrelativo = ""
            'End If

            ''Documento.Add(New Paragraph(" ")) 'Salto de linea
            'Documento.Add(tblCabeceraIzquierdaDebitMemo(strIDFile, strCorrelativo, strDia2, strMes2, strAnio2))
            'Documento.Add(tblDatos_ClienteRucDebitMemo(strDescrClient, strRuc, bytLineas)) 'Salto de linea
            'Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL)))
            'Documento.Add(tblDatos_ReferenciaDebitMemo(strRef, bytLineas))
            'Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL)))
            'Documento.Add(tblDatos_AddressDebitMemo(strDireccion, bytLineas))
            'Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Salto de linea
            'If vblnEsDebitMemoResumen Then
            '    Documento.Add(tblDatos_FileFecInOUTPaxDebitMemo(" ", " ", " ", " "))
            'Else
            '    Documento.Add(tblDatos_FileFecInOUTPaxDebitMemo(strIDFile, strFechaIn, strFechaOut, strNroPax))
            'End If
            'Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Salto de linea
            'Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL))) 'Salto de linea
            'Documento.Add(tblDetalleDebitMemo(vdttDebitMemo, pdfw, bytLineas)) 'Salto de linea
            pdfw.Flush()
            Documento.Close()

            Return strNombreArchivo
        Catch ex As System.Exception
            If pdfw IsNot Nothing Then pdfw.Flush()
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Public Function GenerarPdfListaAcomodoMaster(ByVal vdttAcomodos As Data.DataTable, _
                                                 ByVal vdttAcomodosEspeciales As Data.DataTable, _
                                                 ByVal vdttReservasDetAcomodosEspeciales As Data.DataTable) As String
        If vdttAcomodos.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 33.0F, 20.0F, 36.0F, 20.0F)
        Dim parrafo As New Paragraph
        Dim imgLogo As iTextSharp.text.Image

        Try
            Dim strIDFile As String = vdttAcomodos(0)("IDFile")
            Dim strResponsable As String = vdttAcomodos(0)("Responsable")
            Dim strTituloCot As String = vdttAcomodos(0)("Titulo")
            Dim strNombreArchivo As String = gstrRutaImagenes & "ACOMODO POR FILE - " & strIDFile & ".pdf"
            Dim strIDReserva As String = vdttAcomodos(0)("IDReserva")
            Dim intIDCab As Integer = CInt(vdttAcomodos(0)("IDCab"))

            pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 9, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))

            Documento.Open() 'Abre documento para su escritura
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(15, 740) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(570) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            For i As Byte = 1 To 6
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            Documento.Add(tblMasDatosAcomodoOperacionesPax(strTituloCot, strIDFile, strResponsable))
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            NewFontGray.Size = 10
            Documento.Add(New Paragraph("Acomodo para ", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
            ''
            Dim strNombreCorto As String = "", bytFila As Byte = 0
            Dim LstAcomodoEspeciales As New List(Of String)
            For Each dRow As Data.DataRow In vdttAcomodos.Rows
                If Not CBool(dRow("ExisteAcomodoEspecial")) Then
                    If InStr(strNombreCorto, dRow("NombreCorto")) = 0 Then
                        strNombreCorto += dRow("NombreCorto") & " - "
                    End If
                Else
                    Dim strIDProve_Nombre As String = dRow("IDProveedor").ToString.Trim & "|" & dRow("NombreCorto").ToString.Trim
                    If Not LstAcomodoEspeciales.Contains(strIDProve_Nombre) Then
                        LstAcomodoEspeciales.Add(strIDProve_Nombre)
                    End If
                End If
            Next
            If strNombreCorto <> "" Then strNombreCorto = strNombreCorto.Substring(0, strNombreCorto.Length - 2).Trim
            Documento.Add(New Paragraph(strNombreCorto, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
            NewFontGray.Size = 9
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            Documento.Add(tblAcomodosMasterPax(vdttAcomodos, strIDReserva))

            If LstAcomodoEspeciales.Count > 0 Then
                NewFontGray.Size = 10
                For Each strNombreCortoEspecial As String In LstAcomodoEspeciales
                    Dim strIDProveedor As String = strDevuelveCadenaNoOcultaIzquierda(strNombreCortoEspecial)
                    Documento.Add(New Paragraph(" ", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
                    Documento.Add(New Paragraph("Acomodos Especiales para ", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
                    Documento.Add(New Paragraph(strDevuelveCadenaOcultaDerecha(strNombreCortoEspecial), NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea

                    Documento.Add(tblInsertarAcomodoEspecial(vdttReservasDetAcomodosEspeciales, vdttAcomodosEspeciales, strIDProveedor))
                Next
            End If
            ''''''''''''''''''''''''''''''''''''
            ' ''Dim strIDFile As String = vdttAcomodos(0)("IDFile")
            ' ''Dim strResponsable As String = vdttAcomodos(0)("Responsable")
            ' ''Dim strTituloCot As String = vdttAcomodos(0)("Titulo")
            ' ''Dim strNombreArchivo As String = gstrRutaImagenes & "ACOMODO POR FILE - " & strIDFile & ".pdf"
            ' ''Dim strIDReserva As String = vdttAcomodos(0)("IDReserva")
            ' ''Dim intIDCab As Integer = CInt(vdttAcomodos(0)("IDCab"))

            ' ''pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 

            ' ''Documento.Open() 'Abre documento para su escritura

            ' ''imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            ' ''imgLogo.SetAbsolutePosition(35, 760) 'Posición en el eje cartesiano
            ' ''imgLogo.ScaleAbsoluteWidth(210) 'Ancho de la imagen
            ' ''imgLogo.ScaleAbsoluteHeight(70) 'Altura de la imagen
            ' ''Documento.Add(imgLogo) ' Agrega la imagen al documento

            ' ''Documento.Add(New Paragraph(" ")) 'Salto de linea
            ' ''Documento.Add(New Paragraph(" ")) 'Salto de linea
            ' ''Documento.Add(New Paragraph(" ")) 'Salto de linea
            '' ''Documento.Add(New Paragraph(" ")) 'Salto de linea
            '' ''Documento.Add(New Paragraph(" ")) 'Salto de linea

            ' ''Documento.Add(tblMasDatosAcomodoOperacionesPax(strTituloCot, strIDFile, strResponsable))
            ' ''Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            ' ''Documento.Add(New Paragraph("Acomodo para ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
            ' '' ''
            ' ''Dim strNombreCorto As String = "", bytFila As Byte = 0
            ' ''Dim LstAcomodoEspeciales As New List(Of String)
            ' ''For Each dRow As Data.DataRow In vdttAcomodos.Rows
            ' ''    If Not CBool(dRow("ExisteAcomodoEspecial")) Then
            ' ''        If InStr(strNombreCorto, dRow("NombreCorto")) = 0 Then
            ' ''            strNombreCorto += dRow("NombreCorto") & " - "
            ' ''        End If
            ' ''    Else
            ' ''        Dim strIDProve_Nombre As String = dRow("IDProveedor").ToString.Trim & "|" & dRow("NombreCorto").ToString.Trim
            ' ''        If Not LstAcomodoEspeciales.Contains(strIDProve_Nombre) Then
            ' ''            LstAcomodoEspeciales.Add(strIDProve_Nombre)
            ' ''        End If
            ' ''    End If
            ' ''Next
            ' ''If strNombreCorto <> "" Then strNombreCorto = strNombreCorto.Substring(0, strNombreCorto.Length - 2).Trim
            ' ''Documento.Add(New Paragraph(strNombreCorto, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
            ' ''Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            ' ''Documento.Add(tblAcomodosMasterPax(vdttAcomodos, strIDReserva))

            ' ''If LstAcomodoEspeciales.Count > 0 Then
            ' ''    For Each strNombreCortoEspecial As String In LstAcomodoEspeciales
            ' ''        Dim strIDProveedor As String = strDevuelveCadenaNoOcultaIzquierda(strNombreCortoEspecial)
            ' ''        Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            ' ''        Documento.Add(New Paragraph("Acomodos Especiales para ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
            ' ''        Documento.Add(New Paragraph(strDevuelveCadenaOcultaDerecha(strNombreCortoEspecial), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea

            ' ''        Documento.Add(tblInsertarAcomodoEspecial(vdttReservasDetAcomodosEspeciales, vdttAcomodosEspeciales, strIDProveedor))
            ' ''    Next
            ' ''End If

            Documento.Close()
            Return strNombreArchivo
        Catch ex As System.Exception
            If Documento IsNot Nothing Then Documento.Close()
            Throw
        End Try
    End Function

    Private Function tblInsertarAcomodoEspecial(ByVal vdttReservasDetAcomodosEspeciales As Data.DataTable, _
                                                 ByVal vdttAcomodosEspeciales As Data.DataTable, ByVal vstrIDProveedor As String) As PdfPTable
        'Try
        '    Dim objTabla As New PdfPTable(1)
        '    With objTabla
        '        Dim TmpResDet As Data.DataTable = dtFiltrarDataTable(vdttReservasDetAcomodosEspeciales, "IDProveedor='" & vstrIDProveedor & "'")
        '        Dim TmpResAcomodo As Data.DataTable = dtFiltrarDataTable(vdttAcomodosEspeciales, "IDProveedor='" & vstrIDProveedor & "'")
        '        Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        '        Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
        '        Dim NewFontGray As Font = New Font(bsDefault, 10, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))
        '        .DefaultCell.Border = 0
        '        .WidthPercentage = 100

        '        Dim strIDDet As String = ""
        '        For Each dRowItem As Data.DataRow In TmpResDet.Rows
        '            If strIDDet <> dRowItem("IDDet").ToString Then
        '                strIDDet = dRowItem("IDDet").ToString

        '                Dim strFechaIn As String = dRowItem("FechaIn").ToString
        '                Dim strFechaOut As String = dRowItem("FechaOut").ToString
        '                Dim bytDifDays As Byte = DateDiff(DateInterval.Day, CDate(strFechaIn), CDate(strFechaOut))

        '                For i As Byte = 0 To bytDifDays
        '                    Dim strDate As String = DateAdd(DateInterval.Day, i, CDate(strFechaIn)).ToShortDateString
        '                    Dim dttFiltrado As Data.DataTable = dtFiltrarDataTable(TmpResAcomodo, "FechaIn='" & strDate & "'")

        '                    For Each dtRowItem2 As Data.DataRow In dttFiltrado.Rows
        '                        Dim strIDDetTmp As String = dtRowItem2("IDDet").ToString
        '                        Dim dttListaPax As Data.DataTable = dttDatosListaPaxxIDDetAcomodoEspecial(CInt(strIDDetTmp))
        '                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                        .AddCell(New Paragraph(" ", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 4, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

        '                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                        .AddCell(New Paragraph(dtRowItem2("NoCapacidad").ToString, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                        .AddCell(tblAcomodoCabezeraHabit)

        '                        Dim strEsMatrimonial As String = If(CBool(dtRowItem2("EsMatrimonial")), "1", "0")
        '                        Dim dtFiltradoPax As Data.DataTable = dtFiltrarDataTable(dttListaPax, "QtCapacidad='" & dtRowItem2("QtCapacidad").ToString & "' and EsMatrimonial='" & strEsMatrimonial & "'")
        '                        For Each DRowItemFilter As Data.DataRow In dtFiltradoPax.Rows
        '                            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                            .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '                                                            DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString(), DRowItemFilter("DescTC").ToString()))
        '                        Next
        '                    Next
        '                Next
        '            End If

        '            '.DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '            '.AddCell(tblAcomodoCabezeraHabit)

        '            'For Each DRowItemFilter As Data.DataRow In TmpResAcomodo.Rows
        '            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '            '    .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '            '                                    DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString()))
        '            'Next
        '        Next
        '    End With
        '    '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        '    '' ''Dim objTabla As New PdfPTable(1)
        '    '' ''With objTabla
        '    '' ''    Dim TmpResDet As Data.DataTable = dtFiltrarDataTable(vdttReservasDetAcomodosEspeciales, "IDProveedor='" & vstrIDProveedor & "'")
        '    '' ''    Dim TmpResAcomodo As Data.DataTable = dtFiltrarDataTable(vdttAcomodosEspeciales, "IDProveedor='" & vstrIDProveedor & "'")
        '    '' ''    .DefaultCell.Border = 0
        '    '' ''    .WidthPercentage = 100

        '    '' ''    Dim strIDDet As String = ""
        '    '' ''    For Each dRowItem As Data.DataRow In TmpResDet.Rows
        '    '' ''        If strIDDet <> dRowItem("IDDet").ToString Then
        '    '' ''            strIDDet = dRowItem("IDDet").ToString

        '    '' ''            Dim strFechaIn As String = dRowItem("FechaIn").ToString
        '    '' ''            Dim strFechaOut As String = dRowItem("FechaOut").ToString
        '    '' ''            Dim bytDifDays As Byte = DateDiff(DateInterval.Day, CDate(strFechaIn), CDate(strFechaOut))

        '    '' ''            For i As Byte = 0 To bytDifDays
        '    '' ''                Dim strDate As String = DateAdd(DateInterval.Day, i, CDate(strFechaIn)).ToShortDateString
        '    '' ''                Dim dttFiltrado As Data.DataTable = dtFiltrarDataTable(TmpResAcomodo, "FechaIn='" & strDate & "'")

        '    '' ''                For Each dtRowItem2 As Data.DataRow In dttFiltrado.Rows
        '    '' ''                    Dim strIDDetTmp As String = dtRowItem2("IDDet").ToString
        '    '' ''                    Dim dttListaPax As Data.DataTable = dttDatosListaPaxxIDDetAcomodoEspecial(CInt(strIDDetTmp))
        '    '' ''                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    '' ''                    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))

        '    '' ''                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    '' ''                    .AddCell(New Paragraph(dtRowItem2("NoCapacidad").ToString, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '    '' ''                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    '' ''                    .AddCell(tblAcomodoCabezeraHabit)

        '    '' ''                    Dim strEsMatrimonial As String = If(CBool(dtRowItem2("EsMatrimonial")), "1", "0")
        '    '' ''                    Dim dtFiltradoPax As Data.DataTable = dtFiltrarDataTable(dttListaPax, "QtCapacidad='" & dtRowItem2("QtCapacidad").ToString & "' and EsMatrimonial='" & strEsMatrimonial & "'")
        '    '' ''                    For Each DRowItemFilter As Data.DataRow In dtFiltradoPax.Rows
        '    '' ''                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    '' ''                        .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '    '' ''                                                        DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString()))
        '    '' ''                    Next
        '    '' ''                Next
        '    '' ''            Next
        '    '' ''        End If

        '    '' ''        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    '' ''        .AddCell(tblAcomodoCabezeraHabit)

        '    '' ''        For Each DRowItemFilter As Data.DataRow In TmpResAcomodo.Rows
        '    '' ''            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    '' ''            .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '    '' ''                                            DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString()))
        '    '' ''        Next
        '    '' ''    Next
        '    '' ''End With
        '    Return objTabla
        'Catch ex As Exception
        '    Throw
        'End Try
    End Function

    Public Function GenerarPdfListaAcodomoxReservasDiferentes(ByVal vListIDReservasIguales As List(Of Integer), _
                                                              ByVal vListIDReservasDiferentes As List(Of Integer), _
                                                              ByVal vdttAcomodos As Data.DataTable) As String
        If vdttAcomodos.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 33.0F, 20.0F, 36.0F, 20.0F) 'Dim Documento As New Document 'Declaración del documento
        Dim parrafo As New Paragraph ' Declaración de un parrafo
        Dim imgLogo As iTextSharp.text.Image
        Try
            Dim strIDFile As String = vdttAcomodos(0)("IDFile")
            Dim strResponsable As String = vdttAcomodos(0)("Responsable")
            Dim strTituloCot As String = vdttAcomodos(0)("Titulo")
            Dim strNombreArchivo As String = gstrRutaImagenes & "ACOMODO POR FILE - " & strIDFile & ".pdf"

            pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 9, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))
            Documento.Open() 'Abre documento para su escritura

            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(15, 740) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(570) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            For i As Byte = 1 To 6
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next

            Documento.Add(tblMasDatosAcomodoOperacionesPax(strTituloCot, strIDFile, strResponsable))
            Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            Documento.Add(New Paragraph("Acomodo para ", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
            ''
            If vListIDReservasIguales.Count > 0 Then
                Dim strNombreCorto As String = ""
                Dim strIDReserva As String = ""
                For Each intIDReser As Integer In vListIDReservasIguales
                    If strIDReserva = "" Then strIDReserva = intIDReser.ToString
                    For Each Drow As Data.DataRow In vdttAcomodos.Rows
                        If Drow("IDReserva") = intIDReser.ToString Then
                            If InStr(strNombreCorto, Drow("NombreCorto")) = 0 Then
                                strNombreCorto += Drow("NombreCorto") & " - "
                            End If
                        End If
                    Next
                Next

                If strNombreCorto <> "" Then strNombreCorto = strNombreCorto.Substring(0, strNombreCorto.Length - 2).Trim
                Documento.Add(New Paragraph(strNombreCorto, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
                Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea 'Salto de linea
                Documento.Add(tblAcomodosMasterPax(vdttAcomodos, strIDReserva))
                'Documento.Add(New Paragraph(" "))
            End If

            If vListIDReservasDiferentes.Count > 0 Then
                Dim strNombreCorto As String = ""
                Dim blnPintoAcomodoPara As Boolean = False
                Dim bytCont As Byte = 0
                For Each intIDReser As Integer In vListIDReservasDiferentes
                    For Each Drow As Data.DataRow In vdttAcomodos.Rows
                        If Drow("IDReserva") = intIDReser.ToString Then
                            If vListIDReservasIguales.Count <> 0 Or blnPintoAcomodoPara Then
                                Documento.Add(New Paragraph("Acomodo para ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
                            End If

                            Documento.Add(New Paragraph(Drow("NombreCorto"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
                            Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea 'Salto de linea

                            Documento.Add(tblAcomodosMasterPax(vdttAcomodos, intIDReser.ToString))
                            Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea

                            If bytCont = 0 Then
                                blnPintoAcomodoPara = True
                                bytCont = 1
                            End If
                            Exit For
                        End If
                    Next
                Next
                'Documento.Add(New Paragraph(" "))
            End If

            Documento.Close()
            Return strNombreArchivo
        Catch ex As System.Exception
            Documento.Close()
            Throw
        End Try
        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
        'If vdttAcomodos.Rows.Count = 0 Then Return ""
        'Dim Documento As Document = New Document(PageSize.A4, 33.0F, 20.0F, 36.0F, 20.0F) 'Dim Documento As New Document 'Declaración del documento
        'Dim parrafo As New Paragraph ' Declaración de un parrafo
        'Dim imgLogo As iTextSharp.text.Image
        'Try
        '    Dim strIDFile As String = vdttAcomodos(0)("IDFile")
        '    Dim strResponsable As String = vdttAcomodos(0)("Responsable")
        '    Dim strTituloCot As String = vdttAcomodos(0)("Titulo")
        '    Dim strNombreArchivo As String = gstrRutaImagenes & "ACOMODO POR FILE - " & strIDFile & ".pdf"

        '    pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 

        '    Documento.Open() 'Abre documento para su escritura

        '    imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
        '    imgLogo.SetAbsolutePosition(35, 760) 'Posición en el eje cartesiano
        '    imgLogo.ScaleAbsoluteWidth(210) 'Ancho de la imagen
        '    imgLogo.ScaleAbsoluteHeight(70) 'Altura de la imagen
        '    Documento.Add(imgLogo) ' Agrega la imagen al documento

        '    Documento.Add(New Paragraph(" ")) 'Salto de linea
        '    Documento.Add(New Paragraph(" ")) 'Salto de linea
        '    Documento.Add(New Paragraph(" ")) 'Salto de linea
        '    'Documento.Add(New Paragraph(" ")) 'Salto de linea
        '    'Documento.Add(New Paragraph(" ")) 'Salto de linea

        '    Documento.Add(tblMasDatosAcomodoOperacionesPax(strTituloCot, strIDFile, strResponsable))
        '    Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
        '    Documento.Add(New Paragraph("Acomodo para ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
        '    ''
        '    If vListIDReservasIguales.Count > 0 Then
        '        Dim strNombreCorto As String = ""
        '        Dim strIDReserva As String = ""
        '        For Each intIDReser As Integer In vListIDReservasIguales
        '            If strIDReserva = "" Then strIDReserva = intIDReser.ToString
        '            For Each Drow As Data.DataRow In vdttAcomodos.Rows
        '                If Drow("IDReserva") = intIDReser.ToString Then
        '                    If InStr(strNombreCorto, Drow("NombreCorto")) = 0 Then
        '                        strNombreCorto += Drow("NombreCorto") & " - "
        '                    End If
        '                End If
        '            Next
        '        Next

        '        If strNombreCorto <> "" Then strNombreCorto = strNombreCorto.Substring(0, strNombreCorto.Length - 2).Trim
        '        Documento.Add(New Paragraph(strNombreCorto, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
        '        Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea 'Salto de linea
        '        Documento.Add(tblAcomodosMasterPax(vdttAcomodos, strIDReserva))
        '        'Documento.Add(New Paragraph(" "))
        '    End If

        '    If vListIDReservasDiferentes.Count > 0 Then
        '        Dim strNombreCorto As String = ""
        '        Dim blnPintoAcomodoPara As Boolean = False
        '        Dim bytCont As Byte = 0
        '        For Each intIDReser As Integer In vListIDReservasDiferentes
        '            For Each Drow As Data.DataRow In vdttAcomodos.Rows
        '                If Drow("IDReserva") = intIDReser.ToString Then
        '                    If vListIDReservasIguales.Count <> 0 Or blnPintoAcomodoPara Then
        '                        Documento.Add(New Paragraph("Acomodo para ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
        '                    End If

        '                    Documento.Add(New Paragraph(Drow("NombreCorto"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))) 'Salto de linea
        '                    Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea 'Salto de linea

        '                    Documento.Add(tblAcomodosMasterPax(vdttAcomodos, intIDReser.ToString))
        '                    Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea

        '                    If bytCont = 0 Then
        '                        blnPintoAcomodoPara = True
        '                        bytCont = 1
        '                    End If
        '                    Exit For
        '                End If
        '            Next
        '        Next
        '        'Documento.Add(New Paragraph(" "))
        '    End If

        '    Documento.Close()
        '    Return strNombreArchivo
        'Catch ex As System.Exception
        '    Documento.Close()
        '    Throw
        'End Try
    End Function

    Public Function GenerarPdfListaAcomodosPax(ByVal vdttDatosCabecera As Data.DataTable, ByVal vdttDetalleReservas As Data.DataTable, _
                                               ByVal vdttDetalleReservasEspeciales As Data.DataTable, ByVal vdttDatosListaPaxAcomodoEspecial As Data.DataTable, ByRef rBytNumeroError As Byte) As String
        If vdttDatosCabecera.Rows.Count = 0 Then Return ""
        Dim Documento As Document = New Document(PageSize.A4, 33.0F, 20.0F, 36.0F, 20.0F) 'Declaración del documento
        Dim parrafo As New Paragraph ' Declaración de un parrafo
        Dim imgLogo As iTextSharp.text.Image
        Try
            Dim strIDFile As String = vdttDatosCabecera(0)("IDFile")
            Dim strNombreProveedor As String = vdttDatosCabecera(0)("NombreCorto")
            Dim strDireccion As String = vdttDatosCabecera(0)("Direccion")
            Dim strTituloCot As String = vdttDatosCabecera(0)("Titulo")
            Dim strNroPax As String = vdttDatosCabecera(0)("NroPax")
            Dim strResponsable As String = vdttDatosCabecera(0)("Nombre")
            Dim strCodigoReserva As String = vdttDatosCabecera(0)("CodReservaProv")
            Dim strNacionalidad As String = vdttDatosCabecera(0)("Nacionalidad")
            Dim strIDIdioma As String = vdttDatosCabecera(0)("IDidioma")
            Dim strObservaciones As String = vdttDatosCabecera(0)("Observaciones")
            Dim strNombreArchivo As String = gstrRutaImagenes & "ACOMODOS " & strNombreProveedor & " - " & strIDFile & ".pdf"
            Try
                pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            Catch ex As System.Exception
                rBytNumeroError = Err.Number
                Return ""
            End Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)

            Documento.Open() 'Abre documento para su escritura
            imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            imgLogo.SetAbsolutePosition(15, 740) 'Posición en el eje cartesiano
            imgLogo.ScaleAbsoluteWidth(570) 'Ancho de la imagen
            imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
            Documento.Add(imgLogo) ' Agrega la imagen al documento

            For i As Byte = 1 To 6
                Documento.Add(New Paragraph(" ", NewFont)) 'Salto de linea
            Next
            Documento.Add(tblMasDatosAcomodoPax(strTituloCot, strIDFile, strResponsable, strNombreProveedor, _
                                                strCodigoReserva, strObservaciones))
            Documento.Add(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            Documento.Add(tblAcomodosPax(vdttDatosCabecera, vdttDetalleReservas, vdttDatosListaPaxAcomodoEspecial, vdttDetalleReservasEspeciales))
            Documento.Add(New Paragraph(" ", NewFont))
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim strIDFile As String = vdttDatosCabecera(0)("IDFile")
            'Dim strNombreProveedor As String = vdttDatosCabecera(0)("NombreCorto")
            'Dim strDireccion As String = vdttDatosCabecera(0)("Direccion")
            'Dim strTituloCot As String = vdttDatosCabecera(0)("Titulo")
            'Dim strNroPax As String = vdttDatosCabecera(0)("NroPax")
            'Dim strResponsable As String = vdttDatosCabecera(0)("Nombre")
            'Dim strCodigoReserva As String = vdttDatosCabecera(0)("CodReservaProv")
            'Dim strNacionalidad As String = vdttDatosCabecera(0)("Nacionalidad")
            'Dim strIDIdioma As String = vdttDatosCabecera(0)("IDidioma")
            ''Dim strFechaOut As String = vdttDatosCabecera(0)("FechaOut")
            ''Dim strFechaIn As String = vdttDatosCabecera(0)("FechaIn")
            'Dim strObservaciones As String = vdttDatosCabecera(0)("Observaciones")
            'Dim strNombreArchivo As String = gstrRutaImagenes & "ACOMODOS " & strNombreProveedor & " - " & strIDFile & ".pdf"
            'Try
            '    pdf.PdfWriter.GetInstance(Documento, New FileStream(strNombreArchivo, FileMode.Create)) 'Crea el archivo 
            'Catch ex As System.Exception
            '    rBytNumeroError = Err.Number
            '    Return ""
            'End Try

            'Documento.Open() 'Abre documento para su escritura

            'imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
            'imgLogo.SetAbsolutePosition(35, 760) 'Posición en el eje cartesiano
            'imgLogo.ScaleAbsoluteWidth(210) 'Ancho de la imagen
            'imgLogo.ScaleAbsoluteHeight(70) 'Altura de la imagen
            'Documento.Add(imgLogo) ' Agrega la imagen al documento

            'Documento.Add(New Paragraph(" ")) 'Salto de linea
            'Documento.Add(New Paragraph(" ")) 'Salto de linea
            'Documento.Add(New Paragraph(" ")) 'Salto de linea

            'Documento.Add(tblMasDatosAcomodoPax(strTituloCot, strIDFile, strResponsable, strNombreProveedor, _
            '                                    strCodigoReserva, strObservaciones))
            'Documento.Add(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            'Documento.Add(tblAcomodosPax(vdttDatosCabecera, vdttDetalleReservas, vdttDatosListaPaxAcomodoEspecial, vdttDetalleReservasEspeciales))
            'Documento.Add(New Paragraph(" "))

            Documento.Close()
            Return strNombreArchivo

        Catch ex As System.Exception
            'pdfw.Flush()
            Documento.Close()
            Throw
        End Try
    End Function

    Private Function tblAcomodoTitulo(ByVal vstrIDFile As String) As PdfPTable
        Try
            Dim objTablaTit As New PdfPTable(2)

            objTablaTit.DefaultCell.Border = 0

            objTablaTit.SetTotalWidth(New Single() {6, 2})
            objTablaTit.WidthPercentage = 100

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            objTablaTit.AddCell(New Paragraph("FILE #", FontFactory.GetFont(strTipoLetraVoucher, 14, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 14, Font.BOLD)))


            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDatos_ClienteRucDebitMemo(ByVal vstrDesrCliente As String, ByVal vstrRuc As String, _
                                                  ByRef rbytLineasAdd As Byte) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(4)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.Border = 0
                .DefaultCell.PaddingBottom = 4

                .SetTotalWidth(New Single() {4, 20, 3, 8})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("CLIENT        :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 0.03
                .AddCell(New Paragraph(vstrDesrCliente, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                'Dim bytLetrasMaxima As Byte = 49
                rbytLineasAdd += Len(vstrDesrCliente) \ 49

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 0
                .AddCell(New Paragraph("RUC/DI :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 0.03
                .AddCell(New Paragraph(vstrRuc, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(4)
            'With objTabla
            '    .DefaultCell.Border = 0
            '    .DefaultCell.PaddingBottom = 4

            '    .SetTotalWidth(New Single() {4, 20, 3, 8})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '    .AddCell(New Paragraph("CLIENT        :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrDesrCliente, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    Dim bytLetrasMaxima As Byte = 49
            '    rbytLineasAdd += Len(vstrDesrCliente) \ 49

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 0
            '    .AddCell(New Paragraph("RUC/DI :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrRuc, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    Dim strTextoCelda As String = ""
            '    Dim pdfpRw As PdfPRow = .Rows(.Rows.Count)

            '    strTextoCelda = pdfpRw.GetCells
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function tblFooterTotalDebitMemo(ByVal vdblTotal As Double) As PdfPTable
        Try
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim TabTotal As New PdfPTable(3)
            With TabTotal
                .DefaultCell.Border = 0
                .SetTotalWidth(New Single() {55, 45, 90})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = RIGHT_ALIGN
                .AddCell(New Paragraph("TOTAL", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .AddCell(New Paragraph("IN  USD", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .AddCell(New Paragraph(Format(vdblTotal, "###,##0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim TabTotal As New PdfPTable(3)
            'With TabTotal
            '    .DefaultCell.Border = 0
            '    .SetTotalWidth(New Single() {55, 45, 90})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = RIGHT_ALIGN
            '    .AddCell(New Paragraph("TOTAL", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph("IN  USD", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph(Format(vdblTotal, "###,##0.00"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            'End With
            Return TabTotal
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDatos_ReferenciaDebitMemo(ByVal vstrRef As String, ByRef rbytLineasAdd As Byte) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(4)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.Border = 0
                .DefaultCell.PaddingBottom = 4

                .SetTotalWidth(New Single() {4, 20, 3, 8})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("REF             :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 0.03
                .AddCell(New Paragraph(vstrRef, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                rbytLineasAdd += Len(vstrRef) \ 95

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 0
                .AddCell(New Paragraph("DATE :", NewFont))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 0.03
                .AddCell(New Paragraph(Date.Now.ToShortDateString, NewFont))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'With objTabla
            '    .DefaultCell.Border = 0
            '    .DefaultCell.PaddingBottom = 4

            '    .SetTotalWidth(New Single() {4, 31})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '    .AddCell(New Paragraph("REF             :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrRef, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    rbytLineasAdd += Len(vstrRef) \ 95
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDatos_AddressDebitMemo(ByVal vstrDireccion As String, ByRef rbytLineasAdd As Byte) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.Border = 0
                .DefaultCell.PaddingBottom = 4

                .SetTotalWidth(New Single() {4, 31})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("ADDRESS   :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 0.03
                .AddCell(New Paragraph(vstrDireccion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                rbytLineasAdd += Len(vstrDireccion) \ 84
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'With objTabla
            '    .DefaultCell.Border = 0
            '    .DefaultCell.PaddingBottom = 4

            '    .SetTotalWidth(New Single() {4, 31})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '    .AddCell(New Paragraph("ADDRESS   :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrDireccion, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    rbytLineasAdd += Len(vstrDireccion) \ 84
            '    'If InStr(vstrDireccion, "a") > 0 Or InStr(vstrDireccion, "e") > 0 Or InStr(vstrDireccion, "i") > 0 Or _
            '    ' InStr(vstrDireccion, "o") > 0 Then
            '    '    rbytLineasAdd += Len(vstrDireccion) \ 95
            '    'Else
            '    '    rbytLineasAdd += Len(vstrDireccion) \ 84
            '    'End If
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDatos_FileFecInOUTPaxDebitMemo(ByVal vstrFile As String, ByVal vstrFechaIn As String, ByVal vstrFechaOUT As String, ByVal vstrPax As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(8)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.Border = 0
                .DefaultCell.PaddingBottom = 4

                .SetTotalWidth(New Single() {2.23F, 3, 1, 3, 1, 3, 1.55F, 4.43F})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("FILE             :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .DefaultCell.BorderWidthBottom = 0.03
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrFile, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


                .DefaultCell.BorderWidthBottom = 0
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("IN  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .DefaultCell.BorderWidthBottom = 0.03
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrFechaIn, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.BorderWidthBottom = 0
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("OUT:", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.BorderWidthBottom = 0.03
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrFechaOUT, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.BorderWidthBottom = 0
                .AddCell(New Paragraph("N° PAX :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.BorderWidthBottom = 0.03
                .AddCell(New Paragraph(vstrPax, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(8)
            'With objTabla
            '    .DefaultCell.Border = 0
            '    .DefaultCell.PaddingBottom = 4

            '    .SetTotalWidth(New Single() {2.23F, 3, 1, 3, 1, 3, 1.55F, 4.43F})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '    .AddCell(New Paragraph("FILE             :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .DefaultCell.BorderWidthBottom = 1
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrFile, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 0
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph("IN  :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .DefaultCell.BorderWidthBottom = 1
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrFechaIn, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.BorderWidthBottom = 0
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph("OUT:", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .DefaultCell.BorderWidthBottom = 1
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrFechaOUT, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.BorderWidthBottom = 0
            '    .AddCell(New Paragraph("N° PAX :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrPax, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFechaInOutDatosCliente(ByVal vstrIDFile As String, ByVal vstrFechaIn As String, ByVal vstrFechaOut As String) As PdfPTable
        Try
            Dim ObjSbTabla As New PdfPTable(5)
            With ObjSbTabla
                .DefaultCell.Border = 0
                .SetTotalWidth(New Single() {5, 2, 5, 2, 5})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 1
                .AddCell(New Paragraph(vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .DefaultCell.BorderWidthBottom = 0
                .AddCell(New Paragraph("IN", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 1
                .AddCell(New Paragraph(vstrFechaIn, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .DefaultCell.BorderWidthBottom = 0
                .AddCell(New Paragraph("OUT", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .DefaultCell.BorderWidthBottom = 1
                .AddCell(New Paragraph(vstrFechaOut, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim ObjSbTabla As New PdfPTable(5)
            'With ObjSbTabla
            '    .DefaultCell.Border = 0
            '    .SetTotalWidth(New Single() {5, 2, 5, 2, 5})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.BorderWidthBottom = 0
            '    .AddCell(New Paragraph("IN", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrFechaIn, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.BorderWidthBottom = 0
            '    .AddCell(New Paragraph("OUT", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(vstrFechaOut, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            'End With
            Return ObjSbTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFooterResponsableOrdenPago(ByVal vstrUserName As String) As PdfPTable
        Try
            Dim objTablaFoot As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTablaFoot
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {10, 5})
                .WidthPercentage = 100

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(tblFirmaOrdenPago(vstrUserName))


            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaFoot As New PdfPTable(2)
            'With objTablaFoot
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {10, 5})
            '    .WidthPercentage = 100

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(tblFirmaOrdenPago(vstrUserName))


            'End With

            Return objTablaFoot
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFirmaOrdenPago(ByVal vstrUserNombre As String) As PdfPTable
        Try
            Dim objTablaFoot As New PdfPTable(1)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTablaFoot
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {10})
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER

                Dim newFont2 As New Font(bsDefault, 4, Font.COURIER)
                .AddCell(New Paragraph(vbCrLf & vbCrLf & vbCrLf & vstrUserNombre, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .DefaultCell.BorderWidthBottom = 0.03F
                .AddCell(New Paragraph(" ", newFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL)))
                .DefaultCell.BorderWidthBottom = 0
                .AddCell(New Paragraph(" ", newFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 2, Font.NORMAL)))
                .AddCell(New Paragraph("RESPONSABLE" & vbCrLf, NewFontBold)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))


            End With
            '''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaFoot As New PdfPTable(1)
            'With objTablaFoot
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {10})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER

            '    .AddCell(New Paragraph(vbCrLf & vbCrLf & vbCrLf & vstrUserNombre, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(" " & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL)))
            '    .DefaultCell.BorderWidthBottom = 0
            '    .AddCell(New Paragraph(" " & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 2, Font.NORMAL)))
            '    .AddCell(New Paragraph("RESPONSABLE" & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))


            'End With
            Return objTablaFoot
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFooterOrdenPago(ByVal vstrObservaciones As String, ByVal vstrNoFormaPagoTarjCred As String) As PdfPTable
        Try
            Dim objTablaTit As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTablaTit
                .DefaultCell.BorderWidth = 0.03F
                .SetTotalWidth(New Single() {4, 5, 4})
                .WidthPercentage = 100

                .DefaultCell.Colspan = 3
                .DefaultCell.PaddingBottom = 5
                .AddCell(New Paragraph("OBSERVACIONES", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                If vstrNoFormaPagoTarjCred <> "" Then
                    .DefaultCell.PaddingBottom = 20
                    vstrNoFormaPagoTarjCred = "Proveedor acepta pagos con Tarjetas de Crédito " & vstrNoFormaPagoTarjCred
                    .AddCell(New Paragraph(vstrNoFormaPagoTarjCred, NewFont))
                Else
                    .DefaultCell.PaddingBottom = 10
                End If

                .AddCell(New Paragraph(vstrObservaciones, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaTit As New PdfPTable(3)
            'With objTablaTit
            '    .DefaultCell.BorderWidth = 1
            '    .SetTotalWidth(New Single() {4, 5, 4})
            '    .WidthPercentage = 100

            '    .DefaultCell.Colspan = 3
            '    .DefaultCell.PaddingBottom = 5
            '    .AddCell(New Paragraph("OBSERVACIONES", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.PaddingBottom = 10
            '    .AddCell(New Paragraph(vstrObservaciones, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    '.AddCell(New Paragraph("V°B°" & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    '.AddCell(New Paragraph("RECIBI CONFORME:", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    '.AddCell(tblFirmaFooterOrdenPago())

            'End With
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFirmaFooterOrdenPago() As PdfPTable
        Try
            Dim objtTabl As New PdfPTable(3)
            With objtTabl
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {1, 4, 1})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                'Primera Fila
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                'Segunda Fila
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .DefaultCell.BorderWidthBottom = 1
                .AddCell(New Paragraph(gstrUserNombre, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .DefaultCell.BorderWidthBottom = 0
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                'Tercera Fila
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("RESPONSABLE", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objtTabl As New PdfPTable(3)
            'With objtTabl
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {1, 4, 1})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    'Primera Fila
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    'Segunda Fila
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .DefaultCell.BorderWidthBottom = 1
            '    .AddCell(New Paragraph(gstrUserNombre, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .DefaultCell.BorderWidthBottom = 0
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    'Tercera Fila
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph("RESPONSABLE", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            'End With
            Return objtTabl
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoNroFile_NroPax_Estado(ByVal vstrIDFile As String, ByVal vstrNroPax As String, ByVal vstrEstado As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(9)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .SetTotalWidth(New Single() {4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F})
                .DefaultCell.Padding = 0.0F
                .DefaultCell.BorderWidth = 0.0F
                .AddCell(New Paragraph("N° DE FILE", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(vstrIDFile, NewFont)) '3
                .AddCell(New Paragraph("N° DE PAX", NewFont)) '4
                .AddCell(New Paragraph(":", NewFont)) '5
                .AddCell(New Paragraph(vstrNroPax, NewFont)) '6
                .AddCell(New Paragraph("ESTADO", NewFont)) '7
                .AddCell(New Paragraph(":", NewFont)) '8
                .AddCell(New Paragraph(vstrEstado, NewFont)) '9
            End With

            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoSolvencia_EstadoDM_TCambio(ByVal bytSolvencia As Byte, ByVal vstrEstadoDM As String, ByVal vdblTCambio As Double) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(9)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .SetTotalWidth(New Single() {4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F})
                .DefaultCell.Padding = 0.0F
                .DefaultCell.BorderWidth = 0.0F
                .AddCell(New Paragraph("SOLVENCIA", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(StrDup(bytSolvencia, "*"), NewFont)) '3
                .AddCell(New Paragraph("ESTADO D.M.", NewFont)) '4
                .AddCell(New Paragraph(":", NewFont)) '5
                .AddCell(New Paragraph(vstrEstadoDM, NewFont)) '6
                .AddCell(New Paragraph("T. CAMBIO", NewFont)) '7
                .AddCell(New Paragraph(":", NewFont)) '8
                .AddCell(New Paragraph(vdblTCambio.ToString("#,##0.00"), NewFont)) '9
            End With

            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoSolvencia_EstadoDM_TCambio2(ByVal bytSolvencia As Byte, ByVal vstrEstadoDM As String, ByVal vdblTCambio As Double) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(9)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .SetTotalWidth(New Single() {4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F})
                .DefaultCell.Padding = 0.0F
                .DefaultCell.BorderWidth = 0.0F
                .AddCell(New Paragraph("SOLVENCIA", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(StrDup(bytSolvencia, "*"), NewFont)) '3
                .AddCell(New Paragraph("ESTADO D.M.", NewFont)) '4
                .AddCell(New Paragraph(":", NewFont)) '5
                .AddCell(New Paragraph(vstrEstadoDM, NewFont)) '6
                .AddCell(New Paragraph("T. CAMBIO", NewFont)) '7
                .AddCell(New Paragraph(":", NewFont)) '8
                .AddCell(New Paragraph(vdblTCambio.ToString("#,##0.00"), NewFont)) '9
            End With

            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoReferencia(ByVal vstrReferencia As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .SetTotalWidth(New Single() {3.65F, 0.65F, 29.8F})
                .DefaultCell.Padding = 0.0F
                .DefaultCell.BorderWidth = 0.0F
                .AddCell(New Paragraph("REFERENCIA", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(vstrReferencia, NewFont)) '3
            End With

            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoPaxGrupo(ByVal vstrPaxGrupo As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .SetTotalWidth(New Single() {3.65F, 0.65F, 29.8F})
                .DefaultCell.Padding = 0.0F
                .DefaultCell.BorderWidth = 0.0F
                .AddCell(New Paragraph("PAX/GRUPO", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(vstrPaxGrupo, NewFont)) '3
            End With

            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoSolvencia_EstadoDebitMemo(ByVal bytSolvencia As Byte, ByVal vstrEstadoDebitMemo As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(6)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0.0F
                .WidthPercentage = 100
                .DefaultCell.Padding = 0.0F
                .SetTotalWidth(New Single() {4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 20.1F})

                .AddCell(New Paragraph("SOLVENCIA", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(StrDup(bytSolvencia, "*"), NewFont)) '3
                .AddCell(New Paragraph("ESTADO D.M.", NewFont)) '4
                .AddCell(New Paragraph(":", NewFont)) '5
                .AddCell(New Paragraph(vstrEstadoDebitMemo, NewFont)) '6
            End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoAgencia_EjecutivoVenta(ByVal vstrAgencia As String, ByVal vstrEjecutivoVenta As String) As PdfPTable
        Try
            Dim objTablaAgEj As New PdfPTable(6)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTablaAgEj
                .DefaultCell.Padding = 0.0F
                .DefaultCell.BorderWidth = 0.0F
                .DefaultCell.Padding = 0
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {4.0F, 0.7F, 20.1F, 4.0F, 0.7F, 7.7F})

                .AddCell(New Paragraph("AGENCIA", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(vstrAgencia, NewFont)) '3
                .AddCell(New Paragraph("EJECUTIVO", NewFont)) '4
                .AddCell(New Paragraph(":", NewFont)) '5
                .AddCell(New Paragraph(vstrEjecutivoVenta, NewFont)) '6
            End With
            Return objTablaAgEj
        Catch ex As System.Exception
            Throw
        End Try
    End Function


    Private Function tblOrdenPagoFechaIN_FechaOUT(ByVal vstrFechaIn As String, ByVal vstrFechaOUT As String, Optional ByVal vstrOficina_Facturacion As String = "") As PdfPTable
        Try
            Dim objTabla As New PdfPTable(9)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .SetTotalWidth(New Single() {4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 7.7F})

                .DefaultCell.Padding = 0.0F
                .DefaultCell.BorderWidth = 0.0F


                '.DefaultCell.BorderWidth = 0.0F
                '' .WidthPercentage = 100
                '.DefaultCell.Padding = 0.0F

                ''.SetTotalWidth(New Single() {4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 20.1F})


                .AddCell(New Paragraph("FECHA IN", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(vstrFechaIn, NewFont)) '3
                .AddCell(New Paragraph("FECHA OUT", NewFont)) '4
                .AddCell(New Paragraph(":", NewFont)) '5
                .AddCell(New Paragraph(vstrFechaOUT, NewFont)) '6
                .AddCell(New Paragraph("OF. FACT.", NewFont)) '7
                .AddCell(New Paragraph(":", NewFont)) '8
                .AddCell(New Paragraph(vstrOficina_Facturacion, NewFont)) '9
            End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPagoFechaIN_FechaOUT_Anterior(ByVal vstrFechaIn As String, ByVal vstrFechaOUT As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(6)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0.0F
                .WidthPercentage = 100
                .DefaultCell.Padding = 0.0F
                .SetTotalWidth(New Single() {4.0F, 0.7F, 7.7F, 4.0F, 0.7F, 20.1F})

                .AddCell(New Paragraph("FECHA IN", NewFont)) '1
                .AddCell(New Paragraph(":", NewFont)) '2
                .AddCell(New Paragraph(vstrFechaIn, NewFont)) '3
                .AddCell(New Paragraph("FECHA OUT", NewFont)) '4
                .AddCell(New Paragraph(":", NewFont)) '5
                .AddCell(New Paragraph(vstrFechaOUT, NewFont)) '6
            End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenPago(ByVal vdtt As Data.DataTable) As PdfPTable
        Try
            Dim strIDFile As String = vdtt(0)("IDFile")
            Dim strNroPax As String = vdtt(0)("NroPax")
            Dim strNroLiberados As String = vdtt(0)("NroLiberados")
            Dim strTitulo As String = vdtt(0)("Titulo")
            Dim strReferencia As String = vdtt(0)("ProveedorReferente")
            Dim strCliente As String = vdtt(0)("Cliente")
            Dim strEecutivoVenta As String = vdtt(0)("Nombre")
            Dim strRazonSocial As String = vdtt(0)("ProveedorRazonSocial")
            Dim strCtaCte As String = If(IsDBNull(vdtt(0)("CtaCte")), "", vdtt(0)("CtaCte"))
            Dim strBanco As String = vdtt(0)("BancoDeposito")
            Dim strIDMoneda As String = vdtt(0)("IDMoneda")
            Dim strIDMonedaCuenta As String = vdtt(0)("MonedaIDCuenta")
            Dim dblTipoCambio As Double = CDbl(vdtt(0)("TCambio"))
            Dim strDescEstado As String = vdtt(0)("DescEstado")
            Dim strFechaIN As String = vdtt(0)("FechaIn")
            Dim strFechaOUT As String = vdtt(0)("FechaOut")
            Dim bytSolvencia As Byte = vdtt(0)("Solvencia")
            Dim strEstadoDebiMemo As String = vdtt(0)("EstadoDebitMemo")
            Dim strTipoCuenta As String = vdtt(0)("TipoCuenta")
            Dim strNombreTitularBenef As String = vdtt(0)("TitularBenef")
            Dim strDireccionBenef As String = vdtt(0)("DireccionBenef")
            Dim strNroCtaCte As String = vdtt(0)("NroCuentaBenef")
            Dim strNombreBancoInter As String = vdtt(0)("NombreBancoInte")
            Dim strDireccionBancoInter As String = vdtt(0)("PECBancoInte")
            Dim strSwiftBancoInter As String = vdtt(0)("SWIFTBancoInte")
            Dim strNombreBancoPagador As String = vdtt(0)("NombreBancoPag")
            Dim strDireccionBancoPagador As String = vdtt(0)("PECBancoPag")
            Dim strSwiftBancoPagador As String = vdtt(0)("SWIFTBancoPag")
            Dim dblDetraccion As Double = vdtt(0)("SSDetraccion")
            Dim dblTotal As Double = 0
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim strOficina_Facturacion As String = vdtt(0)("Oficina_Facturacion")

            Dim objTablaTit As New PdfPTable(1)
            With objTablaTit
                .DefaultCell.BorderWidth = 0.03F
                .DefaultCell.PaddingBottom = 5 'Asigna una espacio en la parte inferior
                .WidthPercentage = 100

                .AddCell(tblOrdenPagoNroFile_NroPax_Estado(strIDFile, strNroPax & " / " & strNroLiberados, strDescEstado))
                .AddCell(tblOrdenPagoReferencia(strReferencia))
                .AddCell(tblOrdenPagoPaxGrupo(strTitulo))
                .AddCell(tblOrdenPagoAgencia_EjecutivoVenta(strCliente, strEecutivoVenta))
                '.AddCell(tblOrdenPagoSolvencia_EstadoDebitMemo(bytSolvencia, strEstadoDebiMemo))
                .AddCell(tblOrdenPagoSolvencia_EstadoDM_TCambio(bytSolvencia, strEstadoDebiMemo, dblTipoCambio))
                '.AddCell(tblOrdenPagoSolvencia_EstadoDM_TCambio2(bytSolvencia, strEstadoDebiMemo, dblTipoCambio))
                .AddCell(tblOrdenPagoFechaIN_FechaOUT(strFechaIN, strFechaOUT, strOficina_Facturacion))

                .AddCell(tblCabeceraDetalleOrdenPago(vdtt, dblTotal))

                Dim dblTotal_Detrac As Double = dblTotal - dblDetraccion

                If strIDMoneda = gstrTipoMonedaDol Then
                    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal_Detrac, "#####0.00")).ToUpper & "/100 DOLARES AMERICANOS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                ElseIf strIDMoneda = gstrTipoMonedaSol Then
                    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal_Detrac, "#####0.00")).ToUpper & "/100 SOLES", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                ElseIf strIDMoneda = gstrTipoMonedaChi Then
                    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal_Detrac, "#####0.00")).ToUpper & "/100 PESOS CHILENOS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                ElseIf strIDMoneda = gstrTipoMonedaArg Then
                    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal_Detrac, "#####0.00")).ToUpper & "/100 PESOS ARGENTINOS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                End If
                'If strIDMoneda = gstrTipoMonedaDol Then
                '    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 DOLARES AMERICANOS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                'ElseIf strIDMoneda = gstrTipoMonedaSol Then
                '    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 SOLES", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                'ElseIf strIDMoneda = gstrTipoMonedaChi Then
                '    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 PESOS CHILENOS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                'ElseIf strIDMoneda = gstrTipoMonedaArg Then
                '    .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 PESOS ARGENTINOS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                'End If
                If strTipoCuenta <> gstrtipoCuentaExterior Then
                    .AddCell(tblDatosDeposito1OrdenPago(strRazonSocial, strBanco, strCtaCte, strIDMonedaCuenta))
                    .AddCell(tblDatosDeposito2OrdenPago(" ", " ", " "))
                Else
                    .AddCell(tblTituloDatosCuentaExterior("Datos del Beneficiario"))
                    strNombreTitularBenef = If(strNombreTitularBenef = String.Empty, " ", strNombreTitularBenef)
                    strDireccionBenef = If(strDireccionBenef = String.Empty, " ", strDireccionBenef)
                    strNroCtaCte = If(strNroCtaCte = String.Empty, " ", strNroCtaCte)
                    .AddCell(tblDatosBeneficiarioDepositoExterior(strNombreTitularBenef, strDireccionBenef, strNroCtaCte))
                    .AddCell(tblDatosDeposito2OrdenPago(" ", " ", " "))
                    .AddCell(tblTituloDatosCuentaExterior("Datos del Banco Pagador"))
                    strNombreBancoPagador = If(strNombreBancoPagador = String.Empty, " ", strNombreBancoPagador)
                    strDireccionBancoPagador = If(strDireccionBancoPagador = String.Empty, " ", strDireccionBancoPagador)
                    strSwiftBancoPagador = If(strSwiftBancoPagador = String.Empty, " ", strSwiftBancoPagador)
                    .AddCell(tblDatosBancoDepositoExterior(strNombreBancoPagador, strDireccionBancoPagador, strSwiftBancoPagador))
                    .AddCell(tblTituloDatosCuentaExterior("Datos del Banco Intermediario"))
                    strNombreBancoInter = If(strNombreBancoInter = String.Empty, " ", strNombreBancoInter)
                    strDireccionBancoInter = If(strDireccionBancoInter = String.Empty, " ", strDireccionBancoInter)
                    strSwiftBancoInter = If(strSwiftBancoInter = String.Empty, " ", strSwiftBancoInter)
                    .AddCell(tblDatosBancoDepositoExterior(strNombreBancoInter, strDireccionBancoInter, strSwiftBancoInter))
                End If
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim strIDFile As String = vdtt(0)("IDFile")
            'Dim strNroPax As String = vdtt(0)("NroPax")
            'Dim strNroLiberados As String = vdtt(0)("NroLiberados")
            'Dim strTitulo As String = vdtt(0)("Titulo")
            'Dim strReferencia As String = vdtt(0)("ProveedorReferente")
            'Dim strCliente As String = vdtt(0)("Cliente")
            'Dim strEecutivoVenta As String = vdtt(0)("Nombre")
            'Dim strRazonSocial As String = vdtt(0)("ProveedorRazonSocial")
            'Dim strCtaCte As String = If(IsDBNull(vdtt(0)("CtaCte")), "", vdtt(0)("CtaCte"))
            'Dim strBanco As String = vdtt(0)("BancoDeposito")
            'Dim strIDMoneda As String = vdtt(0)("IDMoneda")
            'Dim strDescEstado As String = vdtt(0)("DescEstado")
            'Dim strFechaIN As String = vdtt(0)("FechaIn")
            'Dim strFechaOUT As String = vdtt(0)("FechaOut")
            'Dim dblTotal As Double = 0

            'Dim objTablaTit As New PdfPTable(3)
            'With objTablaTit
            '    .DefaultCell.BorderWidth = 1
            '    '.DefaultCell.BorderWidthTop = 0.5F
            '    '.DefaultCell.BorderWidthBottom = 0.5F
            '    '.DefaultCell.BorderWidthLeft = 0.5F
            '    '.DefaultCell.BorderWidthRight = 0.5F
            '    .DefaultCell.PaddingBottom = 5 'Asigna una espacia en la parte inferior

            '    .SetTotalWidth(New Single() {4, 5, 4})
            '    .WidthPercentage = 100

            '    .AddCell(New Paragraph("N° DE FILE       : " & strIDFile, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph("N° DE PAX    : " & strNroPax & " / " & strNroLiberados, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph("ESTADO      : " & strDescEstado, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.Colspan = 3
            '    .AddCell(New Paragraph("REFERENCIA  : " & strReferencia, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph("PAX/GRUPO    : " & strTitulo, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .AddCell(tblFilaAgenciaEjecutivo(strCliente, strEecutivoVenta))
            '    .AddCell(tblFilaFechaInFechaOut(strFechaIN, strFechaOUT))
            '    .AddCell(tblCabeceraDetalleOrdenPago(vdtt, dblTotal))


            '    If strIDMoneda = gstrTipoMonedaDol Then
            '        .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 DOLARES AMERICANOS", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    ElseIf strIDMoneda = gstrTipoMonedaSol Then
            '        .AddCell(New Paragraph("SON : " & strDevuelveNumeroaLetras(Format(dblTotal, "#####0.00")).ToUpper & "/100 SOLES", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    End If
            '    .AddCell(tblDatosDeposito1OrdenPago(strRazonSocial, strBanco, strCtaCte, strIDMoneda))
            '    .AddCell(tblDatosDeposito2OrdenPago(" ", " ", " "))

            'End With
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDatosBeneficiarioDepositoExterior(ByVal vstrNombreTitular As String, ByVal vstrDireccion As String, ByVal vstrNroCuenta As String) As PdfPTable
        Try
            Dim objTablaDatosDeposito As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTablaDatosDeposito
                .DefaultCell.Border = 0
                .WidthPercentage = 100
                .DefaultCell.Padding = 0.0F

                .DefaultCell.PaddingLeft = 2.0F
                .DefaultCell.PaddingTop = 2.0F
                .AddCell(New Paragraph("NOMBRE DEL TITULAR  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("DIRECCIÓN  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("NÚMERO DE CUENTA :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .AddCell(New Paragraph(vstrNombreTitular, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrDireccion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrNroCuenta, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            Return objTablaDatosDeposito
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDatosBancoDepositoExterior(ByVal vstrNombreTitular As String, ByVal vstrDireccion As String, ByVal vstrSwift As String) As PdfPTable
        Try
            Dim objTablaDatosDeposito As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTablaDatosDeposito
                .DefaultCell.Border = 0
                .WidthPercentage = 100
                .DefaultCell.Padding = 0.0F

                .DefaultCell.PaddingLeft = 2.0F
                .DefaultCell.PaddingTop = 2.0F
                .AddCell(New Paragraph("NOMBRE  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("PAÍS/ESTADO/CIUDAD  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("SWIFT :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .AddCell(New Paragraph(vstrNombreTitular, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrDireccion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrSwift, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            Return objTablaDatosDeposito
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblTituloDatosCuentaExterior(ByVal vstrTitulo As String) As PdfPTable
        Try
            Dim objTableTitulo As New PdfPTable(1)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTableTitulo
                .DefaultCell.Border = 0
                .WidthPercentage = 100
                .DefaultCell.Padding = 0.0F
                .AddCell(New Paragraph(vstrTitulo, NewFontBold))
            End With

            Return objTableTitulo
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblDatosDeposito1OrdenPago(ByVal vstrChequeNombrea As String, ByVal vstrBanco As String, ByVal vstrNroCuenta As String, ByVal vstrIDMoneda As String) As PdfPTable
        Try
            Dim objTablaDatosDeposito As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTablaDatosDeposito
                .DefaultCell.Border = 0
                .WidthPercentage = 100
                .DefaultCell.Padding = 0.0F

                .AddCell(New Paragraph("CHEQUE A NOMBRE DE  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("BCO. EN QUE SE DEPOSITA  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                If vstrIDMoneda = gstrTipoMonedaDol Then
                    .AddCell(New Paragraph("N° DE CTA DÓLARES  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                ElseIf vstrIDMoneda = gstrTipoMonedaSol Then
                    .AddCell(New Paragraph("N° DE CTA NUEVOS SOLES  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                ElseIf vstrIDMoneda = gstrTipoMonedaChi Then
                    .AddCell(New Paragraph("N° DE CTA PESOS CHILENOS  :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                ElseIf vstrIDMoneda = gstrTipoMonedaArg Then
                    .AddCell(New Paragraph("N° DE CTA PESOS ARGENTINOS :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                End If

                .AddCell(New Paragraph(vstrChequeNombrea, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrBanco, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrNroCuenta, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            Return objTablaDatosDeposito
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDatosDeposito2OrdenPago(ByVal vstrBancoPTB As String, ByVal vstrNroCheque As String, ByVal vstrFechaGiro As String) As PdfPTable
        Try
            Dim objTablaDatosDeposito As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTablaDatosDeposito
                .DefaultCell.Border = 0
                .WidthPercentage = 100

                .AddCell(New Paragraph("BANCO SETOURS :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("N° DE CHEQUE :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("FECHA DE GIRO :", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .AddCell(New Paragraph(vstrBancoPTB, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrNroCheque, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrFechaGiro, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaDatosDeposito As New PdfPTable(3)
            'With objTablaDatosDeposito
            '    .DefaultCell.Border = 0
            '    .WidthPercentage = 100

            '    .AddCell(New Paragraph("BANCO SETOURS :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph("N° DE CHEQUE :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph("FECHA DE GIRO :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .AddCell(New Paragraph(vstrBancoPTB, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph(vstrNroCheque, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph(vstrFechaGiro, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            'End With
            Return objTablaDatosDeposito
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCabeceraDetalleOrdenPago(ByVal vdtt As Data.DataTable, ByRef rdblTotal As Double) As PdfPTable
        Try
            Dim strObservacion As String = vdtt(0)("Observacion")
            Dim objTablaDetalle As New PdfPTable(1)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTablaDetalle
                .DefaultCell.Border = 0
                .WidthPercentage = 100
                .DefaultCell.Padding = 0.0F
                '.SetTotalWidth(New Single() {1, 4, 2, 2, 1, 1, 1, 5, 1, 5})
                .AddCell(New Paragraph("DETALLE", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(strObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("LIQUIDACION", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(tblDetalleOrdenPago(vdtt, rdblTotal))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim strObservacion As String = vdtt(0)("Observacion")
            'Dim objTablaDetalle As New PdfPTable(1)
            'With objTablaDetalle
            '    .DefaultCell.Border = 0
            '    .WidthPercentage = 100
            '    '.SetTotalWidth(New Single() {1, 4, 2, 2, 1, 1, 1, 5, 1, 5})
            '    .AddCell(New Paragraph("DETALLE", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph(strObservacion, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(New Paragraph("LIQUIDACION", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '    .AddCell(tblDetalleOrdenPago(vdtt, rdblTotal))
            'End With
            Return objTablaDetalle
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDetalleOrdenPago(ByVal vdtt As Data.DataTable, ByRef rdblTotal As Double) As PdfPTable
        Try
            Dim strIDMoneda As String = vdtt(0)("SimboloMoneda")
            Dim strIDMonedaCuenta As String = vdtt(0)("MonedaIDCuenta")
            Dim strSimMonedaCuenta As String = vdtt(0)("MonedaSimboloCuenta")
            Dim dblTipoCambio As Double = vdtt(0)("TCambio")

            Dim dblDetraccion As Double = vdtt(0)("SSDetraccion")
            Dim dblSubTotal As Double = 0
            Dim objTablaDetalle As PdfPTable
            Dim bytCols As Byte = 10
            objTablaDetalle = New PdfPTable(bytCols)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontBold As New Font(bsDefault, 10, Font.BOLD)
            With objTablaDetalle
                .DefaultCell.Border = 0
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {1, 9, 1.55F, 3, 1, 1, 4, 9, 1.55F, 5})

                Dim dblMontoTotal As Double = 0
                Dim dblSaldoxPagar As Double = 0

                For Each dt As Data.DataRow In vdtt.Rows
                    .AddCell(New Paragraph(dt("CantidadAPagar"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    .AddCell(New Paragraph(dt("Servicio"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    .AddCell(New Paragraph(dt("SimboloMoneda"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                    .AddCell(New Paragraph(Format(CDbl(dt("Total")), "####0.00"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT

                    .AddCell(New Paragraph("X", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    If dt("IDTipoProv") = gstrTipoProveeHoteles Then
                        .AddCell(New Paragraph(dt("Noches"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .DefaultCell.HorizontalAlignment = ALIGN_MIDDLE
                        .AddCell(New Paragraph("Noch =", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    Else
                        .AddCell(New Paragraph(dt("CantidadaPagar"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .DefaultCell.HorizontalAlignment = ALIGN_MIDDLE
                        .AddCell(New Paragraph("Pax =", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    End If
                    .AddCell(New Paragraph(Format(CDbl(dt("TotalCompleto")), "####0.00") & " " & dt("PorPrepago"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    .AddCell(New Paragraph(dt("SimboloMoneda"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                    .AddCell(New Paragraph(dt("TotalPorcentaje"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    dblMontoTotal += Convert.ToDouble(dt("TotalPorcentaje"))
                    dblSaldoxPagar += Convert.ToDouble(dt("SaldoxPagar"))
                Next

                rdblTotal = dblMontoTotal
                Dim NewFont2 As New Font(bsDefault, 2, Font.COURIER)
                'Agregando una fila de separación
                For i = 1 To bytCols
                    If i = bytCols Then .DefaultCell.BorderWidthBottom = 0.03F
                    .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 2, Font.NORMAL)))
                Next

                dblSubTotal = dblMontoTotal
                If dblDetraccion > 0 Then

                    dblMontoTotal = dblSubTotal - dblDetraccion

                    'Agregando Fila de SubTotal
                    For i = 1 To bytCols
                        .DefaultCell.BorderWidthBottom = 0
                        If i > bytCols - 3 Then
                            .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                            Dim strTexto As String = ""
                            If i = bytCols - 2 Then
                                strTexto = "SUBTOTAL"
                            ElseIf i = bytCols - 1 Then
                                strTexto = strIDMoneda
                            ElseIf i = bytCols Then
                                strTexto = Format(dblSubTotal, "####0.00")
                            End If

                            .AddCell(New Paragraph(strTexto, NewFontBold)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                        Else
                            .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        End If
                    Next

                    'Agregando Fila de Detraccion
                    For i = 1 To bytCols
                        .DefaultCell.BorderWidthBottom = 0
                        If i > bytCols - 3 Then
                            .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                            Dim strTexto As String = ""
                            If i = bytCols - 2 Then
                                strTexto = "DETRACCION"
                            ElseIf i = bytCols - 1 Then
                                strTexto = strIDMoneda
                            ElseIf i = bytCols Then
                                strTexto = Format(dblDetraccion, "####0.00")
                            End If

                            .AddCell(New Paragraph(strTexto, NewFontBold)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                        Else
                            .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        End If
                    Next

                End If

                'Agregando Fila de Total
                For i = 1 To bytCols
                    .DefaultCell.BorderWidthBottom = 0
                    If i > bytCols - 3 Then
                        .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                        Dim strTexto As String = ""
                        If i = bytCols - 2 Then
                            strTexto = "TOTAL"
                        ElseIf i = bytCols - 1 Then
                            strTexto = strIDMoneda
                        ElseIf i = bytCols Then
                            strTexto = Format(dblMontoTotal, "####0.00")
                        End If

                        .AddCell(New Paragraph(strTexto, NewFontBold)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                    Else
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    End If
                Next

                If strSimMonedaCuenta <> strIDMoneda And strSimMonedaCuenta <> String.Empty Then
                    For i = 1 To bytCols
                        .DefaultCell.BorderWidthBottom = 0
                        If i > bytCols - 3 Then
                            .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                            Dim strTexto As String = ""
                            If i = bytCols - 2 Then
                                strTexto = "TOTAL en "
                            ElseIf i = bytCols - 1 Then
                                strTexto = strSimMonedaCuenta
                            ElseIf i = bytCols Then
                                Dim dblTmpTotal As Double = dblMontoTotal
                                If strIDMoneda = "US$" And strSimMonedaCuenta <> "US$" Then
                                    dblTmpTotal = dblMontoTotal * dblTipoCambio
                                ElseIf strIDMoneda <> "US$" And strSimMonedaCuenta = "US$" Then
                                    dblTmpTotal = dblMontoTotal / dblTipoCambio
                                End If

                                strTexto = Format(dblTmpTotal, "###,###0.00")
                            End If

                            .AddCell(New Paragraph(strTexto, NewFontBold)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                        Else
                            .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        End If
                    Next
                End If

                'Agregando Fila de Total Pendiente
                For i = 1 To bytCols
                    .DefaultCell.BorderWidthBottom = 0
                    If i > bytCols - 3 Then
                        .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                        Dim strTexto As String = ""
                        If i = bytCols - 2 Then
                            strTexto = "Saldo Pendiente"
                        ElseIf i = bytCols - 1 Then
                            strTexto = strIDMoneda
                        ElseIf i = bytCols Then
                            strTexto = Format(dblSaldoxPagar, "####0.00")
                        End If

                        .AddCell(New Paragraph(strTexto, NewFontBold)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                    Else
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    End If
                Next

                'Imprimir total en la moneda banacaria [Solo si es distinta de la moneda]
                If strSimMonedaCuenta <> strIDMoneda And strSimMonedaCuenta <> String.Empty Then
                    For i = 1 To bytCols
                        .DefaultCell.BorderWidthBottom = 0
                        If i > bytCols - 3 Then
                            .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                            Dim strTexto As String = ""
                            If i = bytCols - 2 Then
                                strTexto = "Saldo Pendiente en"
                            ElseIf i = bytCols - 1 Then
                                strTexto = strSimMonedaCuenta
                            ElseIf i = bytCols Then
                                Dim dblSaldoPendienteTMP As Double = dblSaldoxPagar
                                If strIDMoneda = "US$" And strSimMonedaCuenta <> "US$" Then
                                    dblSaldoPendienteTMP = dblSaldoxPagar * dblTipoCambio
                                ElseIf strIDMoneda <> "US$" And strSimMonedaCuenta = "US$" Then
                                    dblSaldoPendienteTMP = dblSaldoxPagar / dblTipoCambio
                                End If

                                strTexto = Format(dblSaldoPendienteTMP, "###,###0.00")
                            End If

                            .AddCell(New Paragraph(strTexto, NewFontBold)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                        Else
                            .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        End If
                    Next
                End If
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim strIDMoneda As String = vdtt(0)("SimboloMoneda")
            'Dim objTablaDetalle As PdfPTable
            'Dim bytCols As Byte = 10
            ''If vdtt(0)("IDTipoProv") = gstrTipoProveeHoteles Then
            'objTablaDetalle = New PdfPTable(bytCols)
            ''bytCols = 10
            ''Else
            ''objTablaDetalle = New PdfPTable(9)
            ''bytCols = 9
            ''End If

            'With objTablaDetalle
            '    .DefaultCell.Border = 0
            '    .WidthPercentage = 100
            '    .SetTotalWidth(New Single() {1, 9, 1.55F, 3, 1, 1, 4, 9, 1.55F, 5})

            '    Dim dblMontoTotal As Double = 0
            '    Dim dblSaldoxPagar As Double = 0

            '    For Each dt As Data.DataRow In vdtt.Rows
            '        .AddCell(New Paragraph(dt("CantidadAPagar"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        .AddCell(New Paragraph(dt("Servicio"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        .AddCell(New Paragraph(dt("SimboloMoneda"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '        .AddCell(New Paragraph(Format(CDbl(dt("Total")), "####0.00"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT

            '        .AddCell(New Paragraph("X", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        If dt("IDTipoProv") = gstrTipoProveeHoteles Then
            '            .AddCell(New Paragraph(dt("Noches"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '            .DefaultCell.HorizontalAlignment = ALIGN_MIDDLE
            '            .AddCell(New Paragraph("Noch =", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        Else
            '            .AddCell(New Paragraph(dt("CantidadaPagar"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '            .DefaultCell.HorizontalAlignment = ALIGN_MIDDLE
            '            .AddCell(New Paragraph("Pax =", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        End If
            '        .AddCell(New Paragraph(Format(CDbl(dt("TotalCompleto")), "####0.00") & " " & dt("PorPrepago"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        .AddCell(New Paragraph(dt("SimboloMoneda"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '        .AddCell(New Paragraph(dt("TotalPorcentaje"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        dblMontoTotal += Convert.ToDouble(dt("TotalPorcentaje"))
            '        dblSaldoxPagar += Convert.ToDouble(dt("SaldoxPagar"))
            '    Next

            '    rdblTotal = dblMontoTotal
            '    'Agregando una fila de separación
            '    For i = 1 To bytCols
            '        If i = bytCols Then .DefaultCell.BorderWidthBottom = 1
            '        .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.NORMAL)))
            '    Next

            '    'Agregando Fila de Total
            '    For i = 1 To bytCols
            '        .DefaultCell.BorderWidthBottom = 0
            '        If i > bytCols - 3 Then
            '            .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '            Dim strTexto As String = ""
            '            If i = bytCols - 2 Then
            '                strTexto = "TOTAL"
            '            ElseIf i = bytCols - 1 Then
            '                strTexto = strIDMoneda
            '            ElseIf i = bytCols Then
            '                strTexto = Format(dblMontoTotal, "####0.00")
            '            End If

            '            .AddCell(New Paragraph(strTexto, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '        Else
            '            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        End If
            '    Next

            '    'Agregando Fila de Total Pendiente
            '    For i = 1 To bytCols
            '        .DefaultCell.BorderWidthBottom = 0
            '        If i > bytCols - 3 Then
            '            .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '            Dim strTexto As String = ""
            '            If i = bytCols - 2 Then
            '                strTexto = "Saldo Pendiente"
            '            ElseIf i = bytCols - 1 Then
            '                strTexto = strIDMoneda
            '            ElseIf i = bytCols Then
            '                strTexto = Format(dblSaldoxPagar, "####0.00")
            '            End If

            '            .AddCell(New Paragraph(strTexto, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '        Else
            '            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        End If
            '    Next

            'End With
            Return objTablaDetalle
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFilaFechaInFechaOut(ByVal vstrFechaIn As String, ByVal vstrFechaOUT As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            With objTabla
                .DefaultCell.Border = 0
                .DefaultCell.BorderWidth = 1
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {9, 4})

                .AddCell(tblFilaFechaInFechaOut2(vstrFechaIn, vstrFechaOUT))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFilaFechaInFechaOut2(ByVal vstrFechaIN As String, ByVal vstrFechaOUT As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(4)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {1.47F, 1.82F, 1.3F, 3.0F})
                .DefaultCell.Padding = 0
                .DefaultCell.PaddingLeft = 0

                .AddCell(New Paragraph("Fecha IN          :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrFechaIN, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .AddCell(New Paragraph("Fecha OUT    :", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph(vstrFechaOUT, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFilaAgenciaEjecutivo(ByVal vstrAgencia As String, ByVal vstrEjecutivoVenta As String) As PdfPTable
        Try
            Dim objTablaAgEj As New PdfPTable(2)
            With objTablaAgEj
                .DefaultCell.Border = 0
                .DefaultCell.BorderWidth = 1
                .WidthPercentage = 100
                .SetTotalWidth(New Single() {9, 4})

                .AddCell(New Paragraph("AGENCIA        : " & vstrAgencia, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                .AddCell(New Paragraph("EJECUTIVO : " & vstrEjecutivoVenta, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            End With
            Return objTablaAgEj
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblTituloOrdenPago(ByVal vstrIDOrdenPago As String) As PdfPTable
        Try
            Dim objTablaTit As New PdfPTable(1)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 18, Font.BOLD)
            With objTablaTit
                .DefaultCell.Border = 0
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER

                .AddCell(New Paragraph("ORDEN DE PAGO  -  " & vstrIDOrdenPago, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 22, Font.NORMAL)))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaTit As New PdfPTable(1)
            'With objTablaTit
            '    .DefaultCell.Border = 0
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER

            '    .AddCell(New Paragraph("ORDEN DE PAGO  -  " & vstrIDOrdenPago, FontFactory.GetFont(strTipoLetraVoucher, 22, Font.NORMAL)))
            'End With
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblCabeceraIzquierdaOrdenPago(ByVal vstrFechaHoy As String, ByVal vstrFechaPago As String) As PdfPTable
        Try
            Dim objTablaTit As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTablaTit
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {6, 2})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER

                .DefaultCell.Padding = 7
                .AddCell(New Paragraph(" ", NewFont))
                DefinirBordesTablas(objTablaTit, 0.03, 0.03, 0.03, 0.03)
                .AddCell(New Paragraph("EMITIDO :  " & vstrFechaHoy, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

                .DefaultCell.Padding = 3
                For i As Byte = 1 To 2
                    DefinirBordesTablas(objTablaTit, 0, 0, 0, 0)
                    Dim NewFont2 As New Font(bsDefault, 2, Font.COURIER)
                    .AddCell(New Paragraph(" ", NewFont2))
                Next

                .DefaultCell.Padding = 7
                .AddCell(New Paragraph(" ", NewFont))
                DefinirBordesTablas(objTablaTit, 0.03, 0.03, 0.03, 0.03)
                .AddCell(New Paragraph("A GIRAR :  " & vstrFechaPago, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaTit As New PdfPTable(2)
            'With objTablaTit
            '    .DefaultCell.Border = 0
            '    .SetTotalWidth(New Single() {6, 2})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER

            '    .AddCell(New Paragraph(" "))
            '    .AddCell(New Paragraph("EMITIDO :  " & vstrFechaHoy, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            '    .AddCell(New Paragraph(" "))
            '    .AddCell(New Paragraph(" "))

            '    .AddCell(New Paragraph(" "))
            '    .AddCell(New Paragraph("A GIRAR :  " & vstrFechaPago, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'End With
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function blnPantallaFileAbierto(ByVal vstrNoForm As String, ByVal vintIDCab As Integer) As Boolean
        Try
            Dim ListaForms As New List(Of String)
            Dim ListaFormObj As New List(Of Object)
            For Each frm As Object In My.Application.OpenForms
                If frm.Name = vstrNoForm Then
                    If frm.intIDCab = vintIDCab Then
                        If Not ListaForms.Contains(vstrNoForm & " " & vintIDCab.ToString) Then
                            ListaForms.Add(vstrNoForm & " " & vintIDCab.ToString)
                            ListaFormObj.Add(frm)
                        Else
                            'frm.BringToFront()
                            For Each frmAbierto As Object In ListaFormObj
                                If frm.name = frmAbierto.name Then
                                    frmAbierto.BringToFront()

                                End If
                            Next
                            MessageBox.Show("File ya se encuentra abierto.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                            Return True

                        End If
                    End If
                End If
            Next
            Return False

        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblCabeceraIzquierdaDebitMemo(ByVal vstrIDFile As String, ByVal vstrCorrelativo As String) As PdfPTable
        'Private Function tblCabeceraIzquierdaDebitMemo(ByVal vstrIDFile As String, ByVal vstrCorrelativo As String, _
        '                                  ByVal vstrDia As String, ByVal vstrMes As String, ByVal vstrAnio As String) As PdfPTable
        Try
            Dim objTablaTit As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTablaTit
                .DefaultCell.BorderWidth = 0

                .SetTotalWidth(New Single() {6.8F, 2})
                .WidthPercentage = 100

                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.BorderWidthBottom = 0
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(" ", NewFont))

                .DefaultCell.BorderWidthLeft = 0.03
                .DefaultCell.BorderWidthRight = 0.03
                .DefaultCell.BorderWidthTop = 0.03
                .DefaultCell.BorderWidthBottom = 0.03
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .AddCell(tblDivDebitMemoFecha(vstrIDFile, vstrCorrelativo))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaTit As New PdfPTable(2)
            'With objTablaTit
            '    .DefaultCell.Border = 0
            '    .SetTotalWidth(New Single() {6, 2})
            '    .WidthPercentage = 100

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & vbCrLf & gstrDireccion & vbCrLf & _
            '                           "T    : " & gstrTelefonoEmpresa & "                 F   : " & gstrFaxEmpresa & vbCrLf & _
            '                           "E    : " & gstrEmailEmpresa & "             " & gstrWebEmpresa & vbCrLf & _
            '                           "" & Replace(strDevuelveFormatoNombreCorreo(gstrRazonSocial, "", True), "S.a.", "S.A.") & "             RUC: " & gstrRuc, FontFactory.GetFont(strTipoLetraVoucher, 7, Font.NORMAL)))

            '    .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
            '    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '    .AddCell(tblDivDebitMemoFecha(vstrIDFile, vstrCorrelativo, vstrDia, vstrMes, vstrAnio))

            '    '.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    '.AddCell(New Paragraph(" "))
            'End With
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDivDebitMemoFecha(ByVal vstrIDFile As String, ByVal vstrCorrelativo As String) As PdfPTable
        'Private Function tblDivDebitMemoFecha(ByVal vstrIDFile As String, ByVal vstrCorrelativo As String, _
        '                                  ByVal vstrDia As String, ByVal vstrMes As String, ByVal vstrAnio As String) As PdfPTable
        Try
            Dim objTbl As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTbl
                .DefaultCell.Border = 0
                .SetTotalWidth(New Single() {3, 3, 3})
                .SpacingAfter = 6
                .SpacingBefore = 6
                .WidthPercentage = 100
                .DefaultCell.Colspan = 3

                Dim vstrMensaje As String = "DEBIT MEMO" & vbCrLf & vbCrLf & "N° " & vstrIDFile & If(vstrCorrelativo.Trim = "", "", " - " & vstrCorrelativo) & vbCrLf

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .DefaultCell.VerticalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrMensaje.Trim, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            End With
            ''''''''''''''''''''''''''''''''
            'Dim objTbl As New PdfPTable(3)
            'With objTbl
            '    .DefaultCell.Border = 0
            '    .SetTotalWidth(New Single() {3, 3, 3})
            '    .SpacingAfter = 6
            '    .SpacingBefore = 6
            '    .WidthPercentage = 100
            '    .DefaultCell.Colspan = 3

            '    Dim vstrMensaje As String = "DEBIT MEMO" & vbCrLf & vbCrLf & "N° " & vstrIDFile & If(vstrCorrelativo.Trim = "", "", " - " & vstrCorrelativo) & vbCrLf

            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.VerticalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(vstrMensaje & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            '    'Space
            '    .SpacingAfter = 7
            '    .SpacingBefore = 7
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.VerticalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            '    'Segunda Fila : Fecha (Titulo)
            '    .SpacingAfter = 6
            '    .SpacingBefore = 6
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.VerticalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph("DATE", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

            '    'Tercera Fila : Fecha (dmy)
            '    .SpacingAfter = 6
            '    .SpacingBefore = 6
            '    .WidthPercentage = 100
            '    .AddCell(tblFechaDebitMemo(vstrDia, vstrMes, vstrAnio))

            '    'Space
            '    .SpacingAfter = 7
            '    .SpacingBefore = 7
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.VerticalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            'End With
            Return objTbl
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblFechaDebitMemo(ByVal vstrDia As String, ByVal vstrMes As String, ByVal vstrAnio As String) As PdfPTable
        Try
            'Dim strDia As String = Date.Now.Day.ToString
            'Dim strMes As String = Date.Now.Month.ToString
            'Dim strAnio As String = Date.Now.Year.ToString

            Dim objTablaFecha As New PdfPTable(3)
            With objTablaFecha
                .SetTotalWidth(New Single() {3, 3, 3})
                .DefaultCell.BorderWidth = 0
                .WidthPercentage = 100

                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 3, Font.NORMAL, New iTextSharp.text.Color(0, 0, 0))))


                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrDia, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrMes, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrAnio, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 0))))
            End With
            Return objTablaFecha
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    'Private Function tblFooterDatosBanco(ByVal vdblTotal As Double) As PdfPTable
    '    Try

    '    Catch ex As System.Exception
    '        Throw
    '    End Try
    'End Function

    Private Sub DefinirBordesTablas(ByRef vpdfPTable As PdfPTable, ByVal vBorderLeftWidth As Double, _
                                    ByVal vBorderRightWidth As Double, ByVal vBorderTopWidth As Double, _
                                    ByVal vBorderBottomWidth As Double)
        Try
            With vpdfPTable
                .DefaultCell.BorderWidthLeft = vBorderLeftWidth
                .DefaultCell.BorderWidthRight = vBorderRightWidth
                .DefaultCell.BorderWidthTop = vBorderTopWidth
                .DefaultCell.BorderWidthBottom = vBorderBottomWidth
            End With
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function tblDetalleDebitMemo(ByRef vdtDetalle As Data.DataTable, ByVal vpdfWriter As pdf.PdfWriter, _
                                         ByVal vBytLineasAdicionales As Byte, ByVal vbytLimitexPag As Byte) As PdfPTable
        Try
            Dim objTablaDet As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim ListOfOrdDelete As New List(Of Byte)
            With objTablaDet
                .DefaultCell.BorderWidth = 1
                '.DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {2, 20, 4})
                .WidthPercentage = 100

                'Dim sngPosicionY As Single = 605 : Dim sngTamanioHeight As Single = 20
                Dim dblTotal As Double = 0
                Dim bytCont As Byte = 1

                DefinirBordesTablas(objTablaDet, 0, 0, 0.03, 0)
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))

                DefinirBordesTablas(objTablaDet, 0, 0, 0, 0)
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph("ITEM", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .AddCell(New Paragraph("D   E   S   C   R   I   P   T   I   O   N", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .AddCell(New Paragraph("AMOUNT USD", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                DefinirBordesTablas(objTablaDet, 0, 0, 0, 0.03)
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                DefinirBordesTablas(objTablaDet, 0, 0, 0, 0)
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))

                For Each DRow As DataRow In vdtDetalle.Rows
                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph(DRow("Ord"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(DRow("Descripcion"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                    .AddCell(New Paragraph(Format(CDbl(DRow("Total")), "###,##0.00") & "  ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                    dblTotal += CDbl(DRow("Total"))

                    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
                    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))

                    If Convert.ToByte(DRow("Ord")) = vbytLimitexPag Then
                        ListOfOrdDelete.Add(Convert.ToByte(DRow("Ord")))
                        GoTo FinPage
                    Else
                        ListOfOrdDelete.Add(Convert.ToByte(DRow("Ord")))
                    End If
                Next
FinPage:
DeleteOrds:
                For Each bytOrd As Byte In ListOfOrdDelete
                    For Each drowItem As DataRow In vdtDetalle.Rows
                        If Convert.ToByte(drowItem("Ord")) = bytOrd Then
                            vdtDetalle.Rows.Remove(drowItem)
                            ListOfOrdDelete.Remove(bytOrd)
                            GoTo DeleteOrds
                        End If
                    Next
                Next
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTablaDet As New PdfPTable(3)
            'With objTablaDet
            '    .DefaultCell.Border = 0
            '    .SetTotalWidth(New Single() {2, 20, 4})
            '    .WidthPercentage = 100


            '    'Dim sngPosicionY As Single = 605 : Dim sngTamanioHeight As Single = 20
            '    Dim dblTotal As Double = 0
            '    Dim bytCont As Byte = 1


            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 5, Font.BOLD)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 5, Font.BOLD)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 5, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph("ITEM", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph("D  E  S  C  R  I  P  T  I  O  N", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph("AMOUNT USD", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    'Dim bytFilaAdd As Byte = 0
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    For Each DRow As DataRow In vdtDetalle.Rows

            '        .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '        .AddCell(New Paragraph(DRow("Ord"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(DRow("Descripcion"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '        .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            '        .AddCell(New Paragraph(Format(CDbl(DRow("Total")), "###,##0.00") & "  ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        dblTotal += CDbl(DRow("Total"))

            '        'Dim strDescripcion As String = DRow("Descripcion").ToString

            '        .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
            '        .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))
            '        .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 2, Font.BOLD)))

            '        'Dim bytLineas As Byte = bytDevuelveCantidadLineas(DRow("Descripcion").ToString)
            '        'bytFilaAdd += bytLineas
            '        'bytFilaAdd += 1
            '    Next

            '    Dim sglPosY_Add As Single = 0
            '    Dim sglHeight_Add As Single = 0
            '    If vBytLineasAdicionales <> 0 Then
            '        sglPosY_Add = vBytLineasAdicionales * -1.5F
            '        sglHeight_Add = vBytLineasAdicionales * -9.5F
            '    End If

            '    'Cuadro y Lineas del Detalle
            '    Dim Rect As PdfContentByte = vpdfWriter.DirectContent
            '    Rect.RoundRectangle(35.0F, 34.5F + sglPosY_Add, 525.0F, 560.5F + sglHeight_Add, 5.0F) '(Pos X, Pos Y, Tam width, Tam Height, Redon )
            '    'Rect.RoundRectangle(35.0F, 30.5F, 525.0F, 558.5F, 5.0F) '(Pos X, Pos Y, Tam width, Tam Height, Redon )
            '    Rect.Stroke()

            '    Dim sglPuntoX As Single = 0
            '    If vBytLineasAdicionales <> 0 Then
            '        sglPuntoX = vBytLineasAdicionales * -10.0F
            '    End If

            '    'Linea Titulos
            '    Dim Lin As PdfContentByte = vpdfWriter.DirectContent
            '    Lin.SetLineWidth(0.85) 'configurando el ancho de linea
            '    Lin.MoveTo(35.5F, 575.0F + sglPuntoX) 'MoveTo indica el punto de inicio
            '    Lin.LineTo(560.0F, 575.0F + sglPuntoX) 'LineTo indica hacia donde se dibuja la linea
            '    Lin.Stroke() 'traza la linea actual y se puede iniciar una nueva

            '    Dim sglLinaY1 As Single = 0
            '    Dim sglLinaY2 As Single = 0
            '    If vBytLineasAdicionales <> 0 Then
            '        sglLinaY1 = vBytLineasAdicionales * -10
            '        sglLinaY2 = vBytLineasAdicionales * -2
            '    End If
            '    'Lineas Verticales
            '    Dim Lin2 As PdfContentByte = vpdfWriter.DirectContent
            '    Lin2.SetLineWidth(0.85) 'configurando el ancho de linea
            '    Lin2.MoveTo(75.0F, 575.0F + sglLinaY1) 'MoveTo indica el punto de inicio
            '    Lin2.LineTo(75.0F, 35.0F + sglLinaY2) 'LineTo indica hacia donde se dibuja la linea
            '    Lin2.Stroke() 'traza la linea actual y se puede iniciar una nueva

            '    Dim sglLina3Y1 As Single = 0
            '    Dim sglLina3Y2 As Single = 0
            '    If vBytLineasAdicionales <> 0 Then
            '        sglLina3Y1 = vBytLineasAdicionales * -10
            '        sglLina3Y2 = vBytLineasAdicionales * -2
            '    End If
            '    Dim Lin3 As PdfContentByte = vpdfWriter.DirectContent
            '    Lin3.SetLineWidth(0.85) 'configurando el ancho de linea
            '    Lin3.MoveTo(480.0F, 575.0F + sglLina3Y1) 'MoveTo indica el punto de inicio
            '    Lin3.LineTo(480.0F, 35.0F + sglLina3Y2) 'LineTo indica hacia donde se dibuja la linea
            '    Lin3.Stroke() 'traza la linea actual y se puede iniciar una nueva


            '    ''Linea Titulos -----------TEMPORAL PARA CASO DE KEYLA (URGENCIA.)
            '    'Dim Lin As PdfContentByte = vpdfWriter.DirectContent
            '    'Lin.SetLineWidth(0.85) 'configurando el ancho de linea
            '    'Lin.MoveTo(35.5F, 565.0F) 'MoveTo indica el punto de inicio
            '    'Lin.LineTo(560.0F, 565.0F) 'LineTo indica hacia donde se dibuja la linea
            '    'Lin.Stroke() 'traza la linea actual y se puede iniciar una nueva

            '    ''Lineas Verticales
            '    'Dim Lin2 As PdfContentByte = vpdfWriter.DirectContent
            '    'Lin2.SetLineWidth(0.85) 'configurando el ancho de linea
            '    'Lin2.MoveTo(75.0F, 565.0F) 'MoveTo indica el punto de inicio
            '    'Lin2.LineTo(75.0F, 31.0F) 'LineTo indica hacia donde se dibuja la linea
            '    'Lin2.Stroke() 'traza la linea actual y se puede iniciar una nueva

            '    'Dim Lin3 As PdfContentByte = vpdfWriter.DirectContent
            '    'Lin3.SetLineWidth(0.85) 'configurando el ancho de linea
            '    'Lin3.MoveTo(480.0F, 565.0F) 'MoveTo indica el punto de inicio
            '    'Lin3.LineTo(480.0F, 31.0F) 'LineTo indica hacia donde se dibuja la linea
            '    'Lin3.Stroke() 'traza la linea actual y se puede iniciar una nueva

            '    'Linea Separacion Total
            '    Dim Lin4 As PdfContentByte = vpdfWriter.DirectContent
            '    Lin4.SetLineWidth(0.85) 'configurando el ancho de linea
            '    Lin4.MoveTo(75.0F, 51.0F) 'MoveTo indica el punto de inicio
            '    Lin4.LineTo(560.0F, 51.0F) 'LineTo indica hacia donde se dibuja la linea
            '    Lin4.Stroke() 'traza la linea actual y se puede iniciar una nueva
            'End With
            Return objTablaDet
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblSubDetalleDebitMemo(ByVal vdttDetalle As Data.DataTable) As PdfPTable
        Try
            Dim strFechaVenc As String = CDate(vdttDetalle(0)("FecVencim")).ToShortDateString
            Dim strAccount As String = vdttDetalle(0)("NroCuenta")
            Dim strswift As String = vdttDetalle(0)("Swift")
            Dim strBanco As String = vdttDetalle(0)("NomBanc")
            Dim strDireccion As String = vdttDetalle(0)("Direccion")
            Dim objSbDetalle As New PdfPTable(2)
            With objSbDetalle
                .SetTotalWidth(New Single() {14, 20})
                .DefaultCell.Border = 0
                .WidthPercentage = 100

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("TO BE PAIND UNTIL", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD)))
                .AddCell(New Paragraph(strFechaVenc, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)))

                .AddCell(New Paragraph("PAYMENTS SHALL BE MADE TO", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD)))
                .AddCell(New Paragraph(gstrEmpresa, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)))

                .AddCell(New Paragraph("ACCOUNT", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD)))
                .AddCell(New Paragraph(strAccount, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)))

                .AddCell(New Paragraph("SWIFT", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD)))
                .AddCell(New Paragraph(strswift, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)))

                .AddCell(New Paragraph("BANK", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD)))
                .AddCell(New Paragraph(strBanco, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)))

                .AddCell(New Paragraph("ADDRESS", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.BOLD)))
                .AddCell(New Paragraph(strDireccion, FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)))

                .DefaultCell.Colspan = 2
                .AddCell(New Paragraph("We kindly remind you that bank transfer fees must be covered by the agency/client.", FontFactory.GetFont(strTipoLetraVoucher, 8, Font.NORMAL)))
            End With

            Return objSbDetalle
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    ' ''Private Function tblAcomodoPara(ByVal vstrDescrProveedor As String, ByVal vstrDireccion As String) As PdfPTable
    ' ''    Try
    ' ''        Dim objTabla As New PdfPTable(2)
    ' ''        objTabla.SetTotalWidth(New Single() {6, 30})

    ' ''        objTabla.WidthPercentage = 100
    ' ''        objTabla.DefaultCell.Border = 0

    ' ''        objTabla.DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ' ''        objTabla.AddCell(New Paragraph("PARA", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

    ' ''        objTabla.DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ' ''        objTabla.AddCell(New Paragraph(vstrDescrProveedor, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD)))
    ' ''        objTabla.AddCell(" ")

    ' ''        objTabla.DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ' ''        objTabla.AddCell(New Paragraph(vstrDireccion, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))



    ' ''        Return objTabla
    ' ''    Catch ex As System.Exception
    ' ''        Throw
    ' ''    End Try
    ' ''End Function

    ''Private Function tblDetalleAcomodosOperaciones(ByVal vdttAcodomo As Data.DataTable) As PdfPTable
    ''    Try
    ''        Dim objTabla As New PdfPTable(1)
    ''        With objTabla
    ''            .SetTotalWidth(New Single() {100})
    ''            .DefaultCell.BorderWidth = 1
    ''            .WidthPercentage = 100

    ''            'Dim dttSimple As DataTable = dtFiltrarDataTable(vdttAcodomo, "CapacidadHab=1")
    ''            'Dim dttDobles As DataTable = dtFiltrarDataTable(vdttAcodomo, "CapacidadHab=2")

    ''            'Dim dttEsMatrimonial As DataTable = dtFiltrarDataTable(dttDobles, "EsMatrimonial=1")
    ''            'Dim dttNoEsMatrimonial As DataTable = dtFiltrarDataTable(dttDobles, "EsMatrimonial=0")

    ''            'Dim dttTriple As DataTable = dtFiltrarDataTable(vdttAcodomo, "CapacidadHab=3")

    ''            'Dim dttDoblesMatrimoniales As DataTable = dtFiltrarDataTable(dttEsMatrimonial, "CapacidadHab=2")
    ''            'Dim dttDoblesTwin As DataTable = dtFiltrarDataTable(dttNoEsMatrimonial, "CapacidadHab=2")

    ''            Dim strIDReserva As String = ""
    ''            For Each DRow As Data.DataRow In vdttAcodomo.Rows
    ''                If DRow("IDReserva").ToString <> strIDReserva Then
    ''                    'Agregando al Proveedor
    ''                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ''                    .AddCell(New Paragraph(DRow("NombreCorto"), FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

    ''                    strIDReserva = DRow("IDReserva").ToString
    ''                End If
    ''            Next
    ''        End With
    ''        Return objTabla
    ''    Catch ex As System.Exception
    ''        Throw
    ''    End Try
    ''End Function

    Private Function tblMasDatosAcomodoOperacionesPax(ByVal vstrTitulo As String, ByVal vstrIDFile As String, _
                                                      ByVal vstrResponsable As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 9, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 9, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))
            With objTabla
                .SetTotalWidth(New Single() {3, 16})
                .WidthPercentage = 100
                .DefaultCell.Border = 0

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Título", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrTitulo, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Nro. File", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrIDFile, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Responsable.", NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrResponsable, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'With objTabla
            '    .SetTotalWidth(New Single() {3, 16})
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Título", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrTitulo, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Nro. File", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Responsable.", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrResponsable, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblMasDatosAcomodoPax(ByVal vstrTitulo As String, ByVal vstrIDFile As String, ByVal vstrResponsable As String, ByVal vstrDescProveedor As String, _
                                           ByVal vstrCodReserva As String, ByVal vstrObservaciones As String) As PdfPTable
        Try
            'Dim datFechaIn As Date = CDate(vstrFechaIN)
            'Dim datFechaOut As Date = CDate(vstrFechaOut)
            'Dim strDiferDias As String = DateDiff(DateInterval.Day, datFechaIn, datFechaOut).ToString & "N"

            Dim objTabla As New PdfPTable(2)
            objTabla.SetTotalWidth(New Single() {3, 16})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 10, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))
            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Border = 0

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Título", NewFontGray)) 'FontFactory.GetFont("ARIALN", 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrTitulo, NewFont)) 'FontFactory.GetFont("Arial Narrow", 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Nro. File", NewFontGray)) 'FontFactory.GetFont("Arial Narrow", 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrIDFile, NewFont)) 'FontFactory.GetFont("Arial Narrow", 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Responsable.", NewFontGray)) 'FontFactory.GetFont("Arial Narrow", 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrResponsable, NewFont)) 'FontFactory.GetFont("Arial Narrow", 9, Font.NORMAL)))

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont("Arial Narrow", 6, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont("Arial Narrow", 6, Font.NORMAL)))

                'Unión de Celda
                Dim Celda As iTextSharp.text.pdf.PdfPCell
                Dim Prgh As iTextSharp.text.Paragraph
                Prgh = New Paragraph(vstrDescProveedor, NewFontGray) 'FontFactory.GetFont("Arial Narrow", 11, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))

                Celda = New iTextSharp.text.pdf.PdfPCell(Prgh)
                Celda.Colspan = 2
                Celda.Border = 0
                .AddCell(Celda)

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Cód. Reserva", NewFontGray)) 'FontFactory.GetFont("Arial Narrow", 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrCodReserva, NewFont)) 'FontFactory.GetFont("Arial Narrow", 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Observación", NewFontGray)) 'FontFactory.GetFont("Arial Narrow", 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrObservaciones, NewFont)) 'FontFactory.GetFont("Arial Narrow", 10, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'objTabla.SetTotalWidth(New Single() {3, 16})

            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Título", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrTitulo, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Nro. File", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Responsable.", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrResponsable, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 6, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 6, Font.NORMAL)))

            '    'Unión de Celda
            '    Dim Celda As iTextSharp.text.pdf.PdfPCell
            '    Dim Prgh As iTextSharp.text.Paragraph
            '    Prgh = New Paragraph(vstrDescProveedor, FontFactory.GetFont(strTipoLetraVoucher, 11, Font.BOLD, New iTextSharp.text.Color(0, 0, 64)))

            '    Celda = New iTextSharp.text.pdf.PdfPCell(Prgh)
            '    Celda.Colspan = 2
            '    Celda.Border = 0
            '    .AddCell(Celda)

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Cód. Reserva", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrCodReserva, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Observación", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrObservaciones, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            'End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblAcomodoDetalleHabit(ByVal vstrNumHabit As String, ByVal vstrNombrePax As String, ByVal vstrNumeroIdentidad As String, _
                                            ByVal vstrNacionalidad As String, ByVal vstrFecha_Nacimiento As String, ByVal vstrObservacion As String, _
                                            ByVal vstrTC As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(7)
            objTabla.SetTotalWidth(New Single() {1.7F, 7.0F, 4.0F, 2.0F, 3.0F, 9.0F, 1.0F})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 10, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Border = 0

                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph(vstrNumHabit, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrNombrePax, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrNumeroIdentidad, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrNacionalidad, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrFecha_Nacimiento, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrTC, NewFont))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(6)
            'objTabla.SetTotalWidth(New Single() {1.7F, 7.0F, 4.0F, 2.0F, 3.0F, 10.0F})

            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrNumHabit, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrNombrePax, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrNumeroIdentidad, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrNacionalidad, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrFecha_Nacimiento, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrObservacion, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    ''Private Function tblAcomodoDetalleHabit(ByVal vstrObservacionesEspeciales As String)
    ''    Try
    ''        Dim objTabla As New PdfPTable(5)
    ''        objTabla.SetTotalWidth(New Single() {6, 6, 10, 6, 10})

    ''        With objTabla
    ''            .WidthPercentage = 100
    ''            .DefaultCell.Border = 0
    ''            '.DefaultCell.BorderWidthBottom = 1

    ''            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ''            .AddCell(New Paragraph("Observ. Especiales: ", FontFactory.GetFont(strTipoLetraVoucher, 11, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

    ''            Dim Celda As iTextSharp.text.pdf.PdfPCell
    ''            Dim Prgh As iTextSharp.text.Paragraph
    ''            Prgh = New Paragraph(vstrObservacionesEspeciales, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL))

    ''            Celda = New iTextSharp.text.pdf.PdfPCell(Prgh)
    ''            Celda.Colspan = 5
    ''            Celda.Border = 0
    ''            'Celda.BorderWidthBottom = 1
    ''            'Celda.BackgroundColor = Color.LIGHT_GRAY
    ''            'Celda.HorizontalAlignment = iTextSharp.text.Element.ALIGN_LEFT
    ''            'Celda.VerticalAlignment = iTextSharp.text.Element.ALIGN_MIDDLE

    ''            .AddCell(Celda)
    ''        End With

    ''        Return objTabla
    ''    Catch ex As System.Exception
    ''        Throw
    ''    End Try
    ''End Function

    ' ''Private Function tblAcomodoDetalleEspecialHabit(ByVal vdttAcomodosEspeciales As Data.DataTable) As PdfPTable
    ' ''    Try
    ' ''        Dim objTabla As New PdfPTable(7)
    ' ''        objTabla.SetTotalWidth(New Single() {1.7F, 7.0F, 4.0F, 2.0F, 3.0F, 9.8F, 0.2F})
    ' ''        With objTabla
    ' ''            .WidthPercentage = 100
    ' ''            .DefaultCell.BorderWidth = 0

    ' ''            For Each dr2 As DataRow In vdttAcomodosEspeciales.Rows
    ' ''                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ' ''                .AddCell(tblAcomodoCabezeraHabit)

    ' ''                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ' ''                .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
    ' ''                                                dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString()))
    ' ''            Next
    ' ''        End With
    ' ''        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
    ' ''        ''Dim objTabla As New PdfPTable(6)
    ' ''        ''objTabla.SetTotalWidth(New Single() {1.7F, 7.0F, 4.0F, 2.0F, 3.0F, 10.0F})
    ' ''        ''With objTabla
    ' ''        ''    .WidthPercentage = 100
    ' ''        ''    .DefaultCell.BorderWidth = 0

    ' ''        ''    For Each dr2 As DataRow In vdttAcomodosEspeciales.Rows
    ' ''        ''        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ' ''        ''        .AddCell(tblAcomodoCabezeraHabit)

    ' ''        ''        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
    ' ''        ''        .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
    ' ''        ''                                        dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString()))
    ' ''        ''    Next
    ' ''        ''End With
    ' ''        Return objTabla
    ' ''    Catch ex As Exception
    ' ''        Throw
    ' ''    End Try
    ' ''End Function

    Private Function tblAcomodoCabezeraHabit() As PdfPTable
        Try
            Dim objTabla As New PdfPTable(7)
            objTabla.SetTotalWidth(New Single() {1.7F, 7.0F, 4.0F, 2.0F, 3.0F, 9.0F, 1.0F})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 10, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 1

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("# Hab", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Nombre", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Doc. Ident.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Nat.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("DOB", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("Observación", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("TC", NewFont))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(6)
            'objTabla.SetTotalWidth(New Single() {1.7F, 7.0F, 4.0F, 2.0F, 3.0F, 10.0F})

            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.BorderWidth = 1

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("# Hab", FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Nombre", FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Doc. Ident.", FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Nat.", FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("DOB", FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("Observación", FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            'End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblAcomodosMasterPax(ByVal vdttAcomodoPax As Data.DataTable, ByVal vstrIDReserva As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(1)
            Dim dttDobles As DataTable = dtFiltrarDataTable(vdttAcomodoPax, "CapacidadHab=2 and IDReserva=" & vstrIDReserva)

            Dim dttEsMatrimonial As DataTable = dtFiltrarDataTable(dttDobles, "EsMatrimonial=1 and IDReserva=" & vstrIDReserva)
            Dim dttNoEsMatrimonial As DataTable = dtFiltrarDataTable(dttDobles, "EsMatrimonial=0 and IDReserva=" & vstrIDReserva)

            Dim dttSimple As DataTable = dtFiltrarDataTable(vdttAcomodoPax, "CapacidadHab=1 and IDReserva=" & vstrIDReserva)
            Dim dttTriple As DataTable = dtFiltrarDataTable(vdttAcomodoPax, "CapacidadHab=3 and IDReserva=" & vstrIDReserva)

            Dim dttDoblesMatrimoniales As DataTable = dtFiltrarDataTable(dttEsMatrimonial, "CapacidadHab=2 and IDReserva=" & vstrIDReserva)
            Dim dttDoblesTwin As DataTable = dtFiltrarDataTable(dttNoEsMatrimonial, "CapacidadHab=2 and IDReserva=" & vstrIDReserva)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 10, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))

            Dim strTitulo As String = ""

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Border = 0

                If dttSimple.Rows.Count > 0 Then
                    strTitulo = If(IsDBNull(dttSimple(0)("Acomodo")) = True, "", dttSimple(0)("Acomodo"))
                    'Dim bytIDHabit As Byte = 0
                    'Dim dtt2ndoFiltro As DataTable

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(strTitulo, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(tblAcomodoCabezeraHabit)

                    'dtt2ndoFiltro = dtFiltrarDataTable(dttSimple, "IDHabit=" & dr("IDHabit"))
                    For Each dr2 As DataRow In dttSimple.Rows

                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                        .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
                                                        dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString(), dr2("DescTC").ToString()))
                    Next
                    '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
                End If

                If dttDoblesTwin.Rows.Count > 0 Then
                    strTitulo = If(IsDBNull(dttDoblesTwin(0)("Acomodo")) = True, "", dttDoblesTwin(0)("Acomodo"))
                    'Dim bytIDHabit As Byte = 0
                    'Dim dtt2ndoFiltro As DataTable

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(strTitulo, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(tblAcomodoCabezeraHabit)

                    For Each dr2 As DataRow In dttDoblesTwin.Rows
                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                        .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
                                                        dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString(), dr2("DescTC").ToString()))
                    Next
                    '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
                End If

                If dttDoblesMatrimoniales.Rows.Count > 0 Then
                    strTitulo = If(IsDBNull(dttDoblesMatrimoniales(0)("Acomodo")) = True, "", dttDoblesMatrimoniales(0)("Acomodo"))
                    'Dim bytIDHabit As Byte = 0
                    'Dim dtt2ndoFiltro As DataTable

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(strTitulo, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(tblAcomodoCabezeraHabit)

                    For Each dr2 As DataRow In dttDoblesMatrimoniales.Rows
                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                        .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
                                                        dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString(), dr2("DescTC").ToString()))
                    Next
                    '.AddCell(New Paragraph(" "))
                End If

                If dttTriple.Rows.Count > 0 Then
                    strTitulo = If(IsDBNull(dttTriple(0)("Acomodo")) = True, "", dttTriple(0)("Acomodo"))
                    'Dim bytIDHabit As Byte = 0
                    'Dim dtt2ndoFiltro As DataTable

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(strTitulo, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(tblAcomodoCabezeraHabit)

                    For Each dr2 As DataRow In dttTriple.Rows
                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                        .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
                                                        dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString(), dr2("DescTC").ToString()))
                    Next
                    '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
                End If
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(1)
            'Dim dttDobles As DataTable = dtFiltrarDataTable(vdttAcomodoPax, "CapacidadHab=2 and IDReserva=" & vstrIDReserva)

            'Dim dttEsMatrimonial As DataTable = dtFiltrarDataTable(dttDobles, "EsMatrimonial=1 and IDReserva=" & vstrIDReserva)
            'Dim dttNoEsMatrimonial As DataTable = dtFiltrarDataTable(dttDobles, "EsMatrimonial=0 and IDReserva=" & vstrIDReserva)

            'Dim dttSimple As DataTable = dtFiltrarDataTable(vdttAcomodoPax, "CapacidadHab=1 and IDReserva=" & vstrIDReserva)
            'Dim dttTriple As DataTable = dtFiltrarDataTable(vdttAcomodoPax, "CapacidadHab=3 and IDReserva=" & vstrIDReserva)

            'Dim dttDoblesMatrimoniales As DataTable = dtFiltrarDataTable(dttEsMatrimonial, "CapacidadHab=2 and IDReserva=" & vstrIDReserva)
            'Dim dttDoblesTwin As DataTable = dtFiltrarDataTable(dttNoEsMatrimonial, "CapacidadHab=2 and IDReserva=" & vstrIDReserva)


            'Dim strTitulo As String = ""

            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0

            '    If dttSimple.Rows.Count > 0 Then
            '        strTitulo = If(IsDBNull(dttSimple(0)("Acomodo")) = True, "", dttSimple(0)("Acomodo"))
            '        'Dim bytIDHabit As Byte = 0
            '        'Dim dtt2ndoFiltro As DataTable

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(strTitulo, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(tblAcomodoCabezeraHabit)

            '        'dtt2ndoFiltro = dtFiltrarDataTable(dttSimple, "IDHabit=" & dr("IDHabit"))
            '        For Each dr2 As DataRow In dttSimple.Rows

            '            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '            .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
            '                                            dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString()))
            '        Next
            '        '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            '    End If

            '    If dttDoblesTwin.Rows.Count > 0 Then
            '        strTitulo = If(IsDBNull(dttDoblesTwin(0)("Acomodo")) = True, "", dttDoblesTwin(0)("Acomodo"))
            '        'Dim bytIDHabit As Byte = 0
            '        'Dim dtt2ndoFiltro As DataTable

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(strTitulo, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(tblAcomodoCabezeraHabit)

            '        For Each dr2 As DataRow In dttDoblesTwin.Rows
            '            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '            .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
            '                                            dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString()))
            '        Next
            '        '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            '    End If

            '    If dttDoblesMatrimoniales.Rows.Count > 0 Then
            '        strTitulo = If(IsDBNull(dttDoblesMatrimoniales(0)("Acomodo")) = True, "", dttDoblesMatrimoniales(0)("Acomodo"))
            '        'Dim bytIDHabit As Byte = 0
            '        'Dim dtt2ndoFiltro As DataTable

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(strTitulo, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(tblAcomodoCabezeraHabit)

            '        For Each dr2 As DataRow In dttDoblesMatrimoniales.Rows
            '            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '            .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
            '                                            dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString()))
            '        Next
            '        '.AddCell(New Paragraph(" "))
            '    End If

            '    If dttTriple.Rows.Count > 0 Then
            '        strTitulo = If(IsDBNull(dttTriple(0)("Acomodo")) = True, "", dttTriple(0)("Acomodo"))
            '        'Dim bytIDHabit As Byte = 0
            '        'Dim dtt2ndoFiltro As DataTable

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(strTitulo, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(tblAcomodoCabezeraHabit)

            '        For Each dr2 As DataRow In dttTriple.Rows
            '            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '            .AddCell(tblAcomodoDetalleHabit(dr2("IDHabit"), dr2("NombrePax"), dr2("NumIdentidad"), _
            '                                            dr2("NacionalidadPax"), dr2("FechNacimiento"), dr2("ObservEspecial").ToString()))
            '        Next
            '        '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 4, Font.BOLD, New iTextSharp.text.Color(0, 0, 0)))) 'Salto de linea
            '    End If
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function
    Private Function blnAgrupacionAcomodo(ByVal vdttDetalleReservas As Data.DataTable) As Boolean
        Try
            Dim strIDDet As String = "" : Dim strAcomodoGeneral As String = "" : Dim blnAgrupacion As Boolean = True
            For Each dRowItem As Data.DataRow In vdttDetalleReservas.Rows
                If dRowItem("IDDet").ToString <> strIDDet Then
                    strIDDet = dRowItem("IDDet").ToString
                    If strAcomodoGeneral = "" Then strAcomodoGeneral = dRowItem("Acomodo").ToString

                    If dRowItem("Acomodo").ToString.Trim <> strAcomodoGeneral.Trim Then
                        blnAgrupacion = False
                    End If
                End If
            Next
            Return blnAgrupacion
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strTituloFechaxAcomodo(ByVal vdttDetalleReservas As Data.DataTable) As String
        Try
            Dim strFechas As String = ""
            Dim strIDDet As String = ""
            For Each dRowItem As Data.DataRow In vdttDetalleReservas.Rows
                If dRowItem("IDDet").ToString <> strIDDet Then
                    strIDDet = dRowItem("IDDet").ToString
                    If InStr(strFechas, "[" & dRowItem("FechaIN").ToString & " - " & dRowItem("FechaOut").ToString & "] - ") = 0 Then
                        strFechas += "[" & dRowItem("FechaIN").ToString & " - " & dRowItem("FechaOut").ToString & "] - "
                    End If
                End If
            Next
            If strFechas <> "" Then
                strFechas = strFechas.Substring(0, strFechas.Length - 2).ToString
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim strFechas As String = "IN/OUT " & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & " : "
            'Dim strIDDet As String = ""
            'For Each dRowItem As Data.DataRow In vdttDetalleReservas.Rows
            '    If dRowItem("IDDet").ToString <> strIDDet Then
            '        strIDDet = dRowItem("IDDet").ToString
            '        If InStr(strFechas, "[" & dRowItem("FechaIN").ToString & " - " & dRowItem("FechaOut").ToString & "] - ") = 0 Then
            '            strFechas += "[" & dRowItem("FechaIN").ToString & " - " & dRowItem("FechaOut").ToString & "] - "
            '        End If
            '    End If
            'Next
            'If strFechas <> "" Then
            '    strFechas = strFechas.Substring(0, strFechas.Length - 2).ToString
            'End If
            Return strFechas.Trim
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function strTituloAcomodosGeneral(ByVal vdttDetalleReservas As Data.DataTable, ByVal vstrColumnName As String) As String
        Try
            Dim strTituloAcomodos As String = ""
            Dim blnDosColumnas As Boolean = If(InStr(vstrColumnName, "|") > 0, True, False)
            For Each dRowItem As Data.DataRow In vdttDetalleReservas.Rows
                If blnDosColumnas Then
                    If InStr(strTituloAcomodos, dRowItem(strDevuelveCadenaNoOcultaIzquierda(vstrColumnName)).ToString & " " & dRowItem(strDevuelveCadenaOcultaDerecha(vstrColumnName)).ToString) = 0 Then
                        strTituloAcomodos += dRowItem(strDevuelveCadenaNoOcultaIzquierda(vstrColumnName)).ToString & " " & dRowItem(strDevuelveCadenaOcultaDerecha(vstrColumnName)).ToString & " + "
                    End If
                Else
                    If InStr(strTituloAcomodos, dRowItem(vstrColumnName).ToString) = 0 Then
                        strTituloAcomodos += dRowItem(vstrColumnName).ToString & " + "
                    End If
                End If

            Next
            If strTituloAcomodos <> "" Then
                strTituloAcomodos = strTituloAcomodos.Substring(0, strTituloAcomodos.Length - 2).ToString
            End If
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim strTituloAcomodos As String = "ACOMODO   : "
            'Dim blnDosColumnas As Boolean = If(InStr(vstrColumnName, "|") > 0, True, False)
            'For Each dRowItem As Data.DataRow In vdttDetalleReservas.Rows
            '    If blnDosColumnas Then
            '        If InStr(strTituloAcomodos, dRowItem(strDevuelveCadenaNoOcultaIzquierda(vstrColumnName)).ToString & " " & dRowItem(strDevuelveCadenaOcultaDerecha(vstrColumnName)).ToString) = 0 Then
            '            strTituloAcomodos += dRowItem(strDevuelveCadenaNoOcultaIzquierda(vstrColumnName)).ToString & " " & dRowItem(strDevuelveCadenaOcultaDerecha(vstrColumnName)).ToString & " + "
            '        End If
            '    Else
            '        If InStr(strTituloAcomodos, dRowItem(vstrColumnName).ToString) = 0 Then
            '            strTituloAcomodos += dRowItem(vstrColumnName).ToString & " + "
            '        End If
            '    End If

            'Next
            'If strTituloAcomodos <> "" Then
            '    strTituloAcomodos = strTituloAcomodos.Substring(0, strTituloAcomodos.Length - 2).ToString
            'End If
            Return strTituloAcomodos.Trim
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblEncabezadoAcomodos_INOUTFECHAS(ByVal vstrFecha As String, ByVal vstrAcomodo As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(3)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            Dim NewFontGray As Font = New Font(bsDefault, 10, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))
            With objTabla
                .SetTotalWidth(New Single() {5.0F, 0.8F, 50.0F})
                .WidthPercentage = 80
                .DefaultCell.BorderWidth = 0

                .DefaultCell.BorderWidthLeft = 0.03F
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0.03F
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("IN/OUT", NewFont))
                .DefaultCell.BorderWidthBottom = 0



                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0.03F
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(":", NewFont))
                .DefaultCell.BorderWidthBottom = 0


                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0.03F
                .DefaultCell.BorderWidthTop = 0.03F
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrFecha, NewFont))
                .DefaultCell.BorderWidthBottom = 0.3F


                .DefaultCell.BorderWidthLeft = 0.03F
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("ACOMODO", NewFont))


                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(":", NewFont))
                .DefaultCell.BorderWidthBottom = 0.3F

                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0.03F
                .DefaultCell.BorderWidthTop = 0
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrAcomodo, NewFont))
                .DefaultCell.BorderWidthBottom = 0.03F

            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblAcomodosPax(ByVal vdttListaAcomodoNormalPax As Data.DataTable, ByVal vdttReservasDet As Data.DataTable, _
                                    ByVal vdttListaAcomodoEspecialPax As Data.DataTable, ByVal vdttReservaDetEspeciales As Data.DataTable) As PdfPTable
        'Try
        '    Dim objTabla As New PdfPTable(1)
        '    Dim strIDDetAcomodoNormal As String = vdttListaAcomodoNormalPax(0)("IDDet").ToString
        '    Dim dttAcomodosspeciales As Data.DataTable = dtFiltrarDataTable(vdttReservasDet, "ExisteAcomodoEspecial='1'")
        '    Dim dttAcomodosNormal As Data.DataTable = dtFiltrarDataTable(vdttReservasDet, "ExisteAcomodoEspecial='0'")
        '    Dim dttListaPaxAcomodoNormal As New Data.DataTable
        '    If strIDDetAcomodoNormal <> "" Then
        '        dttListaPaxAcomodoNormal = dtFiltrarDataTable(vdttListaAcomodoNormalPax, "IDDet='" & strIDDetAcomodoNormal & "'")
        '    Else
        '        dttListaPaxAcomodoNormal = vdttListaAcomodoNormalPax 'dtFiltrarDataTable(vdttListaAcomodoNormalPax, "IDDet='" & strIDDetAcomodoNormal & "'")
        '    End If
        '    Dim blnPintoAcomodoNormal As Boolean = False
        '    Dim blnPintoAcomodoEspecial As Boolean = False
        '    Dim blnAgrupacion As Boolean = False
        '    Dim dcPartitions As New Dictionary(Of String, String)
        '    Dim dtFiltro As New Data.DataTable
        '    Dim strIDDetTmp As String = ""
        '    Dim strFechaTmp As String = ""
        '    Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
        '    Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
        '    Dim NewFontGray As Font = New Font(bsDefault, 10, Font.COURIER, New iTextSharp.text.Color(145, 145, 145))

        '    Dim dttListadoPax As New DataTable
        '    With objTabla
        '        .WidthPercentage = 100
        '        .DefaultCell.Border = 0

        '        Dim strIDDet As String = ""
        '        For Each DRow As Data.DataRow In vdttReservasDet.Rows
        '            If strIDDet <> DRow("IDDet").ToString Then
        '                strIDDet = DRow("IDDet").ToString
        '                Dim blnExisteAcomodoEspecial As Boolean = CBool(DRow("ExisteAcomodoEspecial"))

        '                If Not blnExisteAcomodoEspecial And Not blnPintoAcomodoNormal Then
        '                    If dttAcomodosNormal.Rows.Count > 0 Then
        '                        blnAgrupacion = blnAgrupacionAcomodo(dttAcomodosNormal)
        '                        If blnAgrupacion Then
        '                            Dim strFechaRangos As String = strTituloFechaxAcomodo(dttAcomodosNormal)
        '                            '.DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                            '.AddCell(New Paragraph(strTituloFechaxAcomodo(dttAcomodosNormal), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10.0F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                            strIDDetTmp = dttAcomodosNormal(0)("IDDet").ToString
        '                            dttAcomodosNormal = dtFiltrarDataTable(dttAcomodosNormal, "IDDet='" & strIDDetTmp & "'")

        '                            '.DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                            '.AddCell(New Paragraph(strTituloAcomodosGeneral(dttAcomodosNormal, "TituloAcomodoxCapacidad"), NewFont))  'FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                            .AddCell(tblEncabezadoAcomodos_INOUTFECHAS(strFechaRangos, strTituloAcomodosGeneral(dttAcomodosNormal, "TituloAcomodoxCapacidad")))

        '                            For Each dRowItem As Data.DataRow In dttAcomodosNormal.Rows
        '                                'If CBool(dRowItem("IncGuia")) Then dttListaPaxAcomodoNormal = vdttListaAcomodoNormalPax Else dttListaPaxAcomodoNormal = dtFiltrarDataTable(vdttListaAcomodoNormalPax, "IDDet='" & strIDDetAcomodoNormal & "'")

        '                                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                .AddCell(New Paragraph("Tipo de Habitación : " & dRowItem("Servicio").ToString, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                .AddCell(tblAcomodoCabezeraHabit)

        '                                dtFiltro = dtFiltrarDataTable(dttListaPaxAcomodoNormal, "CapacidadHab='" & dRowItem("CapacidadHab") & "' and EsMatrimonial='" & dRowItem("EsMatrimonial") & "'")
        '                                For Each DRowItemFilter As Data.DataRow In dtFiltro.Rows
        '                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                    .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '                                                                    DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString(), DRowItemFilter("DescTC").ToString()))
        '                                Next
        '                            Next
        '                            blnPintoAcomodoNormal = True
        '                        Else
        '                            Dim blnPintoTitAcomodos As Boolean = False
        '                            For Each DRowItem As DataRow In vdttReservasDet.Rows
        '                                If strIDDetTmp <> DRowItem("IDDet").ToString Then
        '                                    strIDDetTmp = DRowItem("IDDet").ToString
        '                                    strFechaTmp += "[" & DRowItem("FechaIN").ToString & " - " & DRowItem("FechaOut").ToString & "]"
        '                                    If Not blnPintoTitAcomodos Then
        '                                        '.DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                        '.AddCell(New Paragraph(strTituloAcomodosGeneral(dttAcomodosNormal, "TituloAcomodoxCapacidad"), FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                        .AddCell(tblEncabezadoAcomodos_INOUTFECHAS(strFechaTmp, strTituloAcomodosGeneral(dttAcomodosNormal, "TituloAcomodoxCapacidad")))

        '                                        blnPintoTitAcomodos = True
        '                                    End If

        '                                    'Fechas
        '                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                    .AddCell(New Paragraph(strFechaTmp, FontFactory.GetFont(strTipoLetraVoucher, 10.0F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                                    'Acomodos
        '                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                    .AddCell(New Paragraph("Tipo de Habitación : " & DRowItem("TituloAcomodoxCapacidad").ToString, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))


        '                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                    .AddCell(tblAcomodoCabezeraHabit)

        '                                    If dtFiltro.Rows.Count = 0 Then dtFiltro = dtFiltrarDataTable(dttListaPaxAcomodoNormal, "CapacidadHab='" & DRowItem("CapacidadHab") & "' and EsMatrimonial='" & DRowItem("EsMatrimonial") & "'")
        '                                    For Each DRowItemFilter As Data.DataRow In dtFiltro.Rows
        '                                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                        .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '                                                                        DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString(), DRowItemFilter("DescTC").ToString()))
        '                                    Next

        '                                    strFechaTmp = ""
        '                                End If
        '                            Next
        '                            blnPintoAcomodoNormal = True
        '                        End If
        '                    End If
        '                ElseIf blnExisteAcomodoEspecial Then ' And Not blnPintoAcomodoEspecial Then
        '                    strIDDet = ""
        '                    Dim strFecha1 As String = DRow("FechaIn").ToString.Trim
        '                    Dim strFecha2 As String = DRow("FechaOut").ToString.Trim
        '                    Dim dttAcomodosspecialesTmp As Data.DataTable = dtFiltrarDataTable(dttAcomodosspeciales, "FechaIn='" & strFecha1 & "' and FechaOut='" & strFecha2 & "'")

        '                    If dttAcomodosspecialesTmp.Rows.Count > 0 Then
        '                        Dim strIDDetTmp2 As String = ""
        '                        'Dim blnPintoTitAcomodos As Boolean = False
        '                        For Each dRowItemEsp As Data.DataRow In dttAcomodosspecialesTmp.Rows
        '                            If strIDDet <> dRowItemEsp("IDDet").ToString Then
        '                                strIDDet = dRowItemEsp("IDDet").ToString
        '                                strFechaTmp += "[" & dRowItemEsp("FechaIN").ToString & " - " & dRowItemEsp("FechaOut").ToString & "]"

        '                                '.DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                '.AddCell(New Paragraph(strFechaTmp, FontFactory.GetFont(strTipoLetraVoucher, 10.0F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                                Dim strFechaIn As String = dRowItemEsp("FechaIn").ToString
        '                                Dim strFechaOut As String = dRowItemEsp("FechaOut").ToString
        '                                Dim bytDifDays As Byte = DateDiff(DateInterval.Day, CDate(strFechaIn), CDate(strFechaOut)) + 1

        '                                For i As Byte = 0 To bytDifDays
        '                                    Dim strFechaINTmp As String = DateAdd(DateInterval.Day, i, CDate(strFechaIn)).ToShortDateString
        '                                    dtFiltro = dtFiltrarDataTable(vdttReservaDetEspeciales, "FechaIN='" & strFechaINTmp & "'")

        '                                    Dim strTituloAcomodo As String = strTituloAcomodosGeneral(dtFiltro, "AcomodoTitulo")
        '                                    If strTituloAcomodo <> "" Then
        '                                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                        .AddCell(tblEncabezadoAcomodos_INOUTFECHAS(strFechaTmp, strTituloAcomodo))
        '                                    End If

        '                                    For Each dRowItem As Data.DataRow In dtFiltro.Rows
        '                                        Dim dttListaPax As Data.DataTable = dttDatosListaPaxxIDDetAcomodoEspecial(CInt(dRowItem("IDDet")))

        '                                        'Acomodos
        '                                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                        .AddCell(New Paragraph("Tipo de Habitación : " & dRowItem("AcomodoTituloxCapacidad").ToString, NewFontGray)) 'FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '                                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                        .AddCell(tblAcomodoCabezeraHabit)

        '                                        dttListaPax = dtFiltrarDataTable(dttListaPax, "QtCapacidad='" & dRowItem("QtCapacidad").ToString & "' and EsMatrimonial='" & dRowItem("EsMatrimonial").ToString & "'")
        '                                        For Each DRowItemFilter As Data.DataRow In dttListaPax.Rows
        '                                            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '                                            .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '                                                                            DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString(), DRowItemFilter("DescTC").ToString()))
        '                                        Next
        '                                        'Dim n As Integer = 0
        '                                    Next
        '                                Next
        '                            End If
        '                            strFechaTmp = ""
        '                        Next
        '                        blnPintoAcomodoEspecial = True
        '                    End If
        '                End If

        '            End If
        '        Next

        '        ''''
        '        ' ''If dttAcomodosspeciales.Rows.Count > 0 Then
        '        ' ''    ''blnAgrupacion = blnAgrupacionAcomodo(dttAcomodosspeciales)
        '        ' ''    ''If blnAgrupacion Then
        '        ' ''    ''    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '        ' ''    ''    .AddCell(New Paragraph(strTituloFechaxAcomodo(dttAcomodosspeciales), FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
        '        ' ''    ''Else
        '        ' ''    ''End If
        '        ' ''    'Dim strIDDet As String = ""

        '        ' ''    'For Each dRowItem As Data.DataRow In dttAcomodosspeciales.Rows
        '        ' ''    '    If dRowItem("IDDet").ToString.Trim <> strIDDet Then
        '        ' ''    '        strIDDet = dRowItem("IDDet").ToString.Trim

        '        ' ''    '    End If
        '        ' ''    'Next
        '        ' ''End If

        '    End With
        '    ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

        '    ''Dim objTabla As New PdfPTable(1)
        '    ''Dim strIDDetAcomodoNormal As String = vdttListaAcomodoNormalPax(0)("IDDet").ToString
        '    ''Dim dttAcomodosspeciales As Data.DataTable = dtFiltrarDataTable(vdttReservasDet, "ExisteAcomodoEspecial='1'")
        '    ''Dim dttAcomodosNormal As Data.DataTable = dtFiltrarDataTable(vdttReservasDet, "ExisteAcomodoEspecial='0'")
        '    ''Dim dttListaPaxAcomodoNormal As New Data.DataTable
        '    ''If strIDDetAcomodoNormal <> "" Then
        '    ''    dttListaPaxAcomodoNormal = dtFiltrarDataTable(vdttListaAcomodoNormalPax, "IDDet='" & strIDDetAcomodoNormal & "'")
        '    ''Else
        '    ''    dttListaPaxAcomodoNormal = vdttListaAcomodoNormalPax 'dtFiltrarDataTable(vdttListaAcomodoNormalPax, "IDDet='" & strIDDetAcomodoNormal & "'")
        '    ''End If
        '    ''Dim blnPintoAcomodoNormal As Boolean = False
        '    ''Dim blnPintoAcomodoEspecial As Boolean = False
        '    ''Dim blnAgrupacion As Boolean = False
        '    ''Dim dcPartitions As New Dictionary(Of String, String)
        '    ''Dim dtFiltro As New Data.DataTable
        '    ''Dim strIDDetTmp As String = ""
        '    ''Dim strFechaTmp As String = "IN/OUT " & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & " : "

        '    ''Dim dttListadoPax As New DataTable
        '    ''With objTabla
        '    ''    .WidthPercentage = 100
        '    ''    .DefaultCell.Border = 0

        '    ''    Dim strIDDet As String = ""
        '    ''    For Each DRow As Data.DataRow In vdttReservasDet.Rows
        '    ''        If strIDDet <> DRow("IDDet").ToString Then
        '    ''            strIDDet = DRow("IDDet").ToString
        '    ''            Dim blnExisteAcomodoEspecial As Boolean = CBool(DRow("ExisteAcomodoEspecial"))

        '    ''            If Not blnExisteAcomodoEspecial And Not blnPintoAcomodoNormal Then
        '    ''                If dttAcomodosNormal.Rows.Count > 0 Then
        '    ''                    blnAgrupacion = blnAgrupacionAcomodo(dttAcomodosNormal)
        '    ''                    If blnAgrupacion Then
        '    ''                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                        .AddCell(New Paragraph(strTituloFechaxAcomodo(dttAcomodosNormal), FontFactory.GetFont(strTipoLetraVoucher, 10.0F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '    ''                        strIDDetTmp = dttAcomodosNormal(0)("IDDet").ToString
        '    ''                        dttAcomodosNormal = dtFiltrarDataTable(dttAcomodosNormal, "IDDet='" & strIDDetTmp & "'")

        '    ''                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                        .AddCell(New Paragraph(strTituloAcomodosGeneral(dttAcomodosNormal, "TituloAcomodoxCapacidad"), FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
        '    ''                        'If CBool(DRow("IncGuia")) Then dttListaPaxAcomodoNormal = vdttListaAcomodoNormalPax

        '    ''                        For Each dRowItem As Data.DataRow In dttAcomodosNormal.Rows
        '    ''                            'If CBool(dRowItem("IncGuia")) Then dttListaPaxAcomodoNormal = vdttListaAcomodoNormalPax Else dttListaPaxAcomodoNormal = dtFiltrarDataTable(vdttListaAcomodoNormalPax, "IDDet='" & strIDDetAcomodoNormal & "'")

        '    ''                            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                            .AddCell(New Paragraph("Tipo de Habitación : " & dRowItem("Servicio").ToString, FontFactory.GetFont(strTipoLetraVoucher, 8.75F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '    ''                            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                            .AddCell(tblAcomodoCabezeraHabit)

        '    ''                            dtFiltro = dtFiltrarDataTable(dttListaPaxAcomodoNormal, "CapacidadHab='" & dRowItem("CapacidadHab") & "' and EsMatrimonial='" & dRowItem("EsMatrimonial") & "'")
        '    ''                            For Each DRowItemFilter As Data.DataRow In dtFiltro.Rows
        '    ''                                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '    ''                                                                DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString()))
        '    ''                            Next
        '    ''                        Next
        '    ''                        blnPintoAcomodoNormal = True
        '    ''                    Else
        '    ''                        Dim blnPintoTitAcomodos As Boolean = False
        '    ''                        For Each DRowItem As DataRow In vdttReservasDet.Rows
        '    ''                            If strIDDetTmp <> DRowItem("IDDet").ToString Then
        '    ''                                strIDDetTmp = DRowItem("IDDet").ToString
        '    ''                                strFechaTmp += "[" & DRowItem("FechaIN").ToString & " - " & DRowItem("FechaOut").ToString & "]"
        '    ''                                If Not blnPintoTitAcomodos Then
        '    ''                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                    .AddCell(New Paragraph(strTituloAcomodosGeneral(dttAcomodosNormal, "TituloAcomodoxCapacidad"), FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
        '    ''                                    blnPintoTitAcomodos = True
        '    ''                                End If

        '    ''                                'Fechas
        '    ''                                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                .AddCell(New Paragraph(strFechaTmp, FontFactory.GetFont(strTipoLetraVoucher, 10.0F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '    ''                                'Acomodos
        '    ''                                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                .AddCell(New Paragraph("Tipo de Habitación : " & DRowItem("TituloAcomodoxCapacidad").ToString, FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '    ''                                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                .AddCell(tblAcomodoCabezeraHabit)

        '    ''                                If dtFiltro.Rows.Count = 0 Then dtFiltro = dtFiltrarDataTable(dttListaPaxAcomodoNormal, "CapacidadHab='" & DRowItem("CapacidadHab") & "' and EsMatrimonial='" & DRowItem("EsMatrimonial") & "'")
        '    ''                                For Each DRowItemFilter As Data.DataRow In dtFiltro.Rows
        '    ''                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                    .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '    ''                                                                    DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString()))
        '    ''                                Next

        '    ''                                strFechaTmp = "IN/OUT " & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & " : "
        '    ''                            End If
        '    ''                        Next
        '    ''                        blnPintoAcomodoNormal = True
        '    ''                    End If
        '    ''                End If
        '    ''            ElseIf blnExisteAcomodoEspecial Then ' And Not blnPintoAcomodoEspecial Then
        '    ''                strIDDet = ""
        '    ''                Dim strFecha1 As String = DRow("FechaIn").ToString.Trim
        '    ''                Dim strFecha2 As String = DRow("FechaOut").ToString.Trim
        '    ''                Dim dttAcomodosspecialesTmp As Data.DataTable = dtFiltrarDataTable(dttAcomodosspeciales, "FechaIn='" & strFecha1 & "' and FechaOut='" & strFecha2 & "'")

        '    ''                If dttAcomodosspecialesTmp.Rows.Count > 0 Then
        '    ''                    Dim strIDDetTmp2 As String = ""
        '    ''                    'Dim blnPintoTitAcomodos As Boolean = False
        '    ''                    For Each dRowItemEsp As Data.DataRow In dttAcomodosspecialesTmp.Rows
        '    ''                        If strIDDet <> dRowItemEsp("IDDet").ToString Then
        '    ''                            strIDDet = dRowItemEsp("IDDet").ToString
        '    ''                            strFechaTmp += "[" & dRowItemEsp("FechaIN").ToString & " - " & dRowItemEsp("FechaOut").ToString & "]"

        '    ''                            .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                            .AddCell(New Paragraph(strFechaTmp, FontFactory.GetFont(strTipoLetraVoucher, 10.0F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '    ''                            Dim strFechaIn As String = dRowItemEsp("FechaIn").ToString
        '    ''                            Dim strFechaOut As String = dRowItemEsp("FechaOut").ToString
        '    ''                            Dim bytDifDays As Byte = DateDiff(DateInterval.Day, CDate(strFechaIn), CDate(strFechaOut)) + 1

        '    ''                            For i As Byte = 0 To bytDifDays
        '    ''                                Dim strFechaINTmp As String = DateAdd(DateInterval.Day, i, CDate(strFechaIn)).ToShortDateString
        '    ''                                dtFiltro = dtFiltrarDataTable(vdttReservaDetEspeciales, "FechaIN='" & strFechaINTmp & "'")

        '    ''                                Dim strTituloAcomodo As String = strTituloAcomodosGeneral(dtFiltro, "AcomodoTitulo")
        '    ''                                If strTituloAcomodo <> "ACOMODO" Then
        '    ''                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                    .AddCell(New Paragraph(strTituloAcomodo, FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
        '    ''                                    'blnPintoTitAcomodos = True
        '    ''                                End If

        '    ''                                For Each dRowItem As Data.DataRow In dtFiltro.Rows
        '    ''                                    Dim dttListaPax As Data.DataTable = dttDatosListaPaxxIDDetAcomodoEspecial(CInt(dRowItem("IDDet")))

        '    ''                                    'Acomodos
        '    ''                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                    .AddCell(New Paragraph("Tipo de Habitación : " & dRowItem("AcomodoTituloxCapacidad").ToString, FontFactory.GetFont(strTipoLetraVoucher, 9.25F, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

        '    ''                                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                    .AddCell(tblAcomodoCabezeraHabit)

        '    ''                                    dttListaPax = dtFiltrarDataTable(dttListaPax, "QtCapacidad='" & dRowItem("QtCapacidad").ToString & "' and EsMatrimonial='" & dRowItem("EsMatrimonial").ToString & "'")
        '    ''                                    For Each DRowItemFilter As Data.DataRow In dttListaPax.Rows
        '    ''                                        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''                                        .AddCell(tblAcomodoDetalleHabit(DRowItemFilter("IDHabit"), DRowItemFilter("NombrePax"), DRowItemFilter("NumIdentidad"), _
        '    ''                                                                        DRowItemFilter("NacionalidadPax"), DRowItemFilter("FechNacimiento"), DRowItemFilter("ObservEspecial").ToString()))
        '    ''                                    Next

        '    ''                                    Dim n As Integer = 0
        '    ''                                Next
        '    ''                            Next
        '    ''                        End If
        '    ''                        strFechaTmp = "IN/OUT " & vbTab & vbTab & vbTab & vbTab & vbTab & vbTab & " : "
        '    ''                    Next
        '    ''                    blnPintoAcomodoEspecial = True
        '    ''                End If
        '    ''            End If

        '    ''        End If
        '    ''    Next

        '    ''    ' ''If dttAcomodosspeciales.Rows.Count > 0 Then
        '    ''    ' ''    ''blnAgrupacion = blnAgrupacionAcomodo(dttAcomodosspeciales)
        '    ''    ' ''    ''If blnAgrupacion Then
        '    ''    ' ''    ''    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
        '    ''    ' ''    ''    .AddCell(New Paragraph(strTituloFechaxAcomodo(dttAcomodosspeciales), FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
        '    ''    ' ''    ''Else
        '    ''    ' ''    ''End If
        '    ''    ' ''    'Dim strIDDet As String = ""

        '    ''    ' ''    'For Each dRowItem As Data.DataRow In dttAcomodosspeciales.Rows
        '    ''    ' ''    '    If dRowItem("IDDet").ToString.Trim <> strIDDet Then
        '    ''    ' ''    '        strIDDet = dRowItem("IDDet").ToString.Trim

        '    ''    ' ''    '    End If
        '    ''    ' ''    'Next
        '    ''    ' ''End If

        '    ''End With
        '    Return objTabla
        'Catch ex As System.Exception
        '    Throw
        'End Try
    End Function

    Private Function tblOrdenTitulo(ByVal vstrNuOrden As String) As PdfPTable

        Try
            Dim objTablaTit As New PdfPTable(1)

            objTablaTit.DefaultCell.Border = 0
            objTablaTit.SetTotalWidth(New Single() {45})
            objTablaTit.WidthPercentage = 100

            Dim bsFont As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsFont, 16, Font.COURIER, New iTextSharp.text.Color(0, 0, 0))
            Dim NewFontBold As New Font(bsFont, 16, Font.BOLD)

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("ORDEN DE COMPRA : " & vstrNuOrden, NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
           
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function


    Private Function tblOrdenCompra_DatosProv1(ByVal vdttCabeceraOrden As DataTable) As PdfPTable

        Try
            Dim objTablaTit As New PdfPTable(4)

            objTablaTit.DefaultCell.Border = 0
            objTablaTit.SetTotalWidth(New Single() {10, 22, 6, 7})
            objTablaTit.WidthPercentage = 100

            Dim bsFont As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsFont, 10, Font.COURIER, New iTextSharp.text.Color(0, 0, 0))
            Dim NewFontBold As New Font(bsFont, 10, Font.BOLD)

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("RUC :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("NumIdentidad"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Fec. O/C:", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("FeOrdCom"), NewFont))


            '------

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Proveedor :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("NombreCorto"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Fec. Entrega:", NewFontBold))

            Dim strFeEntrega As String = ""
            If IsDBNull(vdttCabeceraOrden.Rows(0).Item("FeEntrega")) = False Then

                strFeEntrega = CDate(vdttCabeceraOrden.Rows(0).Item("FeEntrega")).ToString("dd/MM/yyyy")
            End If


            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(strFeEntrega, NewFont))
            'objTablaTit.AddCell(New Paragraph("", NewFont))


            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Dirección :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Direccion"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            '---

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))


            '-----

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Teléfono :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Telefono1"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            '-----

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Vendedor/Contacto :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Contacto"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenCompra_DatosTotal(ByVal vdttCabeceraOrden As DataTable) As PdfPTable

        Try
            Dim objTablaTit As New PdfPTable(4)

            objTablaTit.DefaultCell.Border = 0
            objTablaTit.SetTotalWidth(New Single() {8, 25, 7, 5})
            objTablaTit.WidthPercentage = 100

            Dim bsFont As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsFont, 10, Font.COURIER, New iTextSharp.text.Color(0, 0, 0))
            Dim NewFontBold As New Font(bsFont, 10, Font.BOLD)

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Moneda :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Moneda"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Total (Sin IGV):", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("SSSubTotal"), NewFont))


            '------

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Condición de Pago :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Forma_Pago"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("IGV 18%:", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("SSIGV"), NewFont))


            '-------------------

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Fecha de Pago :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            Dim strFePago As String = ""
            If IsDBNull(vdttCabeceraOrden.Rows(0).Item("FePago")) = False Then

                strFePago = CDate(vdttCabeceraOrden.Rows(0).Item("FePago")).ToString("dd/MM/yyyy")
            End If


            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(strFePago, NewFont))
            'objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Total (IGV):", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            'objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("SSTotal"), NewFont))
            objTablaTit.AddCell(New Paragraph(CDbl(vdttCabeceraOrden.Rows(0).Item("SSSubTotal")) + CDbl(vdttCabeceraOrden.Rows(0).Item("SSIGV")), NewFont))

            '---

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Titular de la Cuenta:", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Titular_cta"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Detracción " & CInt(vdttCabeceraOrden.Rows(0).Item("Porc_Detraccion")).ToString & "%:", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("SSDetraccion"), NewFont))


            '-----

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Banco :", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Banco"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("Monto a Pagar:", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            objTablaTit.AddCell(New Paragraph(CDbl(vdttCabeceraOrden.Rows(0).Item("SSTotal")), NewFont))
            'objTablaTit.AddCell(New Paragraph(CDbl(vdttCabeceraOrden.Rows(0).Item("SSTotal")) - CDbl(vdttCabeceraOrden.Rows(0).Item("SSDetraccion")), NewFont))

            '-----

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("N° de Cuenta:", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Cuenta"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            ''-----

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("Detracción:", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Banco_Detraccion"), NewFont))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("", NewFont))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("", NewFont))


            ''-----
            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("N° de Cuenta:", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Cuenta_Detraccion"), NewFont))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("", NewFont))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("", NewFont))

            '-----
            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            '-----
            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("CECO:", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vdttCabeceraOrden.Rows(0).Item("Centro_Costos"), NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("", NewFont))

            
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function


    Private Function tblOrdenCompra_DatosNotas(ByVal vdttCabeceraOrden As DataTable) As PdfPTable

        Try
            Dim objTablaTit As New PdfPTable(1)

            objTablaTit.DefaultCell.Border = 0
            objTablaTit.SetTotalWidth(New Single() {33})
            objTablaTit.WidthPercentage = 80

            DefinirBordesTablas(objTablaTit, 0.5, 0.5, 0.5, 0.5)

            Dim bsFont As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsFont, 10, Font.COURIER, New iTextSharp.text.Color(0, 0, 0))


            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("NOTAS: ", NewFont)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("NOTAS: " + vdttCabeceraOrden.Rows(0).Item("TxDescripcion"), NewFont))

          

            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenCompra_Firmas() As PdfPTable

        Try
            Dim objTablaTit As New PdfPTable(3)

            objTablaTit.DefaultCell.Border = 0
            objTablaTit.SetTotalWidth(New Single() {20, 15, 20})
            objTablaTit.WidthPercentage = 90


            DefinirBordesTablas(objTablaTit, 0, 0, 0, 0)

            Dim bsFont As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsFont, 10, Font.COURIER, New iTextSharp.text.Color(0, 0, 0))
            Dim NewFontBold As New Font(bsFont, 10, Font.BOLD)

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("_____________________________", NewFont))
            objTablaTit.AddCell(New Paragraph("", NewFont))
            objTablaTit.AddCell(New Paragraph("_____________________________", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("Milagros Freyre", NewFont))
            objTablaTit.AddCell(New Paragraph("", NewFont))
            objTablaTit.AddCell(New Paragraph("Karina Mosqueira G.", NewFont))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("Jefe de Administración & Finanzas ", NewFontBold))
            objTablaTit.AddCell(New Paragraph("", NewFont))
            objTablaTit.AddCell(New Paragraph("Supervisora de Administración", NewFontBold))

            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblOrdenCompra_Detalle(ByVal vdttDetalleOrden As DataTable) As PdfPTable

        Try
            Dim objTablaTit As New PdfPTable(5)

            objTablaTit.DefaultCell.Border = 0
            objTablaTit.SetTotalWidth(New Single() {6, 20, 6, 6, 7})
            objTablaTit.WidthPercentage = 100

            Dim bsFont As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsFont, 10, Font.COURIER, New iTextSharp.text.Color(0, 0, 0))
            Dim NewFontBold As New Font(bsFont, 10, Font.BOLD)

            DefinirBordesTablas(objTablaTit, 0.5, 0.5, 0.5, 0.5)

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("N° ITEM", NewFontBold)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("DESCRIPCION ITEM", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("CANT.", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("P. UNIT", NewFontBold))

            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
            objTablaTit.AddCell(New Paragraph("PRECIO", NewFontBold))

            For i As Integer = 0 To vdttDetalleOrden.Rows.Count - 1

                objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
                objTablaTit.AddCell(New Paragraph(vdttDetalleOrden.Rows(i).Item("Item"), NewFont))

                objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
                objTablaTit.AddCell(New Paragraph(vdttDetalleOrden.Rows(i).Item("TxServicio"), NewFont))

                objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
                objTablaTit.AddCell(New Paragraph(vdttDetalleOrden.Rows(i).Item("SSCantidad"), NewFont))

                objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
                objTablaTit.AddCell(New Paragraph(vdttDetalleOrden.Rows(i).Item("SSPrecUnit"), NewFont))

                objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_CENTER
                objTablaTit.AddCell(New Paragraph(vdttDetalleOrden.Rows(i).Item("SSTotal"), NewFont))

            Next

       
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblVoucherTitulo(ByVal vstrIDVoucher As String, ByVal vstrIDFile As String) As PdfPTable

        Try
            Dim objTablaTit As New PdfPTable(4)

            objTablaTit.DefaultCell.Border = 0
            objTablaTit.SetTotalWidth(New Single() {10, 6, 22, 7})
            objTablaTit.WidthPercentage = 100

            Dim bsFont As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsFont, 16, Font.COURIER, New iTextSharp.text.Color(0, 0, 0))


            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph("VOUCHER #", NewFont)) 'FontFactory.GetFont("ArialNB", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vstrIDVoucher, NewFont)) 'FontFactory.GetFont("Arial Narrow", 16, Font.BOLD)))
            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            objTablaTit.AddCell(New Paragraph("FILE #", NewFont)) 'FontFactory.GetFont("Arial Narrow", 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
            objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            objTablaTit.AddCell(New Paragraph(vstrIDFile, NewFont)) ''FontFactory.GetFont("Arial Narrow", 16, Font.BOLD)))
            ''''''''''''''''''''''''
            'Dim objTablaTit As New PdfPTable(4) 'Declaración de una tabla con 4 Columnas

            'objTablaTit.DefaultCell.Border = 0

            ''objTablaTit.SetWidthPercentage(New Single() {15.0F, 35.0F, 25.0F, 25.0F}, PageSize.A4) 'Ajusta el tamaño de cada columna
            'objTablaTit.SetTotalWidth(New Single() {10, 6, 22, 7})
            'objTablaTit.WidthPercentage = 100


            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph("VOUCHER #", FontFactory.GetFont(strTipoLetraVoucher, 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph(vstrIDVoucher, FontFactory.GetFont(strTipoLetraVoucher, 16, Font.BOLD)))

            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_RIGHT
            'objTablaTit.AddCell(New Paragraph("FILE #", FontFactory.GetFont(strTipoLetraVoucher, 16, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
            'objTablaTit.DefaultCell.HorizontalAlignment = ALIGN_LEFT
            'objTablaTit.AddCell(New Paragraph(vstrIDFile, FontFactory.GetFont(strTipoLetraVoucher, 16, Font.BOLD)))
            Return objTablaTit
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblVoucherPara(ByVal vstrDescProveedor As String, ByVal vstrDireccion As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            objTabla.SetTotalWidth(New Single() {7.4F, 30})
            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Border = 0
                Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
                Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("PARA", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrDescProveedor, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD)))

                '.DefaultCell.Colspan = 2
                .AddCell(" ")

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrDireccion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            End With
            ''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'objTabla.SetTotalWidth(New Single() {7.4F, 30})


            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("PARA", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrDescProveedor, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD)))

            '    '.DefaultCell.Colspan = 2
            '    .AddCell(" ")


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrDireccion, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function


    Private Function tblMasDatosVoucher(ByVal vstrTitulo As String, ByVal vstrCodReserva As String, _
                                        ByVal vstrPax As String, ByVal vstrNacionalidad As String, _
                                        ByVal vstrResponsable As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(4)
            objTabla.SetTotalWidth(New Single() {9, 16, 10, 10})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Border = 0

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("NOMBRE", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrTitulo, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("COD. RESERVA", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrCodReserva, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("# PAX", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrPax, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("RESPONSABLE", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrResponsable, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("PAIS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrNacionalidad, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(4)
            'objTabla.SetTotalWidth(New Single() {9, 16, 10, 10})


            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("NOMBRE", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrTitulo, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("COD. RESERVA", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrCodReserva, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("# PAX", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrPax, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("RESPONSABLE", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrResponsable, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("PAIS", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrNacionalidad, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblMasDatosVoucherRestaurantes(ByVal vstrTitulo As String, _
                                        ByVal vstrPax As String, ByVal vstrNacionalidad As String, _
                                        ByVal vstrResponsable As String) As PdfPTable

        Try
            Dim objTabla As New PdfPTable(4)
            objTabla.SetTotalWidth(New Single() {9, 16, 10, 10})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Border = 0

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("NOMBRE", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrTitulo, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("# PAX", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrPax, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("RESPONSABLE", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrResponsable, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("PAIS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrNacionalidad, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(4)
            'objTabla.SetTotalWidth(New Single() {9, 16, 10, 10})


            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("NOMBRE", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrTitulo, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("# PAX", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrPax, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("RESPONSABLE", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrResponsable, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("PAIS", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrNacionalidad, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblMasDatosVoucherServicios(ByVal vstrTitulo As String, _
                                    ByVal vstrPax As String, ByVal vstrNacionalidad As String, _
                                    ByVal vstrResponsable As String) As PdfPTable

        Try
            Dim objTabla As New PdfPTable(2)
            objTabla.SetTotalWidth(New Single() {9, 36})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Border = 0

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("NOMBRE", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrTitulo, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))


                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("# PAX", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrPax, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("RESPONSABLE", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrResponsable, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("PAIS", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vstrNacionalidad, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            End With
            '''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(4)
            'objTabla.SetTotalWidth(New Single() {9, 16, 10, 10})


            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Border = 0

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("NOMBRE", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrTitulo, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("# PAX", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrPax, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))


            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("RESPONSABLE", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrResponsable, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("PAIS", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrNacionalidad, FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 10, Font.NORMAL)))

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblBorderSeparacionParrafo(ByVal vblnBordeSuperior As Boolean) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(4)
            objTabla.SetTotalWidth(New Single() {32, 2, 30, 20})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont2 As Font = New Font(bsDefault, 5, Font.COURIER)
            With objTabla
                .WidthPercentage = 100
                If vblnBordeSuperior Then
                    .DefaultCell.BorderWidthTop = 1
                    .DefaultCell.BorderWidthBottom = 0
                    .DefaultCell.BorderWidthLeft = 0
                    .DefaultCell.BorderWidthRight = 0
                Else
                    .DefaultCell.BorderWidthTop = 0
                    .DefaultCell.BorderWidthBottom = 1
                    .DefaultCell.BorderWidthLeft = 0
                    .DefaultCell.BorderWidthRight = 0
                End If

                For i As Byte = 1 To 4
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(" ", NewFont2))
                Next
            End With
            Return objTabla
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function tblServiciosVoucherHotel(ByVal vDtt As Data.DataTable) As PdfPTable
        Try
            'Dim objTabla As New PdfPTable(4)
            'objTabla.SetTotalWidth(New Single() {32, 2, 30, 20})

            'jorge
            Dim objTabla As New PdfPTable(5)
            'objTabla.SetTotalWidth(New Single() {32, 2, 30, 25, 20})
            objTabla.SetTotalWidth(New Single() {32, 4, 30, 25, 20})

            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0
                For Each dr As DataRow In vDtt.Rows
                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(dr("FechaIn").ToString & " / " & dr("FechaOut").ToString, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(dr("Cantidad"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(dr("Servicio"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(dr("CodTarifaDet"), NewFont))

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(New Paragraph(dr("Buffet"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                Next
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(4)
            'objTabla.SetTotalWidth(New Single() {32, 2, 30, 20})

            'With objTabla
            '    .WidthPercentage = 100
            '    Dim byCantidadFilas As Byte = 0
            '    For Each dr As DataRow In vDtt.Rows
            '        If byCantidadFilas = 0 Then
            '            .DefaultCell.BorderWidthTop = 1
            '            .DefaultCell.BorderWidthBottom = 0
            '            .DefaultCell.BorderWidthLeft = 0
            '            .DefaultCell.BorderWidthRight = 0

            '            If byCantidadFilas = vDtt.Rows.Count - 1 Then
            '                .DefaultCell.BorderWidthTop = 1
            '                .DefaultCell.BorderWidthBottom = 1
            '                .DefaultCell.BorderWidthLeft = 0
            '                .DefaultCell.BorderWidthRight = 0
            '            End If
            '        Else
            '            If byCantidadFilas = vDtt.Rows.Count - 1 Then
            '                .DefaultCell.BorderWidthTop = 0
            '                .DefaultCell.BorderWidthBottom = 1
            '                .DefaultCell.BorderWidthLeft = 0
            '                .DefaultCell.BorderWidthRight = 0
            '            Else
            '                .DefaultCell.BorderWidthTop = 0
            '                .DefaultCell.BorderWidthBottom = 0
            '                .DefaultCell.BorderWidthLeft = 0
            '                .DefaultCell.BorderWidthRight = 0
            '            End If
            '        End If

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(dr("FechaIn").ToString & " / " & dr("FechaOut").ToString, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(dr("Cantidad"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(dr("Servicio"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(New Paragraph(dr("Buffet"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '        byCantidadFilas += 1
            '    Next
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblExtrasNoIncluidoRestaurantes(ByVal vstrDetaTipo As String, ByVal vstrObservacion As String, _
                                                     ByVal vblnIncluido As Boolean, ByVal vstrIDProveedor As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {31, 14})
                .WidthPercentage = 100

                Dim NewFont2 As New Font(bsDefault, 12, Font.COURIER)
                .AddCell(New Paragraph("OBSERVACIONES", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph("Extras and expenses not included /" & vbCrLf & "Extras no incluidos", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.Colspan = 2
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                If vstrDetaTipo.Trim <> "" Then
                    .AddCell(New Paragraph(vstrDetaTipo & vbCrLf & vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                Else
                    .AddCell(New Paragraph(vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                End If
                '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))


                '.DefaultCell.Colspan = 2

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(tblDetalleImgServiciosVoucher(vstrIDProveedor))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {31, 14})
            '    .WidthPercentage = 100

            '    .AddCell(New Paragraph("OBSERVACIONES", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph("Extras and expenses not included /" & vbCrLf & "Extras no incluidos", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    .DefaultCell.Colspan = 2
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrObservacion, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))


            '    '.DefaultCell.Colspan = 2

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(tblDetalleImgServiciosVoucher())
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblExtrasNoIncluidoHotel(ByVal vstrDescripcionTipo As String, ByVal vstrObservacion As String, _
                                              ByVal vblnIncluido As Boolean, ByVal vstrIDProveedor As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {31, 14})
                .WidthPercentage = 100
                Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
                Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)

                .AddCell(tblAgregarObservacion2(vstrDescripcionTipo, vstrObservacion))
                .AddCell(tblExportacionServiciosHotel(vblnIncluido))

                .DefaultCell.Colspan = 2

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(tblDetalleImgHotelVoucher(vstrIDProveedor))

            End With
            ''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {31, 14})
            '    .WidthPercentage = 100

            '    .AddCell(tblAgregarObservacion2(vstrObservacion))
            '    .AddCell(tblExportacionServiciosHotel(vblnIncluido))

            '    .DefaultCell.Colspan = 2

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(tblDetalleImgHotelVoucher())

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblAgregarObservacion2(ByVal vstrDescripcionTipo As String, _
                                            ByVal vstrObservacion As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(1)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {31})
                .WidthPercentage = 100
                Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
                Dim NewFont As Font = New Font(bsDefault, 12, Font.COURIER)

                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .AddCell(New Paragraph("OBSERVACIONES", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
                .DefaultCell.VerticalAlignment = 4

                If vstrDescripcionTipo.Trim <> "" Then
                    .AddCell(New Paragraph(vstrDescripcionTipo.Trim & vbCrLf & vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                Else
                    .AddCell(New Paragraph(vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                End If

            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(1)
            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {31})
            '    .WidthPercentage = 100

            '    .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
            '    .AddCell(New Paragraph("OBSERVACIONES", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
            '    .DefaultCell.VerticalAlignment = 4

            '    .AddCell(New Paragraph(vstrObservacion, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            'End With

            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblExportacionServiciosHotel(ByVal vblnIncluido As Boolean) As PdfPTable
        Try
            Dim objTable As New PdfPTable(1)
            With objTable
                .DefaultCell.BorderWidth = 0.5F
                .SetTotalWidth(New Single() {100})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
                Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

                If vblnIncluido Then
                    .DefaultCell.PaddingTop = 7.0F
                    .DefaultCell.PaddingBottom = 8.0F

                    Dim strTexto2 As String = "EXPORTACION DE SERVICIOS" & vbCrLf & "D.L. 919"
                    .AddCell(New Paragraph(strTexto2, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9.0F, Font.BOLD)))
                Else
                    .DefaultCell.BorderWidth = 0
                End If

                .DefaultCell.BorderWidthBottom = 0
                .DefaultCell.BorderWidthLeft = 0
                .DefaultCell.BorderWidthRight = 0
                Dim strTexto As String = "Extras and expenses not incluided /" & vbCrLf & "Extras no incluidos"
                .AddCell(New Paragraph(strTexto, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTable As New PdfPTable(1)
            'With objTable
            '    .DefaultCell.BorderWidth = 1
            '    .SetTotalWidth(New Single() {100})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.VerticalAlignment = ALIGN_MIDDLE

            '    If vblnIncluido Then
            '        .DefaultCell.PaddingTop = 7.0F
            '        .DefaultCell.PaddingBottom = 8.0F

            '        Dim strTexto2 As String = "EXPORTACION DE SERVICIOS" & vbCrLf & "D.L. 919"
            '        .AddCell(New Paragraph(strTexto2, FontFactory.GetFont(strTipoLetraVoucher, 9.0F, Font.BOLD)))
            '    Else
            '        .DefaultCell.BorderWidth = 0
            '    End If

            '    .DefaultCell.BorderWidthBottom = 0
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    Dim strTexto As String = "Extras and expenses not incluided /" & vbCrLf & "Extras no incluidos"
            '    .AddCell(New Paragraph(strTexto, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            'End With
            Return objTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDetalleImgHotelVoucher(ByVal vstrIDProveedor As String) As PdfPTable
        Try
            Dim objTable As New PdfPTable(1)
            With objTable
                .DefaultCell.BorderWidth = 0.5F
                .SetTotalWidth(New Single() {100})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.PaddingTop = 7.0F
                .DefaultCell.PaddingBottom = 8.0F

                Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
                Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)

                'Dim strRazSocialSetours As String = "SETOURS S.A."
                'If vstrUbigeoOficina = gstrBuenosAires Then
                '    strRazSocialSetours = ""
                'ElseIf vstrUbigeoOficina = gstrSantiago Then
                '    strRazSocialSetours = "Setours Chile SPA."
                'End If
                Dim objBN As New clsOrdenPagoBN
                Dim strRazSocialSetours As String = strDevuelveCadenaNoOcultaIzquierda(objBN.strRazSocialUbigeoOficina(vstrIDProveedor))

                Dim objPrv As New clsProveedorBN
                Dim blnProvExterior As Boolean = objPrv.blnProveedorDelExterior(vstrIDProveedor)
                Dim strTexto As String = "Por favor adjuntar este VOUCHER a su factura cuando le envíe a " & UCase(strRazSocialSetours) & "."
                If blnProvExterior Then
                    strTexto = "Por favor adjuntar este Voucher a su Factura o Invoice cuando la envíe al e-mail " & strDevuelveCadenaOcultaDerecha(objBN.strRazSocialUbigeoOficina(vstrIDProveedor)) & " para poder programar su pago."
                End If

                strTexto &= vbCrLf & "Si el pasajero no se presenta es obligación del hotel informar a Setours de inmediato. En caso contrario una eventual factura de No-show no podrá ser aceptada."
                strTexto &= vbCrLf & "El Hotel tiene la obligación de solicitar la Tarjeta Andina de Migraciones al pasajero. En caso contrario el IGV deberá ser cobrado directamente al pasajero."

                .AddCell(New Paragraph(strTexto, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 7.0F, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTable As New PdfPTable(1)
            'With objTable
            '    .DefaultCell.BorderWidth = 1
            '    .SetTotalWidth(New Single() {100})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
            '    .DefaultCell.PaddingTop = 7.0F
            '    .DefaultCell.PaddingBottom = 8.0F

            '    Dim strTexto As String = "Por favor adjuntar este VOUCHER a su factura cuando le envie a SETOURS S.A."
            '    strTexto &= vbCrLf & "Si el pasajero no se presenta es obligación del hotel informar a Setours de inmediato. En caso contrario una eventual factura de No-show no podrá ser aceptada."
            '    strTexto &= vbCrLf & "El Hotel tiene la obligación de solicitar la Tarjeta Andina de Migraciones al pasajero. En caso contrario el IGV deberá ser cobrado directamente al pasajero."

            '    .AddCell(New Paragraph(strTexto, FontFactory.GetFont(strTipoLetraVoucher, 7.0F, Font.NORMAL)))
            'End With
            Return objTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblExtrasNoIncluido(ByVal vstrObservacion As String, ByVal vstrDescripTarifa As String, ByVal vstrIDProveedor As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                .SetTotalWidth(New Single() {31, 14})
                .WidthPercentage = 100

                Dim NewFont2 As New Font(bsDefault, 12, Font.COURIER)
                .AddCell(New Paragraph("OBSERVACIONES", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .AddCell(New Paragraph("Extras and expenses not included /" & vbCrLf & "Extras no incluidos", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

                .DefaultCell.Colspan = 2
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                If vstrDescripTarifa.Trim <> "" Then vstrObservacion = vstrDescripTarifa & vbCrLf & vstrObservacion
                .AddCell(New Paragraph(vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))


                '.DefaultCell.Colspan = 2

                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
                .AddCell(tblDetalleImgServiciosVoucher(vstrIDProveedor))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)

            'With objTabla
            '    .DefaultCell.BorderWidth = 0
            '    .SetTotalWidth(New Single() {31, 14})
            '    .WidthPercentage = 100

            '    .AddCell(New Paragraph("OBSERVACIONES", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.BOLD, New iTextSharp.text.Color(0, 0, 64))))
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .AddCell(New Paragraph("Extras and expenses not included /" & vbCrLf & "Extras no incluidos", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '    .DefaultCell.Colspan = 2
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vstrObservacion, FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    '.AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))


            '    '.DefaultCell.Colspan = 2

            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 12, Font.NORMAL)))
            '    .AddCell(tblDetalleImgServiciosVoucher())
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDetalleImgServiciosVoucher(ByVal vstrIDProveedor As String) As PdfPTable
        Try
            Dim objTable As New PdfPTable(1)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
            With objTable
                .DefaultCell.BorderWidth = 1
                .SetTotalWidth(New Single() {100})
                .WidthPercentage = 100
                .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
                .DefaultCell.PaddingBottom = 4.0F

                Dim objBN As New clsOrdenPagoBN
                Dim strRazSocialSetours As String = strDevuelveCadenaNoOcultaIzquierda(objBN.strRazSocialUbigeoOficina(vstrIDProveedor))

                'Dim strRazSocialSetours As String = vstrUbigeoOficina
                'If vstrUbigeoOficina = gstrBuenosAires Then
                '    strRazSocialSetours = ""
                'ElseIf vstrUbigeoOficina = gstrSantiago Then
                '    strRazSocialSetours = "Setours Chile SPA."
                'End If

                Dim objPrv As New clsProveedorBN
                Dim blnProvExterior As Boolean = objPrv.blnProveedorDelExterior(vstrIDProveedor)
                Dim strTexto As String = "Por favor adjuntar este VOUCHER a su factura cuando le envíe a " & UCase(strRazSocialSetours) & "."
                If blnProvExterior Then
                    strTexto = "Por favor adjuntar este Voucher a su Factura o Invoice cuando la envíe al e-mail " & strDevuelveCadenaOcultaDerecha(objBN.strRazSocialUbigeoOficina(vstrIDProveedor)) & " para poder programar su pago."
                End If

                .AddCell(New Paragraph(strTexto, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTable As New PdfPTable(1)
            'With objTable
            '    .DefaultCell.BorderWidth = 1
            '    .SetTotalWidth(New Single() {100})
            '    .WidthPercentage = 100
            '    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
            '    .DefaultCell.VerticalAlignment = ALIGN_MIDDLE
            '    .DefaultCell.PaddingBottom = 4.0F

            '    .AddCell(New Paragraph("Por favor adjuntar este VOUCHER a su factura cuando le envie a SETOURS S.A.", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            'End With
            Return objTable
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblServiciosVoucherServicios(ByVal vDtt As Data.DataTable) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(1)
            objTabla.SetTotalWidth(New Single() {77})

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0
                For Each dr As DataRow In vDtt.Rows

                    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                    .AddCell(tblDetalleServiciosVoucher(dr))

                    Dim strObservacionInterno As String = If(IsDBNull(dr("ObservInterno")) = True, "", dr("ObservInterno"))
                    .AddCell(tblDetalleServiciosObservacionDetalle(strObservacionInterno))
                Next
            End With
            ''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(1)
            'objTabla.SetTotalWidth(New Single() {77})

            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Padding = 0

            '    Dim byCantidadFilas As Byte = 0
            '    For Each dr As DataRow In vDtt.Rows

            '        If byCantidadFilas = 0 Then .DefaultCell.BorderWidthTop = 1
            '        .DefaultCell.BorderWidthBottom = 0
            '        .DefaultCell.BorderWidthLeft = 0
            '        .DefaultCell.BorderWidthRight = 0
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(tblDetalleServiciosVoucher(dr))

            '        .DefaultCell.BorderWidthTop = 0
            '        .DefaultCell.BorderWidthRight = 0
            '        .DefaultCell.BorderWidthLeft = 0
            '        If byCantidadFilas = vDtt.Rows.Count - 1 Then .DefaultCell.BorderWidthBottom = 1

            '        Dim strObservacionInterno As String = If(IsDBNull(dr("ObservInterno")) = True, "", dr("ObservInterno"))
            '        .AddCell(tblDetalleServiciosObservacionDetalle(strObservacionInterno))

            '        byCantidadFilas += 1
            '    Next
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblServiciosVoucherRestaurantes(ByVal vDtt As Data.DataTable) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(1)
            objTabla.SetTotalWidth(New Single() {77})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.Padding = 0
                .DefaultCell.BorderWidth = 0
                For Each dr As DataRow In vDtt.Rows
                    .AddCell(tblServiciosVoucherRestaurant(dr))

                    Dim strObservacionInterno As String = If(IsDBNull(dr("ObservInterno")) = True, "", dr("ObservInterno"))
                    .AddCell(tblDetalleRestaurantesObservacionDetalle(strObservacionInterno))
                Next

            End With
            ''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(1)
            'objTabla.SetTotalWidth(New Single() {77})

            'With objTabla
            '    .WidthPercentage = 100
            '    .DefaultCell.Padding = 0

            '    Dim byCantidadFilas As Byte = 0
            '    For Each dr As DataRow In vDtt.Rows

            '        If byCantidadFilas = 0 Then .DefaultCell.BorderWidthTop = 1
            '        .DefaultCell.BorderWidthBottom = 0
            '        .DefaultCell.BorderWidthLeft = 0
            '        .DefaultCell.BorderWidthRight = 0
            '        .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '        .AddCell(tblServiciosVoucherRestaurant(dr))

            '        .DefaultCell.BorderWidthTop = 0
            '        .DefaultCell.BorderWidthRight = 0
            '        .DefaultCell.BorderWidthLeft = 0
            '        If byCantidadFilas = vDtt.Rows.Count - 1 Then .DefaultCell.BorderWidthBottom = 1

            '        Dim strObservacionInterno As String = If(IsDBNull(dr("ObservInterno")) = True, "", dr("ObservInterno"))
            '        .AddCell(tblDetalleRestaurantesObservacionDetalle(strObservacionInterno))

            '        byCantidadFilas += 1
            '    Next

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDetalleServiciosObservacionDetalle(ByVal vstrObservacion As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            objTabla.SetTotalWidth(New Single() {7, 70})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            With objTabla
                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0

                .AddCell(New Paragraph("Obs.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .AddCell(New Paragraph(vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'objTabla.SetTotalWidth(New Single() {7, 70})
            'With objTabla
            '    .WidthPercentage = 100
            '    '.DefaultCell.Padding = 1.0F
            '    .DefaultCell.BorderWidthTop = 0
            '    .DefaultCell.BorderWidthBottom = 0
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    .AddCell(New Paragraph("Obs.", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph(vstrObservacion, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDetalleServiciosVoucher(ByVal vDRow As Data.DataRow) As PdfPTable
        Try
            'Dim objTabla As New PdfPTable(5)
            'objTabla.SetTotalWidth(New Single() {7, 5, 50, 5, 10})

            'jorge
            Dim objTabla As New PdfPTable(6)
            objTabla.SetTotalWidth(New Single() {7, 5, 50, 10, 5, 10})

            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            With objTabla

                .WidthPercentage = 100
                .DefaultCell.BorderWidth = 0
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("FechaIn"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("Hora"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("Servicio"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("CodTarifaDet"), NewFont))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("TipoServ"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph("(" & vDRow("IdiomaServicio").ToString.Substring(0, 3) & ") " & vDRow("Pax") & " PAX".ToString, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(5)
            'objTabla.SetTotalWidth(New Single() {7, 5, 50, 5, 10})

            'With objTabla
            '    .DefaultCell.BorderWidthTop = 0
            '    .DefaultCell.BorderWidthBottom = 0
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    .DefaultCell.Padding = 2

            '    .WidthPercentage = 100
            '    '.DefaultCell.BorderWidth = 0
            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("FechaIn"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("Hora"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("Servicio"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("TipoServ"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph("(" & vDRow("IdiomaServicio").ToString.Substring(0, 3) & ") " & vDRow("Pax") & " PAX".ToString, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblServiciosVoucherRestaurant(ByVal vDRow As Data.DataRow) As PdfPTable
        Try
            'Dim objTabla As New PdfPTable(4)
            'objTabla.SetTotalWidth(New Single() {6, 4, 2, 50})

            'jorge
            Dim objTabla As New PdfPTable(5)
            objTabla.SetTotalWidth(New Single() {6, 4, 2, 50, 10})

            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .DefaultCell.BorderWidth = 0
                '.DefaultCell.Padding = 2

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("FechaIn").ToString, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("Hora"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("Cantidad"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("Servicio"), NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                'jorge
                .DefaultCell.HorizontalAlignment = ALIGN_LEFT
                .AddCell(New Paragraph(vDRow("CodTarifaDet"), NewFont))
            End With
            ''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(4)
            'objTabla.SetTotalWidth(New Single() {6, 4, 2, 50})

            'With objTabla
            '    .DefaultCell.BorderWidthTop = 0
            '    .DefaultCell.BorderWidthBottom = 0
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    .DefaultCell.Padding = 2

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("FechaIn").ToString, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("Hora"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("Cantidad"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '    .DefaultCell.HorizontalAlignment = ALIGN_LEFT
            '    .AddCell(New Paragraph(vDRow("Servicio"), FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Private Function tblDetalleRestaurantesObservacionDetalle(ByVal vstrObservacion As String) As PdfPTable
        Try
            Dim objTabla As New PdfPTable(2)
            objTabla.SetTotalWidth(New Single() {7.5F, 70})
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)
            With objTabla
                .WidthPercentage = 100
                '.DefaultCell.Padding = 1.0F
                '.DefaultCell.BorderWidthTop = 0
                '.DefaultCell.BorderWidthBottom = 0
                '.DefaultCell.BorderWidthLeft = 0
                '.DefaultCell.BorderWidthRight = 0
                '.DefaultCell.PaddingBottom = 4
                .DefaultCell.BorderWidth = 0
                .AddCell(New Paragraph("Obs.", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                .AddCell(New Paragraph(vstrObservacion, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            End With
            '''''''''''''''''''''''''''''''''''''''''''''''''''''''
            'Dim objTabla As New PdfPTable(2)
            'objTabla.SetTotalWidth(New Single() {7.5F, 70})
            'With objTabla
            '    .WidthPercentage = 100
            '    '.DefaultCell.Padding = 1.0F
            '    .DefaultCell.BorderWidthTop = 0
            '    .DefaultCell.BorderWidthBottom = 0
            '    .DefaultCell.BorderWidthLeft = 0
            '    .DefaultCell.BorderWidthRight = 0
            '    .DefaultCell.PaddingBottom = 4
            '    .AddCell(New Paragraph("Obs.", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '    .AddCell(New Paragraph(vstrObservacion, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            'End With
            Return objTabla
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function bytsConvertirArchivoToBytes(ByVal vstrRutaImagen As String) As Byte()
        Try
            Dim byts As Byte() = Nothing
            If IO.File.Exists(vstrRutaImagen) Then
                byts = IO.File.ReadAllBytes(vstrRutaImagen)
            End If
            Return byts
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function ImgConvertirBytesToImagen(ByVal byteArrayIn As Byte()) As Drawing.Image
        Try
            Dim ms As New IO.MemoryStream(byteArrayIn)
            Dim returnImage As Drawing.Image = Drawing.Image.FromStream(ms)
            Return returnImage
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Sub pPosicionarFormsCascada(ByVal vstrFormName As String)
        Try
            Dim intCant As Byte = My.Application.OpenForms.Count
            Dim x As Integer = 0 : Dim y As Integer = 0 : Dim space As Byte = 20
            Dim intInd As Byte = 0 : Dim cntFormularios As Byte = 0
            For i = 0 To intCant - 1
                If My.Application.OpenForms.Item(i).Name = vstrFormName Then
                    cntFormularios += 1
                End If
            Next

            'If cntFormularios = 1 Then My.Application.OpenForms.Item(cntForms).Location = New Point(0, 0)
            Dim bytFormRef As Byte = 0
            For i = 0 To intCant - 1
                If My.Application.OpenForms.Item(i).Name = vstrFormName Then
                    intInd += 1
                    x = My.Application.OpenForms.Item(i).Location.X
                    y = My.Application.OpenForms.Item(i).Location.Y
                    If cntFormularios = 1 Then
                        My.Application.OpenForms.Item(i).Location = New Point(0, 0)
                    Else
                        If intInd = 1 Then
                            My.Application.OpenForms.Item(i).Location = New Point(0, 0)
                        Else
                            intInd -= 1
                            My.Application.OpenForms.Item(i).Location = New Point(x + (intInd * space), y + (intInd * space))
                            intInd += 1
                        End If
                    End If
                End If
            Next
        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Function strDevuelveColumnaExcel(ByVal vintColumna As Int16) As String
        Try
            Dim dc As New Dictionary(Of Int16, String)
            Dim intInd As Int16 = 1
            For i As Byte = 65 To 90
                dc.Add(intInd, Convert.ToChar(i).ToString)
                intInd += 1
            Next

            If vintColumna > dc.Count Then
                Return dc.Item(vintColumna \ dc.Count) & dc.Item(vintColumna - (dc.Count * (vintColumna \ dc.Count)))
            Else
                Return dc.Item(vintColumna)
            End If
        Catch ex As System.Exception
            Throw
        End Try
    End Function
    Public Function dblObtenerTotalxColumna(ByVal vDgv As DataGridView, ByVal vstrColumnName As String) As Double
        If vDgv.Rows.Count = 0 Then Return 0
        Try
            Dim dblReturn As Double = 0
            For Each dgvItem As DataGridViewRow In vDgv.Rows
                Dim dblPositivo As Double = CDbl(dgvItem.Cells(vstrColumnName).Value)
                'If dblPositivo < 0 Then dblPositivo = 0 'dblPositivo = dblPositivo * -1
                dblReturn += dblPositivo
            Next
            Return CDbl(Format(dblReturn, "###,###0.00"))
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function dblObtenerTotalxColumna(ByVal vdat As Data.DataTable, ByVal vstrColumna As String) As Double
        If vdat.Rows.Count = 0 Then Return 0
        Try
            Dim dblReturnSuma As Double = 0
            For Each ItemRow As DataRow In vdat.Rows
                dblReturnSuma += CDbl(ItemRow(vstrColumna))
            Next
            Return dblReturnSuma
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function dblNumeroConDecimalesExactos(ByVal vstrNumeroCompleto As String, ByVal bytNumDecimales As Byte) As Double
        Try
            Dim strNumReturn As String = ""
            Dim bytNumDec As Byte = 0
            Dim blnInicioDecimales As Boolean = False
            Dim strNumeros() As Char
            strNumeros = vstrNumeroCompleto.ToCharArray

            For iL As Byte = 0 To strNumeros.Length - 1
                If IsNumeric(strNumeros(iL)) Then
                    If Not blnInicioDecimales Then
                        strNumReturn += strNumeros(iL)
                    Else
                        strNumReturn += strNumeros(iL)
                        bytNumDec += 1
                        If bytNumDec = bytNumDecimales Then Exit For
                    End If
                Else
                    If strNumeros(iL) = "." Then
                        strNumReturn += strNumeros(iL)
                        blnInicioDecimales = True
                    End If
                End If
            Next
            Return Convert.ToDouble(strNumReturn)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Sub GenerarExcelDatosPax_PeruRail(ByVal vDtDatosExcel As Data.DataTable)
        Dim CurrentCiI As Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture
        System.Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo(gstrUserCultureInfo)
        Try
            Dim xlApp As Object
            Dim xlWorkBook As Object
            Dim xlWorkSheet As Object

            'CurrentCiI.DateTimeFormat.FullDateTimePattern = "dd/MM/yyyy"
            xlApp = CreateObject("Excel.Application")
            xlWorkBook = xlApp.Workbooks.Add
            xlWorkSheet = xlWorkBook.Sheets(1)

            xlApp.Visible = True

            Dim arrTitulos() As String = {"ITEM RESERVA", "PRIMER NOMBRE", "PRIMER APELLIDO", "TIPO DOC", _
                                          "NRO DOC", "NACIONALIDAD", "FECHA NAC."}

            'xlWorkSheet.Columns(1).Width = 80
            'xlWorkSheet.Range("A1", "AZ1").Width = 80.0F

            'Cabecera 
            For iCol As Byte = 0 To arrTitulos.Length - 1
                xlWorkSheet.Cells(1, iCol + 1) = arrTitulos(iCol)
                xlWorkSheet.Cells(1, iCol + 1).HorizontalAlignment = 3
            Next

            Dim intFilaExcel As Byte = 2
            For d As Byte = 0 To vDtDatosExcel.Rows.Count - 1
                For c As Byte = 0 To arrTitulos.Length - 1
                    Dim strDatoCelda As String = vDtDatosExcel(d)(c).ToString()
                    'Definiendo Formatos
                    If c = 0 Then
                        xlWorkSheet.Cells(intFilaExcel, c + 1).NumberFormat = "#,##0"
                    ElseIf c >= 1 And c <= 3 Then
                        xlWorkSheet.Cells(intFilaExcel, c + 1).NumberFormat = "General"
                    ElseIf c >= 4 And c <= 5 Then
                        If IsNumeric(strDatoCelda) Then strDatoCelda = strDatoCelda & "DatoMigEx"
                        xlWorkSheet.Cells(intFilaExcel, c + 1).NumberFormat = "General"
                    Else
                        xlWorkSheet.Cells(intFilaExcel, c + 1).NumberFormat = "@"
                    End If
                    xlWorkSheet.Cells(intFilaExcel, c + 1) = strDatoCelda 'vDtDatosExcel(d)(c).ToString()
                    xlWorkSheet.Cells(intFilaExcel, c + 1).HorizontalAlignment = 3
                Next
                intFilaExcel += 1
            Next

            intFilaExcel = 2
            For d As Byte = 0 To vDtDatosExcel.Rows.Count - 1
                xlWorkSheet.Cells(intFilaExcel, 5) = Replace(xlWorkSheet.Cells(intFilaExcel, 5).Text, "DatoMigEx", "")
                xlWorkSheet.Cells(intFilaExcel, 7).NumberFormat = "dd/MM/yyyy"
                intFilaExcel += 1
            Next
            'xlWorkSheet.Cells(2, 5) = Replace(xlWorkSheet.Cells(2, 5).Text, "DatoMigEx", "")

            xlWorkSheet.Columns.AutoFit()

            System.Threading.Thread.CurrentThread.CurrentCulture = CurrentCiI
            Try
                xlApp.Visible = True
            Catch ex As System.Exception
                Exit Sub
            End Try
        Catch ex As System.Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = CurrentCiI
            Throw
        End Try
    End Sub

    Public Sub GenerarArchivoExcelReporteCotizacion(ByVal vDt As DataTable, ByVal vLstColumnasNoVisible As List(Of String), _
                                                    ByVal vblnContieneTit As Boolean, ByVal vstrTipoDetalleReportes As String, _
                                                    Optional ByVal vstrNomArch As String = "", Optional ByVal vblnAbrirArchivo As Boolean = True, Optional vstrVersion As String = "")

        Dim CurrentCiI As Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture
        System.Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo(gstrUserCultureInfo)
        Try
            Dim xlApp As Object
            Dim xlWorkBook As Object
            Dim xlWorkSheet As Object

            Dim TmpDgv As New DataGridView
            TmpDgv.AllowUserToAddRows = False

            'xlApp = CreateObject("Excel.Application.12")
            xlApp = CreateObject("Excel.Application" + vstrVersion)
            'xlApp = CreateObject("Excel.Application.11")

            xlWorkBook = xlApp.Workbooks.Add
            xlWorkSheet = xlWorkBook.Sheets(1)

            Dim tmpVdt As New DataTable
            tmpVdt = vDt.Copy

            If Not vLstColumnasNoVisible Is Nothing Then
                If vstrTipoDetalleReportes.Trim = "" Then tmpVdt = dtFiltrarDataTable(tmpVdt, "NroDebitMemoOrd=0")
                'Eliminando las columnas que no quiero mostrar en el excel
                For Each strColRem As String In vLstColumnasNoVisible
                    For Each ColumnDgv As DataColumn In tmpVdt.Columns
                        If ColumnDgv.ColumnName.Trim.ToUpper = strColRem.Trim.ToUpper Then
                            tmpVdt.Columns.Remove(ColumnDgv)
                            Exit For
                        End If
                    Next
                Next
            End If
            'Migrando mis columnas al tmp
            Dim strNombreColumna As String = ""
            For Each ColumnDgv As DataColumn In tmpVdt.Columns
                strNombreColumna = Replace(ColumnDgv.ColumnName, " ", "_")
                If strNombreColumna = "CoInternacPais" Then
                    TmpDgv.Columns.Add(strNombreColumna, "Residencia")
                Else
                    TmpDgv.Columns.Add(strNombreColumna, ColumnDgv.ColumnName)
                End If
            Next


            'Insertando los datos

            Dim bytCol As Byte = 0
            For Each Dr As DataRow In tmpVdt.Rows
                TmpDgv.Rows.Add()
                For Each Col As DataGridViewColumn In TmpDgv.Columns
                    'MessageBox.Show(Dr(bytCol))
                    TmpDgv.Item(bytCol, TmpDgv.Rows.Count - 1).Value = Dr(bytCol)
                    bytCol += 1
                Next
                bytCol = 0
            Next

            xlApp.Visible = vblnAbrirArchivo

            'Cabecera
            If Not vblnContieneTit Then
                For i As Byte = 0 To TmpDgv.Columns.Count - 1
                    xlWorkSheet.Cells(1, i + 1) = TmpDgv.Columns(i).HeaderText
                Next
            End If


            'Detalle
            For i = 0 To TmpDgv.Rows.Count - 1
                For j = 0 To TmpDgv.Columns.Count - 1
                    If xlWorkSheet.Cells(1, 1).ToString <> "" Then



                        xlWorkSheet.Cells(i + 2, j + 1) = If(TmpDgv(j, i).Value Is Nothing, "", TmpDgv(j, i).Value.ToString())

                        Dim strNumero As String = If(TmpDgv(j, i).Value Is Nothing, "", TmpDgv(j, i).Value.ToString()).ToString() 'xlWorkSheet.Cells(i + 2, j + 1).Text
                        'MessageBox.Show(strNumero)
                        'If strNumero = "5.04" Then
                        '    MessageBox.Show(strNumero)
                        'End If

                        'xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "General"
                        If blnExisteColumna(tmpVdt, "FilTitulo") Then
                            If IsNumeric(strNumero) And (TmpDgv.Item("FilTitulo", i).Value.ToString.Trim = "False" Or _
                                                         TmpDgv.Item("FilTitulo", i).Value.ToString.Trim = "R" Or _
                                                         TmpDgv.Item("FilTitulo", i).Value.ToString.Trim = "D") Then
                                If blnExisteColumna(tmpVdt, "Anio") Then
                                    If TmpDgv.Columns(j).Name <> "Anio" And TmpDgv.Columns(j).Name <> "Var" Then xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                Else
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                End If
                            End If
                            If TmpDgv.Columns(j).Name = "FilTitulo" Then xlWorkSheet.Cells(i + 2, j + 1).ColumnWidth = 0
                        Else
                            If IsNumeric(strNumero) And vstrTipoDetalleReportes.Trim = "CotSpx" Then
                                xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                            ElseIf IsNumeric(strNumero) And vstrTipoDetalleReportes.Trim = "PDB" Then
                                If TmpDgv.Columns(j).Name = "SerieDoc" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "00#"
                                ElseIf TmpDgv.Columns(j).Name = "TotalxPax" Then
                                    'xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "00#"
                                    Dim dblExacto As Double = dblNumeroConDecimalesExactos(strNumero, 2)
                                    xlWorkSheet.Cells(i + 2, j + 1) = dblExacto
                                    'xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                ElseIf TmpDgv.Columns(j).Name = "CoOperacionBan" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "@"
                                    xlWorkSheet.Cells(i + 2, j + 1) = strNumero
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "General"
                                End If

                                xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = ""
                            ElseIf IsNumeric(strNumero) And vstrTipoDetalleReportes.Trim = "ReporteFacturacionContabilidad" Then
                                'Formato para ReporteFacturacionContabilidad
                                If TmpDgv.Columns(j).Name = "SubDiario" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "0#"
                                ElseIf TmpDgv.Columns(j).Name = "NroRegistro" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "000#"
                                ElseIf TmpDgv.Columns(j).Name = "IDTipoDoc" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "0#"
                                ElseIf TmpDgv.Columns(j).Name = "SerieDoc" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "00#"
                                ElseIf TmpDgv.Columns(j).Name = "NumeroDoc" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "00000#"
                                ElseIf TmpDgv.Columns(j).Name = "TipoCliente" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "0#"

                                ElseIf TmpDgv.Columns(j).Name = "ValorFacturadoExport" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                ElseIf TmpDgv.Columns(j).Name = "BaseImponible" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                ElseIf TmpDgv.Columns(j).Name = "Exonerada" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                ElseIf TmpDgv.Columns(j).Name = "Inafecta" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                ElseIf TmpDgv.Columns(j).Name = "IGV" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                ElseIf TmpDgv.Columns(j).Name = "ImporteTotal" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                ElseIf TmpDgv.Columns(j).Name = "TipoCambio" Then
                                    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "#,##0.00"
                                End If


                            End If


                            Dim strValor As String = If(TmpDgv(j, i).Value Is Nothing, "", TmpDgv(j, i).Value.ToString())
                            If IsDate(strValor) And InStr(strValor, "/") > 0 Then
                                xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "dd/mm/yyyy"
                                'Else
                                '    xlWorkSheet.Cells(i + 2, j + 1).NumberFormat = "General"
                            End If
                        End If
                    End If
                Next
            Next

            If vblnContieneTit Then xlWorkSheet.Range("A1", "AZ1").Delete()
            If vstrTipoDetalleReportes = "PDB" Then xlWorkSheet.Range("B1", "B1000").HorizontalAlignment = 2 'Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignLeft()

            'Dim strTest As String = strDevuelveColumnaExcel(29)
            'Armando el Detalle.
            Select Case vstrTipoDetalleReportes.Trim
                Case "CotCli"
                    'xlWorkSheet.Range("K:K").Clear()

                    Dim dblTotal As Double = 0
                    Dim dblSSTotal As Double = 0
                    Dim dblSTTotal As Double = 0

                    'Union de Cols
                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 3), xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 6)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 3), xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 6)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 3), xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 6)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 3) = "Total per passenger"
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 3) = "Single Supplement"
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 3) = "Triple Discount"

                    'Col1
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 7) = dblDevuelveTotalxColumna(vDt, "TotalCotIdet")
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 7) = dblDevuelveTotalxColumna(vDt, "SSTotal")
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 7) = dblDevuelveTotalxColumna(vDt, "STTotal")

                    'Las demas Columnas Dinamicas
                    For i As Byte = 1 To 4
                        Dim dblTotalColumna As Double = dblDevuelveTotalxColumna(vDt, "C" & i.ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 7 + i) = If(dblTotalColumna = 0, "", dblTotalColumna.ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 7 + i) = If(dblTotalColumna = 0, "", dblDevuelveTotalxColumna(vDt, "SSTotalC" & i.ToString).ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 7 + i) = If(dblTotalColumna = 0, "", dblDevuelveTotalxColumna(vDt, "STTotalC" & i.ToString).ToString)

                        If xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 7 + i).Text = "" Then xlWorkSheet.Range(strDevuelveColumnaExcel(7 + i) & ":" & strDevuelveColumnaExcel(7 + i)).Clear()
                    Next

                    'Asignado Formato Numerico
                    For i As Byte = 6 To 10
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), i).NumberFormat = "#,##0.00"
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), i).NumberFormat = "#,##0.00"
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), i).NumberFormat = "#,##0.00"
                    Next

                Case "CotSpx"

                    Dim dblTotal As Double = dblDevuelveTotalxColumna(vDt, "Total")

                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 2), xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 3)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 2) = "Total"
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 4) = dblTotal
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 4).NumberFormat = "#,##0.00"

                Case "CotCos"
                    xlWorkSheet.Range("A1:H1").Clear()

                    'Tabla #1
                    Dim dblTotalGnCosto As Double = dblDevuelveTotalxColumna(vDt, "CostoTotalCal")
                    Dim dblTotalGnVenta As Double = dblDevuelveTotalxColumna(vDt, "CostoTotalDet")
                    Dim dblSSCR As Double = dblDevuelveTotalxColumna(vDt, "SSCR")
                    Dim dblSSTotal As Double = dblDevuelveTotalxColumna(vDt, "SSTotal")
                    Dim dblSTCR As Double = dblDevuelveTotalxColumna(vDt, "STCR")
                    Dim dblSTTotal As Double = dblDevuelveTotalxColumna(vDt, "STTotal")

                    'Union de Cols
                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 7), xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 10)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 7), xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 10)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 7), xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 10)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Range(xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), 7), xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), 10)).Merge() '"C" & (vDt.Rows.Count + 2).ToString, "C" & (vDt.Rows.Count + 2).ToString)
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 7) = "Total general por pax"
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), 7) = "Margen (%)"
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 7) = "Single Supplement"
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), 7) = "Triple Discount"

                    'Col1
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 11) = dblTotalGnCosto
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 12) = dblTotalGnVenta
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 11) = dblSSCR
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), 12) = dblSSTotal
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), 11) = dblSTCR
                    xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), 12) = dblSTTotal


                    'Las demas Columnas Dinamicas
                    Dim bytColumn As Byte = 13
                    Dim bytColumn2 As Byte = 14
                    For i As Byte = 1 To 4
                        Dim dblTotalCosto As Double = dblDevuelveTotalxColumna(vDt, "CostoTotalCalC" & i.ToString)
                        Dim dblTotalVenta As Double = dblDevuelveTotalxColumna(vDt, "CostoTotalDetC" & i.ToString)
                        Dim dblSSCRTmp As Double = dblDevuelveTotalxColumna(vDt, "SSCRC" & i.ToString)
                        Dim dblSSTotalTmp As Double = dblDevuelveTotalxColumna(vDt, "SSTotalC" & i.ToString)
                        Dim dblSTCRTmp As Double = dblDevuelveTotalxColumna(vDt, "STCRC" & i.ToString)
                        Dim dblSTTotalTmp As Double = dblDevuelveTotalxColumna(vDt, "STTotalC" & i.ToString)

                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), bytColumn) = If(dblTotalCosto = 0 And dblTotalVenta = 0, "", dblTotalCosto.ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), bytColumn2) = If(dblTotalCosto = 0 And dblTotalVenta = 0, "", dblTotalVenta.ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), bytColumn) = If(dblTotalCosto = 0 And dblTotalVenta = 0, "", dblSSCRTmp.ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), bytColumn2) = If(dblTotalCosto = 0 And dblTotalVenta = 0, "", dblSSTotalTmp.ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), bytColumn) = If(dblTotalCosto = 0 And dblTotalVenta = 0, "", dblSTCRTmp.ToString)
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), bytColumn2) = If(dblTotalCosto = 0 And dblTotalVenta = 0, "", dblSTTotalTmp.ToString)
                        'If xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), 6 + i).Text = "" Then xlWorkSheet.Range(strDevuelveColumnaExcel(6 + i) & ":" & strDevuelveColumnaExcel(6 + i)).Clear()
                        bytColumn += 2
                        bytColumn2 += 2
                    Next

                    'Dandole El formato a la primera tabla
                    For i As Byte = 10 To 20
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 2), i).NumberFormat = "#,##0.00"
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 3), i).NumberFormat = "#,##0.00"
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 4), i).NumberFormat = "#,##0.00"
                        xlWorkSheet.Cells((tmpVdt.Rows.Count + 5), i).NumberFormat = "#,##0.00"
                    Next
            End Select

            xlWorkSheet.Columns.AutoFit()
            System.Threading.Thread.CurrentThread.CurrentCulture = CurrentCiI

            'MessageBox.Show("Se ha generado el archivo exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            tmpVdt.Dispose()
            TmpDgv.Dispose()

            Dim FileFormatNum As Long
            If Val(xlApp.Version) < 12 Then
                FileFormatNum = -4143
            Else
                FileFormatNum = 56
            End If

            If vstrNomArch <> "" Then
                xlWorkBook.SaveAs(vstrNomArch, FileFormatNum)
                xlWorkBook.Close()

            End If

            Try
                xlApp.Visible = vblnAbrirArchivo
                'xlApp = Nothing
            Catch ex As System.Exception
                Exit Sub
            End Try

        Catch ex As System.Exception
            System.Threading.Thread.CurrentThread.CurrentCulture = CurrentCiI
            Throw
        End Try
    End Sub

    Public Function strFormatearDocumProvee(ByVal vstrNuDocum As String)
        If vstrNuDocum.Length = 0 Then
            Return ""
        End If

        Dim bytCantCar As Byte = 3
Inicio:
        Dim strNuSerie As String = vstrNuDocum.Substring(0, bytCantCar)

        If Convert.ToInt16(strNuSerie) = 0 Then
            bytCantCar += 1
            GoTo Inicio
        Else
            Return vstrNuDocum.Insert(bytCantCar, "-")
        End If

    End Function

    Public Sub GenerarArchivoExcel(ByVal vDgv As DataGridView, Optional ByVal vDt As DataTable = Nothing)
        Try
            'Dim xlApp As Excel.Application
            Dim xlApp As Object
            'Dim xlWorkBook As Excel.Workbook
            Dim xlWorkBook As Object
            'Dim xlWorkSheet As Excel.Worksheet
            Dim xlWorkSheet As Object

            'Dim misValue As Object = System.Reflection.Missing.Value
            Dim TmpDgv As New DataGridView
            TmpDgv.AllowUserToAddRows = False

            'xlApp = New Excel.Application
            xlApp = CreateObject("Excel.Application")


            Dim CurrentCiI As Globalization.CultureInfo = Threading.Thread.CurrentThread.CurrentCulture
            'System.Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo("de-DE")
            System.Threading.Thread.CurrentThread.CurrentCulture = New Globalization.CultureInfo(gstrUserCultureInfo)


            xlWorkBook = xlApp.Workbooks.Add
            'xlWorkSheet = xlWorkBook.Sheets("Hoja1")
            xlWorkSheet = xlWorkBook.Sheets(1)

            'TmpDgv = vDgv



            If vDt Is Nothing Then
                For Each Col As DataGridViewColumn In vDgv.Columns
                    If Col.Visible Then
                        If InStr(Col.Name, "btn") = 0 Then
                            TmpDgv.Columns.Add(Col.Name, Col.HeaderText)
                        End If
                    End If
                Next

                Dim bytCol As Byte = 0
                For Each Dr As DataGridViewRow In vDgv.Rows
                    TmpDgv.Rows.Add()
                    For Each Col As DataGridViewColumn In vDgv.Columns
                        If Col.Visible Then
                            If InStr(Col.Name, "btn") = 0 Then
                                TmpDgv.Item(bytCol, TmpDgv.Rows.Count - 1).Value = Dr.Cells(Col.Index).Value
                                bytCol += 1
                            End If
                        End If
                    Next
                    bytCol = 0
                Next
            Else
                Dim strNombreColumna As String = ""
                For Each ColumnDgv As DataColumn In vDt.Columns
                    strNombreColumna = Replace(ColumnDgv.ColumnName, " ", "_")
                    TmpDgv.Columns.Add(strNombreColumna, ColumnDgv.ColumnName)
                Next

                'TmpDgv.DataSource = vDt
                Dim bytCol As Byte = 0
                For Each Dr As DataRow In vDt.Rows
                    TmpDgv.Rows.Add()
                    For Each Col As DataGridViewColumn In TmpDgv.Columns
                        TmpDgv.Item(bytCol, TmpDgv.Rows.Count - 1).Value = Dr(bytCol)
                        bytCol += 1
                    Next
                    bytCol = 0
                Next

            End If

            xlApp.Visible = True

            'Cabecera
            For i As Byte = 0 To TmpDgv.Columns.Count - 1
                xlWorkSheet.Cells(1, i + 1) = TmpDgv.Columns(i).HeaderText
            Next

            'Detalle
            For i = 0 To TmpDgv.Rows.Count - 1
                For j = 0 To TmpDgv.Columns.Count - 1
                    If xlWorkSheet.Cells(i + 1, j + 1).ToString <> "" Then
                        'If TmpDgv(j, i).Value Is Nothing Then
                        'End If
                        xlWorkSheet.Cells(i + 2, j + 1) = If(TmpDgv(j, i).Value Is Nothing, "", TmpDgv(j, i).Value.ToString())
                    End If
                Next
            Next

            'Estilos
            'Dim chartRange As Excel.Range
            Dim chartRange As Object
            chartRange = xlWorkSheet.Range("A1", "K1")
            chartRange.Font.Bold = True

            'chartRange = xlWorkSheet.Range("A1", "A1")
            System.Threading.Thread.CurrentThread.CurrentCulture = CurrentCiI
            'chartRange.EntireColumn(1, 1) = ALIGN_JUSTIFIED_ALL

            'Dim strRutaArchivo As String = gstrRutaImagenes & "TestExcelContactos_de_Cliente.xlsx"

            'If IO.File.Exists(strRutaArchivo) Then
            '    IO.File.Delete(strRutaArchivo)
            'End If

            'MessageBox.Show("Se ha generado el archivo exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            'xlWorkBook.Activate()

            'Return strRutaArchivo
        Catch ex As System.Exception
            Throw
        End Try
    End Sub



    Public Function blnExisteColumna(ByVal vdt As DataTable, ByVal vstrColumn As String) As Boolean
        Dim blnEncontroColumna As Boolean = False
        For Each dc As DataColumn In vdt.Columns
            If dc.ColumnName.ToUpper = vstrColumn.ToUpper Then
                blnEncontroColumna = True
                Exit For
            End If
        Next
        Return blnEncontroColumna
    End Function

    Public Function dblDevuelveTotalxColumna(ByVal vdt As DataTable, ByVal vstrColumn As String) As Double
        Try
            Dim dblRetorno As Double = 0


            If blnExisteColumna(vdt, vstrColumn) Then
                If blnExisteColumna(vdt, "FilTitulo") Then
                    For Each Dr As DataRow In vdt.Rows
                        If Dr("Filtitulo").ToString.Trim <> "True" And Dr("Filtitulo").ToString.Trim <> "T" And _
                           Dr("Filtitulo").ToString.Trim <> "S" Then
                            Dim strValorNum As String = If(IsDBNull(Dr(vstrColumn)) = True, "", Dr(vstrColumn))
                            If IsNumeric(strValorNum) Then
                                dblRetorno += CDbl(strValorNum)
                            End If
                        End If
                    Next
                Else
                    For Each Dr As DataRow In vdt.Rows
                        Dim strValorNum As String = If(IsDBNull(Dr(vstrColumn)) = True, "", Dr(vstrColumn))
                        If IsNumeric(strValorNum) Then
                            dblRetorno += CDbl(strValorNum)
                        End If
                    Next
                End If
            End If

            Return dblRetorno
        Catch ex As System.Exception
            Throw
        End Try
    End Function
    Public Sub GenerarArchivoTexto(ByVal vDtt As DataTable, ByVal chrSep As Char, _
                                   ByVal vstrRuta As String, _
                                   ByVal vListaColFechasYYYMMDD As List(Of String), _
                                   ByVal vListaColNoTomar As List(Of String), _
                                   ByVal vListaColumnasNumero As List(Of stColumnaNumeroFormato), _
                                   ByVal vListaColFechasDDMMYYYY As List(Of String))

        Dim sw As StreamWriter = Nothing
        Try

            sw = New StreamWriter(vstrRuta)

            For Each dr As DataRow In vDtt.Rows
                Dim strLinea As String = ""
                For bytCol As Byte = 0 To vDtt.Columns.Count - 1

                    Dim blnTomar As Boolean = True
                    If Not vListaColNoTomar Is Nothing Then
                        For Each strCampoNoTomar As String In vListaColNoTomar
                            If strCampoNoTomar = vDtt.Columns(bytCol).ColumnName Then
                                blnTomar = False
                                Exit For
                            End If
                        Next
                    End If
                    If Not blnTomar Then GoTo SgteCol

                    Dim blnFormatoFechaYYYYMMDD As Boolean = False
                    If Not vListaColFechasYYYMMDD Is Nothing Then
                        For Each strCampoFecha As String In vListaColFechasYYYMMDD
                            If strCampoFecha = vDtt.Columns(bytCol).ColumnName Then
                                blnFormatoFechaYYYYMMDD = True
                                Exit For
                            End If
                        Next
                    End If

                    Dim blnColNumerica As Boolean = False
                    Dim strFormatoNumerico As String = ""
                    If Not vListaColumnasNumero Is Nothing Then
                        For Each ItemCol As stColumnaNumeroFormato In vListaColumnasNumero
                            If ItemCol.strColumna = vDtt.Columns(bytCol).ColumnName Then
                                blnColNumerica = True
                                strFormatoNumerico = ItemCol.strFormato
                                Exit For
                            End If
                        Next
                    End If

                    Dim blnFormatoFechaDDMMYYYY As Boolean = False
                    If Not vListaColFechasDDMMYYYY Is Nothing Then
                        For Each strCampoFecha As String In vListaColFechasDDMMYYYY
                            If strCampoFecha = vDtt.Columns(bytCol).ColumnName Then
                                blnFormatoFechaDDMMYYYY = True
                                Exit For
                            End If
                        Next
                    End If

                    If Not IsDBNull(dr(bytCol)) Then
                        If Not blnFormatoFechaYYYYMMDD Then
                            If Not blnFormatoFechaDDMMYYYY Then
                                If Not blnColNumerica Then
                                    strLinea += dr(bytCol) & chrSep
                                Else
                                    strLinea += dblNumeroConDecimalesExactos(dr(bytCol).ToString(), 2) & chrSep
                                    'strLinea += Format(dr(bytCol), strFormatoNumerico) & chrSep
                                End If

                            Else
                                strLinea += strFechaTextoDDMMYYYY(dr(bytCol)) & chrSep
                            End If
                        Else
                            strLinea += strFechaTextoAAAAMMDD(dr(bytCol)) & chrSep
                        End If

                    Else
                        strLinea += "" & chrSep
                    End If
SgteCol:
                Next
                sw.WriteLine(strLinea)
            Next

            sw.Close()

            MessageBox.Show("El archivo plano se generó exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)


        Catch ex As System.Exception
            sw.Close()
            Throw
        End Try
    End Sub
    Public Function strFechaTextoAAAAMMDD(ByVal vdatFecha As Date) As String
        'Return Year(vdatFecha).ToString & Format(Month(vdatFecha), "00").ToString & Format(DatePart(DateInterval.Day, vdatFecha), "00")
        If vdatFecha.Year <> 1 And vdatFecha.Year <> 1900 Then
            Return vdatFecha.Year.ToString & Format(vdatFecha.Month, "00").ToString & Format(vdatFecha.Day, "00")
        Else
            Return ""
        End If

    End Function
    Public Function strFechaTextoDDMMYYYY(ByVal vdatFecha As Date) As String
        'Return Year(vdatFecha).ToString & Format(Month(vdatFecha), "00").ToString & Format(DatePart(DateInterval.Day, vdatFecha), "00")
        If vdatFecha.Year <> 1 And vdatFecha.Year <> 1900 Then
            Return Format(vdatFecha.Day, "00") & "/" & Format(vdatFecha.Month, "00").ToString & "/" & vdatFecha.Year.ToString
        Else
            Return ""
        End If

    End Function
    Public Function strCampoCorreoTexto(ByVal vbytTamanioColumna As Byte, ByVal vstrValor As String) As String
        Try
            Dim strValorRet As String = ""

            If vbytTamanioColumna <= strValorRet.Trim.Length Then
                strValorRet = vstrValor.Substring(0, vbytTamanioColumna)
            Else
                strValorRet = vstrValor.Trim & Space(vbytTamanioColumna - vstrValor.Trim.Length)
            End If

            Return strValorRet
        Catch ex As System.Exception
            Throw
        End Try

    End Function

    Public Function strRepetirCampoLine(ByVal vstrTexto As String, ByVal vintRepeticiones As Int16) As String
        Return StrDup(vintRepeticiones, vstrTexto)
    End Function

    Public Function blnFilaActualEsPadre(ByVal vDgv As DataGridView, _
                                          ByVal vstrIDDet As String, _
                                          ByVal vstrIDDetRel As String) As Boolean

        Dim blnEsPadre As Boolean = False

        With vDgv
            If .CurrentRow Is Nothing Then Exit Function

            Dim bytFilaDgv As Byte = .CurrentRow.Index
            If IsNumeric(vstrIDDet) Then

                For Each FilaDgv As DataGridViewRow In .Rows
                    If FilaDgv.Cells("IDDetRel").Value.ToString = vstrIDDet Then
                        blnEsPadre = True
                        Return blnEsPadre
                    End If
                Next
            Else
                If vstrIDDetRel.Length >= 1 Then
                    If vstrIDDetRel.Substring(0, 1) = "P" Then
                        'blnEsPadre = True
                        'Return blnEsPadre
                        Dim strNroDerechaPadre As String = Mid(vstrIDDetRel, 2)
                        For Each FilaDgv As DataGridViewRow In .Rows
                            If FilaDgv.Cells("IDDetRel").Value.ToString.Length > 1 Then
                                If FilaDgv.Cells("IDDetRel").Value.ToString.Substring(0, 1) = "H" Then
                                    Dim strNroDerechaHijo As String = Mid(FilaDgv.Cells("IDDetRel").Value.ToString, 2)
                                    If strNroDerechaPadre = strNroDerechaHijo Then
                                        blnEsPadre = True
                                        Exit For
                                    End If
                                End If
                            End If
                        Next

                    End If
                End If
            End If
        End With

        Return blnEsPadre
    End Function


    Public Function blnFilaActualEsHijo(ByVal vDgv As DataGridView, _
                                      ByVal vstrIDDet As String, _
                                      ByVal vstrIDDetRel As String) As Boolean

        Dim blnEsHijo As Boolean = False

        With vDgv
            If .CurrentRow Is Nothing Then Exit Function

            Dim bytFilaDgv As Byte = .CurrentRow.Index
            If IsNumeric(vstrIDDetRel) Then

                For Each FilaDgv As DataGridViewRow In .Rows
                    If FilaDgv.Cells("IDDet").Value.ToString = vstrIDDetRel Then
                        blnEsHijo = True
                        Return blnEsHijo
                    End If
                Next
            Else
                If vstrIDDetRel.Length >= 1 Then
                    If vstrIDDetRel.Substring(0, 1) = "H" Then
                        'blnEsHijo = True
                        'Return blnEsHijo
                        Dim strNroDerechaPadre As String = Mid(vstrIDDetRel, 2)
                        For Each FilaDgv As DataGridViewRow In .Rows
                            If FilaDgv.Cells("IDDetRel").Value.ToString.Length > 1 Then
                                If FilaDgv.Cells("IDDetRel").Value.ToString.Substring(0, 1) = "P" Then
                                    Dim strNroDerechaHijo As String = Mid(FilaDgv.Cells("IDDetRel").Value.ToString, 2)
                                    If strNroDerechaPadre = strNroDerechaHijo Then
                                        blnEsHijo = True
                                        Exit For
                                    End If
                                End If
                            End If
                        Next

                    End If
                End If
            End If
        End With

        Return blnEsHijo
    End Function

    Public Sub ColocarPosicionDgv(ByVal vDgv As DataGridView, ByVal vstrColumn As String, _
                                                 ByVal vstrValue As String)
        Try
            Dim strColumn As String = ""

            'Buscando una columnVisible
            For Each ColumnDgv As DataGridViewColumn In vDgv.Columns
                If ColumnDgv.Visible = True And InStr(ColumnDgv.GetType.ToString, "DataGridViewButtonColumn") = 0 And _
                InStr(ColumnDgv.GetType.ToString, "DataGridViewCheckBoxColumn") = 0 And InStr(ColumnDgv.GetType.ToString, "DataGridViewImageColumn") = 0 And _
                InStr(ColumnDgv.GetType.ToString, "DataGridViewComboBoxColumn") = 0 Then
                    strColumn = ColumnDgv.Name
                    Exit For
                End If
            Next

            For Each ItemDgv As DataGridViewRow In vDgv.Rows
                If ItemDgv.Cells(vstrColumn).Value.ToString = vstrValue Then
                    vDgv.CurrentCell = vDgv.Item(strColumn, ItemDgv.Index)

                    Exit For
                End If
            Next
        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Sub OrdenarDgvxVariasColumnas(ByVal vDgv As DataGridView, _
                                         ByVal vstrFilterColumns As String, _
                                         Optional ByVal vstrCampoOrden As String = "", _
                                         Optional ByVal chrTipoDato As Char = "")
        Try
            If vDgv.Rows.Count <= 1 Then Exit Sub
            If vDgv.Rows.Count <> 0 Then
                vDgv.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)

                Dim DttOrdenado As New DataTable("tmp")
                For Each dtColumn As DataGridViewColumn In vDgv.Columns
                    If vstrCampoOrden <> "" Then
                        If dtColumn.Name = vstrCampoOrden Then
                            If chrTipoDato = "C" Then
                                DttOrdenado.Columns.Add(dtColumn.Name, GetType(String))
                            ElseIf chrTipoDato = "D" Then
                                DttOrdenado.Columns.Add(dtColumn.Name, GetType(Date))
                            ElseIf chrTipoDato = "I" Then
                                DttOrdenado.Columns.Add(dtColumn.Name, GetType(Integer))
                            ElseIf chrTipoDato = "N" Then
                                DttOrdenado.Columns.Add(dtColumn.Name, GetType(Single))
                            End If
                        Else
                            If dtColumn.Name = "Dia" Then
                                DttOrdenado.Columns.Add(dtColumn.Name, GetType(Date))
                            ElseIf dtColumn.Name = "Item" Then
                                DttOrdenado.Columns.Add(dtColumn.Name, GetType(Single))
                            Else
                                DttOrdenado.Columns.Add(dtColumn.Name)
                            End If

                        End If
                    Else
                        If dtColumn.Name = "Dia" Then
                            DttOrdenado.Columns.Add(dtColumn.Name, GetType(Date))
                        ElseIf dtColumn.Name = "Item" Then
                            DttOrdenado.Columns.Add(dtColumn.Name, GetType(Single))
                        ElseIf dtColumn.Name = "OrdIncluido" Or dtColumn.Name = "NroOrdenNoIn" Or dtColumn.Name = "NroOrdenObs" Then
                            DttOrdenado.Columns.Add(dtColumn.Name, GetType(Single))
                        Else
                            DttOrdenado.Columns.Add(dtColumn.Name)
                        End If
                    End If
                Next

                Dim bytNroColumnasBtn As Byte = bytRetornarNroBotonesDgv(vDgv)
                Dim bytNroImagenesBtn As Byte = 0 'bytRetornarNroImagenesDgv(vDgv)
                ExportarDataGridViewADataTable(vDgv, DttOrdenado, bytNroColumnasBtn + bytNroImagenesBtn)

                Dim DtView As New DataView(DttOrdenado)
                DtView.Sort = vstrFilterColumns
                DttOrdenado = DtView.ToTable()

                Dim intColumnasReales As Byte
                intColumnasReales = DttOrdenado.Columns.Count - (bytNroColumnasBtn + bytNroImagenesBtn)

                vDgv.Rows.Clear()

                For Each drDet As DataRow In DttOrdenado.Rows
                    vDgv.Rows.Add()
                    For i As Byte = 0 To intColumnasReales - 1
                        vDgv.Item(i, vDgv.Rows.Count - 1).Value = drDet(i)
                    Next
                Next
            End If
        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Function bytRetornarNroBotonesDgv(ByVal vDgv As DataGridView) As Byte
        Try
            Dim bytNroReturn As Byte = 0

            For Each dgvCol As DataGridViewColumn In vDgv.Columns
                If TypeOf (dgvCol) Is DataGridViewButtonColumn Then
                    bytNroReturn += 1
                End If
            Next

            Return bytNroReturn
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function bytRetornarNroImagenesDgv(ByVal vDgv As DataGridView) As Byte
        Try
            Dim bytNroReturn As Byte = 0

            For Each dgvCol As DataGridViewColumn In vDgv.Columns
                If TypeOf (dgvCol) Is DataGridViewImageColumn Then
                    bytNroReturn += 1
                End If
            Next

            Return bytNroReturn
        Catch ex As System.Exception
            Throw
        End Try
    End Function
    Public Sub ExportarDataGridViewADataTable(ByVal miDataGrid As DataGridView, ByRef Tabla As DataTable, _
                                                      ByVal vbytNroColumnasBoton As Byte)
        Try
            Dim filaNueva As Data.DataRow
            Dim numCols As Integer
            numCols = miDataGrid.ColumnCount - vbytNroColumnasBoton

            For Each filaDatos As DataGridViewRow In miDataGrid.Rows
                filaNueva = Tabla.NewRow()
                For i As Integer = 0 To numCols - 1
                    filaNueva(i) = filaDatos.Cells(i).Value
                Next
                Tabla.Rows.Add(filaNueva)
            Next

        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Sub AlinearFilaGrid(ByRef rDgv As DataGridView, ByVal vstrNombreID As String, _
                               ByVal vstrID As String, ByVal vbytColEnfoque As Byte)
        Try
            For Each DgvFila As DataGridViewRow In rDgv.Rows
                If DgvFila.Cells(vstrNombreID).Value.ToString = vstrID Then
                    rDgv.CurrentCell = DgvFila.Cells(vbytColEnfoque)
                    Exit For
                End If
            Next

        Catch ex As System.Exception
            Throw
        End Try
    End Sub
    Public Sub AlinearFilaGridNroFila(ByRef rDgv As DataGridView, ByVal vintNroFila As Int16, ByVal vbytColEnfoque As Byte)
        Try
            If vintNroFila < 0 Then Exit Sub
            rDgv.CurrentCell = rDgv.Item(vbytColEnfoque, vintNroFila)
        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Sub CargarblnConsulta(ByVal rBtnAceptar As Button, ByRef rGrb As GroupBox, ByVal BlnConsulta As Boolean)
        Try
            rBtnAceptar.Enabled = Not BlnConsulta
            pEnableControlsGroupBox(rGrb, Not BlnConsulta)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Public Function strDevueleCodigoHTMLxLetra(ByVal vstrTexto As String) As String
        Dim strResultado As String = vstrTexto
        strResultado = Replace(strResultado, "á", "&aacute;")
        strResultado = Replace(strResultado, "é", "&eacute;")
        strResultado = Replace(strResultado, "í", "&iacute;")
        strResultado = Replace(strResultado, "ó", "&oacute;")
        strResultado = Replace(strResultado, "ú", "&uacute;")
        strResultado = Replace(strResultado, "ñ", "&ntilde;")

        strResultado = Replace(strResultado, "Á", "&Aacute;")
        strResultado = Replace(strResultado, "É", "&Eacute;")
        strResultado = Replace(strResultado, "Í", "&Iacute;")
        strResultado = Replace(strResultado, "Ó", "&Oacute;")
        strResultado = Replace(strResultado, "Ú", "&Uacute;")
        strResultado = Replace(strResultado, "Ñ", "&Ntilde;")

        strResultado = Replace(strResultado, "Ö", "&Ouml;")
        strResultado = Replace(strResultado, "Ü", "&Uuml;")

        vstrTexto = strResultado
        Return strResultado
    End Function

    Public Function strDevuelteLetraxCodigoHTML(ByVal vstrTexto As String) As String
        Dim strResultado As String = vstrTexto

        strResultado = Replace(strResultado, "&aacute;", "á")
        strResultado = Replace(strResultado, "&eacute;", "é")
        strResultado = Replace(strResultado, "&iacute;", "í")
        strResultado = Replace(strResultado, "&oacute;", "ó")
        strResultado = Replace(strResultado, "&uacute;", "ú")
        strResultado = Replace(strResultado, "&ntilde;", "ñ")

        strResultado = Replace(strResultado, "&Aacute;", "Á")
        strResultado = Replace(strResultado, "&Eacute;", "É")
        strResultado = Replace(strResultado, "&Iacute;", "Í")
        strResultado = Replace(strResultado, "&Oacute;", "Ó")
        strResultado = Replace(strResultado, "&Uacute;", "Ú")
        strResultado = Replace(strResultado, "&Ntilde;", "Ñ")

        strResultado = Replace(strResultado, "&Ouml;", "Ö")
        vstrTexto = strResultado
        Return strResultado
    End Function

    Public Sub DTPickerValueCheck(ByRef rDTPicher As DateTimePicker, ByVal vblnLoad As Boolean, _
                                  Optional ByVal vdatFecha As Date = #1/1/1950#)
        Try
            If vblnLoad Then
                If Not rDTPicher.Checked Then
                    rDTPicher.Checked = False
                Else
                    If rDTPicher.Text = "01/01/1900" Then
                        'rDTPicher.Value = #1/1/1950#
                        rDTPicher.Value = vdatFecha
                    End If
                End If
            Else
                If rDTPicher.Text = "01/01/1900" Then
                    rDTPicher.Checked = False
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Public Function dtDTPickerValue(ByVal vDTPicker As DateTimePicker) As Date
        If Not vDTPicker.Checked Then
            Return #1/1/1900#
        End If
        Return vDTPicker.Value
    End Function
    Public Function dtDTPickerText(ByVal vDTPicker As DateTimePicker, ByVal vblnEnBlanco As Boolean) As String
        If Not vDTPicker.Checked Then
            If vblnEnBlanco Then
                Return ""
            Else
                Return "01/01/1900"
            End If
        End If

        Return vDTPicker.Text
    End Function

    Public Sub ConvertirFilasValorxColumna(ByRef vDgv As DataGridView, ByVal vstrNameColumn As String, ByVal vstrDataType As String)
        Try
            For Each DgvRow As DataGridViewRow In vDgv.Rows
                Select Case vstrDataType
                    Case "System.Single"
                        DgvRow.Cells(vstrNameColumn).Value = CSng(If(DgvRow.Cells(vstrNameColumn).Value Is Nothing, 0, If(DgvRow.Cells(vstrNameColumn).Value.ToString = "", 0, DgvRow.Cells(vstrNameColumn).Value)))
                    Case "System.DateTime"
                        DgvRow.Cells(vstrNameColumn).Value = CDate(DgvRow.Cells(vstrNameColumn).Value)
                    Case Else
                        DgvRow.Cells(vstrNameColumn).Value = CStr(DgvRow.Cells(vstrNameColumn).Value)
                End Select
            Next
        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Function objLlenaGridaObjeto(ByVal vDgv As DataGridView, ByVal intDgvFila As Int16, ByVal vObjBE As Object) As Object


        Try
            Dim objBE As Object = vObjBE
            For Each Prop As Object In vObjBE.GetType.GetProperties
                'If Prop.GetType.ToString = "System.Reflection.RuntimePropertyInfo" Then
                'System.Reflection.runtime()
                'MessageBox.Show(Prop.GetType.ToString & " ----- " & Prop.name.ToString)
                'MessageBox.Show(Prop.name.ToString)
                For Each dgvCol As DataGridViewColumn In vDgv.Columns
                    If Prop.name.ToString.ToUpper = dgvCol.Name.ToUpper Then
                        'MessageBox.Show(Prop.name.ToString)
                        'MessageBox.Show(objBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString)
                        'MessageBox.Show(vDgv.Item(dgvCol.Index, intDgvFila).Value)
                        If Not vDgv.Item(dgvCol.Index, intDgvFila).Value Is Nothing Then
                            Dim newValueProp As PropertyInfo = objBE.GetType.GetProperty(Prop.name.ToString)
                            If objBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString = "System.DateTime" Then
                                newValueProp.SetValue(objBE, Convert.ToDateTime(vDgv.Item(dgvCol.Index, intDgvFila).Value), Nothing)
                            ElseIf objBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString = "System.Char" Then
                                If vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString <> "" Then
                                    newValueProp.SetValue(objBE, Convert.ToChar(vDgv.Item(dgvCol.Index, intDgvFila).Value), Nothing)
                                End If
                            ElseIf objBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString = "System.Int16" Then
                                newValueProp.SetValue(objBE, Convert.ToInt16(If(vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString = "", 0, vDgv.Item(dgvCol.Index, intDgvFila).Value)), Nothing)
                            ElseIf objBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString = "System.Int32" Then
                                newValueProp.SetValue(objBE, Convert.ToInt32(If(vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString = "", 0, vDgv.Item(dgvCol.Index, intDgvFila).Value)), Nothing)
                            ElseIf objBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString = "System.Boolean" Then
                                newValueProp.SetValue(objBE, Convert.ToBoolean(If(vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString = "", True, vDgv.Item(dgvCol.Index, intDgvFila).Value)), Nothing)
                            ElseIf objBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString = "System.Single" Then
                                newValueProp.SetValue(objBE, Convert.ToSingle(If(vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString = "", 0, vDgv.Item(dgvCol.Index, intDgvFila).Value)), Nothing)
                            Else
                                'objBE.GetType.GetProperty(Prop.name.ToString).SetValue(newProp, vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString, Nothing)
                                'MessageBox.Show(Prop.name.ToString & "--" & vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString)
                                newValueProp.SetValue(objBE, vDgv.Item(dgvCol.Index, intDgvFila).Value.ToString, Nothing)

                            End If
                        End If
                        Exit For
                    End If
                Next

                'End If
            Next
            Return objBE
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Sub dgvLlenaObjetoaGrid(ByVal vObjBE As Object, _
                                   ByRef rDgv As DataGridView, _
                                   ByVal vintDgvFila As Int16)

        Try
            Dim Dgv As New DataGridView
            For Each dgvCol As DataGridViewColumn In rDgv.Columns
                Dgv.Columns.Add(dgvCol.Name, dgvCol.Name)
            Next

            For Each Prop As Object In vObjBE.GetType.GetProperties
                'If Prop.GetType.ToString = "System.Reflection.RuntimePropertyInfo" Then
                'System.Reflection.runtime()
                'MessageBox.Show(Prop.GetType.ToString & " ----- " & Prop.name.ToString)
                Dgv.Rows.Add()

                For Each dgvCol As DataGridViewColumn In rDgv.Columns

                    If Prop.name.ToString.ToUpper = dgvCol.Name.ToUpper Then
                        Dim newValueProp As PropertyInfo = vObjBE.GetType.GetProperty(Prop.name.ToString)

                        Dgv.Item(dgvCol.Index, 0).Value = newValueProp.GetValue(vObjBE, Nothing)
                        If vObjBE.GetType.GetProperty(Prop.name.ToString).PropertyType.ToString = "System.DateTime" Then
                            Dgv.Item(dgvCol.Index, 0).Value = Format(Dgv.Item(dgvCol.Index, 0).Value, "dd/MM/yyyy")
                        End If

                        Exit For
                    End If
                Next

                'End If
            Next

            Dim intDgvFila As Int16 = vintDgvFila
            If vintDgvFila = -1 Then
                rDgv.Rows.Add()
                intDgvFila = rDgv.Rows.Count - 1
            End If
            For Each dgvColOri As DataGridViewColumn In Dgv.Columns
                For Each dgvColDes As DataGridViewColumn In rDgv.Columns
                    If dgvColOri.Name = dgvColDes.Name Then
                        'MessageBox.Show(dgvColDes.Name.ToString & "--" & Dgv.Item(dgvColOri.Index, 0).Value.ToString)

                        rDgv.Item(dgvColDes.Index, intDgvFila).Value = Dgv.Item(dgvColOri.Index, 0).Value
                        Exit For
                    End If
                Next
            Next

        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Function lstCadenasSeparadasxCaracter(ByVal vstrCadena As String, ByVal vchrCaract As Char) As List(Of String)
        Try
            Dim strCadena As String = ""
            Dim ListCadenas As New List(Of String)
            For bytInx As Int16 = 0 To vstrCadena.Length - 1
                Dim chrCar As Char = vstrCadena.Substring(bytInx, 1)
                If chrCar <> vchrCaract Then
                    strCadena &= chrCar
                Else
                    ListCadenas.Add(strCadena)
                    strCadena = ""
                End If
            Next
            Return ListCadenas
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function strCaracteresMinuscRTFaHTML(ByVal vstrTexto As String) As String
        Dim strResultado As String = vstrTexto
        strResultado = Replace(strResultado, "Ã¡", "&aacute;") 'á
        strResultado = Replace(strResultado, "Ã©", "&eacute;") 'é
        strResultado = Replace(strResultado, "Ã³", "&oacute;") 'ó
        strResultado = Replace(strResultado, "Ãº", "&uacute;") 'ú
        strResultado = Replace(strResultado, "Ã±", "&ntilde;") 'ñ
        strResultado = Replace(strResultado, "Ã", "&iacute;") 'í

        Return strResultado
    End Function
    'Public Function strCaracteresMayuscRTFaHTML(ByVal vstrTexto As String) As String
    '    Dim strResultado As String = vstrTexto
    '    strResultado = Replace(strResultado, "Ã", "&Aacute;") 'á
    '    strResultado = Replace(strResultado, "Ã‰", "&Eacute;") 'é
    '    strResultado = Replace(strResultado, "Ã" & Asc(34), "&Oacute;") 'ó
    '    strResultado = Replace(strResultado, "Ãš", "&Uacute;") 'ú
    '    strResultado = Replace(strResultado, "Ã‘", "&Ntilde;") 'ñ
    '    strResultado = Replace(strResultado, "Ã", "&Iacute;") 'í

    '    Return strResultado
    'End Function

    Public Sub PintarChecksPadres(ByVal treeNode As TreeNode, ByVal blnChecked As Boolean)
        Try
            Dim bytContNodes As Byte = 0
            If treeNode.Parent Is Nothing Then Exit Sub
            For Each Nodo As TreeNode In treeNode.Parent.Nodes
                If Nodo.Checked Then
                    bytContNodes += 1
                    Exit For
                End If
            Next

            If bytContNodes > 0 Then
                treeNode.Parent.Checked = True
            Else
                treeNode.Parent.Checked = False
            End If
        Catch ex As System.Exception
            Throw
        End Try
    End Sub

    Public Sub CheckAllChildNodes(ByVal treeNode As TreeNode, ByVal nodeChecked As Boolean)
        Dim node As TreeNode
        For Each node In treeNode.Nodes
            node.Checked = nodeChecked
            If node.Nodes.Count > 0 Then
                CheckAllChildNodes(node, nodeChecked)
            End If
        Next node
    End Sub

    Public Function blnNoExisteNodosMarcados(ByVal vstrIDNodo As String, ByVal tvw As TreeView) As Boolean
        Try
            Dim cnt As Byte = 0
            For i As Integer = 0 To tvw.Nodes.Count - 1
                For t As Integer = 0 To tvw.Nodes(i).Nodes.Count - 1
                    If tvw.Nodes(i).Nodes(t).Checked And tvw.Nodes(i).Nodes(t).Name = vstrIDNodo Then
                        cnt += 1
                    End If
                Next
            Next

            If cnt > 1 Then
                Return False
            Else
                Return True
            End If
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveTextoSaltosLineaMinimo(ByVal strTexto As String)
        Dim strReturn As String = ""
        Try
            Dim Lineas() As String
            Lineas = Split(strTexto, vbCrLf)
            For i As Int16 = 0 To Lineas.Length - 1
                If Lineas(i).Trim <> "" Then
                    strReturn += Lineas(i).ToString & vbCrLf
                Else
                    If i + 1 = Lineas.Length Then
                    Else
                        If Lineas(i).Trim = "" Then
                            If Lineas(i + 1).Trim <> "" Then
                                strReturn += Lineas(i).ToString & vbCrLf
                            End If
                        End If
                    End If
                End If
            Next
            Return strReturn
        Catch ex As System.Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveFormatoNombreCorreo(ByVal vstrNombre As String, ByVal vstrCorreo As String, _
                                                   Optional ByVal vblnSoloNombre As Boolean = False, _
                                                   Optional ByVal vbln2ndaPalabrasMin As Boolean = False)
        If vstrNombre.Trim = "" Then Return ""
        Dim strReturn As String = ""
        Dim chrAr() As Char = vstrNombre.Trim.ToCharArray
        For id As Byte = 0 To chrAr.Length - 1
            If id = 0 Then
                strReturn += chrAr(id).ToString.ToUpper()
            Else
                If chrAr(id).ToString = " " Then
                    strReturn += chrAr(id) & chrAr(id + 1).ToString.ToUpper
                    id += 1
                Else
                    strReturn += chrAr(id).ToString.ToLower
                End If
            End If
        Next
        If Not vblnSoloNombre Then
            strReturn += " - SETOURS S.A.[" & vstrCorreo.ToLower.Trim & "]"
        End If

        Return strReturn
    End Function

    Public Function strDevueleCodigoHTMLxLetra2(ByVal vstrTexto As String) As String
        Dim strResultado As String = vstrTexto
        strResultado = Replace(strResultado, "+a+", "&aacute;")
        strResultado = Replace(strResultado, "+e+", "&eacute;")
        strResultado = Replace(strResultado, "+i+", "&iacute;")
        strResultado = Replace(strResultado, "+o+", "&oacute;")
        strResultado = Replace(strResultado, "+u+", "&uacute;")
        strResultado = Replace(strResultado, "+n+", "&ntilde;")

        vstrTexto = strResultado
        Return strResultado
    End Function

    Public Function bytDevuelveCantidadLineas(ByVal vstrCadena As String) As Byte
        If vstrCadena.Trim.Length = 0 Then Return 0
        Dim bytReturn As Byte = 1
        Dim chrAr() As Char = vstrCadena.ToCharArray
        For id As Byte = 0 To chrAr.Length - 1
            If Asc(chrAr(id)) = Asc(vbCrLf) Then
                bytReturn += 1
            End If

        Next
        Return bytReturn
    End Function

    Public Function strTransformarNombrePax(ByVal str_pPaxNombre As String) As String
        Try
            Dim str_vApellido As String = Mid(str_pPaxNombre, InStr(str_pPaxNombre, " ") + 1, InStr(str_pPaxNombre, ",") - 5)
            Dim str_vNombre As String = Mid(str_pPaxNombre, InStr(str_pPaxNombre, ",") + 1, str_pPaxNombre.Length)
            Dim str_vTitulo As String = Mid(str_pPaxNombre, 1, InStr(str_pPaxNombre, " "))
            Dim str_vNuevoNombre As String = str_vApellido.Trim & "/" & str_vNombre.Trim & " " & str_vTitulo.Trim

            Return str_vNuevoNombre.ToUpper
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function strDevuelveNumeroaLetras(ByVal numero As String) As String
        Dim palabras = "", entero = "", dec = "", flag As String = ""
        Dim num, x, y As Integer

        flag = "N"
        If Mid(numero, 1, 1) = "-" Then
            numero = Mid(numero, 2, numero.ToString.Length - 1).ToString
            palabras = "menos "
        End If
        For x = 1 To numero.ToString.Length
            If Mid(numero, 1, 1) = "0" Then
                numero = Trim(Mid(numero, 2, numero.ToString.Length).ToString)
                If Trim(numero.ToString.Length) = 0 Then palabras = ""
            Else
                Exit For
            End If
        Next
        For y = 1 To Len(numero)
            If Mid(numero, y, 1) = "." Then
                flag = "S"
            Else
                If flag = "N" Then
                    entero = entero + Mid(numero, y, 1)
                Else
                    dec = dec + Mid(numero, y, 1)
                End If
            End If
        Next y

        If Len(dec) = 1 Then dec = dec & "0"
        flag = "N"

        If Val(numero) <= 999999999 Then
            For y = Len(entero) To 1 Step -1
                num = Len(entero) - (y - 1)
                Select Case y
                    Case 3, 6, 9
                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" And Mid(entero, num + 2, 1) = "0" Then
                                    palabras = palabras & "cien "
                                Else
                                    palabras = palabras & "ciento "
                                End If
                            Case "2"
                                palabras = palabras & "doscientos "
                            Case "3"
                                palabras = palabras & "trescientos "
                            Case "4"
                                palabras = palabras & "cuatrocientos "
                            Case "5"
                                palabras = palabras & "quinientos "
                            Case "6"
                                palabras = palabras & "seiscientos "
                            Case "7"
                                palabras = palabras & "setecientos "
                            Case "8"
                                palabras = palabras & "ochocientos "
                            Case "9"
                                palabras = palabras & "novecientos "
                        End Select
                    Case 2, 5, 8
                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    flag = "S"
                                    palabras = palabras & "diez "
                                End If
                                If Mid(entero, num + 1, 1) = "1" Then
                                    flag = "S"
                                    palabras = palabras & "once "
                                End If
                                If Mid(entero, num + 1, 1) = "2" Then
                                    flag = "S"
                                    palabras = palabras & "doce "
                                End If
                                If Mid(entero, num + 1, 1) = "3" Then
                                    flag = "S"
                                    palabras = palabras & "trece "
                                End If
                                If Mid(entero, num + 1, 1) = "4" Then
                                    flag = "S"
                                    palabras = palabras & "catorce "
                                End If
                                If Mid(entero, num + 1, 1) = "5" Then
                                    flag = "S"
                                    palabras = palabras & "quince "
                                End If
                                If Mid(entero, num + 1, 1) > "5" Then
                                    flag = "N"
                                    palabras = palabras & "dieci"
                                End If
                            Case "2"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "veinte "
                                    flag = "S"
                                Else
                                    palabras = palabras & "veinti"
                                    flag = "N"
                                End If
                            Case "3"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "treinta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "treinta y "
                                    flag = "N"
                                End If
                            Case "4"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "cuarenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "cuarenta y "
                                    flag = "N"
                                End If
                            Case "5"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "cincuenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "cincuenta y "
                                    flag = "N"
                                End If
                            Case "6"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "sesenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "sesenta y "
                                    flag = "N"
                                End If
                            Case "7"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "setenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "setenta y "
                                    flag = "N"
                                End If
                            Case "8"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "ochenta "
                                    flag = "S"
                                Else
                                    palabras = palabras & "ochenta y "
                                    flag = "N"
                                End If
                            Case "9"
                                If Mid(entero, num + 1, 1) = "0" Then
                                    palabras = palabras & "noventa "
                                    flag = "S"
                                Else
                                    palabras = palabras & "noventa y "
                                    flag = "N"
                                End If
                        End Select
                    Case 1, 4, 7
                        '*********Asigna las palabras para las unidades*********
                        Select Case Mid(entero, num, 1)
                            Case "1"
                                If flag = "N" Then
                                    If y = 1 Then
                                        palabras = palabras & "uno "
                                    Else
                                        palabras = palabras & "un "
                                    End If
                                End If
                            Case "2"
                                If flag = "N" Then palabras = palabras & "dos "
                            Case "3"
                                If flag = "N" Then palabras = palabras & "tres "
                            Case "4"
                                If flag = "N" Then palabras = palabras & "cuatro "
                            Case "5"
                                If flag = "N" Then palabras = palabras & "cinco "
                            Case "6"
                                If flag = "N" Then palabras = palabras & "seis "
                            Case "7"
                                If flag = "N" Then palabras = palabras & "siete "
                            Case "8"
                                If flag = "N" Then palabras = palabras & "ocho "
                            Case "9"
                                If flag = "N" Then palabras = palabras & "nueve "
                        End Select
                End Select
                If y = 4 Then
                    If Mid(entero, 6, 1) <> "0" Or Mid(entero, 5, 1) <> "0" Or Mid(entero, 4, 1) <> "0" Or _
                    (Mid(entero, 6, 1) = "0" And Mid(entero, 5, 1) = "0" And Mid(entero, 4, 1) = "0" And _
                    Len(entero) <= 6) Then palabras = palabras & "mil "
                End If
                If y = 7 Then
                    If Len(entero) = 7 And Mid(entero, 1, 1) = "1" Then
                        palabras = palabras & "millón "
                    Else
                        palabras = palabras & "millones "
                    End If
                End If
            Next y

            If InStr(palabras, "un mil") > 0 Then
                palabras = Replace(palabras, "un mil", "mil")
            End If

            If dec <> "" Then
                strDevuelveNumeroaLetras = palabras & "Y " & dec
            Else
                strDevuelveNumeroaLetras = palabras
            End If
        Else
            strDevuelveNumeroaLetras = ""
        End If
    End Function


    Public Function strValorCampoArchTexto(ByVal vstrCampo As String, ByVal vstrRutaArchivo As String, _
                                  ByVal vdtCampos As DataTable, ByVal vbytCantLineas As Byte) As String
        Try
            Dim dtPos As DataTable = dtFiltrarDataTable(vdtCampos, "NoCampo='" & vstrCampo & "'")

            Dim oReader As New StreamReader(vstrRutaArchivo)
            Dim strLinea As String = ""
            Dim strClaveLinea As String = dtPos(0)("TxClave")
            Dim bytQtSegmentClave As Byte = dtPos(0)("QtSegmentClave")
            Dim blnClaveEncontrada As Boolean = False
            Dim strValorTemp As String = ""
            Dim bytCantLineas As Byte = 0
            Do
                strLinea = oReader.ReadLine()

                Dim str1erValorLinea As String = ""
                blnClaveEncontrada = False

                If InStr(strLinea, ";") > 0 Then
                    str1erValorLinea = strLinea.Substring(0, InStr(strLinea, ";") - 1)
                Else
                    str1erValorLinea = strLinea
                End If
                'If InStr(str1erValorLinea, strClaveLinea) > 0 Then
                If strClaveLinea.Length <= str1erValorLinea.Length Then
                    If str1erValorLinea.Substring(0, strClaveLinea.Length) = strClaveLinea Then
                        blnClaveEncontrada = True
                    End If
                End If

                'If InStr(strLinea, strClaveLinea) > 0 Then
                If blnClaveEncontrada Then

                    bytCantLineas += 1
                    If bytCantLineas < vbytCantLineas And vstrCampo <> "CoTicket" Then
                        blnClaveEncontrada = False
                        GoTo SgteLinea
                    End If
                    blnClaveEncontrada = True

                    Dim strLineaSel As String = Mid(strLinea, strClaveLinea.Length + 1)

                    strValorTemp = ""
                    Dim bytContValores As Byte = 0
                    If InStr(strLineaSel, ";") > 0 Then
                        For intCar As Int16 = 0 To strLineaSel.Length - 1
                            Dim chrCar As Char = strLineaSel.Substring(intCar, 1)
                            If chrCar <> ";" Then
                                strValorTemp &= chrCar
                            Else
                                bytContValores += 1
                                If bytContValores = bytQtSegmentClave Then
                                    If IsDBNull(dtPos(0)("QtPosInicio")) And IsDBNull(dtPos(0)("QtTamanio")) Then
                                        Return strValorTemp.Trim
                                    ElseIf Not IsDBNull(dtPos(0)("QtPosInicio")) And Not IsDBNull(dtPos(0)("QtTamanio")) Then
                                        If strValorTemp <> "" Then
                                            strValorTemp = strValorTemp.Substring(dtPos(0)("QtPosInicio") - 1, dtPos(0)("QtTamanio"))
                                        End If
                                        Return strValorTemp.Trim
                                    ElseIf Not IsDBNull(dtPos(0)("QtPosInicio")) And IsDBNull(dtPos(0)("QtTamanio")) Then
                                        If strValorTemp <> "" Then
                                            strValorTemp = Mid(strValorTemp, dtPos(0)("QtPosInicio"))
                                        End If
                                        Return strValorTemp.Trim
                                    End If
                                Else
                                    strValorTemp = ""
                                End If
                            End If

                        Next


                    Else
                        strValorTemp = strLineaSel
                        If IsDBNull(dtPos(0)("QtPosInicio")) And IsDBNull(dtPos(0)("QtTamanio")) Then
                            Return strValorTemp.Trim
                        ElseIf Not IsDBNull(dtPos(0)("QtPosInicio")) And Not IsDBNull(dtPos(0)("QtTamanio")) Then
                            strValorTemp = strValorTemp.Substring(dtPos(0)("QtPosInicio") - 1, dtPos(0)("QtTamanio"))
                            Return strValorTemp.Trim
                        ElseIf Not IsDBNull(dtPos(0)("QtPosInicio")) And IsDBNull(dtPos(0)("QtTamanio")) Then
                            strValorTemp = Mid(strValorTemp, dtPos(0)("QtPosInicio"))
                            Return strValorTemp.Trim
                        End If
                    End If
                End If

SgteLinea:

            Loop Until strLinea Is Nothing Or blnClaveEncontrada = True
            oReader.Close()

            Return strValorTemp.Trim

        Catch ex As Exception
            Throw
        End Try

    End Function
    Public Function bytCantLineasArchTexto(ByVal vstrCampo As String, ByVal vstrRutaArchivo _
                                           As String, ByVal vstrClave As String) As Byte
        Try
            Dim oReader As New StreamReader(vstrRutaArchivo)
            Dim strLinea As String = ""
            Dim bytCantLineas As Byte = 0
            Do
                strLinea = oReader.ReadLine()
                'If InStr(strLinea, vstrClave) > 0 Then
                '    bytCantLineas += 1
                'End If
                If strLinea Is Nothing Then Return bytCantLineas

                Dim blnClaveEncontrada As Boolean = False
                Dim str1erValorLinea As String
                If InStr(strLinea, ";") > 0 Then
                    str1erValorLinea = strLinea.Substring(0, InStr(strLinea, ";") - 1)
                Else
                    str1erValorLinea = strLinea
                End If
                'If InStr(str1erValorLinea, strClaveLinea) > 0 Then
                If vstrClave.Length <= str1erValorLinea.Length Then
                    If str1erValorLinea.Substring(0, vstrClave.Length) = vstrClave Then
                        blnClaveEncontrada = True
                    End If
                End If

                If blnClaveEncontrada Then
                    bytCantLineas += 1
                End If

            Loop Until strLinea Is Nothing


            Return bytCantLineas
        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function CalculoCotizacionPreview(ByVal vListaTCambioMonedas As List(Of clsCotizacionBE.clsCotizacionTiposCambioBE), _
                                      ByVal vstrIDServicio As String, _
                                      ByVal vintIDServicio_Det As Int32, ByVal vstrAnio As String, ByVal vstrTipo As String, _
                                      ByVal vstrTipoProv As String, ByVal vintPax As Int16, _
                                      ByVal vintLiberados As Int16, ByVal vstrTipo_Lib As Char, _
                                      ByVal vdblMargenCotiCab As Double, ByVal vdblTCambioCotiCab As Single, _
                                      ByVal vdblCostoReal As Double, ByVal vdblMargenAplicado As Double, ByVal vstrTipoPax As String, _
                                      ByVal vsglRedondeo As Single, _
                                      ByRef rdttDetCotiServ As DataTable, _
                                      Optional ByVal vsglIGV As Single = 0, _
                                      Optional ByVal vstrIDDet_DetCotiServ As String = "", _
                                      Optional ByVal vblnIncGuia As Boolean = False, _
                                      Optional ByVal vblnMargenCero As Boolean = False, _
                                      Optional ByVal vCnn As SqlClient.SqlConnection = Nothing, _
                                      Optional ByVal vCnn2 As SqlClient.SqlConnection = Nothing, _
                                      Optional ByVal vdatFechaServicio As Date = #1/1/1900#, _
                                      Optional ByVal vdblSSCR As Double = -1, _
                                      Optional ByVal vdblSTCR As Double = -1, _
                                      Optional ByVal vblnServicioTC As Boolean = False, _
                                      Optional ByVal vstrIDTipoHonorario As String = "", _
                                      Optional ByVal vdicValoresTC As Dictionary(Of String, Double) = Nothing, _
                                      Optional ByVal vblnAcomVehic As Boolean = False) As Dictionary(Of String, Double)

        Try
            Dim objDetServ As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
            Dim strCoMoneda As String = objDetServ.strCoMonedaDetServicio(vintIDServicio_Det)
            Dim sglTCambioCoti As Single = 0
            If vListaTCambioMonedas IsNot Nothing Then
                For Each ItemTC As clsCotizacionBE.clsCotizacionTiposCambioBE In vListaTCambioMonedas
                    If strCoMoneda = ItemTC.CoMoneda Then
                        sglTCambioCoti = ItemTC.SsTipCam
                        Exit For
                    End If
                Next
            End If

            If vblnServicioTC Then
                sglTCambioCoti = vdblTCambioCotiCab
            End If

            Dim obj As New clsCotizacionBN
            Return obj.CalculoCotizacion( _
                vstrIDServicio, vintIDServicio_Det, vstrAnio, vstrTipo, vstrTipoProv, vintPax, _
                vintLiberados, _
                vstrTipo_Lib, _
                vdblMargenCotiCab, sglTCambioCoti, vdblCostoReal, vdblMargenAplicado, vstrTipoPax, vsglRedondeo, rdttDetCotiServ, _
                gsglIGV, vstrIDDet_DetCotiServ, vblnIncGuia, vblnMargenCero, vCnn, vCnn2, vdatFechaServicio, _
                vdblSSCR, vdblSTCR, vblnServicioTC, vstrIDTipoHonorario, vdicValoresTC, vblnAcomVehic)


        Catch ex As Exception
            Throw
        End Try
    End Function

    Public Function blnNumeroEncontradoenCadena(ByVal vstrCadena As String) As Boolean

        For bytCar As Byte = 0 To vstrCadena.Length - 1
            Dim chrCar As Char = vstrCadena.Substring(bytCar, 1)
            If IsNumeric(chrCar) Then
                Return True
            End If
        Next

        Return False
    End Function

    Public Sub ImprimirSinPreview(ByVal vstrRepNom As String, ByVal vDtt As DataTable, ByVal vLstPar As List(Of String))
        'Try
        '    Dim objRep As Object = Nothing
        '    Select Case vstrRepNom
        '        Case "Documento"
        '            objRep = New cryDocumentoDet
        '            objRep.SetDataSource(vDtt)
        '            objRep.SetParameterValue("pstrTotalAcumuladoLetras", vLstPar(0))
        '            objRep.SetParameterValue("pTipoDocumentoDet", vLstPar(1))
        '    End Select

        '    Dim docToPrint As New System.Drawing.Printing.PrintDocument()
        '    docToPrint.PrinterSettings.PrinterName = "EPSON FX-890 Ver 2.0" ' (redirected 10)" ' "finanzas" ' "EPSON FX-890 Ver 2.0" ' (redirected 10)" ' "Xerox Tesoreria" ' "EPSON FX-890 Ver 2.0"
        '    For i = 0 To docToPrint.PrinterSettings.PaperSizes.Count - 1
        '        Dim rawKind As Integer
        '        If docToPrint.PrinterSettings.PaperSizes(i).PaperName = "BoletaVentaSetra" Then
        '            rawKind = Convert.ToInt32(docToPrint.PrinterSettings.PaperSizes(i).GetType().GetField("kind", Reflection.BindingFlags.Instance Or Reflection.BindingFlags.NonPublic).GetValue(docToPrint.PrinterSettings.PaperSizes(i)))
        '            objRep.PrintOptions.PaperSize = CType(rawKind, CrystalDecisions.Shared.PaperSize)
        '            objRep.PrintOptions.PrinterName = "EPSON FX-890 Ver 2.0" ' (redirected 10)" ' "finanzas" ' "EPSON FX-890 Ver 2.0 (redirected 10)"
        '            'MessageBox.Show(objRep.PrintOptions.SavedPrinterName)
        '            Exit For
        '        End If
        '    Next
        '    objRep.PrintToPrinter(1, False, 1, 1)

        'Catch ex As Exception
        '    Throw
        'End Try

    End Sub

    Public Sub MarcarColumnaCheckenBloque(ByRef rDgv As DataGridView, ByVal vbytCol As Byte)
        Try
            Dim bytCont As Byte = 1
            Dim blnActivar As Boolean = False
            For Each dr As DataGridViewRow In rDgv.Rows
                If dr.Cells(vbytCol).Selected Then
                    If bytCont = 1 Then
                        blnActivar = Convert.ToBoolean(dr.Cells(vbytCol).Value)
                    End If
                    If bytCont >= 2 Then
                        If Convert.ToBoolean(dr.Cells(vbytCol).Value) = Not blnActivar Then
                            dr.Cells(vbytCol).Value = blnActivar
                        End If
                    End If
                    bytCont += 1
                End If
            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub


    Public Class PDFFooter
        Inherits PdfPageEventHelper

        Private _FechaPago As Date
        Public Property FechaPago() As Date
            Get
                Return _FechaPago
            End Get
            Set(ByVal value As Date)
                _FechaPago = value
            End Set
        End Property

        Private _Account As String
        Public Property Account() As String
            Get
                Return _Account
            End Get
            Set(ByVal value As String)
                _Account = value
            End Set
        End Property

        Private _Swift As String
        Public Property Swift() As String
            Get
                Return _Swift
            End Get
            Set(ByVal value As String)
                _Swift = value
            End Set
        End Property

        Private _Bank As String
        Public Property Bank() As String
            Get
                Return _Bank
            End Get
            Set(ByVal value As String)
                _Bank = value
            End Set
        End Property

        Private _Address As String
        Public Property Address() As String
            Get
                Return _Address
            End Get
            Set(ByVal value As String)
                _Address = value
            End Set
        End Property

        Private _Comment As String
        Public Property Comment() As String
            Get
                Return _Comment
            End Get
            Set(ByVal value As String)
                _Comment = value
            End Set
        End Property

        Private _Total As Double
        Public Property Total() As Double
            Get
                Return _Total
            End Get
            Set(ByVal value As Double)
                _Total = value
            End Set
        End Property

        Private _ActionFooterSelect As String
        Public Property ActionFooterSelect() As String
            Get
                Return _ActionFooterSelect
            End Get
            Set(ByVal value As String)
                _ActionFooterSelect = value
            End Set
        End Property

        Private _IDTipoProveedor As String
        Public Property IDTipoProveedor() As String
            Get
                Return _IDTipoProveedor
            End Get
            Set(ByVal value As String)
                _IDTipoProveedor = value
            End Set
        End Property

        Private _NewPage As Boolean
        Public Property NewPage() As Boolean
            Get
                Return _NewPage
            End Get
            Set(ByVal value As Boolean)
                _NewPage = value
            End Set
        End Property

        Private _DatosNewPage As System.Data.DataTable
        Public Property DatosNewPage() As System.Data.DataTable
            Get
                Return _DatosNewPage
            End Get
            Set(ByVal value As System.Data.DataTable)
                _DatosNewPage = value
            End Set
        End Property

        Private _FechaIn_BibliaReport As String
        Public Property FechaIn_BibliaReport() As String
            Get
                Return _FechaIn_BibliaReport
            End Get
            Set(ByVal value As String)
                _FechaIn_BibliaReport = value
            End Set
        End Property

        Private _FechaOut_BibliaReport As String
        Public Property FechaOut_BibliaReport() As String
            Get
                Return _FechaOut_BibliaReport
            End Get
            Set(ByVal value As String)
                _FechaOut_BibliaReport = value
            End Set
        End Property

        Private _EsOrdenPago As Boolean
        Public Property EsOrdenPago() As Boolean
            Get
                Return _EsOrdenPago
            End Get
            Set(ByVal value As Boolean)
                _EsOrdenPago = value
            End Set
        End Property


        Protected Friend Function OnNumberPageHeader(ByVal writer As iTextSharp.text.pdf.PdfWriter) As PdfPTable
            Try
                Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
                Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
                Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
                Dim NewFontBoldtLt As Font = New Font(bsDefault, 8, Font.BOLD)

                Dim pdftabla As New PdfPTable(1)
                With pdftabla
                    '.SetTotalWidth(New Single() {3, 16})
                    .DefaultCell.BorderWidth = 0
                    .DefaultCell.HorizontalAlignment = ALIGN_RIGHT
                    .WidthPercentage = 100
                    .AddCell(New Paragraph("Pág :       " & writer.PageNumber.ToString, NewFont))

                    .DefaultCell.HorizontalAlignment = ALIGN_CENTER
                    .AddCell(New Paragraph("Biblia de Servicios", NewFontBold))
                    .AddCell(New Paragraph("Del " & Me.FechaIn_BibliaReport & " Al " & Me.FechaOut_BibliaReport, NewFontBoldtLt))

                    .DefaultCell.Padding = 0
                    .AddCell(New Paragraph(" ", NewFontBold))
                    .AddCell(OnTableHeader())
                End With

                Return pdftabla
            Catch ex As Exception
                Throw
            End Try
        End Function

        Private Function OnTableHeader() As PdfPTable
            Try
                Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
                Dim NewFont As Font = New Font(bsDefault, 8, Font.COURIER)
                Dim NewFontBold As Font = New Font(bsDefault, 16, Font.BOLD)
                Dim NewFontBoldtLt As Font = New Font(bsDefault, 7, Font.BOLD)
                Dim pdfTableHeader As New PdfPTable(10)
                With pdfTableHeader
                    .SetTotalWidth(New Single() {3.0F, 6.0F, 1.8F, 3.5F, 2.0F, 18.0F, 3.0F, 4.0F, 5.0F, 1.0F})
                    .DefaultCell.BorderWidth = 1
                    .WidthPercentage = 100
                    .DefaultCell.VerticalAlignment = ALIGN_MIDDLE


                    .AddCell(New Paragraph("File", NewFontBoldtLt))
                    .AddCell(New Paragraph("Cliente", NewFontBoldtLt))
                    .AddCell(New Paragraph("Hora", NewFontBoldtLt))
                    .AddCell(New Paragraph("Ubigeo", NewFontBoldtLt))
                    .AddCell(New Paragraph("Pax/L", NewFontBoldtLt))
                    .AddCell(New Paragraph("Servicio", NewFontBoldtLt))
                    .AddCell(New Paragraph("Tipo/Id.", NewFontBoldtLt))
                    .AddCell(New Paragraph("Guía / Trasladista", NewFontBoldtLt))
                    .AddCell(New Paragraph("Operador / Transportista", NewFontBoldtLt))
                    .AddCell(New Paragraph("R", NewFontBoldtLt))

                    '.AddCell(New Paragraph("14040178", NewFontBoldtLt))
                    '.AddCell(New Paragraph("ECO ADVENTURES INC", NewFontBoldtLt))
                    '.AddCell(New Paragraph("05:20", NewFontBoldtLt))
                    '.AddCell(New Paragraph("RIO DE JANEIRO", NewFontBoldtLt))
                    '.AddCell(New Paragraph("2 / 0", NewFontBoldtLt))
                    '.AddCell(New Paragraph("Transfer Casa Andina PC Miraflores / Airport (LA2047 LIM/CUZ 09:15) - E1", NewFontBoldtLt))
                    '.AddCell(New Paragraph("PVT(ENG)", NewFontBoldtLt))
                    '.AddCell(New Paragraph("Chavarria Dulanto, Renato", NewFontBoldtLt))
                    '.AddCell(New Paragraph("Setours Cusco / Sava Tours (Bus Modasa - 32 Pax )", NewFontBoldtLt))
                    '.AddCell(New Paragraph("JS", NewFontBoldtLt))
                End With
                Return pdfTableHeader
            Catch ex As Exception
                Throw
            End Try
        End Function

        Public Overrides Sub OnStartPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document)
            MyBase.OnStartPage(writer, document)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            If ActionFooterSelect Is Nothing Then Exit Sub
            Select Case ActionFooterSelect
                Case "BibliaHeader"
                    'document.Add(New Paragraph("Test " & writer.PageNumber.ToString()))
                    document.Add(OnNumberPageHeader(writer))
                    'document.Add(OnTableHeader())

                Case "DefaultImageHeader"
                    Dim imgLogo As iTextSharp.text.Image
                    imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
                    imgLogo.SetAbsolutePosition(36, 730) 'Posición en el eje cartesiano
                    imgLogo.ScaleAbsoluteWidth(523) 'Ancho de la imagen
                    imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
                    document.Add(imgLogo) ' Agrega la imagen al documento

                    If EsOrdenPago Then
                        'Lineas Verticales
                        Dim Lin1 As PdfContentByte = writer.DirectContent
                        Lin1.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin1.MoveTo(202.0F, 608.0F) 'MoveTo indica el punto de inicio
                        Lin1.LineTo(202.0F, 591.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin1.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin2 As PdfContentByte = writer.DirectContent
                        Lin2.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin2.MoveTo(202.0F, 540.0F) 'MoveTo indica el punto de inicio
                        Lin2.LineTo(202.0F, 505.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin2.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin3 As PdfContentByte = writer.DirectContent
                        Lin3.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin3.MoveTo(376.0F, 608.0F) 'MoveTo indica el punto de inicio
                        Lin3.LineTo(376.0F, 591.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin3.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin4 As PdfContentByte = writer.DirectContent
                        Lin4.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin4.MoveTo(376.0F, 557.0F) 'MoveTo indica el punto de inicio
                        Lin4.LineTo(376.0F, 540.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin4.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin5 As PdfContentByte = writer.DirectContent
                        Lin5.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin5.MoveTo(376.0F, 540.0F) 'MoveTo indica el punto de inicio
                        Lin5.LineTo(376.0F, 523.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin5.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin6 As PdfContentByte = writer.DirectContent
                        Lin6.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin6.MoveTo(376.0F, 523.0F) 'MoveTo indica el punto de inicio
                        Lin6.LineTo(376.0F, 506.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin6.Stroke() 'traza la linea actual y se puede iniciar una nueva
                    End If
                Case "TKTPropinaHeader"
                    Dim imgLogo As iTextSharp.text.Image
                    imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
                    imgLogo.SetAbsolutePosition(10, 215) 'Posición en el eje cartesiano
                    imgLogo.ScaleAbsoluteWidth(document.PageSize.Width - 20) 'Ancho de la imagen
                    imgLogo.ScaleAbsoluteHeight(65) 'Altura de la imagen
                    document.Add(imgLogo) ' Agrega la imagen al documento
                Case "FooterDebitMemo"
                    Dim imgLogo As iTextSharp.text.Image
                    imgLogo = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "SETOURS LOGO-FINAL.jpg") 'Dirección a la imagen que se hace referencia
                    imgLogo.SetAbsolutePosition(36, 740) 'Posición en el eje cartesiano
                    imgLogo.ScaleAbsoluteWidth(523) 'Ancho de la imagen
                    imgLogo.ScaleAbsoluteHeight(85) 'Altura de la imagen
                    document.Add(imgLogo) ' Agrega la imagen al documento

                    If NewPage Then
                        'Lineas Verticales
                        Dim Lin2 As PdfContentByte = writer.DirectContent
                        Lin2.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin2.MoveTo(36.0F, 720.0F) 'MoveTo indica el punto de inicio
                        Lin2.LineTo(36.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin2.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin3 As PdfContentByte = writer.DirectContent
                        Lin3.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin3.MoveTo(73.0F, 694.0F) 'MoveTo indica el punto de inicio
                        Lin3.LineTo(73.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin3.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin4 As PdfContentByte = writer.DirectContent
                        Lin4.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin4.MoveTo(480.0F, 694.0F) 'MoveTo indica el punto de inicio
                        Lin4.LineTo(480.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin4.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin5 As PdfContentByte = writer.DirectContent
                        Lin5.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin5.MoveTo(559.0F, 720.0F) 'MoveTo indica el punto de inicio
                        Lin5.LineTo(559.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin5.Stroke() 'traza la linea actual y se puede iniciar una nueva


                        'Lineas Horizontal
                        Dim Lin6 As PdfContentByte = writer.DirectContent
                        Lin6.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin6.MoveTo(73.0F, 38.0F) 'MoveTo indica el punto de inicio
                        Lin6.LineTo(559.0F, 38.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin6.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Horizontal
                        Dim Lin7 As PdfContentByte = writer.DirectContent
                        Lin7.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin7.MoveTo(36.0F, 20.0F) 'MoveTo indica el punto de inicio
                        Lin7.LineTo(559.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin7.Stroke() 'traza la linea actual y se puede iniciar una nueva
                    Else
                        'Lineas Verticales
                        Dim Lin2 As PdfContentByte = writer.DirectContent
                        Lin2.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin2.MoveTo(36.0F, 578.0F) 'MoveTo indica el punto de inicio
                        Lin2.LineTo(36.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin2.Stroke() 'traza la linea actual y se puede iniciar una nueva


                        'Lineas Verticales
                        Dim Lin3 As PdfContentByte = writer.DirectContent
                        Lin3.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin3.MoveTo(73.0F, 552.0F) 'MoveTo indica el punto de inicio
                        Lin3.LineTo(73.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin3.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin4 As PdfContentByte = writer.DirectContent
                        Lin4.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin4.MoveTo(480.0F, 552.0F) 'MoveTo indica el punto de inicio
                        Lin4.LineTo(480.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin4.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Verticales
                        Dim Lin5 As PdfContentByte = writer.DirectContent
                        Lin5.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin5.MoveTo(559.0F, 578.0F) 'MoveTo indica el punto de inicio
                        Lin5.LineTo(559.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin5.Stroke() 'traza la linea actual y se puede iniciar una nueva


                        'Lineas Horizontal
                        Dim Lin6 As PdfContentByte = writer.DirectContent
                        Lin6.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin6.MoveTo(73.0F, 38.0F) 'MoveTo indica el punto de inicio
                        Lin6.LineTo(559.0F, 38.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin6.Stroke() 'traza la linea actual y se puede iniciar una nueva

                        'Lineas Horizontal
                        Dim Lin7 As PdfContentByte = writer.DirectContent
                        Lin7.SetLineWidth(0.03) 'configurando el ancho de linea
                        Lin7.MoveTo(36.0F, 20.0F) 'MoveTo indica el punto de inicio
                        Lin7.LineTo(559.0F, 20.0F) 'LineTo indica hacia donde se dibuja la linea
                        Lin7.Stroke() 'traza la linea actual y se puede iniciar una nueva
                    End If
            End Select
        End Sub

        Public Overrides Sub OnEndPage(ByVal writer As iTextSharp.text.pdf.PdfWriter, ByVal document As iTextSharp.text.Document)
            MyBase.OnEndPage(writer, document)
            Dim bsDefault As BaseFont = BaseFont.CreateFont(gstrRutaImagenes & "ARIALN.ttf", BaseFont.CP1252, BaseFont.EMBEDDED)
            Dim NewFont As Font = New Font(bsDefault, 10, Font.COURIER)

            Select Case ActionFooterSelect
                Case "FooterDebitMemo"
                    If Not NewPage Then
                        document.SetMargins(200.0F, 200.0F, 200.0F, 600.0F)
                    Else
                        document.SetMargins(36.0F, 36.0F, 122.0F, 170.0F)
                    End If

                    Dim tabFot As New PdfPTable(6)
                    With tabFot
                        .DefaultCell.BorderWidth = 0
                        .SetTotalWidth(New Single() {16, 120, 5, 180, 8, 170})
                        .WidthPercentage = 100

                        Dim NewFont2 As New Font(bsDefault, 10, Font.BOLD)
                        .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph("PAYMENT DUE DATE" & vbCrLf, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(":", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" " & FechaPago.ToShortDateString, NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                        .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont2)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph("BENEFICIARY NAME" & vbCrLf, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" " & gstrEmpresa.ToUpper, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph("ACCOUNT" & vbCrLf, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" " & Account, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph("SWIFT CODE" & vbCrLf, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" " & Swift, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph("BANK" & vbCrLf, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" " & Bank, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph("ADDRESS" & vbCrLf, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(":", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" " & Address, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        '.AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        '.AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        '.AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        '.AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        '.AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        '.AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        'DefinirBordesTablas(tabFot, 0, 0, 0, 0.03)
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .DefaultCell.Colspan = 5
                        .DefaultCell.PaddingBottom = 4.5F
                        .AddCell(New Paragraph(Comment, NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
                        'DefinirBordesTablas(tabFot, 0, 0, 0, 0)


                        '.AddCell(New Paragraph("The agency/client must indicate in the bank wire order: ""Details of Charges - OURS"""))
                        .DefaultCell.PaddingLeft = 18
                        .AddCell(New Paragraph("The agency/client must indicate in the bank wire order: ""Details of Charges - OURS""", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

                        .DefaultCell.PaddingBottom = 2.0F
                        .DefaultCell.PaddingLeft = 2.0F
                        'Agregando la Ultima Fila Total
                        .AddCell(New Paragraph(" ", NewFont)) 'FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
                        .AddCell(tblFooterTotalDebitMemo(Total))
                    End With
                    tabFot.WriteSelectedRows(0, -1, 60, document.Bottom, writer.DirectContent)
                Case "FooterVoucher"
                    'document.SetMargins(100.0F, 200.0F, 200.0F, 900.0F)
                    Dim tabFot As New PdfPTable(1)
                    Dim imgPieVoucher As iTextSharp.text.Image = Nothing

                    With tabFot
                        .DefaultCell.Border = 0

                        If IDTipoProveedor = gstrTipoProveeHoteles Then
                            .SetTotalWidth(New Single() {490.0F})
                            imgPieVoucher = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "PieVoucher.png") 'Dirección a la imagen que se hace referencia
                            imgPieVoucher.SetAbsolutePosition(30, 200)
                            imgPieVoucher.ScaleAbsoluteWidth(550)
                            imgPieVoucher.ScaleAbsoluteHeight(100)
                        Else
                            .SetTotalWidth(New Single() {475.0F})
                            imgPieVoucher = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "PieVoucherServicios.png") 'Dirección a la imagen que se hace referencia
                            imgPieVoucher.SetAbsolutePosition(30, 200)
                            imgPieVoucher.ScaleAbsoluteWidth(650)
                            imgPieVoucher.ScaleAbsoluteHeight(200)
                        End If

                        .WidthPercentage = 100
                        .DefaultCell.HorizontalAlignment = ALIGN_CENTER

                        .AddCell(imgPieVoucher)

                    End With

                    tabFot.WriteSelectedRows(0, -1, 60, document.Bottom, writer.DirectContent)
            End Select
            ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
            '' ''Select Case ActionFooterSelect
            '' ''    Case "FooterDebitMemo"
            '' ''        document.SetMargins(200.0F, 200.0F, 200.0F, 600.0F)
            '' ''        Dim tabFot As New PdfPTable(6)
            '' ''        With tabFot
            '' ''            .DefaultCell.BorderWidth = 0
            '' ''            .SetTotalWidth(New Single() {16, 120, 5, 180, 8, 170})
            '' ''            .WidthPercentage = 100

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph("PAYMENT DUE DATE" & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" " & FechaPago.ToShortDateString, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph("BENEFICIARY NAME" & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" " & gstrEmpresa.ToUpper, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph("ACCOUNT" & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" " & Account, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph("SWIFT CODE" & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" " & Swift, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph("BANK" & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" " & Bank, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph("ADDRESS" & vbCrLf, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(":", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" " & Address, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))

            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .DefaultCell.Colspan = 5
            '' ''            .AddCell(New Paragraph(Comment, FontFactory.GetFont(strTipoLetraVoucher, 9, Font.BOLD)))

            '' ''            'Agregando la Ultima Fila Total
            '' ''            .AddCell(New Paragraph(" ", FontFactory.GetFont(strTipoLetraVoucher, 9, Font.NORMAL)))
            '' ''            .AddCell(tblFooterTotalDebitMemo(Total))
            '' ''        End With
            '' ''        tabFot.WriteSelectedRows(0, -1, 60, document.Bottom, writer.DirectContent)
            '' ''    Case "FooterVoucher"
            '' ''        'document.SetMargins(100.0F, 200.0F, 200.0F, 900.0F)
            '' ''        Dim tabFot As New PdfPTable(1)
            '' ''        Dim imgPieVoucher As iTextSharp.text.Image = Nothing

            '' ''        With tabFot
            '' ''            .DefaultCell.Border = 0

            '' ''            If IDTipoProveedor = gstrTipoProveeHoteles Then
            '' ''                .SetTotalWidth(New Single() {490.0F})
            '' ''                imgPieVoucher = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "PieVoucher.png") 'Dirección a la imagen que se hace referencia
            '' ''                imgPieVoucher.SetAbsolutePosition(30, 200)
            '' ''                imgPieVoucher.ScaleAbsoluteWidth(550)
            '' ''                imgPieVoucher.ScaleAbsoluteHeight(100)
            '' ''            Else
            '' ''                .SetTotalWidth(New Single() {475.0F})
            '' ''                imgPieVoucher = iTextSharp.text.Image.GetInstance(gstrRutaImagenes & "PieVoucherServicios.png") 'Dirección a la imagen que se hace referencia
            '' ''                imgPieVoucher.SetAbsolutePosition(30, 200)
            '' ''                imgPieVoucher.ScaleAbsoluteWidth(650)
            '' ''                imgPieVoucher.ScaleAbsoluteHeight(200)
            '' ''            End If

            '' ''            .WidthPercentage = 100
            '' ''            .DefaultCell.HorizontalAlignment = ALIGN_CENTER

            '' ''            .AddCell(imgPieVoucher)

            '' ''        End With

            '' ''        tabFot.WriteSelectedRows(0, -1, 60, document.Bottom, writer.DirectContent)
            '' ''End Select
        End Sub


        Public Sub LlenarCeldaExcel(ByVal xlWorkSheet As Object, ByVal x As Integer, ByVal y As Integer, ByVal vstrTexto As String, ByVal vintAlineamiento As Integer, ByVal vintNegrita As Integer, ByVal vstrFuente As String, ByVal vintTamanio As Integer, ByVal vintColorFondo As Integer, ByVal vintColorTexto As Integer)
            Try
                If vstrTexto.Trim.Length > 0 Then
                    xlWorkSheet.Cells.Item(x, y) = vstrTexto
                End If


                If vintTamanio > 0 Then
                    xlWorkSheet.Cells.Item(x, y).HorizontalAlignment = vintTamanio
                End If

                If vintNegrita > 0 Then
                    xlWorkSheet.Cells.Item(x, y).Font.Bold = vintNegrita
                End If

                xlWorkSheet.Cells.Item(x, y).Font.Name = vstrFuente

                If vintColorFondo > 0 Then
                    xlWorkSheet.Cells.Item(x, y).Interior.Color = vintColorFondo
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End Sub

        Public Sub LlenarCeldaExcel_Ref(ByRef xlWorkSheet As Object, ByVal x As Integer, ByVal y As Integer, ByVal vstrTexto As String, ByVal vintAlineamiento As Integer, ByVal vintNegrita As Integer, ByVal vstrFuente As String, ByVal vintTamanio As Integer, ByVal vintColorFondo As Integer, ByVal vintColorTexto As Integer)
            Try
                If vstrTexto.Trim.Length > 0 Then
                    xlWorkSheet.Cells.Item(x, y) = vstrTexto
                End If


                If vintAlineamiento > 0 Then
                    xlWorkSheet.Cells.Item(x, y).HorizontalAlignment = vintAlineamiento
                End If

                If vintTamanio > 0 Then
                    xlWorkSheet.Cells.Item(x, y).Font.Size = vintTamanio
                End If

                If vintNegrita > 0 Then
                    xlWorkSheet.Cells.Item(x, y).Font.Bold = vintNegrita
                End If

                xlWorkSheet.Cells.Item(x, y).Font.Name = vstrFuente

                If vintColorFondo > 0 Then
                    xlWorkSheet.Cells.Item(x, y).Interior.Color = vintColorFondo
                End If

                If vintColorTexto > 0 Then
                    xlWorkSheet.Cells.Item(x, y).Font.Color = vintColorTexto
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End Sub

        Public Sub LlenarCeldaExcel_Ref_Rango(ByRef xlWorkSheet As Object, ByVal strRango As String, ByVal blnMerge As Boolean, ByVal vstrTexto As String, ByVal vintAlineamiento As Integer, ByVal vintNegrita As Integer, ByVal vstrFuente As String, ByVal vintTamanio As Integer, ByVal vintColorFondo As Integer, ByVal vintColorTexto As Integer, Optional ByVal x As Integer = 0, Optional ByVal y As Integer = 0)
            Try
                If vstrTexto.Trim.Length > 0 Then
                    If x > 0 Or y > 0 Then
                        xlWorkSheet.Cells.Item(x, y) = vstrTexto
                    End If
                End If

                If blnMerge Then
                    xlWorkSheet.Range(strRango).Merge(blnMerge)
                End If

                If vintAlineamiento > 0 Then
                    xlWorkSheet.Range(strRango).HorizontalAlignment = vintAlineamiento
                End If

                If vintTamanio > 0 Then
                    xlWorkSheet.Range(strRango).Font.Size = vintTamanio
                End If

                If vintNegrita > 0 Then
                    xlWorkSheet.Range(strRango).Font.Bold = vintNegrita
                End If

                xlWorkSheet.Range(strRango).Font.Name = vstrFuente

                If vintColorFondo > 0 Then
                    xlWorkSheet.Range(strRango).Interior.Color = vintColorFondo
                End If

                If vintColorTexto > 0 Then
                    xlWorkSheet.Range(strRango).Font.Color = vintColorTexto
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End Sub

        Public Sub LlenarCeldaExcel_Ref_Rango_Sel(ByRef xlApp As Object, ByRef xlWorkSheet As Object, ByVal strRango As String, ByVal blnMerge As Boolean, ByVal vstrTexto As String, ByVal vintAlineamiento As Integer, ByVal vintAlineamiento_V As Integer, ByVal vintNegrita As Integer, ByVal vstrFuente As String, ByVal vintTamanio As Integer, ByVal vintColorFondo As Integer, ByVal vintColorTexto As Integer, Optional ByVal x As Integer = 0, Optional ByVal y As Integer = 0)
            Try
                If vstrTexto.Trim.Length > 0 Then
                    If x > 0 Or y > 0 Then
                        xlWorkSheet.Cells.Item(x, y) = vstrTexto
                    End If
                End If

                If blnMerge Then
                    'xlWorkSheet.Range(strRango).Merge(blnMerge)
                    xlWorkSheet.Range(strRango).Select()
                    xlApp.Selection.MergeCells = blnMerge
                End If

                If vintAlineamiento > 0 Then
                    xlWorkSheet.Range(strRango).HorizontalAlignment = vintAlineamiento
                End If

                If vintAlineamiento_V > 0 Then
                    xlWorkSheet.Range(strRango).VerticalAlignment = vintAlineamiento_V
                End If

                If vintTamanio > 0 Then
                    xlWorkSheet.Range(strRango).Font.Size = vintTamanio
                End If

                If vintNegrita > 0 Then
                    xlWorkSheet.Range(strRango).Font.Bold = vintNegrita
                End If

                xlWorkSheet.Range(strRango).Font.Name = vstrFuente

                If vintColorFondo > 0 Then
                    xlWorkSheet.Range(strRango).Interior.Color = vintColorFondo
                End If

                If vintColorTexto > 0 Then
                    xlWorkSheet.Range(strRango).Font.Color = vintColorTexto
                End If

            Catch ex As Exception
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End Try

        End Sub

        'Private Sub EliminarAgregarDetPaxMPWP(ByVal vbytFila As String, ByVal vstrMPWP As String, ByRef rFrm As frmIngDetCotizacionDatoMem)
        '    Try
        '        Dim strIDPax As String = rFrm.lvwPax.Items(vbytFila).SubItems(3).Text
        '        Dim strCol As String = ""
        '        Dim strIDServicioMPWP As String = ""
        '        If vstrMPWP = "MP" Then
        '            strCol = "FlEntradaMAPI"
        '            strIDServicioMPWP = gstrIDServicioMP
        '        ElseIf vstrMPWP = "WP" Then
        '            strCol = "FlEntradaWayna"
        '            strIDServicioMPWP = gstrIDServicioWP
        '        End If



        '    Catch ex As Exception
        '        Throw
        '    End Try
        'End Sub

    End Class

    Public Function strReemplazarNombreCarpeta(vstr As String) As String
        Try
            'Public gstrCaracteresEspeciales As String = "\/:*?""<>|"

            For Each c As Char In gstrCaracteresEspeciales.ToCharArray
                vstr = vstr.Replace(c.ToString, "")
            Next

            Return vstr

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function
End Module




