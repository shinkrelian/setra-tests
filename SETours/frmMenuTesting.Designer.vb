﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMenuTesting
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PantallasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuCotizacion = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuDocumentos = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuPedidos = New System.Windows.Forms.ToolStripMenuItem()
        Me.mnuSalir = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.PantallasToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(464, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(12, 20)
        '
        'PantallasToolStripMenuItem
        '
        Me.PantallasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.mnuCotizacion, Me.mnuPedidos, Me.mnuDocumentos, Me.mnuSalir})
        Me.PantallasToolStripMenuItem.Name = "PantallasToolStripMenuItem"
        Me.PantallasToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.PantallasToolStripMenuItem.Text = "Pantallas"
        '
        'mnuCotizacion
        '
        Me.mnuCotizacion.Name = "mnuCotizacion"
        Me.mnuCotizacion.Size = New System.Drawing.Size(216, 22)
        Me.mnuCotizacion.Text = "Cotización"
        '
        'mnuDocumentos
        '
        Me.mnuDocumentos.Name = "mnuDocumentos"
        Me.mnuDocumentos.Size = New System.Drawing.Size(216, 22)
        Me.mnuDocumentos.Text = "Recepción de Documentos"
        '
        'mnuPedidos
        '
        Me.mnuPedidos.Name = "mnuPedidos"
        Me.mnuPedidos.Size = New System.Drawing.Size(216, 22)
        Me.mnuPedidos.Text = "Pedidos"
        '
        'mnuSalir
        '
        Me.mnuSalir.Name = "mnuSalir"
        Me.mnuSalir.Size = New System.Drawing.Size(216, 22)
        Me.mnuSalir.Text = "Salir"
        '
        'frmMenuTesting
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(464, 266)
        Me.Controls.Add(Me.MenuStrip1)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "frmMenuTesting"
        Me.Text = "SETRA - Testing"
        Me.WindowState = System.Windows.Forms.FormWindowState.Maximized
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents MenuStrip1 As System.Windows.Forms.MenuStrip
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PantallasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuCotizacion As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuPedidos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuDocumentos As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents mnuSalir As System.Windows.Forms.ToolStripMenuItem
End Class
