﻿Imports ComSEToursBE2
Imports ComSEToursBL2
Public Class frmMantDetServiciosProveedorDato
    Public strModoEdicion As Char
    Public strIDProveedor As String = ""
    Public strIDServicio As String = ""
    Public strDescServicio As String = ""
    Public strIDServicio_Det As Int32 = 0
    Public strIDTipoProv As String = ""
    Public strDescripcion As String = ""
    Public intIDServicio_Det_V As Int32
    Public strIDTipoOper As String = ""
    'Dim strCodxTriple As String = ""

    Public strIDServicioNue As String = ""
    Public strTipoNue As String = ""
    Public strAnioNue As String = ""

    Public FormRef As frmMantDetServiciosProveedor
    Public FormRefMantServiciosProveedor As frmMantServiciosProveedor
    Public blnServicioVarios As Boolean
    Public blnEsServicioInternacional As Boolean = False
    Dim blnNuevoEnabled As Boolean
    Dim blnGrabarEnabled As Boolean
    Dim blnDeshacerEnabled As Boolean
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnImprimirEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean

    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False

    Dim blnLoad As Boolean = False
    Dim blnCargaIniStruct As Boolean
    Public dttCostos As New DataTable("Costos")
    Public dttRangosAPT As New DataTable("RangosAPT")
    Public blnCotizacion As Boolean = False
    Dim blnPintarCorrel As Boolean = True
    Dim blnFormClosed As Boolean = False
    Public blnBloquearEdit As Boolean = False
    Dim blnCambioServRel As Boolean = False
    Dim blnCambioServRelAcepto As Boolean = False
    Dim strAnioPreciosCopia As String = ""
    Dim blnConAlojamiento As Boolean = False
    Dim dcValoresCostos As Dictionary(Of String, Double)
    Dim dcValoresRangosAPT As Dictionary(Of String, Double)
    Public blnIDServicio_DetRelIgual As Boolean = False

    Public sglTipoCambioPais As Single = 0
    Public strCoMonedaPais As String = ""
    Public strCoSimboloMonedaPais As String = ""

    Dim dttCuentasContables As DataTable
    Dim dttTipoOperaciones As DataTable

    Public strCoCeCos As String = ""

    Private Structure stCostos
        Dim ID As Byte
        Dim PaxDesde As Int16
        Dim PaxHasta As Int16
        Dim Monto As Double
        Dim strAccion As Char
    End Structure
    Dim objLst As New List(Of stCostos)

    Private Structure stRangosAPT
        Dim ID As Byte
        Dim PaxDesde As Int16
        Dim PaxHasta As Int16
        Dim Monto As Double
        Dim strAccion As Char
        Dim IDTemporada As Integer
    End Structure
    Dim objLstAPT As New List(Of stRangosAPT)

    Private Structure stDescRangosAPT
        Dim IDTemporada As Integer
        Dim DescripAlter As String
        Dim SingleSupplement As Double
        ''PPMG20160315
        Dim PoliticaLiberado As Boolean
        Dim Liberado As Byte
        Dim TipoLib As String
        Dim MaximoLiberado As Byte
        Dim MontoL As Double
    End Structure
    Dim objLstDescAPT As New List(Of stDescRangosAPT)

    Dim blnCambiosDescripcion As Boolean = False
    Dim blnDoubleClickLvw As Boolean = False
    Dim intIDTemporada As Integer = 0

    Dim frmmain As Object = Nothing
    Private Sub AgregarEditarValorStruct(ByVal vbytNroFila As Byte, _
                                 ByVal vstrAccion As String)

        'If Not blnLoad Then Exit Sub
        If dgv.Rows(vbytNroFila).IsNewRow Then Exit Sub

        ErrPrv.SetError(dgv, "")

        Dim strAccion As String = "" 'N,B,M
        Dim bytID As Int16 = 0

        If strModoEdicion = "N" Then
            bytID = intMayorIddetGenerado()
            If Not dgv.Item("Correlativo", vbytNroFila).Value Is Nothing Then
                If dgv.Item("Correlativo", vbytNroFila).Value.ToString <> "" Then
                    bytID = dgv.Item("Correlativo", vbytNroFila).Value.ToString
                End If
            Else
                bytID = intMayorIddetGenerado()
            End If

        ElseIf strModoEdicion = "E" Then
            If Not dgv.Item("Correlativo", vbytNroFila).Value Is Nothing Then
                bytID = If(dgv.Item("Correlativo", vbytNroFila).Value.ToString = "", "0", dgv.Item("Correlativo", vbytNroFila).Value.ToString)
                If bytID = 0 Then
                    bytID = intMayorIddetGenerado()
                End If
            Else
                bytID = intMayorIddetGenerado()
            End If
        End If
        Dim RegValor As stCostos
        Try

            Dim intInd As Int16 = 0
            For Each ST As stCostos In objLst

                If ST.ID = bytID Then
                    If ST.strAccion <> "N" And ST.strAccion <> "B" Then
                        strAccion = "M"
                    Else
                        strAccion = "N"
                    End If
                    objLst.RemoveAt(intInd)
                    Exit For
                End If

                intInd += 1
            Next
            If vstrAccion <> "B" Then 'si no es borrado de fila
            Else
                strAccion = vstrAccion
            End If

            RegValor = New stCostos
            With RegValor
                .ID = bytID
                dgv.Item("Correlativo", vbytNroFila).Value = bytID
                If Not dgv.Item("PaxDesde", vbytNroFila).Value Is Nothing Then
                    .PaxDesde = If(dgv.Item("PaxDesde", vbytNroFila).Value.ToString = "", "0", dgv.Item("PaxDesde", vbytNroFila).Value.ToString)
                Else
                    .PaxDesde = 0
                End If
                If Not dgv.Item("PaxHasta", vbytNroFila).Value Is Nothing Then
                    .PaxHasta = If(dgv.Item("PaxHasta", vbytNroFila).Value.ToString = "", "0", dgv.Item("PaxHasta", vbytNroFila).Value.ToString)
                Else
                    .PaxHasta = 0
                End If
                If Not dgv.Item("Monto", vbytNroFila).Value Is Nothing Then
                    .Monto = If(dgv.Item("Monto", vbytNroFila).Value.ToString = "", "0", dgv.Item("Monto", vbytNroFila).Value.ToString)
                Else
                    .Monto = 0
                End If


                If strAccion = "" Then 'si no existe en struct
                    If Not blnCargaIniStruct Then
                        .strAccion = "N"
                    Else
                        .strAccion = strAccion
                    End If

                Else
                    .strAccion = strAccion
                End If

            End With

            objLst.Add(RegValor)

        Catch ex As Exception
            Throw

        End Try
    End Sub
    Public Function intMayorIddetGenerado() As Int16
        Try

            Dim intMayor As Int16 = 0
            For Each ST As stCostos In objLst

                Dim intID As Int16 = ST.ID

                If intID > intMayor Then intMayor = intID

            Next
            Return intMayor + 1
        Catch ex As Exception
            Throw
        End Try
    End Function

    '-----------------------------------------------------------------------------------------------------

    Private Sub AgregarEditarValorStructAPT(ByVal vbytNroFila As Byte, _
                             ByVal vstrAccion As String)

        'If Not blnLoad Then Exit Sub
        If dgvAPT.Rows(vbytNroFila).IsNewRow Then Exit Sub

        ErrPrv.SetError(dgvAPT, "")

        Dim strAccion As String = "" 'N,B,M
        Dim bytID As Int16 = 0

        If strModoEdicion = "N" Then
            bytID = intMayorIddetGeneradoAPT()
            If Not dgvAPT.Item("Correlativo2", vbytNroFila).Value Is Nothing Then
                If dgvAPT.Item("Correlativo2", vbytNroFila).Value.ToString <> "" Then
                    bytID = dgvAPT.Item("Correlativo2", vbytNroFila).Value.ToString
                End If
            Else
                bytID = intMayorIddetGeneradoAPT()
            End If

        ElseIf strModoEdicion = "E" Then
            If Not dgvAPT.Item("Correlativo2", vbytNroFila).Value Is Nothing Then
                bytID = If(dgvAPT.Item("Correlativo2", vbytNroFila).Value.ToString = "", "0", dgvAPT.Item("Correlativo2", vbytNroFila).Value.ToString)
                If bytID = 0 Then
                    bytID = intMayorIddetGeneradoAPT()
                End If
            Else
                bytID = intMayorIddetGeneradoAPT()
            End If
        End If
        Dim RegValor As stRangosAPT
        Try

            Dim intInd As Int16 = 0
            For Each ST As stRangosAPT In objLstAPT
                If ST.IDTemporada = intIDTemporada Then
                    If ST.ID = bytID Then
                        If ST.strAccion <> "N" And ST.strAccion <> "B" Then
                            strAccion = "M"
                        Else
                            strAccion = "N"
                        End If
                        objLstAPT.RemoveAt(intInd)
                        Exit For
                    End If
                End If
                intInd += 1
            Next
            If vstrAccion <> "B" Then 'si no es borrado de fila
            Else
                strAccion = vstrAccion
            End If

            RegValor = New stRangosAPT
            With RegValor
                .IDTemporada = intIDTemporada
                .ID = bytID
                dgvAPT.Item("Correlativo2", vbytNroFila).Value = bytID
                If Not dgvAPT.Item("PaxDesde2", vbytNroFila).Value Is Nothing Then
                    .PaxDesde = If(dgvAPT.Item("PaxDesde2", vbytNroFila).Value.ToString = "", "0", dgvAPT.Item("PaxDesde2", vbytNroFila).Value.ToString)
                Else
                    .PaxDesde = 0
                End If
                If Not dgvAPT.Item("PaxHasta2", vbytNroFila).Value Is Nothing Then
                    .PaxHasta = If(dgvAPT.Item("PaxHasta2", vbytNroFila).Value.ToString = "", "0", dgvAPT.Item("PaxHasta2", vbytNroFila).Value.ToString)
                Else
                    .PaxHasta = 0
                End If
                If Not dgvAPT.Item("Monto2", vbytNroFila).Value Is Nothing Then
                    .Monto = If(dgvAPT.Item("Monto2", vbytNroFila).Value.ToString = "", "0", dgvAPT.Item("Monto2", vbytNroFila).Value.ToString)
                Else
                    .Monto = 0
                End If


                If strAccion = "" Then 'si no existe en struct
                    If Not blnCargaIniStruct Then
                        .strAccion = "N"
                    Else
                        .strAccion = strAccion
                    End If

                Else
                    .strAccion = strAccion
                End If

            End With

            objLstAPT.Add(RegValor)
            ''AgregarEditarValorStructDescAPT(intIDTemporada, txtDescAlter.Text.Trim, txtSinpIndi.Text)
            AgregarEditarValorControlAPT(intIDTemporada)

        Catch ex As Exception
            Throw

        End Try
    End Sub
    Public Function intMayorIddetGeneradoAPT() As Int16
        Try
            Dim intMayor As Int16 = 0
            For Each ST As stRangosAPT In objLstAPT
                If ST.IDTemporada = intIDTemporada Then
                    Dim intID As Int16 = ST.ID
                    If intID > intMayor Then intMayor = intID
                End If
            Next
            Return intMayor + 1
        Catch ex As Exception
            Throw
        End Try
    End Function

    '-----------------------------------------------------------------------------------------------------


    Private Sub frmMantUsuariosDato_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated
        'AddHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'btnNuevo.Enabled = blnNuevoEnabled
        'AddHandler frmMain.btnGrabar.Click, AddressOf pGrabar
        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'btnGrabar.Enabled = blnGrabarEnabled
        'AddHandler frmMain.btnDeshacer.Click, AddressOf pDeshacer
        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        'btnDeshacer.Enabled = blnDeshacerEnabled
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        'frmMain.btnSalir.Enabled = blnSalirEnabled
        'btnSalir.Enabled = blnSalirEnabled
        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        'btnImprimir.Enabled = blnImprimirEnabled
        'frmMain.btnEditar.Enabled = blnEditarEnabled
        'btnEditar.Enabled = blnEditarEnabled
        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        'btnEliminar.Enabled = blnEliminarEnabled

        cboCuentaContable.DrawMode = DrawMode.OwnerDrawFixed
        cboIdTipoOC.DrawMode = DrawMode.OwnerDrawFixed
        AddHandler cboCuentaContable.DrawItem, AddressOf cboCuentaContable_DrawItem
        AddHandler cboIdTipoOC.DrawItem, AddressOf cboIdTipoOC_DrawItem
        blnLoad = True
        pCambiarTitulosToolBar(Me)
    End Sub
    Private Sub frmMantBancosDato_KeyDown(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles Me.KeyDown
        Select Case e.KeyCode
            Case Keys.F6
                If Not blnCotizacion Then pGrabar(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub

    Private Sub frmMantUsuariosDato_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnNuevo.Click, AddressOf pNuevo
        'RemoveHandler frmMain.btnGrabar.Click, AddressOf pGrabar
        'RemoveHandler frmMain.btnDeshacer.Click, AddressOf pDeshacer

        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir

        '--
        cboCuentaContable.DrawMode = DrawMode.Normal
        cboIdTipoOC.DrawMode = DrawMode.Normal
        RemoveHandler cboCuentaContable.DrawItem, AddressOf cboCuentaContable_DrawItem
        RemoveHandler cboIdTipoOC.DrawItem, AddressOf cboIdTipoOC_DrawItem
    End Sub
    Private Sub CrearListView()

        With lvw
            .View = View.Details
            .FullRowSelect = True
            '.GridLines = True
            .LabelEdit = False
            .HideSelection = False

            .Columns.Clear()
            .Columns.Add("Descripción", 250, HorizontalAlignment.Left)

        End With
    End Sub

    Private Sub BloquearCtrlsConsulta()
        If Not blnBloquearEdit Then Exit Sub

        dgv.Columns("PaxDesde").ReadOnly = True
        dgv.Columns("PaxHasta").ReadOnly = True
        dgv.Columns("Monto").ReadOnly = True
        dgv.Columns("Sel").ReadOnly = True

        dgvAPT.Columns("PaxDesde2").ReadOnly = True
        dgvAPT.Columns("PaxHasta2").ReadOnly = True
        dgvAPT.Columns("Monto2").ReadOnly = True
        dgvAPT.Columns("Sel2").ReadOnly = True

        btnBuscarDetServicio.Enabled = False

        Me.Text = "Consulta de Detalle Servicio: " & lblTitulo.Text
        blnGrabarEnabled = False
        'frmMain.btnGrabar.Enabled = False

        'grb.Enabled = False
        For Each ctrl As Object In grb.Controls
            If Not ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                ctrl.enabled = False
            End If
        Next

        'grb.Enabled = False
        For Each ctrl As Object In tbgRangosAPT.Controls
            If Not ctrl.GetType Is GetType(System.Windows.Forms.DataGridView) Then
                ctrl.enabled = False
            End If
        Next

    End Sub

    Private Sub CargarCombos()
        Try
            'Dim objBLMon As New clsTablasApoyoBN.clsMonedaBN
            'pCargaCombosBox(cboMoneda, objBLMon.ConsultarCbo())

            dttTipoOperaciones = New DataTable

            'Dim objBLTOpe As New clsTablasApoyoBN.clsTipoOperacContabBN
            'dttTipoOperaciones = objBLTOpe.ConsultarCbo()
            ''pCargaCombosBox(cboIdTipoOC, objBLTOpe.ConsultarCbo())
            'pCargaCombosBox(cboIdTipoOC, dttTipoOperaciones)


            Dim dttTipoLib As New DataTable("TipoLiberado")
            dttTipoLib.Columns.Add("Codigo")
            dttTipoLib.Columns.Add("Descripcion")

            dttTipoLib.Rows.Add("H", "HABITACIONES")
            dttTipoLib.Rows.Add("P", "PASAJEROS(PAX)")

            pCargaCombosBox(cboTipoLib, dttTipoLib)

            ''PPMG20160315
            pCargaCombosBox(cboTipoLibAPT, dttTipoLib)

            'CargarCboCodTriple()


            'TEMP
            'Dim dttTipoDesayuno As New DataTable("TipoDesayuno")
            'dttTipoDesayuno.Columns.Add("Codigo")
            'dttTipoDesayuno.Columns.Add("Descripcion")

            'dttTipoDesayuno.Rows.Add("NA", "NO APLICA")
            'dttTipoDesayuno.Rows.Add("D1", "AMERICANO")
            'dttTipoDesayuno.Rows.Add("D2", "CONTINENTAL")
            'dttTipoDesayuno.Rows.Add("D3", "BUFFET")

            Dim objBNDes As New clsTablasApoyoBN.clsDesayunosBN
            pCargaCombosBox(cboTipoDesayuno, objBNDes.ConsultarCbo(False))
            '---

            Dim dttTipoGas As New DataTable("TipoGasto")
            With dttTipoGas
                .Columns.Add("Codigo")
                .Columns.Add("Descripcion")

                .Rows.Add("O", "OPERATIVO")
                .Rows.Add("G", "GUÍA")
                .Rows.Add("T", "TRANSPORTISTA")

            End With
            pCargaCombosBox(cboTipoGasto, dttTipoGas)

            Dim objBNHab As New clsTablasApoyoBN.clsHabitTripleBN
            pCargaCombosBox(cboHabitTriple, objBNHab.ConsultarCbo, True)

            'Dim objBNCta As New clsTablasApoyoBN.clsPlanCuentasBN
            'pCargaCombosBox(cboCuentaContable, objBNCta.COnsultarCbo(), True)

            Dim objCenCosBN As New clsCentroCostosBN
            If blnServicioVarios Then
                pCargaCombosBox(cboCentroCostos, objCenCosBN.ConsultarListCbo_ServiosVarios)
            Else
                pCargaCombosBox(cboCentroCostos, objCenCosBN.ConsultarListCbo)
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarCboCodTriple()
        Try
            If txtAnio.Text <> "" Then
                Dim objBLDetSer As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                pCargaCombosBox(cboCodxTriple, objBLDetSer.ConsultarCodxTripleCbo(strIDServicio, txtAnio.Text))
            End If
            'If strCodxTriple <> "" Then cboCodxTriple.SelectedValue = strCodxTriple
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub frmMantUsuariosDato_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        dgv.DefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Regular)
        Try
            'dgvAPT.Columns(0).Visible = False
            'combo cuentas contables

            cboCuentaContable.DrawMode = DrawMode.OwnerDrawFixed
            cboIdTipoOC.DrawMode = DrawMode.OwnerDrawFixed
            AddHandler cboCuentaContable.DrawItem, AddressOf cboCuentaContable_DrawItem
            AddHandler cboIdTipoOC.DrawItem, AddressOf cboIdTipoOC_DrawItem

            txtAnio.BackColor = gColorTextBoxObligat
            txtDescripcion.BackColor = gColorTextBoxObligat
            If Not blnEsServicioInternacional Then
                txtMonto_sgls.BackColor = gColorTextBoxObligat
                txtMonto_dbls.BackColor = gColorTextBoxObligat
                txtMonto_sgl.BackColor = gColorTextBoxObligat
                txtMonto_dbl.BackColor = gColorTextBoxObligat
            Else
                txtMonto_sgls.BackColor = Color.White
                txtMonto_dbls.BackColor = Color.White
                txtMonto_sgl.BackColor = Color.White
                txtMonto_dbl.BackColor = Color.White
            End If

            txtAnio.Text = Today.Year

            With dttCostos.Columns
                .Add("Correlativo")
                .Add("PaxDesde")
                .Add("PaxHasta")
                .Add("Monto")
            End With

            With dttRangosAPT.Columns
                .Add("Correlativo")
                .Add("PaxDesde")
                .Add("PaxHasta")
                .Add("Monto")
            End With


            CargarSeguridad()
            CargarCombos()
            CargarCombo_Temporada()
            CrearListView()
            ActivarDesactivarBotones()
            If strIDServicio_Det = 0 Then
                ModoNuevo()
            Else
                ModoEditar()
                'blnCargaIniStruct = True
                CargarControles()

                blnCargaIniStruct = True
                For Each FilaDgv As DataGridViewRow In dgv.Rows
                    AgregarEditarValorStruct(FilaDgv.Index, "")
                Next

                ''For Each FilaDgvAPT As DataGridViewRow In dgvAPT.Rows
                ''    AgregarEditarValorStructAPT(FilaDgvAPT.Index, "")
                ''Next

                blnCargaIniStruct = False

                dgvAPT.Columns("Monto2").HeaderText = "Monto (US$)"
                'dgvAPT.Columns(0).Visible = False

                lblTCEtiq.Enabled = False
                txtTC.Enabled = False

                Dim sglTC As Single = 0
                If txtTC.Text <> "" Then
                    sglTC = Convert.ToSingle(txtTC.Text)
                End If
                'If txtTC.Text <> "0.00" Then
                If sglTC <> 0 Then
                    lblTCEtiq.Enabled = True
                    txtTC.Enabled = True
                    'dgv.Columns("Monto").HeaderText = "Monto (S/.)"
                    dgv.Columns("Monto").HeaderText = "Monto (" & strCoSimboloMonedaPais & ")"
                Else
                    dgv.Columns("Monto").HeaderText = "Monto (US$)"
                End If
            End If

            ActivarControlesMontos()

            If strIDTipoProv = gstrTipoProveeHoteles Or strIDTipoProv = gstrTipoProveeRestaurantes Then
                chkTarifario.Enabled = True
                lblCostosEtiq.Text = "Costos:"

                If strIDTipoProv = gstrTipoProveeRestaurantes Then
                    chkRangos.Visible = True
                    dgv.Visible = True
                    chkSelTodos.Visible = True
                    btnEliminarFila.Visible = True
                    btnCalculo.Visible = True
                Else
                    chkRangos.Visible = False
                    dgv.Visible = False
                    chkSelTodos.Visible = False
                    btnEliminarFila.Visible = False
                    btnCalculo.Visible = False
                End If


            Else
                chkTarifario.Enabled = False

                'lblCodigoEtiq.Visible = False
                'txtCodigo.Visible = False

                lblCostosEtiq.Text = "Costos por persona:"

                lblTipoGastoEtiq.Visible = True
                cboTipoGasto.Visible = True
                If strModoEdicion = "N" Then
                    'cboTipoGasto.SelectedValue = "O"
                    If strIDTipoProv = gstrTipoProveeGuias Then
                        cboTipoGasto.SelectedValue = "G"
                    ElseIf strIDTipoProv = gstrTipoProveeTransportista Then
                        cboTipoGasto.SelectedValue = "T"
                    Else
                        cboTipoGasto.SelectedValue = "O"
                    End If

                    lblTCEtiq.Enabled = True
                    txtTC.Enabled = True
                End If


                chkRangos.Visible = True
                dgv.Visible = True
                chkSelTodos.Visible = True
                btnEliminarFila.Visible = True
                btnCalculo.Visible = True

                PintarCorrelativo()
            End If
            'If strIDTipoProv = gstrTipoProveeHoteles Or strIDTipoProv = gstrTipoProveeRestaurantes Then
            If strIDTipoProv = gstrTipoProveeHoteles Or strIDTipoProv = gstrTipoProveeRestaurantes Then
                chkPlanAlimenticio.Visible = True

                lblIncluyeEtiq.Visible = True
                chkDesayuno.Visible = True
                chkLonche.Visible = True
                chkAlmuerzo.Visible = True
                chkCena.Visible = True
                chkVerDetaTipo.Visible = True

                'lblEtiqCodTarifa.Visible = False
                txtCodTarifa.Visible = False
            Else
                If strIDTipoProv = gstrTipoProveeOperadores Then
                    chkPlanAlimenticio.Visible = True

                    lblIncluyeEtiq.Visible = True
                    chkDesayuno.Visible = True
                    chkLonche.Visible = True
                    chkAlmuerzo.Visible = True
                    chkCena.Visible = True
                Else
                    chkPlanAlimenticio.Visible = False

                    lblIncluyeEtiq.Visible = False
                    chkDesayuno.Visible = False
                    chkLonche.Visible = False
                    chkAlmuerzo.Visible = False
                    chkCena.Visible = False
                End If
                'chkPlanAlimenticio.Visible = False

                'lblIncluyeEtiq.Visible = False
                'chkDesayuno.Visible = False
                'chkLonche.Visible = False
                'chkAlmuerzo.Visible = False
                'chkCena.Visible = False

                If strIDTipoProv = gstrTipoProveeOperadores And chkConAlojamiento.Checked Then
                    chkVerDetaTipo.Visible = True
                    'txtCodTarifa.Enabled = chkVerDetaTipo.Enabled
                Else
                    If strIDTipoProv <> "" And chkConAlojamiento.Checked Then
                        chkVerDetaTipo.Visible = True
                        'lblEtiqCodTarifa.Visible = True
                        txtCodTarifa.Visible = True
                    Else
                        chkVerDetaTipo.Visible = False
                        'lblEtiqCodTarifa.Visible = False
                        txtCodTarifa.Visible = False
                    End If
                End If


            End If
            If strIDTipoProv = gstrTipoProveeOperadores Then
                chkPlanAlimenticio.Visible = True
            End If
            If strIDTipoProv = gstrTipoProveeHoteles And chkPlanAlimenticio.CheckState = CheckState.Unchecked Then

                lblCodxTripleEtiq.Visible = True
                cboCodxTriple.Visible = True

                lblDefinicionTripleEtiq.Visible = True
                cboHabitTriple.Visible = True
            ElseIf strIDTipoProv = gstrTipoProveeOperadores Then
                lblCodxTripleEtiq.Visible = False
                cboCodxTriple.Visible = False

                lblDefinicionTripleEtiq.Visible = True
                cboHabitTriple.Visible = True
            Else
                lblCodxTripleEtiq.Visible = False
                cboCodxTriple.Visible = False

                lblDefinicionTripleEtiq.Visible = False
                cboHabitTriple.Visible = False
            End If
            blnCambiosDescripcion = False

            BloquearCtrlsConsulta()

            If (strIDTipoProv = gstrTipoProveeHoteles) Or (strIDTipoProv = gstrTipoProveeOperadores And chkConAlojamiento.Checked) Then
                tbgRangosAPT.Parent = tbc
            Else
                tbgRangosAPT.Parent = Nothing
            End If

            If blnServicioVarios Then
                Me.WindowState = FormWindowState.Maximized
                Me.MaximizeBox = True
                Me.MinimizeBox = True
                grb.Dock = DockStyle.None
                grb.Location = New System.Drawing.Point(0, 80)

                grb.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right),  _
                    System.Windows.Forms.AnchorStyles)

                Toolbar.Visible = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub
    Private Sub ActivarControlesMontos()
        Try
            If strIDTipoProv = gstrTipoProveeHoteles Then
                'chkPoliticaLiberado.Enabled = True
                lblTipoDesayunoEtiq.Visible = True
                cboTipoDesayuno.Visible = True

                'lblMontoSimpleEtiq.Text = "Habitación Simple S/."
                lblMontoSimpleEtiq.Text = "Habitación Simple " & strCoSimboloMonedaPais
                lblMontoDobleEtiq.Text = "Habitación Doble " & strCoSimboloMonedaPais
                lblMontoTripleEtiq.Text = "Habitación Triple " & strCoSimboloMonedaPais

                cboCodxTriple.Visible = True
                lblCodxTripleEtiq.Visible = True

                chkConAlojamiento.Visible = False
                lblDiferSSEtiq.Visible = False
                txtDiferSS.Visible = False
                lblDiferSTEtiq.Visible = False
                txtDiferST.Visible = False

            Else
                'chkPoliticaLiberado.Enabled = False
                lblTipoDesayunoEtiq.Visible = False
                cboTipoDesayuno.Visible = False

                'lblMontoSimpleEtiq.Text = "Monto S/."
                lblMontoSimpleEtiq.Text = "Monto " & strCoSimboloMonedaPais
                lblMontoSimpleEtiq2.Text = "Monto US$"

                lblMontoDobleEtiq.Visible = False
                txtMonto_dbls.Visible = False
                lblMontoDobleEtiq2.Visible = False
                txtMonto_dbl.Visible = False

                lblMontoTripleEtiq.Visible = False
                txtMonto_tri.Visible = False
                lblMontoTripleEtiq2.Visible = False
                txtMonto_tris.Visible = False

                cboCodxTriple.Visible = False
                lblCodxTripleEtiq.Visible = False

                If strIDTipoProv = gstrTipoProveeRestaurantes Then
                    chkConAlojamiento.Visible = False
                    chkConAlojamiento.Enabled = False
                    lblDiferSSEtiq.Visible = False
                    txtDiferSS.Visible = False
                    lblDiferSTEtiq.Visible = False
                    txtDiferST.Visible = False
                Else
                    chkConAlojamiento.Visible = True
                    chkConAlojamiento.Enabled = True
                    lblDiferSSEtiq.Visible = True
                    txtDiferSS.Visible = True
                    lblDiferSTEtiq.Visible = True
                    txtDiferST.Visible = True
                End If
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Function blnValidarIngresos() As Boolean


        Dim blnOk As Boolean = True

        If txtAnio.Text.Trim = "" Then
            ErrPrv.SetError(txtAnio, "Debe ingresar Año")
            blnOk = False
        End If

        If txtDescripcion.Text.Trim = "" Then
            ErrPrv.SetError(txtDescripcion, "Debe ingresar Descripción")
            blnOk = False
        End If

        Dim blnDgvVacio As Boolean = True
        For Each FilaDgv As DataGridViewRow In dgv.Rows
            If Not FilaDgv.IsNewRow Then
                blnDgvVacio = False
                Exit For
            End If
        Next

        Dim blnDgvVacioAPT As Boolean = True
        For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
            If Not FilaDgv.IsNewRow Then
                blnDgvVacioAPT = False
                Exit For
            End If
        Next

        If (strIDTipoProv = gstrTipoProveeHoteles Or strIDTipoProv = gstrTipoProveeRestaurantes) And blnDgvVacio = True Then
            If Not blnEsServicioInternacional Then
                If Convert.ToDouble(txtMonto_sgl.Text) = 0 And Convert.ToDouble(txtMonto_sgls.Text) = 0 Then
                    If strIDTipoProv = gstrTipoProveeHoteles Then
                        'ErrPrv.SetError(txtMonto_sgl, "Debe ingresar Costo: Habitación Simple $ o Habitación Simple S/.")
                        ErrPrv.SetError(txtMonto_sgl, "Debe ingresar Costo: Habitación Simple US$ o Habitación Simple " & strCoSimboloMonedaPais)

                    Else
                        'ErrPrv.SetError(txtMonto_sgl, "Debe ingresar Costo: Monto $ o Monto S/.")
                        ErrPrv.SetError(txtMonto_sgl, "Debe ingresar Costo: Monto US$ o Monto " & strCoSimboloMonedaPais)
                    End If
                    txtMonto_sgl.Focus()
                    blnOk = False
                End If
            End If
        End If


        'If lblDefinicionTriple.Visible Then
        '    If cboHabitTriple.Text.Trim = "" Then
        '        ErrPrv.SetError(cboHabitTriple, "Debe seleccionar un tipo de habitación triple.")
        '        cboHabitTriple.Focus()
        '        blnOk = False
        '        'Else
        '        '    If cboHabitTriple.SelectedValue = "001" Then
        '        '        ErrPrv.SetError(cboHabitTriple, "La seleccion de tipo de habitación triple no es válida")
        '        '        cboHabitTriple.Focus()
        '        '        blnOk = False
        '        '    End If
        '    End If
        'End If

        If strIDTipoProv = gstrTipoProveeHoteles Then
            If chkVerDetaTipo.Checked Then
                If txtDetaTipo.Text.Trim = "" Then
                    ErrPrv.SetError(txtDetaTipo, "Debe ingresar Detalle de Variante")
                    blnOk = False
                End If
            End If
            If blnDgvVacio = True Then
                If Not chkPlanAlimenticio.Checked Then
                    If Not blnEsServicioInternacional Then
                        If Convert.ToDouble(txtMonto_dbl.Text) = 0 And Convert.ToDouble(txtMonto_dbls.Text) = 0 Then
                            'ErrPrv.SetError(txtMonto_dbl, "Debe ingresar Costo: Habitación Doble $ o Habitación Doble S/.")
                            ErrPrv.SetError(txtMonto_dbl, "Debe ingresar Costo: Habitación Doble US$ o Habitación Doble " & strCoSimboloMonedaPais)
                            txtMonto_dbls.Focus()
                            blnOk = False
                        End If

                        If Convert.ToDouble(txtMonto_tri.Text) = 0 And Convert.ToDouble(txtMonto_tris.Text) = 0 And cboCodxTriple.Text = "" Then
                            'If MessageBox.Show("Debe ingresar Costo: Habitación Triple $ o Habitación Triple S/. o seleccionar un Cod. Monto Triple, Desea grabar de todas formas?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                            If MessageBox.Show("Debe ingresar Costo: Habitación Triple US$ o Habitación Triple " & strCoSimboloMonedaPais & " o seleccionar un Cod. Monto Triple, Desea grabar de todas formas?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                                blnOk = False
                            End If
                            txtMonto_dbls.Focus()
                        End If
                    End If
                End If
            Else
                For Each FilaDgv As DataGridViewRow In dgv.Rows
                    If Not FilaDgv.IsNewRow Then
                        If FilaDgv.Cells("PaxDesde").Value Is Nothing Then
                            ErrPrv.SetError(dgv, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                            blnOk = False
                            Exit For
                        End If
                        If Convert.ToInt16(FilaDgv.Cells("PaxDesde").Value) = 0 Then
                            ErrPrv.SetError(dgv, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                            blnOk = False
                            Exit For
                        End If

                        If FilaDgv.Cells("PaxHasta").Value Is Nothing Then
                            ErrPrv.SetError(dgv, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                            blnOk = False
                            Exit For
                        End If
                        If Convert.ToInt16(FilaDgv.Cells("PaxHasta").Value) = 0 Then
                            ErrPrv.SetError(dgv, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                            blnOk = False
                            Exit For
                        End If

                        If FilaDgv.Cells("Monto").Value Is Nothing Then
                            ErrPrv.SetError(dgv, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                            blnOk = False
                            Exit For
                        End If
                        If Convert.ToDouble(FilaDgv.Cells("Monto").Value) = 0 Then
                            ErrPrv.SetError(dgv, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                            blnOk = False
                            Exit For
                        End If
                    End If
                Next

            End If

            If cboTipoDesayuno.Text = "" Then
                ErrPrv.SetError(cboTipoDesayuno, "Debe seleccionar Tipo de Desayuno")
                blnOk = False
            Else
                If cboTipoDesayuno.SelectedValue.ToString = "00" Then
                    ErrPrv.SetError(cboTipoDesayuno, "Debe seleccionar Tipo de Desayuno")
                    blnOk = False
                End If
            End If

        End If

        'rangos apt
        If blnDgvVacioAPT = False Then
            For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
                If Not FilaDgv.IsNewRow Then
                    If FilaDgv.Cells("PaxDesde2").Value Is Nothing Then
                        ErrPrv.SetError(dgvAPT, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                        blnOk = False
                        Exit For
                    End If
                    If Convert.ToInt16(FilaDgv.Cells("PaxDesde2").Value) = 0 Then
                        ErrPrv.SetError(dgvAPT, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                        blnOk = False
                        Exit For
                    End If

                    If FilaDgv.Cells("PaxHasta2").Value Is Nothing Then
                        ErrPrv.SetError(dgvAPT, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                        blnOk = False
                        Exit For
                    End If
                    If Convert.ToInt16(FilaDgv.Cells("PaxHasta2").Value) = 0 Then
                        ErrPrv.SetError(dgvAPT, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                        blnOk = False
                        Exit For
                    End If

                    If FilaDgv.Cells("Monto2").Value Is Nothing Then
                        ErrPrv.SetError(dgvAPT, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                        blnOk = False
                        Exit For
                    End If
                    If Convert.ToDouble(FilaDgv.Cells("Monto2").Value) = 0 Then
                        ErrPrv.SetError(dgvAPT, "Debe ingresar todos los valores en la columnas Desde, Hasta, Monto")
                        blnOk = False
                        Exit For
                    End If
                End If
            Next
        End If

        If strIDTipoProv = gstrTipoProveeOperadores Then
            If chkVerDetaTipo.Checked Then
                If txtCodTarifa.Text.Trim = "" Then
                    ErrPrv.SetError(txtCodTarifa, "Debe ingresar un código de tarifa")
                    blnOk = False
                End If
            End If
        End If

        If Convert.ToDouble(txtMonto_sgls.Text) > 0 Then
            If Convert.ToDouble(txtTC.Text) = 0 Then
                ErrPrv.SetError(txtTC, "Debe ingresar Tipo de Cambio")
                txtTC.Focus()
                blnOk = False
            End If
        End If



        If strIDTipoProv = gstrTipoProveeHoteles Or strIDTipoProv = gstrTipoProveeRestaurantes Then

            'If txtCodigo.Text = "" Then
            '    ErrPrv.SetError(txtCodigo, "Debe ingresar Código")
            '    txtCodigo.Focus()
            '    blnOk = False
            'End If
        Else
            If cboTipoGasto.Text = "" Then
                ErrPrv.SetError(cboTipoGasto, "Debe seleccionar Tipo de Gasto")
                cboTipoGasto.Focus()
                blnOk = False
            End If
        End If


        If chkPoliticaLiberado.Checked And chkPoliticaLibNivelDetalle.Checked Then
            If txtLiberado.Text.Trim = "" Then
                ErrPrv.SetError(txtLiberado, "Debe ingresar Cantidad")
                txtLiberado.Focus()
                blnOk = False
            End If
            If cboTipoLib.Text = "" Then
                ErrPrv.SetError(cboTipoLib, "Debe seleccionar Tipo de Liberado")
                cboTipoLib.Focus()
                blnOk = False
            End If
            'If txtMontoL.Text.Trim = "" Then
            '    ErrPrv.SetError(txtMontoL, "Debe ingresar Monto Liberado")
            '    txtMontoL.Focus()
            '    blnOk = False
            'End If

        End If


        If chkTarifario.Checked Then
            If txtTituloGrupo.Text.Trim = "" Then
                ErrPrv.SetError(txtTituloGrupo, "Debe ingresar Texto Grupo en el Tarifario")
                txtTituloGrupo.Focus()
                blnOk = False
            End If
        End If

        'If chkDetraccion.Checked Then
        '    Dim objProvBN As New clsProveedorBN.clsAdministProveedorBN
        '    Dim dttDetrac As DataTable = objProvBN.ConsultarxIDServicio_Detracciones(strIDServicio)
        '    If dttDetrac.Rows.Count = 0 Then
        '        ErrPrv.SetError(chkDetraccion, "Para poder habilitar la detracción, el proveedor debe de tener una cuenta bancaria de tipo Detracción")
        '        chkDetraccion.Focus()
        '        blnOk = False
        '    End If
        'End If

        If blnCambiosDescripcion Then
            blnDoubleClickLvw = False
            txtUsuario_TextChanged(txtDescripcion, Nothing)
        End If

        If blnCambioServRel Then
            If Not blnCambioServRelAcepto Then
                Dim objServBN As New clsServicioProveedorBN
                If objServBN.blnExisteenCotiz(strIDServicio) Then
                    'MessageBox.Show("No se puede cambiar de servicio relacionado. Existen cotizaciones que hacen referencia a este servicio", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    If MessageBox.Show("Existen cotizaciones que hacen referencia a este servicio." & Chr(13) & "¿Desea Ud. continuar?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                        blnOk = False
                    End If

                    'blnOk = False
                End If
            End If

        End If




        If blnOk = True And lblIDServicio_Det_V.Text.Trim = "" Then
            If lvw.Visible = True Then
                If MessageBox.Show("Existen servicios similares ya registradas. Desea Ud. grabar de todas formas?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    blnOk = True
                Else
                    blnOk = False
                End If
            End If
        End If
        If Not blnOk Then
            Return False
        Else
            Return True
        End If
    End Function
    ''Private Sub frmMantTipoDocDato_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
    ''    'FormRef.dgv_Click(Nothing, Nothing)
    ''    FormRefMantServiciosProveedor.dgv_Click(Nothing, Nothing)
    ''    FormRefMantServiciosProveedor.dgv_CellEnter(Nothing, Nothing)
    ''    frmMain.DesabledBtns()

    ''End Sub
    Private Sub frmMantNivelesDato_FormClosed(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
        'FormRef.dgv_Click(Nothing, Nothing)
        blnFormClosed = True
        'If Not FormRefMantServiciosProveedor Is Nothing Then
        FormRefMantServiciosProveedor.dgv_Click(Nothing, Nothing)
        FormRefMantServiciosProveedor.dgv_CellEnter(Nothing, Nothing)
        ColocarPosicionDgv(FormRefMantServiciosProveedor.dgvDet, "IDServicio_Det", strIDServicio_Det)
        'End If
        'frmMain.DesabledBtns()

    End Sub
    Private Sub pNuevo(ByVal sender As System.Object, ByVal e As System.EventArgs)
        ModoNuevo()
    End Sub
    Private Sub ModoNuevo()
        strModoEdicion = "N"
        'lblEstadoForm.Text = "Nuevo"
        'grb.Text = "Servicio: " & strDescServicio
        lblTitulo.Text = "Servicio: " & strDescServicio
        lblTitulo2.Text = "Servicio: " & strDescServicio
        'chkAfecto_CheckedChanged(Nothing, Nothing)

        blnNuevoEnabled = False
        blnDeshacerEnabled = True
        blnGrabarEnabled = True
        blnEditarEnabled = False
        blnEliminarEnabled = False
        blnSalirEnabled = False
        ActivarDesactivarBotones()

        'pClearControls(Me)
        'pEnableControls(Me, True)
        'ErrPrv.SetError(txtcAnio, "")
        ''ErrPrv.SetError(txtcPedCod, "")
        'ErrPrv.SetError(cboEmpresa, "")
        'ErrPrv.SetError(cboCliente, "")
        'ErrPrv.SetError(cboVendedor, "")

        Using objBNServ As New clsServicioProveedorBN
            Using dr As SqlClient.SqlDataReader = objBNServ.ConsultarPk(strIDServicio)
                dr.Read()
                If dr.HasRows Then
                    chkPoliticaLiberado.Checked = dr("PoliticaLiberado")
                    txtLiberado.Text = If(IsDBNull(dr("Liberado")) = True, "", dr("Liberado"))
                    If Not IsDBNull(dr("TipoLib")) Then
                        cboTipoLib.SelectedValue = dr("TipoLib")
                    End If
                    txtMaximoLiberado.Text = If(IsDBNull(dr("MaximoLiberado")) = True, "", dr("MaximoLiberado"))
                    txtMontoL.Text = If(IsDBNull(dr("MontoL")) = True, "", dr("MontoL"))

                Else
                    MessageBox.Show("El servicio ha sido eliminado recientemente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                End If
                dr.Close()
            End Using
        End Using

        If strIDTipoOper = "E" Then
            txtDescripcion.Text = strDescServicio
        End If

        lblTCEtiq.Enabled = True
        txtTC.Enabled = True
        lblTitulo.ForeColor = Color.White
        txtDescripcion.Focus()

        'txtTC.Text = sglTipoCambioPais
    End Sub

    Private Sub CargarSeguridad()
        Try
            Dim strNombreForm As String
            strNombreForm = Replace(Me.Name, "frm", "mnu")
            strNombreForm = Replace(strNombreForm, "Dato", "")

            If blnServicioVarios Then
                strNombreForm = "mnuMantServiciosVariosDetalleCotizacion"
            End If

            Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
                    If dr.HasRows Then
                        dr.Read()
                        blnGrabarEnabledBD = dr("Grabar")
                        blnEditarEnabledBD = dr("Actualizar")
                        blnEliminarEnabledBD = dr("Eliminar")
                    End If
                    dr.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActivarDesactivarBotones()

        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        If blnNuevoEnabled = True Then
            If blnGrabarEnabledBD Then
                frmMain.btnNuevo.Enabled = blnNuevoEnabled
                btnNuevo.Enabled = blnNuevoEnabled
            Else
                frmMain.btnNuevo.Enabled = False
                btnNuevo.Enabled = False
                blnNuevoEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnNuevo.Enabled = blnNuevoEnabled
            btnNuevo.Enabled = blnNuevoEnabled
        End If

        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        If blnGrabarEnabled = True Then
            If blnGrabarEnabledBD Then
                If blnCotizacion Then blnGrabarEnabled = False
                If blnServicioVarios Then blnGrabarEnabled = True
                frmMain.btnGrabar.Enabled = blnGrabarEnabled
                btnGrabar.Enabled = blnGrabarEnabled
            Else
                frmMain.btnGrabar.Enabled = False
                btnGrabar.Enabled = False
                blnGrabarEnabled = blnGrabarEnabledBD
            End If
        Else
            frmMain.btnGrabar.Enabled = blnGrabarEnabled
            btnGrabar.Enabled = blnGrabarEnabled
        End If

        frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        btnDeshacer.Enabled = blnDeshacerEnabled

        'frmMain.btnEditar.Enabled = blnEditarEnabled
        If blnEditarEnabled = True Then
            If blnEditarEnabledBD Then
                frmMain.btnEditar.Enabled = blnEditarEnabled
                btnEditar.Enabled = blnEditarEnabled
            Else
                frmMain.btnEditar.Enabled = False
                btnEditar.Enabled = False
                blnEditarEnabled = blnEditarEnabledBD
            End If
        Else
            frmMain.btnEditar.Enabled = blnEditarEnabled
            btnEditar.Enabled = blnEditarEnabled
        End If

        'frmMain.btnEliminar.Enabled = blnEliminarEnabled
        If blnEliminarEnabled = True Then
            If blnEliminarEnabledBD Then
                frmMain.btnEliminar.Enabled = blnEliminarEnabled
                btnEliminar.Enabled = blnEliminarEnabled
            Else
                frmMain.btnEliminar.Enabled = False
                btnEliminar.Enabled = False
                blnEliminarEnabled = blnEliminarEnabledBD
            End If
        Else
            frmMain.btnEliminar.Enabled = blnEliminarEnabled
            btnEliminar.Enabled = blnEliminarEnabled
        End If

        frmMain.btnSalir.Enabled = blnSalirEnabled
        btnSalir.Enabled = blnSalirEnabled

    End Sub


    Private Sub ModoEditar()
        strModoEdicion = "E"
        'lblEstadoForm.Text = "Edición"
        'grb.Text = "Detalle Servicio: " & strDescripcion
        lblTitulo.Text = "Detalle Servicio: " & strDescripcion
        lblTitulo2.Text = "Detalle Servicio: " & strDescripcion
        Me.Text = "Edición de Detalle Servicio: " & strDescripcion

        blnNuevoEnabled = False
        blnDeshacerEnabled = True
        blnGrabarEnabled = True
        'blnEditarEnabled = False
        'blnEliminarEnabled = False
        blnSalirEnabled = False
        ActivarDesactivarBotones()

        'tab.TabPages.Item(0).Text = "Actualización Registro"
        'tab.SelectedIndex = 1
        'CargaControles()
        'pClearControls(Me)
        'pEnableControls(Me, True)

        'btnBuscarDetServicio.Enabled = False
        lblTitulo.ForeColor = Color.White
        txtDescripcion.Focus()
    End Sub
    Private Sub pDeshacer(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnDeshacer.Click


        Me.Close()

    End Sub
    Private Sub ModoConsulta()
        strModoEdicion = "C"
        'lblEstadoForm.Text = "Consulta"
        'grb.Text = "Detalle Servicio: " & strDescripcion
        lblTitulo.Text = "Detalle Servicio: " & strDescripcion
        lblTitulo2.Text = "Detalle Servicio: " & strDescripcion

        blnNuevoEnabled = True
        blnDeshacerEnabled = False
        blnGrabarEnabled = False
        'blnEditarEnabled = True
        'blnEliminarEnabled = True
        blnSalirEnabled = True
        ActivarDesactivarBotones()

        'pClearControls(Me)
        'pEnableControls(Me, False)

        'txtvPrvRso.Focus()
    End Sub
    Private Sub CalcularMontosPlanAlimenticio()

        'txtMonto_dbl.Text = Convert.ToDouble(txtMonto_sgl.Text) * 2
        'txtMonto_dbls.Text = Convert.ToDouble(txtMonto_sgls.Text) * 2

        'txtMonto_tri.Text = Convert.ToDouble(txtMonto_sgl.Text) * 3
        'txtMonto_tris.Text = Convert.ToDouble(txtMonto_sgls.Text) * 3

    End Sub
    Private Sub pGrabar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGrabar.Click

        dgv.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
        For Each FilaDgv As DataGridViewRow In dgv.Rows
            AgregarEditarValorStruct(FilaDgv.Index, "")
        Next

        dgvAPT.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
        For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
            AgregarEditarValorStructAPT(FilaDgv.Index, "")
        Next

        CalcularMontosDolares()
        If Not blnValidarIngresos() Then Exit Sub
        If chkPlanAlimenticio.Checked Then CalcularMontosPlanAlimenticio()

        Try
            If strModoEdicion = "N" Then
                Grabar()
                MessageBox.Show("Se creó el Detalle Servicio " & txtDescripcion.Text & " exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            ElseIf strModoEdicion = "E" Then
                Actualizar()
                MessageBox.Show("Se actualizó el Detalle Servicio " & txtDescripcion.Text & " exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If

            'FormRef.Buscar()
            'FormRef.dgv_Click(Nothing, Nothing)

            FormRefMantServiciosProveedor.dgv_CellEnter(Nothing, Nothing)
            Me.Close()
            frmMain.DesabledBtns()
        Catch ex As Exception
            If InStr(ex.Message, "PK_") > 0 Then
                MessageBox.Show("Ya existe un registro con el mismo Id", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            ElseIf InStr(ex.Message, "IX_") > 0 Then
                MessageBox.Show("Ya existe un registro con la misma Descripción", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Else
                MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
            End If
        End Try

    End Sub
    Private Sub Grabar()
        Try
            ''AgregarEditarValorStructDescAPT(intIDTemporada, txtDescAlter.Text.Trim, txtSinpIndi.Text)
            AgregarEditarValorControlAPT(intIDTemporada)

            Dim objBE As New clsServicioProveedorBE.clsDetalleServicioProveedorBE
            With objBE
                .Afecto = chkAfecto.Checked
                .Anio = txtAnio.Text
                .CoCeCos = If(cboCentroCostos.Text = "", "000000", cboCentroCostos.SelectedValue.ToString)
                .CtaContable = If(cboCuentaContable.Text = "", "000000", cboCuentaContable.SelectedValue.ToString)
                .CtaContableC = ""
                .Codigo = If(txtDescripcion.Text.Trim.Length >= 20, txtDescripcion.Text.Trim.Substring(0, 20), txtDescripcion.Text.Trim)
                .Descripcion = txtDescripcion.Text
                .DetaTipo = txtDetaTipo.Text
                .VerDetaTipo = chkVerDetaTipo.Checked
                .IDProveedor = strIDProveedor
                .IDServicio = strIDServicio
                .IDServicio_Det_V = intIDServicio_Det_V
                .IDTipoOC = If(cboIdTipoOC.Text = "", "NAP", cboIdTipoOC.SelectedValue.ToString)
                .PoliticaLiberado = chkPoliticaLiberado.Checked
                .Liberado = If(txtLiberado.Text = "", "0", txtLiberado.Text)
                .MontoL = If(txtMontoL.Text = "", "0", txtMontoL.Text)
                .TipoLib = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
                .MaximoLiberado = If(txtMaximoLiberado.Text = "", "0", txtMaximoLiberado.Text)
                .PoliticaLibNivelDetalle = chkPoliticaLibNivelDetalle.Checked
                .Monto_dbl = If(txtMonto_dbl.Text.Trim = "", 0, CDbl(txtMonto_dbl.Text.Trim))
                .Monto_tri = If(txtMonto_tri.Text.Trim = "", 0, CDbl(txtMonto_tri.Text.Trim))
                .Monto_Sgl = If(txtMonto_sgl.Text.Trim = "", 0, CDbl(txtMonto_sgl.Text.Trim))
                .Monto_dbls = If(txtMonto_dbls.Text.Trim = "", 0, CDbl(txtMonto_dbls.Text.Trim))
                .Monto_tris = If(txtMonto_tris.Text.Trim = "", 0, CDbl(txtMonto_tris.Text.Trim))
                .Monto_Sgls = If(txtMonto_sgls.Text.Trim = "", 0, CDbl(txtMonto_sgls.Text.Trim))
                .TC = If(txtTC.Text.Trim = "", 0, CDbl(txtTC.Text.Trim))
                .ConAlojamiento = chkConAlojamiento.Checked
                .PlanAlimenticio = chkPlanAlimenticio.Checked
                .DiferSS = If(txtDiferSS.Text.Trim = "", 0, CDbl(txtDiferSS.Text.Trim))
                .DiferST = If(txtDiferST.Text.Trim = "", 0, CDbl(txtDiferST.Text.Trim))
                .TipoGasto = If(cboTipoGasto.Text = "", "", cboTipoGasto.SelectedValue.ToString)
                '.Cod_x_Triple = If(cboCodxTriple.Text = "", "", cboCodxTriple.SelectedValue.ToString)
                .Cod_x_Triple = ""
                .IDServicio_DetxTriple = If(cboCodxTriple.Text = "", "0", cboCodxTriple.SelectedValue.ToString)
                .Tarifario = chkTarifario.Checked
                .Tipo = txtTipo.Text
                .TipoDesayuno = If(cboTipoDesayuno.Text = "", "NAP", cboTipoDesayuno.SelectedValue.ToString)
                .Desayuno = chkDesayuno.Checked
                .Lonche = chkLonche.Checked
                .Almuerzo = chkAlmuerzo.Checked
                .Cena = chkCena.Checked
                .TituloGrupo = txtTituloGrupo.Text
                .IdHabitTriple = If(cboHabitTriple.Text.Trim = "", "000", cboHabitTriple.SelectedValue.ToString)
                .CodTarifa = txtCodTarifa.Text.Trim
                .FlDetraccion = chkDetraccion.Checked
                If .TC > 0 Then
                    .CoMoneda = strCoMonedaPais
                Else
                    .CoMoneda = gstrTipoMonedaDol
                End If

                .UserMod = gstrUser
            End With

            Dim objBL As New clsServicioProveedorBT.clsDetalleServicioProveedorBT

            Dim objBEDet As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE
            Dim ListaDet As New List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE)

            Dim objBEDetAPT As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE
            Dim ListaDetAPT As New List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE)

            For Each ST As stCostos In objLst
                objBEDet = New clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE
                With objBEDet

                    .Correlativo = ST.ID
                    .PaxDesde = ST.PaxDesde
                    .PaxHasta = ST.PaxHasta
                    .Monto = ST.Monto
                    .UserMod = gstrUser

                    .Accion = ST.strAccion
                End With
                ListaDet.Add(objBEDet)
            Next
            objBE.ListaCostos = ListaDet

            '---
            If strIDTipoProv = gstrTipoProveeOperadores And chkConAlojamiento.Checked = False And objLstDescAPT.Count > 0 Then
                Eliminar_RangosAPT()
            End If

            Dim objBEDetDescAPT As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE
            Dim ListaDetDescAPT As New List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE)
            For Each STDesc As stDescRangosAPT In objLstDescAPT
                objBEDetDescAPT = New clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE
                With objBEDetDescAPT
                    .IDTemporada = STDesc.IDTemporada
                    .DescripcionAlterna = STDesc.DescripAlter
                    .SingleSupplement = STDesc.SingleSupplement
                    .PoliticaLiberado = STDesc.PoliticaLiberado
                    .Liberado = STDesc.Liberado
                    .TipoLib = STDesc.TipoLib
                    .MaximoLiberado = STDesc.MaximoLiberado
                    .MontoL = STDesc.MontoL
                    .UserMod = gstrUser
                End With
                ListaDetDescAPT.Add(objBEDetDescAPT)
            Next
            objBE.ListaDescRangos = ListaDetDescAPT

            '---
            For Each ST As stRangosAPT In objLstAPT
                objBEDetAPT = New clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE
                With objBEDetAPT

                    .IDTemporada = ST.IDTemporada
                    .Correlativo = ST.ID
                    .PaxDesde = ST.PaxDesde
                    .PaxHasta = ST.PaxHasta
                    .Monto = ST.Monto
                    .UserMod = gstrUser

                    .Accion = ST.strAccion
                End With
                ListaDetAPT.Add(objBEDetAPT)
            Next
            objBE.ListaRangos = ListaDetAPT

            objBL.Insertar(objBE)



        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub Eliminar_RangosAPT()
        For iTemp As Int16 = objLstDescAPT.Count - 1 To 0 Step -1
            For iRango As Int16 = objLstAPT.Count - 1 To 0 Step -1
                If objLstDescAPT(iTemp).IDTemporada = objLstAPT(iRango).IDTemporada And objLstAPT(iRango).strAccion <> "B" Then
                    Dim RegValor As stRangosAPT = New stRangosAPT
                    With RegValor
                        .IDTemporada = objLstAPT(iRango).IDTemporada
                        .ID = objLstAPT(iRango).ID
                        .PaxDesde = objLstAPT(iRango).PaxDesde
                        .PaxHasta = objLstAPT(iRango).PaxHasta
                        .Monto = objLstAPT(iRango).Monto
                        .strAccion = "B"
                    End With
                    objLstAPT.RemoveAt(iRango)
                    objLstAPT.Add(RegValor)
                End If
            Next
        Next
    End Sub

    Private Sub Actualizar()
        Try
            'Dim intAnioPreciosCopia As Int16 = If(strAnioPreciosCopia = "", "0", strAnioPreciosCopia)
            'If txtAnio.Text <> strAnioPreciosCopia And strAnioPreciosCopia <> "" Then
            '    If MessageBox.Show("¿Actualizó Ud. los precios para este año?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
            '        intAnioPreciosCopia += 1
            '    End If
            'End If

            ''AgregarEditarValorStructDescAPT(intIDTemporada, txtDescAlter.Text.Trim, txtSinpIndi.Text)
            AgregarEditarValorControlAPT(intIDTemporada)

            Dim objBE As New clsServicioProveedorBE.clsDetalleServicioProveedorBE
            With objBE
                .IDServicio_Det = strIDServicio_Det
                .Afecto = chkAfecto.Checked
                .Anio = txtAnio.Text
                '.CtaContable = cboCuentaContable.SelectedValue.ToString
                .CoCeCos = If(cboCentroCostos.Text = "", "000000", cboCentroCostos.SelectedValue.ToString)
                .CtaContable = If(cboCuentaContable.Text = "", "000000", cboCuentaContable.SelectedValue.ToString)
                .CtaContableC = ""
                .Codigo = If(txtDescripcion.Text.Trim.Length >= 20, txtDescripcion.Text.Trim.Substring(0, 20), txtDescripcion.Text.Trim)
                .Descripcion = txtDescripcion.Text
                .DetaTipo = txtDetaTipo.Text
                .VerDetaTipo = chkVerDetaTipo.Checked
                .IDProveedor = strIDProveedor
                .IDServicio = strIDServicio
                .IDServicio_Det_V = intIDServicio_Det_V
                .IDTipoOC = If(cboIdTipoOC.Text = "", "NAP", cboIdTipoOC.SelectedValue.ToString)
                .Liberado = If(txtLiberado.Text = "", "0", txtLiberado.Text)
                .Monto_dbl = If(txtMonto_dbl.Text.Trim = "", 0, CDbl(txtMonto_dbl.Text.Trim))
                .Monto_tri = If(txtMonto_tri.Text.Trim = "", 0, CDbl(txtMonto_tri.Text.Trim))
                .Monto_Sgl = If(txtMonto_sgl.Text.Trim = "", 0, CDbl(txtMonto_sgl.Text.Trim))
                .Monto_dbls = If(txtMonto_dbls.Text.Trim = "", 0, CDbl(txtMonto_dbls.Text.Trim))
                .Monto_tris = If(txtMonto_tris.Text.Trim = "", 0, CDbl(txtMonto_tris.Text.Trim))
                .Monto_Sgls = If(txtMonto_sgls.Text.Trim = "", 0, CDbl(txtMonto_sgls.Text.Trim))
                .TC = If(txtTC.Text.Trim = "", 0, CDbl(txtTC.Text.Trim))
                .ConAlojamiento = chkConAlojamiento.Checked
                .PlanAlimenticio = chkPlanAlimenticio.Checked
                .CodTarifa = txtCodTarifa.Text.Trim

                .DiferSS = If(txtDiferSS.Text.Trim = "", 0, CDbl(txtDiferSS.Text.Trim))
                .DiferST = If(txtDiferST.Text.Trim = "", 0, CDbl(txtDiferST.Text.Trim))
                .TipoGasto = If(cboTipoGasto.Text = "", "", cboTipoGasto.SelectedValue.ToString)
                '.Cod_x_Triple = If(cboCodxTriple.Text = "", "", cboCodxTriple.SelectedValue.ToString)
                .Cod_x_Triple = ""
                .IDServicio_DetxTriple = If(cboCodxTriple.Text = "", "0", cboCodxTriple.SelectedValue.ToString)
                .MontoL = If(txtMontoL.Text = "", "0", txtMontoL.Text)
                .PoliticaLiberado = chkPoliticaLiberado.Checked
                .Tarifario = chkTarifario.Checked
                .Tipo = txtTipo.Text
                .TipoDesayuno = If(cboTipoDesayuno.Text = "", "NAP", cboTipoDesayuno.SelectedValue.ToString)
                .Desayuno = chkDesayuno.Checked
                .Lonche = chkLonche.Checked
                .Almuerzo = chkAlmuerzo.Checked
                .Cena = chkCena.Checked
                .TipoLib = If(cboTipoLib.Text = "", "", cboTipoLib.SelectedValue.ToString)
                .TituloGrupo = txtTituloGrupo.Text
                .MaximoLiberado = If(txtMaximoLiberado.Text = "", "0", txtMaximoLiberado.Text)
                .IdHabitTriple = If(cboHabitTriple.Text.Trim = "", "000", cboHabitTriple.SelectedValue.ToString)
                '.AnioPreciosCopia = If(intAnioPreciosCopia.ToString = "0", "", intAnioPreciosCopia.ToString)
                .PoliticaLibNivelDetalle = chkPoliticaLibNivelDetalle.Checked
                .CambioCostos = blnCambioCostos()
                .FlDetraccion = chkDetraccion.Checked
                If .TC > 0 Then
                    .CoMoneda = strCoMonedaPais
                Else
                    .CoMoneda = gstrTipoMonedaDol
                End If
                .UserMod = gstrUser
            End With

            Dim objBL As New clsServicioProveedorBT.clsDetalleServicioProveedorBT

            Dim objBEDet As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE
            Dim ListaDet As New List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE)

            Dim objBEDetAPT As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE
            Dim ListaDetAPT As New List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE)

            For Each ST As stCostos In objLst
                objBEDet = New clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsCostosDetalleServicioProveedorBE
                With objBEDet

                    .Correlativo = ST.ID
                    .PaxDesde = ST.PaxDesde
                    .PaxHasta = ST.PaxHasta
                    .Monto = ST.Monto
                    .UserMod = gstrUser

                    .Accion = ST.strAccion
                End With
                ListaDet.Add(objBEDet)
            Next
            objBE.ListaCostos = ListaDet

            '---
            If strIDTipoProv = gstrTipoProveeOperadores And chkConAlojamiento.Checked = False And objLstDescAPT.Count > 0 Then
                Eliminar_RangosAPT()
            End If

            Dim objBEDetDescAPT As clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE
            Dim ListaDetDescAPT As New List(Of clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE)
            For Each STDesc As stDescRangosAPT In objLstDescAPT
                objBEDetDescAPT = New clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsDescRangosAPTDetalleServicioProveedorBE
                With objBEDetDescAPT
                    .IDTemporada = STDesc.IDTemporada
                    .DescripcionAlterna = STDesc.DescripAlter
                    .SingleSupplement = STDesc.SingleSupplement
                    .PoliticaLiberado = STDesc.PoliticaLiberado
                    .Liberado = STDesc.Liberado
                    .TipoLib = STDesc.TipoLib
                    .MaximoLiberado = STDesc.MaximoLiberado
                    .MontoL = STDesc.MontoL
                    .UserMod = gstrUser
                End With
                ListaDetDescAPT.Add(objBEDetDescAPT)
            Next
            objBE.ListaDescRangos = ListaDetDescAPT

            '--
            For Each ST As stRangosAPT In objLstAPT
                objBEDetAPT = New clsServicioProveedorBE.clsDetalleServicioProveedorBE.clsRangosAPTDetalleServicioProveedorBE
                With objBEDetAPT

                    .IDTemporada = ST.IDTemporada
                    .Correlativo = ST.ID
                    .PaxDesde = ST.PaxDesde
                    .PaxHasta = ST.PaxHasta
                    .Monto = ST.Monto
                    .UserMod = gstrUser

                    .Accion = ST.strAccion
                End With
                ListaDetAPT.Add(objBEDetAPT)
            Next
            objBE.ListaRangos = ListaDetAPT


            objBL.Actualizar(objBE)

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarControles(Optional ByVal vintIDServicio_Det_V As Int32 = 0)
        Try
            Using objBN As New clsServicioProveedorBN.clsDetalleServicioProveedorBN

                Dim dr As SqlClient.SqlDataReader
                If vintIDServicio_Det_V = 0 Then
                    dr = objBN.ConsultarPk(strIDServicio_Det)
                Else
                    dr = objBN.ConsultarPk(intIDServicio_Det_V)
                End If
                dr.Read()
                If dr.HasRows Then
                    'strIDProveedor = dr("IDProveedor")
                    If vintIDServicio_Det_V = 0 Then
                        intIDServicio_Det_V = If(IsDBNull(dr("IDServicio_Det_V")) = True, "0", dr("IDServicio_Det_V"))
                        lblIDServicio_Det_V.Text = If(IsDBNull(dr("IDServicio_Det_V")) = True, "", dr("IDServicio_Det_V"))
                        btnBuscarDetServicio.Enabled = True
                    Else
                        btnBuscarDetServicio.Enabled = False
                        lblIDServicio_Det_V.Text = vintIDServicio_Det_V
                    End If
                    'strIDTipoProv = dr("IDTipoProv")
                    txtAnio.Text = dr("Anio")
                    CargarCboCodTriple()

                    If blnPintarCorrel Then
                        lblCorrelativo.Text = dr("Correlativo")
                    End If
                    'cboCuentaContable.SelectedValue = If(IsDBNull(dr("CtaContable")) = True, "", dr("CtaContable"))
                    txtCtaContableC.Text = If(IsDBNull(dr("CtaContableC")) = True, "", dr("CtaContableC"))
                    'txtCodigo.Text = dr("Codigo")
                    txtDescripcion.Text = dr("Descripcion")
                    lblTitulo.Text = dr("Descripcion")
                    lblTitulo2.Text = dr("Descripcion")
                    txtDetaTipo.Text = If(IsDBNull(dr("DetaTipo")) = True, "", dr("DetaTipo"))
                    chkVerDetaTipo.Checked = dr("VerDetaTipo")
                    chkVerDetaTipo_CheckedChanged(Nothing, Nothing)
                    txtLiberado.Text = If(IsDBNull(dr("Liberado")) = True, "0", dr("Liberado"))
                    txtMonto_dbls.Text = If(IsDBNull(dr("Monto_dbls")) = True, "0.0000", dr("Monto_dbls"))
                    txtMonto_sgls.Text = If(IsDBNull(dr("Monto_sgls")) = True, "0.0000", dr("Monto_sgls"))
                    txtMonto_tris.Text = If(IsDBNull(dr("Monto_tris")) = True, "0.0000", dr("Monto_tris"))

                    txtMonto_dbl.Text = If(IsDBNull(dr("Monto_dbl")) = True, "0.0000", dr("Monto_dbl"))
                    txtMonto_sgl.Text = If(IsDBNull(dr("Monto_sgl")) = True, "0.0000", dr("Monto_sgl"))
                    txtMonto_tri.Text = If(IsDBNull(dr("Monto_tri")) = True, "0.0000", dr("Monto_tri"))

                    txtTC.Text = If(IsDBNull(dr("TC")) = True, "0.0000", dr("TC"))
                    'sglTipoCambioPais = If(IsDBNull(dr("TC")) = True, "0.0000", dr("TC"))
                    Dim sglTC As Single = If(IsDBNull(dr("TC")) = True, "0.0000", dr("TC"))
                    If sglTC > 0 Then
                        'Por si difiere el grid de la bd
                        strCoMonedaPais = If(IsDBNull(dr("CoMoneda")) = True, "", dr("CoMoneda"))
                        strCoSimboloMonedaPais = If(IsDBNull(dr("SimboloMonedaPais")) = True, "", dr("SimboloMonedaPais"))
                    End If

                    chkConAlojamiento.Checked = dr("ConAlojamiento")
                    blnConAlojamiento = chkConAlojamiento.Checked
                    chkPlanAlimenticio.Checked = dr("PlanAlimenticio")
                    chkPlanAlimenticio_CheckedChanged(Nothing, Nothing)

                    txtCodTarifa.Text = If(IsDBNull(dr("CodTarifa")) = True, "", dr("CodTarifa").ToString)

                    txtDiferSS.Text = If(IsDBNull(dr("DiferSS")) = True, "0.0000", dr("DiferSS"))
                    txtDiferST.Text = If(IsDBNull(dr("DiferST")) = True, "0.0000", dr("DiferST"))

                    If Not IsDBNull(dr("TipoGasto")) = True Then
                        cboTipoGasto.SelectedValue = dr("TipoGasto")
                    End If

                    'If Not IsDBNull(dr("cod_x_triple")) = True Then
                    '    cboCodxTriple.SelectedValue = dr("cod_x_triple")
                    'End If
                    If Not IsDBNull(dr("IDServicio_DetxTriple")) Then
                        cboCodxTriple.SelectedValue = dr("IDServicio_DetxTriple")
                    End If


                    txtMontoL.Text = If(IsDBNull(dr("MontoL")) = True, "0.0000", dr("MontoL"))
                    txtTipo.Text = If(IsDBNull(dr("Tipo")) = True, "", dr("Tipo"))

                    txtTituloGrupo.Text = If(IsDBNull(dr("TituloGrupo")) = True, "", dr("TituloGrupo"))
                    ''If Not IsDBNull(dr("IdTipoOC")) Then
                    'cboIdTipoOC.SelectedValue = dr("IdTipoOC")
                    'CargarCombo_CuentasContables()
                    If Not IsDBNull(dr("CoCecos")) Then
                        cboCentroCostos.SelectedValue = dr("CoCecos")
                    Else
                        cboCentroCostos.SelectedValue = dr("CoCecos1")
                    End If
                    'cboCentroCostos.SelectedValue = dr("CoCecos")
                    CargarCombo_CuentasContables_CeCos()
                    CargarCombo_TiposdeOperacion_CeCos()
                    cboCuentaContable.SelectedValue = If(IsDBNull(dr("CtaContable")) = True, "", dr("CtaContable"))
                    cboIdTipoOC.SelectedValue = If(IsDBNull(dr("IdTipoOC")) = True, "", dr("IdTipoOC"))
                    'End If
                    'cboMoneda.SelectedValue = dr("IDMoneda")
                    If Not IsDBNull(dr("TipoLib")) Then
                        cboTipoLib.SelectedValue = dr("TipoLib")
                    End If
                    cboTipoDesayuno.SelectedValue = dr("TipoDesayuno")

                    chkDesayuno.Checked = dr("Desayuno")
                    chkLonche.Checked = dr("Lonche")
                    chkAlmuerzo.Checked = dr("Almuerzo")
                    chkCena.Checked = dr("Cena")

                    chkAfecto.Checked = dr("Afecto")
                    chkPoliticaLiberado.Checked = dr("PoliticaLiberado")
                    chkPoliticaLibNivelDetalle.Checked = dr("PoliticaLibNivelDetalle")
                    chkTarifario.Checked = dr("Tarifario")
                    txtMaximoLiberado.Text = If(IsDBNull(dr("MaximoLiberado")) = True, "", dr("MaximoLiberado"))




                    If Convert.ToDouble(txtMonto_sgls.Text) + Convert.ToDouble(txtMonto_dbls.Text) = 0 Then
                        lblMontoSimpleEtiq2.Enabled = True
                        txtMonto_sgl.Enabled = True
                        lblMontoDobleEtiq2.Enabled = True
                        txtMonto_dbl.Enabled = True
                        lblMontoTripleEtiq2.Enabled = True
                        txtMonto_tri.Enabled = True
                    Else
                        lblMontoSimpleEtiq2.Enabled = False
                        txtMonto_sgl.Enabled = False
                        lblMontoDobleEtiq2.Enabled = False
                        txtMonto_dbl.Enabled = False
                        lblMontoTripleEtiq2.Enabled = False
                        txtMonto_tri.Enabled = False

                    End If

                    cboHabitTriple.SelectedValue = If(IsDBNull(dr("IdHabitTriple").ToString) = True, "", dr("IdHabitTriple").ToString)
                    'Dim dtt As New DataTable("Costos")
                    'With dtt.Columns
                    '    .Add("Correlativo")
                    '    .Add("PaxDesde")
                    '    .Add("PaxHasta")
                    '    .Add("Monto")
                    'End With
                    Dim objBNDet As New clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsCostosDetalleServicioProveedorBN

                    Dim dtDet As New DataTable

                    If vintIDServicio_Det_V = 0 Then
                        dtDet = objBNDet.ConsultarListxIDDetalle(strIDServicio_Det)
                    Else
                        If Not blnIDServicio_DetRelIgual Then
                            dtDet = objBNDet.ConsultarListxIDDetalle(vintIDServicio_Det_V)
                        End If
                    End If

                    For Each drDet As DataRow In dtDet.Rows
                        dttCostos.Rows.Add(drDet("Correlativo"), drDet("PaxDesde"), drDet("PaxHasta"), drDet("Monto"))
                    Next

                    'If dtDet.Rows.Count > 0 Then
                    '    dttCostos.Rows.Add()
                    'End If

                    dgv.DataSource = dttCostos
                    dcValoresCostos = New Dictionary(Of String, Double)
                    'CargarValores Iniciales de Costos
                    With dcValoresCostos
                        .Add(txtMonto_dbls.Name, CDbl(txtMonto_dbls.Text))
                        .Add(txtMonto_sgls.Name, CDbl(txtMonto_sgls.Text))
                        .Add(txtMonto_tris.Name, CDbl(txtMonto_tris.Text))
                        .Add(txtMonto_dbl.Name, CDbl(txtMonto_dbl.Text))
                        .Add(txtMonto_sgl.Name, CDbl(txtMonto_sgl.Text))
                        .Add(txtMonto_tri.Name, CDbl(txtMonto_tri.Text))
                        .Add(txtTC.Name, CDbl(txtTC.Text))
                        .Add(txtDiferSS.Name, If(txtDiferSS.Text.Trim = "", 0, CDbl(txtDiferSS.Text)))
                        .Add(txtDiferST.Name, If(txtDiferST.Text.Trim = "", 0, CDbl(txtDiferST.Text)))
                        .Add(txtMontoL.Name, CDbl(txtMontoL.Text))
                        'Cargando los valos de los rangos
                        For Each DgvRowItem As DataGridViewRow In dgv.Rows
                            If DgvRowItem.Cells("PaxDesde").Value IsNot Nothing And DgvRowItem.Cells("PaxHasta").Value IsNot Nothing Then
                                .Add(DgvRowItem.Cells("PaxDesde").Value.ToString & "-" & DgvRowItem.Cells("PaxHasta").Value.ToString, _
                                     CDbl(DgvRowItem.Cells("Monto").Value))
                            End If
                        Next
                    End With

                    If dtDet.Rows.Count > 0 Then
                        chkRangos.Checked = True
                    Else
                        chkRangos.Checked = False
                    End If

                    chkDetraccion.Checked = If(IsDBNull(dr("FlDetraccion")) = True, False, Convert.ToBoolean(dr("FlDetraccion")))

                    '---------------RANGOS APT-----------------
                    Cargar_Todos_Rangos(vintIDServicio_Det_V)

                    ''Dim objBNDetAPT As New clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsRangosAPTDetalleServicioProveedorBN

                    ''Dim dtDetAPT As New DataTable

                    ''If vintIDServicio_Det_V = 0 Then
                    ''    dtDetAPT = objBNDetAPT.ConsultarListxIDDetalle(strIDServicio_Det)
                    ''Else
                    ''    If Not blnIDServicio_DetRelIgual Then
                    ''        dtDetAPT = objBNDetAPT.ConsultarListxIDDetalle(vintIDServicio_Det_V)
                    ''    End If
                    ''End If

                    ''For Each drDet As DataRow In dtDetAPT.Rows
                    ''    dttRangosAPT.Rows.Add(drDet("Correlativo"), drDet("PaxDesde"), drDet("PaxHasta"), drDet("Monto"))
                    ''Next

                    ''dgvAPT.DataSource = dttRangosAPT

                    ''If dtDetAPT.Rows.Count > 0 Then
                    ''    chkRangosAPT.Checked = True
                    ''Else
                    ''    chkRangosAPT.Checked = False
                    ''End If

                    ''dgvAPT.Columns("Correlativo2").Visible = False
                    'dgvAPT.Columns(0).Visible = False
                    '------------------------------------------



                    If intIDServicio_Det_V <> 0 Then 'Se desactiva todo si tiene relacion
                        pEnableControlsGroupBox(grb, False)
                        dgv.ReadOnly = True
                        dgvAPT.ReadOnly = True
                    End If

                    txtTipo.Enabled = True : txtDetaTipo.Enabled = True : btnBuscarDetServicio.Enabled = True : txtAnio.Enabled = True

                    'strAnioPreciosCopia = If(IsDBNull(dr("AnioPreciosCopia")) = True, "", dr("AnioPreciosCopia"))
                    If strIDTipoProv = gstrTipoProveeOperadores Then chkVerDetaTipo.Text = "Ver Código de Tarifa"
                Else
                    MessageBox.Show("El registro ha sido eliminado recientemente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                End If
                dr.Close()
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click

        Me.Close()

    End Sub

    'Private Sub ActivarCheck_TextoCodigoTarifa()
    '    If strIDTipoProv = gstrTipoProveeOperadores And chkConAlojamiento.Checked Then
    '        chkVerDetaTipo.Visible = True
    '        chkVerDetaTipo.Enabled = True
    '        'lblEtiqCodTarifa.Visible = True
    '        txtCodTarifa.Visible = True
    '    Else
    '        chkVerDetaTipo.Visible = False
    '        chkVerDetaTipo.Checked = False
    '        'lblEtiqCodTarifa.Visible = False
    '        txtCodTarifa.Visible = False
    '        txtCodTarifa.Text = ""
    '    End If
    'End Sub

    Private Sub txtMonto_sgls_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTC.LostFocus, txtMonto_tris.LostFocus, txtMonto_sgls.LostFocus, txtMonto_dbls.LostFocus
        Try
            CalcularMontosDolares()

            If sender.name = "txtMonto_sgls" Then
                If Convert.ToDouble(txtMonto_sgls.Text) = 0 Then
                    txtMonto_sgl.Focus()
                Else
                    txtMonto_dbls.Focus()
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtUsuario_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtTC.TextChanged, _
    txtMontoL.TextChanged, cboTipoLib.TextChanged, txtMonto_tris.TextChanged, txtMonto_tri.TextChanged, txtMonto_sgls.TextChanged, txtMonto_sgl.TextChanged, txtMonto_dbls.TextChanged, txtMonto_dbl.TextChanged, _
    txtDescripcion.TextChanged, txtAnio.TextChanged, cboCodxTriple.TextChanged, cboTipoDesayuno.TextChanged, txtDetaTipo.TextChanged, cboHabitTriple.SelectionChangeCommitted, chkDetraccion.CheckedChanged

        Select Case sender.name
            Case "txtDescripcion"
                ErrPrv.SetError(txtDescripcion, "")
                blnCambiosDescripcion = True
                If blnLoad Then
                    If txtDescripcion.Text.Length >= 3 Then
                        Dim objBL As New clsServicioProveedorBN.clsDetalleServicioProveedorBN
                        Dim dt As New DataTable
                        dt = objBL.ConsultarLvw(strIDServicio_Det, strIDServicio, txtDescripcion.Text)
                        pCargaListView(lvw, dt, dt.Columns.Count - 1, 0)
                        If dt.Rows.Count > 0 Then
                            If Not blnDoubleClickLvw Then
                                lvw.Visible = True
                                'ErrPrv.SetError(lvw, "Existen descripciones similares ya registradas")
                                ErrPrv.SetError(lvw, "")
                            End If
                        Else
                            lvw.Visible = False
                            ErrPrv.SetError(lvw, "")
                        End If
                    Else
                        lvw.Visible = False
                        ErrPrv.SetError(lvw, "")
                    End If
                End If

            Case "txtAnio"
                ErrPrv.SetError(txtAnio, "")

            Case "txtMontoL"
                ErrPrv.SetError(txtMontoL, "")
            Case "cboTipoLib"
                ErrPrv.SetError(cboTipoLib, "")
            Case "txtMonto_dbls"
                ErrPrv.SetError(txtMonto_dbls, "")
            Case "txtMonto_sgls"
                ErrPrv.SetError(txtMonto_sgls, "")
            Case "txtMonto_tris"
                ErrPrv.SetError(txtMonto_tris, "")
            Case "txtMonto_dbl"
                ErrPrv.SetError(txtMonto_dbl, "")
            Case "txtMonto_sgl"
                ErrPrv.SetError(txtMonto_sgl, "")
            Case "txtMonto_tri"
                ErrPrv.SetError(txtMonto_tri, "")
            Case "txtTC"
                ErrPrv.SetError(txtTC, "")
                If blnLoad Then
                    Dim sglTC As Single = 0
                    If IsNumeric(txtTC.Text) Then
                        sglTC = txtTC.Text
                    Else
                        If txtTC.Text <> "" Then ErrPrv.SetError(txtTC, "Debe ingresar un número válido")
                    End If
                    dgv.Columns("Monto").HeaderText = "Monto (US$)"
                    If sglTC > 0 Then
                        'dgv.Columns("Monto").HeaderText = "Monto (S/.)"
                        dgv.Columns("Monto").HeaderText = "Monto (" & strCoSimboloMonedaPais & ")"
                    End If
                End If
            Case "cboCodxTriple"
                ErrPrv.SetError(cboCodxTriple, "")
            Case "cboTipoDesayuno"
                ErrPrv.SetError(cboTipoDesayuno, "")
            Case "txtDetaTipo"
                ErrPrv.SetError(txtDetaTipo, "")
            Case "cboHabitTriple"
                ErrPrv.SetError(cboHabitTriple, "")
            Case "chkDetraccion"
                ErrPrv.SetError(chkDetraccion, "")
        End Select

    End Sub

    Private Sub chkTarifario_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkTarifario.CheckedChanged
        If chkTarifario.Checked Then

            lblTituloGrupoEtiq.Enabled = True
            txtTituloGrupo.Enabled = True

        Else
            lblTituloGrupoEtiq.Enabled = False
            txtTituloGrupo.Enabled = False
            txtTituloGrupo.Text = ""
        End If
    End Sub

    Private Sub txtMonto_sgl_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles _
        txtTC.KeyPress, txtMontoL.KeyPress, txtMonto_tris.KeyPress, txtMonto_tri.KeyPress, _
        txtMonto_sgls.KeyPress, txtMonto_sgl.KeyPress, txtMonto_dbls.KeyPress, txtMonto_dbl.KeyPress, _
        txtDiferSS.KeyPress, txtDiferST.KeyPress, txtSinpIndi.KeyPress, txtMontoLAPT.KeyPress

        If sender.name = "txtDiferSS" Or sender.name = "txtDiferST" Then
            If Asc(e.KeyChar) <> 8 Then
                If InStr("0123456789.,-", e.KeyChar) = 0 Then
                    e.KeyChar = ""
                End If
            End If

        Else
            If Asc(e.KeyChar) <> 8 Then
                If InStr("0123456789.,", e.KeyChar) = 0 Then
                    e.KeyChar = ""
                End If
            End If

        End If

    End Sub


    Private Sub txtLiberado_KeyPress(ByVal sender As Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaximoLiberado.KeyPress, txtLiberado.KeyPress, txtAnio.KeyPress, _
        txtMaximoLiberadoAPT.KeyPress, txtLiberadoAPT.KeyPress
        If Asc(e.KeyChar) <> 8 Then
            If Not IsNumeric(e.KeyChar) Then
                e.KeyChar = ""
            End If
        End If
    End Sub

    Private Sub chkPoliticaLiberado_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPoliticaLiberado.CheckedChanged, chkPoliticaLibNivelDetalle.CheckedChanged
        If chkPoliticaLiberado.Checked And chkPoliticaLibNivelDetalle.Checked Then
            lblLiberadoEtiq.Enabled = True
            txtLiberado.Enabled = True
            lblMontoLEtiq.Enabled = True
            txtMontoL.Enabled = True
            lblTipoLibEtiq.Enabled = True
            cboTipoLib.Enabled = True
            lblMaximoLiberado.Enabled = True
            txtMaximoLiberado.Enabled = True
        Else

            lblLiberadoEtiq.Enabled = False
            txtLiberado.Enabled = False
            'txtLiberado.Text = ""
            lblMontoLEtiq.Enabled = False
            txtMontoL.Enabled = False
            'txtMontoL.Text = ""
            lblTipoLibEtiq.Enabled = False
            cboTipoLib.Enabled = False
            'cboTipoLib.SelectedIndex = -1
            lblMaximoLiberado.Enabled = False
            txtMaximoLiberado.Enabled = False
        End If
        If Not chkPoliticaLiberado.Checked And Not chkPoliticaLibNivelDetalle.Checked Then
            txtLiberado.Text = "0"
            cboTipoLib.SelectedValue = ""
            txtMontoL.Text = "0"
            txtMaximoLiberado.Text = "0"
        End If
    End Sub

    Private Sub CalcularMontosDolares()
        If txtMonto_sgl.Text = "" Then txtMonto_sgl.Text = "0.0000"
        If txtMonto_dbl.Text = "" Then txtMonto_dbl.Text = "0.0000"
        If txtMonto_tri.Text = "" Then txtMonto_tri.Text = "0.0000"
        If txtMonto_sgls.Text = "" Then txtMonto_sgls.Text = "0.0000"
        If txtMonto_dbls.Text = "" Then txtMonto_dbls.Text = "0.0000"
        If txtMonto_tris.Text = "" Then txtMonto_tris.Text = "0.0000"
        If txtTC.Text = "" Then txtTC.Text = "0.0000"

        Try
            If Convert.ToDouble(txtMonto_tri.Text) = 0 And Convert.ToDouble(txtMonto_tris.Text) = 0 Then
                cboCodxTriple.Enabled = True
                lblCodxTripleEtiq.Enabled = True

            Else
                cboCodxTriple.Enabled = False
                lblCodxTripleEtiq.Enabled = False

            End If

            If Convert.ToDouble(txtMonto_sgls.Text) + Convert.ToDouble(txtMonto_dbls.Text) = 0 Then
                lblMontoSimpleEtiq2.Enabled = True
                txtMonto_sgl.Enabled = True
                lblMontoDobleEtiq2.Enabled = True
                txtMonto_dbl.Enabled = True
                lblMontoTripleEtiq2.Enabled = True
                txtMonto_tri.Enabled = True
            Else
                lblMontoSimpleEtiq2.Enabled = False
                txtMonto_sgl.Enabled = False
                lblMontoDobleEtiq2.Enabled = False
                txtMonto_dbl.Enabled = False
                lblMontoTripleEtiq2.Enabled = False
                txtMonto_tri.Enabled = False

            End If

            'If Convert.ToDouble(txtTC.Text) = 0 Then
            '    dgv.Columns("Monto").HeaderText = "Monto ($)"
            'Else
            '    dgv.Columns("Monto").HeaderText = "Monto (S/.)"
            'End If



            If Convert.ToDouble(txtMonto_sgls.Text) + Convert.ToDouble(txtMonto_dbls.Text) > 0 Then
                If Convert.ToDouble(txtTC.Text) = 0 Then
                    lblTCEtiq.Enabled = True
                    txtTC.Enabled = True
                    'ErrPrv.SetError(txtTC, "Debe ingresar Tipo de Cambio")
                    'Exit Sub
                    txtTC.Text = sglTipoCambioPais
                End If
            Else
                Exit Sub
            End If

            txtMonto_sgl.Text = (Convert.ToDouble(txtMonto_sgls.Text) / Convert.ToDouble(txtTC.Text)).ToString("##,##0.0000")
            txtMonto_dbl.Text = (Convert.ToDouble(txtMonto_dbls.Text) / Convert.ToDouble(txtTC.Text)).ToString("##,##0.0000")
            txtMonto_tri.Text = (Convert.ToDouble(txtMonto_tris.Text) / Convert.ToDouble(txtTC.Text)).ToString("##,##0.0000")
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub PintarCorrelativo()
        If Not blnPintarCorrel Then Exit Sub
        Try
            'lblCorrelativo.Visible = True
            'lblCorrelativoEtiq.Visible = True

            If txtAnio.Text = "" Then Exit Sub
            If strModoEdicion <> "N" Then Exit Sub

            Dim objBL As New clsServicioProveedorBN.clsDetalleServicioProveedorBN

            If strIDTipoProv = gstrTipoProveeHoteles Or strIDTipoProv = gstrTipoProveeRestaurantes Then
                lblCorrelativo.Text = 1
            Else
                lblCorrelativo.Text = objBL.bytCorrelativo(strIDServicio, txtAnio.Text, txtTipo.Text)
            End If
        Catch ex As Exception
            Throw
        End Try

    End Sub

    Private Sub txtTipo_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtTipo.LostFocus, txtAnio.LostFocus
        If blnFormClosed Then Exit Sub
        Try
            If strIDTipoProv <> gstrTipoProveeHoteles Or strIDTipoProv <> gstrTipoProveeRestaurantes Then
                PintarCorrelativo()
            End If

            If sender.name = "txtAnio" And strModoEdicion = "N" Then

                CargarCboCodTriple()

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub txtLiberado_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLiberado.TextChanged

    End Sub

    Private Sub dgv_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgv.CellBeginEdit

        If Not dgv.CurrentRow.Cells("PaxDesde").Value Is Nothing And Not dgv.CurrentRow.Cells("PaxHasta").Value Is Nothing Then
            If dgv.CurrentRow.Cells("PaxDesde").Value.ToString <> "" And dgv.CurrentRow.Cells("PaxHasta").Value.ToString <> "" Then
                If Convert.ToInt16(dgv.CurrentRow.Cells("PaxDesde").Value.ToString) > Convert.ToInt16(dgv.CurrentRow.Cells("PaxHasta").Value.ToString) Then
                    MessageBox.Show("Debe ingresar un valor Hasta mayor al valor Desde", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgv.CancelEdit()
                    'dgv.CurrentCell.Value = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub dgvAPT_CellBeginEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellCancelEventArgs) Handles dgvAPT.CellBeginEdit
        If Not dgvAPT.CurrentRow.Cells("PaxDesde2").Value Is Nothing And Not dgvAPT.CurrentRow.Cells("PaxHasta2").Value Is Nothing Then
            If dgvAPT.CurrentRow.Cells("PaxDesde2").Value.ToString <> "" And dgvAPT.CurrentRow.Cells("PaxHasta2").Value.ToString <> "" Then
                If Convert.ToInt16(dgvAPT.CurrentRow.Cells("PaxDesde2").Value.ToString) > Convert.ToInt16(dgvAPT.CurrentRow.Cells("PaxHasta2").Value.ToString) Then
                    MessageBox.Show("Debe ingresar un valor Hasta mayor al valor Desde", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    dgvAPT.CancelEdit()
                    'dgv.CurrentCell.Value = ""
                    Exit Sub
                End If
            End If
        End If
    End Sub

    Private Sub dgv_CellClick(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellClick

    End Sub

    Private Sub dgv_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellContentClick


        dgv.CommitEdit(DataGridViewDataErrorContexts.Commit)
        btnEliminarFila.Enabled = False
        For Each FilaDgv As DataGridViewRow In dgv.Rows
            If FilaDgv.Cells("Sel").Value = True Then
                btnEliminarFila.Enabled = True
                Exit For
            End If
        Next
    End Sub

    Private Sub dgvAPT_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAPT.CellContentClick
        dgvAPT.CommitEdit(DataGridViewDataErrorContexts.Commit)
        btnEliminarFilaAPT.Enabled = False
        For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
            If FilaDgv.Cells("Sel2").Value = True Then
                btnEliminarFilaAPT.Enabled = True
                Exit For
            End If
        Next
    End Sub

    Private Sub dgv_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.CellEndEdit
        Try

            If Not dgv.CurrentRow.Cells("PaxDesde").Value Is Nothing And Not dgv.CurrentRow.Cells("PaxHasta").Value Is Nothing Then
                If dgv.CurrentRow.Cells("PaxDesde").Value.ToString <> "" And dgv.CurrentRow.Cells("PaxHasta").Value.ToString <> "" Then
                    If Convert.ToInt16(dgv.CurrentRow.Cells("PaxDesde").Value.ToString) > Convert.ToInt16(dgv.CurrentRow.Cells("PaxHasta").Value.ToString) Then
                        MessageBox.Show("Debe ingresar un valor Hasta mayor al valor Desde", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgv.CancelEdit()
                        'dgv.CurrentCell.Value = ""
                        Exit Sub
                    End If
                End If
            End If

            Dim FilaActual As Byte = dgv.CurrentRow.Index

            If dgv.CurrentCell.ColumnIndex = dgv.Columns("PaxDesde").Index Then
                For Each FilaDtt As DataGridViewRow In dgv.Rows
                    If Not FilaDtt.IsNewRow Then
                        If FilaActual <> FilaDtt.Index Then
                            If dgv.CurrentCell.Value = FilaDtt.Cells("PaxDesde").Value.ToString Then
                                MessageBox.Show("El Valor Desde se cruza con lo ya ingresado", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgv.CancelEdit()
                                'dgv.CurrentCell.Value = ""

                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If


            If dgv.CurrentRow.Cells("PaxHasta").Value Is Nothing Then
                dgv.CurrentRow.Cells("PaxHasta").Value = dgv.CurrentRow.Cells("PaxDesde").Value
            End If
            If dgv.CurrentCell.ColumnIndex = dgv.Columns("Monto").Index Then
                If Not IsDBNull(dgv.CurrentCell.Value) And Not dgv.CurrentCell.Value Is Nothing Then
                    dgv.CurrentCell.Value = Convert.ToDouble(dgv.CurrentCell.Value).ToString("##,##0.0000")
                End If
            End If

            If Not e Is Nothing Then
                If e.RowIndex < 0 Then Exit Sub

                AgregarEditarValorStruct(e.RowIndex, "")
            Else
                If Not dgv.CurrentCell Is Nothing Then
                    AgregarEditarValorStruct(dgv.CurrentCell.RowIndex, "")
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    '--------RANGOS APT-------------------
    Private Sub dgvAPT_CellEndEdit(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAPT.CellEndEdit
        Try

            If Not dgvAPT.CurrentRow.Cells("PaxDesde2").Value Is Nothing And Not dgvAPT.CurrentRow.Cells("PaxHasta2").Value Is Nothing Then
                If dgvAPT.CurrentRow.Cells("PaxDesde2").Value.ToString <> "" And dgvAPT.CurrentRow.Cells("PaxHasta2").Value.ToString <> "" Then
                    If Convert.ToInt16(dgvAPT.CurrentRow.Cells("PaxDesde2").Value.ToString) > Convert.ToInt16(dgvAPT.CurrentRow.Cells("PaxHasta2").Value.ToString) Then
                        MessageBox.Show("Debe ingresar un valor Hasta mayor al valor Desde", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        dgvAPT.CancelEdit()
                        'dgv.CurrentCell.Value = ""
                        Exit Sub
                    End If
                End If
            End If

            Dim FilaActual As Byte = dgvAPT.CurrentRow.Index

            If dgvAPT.CurrentCell.ColumnIndex = dgvAPT.Columns("PaxDesde2").Index Then
                For Each FilaDtt As DataGridViewRow In dgvAPT.Rows
                    If Not FilaDtt.IsNewRow Then
                        If FilaActual <> FilaDtt.Index Then
                            If dgvAPT.CurrentCell.Value = FilaDtt.Cells("PaxDesde2").Value.ToString Then
                                MessageBox.Show("El Valor Desde se cruza con lo ya ingresado", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                                dgvAPT.CancelEdit()
                                'dgv.CurrentCell.Value = ""

                                Exit Sub
                            End If
                        End If
                    End If
                Next
            End If


            If dgvAPT.CurrentRow.Cells("PaxHasta2").Value Is Nothing Then
                dgvAPT.CurrentRow.Cells("PaxHasta2").Value = dgvAPT.CurrentRow.Cells("PaxDesde2").Value
            End If
            If dgvAPT.CurrentCell.ColumnIndex = dgvAPT.Columns("Monto2").Index Then
                If Not IsDBNull(dgvAPT.CurrentCell.Value) And Not dgvAPT.CurrentCell.Value Is Nothing Then
                    dgvAPT.CurrentCell.Value = Convert.ToDouble(dgvAPT.CurrentCell.Value).ToString("##,##0.0000")
                End If
            End If

            If Not e Is Nothing Then
                If e.RowIndex < 0 Then Exit Sub

                AgregarEditarValorStructAPT(e.RowIndex, "")
            Else
                If Not dgvAPT.CurrentCell Is Nothing Then
                    AgregarEditarValorStructAPT(dgvAPT.CurrentCell.RowIndex, "")
                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub ValidarNrosDecimales_Keypress( _
                 ByVal sender As Object, _
                 ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim intColumna As Integer = dgv.CurrentCell.ColumnIndex

        If intColumna = dgv.Columns("Monto").Index Then

            Dim Caracter As Char = e.KeyChar

            ' comprobar si el caracter es un número o punto o coma o el retroceso   
            If Strings.InStr("1234567890.,", Caracter) = 0 And (Caracter = ChrW(Keys.Back)) = False Then
                'Me.Text = e.KeyChar   
                e.KeyChar = Chr(0)
            End If
        End If
    End Sub

    Private Sub ValidarNrosDecimalesAPT_Keypress( _
               ByVal sender As Object, _
               ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim intColumna As Integer = dgvAPT.CurrentCell.ColumnIndex

        If intColumna = dgvAPT.Columns("Monto2").Index Then

            Dim Caracter As Char = e.KeyChar

            ' comprobar si el caracter es un número o punto o coma o el retroceso   
            If Strings.InStr("1234567890.,", Caracter) = 0 And (Caracter = ChrW(Keys.Back)) = False Then
                'Me.Text = e.KeyChar   
                e.KeyChar = Chr(0)
            End If
        End If
    End Sub

    Private Sub ValidarNrosEnteros_Keypress( _
                 ByVal sender As Object, _
                 ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim intColumna As Integer = dgv.CurrentCell.ColumnIndex

        If intColumna = dgv.Columns("PaxDesde").Index Or intColumna = dgv.Columns("PaxHasta").Index Then

            Dim Caracter As Char = e.KeyChar

            ' comprobar si el caracter es un número o punto o coma o el retroceso   
            If Not IsNumeric(Caracter) And (Caracter = ChrW(Keys.Back)) = False Then
                'Me.Text = e.KeyChar   
                e.KeyChar = Chr(0)
            End If
        End If
    End Sub

    Private Sub ValidarNrosEnterosAPT_Keypress( _
                ByVal sender As Object, _
                ByVal e As System.Windows.Forms.KeyPressEventArgs)

        Dim intColumna As Integer = dgvAPT.CurrentCell.ColumnIndex

        If intColumna = dgvAPT.Columns("PaxDesde2").Index Or intColumna = dgvAPT.Columns("PaxHasta2").Index Then

            Dim Caracter As Char = e.KeyChar

            ' comprobar si el caracter es un número o punto o coma o el retroceso   
            If Not IsNumeric(Caracter) And (Caracter = ChrW(Keys.Back)) = False Then
                'Me.Text = e.KeyChar   
                e.KeyChar = Chr(0)
            End If
        End If
    End Sub

    Private Sub dgv_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAPT.DoubleClick
        'MessageBox.Show(dgv.Item("PaxDesde", 1).Value)
    End Sub

    Private Sub dgv_DoubleClickAPT(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgvAPT.DoubleClick
        'MessageBox.Show(dgv.Item("PaxDesde", 1).Value)
    End Sub

    Private Sub dgv_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgv.EditingControlShowing
        Dim txtEntero As TextBox = CType(e.Control, TextBox)
        Dim txtNrosDecimales As TextBox = CType(e.Control, TextBox)
        AddHandler txtNrosDecimales.KeyPress, AddressOf ValidarNrosDecimales_Keypress
        AddHandler txtEntero.KeyPress, AddressOf ValidarNrosEnteros_Keypress
    End Sub


    Private Sub dgvAPT_EditingControlShowing(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewEditingControlShowingEventArgs) Handles dgvAPT.EditingControlShowing
        Dim txtEntero As TextBox = CType(e.Control, TextBox)
        Dim txtNrosDecimales As TextBox = CType(e.Control, TextBox)
        AddHandler txtNrosDecimales.KeyPress, AddressOf ValidarNrosDecimalesAPT_Keypress
        AddHandler txtEntero.KeyPress, AddressOf ValidarNrosEnterosAPT_Keypress
    End Sub


    Private Sub btnEliminarFila_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarFila.Click
        If MessageBox.Show("¿Está Ud. seguro de eliminar las filas seleccionadas de la cuadrícula?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
BorrarFila:
            For Each FilaDgv As DataGridViewRow In dgv.Rows
                If FilaDgv.Cells("Sel").Value = True Then
                    AgregarEditarValorStruct(FilaDgv.Index, "B")
                    dgv.Rows.Remove(FilaDgv)
                    GoTo BorrarFila
                End If
            Next

            If chkSelTodos.Checked Then chkSelTodos.Checked = False

        End If

    End Sub

    Private Sub btnEliminarFilaAPT_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminarFilaAPT.Click
        If MessageBox.Show("¿Está Ud. seguro de eliminar las filas seleccionadas de la cuadrícula?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
BorrarFila:
            For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
                If FilaDgv.Cells("Sel2").Value = True Then
                    AgregarEditarValorStructAPT(FilaDgv.Index, "B")
                    dgvAPT.Rows.Remove(FilaDgv)
                    GoTo BorrarFila
                End If
            Next
            EliminarStructDescAPT(intIDTemporada)
            If chkSelTodosAPT.Checked Then chkSelTodosAPT.Checked = False

        End If
    End Sub

    Private Sub chkRangos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkRangos.CheckedChanged
        If chkRangos.Checked Then
            dgv.Enabled = True
            'btnEliminarFila.Enabled = True
            chkSelTodos.Enabled = True
            btnCalculo.Enabled = True

            lblMontoSimpleEtiq.Enabled = False
            txtMonto_sgls.Enabled = False
            txtMonto_sgls.Text = "0.0000"
            lblMontoSimpleEtiq2.Enabled = False
            txtMonto_sgl.Enabled = False
            txtMonto_sgl.Text = "0.0000"

            lblMontoDobleEtiq.Enabled = False
            txtMonto_dbl.Enabled = False
            txtMonto_dbl.Text = "0.0000"
            lblMontoDobleEtiq2.Enabled = False
            txtMonto_dbls.Enabled = False
            txtMonto_dbls.Text = "0.0000"

            lblMontoTripleEtiq.Enabled = False
            txtMonto_tri.Enabled = False
            txtMonto_tri.Text = "0.0000"
            lblMontoTripleEtiq2.Enabled = False
            txtMonto_tris.Enabled = False
            txtMonto_tris.Text = "0.0000"


            'lblTCEtiq.Enabled = False
            'txtTC.Enabled = False
            'txtTC.Text = "0.00"

            lblCodxTripleEtiq.Enabled = False
            cboCodxTriple.Enabled = False
            cboCodxTriple.SelectedIndex = -1

            If strIDTipoProv = gstrTipoProveeOperadores Then
                lblDiferSSEtiq.Enabled = True
                lblDiferSTEtiq.Enabled = True
                txtDiferSS.Enabled = True
                txtDiferST.Enabled = True
            End If
        Else

            If strModoEdicion = "N" Then
BorrarFila:
                For Each FilaDgv As DataGridViewRow In dgv.Rows
                    If Not FilaDgv.IsNewRow Then
                        AgregarEditarValorStruct(FilaDgv.Index, "")
                        dgv.Rows.Remove(FilaDgv)
                        GoTo BorrarFila
                    End If
                Next
            End If
            dgv.Enabled = False
            'btnEliminarFila.Enabled = False
            chkSelTodos.Enabled = False
            btnCalculo.Enabled = False

            lblMontoSimpleEtiq.Enabled = True
            txtMonto_sgls.Enabled = True
            lblMontoSimpleEtiq2.Enabled = True
            txtMonto_sgl.Enabled = True

            lblMontoDobleEtiq.Enabled = True
            txtMonto_dbl.Enabled = True
            lblMontoDobleEtiq2.Enabled = True
            txtMonto_dbls.Enabled = True

            lblMontoTripleEtiq.Enabled = True
            txtMonto_tri.Enabled = True
            lblMontoTripleEtiq2.Enabled = True
            txtMonto_tris.Enabled = True

            lblTCEtiq.Enabled = False
            txtTC.Enabled = False
            If (dgv.RowCount = 1 And dgv.Item(1, 0).Value Is Nothing) Then
                lblTCEtiq.Enabled = True
                txtTC.Enabled = True
            Else
                If txtTC.Text <> "0.0000" Then
                    lblTCEtiq.Enabled = True
                    txtTC.Enabled = True
                End If
            End If
            lblCodxTripleEtiq.Enabled = True
            cboCodxTriple.Enabled = True

            txtDiferSS.Enabled = False
            txtDiferST.Enabled = False
            lblDiferSSEtiq.Enabled = False
            lblDiferSTEtiq.Enabled = False
            txtDiferSS.Text = "0.0000"
            txtDiferST.Text = "0.0000"
        End If

    End Sub

    '    Private Sub chkRangosAPT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '        If chkRangosAPT.Checked Then
    '            gpbAPT.Enabled = True
    '            ''dgvAPT.Enabled = True
    '            ''btnEliminarFilaAPT.Enabled = True
    '            ''chkSelTodosAPT.Enabled = True

    '            'dgvAPT.Columns(0).Visible = False
    '            '    btnCalculo.Enabled = True

    '            '    lblMontoSimpleEtiq.Enabled = False
    '            '    txtMonto_sgls.Enabled = False
    '            '    txtMonto_sgls.Text = "0.0000"
    '            '    lblMontoSimpleEtiq2.Enabled = False
    '            '    txtMonto_sgl.Enabled = False
    '            '    txtMonto_sgl.Text = "0.0000"

    '            '    lblMontoDobleEtiq.Enabled = False
    '            '    txtMonto_dbl.Enabled = False
    '            '    txtMonto_dbl.Text = "0.0000"
    '            '    lblMontoDobleEtiq2.Enabled = False
    '            '    txtMonto_dbls.Enabled = False
    '            '    txtMonto_dbls.Text = "0.0000"

    '            '    lblMontoTripleEtiq.Enabled = False
    '            '    txtMonto_tri.Enabled = False
    '            '    txtMonto_tri.Text = "0.0000"
    '            '    lblMontoTripleEtiq2.Enabled = False
    '            '    txtMonto_tris.Enabled = False
    '            '    txtMonto_tris.Text = "0.0000"


    '            '    'lblTCEtiq.Enabled = False
    '            '    'txtTC.Enabled = False
    '            '    'txtTC.Text = "0.00"

    '            '    lblCodxTripleEtiq.Enabled = False
    '            '    cboCodxTriple.Enabled = False
    '            '    cboCodxTriple.SelectedIndex = -1

    '            '    If strIDTipoProv = gstrTipoProveeOperadores Then
    '            '        lblDiferSSEtiq.Enabled = True
    '            '        lblDiferSTEtiq.Enabled = True
    '            '        txtDiferSS.Enabled = True
    '            '        txtDiferST.Enabled = True
    '            '    End If
    '        Else
    '            'dgvAPT.Columns(0).Visible = False
    '            If strModoEdicion = "N" Then
    'BorrarFila:
    '                For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
    '                    If Not FilaDgv.IsNewRow Then
    '                        AgregarEditarValorStructAPT(FilaDgv.Index, "")
    '                        dgvAPT.Rows.Remove(FilaDgv)
    '                        GoTo BorrarFila
    '                    End If
    '                Next
    '            End If

    '            gpbAPT.Enabled = False
    '            ''dgvAPT.Enabled = False
    '            ''btnEliminarFilaAPT.Enabled = False
    '            ''chkSelTodosAPT.Enabled = False

    '            'btnCalculo.Enabled = False

    '            'lblMontoSimpleEtiq.Enabled = True
    '            'txtMonto_sgls.Enabled = True
    '            'lblMontoSimpleEtiq2.Enabled = True
    '            'txtMonto_sgl.Enabled = True

    '            'lblMontoDobleEtiq.Enabled = True
    '            'txtMonto_dbl.Enabled = True
    '            'lblMontoDobleEtiq2.Enabled = True
    '            'txtMonto_dbls.Enabled = True

    '            'lblMontoTripleEtiq.Enabled = True
    '            'txtMonto_tri.Enabled = True
    '            'lblMontoTripleEtiq2.Enabled = True
    '            'txtMonto_tris.Enabled = True

    '            'lblTCEtiq.Enabled = False
    '            'txtTC.Enabled = False
    '            'If (dgv.RowCount = 1 And dgv.Item(1, 0).Value Is Nothing) Then
    '            '    lblTCEtiq.Enabled = True
    '            '    txtTC.Enabled = True
    '            'Else
    '            '    If txtTC.Text <> "0.0000" Then
    '            '        lblTCEtiq.Enabled = True
    '            '        txtTC.Enabled = True
    '            '    End If
    '            'End If
    '            'lblCodxTripleEtiq.Enabled = True
    '            'cboCodxTriple.Enabled = True

    '            'txtDiferSS.Enabled = False
    '            'txtDiferST.Enabled = False
    '            'lblDiferSSEtiq.Enabled = False
    '            'lblDiferSTEtiq.Enabled = False
    '            'txtDiferSS.Text = "0.0000"
    '            'txtDiferST.Text = "0.0000"
    '        End If

    '    End Sub

    Private Sub btnCalculo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCalculo.Click
        '        Try

        '            Dim dttCostosTmp As DataTable = dttCostos.Clone
        '            Dim intFilas As Int16
        'LlamarForm:
        '            Dim blnGridVacio As Boolean = True
        '            For Each drDet As DataGridViewRow In dgv.Rows
        '                If Not drDet.IsNewRow Then
        '                    blnGridVacio = False
        '                    Exit For
        '                End If
        '            Next

        '            Dim intDesde As Int16 = 0
        '            If Not blnGridVacio Then
        '                If dttCostos.Rows.Count = 0 Then

        '                    For Each DgvFila As DataGridViewRow In dgv.Rows
        '                        If Not DgvFila.IsNewRow Then
        '                            dttCostos.Rows.Add(DgvFila.Cells("Correlativo").Value, _
        '                                               DgvFila.Cells("PaxDesde").Value, _
        '                                               DgvFila.Cells("PaxHasta").Value, _
        '                                               DgvFila.Cells("Monto").Value)
        '                        End If
        '                    Next
        '                End If
        '                'dttCostosTmp = dttCostos.Copy

        '                dttCostosTmp.Rows.Clear()
        '                For Each dr As DataRow In dttCostos.Rows
        '                    'dttCostosTmp.ImportRow(dr)
        '                    dttCostosTmp.Rows.Add(dr("Correlativo"), dr("PaxDesde"), dr("PaxHasta"), dr("Monto"))
        '                Next

        '                dttCostos.Rows.Clear()
        '                'dttCostos = dttCostosTmp.Copy
        '                For Each dr As DataRow In dttCostosTmp.Rows
        '                    dttCostos.Rows.Add(dr("Correlativo"), dr("PaxDesde"), dr("PaxHasta"), dr("Monto"))
        '                Next

        '                dgv.DataSource = dttCostos
        '                If IsNumeric(dgv.Rows(dgv.Rows.Count - 2).Cells("PaxHasta").Value) Then
        '                    intDesde = Convert.ToInt16(dgv.Rows(dgv.Rows.Count - 2).Cells("PaxHasta").Value) + 1
        '                End If
        '            Else
        '                dttCostos.Rows.Clear()
        '                For Each drDet As DataGridViewRow In dgv.Rows
        '                    If Not drDet.IsNewRow Then
        '                        dttCostos.Rows.Add(drDet.Cells("Correlativo").Value, drDet.Cells("PaxDesde").Value, drDet.Cells("PaxHasta").Value, drDet.Cells("Monto").Value)
        '                    End If
        '                Next
        '            End If
        '            Dim frm As New frmCalculoCostos
        '            frm.FrmRef = Me
        '            frm.intDesde = intDesde

        '            If dgv.Rows.Count >= 2 Then
        '                dgv.CurrentCell = dgv.Rows(dgv.Rows.Count - 2).Cells("PaxHasta")
        '            End If

        '            intFilas = dgv.Rows.Count
        '            frm.ShowDialog()

        '            dgv.DataSource = dttCostos


        '            For Each FilaDgv As DataGridViewRow In dgv.Rows
        '                AgregarEditarValorStruct(FilaDgv.Index, "")
        '            Next
        '            If dgv.Rows.Count >= 2 Then
        '                dgv.CurrentCell = dgv.Rows(dgv.Rows.Count - 2).Cells("PaxHasta")
        '            End If
        '            'dgv.Rows(dgv.Rows.Count - 2).Selected = True

        '            'Se vuelve a llamar al form
        '            If dgv.Rows.Count > intFilas Then
        '                GoTo LlamarForm
        '            End If

        '            ''blnGridVacio = True
        '            ''For Each drDet As DataGridViewRow In dgv.Rows
        '            ''    If Not drDet.IsNewRow Then
        '            ''        blnGridVacio = False
        '            ''        Exit For
        '            ''    End If
        '            ''Next

        '            ''If Not blnGridVacio Then
        '            ''    If IsNumeric(dgv.Rows(dgv.Rows.Count - 2).Cells("PaxHasta").Value) Then
        '            ''        intDesde = Convert.ToInt16(dgv.Rows(dgv.Rows.Count - 2).Cells("PaxHasta").Value) + 1
        '            ''    End If

        '            ''    Dim frm2 As New frmCalculoCostos
        '            ''    frm2.FrmRef = Me

        '            ''    frm2.intDesde = intDesde
        '            ''    frm2.ShowDialog()

        '            ''    dgv.CurrentCell = dgv.Rows(dgv.Rows.Count - 1).Cells("PaxHasta")
        '            ''    dgv.Focus()
        '            ''End If
        '        Catch ex As Exception
        '            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        '        End Try

    End Sub

    Private Sub chkSelTodos_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelTodos.CheckedChanged
        If chkSelTodos.Checked Then
            For Each FilaDgv As DataGridViewRow In dgv.Rows
                If Not FilaDgv.IsNewRow Then
                    FilaDgv.Cells("Sel").Value = True
                End If
            Next
        Else
            For Each FilaDgv As DataGridViewRow In dgv.Rows
                If Not FilaDgv.IsNewRow Then
                    FilaDgv.Cells("Sel").Value = False
                End If
            Next

        End If
        dgv_CellContentClick(Nothing, Nothing)

    End Sub


    Private Sub chkSelTodosAPT_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkSelTodosAPT.CheckedChanged
        If chkSelTodosAPT.Checked Then
            For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
                If Not FilaDgv.IsNewRow Then
                    FilaDgv.Cells("Sel2").Value = True
                End If
            Next
        Else
            For Each FilaDgv As DataGridViewRow In dgvAPT.Rows
                If Not FilaDgv.IsNewRow Then
                    FilaDgv.Cells("Sel2").Value = False
                End If
            Next

        End If
        dgvAPT_CellContentClick(Nothing, Nothing)

    End Sub

    Private Sub txtMaximoLiberado_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtMaximoLiberado.TextChanged

    End Sub


    Private Sub btnBuscarDetServicio_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarDetServicio.Click
        'Dim frmCons As New frmConsDetServiciosProveedor

        'intIDServicio_Det_V = 0
        'frmCons.strIDTipoProv = strIDTipoProv
        'frmCons.frmRefMantDetServiciosProveedorDato = Me
        'frmCons.ShowDialog()

        'If intIDServicio_Det_V <> 0 Then
        '    lblIDServicio_Det_V.Text = intIDServicio_Det_V

        '    blnPintarCorrel = False
        '    CargarControles(intIDServicio_Det_V)
        '    blnPintarCorrel = True
        '    pEnableControlsGroupBox(grb, False)
        '    btnBuscarDetServicio.Enabled = True

        '    If MessageBox.Show("Por favor Acepte si considera que los datos son correctos para proceder a grabar o seleccione otro servicio", gstrTituloMsg, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then

        '        pGrabar(Nothing, Nothing)

        '    End If
        'End If

        Try

            Dim objServBN As New clsServicioProveedorBN
            If objServBN.blnExisteenCotiz(strIDServicio) Then
                'MessageBox.Show("No se puede cambiar de servicio relacionado. Existen cotizaciones que hacen referencia a este servicio", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                blnCambioServRelAcepto = True
                If strModoEdicion = "E" Then
                    blnCambioServRel = True
                End If

                If MessageBox.Show("Existen cotizaciones que hacen referencia a este servicio." & Chr(13) & "¿Desea Ud. continuar?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.No Then
                    blnCambioServRelAcepto = False
                    Exit Sub
                End If
            End If


            Dim frmCons As New frmMantServiciosProveedor
            strIDServicioNue = ""
            frmCons.blnFiltrarTodosServicioyVarios = True
            If Not FormRefMantServiciosProveedor Is Nothing Then
                frmCons.intIDCab = FormRefMantServiciosProveedor.intIDCab
            End If
            frmCons.frmRefIngDetServicioDato = Me
            frmCons.ShowDialog()
            If strIDServicioNue <> "" Then

                Dim objBL As New clsServicioProveedorBN
                Dim dtt As DataTable = objBL.ConsultarTodosxIDServicioAnioTipo(strIDServicioNue, strAnioNue, strTipoNue)
                If dtt.Rows.Count > 0 Then
                    ActivarDesactivarBotones()
                    intIDServicio_Det_V = dtt(0)("IDServicio_Det")

                    If Not blnIDServicio_DetRelIgual Then

                        For Each dgvRowCosto As DataGridViewRow In dgv.Rows
                            AgregarEditarValorStruct(dgvRowCosto.Index, "B")
                        Next
BorrarCostos:
                        For Each dgvRowCosto As DataGridViewRow In dgv.Rows
                            If Not dgvRowCosto.IsNewRow Then
                                dgv.Rows.Remove(dgvRowCosto)
                                GoTo BorrarCostos
                            End If
                        Next


                        '----
                        For Each dgvRowCosto As DataGridViewRow In dgvAPT.Rows
                            AgregarEditarValorStructAPT(dgvRowCosto.Index, "B")
                        Next
BorrarCostosAPT:
                        For Each dgvRowCosto As DataGridViewRow In dgvAPT.Rows
                            If Not dgvRowCosto.IsNewRow Then
                                dgvAPT.Rows.Remove(dgvRowCosto)
                                GoTo BorrarCostosAPT
                            End If
                        Next

                    End If

                    blnPintarCorrel = False
                    CargarControles(intIDServicio_Det_V)
                    blnPintarCorrel = True

                    pEnableControlsGroupBox(grb, False)
                    txtTipo.Enabled = True : txtDetaTipo.Enabled = True
                    btnBuscarDetServicio.Enabled = True
                    blnIDServicio_DetRelIgual = False
                    'If MessageBox.Show("Por favor Acepte si considera que los datos son correctos para proceder a grabar o seleccione otro servicio", gstrTituloMsg, MessageBoxButtons.OKCancel, MessageBoxIcon.Question) = Windows.Forms.DialogResult.OK Then

                    'Dim objBT As New clsServicioProveedorBT
                    'objBT.InsertarDetallesxIDServicio(dtt, gstrUser, strIDServicio)

                    'MessageBox.Show("Se creó el Detalle Servicio " & txtDescripcion.Text & " exitosamente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)

                    'FormRefMantServiciosProveedor.dgv_CellEnter(Nothing, Nothing)
                    'Me.Close()
                    'frmMain.DesabledBtns()

                    'End If
                Else
                    MessageBox.Show("No se encontraron detalles en el servicio seleccionado", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If

            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        End Try

    End Sub

    Private Function blnCambioCostos() As Boolean
        Try
            Dim blnCambio As Boolean = False
            Dim dcCompare As New Dictionary(Of String, Double)
            With dcCompare
                .Add(txtMonto_dbls.Name, CDbl(txtMonto_dbls.Text))
                .Add(txtMonto_sgls.Name, CDbl(txtMonto_sgls.Text))
                .Add(txtMonto_tris.Name, CDbl(txtMonto_tris.Text))
                .Add(txtMonto_dbl.Name, CDbl(txtMonto_dbl.Text))
                .Add(txtMonto_sgl.Name, CDbl(txtMonto_sgl.Text))
                .Add(txtMonto_tri.Name, CDbl(txtMonto_tri.Text))
                .Add(txtTC.Name, CDbl(txtTC.Text))
                .Add(txtDiferSS.Name, If(txtDiferSS.Text.Trim = "", 0, CDbl(txtDiferSS.Text)))
                .Add(txtDiferST.Name, If(txtDiferST.Text.Trim = "", 0, CDbl(txtDiferST.Text)))
                .Add(txtMontoL.Name, CDbl(txtMontoL.Text))
                For Each DgvRowItem As DataGridViewRow In dgv.Rows
                    If DgvRowItem.Cells("PaxDesde").Value IsNot Nothing And DgvRowItem.Cells("PaxHasta").Value IsNot Nothing Then
                        .Add(DgvRowItem.Cells("PaxDesde").Value.ToString & "-" & DgvRowItem.Cells("PaxHasta").Value.ToString, _
                             CDbl(DgvRowItem.Cells("Monto").Value))
                    End If
                Next
            End With

            For Each dc As KeyValuePair(Of String, Double) In dcValoresCostos
                If dcCompare.ContainsKey(dc.Key) Then
                    If dc.Value <> dcCompare.Item(dc.Key) Then
                        blnCambio = True
                        Exit For
                    End If
                Else
                    blnCambio = True
                    Exit For
                End If
            Next

            Return blnCambio
        Catch ex As Exception
            Throw
        End Try
    End Function


    Private Sub dgv_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.RowEnter

        If dgv.CurrentRow Is Nothing Then Exit Sub
        'MessageBox.Show("Fila:" & dgv.CurrentRow.Index.ToString)
        'MessageBox.Show("Tot:" & dgv.Rows.Count.ToString)
        'MessageBox.Show(dgv.CurrentCell.ColumnIndex)


        Dim intFila As Byte = dgv.Rows.Count - 1
        'If Not dgv.Item("PaxDesde", intFila).Value Is Nothing Then
        ' If dgv.Item("PaxDesde", intFila).Value.ToString = "" Then

        If dgv.Rows(intFila).IsNewRow Then
            If IsDBNull(dgv.Item("PaxHasta", intFila - 1).Value) = False Then
                'If dgv.CurrentCell.ColumnIndex <> dgv.Columns("Sel").Index Then
                'If dgv.Item("Sel", intFila - 1).Value = False Then
                If Not dgv.Item("PaxHasta", intFila - 1).Value Is Nothing Then
                    dgv.Item("PaxDesde", intFila).Value = dgv.Item("PaxHasta", intFila - 1).Value + 1

                End If

            End If


        End If
        'End If
        'End If

    End Sub


    Private Sub dgvAPT_RowEnter(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvAPT.RowEnter
        If dgvAPT.CurrentRow Is Nothing Then Exit Sub
        'MessageBox.Show("Fila:" & dgv.CurrentRow.Index.ToString)
        'MessageBox.Show("Tot:" & dgv.Rows.Count.ToString)
        'MessageBox.Show(dgv.CurrentCell.ColumnIndex)

        Dim intFila As Byte = dgvAPT.Rows.Count - 1
        'If Not dgv.Item("PaxDesde", intFila).Value Is Nothing Then
        ' If dgv.Item("PaxDesde", intFila).Value.ToString = "" Then

        If dgvAPT.Rows(intFila).IsNewRow Then
            If IsDBNull(dgvAPT.Item("PaxHasta2", intFila - 1).Value) = False Then
                'If dgv.CurrentCell.ColumnIndex <> dgv.Columns("Sel").Index Then
                'If dgv.Item("Sel", intFila - 1).Value = False Then
                If Not dgvAPT.Item("PaxHasta2", intFila - 1).Value Is Nothing Then
                    dgvAPT.Item("PaxDesde2", intFila).Value = dgvAPT.Item("PaxHasta2", intFila - 1).Value + 1

                End If

            End If


        End If
        'End If
        'End If

    End Sub
    'Private Sub chkAfecto_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkAfecto.CheckedChanged
    '    If strModoEdicion = "N" Then
    '        If chkAfecto.Checked Then
    '            cboIdTipoOC.SelectedValue = gstrTipoOperacContabGravado
    '        Else
    '            cboIdTipoOC.SelectedValue = gstrTipoOperacContabExportac
    '        End If
    '    End If
    'End Sub

    Private Sub chkConAlojamiento_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkConAlojamiento.CheckedChanged
        If chkConAlojamiento.Checked Then
            lblDiferSSEtiq.Enabled = True
            txtDiferSS.Enabled = True
            lblDiferSTEtiq.Enabled = True
            txtDiferST.Enabled = True
            txtDiferSS.Focus()

            If strIDTipoProv = gstrTipoProveeOperadores Then
                chkVerDetaTipo.Visible = True
                'chkVerDetaTipo.Enabled = True
                'lblEtiqCodTarifa.Visible = True
                txtCodTarifa.Visible = True
                'txtCodTarifa.Enabled = chkVerDetaTipo.Enabled
                tbgRangosAPT.Parent = tbc
            End If
        Else
            lblDiferSSEtiq.Enabled = False
            txtDiferSS.Enabled = False
            txtDiferSS.Text = "0.00"
            lblDiferSTEtiq.Enabled = False
            txtDiferST.Enabled = False
            txtDiferST.Text = "0.00"

            If strIDTipoProv = gstrTipoProveeOperadores Then
                chkVerDetaTipo.Visible = False
                chkVerDetaTipo.Checked = False
                'lblEtiqCodTarifa.Visible = False
                txtCodTarifa.Visible = False
                txtCodTarifa.Text = ""
                tbgRangosAPT.Parent = Nothing
            End If
        End If
    End Sub

    Private Sub chkPlanAlimenticio_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkPlanAlimenticio.CheckedChanged
        If Not chkPlanAlimenticio.Visible Then Exit Sub
        If chkPlanAlimenticio.Checked Then
            'lblMontoSimpleEtiq.Text = "Monto S/."
            lblMontoSimpleEtiq.Text = "Monto " & strCoSimboloMonedaPais
            lblMontoSimpleEtiq2.Text = "Monto US$"

            lblMontoDobleEtiq.Visible = False
            txtMonto_dbls.Visible = False
            txtMonto_dbls.Text = "0.00"
            lblMontoDobleEtiq2.Visible = False
            txtMonto_dbl.Visible = False
            txtMonto_dbl.Text = "0.00"

            lblMontoTripleEtiq.Visible = False
            txtMonto_tri.Visible = False
            txtMonto_tri.Text = "0.00"
            lblMontoTripleEtiq2.Visible = False
            txtMonto_tris.Visible = False
            txtMonto_tris.Text = "0.00"

            cboCodxTriple.Visible = False
            lblCodxTripleEtiq.Visible = False

            lblDefinicionTripleEtiq.Visible = False
            cboHabitTriple.Visible = False
            cboHabitTriple.SelectedIndex = -1
        Else
            If strIDTipoProv = gstrTipoProveeHoteles Then
                'lblMontoSimpleEtiq.Text = "Habitación Simple S/."
                lblMontoSimpleEtiq.Text = "Habitación Simple " & strCoSimboloMonedaPais
                lblMontoSimpleEtiq2.Text = "Habitación Simple US$"

                lblMontoDobleEtiq.Visible = True
                txtMonto_dbls.Visible = True
                lblMontoDobleEtiq2.Visible = True
                txtMonto_dbl.Visible = True

                lblMontoTripleEtiq.Visible = True
                txtMonto_tri.Visible = True
                lblMontoTripleEtiq2.Visible = True
                txtMonto_tris.Visible = True

                cboCodxTriple.Visible = True
                lblCodxTripleEtiq.Visible = True

                lblDefinicionTripleEtiq.Visible = True
                cboHabitTriple.Visible = True
            End If
            'lblDefinicionTripleEtiq.Visible = True
            'cboHabitTriple.Visible = True
        End If
    End Sub

    Private Sub dgv_RowValidated(ByVal sender As Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgv.RowValidated
        Try
            If blnLoad Then
                dgv.CurrentCell = dgv("PaxHasta", e.RowIndex)
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub lvw_DoubleClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles lvw.DoubleClick
        Try
            'capturar texto 
            Dim strTexto As String = lvw.SelectedItems(0).SubItems(0).Text
            txtDescripcion.Text = strTexto
            lvw.Visible = False
            blnDoubleClickLvw = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtCodTarifa_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtCodTarifa.TextChanged
        ErrPrv.SetError(txtCodTarifa, "")
    End Sub

    Private Sub chkVerDetaTipo_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkVerDetaTipo.CheckedChanged
        If chkVerDetaTipo.Checked Then
            txtCodTarifa.Enabled = True
            txtCodTarifa.Focus()
        Else
            txtCodTarifa.Enabled = False
            txtCodTarifa.Text = ""
        End If
        ErrPrv.SetError(txtCodTarifa, "")
        ErrPrv.SetError(txtDetaTipo, "")
    End Sub
    Private Sub CargarCombo_CuentasContables()
        Try

            Dim strTipoOpe As String = ""

            If cboIdTipoOC.Text <> "" Then
                strTipoOpe = cboIdTipoOC.SelectedValue.ToString
            End If

            Dim strEsProvAdmin As String = ""

            If strIDTipoProv = "019" Then
                strEsProvAdmin = "S"
            Else
                strEsProvAdmin = "N"
            End If

            Dim objBN As New clsDocumentoProveedorBN
            dttCuentasContables = New DataTable
            dttCuentasContables = objBN.ConsultarCtaContable(strTipoOpe, strEsProvAdmin)
            'pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable(strTipoOpe), False)
            pCargaCombosBox(cboCuentaContable, dttCuentasContables, False)
            If cboCuentaContable.Items.Count = 1 Then
                cboCuentaContable.SelectedIndex = 0
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub CargarCombo_Temporada()
        Dim objBNTemporada As New clsTablasApoyoBN.clsTemporadaBN
        pCargaCombosBox(cboTemporada, objBNTemporada.ConsultarCboXTipoServ(strIDServicio), False)
    End Sub

    Private Sub CargarCombo_CuentasContables_CeCos()
        Try


            Dim strCecos As String = ""


            If cboCentroCostos.Items.Count > 0 Then
                If Not cboCentroCostos.SelectedValue Is Nothing Then
                    strCecos = cboCentroCostos.SelectedValue.ToString
                End If
            End If

            Dim objBN As New clsDocumentoProveedorBN
            dttCuentasContables = New DataTable
            dttCuentasContables = objBN.ConsultarCtaContable_CentroCostos_Servicios(strCecos)
            'pCargaCombosBox(cboCuentaContable, objBN.ConsultarCtaContable(strTipoOpe), False)
            pCargaCombosBox(cboCuentaContable, dttCuentasContables, False)
            If cboCuentaContable.Items.Count = 1 Then
                cboCuentaContable.SelectedIndex = 0
            End If

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboCentroCostos_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboCentroCostos.SelectionChangeCommitted
        Try
            Dim strValor1 As String = sender.SelectedValue.ToString
            If strValor1 <> "System.Data.DataRowView" Then
                ErrPrv.SetError(cboCentroCostos, "")
                strCoCeCos = strValor1

                CargarCombo_CuentasContables_CeCos()
                CargarCombo_TiposdeOperacion_CeCos()

            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarCombo_TiposdeOperacion_CeCos()
        Try


            Dim strCecos As String = ""

            If cboCentroCostos.Items.Count > 0 Then
                If Not cboCentroCostos.SelectedValue Is Nothing Then
                    strCecos = cboCentroCostos.SelectedValue.ToString
                End If

            End If

            Dim objBN As New clsDocumentoProveedorBN
            dttTipoOperaciones = New DataTable
            dttTipoOperaciones = objBN.ConsultarTipoOperacionCont_CentroCostos(strCecos)
            pCargaCombosBox(cboIdTipoOC, dttTipoOperaciones, False)


        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboIdTipoOC_SelectionChangeCommitted(ByVal sender As Object, ByVal e As System.EventArgs) Handles cboIdTipoOC.SelectionChangeCommitted
        Try
            ''RemoveHandler cboCuentaContable.DrawItem, AddressOf cboCuentaContable_DrawItem
            ''AddHandler cboIdTipoOC.DrawItem, AddressOf cboIdTipoOC_DrawItem
            'CargarCombo_CuentasContables()
            ''RemoveHandler cboCuentaContable.DrawItem, AddressOf cboCuentaContable_DrawItem
            ''AddHandler cboIdTipoOC.DrawItem, AddressOf cboIdTipoOC_DrawItem
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub cboCuentaContable_DrawItem(sender As Object, e As DrawItemEventArgs) 'Handles cboCuentaContable.DrawItem
        If Not blnLoad Then Exit Sub
        If sender Is Nothing Then Exit Sub
        If e Is Nothing Then Exit Sub
        If IsNothing(Me) Then Exit Sub
        Try
            If e.Index = -1 Then
                Exit Sub
            End If
            If cboCuentaContable Is Nothing Then Exit Sub
            If dttCuentasContables Is Nothing Then Exit Sub

            Dim p As Point = New Point(cboCuentaContable.Location.X + 120, cboCuentaContable.Location.Y + cboCuentaContable.Height + (25 + e.Index * 10))

            Dim text As String = cboCuentaContable.GetItemText(cboCuentaContable.Items(e.Index))
            Dim indice As Int16 = e.Index
            Dim definicion As String = ""
            If dttCuentasContables.Rows.Count > 0 Then
                definicion = dttCuentasContables.Rows(indice).Item("TxDefinicion").ToString
            End If
            e.DrawBackground()

            Using br As SolidBrush = New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using

            If Not cboCuentaContable.Enabled Then Exit Sub
            Dim intX, intY As Int16
            If FormRefMantServiciosProveedor.frmRefIngDetCotiDatoMem Is Nothing Then
                intX = MousePosition.X - 200
                intY = MousePosition.Y - 73
            Else
                intX = cboCuentaContable.Location.X + cboCuentaContable.Width
                intY = MousePosition.Y
            End If

            'If Not (MousePosition.X >= 356 And MousePosition.Y <= 660) And _
            '    Not (MousePosition.Y >= 544 And MousePosition.Y <= 560) Then
            '    Exit Sub
            'End If


            If (e.State And DrawItemState.Selected) = (DrawItemState.Selected And cboCuentaContable.DroppedDown) Then
                If dttCuentasContables.Rows.Count > 0 Then
                    If definicion.Trim.Length > 0 Then
                        definicion = SplitToolTip(definicion)
                    End If

                    If Not (e.State = (DrawItemState.NoAccelerator Or DrawItemState.NoFocusRect Or DrawItemState.ComboBoxEdit) Or _
                            e.State = (DrawItemState.NoAccelerator Or DrawItemState.ComboBoxEdit)) Then
                        Me.ToolTip1.Show(definicion, Me, intX, intY)
                    End If
                    'Me.ToolTip1.Show(definicion, Me, intX, intY)
                End If
            Else
                Me.ToolTip1.Hide(cboCuentaContable)
            End If
            e.DrawFocusRectangle()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub


    Private Sub cboIdTipoOC_DrawItem(sender As Object, e As DrawItemEventArgs) 'Handles cboCuentaContable.DrawItem
        If Not blnLoad Then Exit Sub
        If sender Is Nothing Then Exit Sub
        If e Is Nothing Then Exit Sub
        If IsNothing(Me) Then Exit Sub
        Try
            If e.Index = -1 Then
                Exit Sub
            End If
            If cboIdTipoOC Is Nothing Then Exit Sub
            If dttTipoOperaciones Is Nothing Then Exit Sub

            Dim p As Point = New Point(cboIdTipoOC.Location.X + 120, cboIdTipoOC.Location.Y + cboIdTipoOC.Height + (25 + e.Index * 10))
            Dim text As String = cboIdTipoOC.GetItemText(cboIdTipoOC.Items(e.Index))

            'Dim valor As String = cboCuentaContable.Ge
            Dim indice As Integer = e.Index
            Dim definicion As String = ""
            If dttTipoOperaciones.Rows.Count > 0 Then
                definicion = dttTipoOperaciones.Rows(indice).Item("TxDefinicion").ToString
            End If
            e.DrawBackground()

            Using br As SolidBrush = New SolidBrush(e.ForeColor)
                e.Graphics.DrawString(text, e.Font, br, e.Bounds)
            End Using

            'If Not (MousePosition.X >= 25 And MousePosition.X <= 500) And _
            '   Not (MousePosition.Y >= 544 And MousePosition.Y <= 560) Then
            '    Exit Sub
            'End If

            If Not cboIdTipoOC.Enabled Then Exit Sub
            Dim intX, intY As Integer
            If FormRefMantServiciosProveedor.frmRefIngDetCotiDatoMem Is Nothing Then
                intX = MousePosition.X - 200
                intY = MousePosition.Y - 73
            Else
                intX = cboIdTipoOC.Location.X + cboIdTipoOC.Width 'MousePosition.X + 50
                intY = MousePosition.Y
            End If

            If (e.State And DrawItemState.Selected) = (DrawItemState.Selected And cboIdTipoOC.DroppedDown) Then
                If dttTipoOperaciones.Rows.Count > 0 Then
                    If definicion.Trim.Length > 0 Then
                        definicion = SplitToolTip(definicion)
                    End If
                    If Not (e.State = (DrawItemState.NoAccelerator Or DrawItemState.NoFocusRect Or DrawItemState.ComboBoxEdit) Or _
                            e.State = (DrawItemState.NoAccelerator Or DrawItemState.ComboBoxEdit)) Then
                        Me.ToolTip1.Show(definicion, Me, intX, intY)
                    End If
                End If
            Else
                Me.ToolTip1.Hide(cboIdTipoOC)
            End If
            e.DrawFocusRectangle()
            'My.Application.DoEvents()
        Catch ex As Exception
            'MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Friend Function SplitToolTip(ByVal strOrig As String) As String
        Dim strArray As String()
        Dim SPACE As String = " "
        Dim CR As String = vbCrLf
        Dim strOneWord As String
        Dim strBuilder As String = ""
        Dim strReturn As String = ""
        strArray = strOrig.Split(SPACE)
        For Each strOneWord In strArray
            strBuilder = strBuilder & strOneWord & SPACE
            If Len(strBuilder) > 70 Then
                strReturn = strReturn & strBuilder & CR
                strBuilder = ""
            End If
        Next
        If Len(strBuilder) < 8 Then
            If strReturn.Length > 2 Then
                strReturn = strReturn.Substring(0, strReturn.Length - 2)
            Else
                strReturn = ""
            End If

        End If
        Return strReturn & strBuilder
    End Function

    Private Sub cboTemporada_SelectionChangeCommitted(sender As Object, e As EventArgs) Handles cboTemporada.SelectionChangeCommitted
        Try
            Dim strValor1 As String = sender.SelectedValue.ToString
            If strValor1 <> "System.Data.DataRowView" Then
                ''Actualizar_Descripcion_Alterna(intIDTemporada)
                intIDTemporada = Int16.Parse(strValor1)
            Else
                intIDTemporada = 0
            End If
            Cargar_RangoXTemporada(intIDTemporada)
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub Cargar_RangoXTemporada(ByVal vintIDTemporada As Integer)
        dttRangosAPT.Rows.Clear()
        For Each ST As stRangosAPT In objLstAPT
            If ST.IDTemporada = vintIDTemporada And ST.strAccion <> "B" Then
                dttRangosAPT.Rows.Add(ST.ID, ST.PaxDesde, ST.PaxHasta, ST.Monto)
            End If
        Next
        dgvAPT.DataSource = dttRangosAPT
        dgvAPT.Columns("Correlativo2").Visible = False
        Dim _stDescApt As stDescRangosAPT
        _stDescApt = GetDescAlterStructDescAPT(vintIDTemporada)
        txtDescAlter.Text = _stDescApt.DescripAlter
        txtSinpIndi.Text = _stDescApt.SingleSupplement
        chkPolLibAPT.Checked = _stDescApt.PoliticaLiberado
        txtLiberadoAPT.Text = _stDescApt.Liberado
        If (_stDescApt.TipoLib <> "") Then
            cboTipoLibAPT.SelectedValue = _stDescApt.TipoLib
        Else
            cboTipoLibAPT.SelectedIndex = -1
        End If
        txtMaximoLiberadoAPT.Text = _stDescApt.MaximoLiberado
        txtMontoLAPT.Text = _stDescApt.MontoL
    End Sub

    Private Sub Cargar_Todos_Rangos(ByVal pintIDServicio_Det_V As Integer)
        Dim objBNDetAPT As New clsServicioProveedorBN.clsDetalleServicioProveedorBN.clsRangosAPTDetalleServicioProveedorBN

        Dim dtDetAPT As New DataTable

        If pintIDServicio_Det_V = 0 Then
            dtDetAPT = objBNDetAPT.ConsultarListxIDDetalle(strIDServicio_Det)
        Else
            If Not blnIDServicio_DetRelIgual Then
                dtDetAPT = objBNDetAPT.ConsultarListxIDDetalle(pintIDServicio_Det_V)
            End If
        End If
        Dim _intIDTemp As Integer = 0
        For Each drDet As DataRow In dtDetAPT.Rows
            If _intIDTemp <> Integer.Parse(drDet("IDTemporada").ToString) Then
                _intIDTemp = Integer.Parse(drDet("IDTemporada").ToString)
                AgregarEditarValorStructDescAPT(_intIDTemp, drDet("DescripcionAlterna").ToString, drDet("SingleSupplement").ToString, _
                                                Boolean.Parse(drDet("PoliticaLiberado").ToString), Byte.Parse(drDet("Liberado").ToString),
                                                drDet("TipoLib").ToString, Byte.Parse(drDet("MaximoLiberado").ToString), _
                                                Double.Parse(drDet("MontoL").ToString))
            End If

            Dim RegValor = New stRangosAPT
            RegValor.IDTemporada = Integer.Parse(drDet("IDTemporada").ToString)
            RegValor.ID = Byte.Parse(drDet("Correlativo").ToString)
            RegValor.PaxDesde = Short.Parse(drDet("PaxDesde").ToString)
            RegValor.PaxHasta = Short.Parse(drDet("PaxHasta").ToString)
            RegValor.Monto = Double.Parse(drDet("Monto").ToString)
            RegValor.strAccion = ""

            objLstAPT.Add(RegValor)
        Next

        If dtDetAPT.Rows.Count > 0 Then
            If cboTemporada.Items.Count > 0 Then
                cboTemporada.SelectedIndex = 0
                cboTemporada_SelectionChangeCommitted(cboTemporada, Nothing)
            End If
        End If

        If intIDTemporada <> 0 Then
            Cargar_RangoXTemporada(intIDTemporada)
        End If

    End Sub

    Private Sub txtDescAlter_LostFocus(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtDescAlter.LostFocus, txtSinpIndi.LostFocus, _
                                                                                                    txtLiberadoAPT.LostFocus, cboTipoLibAPT.LostFocus, _
                                                                                                    txtMaximoLiberadoAPT.LostFocus, txtMontoLAPT.LostFocus, _
                                                                                                    chkPolLibAPT.LostFocus
        AgregarEditarValorControlAPT(intIDTemporada)
    End Sub

    Private Sub AgregarEditarValorControlAPT(ByVal vintIDTemporada As Integer)
        Dim lbytLiberado As Byte = 0, lstrTipoLib As String = "", lbytMaximoLiberado As Byte = 0, ldobMontoL As Double = 0
        If chkPolLibAPT.Checked = True Then
            If (IsNumeric(txtLiberadoAPT.Text) = False) Then

            Else
                lbytLiberado = Byte.Parse(txtLiberadoAPT.Text)
            End If

            If cboTipoLibAPT.Text.Trim = "" Then
                ErrPrv.SetError(cboTipoLibAPT, "Seleccione Tipo de Liberado.")
            Else
                ErrPrv.SetError(cboTipoLibAPT, "")
                lstrTipoLib = cboTipoLibAPT.SelectedValue.ToString
            End If
            If (IsNumeric(txtMaximoLiberadoAPT.Text) = False) Then

            Else
                lbytMaximoLiberado = Byte.Parse(txtMaximoLiberadoAPT.Text)
            End If
            If (IsNumeric(txtMontoLAPT.Text) = False) Then

            Else
                ldobMontoL = Double.Parse(txtMontoLAPT.Text)
            End If
        End If
        AgregarEditarValorStructDescAPT(intIDTemporada, txtDescAlter.Text.Trim, txtSinpIndi.Text, chkPolLibAPT.Checked, _
                                            lbytLiberado, lstrTipoLib, lbytMaximoLiberado, ldobMontoL)
    End Sub

    Private Sub AgregarEditarValorStructDescAPT(ByVal vintIDTemporada As Integer, ByVal vstrDescAlter As String, ByVal vstrSingleSupplement As String, _
                                                ByVal vbolPoliticaLiberado As Boolean, ByVal vbytLiberado As Byte, ByVal vstrTipoLib As String, _
                                                ByVal vbytMaximoLiberado As Byte, ByVal vdobMontoL As Double)
        If vintIDTemporada = 0 Then Exit Sub
        Dim _intFila As Integer = 0
        For Each stDescApt As stDescRangosAPT In objLstDescAPT
            If stDescApt.IDTemporada = vintIDTemporada Then
                objLstDescAPT.RemoveAt(_intFila)
                Exit For
            End If
            _intFila += 1
        Next
        Dim _dSS As Double
        _dSS = Double.Parse(vstrSingleSupplement)
        Dim _stDescApt As stDescRangosAPT
        _stDescApt.IDTemporada = vintIDTemporada
        _stDescApt.DescripAlter = vstrDescAlter
        _stDescApt.SingleSupplement = _dSS
        _stDescApt.PoliticaLiberado = vbolPoliticaLiberado
        _stDescApt.Liberado = vbytLiberado
        _stDescApt.TipoLib = vstrTipoLib
        _stDescApt.MaximoLiberado = vbytMaximoLiberado
        _stDescApt.MontoL = vdobMontoL
        objLstDescAPT.Add(_stDescApt)
    End Sub

    Private Function GetDescAlterStructDescAPT(ByVal vintIDTemporada As Integer) As stDescRangosAPT
        Dim _strDescAlter As stDescRangosAPT
        _strDescAlter.IDTemporada = vintIDTemporada
        _strDescAlter.DescripAlter = ""
        _strDescAlter.SingleSupplement = 0
        _strDescAlter.PoliticaLiberado = False
        _strDescAlter.Liberado = 0
        _strDescAlter.TipoLib = ""
        _strDescAlter.MaximoLiberado = 0
        _strDescAlter.MontoL = 0
        If vintIDTemporada = 0 Then Return _strDescAlter
        For Each stDescApt As stDescRangosAPT In objLstDescAPT
            If stDescApt.IDTemporada = vintIDTemporada Then
                _strDescAlter = stDescApt
                Exit For
            End If
        Next
        Return _strDescAlter
    End Function

    Private Sub EliminarStructDescAPT(ByVal vintIDTemporada As Integer)
        If vintIDTemporada = 0 Then Exit Sub
        Dim _intFila As Integer = 0
        Dim _bolExiste As Boolean = False
        For Each stApt As stRangosAPT In objLstAPT
            If stApt.IDTemporada = vintIDTemporada And stApt.strAccion <> "B" Then
                _bolExiste = True
                Exit For
            End If
        Next
        If _bolExiste = False Then
            For Each stDescApt As stDescRangosAPT In objLstDescAPT
                If stDescApt.IDTemporada = vintIDTemporada Then
                    objLstDescAPT.RemoveAt(_intFila)
                    Exit For
                End If
                _intFila += 1
            Next
        End If
    End Sub

    Private Sub chkPolLibAPT_CheckedChanged(sender As Object, e As EventArgs) Handles chkPolLibAPT.CheckedChanged
        If Not chkPolLibAPT.Checked Then
            txtLiberadoAPT.Text = "0"
            cboTipoLibAPT.SelectedValue = ""
            txtMontoLAPT.Text = "0"
            txtMaximoLiberadoAPT.Text = "0"
        End If
        gpbPolLibAPT.Enabled = chkPolLibAPT.Checked
    End Sub
End Class