﻿Imports ComSEToursBL2
Imports ComSEToursBE2
Public Class frmIngPedidoDato
    Dim blnNuevoEnabled As Boolean = True
    Dim blnGrabarEnabled As Boolean = False
    Dim blnDeshacerEnabled As Boolean = False
    Dim blnEditarEnabled As Boolean = False
    Dim blnEliminarEnabled As Boolean = False
    Dim blnSalirEnabled As Boolean = True
    Dim blnImprimirEnabled As Boolean = False
    Dim blnConsultarEnabledBD As Boolean = False
    Dim blnGrabarEnabledBD As Boolean = False
    Dim blnEditarEnabledBD As Boolean = False
    Dim blnEliminarEnabledBD As Boolean = False
    Dim dtUsuarios_Combo As DataTable
    Public intNuPedInt As Integer = 0
    Public strNuPedido As String = ""
    Public FormRef As frmIngPedido
    Dim strModoEdicion As Char
    Public strCoCliente As String = ""
    Public strCoPais As String = ""
    Public blnClienteTop As Boolean = False
    Public bytPosClienteTop As Byte = 0
    Public blnEditarCotizac As Boolean = False
    Dim blnPerfilClienteCargado As Boolean = False
    Dim blnBandCorreosCargada As Boolean = False
    Dim strCoArea As String = ""
    Dim strCoProvClie As String = ""
    Dim intIDCab As Integer = 0
    Dim strIDFile As String = ""
    Dim strTitulo As String = ""
    Dim blnCambios As Boolean = False
    Dim blnLoad As Boolean = False
    Public blnEnvio As Boolean = False
    Public strTipoFormArchivos As String = "VEN"
    Dim blnCargaIniStruct As Boolean = False
    Public strTituloFile As String = ""
    Private Structure stGuias
        Dim CoProveedor As String
        Dim NombreCorto As String
        Dim IDubigeo As String
        Dim strAccion As String
    End Structure
    Dim objLstGuias As New List(Of stGuias)

    Public blnActualizoArchivo As Boolean = False
    Public blnEliminarArchivo As Boolean = False
    Public datFechaFile As Date = Now

    'Archivos
    Public Structure stArchivos

        Dim IDArchivo As Integer
        Dim Nombre As String
        Dim RutaOrigen As String
        Dim RutaDestino As String
        Dim CoTipo As String
        Dim Tipo As String
        Dim FlCopiado As Boolean
        Dim FechaModificacion As Date
        Dim CoUsuarioCreacion As String
        Dim Usuario As String
        Dim Accion As String
    End Structure
    Public objLstArchivos As New List(Of stArchivos)
    Dim intIDCab_File As Integer = 0
    Dim strIDFileAceptado As String = ""

    Private Sub frmIngPedidoDato_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        If blnCambios = True Then

            If MessageBox.Show("Se han realizado cambios. ¿Está Ud. seguro de salir sin grabar?", "Confirmar", MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                e.Cancel = False


                'frmMain.DesabledBtns()
            Else
                e.Cancel = True

            End If
        Else

            'frmMain.DesabledBtns()
        End If
    End Sub
    Private Sub frmIngPedidoDato_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Try
            cboCoEjeVta.DrawMode = DrawMode.OwnerDrawFixed
            cboCoEjeVta.BackColor = gColorTextBoxObligat
            cboEstado.BackColor = gColorTextBoxObligat
            CargarCombos()
            chkActivos.Checked = True

            CargarSeguridad()
            ActivarDesactivarBotones()

            dgv.ColumnHeadersDefaultCellStyle.Font = New Font(dgv.Font, FontStyle.Bold)
            dgv.DefaultCellStyle.SelectionBackColor = gColorSelectGrid
            dgv.DefaultCellStyle.SelectionForeColor = System.Drawing.Color.Black

            If intNuPedInt <> 0 Then
                ModoEditar()
                CargarControles()
                ConsultarCotizaciones()
                MostrarOcultarTabArchivos()
            Else
                ModoNuevo()
            End If
            If Not blnEditarCotizac Then
                btnNuevo.Enabled = False
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarCombos()
        Try
            CargarCombo_Usuario_Activos()

            Dim dttEstados As New DataTable("Estados")
            With dttEstados
                .Columns.Add("Codigo")
                .Columns.Add("Descripcion")
                .Rows.Add("PD", "PENDIENTE")
                .Rows.Add("AC", "ACEPTADO")
                .Rows.Add("EL", "ELIMINADO")
                .Rows.Add("NA", "NO ACEPTADO")
                .Rows.Add("AN", "ANULADO")
            End With
            pCargaCombosBox(cboEstado, dttEstados)

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarCombo_Usuario_Activos()
        Try
            Dim strActivos As String = ""
            If chkActivos.Checked Then
                strActivos = "A"
            End If
            Dim strNombreForm As String = Me.Name
            strNombreForm = Replace(strNombreForm, "frm", "mnu")
            strNombreForm = Replace(strNombreForm, "Dato", "")
            Dim objBLUser As New clsUsuarioBN
            Dim dtCombo As DataTable = objBLUser.ConsultarCboxOpcion_Activos(strNombreForm, False, strActivos)
            dtUsuarios_Combo = New DataTable
            dtUsuarios_Combo = dtCombo.Clone
            For Each row As DataRow In dtCombo.Rows
                dtUsuarios_Combo.ImportRow(row)
            Next

            pCargaCombosBox(cboCoEjeVta, dtCombo, False)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub frmIngPedido_FormClosed(sender As Object, e As FormClosedEventArgs) Handles Me.FormClosed
        ' frmMain.DesabledBtns()
    End Sub

    Private Sub frmIngPedido_KeyDown(sender As Object, e As KeyEventArgs) Handles Me.KeyDown

        Select Case e.KeyCode
            'Case Keys.F6
            '    If frmMain.btnGrabar.Enabled Then pGrabar(Nothing, Nothing)
            Case Keys.F3
                'If frmMain.btnEditar.Enabled Then pEditar(Nothing, Nothing)
                pEditar(Nothing, Nothing)
            Case Keys.Escape
                pSalir(Nothing, Nothing)
        End Select
    End Sub
    Private Sub CargarSeguridad()
        Try
            Dim strNombreForm As String = Me.Name

            strNombreForm = Replace(strNombreForm, "frm", "mnu")
            strNombreForm = Replace(strNombreForm, "Dato", "")
            Dim strPermisoEliminarArchivo As String = "EliminarArchivosFile"
            Using objSeg As New clsSeguridadBN.clsOpcionesBN.clsUsuarioBN
                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strNombreForm)
                    If dr.HasRows Then
                        dr.Read()
                        blnGrabarEnabledBD = dr("Grabar")
                        blnEditarEnabledBD = dr("Actualizar")
                        blnEliminarEnabledBD = dr("Eliminar")
                        blnConsultarEnabledBD = dr("Consultar")
                    End If
                    dr.Close()
                End Using

                Using dr As SqlClient.SqlDataReader = objSeg.ConsultarOpcionesForm(gstrUser, strPermisoEliminarArchivo)
                    If dr.HasRows Then
                        dr.Read()

                        blnEliminarArchivo = dr("Eliminar")
                        dgvArchivos.Columns("btnDelA").Visible = blnEliminarArchivo
                    End If
                    dr.Close()
                End Using
            End Using


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ActivarDesactivarBotones()

        ''frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'If blnNuevoEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnNuevo.Enabled = blnNuevoEnabled
        '    Else
        '        frmMain.btnNuevo.Enabled = False
        '        blnNuevoEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'End If

        ''frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'If blnGrabarEnabled = True Then
        '    If blnGrabarEnabledBD Then
        '        frmMain.btnGrabar.Enabled = blnGrabarEnabled
        '    Else
        '        frmMain.btnGrabar.Enabled = False
        '        blnGrabarEnabled = blnGrabarEnabledBD
        '    End If
        'Else
        '    frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'End If

        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled

        ''frmMain.btnEditar.Enabled = blnEditarEnabled
        'If blnEditarEnabled = True Then
        '    If blnEditarEnabledBD Then
        '        frmMain.btnEditar.Enabled = blnEditarEnabled
        '    Else
        '        frmMain.btnEditar.Enabled = False
        '        blnEditarEnabled = blnEditarEnabledBD
        '    End If
        'Else
        '    frmMain.btnEditar.Enabled = blnEditarEnabled
        'End If

        ''frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''If blnEliminarEnabled = True Then
        ''    If blnEliminarEnabledBD Then
        ''        frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''    Else
        ''        frmMain.btnEliminar.Enabled = False
        ''        blnEliminarEnabled = blnEliminarEnabledBD
        ''    End If
        ''Else
        ''    frmMain.btnEliminar.Enabled = blnEliminarEnabled
        ''End If

        ''If chrModulo = "C" Then
        '' If Not dgv.Columns("btnDel") Is Nothing Then dgv.Columns("btnDel").Visible = blnEliminarEnabled
        ''End If

        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        'frmMain.btnSalir.Enabled = blnSalirEnabled

    End Sub
    Private Sub frmIngPedido_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Activated

        'frmMain.btnNuevo.Enabled = blnNuevoEnabled
        'AddHandler frmMain.btnSalir.Click, AddressOf pSalir
        'frmMain.btnSalir.Enabled = blnSalirEnabled
        'AddHandler frmMain.btnGrabar.Click, AddressOf pGrabar
        'frmMain.btnGrabar.Enabled = blnGrabarEnabled
        'AddHandler frmMain.btnDeshacer.Click, AddressOf pDeshacer
        'frmMain.btnDeshacer.Enabled = blnDeshacerEnabled
        'frmMain.btnImprimir.Enabled = blnImprimirEnabled
        blnLoad = True
        pCambiarTitulosToolBar(Me)
    End Sub
    Private Sub pSalir(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Me.Close()

    End Sub
    Private Sub pDeshacer(ByVal sender As System.Object, ByVal e As System.EventArgs)

        pSalir(Nothing, Nothing)

    End Sub
    Private Sub pGrabar(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Try
            If Not blnValidarIngresos() Then Exit Sub

            If strModoEdicion = "N" Then
                Grabar()
            ElseIf strModoEdicion = "E" Then
                Actualizar()
            End If
            If Not FormRef Is Nothing Then
                FormRef.Buscar()
            End If
            blnCambios = False
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Grabar()
        Try
            Dim objBT As New clsPedidoBT

            Dim objBE As New clsCotizacionBE.clsPedidoBE _
                    With {.CoCliente = strCoCliente, _
                          .CoEjeVta = cboCoEjeVta.SelectedValue, _
                          .CoEstado = cboEstado.SelectedValue, _
                          .FePedido = dtpFePedido.Value, _
                          .TxTitulo = txtTxTitulo.Text.Trim, _
                          .UserMod = gstrUser}
            Dim strNuPedidoPedidoInt As String = objBT.Insertar(objBE)
            Dim ArrNuPedido() As String = strNuPedidoPedidoInt.Split("|")
            intNuPedInt = ArrNuPedido(0)
            strNuPedido = ArrNuPedido(1)
            lblNuPedido.Text = strNuPedido
            strModoEdicion = "E"
            MessageBox.Show("El Pedido " & strNuPedido & " se registró exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub chkActivos_CheckedChanged(sender As Object, e As EventArgs) Handles chkActivos.CheckedChanged

        Try
            CargarCombo_Usuario_Activos()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub Actualizar()
        Try
            Dim objBT As New clsPedidoBT

            Dim objBE As New clsCotizacionBE.clsPedidoBE _
                    With {.NuPedInt = intNuPedInt, .CoCliente = strCoCliente, _
                          .CoEjeVta = cboCoEjeVta.SelectedValue, _
                          .CoEstado = cboEstado.SelectedValue, _
                          .FePedido = dtpFePedido.Value, _
                          .TxTitulo = txtTxTitulo.Text.Trim, _
                          .UserMod = gstrUser}

            'If tbArchivos.Parent = tabPedido Then
            ''ARCHIVOS
            'Jorge 20160408: Lista de archivos
            Dim objBECot As New clsCotizacionBE
            objBECot.IdCab = intIDCab
            Dim objBEArchivos As clsCotizacionBE.clsCotizacionArchivosBE
            Dim ListaArchivos As New List(Of clsCotizacionBE.clsCotizacionArchivosBE)

            For Each ST As stArchivos In objLstArchivos
                objBEArchivos = New clsCotizacionBE.clsCotizacionArchivosBE
                With objBEArchivos

                    .IDArchivo = ST.IDArchivo
                    .IDCab = intIDCab_File
                    .Nombre = ST.Nombre
                    .RutaOrigen = ST.RutaOrigen
                    .RutaDestino = ST.RutaDestino
                    .CoTipo = ST.CoTipo
                    .FlCopiado = ST.FlCopiado
                    .CoUsuarioCreacion = ST.CoUsuarioCreacion
                    .UserMod = gstrUser
                    .Accion = ST.Accion
                End With
                ListaArchivos.Add(objBEArchivos)
            Next
            CrearCarpetasArchivos()
            objBECot.ListaCotizArchivos = ListaArchivos

            'Dim objBT As New clsCotizacionBT.clsCotizacionArchivosBT
            'objBT.InsertarActualizarEliminar(objBECot)
            'End If
            'If TieneFile() Then

            If Not TieneFile() Then
                objBECot = Nothing
            End If

            objBT.Actualizar(objBE, objBECot)
            'End If



            MessageBox.Show("El Pedido " & strNuPedido & " se actualzó exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub frmIngPedido_Deactivate(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Deactivate
        'RemoveHandler frmMain.btnSalir.Click, AddressOf pSalir

        'RemoveHandler frmMain.btnGrabar.Click, AddressOf pGrabar
        'RemoveHandler frmMain.btnDeshacer.Click, AddressOf pDeshacer

        pCambiarTitulosToolBar(Me, True)
    End Sub
    Public Sub ConsultarCotizaciones()
        If intNuPedInt = 0 Then Exit Sub
        Try
            Dim intIDCab As Integer = 0
            If Not dgv.CurrentRow Is Nothing Then
                intIDCab = dgv.CurrentRow.Cells("IDCab").Value
            End If


            Dim objBN As New clsPedidoBN
            dgv.DataSource = objBN.ConsultarCotizaciones(intNuPedInt)

            AlinearFilaGrid(dgv, "IDCab", intIDCab, dgv.Columns("EjecutivoVtas").Index)
            pDgvAnchoColumnas(dgv)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub ModoNuevo()
        Try
            strModoEdicion = "N"

            blnNuevoEnabled = False
            blnDeshacerEnabled = True
            blnGrabarEnabled = True
            blnEditarEnabled = False
            blnEliminarEnabled = False
            blnSalirEnabled = False
            ActivarDesactivarBotones()


            pClearControls(Me)
            pEnableControls(Me, True)


            dtpFePedido.Text = Today.Date
            chkActivos.Enabled = False
            cboEstado.SelectedValue = "PD"
            cboEstado.Enabled = False
            If Not FormRef.cboCoEjeVta.SelectedValue Is Nothing Then
                cboCoEjeVta.SelectedValue = FormRef.cboCoEjeVta.SelectedValue
            End If
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub cboCoEjeVta_DrawItem(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DrawItemEventArgs) Handles cboCoEjeVta.DrawItem
        Try
            pColorear_ComboBox_Usuarios(cboCoEjeVta, dtUsuarios_Combo, sender, e)

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnCliente_Click(sender As Object, e As EventArgs) Handles btnCliente.Click
        Try
            Dim tmpstrIDCliente As String = strCoCliente
            Dim frmAyuda As New frmAyudas
            With frmAyuda
                .strCaller = Me.Name
                .frmRefPedidosDato = Me
                .Text = "Consulta de Clientes"
                .strTipoBusqueda = "Cliente"
                If gstrNivelUser = "COU" Or gstrNivelUser = "SCO" Then
                    .strCoTipoVenta = "CU"
                Else
                    '.strCoTipoVenta = "RC"
                    .strCoTipoVenta = ""
                End If
                .ShowDialog()
            End With
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function blnValidarIngresos() As Boolean
        Dim blnOk As Boolean = True
        If cboCoEjeVta.Text = "" Then
            ErrPrv.SetError(cboCoEjeVta, "Debe seleccionar un Ejecutivo.")
            blnOk = False
        End If
        If lblCliente.Text.Trim = "" Then
            ErrPrv.SetError(btnCliente, "Debe seleccionar un Cliente.")
            blnOk = False
        End If
        If txtTxTitulo.Text.Trim = "" Then
            ErrPrv.SetError(txtTxTitulo, "Debe ingresar Título.")
            blnOk = False
        End If
        Return blnOk
    End Function

    Private Sub ModoEditar()
        strModoEdicion = "E"
        Me.Text = "Edición de Pedido: " & strNuPedido
        'lblEstadoForm.Text = "Edición"

        chkActivos.Checked = False
        blnNuevoEnabled = False
        blnDeshacerEnabled = True
        blnGrabarEnabled = True
        'blnEditarEnabled = False
        'blnEliminarEnabled = False
        blnSalirEnabled = False
        ActivarDesactivarBotones()
        'TabPage2.Parent = Tab
    End Sub

    Private Sub CargarControles()
        Try
            Dim objBN As New clsPedidoBN
            Using dr As SqlClient.SqlDataReader = objBN.ConsultarPK(intNuPedInt)
                dr.Read()
                If dr.HasRows Then
                    cboCoEjeVta.SelectedValue = dr("CoEjeVta")
                    lblNuPedido.Text = dr("NuPedido")
                    lblCliente.Text = dr("RazonComercial")
                    strCoCliente = dr("CoCliente")
                    strCoPais = dr("CoPais")
                    txtTxTitulo.Text = dr("TxTitulo")
                    dtpFePedido.Value = dr("FePedido")
                    cboEstado.SelectedValue = dr("CoEstado")
                Else
                    MessageBox.Show("El registro ha sido eliminado recientemente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
                dr.Close()
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub btnNuevo_Click(sender As Object, e As EventArgs) Handles btnNuevo.Click
        If intNuPedInt = 0 Then
            MessageBox.Show("Debe antes grabar el pedido para poder ingresar Cotizaciones correctamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
            Exit Sub
        End If

        Dim frm As New frmIngCotizacionDato
        With frm
            ' .MdiParent = frmMain
            .FormRefPedido = Me
            .Show()

        End With
    End Sub

    Private Sub Editar()
        Try
            Dim frm As frmIngCotizacionDato
            For Each dgvFila As DataGridViewRow In dgv.Rows
                If dgvFila.Selected = True Then
                    frm = New frmIngCotizacionDato
                    With frm
                        .intIDCab = dgvFila.Cells("IDCab").Value.ToString
                        .strCotizacion = dgvFila.Cells("Cotizacion").Value.ToString
                        '.strCorreoReservas = dgvFila.Cells("CorreoEjecReservas").Value.ToString
                        '.MdiParent = frmMain
                        .FormRefPedido = Me
                        .FormRefPedido.WindowState = FormWindowState.Maximized
                        .Show()

                        If blnPantallaFileAbierto(.Name, .intIDCab) Then
                            .Close()
                        End If

                    End With

                End If
            Next

        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub pEditar(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles dgv.DoubleClick
        If dgv.CurrentCell Is Nothing Then Exit Sub
        Try
            Editar()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub dgv_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgv.CellContentClick
        If dgv.CurrentCell Is Nothing Then Exit Sub

        Try
            If dgv.CurrentCell.ColumnIndex = dgv.Columns("btnEdit").Index Then
                Editar()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgv_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgv.CellPainting
        If e.ColumnIndex = -1 Then Exit Sub
        If e.RowIndex = -1 Then Exit Sub

        If dgv.Columns(e.ColumnIndex).Name = "btnEdit" Then
            e.Paint(e.CellBounds, DataGridViewPaintParts.All)
            Dim celBoton As DataGridViewButtonCell = CType(dgv.Rows(e.RowIndex).Cells("btnEdit"), DataGridViewButtonCell)
            Dim icoAtomico As Image = Nothing
            icoAtomico = My.Resources.Resources.zoom
            If blnEditarCotizac Then
                icoAtomico = My.Resources.Resources.editarpng
            End If
            e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
            dgv.Rows(e.RowIndex).Height = 21
            dgv.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
            e.Handled = True
        End If

    End Sub

    Private Sub CargarPerfilCliente()
        If blnPerfilClienteCargado Then Exit Sub
        Try
            'cboPais_SelectionChangeCommitted(Nothing, Nothing)
            Using objBN As New clsClienteBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(strCoCliente)
                    dr.Read()
                    If dr.HasRows Then

                        Dim strTipoCl As String = dr("Tipo")
                        If strTipoCl = "ND" Then
                            MessageBox.Show("Cliente no deseado", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                        End If

                        'campos nuevos - perfil
                        'dtpFechaUpdate.Value = If(IsDBNull(dr("FePerfilUpdate")) = True, Now, dr("FePerfilUpdate")) 'dr("FlTop")
                        dtpFechaUpdate.Value = If(IsDBNull(dr("FePerfilUpdate")) = True, Now, dr("FePerfilUpdate")) 'dr("FlTop")

                        'lblFechaPerfil.Text = If(IsDBNull(dr("FePerfilUpdate")) = True, "", dr("FePerfilUpdate").ToString("dd/MM/yyyy hh:mm:ss"))
                        lblFechaPerfil.Text = If(IsDBNull(dr("FePerfilUpdate")) = True, "", CDate(dr("FePerfilUpdate").ToString).ToString("dd/MM/yyyy hh:mm:ss"))


                        'strUsuarioPerfil = If(IsDBNull(dr("CoUserPerfilUpdate")) = True, "", dr("CoUserPerfilUpdate"))
                        lblUsuarioPerfil.Text = If(IsDBNull(dr("Usu_Perfil")) = True, "", dr("Usu_Perfil")) '

                        txtInformacionGeneral.Text = If(IsDBNull(dr("txPerfilInformacion")) = True, "", dr("txPerfilInformacion")) 'dr("FlTop")
                        txtOperatividad.Text = If(IsDBNull(dr("txPerfilOperatividad")) = True, "", dr("txPerfilOperatividad")) 'dr("FlTop")

                        CargarSolvencia(dr("Solvencia"))


                        CargarGuias_Data()
                        CargarCiudades_Guias()

                    Else
                        MessageBox.Show("El registro ha sido eliminado recientemente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)

                    End If
                    dr.Close()
                End Using
            End Using

            'grbPerfil.Enabled = False

            txtInformacionGeneral.ReadOnly = True
            txtOperatividad.ReadOnly = True
            dgvCiudades.ReadOnly = True
            dgvGuias.ReadOnly = True

            blnPerfilClienteCargado = True
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub CargarSolvencia(ByVal vbytNuSolvencia As Byte)
        If vbytNuSolvencia = 0 Then Exit Sub
        Try

            Dim strToolTipText As String = ""
            If vbytNuSolvencia = 1 Then
                pcbstar1.Visible = True
                pcbstar2.Visible = False
                pcbstar3.Visible = False
                strToolTipText = "Cliente nuevo o moroso" & vbCrLf & "Prepagos/garantías se pueden realizar cómo máximo por el monto del depósito realizado."
            ElseIf vbytNuSolvencia = 2 Then
                pcbstar1.Visible = True
                pcbstar2.Visible = True
                pcbstar3.Visible = False
                strToolTipText = "Cliente estable" & vbCrLf & "Prepagos/garantías se pueden realizar siempre cuando haya por lo " & vbCrLf & "menos un depósito (la garantía puede exceder el monto del depósito)."
            ElseIf vbytNuSolvencia = 3 Then
                pcbstar1.Visible = True
                pcbstar2.Visible = True
                pcbstar3.Visible = True
                strToolTipText = "Cliente de confianza." & vbCrLf & "Prepagos/garantías se pueden realizar sin pago."
            End If

            Dim ToolTp As New ToolTip
            ToolTp.SetToolTip(pcbstar1, strToolTipText)
            ToolTp.SetToolTip(pcbstar2, strToolTipText)
            ToolTp.SetToolTip(pcbstar3, strToolTipText)
        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarGuias_Data()
        Try

            Dim objBL As New clsClienteBN.clsClientes_Perfil_GuiasBN
            Dim dtDet As Data.DataTable = objBL.ConsultarList(strCoCliente, "", "", "")


            Dim stReg As stGuias

            For i As Integer = 0 To dtDet.Rows.Count - 1

                'llenar los detalles en la lista
                stReg = New stGuias


                '-----------------------------------------------------------------------------------

                With stReg

                    .CoProveedor = dtDet.Rows(i).Item("CoProveedor")
                    .NombreCorto = dtDet.Rows(i).Item("NombreCorto")
                    .IDubigeo = dtDet.Rows(i).Item("IDCiudad")
                    .strAccion = ""


                End With


                objLstGuias.Add(stReg)

                '-----------------------------------------------------------------------------------


            Next


        Catch ex As Exception
            Throw
        End Try
    End Sub
    Private Sub CargarCiudades_Guias()
        Try
            Dim objBL As New clsClienteBN.clsClientes_Perfil_GuiasBN
            Dim dtGuias As DataTable = objBL.ConsultarList_Ciudades(strCoCliente)
            dgvCiudades.Rows.Clear()
            Dim intRowIndex As Int16 = 0
            For Each dRowItem As DataRow In dtGuias.Rows
                dgvCiudades.Rows.Add()

                For i As Byte = 0 To dtGuias.Columns.Count - 1
                    dgvCiudades.Item(i, intRowIndex).Value = dtGuias(intRowIndex)(i)
                Next
                intRowIndex += 1
            Next

            dgvCiudades.Columns("Ciudad").DisplayIndex = 1
            dgvCiudades.Columns("Pais").DisplayIndex = 2

        Catch ex As Exception
            Throw
        End Try
    End Sub

    Private Sub tabPedido_SelectedIndexChanged(sender As Object, e As EventArgs) Handles tabPedido.SelectedIndexChanged
        If tabPedido.SelectedIndex = -1 Then Exit Sub
        Try
            Select Case tabPedido.SelectedTab.Name
                Case "tbCliente"
                    CargarPerfilCliente()
                Case "tbCorreos"
                    CargarTreeViewBandejaCorreo()
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#Region "Bandeja Correos"

    Private Function dstOpcionesCorreosPedido() As DataSet
        Dim ds As New DataSet
        'Dim strCabecera As String = "File: " & strIDFile & vbCrLf & "-" & strDescProveedor
        'Dim strCabecera As String = "File: " & lblIDFile.Text & "-" & txtTitulo.Text
        Dim strCabecera As String = "Pedido: " & strNuPedido & "-" & txtTxTitulo.Text

        Dim objPedBN As New clsPedidoBN

        Dim dt1 As New DataTable("dtFile")
        Dim dt2 As New DataTable("dtBandejas")
        Dim dt3 As New DataTable("dtAreas")
        Dim dtProvee As New DataTable("dtProvee")

        With dt1
            .Columns.Add("dtParentValor")
            .Columns.Add("NoTitulo")
            .Rows.Add("PEDIDO", strCabecera)
        End With
        'dt1 = objPedBN.ConsultarCotizacionesFileCorreo(intNuPedInt)
        'dt1.TableName = "dtFile"
        ds.Tables.Add(dt1)

        With dt2 'dtBandejas
            .Columns.Add("dtParentValor")
            .Columns.Add("CoBandeja")
            .Columns.Add("NoBandeja")
            .Rows.Add("PEDIDO", "BE", "Bandeja de Entrada")
            .Rows.Add("PEDIDO", "EE", "Elementos Enviados")

            'For Each dr As DataRow In dt1.Rows
            '    .Rows.Add(dr("dtParentValor"), "BE" & dr("dtParentValor"), "Bandeja de Entrada")
            '    .Rows.Add(dr("dtParentValor"), "EE" & dr("dtParentValor"), "Elementos Enviados")
            'Next
        End With
        ds.Tables.Add(dt2)



        With dt3 'dtAreas
            .Columns.Add("dtParentValor")
            .Columns.Add("CoArea")
            .Columns.Add("NoArea")
            .Rows.Add("BE", "BEVS", "Ventas")
            .Rows.Add("BE", "BERS", "Reservas")
            .Rows.Add("BE", "BEOP", "Operaciones")
            .Rows.Add("BE", "BEFI", "Finanzas")

            .Rows.Add("EE", "EEVS", "Ventas")
            .Rows.Add("EE", "EERS", "Reservas")
            .Rows.Add("EE", "EEOP", "Operaciones")
            .Rows.Add("EE", "EEFI", "Finanzas")
            'For Each dr As DataRow In dt1.Rows
            '    .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BEVS", "Ventas")
            '    .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BERS", "Reservas")
            '    .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BEOP", "Operaciones")
            '    .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BEFI", "Finanzas")

            '    .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EEVS", "Ventas")
            '    .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EERS", "Reservas")
            '    .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EEOP", "Operaciones")
            '    .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EEFI", "Finanzas")
            'Next
        End With
        ds.Tables.Add(dt3)


        dtProvee = objPedBN.ConsultarProveedoresCorreo(intNuPedInt)
        dtProvee.TableName = "dtProvee"



        ds.Tables.Add(dtProvee)

        ds.Relations.Add("Rel-FILE-PROVEEDOR", ds.Tables("dtFile").Columns("dtParentValor"), ds.Tables("dtBandejas").Columns("dtParentValor"))
        ds.Relations.Add("Rel-BANDEJA-AREA", ds.Tables("dtBandejas").Columns("CoBandeja"), ds.Tables("dtAreas").Columns("dtParentValor"))

        ds.Relations.Add("Rel-AREA-PROVEE", ds.Tables("dtAreas").Columns("CoArea"), ds.Tables("dtProvee").Columns("CoArea"))


        Return ds
    End Function


    Private Function dstOpcionesCorreosFile() As DataSet
        Dim ds As New DataSet
        'Dim strCabecera As String = "File: " & strIDFile & vbCrLf & "-" & strDescProveedor
        'Dim strCabecera As String = "File: " & lblIDFile.Text & "-" & txtTitulo.Text

        Dim objPedBN As New clsPedidoBN

        Dim dt1 As New DataTable("dtFile")
        Dim dt2 As New DataTable("dtBandejas")
        Dim dt3 As New DataTable("dtAreas")
        Dim dtProvee As New DataTable("dtProvee")

        'With dt1
        '    .Columns.Add("dtParentValor")
        '    .Columns.Add("NoTitulo")
        '    .Rows.Add("FILE-PROVEEDOR", strCabecera)
        'End With
        dt1 = objPedBN.ConsultarCotizacionesFileCorreo(intNuPedInt)
        dt1.TableName = "dtFile"
        ds.Tables.Add(dt1)

        With dt2 'dtBandejas
            .Columns.Add("dtParentValor")
            .Columns.Add("CoBandeja")
            .Columns.Add("NoBandeja")
            '.Rows.Add("FILE-PROVEEDOR", "BE", "Bandeja de Entrada")
            '.Rows.Add("FILE-PROVEEDOR", "EE", "Elementos Enviados")

            For Each dr As DataRow In dt1.Rows
                .Rows.Add(dr("dtParentValor"), "BE" & dr("dtParentValor"), "Bandeja de Entrada")
                .Rows.Add(dr("dtParentValor"), "EE" & dr("dtParentValor"), "Elementos Enviados")
            Next
        End With
        ds.Tables.Add(dt2)



        With dt3 'dtAreas
            .Columns.Add("dtParentValor")
            .Columns.Add("CoArea")
            .Columns.Add("NoArea")
            '.Rows.Add("BE", "BEVS", "Ventas")
            '.Rows.Add("BE", "BERS", "Reservas")
            '.Rows.Add("BE", "BEOP", "Operaciones")
            '.Rows.Add("BE", "BEFI", "Finanzas")

            '.Rows.Add("EE", "EEVS", "Ventas")
            '.Rows.Add("EE", "EERS", "Reservas")
            '.Rows.Add("EE", "EEOP", "Operaciones")
            '.Rows.Add("EE", "EEFI", "Finanzas")
            For Each dr As DataRow In dt1.Rows
                .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BEVS", "Ventas")
                .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BERS", "Reservas")
                .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BEOP", "Operaciones")
                .Rows.Add("BE" & dr("dtParentValor"), "BE" & dr("dtParentValor") & "|BEFI", "Finanzas")

                .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EEVS", "Ventas")
                .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EERS", "Reservas")
                .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EEOP", "Operaciones")
                .Rows.Add("EE" & dr("dtParentValor"), "EE" & dr("dtParentValor") & "|EEFI", "Finanzas")
            Next
        End With
        ds.Tables.Add(dt3)


        dtProvee = objPedBN.ConsultarProveedoresCorreo(intNuPedInt)
        dtProvee.TableName = "dtProvee"



        ds.Tables.Add(dtProvee)

        ds.Relations.Add("Rel-FILE-PROVEEDOR", ds.Tables("dtFile").Columns("dtParentValor"), ds.Tables("dtBandejas").Columns("dtParentValor"))
        ds.Relations.Add("Rel-BANDEJA-AREA", ds.Tables("dtBandejas").Columns("CoBandeja"), ds.Tables("dtAreas").Columns("dtParentValor"))

        ds.Relations.Add("Rel-AREA-PROVEE", ds.Tables("dtAreas").Columns("CoArea"), ds.Tables("dtProvee").Columns("CoArea"))


        Return ds
    End Function


    Private Sub CargarTreeViewBandejaCorreo()
        'If blnBandCorreosCargada Then Exit Sub

        'Try
        '    Me.Cursor = Cursors.WaitCursor
        '    tvw.Nodes.Clear()
        '    Dim objBN As New clsCorreoFileBN

        '    Dim ds As DataSet = dstOpcionesCorreosPedido()

        '    Dim dtFile As DataTable = ds.Tables("dtFile")
        '    Dim dtBandejas As DataTable = ds.Tables("dtBandejas")
        '    Dim dtAreas As DataTable = ds.Tables("dtAreas")
        '    Dim dtProvee As DataTable = ds.Tables("dtProvee")

        '    For Each ParentRow As DataRow In dtFile.Rows
        '        Dim ParentFile As TreeNode
        '        ParentFile = tvw.Nodes.Add(ParentRow("dtParentValor").ToString, ParentRow("NoTitulo").ToString)
        '        'Dim intIDCab As Integer = ParentRow("dtParentValor")
        '        ParentFile.ImageIndex = 0
        '        Dim childBand As TreeNode
        '        childBand = New TreeNode()

        '        For Each chBandeja As DataRow In ParentRow.GetChildRows("Rel-FILE-PROVEEDOR") 'dtBandejas.Rows
        '            childBand = ParentFile.Nodes.Add(chBandeja("CoBandeja").ToString, chBandeja("NoBandeja").ToString)
        '            Dim strCoBand As String = chBandeja("CoBandeja").ToString.Substring(0, 2)
        '            If strCoBand = "BE" Then
        '                childBand.ImageIndex = 1
        '            ElseIf strCoBand = "EE" Then
        '                childBand.ImageIndex = 2
        '            End If

        '            Dim childArea As TreeNode
        '            For Each chArea As DataRow In chBandeja.GetChildRows("Rel-BANDEJA-AREA")
        '                childArea = childBand.Nodes.Add(chArea("CoArea").ToString, chArea("NoArea").ToString)
        '                'Dim strCoArea As String = chArea("CoArea").ToString.Substring(2, 2)
        '                'Dim strCoArea As String = strDevuelveCadenaOcultaDerecha(chArea("CoArea").ToString)
        '                Dim strCoArea As String = chArea("CoArea").ToString
        '                strCoArea = strCoArea.Substring(2, 2)
        '                Select Case strCoArea
        '                    Case "VS"
        '                        childArea.ImageIndex = 3
        '                    Case "RS"
        '                        childArea.ImageIndex = 4
        '                    Case "OP"
        '                        childArea.ImageIndex = 5
        '                End Select

        '                Dim childProvee As TreeNode
        '                For Each chProv As DataRow In chArea.GetChildRows("Rel-AREA-PROVEE")
        '                    Dim strCoArea2 As String = chProv("CoArea").ToString 'strDevuelveCadenaOcultaDerecha(chProv("CoArea").ToString)
        '                    strCoArea2 = strCoArea2.Substring(2, 2)
        '                    If strCoArea2 = "VE" Then
        '                        If objBN.blnExistenCorreosPedidoCliexBandejaArea(intNuPedInt, chProv("CoClienteProvee").ToString, strCoBand, strCoArea) Then
        '                            childProvee = childArea.Nodes.Add(chProv("CoClienteProvee").ToString, chProv("DescClienteProvee").ToString)
        '                            childProvee.ImageIndex = 7
        '                        End If

        '                    Else
        '                        If objBN.blnExistenCorreosPedidoProvxBandejaArea(intNuPedInt, chProv("CoClienteProvee").ToString, strCoBand, strCoArea) Then
        '                            childProvee = childArea.Nodes.Add(chProv("CoClienteProvee").ToString, chProv("DescClienteProvee").ToString)
        '                            childProvee.ImageIndex = 6
        '                        End If

        '                    End If

        '                Next
        '            Next


        '        Next
        '    Next


        '    tvw.SelectedNode = tvw.Nodes(0).Nodes(0).Nodes(0)

        '    tvw_AfterSelect(Nothing, Nothing)
        '    tvw.ExpandAll()

        '    blnBandCorreosCargada = True
        '    Me.Cursor = Cursors.Default
        'Catch ex As Exception
        '    Me.Cursor = Cursors.Default
        '    Throw
        'End Try
    End Sub

    Public Sub CargarMensajes()
        Try
            If tvw.SelectedNode Is Nothing Then Exit Sub
            If tvw.SelectedNode.Level <= 1 Then Exit Sub
            If intNuPedInt = 0 Then Exit Sub
            Me.Cursor = Cursors.WaitCursor

            Dim strCoTipoBandeja As String = ""
            Dim strDescArea As String = ""
            'Dim strCoArea As String = ""
            'Dim strCoProvClie As String = ""


            If tvw.SelectedNode.Level = 3 Then
                strCoTipoBandeja = tvw.SelectedNode.Parent.Parent.Name.Substring(0, 2)
                strCoArea = tvw.SelectedNode.Parent.Name.Substring(2, 2)
                'strCoArea = strDevuelveCadenaOcultaDerecha(tvw.SelectedNode.Parent.Name)
                'strCoArea = strCoArea.Substring(2, 2)
                strDescArea = tvw.SelectedNode.Parent.Text
                strCoProvClie = tvw.SelectedNode.Name
                'intIDCab = tvw.SelectedNode.Parent.Parent.Parent.Name
                strTitulo = tvw.SelectedNode.Parent.Parent.Parent.Text

            ElseIf tvw.SelectedNode.Level = 2 Then
                strCoTipoBandeja = tvw.SelectedNode.Parent.Name.Substring(0, 2)
                strCoArea = tvw.SelectedNode.Name.Substring(2, 2)
                'strCoArea = strDevuelveCadenaOcultaDerecha(tvw.SelectedNode.Name)
                'strCoArea = strCoArea.Substring(2, 2)
                strDescArea = tvw.SelectedNode.Text
                strCoProvClie = ""
                'intIDCab = tvw.SelectedNode.Parent.Parent.Name
                strTitulo = tvw.SelectedNode.Parent.Parent.Text
            End If
            If InStr(strTitulo, "File:") > 0 Then
                strIDFile = Mid(strTitulo, InStr(strTitulo, "File:") + 6, 8)
                strTitulo = Mid(strTitulo, InStr(strTitulo, "File:") + 15)
            Else
                strTitulo = Mid(strTitulo, InStr(strTitulo, "Cot:") + 15)
                strIDFile = ""
            End If

            Dim lstConversationIndex As New List(Of String)

            Dim strTab As String = "     "
            Dim TmpstrIDFile As String = ""
            If strCoTipoBandeja = "BE" Then
                lblTipoCorreo.ImageIndex = 1
                dgvCorreos.Columns("FechaCorreo").HeaderText = "Recibido el"

                lblTipoCorreo.Text = strTab & "Bandeja de Entrada - " & strDescArea

            ElseIf strCoTipoBandeja = "EE" Then
                lblTipoCorreo.ImageIndex = 2
                dgvCorreos.Columns("FechaCorreo").HeaderText = "Enviado el"

                lblTipoCorreo.Text = strTab & "Elementos Enviados  - " & strDescArea

            Else
                lblTipoCorreo.Text = ""
            End If

            Me.Cursor = Cursors.WaitCursor

            Dim dttCorreos As DataTable = dtLeerInboxBD(strCoTipoBandeja, strCoArea, strCoProvClie, intIDCab)
            webHtml.DocumentText = ""
            rtbMensaje.Text = ""

            ''''
            dgvCorreos.DataSource = dttCorreos
            'blnNewRow = True
            If dgvCorreos.RowCount > 0 Then dgvCorreos.Sort(dgvCorreos.Columns("FechaCorreo"), System.ComponentModel.ListSortDirection.Descending)

            pDgvAnchoColumnas(dgvCorreos)
            dgvCorreos.Columns("FechaCorreo").Width = 120
            dgvCorreos.Columns("De").Width = 130
            dgvCorreos.Columns("Para").Width = 130
            dgvCorreos.Columns("Asunto").Width = 150
            dgvCorreos.Columns("ImgAdjuntos").Width = 25
            dgvCorreos.Columns("IconoCorreo").Width = 25

            Me.Cursor = Cursors.Default

        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub

    Public Function dtLeerInboxBD(ByVal vstrFolder As String, vstrCoArea As String, vstrCoProvClie As String, ByVal intIDCab As Integer) As DataTable
        'Try
        '    Dim DtCorreos As New DataTable("Correos")
        '    With DtCorreos
        '        .Columns.Add("IconoCorreo", GetType(Image))
        '        .Columns.Add("ImgAdjuntos", GetType(Image))
        '        .Columns.Add("NuCorreo", GetType(String))
        '        .Columns.Add("CantAdjuntos", GetType(Integer))
        '        .Columns.Add("EntryID", GetType(String))
        '        .Columns.Add("De", GetType(String))
        '        .Columns.Add("Para", GetType(String))
        '        .Columns.Add("Asunto", GetType(String))
        '        .Columns.Add("Body", GetType(String))
        '        .Columns.Add("BodyHtml", GetType(String))
        '        .Columns.Add("ConCopia", GetType(String))
        '        .Columns.Add("Importance", GetType(String))
        '        .Columns.Add("EstadoLeido", GetType(Boolean))
        '        .Columns.Add("BodyFormat", GetType(String))
        '        .Columns.Add("RutasAdjuntos", GetType(String))
        '        .Columns.Add("Fecha", GetType(Date))
        '    End With

        '    Dim objBN As New clsCorreoFileBN
        '    Dim dtt As New DataTable '= Nothing


        '    If vstrFolder = "BE" Then
        '        If vstrCoArea = gstrAreaVentas Then 'Clientes
        '            dtt = objBN.ConsultarListBE(intIDCab, intNuPedInt, vstrCoProvClie, "", "", vstrCoArea, txtBusqAsunto.Text.Trim)
        '        ElseIf vstrCoArea = gstrAreaReservas Or vstrCoArea = gstrAreaOperaciones Or vstrCoArea = gstrAreaFinanzas Then 'Proveedores
        '            dtt = objBN.ConsultarListBE(intIDCab, intNuPedInt, "", vstrCoProvClie, "", vstrCoArea, txtBusqAsunto.Text.Trim)
        '        End If
        '    Else
        '        If vstrCoArea = gstrAreaVentas Then 'Clientes
        '            dtt = objBN.ConsultarListEE(intIDCab, intNuPedInt, vstrCoProvClie, "", "", vstrCoArea, txtBusqAsunto.Text.Trim)
        '        ElseIf vstrCoArea = gstrAreaReservas Or vstrCoArea = gstrAreaOperaciones Or vstrCoArea = gstrAreaFinanzas Then 'Proveedores
        '            dtt = objBN.ConsultarListEE(intIDCab, intNuPedInt, "", vstrCoProvClie, "", vstrCoArea, txtBusqAsunto.Text.Trim)
        '        End If

        '    End If

        '    For Each dr As DataRow In dtt.Rows
        '        DtCorreos.Rows.Add(If(dr("EstadoLeido") = True, My.Resources.email_open, My.Resources.email), _
        '                           If(dr("CantAdjuntos") <> 0, My.Resources.attach, My.Resources.attach_transparent), _
        '                            dr("NuCorreo"), _
        '                            dr("CantAdjuntos"), _
        '                            dr("EntryID"), _
        '                            dr("De"), _
        '                            dr("Para"), _
        '                            dr("Asunto"), _
        '                            dr("Body"), _
        '                            dr("BodyHtml"), _
        '                            dr("ConCopia"), _
        '                            dr("Importance"), _
        '                            dr("EstadoLeido"), _
        '                            dr("BodyFormat"), _
        '                            dr("RutasAdjuntos"), _
        '                            dr("Fecha"))

        '    Next
        '    Return DtCorreos
        'Catch ex As Exception
        '    Throw
        'End Try
    End Function


    Private Sub tvw_AfterSelect(sender As Object, e As TreeViewEventArgs) Handles tvw.AfterSelect
        Try

            CargarMensajes()

        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvCorreos_CellEnter(sender As Object, e As DataGridViewCellEventArgs) Handles dgvCorreos.CellEnter

        Try
            If Not IsDBNull(dgvCorreos.Item("BodyHtml", dgvCorreos.CurrentCell.RowIndex).Value) Then

                If dgvCorreos.CurrentRow.Cells("BodyFormat").Value.ToString = "H" Then
                    grbWeb.Visible = True
                    grbTexto.Visible = False
                    webHtml.DocumentText = dgvCorreos.CurrentRow.Cells("BodyHtml").Value.ToString
                ElseIf dgvCorreos.CurrentRow.Cells("BodyFormat").Value.ToString <> "P" Then
                    grbWeb.Visible = False
                    grbTexto.Visible = True
                    rtbMensaje.Text = dgvCorreos.CurrentRow.Cells("Body").Value.ToString
                Else
                    grbWeb.Visible = False
                    grbTexto.Visible = True
                    rtbMensaje.Text = dgvCorreos.CurrentRow.Cells("Body").Value.ToString

                End If

            Else
                webHtml.DocumentText = ""
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub TssBtnNuevo_ButtonClick(sender As Object, e As EventArgs) Handles TssBtnNuevo.ButtonClick, ToolSSMIMensaje.Click
        Try
            NuevoCorreo()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub NuevoCorreo()
        'Try
        '    'Dim strNombreArchivo As String = "RESUMEN DE ESTADO DE RESERVA.docx"
        '    Dim frm As New frmCorreosDato
        '    With frm
        '        .strModulo = strCoArea.Substring(0, 1)

        '        '.blnClickNuevo = If(dgvCorreos.Rows.Count = 0, True, False)

        '        .strTitulo = strTitulo
        '        .strIDFile = strIDFile
        '        .intNuPedInt = intNuPedInt

        '        .strModoMensaje = "NU"
        '        .intIDCab = intIDCab
        '        blnEnvio = False
        '        .ShowDialog()
        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub TsBtnReenviar_Click(sender As Object, e As EventArgs) Handles TsBtnReenviar.Click, TsBtnResponder.Click, TsBtnResponderTodos.Click
        'Try
        '    If dgvCorreos.Rows.Count <> 0 Then
        '        Dim frm As New frmCorreosDato
        '        With frm

        '            .FormRef = Me
        '            '.strIDReserva = strIDReserva

        '            .strIDProveedor = strCoProvClie

        '            If sender.name = "TsBtnResponder" Then
        '                .strCorreoDestinatario = dgvCorreos.CurrentRow.Cells("De").Value.ToString
        '                .strModoMensaje = "RE"
        '            ElseIf sender.name = "TsBtnReenviar" Then
        '                .strCorreoDestinatario = ""
        '                .strModoMensaje = "RV"
        '            ElseIf sender.name = "TsBtnResponderTodos" Then
        '                .strCorreoDestinatario = dgvCorreos.CurrentRow.Cells("ConCopia").Value.ToString
        '                .strModoMensaje = "RT"
        '            End If


        '            .strModulo = strCoArea.Substring(0, 1)
        '            .intIDCab = intIDCab

        '            .intNuCorreo = dgvCorreos.CurrentRow.Cells("NuCorreo").Value.ToString
        '            .strFormatoCorreo = dgvCorreos.CurrentRow.Cells("BodyFormat").Value
        '            .strCuerpoCorreo = dgvCorreos.CurrentRow.Cells("Body").Value
        '            .strCuerpoCorreoHTML = dgvCorreos.CurrentRow.Cells("BodyHtml").Value
        '            .blnProveeInterno = False
        '            Dim strPerfilEnvio As String = String.Empty
        '            Using oBLCorreo As clsCorreoFileBN = New clsCorreoFileBN()
        '                Using odrCorreo As SqlClient.SqlDataReader = oBLCorreo.ConsultarPk(.intNuCorreo)
        '                    odrCorreo.Read()
        '                    If odrCorreo.HasRows Then
        '                        strPerfilEnvio = odrCorreo("Perfil")
        '                    End If
        '                    odrCorreo.Close()
        '                End Using
        '            End Using

        '            If strPerfilEnvio <> String.Empty Then
        '                If Not dgvCorreos.CurrentRow.Cells("RutasAdjuntos").Value Is Nothing Then
        '                    If dgvCorreos.CurrentRow.Cells("RutasAdjuntos").Value.ToString.Trim <> String.Empty Then
        '                        .strArchivoReenvio = gstrRutaAdjuntosxPerfilCorreoSQLServer & strPerfilEnvio & "\" & _
        '                    .intNuCorreo.ToString & "\" & dgvCorreos.CurrentRow.Cells("RutasAdjuntos").Value
        '                    End If
        '                End If
        '            End If
        '            .ShowDialog()

        '            'GrabarConvertionID()
        '            Dim LstCorreosFiltrar As New List(Of String)
        '            If strCoArea <> "VS" Then
        '                LstCorreosFiltrar = LstCorreosProveedor()
        '            Else
        '                LstCorreosFiltrar = LstCorreosClientes()
        '            End If



        '        End With
        '    Else
        '        MessageBox.Show("No hay mensaje para reenviar.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
        '    End If

        'Catch ex As Exception
        '    MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)

        'End Try
    End Sub

    Private Function LstCorreosProveedor() As List(Of String)
        Try
            Dim objLstCorreosTo As New List(Of String)
            'Dim objLstCorreosCCO As New List(Of String)

            Dim dt As New DataTable
            'If Not blnProveeInterno Then 
            If strCoProvClie <> "" Then
                dt = dtbCargarCorreosProveedor()
            End If

            'End If

            'If objLstCorreos.Count = 0 Then
            For Each rw As DataRow In dt.Rows
                objLstCorreosTo.Add(rw("Correo").ToString)
            Next

            Return objLstCorreosTo
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Function LstCorreosClientes() As List(Of String)
        Try
            Dim objLstCorreosTo As New List(Of String)

            Dim dt As New DataTable
            'If Not blnProveeInterno Then
            If strCoArea = "VS" Then
                dt = dtbCargarCorreosClientes()
            Else
                dt = dtbCargarCorreosProveedor()
            End If

            'End If

            For Each rw As DataRow In dt.Rows
                objLstCorreosTo.Add(rw("Correo").ToString)
            Next

            Return objLstCorreosTo
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function dtbCargarCorreosClientes() As DataTable
        Try
            Dim objCorr As New clsClienteBN.clsContactoClienteBN
            Dim dt As DataTable = objCorr.ConsultarCorreos(strCoProvClie)

            Return dt
        Catch ex As Exception
            Throw
        End Try
    End Function
    Private Function dtbCargarCorreosProveedor() As DataTable
        Try
            Dim objCorr As New clsProveedorBN.clsCorreoReservasProveedorBN
            Dim dt As DataTable = objCorr.ConsultarCorreosFiltroBandejas(strCoProvClie)

            Return dt
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub dgvCorreos_DoubleClick(sender As Object, e As EventArgs) Handles dgvCorreos.DoubleClick
        Try
            AbrirCorreo()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
    Private Sub AbrirCorreo()

        'Try
        '    If dgvCorreos.CurrentRow Is Nothing Then Exit Sub

        '    Dim frm As New frmCorreosDato
        '    With frm
        '        .strModoMensaje = "LE"
        '        '.dtCorreos = dtbCargarCorreosProveedor()
        '        .FormRef = Me
        '        '.intIDReservas = intIDReserva
        '        .strModulo = strCoArea.Substring(0, 1)
        '        .intNuCorreo = dgvCorreos.CurrentRow.Cells("NuCorreo").Value.ToString
        '        .strFormatoCorreo = dgvCorreos.CurrentRow.Cells("BodyFormat").Value
        '        .strCuerpoCorreo = dgvCorreos.CurrentRow.Cells("Body").Value
        '        .strCuerpoCorreoHTML = dgvCorreos.CurrentRow.Cells("BodyHtml").Value
        '        blnEnvio = False
        '        .ShowDialog()


        '    End With
        'Catch ex As Exception
        '    Throw
        'End Try
    End Sub

    Private Sub btnActualizarBand_Click(sender As Object, e As EventArgs) Handles btnActualizarBand.Click
        Try
            CargarMensajes()
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtBusqAsunto_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtBusqAsunto.KeyPress
        Try
            Select Case Asc(e.KeyChar)
                Case 13
                    CargarMensajes()
            End Select
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub
#End Region

    Private Sub CopiarPegarCotiz(ByVal vblnCopiar As Boolean, ByVal vintIDCabCopia As Integer, ByVal vdatFechaCopia As Date)

        Try
            If vblnCopiar Then

                If MessageBox.Show("¿Está Ud. seguro de copiar la Cotización seleccionada a otro Pedido?", _
                                   gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                    gintIDCabCopiaraPedido = vintIDCabCopia
                    gdatFechaCopiaraPedido = vdatFechaCopia

                    Dim objVuelosBN As New clsCotizacionBN.clsCotizacionVuelosBN
                    Dim blnExistenVuelos As Boolean = objVuelosBN.blnExisteVuelosManuales(vintIDCabCopia)
                    If blnExistenVuelos Then
                        gblnTodosVuelosCopiaraPedido = MessageBox.Show("¿Desea Ud. Copiar todos los vuelos relacionados con esta cotización?", _
                                           gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes
                    End If

                    MessageBox.Show("Debe ir al Pedido destino y darle click derecho Pegar para completar la copia.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                Else
                    gintIDCabCopiaraPedido = 0
                    gdatFechaCopiaraPedido = "01/01/1900"
                    gblnTodosVuelosCopiaraPedido = False
                End If

            Else 'Pegar
                If intNuPedInt = 0 Then
                    MessageBox.Show("Debe antes grabar el Pedido Destino para poder pegar la Cotización correctamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    Exit Sub
                End If
                If gintIDCabCopiaraPedido <> 0 Then
                    If MessageBox.Show("¿Está Ud. seguro de pegar la Cotización seleccionada en el actual Pedido?", _
                                   gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then

                        Me.Cursor = Cursors.WaitCursor
                        Dim objBL As New clsVentasCotizacionBT
                        objBL.Copiar(gintIDCabCopiaraPedido, cboCoEjeVta.SelectedValue, _
                                     gdatFechaCopiaraPedido, gstrUser, gblnTodosVuelosCopiaraPedido, intNuPedInt)

                        gintIDCabCopiaraPedido = 0
                        gdatFechaCopiaraPedido = "01/01/1900"
                        gblnTodosVuelosCopiaraPedido = False

                        ConsultarCotizaciones()
                        Me.Cursor = Cursors.Default
                        MessageBox.Show("La copia de la Cotización al Pedido destino se realizó exitosamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
                    Else
                        gintIDCabCopiaraPedido = 0
                        gdatFechaCopiaraPedido = "01/01/1900"
                        gblnTodosVuelosCopiaraPedido = False
                    End If
                Else
                    MessageBox.Show("No ha seleccionar ninguna Cotización previamente.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                End If
            End If
        Catch ex As Exception
            Me.Cursor = Cursors.Default
            Throw
        End Try
    End Sub
    Private Sub mnuCopiar_Click(sender As Object, e As EventArgs) Handles mnuCopiar.Click, mnuPegar.Click
        Try

            If sender.name = "mnuCopiar" Then
                CopiarPegarCotiz(True, dgv.CurrentRow.Cells("IDCab").Value, dgv.CurrentRow.Cells("Fecha").Value)
            ElseIf sender.name = "mnuPegar" Then
                CopiarPegarCotiz(False, 0, "01/01/1900")
            End If


        Catch ex As Exception

            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgv_MouseDown(sender As Object, e As MouseEventArgs) Handles dgv.MouseDown

        Try
            If e.Button = Windows.Forms.MouseButtons.Right Then
                dgv.ContextMenuStrip = cmsCopiarCotiz
                If gintIDCabCopiaraPedido = 0 Then
                    mnuCopiar.Enabled = True
                    mnuPegar.Enabled = False
                Else
                    mnuCopiar.Enabled = False
                    mnuPegar.Enabled = True
                End If
                cmsCopiarCotiz.Show(Cursor.Position)

                dgv.ContextMenuStrip = Nothing
                dgv.ContextMenuStrip = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub txtTxTitulo_TextChanged(sender As Object, e As EventArgs) Handles txtTxTitulo.TextChanged, _
            cboCoEjeVta.SelectionChangeCommitted, lblCliente.TextChanged, cboEstado.SelectionChangeCommitted, _
            cboUsuarioRealizado.SelectionChangeCommitted, dtpFePedido.ValueChanged
        If sender.name = "txtTxTitulo" Then
            ErrPrv.SetError(txtTxTitulo, "")
        ElseIf sender.name = "cboCoEjeVta" Then
            ErrPrv.SetError(cboCoEjeVta, "")
        ElseIf sender.name = "lblCliente" Then
            ErrPrv.SetError(btnCliente, "")
        End If

        If blnLoad Then blnCambios = True
    End Sub




    Public Sub AgregarEditarEliminarItinerario_Archivos(ByVal vbytIndex As Byte, ByVal vstrAccion As String)
        Dim strAccion As String = "" 'N,B,M
        Dim bytCoDetalle As Integer = 0
        Try
            dgvArchivos.CommitEdit(DataGridViewDataErrorContexts.LeaveControl)
            bytCoDetalle = dgvArchivos.Item("IDArchivo", vbytIndex).Value

            Dim stReg As stArchivos
            Dim intInd As Int16
            For Each ST As stArchivos In objLstArchivos
                If ST.IDArchivo = bytCoDetalle Then
                    If ST.Accion <> "N" Then
                        strAccion = "M"
                    Else
                        strAccion = "N"
                    End If
                    objLstArchivos.RemoveAt(intInd)
                    Exit For
                End If
                intInd += 1
            Next

            If vstrAccion <> "B" Then
            Else
                strAccion = vstrAccion
            End If

            stReg = New stArchivos
            With stReg

                '              IDArchivo,
                '	IDCAB,
                '	ca.Nombre,
                '	RutaOrigen,
                '	RutaDestino,
                '	CoTipo,
                'Tipo=CoTipo,
                '	FlCopiado,
                'ca.FecMod as FechaModificacion,
                '	CoUsuarioCreacion,
                'Usuario=u.Nombre +' '+u.TxApellidos


                .IDArchivo = bytCoDetalle
                'If Not dgvArchivos.Item("IDCAB", vbytIndex).Value Is Nothing Then
                '    .IDCAB = dgvArchivos.Item("IDCAB", vbytIndex).Value.ToString
                'End If

                If Not dgvArchivos.Item("Nombre", vbytIndex).Value Is Nothing Then
                    .Nombre = dgvArchivos.Item("Nombre", vbytIndex).Value.ToString
                End If

                If Not dgvArchivos.Item("RutaOrigen", vbytIndex).Value Is Nothing Then
                    .RutaOrigen = dgvArchivos.Item("RutaOrigen", vbytIndex).Value.ToString
                End If

                If Not dgvArchivos.Item("RutaDestino", vbytIndex).Value Is Nothing Then
                    .RutaDestino = dgvArchivos.Item("RutaDestino", vbytIndex).Value.ToString
                End If

                If Not dgvArchivos.Item("CoTipo", vbytIndex).Value Is Nothing Then
                    .CoTipo = dgvArchivos.Item("CoTipo", vbytIndex).Value.ToString
                End If

                If Not dgvArchivos.Item("Tipo_Archivo", vbytIndex).Value Is Nothing Then
                    .Tipo = dgvArchivos.Item("Tipo_Archivo", vbytIndex).Value.ToString
                End If

                If Not dgvArchivos.Item("FlCopiado", vbytIndex).Value Is Nothing Then
                    .FlCopiado = dgvArchivos.Item("FlCopiado", vbytIndex).Value.ToString
                End If

                If Not dgvArchivos.Item("FechaModificacion", vbytIndex).Value Is Nothing Then
                    .FechaModificacion = dgvArchivos.Item("FechaModificacion", vbytIndex).Value.ToString
                End If


                If Not dgvArchivos.Item("CoUsuarioCreacion", vbytIndex).Value Is Nothing Then
                    .CoUsuarioCreacion = dgvArchivos.Item("CoUsuarioCreacion", vbytIndex).Value.ToString
                End If

                If Not dgvArchivos.Item("Usuario", vbytIndex).Value Is Nothing Then
                    .Usuario = dgvArchivos.Item("Usuario", vbytIndex).Value.ToString
                End If


                If strAccion = "" Then
                    If Not blnCargaIniStruct Then
                        .Accion = "N"
                    Else
                        .Accion = strAccion
                    End If
                Else
                    .Accion = strAccion
                End If
            End With
            objLstArchivos.Add(stReg)
        Catch ex As Exception
            Throw
        End Try
    End Sub


    'PONER EL METODO EN CARGAR CONTROLES
    Private Sub CargarArchivos_File()
        Try
            Dim objBLDet As New clsCotizacionBN.clsCotizacionArchivosBN
            Dim dtDetalles As DataTable = objBLDet.ConsultarListxIDCab(intIDCab)
            dgvArchivos.Rows.Clear()
            For Each drDet As DataRow In dtDetalles.Rows
                dgvArchivos.Rows.Add()
                For i As Byte = 0 To dtDetalles.Columns.Count - 1
                    'If dgvArchivos.Columns(i).Name = "SsTotal" Then
                    '    dgvArchivos.Item(i, dgvArchivos.Rows.Count - 1).Value = Format(CDbl(drDet(i)), "###,##0.00")
                    'Else
                    dgvArchivos.Item(i, dgvArchivos.Rows.Count - 1).Value = drDet(i)
                    'End If
                Next
            Next

            If dtDetalles.Rows.Count = 0 Then
                'CargarDetallePorDefecto()
            Else
                blnCargaIniStruct = True
                For Each dgvItem As DataGridViewRow In dgvArchivos.Rows
                    AgregarEditarEliminarItinerario_Archivos(dgvItem.Index, "")
                Next
                blnCargaIniStruct = False
            End If

            'PintarTotales()
        Catch ex As Exception
            Throw
        End Try
    End Sub


    Private Sub btnNuevoArchivo_Click(sender As Object, e As EventArgs) Handles btnNuevoArchivo.Click
        Try
            Dim frm As New frmIngCotizacionDato_ArchivosMem
            frm.frmRefP = Me
            frm.strIDFile = strIDFileAceptado
            frm.strTitulo = strTituloFile
            frm.datFechaFile = datFechaFile
            frm.strTipoFormArchivos = strTipoFormArchivos
            frm.ShowDialog()
            If blnActualizoArchivo Then
                AgregarEditarEliminarItinerario_Archivos(dgvArchivos.Rows.Count - 1, "N")
                blnActualizoArchivo = False
                blnCambios = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvArchivos_CellClick(sender As Object, e As DataGridViewCellEventArgs) Handles dgvArchivos.CellClick
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            If e.ColumnIndex = dgvArchivos.Columns("btnEditA").Index Then
                Me.Editar_Archivo()
            ElseIf e.ColumnIndex = dgvArchivos.Columns("btnDelA").Index Then
                If MessageBox.Show("¿Está Ud. seguro de eliminar el registro de la cuadrícula?", gstrTituloMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question) = Windows.Forms.DialogResult.Yes Then
                    AgregarEditarEliminarItinerario_Archivos(e.RowIndex, "B")
                    dgvArchivos.Rows.RemoveAt(e.RowIndex)
                    blnCambios = True

                End If
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Function bytGenerarNuArchivo() As Byte
        If objLstArchivos.Count = 0 Then Return 1
        Try
            Dim bytNuDocumProvDet As Byte = 0
            For Each ST As stArchivos In objLstArchivos
                If ST.IDArchivo > bytNuDocumProvDet Then bytNuDocumProvDet = ST.IDArchivo
            Next

            Return (bytNuDocumProvDet + 1)
        Catch ex As Exception
            Throw
        End Try
    End Function

    Private Sub Editar_Archivo()
        If dgvArchivos.CurrentRow Is Nothing Then Exit Sub
        Try
            Dim frm As New frmIngCotizacionDato_ArchivosMem
            frm.bytNuSegmento = dgvArchivos.CurrentRow.Cells("IDArchivo").Value
            frm.strDescripcion = dgvArchivos.CurrentRow.Cells("IDArchivo").Value
            frm.frmRefP = Me
            frm.strIDFile = strIDFileAceptado
            frm.strTitulo = strTituloFile
            frm.datFechaFile = datFechaFile 'CDate(lblFecha.Text)
            frm.ShowDialog()
            If blnActualizoArchivo Then
                AgregarEditarEliminarItinerario_Archivos(dgvArchivos.CurrentRow.Index, "")
                blnActualizoArchivo = False
                blnCambios = True
                'CalcularTotales_Detalle()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvArchivos_DoubleClick(sender As Object, e As EventArgs) Handles dgvArchivos.DoubleClick
        If dgvArchivos.CurrentRow Is Nothing Then Exit Sub
        Dim sArchivo As String
        sArchivo = dgvArchivos.CurrentRow.Cells("RutaDestino").Value
        Me.EjecutarArchivo(sArchivo)
    End Sub

    Private Sub EjecutarArchivo(ByVal vstrArchivo As String)
        Try
            If System.IO.File.Exists(vstrArchivo) Then
                System.Diagnostics.Process.Start(vstrArchivo)
            Else
                MessageBox.Show("No se encuentra el archivo.", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Information)
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Sub dgvArchivos_DragEnter(sender As Object, e As DragEventArgs) Handles dgvArchivos.DragEnter
        Try
            If (e.Data.GetDataPresent(DataFormats.FileDrop)) Then
                e.Effect = DragDropEffects.All
            Else
                e.Effect = DragDropEffects.None
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvArchivos_DragDrop(sender As Object, e As DragEventArgs) Handles dgvArchivos.DragDrop
        Try
            Dim s() As String = e.Data.GetData("FileDrop", False)
            'Dim i As Integer
            MsgBox(s(0))
        Catch ex As Exception

        End Try
    End Sub

    Private Sub CrearCarpetasArchivos()
        Try
            Dim strRuta As String = gstrRutaArchivos_File
            ' Dim datFechaFile As Date = CDate(lblFecha.Text)
            Dim strAnioMes As String = Month(datFechaFile).ToString.PadLeft(2, "0") + Year(datFechaFile).ToString
            Dim strRutaFile1 As String = strIDFileAceptado + " " + strReemplazarNombreCarpeta(strTituloFile)
            Dim strTipoTmp As String = ""
            Dim strRutaFinal1 As String = strRuta + Year(datFechaFile).ToString + "\" + strAnioMes + "\" + strRutaFile1

            Dim strRutaFinal2 As String = ""

            Dim blnExisteRuta As Boolean
            blnExisteRuta = System.IO.Directory.Exists(strRutaFinal1)

            If System.IO.Directory.Exists(strRutaFinal1) = False Then
                'si no existe la ruta crearla
                System.IO.Directory.CreateDirectory(strRutaFinal1)
            End If



            'Dim strRutaFinal1 As String = strRuta + Year(datFechaFile).ToString + "\" + strAnioMes + "\" + strRutaFile1
            For Each objSTArchivo As stArchivos In objLstArchivos
                If objSTArchivo.FlCopiado = False Then
                    Dim objSTArchivoTmp As New stArchivos
                    objSTArchivoTmp = objSTArchivo
                    strTipoTmp = objSTArchivo.Tipo
                    'strRutaFinal2 = strRutaFinal1 + "\" + objSTArchivo.Tipo
                    If System.IO.Directory.Exists(strRutaFinal1 + "\" + objSTArchivo.Tipo) = False Then
                        'si no existe la ruta crearla
                        objSTArchivo.RutaDestino = strRutaFinal1 + "\" + objSTArchivo.Tipo + "\" + objSTArchivo.Nombre
                        System.IO.Directory.CreateDirectory(strRutaFinal1 + "\" + objSTArchivo.Tipo)
                    End If
                    '                'copiar el archivo
                    '                My.Computer.FileSystem.CopyFile(
                    'objSTArchivo.RutaOrigen,
                    '                objSTArchivo.RutaDestino,
                    'Microsoft.VisualBasic.FileIO.UIOption.AllDialogs,
                    'Microsoft.VisualBasic.FileIO.UICancelOption.DoNothing)


                    'Dim blnExisteRuta As Boolean
                    'blnExisteRuta = System.IO.Directory.Exists("c:\ExistingFolderName")
                End If
            Next
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try

    End Sub

    Private Sub dgvArchivos_CellPainting(sender As Object, e As DataGridViewCellPaintingEventArgs) Handles dgvArchivos.CellPainting
        If e.RowIndex = -1 Then Exit Sub
        If e.ColumnIndex = -1 Then Exit Sub
        Try
            If dgvArchivos.Columns(e.ColumnIndex).Name = "btnEditA" Then
                e.Paint(e.CellBounds, DataGridViewPaintParts.All)
                Dim celBoton As DataGridViewButtonCell = CType(dgvArchivos.Rows(e.RowIndex).Cells("btnEditA"), DataGridViewButtonCell)
                Dim icoAtomico As Image
                'If Not bln_mMigrado Then
                '    icoAtomico = My.Resources.Resources.editarpng
                'Else
                '    icoAtomico = My.Resources.Resources.zoom
                'End If
                icoAtomico = My.Resources.Resources.editarpng
                e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
                dgvArchivos.Rows(e.RowIndex).Height = 21
                dgvArchivos.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
                e.Handled = True
            End If

            If dgvArchivos.Columns(e.ColumnIndex).Name = "btnDelA" Then
                e.Paint(e.CellBounds, DataGridViewPaintParts.All)
                Dim celBoton As DataGridViewButtonCell = CType(dgvArchivos.Rows(e.RowIndex).Cells("btnDelA"), DataGridViewButtonCell)
                Dim icoAtomico As Image = My.Resources.Resources.eliminarpng
                e.Graphics.DrawImage(icoAtomico, e.CellBounds.Left + 3, e.CellBounds.Top + 3)
                dgvArchivos.Rows(e.RowIndex).Height = 21
                dgvArchivos.Columns(e.ColumnIndex).Width = icoAtomico.Width + 5
                e.Handled = True
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Private Function TieneFile() As Boolean
        Try
            Dim bln As Boolean = False
            For Each row As DataGridViewRow In dgv.Rows
                Dim strIDFile_Tmp As String = row.Cells("NuFile").Value.ToString.Trim
                If strIDFile_Tmp <> "" Then
                    bln = True
                    intIDCab_File = row.Cells("IDCab").Value.ToString.Trim
                End If
            Next
            Return bln
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Function

    Private Sub MostrarOcultarTabArchivos()
        Try
            If TieneFile() Then
                tbArchivos.Parent = tabPedido
                CargarDatosFile()
            Else
                tbArchivos.Parent = Nothing
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error)
        End Try
    End Sub

    Public Sub CargarDatosFile()
        Try
            Using objBN As New clsCotizacionBN
                Using dr As SqlClient.SqlDataReader = objBN.ConsultarPk(intIDCab_File)
                    dr.Read()
                    If dr.HasRows Then


                        datFechaFile = dr("Fecha")
                        strTituloFile = dr("Titulo")
                        strIDFileAceptado = dr("IDFile")
                        CargarArchivos_File()
                    Else
                        MessageBox.Show("El registro ha sido eliminado recientemente", gstrTituloMsg, MessageBoxButtons.OK, MessageBoxIcon.Exclamation)
                    End If
                    dr.Close()
                End Using
            End Using
        Catch ex As Exception
            Throw
        End Try
    End Sub
End Class